<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII: Patenty na oprogramowanie w Europie

#descr: For the last few years the European Patent Office (EPO) has, contrary
to the letter and spirit of the existing law, granted more than 30000
patents on rules of organisation and calculation claimed in terms of
general-purpose computing equipment, called %(q:programs for
computers) in the law of 1973 and %(q:computer-implemented inventions)
in EPO Newspeak since 2000.  Europe's patent movement is pressing to
legitimate this practise by writing a new law.  Although the patent
movement has lost major battles in November 2000 and September 2003,
Europe's programmers and citizens are still facing considerable risks.
 Here you find the basic documentation, starting from the latest news
and a short overview.

#lae: Why all this fury about software patents?

#dsW: If Haydn had patented %(q:a symphony, characterised by that sound is
produced [ in extended sonata form ]), Mozart would have been in
trouble.

#ltd: Unlike copyright, patents can block independent creations.  Software
patents can render software copyright useless.  One copyrighted work
can be covered by hundreds of patents of which the author doesn't even
know but for whose infringement he and his users can be sued.  Some of
these patents may be impossible to work around, because they are broad
or because they are part of communication standards.

#eas: Evidence from %(es:economic studies) shows that software patents have
lead to a decrease in R&D spending.

#iWd: %(it:Advances in software are advances in abstraction).  While
traditional patents were for concrete and physical %(e:inventions),
software patents cover %(e:ideas).  Instead of patenting a specific
mousetrap, you patent any %(q:means of trapping mammals) or %(ep:means
of trapping data in an emulated environment).  The fact that the
universal logic device called %(q:computer) is used for this does not
constitute a limitation.  %(s:When software is patentable, anything is
patentable).

#tek: In most countries, software has, like mathematics and other abstract
subject matter, been explicitely considered to be outside the scope of
patentable inventions.  However these rules were broken one or another
way.  The patent system has gone out of control.  A closed community
of patent lawyers is creating, breaking and rewriting its own rules
without much supervision from the outside.

#oOv: Current Situation in Europe

#and: What can we do?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: mlhtimport@ffii.org ;
# login: ciniol ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpat ;
# txtlang: pl ;
# multlin: t ;
# End: ;

