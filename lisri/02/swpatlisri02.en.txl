<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#K3h: FFII debunks myths about EPC conference of 1973

#IAB: Meanwhile the protocol of the %(bt:German Federal Parliament's Software Patent Hearing) has been made public and is accessible through our page.  Some speakers clearly criticised the %(q:weird, oops, wise) development of patentability doctrines by the BGH patent senate.  In the face of general laughter about the moderator's subtle irony, advocates of these BGH/EPO developments such as MPI patent lawyer %(ds:Daniele Schiuma) had a hard time.  All patentability advocates had to acknowledge that economic studies show only negative effects of patents in the software field.  The %(kb:legal affairs delegate of the German IT super-association Bitkom, Ms Dr. iur. Kathrin Bremer) entangled herself in half-understood doctrinal arguments from her predecessor in office, IBM patent attorney Fritz Teufel, claiming that the economic studies are based on bad methods and that copyright leads to as much legal insecurity as patents.  Unfortunately hers and other written statements are still not available in spite of repeated requests by FFII.  Thus Ms Bremer's  remains the only official public statement that has so far emanated from the super-association, whose members are known to be mostly uninformed and divided about this subject.

#Ppn: Bundestag swpat hearing protocol published

#Vae: Moreover this is of interest as a case for comparison.  It should e.g. be asked, whether this time the exemption of computer programs is to be taken seriously in practise or whether there are still possibilities to work around the literal meaning of the text.  Inversely it is to be asked whether the reasoning used to support the non-observance of Art 52 EPC also leads to non-observance of the new EG design patent regulation or whether for some reason it doesn't apply here.

#Dbu: This is of interest as a parallel case for the patent protection of computer programs.  First of all it is noteworthy that newest immaterial property laws at the european level once again explicitely exempt computer programs.  In contrast to %(ep:art 52 EPC) this is the only exemption.  It shows that there is no fatal necessity for european legislation to always move in the direction of property rights inflation.

#cen: %(q:komplexes Erzeugnis) ein Erzeugnis aus mehreren Bauelementen, die sich ersetzen lassen, so dass das Erzeugnis auseinander- und wieder zusammengebaut werden kann.

#cnh: %(q:Erzeugnis) jeden industriellen oder handwerklichen Gegenstand, einschliesslich - unter anderem - der Einzelteile, die zu einem komplexen Erzeugnis zusammengebaut werden sollen, Verpackung, Ausstattung, graphischen Symbolen und typographischen Schriftbildern; ein Computerprogramm gilt jedoch nicht als %(q:Erzeugnis);

#css: %(q:Geschmacksmuster) die Erscheinungsform eines Erzeugnisses oder eines Teils davon, die sich insbesondere aus den Merkmalen der Linien, Konturen, Farben, der Gestalt, Oberflächenstruktur und/oder der Werkstoffe des Erzeugnisses selbst und/oder seiner Verzierung ergibt;

#Isn: Im Sinne dieser Verordnung bezeichnet:

#A0a: Art. 3 Verordnung 6/2002 der EG, Gemeinschaftsgeschmacksmuster,

#Imn: The EC Official Journal L3 2002 contains the %(ab:Regulation about a Community Design Patent).  Art 3 of this regulation defines %(q:design patent) in the following way

#Cmn: Computer programs exempted in new EC regulation

#Dri: The patent experts of the European Commission in the General Directorate for the Internal Market have, together with colleagues from teh Research Directorate, published a study, which on the one hand side finds that the publication of research results is being more or less severely delayed by the patenting process and that most knowledge would have been published as early without patent protection.  On the other hand the study points to a high revenue stream from gene patents from which US universities have benefitted and concludes from this that gene patents are good for the economy.  The argumentation flaws found in this study are similar to those known from DGIM software patentability papers.

#Ein: EC study on the effects of gene patents on research

#DWp2: The French Socialist Party (PSF) has pronounced itself against software patents as the EPO is granting them and has demanded a %(q:fight) against abusive policies of the EPO.  The PSF also notes that there is considerable US pressure for software patentability in Europa that those who want software patents have yet to convincingly argue that patentability in this field is conducive to innovation.

#DWp: French Governmental Party against EPO swpat

#ESp: CEC adopts BSA directive proposal

#DWt: The UK patent family is planning a propaganda rally in support of software patentability and the directive proposal drafted by their Brussels family members together with BSA.  All big names will be there, and FFII/Eurolinux will be presented with a doctored speech summary in a specially engineered setting so as to make the software patentability critics look minoritarian.

#Mom: MS will use patents to crack down on opensource implementations of Dotnet

#IWw: In an %(iv:interview) from 2002/02, a leading developper working for IBM explained some aspects of IBM's patent policy and its conflict with the policy of supporting GNU/Linux.

#Ist: IBM Linux guru speaks out about patent dangers

#Tie: The core kernel developper and CTO of Redhat Inc in Britain, wrote an article on a major %(lu:British Linux portal), in which he criticises the UK Patent Office's new consultation on the CEC Directive as another deceptive maneuver and asks readers to participate in a campaign effort and to inform themselves by reading the %(q:excellent ressources) provided by FFII.

#Uvn: UK-PTO launches deceptive consultation -- Alan Cox calls for action

#Dpa: DE-PTO celebrates patent inflation, announces new round

#1dt: The basic pattern of conflict around software patents has stayed much the same.  We have unearthed an important document.

#Ihi: In a response to a parliamentary inquiry from the Christian Democrats, the german government, represented by the ministry of justice, presented the problem of patentability of %(q:computer-implemented inventions) (i.e. rules of organisation and calculation) mainly as one of helping patent offices to inform freelance programmers and SMEs about the %(q:legal situation) (i.e. the illegal caselaw of the EPO) and bringing all national jurisdiction in line with that situation.  It characterised the CEC/BSA directive proposal as an appropriate negotiation basis for achieving this aim.  Meanwhile the BMJ has invited a circle of pro-swpat lawyers from large organisations (with 1 %(q:open source movement representative), excluding the FFII) to discuss grammatical questions of law wording under the premise that the CEC/BSA approach is basically desirable.  A semi-internal BMJ memorandum asserts that this circle reached a consensus in favor of allowing direct computer program and data structure claims, something even CEC/BSA shyed away from.  Meanwhile, the chancellor's office appears to be pursuing a policy of keeping swpat out of the election campaign debates, and critical MPs are receiving pressure to keep quiet instead of answers to their questions.

#Gtn: Bundesregierung vorsichtig für grenzenlose Patentierbarkeit

#Tme: The Council of the European Union (CEU) proposes to rewrite some articles of the CEC/BSA proposal of 2002/02/20 in order to take into account various criticisms made by national delegations to the Council's Intellectual Property Working Party, a workgroup consisting of delegates from national patent administrations.  This counter-proposal was worked out by the delegates from Denmark, i.e. from the Danish Patent Office (DKPTO), which are presiding over the workgroup during the second half of 2002.  The paper is the subject of decision at the Working Party's session on 2002/10/03 in Brussels.  We present the paper in tabular comparison with the original CEC/BSA proposal of 2002/02/20.  It becomes evident that the DKPTO proposal, while strengthening the rhetorical emphasis on the %(q:technical contribution), creates additional ambiguities and in effect only further widens the scope of patentability.

#Cmb: CEU/DKPTO 2002/09/23: Amended Software Patentability Directive Proposal

#nWi: News Items

#iWh: Major Events

#descr: What FFII had to report in 2002 about broad property claims on inventions and their abusive extension to computer-aided reasoning, calculating, organising and formulating.

#title: FFII Software Patent News 2002

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri02 ;
# txtlang: en ;
# multlin: t ;
# End: ;

