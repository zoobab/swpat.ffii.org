<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: Ce que la FFII avait pour rapporter en 2002 sur les monopoles d'invention accordés par l'état et leur extension abusive sur la pensée, le calcul, l'organisation et la formulation a l'aide de l'ordinateur universel.
#title: Nouvelles sur les Brevets Logiciels en 2002
#iWh: Évènements des l'Année
#nWi: Nouvelles Individuelles
#Cmb: CUE/DK 2002/09/23: Propos de Directive pour Brevets Logiciels Revisé
#Tme: The Council of the European Union (CEU) proposes to rewrite some articles of the CEC/BSA proposal of 2002/02/20 in order to take into account various criticisms made by national delegations to the Council's Intellectual Property Working Party, a workgroup consisting of delegates from national patent administrations.  This counter-proposal was worked out by the delegates from Denmark, i.e. from the Danish Patent Office (DKPTO), which are presiding over the workgroup during the second half of 2002.  The paper is the subject of decision at the Working Party's session on 2002/10/03 in Brussels.  We present the paper in tabular comparison with the original CEC/BSA proposal of 2002/02/20.  It becomes evident that the DKPTO proposal, while strengthening the rhetorical emphasis on the %(q:technical contribution), creates additional ambiguities and in effect only further widens the scope of patentability.
#Gtn: Gouvernment Allemand en faveur de la brevetabilité sans limites
#Ihi: In a response to a parliamentary inquiry from the Christian Democrats, the german government, represented by the ministry of justice, presented the problem of patentability of %(q:computer-implemented inventions) (i.e. rules of organisation and calculation) mainly as one of helping patent offices to inform freelance programmers and SMEs about the %(q:legal situation) (i.e. the illegal caselaw of the EPO) and bringing all national jurisdiction in line with that situation.  It characterised the CEC/BSA directive proposal as an appropriate negotiation basis for achieving this aim.  Meanwhile the BMJ has invited a circle of pro-swpat lawyers from large organisations (with 1 %(q:open source movement representative), excluding the FFII) to discuss grammatical questions of law wording under the premise that the CEC/BSA approach is basically desirable.  A semi-internal BMJ memorandum asserts that this circle reached a consensus in favor of allowing direct computer program and data structure claims, something even CEC/BSA shyed away from.  Meanwhile, the chancellor's office appears to be pursuing a policy of keeping swpat out of the election campaign debates, and critical MPs are receiving pressure to keep quiet instead of answers to their questions.
#1dt: The basic pattern of conflict around software patents has stayed much the same.  We have unearthed an important document.
#Dpa: Office Allemand de Brevets annonce nouvelle ronde d'inflation de brevet

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri02 ;
# txtlang: fr ;
# End: ;

