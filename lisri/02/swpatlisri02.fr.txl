<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#K3h: FFII debunks myths about EPC conference of 1973

#IAB: Entretemps le protocl de l'%(bt:Audition du Parlament Fédéral Allemand sur les Brevets Logiciels) a été publié et rendu accessible a travers notre page.

#Ppn: protocole de l'audition brevets logiciels du Bundestag publié

#Vae: Surtout il y a ici un cas de comparaison intéressant.  On doit par exemple se demander si cette fois ci l'exclusion de programmes d'ordinateur doit être prise au sérieux ou si il y a en pratique encore des possibilité de contorsion du sense litérale du texte.  Conversement on doit se demander si le raisonnement utilisé par l'OEB pour rendre art 52 CEB ineffectif depuis 1986/1998 doit aussi être utilisé pour rendre cette nouvelle régulation communautaire inapplicable du début.

#Dbu: Cela est intéressant pour la discussion du cas parallel de la protection des programmes d'ordinateur par brevet.  D'abord il est intéressant que une fois plus les %(q:programmes d'ordinateur) sont explicitement exclus de la protection dans une régulation européenne sur und droit de appropriation d'idées.  En contraste avec %(ep:art 52 CEB) c'est ici la seule exclusion.  Donc il est claire que la législation européenne ne marche pas nécessairement toujours dans la direction d'une inflation de droit d'appropriation.

#cen: %(q:komplexes Erzeugnis) ein Erzeugnis aus mehreren Bauelementen, die sich ersetzen lassen, so dass das Erzeugnis auseinander- und wieder zusammengebaut werden kann.

#cnh: %(q:Erzeugnis) jeden industriellen oder handwerklichen Gegenstand, einschliesslich - unter anderem - der Einzelteile, die zu einem komplexen Erzeugnis zusammengebaut werden sollen, Verpackung, Ausstattung, graphischen Symbolen und typographischen Schriftbildern; ein Computerprogramm gilt jedoch nicht als %(q:Erzeugnis);

#css: %(q:Geschmacksmuster) die Erscheinungsform eines Erzeugnisses oder eines Teils davon, die sich insbesondere aus den Merkmalen der Linien, Konturen, Farben, der Gestalt, Oberflächenstruktur und/oder der Werkstoffe des Erzeugnisses selbst und/oder seiner Verzierung ergibt;

#Isn: Im Sinne dieser Verordnung bezeichnet:

#A0a: Art. 3 Verordnung 6/2002 der EG, Gemeinschaftsgeschmacksmuster,

#Imn: Le Journal Officiel CE L3 2002 contient la %(ab:Régulation sur un Brevet de Dessein Communautaire).  L'article 3 de cette régulation definisse %(q:brevet de dessein) de façon suivante:

#Cmn: Exclusions de logiciels dans une nouvelle régulation CE

#Dri: Les experts en propriété industrielle de la commission européenne dans la Direction Générale du Marché Intérieur ont, ensemble avec des collègues de la DG Recherche, publié un rapport qui montre un effet plutôt négatif du processus de brevetage sur la publication de résulats de recherche en génétique.  Mais le rapport argumente pour tels brevets, en citant des rapports américains selon lesquels les universités aux EUA ont gagné larges sommes d'argent par des brevets récemment.

#Ein: Étude CE sur les effets des brevets du genome sur la recherche

#DWp2: Le Parti Socialiste Francais s'a prononcé clairement contre la brevetabilité du logiciel et contre la pratique de l'OEB dans ce domaine.

#DWp: PSF contre la politique de l'OEB sur les brevets logiciels

#ESp: CE accepte Propos de Directive de BSA

#DWt: The UK patent family is planning a propaganda rally in support of software patentability and the directive proposal drafted by their Brussels family members together with BSA.  All big names will be there, and FFII/Eurolinux will be presented with a doctored speech summary in a specially engineered setting so as to make the software patentability critics look minoritarian.

#Mom: MS va utiliser ses brevets pour empecher les mises en oevre libres de Dotnet

#IWw: Dans un %(iv:interview) de février, un developpeur chef de IBM explique quelques problèmes et contradictions dans la politique de IBM sur les brevets.

#Ist: Developpeur IBM: IBM a peur de Linux a cause de Brevets

#Tie: Le developpeur Linux et Redhat critique dans un article sur un %(lu:grand portail britannique de Linux) la nouvelles %(q:consultation) de l'Office de Brevets Britannique comme une friponnerie et demande une action forte de la part de la communité du logiciel.

#Uvn: Consultation Déceptive de l'Office Britannique -- Alan Cox lance une action

#Dpa: Office Allemand de Brevets annonce nouvelle ronde d'inflation de brevet

#1dt: The basic pattern of conflict around software patents has stayed much the same.  We have unearthed an important document.

#Ihi: In a response to a parliamentary inquiry from the Christian Democrats, the german government, represented by the ministry of justice, presented the problem of patentability of %(q:computer-implemented inventions) (i.e. rules of organisation and calculation) mainly as one of helping patent offices to inform freelance programmers and SMEs about the %(q:legal situation) (i.e. the illegal caselaw of the EPO) and bringing all national jurisdiction in line with that situation.  It characterised the CEC/BSA directive proposal as an appropriate negotiation basis for achieving this aim.  Meanwhile the BMJ has invited a circle of pro-swpat lawyers from large organisations (with 1 %(q:open source movement representative), excluding the FFII) to discuss grammatical questions of law wording under the premise that the CEC/BSA approach is basically desirable.  A semi-internal BMJ memorandum asserts that this circle reached a consensus in favor of allowing direct computer program and data structure claims, something even CEC/BSA shyed away from.  Meanwhile, the chancellor's office appears to be pursuing a policy of keeping swpat out of the election campaign debates, and critical MPs are receiving pressure to keep quiet instead of answers to their questions.

#Gtn: Gouvernment Allemand en faveur de la brevetabilité sans limites

#Tme: The Council of the European Union (CEU) proposes to rewrite some articles of the CEC/BSA proposal of 2002/02/20 in order to take into account various criticisms made by national delegations to the Council's Intellectual Property Working Party, a workgroup consisting of delegates from national patent administrations.  This counter-proposal was worked out by the delegates from Denmark, i.e. from the Danish Patent Office (DKPTO), which are presiding over the workgroup during the second half of 2002.  The paper is the subject of decision at the Working Party's session on 2002/10/03 in Brussels.  We present the paper in tabular comparison with the original CEC/BSA proposal of 2002/02/20.  It becomes evident that the DKPTO proposal, while strengthening the rhetorical emphasis on the %(q:technical contribution), creates additional ambiguities and in effect only further widens the scope of patentability.

#Cmb: CUE/DK 2002/09/23: Propos de Directive pour Brevets Logiciels Revisé

#nWi: Nouvelles Individuelles

#iWh: Évènements des l'Année

#descr: Ce que la FFII avait pour rapporter en 2002 sur les monopoles d'invention accordés par l'état et leur extension abusive sur la pensée, le calcul, l'organisation et la formulation a l'aide de l'ordinateur universel.

#title: Nouvelles sur les Brevets Logiciels en 2002

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri02 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

