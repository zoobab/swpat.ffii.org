<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#K3h: Konferenzakte von 1973 vs Freimütige Geschichtsauslegung des EPA/BGH

#IAB: Inzwischen ist das Protokoll der %(bt:Bundestagsanhörung %(q:Softwarepatente und Opensource) von 2001-06-21) veröffentlicht und über unsere Seite einsehbar.  Einige Redner sparten nicht mit Kritk und an den %(q:wirren, nein weisen) Entwicklung der BGH-Rechtsprechung.  Angesichts des allgemeinen Gelächters über die subtile Ironie des Gesprächsleiters standen Apologeten der Patentierbarkeit wie MPI-Patentanwalt %(ds:Daniele Schiuma) ein wenig auf verlorenem Posten.  Sie mussten anerkennen, dass die volkswirtschaftliche Forschung durchweg entweder keine oder negative Wirkungen von Patenten auf die Softwareinnovation erwarten lässt.  Die %(kb:Vertreterin des Branchenverbandes Bitkom, Frau Dr. iur. Kathrin Bremer) referierte halbverstandene Argumente ihres Amtsvorgängers Teufel (IBM-Patentanwalt), denen zufolge die wirtschaftlichen Studien nicht aussagekräftig sind.  Ferner meinte Frau Bremer, %(q:im Opensource-Bereich) habe %(q:man) von der Rechtslage keine Ahnung und wisse daher z.B. nicht, dass Softwarepatente unvermeidbar und auch nicht gefährlicher als das Urheberrecht seien.  Leider sind einige der schriftlichen Stellungnahmen bisher trotz wiederholter Anfrage des FFII nicht öffentlich verfügbar.  So bleibt Frau Bremers Gestammel die einzige bisherige offizielle Meinungsäußerung des zerstrittenen Branchenverbandes.

#Ppn: Protokoll der Softwarepatente-Anhörung im Bundestag veröffentlicht

#Vae: Vor allem ist dies aber auch theoretisch als Vergleichsfall interessant. So wäre etwa zu fragen, ob der Ausschluss von Computerprogrammen diesmal nicht nur im Gesetz steht, sondern auch in der Praxis so ernstgenommen werden muss, oder ob es nicht auch hier noch Möglichkeiten gibt, sich am klaren Wortlaut vorbeizumogeln. Umgekehrt wäre zu fragen, ob Begründungen, die für die Missachtung des Verbotes in Art. 52 EPÜ geliefert werden, auch für das hier vorliegende Verbot zu dessen Missachtung führen müssten, oder ob dies nicht der Fall ist, wenn man derartige Begründungen diskutiert.

#Dbu: This is of interest as a parallel case for the patent protection of computer programs.  First of all it is noteworthy that newest immaterial property laws at the european level once again explicitely exempt computer programs.  In contrast to %(ep:art 52 EPC) this is the only exemption.  It shows that there is no fatal necessity for european legislation to always move in the direction of property rights inflation.

#cen: %(q:komplexes Erzeugnis) ein Erzeugnis aus mehreren Bauelementen, die sich ersetzen lassen, so dass das Erzeugnis auseinander- und wieder zusammengebaut werden kann.

#cnh: %(q:Erzeugnis) jeden industriellen oder handwerklichen Gegenstand, einschliesslich - unter anderem - der Einzelteile, die zu einem komplexen Erzeugnis zusammengebaut werden sollen, Verpackung, Ausstattung, graphischen Symbolen und typographischen Schriftbildern; ein Computerprogramm gilt jedoch nicht als %(q:Erzeugnis);

#css: %(q:Geschmacksmuster) die Erscheinungsform eines Erzeugnisses oder eines Teils davon, die sich insbesondere aus den Merkmalen der Linien, Konturen, Farben, der Gestalt, Oberflächenstruktur und/oder der Werkstoffe des Erzeugnisses selbst und/oder seiner Verzierung ergibt;

#Isn: Im Sinne dieser Verordnung bezeichnet:

#A0a: Art. 3 Verordnung 6/2002 der EG, Gemeinschaftsgeschmacksmuster,

#Imn: Im Amtsblatt der EG Nr. L3 dieses Jahres ist die %(ab:Verordnung über ein Geschmacksmuster der Gemeinschaft) abgedruckt.  Diese Verordnung enthält in ihrem Artikel 3 folgende Definition des Begriffes %(q:Geschmacksmuster):

#Cmn: Ausschluss von Computerprogrammen in neuer EG-Verordnung

#Dri: Die Patentexperten der Europäischen Kommission in der Generaldirektion Binnenmarkt haben zusammen mit Kollegen der Generaldirektion Forschung eine Studie veröffentlicht, die einerseits bei der Veröffentlichung von Forschungsergebnissen erhebliche Zeit- und Reibungsverluste durch Patentprozeduren feststellt und nachweist, dass das meiste Wissen auch ohne Patentschutz veröffentlicht würde, andererseits aber auf die hohen Einnahmen amerikanischer Universitäten aus Genpatenten verweisen und hieraus folgern, Genpatente seien ein volkswirtschaftlicher Erfolg.  Die Argumentationsfehler dieser Studie erinnern an Muster, die man aus den Softwarepatent-Papieren der Patentjuristen in der Generaldirektion Binnenmarkt kennt.

#Ein: EK-Bericht ueber Wirkung von Genpatenten auf Forschung

#DWp2: Die Französische Sozialistische Partei (PSF) hat sich gegen Softwarepatente und gegen die diesbezügliche Praxis des Europäischen Patentamtes (EPA) ausgesprochen und zugleich einen %(q:Kampf) gegen Rechtsmissbräuche des EPA gefordert.  Das PSF-Papier weist auch darauf hin, dass es beträchtlichen Druck aus den USA zur Einführung von Swpat in Europa gebe und dass es bislang die Swpat-Befürworter schlüssigen Argumente dafür schuldig geblieben seien, dass Softwarepatente insgesamt die Produktivität oder Innovativität der Softwarebranche erhöhen.

#DWp: Französische Regierungspartei gegen EPA-Swpat

#ESp: EuK nimmt BSA-Richtlinienvorschlag an

#DWt: Das Britische Patentamt und sein Klüngel sind derzeit das wichtigste Kraftzentrum der Softwarepatent-Lobby in Europa.  Am 19. Juni wird die britische Patentfamilie in Zusammenarbeit mit EPA, IBM u.a. in Brüssel eine Kundgebung für die Patentierbarkeit von Software im Sinne des EU/BSA-Richtlinienvorschlags halten, der auch unter maßgeblicher Mitwirkung des britischen Patentklüngels entstand.  Eine kleine Rolle als sozialromantischer Minderheitenvertreter ist auch für FFII/Eurolinux vorgesehen.  Unsere Vortragsankündigung wurde indes gefälscht, so dass sie in dieses Bild passt.

#Mom: MS wird Patente einsetzen, um quelloffene Implementationen von Dotnet zu verhindern

#IWw: In einem %(iv:Interview) vom Februar erklärt ein führender IBM-Entwickler einige Aspekte der Patentpolitik von IBM und die Interessenkonflikte innerhalb der Firma.

#Ist: IBM-Entwickler: IBM meidet Linux aus Angst vor Patenten

#Tie: Der Linux- und Redhat-Chefentwickler Alan Cox kritisiert in einem Artikel auf einem führenden %(lu:britischen Linux-Portal) die neueste Konsultationsübung des Britischen Patentamtes als ein betrügerisches Manöver und ruft die Softwaregemeinde zu entschlossenem Handeln auf.

#Uvn: Scheinheilige Konsultation des Britischen Patentamtes -- Alan Cox ruft zum Handeln

#Dpa: DPMA feiert Patentinflation, läutet neue Runde ein

#1dt: 1967 waren die Fronten der Auseinandersetzung um Swpat in den USA im wesentlichen die gleichen wie heute:  %(q:Patentanwälte in Konzernen und Regierungen)  auf der einen und allgemeines Desinteresse auf der anderen Seite.  Volkswirtschaftliche Überlegungen spielten kaum eine Rolle.  Die Patentjuristen preschten gegen etablierte Gesetze vor und verteidigten ihren Freiheit, ohne den Gesetzgeber Regeln zu setzen.  Wir haben ein wichtiges historisches Dokument zugänglich gemacht.

#Ihi: In einer Antwort auf eine Anfrage der CDU/CSU-Fraktion im Bundestag stellt die Bundesregierung das Problem der Patentierbarkeit von %(q:computer-implementierten Erfindungen) (d.h. Organisations- und Rechenregeln) im wesentlichen als ein Problem der Aufklärung unwissender Programmierer und KMU über die von den Patentämtern geschaffenen Tatsachen und der Anpassung der gesamten europäischen Rechtsprechung an diese Tatsachen dar.  Die Bundesregierung sieht demnach in dem Vorschlag von EUK/BSA eine geeignete Grundlage für ihre Bemühungen.  Um noch ein wenig an der Grammatik mancher der darin enthaltenen schwer verständlichen Sätze zu feilen, lud das Bundesministerium der Justiz (BMJ) am 2002-05-13 einen Kreis zuverlässiger Patentjuristen zu einer Diskussion ein.  Als Diskussionsergebnis wurde ein Konsens festgehalten, wonach die EUK/BSA-Richtlinie noch nicht weit genug geht: auch Patentansprüche auf funktionale Texte (Programmprodukt, Programm, Datenstruktur etc) müssten zugelassen werden.  Mit dieser Linie unzufriedene Bundestagsabgeordnete der Regierungskoalition wurden mit leeren Worten und mit Mahnungen zur Disziplin in Wahlkampfzeiten abgespeist.  Insbesondere aus dem Bundeskanzleramt verlautete, dass diese Diskussion derzeit höchst unerwünscht sei.

#Gtn: German Government cautiously in favor of unlimited patentability

#Tme: Der Rat der Europäischen Union (REU) schlägt vor, den Softwarepatent-Richtlinienvorschlages der Europäischen Kommission in einigen Punkten umzuschreiben, um diverse Kritikpunkte aufzunehmen, die im Kreis der Patentrechts-Arbeitsgruppe des Rates geäußert wurden.  Diese Arbeitsgruppe besteht aus Fachreferenten nationaler Regierungen, die wiederum weitgehend aus den nationalen Patentämtern stammen oder mit diesen in enger Tuchfühlung stehen.  Der Gegenvorschlag des REU stammt aus der Feder der dänischen Delegierten (d.h. des dänischen Patent- und Markenamtes DKPMA), die im zweiten Halbjahr 2002 die Präsidentschaft des REU inne haben.  Der DKPMA-Vorschlag steht auf der Arbeitssitzung vom 3. Oktober 2002 zur Entscheidung an.  Wir präsentieren ihn in tabellarischer Gegenüberstellung mit dem Entwurfder Europäischen Kommission (BSA/EUK-Entwurf) vom 20. Februar 2002.  Es zeigt sich, dass das DKPMA -- bei aller Beschwörung der Technizität -- zusätzliche Unklarheiten schafft und die Grenzen des Patentierbaren nur noch weiter ausdehnt.

#Cmb: REU/DKPMA 2002/09/23: Änderungsvorschläge zum Softwarepatente-Richtlinienentwurf

#nWi: Einzelne Nachrichten

#iWh: Ereignisse des Jahres

#descr: Was der FFII im Jahre 2002 über staatlich gewährte Erfindungsmonopole und ihre missbräuchliche Ausweitung auf rechnergestütztes Denken, Rechnen, Organisieren und Formulieren zu berichten hatte

#title: Logikpatentnachrichten 2002

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri02 ;
# txtlang: de ;
# multlin: t ;
# End: ;

