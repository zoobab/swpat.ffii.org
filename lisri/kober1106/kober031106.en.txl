<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#obK: Questions submitted by Piia-Noora Kauppi MEP

#tWu: Other Issues

#cnt: We welcome the EPO's new mission statement of %(q:strengthening
innovation and economic growth in the interest of European citizens).
In the European Parliament we have decided by large majority that
certain classes of patents which are currently granted by the EPO do
not serve innovation and economic growth. Is this reason enough for
the EPO to consider stopping to grant such patents?

#Wto: Vice president Desantes said in Paris at OECD that indeed this might
be a reason for the EPO to reconsider the whole policy on software.
Desantes and his colleagues showed a remarkable spirit of political
leadership, very much contrasting with that of the USPTO's officials
at that conference. And it's not the only time that this has been
remarked. You have said something similar to a Belgian newspaper last
November. Art 52 seems to give you a legitimate basis for refusing to
grant software and business method patents. In case you do consider
this option, what obstacle is there to overcome?  Who will be the
resisting forces, who might be the supporting ones?

#bIe: The current EPO practice on software appears to be based on a series
of decisions taken by very few people at the Technical Boards of
Appeal, from Vicom (1986) to IBM I & II (1998) and Pension Benefit
Systems (2000). Did the TBA arrive at these decisions independently or
do they rflect an underlying decision by the EPO (President or the
Administrative Council)?

#oat: Some say that not the TBA but only the Enlarged Board of Appeal (EBA)
could possibly have been legitimated to take decisions on fundamental
legal matters. To what degree are these decisions binding for the EPO
today?

#dWl: Can the EPO not decide to adopt a different administrative practise,
based on the political will that has been expressed by this
Parliament?

#kBt: What about taking the matter to the Enlarged Board of Appeal and
asking them to overrule the TBA?

#lvu: CEC has suggested establishing a quality control division in EPO. Do
you believe that such a quality control division would be a useful
addition to the EPO structure?

#vef: Do you believe there are systematic problems of quality in any areas
of the EPO's work?

#epe: Are there any areas of patentability which should be particularly
investigated to determine whether the current

#Wne: Could you give an order-of-magnitude estimate of the number of

#nWe: Would you be the right person to choose the members of such a
division, or would it be better for them to be independently nominated
by the legislatures of the EPC member states (including the EU)?

#lpW: What is the real reason mathematical formula aren't patentable and why
doesn't that apply to computer programs?

#fWC: It has been argued that proposed Art. 3a (which says data processing
is not a field of technology) would preclude patenting computers or
data processing devices. Yet, music is not a field of technology but
musical instruments (and CD-players &c) can be patented.  What is the
difference?

#eWW: How exactly do you determine whether something 'advances the state of
the Art' or 'makes a technical contribution'? Or, when do you ever
decide it doesn't?

#Wnn: What is EPO's position on the effect of competition in encouraging
innovation, and in particular when (if ever) patents might actually
hinder innovation by stifling competition?

#nle: Does EPO see any problems in the potential conflict between patents
and citizens' rights of fair use?

#tnc: Assuming the office had plenty of resources to do examination in a
perfect way, how much income in fees would it loose with each rejected
patent as compared to what would it get if granted? Is additional
expenditure on examination sustainable in case some applicant drowns
the office in invalid patent applications and appeals?  What mechanism
is there to respond to such a kind of denial of service attack by
applicants, besides degrading examination standards and hoping for
judges to invalidate granted patents?

#ere: How do you reconcile your behaviour in granting such patents as, e.g.,
the Amazon gift-order patent with the examination guidelines of 1978
and with article 52 of the European Patent Convention?

#WWn: Would it really be acceptable to grant a patent to the use of
non-English languages in Internet addresses?

#eWt: How would the directive in various forms affect the sample
%(q:inventions) in %(URL)?

#aro: What is currently the average time for an ICT-patent to be granted in
the EPO? And how long does it take to receive the preliminary research
report of the second phase of the PCT process? Do you think these
timelines are reasonable and what are you planning to do to change
this?

#ntp: Is it true, what companies say, that at the moment it is possible to
patent business models if you are able to %(q:top) your application
technically enough?

#nWw: The Competitiveness Council %(cc:meets) on November 27th -- the same
day that Kober is in Brussels, to speak about the amended software
patent directive proposal.

#nei: Note that Kober will be replaced by Alain Pompidou on 1st July 2004. 
Alain Pompidou - though originally a skeptic on gene sequence
patentability - became famous for his manoeuvering of the Parliament
to vote the infamous article 5 of directive 98/44. Be prepared for
advanced casuistics at the time. However, he might want to show that
he is giving a new direction.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: kober031106 ;
# txtlang: en ;
# multlin: t ;
# End: ;

