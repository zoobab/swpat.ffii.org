<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Swedish Government Fighting for Software Patents

#descr: In a response to a parliamentary question in december 2003, Sweden's
minister of justice Thomas Bodström endorsed the approach of the
European Patent Office and the European Commission on software patents
and criticised the European Parliament for amending the directive with
the effect of narrowing the scope of patentability and making
already-granted patents invalid. Bodström announced that his
government will push for reversal of these amendments. His comments
provoked applause from the %(tp|patent movement|also called the
%(q:technostructure) in Sweden) and criticism from some software
associations and companies.

#tte: Introductory Remarks

#isa: The minister's answer with translation

#hli: The liberal MP Nyamko Sabuni submitted parliamentary questions to the
government about its policy on the %(ec:proposed software patent
directive) and the %(ea:European Parliament's amendments).  Sabuni
does not seem to be in favour of software patents.  From justice
minister Thomas Bodström's response of 2003/12/16 (see text below) it
appears that the Swedish government is blindly following the patent
establishment, which again is blindly following the European Patent
Office in order to evade the basic policy questions.

#tni: The minister's statement contains several layers of misinformation.

#Wat: The minister says that the Council took a first common position before
the EP vote of 2003-09-24.  That's false. The council common position
can't come before the Parliament's first reading. The current state is
Council first reading, not council second reading. The state this
minister reports (second reading in council without a second reading
in Parliament) might be illegal in EU law see EU treaties art 251. 
What the council did in november 2002 is no formal part of the
codecision procedure and it's just a marketing/pressure move by patent
offices, presumably with consent of their ministers.  They talked, and
issued a %(dk:text), but it was not their turn to speak, so it has no
formal weight. It's just an early informal agreement by governmental
patent experts.  It may truly represent their governments' current
intention, but it's not a completed first reading.

#Pbc: Bodström confuses current EPO caselaw with the current law.  The EPO
caselaw is widely criticised for not being in line with the law, and
even according to the advocates of the directive the chief reason for
having a directive is to remove doubts about the legality of the EPO's
practise.

#cCt: There is no significant difference between current EPO caselaw and the
practise of the US Patent Office which Bodström claims he does not
want in Europe.  Nor is there anything in the Council's inofficial
%(q:position/alignement) that could prevent such a practise. 
Evidently it is the intent of the minister or the people standing
behind him to allow unlimited patentability in Europe.

#Wvo: The minister boldly states that software patents as granted by the EPO
are conducive to innovation and a lesser scope of patentability will
mean less innovation.  This statement is unsubstantiated and at odds
with all scientific evidence as well as the common sense of the
developpers skilled in the concerned fields.

#Wah: The swedish translation of c.i.i. is %, not %(q:datorimplementerade
uppfinningar) - probably a move to make sure nobody understands that
c.i.i. is what it says it is

#adW: The minister says %(dr:computer-related invention) and means pure
software patents, as granted by the EPO.  The EPO's newspeak term
%(ki:computer-implemented invention) was apparently not yet
euphemistic enough for Mr. Bodström.  By renaming it to
%(q:computer-related inventions), Bodström makes it even less likely
that his audience can understand what he is talking about.

#6Wa: Answer to question 2003/04:393 regarding computer realated patents

#ieB: Minister of Justice Thomas Bodström

#EaW: Nyamko Sabuni has asked me about the ongoing EU-negotiations about a
directive on computer related inventions. The question is partly about
which actions I intend to take to clarify the legal situation in the
area, partly about preventing that the consequences of the directive
will render already granted patents void/inneffective in infringement
situations.

#Wdd: The Directive is enacted by the Council and the Parliamnet through
codecision procedure. Both the Council and the Parliament has
finalized their first processing of the legal document. The Council
took a common position november 14 2002 and the Parliament accepted
their statement at the first reading september 24 this year. The
Council has just now started its second processing [behandling].

#imn: The Commission proposal and the general alignement/position of the
Council aims to make clear the legal situation through uniform rules
within the union. The proposal builds on the practice that is applied
by the EPO. This is an application of law that already is
embraced/serving as guidance in Sweden and in other states that are
membes of the EPC.

#Wid: Furtermore, it is the ambition/will of the Commission and the Council
to establish a boundary against the legal situation in the US, where
they grant patents on computer related business methods. That is a
developement that the Commission and the Council does not want in
Europe. The establishing of a boundary is realised by the requirement
that all computer related inventions must involve a technical
contribution, that is a technical new way of thinking [technical
newthinking], to be patent protectable.

#dEt: Sweden, as also a broad majority of EU member states, stand behind the
general alignement/position of the Council. Sweden thinks that the
general alignement/position involve a well balanced solution that
makes it possible for the business world to further future protection
of their innovations, at the same time as the scope of the patet
protection does not become unreasonably broad in relation to third
parties.

#drn: The Directive is important since it creates clearer rules for when
patents on computer related inventions can be granted/given, at the
same time as it clearly markes/indicates that the EU will not go in
the direction towards the american legal developement.

#tWo: The Parliament has in its statement proposed a large number of changes
to the Commission proposal. Many of them are such that they
substantially would limit the possabilities of today to get and
enforce patent protection for computer related inventions, and thereby
have serious consequences for employment and economic growth. The
proposals [of the Parliament] in those parts, goes against the general
alignement/position of the Council.

#nrs: What now is about to happen in the negotiations, is that the Council
shall review and take a position on the Parliament statement. The
Council will then decide on a common position, which will be given to
the Parliament at their second reading.

#fnt: In the future work [arbetet], I will forcefully work for [verka för]
that the directive gets a balanced solution in line with the general
alignement/position of the Council. Such a wording means that the
legal situation in the field is clarified and that the business world
is given satisfactory opportunities of protection of their innovations
in the field.

#dmn: Bodström's answer

#Wcr: A patent litigation service company hyping Bodström's answer:

#eln: Sweden refuses to accept the EU Parliament's proposal for drastically
diminished patent protection for computer-related inventions. That is
the clear message given by the country's Minister for Justice, Thomas
Bodström, in his reply to a question from the Swedish parliament.

#trW: Awapatent is a patent bureau that last year built an extra floor on
top of their already mighty building here in Malmö.

#WWA: Swedish Private Equity & Venture Capital Association

#otW: Awapatent is an Associate Corporate Member.  NUTEK and other state
agencies for %(q:innovation and technology transfer) are also members.

#nwt: NUTEK (co)financed e.g. the startup Hapax.com, owner of many text
processing patents (granted by SePTO).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: sver040116 ;
# txtlang: en ;
# multlin: t ;
# End: ;

