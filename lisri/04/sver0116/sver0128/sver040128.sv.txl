<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Public letter of 5 Swedish CEOs against European Parliament's Amendments

#descr: CEOs of 5 of the largest patenting companies in Sweden falsely accuse the European Parliament of having voted to make software-controlled %(tp|industrial production processes|e.g. robots of ABB) unpatentable.  Based on this false accusation, they demand support for the position of the European Commission and European Council, which is to legalise 30000 patents on pure software and business methods which have been granted by the European Patent Office in recent years against the letter and spirit of the existing law.  The CEOs moreover make it clear that %(q:harmonisation and clarification): they want either a full codification of unlimited patentability according to the EPO doctrines or no directive at all.  Similar letters have been signed by prominent industry executives and sent to heads of government.  This is the first time that they are directly going public with such a letter.  The letter comes after a statement by Sweden's Justice Minister Thomas Bodström in support of unlimited patentability as practised by the European Patent Office.

#aWi: Urholkat patentskydd hotar svensk industri

#EWp: Europaparlamentet vill ändra reglerna om patent. Om förslagen går igenom försämras patentskydden kraftigt för innovationer som utnyttjar datorprogram. Förslagen drabbar inte bara rena mjukvaruföretag utan också traditionell verkstadsindustri, konsumentvaruföretag och telekomsektorn. Det skriver fem toppdirektörer och uppmanar svenska politiker att sätta sig in i förslagen.

#ite: Signatories:

#ieV: vice VD

#een: Group Vice President Head of Research and Development Scania AB

#rae: Lars-Göran Rosengren

#Veg: VD Volvo Technology AB

#Wsm: Tekniska innovationer är en av grundpelarna för det svenska välståndet. Incitament att investera i sådana innovationer har skapats genom ett effektivt och balanserat patentskydd. Vi som representerar svenska företag som byggs på sådana innovationer är djupt oroade över våra företags framtid om EU-parlamentets försök att rasera detta skydd skulle bli verklighet.

#ans: Inom EU pågår ett arbete för att harmonisera reglerna om patent på datorrelaterade uppfinningar. Under hösten 2003 har EU-parlamentet behandlat Europeiska kommissionens förslag och begärt omfattande ändringar.

#ram: Om dessa ändringar går igenom försämras kraftigt möjligheterna att patentskydda innovationer som utnyttjar datorprogram. Också redan beviljade patent på datorrelaterade uppfinningar som innehas av svenska företag kan ifrågasättas.

#awf: Detta drabbar inte bara rena mjukvaruföretag utan till exempel också traditionell verkstadsindustri, konsumentvaruföretag och telekomsektorn.

#Wkg: De innovativa produkter som gör svensk industri konkurrenskraftig är i dag en kombination av traditionell ingenjörskonst, modern design och avancerad datorteknologi.

#nrW: Datorstyrda tvättmaskiner, spisar och mikrovågsugnar är några exempel på produkter som svenska företag framgångsrikt utvecklar och säljer över hela världen.

#dts: Än mer avancerade är de system i bilar och lastbilar där datorprogram styr elektroniken i motorn, styrningen, bromsarna, säkerhetsfunktioner med mera.

#etv: Industrirobotar, kraftöverföringssystem och processer inom tillverkningsindustrin styrs med hjälp av datorprogram. Ännu ett exempel är de avancerade system som styr och definierar funktionerna och tjänsterna i våra telekommunikationsprodukter, allt från mobiltelefoner till växlar och andra system som ser till att kommunikationen fungerar.

#oee: Gemensamt för alla dessa områden är att elektroniken och de funktioner som kontrolleras av datorprogram i allt högre grad blivit en central del av produktens unika egenskaper.

#top: Utan ett starkt patentskydd för sådana datorrelaterade uppfinningar också i framtiden riskerar innovationer gjorda i Sverige att kopieras och säljas på den europeiska marknaden av andra än de företag och uppfinnare som investerat tid, pengar och tankemöda för att ta fram dessa innovationer.

#nte: En sådan utveckling skulle ha en förödande effekt på investeringsklimatet i Sverige och på tillväxten i de svenska företagen.

#Ueg: Vad är då bakgrunden till vår oro? Jo, inom EU pågår ett arbete med att harmonisera patentskyddet för datorrelaterade uppfinningar.  Europeiska patentverket (EPO) har redan beviljat mer än 30.000 patent avseende sådana uppfinningar - patent som ligger till grund för företagens affärsupplägg och licensieringsstrategier.

#Wta: Tvärt emot vad som ofta påstås i debatten avsåg det ursprungliga förslaget från kommissionen inte att utvidga patenteringsmöjligheterna till att - såsom i USA - tillåta patent på till exempel rena affärsmetoder.

#oio: Tvärtom går kommissionens förslag ut på att undvika en sådan utveckling genom att fastslå att nuvarande praxis i Europa skall gälla även framöver.

#sas: Industrin i Sverige och i resten av EU har med vissa invändningar välkomnat kommissionens förslag som ett steg i rätt riktning. Även EU:s medlemsländer har i ministerrådet anslutit sig till denna linje.

#erW: Men denna samsyn har radikalt förändrats genom EU-parlamentets beslut. Så gott som samtliga av EU-parlamentets förslag är helt oacceptabla för industrin. Förslagen skulle omöjliggöra patentskydd för en rad datorrelaterade uppfinningar som i dag kan skyddas och/eller göra sådana patent meningslösa.

#oas: Inom vissa sektorer såsom telekomindustrin skulle patentskydd omöjliggöras för så gott som alla innovationer.

#dWd: Detta är tyvärr bara det senaste i en rad beslut och politiska utlåtanden som markerar en olycklig utveckling i den allmänna politiska debatten kring modern innovation och dess patentskydd.  Beslutet visar med all tydlighet att det råder okunskap och missförstånd om ämnet hos många.

#rWr: Frågans komplexitet tycks ha lett till förenkling och polarisering där så kallade mjukvarupatent generellt utmålats som något farligt och konkurrensfientligt. Till denna utveckling har bidragit att debatten dominerats av några få högröstade aktörer som är emot allt vad patentskydd heter.

#pta: För oss och våra företag är det avgörande att svenska politiker tar frågan på allvar och arbetar med all kraft för att bibehålla och förbättra den nuvarande lagstiftningen om patentskydd för datorimplementerade uppfinningar. Detta för att säkerställa att den svenska industrins innovationer och investeringar även i fortsättningen omfattas av ett starkt och tydligt patentskydd.

#nWn: Finns inte detta incitament riskerar vi att förlora Sveriges styrka som en kunskapsnation, driven av innovativa entreprenörer.

#rea: Vi riktar därför följande uppmaningar till svenska politiker: l Sätt er in i vad kommissionens förslag respektive EU-parlamentets motförslag innebär och hur de skulle påverka svensk industri. Vi träffar er gärna för att ge en nyanserad bild av vad %(q:mjukvarupatent) innebär för våra företag! l Arbeta emot att frågan forceras fram inom EU med en kompromisslösning som följd. Vi klarar oss bättre utan något direktiv alls än med ett dåligt sådant! l Kontakta era kolleger i de andra europeiska länderna och se till att de förstår situationens allvar för europeisk industri och i slutändan för Europa! l Uppmana era partiers representanter att driva frågan i EU-parlamentet som den kritiska näringslivsfråga för Europa den är!

#sWe: 5 Swedish CEOs 2004-01-26 FUD letter in Dagens Industri

#gla: CEOs of 5 of the largest patenting companies in Sweden falsely accuse the European Parliament of having voted to make software-controlled %(tp|industrial production processes|e.g. robots of ABB) unpatentable.  Based on this false accusation, they demand support for the position of the European Commission and European Council, which is to legalise 30000 patents on pure software and business methods which have been granted by the European Patent Office in recent years against the letter and spirit of the existing law.  The CEOs moreover make it clear that %(q:harmonisation and clarification): they want either a full codification of unlimited patentability according to the EPO doctrines or no directive at all.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri04.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: sver040128 ;
# txtlang: sv ;
# multlin: t ;
# End: ;

