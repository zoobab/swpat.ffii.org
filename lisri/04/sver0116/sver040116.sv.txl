<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Svenska regeringen kämpar för mjukvarupatent

#descr: I ett svar på en fråga från riksdagen i december 2003 stödjer
justitieminister Thomas Bodström Europeiska Patentverket och
Europeiska kommissionen i frågan om mjukvarupatent, och kritiserade
Europaparlementet för att ha ändrat direktivet så att det patenterbara
området förminskas och flera redan beviljade patent blir
ogiltiggjorda. Bodström meddelade att hans regering kommer att arbeta
för att återställa dessa ändringar. Hans kommentarer har väckt
applåderader hos %(tp|patentrörelsen|även kallad %(q:teknostrukturen)
i Sverige) och kritik hos mjukvaruföretag och föreningar.

#tte: Introductory Remarks

#isa: The Minister's Statement

#hli: The liberal MP Nyamko Sabuni submitted parliamentary questions to the
government about its policy on the %(ec:proposed software patent
directive) and the %(ea:European Parliament's amendments).  Sabuni
does not seem to be in favour of software patents.  From justice
minister Thomas Bodström's response of 2003/12/16 (see text below) it
appears that the Swedish government is blindly following the patent
establishment, which again is blindly following the European Patent
Office in order to evade the basic policy questions.

#tni: The minister's statement contains several layers of misinformation.

#Wat: The minister says that the Council took a first common position before
the EP vote of 2003-09-24.  That's false. The council common position
can't come before the Parliament's first reading. The current state is
Council first reading, not council second reading. The state this
minister reports (second reading in council without a second reading
in Parliament) might be illegal in EU law see EU treaties art 251. 
What the council did in november 2002 is no formal part of the
codecision procedure and it's just a marketing/pressure move by patent
offices, presumably with consent of their ministers.  They talked, and
issued a %(dk:text), but it was not their turn to speak, so it has no
formal weight. It's just an early informal agreement by governmental
patent experts.  It may truly represent their governments' current
intention, but it's not a completed first reading.

#Pbc: Bodström confuses current EPO caselaw with the current law.  The EPO
caselaw is widely criticised for not being in line with the law, and
even according to the advocates of the directive the chief reason for
having a directive is to remove doubts about the legality of the EPO's
practise.

#cCt: There is no significant difference between current EPO caselaw and the
practise of the US Patent Office which Bodström claims he does not
want in Europe.  Nor is there anything in the Council's inofficial
%(q:position/alignement) that could prevent such a practise. 
Evidently it is the intent of the minister or the people standing
behind him to allow unlimited patentability in Europe.

#Wvo: The minister boldly states that software patents as granted by the EPO
are conducive to innovation and a lesser scope of patentability will
mean less innovation.  This statement is unsubstantiated and at odds
with all scientific evidence as well as the common sense of the
developpers skilled in the concerned fields.

#Wah: The swedish translation of c.i.i. is %, not %(q:datorimplementerade
uppfinningar) - probably a move to make sure nobody understands that
c.i.i. is what it says it is

#adW: The minister is further confusing the discussion by talking about
%(dr:computer-related invention) and meaning not machinery related to
computers but software, as can be concluded from his endorsement of
EPO practise and from the fact that %(q:computer-related invention) is
used as an equivalent of the EPO's already misleading term
%(ki:computer-implemented invention).

#6Wa: %(nl|den 16 december|Svar pe frega 2003/04:393 om datorrelaterade
patent)

#ieB: Justitieminister Thomas Bodström

#EaW: Nyamko Sabuni har frågat mig om de pågående EU-förhandlingarna om ett
direktiv om patent på datorrelaterade uppfinningar. Frågan gäller
vilka åtgärder jag avser att vidta för att dels klargöra rättsläget på
området, dels förhindra att direktivet ska få till följd att en del
redan beviljade patent blir verkningslösa vid intrångssituationer.

#Wdd: Direktivet antas av rådet och Europaparlamentet genom
medbeslutandeförfarande. Både rådet och parlamentet har avslutat sin
första behandling av rättsakten. Rådet antog en gemensam inriktning
den 14 november 2002 och parlamentet antog sitt yttrande vid första
läsningen den 24 september i år. Rådet har just inlett sin andra
behandling.

#imn: Kommissionens förslag och rådets allmänna inriktning syftar till att
klargöra rättsläget genom enhetliga regler inom gemenskapen. Förslaget
bygger på den praxis som tillämpas av det europeiska patentverket.
Detta är en rättstillämpning som redan är vägledande i Sverige och i
andra stater som är anslutna till den europeiska patentkonventionen.

#Wid: Dessutom vill kommissionen och rådet dra en gräns mot rättsläget i
USA, där man beviljar patent på datorrelaterade affärsmetoder. Det är
en utveckling som kommissionen och rådet inte vill ha i Europa.
Gränsdragningen sker genom kravet att alla datorrelaterade
uppfinningar måste innebära ett tekniskt bidrag, det vill säga ett
tekniskt nytänkande, för att kunna patentskyddas.

#dEt: Sverige, liksom en mycket bred majoritet av EU:s medlemsstater, står
bakom rådets allmänna inriktning. Sverige anser att den allmänna
inriktningen innebär en väl avvägd lösning som möjliggör för
näringslivet att även fortsättningsvis skydda sina innovationer på
området samtidigt som patentskyddet inte blir oskäligt omfattande i
förhållande till tredje man.

#drn: Direktivet är viktigt eftersom det skapar tydligare regler för när
patent på datorrelaterade uppfinningar kan meddelas samtidigt som det
klart markerar att EU inte ska gå i riktning mot den amerikanska
rättsutvecklingen.

#tWo: Parlamentet har i sitt yttrande föreslagit ett stort antal ändringar
av kommissionens förslag. Flera av ändringarna är sådana att de
väsentligt skulle inskränka dagens möjligheter att erhålla och
upprätthålla patentskydd för datorrelaterade uppfinningar och därmed
få allvarliga konsekvenser för sysselsättning och tillväxt. Förslagen
i dessa delar går emot rådets allmänna inriktning.

#nrs: Det som nu ska ske i förhandlingarna är att rådet ska granska och ta
ställning till parlamentets yttrande. Rådet ska sedan besluta om en
gemensam ståndpunkt, vilken lämnas till parlamentet för dess andra
läsning.

#fnt: I det fortsatta arbetet kommer jag att kraftfullt verka för att
direktivet får en balanserad lösning i linje med rådets allmänna
inriktning. En sådan utformning innebär att rättsläget på området
klargörs och att näringslivet ges fullgoda möjligheter att även
fortsättningsvis skydda sina innovationer på området.

#dmn: Bodström's answer

#Wcr: A patent litigation service company hyping Bodström's answer:

#eln: Sweden refuses to accept the EU Parliament's proposal for drastically
diminished patent protection for computer-related inventions. That is
the clear message given by the country's Minister for Justice, Thomas
Bodström, in his reply to a question from the Swedish parliament.

#trW: Awapatent is a patent bureau that last year built an extra floor on
top of their already mighty building here in Malmö.

#WWA: Swedish Private Equity & Venture Capital Association

#otW: Awapatent is an Associate Corporate Member.  NUTEK and other state
agencies for %(q:innovation and technology transfer) are also members.

#nwt: NUTEK (co)financed e.g. the startup Hapax.com, owner of many text
processing patents (granted by SePTO).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: sver040116 ;
# txtlang: sv ;
# multlin: t ;
# End: ;

