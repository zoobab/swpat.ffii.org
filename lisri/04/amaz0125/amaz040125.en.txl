<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII Opposition against Amazon

#descr: FFII has filed an opposition at the %(tp|European Patent Office|EPO)
against the Gift-Ordering Patent of Amazon.  The FFII is asking the
Technical Board of Appeal to revoke the patent and to return a more
straightforward understanding of the European Patent Convention, which
alone could securely prevent the granting of masses of broad and
trivial monopolies on rules of organisation and calculation.  This
demand of the FFII is backed by a petition which has been signed by
numerous politicians, entrepreneurs and organisations.

#agu: Backgrounds

#media: Media contacts

#ffii: About the FFII

#dokurl: Permanent URL of this Press Release

#aWW: The %(ep:gift ordering patent) was granted by the European Patent
Office in Summer 2003. This patent was created by dividing the
European version of %(a1:Amazon-One-Click) into two patent
applications.  The Amazon-1-Click patent had become famous in 1998,
when Amazon used it to obtain an injunction against its competitor
Barnes & Nobles during the chrismats season.  By a FFII press release,
the public took notice of the EPO's decision to grant a successor of
the 1-Click patent in August 2003. Until then, the proponents of the
EU Commission's software patents directive had regularly claimed that
patents like %(q:Amazon-1-Click) would be impossible in Europe. 
Besides the FFII, two more parties have filed opposition: the %(GI)
and the flower delivery service Fleurop.

#KrW: FFII's legal representative is Olaf Koglin of Lenhardt Rechtsanwälte.

#erj: André Rebentisch, press speaker of FFII, explains:

#WiW: The patent system produces broad monopolies in combination with
expensive administrative and judicial procedures.  It has thereby
created a paradise for patent professionals, who again promote the
system by claiming that it benefits the economy as a whole.  This
claim has always been doubted and often negated by those economists
who have endeavoured to subject it to scrutiny.  When granting
patents, at least special caution is needed.  Increasing the number of
patents by %(sk:extending the invention concept), as practised by the
European Patent Office since the mid eighties, leads to a flood of
harmful low-quality patents.  Very many of the patents which the EPO
has recently been granting are of similar breadth and triviality as
the gift ordering patent of Amazon.  Nowadays it is difficult to
operate a %(ws:webshop) without infringing European patents.  In the
USA, the Federal Trade Commission has, after intensive hearings,
%(ft:concluded) that software patents are damaging to innovation and
competition.  Claims to the contrary can be safely assumed to be
empirically falsified.  This is also the view of the European
Parliament, which %(ep:amended) the European Commission's software
patent directive proposal last September.  But patent professionals
are %(er:pushing) governments and sectoral bodies to block the
Parliament's decision.  The European Patent Office is also persisting
on its unfortunate path.

#tzW: Hartmut Pilch, president of FFII, explains:

#nWn: In the past, e.g. in the %(PB) decision, the European Patent Office
has abused opposition procedings against trivial business method
patents as an opportunity to extend the limits of patentability while
at the same time killing the trivial patent in question. Especially a
patent such as Amazon Gift Ordering is susceptible to such manoeuvres.
The opposition of GI seems to point into that direction.

#ntr: By our opposition we remind the board of the context of its action. A
new call %(ca:Appeal) signed by politicians, entrepreneurs and
organisations urges the European Patent Office to immediately stop the
illegal granting of software and business method patents. When the
president of the European Patent Office, Dr. Ingo Kober, was asked
about this during a hearing in the European Parliament, Kober referred
in his published %(ka:written answer) to the decisions of the
technical board of appeals, which were not bound to his orders.  Also
national governments like to duck behind the Technical Boards of
Appeals of the %(tp|European Patent Office|EPO).  It is time that we
directly contact this de-facto patent legislator.  The opposition
procedure offers a good opportunity.

#eee: In our opposition we only referred to the valid law in the consistent
interpretation given by case law and examination guidelines of the 80s
or the European Parliament Vote of 2003. In contrast to the Pension
Benefits doctrine the straightforward interpretation of the law gives
a secure basis for withdrawing the contested patent as well as many
other monopolies on algorithms and business methods framed in terms of
general-purpose computing equipment.

#saf: This FFII Press Release of september 2003 drew attention to the EPO's
granting of the Amazon patent.

#iCf: The signators explicitely ask the EPO to follow the European Patent
Convention and the European Parliament instead of the TBA's
illegitimate caselaw.

#hni: The TBA used this case as an opportunity to extend patentability and
at the same time spectacularly revoke one patent.  The Pension
Benefits doctrine was the basis of the European Commission's Directive
Proposal, and it was completely negated by the amendments of the
European Parliament.

#oWW: More Contacts to be supplied upon request

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: blasum ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amaz040125 ;
# txtlang: en ;
# multlin: t ;
# End: ;

