<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII Opposition against Amazon

#descr: FFII has filed an opposition at the %(tp|European Patent Office|EPO)
against the Gift-Ordering Patent of Amazon.  The FFII is asking the
Technical Board of Appeal to revoke the patent and to return a more
straightforward understanding of the European Patent Convention, which
alone could securely prevent the granting of masses of broad and
trivial monopolies on rules of organisation and calculation.  This
demand of the FFII is backed by a petition which has been signed by
numerous politicians, entrepreneurs and organisations.

#agu: Hintergründe

#media: Medienkontakte

#ffii: Über den FFII

#dokurl: Permanente Netzadresse dieser Presseerklärung

#aWW: Das %(ep:Patent auf die Bestellung von Geschenken) erteilte das
Europäische Patentamt im Sommer 2003.  Dieses Patent entstand durch
eine Aufteilung der europäischen Version des %(a1:1-Klick-Patentes),
mit dem Amazon 1998 im Weihnachtsgeschäft eine einstweilige Verfügung
gegen seinen Konkurrenten Barnes & Nobles erwirkt hatte. Im August
2003 wurde die Öffentlichkeit durch eine %(pf:Presseerklärung des
FFII) auf die europäische Erteilung des 1-Klick-Nachfolgepatentes
aufmerksam.  Die Verfechter der EU-Softwarepatentrichtlinie hatten bis
dahin immer wieder versichert, Patente vom Schlage eines
%(q:Amazon-1-Click) könne es in Europa nicht geben.  Außer dem FFII
haben auch die %(GI) und der Blumenversender Fleurop Einspruch gegen
das breite Trivialpatent eingelegt.

#KrW: Die anwaltliche Vertretung des FFII geschieht durch Olaf Koglin von
Lenhardt Rechtsanwälte.

#erj: André Rebentisch, Pressesprecher des FFII, erklärt:

#WiW: Das Patentwesen produziert breite Monopole in Kombination mit
kostspieligen Verwaltungs- und Gerichtsverfahren.  Damit hat es ein
Paradies für Patentjuristen und andere interessierte Gruppen
geschaffen, die das System propagieren.  Die Behauptung, das
Patentwesen diene der Volkswirtschaft insgesamt, ist indes von der
einschlägigen Wissenschaft immer bezweifelt und vielfach verneint
worden.  Bei der Erteilung von Patenten wäre daher zumindest besondere
Vorsicht angebracht.  Eine Erhöhung der Patentzahl durch
%(sk:Aufweichung des Erfindungsbegriffs), wie sie am Europäischen
Patentamt seit Mitte der 80er Jahre statt findet, geht auf Kosten der
Qualität und Wirtschaftlichkeit der erteilten Patente.   Sehr viele
der vom EPA in den letzten Jahren erteilten Software-Patente sind
ähnlich breit und trivial wie das Geschenkbestellungs-Patent von
Amazon.  Man kann heute kaum noch einen %(ws:Webshop) betreiben, ohne
europäische Patente zu verletzen.  In den USA haben Wettbewerbshüter
inzwischen nach ausführlichen Anhörungen über Softwarepatente deren
volkswirtschaftliche Wirkungen %(ft:sehr negativ beurteilt) und
Reformen angemahnt.  Die Schädlichkeit von Softwarepatenten kann dank
%(ss:zahlreicher Studien) als empirisch erwiesen gelten.  So sieht es
auch das Europäische Parlament, das im September eine Richtlinie der
EU-Kommission zur Legalisierung von Softwarepatenten %(ep:in ihr
Gegenteil umgewandelt) hat.  Doch die profitierenden Berufsgruppen
haben nach wie vor die meisten Verbände und Regierungen fest im Griff
und %(er:arbeiten) daran, das Parlamentsvotum über den EU-Ministerrat
auszuhebeln.  Auch das Europäische Patentamt verharrt bislang auf
seinem Irrweg.

#tzW: Hartmut Pilch, Vorsitzender des FFII erklärt:

#nWn: In der Vergangenheit, etwa bei der Entscheidung %(PB), hat das
Europäische Patentamt Verfahren gegen triviale
Geschäftsmethodenpatente genutzt, um die Grenzen der Patentierbarkeit
weiter auszudehnen und gleichzeitig ein einzelnes Patent
publikumswirksam zu Fall zu bringen.  Gerade ein Patent wie
Amazon-Geschenkbestellung eignet sich hervorragend für solche Manöver.
 Auch jetzt scheint die Einspruchsschrift der GI in diese Richtung zu
zielen.

#ntr: Mit unserem Einspruch erinnern wir die Kammer aber an den Kontext
ihres Handelns.  Ein neuer, von einigen Parlamentariern
unterzeichneter %(ca:Appell) ruft das Europäische Patentamt auf, die
gesetzeswidrige Erteilung von Software- und Geschäftsmethodenpatenten
unverzüglich einzustellen.  Der Präsident des Europäischen
Patentamtes, Dr. Ingo Kober, wurde bei einer Gesprächsstunde im
Europäischen Parlament hierauf angesprochen.  Kober verwies in seiner
nachgereichten %(ka:schriftlichen Antwort) auf die Rechtsprechung
seiner Technischen Beschwerdekammern, die nicht an seine Weisungen
gebunden seien.  Auch nationale Regierungen verstecken sich gerne
hinter den Technischen Beschwerdekammern des EPA.  Es ist daher an der
Zeit, dass wir zu diesem De-Facto-Gesetzgeber Europas direkten Kontakt
aufnehmen.  Das Einspruchsverfahren bietet hierzu eine gute
Gelegenheit.

#eee: Wir haben uns in unserer Einspruchsschrift nur auf das geltende Gesetz
in der gradlinigen Auslegung berufen, wie man sie vor allem in der
Spruchpraxis der 1980er Jahre aber auch in manchen neueren
Entscheidungen nationaler Gerichte und im Votum des Europäischen
Parlaments vom 24. September 2003 findet.  Die gradlinige
Gesetzesauslegung bietet im Gegensatz zur Pension-Benefits-Doktrin
eine sichere Basis zur Widerrufung des Streitpatentes und vieler
ähnlicher Monopole auf Organisations- und Rechenregeln.

#saf: Diese FFII-Presseerklärung von 2003 machte die Welt auf die Erteilung
des Amazon-Patentes durch das EPA aufmerksam.

#iCf: Die Unterzeichner fordern das EPA ausdrücklich auf, sich von dem
Fallrecht der Technischen Beschwerdekammern zu lösen und stattdessen
das Europäische Patentübereinkommmen nach den üblichen Methoden der
Gesetzesauslegung anzuwenden.

#hni: The TBA used this case as an opportunity to extend patentability and
at the same time spectacularly revoke one patent, shortly before the
EPC revision conference in 2000.  The Pension Benefits doctrine was
the basis of the European Commission's Directive Proposal, and it was
completely negated by the amendments of the European Parliament.

#oWW: Weitere Kontakte auf Anfrage

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amaz040125 ;
# txtlang: de ;
# multlin: t ;
# End: ;

