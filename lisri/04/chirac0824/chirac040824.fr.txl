<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Réponse du conseiller technique de Jacques Chirac

#descr: Dans une lettre datée du 24 août 2004, le Conseiller Technique du
Président français répond à l'Appel urgent.

#sVi: Monsieur le Vice-Président,

#0Wt: Par lettre en date du 5 août 2004, vous avez bien voulu attirer
l'attention de Monsieur le Président de la République sur vos
préoccupations concernant le projet de directive européenne sur les
brevets logiciels.

#oWi: Très sensible à vos préoccupations, le Chef de l'Etat m'a chargé de
vous remercier de votre courrier et de vous répondre.

#ecl: Comme vous le savez, la France a soutenu le compromis irlandais mais a
assorti son vote d'une déclaration rappelant les objectifs qu'elle
entend poursuivre, à savoir un encadrement clair de la brevetabilité
des logiciels qui mette fin aux dérives de l'%(ep:OEB).

#ecW: Si le texte retenu s'avérait ne pas y parvenir, il conviendrait d'y
revenir avec l'aide du Parlement Européen.

#did: Je vous prie d'agréer, Monsieur le Vice-Président, l'expression de ma
considération distinguée.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/chirac040824.el ;
# mailto: mlhtimport@a2e.de ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: chirac040824 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

