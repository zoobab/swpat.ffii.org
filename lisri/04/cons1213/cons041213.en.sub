\begin{subdocument}{cons041213}{2004-12-13 EU Council Presidency Schedules Software Patent Directive for Adoption at Fishery Meeting}{http://swpat.ffii.org/lisri/04/cons1213/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{/Brussels, 14 December 2004 --/ Diplomats of the EU Council will decide on Tuesday and Wednesday whether the Council's Software Patent Agreement of May 2004 will be passed by the upcoming Fishery or Environment Council meetings, the last ones of this year. Contrary to recent information given by the Belgian government, the Dutch Presidency is apparently still trying to push through the text from last May as an A-item, i.e. without discussion and without vote. The published justifications for throwing away all of the European Parliament's substantial amendments range from the [longtime debunked (``TRIPs requires software patents``) http://swpat.ffii.org/analysis/trips/index.en.html] to the downright absurd (``politicians must not change established practice``).}
\begin{sect}{intro}{introduction}
A recently published Council agenda\footnote{\url{http://register.consilium.eu.int/pdf/en/04/st15/st15967.en04.pdf}} (page 2, item 5) notes that the adoption of the Council's text from May will be discussed by the Mertens group\footnote{\url{http://www.ukrep.be/jargon.html}} on Tuesday. This group prepares the meetings of Coreper, which in turn prepares the meetings of the Council. The only Council meetings left under the Dutch Presidency are one on Environment on 20 December and one on Agriculture and Fisheries on 21-22 December.

The Dutch Presidency has been using diplomatic pressure to bully Poland\footnote{\url{}}.  Although everyone knows that neither the Polish government nor the Polish industry supports the directive text, the Dutch Presidency insists that, due to some formal reasons\footnote{\url{}}, Poland must vote ``Yes'' or agree to a formal adoption without a vote. However, as has been pointed out before and as verified with the Council's own public information service, any country has the right to demand that the directive text should be treated again as a B item (i.e. as a discussion point).

Laura Creighton, software entrepreneur, venture capitalist and vice-president of FFII, comments:

\begin{quote}
{\it Before today it was possible for generous people to look charitably at this text as an example of a tragic mistake, not malice. But not with this last-minute maneuvering. Only the most committed opponent to the democratic process would believe that the proper response to the widespread consensus that there is something profoundly wrong with the Council's text, is to race it through with an A-item approval the week before Christmas in a \emph{Fisheries} Council Meeting. The bad smell coming from Brussels has nothing to do with the fish.}
\end{quote}

Othmar Karas, MEP of the Austrian People's Party (\"{O}VP) and Vice President of the European People's Party (EPP) in the European Parliament, recently lambasted the Council's behaviour as well in a recent press release\footnote{\url{}}:

\begin{quote}
{\it The planned directive on software patents no longer has a majority in the Council. The political agreement of May is outdated, both because of the new voting weights stipulated by the Nice treaty and because of the changes of position in Poland, the Netherlands and Germany.}
\end{quote}

A draft statement of reasons\footnote{\url{}} proposed by the Dutch Presidency has to justify this defiance of democratic decision making. It claims that a large number of the European Parliament's amendments ``did not reflect established practice'' or ``would be contrary to the international obligations under the TRIPs agreement\footnote{\url{http://localhost/swpat/stidi/trips/index.en.html}}''.

Hartmut Pilch, president of the FFII, notes:

\begin{quote}
{\it The Dutch presidency seems to be avoiding conflicts by preparing an explanatory document that explains as little as possible. They do not explain which kinds of patent claims should be acceptable and which not, which interests are served thereby and which not and why. They merely point to the European Patent Office as the authority. One may wonder whether this is the intention of a qualified majority. Also, the Dutch presidency adds the TRIPs fallacy. They could just as well have added a statement that the earth is flat.}
\end{quote}

Jonas Maebe of FFII Belgium adds:

\begin{quote}
{\it One group of amendments from the European Parliament is dismissed by the Council with the simple justification that they ``did not reflect established practice''. Let's hope for the Council's sake that the European Parliament does not ever again get the idea that they can submit proposals which actually change things.}
\end{quote}

It is not yet known whether Poland will bow to the pressure. Poland's diplomats at the Council have, ever since May 2004, contradicted their government's repeated statements of opposition to the directive draft. They have always insisted on unwritten rules of diplomacy whose violation, they fear, would be disadvantageous to Poland.
\end{sect}

\begin{sect}{koments}{Further Comments}
\begin{sect}{arebenti}{Andre Rebentisch\footnote{\url{}}}
\begin{quote}
{\it As we found out, it is no ius nondum to change your council voting|ConsRevers. In fact there is no formal obligation to stick to a ``political agreement'' that is obsolete. Adoption of obsolete agreements in fact contradicts the whole concept of {\bf political agreements} as an informal part of the decision making process. However the Dutch presidency insists on non-existing council rules. They are doing the exact opposite of what a 2/3 majority of their Parliament asked them to do\footnote{\url{}}}
\end{quote}
\end{sect}

\begin{sect}{jhalber}{Jozef Halbersztadt}
Jozef Halbersztadt works as a patent examiner at Polish Patent Office. The Polish Patent Office has resisted pressure from the European Patent Office to allow software patents and is one of the most fervent opponents of the Council's text, which it fears would plunge it into a quagmire of broad and trivial patents on abstract subject matter.  Halbersztadt has watched the EU developments for many years and draws the following analogy to the Ukrainian presidential elections:

\begin{quote}
{\it We could see our fight in terms of Ukrainian presidential election.  Yanukovych was picked as a candidate by the current president, Kuchma, in return for guarantees to the president and his allies of immunity from prosecution for all his crimes, committed while in power, as well as protection of their assets gained through manipulated privatisation.  The Ukrainian people were fed up and voted him down. And they were denied their victory by a group of crooks.}

{\it Our scenario is similar. We have won in the European Parliament.  We are now denied victory by manipulation and irregularities in the Council. The Kuchmas of the EU don't like consultation with national parliaments, recounting in the Council or restarting in the EP, nor any other open approach to problems. The main difference is that they --- hopefully --- aren't considering deployment of troops and use of force.}
\end{quote}
\end{sect}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf 2004-12-07 Belgium confirms abstention regarding Council text from May\footnote{\url{}} (and remarks that the common position will not be finalised under the Dutch presidency\footnote{\url{}})}}
\filbreak

\item
{\bf {\bf 2004-07-02 Motion by the Parliament of The Netherlands\footnote{\url{}}}}
\filbreak

\item
{\bf {\bf 2004-12-13 EU Heise: Software Patents Circumvent European Parliament\footnote{\url{http://www.heise.de/english/newsticker/news/54199}}}}
\filbreak

\item
{\bf {\bf 2004-12-13 EU Slashdot: Software Patents Circumvent European Parliament\footnote{\url{http://yro.slashdot.org/article.pl?sid=04/12/13/1248202&tid=155&tid=219}}}}
\filbreak

\item
{\bf {\bf 2004-12-13 EU ZDNet.co.uk: Patent directive faces fishy future\footnote{\url{http://news.zdnet.co.uk/software/0,39020381,39180705,00.htm}}}}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

