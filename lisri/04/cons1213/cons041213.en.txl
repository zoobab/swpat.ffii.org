<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2004-12-13 EU Council Presidency Schedules Software Patent Directive
for Adoption at Fishery Meeting

#descr: /Brussels, 14 December 2004 --/ Diplomats of the EU Council will
decide on Tuesday and Wednesday whether the Council's Software Patent
Agreement of May 2004 will be passed by the upcoming Fishery or
Environment Council meetings, the last ones of this year. Contrary to
recent information given by the Belgian government, the Dutch
Presidency is apparently still trying to push through the text from
last May as an A-item, i.e. without discussion and without vote. The
published justifications for throwing away all of the European
Parliament's substantial amendments range from the [longtime debunked
("TRIPs requires software patents") http://swpat.ffii.org/analysis/tri
ps/index.en.html] to the downright absurd ("politicians must not
change established practice").

#rWe: Further Comments

#ntE: A recently published %(ca:Council agenda) (page 2, item 5) notes that
the adoption of the Council's text from May will be discussed by the
%(mg:Mertens group) on Tuesday. This group prepares the meetings of
Coreper, which in turn prepares the meetings of the Council. The only
Council meetings left under the Dutch Presidency are one on
Environment on 20 December and one on Agriculture and Fisheries on
21-22 December.

#Amt: The Dutch Presidency has been using diplomatic pressure to %(bp:bully
Poland).  Although everyone knows that neither the Polish government
nor the Polish industry supports the directive text, the Dutch
Presidency insists that, due to some %(cr:formal reasons), Poland must
vote %(q:Yes) or agree to a formal adoption without a vote. However,
as has been pointed out before and as verified with the Council's own
public information service, any country has the right to demand that
the directive text should be treated again as a B item (i.e. as a
discussion point).

#orW: Laura Creighton, software entrepreneur, venture capitalist and
vice-president of FFII, comments:

#epi: Before today it was possible for generous people to look charitably at
this text as an example of a tragic mistake, not malice. But not with
this last-minute maneuvering. Only the most committed opponent to the
democratic process would believe that the proper response to the
widespread consensus that there is something profoundly wrong with the
Council's text, is to race it through with an A-item approval the week
before Christmas in a %(e:Fisheries) Council Meeting. The bad smell
coming from Brussels has nothing to do with the fish.

#afu: Othmar Karas, MEP of the Austrian People's Party (ÖVP) and Vice
President of the European People's Party (EPP) in the European
Parliament, recently lambasted the Council's behaviour as well in a
%(pr:recent press release):

#ths: The planned directive on software patents no longer has a majority in
the Council. The political agreement of May is outdated, both because
of the new voting weights stipulated by the Nice treaty and because of
the changes of position in Poland, the Netherlands and Germany.

#tWW2: A %(dr:draft statement of reasons) proposed by the Dutch Presidency
has to justify this defiance of democratic decision making. It claims
that a large number of the European Parliament's amendments %(q:did
not reflect established practice) or %(q:would be contrary to the
international obligations under the %(tr:TRIPs agreement)).

#tdI: Hartmut Pilch, president of the FFII, notes:

#twi: The Dutch presidency seems to be avoiding conflicts by preparing an
explanatory document that explains as little as possible. They do not
explain which kinds of patent claims should be acceptable and which
not, which interests are served thereby and which not and why. They
merely point to the European Patent Office as the authority. One may
wonder whether this is the intention of a qualified majority. Also,
the Dutch presidency adds the TRIPs fallacy. They could just as well
have added a statement that the earth is flat.

#sFm: Jonas Maebe of FFII Belgium adds:

#lde: One group of amendments from the European Parliament is dismissed by
the Council with the simple justification that they %(q:did not
reflect established practice). Let's hope for the Council's sake that
the European Parliament does not ever again get the idea that they can
submit proposals which actually change things.

#taW: It is not yet known whether Poland will bow to the pressure. Poland's
diplomats at the Council have, ever since May 2004, contradicted their
government's repeated statements of opposition to the directive draft.
They have always insisted on unwritten rules of diplomacy whose
violation, they fear, would be disadvantageous to Poland.

#4hW: As we found out, it is %(ni:no ius nondum to change your council
voting|ConsRevers). In fact there is no formal obligation to stick to
a %(q:political agreement) that is obsolete. Adoption of obsolete
agreements in fact contradicts the whole concept of %(s:political
agreements) as an informal part of the decision making process.
However the Dutch presidency insists on non-existing council rules.
They are doing the exact opposite of what a %(nv:2/3 majority of their
Parliament asked them to do)

#enn: Jozef Halbersztadt works as a patent examiner at Polish Patent Office.
The Polish Patent Office has resisted pressure from the European
Patent Office to allow software patents and is one of the most fervent
opponents of the Council's text, which it fears would plunge it into a
quagmire of broad and trivial patents on abstract subject matter. 
Halbersztadt has watched the EU developments for many years and draws
the following analogy to the Ukrainian presidential elections:

#luw: We could see our fight in terms of Ukrainian presidential election. 
Yanukovych was picked as a candidate by the current president, Kuchma,
in return for guarantees to the president and his allies of immunity
from prosecution for all his crimes, committed while in power, as well
as protection of their assets gained through manipulated
privatisation.  The Ukrainian people were fed up and voted him down.
And they were denied their victory by a group of crooks.

#miW: Our scenario is similar. We have won in the European Parliament.  We
are now denied victory by manipulation and irregularities in the
Council. The Kuchmas of the EU don't like consultation with national
parliaments, recounting in the Council or restarting in the EP, nor
any other open approach to problems. The main difference is that they
--- hopefully --- aren't considering deployment of troops and use of
force.

#hfW: and remarks that the common position will %(nf:not be finalised under
the Dutch presidency)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons041213 ;
# txtlang: en ;
# multlin: t ;
# End: ;

