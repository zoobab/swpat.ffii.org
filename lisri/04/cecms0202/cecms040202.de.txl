<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Kartellverfahren EU gegen MS: Ausschluss von Freier Software bei patentierten Standards scheint als Vernünftig eingestuft

#descr: According to this report, the European Commission demands that Microsoft's workgroup server protocols should be available under %(q:Reasonable and Non-Discriminatory) licensing conditions, which means that they should not be available for free/opensource software projects such as Samba.

#sWW: Economist report über das CEC vs MS Kartellverfahren

#WWf: According to this report, the European Commission demands that Microsoft protocols should be available under %(q:Reasonable and Non-Discriminatory) licensing conditions, which means that they should not be available for free/opensource software projects such as Samba.

#tWW: In its anti-trust procedings against Microsoft, the European Commission has found that Microsoft is trying to extend its desktop monopoly into the market for %(tp|workgroup servers|file, print, mail and web servers) by keeping secret the communications protocols that enable its desktop and server products to talk to each other. %(q:Without such information, alternative server software would be denied a level playing field, as it would be artificially deprived of the opportunity to compete with Microsoft's products on technical merits alone,) the Commission warned in 2001.

#atW: Yet, the Commission has stopped short of demanding that Microsoft make its protocols freely available.  According to an article in the Economist, Microsoft will only be required to license its server-communications protocols to rivals on a %(q:reasonable and non-discriminatory) basis, which means e.g. that free/opensource software projects such as Samba can be excluded, and a look at Microsoft's recently published aggressive licensing program shows that indeed they are.

#frW: Another arm of the Commission is fighting for the right of Microsoft and other companies to forbid interoperation with any patented standard which they chose to impose by their market power.  Bolkestein's Industrial Property Unit has been urging the Council members to remove Art 6a of the European Parliament's Software Patent Directive Proposal.  Art 6a demands that the use of a patented technique for mere interoperation purposes should always be royalty-free.)

#wee: The Economist this week has a long piece on the state of play between Microsoft and the antitrust authorities, particularly in Europe.

#seg: Even in this very high profile anti-trust case, the Commission seems not to be prepared to go further than requiring %(q:reasonable and non-discriminary) (= uniform fee) licensing -- there seems to be no suggestion of demanding royalty-free open publication of standards, even though for Workgroup services, one of the major competitiors must be Samba.

#Wog: On the other hand, Article 6a of the %(ep:Parliament's version of the software patent directive) says that RAND is not good enough -- it must be possible to compete without the burden of license-collecting, e.g. on the basis of free/opensource software.

#ror: Auszüge aus dem Artikel:

#eWi: The commission's case against Microsoft is detailed in a confidential document, known as the %(q:Statement of Objections), the most recent version of which has been seen by The Economist. The document, drawn up last August, builds on two previous statements (in 2000 and 2001) which accused Microsoft of behaving anti-competitively in two areas.

#tcu: First, the commission alleged that Microsoft was trying to extend its desktop monopoly into the market for workgroup servers (file, print, mail and web servers) by keeping secret the communications protocols that enable its desktop and server products to talk to each other. %(q:Without such information, alternative server software would be denied a level playing field, as it would be artificially deprived of the opportunity to compete with Microsoft's products on technical merits alone,) the commission warned in 2001.

#yWa: Second, Microsoft was accused of trying to extend its monopoly into the media-player market, by incorporating its Windows Media Player (WMP) software into Windows, so ensuring that it would be installed on over 95% of new PCs. Rival products, the commission observed, did not have this advantage; nor could WMP be uninstalled. %(q:The result is a weakening of effective competition in the market...and less innovation,) it concluded.

#lpi: der neueste unterstüztzende Beweis

#cWw: Accordingly, a number of remedies are proposed. The simplest is a fine that reflects the %(q:gravity and duration) of the infringement. European antitrust law allows violators to be fined as much as 10% of their annual worldwide revenues -- a fine of more than $3 billion in Microsoft's case. In addition, Microsoft would be required to license its server-communications protocols to rivals on a %(q:reasonable and non-discriminatory) basis. This is consistent with the settlement that Microsoft reached in America, which also requires it to license some of its protocols.

#awe: Following a review of the progress of the American settlement, on January 23rd Microsoft agreed to simplify and extend its licensing programme to encourage wider use. Critics had complained that its previous licensing terms were so complicated that only 11 companies had signed up for them. After the announcement, Judge Colleen Kollar-Kotelly, who is overseeing the American settlement, declared herself satisfied with the company's efforts to comply with the settlement.

#gnr: devising remedies for Windows Media Player is harder

#rlW: Short of a break-up, however, there is no effective antidote to tying. Forcing Microsoft to produce a Europe-specific version of Windows without WMP (or any other specific features) would, in effect, impose an inferior product on European consumers. It is difficult to argue that this would be in their interests. And it would, in any case, probably result in a grey market as the full version of Windows was imported from elsewhere. There are also problems with the must-carry approach: which other media players would be included? Presumably those with the greatest market share. But that would itself be anti-competitive, since it would entrench the positions of the existing players. Furthermore, WMP would still be ubiquitous.

#ese: Both sides may well want to settle before enlargement in May

#irf: Reading between the lines, it is just possible to discern the kind of settlement that might now be under discussion: a far wider licensing programme, perhaps one that confers special privileges on companies (such as RealNetworks and Apple) that develop software which competes with parts of Windows. But the devil would be in the details: such a deal might prove to be no more than a grand gesture, allowing the commission to declare victory and then retreat. Microsoft has, after all, shown itself to be a master at running rings around regulators. That said, the commission's Statement of Objections pre-emptively disallows a number of ways in which Microsoft could evade the proposed tying remedies --- which suggests that the commission has learned from past regulatory experiences.

#ian: But MS may fight it all the way; and the whole issue may happen again, if MS integrates search technology, threatening Google

#eWh: Initially, the MSN search toolbar is a free optional download, as Microsoft's web browser and media player once were. The next step, inevitably, will be to integrate such search functions into Windows, on the grounds that it constitutes a core technology that should be part of the operating system. In his keynote speech at last November's Comdex show in Las Vegas, Mr Gates demonstrated a prototype technology called %(q:Stuff I've Seen) which does just that. It allows computer users to search for context-specific words in e-mails and in recently visited web pages, as well as in documents on their computers.

#WWe: eventually MS might do best if it handed over some of its Windows protocols to an independent standards body... but that still seems unimaginable at the moment

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri04.el ;
# mailto: mlhtimport@a2e.de ;
# login: astohrl ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cecms040202 ;
# txtlang: de ;
# multlin: t ;
# End: ;

