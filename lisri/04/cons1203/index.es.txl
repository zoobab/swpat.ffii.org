<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: El Consejo se empeña en el acuerdo sobre Patentes de Software,
publicando un Documento Explicativo en múltiples idiomas

#descr: The Council seems to be working under high pressure to prepare
materials in order to adopt the Software Patent Agreement of
2004-05-18 as a Common Position before the end of term of the Dutch
Presidency this year.

#uWo: Razonamiento del Consejo

#pth: Un borrador que explica el razonamiento del consejo en la directiva de
patentes de software apareció hace unos días en inglés y ha sido
traducido a 20 idiomas.

#rAn: Busque en la lista las entradas %(q:ADD 1).

#eaW: Los documentos explicativos pueden tener bastante influencia en el
significado de un texto legal. Mucho más en caso de opacidad.

#rdw: Uno puede preguntarse si un nuevo documento que explica las razones
del Acuerdo puede ser transmitido al consejo sin una mayoría 
cualificada.

#eft: La Presidencia holandesa parece estar evitando conflictos al preparar
un documenmto explicativo que explica lo menos posible. No se explica
que tipo de peticiones de patente deberían ser aceptables y cuáles no,
ni qué intereses serían beneficiados, ni cuáles no y porqué.
Simplemente señalan a la Oficina Europea de Patentes como la
autoridad. La Presidencia holandesa también añade la %(tr:falacia del
TRIPs).  Igualmente podrían haber declarado que la Tierra es plana. De
nuevo, uno podría preguntarse si los estados miembros pueden ser
alistados en apoyo de tal declaración sin una nueva votación.

#twe: Dada la absoluta %(np:lealtad al Consejo) del Gobierno  Holandés es
decir, obediencia a %(Philips) y desprecio al %(dp:Parlamento
holandés)), probablemente meterán el nuevo texto en una enorme pila de
Items A y tratarán de sacarlo adelante en la próxima sesión de  algún
consejo de ministros no relacionado (p.ej: sobre subsidios de
Agricultura o energía nuclear) en los próximos días.

#WWW: Un grupo de enmiendas del Parlamento europeo es rechazado por el
Consejo con la unica justificación de %(q:no reflejar la práctica
establecida). ¿Qué tipo de argumento es éste?. Esperemos por el bien
del Consejo que el Parlamento Europeo nunca más tenga la idea de que
puede enviar propuestas que realmente puedan cambiar las cosas.
Imagina, políticos pensando que pueden detener la práctica establecida
porque perjudica la economía. ¿A dónde iría a parar el mundo?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/swpatlisri04.el ;
# mailto: tatel@infonegocio.com ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: cons041203 ;
# txtlang: xx ;
# End: ;

