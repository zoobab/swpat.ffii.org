RAT DER EUROP�ISCHEN UNION

Br�ssel, den 24. November 2004 (30.11) (OR. en)

Interinstitutionelles Dossier: 2002/0047 (COD)

11979/04 ADD 1

PI 61 CODEC 962

ENTWURF DER BEGR�NDUNG DES RATES Betr.: Gemeinsamer Standpunkt des Rates im Hinblick auf den Erlass einer Richtlinie des Europ�ischen Parlaments und des Rates �ber die Patentierbarkeit computerimplementierter Erfindungen

ENTWURF DER BEG�NDUNG DES RATES

11979/04 ADD 1 DG C I

rs/wk

1

DE


I.

EINLEITUNG 1. Die Kommission hat am 20. Februar 2002 einen auf Artikel 95 des EG�Vertrags gest�tzten Vorschlag f�r eine Richtlinie des Europ�ischen Parlaments und des Rates �ber die Patentierbarkeit computerimplementierter Erfindungen 1 vorgelegt. 2. Der Wirtschafts- und Sozialausschuss hat seine Stellungnahme am 19. September 2002 abgegeben 2. 3. Das Europ�ische Parlament hat am 24. September 2003 in erster Lesung Stellung genommen 3. 4. 5. Die Kommission hat keinen ge�nderten Vorschlag unterbreitet. Der Rat hat seinen Gemeinsamen Standpunkt gem�� Artikel 251 des EG�Vertrags am ... festgelegt.

II.

ZIEL 6. Ziel der vorgeschlagenen Richtlinie ist es, die Vorschriften des nationalen Patentrechts hinsichtlich der Patentierbarkeit computerimplementierter Erfindungen zu harmonisieren und die Patentierungsvoraussetzungen transparenter zu machen.

III. GEMEINSAMER STANDPUNKT Erw�gungsgr�nde 7. Der Rat hat eine Reihe von Erw�gungsgr�nden des Kommissionsvorschlags modifiziert oder zusammengefasst und einige zus�tzliche Erw�gungsgr�nde aufgenommen. Dabei �bernahm er die Ab�nderungen 1, 2, 88, 3, 34, 115, 85, 7, 8, 9, 86, 11, 12 und 13 des Europ�ischen Parlaments vollst�ndig, teilweise oder in umformulierter Form. Nachfolgend wird bei den entsprechenden Artikeln auf die wichtigsten �nderungen an den Erw�gungsgr�nden verwiesen.

1 2 3

ABl. C 151 vom 25.6.2002, S. 129. ABl. C 61 vom 14.3.2003, S. 154. Dok. 11503/03 CODEC 995 PI 70. rs/wk DG C I 2

11979/04 ADD 1

DE


Artikel 
Artikel 1 (Anwendungsbereich) 
8. Artikel 1 wurde in der im Kommissionsvorschlag enthaltenen Fassung �bernommen. Auch vom Europ�ischen Parlament wurden keine �nderungsvorschl�ge zu diesem Artikel gemacht. 
Artikel 2 (Begriffsbestimmungen) 
9. Was Buchstabe a anbelangt, so ist der Rat den EP�Ab�nderungen 36, 42 und 117 teilweise gefolgt und hat die Worte "auf den ersten Blick mindestens" und "neuartiges" aus der Definition des Begriffs "computerimplementierte Erfindung" gestrichen, weil sie seiner Ansicht nach �berfl�ssig sind und hinsichtlich ihres Bezugs zum Neuheitstest, der im Zuge der Pr�fung auf Patentierbarkeit einer Erfindung vorgenommen wird, Verwirrung stiften k�nnten. 
10. In Bezug auf Buchstabe b hat der Rat - den Ausdruck "Gebiet der Technik" durch "Gebiet der Technologie" ersetzt, wie er �blicherweise in internationalen Patentrechts�bereinkommen wie dem TRIPS� �bereinkommen verwendet wird; - das Wort "neu" eingef�gt, um die Kriterien des "technischen Beitrags" deutlicher zu machen; - einen zweiten Satz aufgenommen, bei dem es sich im Wesentlichen um die Bestimmung aus Artikel 4 Absatz 3 des Kommissionsvorschlags handelt, die geringf�gig abge�ndert wurde, um deutlich zu machen, dass technische Merkmale unverzichtbarer Bestandteil von Patentanspr�chen sind, auch wenn zur Bewertung des technischen Beitrags einer computerimplementierten Erfindung nichttechnische Merkmale herangezogen werden. Diese Sichtweise deckt sich zum Teil mit den Ab�nderungen 16, 100, 57, 99, 110 und 70 des Europ�ischen Parlaments.

11979/04 ADD 1 DG C I

rs/wk

3

DE


Artikel 3 des Kommissionsvorschlags (Gebiet der Technik) 

11. Mit diesem Artikel sollte den Mitgliedstaaten die Verpflichtung auferlegt werden, in ihren nationalen Rechtsvorschriften sicherzustellen, dass computerimplementierte Erfindungen als einem Gebiet der Technik zugeh�rig gelten. Im Einklang mit der Ab�nderung 15 des Europ�ischen Parlaments hat der Rat die Streichung von Artikel 3 beschlossen, da sich seiner Auffassung nach eine generelle Verpflichtung dieser Art nur schwer in nationales Recht umsetzen l�sst. Stattdessen hat sich der Rat daf�r entschieden, im Erw�gungsgrund 13 die diesbez�gliche Aussage, die im Erw�gungsgrund 11 des Kommissionsvorschlags enthalten ist, in deutlicherer Form aufzugreifen. Artikel 3 (Artikel 4 des Kommissionsvorschlags) (Voraussetzungen der Patentierbarkeit) 

12. Der Rat hat die ersten beiden Abs�tze von Artikel 4 des Kommissionsvorschlags zu einem einzigen Absatz zusammengefasst und zugleich geringf�gige redaktionelle �nderungen vorgenommen, um den Text klarer zu gestalten. Die neue Fassung entspricht w�rtlich dem vom Europ�ischen Parlament in der Ab�nderung 16 f�r Artikel 4 Absatz 1 vorgeschlagenen Wortlaut. 

13. Wie bereits erw�hnt, wurde Artikel 4 Absatz 3 des Kommissionsvorschlags in die Definition des Begriffs "technischer Beitrag" in Artikel 2 Buchstabe b �bernommen, weil er offensichtlich eher in die Begriffsbestimmungen als in einen Artikel mit der �berschrift "Voraussetzungen der Patentierbarkeit" geh�rt. 

Artikel 4 (Ausschluss von der Patentierbarkeit) 

14. Um Missverst�ndnissen vorzubeugen, hat der Rat in Absatz 1 dieses Artikels eine eindeutige Aussage getroffen, n�mlich dass ein Computerprogramm als solches keine patentierbare Erfindung darstellen kann.

11979/04 ADD 1 DG C I

rs/wk

4

DE


15.

Absatz 2, der sich mit der Ab�nderung 17 des Europ�ischen Parlaments deckt, soll klarstellen, wo die Grenzen der Patentierbarkeit im Rahmen der Richtlinie liegen; er ist im Zusammenhang mit den Erw�gungsgr�nden 14 bis 16 zu sehen, die den Ab�nderungen 85, 7 und 8 des Europ�ischen Parlaments entsprechen. Jedoch hat der Rat den Passus "sei es als Quellcode, als Objektcode oder in anderer Form ausgedr�ckt" eingef�gt, um klarer zum Ausdruck zu bringen, was unter "Erfindungen, zu deren Ausf�hrung ein Computerprogramm eingesetzt wird" zu verstehen ist.

Artikel 5 (Form des Patentanspruchs) 
16. 
17. Absatz 1 wurde unver�ndert aus dem Kommissionsvorschlag �bernommen. Absatz 2 wurde hinzugef�gt, um deutlich zu machen, dass ein Patent unter bestimmten Umst�nden und unter genau definierten Bedingungen einen Patentanspruch auf ein Computerprogramm, sei es auf das Programm allein oder auf ein auf einem Datentr�ger vorliegendes Programm, begr�nden kann. Nach Ansicht des Rates folgt die Richtlinie damit der g�ngigen Praxis sowohl des Europ�ischen Patentamts als auch der Mitgliedstaaten. 

Artikel 6 
(Konkurrenz zur Richtlinie 91/250/EWG) 
18. Der Rat hat die Ab�nderung 19 des Europ�ischen Parlaments �bernommen, weil sie seiner Ansicht nach klarer als der Kommissionsvorschlag formuliert ist. Die Verweise auf Marken und Halbleitertopografien wurden gestrichen, da sie in diesem Zusammenhang f�r unerheblich gehalten werden. 

19. Die Ab�nderung 76 des Europ�ischen Parlaments wurde vom Rat nicht �bernommen, da sie seiner Meinung nach zu ungenau ist und im Widerspruch zum TRIPS��bereinkommen steht. Der Aspekt der Interoperabilit�t ist bereits hinreichend durch Artikel 6 sowie durch die Anwendung allgemeiner Wettbewerbsvorschriften geregelt, wie eindeutig aus den Erw�gungsgr�nden 21 und 22 des Gemeinsamen Standpunkts des Rates hervorgeht.

11979/04 ADD 1 DG C I

rs/wk

5

DE


Artikel 7 (Beobachtung) 

20. Der Rat hat die Ab�nderung 71 des Europ�ischen Parlaments �bernommen.

Artikel 8 (Bericht �ber die Auswirkungen der Richtlinie) 
21. Der Rat hat die Fassung des Kommissionsvorschlags unver�ndert �bernommen und folgende zus�tzliche Elemente eingef�gt: - Buchstabe b: Wie vom Europ�ischen Parlament in Ab�nderung 92 vorgeschlagen, wurden die Worte "die Laufzeit des Patents und" hinzugef�gt; zudem hat der Rat unter Ber�cksichtigung der Ab�nderung 25 des Europ�ischen Parlaments einen erg�nzenden Verweis auf die internationalen Verpflichtungen der Gemeinschaft aufgenommen. - - - Buchstabe d: Der Rat hat die Ab�nderung 23 des Europ�ischen Parlaments �bernommen. Buchstabe c: Der Rat hat die Ab�nderung 26 des Europ�ischen Parlaments �bernommen. Buchstabe f: Der Rat hat die Ab�nderung 25 des Europ�ischen Parlaments �bernommen, jedoch die Bezugnahme auf das Gemeinschaftspatent gestrichen, da sie seiner Auffassung nach in diesem Zusammenhang unerheblich ist. - Buchstabe g: Der Rat hat die Ab�nderung 89 des Europ�ischen Parlaments zwar inhaltlich �bernommen, jedoch eine klarere Formulierung gew�hlt. Artikel 9 des Gemeinsamen Standpunkts des Rates (Pr�fung der Auswirkungen) 
22. Der Rat hat die Ab�nderung 27 des Europ�ischen Parlaments �bernommen.

Artikel 10 (Artikel 9 des Kommissionsvorschlags) (Umsetzung) 
23. Im Unterschied zum Europ�ischen Parlament, das eine Umsetzungsfrist von achtzehn Monaten w�hlte (Ab�nderung 28), hat sich der Rat f�r eine Umsetzungsfrist von vierundzwanzig Monaten entschieden.

11979/04 ADD 1 DG C I

rs/wk

6

DE


Artikel 11 (Inkrafttreten) und 12 (Adressaten) (Artikel 10 und 11 des Kommissionsvorschlags) 24. Der Rat hat die Fassung des Kommissionsvorschlags �bernommen.

IV. NICHT �BERNOMMENE AB�NDERUNGEN DES EUROP�ISCHEN PARLAMENTS 
25. 
Nach eingehender Pr�fung der Ab�nderungen sah sich der Rat au�er Stande, die Ab�nderungen 88 (Satz 1), 31, 32, 112, 95, 84, 114, 125, 75, 36, 42, 117, 107, 69, 55/rev, 97, 108, 38, 44, 118, 45, 16, 100, 57, 99, 110, 70 (teilweise), 60, 102, 111, 72, 103, 119, 104, 120, 76, 24, 81, 93, 94 und 28 des Europ�ischen Parlaments zu �bernehmen. 

26. 
Der Rat vertritt die Ansicht, dass einige dieser Ab�nderungen �berfl�ssig sind (Ab�nderungen 88 (Satz 1), 31, 75, 94), unklar sind und zu Verwirrung f�hren k�nnten (Ab�nderungen 36, 42, 117, 72, 104, 120), keinen unmittelbaren Bezug zu den anstehenden Fragen haben (Ab�nderungen 95, 24, 81), nicht die g�ngige Praxis widerspiegeln (Ab�nderungen 32, 112, 16, 100, 57, 99, 110, 70, 102, 111) oder im Widerspruch zu den internationalen Verpflichtungen der Europ�ischen Gemeinschaft und ihrer Mitgliedstaaten im Rahmen des TRIPS��bereinkommens sowie den allgemeinen Grunds�tzen des Patentrechts stehen (Ab�nderungen 84, 114, 125, 107, 69, 55/rev, 97, 108, 38, 44, 118, 45, 60, 103, 119, 76, 93). 

V. FAZIT 27. Der Rat hat in seinen Gemeinsamen Standpunkt eine erhebliche Zahl von Ab�nderungsvorschl�gen des Europ�ischen Parlaments �bernommen. Er hat sich in dem Gemeinsamen Standpunkt durchg�ngig darum bem�ht, einen vern�nftigen und realistischen Ausgleich zwischen den Interessen der Patentinhaber und denen anderer betroffener Parteien herzustellen. Der Gemeinsame Standpunkt des Rates ist von der Kommission als insgesamt ausgewogen begr��t und als zufrieden stellendes Kompromisspaket akzeptiert worden. 
________________________

11979/04 ADD 1 DG C I

rs/wk

7

DE


