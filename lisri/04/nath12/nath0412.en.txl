<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#cee: The %(on:ordinance) represents further amendment to the Indian
%(pa:Patents Act of 1970) (see %(mg:ministry gloss)).

#ipt: Regarding software patents, its effect is to replace the following
clause, introduced in the %(pa:%(tp|Patents|Amendment) Act 2002),
specifying as excluded subject-matter: %(OLD) by new clauses: %(NOV)

#cWe: a mathematical or business method or computer programme per se or
algorithms;

#rln: a computer programme per se other than its technical application to
industry or a combination with hardware;

#mbW: a mathematical method or business method or algorithms;

#Weo: It is not yet clear how these changes would effect the %(rs:Rules for
Software Patentability) in the Indian Patent Office's Manual of Patent
Practice and Procedure.

#tjl: Similar changes were %(uk:proposed by the UK Patent Office as an
alternative to the faltering directive project in early 2002) and by
the %(ce:Council's text of 2004-05-18).  The trick consists in
redefining the exclusion of computer programs (i.e. claim objects such
as %(q:a computer program, characterised by [ some allegedly new way
of using conventional general-purpose computing equipment ])) to a
pseudo-exclusion that has no regulatory meaning whatsoever (but may
serve temporary political purposes, such as deception of journalists).

#nnW: The ministry says that the ordinance is needed for implementing TRIPs
requirements, for %(q:adapting to the practice of our major trading
partners), for %(q:harmonising the practice of regional patent
offices).  While the ministry says that the software industry needs
patents, it claims that these %(q:only cover software products, not
the algorithms as such), or even %(q:only embedded software) (i.e.
software %(q:embedded) in a general-purpose computer).   Nath's patent
officials seem to have learnt the whole European repertoire of tricks
and lies.

#krW: The ordinance enters into force on 2005-01-01, but lapses
automatically on 2005-07-01 unless it gains parliamentary
confirmation.  The question of product patents for drugs, apparently
without any of the flexibilities made allowable under the WTO Doha
round, has raised particular widespread concern, especially among the
Left and Communist parties, the partners of Nath's Congress party in
the governing coalition.  The issues are therefore still very much up
for grabs; discussion of them in the Indian parliament will probably
resume in February 2005.

#dWo: Software per se was excluded; we felt this meant if software has
technical character, it can be patented, but patent office practice
was not uniform; this called for a clarification

#Wee: good for procedural overview

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: nath0412 ;
# txtlang: en ;
# multlin: t ;
# End: ;

