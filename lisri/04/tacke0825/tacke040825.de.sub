\begin{subdocument}{tacke040825}{Staatssekret\"{a}r entschuldigt sich bei Bitkom f\"{u}r BMWA-Umfrage}{http://swpat.ffii.org/log/04/tacke0825/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2004/09/03 von PILCH Hartmut\footnote{\url{http://www.ffii.org/\~phm}}}{Der Staastssekret\"{a}r im Bundesministerium f\"{u}r Wirtschaft und Arbeit, Dr. Alfred Tacke, entschuldigt sich in einem Brief an den Bitkom-Vorsitzenden Bernhard Rohleder f\"{u}r die vom BMWA im Juli veranstaltete Umfrage \"{u}ber die Auswirkungen von Softwarepatenten auf die Softwarebranche.}
\begin{sect}{intro}{Der Brief}
Der Staastssekret\"{a}r im Bundesministerium f\"{u}r Wirtschaft und Arbeit, Dr. Alfred Tacke, entschuldigt sich in einem Brief an den Bitkom\footnote{\url{http://swpat.ffii.org/akteure/bitkom/index.de.html}}-Vorsitzenden Bernhard Rohleder\footnote{\url{tacke040825.pdf}} f\"{u}r die vom BMWA im Juli veranstaltete Umfrage \"{u}ber die Auswirkungen von Softwarepatenten auf die Softwarebranche\footnote{\url{}}.

Insbesondere bedauert Tacke, dass die vom Bitkom beanstandete Wortwahl (``Softwarepatente'' statt ``computer-implementierte Erfindungen\footnote{\url{http://swpat.ffii.org/papiere/eubsa-swpat0202/kinv/index.de.html}}'' zu dem falschen Eindruck Anlass gegeben haben k\"{o}nnte, dass f\"{u}r die Bundesregierung die Frage der Patentierbarkeit von Software noch offen sei.

Tacke schreibt mit Datum 2004-08-25:

\begin{quote}
{\it Sehr geehrter Herr Rohleder,}

{\it vielen Dank f\"{u}r Ihr Schreiben vom 22. Juli 2004.  Ich bedaure, wennn die Umfrage zur Wechselbeziehung zwischen Interoperabilit\"{a}t, Patentschutz und Wettbwerb zu Missverst\"{a}ndnissen und Irritationen \"{u}ber die grundlegende Position der Bundesregierung zum Richtlinienentwurf \"{u}ber die Patentierbarkeit computerimplementierter Erfindungen gef\"{u}hrt hat.}

{\it Diese Umfrage ist Teil einer Studie, mit der ausschlie{\ss}lich die Wechselbeziehungen zwischen Interoperabilit\"{a}t, Patentschutz und Wettbewerb untersucht werden sollen.  Keinesfalls soll diese Umfrage, wie es die bedauerlicherweise in Teilen recht ungl\"{u}ckliche Wortwahl suggeriert, zu einer Neuauflage der Diskussion \"{u}ber die Patentierbarkeit von computerimplementierten Erfindungen insgesamt f\"{u}hren.  Es ist schon jetzt absehbar, dass die Fragebogenaktion keine Resultate bringen wird, die den Anforderungen an wissenschaftlich fundierte Ergebnisse gerecht werden.  Dies liegt auch daran, dass es -- nicht zuletzt wegen der K\"{u}rze der Erhebungszeit -- vielen Unternehmen nicht m\"{o}glich war, den Fragebogen zu beantworten.}

{\it Ich hoffe, Ihre Bedenken damit ausger\"{a}umt zu haben.}
\end{quote}
\end{sect}

\begin{sect}{bmwa}{BMWA-Kontinuit\"{a}t: Interessen der Patent\"{a}mter sind Heilige K\"{u}he}
Tacke und andere BMWA-Beamte hatten schon in der Vergangenheit immer wieder darauf aufmerksam gemacht, dass die Interessen der Patentbeh\"{o}rden an der nachtr\"{a}glichen Absegnung ihrer Softwarepatentierungspraxis f\"{u}r das BMWA eine heilige Kuh darstellt und keinesfalls zur Diskussion gestellt werden darf.

Bisher vom BWMA in Auftrag gegebene Studien belegten, dass die Softwarebranche zur Belohnung/Stimulierung ihrer Investitionst\"{a}tigkeit keine Patente braucht.  Dennoch empfahlen diese Studien in dem von Politikern gelesenen kurzen Zusammenfassungsteil, die Regierung m\"{o}ge den Patent\"{a}mtern geben, was sie verlangen.

Eine vom BMWA in Auftrag gegebene Studie des Infratest-Instituts vom April 2004\footnote{\url{http://swpat.ffii.org/papiere/bmwa0404/index.de.html}} fordert ebenfalls die ``Erm\"{o}glichung der Patentierung computer-implementierter Erfindungen'', ohne jedoch in ihrem Hauptteil \"{u}berhaupt Untersuchungen zum Thema Patente oder gewerblicher Rechtschutz anzustellen.

Die Arbeit des BMWA zu Softwarepatenten wird im wesentlichen nicht von Alfred Tacke sondern vom Leiter des Patentreferats des Ministeriums, NB, geleitet.  Wir wissen nicht, in wie weit Tacke selber hinter dem Brief an Bitkom steht.

Bitkom hatte Anfang August vom BMWA gefordert, seine Untersuchung zu Softwarepatenten zu stoppen, da darin der ``irref\"{u}hrende'' Begriff ``Softwarepatente'' gebraucht wird.  Die Entscheidung hierzu war von dem Vorsitzenden des ``Arbeitskreises f\"{u}r Geistiges Eigentum'' des Bitkom, IBM-Patentanwalt Fritz Teufel, getroffen worden.  Bislang leitet Teufel, gest\"{u}tzt von den 5-10 Gro{\ss}konzern-Patentanw\"{a}lten, die regelm\"{a}{\ss}ig an seinen Arbeitskreissitzungen teilnehmen, weitgehend unangefochten die Arbeit des Bitkom zu Patentfragen.  Seine Entscheidung, die Bundesregierung \"{o}ffentlich zu kritisieren, f\"{u}hrte jedoch zum \"{U}berkochen der Unruhe innerhalb des Vereins.  Nach Erhalt zahlreicher Protestschreiben beauftragte Rohleder Teufels Arbeitskreis, den Bitkom-Standpunkt zu \"{u}berarbeiten.  Das Schreiben von Alfred Tacke d\"{u}rfte nun dazu beitragen, den Druck von Teufel wegzunehmen.

Laut neuesten Zeitungsberichten\footnote{\url{http://www.sueddeutsche.de/wirtschaft/artikel/593/38555/}} verl\"{a}sst Tacke gerade seinen Posten, um zu seinem fr\"{u}heren Minister Werner M\"{u}ller\footnote{\url{}} in die Energiewirtschaft \"{u}berzuwechseln.

Aus dem Schreiben von Tacke ist u.a. folgendes zu schlie{\ss}en
\begin{itemize}
\item
Die 1400 R\"{u}ckl\"{a}ufe der Studie haben zu Erkenntnissen gef\"{u}hrt, die im BMWA nicht erw\"{u}nscht sind.

\item
Der Initiator der Studie, Dr. Ulrich Sandl, hat \"{A}rger bekommen
\end{itemize}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Wirtschaftsministerium distanziert sich von eigener Umfrage\footnote{\url{http://www.heise.de/newsticker/meldung/50669}}}}

\begin{quote}
Stefan Krempl berichtet \"{u}ber den Tacke-Brief und zitiert entgegengesetzte Stellungnahmen von J\"{o}rg Tauss u.a.

siehe auch \url{}
\end{quote}
\filbreak

\item
{\bf {\bf FAZ 2004-09-05: Rote Amigos\footnote{\url{http://www.faz.net/s/Rub4D8A76D29ABA43699D9E59C0413A582C/Doc\~E526DEB118E314CCD8655A69C4B283FFE\~ATpl\~Ecommon\~Scontent.html}}}}

\begin{quote}
Kommentar wirft Tacke vor, sich im privaten Interesse gemeinsam mit Werner M\"{u}ller \"{u}ber Wettbewerbsbedenken hinweggesetzt und ein Energiemonopol geschaffen zu haben, in dessen Vorstand M\"{u}ller und er sich dann abseilten.

siehe auch Wirtschaftsministerium distanziert sich von eigener Umfrage\footnote{\url{http://www.heise.de/newsticker/meldung/50669}} und \url{}
\end{quote}
\filbreak

\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Vom Teufel geritten --- BITKOM e.V.\footnote{\url{http://swpat.ffii.org/akteure/bitkom/index.de.html}}}}

\begin{quote}
Der deutsche Branchenverband Bitkom hat erst Mitte 2001 begonnen, sich mit Fragen der Patentpolitik zu befassen.  Die Meinungsbildung fand offenbar in einem sehr kleinen Kreis von Juristen und Patentjuristen statt, wobei IBM-Patentanwalt Fritz Teufel alles dominierte.  An der EU-Konsultation 2000 zu Swpat nahm Bitkom wegen unabgeschlossener Meinungbildung in der Sache nicht teil, daf\"{u}r aber sprang der europ\"{a}ische Dachverband EICTA ein, f\"{u}r den offenbar ebenfalls Teufel die Stellungnahme schrieb.
\end{quote}
\filbreak

\item
{\bf {\bf PA Fritz Teufel\footnote{\url{http://swpat.ffii.org/akteure/teufel/index.en.html}}}}

\begin{quote}
Patent lawyer, software patentability guru, patent department head of IBM in Germany and Europe, working in Stuttgart, active promoter of software patents, responsible for pushing many landmark cases through the EPO and the German courts.  Ghostwriter of various patent papers of German and European trade associations.  Positions and style well known from public discussions. Hardline advocate of software patentability and very much in love with certain dogmatic fallacies which he successfully used to win over the (already very inclined) EPO and BGH in a series of decisive battles.  Do not expect Teufel to understand the viewpoint of opensource programmers or to come up with solutions to non-juridical problems.  Expect him to stick to EPO fallacies as steadfastly as anyone.  These fallacies constitute his success experience before the lawcourts.
\end{quote}
\filbreak

\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Softwarepatent-Nachrichten\footnote{\url{http://swpat.ffii.org/log/neues/index.de.html}}}}

\begin{quote}
FFII berichtet \"{u}ber neueste Entwicklungen in der Auseinandersetzung um die Grenzen der Patentierbarkeit.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri04.el ;
% mode: latex ;
% End: ;

