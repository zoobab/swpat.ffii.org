<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Staatssekretär entschuldigt sich bei Bitkom wegen BMWA-Umfrage

#descr: [Heise http://www.heise.de/newsticker/meldung/50669] | [silicon.de
http://www.silicon.de/cpo/news-wipo/detail.php?nr=16342] | [FAZ
http://tinyurl.com/48phe] | [FTD http://www.ftd.de/pw/de/1094280024787
.html?nv=cpwd über Tacke's Desinterresse an Makroökonomie]

#eBe: Der Brief

#tWd: BMWA-Kontinuität: Interessen der Patentämter sind Heilige Kühe

#fks: Der Staastssekretär im Bundesministerium für Wirtschaft und Arbeit,
Dr. Alfred Tacke, entschuldigt sich in einem %(bb:Brief an den
Bitkom-Vorsitzenden Bernhard Rohleder) für die %(bm:vom BMWA im Juli
veranstaltete Umfrage über die Auswirkungen von Softwarepatenten auf
die Softwarebranche).

#mgr: Insbesondere bedauert Tacke, dass die vom Bitkom beanstandete Wortwahl
(%(q:Softwarepatente) statt %(q:%(ci:computer-implementierte
Erfindungen)) zu dem falschen Eindruck Anlass gegeben haben könnte,
dass für die Bundesregierung die Frage der Patentierbarkeit von
Software noch offen sei.

#Wtk: Tacke schreibt mit Datum 2004-08-25:

#WWl: Sehr geehrter Herr Rohleder,

#Wse: vielen Dank für Ihr Schreiben vom 22. Juli 2004.  Ich bedaure, wennn
die Umfrage zur Wechselbeziehung zwischen Interoperabilität,
Patentschutz und Wettbwerb zu Missverständnissen und Irritationen über
die grundlegende Position der Bundesregierung zum Richtlinienentwurf
über die Patentierbarkeit computerimplementierter Erfindungen geführt
hat.

#boW: Diese Umfrage ist Teil einer Studie, mit der ausschließlich die
Wechselbeziehungen zwischen Interoperabilität, Patentschutz und
Wettbewerb untersucht werden sollen.  Keinesfalls soll diese Umfrage,
wie es die bedauerlicherweise in Teilen recht unglückliche Wortwahl
suggeriert, zu einer Neuauflage der Diskussion über die
Patentierbarkeit von computerimplementierten Erfindungen insgesamt
führen.  Es ist schon jetzt absehbar, dass die Fragebogenaktion keine
Resultate bringen wird, die den Anforderungen an wissenschaftlich
fundierte Ergebnisse gerecht werden.  Dies liegt auch daran, dass es
-- nicht zuletzt wegen der Kürze der Erhebungszeit -- vielen
Unternehmen nicht möglich war, den Fragebogen zu beantworten.

#fWW: Ich hoffe, Ihre Bedenken damit ausgeräumt zu haben.

#iat: Tacke und andere BMWA-Beamte hatten schon in der Vergangenheit immer
wieder darauf aufmerksam gemacht, dass die Interessen der
Patentbehörden an der nachträglichen Absegnung ihrer
Softwarepatentierungspraxis für das BMWA eine heilige Kuh darstellt
und keinesfalls zur Diskussion gestellt werden darf.

#bbi: Bisher vom BWMA in Auftrag gegebene Studien belegten, dass die
Softwarebranche zur Belohnung/Stimulierung ihrer Investitionstätigkeit
keine Patente braucht.  Dennoch empfahlen diese Studien in dem von
Politikern gelesenen kurzen Zusammenfassungsteil, die Regierung möge
den Patentämtern geben, was sie verlangen.

#siu: Eine vom BMWA in Auftrag gegebene [Studie des Infratest-Instituts vom
April 2004 http:Bmwa0404De] fordert ebenfalls die %(q:Ermöglichung der
Patentierung computer-implementierter Erfindungen), ohne jedoch in
ihrem Hauptteil überhaupt Untersuchungen zum Thema Patente oder
gewerblicher Rechtschutz anzustellen.

#tna: Die Arbeit des BMWA zu Softwarepatenten wird im wesentlichen nicht von
Alfred Tacke sondern vom Leiter des Patentreferats des Ministeriums,
%(NB), geleitet.  Wir wissen nicht, in wie weit Tacke selber hinter
dem Brief an Bitkom steht.

#dqa: Bitkom hatte Anfang August vom BMWA gefordert, seine Untersuchung zu
Softwarepatenten zu stoppen, da darin der %(q:irreführende) Begriff
%(q:Softwarepatente) gebraucht wird.  Die Entscheidung hierzu war von
dem Vorsitzenden des %(q:Arbeitskreises für Geistiges Eigentum) des
Bitkom, IBM-Patentanwalt Fritz Teufel, getroffen worden.  Bislang
leitet Teufel, gestützt von den 5-10 Großkonzern-Patentanwälten, die
regelmäßig an seinen Arbeitskreissitzungen teilnehmen, weitgehend
unangefochten die Arbeit des Bitkom zu Patentfragen.  Seine
Entscheidung, die Bundesregierung öffentlich zu kritisieren, führte
jedoch zum Überkochen der Unruhe innerhalb des Vereins.  Nach Erhalt
zahlreicher Protestschreiben beauftragte Rohleder Teufels
Arbeitskreis, den Bitkom-Standpunkt zu überarbeiten.  Das Schreiben
von Alfred Tacke dürfte nun dazu beitragen, den Druck von Teufel
wegzunehmen.

#sWe: Laut %(sz:neuesten Zeitungsberichten) verlässt Tacke gerade seinen
Posten, um zu seinem früheren Minister %(WM) in die Energiewirtschaft
überzuwechseln.

#SWW: Aus dem Schreiben von Tacke ist u.a. folgendes zu schließen

#lni: Die 1400 Rückläufe der Studie haben zu Erkenntnissen geführt, die im
BMWA nicht erwünscht sind.

#ilO: Der Initiator der Studie, Dr. Ulrich Sandl, hat Ärger bekommen

#heise040825t: Wirtschaftsministerium distanziert sich von eigener Umfrage

#heise040825d: Stefan Krempl berichtet über den Tacke-Brief und zitiert
entgegengesetzte Stellungnahmen von Jörg Tauss u.a.

#W0A: FAZ 2004-09-05: Rote Amigos

#cbW: Kommentar wirft Tacke vor, sich im privaten Interesse gemeinsam mit
Werner Müller über Wettbewerbsbedenken hinweggesetzt und ein
Energiemonopol geschaffen zu haben, in dessen Vorstand Müller und er
sich dann abseilten.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatlisri04.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: tacke040825 ;
# txtlang: de ;
# multlin: t ;
# End: ;

