<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Council Presses Ahead with Software Patent Agreement, Publishes
Multilingual Explanatory Document

#descr: The Council seems to be working under high pressure to prepare
materials in order to adopt the Software Patent Agreement of
2004-05-18 as a Common Position before the end of term of the Dutch
Presidency this year.

#uWo: Council Reasoning

#pth: A draft document that explains the reasoning of the council on the
software patent directive, this appeared a few days ago in English and
has been translated to 20 languages

#rAn: Look for all those %(q:ADD 1) entries in the list.

#eaW: Explanatory documents can have quite an influence on what a legal text
means.  This is all the more so in the case of opaque

#rdw: One may wonder whether a new document that explains the reasoning
behind the Agreement can be transmitted to the Council without a
qualified majority.

#eft: The Dutch presidency seems to be avoiding conflicts by preparing an
explanatory document that explains as little as possible.  They do not
explain which kinds of patent claims should be acceptable and which
not, which interests are served thereby and which not and why.  They
merely point to the European Patent Office as the authority. One may
wonder whether this is the intention of a qualified majority.  Also,
the Dutch presidency adds the %(tr:TRIPs fallacy).  They could just as
well have added a statement that the earth is flat.  Again it is to be
wondered whether the member states can be enlisted as supporters of
such a statement without a new vote.

#twe: Given the Dutch government's absolute %(np:loyalty to the Council)
(i.e. obedience to %(Philips) and contempt of the %(dp:Dutch
Parliament), expect them to insert this new text into large pile of
%(A-items) and try to push it through the next session of some
unrelated round of ministers (e.g. on agricultural subsidies or
nuclear energy) in the coming days.

#WWW: One group of amendments from the European Parliament is dismissed by
the Council with the simple justification that they %(q:did not
reflect established practice). What kind of argument is that? Let's
hope for the Council's sake that the European Parliament does not ever
again get the idea that they can submit proposals which actually
change things. Imagine that, politicians that think they can stop
existing practice because it hurts the economy. What is this world
coming to?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatlisri04.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons041203 ;
# txtlang: en ;
# multlin: t ;
# End: ;

