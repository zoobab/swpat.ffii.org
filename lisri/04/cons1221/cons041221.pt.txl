<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2004-12-21 Decisão sobre Patentes de Software Retirada da Agenda do
Conselho da Agricultura sob Solicitação da Polónia

#descr: A Directiva de Patentes de Software foi removida da Agenda do Conselho
da Agricultura. O Secretário de Estado polaco do Ministério da
Tecnologia e Sociedade da Informação, Wlodzimierz Marcinski, solicitou
firmemente a sua remoção no início da reunião. O Comissário da
Agricultura expressou o seu pesar, mas o item-A foi apagado e não será
adoptado este ano.

#Wrr: See Wiki Version for more

#Sto: Story

#iti: Few people had expected the appearance of the Polish %(tp|Vice
Minister of Science and Computerisation|literally %(q:State
Under-Secretary in the Ministry of Science and Informatisation)),
%(WM). Marcinski came personally to Brussels to present Poland's point
of view, because the permanent representatives of Poland at the EU had
been put under great pressure by the Dutch Presidency to accept the
text, rather than to communicate the wishes of the Polish government.

#aeu: After having been targeted by a %(dc:demo campaign), Germany's
minister of agriculture, Renate Künast of the Green Party, %(sk:stayed
away from the meeting).  Instead a diplomat from the German
representation in Brussels was sent with instructions to nod off the
text.  This openly showed a rift within the government.  Meanwhile the
German minister of justice %(bz:hinted a possible will to make
concessions and renegotiate the Council Agreement).  Her ministry had,
in spite of %(bt:unanimous cricicism from all parties in the
parliament), tried to push the agreement through today.

#llh: It is not yet clear %(sf:what will happen next).  With luck, the
Luxembourg presidency will take a less rigid line, and allow the text
to be reconsidered.  However, it is not impossible that today's text
could resurface again as an %(q:uncontroversial) A-item in the
Agricultural Council on the 10th of January, in the Economics and
Finance Council of the 18th, or in the Competitiveness Council in
March.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons041221 ;
# txtlang: pt ;
# multlin: t ;
# End: ;

