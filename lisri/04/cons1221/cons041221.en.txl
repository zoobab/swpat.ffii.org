<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2004-12-21 Software Patents Withdrawn from the Agenda of the Council
of Agriculture and Fisheries at Poland's Request

#descr: The Software Patent Directive has been withdrawn from the Agenda of
the Council of Agriculture and Fisheries.  Poland's Minister of
Science and Computerisation, Wlodzimierz Marcinski, firmly requested
the item be withdrawn from the agenda.  The Agriculture Commissioner
expressed regret, but the A-item was deleted and will not now be
adopted this year.

#Wrr: See Wiki Version for more

#Sto: Story

#iti: Few people had expected the appearance of the Polish %(tp|Vice
Minister of Science and Computerisation|literally %(q:State
Under-Secretary in the Ministry of Science and Informatisation)),
%(WM). Marcinski came personally to Brussels to present Poland's point
of view, because the permanent representatives of Poland at the EU had
been put under great pressure by the Dutch Presidency to accept the
text, rather than to communicate the wishes of the Polish government.

#aeu: After having been targeted by a %(dc:demo campaign), Germany's
minister of agriculture, Renate Künast of the Green Party, %(sk:stayed
away from the meeting).  Instead a diplomat from the German
representation in Brussels was sent with instructions to nod off the
text.  This openly showed a rift within the government.  Meanwhile the
German minister of justice %(bz:hinted a possible will to make
concessions and renegotiate the Council Agreement).  Her ministry had,
in spite of %(bt:unanimous cricicism from all parties in the
parliament), tried to push the agreement through today.

#llh: It is not yet clear %(sf:what will happen next).  With luck, the
Luxembourg presidency will take a less rigid line, and allow the text
to be reconsidered.  However, it is not impossible that today's text
could resurface again as an %(q:uncontroversial) A-item in the
Agricultural Council on the 10th of January, in the Economics and
Finance Council of the 18th, or in the Competitiveness Council in
March.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons041221 ;
# txtlang: en ;
# multlin: t ;
# End: ;

