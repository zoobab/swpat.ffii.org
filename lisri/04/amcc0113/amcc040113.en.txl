<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rebellion in Arlene McCarthy's constituency

#descr: Parts of the Labour party and electorate of Arlene McCarthy's district
are asking to have her replaced.  McCarthy responds by pointing to her
affiliation with the regional venture capital scene (= patent
movement) and claiming merits in bringing special EU funds to her
regions.

#rcW: Report from McCarthy's region

#kta: %(q:Our current MEP, Arlene McCarthy, has annoyed so many small
business people and creative folk with her pro-corporate attitude and
proved so unresponsive to her constituents on other issues, that I
think there's a real chance we can replace her with a true
representative of the North West.)  Ms McCarthy last night hit back at
the criticism. She said: %(q:I am actually endorsed by the Merseyside
Special Investment fund for my help with small businesses and have
done a lot in Europe to help small and medium enterprises.|It was my
report that secured the last Objective 1 funding for Liverpool and
Merseyside so I do not know where this criticism has come from
really.)

#shs: Compare this to McCarthy's response to criticism in the case of the
software patent directive.  The pattern is the same: claiming that she
brought in some helpful amendments, which in this case (see letter to
the Guardian) did not exist.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amcc040113 ;
# txtlang: en ;
# multlin: t ;
# End: ;

