<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: OECD ministers call for closer scrutiny of the patent system

#descr: The Paris based Organisation for Economic Cooperation and Development,
in which the world's wealthier countries are members, has issued a
report on problems with the patent system.  OECD calls for %(q:closer
scrutiny by science, technology and innovation policy makers) of
patent regimes, warning that governments %(q:must remain vigilant in
ensuring that patenting does not unnecessarily hinder access to
knowledge, reduce incentives to disseminate knowledge, or impede
follow-on innovation).  The report specially names software, business
method and gene patents as fields where the efficiency of the patent
system seems particularly questionable.

#iCW: Communique of the OECD Ministerial Meeting

#iWi: Scidev 2004/01 on OECD Patent Communique

#lrW: Reporting about the OECD report, the journalist calls for %(q:closer
scrutiny by science, technology and innovation policy makers) of
patent regimes, warning that governments %(q:must remain vigilant in
ensuring that patenting does not unnecessarily hinder access to
knowledge, reduce incentives to disseminate knowledge, or impede
follow-oninnovation).

#Rin: OECD Report on Science and Innovation

#eii: Conclusions drawn from a series of research activities and conferences
conducted by the Organisation for Economic Cooperation and Development
at the level of ministries of science, innovation and economics with
participation of many economists and patent lawyers.  While being
careful not to offend the faith of the patent professionals, the
report does cast doubt on the efficiency of the patent system,
particularly in the areas of genes, software and business methods.

#iss: The OECD ministerial meeting has issued a communique this weekend,
based on the %(rs:report on science and innovation).

#tte: About patents the ministers say:

#mhW: Patent regimes play an increasingly complex role in encouraging
innovation, diffusing scientific and technical knowledge, and
enhancing market entry and firm creation. As such, they should be
subject to closer scrutiny by science, technology and innovation
policy makers.

#WWi: In more detail,

#aWe: Adapting IPR regimes

#lif: Patenting has accelerated rapidly in the past decade, with the number
of patent applications filed in Europe, Japan and the United States
increasing by 40% between 1992 and 2002, from 600 000 to 850 000 per
year. The effects of such patenting on incentives to innovate, on the
diffusion of scientific and technical knowledge and on competition
remain unclear and vary across industry sectors and technological
fields. In this regard, Ministers welcomed the OECD report on
%(q:Patents and Innovation: Trends and Policy Challenges), and
encouraged continuation of OECD work in this area.

#Wem: Although not widespread, cases of restricted access to patented
inventions and delays in conducting or publishing research, indicate
that governments must remain vigilant in ensuring that patenting does
not unnecessarily hinder access to knowledge, reduce incentives to
disseminate knowledge, or impede follow-on innovation. Ministers
recognised the growing importance of patent licences and other
market-based transactions in fostering knowledge diffusion and agreed
that policy should encourage their development. Ministers further
shared the view that IPR regimes need to protect researchers' access
to fundamental inventions, such as through exemptions for research use
of patented inventions.

#erW: The more important patents become to economic growth and performance,
the more necessary it will be to ensure the quality of patents awarded
while minimising their overall costs to society. Ministers welcomed
the steps that a number of countries have already taken in that
direction, and agreed that good practices in this area should be
emulated. In this context, they encouraged the development of efforts
to forge closer co-operation among major patent offices towards a more
coherent global patent system.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: oecd040130 ;
# txtlang: en ;
# multlin: t ;
# End: ;

