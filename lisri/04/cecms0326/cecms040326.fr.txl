<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: L'UE renforce le monopole de Microsoft

#descr: Les procédures anti-concurrentielles de la Commission européenne
contre Microsoft ont débouché sur un verdict boostant fortement la
position de monopole de Microsoft sur le marché des OS et aident
Microsoft à étendre cette position sur d'autres marchés. Alors que la
Commission peut se targuer d'avoir gagné une somme substantielle en
imposant une amende ponctuelle, équivalente à 1% des réserves de
caisse de Microsoft, on peut voir entre les lignes de ce verdict un
feu vert donné à Microsoft pour tuer ses principaux concurrents sur le
marché des systèmes d'exploitation. Ce sous-entendu a été en même
temps renforcé par les négociations en coulisses du Groupe sur la
politique de brevets du Conseil de l'UE, dont des copies ont été
transmises à la FFII. Immédiatement après l'annonce, le cours boursier
de MSFT s'est élevé de 3%.

#WyT: Exceptions to Intellectual Property Rights: Lessons from WTO-Trips
Panels

#bor: Three law scholars explain how the European Commission has been
pressing for extreme interpretations of Art 30 TRIPs at the WTO
panels, partially with success.  E.g. the EU succeded in using TRIPs
to disallow internal stockpiling of pharmaceuticals by Canadian
generica manufacturers before the expiry of the patent term and to
disallow royalty-free use of musical works in the context of
smallscale events in the USA.  The authors conclude: %(q:WTO-TRIPS
panels are restrictively interpreting the restriction to IPR's. They
thereby confirm the monopolist strength of the IPR holder, whose
powers remains absolute in most respects. Given the stance of
WTO-TRIPS panels reinforcing the IPR holder/owner's position, WTO
contributes to stifling innovation in the digital economy by limiting
the exception for the public good ever further.)

#aWv: WTO complaint by EU 2000 against Canadian regulatory review exemption

#uyi: Canada's Patent Act allowed competitors to start product safety
testing and permission procedures and associated test production and
stockpiling during the last 1/2 year of the patent term.  The EU filed
a TRIPs complaint against this, saying that this made its pharma
industry lose 100 mn CAD per year, because it allowed generics
producers to enter the market as soon as the patent expired rather
than 2 years later.  The EU maintained that the Canadian provision
discriminated against a field of technology, thereby violating Art 27
TRIPs, and was not a limited and reasonable limitation in the sense of
Art 30.  The Canadian representative pointed out in detail why the
EU's interpretations of TRIPs were inconsistent with the letter and
spirit of TRIPs and extremely biased toward rightholders.  Yet the WTO
panel decided largely in favor of the EU.

#srn: Presseerklärung der Europäischen Union über Urteil im
Microsoft-Kartellverfahren

#Wtt: Microsoft darf eine %(q:faire Entschädigung) für jegliche Verwendung
seines %(q:geistigen Eigentums) durch Firmen, die mit
Microsoft-Produkten interoperieren müssen, verlangen.

#xmW: Pressetext 2004-02-25: EU-Kommissar interveniert für Microsoft

#tso: one of many superficial press reports about the intervention of
Bolkestein in favor of Microsoft in the Commission's procedings.  This
was later seconded by Lamy and accepted by Monti.  Bolkestein raised
concerns that imposition of duties with respect to Microsoft's
interfaces might unreasonably prejudice Microsoft's intellectual
property.  TRIPs Art 30 is not directly mentioned, but it was
mentioned orally by officials of the European Commission in
conversations with FFII as well as in public meetings.  It is well
known that the European Commission's vision of TRIPs is the main
reason of concern.  Lamy seconded because he is in charge of relations
with WTO.

#0sr: Heise 2004-02-25: EU-Kommissar interveniert für Microsoft

#rlp: The Register 2004-03-25: EK errichtet Zollschranken für Microsofts
Wettbewerber aus dem Open-Source-Lager

#Efi: berichtet, wie die Europäische Kommission Microsofts neuem
Patentstrategen Phelps dabei hilft, Projekten wie Samba die Luft
abzuschüren.

#uca: Am Tag nach der Ankündigung der Entscheidung der Europäischen
Kommission über die wettbewerbsbehindernden Methoden von Microsoft
stieg deren Aktienkurs um drei Prozent.

#Wfp: Während die Kommission eine einmalige Geldstrafe von 1% der
Microsoft'schen Bargeldreserve verhängte, beinhaltete diese
Entscheidung eine extreme Auslegung des TRIPS-Vertrages, die es
Microsoft erlaubt, einen a %(q:fairen Ausgleich) für die Benutzung
jedweder proprietärer Protokolle, für die sie in Europa Patente
erhalten haben, zu verlangen.

#eyn: Dies bedeutet, sofern die Ergänzungen des Europäischen Parlaments zu
der geplanten Software-Patent-Direktive vollständig akzeptiert werden,
daß Microsoft offiziell grünes Licht für die Beseitigung ihrer
Haupt-Wettbewerber von der Europäischen Kommission erhalten hat.

#hro: Wie %(TR) %(rr:reports) berichtet:

#eEh: Anstatt Microsoft zu bestrafen, hat die am Mittwoch bekanntgewordene
Entscheidung der Europäischen Kommission dieser Firma eine strahlende
Zukunft als eine Patent-Lizensierungs-Gesellschaft gesichert. XXX Der
folgende Satz ist ziemlich unklar:  Nach einem Vertreter wurden nur
zwei Open-Source-Fürsprecher vor der Untersuchung als Zeugen geladen.
Weil es Microsoft erlaubt sein wird, Lizenzgebühren für
veröffentlichte APIs zu verlangen, sagte Jeremy Allison, das Projekte
wie Samba, an deren Leitung er beteiligt ist, einer prohibitive Hürde
entgegensehen würden. Die Wettbewerber von Microsoft benutzen Samba,
um von Windows-Maschinen auf Datei- und Druckdienste zuzugreifen. Die
Untersuchung der Europäischen Kommission (XXX Gemeinschaft?) wurde von
Sun Microsystems, die durch Unterfangen wie "Projekt "Cascade"
versucht haben, in proprietärer Form das zu tun, was Samba und
ähnliche Projekte wie der "Evolution Exchange"-Client von Novell in
Form freier Software tun, angezettelt: Bereitstellung einer
kompatiblen und interoperabelen Infrastruktur, um mit der
Unternehmenssoftware von Microsoft zu konkurrieren. Die Wettbewerber
Microsofts haben sich zunehmend auf diese freien Softwareprojekte
verlassen. Aber die Entscheidung vom Mittwoch hat das
Anti-Microsoft-Lager unerwarteterweise gespalten, sagt Allison.
%(q:Die EU hatte eine wunderbare Gelegenheit, wurde aber zu gierig,)
sagte er uns gestern. %(q:Dies spaltet den Wettbewerb.)

#Cte: Håkon Wium Lie, der CTO (Technik-Vorstand) von %(OS), einer von
Microsofts wenigen verbleibenden Wettbewerbern im Browser-Markt, fügt
hinzu:

#sib: Es ist nie fair für irgendeine Firma, unabhängig von der Größe, von
den Wettbewerbern für die bloße Benutzung von Protokollen, die für die
Interoperabilität zwingend erforderlich sind, Geld zu verlangen. Die
einzige faire Entschädigung für eine solche Nutzung ist gar keine
Entschädigung. Deswegen haben wir beim World Wide Web Consortium (W3C)
entschieden, daß Web-Standards lizenzgebührenfrei zu sein haben.

#onh: Diese Position wurde auch von dem Europäischen Parlament in dem
erweiterten Artikel 6a über die Begrenzungen des Patentschutzes im
Hinblick auf die Interoperabilität ausgedrückt.

#san: Trotzdem kämpft die Europäische Kommission auf Basis der gleichen
fragwürdigen Interpretation des TRIPS-Vertrages gegen diesen Artikel
6a des Europäischen Parlaments, die zugunsten der Monopolinteressen
von Microsoft in diesem Wettbewerbsfall herangezogen wird.

#ceW: Ein Professor für Wettbewerbsrecht erklärte kürzlich bei einem Treffen
in der Nähe von Brüssel, was an der Interpretation von TRIPS durch die
Kommission nicht stimmt:

#cre: Artikel 30 des TRIPS-Vertrages sagt klar aus, daß die Ausschlußrechte,
die ein Patent gewährt, aufgrund von wettbewerbsrechtlichen
Überlegungen beschränkt werden können, wenn dies nicht die normale
Verwertung dieses Patents verhindert. Die normale Verwertung des
Patents ist, Leute dazu zu zwingen, durch verbesserte Lösungen statt
durch Imitation zu konkurrieren. Aber bei Kommunikations-Standards
gibt es keinen Grund, verbesserte Verfahren zu finden, Selbst, wenn
Ihre Sorte Esperanto performanter als Englisch ist, sind Sie gut
beraten, eine automatische Übersetzung von und nach Englisch
anzubieten, Ansonsten wird niemand Ihre Sprache nutzen, und Sie werden
nicht in der Lage sein, alle Ihre wundervollen Werke, die Sie in
dieser Sprache geschrieben haben, zu verkaufen. Wenn die Englische
Sprache patentiert wäre, könnten Leute nicht mehr auf der Basis der
Qualität ihrer Werke konkurrieren. Ähnliche wettbewerbsfeindliche
Situationen treten bei Softwarepatenten regelmäßig auf, und sie
verlangen nach einer generellen Lösung im Patentrecht und nicht nach
einer Fortschreibung des Wettbewerbsrechts von Fall zu Fall. Eine
generelle Ausnahme zugunsten der Interoperabilität würde das
betroffene Patent kein bischen weniger wertvoll als andere Patente
machen. Man könnte hierin genau die Art von Ausnahme erkennen, die der
Dreistufenteset gemäß Artikel 30 TRIPs erlaubt.  Glauben Sie nie
jemandem, der etwas über TRIPS sagt, ohne den Text zu prüfen!

#hal: These comments do not yet take into account the fact that the software
patents on which Microsoft's interface ownership attempts are based
have been granted against the letter and spirit of the European Patent
Convention and in fact of the TRIPs treaty itself, as is easy to see
and many law scholars have pointed out.

#nts: Die Kommission scheint fragwürdige Interpretationen von TRIPS unter
dem Druck von Frits Bolkestein, dem Kommissar für Binnenmarkt,
anzunehmen, der behauptete, daß die ursprünglichen Anforderungen zu
für Microsoft zu hart gewesen wären und zu Sanktionen seitens der WTO
geführt hätten, weil sie nicht durch den Artikel 30 von TRIPS
gerechtfertigt seien.

#Wto: Bolkesteins Direktorium hat (XXX Anm. des Übersetzers: schon seit
einiger Zeit, kommt auch der Semantik im Englischen am Nächsten) dem
Parlament gedroht und die Kommission falsch informiert, um extremen
Interpretationen von TRIPS zu fördern und der unbegrenzten
Patentierbarkeit von Software, einschließlich
%(q:computer-implementierten Geschäftsmethoden), in der EU den Weg zu
bereiten.

#row: Die von Bolkesteins Direktorium in diesem Prozeß verwendeten Texte
kamen vom Europäischen Patentamt (EPO) und von der Business Software
Alliance (BSA). Der Entwurf zur Softwarepatent-Direktive trug die
Handschrift derselben, der BSA und Microsoft nahestehenden Kreise, die
(XXX Anm. des Uebersetzers: auch) in einem  %(us:Dokument der
US-Regierung) auftauchte, die gegen den Artikel 6a des Europäischen
Parlaments (XXX Anm. des Übersetzers: gerichtet war), das Anfang
September 2003 von der US-Vertretung in Brüssel unter MdEPs in Umlauf
gebracht wurde.

#ior: Während Bolkestein von einem Schwarm von Lobbyisten der Großindustrie
und der Patent-Gemeinde jederzeit umschwärmt zu sein scheint, hat er
bislang noch. Die Generaldirektion Binnenmakrt hat immer ienseitig die
Interessen der  %(q:wirtschaftlichen Mehrheit) gefördert, also die
firmeneigenen Patentanwälte, die die relevanten Kommittees bei der 
EICTA und der UNICE beherrschen, ohne je zu erklären, warum die
Interessen der anderen Seite keines Schutzes wert sind. Es gibt seit
Jahren eine Kommunikationssperre.

#cif: Dies scheint sich nun langsam zu ändern. David Ellard, Nachfolger von
%(ah:Anthony Howard) und bei der Generaldirektion Binnenmarkt für das
Dossier "Software-Patent" verantwortlich, hat einer Teilnahme an der
%(ci:Konferenz über Softwarepatente), die der FFII innerhalb des
Europäischen Parlaments für den 14.ten April organisiert hat,
zugestimmt.

#iin: Der Rat der Europäischen Union hat bei seiner Sitzung am 2004-03-02
wieder einmal seine Fehlinterpretation von Artikel 30 TRIPS bestätigt.
Eine Eingabe Luxemburgs zugunsten einer Variation des Art 6a wurde
abgelehnt. Stattdessen stimmte die Patent-Arbeitsgruppe des Rates für
eine Änderung, die besagt, daß Wettbewerbsprobleme nicht innerhalb des
Patentrechts, sondern nur fallweise aufgrund von Kartellverfahren wie
das gerade anhängige gegen Microsoft, gelöst werden dürfen. Durch
diese Erweiterung seines vorherigen %(sp:Arbeitsdokuments) hat der Rat
seine Position als der kompromißlose Fürsprecher extremer
Patentierbarkeit und extremen Patentschutzes und seiner
Durchsetzbarkeit noch weiter gestärkt. Diese Entwicklung geschieht
ebenfalls auf %(di:Betreiben von Bolkesteins Direktorium).

#rbn: The Canadian representative found clear words about the EU's methods
of interpretation of TRIPs:

#scd: According to the EC, Canada's arguments were based on misconceptions
about patent rights. It seemed apparent though that the EC's
argumentation flowed from the misconception that Article 30 was an
%(q:inutility). In other words, the EC did not accept that patent
rights could be limited in any way, as was evident from its assertion
that Canada's measures unreasonably prejudiced the legitimate
interests of the patent owner because the legitimate interests of a
patent owner could only be the full enjoyment of all his patent rights
during the entire patent term. This left no room for any prejudice at
all to a patent holder's interests, and equated %(q:normal
exploitation) with %(q:unfettered exploitation), notwithstanding
Article 30's unequivocal acceptance of reasonable prejudice. Again,
the failure to accord any meaning to plain words was a fundamental
error of treaty interpretation.

#ant: It may be worth noting that Bolkestein, as a pharma man, may be
somewhat more interested than others in reinforcing the EU's extreme
stance on Art 27 and 30 TRIPs taken in the WTO patent case against
Canada.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cecms040326 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

