<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: UE Apoia Monopólio da Microsoft

#descr: Os procedimentos competitivos da Comissão Europeia contra a Microsoft
levaram a um veredicto que apoia fortemente a posição monopolista da
Microsoft no mercado dos sistemas operativos e ajuda-a a expandir esta
posição a outros mercados. Embora a Comissão possa ter ganho fundos
substancialmente com a imposição de uma multa de 1% das reservas em
dinheiro líquido da Microsoft, as letras pequenas do veredicto dão luz
verde à Microsoft para matar os seus principais concorrentes no
mercado dos sistemas operativos. Estas letras pequenas foram
simultâneamente reforçadas nas negociações nos bastidores do grupo de
trabalho de Legislação de Patentes do Conselho, cujas cópias de
documentos chegaram às mãos da FFII. Imediatamente após o anúncio, o
valor de bolsa da MSFT aumentou cerca de 3%.

#WyT: Excepções a Direitos de Propriedade Intelectual: Lições dos Paineis
OMC-TRIPs

#bor: Três professores de lei explicam como a Comissão Europeia tem estado a
pressionar por interpretações extremistas do Artigo 30 do tratado
TRIPs nos paineis da OMC, com sucesso parcia. Por exemplo, a UE
conseguiu utilizar o TRIPs para impedir o armazenamento de
farmacêuticos antes do prazo de expiração da patente, da parte de
fabricantes canadianos de genéricos, e conseguiu impedir a utilização
sem royalties de obras musicais no contexto de eventos de pequena
escala nos EUA. Os autores concluem %(q:Os paineis OMC-TRIPs estão a
interpretar de forma restrictiva as restrições de direitos de PI.
Dessa forma, eles confirmam a força monopolista do detentor de
direitos, cujos poderes continuam absolutos na maioria dos seus
aspectos. Dada a posição dos paineis da OMC-TRIPs de reforçar a
posição do detentor/dono dos direitos, a OMC contribui para fazer
estagnar a inovação na economia digital ao limitar ainda mais a
excepção para o bem público.)

#aWv: WTO complaint by EU 2000 against Canadian regulatory review exemption

#uyi: Canada's Patent Act allowed competitors to start product safety
testing and permission procedures and associated test production and
stockpiling during the last 1/2 year of the patent term.  The EU filed
a TRIPs complaint against this, saying that this made its pharma
industry lose 100 mn CAD per year, because it allowed generics
producers to enter the market as soon as the patent expired rather
than 2 years later.  The EU maintained that the Canadian provision
discriminated against a field of technology, thereby violating Art 27
TRIPs, and was not a limited and reasonable limitation in the sense of
Art 30.  The Canadian representative pointed out in detail why the
EU's interpretations of TRIPs were inconsistent with the letter and
spirit of TRIPs and extremely biased toward rightholders.  Yet the WTO
panel decided largely in favor of the EU.

#srn: Press relese da CEC sobre a decisão de concorrência à Microsoft

#Wtt: diz que a Microsoft tem o direito a %(q:justa remuneração) em qualquer
utilização da sua %(q:propriedade intelectual) da parte de companhias
que necessitem de interoperar com a Microsoft.

#xmW: Pressetext 2004-02-25: EU-Kommissar interveniert für Microsoft

#tso: one of many superficial press reports about the intervention of
Bolkestein in favor of Microsoft in the Commission's procedings.  This
was later seconded by Lamy and accepted by Monti.  Bolkestein raised
concerns that imposition of duties with respect to Microsoft's
interfaces might unreasonably prejudice Microsoft's intellectual
property.  TRIPs Art 30 is not directly mentioned, but it was
mentioned orally by officials of the European Commission in
conversations with FFII as well as in public meetings.  It is well
known that the European Commission's vision of TRIPs is the main
reason of concern.  Lamy seconded because he is in charge of relations
with WTO.

#0sr: Heise 2004-02-25: EU-Kommissar interveniert für Microsoft

#rlp: The Register 2004-03-25: CE ergue portagens aos rivais opensource da
Microsoft

#Efi: relata como a Comissão Europeia está a ajudar o novo estratega de
patentes da Microsoft, Phelps, a matar projectos como o Samba.

#uca: No dia seguinta ao anúncio da decisão da Comissão Europeia sobre as
práticas anti-concorrênciais da Microsoft, o valor das acções em bolsa
desta subiram cerca de 3 por cento.

#Wfp: Embora a Comissão imponha uma única multa de 1% das reservas líquidas
de dinheiro da Microsoft, a decisão sugere implicitamente
interpretações extremistas do tratado TRIPs, pelas quais a Microsoft
estaria autorizada a cobrar uma %(q:justa remuneração) pelo usa de
qualquer protocolo proprietário sobre o qual detenha patentes na
Europa.

#eyn: Isto significa que, a menos que as emendas do Parlamento Europeu à
proposta de directiva sobre patentes de software sejam completamente
aceites, a Microsoft terá obtido luz verde da Comissão Europeia para
matar os seus principais concorrentes.

#hro: Tal como o The Register %(rr:relata):

#eEh: Longe de penalizar a Microsoft, a decisão da Comissão Europeia de
Quarta-feira passada assegura um futuro brilhante para a companhia
como operação de licenciamento de patentes, de acordo com um
representante apenas dois interessados no opensource foram chamados a
testemunhar pertante a investigação. Porque será permitido à Microsoft
cobrar royalties pelas APIs que publicar, Jeremy Allison diz que
projectos tal como o Samba, que ele co-lidera, podem ter sarilhos
proibitivos. Os concorrentes da Microsoft utilizam software como o
Samba para aceder a serviços de partilha de ficheiros e impressoras em
máquinas Windows. Que o Samba e projectos similares como o cliente de
Exchange para Evolution da Novell fazem de uma forma livre:
providenciam uma infraestrutura compatível e interoperável para
competir com o software empresarial da Microsoft. Os rivais da
Microsoft estão a dependender crescentemente destes projectos de
Software Livre. Mas a decisão de Quarta-feira   divide inesperadamente
o campo anti-Microsoft, diz Allison. %(q:A UE teve uma oportunidade
maravilhosa mas tornou-se avarenta) disse-nos ontem. %(q:Isto divide a
concorrência.)

#Cte: Håkon Wium Lie, CTO da Opera Software, um dos poucos restantes
concorrentes da Microsoft no mercado de browsers, acrescenta:

#sib: Nunca é justo que uma companhia, não importa o seu tamanho, cobre à
concorrência pela utilização de protocolos que são necessários para a
interoperabilidade. A única remuneração justa por tal uso é nenhuma
remuneração. É por isso que no World Wide Web Consortium decidimos que
os standards da web têm de ser sem royalties.

#onh: Esta posição também foi afirmada pelo Parlamento Europeu no emendado
Artigo 6a sobre os limites à aplicação de direitos de patentes no que
diz respeito à interoperabilidade.

#san: Contudo, a Comissão Europeia está a lutar contra o Artigo 6a do
Parlamento Europeu, baseada nas mesmas interpretações questionáveis do
tratado TRIPs que utilizou para fomentar ainda mais os interesses
monopolistas da Microsoft no caso da concorrência.

#ceW: Reinier Bakels, académico de lei do Instituto Para Lei em Informática
de Amsterdão e co-autor de um estudo sovre patentes de software
encomendado pelo Parlamento Europeu em 2002, comenta:

#cre: Nunca acredite em ninguém que diga algo sobre o TRIPs sem verificar o
texto! O Artigo 30 do TRIPs declara que os direitos de exclusão
conferidos or uma patente podem ser limitados por considerações
concorrenciais desde que isto não previsa a utilização normal da
patente. O significado da maioria das provisões do TRIPs é abstracto e
aberto a interpretação, mas eu não penso que muita gente vá concordar
que os interfaces de software deveriam ser apropriáveis, muito menos
que %(q:o uso normal) de patentes seja torná-los apropriáveis. Em vez
disso, a apropriação de interfaces leva a efeitos anti-concorrênciais
o que apela a uma solução sistemática na lei de patentes, de acordo
com o teste de 3 iterações do Artigo 30 do TRIPs. O Artigo 6a das
emendas do Parlamento Europeu à directiva de patentes de software é um
exemplo de uma solução nestas linhas. A Comissão Europeia ainda não
explicou detalhadamente porque pensa o contrário e como interpreta o  
Artigo 30 do TRIPs. Em vez disso, a Comissão demonstra um padrão
regular de utilizar artigos não interpretados do TRIPs para fomendar
medo, incerteza e desconfiança de forma vantajosa a certos interesses
anti-concorrenciais no campo do software.

#hal: Estes comentários não levam ainda em conta o facto de que as patentes
de software sobre as quais se baseiam as tentativas de apropriação da
Microsoft foram concedidas contra a letra e espírito da Convenção
Europeia de Patentes e, de facto, do próprio tratado TRIPs, tal como é
fácil e ver e como muitos académicos de lei o têm apontado.

#nts: A Comissão aparenta ter adoptado as suas interpretações
anti-concorrenciais do tratado TRIPs sob pressão de %(FB), o
comissário do directorado para o mercado interno, que tinha protestado
que as exigências de interoperabilidade planeadas originalmente eram
demasiado duras para a Microsoft e levariam a sanções da OMC porque
não eram justificáveis sob o Artigo 30 do TRIPs.

#Wto: O directorado de Bolkestein tem ameaçado o Parlamento e desinformado o
Conselho de forma a promover interpretações extremistas do TRIPs e a
pavimentar o caminho para a patenteabilidade ilimitada do software na
UE, incluindo %(q:modelos de negócio implementados em computador).

#row: Os textos que o directorado de Bolkestein tem utilizado neste processo
vieram o Gabinete Europeu de Patentes (EPO -- European Patent Office)
e da Business Software Alliance. A proposta de directiva de patentes
de software trazia a mesma escrita de certos círculos próximos da BSA
e da Microsoft que apareceram %(us:num documento do governo Norte
Americano) contra o Artigo 6a do Parlamento Europeu que circulou pelos
Eurodeputados via a representação dos EUA em Bruxelas a inícios de
Setembro de 2003.

#ior: Enquanto que Bolkestein aparenta estar constantemente rodeado de um
enxame de lobby-istas da comunidade de patentes e dos grandes
negócios, até agora não se encontrou com nenhum representanto do campo
dos críticos da sua agenda de patentes de software. o Directorado
Geral do Mercado Interno tem sempre favorecido unilateralmente os
interesses da %(q:maioria económica), i.e. os advogados corporativos
de patentes que dominam os comités relevantes na EICTA e UNICE, sem
nunca explicarem o porque é que os interesses do outro lado não são
merecedores de protecção. Tem havido um bloqueio comunicativo há
vários anos.

#cif: Contudo, isto pode estar a mudar lentamente. David Ellard, sucessor de
%(ah:Anthony Howard), responsável pelo dossier de patentes de software
do DG Mercado Interno, concordou participar numa %(ci:conferência
sobre patentes de software) organizada pela FFII dentro do Parlamento
Europeu a 14 de Abril. Este diálogo seria o primeiro deste género
desde o início dos planos de directiva da Comissão em 1997.

#iin: O Conselho da União Europeia reconfirmou novamente, na sua sessão de
2004-03-02, a sua insistência em interpretar de forma extremista o
Artigo 30 do TRIPs. Uma moção de Luxemburgo a favor de uma variante do
Artigo 6a foi rejeitada. Em vez disso, o grupo de trabalho sobre
patentes do Conselho optou por um recital onde diz que os problemas
concorrências não devem ser resolvidos no âmbito do direito de
patentes mas caso a caso através de procedimentos concorrênciais tal
como este último contra a Microsoft. Com esta extensão ao seu
%(sp:documento de trabalho) anterior, o Conselho reforçou ainda mais a
sua posição como promotor irredutível da patenteabilidade extrema e
aplicação extrema de patentes na União Europeia. Este movimentos do
Conselho também aconteceram, pelo menos parcialmente, sob
%(di:instigação do directorado de Bolkestein).

#rbn: The Canadian representative found clear words about the EU's methods
of interpretation of TRIPs:

#scd: According to the EC, Canada's arguments were based on misconceptions
about patent rights. It seemed apparent though that the EC's
argumentation flowed from the misconception that Article 30 was an
%(q:inutility). In other words, the EC did not accept that patent
rights could be limited in any way, as was evident from its assertion
that Canada's measures unreasonably prejudiced the legitimate
interests of the patent owner because the legitimate interests of a
patent owner could only be the full enjoyment of all his patent rights
during the entire patent term. This left no room for any prejudice at
all to a patent holder's interests, and equated %(q:normal
exploitation) with %(q:unfettered exploitation), notwithstanding
Article 30's unequivocal acceptance of reasonable prejudice. Again,
the failure to accord any meaning to plain words was a fundamental
error of treaty interpretation.

#ant: It may be worth noting that Bolkestein, as a pharma man, may be
somewhat more interested than others in reinforcing the EU's extreme
stance on Art 27 and 30 TRIPs taken in the WTO patent case against
Canada.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: rmseabra ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cecms040326 ;
# txtlang: pt ;
# multlin: t ;
# End: ;

