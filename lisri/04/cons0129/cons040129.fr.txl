<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: %(q:Compromis) du Conseil de l'UE pour une brevetabilité illimitée

#descr: Le Conseil des ministres de l'Union européenne, actuellement présidé
par l'Irlande, fait circuler un document de travail avec des
contre-propositions aux amendements du Parlement européen. Contrastant
avec la version du Parlement européen, la version du Conseil autorise
une brevetabilité illimitée et l'applicabilité des brevets.  D'après
la version du Conseil, le %(q:One Click shopping) d'Amazon est sans
l'ombre d'un doute une invention brevetable, la publication de
programmes sur un serveur constitue déjà une infraction et
l'utilisation de formats de fichier brevetés dans un but
d'interopérabilité n'est pas autorisée.  Puisque la procédure de
décision du Conseil est secrète, on ne sait pas qui appuie cette
proposition au nom de quel gouvernement mais il est bien connu que le
groupe de travail responsable est composé de fonctionnaires des
bureaux des brevets nationaux et de gens proches de ce groupe qui ont
également un siège côte à côte au cons  eil administratif de l'Office
européen des brevets.

#qii: Le %(q:document de compromis de la présidence du Conseil)

#msi: Position de compromis ?

#ehP: Intérêts derrière la position du Conseil

#media: Contacts media

#ffii: À propos de la FFII

#dokurl: URL permanente de ce communiqué de presse

#Wom: Le %(q:Groupe de travail sur %(tp|la Propriété intellectuelle|les
brevets)) du Conseil de l'UE a %(cm:planifié) de se réunir le 2 mars
pour discuter des %(ep:amendements du Parlement européen à la
directive sur les brevets logiciels).

#rWt: Le document de travail du Conseil supprime tout ce qui dans les
amendements du Parlement limite la brevetabilité, y compris :

#WWb: Toutes les définitions de %(q:technique), %(q:invention) and
%(q:industriel) sont supprimées alors que la brevetabilité n'est
toujours limitée par rien d'autre que ces termes.

#maW: La liberté de publication est supprimée de l'article 5 ; à la place,
les biens informationnels deviennent directement revendicables, les
fournisseurs d'accès à internet peuvent ainsi être poursuivis pour
avoir permis la publication sur leur serveur de programmes développés
de manière indépendante.

#een: L'article 6a (liberté d'interopération) est supprimé.  Avec ceci,
Microsoft obtient le feu vert pour sa nouvelle politique de license
sur les formats de fichier et les grosses entreprises informatiques et
de télécommunicatons peuvent faire payer pour l'utilisation de la
prochaine génération de standards Internet.

#tri: Quelques amendements cosmétiques de députés pro-OEB, tels qu'Arlene
McCarthy et Joachim Wuermeling, sont repris dans le document de
travail.  Parmis ces amendements, certains avaient été rejetés par le
Parlement européen lors du vote en plénière.

#kWp: Jonas Maebe, porte-parole de la FFII en Belgique, commente le
%(q:compromis) de la présidence irlandaise :

#nWn: C'est comme si dans le débat sur l'acceptation ou le refus que de
nouveaux états membres rejoignent l'UE, le compromis etait de ne pas
accepter et de leur imposer en outre des sanctions économiques.

#wWt: C'est comme si dans le débat sur l'augmentation ou non des limitations
de vitesse sur les routes, le compromis était de les augemnter et de
supprimer en outre l'obligation d'attacher sa ceinture de sécurité.

#rWo: Ce qu'est un compromis et ce qui ne l'est pas devrait s'éclaircir si
nous rangeons les différentes positions sur une échelle de valeurs :

#nme: rang

#oii: position

#uoe: partisans

#Wnt: pas de brevets du tout

#nnr: nombre d'%(es:économistes) mais aucun groupe d'intérêt concernés par
le débat qui nous occupe

#nhe: aucun brevet sur quoique ce soit qui touche d'une manière ou d'une
autre au logiciel

#WsW: Certains acteurs imaginaires comme la %(fs:Free Software Alliance)
d'Arlene McCarthy et les %(ms:%(q:quelques gros vendeurs de logiciels)
dépeints par l'avocat des brevets Dr. Kay Brandt dans un magazine pour
les employés de Siemens).

#oen: aucun brevet sur les logiciels purs, garantie de la liberté de
publication et de l'interopérabilité

#pak: Parlement européen le 24-09-2004

#tto: brevetabilité illimitée, applicabilité limitée

#mW2: Commission le 20-02-2002

#R0k: Commission parlementaire JURI le 17-06-2003

#tyo: brevetabilité illimitée, applicabilité illimitée

#tbC: Il faut noter que le Conseil a été poussé dans cette position
extrêmiste par la Commission, comme on peut le constater dans %(cp:un
document divulgué de la Commission datant de novembre 2003).

#dhl: Ce document est rempli de fausses déclarations désobligeantes sur le
travail du Parlement européenn Parliament et il montre entre les
lignes que le fonctionnaire de la Commission pour la PI (propriété
intellectuelle) aurait aimé adopter également les revendications de
programmes.

#Wah2: Dans le même temps, les avocats en brevets des grosses entreprises des
TIC impliquent leurs PDG dans une campagne par lettre écrite pour
discréditer le vote du Parlement européen aux yeux des chefs d'état et
même du public dans son ensemble.

#eth: Les lettres des PDG et les affirmations des avocats en brevets des
sociétés accompagnant ces lettres, tentent de répandre la peur que les
amendements des parlementaires européens empêcheraient de breveter les
procédés dans les domaines techniques traditionnels, tels que les
robots industriels, etc. dès lors qu'un ordinateur est employé pour
les contrôler et qu'une telle approche restrictive découragerait
sérieusement l'innovation.   Ces deux craintes sont sans fondement. 
La position du Parlement est parfaitement conforme à la fameuse
décision %(ab:Freinage non bloquant %(pe:Anti Blocking System, ABS))
de la Cour fédérale allemande.  Elle autorise des brevets sur tout
procédé apportant %(tp|une nouvelle connaissance empirique sur les
forces de la nature|ex. les relations entre la temperature et les
frottements dans les freins d'automobile), indépendamment du fait
qu'un tel procédé soit contrôlé par une ordinateur ou non.  Le travail
d'un ingénieur   des procédés est brevetable, tandis que celui d'un
programmeur tombe sous le droit d'auteur.  Le Parlement a voté pour la
position très consensuelle que les PDG affirment vouloir.

#eoe: Le problème est que les avocats en brevets des sociétés, qui sont
derrière ces PDG, veulent breveter bien plus que le consensus général
ne le permet.  Les lecteurs attentifs auront aisément remarqué qu'ils
ne réclament pas de brevets sur les robots contrôlés par ordinateur ni
sur les procédés industriels mais la légalisation de 30000 brevets
dans le style des brevets américains sur les algorithmes et les
méthodes pour l'exercice d'activités économiques, avec des
revendications en termes d'équipement conventionnel générique de
traitement de données, ce que l'Office européen des brevets a accordé
ces dernières années, en contradiction avec la lettre et l'esprit de
la loi écrite.

#oem: Les partisans pro-brevets sont surtout les chefs des services de
brevets des grosses sociétés.  Grâce à la pratique inflationniste des
offices de brevets, ils ont pris de l'importance ces dernières années
et se battent pour accroître cette importance.  Les bras pro-brevets
de la Commission européenne et des gouvernements nationaux partagent
les intérêts de ces partisants et ont réussi de manière égale à
dominer la politique de brevets de leurs institutions respectives. 
Depuis que le projet de directive de l'UE sur les brevets logiciels a
débuté en 1997, ce mouvement très organisé et très retranché a appuyé
dans le sens d'une brevetabilité illimité en semant une propagande au
nom des hautes autorités (gouvernements, associations, PDG), espérant
apparemment que cela obligerait les légisateurs à obéir sans se poser
de questions.

#ine: Pendant ce temps, un groupe de parlementaires, des entreprises
informatiques et des associations ont publié un %(ca:Appel à l'action)
dans lequel ils critiquent les tentatives du mouvement pro-brevets de
%(q:tromper et d'intimider le Parlement Européen) et appellent les
députés nationaux à formuler leurs politiques nationle de brevets et à
superviser les activités des experts en brevets de leur gouvernement
au Conseil, afin de s'assurer de leur loyauté à servir leurs
politiques nationales.

#vmp: En fait, c'est une pratique commune des experts en brevets au Conseil
que d'ignorer ou de donner une interprétation erronée même avec des
instructions écrites de leurs ministres.  De plus, le groupe de
travail %(q:Brevets) du Conseil offre un voile d'anonymat sous lequel
les représentants des politiques nationales de brevets peuvent prendre
le point de vue qu'ils veulent sans en être tenus pour responsables.
Par exemple, en Allemagne, le gouvernement allemand déclare qu'ils
sont une force de modération au Conseil et qu'on les presse sur les
brevets logiciels.  D'après d'autres sources, nous savons que les
représentants allemand ont fait pression pour des revendications de
programme, i.e. pour la forme la plus radicale de la brevetabilité des
logiciels.  Mais une infime partie de tout ceci peut être prouvée car
même les membres du parlement n'ont pas accès aux négociations du
Conseil.  Dans la mesure où les protocoles écrits des sessions du
Conseil sont   accessibles sans détail les positions individuelles des
gouvernements deviennent anonymes.  De plus, les documents de travail
sont gardés confidentiels.  Ainsi, les parlements nationaux ne peuvent
plus connaître, et encore moins influencer, les processus législatifs
qui affectent le plus profondément les intérêts des citoyens de leur
nation.

#oWW: Plus de contacts peuvent être fournis sur simple demande

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040129 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

