<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rats-%(q:Kompromiss) für Unbegrenzte Patentierbarkeit

#descr: Der Rat der Minister der Europäischen Union, momentan unter der
Präsidentschaft von Irland, verteilt ein Arbeitspapier mit
Gegenvorschlägen zu den Änderungen des Europäischen Parlaments am
Softwarepatent-Richtlinienentwurf.  Im Gegensatz zur Version des
Europäischen Parlaments erlaubt die Version des Rates unbegrenzte
Patentierbarkeit und Patentdurchsetzbarkeit.  Nach der Version des
Rates sind %(q:computer-implementierte) Algorithmen und
Geschäftsmethoden, wie sie bereits in großer Zahl vom Europäischen
Patentamt gegen den Buchstaben und Geist des Gesetzes erteilt wurden,
allesamt patentierbare Erfindungen. Die Veröffentlichung einer
Beschreibung einer patentierten Idee auf einem Webserver in einer
formalen Sprache stellt eine Patentverletzung dar und die Verwendung
von patentierten Protokollen oder Dateiformaten zum Zweck der
Interoperabilität ist nicht erlaubt. Dieser Vorschlag wurde hinter
verschlossenen Türen von der Patentarbeitsgruppe des Rates
vorbereitet, einer Gruppe von Patentrechtlern die das Europäische
Patentamt betreiben. Die Patentarbeitsgruppe wurde in ihrem Streben
nach unbegrenzter Patentierbarkeit durch eine Briefkampagne an
hochgestellte Politiker ermutigt, im Rahmen derer die Geschäftsführer
einiger der meistpatentierenden Unternehmen Europas die
herabwürdigende Falschaussagen ihrer Patentanwälte über das
Parlamentsvotum unterstützen.

#qii: The %(q:Council Presidency Compromise Paper)

#msi: Compromise Position?

#ehP: Interests behind the Council's Position

#media: Media contacts

#ffii: About the FFII

#dokurl: Permanent URL of this Press Release

#Wom: The EU Council's %(q:Working Party on %(tp|Intellectual
Property|Patents)) is %(cm:scheduled) to meet on March 2nd to discuss
the %(ep:European Parliament's amendments to the software patent
directive).

#rWt: The discussion is based on a confidential %(wd:working document)
titled %(q:Council Presidency Compromise Paper). This proposal removes
everything from the Parliament's amendments that limits patentability,
including:

#WWb: All definitions of %(q:technical), %(q:invention) and %(q:industrial)
are removed, and yet patentability is limited by nothing but these
terms.

#maW: Freedom of publication is removed from Art 5, instead information
goods become directly claimable, so that internet service providers
can be sued for allowing publication of independently developped
programs on their server.

#een: Art 6a (freedom of interoperation) is removed.  This way Microsoft
receives green light for its new licensing program on file formats,
and large software and telecommunication companies can charge fees for
the use of the next generation of Internet standards.

#tri: A few cosmetic amendments by EPO-friendly MEPs such as Arlene McCarthy
and Joachim Wuermeling are admitted into the draft.  Among these are
amendments which were rejected by the European Parliament in the
plenary vote.

#kWp: Jonas Maebe, speaker of FFII in Belgium, comments on the Irish
presidency's %(q:compromise):

#nWn: It's as if in the debate on whether or not we should allow new member
states to join the EU, the compromise would be to not allow them, and
in addition to impose trade sanctions on them.

#wWt: It's as if in a debate on whether or not we should raise the speed
limits on the roads, the compromise would be to raise them and
additionally remove the requirement to wear seat belts.

#rWo: What is a compromise and what not should become clearer if we put the
various proposals on a scale:

#nme: number

#oii: position

#uoe: supporters

#Wnt: no patents at all

#nnr: many %(es:economists) but no organised stakeholders in the current
debate

#nhe: no patents on anything that somehow touches software

#WsW: Certain imaginary players such as Arlene McCarthy's %(fs:Free Software
Alliance) and the %(ms:%(q:few large software distributors) painted by
patent attorney Dr. Kai Brandt in a magazine for Siemens employees).

#oen: no patents on pure software, guarantee freedom of publication and
interoperation

#pak: European Parliament 2003-09-24

#tto: unlimited patentability, limited enforcability

#mW2: Commission 2002-02-20

#R0k: JURI 2003-06-17

#tyo: unlimited patentability, unlimited enforcability

#tbC: It should be noted that the Council was pushed toward its extreme
position by the Commission, as can be seen from a leaked
%(cp:Commission's paper of 2003/11).

#dhl: This paper is full of untrue derogatory statements about the European
Parliament's work, and it shows between the lines that the
Commission's IP official would have liked to opt for program claims as
well.

#Wah2: At the same time, the patent lawyers of large ICT companies are
involving their CEOs in a letter-writing campaign to discredit the
European Parliament's vote in the eyes of political decisionmakers.

#eth: The CEO letters and accompanying statements by the corporate patent
lawyers attempt to spread fears that the EP amendments would prevent
patenting of processes in traditional technical fields, such as
industrial robots etc, as soon as a computer is used to control them,
and that such a restrictive approach to patentability would seriously
discourage innovation.   Both fears are groundless.  The Parliament's
position is well in line with the famous %(ab:Anti Blocking System)
decision of the German Federal Court.  It allows patents on any
process that imparts %(tp|new empirical knowledge about forces of
nature|e.g. relations of temperature and friction in automobile
brakes), regardless of whether such a process is computer-controlled
or not.  The work of the process engineer is patentable, while that of
the programmer falls under copyright.  The Parliament has voted for
the very consensus position which the CEOs say they want.

#eoe: The trouble is that the corporate patent lawyers behind these CEOs
want to patent much more than what the general consensus allows. 
Attentive readers easily notice that they are not asking for patents
on computer-controlled robots or industrial processes, but for
legalisation of 30000 US-style patents on algorithms and business
methods, claimed in terms of conventional general-purpose
data-processing equipment, which the European Patent Office has
granted against the letter and spirit of the written law in recent
years.

#oem: The pro-patent campaigners are mainly managers of large corporate
patent departments.  Thanks to an inflationary practise of patent
offices, they have gained great importance in recent years and are
fighting to increase this importance.  The patent arms of the European
Commission and national governments share the interests of the
campaigners and have been equally successful in dominating the patent
policy of their respective bodies.  Ever since the EU software patent
directive project started in 1997, this well-organised and
well-entrenched movement has pushed for unlimited patentability by
spreading propaganda in the name of big authorities (governments,
associations, CEOs), apparently hoping that these will compel
legislators to obey without asking any questions.

#ine: Meanwhile, a group of parlamentarians, software companies and
associations has published a %(ca:Call for Action) in which it
criticises the attempts of the patent movement to %(q:deceive and
intimidate the European Parliament) and calls on national MPs to
formulate their national patent policies and to supervise the
activities of their government's patent experts in the Council, so as
to make sure that they loyally serve their national policies.

#vmp: It is common practise of the patent experts in the Council to act on
their own, ignoring or circumventing any instructions from their
ministers even in the rare case that such instructions exist.  The
Council's Patent Working Party moreover provides a veil of anonymity
where national patent policy representatives can take any point of
view they like without ever being held accountable.  E.g. in Germany
the german government claims that they are a moderating force in the
Council and software patents are being pushed on them.  From other
sources we know that the german representatives have been pushing for
program claims, i.e. for the most radical form of software
patentability.  But very little of all this can be proven, because
even members of parliament have no access to the negotiations of the
Council.  As far as written protocols of the Council sessions are
accessible at all, the positions of the individual governments are
anonymised.  The working papers are moreover kept confidential.  Thus
national parliaments can not know about, let alone influence,
legislative processes that are deeply affecting the interests,
including basic freedoms, of their nation's citizens.

#oWW: More Contacts to be supplied upon request

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: etschert ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040129 ;
# txtlang: de ;
# multlin: t ;
# End: ;

