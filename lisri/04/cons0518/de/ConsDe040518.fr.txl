<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: L'Allemagne : le cheval de Troie de Bolkestein au Conseil ?

#descr: Si l'Allemagne avait tenu sa promesse d'au moins s'abstenir, les 37 votes nécessaires pour bloquer la proposition auraient été atteints et même dépassés avec le vote de pays comme la Pologne qui avaient apparemment reçu comme consigne de suivre les Allemands.

#het: Les délégués italien et danois ont averti que les amendements du compromis du délégué allemand étaient dénués de sens. Pourtant, la Pologne et la Lettonie ont suivi Bolkestein et l'Allemagne, sur la position de laquelle ils avaient basé leur propre opposition. Sans ce leadership de l'Allemagne, Bolkestein aurait pu échouer à obtenir une majorité qualifiée au Conseil.

#Wrt: Le 12 mai et les jours suivants, le gouvernement allemand avait promis de s'opposer au document du Conseil ou au moins de s'abstenir. Il s'était lui-même posé en chef de file de l'opposition contre le Conseil.

#tWe: La ministre allemande en charge du dossier, Brigitte Zypries, a publié un article aujourd'hui en ligne sur Heise dans lequel elle affirme que sa délégation a obtenu des améliorations significatives empêchant les brevets triviaux. Il n'y a pas la moindre once de vérité dans ces affirmations.

#mWq: Tout ce qu'a obtenu le gouvernement allemand est d'insérer dans l'article 2ter, l'obligation qu'une %(q:contribution technique) ne soit pas seulement %(e:non évidente) mais également %(e:nouvelle). Zypries en a fait l'éloge comme étant un garde-fou efficace contre les brevets étendus et triviaux.

#ena: Pendant ce temps, le responsable ministériel de la délégation allemande, Hansjoerg Geiger, a rapporté à diverses chaînes d'informations que le gouvernement était contre les brevets étendus et triviaux. Il est de notoriété publique que les régulations mêmes que le gouvernement allemand a contribué à pousser contre le Parlement européen produisent systématiquement des tonnes de brevets étendus et triviaux. Voir

#Wto: Pourquoi le brevet %(q:Amazon One Click Shopping) est brevetable selon la proposition de la commission JURI et du Conseil

#oeW: Pourquoi les brevets sont si triviaux

#ove: Zypries est quelque peu sous pression à cause d'un accord de coalition entre les Sociaux-démocrates et les Verts, qui stipule explicitement que le secteur du logiciel devrait être gouverné par le droit d'auteur et non par les brevets. L'action de l'actuel gouvernement allemand peut difficilement se comprendre, quelle que soit l'interprétation que l'on en fait, comme étant conforme à cet accord de coalition.

#ihl: Au sein du gouvernement allemand, la rumeur court qu'il y a eu un marchandage pour ouvrir une brèche dans ces obligations si en retour les projets favoris des politiciens concernés étaient poussés dans l'UE via le Conseil. Les politiciens des partis de coalition sont restés remarquablement calmes. Dans le passé, des accords similaires avaient conduit le gouvernement allemand à faire passer dans l'UE des subventions à l'industrie charbonnière via le Conseil, autorisant en échange d'autres pays à continuer à distribuer d'autres subventions. Le ministre de l'économie responsable à l'époque de ce marchandage, Werner Mueller, est maintenant à la tête de l'agence pour le charbon, RAG. Il est bien connu que le Conseil est un endroit de choix pour ce type de négoce. Ce genre de commerce aide les gouvernements des États membres à conserver et renforcer leur pouvoir au détriment des autres institutions de l'UE.

#arW: Nombre d'autres délégations ont également agi à l'encontre d'accords intra-ministériels ou contre le consentement de leur majorité parlementaire.

#Wnu: La décision n'est pas encore validée. Il s'agit d'un accord politique pour faire passer le document actuel en %(q:point A), qui devra être approuvé à la prochaine réunion du Conseil.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/cons040518.el ;
# mailto: mlhtimport@a2e.de ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ConsDe040518 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

