<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Une courte majorité de ministres, influencés par un compromis boggué, approuve les brevets logiciels

#descr: La présidence irlandaise a obtenu une approbation politique pour une nouvelle ébauche de la très controversée directive sur les brevets logiciels lors d'une réunion du Conseil des ministres, ce mardi 18 mai 2004. Cette ébauche et le communiqué de presse l'accompagnant sont remplis de déclarations de bonnes intentions prétendant empêcher les brevets sur les logiciels et les méthodes d'affaire en tant que telles, tandis que les dispositions inscrites dans le texte s'assurent que ceux-ci doivent sans l'ombre d'un doute être traités comme des inventions brevetables en Europe. Le texte doit sa majorité à une manoeuvre de la délégation allemande, qui avait rassemblé l'opposition sous son drapeau, pour à la dernière minute se contenter d'un amendement boggué et entraîner avec elle les polonais et les lettoniens. Les représentants des Pays-bas, de la Hongrie, du Danemark et de la France ont apparemment agi en contradiction avec les promesses données par   leurs parlements ou leurs gouvernements.

#dca: Contacts média

#otF: À propos de la FFII

#nWW: URL permanente de ce communiqué de presse

#trn: Les premières indications sont que la Présidence irlandaise a obtenu aujourd'hui au Conseil des ministres une approbation politique pour une nouvelle mouture de la très controversée directive sur les brevets logiciels.

#tta: L'Espagne (8) a expressément voté contre la proposition. La Belgique (5), l'Italie (10) et l'Autriche (4) ont refusé de soutenir le nouveau texte. Le Danemark (3) a semblé peu enthousiaste, mais ne s'est finalement pas dresser contre la demande de soutien de la part de la présidence.

#tcd: Ce qui fait 27 votes refusant de soutenir le texte. Si l'Allemagne avait tenu sa promesse d'au moins s'abstenir, les 37 votes nécessaires pour bloquer la proposition auraient été atteints et même dépassés avec le vote de pays comme la Pologne qui avaient apparemment reçu comme instruction de suivre les Allemands.

#WWW: Durant le premier tour de discussions, la Suède, le Royaume-Uni, la France, les Pays-Bas, la République Tchèque et la Hongrie se sont prononcés en faveur de la proposition irlandaise.

#Eas: La Belgique, la Pologne, l'Espagne, le Danemark, l'Autriche, l'Allemagne, la Lettonie et l'Italie ont exprimé des réserves.

#onh: Cependant, un %(q:compromis) proposé par la Commission a apparemment suffi à récupérer les délégations allemande, polonaise et letttonienne.

#Wyg: Ce qui donne finalement : l'Espagne qui a voté contre la proposition; et la Belgique, l'Italie et l'Autriche qui ont refusé de l'approuver.

#the: Sur la question clé de ce qui devrait et ce qui ne devrait pas être considéré comme %(q:technique), et par conséquent brevetable, les Allemands avaient à l'origine proposé l'amendement %(e:ainsi) rédigé :

#dco: Une contribution technique désigne une contribution à l'état de l'art dans un domaine technique, qui est %(s:nouvelle et) n'est pas évidente pour une personne du métier. La contribution technique est évaluée en prenant en considération la différence entre l'état de l'art et l'objet de la revendication de brevet considéré dans son ensemble, qui doit comprendre des caractéristiques techniques, qu'elles soient ou non accompagnées de caractéristiques non techniques, %(s:parmis lesquelle les caractéristiques techniques doivent prédominer. L'utilisation des forces de la nature pour contrôler les effets physiques au-delà de la représentation numérique de l'information appartient à un domaine technique. Les seuls traitement, manipulation et présentation de l'information n'appartiennent pas à un domaine technique, même si des équipements techniques sont employés pour de tels buts).

#mrt: Le %(q:compromis) de la Commision a coupé ceci pour donner :

#tol: Une contribution technique désigne une contribution à l'état de l'art dans un domaine technique, qui est %(s:nouvelle et) n'est pas évidente pour une personne du métier. La contribution technique est évaluée en prenant en considération la différence entre l'état de l'art et l'objet de la revendication de brevet considéré dans son ensemble, qui doit comprendre des caractéristiques techniques, qu'elles soient ou non accompagnées de caractéristiques non techniques.

#sto: C'est-à-dire que le seul effet a été d'insérer le mot %(q:nouvelle).

#aah: La Commission a également proposé des modifications à l'article 4 et au considérant 13, mais leurs effets ne sont que cosmétiques.

#Wes: Pour une raison ou pour une autre, cela a suffit à gagner la délégation allemande et les Polonais et Lettoniens ont suivi.

#yeh: Bien qu'apparemment suffisant pour convaincre les ministres, le texte demeure aussi intransigeamment pro-brevets que la proposition originale de l'Irlande.

#snr: Les délégations polonaise, lettonienne et danoise ont paru en réalité indécises. La plupart des autres délégations ont pris leur décision malgré les promesses contraires que leurs gouvernements avaient faites au parlement ou au public.

#dWl: Le texte sera présenté au prochain Conseil des ministres (début juin ou peut-être plus tard) pour une adoption formelle. Il n'est pas encore certain que la présidence irlandaise soit assurée d'une majorité bien réelle.

#uWs: Pour réinstaurer les amendements au Parlement européen, il faut une majorité absolue. C'est tout à fait possible : nombre d'amendements avaient réussi à rassembler ce niveau de soutien lors de la première lecture. Mais certains votes sont susceptibles d'être très serrés.

#eWW: Aussi, la FFII empresse ses supporters de s'assurer que les candidats aux élections européennes sauront se rendre compte de la profonde inquiétude engendrée par le sujet.

#ifs: Compte-rendu provisoire du Conseil sur la compétitivité

#uhe: Compte-rendu de la session dans laquelle a été débattue la Directive sur les brevets logiciels.

#neW: Procédure de vote au Conseil de l'UE

#rct: Transcription

#rct2: Transcription

#Ceo: Plus de contacts peuvent être fournis sur simple demande

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/cons040518.el ;
# mailto: mlhtimport@a2e.de ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040518 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

