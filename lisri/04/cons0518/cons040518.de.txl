<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Knappe Mehrheit der Minister stimmt Softwarepatenten zu, erreicht
durch faulen Kompromiss

#descr: Die irische Präsidentschaf hat sich die politische Zustimmung zu einem
neuen Entwurf der kontroversen Softwarepatent-Richtlinie bei einem
Treffen des Ministerrats am Dienstag, den 18. Mai 2004, gesichtert.
Der Entwurf und die begleitenden Pressemitteilungen sind voll von
Aussagen über gute Absichten zur Vermiedung von Patenten auf Software
und Geschäftsmethoden als Solche, während dagegen die Formulierungen
im Text sicherstellen, dass solche Objekte zweifelsfrei als
patentierbare Erfindungen in Europa behandelt werden müssen. Der
Entwurf verdankt seine Mehrheit einem Manöver der deutschen
Delegation, welche die Opposition unter ihrer Flagge versammelt hat,
um sich in letzter Minute zu einer Schein-Änderung zu entschliessen
und damit die Polen und die Letten mit sich zu ziehen. Die
Repräsentanten der Niederlande, der Ungarn, der Dänen und der
Franzosen agierten offensichtlich entgegen den Zusagen, welche sie
ihren Parlamenten oder Regierungen gegeben hatten.

#dca: Pressekontakte

#otF: Über den FFII

#nWW: Permanente URL dieser Pressemittelung

#trn: First indications are that the Irish presidency has secured political
agreement for a new draft of the controversial software patents
directive in a meeting of the Council of Ministers today.

#tta: Spain (8) expressly voted against the proposal.  Belgium (5), Italy
(10) and Austria (4) refused to support the new text.  Denmark (3)
seemed uneager, but in the end did not stand against the presidency's
demand for support.

#tcd: That made 27 votes refusing to support the text. Had Germany kept its
promise to at least abstain, the 37 votes needed to block it would
have been achieved, and surpassed by votes from countries like Poland
who had apparently been instructed to follow the Germans.

#WWW: In the initial round of discussions SE, UK, FR, NL, CZ and HU spoke in
favour of the Irish proposal.

#Eas: BE, PL, ES, DK, AT, DE, LV und IT äußerten Vorbehalte.

#onh: However a %(q:compromise) proposed by the Commission was apparently
enough to bring round the German, Polish and Latvian delegations,
whose silence was interpreted as agreement.

#Wyg: That left ES voting against the proposal; and BE, IT, and AT refusing
to approve it.

#the: On the key issue of what should and should not count as
%(q:technical), and therefore patentable, the Germans had originally
proposed the additions shown %(e:thus):

#dco: A technical contribution means a contribution to the state of the art
in a field of technology which is %(s:new and) not obvious to a person
skilled in the art. The technical contribution shall be assessed by
consideration of the difference between the state of the art and the
scope of the patent claim considered as a whole, which must comprise
technical features, irrespective of whether these are accompanied by
non-technical features, %(s:whereby the technical features must
predominate. The use of natural forces to control physical effects
beyond the digital representation of information belongs to a
technical field. The mere processing, handling, and presentation of
information do not belong to a technical field, even where technical
devices are employed for such purposes).

#mrt: Der %(q:Kompromiss) der Kommision schlug vor die folgendermaßen zu
kürzen:

#tol: A technical contribution means a contribution to the state of the art
in a field of technology which is %(s:new and) not obvious to a person
skilled in the art. The technical contribution shall be assessed by
consideration of the difference between the state of the art and the
scope of the patent claim considered as a whole, which must comprise
technical features, irrespective of whether these are accompanied by
non-technical features.

#sto: Das bedeutet, die einzige wirksame Änderung war die Einfügung des
Wortes %(q:neu).

#aah: Die Kommission hat weiterhin Änderungen zu Artikel 4 und zu Präambel
13 vorgeschlagen, wobei deren Auswirkungen lediglich kosmetischer
Natur sind.

#Wes: But for whatever reason, this was sufficient to win over the German
delegation, and the Poles and Latvians apparently followed.

#yeh: Although apparently enough to convince ministers, the text remains as
uncompromisingly pro-patent as the original Irish draft.

#snr: The Polish, Latvian and Danish delegation did not visibly decide for
anything.  Most other delegations decided in spite of promises to the
contrary given by their governments to the parliament or the public.

#dWl: The text will be presented to the next Council of Ministers (in early
June or perhaps later) for formal adoption.  It is not yet certain
that the Irish Presicendy has secured a real majority.

#uWs: It is possible that after passing the Council the directive will go
into 2nd reading in the newly elected European Parliament this autumn.
 It is also possible (and advocated e.g. by Olga Zrihen MEP) that it
could go into another 1st reading. To re-instate amendments in the
European parliament in 2nd reading would require absolute majorities. 
 This is achievable: many of the amendments did achieve this level of
support in the first reading.   But some of the votes are likely to be
very close.

#eWW: Darum fordert der FFII die Unterstüzter auf, sicher zu gehen dass die
MEP Teilnehmer für diese Wahl die tiefe Sorge in dieser Sache auch
würdigen.

#ifs: Vorläufige Mitschrift des Wettbewerbsrats

#uhe: Sitzungsprotokoll vom Tag der Zusammenkunft an dem die
Softwarepatent-Richtlinie diskutiert wurde.

#neW: Wahlverfahren im Europarat

#rct: Transcription

#rct2: Transcirption

#Ceo: Weitere Kontakte werden auf Anfrage mitgeteilt

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: astohrl ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040518 ;
# txtlang: de ;
# multlin: t ;
# End: ;

