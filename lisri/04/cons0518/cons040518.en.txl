<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Thin Majority of Ministers Approve Software Patents, Swayed by Bogus
Compromise

#descr: The Irish presidency has secured political approval for a new draft of
the controversial software patents directive in a meeting of the
Council of Ministers on Tuesday, 2004-05-18.  The draft and
accompanying press releases are full of statements of good intentions
to avoid patents on software and business methods as such, whereas the
provisions in the text assure that such items are without any doubt to
be treated as patentable inventions in Europe.  The draft owes its
majority to a maneuver by the German delegation, which had collected
the opposition under its flag, to settle for a bogus amendment in the
last minute and take the Poles and Latvians with it.  Representatives
of the Netherlands, Hungary, Denmark and France apparently acted in
breach of promises given to their parliaments or governments.

#trn: First indications are that the Irish presidency has secured political
approval for a new draft of the controversial software patents
directive in a meeting of the Council of Ministers today.

#tta: Spain (8) expressly voted against the proposal.  Belgium (5), Italy
(10) and Austria (4) refused to support the new text.  Denmark (3)
seemed uneager, but in the end did not stand against the presidency's
demand for support.

#tcd: That made 27 votes refusing to support the text. Had Germany kept its
promise to at least abstain, the 37 votes needed to block it would
have been achieved, and surpassed by votes from countries like Poland
who had apparently been instructed to follow the Germans.

#WWW: In the initial round of discussions SE, UK, FR, NL, CZ and HU spoke in
favour of the Irish proposal.

#Eas: BE, PL, ES, DK, AT, DE, LV and IT expressed reservations.

#onh: However a %(q:compromise) proposed by the Commission was apparently
enough to bring round the German, Polish and Latvian delegations.

#Wyg: That left ES voting against the proposal; and BE, IT, and AT refusing
to approve it.

#the: On the key issue of what should and should not count as
%(q:technical), and therefore patentable, the Germans had originally
proposed the additions shown %(e:thus):

#dco: A technical contribution means a contribution to the state of the art
in a field of technology which is *new and* not obvious to a person
skilled in the art. The technical contribution shall be assessed by
consideration of the difference between the state of the art and the
scope of the patent claim considered as a whole, which must comprise
technical features, irrespective of whether these are accompanied by
non-technical features, *whereby the technical features must
predominate. The use of natural forces to control physical effects
beyond the digital representation of information belongs to a
technical field. The mere processing, handling, and presentation of
information do not belong to a technical field, even where technical
devices are employed for such purposes*.

#mrt: The Commissions %(q:compromise) was to cut this to:

#tol: A technical contribution means a contribution to the state of the art
in a field of technology which is *new and* not obvious to a person
skilled in the art. The technical contribution shall be assessed by
consideration of the difference between the state of the art and the
scope of the patent claim considered as a whole, which must comprise
technical features, irrespective of whether these are accompanied by
non-technical features.

#sto: This means that the only effect was to insert the word %(q:new).

#aah: The Commission also proposed changes to Article 4 and Recital 13, but
the effect of these is only cosmetic.

#Wes: But for whatever reason, this was sufficient to win over the German
delegation, and the Poles and Latvians followed.

#yeh: Although apparently enough to convince ministers, the text remains as
uncompromisingly pro-patent as the original Irish draft.

#snr: The Polish, Latvian and Danish delegation appeared in reality
undecided.  Most other delegations decided in spite of promises to the
contrary given by their governments to the parliament or the public.

#dWl: The text will be presented to the next Council of Ministers (in early
June or perhaps later) for formal adoption.  It is not yet certain
that the Irish Presicendy has secured a real majority.

#uWs: To re-instate amendments in the European parliament requires absolute
majorities.   This is achievable: many of the amendments did achieve
this level of support in the first reading.   But some of the votes
are likely to be very close.

#eWW: FFII therefore urges supporters to make sure MEP candidates at this
election truly appreciate the depth of concern about this issue.

#ifs: Provisional minutes of Competitiveness Council

#uhe: Meeting minutes of the session where the Software Patent directive was
discussed.

#neW: Voting Procedure in the EU Council

#rct: Transcription

#rct2: Transcirption

#dca: Media contacts

#Ceo: More Contacts to be supplied upon request

#otF: About the FFII

#nWW: Permanent URL of this Press Release

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/cons040518.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: a2edir ;
# dok: cons040518 ;
# txtlang: en ;
# multlin: t ;
# End: ;

