<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#nCr: Reacções à posição do Grupo de Trabalho do Conselho

#kWa: Mais informação ppreparativa

#dca: Contactos para os media

#otF: Sobre a FFII

#nWW: URL permanente  deste Press Release

#MEP: Eurodeputado

#PSE: PES

#mbe: Parlamento Anulado por Administradores de Gabinetes de Patentes

#ien: Conselho Ignora Representantes Eleitos

#WFi: Conselho e Comissão Falham no Trabalho-de-casa

#ici: Administradores de Patentes Não Estão Interessados em %(q:Harmonização
nem Clarificação) mas apenas em ...

#mSW: Movimentação do Conselho Expõe Falta de Democracia na UE

#eWU: Presidencia Irlandesa Protege Companhias dos EUA de Competição
Europeia

#atW: Anne Van Lancker, Eurodeputada Belga do grupo Socialista, diz que os
funcionários públicos que são supostos ser regulados por esta
directiva são agora quem escreve as suas próprias leis:

#anj: O Conselho não só ignora o Parlamento Europeu sobre esta matéria, mas
adiciona insulto ao ir ainda mais além do que a Comissão: em adição a
tornar numa infracção a utilizão num computador de modelos de negócio
e algoritmos patenteados, também propõem proibir a sua publicação ao
permitir reivindicações directas às entidades informativas (também
conhecidas como %(q:reivindicações sobre programas)).

#CeW: Dado que a proposta actual do Conselho foi escrita a portas fechadas
por administradores de gabinetes de patentes, este resultado irreal
não deveria, infelizmente, surpreender ninguém.

#raW: Pernille Frahm, Eurodeputada Dinamarquesa e Vice-Presidente do grupo
GUE/NGL, considera que a Comissão Europeia e o Conselho Europeu estão
a abusar as suas funções e a falhar no seu papel legislativo na União
Europeia.

#fue: Os administradores de patentes na Comissão e no Conselho estão a
abusar o processo legislativo da UE.

#oeh: A sua convulta e enganadora Novilíngua de Patentes, negociada em
obscuras negociações nos bastidores, é um insulto ao Parlamento
Europeu, ao Comité Económico e Social Europeu, ao Comité das Regiões e
aos inúmeros peritos e partes que têm investigado seriamente este
projecto de directiva connosco. Não só a Comissão e o Conselho falham
o trabalho de casa, eles também tentam deitar fora o trabalho árduo
que os legisladores eleitos fizeram pr eles, sem sequer tentarem
responder às preocupações que têm sido levantadas.

#toa: O grupo de trabalho do Conselho tem até agora falhado completamente em
lidar com os problemas que os Comités do Parlamento Europeu, da
Cultura e Indústria, tentaram resolver. Eles comportam-se da mesma
forma que o Comité dos Assuntos Jurídicos se comportou no ano passado,
e podemos esperar que falhem da mesma forma.

#tec: É evidente que os oficiais nacionais de patentes no Conselho não
querem %(q:harmonização) ou %(q:clarificação). Eles meramente querem
assegurar os interesses estabelecidos com patentes. Se não conseguirem
o que querem simplesmente enterram o projecto de directiva e tentam
encontrar outras formas de dar a volta à lei existente, cuja clareze
lhe dói tanto.

#jWW: Johanna Boogerd-Quaak, membra Holandesa do Partido Liberal Democrata e
Reformador, indica que a Irlanda parece estar a fazer-se passar de
cachorrinho de colo para os EUA:

#uWl: Tenho a impressão que a Presidência Irlandesa se está a resguardar sob
os interesses das companhias Americanas. Estas grandes companhias
Americanas irão lucrar com patentes de software, mas é um negócio
muito mau para a inovação feita pelas PMEs Europeias. Adicionalmente,
o Conselho está a demonstrar um desprezo pela democracia parlamentar.
Devemos fazer garantir que após as eleições haja novamente uma maioria
no Parlamento Europeu que esteja disposta a mostrar os seus dentes.

#itu: 15 Eurodeputados assinaram uma %(ca:Call for Action - Proposta de
Medidas) (o mesmo título utilizado pela Nokia na carta por si
distribuída) que alerta que %(q:profissionais de patentes em vários
governos e organizações estão aogra a tentar utilizar o Conselho
Europeu de Ministros de forma a contornar a democracia parlamentar da
União Europeia) e urge ao Conselho que se %(q:refreie de
contra-propostas à versão do Parlamento Europeu do rascunho, a menos
que tais contra-propostas tenham sido explícitamente apoiadas por uma
decisão maioritária dos parlamentos nacionais).

#Edg: O poderoso comité dos Representantes Permanentes em Bruxelas dos
estados-membros da UE, COREPER, adicionalmente concordou com um novo
rascunho da controversa directiva de Patentes de Software, anulando a
oposição da Alemanha, Bélgica, Dinamarca e Eslováquia.

#Wjm: O novo rascunho rejeita todas as emendas limitativas do Parlamento
Europeu, e é descrito pela FFII como %(q:o mais intransigente texto
pro-patentes até hoje).

#ima: Tecnicamente, a decisão do COREPER de Quarta-feira é apenas uma
%(q:previsão) da decisão final, a ser confirmado pelo Conselho de
Ministros da Competitividade em 17 e 18 de Maio. Até essa data, os
Estados-membros ainda podem mudar a sua opinião (e os seus votos).

#fWs: Diz-se que o apoio para o documento ao nível político é muito suave; e
que as decisões definidas no COREPER podem ser desfeitas (as
discussões do ano passado sobre a Patente Comunitária, por exemplo).

#Won: O texto do COREPER vai mais além do que o texto de 2002 da Comissão
Europeia ao legalizar as patentes de software. Em 2002 a Comissão
tinha concordado, em difíceis negociações entre o DG do Mercado
Interno (Bolkestein) e o DG da Sociedade da Informação (Liikanen), em
não permitir reivindicações sobre programas. Agora o DG da Sociedade
da Informação aparentemente juntou-se à pressão unida de Bolkestein e
dos administradores de patentes do Conselho.

#gie: Um %(ld:documento que veio a público do DG do Mercado Interno de
Bolkestein) sugere que o DG da Sociedade da Informação deixou de
rejeitar reivindicações sobre programas. Esta concessão de Liikanen é
necessária por forma a apressar o grupo de trabalho do Conselho na
sessão ministerial como um %(q:item A), isto é, um ponto de consenso
que não necessita qualquer discussão da parte dos ministros.

#rWh: Durante a próxima semana a FFII apela a uma nova greve on-line e a uma
onde de eventos e manifestações locais. Nos últimos dias já, pessoas
manifestam-se com cartazes próximo dos gabinetes da Comissão.

#5Wa: Os documentos proposta do Conselho 8253/04 e 8253/04 ADD de 6 de Abril
não são acessíveis %(q:devido à natureza sensitiva das negociações e à
ausência de um interesse público superior), de acordo com o
Secretariado Geral do Conselho.

#tah: %(jm:Carta) escrita ao Secretário Geral do Conselho, apelando da
recusa de publicação dos documentos de 6 de Abril.

#oeW: Providenciámos uma %(an:análise detalhada) do último %(q:compromisso)
do Conselho. A ultima versão veio ao nosso conhecimento e está
acessível segunda a secção %(L). A principal diferença entre a última
versão e as anteriores é que não vai haver mais avaliação da
compatibilidade com a Directiva Europeia do Direito de Autor de
Software de 1991. O motivo é provavelmente que descobriram, tal como
nós, que %(cb:as patentes de software são de facto incompatíveis com
essa directiva).

#hai: Um %(bv:documento que veio a público do Ministério Austríaco da
Tecnologia) demonstra que o suporte desta decisão se baseia num
empurrão de oficiais de patentes que estão a dar o seu melhor para
escaparem da atenção dos seus governos e parlamentos nacionais. Os
Eurodeputados Austríacos de todos os partidos votaram essencialmente a
favor das emendas do Parlamento. Othmar Karas, chefe do Partitdo
Popular Europeu Austríaco no Parlamento Europeu, queixou-se aos seus
colegas responsáveis nos ministérios do governo mas não recebeu
qualquer resposta.

#sno: A Presidência Irlandesa explica no seu website que é %(ms:patrocinada
pela Microsoft). A Irlanda é %(q:o país que mais exporta software na
Europa), graças a uma política fiscal que a torna um paraíso fiscal
para grandes companhias dos EUA: tem um imposto sobre rendimentos de
patentes de 0%. Algumas pessoas (%bl:especulam) que talvez %(q:haja
alguma ligação).

#rcn: Mais notícias recentes

#Ceo: Mais contactos serão disponibilizados sob pedido

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: rmseabra ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040507 ;
# txtlang: pt ;
# multlin: t ;
# End: ;

