\contentsline {section}{\numberline {1}Proposals slammed by MEPs from all sides}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Anne Van Lancker (MEP, BE, PES): Parliament Overruled by Patent Office Administrators}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Piia-Noora Kauppi (MEP, FI, EPP-DE): Council Ignores Elected Representatives}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Pernille Frahm (MEP, DK, GUE): Council and Commission Failed to do their Homework}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Daniel Cohn-Bendit (MEP, FR, VERD): Patent Administrators Not Interested in ``Harmonisation and Clarification''}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Bent Hindrup Andersen (MEP, DK, EDD): Council Moves Highlight Lack of Democracy in the EU}{4}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Johanna Boogerd-Quaak (MEP, NL, ELDR): Irish Presidencey Protecting US Companies from EU Competition}{4}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Aufrufe zum Handeln}{5}{subsection.1.7}
\contentsline {section}{\numberline {2}Details}{5}{section.2}
\contentsline {section}{\numberline {3}More Background Information}{5}{section.3}
\contentsline {section}{\numberline {4}Media contacts}{6}{section.4}
\contentsline {section}{\numberline {5}About the FFII -- www.ffii.org}{6}{section.5}
\contentsline {section}{\numberline {6}Permanent URL of this Press Release}{7}{section.6}
