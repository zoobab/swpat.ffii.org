<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#nCr: Ratsvorschläge von MdEPs aller Fraktionen verurteilt

#kWa: Weitere Hintergrundinformationen

#dca: Pressekontakte

#otF: Über den FFII

#nWW: Ständige Netzadresse dieser Mitteilung

#MEP: MdEP

#PSE: PES

#mbe: Parlament durch Patentamtsfunktionäre überstimmt

#ien: Rat ignoriert gewählte Vertreter

#WFi: Rat und Kommission machten ihre Hausarbeiten nicht

#ici: Patentfunktionäre nicht interessiert an %(q:Harmonisierung und
Klarstellung)

#mSW: Ratsmanöver zeigt Demokratiedefizit der EU auf

#eWU: Irische Ratspräsidentschaft schützt US-Konzerne vor EU-Wettbewerb

#atW: Anne Van Lancker, belgische Europaparlamentarierin der sozialistischen
Fraktion, stellt fest, dass die im Rat mit dieser Richtlinie betrauten
Beamten Gesetze in eigener Sache schreiben:

#anj: Die Ministerialbeamten ignorieren das Europäische Parlament in dieser
Angelgenheit nicht nur, sondern vergrößern den Affront, indem sie noch
weiter geht als die Kommission: Nicht nur den Gebrauch patentierter
Algorithmen und Geschäftsmethoden in Computerprogrammen machen sie zu
einer Verletzung, sondern sie schlagen auch noch vor, deren
Veröffentlichung zu verbieten, indem sie %(tp|direkte Ansprüche auf
Informationsgüter|auch bekannt als %(q:Programmansprüche)) erlauben.

#CeW: In Anbetracht der Tatsache, dass der gegenwärtige Ratsvorschlag hinter
verschlossenen Türen von Patentamtsfunktionären verfasst worden ist,
sollte dieses befremdliche Ergebnis niemanden überraschen, leider.

#raW: Pernille Frahm, dänische Abgeordnete und stellvertretende Vorsitzende
der GUE/NGL-Fraktion, findet, dass die Europäische Kommission und der
Rat ihre Funktionen missbrauchen und ihre Aufgabe bei der
EU-Gesetzgebung nicht wahrnehmen:

#fue: Die Patentfunktionäre bei der Kommission und dem Rat missbrauchen den
Gesetzgebungsprozess der EU.

#oeh: Ihr verschlungenem und irreführendem Patentneusprech, ausgehandelt in
undurchsichtigen Hinterzimmergeschäften, ist eine Beleidigung des
Europäischen Parlaments, des Europäischen Wirtschafts- und
Sozialausschusses, des Länderausschusses und der unzählbaren Experten
und Interessenvertreter, die sich in ernsthafte Erörterungen über
dieses Richtlinienprojekt mit uns eingebracht haben.  Kommission und
Rat haben nicht nur ihre eigenen Hausarbeiten nicht gemacht, sie
versuchen auch, all die harte Arbeit, welche die gewählten Gesetzgeber
für sie getan haben, zu verwerfen, ohne eine Antwort auf die erhobenen
Bedenken auch nur zu versuchen.

#toa: Die Ratsarbeitsgruppe hat es bislang völlig versäumt, die Probleme
anzusprechen, welche der Kultur- und der Gewerbeausschuss zu lösen
versuchten.  Sie verhalten sich genau so, wie der Rechtsausschuss sich
letztes Jahr verhielt, und wir können erwarten, dass sie genauso
scheitern werden.

#tec: Es wird klar, dass die nationalen Patentbeamten im Rat weder
%(q:Harmonisierung) noch %(q:Klarstellung) wollen. Ihnen geht es
lediglich um Sicherung von Pfründen der Patent-Institutionen. Wenn sie
nicht bekommen, was sie wollen, beerdigen sie einfach die ganze
Richtlinie und versuchen, andere Wege um das geltende Recht herum zu
finden, dessen Klarheit ihnen so viel Schmerzen bereitet.

#jWW: Johanna Boogerd-Quaak, niederländische Abgeordnete der Europäischen
Liberalen, Demokraten und Reformer, deutet an, dass Irland das
Schoßhündchen der USA zu spielen scheint:

#uWl: Ich habe den Eindruck, dass die irisiche Ratspräsidentschaft sich vor
den Interessen amerikanischer Gesellschaften zu Boden geneigt hat.
Eine Handvoll amerikanischer Großunternehmen dürfte von
Software-Patenten profitieren, aber das sind sehr schlechte Karten für
Innovation in europäischen KMUs.  Außerdem zeigt der Rat Missachtung
der parlamentarischen Demokratie.  Wir müssen dafür sorgen, dass nach
den Wahlen wieder eine Mehrheit im Europäischen Parlament zustande
kommt, die bereit ist, Zähne zu zeigen.

#itu: Mehr als 20 Parlamentarier haben einen %(ca:Aufruf zum Handeln)
unterzeichnet, der darauf hinweist, dass derzeit %(q:einflußreiche
Patentfachleute in verschiedenen Regierungen und Organisationen
versuchen, den EU-Ministerrat zu benutzen, um an der parlamentarischen
Demokratie in der Europäischen Union vorbei zu kommen), und den Rat
aufruft, %(q:von jeglichem Gegenvorschlag zu der Version des
Europäischen Parlaments Abstand zu nehmen, solange solch ein
Gegenvorschlag keine ausdrückliche Unterstützung des Parlaments ihres
Mitgliedsstaates gefunden hat).

#Edg: Das Kommittee COREPER der ständigen Vertreter der EU-Mitgliedstaaten
hat in Brüssel einen neuen Entwurf der kontroversen
Softwarepatent-Richtlinie vorläufig beschlossen, wobei Widerstand von
Deutschland, Belgien, Dänemark und der Slowakei überstimmt wurde.

#Wjm: Der neue Entwurf weist alle begrenzenden Änderungen des Europäischen
Parlaments zurück und wird vom FFII als %(q:der patentfetischistischer
Entwurf, der in seiner Kompromisslosigkeit alles bisher dagewesene in
den Schatten stellt) beschrieben.

#ima: Formal ist COREPERs Entscheidung vom Mittwoch nur eine %(q:Vorhersage)
der endgültigen Entscheidung, die beim Ratstreffen der Minister für
Wettbewerbsfragen am 17.-18. Mai zu bestätigen wäre.  Bis dahin können
Mitgliedsstaaten immer noch ihre Meinung (und ihre Entscheidung)
ändern.

#fWs: Die inhaltliche Unterstützung des COREPER-Textes wird in einigen
Staaten als recht wacklig eingeschätzt, und von COREPER vermittelte
Entscheidungen können durchaus platzen (wie beispielsweise bei der
Diskussion um das Gemeinschaftspatent im letzten Jahr).

#Won: Bei der Legalisierung von Software-Patenten geht der COREPER-Text noch
weiter als der Text der Europäischen Kommission von 2002.  Damals
hatte die Kommission in schwierigen Verhandlungen zwischen der
Generaldirektion Binnenmarkt (Bolkestein) und der Generaldirektion
Informationsgesellschaft (Liikanen) zugestimmt, keine
Programmansprüche vorzusehen.  Nun scheint die Generaldirektion
Informationsgesellschaft unter dem vereinten Druck von Bolkestein und
den Patentsachbearbeitern des Rates umgefallen zu sein.

#gie: Ein %(ld:durchgesickertes Dokument aus Bolkesteins Generaldirektion
Binnenmarkt) deutet an, dass die Generaldirektion
Informationsgesellschaft keine Einwände gegen Programmansprüche mehr
erhebt.  Dieses Zugeständnis von Liikanen wird benötigt, um den
Vorschlag der Ratsarbeitsgruppe als %(q:A-Punkt) durch die
Ministerratstagung zu expedieren, d.h. als Konsenspunkt, der keiner
weiteren Diskussion unter den Ministern mehr bedarf.

#rWh: Für die kommende Woche ruft der FFII zu einem weiteren Netzstreik und
zu einer Welle von %(sd:örtlichen Veranstaltungen und Demonstrationen)
auf.  Bereits jetzt demonstrieren Leute mit Transparenten nahe den
Büros der EU-Kommission.

#5Wa: Die Ratsvorschläge in den Dokumenten 8253/04 and 8253/04 ADD 1 vom 6.
April sind nach Bescheid des Rats-Sekretariats %(q:wegen der
empfindlichen Art der Verhandlungen und der Abwesenheit überwiegenden
öffentlichen Interesses) nicht verfügbar.

#tah: %(jm:Brief) an das Sekretariat des Rates, der gegen die Weigerung, die
Dokumente vom 6. April zu veröffentlichen, Einspruch erhebt.

#oeW: Wir haben eine %(eingehende Analyse) des jüngsten
Rats-%(q:Kompromisses) durchgeführt.  Die jüngste Version wurde uns
zugespielt und ist im Abschnitt %(q:%(L)) verfügbar.

#hai: Ein %(bv:zugespieltes Dokument aus dem österreichischen
Technologieministerium) zeigt exemplarisch die Entscheidungsfindung
hinter dem Ratsvorschlag.  Österreichische Abgeordnete aller
Fraktionen hatten größtenteils für die Änderungen des Parlaments
gestimmt.  Othmar Karas, Kopf der Österreichischen Volkspartei im
Europaparlament, hatte an verschiedene Regierungsfunktionäre
geschrieben und ihn gebeten, die Ratsposition nicht zu unterstützen,
aber keine Antwort erhalten.  Stattdessen überließ die Regierung alle
Entscheidungen dem ÖsterreichischenPatentamt, welches dem Europäischen
Patentamt schon immer 100%ig gefolgt ist.

#sno: Die irische Ratspräsidentschaft erklärt auf ihrer Webseite, dass sie
%(ms:von Microsoft gesponsort) sei.  %(ie:Irland) ist %(q:das größte
Software-Exportland in Europa), dank einer Steuerpolitik, die es zu
einem Steuerparadies für große US-Konzerne macht.  U.a. erhebt es 0%
Steuern auf Patenteinkünfte.

#rcn: Jüngste Neuigkeiten

#Ceo: Weitere Kontakte können auf Anfrage bereitgestellt werden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ccorn ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040507 ;
# txtlang: de ;
# multlin: t ;
# End: ;

