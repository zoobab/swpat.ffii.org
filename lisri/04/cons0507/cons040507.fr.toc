\select@language {french}
\contentsline {section}{\numberline {1}Une proposition fustig\'{e}e par des d\'{e}put\'{e}s europ\'{e}ens de tous bords}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Anne Van Lancker (D\'{e}put\'{e} europ\'{e}en, BE, PSE): Les administrateurs des bureaux des brevets passent outre le Parlement}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Piia-Noora Kauppi (D\'{e}put\'{e} europ\'{e}en, FI, EPP-DE): Le Conseil ignore les repr\'{e}sentants \'{e}lus}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Pernille Frahm (D\'{e}put\'{e} europ\'{e}en, DK, GUE): Le Conseil et la Commission n'ont pas fait leur travail}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Daniel Cohn-Bendit (D\'{e}put\'{e} europ\'{e}en, FR, VERD): Les administrateurs de brevets ne s'int\'{e}ressent pas ``\`{a} l'harmonisation ni \`{a} la clarification''}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Bent Hindrup Andersen (D\'{e}put\'{e} europ\'{e}en, DK, EDD): Le comportement du Conseil souligne le manque de d\'{e}mocratie dans l'UE}{3}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Johanna Boogerd-Quaak (D\'{e}put\'{e} europ\'{e}en, NL, ELDR): La pr\'{e}sidence irlandaise prot\`{e}ge les entreprises des USA contre la concurrence de l'UE}{4}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Aufrufe zum Handeln}{4}{subsection.1.7}
\contentsline {section}{\numberline {2}D\'{e}tails}{4}{section.2}
\contentsline {section}{\numberline {3}Informations compl\'{e}mentaires sur la situation}{5}{section.3}
\contentsline {section}{\numberline {4}Contacts m\'{e}dia}{6}{section.4}
\contentsline {section}{\numberline {5}\`{A} propos de la FFII -- www.ffii.org}{6}{section.5}
\contentsline {section}{\numberline {6}URL permanente de ce communiqu\'{e} de presse}{6}{section.6}
