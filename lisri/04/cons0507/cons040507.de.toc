\contentsline {section}{\numberline {1}Ratsvorschl\"{a}ge von MdEPs aller Fraktionen verurteilt}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Anne Van Lancker (MdEP, BE, PES): Parlament durch Patentamtsfunktion\"{a}re \"{u}berstimmt}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Piia-Noora Kauppi (MdEP, FI, EPP-DE): Rat ignoriert gew\"{a}hlte Vertreter}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Pernille Frahm (MdEP, DK, GUE): Rat und Kommission machten ihre Hausarbeiten nicht}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Daniel Cohn-Bendit (MdEP, FR, VERD): Patentfunktion\"{a}re nicht interessiert an ``Harmonisierung und Klarstellung''}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Bent Hindrup Andersen (MdEP, DK, EDD): Ratsman\"{o}ver zeigt Demokratiedefizit der EU auf}{4}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Johanna Boogerd-Quaak (MdEP, NL, ELDR): Irische Ratspr\"{a}sidentschaft sch\"{u}tzt US-Konzerne vor EU-Wettbewerb}{4}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Aufrufe zum Handeln}{5}{subsection.1.7}
\contentsline {section}{\numberline {2}Detaily}{5}{section.2}
\contentsline {section}{\numberline {3}Weitere Hintergrundinformationen}{6}{section.3}
\contentsline {section}{\numberline {4}Pressekontakte}{7}{section.4}
\contentsline {section}{\numberline {5}\"{U}ber den FFII -- www.ffii.org}{7}{section.5}
\contentsline {section}{\numberline {6}St\"{a}ndige Netzadresse dieser Mitteilung}{7}{section.6}
