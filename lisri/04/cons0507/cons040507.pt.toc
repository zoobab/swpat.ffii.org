\select@language {portuguese}
\contentsline {section}{\numberline {1}Reac\c {c}\~{o}es \`{a} posi\c {c}\~{a}o do Grupo de Trabalho do Conselho}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Anne Van Lancker (Eurodeputado, BE, PES): Parlamento Anulado por Administradores de Gabinetes de Patentes}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Piia-Noora Kauppi (Eurodeputado, FI, EPP-DE): Conselho Ignora Representantes Eleitos}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Pernille Frahm (Eurodeputado, DK, GUE): Conselho e Comiss\~{a}o Falham no Trabalho-de-casa}{3}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Daniel Cohn-Bendit (Eurodeputado, FR, VERD): Administradores de Patentes N\~{a}o Est\~{a}o Interessados em ``Harmoniza\c {c}\~{a}o nem Clarifica\c {c}\~{a}o'' mas apenas em ...}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Bent Hindrup Andersen (Eurodeputado, DK, EDD): Movimenta\c {c}\~{a}o do Conselho Exp\~{o}e Falta de Democracia na UE}{4}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Johanna Boogerd-Quaak (Eurodeputado, NL, ELDR): Presidencia Irlandesa Protege Companhias dos EUA de Competi\c {c}\~{a}o Europeia}{4}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Aufrufe zum Handeln}{5}{subsection.1.7}
\contentsline {section}{\numberline {2}Detaily}{5}{section.2}
\contentsline {section}{\numberline {3}Mais informa\c {c}\~{a}o ppreparativa}{6}{section.3}
\contentsline {section}{\numberline {4}Contactos para os media}{6}{section.4}
\contentsline {section}{\numberline {5}Sobre a FFII -- www.ffii.org}{7}{section.5}
\contentsline {section}{\numberline {6}URL permanente deste Press Release}{7}{section.6}
