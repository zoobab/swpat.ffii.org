<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#nCr: Une proposition fustigée par des députés européens de tous bords

#kWa: Informations complémentaires sur la situation

#dca: Contacts média

#otF: À propos de la FFII

#nWW: URL permanente de ce communiqué de presse

#MEP: Député européen

#PSE: PSE

#mbe: Les administrateurs des bureaux des brevets passent outre le Parlement

#ien: Le Conseil ignore les représentants élus

#WFi: Le Conseil et la Commission n'ont pas fait leur travail

#ici: Les administrateurs de brevets ne s'intéressent pas %(q:à
l'harmonisation ni à la clarification)

#mSW: Le comportement du Conseil souligne le manque de démocratie dans l'UE

#eWU: La présidence irlandaise protège les entreprises des USA contre la
concurrence de l'UE

#atW: Anne Van Lancker, une députée européenne belge du Groupe socialiste,
note que les fonctionnaires qui sont censés respecter cette directive
sont maintenant ceux qui peuvent écrire leurs propres lois :

#anj: Non seulement le Conseil ignore le Parlement européen sur ce sujet,
mais il en rajoute dans l'insulte en allant encore plus loin que la
Commission : en plus de faire une infraction de l'utilisation
d'algorithmes brevetés ou de méthodes commerciales dans des programmes
d'ordinateur brevetés, il propose également d'interdire leur
publication, en autorisant les revendications directes sur des objets
informatiques (également appelés %(q:revendications de programmes)).

#CeW: Étant donné que la proposition actuelle du Conseil a été écrite
derrière des portes closes par des administrateurs de bureaux des
brevets, ce résultat surréaliste ne devrait surprendre personne,
hélas.

#raW: Pernille Frahm, députée danois et vice-présidente du Groupe GUE/NGL,
constate que la Commission européenne et le Conseil abusent de leurs
fonctions et ne jouent pas leur rôle dans la législation européenne :

#fue: Les administrateurs de brevets à la Commission et au Conseil
maltraitent la procédure législative de l'UE.

#oeh: Leur texte sur les brevets dans une novlangue compliquée et
fallacieuse, négocié dans d'opaques transactions en coulisses, est une
insulte au Parlement européen, au Comité économique et social
européen, au Comité des régions et aux innombrables experts et groupes
d'intérêts concernés qui ont entrepris avec nous de sérieuses
investigations sur ce projet de directive. Non seulement la Commission
et le Conseil ne font pas leur propre travail mais maintenant ils
essayent également de gâcher tout le difficile travail que les
législateurs élus ont fait pour eux, sans même essayer de répondre aux
inquiétudes qui ont été soulevées.

#toa: Le Groupe de travail du Conseil a jusqu'ici complètement échouer à
aborder les problèmes que les commissions parlementaires à la Culture
et à l'Industrie avaient essayé de résoudre. Il se comporte comme la
commission Juridique l'année dernière et on peut s'attendre à ce qu'il
échoue de la même manière.

#tec: Il est clair que les fonctionnaires nationaux des brevets au sein du
Conseil ne veulent pas %(q:d'harmonisation) ou de %(q:clarification).
Ils veulent tout simplement garantir les intérêts de l'establishment
des brevets. S'ils n'obtiennent pas ce qu'ils veulent, ils enterreront
tout bonnement le projet de directive et essayeront de trouver
d'autres moyens de venir à bout du droit existant, dont la clarté leur
est si douloureuse.

#jWW: Johanna Boogerd-Quaak, députée néerlandaise du Parti européen des
libéraux, démocrates et réformateurs, indique que l'Irlande semble
jouer les toutous des USA :

#uWl: J'ai comme l'impression que la Présidence irlandaise s'est courbée
sous les intérêts des compagnies américaines. Une poignée de grandes
entreprises américaines peut vraiment profiter des brevets logiciels
mais c'est une très mauvaise affaire pour l'innovation des PME
européennes. En outre, le Conseil montre du mépris pour la démocratie
parlementaire. Nous devons nous assurer qu'après les élections il y a
encore encore au Parlement européen une majorité disposée à montrer
ses dents.

#itu: Plus de 20 parlementaires européens ont signé un %(ca:Appel à
l'action) soulignant que %(q:des professionnels des brevets dans
divers gouvernements et organisations tentent maintenant d'utiliser le
Conseil des ministres de l'UE afin de se soustraire à la démocratie
parlementaire au sein de l'Union européenne) et poussent le Conseil
avec insistance %(q:à s'abstenir de présenter toute contre-proposition
à la version du projet du Parlement Européen, à moins que cette
contre-proposition n'ait été soutenue par un vote à la majorité du
Parlement de l'État membre concerné).

#Edg: Le puissant Comité des Représentants permanents (COREPER) des États
membres de l'UE à Bruxelles s'est provisoirement accordé sur une
nouvelle ébauche de la directive controversée sur les brevets
logiciels, passant outre l'opposition de l'Allemagne, de la Belgique,
du Danemark et de la Slovaquie.

#Wjm: Le nouveau document de travail rejette tous les amendements du
Parlement européen limitant la brevetabilité. Il est décrit par la
FFII comme %(q:le texte le plus radicalement pro-brevets à ce jour).

#ima: Techniquement, la décision de mercredi par le COREPER n'est qu'une
%(q:anticipation) de la décision finale qui doit être confirmée par le
Conseil des ministres sur la compétitivité les 17 et 18 mai. Jusqu'à
cette date, les États membres peuvent encore changer d'avis (et de
vote).

#fWs: Certains états soutiendraient encore assez mollement le texte au
niveau politique ; et il arrive aux décisions négociées par le COREPER
 de tomber en morceaux (les discussions de l'année dernière sur le
Brevet communautaire, par exemple).

#Won: Le texte du COREPER va plus loin que celui élaboré par la Commission
europénne en 2002 en ce qui concerne la légalisation des brevets
logiciels. En 2002, la Commission avait accepté, après de difficiles
négociations entre la Direction générale Marché intérieur (Bolkestein)
et la DG Société de l'information (Liikanen), de ne pas autoriser les
revendications de programmes. Il semble maintenant que la DG Société
de l'information ait fait demi-tour sous la pression conjointe de
Bolkestein et des administrateurs de brevets du Conseil.

#gie: Une %(ld:fuite d'un document de la DG Marché intérieur de Bolkestein)
recommande à la DG Société de l'information de ne plus s'opposer aux
revendications de programmes. Cette concession de Liikanen est
nécessaire pour précipiter la proposition du Groupe du travail du
Conseil à la réunion des ministres en tant que %(q:point A), i.e. un
point de concensus n'appelant pas de discussion entre les ministres.

#rWh: La semaine prochaine, la FFII appelle à une nouvelle grêve en ligne et
à une vague %(sd:d'actions et de manifestations locales). Déjà ces
jours-ci, des personnes sont en train de manifester avec une banderole
à proximité des bureaux de la Commission.

#5Wa: Les documents du Conseil 8253/04 et 8253/04 ADD du 6 avril ne sont pas
accessibles %(q:à cause de la nature sensible des négociations et en
l'absence d'un intérêt public supérieur) selon le Secrétariat général
du Conseil.

#tah: %(jm:Lettre) écrite au Secrétariat général du Conseil, faisant appel
du refus de publier les documents du 6 avril (en anglais).

#oeW: Nous avons fourni une %(an:analyse complète) du dernier %(q:compromis)
du Conseil. La dernière version provient d'une fuite qui nous est
parvenue et qui est accessible dans la section des %(L).

#hai: Un %(bv:document du Ministère autrichien des technologies) que nous
nous sommes proccuré grâce à une fuite et qui montre un bon exemple de
la prise de décision dans la proposition du Conseil. Les députés
autrichiens de tous les partis ont majoritairement voté en faveur des
amendements du Parlement. Othmar Karas,, à la tête du Parti populaire
autrichien au Parlement européen avait écrit au Ministre des
technologies en lui demandant de ne pas soutenir la position du
Conseil mais n'a pas reçu de réponse. Au lieu de ça, le ministre a
laissé toutes les décisions à son bureau des brevets, qui a toujours
suivi à 100% le Bureau européen des brevets.

#sno: La Présidence irlandaise explique sur son site web qu'elle est
%(ms:sponsorisée par Microsoft). L'%(ie:Irlande) est %(q:le plus gros
pays exportateur de logiciels en Europe), grâce à une politique
fiscale qui en fait un paradis fiscal pour les grosses entreprises des
USA : son taux d'imposition sur les revenus des brevets est de 0%.

#rcn: Nouvelles plus récentes

#Ceo: Plus de contacts peuvent être fournis sur simple demande

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040507 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

