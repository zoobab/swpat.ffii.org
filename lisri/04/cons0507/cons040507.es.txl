<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EU Council Plans to Scrap Parliamentary Vote without Discussion

#descr: The EU Council of Ministers is demonstrating that the concept of democracy is alien to the EU.  This Wednesday, the Irish Presidency managed to secure a qualified majority for a counter-proposal to the software patents directive, with only a few countries - including Belgium and Germany - showing resistance. The new text proposes to discard all the amendments from the European which would limite patentability.  Instead the lax language of the original Commission proposal is to be reinstated in its entirety, with direct patentability of computer programs, data structures and process descriptions added as icing on the cake. The proposal is now scheduled to be confirmed without discussion at a meeting of ministers on 17-18 May, unless one of the Member States changes its vote.  In a remarkable sign of unity in times of imminent elections, members of the European Parliament from all groups across the political spectrum are condemning this blatant disrespect for democracy in Europe.

#nCr: Proposals slammed by MEPs from all sides

#kWa: More Background Information

#dca: Contactos de los medios

#otF: Acerca de la FFII

#nWW: URL permanente de esta Nota de Prensa

#MEP: MEP

#PSE: PES

#mbe: Parliament Overruled by Patent Office Administrators

#ien: Council Ignores Elected Representatives

#WFi: Council and Commission Failed to do their Homework

#ici: Patent Administrators Not Interested in %(q:Harmonisation and Clarification)

#mSW: Council Moves Highlight Lack of Democracy in the EU

#eWU: Irish Presidencey Protecting US Companies from EU Competition

#atW: Anne Van Lancker, a Belgian MEP of the Socialist group, notes that the civil servants who are supposed to abide by this directive are now the ones who can write their own laws:

#anj: The Council not only ignores the European Parliament in this matter, but adds to the insult by going even further than the Commission: in addition to making the usage of patented algorithms and business methods in computer programs an infringement, they also propose to forbid their publication, by allowing direct claims to information entities (also known as %(q:program claims)).

#CeW: Given that the current Council proposal was written behind closed doors by patent office administrators, this unworldly outcome should not surprise anyone, unfortunately.

#raW: Pernille Frahm, Danish member and Vice-Chairwoman of the GUE/NGL group, finds that the European Commission and Council are abusing their functions and failing to play their role in EU legislation:

#fue: The patent administrators in the Commission and Council are abusing the legislative process of the EU.

#oeh: Their convoluted and misleading Patent Newspeak, negotiated in intransparent backroom dealings, is an insult to the European  Parliament, the European Economic and Social Committee, the Committee of Regions and the innumerable experts and stakeholders who have engaged in serious investigations on this directive project with us.  Not only did the Commission and Council fail to do their own homework, they are now also attempting to throw away all the hard work that the elected legislators did for them, without even trying to respond to the concerns which have been raised.

#toa: El Partido de Trabajo del Consejo no pudo hasta ahora totalmente  tratar los problemas que los comités de los asuntos culturales y  laborales del Parlamento Europeo intentaron solucionar.  Se comportan de  la misma manera que las comisiones de asuntos jurídicos se comportaron  el año pasado, y podemos contar con que fallen de la misma manera.

#tec: Está claro que los funcionarios nacionales de las patentes en el  consejo no desean %(q:armonización) o %(q:clarificación). Desean  simplemente asegurar los intereses del establecimiento de patentes. Si  no consiguen lo que buscan, simplemente entierran el proyecto de la  directiva e intentan buscar otras formas de conseguirlo alrededor de la  ley existente, tanta claridad es dolorosa para ellos.

#jWW: Johanna Boogerd-Quaak, a Dutch member of the European Liberal, Democrat and Reform Party, indicates that Ireland seems to be playing lapdog for the US:

#uWl: I'm under the impression that the Irish Presidency has buckled under the interests of American Companies. A handful of big American Companies may actually profit from software patents, but it is a very bad deal for innovation in European SMEs.  Additionally, the Council is showing contempt for parliamentary democracy.  We must make sure that after the elections there will again be a majority in the European Parliament that is willing to show its teeth.

#itu: Los procedimientos intransparentes y el acercamiento no  constructivo del consejo también han dado lugar a otra %(ca:Llamada a la  Acción), la cual numerosos parlamentarios han firmado desde su  publicacion el pasado Noviembre.  Nokia parece haber pedido prestado el  titulo de su carta de su bien conocida %(q:Llamad a la Acción).

#Edg: The powerful COREPER committee of EU member states' Permanent Representatives in Brussels has provisionally agreed on a new draft for the controversial Software Patent directive, overruling opposition from Germany, Belgium, Denmark and Slovakia.

#Wjm: The new draft rejects all of the European Parliament's limiting amendments, and is described by FFII as %(q:the most uncompromisingly pro-patent text yet).

#ima: Technically, the decision by COREPER on Wednesday is only a %(q:forecast) of the final decision, to be confirmed at the Competitiveness Council of Ministers on 17-18 May.  Until that date, Member states can still change their minds (and their votes).

#fWs: Support for the text at a political level in some states is still said to be quite soft; and decisions brokered in Coreper do fall apart (last year's discussions on the Community Patent, for example).

#Won: The Coreper text goes further than the text of the European Commission of 2002 in legalising software patents.  In 2002 the Commission had agreed, in difficult negotiations between DG Internal Market (Bolkestein) and DG Informations Societey (Liikanen) not to allow program claims.  Now it seems that DG Information Society has rolled over to the united pressure of Bolkestein and the Council's patent administrators.

#gie: A %(ld:leaked document from Bolkestein's DG Internal Market) suggests that DG Information Society no longer objects to program claims.  This concession by Liikanen is needed in order to rush the Council working group proposal through the ministers' session as an %(q:A item), i.e. a consensus point which does not need any discussion by the ministers.

#rWh: For next week, the FFII is calling for another net strike and a wave of %(sd:local events and demonstrations).  Even these days people are demonstrating with banners near the offices of the Commission.

#5Wa: The Council proposal documents 8253/04 and 8253/04 ADD from April 6 are not accessible %(q:due to the sensitive nature of the negotiations and the absence of an overriding public interest) according to the Council's General Secretariat.

#tah: %(jm:Letter) written to the Consilium's General Secretariat, appealing the refusal to publish the 6 April documents.

#oeW: We have provided a %(an:thorough analysis) of the latest Council %(q:compromise).  The latest version was leaked to us and is accessible through the %(L) section.

#hai: A %(bv:leaked document from the Austrian Ministry of Technology) exemplifies the decisionmaking behind the Council's proposal.   Austrian MEPs of all parties had mostly voted in favor of the Parliament's amendments.  Othmar Karas, head of the Austrian People's Party in the EP, had written to the Minister of Technology asking him not to support the Council position, but did not receive any reply.  Instead the ministry left all decisions to its patent office, which has always been a 100% follower of the European Patent Office.

#sno: The Irish Presidency explains on its website that it is %(ms:sponsored by Microsoft).  %(ie:Ireland) is %(q:the largest software-exporting country in Europe), thanks to a fiscal policy which makes it a tax haven for large US companies: it has a tax rate on patent revenues of 0%.

#rcn: More news

#Ceo: More Contacts to be supplied upon request

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/cons040507.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040507 ;
# txtlang: es ;
# multlin: t ;
# End: ;

