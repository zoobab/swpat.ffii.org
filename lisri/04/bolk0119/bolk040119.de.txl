<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Niedersachsen wirft Bolkestein Unredlichkeit vor

#descr: Die Niedersächsische Landesregierung rebelliert gegen
EU-Binnenmarktkommissar Frits Bolkesteins Bemühungen um eine
Richtlinie zur Entflechtung regionaler Industriekonglomerate und
Erleichterung von Übernahmen an der Börse.  In einem Schreiben an
Kommissionspräsident Prodi und diverse EU-Kommissare wirft die
Landesregierung Bolkesteins Apparat ausdrücklich unredliche
Argumentationstaktiken vor.  Der FFII fühlt sich an Erfahrungen mit
dem gleichen Apparat im Zusammenhang mit anderen Richtlinien erinnert.

#tKs: Im Handelsblatt vom 2004-01-16 wird über einen Kampf der
Niedersächsischen Landesregierung berichtet.

#see: Es geht nicht um Patente, sondern um Bolkesteins Vorgehen gegen das
%(q:VW-Gesetz), das der Niedersächsischen Landesregierung natürlich
nicht gefällt. Ministerpräsident Christian Wulff (CDU) hat deshalb
alle 19 EU-Kommissare und den Kommissionspräsidenten Romano Prodi
angeschrieben mit der Bitte, dem von Bolkestein vorangetriebenen
Verfahren nicht zuzustimmen. Die beiden deutschen, den belgischen und
die beiden französischen Kommissare haben inzwischen Vorbehalte gegen
Bolkesteins Vorgehensweise erhoben.

#kad: Interessant ist, dass Bolkestein in diesem Zusammenhang von der
Niedersächsischen Landesregierung ganz offiziell eine unredliche
Darstellung des Sachverhalts vorgeworfen wird:

#ske: Über das offizielle Schreiben hinaus verteilt die Niedersächsische
Staatskanzlei an Spitzenbeamte der Kommission %(q:Richtigstellungen)
zu Bolkesteins wichtigsten Behauptungen gegen das VW-Gesetz. Ein
Mitarbeiter des Ministerpräsidenten rechtfertigt das ungewöhnliche
Vorgehen. Denn in den Dokumenten der Kommission zu dem Fall würden die
deutschen Gegenargumente %(q:systematisch totgeschwiegen).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bolk040119 ;
# txtlang: de ;
# multlin: t ;
# End: ;

