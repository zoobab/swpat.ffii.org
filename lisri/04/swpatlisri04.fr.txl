<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Nouvelles sur les Brevets Logiciels en 2004

#descr: Ce que la FFII avait pour rapporter en 2004 sur les monopoles
d'invention accordés par l'état et leur extension abusive sur la
pensée, le calcul, l'organisation et la formulation a l'aide de
l'ordinateur universel.

#Zdf040824T: German TV Examines FFII Complaint

#Zdf040824D: The German State TV Channel ZDF.de is examining a complaint by FFII
about news film of July 6th in which journalist Manfred Ahlers had
reported about a %(q:summit) of the german patent establishment with
the Siemens boss H.v. Pierer and chancellor Schröder as keynote
speakers.  Ahlers wrongly reported that Schröder had called for
software patentability and that %(q:theft of ideas caused billions of
euro of damage to the software industry every year), using a interview
with a BSA official about the unrelated subject of copyright
violations in order to confuse the issues and fabricate a consensus in
favor of software patents, in the same way as the BSA-written
%(q:explanatory memorandum) of the European Commission's directive
proposal attempted this in 2002.  The Ahlers report falsely presents
FFII as a lobby group of copyright violators campaigning for a right
to %(q:freely reuse programs written by others).  The suggestive
techniques and circumstances make it difficult to assume that this was
done by mistake.  FFII submitted a formal complaint to ZDF's
multi-partisan supervisory body asking for compensatory measures.  The
director of the body, MP Rupert Polenz, has affirmed receipt of the
complaint and forwarded it to the program director (Intendant).  A
further decision will be taken after receipt of the program director's
opinion.

#Cons040518T: Thin Majority of Ministers Approve Software Patents, Swayed by Bogus
Compromise

#Cons040518D: The Irish presidency has secured political approval for a new draft of
the controversial software patents directive in a meeting of the
Council of Ministers on Tuesday, 2004-05-18.  The draft and
accompanying press releases are full of statements of good intentions
to avoid patents on software and business methods as such, whereas the
provisions in the text assure that such items are without any doubt to
be treated as patentable inventions in Europe.  The draft owes its
majority to a maneuver by the German delegation, which had collected
the opposition under its flag, to settle for a bogus amendment in the
last minute and take the Poles and Latvians with it.  Representatives
of the Netherlands, Hungary, Denmark and France apparently acted in
breach of promises given to their parliaments or governments.

#Cons040507T: EU Council Plans to Scrap Parliamentary Vote without Discussion

#Cons040507D: The EU Council of Ministers is demonstrating that the concept of
democracy is alien to the EU.  This Wednesday, the Irish Presidency
and the European Commission managed to secure support for a
counter-proposal on the software patents directive, with only a few
delegations - including Belgium and Germany - showing resistance. The
new text proposes to discard all the amendments from the European
Parliament which limit patentability.  Instead the lax language of the
original Commission proposal is to be reinstated in its entirety, with
direct patentability of computer programs, data structures and process
descriptions added as icing on the cake. The proposal is now scheduled
to be confirmed without discussion at a meeting of ministers on 17-18
May, unless one of the Member States changes its vote.  In a
remarkable sign of unity in times of imminent elections, members of
the European Parliament from all groups across the political spectrum
are condemning this blatant disrespect for democracy in Europe.

#Cons040408T: The gloves come off for Round Two in the EU fight over Software
Patents

#Cons040408D: After months of closed back room discussions, the Irish Presidency of
the European Union has referred the proposed EU Directive on software
patents back up to %(q:political) level.  The Irish want members of
the Council of Ministers of the member states to agree to drop all
objections by May.  The Presidency proposed draft text rejects all
clarifying amendments made by the European Parliament in September
2003 and instead pushes for direct patentability of computer programs,
data structures and process descriptions.  A last ditch attempt by the
Luxembourg delegation to ensure interoperability with patented
standards was rejected.  The Patent Department at Nokia is collecting
signatures from top company executives for a %(q:Call for Action) in
favour of the Presidency text.  In the other corner, supporters of the
European Parliament's position have arranged conferences to explain
the dangers of software patents, and are mobilising for a %(q:net
strike) and a rally in Brussels on April 14th under the slogan %(q:No
Software Patents -- Power to the Parliament).  They are hoping for a
repeat of the impact of similar actions in the run-up to September
2003, which helped convince the European Parliament to vote clearly
against software patents.

#Cons040402T: Council Working Party %(q:compromises) on unlimited patentability and
unfettered patent enforcement

#Cons040402D: In the Council of Ministers of the European Union, national patent
administration officials have, after several months of secret
negotiations about the proposed EU software patent directive, have
reached the end of several months of secret negotiations about textual
questions.  The resulting %(q:compromise document) rejects all
clarifying amendments made by the European Parliament and instead
pushes for direct patentability of computer programs, data structures
and process descriptions.  A last minute attempt by the Luxemburg
delegation to allow interoperation with patented standards was
rejected.  Many other delegations have expressed reservations about
the document, which are inserted as anonymised footnotes.  Yet the
Irish Council presidency wants the ministers to jointly adopt this
text at the %(q:Competitivity Council) meeting on May 17-18.

#Cecms040326T: EU Boosts Microsoft's Monopoly

#Cecms040326D: The European Commission's competition procedings against Microsoft
have led to a verdict which gives a big boost to Microsoft's monopoly
position in the OS market and helps Microsoft expand this position to
other markets.  While the Commission may have earned substantial
revenues for itself by imposing a one-time fine of 1% of Microsoft's
liquid cash reserves, the smallprint of the verdict gives Microsoft
green light to kill its main competitors in the operating systems
market.  This smallprint was simultaneously reinforced through
backroom deals in the Council's Patent Policy working party, of which
copies have been leaked to FFII.  Immediately after the announcments
the stock value of MSFT rose by 3%.

#Cons040317T: Council Presidency 2004-03-17 %(q:Compromise Paper) on Software Patent
Directive

#Cons040317D: published 2-3 weeks later on the consilium site.

#Cecms040202T: EU vs MS Antitrust Procedings: Exclusion of Free Software from
Patented Standards Deemed Reasonable

#Cecms040202D: In its anti-trust procedings against Microsoft, the European
Commission has found that Microsoft is trying to extend its desktop
monopoly into the market for %(tp|workgroup servers|file, print, mail
and web servers) by keeping secret the communications protocols that
enable its desktop and server products to talk to each other.
%(q:Without such information, alternative server software would be
denied a level playing field, as it would be artificially deprived of
the opportunity to compete with Microsoft's products on technical
merits alone,) the Commission warned in 2001. Yet, the Commission has
stopped short of demanding that Microsoft make its protocols freely
available.  According to an article in the Economist, Microsoft will
only be required to license its server-communications protocols to
rivals on a %(q:reasonable and non-discriminatory) basis, which means
e.g. that free/opensource software projects such as Samba can be
excluded, and a look at Microsoft's recently published aggressive
licensing program shows that indeed they are. Another arm of the
Commission is fighting for the right of Microsoft and other companies
to forbid interoperation with any patented standard which they chose
to impose by their market power.  Bolkestein's Industrial Property
Unit has been urging the Council members to remove Art 6a of the
European Parliament's Software Patent Directive Proposal.  Art 6a
demands that the use of a patented technique for mere interoperation
purposes should always be royalty-free.

#Oecd040130T: OECD ministers call for closer scrutiny of the patent system

#Oecd040130D: The Paris based Organisation for Economic Cooperation and Development,
in which the world's wealthier countries are members, has issued a
report on problems with the patent system.  OECD calls for %(q:closer
scrutiny by science, technology and innovation policy makers) of
patent regimes, warning that governments %(q:must remain vigilant in
ensuring that patenting does not unnecessarily hinder access to
knowledge, reduce incentives to disseminate knowledge, or impede
follow-on innovation).  The report specially names software, business
method and gene patents as fields where the efficiency of the patent
system seems particularly questionable.

#Bmss040130T: Bertelsmann Tocher wegen eCommerce-Patent vor Gericht

#Bmss040130D: Vor dem Bezirksgericht in Pittsburgh soll eine Patentverletzungklage
in dreistelliger MillionenhÃ¶he gegen CD Now, eine E-Commerce Tochter
von Bertelsmann verhandelt werden. Bereits 1998 hat Sightsound
Technologies die Klage gegen die Bertelsmann-Tochter wegen
Patentverletzung eingereicht.  1993 erhielt Sightsound - GrÃ¼nder Art
Hair ein erstes US-Patent auf das Verfahren fÃ¼r den Austausch von
Musik als komprimierter Internet-Datenstrom gegen Zahlung.  Ã?hnliche
Patente sind auch vom EuropÃ¤ischen Patentamt gegen den Buchstaben und
Geist des Gesetzes erteilt worden.  WÃ¤hrend das EuropÃ¤ische
Parlament im September 2003 solche Patente erneut fÃ¼r illegal
erklÃ¤rt hat, drÃ¤ngt die Patentarbeitsgruppe des Rates der
EuropÃ¤ischen Union (ein Gremium der Betreiber des EuropÃ¤ischen
Patetamtes) auf ihre Legalisierung.

#Mstv040129T: Microsoft patents upcoming Digital Media Standards

#Mstv040129D: Microsoft has just been awarded %(tp|two patents|US 6,510,177 and US
6,683,980) for a method of packing a high-definition television signal
onto a hard drive, internetnews.com reports.  The article goes on to
quote anonymous experts who praise the methods patented here are basic
and advantageous building blocks of any future digital media standards
and speculate that Microsoft's %(q:Windows Media 9 technology) will
become irresistible if it is the only one that can use these methods. 
Microsoft has meanwhile published licensing conditions for its
protocols which make it clear that they cannot be used by opensource
software.

#Cons040129T: EU Council %(q:Compromise) for Unlimited Patentability

#Cons040129D: In September 2003, the European Parliament had voted to maintain and
reinforce the exclusion of software and business methods from
patentability.  The Council of the Ministers of the European Union,
currently presided by Ireland, is now circulating a Working Paper with
counter-proposals to the European Parliaments position.  In contrast
to the European Parliament's version, the Council version allows
unlimited patentability and patent enforcability.  Under the Council
version, %(q:computer-implemented) algorithms and business methods, as
have been granted in large numbers by the European Patent Office
against the letter and spirit of the written law, are by default
patentable inventions.  Publication of a description of a patented
idea on a web server in formal language constitutes a patent
infringment, and use of patented protocols and file formats for the
purpose of interoperation is not allowed.  This proposal was prepared
behind closed doors by the Council's Patent Working Party, a group of
patent administrators who run the European Patent Office.  The Patent
Working Party has been encouraged in its uncompromising pursuit of
unlimited patentability by a campaign of letters to top-level
politicians, signed by CEOs of some of Europe's largest patenting
companies.  These letters falsely claim that computer-controlled
industrial processes are made unpatentable by the Parliament's
amendments and that software innovation is driven by patents.  In the
context of another directive, the EU Council is currently pressing the
EU Parliament to mandate criminal sanctions even for minor cases of
patent infringment.

#Sver040128T: Public letter of 5 Swedish CEOs against European Parliament's
Amendments

#Sver040128D: CEOs of 5 of the largest patenting companies in Sweden falsely accuse
the European Parliament of having voted to make software-controlled
%(tp|industrial production processes|e.g. robots of ABB) unpatentable.
 Based on this false accusation, they demand support for the position
of the European Commission and European Council, which is to legalise
30000 patents on pure software and business methods which have been
granted by the European Patent Office in recent years against the
letter and spirit of the existing law.  The CEOs moreover make it
clear that %(q:harmonisation and clarification): they want either a
full codification of unlimited patentability according to the EPO
doctrines or no directive at all.  Similar letters have been signed by
prominent industry executives and sent to heads of government.  This
is the first time that they are directly going public with such a
letter.  The letter comes after a statement by Sweden's Justice
Minister Thomas BodstrÃ¶m in support

#Amaz040125T: FFII Opposition against Amazon

#Amaz040125D: FFII has filed an opposition at the %(tp|European Patent Office|EPO)
against the Gift-Ordering Patent of Amazon.  The FFII is asking the
Technical Board of Appeal to revoke the patent and to return a more
straightforward understanding of the European Patent Convention, which
alone could securely prevent the granting of masses of broad and
trivial monopolies on rules of organisation and calculation.  This
demand of the FFII is backed by a petition which has been signed by
numerous politicians, entrepreneurs and organisations.

#Bolk040119T: Niedersachsen wirft Bolkestein Unredlichkeit vor

#Bolk040119D: Die Niedersächsische Landesregierung rebelliert gegen
EU-Binnenmarktkommissar Frits Bolkesteins Bemühungen um eine
Richtlinie zur Entflechtung regionaler Industriekonglomerate und
Erleichterung von Übernahmen an der Börse.  In einem Schreiben an
Kommissionspräsident Prodi und diverse EU-Kommissare wirft die
Landesregierung Bolkesteins Apparat ausdrücklich unredliche
Argumentationstaktiken vor.  Der FFII fühlt sich an Erfahrungen mit
dem gleichen Apparat im Zusammenhang mit anderen Richtlinien erinnert.

#Acacia040116T: EPO grants video streaming patent to Acacia

#Acacia040116D: The European Patent Office has granted the US patent rent extraction
company Acacia Research a patent with a broad claim on almost any
practical method of making videos available via web.  Acacia has
enforced the US cousin of this patent against numerous porn websites
as well as universities and has also been sending warning letters to
companies in Europe.

#Sver040116T: Swedish Government Fighting for Software Patents

#Sver040116D: In a response to a parliamentary question in december 2003, Sweden's
minister of justice Thomas Bodström endorsed the approach of the
European Patent Office and the European Commission on software patents
and criticised the European Parliament for amending the directive with
the effect of narrowing the scope of patentability and making
already-granted patents invalid. Bodström announced that his
government will push for reversal of these amendments. His comments
provoked applause from the %(tp|patent movement|also called the
%(q:technostructure) in Sweden) and criticism from some software
associations and companies.

#Acacia040115T: EPO grants video streaming patent to Acacia

#Acacia040115D: The European Patent Office has granted the US patent rent extraction
company Acacia Research a patent with a broad claim on almost any
practical method of making videos available via web.  Acacia has
enforced the US cousin of this patent against numerous porn websites
as well as universities and has also been sending warning letters to
companies in Europe.

#Kober040114T: EPO answers to Kauppi questions available

#Kober040114D: A spokeseman of the European Patent Office has provided official
answers of the EPO to questions which MEP Piia-Noora Kauppi submitted
to president Kober before his visit to the European Parliament on
November 27.  The answers are evasive and it is unclear whether Kober
himself stands by them.

#Amcc040113T: Rebellion in Arlene McCarthy's constituency

#Amcc040113D: Parts of the Labour party and electorate of Arlene McCarthy's district
are asking to have her replaced.  McCarthy responds by pointing to her
affiliation with the regional venture capital scene (= patent
movement) and claiming merits in bringing special EU funds to her
regions.

#Ie040109T: Irish EU Presidency to %(q:protect software inventions) in May

#Ie040109D: The Irish vice prime minister has unveiled a brochure which describes
the agenda of the Competitiveness Council of the Irish EU Presidency. 
The brochure places high emphasis on the Community Patent and the IP
Enforcement Directive and somewhat lower emphasis on the software
patent directive, although it asserts that %(q:effective instruments)
for %(q:protection) of %(q:software inventions) form an %(q:important
underpinning) of the %(q:knowledge based economy).  The IE Presidency
will try to bring about an agreement on the software patent directive
at the May meeting of the Competitiveness Council.

#Ieeu040109T: Irish EU Presidency to %(q:protect software inventions) in May

#Ieeu040109D: The Irish vice prime minister has unveiled a brochure which describes
the agenda of the Competitiveness Council of the Irish EU Presidency. 
The brochure places high emphasis on the Community Patent and the IP
Enforcement Directive and somewhat lower emphasis on the software
patent directive, although it asserts that %(q:effective instruments)
for %(q:protection) of %(q:software inventions) form an %(q:important
underpinning) of the %(q:knowledge based economy).  The IE Presidency
will try to bring about an agreement on the software patent directive
at the May meeting of the Competitiveness Council.

#Arthur040107T: The Independent: Software Patenting Discourages Innovation

#Arthur040107D: The british daily newspaper %(q:The Independent) has an
%(ah|http://news.independent.co.uk/digital/features/story.jsp?story=47
8647|article) by Charles Arthur titled %(q:The patenting of software
is a complete mess and discourages innovation).  The article explains
the reasoning that leads to software patents and points to the
abstract nature of software innovation of one of the reasons why the
results are unsatisfactory.  The article mentions the European Patent
Office's handling of the Amazon patent but not the september vote of
the European Parliament.

#Ebay040106T: EBay patent dispute with Tumbleweed settled

#Ebay040106D: Tumbleweed Communications is one of a handful of patent companies that
are suing EBay/Paypal for infringment of Internet patents.  EBay was
condemned to pay large indemnity sums to CertCo and litigation with
MercExchange and AT&T is still pending.  Now Tumbleweed and EBay have
reached an out-of-court settlement whose details are unknown.

#Cisco040105T: Cisco to bridge the IP worlds

#Cisco040105D: Cisco Systems is introducing the Internet culture of open standards
into the heavily patented telephone industry where strategic patenting
is abundant. Sandbridge Technologies filed at least four virtual
patents at WIPO in 2003, all covering methods of data exchange,

#Eolas040104T: A workaround for the Eolas patent

#Eolas040104D: The Eolas patent ruling against Microsoft means Web pages that use
embedded plugins are infringing on the US patent. Microsoft recommends
a procedure to filter your HTML. They released information on a
workaround that would allow web sites to continue to use ActiveX
controls, Flash and other plugins without infringing on the patent.
Web developers have to invest time and money in filter software. A
DevX.com article explains how to do it with a homebrewn Java Server
filter.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri04 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

