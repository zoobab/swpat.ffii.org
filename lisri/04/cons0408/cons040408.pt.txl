<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Luvas removidas para Segundo Round na luta Europeia sobre Patentes de
Software

#descr: Após meses de negociações nos bastidores, a Presidência Irlandesa da
União Europeia trouxe novamente ao nível %(q:político) a proposta de
directiva Europeia sobre patentes de software. Os Irlandeses querem
que os membros do Conselho de Ministros dos estados-membros concordem
em desistir de quaisquer objecções até Maio. O rascunho do texto
proposto pela Directiva rejeita todas as emendas clarificativas feitas
pelo Parlamento Europeu em Setembro de 2003 e em vez delas força a
patenteabilidade directa de programas de computador, estruturas de
dados e descrições de processos. Uma tentiva de último recurso da
delegação Luxemburguesa para assegurar a interoperabilidade com
standards patenteados foi rejeitada. O Departamento de Patentes da
Nokia está a coleccionar subscritores de executivos de topo numa
%(q:Chamada à Acção) a favor do texto da Presidência. Do outro lado,
apoiantes da posição do Parlamento Europeu organizaram conferências
para explicar   os perigos das patentes de software, e estão a
mobilizar uma %(q:greve online) e um desfile em Bruxelas a 14 de Abril
sob o slogan %(q:Patentes de Software Não -- Poder ao Parlamento).
Eles esperam que se repita o impacto de acções similares às de
Setembro de 2003, que ajudaram a convencer o Parlamento Europeu a
votar claramente contra patentes de software.

#ssi: Regresso das Patentes de Software ào centro do Palco Político Europeu

#oio: %(q:Chamada à Acção) do Departamento de Patentes da Nokia

#nCr: Reacções de Eurodeputados à posição do Grupo de Trabalho do Conselho

#dje: Texto da Presidência Rejeita Interoperabilidade

#Slf: Greve On-line, Desfiles, Conferências

#dca: Contactos para os media

#otF: Sobre a FFII

#nWW: URL permanente deste Press Release

#eor: Após meses sonegadas do escrutínio público em Bruxelas, o debate sobre
a controversa Directiva Europeia sobre patentes software está a
retornar à ribalta política. Terça-feira, 6 de Abril, a Presidência
Irlandesa da UE referiu o assunto para os Representantes Permanentes
dos estados-membros no comité CoRePer, a via tradicional para
negociação de cavalos políticos difíceis.

#W1x: Em resposta, os proponentes pro e contra patentes de software estão a
mobilizar os seus constituintes para apoiar ou o Conselho ou o
Parlamento. Os apoiantes do Parlamento anunciaram um dia de acção
massiva em Bruxelas na Quarta-feira, 14 de Abril, culminando numa
conferência de alto nível no próprio edifício do Parlamento Europeu.
Adicionalmente, urgem aos seus apoiantes que entrem %(q:em greve
on-line) na próxima semana, bloqueando os seus sítios na web para
%(q:demonstrar os efeitos das patentes de software antes que seja
tarde de mais).

#rxr: A movimentação da Presidência Irlandesa Terça-feira para referir o
dossier não é inesperada, mas marca um retorno significante do dossier
ao centro do palco político.

#Wcu: De acordo com uma fonte conhecedora da posição da Presidência, %(q:tem
havido um progresso evidente no Grupo de Trabalho com problemas que
alguns estados-membros tinham em assuntos particulares de
terminologia; mas ainda há diferenças significativas entre
estados-membros em alguns dos assuntos-chave fundamentais. A sensação
geral é que o trabalho ao nível técnico foi tão longe quanto poderia,
sendo agora necessário um árduo trabalho ao nível político, caso se
pretende obter um consenso geral até Maio).

#sih: Os estados-membro deverão assinar a sua posição comum no encontro do
Conselho do Conselho de Ministros da Competitividade que decorrerá em
Bruxelas a 17 e 18 de Maio.

#AWW: Os lobbies a favor das patentes de software estão a preparar-se para a
luta. A FFII obteve uma cópia de uma %(no:carta) posta em circulação
por Tim Frain (%(Nokia)/Southwood) e Dany Ducoulombier
(Nokia/Bruxelas) para subscritores pro-patentes antes de 8 de Abril. A
carta apela aos ministros que deixem de objectar, e que suportem o
rascunho do texto emitido pela Presidência Irlandesa em 17 de Março:

#dWi: %(q:Todos os inovadores Europeus, incluindo inventores individuais,
%(tp|pequenas e médias empresas|PMEs), bem como grandes
multinacionais, exigem patentes para proteger as suas invenções,
providenciar incentivos para levar a cabo investigação e
desenvolvimento na Europa, e para promover licenciamento e
transferências de tecnologia), reivindica a carta.

#ohe: %(q:A Nokia não deve estar a contar com a Opera entre os inovadores
Europeus), comenta Håkon Wium Lie, CTO da Opera Software Inc, um líder
de inovação no mercado dos browsers da web e produtor de muito do
software utilizado nos telefones da Nokia.

#onr: E tal como Hartmut Pilch, presidente da FFII e orador da Aliança
Eurolinux, explica, a Opera é apenas uma em milhares de companhias
Europeias inovadoras que publicamente apoiam as nossas petições contra
patentes de software.

#lou: Pilch continua:

#Wsb: A reivindicação do departamento de patentes da Nokia de que as
patentes são necessárias para dar fundos à investigação no sector do
software parecem uma tentativa desesperada de capitalizar no
desconhecimento das pessoas que não estão familiarizadas com o sector
das Tecnologias de Informação e Comunicação. Todos os %(ss:estudos
económicos) que conhecemos, incluindo aqueles encomendados pela
Comissão Europeia e governos dos estados-membros, demonstram que as
patentes de software são de uma importância muito secundária como meio
de captar os retornos de investimento em Investigação e
Desenvolvimento. Os eixos principais de vantagem competitiva são o
direito de autor, segredo comercial, complexidade, efeitos de rede e a
capacidade de reagir rapidamente às necessidades dos clientes. De
facto, de acordo com os estudos económicos mais detalhados, confirmado
por diversos testemunhos de directores de companhias, os investimentos
em patentes têm na realida  de levado a reduzir as despesas e
%(e:desviar o dinheiro) do investimento em I&D deste sector.

#nWr: A carta da Nokia é muito similar em estilo e conteúdo da %(c5:carta de
5 CEOs) de Novembro passado e a %(q:Declaração Conjunta da Indústria)
de Abril de 2003. O chefe de propriedade intelectual da Nokia, Tim
Frain, que identificamos previamente como o autor destas cartas, está
desta vez a solicitar assinaturas de CTOs. Contudo esta carta, tal
como as suas outras, continua a ser escrita da perspectiva de um
advogado de patentes corporativo que teme a erosão da importância do
departamento de patentes dentro da companhia.

#qjd: De acordo com a Nokia, a Presidência Irlandesa deve ser %(q:apreciada)
por %(q:ter apresentado um texto equilibrado que preserva os
incentivos para a inovação europeia em sectores tão diversos como as
telecomunicações, tecnologias de informação, electrónicos de consumo,
produtos de casa, transporte e instrumentos médicos ao mesmo tempo que
responde ao apelo do Parlamento Europeu para limitações que asseguram
não se extendao a áreas não técnicas ou que firam indevidamente a
interoperabilidade na nossa sociedade em rede.)

#oor: Por outro lado o coordenador da FFII - UK, James Heald, diz que o
texto na realidade %(q:é o mais extremista visto até hoje, levado a
cabo apenas com as secções mais pro-patentes dos textos anteriores.
Todas as importantes emendas passadas pelo Parlamento Europeu em
Setembro são completamente ignoradas. O rascunho é deliberadamente
cego a todos os problemas que o Parlamento tentou resolver.)

#jep: Este ponto de vista é partilhado por importantes Eurodeputados.

#jep2: %(PNK), Eurodeputada Finlandesa do Partido Popular Europeu, expressa o
seu estado de choque perante o desprezo do Grupo de Trabalho do
Conselho perante a democracia parlamentar:

#lie: Ao tentar procurar por um compromisso com o Parlamento Europeu sobre a
proposta para patentear inovações implementadas em computador
(patentes de software), o Conselho deveria basear o seu trabalho na
decisão final do Parlamento tomada em plenário, não na da Comissão ou
do Comité dos Assuntos Jurídicos. Avaliando os documentos produzidos
até agora pelo grupo de trabalho do Conselho, parece que o Conselho
não está a tomar em conta a vontade dos legisladores eleitos da
Europa.

#kht: %(DCB), Eurodeputado, líder parlamentar do %(ge:grupo dos Verdes/EFA)
acrescenta:

#toa: O grupo de trabalho do Conselho tem até agora falhado completamente em
lidar com os problemas que os comités da Cultura e da Indústria
tentaram resolver. Estão a actuar da mesma forma que o comité dos
Assuntos Jurídicos actuou no ano passado, e podemos esperar que eles
falhem da mesma forma.

#tec: É evidente que os oficiais de patentes nacionais no Conselho não
querem %(q:harmonização) ou %(q:clarificação). Eles simplesmente
querem assegurar os interesses estabelecidos das patentes. Se não
conseguirem o que querem, eles simplesmente enterram o projecto de
directiva e tentam encontrar outras formas de dar a volta à lei
existente, que claramente lhes é tão dolorosa.

#iWi: %(BHA), Eurodeputado do %(jm:Movimento de Junho) Dinamarquês e membro
do %(ed:Grupo EDD), chama a atenção sobre a falta de democracia na UE
que é exemplificada pelo comportamento da Comissão e do Conselho:

#cne: The approach of the Commission and Council in this directive is
shocking.  They are making full use of all the possibilities of
evading democracy that the current Community Law provides.  First they
ignored 94% of the participants of their own consultation, without
given any justification apart from the claim that the remaining 6%
represented the %(q:economic majority).  Now they are completely
disregarding the vote of the European Parliament, and by the way also
of the Economic and Social Council and of the Council of Regions. 
They are doing this because they are used to succede by doing this. 
The EU is constructed this way.  It makes unaccountable bureaucrats
the masters of legislation.  The problem is compounded by the complete
lack of democratic checks and balances in the European patent system. 
EU and Patents combine into a particularly toxic mixture.  Europe's
citizens urgently need to take up this issue and learn the lessons
before it is too late.  They should in particular not allow this kind
of structure to be perpetuated by a European Constitution this year.

#itu: 15 Eurodeputados subscreveram uma %(ca:Chamada à Acção) (mesmo título
utilizado pela Nokia para a sua carta) que chama a atenção de que
%(q:profissionais de patentes em vários governos e organizações estão
agora a tentar utilizar o Conselho de Ministros da UE para contornar a
democracia parlamentar da União Europeia) e urge o Conselho a
%(q:refrear-se de qualquer contra-proposta à versão do Parlamento
Europeu a menos que tais contra-propostas tenham sido explícitamente
apoiadas por uma decisão maioritária dos respectivos parlamentos
nacionais).

#WeW: O riso mais oco da FFII dirige-se à alegação de que o texto proposto
pelos Irlandeses não %(q:feriria indevidamente a interoperabilidade).

#Msj: Jonas Maebe, representante Belga da FFII, explica:

#eei: Tanto o comité da Indústria como o comité dos Assuntos Jurídicos e a
sessão do Parlamento Europeu, exigiram uma provisão especial para
permitir que dados possam ser inter-convertidos entre pacotes e
plataformas de software diferentes. De outra forma, algumas companhias
podeeriam utilizar patentes de software para bloquear dados dos
utilizadores a um prorama ou sistema operativo em particular, e a
competição seria impossível.

#cwa: É um problema sistemático. Todo e qualquer nicho de mercado está
potencialmente em risco individual. É por isso que, na votação final
em Setembro, o Parlamento Europeu votou a favor dessa provisão com 393
votos contra 35.

#suw: Mas de acordo com a Nokia, o Grupo de Trabalho de Patentes 'respondeu'
ao apelo do Parlamento Europeu, e por isso está tudo bem. E como
(apesar de uma valente %(lx:oposição de última-hora dos
Luxemburgueses)) se propõe responder o Grupo de Trabalho? Apagando
completamente o artigo do Parlamento Europeu, e substituindo-o por um
considerando que diz que quaisquer problemas podem ser relegados à lei
anti abuso de monopólio.

#eaW: Lembrem-se que esta é a lei anti abuso de monopólios que acabou de
demorar quatro anos, com elevados custos, para ir atras de %(q:uma
única) companhia acusada, a Microsoft; que já disse poder atrasar a
decisão com recursos por mais quatro anos; e que no final de contas
parece que o caso vai ser resolvido com um acordo de troca de
licenciamento amigável entre a Microsoft e a Sun, deixando o Samba
(projecto de Software Livre que implementa partilha de ficheiros e
impressoras compatível com Windows, recomendado para
interoperabilidade por companhias como a IBM, HP e Apple)
definitamente como não convidado para a festa.

#dyr: Uma pessoa começa a perguntar-se em que tipo de mundo de sonhos estas
pessoas viverão.

#iWl: A FFII está, entretanto, a mobilizar os seus 50000 apoiantes e 300000
subscritores da petição para se manifestarem tanto na Internet como em
Bruxelas em 14 de Abril. O sítio web demo.ffii.org declara:

#snW: O sítio providencia várias páginas exemplo e banners que os webmasters
podem utilizar para suportar a acção.

#Wri: Os eventos em Bruxelas começam a 14 de Abril às 10:00 com uma
conferência de imprensa no Parlamento Europeu, sala AG2 às 10:00. Os
manifestantes reunirão-se às 11:30 ao lado do Parlamento. Os
participantes vestirão t-shirts com os slogans %(q:Não às Patentes de
Software -- Poder ao Parlamento). Haverá discursos e actuações.

#nri: À manifestação seguir-se-á uma conferência interdisciplinar no
Parlamento Europeu, novamente na sala AG2, às 14:00. Entre os
participantes da intensamente preparada agenda de discussão estão
Eurodeputados, oficiais da Comissão Europeia, o Grupo de Trabalho do
Conselho e o Gabinete Europeu de Patentes, programadores de software,
economistas, advogados de várias escolas.

#gBW: Programa dos Eventos em Bruxelas

#eeI: Para já, mais de 150 participantes se %(ak:registraram) para o evento
na FFII.

#dpr: Mais greves on-line e conferências em várias capitais Europeias terão
lugar durante os seguintes meses até às eleições para o Parlamento
Europeu em 10 a 13 de Junho, em particular durante a semana após o Dia
da Europa de 9 de Maio.

#nu5: Documento 8253/04 do Conselho

#Tit: Título

#Weo: Proposta para uma Directiva do Parlamento Europeu e do Conselho sobre
a patenteabilidade de invenções implementadas em computador -
Preparação da posição comum do Conselho

#RPR: Relatório

#tfl: Data de Criação

#e24: Terça-feira 2004-04-06

#om4: Documento 8253/04 ADD 1 do Conselho da UE

#eoe: Adenda ao Relatório

#Ceo: Mais contactos serão fornecidos sob pedido

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: rmseabra ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040408 ;
# txtlang: pt ;
# multlin: t ;
# End: ;

