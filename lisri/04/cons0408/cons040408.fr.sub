\begin{subdocument}{cons040408}{On remet les gants pour le deuxième round dans le combat européen sur les brevets logiciels}{http://swpat.ffii.org/lisri/04/cons0408/index.fr.html}{Groupe de travail\\\url{swpatag@ffii.org}\\version fran\c{c}aise 2008/04/04 par Gerald SEDRATI-DINET\footnote{\url{http://gibuskro.lautre.net/}}}{Apr\`{e}s des mois de discussion dans le secret des coulisses, la Pr\'{e}sidence irlandaise de l'Union europ\'{e}enne renvoie la proposition de directive de l'UE sur les brevets logiciels \`{a} la case ``politique''. L'Irlande veut que les ministres des \'{E}tats membres se mettent d'accord pour que toute objection soit lev\'{e}e en mai. La proposition de la Pr\'{e}sidence rejette tous les amendements du Parlement europ\'{e}en qui clarifient le texte et pousse au contraire \`{a} une brevetabilit\'{e} directe des programmes d'ordinateur, des structures de donn\'{e}es et des descriptions de proc\'{e}d\'{e}s. La d\'{e}l\'{e}guation luxembourgeoise a fait en vain une derni\`{e}re tentative pour garantir l'interop\'{e}rabilit\'{e} des standards brevet\'{e}s, tentative rejet\'{e}e. En renfort de la position du Conseil, le d\'{e}partement brevets de Nokia r\'{e}colte les signatures de cadres des plus importantes entreprises dans un ``Appel \`{a} l'action'' soutenant le texte de la Pr\'{e}sidence. Dans le camp oppos\'{e}, les d\'{e}fenseurs de la posi  tion du Parlement europ\'{e}en ont organis\'{e} des conf\'{e}rences pour expliquer les dangers des brevets logciciels et se mobilisent pour une ``gr\`{e}ve en ligne'' et un rassemblement \`{a} Bruxelles le 14 avril avec comme slogan : ``Non aux brevets logiciels -- Le pouvoir au Parlement''. Ils esp\`{e}rent r\'{e}cidiver avec le m\^{e}me impact que des actions similaires avaient eu en septembre 2003, contribuant \`{a} convaincre le Parlement europ\'{e}en de voter clairement contre la brevetabilit\'{e} des logiciels.}
\begin{sect}{eumin}{Les brevets logiciels reviennent sur la sc\`{e}ne politique de l'UE}
Apr\`{e}s des mois de planque dans le maquis bruxellois, la discussion \`{a} prpopos de la tr\`{e}s controvers\'{e}e Directive europ\'{e}enne sur les brevets logiciels revient \`{a} nouveau dans les hautes sph\`{e}res politiques. Mardi 6 avril, la Pr\'{e}sidence irlandaise de l'UE a soumis la question au comit\'{e} des repr\'{e}sentants permanents (CoRePer) des \'{E}tats membres, le lieu habituel pour les maquignonnages compliqu\'{e}s.

voir aussi Council Working Party \percent{}(q:compromises) on unlimited patentability and unfettered patent enforcement\footnote{\url{http://localhost/swpat/lisri/04/cons0402/index.fr.html}}

En r\'{e}action, les partisans pro et anti brevets logiciels mobilisent leurs troupes pour soutenir respectivement le Conseil ou le Parlement. Les supporters du Parlement ont annonc\'{e} une journ\'{e}e de grande action \`{a} Bruxelles, mercredi 14 avril, culminant avec une conf\'{e}rence de haut niveau dans les b\^{a}timents m\^{e}mes du Parlement europ\'{e}en. De plus, ils encouragent leurs sympathisants \`{a} ent\^{a}mer ``une gr\`{e}ve en ligne'' la semaine prochaine, en mettant en deuil leur sites web pour ``d\'{e}montrer les cons\'{e}quences des brevets logiciels avant qu'il ne soit trop tard''.

La d\'{e}cision de la Pr\'{e}sidence irlandaise de soumettre le dossier est inattendue mais marque un retour significatif du sujet sur la sc\`{e}ne politique.

D'apr\`{e}s une source proche de la position de la Pr\'{e}sidence, ``de nets progr\`{e}s ont \'{e}t\'{e} accomplis par le Groupe de travail concernant des probl\`{e}mes que les \'{E}tats membres avaient eus sur des questions sp\'{e}cifiques de formulation mais il reste des diff\'{e}rences significatives entre les \'{E}tats membres sur certains points cl\'{e}s. Le sentiment g\'{e}n\'{e}ral est que le travail au niveau etchnique est all\'{e} aussi loin que possible et qu'un apport important au niveau politique est maintenant n\'{e}cessaire si l'on veut arriver \`{a} un accord global en mai''.

Les \'{E}tats membres devraient fixer une position commune lors d'une r\'{e}union du Conseil des ministres \`{a} la concurrence devant avoir lieu \`{a} Bruxelles les 17 ou 18 mai.
\end{sect}

\begin{sect}{nokia}{``Appel \`{a} l'action'' du d\'{e}partement brevets de Nokia}
Les lobbyistes pro-brevets logiciels se pr\'{e}parent au combat. La FFII a obtenu une copie d'une lettre circulaire\footnote{\url{}} diffus\'{e}e par Tim Frain (Nokia/Southwood) et Dany Ducoulombier (Nokia/Bruxelles) (voir Nokia and Software Patents\footnote{\url{http://localhost/swpat/gasnu/nokia/index.en.html}}) pour obtenir des signatures pro-brevets avant le 8 avril. La lettre appelle les ministres \`{a} lever leurs objections et \`{a} supporter un document de travail \'{e}mis par la Pr\'{e}sidence irlandaise le 17 mars.

``Tous les innovateurs europ\'{e}ens, comprenant des inventeurs particuliers, de petites et moyennes enterprises (PME) mais aussi de grosses entreprises multinationales, r\'{e}clament des brevets pour prot\'{e}ger leurs inventions, encourager la recherche et le d\'{e}velopppement en Europe et promouvoir les transferts de licences et de technoloqies'', affirme la lettre.

``Nokia semble ne pas compter Opera dans les innovateurs europ\'{e}ens'', commente H\aa{}kon Wium Lie, PDG d'Opera Software Inc, un leader innovant sur le march\'{e} des navigateurs internet et fournisseur d'une grande partie du logiciel utilis\'{e} dans les t\'{e}l\'{e}phones mobiles Nokia.

Et, comme l'explique Hartmut Pilch, pr\'{e}sident de la FFII et porte-parole de l'Alliance Eurolinux Alliance, Opera n'est qu'une entreprise europ\'{e}enne innovante parmis les milliers qui ont soutenu nos p\'{e}titions contre les brevets logiciels.

Pilch poursuit :

\begin{quote}
{\it L'affirmation du d\'{e}partement des brevets de Nokia comme quoi les brevets sont n\'{e}cessaires pour financer la recherche dans le secteur informatique ressmble \`{a} une tentative d\'{e}sesp\'{e}r\'{e}e pour r\'{e}veiller les fausses id\'{e}es que se font ceux qui connaisent mal le domaine des TIC. Toutes les \'{e}tudes \'{e}conomiques que nous connaissons, y compris celles command\'{e}es par la Commission europ\'{e}enne et les gouvernements des \'{E}tats membres, ont montr\'{e} que les brevets logiciels ne sont qu'un moyen de second ordre pour s\'{e}curiser les investissements dans la recherche et le d\'{e}veloppement. Les principaux facteurs donnant un avantage comp\'{e}tifif sont les droits d'auteurs, les savoirs-faire maison, la complexit\'{e} intrins\`{e}que et la r\'{e}activit\'{e} aux besoins des clients. En fait, selon les \'{e}tudes \'{e}conomiques les plus d\'{e}taill\'{e}es, confirm\'{e}es par nombres de t\'{e}moignages de directeurs d'entreprises, les investissements dans les brevets ont tendance en r\'{e}alit\'{e} \`{a} r\'{e}duire les d\'{e}penses   et \`{a} les \emph{d\'{e}tourner} des investissements de R\&D dans ce secteur.}

{\it La lettre du d\'{e}partement des brevets de Nokia ressemble beaucoup, dans son style et son contenu, \`{a} (c5:la lettre des 5 PDG) de novembre dernier et au ``Rapport commun de l'Industrie'' d'avril 2003. L'avocat en chef des brevets de Nokia, Tim Frain, que nous avons auparavant identifi\'{e} comme \'{e}tant l'auteur de ces lettres, sollicite cette fois les signatures de PDG. N\'{e}anmoins sa lettre est \'{e}crite du point de vue d'un avocat en brevet attach\'{e} \`{a} une firme craignant une \'{e}rosion de l'inportance qu'occupe le d\'{e}partement des brevets dans une entreprise.}
\end{quote}
\end{sect}

\begin{sect}{meps}{R\'{e}actions des parlementaires europ\'{e}ens \`{a} la position du Groupe de travail du Conseil}
Selon Nokia, il faut ``f\'{e}liciter'' la Pr\'{e}sidence irlandaise de ``pr\'{e}senter un texte \'{e}quilibr\'{e} pr\'{e}servant les incitations \`{a} l'innovation en Europe dans des domaines aussi vari\'{e}s que les t\'{e}l\'{e}communications, les technologies de l'information, l'\'{e}lectronique grand public, les appareils \'{e}lectrom\'{e}nagers, les transports et les appareils chirurgicaux tout en r\'{e}pondant \`{a} l'appel du Parlement pour des limitations garantissant que la brevetabilit\'{e} ne s'\'{e}tende pas \`{a} des domaines non techniques ou ne g\^{e}ne pas excessivement l'interop\'{e}rabilit\'{e} dans notre soci\'{e}t\'{e} toujours plus connect\'{e}e''.

De l'autre c\^{o}t\'{e}, James Heald, le coordinateur de la FFII britanique, affirme que le text eest en fait ``le plus extr\^{e}miste jamais vu, reprenant seulement les paragraphes pro-brevets de tous les autres text. Tous les amendements importants vot\'{e}s par le Parlement europ\'{e}ne en septembre sont compl\`{e}tement igonr\'{e}s. Le document de travail est d\'{e}lib\'{e}r\'{e}ment aveugle face aux probl\`{e}mes que le Parlement a tent\'{e} de r\'{e}soudre.''

Ce point de vue est partag\'{e} par des parlementaires europ\'{e}ens influents.

Piia-Noora Kauppi, d\'{e}put\'{e}e europ\'{e}enne finlandaise du Parti populaire, exprime sa consternation face au d\'{e}dain du Groupe de travail du Conseil envers la d\'{e}mocratie parlementaire :

\begin{quote}
{\it Puisque le Conseil essaie de trouver un compromis avec le Parlement europ\'{e}en sur la proposition concernant la brevetabilit\'{e} des inventions mises en oeuvre par ordinateur (brevets logiciels), il devrait baser son travail sur la conclusion prise par le Parlement en session pl\'{e}ni\`{e}re et non sur les d\'{e}cisions de la Commission ou de la commission parlementaire Juridique. \`{A} en juger par les document produits jusqu'ici par le Groupe de travail du Conseil, il semblerait que le Conseil ne veuille pas prendre en compte la volont\'{e} des repr\'{e}sentants d\'{e}mocratiquement \'{e}lus de l'Europe.}
\end{quote}

Daniel Cohn-Bendit\footnote{\url{http://www.cohn-bendit.com/}}, co-pr\'{e}sident du groupe Verts/ALE ajoute :

\begin{quote}
{\it Le Groupe de travail du Conseil a jusqu'ici compl\'{e}tement \'{e}chouer \`{a} aborder les probl\`{e}mes que les commissions parlementaires \`{a} la Culture et \`{a} l'Industrie avait essay\'{e} de r\'{e}soudre. Ils se comportent comme la commission Juridique l'ann\'{e}e derni\`{e}re et on peut s'attendre \`{a} ce qu'ils \'{e}chouent de la m\^{e}me mani\`{e}re.}

{\it Il est clair que les fonctionnaires nationaux des brevets au sein du Conseil ne veulent pas ``d'harmonisation'' ou de ``clarification''. Ils veulent tout simplement garantir les int\'{e}r\^{e}ts de l'establishment des brevets. S'ils n'obtiennent pas ce qu'ils veulent, ils enterreront tout bonnement le projet de directive et essayeront de trouver d'autres moyens de venir \`{a} bout du droit existant, dont la clart\'{e} leur est si douloureuse.}
\end{quote}

Bent Hindrup Andersen\footnote{\url{http://www.hindrup.dk}}, d\'{e}put\'{e} du Mouvement de juin\footnote{\url{http://www.j.dk/}} danois dans le Groupe EDD\footnote{\url{http://www.europarl.eu.int/edd/}} attire l'attention sur le manque de d\'{e}mocratie dans l'UE, qui est flagrant dans le comportement de la Commission et du Conseil :

\begin{quote}
{\it L'approche de la Commission et du Conseil dans cette directive est choquante. Ils essaient par tous les moyens d'\'{e}luder la d\'{e}mocratie qu'apporte le Droit communautaire actuel. Tout d'abord, ils ont ignor\'{e} 94\percent{} des participants \`{a} leur propre consultation, sans jamais se justifier \`{a} part en affirmant que les 6\percent{} restants repr\'{e}sentaient la ``majorit\'{e} \'{e}conomique''. Maintenant, ils n\'{e}gligent compl\`{e}tement le vote du Parlement europ\'{e}en et par la m\^{e}me occasion les avis du Conseil \'{e}conomique et social et du Conseil des r\'{e}gions. Ils agissent ainsi parce qu'ils ont l'habitude que cette tactique soit payante. L'UE s'est construite de cette fa\c{c}on. Des bureaucrates qui n'ont pas \`{a} rendre de compte deviennent ainsi les ma\^{\i}tres de la l\'{e}gislation. Le probl\`{e}me est accentu\'{e} par le manque complet de contr\^{o}le d\'{e}mocratique et d'\'{e}quilibre dans le syst\`{e}me europ\'{e}en de brevets. L'UE et les brevets se m\'{e}langent dans un cocktail particuli\`{e}rement toxique. Les citoyens europ  \'{e}en doivent de toute urgence reprendre \`{a} leur compte ce probl\`{e}me et en tirer les le\c{c}ons avant qu'il soit trop tard. Ils ne devraient pas en particulier permettre \`{a} ce genre de structure de se perp\'{e}tuer cette ann\'{e}e avec la Constitution europ\'{e}enne.}
\end{quote}

15 parlementaires europ\'{e}ens ont sign\'{e} un Appel \`{a} l'action\footnote{\url{}} (m\^{e}me appellation que la circulaire de Nokia) soulignant que ``des professionnels des brevets dans divers gouvernements et organisations tentent maintenant d'utiliser le Conseil des ministres de l'UE afin de se soustraire \`{a} la d\'{e}mocratie parlementaire au sein de l'Union euro\'{e}penne'' et invitent le Conseil ``\`{a} s'abstenir de pr\'{e}senter toute contre-proposition \`{a} la version du projet du Parlement Europ\'{e}en, \`{a} moins que cette contre-proposition n'ait \'{e}t\'{e} soutenue par un vote \`{a} la majorit\'{e} du Parlement de l'\'{E}tat membre concern\'{e}''.
\end{sect}

\begin{sect}{itop}{Le texte de la Pr\'{e}sidence rejette l'interop\'{e}rabilit\'{e}}
La FFII rit jaune devant la d\'{e}claration selon laquelle le texte propos\'{e} par l'Irlande ne ``g\^{e}nerait pas excessivement l'interop\'{e}rabilit\'{e}''.

Jonas Maebe, porte-parole belge de la FFII, explique :

\begin{quote}
{\it La commission \`{a} l'Industrie, la commission Juridique, le Parlement europ\'{e}en en session pl\'{e}ni\`{e}re, ont tous demand\'{e} un disposition particuli\`{e}re pour permettre aux donn\'{e}es d'\^{e}tre converties et \'{e}chang\'{e}es entre diff\'{e}rents syst\`{e}mes et plates-formes logicielles. Sinon, les entreprises pourraient utiliser les brevets logiciels pour bloquer les donn\'{e}es des utilisateurs \`{a} double tour dans un programme ou un syst\`{e}me d'exploitation particulier et la concurrence serait impossible.}

{\it C'est un probl\`{e}me r\'{e}current. Chaque niche de march\'{e} est individuellement menac\'{e}e. C'est pourquoi, dans le vote final en septembre, le Parlement europ\'{e}en a vot\'{e} pour cette disposition par 393 voix contre 35.}

{\it Mais selon Nokia, le Groupe de travail du Conseil a ``r\'{e}pondu'' \`{a} l'appel du Parlement europ\'{e}en, doc tout va bien. Et comment (malgr\'{e} une derni\`{e}re opposition rejet\'{e}e de la d\'{e}l\'{e}gation luxembourgeoise\footnote{\url{\#itop}} le Groupe de travail a-t-il propos\'{e} de r\'{e}pondre ? En supprimant int\'{e}gralement la clause du Parlement europ\'{e}en et en la rempla\c{c}ant par un consid\'{e}rant qui stipule que tout probl\`{e}me doit \^{e}tre confi\'{e} aux lois antit-trust existantes.}

{\it Rappelons que c'est une loi anti-trust qui n'a dur\'{e} ``que'' quatre ans, avec des co\^{u}ts exorbitants, afin de poursuivre une ``seule'' entreprise accus\'{e}e, Microsoft ; lequel Microsoft a d\'{e}clar\'{e} qu'il \'{e}tait pr\^{e}t \`{a} poursuivre en appel pendant encore quatre ans ; et qui en fin de compte a l'air d'aboutir \`{a} ce que l'affaire soit tranquillement arrang\'{e}e par un accord de licences crois\'{e}e entre Microsoft et Sun, sans que Samba (un projet en logiciel libre/open source qui impl\'{e}mente un partage de fichier et d'impression compatible avec Windows, recommand\'{e} et utilis\'{e} \`{a} des fins d'interop\'{e}rabilit\'{e} par des entreprises comme IBM, HP et Apple) n'ait \'{e}t\'{e} invit\'{e} \`{a} la f\^{e}te.}

{\it On commence \`{a} se demander dans quel genre de monde f\'{e}\'{e}rique vivent ces personnes.}
\end{quote}
\end{sect}

\begin{sect}{bxl44}{Gr\`{e}ve en ligne, rassemblements, conf\'{e}rences}
Pendant ce temps, la FFII mobilise ses 50 000 supporters et les 300 000 signataires de la p\'{e}tition pour manifester \`{a} la fois sur Internet et \`{a} Bruxelles le 14 avril. Le site demo.ffii.org d\'{e}clare :

Le site demo.ffii.org fournit de nombreux exemples de pages de gr\`{e}ve et des bandeaux que les webmestres peuvent utiliser pour soutenir cette action.

Les manifestations de Bruxelles s'ouvriront sur une conf\'{e}rence de presse au Parlement europ\'{e}en, salle AG2, le 14 avril \`{a} 10 heures. Les manifestants se rassembleront \`{a} 11:30 pr\`{e}s du Parlement. Ils porteront des tshirts avec les slogans ``No Software Patents -- Power to the Parliament'' (Non aux brevets logiciels - Le pouvoir au Parlement). Des discours et spectacles auront lieu.

La manifestation sera suivie par une conf\'{e}rence interdisciplinaire au Parlement europ\'{e}en, toujours salle AG2, \`{a} 14 heures. Parmis les participants \`{a} ce d\'{e}bat scrupuleusement pr\'{e}par\'{e} se trouvent des memebres du Parlement europ\'{e}en, des fonctionnaires de la Commission europ\'{e}enne, du Groupe de travail du Conseil et de l'Office europ\'{e}en des brevets, des d\'{e}veloppeurs de logiciels, des \'{e}conomistes et des avocats de diverses \'{e}coles de pens\'{e}e.

voir Programme des manifestations de Bruxelles\footnote{\url{http://plone.ffii.org/events/2004/bxl04/}}

Jusqu'\`{a} pr\'{e}sent, plus de 200 participants se sont inscrits\footnote{\url{http://aktiv.ffii.org/}} \`{a} la FFII pour la conf\'{e}rence.

D'autres gr\`{e}ves en ligne et conf\'{e}rences dans diverses capitales europ\'{e}enne auront lien durant les mois pr\'{e}c\'{e}dant les \'{e}lections du Parlement europ\'{e}en qui se tiennent du 10 au 13 juin, en particulier pendant la semaine du Jour de l'europe, le 9 mai.

voir Local Action Days 2004/05/08-12\footnote{\url{http://localhost/swpat/penmi/2004/demo05/index.en.html}}
\end{sect}

\begin{sect}{links}{Liens annot\'{e}s}
\begin{itemize}
\item
{\bf {\bf Document du Conseil de l'UE 8253/04\footnote{\url{http://register.consilium.eu.int/scripts/utfregisterDir/WebDriver.exe?MIlang=EN&key=REGISTER&ssf=DATE_DOCUMENT+DESC&fc=REGAISEN&srm=25&md=400&what=simple&ff_TITRE=patentability&ff_FT_TEXT=&ff_SOUS_COTE_MATIERE=&dd_DATE_REUNION=&rc=1&nr=18&MIval=detail}}}}

\begin{quote}
\begin{description}
\item[Titre:]\ Proposition de Directive du Parlement europ\'{e}en et du Conseil concernant la brevetabilit\'{e} des inventions mises en oeuvre par ordinateur - Pr\'{e}paration de la position commune du Conseil (RAPPORT)
\item[Date de la mise en ligne:]\ mardi 06/04/2004
\end{description}
\end{quote}
\filbreak

\item
{\bf {\bf Document du Conseil de l'UE 8253/04 ADD 1\footnote{\url{http://register.consilium.eu.int/scripts/utfregisterDir/WebDriver.exe?MIlang=EN&key=REGISTER&ssf=DATE_DOCUMENT+DESC&fc=REGAISEN&srm=25&md=400&what=simple&ff_TITRE=patentability&ff_FT_TEXT=&ff_SOUS_COTE_MATIERE=&dd_DATE_REUNION=&rc=2&nr=18&MIval=detail}}}}

\begin{quote}
\begin{description}
\item[Titre:]\ Proposition de Directive du Parlement europ\'{e}en et du Conseil concernant la brevetabilit\'{e} des inventions mises en oeuvre par ordinateur - Pr\'{e}paration de la position commune du Conseil (Addendum au Rapport)
\item[Date de la mise en ligne:]\ mardi 06/04/2004
\end{description}
\end{quote}
\filbreak

\item
{\bf {\bf %(q:Compromis) du Conseil de l'UE pour une brevetabilité illimitée\footnote{\url{http://localhost/swpat/lisri/04/cons0129/index.fr.html}}}}

\begin{quote}
Le Conseil des ministres de l'Union europ\'{e}enne, actuellement pr\'{e}sid\'{e} par l'Irlande, fait circuler un document de travail avec des contre-propositions aux amendements du Parlement europ\'{e}en. Contrastant avec la version du Parlement europ\'{e}en, la version du Conseil autorise une brevetabilit\'{e} illimit\'{e}e et l'applicabilit\'{e} des brevets.  D'apr\`{e}s la version du Conseil, le ``One Click shopping'' d'Amazon est sans l'ombre d'un doute une invention brevetable, la publication de programmes sur un serveur constitue d\'{e}j\`{a} une infraction et l'utilisation de formats de fichier brevet\'{e}s dans un but d'interop\'{e}rabilit\'{e} n'est pas autoris\'{e}e.  Puisque la proc\'{e}dure de d\'{e}cision du Conseil est secr\`{e}te, on ne sait pas qui appuie cette proposition au nom de quel gouvernement mais il est bien connu que le groupe de travail responsable est compos\'{e} de fonctionnaires des bureaux des brevets nationaux et de gens proches de ce groupe qui ont \'{e}galement un si\`{e}ge c\^{o}te \`{a} c\^{o}te au cons  eil administratif de l'Office europ\'{e}en des brevets.
\end{quote}
\filbreak

\item
{\bf {\bf L'UE renforce le monopole de Microsoft\footnote{\url{http://localhost/swpat/lisri/04/cecms0326/index.fr.html}}}}

\begin{quote}
Les proc\'{e}dures anti-concurrentielles de la Commission europ\'{e}enne contre Microsoft ont d\'{e}bouch\'{e} sur un verdict boostant fortement la position de monopole de Microsoft sur le march\'{e} des OS et aident Microsoft \`{a} \'{e}tendre cette position sur d'autres march\'{e}s. Alors que la Commission peut se targuer d'avoir gagn\'{e} une somme substantielle en imposant une amende ponctuelle, \'{e}quivalente \`{a} 1\percent{} des r\'{e}serves de caisse de Microsoft, on peut voir entre les lignes de ce verdict un feu vert donn\'{e} \`{a} Microsoft pour tuer ses principaux concurrents sur le march\'{e} des syst\`{e}mes d'exploitation. Ce sous-entendu a \'{e}t\'{e} en m\^{e}me temps renforc\'{e} par les n\'{e}gociations en coulisses du Groupe sur la politique de brevets du Conseil de l'UE, dont des copies ont \'{e}t\'{e} transmises \`{a} la FFII. Imm\'{e}diatement apr\`{e}s l'annonce, le cours boursier de MSFT s'est \'{e}lev\'{e} de 3\percent{}.
\end{quote}
\filbreak

\item
{\bf {\bf EU Council 2004 Proposal on Software Patents\footnote{\url{}}}}

\begin{quote}
La pr\'{e}sidence irlandaise du Conseil de l'UE a distribu\'{e} aux repr\'{e}sentants des gouvernements un papier contenat des suggestions alternatives aux amendements \`{a} la directive ``sur la brevetabilit\'{e} des inventions mises en oeuvre par ordinateur'' vot\'{e}s par le Parlement europ\'{e}en (PE). Contrastant avec la version du PE, la version du Conseil autorise une brevetabilit\'{e} illimit\'{e}e et le respect l\'{e}gal des brevets. Selon la version actuelle, les algorithmes ``mis en oeuvre par ordinateur'' et les m\'{e}thodes pour l'exercice d'activit\'{e}s \'{e}conomiques seraient des inventions au sens du droit des brevets et la publication d'une description fonctionnelle d'une id\'{e}e brevet\'{e}e constituerait une infraction au brevet. Les protocoles et les formats de donn\'{e}es pourraient \^{e}tre brevet\'{e}s et ne seraient alors plus librement utilisables m\^{e}me dans un objectif d'interop\'{e}rabilit\'{e}. Ces cons\'{e}quences peuvent ne pas sauter aux yeux d'un lecteur non concern\'{e}.  Nous tentons ici de d\'{e}c  hiffrer le language opaque de la proposition et d'expliciter ses cons\'{e}quences.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Directive: Parliament's vs Council's Version\footnote{\url{}}}}

\begin{quote}
In September 2003 the European Parliament amended a proposal from the European Commission for patentability of software in such a way that it became a proposal that clearly disallows software patents.   The EU Council of Ministers and the European Commission subsequently disregarded the European Parliament's Amendments and reached ``political agreement'' in May 2004 in favor of a text that goes even further than the previous text of the Commission and allows US-style unlimited patentability, but shrouds this in a veil of convoluted wording, characterised by multiple negations and other misleading syntactic devices.  Below you can find out yourself by carefully reading the core provisions of the proposed EU directive ``on the patentability of computer-implemented inventions'' which we have collected in a table, together with short explanations of the implications.  At the end we add pointers to the originals from the Parliament and Council websites as well as to related analyses and news reports.
\end{quote}
\filbreak

\item
{\bf {\bf PE 2003-09-24: Directive Brevets Logiciels Amendées\footnote{\url{http://localhost/swpat/papri/europarl0309/index.fr.html}}}}

\begin{quote}
Version consolid\'{e}e des principales provisions (Art 1-6) de la Directive ``sur la brevetabilit\'{e} des inventions mises en oevre par ordinateur'' pour lesquelles le parlament europ\'{e}en a vot\'{e} le 24 septembre de 2003.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Plans Shelved Amid Massive Demonstrations\footnote{\url{http://localhost/swpat/lisri/03/demo0827/index.en.html}}}}

\begin{quote}
On Aug 28th, the European Parliament postponed its vote on the proposed EU Software Patent Directive.  The day before, approximately 500 persons had gathered for a rally beside the Parliament in Brussels, accompanied by an online demonstration involving more than 2000 websites.  The events in and near the Parliament were reported extensively covered in the media, including tv and radio, all over Europe and beyond.  Within a few days, the petition calling the European Parliament to reject software patentability accumulated 50,000 new signatures.
\end{quote}
\filbreak

\item
{\bf {\bf PC Magazine: Patent Riots of 2003\footnote{\url{http://www.pcmag.com/article2/0,4149,1236389,00.asp}}}}

\begin{quote}
John C. Dvorak, famous author in prestigious US magazine, says that the year 2003 marks a new height in the crisis of the patent system, which has gone berserk to the extent that it is almost causing civil unrest.  Dvorak cites some texts from the ffii site and calls on readers to support the FFII.
\end{quote}
\filbreak

\item
{\bf {\bf FFII Demo Site\footnote{\url{http://demo.ffii.org}}}}

\begin{quote}
Introduction to Demos and related FFII Events in Brussels and elsewhere
\end{quote}
\filbreak

\item
{\bf {\bf CEC 2003/11: Secret Nitpicking on European Parliament's Amendments\footnote{\url{}}}}

\begin{quote}
The Industrial Property Unit of the Commission of the European Communities (CEC) had stated in October 2003 that it finds the European Parliament's Amendments to its software patent directive proposal mostly inacceptable.  In a confidential document distributed to EU member state governments in November 2003, the Commission's patent officials added some critical notes about each of the amendments of the European Parliament.  The Commision points out that the text deviates from the practise of the European Patent Office in its use of the terminology and in its reasoning.  This is enough for the Commission to find the Parliament's text inacceptable.  Rather than examine the the merits of the Parliament's versus the EPO's approach, the Commission treats the EPO's approach as the absolute authority that must be followed and tries to find fault in the Parliament's legal logic, mostly by misunderstanding this logic or claiming that it is unclear or that it is at odds with some established practise.  Some of these claims are provably untrue.  The Commission's own proposal has been heavily criticised by prominent patent law experts for its incoherence and lack of clarity.
\end{quote}
\filbreak

\item
{\bf {\bf Influencer le Conséil\footnote{\url{http://localhost/swpat/gasnu/consilium/index.en.html}}}}

\begin{quote}
Collectioner et fournir des Infos sur qui d\'{e}termine la politique du Cons\'{e}il des Ministres de l'UE et comment on peut entrer en dialogue avec ces gents.
\end{quote}
\filbreak

\item
{\bf {\bf CEOs of big telcos sign letter against Europarl Amendments\footnote{\url{http://localhost/swpat/lisri/03/telcos1107/index.en.html}}}}

\begin{quote}
The chief executive officers of Alcatel, Ericsson, Nokia and Siemens have signed a letter to the European Commission and the European Council which complains about the European Parliament's amendments to the proposed software patent directive, saying that these will effectively remove the value of most of the patents of their companies and thereby harm the competitiveness of Europe's industry and violate the TRIPs treaty.  FFII points out that the Directive indeed threatens the interests of the patent departments of such companies, but not of the companies themselves: The letter is characterised by untruthful dogmatic assertions which say much about the thinking of patent departments and little about the interests of their companies, many of whose employees, especially software developers, support the positions of FFII.
\end{quote}
\filbreak

\item
{\bf {\bf Irish EU Presidency to %(q:protect software inventions) in May\footnote{\url{http://localhost/swpat/lisri/04/ieeu0109/index.en.html}}}}

\begin{quote}
The Irish vice prime minister has unveiled a brochure which describes the agenda of the Competitiveness Council of the Irish EU Presidency.  The brochure places high emphasis on the Community Patent and the IP Enforcement Directive and somewhat lower emphasis on the software patent directive, although it asserts that ``effective instruments'' for ``protection'' of ``software inventions'' form an ``important underpinning'' of the ``knowledge based economy''.  The IE Presidency will try to bring about an agreement on the software patent directive at the May meeting of the Competitiveness Council.
\end{quote}
\filbreak

\item
{\bf {\bf Aufrufe zum Handeln\footnote{\url{}}}}

\begin{quote}
Faire en sorte que des appels \`{a} l'action soient soutenus par les  acteurs d\'{e}cisifs.
\end{quote}
\filbreak

\item
{\bf {\bf Nokia and Software Patents\footnote{\url{http://localhost/swpat/gasnu/nokia/index.en.html}}}}

\begin{quote}
== News \& Chronology
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{Contacts media}
\begin{description}
\item[courriel:]\ media at ffii org
\item[t\'{e}l:]\ Hartmut Pilch +49-89-18979927 (Allemand/Anglais/Fran\c{c}ais)
Benjamin Henrion +32-498-292771 (Fran\c{c}ais/Anglais)
Jonas Maebe +32-485-36-96-45 (N\'{e}erlandais/Anglais/Fran\c{c}ais)
Dieter Van Uytvanck +32-499-16-70-10 (N\'{e}erlandais/Anglais/Fran\c{c}ais)
Erik Josefsson +46-707-696567 (Su\'{e}dois/Anglais)
James Heald +44 778910 7539 (Anglais)
Plus de contacts peuvent \^{e}tre fournis sur simple demande
\end{description}
\end{sect}

\begin{sect}{ffii}{\`{A} propos de la FFII -- www.ffii.org}
L'Association pour une infrastructure de l'information libre (FFII) est une association \`{a} but non lucratif enregistr\'{e}e \`{a} Munich, ayant pour objet de promouvoir les savoirs dans le domaine du traitement des donn\'{e}es. La FFII soutient le d\'{e}veloppement de biens informationnels publics fond\'{e}s sur les droits d'auteur, la libre concurrence et les standards ouverts. Plus de 500 membres, 1000 soci\'{e}t\'{e}s et 60000 adh\'{e}rents ont charg\'{e} la FFII de repr\'{e}senter leurs int\'{e}r\^{e}ts dans le domaine de la l\'{e}gislation sur les droits de propri\'{e}t\'{e} attach\'{e}s aux logiciels.
\end{sect}

\begin{sect}{url}{URL permanente de ce communiqu\'{e} de presse}
http://localhost/swpat/lisri/04/cons0408/index.fr.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

