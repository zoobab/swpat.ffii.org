<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Ring frei für die nächste Runde im Tauziehen um EU-Software-Patente

#descr: Nach monatelangen Diskussionen hinter verschlossenen Türen hat die
irische EU-Ratspräsidentschaft den Vorschlag für eine
EU-Softwarepatent-Richtlinie wieder auf die %(q:politische) Ebene
gehoben. Die Iren wollen, dass der Ministerrat der Mitgliedsstaaten
bis Mai alle Einwände fallen lässt. Der Entwurf der
Ratspräsidentschaft streicht dabei sämtliche klarstellenden Änderungen
des EU-Parlaments vom September 2003 und macht sich für direkte
Patente auf Computerprogramme, Datenstrukturen und
Prozessbeschreibungen stark. Ein letzter Versuch der Delegation
Luxemburgs, wenigstens die Umwandlung zwischen Dateiformaten
patentfrei zu halten, wurde zurückgewiesen. Die Patentabteilung von
Nokia ist momentan dabei, Unterschriften von leitenden
Firmenangestellten für einen %(tp|%(q:Aufruf zum Handeln)|%(q:Call for
Action)) zu Gunsten des Präsidentschaftsentwurfes zu sammeln. Auf der
anderen Seite machen die Befürworter des Parlamentsbeschlusses mit
diversen Veranstaltungen auf die Gefahren der Softwarepatentierung
aufmerksam.  Sie rufen zu einem %(q:Netzstreik) und einer
Demonstration am 14. April in Brüssel unter dem Motto %(tp|%(q:No
Software Patents -- Power to the Parliament!)|%(q:Softwarepatente nein
danke! -- Gesetze nur von gewählten Vertretern!)) auf. Sie rechnen
damit, dass so erneut eine Wirkung wie im Vorfeld der
EU-Parlamentsentscheidung im September 2003 erreicht werden kann;
seinerzeit waren die Aktionen wichtiger Faktor für die klare
Entscheidung des Parlaments gegen Softwarepatente.

#ssi: Softwarepatente kehren auf die politische Bühne der EU zurück

#oio: Der %(q:Aufruf zum Handeln) aus der Nokia-Patentabteilung

#nCr: Reaktionen von Abgeordneten auf die Position der Rats-Arbeitsgruppe

#dje: Präsidentschaft lehnt Interoperabilitätsprivileg ab

#Slf: Netzstreik, Demonstrationen, Konferenzen

#dca: Pressekontakte

#otF: Über den FFII

#nWW: Dauerhafte Netzadresse dieser Presseerklärung

#eor: Nachdem sie Monate lang tief im Dickicht der Brüsseler Bürokratie
verschollen war, ist die EU-Softwarepatent-Richtlinie wieder in die
hohe Politik zurückgekehrt - und mit ihr die kontroverse Debatte
darüber. Am 6. April hat die irische EU-Ratspräsidentschaft die
Angelegenheit in das CoRePer-Komitee der ständigen Vertreter der
EU-Mitgliedsstaaten übergeben, dem traditionellen Platz für die
komplizierteren politischen Kuhhandel.

#W1x: Als Antwort darauf haben sich sowohl Befürworter als auch Gegner von
Softwarepatenten erneut gesammelt, um entweder den Rat oder das
Parlament zu unterstützen. Die Anhänger des Parlaments haben für den
14. April einen Aktionstag in Brüssel ausgerufen. Höhepunkt ist eine
hochkarätig besetzte Konferenz direkt im Parlamentsgebäude. Darüber
hinaus sind auch die Anhänger zum %(q:Netzstreik) in der Woche nach
Ostern aufgerufen, indem sie ihre Internet-Auftritte schließen oder
schwärzen um so %(q:die Auswirkungen von Softwarepatenten zu
demonstrieren, bevor es zu spät ist).

#rxr: Der Schachzug der irischen Ratspräsidentschaft kommt nicht unerwartet,
markiert aber einen wichtigen Wendepunkt: Das Papier kehrt in die
zentralen EU-Gremien zurück.

#Wcu: Wie inoffiziell aus Kreisen der Rats-Arbeitsgruppe verlautet, %(q:hat
die Arbeitsgruppe bei den Problemen, die einige Mitgliedsstaaten mit
bestimmten Ausdrucksweisen im Text hatten, einige Fortschritte
gemacht. Es gibt aber immer noch erhebliche Meinungsverschiedenheiten
zwischen den Mitgliedsstaaten über mehrere grundlegende Fragen. Der
allgemeine Eindruck ist, dass auf technischer Ebene nun kein
Weiterkommen mehr ist, solange keine politischen Weichenstellungen
erfolgen. Und diese müssen viele offene Fragen beantworten, wenn wie
geplant bis Mai eine Einigung erzielt werden soll).

#sih: Es wird allgemein erwartet, dass die gemeinsame Position der
Mitgliedsstaaten dann bei einem Treffen der zuständigen Minister in
Brüssel am 17./18. Mai unterzeichnet wird.

#AWW: Die Pro-Patent-Lobbyisten bereiten sich auf den Kampf vor. Dem FFII
liegt eine Kopie eines %(no:Rundschreibens) vor, das von %(i:Nokia)s
Tim Frain %(pe:Nokia/Southwood) und Dany Ducoulombier
%(pe:Nokia/Brüssel) verfasst wurde und für Pro-Patent-Unterschriften
bis zum 8. April wirbt. Der Brief ruft die Minister dazu auf, ihre
Einwände fallen zu lassen und den Vorschlag der Ratspräsidentschaft
vom 17. März zu unterstützen:

#dWi: %(q:Die innovativen Kräfte Europas, einzelne Erfinder genauso wie
%(tp|kleine und mittelständische Unternehmen|KMUs) oder große,
multinationale Konzerne, benötigen Patente um ihre Erfindungen zu
schützen, Forschung und Entwicklung zu finanzieren sowie Lizenzwesen
und Technologietransfer zu fördern), so der Brief.

#ohe: %(q:Nokia scheint Opera nicht den innovativen Kräften Europas
zuzurechnen), kommentiert Håkon Wium Lie, Technischer Leiter von Opera
Software Inc., einem der innovativsten Unternehmen im Webbrowser-Markt
und wichtigem Produzenten von Nokia-Handysoftware.

#onr: Und, wie Hartmut Pilch, Vorstand des FFII und Sprecher der
Eurolinux-Allianz, erläutert: Opera ist nur eine von Tausenden
innovativer europäischer Firmen, die sich öffentlich gegen
Softwarepatente ausgesprochen und die entsprechende Petition
unterzeichnet haben.

#lou: Pilch weiter:

#Wsb: Die Behauptung von Nokias Patentabteilung, Patente seien nötig um die
Softwareentwicklung zu finanzieren, wirkt wie ein verzweifelter
Versuch, Menschen zu beeinflussen, die keine Erfahrungen im IuK-Sektor
haben. Alle uns bekannten %(ss:ökonomischen Studien), inklusiver
derer, die von der Europäischen Kommission oder den Regierungen der
Mitgliedsstaaten beauftragt wurden, haben gezeigt, dass Patente nur
einen sehr nachrangigen Einfluss auf den Erlös aus der
Softwareentwicklung haben. Die Hauptfaktoren für den wirtschaftlichen
Erfolg sind Urheberrecht, Betriebsgeheimnis, Komplexität,
Netzwerkeffekte und Nähe zum Markt. Tatsächlich weisen diese Studien
detailliert nach, dass Investitionen in Patente eher finanzielle
Mittel aus den Abteilungen für Forschung und Entwicklung
%(e:abgezogen) haben, was auch von Geschäftsführern großer Firmen
bezeugt wird.

#nWr: Der Nokia-Brief sieht stilistisch und inhaltlich dem %(c5:Brief der 5
Vorstände) von letztem November und dem %(js:Gemeinsamen
Industrie-Standpunkt) aus dem April 2003 sehr ähnlich.  Nokias
Patentchef Tim Frain, den wir auch schon als Autor dieser
vorangegangenen Schreiben identifiziert hatten, sammelt diesmal
Unterschriften von Entwicklungsabteilungsleitern. Nichtsdestotrotz ist
sein Brief aus der Sicht eines Patentanwaltes geschrieben, der sich um
den Stellenwert seiner Abteilung innerhalb der Firma Sorgenmacht.

#qjd: Nokia zufolge muss die irische Ratspräsidentschaft dafür %(q:gelobt
werden), einen %(q:ausgewogenen Richtlinienvorschlag präsentieren zu
haben. Sie hat die Anreize für europäische Innovationen in so
verschiedenen Bereichen wie Telekommunikation,
Informationstechnologie, Konsum- und Haushaltselektronik,
Transportwesen und bei medizinischen Gerätschaften erhalten und
gleichzeitig, wie vom Europäischen Parlament verlangt, verhindert,
dass Patente auf nicht-technische Bereiche ausgeweitet werden oder den
Informationsaustausch in unserer zunehmend vernetzten Welt
unverhältnismäßig einschränken).

#oor: Der britische FFII-Koordinator James Heald bewertet den Ratsentwurf
etwas anders, nämlich als %(q:die extremste bisher veröffentlichte
Version, zusammengestellt aus den weitestgefassten Bestimmungen aller
vorangegangenen Entwürfe. Alle wichtigen Änderungen des Parlaments vom
September werden ignoriert. Der Ratsentwurf ist vollständig blind
gegenüber allen Problemen, die das Parlament aufgegriffen hat.)

#jep: Diese Ansicht wird von führenden Parlamentsmitgliedern geteilt.

#jep2: Piia-Noora Kauppi, finnische Abgeordnete der Europäischen Volkspartei,
drückt ihre Bestürzung über das Verständnis der Rats-Arbeitsgruppe für
parlamentarische Demokratie aus:

#lie: Wenn der Rat nach einem Kompromiss mit dem Parlament über den
Vorschlag zur %(tp|Patentierbarkeit von computer-implementierten
Innovationen|Softwarepatenten) sucht, sollte er als Basis seiner
Arbeit das Ergebnis der Abstimmung im Parlament nehmen und nicht
Beschlüsse der Kommission oder des Rechtsausschusses. Den Papieren der
Rats-Arbeitsgruppe nach zu urteilen sieht es so aus, als ob der EU-Rat
den Willen der gewählten EU-Gesetzgebung nicht weiter für wichtig
hält.

#kht: Daniel Cohn-Bendit, Vorsitzender der %(ge:Gruppe Grüne/EFA), fügt
hinzu:

#toa: Die Ratsarbeitsgruppe hat sich bis jetzt überhaupt nicht den Problemen
gestellt, die der Kultur- und der Industrie-Ausschuss des Parlaments
zu lösen versucht haben.  Sie verhält sich offenbar genauso wie es der
Rechtsausschuss letztes Jahr getan hat, und wir können erwarten, dass
sie genauso scheitern wird.

#tec: Es wird klar, dass die nationalen Patentbeamten im Rat weder
%(q:Harmonisierung) noch %(q:Klarstellung) wollen. Ihnen geht es
lediglich um Sicherung von Pfründen der Patent-Institutionen. Wenn sie
nicht bekommen, was sie wollen, beerdigen sie einfach die ganze
Richtlinie und versuchen, andere Wege um das geltende Recht herum zu
finden, dessen Klarheit ihnen so viel Schmerzen bereitet.

#iWi: %(BHA) von der Dänischen %(jm:Junibewegung) und der %(ed:EDD-Gruppe)
lenkt die Aufmerskamkeit auf Demokratiedefizite der EU, die sich am
Verhalten der Kommission  und des Rates offenbaren:

#cne: Der Anlauf der Kommision und des Rates bei dieser Richtlinie ist
schockierend. Sie bedienen sich aller gangbaren Wege um die Demokratie
zu umgehen wie sie durch das Gemeinschaftsrecht vorgegeben ist. Zuerst
ignorierten sie 94% der Teilnehmer ihrer eigenen Konsultation ohne
irgendwelche Rechtfertigung, abgesehen von der Behauptung, dass die
verbleibenden 6% eine %(q:wirtschafliche Mehrheit) darstellen würde.
Jetzt ignorieren sie das Abstimmungsergebnis des Europäischen
Parlaments vollständig, und damit auch noch den Wirtschafts und
Sozialsausschuss und den Ausschuss der Regionen. Sie tun dies weil sie
es gewohnt sind damit erfolgreich zu sein. Die EU ist so konstruiert.
Sie macht unverantwortliche Bürokraten zu den Herren der Gesetzgebung.
Das Problem setzt sich zusammen aus einem vollständigen Mangel an
demokratischer Kontrolle und demokratischem Gleichgewicht im
Europäischen Patentsystem. Die EU und Patente fügen sich zusammen zu
einer besonders giftigen Mischung. Die Einwohner Europas müssen sich
dringend um diese Sache bemühen und die Lektion lernen bevor es zu
spät ist. Sie sollen es insbesondere nicht dulden, dass diese
Strukturen dieses Jahr in der Europäischen Verfassung festgeschrieben
werden.

#itu: 15 MdEP haben einen %(ca:Aufruf zum Handeln) (gleicher Titel wie
Nokias Rundbrief) unterzeichnet, der darauf hinweist, dass derzeit
%(q:einflußreiche Patentfachleute in verschiedenen Regierungen und
Organisationen) versuchen, %(q:den EU-Ministerrat zu benutzen, um an
der parlamentarischen Demokratie in der Europäischen Union vorbei zu
kommen), und den Rat aufruft %(q:von jeglichem Gegenvorschlag zu der
Version des Europäischen Parlaments Abstand zu nehmen, solange solch
ein Gegenvorschlag keine ausdrückliche Unterstützung des Parlaments
ihres Mitgliedsstaates gefunden hat).

#WeW: Besonders laut muss der FFII über die Behauptung des Nokia-Briefes
lachen, der irische Vorschlag würde %(q:die Interoperabilität nicht
unangemessen behindern).

#Msj: Jonas Maebe, Belgischer FFII-Sprecher, erläutert:

#eei: Der Industrie-Ausschuss, der Rechtsausschuss und das gesamte Plenum
des Europäischen Parlaments, alle haben sich für spezielle
Vorkehrungen ausgesprochen, die die Konvertierbarkeit von Daten
zwischen verschiedenen Programmsystem und Softwareplattformen
sicherstellen. Andernfalls könnten Softwarepatente dazu genutzt
werden, Daten in einem Programmpaket oder Betriebssystem
%(q:einzuschließen) und so jeden Wettbewerb im Keim zu ersticken.

#cwa: Dies ist ein prinzipielles Problem, das jedes beliebige Marktsegment
bedroht. Deshalb wurde der entsprechende Absatz bei der entscheidenden
Abstimmung im September von Europäischen Parlament mit 393 zu 35
Stimmen angenommen.

#suw: Wie Nokia sagt, hat die Arbeitsgruppe des Rates auf das Europäische
Parlament %(q:geantwortet), sodass ja alles in bester Ordnung ist. Und
wie --- abgesehen von einem tapferen %(lx:Oppositionsversuch aus
Luxemburg) --- sieht diese Antwort der Arbeitsgruppe aus? Sie
streichen die Klausel des Parlaments einfach komplett und ersetzen sie
durch einen Erwägungsgrund, dem zu Folge alle Probleme durch die
existierenden Kartellgesetze gelöst werden.

#eaW: Wir erinnern uns, das sind genau diejenigen Wettbewerbsregeln, mit
denen es vier Jahre gedauert und enormen Aufwand gekostet hat, einer
einzigen beschuldigten Firma beizukommen, nämlich Microsoft. Mit dem
Ergebnis, dass Microsoft %(cm:grünes Licht für die Erhebung von
Zollgebühren für die Kommunikation mit seinen patentierten Standards
erhielt) und nun in die zweite Instanz geht, um das Verfahren weiter
zu seinen Gunsten zu wenden oder wenigstens noch ein paar Jahre
hinauszuzögern.

#dyr: Man ist geneigt, sich zu fragen, in welcher Traumwelt diese Leute
leben.

#iWl: Der FFII hat mittlerweile seine 50.000 Unterstützer gebeten, sowohl im
Internet als auch vor Ort in Brüssel am 14. April zu demonstrieren.
Die Website demo.ffii.org berichtet:

#snW: Die Website demo.ffii.org bietet verschiedene Beispiel-Streikseiten
und -banner an, die Webmaster zur Unterstützung der Aktion verwenden
können.

#Wri: Die Brüsseler Veranstaltung beginnt am 14. April um 10:00 Uhr mit
einer Pressekonferenz im Europäischen Parlament, Raum AG2. Die
Demonstranten versammeln sich um 11:30 Uhr neben dem Parlament. Die
Teilnehmer werden T-Shirts mit Aufschriften wie %(q:No Software
Patents -- Power to the Parliament!) tragen. Es wird Reden und
Darbietungen geben.

#nri: Auf die Demonstration folgt eine interdisziplinäre Konferenz im
Europäischen Parlament, ebenfalls im Raum AG2, um 14:00 Uhr. Unter den
Teilnehmern der gemeinsam mit dem %(tp|Maastrichter Institut für
Forschung in Ökonomie der Information und Technik|MERIT) intensiv
vorbereiteten Konferenz sind Abgeordnete des Europäischen Parlaments,
Vertreter der Europäischen Kommission, der Rats-Arbeitsgruppe und des
Europäischen Patentamtes, Softwareentwickler, Ökonomen und Juristen
verschiedener Denkrichtungen.

#gBW: Programm der Brüsseler Aktionen

#eeI: Bis jetzt haben sich bereits mehr als %(N) Teilnehmer beim FFII für
die Konferenz %(ak:registriert).

#dpr: Weitere Netzstreiks und Konferenzen in verschiedenen europäischen
Hauptstädten finden in der Zeit bis zu den Wahlen zum Europaparlament
am 10.-13. Juni statt, insbesondere in der Woche, die auf den
Europatag am 9. Mai folgt.

#nu5: Ratsdokument 8253/04

#Tit: Titel

#Weo: Vorschlag für eine Richtlinie des Europäischen Parlaments und des Rats
über die Patentierbarkeit computer-implementierter Erfindungen -
Vorbereitung der gemeinsamen Ratsposition

#RPR: REPORT

#tfl: Veröffentlicht

#e24: Dienstag, 2004-04-06

#om4: EU Ratsdokument 8253/04 ADD 1

#eoe: Anhang zum Report

#Ceo: Weitere Kontakte auf Nachfrage

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: dhillbre ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040408 ;
# txtlang: de ;
# multlin: t ;
# End: ;

