<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: On remet les gants pour le deuxième round dans le combat européen sur
les brevets logiciels

#descr: Après des mois de discussion dans le secret des coulisses, la
Présidence irlandaise de l'Union européenne renvoie la proposition de
directive de l'UE sur les brevets logiciels à la case %(q:politique).
L'Irlande veut que les ministres des États membres se mettent d'accord
pour que toute objection soit levée en mai. La proposition de la
Présidence rejette tous les amendements du Parlement européen qui
clarifient le texte et pousse au contraire à une brevetabilité directe
des programmes d'ordinateur, des structures de données et des
descriptions de procédés. La déléguation luxembourgeoise a fait en
vain une dernière tentative pour garantir l'interopérabilité des
standards brevetés, tentative rejetée. En renfort de la position du
Conseil, le département brevets de Nokia récolte les signatures de
cadres des plus importantes entreprises dans un %(q:Appel à l'action)
soutenant le texte de la Présidence. Dans le camp opposé, les
défenseurs de la posi  tion du Parlement européen ont organisé des
conférences pour expliquer les dangers des brevets logciciels et se
mobilisent pour une %(q:grève en ligne) et un rassemblement à
Bruxelles le 14 avril avec comme slogan : %(q:Non aux brevets
logiciels -- Le pouvoir au Parlement). Ils espèrent récidiver avec le
même impact que des actions similaires avaient eu en septembre 2003,
contribuant à convaincre le Parlement européen de voter clairement
contre la brevetabilité des logiciels.

#ssi: Les brevets logiciels reviennent sur la scène politique de l'UE

#oio: %(q:Appel à l'action) du département brevets de Nokia

#nCr: Réactions des parlementaires européens à la position du Groupe de
travail du Conseil

#dje: Le texte de la Présidence rejette l'interopérabilité

#Slf: Grève en ligne, rassemblements, conférences

#dca: Contacts media

#otF: À propos de la FFII

#nWW: URL permanente de ce communiqué de presse

#eor: Après des mois de planque dans le maquis bruxellois, la discussion à
prpopos de la très controversée Directive européenne sur les brevets
logiciels revient à nouveau dans les hautes sphères politiques. Mardi
6 avril, la Présidence irlandaise de l'UE a soumis la question au
comité des représentants permanents (CoRePer) des États membres, le
lieu habituel pour les maquignonnages compliqués.

#W1x: En réaction, les partisans pro et anti brevets logiciels mobilisent
leurs troupes pour soutenir respectivement le Conseil ou le Parlement.
Les supporters du Parlement ont annoncé une journée de grande action à
Bruxelles, mercredi 14 avril, culminant avec une conférence de haut
niveau dans les bâtiments mêmes du Parlement européen. De plus, ils
encouragent leurs sympathisants à entâmer %(q:une grève en ligne) la
semaine prochaine, en mettant en deuil leur sites web pour
%(q:démontrer les conséquences des brevets logiciels avant qu'il ne
soit trop tard).

#rxr: La décision de la Présidence irlandaise de soumettre le dossier est
inattendue mais marque un retour significatif du sujet sur la scène
politique.

#Wcu: D'après une source proche de la position de la Présidence, %(q:de nets
progrès ont été accomplis par le Groupe de travail concernant des
problèmes que les États membres avaient eus sur des questions
spécifiques de formulation mais il reste des différences
significatives entre les États membres sur certains points clés. Le
sentiment général est que le travail au niveau etchnique est allé
aussi loin que possible et qu'un apport important au niveau politique
est maintenant nécessaire si l'on veut arriver à un accord global en
mai).

#sih: Les États membres devraient fixer une position commune lors d'une
réunion du Conseil des ministres à la concurrence devant avoir lieu à
Bruxelles les 17 ou 18 mai.

#AWW: Les lobbyistes pro-brevets logiciels se préparent au combat. La FFII a
obtenu une copie d'une %(no:lettre circulaire) diffusée par Tim Frain
(Nokia/Southwood) et Dany Ducoulombier (Nokia/Bruxelles) (voir
%(Nokia)) pour obtenir des signatures pro-brevets avant le 8 avril. La
lettre appelle les ministres à lever leurs objections et à supporter
un document de travail émis par la Présidence irlandaise le 17 mars.

#dWi: %(q:Tous les innovateurs européens, comprenant des inventeurs
particuliers, de %(tp|petites et moyennes enterprises|PME) mais aussi
de grosses entreprises multinationales, réclament des brevets pour
protéger leurs inventions, encourager la recherche et le
développpement en Europe et promouvoir les transferts de licences et
de technoloqies), affirme la lettre.

#ohe: %(q:Nokia semble ne pas compter Opera dans les innovateurs européens),
commente Håkon Wium Lie, PDG d'Opera Software Inc, un leader innovant
sur le marché des navigateurs internet et fournisseur d'une grande
partie du logiciel utilisé dans les téléphones mobiles Nokia.

#onr: Et, comme l'explique Hartmut Pilch, président de la FFII et
porte-parole de l'Alliance Eurolinux Alliance, Opera n'est qu'une
entreprise européenne innovante parmis les milliers qui ont soutenu
nos pétitions contre les brevets logiciels.

#lou: Pilch poursuit :

#Wsb: L'affirmation du département des brevets de Nokia comme quoi les
brevets sont nécessaires pour financer la recherche dans le secteur
informatique ressmble à une tentative désespérée pour réveiller les
fausses idées que se font ceux qui connaisent mal le domaine des TIC.
Toutes les études économiques que nous connaissons, y compris celles
commandées par la Commission européenne et les gouvernements des États
membres, ont montré que les brevets logiciels ne sont qu'un moyen de
second ordre pour sécuriser les investissements dans la recherche et
le développement. Les principaux facteurs donnant un avantage
compétifif sont les droits d'auteurs, les savoirs-faire maison, la
complexité intrinsèque et la réactivité aux besoins des clients. En
fait, selon les études économiques les plus détaillées, confirmées par
nombres de témoignages de directeurs d'entreprises, les
investissements dans les brevets ont tendance en réalité à réduire les
dépenses   et à les %(e:détourner) des investissements de R&D dans ce
secteur.

#nWr: La lettre du département des brevets de Nokia ressemble beaucoup, dans
son style et son contenu, à (c5:la lettre des 5 PDG) de novembre
dernier et au %(q:Rapport commun de l'Industrie) d'avril 2003.
L'avocat en chef des brevets de Nokia, Tim Frain, que nous avons
auparavant identifié comme étant l'auteur de ces lettres, sollicite
cette fois les signatures de PDG. Néanmoins sa lettre est écrite du
point de vue d'un avocat en brevet attaché à une firme craignant une
érosion de l'inportance qu'occupe le département des brevets dans une
entreprise.

#qjd: Selon Nokia, il faut %(q:féliciter) la Présidence irlandaise de
%(q:présenter un texte équilibré préservant les incitations à
l'innovation en Europe dans des domaines aussi variés que les
télécommunications, les technologies de l'information, l'électronique
grand public, les appareils électroménagers, les transports et les
appareils chirurgicaux tout en répondant à l'appel du Parlement pour
des limitations garantissant que la brevetabilité ne s'étende pas à
des domaines non techniques ou ne gêne pas excessivement
l'interopérabilité dans notre société toujours plus connectée).

#oor: De l'autre côté, James Heald, le coordinateur de la FFII britanique,
affirme que le text eest en fait %(q:le plus extrêmiste jamais vu,
reprenant seulement les paragraphes pro-brevets de tous les autres
text. Tous les amendements importants votés par le Parlement européne
en septembre sont complètement igonrés. Le document de travail est
délibérément aveugle face aux problèmes que le Parlement a tenté de
résoudre.)

#jep: Ce point de vue est partagé par des parlementaires européens
influents.

#jep2: Piia-Noora Kauppi, députée européenne finlandaise du Parti populaire,
exprime sa consternation face au dédain du Groupe de travail du
Conseil envers la démocratie parlementaire :

#lie: Puisque le Conseil essaie de trouver un compromis avec le Parlement
européen sur la proposition concernant la brevetabilité des inventions
mises en oeuvre par ordinateur (brevets logiciels), il devrait baser
son travail sur la conclusion prise par le Parlement en session
plénière et non sur les décisions de la Commission ou de la commission
parlementaire Juridique. À en juger par les document produits
jusqu'ici par le Groupe de travail du Conseil, il semblerait que le
Conseil ne veuille pas prendre en compte la volonté des représentants
démocratiquement élus de l'Europe.

#kht: %(DCB), co-président du groupe Verts/ALE ajoute :

#toa: Le Groupe de travail du Conseil a jusqu'ici complétement échouer à
aborder les problèmes que les commissions parlementaires à la Culture
et à l'Industrie avait essayé de résoudre. Ils se comportent comme la
commission Juridique l'année dernière et on peut s'attendre à ce
qu'ils échouent de la même manière.

#tec: Il est clair que les fonctionnaires nationaux des brevets au sein du
Conseil ne veulent pas %(q:d'harmonisation) ou de %(q:clarification).
Ils veulent tout simplement garantir les intérêts de l'establishment
des brevets. S'ils n'obtiennent pas ce qu'ils veulent, ils enterreront
tout bonnement le projet de directive et essayeront de trouver
d'autres moyens de venir à bout du droit existant, dont la clarté leur
est si douloureuse.

#iWi: %(BHA), député du %(jm:Mouvement de juin) danois dans le %(ed:Groupe
EDD) attire l'attention sur le manque de démocratie dans l'UE, qui est
flagrant dans le comportement de la Commission et du Conseil :

#cne: L'approche de la Commission et du Conseil dans cette directive est
choquante. Ils essaient par tous les moyens d'éluder la démocratie
qu'apporte le Droit communautaire actuel. Tout d'abord, ils ont ignoré
94% des participants à leur propre consultation, sans jamais se
justifier à part en affirmant que les 6% restants représentaient la
%(q:majorité économique). Maintenant, ils négligent complètement le
vote du Parlement européen et par la même occasion les avis du Conseil
économique et social et du Conseil des régions. Ils agissent ainsi
parce qu'ils ont l'habitude que cette tactique soit payante. L'UE
s'est construite de cette façon. Des bureaucrates qui n'ont pas à
rendre de compte deviennent ainsi les maîtres de la législation. Le
problème est accentué par le manque complet de contrôle démocratique
et d'équilibre dans le système européen de brevets. L'UE et les
brevets se mélangent dans un cocktail particulièrement toxique. Les
citoyens europ  éen doivent de toute urgence reprendre à leur compte
ce problème et en tirer les leçons avant qu'il soit trop tard. Ils ne
devraient pas en particulier permettre à ce genre de structure de se
perpétuer cette année avec la Constitution européenne.

#itu: 15 parlementaires européens ont signé un %(ca:Appel à l'action) (même
appellation que la circulaire de Nokia) soulignant que %(q:des
professionnels des brevets dans divers gouvernements et organisations
tentent maintenant d'utiliser le Conseil des ministres de l'UE afin de
se soustraire à la démocratie parlementaire au sein de l'Union
euroépenne) et invitent le Conseil %(q:à s'abstenir de présenter toute
contre-proposition à la version du projet du Parlement Européen, à
moins que cette contre-proposition n'ait été soutenue par un vote à la
majorité du Parlement de l'État membre concerné).

#WeW: La FFII rit jaune devant la déclaration selon laquelle le texte
proposé par l'Irlande ne %(q:gênerait pas excessivement
l'interopérabilité).

#Msj: Jonas Maebe, porte-parole belge de la FFII, explique :

#eei: La commission à l'Industrie, la commission Juridique, le Parlement
européen en session plénière, ont tous demandé un disposition
particulière pour permettre aux données d'être converties et échangées
entre différents systèmes et plates-formes logicielles. Sinon, les
entreprises pourraient utiliser les brevets logiciels pour bloquer les
données des utilisateurs à double tour dans un programme ou un système
d'exploitation particulier et la concurrence serait impossible.

#cwa: C'est un problème récurrent. Chaque niche de marché est
individuellement menacée. C'est pourquoi, dans le vote final en
septembre, le Parlement européen a voté pour cette disposition par 393
voix contre 35.

#suw: Mais selon Nokia, le Groupe de travail du Conseil a %(q:répondu) à
l'appel du Parlement européen, doc tout va bien. Et comment (malgré
une %(lx:dernière opposition rejetée de la délégation luxembourgeoise)
le Groupe de travail a-t-il proposé de répondre ? En supprimant
intégralement la clause du Parlement européen et en la remplaçant par
un considérant qui stipule que tout problème doit être confié aux lois
antit-trust existantes.

#eaW: Rappelons que c'est une loi anti-trust qui n'a duré %(q:que) quatre
ans, avec des coûts exorbitants, afin de poursuivre une %(q:seule)
entreprise accusée, Microsoft ; lequel Microsoft a déclaré qu'il était
prêt à poursuivre en appel pendant encore quatre ans ; et qui en fin
de compte a l'air d'aboutir à ce que l'affaire soit tranquillement
arrangée par un accord de licences croisée entre Microsoft et Sun,
sans que Samba (un projet en logiciel libre/open source qui implémente
un partage de fichier et d'impression compatible avec Windows,
recommandé et utilisé à des fins d'interopérabilité par des
entreprises comme IBM, HP et Apple) n'ait été invité à la fête.

#dyr: On commence à se demander dans quel genre de monde féérique vivent ces
personnes.

#iWl: Pendant ce temps, la FFII mobilise ses 50 000 supporters et les 300
000 signataires de la pétition pour manifester à la fois sur Internet
et à Bruxelles le 14 avril. Le site demo.ffii.org déclare :

#snW: Le site demo.ffii.org fournit de nombreux exemples de pages de grève
et des bandeaux que les webmestres peuvent utiliser pour soutenir
cette action.

#Wri: Les manifestations de Bruxelles s'ouvriront sur une conférence de
presse au Parlement européen, salle AG2, le 14 avril à 10 heures. Les
manifestants se rassembleront à 11:30 près du Parlement. Ils porteront
des tshirts avec les slogans %(q:No Software Patents -- Power to the
Parliament) (Non aux brevets logiciels - Le pouvoir au Parlement). Des
discours et spectacles auront lieu.

#nri: La manifestation sera suivie par une conférence interdisciplinaire au
Parlement européen, toujours salle AG2, à 14 heures. Parmis les
participants à ce débat scrupuleusement préparé se trouvent des
memebres du Parlement européen, des fonctionnaires de la Commission
européenne, du Groupe de travail du Conseil et de l'Office européen
des brevets, des développeurs de logiciels, des économistes et des
avocats de diverses écoles de pensée.

#gBW: Programme des manifestations de Bruxelles

#eeI: Jusqu'à présent, plus de %(N) participants se sont %(ak:inscrits) à la
FFII pour la conférence.

#dpr: D'autres grèves en ligne et conférences dans diverses capitales
européenne auront lien durant les mois précédant les élections du
Parlement européen qui se tiennent du 10 au 13 juin, en particulier
pendant la semaine du Jour de l'europe, le 9 mai.

#nu5: Document du Conseil de l'UE 8253/04

#Tit: Titre

#Weo: Proposition de Directive du Parlement européen et du Conseil
concernant la brevetabilité des inventions mises en oeuvre par
ordinateur - Préparation de la position commune du Conseil

#RPR: RAPPORT

#tfl: Date de la mise en ligne

#e24: mardi 06/04/2004

#om4: Document du Conseil de l'UE 8253/04 ADD 1

#eoe: Addendum au Rapport

#Ceo: Plus de contacts peuvent être fournis sur simple demande

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040408 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

