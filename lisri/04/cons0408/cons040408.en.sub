\begin{subdocument}{cons040408}{The gloves come off for Round Two in the EU fight over Software Patents}{http://swpat.ffii.org/lisri/04/cons0408/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2008/04/04 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{After months of closed back room discussions, the Irish Presidency of the European Union has referred the proposed EU Directive on software patents back up to ``political'' level.  The Irish want members of the Council of Ministers of the member states to agree to drop all objections by May.  The Presidency proposed draft text rejects all clarifying amendments made by the European Parliament in September 2003 and instead pushes for direct patentability of computer programs, data structures and process descriptions.  A last ditch attempt by the Luxembourg delegation to ensure interoperability with patented standards was rejected.  The Patent Department at Nokia is collecting signatures from top company executives for a ``Call for Action'' in favour of the Presidency text.  In the other corner, supporters of the European Parliament's position have arranged conferences to explain the dangers of software patents, and are mobilising for a ``net strike'' and a rally in Brussels on April 14th under the slogan ``No Software Patents -- Power to the Parliament''.  They are hoping for a repeat of the impact of similar actions in the run-up to September 2003, which helped convince the European Parliament to vote clearly against software patents.}
\begin{sect}{eumin}{Software Patents return to the EU Political Centre Stage}
Following months hidden away deep in the Brussels undergrowth, debate on the controversial European Directive on software patents is returning back to the political high level.  On Tuesday 6 April, the Irish Presidency of the EU referred the issue to the CoRePer committee of Member States' Permanent Representatives, the traditional venue for difficult political horse trading.

see also Council Working Party \percent{}(q:compromises) on unlimited patentability and unfettered patent enforcement\footnote{\url{http://localhost/swpat/lisri/04/cons0402/index.en.html}}

In response, campaigners for and against software patents are mobilising their constituents in support of either the Council or the Parliament.  The supporters of the Parliamment have announced a day of mass action in Brussels on Wednesday 14 April, culminating in a high-level conference in the European Parliament building itself.  Additionally, they are urging their supporters to go on ``net strike'' next week, blacking out their websites to ``demonstrate the effects of software patents before it is too late''.

The move by the Irish Presidency on Tuesday to refer the dossier was not unexpected, but marks a significant return of the dossier to the political centre-stage.

According to one source familiar with the Presidency position, ``there has been some clear progress in the Working Group with problems that some of the Member States had had on some particular issues of wording; but there are still significant differences between Member States on some of the most key fundamental issues.  The general feeling is that work at the technical level has now gone about as far as it can, and strong input from the political level is now going to be needed, if general agreement is to be achieved by May''.

The member states are supposed to sign off their common position at a meeting of the Competiveness Council of Ministers to be held in Brussels on May 17-18.
\end{sect}

\begin{sect}{nokia}{``Call for Action'' from Nokia's Patent Department}
Lobbyists in favour of software patents are gearing up for the fight.  FFII has obtained a copy of a round-robin letter\footnote{\url{}} circulated by Nokia and Software Patents\footnote{\url{http://localhost/swpat/gasnu/nokia/index.en.html}}'s Tim Frain (Nokia/Southwood) and Dany Ducoulombier (Nokia/Brussels) for pro-patent signatures before April 8th.  The letter calls on ministers to drop their objections, and to support a draft text issued by the Irish Presidency on March 17th:

``All of Europe's innovators, including individual inventors, small and medium size enterprises (SMEs), as well as large multinational companies, require patents to protect their inventions, provide incentives to undertake research and development in Europe, and to promote licensing and technology transfer'', claims the letter.

``Nokia doesn't seem to be counting Opera among the European innovators'', comments H\aa{}kon Wium Lie, CTO of Opera Software Inc, an innovation leader in the web browser market and producer of much of the software used in Nokia's mobile phones.

And, as Hartmut Pilch president of FFII and speaker of the Eurolinux Alliance explains, Opera is just one in thousands of innovative European companies who have publicly endorsed our petitions against software patents.

Pilch continues:

\begin{quote}
{\it The Nokia patent department's claim that patents are needed to fund research in the ICT sector looks like a desperate attempt to mobilise the misconceptions of people who are not familiar with this sector.  All the economic studies\footnote{\url{http://localhost/swpat/vreji/minra/sisku.de.html}} we know of, including those ordered by the European Commission and by member state governments, have shown that software patents are only of very secondary importance as a means of capturing returns from R\&D investments.  The main drivers of competitive advantage are copyright, trade secret, complexity, network effects and the ability to react quickly to customer needs.  In fact, according to the most detailed economic studies, confirmed by numerous testimonies from company directors, patent investments have actually tended to reduce spending and divert it \emph{away} from R\&D investment in this sector.}

{\it The Nokia letter looks very similar in style and content to the ``letter of the 5 CEOs\footnote{\url{http://localhost/swpat/lisri/03/telcos1107/index.en.html}}'' of last november and the ``Joint Industry Statement\footnote{\url{}}'' of April 2003.  Nokia's head of intellectual property, Tim Frain, whom we have also previously identified as the author of these letters, is solliciting signatures from CTOs this time.  Yet this letter, like his others, is still written from a perspective of a corporate patent lawyer who fears an erosion of the patent department's importance within the company.}
\end{quote}
\end{sect}

\begin{sect}{meps}{Reactions by MEPs to the Council Working Party position}
According to Nokia, the Irish Presidency is to be ``commended'' for ``presenting a balanced text which preserves the incentives for European innovation in sectors as diverse as telecommunications, information technology, consumer electronics, household appliances, transportation and medical instruments while responding to the European Parliament's call for limitations to ensure that patentability does not extend into non-technical areas or unduly hinder interoperability in our increasingly networked society.''

On the other hand, FFII's UK co-ordinator James Heald, says the text is actually ``the most extreme yet seen, put together only from the most pro-patent sections of all the previous texts.  All of the important amendments passed by the European Parliament in September are completely ignored. The draft text is deliberately blind to all of the problems which the Parliament tried to address.''

This view is shared by leading MEPs.

Piia-Noora Kauppi\footnote{\url{http://www.pkauppi.net}}, Finnish MEP of the European People's Party, expresses dismay at the Council Working Party's contempt for parliamentary democracy:

\begin{quote}
{\it As the Council is trying to look for a compromise with the European Parliament on the proposal for patenting of computer-implemented innovations (software patents), it should base its work on the final decision taken by the plenary session of the Parliament, not on that of the Commission or of the Legal Affairs Committee.  Judging from the papers produced so far by the Council's working party, it seems that the Council is not taking the will of Europe's elected legislators into account.}
\end{quote}

Daniel Cohn-Bendit\footnote{\url{http://www.cohn-bendit.com/}}, chairman of the Greens/EFA Group adds:

\begin{quote}
{\it The Council working party has so far completely failed to address the problems which the European Parliament's Cultural and Industrial Affairs committees tried to solve.  They behave in the same way as the Legal Affairs Committee behaved last year, and we can expect that they will fail in the same way.}

{\it It is clear that the national patent officials in the Council do not want ``harmonisation'' or ``clarification''. They merely want to secure the interests of the patent establishment. If they don't get what they want, they simply bury the directive project and try to find other ways to get around the existing law, whose clarity is so painful to them.}
\end{quote}

Bent Hindrup Andersen\footnote{\url{http://www.hindrup.dk}} MEP of the Danish June Movement\footnote{\url{http://www.j.dk/}} and the EDD Group\footnote{\url{http://www.europarl.eu.int/edd/}} draws attention to the lack of democracy in the EU which is exemplified by the Commission's and Council's behaviour:

\begin{quote}
{\it The approach of the Commission and Council in this directive is shocking.  They are making full use of all the possibilities of evading democracy that the current Community Law provides.  First they ignored 94\percent{} of the participants of their own consultation, without given any justification apart from the claim that the remaining 6\percent{} represented the ``economic majority''.  Now they are completely disregarding the vote of the European Parliament, and by the way also of the Economic and Social Council and of the Council of Regions.  They are doing this because they are used to succede by doing this.  The EU is constructed this way.  It makes unaccountable bureaucrats the masters of legislation.  The problem is compounded by the complete lack of democratic checks and balances in the European patent system.  EU and Patents combine into a particularly toxic mixture.  Europe's citizens urgently need to take up this issue and learn the lessons before it is too late.  They should in particular not allow this kind of structure to be perpetuated by a European Constitution this year.}
\end{quote}

15 MEPs have signed a Call for Action\footnote{\url{}} (same title as used by Nokia for its round-robin letter) which points out that ``patent professionals in various governments and organisations are now trying to use the EU Council of Ministers in order to sidestep parliamentary democracy in the European Union'' and urges the Council to ``refrain from any counter-proposals to the European Parliament's version of the draft, unless such counter-proposals have been explicitely endorsed by a majority decision of the member's national parliament''.
\end{sect}

\begin{sect}{itop}{Presidency Text Rejects Interoperability}
FFII's most hollow laughter is directed at the claim that the Irish proposed text would not ``unduly hinder interoperability''.

Jonas Maebe, Belgian speaker of FFII, explains:

\begin{quote}
{\it The Industry committee, and the Legal Affairs committee, and the full session of the European Parliament, all demanded a special provision to allow data to be inter-converted between different packages and software platforms.  Otherwise companies could use software patents to lock in users' data to a particular program or operating system, and competition would be impossible.}

{\it It's a systematic problem.  Each and every market niche is individually potentially at risk.  That's why, in the final vote in September, the European Parliament voted in favour of the provision by 393 votes to 35.}

{\it But according to Nokia, the Council Working Party has ``responded'' to the European Parliament's call, so everything's all right. And how (despite a valiant last-ditch opposition by the Luxemburgers\footnote{\url{\#itop}}) does the Working Party propose to respond ?  By deleting the European Parliament's clause entirely, and instead inserting a recital clause that says any problems can be left to existing antitrust law.}

{\it Remember, this is the antitrust law which has just taken four years, at vast expense, to go after a single accused company, Microsoft.  With the result that Microsoft received green light for collecting toll fees from anyone wishing to communicate with its patented standards\footnote{\url{http://localhost/swpat/lisri/04/cecms0326/index.en.html}}, and that Microsoft will appeal to further improve the conditions and tie up the procedings for another four years.}

{\it One might start to wonder what kind of dream world these people live in.}
\end{quote}
\end{sect}

\begin{sect}{bxl44}{Net Strike, Rallies, Conferences}
The FFII is meanwhile asking its 50,000 supporters and 300,000 petition signatories to demonstrate both on the Internet and in Brussels on April 14th.

The website demo.ffii.org provides numerous sample strike pages and banners which can be used by webmasters to support the action.

The Brussels events begin on April 14 at 10.00 with a press conference in the European Parliament, room AG2, at 10.00.  The demonstrators will assemble at 11.30 beside the Parliament.  Participants will wear t-shirts with the slogans ``No Software Patents -- Power to the Parliament''.  There will be speeches and performances.

The demonstration is followed by an interdisciplinary conference in the European Parliament, again room AG2, at 14.00.  Among the participants of the intensely prepared discussion agenda are members of the European Parliament, officials from the European Commission, the Council Working Party and the European Patent Office, software developpers, economists, lawyers of various schools of thought.

see Program of Brussels Events\footnote{\url{http://plone.ffii.org/events/2004/bxl04/}}

So far, more than 200 participants have registered\footnote{\url{http://aktiv.ffii.org/}} with FFII for the conference.

Further net strikes and conferences in various European Capitals will take place during the months up to the elections of the European Parliament on June 10-13, in particular during the week after the Day of Europe of May 9th.

see Local Action Days 2004/05/08-12\footnote{\url{http://localhost/swpat/penmi/2004/demo05/index.en.html}}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Council Document 8253/04\footnote{\url{http://register.consilium.eu.int/scripts/utfregisterDir/WebDriver.exe?MIlang=EN&key=REGISTER&ssf=DATE_DOCUMENT+DESC&fc=REGAISEN&srm=25&md=400&what=simple&ff_TITRE=patentability&ff_FT_TEXT=&ff_SOUS_COTE_MATIERE=&dd_DATE_REUNION=&rc=1&nr=18&MIval=detail}}}}

\begin{quote}
\begin{description}
\item[Title:]\ Proposal for a Directive of the European Parliament and of the Council on the patentability of computer-implemented inventions - Preparation of the Council's common position (REPORT)
\item[Date of Upload:]\ tuesday 2004-04-06
\end{description}
\end{quote}
\filbreak

\item
{\bf {\bf EU Council document  8253/04 ADD 1\footnote{\url{http://register.consilium.eu.int/scripts/utfregisterDir/WebDriver.exe?MIlang=EN&key=REGISTER&ssf=DATE_DOCUMENT+DESC&fc=REGAISEN&srm=25&md=400&what=simple&ff_TITRE=patentability&ff_FT_TEXT=&ff_SOUS_COTE_MATIERE=&dd_DATE_REUNION=&rc=2&nr=18&MIval=detail}}}}

\begin{quote}
\begin{description}
\item[Title:]\ Proposal for a Directive of the European Parliament and of the Council on the patentability of computer-implemented inventions - Preparation of the Council's common position (Addendum to the Report)
\item[Date of Upload:]\ tuesday 2004-04-06
\end{description}
\end{quote}
\filbreak

\item
{\bf {\bf %(q:Compromis) du Conseil de l'UE pour une brevetabilité illimitée\footnote{\url{http://localhost/swpat/lisri/04/cons0129/index.en.html}}}}

\begin{quote}
Le Conseil des ministres de l'Union europ\'{e}enne, actuellement pr\'{e}sid\'{e} par l'Irlande, fait circuler un document de travail avec des contre-propositions aux amendements du Parlement europ\'{e}en. Contrastant avec la version du Parlement europ\'{e}en, la version du Conseil autorise une brevetabilit\'{e} illimit\'{e}e et le respect l\'{e}gal des brevets.  D'apr\`{e}s la version du Conseil, le q(\percent{}:One Click shopping) d'Amazon est sans l'ombre d'un doute une invention brevetable, la publication de programmes sur un serveur constitue d\'{e}j\`{a} une infraction et l'utilisation de formats de fichier brevet\'{e}s dans un but d'interop\'{e}rabilit\'{e} n'est pas autoris\'{e}e.  Puisque la proc\'{e}dure de d\'{e}cision du Conseil est secr\`{e}te, on ne sait pas qui appuie cette proposition au nom de quel gouvernement mais il est bien connu que le groupe de travail responsable est compos\'{e} de fonctionnaires des bureaux des brevets nationaux et de gens proches de ce groupe qui ont \'{e}galement un si\`{e}ge c\^{o}te \`{a} c\^{o}te au conseil administratif de l'Office europ\'{e}en des brevets.
\end{quote}
\filbreak

\item
{\bf {\bf EU Boosts Microsoft's Monopoly\footnote{\url{http://localhost/swpat/lisri/04/cecms0326/index.en.html}}}}

\begin{quote}
The European Commission's competition procedings against Microsoft have led to a verdict which gives a big boost to Microsoft's monopoly position in the OS market and helps Microsoft expand this position to other markets.  While the Commission may have earned substantial revenues for itself by imposing a one-time fine of 1\percent{} of Microsoft's liquid cash reserves, the smallprint of the verdict gives Microsoft green light to kill its main competitors in the operating systems market.  This smallprint was simultaneously reinforced through backroom deals in the Council's Patent Policy working party, of which copies have been leaked to FFII.  Immediately after the announcments the stock value of MSFT rose by 3\percent{}.
\end{quote}
\filbreak

\item
{\bf {\bf EU Council 2004 Proposal on Software Patents\footnote{\url{}}}}

\begin{quote}
The Council of Ministers has reached political agreement on a paper which contains alternative suggestions to the amendments on the directive ``on the patentability of computer-implemented inventions'' passed by the European Parliament (EP). In contrast to the EP version, the council version permits unlimited patentability and patent enforceability. Following the current version, ``computer-implemented'' algorithms and business methods would be inventions in the sense of patent law, and the publication of a functional description of a patented idea would constitute a patent infringement. Protocols and data formats could be patented and would then not be freely usable even for interoperability purposes. These implications might not be apparent to the casual reader.  Here we try to decipher the misleading language of the proposal and explain its implications.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Directive: Parliament's vs Council's Version\footnote{\url{}}}}

\begin{quote}
In September 2003 the European Parliament amended a proposal from the European Commission for patentability of software in such a way that it became a proposal that clearly disallows software patents.   The EU Council of Ministers and the European Commission subsequently disregarded the European Parliament's Amendments and reached ``political agreement'' in May 2004 in favor of a text that goes even further than the previous text of the Commission in imposing US-style unlimited patentability, but shrouds this in a veil of convoluted wording, characterised by multiple negations and other misleading syntactic devices.  Below you can find out yourself by carefully reading the core provisions of the proposed EU directive ``on the patentability of computer-implemented inventions'' which we have collected in a table, together with short explanations of the implications.  At the end we add pointers to the originals from the Parliament and Council websites as well as to related analyses and news reports.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003-09-24: Amended Software Patent Directive\footnote{\url{http://localhost/swpat/papri/europarl0309/index.en.html}}}}

\begin{quote}
Consolidated version of the amended directive ``on the patentability of computer-implemented inventions'' for which the European Parliament voted on 2003-09-24.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Plans Shelved Amid Massive Demonstrations\footnote{\url{http://localhost/swpat/lisri/03/demo0827/index.en.html}}}}

\begin{quote}
On Aug 28th, the European Parliament postponed its vote on the proposed EU Software Patent Directive.  The day before, approximately 500 persons had gathered for a rally beside the Parliament in Brussels, accompanied by an online demonstration involving more than 2000 websites.  The events in and near the Parliament were reported extensively covered in the media, including tv and radio, all over Europe and beyond.  Within a few days, the petition calling the European Parliament to reject software patentability accumulated 50,000 new signatures.
\end{quote}
\filbreak

\item
{\bf {\bf PC Magazine: Patent Riots of 2003\footnote{\url{http://www.pcmag.com/article2/0,4149,1236389,00.asp}}}}

\begin{quote}
John C. Dvorak, famous author in prestigious US magazine, says that the year 2003 marks a new height in the crisis of the patent system, which has gone berserk to the extent that it is almost causing civil unrest.  Dvorak cites some texts from the ffii site and calls on readers to support the FFII.
\end{quote}
\filbreak

\item
{\bf {\bf FFII Demo Site\footnote{\url{http://demo.ffii.org}}}}

\begin{quote}
Introduction to Demos and related FFII Events in Brussels and elsewhere
\end{quote}
\filbreak

\item
{\bf {\bf CEC 2003/11: Secret Nitpicking on European Parliament's Amendments\footnote{\url{}}}}

\begin{quote}
The Industrial Property Unit of the Commission of the European Communities (CEC) had stated in October 2003 that it finds the European Parliament's Amendments to its software patent directive proposal mostly inacceptable.  In a confidential document distributed to EU member state governments in November 2003, the Commission's patent officials added some critical notes about each of the amendments of the European Parliament.  The Commision points out that the text deviates from the practise of the European Patent Office in its use of the terminology and in its reasoning.  This is enough for the Commission to find the Parliament's text inacceptable.  Rather than examine the the merits of the Parliament's versus the EPO's approach, the Commission treats the EPO's approach as the absolute authority that must be followed and tries to find fault in the Parliament's legal logic, mostly by misunderstanding this logic or claiming that it is unclear or that it is at odds with some established practise.  Some of these claims are provably untrue.  The Commission's own proposal has been heavily criticised by prominent patent law experts for its incoherence and lack of clarity.
\end{quote}
\filbreak

\item
{\bf {\bf Influencing the EU Council\footnote{\url{http://localhost/swpat/gasnu/consilium/index.en.html}}}}

\begin{quote}
Collect and Provide Information on who makes the policies of the Council on patents and information infrastructure issues and how to best contact these people.
\end{quote}
\filbreak

\item
{\bf {\bf CEOs of big telcos sign letter against Europarl Amendments\footnote{\url{http://localhost/swpat/lisri/03/telcos1107/index.en.html}}}}

\begin{quote}
The chief executive officers of Alcatel, Ericsson, Nokia and Siemens have signed a letter to the European Commission and the European Council which complains about the European Parliament's amendments to the proposed software patent directive, saying that these will effectively remove the value of most of the patents of their companies and thereby harm the competitiveness of Europe's industry and violate the TRIPs treaty.  FFII points out that the Directive indeed threatens the interests of the patent departments of such companies, but not of the companies themselves: The letter is characterised by untruthful dogmatic assertions which say much about the thinking of patent departments and little about the interests of their companies, many of whose employees, especially software developers, support the positions of FFII.
\end{quote}
\filbreak

\item
{\bf {\bf Irish EU Presidency to %(q:protect software inventions) in May\footnote{\url{http://localhost/swpat/lisri/04/ieeu0109/index.en.html}}}}

\begin{quote}
The Irish vice prime minister has unveiled a brochure which describes the agenda of the Competitiveness Council of the Irish EU Presidency.  The brochure places high emphasis on the Community Patent and the IP Enforcement Directive and somewhat lower emphasis on the software patent directive, although it asserts that ``effective instruments'' for ``protection'' of ``software inventions'' form an ``important underpinning'' of the ``knowledge based economy''.  The IE Presidency will try to bring about an agreement on the software patent directive at the May meeting of the Competitiveness Council.
\end{quote}
\filbreak

\item
{\bf {\bf Aufrufe zum Handeln\footnote{\url{}}}}

\begin{quote}
Make sure that calls for action are supported by key actors.
\end{quote}
\filbreak

\item
{\bf {\bf Nokia and Software Patents\footnote{\url{http://localhost/swpat/gasnu/nokia/index.en.html}}}}

\begin{quote}
Tim Frain, head of Nokia's patent department, is a ``permanent resident'' of the European parliament and has used every opportunity to ask politicians in Brussels and in Finland to support the European Commission's software patentability directive.  He is present at conferences everywhere.  He argues that small companies badly need software patents because otherwise their ideas might be stolen by large companies.  Interestingly, most of the software which Nokia uses in its mobile phones is written by Opera, a relatively small (120 employees) company which has actively supported the Eurolinux campaign against software patents.  Frain's department is one of the most active producers of software patents in Europe.  Here you find an overview of their applications at the European Patent Office.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{Media contacts}
\begin{description}
\item[mail:]\ media at ffii org
\item[phone:]\ Hartmut Pilch +49-89-18979927 (German/English/French)
Benjamin Henrion +32-498-292771 (French/English)
Jonas Maebe +32-485-36-96-45 (Dutch/English/French)
Dieter Van Uytvanck +32-499-16-70-10 (Dutch/English/French)
Erik Josefsson +46-707-696567 (Swedish/English)
James Heald +44 778910 7539 (English)
More Contacts to be supplied upon request
\end{description}
\end{sect}

\begin{sect}{ffii}{About the FFII -- www.ffii.org}
The Foundation for a Free Information Infrastructure (FFII) is a non-profit association registered in Munich, which is dedicated to the spread of data processing literacy.  FFII supports the development of public information goods based on copyright, free competition, open standards.  More than 500 members, 1000 companies and 70000 supporters have entrusted the FFII to act as their voice in public policy questions in the area of exclusion rights (intellectual property) in data processing.
\end{sect}

\begin{sect}{url}{Permanent URL of this Press Release}
http://localhost/swpat/lisri/04/cons0408/index.en.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

