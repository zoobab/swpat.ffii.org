<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: The gloves come off for Round Two in the EU fight over Software
Patents

#descr: After months of closed back room discussions, the Irish Presidency of
the European Union has referred the proposed EU Directive on software
patents back up to %(q:political) level.  The Irish want members of
the Council of Ministers of the member states to agree to drop all
objections by May.  The Presidency proposed draft text rejects all
clarifying amendments made by the European Parliament in September
2003 and instead pushes for direct patentability of computer programs,
data structures and process descriptions.  A last ditch attempt by the
Luxembourg delegation to ensure interoperability with patented
standards was rejected.  The Patent Department at Nokia is collecting
signatures from top company executives for a %(q:Call for Action) in
favour of the Presidency text.  In the other corner, supporters of the
European Parliament's position have arranged conferences to explain
the dangers of software patents, and are mobilising for a %(q:net
strike) and a rally in Brussels on April 14th under the slogan %(q:No
Software Patents -- Power to the Parliament).  They are hoping for a
repeat of the impact of similar actions in the run-up to September
2003, which helped convince the European Parliament to vote clearly
against software patents.

#ssi: Software Patents return to the EU Political Centre Stage

#oio: %(q:Call for Action) from Nokia's Patent Department

#nCr: Reactions by MEPs to the Council Working Party position

#dje: Presidency Text Rejects Interoperability

#Slf: Net Strike, Rallies, Conferences

#dca: Media contacts

#otF: About the FFII

#nWW: Permanent URL of this Press Release

#eor: Following months hidden away deep in the Brussels undergrowth, debate
on the controversial European Directive on software patents is
returning back to the political high level.  On Tuesday 6 April, the
Irish Presidency of the EU referred the issue to the CoRePer committee
of Member States' Permanent Representatives, the traditional venue for
difficult political horse trading.

#W1x: In response, campaigners for and against software patents are
mobilising their constituents in support of either the Council or the
Parliament.  The supporters of the Parliamment have announced a day of
mass action in Brussels on Wednesday 14 April, culminating in a
high-level conference in the European Parliament building itself. 
Additionally, they are urging their supporters to go on %(q:net
strike) next week, blacking out their websites to %(q:demonstrate the
effects of software patents before it is too late).

#rxr: The move by the Irish Presidency on Tuesday to refer the dossier was
not unexpected, but marks a significant return of the dossier to the
political centre-stage.

#Wcu: According to one source familiar with the Presidency position,
%(q:there has been some clear progress in the Working Group with
problems that some of the Member States had had on some particular
issues of wording; but there are still significant differences between
Member States on some of the most key fundamental issues.  The general
feeling is that work at the technical level has now gone about as far
as it can, and strong input from the political level is now going to
be needed, if general agreement is to be achieved by May).

#sih: The member states are supposed to sign off their common position at a
meeting of the Competiveness Council of Ministers to be held in
Brussels on May 17-18.

#AWW: Lobbyists in favour of software patents are gearing up for the fight. 
FFII has obtained a copy of a %(no:round-robin letter) circulated by
%(Nokia)'s Tim Frain (Nokia/Southwood) and Dany Ducoulombier
(Nokia/Brussels) for pro-patent signatures before April 8th.  The
letter calls on ministers to drop their objections, and to support a
draft text issued by the Irish Presidency on March 17th:

#dWi: %(q:All of Europe's innovators, including individual inventors,
%(tp|small and medium size enterprises|SMEs), as well as large
multinational companies, require patents to protect their inventions,
provide incentives to undertake research and development in Europe,
and to promote licensing and technology transfer), claims the letter.

#ohe: %(q:Nokia doesn't seem to be counting Opera among the European
innovators), comments Håkon Wium Lie, CTO of Opera Software Inc, an
innovation leader in the web browser market and producer of much of
the software used in Nokia's mobile phones.

#onr: And, as Hartmut Pilch president of FFII and speaker of the Eurolinux
Alliance explains, Opera is just one in thousands of innovative
European companies who have publicly endorsed our petitions against
software patents.

#lou: Pilch continues:

#Wsb: The Nokia patent department's claim that patents are needed to fund
research in the ICT sector looks like a desperate attempt to mobilise
the misconceptions of people who are not familiar with this sector. 
All the %(ss:economic studies) we know of, including those ordered by
the European Commission and by member state governments, have shown
that software patents are only of very secondary importance as a means
of capturing returns from R&D investments.  The main drivers of
competitive advantage are copyright, trade secret, complexity, network
effects and the ability to react quickly to customer needs.  In fact,
according to the most detailed economic studies, confirmed by numerous
testimonies from company directors, patent investments have actually
tended to reduce spending and divert it %(e:away) from R&D investment
in this sector.

#nWr: The Nokia letter looks very similar in style and content to the
%(c5:letter of the 5 CEOs) of last november and the %(js:Joint
Industry Statement) of April 2003.  Nokia's head of intellectual
property, Tim Frain, whom we have also previously identified as the
author of these letters, is solliciting signatures from CTOs this
time.  Yet this letter, like his others, is still written from a
perspective of a corporate patent lawyer who fears an erosion of the
patent department's importance within the company.

#qjd: According to Nokia, the Irish Presidency is to be %(q:commended) for
%(q:presenting a balanced text which preserves the incentives for
European innovation in sectors as diverse as telecommunications,
information technology, consumer electronics, household appliances,
transportation and medical instruments while responding to the
European Parliament's call for limitations to ensure that
patentability does not extend into non-technical areas or unduly
hinder interoperability in our increasingly networked society.

#oor: On the other hand, FFII's UK co-ordinator James Heald, says the text
is actually %(q:the most extreme yet seen, put together only from the
most pro-patent sections of all the previous texts.  All of the
important amendments passed by the European Parliament in September
are completely ignored. The draft text is deliberately blind to all of
the problems which the Parliament tried to address.)

#jep: This view is shared by leading MEPs.

#jep2: %(PNK), Finnish MEP of the European People's Party, expresses dismay
at the Council Working Party's contempt for parliamentary democracy:

#lie: As the Council is trying to look for a compromise with the European
Parliament on the proposal for patenting of computer-implemented
innovations (software patents), it should base its work on the final
decision taken by the plenary session of the Parliament, not on that
of the Commission or of the Legal Affairs Committee.  Judging from the
papers produced so far by the Council's working party, it seems that
the Council is not taking the will of Europe's elected legislators
into account.

#kht: %(DCB), chairman of the %(ge:Greens/EFA Group) adds:

#toa: The Council working party has so far completely failed to address the
problems which the European Parliament's Cultural and Industrial
Affairs committees tried to solve.  They behave in the same way as the
Legal Affairs Committee behaved last year, and we can expect that they
will fail in the same way.

#tec: It is clear that the national patent officials in the Council do not
want %(q:harmonisation) or %(q:clarification). They merely want to
secure the interests of the patent establishment. If they don't get
what they want, they simply bury the directive project and try to find
other ways to get around the existing law, whose clarity is so painful
to them.

#iWi: %(BHA) MEP of the Danish %(jm:June Movement) and the %(ed:EDD Group)
draws attention to the lack of democracy in the EU which is
exemplified by the Commission's and Council's behaviour:

#cne: The approach of the Commission and Council in this directive is
shocking.  They are making full use of all the possibilities of
evading democracy that the current Community Law provides.  First they
ignored 94% of the participants of their own consultation, without
given any justification apart from the claim that the remaining 6%
represented the %(q:economic majority).  Now they are completely
disregarding the vote of the European Parliament, and by the way also
of the Economic and Social Council and of the Council of Regions. 
They are doing this because they are used to succede by doing this. 
The EU is constructed this way.  It makes unaccountable bureaucrats
the masters of legislation.  The problem is compounded by the complete
lack of democratic checks and balances in the European patent system. 
EU and Patents combine into a particularly toxic mixture.  Europe's
citizens urgently need to take up this issue and learn the lessons
before it is too late.  They should in particular not allow this kind
of structure to be perpetuated by a European Constitution this year.

#itu: 15 MEPs have signed a %(ca:Call for Action) (same title as used by
Nokia for its round-robin letter) which points out that %(q:patent
professionals in various governments and organisations are now trying
to use the EU Council of Ministers in order to sidestep parliamentary
democracy in the European Union) and urges the Council to %(q:refrain
from any counter-proposals to the European Parliament's version of the
draft, unless such counter-proposals have been explicitely endorsed by
a majority decision of the member's national parliament).

#WeW: FFII's most hollow laughter is directed at the claim that the Irish
proposed text would not %(q:unduly hinder interoperability).

#Msj: Jonas Maebe, Belgian speaker of FFII, explains:

#eei: The Industry committee, and the Legal Affairs committee, and the full
session of the European Parliament, all demanded a special provision
to allow data to be inter-converted between different packages and
software platforms.  Otherwise companies could use software patents to
lock in users' data to a particular program or operating system, and
competition would be impossible.

#cwa: It's a systematic problem.  Each and every market niche is
individually potentially at risk.  That's why, in the final vote in
September, the European Parliament voted in favour of the provision by
393 votes to 35.

#suw: But according to Nokia, the Council Working Party has %(q:responded)
to the European Parliament's call, so everything's all right. And how
(despite a valiant %(lx:last-ditch opposition by the Luxemburgers))
does the Working Party propose to respond ?  By deleting the European
Parliament's clause entirely, and instead inserting a recital clause
that says any problems can be left to existing antitrust law.

#eaW: Remember, this is the antitrust law which has just taken four years,
at vast expense, to go after a single accused company, Microsoft. 
With the result that Microsoft %(cm:received green light for
collecting toll fees from anyone wishing to communicate with its
patented standards), and that Microsoft will appeal to further improve
the conditions and tie up the procedings for another four years.

#dyr: One might start to wonder what kind of dream world these people live
in.

#iWl: The FFII is meanwhile asking its 50,000 supporters and 300,000
petition signatories to demonstrate both on the Internet and in
Brussels on April 14th.

#snW: The website demo.ffii.org provides numerous sample strike pages and
banners which can be used by webmasters to support the action.

#Wri: The Brussels events begin on April 14 at 10.00 with a press conference
in the European Parliament, room AG2, at 10.00.  The demonstrators
will assemble at 11.30 beside the Parliament.  Participants will wear
t-shirts with the slogans %(q:No Software Patents -- Power to the
Parliament).  There will be speeches and performances.

#nri: The demonstration is followed by an interdisciplinary conference in
the European Parliament, again room AG2, at 14.00.  Among the
participants of the intensely prepared discussion agenda are members
of the European Parliament, officials from the European Commission,
the Council Working Party and the European Patent Office, software
developpers, economists, lawyers of various schools of thought.

#gBW: Program of Brussels Events

#eeI: So far, more than %(N) participants have %(ak:registered) with FFII
for the conference.

#dpr: Further net strikes and conferences in various European Capitals will
take place during the months up to the elections of the European
Parliament on June 10-13, in particular during the week after the Day
of Europe of May 9th.

#nu5: Council Document 8253/04

#Tit: Title

#Weo: Proposal for a Directive of the European Parliament and of the Council
on the patentability of computer-implemented inventions - Preparation
of the Council's common position

#RPR: REPORT

#tfl: Date of Upload

#e24: tuesday 2004-04-06

#om4: EU Council document  8253/04 ADD 1

#eoe: Addendum to the Report

#Ceo: More Contacts to be supplied upon request

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040408 ;
# txtlang: en ;
# multlin: t ;
# End: ;

