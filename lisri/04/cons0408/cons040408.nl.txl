<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Tweede ronde in Europese strijd tegen softwarepatenten

#descr: Na maanden van gesprekken achter gesloten deuren heeft het Ierse
voorzitterschap van de Europese Unie de voorgestelde EU-richtlijn over
sofwarepatenten teruggebracht naar het %(q:politieke) niveau. De Ieren
willen dat de leden van de Raad van Ministers van de lidstaten
overeenkomen om tegen mei alle bezwaren te laten vallen. De tekst van
het vooritterschap verwerpt alle verhelderende amendementen van het
Europees Parlement van september 2003. In plaats daarvan drukt hij
pleit hij voor omonwonden octrooien op computerprogramma's,
datastructuren procesbeschrijvingen. Een poging van de laatste kans
door de Luxemburgse delegatie om interoperabiliteit met gepatenteerde
standaarden te verzekeren werd verworpen. De patentafdeling van Nokia
verzamelt momenteel handtekeningen van bedrijven die voorstander zijn
van de tekst van het voorzitterschap. Aan de andere kant staan de
sympathisanten van het voorstel van het Europees Parlement. Zij roepen
op tot een %(q:webstaking) en een   demonstratie in Brussel op 14
april onder het motto %(q:Geen softwarepatenten -- Alle macht aan het
parlement).

#ssi: Softwarepatenten terug in de schijnwerpers van de Europese politiek

#oio: %(q:Oproep tot handelen) van Nokia's patentafdeling

#nCr: Reacties van EP-leden over de positie van de Raad-werkgroep

#dje: Tekst van het voorzitterschap verwerpt interoperabiliteit

#Slf: Webstaking, Demonstraties, Conferenties

#dca: Contactpersonen voor de media

#otF: Over FFII

#nWW: Permanente URL van dit persbericht

#eor: Na maanden van verborgen Brusselse achterkamerpolitiek keert het debat
over de controversiële Europese richtlijn over softwarepatenten terug
naar het hoogste politieke niveau. Op dinsdag 6 april stuurde het
Ierse voorzitterschap van de EU de zaak terug naar CoRePer. Dat
comité, dat bestaat uit permanente vertegenwoordigers van de
lidstaten, staat traditioneel bekend als het centrum van koehandel
voor moeilijke dossiers.

#W1x: Als antwoord hierop mobiliseren campagnevoerders voor en tegen
softwarepatenten hun medestanders in de Raad of het Parlement. De
sympathisanten van het Parlement hebben een dag van massale acties in
Brussel aangekondigd, op woensdag 14 april. Hoogtepunt wordt een
conferentie in het gebouw van het Europees Parlement. Ze vragen
daarnaast hun medestanders om deel te nemen aan een %(q:webstaking)
volgende week. Daarbij worden websites afgesloten om %(q:de gevolgen
van softwarepatenten te demonstreren, voor het te laat is).

#rxr: De zet van het Ierse voorzitterschap om het dossier terug te sturen
was niet onverwacht, maar geeft duidelijk aan dat het dossier opnieuw
in het centrum van de politieke belangstelling staat.

#Wcu: Volgens een bron die vertrouwd is met het standpunt van het
voorzitterschap %(q:is er duidelijk vooruitgang merkbaar binnen de
werkgroep op het gebied van de problemen die sommige lidstaten hadden
met de bewoordingen. Maar er zijn nog steeds belangrijke verschillen
tussen lidstaten in verband met sommige van de meest fundamentele
discussiepunten. Algemeen wordt aangenomen dat het technische werk
voor zover mogelijk nu achter de rug is. Er is nu nood aan inbreng van
uit de politiek, als er algemene overeenstemming tegen mei bereikt
moet worden).

#sih: De lidstaten worden verondersteld om tot een gemeenschappelijk
standpunt te komen bij een bijeenkomst van de Raadsformatie
Concurrentievermogen in Brussel op 17 en 18 mei.

#AWW: Lobbyisten voor softwarepatenten maken zich op voor de strijd. FFII
heeft een kopij bemachtigd van een %(no:aanbevelingsbrief) die uitging
van %(Nokia) bij monde van Tim Frain (Nokia/Southwood) en Dany
Ducoulombier (Nokia/Brussel) waarin gevraagd wordt naar pro-patent
handtekeningen.

#dWi: %(q:Alle Europese innovatieve krachten, inclusief individuele
uitvinders, %(tp|kleine en middelgrote ondernemingen|KMO's), evenals
grote multinationals, hebben patenten nodig om hun uitvindingen te
beschermen, een beloning te voorzien voor het voeren van Onderzoek en
Ontwikkeling (O&O) in Europa, en om licentie- en
technologie-uitwisselingen te promoten), zo stelt de brief.

#ohe: %(q:Het lijkt erop dat Nokia Opera niet onder de Europese innovatieve
krachten rekent), zegt Håkon Wium Lie, CTO van Opera Software Inc, een
innovatieleider in de webbrowsermarkt en producent van veel van de
software die in Nokia's mobiele telefoons gebruikt wordt.

#onr: En, zoals Hartmut Pilch (voorzitter van FFII en woordvoerder van de
Eurolinux-alliantie) verklaart, is Opera slechts een van de duizenden
innovatieve Europese bedrijven die publiek onze petities tegen
softwarepatenten steunen.

#lou: Pilch gaat verder:

#Wsb: Nokia's patentafdeling stelt dat patenten nodig zijn om onderzoek te
bekostigen in de softwaresector. Dat lijkt op een wanhopige poging om
de misverstanden te versterken van mensen die niet vertrouwd zijn met
de ICT-sector. Alle %(ss:economische studies) die we kennen, inclusief
die die door de Europese Commissie en door lidstaten besteld is,
hebben aangetoond dat softwarepatenten slechts een tweederangsrol
vervullen als middel om onderzoeksfondsen terug te verdienen. De
belangrijkste drijfveren van concurrentievoordeel zijn het
auteursrecht (copyright), het bedrijfsgeheim, complexiteit,
netwerkeffecten en de mogelijkheid om snel klantgericht te reageren.
In feite hebben patentinvesteringen, zo wijzen de meest gedetailleerde
studies uit, ertoe geleid dat het onderzoeksbudget daalt en
%(e:weggehaald) wordt van O&O-investeringen in deze sector. Talrijke
getuigenissen van bedrijfsleiders bevestigen dit.

#nWr: De brief van Nokia lijkt qua stijl en inhoud erg op de %(c5:brief van
de 5 CEO's) van november laatstleden en het %(q:Gezamelijke Standpunt
van de Industrie) van april 2003. Tim Frain, hoofd van de afdeling
Intellectueel Eigendom bij Nokia, die ook verantwoordelijk was voor de
eerdere brieven, probeert dit maal handtekeningen van CTO's te
verzamelen. Niettemin is zijn brief geschreven vanuit het perspectief
van een patentadvocaat van een bedrijf die vreest voor de erosie van
het belang van de octrooiafdeling binnen die onderneming. Om deze
erosie te voorkomen leggen Frain en een paar andere patentadvocaten
steeds opnieuw goedgelovige mensen woorden in de mond die niet meer
zijn dan een eeuwige herhaling van enkele bedrieglijke mantra's. Maar
dit keer, door zich te richten op CTO's, bedriegen ze niet alleen de
lezers maar ook de ondertekenaars. De brief stelt dat de positie van
de Raad erin bestaat patenteerbaarheid van %(q:consumentenelektronica,
huishoudtoestellen, tran  sportmiddelen en medische instrumenten) te
verzekeren. De amendementen van het Europees Parlement zouden deze
zaken onpatenteerbaar maken. Niets is minder waar. Maar de CTO's
kunnen vaak onmogelijk deze stellingen onderzoeken, in het bijzonder
als hun secretariaat hen vertelt dat de juridische afdeling deze
%(q:Oproep tot Handelen) steunt. Dus, zelfs als CTO's deze tekst
ondertekenen, kunnen we

#qjd: Volgens Nokia is het %(q:raadzaam) voor het Ierse voorzitterschap om
%(q:een evenwichtige tekst voor te stellen die de beloningen behoudt
voor Europese innovatie in diverse sectoren als telecommunicatie,
informatietechnologie, consumentenelektronica, huishoudapparaten,
transportmiddelen en medische instrumenten). Dat terwijl het Europees
Parlement vroeg om beperkingen om te verzekeren dat patenten niet
doordringen tot niet-technische gebieden of interoperabiliteit ernstig
gehinderd wordt in onze netwerkmaatschappij.

#oor: Aan de andere kant zegt James Heald, coördinator van FFII-Engeland,
dat de tekst eigenlijk %(q:de meest extreme tot nu toe is, en hij
alleen samengesteld is uit de meest pro-patentgerichte delen van alle
vorige teksten. Alle belangrijke amendementen die het Europees
Parlement stemde in september worden compleet genegeerd. De voorlopige
tekst is gewild blind voor alle problemen die het EP probeerde aan te
pakken.)

#jep: Dit inzicht wordt gedeeld door vooraanstaande parlementsleden.
Parlementslid Piia-Noora Kauppi, de Finse vertegenwoordigster van de
Europese Volkspartij, spreekt haar ongenoegen uit over de de
minachting van de Raadswerkgroep voor parlementaire democratie:

#jep2: %(PNK), Finnish MEP of the European People's Party, expresses dismay
at the Council Working Party's contempt for parliamentary democracy:

#lie: Aangezien de Raad aan het proberen is om een compromis te vinden met
het Europees Parlement over het voorstel tot patentering van in
computers geïmplementeerde uitvindingen (softwarepatenten), zou het
zijn werk moeten baseren op de uiteindelijke beslissing die door het
plenaire Parlement genomen is. Niet op het voorstel van de Commissie
of het Comité voor Juridische Zaken. Te zien aan de documenten die
door de werkgroep van de Raad tot nog toe opgesteld zijn, lijkt het er
niet op dat de Raad rekening houdt met Europa's verkozen wetgevers.

#kht: EP-lid Daniel Cohn-Bendit, voorzitter van de Groenen/EFA-groep, stelt
onomwonden vast:

#toa: De werkgroep van de Raad is er tot nu toe totaal in mislukt om de
problemen aan te pakken die de comités Cultuur en Industrie van het EP
probeerden op te lossen. Ze gedragen zich op dezelfde manier als het
comité Juridische Zaken vorig jaar, en we kunnen verwachten dat ze op
dezelfde manier zullen mislukken.

#tec: Het is duidelijk dat de nationale patentambtenaren in de Raad geen
%(q:harmonisering) of %(q:verduidelijking) willen. Ze willen veeleer
de belangen van het octrooi-etablishment verzekeren. Als ze niet
krijgen wat ze willen, begraven ze simpelweg de richtlijn en proberen
ze andere middelen te vinden om de bestaande wet te omzeilen. De
duidelijkheid van de bestaande wet is werkelijk pijnlijk voor hen.

#iWi: Bent Hindrup Andersen von der Dänischen Junibewegung und der
%(ed:EDD-Gruppe) lenkt die Aufmerskamkeit auf allgemeinere Probleme,
die sich am Verhalten der Kommission  und des Rates offenbaren:

#cne: The approach of the Commission and Council in this directive is
shocking.  They are making full use of all the possibilities of
evading democracy that the current Community Law provides.  First they
ignored 94% of the participants of their own consultation, without
given any justification apart from the claim that the remaining 6%
represented the %(q:economic majority).  Now they are completely
disregarding the vote of the European Parliament, and by the way also
of the Economic and Social Council and of the Council of Regions. 
They are doing this because they are used to succede by doing this. 
The EU is constructed this way.  It makes unaccountable bureaucrats
the masters of legislation.  The problem is compounded by the complete
lack of democratic checks and balances in the European patent system. 
EU and Patents combine into a particularly toxic mixture.  Europe's
citizens urgently need to take up this issue and learn the lessons
before it is too late.  They should in particular not allow this kind
of structure to be perpetuated by a European Constitution this year.

#itu: De ondoorzichtige procedures en niet-constructieve werkwijze hebben
ook aanleiding gegeven tot een nieuwe %(ca:Oproep tot Handelen), die
door talrijke parlementairen ondertekend is sinds de publicatie ervan
in november laatstleden. Nokia schijnt de titel van hun brief ontleend
te hebben aan deze welbekende %(q:Oproep tot Handelen).

#WeW: Het meest ironisch vindt FFII de bewering dat de tekst die voorgesteld
werd onder het Iers voorzitterschap %(q:de interoperabiliteit niet
overdreven zou schaden).

#Msj: Jonas Maebe, de Belgische vertegenwoordiger van FFII verduidelijkt:

#eei: De parlementaire commissies ITRE (industrie, externe handel, onderzoek
en energie) en JURI (juridische zaken en interne markt) en de plenaire
vergadering van het Europees Parlement, vroegen allen een speciale
bepaling om dataconversie tussen verschillende pakketten en
softwareplatformen mogelijk te maken. Zonder zulke bepaling kunnen
bedrijven softwarepatenten gebruiken om consumenten voor het gebruik
van hun data afhankelijk te maken van een bepaald programma of
besturingssysteem; concurrentie zou onmogelijk zijn.

#cwa: Het is een structureel probleem. Elke individuele nichemarkt wordt
bedreigd. Dat is ook de reden waarom in de uiteindelijke stemming in
September het Europees Parlement de bepaling goedkeurde met een
meerderheid van 393 stemmen tegen 35.

#suw: Maar volgens Nokia is alles in orde, want, zo zeggen ze, de Werkgroep
van de Raad van Ministers komt tegemoet aan de vraag van het Europees
Parlement. Maar hoe (afgezien van een dapper %(lx:verzet van
Luxemburg)) doet de Werkgroep dat? Door het artikel van het Europees
Parlement in zijn geheel te verwijderen, en in plaats daarvan een
overweging toe te voegen dat alle problemen kunnen worden overgelaten
aan de bestaande kartelwetgeving.

#eaW: Denk eraan, dit is de anti-monopoliewet die er net 4 jaar over gedaan
heeft, met enorme uitgaven, om achter één %(q:enkel) beschuldigd
bedrijf aan te gaan, Microsoft. Microsoft zegt nu dat het voor nog
eens 4 jaar in beroep kan gaan. Uiteindelijk lijkt het er op dat de
zaak in der minne geregeld zal worden met een cross-license akkoord
tussen Microsoft en Sun. Samba (een vrij software project dat
Windows-compatibel bestands- en printerdelen aanbiedt, dat aanbevolen
en gebruikt wordt door bedrijven als IBM, HP en Apple) zal vast en
zeker niet op dat feestje uitgenodigd worden.

#dyr: Men kan zich de vraag stellen in wat voor droomwereld deze mensen
leven.

#iWl: Ondertussen mobiliseert FFII zijn 50,000 sympathisanten en 300.000
mensen die een petitie ondertekenden om te protesteren op het Internet
en in Brussel op 14 april. De website demo.ffii.org vermeldt:

#snW: De site demo.ffii.org verschaft verschillende voorbeelden van
protestpagina's en banners die webmasters kunnen gebruiken om de actie
te ondersteunen.

#Wri: De evenementen in Brussel op 14 april beginnen om 10.00h met een
persconferentie in het Europees Parlement, zaal AG2. De betoging wordt
gevormd om 11.30h voor het Europees Parlement. De deelnemers zullen
t-shirts dragen met de slogan %(q:Geen softwarepatenten -- Alle macht
aan het parlement). Er zullen toespraken en optredens zijn.

#nri: De betoging wordt gevolgd door een interdisciplinaire conferentie in
het Europees Parlement, weer in zaal AG2, om 14.00h. Onder de
deelnemers van de intensief voorbereide discussie zijn leden van het
Europees Parlement, officiële afgevaardigden van de Europese
Commissie, van de Werkgroep van de Raad van Ministers en van het
Europees Octrooi Bureau, software ontwikkelaars, economen en juristen
van verschillende strekkingen.

#gBW: Programma van de evenementen in Brussel

#eeI: Meer dan 150 deelnemers hebben zich reeds %(ak:geregistreerd) bij FFII
voor de actie.

#dpr: In de maanden voor de Europese Parlementsverkiezingen van 10-13 juni,
en vooral in de week na de Dag van Europa van 9 mei, zullen er
aanvullende online betogingen en conferenties zijn in verschillende
Europese hoofdsteden.

#nu5: Raadsdocument 8253/04

#Tit: Titel

#Weo: Voorstel tot Richtlijn van het Europees Parlement en van de Raad over
de patenteerbaarheid van in computers geïmplementeerde uitvindingen -
Voorbereiding van de gemeenschappelijke positie van de Raad

#RPR: Rapport

#tfl: Datum van Upload

#e24: dinsdag 2004-04-06

#om4: EU Raadsdocument 8253/04 ADD 1

#eoe: Toevoeging bij het Rapport

#Ceo: Meer contactpersonen worden op verzoek gegeven

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: dietvu ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040408 ;
# txtlang: nl ;
# multlin: t ;
# End: ;

