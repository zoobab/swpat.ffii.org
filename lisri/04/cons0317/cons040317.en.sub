\begin{subdocument}{cons040317}{EU Council Patent Officials Reject Luxemburg Proposal for Interoperability Exemption}{http://swpat.ffii.org/news/04/cons0317/cons040317.en.html}{Workgroup\\swpatag@ffii.org\\english version 2004/04/04 by FFII\footnote{http://lists.ffii.org/mailman/listinfo/traduk/TradukListinfo.en.html}}{The Council of Ministers of the European Union is currently conducting secret negotiations between national patent administrators about a controversial EU directive ``on the patentability of computer-implemented inventions'', colloquially termed ``the software patent directive''.  While the EU Parliament had voted in September 2003 to reconfirm the exclusion of software from patentability, the EU Council have, along with the European Patent Office over whose Administrative Council they preside, been pushing to establish and legalise an US-style practise of patenting ``computer-implemented'' algorithms and business methods.  In particular the Council's patent policy working party has taken an extreme position, dubbed as ``compromise'', in a working paper circulated internally on January 29 by the Irish Council Presidency.  During secret negotiations on March 2, this position was apparently confirmed and further reinforced by the Council members.  A proposal by the Luxemburg delegation to allow use of a patented technique for interoperation (e.g. to allow the use of GIF images as long as Internet Explorer does not yet support patent-free and functionally superior alternatives such as PNG and MNG) was rejected by the group.  Moreover the group added a recital wording which states that a right to interoperate with a patented standard can only be obtained through costly and lengthy competition procedings on a patent-by-patent basis, such as the European Commission's procedings against Microsoft of this year, which in fact resulted in the denial of such rights.  Once more, the Council Working Group is offering hardened extreme positions as ``compromises'' and disguising the meaning of these positions in deceptive wordings.  Once more, the Council Working Group is doing this through secret backroom negotiations, refusing to reveal which governments are responsible for the positions taken and how the different interests were taken into account.  The Luxemburg paper was not published by the Council Working Party.}
\begin{sect}{bakgr}{backgrounds}
Current European and international law (EEC software directive, European Patent Convention, TRIPs) states that computer programs are to be ``protected as literary works'' by the rules of copyright and are ``not inventions in the sense of patent law''.  The European Patent Office, the European Commission and the Council of the European Union have since 1997 been involved in a concerted effort to change these rules and adopt a regime similar to that of the USA, where software is subjected to both copyright and patents.  In 2002 the European Commission, backed by the member states through the EU Council of Ministers, proposed a EU directive ``on the patentability of computer implemented inventions'' in order to codify this regime change.  However, under the impression of strong opposition from software developpers, software companies and academia, the European Parliament amended the proposal in september 2003 so as to reconfirm the non-patentability of software.  This decision of the European Parliament is meeting fierce opposition from ministerial patent administrators in national governments and at the European Commission.
\end{sect}

\begin{sect}{draft}{The ``Council Presidency Compromise Paper''}
The EU Council's ``Working Party on Intellectual Property (Patents)'' met\footnote{http://register.consilium.eu.int/pdf/en/04/st07/st07230.en04.pdf} on March 2nd to discuss the European Parliament's amendments to the software patent directive\footnote{http://swpat.ffii.org/papers/europarl0309/europarl0309.en.html}.

The discussion was based on a confidential working document\footnote{http://swpat.ffii.org/papers/europarl0309/cons0401/cons0401.en.html} titled ``Council Presidency Compromise Paper''. This proposal removes everything from the Parliament's amendments that limits patentability, including:
\begin{enumerate}
\item
All definitions of ``technical'', ``invention'' and ``industrial'' are removed, and yet patentability is limited by nothing but these terms.

\item
Freedom of publication is removed from Art 5, instead information goods become directly claimable, so that internet service providers can be sued for allowing publication of independently developped programs on their server. (see Program Claims: Bans on Publication of Patent Descriptions\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prog/eubsa-prog.en.html})

\item
Art 6a (freedom of interoperation) is removed.  This way Microsoft receives green light for its new licensing program on file formats, and large software and telecommunication companies can charge fees for the use of the next generation of Internet standards.

\item
A few cosmetic amendments by EPO-friendly MEPs such as Arlene McCarthy and Joachim Wuermeling are admitted into the draft.  Among these are amendments which were rejected by the European Parliament in the plenary vote.
\end{enumerate}

see EU Council 2004/01/29 ``Presidency Compromise Proposal'' on Software Patents\footnote{http://swpat.ffii.org/papers/europarl0309/cons0401/cons0401.en.html} and EU Software Patent Directive Articles 1-6: Parliament's vs Council's Version\footnote{http://swpat.ffii.org/papers/europarl0309/cons0401/tab/constab0401.en.html}
\end{sect}

\begin{sect}{compr}{Compromise Position?}
Jonas Maebe, speaker of FFII in Belgium, comments on the Irish presidency's ``compromise'':

\begin{quote}
{\it It's as if in the debate on whether or not we should allow new member states to join the EU, the compromise would be to not allow them, and in addition to impose trade sanctions on them.

It's as if in a debate on whether or not we should raise the speed limits on the roads, the compromise would be to raise them and additionally remove the requirement to wear seat belts.}
\end{quote}

What is a compromise and what not should become clearer if we put the various proposals on a scale:

\begin{center}
\begin{tabular}{C{29}C{29}C{29}}
number & position & supporters\\
0 & no patents at all & many economists\footnote{http://swpat.ffii.org/archive/mirror/impact/swpatsisku.en.html} but no organised stakeholders in the current debate\\
1 & no patents on anything that somehow touches software & Certain imaginary players such as Arlene McCarthy's Free Software Alliance\footnote{http://swpat.ffii.org/players/fsa/swpatfsa.en.html} and the ``few large software distributors'' painted by patent attorney Dr. Kai Brandt in a magazine for Siemens employees\footnote{http://swpat.ffii.org/papers/siemens-brandt03/siemens-brandt03.en.html}.\\
2 & no patents on pure software, guarantee freedom of publication and interoperation & European Parliament 2003-09-24\footnote{http://swpat.ffii.org/papers/europarl0309/europarl0309.en.html}, FFII\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/eubsa-prop.en.html}\\
3 & unlimited patentability, limited enforcability & Commission 2002-02-20\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/eubsa-swpat0202.en.html}, JURI 2003-06-17\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/juri0304/juri0304.en.html}\\
4 & unlimited patentability, unlimited enforcability & EU Council 2004/01/29 ``Presidency Compromise Proposal'' on Software Patents\footnote{http://swpat.ffii.org/papers/europarl0309/cons0401/cons0401.en.html}\\
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{forc}{Interests behind the Council's Position}
It should be noted that the Council was pushed toward its extreme position by the Commission, as can be seen from a leaked Commission's paper of 2003/11\footnote{http://swpat.ffii.org/papers/europarl0309/cec0311/cec0311.en.html}.

This paper is full of untrue derogatory statements about the European Parliament's work, and it shows between the lines that the Commission's IP official would have liked to opt for program claims as well.

At the same time, the patent lawyers of large ICT companies are involving their CEOs in a letter-writing campaign to discredit the European Parliament's vote in the eyes of political decisionmakers.

The CEO letters and accompanying statements by the corporate patent lawyers attempt to spread fears that the EP amendments would prevent patenting of processes in traditional technical fields, such as industrial robots etc, as soon as a computer is used to control them, and that such a restrictive approach to patentability would seriously discourage innovation.   Both fears are groundless.  The Parliament's position is well in line with the famous Anti Blocking System\footnote{http://swpat.ffii.org/papers/bgh-abs80/bgh-abs80.de.html} decision of the German Federal Court.  It allows patents on any process that imparts new empirical knowledge about forces of nature (e.g. relations of temperature and friction in automobile brakes), regardless of whether such a process is computer-controlled or not.  The work of the process engineer is patentable, while that of the programmer falls under copyright.  The Parliament has voted for the very consensus position which the CEOs say they want.

The trouble is that the corporate patent lawyers behind these CEOs want to patent much more than what the general consensus allows.  Attentive readers easily notice that they are not asking for patents on computer-controlled robots or industrial processes, but for legalisation of 30000 US-style patents on algorithms and business methods, claimed in terms of conventional general-purpose data-processing equipment, which the European Patent Office has granted against the letter and spirit of the written law in recent years.

The pro-patent campaigners are mainly managers of large corporate patent departments.  Thanks to an inflationary practise of patent offices, they have gained great importance in recent years and are fighting to increase this importance.  The patent arms of the European Commission and national governments share the interests of the campaigners and have been equally successful in dominating the patent policy of their respective bodies.  Ever since the EU software patent directive project started in 1997, this well-organised and well-entrenched movement has pushed for unlimited patentability by spreading propaganda in the name of big authorities (governments, associations, CEOs), apparently hoping that these will compel legislators to obey without asking any questions.

Meanwhile, a group of parlamentarians, software companies and associations has published a Call for Action\footnote{http://swpat.ffii.org/papers/europarl0309/demands/europarl-cpedu.en.html} in which it criticises the attempts of the patent movement to ``deceive and intimidate the European Parliament'' and calls on national MPs to formulate their national patent policies and to supervise the activities of their government's patent experts in the Council, so as to make sure that they loyally serve their national policies.

It is common practise of the patent experts in the Council to act on their own, ignoring or circumventing any instructions from their ministers even in the rare case that such instructions exist.  The Council's Patent Working Party moreover provides a veil of anonymity where national patent policy representatives can take any point of view they like without ever being held accountable.  E.g. in Germany the german government claims that they are a moderating force in the Council and software patents are being pushed on them.  From other sources we know that the german representatives have been pushing for program claims, i.e. for the most radical form of software patentability.  But very little of all this can be proven, because even members of parliament have no access to the negotiations of the Council.  As far as written protocols of the Council sessions are accessible at all, the positions of the individual governments are anonymised.  The working papers are moreover kept confidential.  Thus national parliaments can not know about, let alone influence, legislative processes that are deeply affecting the interests, including basic freedoms, of their nation's citizens.
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf EU Council 2004/01/29 ``Presidency Compromise Proposal'' on Software Patents\footnote{http://swpat.ffii.org/papers/europarl0309/cons0401/cons0401.en.html}}}

\begin{quote}
La pr\'{e}sidence irlandaise du Conseil de l'UE a distribu\'{e} aux repr\'{e}sentants des gouvernements un papier contenat des suggestions alternatives aux amendements \`{a} la directive ``sur la brevetabilit\'{e} des inventions mises en oeuvre par ordinateur'' vot\'{e}s par le Parlement europ\'{e}en (PE). Contrastant avec la version du PE, la version du Conseil autorise une brevetabilit\'{e} illimit\'{e}e et le respect l\'{e}gal des brevets. Selon la version actuelle, les algorithmes ``mis en oeuvre par ordinateur'' et les m\'{e}thodes pour l'exercice d'activit\'{e}s \'{e}conomiques seraient des inventions au sens du droit des brevets et la publication d'une description fonctionnelle d'une id\'{e}e brevet\'{e}e constituerait une infraction au brevet. Les protocoles et les formats de donn\'{e}es pourraient \^{e}tre brevet\'{e}s et ne seraient alors plus librement utilisables m\^{e}me dans un objectif d'interop\'{e}rabilit\'{e}. Ces cons\'{e}quences peuvent ne pas sauter aux yeux d'un lecteur non concern\'{e}. Nous tentons ici de d\'{e}ch  iffrer le language opaque de la proposition et d'expliciter ses cons\'{e}quences.
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Directive Articles 1-6: Parliament's vs Council's Version\footnote{http://swpat.ffii.org/papers/europarl0309/cons0401/tab/constab0401.en.html}}}

\begin{quote}
Tabular Comparison of the core part of the EU directive ``on the patentability of computer-implemented inventions'', so far with encoded numbering only and without comments.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003-09-24: Amended Software Patent Directive\footnote{http://swpat.ffii.org/papers/europarl0309/europarl0309.en.html}}}

\begin{quote}
Consolidated version of the amended directive ``on the patentability of computer-implemented inventions'' for which the European Parliament voted on 2003-09-24.
\end{quote}
\filbreak

\item
{\bf {\bf CEC 2003/11: Secret Nitpicking on European Parliament's Amendments\footnote{http://swpat.ffii.org/papers/europarl0309/cec0311/cec0311.en.html}}}

\begin{quote}
The Industrial Property Unit of the Commission of the European Communities (CEC) had stated in October 2003 that it finds the European Parliament's Amendments to its software patent directive proposal mostly inacceptable.  In a confidential document distributed to EU member state governments in November 2003, the Commission's patent officials added some critical notes about each of the amendments of the European Parliament.  The Commision points out that the text deviates from the practise of the European Patent Office in its use of the terminology and in its reasoning.  This is enough for the Commission to find the Parliament's text inacceptable.  Rather than examine the the merits of the Parliament's versus the EPO's approach, the Commission treats the EPO's approach as the absolute authority that must be followed and tries to find fault in the Parliament's legal logic, mostly by misunderstanding this logic or claiming that it is unclear or that it is at odds with some established practise.  Some of these claims are provably untrue.  The Commission's own proposal has been heavily criticised by prominent patent law experts for its incoherence and lack of clarity.
\end{quote}
\filbreak

\item
{\bf {\bf Council of the European Union and Software Patents\footnote{http://swpat.ffii.org/players/consilium/swpatconsilium.en.html}}}

\begin{quote}
Together with the European Commission and the European Parliament, the Council is one of the three pillars of the European Union, which jointly legislate in a \emph{co-decision procedure}.  It is a forum where the national governments and their specialised ministries meet.  The question of how to limit patentability is handled in the ``Council Working Party on Intellectual Property and Patents''.  This council has been holding increasingly frequent meetings to discuss the European Commission's proposal for a software patentability directive and come up with a counter-proposal.  The national delegations are mostly composed of national patent office representatives or people whose career path is confined to the national patent establishment and who are factually dependent on this establishment in many ways.  Some delegations, such as the french and belgians, have comprised independent delegates and been fairly critical of the CEC proposal.  Others have been even more pro-patent than the CEC.  All have focussed on textual questions and caselaw rather than on what kind of output they want from the legislation in terms of patents granted/rejected and economic policy objectives.
\end{quote}
\filbreak

\item
{\bf {\bf CEOs of big telcos sign letter against Europarl Amendments\footnote{http://swpat.ffii.org/news/03/telcos1107/telcos031107.en.html}}}

\begin{quote}
The chief executive officers of Alcatel, Ericsson, Nokia and Siemens have signed a letter to the European Commission and the European Council which complains about the European Parliament's amendments to the proposed software patent directive, saying that these will effectively remove the value of most of the patents of their companies and thereby harm the competitiveness of Europe's industry and violate the TRIPs treaty.  FFII points out that the Directive indeed threatens the interests of the patent departments of such companies, but not of the companies themselves: The letter is characterised by untruthful dogmatic assertions which say much about the thinking of patent departments and little about the interests of their companies, many of whose employees, especially software developers, support the positions of FFII.
\end{quote}
\filbreak

\item
{\bf {\bf Kai Brandt 2003: Patent Protection in Europe in Danger\footnote{http://swpat.ffii.org/papers/siemens-brandt03/siemens-brandt03.en.html}}}

\begin{quote}
In an internal journal of Siemens, Dr. Kai Brandt, an independent patent attorney residing in Munich and member of the Siemens patent department, writes that the European Parliament voted to ban patenting of all innovative industrial processes that make use of software, that there is no R\&D without patents, that the European Commission's original proposal was well balanced, that the EP voted for amendments because it was misled to believe that patents and opensource software are incompatible, and that Siemens boss Heinrich von Pierer has teamed up with other CEOs and associations to prevent this disaster, which is only in the interest of a few software distributors and against the interests of all innovative SMEs.  Brandt fails to give his audience any usable pointers that could allow them to inform themselves about the other side's arguments.
\end{quote}
\filbreak

\item
{\bf {\bf Irish EU Presidency to ``protect software inventions'' in May\footnote{http://swpat.ffii.org/news/04/ieeu0109/ieeu040109.en.html}}}

\begin{quote}
The Irish vice prime minister has unveiled a brochure which describes the agenda of the Competitiveness Council of the Irish EU Presidency.  The brochure places high emphasis on the Community Patent and the IP Enforcement Directive and somewhat lower emphasis on the software patent directive, although it asserts that ``effective instruments'' for ``protection'' of ``software inventions'' form an ``important underpinning'' of the ``knowledge based economy''.  The IE Presidency will try to bring about an agreement on the software patent directive at the May meeting of the Competitiveness Council.
\end{quote}
\filbreak

\item
{\bf {\bf FFII UK: IPR Enforcement Directive\footnote{http://www.ffii.org.uk/ip\_enforce/ipred.html}}}

\begin{quote}
Oorspronkelijk bedoeld als een maatregel om georganiseerde criminele namaak en piraterij hard aan te pakken, is het bereik van het wetsvoorstel uitgebreid zodat het nu van toepassing is op alle mogelijke intellectuele eigendomsdisputen. FFII UK documenteert deze zaak, in het bijzonder vanuit het oogpunt van het UK, waar gelijkaardige wetgeving ontwikkeld is door rechtspraak gedurende de laatste 20 jaar. Niettemin is zelfs daar het ontevreden over de nieuwe EU richtlijn groot, zowel onder de burgers als bij grote en kleine bedrijven.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{Media contacts}
\begin{description}
\item[mail:]\ media at ffii org
\item[phone:]\ Hartmut Pilch +49-89-18979927
More Contacts to be supplied upon request
\end{description}
\end{sect}

\begin{sect}{ffii}{About the FFII -- www.ffii.org}
The Foundation for a Free Information Infrastructure (FFII) is a non-profit association registered in Munich, which is dedicated to the spread of data processing literacy.  FFII supports the development of public information goods based on copyright, free competition, open standards.  More than 300 members, 700 companies and 50,000 supporters have entrusted the FFII to act as their voice in public policy questions in the area of exclusion rights (intellectual property) in data processing.
\end{sect}

\begin{sect}{url}{Permanent URL of this Press Release}
http://swpat.ffii.org/news/04/cons0317/cons040317.en.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri04.el ;
% mode: latex ;
% End: ;

