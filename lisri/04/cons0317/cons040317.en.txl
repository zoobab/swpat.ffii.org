<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EU Council Patent Officials Push for Unlimited Patentability of Communication Conventions

#descr: The Council of Ministers of the European Union is currently conducting secret negotiations between national patent administrators about a controversial EU directive %(q:on the patentability of computer-implemented inventions), colloquially termed %(q:the software patent directive).  While the EU Parliament had voted in September 2003 to reconfirm the exclusion of software from patentability, the patent officials in the EU Council's patent working party have, along with the European Patent Office over which they also preside, been pushing to establish and legalise an US-style practise of patenting %(q:computer-implemented) business methods, algorithms and communication interfaces.  In particular the Council's patent policy working party has taken an extreme position, dubbed as %(q:compromise), in a working paper circulated internally on January 29 by the Irish Council Presidency.  During secret negotiations on March 2, this position was apparently confirmed and further reinforced by the patent officials.  A proposal by the Luxemburg delegation, which was leaked to the FFII, would at least allow the use of a patented technique for interoperation (e.g. allow the use of GIF images as long as Internet Explorer does not yet support patent-free and functionally equivalent/superior alternatives such as PNG and MNG).  The group rejected this proposal and added a recital wording which states that a right to interoperate with a patented standard can only be obtained by competition law on a case-by-case basis, i.e. only by costly and lengthy procedings with highly uncertain outcome.  Once more, the Council Working Group is presenting unprecedented extreme positions as %(q:compromises) and disguising the meaning of these positions behind a seemingly compromising rhetoric.  Once more, the Council Working Group is doing this through secret backroom negotiations, refusing to reveal which governments are responsible for the positions taken and which interests were taken into account.  The FFII is meanwhile organising demonstrations under the slogan %(q:No Software Patents -- Power to the Parliament).  The first such demonstration, modelled on successful demonstrations of last August, is to take place in Brussels and on the Net on April 14th.

#qii: The %(q:Council Presidency Compromise Paper)

#uWt: Luxemburg Proposal on Interoperability Rejected

#Slf: Net Strike, Rallies, Conferences

#media: Media contacts

#ffii: About the FFII

#dokurl: Permanent URL of this Press Release

#yes: Current European and international law (EEC software directive, European Patent Convention, TRIPs) states that computer programs are to be %(q:protected as literary works) by the rules of copyright and are %(q:not inventions in the sense of patent law).  The European Patent Office, the European Commission and the Council of the European Union have since 1997 been involved in a concerted effort to change these rules and adopt a regime similar to that of the USA, where software is subjected to both copyright and patents.  In 2002 the European Commission, backed by the member states through the EU Council of Ministers, proposed a EU directive %(q:on the patentability of computer implemented inventions) in order to codify this regime change.  However, under the impression of strong opposition from software developpers, software companies and academia, the European Parliament amended the proposal in september 2003 so as to reconfirm the non-patentability of software.  This decision of the European Parliament is meeting fierce opposition from ministerial patent administrators in national governments and at the European Commission.

#Wom: The EU Council's %(q:Working Party on %(tp|Intellectual Property|Patents)) %(cm:met) on March 2nd to discuss the %(ep:European Parliament's amendments to the software patent directive).

#rWt: The discussion was based on a confidential %(wd:working document) titled %(q:Council Presidency Compromise Paper). This proposal removes everything from the Parliament's amendments that limits patentability, including:

#WWb: All definitions of %(q:technical), %(q:invention) and %(q:industrial) are removed, and yet patentability is limited by nothing but these terms.

#maW: Freedom of publication is removed from Art 5, instead information goods become directly claimable, so that internet service providers can be sued for allowing publication of independently developped programs on their server.

#een: Art 6a (freedom of interoperation) is removed.  This way Microsoft receives green light for its new licensing program on file formats, and large software and telecommunication companies can charge fees for the use of the next generation of Internet standards.

#tri: A few cosmetic amendments by EPO-friendly MEPs such as Arlene McCarthy and Joachim Wuermeling are admitted into the draft.  Among these are amendments which were rejected by the European Parliament in the plenary vote.

#sWt: When file formats can be owned, it becomes impossible for competitors to convert from it to another file format.  This reinforces monopolistic tendencies in the software market. Standardisation Consortia such as the W3C (responsible for the world wide web standards) are regularly slowed down by attempts of patent owners to charge fees on the use of a standard.  Patent fees impose heavy burdens on the managment of the standard itself and of the compliant software.  Free software and shareware can usually not comply.  Due to the high transaction costs, formalised standards become less competitive in relation to de-facto standards, where one vendor alone decides.  %(ms:Microsoft) is already pushing patented protocols into the market under licensing conditions which exclude free software.  Even the European Commission's %(cm:recent competition procedings against Microsoft) have failed to change this.  Anti-trust law alone, without further provisions such as Art 6a, is out of the reach of normal market participants and provides insufficient remedies against the anti-competitive effects of proprietary communication standards.

#tWW: In its version of March 17, the paper goes yet one step further.  A newly worded recital 17 indicates that the right of interoperation shall not be regulated within patent law but only within antitrust law:

#Wde: Hereby the working party creates the impression that it is taking the needs for freedom of interoperation into account.   Upon closer reading it turns out however that the exact opposite is achieved: without a costly, lengthy, and rarely successful legal procedure software creators shall, according to the will of the Council patent working party, be not be allowed to interoperate.

#uaW: By this hardening of its position the Council Working Group responds to the following proposal from the Luxemburg Delegation:

#Wnb: Delegations will find below a proposal by the Luxembourg delegation for an interoperability clause (new article 6a) to be included in the text of the proposed Directive. The proposed text is based on amendment 76 of the European Parliament, but is more restrictive in that it takes the example in the original amendment and uses it as the only acceptable exception for the purpose of ensuring interoperability. This stricter wording should make the provision fully compatible with Article 30 TRIPS.

#EWe: This Proposal corresponds to the article in the form in which it was approved by the parliamentary committees CULT, ITRE and JURI.  It avoids the term %(q:significant purpose), which had been newly introduced in the plenary vote.  Thereby the Luxemburg delegation fully satisfies the %(kk:criticism of the Commission) and the requirement of the three-step test (limited limitation) according to Art 30 TRIPs.  By refusing the Luxemburg proposal and with it the position of all three concerned committees of the European Parliament, the patent working party shows that Art 30 TRIPs is not its primary concern.  Rather, it must be assumed that rent-seeking interests associated with the dominance of communication standards, as recently advocated in the name of the CEOs of %(te:five large European telecommunication companies), are the driving forces behind the Council patent working party's intransigence.

#sor: The FFII is meanwhile mobilising its 50,000 supporters and 300,000 petition signatories to demonstrate both on the Internet and in Brussels on April 14th.  The website demo.ffii.org states:

#ilh: In February 2002, the European Commission proposed a directive that would legalise software patents. However, the European Parliament decided in its Plenary Vote of 24th September 2003 to fix all the loopholes in this proposal and explicitly banned software patents.

#WWt: Currently, the European Council of Ministers is discussing this directive. Their internal working party proposes to simply discard all clarifying amendments from the Parliament. They want to make everything patentable.

#ith: That is not an option Europe is willing to accept. We showed them this on 27 August 2003. We will show them again on 14 April 2004.

#ebp: Close your website in protest

#snW: The site provides numerous sample strike pages and banners which can be used by webmasters to support the action.

#IAn: The demonstration begins at 11.30 at Luxemburg Square in Brussels.  Participants will wear t-shirts with the slogans %(q:No Software Patents -- Power to the Parliament).  There will be speeches and performances.  The marchers will thank the European Parliament and protest against the lack of accountability in the Commission, in the Council, and sectoral bodies such as UNICE and EICTA, which they accuse of misrepresenting the industry by leaving policy decisions to specialist patent lawyer committees, similar to the patent working party of the EU Council.

#WtW: The demonstration is preceded by a press conference in the European Parliament, room AG2, at 10.00.

#ier: The demonstration is followed by an interdisciplinary conference in the European Parliament, again room AG2, at 14.00.  Among the participants of the intensely prepared discussion agenda are members of the European Parliament, officials from the European Commission and the Council Working Party, software developpers, economists, lawyers from both sides of the divide.

#usn: Program of Brussels Events

#oWW: More Contacts to be supplied upon request

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri04.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040317 ;
# txtlang: en ;
# multlin: t ;
# End: ;

