<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Fonctionnaires en Brevet du Conseil de l'UE Poussent Pour Brevetabilité Illimitée des Conventions de Communication

#descr: Le Conseil des ministres de l'Union européenne est en train de conduire des négociations secrètes avec les administrateurs des offices de brevets nationaux à propos d'une directive controversée de l'UE %(q:sur les inventions mises en oeuvre par ordinateur), familièrement désignée comme %(q:la directive sur les brevets logiciels). Alors que le Parlement européen a voté en septembre 2003 pour une réaffirmation de l'exclusion des logiciels de la brevetabilité, le Conseil de l'UE, avec l'Office européen des brevets, dont le Conseil d'administration est présidé par des membres du Groupe de travail sur les brevets du Conseil de l'UE, a fait pression pour établir et légaliser des pratiques semblables à celles des États-Unis pour le brevetage des algorithmes mis en oeuvre par ordinateur et des méthodes d'affaire. Le Groupe de travail sur la politique des brevets du Conseil a pris en particulier une position extrêmiste, baptisée %(q:compromis), dans un doc  ument de travail diffusé en interne le 29 janvier par la Présidence irlandaise du Conseil. Lors de négociations secrètes le 2 amrs, cette position a été apparemment confirmée et même accentuée par les membres du Conseil. Une proposition de la délégation luxembourgeoise d'autoriser l'utilisation d'une technique brevetée à des fins d'interopérabilité (par ex. pour permettre l'utilisation d'images GIF tant qu'Internet Explorer ne supporte toujours pas d'alternatives libres de brevets et fonctionnelement supérieures comme PNG ou MNG)) a été reje té par le Groupe de travail. Plus encore, le Groupe a formulé un considérant additionnel statuant qu'un droit à l'interopérabilité sur un standard breveté ne peut être accordé qu'après une procédure anti-concurentielle longue et coûteuse, au cas par cas, comme la procédure de la Commission européenne contre Microsoft cette année, qui a en fait abouti à une dénégation d'un tel droit. Une fois de plus, l  e Groupe de travail du Conseil intensifie sa position extrêmiste, soit-disant de %(q:compromis) et l'enrobe dans des formulations décevantes. Une fois de plus, le Groupe de travail du Conseil accompli ceci dans des négociations en coulisses, en refusant de révéler quels gouvernements sont responsables des positions prises et comment ont été pris en compte les différents intérêts. Le document du Luxembourg n'a pa été publié par le Groupe de travail du Conseil.

#qii: Le %(q:document de compromis de la présidence du Conseil)

#uWt: Luxemburg Proposal on Interoperability Rejected

#Slf: Net Strike, Rallies, Conferences

#media: Contacts media

#ffii: À propos de la FFII

#dokurl: URL permanente de ce communiqué de presse

#yes: Le droit actuel européen et international (directive européenne sur les logiciels, Convention sur le brevet européen, ADPIC) stipule que les programmes d'ordinateur doivent être %(q:protégés comme les oeuvres littéraires) par les règles du droit d'auteur et %(q:ne sont pas des inventions au sens du droit des brevets). L'Office européen des brevets, la Commission européenne et le Conseil de l'Union européenne se sont investis dans un travail concerté pour changer ces lois et adopter un régime similaire à celui des USA, où les logiciels sont susceptible d'être protégés à la fois par des droits d'auteur et par des brevets. En 2002, La Commission européenne, soutenue par les États membres du Conseil des ministre de l'UE, a proposé une directive %(q:sur la brevetabilité des inventions mises en oeuvre par ordinateur) afin de codifier ce changement de régime. Cependant, sous une forte pression de la parts de développeurs de logiciels, d'entreprises informatiques et de scientifiques, le Parlement européen a amendé cette proposition en septembre 2003, faisant en sorte de réaffirmer la non brevetabilité des logiciels. Cette décision du Parlement européen rencontre une violente opposition des administrateurs de brevets dans les gouvernements nationaux et de la Commission européenne.

#Wom: Le %(q:Groupe de travail sur %(tp|la Propriété intellectuelle|les brevets)) du Conseil de l'UE %(cm:s'est réuni) le 2 mars pour discuter des %(ep:amendements du Parlement européen à la directive sur les brevets logiciels).

#rWt: La discussion a porté sur un %(wd:document de travail) confidentiel, intitulé %(q:Document de compromis de la Présidence du Conseil). Cette proposition supprime tout ce qui dans les amendements du Parlement limite la brevetabilité, y compris :

#WWb: Toutes les définitions de %(q:technique), %(q:invention) and %(q:industriel) sont supprimées alors que la brevetabilité n'est toujours limitée par rien d'autre que ces termes.

#maW: La liberté de publication est supprimée de l'article 5 ; à la place, les biens informationnels deviennent directement revendicables, les fournisseurs d'accès à internet peuvent ainsi être poursuivis pour avoir permis la publication sur leur serveur de programmes développés de manière indépendante.

#een: L'article 6 bis (liberté d'interopération) est supprimé. Avec ceci, Microsoft obtient le feu vert pour sa nouvelle politique de license sur les formats de fichier et les grosses entreprises informatiques et de télécommunicatons peuvent faire payer pour l'utilisation de la prochaine génération de standards Internet.

#tri: Quelques amendements cosmétiques de députés pro-OEB, tels qu'Arlene McCarthy et Joachim Wuermeling, sont repris dans le document de travail. Parmis ces amendements, certains avaient été rejetés par le Parlement européen lors du vote en plénière.

#sWt: Lorsqu'un format de fichier peut être couvert par des brevets, il devient impossible pour les concurrents de le convertir vers un autre format.  Ceci renforce les tendances monopolistiques sur le marché du logiciel.   Les Consortiums de standardisation comme le W3C (responsable des standards du world wide web) sont régulièrement ralentis par des tentatives des propriétaires de brevets d'imposer des taxes de brevet à un standard.  Les standards brevetés et taxés imposent de lourdes charges sur la gestion du standard lui-même et sur les logiciels s'y conformant.  Le logiciel libre ne peut en général s'y  plier. Lorsque qu'un éditeur domine seul un standard avec des brevets, les problèmes empirent.  %(ms:Microsoft) pousse d'ores et déjà les brevets sur le marché avec des conditions de licence qui excluent le logiciel libre.  Même les procédures concurrentielles en cours de la Commission europénenne contre Microsoft ont jusqu'ici échoué à changer ceci.  Le dr  oit de la concurrence seul, sans dispositions supplémentaires telles que celle de l'article 6a, est hors de portée des acteurs classiques du marché et offre des remèdes insufisants contre les pratiques anti-concurrentiels avec lesquelles la domination des standards de communication tend à être associée.

#tWW: Dans sa mise à jour du 17 mars, le document va encore plus loin. Une nouvelle reformulation du considérant 17 indique que le droit à l'interopérabilité ne doit pas être régulé au sein du droit des brevets mais uniquement dans le droit anti-concurrentiel.

#Wde: En agissant ainsi, le Groupe de travail donne l'inpression qu'il prend en copte le droit à l'interopérabilité. En y regardant de plus près, il s'avère cependant qu'on aboutit à l'effet exactement inverse : à moins de se se lancer dans des procédures juridiques coûteuses, longues et rarement couronnées de succès, les créateurs de logiciels ne doivent pas, d'après le Groupe de travail sur les brevets du Conseil, être autorisés à interopérer.

#uaW: Par ce durcissement de sa position, le Groupe de travail du Conseil répond à la proposition suivante de la délégation luxembourgeoise :

#Wnb: Les délegations trouveront ci-dessous une proposition de la délégation luxembourgeoise sur une clause d'interopérabilité (nouvel article 6 bis) à insérer dans le texte de la proposition de Directive. Le texte proposé se fonde sur l'amendement 76 du Parlement européen mais est plus restrictif en cela qu'il prend l'exemple de l'amendement originale et l'emploie comme étant la seule exception acceptable pour garantir l'inetropérabilité. Cette reformulation plus stricte devrait rendre la disposition complètement en phase avec l'article 30 des ADPIC.

#EWe: Cette proposition correspond à l'article sous la forme dans laquelle il a été aprouvé par les commissions parlementaires CULT, ITRE et JURI. Il évite le terme de %(q:une fin significative), qyui avait été introduit pour la première fois lros du vote en plénière. Ainsi, la délégation luxembourgeoise répond complétement aux %(kk:critiques de la Commission) et à l'exigence du test en trois étapes (exceptions limitées) de l'article 30 des ADPIC. En refusant la proposition luxembourgeoise et avec, celle des trois commissions parlementaires, le Groupe de travail sur les brevets démontre que l'article 30 des ADPIC n'est pas sa préoccupation première. On doit plutôt penser que ce qui guide l'intransigeance du Groupe de travail sur les brevets du Conseil, ce sont davantage les intérêts à la recherche de profit associés aux standards de communication, comme cela a été récemment défendu au nom de PDG de %(te:cinq grosses entreprises européennes de télécommunication).

#sor: The FFII is meanwhile mobilising its 50,000 supporters and 300,000 petition signatories to demonstrate both on the Internet and in Brussels on April 14th.  The website demo.ffii.org states:

#ilh: In February 2002, the European Commission proposed a directive that would legalise software patents. However, the European Parliament decided in its Plenary Vote of 24th September 2003 to fix all the loopholes in this proposal and explicitly banned software patents.

#WWt: Currently, the European Council of Ministers is discussing this directive. Their internal working party proposes to simply discard all clarifying amendments from the Parliament. They want to make everything patentable.

#ith: That is not an option Europe is willing to accept. We showed them this on 27 August 2003. We will show them again on 14 April 2004.

#ebp: Close your website in protest

#snW: The site provides numerous sample strike pages and banners which can be used by webmasters to support the action.

#IAn: The demonstration begins at 11.30 at Luxemburg Square in Brussels.  Participants will wear t-shirts with the slogans %(q:No Software Patents -- Power to the Parliament).  There will be speeches and performances.  The marchers will thank the European Parliament and protest against the lack of accountability in the Commission, in the Council, and sectoral bodies such as UNICE and EICTA, which they accuse of misrepresenting the industry by leaving policy decisions to specialist patent lawyer committees, similar to the patent working party of the EU Council.

#WtW: The demonstration is preceded by a press conference in the European Parliament, room AG2, at 10.00.

#ier: The demonstration is followed by an interdisciplinary conference in the European Parliament, again room AG2, at 14.00.  Among the participants of the intensely prepared discussion agenda are members of the European Parliament, officials from the European Commission and the Council Working Party, software developpers, economists, lawyers from both sides of the divide.

#usn: Program of Brussels Events

#oWW: Plus de contacts peuvent être fournis sur simple demande

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri04.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: cons040317 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

