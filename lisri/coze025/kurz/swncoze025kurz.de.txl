<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#MrW: Microsoft besitzt zahlreiche Softwarepatente und %(ms:setzt sie neuerdings auch aggressiv ein).  Microsoft dominiert den amerikanischen Verband, aus dessen Feder der EU-Richtlinienentwurf stammt.  Dennoch ist Microsoft nicht mehr als ein Trittbrettfahrer der Patentinflation.  Die eigentliche treibende Kraft sind Patentexperten, die versuchen, in der Softwarebranche alte Glaubenskriege um das %(q:Geistige Eigentum) oder um den Status der Informatik als %(q:Ingenieurwissenschaft) anzufachen.  Die GI ließ sich in diesem Sinne von einem Patentanwalt eine %(es:Stellungnahme) verfassen.  Ähnlich wie seinerzeit der %(e:real existierende Sozialismus) scheint heute das real existierende %(gk:Gruselkabinett der gesetzeswidrig erteilten Europäischen Softwarepatente) für einige Leute als Hoffnungsträger immer noch gut genug zu sein.

#Stq: Stellen Sie sich vor, Sie besitzen eine kleine Softwarefirma.  Sie haben eine starkes Softwareprodukt entwickelt.  Dieses Werk ist eine schöpferische Kombination von 1000 Rechenregeln (Algorithmen) und einer Menge Daten.  Pro Rechenregel investierten Sie ca 1 Glas Rotwein oder 1 Badewanne heißes Wasser.  Die Entwicklung des gesamten Werkes erforderte 20 Mannjahre.  900 der Rechenregeln waren vor 20 Jahren schon bekannt.  50 sind heute patentiert.  Sie besitzen 3 dieser Patente.  Um sich die 3 Rechenregeln zu schützen, sind Sie zum Patentamt geeilt, haben ihre Geschäftsstrategie offenbart und Anwaltskosten bezahlt.  IBM und Microsoft setzen derweil Ihre Ideen in klingende Münze um und weisen Sie darauf hin, dass Sie ca 20-30 von 50000 Patenten aus deren Portfolio verletzen.  Sie einigen sich gütlich:  3% Ihrer jährlichen Einnahmen gehen an IBM, 2% an Microsoft, 2% %(dots).  Dennoch erreichen Sie bald die Gewinnzone.  Jetzt sind Sie eine attraktive Firma.  Eine Patentagentur wendet sich an Sie.  Sie verletzen 2-3 Patente, heißt es, und sollen 100.000 EUR berappen.  Eine gerichtliche Klärung könnte 10 Jahre dauern und 1 Million EUR kosten.  Sie zahlen.  Einen Monat später steht die nächste Patentagentur auf der Matte %(dots) Bald sind Sie pleite.  Sie suchen Schutz.  Microsoft bietet an, Sie für einen symbolischen Preis zu kaufen.  Sie akzeptieren.  Unter einem reinen Urheberrechtssystem wären Sie jetzt unabhängig und reich.  Mithilfe von Patenten ist es Microsoft und anderen gelungen, Ihr geistiges Eigentum zu stehlen.

#Nnc: Neue Wirkungszusammenhänge von Naturkräften zu entdecken und anzuwenden erfordert im allgemeinen viel größeren Aufwand als neue Rechenregeln zu entwickeln.  Das Verhältnis von Erfindungs- und Nachahmungsaufwand ist bei Algorithmen umgekehrt wie etwa bei Medikamenten.  Der Neumannsche Universalrechner war eine Erfindung zur Erleichterung des Erfindens.  Dank ihr entwickeln wir heute Algorithmengewebe von ehemals undenkbarer Komplexität.  Solchen Leistungen wird das Urheberrecht mit seinem flexiblen Begriff der %(q:individuellen Schöpfung) besser als das Patentwesen gerecht.

#Dse: Der Erfolg des Patentwesens ruhte noch auf einer weiteren Säule:  einem strengen Erfindungsbegriff.  Unter einer %(e:Erfindung) verstand man eine %(e:Einheit aus Entdeckung und Anwendung eines Wirkungszusammenhanges von Naturkräften), in den Worten des Bundesgerichtshofes (BGH) eine %(q:Lehre zum planmäßigen Handeln unter Einsatz beherrschbarer Naturkräfte zur unmittelbaren Herbeiführung eines kausal übersehbaren Erfolges).  Der Einsatz von Naturkräften musste laut BGH %(dp:zum Auffinden der Problemlösung notwendig) sein.  Dies schloss %(e:Organisations- und Rechenregeln) (Algorithmen, Programmierlösungen) auch dann von der Patentierbarkeit aus, wenn sie zur %(wt:Walzstabteilung) oder %(fm:Motorsteuerung) dienten.

#SEr: Schon um 1877, als in Deutschland das Patentsystem eingeführt wurde, wurde dies weithin als ein %(q:Sieg der Juristen und Protektionisten gegen die Volkswirte) wahrgenommen.  Die Nationalökonomen hatten vorgerechnet, dass das Patentwesen den technischen Fortschritt bremsen würde, und an diesem Urteil hat sich seitdem nicht viel geändert.  Demnach sollten Unternehmer ihre Investitionen lieber über weichere Mittel schützen.  Wettbewerbsrecht, Betriebsgeheimnis, Kompetenzvorsprung etc lassen jedoch oft Wünsche offen.  So konnte das Patentwesen zum Hoffnungsträger einer Bewegung werden, die 1877 ihren ersten Sieg feierte.   Manche der Hoffnungen gingen wohl in Erfüllung.  Der eigentliche Erfolg des Patentwesens lag jedoch auf politischer Ebene:  das System erzeugte sofort einen selbstverstärkenden Mechanismus:  Patentinhaber, Patentjuristen, Patentverwerter und andere interessierte Sachverständige nahmen die Gesetzgebung fest in ihre Hand.  Auf der Gegenseite stand nur eine weit gestreute Masse von Geschädigten, die sich des Schadens meist kaum bewusst waren.

#1en: 115000 Personen, darunter ca 800 Vorstände von Softwarefirmen und Tausende von Mitarbeitern großer Firmen wie IBM, Siemens, Oracle, SAP etc haben eine Petition zur Verhinderung von Softwarepatenten in Europa %(ep:unterzeichnet).  Praktisch alle angesehenen Programmierer und Wirtschaftswissenschaftler sind sich einig, dass Softwarepatente eine schlechte Idee sind.  Dennoch %(es:drängen) die federführenden Patentjuristen der Europäischen Kommission darauf, in Europa Softwarepatente amerikanischen Stils ohne jede wirksame Begrenzung zu legalisieren.

#descr: Artikel für die ComputerZeitung

#title: Software-Engineering-Utopie und real existierende Patentinflation

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swncoze025kurz ;
# txtlang: de ;
# multlin: t ;
# End: ;

