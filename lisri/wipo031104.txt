
WIPO has just published a 377 page brochure, "Intellectual Property - A 
Power Tool for Economic Growth", aimed at policy-makers in businesses 
and governments worldwide, and as the preface puts it, "written from a 
definite perspective -- that IP is good".

http://www.wipo.org/about-wipo/en/dgo/wipo_pub_888/index_wipo_pub_888.html

Chapter 4 on Patents has some useful material for sharpening one's teeth 
before talking to policy makers.  As well as setting out a general 
pro-patents case, it goes on to discuss why it is good public policy for 
universities to be forced to patent their research (Bayh-Dole act), why 
technology transfer agencies should be privatised, and then goes on to 
consider software patentability:



Computer Technology in its Infancy Needed Patent Protection
[page 107 of the book, page 16 of the chapter pdf]

...

The focus of research efforts by the computer industry shifted towards 
software-related technology in the late 1980s as the performance of 
processors increased. Initially, software was construed as a creation 
outside patent protection. After worldwide discussions on the 
implication of patent rights on functional and indivisible designs, this 
view began to change. The economic momentum and significance of the 
software business, as a driver for the computer industry, and therefore 
for many related industries, became evident. Today, with the advent of 
the Internet, another test for the patent system is the rise of business 
method inventions. The race between the development of computer 
technology, and policy-makers concerned with patent law and policy will 
be discussed in detail below.


Computer Software Blossomed under IP Protection

In the 1980s, there were extensive discussions on whether the patent 
system or the copyright system, or both, should provide protection for 
computer software. It had become clear that computer software, in order 
to develop and thrive, needed strong intellectual property protection. 
These discussions resulted in the generally accepted principle that 
computer software should be protected by copyright, whereas apparatuses 
using computer software or software-related inventions should be 
protected by patents. This legal development culminated in international
agreements (the TRIPs Agreement and the WCT) obligating signatories to 
legally protect software. The law relating to the patentability of 
software is still not harmonized internationally, as some countries 
embraced the patentability of computer software and others adopted 
approaches that recognize inventions assisted by computer software. 
Today, in many countries computer software and hardware may be protected 
by both copyright and patent law, depending on the nature of the IP at 
issue.

There was also a growing need at that time for the protection of the 
physical layout design of an integrated circuit, which is why a 
diplomatic conference on that subject was organized by WIPO in 1989. 
Given that patent protection is stronger than the protection afforded by 
other types of IP, including copyright protection, how different IP laws 
were used to protect various types of computer technology was an 
important policy decision. Along with the desire to protect new computer 
technologies, there was also the widely-shared concern that if the 
protection was too strong, it could discourage further developments of 
intellectual creation.

During the debate on patent and copyright protection, the 
competitiveness of the computer industry created a political climate 
more favorable to protecting certain software-related inventions by 
patents, rather than by copyright alone. Policy-makers were well aware 
that it was strategically advantageous to have patent protection for 
computer-related inventions in order to encourage the transfer of this 
emerging technology from countries advanced in this area. It was also 
important to protect computer-related inventions in order to stimulate 
investment within their own national industry.

The number of companies developing software continues to grow. Smaller 
software companies are filing for patents more often, particularly as 
venture capitalists frequently require patents as a condition for 
providing funding. Companies that have previously filed patent 
applications for software-related innovations have also stepped up their 
efforts to patent these innovations, including, in particular, companies 
with strong software portfolios.33 Also, software increasingly underlies 
other technologies, such as the software that today is used to design, 
manufacture, and render operable the hardware in a microprocessor (e.g. 
"verilog"; a hardware description language). This software also is 
protected by both copyright and patent law in many cases.

Today, the extent to which software-related inventions are patentable 
varies considerably from one country to another. There appears to be an 
acute need for international harmonization, particularly as 
international trade in software products continues to increase and 
companies competing on a global basis are interested in protecting their 
IP assets uniformly around the world. Also, the consequences of one 
country offering patent protection while others do not are that some 
nations will have a more sheltered environment for growth in that 
particular industry, and enterprises there will accumulate more patents 
("patent assets"), while other nations and their enterprises will not 
build up patent assets in these areas. The practical significance of 
patent assets will be addressed later in this Chapter when we explore 
licensing as an important element in the strategic use of patents to 
stimulate development.


Business Methods and IP Protection Correspond to Internet Business Growth

Another area where business growth has stimulated the expansion of IP 
protection, and where IP protection has in turn stimulated business 
growth, is the fast-growing field of electronic commerce on the 
Internet. Patents have recently been granted for so-called 
business-method inventions in this area. As most modern business models 
involve the application of computer software, the patentability of 
business methods in the United States is closely linked to recent 
developments in the law on the patentability of software and 
software-related inventions. The decision of the Court of Appeals of the 
Federal Circuit in 1998 in State Street Bank & Trust v. Signature 
Financial Group holding that business methods could be the subject 
matter of patents, triggered a sharp increase in such applications.34 
This seminal decision challenged traditional views of the "technical 
nature" of patents common in certain countries.

Since State Street Bank, patents have been issued on methods for online 
decision analysis, on-line financial systems, on-line customer rewards 
systems, and even systems for categorizing and valuing patents. 
Suddenly, the potential of patent protection seemed much broader and 
more accessible to a wider range of new ideas than had previously been 
contemplated. In fact, the growing quantity and complexity of business 
method applications reflect the increasing importance of business 
technologies in today's economy.

Business-method inventions which are applicable to e-commerce consist 
mainly of software-based systems and methods which are used to effect or 
simplify electronic transactions taking place over the Internet. These 
inventions enable the transaction to be effected by the computer system 
without requiring the relevant parties to be present in close proximity 
during the transaction. The majority of these patents have been issued 
in the United States of America. Patents for software-related business 
methods, such as those mentioned above, soared from 700 in 1996 to 2,600 
in 1999. Some attracted significant public attention.35 There is a 
raging debate on the long-term social and economic effects of 
Internetrelated patents and the related issues of patentability, prior 
art, and broad patents.36 Recently, several high-profile patent 
infringement suits involving Internet patents have added to the growing 
discussion.

For example, Amazon.com sued Barnesandnoble.com, claiming that the 
latter's ordering method infringed Amazon's "one-click" patent. Amazon 
won a preliminary injunction prohibiting its competitor from using 
one-click shopping methods. A lawsuit brought by Priceline.com against 
Microsoft claimed that the latter's Expedia.com travel site infringed 
Priceline's patent on "reverse-auction." Under a settlement Expedia will 
pay royalties to Priceline. Much appears to be at stake as litigation 
involving patent infringement suits continue to be filed in relation to 
Internet business methods.

In the evolving knowledge-based economy, innovative ideas are often a 
company's most valuable source of competitive advantage. This is 
especially so for companies engaged in online business, with limited 
tangible assets and whose success is mainly dependent on innovative 
ideas and other intangibles which can include business models. These 
patents are expected to increase in tandem with the continued expansion 
of the Internet and electronic commerce.37 According to the USPTO, prior 
to 1990, business-method patents were heavily focused on computerized 
postage metering and cash register systems. However, by 1994 these were 
overshadowed by financial transaction systems, and by 1999, electronic 
shopping and financial transaction systems became the two dominant 
categories. Newly filed applications indicate that advertising 
management systems will join the ranks of the most popular categories in 
this patent class. In 2000, 899 business-method patents were granted by 
the USPTO.

Some countries have moved to protect business-method inventions. For 
example, business-method inventions are now patentable in the Republic 
of Korea, following the introduction of Examination Guidelines for 
Business Methods on August 1, 2000. According to the Korean Intellectual 
Property Office (KIPO), it has already received 4,000 local applications 
for such patents, most of which are related to the Internet by 
clarifying or modifying their examination standards. Other countries are 
still holding discussions centered on the patentability of business 
methods which are applicable to the Internet and e-commerce. For 
example, the European Commission held a series of consultations in 2000 
on whether and how business-method inventions should be protected by 
patent within the European Community and recently proposed the new 
policy for adoption by the European Union member states.38 As discussed 
above, in the 1980s, the demarcation line was made between copyright 
protection of computer software, and patent protection of 
software-related inventions. The Internet has virtually erased that 
borderline, as many business methods are deemed more than mere computer 
software programs.

As in all other areas of technology, business-method patents are 
important to create incentives and encourage investment in new 
technologies (see Box - 4.9). However, some are concerned that the 
granting of overly broad patents, particularly those involving 
fundamental online business models, could stifle innovation and have a 
detrimental effect on the growth of ecommerce, particularly if these 
patents are abused.39 Some have also expressed doubts as to whether some 
of these online business models fulfill the basic requirements for the 
granting of a patent; they contend that existing business models are 
being reinvented for use on the Internet.

Still others point out the intersection of business method patents and 
the digital divide: with parts of the world patenting business methods, 
while other parts are still catching up with the most basic computer 
hardware technology. The consequences of this aspect of the digital 
divide are that ownership of business methods in the global marketplace 
will be secured by businesses in the nations that have aggressively 
supported such patents, whereas businesses from countries that are not 
so enamoured of business methods, or that have not yet addressed the 
issue, will be left behind. This gap is not as significant when markets 
are merely national, but in the international e-commerce marketplace of 
the Internet, businesses not holding IP assets in this area will be at 
risk of infringement. If policy-makers are not concerned about the lack 
of skilled human resources to develop the e-commerce industry, software 
programmers may wish to seek more attractive job opportunities in 
foreign countries where technological developments in this area are more 
dynamic than others for various reasons, including stronger protection 
of intellectual creation. This may lead to a "brain drain" which in turn 
exacerbates the digital divide. As the coverage and impact of 
businessmethod patents are global, the need for international 
cooperation to find the best solution is acute.


Box-4.9 The Idea Factory

The Walker Digital Corporation was founded in 1994 by Internet 
entrepreneur Jay Walker. The company develops and patents innovative 
information-based solutions for businesses and is modeled after Thomas 
Edison's famed Menlo Park invention factory. Walker Digital has at least 
12 patents on business methods. Mr. Walker, chairman of Walker Digital 
and founder and vice chairman of Priceline, said that "[w]ith recent US 
Patent and Trademark Office and court affirmations regarding the 
patentability of business methods, a company now has the ability to 
protect not only its business products but the actual methods employed 
in bringing them to market and satisfying customers. This important new 
recognition of patent protection creates the incentive for exciting and 
significant innovation in US businesses, and will play a central role in 
enabling a new generation of businesses to emerge and flourish, both on 
the Internet and off." Priceline.com is based, in part, on a patent 
issued to Walker Digital for an innovative software-enabled business 
process that allows interested buyers to communicate a binding offer to 
potential sellers, the so-called "nameyour-price reverse auction" 
business model. Walker Digital received 7.5 million Priceline shares in 
exchange for the assignment of patent rights and a US$500,000 
investment. "Patents are a critical element of priceline.com's business 
strategy as they strengthen and expand our competitive position," said 
Rick Braddock, Priceline's chairman and CEO. According to Mr. Braddock, 
"protected intellectual property enables us to establish and maintain 
our distinctive position in the ecommerce marketplace and gives the 
company the ability to focus on building its business and brand, and not 
be as concerned with the competitive copying going on in the e-commerce 
space. These patents also present priceline.com with the potential 
opportunity to open up new revenue streams through licensing."

Source: Forbes Magazine, Walker Digital Corporation, and Priceline.com.



Footnotes:

33    For example, nearly one-third of the patents which are issued to 
IBM in the United States of America are said to be for software-related 
inventions.

34    In the United States of America, there was a surge in business 
method filings in 1998 and 1999. Although these applications represent 
only 1 percent of total patent applications filed at the USPTO in 1999, 
they have increased significantly, from 700 in 1996 to 2,100 in 1999.

35    These include Priceline.com's "name your own price for..." system 
of purchasing goods through reverse auctions (US Patent Number 5897620); 
Amazon.com's "Method and System for Placing a Purchase Order Via a 
Communications Network" (US Patent Number 5960411), often referred to as 
the one-click ordering method, which allows users to make fast track 
orders; and Doubleclick's patent on a "System and Method for Delivering 
Customized Advertisements Within Interactive Communications Systems" (US 
Patent Number 5933811), which covers the manner in which the company 
delivers advertisements over the Internet.

36     The issuing of overly broad patents, caused by the unavailability 
of searchable prior art, is a concern that is often raised in relation 
to the patenting of new areas of technology, including, in particular, 
software, business methods, and biotechnology. On March 29, 2000, the 
USPTO announced a plan to improve the quality of the examination process 
in technologies related to e-commerce and business-methods. See USPTO, 
"Automated Financial or Management Data Processing Methods (Business 
Methods)," USPTO White Paper (Washington, D.C.: USPTO, 2000).

37     See, for example, USPTO, "Automated Financial or Management Data 
Processing Methods," http://www.hellopatent.com/us_bm/us_bm.html

38     See http://europa.eu.int/comm/internal_market

39     Some have argued that the current one-size-fits-all patent system 
should be revamped to create a more differentiated system, whereby 
distinctions are drawn between different types of knowledge and 
alternative types of patents awarded, in order to arrive at an optimal 
balance to encourage the production as well as the diffusion of new 
technologies. Other critics, especially in the legal profession, argue 
on the other side: that it is a bad idea to differentiate based on 
subject matter, and that the problems with business method patents stem 
from problems in the application of the current standards.
