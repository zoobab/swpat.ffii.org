<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#DhW: WIPO is planning several negotiation rounds for April and May 2001 which aim to make patents easier to obtain and to assert throughout the world and to hardcode the rights of patent holders into international law, so that no state may easily limit them.  Especially a draft for a %(q:Substantive Patent Law Treaty) looks like a bold step forward in making patent law unreformable for a long time to come.

#Wbd: WIPO to codify unlimited patentability and strict limitation of patent quality

#Dis: By means of a %(gf:Law on the Promotion of the Patent System in Institutions of Higher Learning) and a %(ga:Law for Changing the Law of Employees' Inventions), the right of university teachers to make their findings freely available to the public according to the ethics of Benjamin Franklin will be completely removed.  The university teacher will be obliged to inform his university's patent administration about anything that could be patentable, before he writes about it.  The patent administration, which is to be built by generous state funding in the next few years, will then dispose of all %(q:intellectual property).  The consent of the professors to this loss of freedom and dignity is to be bought by a share of 30% in the revenues.

#BWe: German Law to turn universities into patent factories

#2hh: The German Minister of Justice, Ms D�ubler-Gmelin, explained in a %(cz:newspaper interview of May 3rd) her %(q:vehement opposition) to plans of the Directorate of the Internal Market to codify the practise of the European Patent Office (EPO) and legalise software patents by means of a European directive.  The french minister of economics, Christian Pierret had already expressed strong opposition to software patents before, and British %(q:e-minister) Patricia Hewitt had expressed similar reserves.  The Dutch parliament had decided that the problem of triviality must be solved before a legalisation of software patents can be envisaged.  While the patent department of the German ministery of justice is avoiding comments and discussions, the ministery of economics has ordered two studies from patent-friendly sources.  In the %(lb:first), a patent lawyer explains why software patents are unavoidable and proposes some mitigations to reduce the danger to free/opensource software.   Another study, conducted by %(fh:institutes with deep affiliations with the patent movement), is to interview thousands of software companies.  In several European countries studies are under way which are based on rather shy questions and risk not to get through to the real issues.  We hope to improve this situation out by publishing a %(kf:list of consultation questions).

#Ses: EU Governments against codification of EPO practise -- studies and consultations to open freedom of action

#AWi: On 2001-06-06 in The Hague a new round of negotiations on the Hague Convention will commence.  This Convention allows and regulates the execution of foreign court decisions in certain areas.  According to a current revision draft available to the FFII, affairs of economic law and intellectual property law are to be integrated into the scope of the Hague Conventions.  FFII warns that this could have disastrous consequences and suggests that any information or expression crimes should stay out of the scope of the Hague Convention, just like questions of maritime law.

#Dxg: Dangers from Extension of Hague Convention

#Icr: In this arcane paper written by a patent lawyer, the GI demands unlimited patentability of all rules of organisation and calculation, including business methods, which are realised using a universal computer.  To support this demand, it uses legal arguments which were already explicitely rejected by lawcourts in the 70s.  Various lawyers, engineers and informaticians laugh about this paper.

#Dcn: The presidency of the Society for Informatics (GI) has again publicised its submission to the European Commission's consultation on the patentability of computer-implementable rules of organisation and calculation.

#Etd: Eine Arbeitsgruppe der CDU hat ein %(q:Strategiepapier) für die Gestaltung und Nutzung des Internet in Deutschland veröffentlicht.  Dieses Papier enthält eine kurze und widersprüchliche Passage zur Patent-Thematik.  Die Arbeitsgruppe will die Laufzeit von Logikpatenten auf 5 Jahre reduzieren und weitere %(kr:abfedernde Sonderregelungen) einführen, um die angeblich aufgrund %(q:internationaler Verpflichtungen) erforderliche Wahrung der %(q:Rechte der Urheber) zu %(q:ermöglichen).  Offenbar wurde hier einiges missverstanden.  Zudem scheint das Papier einem früheren Antrag der CDU/CSU-Fraktion zu widersprechen und auch in vielen anderen Punkten Ungereimtheiten aufzuweisen.  Aus CDU-Kreisen war zu hören, dass dieses Papier noch diskutiert werde und keine offizielle Meinung der Partei oder der Fraktion darstellen solle.

#CPn: CDU-Papier schlägt Patent-Sonderregelung für Software vor

#cid: %(BC), a leading journal of the ICT sector in Turkey, published an %(ih:interview on European devolpments concerning software patents with Eurolinux Alliance speaker Hartmut Pilch) and placed it in on the webserver front page for several days.  The interview incited some resonance, leading to a panel discussion at the %(lo:Ankara Linux and Opensource Conference) of 2001-12-07..8.

#Tne: Turkish software professionals alarmed over Patent Dangers

#IrW: On the other hand since 2000 several scholars of patent law have contradicted this view in journal articles, criticised the recent %(q:liberalising decisions) of the German Federal Court and demanded a return to the concept of %(ti:technical invention).  Simulatneously the %(q:stubbornly conservative) 17th senate of the Federal Patent Court has continued to %(bp:reject) software patents while allowing a complaint to the Federal Court.  A last such complaint was handled by the FC but adjourned indefinitly.

#Bia: BGH: programs not patentable but program claims may be acceptable

#Dtn: During a press conference on December 4th at the IST event 2001 in Düsseldorf, the European Commission's Information Society commissioner Mr. Erkki Liikanen informed the press that a proposal on patents for computer programmes from the European Commission, DG Internal Market was due very soon.  Mr. Liikanen said that there would not be patents on business methods but there would be patents on computer-implemented rules of organisation and calculation (programs for computers), provided that they had a %(te:technical effect).  This seems to imply (a) allowing claims to information objects such as %(q:computer program product), %(q:computer program) and %(q:data structure) (b) not demanding that the invention be a teaching of applied physical causality, not defining what is %(q:technical) (c) allowing the EPO to continue granting patents for computerised business methods.  It seems that the Comission intends to follow what the EC consultation report called the %(q:economic majority) (i.e. patent departments of corporations such as %{LST}) and go ahead legalising the americanised practise of the EPO without any meaningful restrictions and make it mandatory for all national jurisdictions.

#Lke: Liikanen: Commission to Propose Legalisation of Software Patents

#Dnv: %(SP), The French trade union for education, science and informatics etc, expressed its fears that software patents will change the rules of the software economy in an undesirable way, thereby destroying huge European growth potentials and further strengthening the predominance of the motherland of software patents.  Other informations' unions such as %(LST) made similar statements earlier.

#FWW: Frz Informatiker-Gewerkschaft: Softwarepatente machen Europa zur Vierten Welt der Informatik

#Dde: The European Commission has published a %(cl:deregulation whitebook) in which it finds that 80000 pages of EC regulation are too much and promises to reduce this body by 25% within a few years.  A German law professor suggests that the EC make it clear that the software patents granted by the EPO are illegal and will remain illegal.  This would immediately remove 100,000 pages of harmful European regulation.  If in patent matters the European Commission prefers red tape over the public interest, this may be seen as an indication of a general inability to deregulate.

#Ptu: Patentinflation konterkariert EU-Deregulierungs-Initiativen

#Dup: The politicians at the european summit in Laeken %(ld:confirmed their belief in the key role of the community patent for innovation and growth in Europe).  They want to start another attempt on Dec 20th in order to realise the CP this year.  The %(ek:counter-arguments of the Eurolinux Alliance) are meanwhile enjoying well-informed support, which however do not seem to have reached the patent politicians' negotiating rooms.  Software patents were not an issue in Laeken, but according to current %(gr:rumors), the Commissions' directorates are working together on a laxist directive which will legalise the current illegal practise of the European Patent Office and replace the current clear limits on patentability by empty words.  The final negotiations about exactly which empty words to use are expected to yield results by january.

#Gen: Gemeinschaftspatent-Bekenntnis in Laeken

#Aim: Seit Anfang 2000 haben einige Patentrechtler dem EPA-Kurs in in Fachartikeln (GRUR) widersprochen, die %(q:Liberalisierungs)-Entscheidungen des BGH (%(q:Tauchcomputer), %(q:Sprachanalyse), %(q:Logikverifikation)) kritisiert und eine Rückkehr zum Begriff der %(ti:technischen Erfindung) gefordert.  Gleichzeitig hat der %(q:stockkonservative) 17. Senat des Bundespatentgericht (BPatG) erneut ein Softwarepatent wegen mangelnder Technizität zurückgewiesen und eine Rechtsbeschwerde zum BGH zugelassen.  In ähnlicher Weise hatte das BPatG im August 2000 Computerprogramm-Patentansprüche grundsätzlich %(bp:abgelehnt) und dem Anmelder IBM eine Rechtsbeschwerde zum BGH gestattet.  Inzwischen hat der BGH %(bg:entschieden) und ist dabei einen Schritt vorwärts und zwei zurück gegangen, sofern man eine lineare Progression in Richtung Patentinflation als Zeitachse zugrunde legt.

#FWn: Leading institutes that study legal questions of patent law, such as the Max Planck Institute for International Patent Copyright and Competition Law in Munich, meanwhile continue to %(mp:plead) for patentability of computer programs.  A consortium of such institutes has completed a %(fm:research study) on this question.  Earlier, professor Lutterbeck of Berlin Technical University worked out a %(lb:study) for the same ministery, in which he also said that patentability cannot be limited in a reasonable way.

#BWW: At an %(bt:expert hearing in the German Parliament) the consensus of all german political parties against software patents was reinforced.  The %{PET} is now supported by more than 80000 signatories, 2000 chief responsible officers of IT enterprises and 300 companies.  According to experts from politik-digital.de this is the largest cyber-right signature campaign in the Internet so far.  The second-largest was directed against spam and had 30000 signatures.

#Ate: In view of these developments the Brussels initiative to legalise software patents via a EU directive has temporarily come to a halt.  The consultation was also interrupted and its results published only as piecemeal.  We have collected the scraps and %(ek:reported in detail) about the background.  We must expect the  European Commission's Industrial Property Unit to come up with another initiative for a pro software patent directive this fall.

#BPr: The German Minister of Justice expressed %(e:vehement opposition against software patents) in an interview and criticised contray plans of the patent lawmakers in Brussels.  French industry minister Christian Pierret and British e-minister Patricia Hewitt expressed similar concerns earlier.  The Dutch parliament decided that before any possible legalisation of software patents a rule for securing an appropriate level of inventivity would have to be found, and installed a workgroup, whose recommendations we report in our article %{CR}.

#Kac: Kurznachrichten

#Dei: Situation im Allgemeinen

#descr: What FFII had to report in 2001 about broad property claims on inventions and their abusive extension to computer-aided reasoning, calculating, organising and formulating.

#title: FFII Logic Patent News

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri01 ;
# txtlang: en ;
# multlin: t ;
# End: ;

