\select@language {french}
\select@language {french}
\contentsline {section}{\numberline {1}Situation en G\'{e}n\'{e}ral}{1}{section.0.1}
\contentsline {section}{\numberline {2}Kurznachrichten}{2}{section.0.2}
\contentsline {subsection}{\numberline {2.1}Laeken Summit presses for Community Patent}{2}{subsection.0.2.1}
\contentsline {subsection}{\numberline {2.2}L'Inflation de brevets neutralise l'Initiative de D\'{e}r\'{e}gulation}{3}{subsection.0.2.2}
\contentsline {subsection}{\numberline {2.3}Syndicats d'Informatique Fran\c {c}ais\@DP Brevets Logiciels risquent de transformer l'Europe en Quart Monde de l'Informatique}{3}{subsection.0.2.3}
\contentsline {subsection}{\numberline {2.4}Liikanen\@DP CE proposera la l\'{e}galisation des brevets logiciels}{3}{subsection.0.2.4}
\contentsline {subsection}{\numberline {2.5}BGH\@DP programs not patentable but program claims may be acceptable}{4}{subsection.0.2.5}
\contentsline {subsection}{\numberline {2.6}Turkish ICT sector alarmed by danger of software patents}{4}{subsection.0.2.6}
\contentsline {subsection}{\numberline {2.7}Position Eurolinux a propos du Brevet Communautaire}{4}{subsection.0.2.7}
\contentsline {subsection}{\numberline {2.8}CEC Consensus on Software Patentability Directive}{5}{subsection.0.2.8}
\contentsline {subsection}{\numberline {2.9}Directive Kober\@DP Le pr\'{e}sident de l'OEB autorise les brevets sur programmes d'ordinateur et m\'{e}thodes de gestion}{5}{subsection.0.2.9}
\contentsline {subsection}{\numberline {2.10}Le W3C veut permettre des standards WWW brevet�s payables}{5}{subsection.0.2.10}
\contentsline {subsection}{\numberline {2.11}CDU-Papier schl\"{a}gt Patent-Sonderregelung f\"{u}r Software vor}{5}{subsection.0.2.11}
\contentsline {subsection}{\numberline {2.12}Corporatistes de l'Informatique Allemande soucieux de la Syst\'{e}matique du Loi de Brevet\@DP Position GI sur la Brevetabilit\'{e} des R\`{e}gles d'Organisation et Calculation Mettables en Oevre par Ordinateur}{6}{subsection.0.2.12}
\contentsline {subsection}{\numberline {2.13}Fraunhofer/MPI 2001\@DP \'{E}tude \'{E}conomique/L\'{e}gale sur les Brevets Logiciels}{6}{subsection.0.2.13}
\contentsline {subsection}{\numberline {2.14}Seminaire sur Linuxtag Stuttgart 2001-07-05\@DP 15 ans de brevets logiciels de l'Office Europ\'{e}en de Brevets\@DP la notion de ``invention technique'' en 1986, au jour d'hui et dans l'avenir}{7}{subsection.0.2.14}
\contentsline {subsection}{\numberline {2.15}Conf�rences au sujet des brevets logiciels 2001}{8}{subsection.0.2.15}
\contentsline {subsection}{\numberline {2.16}Danger de l'Extension de la Convention de La Haye}{8}{subsection.0.2.16}
\contentsline {subsection}{\numberline {2.17}Gouvernements UE contre la pratique de l'OEB -- \'{e}tudes et consultations sur brevets logiciels ouvrent des nouvelles voies}{8}{subsection.0.2.17}
\contentsline {subsection}{\numberline {2.18}Loi Allemande a transformer universit\'{e}s en usines de brevet}{9}{subsection.0.2.18}
\contentsline {subsection}{\numberline {2.19}OMPI veut imposer la brevetabilit\'{e} sans limites et des limites sur la qualit\'{e} du brevet}{9}{subsection.0.2.19}
