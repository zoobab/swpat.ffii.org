\contentsline {section}{\numberline {1}Situation im Allgemeinen}{2}{section.1}
\contentsline {section}{\numberline {2}Kurznachrichten}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Gemeinschaftspatent-Bekenntnis in Laeken}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Patentinflation konterkariert EU-Deregulierungs-Initiativen}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Frz Informatiker-Gewerkschaft: Softwarepatente machen Europa zur Vierten Welt der Informatik}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Liikanen: Kommission wird Legalisierung von Softwarepatenten vorschlagen}{5}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}BGH: Programme nicht patentierbar aber Programmanspr\"{u}che vielleicht akzeptierbar}{5}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}T\"{u}rkische IuK-Branche \"{u}ber Swpat-Gefahren alarmiert}{5}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Eurolinux-Position zum Gemeinschaftspatent}{6}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Einigung der Eur Kommission \"{u}ber Softwarepatentrichtlinie?}{6}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Kobers Richtlinie: EPA-Pr\"{a}sident autorisiert Patente auf Computerprogramme und Gesch\"{a}ftsverfahren}{6}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}W3C will geb\"{u}hrenpflichtige WWW-Standards erlauben}{7}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}CDU-Papier schl\"{a}gt Patent-Sonderregelung f\"{u}r Software vor}{7}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}Informatik-Honoratioren sorgen sich um Systematik des Patentrechts: Stellungnahme der GI zur Frage der Patentierbarkeit Computer-Implementierbarer Organisations- und Rechenregeln}{8}{subsection.2.12}
\contentsline {subsection}{\numberline {2.13}Fraunhofer/ISI 2001: \"{O}konomisch-Rechtliche Studie \"{u}ber Softwarepatente}{8}{subsection.2.13}
\contentsline {subsection}{\numberline {2.14}Seminar Linuxtag Stuttgart 2001-07-05: 15 Jahre Softwarepatentierung am Europ\"{a}ischen Patentamt: der Technikbegriff 1986, heute und in Zukunft}{9}{subsection.2.14}
\contentsline {subsection}{\numberline {2.15}Berlin 2001-06-21: Bundestags-Expertengespr\"{a}ch Softwarepatente}{9}{subsection.2.15}
\contentsline {subsection}{\numberline {2.16}Gefahren durch Ausweitung des Haager \"{U}bereinkommens}{10}{subsection.2.16}
\contentsline {subsection}{\numberline {2.17}EU-Regierungen gegen Kodifizierung der EPA-Praxis -- Studien und Konsultationen er\"{o}ffnen Handlungsspielraum}{10}{subsection.2.17}
\contentsline {subsection}{\numberline {2.18}Gesetzentwurf soll Hochschulen zu Patentfabriken machen}{10}{subsection.2.18}
\contentsline {subsection}{\numberline {2.19}WIPO will grenzenlose Patentierbarkeit und strenge Begrenzung der Patentqualit\"{a}t festschreiben.}{11}{subsection.2.19}
