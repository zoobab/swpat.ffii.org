<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#DhW: Die %(wp:Weltorganisation für Geistiges Eigentum) plant für April und Mai 2001 einige Verhandlungsrunden, die darauf zielen, Patente weltweit leichter erhältlich und durchsetzbar zu machen und die Rechte der Patentinhaber auf der Ebene des Völkerrechts festzuschreiben, so dass kein einzelner Staat sie mehr einschränken kann.  Insbesondere der Entwurf zu einem %(sp:Vertrag über das Materielle Patentrecht) läuft darauf hinaus, das Patentrecht dauerhaft unreformierbar zu machen.

#Wbd: OMPI veut imposer la brevetabilité sans limites et des limites sur la qualité du brevet

#Dis: Durch ein im Eilverfahren zu verabschiedenes %(gf:Gesetz zur Förderung des Patentwesens an den Hochschulen) und ein dazugehöriges %(ga:Gesetz zur Änderung des Gesetzes über Arbeitnehmererfindungen) soll das Recht des Hochschullehrers, seine Forschungsergebnisse der Öffentlichkeit zur Verfügung zu stellen, restlos abgeschafft werden.  Der Hochschullehrer wird verpflichtet, alles, was möglicherweise patentrechtlich verwertbar sein könnte, der Patentverwaltung seiner Hochschule zu melden, bevor er darüber schreibt. Die Patentverwaltung, die mit staatlicher Anschubfinanzierung bis ca 2003 aufgebaut werden soll, verfügt dann über alles %(q:geistige Eigentum). Die Einwilligung der Professoren in ihren Verlust an Freiheit und Würde wird ihnen durch Vorteilsgewährung versüßt: sie erhalten 30% der erwirtschafteten Einnahmen.

#BWe: Loi Allemande a transformer universités en usines de brevet

#2hh: Bundesjustizministerin Herta Däubler-Gmelin erklärte sich in einem %(cz:Zeitungsbericht vom 3. Mai) zufolge %(q:vehement gegen Softwarepatente) und erteilte Plänen der Generaldirektion Binnenmarkt, die Praxis des europäischen Patentamts durch eine Richtlinie zu kodifizieren, eine Absage.  Ähnlich hatten sich zuvor bereits der französische Wirtschaftsminister, Christian Pierret, die britische Kollegin Patricia Hewitt und das niederländische Parlament geäußert.  Während das Patentreferat im Bundesministerium der Justiz (BMJ) Kommentare und Diskussionen zum Thema Softwarepatente in Deutschland meidet, hat das Bundesministerium für Wirtschaft und Technologie (BMWi) einiges getan, um die Diskussion zu zu vertiefen.  Dabei geht das BMWi äußerst rücksichtsvoll mit den etablierten Mächten der Patentbewegung um.  2000 ließ sich das BMWi durch ein %(lb:Gutachten) bescheinigen, dass Softwarepatente unvermeidbar seien.  Ein weiterer Auftrag erging Anfang 2001 an eine %(fh:Bietergemeinschaft von bewährten Sprachrohren der Patentbewegung).  Auch bei diesem Gutachten ist zu erkennen, dass es sich an die Sprachregelungen der Patentbewegung hält.  In anderen europäischen Ländern ist die Situation ähnlich.  Der FFII hat daher %(kf:Konsultationsfragen) erarbeitet, die Anhörungen und Gutachten auf die Sprünge helfen könnten.

#Ses: Gouvernements UE contre la pratique de l'OEB -- études et consultations sur brevets logiciels ouvrent des nouvelles voies

#AWi: Am 6. Juni 2001 beginnt in Den Haag eine neue Verhandlungsrunde zur Erweiterung des Hager Übereinkommens, welches die Vollstreckung ausländischer Urteile in bestimmten Bereichen erlaubt und regelt.  Einem dem FFII vorliegenden aktuellen Entwurf zufolge sollen Angelegenheiten des Wirtschaftsrechts und insbesondere der Immaterialgüterrechte (Patent-, Urheber-, Markenrecht) nun in den Bereich des Hager Übereinkommens mit aufgenommen werden.   Der FFII fordert, Informationsdelikte ebenso wie das Seerecht weiterhin aus dem Bereich des Hager Übereinkommens auszusparen.

#Dxg: Danger de l'Extension de la Convention de La Haye

#Icr: Dans ce papier écrit par un conseil en propriété industrielle, le président demande une brevetabilité sans limites sur toute règles d'organisation et calculation, y inclu les méthodes de gestion.  Tandis que selon le conseil GI ce type de méthode est technique et non-distingable des autre inventions, le papier demande un traitement spéciale pour ces méthodes.  Il utilise des argument juridiques naives déja longtemps refutés par les courts et les théoréticiens de droit.  Des juristes, ingénieurs et informaticiens amis de la FFII se moquent publiquement de ce papier.

#Dcn: Le président de la Société d'Informatique à de nouveau publié sa prise de position pour les brevets logiciels soumise a la Commission Européenne en Décembre 2000.

#Etd: Eine Arbeitsgruppe der CDU hat ein %(q:Strategiepapier) für die Gestaltung und Nutzung des Internet in Deutschland veröffentlicht.  Dieses Papier enthält eine kurze und widersprüchliche Passage zur Patent-Thematik.  Die Arbeitsgruppe will die Laufzeit von Logikpatenten auf 5 Jahre reduzieren und weitere %(kr:abfedernde Sonderregelungen) einführen, um die angeblich aufgrund %(q:internationaler Verpflichtungen) erforderliche Wahrung der %(q:Rechte der Urheber) zu %(q:ermöglichen).  Offenbar wurde hier einiges missverstanden.  Zudem scheint das Papier einem früheren Antrag der CDU/CSU-Fraktion zu widersprechen und auch in vielen anderen Punkten Ungereimtheiten aufzuweisen.  Aus CDU-Kreisen war zu hören, dass dieses Papier noch diskutiert werde und keine offizielle Meinung der Partei oder der Fraktion darstellen solle.

#CPn: CDU-Papier schlägt Patent-Sonderregelung für Software vor

#cid: %(BC), a leading journal of the ICT sector in Turkey, published an %(ih:interview on European devolpments concerning software patents with Eurolinux Alliance speaker Hartmut Pilch) and placed it in on the webserver front page for several days.  The interview incited some resonance, leading to a panel discussion at the %(lo:Ankara Linux and Opensource Conference) of 2001-12-07..8.

#Tne: Turkish ICT sector alarmed by danger of software patents

#IrW: De l'autre coté plusieurs experts en loi de brevets on contredit cet avis dans des journaux de doctrine juridique et demandé une retour a l'%(ti:invention technique).  Le 17ème senat de la Court Fédérale de Brevet (BPatG) a de nouveaux rejeté un brevet logiciel a cause de manque de technicité.  et permis une plainte a la BGH, qui a récemment eu tendance á accorder de brevets logiciels %(q:libéralment).  Une telle plainte á été délibérée par la BGH cet année mais ajournée sans spécification d'une nouvelle date.

#Bia: BGH: programs not patentable but program claims may be acceptable

#Dtn: During a press conference on December 4th at the IST event 2001 in Düsseldorf, the European Commission's Information Society commissioner Mr. Erkki Liikanen informed the press that a proposal on patents for computer programmes from the European Commission, DG Internal Market was due very soon.  Mr. Liikanen said that there would not be patents on business methods but there would be patents on computer-implemented rules of organisation and calculation (programs for computers), provided that they had a %(te:technical effect).  This seems to imply (a) allowing claims to information objects such as %(q:computer program product), %(q:computer program) and %(q:data structure) (b) not demanding that the invention be a teaching of applied physical causality, not defining what is %(q:technical) (c) allowing the EPO to continue granting patents for computerised business methods.  It seems that the Comission intends to follow what the EC consultation report called the %(q:economic majority) (i.e. patent departments of corporations such as %{LST}) and go ahead legalising the americanised practise of the EPO without any meaningful restrictions and make it mandatory for all national jurisdictions.

#Lke: Liikanen: CE proposera la légalisation des brevets logiciels

#Dnv: Pour le %(SP), il est essentiel que la créativité européenne ait toutes ses chances de rester Européenne. Le transfert de nos techniciens supérieurs et de nos scientifiques à l'étranger avec les technologies qu'ils contribuent à créer, représente un appauvrissement qui mine l'Europe.  Plusieurs autres syndicats d'informaticiens s'ont exprimé d'une façon similaire plus tot.

#FWW: Syndicats d'Informatique Français: Brevets Logiciels risquent de transformer l'Europe en Quart Monde de l'Informatique

#Dde: The European Commission has published a %(cl:deregulation whitebook) in which it finds that 80000 pages of EC regulation are too much and promises to reduce this body by 25% within a few years.  A German law professor suggests that the EC make it clear that the software patents granted by the EPO are illegal and will remain illegal.  This would immediately remove 100,000 pages of harmful European regulation.  If in patent matters the European Commission prefers red tape over the public interest, this may be seen as an indication of a general inability to deregulate.

#Ptu: L'Inflation de brevets neutralise l'Initiative de Dérégulation

#Dup: The politicians at the european summit in Laeken %(ld:confirmed their belief in the key role of the community patent for innovation and growth in Europe).  They want to start another attempt on Dec 20th in order to realise the CP this year.  The %(ek:counter-arguments of the Eurolinux Alliance) are meanwhile enjoying well-informed support, which however do not seem to have reached the patent politicians' negotiating rooms.  Software patents were not an issue in Laeken, but according to current %(gr:rumors), the Commissions' directorates are working together on a laxist directive which will legalise the current illegal practise of the European Patent Office and replace the current clear limits on patentability by empty words.  The final negotiations about exactly which empty words to use are expected to yield results by january.

#Gen: Laeken Summit presses for Community Patent

#Aim: Seit Anfang 2000 haben einige Patentrechtler dem EPA-Kurs in in Fachartikeln (GRUR) widersprochen, die %(q:Liberalisierungs)-Entscheidungen des BGH (%(q:Tauchcomputer), %(q:Sprachanalyse), %(q:Logikverifikation)) kritisiert und eine Rückkehr zum Begriff der %(ti:technischen Erfindung) gefordert.  Gleichzeitig hat der %(q:stockkonservative) 17. Senat des Bundespatentgericht (BPatG) erneut ein Softwarepatent wegen mangelnder Technizität zurückgewiesen und eine Rechtsbeschwerde zum BGH zugelassen.  In ähnlicher Weise hatte das BPatG im August 2000 Computerprogramm-Patentansprüche grundsätzlich %(bp:abgelehnt) und dem Anmelder IBM eine Rechtsbeschwerde zum BGH gestattet.  Inzwischen hat der BGH %(bg:entschieden) und ist dabei einen Schritt vorwärts und zwei zurück gegangen, sofern man eine lineare Progression in Richtung Patentinflation als Zeitachse zugrunde legt.

#FWn: Des instituts importants qui étudient la système de brevets continuent a se %(mp:prononcer) décidemment pour une brevetabilité des logiciels.  Un groupe d'instituts bien connu pour leur position pro-brevet á recu l'ordre du ministère d'économie de conduire une enquète parmi les entreprises.  Plus tôt un %(lb:rapport) écrit sous commande de ce ministère avait aussi estimé que la brevetabilité ne peut pas être limitée d'une façon raisonnable.

#BWW: Dans un %(bt:colloque d'experts dans le parlament allemand) le consensus de omnipartisan contre les brevets logiciels á été renforcé.  La %{PET} est maintenant soutenu par 80000 signatures, 2000 responsables d'entreprises TI et 300 entreprises.  Selon des experts d'enquête (politik-digital.de), c'est la plus large action de ce type dans l'Internet jusqu'a maintenant.

#Ate: En vue de ces developpements, l'initiative de Bruxelles de légaliser les brevets logiciels a été haltée préliminairement.  La consultation aussi n'était pas continuée et les résultats de consultation était publiés seulement en petits morceaus.  Nous avons collectionné les morceaus et %(ek:présenté l'ensemble sur une page de consultation).  Nous attendons que l'Unité de Propriété Industrielle de la Commission Européenne proposera une directive pour la légalisation des brevets logiels en Europe cet automne.

#BPr: La ministre allemande de justice a exprimé une %(e:opposition véhémente contre une brevetabilité du logiciel) et critiqué les plans de Bruxelles dans ce sense.  Le ministre d'industrie français et la %(q:e-ministre) anglaise s'ont exprimé de façon similaire.  Le parlament néérlandais a demandé que avant discuter la brevetabilité du logiciel il fallait trouver une règle appropriée pour déterminer un niveau suffisant d'inventivité.  Pour cela le parlament installa un groupe de travail dont nous rapportons les recommandations dans nôtre article %(CR).

#Kac: Kurznachrichten

#Dei: Situation en Général

#descr: Ce que la FFII avait pour rapporter en 2001 sur les monopoles d'invention accordés par l'état et leur extension abusive sur la pensée, le calcul, l'organisation et la formulation a l'aide de l'ordinateur universel.

#title: Nouvelles sur les Brevets Logiciels en 2001

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri01 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

