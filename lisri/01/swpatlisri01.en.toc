\contentsline {section}{\numberline {1}Situation im Allgemeinen}{2}{section.1}
\contentsline {section}{\numberline {2}Kurznachrichten}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Gemeinschaftspatent-Bekenntnis in Laeken}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Patentinflation konterkariert EU-Deregulierungs-Initiativen}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Frz Informatiker-Gewerkschaft: Softwarepatente machen Europa zur Vierten Welt der Informatik}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Liikanen: Commission to Propose Legalisation of Software Patents}{4}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}BGH: programs not patentable but program claims may be acceptable}{4}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Turkish software professionals alarmed over Patent Dangers}{5}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Eurolinux Position on the Community Patent }{5}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}CEC Consensus on Software Patentability Directive}{5}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Kober's Directive: EPO President authorises patents on computer programs and business methods}{5}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}W3C to allow royalty-burdened standards}{6}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}CDU-Papier schl\"{a}gt Patent-Sonderregelung f\"{u}r Software vor}{6}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}German Informatics Corporatists worried about Systematics of Patent Law: GI Position Paper on Patentability of Computer Implementable Rules of Organisation and Calculation}{6}{subsection.2.12}
\contentsline {subsection}{\numberline {2.13}Fraunhofer/MPI 2001: Economic/Legal Study about Software Patents}{7}{subsection.2.13}
\contentsline {subsection}{\numberline {2.14}Seminar Linuxtag Stuttgart 2001-07-05: 15 years of software patenting at the European Patent Office: the concept of ``technical invention'' in 1986, today and in the future}{8}{subsection.2.14}
\contentsline {subsection}{\numberline {2.15}Berlin 2001-06-21: Software Patents Hearing in the Federal Parliament}{8}{subsection.2.15}
\contentsline {subsection}{\numberline {2.16}Dangers from Extension of Hague Convention}{9}{subsection.2.16}
\contentsline {subsection}{\numberline {2.17}EU Governments against codification of EPO practise -- studies and consultations to open freedom of action}{9}{subsection.2.17}
\contentsline {subsection}{\numberline {2.18}German Law to turn universities into patent factories}{9}{subsection.2.18}
\contentsline {subsection}{\numberline {2.19}WIPO to codify unlimited patentability and strict limitation of patent quality}{10}{subsection.2.19}
