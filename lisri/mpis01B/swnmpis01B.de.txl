<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Dpe: Das Bundesministerium für Wirtschaft und Technologie (BMWi) hat am 15. November 2001 die im Januar 2001 erteilte und ursprünglich im Juli oder August erwartete Auftragsstudie von von drei dem Patentmilieu nahestehenden Forschungseinrichtungen zum Thema der %(q:makro- und mikroökonomischen Implikationen der Patentierung von Softwareinnovationen) veröffentlicht.  Diese Studie beruht auf einer aufwändigen und bisher einmaligen Befragung von mehreren hundert Softwareunternehmen und unabhängigen Entwicklern zum Stellenwert von Innovation, Standards, freier Software und gewerblichen Schutzrechten in ihrem Schaffen.  Diese Befragung ist in z.T. recht eigenwillige wirtschaftstheoretische und rechtsdogmatische Überlegungen eingebettet.  Während der hauptsächlich von Mitarbeitern des Fraunhofer Instituts für Systemtechnik und Innovationsforschung (Fraunhofer ISI) erarbeitete wirtschaftstheoretische Teil deutlich macht, dass die Softwarebranche von Patenten zu Recht wenig gutes erwartet, empfiehlt der hauptsächlich unter Leitung des Patentrechtlers Prof. Josef Straus vom Münchener Max-Planck-Institut für ausländisches und internationales Patent, Urheber- und Wettbewerbsrecht (MPI) erarbeitete rechtsdogmatische Teil erneut mit längst entkräfteten naiven Argumenten die vom MPI seit Jahren propagierte Legalisierung von Softwarepatenten, die Aufgabe des Technikbegriffs, die Patentierbarkeit funktioneller Texte und den Verzicht auf jegliche gesetzgeberische Kontrolle des Europäischen Patentamts (EPA).  Die Veröffentlichung der Studie fällt zeitlich zwischen einen rechtsetzenden Vorstoß des EPA in ebendiesem Sinne und einen Richtlinienvorschlag der europäischen Kommission, welcher die Rechtsetzungsakte des EPA im nachhinein legitimieren soll.  Betroffene und Experten fordern die Bundesregierung auf, dem in der Studie offenbar gewordenen Willen der Softwarebranche die gebührende Beachtung zu schenken und das Gemeinwohl gegen die Partikularinteressen des EPA und des MPI zu verteidigen.

#descr: Die Bundesregierung hat Patentlobby-Organisationen beauftragt, eine repräsentative Befragung der Softwarebranche über die Wirkungen von Softwarepatenten durchzuführen.  Obwohl sich dabei weitgehendes Desinteresse und starke Ablehnung der Patentierung ergab, empfehlen die Autoren in ihrem zusammenfassenden Vorspann die Legalisierung von Softwarepatenten.

#title: Fraunhofer & MPI: Die Softwarebranche will keine Patente, also lasst uns die Erteilungspraxis des EPA zügig legalisieren!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnmpis01B ;
# txtlang: de ;
# multlin: t ;
# End: ;

