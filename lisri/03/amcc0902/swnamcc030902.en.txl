<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#oPn: German press report (english version available) about McCarthy's PR and other UK patent lobbying activities in the European Parlament

#fsy: Dr. Lenz finds some more inconsistencies in McCarthy's reasoning.  According to her own argumentation, McCarthy is putting Europe at a competitive disadvantage.

#esq: McCarthy has meanwhile published her press release, in which she accuses %(q:dishonest and destructive campaigners) of %(q:abusing) parliamentary staff)

#qWi: This PR argues for a EU-financed %(q:patent defence fund) to help SMEs assert their patents %(q:against the might of multinationals), i.e. an insurance for patent owners, which is currently functioning nowhere in the world on a free-market basis.   The patent insurance idea has been championned by the Danish Patent Office, by David Ellard from DG Internal Market of the European Commission, and by John Mitchell, CEO of Allvoice Computing PLC, founder of the %(q:Patent Defence Union).   Allvoice is a patent litigation company in Devon, South-West England, whose business model McCarthy has been repeatedly citing as an example of how patents can be beneficial to software SMEs.

#tlc: Some basic questions posed to McCarthy in %(DOK) and elsewhere are meanwhile still left without a trace of an answer.  An answer to these questions could have been a first step toward the %(q:calm and considered) reasoning which McCarthy is calling for.

#Wiy: McCarthy's discourse has been criticized many times:

#sos: We would like to see more being done to assist SME's in getting patents and the setting up of a defence fund to help them protect their own inventions against the might of multinationals. Patents and the benefits they can bring, must not be seen as the exclusive domain of big companies.

#ntn: The Parliament is also challenging the Commission and Member States to do more for SME's, to enable them to both have easy access and affordable patents, but also to protect them against patent infringements by Industry giants. Some SME's have lost their patent rights because they have been unable to defend them in court against the might of the multinationals.

#tiW: An EU law can improve the current practice by both the EPO and national patent offices, by ensuring a stricter interpretation of what is patentable. It will also open up avenues to the European Court of Justice allowing more democratic scrutiny and oversight over the granting of patents in this field, and a better system of appeals to ensure that patents are handed out for 'genuine' inventions. The long-term result will be better patent law in Europe.

#Cye: McCarthy added,

#haW: This will sound the death knell for our brightest and best European inventors, whilst the US and Japan will demand licence fees from European companies for the use of their patents. Without patent protection there will be no financial incentive for our most creative industries to develop genuine inventions.

#nWi: If we were to follow the demands of these lobbyists then we would be handing over inventions to US multinationals and getting no return on our R&D investments in the field of computer implemented inventions.

#tna: She warned that campaigners against patents for computer implemented-inventions risk seriously undermining European Industry's interests by putting them at an extreme disadvantage in the global marketplace and putting at risk jobs in the growing software industry, adding,

#Wtr: I welcome representation from all sides of industry on this complex area of patent law, but it does not entitle people to abuse my staff in an aggressive and bullying manner. We need to approach this legislation, in particular the more difficult issues, in a calm considered way.

#voc: This is a dishonest and destructive campaign designed to cause confusion about what the Parliament is trying to achieve. They are bombarding members with factually incorrect claims and orchestrating ranting calls to members' offices.

#Wei: McCarthy hit out at claims made by opponents against the new EU law, who last week brought their campaign to Brussels:

#oic: Patent protection is vital if we are to challenge the US dominance in the software inventions market. While Japan is to step up its activity in supporting industry's efforts to use patents and licensing fees to reap the benefits of their inventions in this field, if European industry is to have a fighting chance in the field of computer-implemented inventions, then they must have the necessary protection.

#1We: In a situation where both the European Patent Office (EPO) and the 15 national patent offices are handing out patents for computer-implemented inventions, an EU law can assist in clarifying the limits to patentability in the field of computer-implemented inventions. This would give industry more certainty and help innovators realise financial rewards and returns on their investment in novel and inventive technologies in the increasingly cut throat global marketplace.

#ooW: A proposal for an EU wide law on patents for computer-implemented inventions is essential both to protect the interests of European Industry and prevent the drift towards US-style patenting of business methods.

#esg: Following a barrage of misinformation about the new EU wide patenting proposals, Labour MEP Arlene McCarthy - who wrote the Parliament's Report on the new proposals and is steering it through the Parliament - spoke out against the systematic campaign of misinformation being waged against new rules in the run up to the Strasbourg vote saying:

#olh: Controversial new legislation on patents for computer-implemented inventions will be put to a critical vote in the European Parliament in Strasbourg at the end of this month (Parliamentary Session 22-25 September).

#sWW: MEPs must back EU plans for patents for inventions

#te2: 1st September 2003

#Wae: For immediate release

#Wpo: Subject: MEPs must back EU plans for patents for inventions

#eWo: This Press Release was sent out by the %(q:UK Labour Delegation in the European Parliament) (PseDelegUK at europarl eu int) to all Labour MEPs on monday Sep 1st 18:11 for immediate publication.

#PWf: McCarthy/Labour PR: %(q:MEPs must back EU plans for patents for inventions)

#descr: In response to last week's wave of protests against her software patent directive proposal, Arlene McCarthy MEP, rapporteur of the European Parliament for the EU Software Patent Directive Proposal, has circulated a press release which %(q:hits out against claims made by opponents to the new EU law).  McCarthy, speaking in the name of UK Labour and partially of the parliament as a whole, calls on fellow MEPs to %(q:back EU plans for patents for inventions) later this month, warning them not to be misled by a %(q:dishonest and unconstructive campaign), %(q:orchestrated) by a group of %(q:lobbyists), who are %(q:bullying) parliamentary staff and %(q:putting at risk jobs in the growing software industry).

#title: Arlene McCarthy PR %(q:hits out) against %(q:dishonest ... bullying ... lobbyists)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnamcc030902 ;
# txtlang: en ;
# multlin: t ;
# End: ;

