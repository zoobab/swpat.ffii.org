<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Intel chairman: patent system unfit for information age

#descr: In an interview with the Washington Post, Intel Chaiman Andrew S. Grove says that the USA %(q:needs to revamp not just the patent system, but the entire system of intellectual property law. ... It needs to redefine it for an era that is the information age as compared to the industrial age.)

#tnm: IP system needs redefinition for information age

#cWe: US ICT decline due to neglected infrastructures + patent inflation

#haw: The Washington Post has an %(wp:article) which reads:

#uWn: The country %(q:needs to revamp not just the patent system, but the entire system of intellectual property law,) said Andrew S. Grove, chairman of Intel Corp. %(q:It needs to redefine it for an era that is the information age as compared to the industrial age.)

#pnW: This coincides with the %(ep:European Parliament's clarification of september) that %(orig:Data processing is not a field of technology and innovations in the field of data processing are not inventions in the sense of patent law.)

#xWW: The telcoCEOs %(tc:wrote): %(q:We do not want to see any sudden or dramatic reduction in the scope of what is patentable), and here we have Intel Corp saying %(q:please change now!)

#Wka: In a %(ag:presentation) given at a BSA meeting, Grove points out several factors which cumulatively are destroying the competitive capability of the US IT sector:  (1) lack of governmental spending on education and basic non-competitive infrastructures (he cites East-Asian countries as counter-examples, where greater numbers of well-educated people are concentrated and better IT infrastructures have been built thanks to government spending) (2) a disastrous rise in the cost of litigation, caused by changes in the US patent system during the last 20 years.

#oei: Grove's conclusion is that the US government should spend 1 billion USD per year to %(q:strengthen the patent office), so as to enable it to reject more patents.  Hartmut Pilch, president of FFII explains: %(q:We have %(sp:calculated) that 1 billion USD is far too little to make any noticeable difference.  A more effective and very cheap cure is available, but apparently the political taboos erected by patent lawyers are still functioning in the USA, where, unlike in Europe, the problem of software patents has never been subjected to parliamentary decision.)

#Wsi: Intel Chairman: Patent System is Slowing Innovation and Decreasing Competitivity in US

#sos: Intel co-founder and chairman Andrew S. Grove, in which he warns that the US is losing competitivity in key tech sectors: %(orig|Grove also criticized the nation's overburdened patent system, which he said is causing an abundance of innovation-slowing litigation.|He said that the inability of patent examiners to handle the workload has led to a backlog of important applications, but also less than thorough vetting of patents that perhaps should not be granted.)  This was reported by Jonathan Krim in the Washington Post, 2003-10-10, Page E01.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: intel031211 ;
# txtlang: en ;
# multlin: t ;
# End: ;

