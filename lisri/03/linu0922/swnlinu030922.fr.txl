<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#oWW: Autres contacts sur demande

#son: Le communiqué de presse a été rédigé par Electronic Frontier Finland.

#Fsf: Version EFFI du communiqué de presse

#svvers: Version suédoise

#fivers: Version finnoise

#wEi: La lettre a d'abord été publiée par Electronic Frontier Finland.

#Ioe: Version EFFI de la lettre

#eWl: Dans leur lettre, Torvalds et Cox formulent trois requêtes au  sujet de la Directive. Premièrement, elle doit clarifier les limites de  la brevetabilité, de telle sorte que les programmes informatiques et  les méthodes commerciales ne puissent véritablement pas être brevetés  en tant que tels. Deuxièmement, elle doit veiller à ce que l'on ne  puisse utiliser les brevets de façon abusive, en faisant obstacle à  l'interopérabilité de produits concurrents pour échapper à la  concurrence technique. Enfin, elle doit garantir que les brevets ne  pourront être utilisés pour empêcher la publication d'informations.

#aWs: La %(ol:Lettre Ouverte) milite aussi fortement en faveur des  standards ouverts. Linus Torvalds explique que : %(bc:Sans standards  ouverts, il n'est pas vraiment possible de développer des systèmes  ouverts. Et au final, sans systèmes ouverts, les citoyens ne seront  plus libres dans la société.

#gob: Alan Cox, créateur et mainteneur d'une grande partie du noyau  Linux et qui travaille au Royaume-Uni pour Redhat, note que :  %(bc:Actuellement, les sociétés délocalisent la programmation. Cet  immense mouvement d'émigration à partir des États-Unis n'est pas  seulement commandé par des considérations de prix de revient, mais  aussi par les risques et litiges liés aux brevets. Les sociétés créent  une une holding américaine pour les droits de propriété intellectuelle.  Cette holding accorde, à son tour, à une personne morale non américaine  le droit d'écrire le programme à l'étranger et de l'importer, ce qui  permet de réduire les risques.) Il souligne que : %(bc:Adopter le même  type de brevets au sein de l'Union Européenne chasserait, de la même  façon, des milliers de travaux de programmations hors d'Europe.)

#owW: Linus Torvalds, créateur initial et mainteneur actuel du noyau du  système opératoire Linux, remarque que : %(bc:L'expérience des  États-Unis montre que les brevets logiciels ne profitent à personne,  sauf peut-être aux conseillers juridiques en matière de brevets. Ils ne  peuvent qu'affaiblir le marché et accroître les coûts liés aux brevets  et les frais de justice, aux dépens de l'innovation technologique et de  la recherche.) Et ajoute : %(bc:Nous espérons que les Membres du  Parlement Européen mesureront ces effets négatifs et n'imposeront pas  le même chaos sur le vieux continent.)

#swnlinu030922d: The two chief architects of the Linus operating system kernel, Linus Torvalds from Finland and Alan Cox from UK, ask for effective limitations to patentability in their letter to the members of the European Parliament. In particular, they recommend MEPs to follow the FFII voting list.  The vote on the Directive will be on Wednesday and it is expected to be a very close one.

#swnlinu030922t: Linus Torvalds et Alan Cox font appel aus MPE de maintenir l'Europe libre de Brevets Logiciels

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: mgaroche ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnlinu030922 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

