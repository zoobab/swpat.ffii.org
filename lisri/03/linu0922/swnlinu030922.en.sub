\begin{subdocument}{swnlinu030922}{Linus Torvalds and Alan Cox call on MEPs to keep Europe free from software patents}{http://swpat.ffii.org/news/03/linu0922/index.en.html}{Workgroup\\swpatag@ffii.org}{The two chief architects of the Linus operating system kernel, Linus Torvalds from Finland and Alan Cox from UK, ask for effective limitations to patentability in their letter to the members of the European Parliament. In particular, they recommend MEPs to follow the FFII voting list.  The vote on the Directive will be on Wednesday and it is expected to be a very close one.}
\begin{sect}{detal}{details}
Linus Torvalds, the original creator and current maintainer of the Linux operating system kernel, comments: \begin{quote}
{\it The experiences from the USA demonstrate that software patents don't benefit anyone but perhaps the patent lawyers. They will just weaken the market and increase spending on patents and litigation, at the expense of technological innovation and research.}
\end{quote} He continues: \begin{quote}
{\it We hope that the members of European Parliament see these negative sides and don't push the same chaos to the old continent.}
\end{quote}

Alan Cox, creator and maintainer of large subparts of the Linux kernel, working for Redhat in UK, notes: \begin{quote}
{\it Currenly, the companies are moving programming jobs offshore. The huge move away from the USA is not entirely driven by pricing but by patent litigation and risk. Companies create a US holding company for the IPR which licenses it to a non US body to write the software overseas and import it, so as to reduce risk.}
\end{quote} He stresses: \begin{quote}
{\it Adopting the same kind of patents in the EU will drive thousands of EU programming jobs overseas, too.}
\end{quote}

In their letter, Torvalds and Cox set three requests for the Directive.  Firstly, it should clarify limits of patentability so that computer programs and business methods really cannot be patented as such. Secondly, the Directive should make sure that patents cannot be abused to avoid technical competition by preventing interoperability of competing software. Finally, the patents should not be allowed to be used to prevent publication of information.
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Linus Torvals \& Alan Cox 2003/09/22: Letter to the European Parliament\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/linus0309/index.en.html}}}

\begin{quote}
explains dangers of software patents and chances of the European Parliament to save Europe from these dangers this week, recommends that MEPs should support the voting recommendations of FFII.
\end{quote}
\filbreak

\item
{\bf {\bf EFFI version of Letter\footnote{http://www.effi.org/patentit/patents\_torvalds\_cox.html}}}

\begin{quote}
The letter was first published by the Electronic Frontier Foundation of Finland.

see also Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/vote/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Same in Finnish\footnote{http://www.effi.org/patentit/patents\_torvalds\_cox\_fi.html}}}

\begin{quote}
see also Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/vote/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Same in Swedish\footnote{http://www.effi.org/patentit/patents\_torvalds\_cox\_sv.txt}}}

\begin{quote}
see also Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/vote/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf EFFI version of PR\footnote{http://www.effi.org/julkaisut/tiedotteet/pressrelease-2003-09-22.html}}}

\begin{quote}
The PR was drafted by the Electronic Frontier Foundation of Finland.

see also Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/vote/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Same in Finnish\footnote{http://www.effi.org/julkaisut/tiedotteet/lehdistotiedote-2003-09-22.html}}}

\begin{quote}
see also Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/vote/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003/09 Software Patent Directive Amendments: Real vs Fake Limits\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html}}}

\begin{quote}
The European Parliament is scheduled to decide about the Software Patent Directive on September 23rd.  The directive as proposed by the European Commission demolishes the basic structure of the current law (Art 52 of the European Patent Convention) and replaces it by the Trilateral Standard worked out by US, European and Japanese Patent Offices in 2000, according to which all ``computer-implemented'' problem solutions are patentable inventions.  Some members of the Parliament have proposed amendments which aim to uphold the stricter invention concept of the European Patent Convention, whereas others push for unlimited patentability according to the Trilateral Standard, albeit in a restrictive rhetorical clothing.  We attempt a comparative analysis of all proposed amendments, so as to help decisionmakers recognise whether they are voting for real or fake limits on patentability.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{contact}
\begin{description}
\item[mail:]\ media at ffii org
media-fi at ffii org
info at effi org
\item[phone:]\ Hartmut Pilch +49-89-18979927
More Contacts to be supplied upon request
\end{description}
\end{sect}

\begin{sect}{ffii}{About the FFII -- www.ffii.org}
The Foundation for a Free Information Infrastructure (FFII) is a non-profit association registered in Munich, which is dedicated to the spread of data processing literacy.  FFII supports the development of public information goods based on copyright, free competition, open standards.  More than 300 members, 700 companies and 50,000 supporters have entrusted the FFII to act as their voice in public policy questions in the area of exclusion rights (intellectual property) in data processing.
\end{sect}

\begin{sect}{url}{Permanent URL of this Press Release}
http://swpat.ffii.org/news/03/linu0922/index.en.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

