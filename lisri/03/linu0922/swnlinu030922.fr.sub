\begin{subdocument}{swnlinu030922}{Linus Torvalds et Alan Cox font appel aus MPE de maintenir l'Europe libre de Brevets Logiciels}{http://swpat.ffii.org/journal/03/linu0922/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise 2003/10/13 par Michèle Garoche\footnote{http://micmacfr.homeunix.fr}}{The two chief architects of the Linus operating system kernel, Linus Torvalds from Finland and Alan Cox from UK, ask for effective limitations to patentability in their letter to the members of the European Parliament. In particular, they recommend MEPs to follow the FFII voting list.  The vote on the Directive will be on Wednesday and it is expected to be a very close one.}
\begin{sect}{detal}{details}
Linus Torvalds, cr\'{e}ateur initial et mainteneur actuel du noyau du  syst\`{e}me op\'{e}ratoire Linux, remarque que : \begin{quote}
{\it L'exp\'{e}rience des  \'{E}tats-Unis montre que les brevets logiciels ne profitent \`{a} personne,  sauf peut-\^{e}tre aux conseillers juridiques en mati\`{e}re de brevets. Ils ne  peuvent qu'affaiblir le march\'{e} et accro\^{\i}tre les co\^{u}ts li\'{e}s aux brevets  et les frais de justice, aux d\'{e}pens de l'innovation technologique et de  la recherche.}
\end{quote} Et ajoute : \begin{quote}
{\it Nous esp\'{e}rons que les Membres du  Parlement Europ\'{e}en mesureront ces effets n\'{e}gatifs et n'imposeront pas  le m\^{e}me chaos sur le vieux continent.}
\end{quote}

Alan Cox, cr\'{e}ateur et mainteneur d'une grande partie du noyau  Linux et qui travaille au Royaume-Uni pour Redhat, note que :  \begin{quote}
{\it Actuellement, les soci\'{e}t\'{e}s d\'{e}localisent la programmation. Cet  immense mouvement d'\'{e}migration \`{a} partir des \'{E}tats-Unis n'est pas  seulement command\'{e} par des consid\'{e}rations de prix de revient, mais  aussi par les risques et litiges li\'{e}s aux brevets. Les soci\'{e}t\'{e}s cr\'{e}ent  une une holding am\'{e}ricaine pour les droits de propri\'{e}t\'{e} intellectuelle.  Cette holding accorde, \`{a} son tour, \`{a} une personne morale non am\'{e}ricaine  le droit d'\'{e}crire le programme \`{a} l'\'{e}tranger et de l'importer, ce qui  permet de r\'{e}duire les risques.}
\end{quote} Il souligne que : \begin{quote}
{\it Adopter le m\^{e}me  type de brevets au sein de l'Union Europ\'{e}enne chasserait, de la m\^{e}me  fa\c{c}on, des milliers de travaux de programmations hors d'Europe.}
\end{quote}

Dans leur lettre, Torvalds et Cox formulent trois requ\^{e}tes au  sujet de la Directive. Premi\`{e}rement, elle doit clarifier les limites de  la brevetabilit\'{e}, de telle sorte que les programmes informatiques et  les m\'{e}thodes commerciales ne puissent v\'{e}ritablement pas \^{e}tre brevet\'{e}s  en tant que tels. Deuxi\`{e}mement, elle doit veiller \`{a} ce que l'on ne  puisse utiliser les brevets de fa\c{c}on abusive, en faisant obstacle \`{a}  l'interop\'{e}rabilit\'{e} de produits concurrents pour \'{e}chapper \`{a} la  concurrence technique. Enfin, elle doit garantir que les brevets ne  pourront \^{e}tre utilis\'{e}s pour emp\^{e}cher la publication d'informations.
\end{sect}

\begin{sect}{links}{Liens Annot\'{e}s}
\begin{itemize}
\item
{\bf {\bf Linus Torvals \& Alan Cox 2003/09/22: Lettre au Parlement Europ\'{e}en\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/linus0309/index.fr.html}}}

\begin{quote}
explique les danger des brevets logiciels et les possibilit\'{e}s pour le Parlement  Europ\'{e}en de sauver l'Europe de ces dangers cette semaine, recommande que les membres du parlement europ\'{e}en suivent les recommandations de vote de la FFII.
\end{quote}
\filbreak

\item
{\bf {\bf Version EFFI de la lettre\footnote{http://www.effi.org/patentit/patents\_torvalds\_cox.html}}}

\begin{quote}
La lettre a d'abord \'{e}t\'{e} publi\'{e}e par Electronic Frontier Finland.

voir aussi Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/plen0309/vote/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Version finnoise\footnote{http://www.effi.org/patentit/patents\_torvalds\_cox\_fi.html}}}

\begin{quote}
voir aussi Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/plen0309/vote/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Version su\'{e}doise\footnote{http://www.effi.org/patentit/patents\_torvalds\_cox\_sv.txt}}}

\begin{quote}
voir aussi Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/plen0309/vote/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Version EFFI du communiqu\'{e} de presse\footnote{http://www.effi.org/julkaisut/tiedotteet/pressrelease-2003-09-22.html}}}

\begin{quote}
Le communiqu\'{e} de presse a \'{e}t\'{e} r\'{e}dig\'{e} par Electronic Frontier Finland.

voir aussi Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/plen0309/vote/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Version finnoise\footnote{http://www.effi.org/julkaisut/tiedotteet/lehdistotiedote-2003-09-22.html}}}

\begin{quote}
voir aussi Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/plen0309/vote/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 09/2003 Amendements sur la Directive des Brevets Logiciels: Vraies contre Fausses Limites\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/plen0309/index.fr.html}}}

\begin{quote}
Le Parlement Europ\'{e}en a pr\'{e}vu de de donner sa d\'{e}cision au sujet de la Directive sur les Brevets Logiciels le 24 septembre.  La directive telle qu'elle est propos\'{e}e par la Commission Europ\'{e}enne d\'{e}molit les fondements de la structure de la loi actuelle (Article 52 of de la Convention sur les Brevets Europ\'{e}enne) et la remplace par le Standard Trilat\'{e}ral developp\'{e} par les Offices de Brevets am\'{e}ricain, europ\'{e}en et japonais en 2000, selon lequel toutes les solutions aux probl\`{e}mes ``mises en oeuvre sur ordinateur'' sont des inventions brevetables.  Quelques membres du Parlement ont propos\'{e} des amendements dans le but de soutenir le plus strict des concepts d'invention au sens de la Convention des Brevets Europ\'{e}enne, alors que d'autres ont pouss\'{e} \`{a} la brevetabilit\'{e} illimit\'{e}e au sens du Standard Trilat\'{e}ral, quoique dans un habillement rh\'{e}torique restrictif.  Nous avons essay\'{e} de faire une analyse comparative de tous les amendements propos\'{e}s, de fa\c{c}on \`{a} aider les preneurs de d\'{e}cisions \`{a} reconna\^{\i}tre s'ils votent pour de r\'{e}elles ou de fausses limites de brevetabilit\'{e}.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{Contacte}
\begin{description}
\item[courriel:]\ media at ffii org
media-fi at ffii org
info at effi org
\item[t\'{e}l:]\ Hartmut Pilch +49-89-18979927
Autres contacts sur demande
\end{description}
\end{sect}

\begin{sect}{ffii}{A propos de la FFII -- www.ffii.org}
La FFII est une association munichoise reconnue d'utilit\'{e}  publique pour la formation dans le domaine du traitement des donn\'{e}es.  La FFII soutient le d\'{e}veloppement de biens informationnels publics  fond\'{e}s sur les droits d'auteur, la libre concurrence et les standards  ouverts. Plus de 300 membres, 700 soci\'{e}t\'{e}s et 50 000 adh\'{e}rents ont charg\'{e} la FFII de repr\'{e}senter leurs int\'{e}r\^{e}ts dans la domaine de la l\'{e}gislation sur les droits de propri\'{e}t\'{e} attach\'{e}s aux logiciels.
\end{sect}

\begin{sect}{url}{URL permanente de cette publication}
http://swpat.ffii.org/journal/03/linu0922/index.fr.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

