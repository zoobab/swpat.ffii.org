<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#descr: What FFII had to report in 2003 about broad property claims on inventions and their abusive extension to computer-aided reasoning, calculating, organising and formulating.

#title: FFII Software Patent News 2003

#psn: MEPs propose 106 Amendments to Software Patent Directive

#aft: Members from all groups of the European Parliament have submitted a total of 120 amendment proposals to the software patent directive draft.  Virtually all proposals show an intention of wanting to avoid software patents or limit their scope and effects.  Some amendments serve the professed aim while others do not.  None can achieve the aim on their own, embedded in a long directive text which codifies unlimited patentability with many redundant safeguards.  We are working on a tabular overview and analysis of all amendments.

#tWM: Letter to MEPs

#leP: This last appeal was sent by FFII/Eurolinux to all Members of the Euroepan Parliament (MEPs) by E-Mail and Fax.

#hKr: German Government continuing to press for maximal patentability, ignoring criticsm of top patent judge

#uWe: The FFII has reviewed one of several recent articles of judge Klaus J. Melullis.  Unfortunately the German government is not listening to the opinions of judge Melullis and his colleagues.  For them only the views of IBM's corporate patent department count as the real evangile of patent law.  At the EU level, the german government's ministry of justice is continuing to pressure the Commission and the European Parliament to suppress the last remnants of freedom of publication (Art 4) and freedom of interoperation (Art 6a) from the directive proposal.

#dcc: The EU software patent directive is a %(q:wolf in a sheep's coat).  We have documented in detail what misleading terms such as %(q:technical contribution) really mean.

#nsW: Gates: Linux infringing numerous patents, SCO is just the beginning

#rrc: McCarthy's First Public Reply

#rrW: Arlene McCarthy, Member of the European Parliament for UK labour and rapporteur of the JURI committeee for the software patent directive proposal, has for the first time directly answered arguments from critics in a letter to the british newspaper The Guardian.  Yet the basic questions, e.g. what should be patentable and how McCarthy's proposals achieve this, remain unanswered.  McCarthy reiterates demagogic statements of whose untruth she is well-informed and even resorts to lies in the strictest sense of the term, such as saying that she introduced a provision to allow decompilation.  McCarthy moreover attacks the GNU General Public License in an apparent effort to shift focus to unrelated subjects and incite flamewars with the free software community.  We analyse McCarthy's fallacies and the political context of her letter.

#iWP: Petition Submitted to European Parliament

#ftl: %(AD) from %(AEL) has gone through the somewhat cumbersome procedures at the European Parliament and received confirmation that the %(EP) with its 140,000 virtual signatures has been filed.   Now the European Parliament' Petition Commission is entitled to take action in response to it.  Contact persons are Alexandre for AEL and Hartmut Pilch for FFII.

#noL: Belgian Association of Electronic Liberties

#uot: Schedule for Conferences at Europarl

#WWy: 1st JURI discussion on McCarthy Draft

#mse: The Legal Affairs Commission of the European Parliament discussed rapporteur Arlene McCarthy's draft report.  Our analysis of the draft had been handed in printing to all 70 JURI members beforehand.  FFII/Eurolinux sees this report as piece of legalese junk, written from a US large corporate patent department's perspective, characterised by unusually frivolous tactics of confusion and deception.  Few attended and only a handful of JURI MEPs spoke at the meeting.  The atmosphere seemed harmonious and supportive.  Mild criticism about the lack of clarity in McCarthy's draft was uttered by MacCormick (Greens), Gebhard (ESP) and Wuermeling (EPP).

#rWb: McCarthy for unlimited patentability

#Wiw: New FFII Database and Statistics of EPO software patents

#Wip: We have analysed all patents ever granted by the EPO so far, selected those with a high likelihood of being software patents for more convenient browsing and prepared some statistics based thereon.

#eii: ITRE votes for right to publish software and to interoperate

#amu: The Industry and Trade Commission (ITRE) of the European Parliament voted for amendments to the directive proposal that give freedom of publication and interoperability absolute priority over patents.  EPP members (conservative, christian-democrat) proposed amendments in the opposite direction, i.e. for a wider scope of patentability and for treating publication as a direct infringment.  These were rejected by the center-left majority.

#eop: CULT votes for clear exclusion of software from patentability

#wWt: The Cultural Affairs Commission of the European Parliament voted for amendments to the directive proposal that exclude software from patentability.  On the one hand, %(q:data processing is not a field of technology), on the other %(q:technology) is positively defined as %(q:controlling forces of nature to achieve a physical effect).  These amendments were proposed by former french prime minister Michel Rocard and supported by all parties except for the largest, the European People's Party (EPP = conservatives and christian democrats), who, under the guidance of their shadow rapporteur Joachim Wuermeling and committee member Janelly Fourtou (wife of Vivendi president), voted against everything that limited patentability and even proposed amendments to enlarge patentability, which were voted down by the center-left majority.

#vnc: by %1

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri03 ;
# txtlang: en ;
# multlin: t ;
# End: ;

