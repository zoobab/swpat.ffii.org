<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EICTA calls on Commission, Council to Kill Parliament Vote

#descr: EICTA, a lobbying group representing most large telecommunication and consumer electronics companies in Europe as well as some SMEs has published a press release which literally echoes the letter signed by CEOs of Nokia, Ericsson, Alcatel, Philips and Siemens earlier this month, complaining about the European Parliament's amendments to the proposed software patent directive and asking the Commission and the Council to undo these amendments and immediately go on a confrontation course with the Parliament.  The Council did not follow this call during its meeting of today but postponed the debate to next spring, when the Irish presidency takes over.  EICTA expresses dismay at this postponement and directs demands of %(q:the industry) to the Irish presidency as well as to the Belgian government.   The latter demands are based on the false claim that the Belgian EICTA member organisation AGORIA represents Belgian SMEs.

#rWe: Press Release

#Ai3: EICTA Press Briefing 2003-11-26

#Ilf: The EICTA press release can be found via

#ers: Please look for press releases 2003.

#uhm: This press release uses wordings from a %(tl:letter signed by the CEOs of large telecommunication and consumer electronics companies).

#urW: Industry Calls on Council to Correct Damage Done by the European Parliament -- European R&D Will Be Jeopardised If CII Patents Are Eliminated

#WWt: For further information,

#hri: Leo Baumann was until recently the assistant of %(AN), bavarian party colleague working closely with shadow rapporteur %(JW), and thereby influential on much of the policy formulation in the EPP (conservative block in Europarl).

#ocs: The argumentation of EICTA statements on patent policy usually relies on no evidence, no reasoning, only false representation claims -- corporate patent lawyers masquerading as %(q:the industry).

#jWs: EICTA, the European ICT and consumer electronics association, together with AGORIA, the Belgian multi sector federation for the technology industry would like to invite you to our joint press briefing over breakfast on the Commission's proposal on a directive on the patentability of computer-implemented inventions, taking place in Brussels on 26 November 2003 from 9.00 until 10.30 am (Diamant Building, 80 BD. Reyers).

#2me: The Competitiveness Council will discuss the Commission's proposal on 26/27 November. The Council will react to the damaging amendments proposed by the European Parliament in September 2003. Given the negative effect which the Parliament's recommendations would have on European research and development as well as competitiveness, industry expects the Competitiveness Council to express its concern over recent developments which are contrary to the goals of eEurope and the Lisbon process.

#WeW: Tim Frain, Director of IPR Nokia will explain the impact, the directive will have on European Industry in terms of R&D and competitiveness.

#ywn: Roland Lousky, Vice-President %(IC) will present the issue from an SME perspective.

#sfW: Press briefing on the Commission's proposal on a directive on the patentability of computer-implemented inventions in Brussels on 26 November 2003. Venue: EICTA, Diamant Building, 80 BD. Reyers.

#WWD: Venue: EICTA, Diamant Building, 80 BD. Reyers

#rrm: Programme:

#WIr: Welcome by Leo Baumann, EICTA and Filip Geerts, AGORIA

#tWW: Introduction into the topic: Tim Frain, Director of IPR Nokia

#pWe: Commission proposal, implementing the status quo of EPO practice in European law

#ery: importance of the directive for the European Economy as a whole

#rsh: answers to concerns raised in the debate

#eto: co-existence of open source and patents,  patents as addition to copyright etc.

#sdt: consequences of the amendments proposed by the European Parliament in first reading

#Wtk: Patents on CII from an SMEs perspective: Roland Louski, Vice-President %(IC)

#dss: Q&A session

#riW: end of programme, possibility for one-to-one discussions

#nWe: A company focussing on enforcement of copyright in digital environments, presents itself as %(q:your one-stop site for registration and clearance of intellectual property).

#Wi0: Roland Lousky, whom EICTA presents as its SME representative, is a lawyer working for the IPR clearance company info2clear.  Lousky has written french articles on EU legislation in legal journals, of which one titled %(TT), published 2001-01-19 in the french law journal %(DT), is cited here.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eicta031127 ;
# txtlang: en ;
# multlin: t ;
# End: ;

