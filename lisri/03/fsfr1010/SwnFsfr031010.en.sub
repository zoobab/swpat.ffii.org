\begin{subdocument}{SwnFsfr031010}{Software Patents: Breakthrough in the European Parliament}{http://swpat.ffii.org/news/03/fsfr1010/index.en.html}{Workgroup\\swpatag@ffii.org}{The European Parliament voted on September 24th for a directive proposal which confirms the existing European law, makes software explicitely unpatentable and codifies additional safeguards, such as freedom of publication and interoperation.  The amended directive proposal thereby achieves the claimed aims of the European Commission, especially ``harmonisation and clarification of the status quo'' and ``prevention of a drift toward US-style patentability of pure software and business methods''.  However, the European Commission doesn't seem to be happy.  Internal Market Commissioner Frits Bolkestein and others have been threatening to withdraw the directive project and to pass the ball back to national patent administrators and, should that fail, to rely on brotherly help from Washington.  But the European Parliament was neither deceived nor intimidated.  Now the patent movement's strategy is to dismiss the Parliament's position as ``unworkable'' and to attribute it to ``ignorance'' rather than to a conscious policy decision.  Bolkestein's friends can be counted on to resort to whatever inconsistency, illoyalty or illegality is necessary in order to obtain what they really want: ``legal security'' for the owners of more than 30,0000 US-style patents on software and business methods, granted in accordance with a law-to-be, which the European Parliament has refused to pass for them.  A few months of intense struggle lie ahead.}
The amended directive\footnote{http://swpat.ffii.org/papers/europarl0309/index.en.html} speaks for itself.  Art 1-6 exclude software from patentability in multiple ways and form a cohesive whole, which, in contrast to the European Commission's illogical proposal, is completely congruent with Article 52 of the Euroepan Patent Convention and Art 27 of the TRIPs treaty.  Please read the whole text carefully.  Here we just have room to cite a few passages:

\begin{quote}
{\it Recital 7\\
Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception applies because the said subject-matter and activities do not belong to a field of technology.}

{\it Article 2b.\\
``technical contribution'', also called ``invention'', means a contribution to the state of the art in technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.}

{\it 2ba. ``technical field'' means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. ``Technical'' means ``belonging to a technical field''.}

{\it 2bb. ``industry'' in the sense of patent law means ``automated production of material goods'';}

{\it Article 3a.\\
Member states shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not considered to be inventions in the sense of patent law.}

{\it 5.1.b. Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.}

{\it 6a. Member States shall ensure that, wherever the use of a patented technique is needed for a significant purpose such as ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.}
\end{quote}

US patent attorney Erwin Basinski reports\footnote{http://swpat.ffii.org/papers/europarl0309/aipla0310/index.en.html} to AIPLA, the patent lawyer lobby organisation, about what he -- not quite incorrectly -- sees as the success of our work in the European Parliament:

\begin{quote}
{\it The EU Parliament recently voted on the Proposed Software Directive. The text as amended (a copy is attached) appears to completely eliminate any software patent and make unenforceable most existing patents.}

{\it [\dots]}

{\it The apparent influence of the open source community on the members of Parliament and the Parliament's general apparent lack of understanding of the technological and business advances resulting from the current and predicted use of computer related inventions, are truly remarkable and illustrate the political nature of the problems.}

{\it [\dots]}

{\it As to the amendments themselves, to me the most onerous are 32\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am32} (which is just incorrect); 95\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am95} (which would put the EPO out of business); 84\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am84}; all of the amendments to Article 2 (36\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am36}, 42\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am42}, 117\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am117}, 107\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am107}, 69\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am69}, 55rev\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am55}, 97\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am97}, 108\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am108}, 38\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am38}, 44\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am44} and 118\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am118}) (which would negate any software related invention); 45\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am45} (makes no sense when you consider the billions of dollars/euros invested in the financial/banking/stock market and related industries to make those services function); 70\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am70}; 60\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am60}; 102\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am102} and 111\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am111}; 72\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am72}; 103\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am103} and 119\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am119}; 104\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am104} and 120\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am120}; 76\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/index.en.html\#Am76} (reworded Article 6a is still destructive of most existing software patents by itself).}

{\it My sources on the Commission are trying to determine what to do.  They believe the Council will reject this version and send it back to Parliament for a second reading (vote) but the question is whether it can be saved at all.  I think our hope is it will either just die or be withdrawn by the Commission.}
\end{quote}

Another patent attorney reports\footnote{http://swpat.ffii.org/papers/europarl0309/reag/index.en.html}:

\begin{quote}
{\it The users of the patent system have just suffered a severe political defeat, even if the Council manages to rectify the current situation.  This was probably the very first time since the patent disputes of the nineteenth century that patent law was in the headlines. The controversy on patents will continue, that is for sure.}

{\it [\dots]}

{\it After having studied the result of yesterday's voting of the European Parliament I must say that the situation is much more worse than I had feared before. The gist of what the directive says is that preferably no patents should be granted on anything that is related to computers, and if under some narrow preconditions exceptionally a patent is ever granted there are such burdens put upon the applicant that it would be rendered virtually worthless. For example, Article 2, point (bb) of the Directive now narrows the term ``industry'' to the ``automated production of material goods''. This is simply absurd.}

{\it [\dots]}

{\it The Eurolinux activists clearly have overshot their mark: They have managed to bring so many of their preferred amendments in a redundant, self-contradictory and amateurish fashion into the wording of the Directive that every person skilled in IP law recognises without need for further explanation or persuasion that the text as amended by the Parliament is rubbish. This will clearly help the EU Commission as well as the EU Council to move this version into the trash bin. Also it can be expected that the U.S. would start formal WTO proceedings against the EU due to violation of Article 27 of the TRIPS agreement if the EU Council should be trapped so much in bewilderment that they eventually conclude to allow this amended draft to enter into force.}

{\it The misinformation campaign staged by the Eurolinux Alliance is really horrendous. The most abhorrent but nevertheless successful tactics instrumentalised by FFII e.V. was the allegation repeated again and again that in particular the EPO has granted tens of thousands of so-called ``software patents'' (i.e. patents on computer-implemented inventions) in contradiction to the law as it is codified by the European Patent Convention (EPC).}
\end{quote}

These patent lawyers are of course exaggerating our success and our efficiency in an attempt to mobilise their colleagues for a backlash.

Yet some basic facts can not be denied:
\begin{itemize}
\item
The European Parliament has voted for a directive text which, if enacted, would effectively protect Europe's software developpers from patents.

\item
Although this is unrelated to questions of open vs closed source and many of our supporters write proprietary software, we were undoubtedly at the core of a large-scale organised effort.  You may want to browse through the swpat.ffii.org website, starting from the news up to September 24th, to get an idea of this.

\item
Those who donated to our campaign got real value for their money.  Please read the text of the amended directive\footnote{http://swpat.ffii.org/papers/europarl0309/index.en.html} and compare it to the text of the European Commission and the FFII Counter Proposal\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prop/index.en.html}.

\item
A backlash from the patent lobby is under way.  The Council is far less friendly territory than the Parliament.  We need a few people doing full-time work, and we will have further costs.

\item
It is time for our supporters in the US and elsewhere to take the software patent issue out of the hands of the patent lawyers and bring it into their parliaments.  The US government has already been abused by the US Patent Office for lobbying against the proposed directive.  Although the US patent lobby's TRIPs charges are legally without any merit, they can serve as a pretext for politicians here to deprive the parliament of its competence in patent policy matters.
\end{itemize}

We will send more detailed calls for action to our supporters and donors within a few days.

\section{Annotated Links}

\begin{itemize}
\item
{\bf {\bf Europarl 2003-09-24: Amended Software Patent Directive\footnote{http://swpat.ffii.org/papers/europarl0309/index.en.html}}}

\begin{quote}
Consolidated version of the amended directive ``on the patentability of computer-implemented inventions'' for which the European Parliament voted on 2003-09-24.
\end{quote}
\filbreak

\item
{\bf {\bf EU Parliament Votes for Real Limits on Patentability\footnote{http://swpat.ffii.org/news/03/plen0924/index.en.html}}}

\begin{quote}
In its plenary vote on the 24th of September, the European Parliament approved the proposed directive on ``patentability of computer-implemented inventions'' with amendments that clearly restate the non-patentability of programming and business logic, and uphold freedom of publication and interoperation.
\end{quote}
\filbreak

\item
{\bf {\bf AIPLA 03-10-06: EU Parliament making software technology unpatentable\footnote{http://swpat.ffii.org/papers/europarl0309/aipla0310/index.en.html}}}

\begin{quote}
US Patent Attorney Erwin Basinski, chairman of the International Affairs Subcommittee of the Electronic \& Computer Law Committee of the American Industria Property Lawyers Association (AIPLA) calls on his colleagues to rise in arms against the European Parliament's amended directive, which would render granted patents invalid and, by excluding what Basinski calls ``software technology'' from patentability, violate Art 27 TRIPs.  Basinski attributes the amendments to the enormous power of the ``opensource lobby''.  Basinski, a specialist in the art of patenting business methods at the European Patent Office and a diplomat with excellent relations to the European Commission, seems pessimistic about the possibility of amending the directive back to what it was.  He predicts that his colleagues will instead work toward having the directive killed by the Council.
\end{quote}
\filbreak

\item
{\bf {\bf US Gov't Promoting Patent Extremism in the European Parliament\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/usrep0309/index.en.html}}}

\begin{quote}
The ``Mission of the United States of America to the European Union'' in Brussels has sent a long paper ``by the US'', titled ``U.S. Comments on the Draft European Parliament Amendments to the Proposed European Union Directive on the Patentability of Computer-Implemented Inventions'' to numerous members of the European Parliament.  ``The US'' warns that Europe might fall afoul of the TRIPs treaty if it passes the proposed directive as amended by the Parliament.  In particular, ``the US'' believes that conversion between patented file formats should generally not be allowed without a license, and therefore demands deletion of Art 6a.  Moreover ``the US'' cites the same BSA studies and the same reasoning as found in the European Commission's directive proposal, and warns that any failure to wholeheartedly endorse patentablity of software in the directive might ``adversely impact certain sectors of the economy'', because ``copyright does not protect the functionality of the software, which is of significant value to the owner'', and that lack of clarity in the concept of ``technical contribution'' would lead to a continued need for negotiations with the US in WIPO and other fora.  This warning comes shortly after a similar letter to MEPs from the UK Government.  It is part of a US Government ``Action Plan'' to ``promote international harmonisation of substantive patent law'' in order to ``strengthen the rights of American intellectual property holders by making it easier to obtain international protection for their inventions''.  This plan has been promoted aggressively by top officials of the US Patent Office in international fora such as WIPO, WSIS and OECD as well as through bilateral negotiations.
\end{quote}
\filbreak

\item
{\bf {\bf Bolkestein's Threats\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/plen0309/deba/index.en.html\#bolk}}}

\begin{quote}
One day before the vote Bolkestein told the MEPs: \begin{quote}
{\it Now I am aware that the large number of amendments to the McCarthy report have been tabled.  Many of those try to re-introduce ideas and themes which were already considered and rejected by the committee during the preparation of the report.  There are some interesting points, but in the main, I am afraid that the majority of those amendments will be unacceptable to the Commission.  And I must confess, to being very concerned about this situation.  Many of these amendments are fundamental, and there is the very real possibility of the failure of the proposal if the parliament chooses to accept them.  If that were to happen, there would I fear be two consequences, neither of which I suspect has been forseen by some mebers of parliament, and neither of which I can only assume would advance the objectives which seem to lie behind a number of amendments.  Firstly, in the complete absence of harmonisation at the level of the community, the European and various national patent offices would be free to continue their current practice of issuing patents for software-implemented inventions which may blur or even cross the line in undermining the exclusion from patentability of software as such under article 52 of the European Patent Convention.  And the result would be not only continuing legal uncertainty and divergence for inventors; but also erode the position which I think almost everyone in this room and above all the Commission itself wants -- namely to maintain the exclusion of pure software from patentability.  That we do not want.   That the proposal rejects.  And secondly, in the absence of harmonisation at Community level, member states would be very likely to pursue harmonisation at the European level instead.  And may I explain what I mean by that remark.  Unlike many fields, patents are unusual in that as a result of the existence of the European Patent Convention, and the creation of the European Patent Office, there already exists a supranational patent system, which covers the whole of the European Union, and indeed beyond, and which can act independently of the Community's legislative process.  Now if we fail in our efforts to achieve a harmonisation of patent law relating to computer-implemented inventions in the European Union, we may well be confronted with a renegotiation of the European Patent Convention. And if I may be blunt, President, the process of renegotiation of the European Patent Convention would not require any contribution from this parliament.  So the situation is clear: there is a single objective but a choice of means.  Either we proceed using the community method, or we take a back seat and watch while member states go via the route of an intergovernmental treaty.  And I think it is clear which route would give European citizens a greater say through this parliament in patent legislation in an area which is so crucial to our economy.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf The TRIPs Treaty and Software Patents\footnote{http://swpat.ffii.org/analysis/trips/index.en.html}}}

\begin{quote}
European patent authorities often cite the TRIPs treaty as a reason for making computer programs and business methods patentable and for making such patents enforcable in the most indecent ways.  This reasoning is fallacious and easy to refute.
\end{quote}
\filbreak

\item
{\bf {\bf Council of the European Union and Software Patents\footnote{http://swpat.ffii.org/players/consilium/index.en.html}}}

\begin{quote}
Together with the European Commission and the European Parliament, the Council is one of the three pillars of the European Union, which jointly legislate in a \emph{co-decision procedure}.  It is a forum where the national governments and their specialised ministries meet.  The question of how to limit patentability is handled in the ``Council Working Party on Intellectual Property and Patents''.  This council has been holding increasingly frequent meetings to discuss the European Commission's proposal for a software patentability directive and come up with a counter-proposal.  The national delegations are mostly composed of national patent office representatives or people whose career path is confined to the national patent establishment and who are factually dependent on this establishment in many ways.  Some delegations, such as the french and belgians, have comprised independent delegates and been fairly critical of the CEC proposal.  Others have been even more pro-patent than the CEC.  All have focussed on textual questions and caselaw rather than on what kind of output they want from the legislation in terms of patents granted/rejected and economic policy objectives.
\end{quote}
\filbreak

\item
{\bf {\bf Software Patent Discussions in and near the European Parliament in 2003\footnote{http://swpat.ffii.org/events/2003/europarl/index.en.html}}}

\begin{quote}
The European Parliament may pass or reject the Software Patentability Directive Proposal of the European Commission immediately after plenary discussion on 2003-09-01.  The most likely course is that it will propose amendments.  Currently many members of the three concerned commissions (juri, itre, cult) have lost confidence in the Newspeak from the European Patent Office (EPO), in which the proposal is written.  We are trying to keep track of the Parliament's schedule and to organise some complementary occasions for an informed discussion.  In fact we want more than that: justice.  The patent lobby has trampled on our rights without justification and is asking MEPs to perpetuate the injustice.  We ask for a fair trial.  Only the European Parliament can offer it.
\end{quote}
\filbreak

\item
{\bf {\bf How you can help us end the software patent nightmare\footnote{http://swpat.ffii.org/group/todo/index.en.html}}}

\begin{quote}
The patent movement has during several decades won the support of large corporations and governments for its expansionist cause.  Rolling trains are hard to halt.  Yet FFII, Eurolinux and others have devoted themselves to this work with considerable success. Still, we continue to have more tasks than free hands.  Here we tell you how you can help us move forward more quickly.
\end{quote}
\filbreak

\item
{\bf {\bf Current Situation in Europe\footnote{http://swpat.ffii.org/news/03/situ0923/index.en.html}}}

\begin{quote}
Situation before the Vote in the European Parliament
\end{quote}
\filbreak

\item
{\bf {\bf FFII: Software Patents in Europe\footnote{http://swpat.ffii.org/index.en.html}}}

\begin{quote}
For the last few years the European Patent Office (EPO) has, contrary to the letter and spirit of the existing law, granted more than 30000 patents on rules of organisation and calculation claimed in terms of general-purpose computing equipment, called ``programs for computers'' in the law of 1973 and ``computer-implemented inventions'' in EPO Newspeak since 2000.  Europe's patent movement is pressing to legitimate this practise by writing a new law.  Although the patent movement has lost major battles in November 2000 and September 2003, Europe's programmers and citizens are still facing considerable risks.  Here you find the basic documentation, starting from the latest news and a short overview.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

