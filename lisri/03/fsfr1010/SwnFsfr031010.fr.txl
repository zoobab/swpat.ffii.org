<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#mWw: We will send more detailed calls for action to our supporters and donors within a few days.

#sWy: It is time for our supporters in the US and elsewhere to take the software patent issue out of the hands of the patent lawyers and bring it into their parliaments.  The US government has already been abused by the US Patent Office for lobbying against the proposed directive.  Although the US patent lobby's TRIPs charges are legally without any merit, they can serve as a pretext for politicians here to deprive the parliament of its competence in patent policy matters.

#Ano: A backlash from the patent lobby is under way.  The Council is far less friendly territory than the Parliament.  We need a few people doing full-time work, and we will have further costs.

#aae: Those who donated to our campaign got real value for their money.  Please read the %(ad:text of the amended directive) and compare it to the %(ot:text of the European Commission and the FFII Counter Proposal).

#dIy: Although this is unrelated to questions of open vs closed source and many of our supporters write proprietary software, we were undoubtedly at the core of a large-scale organised effort.  You may want to browse through the swpat.ffii.org website, starting from the news up to September 24th, to get an idea of this.

#ieW: The European Parliament has voted for a directive text which, if enacted, would effectively protect Europe's software developpers from patents.

#idi: Yet some basic facts can not be denied:

#rtf: These patent lawyers are of course exaggerating our success and our efficiency in an attempt to mobilise their colleagues for a backlash.

#sai: The misinformation campaign staged by the Eurolinux Alliance is really horrendous. The most abhorrent but nevertheless successful tactics instrumentalised by FFII e.V. was the allegation repeated again and again that in particular the EPO has granted tens of thousands of so-called %(q:software patents) (i.e. patents on computer-implemented inventions) in contradiction to the law as it is codified by the European Patent Convention (EPC).

#rue: The Eurolinux activists clearly have overshot their mark: They have managed to bring so many of their preferred amendments in a redundant, self-contradictory and amateurish fashion into the wording of the Directive that every person skilled in IP law recognises without need for further explanation or persuasion that the text as amended by the Parliament is rubbish. This will clearly help the EU Commission as well as the EU Council to move this version into the trash bin. Also it can be expected that the U.S. would start formal WTO proceedings against the EU due to violation of Article 27 of the TRIPS agreement if the EU Council should be trapped so much in bewilderment that they eventually conclude to allow this amended draft to enter into force.

#WdD: After having studied the result of yesterday's voting of the European Parliament I must say that the situation is much more worse than I had feared before. The gist of what the directive says is that preferably no patents should be granted on anything that is related to computers, and if under some narrow preconditions exceptionally a patent is ever granted there are such burdens put upon the applicant that it would be rendered virtually worthless. For example, Article 2, point (bb) of the Directive now narrows the term %(q:industry) to the %(q:automated production of material goods). This is simply absurd.

#rle: The users of the patent system have just suffered a severe political defeat, even if the Council manages to rectify the current situation.  This was probably the very first time since the patent disputes of the nineteenth century that patent law was in the headlines. The controversy on patents will continue, that is for sure.

#hWr: Another patent attorney %(er:reports):

#dtW: My sources on the Commission are trying to determine what to do.  They believe the Council will reject this version and send it back to Parliament for a second reading (vote) but the question is whether it can be saved at all.  I think our hope is it will either just die or be withdrawn by the Commission.

#c43: As to the amendments themselves, to me the most onerous are %(am:32) (which is just incorrect); %(am:95) (which would put the EPO out of business); %(am:84); %(a2:all of the amendments to Article 2) (which would negate any software related invention); %(am:45) (makes no sense when you consider the billions of dollars/euros invested in the financial/banking/stock market and related industries to make those services function); %(am:70); %(am:60); %(am:102) and %(am:111); %(am:72); %(am:103) and %(am:119); %(am:104) and %(am:120); %(am:76) (reworded Article 6a is still destructive of most existing software patents by itself).

#mWf: The apparent influence of the open source community on the members of Parliament and the Parliament's general apparent lack of understanding of the technological and business advances resulting from the current and predicted use of computer related inventions, are truly remarkable and illustrate the political nature of the problems.

#tsa: The EU Parliament recently voted on the Proposed Software Directive. The text as amended (a copy is attached) appears to completely eliminate any software patent and make unenforceable most existing patents.

#etu: US patent attorney Erwin Basinski %(eb:reports) to AIPLA, the patent lawyer lobby organisation, about what he -- not quite incorrectly -- sees as the success of our work in the European Parliament:

#Am76P1: Les États membres veillent à ce que, lorsque le recours à une  technique brevetée est nécessaire à une fin significative, par exemple  pour assurer la conversion des conventions utilisées dans deux systèmes  ou réseaux informatiques différents, de façon à permettre entre eux la  communication et l'échange de données, ce recours ne soit pas considéré  comme une contrefaçon de brevet.

#Am103: Les États membres veillent à ce que la production, la  manipulation, le traitement, la distribution et la publication de  l'information, sous quelque forme que ce soit, ne puisse jamais  constituer une contrefaçon de brevet, directe ou indirecte, même  lorsqu'un dispositif technique est utilisé dans ce but.

#Am45: Les États membres veillent à ce que le traitement des données ne  soit pas considéré comme un domaine technique au sens du droit des  brevets et à ce que les innovations en matière de traitement des données  ne constituent pas des inventions au sens du droit des brevets.

#Am38: %(q:industrie), au sens du droit des brevets, signifie  %(q:production automatisée de biens matériels);

#Am97: %(q:domaine technique) désigne un domaine industriel d'application  nécessitant l'utilisation de forces contrôlables de la nature pour  obtenir des résultats prévisibles. %(q:Technique) signifie  %(q:appartenant à un domaine technique). L'utilisation de forces de la  nature pour contrôler des effets physiques au delà de la représentation  numérique de l'information appartient à un domaine technique. La  production, la manipulation, le traitement, la distribution et la  présentation de l'information n'appartiennent pas à un domaine  technique, même si des dispositifs techniques sont utilisés dans ce but.

#Am107: %(q:contribution technique) , également appelée %(q:invention),  désigne une contribution à l’état de la technique dans un domaine  technique. Le caractère technique de la contribution est une des quatre  conditions de la brevetabilité. En outre, pour mériter un brevet, la  contribution technique doit être nouvelle, non évidente et susceptible  d'application industrielle.

#Am32: En vertu de la Convention sur la délivrance de brevets européens  signée à Munich, le 5 octobre 1973, et du droit des brevets des États  membres, les programmes d’ordinateurs ainsi que les découvertes,  théories scientifiques, méthodes mathématiques, créations esthétiques,  plans, principes et méthodes dans l’exercice d’activités  intellectuelles, en matière de jeu ou dans le domaine des activités  économiques et les présentations d’informations, ne sont pas considérés  comme des inventions et sont donc exclus de la brevetabilité. Cette  exception s’applique parce que lesdits objets et activités  n’appartiennent à aucun domaine technique.

#tea: The %(ad:amended directive) speaks for itself.  Art 1-6 exclude software from patentability in multiple ways and form a cohesive whole, which, in contrast to the European Commission's illogical proposal, is completely congruent with Article 52 of the Euroepan Patent Convention and Art 27 of the TRIPs treaty.  Please read the whole text carefully.  Here we just have room to cite a few passages:

#descr: The European Parliament voted on September 24th for a directive proposal which confirms the existing European law, makes software explicitely unpatentable and codifies additional safeguards, such as freedom of publication and interoperation.  The amended directive proposal thereby achieves the claimed aims of the European Commission, especially %(q:harmonisation and clarification of the status quo) and %(q:prevention of a drift toward US-style patentability of pure software and business methods).  However, the European Commission doesn't seem to be happy.  Internal Market Commissioner Frits Bolkestein and others have been threatening to withdraw the directive project and to pass the ball back to national patent administrators and, should that fail, to rely on brotherly help from Washington.  But the European Parliament was neither deceived nor intimidated.  Now the patent movement's strategy is to dismiss the Parliament's position as %(q:unworkable) and to attribute it to %(q:ignorance) rather than to a conscious policy decision.  Bolkestein's friends can be counted on to resort to whatever inconsistency, illoyalty or illegality is necessary in order to obtain what they really want: %(q:legal security) for the owners of more than 30,0000 US-style patents on software and business methods, granted in accordance with a law-to-be, which the European Parliament has refused to pass for them.  A few months of intense struggle lie ahead.

#title: Brevets logiciels : une percée au Parlement européen

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: ccorazza ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwnFsfr031010 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

