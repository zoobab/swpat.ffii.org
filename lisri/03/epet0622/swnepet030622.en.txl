<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#nlr: This study, conducted by a Finnish university on behalf of the European Commission in 2003, may help explain why so many employees of telecom giants pronounce themselves against software patentability.

#aos: classified statements from software professionals, business people, scientists, politicians etc

#srt: contains new statements from supporters of the petition

#oWW: More Contacts to be supplied upon request

#iie: Zurich Technical University

#hie: Aachen Technical University

#cmn: comany

#rwe: Below you find the number of employees of some major companies and institutions who signed the petition.  Note that 2/3 of the signatories did not specify their employer.

#nme2: number

#onr: country

#lwe: lawyer

#rss: professor (mainly computer science, mathematics, physics)

#ere: scientist, researcher etc

#rcg: programmer, software/system architect/engineer/designer/analyst

#WIo: CTO, head of IT, head of R&D

#Wui: company owner, chief executive (CEO), managing director

#iiu: minimum

#nme: number

#rei: position

#Wty: Since the wording in the entry %(q:profession) is free for everyone to chose, aggregation was difficult.  We can only state minimal numbers here.

#yoa: By company

#yot: By country

#yos: By profession / position in company

#WWt: For more statements, please read our documentation %(DOC).

#hee: The European Parliament parliament's plenary will decide on a draft report on the European Commission's software patentability directive proposal COM(2002)92 on monday, June 30th.  Hartmut Pilch explains on behalf of Eurolinux, an alliance of associations and companies from all European countries and all sectors of the European software industry: %(bc:If the European Parliament accepts this report, even with amendments, it will not only find itself in complete contradiction with public opinion, as expressed in the largest online petition on IT matters which the world has seen so far.  It would also be in contradiction with its own proclaimed aims.  The result of passing the McCarthy Directive Proposal would be that %(q:Amazon One Click Shopping) indisputably becomes a patentable invention, and that more than 20000 broad and trivial software and business method patents, which have been granted by the %(tp|European Patent Office|EPO) against the letter and spirit of the written law, will no longer be contestable in court, except with rock-solid evidence of prior art.) Reinier Bakels from the Insitute for Information Law of Amsterdam University, author of a %(dg:study) on the software patent directive, says: %(bc:Now it is up to the European Parliament to decide on a highly controversial proposal for a directive for software patents. The JURI proposal aims at improving clarity. It should have at last defined clearly what is patentable and what not. But in reality it is a bunch of magic formulas that even legal experts do not understand. In particular for small and medium sized software developers it is a disaster. A patent infringement claim can ruin such a company.  It is to be hoped that the European Parliament understands this if they vote about the proposed directive in a few days.) Dr. Karl Friedrich Lenz, professor of European Law in Tokyo, comments: %(bc:If the European Parliament follows JURI in ignoring public opinion and all scientific studies, we will see large license payments from the European to the American software industry, lots of litigation based on software patents, Internet patents and business method patents, and some very unfavorable effects for open source software. And introducing a large number of new monopoly rights in the information society sector certainly won't help with the EU strategic goal %(q:to become the most competitive and knowledge-based economy in the world.))

#dokurl: Permanent URL of this Press Release

#ffii: About the FFII

#eurolinux: About the Eurolinux Alliance

#eWt: Media Contacts

#tWn: Statistics of signatories

#title: Vote in 8 days: 2000 IT bosses ask European Parliament to say NO to software patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnepet030622 ;
# txtlang: en ;
# multlin: t ;
# End: ;

