<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#nlr: Diese im Jahre 2002 im Auftrag der Europäischen Kommission von einer finnischen Universität erstellte Studie könnte erklären, warum so viele Mitarbeiter von Telekommunikations-Großunternehmen sich gegen den Softwarepatente aussprechen.

#aos: klassifizierte Stellungnahmen von Software-Experten, Wirtschaftsfachleuten, Wissentschaftlern, Politikern, usw.

#srt: beinhaltet neue Stellungnahmen von Unterstützern der Petition

#oWW: Weitere Kontakte auf Anfrage

#iie: Technische Universität zu Zürich

#hie: Technische Universität zu Aachen 

#cmn: Unternehmen

#rwe: Untenstehend finden Sie die Anzahl der Mitarbeiter einiger großer Unternehmen und Institutionen, die unsere Petition unterzeichnet haben.  Bitte beachten Sie, daß 2/3 der Unterzeichner Ihren Arbeitgeber nicht angegeben haben.

#nme2: Anzahl

#onr: Land

#lwe: Anwalt

#rss: Professor (vor allem Informatik, Mathematik, Physik)

#ere: Wissentschaftler, Forscher, etc

#rcg: Programmierer, Software/System-Architekt/Ingenieur/Designer/Analyst

#WIo: Chefentwickler, IT-Leiter, Leiter v. Forschung & Entwicklung

#Wui: Unternehmseigentümer, Geschäftsführer, Vorstandsvorsitzender

#iiu: (mindestens)

#nme: Anzahl

#rei: Position

#Wty: Da die Bezeichnung im Feld %(q:Beruf) für jeden frei wählbar war, gestaltete sich die Zusammenfassung recht schwierig.  Deshalb können wir hier nur Mindestzahlen nennen.

#yoa: Nach Unternehmen

#yot: Nach Land

#yos: Nach Aufgabe / Stellung im Unternehmen

#WWt: Weitere Stellungnahmen entnehmen Sie bitte unserer Dokumentation %(DOC).

#hee: Das Plenum des Europäischen Parlaments wird über diesen Entwurf der Europäischen Komission zu einem Richtlinienvorschlag zur Patentierbarkeit von Software COM(2002)92 am Montag den 30. Juni abstimmen.  Hartmut Pilch erklärt als Vertreter von Eurolinux  - einer Allianz von Organisationen und Unternehmen aus allen Staaten Europas und allen Sektoren der europäischen Software-Industrie: %(bc:Sollte das Europäische Parlament diesen Report annehmen, selbst mit den Anmerkungen, wird es nicht nur im völligen Widerspruch mit der öffentlichen Meinung befinden, wie die größte Online-Petition zu IT-Fragen gezeigt hat, die die Welt je gesehen hat.  Es wäre ebenso im völligen Widerspruch mit seinen selbst proklamierten Absichten.  Das Resultat des Passierens von McCarthy's Richtlinienvorschlag wäre, daß %(q:Amazon's Ein-Click-Einkauf) unzweifelhaft zu einer patentierbaren Erfindung wird, und so werden auch mehr als 20000 breite und triviale Software-Patente und Patente auf Geschäftsmethoden, welche vom %(tp|European Patent Office|EPO) gegen Buchstaben und Geist des geschriebenen Gesetzes erteilt wurden, nicht mehr vor Gericht anfechtbar, außer einer felsenfesten Nachweisbarkeit von Prior Art.) Reinier Bakels vom Institut für Informationsrecht an der Universität Amsterdam, Author einer %(dg:Studie) über die Direktive zu Software-Patenten, sagt: %(bc:Nun ist es am Europäischen Parlament, über ein hochumstrittenen Entwurf für eine Richtlinie zu Software-Patenten abzustimmen. Der JURI-Entwurf bebsichtigt, die Klarheit zu verbessern. Es hätte dabei zumindest klar definieren sollen, was patentierbar ist, und was nicht. In Wirklichkeit jedoch ist es eine Sammlung von magischen Formeln, die nichtmal Rechtsexperten verstehen. Vor allem für kleine und mittelgroße Software-Entwickler ist das ein Disaster. Ein Patentstreit kann ein solches Unternehmen ruinieren.  Es bleibt zu hoffen, daß das Europäische Parlament versteht das, wenn sie in wenigen Tagen über die vorgeschlagene Richtline abstimmen.) Dr. Karl Friedrich Lenz, Professor für europäisches Recht in Tokyo, kommentiert: %(bc:Sollte das Europäische Parlament JURI folgen und die öffentliche Meinung und alle wissentschaftlichen Studien ignorieren, werden wir horente Lizenzzahlungen von der europäischen Software-Industrie an die Amerikanische sehen, viele Prozeße wegen Software-Patenten, Internet-Patenten und Geschäftsmethoden, und einie Reihe nicht wünschenswerte Effekte auf Open-Source-Software. Und eine große Menge an neuen Monopolrechten im Sektor der Informationsgesellschaft einzuführen, wird bestimmt nicht bei dem strategischen EU-Ziel helfen, %(q:die am meisten wettbewerbsfähigste und wissensbasierte Wirtschaft der Welt zu werden.))

#dokurl: Permanente URL dieser Pressemitteilung

#ffii: Über den FFII

#eurolinux: Über die Eurolinux-Allianz

#eWt: Pressekontakt

#tWn: Statistiken der Unterschriften

#title: Abstimmung in 8 Tagen: 2000 IT-Arbeitgeber fordern das Europäische Parlament auf, NEIN zu Software-Patenten zu sagen

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: eweigelt ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnepet030622 ;
# txtlang: de ;
# multlin: t ;
# End: ;

