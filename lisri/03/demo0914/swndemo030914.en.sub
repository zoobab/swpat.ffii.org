\begin{subdocument}{swndemo030914}{Action Week against EU Software Patent Plans}{http://swpat.ffii.org/journal/03/demo0914/index.en.html}{Workgroup\\swpatag@ffii.org}{The Proposal for a software patent directive, which will be submitted to the European Parliament for decision on September 24th, is giving rise to yet another wave of protests. These include a conference in Brussels on Wednesday September 17th, a rally in Strasbourg on Tuesday September 23nd, as well as a series of ``satellite demos'' in other cities of Europe. These actions will be accompanied by an Internet Strike on the 17th and 23rd.  At a comparable action on Aug 27, 500 demonstrators came to Brussels and 3000 websites went on strike.}
\begin{sect}{detal}{details}
The conference in the European Parliament on Wednesday the 17th of September\footnote{http://swpat.ffii.org/events/2003/europarl/09/bruxelles/index.en.html} is organised by the Greens/EFA group.  Among the speakers are prominent scientists and entrepreneurs who have recently signed appeals against the proposed software patent directive, as well as Tim Berners-Lee, director of the World Wide Web Consortium (W3C) and ``inventor'' of the Web.

H\aa{}kon Wium Lie, CTO of the browser creator Opera Inc and member of W3C, explains what is at stake for the Web: \begin{quote}
{\it While Tim Berners-Lee is struggling to keep Web standards free from patent disasters, some people in the European Parliament are advertising these same disasters as success stories of the patent system.  What's worse, there is a strong movement to demolish the only achievement of the Parliament so far, namely Art 6a, which allows free conversion between data formats.}
\end{quote}

Wium Lie may be referring to the Eolas patent on browser extensions\footnote{http://swpat.ffii.org/patents/effects/eolas/index.en.html} and letters sent out by Dr. Joachim Wuermeling\footnote{http://swpat.ffii.org/players/jwuermeling/index.en.html} and other german conservative MEPs, and to papers by Arlene McCarthy, backed by the UK government\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/ukrep0309/index.en.html} and others, including Wuermeling, which call for suppression or demolition of Art 6a\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/itop/index.en.html}.

Daniel Cohn-Bendit, president of the Greens/EFA group in the European Parliament, explained to FFII in an interview: \begin{quote}
{\it This is the third conference on software patents which the Greens/EFA have organised in the European Parliament.  Meanwhile many MEPs within the two big party groups, the conservatives and socialists, are no longer blindly following the designated ``patent experts'' of their party groups.  By failing to provide any serious analysis of interests and choices, these so-called ``experts'' have drawn sharp criticism from prominent economists and computer scientists, whom we are inviting to our conference.  We hope that many MEPs from all groups will listen.}
\end{quote}

The event in Strasbourg on Tuesday 23rd of September\footnote{http://swpat.ffii.org/events/2003/europarl/09/strasbourg/index.en.html}, organised by local groups together with FFII/Eurolinux, is structured as follows:

\begin{center}
\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
When & Where & Subject\\\hline
11.00-11.30 & Place Kl\'{e}ber & Rally in the streets of Strasbourg marching to the European Parliament\\\hline
12.30-14.00 & EP & Demonstration in front of the European Parliament with performance, balloons, patent chain, speeches.\\\hline
\end{tabular}
\end{center}
\end{center}

``On August 27th, a rally in and near the European Parliament in Brussels attracted 500 participants. Leaders of the scientific communities and software business world have taken the directive proposal apart and condemned it in every respect during the last few mongth.  This has however left some influential groups in the Parliament unimpressed.  It still seems likely that the Parliament will pass a directive on September 24th which renders patents on algorithms and business methods, such as Amazon One Click Shopping, legal and uniformly enforcable in Europe\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/tech/index.en.html},'' explains Guy Brand, organiser of the Strasburg rally. ``More and more people are now seeing this very clearly. We expect even more participants this time.''

Satellite demonstrations are planned in Munich\footnote{http://swpat.ffii.org/events/2003/europarl/09/muenchen/index.de.html}, Berlin\footnote{http://swpat.ffii.org/events/2003/europarl/09/SwpParlBer039/index.de.html}, Vienna\footnote{http://swpat.ffii.org/events/2003/europarl/09/wien/index.de.html} and elsewhere.  The Munich demonstration begins on Friday the 19th of September 15.30 in front of the European Patent Office and procedes from there via the Bavarian Government to a central square in the city, where it ends at 18.00.  Speeches are held by leading local politicians, who criticise Joachim Wuermeling and his party, the ruling Christian Social Union (EPP), for its irresponsible and incompetent treatment of fundamental economic policy questions.  The actions in Berlin and Vienna will target the departments of the German and Austrian governments which have been instrumental in promoting program claims and other harmful policies in the European Council\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/dkpto0209/index.en.html}.  Further local actions are continuing to pop up.  Patrick Fromberg, a software developper in Munich who is organising the activity there, explains: ``We hope to help this grow into a popular movement, so that national parliaments will feel more inclined to take a close look at the back-room activities of their government's patent administrators when the directive proposal comes to the European Council.''

``The vast majority of our supporters will certainly not be on Place Kl\'{e}ber on September 23nd, or at any of the satellite demonstrations. Those who can not come to Strasbourg should demonstrate online, using their web servers or other internet services'', says Hartmut Pilch, president of FFII. ``We have proposed a series of ways in which this can be done. There is certainly a way for everyone. Better make access to your webpage a bit more difficult now for one or two days than lose your freedom of publication for the next ten years.  If the rapporteurs of the big party groups have their way, copyright and freedom of publication will become worthless. Programmers and Internet Service Providers may become regular targets for cease-and-desist letters and patent lawsuits.  If the JURI report is not drastically amended, paragraph by paragraph, we will be stuck with a system of unlimited patentability of software and business methods in Europe for the next 10 years, and our software industry will be at the mercy of a few large companies, mostly of US and Japanese origin, who hold 2/3 of the software and business method patents which the European Patent Office (EPO) has been illegally granting since 1986.  The deadline for democratic scrutiny is scheduled for September 24th, and the Action Week may well be your last chance to make your voice heard in the European patent decisionmaking process.''
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf 2003/09 EP: Software Patent Directive Vote\footnote{http://swpat.ffii.org/events/2003/europarl/09/index.en.html}}}

\begin{quote}
The European Parliament is faced with a proposal which, while trying to sound harmless, would make algorithms and business methods patentable.  If the Parliament approves this proposal on 2003/09/23 in Strasburg, chances for democratic decisionmaking on software patents in Europe may be remote for a long time to come.  FFII and others are preparing detailed analyses and voting recommendations and demonstrating in various cities, including Munich, Vienna, Berlin, Stuttgart and Strasbourg.  In Vienna and Munich 300 people participated.
\end{quote}
\filbreak

\item
{\bf {\bf Strasburg LUG: Patent Pages\footnote{http://strasbourg.linuxfr.org/pat/}}}

\begin{quote}
Software Patent Activities of Linux users and others in Strasburg.  This contains a press release for 2003/09/23.
\end{quote}
\filbreak

\item
{\bf {\bf AEL Wiki: Online Demo Planning\footnote{http://wiki.ael.be/index.php/OnlineDemo}}}

\begin{quote}
Planning Details about the Online Demonstrations of 17th and 23rd.  World-editable webspace provided by AEL.be.

see also Strasbourg 2003/09/23\footnote{http://swpat.ffii.org/events/2003/europarl/09/strasbourg/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf AEL Wiki: Strasburg Event Planning\footnote{http://wiki.ael.be/index.php/EventStrasbourg}}}

\begin{quote}
Planning Details about the demonstration in Strasburg 2003/09/23.  World-editable webspace provided by AEL.be.

see also Strasbourg 2003/09/23\footnote{http://swpat.ffii.org/events/2003/europarl/09/strasbourg/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf EU Software Patent Plans Shelved Amid Massive Demonstrations\footnote{http://swpat.ffii.org/journal/03/demo0827/index.en.html}}}

\begin{quote}
On Aug 28th, the European Parliament postponed its vote on the proposed EU Software Patent Directive.  The day before, approximately 500 persons had gathered for a rally beside the Parliament in Brussels, accompanied by an online demonstration involving more than 2000 websites.  The events in and near the Parliament were reported extensively covered in the media, including tv and radio, all over Europe and beyond.  Within a few days, the petition calling the European Parliament to reject software patentability accumulated 50,000 new signatures.
\end{quote}
\filbreak

\item
{\bf {\bf Online Demonstration Against Software Patents\footnote{http://swpat.ffii.org/group/demo/index.en.html}}}

\begin{quote}
We can show our concern by physical presence as well as by more or less gently blocking access to webpages in a concerted manner at certain times.
\end{quote}
\filbreak

\item
{\bf {\bf JURI votes for Fake Limits on Patentability\footnote{http://swpat.ffii.org/journal/03/juri0617/index.en.html}}}

\begin{quote}
The European Parliament's Committee for Legal Affairs and the Internal Market (JURI) voted on tuesday morning about a list of proposed amendments to the planned software patent directive.  It was the third and last in a series of committee votes, whose results will be presented to the plenary in early september.  The other two commissions (CULT, ITRE) had opted to more or less clearly exclude software patents.  The JURI rapporteur Arlene McCarthy MEP (UK socialist) also claimed to be aiming for a ``restrictive harmonisation of the status quo'' and ``exclusion of software as such, algorithms and business methods from patentability''.  Yet McCarthy presented a voting list to fellow MEPs which, upon closer look, turns ideas like ``Amazon One-Click Shopping'' into patentable inventions.  McCarthy and her followers rejected all amendment proposals that try to define central terms such as ``technical'' or ``invention'', while supporting some proposals which reinforce the patentability of software, e.g. by making publication of software a direct patent infringment, by stating that ``computer-implemented inventions by their very nature belong to a field of technology'', or by inserting new economic rationales (``self-evident'' need for Europeans to rely on ``patent protection'' in view of ``the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union'') into the recitals.  Most of McCarthy's proposals found a conservative-socialist 2/3 majority (20 of 30 MEPs), whereas most of the proposals from the other committees (CULT = Culture, ITRE = Industry) were rejected.  Study reports commissioned by the Parliament and other EU institutions were disregarded or misquoted, as some of their authors point out (see below).  A few socialists and conservatives voted together with Greens and Left in favor of real limits on patentability (such as the CULT opinion, based on traditional definitions, that ``data processing is not a field of technology'' and that technical invention is about ``use of controllable forces of nature''), but they were overruled by the two largest blocks.  Most MEPs simply followed the voting lists of their ``patent experts'', such as Arlene McCarthy (UK) for the Socialists (PSE) and shadow rapporteur Dr. Joachim Wuermeling (DE) for the Conservatives (EPP).  Both McCarthy and Wuermeling have closely followed the advice of the directive proponents from the European Patent Office (EPO) and the European Commission's Industrial Property Unit (CEC-Indprop, represented by former UK Patent Office employee Anthony Howard) and declined all offers of dialog with software professionals and academia ever since they were nominated rapporteurs in May 2002.
\end{quote}
\filbreak

\item
{\bf {\bf Why Amazon One Click Shopping is Patentable under the Proposed EU Directive\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/tech/index.en.html}}}

\begin{quote}
According to the European Commission (CEC)'s Directive Proposal COM(2002)92 for ``Patentability of Computer-Implemented Inventions'' and the revised version approved by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI), algorithms and business methods such as Amazon One Click Shopping are without doubt patentable subject matter. This is because \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO).  The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf Interoperability and the Software Patents Directive: What Degree of Exemption is Needed\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/itop/index.en.html}}}

\begin{quote}
Art 6 of the proposed software patent directive pretends to impose a limit on patent enforcement to safeguard interoperability.  Art 6a, which was inserted by the European Parliament and approved by all three concerned committees, actually does impose a gentle but real limit.  It says that filters for conversion from one format to another may always be used, regardless of patents.   Unfortunately even this limit has provoked a furious backlash from corporate patent lawyers, seconded by large IT associations and governments (whose patent policy is usually formulated by corporate patent lawyers).  These patent lawyers are spreading fear propaganda about the implications of Art 6a and pressing hard to have it removed.  After the summer pause of 2003, Arlene McCarthy proposed an amendment to Art 6a which would render Art 6a meaningless.  We explain the meaning of Art 6a and the different amendments under discussion.
\end{quote}
\filbreak

\item
{\bf {\bf Program Claims: Bans on Publication of Patent Descriptions\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prog/index.en.html}}}

\begin{quote}
Patent Claims to ``computer program, characterised by that upon loading it into memory [ some process ] is executed'', are called ``program claims'', ``Beauregard claims'', ``In-re-Lowry-Claims'', ``program product claims'', ``text claims'' or ``information claims''.  Patents which contain these claims are sometimes called ``text patents'' or ``information patents''.  Such patents no longer monopolise a physical object but a description of such an object.  Whether this should be allowed is one of the controversial questions in the struggle about the proposed EU Software Patent Directive.  We try to explain how this debate emerged and what is really at stake.
\end{quote}
\filbreak

\item
{\bf {\bf Call for Action\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/demands/index.en.html}}}

\begin{quote}
The European Commission's proposal for the patentability of software innovations requires a clear response from the European Parliament, the member state governments and other political players. Here is what we think should be done.
\end{quote}
\filbreak

\item
{\bf {\bf Eurolinux Petition for a Software Patent Free Europe\footnote{http://petition.eurolinux.org/}}}
\filbreak

\item
{\bf {\bf FFII: Software Patents in Europe\footnote{http://swpat.ffii.org/index.en.html}}}

\begin{quote}
For the last few years the European Patent Office (EPO) has, contrary to the letter and spirit of the existing law, granted more than 30000 patents on rules of organisation and calculation claimed in terms of general-purpose computing equipment, called ``programs for computers'' in the law of 1973 and ``computer-implemented inventions'' in EPO Newspeak since 2000.  Europe's patent movement is pressing to legitimate this practise by writing a new law.  Although the patent movement has lost major battles in November 2000 and September 2003, Europe's programmers and citizens are still facing considerable risks.  Here you find the basic documentation, starting from the latest news and a short overview.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{contact}
\begin{description}
\item[mail:]\ media at ffii org
\item[phone:]\ Guy Brand +33-3-90245102 (Strasburg)
Benjamin Henrion 0032-10-454779 (Brussels)
Patrick Fromberg +49-89-500777-71 (Munich)
Hartmut Pilch +49-89-18979927 (Munich)
More Contacts to be supplied upon request
\end{description}
\end{sect}

\begin{sect}{url}{Permanent URL of this Press Release}
http://swpat.ffii.org/journal/03/demo0914/index.en.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

