<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#oWW: Autres contacts fournis sur demande

#WWt: Planning Details about the demonstration in Strasburg 2003/09/23.  World-editable webspace provided by AEL.be.

#iul: AEL Wiki: Strasburg Event Planning

#Air: contient des annonces de presse concernant la manif du 2003/09/23.

#aGW: Strasburg LUG: Patent Pages

#eia: %(q:La vaste majorité de nos supporters ne sera certainement pas sur la Place Kléber le 23 septembre, ou aux autres manifestations. Ceux qui ne viendront pas à Strasbourg pourront manifester en ligne, par l'intermédiaire de leur serveur web ou d'autres services Internet), selon Hartmu Pilch, président de la FFII. %(q:Nous avons proposé une série de moyens pour ce faire. Il y a certainement un moyen pour chacun. Il est préférable de rendre l'accès à votre page d'accueil un peu plus difficile pour un ou deux jours, plutôt que de perdre votre liberté de publication pour les dix prochaines années. Si les rapporteurs des groupes des grands partis l'emportent, le droit d'auteur et la liberté de publication seront nuls et non avenus. Les programmeurs et les fournisseurs d'accès Internet deviendront régulièrement la cible de poursuites pour infraction de brevet. Si le rapport du JURI n'est pas drastiquement amendé, paragraphe par paragraphe, nous serons emprisonnés dans un système de brevetabilité illimitée des programmes et méthodes organisationnelles pendant les dix prochaines années, et notre industrie du logiciel sera à la merci de quelques grosses sociétés, principalement américaines et japonaises, qui possèdent deux tiers des brevets logiciels que l'%(tp|Office Européen des Brevets|OEB) a accordés, illégalement, depuis 1986. La date butoir pour l'examen démocratique a été fixée au 24 septembre, et la Semaine d'action pourrait bien être votre dernière chance de faire entendre votre voix dans le processus de décision européen sur les brevets.

#Weh: Des manifestations auxiliaires se dérouleront à %(MUC), %(BER), %(VIE) et ailleurs. La manifestation de Munich commencera le vendredi 19 septembre à 15h30 en face de l'Office Européen des Brevets, et se dirigera, en passant devant le siège du gouvernement bavarois, vers une place centrale de la ville, où ell s'achèvera vers 18h00. Des dirigeants politiques locaux prononceront des discours critiquant Joachim Wuermeling et son parti, l'Union sociale-démocrate (EPP) au pouvoir, pour son traitement irresponsable et incompétent de questions fondamentales de la politique économique. Les actions à Berlin et Vienne viseront les départements des gouvernements allemand et autrichien %(bm:qui ont activement contribué à la promotion de la brevetabilité logicielle et à d'autres politiques nuisibles au Conseil de l'Europe). De nouvelles initiatives locales continuent d'apparaître. Patrick Fromberg, un développeur de Munich qui organise les activités là-bas, explique: %(q:Nous espérons faire en sorte que ceci devienne un mouvement populaire, afin que les parlements nationaux soient enclins à examiner de plus près les agissements des administrateurs en brevets de leurs gouvernements, au moment où la proposition atteindra le Conseil de l'Europe.)

#tno: %(q:Le 27 août, une manifestation dans et aux alentours du Parlement européen, à Bruxelles, a réuni 500 participants. Les dirigeants des communautés scientifiques et du monde du développement logiciel ont condamné la directive dans tous ses aspects au cours des derniers mois. Cependant, certains groupes influents du Parlement n'ont guère été impressionnés. Il est encore possible que le 24 septembre, le Parlement approuvera une directive %(et:légalisant les brevets sur les algorithmes et méthodes organisationnelles, tels %(q:Amazon One Clock Shopping), rendant ceux-ci uniformément exécutoires dans toute l'UE),) comme l'explique Guy Brand, organisateur de la manifestation de Strasbourg. %(q:Un public toujours plus nombreux se rend très clairement compte de cela. Nous nous attendons à une participation plus grande encore cette fois-ci.)

#kitem: Sujet

#kilok: Où

#kiam: Quand

#gWa: L'événenement de Strasbourg du mardi 23 septembre, organisé par des groupes locaux en collaboration avec la FFII/Eurolinux, se déroulera comme suit:

#sWy: Daniel Cohn-Bendit, président du groupe Verts/EFA au Parlement européen, a expliqué lors d'une interview pour la FFII: %(bc:C'est la troisième conférence sur les brevets logiciels que les Verts/EFA ont organisé au Parlement européen. Entretemps, de nombreux membres du Parlement au sein des groupes des deux grands partis, ne suivent plus aveuglément les %(q:experts) désignés de leur groupes. N'ayant pu fournir aucune analyse sérieuse, ces soi-disant %(q:experts)  se sont attirés les vives critiques d'économistes éminents et d'informaticiens, que nous avons invités à notre conférence. Nous espérons que de nombreux parlementaires viendront les écouter.)

#spl: Wium Lie semble se référer au %(eo:brevet Eolas sur les extensions pour navigateurs Internet) et aux lettres issues du %(jw:Dr. Joachim Wuermeling) et d'autres membres conservateurs allemands du Parlement européen, ainsi qu'à des articles rédigés par Arlene McCarthy, soutenue par le %(ug:gouvernement britannique) et d'autres, qui %(ei:appelle à la suppression ou au démantèlement de l'Article 6a).

#reo: Håkon Wium Lie, architecte du navigateur Opera et membre du W3C, explique les enjeux pour le Web: %(bc: Alors que Tim Berners-Lee lutte pour protéger les standards du Web des désastres engendrés par les brevets logiciels, des gens au sein du Parlement européen présentent ces mêmes désastres comme des succès du système des brevets. Pire encore, il existe une volonté forte de détruire le seul accomplissement du Parlement juqu'à présent, à savoir l'Article 6a, qui permet la libre conversion entre formats de données.)

#aer: La %(sb:conférence du mervredi 17 septembre au Parlement européen) est organisée par le groupe des Verts/EFA. Parmi les orateurs, on retrouvera des chercheurs éminents et des entrepreneurs qui ont récemment signé des appels contre la proposition de directive sur le brevets logiciels, ainsi que Tim Berners-Lee, directeur du World Wide Web Consortium (W3C) et %(q:inventeur) du Web.

#woa: La proposition de directive sur les brevets logiciels, qui sera soumise à la décision du Parlement européen le 24 septembre prochain, provoque une nouvelle vague de protestations. Parmi celles-ci, on comptera une conférence à Bruxelles le mercredi 17 septembre, une grande manifestation à Strasbourg le mardi 23 septembre, ainsi qu'une série de %(q:manifestations auxiliaires) dans d'autres villes d'Europe. Ces actions seront accompagnées par une grève Internet les 17 et 23 septembre. Le 27 août dernier, 500 manifestants s'étaient réunis à Bruxelles et 3000 sites Internet avaient débrayé lors d'une action similaire.

#tta: Semaine d'action contre les projets de brevets logiciels de l'UE

#dokurl: URL permanente de cette dépêche

#ffii: A propos de la FFII

#eurolinux: A propos de l'Alliance Eurolinux

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: gharfang ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swndemo030914 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

