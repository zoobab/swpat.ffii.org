# -*- mode: makefile -*-

swnport030711.en.lstex:
	lstex swnport030711.en | sort -u > swnport030711.en.lstex
swnport030711.en.mk:	swnport030711.en.lstex
	vcat /ul/prg/RC/texmake > swnport030711.en.mk


swnport030711.en.dvi:	swnport030711.en.mk
	rm -f swnport030711.en.lta
	if latex swnport030711.en;then test -f swnport030711.en.lta && latex swnport030711.en;while tail -n 20 swnport030711.en.log | grep -w references && latex swnport030711.en;do eval;done;fi
	if test -r swnport030711.en.idx;then makeindex swnport030711.en && latex swnport030711.en;fi

swnport030711.en.pdf:	swnport030711.en.ps
	if grep -w '\(CJK\|epsfig\)' swnport030711.en.tex;then ps2pdf swnport030711.en.ps;else rm -f swnport030711.en.lta;if pdflatex swnport030711.en;then test -f swnport030711.en.lta && pdflatex swnport030711.en;while tail -n 20 swnport030711.en.log | grep -w references && pdflatex swnport030711.en;do eval;done;fi;fi
	if test -r swnport030711.en.idx;then makeindex swnport030711.en && pdflatex swnport030711.en;fi
swnport030711.en.tty:	swnport030711.en.dvi
	dvi2tty -q swnport030711.en > swnport030711.en.tty
swnport030711.en.ps:	swnport030711.en.dvi	
	dvips  swnport030711.en
swnport030711.en.001:	swnport030711.en.dvi
	rm -f swnport030711.en.[0-9][0-9][0-9]
	dvips -i -S 1  swnport030711.en
swnport030711.en.pbm:	swnport030711.en.ps
	pstopbm swnport030711.en.ps
swnport030711.en.gif:	swnport030711.en.ps
	pstogif swnport030711.en.ps
swnport030711.en.eps:	swnport030711.en.dvi
	dvips -E -f swnport030711.en > swnport030711.en.eps
swnport030711.en-01.g3n:	swnport030711.en.ps
	ps2fax n swnport030711.en.ps
swnport030711.en-01.g3f:	swnport030711.en.ps
	ps2fax f swnport030711.en.ps
swnport030711.en.ps.gz:	swnport030711.en.ps
	gzip < swnport030711.en.ps > swnport030711.en.ps.gz
swnport030711.en.ps.gz.uue:	swnport030711.en.ps.gz
	uuencode swnport030711.en.ps.gz swnport030711.en.ps.gz > swnport030711.en.ps.gz.uue
swnport030711.en.faxsnd:	swnport030711.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo swnport030711.en-??.g3n`;source faxsnd main
swnport030711.en_tex.ps:	
	cat swnport030711.en.tex | splitlong | coco | m2ps > swnport030711.en_tex.ps
swnport030711.en_tex.ps.gz:	swnport030711.en_tex.ps
	gzip < swnport030711.en_tex.ps > swnport030711.en_tex.ps.gz
swnport030711.en-01.pgm:
	cat swnport030711.en.ps | gs -q -sDEVICE=pgm -sOutputFile=swnport030711.en-%02d.pgm -
swnport030711.en/swnport030711.en.html:	swnport030711.en.dvi
	rm -fR swnport030711.en;latex2html swnport030711.en.tex
xview:	swnport030711.en.dvi
	xdvi -s 8 swnport030711.en &
tview:	swnport030711.en.tty
	browse swnport030711.en.tty 
gview:	swnport030711.en.ps
	ghostview  swnport030711.en.ps &
print:	swnport030711.en.ps
	lpr -s -h swnport030711.en.ps 
sprint:	swnport030711.en.001
	for F in swnport030711.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	swnport030711.en.faxsnd
	faxsndjob view swnport030711.en &
fsend:	swnport030711.en.faxsnd
	faxsndjob jobs swnport030711.en
viewgif:	swnport030711.en.gif
	xv swnport030711.en.gif &
viewpbm:	swnport030711.en.pbm
	xv swnport030711.en-??.pbm &
vieweps:	swnport030711.en.eps
	ghostview swnport030711.en.eps &	
clean:	swnport030711.en.ps
	rm -f  swnport030711.en-*.tex swnport030711.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} swnport030711.en-??.* swnport030711.en_tex.* swnport030711.en*~
swnport030711.en.tgz:	clean
	set +f;LSFILES=`cat swnport030711.en.ls???`;FILES=`ls swnport030711.en.* $$LSFILES | sort -u`;tar czvf swnport030711.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
