<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Wpp: Symlabs defines itself as %(q:an energetic collection of bright and gifted software engineers). These engineers have worked together as a team on various large directory development and deployment projects throughout Europe, United States and Japan. Some of Symlabs' major clients include Vodafone UK, Vodafone Global Content Services (VGCS), Vodafone Spain and Vodafone Portugal. Witnessing first hand some of the repetitive challenges that these organizations were experiencing in their directory services and applications, it was decided that a valuable service could be offered in this specialized area. Symlabs began operations in a high-tech technology park in Lisbon, Portugal and has branched out to include its new headquarters in San Francisco, California, and an office in Madrid, Spain.  Symlabs has since then evolved to become a middleware development company specializing in the development of LDAP directory solutions and directory development productivity tools.

#eso: Isabel Afonso speaking for INPI

#nhW: another MEP

#Sre: ANSOL president

#wih: MEP who organized the hearing

#nss: Among the speakers were

#hvl: The INPI representative Isabel Alfonso said that there's a national consensus %(e:for) the directive, based on an earlier consultation.  Isabel Alfonso did not seem very convinced of this consensus herself and stressed that she was merely trying her best to say what INPI had told her to say.  Later INPI provided a set of %(pt:documents) which explain how the previous %(q:consensus) had come about, and promised to %(q:re-open the consultations).

#moa: Everyone in the meeting, except for the representative of the Portuguese Patent Office INPI, was against the directive.

#WWn: Most employment in the IT sector in Portugal is created by companies of a size below 50 employees.  Portuguese companies do not own a single one of the more than 30000 patents on algorithms and business methods that have been illegally granted by the European Patent Office (EPO) in Munich.  Most people in these companies have protected their investments by means of copyright.  The idea that an algorithm can be owned is completely unheard of to them and contrary to their ethics.  When faced with the broad and trivial patents on abstract ideas which the EPO has been granting, they usually just say %(q:You're kidding).  It is very difficult to get people to face the reality that has been created in Munich and Brussels and that will wind down on us if we don't wake up now.  Although the hearing was set up very late and hastily, we urge everybody to participate.

#yWt: Jaime Villate, who represents the %(as:Portuguese Free Software Association ANSOL) and the Eurolinux Alliance at the hearing, explains:

#ohr: We therefore call on our Portuguese colleagues: Wake up and Catch the Thief!  There is no time to lose!  Join hands now with MEP Ilda Figueiredo and with all other politicians who are willing to defend the interests of Portugal's industry and citizens!

#ned: We see this directive as an assault on our livelihood and interests, an attempt to steal our intellectual property and to prevent us from competing.  Bloated corporations with big legal departments, mostly located in what McCarthy would call %(q:high-cost economies), are trying to gain control of the jewels of Portugal's and in fact Europe's software industry.  They are making sure that broad and trivial patents on algorithms and business methods can no longer be revoked by Portuguese courts.  Tautological requirements, such as that a %(q:computer-implemented invention) should %(q:make a technical contribution in its inventive step), do not prevent the enforcement of broad and trivial patents on algorithms and business methods.  Rather, they prevent people from figuring out what is going on, and that may well be the intention behind these awkward formulas.

#jhW: If the Legal Affairs Committee has its way, the US patent game will be imposed on us in Europe, and productivity in software development will be greatly reduced, in particular in countries like Portugal.  MEP Arlene McCarthy's draft report recommends that about 10% of development cost should be spent for patenting only, and she has introduced an amendment which says that Europe needs to impose such high legal costs in order to compete with %(q:low-cost economies).

#bkW: According to the laxist standards proposed by the European Parliament's Legal Affairs Commission, we could have obtained plenty of patents on our software innovations, and we are indeed in obtaining some such patents in the US.  The only reason that we do this is because in the US you need to play the %(q:patent game). We have no intention of enforcing these patents ourselves - we need them only as bargaining chips to defend ourselves against bigger players.  Patents to not protect our investments, and they do not even reliably protect us against patent attacks from other players.  Patents make sense in the chemical industry perhaps, but in software their main effect is to make innovation expensive and dangerous.

#oae: Portugal is a great place for developing software.  There are many smart and talented people coming out of the universities, and the labor costs are amongst the lowest in the EU.  Any group of 5-10 good developpers can turn out nifty things and make a lot of money, relying on copyright and fast reaction to market demands.  Our company has gained a leading position in this way.

#eWs: Felix Gaehtgens, managing director of %(sl:Symlabs Lda) from Lisbon, who spoke at a recent hearing in the European Parliament on the subject of software patents, explains:

#esW: Participants need to register only at the entrance.

#WWo: The hearing will take place at %(bq|Auditório da Junta de Freguesia de Miragaia|Campo dos Martires da Pátria nº 22 Porto, Portugal|July 15th 15:00.)

#dokurl: Permanent URL of this Press Release

#eurolinux: the Eurolinux Alliance

#pkh: What happened in Port -- Report after the Hearing

#eal: Details

#descr: The Municipality of Porto in Portugal is conducting a hearing on July 15th about the possible effects of the proposed European Software Patent Directive on companies and citizens in Portugal.  The hearing is chaired by Ilda Figueiredo, member of the European Parliament.

#title: Software Patent Hearing 2003/07/15 in Porto

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnport030711 ;
# txtlang: en ;
# multlin: t ;
# End: ;

