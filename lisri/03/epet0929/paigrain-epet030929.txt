       Intervention de Philippe Aigrain devant le Comit� des p�titions du Parlement europ�en.
                                                P�tition 2003/407

Bonjour. Je m'appelle Philippe Aigrain et je suis chercheur en informatique. Apr�s plusieurs ann�es
pass�es comme chef de secteur � technologies logicielles � dans les programmes de recherche
europ�ens, je suis en train de cr�er une soci�t� logicielle. C'est moi qui ai soumis la p�tition de plus
de 30 grands scientifiques europ�ens en informatique, bien que mes titres soient beaucoup plus
modestes, parce que parmi les informaticiens, j'�tais l'un des plus familiers avec le fonctionnement
des institutions europ�ennes.

Ma t�che aujourd'hui est grandement simplifi�e, puisque dans votre vote de mercredi dernier, vous
avez donn� satisfaction � l'essentiel des requ�tes qui nous avions formul�es. Nous vous demandions
de voter des amendements assurant qu'il soit impossible de breveter les id�es sous-jacentes des
logiciels (ou algorithmes), les m�thodes de traitement de l'information, les structures de donn�es, et
les logiciels d'interaction entre �tre humains et ordinateurs.

Note to interpreters: the text of the petition "urged the Members of the European Parliament, whatever their
party affiliation, to adopt a text that will make impossible, clearly, for today and tomorrow, any patenting of the
underlying ideas of software (or algorithms), of information processing methods, of representations of
information and data, and of software interaction between human beings and computers.


Je dois donc avant tout vous remercier. Mais, je veux �galement profiter de l'honneur qui m'est fait
de pouvoir m'adresser au comit�, pour attirer votre attention sur les dangers de la suite du processus
l�gislatif.

Les amendements que vous avez adopt�s forment un tout interd�pendant. Retirez-en un seul, et tout
sera perdu. Laissez affaiblir la d�finition de ce qui est technique et donc potentiellement brevetable,
et 100,000 brevets de logiciels s'y engouffreront. Laissez supprimer l'amendement qui pr�voit que
seules les caract�ristiques techniques puissent �tre utilis�es pour d�cider s'il y a innovation
technique, et l'Office des Brevets pourra consid�rer tout et n'importe quoi comme technique.
Laissez r�introduire les revendications logicielles (software or program claims) et quelles que
soient les autres pr�cautions, tous les d�veloppeurs, distributeurs, ou �diteurs de logiciels seront
victimes d'un oc�an de litiges.

On vous dira que les amendements adopt�s priveraient de leur propri�t� intellectuelle les d�tenteurs
de dizaines de milliers de brevets soudainement invalid�s. Rien de plus faux. Une part de ces
brevets (ceux qui portent sur des dispositifs et proc�d�s physiques) restent parfaitement valides. Les
autres ne l'ont jamais �t�, et vous n'avez fait que le rappeler. Mais ils pourront rester paisiblement
dans les portefeuilles de brevets des soci�t�s qui les d�tiennent. Ce n'est que si ces soci�t�s en
abusent pour tuer la concurrence ou le domaine public des id�es que la question de leur validit� se
posera.


On vous dira que vos amendements imposent un changement dans l'organisation du travail des
offices. Tant mieux, car cette organisation du travail aboutit aujourd'hui � un retard de 6 � 10 ans
(vous avez bien entendu 6 � 10 ans) dans l'examen des brevets de la cat�gorie � computer and
telecommunications �.

On vous dira que les amendements remettent au go�t du jour des th�ories d�pass�es. Si par th�orie
d�pass�e on veut dire la libre circulation et utilisation des connaissances, alors n'est-ce pas la
condition d'existence de la soci�t� de la connaissance que nous cherchons � b�tir?

Et puis viendront les arguments de dernier ressort : on vous dira que vos amendements sont
incompatibles avec les accords ADPIC (TRIPS) . Il n'en est rien. Seules des interpr�tations
maximalistes peuvent pr�tendre que ces accords forceraient � breveter quoi que ce soit au-del� des


produits physiques et des proc�d�s de production, ou interdiraient des clauses de protection de
l'interop�rabilit� comme celle du nouvel article 6a

Merci de votre attention. Je suis bien s�r disponible pour r�pondre � toute question, aujourd'hui ou
dans le futur.


