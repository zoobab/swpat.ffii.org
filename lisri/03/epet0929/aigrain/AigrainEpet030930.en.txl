<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Philippe Aigrain 2003-09-30: Speech before the Petition Commission of the European Parliament

#descr: A petition of 33 famous computer scientists had %(q:urged the Members of the European Parliament, whatever their party affiliation, to adopt a text that will make impossible, clearly, for today and tomorrow, any patenting of the underlying ideas of %(tp|software|or algorithms), of information processing methods, of representations of information and data, and of software interaction between human beings and computers.) in May 2003.  Philippe Aigrain, initiator of the petition, thanks MEPs for basically fulfilling the demands of the petitioners, and warns of various pressures and fallacies from the patent lawyer lobby with which the Parliament will be confronted in the coming months.

#eli: Hello. I am Philippe Aigrain, a researcher in informatics. After being in charge of European research programs in the « software technologies » sector for several years, I am now working on creating a software society. Even though my titles are quite modest, I have submitted the petition of more than 30 great European scientists in informatics, because among those informaticians, I was one of the people most familiar with the way the European institutions work.

#eeW: My task of today has been greatly simplified, because in your vote from last Wednesday, you have fulfilled the essential part of the requests we formulated. We asked you to vote the amendments which would ensure that it would be impossible to patent ideas underlying software (or algorithms), data processing methods, data structures, and software that allows interaction between humans and computers.

#eda: As such, I have to thank you all. Nonetheless, I also want to take advantage of the honor that was granted to me to address the committee, to point out the dangers that lie in the rest of the legislative process.

#Wis: The amendments you have adopted form an interdependent entity. Retract one of them, and everything will be lost. Allow the weakening of the definition of what is technical and thus potentially patentable, and 100,000 software patents will swamp it. Allow the suppression of the amendment which ensures that only technical features can be taken into account when determining whether there is a technical innovation, and the Patent Office will be able to consider anything as technical. Allow the reintroduction of program (or software) claims and regardless of other precautions, all developers, distributors and editors of software will fall victim to an flood of lawsuits.

#lic: You will be told that the adopted amendments will deprive the owners of tens of thousands of patents suddenly of their intellectual property. Nothing could be further from the truth. One part of those patents (those based on devices and physical processes) remain perfectly valid. The others never were valid in the first place, and all you did was pointing this out. But these patents will be able to stay peacefully in the patent portfolios of the companies that hold them. It is only when these companies try to abuse them to kill their competition or the public domain to which these ideas belong, that the question of their validity will arise.

#nWr: You will be told that your amendments impose a change on the organisation of the functioning of the Patent Office. This is great, because this current organisation has led today to a delay of 6 to 10 years (yes, you heard me well, 6 to 10 years) in the examination of patents in the category « computer and telecommunications ».

#ecs: You will be told that the amendments reinstate old-fashioned theories. If by old-fashioned theories they mean the free dissemination and use of knowledge, then is this not the required condition for the existence of the knowledge society we seek to build?

#dpt: And then the arguments of the last resort will come : you will be told that your amendments are incompatible with the TRIPs agreements. This is completely false. Only the most maximalist interpretations can claim that these agreements would force patentability of anything beyond physical products and production processes, or would prohibit clauses that protect the interoperability such as the new article 6a.

#tpd: Thank you for your attention. I am of course available to reply to all questions, both today and in the future.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: jmaebe ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: AigrainEpet030930 ;
# txtlang: en ;
# multlin: t ;
# End: ;

