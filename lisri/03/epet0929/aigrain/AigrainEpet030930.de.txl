<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Philippe Aigrain 2003-09-30: Rede vor dem Petitionsausschuss des Europäischen Parlaments

#descr: Eine Petition von 33 berühmten Informatikern im Mai 2003 hat %(q:die Mitglieder des Europäischen Parlaments, gleich welcher Parteizugehörigkeit, dringend gebeten, einen Gesetzestext zu wählen, der es unmöglich machen wird, in klarer Weise, ein- für allemal, die zu Grunde liegenden Ideen von %(tp|Software|oder Algorithmen), von Methoden zur Informationsverarbeitung, von Repräsentationen von Informationen und Daten und von Software-Interaktionen zwischen Menschen und Computern zu patentieren.)  Philippe Aigrain, Initiator der Petition, dankt MdEPs für die grundsätzliche Erfüllung des Verlangens der Petitionäre und warnt vor vielfältigem Druck und Täuschungsmanövern der Patentanwaltslobby, mit denen das Parlament in den kommenden Monaten konfrontiert werden wird.

#eli: Guten Tag.  Ich bin Philippe Aigrain, Informatikforscher.  Nach mehreren Jahren als Leiter des Bereichs Software-Technologie der europäischen Forschungsprogramme bin ich nun dabei, eine Software-Gesellschaft zu gründen.  Ich bin derjenige, der die Petition von über 30 großen europäischen Wissenschaftlern der Informatik eingereicht hat, obwohl meine Titel viel gemäßigter sind, weil ich unter den Informatikern einer der am meisten mit der Funktionsweise der europäischen Institutionen Vertrauten war.

#eeW: Meine heutige Aufgabe hat sich großartig vereinfacht, weil Sie in Ihrer Abstimmung letzten Mittwoch die von uns formulierten Anforderungen im Wesentlichen erfüllt haben.  Wir ersuchten Sie darum, für diejenigen Änderungsanträge zu stimmen, die sicherstellen würden, dass es unmöglich sein werde, die einer Software (oder Algorithmen) zugrundeliegenden Ideen, Methoden zur Informationsverarbeitung, Datenstrukturen und Software zur Interaktion zwischen Mensch und Computer zu patentieren.

#eda: Deswegen muss ich Ihnen vor allem danken.  Aber ich möchte zugleich die Ehre nutzen, die es mir bereitet, mich an den Ausschuss wenden zu können, um Ihre Aufmerksamkeit auf die Gefahren des folgenden Gesetzgebungsverfahrens zu lenken.

#Wis: Die Änderungsanträge, für die Sie gestimmt haben, bilden ein interdependentes Ganzes.  Nehmen Sie einen davon zurück, und alles ist verloren.  Lassen Sie die Abschwächung der Definition dessen zu, was technisch und daher möglicherweise patentierbar ist, und 100.000 Software-Patente werden hindurchsickern.  Lassen Sie die Unterdrückung des Änderungsantrages zu, welcher vorsieht, dass nur technische Merkmale zur Entscheidung, ob eine technische Neuerung vorliegt, herangezogen werden können, und das Patentamt wird alles, egal was, als technisch erachten können.  Lassen Sie die Wiederaufnahme von Ansprüchen auf Software (oder Programmansprüche) zu, und unabhängig davon, welch andere Vorsichtsmaßnahmen auch getroffen werden, würden alle Entwickler, Distributoren oder Herausgeber von Software Opfer eines Ozeans von Klageverfahren werden.

#lic: Man wird Ihnen erzählen, dass die angenommenen Änderungsanträge die Halter zehntausender Patente plötzlich ihres geistigen Eigentums berauben würden.  Nichts könnte falscher sein.  Ein Teil jener Patente (der auf Geräten und physikalischen Vorgängen aufbaut) bleibt unverändert gültig.  Die anderen waren es nie, und Sie haben nichts weiter getan, als das festzustellen.  Aber diese Patente können friedlich in den Patentportfolios der Gesellschaften ihrer Halter verbleiben.  Nur wenn diese Gesellschaften sie missbrauchen, um die Konkurrenz oder die allgemeine Verfügbarkeit von Ideen auszuschalten, wird sich die Frage ihrer Gültigkeit stellen.

#nWr: Man wird Ihnen erzählen, dass Ihre Änderungen den Patentämtern einen Wechsel der Arbeitsabläufe auferlegen.  Um so besser, denn diese Arbeitsabläufe bringen mittlerweile eine Verzögerung von 6 bis 10 Jahren (Sie haben richtig verstanden, 6 bis 10 Jahren) bei der Prüfung von Patentanträgen in der Kategorie »Computer und Telekommunikation« mit sich.

#ecs: Man wird Ihnen erzählen, dass die Änderungen altmodische Lehren wieder in Kraft setzen.  Wenn man mit altmodischen Lehren den freien Umlauf und den freien Gebrauch von Wissen meint, ist das denn nicht die Daseinsbedingung der Wissensgesellschaft, die wir aufzubauen versuchen?

#dpt: Und dann werden die Argumente der letzten Zuflucht kommen: Man wird Ihnen erzählen, dass Ihre Änderungsanträge unverträglich mit dem TRIPS-Abkommen seien.  Davon stimmt nichts.  Nur maximalistische Auslegungen können so tun, als ob dieses Abkommen alles jenseits physischer Produkte und Produktionsverfahren zwingend patentierbar mache, oder Klauseln zum Schutz der Interoperabilität wie den neuen Artikel 6a untersage.

#tpd: Vielen Dank für Ihre Aufmerksamkeit.  Ich stehe selbstverständlich zur Beantwortung jeder Frage zur Verfügung, heute und in Zukunft.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: ccornels ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: AigrainEpet030930 ;
# txtlang: de ;
# multlin: t ;
# End: ;

