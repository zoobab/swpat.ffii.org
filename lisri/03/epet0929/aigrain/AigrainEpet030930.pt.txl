<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Philippe Aigrain 2003-09-30: Discurso perante a Comissão de Patentes do Parlamento Europeu

#descr: Em Maio de 2003, uma petição de 33 cientistas famosos %(q:apelou aos Membros do Parlamento Europeu, qualquer que fosse a sua afiliação política, para que adoptassem um texto que tornaria impossível, claramente, para hoje e para o futuro, qualquer patente sobre as ideias base do %(tp|software|ou algoritmos), métodos de processamento de informação, de representação de informação e dados, e da interacção entre software e seres humanos e computadores). Philippe Aigrain, iniciador dessa petição, agradece aos Euro-deputados por, basicamente, cumprido com as exigências da petição, e avisa das várias pressões e falácias, vindas do lobby dos advogados de patentes, com as quais o Parlamento vai ser confrontado nos próximos meses.

#eli: Bom dia. Chamo-me Philippe Aigrain e sou investigador em informática. Após vários anos passados como chefe do sector «tecnologias do software» nos programas europeus de investigação, estou a criar uma sociedade de software. Fui eu quem submeteu a petição de mais de 30 grandes cientistas europeus de informática, embora os meus títulos sejam bastante mais modestos, porque de entre os informáticos, eu era um dos mais familiarizados com o funcionamento das instituições europeias.

#eeW: A minha tarefa hoje em dia foi muito simplificada, dado que com o vosso voto na passada Quarta-feira, satisfizeram o essencial dos requisitos que nós formulámos. Exigíamos que votassem por emendas que garantiriam que seria impossível patentear as ideias subjacentes do software (ou algoritmos), os métodos de tratamento da informação, as estruturas de dados e o software de interacção entre humanos e computadores.

#eda: Devo portanto, antes de mais, agradecer-vos. Mas, desejo igualmente aproveitar-me da honra que me é prestada para me dirigir ao comité, para dirigir a vossa atenção para os perigos para o seguimento do processo legislativo.

#Wis: As emendas que adoptaram formam um todo interdependente. Retirem uma só e tudo será perdido. Deixem diluir a definição do que é técnico e por isso potencialmente patenteável, e 100.000 patentes de software por aí surgirão. Deixem suprimir a emenda que diz que apenas as características técnicas podem ser utilizadas para decidir se há inovação técnica e o Gabinete Europeu de Patentes poderá considerar que tudo não importa o quê seja técnico. Deixem reintroduzir as reivindicações de software (reivindicações sobre software ou programas) e aquelas que são as outras precauções, e todos os programadores, distribuidores ou editores de software serão vítimas de um oceano de litígios.

#lic: Ser-vos-á dito que as emendas adoptadas privariam da sua propriedade intelectual os detentores de milhares de patentes subitamente inválidas. Nada seria mais falso. Uma parte destas patentes (aqueles que se baseiam em dispositivos e processos físicos) continuam perfeitamente válidas. As outras nunca o foram, e vocês não fizeram mais que o relembrar. Mas elas poderão descansar em paz nos portfólios de patentes das sociedades que as detêem. É apenas quando estas sociedades abusam delas para matar a concorrência ou o domínio público das ideias que será posta em questão a sua validade.

#nWr: Ser-vos-á dito que as vossas emendas impõem uma mudança na organização do trabalho dos gabinetes. Tanto melhor, pois esta organização do trabalho tem hoje um atraso de 6 a 10 anos (entenderam bem, 6 a 10 anos) na examinação de patentes da categoria «computador e telecomunicações».

#ecs: Ser-vos-á dito que as emendas remetem para hoje teorias ultrapassadas. Se por teorias ultrapassadas queremos dizer a livre circulação e utilização do conhecimento, então não é esta a condição de existência da sociedade da informação que nós procuramos construir?

#dpt: E depois surgem os argumentos de último recurso: ser-vos-á dito que as vossas emendas são incompatíveis com os acordos do TRIPS. Isto não vale nada. Apenas as interpretações maximalistas podem fazer de conta que estes acordos forçariam a patentear o que está para além dos produtos físicos e dos processos de produção, ou que impediriam cláusulas de protecção da interoperabilidade como a do novo Artigo 6º a).

#tpd: Obrigado pela vossa atenção. Estou certamente disponível para responder a todas as perguntas, hoje e no futuro.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: rumise ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: AigrainEpet030930 ;
# txtlang: pt ;
# multlin: t ;
# End: ;

