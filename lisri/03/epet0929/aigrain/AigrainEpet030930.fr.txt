<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: A petition of 33 famous computer scientists had %(q:urged the Members of the European Parliament, whatever their party affiliation, to adopt a text that will make impossible, clearly, for today and tomorrow, any patenting of the underlying ideas of %(tp|software|or algorithms), of information processing methods, of representations of information and data, and of software interaction between human beings and computers.) in May 2003.  Philippe Aigrain, initiator of the petition, thanks MEPs for basically fulfilling the demands of the petitioners, and warns of various pressures and fallacies from the patent lawyer lobby with which the Parliament will be confronted in the coming months.
#title: Philippe Aigrain 2003-09-30: Intervention devant le Comité des pétitions du Parlement Européen
#eli: Bonjour. Je m'appelle Philippe Aigrain et je suis chercheur en informatique. Après plusieurs années passées comme chef de secteur « technologies logicielles » dans les programmes de recherche européens, je suis en train de créer une société logicielle. C'est moi qui ai soumis la pétition de plus de 30 grands scientifiques européens en informatique, bien que mes titres soient beaucoup plus modestes, parce que parmi les informaticiens, j'étais l'un des plus familiers avec le fonctionnement des institutions européennes.
#eeW: Ma tâche aujourd'hui est grandement simplifiée, puisque dans votre vote de mercredi dernier, vous avez donné satisfaction à l'essentiel des requêtes qui nous avions formulées. Nous vous demandions de voter des amendements assurant qu'il soit impossible de breveter les idées sous-jacentes des logiciels (ou algorithmes), les méthodes de traitement de l'information, les structures de données, et les logiciels d'interaction entre être humains et ordinateurs.
#eda: Je dois donc avant tout vous remercier. Mais, je veux également profiter de l'honneur qui m'est fait de pouvoir m'adresser au comité, pour attirer votre attention sur les dangers de la suite du processus législatif.
#Wis: Les amendements que vous avez adoptés forment un tout interdépendant. Retirez-en un seul, et tout sera perdu. Laissez affaiblir la définition de ce qui est technique et donc potentiellement brevetable, et 100,000 brevets de logiciels s'y engouffreront. Laissez supprimer l'amendement qui prévoit que seules les caractéristiques techniques puissent être utilisées pour décider s'il y a innovation technique, et l'Office des Brevets pourra considérer tout et n'importe quoi comme technique.  Laissez réintroduire les revendications logicielles (software or program claims) et quelles que soient les autres précautions, tous les développeurs, distributeurs, ou éditeurs de logiciels seront victimes d'un océan de litiges.
#lic: On vous dira que les amendements adoptés priveraient de leur propriété intellectuelle les détenteurs de dizaines de milliers de brevets soudainement invalidés. Rien de plus faux. Une part de ces brevets (ceux qui portent sur des dispositifs et procédés physiques) restent parfaitement valides. Les autres ne l'ont jamais été, et vous n'avez fait que le rappeler. Mais ils pourront rester paisiblement dans les portefeuilles de brevets des sociétés qui les détiennent. Ce n'est que si ces sociétés en abusent pour tuer la concurrence ou le domaine public des idées que la question de leur validité se posera.
#nWr: On vous dira que vos amendements imposent un changement dans l'organisation du travail des offices. Tant mieux, car cette organisation du travail aboutit aujourd'hui à un retard de 6 à 10 ans (vous avez bien entendu 6 à 10 ans) dans l'examen des brevets de la catégorie « computer and telecommunications ».
#ecs: On vous dira que les amendements remettent au goût du jour des théories dépassées. Si par théorie dépassée on veut dire la libre circulation et utilisation des connaissances, alors n'est-ce pas la condition d'existence de la société de la connaissance que nous cherchons à bâtir?
#dpt: Et puis viendront les arguments de dernier ressort : on vous dira que vos amendements sont incompatibles avec les accords ADPIC (TRIPS) . Il n'en est rien. Seules des interprétations maximalistes peuvent prétendre que ces accords forceraient à breveter quoi que ce soit au-delà des produits physiques et des procédés de production, ou interdiraient des clauses de protection de l'interopérabilité comme celle du nouvel article 6a.
#tpd: Merci de votre attention. Je suis bien sûr disponible pour répondre à toute question, aujourd'hui ou dans le futur.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: rumise ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: AigrainEpet030930 ;
# txtlang: fr ;
# End: ;

