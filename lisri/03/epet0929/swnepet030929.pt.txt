<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: Na passada Terça Feira o Parlamento votou contra as patentes de software e a favor da liberdade de publicação, liberdade de inter-operacionalidade e outros valores básicos da sociedade de informação, invertendo o sentido de uma proposta de directiva da Comissão Europeia, para assim satisfazer as exigências do quarto de milhão de signatários da %(q:Petição Eurolinux para uma Europa Livre de Patentes de Software) e de 30 eminentes cientistas de computadores. Os iniciadores de ambas as petições irão falar perante o Comité de Petições do Parlamento Europeu na Terça Feira pelas 18:00 para expressarem o seu agradecimento e terem algumas conversas com membros do Parlamento Europeu.
#title: Iniciadores de Petições Agradecem ao Parlamento Europeu
#FmS: A %(ep:Petição para uma Europa Livre de Patentes de Software) e a %(sp:Petição dos Cientistas) irão ser apresentadas na Terça feira, dia 30 de Setembro de 2003 pelas 6 horas da tarde no Comité das Petições na Sala A3G2 do Parlamento Europeu, Edifício Spinelli (ASP) em Bruxelas por %(BL) e Philippe Aigrain.
#WaW: Dia 10 de Novembro as patentes de software estarão possívelmente na agenda de uma reunião de peritos de patentes dos governos de estados membros da UE no Concelho Europeu. O lobby dos proprietários de patentes de software e o Comissário da UE para o Mercado Interno Frits Bolkestein contam agora com o Concelho, cujo %(q:gabinete de trabalho para a política de patentes) já provou no passado dar pronta resposta aos desejos dos proprietários de patentes. Bolkestein e os seus apoiantes previram que o Concelho irá retirar a directiva ou, se isso falhar, ceder à pressão já antecipada dos EUA. No dia antes do voto, %(fb:Bolkestein) avisou os Membros do Parlamento Europeu que arruinariam as suas hipóteses de participação democrática caso votassem como o fizeram na Quarta Feira passada. Os governos dos %(us:EUA) e do %(uk:Reino Unido) enviaram avisos de conteúdo semelhante a Membros do Parlamento Europeu durante este mês.
#apt: Nesta atmosfera de medo, incerteza e desconfiança (FUD) lançada contra o Parlamento Europeu, espera-se que tanto o %(pa:Sr. Aigrain) como o Sr. Lang fortaleçam a posição do Parlamento.
#egi: Para estar presente nesta reunião, por favor enviem mail para %(MT).
#ewo: Bolkestein's warning to MEPs
#Wai: Similar statements were uttered previously by Arlene McCarthy and later by several patent attorneys, some in names of organisations.
#asP: Reactions to EP Vote

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: jcarvalho ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnepet030929 ;
# txtlang: pt ;
# End: ;

