\begin{subdocument}{swnepet030929}{Iniciadores de Peti\c{c}\~{o}es Agradecem ao Parlamento Europeu}{http://swpat.ffii.org/lisri/03/epet0929/index.pt.html}{Workgroup\\swpatag@ffii.org\\vers\~{a}o inglesa 2003/09/28 por Joaquim Carvalho\footnote{http://lists.ffii.org/mailman/listinfo/traduk/index.html}}{Na passada Ter\c{c}a Feira o Parlamento votou contra as patentes de software e a favor da liberdade de publica\c{c}\~{a}o, liberdade de inter-operacionalidade e outros valores b\'{a}sicos da sociedade de informa\c{c}\~{a}o, invertendo o sentido de uma proposta de directiva da Comiss\~{a}o Europeia, para assim satisfazer as exig\^{e}ncias do quarto de milh\~{a}o de signat\'{a}rios da ``Peti\c{c}\~{a}o Eurolinux para uma Europa Livre de Patentes de Software'' e de 30 eminentes cientistas de computadores. Os iniciadores de ambas as peti\c{c}\~{o}es ir\~{a}o falar perante o Comit\'{e} de Peti\c{c}\~{o}es do Parlamento Europeu na Ter\c{c}a Feira pelas 18:00 para expressarem o seu agradecimento e terem algumas conversas com membros do Parlamento Europeu.}
\begin{sect}{detal}{details}
A Peti\c{c}\~{a}o para uma Europa Livre de Patentes de Software\footnote{http://petition.eurolinux.org/index.pt.html} e a Peti\c{c}\~{a}o dos Cientistas\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/komp0305/index.en.html} ir\~{a}o ser apresentadas na Ter\c{c}a feira, dia 30 de Setembro de 2003 pelas 6 horas da tarde no Comit\'{e} das Peti\c{c}\~{o}es na Sala A3G2 do Parlamento Europeu, Edif\'{\i}cio Spinelli (ASP) em Bruxelas por Bernard Lang\footnote{http://pauillac.inria.fr/~lang/reperes/patents/} e Philippe Aigrain.

Dia 10 de Novembro as patentes de software estar\~{a}o poss\'{\i}velmente na agenda de uma reuni\~{a}o de peritos de patentes dos governos de estados membros da UE no Concelho Europeu. O lobby dos propriet\'{a}rios de patentes de software e o Comiss\'{a}rio da UE para o Mercado Interno Frits Bolkestein contam agora com o Concelho, cujo ``gabinete de trabalho para a pol\'{\i}tica de patentes'' j\'{a} provou no passado dar pronta resposta aos desejos dos propriet\'{a}rios de patentes. Bolkestein e os seus apoiantes previram que o Concelho ir\'{a} retirar a directiva ou, se isso falhar, ceder \`{a} press\~{a}o j\'{a} antecipada dos EUA. No dia antes do voto, Bolkestein\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/deba/index.en.html\#bolk} avisou os Membros do Parlamento Europeu que arruinariam as suas hip\'{o}teses de participa\c{c}\~{a}o democr\'{a}tica caso votassem como o fizeram na Quarta Feira passada. Os governos dos EUA\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/usrep0309/index.en.html} e do Reino Unido\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/ukrep0309/index.en.html} enviaram avisos de conte\'{u}do semelhante a Membros do Parlamento Europeu durante este m\^{e}s.

Nesta atmosfera de medo, incerteza e desconfian\c{c}a (FUD) lan\c{c}ada contra o Parlamento Europeu, espera-se que tanto o Sr. Aigrain\footnote{http://swpat.ffii.org/lisri/03/epet0929/aigrain/index.pt.html} como o Sr. Lang fortale\c{c}am a posi\c{c}\~{a}o do Parlamento.

Para estar presente nesta reuni\~{a}o, por favor enviem mail para Benjamin Henrion\footnote{mailto:epreg030929@ffii.org?subject=http://swpat.ffii.org/lisri/03/epet0929/index.pt.html}.
\end{sect}

\begin{sect}{media}{Contactos para os media}
\begin{description}
\item[mail:]\ media at ffii org
\item[telefone:]\ FFII Munique (Alem\~{a}o, Ingl\^{e}s e Franc\^{e}s): 0049/89/18979927
Benjamin Henrion (Franc\^{e}s e Ingl\^{e}s): 0032/498/292771 ou 0032/10/454779
Jonas Maebe (Nederlands e Ingl\^{e}s): +32-485-36-96-45
Dieter Van Uytvanck (Nederlands e Ingl\^{e}s): +32-499-16-70-10
Erik Josefsson (Sueco e Ingl\^{e}s): +46-707-696567
Alex Macfie (Ingl\^{e}s): +44 7901 751753
Joaquim Carvalho (Portoges e Ingl\^{e}s): +35-1-93-6169633
Mais Contactos ser\~{a}o fornecidos a pedido.
\end{description}
\end{sect}

\begin{sect}{url}{URL Permanente deste Press Release}
http://swpat.ffii.org/lisri/03/epet0929/index.pt.html
\end{sect}

\begin{sect}{links}{Links Anotados}
\begin{itemize}
\item
{\bf {\bf Bolkestein's warning to MEPs\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/deba/index.en.html\#bolk}}}

\begin{quote}
Similar statements were uttered previously by Arlene McCarthy and later by several patent attorneys, some in names of organisations.

ver Reactions to EP Vote\footnote{http://swpat.ffii.org/lisri/03/plen0924/index.pt.html\#links}
\end{quote}
\filbreak

\item
{\bf {\bf O Parlamento da Uni\~{a}o Europeia Vota a favor de Limites Reais \`{a} Patenteabilidade\footnote{http://swpat.ffii.org/lisri/03/plen0924/index.pt.html}}}

\begin{quote}
No seu voto de plen\'{a}rio de 24 de Setembro, o Parlamento Europeu aprovou a proposta de directiva sobre a ``patenteabilidade de inven\c{c}\~{o}es implementadas em computador'', com emendas que claramente re-afirmam a n\~{a}o patenteabilidade de programas e de l\'{o}gica de neg\'{o}cios, e garantem as liberdades de publica\c{c}\~{a}o e de inter-operacionalidade.
\end{quote}
\filbreak

\item
{\bf {\bf Vote in 8 days: 2000 IT bosses ask European Parliament to say NO to software patents\footnote{http://swpat.ffii.org/lisri/03/epet0622/index.en.html}}}

\begin{quote}
A ``Petition for a Free Europe without Software Patents'' has gained more than 150000 signatures.  Among the supporters are more than 2000 company owners and chief executives and 25000 developpers and engineers from all sectors of the European information and telecommunication industries, as well as more than 2000 scientists and 180 lawyers.  Companies like Siemens, IBM, Alcatel and Nokia lead the list of those whose researchers and developpers want to protect programming freedom and copyright property against what they see as a ``patent landgrab''.  Currently the patent policy of many of these companies is still dominated by their patent departments.  These have intensively lobbied the European Parliament to support a proposal to allow patentability of ``computer-implemented inventions'' (recent patent newspeak term which usually refers to software in the context of patent claims, i.e. algorithms and business methods framed in terms of generic computing equipment), which the rapporteur, UK Labour MEP Arlene McCarthy, backed by ``patent experts'' from the socialist and conservative blocks, is trying to rush through the European Parliament on June 30, just 13 days after she had won the vote in the Legal Affairs Committe (JURI).
\end{quote}
\filbreak

\item
{\bf {\bf 30 Scientists 2003/05: Petition against Software Patent Directive\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/komp0305/index.en.html}}}

\begin{quote}
30 famous computer scientists sharply criticise the European Commission's proposal to legalise software patents in Europe.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

