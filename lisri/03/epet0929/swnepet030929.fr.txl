<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#asP: Reactions to EP Vote

#Wai: Similar statements were uttered previously by Arlene McCarthy and later by several patent attorneys, some in names of organisations.

#ewo: Bolkestein's warning to MEPs

#egi: Pour assister à la réunion, veuillez envoyez un courrier à %(MT).

#apt: Dans une telle atmosphère de crainte, d'incertitude, et de défiance (FUD) entourant le Parlement européen, on s'attend à ce que %(pa:M. Aigrain) et M. Lang apportent leur soutien au renforcement de la position du Parlement dans la législation européenne.

#WaW: Le 10 novembre, le sujet des brevets logiciels sera sans doute à l'agenda d'une réunion d'experts en brevets des états membres de l'UE au %(ec:Conseil de l'Europe). Le lobby des détenteurs de brevets logiciels et le Commissaire au Marché intérieur de l'UE, Frits Bolkestein, comptent maintenant sur l'appui du Conseil, dont le %(q:parti pris) en faveur des détenteurs de brevets n'est plus à démontrer. Bolkestein et ses supporters ont prédit que le Conseil retirera la directive ou, si ceci n'est pas possible, qu'il cédera aux pressions attendues des Américains. Le jour précédent le vote, %(fb:Bolkestein) a averti les MPE qu'ils réduiraient à néant leur chance de participer démocratiquement s'ils votaient comme ils l'ont fait mercredi dernier. Les gouvernements %(us:américain) et %(uk:britannique) ont formulé des avertissements similaires au début de ce mois.

#FmS: La %(q:Pétition pour une Europe libre de brevets logiciels) et la %(sp: Pétition des Scientifiques) seront présentées par %(BL) et Philippe Aigrain au Comité des Pétitions le mardi 30 septembre 2003 à 18 heures, en salle A3G2 du Parlement européen, bâtiment Spinelli (ASP), à Bruxelles.

#descr: Mercredi dernier, le Parlement européen a voté contre les brevets logiciels, et en faveur de la liberté de publication, de la liberté d'interopération, et d'autres valeurs fondamentales de la société de l'information, renversant par là l'impulsion d'une proposition de directive de la Commission européenne, pour, en fin de compte, satisfaire les exigences du quart de million de signataires de la %(q:Pétition Eurolinux pour une Europe libre de brevets logiciels) et de trente éminents chercheurs en informatique. Les initiateurs des deux pétitions s'exprimeront devant le Comité des Pétitions du Parlement européen ce mardi à 18 heures, pour remercier les Membres du Parlement et, avec eux, examiner ce qui reste encore à faire.

#title: Les initiateurs de la Pétition remercient le Parlement européen

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: gharfang ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnepet030929 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

