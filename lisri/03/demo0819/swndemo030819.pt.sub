\begin{subdocument}{swndemo030819}{Aug 27 Demonstrations against EU Software Patent Plans}{http://swpat.ffii.org/lisri/03/demo0819/index.pt.html}{Workgroup\\swpatag@ffii.org\\vers\~{a}o inglesa 2003/08/18 por Associa\c{c}\~{a}o Nacional para o Software Livre\footnote{http://www.ansol.org/}}{The Proposal for a software patent directive, which will be submitted to the European Parliament for plenary debate and subsequent decision on September 1st, is giving rise to another wave of protests.  Various groups in Belgium and elsewhere are mobilising for a rally in Brussels on August 27th and are calling on web administrators to temporarily block their web sites.}
\begin{sect}{detal}{details}
A Proposta para uma directiva de patentes de software, que ser\'{a} submetida ao Parlamento Europeu para decis\~{a}o a 1 de Setembro, est\'{a} a levantar outra onda de protestos. A Alian\c{c}a Eurolinux est\'{a} a apelar \`{a} participa\c{c}\~{a}o numa manifesta\c{c}\~{a}o em Bruxelas em 27 de Agosto\footnote{http://wiki.ael.be/index.php/BigDemo27aug}, acompanhada de um teatro de rua pelas 12:00 na Pra\c{c}a do Luxemburgo e confer\^{e}ncia \`{a}s 14:00 no Parlamento Europeu, e para o acompanhamento das manifesta\c{c}\~{o}es on-line\footnote{http://swpat.ffii.org/girzu/jarco/index.de.html}.

``A proposta de directiva tal como preparada pela Eurodeputada Arlene McCarthy ir\'{a} impor o patenteamento ilimitado de algoritmos e modelos de neg\'{o}cio tais como o One Click Shopping da Amazon, no estilo do que \'{e} praticado nos EUA'' diz Benjamin Henrion, que lidera uma equipa local de organiza\c{c}\~{a}o com o apoio de uma coliga\c{c}\~{a}o de organiza\c{c}\~{o}es representando 2000 companhias de software e 160,000 indiv\'{\i}duos, na sua maioria profissionais do software.

The proposal would, according to the organisers, ``legalise thousands of logic patents that have been granted by the European Patent Office against the letter and spirit of the law, making if impossible for national courts to continue to revoke these patents\footnote{http://swpat.ffii.org/papri/bpatg17-suche02/index.de.html}''.  This would protect the interests of patent holders and patent lawyers, i.e. the people whom the Commission called ``an economic majority'', discarding the evidence against software patents provided by 94\percent{} of the respondents to its consultation on software patents\footnote{http://swpat.ffii.org/papri/eukonsult00/index.en.html}.

O programa em Bruxelas ser\'{a} aproximadamente o que se segue, com mais detalhes comunicados em breve:

\begin{center}
\begin{center}
\begin{tabular}{|C{29}|C{29}|C{29}|}
\hline
When & Where & Subject\\\hline
12.00-14.00 & Place du Luxembourg & Performance
\begin{itemize}
\item
Pantomime

\item
Balloons

\item
Speeches
\end{itemize}\\\hline
14.00-16.00 & EP A1E1 & Conference\par

Hosts
\begin{itemize}
\item
Bart Staes (MEP, NL, VERD)

\item
Johanna Boogerd (MEP, BE, ELDR)

\item
Heidi R\"{u}hle (MEP, DE, VERD)
\end{itemize}\par

Speakers
\begin{description}
\item[Henk Barendregt:]\ NL, Chair Foundation of Mathematics and Computer Science Nijmegen University
Patentability of Software: a perspective from a computer science\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/komp0305/index.en.html}
\item[Reinier Bakels:]\ NL, University of Amsterdam, Institute for Information Law\footnote{http://swpat.ffii.org/penmi/2002/ivir08/index.en.html}
``The JURI Proposal: Clarification by Confusion?\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/dgiv0206/index.en.html}''
\item[Marco Schulze:]\ CEO, Nightlabs GmbH\footnote{http://www.nightlabs.de/}
Ticketing System Patents in Europe
\item[Hartmut Pilch:]\ DE, president of FFII
``Why Amazon One Click Shopping is Patentable under the Proposed EU Directive\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/index.en.html}''
\end{description}\par

[\dots]\\\hline
\end{tabular}
\end{center}
\end{center}

``Em Maio, numa confer\^{e}ncia sobre patentes de software de dois dias\footnote{http://swpat.ffii.org/penmi/2003/europarl/05/index.en.html}, no Parlamento Europeu e na sua proximidade atraiu 200 participantes. L\'{\i}deres de comunidades cient\'{\i}ficas e do mundo de neg\'{o}cios de software condenam a proposta de directiva sobre todos os aspectos. Contudo em Junho a Comiss\~{a}o dos Assuntos Jur\'{\i}dicos do Parlamento Europeu (JURI'' aprovou esta proposta com emendas extras que ainda a tornam pior), explica Henrion. ``Mais e mais gente come\c{c}a agora a ver isto de forma bem clara. Esperamos haver maior participa\c{c}\~{a}o desta vez.''

``Contudo a vasta maioria dos nossos apoiantes certamente n\~{a}o estar\'{a} na Pra\c{c}a do Luxemburgo a 27 de Agosto.  Aqueles que n\~{a}o puderem vir a Bruxelas deveriam manifestar-se on-line\footnote{http://swpat.ffii.org/girzu/jarco/index.de.html}, utilizando os seus servidores de www ou outros servi\c{c}os internet'', diz Hartmut Pilch, presidente do FFII. ``Propomos uma s\'{e}rie de formas de o fazer. Certamente haver\'{a} um modo para cada um. \'{E} melhor tornar o acesso \`{a} sua p\'{a}gina de www por uns dias do que perder a sua liberdade de publicar pelos pr\'{o}ximos dez anos. Note que se o relat\'{o}rio de McCarthy for aprovado sem emendas dr\'{a}sticas, programadores e Internet Service Providers ser\~{a}o regularmente processados por infrac\c{c}\~{a}o de patentes, se publicarem programas na Internet. A data limite para o escrut\'{\i}nio democr\'{a}tico \'{e} 1 de Setembro. 27 de Agosto \'{e} a sua \'{u}ltima oportunidade para fazer ouvir a sua v\'{o}z no processo Europeu de decis\~{a}o sobre patentes.''
\end{sect}

\begin{sect}{links}{Links Anotados}
\begin{itemize}
\item
{\bf {\bf AEL Big Demo 27 aug Wiki\footnote{http://wiki.ael.be/index.php/BigDemo27aug}}}

\begin{quote}
Hints on how to participate in the demo, needed equipment on site, who provides what, etc
\end{quote}
\filbreak

\item
{\bf {\bf Online Demonstration Against Software Patents\footnote{http://swpat.ffii.org/girzu/jarco/index.de.html}}}

\begin{quote}
We can show our concern by physical presence as well as by more or less gently blocking access to webpages in a concerted manner at certain times.
\end{quote}
\filbreak

\item
{\bf {\bf 2003/08 Letter to Software Creators and Users\footnote{http://swpat.ffii.org/xatra/parl038/index.en.html}}}

\begin{quote}
The European Parliament will, in its plenary session on September 1st, decide on a directive proposal which ensures that algorithms and business methods like Amazon One Click Shopping become patentable inventions in Europe.  This proposal has the backing of about half of the parliament.  Please help us make sure that it will be rejected.  Here are some things to do.
\end{quote}
\filbreak

\item
{\bf {\bf Software Patent Events Wednesday 2003/08/27 12.00-16.00\footnote{http://swpat.ffii.org/xatra/meps038/index.en.html}}}

\begin{quote}
Letter to Members of the European Parliament
\end{quote}
\filbreak

\item
{\bf {\bf 2003/08/25-9 BXL: Software Patent Directive Amendments\footnote{http://swpat.ffii.org/penmi/2003/europarl/08/index.en.html}}}

\begin{quote}
Members of the European Parliament are coming back to work on monday August 25th.  It is the last week before the vote on the Software Patent Directive Proposal.  We are organising a conference and street rally wednesday the 27th.  Some of our friends will moreover be staying in the parliament for several days.  Time to work decide on submission of amendments to the software patent directive proposal is running out.  FFII has proposed one set of amendments that stick as closely as possible to the original proposal while debugging and somewhat simplifying it.  An alternative small set of amendments would ``cut the crap'' and rewrite the directive from scratch.  We present and explain the possible approaches.
\end{quote}
\filbreak

\item
{\bf {\bf European Parliament Rejects Attempt to Rush Vote on Software Patent Directive\footnote{http://swpat.ffii.org/lisri/03/plen0626/index.en.html}}}

\begin{quote}
The European Parliament has postponed the vote on the software patent directive back to the original date of 1st of September, thereby rejecting initially successful efforts of its rapporteur Arlene McCarthy (UK Labour MEP of Manchester) and her supporters to rush to vote on June 30th, a mere twelve days after publication of the highly controversial report and ten days after the unexpected change of schedule.
\end{quote}
\filbreak

\item
{\bf {\bf JURI votes for Fake Limits on Patentability\footnote{http://swpat.ffii.org/lisri/03/juri0617/index.en.html}}}

\begin{quote}
The European Parliament's Committee for Legal Affairs and the Internal Market (JURI) voted on tuesday morning about a list of proposed amendments to the planned software patent directive.  It was the third and last in a series of committee votes, whose results will be presented to the plenary in early september.  The other two commissions (CULT, ITRE) had opted to more or less clearly exclude software patents.  The JURI rapporteur Arlene McCarthy MEP (UK socialist) also claimed to be aiming for a ``restrictive harmonisation of the status quo'' and ``exclusion of software as such, algorithms and business methods from patentability''.  Yet McCarthy presented a voting list to fellow MEPs which, upon closer look, turns ideas like ``Amazon One-Click Shopping'' into patentable inventions.  McCarthy and her followers rejected all amendment proposals that try to define central terms such as ``technical'' or ``invention'', while supporting some proposals which reinforce the patentability of software, e.g. by making publication of software a direct patent infringment, by stating that ``computer-implemented inventions by their very nature belong to a field of technology'', or by inserting new economic rationales (``self-evident'' need for Europeans to rely on ``patent protection'' in view of ``the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union'') into the recitals.  Most of McCarthy's proposals found a conservative-socialist 2/3 majority (20 of 30 MEPs), whereas most of the proposals from the other committees (CULT = Culture, ITRE = Industry) were rejected.  Study reports commissioned by the Parliament and other EU institutions were disregarded or misquoted, as some of their authors point out (see below).  A few socialists and conservatives voted together with Greens and Left in favor of real limits on patentability (such as the CULT opinion, based on traditional definitions, that ``data processing is not a field of technology'' and that technical invention is about ``use of controllable forces of nature''), but they were overruled by the two largest blocks.  Most MEPs simply followed the voting lists of their ``patent experts'', such as Arlene McCarthy (UK) for the Socialists (PSE) and shadow rapporteur Dr. Joachim Wuermeling (DE) for the Conservatives (EPP).  Both McCarthy and Wuermeling have closely followed the advice of the directive proponents from the European Patent Office (EPO) and the European Commission's Industrial Property Unit (CEC-Indprop, represented by former UK Patent Office employee Anthony Howard) and declined all offers of dialog with software professionals and academia ever since they were nominated rapporteurs in May 2002.
\end{quote}
\filbreak

\item
{\bf {\bf Why Amazon One Click Shopping is Patentable under the Proposed EU Directive\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/index.en.html}}}

\begin{quote}
According to the European Commission (CEC)'s Directive Proposal COM(2002)92 for ``Patentability of Computer-Implemented Inventions'' and the revised version approved by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI), algorithms and business methods such as Amazon One Click Shopping are without doubt patentable subject matter. This is because \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO).  The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf Call for Action\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/cpedu/index.en.html}}}

\begin{quote}
The European Commission's proposal for the patentability of software innovations requires a clear response from the European Parliament, the member state governments and other political players. Here is what we think should be done.
\end{quote}
\filbreak

\item
{\bf {\bf Abaixo assinado: Por Uma Europa Livre de Patentes para Software\footnote{http://petition.eurolinux.org/index.pt.html}}}
\filbreak

\item
{\bf {\bf FFII: Patentes de Software na Europa\footnote{http://swpat.ffii.org/index.pt.html}}}

\begin{quote}
Nos \'{u}ltimos anos o Gabinete Europeu de Patentes (GPO) (EPO -- European Patent Office) tem, contrariamente \`{a} letra e esp\'{\i}rito da lei vigente, concedido mais de 30000 patentes em regras de organiza\c{c}\~{a}o e c\'{a}lculo implementadas em computador (programas para computadores). Agora o movimento de patentes Europeu est\'{a} a pressionar para consolidar esta pr\'{a}ctica escrevendo uma nova lei. Os cidad\~{a}os e programadores europeus encaram riscos consider\'{a}veis. Aqui encontrar\'{a} a documenta\c{c}\~{a}o b\'{a}sica, come\c{c}ando por uma vis\~{a}o por alto e as \'{u}ltimas novidades.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{contact}
\begin{description}
\item[mail:]\ media at ffii org
\item[telefone:]\ Hartmut Pilch +49-89-18979927
Benjamin Henrion\footnote{http://bh.udev.org/} +32-10-454761
Mais contactos dispon\'{\i}veis sob pedido
\end{description}
\end{sect}

\begin{sect}{url}{URL permanente deste Press Release}
http://swpat.ffii.org/lisri/03/demo0819/index.pt.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

