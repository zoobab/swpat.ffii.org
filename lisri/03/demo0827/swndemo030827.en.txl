<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#oWW: More Contacts to be supplied upon request

#hWi: Blog with 28 photos from the demo of 2003/08/27 in Brussels.

#ehD: ZDNet on the BXL Demo

#seh: Polish newspaper article about software patents, motivated by the demo

#WtW: The author of a column in the weekly journal which was motivated by the BXL protests believes that anything useful must be patentable, but that on the other hand opensource software should enjoy an exception.

#otL: report about the BXL demo

#oud: report about the postponement of the decision in the Parliament

#jwW: Bruce Perens, famous Linux developper, says that the software patents are gradualy showing their effects and it is not clear whether independent software development will prevail against the threat, calls software developpers to support the Brussels demonstration.

#ejf: John C. Dvorak, famous author in prestigious US magazine, says that the year 2003 marks a new height in the crisis of the patent system, which has gone berserk to the extent that it is almost causing civil unrest.  Dvorak cites some texts from the ffii site and calls on readers to support the FFII.

#cBo: French news agency announces the planned %(q:demo in Brussels and virtual strike against software patentability)

#tt0: Report about swpat demo of 2003/08/27

#pot: Simple opinion poll on the software patent directive project

#uWr: Reports about %(q:opensource companies) protesting in Brussels, contains a lot of misinformation, much of it contributed by a lengthy interview with Joachim Wuermeling MEP.  FFII was not consulted.

#onn: An official EU news agency reports about the demo, with an interview of Peter Gerwinski from FFII

#Wem: Report about the successes of the Demo in BXL, focussing on the rise in number of petition signatures.

#oWc: During a conference in the European Parliament on Wednesday 14.00-16.00, Reinier Bakels, a dutch law scholar who had written a study on the directive at the order of the European Parliament, criticised: %(bc:This directive proposal brings no clarity and no harmonisation.  It is unclear and contradictory both on its aims and on the means of achieving these aims.  The European Parliament can not be expected to repair such a fundamentally broken directive proposal.  The best thing the Parliament can do is to send this proposal back to the Commission and demand that an interdisciplinary group of experts should work out a new proposal.)  Hartmut Pilch, president of FFII and spokesperson of the Eurolinux Alliance, agreed to this, but added:  %(bc:We hope that MEPs can, during the coming 3 weeks, understand that almost every single article and every single recital of this directive needs major amendments, if a clear limitation of patentability is to be achieved.   We have proposed a %(cp:set of amendments) which could do the job and has received %(ca:backing by a large part ot the organised software community).  Many of these amendments have already been or are being tabled by MEPs from various political groups.  By voting for these amendments, MEPs can prompt the Commission and the Council to work out a new proposal, this time based on a serious assessment of the interests of all parties and a verifiable solution to the problems, without any more doublespeak or ambiguous terminology.)

#WeW: The Parliament was already %(dv:divided) in June, when it %(pp:postponed) the decision to September.  The massive protest among computer professionals, software companies and computer users, and its echo in the press on the Internet, radio and television, appear to have further eroded the directive's support in the European Parliament and encouraged various party groups to come out with new amendment proposals.  The presidents of the transnational groups decided in a meeting on thursday afternoon to postpone the debate again from the planned European Parliament plenary session of Monday September 1st. The debate and vote may now take place in the next session (September 22-26) or at a later date, subject to decision next week.  The directive has been controversial since its publication on 2002-02-20, and decisions have been delayed already seven times from the initially scheduled vote of 2002-12-16.

#etb: The strike coincided with initiatives by new players, including national associations of SMEs, national labor unions, the internet sections of the French Parti Socialiste and German Social Democratic Party, and a %(ek:group of economists), all of whom sent letters to members of the European Parliament, warning them of faulty reasoning in the JURI report and catastrophic consequences for the European economy.

#WLl: In spite of the short notice (it was only announced one week in advance), the online demonstration, calling to substitute homepages by a protest page for all of Wednesday, was followed by over 2800 websites. Among those were important websites such as those of the biggest Spanish labour union Comisiones Obreras, the Andalusian CGT union, the French SPECIS; large associations of computer professionals such as ATI.es, AI2 and Prosa.dk; user associations like SSLUG, Hispalinux, Asociación de Internautas, AFUL and GUUG; software projects like Apache (developers of the most used web server in the world, with over 25 million installations), PHP (a very popular programing language), the two main free desktop projects (KDE and GNOME); operating system distributions LinEx, Slackware, Debian, Knoppix and Mandrake; civil rights associations, distributed development platform Savannah (hosting over 1500 projects), companies, weblogs, personal websites...

#lde: Citizens demonstrated in front of the European Parliament, wearing black t-shirts and launching black balloons %(q:to symbolise their sorrow for the innovation that the EU would lose if it approved a monopoly regime on computer based solutions as the Commission and the JURI report propose), as one of the organisers explained. Surrounded by banners and patent tombstones, several speeches were held and a pantomime play was performed which showed the EU bureaucracy helping wealthy corporations to strangle small innovative software enterprises.

#7Wr: Last Wednesday, August 27th, several organisations called for a demonstration in Brussels and for an on-line demonstration against the proposed EU %(q:software patent directive) COM(2002)92, euphemistically titled %(q:on the patentability of computer-implemented inventions).

#descr: On Aug 28th, the European Parliament postponed its vote on the proposed EU Software Patent Directive.  The day before, approximately 500 persons had gathered for a rally beside the Parliament in Brussels, accompanied by an online demonstration involving more than 2000 websites.  The events in and near the Parliament were reported extensively covered in the media, including tv and radio, all over Europe and beyond.  Within a few days, the petition calling the European Parliament to reject software patentability accumulated 50,000 new signatures.

#title: EU Software Patent Plans Shelved Amid Massive Demonstrations

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swndemo030827 ;
# txtlang: en ;
# multlin: t ;
# End: ;

