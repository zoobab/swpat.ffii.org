<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: Het Europees Octrooibureau (EOB) in München heeft onlangs een patent toegekend aan Amazon dat alle door computers uitgevoerde methodes om een geschenk automatisch aan een derde toe te sturen, dekt. Dit octrooi is een afstammeling van het beruchte %(q:Amazon Eén Klik Octrooi) dat toegekend werd in de VSA, maar met een nog meer allesomvattende omschrijving dan de VS-versie. In 2001 werd de oorspronkelijke octrooiaanvraag door Amazon ingetrokken en vervangen door twee nieuwe aanvragen, waarvan één ondertussen is toegekend en de andere nog steeds loopt. De in München gevestigde Stichting voor een Vrije Informatieinfrastructuur (Foundation for a Free Information Infrastructure, FFII) ontdekte dit tijdens een routineonderzoek voor een document met als titel %(q:Waarom Amazon Eén Klik Kopen patenteerbaar is onder de Voorgestelde EU Softwarepatent-Directieve). Het document toont aan dat het EOB zijn eigen regels heeft gemaakt om systematisch duizenden patenten op algoritmes en zakenmethodes te kunnen toekennen, zoals dit ook in de VS gebeurt. De voorgestelde EU richtlijn i.v.m. software patenten die momenteel in het Europees Parlement ter discussie staat, zou precies deze EOB-regels opleggen aan de Europese nationale gerechtshoven. Niettemin was zelfs FFII verbaasd te ontdekken dat het EOB een patent van deze omvang toekende aan Amazon.
#title: Amazon Aankoopmethode Gepatenteerd in Europa
#agu: Achtergrondinformatie
#media: Media Contacten
#ffii: Over FFII
#dokurl: Permanente URL van dit Persbericht
#nnW: Laura Creighton, een software-ondernemer en risico-investeerder uit Zweden, geeft volgende commentaar:
#nWy: %(AM) en haar medestanders hebben herhaaldelijk gesteld dat onder de EOB-doctrine die ze voorstellen, iets als het Amazon Eén Klik Octrooi  %(q:onmogelijk) of %(q:hoogst onwaarschijnlijk) zou zijn. Ik heb telkens opnieuw gevraagd:  %(q:Als u oprecht wenst dat zakenmethodes niet patenteerbaar zijn, toon dan alstublieft aan hoe uw wet de nodige garanties biedt om dit te voorkomen.) Ze hebben niettemin geweigerd om uit te leggen waar deze garanties te vinden zijn. 
#pre: FFII-voorzitter Hartmut Pilch verklaart:
#tuE: Het Amazon Octrooi op het Bestellen van Geschenken werd toegekend na jarenlang diepgaand onderzoek van de "stand van de techniek". Gezien het publieke protest dat Amazon wereldwijd veroorzaakt heeft, zou men verwachten het EOB zijn best zou hebben gedaan om te argumenteren dat %(q:de nieuwheid van de bekendgemaakte methode een technische bijdrage mist). Een geschenk voor een vriend bestellen is allesbehalve nieuw, en de hoofdbijdrage behandelt nog geen methode om het aantal muisklikken dat daarvoor nodig is te verminderen. Ze dekt enkel op een zeer algemene manier het proces van automatische aflevering van geschenken. Dit illustreert mooi wat we %(et:elders) haarfijn uitgewerkt hebben: de EOB-doctrine, en daarmee de door McCarthy voorgestelde richtlijn, verzekert dat algoritmes en zakenmethodes zoals Amazon Eén Klik Kopen ontegensprekelijk patenteerbare uitvindingen worden en ontegensprekelijk voldoen aan de valse vereiste dat %(q:de nieuwheid een technische bijdrage moet leveren).
#oWW: Meer Contactpersonen worden op verzoek verschaft

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swnamaz030818 ;
# txtlang: nl ;
# End: ;

