<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: L'Office Européen des Brevets (OEB) a récemment accordé à Amazon un brevet couvrant toute méthode informatique permettant la livraison automatique de présents à une tierce personne.  Ce brevet est un descendant direct du fameux %(q:Brevet Amazon One Click Shopping) qui a été accordé à Amazon aux Etats-Unis mais ses revendications sont bien plus étendues que celles du brevet original.  En 2001, la demande de brevet originale d'Amazon avait été retirée pour être remplacée par deux nouvelles demandes dont une a été accordée et l'autre est toujours en cours d'étude.  La Fondation pour une Infrastructure Informatique Libre (Foundation for a Free Information Infrastructure - FFII), basée à Munich (Allemagne) a découvert ces faits durant une recherche de routine sur un document intitulé %(q:Pourquoi la méthode Amazone d'achats en un click est brevetable avec la Proposition de Directive européenne sur les brevets logiciels). Ce document montre que l'OEB a défini ses propres règles pour accorder systématiquement des milliers de brevets de style américains sur les algorithmes et sur les méthodes d'affaires (dites %(q:business methods)) et que la proposition de Directive sur la brevetabilité des logiciels sur laquelle le Parlement européen doit se prononcer le 1er septembre imposerait précisément ces règles aux courts de justices nationales d'Europe. Cependant, même la FFII a semblé surprise de découvrir que l'OEB accordait un brevet aussi vague à Amazon.
#title: La méthode de commandes d'Amazon brevetée en Europe
#agu: Contexte
#media: Contacts presse
#ffii: Au sujet de la FFII
#dokurl: URL permanente de ce Communiqué de Presse
#nnW: La chef d'entreprise informatique et investisseuse Laura Creighton, vivant en Suède commente la nouvelle en ces termes:
#nWy: %(AM) et ses alliés on régulièrement affirmé qu'avec les règles qu'ils proposent pour l'OEB, un brevet tel que le Brevet One-Click Shopping d'Amazon serait %(q:impossible) ou %(q:hautement improbable).  Je leur ai demandé à maintes et maintes reprises demandé : %(q:Si vous espérez sincèrement que les méthodes commerciales restent non-brevetables, montrez-moi où sont les crocs dans votre projet de loi qui l'empèchent.)  Ils ont toujours refusé d'expliquer ce qui donne de la force à leur proposition.
#pre: Hartmut Pilch, président de la FFII explique:
#tuE: Le brevet de Commande de Cadeaux Amazon a été accordé après des années d'examen et d'étude d'antécédents.  On aurait pu supposer que vu les hauts-cris causés par Amazon de part le monde, l'OEB ferait de son mieux pour montrer que la méthode d'Amazon %(q:ne fait aucune contribution d'ordre technique dans son approche inventive).  Il n'aurait pas été difficile de le dire.  Passer commande d'un cadeau pour un ami n'est vraiment pas quelque chose de nouveau, et la principale revendication ne prétend même pas indiquer une méthode pour réduire le nombre 'clicks souris' nécéssaires pour ce faire mais couvre juste de façon sommaire le processus automatisé de livraison de cadeaux.  Ceci illustre parfaitement ce que nous avons démontré en détail %(et:ailleurs): en accord avec les règles de McCarthy et de l'OEB, les algorithmes et méthodes commerciales telles que l'Amazon One-Click Shopping sont considérées tout naturellement comme des inventions brevetables et l'exigence d'une %(q:contribution d'ordre technique dans l'étape inventive) n'est visiblement pas un obstacle réel.
#oWW: Plus d'informations et de contacts sur demande

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnamaz030818 ;
# txtlang: fr ;
# End: ;

