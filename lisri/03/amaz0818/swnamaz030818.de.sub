\begin{subdocument}{swnamaz030818}{Amazon Kaufmethode in Europa Patentiert}{http://swpat.ffii.org/chronik/03/amaz0818/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Das Europ\"{a}ische Patentamt in M\"{u}nchen hat k\"{u}rzlich der Firma Amazon ein Patent erteilt, das alle computerisierten Verfahren der bestellten Lieferung von Geschenken an Dritte umfasst.  Dieses Patent ist ein Abk\"{o}mmling des in den USA erteilten ``Amazon One Click Patentes'', weist aber einen breiteren Anspruchsbereich als das Original auf.  Im Jahre 2001 wurde die urspr\"{u}ngliche Amazon-Anmeldung zur\"{u}ckgezogen und durch zwei neue Anmeldungen ersetzt, von denen inzwischen eine erteilt wurde, w\"{a}hrend die andere noch der Pr\"{u}fung harrt.  Der in M\"{u}nchen ans\"{a}ssige F\"{o}rderverein f\"{u}r eine Freie Informationelle Infrastruktur (FFII) stie{\ss} im Rahmen einer Routineuntersuchung f\"{u}r eine Dokumentation namens ``Warum Amazon One-Click Shopping gem\"{a}{\ss} EU-Richtlinienvorschlag patentf\"{a}hig ist'' auf diese Erkenntnisse.  Die Dokumentation zeigt, dass das EPA seine eigenen Regeln geschaffen hat, um systematisch Tausende von Patenten amerikanischen Stils auf Algorithmen und Gesch\"{a}ftsmethoden zu erteilen, und dass der Softwarepatent-Richtlinienvorschlag, \"{u}ber den das Europ\"{a}ische Parlament am 1. September abstimmen soll, Europas nationalen Gerichten genau dieses Regeln aufzwingen w\"{u}rde.  Dennoch zeigt sich sogar der FFII \"{u}berrascht, dass das EPA Amazon ein Patent dieser Breite gew\"{a}hrt hat.}
\begin{sect}{bakgr}{Hintergr\"{u}nde}
Die in Schweden ans\"{a}ssige Software-Unterneherin und -Investorin Laura Creighton kommentiert:
\begin{quote}
{\it Arlene McCarthy\footnote{http://swpat.ffii.org/akteure/amccarthy/index.en.html} und ihre Verb\"{u}ndete haben wiederholt erkl\"{a}rt, dass unter der von ihnen vorgeschlagenen EPA-Doktrin eine Patenterteilung f\"{u}r Amazons One-Click-Verfahren ``undenkbar'' oder ``h\"{o}chst unwahrscheinlich'' w\"{a}re.  Ich habe sie immer wieder gefragt: ``Wenn Sie ehrlich w\"{u}nschen, dass Gesch\"{a}ftsmethoden nicht patentierbar sein sollen, dann bitte ich Sie, mir zu zeigen, wo in Ihrem Gesetz die Z\"{a}hne sind, die das verhindern!''  Sie haben sich aber durchweg geweigert, mir zu erkl\"{a}ren, was ihrem Vorschlag diese Z\"{a}hne verleiht.}
\end{quote}

Der FFII-Vorsitzende Hartmut Pilch erkl\"{a}rt:
\begin{quote}
{\it Dieses Patent wurde nach jahrlanger ausgiebiger Pr\"{u}fung des Standes der Technik erteilt.  Man h\"{a}tte angesichts des von Amazon weltweit verursachten Skandals vermuten k\"{o}nnen, dass das EPA sich bem\"{u}hen w\"{u}rde, es wegen ``Fehlen eines technischen Beitrags im erfinderischen Schritt'' zur\"{u}ckzuweisen.  F\"{u}r einen Freund ein Geschenk zu bestellen ist ja kaum etwas neues, und der Hauptanspruch lehrt noch nicht einmal eine Methode zur Einsparung hierzu ben\"{o}tigter Mausklicks, sondern verbietet schlechthin jede automatisierte Lieferung von Geschenken.  Hierbei zeigt sich \"{u}berdeutlich, was wir anderswo\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/tech/index.en.html} ausf\"{u}hrlich nachweisen: Gem\"{a}{\ss} dem McCarthy-Richtlinienvorschlag m\"{u}ssen Algorithmen und Gesch\"{a}ftsmethoden wie Amazon One Click Shopping in Europa selbstverst\"{a}ndlich als patentf\"{a}hige Erfindungen gelten, und das Erfordernis eines ``technischen Beitrags im erfinderischen Schritt'' stellt kein ernsthaftes Hindernis dar.}
\end{quote}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Ein-Klick-Einkaufen\footnote{http://swpat.ffii.org/patente/wirkungen/1click/index.en.html}}}

\begin{quote}
Amazon (internet bookstore) received a US patent on reducing the need for data input in case of repeated ordering through a network like the WWW.  Based on this patent, Amazon sought an injunction against a competing bookstore.  Amazon had applied for the same patent at the EPO under EP0902381 in Sep. 1998 under the name ``Method and system for placing a purchase order via a communications network''.  By the time a search report was issued by the EPO, this patent had already aroused an uproar in the USA, leading to the discovery of new prior art, including similar patents which Amazon might be infringing.  The EPO found the Amazon method patentable in principle, but listed new prior art in an examination report of 2001.  Amazon decided to split the patent into two new applications.  Of these, one, EP0927945, a method for simplified ordering of articles via Internet, was granted by the EPO in May 2003.  The other is still pending.
\end{quote}
\filbreak

\item
{\bf {\bf Amazon Geschenkbestellung\footnote{http://swpat.ffii.org/patente/muster/ep927945/index.de.html}}}

\begin{quote}
Wenn Sie Ihren Internet-Kaufladen so programmieren m\"{o}chten, dass er Ihre Waren als Geschenke an Dritte liefert, t\"{a}ten Sie wohl gut daran, beizeiten die Firma Amazon um eine Lizenz zu bitten.  Dieses Patent, ein direkter Abk\"{o}mmling des 1-Click-Patentes, wurde vom Europ\"{a}ischen Patentamt im Mai 2003 erteilt.
\end{quote}
\filbreak

\item
{\bf {\bf Amazon 1Click\footnote{http://swpat.ffii.org/patente/muster/ep902381/index.en.html}}}

\begin{quote}
Amazons Patentanmeldung seiner One-Click-Shopping-Methode am EPA.  Die Anmeldung errichte das dritte Stadium der Pr\"{u}fung (A3), d.h. sie wurde als patentf\"{a}hige Erfindung anerkannt und eine volle Neuheitspr\"{u}fung wurde durchgef\"{u}hrt.  Im August 2001 wurde die Anmeldung in zwei neue Anmeldungen aufgeteilt.  Von diesen wurde eine anschlie{\ss}en erteilt, w\"{a}hrend die andere noch auf Pr\"{u}fung wartet.
\end{quote}
\filbreak

\item
{\bf {\bf Warum Amazon One Click Shopping gem\"{a}{\ss} EU-Richtlinienvorschlag patentf\"{a}hig ist\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/tech/index.en.html}}}

\begin{quote}
Gem\"{a}ss des Richtlinienvorschlags COM(2002)92 der Europ\"{a}ischen Kommission (CEC) f\"{u}r ``Patentierbarkeit Computer-Implementierter Erfindungen'' und die \"{u}berarbeitete Version genehmigt durch das Komitee f\"{u}r Rechtsangelegenheiten und Binnenmarkt (JURI) des Europa Parlaments, sind Algorithmem und Gesch\"{a}ftmethoden, wie etwa Amazon One Click Shopping, ohne Zweifel als patentierbare Gegenst\"{a}nde zu betrachten. Die ist so weil \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO). The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf FFII: Logikpatente in Europa\footnote{http://swpat.ffii.org/index.de.html}}}

\begin{quote}
In den letzten Jahren hat das Europ\"{a}ische Patentamt (EPA) gegen den Buchstaben und Geist der geltenden Gesetze \"{u}ber 30.000 Patente auf computer-eingekleidete Organisations- und Rechenregeln (laut Gesetz \emph{Programme f\"{u}r Datenverarbeitungsanlagen}, laut EPA-Neusprech von 2000 ``computer-implementierte Erfindungen'') erteilt.  Nun m\"{o}chte Europas Patentbewegung diese Praxis durch ein neues Gesetz festschreiben.  Europas Programmierer und B\"{u}rger sehen sich betr\"{a}chtlichen Risiken ausgesetzt.  Hier finden Sie die grundlegende Dokumentation zur aktuellen Debatte, ausgehend von einer Kurzeinf\"{u}hrung und \"{U}bersicht \"{u}ber die neuesten Entwicklungen.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{Medienkontakte}
\begin{description}
\item[E-Post:]\ media at ffii org
\item[Telefon:]\ Hartmut Pilch +49-89-18979927
Weitere Kontakte auf Anfrage
\end{description}
\end{sect}

\begin{sect}{url}{Permanente Netzadresse dieser Presseerkl\"{a}rung}
http://swpat.ffii.org/chronik/03/amaz0818/index.de.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

