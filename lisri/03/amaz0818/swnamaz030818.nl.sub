\begin{subdocument}{swnamaz030818}{Amazon Aankoopmethode Gepatenteerd in Europa}{http://swpat.ffii.org/lisri/03/amaz0818/index.nl.html}{Werkgroep\\swpatag@ffii.org\\Nederlandse versie 2003/07/15 door Reinout Van Schouwen\footnote{http://www.cs.vu.nl/~reinout/}}{Het Europees Octrooibureau (EOB) in M\"{u}nchen heeft onlangs een patent toegekend aan Amazon dat alle door computers uitgevoerde methodes om een geschenk automatisch aan een derde toe te sturen, dekt. Dit octrooi is een afstammeling van het beruchte ``Amazon E\'{e}n Klik Octrooi'' dat toegekend werd in de VSA, maar met een nog meer allesomvattende omschrijving dan de VS-versie. In 2001 werd de oorspronkelijke octrooiaanvraag door Amazon ingetrokken en vervangen door twee nieuwe aanvragen, waarvan \'{e}\'{e}n ondertussen is toegekend en de andere nog steeds loopt. De in M\"{u}nchen gevestigde Stichting voor een Vrije Informatieinfrastructuur (Foundation for a Free Information Infrastructure, FFII) ontdekte dit tijdens een routineonderzoek voor een document met als titel ``Waarom Amazon E\'{e}n Klik Kopen patenteerbaar is onder de Voorgestelde EU Softwarepatent-Directieve''. Het document toont aan dat het EOB zijn eigen regels heeft gemaakt om systematisch duizenden patenten op algoritmes en zakenmethodes te kunnen toekennen, zoals dit ook in de VS gebeurt. De voorgestelde EU richtlijn i.v.m. software patenten die momenteel in het Europees Parlement ter discussie staat, zou precies deze EOB-regels opleggen aan de Europese nationale gerechtshoven. Niettemin was zelfs FFII verbaasd te ontdekken dat het EOB een patent van deze omvang toekende aan Amazon.}
\begin{sect}{bakgr}{Achtergrondinformatie}
Laura Creighton, een software-ondernemer en risico-investeerder uit Zweden, geeft volgende commentaar:
\begin{quote}
{\it Arlene McCarthy\footnote{http://swpat.ffii.org/gasnu/amccarthy/index.en.html} en haar medestanders hebben herhaaldelijk gesteld dat onder de EOB-doctrine die ze voorstellen, iets als het Amazon E\'{e}n Klik Octrooi  ``onmogelijk'' of ``hoogst onwaarschijnlijk'' zou zijn. Ik heb telkens opnieuw gevraagd:  ``Als u oprecht wenst dat zakenmethodes niet patenteerbaar zijn, toon dan alstublieft aan hoe uw wet de nodige garanties biedt om dit te voorkomen.'' Ze hebben niettemin geweigerd om uit te leggen waar deze garanties te vinden zijn. }
\end{quote}

FFII-voorzitter Hartmut Pilch verklaart:
\begin{quote}
{\it Het Amazon Octrooi op het Bestellen van Geschenken werd toegekend na jarenlang diepgaand onderzoek van de ``stand van de techniek``. Gezien het publieke protest dat Amazon wereldwijd veroorzaakt heeft, zou men verwachten het EOB zijn best zou hebben gedaan om te argumenteren dat ``de nieuwheid van de bekendgemaakte methode een technische bijdrage mist''. Een geschenk voor een vriend bestellen is allesbehalve nieuw, en de hoofdbijdrage behandelt nog geen methode om het aantal muisklikken dat daarvoor nodig is te verminderen. Ze dekt enkel op een zeer algemene manier het proces van automatische aflevering van geschenken. Dit illustreert mooi wat we elders\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/index.en.html} haarfijn uitgewerkt hebben: de EOB-doctrine, en daarmee de door McCarthy voorgestelde richtlijn, verzekert dat algoritmes en zakenmethodes zoals Amazon E\'{e}n Klik Kopen ontegensprekelijk patenteerbare uitvindingen worden en ontegensprekelijk voldoen aan de valse vereiste dat ``de nieuwheid een technische bijdrage moet leveren''.}
\end{quote}
\end{sect}

\begin{sect}{links}{koppelingen met voetnoten}
\begin{itemize}
\item
{\bf {\bf One-Click Shopping\footnote{http://swpat.ffii.org/pikta/xrani/1click/index.en.html}}}

\begin{quote}
Amazon (internet bookstore) received a US patent on reducing the need for data input in case of repeated ordering through a network like the WWW.  Based on this patent, Amazon sought an injunction against a competing bookstore.  Amazon had applied for the same patent at the EPO under EP0902381 in Sep. 1998 under the name ``Method and system for placing a purchase order via a communications network''.  By the time a search report was issued by the EPO, this patent had already aroused an uproar in the USA, leading to the discovery of new prior art, including similar patents which Amazon might be infringing.  The EPO found the Amazon method patentable in principle, but listed new prior art in an examination report of 2001.  Amazon decided to split the patent into two new applications.  Of these, one, EP0927945, a method for simplified ordering of articles via Internet, was granted by the EPO in May 2003.  The other is still pending.
\end{quote}
\filbreak

\item
{\bf {\bf Amazon Gift Ordering\footnote{http://swpat.ffii.org/pikta/mupli/ep927945/index.en.html}}}

\begin{quote}
If you want to program your online shop so that it delivers your articles as gifts to a third person specified by the customer, you might want to negotiate with Amazon Inc for a license.  This patent, which is a direct descendant of Amazon's One Click Patent, was granted by the European Patent Office (EPO) in May 2003.
\end{quote}
\filbreak

\item
{\bf {\bf Amazon 1Click\footnote{http://swpat.ffii.org/pikta/mupli/ep902381/index.en.html}}}

\begin{quote}
Amazon's application for a patent on its One-Click Shopping method at the EPO.  The application reached the third stage of examination (A3), i.e. it was recognised as referring to a patentable invention and a full novelty examination was conducted.  In 2001 the patent application was split into two new applications, of which one was granted and one is still pending.
\end{quote}
\filbreak

\item
{\bf {\bf Waarom Amazon One Click Shopping Patenteerbaar is onder de Voorgestelde EU Richtlijn\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/index.en.html}}}

\begin{quote}
Volgens het voorstel COM(2002)92 van de Europese Commissie (CEC) over de ``Octrooieerbaarheid van in Computers Ge\"{\i}mplementeerde Uitvindingen'' en de herziene versie die goedgekeurd is door de Commissie voor Juridische Zaken en Interne Markt van het Europees Parlement (JURI), zijn algoritmen en wijzen van handelsvoering zoals Amazon One Click Shopping zonder twijfel patenteerbaar. Dit komt door het feit dat \begin{enumerate}
\item
Elke ``in een computer ge\"{\i}mplementeerde'' innovatie beschouwd wort als een patenteerbare ``uitvinding''.

\item
De extra voorwaarde om een ``technische bijdrage in de nieuwheid'' te leveren, betekent niet wat de meeste mensen denken.

\item
Het voorstel tot richtlijn is expliciet gericht op het wettelijk vastleggen van de praktijk van het Europees Octrooibureau (EOB).  Het EOB heeft reeds duizenden patenten toegekend op algoritmen en wijzen van handelsvoering die zeer analoog aan het Amazon One Click Shopping patent zijn.

\item
CEC en JURI hebben verdere achterpoortjes ingebouwd zodat, zelfs wanneer sommige bepalingen geamendeerd worden door het Europees Parlement, onbeperkte octrooieerbaarheid gegarandeerd blijft.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf FFII: Softwarepatenten in Europa\footnote{http://swpat.ffii.org/index.nl.html}}}

\begin{quote}
Gedurende de laatste paar jaar heeft het Europees Octrooibureau (EOB) tegen de letter en de geest van de bestaande wet in, meer dan 30.000 patenten uitgegeven aan wat de wet noemt ``programma's voor computers'' en wat het Europees Patentburo ``computer-ge\"{\i}mplementeerde uitvindingen'' is gaan noemen in in 2000: software in een context van patentaanspraken, oftewel regels van organisatie en berekening gevat in termen van algemene computerapparatuur. Op dit moment is de Europese patentgemeenschap druk aan het uitoefenen om de recente praktijk van het EOB op te leggen door een nieuwe wet te schrijven. Europa's programmeurs en burgers lopen een aanzienlijk risico. Hier vindt u de basisdocumentatie, beginnend met een kort overzicht en het laatste nieuws.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{Media Contacten}
\begin{description}
\item[mail:]\ media at ffii org
\item[telefoon:]\ Hartmut Pilch +49-89-18979927
Meer Contactpersonen worden op verzoek verschaft
\end{description}
\end{sect}

\begin{sect}{url}{Permanente URL van dit Persbericht}
http://swpat.ffii.org/lisri/03/amaz0818/index.nl.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

