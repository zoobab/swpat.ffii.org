\begin{subdocument}{swnamaz030818}{Amazon Ordering Method Patented in Europe}{http://swpat.ffii.org/journal/03/amaz0818/index.en.html}{Workgroup\\swpatag@ffii.org}{The European Patent Office (EPO) in Munich has recently granted a patent to Amazon which covers all computerised methods of automatically delivering a gift to a third party.  This patent is a descendant of the famous ``Amazon One Click Patent'' granted in the USA, but with a broader claim scope than the original US version.  In 2001 the original Amazon Patent Application was withdrawn and replaced with two new applications, of which one has meanwhile been granted while the other is still pending.  The Munich-based Foundation for a Free Information Infrastructure (FFII) found these facts during routine research for a documentation named ``Why Amazon One-Click Shopping is Patentable under the Proposed EU Software Patent Directive''.  The documentation shows that the EPO has created its own rules to systematically grant thousands of US-style patents on algorithms and business methods, and that the proposed EU Software Patent Directive on which the European Parliament is scheduled to vote on September 1st would impose exactly these EPO rules on Europe's national courts.  Yet even the FFII appeared surprised to find that the EPO granted Amazon a patent of this breadth.}
\begin{sect}{bakgr}{Backgrounds}
Laura Creighton, a software entrepreneur and venture capitalist living in Sweden, comments:
\begin{quote}
{\it Arlene McCarthy\footnote{http://swpat.ffii.org/players/amccarthy/index.en.html} and her allies have repeatedly stated that under the EPO doctrine which they are proposing, something like Amazon's One Click Patent would be ``impossible'' or ``highly unlikely''.  I have again and again asked them: ``If you sincerely wish that business methods be unpatentable,  please show me where are the teeth in your law which will prevent this.''  They have however refused to explain what gives their proposal the teeth.}
\end{quote}

FFII president Hartmut Pilch explains:
\begin{quote}
{\it The Amazon Gift Ordering patent was granted after years of in-depth examination of prior art.  One could have expected that, given the public outcry which Amazon has caused world-wide, the EPO would do its best to say that Amazon's method ``does not make a technical contribution in its inventive step''.  It shouldn't have been difficult to say that.  Ordering a gift for a friend is hardly new, and the main claim does not even teach a method of reducing the number of mouse clicks needed in doing so, but just broadly covers the process of fully-automated delivery of gifts.  This illustrates nicely what we have documented in detail elsewhere\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/tech/index.en.html}: the EPO doctrine ensures that algorithms and business methods like Amazon One Click Shopping indisputably become patentable inventions, and the requirement of ``technical contribution in the inventive step'' is usually not a real obstacle.}
\end{quote}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf One-Click Shopping\footnote{http://swpat.ffii.org/patents/effects/1click/index.en.html}}}

\begin{quote}
Amazon (internet bookstore) received a US patent on reducing the need for data input in case of repeated ordering through a network like the WWW.  Based on this patent, Amazon sought an injunction against a competing bookstore.  Amazon had applied for the same patent at the EPO under EP0902381 in Sep. 1998 under the name ``Method and system for placing a purchase order via a communications network''.  By the time a search report was issued by the EPO, this patent had already aroused an uproar in the USA, leading to the discovery of new prior art, including similar patents which Amazon might be infringing.  The EPO found the Amazon method patentable in principle, but listed new prior art in an examination report of 2001.  Amazon decided to split the patent into two new applications.  Of these, one, EP0927945, a method for simplified ordering of articles via Internet, was granted by the EPO in May 2003.  The other is still pending.
\end{quote}
\filbreak

\item
{\bf {\bf Amazon Gift Ordering\footnote{http://swpat.ffii.org/patents/samples/ep927945/index.en.html}}}

\begin{quote}
If you want to program your online shop so that it delivers your articles as gifts to a third person specified by the customer, you might want to negotiate with Amazon Inc for a license.  This patent, which is a direct descendant of Amazon's One Click Patent, was granted by the European Patent Office (EPO) in May 2003.
\end{quote}
\filbreak

\item
{\bf {\bf Amazon 1Click\footnote{http://swpat.ffii.org/patents/samples/ep902381/index.en.html}}}

\begin{quote}
Amazon's application for a patent on its One-Click Shopping method at the EPO.  The application reached the third stage of examination (A3), i.e. it was recognised as referring to a patentable invention and a full novelty examination was conducted.  In 2001 the patent application was split into two new applications, of which one was granted and one is still pending.
\end{quote}
\filbreak

\item
{\bf {\bf Why Amazon One Click Shopping is Patentable under the Proposed EU Directive\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/tech/index.en.html}}}

\begin{quote}
According to the European Commission (CEC)'s Directive Proposal COM(2002)92 for ``Patentability of Computer-Implemented Inventions'' and the revised version approved by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI), algorithms and business methods such as Amazon One Click Shopping are without doubt patentable subject matter. This is because \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO).  The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf FFII: Software Patents in Europe\footnote{http://swpat.ffii.org/index.en.html}}}

\begin{quote}
For the last few years the European Patent Office (EPO) has, contrary to the letter and spirit of the existing law, granted more than 30000 patents on rules of organisation and calculation claimed in terms of general-purpose computing equipment, called ``programs for computers'' in the law of 1973 and ``computer-implemented inventions'' in EPO Newspeak since 2000.  Europe's patent movement is pressing to legitimate this practise by writing a new law.  Although the patent movement has lost major battles in November 2000 and September 2003, Europe's programmers and citizens are still facing considerable risks.  Here you find the basic documentation, starting from the latest news and a short overview.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{Media Contacts}
\begin{description}
\item[mail:]\ media at ffii org
\item[phone:]\ Hartmut Pilch +49-89-18979927
More Contacts to be supplied upon request
\end{description}
\end{sect}

\begin{sect}{url}{Permanent URL of this Press Release}
http://swpat.ffii.org/journal/03/amaz0818/index.en.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

