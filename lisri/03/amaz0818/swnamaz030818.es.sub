\begin{subdocument}{swnamaz030818}{M\'{e}todo de pedido de Amazon patentado en Europa.}{http://swpat.ffii.org/lisri/03/amaz0818/index.es.html}{Workgroup\\swpatag@ffii.org\\versio es 2002/04/24 de proinnova\footnote{http://www.proinnova.hispalinux.es/}}{La oficina de patentes europea (EPO) en Munich ha concedido recientemente una patente a Amazon que cubre todos los m\'{e}todos automatizados para entregar un regalo. Esta patente es una descendiente de la famosa patente del rat\'{o}n de ``Amazon One Click Patent'' concedida en los E.E.U.U., pero con un alcance m\'{a}s amplio que la versi\'{o}n original de los E.E.U.U. En 2001 el uso de la patente original de Amazon fue retirado y substituido por dos nuevos usos, de los cuales uno se ha concedido mientras que el otro sigue pendiente. La fundaci\'{o}n residente en Munich para una infraestructura libre de la informaci\'{o}n (FFII) encontr\'{o} estos hechos durante la investigaci\'{o}n rutinaria sobre la documentaci\'{o}n de ``Porqu\'{e} el metodo Amazon One-Click Shopping es patentable bajo la directiva de patentes propuesta per la EU''. La documentaci\'{o}n demuestra que el EPO ha creado sus propias reglas para conceder sistem\'{a}ticamente millares de patentes al estilo USA en algoritmos y m\'{e}todos de negocio, y que la directiva propuesta de patentes de software en la EU sobre la cual el Parlamento Europeo tiene programado votar el 1 de septiembre impondr\'{\i}a exactamente estas reglas de la EPO ante las cortes nacionales de Europa. A pesar de todo esto, incluso la FFII se ha sorprendido al encontrar que la EPO concedi\'{o} a Amazon una patente de esta magnitud.}
\begin{sect}{bakgr}{Backgrounds}
Laura Creighton, una analista de inversiones y especialista en software que vive en Suecia, comenta:
\begin{quote}
{\it Arlene McCarthy\footnote{http://swpat.ffii.org/gasnu/amccarthy/index.en.html} and her allies have repeatedly stated that under the EPO doctrine which they are proposing, something like Amazon's One Click Patent would be ``impossible'' or ``highly unlikely''.  I have again and again asked them: ``If you sincerely wish that business methods be unpatentable,  please show me where are the teeth in your law which will prevent this.''  They have however refused to explain what gives their proposal the teeth.}
\end{quote}

Hartmut Pilch presidente de FFII nos dice:
\begin{quote}
{\it La patente de Amazon fue concedida despu\'{e}s de a\~{n}os de profundo exam\'{e}n.  Uno habr\'{\i}a podido esperar con que, dado la protesta p\'{u}blica que Amazon ha causado por todo el mundo, la EPO har\'{\i}a su mejor trabajo para decir que el m\'{e}todo de Amazon ``carec\'{\i}a de cualquier contribuci\'{o}n t\'{e}cnica en su proceso de creaci\'{o}n''.  No deber\'{\i}a haber sido tan  dif\'{\i}cil decir eso.  Pedir un regalo para un amigo no es nada nuevo, y la petici\'{o}n no ense\~{n}a un m\'{e}todo para reducir el n\'{u}mero de los clics del rat\'{o}n necesarios para hacer esto, sino que apenas cubre el proceso de la entrega automatizada de regalos.  Esto ilustra lo que hemos documentado detalladamente en otras partes\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/index.en.html}: la doctrina de la EPO assegura que los algoritmos y metodos de negocio c\'{o}mo el Amazon One Click Shopping sean invenciones patentables y se conviertan de manera totalmente erronea en ``una contribuci\'{o}n t\'{e}cnica a un metodo de invenci\'{o}n''.}
\end{quote}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf One-Click Shopping\footnote{http://swpat.ffii.org/pikta/xrani/1click/index.en.html}}}

\begin{quote}
Amazon (internet bookstore) received a US patent on reducing the need for data input in case of repeated ordering through a network like the WWW.  Based on this patent, Amazon sought an injunction against a competing bookstore.  Amazon had applied for the same patent at the EPO under EP0902381 in Sep. 1998 under the name ``Method and system for placing a purchase order via a communications network''.  By the time a search report was issued by the EPO, this patent had already aroused an uproar in the USA, leading to the discovery of new prior art, including similar patents which Amazon might be infringing.  The EPO found the Amazon method patentable in principle, but listed new prior art in an examination report of 2001.  Amazon decided to split the patent into two new applications.  Of these, one, EP0927945, a method for simplified ordering of articles via Internet, was granted by the EPO in May 2003.  The other is still pending.
\end{quote}
\filbreak

\item
{\bf {\bf Amazon Gift Ordering\footnote{http://swpat.ffii.org/pikta/mupli/ep927945/index.en.html}}}

\begin{quote}
If you want to program your online shop so that it delivers your articles as gifts to a third person specified by the customer, you might want to negotiate with Amazon Inc for a license.  This patent, which is a direct descendant of Amazon's One Click Patent, was granted by the European Patent Office (EPO) in May 2003.
\end{quote}
\filbreak

\item
{\bf {\bf Amazon 1Click\footnote{http://swpat.ffii.org/pikta/mupli/ep902381/index.en.html}}}

\begin{quote}
Amazon's application for a patent on its One-Click Shopping method at the EPO.  The application reached the third stage of examination (A3), i.e. it was recognised as referring to a patentable invention and a full novelty examination was conducted.  In 2001 the patent application was split into two new applications, of which one was granted and one is still pending.
\end{quote}
\filbreak

\item
{\bf {\bf Why Amazon One Click Shopping is Patentable under the Proposed EU Directive\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/index.en.html}}}

\begin{quote}
According to the European Commission (CEC)'s Directive Proposal COM(2002)92 for ``Patentability of Computer-Implemented Inventions'' and the revised version approved by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI), algorithms and business methods such as Amazon One Click Shopping are without doubt patentable subject matter. This is because \begin{enumerate}
\item
Any ``computer-implemented'' innovation is in principle considered to be a patentable ``invention''.

\item
The additional requirement of ``technical contribution in the inventive step'' does not mean what most people think it means.

\item
The directive proposal explicitly aims to codify the practise of the European Patent Office (EPO).  The EPO has already granted thousands of patents on algorithms and business methods similar to Amazon One Click Shopping.

\item
CEC and JURI have built in further loopholes so that, even if some provisions are amended by the European Parliament, unlimited patentability remains assured.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf FFII: Software Patents in Europe\footnote{http://swpat.ffii.org/index.en.html}}}

\begin{quote}
For the last few years the European Patent Office (EPO) has, contrary to the letter and spirit of the existing law, granted more than 30000 patents on rules of organisation and calculation claimed in terms of general-purpose computing equipment, called ``programs for computers'' in the law of 1973 and ``computer-implemented inventions'' in EPO Newspeak since 2000.  Europe's patent movement is pressing to legitimate this practise by writing a new law.  Although the patent movement has lost major battles in November 2000 and September 2003, Europe's programmers and citizens are still facing considerable risks.  Here you find the basic documentation, starting from the latest news and a short overview.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{Media Contacts}
\begin{description}
\item[mail:]\ media at ffii org
\item[tel:]\ Hartmut Pilch +49-89-18979927
More Contacts to be supplied upon request
\end{description}
\end{sect}

\begin{sect}{url}{Permanent URL of this Press Release}
http://swpat.ffii.org/lisri/03/amaz0818/index.es.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

