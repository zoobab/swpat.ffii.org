<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#oWW: More Contacts to be supplied upon request

#tuE: The Amazon Gift Ordering patent was granted after years of in-depth examination of prior art.  One could have expected that, given the public outcry which Amazon has caused world-wide, the EPO would do its best to say that Amazon's method %(q:does not make a technical contribution in its inventive step).  It shouldn't have been difficult to say that.  Ordering a gift for a friend is hardly new, and the main claim does not even teach a method of reducing the number of mouse clicks needed in doing so, but just broadly covers the process of fully-automated delivery of gifts.  This illustrates nicely what we have documented in detail %(et:elsewhere): the EPO doctrine ensures that algorithms and business methods like Amazon One Click Shopping indisputably become patentable inventions, and the requirement of %(q:technical contribution in the inventive step) is usually not a real obstacle.

#pre: FFII president Hartmut Pilch explains:

#nWy: %(AM) and her allies have repeatedly stated that under the EPO doctrine which they are proposing, something like Amazon's One Click Patent would be %(q:impossible) or %(q:highly unlikely).  I have again and again asked them: %(q:If you sincerely wish that business methods be unpatentable,  please show me where are the teeth in your law which will prevent this.)  They have however refused to explain what gives their proposal the teeth.

#nnW: Laura Creighton, a software entrepreneur and venture capitalist living in Sweden, comments:

#dokurl: Permanent URL of this Press Release

#ffii: About the FFII

#media: Media Contacts

#agu: Backgrounds

#descr: The European Patent Office (EPO) in Munich has recently granted a patent to Amazon which covers all computerised methods of automatically delivering a gift to a third party.  This patent is a descendant of the famous %(q:Amazon One Click Patent) granted in the USA, but with a broader claim scope than the original US version.  In 2001 the original Amazon Patent Application was withdrawn and replaced with two new applications, of which one has meanwhile been granted while the other is still pending.  The Munich-based Foundation for a Free Information Infrastructure (FFII) found these facts during routine research for a documentation named %(q:Why Amazon One-Click Shopping is Patentable under the Proposed EU Software Patent Directive).  The documentation shows that the EPO has created its own rules to systematically grant thousands of US-style patents on algorithms and business methods, and that the proposed EU Software Patent Directive on which the European Parliament is scheduled to vote on September 1st would impose exactly these EPO rules on Europe's national courts.  Yet even the FFII appeared surprised to find that the EPO granted Amazon a patent of this breadth.

#title: Amazon Ordering Method Patented in Europe

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnamaz030818 ;
# txtlang: en ;
# multlin: t ;
# End: ;

