<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#sja: A choir of industrial patent lawyers, supported by Commissioner Bolkestein, has been calling for withdrawal of the directive project.  They say the European Parliament ruined its chance of democratic participation by failing to support the European Commission's proposal.

#Wal: JURI hat am 17. Juni %(jv:für weitere Ausdehnung der Patentierbarkeit und einenige Scheinbeschränkungen gestimmt).  Der JURI-Beschluss wurde am 24. September dem Plenum zur Entscheidung vorgelegt.  Zahlreiche Gruppen stellten erfolgreiche %(pl:Änderungsanträge).  Der resultierende %(ad:geänderte Richtlinienvorschlag) schützt die Freiheit des Publizierens und Interoperierens und schließt abstrakte Ideen, einschließlich Algorithmen und Geschäftsmethoden, von der Patentierbarkeit aus.

#cWi: Der %(cu:Kulturausschuss) und %(it:Industrieausschuss) des Europa-Parlamentes haben Anfang 2003 für Änderungsanträge gestimmt, die mehr oder weniger deutlich Software von der Patentierbarkeit ausschließen.

#sou: Die geplante Patentierbarkeit von Software wird unter anderem von dem %(cr:Ausschuss der Regionen der EU), dem %(es:Wirtschafts- und Sozialausschuss der EU), der %(fg:französische Regierung), dem %(di:Deutsche Industrie- und Handelskammertag), der deutsche %(mk:Monopolkommission), der %(ui:Kommission für Geistiges Eigentum der Britischen Regierung), der %(fe:Französische Staatliche Planungskommission), zahlreichen %(sk:wissenschaftlichen Studien), %(ls:30 führende Wissenschaftler) sowie %(ek:91% den Teilnehmer einer EU-Konsultation zum Thema) und ca %(ep:2000 Software-Unternehmen und %(NSIG) Unterzeichnern einer Petition an das Europa-Parlament) und %(pm:KMU-Verbänden mit insgesamt über %(NPME) Mitgliedsunternehmen) heftig kritisiert.

#ejr: Die um das EPA gescharte Gemeinde der Industriepatentanwälte dominiert seit Jahren die Patentpolitik der Europäischen Kommission ebenso wie der meisten nationalen Regierungen, der meisten großen Industrieverbände, des Europäischen Rates und weitgehend auch des %(ju:Rechtsausschusses des Europäischen Parlamentes).  In letzterem haben insbesondere die Abgeordneten %(LST) energisch jedem Versuch einer Begrenzung der Patentierbarkeit entgegengewirkt.

#0ge: Die Europäische Kommission hat %(pr:vorgeschlagen), die %(ep:derzeitigen klaren und europaweit einheitlichen gesetzliche Regelungen zur Begrenzung der Patentierbarkeit) (%(q:Mathematische Methoden, Algorithmen, Geschäftsmethoden, Programme für Datenverarbeitungsanlagen etc sind keine patentfähigen Erfindungen)) durch eine auf nationaler Ebene umzusetzende Richtlinie zu ersetzen, welche es nationalen Gerichten sehr schwer macht, Patente auf Algorithmen und Geschäftsmethoden wie Amazon One Click Shopping für nichtig zu erklären.  %(pi:30000 Patente dieser Art )hat das Europäische Patentamt (EPA) seit 1986 mehr oder weniger verdeckt und seit 1998 offen erteilt.  Im Gegenzug für die Abtretung formeller regelsetzender Kompetenzen nach Brüssel darf das Münchener Machtzentrum die materiellen Regeln der Patentierbarkeit ungestört so festsetzen, wie es ihm beliebt.  Europas Patentanwälte müssen demnach allerdings weiterhin einen geringfügig größeren rhetorischen Aufwand als ihre US-Kollegen betreiben: %(et:es muss eine %(q:computer-implementierte Erfindung) präsentiert werden, die %(q:in ihrem erfinderischen Schritt einen technischen Beitrag leistet)).

#descr: Die Situation vor der Abstimmung im Europäischen Parlament

#title: Derzeitige Situation in Europa

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwnSitu030923 ;
# txtlang: de ;
# multlin: t ;
# End: ;

