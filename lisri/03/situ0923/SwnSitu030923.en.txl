<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#sja: A choir of industrial patent lawyers, supported by Commissioner Bolkestein, has been calling for withdrawal of the directive project.  They say the European Parliament ruined its chance of democratic participation by failing to support the European Commission's proposal.

#Wal: JURI has %(jv:voted on June 17th in favor of further extensions to patentability and some fake limitations).  The JURI proposal was presented to the plenary for decision on %(js:24th of September).  Numerous groups tabled %(pl:amendments), which succeded.  The result is an %(ad:amended directive) in the sense of the CULT and ITRE decisions, which clearly upholds freedom of publication and interoperation and excludes abstract ideas, including algorithms and business methods, from patentability.

#cWi: The European Parliament's %(cu:Cultural Commission) and %(it:Industry Commission) have in early 2003 voted for amendments which more or less clearly exempt software from patentability.

#sou: The plans for patentability of software have been strongly criticised by the %(cr:Council of Regions of the EU), the %(es:Economic and Social Council of the EU), the %(fg:French Government), the %(di:German Chamber of Industry and Commerce), the %(mk:German Monopoly Commission), the %(ui:British Government's Intellectual Property Rights Commission), the %(fe:French Industrial Planning Commission), %(sk:numerous economic studies), %(ls:30 leading scientists), %(po:numerous politicians and political parties) as well as %(ek:91% of the participants in a EU consultation) and %(ep:more than %(NSIG) individual and 2000 corporate signatories of a petition to the European Parliament and %(pm:associations representing %(NPME) enterprises)).

#ejr: The EPO and its community of industrial patent lawyers, assembled in its Standing Advisory Committee, have for years been dominating patent policy not only of the European Commission but also of most national governments, most large industry associations, the %(dp:European Council) and the European Parliament's %(ju:Commission for Legal Affairs and the Internal Market).  In the latter, especially %(LST) have been firmly opposing all attempts to limit patentability.

#0ge: The European Commission has %(pr:proposed) to override the %(ep:current clear and uniform European patentability rules) (%(q:mathematical methods, schemes and rules for mental activity, methods of doing business and programs for computers are not patentable inventions)) and replace them by a set of nationally implementable rules which make it very difficult for national courts to reject patents on algorithms and business methods such as Amazon One Click Shopping.  %(pi:30,000 such patents) have already been granted by the %(ea:European Patent Office) (EPO), openly since 1998 and more or less covertly since 1986.  In return for transferring formal legislative competence to Brussels, the patent community's power center in Munich is officially authorised to set the substantive rules of patentability as it pleases.  European patent attorneys are however obliged to bear a slightly greater rhetorical burden than their US colleagues: %(et:they must present their algorithm as a %(q:computer implemented invention) which %(q:makes a technical contribution in its inventive step)).

#descr: Situation before the Vote in the European Parliament

#title: Current Situation in Europe

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwnSitu030923 ;
# txtlang: en ;
# multlin: t ;
# End: ;

