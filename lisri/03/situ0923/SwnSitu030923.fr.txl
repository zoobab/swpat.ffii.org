<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#sja: A choir of industrial patent lawyers, supported by Commissioner Bolkestein, has been calling for withdrawal of the directive project.  They say the European Parliament ruined its chance of democratic participation by failing to support the European Commission's proposal.

#Wal: le Comité des Affaires Juridiques %(jv:a voté le 17 juin pour une résolution législative que élargit encore la brevetabilité, tout en introduisant quelques réstrictions bidons).  Cette résolution va être présentée a la séance plénière du Parlament pour décision le %(js:2003-09-23).  Plusieurs groupes ont posé des %(pl:amendements).

#cWi: La %(cu:Commission de la Culture) et la %(it:Commission de l'Industrie) du Parlement Européen ont voté au début 2003 en faveur d'amendements qui excluent plus ou moins la brevetabilité du logiciel.

#sou: Se sont prononcés contre la brevetabilité du logiciel: le %(cr:Conseil des Régions de l'UE), le %(es:Conseil Économique et Social de l'UE), le %(fg:Gouvernement Français), la %(di:Chambre du Commerce et de l'Industrie Allemande), la  %(mk:Commission Anti-Monopole Allemande), la %(ui:Commission de la Propriété Intellectuelle du Gouvernment Britannique), le %(fe:Commissariat Général au Plan Français), de nombreuses %(sk:études économiques) ainsi que %(ek:91% des participants à la consultation de l'UE sur le sujet) et %(ep:2000 entreprises, %(NSIG) signataires d'une pétition soumise au Parlament Européen) et %(pm:associations de PME avec un total de plus de %(NPME) entreprises membres).

#ejr: L'OEB et sa communauté d'avocats en brevets des grandes groupes, assemblés dans le Conseil Consultatif de l'OEB, ont pendant des années dominé la politique du brevet non seulement de la Commission Européenne mais aussi de la plupart des gouvernements nationaux, de la plupart des associations d'industriels, du %(dp:Conseil de l'Union Européenne) et du %(ju:Comité des Affaires Juridiques et du Marché Intérieur du Parlement Européen).  Particulièrement dans le dernier cas les députés %(LST) travaillent avec acharnement contre toute limitation de la brevetabilité.

#0ge: La Commission Européenne a %(pr:proposé) de remplacer les %(ep:règles de brevetabilité actuelles) (%(q:les méthodes mathématiques, les méthodes commerciales et les programmes d'ordinateurs ne sont pas des inventions brevetables)) par une brevetabilité sans limite à l'américaine, comme cela a été %(pi:pratiqué) ouvertement envers la loi par l'Office Européen du Brevet (OEB) depuis 1998 et plus ou moins officieusement depuis 1986, en acceptant la délivrance de brevets portant sur des méthodes commerciales. En échange d'un transfert formel de compétence à Bruxelles, l' Office de Munich se voit officiellement autorisé à poser les règles de la brevetabilité comme il l'entend. Les conseils en brevets européens sont néanmoins obligés d'user de figures rhétoriques plus importantes que dans le système américain: ils doivent présenter leurs algorithmes comme des " inventions mises en oeuvres par ordinateur" qui présente une " contribution technique".

#descr: La Situation avant le vote au Parlement Européen

#title: Situation actuelle en Europe

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwnSitu030923 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

