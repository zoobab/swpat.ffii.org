\begin{subdocument}{bolk031105}{Bolkestein urges businessmen to step up lobbying %(q:to get key proposals adopted)}{http://swpat.ffii.org/lisri/03/bolk1105/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{The EU's press page has the text of a speech make by Bolkestein at the weekend, which nicely captures his world view.  The overall argument is revealing -- do what I say, or all the jobs go to Turkey, China and India.  He cites the Community patent, the biotech directive, recognition of professional qualifications, and the take-over bids directive, criticising Member States for holding up progress.  To deal with this, he urges his audience of businessmen to step up their lobbying efforts:  ``Fifth, there is a crucial role for business. Let me ask you a few questions. How many of you as business leaders are personally involved in the public affairs side of your companies? How many of you have entered political debates (e.g. by writing articles for newspapers''? How many of you are lobbying behind the scenes to get key proposals adopted? You need to be doing all these things.)}
\begin{sect}{text}{The Text}
The EU's press page has the text of a speech make by Bolkestein on the weekend, which nicely captures his world view.

\begin{itemize}
\item
{\bf {\bf Bolkestein urges businessmen to %(q:lobby behind the scenes to get key proposals adopted)\footnote{\url{http://europa.eu.int/rapid/start/cgi/guestfr.ksh?p_action.gettxt=gt&doc=SPEECH/03/507\%7C0\%7CRAPID&lg=EN&display=}}}}

\begin{quote}
In a speech delivered to an audience of business lobbies, EU Internal Market commissioner Bolkestein complaines that national governments are slow in going along with reforms needed to unify the European internal market and make Europe more competitive and tries to mobilise his allies, the industry lobbies, to step up the pressure on these national governments.
\end{quote}
\filbreak
\end{itemize}

The overall argument is revealing -- do what I say, or all the jobs go to Turkey, China and India.

He doesn't specifically mention software patents, though we can bet he sees them as part of the same cloth.

Instead he cites the Community patent, the biotech directive, recognition of professional qualifications, and the take-over bids directive, criticising Member States for holding up progress.

To deal with this, he urges his audience of businessmen to lobby more loudly and openly:

\begin{quote}
{\it Fifth, there is a crucial role for business. Let me ask you a few questions. How many of you as business leaders are personally involved in the public affairs side of your companies? How many of you have entered political debates (e.g. by writing articles for newspapers)? How many of you are lobbying behind the scenes to get key proposals adopted? You need to be doing all these things.}

{\it If reform is too sluggish in the EU, is it perhaps because Ministers do not feel under strong enough pressure to take the difficult decisions?}
\end{quote}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Frits Bolkestein and Software Patents\footnote{\url{http://localhost/swpat/gasnu/bolkestein/index.en.html}}}}

\begin{quote}
Internal Market Commissioner of the European Commission since 2000, leader of dutch right-wing liberal party VVD, known mainly by interventions in favor of the world's second largest pharmaceutical company, of whose supervisory board he is a member, and by various policies in favor of big business.  Ever since he took office, Bolkestein firmly committed himself to the agenda for legalisation of software patents in Europe and pushed this agenda through an unwilling European Commission.  Bolkestein saw himself forced to pay lipservice to the goals of his opponents and sell his directive as a means to achieve those goals.  When it became apparent that this strategy did not work, Bolkestein threatened the Parliament with removal of its competence of decision.  The Parliament was neither deceived nor intimidated.  A month later, Bolkestein's directorate lobbied the national ministers to ignore the Parliament's will and drive the directive project against the wall.  When asked for justification, officials of the directorate often point to ``the Commissioner'' who allegedly is insisting on this.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

