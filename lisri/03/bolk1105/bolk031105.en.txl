<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bolkestein urges businessmen to step up lobbying %(q:to get key
proposals adopted)

#descr: The EU's press page has the text of a speech make by Bolkestein at the
weekend, which nicely captures his world view.  The overall argument
is revealing -- do what I say, or all the jobs go to Turkey, China and
India.  He cites the Community patent, the biotech directive,
recognition of professional qualifications, and the take-over bids
directive, criticising Member States for holding up progress.  To deal
with this, he urges his audience of businessmen to step up their
lobbying efforts:  %(q:Fifth, there is a crucial role for business.
Let me ask you a few questions. How many of you as business leaders
are personally involved in the public affairs side of your companies?
How many of you have entered political debates (e.g. by writing
articles for newspapers)? How many of you are lobbying behind the
scenes to get key proposals adopted? You need to be doing all these
things.)

#ala: The EU's press page has the text of a speech make by Bolkestein on the
weekend, which nicely captures his world view.

#rWW: Bolkestein urges businessmen to %(q:lobby behind the scenes to get key
proposals adopted)

#bmd: In a speech delivered to an audience of business lobbies, EU Internal
Market commissioner Bolkestein complaines that national governments
are slow in going along with reforms needed to unify the European
internal market and make Europe more competitive and tries to mobilise
his allies, the industry lobbies, to step up the pressure on these
national governments.

#gWe: The overall argument is revealing -- do what I say, or all the jobs go
to Turkey, China and India.

#ita: He doesn't specifically mention software patents, though we can bet he
sees them as part of the same cloth.

#iqe: Instead he cites the Community patent, the biotech directive,
recognition of professional qualifications, and the take-over bids
directive, criticising Member States for holding up progress.

#Wol: To deal with this, he urges his audience of businessmen to lobby more
loudly and openly:

#Wos: Fifth, there is a crucial role for business. Let me ask you a few
questions. How many of you as business leaders are personally involved
in the public affairs side of your companies? How many of you have
entered political debates (e.g. by writing articles for newspapers)?
How many of you are lobbying behind the scenes to get key proposals
adopted? You need to be doing all these things.

#gsa: If reform is too sluggish in the EU, is it perhaps because Ministers
do not feel under strong enough pressure to take the difficult
decisions?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: bolk031105 ;
# txtlang: en ;
# multlin: t ;
# End: ;

