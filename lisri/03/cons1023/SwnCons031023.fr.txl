<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#srW: L'Office des brevets britannique pousse à une décision rapide, alors que  les représentants allemands insistent sur la nouveauté de la situation et que  des efforts supplémentaires sont nécessaires pour aboutir à une décision  acceptable par le Conseil et le Parlement.

#tas: Ils constatent l'abîme entre les positions du Parlement européen, de la  commission et du Conseil des ministres ; ceci implique d'abord que la  décision soit prise au niveau politique avant d'être finalisée au niveau des  %(q:experts en matière de brevets). La décision politique sera préparée par  le comité des représentants permanents (Coreper).

#cke: Le groupe de travail sur le brevet s'est réuni toute la journée pour  parler du brevet communautaire et de la directive  sur le logiciel.

#descr: Depuis le vote du Parlement européen, en septembre 2003, qui exclut  clairement la possibilité de breveter le logiciel, le mouvement pro brevets a  contre-attaqué au niveau du Conseil européen. Une directive européenne  nécessite l'accord du parlement et du conseil européen pour devenir une loi.  La position du Conseil sera préparée par des représentants des offices des  brevets le vendredi 23 octobre à Bruxelles. À cette réunion, les  représentants des offices des brevets pourraient proposer de ne pas tenir  compte des amendements du Parlement européen pour revenir aux textes initiaux  du Conseil et de la Commission qui permettaient une brevetabilité sans  restriction. Le gouvernement britannique, dont la politique est définie par  son office des brevets devrait appuyer cette position.

#title: Rencontre du groupe de travail sur le brevet du Conseil de l'UE

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: pleonard ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwnCons031023 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

