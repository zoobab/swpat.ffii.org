<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#srW: The UK Patent Office is pressing for a quick decision, whereas the German representatives stress that the situation is new and more time-consuming efforts are needed, if any workable co-decision is to be achieved between Council and Parliament.

#tas: They agreed that there is a gulf between the published positions of the European Parliament (EP), the Commission of the European Communities (CEC) and the EU Council of ministers (Consilium), and that this means that decisions must first be taken at the political level and only later at the %(q:patent expert) level.  The political decisions are to be prepared by Coreper, the Committee of permanent representatives.

#cke: The patent policy working group met all day to talk about the Community Patent and the Software Directive.

#descr: Since the European Parliament voted for clear exclusion of software from patentability in september 2003, the patent movement has been fighting back through the EU Council.  A EU directive can only become law if Council and Parliament agree.  The position of the Council will be prepared by a workgroup of national patent officials on thursday 2003-10-23 in Brussels.  At the meeting, the European Commission's patent officials may propose to disregard the European Parliament's amendments and go back to earlier texts from the Commission's and the Council's patent officials which allow unlimited patentability.  The UK government, whose patent policy is determined by its Patent Office, is expected to push for this option.

#title: EU Council Patent Workgroup Meeting

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwnCons031023 ;
# txtlang: en ;
# multlin: t ;
# End: ;

