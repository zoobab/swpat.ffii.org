\begin{subdocument}{SwnCons031023}{Rencontre du Groupe de Travail de Brevet du Conseil UE}{http://swpat.ffii.org/journal/03/cons1023/index.fr.html}{Groupe de travail\\swpatag@ffii.org\\version fran\c{c}aise 2003/11/04 par Michèle Garoche\footnote{http://micmacfr.homeunix.org}}{Since the European Parliament voted for clear exclusion of software from patentability in september 2003, the patent movement has been fighting back through the EU Council.  A EU directive can only become law if Council and Parliament agree.  The position of the Council will be prepared by a workgroup of national patent officials on thursday 2003-10-23 in Brussels.  At the meeting, the European Commission's patent officials may propose to disregard the European Parliament's amendments and go back to earlier texts from the Commission's and the Council's patent officials which allow unlimited patentability.  The UK government, whose patent policy is determined by its Patent Office, is expected to push for this option.}
\begin{sect}{intro}{introduction}
The patent policy working group met all day to talk about the Community Patent and the Software Directive.

They agreed that there is a gulf between the published positions of the European Parliament (EP), the Commission of the European Communities (CEC) and the EU Council of ministers (Consilium), and that this means that decisions must first be taken at the political level and only later at the ``patent expert'' level.  The political decisions are to be prepared by Coreper, the Committee of permanent representatives.

The UK Patent Office is pressing for a quick decision, whereas the German representatives stress that the situation is new and more time-consuming efforts are needed, if any workable co-decision is to be achieved between Council and Parliament.
\end{sect}

\begin{sect}{links}{Liens annot\'{e}s}
\begin{itemize}
\item
{\bf {\bf FFII UK: Lobbying the Council of Europe\footnote{http://www.ffii.org.uk/council.html}}}

\begin{quote}
How decisionmaking in the Council works and what this means for the amended software patent directive
\end{quote}
\filbreak

\item
{\bf {\bf Conseil de l'Union Europ\'{e}enne et Brevets Logiciels\footnote{http://swpat.ffii.org/acteurs/consilium/index.en.html}}}

\begin{quote}
Together with the European Commission and the European Parliament, the Council is one of the three pillars of the European Union, which jointly legislate in a \emph{co-decision procedure}.  It is a forum where the national governments and their specialised ministries meet.  The question of how to limit patentability is handled in the ``Council Working Party on Intellectual Property and Patents''.  This council has been holding increasingly frequent meetings to discuss the European Commission's proposal for a software patentability directive and come up with a counter-proposal.  The national delegations are mostly composed of national patent office representatives or people whose career path is confined to the national patent establishment and who are factually dependent on this establishment in many ways.  Some delegations, such as the french and belgians, have comprised independent delegates and been fairly critical of the CEC proposal.  Others have been even more pro-patent than the CEC.  All have focussed on textual questions and caselaw rather than on what kind of output they want from the legislation in terms of patents granted/rejected and economic policy objectives.
\end{quote}
\filbreak

\item
{\bf {\bf CEC 2003-10: Inacceptable Amendments\footnote{http://register.consilium.eu.int/pdf/en/03/st11/st11503.en03.pdf}}}

\begin{quote}
In a first reaction to the European Parliament's amendments, the European Commission's patent experts (i.e. the authors of the amended draft) list the amendments which they say are ``inacceptable to the Commission''.  The list is long.  It comprises all amendments that can limit patentability or patent enforcability in any way.  The only ``acceptable'' amendments are the cosmetic ones from JURI.  CEC does not give any reasoning as to why it can't accept the others.  This CEC statement was published on the Coucil website two weeks after the EP vote, shortly before a first meeting of the Council's ``Patent Working Party''.

voir aussi PE 2003-09-24: Directive Brevets Logiciels Amend\'{e}es\footnote{http://swpat.ffii.org/papiers/europarl0309/index.fr.html}
\end{quote}
\filbreak

\item
{\bf {\bf Heise 2003-10-23: EU-Parlament erhaelt Unterstuetzung bei Software-Patenten\footnote{http://www.heise.de/newsticker/data/anw-23.10.03-004/}}}

\begin{quote}
Quotes press release of DMMV, which claims to be the largest software industry association of Germany.  DMMV board member Rudolf Gallist, a former top manager of Microsoft Germany, applauds the European Parliament's decision, because it allows the software industry, in particular SMEs, to be free from the threat of patent infringement.  He says that the Parliament has struck a good compromise between the need of patenting in classical industries and the need to keep data processing free from patents.  The article also quotes Daniel Riek from Linux-Verband (association of Linux-related companies) who sharply criticises the Bitkom press release.

voir aussi Deutscher Multi-Media-Verband\footnote{http://swpat.ffii.org/acteurs/dmmv/index.de.html}
\end{quote}
\filbreak

\item
{\bf {\bf Heise 03-10-22: ``IT-Verband ruft EU auf den rechten Weg zur\"{u}ck''\footnote{http://www.heise.de/newsticker/data/anw-22.10.03-001/}}}

\begin{quote}
Bericht \"{u}ber Bitkom-PE und Warnung von FFII UK

voir aussi Bitkom-PE 2003-03-22: \footnote{http://www.bitkom.org/index.cfm?gbAction=gbcontentfulldisplay\&ObjectID=D321C079-46F6-4D51-9B08AA448674390A\&MenuNodeID=4C872DB6-8470-4B01-A36FD8C1EBA2E22D}
\end{quote}
\filbreak

\item
{\bf {\bf BMJ-PE 2003-09-26: Nur ABS, nicht Software als solche\footnote{http://swpat.ffii.org/papiers/europarl0309/bmj030926/index.de.html}}}

\begin{quote}
Kurz nach der Entscheidung des Europ\"{a}ischen Parlamentes f\"{u}r eine \"{A}nderung der geplanten Softwarepatent-Richtlinie nimmt das Bundesministerium der Justiz in sibyllinischer Manier Stellung.  Die Presse-Erkl\"{a}rung wendet sich schulmeisterlich belehrend gegen den Begriff ``Softwarepatente'' und behauptet, diese seien nie bef\"{u}rwortet worden und mit ``computer-implementierte Erfindung'' seien etwas ganz anderes gemeint, n\"{a}mlich nur ``Anti-Blockier-Systeme''.  Was noch zu der Kategorie ``ABS'' geh\"{o}rt und wie das BMJ verhindern will, dass allgemeine Datenverarbeitung im Gewande von Universalrechnern patentiert wird, verr\"{a}t das BMJ nicht.  Im Oktober 2002 hatte sich das BMJ im Rat f\"{u}r Programm-Anspr\"{u}che stark gemacht.  Ob das BMJ hieran festhalten will und wie es sich sonst gegen\"{u}ber dem Parlamentsvotum verhalten wird, beantwortet die Erkl\"{a}rung mit keinem Wort.  Dieser ausweichende Formeltext wurde w\"{o}rtlich im Namen der Ministerin an zahlreiche Fragesteller \"{u}bersandt.  Forderungen Tausender von Unterzeichnern nach Pr\"{a}zisierung der anzustrebenden Zielvorgaben hat das BMJ unbeantwortet gelassen.

voir aussi Qu'est-ce qu'une ``Invention mise en oeuvre par ordinateur'' ?\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/kinv/index.fr.html},PE 2003-09-24: Directive Brevets Logiciels Amend\'{e}es\footnote{http://swpat.ffii.org/papiers/europarl0309/index.fr.html},Minist\`{e}re F\'{e}d\'{e}ral de Justice et Brevets Logiciels\footnote{http://swpat.ffii.org/acteurs/bmj/index.de.html} et Conseil de l'Union Europ\'{e}enne et Brevets Logiciels\footnote{http://swpat.ffii.org/acteurs/consilium/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf PE 2003-09-24: Directive Brevets Logiciels Amend\'{e}es\footnote{http://swpat.ffii.org/papiers/europarl0309/index.fr.html}}}

\begin{quote}
Version consolidée des principales provisions (Art 1-6) de la Directive %(q:sur la brevetabilité des inventions mises en oevre par ordinateur) pour lesquelles le parlament européen a voté le 24 septembre de 2003.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

