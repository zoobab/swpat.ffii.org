\begin{subdocument}{SwnCons031023}{Treffen der Patent-Arbeitsgruppe des EU-Ministerrates}{http://swpat.ffii.org/log/03/cons1023/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org\\deutsche Version 2003/11/08 von PILCH Hartmut\footnote{http://www.ffii.org/~phm}}{Seitdem das Europ\"{a}ische Parlament im September f\"{u}r einen klaren Ausschuss von Software von der Patentierbarkeit abgestimmt hat, versucht die Patentbewegung, \"{u}ber den Rat der Europ\"{a}ischen Union, in dem die Fachminister gemeinsame Standpunkte erarbeiten, dagegen anzugehen.  Eine EU-Richtlinie kann nur Gesetz werden, wenn der Rat und das Parlament zustimmen.  Die Position des Rates wird von einer Arbeitsgruppe nationaler Patentverwaltungsbeamter am Donnerstag, den 23. Oktober in Br\"{u}ssel vorbereitet.  Die Patentbeamten der Europ\"{a}ischen Kommission unter Kommissar Bolkestein dr\"{a}ngen darauf, die Vorschl\"{a}ge des Parlaments beiseite zu schieben und und einen fr\"{u}heren Vorschlag des Rates wieder hervorzuholen, der grenzenlose Patentierbarkeit vorsieht.  Die britische Regierung, deren Position von ihrem Patentamt bestimmt wird, dr\"{a}ngt aktiv in diese Richtung.}
\begin{sect}{intro}{Einf\"{u}hrung}
Die Arbeitsgruppe traf sich den ganzen Tag, um \"{u}ber das Gemeinschaftspatent und die Software-Richtlinie zu sprechen.

They agreed that there is a gulf between the published positions of the European Parliament (EP), the Commission of the European Communities (CEC) and the EU Council of ministers (Consilium), and that this means that decisions must first be taken at the political level and only later at the ``patent expert'' level.  The political decisions are to be prepared by Coreper, the Committee of permanent representatives.

The UK Patent Office is pressing for a quick decision, whereas the German representatives stress that the situation is new and more time-consuming efforts are needed, if any workable co-decision is to be achieved between Council and Parliament.
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf FFII UK: Lobbying the Council of Europe\footnote{http://www.ffii.org.uk/council.html}}}

\begin{quote}
How decisionmaking in the Council works and what this means for the amended software patent directive
\end{quote}
\filbreak

\item
{\bf {\bf Rat der Europ\"{a}ischen Union und Logikpatente\footnote{http://swpat.ffii.org/akteure/consilium/index.en.html}}}

\begin{quote}
Together with the European Commission and the European Parliament, the Council is one of the three pillars of the European Union, which jointly legislate in a \emph{co-decision procedure}.  It is a forum where the national governments and their specialised ministries meet.  The question of how to limit patentability is handled in the ``Council Working Party on Intellectual Property and Patents''.  This council has been holding increasingly frequent meetings to discuss the European Commission's proposal for a software patentability directive and come up with a counter-proposal.  The national delegations are mostly composed of national patent office representatives or people whose career path is confined to the national patent establishment and who are factually dependent on this establishment in many ways.  Some delegations, such as the french and belgians, have comprised independent delegates and been fairly critical of the CEC proposal.  Others have been even more pro-patent than the CEC.  All have focussed on textual questions and caselaw rather than on what kind of output they want from the legislation in terms of patents granted/rejected and economic policy objectives.
\end{quote}
\filbreak

\item
{\bf {\bf CEC 2003-10: Inacceptable Amendments\footnote{http://register.consilium.eu.int/pdf/en/03/st11/st11503.en03.pdf}}}

\begin{quote}
In a first reaction to the European Parliament's amendments, the European Commission's patent experts (i.e. the authors of the amended draft) list the amendments which they say are ``inacceptable to the Commission''.  The list is long.  It comprises all amendments that can limit patentability or patent enforcability in any way.  The only ``acceptable'' amendments are the cosmetic ones from JURI.  CEC does not give any reasoning as to why it can't accept the others.  This CEC statement was published on the Coucil website two weeks after the EP vote, shortly before a first meeting of the Council's ``Patent Working Party''.

siehe auch Europarl 2003-08-24: Ge\"{a}nderte Softwarepatent-Richtlinie\footnote{http://swpat.ffii.org/papiere/europarl0309/index.de.html}
\end{quote}
\filbreak

\item
{\bf {\bf Heise 2003-10-23: EU-Parlament erhaelt Unterstuetzung bei Software-Patenten\footnote{http://www.heise.de/newsticker/data/anw-23.10.03-004/}}}

\begin{quote}
Zitiert eine Presseerkl\"{a}rung des DMMV.  Dessen Vorstandsmitglied Rudi Gallist sieht im Ansatz des Europ\"{a}ischen Parlamentes einen guten Kompromiss zwischen Erfordernis des Patentschutzes f\"{u}r den klassischen Bereich der Technik und dem Bed\"{u}rfnis der Softwarebranche, insbesonders der KMU, nach Freiheit von Patentverletzungsrisiken.  Ferner kommt Daniel Riek vom Linux-Verband zu Wort, der die Bitkom-Stellungnahme vom Vortag scharf kritisiert.

siehe auch Deutscher Multi-Media-Verband\footnote{http://swpat.ffii.org/akteure/dmmv/index.de.html}
\end{quote}
\filbreak

\item
{\bf {\bf Heise 03-10-22: ``IT-Verband ruft EU auf den rechten Weg zur\"{u}ck''\footnote{http://www.heise.de/newsticker/data/anw-22.10.03-001/}}}

\begin{quote}
Bericht \"{u}ber Bitkom-PE und Warnung von FFII UK

siehe auch Bitkom-PE 2003-03-22: \footnote{http://www.bitkom.org/index.cfm?gbAction=gbcontentfulldisplay\&ObjectID=D321C079-46F6-4D51-9B08AA448674390A\&MenuNodeID=4C872DB6-8470-4B01-A36FD8C1EBA2E22D}
\end{quote}
\filbreak

\item
{\bf {\bf BMJ-PE 2003-09-26: Nur ABS, nicht Software als solche\footnote{http://swpat.ffii.org/papiere/europarl0309/bmj030926/index.de.html}}}

\begin{quote}
Kurz nach der Entscheidung des Europ\"{a}ischen Parlamentes f\"{u}r eine \"{A}nderung der geplanten Softwarepatent-Richtlinie nimmt das Bundesministerium der Justiz in sibyllinischer Manier Stellung.  Die Presse-Erkl\"{a}rung wendet sich schulmeisterlich belehrend gegen den Begriff ``Softwarepatente'' und behauptet, diese seien nie bef\"{u}rwortet worden und mit ``computer-implementierte Erfindung'' seien etwas ganz anderes gemeint, n\"{a}mlich nur ``Anti-Blockier-Systeme''.  Was noch zu der Kategorie ``ABS'' geh\"{o}rt und wie das BMJ verhindern will, dass allgemeine Datenverarbeitung im Gewande von Universalrechnern patentiert wird, verr\"{a}t das BMJ nicht.  Im Oktober 2002 hatte sich das BMJ im Rat f\"{u}r Programm-Anspr\"{u}che stark gemacht.  Ob das BMJ hieran festhalten will und wie es sich sonst gegen\"{u}ber dem Parlamentsvotum verhalten wird, beantwortet die Erkl\"{a}rung mit keinem Wort.  Dieser ausweichende Formeltext wurde w\"{o}rtlich im Namen der Ministerin an zahlreiche Fragesteller \"{u}bersandt.  Forderungen Tausender von Unterzeichnern nach Pr\"{a}zisierung der anzustrebenden Zielvorgaben hat das BMJ unbeantwortet gelassen.

siehe auch Was ist eine Computerimplementierte Erfindung?\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/kinv/index.de.html}, Europarl 2003-08-24: Ge\"{a}nderte Softwarepatent-Richtlinie\footnote{http://swpat.ffii.org/papiere/europarl0309/index.de.html}, BMJ: \"{A}ngstlicher Vorreiter der Patentinflation in DE und EU\footnote{http://swpat.ffii.org/akteure/bmj/index.de.html} und Rat der Europ\"{a}ischen Union und Logikpatente\footnote{http://swpat.ffii.org/akteure/consilium/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003-08-24: Ge\"{a}nderte Softwarepatent-Richtlinie\footnote{http://swpat.ffii.org/papiere/europarl0309/index.de.html}}}

\begin{quote}
Konsolidierte Version der wesentlichen Bestimmungen (Art 1-6) der geänderten Richtlinie %(q:über die Patentierbarkeit computer-implementierter Erfindungen), für die das Europäische Parlament am 24. September 2003 gestimmt hat.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

