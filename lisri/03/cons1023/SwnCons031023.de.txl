<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#descr: Seitdem das Europäische Parlament im September für einen klaren Ausschuss von Software von der Patentierbarkeit abgestimmt hat, versucht die Patentbewegung, über den Rat der Europäischen Union, in dem die Fachminister gemeinsame Standpunkte erarbeiten, dagegen anzugehen.  Eine EU-Richtlinie kann nur Gesetz werden, wenn der Rat und das Parlament zustimmen.  Die Position des Rates wird von einer Arbeitsgruppe nationaler Patentverwaltungsbeamter am Donnerstag, den 23. Oktober in Brüssel vorbereitet.  Die Patentbeamten der Europäischen Kommission unter Kommissar Bolkestein drängen darauf, die Vorschläge des Parlaments beiseite zu schieben und und einen früheren Vorschlag des Rates wieder hervorzuholen, der grenzenlose Patentierbarkeit vorsieht.  Die britische Regierung, deren Position von ihrem Patentamt bestimmt wird, drängt aktiv in diese Richtung.

#title: Treffen der Patent-Arbeitsgruppe des EU-Ministerrates

cke: Die Arbeitsgruppe traf sich den ganzen Tag, um über das Gemeinschaftspatent und die Software-Richtlinie zu sprechen.

tas: Sie kamen zu der Auffassung, dass zwischen den veröffentlichten Positionen des Parlamentes und des Rates eine Lücke klafft, und dass dies zunächst nicht patentrechtliche Fachdiskussionen sondern politische Entscheidungen erfordere.  Diese Diskussionen sollen nun vom Kommittee der ständigen Vertreter (Coreper) vorbereitet werden.  Ein möglicher Termin für ein erstes Treffen der Minister ist der 10. Oktober.

srW: Während die britische Patentbeamten gemeinsam mit denen der Europäischen Kommission auf eine schnelle Ablehnung der Parlamentsvorschläge und Einreichung einer Gegenposition auf Grundlage der Praxis des Europäischen Patentamtes fordern (in dessen Verwaltungsrat die Ministerrats-Beamten in Personalunion sitzen),  wollen einige andere Delegationen, darunter auch die deutsche, keine schnelle Konfrontation mit dem Parlament suchen sondern Zeit bis nach dessen Neuwahlen lassen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwnCons031023 ;
# txtlang: de ;
# multlin: t ;
# End: ;

