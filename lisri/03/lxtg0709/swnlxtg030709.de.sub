\begin{subdocument}{swnlxtg030709}{Linuxtag 2003 im Zeichen der Softwarepatente}{http://swpat.ffii.org/chronik/03/lxtg0709/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Mit einem Informationsstand, Vortr\"{a}gen, Diskussionen sowie einer Pantomimen-Auff\"{u}hrung wird der FFII e.V. auf dem kommenden LinuxTag \"{u}ber Software-Patente informieren.  Diese Aktivit\"{a}ten erfahren besondere Unterst\"{u}tzung seitens der Organisatoren des Linuxtages.}
\begin{sect}{detal}{Einzelheiten}
Dem Europ\"{a}ische Parlament liegt ein Gesetzesentwurf vor, demzufolge Algorithmen und Gesch\"{a}ftsmethoden wie ``Amazon One Click Shopping'' k\"{u}nftig in ganz Europa als patentf\"{a}hige Erfindungen gelten m\"{u}ssen. Die Bef\"{u}rworter, allen voran die Britische Sozialdemokratin Arlene McCarthy und der Bayreuther CSU-Abgeordnete Dr. Joachim Wuermeling, haben es verstanden, diesen Vorschlag als ``gem\"{a}{\ss}igte Harmonisierung des Status Quo'' zu verpacken. Nur die wenigsten Parlamentskollegen lesen das Kleingedruckte.  Am 1. September 2003 wird das Parlament entscheiden.  Von der Entscheidung h\"{a}ngt die wirtschaftliche Existenz der kleinen und mittelst\"{a}ndischen Software-Unternehmen sowie der Freien Software (Opensource, GNU/Linux) ab.

Auf dem LinuxTag vom 10. bis 13. Juli 2003 in Karlsruhe wird der F\"{o}rderverein f\"{u}r eine Freie Informationelle Infrastruktur (FFII) e.V. in Zusammenarbeit mit der Essener Linux User Group (ELUG) mit einem Informationsstand (G13) pr\"{a}sent sein.  Am Sonntag, den 13. Juli um 13:00 Uhr h\"{a}lt der Software-Unternehmer Dr. Peter Gerwinski einen einf\"{u}hrenden Vortrag.  In der folgenden Diskussion werden einige Experten, darunter der Vorsitzende Richter am Patentsenat des Bundesgerichtshofes Dr. Klaus Melullis und der FFII-Vorsitzende Hartmut Pilch Fragen \"{u}ber die Bedeutung der anstehenden Gesetzesvorschl\"{a}ge zu beantworten versuchen.

Auf sehr anschauliche Weise wird am Sonntag ferner die Essener Mimengruppe ``Commedia Triennale'' mit ihrer Performance ``Fair Play'' das Thema aufgreifen.  Eine \"{a}hnliche Auff\"{u}hrung fand bereits am am 8. Mai vor dem Europ\"{a}ischen Parlament in Br\"{u}ssel statt.

\begin{center}
\mbox{\includegraphics{auffuehrung}}
\end{center}
\end{sect}

\begin{sect}{media}{Medienkontakte}
\begin{description}
\item[E-Post:]\ pr at ffii org
\item[Telefon:]\ FFII-B\"{u}ro +49-89-18979927
Peter GERWINSKI 01776751176
Weitere Kontakte auf Anfrage
\end{description}
\end{sect}

\begin{sect}{elug}{\"{U}ber die Essener Linux User Group -- www.elug.de}
Seit Juli 1999 gibt es die Essener Linux User Group (ELUG). Eine Linux User Group ist ein Forum, in dem sich Linux-Interessierte treffen, gegenseitig Tips geben, diskutieren.  Die ELUG sieht die Legalisierung von Software-Patenten in Europa als eine massive Bedrohung und unterst\"{u}tzt aus diesem Grunde seit 2002 mit verschiedenen Aktionen die Arbeit des FFII.  Hierzu geh\"{o}rt ein bereits zwei mal durchgef\"{u}hrter Patentverletzungsprogrammierwettbewerb.
\end{sect}

\begin{sect}{url}{Permanente Netzadresse dieser Mitteilung}
http://swpat.ffii.org/chronik/03/lxtg0709/index.de.html
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf FFII @ Linuxtag 2003\footnote{http://swpat.ffii.org/termine/2003/linuxtag/index.de.html}}}

\begin{quote}
Aktivit\"{a}ten des FFII auf dem Linuxtag 2003
\end{quote}
\filbreak

\item
{\bf {\bf FFII informiert auf dem LinuxTag \"{u}ber Software-Patente\footnote{http://www.pro-linux.de/news/2003/5706.html}}}

\begin{quote}
Bericht und Diskussion auf pro-linux.de
\end{quote}
\filbreak

\item
{\bf {\bf Die Gedanken sind ...\footnote{http://patinfo.ffii.org/}}}

\begin{quote}
Leicht verst\"{a}ndliche und unterhaltsame Einf\"{u}hrung in die Problematik der Softwarepatente von Peter Gerwinski
\end{quote}
\filbreak

\item
{\bf {\bf 2003/05/08 13:00 BXL Luxemburger Platz: Freie Ideen f\"{u}r eine Freie Welt\footnote{http://swpat.ffii.org/termine/2003/europarl/05/08/13/index.de.html}}}

\begin{quote}
Hierbei wurde die Pantomime-Darbietung ``Fair Play'' erstmals aufgef\"{u}hrt.
\end{quote}
\filbreak

\item
{\bf {\bf JURI 2003/04/08 \"{A}nderungsantr\"{a}ge: Echte  und Falsche Grenzen der Patentierbarkeit\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/juri0304/index.en.html}}}

\begin{quote}
Mitglieder des Rechtsausschusses des Europaparlaments (JURI) haben \"{A}nderungsvorschl\"{a}ge zum Softwarepatent-Richtlinienvorschlag der Europ\"{a}ischen Kommission eingereicht.  W\"{a}hrend einige Abgeordnete der Gr\"{u}nen und Soziademokratischen Fraktionen (Berenguer, Echerer, Gebhardt, MacCormick, Schr\"{o}der) fordern, die Richtlinie in Einklang mit Art 52 EP\"{U} zu bringen und klar auszudr\"{u}cken, dass Computerprogramme keine patentierbaren Erfindungen sind, schl\"{a}gt eine andere Gruppe von Abgeordneten, haupts\"{a}chlich aus den konservativen und liberalen Fraktionen, vor, Programme und Datenstrukturen direkt beanspruchbar zu machen, um sicher zu stellen dass Softwarepatente nicht nur erteilt werden sondern auch maximale Blockierwirkung entfalten k\"{o}nnen.
\end{quote}
\filbreak

\item
{\bf {\bf Testsuite f\"{u}r die Gesetzgebung \"{u}ber die Grenzen der Patentierbarkeit\footnote{http://swpat.ffii.org/analyse/testsuite/index.de.html}}}

\begin{quote}
Um eine Patentierbarkeitsrichtlinie auf Tauglichkeit zu pr\"{u}fen, sollten wir sie an Beispiel-Innovationen ausprobieren.  F\"{u}r jedes Beispiel gibt es einen Stand der Technik, eine technische Lehre und eine Reihe von Anspr\"{u}chen.  In der Annahme, dass die Beispiele zutreffend beschrieben wurden, probieren wir dann unsere neue Gesetzesregel daran aus.  Unser Augenmerkt liegt auf (1) Klarheit (2) Angemessenheit:  f\"{u}hrt die vorgeschlagene Regelung zu einem vorhersagbaren Urteil?  Welche der Anspr\"{u}che w\"{u}rden erteilt?  Entspricht dieses Ergebnis unseren W\"{u}nschen?  Wir probieren verschiedene Gesetzesvorschl\"{a}ge an der gleichen Beispielserie (Testsuite) aus und vergleichen, welches am besten abschneidet.  F\"{u}r Programmierer ist es Ehrensache, dass man ``die Fehler beseitigt, bevor man das Programm freigibt'' (first fix the bugs, then release the code).  Testsuiten sind ein bekanntes Mittel zur Erreichung dieses Ziels.  Gem\"{a}{\ss} Art. 27 TRIPS geh\"{o}rt die Gesetzgebung zu einem ``Gebiet der Technik'' namens ``Sozialtechnik'' (social engineering), nicht wahr?  Technizit\"{a}t hin oder her, es ist Zeit an die Gesetzgebung mit derjenigen methodischen Strenge heran zu gehen, die \"{u}berall dort angesagt ist, wo schlechte Konstruktionsentscheidungen das Leben der Menschen stark beeintr\"{a}chtigen k\"{o}nnen.
\end{quote}
\filbreak

\item
{\bf {\bf FFII: Logikpatente in Europa\footnote{http://swpat.ffii.org/index.de.html}}}

\begin{quote}
In den letzten Jahren hat das Europ\"{a}ische Patentamt (EPA) gegen den Buchstaben und Geist der geltenden Gesetze \"{u}ber 30.000 Patente auf computer-eingekleidete Organisations- und Rechenregeln (laut Gesetz \emph{Programme f\"{u}r Datenverarbeitungsanlagen}, laut EPA-Neusprech von 2000 ``computer-implementierte Erfindungen'') erteilt.  Nun m\"{o}chte Europas Patentbewegung diese Praxis durch ein neues Gesetz festschreiben.  Europas Programmierer und B\"{u}rger sehen sich betr\"{a}chtlichen Risiken ausgesetzt.  Hier finden Sie die grundlegende Dokumentation zur aktuellen Debatte, ausgehend von einer Kurzeinf\"{u}hrung und \"{U}bersicht \"{u}ber die neuesten Entwicklungen.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

