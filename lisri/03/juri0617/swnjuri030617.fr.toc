\select@language {french}
\select@language {french}
\contentsline {section}{\numberline {1}introduction}{2}{section.0.1}
\contentsline {section}{\numberline {2}Que s'est-il pass\'{e} \`{a} la Commission juridique (JURI) \@PI }{3}{section.0.2}
\contentsline {section}{\numberline {3}Int\'{e}grale des d\'{e}clarations \'{e}manant de l'industrie et de l'universit\'{e}}{9}{section.0.3}
\contentsline {subsection}{\numberline {3.1}Dr. Bernhard Runge, d\'{e}veloppeur chez SAP AG, ancien professeur de math\'{e}matiques.}{9}{subsection.0.3.1}
\contentsline {subsection}{\numberline {3.2}Stefan Pollmeier, dir. de ESR Pollmeier GmbH, Allemagne}{10}{subsection.0.3.2}
\contentsline {subsection}{\numberline {3.3}Bernhard Kaindl, d\'{e}veloppeur chez SuSE Linux AG}{10}{subsection.0.3.3}
\contentsline {subsection}{\numberline {3.4}H\r a{}kon Wium Lie, dir. tech. de Opera Inc}{11}{subsection.0.3.4}
\contentsline {subsection}{\numberline {3.5}Richard Clark, PDG de Elysium Co Ltd et r\'{e}dacteur en chef pour le comit\'{e} de normalisation JPEG}{12}{subsection.0.3.5}
\contentsline {subsection}{\numberline {3.6}Peter Holmes, \'{e}conomiste, ayant particip\'{e} \`{a} une \'{e}tude command\'{e}e par l'UE, University of Sussex}{12}{subsection.0.3.6}
\contentsline {subsection}{\numberline {3.7}Reinier Bakels, auteur d'une \'{e}tude command\'{e}e par le parlement sur la brevetabilit\'{e} des programmes d'ordinateurs}{13}{subsection.0.3.7}
\contentsline {subsection}{\numberline {3.8}Jozef Halbersztadt, examinateur de brevets \`{a} l'Office polonais des brevets}{13}{subsection.0.3.8}
\contentsline {subsection}{\numberline {3.9}Karl-Friedrich Lenz, professeur de Droit allemand et europ\'{e}en, Universit\'{e} Aoyama Gakuin, Tokyo}{14}{subsection.0.3.9}
\contentsline {subsection}{\numberline {3.10}Hans Appel, dir. tech. de Sun Microsystems pour l'Europe du nord}{14}{subsection.0.3.10}
\contentsline {subsection}{\numberline {3.11}Arlene McCarthy, d\'{e}put\'{e}e europ\'{e}enne \@DP `` Nous ne sommes pas des experts ''}{14}{subsection.0.3.11}
\contentsline {subsection}{\numberline {3.12}Jean-Paul Smets-Solanes, PDG de Nexedi, Lille, France}{15}{subsection.0.3.12}
\contentsline {subsection}{\numberline {3.13}Brian Kahin, University of Michigan, Ann Arbor, \'{E}tats-Unis}{16}{subsection.0.3.13}
\contentsline {section}{\numberline {4}Contact m\'{e}dias}{17}{section.0.4}
\contentsline {section}{\numberline {5}\`{A} propos de l'Alliance Eurolinux -- www.eurolinux.org}{17}{section.0.5}
\contentsline {section}{\numberline {6}\`{A} propos de la FFII -- www.ffii.org}{17}{section.0.6}
\contentsline {section}{\numberline {7}URL permanente de cette publication}{17}{section.0.7}
\contentsline {section}{\numberline {8}Liens Annot\'{e}s}{18}{section.0.8}
