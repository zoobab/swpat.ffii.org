<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#oag: Many readers fooled by the Newman/Yahoo propaganda article, but also many enlightening statements.

#tWi: A public letter to Matthew Newman, author of a Yahoo article which collects deceptive patent lobby statements into a misleading message.

#0ix: phm 2003/06/17: Abstimmung in JURI: Ergebnis

#0sU: phm 2003/06/17: Abstimmung in JURI jetzt

#Wfp: Dr. Lenz, professor of European law in Tokyo, comments on the Council's professed commitment to %(q:protection of patents on computer-implmenented inventions.)

#lEr: The European Council apparently no longer wants to %(q:protect innovation), let alone stimulate innovation, but only to %(q:protect patents).  This is at least what a few bureaucrats at the March 20/21 Summit inserted into the final document.  The ministerial officials of the European Council have in recent years been particularly responsive to interests of large corporate patent departments.  At the request of the patent lobby, they proposed go a step further than the European Commission and make publication of program code on the web a direct patent infringement.

#Wne: quotes Daniel Cohn-Bendit and Mercedes Echerer

#Eet: Greens/EFA press release: disastrous vote in JURI

#mht: Miquel Mayol MEP (Greens/EFA, Catalonia) issues a press release shortly before the vote in which he warns of disastrous consequences of the imminent JURI decision on SMEs, which form the bulk of the catalonian software economy.

#RWe: Carles Gasoliba i Bohn (CdC, CiU, ELDR), liberal MEP from Catalonia, warns that the JURI decision leads to broad and trivial patents on non-inventions and that controllable forces of nature must be an essential element of any invention in the sense of patent law.

#Ape: Similar to %(q:European Voice), EurActiv seems to be a platform for journalism and lobbying around EU institutions, sponsored by large corporations.  Yet this short report is more informative than the reports from IDG, Yahoo or EU-owned newsmedia.

#Hen: In a public letter to Dow Jones journalist Matthew Newman, Hartmut Pilch points out that JURI, contrary to what Newman wrote in Yahoo, did not impose any limits on patentability but in fact did everything to assure that ideas of the Amazon One-Click type are indisputably considered patentable inventions in Europe and that such patents can be enforced in ways which even the European Commission had not proposed.

#WyW: Journalist Matthew Newman copy&pastes patent lobby propaganda.  The truth is: JURI is replacing an already unified patent standard by a set of intransparent rules which turn Amazon's One-Click idea into a patentable invention and create no unity but at best put the EPO more firmly in charge.

#tIM: German IT news magazine with as lively forum discussion as Slashdot.  Article quotes FFII and Cohn-Bendit.  Forum is full of furious condemnations of McCarthy and the JURI politicians in general.

#eaU: A EU organ transmits Arlene McCarthy's view as official truth.

#bWI: A fairly objective account of what JURI chose and what JURI voted against.  Also cites some elements from our press release.

#eta: Computerwoche, german branch of IDG, parrotts Paul Meller's = Arlene McCarthy's misinformation

#hrr: Computerwoche: Kommission des Europaparlaments macht Vorschlag für EU-Patentgesetz

#sWo: Paul Meller, IDG correspondent in Brussels, presents the JURI proposal as an effort to limit patentability, a middle way between two extremes, that of following the US and that of the opensource business model.

#bee: Report submitted to the Parliament after the JURI vote of 2003/06/17

#neW: Explanatory Statement by Arlene McCarthy.  Slightly revised version of her draft report of February, without corrections but with additions, e.g. mention of a small company in an unemployment blackspot in Southwest England which benefitted from voice recognition related software patents.

#oWW: More Contacts to be supplied upon request

#ncW: The reality is that small companies are threatened by patent speculators and massive unreadable portfolios of large companies, who want recognition of their R&D investments through rapidly expanding licensing programs.  Unfortunately the rapporteur's analysis of the U.S. experience is limited to one unsubstantiated, undocumented sentence written by two British patent lawyers and lifted out of context.  She has avoided meeting with informed disinterested observers and completely ignores the Parliament's own commissioned study, authored by the prestigious Institute for Information Law.  Instead of investigating the complex business and economic issues behind software patents, she treats the issue as a political compromise.  If Europe is serious about sound innovation policy, it should await the forthcoming reports from the National Academic of Sciences and the Federal Trade Commission and, with help from its own economists, think much harder about how software should be regulated.

#ohe: Brian Kahin, professor for information infrastructure policy studies and former Whitehouse advisor on this subject matter, finds the European decision process bewildering:

#Wcf: Dogmas make life easy for legislators.  But for the rest of us, the price to pay can be very high.  Software patents are cheap and easy to obtain, because you need to do no laboratory work, you just patent some new abstract idea.  Currently in Japan concerted efforts are under way, sponsored by the government, to preemptively secure patents on all kinds of software ideas.  Housewives are taking courses in writing patent applications.  JURI is now making sure that these patents will come to Europe.  Thus, even when we catch up and achieve the same level of communication infastructure as East Asia, we may not even be free to use it efficiently, because the road is cluttered with the patents of the first movers.  Already now 75% of the software patents which the European Patent Office (EPO) is illegally granting are owned by companies from the US and Japan, and, as %(ee:empirical evidence) shows, patent-owner-driven legislation has in the past tended to stimulate an influx of foreign patents and throw the domestic economy further behind.

#nrn: Now Europe seems to be following the same type of dogma again: privatisation of abstract ideas, erection of enclosures in a way that simply can't work, as common sense and economic studies tell us.

#sia: Europe is today 7 years behind Korea and Japan, where citizens pay 12 EUR per month for 24 Mbps ADSL access to the Internet.  This is because EU politicians chose to believe in the dogma that infrastructure buildup must be left to private initiative.

#nsp: Today the JURI voted for %(e:not) stopping the monopolisation of public informational infrastructure. If the Plenary does not open their eyes for what is happening, soon every citizen will pay a new tax, not to the state, but to american and japanese corporations, because they own the patent rights on digital communications in the information society.

#nsn: Jean-Paul Smets, CEO of %(NE), a successful Enterprise Ressource Planning software startup, and owner of serveral European software and business method patents, was trained as an informatician and economist and and served as a french industrial planning administrator before he founded Nexedi.  Smets wrote an important %(ss:study on software patents) for a french governmental institution in 2001.  Upon hearing the news of the JURI vote, he restates a concern which he already expressed at the %(dk:software patent symposium in Brussels in May 2003):

#aei: After the JURI session, Arlene McCarthy was involved in a discussion with a colleague who confronted her with the studies written by the experts whom the European Parliament had asked for an opinion.  She then said %(q:Anybody can write anything) and suddenly had to go to a next meeting.

#ttr: Another witness writes:

#nbe: The president then solved the issue, it was not 4 Green, but 2 Green + 2 GUE (the other green-like group in parliament).

#sWs: During the vote she also asked (turning to see behind her) whether it was normal the green had 4 votes (maybe she was surprised that 1/3 was against her).

#rml: I was a bit surprised to hear Arlene McCarthy say this: %(q:Let the expert do their work could mean let the EPO (or patent expert) do their work.) Where they should have asked the %(q:Software experts).

#iWW: One witness to the JURI session confirms:

#nvm: Arlene McCarthy and her colleagues Wuermeling and Harbour from PPE refused to consult any expertise at the legislative level.  At the judicative level, matters will be left to patent experts, if any, to straighten things out by interpreting magical forumlas which not even the legislator understood.

#rmw: This may be seen as Arlene McCarthy's response to the question which has been repeatedly put forward for her from many sides, namely whether certain %(ts:sample patent claims) are intended to be patentable or not, and what, if anything, in her directive proposal says so.

#jet: So I hope members, and I find those members that support my approach, and I hope that members, well, at the end of the day, feel that we have actually made progress in this area, and that we have attemted to set some limits in perhaps a moderately restrictive way, without entirely reinventing patent law, which I would hasten to add, we are not in ability to do that, we are legislators to create framework and laws for interpretation by experts, but we are not experts ourselves.

#ctW: In her last speech to JURI before the vote, according to the %(am:sound recording), McCarthy concluded as follows:

#kse: If you already got money for something, why should you keep it closed? I think patents on software are not a good thing. It is better to compete on other areas, like training, advice, special libraries for software and the development of extra tools. In some cases however you are forced to request a patent, for example in the form of combined licenses by cooperation with third parties.

#foe: Hans Appel, Chief Technology Officer Northern Europe from Sun, states in a dutch press interview after the JURI vote:

#esW: The European Parliament seems to be set to ignore the more than one hundred thousand signatures of European citizens opposing software patents and go ahead with legalizing them anyway. If that happens, we will see large license payments from the European to the American software industry, lots of litigation based on software patents, Internet patents and business method patents, and some very unfavorable effects for open source software. And introducing a large number of new monopoly rights in the information society sector certainly won't help with the EU strategic goal %(q:to become the most competitive and knowledge-based economy in the world.)

#tmp: Polish companies own practically no software patents and Poland has a tradition of refusing to grant such patents.  The McCarthy directive would bring a radical change to Poland.  In Recital 16 it is said clearly, that this proposal is to be considered as a measure for protecting developped economies against competition from %(q:low-cost economies).  While I do not believe that protection against legitimate competition in the sofware industry will work to any country's advantage, it will surely hurt Eastern Europe even more severely than Western Europe, simply because it imposes a high level of legal costs which we are less able to afford.

#lWW: Now it is up to the European Parliament to decide on a highly controversial proposal for a directive for software patents. The JURI proposal aims at improving clarity. It should have at last defined clearly what is patentable and what not. But in reality it is a bunch of magic formulas that even legal experts do not understand. In particular for small and medium sized software developers it is a disaster. A patent infringement claim can ruin such a company.  It is to be hoped that the European Parliament understands this if they vote about the proposed directive in a few days.

#rsl: Dr. Reinier Bakels, %(ss:researcher at Institute for Informatic Law) of Amsterdam University and co-author of a %(ss:study) about the proposed directive that was commissioned by the European Parliamentin 2002, comments on the JURI decision:

#tgr: The study which we conducted for the European Commission said that there is no evidence to support an extension of software patentability and that evidence on the economic effects of software patents in general was inconclusive, and if anything I felt the balance of research was on the negative side. But we had to compromise on this formula, because this was a committee decision, and my co-authors were patent law experts with firm initial convictions about the usefulness of patents for firms wishing to raise capital. My co-authors felt that it would be difficult to prevent software with a technical effect from being patentable but agreed that nothing should be done to permit the spread of bad softweare patents as in the US. I feel that even the compromise conclusion of the study was in some ways  already  progress: at the time the study was commissioned there appeared to be an almost unquestioning assumption in the Commission that we should copy the US model.  If we had to take the write the study today, we might be able to advance further. The recent Bessen & Hunt study seems to offer much stronger evidence that that software patents in the US have stifled software innovation. Today, the burden of proof should be on those who propose any legislation to extend patents in the software field.

#ecf: The members of the JPEG committee have spent years fighting off frivolous patent claims that have partially succeded in destroying our standardisation efforts.  With the new regulation proposed by JURI, we are in for even more trouble.  The European Patent Office (EPO) has shown no more mercy to the software industry than the US Patent Office (USPTO), but their patents have so far been considered invalid in much of Europe.  Now, according to Arlene McCarthy's voting list, we will not only be forced to regularly face litigation on broad and trivial software patents according to EPO/USPTO standards in Europe.  We will also know for sure that there is no exemption for the purposes of interoperability.  While in the USA there is hope that courts will give in to mounting pressure of public opinion and reform to some extent, the JURI Commission is preparing to codify the state of the art in current patent office malpractise.  This would allow US corporate landgrabbers to go on desertifying the European IT landscape while the US is already moving to recover from the catastrophe.

#oem: Arlene McCarthy and her followers seem determined to kill interoperability on the World Wide Web and to strengthen large US-based monopolies.  At the World Wide Web Consortium (W3C), we have been struggling with patent policy questions for years.  The Industry Commission proposed to exempt the use of software for interoperability purposes from the scope of patent enforcability. This could have been extremely helpful for standardisation.  It would also have helped to avoid some of the worst anti-competitive practises.  And it wouldn't have cost the patent owner lobby very much.  Yet they discarded even this very moderate amendment without even giving a reason.  Our representations to the parliament have been completely ignored.  The JURI rapporteur's lawmaking clearly has nothing whatsoever to do with the interests of the European software industry.

#imy: Håkon Wium Lie, CTO of %(op:Opera Inc) comments on %(vl:Arlene McCarthy's voting list):

#kGA: Leading JURI committee members are not satisfied with putting the software industry at risk.  They also want to make sure that every programmer will run afoul of patents as soon as he publishes a program on the web.  While CULT and ITRE introduced safeguards for the freedom of publication, Arlene McCarthy is flatly ignoring these safeguards and instead recommending %(q:compromise amendment 1), which makes publication a direct infringment.  It seems almost entrepreneurially irresponsible to go on selling Linux distributions under these conditions.  If one of thousands of programs in our distribution infringes on one of tens of thousands of broad and trivial software patents granted by the EPO, we can be sued and forced to take our CDs from the market.  Similar cases have already arisen due to trademarks, but legal insecurity around patents is far greater. Cases such as SCO and EBay show that the danger is not merely theoretical.  At a moment where public administrations are introducing free/opensource operating systems or using them to pressure Microsoft into price reductions, Arlene McCarthy and her allies seem to be joining Microsoft in its crusade to suppress free software.  Bavaria's Christian Social Union appears to be instrumental in this and has at the same time been %(cs:pushing the Microsoft cause) in Bavaria.  Arlene McCarthy herself has openly %(gm:attacked the GNU General Public License) as %(q:just another form of monopolism).  McCarthy and her ally Dr. Joachim Wuermeling have declined all invitations for dialog from our side and refused to reply to our questions.  They are of course not obliged to talk to us.  However, strengthening monopolies by handcuffing competitors is about the worst thing a government can do against itself.

#mhW: Many people had %(sm:asked) EU commission and the supporters of the amended directive proposal to make clear the outcome using example patents (Amazon One Click could be one). As far as I know they got no answers to their questions.

#lue: From this it should be clear that a directive harmonizing current EPO practice would allow %(e:most) new (computer implemented) business methods, including Amazon One Click.

#Wit: This statement %(or:originates from) the acclaimed German IP law firm %(q:Wuesthoff & Wuesthoff).

#tcs: Let us take for granted that the amended directive proposal wants to harmonize the current practice of the European patent office EPO. Please consider the following statements from IP specialists in 2001:

#una: During the last year I %(sp:learned much) about the directive proposal and I know the amendments that passed JURI today. I don't think that business method patents such as Amazon One-Click could be avoided in Europe if the amended directive proposal would pass in parliament.

#mto: Stefan Pollmeier is managing director of an electronics company with 45 employees near Frankfurt, Germany.  80% of their R&D is in software.

#isf: Contrary to what some of the directive's proponents say in public, this directive make algorithms and business methods patentable.  How could it be otherwise?  What else can there be to patent in our software, if not algorithms and business methods?

#ead: Our patent lawyers have meanwhile persuaded some people in the managment that the EU directive is a good idea, because it would allow us to play the software patent game in Europe as well.  But I think they are wrong and their thinking will not prevail in the company.

#omi: SAP has suffered a lot from US patents on what under this directive would probably be called %(q:computer-implemented inventions with a technical contribution in their inventive step).  SAP has grown big by copyright, and being imitated was never seen as a major problem.  We do not need protection by patents but rather protection from patents.  SAP had to pay exorbitant extortion sums to some individual patent owners (among them professors from well known US-universities) with high criminal energy.  Software patents legalize and encourage criminal or immoral behaviour in the software industry.  There are probably nearly no non-trivial software patents. Therefore the only reasonable use of software patents is to have weapons for conflicts between companies (%(q:you have infringed my 100 patents [admittedly trivial and meaningless - which we will never say in public], we have infringed your 100 patents [also admittedly trivial and meaningless - which you will never say in public], therefore let's forget this conflict and make sure that nobody else will disturb both of us)). The armaments race imposes high costs for all software companies.  SAP had to install a patent department for defensive purposes in 1998.  We are now regularly diverting ressources to this department which could otherwise have been spent on R&D.  However, even a high number of patents does not help us against individuals with high criminal energy.

#rna: Prof. Brian Kahin, University of Michigan, Ann Arbor, USA

#lEg: Dr. Jean-Paul Smets-Solanes, CEO of Nexedi, Lille, France

#ePW: Arlene McCarthy MEP: We are not experts

#pct: Hans Appel, CTO of Sun Microsystems for Northern Europe

#ran: Dr. Karl-Friedrich Lenz, Professor for German Law and European Law, Aoyama Gakuin University, Tokyo:

#bma: Jozef Halbersztadt, patent examiner at the Polish Patent Office

#eop: Reinier Bakels, author of parliament-ordered study on software patent directive

#jai: Peter Holmes, economist, participant of an EU-commissioned study, University of Sussex

#jir: Richard Clark, CEO of Elysium Co Ltd and chief editor for the JPEG standardisation committee

#njp: Håkon Wium Lie, CTO of Opera Inc

#hdE: Bernhard Kaindl, developer at SuSE Linux AG

#Wee: Stefan Pollmeier, MD of ESR Pollmeier GmbH, Germany

#WSf: Dr. Bernhard Runge, developer at SAP AG and former professor of mathematics

#vav: If the server is down, please try the %(xv:version on Xavi's home page).

#teM: JURI report about result of the vote, converted to PDF from MSWord document.

#Wtt: This page contains links to (mis)information about the proposed directive, from the perspective of the proponents, including %(q:forecasts) and the text adopted by JURI.

#WoW: Title page leading to the text as adopted by JURI

#kni: JURI-amended and voted directive

#cam: Explains among others: that %(q:susceptible of industrial application) is not a restriction at all, because it only means %(q:commercially valuable); that program claims, as proposed by JURI, are welcome from the European Commission's point of view.

#WWq: Angelika Niebler, DE conservative MEP (christian social union from Bavaria, close associate of EPP rapporteur Wuermeling who was absent), praises CEC proposal, McCarthy report and JURI %(q:compromise amendments) as %(q:great work).  Contradicts Rothley's negative comments.

#eWW: Malcolm Harbour, UK conservative MEP, celebrates CEC Software Patent Directive Proposal and McCarthy report as a great achievement in limiting patentability, explains that program claims are needed, because mobile phones are patentable and the functioning principles of a mobile phone might well be embodied in a program that is stored on a medium and sold in that form.  Answers Berenguer on some points.

#rwt: Luis Berenguer MEP (PSE, Spain, law professor), warns of dangers of software patents, says that directive does not achieve what its proponents claim to be aiming for, recommends amendment 56.  This speech was the only one that criticised the McCarthy proposal at all, albeit not very strongly.

#Wai: Overview of transcripts of speeches given in JURI one day before their vote on the software patentability directive.  The only critical speech was delivered by Luis Berenguer (PSE, Spain)

#bWW: In her speech to JURI one day before the vote, Arlene McCarthy MEP explains why in her view there has been sufficient discussion and it is time to vote.  Thanks to Erik for the transscription from sound recording.

#ltW: explains in detail how all viewpoints except for those of the patent lobby were systematically disregarded in JURI ever since Arlene McCarthy became the rapporteur in May 2002.

#ilI: Explains what JURI wanted and what they decided, what they rejected and what they approved, and what consequences this would have if approved by the Parliament.

#pls2: please help working out more detailed notes, based on the sound recording!

#pls: please help working out more detailed notes, based on the sound recording!

#Wyj: German social democrat MEP Willi Rothley expresses frustration with the European Commission's proposal, saying that it is messy and unnecessary.  The patent courts find the right balance by themselves and the legislator is hardly able to set any better rules than what the courts work out.  However the legislative apparatus is like a sausage machine that has to be kept working.  From time to time it just must spit out new laws, no matter whether needed or not, and we, the MEPs are caught in this machine and forced to somehow make the best of it.  Rothley's speech struck a chord of resonance in JURI, as the laughter shows.

#sns: english translation of Rothley's speech

#eWe: Willi Rothley MEP (DE, PSE) says that the directive as proposed by the European Commission is poorly crafted and unnecessary and that it should really be sent back to the European Commission.  But JURI keeps rotatint like a sausage machine, turning out one law after another, and we have to make the best out of it.

#tmW: please listen to second Rothley comment on mon 16: JURI is a sausage machine

#cok: Bericht und Forums-Diskussion

#xrW: Pro-Linux 2003/06/17: Softwarepatente rücken ein Stück näher

#ses: The Liberals took an ambiguous position, supporting most of the proposals of the Industry Commission (against software patents) but also supporting McCarty's opposite proposals, with the result that the ITRE proposals were considered defeated and not even put to vote.  Thus Arlene McCarthy's proposal to make publication of software a direct patent infringement was adopted with support of Socialists, Conservatives and Liberals, and the ITRE proposal to guarantee the freedom of publication in the software area was removed from the agenda.

#nWW2: In her voting list, Arlene McCarthy recommended to vote against all amendment that proposed to limit patentability or patent enforcability in any way, and for most of those that proposed the opposite or provided a rationale for software patenting.  McCarthy's voting list was approximately followed by 2/3 of the JURI members.  They were opposed by the Green MEPs Mercedes Echerer (AT) and Professor Neil MacCormick (UK), the Left MEPs Ilka Schröder (DE) and Pernille Frahm (DK), the Socialist Evelyn Gebhardt (DE), the Conservative Piia-Noora Kauppi (FI), the Independent Marco Cappato (IT) and one more MEP whom we were so far not able to identify.  Moreover on a few points the majority did not follow McCarthy's recommendation.  The most important of these is ITRE-15, which says that the use of a patented process for the sole purpose of interoperability is always allowed.

#gio: Legislative proposal

#are: Final vote projet directive

#bei: abstention

#cnr: contra

#pro: pro

#dokurl: Permanent URL of this Press Release

#ffii: About the FFII

#eurolinux: About the Eurolinux Alliance

#media: Media Contacts

#ten: Full Statements from Industry and Academia

#tnJ: What happened in JURI?

#title: JURI votes for Fake Limits on Patentability

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnjuri030617 ;
# txtlang: en ;
# multlin: t ;
# End: ;

