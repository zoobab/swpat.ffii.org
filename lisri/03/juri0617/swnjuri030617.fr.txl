<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#oag: De nombreux lecteurs se sont laissés abusés par la propagande répandue par Mr Newman sur Yahoo!, mais on croise aussi quelques brillantes réparties.

#tWi: Une lettre ouverte à Matthew Newman, auteur d'un l'article fallacieux sur Yahoo! qui n'est qu'un florilège des affirmations mensongères du lobby pro-brevet.

#0ix: phm 2003/06/17: Abstimmung in JURI: Ergebnis

#0sU: phm 2003/06/17: Abstimmung in JURI jetzt

#Wfp: Dr. Lenz, professor of European law in Tokyo, comments on the Council's professed commitment to %(q:protection of patents on computer-implmenented inventions.)

#lEr: The European Council apparently no longer wants to %(q:protect innovation), let alone stimulate innovation, but only to %(q:protect patents).  This is at least what a few bureaucrats at the March 20/21 Summit inserted into the final document.  The ministerial officials of the European Council have in recent years been particularly responsive to interests of large corporate patent departments.  At the request of the patent lobby, they proposed go a step further than the European Commission and make publication of program code on the web a direct patent infringement.

#Wne: quotes Daniel Cohn-Bendit and Mercedes Echerer

#Eet: Communiqué de presse des Verts/ALE : « Vote désastreux en Commission juridique du PE ».

#mht: Miquel Mayol, député vert européen de Catalogne, a publié un communiqué de presse peu de temps avant le vote. Il estime que  la décision imminente de la Commission juridique aura des conséquences désastreuses pour les PME, qui constituent le gros de l'industrie logicielle en Catalogne.

#RWe: Carles-Alfred GASÒLIBA i BÖHM, député libéral européen de Catalogne, estime que la décision de la Commission juridique conduit malheureusement à des brevets vagues et triviaux pour de non-inventions alors que l'utilisation de forces contrôlables de la nature devrait être la caractéristique essentielle d'une invention au sens de la loi sur les brevets.

#Ape: Un peu comme %(q:European Voice), EurActiv semble être un portail consacré au journalisme et au lobbysme dans les parages des institutions de l'UE. Il est commandité par de grands groupes. Ce court compte-rendu est cependant plus informatif que ceux de IDG, Yahoo ou des canaux officiels de l'UE.

#Hen: In a public letter to Dow Jones journalist Matthew Newman, Hartmut Pilch points out that JURI, contrary to what Newman wrote in Yahoo, did not impose any limits on patentability but in fact did everything to assure that ideas of the Amazon One-Click type are indisputably considered patentable inventions in Europe and that such patents can be enforced in ways which even the European Commission had not proposed.

#WyW: Le journaliste Matthew Newman se contente de copier/coller la propagande du lobby pro-brevets. En réalité, la Commission juridique remplace un système de brevets qui est déjà bien unifié, par un ensemble de règles opaques qui feront de l'idée « One-Click » de Amazon une invention brevetable, sans pour autant ajouter d'uniformité si ce n'est que tout contrôle sera abandonné à l'OEB.

#tIM: Ce magazine informatique allemand dispose d'un forum de discussion aussi animé que celui de Slashdot. Cet article cite la FFII et M. Cohn-Bendit. Le forum regorge de réprobations véhémentes à l'encontre de  Mrs McCarthy et des politiciens membres de la Commission juridique en général.

#eaU: Un organe de l'UE relaie le point de vue de Mrs Arlene McCarthy comme s'il s'agissait de la vérité officielle.

#bWI: Un compte-rendu assez objectif de ce qui a été  approuvé ou rejeté par la Commission juridique; contient quelques citations de notre communiqué de presse.

#eta: Computerwoche, la filiale allemande de IDG, reproduit la désinformation répandue par Mr Paul Meller et Mrs Arlene McCarthy

#hrr: Computerwoche: Kommission des Europaparlaments macht Vorschlag für EU-Patentgesetz

#sWo: Paul Meller, correspondant de IDG à Bruxelles, présente la proposition de la Commission juridique comme un effort pour limiter la brevetabilité, comme une voie médiane entre deux extrêmes : la brevetabilité sans limites a l'américaine et l'adoption exclusive des modèles d'affaire open source.

#bee: Report submitted to the Parliament after the JURI vote of 2003/06/17

#neW: Explanatory Statement by Arlene McCarthy.  Slightly revised version of her draft report of February, without corrections but with additions, e.g. mention of a small company in an unemployment blackspot in Southwest England which benefitted from voice recognition related software patents.

#oWW: D'autres contacts disponibles sur demande

#ncW: La réalité, c'est que les petites entreprises sont menacées par les spéculateurs en brevets et par les énormes porte-feuilles des grosses entreprises, qui souhaitent que leurs investissements en R&D se répercutent dans l'expansion de leurs ventes de licences.  Malheureusement, l'analyse de l'expérience américaine qu'a faite la rapporteur se limite à une seule phrase non documentée, non corroborée et sortie de son contexte, écrite par deux juristes en brevets anglais. Elle a évité de rencontrer les observateurs désintéressés et elle ignore complètement l'étude commandée par le parlement lui-même au prestigieux Institut du droit de l'information [Instituut voor Informatierecht IVIR - NdT]. Au lieu d'approfondir les questions complexes d'affaires et d'économie que soulèvent les brevets de logiciels, elle traite ces questions comme un compromis politique. Si l'Europe veut sincèrement une politique de l'innovation digne de ce nom, elle devrait attendre les rapports à venir de l'Académie des sciences américaine et de la commission fédérale sur le commerce (Federal Trade Commission)  et repenser plus à fond, en s'aidant d'économistes, à la façon dont le logiciel devrait être réglementé.

#ohe: Brian Kahin, professeur en études sur les politiques d'infrastructure de l'information et ancien conseiller de la Maison blanche sur le sujet, juge déroutant le processus de décision européen :

#Wcf: Les dogmes facilitent la vie des législateurs, mais ils peuvent coûter cher au commun des mortels. Les brevets de logiciels s'obtiennent facilement et pour pas cher, dans la mesure où ils ne nécessitent aucun travail de laboratoire. Il suffit de breveter une idée abstraite. Il y a actuellement au Japon des efforts concertés, parrainés par le gouvernement, pour sécuriser préventivement toutes sortes d'idées de logiciel. Des femmes au foyer suivent des cours en dépôt de brevets. La Commission juridique vient s'assurer que ces brevets viendront en Europe. Ainsi, même si nous rattrapons le retard et que nous atteignons un niveau similaire en matière d'infrastructure de communication que l'Asie orientale, nous ne serons pas autorisés à l'employer efficacement, puisque la route est obstruée par les brevets des premiers qui sont passés par là. Aujourd'hui déjà, 75% des brevets de logiciels que l'Office Européen des Brevets (OEB) accorde illégalement, sont détenus par des entreprises provenant des États-Unis ou du Japon et comme le montre %(ee:l'expérience empirique), une législation promue par les propriétaires de brevets a eu tendance dans le passé à stimuler un afflux de brevets étrangers au détriment de l'économie locale.

#nrn: Aujourd'hui, l'Europe semble poursuivre selon le même genre de dogme : privatisation des idées abstraites et érection de barrières avec des méthodes qui ne fonctionnent tout simplement pas, comme nous l'enseignent le bon sens et les études économiques.

#sia: L'Europe accuse aujourd'hui un retard de 7 ans sur la Corée et le Japon, où les citoyens payent 12 € par mois pour un accès Internet ADSL à 24Mbps. La raison en est que les politiciens de l'UE ont adopté le dogme qui veut que la construction de l'infrastructure doive être confiée à l'initiative privée.

#nsp: La Commission juridique a résolu aujourd'hui de %(e:ne pas) empêcher la monopolisation de l'infrastructure informationnelle publique. Si l'assemblée plénière ne réalise pas ce qui est en train de se passer, bientôt chaque citoyen devra payer une nouvelle taxe, non plus à l'État, mais aux groupes américains et japonais, puisque ceux-ci possèdent les droits de brevet sur les communications numériques de la société de l'information.

#nsn: Jean-Paul Smets est le PDG de %(NE), une jeune entreprise qui performe bien dans les progiciels de gestion intégrés. Il possède plusieurs brevets de logiciels et de méthodes d'affaires. Il possède une formation d'informaticien et d'économiste. Il fut conseiller des pouvoirs publics en informatique et en économie, avant de fonder Nexedi.  M. Smets a rédigé en 2001 une importante %(ss:étude sur les brevets de logiciels)pour le compte d'un organisme officiel français. À la nouvelle du vote intervenu à la Commission juridique, il exprime à nouveau les préoccupations qu'il avait exposées lors du %(dk:le symposium sur le brevet de logiciels de Bruxelles en mai 2003) :

#aei: Après la session de la Commission juridique, Arlene McCarthy a été prise à partie par un collègue qui lui opposait les études réalisées par les experts sollicités par le Parlement européen. Elle a répondu %(q:N'importe qui a bien le droit d'écrire n'importe quoi) et puis tout à coup il fallait absolument qu'elle se rende à réunion.

#ttr: D'un autre témoin :

#nbe: Le président a résolu la question, ce n'était pas 4 verts, mais deux verts plus deux GUE (l'autre parti écologiste au parlement).

#sWs: Pendant le vote, elle a également demandé (en jetant un regard derrière elle) s'il était normal que les verts aient quatre votes (elle était probablement surprise d'avoir un tiers contre elle).

#rml: J'ai été un peu surpris d'entendre Arlene McCarthy dire : %(q:Laissons les experts faire leur travail). Cela voulait dire %(q:Laissons l'OEB (ou les experts en brevet) faire leur travail), alors qu'ils auraient dû solliciter les %(q:experts en logiciel).

#iWW: D'un témoin à la session de la Commission juridique :

#nvm: Arlene McCarthy et ses collègues MM. Wuermeling et Harbour du PPE ont refusé de consulter aucun expert pour les questions législatives. Du point de vue juridique, le champ est laissé libre aux experts en brevets, charge à eux de démêler les fils de ces formules incantatoires auxquelles le législateur lui-même ne comprenait rien.

#rmw: On peut considérer cela comme la réponse de Arlene McCarthy à la question qui lui a été maintes fois soumise, à savoir si selon ses intentions, %(ts:certaines revendications de brevets bien connues) seront ou non acceptées et si sa proposition de directive se prononcera sur ces cas.

#jet: J'espère, chers membres, que nous sommes conscients qu'en fin de compte nous avons accompli des progrès dans ce domaine et que nous avons tenté d'instaurer certaines limites, sans réinventer tout le droit des brevets, ce pour quoi, je m'empresse de l'ajouter, nous n'avons pas compétence. Nous sommes des législateurs, nous créons un cadre et des lois qui devront être interprétés par les experts, mais nous ne sommes pas des experts nous-mêmes.

#ctW: Mrs McCarthy conclue ainsi son dernier discours d'avant le vote, devant la Commission juridique (d'après %(am:enregistrement sonore)) :

#kse: Si vous gagnez déjà de l'argent avec quelque chose, pourquoi devriez-vous l'enfermer ? Je pense que les brevets sur les logiciels ne sont pas une bonne chose. Il est préférable de se faire concurrence dans d'autres domaines tels que la formation, le conseil, les bibliothèques spécifiques et le développement d'outils complémentaires. Dans certains cas cependant, vous êtes contraints de demander un brevet pour coopérer avec des tiers, par exemple sous forme de licences combinées.

#foe: Hans Appel, directeur technique pour l'Europe du nord chez Sun. Déclaration dans une entrevue à la presse néerlandaise après le vote à la Commission juridique :

#esW: Le Parlement européen semble résolu à ignorer les signatures de plus de cent mille citoyens européens opposés aux brevets de logiciels et à foncer tête baissée vers leur légalisation. Si cela se produit, nous verrons des gros paiements en droits de licence de l'industrie européenne du logiciel vers l'industrie américaine, nous verrons des tas de litiges à base de brevets de logiciels, de brevets Internet et de brevets en méthodes d'affaires et nous verrons des effets très dommageables pour le logiciel libre. De plus, l'introduction de nouveaux droits monopolistiques dans le secteur de la société de l'information n'aidera certainement pas l'UE a réaliser son objectif stratégique %(q:de devenir l'économie la plus compétitive au monde et la plus orientée vers le savoir).

#tmp: Les entreprises polonaises ne possèdent pratiquement aucun brevet de logiciel et la tradition polonaise est de refuser d'accorder de tels brevets. La directive McCarthy amènerait un changement radical en Pologne. Dans le considérant 16, il est dit clairement que cette proposition doit être considérée comme une mesure pour protéger les économies développées contre la concurrence des %(q:économies à faible coût de revient). Même si je ne crois pas qu'une protection contre la légitime concurrence dans l'industrie logicielle n'avantage aucun pays, elle va certainement affecter plus gravement l'Europe de l'est que l'Europe de l'ouest, tout simplement parce qu'elle impose des coûts juridiques que nous sommes moins à même d'assumer.

#lWW: C'est aujourd'hui au tour du Parlement européen de prendre une décision sur une proposition de directive très controversée portant sur les brevets de logiciels. Cette proposition de la Commission juridique aurait dû clarifier les choses et elle aurait dû définir une bonne foi clairement ce qui est brevetable et ce qui ne l'est pas. On se retrouve malheureusement avec un florilège de formules incantatoires auquel même les experts juristes ne comprennent goutte. C'est un désastre, en particulier pour les petits et moyens développeurs de logiciels. Une seule plainte en contrefaçon de brevet peut ruiner de telles entreprises. Il faut espérer que le parlement européen va comprendre cela s'il doit se prononcer sur ce projet de directive dans quelques jours.

#rsl: Commentaires sur la décision de la Commission juridique par Reinier Bakels, %(ss:chercheur à l'institut de Droit de l'informatique)de l'Université d'Amsterdam et co-auteur d'une %(ss:étude) sur le projet de directive, commandée par le parlement européen :

#tgr: L'étude que nous avons menée pour le compte de la Commission européenne mentionnait qu'il n'y avait aucune démonstration à l'appui d'un élargissement de la brevetabilité logicielle et que  les recherches sur les effets économiques des brevets de logiciels en général n'avaient pu conclure. J'ai estimé que la Recherche penchait plutôt pour la négative. Cependant, j'ai dû faire des compromis sur cette formulation, puisqu'il s'agissait d'une décision en comité et que mes co-auteurs étaient des experts du droit des brevets ayant à priori de fermes convictions quand à l'utilité des brevets pour les entreprises désireuses de lever des capitaux. Mes co-auteurs ont estimé qu'il serait difficile de s'opposer à la brevetabilité des logiciels ayant un effet technique, mais qu'il ne fallait pas permettre la multiplication des mauvais brevets comme aux États-Unis. J'ai estimé que même cette conclusion de compromis représentait d'une certaine façon un progrès : Au moment où l'étude a été commandée, l'hypothèse dominante que nous devions copier le modèle américain n'était quasiment pas contestée. Si nous devions entamer la rédaction de cette étude aujourd'hui, nous irions probablement plus loin. L'étude récente de Bessen & Hunt semble apporter une démonstration bien plus solide de ce que les brevets aux États-Unis ont étouffé l'innovation. Aujourd'hui, la charge de la preuve devrait incomber à ceux qui proposent des lois pour généraliser les brevets dans le domaine du logiciel.

#ecf: Les membres du comité JPEG ont perdu des années à contrer des revendications de brevets futiles lesquelles en sont presque parvenues à ruiner nos efforts de normalisation. Avec la nouvelle réglementation proposée par la Commission juridique, on s'engage dans encore plus de problèmes. L'Office Européen des Brevets (OEB) n'a jamais démontré plus de pitié envers l'industrie européenne du logiciel que son homologue américain (USPTO), mais leurs brevets étaient jusque là caducs dans la majeure partie de l'Europe. Désormais, en vertu de l'ordre de vote de Arlene McCarthy, le champ libre donné à l'OEB/USPTO en Europe va non seulement nous replonger continuellement dans des litiges pour histoires des brevets de logiciels vagues et triviaux, mais en plus nous perdons tout espoir de bénéficier d'une exception aux fins de l'intéropérabilité. Tandis qu'aux États-Unis on prend espoir que les tribunaux cèdent à la pression montante de l'opinion publique et réforment ici ou là, la Commission juridique s'apprête à consacrer l'incurie de l'office des brevets comme étant l'état de l'art. Les grandes entreprises spoliatrices américaines seront autorisées à désertifier le paysage informatique européen, alors que les États-Unis sont déjà en train de se remonter la pente.

#oem: Arlene McCarthy et ses partisans sont résolus à assassiner l'intéropérabilité sur le World-Wide-Web et à renforcer les grands monopoles américains. Au Consortium du World-Wide-Web (W3C), nous nous sommes débattus pendant des années dans les problèmes de la politique de brevets. La Commission de l'industrie a proposé d'exempter du domaine d'application des brevets l'usage de logiciels aux fins d'intéropérabilité. C'eut été un soulagement pour les efforts de normalisation. Cela nous aurait protégé contre les pires pratiques anti-concurrentielles, sans affecter sensiblement le lobby des détenteurs de brevets. Pourtant cet amendement très modéré a été écarté, sans qu'on nous donne de raison. Nos démarches devant le parlement ont été complètement ignorées. La pratique législative du rapporteur de la Commission juridique ne traduit en rien les intérêts de l'industrie européenne du logiciel.

#imy: Les commentaires de Håkon Wium Lie, dir. tech. de %(op:Opera Inc)à propos de %(vl:l'ordre de vote de Arlene McCarthy) :

#kGA: Certains membres éminents de la Commission juridique ne se contentent pas de mettre en danger l'industrie du logiciel, ils veulent également s'assurer que tout programmeur se heurtera à des brevets sitôt qu'il publiera un programme sur le Web. Alors que les commissions CULT et ITRE avaient introduit des gardes-fou pour la liberté de publication, Arlene McCarthy les ignore purement et simplement et recommande à la place  %(q:l'amendement de compromis 1), qui fait de la simple publication une contrefaçon directe. D'un point de vue d'entreprise, il semble presque irresponsable d'aller vendre des distributions Linux dans ces conditions. Si l'un quelconque des programmes parmi les milliers que compte notre distribution, contrefait l'un quelconque parmi ces dizaines de milliers de brevets de logiciels vagues et triviaux qu'à accordé l'OEB, nous pouvons être poursuivis et contraints de retirer nos CD du marché. Des cas similaires se sont déjà produits en vertu des marques de commerce, mais l'insécurité juridique à  laquelle nous exposent les brevets est bien plus grande encore. Des affaires comme « SCO » ou « eBay » ont bien montré que ce danger n'est pas de pure théorie. Au moment même où les administrations publiques se procurent des systèmes d'exploitation libres ou les utilisent pour amener Microsoft à réduire ses prix,  Arlene McCarthy et ses alliés semblent rejoindre la croisade de Microsoft pour éradiquer le logiciel libre. L'Union chrétienne-sociale bavaroise semble y être pour quelque chose, d'ailleurs, au même moment, elle %(cs:prend fait et cause pour Microsoft) en Bavière. Arlene McCarthy elle-même, %(gm:s'en est prise publiquement au contrat de licence publique GNU GPL), qu'elle qualifie comme %(q:un genre de monopole). Mrs   McCarthy et son allié le Dr. Joachim Wuermeling ont décliné toute invitation au dialogue de notre part et ont refusé de répondre à nos questions. Bien sûr, ils ne sont pas obligés de nous adresser la parole, mais renforcer les monopoles en liant les mains des concurrents est quasiment la pire chose qu'un gouvernement puisse se faire à lui-même.

#mhW: Un grand nombre des personnalités %(sm:ont demandé à la Commission européenne) et aux partisans de la proposition de directive amendée d'en clarifier les conséquences en se prononçant sur des cas bien connus (notamment le brevet « One-Click » de Amazon). Autant que je sache, ils n'ont pas obtenu de réponse à leurs questions.

#lue: Il s'en suit clairement qu'une directive harmonisant les pratiques actuelles de l'OEB autoriserait %(e:la plupart) des nouvelles méthodes d'affaires (réalisées sur ordinateur), y compris le « One-Click » de Amazon.

#Wit: %(or:Cette déclaration)émane du très réputé cabinet allemand %(q:Wuesthoff & Wuesthoff), conseillers en propriété industrielle :

#tcs: Soyons assurés que la proposition de directive amendée vise à harmoniser les pratiques actuelles de l'Office européen des brevets OEB. Voyez les déclarations de ces spécialistes en propriété intellectuelle, datant de 2001 :

#una: Je me suis %(sp:beaucoup documenté) au cours de la dernière année sur cette proposition de directive et je suis bien au courant des amendements qui ont étés adoptés à la Commission juridique. Je ne crois pas que les brevets sur les méthodes d'affaires tels le « One-Click » de Amazon pourraient être évités en Europe si la proposition de directive ainsi amendée était adoptée au parlement.

#mto: Stefan Pollmeier dirige une entreprise d'électronique de 45 employés dans la région de Francfort en Allemagne. 80% de leur effort R&D concerne les logiciels.

#isf: Contrairement aux affirmations publiques de certains partisans de la directive, celle-ci rend brevetable les algorithmes et les méthodes d'affaires [littéralement : « destinées à l'exercice d'activités économiques », selon les termes de la directive. NdT.]. Comment pourrait-il en être autrement ? Que pourrait-on breveter dans nos logiciels, si ce n'est des algorithmes et des méthodes d'affaires ?

#ead: Nos conseillers en brevets ont convaincu certains de nos directeurs que la directive européenne serait une bonne affaire parce que ça nous permettrait de jouer au petit jeu des brevets en Europe aussi. Je pense néanmoins qu'ils se trompent et leur point de vue ne s'est pas imposé dans l'entreprise.

#omi: SAP a énormément souffert du fait de brevets américains portant sur ce que - selon la présente directive - il conviendrait d'appeler des %(q:inventions mises en œuvre par ordinateur apportant une contribution technique par leur activité inventive). SAP est devenue une grosse entreprise en s'appuyant sur ses droits d'auteur. Le risque d'être imités ne nous a jamais fait. Nous n'avons pas besoin d'une protection par les brevets. En revanche nous aurions besoin d'une protection contre les brevets. SAP s'est faite extorquée des sommes importantes par des individus à tendance délinquante, armés de brevets (y compris des professeurs d'universités américaines réputées). Les brevets de logiciels légalisent et encouragent dans l'industrie du logiciel des comportements immoraux voire criminels. Il n'existe probablement aucun brevet de logiciel qui ne soit trivial. Il s'en suit que la seule utilité des brevets de logiciels est de constituer des munitions en cas de conflits entre entreprises : (%(q:Vous avez contrefait mes 100 brevets - dont je sais aussi bien que vous qu'ils sont triviaux et ineptes, même si on ne le reconnaîtra jamais publiquement -, nous avons contrefait vos 100 brevets - tout aussi triviaux et ineptes -; par conséquent, arrangeons-nous entre nous et assurons-nous plutôt que personne d'autre ne vienne nous déranger)). La course aux armements coûte très cher à toutes les entreprises du secteur logiciel. SAP a dû, pour assurer sa défense, monter un département de brevets en 1998. Ce département ponctionne des ressources qui auraient pu être investies en R&D. Cependant, même un grand nombre de brevets ne nous met pas à l'abri d'individus à tendance délinquante.

#rna: Brian Kahin, University of Michigan, Ann Arbor, États-Unis

#lEg: Jean-Paul Smets-Solanes, PDG de Nexedi, Lille, France

#ePW: Arlene McCarthy, députée européenne : « Nous ne sommes pas des experts »

#pct: Hans Appel, dir. tech. de Sun Microsystems pour l'Europe du nord

#ran: Karl-Friedrich Lenz, professeur de Droit allemand et européen, Université Aoyama Gakuin, Tokyo

#bma: Jozef Halbersztadt, examinateur de brevets à l'Office polonais des brevets

#eop: Reinier Bakels, auteur d'une étude commandée par le parlement sur la brevetabilité des programmes d'ordinateurs

#jai: Peter Holmes, économiste, ayant participé à une étude commandée par l'UE, University of Sussex

#jir: Richard Clark, PDG de Elysium Co Ltd et rédacteur en chef pour le comité de normalisation JPEG

#njp: Håkon Wium Lie, dir. tech. de Opera Inc

#hdE: Bernhard Kaindl, développeur chez SuSE Linux AG

#Wee: Stefan Pollmeier, dir. de ESR Pollmeier GmbH, Allemagne

#WSf: Dr. Bernhard Runge, développeur chez SAP AG, ancien professeur de mathématiques.

#vav: Si le serveur est défaillant, essayez la %(xv:version présentée sur la page de Xavi).

#teM: Compte-rendu du vote à la Commission juridique. Document MS-Word converti en PDF.

#Wtt: This page contains links to (mis)information about the proposed directive, from the perspective of the proponents, including %(q:forecasts) and the text adopted by JURI.

#WoW: Title page leading to the text as adopted by JURI

#kni: JURI-amended and voted directive

#cam: Explains among others: that %(q:susceptible of industrial application) is not a restriction at all, because it only means %(q:commercially valuable); that program claims, as proposed by JURI, are welcome from the European Commission's point of view.

#WWq: Angelika Niebler, DE conservative MEP (christian social union from Bavaria, close associate of EPP rapporteur Wuermeling who was absent), praises CEC proposal, McCarthy report and JURI %(q:compromise amendments) as %(q:great work).  Contradicts Rothley's negative comments.

#eWW: Malcolm Harbour, UK conservative MEP, celebrates CEC Software Patent Directive Proposal and McCarthy report as a great achievement in limiting patentability, explains that program claims are needed, because mobile phones are patentable and the functioning principles of a mobile phone might well be embodied in a program that is stored on a medium and sold in that form.  Answers Berenguer on some points.

#rwt: Luis Berenguer MEP (PSE, Spain, law professor), warns of dangers of software patents, says that directive does not achieve what its proponents claim to be aiming for, recommends amendment 56.  This speech was the only one that criticised the McCarthy proposal at all, albeit not very strongly.

#Wai: Overview of transcripts of speeches given in JURI one day before their vote on the software patentability directive.  The only critical speech was delivered by Luis Berenguer (PSE, Spain)

#bWW: Dans ce discours devant la Commission juridique la veille du vote, Arlene McCarthy argumente qu'il y a eu suffisamment de débats et qu'il est temps de voter. Merci à Erik pour la transcription de cet enregistrement sonore.

#ltW: Montre comment tous les points de vue autres que ceux du lobby pro-brevet ont été systématiquement écartés par la Commission juridique depuis qu'Arlene McCarthy en est devenue le rapporteur en mai 2002.

#ilI: Décrit les intentions et les décisions de la Commission juridique, ce qui a été rejeté et ce qui a été approuvé et quelles seront les conséquences si ces décisions sont suivies par le parlement.

#pls2: Donnez un coup de main pour rédiger d'autres compte-rendus détaillés à partir des enregistrements sonores !

#pls: Donnez un coup de main pour rédiger d'autres compte-rendus détaillés à partir des enregistrements sonores !

#Wyj: Le député social-démocrate allemand Willi Rothley exprime sa réprobation envers la proposition de la Commission européenne, la jugeant embrouillée et inutile : « Les tribunaux de brevets trouvent le bon équilibre par eux-même et le législateur est peu capable d'établir des règles meilleures que celles qu'élaborent les tribunaux. Cependant, le mécanisme législatif est comme une machine à saucisses qu'il faut faire tourner sans arrêt. De temps en temps, elle pond de nouvelles lois, sans qu'on sache si quelqu'un en a besoin et nous les députés nous sommes enchaînés à cette machine et condamnés à essayer d'en tirer le meilleur parti. » Les rires qui l'ont accompagné montrent que le discours de Willi Rothley a touché un point sensible au sein de la Commission juridique .

#sns: english translation of Rothley's speech

#eWe: Willi Rothley MEP (DE, PSE) says that the directive as proposed by the European Commission is poorly crafted and unnecessary and that it should really be sent back to the European Commission.  But JURI keeps rotatint like a sausage machine, turning out one law after another, and we have to make the best out of it.

#tmW: Ne manquez pas le second commentaire de Willi ROTHLEY, le lundi 16 : « JURI est une machine à saucisses ».

#cok: Bericht und Forums-Diskussion

#xrW: Pro-Linux 2003/06/17: Softwarepatente rücken ein Stück näher

#ses: Les libéraux ont adopté une position ambiguë, appuyant la plupart des propositions de la Commission de l'industrie (contre les brevets) mais appuyant aussi les propositions inverses de Mrs McCarthy; ce qui fait que les propositions apportées par la Commission de l'industrie (ITRE) furent considérées battues et ne furent même pas soumises au vote. Ainsi, la proposition de Mrs McCarthy de faire de la simple publication d'un logiciel une contrefaçon directe de brevet a été adoptée avec le soutien des socialistes, du Parti Populaire et des libéraux. De même, la proposition de ITRE de garantir la liberté de publication dans le domaine du logiciel a été retirée de l'ordre du jour.

#nWW2: Dans son ordre de vote, Arlene McCarthy a recommandé de s'opposer à tous les amendements qui tendaient à restreindre la brevetabilité ou l'application des brevets et d'appuyer la plupart de ceux allant dans le sens inverse ou ceux qui apportaient une justification aux brevets de logiciels. L'ordre de vote de McCarthy a été suivi par les deux tiers environ des membres de la Commission juridique. S'y sont opposés, pour les verts : Mercedes Echerer (AT) et Neil MacCormick (UK), pour la gauche : Ilka Schröder (DE) et Pernille Frahm (DK), pour les socialistes : Evelyne Gebhardt, pour le Parti populaire : Piia-Noora Kauppi (FI), le non-inscrit Marco Cappato et un autre député que nous n'avons pas encore identifié. Cependant, la majorité n'a pas suivi Mrs McCarthy sur quelques points, le plus important étant ITRE-15, qui autorise le recours à un procédé breveté quand l'interopérabilité est en jeu.

#gio: Proposition législative

#are: Vote final, projet de directive

#bei: Abstentions

#cnr: Contres

#pro: Pour

#dokurl: URL permanente de cette publication

#ffii: À propos de la FFII

#eurolinux: À propos de l'Alliance Eurolinux

#media: Contact médias

#ten: Intégrale des déclarations émanant de l'industrie et de l'université

#tnJ: Que s'est-il passé à la Commission juridique (JURI) ?

#title: Vote à la « JURI » pour des restrictions bidons de la brevetabilité

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: lcaprani ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnjuri030617 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

