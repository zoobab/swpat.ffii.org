\contentsline {section}{\numberline {1}introduction}{3}{section.1}
\contentsline {section}{\numberline {2}What happened in JURI?}{4}{section.2}
\contentsline {section}{\numberline {3}Full Statements from Industry and Academia}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Dr. Bernhard Runge, developer at SAP AG and former professor of mathematics}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Stefan Pollmeier, MD of ESR Pollmeier GmbH, Germany}{10}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Bernhard Kaindl, developer at SuSE Linux AG}{11}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}H\r a{}kon Wium Lie, CTO of Opera Inc}{12}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Richard Clark, CEO of Elysium Co Ltd and chief editor for the JPEG standardisation committee}{12}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Peter Holmes, economist, participant of an EU-commissioned study, University of Sussex}{13}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Reinier Bakels, author of parliament-ordered study on software patent directive}{13}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Jozef Halbersztadt, patent examiner at the Polish Patent Office}{14}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Dr. Karl-Friedrich Lenz, Professor for German Law and European Law, Aoyama Gakuin University, Tokyo:}{14}{subsection.3.9}
\contentsline {subsection}{\numberline {3.10}Hans Appel, CTO of Sun Microsystems for Northern Europe}{14}{subsection.3.10}
\contentsline {subsection}{\numberline {3.11}Arlene McCarthy MEP: We are not experts}{15}{subsection.3.11}
\contentsline {subsection}{\numberline {3.12}Dr. Jean-Paul Smets-Solanes, CEO of Nexedi, Lille, France}{16}{subsection.3.12}
\contentsline {subsection}{\numberline {3.13}Prof. Brian Kahin, University of Michigan, Ann Arbor, USA}{17}{subsection.3.13}
\contentsline {section}{\numberline {4}Media Contacts}{17}{section.4}
\contentsline {section}{\numberline {5}Permanent URL of this Press Release}{17}{section.5}
\contentsline {section}{\numberline {6}Annotated Links}{17}{section.6}
