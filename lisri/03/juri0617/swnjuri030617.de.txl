<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#oag: Viele Leser ließen sich von Newman/Yahoo irreführen, andere klären auf.

#tWi: Ein öffentlicher Brief an Matthew Newman, Author eines Yahoo-Artikels, der die JURI-Entscheidung verniedlicht.

#0ix: phm 2003/06/17: Abstimmung in JURI: Ergebnis

#0sU: phm 2003/06/17: Abstimmung in JURI jetzt

#Wfp: Dr. Lenz, professor of European law in Tokyo, comments on the Council's professed commitment to %(q:protection of patents on computer-implmenented inventions.)

#lEr: Der Europäische Rat (?!?) will anscheinend nicht länger %(q:die Innovation schützen), ganz zu schweigen davon, sie zu stimulieren, sondern nur noch %(q:Patente schützen).  Das ist jedenfalls das, was ein paar Bürokraten auf dem Gipfel am 20. und 21. März in das abschließende Dokument eingefügt haben.  Die Ministerialbeamten des Europäischen Rates waren in den verganenen Jahren besonders für die Interessen der Patentabteilungen großer Konzerne empfänglich.  Auf Verlangen der Patentlobby schlugen sie vor, einen Schritt weiter als die Europäische Kommission zu gehen, und die Veröffentlichung von Programmcode auf einer Website als direktes Patentvergehen zu klassifizieren.

#Wne: quotes Daniel Cohn-Bendit and Mercedes Echerer

#Eet: Die Grünen/EFA Pressemitteilung: Verheerende Abstimmung im JURI

#mht: Miquel Mayol, MEP der Grünen/EFA aus Katalonien veröffentlicht eine Pressemitteilung kurz vor der Abstimmung, in der er vor den verheerenden Folgen der drohenden JURI-Entscheidung auf SMEs warnt, die den größten Teil der katalonischen Softwarewirtschaft darstellen.

#RWe: Carles Gasoliba i Bohn (CdC, CiU, ELDR), liberaler MEP aus Katalonien, warnt davor, daß die JURI-Entscheidung zur Einführung weitgefaßter und trivialer Patente für Nicht-Erfindungen führt und fordert, daß kontrollierbare Naturkräfte wesentlicher Teil jeder Erfindung im Sinne des Patentrechts sein sollen.

#Ape: Ähnlich wie %(q:European Voice) scheint EurActiv eine Plattform für Journalismus und Lobbyarbeit im Umfeld von EU-Einrichtungen zu sein, unterstützt von großen Konzernen. Dennoch ist dieser kurze Bericht informativer als die von IDG, Yahoo oder der EU-eigenen Nachrichtenquellen.

#Hen: In a public letter to Dow Jones journalist Matthew Newman, Hartmut Pilch points out that JURI, contrary to what Newman wrote in Yahoo, did not impose any limits on patentability but in fact did everything to assure that ideas of the Amazon One-Click type are indisputably considered patentable inventions in Europe and that such patents can be enforced in ways which even the European Commission had not proposed.

#WyW: Der Journalist Matthew Newman kopiert Propaganda der Patentlobby. Die Wahrheit ist: JURI ersetzt einen bereits vereinheitlichten Patentstandard durch eine Sammlung undurchsichtiger Regeln, die Amazons Ein-Klick-Idee zu einer patentierbaren Erfindung machen und keine Vereinheitlichung schaffen, sondern bestenfalls das EPO fester damit betrauen.

#tIM: Deutsches IT-Nachrichtenmagazin mit einem lebhaften Diskussionsforum ähnlich Slashdot.  Der Artikel zitiert den FFII und Cohn-Bendit.  Das Forum ist voll mit wütenden Verurteilungen von McCarthy und den JURI-Politikern allgemein.

#eaU: Ein EU-Organ stellt Arlene McCarthy's Standpunkt als offizielle Wahrheit dar.

#bWI: Ein recht objektiver Bericht über was JURI wählte und wogegen JURI stimmte.  Zitiert auch einige Teile unserer Pressemitteilung.

#eta: Computerwoche, die deutsche Branche der IDG, plappert Paul Meller's = Arlene McCarthy's Falschinformation wie ein Papagei nach

#hrr: Computerwoche: Kommission des Europaparlaments macht Vorschlag für EU-Patentgesetz

#sWo: Paul Meller, IDG-Korrespondent in Brüssel, presentiert den JURI-Vorschlag als eine Leistung zur Begrenzung der Patentierbarkeit, einen Mittelweg zwischen den beiden Extremen, entweder den USA oder ausschließlich dem Opensource-Geschäftsmodell zu folgen.

#bee: Report submitted to the Parliament after the JURI vote of 2003/06/17

#neW: Explanatory Statement by Arlene McCarthy.  Slightly revised version of her draft report of February, without corrections but with additions, e.g. mention of a small company in an unemployment blackspot in Southwest England which benefitted from voice recognition related software patents.

#oWW: Weitere Kontakte auf Anfrage

#ncW: The reality is that small companies are threatened by patent speculators and massive unreadable portfolios of large companies, who want recognition of their R&D investments through rapidly expanding licensing programs.  Unfortunately the rapporteur's analysis of the U.S. experience is limited to one unsubstantiated, undocumented sentence written by two British patent lawyers and lifted out of context.  She has avoided meeting with informed disinterested observers and completely ignores the Parliament's own commissioned study, authored by the prestigious Institute for Information Law.  Instead of investigating the complex business and economic issues behind software patents, she treats the issue as a political compromise.  If Europe is serious about sound innovation policy, it should await the forthcoming reports from the National Academic of Sciences and the Federal Trade Commission and, with help from its own economists, think much harder about how software should be regulated.

#ohe: Brian Kahin, professor for information infrastructure policy studies and former Whitehouse advisor on this subject matter, finds the European decision process bewildering:

#Wcf: Dogmas machen das Leben einfacher für den Gesetzgeber. Aber für den Rest der Bevölkerung, kann der zu zahlende Preis sehr hoch sein. Softwarepatente sind einfach und billig zu beschaffen, denn man benötigt keinerlei praktische Arbeit. Man patentiert einfach einige neue abstrakte Ideen. Derzeit gibt es in Japan konzentrierte Bestrebungen - von der Regierungs gestützt - alle Arten von Software-Ideen vorsorglich zu patentieren. Hausfrauen besuchen Kurse zum Schreiben von Patentanmeldungen. JURI ist nun auf dem besten Weg, sicherzustellen, daß alle diese Patente auch nach Europa  kommen. Somit werden, auch wenn wir eines Tages aufholen und den gleichen Grad an Kommunikations-Infrastruktur erreichen wie Ost-Asien, wir wohl nicht  in der Lage sein, diese effizient zu nutzen, denn der Weg ist überhäuft mit Patenten zuerst handelnden. Zum heutigen Tag gehören beireits 75%  der Software-Patente, die das Europäische Patentamt rechtswidrig erteilt hat, Unternehmen aus den USA und Japan, und %(ee:empirische Nachweise) zeigen, daß die von Patentbesitzern getriebene Gesetzgebung in den letzten Jahren dazu, tendierte, den Zustrom ausländischer Patente zu stimulieren und damit die einheimische Wirtschaft weit zurück zu werfen.

#nrn: Nun sieht es so aus, als ob Europa wiedereinmal der gleichen Art Dogma folgt: Privatisierung von abstrakten Ideen, Errichtung von Einschränkungen auf eine Weise die einfach nicht funktionieren kann, wie gesunder Menschenverstand und wirtschaftliche Studien aufzeigen.

#sia: Europe hinkt nun 7 Jahre hinter Korea und Japan her, dort wo Einwohner 12 EUR je Monat für einen 24 Mbps ADSL-Internetzugang bezahlen. Das weil EU-Politiker sich entschlossen, an das Dogma glauben, daß Infrastruktur-Aufbau den privaten Initiativen überlassen werden muß.

#nsp: Heute stimmte JURI dafür, %(e:nicht) die Monopilisierung der öffentlichen Informations-Infrastruktur zu stoppen. Wenn die Abgeordneten nicht bald Ihre Augen für das öffenen was passiert, werden alle Bürger bald eine neue Steuer zahlen, nicht an den Staat, sondern an Amerikanische und Japanische Unternehmen, weil diese die Patentrechte an digitaler Kommunikation der Informationsgesellschaft besitzen.

#nsn: Jean-Paul Smets, CEO von %(NE), einem erfolgreichen Software-Startup im Bereich Enterprise Ressource Planning, und Eigentümer einiger Europäischer Patente auf Programme und Geschäftsmethoden, war ausgebildet als Informatike und Ökonom und arbeitete als französischer Industrieplanungs-Administrator, bevor er Nexedi gründete. Smets verfaßte eingie wichtige %(ss:Studien über Software-Patente) für französische Regierungseinrichtugnenim Jahre 2001. Als er vom Abstimmungsergebis der JURI erfuhr, wiederholte er seine Benkden, die er bereits auf dem %(dk:Symposium zu Software-Patent in Brussels im Mai 2003) äußerte:

#aei: After the JURI session, Arlene McCarthy was involved in a discussion with a colleague who confronted her with the studies written by the experts whom the European Parliament had asked for an opinion.  She then said %(q:Anybody can write anything) and suddenly had to go to a next meeting.

#ttr: Another witness writes:

#nbe: The president then solved the issue, it was not 4 Green, but 2 Green + 2 GUE (the other green-like group in parliament).

#sWs: During the vote she also asked (turning to see behind her) whether it was normal the green had 4 votes (maybe she was surprised that 1/3 was against her).

#rml: I was a bit surprised to hear Arlene McCarthy say this: %(q:Let the expert do their work could mean let the EPO (or patent expert) do their work.) Where they should have asked the %(q:Software experts).

#iWW: One witness to the JURI session confirms:

#nvm: Arlene McCarthy and her colleagues Wuermeling and Harbour from PPE refused to consult any expertise at the legislative level.  At the judicative level, matters will be left to patent experts, if any, to straighten things out by interpreting magical forumlas which not even the legislator understood.

#rmw: This may be seen as Arlene McCarthy's response to the question which has been repeatedly put forward for her from many sides, namely whether certain %(ts:sample patent claims) are intended to be patentable or not, and what, if anything, in her directive proposal says so.

#jet: So I hope members, and I find those members that support my approach, and I hope that members, well, at the end of the day, feel that we have actually made progress in this area, and that we have attemted to set some limits in perhaps a moderately restrictive way, without entirely reinventing patent law, which I would hasten to add, we are not in ability to do that, we are legislators to create framework and laws for interpretation by experts, but we are not experts ourselves.

#ctW: In her last speech to JURI before the vote, according to the %(am:sound recording), McCarthy concluded as follows:

#kse: Wenn Sie bereits Geld für etwas erhalten haben, warum sollten Sie es dann geheim halten? Ich denke, Softwarepatente sind keine gute Sache. Es ist besser, auf anderen Gebieten zu konkurrieren, wie Schulung, Beratung, spezielle Softwarebibliotheken und der Entwicklung von Zusatzprogrammen. In manchen Fällen sind Sie jedoch gezwungen, ein Patent zu beantragen, zum Beispiel für gemeinsame Lizenzen bei der Zusammenarbeit mit Dritten.

#foe: Hans Appel, CTO für Nordeuropa von Sun, sagte in einem Interview mit der dänischen Presse nach der JURI-Abstimmung:

#esW: Das Europaparlament will offenbar Softwarepatente legalisieren, trotz der mehr als 100.000 Unterschriften von EU-Bürgern, die sich dagegen ausgesprochen haben. Wenn das passiert, werden große Lizenzzahlungen von der europäischen an die amerikanische Softwareindustrie fließen, wir werden viele Rechtstreitigkeiten gegründet auf Softwarepatente, Internetpatente und Geschäftsmodelle sehen und einige sehr unerwünschte Effekte auf Opensource-Software werden entstehen. Die Einführung einer großen Zahl neuer Monopolrechte in der Informationsgesellschaft wird sicherlich nicht hilfreich sein für das strategische Ziel der EU, %(q:der konkurrenzfähigste und qualifizierteste Wirtschaftsraum der Welt) zu werden.

#tmp: Polnische Unternehmen besitzen faktisch keine Software-Patente und Polen hat eine Tradition, die Erteilung solcher Patente abzulehnen.  Die McCarthy-Direktive würde einen radikalen Wandel für Polen bringen.  In Recital 16 wird klar und deutlich gesagt, daß dieser Vorschlag als Maßnahme zum Schutze der entwickelten Wirtschaft gegenüber den Wettbewerb von %(q:low-cost Wirtschaften) gesehen werden soll.  Während ich nicht glaube, daß ein Schutz gegen legitimen Wettbewerb in der Software-Industrie zum Vorteil irgenteines Landes arbeitet, wird sich sicher den osteuropäischen Ländern mehr schaden, als den westeuropäischen, ganz einfach weil es einen großen Aufwand an rechtlichen Kosten verursacht, die wir weniger aufzubringen in der Lage sind.

#lWW: Now it is up to the European Parliament to decide on a highly controversial proposal for a directive for software patents. The JURI proposal aims at improving clarity. It should have at last defined clearly what is patentable and what not. But in reality it is a bunch of magic formulas that even legal experts do not understand. In particular for small and medium sized software developers it is a disaster. A patent infringement claim can ruin such a company.  It is to be hoped that the European Parliament understands this if they vote about the proposed directive in a few days.

#rsl: Dr. Reinier Bakels, %(ss:researcher at Institute for Informatic Law) of Amsterdam University and co-author of a %(ss:study) about the proposed directive that was commissioned by the European Parliamentin 2002, comments on the JURI decision:

#tgr: Diese Studie, welche wir für die Europäische Kommission angefertigt haben, zeigt auf, daß es keine Notwendigkeit für eine Ausweitung der Patentierbarkeit von Software gibt und daß Nachweis des Ökonomischen Effekts von Software-Patenten generell nicht überzeugend war,  und - wenn überhaupt - denke ich daß die Balance oder Forschung eher auf der negativen Seite war. Jedoch brauchten wir einen Kompromiß bei diesen Formalitäten, denn es war eine Kommittee-Entscheidung, und meine Co-Autoren waren Patentanrechts-Experten mit einer starken Grundhaltung zur Nützlichkeit von Patenten für Firmen, die ihr Kapital erhöhen wollen. Meine Co-Autoren waren der Meinung, daß es schwierig sei, Software-Patenten mit technischem Effekt die Patentierbarkeit  zu verwehren, allerdings stimmten sie zu, daß nichts unternommen werden sollte,  um die Verbreitung von schlechten Software-Patenten wie in den USA zu fördern. Ich denke, selbst diese Kompromiß-Haltung dieser Studie war auf viele Weise bereits ein Fortschritt: zum Zeitpunkt, als die Studie angefertigt wurde,  sah es so aus, als gäbe es in der Kommission die nicht hinterfragte Ansicht, daß wir das US-Model kopieren sollten. Hätten wir die Studie heute schreiben sollen, so hätten wir durchaus deutlich weiter gehen können. Die aktuelle Bessen & Hunt Studie scheint wesentlich stärkere Anzeichen zu zeigen, daß Software-Patente in den USA die Software-Innovation erstickt haben. Heute sollte die Beweislast auf all jenen liegen, die irgenteine Gesetzgebung zur Ausweitung von Patenten im Software-Bereich propagieren.

#ecf: Die Mitglieder des JPEG-Kommitees haben Jahre damit verbracht, gegen frivole Patentansprüche zu kämpfen, die es beinahe erreicht haben, daß unsere Standardisierungsbestrebungen zunichte gemacht worden wären. Mit der neuen Regelung wie von JURI vorgeschlagen, bekommen wir noch deutlich mehr Probleme. Das Europäische Patentamt (EPA) hat nicht mehr Gnade gegenüber der Software-Industrie gezeigt, als das US-Patentampt (USPTO), aber deren Patente wurden in Europa bisher als ungültig betrachtet. Nun, nach Arlene McCarthy's Plänen, werden wir nicht nur gezwungen, regelmäßig Prozeßen wegen breiten und trivialen Software-Patenten entsprechend den EPO/USTPO-Standards entgegenzutreten. Wir werden ebenso mit Sicherheit wissen, daß es keine Ausnahmen zum Zwecke der Interoperabilität geben gibt. Während es in den USA die Hoffnung gibt, daß die Gerichte dem steigenden Druck der öffentlichen Meinung nachgeben und ein Stück weit reformieren werden, bereitet die JURI-Kommission vor, die aktuelle Mißwirtschaft der Patentämter festzuschreiben. Das würde den US-Landbesetzern erlauben, weiterhin die Europäische IT-Landschaft auszutrocknen, während die USA bereits auf dem Weg sind, sich von der Katastrophe zu erholen.

#oem: Arlene McCarthy und ihre Anhänger wirken entschlossen, die Interoperabilität vernichten und die große US-Monopole stärken zu wollen. Im Word Wide Web Consortium (W3C) mußten wir bereits seit Jahren mit Fragen der Patent-Politik käpfen. Die Industrie-Kommission schlug vor, Software von der Einklagbarkeit von Patenten zum Zwecke der Interoperabilität auszunehmen. Dies sollte sich als sehr hilfreich bei der Standardisierung erweisen. Außerdem würde dies einige der schlimmsten Wettbewerbsfeindlichen Praktiken verhindern.

#imy: Håkon Wium Lie, Chefentwickler der %(op:Opera Inc) Bemerkungen zu %(vl:Arlene McCarthy's voting list):

#kGA: Führende Mitglieder des JURI-Kommitees sind nicht zufrieden damit, daß die Software-Industrie neuen Risiken ausgesetzt wird.  Sie wollen ebenso klarstellen, daß jeder Programmierer mit Patenten in Konflikt geraten wird, sobald er sein Programm im Web veröffentlicht.  Währenddessen CULT und ITRE Schutzmaßnahmen für die Publikationsfreiheit einführten, ignoriert Arlene McCarthy diese Sicherungsmaßnahmen einfach und empfiehlt stattdessen eine %(q:Kompromiß-Nachbesserung 1), welche eine jede Publikation zu einem direkten Verstoß macht.  Es scheint fast unternehmerisch unverantwortbar, unter diesen Bedingungen weiter Linux-Distributionen zu vertreiben.  Wenn einer von tausenden Programmiererin in unserer Distribution eines von zehntausenden breiten und trivialen vom EPA erteilten Software-Patenten verletzt, verklagt und gewzungen werden, unsere CDs vom Markt zu nehmen.  Solche Fälle hatten wir bereits mit Warenzeichen, jedoch ist die rechtliche Unsicherheit um Patente erheblich größer. Fälle wie SCO und EBay zeigen, daß die Gefahr keineswegs theoretisch ist.  In diesem Moment, wo öffentliche Stellen freie oder opensource-Betriebssysteme einführen oder benutzen, um auf Microsoft Druck zur Preisreduktion ausüben, Arlene McCarthy and her allies seem to be joining Microsoft in its crusade to suppress free software.  Die Beyrische CSU hier ein Instrument zu sein und hat zu selben Zeit %(cs:Microsoft's Sache) in Beyern gefördert.  Arlene McCarthy selbst hat ganz %(gm:offen die GNU General Public License angegriffen), als %(q:nur eine andere Form von Monopolismus).  McCarthy und ihr Verbündeter Dr. Joachim Wuermeling have haben jegliche Einladungen zum Dialog von unserer Seite verworfen und wehement jegliche Antwort auf unsere Fragen verweigert.  Sie sind selbstverstänlich nicht dazu verpflichtet, mit uns zu reden.  Wie dem auch sei, Monopole zu stärken, indem man Wettbewerbern Handschellen anlegt, ist eines der schlimmsten Dinge, die eine Regierung gegen sich selbst tun kann.

#mhW: Viele Leute haben die EU-Kommission und die Unterstützer des erweiterten Direktivenvorschlags um Klarstellung gebeten in Bezug auf Beispielpatente (Amazon Ein-Klick etwa). Soweit ich weiß gab es keine Antwort auf ihre Fragen.

#lue: Demzufolge sollte es klar sein, daß eine Direktive, die die derzeitige Praxis des EPO harmonisiert, die %(e:meisten) neuen (computerimplementierten) Geschäftsmodelle zulassen würde, einschließlich Amazon Ein-Klick.

#Wit: Diese Aussage %(or:stammt von) der geachteten deutschen IP Anwaltskanzlei %(q:Wuesthoff & Wuesthoff).

#tcs: Nehmen wir als gegeben an, daß der erweiterte Direktivenvorschlag die derzeitige Praxis des Europäischen Patentamtes (EPO) harmonisieren will. Betrachten wir die folgenden Aussagen von IP Spezialisten von 2001:

#una: Im letzten Jahr %(sp:lernte ich viel) über den Direktivenvorschlag und ich kenne die Zusätze, die JURI heute verabschiedete. Ich denke nicht, daß Patente auf Geschäftsmehoden wie etwa Amazons Ein-Klick in Europa vermieden werden können, wenn der erweiterte Direktivenvorschlag im Parlament verabschiedet wird.

#mto: Stefan Pollmeier ist der Geschäftsführer eines Elektronik-Unternehmens mit 45 Mitarbeitern im Raum Frankfurt, Deutschland.  80% ihrer Forschung und Entwicklung ist Software.

#isf: Allen öffentlichen Beteuerungen einiger ihrer Fürsprecher zum Trotz macht die Richtlinie Algorithmen and Geschäftsmethoden patentierbar.  Wie könnte es anders sein?  Was sonst könnte in unserer Software patentiert werden, wenn nicht Algorithmen oder Geschäftsmethoden?

#ead: Unsere Patentanwälte haben unterdessen einige Mitglieder unseres Vorstands überzeugt, dass die geplante Richtlinie gut sei, da sie uns erlaubt, das Patentspiel in Europa in gleicher Weise wie in den USA zu spielen.  Ich halte das für einen Irrtum und glaube nicht, dass sich diese Sichtweise in unserem Unternehmen durchsetzen wird.

#omi: SAP hatte viel unter US-Patenten zu leiden, die nach dieser Richtlinie wahrscheinlich als %(q:computerimplemetierte Erfindungen mit einem technischen Beitrag in ihrem erfinderischen Schritt) gelten. SAP wurde durch das Urheberrecht groß, und nachgeahmt zu werden empfanden wir nie als ein nennenswerte Problem.  Wir brauchen keinen Schutz durch Patente, sondern Schutz vor Patenten.  SAP wurde genötigt, exorbitant hohe Beträge an einige individuelle Patentinhaber mit hoher krimineller Energie (darunter einige Professoren von bekannten US-Universitäten) zu zahlen.  Softwarepatente legalisieren und fördern kriminelles und unmoralisches Verhalten in der Softwarebranche. Es gibt wahrscheinlich so gut wie keine nicht-trivialen Softwarepatente. Daher besteht der einzige Zweck von Softwarepatenten darin, als Waffe im Wettrüsten zu dienen (%(q:Ihr habt unsere 100 Patente [ zugegeben trivial und bedeutungslos -- was wir nie öffentlich sagen würden ] verletzt, wir haben eure 100 Patente [ ebenso trivial und bedeutungslos - was ihr nie öffentlich sagen würdet ] verletzt, also laßt uns den Streit begragben und sicherstellen, daß uns beide kein anderer stört)).  Dieses Wettrüsten verursacht hohe Kosten für alle Softwarehersteller.  SAP mußte 1998 eine Patentabteilung aufbauen, um sich verteidigen zu können, und zweigt nun regelmäßig Mittel für diese Abteilung ab, die sonst für Forschung und Entwicklung zur Verfügung gestanden hätten. Aber nicht einmal eine große Anzahl an Patenten hilft uns gegen Individuen mit hoher krimineller Energie.

#rna: Prof. Brian Kahin, University of Michigan, Ann Arbor, USA

#lEg: Dr. Jean-Paul Smets-Solanes, CEO of Nexedi, Lille, France

#ePW: Arlene McCarthy MEP: We are not experts

#pct: Hans Appel, CTO von Sun Microsystems für Nordeuropa

#ran: Dr. Karl-Friedrich Lenz, Professor für Deutsches und Europäisches Recht, Aoyama Gakuin Universität, Tokyo:

#bma: Jozef Halbersztadt, Patentgutachter am Polnischen Patentamt

#eop: Reinier Bakels, author of parliament-ordered study on software patent directive

#jai: Peter Holmes, Ökonom, Mitwirkender an einer von der EU-Kommission beauftragten Studie, Universität zu Sussex

#jir: Richard Clark, Geschäftsführer der Elysium Co Ltd und Chefeditor für das JPEG Sttandardisations-Kommittee

#njp: Håkon Wium Lie, Chefentwickler der Opera Inc.

#hdE: Bernhard Kaindl, Entwickler bei der SuSE Linux AG

#Wee: Stefan Pollmeier, Geschäftsführer der ESR Pollmeier GmbH, Deutschland

#WSf: Dr. Bernhard Runge, Entwickler bei SAP AG und früherer Professor für Mathematik

#vav: If the server is down, please try the %(xv:version on Xavi's home page).

#teM: JURI-Bericht über das Abstimmungsergebnis, aus einer MSWord-Datei nach PDF konvertiert.

#Wtt: This page contains links to (mis)information about the proposed directive, from the perspective of the proponents, including %(q:forecasts) and the text adopted by JURI.

#WoW: Title page leading to the text as adopted by JURI

#kni: JURI-amended and voted directive

#cam: Explains among others: that %(q:susceptible of industrial application) is not a restriction at all, because it only means %(q:commercially valuable); that program claims, as proposed by JURI, are welcome from the European Commission's point of view.

#WWq: Angelika Niebler, DE conservative MEP (christian social union from Bavaria, close associate of EPP rapporteur Wuermeling who was absent), praises CEC proposal, McCarthy report and JURI %(q:compromise amendments) as %(q:great work).  Contradicts Rothley's negative comments.

#eWW: Malcolm Harbour, UK conservative MEP, celebrates CEC Software Patent Directive Proposal and McCarthy report as a great achievement in limiting patentability, explains that program claims are needed, because mobile phones are patentable and the functioning principles of a mobile phone might well be embodied in a program that is stored on a medium and sold in that form.  Answers Berenguer on some points.

#rwt: Luis Berenguer MEP (PSE, Spain, law professor), warns of dangers of software patents, says that directive does not achieve what its proponents claim to be aiming for, recommends amendment 56.  This speech was the only one that criticised the McCarthy proposal at all, albeit not very strongly.

#Wai: Overview of transcripts of speeches given in JURI one day before their vote on the software patentability directive.  The only critical speech was delivered by Luis Berenguer (PSE, Spain)

#bWW: In her speech to JURI one day before the vote, Arlene McCarthy MEP explains why in her view there has been sufficient discussion and it is time to vote.  Thanks to Erik for the transscription from sound recording.

#ltW: explains in detail how all viewpoints except for those of the patent lobby were systematically disregarded in JURI ever since Arlene McCarthy became the rapporteur in May 2002.

#ilI: Explains what JURI wanted and what they decided, what they rejected and what they approved, and what consequences this would have if approved by the Parliament.

#pls2: Bitte helfen Sie mit, weitere detailierte Notiizen aus der Tonaufnahme herauszuarbeiten!

#pls: Bitte helfen Sie mit, weitere detailierte Notiizen aus der Tonaufnahme herauszuarbeiten!

#Wyj: Der deutsche sozialdemokratische MEP Willi Rothley drückt seine Frustration über den Entwurf der Europäischen Kommission aus, indem er sagt, unsauber und unnötig.  Die Patentgerichte finden von selbst die richtige Balance und der Gesetzgeber kann kaum irgentwelche besseren Regeln aufstellen, als jene welche die Gerichte ausarbeiten.  Wie dem auch sei, der gesetzgebende Apparat ist wie eine Wurstmaschiene, die am Arbeiten gehalten werden muß.  Von Zeit zu Zeit muß sie einfach ein paar neue Gesetze ausspucken, ganz gleich ob diese gebraucht werden oder nicht, und wir, die MEPs sind eingespannt in dieser Maschine und gezwungen, irgentwie das beste daraus zu machen.  Rothley's Äußerung errang ein gutes Stück Aufmerksamkeit im JURI, wie das Gelächter zeigt.

#sns: english translation of Rothley's speech

#eWe: Willi Rothley MEP (DE, PSE) says that the directive as proposed by the European Commission is poorly crafted and unnecessary and that it should really be sent back to the European Commission.  But JURI keeps rotatint like a sausage machine, turning out one law after another, and we have to make the best out of it.

#tmW: hören Sie Rothley's zweiten Kommentar am Montag, den 16: JURI ist eine Würstmaschine

#cok: Bericht und Forums-Diskussion

#xrW: Pro-Linux 2003/06/17: Softwarepatente rücken ein Stück näher

#ses: Die Liberalen nahmen keine eindeutige Position ein und unterstützten sowohl die meisten Vorschläge der Industriekommission (gegen Softwarepatente) als auch McCarthys gegenteilige Empfehlungen, mit dem Ergebnis, daß die ITRE-Vorschläge als abgelehnt angesehenn wurden und nicht einmal mehr zur Abstimmung kamen. Dadurch wurden Arlene McCarthys Vorschläge, die Veröffentlichung von Software zu einem direkten Patentverstoß zu machen, mit der Unterstützung von Sozialisten, Konservativen und Liberalen angenommen, während der ITRE-Vorschlag, Publikationsfreiheit zu garantieren, von der Agenda entfernt wurde.

#nWW2: In ihrer Abstimmungsliste hat Arlene McCarthy empfohlen, gegen alle Zusätze zu stimmen, die die Patentierbarkeit oder die Durchsetzbarkeit von Patenten in irgendeiner Weise begrenzen, und für die meisten, die das Gegenteil vorschlagen oder grundsätzlich für Softwarepatente sind. Ungefähr zwei Drittel der JURI-Mitglieder folgten McCarthys Abstimmungsliste. Dagegen waren die Grüne MEP Mercedes Echerer (Österreich) und Professor Neil MacCormick (Großbritannien), die MEP der Linken Ilka Schröder (Deutschland) und Pernille Frahm (Dänemark), die Sozialistin Evelyn Gebhardt (Deutschland), die Konservative Piia-Noora Kauppi (Finnland), der Unabhängige Marco Cappato (Italien) und ein weiteres Mitglied, das wir zur Zeit nicht benennen können. Darüberhinaus folgte eine Mehrheit bei einigen Punkten McCarthys Empfehlung nicht und stimmte für den gesunden Menschenverstand. Darunter ist der wichtigste Punkt ITRE-15, der die Nutzung eines patentierten Vorgangs zum Zwecke der Interoperabilität generell erlaubt.

#gio: Gesetzesentwurf

#are: Schlußabstimmung des Direktivenprojekts

#bei: Stimmenthaltung

#cnr: Dagegen

#pro: Dafür

#dokurl: Permanent URL of this Press Release

#ffii: About the FFII

#eurolinux: About the Eurolinux Alliance

#media: Media Contacts

#ten: Volltext der Aussagen von Industrie und Akademikern 

#tnJ: Was passierte im JURI?

#title: JURI stimmt für Scheinbegrenzung der Patentierbarkeit

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: eweigelt ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnjuri030617 ;
# txtlang: de ;
# multlin: t ;
# End: ;

