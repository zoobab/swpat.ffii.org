\contentsline {section}{\numberline {1}Einf\"{u}hrung}{3}{section.1}
\contentsline {section}{\numberline {2}Was passierte im JURI?}{4}{section.2}
\contentsline {section}{\numberline {3}Volltext der Aussagen von Industrie und Akademikern }{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Dr. Bernhard Runge, Entwickler bei SAP AG und fr\"{u}herer Professor f\"{u}r Mathematik}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Stefan Pollmeier, Gesch\"{a}ftsf\"{u}hrer der ESR Pollmeier GmbH, Deutschland}{11}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Bernhard Kaindl, Entwickler bei der SuSE Linux AG}{11}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}H\r a{}kon Wium Lie, Chefentwickler der Opera Inc.}{12}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Richard Clark, Gesch\"{a}ftsf\"{u}hrer der Elysium Co Ltd und Chefeditor f\"{u}r das JPEG Sttandardisations-Kommittee}{13}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Peter Holmes, \"{O}konom, Mitwirkender an einer von der EU-Kommission beauftragten Studie, Universit\"{a}t zu Sussex}{13}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Reinier Bakels, author of parliament-ordered study on software patent directive}{14}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Jozef Halbersztadt, Patentgutachter am Polnischen Patentamt}{14}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Dr. Karl-Friedrich Lenz, Professor f\"{u}r Deutsches und Europ\"{a}isches Recht, Aoyama Gakuin Universit\"{a}t, Tokyo:}{14}{subsection.3.9}
\contentsline {subsection}{\numberline {3.10}Hans Appel, CTO von Sun Microsystems f\"{u}r Nordeuropa}{15}{subsection.3.10}
\contentsline {subsection}{\numberline {3.11}Arlene McCarthy MEP: We are not experts}{15}{subsection.3.11}
\contentsline {subsection}{\numberline {3.12}Dr. Jean-Paul Smets-Solanes, CEO of Nexedi, Lille, France}{16}{subsection.3.12}
\contentsline {subsection}{\numberline {3.13}Prof. Brian Kahin, University of Michigan, Ann Arbor, USA}{17}{subsection.3.13}
\contentsline {section}{\numberline {4}Media Contacts}{18}{section.4}
\contentsline {section}{\numberline {5}Permanent URL of this Press Release}{18}{section.5}
\contentsline {section}{\numberline {6}Kommentierte Verweise}{18}{section.6}
