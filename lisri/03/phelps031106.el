Subject: [Fwd: Interview de Marshall Phelps nouveau responsable de la PI chez
 Microsoft]

Philippe Aigrain post this very interesting news on a french list. As 
Philppe said [translation from french] : � An essential reading to 
include/understand what Microsoft intends to do with its 3000 software 
patents and 5114 software patent applications. It also informs their 
strategy of lobbying on the software patentability in Europe �
-- 
     ~     Gerald Sedrati-Dinet
    'v'
   // \\   mailto:sedrati@bigfoot.com
  /(   )\  http://gibuskro.lautre.net/
   ^`~'^

http://www.legalmediagroup.com/mip/includes/print.asp?SID=2175

Une lecture indispensable pour comprendre ce que Microsoft compte faire 
de ses 3000 brevets et 5114 demandes de brevets.

On y trouve �galement ceci qui �claire leur strat�gie de lobbying sur la 
brevetabilit� logicielle en Europe dans la p�riode � venir:

**

This international focus is especially important at a time when many 
national and supra-national bodies are debating the shape of their IP 
regimes. In Europe, for instance, the past few months have been 
dominated by the controversy surrounding the future of a European 
software patent directive. Following September's vote in the European 
Parliament, the EU's proposals for the patent are falling some way short 
of what many corporations would like to see, allowing for patents on 
inventions that affect the way hardware works, but not for software 
itself. "We'll focus on Europe that bit more," says Phelps. "Again, I 
think we've been way too US-centric, and I don't think we joined the 
debate [on software patents in Europe] in the right way."

Phelps believes that Microsoft's recent troubles with the European 
Commission - where it is being investigated for abuse of dominance - 
have left the company gun-shy. "Sometimes you don't want to leap out of 
your foxhole, which is natural. But in these debates about what is the 
proper way to go with IP - such as should software be independently 
patentable or do you always want to have a hardware manifestation - 
these are really good overarching questions that ought to be debated in 
a thoughtful manner, with the overall question of are we being 
competitive with our geography being held in mind, or are we marching to 
somebody's narrow agenda?" he says. "My concern with Europe is that what 
they're going to do, writ large, is develop an industrial policy for 
this kind of thing in Europe. The US isn't following that. The US is off 
to the races on software patents and that's not going to be turned 
around anytime soon, if ever, and so is Japan. And why Europe would want 
to be uncompetitive with those two areas of the world, I really don't know."

[...]

*Software patenting
*"I think there's a fundamental lack of understanding [in Europe] that 
there isn't a real difference between software and hardware. I also 
think there are large political forces driving this issue. For some 
reason, it has caught the attention of the Greens, and I'm not clear 
why. But that's true of this country too, where [consumer activist and 
Green Party presidential candidate in 2000] Ralph Nader is involved with 
the Open Source movement. Some on either side approach these issues with 
an almost religious fervour. My own view is that we need to get a little 
more sophisticated. Linux isn't going to go away and Microsoft isn't 
going to go away. IBM isn't going to go away, and IBM has got one foot 
in each camp. So I think we need to enter into a new world with a little 
more sophistication than this black and white world that some people 
seem to see out there, which I frankly don't see.

Also, I can give you all kinds of software implementations that used to 
be in hardware. Apart from the plug in the wall, anything you used to do 
in hardware you can do in software. Just think about the media player 
that exits when you boot up your software on your machine. It looks just 
like any other media player - it's got fast forward and stop and pause 
and all that stuff. That's software. Is there a hardware version of 
that? Sure, any DVD player you want to go and buy. Or if you look at 
some laptops, there's a little button in the middle which is basically 
the mouse. You think that moves, but it doesn't. It's really a strain 
gauge that calculates where you want the cursor to go by how hard you 
push it and in what direction. How does it do that? Well, there's an 
algorithm for an x-axis and an algorithm for the y-axis - that's 
software. Is there a hardware implementation for that? Sure, every mouse 
that's practically ever been built is a hardware implementation of that.

I think people react to software as if it's an animal from another 
planet, and I don't think that, at the end of the day, the distinction 
[between software and hardware] is a very useful one.

Philippe

-----------

From: Tim Jackson <lists@timj.co.uk>

Some people may have already seen this but it's an interesting article
about a new "IP" leader (Marshall Phelps) at MS - the implication is that
they may be getting a whole load more aggressive on software patents
(application, licensing and enforcement).

It also has a number of references to the situation in Europe and talks
about lobbying the EC.

Many of the simplistic arguments in favour of unlimited patentability are
wheeled out alongside the usual dismissal of anyone who opposes the
patentability of pure software as "religious". His narrow viewpoint of the
patent system also appears to largely exclude/ignore from consideration
anything other than large corporates.

http://www.legalmediagroup.com/mip/includes/print.asp?SID=2175


Some choice quotes:

"There is a pretty broad understanding at Microsoft on the development
side of the equation that you need to do [licensing] properly...the good
news for Microsoft is that most of its patents are in one specific area -
software."


The 'software patents allow poor little SMEs to challenge Goliath'
argument:

"It may well be that as a small company, if I have some really good IP, I
actually have quite a bargaining chip against a big company because the
leverage is on my side; I have IP that a Microsoft or an IBM really needs,
if I choose to use it."


"Most of R&D is D - development - by a factor of 90 to 10"


"We'll focus on Europe that bit more...I don't think we joined the debate
[on software patents in Europe] in the right way."


"The US is off to the races on software patents and that's not going to be
turned around anytime soon, if ever, and so is Japan. And why Europe would
want to be uncompetitive with those two areas of the world, I really don't
know."


Pot calling kettle black about lack of understanding:

"I think there's a fundamental lack of understanding [in Europe] that
there isn't a real difference between software and hardware"


"I don't think that, at the end of the day, the distinction [between
software and hardware] is a very useful one"

"I'm broadly in favour of anything that helps the USPTO"
