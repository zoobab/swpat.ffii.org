<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: CEOs of big telcos sign letter against Europarl Amendments

#descr: The chief executive officers of Alcatel, Ericsson, Nokia and Siemens
have signed a letter to the European Commission and the European
Council which complains about the European Parliament's amendments to
the proposed software patent directive, saying that these will
effectively remove the value of most of the patents of their companies
and thereby harm the competitiveness of Europe's industry and violate
the TRIPs treaty.  FFII points out that the Directive indeed threatens
the interests of the patent departments of such companies, but not of
the companies themselves: The letter is characterised by untruthful
dogmatic assertions which say much about the thinking of patent
departments and little about the interests of their companies, many of
whose employees, especially software developers, support the positions
of FFII.

#tsk: Minister Rocco Buttiglione, Competitiveness Council Commissioners
Frederik Bolkestein and Erkki Liikanen, European Commission

#Was: Dear Minister and Commissioners,

#ryn: Proposed Directive on the Patentability of Computer-Implemented
Inventions (CIIs)

#ire: As chief executive officers of five of Europe's leading
telecommunications and consumer electronics companies, we write to
express our deepest concerns about the amendments recently adopted by
the European Parliament to the Commission's proposed Directive on
Patentability of Computer-Implemented Inventions (CIIs), commonly
known as the Software Patents Directive.

#rex: It may be worth noting that the abbreviation %(q:CII) is not used in
the original directive title.  The corporate patent lawyer who wrote
this (Tim Frain from Nokia?) seems very eager to use every appropriate
and inappropriate opportunity to launch %(q:CII) as an abbreviation
for replacing the colloquial term %(q:software patents).  It is clear
that this is a patent lawyer wording, not a wording of CEOs of
telecommunication companies.  More on the campaign around the term
%(q:computer-implemented inventions), which was started at the EPO in
May 2000, is found in the documentation %(DOK).

#iee: Collectively, our companies invest Euros 15 billion per annum in R&D. 
In some fields as much as 90% of these investments are in CIIs as
defined in the directive proposal.

#tuw: The R&D investements of the undersingned companies are concentrated on
the development and deployment of telecommunication machinery and
solutions.  About %(am:5-10% of this money) is spent on patent
acuisition.  The development of new computing ideas, as expressed in
the patents owned by these companies, accounts for far less than that,
probably below 1% of their expenses.  Patents are moreover only one of
several forms of formal and informal ways in which these companies
protect their investments.

#hlo: Arlene McCarthy's figures which were apparently supplied by the
undersigned companies themselves.

#tpi: Details about the patent portfolio and typical software patents of the
companies in question can be found at

#dot: Having a stable and reliable patent system in place is vital to
protect R&D investment and to encourage future innovation in Europe.

#Wem: This may be the case for the pharma industry.  As far as the subject
matter of the directive is concerned, it contradicts all common
knowledge and evidence gathered from %(es:economic studies) so far.

#WWt: The original aim of the directive was (1) to harmonise the law in
Europe, and (2) stop the drift towards the US approach of patenting
pure business methods and non-technical software, while (3)
safeguarding interoperability.  We firmly support all these aims.

#epn: The European Commission indeed claimed that these were the aims of its
directive proposal, but upon a closer look, the proposal achieved none
of these aims.  Instead it achieved other unnamed aims, such as
unlimited patentability of %(q:computer-implemented business methods)
and absolute priority of such patents over any interoperability
considerations.  The patent departments of the companies undersigned
under this letter were major supporters of these real aims.  One
should judge the tree by its fruits.

#nue: The directive was also intended to maintain and codify the status quo
in Europe based on current best practice, which has enabled open
source software business models to flourish alongside patents, while
also protecting the interests of small software developers and SMEs
generally.  We firmly support these aims as well.

#eWt: By %(q:status quo in Europe based on current best practise), the
bosses are referring to a %(ep:practise of the European Patent Office)
which is not significantly different from that of the USPTO.  If there
was anything that protected small businesses in Europe, then it was
the legal uncertainty which results from the fact that this %(q:best
practise) is at odds with the %(wl:written law).

#ahr: As a %(cp:report by the %(tp|French State Planning
Commission|Commissariat du Plan) of 2002) says:

#cWg: Only the armistice which is currently prevailing, precisely due to the
legal uncertainty around the notion of software patent, explains in
effect that the existing patents are not more frequently used.

#pto: The stated aim of the directive proposal advocated by the European
Commission and the undersigned CEOs was precisely to remove this
%(q:legal uncertainty) which has been %(q:protecting the interests of
small software developers and SMEs generally).

#lWc: However, the vote in Parliament on 24 September 2003 has completely
turned the Commission's original proposal around, removing effective
patent protection for much ­ and in the case of telecommunications and
consumer electronics, probably most - of our R&D investment.   This
would have devastating consequences for our companies.  It would be
open for all-comers to exploit the results of our expensive R&D
programmes at no cost, and even without any R&D overheads of their
own.  This is contrary to what was stated at the Lisbon European
Council, namely that %(q:innovation and ideas must be adequately
rewarded within the new knowledge-based economy, particularly through
patent protection).

#org: This bold statement contradicts all common knowledge about the
economics of software development in the telecom field as in other
fields.

#eta: Anyone who wants to compete with one of the undersigned companies
would have to incur huge costs.  Imitating and reimplementing software
itself is an extremely costly process, especially when, as is
regularly the case, the software is available only in binary form and
copyright has to be observed.

#aen: The loss of effective patent protection would put our companies at a
competitive disadvantage in the short term, and in the longer term
reduce the incentive for further investment in R&D in Europe.  Overall
there will be less software-related innovation in Europe, and
ultimately Europe is unlikely to meet the Lisbon goal of becoming the
%(q:most competitive and dynamic, knowledge-based economy in the
world).

#tso: This unreasoned assertion contradicts the common sense of software
developpers, including about more than 500 employees of the
undersigned companies who have signed the %(ep:NoEPatents Petition).
From conversations with managers at some of the undersigned companies
(and from testimonies such as the one of Robert Barr from Cisco, see
above) we know that patents are widely perceived to be a brake on
innovation even at big telcomunication and electronics companies.

#WlW: Parliament's amendments suggest that Europe is ready to fly in the
face of international obligations under the TRIPS Agreement.

#awl: This again is a well-documented lie.

#ulo: Furthermore they would change the legal climate in Europe so suddenly,
dramatically and unexpectedly, that they already send a message to the
rest of the world that one of the legal cornerstones necessary for
attaining a viable European Information Society is unstable,
unpredictable, and unreliable.

#nWW: While the patent system may be a corner stone of a viable pharma
industry, no serious economic study has ever claimed that it is
beneficial, let alone indispensable, for the information society. 
Rather, the information society needs a stable, predictable and
reliable reassurance that everyone is free to create and publish his
own copyrighted works and to interoperate with other systems. The
Parliament's amendments provide precisely this reassurance.

#ina: We therefore believe the Council and the Commission need to take
appropriate measures affirmatively to redress the current situation by
sending a strong counter-signal that the law in Europe will not be
changed suddenly and dramatically, and confirming that Europe will not
ignore or flout international obligations.

#neu: In other words the bosses of 5 large companies are calling on
administration officials to take legislative decisions against the
European Patent Convention and against the EU's only democratically
elected legislative body.

#rot: The Directive as amended by Parliament will deal a severe, perhaps
fatal, blow to our shared aspirations declared at the Lisbon Summit
for Europe to become the most competitive knowledge based economy in
the world.

#Wel: The current legal framework for CIIs in Europe is serving all
stakeholders well.  We do not want to see any sudden or dramatic
reduction in the scope of what is patentable.  If a solution cannot be
found which codifies the status quo, it would be better to have no
directive at all than a directive which does untold damage to European
industry.

#iel: Sincerely,

#tWe: These concluding remarks show that the support of the signators for
the professed goals of %(q:harmonisation) and %(q:clarification) are
indeed of minor importance.

#hhp: The CEOs of the five companies have allowed themselves to be abused by
their patent departments.  These patent departments have been
producing thousands of broad and trivial patents on methods of
organisation and calculation every year, and they want a directive
that makes these patents enforcable.  If they do not get such a
directive, they prefer to have an unclear situation (where their
patents can at least continue to be collected for corporate accounting
and %(ct:tax evasion)).

#sgl: Some of the undersigned CEOs like to present themselves as leaders
with a scientific mind and and a sense of social responsibility. 
Quoting this letter may %(q:deal a severe, perhaps fatal, blow) to
that aspiration.

#ego: Reuters echoing the telcos FUD

#riw: An article by Reuters News Agency which reports about the letter of
the 5 CEOs and adds some comments by a leading employee of Ericsson
who is worried that since the use of computers is progressing hand
making %(q:inventing) very easy, what formerly was a patentable
invention no longer be so in the future.  But without patent
protection, Ericsson would go broke and therefore Ericsson would have
to move out of Sweden.  Note that Ericsson is already under obligation
to move out of Sweden, because swedish voters didn't heed their CEO's
threats when voting to stay out of the euro zone for another few
years.

#eca: PDF version derived from the original msword document which contained
traces hinting to (someone at) Nokia as the author.

#Wds: The Eicta press release was largely copy&pasted from this %(q:CEO
letter).  It was explained to the press by Tim Frain, patent chief of
Nokia.  Its contents also reflect Frain's personal style.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: telcos031107 ;
# txtlang: en ;
# multlin: t ;
# End: ;

