                                                     NOKIA                                                    
                                                                                                              
                                                          
                                                          
                                                          
Minister Rocco Buttiglione, Competitiveness Council 
Commissioners Frederik Bolkestein and Erkki Liikanen, European Commission 
                                                                                                                   
                                                                                           November 07, 2003 
 
Dear Minister and Commissioners,  
 
Re:  Proposed Directive on the Patentability of Computer-Implemented Inventions (CIIs) 
 
As chief executive officers of five of Europe's leading telecommunications and consumer electronics 
companies, we write to express our deepest concerns about the amendments recently adopted by the 
European Parliament to the Commission's proposed Directive on Patentability of Computer-Implemented 
Inventions (CIIs), commonly known as the Software Patents Directive.  
 
Collectively, our companies invest Euros 15 billion per annum in R&D.  In some fields as much as 90% of 
these investments are in CIIs as defined in the directive proposal. 
 
Having a stable and reliable patent system in place is vital to protect R&D investment and to encourage 
future innovation in Europe. 
 
The original aim of the directive was (1) to harmonise the law in Europe, and (2) stop the drift towards the 
US approach of patenting pure business methods and non-technical software, while (3) safeguarding 
interoperability.  We firmly support all these aims. 
 
The directive was also intended to maintain and codify the status quo in Europe based on current best 
practice, which has enabled open source software business models to flourish alongside patents, while also 
protecting the interests of small software developers and SMEs generally.  We firmly support these aims as 
well.   
 
However, the vote in Parliament on 24 September 2003 has completely turned the Commission's original 
proposal around, removing effective patent protection for much � and in the case of telecommunications and 
consumer electronics, probably most - of our R&D investment.   This would have devastating consequences 
for our companies.  It would be open for all-comers to exploit the results of our expensive R&D programmes 
at no cost, and even without any R&D overheads of their own.  This is contrary to what was stated at the 
Lisbon European Council, namely that  "innovation and ideas must be adequately rewarded within the new 
knowledge-based economy, particularly through patent protection".   
 
The loss of effective patent protection would put our companies at a competitive disadvantage in the short 
term, and in the longer term reduce the incentive for further investment in R&D in Europe.  Overall there will 
be less software-related innovation in Europe, and ultimately Europe is unlikely to meet the Lisbon goal of 
becoming the "most competitive and dynamic, knowledge-based economy in the world". 
  
Parliament's amendments suggest that Europe is ready to fly in the face of international obligations under the 
TRIPS Agreement.  Furthermore they would change the legal climate in Europe so suddenly, dramatically  


 
and unexpectedly, that they already send a message to the rest of the world that one of the legal cornerstones 
necessary for attaining a viable European Information Society is unstable, unpredictable, and unreliable. We 
therefore believe the Council and the Commission need to take appropriate measures affirmatively to redress 
the current situation by sending a strong counter-signal that the law in Europe will not be changed suddenly 
and dramatically, and confirming that Europe will not ignore or flout international obligations. 
 
The Directive as amended by Parliament will deal a severe, perhaps fatal, blow to our shared aspirations 
declared at the Lisbon Summit for Europe to become the most competitive knowledge based economy in the 
world.  
 
The current legal framework for CIIs in Europe is serving all stakeholders well.  We do not want to see any 
sudden or dramatic reduction in the scope of what is patentable.  If a solution cannot be found which codifies 
the status quo, it would be better to have no directive at all than a directive which does untold damage to 
European industry. 
 
Sincerely, 
____________________      _____________________ 
Serge Tchuruk             Carl-Henric Svanberg 
Chairman & Chief Executive Officer Chief Executive Officer 
Alcatel                    Telefonaktiebolaget LM Ericsson 
54  rue La Botie           SE-164 83 Stockholm 
F-75008 Paris 
____________________      _______________________ 
Jorma Ollila 
                         Gerard J.  Kleisterlee 
Chairman & Chief Executive Officer
			President & Chief Executive Officer 
Nokia Corporation 
                                Koninklijke Philips Electronics NV 
Keilalahdentie 2-4 
                                Breitner Center 
P.O.  BOX 226 
                         Amstelplein 2 
FIN-00045 NOKIA GROUP NL-1096 BC Amsterdam 
 
_____________________ 
Heinrich v. Pierer 
President & Chief Executive Officer 
Siemens Aktiengesellschaft 
Wittelsbacher Platz 2 
D-80333 Munich  
