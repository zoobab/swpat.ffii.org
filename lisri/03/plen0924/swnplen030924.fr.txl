<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#aea: In 2002 the patent administrators of the Council pushed for unlimited patentability, although according to the procedural rules of EU legislation it was not yet their turn.  Like the European Commission's Directorate for the Internal Market, the Council's %(q:Patent Policy Working Group) is an institution on which the patent department of big IT companies can count.  It's members are always willing to act against written instructions of their own government, if the consensus of the patent lobby demands this.

#con: peut contenir des nouvelles courantes

#Iur: Archive des nouvelles de la FFII

#rra: The PSE-UK rapporteur, whose hardline pro-patent voting list was not followed by any political group in the Parliament, presents her defeat as a victory, all the while not forgetting to lash out against %(q:misinformation campaign) let by an unnamed group, probably the %(q:Free Software Alliance).

#wWs: This voting list is based on a compromise within PSE.  It introduces several amendments which are contrary in spirit to McCarthy's %(ju:JURI draft report).  Yet only 1/3 or the PSE members followed this voting lists.  The rest created a voting list of its own, which is closer to the FFII recommendations.  McCarthy was thus marginalised.

#CWg: Liste de vote de McCarthy

#Wub: McCarthy a déjà proféré les mêmes menaces en février.  L'analyse de sa publication par la FFII avait montré qu'elle n'avait pas pu les proférer sans l'appui de quelqu'un à la Commission Européenne. Nous savons désormais qui était son appui.

#Whe: McCarthy 03-02-19: Refuse au PE son droit de fixer les règles

#lih: Les menaces de Bolkestein

#ale: Contient les résultats du vote

#ins: Horns accuses European Parliament of FFII-inspired dilettantism and predicts that Bolkestein, Council and US friends will kill the directive.  Pilch refutes the arguments.  Discussion in German.

#uoe: Hartmut Pilch responds to PA Axel Horns

#lvt: Brussels correspondent Paul Meller reports that now the roles of supporters and opponents of the directive have changed.  Quotes Hartmut Pilch and Laura Creighton.

#rme: Answer by Hartmut Pilch to Axel Horns in mailing list discussion on the Broersma article

#tpe: Dr. Lenz, professor of german and european law in Tokyo, is worried about the attempts of Horns and other patent lawyers to declare themselves %(q:experts) in this matter and deny the competence of the European Parliament, points out that this runs counter to recent principle decisions of the German Constitutional Court.  Moreover Lenz confesses himself guilty of what Horns calls a %(q:misinformation campaign) about the exclusion of software from patentability by the European Patent Convention and expresses doubt about the correctness of Horns's assertions.

#rsw: Part II of Engström's refutal of Batteson and assessments of the strengths and weaknesses of parliamentary democracy, as shown in the software patent vote.

#vPi: Christian Engström, swedish software developper, refutes statements by UK patent lawyer Alex Batteson who asked that the European Parliament should be stripped of its right to legislate on patent matters, as by voting against software patents it had shown its incompetence.

#cWi: UK patent lawyer and former EPO examiner Alex Batteson denies competence of the parliament in matters of patent legislation, predicts that European Commission and Council will withdraw directive and entrust %(q:patent experts) from national governments with legislation via the European Patent Organisation.

#foh: Un bilan informatif de ce qui s'est produit

#Wbi: Patent lawyers of large ICT companies, speaking in the name of a European industry organisation, are very unhappy about the outcome of the Europarl vote

#sey: Brussels correspondant Mathew Newman confuses patent lawyer interests with industry interests, attributes limiting amendments to %(q:environmentalists and socialists), extensively quotes EICTA statements.

#Wca: Patent attorney Horns says that EP decision is %(q:rubbish), based on FFII %(q:misinformation campaign), will be %(q:thrown into the dustbin) by European Commission or Council and, if not, attacked on the basis of TRIPs by friends from US.

#thh: A patent attorney trying to mobilise his profession for backlash in the Council.

#oWW: D'autres contacts seront proposés sur simple demande

#usy: Le moment est venu de demander aux policitiens européens d'afficher leur courage et leur volonté de diriger le monde en votant une directive que nous envient de nombreux citoyens américains, gouvernement, PMEs et Alan Greenspan.  Demandez leur une harmonisation avec l'Europe.  Les membres du Parlement Européen méritent d'être remerciés pour les efforts qu'ils ont déployé pour comprendre les conséquences sociales de cette décision technique difficile.  Ceci ne s'est pas produit ailleurs dans le monde jusqu'ici.  Nous, Européens, pouvons être fiers de cette réussite politique, et j'espère que nos politiciens partagent cette fierté.

#nto: Désormais ceux qui se sont proclamés opposés à une confusion à l'américaine en matière de brevets tout en espérant une loi qui permettrait les mêmes choses, vont devoir s'exprimer. Je prédis que bon nombre d'entres eux vont dire que notre directive amendée ne doit pas passer, car nous avons besoin d'une loi plus proche de celle des Etats-Unix et du Japon pour ne pas froisser nos partenaires commerciaux.

#Cpa: Laura Creighton, dirigeante d'une entreprise de logiciel et investisseuse, qui a soutenu la campagne de la FFII/Eurolinux par des dons et des voyages répétés entre la Suède et Bruxelles pour participer à des conférences et rencontres avec des eurodéputés:

#cPg: La directive devra résister à l'examen, plus informel et donc moins public que les procédures parlementaires, par le Conseil des Ministres.  Par le passé, le Conseil des Ministres avait confié les décisions en matière de politique des brevets à son %(q:groupe de travail sur la politique des brevets), qui est formé d'experts en droit des brevets qui siègent aussi dans le conseil d'administration de l'Office Européen des Brevets (OEB).  Ce groupe a été l'un des promoteurs les plus déterminés à la brevetabilité illimitée, incluant les programmes, en Europe.

#iml: Cependant, lorsque 78 amendements sont votés en 40 minutes quelques dérapages surviennent : %(q:Les récitals n'ont pas été amendés parfaitement.  L'un d'eux prétend toujours qu'un algorithme peut être breveté lorsqu'il permet de résoudre un problème technique.), déclare Jonas Maebe, représentant belge de la FFII travaillant actuellement au sein du Parlement Européen.  %(q:Mais nous avons tous les ingrédients pour une bonne directive. Nous avons été capables de sculpter le gros oeuvre. Maintenant nous pouvons entamer les finitions.  L'esprit de la Convention Européenne sur les Brevets est réaffirmé à 80% et le Parlement est en bonne voie pour supprimer les inconsistances dans une seconde lecture.)

#mec: %(q:Avec les nouvelles dispositions de l'article 2, une invention implémentée par ordinateur n'est plus un cheval de Troie, mais une machine à laver), explique Erik Josefsson du SSLUG et de la FFII, qui a prodigué des conseils concernant la directive aux députés suédois durant les dernières semaines. La plupart des amendements votés étaient soutenus par des groupes politiques très différents - ceci reflète la discussion politique ardue qui avait conduit auparavant à deux renvois.

#neo: %(q:Le texte de la directive tel qu'amendé par le Parlement Européen, exclut clairement les brevets logiciels.  Elle est étonnamment cohérente.  Je pense que nous avons fait quelquechose de stupéfiant cette semaine) s'exclâme James Heald, un membre du groupe de travail sur les brevets logiciels de la FFII/Eurolinux, alors qu'il rassemble les amendements votés dans une %(pr:version consolidée).

#ifu: Le jour précédant le vote, le commissaire européen Bolkestein avait %(fb:brandi la menace) d'un retrait de la proposition de directive par la Commission et le Conseil, qui conduirait à laisser aux représentants nationaux de l'Office Européen des Brevets (OEB) le soin de trancher les questions en matière de brevetabilité, si le Parlement votait les amendements qui ont été adoptés aujourd'hui.  %(q:Reste à savoir si la Commission Européenne s'engage à %(q:une harmonisation et une clarification) ou se préoccupe uniquement des intérêts des détenteurs de brevets), déclare Hartmut Pilch, président de la FFII.  %(q:Cette directive est désormais aussi la nôtre.  Nous devons aider le Parlement Européen à la défendre.)

#media: Contact Media

#descr: Dans son vote en séance plénière du 24 septembre, le Parlement Européen a approuvé la directive proposée sur la %(q:brevetabilité des inventions implémentées par ordinateur) avec des amendements rétablissant clairement la non-brevetabilité des logiques de programmation et de commerce, et faisant respecter la liberté de publication et d'interopérabilité.

#title: Le Parlement Européen vote de réelles limites à la brevetabilité

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnplen030924 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

