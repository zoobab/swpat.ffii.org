\begin{subdocument}{swnplen030924}{EU Parlement Stemt voor Echte Beperkingen op Octrooieerbaarheid}{http://swpat.ffii.org/lisri/03/plen0924/index.nl.html}{Werkgroep\\swpatag@ffii.org\\Nederlandse versie 2003/09/26 door Jonas Maebe\footnote{http://lists.ffii.org/mailman/listinfo/traduk/index.html}}{In its plenary vote on the 24th of September, the European Parliament approved the proposed directive on ``patentability of computer-implemented inventions'' with amendments that clearly restate the non-patentability of programming and business logic, and uphold freedom of publication and interoperation.}
\begin{sect}{detal}{achtergrondinformatie}
De dag voor de stemming had CEC Commissaris Bokestein het Europees Parlement nog dreigend toegesproken. Hij zei dat indien het Parlement amendementen zou aanvaarden in de zin van deze die ze nu goedgekeurd heeft, de Commissie en de Raad het voorstel tot richtlijn zouden terugtrekken en de zaak zou laten behandelen door het bestuur van het Europese Octrooibureau. ``Het is nu afwachten om te zien of de Europese Commissie zich wil toeleggen op het \percent{}q(harmoniseren en verduidelijken'', of enkel op het verdedigen van de interesses van octrooi-eigenlaars), zegt Hartmut Pilch, voorzitter van FFII.  ``Dit is nu ook onze richtlijn geworden.  We moeten het Europees Parlement helpen hem te verdedigen en verder op punt te stellen.''

``De tekst van de richtlijn zoals hij goedgekeurd is, sluit duidelijk softwarepatenten uit.  Hij is ongelofelijk sterk op dit vlak.  Ik denk dat we deze week iets ongelofelijk hebben bereikt'', vertelde een duidelijk opgewonden James Heald, lid van de FFII/EuroLinux softwarepatenten werkgroep, terwijl hij de gestemde amendementen samenvoegde in een ``geconsolideerde versie''.

``Met de nieuwe voorwaarden in artikel 2 zijn is een in een computer ge\"{\i}mplementeerde uitvinding niet langer een Trojaans Paard, maar een wasmachine'', legt Erik Josefsson van SSLUG en FFII uit, hiermee verwijzend naar het feit dat deze term in zijn vroegere definitie gewoon de octrooieerbaarheid van pure software vastlegde. ``De meerderheden voor de gestemde amendementen kregen steun van zeer verschillende politieke groepen. Dit toont duidelijk de verdeeldheid aan waardoor de beslissing reeds tweemaal werd uitgesteld.''

Niettemin, wanneer er over 78 amendementen gestemd wordt in 40 minuten, kan het niet anders dan dat er een paar fouten gemaakt worden: ``De toelichtingen werden slechts zeer oppervlakkig bijgestuurd.  E\'{e}n ervan beweert nog steeds dat algoritmen patenteerbaar kunnen zijn wanneer ze een technisch probleem oplossen'', zegt Jonas Maebe, een Belgische FFII vertegenwoordiger die momenteel in het Europees Parlement aanwezig is. ``We hebben nu echter alle ingredi\"{e}nten om een goede richtlijn te maken. We zijn erin geslaagd goede fundamenten te bouwen. Nu kan het detailwerk beginnen. De geest van de Europese Patentenconventie is reeds voor 80\percent{} herbevestigd en het Parlement is in een goede positie om de overblijvende inconsistenties in de tweede lezing te corrigeren.''

De richtlijn zal nu een verdere behandeling door de Raad van Ministers moeten doorstaan, die meer informeel en bijgevolg minder publiek is dan de Parlementaire procedures.  In het verleden heeft de Raad van Ministers beslissingen over het octrooibeleid steeds overgelaten aan zijn ``werkgroep voor octrooibeleid''. Deze bestaat uit experten in octrooirecht die ook in de administratieve raad van het Europees Octrooibureau zitten.  Deze groep was tot hiertoe steeds een van de meest vastberaden voorstanders van onbeperkte octrooieerbaarheid, inclusief wat betreft software, binnen Europa.

Laura Creighton, een softwareonderneemster en risicoinvesteerder die de FFII/EuroLinux campagne ondersteund heeft met giften en die reeds verschillende malen van Zweden naar Brussel is gereisd om conferenties en bijeenkomsten met Europarlementslden bij te wonen, zegt:

\begin{quote}
{\it Nu zullen de mensen die beweerden te zijn tegen wanpraktijken zoals in de VS, maar die enkel achter het voorstel stonden omdat het dergelijke toestanden toeliet, zich moeten blootgeven. Ik denk dat verschillende van hen zullen beweren dat we deze richtlijn niet mogen goedkeuren, omdat we een richtlijn zouden nodig hebben die ons meer gelijkstelt met de praktijk in de VS en Japan om onze handelspartners niet boos te maken.}

{\it Nu is het moment aangebroken om de Europese politici te vragen moed en wereldleiderschap te tonen, en om de richtlijn te \emph{steunen}. Het is de richtlijn die de Amerikaanse burgers, regering, KMO's/MKB's en Alan Greenspan zouden willen hebben in plaats van hun huidige wantoestanden.  Vraag hen te harmoniseren met Europa.  De leden van het Europees Parlement verdienen onze welgemeende dank voor hun inzet om te begrijpen wat de sociale gevolgen van deze alles behalve gemakkelijke technische beslissing zouden zijn.  Dit is nog nergens ter wereld gebeurd tot hiertoe.  Wij, Europeanen, kunnen trots zijn op deze politieke verwezenlijking, en ik hoop dat onze politici deze trots delen.}
\end{quote}
\end{sect}

\begin{sect}{media}{Mediacontacten}
\begin{description}
\item[mail:]\ pr at ffii org
\item[telefoon:]\ Hartmut Pilch +49-89-18979927
Jonas Maebe +32-485-369645
Erik Josefsson +46-707-696567
Alex Macfie +44 7901 751753
Joaquim Carvalho +35-1-93-6169633
Meer contacten worden verstrekt op vraag
\end{description}
\end{sect}

\begin{sect}{url}{Permanente link naar deze persmededeling}
http://swpat.ffii.org/lisri/03/plen0924/index.nl.html
\end{sect}

\begin{sect}{links}{koppelingen met voetnoten}
\begin{itemize}
\item
{\bf {\bf Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/vote/index.en.html}}}

\begin{quote}
Bevat de resultaten van de Stemming
\end{quote}
\filbreak

\item
{\bf {\bf Tauss \& Kelber 2003-09-24: EP erkl\"{a}rt Trivialpatenten eine Absage\footnote{http://swpat.ffii.org/papri/europarl0309/spdmdb030924/index.de.html}}}

\begin{quote}
Press release of two members of the german federal parliament who lead the IT policy of the social democratic group.  MP Tauss and Kelber ask the Council and Commission to support the EP's decision.
\end{quote}
\filbreak

\item
{\bf {\bf Bolkestein's dreigementen\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/deba/index.en.html\#bolk}}}
\filbreak

\item
{\bf {\bf McCarthy 03-02-19: Het EP zijn recht ontzeggen om de regels vast te stellen\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/amccarthy0302/index.en.html\#altern}}}

\begin{quote}
McCarthy uitte reeds een aantal dreigementen in februari.  De FFII analyse van haar paper toonde aan dat ze deze niet kon hardmaken zonder iemand in de Europese Commissie te hebben die haar steunde.  Nu weten we wie dat is.

zie Frits Bolkestein and Software Patents\footnote{http://swpat.ffii.org/gasnu/bolkestein/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf McCarthy's Stemlijst\footnote{../../../papri/eubsa-swpat0202/plen0309/vote/lists/mccarthy.pdf}}}

\begin{quote}
This voting list is based on a compromise within PSE.  It introduces several amendments which are contrary in spirit to McCarthy's JURI draft report\footnote{http://swpat.ffii.org/lisri/03/juri0617/index.en.html}.  Yet only 1/3 or the PSE members followed this voting lists.  The rest created a voting list of its own, which is closer to the FFII recommendations.  McCarthy was thus marginalised.
\end{quote}
\filbreak

\item
{\bf {\bf McCarthy Press Release\footnote{amccarthy-pr030924.html}}}

\begin{quote}
De PSE-UK rapporteur, wiens pro-softwarepatenten stemlijst algemeen door geen enkele politieke groep in het Parlement werd gevolgd, presenteert haar nederlaag als een overwinning. Hierbij vergeet ze niet nog een scherp uit te halen naar de ``desinformatiecampagne'' geleid door een niet nader genoemde groep, waarschijnlijk de ``Free Software Alliance''. 

zie Free Software Alliance\footnote{http://swpat.ffii.org/gasnu/fsa/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Plenary Debate 03/09/23\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/deba/index.en.html}}}

\begin{quote}
Rough Transcript of the Speeches given in the Plenary Debate of 2003/09/23.
\end{quote}
\filbreak

\item
{\bf {\bf ZDNet UK News: Patents directive wins European Parliament OK\footnote{http://news.zdnet.co.uk/business/legal/0,39020651,39116642,00.htm}}}

\begin{quote}
Un bilan assez informatif de ce qui s'est produit
\end{quote}
\filbreak

\item
{\bf {\bf EICTA reaction\footnote{http://www.eicta.org/dls/Logon/TopLogon.asp?URL=/Common/GetFile.asp?\&logonname=guest\&ID=6861\&mfd=off}}}

\begin{quote}
Zeer ongelukkig met de richtlijn

zie ook EICTA reaction\footnote{http://www.eicta.org/dls/Logon/TopLogon.asp?URL=/Common/GetFile.asp?\&logonname=guest\&ID=6861\&mfd=off} en EICTA and Software Patents\footnote{http://swpat.ffii.org/gasnu/eicta/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Down Jones Newsletter about the amended directive\footnote{http://www.quicken.com/investments/news\_center/story/?story=NewsStory/dowJones/20030924/ON200309240711000338.var\&column=P0DFP}}}

\begin{quote}
Brussels correspondant Mathew Newman confuses patent lawyer interests with industry interests, attributes limiting amendments to ``environmentalists and socialists'', extensively quotes EICTA statements.

zie ook Reactions to the EU Parliament's Vote of 2003/09/24\footnote{http://swpat.ffii.org/papri/europarl0309/reag/index.nl.html}
\end{quote}
\filbreak

\item
{\bf {\bf IDG.com.sg: reversal of positions on Directive\footnote{http://www.idg.com.sg/idgwww.nsf/unidlookup/955189AAA890112C48256DB10014D9C7?OpenDocument}}}

\begin{quote}
Brussels correspondent Paul Meller reports that now the roles of supporters and opponents of the directive have changed.  Quotes Hartmut Pilch and Laura Creighton.
\end{quote}
\filbreak

\item
{\bf {\bf ZDNet UK News: Software patent limits 'go too far'\footnote{http://news.zdnet.co.uk/business/legal/0,39020651,39116709,00.htm}}}

\begin{quote}
UK patent lawyer and former EPO examiner Alex Batteson denies competence of the parliament in matters of patent legislation, predicts that European Commission and Council will withdraw directive and entrust ``patent experts'' from national governments with legislation via the European Patent Organisation.

zie ook Reactions to the EU Parliament's Vote of 2003/09/24\footnote{http://swpat.ffii.org/papri/europarl0309/reag/index.nl.html} en Christian Engstroem: Democracy not so bad (I)\footnote{http://www.zdnet.co.uk/talkback/?PROCESS=show\&ID=20013781\&AT=39116709-39020651t-10000022c}
\end{quote}
\filbreak

\item
{\bf {\bf Christian Engstroem: Democracy not so bad (I)\footnote{http://www.zdnet.co.uk/talkback/?PROCESS=show\&ID=20013781\&AT=39116709-39020651t-10000022c}}}

\begin{quote}
Christian Engstr\"{o}m, swedish software developper, refutes statements by UK patent lawyer Alex Batteson who asked that the European Parliament should be stripped of its right to legislate on patent matters, as by voting against software patents it had shown its incompetence.

zie ook Reactions to the EU Parliament's Vote of 2003/09/24\footnote{http://swpat.ffii.org/papri/europarl0309/reag/index.nl.html}, ZDNet UK News: Software patent limits 'go too far'\footnote{http://news.zdnet.co.uk/business/legal/0,39020651,39116709,00.htm} en Christian Engstroem: Democracy not so bad (II)\footnote{http://www.zdnet.co.uk/talkback/?PROCESS=show\&ID=20013782\&AT=39116709-39020651t-10000022c}
\end{quote}
\filbreak

\item
{\bf {\bf Horns 03-09-25: Severe Defeat for the Users of the Patent System\footnote{http://lists.ffii.org/archive/mails/swpat/2003/Sep/0749.html}}}

\begin{quote}
Een octrooiadvocaat probeert zijn confraters te mobiliseren om terug te slaan via de Raad van Europa.

zie ook PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/gasnu/horns/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf PA Axel H. Horns' blog on IPR: the day after\footnote{http://www.ipjur.com/2003\_09\_01\_archive.php3\#106451647590989047}}}

\begin{quote}
Patent attorney Horns says that EP decision is ``rubbish'', based on FFII ``misinformation campaign'', will be ``thrown into the dustbin'' by European Commission or Council and, if not, attacked on the basis of TRIPs by friends from US.

zie ook PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/gasnu/horns/index.en.html} en Reactions to the EU Parliament's Vote of 2003/09/24\footnote{http://swpat.ffii.org/papri/europarl0309/reag/index.nl.html}
\end{quote}
\filbreak

\item
{\bf {\bf Lenz Blog: Horns Blog on Patent Vote\footnote{http://k.lenz.name/LB/archives/000617.html}}}

\begin{quote}
Dr. Lenz, professor of german and european law in Tokyo, is worried about the attempts of Horns and other patent lawyers to declare themselves ``experts'' in this matter and deny the competence of the European Parliament, points out that this runs counter to recent principle decisions of the German Constitutional Court.  Moreover Lenz confesses himself guilty of what Horns calls a ``misinformation campaign'' about the exclusion of software from patentability by the European Patent Convention and expresses doubt about the correctness of Horns's assertions.

zie ook Reactions to the EU Parliament's Vote of 2003/09/24\footnote{http://swpat.ffii.org/papri/europarl0309/reag/index.nl.html} en PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/gasnu/horns/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf FFII Softwarepatente Diskussion Mailingliste: Re: Matthew Broersma : Software patent limits 'go too far' (fwd)\footnote{http://lists.ffii.org/archive/mails/swpat/2003/Sep/0873.html}}}

\begin{quote}
Answer by Hartmut Pilch to Axel Horns in mailing list discussion on the Broersma article

zie ook Reactions to the EU Parliament's Vote of 2003/09/24\footnote{http://swpat.ffii.org/papri/europarl0309/reag/index.nl.html} en PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/gasnu/horns/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Hartmut Pilch responds to PA Axel Horns\footnote{http://lists.ffii.org/archive/mails/swpat/2003/Sep/0882.html}}}

\begin{quote}
Horns accuses European Parliament of FFII-inspired dilettantism and predicts that Bolkestein, Council and US friends will kill the directive.  Pilch refutes the arguments.  Discussion in German.

zie ook PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/gasnu/horns/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf FFII nieuwsarchief\footnote{http://lists.ffii.org/archive/mails/neues/index.html}}}

\begin{quote}
Kan wat huidig nieuws bevatten
\end{quote}
\filbreak

\item
{\bf {\bf FFII nieuwsarchief\footnote{http://lists.ffii.org/archive/mails/news/index.html}}}

\begin{quote}
Kan wat huidig nieuws bevatten
\end{quote}
\filbreak

\item
{\bf {\bf CEU/DKPTO 2002/09/23..: Software Patentability Directive Amendment Proposal\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/dkpto0209/index.en.html}}}

\begin{quote}
In 2002 streefde de administratie van de Raad naar onbeperkte octrooieerbaarheid, hoewel het volgens de procedureregels van de EU-wetgeving nog niet hun beurt was.  Net zoals het Directoraat voor de Interne Markt van de Europese Commissie, is de ``werkgroep voor octrooibeleid'' van de Raad een instituut waarop de octrooiafdelingen van grote IT-bedrijven kunnen regelen.  Haar leden zijn altijd bereid te handelen tegen schriftelijke instructies van hun eigen regering in, indien de consensus van de patentenlobby dit vraagt.
\end{quote}
\filbreak

\item
{\bf {\bf Petition Initiators Thank the European Parliament\footnote{http://swpat.ffii.org/lisri/03/epet0929/index.en.html}}}

\begin{quote}
Last Wednesday the Parliament voted against software patents and for freedom of publication, freedom of interoperation and other basic values of the information society, thereby reversing the thrust of a directive proposal from the European Commission, so as to basically satisfy the demands of a quarter million signatories of the ``Eurolinux Petition for a Software Patent Free Europe'' and 30 eminent computer scientists.  The initiators of both petitions will speak before the European Parliament's Petition Committee on tuesday 18.00 to express their thanks and explore with MEPs what still needs to be done.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

