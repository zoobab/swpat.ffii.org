<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#aea: In 2002 the patent administrators of the Council pushed for unlimited patentability, although according to the procedural rules of EU legislation it was not yet their turn.  Like the European Commission's Directorate for the Internal Market, the Council's %(q:Patent Policy Working Group) is an institution on which the patent department of big IT companies can count.  It's members are always willing to act against written instructions of their own government, if the consensus of the patent lobby demands this.

#con: may contain some current news

#Iur: FFII Neues Archiv

#rra: The PSE-UK rapporteur, whose hardline pro-patent voting list was not followed by any political group in the Parliament, presents her defeat as a victory, all the while not forgetting to lash out against %(q:misinformation campaign) let by an unnamed group, probably the %(q:Free Software Alliance).

#wWs: This voting list is based on a compromise within PSE.  It introduces several amendments which are contrary in spirit to McCarthy's %(ju:JURI draft report).  Yet only 1/3 or the PSE members followed this voting lists.  The rest created a voting list of its own, which is closer to the FFII recommendations.  McCarthy was thus marginalised.

#CWg: McCarthy Voting List

#Wub: McCarthy uttered the same threats in February already.  The FFII analysis of her paper pointed out that she couldn't have uttered them if there was not someone at the European Commission backing her.  Now we know who her backer was.

#Whe: McCarthy 03-02-19: Denying the EP its right to set the rules

#lih: Bolkestein's Threats

#ale: Contains Results of the Vote

#ins: Horns accuses European Parliament of FFII-inspired dilettantism and predicts that Bolkestein, Council and US friends will kill the directive.  Pilch refutes the arguments.  Discussion in German.

#uoe: Hartmut Pilch responds to PA Axel Horns

#lvt: Brussels correspondent Paul Meller reports that now the roles of supporters and opponents of the directive have changed.  Quotes Hartmut Pilch and Laura Creighton.

#rme: Answer by Hartmut Pilch to Axel Horns in mailing list discussion on the Broersma article

#tpe: Dr. Lenz, professor of german and european law in Tokyo, is worried about the attempts of Horns and other patent lawyers to declare themselves %(q:experts) in this matter and deny the competence of the European Parliament, points out that this runs counter to recent principle decisions of the German Constitutional Court.  Moreover Lenz confesses himself guilty of what Horns calls a %(q:misinformation campaign) about the exclusion of software from patentability by the European Patent Convention and expresses doubt about the correctness of Horns's assertions.

#rsw: Part II of Engström's refutal of Batteson and assessments of the strengths and weaknesses of parliamentary democracy, as shown in the software patent vote.

#vPi: Christian Engström, swedish software developper, refutes statements by UK patent lawyer Alex Batteson who asked that the European Parliament should be stripped of its right to legislate on patent matters, as by voting against software patents it had shown its incompetence.

#cWi: UK patent lawyer and former EPO examiner Alex Batteson denies competence of the parliament in matters of patent legislation, predicts that European Commission and Council will withdraw directive and entrust %(q:patent experts) from national governments with legislation via the European Patent Organisation.

#foh: An informative account of what happened.

#Wbi: Patent lawyers of large ICT companies, speaking in the name of a European industry organisation, are very unhappy about the outcome of the Europarl vote

#sey: Brussels correspondant Mathew Newman confuses patent lawyer interests with industry interests, attributes limiting amendments to %(q:environmentalists and socialists), extensively quotes EICTA statements.

#Wca: Patent attorney Horns says that EP decision is %(q:rubbish), based on FFII %(q:misinformation campaign), will be %(q:thrown into the dustbin) by European Commission or Council and, if not, attacked on the basis of TRIPs by friends from US.

#thh: A patent attorney trying to mobilise his profession for backlash in the Council.

#oWW: More Contacts to be supplied upon request

#usy: Now is the time to ask European politicians to show courage, and world leadership and vote %(e:up) the directive that the American citizens, government, SMEs and Alan Greenspan wish they had instead of the current mess.  Ask them to harmonise with Europe.  The members of the European Parliament deserve thanks for their efforts in understanding the social consequences of this admittedly difficult technical decision.  This has not happened anywhere else in the world so far.  We Europeans can be proud of this political achievement, and I hope our politicians share this pride.

#nto: Now those people who claimed to be opposed to having a US style mess, but only liked the bill because it permitted such things, will have to expose themselves. I predict a good number of them will claim that we must not pass this one, because we need a bill that makes us more similar to the US and Japan for the sake of not angering our trading partners.

#Cpa: Says Laura Creighton, software entrepreneur and venture capitalist, who has supported the FFII/Eurolinux campaign with donations and travelled from Sweden to Brussels several times to attend conferences and meetings with MEPs:

#cPg: The directive will have to withstand further consultation with the Council of Ministers that is more informal and hence less public than Parliamentary Procedures.  In the past, the Council of Ministers has left patent policy decisions to its %(q:patent policy working party), which consists of patent law experts who are also sitting on the administrative council of the European Patent Office (EPO).  This group has been one of the most determined promoters of unlimited patentability, including program claims, in Europe.

#iml: However, when 78 amendments are voted in 40 minutes some glitches are bound to happen: %(q:The recitals were not amended thouroughly.   One of them still claims algorithms to be patentable when they solve a technical problem.), says Jonas Maebe, Belgian FFII representative currently working in the European Parliament.  %(q:But we have all the ingredients for a good directive. We've been able to do the rough sculpting work.  Now the patching work can begin.  The spirit of the European Patent Convention is 80% reaffirmed, and the Parliament is in a good position to remove the remaining inconsistencies in the second reading.)

#mec: %(q:With the new provisions of article 2, a computer-implemented invention is no longer a trojan horse, but a washing machine), explains Erik Josefsson from SSLUG and FFII, who has been advising Swedish MEPs on the directive in recent weeks. That the majorities for the voted amendments had support from very different political groups - this reflects the arduous political discussion that had led to two postponements before.

#neo: %(q:The directive text as amended by the European Parliament clearly excludes software patents.  It hangs together incredibly cohesively.  I think we have done something amazing this week) exclaimed James Heald, a member of the FFII/Eurolinux software patent working group, as he put together the voted amendments into a %(pr:consolidated version).

#ifu: The day before the vote, CEC Commissioner Bolkestein had %(fb:threatened) that the Commission and the Council would withdraw the directive proposal and hand the questions back to the national patent administrators on the board of the European Patent Office (EPO), should the Parliament vote for the amendments which it supported today.  %(q:It remains to be seen, whether the European Commission is committed to %(q:harmonisation and clarification) or only to patent owner interests), says Hartmut Pilch, president of FFII.  %(q:This is now our directive too.  We must help the European Parliament defend it.)

#media: Media Contacts

#descr: In its plenary vote on the 24th of September, the European Parliament approved the proposed directive on %(q:patentability of computer-implemented inventions) with amendments that clearly restate the non-patentability of programming and business logic, and uphold freedom of publication and interoperation.

#title: EU Parliament Votes for Real Limits on Patentability

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnplen030924 ;
# txtlang: en ;
# multlin: t ;
# End: ;

