<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: In its plenary vote on the 24th of September, the European Parliament approved the proposed directive on %(q:patentability of computer-implemented inventions) with amendments that clearly restate the non-patentability of programming and business logic, and uphold freedom of publication and interoperation.
#title: EU Parlement Stemt voor Echte Beperkingen op Octrooieerbaarheid
#media: Mediacontacten
#ifu: De dag voor de stemming had CEC Commissaris Bokestein het Europees Parlement nog %(fs:dreigend) toegesproken. Hij zei dat indien het Parlement amendementen zou aanvaarden in de zin van deze die ze nu goedgekeurd heeft, de Commissie en de Raad het voorstel tot richtlijn zouden terugtrekken en de zaak zou laten behandelen door het bestuur van het Europese Octrooibureau. %(q:Het is nu afwachten om te zien of de Europese Commissie zich wil toeleggen op het %q(harmoniseren en verduidelijken), of enkel op het verdedigen van de interesses van octrooi-eigenlaars), zegt Hartmut Pilch, voorzitter van FFII.  %(q:Dit is nu ook onze richtlijn geworden.  We moeten het Europees Parlement helpen hem te verdedigen en verder op punt te stellen.)
#neo: %(q:De tekst van de richtlijn zoals hij goedgekeurd is, sluit duidelijk softwarepatenten uit.  Hij is ongelofelijk sterk op dit vlak.  Ik denk dat we deze week iets ongelofelijk hebben bereikt), vertelde een duidelijk opgewonden James Heald, lid van de FFII/EuroLinux softwarepatenten werkgroep, terwijl hij de gestemde amendementen samenvoegde in een %(q:geconsolideerde versie).
#mec: %(q:Met de nieuwe voorwaarden in artikel 2 zijn is een in een computer geïmplementeerde uitvinding niet langer een Trojaans Paard, maar een wasmachine), legt Erik Josefsson van SSLUG en FFII uit, hiermee verwijzend naar het feit dat deze term in zijn vroegere definitie gewoon de octrooieerbaarheid van pure software vastlegde. %(q:De meerderheden voor de gestemde amendementen kregen steun van zeer verschillende politieke groepen. Dit toont duidelijk de verdeeldheid aan waardoor de beslissing reeds tweemaal werd uitgesteld.)
#iml: Niettemin, wanneer er over 78 amendementen gestemd wordt in 40 minuten, kan het niet anders dan dat er een paar fouten gemaakt worden: %(q:De toelichtingen werden slechts zeer oppervlakkig bijgestuurd.  Eén ervan beweert nog steeds dat algoritmen patenteerbaar kunnen zijn wanneer ze een technisch probleem oplossen), zegt Jonas Maebe, een Belgische FFII vertegenwoordiger die momenteel in het Europees Parlement aanwezig is. %(q:We hebben nu echter alle ingrediënten om een goede richtlijn te maken. We zijn erin geslaagd goede fundamenten te bouwen. Nu kan het detailwerk beginnen. De geest van de Europese Patentenconventie is reeds voor 80% herbevestigd en het Parlement is in een goede positie om de overblijvende inconsistenties in de tweede lezing te corrigeren.)
#cPg: De richtlijn zal nu een verdere behandeling door de Raad van Ministers moeten doorstaan, die meer informeel en bijgevolg minder publiek is dan de Parlementaire procedures.  In het verleden heeft de Raad van Ministers beslissingen over het octrooibeleid steeds overgelaten aan zijn %(q:werkgroep voor octrooibeleid). Deze bestaat uit experten in octrooirecht die ook in de administratieve raad van het Europees Octrooibureau zitten.  Deze groep was tot hiertoe steeds een van de meest vastberaden voorstanders van onbeperkte octrooieerbaarheid, inclusief wat betreft software, binnen Europa.
#Cpa: Laura Creighton, een softwareonderneemster en risicoinvesteerder die de FFII/EuroLinux campagne ondersteund heeft met giften en die reeds verschillende malen van Zweden naar Brussel is gereisd om conferenties en bijeenkomsten met Europarlementslden bij te wonen, zegt:
#nto: Nu zullen de mensen die beweerden te zijn tegen wanpraktijken zoals in de VS, maar die enkel achter het voorstel stonden omdat het dergelijke toestanden toeliet, zich moeten blootgeven. Ik denk dat verschillende van hen zullen beweren dat we deze richtlijn niet mogen goedkeuren, omdat we een richtlijn zouden nodig hebben die ons meer gelijkstelt met de praktijk in de VS en Japan om onze handelspartners niet boos te maken.
#usy: Nu is het moment aangebroken om de Europese politici te vragen moed en wereldleiderschap te tonen, en om de richtlijn te %(e:steunen). Het is de richtlijn die de Amerikaanse burgers, regering, KMO's/MKB's en Alan Greenspan zouden willen hebben in plaats van hun huidige wantoestanden.  Vraag hen te harmoniseren met Europa.  De leden van het Europees Parlement verdienen onze welgemeende dank voor hun inzet om te begrijpen wat de sociale gevolgen van deze alles behalve gemakkelijke technische beslissing zouden zijn.  Dit is nog nergens ter wereld gebeurd tot hiertoe.  Wij, Europeanen, kunnen trots zijn op deze politieke verwezenlijking, en ik hoop dat onze politici deze trots delen.
#oWW: Meer contacten worden verstrekt op vraag
#thh: Een octrooiadvocaat probeert zijn confraters te mobiliseren om terug te slaan via de Raad van Europa.
#Wca: Patent attorney Horns says that EP decision is %(q:rubbish), based on FFII %(q:misinformation campaign), will be %(q:thrown into the dustbin) by European Commission or Council and, if not, attacked on the basis of TRIPs by friends from US.
#sey: Brussels correspondant Mathew Newman confuses patent lawyer interests with industry interests, attributes limiting amendments to %(q:environmentalists and socialists), extensively quotes EICTA statements.
#Wbi: Zeer ongelukkig met de richtlijn
#foh: Een informatieve samenvatting van wat gebeurd is
#cWi: UK patent lawyer and former EPO examiner Alex Batteson denies competence of the parliament in matters of patent legislation, predicts that European Commission and Council will withdraw directive and entrust %(q:patent experts) from national governments with legislation via the European Patent Organisation.
#vPi: Christian Engström, swedish software developper, refutes statements by UK patent lawyer Alex Batteson who asked that the European Parliament should be stripped of its right to legislate on patent matters, as by voting against software patents it had shown its incompetence.
#rsw: Part II of Engström's refutal of Batteson and assessments of the strengths and weaknesses of parliamentary democracy, as shown in the software patent vote.
#tpe: Dr. Lenz, professor of german and european law in Tokyo, is worried about the attempts of Horns and other patent lawyers to declare themselves %(q:experts) in this matter and deny the competence of the European Parliament, points out that this runs counter to recent principle decisions of the German Constitutional Court.  Moreover Lenz confesses himself guilty of what Horns calls a %(q:misinformation campaign) about the exclusion of software from patentability by the European Patent Convention and expresses doubt about the correctness of Horns's assertions.
#rme: Answer by Hartmut Pilch to Axel Horns in mailing list discussion on the Broersma article
#lvt: Brussels correspondent Paul Meller reports that now the roles of supporters and opponents of the directive have changed.  Quotes Hartmut Pilch and Laura Creighton.
#uoe: Hartmut Pilch responds to PA Axel Horns
#ins: Horns accuses European Parliament of FFII-inspired dilettantism and predicts that Bolkestein, Council and US friends will kill the directive.  Pilch refutes the arguments.  Discussion in German.
#ale: Bevat de resultaten van de Stemming
#lih: Bolkestein's dreigementen
#Whe: McCarthy 03-02-19: Het EP zijn recht ontzeggen om de regels vast te stellen
#Wub: McCarthy uitte reeds een aantal dreigementen in februari.  De FFII analyse van haar paper toonde aan dat ze deze niet kon hardmaken zonder iemand in de Europese Commissie te hebben die haar steunde.  Nu weten we wie dat is.
#CWg: McCarthy's Stemlijst
#wWs: This voting list is based on a compromise within PSE.  It introduces several amendments which are contrary in spirit to McCarthy's %(ju:JURI draft report).  Yet only 1/3 or the PSE members followed this voting lists.  The rest created a voting list of its own, which is closer to the FFII recommendations.  McCarthy was thus marginalised.
#rra: De PSE-UK rapporteur, wiens pro-softwarepatenten stemlijst algemeen door geen enkele politieke groep in het Parlement werd gevolgd, presenteert haar nederlaag als een overwinning. Hierbij vergeet ze niet nog een scherp uit te halen naar de %(q:desinformatiecampagne) geleid door een niet nader genoemde groep, waarschijnlijk de %(q:Free Software Alliance). 
#Iur: FFII nieuwsarchief
#con: Kan wat huidig nieuws bevatten
#aea: In 2002 streefde de administratie van de Raad naar onbeperkte octrooieerbaarheid, hoewel het volgens de procedureregels van de EU-wetgeving nog niet hun beurt was.  Net zoals het Directoraat voor de Interne Markt van de Europese Commissie, is de %(q:werkgroep voor octrooibeleid) van de Raad een instituut waarop de octrooiafdelingen van grote IT-bedrijven kunnen regelen.  Haar leden zijn altijd bereid te handelen tegen schriftelijke instructies van hun eigen regering in, indien de consensus van de patentenlobby dit vraagt.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: jmaebe ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnplen030924 ;
# txtlang: nl ;
# End: ;

