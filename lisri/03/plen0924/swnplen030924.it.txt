<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: In questo voto plenario del 24 settembre, il Parlamento Europeo ha aprovato la direttiva proposta sulla %(q:brevettabilità delle invenzioni implementate al computer) con ementamenti che chiaramente dichiarano la non brevettabilità delle logiche di programmazione e del business, e difendendo la libertà di pubblicazione e interoperabilità.
#title: Il Parlamento Europeo Vota per i limiti Reali nella Brevettabilità.
#ifu: Il giorno prima del voto, il commissario Bolkestein della CCE aveva %(fb:avvertito) che la Commissione ed il Consiglio avrebbero ritirato la direttiva proposta e passato le domande nuovamente ai coordinatori nazionali sul brevetto dell'Ufficio europeo dei brevetti (EPO), nel caso che il Parlamento votasse per gli emendamenti che sono stati sostenuti oggi. %(q:Dobbiamo ancora vedere se la Commissione Europea è impegnata a %(q:armonizzare e chiarire) o soltanto a rispettare gli interessi dei proprietari di brevetti), dice Hartmut Pilch, presidente dell'FFII. %(q:Ora questa è anche la nostra direttiva. Dobbiamo aiutare il Parlamento Europeo a difenderla.)
#neo: %(q:Il testo della direttiva per come è stato emendato dal Parlamento Europeo è incredibilmente buono! Non riuscivo a crederlo mentre ne stavo inviando la storia pezzo per pezzo su Slashdot. Penso che abbiamo fatto qualcosa di stupefacente questa settimana) spiega James Herald, un membro del gruppo FFII/Eurolinux che lavora sui brevetti del software, poichè ha unito gli emendamenti votati in una %(pr:versione consolidata).
#mec: %(q:Con le nuove disposizioni dell'articolo 2, un'invenzione implementata al computer non è più un cavallo di troia, ma una lavatrice), spiega Erik Josefsson da SSLUG e FFII, che sta avvertendo i membri del Parlamento Europeo svedesi sulle direttive delle recenti settimane. Il fatto che la maggioranza degli emendamenti votati ha avuto supporto da gruppi politici molto differenti riflette la ardua discussione politica che ha costretto due rinvii. 
#iml: However, when 78 amendments are voted in 40 minutes some glitches are bound to happen: %(q:The recitals were not amended thouroughly.   One of them still claims algorithms to be patentable when they solve a technical problem.), says Jonas Maebe, Belgian FFII representative currently working in the European Parliament.  %(q:But we have all the ingredients for a good directive. We've been able to do the rough sculpting work.  Now the patching work can begin.  The spirit of the European Patent Convention is 80% reaffirmed, and the Parliament is in a good position to remove the remaining inconsistencies in the second reading.)
#cPg: La direttiva dovrà sostenere ulteriori consultazioni con il Consiglio dei Ministri che è più informale è meno pubblico delle procedure parlamentari. Nel passato, il Consiglio dei Ministri ha lasciato la gestione dei brevetti ai relativi %(q:gruppi di lavoro sulla politica dei brevetti), formati da esperti di legge nei brevetti, che come se non bastasse siedono al consiglio amministrativo dell'Ufficio europeo dei brevetti (EPO). Questo gruppo è stato uno dei promotori più risoluti della brevettabilità illimitata in Europa, comprese le rivendicazioni sui programmi. 
#Cpa: Dice Laura Creighton, l'imprenditrice nel software e capitalista di impresa, che ha sostenuto la campagna di FFII/Eurolinux con donazioni ed ha viaggiato dalla Svezia a Bruxelles parecchie volte per presenziare ai congressi ed alle riunioni con i membri del Parlamento Europeo:
#nto: Ora quella gente che ha sostenuto di essere contraria ad avere un disastro in stile statunitense, ma ha gradito il progetto di legge soltanto perché consentiva queste cose, dovrà esporsi. Prevedo che un buon numero delle loro società sosterrà che non dobbiamo passare la direttiva com'è ora, perchè abbiamo bisogno di una progetto di legge che ci renda più simili agli Stati Uniti e al Giappone per non fare innervosire i nostri partner commerciali.
#usy: Ora è il momento di chiedere ai politici europei di mostrare coraggio e leadership e votare la direttiva che i cittadini Americani, i governi, le PMI ed Alan Greenspan vorrebbero avere al posto della confusione attuale. Di chiedere a loro di armonizzarsi con l'Europa. I membri del Parlamento Europeo meritano ringraziamenti per i loro sforzi nel capire le conseguenze sociali di questa decisione che ammettiamo essere molto tecnica e difficile. Ciò fino ad ora non era mai accaduto in nessun altro luogo nel mondo. Noi Europei possiamo essere fieri di questo successo politico e spero che i nostri politici condividano questo orgoglio.
#media: Media Contacts
#oWW: Altri contatti verranno forniti su richiesta
#thh: A patent attorney trying to mobilise his profession for backlash in the Council.
#Wca: Patent attorney Horns says that EP decision is %(q:rubbish), based on FFII %(q:misinformation campaign), will be %(q:thrown into the dustbin) by European Commission or Council and, if not, attacked on the basis of TRIPs by friends from US.
#sey: Brussels correspondant Mathew Newman confuses patent lawyer interests with industry interests, attributes limiting amendments to %(q:environmentalists and socialists), extensively quotes EICTA statements.
#Wbi: Patent lawyers of large ICT companies, speaking in the name of a European industry organisation, are very unhappy about the outcome of the Europarl vote
#foh: An informative account of what happened.
#cWi: UK patent lawyer and former EPO examiner Alex Batteson denies competence of the parliament in matters of patent legislation, predicts that European Commission and Council will withdraw directive and entrust %(q:patent experts) from national governments with legislation via the European Patent Organisation.
#vPi: Christian Engström, swedish software developper, refutes statements by UK patent lawyer Alex Batteson who asked that the European Parliament should be stripped of its right to legislate on patent matters, as by voting against software patents it had shown its incompetence.
#rsw: Part II of Engström's refutal of Batteson and assessments of the strengths and weaknesses of parliamentary democracy, as shown in the software patent vote.
#tpe: Dr. Lenz, professor of german and european law in Tokyo, is worried about the attempts of Horns and other patent lawyers to declare themselves %(q:experts) in this matter and deny the competence of the European Parliament, points out that this runs counter to recent principle decisions of the German Constitutional Court.  Moreover Lenz confesses himself guilty of what Horns calls a %(q:misinformation campaign) about the exclusion of software from patentability by the European Patent Convention and expresses doubt about the correctness of Horns's assertions.
#rme: Answer by Hartmut Pilch to Axel Horns in mailing list discussion on the Broersma article
#lvt: Brussels correspondent Paul Meller reports that now the roles of supporters and opponents of the directive have changed.  Quotes Hartmut Pilch and Laura Creighton.
#uoe: Hartmut Pilch responds to PA Axel Horns
#ins: Horns accuses European Parliament of FFII-inspired dilettantism and predicts that Bolkestein, Council and US friends will kill the directive.  Pilch refutes the arguments.  Discussion in German.
#ale: Contiene i risultati dei voti
#lih: Le minaccie di Bolkestein
#Whe: McCarthy 19 febbraio 2003 : Vietare al Parlamento Europeo il suo diritto a definire le regole
#Wub: McCarthy emise già gli stessi avvertimenti a febbraio. L'analisi dell'FFII del suo documento precisò che non avrebbe potuto emetterli se non ci fosse qualcuno nella Commissione Europea che le fornisse supporto. Adesso noi sappiamo chi le ha fornì supporto.
#CWg: Lista di voto McCarthy
#wWs: Questa lista di voto è basata su un compromesso all'interno di PSE. Introduce parecchi emendamenti che sono contrari allo spirito del %(ju:rapporto del JURI sul progetto della direttiva) di McCarthy. Tuttavia soltanto un terzo dei membri del PSE hanno seguito questa lista di voto. Il resto ha generato una lista di voto propria, che è più vicina alle raccomandazioni dell'FFII. McCarthy è stata così completamente emarginata.
#rra: La relatrice del PSE del Regno Unito, la cui la lista di voto pro-brevetto non è stata seguita da nessun gruppo politico nel Parlamento, presenta la sua sconfitta come vittoria, tutto ciò senza dimenticarsi di attaccare la %(q:campagna di disinformazione) lanciata da un gruppo non ben specificato, probabilmente l'%(q:alleanza per il software libero).
#Iur: Archivio novità FFII
#con: Può contenere alcune notizie attuali
#aea: Nel 2002 gli amministratori del Consiglio hanno spinto per la brevettabilità illimitata, anche se secondo le regole procedurali di legislazione dell'EU, non era ancora il loro turno. Come la direzione della Commissione Europea per il mercato interno, il %(q:gruppo di lavoro sulla politica dei brevetti) del Consiglio è un'istituzione su cui il reparto brevetti delle grandi aziende dell'IT può contare. I suoi membri sono sempre disposti ad agire contro le regole scritte dal loro stesso governo, se le lobby dei brevetti richiedono ciò.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: neogeek ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swnplen030924 ;
# txtlang: it ;
# End: ;

