<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#aea: 2002 haben die Patentbeauftragten des Rats auf unbegrenzte Patentierbarkeit gedrängt, obwohl sie laut den Verfahrensregeln des EU-Gesetzgebung gar nicht an der Reihe waren. Genauso wie der Binnenmarkt-Ausschuss der Europäischen Kommission, ist die %(q:Arbeitsgruppe für Patentrecht) des Rates eine Institution auf welche die Patentabteilungen großer IT-Konzerne zählen können. Ihre Mitglieder sind immer dazu bereit, entgegen schriftlicher Anweisungen ihrer eigenen Regierungen zu handeln, falls der Konsens der Patentlobby dies verlangt.

#con: könnte aktuelle Neuigkeiten enthalten

#Iur: FFII News Archive

#rra: Die Berichterstatterin der britischen PSE-Gruppe, deren harte Patent-befürwortende Linie von keiner politischen Gruppe befolgt wurde, stellt ihre Niederlage als Sieg dar, ohne es sich dabei nehmen zu lassen gegen die %(q:Fehlinformations-Kampagne) auszukeilen, die von einer ungenannten Gruppe geführt worden sein soll, vermutlich der %(q:freien Software Allianz).

#wWs: Diese Abstimmungsliste basiert auf einem Kompromiss innerhalb der PSE. Sie führt diverse Änderungsanträge ein, die dem Geist des %(ju:Berichts des Juri zum Entwurf) widersprechen.  Trotzdem hat nur ein Drittel der Abgeordneten aus der PSE sich an diese Liste gehalten. Der Rest hat eine eigene Liste erstellt, die näher an den Empfehlungen des FFII liegt. McCarthy wurde auf diese Weise völlig marginalisiert.

#CWg: Abstimmungsliste von McCarthy

#Wub: McCarthy hat die gleichen Drohungen bereits im Februar geäußert. Die Analyse des FFII zu ihrem Schriftstück zeigt auf, dass sie diese nicht geäußert haben könnte, wenn jemand aus der Europäischen Kommission sie nicht decken würde. Jetzt wissen wir wer das war.

#Whe: McCarthy 03-02-19: Verweigert dem EP sein Recht die Regeln vorzugeben

#lih: Bolkensteins Drohung

#ale: Enthält die Ergebnisse der Abstimmung

#ins: Horns bezichtigt das Europaparlament, dem FFII auf einen dilettantischen und abenteuerlichen Kurs gefolgt zu sein, dem nun die Patentprofis um Bolkestein, im Rat und in der US-Regierung ein Ende bereiten werden, indem sie die Richtlinie abschießen.  Pilch widerlegt die Argumente.

#uoe: Antwort von Hartmut Pilch an PA Axel Horns

#lvt: Brussels correspondent Paul Meller reports that now the roles of supporters and opponents of the directive have changed.  Quotes Hartmut Pilch and Laura Creighton.

#rme: Answer by Hartmut Pilch to Axel Horns in mailing list discussion on the Broersma article

#tpe: Dr. Lenz, professor of german and european law in Tokyo, is worried about the attempts of Horns and other patent lawyers to declare themselves %(q:experts) in this matter and deny the competence of the European Parliament, points out that this runs counter to recent principle decisions of the German Constitutional Court.  Moreover Lenz confesses himself guilty of what Horns calls a %(q:misinformation campaign) about the exclusion of software from patentability by the European Patent Convention and expresses doubt about the correctness of Horns's assertions.

#rsw: Part II of Engström's refutal of Batteson and assessments of the strengths and weaknesses of parliamentary democracy, as shown in the software patent vote.

#vPi: Christian Engström, swedish software developper, refutes statements by UK patent lawyer Alex Batteson who asked that the European Parliament should be stripped of its right to legislate on patent matters, as by voting against software patents it had shown its incompetence.

#cWi: UK patent lawyer and former EPO examiner Alex Batteson denies competence of the parliament in matters of patent legislation, predicts that European Commission and Council will withdraw directive and entrust %(q:patent experts) from national governments with legislation via the European Patent Organisation.

#foh: An informative account of what happened.

#Wbi: Patent lawyers of large ICT companies, speaking in the name of a European industry organisation, are very unhappy about the outcome of the Europarl vote

#sey: Brussels correspondant Mathew Newman confuses patent lawyer interests with industry interests, attributes limiting amendments to %(q:environmentalists and socialists), extensively quotes EICTA statements.

#Wca: Patent attorney Horns says that EP decision is %(q:rubbish), based on FFII %(q:misinformation campaign), will be %(q:thrown into the dustbin) by European Commission or Council and, if not, attacked on the basis of TRIPs by friends from US.

#thh: A patent attorney trying to mobilise his profession for backlash in the Council.

#oWW: Weitere Ansprechpartner werden auf Nachfrage genannt

#usy: Jetzt ist die Zeit gekommen, europäische Politiker aufzufordern Mut zu zeigen und eine weltweite Führungsrolle einzunehmen, indem sie für die Richtline stimmen, die sich die amerikanischen Bürger, Regierung, KMUs und Alan Greenspan anstatt des jetzigen Schlamassels wünschen würden. Sollen sie sich an Europa anpassen. Die Abgeordneten des Europäischen Parlaments verdienen Dank für ihre Bemühungen, die sozialen Konsequenzen dieser zugegebenermaßen schwierigen technischen Entscheidung zu begreifen. Das ist bisher noch nirgendwo anders in der Welt passiert. Wir Europäer können stolz auf diese politische Errungenschaft sein, und ich hoffe unsere Politiker teilen diesen Stolz.

#nto: Jetzt werden die Leute, die behauptet haben ein Schlamassel nach US-Vorbild vermeiden zu wollen, aber in Wirklichkeit den Entwurf nur deswegen mochten weil er genau sowas ermöglicht hätte, ihr wahres Gesicht zeigen müssen. Ich wage die Vorhersage dass ein guter Teil davon behaupten wird, dass wir es in der jetzigen Form nicht durchsetzen dürfen, weil wir ein Gesetz brauchen was uns den USA und Japan ähnlicher macht, um unsere Handelspartner nicht zu verärgern.

#Cpa: Laura Creighton, eine Softwareunternehmerin und Venture-Kapitalgeberin welche die Kampagne von FFII/Eurolinux mit Spenden unterstützt hat, und mehrmals von Schweden nach Brüssel gereist ist um sich an Konferenzen und Treffen mit Abgeordneten zu beteiligen, sagt:

#cPg: Die Richtlinie wird weiteren Beratungen des Ministerrates standhalten müssen, die eher informell und somit weniger öffentlich sind als Vorgänge im Parlament. In der Vergangenheit hat der Ministerrat Entscheidungen über Patentpolitik seiner %(q:Arbeitsgruppe für Patentpolitik) überlassen, die aus Patentrechtsexperten besteht, welche auch im Verwaltungsrat des Europäischen Patentamts sitzen. Diese Gruppe hat sich hervorgetan als einer der entschiedensten Befürworter grenzenloser Patentierbarkeit in Europa, einschließlich von Programmansprüchen.

#iml: Wenn über 78 Änderungsanträge innerhalb von 40 Minuten abgestimmt wird, sind jedoch einige Verwirrungen nicht zu vermeiden: %(q: Die Erwägungen wurden nicht gründlich abgeändert. Eine davon behauptet immer noch, dass Algorithmen patentierbar sind wenn sie ein technisches Problem lösen.), bemerkt Jonas Maebe, belgischer Vertreter des FFII, der zur Zeit im Europäischen Parlament arbeitet. %(q:Aber wir haben alle Zutaten für eine gute Richtlinie. Wir konnten die grobe Form vorgeben. Die Nachbearbeitung kann jetzt folgen. Der Geist der Europäischen Patentübereinkunft wurde zu 80% bestätigt, und das Parlament ist in einer guten Ausganglage die verbleibenden Widersprüche in der zweiten Lesung auszubessern.)

#mec: %(q:Mit den neuen Vorkehrungen in Artikel 2 ist eine computerimplementierte Erfindung kein Trojanisches Pferd mehr, sondern eine Waschmaschiene), erklärt Erik Josefsson von der SSLUG und dem FFII, der die schwedischen Abgeordneten in den letzten Wochen über die Richtlinie beraten hat. Dass die Mehrheiten für die angenommenen Änderungsanträge Unterstützung aus sehr unterschiedlichen politischen Gruppen hatten, spiegelt die beschwerliche politische Debatte wieder, die vorher einen zweifachen Aufschub erzwungen hatte.

#neo: %(q:Der Richtlinientext mit den Änderungen des Parlaments schließt Softwarepatente klar aus!  Er ist in sich unglaublich konsistent. Ich denke wir haben diese Woche etwas wunderbares erreicht), begeisterte sich James Herald, ein Mitglied der Arbeitsgruppe zu Softwarepatenten des FFII/Eurolinux, als er die angenommenen Änderungsanträge zu einer %(pr:konsolidierten Version) zusammensetzte.

#ifu: Am Tag vor der Abstimmung hatte der Beauftragte der Europäischen Kommission, Fritz Bolkenstein, damit %(fb:gedroht) die Kommission und der Rat würden die Richtlinie zurückziehen und die Fragestellung an die nationalen Patentbeauftragten im Ausschuss des Europäischen Patentamts (EPA) zurückverweisen, falls das Parlament für die Änderungsanträge stimmen sollte, die es heute angenommen hat. %(q:Es wird sich zeigen ob die Europäische Kommission tatsächlich an einer %(q:Harmonisierung und Klarstellung) interessiert ist, oder nur Patentinhaber-Interessen durchsetzen wollte), sagt Hartmut Pilch, Vorsitzender des FFII.  %(q:Das ist jetzt auch unsere Richtlinie. Wir müssen dem Europäischen Parlament helfen sie zu verteidigen.)

#media: Ansprechpartner für Medien

#descr: In der Abstimmung am 24. September hat das Europäische Parlament die Richtlinie über %(q:Patentierbarkeit computerimplementierter Erfindungen) mit einer Reihe von Änderungsanträgen angenommen, welche deutlich die Nicht-Patentierbarkeit von Programm- und Geschäftslogik unterstreicht, und freie Veröffentlichung sowie Interoperabilität sichert.

#title: EU-Parlament stimmt für echte Begrenzung der Patentierbarkeit

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: obudden ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnplen030924 ;
# txtlang: de ;
# multlin: t ;
# End: ;

