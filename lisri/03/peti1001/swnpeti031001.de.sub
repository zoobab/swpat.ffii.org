\begin{subdocument}{swnpeti031001}{Parlament nimmt Petitionen gegen Softwarepatente an}{http://swpat.ffii.org/chronik/03/peti1001/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org\\deutsche Version 2003/10/01 von Christian Cornelsson\footnote{http://www.cs.tu-berlin.de/~ccorn/opinions/swpat/}}{Bei der Anh\"{o}rung des Petitionsausschusses im Parlament gestern abend um 18:00 Uhr ersuchte Anthony Howard, ein fr\"{u}herer Angestellter der britischen Patentbeh\"{o}rde, der mit dem Softwarepatent-Richtlinienprojekt der DG Binnenmarkt unter Frits Bolkestein bei der Europ\"{a}ischen Kommission befasst ist, das Parlament darum, die beiden vorgestellten Petitionen gegen Softwarepatente, die von einer Viertelmillion europ\"{a}ischer B\"{u}rger unterzeichnet worden sind, abzuweisen.  Sehr wenige MdEPs nahmen an der abendlichen Sitzung teil, und unter denen, die teilnahmen, waren bekannte Softwarepatent-Hardliner des Rechtsausschusses (JURI), die Howards Aufruf aggressiv unterst\"{u}tzten.  Allerdings folgte der Petitionsausschuss ihren Aufrufen nicht.  Die Annahme der Petitionen bedeutet, dass der Rechtsausschuss, welcher im Juni 2003 gegen jede echte Begrenzung der Patentierbarkeit pl\"{a}dierte und am 24. September von der Vollversammlung \"{u}berstimmt wurde, eine weitere Gelegenheit erh\"{a}lt, seine Position zu erl\"{a}utern.}
\begin{sect}{detal}{details}
Zuf\"{a}lligerweise debattierte der Petitionsausschuss gestern die zwei Petitionen \"{u}ber die Patentierbarkeit von Software, weniger als eine Woche nach der Abstimmung in erster Lesung des Richtlinienvorschlags.  Diese zwei Petitionen sind:

\begin{itemize}
\item
Die Eurolinux-Petition, unterzeichnet von einer Viertelmillion europ\"{a}ischer B\"{u}rger und Software-Berufst\"{a}tiger, verf\"{u}gbar auf petition.eurolinux.org/\footnote{http://petition.eurolinux.org/}

\item
Die Petition von 33 f\"{u}hrenden europ\"{a}ischen Computer- und Informatik-Wissenschaftlern, verf\"{u}gbar auf www.upgrade-cepis.org/issues/2003/3/up4-3Petition.pdf\footnote{http://www.upgrade-cepis.org/issues/2003/3/up4-3Petition.pdf}
\end{itemize}

Zun\"{a}chst wurde die europ\"{a}ische Kommission aufgefordert, ihre Bemerkungen abzugeben.  Ihr Sprecher war Anthony Howard\footnote{http://swpat.ffii.org/akteure/ahoward/index.en.html}, der Verantwortliche f\"{u}r den Entwurf des Richtlinienvorschlags in der zust\"{a}ndigen Abteilung der DG Binnenmarkt.  Es war ihm sichtlich unangenehm, er machte keine Bemerkungen zur Sache und ersuchte den Ausschuss, beide Petitionen f\"{u}r unzul\"{a}ssig zu erkl\"{a}ren, weil sie in einen laufenden Gesetzgebungsprozess eingreifen w\"{u}rden.

Dann wurden den Repr\"{a}sentanten der Petition\"{a}re je f\"{u}nf Minuten gegeben, um ihre Sichtweise darzulegen.  Bernard Lang vertrat Eurolinux, und Philippe Aigrain sprach f\"{u}r die Wissenschaftler\footnote{http://swpat.ffii.org/chronik/03/epet0929/aigrain/index.de.html}.

Beide Redner dankten dem Parlament f\"{u}r dessen Entscheidung und betonten die Wichtigkeit, deren Zielrichtung und Konsistenz in den n\"{a}chsten Schritten des Gesetzgebungsverfahrens zu wahren.

Daraufhin wurden die Mitglieder des europ\"{a}ischen Parlaments (MdEPs) zur Debatte aufgefordert.  Willi Rothley\footnote{http://swpat.ffii.org/akteure/wrothley/index.de.html} (PSE-DE) intervenierte mit \"{u}berraschender Aggressivit\"{a}t.  Da Bernard Lang erw\"{a}hnt hatte, dass die Petition\"{a}re angesichts der Abstimmung stolz auf die europ\"{a}ische Demokratie gewesen w\"{a}ren, warnte Herr Rothley davor, dass nach Hochstimmungen der Katzenjammer komme, und dass die zweite Lesung ganz anders ausfallen k\"{o}nne.  Auch er bat den Ausschuss, die Petitionen f\"{u}r ung\"{u}ltig zu erkl\"{a}ren.  Herr Rothley ist Vizepr\"{a}sident des JURI-Komitees und kein Mitglied des Petitionsausschusses, aber alle MdEPs sind eingeladen, an Versammlungen teilzunehmen und Ansichten \"{u}ber Gegenst\"{a}nde ihres Interesses auszudr\"{u}cken.

Frau Janelly Fourtou\footnote{http://swpat.ffii.org/akteure/jfourtou/index.en.html} (PPE-FR) hielt auf gleicher Linie dagegen.  Auch sie pl\"{a}dierte daf\"{u}r, die Petitionen als unzul\"{a}ssig zu erachten.

Jan Dhaene (Gr\"{u}ne, Belgien) und Marco Cappato (Unabh\"{a}ngige, Italien) sprachen sich f\"{u}r die Petitionen aus und sagten, nicht nur seien sie zul\"{a}ssig, sondern der Petitionsausschuss sollte sich ihrer selber annehmen, da B\"{u}rger und Entwickler sehr wahrscheinlich unter der mi{\ss}br\"{a}uchlichen Vergabe von Softwarepatenten durch das EPA zu leiden haben w\"{u}rden.

Der Vorsitzende der Versammlung erkl\"{a}rte ohne jeglichen Zweifel, dass die Petitionen zul\"{a}ssig seien, da das Recht, \"{u}ber eine gerade in der Gesetzgebung befindliche Angelegenheit zu petitionieren, vom EU-Vertrag anerkannt wird.  Er entschied au{\ss}erdem, dass, weil es sich um eine gesetzgeberische Angelegenheit handele und es die klare Absicht der Petition\"{a}re sei, sich in diese Debatte einzuschalten, es nicht am Petitionsausschuss sei, die Sachdebatte zu f\"{u}hren, sondern die Petition einfach an den f\"{u}r diese Sachfragen zust\"{a}ndigen Ausschuss \"{u}bergeben werde, in diesem Fall JURI.
\end{sect}

\begin{sect}{media}{Ansprechpartner f\"{u}r Medien}
\begin{description}
\item[E-Post:]\ media at ffii org
\item[Telefon:]\ FFII M\"{u}nchen (Deutsch, Englisch und Franz\"{o}sisch): 0049/89/18979927
Benjamin Henrion (Franz\"{o}sisch und Englisch): 0032/498/292771 oder 0032/10/454779
Jonas Maebe (Niederl\"{a}ndisch und Englisch): +32-485-36-96-45
Dieter Van Uytvanck (Niederl\"{a}ndisch und Englisch): +32-499-16-70-10
Erik Josefsson (Schwedisch und Englisch): +46-707-696567
Alex Macfie (Englisch): +44 7901 751753
Joaquim Carvalho (Portugiesisch und Englisch): +35-1-93-6169633
Weitere Ansprechpartner werden auf Nachfrage genannt
\end{description}
\end{sect}

\begin{sect}{url}{Permanente Netzadresse dieser Mitteilung}
http://swpat.ffii.org/chronik/03/peti1001/index.de.html
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Europarl 2003-08-24: Ge\"{a}nderte Softwarepatent-Richtlinie\footnote{http://swpat.ffii.org/papiere/europarl0309/index.de.html}}}

\begin{quote}
Konsolidierte Version der wesentlichen Bestimmungen (Art 1-6) der ge\"{a}nderten Richtlinie ``\"{u}ber die Patentierbarkeit computer-implementierter Erfindungen'', f\"{u}r die das Europ\"{a}ische Parlament am 24. September 2003 gestimmt hat.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

