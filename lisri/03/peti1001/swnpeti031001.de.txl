<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#owb: Der Vorsitzende der Versammlung erklärte ohne jeglichen Zweifel, dass die Petitionen zulässig seien, da das Recht, über eine gerade in der Gesetzgebung befindliche Angelegenheit zu petitionieren, vom EU-Vertrag anerkannt wird.  Er entschied außerdem, dass, weil es sich um eine gesetzgeberische Angelegenheit handele und es die klare Absicht der Petitionäre sei, sich in diese Debatte einzuschalten, es nicht am Petitionsausschuss sei, die Sachdebatte zu führen, sondern die Petition einfach an den für diese Sachfragen zuständigen Ausschuss übergeben werde, in diesem Fall JURI.

#Wtf: Jan Dhaene (Grüne, Belgien) und Marco Cappato (Unabhängige, Italien) sprachen sich für die Petitionen aus und sagten, nicht nur seien sie zulässig, sondern der Petitionsausschuss sollte sich ihrer selber annehmen, da Bürger und Entwickler sehr wahrscheinlich unter der mißbräuchlichen Vergabe von Softwarepatenten durch das EPA zu leiden haben würden.

#nWW: Frau %(JF) hielt auf gleicher Linie dagegen.  Auch sie plädierte dafür, die Petitionen als unzulässig zu erachten.

#aWs: Daraufhin wurden die Mitglieder des europäischen Parlaments (MdEPs) zur Debatte aufgefordert.  %(WR) intervenierte mit überraschender Aggressivität.  Da Bernard Lang erwähnt hatte, dass die Petitionäre angesichts der Abstimmung stolz auf die europäische Demokratie gewesen wären, warnte Herr Rothley davor, dass nach Hochstimmungen der Katzenjammer komme, und dass die zweite Lesung ganz anders ausfallen könne.  Auch er bat den Ausschuss, die Petitionen für ungültig zu erklären.  Herr Rothley ist Vizepräsident des JURI-Komitees und kein Mitglied des Petitionsausschusses, aber alle MdEPs sind eingeladen, an Versammlungen teilzunehmen und Ansichten über Gegenstände ihres Interesses auszudrücken.

#Pfs: Beide Redner dankten dem Parlament für dessen Entscheidung und betonten die Wichtigkeit, deren Zielrichtung und Konsistenz in den nächsten Schritten des Gesetzgebungsverfahrens zu wahren.

#itr: Dann wurden den Repräsentanten der Petitionäre je fünf Minuten gegeben, um ihre Sichtweise darzulegen.  Bernard Lang vertrat Eurolinux, und Philippe Aigrain %(pt:sprach für die Wissenschaftler).

#tru: Zunächst wurde die europäische Kommission aufgefordert, ihre Bemerkungen abzugeben.  Ihr Sprecher war %(AH), der Verantwortliche für den Entwurf des Richtlinienvorschlags in der zuständigen Abteilung der DG Binnenmarkt.  Es war ihm sichtlich unangenehm, er machte keine Bemerkungen zur Sache und ersuchte den Ausschuss, beide Petitionen für unzulässig zu erklären, weil sie in einen laufenden Gesetzgebungsprozess eingreifen würden.

#smc: Die Petition von 33 führenden europäischen Computer- und Informatik-Wissenschaftlern, verfügbar auf %(URL)

#tpl: Die Eurolinux-Petition, unterzeichnet von einer Viertelmillion europäischer Bürger und Software-Berufstätiger, verfügbar auf %(URL)

#Wbc: Zufälligerweise debattierte der Petitionsausschuss gestern die zwei Petitionen über die Patentierbarkeit von Software, weniger als eine Woche nach der Abstimmung in erster Lesung des Richtlinienvorschlags.  Diese zwei Petitionen sind:

#descr: Bei der Anhörung des Petitionsausschusses im Parlament gestern abend um 18:00 Uhr ersuchte Anthony Howard, ein früherer Angestellter der britischen Patentbehörde, der mit dem Softwarepatent-Richtlinienprojekt der DG Binnenmarkt unter Frits Bolkestein bei der Europäischen Kommission befasst ist, das Parlament darum, die beiden vorgestellten Petitionen gegen Softwarepatente, die von einer Viertelmillion europäischer Bürger unterzeichnet worden sind, abzuweisen.  Sehr wenige MdEPs nahmen an der abendlichen Sitzung teil, und unter denen, die teilnahmen, waren bekannte Softwarepatent-Hardliner des Rechtsausschusses (JURI), die Howards Aufruf aggressiv unterstützten.  Allerdings folgte der Petitionsausschuss ihren Aufrufen nicht.  Die Annahme der Petitionen bedeutet, dass der Rechtsausschuss, welcher im Juni 2003 gegen jede echte Begrenzung der Patentierbarkeit plädierte und am 24. September von der Vollversammlung überstimmt wurde, eine weitere Gelegenheit erhält, seine Position zu erläutern.

#title: Parlament nimmt Petitionen gegen Softwarepatente an

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: ccornels ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnpeti031001 ;
# txtlang: de ;
# multlin: t ;
# End: ;

