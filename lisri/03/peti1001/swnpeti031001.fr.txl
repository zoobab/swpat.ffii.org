<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#owb: Le Président de séance déclara qu'il était indiscutable que les pétitions étaient éligibles, puisque le droit des citoyens européens de pétitionner sur des sujets pour lesquels un processus législatif est en cours est reconnu par le traité. Il décida également que s'agissant d'une matière législative, et l'intention des pétitionnaires étant clairement d'intervenir dans ce débat, il n'appartient pas au Comité des Pétitions de discuter lui-même du fond, mais simplement de transmettre la pétition au comité compétent, à savoir le comité JURI.

#Wtf: Jan Dhaene (Verts, Belgique) et Marco Cappato (Indépendants, Italie) intervinrent en soutien aux pétititons, défendant que non seulement elles étaient éligibles, mais aussi que le Comité des Pétitions pouvait débattre du fond, car les citoyens et développeurs européens souffrent des abus de l'OEB en matière de délivrance de brevets.

#nWW: Madame Fourtou (PPE-DE, France) intervint sur la même ligne. Elle demanda aussi que les pétitions soient considérées comme inéligibles.

#aWs: Les parlementaires sont alors invités à intervenir. %(WR) fit une intervention d'une rare agressivité. Comme Bernard Lang avait souligné la fierté ressentie par les pétitionnaires à l'égard de la démocratie européenne et le sentiment festif qui l'avait traversé, Willy Rothley avertit qu'au sentiment festif succède la gueule de bois, et qu'il donnait rendez-vous aux pétitionnaires en seconde lecture. Il demanda également au Comité de considérer la pétition comme inéligible. Mr. Rothley, vice-président de la commission JURI, n'est pas membre du comité des pétitions, mais les parlementaires même non membres peuvent être présents et intervenir sur tout sujet de leur intérêt.

#Pfs: Les deux interventions ont remercié le parlement de son vote et ont souligné l'importance d'en maintenir la cohérence et les orientations dans la suite du processus législatif.

#itr: Les représentants des pétitionnaires ont alors 5 minutes pour faire valoir leur point de vue. %(BL) représentait Eurolinux, et %(PA), les scientifiques.

#tru: La Commission européenne est d'abord invitée à formuler ses observations. C'est %(AH), en charge de la rédaction de la directive dans l'unité concernée de la DG Marché Intérieur qui intervint. Il le fit avec une gène visible, ne formulant aucune observation sur le fond, et demandant au comité de considérer les pétitions comme inéligibles parce qu'intervenant dans le processus législatif en cours.

#smc: La pétition de 33 scientifiques européens renommés en informatique, accessible à %(URL)

#tpl: La pétition d'Eurolinux signée par environ 250,000 citoyens européens (%(URL))

#Wbc: Par un hasard du calendrier, le Comité des Pétitions s'est trouvé à débattre hier des deux pétitions sur le sujet de la brevetabilité des logiciels moins d'une semaine après son vote en première lecture sur le project de directive. Il s'agit de:

#descr: Hier soir à 18 heures, à l'audience du Comité des Pétitions du Parlement, Anthony Howard, un ex-employé de l'Office des brevets britannique, actuellement en charge du projet de directive sur les brevets logiciels pour la DG du Marché intérieur, sous la direction du Commissaire européen Frits Bolkestein, a demandé au Parlement de rejeter les deux pétitions contre les brevets logiciels, signées par 250000 citoyens européens. Très peu de MPE assistèrent à la session tardive; parmi eux se trouvaient des partisans acharnés des brevets logiciels, faisant partie de la Commission des Affaires juridiques (JURI), qui soutinrent avec insistance la demande de Howard. Le Comité des Pétitions n'a cependant pas tenu compte de leur doléances. L'acceptation de la pétition implique que la Commission des Affaires juridiques, qui a voté contre toute limitation réelle de la brevetabilité en juin 2003, mais qui a été désavouée par le vote en session plénière du 24 septembre, reçoit une nouvelle opportunité de expliquer sa position.

#title: Le Parlement accepte les pétitions contre les brevets logiciels

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: gharfang ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnpeti031001 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

