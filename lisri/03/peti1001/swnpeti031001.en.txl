<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#owb: The meeting Chairman declared that, beyond any possible doubt, the petitions were eligible, because the right to petition on a matter currently under legislation is recognised by the Treaty.  However he also decided that, since the intention of petitioners was clearly to intervene in pending legislation, the Petition Commitee should not debate the petitions on substance, but simply transfer them to the committee competent on substance, in this case JURI.

#Wtf: Jan Dhaene (Green, Belgium) and Marco Cappato (NI, Italy) spoke in favour of the petitions, saying that not only were they eligible, but the Committee of Petitions should debate them on substance, as citizens and developers are likely to suffer from the abusive granting of software patents by the EPO.

#nWW: Mrs. %(JF) supported Mr. Rothley's line.  She also asked for the petitions to be considered ineligible.

#aWs: Members of the European Parliament (MEPs) were then invited to debate.  %(WR) intervened in a remarkably aggressive tone.  As Bernard Lang had mentioned that petitioners had been proud of European democracy at the occasion of the vote, and that he felt then in a kind of festive mood, Mr. Rothley warned that after festive moods come hangovers, and that the second reading might be very different. He also asked the committee to declare the petitions ineligible. Mr. Rothley is Vice-President of the JURI committee and not a member of the Committee of Petitions, but all MEPs are invited to attend meetings and express views on subjects of their interest.

#Pfs: Both thanked the Parliament for its vote, and stressed the importance of keeping its consistency and orientation in the next steps of the legislative process.

#itr: The representatives of petitioners were then given 5 minutes each to express their viewpoints. Bernard Lang represented Eurolinux, and Philippe Aigrain %(pt:spoke for the scientists).

#tru: The European Commission was first invited to formulate its observations. The speaker was %(AH), the person in charge of drafting the directive proposal in the responsible unit of the Internal Market DG. He was visibly uncomfortable, made no observations on substance, and asked the committee to declare both petitions ineligible because they were intervening in an ongoing legislative process.

#smc: The petition signed by 33 leading European computer and software scientists accessible at: %(URL)

#tpl: The Eurolinux petition signed by a quarter million European citizens and software professionnals accessible at %(URL)

#Wbc: By coincidence, the Petition Committee debated yesterday  the 2 petitions on software patentability, less than a week after the first reading vote on the directive proposal. These 2 petitions are:

#descr: At the Hearing of the Petition Committee in the Parliament yesterday evening at 18:00, Anthony Howard, a former employee of the UK Patent Office who is handling the Software Patent Directive Project for DG Internal Market under Frits Bolkestein at the European Commission, asked the Parliament to discard the two petitions against software patents, signed by a quarter million european citizens, which were presented to it.  Very few MEPs attended the evening session, and among those who attended were prominent software patentability advocates from the Legal Affairs Commission (JURI) who vocally supported Howard's call.  However the Petition Committee did not follow their requests.  Acceptance of the petition will mean that the Legal Affairs Commission, which voted against meaningful limits on patentability in June 2003 and was overruled by the plenary vote on 24th of September, will receive another opportunity to explain its position.

#title: Parliament Accepts Petitions Against Software Patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnpeti031001 ;
# txtlang: en ;
# multlin: t ;
# End: ;

