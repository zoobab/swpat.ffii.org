\begin{subdocument}{msfat031209}{Microsoft wants money for its File System: Interoperability in Danger}{http://swpat.ffii.org/log/03/msfat1209/msfat031209.de.html}{Arbeitsgruppe\\swpatag@ffii.org\\deutsche Version 2003/12/09 von PILCH Hartmut\footnote{http://www.ffii.org/~phm}}{Microsoft has published licensing terms for a series of patents which cover the FAT file format.  These license terms exclude free software such as the GNU/Linux operating system.  Several of the patents on which Microsoft's terms are based have also been granted by the European Patent Office.  However under the European Patent Convention, as confirmed by the recent vote of the European Parliament, these patents are invalid.  Moreover, the European Parliament's Art 6a assures that the use of a patented technique for the purpose of interoperation is not a patent infringement.}
\begin{sect}{detal}{details}
As discussed with entertaining sarcasm at LST, Microsoft is kicking off an new IP-licencing policy by highlighting its patents on the look-up scheme for translating long-form FAT filenames into DOS 8.3 filenames.

See also Microsoft's PR\footnote{http://www.microsoft.com/mscorp/ip/tech/fat.asp}.

Two of the four patents on which Microsoft's royalty demands are based have also been granted by the European Patent Office\footnote{http://swpat.ffii.org/patente/txt/ep/swpiktxtep.de.html}:

\begin{itemize}
\item
{\bf {\bf Common name space for long and short filenames\footnote{../../../patente/txt/ep/0618/540}}}

\begin{quote}
US6286013\footnote{http://v3.espacenet.com/textdoc?IDX=US5745902}
\end{quote}
\filbreak

\item
{\bf {\bf Multiple file name referencing system\footnote{../../../patente/txt/ep/0578/205}}}

\begin{quote}
US6286013\footnote{http://v3.espacenet.com/textdoc?IDX=US6286013}
\end{quote}
\filbreak
\end{itemize}

Hartmut Pilch, president of FFII, explains

\begin{quote}
{\it According to European Patent Convention, as reconfirmed recently by the European Parliament, we should have nothing to fear in Europe.  The patents which the EPO granted are clearly invalid, because the claimed subject matter is confined to rules for computation on conventional data processing equipment, objectionable either as a \emph{program for computers} or as an \emph{intellectual method}, \emph{mathematical method} or a \emph{method of presenting information}.}

{\it Moreover the use that Microsoft intends is also objectionable on competition grounds.  Article 6a of the European Parliament's amendments makes this explicit: the use of a patented technique for the sole purpose of interoperation (conversion between data representation formats) should never be considered a infringement.}

{\it Recently we have been observing a clear tendency at Microsoft to embark on extreme and unethical uses of patents, similar to those observed at IBM earlier.  IBM's former IP manager Marshall Phelps, known for the }
\end{quote}

Bernhard Kaindl, a german software developper and active member of FFII, comments:

\begin{quote}
{\it FAT has been developed without ever hearing of a patent and now that everybody adopted it because it was no cost, it reduces funds for new FAT devices by 0,25 EUR per unit.}

{\it The FAT case illustrates clearly what is also true for all other software-related patents that I have come across so far: they serve to promote further patents and not real software innovation.}

{\it The patenting activity combined with the much too long life span of patents in the software field results in patent thickets which cause trouble for new innovations.}

{\it Moreover, typical patent licensing policies such as those of Microsoft's FAT cap the license fee at a fixed amount, thereby putting smaller companies at a disadvantage and completely excluding free software.}
\end{quote}

Here for your reading delight is a new text\footnote{http://www.microsoft.com/presspass/features/2003/dec03/12-03ExpandIPQA.asp} by Microsoft's IP leader Marshall Phelps on the importance of patent protection

\begin{quote}
{\it PressPass: There are those who say that patents stifle innovation. How would you respond to them?}

{\it Phelps: The free and open exchange of ideas is an important ingredient in creating and fostering an innovative environment, but it's not the only ingredient. Motivation and incentive also are critical to sustained and ongoing innovation, especially in the context of large, complex systems that software programs often must implement. History shows that nations and countries with robust IP laws to protect the creative works of authors and inventors are at an advantage. Strong IP laws promote innovation and differentiation�both of which ultimately benefit the customer. Patents are part of the U.S. Constitution, and firms such as IBM have been patenting software-related inventions for over 30 years. The fact is, the last 30 years have seen unprecedented innovation in the IT sector.}

{\it PressPass: Why is it important to protect IP?}

{\it Phelps: The spirit of innovation is something we value passionately. Microsoft firmly believes that innovation is the fundamental component of progress and growth in the IT industry. We believe it is the essential foundation for the delivery of great new products and that it fuels ongoing R\&D.}

{\it Companies like Microsoft engage in applied research to develop products that advance the state of technology. In turn, that generates jobs, profits and tax revenues that boost the economy, which, as part of the process, funds additional research. Companies also contribute the results of innovation directly into the larger body of technical knowledge. If software companies are not compensated for the investments they make in R\&D, the cycle of sustainable innovation is disrupted. When that happens, the overall health of the software industry is jeopardized.}

{\it Microsoft is committed to a business model that protects the IP rights in software, just like any other technology, and ensures the continued vitality of an independent software sector that generates revenue and will sustain ongoing R\&D. Our commitment to innovation is reflected in our industry leading annual budget for R\&D.}

{\it PressPass: Do you feel the IT industry as a whole benefits from IP licensing?}

{\it Phelps: Absolutely. A healthy, fluid intellectual property marketplace is essential for a strong IT ecosystem. The revenues generated by IP licenses make it possible for companies like Microsoft to consistently invest in R\&D. The industry is continually adopting the innovations that are a direct result of R\&amp;D spending and this leads to an increase in deployment. When you have increased deployment, the opportunity for further innovation downstream becomes available -- by both the originating company and potential licensees. This downstream innovation is what generates new IP, which generates new licensing, which generates new revenue. By utilizing this revenue for additional R\&amp;D, it furthers the cycle of innovation and that, of course, is a lasting benefit to the industry.}
\end{quote}

Note that Phelps refers to patents only once.  What he says about ``IP'' is largely a general consensus, provided that it is understood that ``IP'' in the context of software means ``copyright and knowhow''.  In other words, even Phelps is not able to advance any argument in favor of software patents.
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Microsoft and Patents\footnote{http://swpat.ffii.org/akteure/microsoft/index.en.html}}}

\begin{quote}
Microsoft Corporation grew large and successful without patents, relying instead on copyright.  In 1991 , Bill Gates warned that software patents could lead the software industry to a standstill but could also be very useful for defending monopoly positions.  At the USPTO hearings of 1994, Microsoft was the only software company that argued in favor of software patentability.   Microsoft has been involved in promoting software patentability in Europe.  Simultaneously Microsoft's has invested ample ressources into a campaign to dissuade governments and corporations from using free operating system.  Pointing out the insecurity caused to Free Software by patents and contributing to this insecurity by occasional threats has become an important part of the campaign.  Microsoft appears to be a favorite victim of patent attacks.  In June 2003, Microsoft hired formar IBM patent strategist Marshall Phelps, the father of the ``IBM tax'', to embark on an aggressive rent-extraction program.  Phelps also announced to step up efforts to campaign for software patentability in Europe.  Much of this campaigning appears to have been done indirectly by Microsoft partners.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

