<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#descr: Was der FFII im Jahre 2003 über staatlich gewährte Erfindungsmonopole und ihre missbräuchliche Ausweitung auf rechnergestütztes Denken, Rechnen, Organisieren und Formulieren zu berichten hatte

#title: Logikpatentnachrichten 2003

#psn: MdEP schlagen 106 Änderungen zur Softwarepatent-Richtlinie vor

#aft: Mitglieder aller Gruppen des Europäischen Parlaments haben insgesamt 120 Änderungsanträge zum Softwarepatent-Richtlinienvorschlag eingericht.  So gut wie alle Anträge zeigen eine Absicht, Softwarepatente meiden oder in ihrer Wirkung beschränken zu wollen. Einige erreichen dies, andere nicht.  Keine Änderungsantrag kann das Ziel für sich alleine erreichen, denn sie sind in einen langen Richtlinientext der Europäischen Kommission eingebettet, der auf grenzenlose Patentierbarkeit zielt und dieses Ziel mit zahlreichen redundanten Bestimmungen mehrfach absichert.

#tWM: Brief an Europarlamentarier

#leP: Dieser letzte Appell wurde von FFII/Eurolinux per E-Post und Fax an alle Mitglieder des Europäischen Parlamentes verschickt.

#hKr: Deutsche Regierung setzt Druck für maximale Patentierbarkeit fort, ignoriert Stimme des Höchsten Patentrichters

#uWe: Der FFII hat einen Artikel aus einer Serie von mehreren Artikeln von Richter Melullis in Auszügen wiederveröffentlicht und bewertet. Leider hört das in Berlin in Patentsachen federführende Justizministerium (BMJ) nicht auf Melullis und Kollegen, seitdem diese eine von führenden Großkonzern-Patentanwäf EU-Ebene übt die Bundesregierung weiterhin Druck aus, um die letzten Reste der Publikationsfreiheit (Art 4) und Interoperabilität (Art 6a) aus der Richtlinie zu entfernen.

#dcc: Der Softwarepatent-Richtlinienvorschlag ist ein %(q:Wolf im Schafspelz).  Wir haben im Detail dokumentiert, was unter %(q:technischer Beitrag) & Co wirklich zu verstehen ist.

#nsW: Gates: Linux verletzt zahlreiche Patente, SCO ist erst der Anfang

#rrc: Erste öffentliche Antwort von McCarthy

#rrW: Arlene McCarthy, Mitglied des Europäischen Parlaments für die englische Labour-Partei und Berichterstatterin des JURI-Ausschusses für den Richtlinienentwurf zur Patentierbarkeit von Software, hat in der englischen Zeitung %(q:The Guardian) zum ersten Mal direkt auf die Argumente von Kritikern geantwortet.  Jedoch waren ihre Antworten ausweichend, irreführend und polemisch.  McCarthy wiederholt demagogische Argumente, deren Unwahrheit ihr seit mindestens einem Jahr bekannt sein musste, und schreckt noch nicht einmal vor offensichtlichen Lügen im engsten Sinne zurück.  So behauptet McCarthy, sie habe einen Artikel in die Richtlinie eingeführt, der die Interoperabilität schütze.  Wir analysieren McCartys Aussagen und den politischen Kontext ihres Briefes.

#iWP: Petition beim Europäischen Parlament eingereicht

#ftl: %(AD) von %(AEL) hat die z.T. recht aufwändigen Prozeduren am Europäischen Parlament absolviert und die Bestätigung erhalten, dass die %(EP) mit ihren 140000 virtuellen Unterschriften eingereicht worden ist.  Jetzt liegt es am Petitionsausschuss des Parlamentes, darauf zu antworten.  Eingetragene Kontaktpersonen für die Petition sind Alexandre Dulaunoy für AEL und Hartmut Pilch für FFII.

#noL: Belgische Gesellschaft für Elektronische Freiheiten

#uot: Zeitplan für Softwarepatent-Diskussionen am Europarl

#WWy: 1ste JURI-Diskussion über McCarthy-Bericht

#mse: Der Rechtsausschus des Europaparlaments diskutierte erstmals den Berichtsentwurf von Arlene McCarthy.  Wir hatten den Mitgliedern unsere Analyse in gedruckter Form im Vorfeld zugestellt.  FFII/Eurolinux sieht diesen Bericht als juristischen Pfusch, aus der Perspektive der Patentabteilungen großer US-Konzerne geschrieben, durch ungewöhnlich dreiste Taktiken der Begriffsverwirrung und Täuschung gekennzeichnet.  Wenige kamen zur Sitzung, und nur eine Handvoll der JURI MEPs sprachen auf der Veranstaltung.  Die Atmosphäre schien harmonisch und unterstützend.  Milde Kritik über den Mangel an Klarheit in McCarthys Entwurf äußerten MacCormick (Grüne), Gebhard (SPE) und Wuermeling (EVP).

#rWb: McCarthy für Grenzenlose Patentierbarkeit

#Wiw: Neue Datenbank und Statistik der EPA-Softwarepatente

#Wip: Wir haben alle jemals vom EPA erteilten Patente analysiert und die softwarelastigen aufbereitet und darauf aufbauend einige Statistiken erstellt.

#eii: ITRE stimmt für Veröffentlichungsfreiheit und das Recht auf Interoperabilität

#amu: Die Industrie- und Handelskommission (ITRE) des Europäischen Parlaments stimmte für Änderungsvorschläge zu dem Richtlinienvorschlag, die der Veröffentlichungsfreiheit und dem Recht auf Interoperabilität absoluten Vorrang über Patente einräumen.  Mitglieder der Europäischen Volkspartei (EVP, konservativ, christlich-demokratisch) brachten Änderungsvorschläge mit entgegengesetzter Zielrichtung ein, also für einen breiteren Schutz durch Patente, und dafür, die Veröffentlichung als direkte Patentvergehen einzustufen.  Diese Vorschläge wurden von der Mitte-Links-Mehrheit abgewiesen.

#eop: CULT stimmt für klaren Ausschluss von Software von der Patentierbarkeit

#wWt: Die Kulturkommission des Europäischen Parlaments stimmte für Änderungsvorschläge zu dem Richtlinienentwurf, die Software von der Patentierbarkeit ausschließen.  Einerseits ist %(q:Datenverarbeitung kein Feld der Technik), andererseits ist %(q:Technik) positiv definiert als %(q:Steuerung von Naturkräften zur Erreichung eines physikalischen Effekts).  Diese Vorschläge wurden von dem früheren französischen Premierminister Michel Rocard eingebracht und von allen Parteien außer der größten, nämlich der Europäischen Volkspartei (EVP, Konservative, christlich-demokratisch), unterstützt, die unter der Führung des Schattenberichterstatters Joachim Wuermeling und dem Ausschussmitglied Janelly Fourtou (die Frau des Präsidenten von Vivendi), gegen alles stimmten, das die Patentierbarkeit einschränken könnte, und sogar Vorschläge einbrachte, die den Patentschutz weiter ausdehnen würden. Diese Vorschläge wurden von der Mitte-Links-Mehrheit niedergestimmt.

#vnc: von %1

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri03 ;
# txtlang: de ;
# multlin: t ;
# End: ;

