<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2003-09-01 OECD Conference %(q:Patents, Innovation and Economic
Performance)

#descr: The procedings of the OECD Conference %(q:Patents, Innovation and
Economic Performance) held in Paris in September 2003 were published
as a book with ISBN 92-64-01526-4.

#rei: Proceedings

#ihe: Hostile Approach of US delegation

#Crs: The procedings of the OECD Conference %(q:Patents, Innovation and
Economic Performance) held in Paris in September 2003 were published
as a book with ISBN 92-64-01526-4.

#vtg: The volume contains among others

#ihy: The US government, represented by its patent office, appeared rather
displeased at the (cautiously) critical way in which the subject was
taken up at OECD and said, in the closing speech of Ms Boland, that
the US would seek to strenghten and expand the patent system through
WIPO, WTO, bilateral agreements or whatever channel seemed suitable
for this purpose and would not let OECD get in its way.  In
comparison, the representatives of the European Patent Office appeared
very enlightened.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnoecd030901 ;
# txtlang: en ;
# multlin: t ;
# End: ;

