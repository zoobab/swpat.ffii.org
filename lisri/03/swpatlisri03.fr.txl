<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Nouvelles sur les Brevets Logiciels en 2003

#descr: Ce que la FFII avait pour rapporter en 2003 sur les monopoles
d'invention accordés par l'état et leur extension abusive sur la
pensée, le calcul, l'organisation et la formulation a l'aide de
l'ordinateur universel.

#jpp: Explains how eventually the Internet will, through limitation of
consumers to reduced functionality, %(q:micro-payments), %(q:trusted
computing) and other developments, step by step become a medium where
texts are attributable to people and tight control by state
authorities is possible.  Walker also mentions that he has had to pay
25000 USD for for infringing on an outrageous software patent, and
that the enforcement of such patents and of various other unreasonable
Intellectual Property claims will be made easy by the future
architecture of the Net.

#neF: Jean-Pierre Courniou, R&D director of Renault and president of the
%(q:Club Informatique des Grandes Entreprises Françaises) (CIGREF =
Computing Club of the Large French Companies) gave a speech about
current IT policy matters in which he pointed out that software
patents are a source of great danger and summarised: %(bc:For me and
for CIGREF, the response is %(q:no, we don't want patentability))

#sWe: Attac against software patents, calls on Europarl to reject directive
proposal

#Wsa: The German chapter of the globalisation-critical organisation ATTAC
has published a letter to members of the European Parliaments and
press release.

#sWt: EU-ordered IBM study 2003/02: Commission should promote patent system
by means of school curricula

#Coh: In February 2003 IBM Business Consulting Services B.V. completed a
study on behalf of the European Commission which compares the role of
major patent offices throughout the world in promoting the patent
system and recommends: %(orig|We strongly recommended the structural
incorporation of IPR education in the curriculum of technical schools
and universities.  The individual patent offices seem to have little
influence in this regard and are likely to be treated with suspicion
given the inherent conflict of interests. In our view, it would be
more appropriate for the European Commission's Directorate-General for
Education and Culture to initiate such a discussion on a pan-European
level. Ideally, this would be supplemented at the national levels with
initiatives by each EU Member State'scorresponding ministry and at the
global level by UNESCO. Obviously, the patent offices should play an
active role in curriculum development and its implementation.)

#0iw: CEPIS 2003/07: Leading Computer scientists reject software patent
directive proposal

#som: The third edition of the renowned computing quarterly CEPIS Upgrade
contains several articles and the text of the %(q:Petition of 30
Scientists).  The table of contents says:%(orig|Why software should be
within the scope of copyright law, and not patent law.  Pierre Haren
opens this section with some brief notes on his opinion on software
patents, %(q:A Note on Software Patents).|Alberto Bercovitz
Rodríguez-Cano offers a transcript of his %(q:On the Patentability of
Inventions Involving Computer Programmes), which he delivered at a
hearing held at the European Parliament.|Roberto Di Cosmo %(q:Legal
Tools to Protect Software:  Choosing the Right One), an article
studying the different legal tools to deal with software.|Closing this
section, we include %(q:Petition to the European Parliament), written
by several well-known European computer scientists and engineers,
related to the proposed Directive on software patents currently being
discussed at the European Parliament.)

#Wel: Greek Software Patents campaign launched

#fio: A group of activists from Linux user groups in Greece has built a
website to campaign against the planned legalisation of software
patents in Europe.

#swnlinu030922t: Linus Torvalds and Alan Cox call on MEPs to keep Europe free from
software patents

#swnlinu030922d: The two chief architects of the Linus operating system kernel, Linus
Torvalds from Finland and Alan Cox from UK, ask for effective
limitations to patentability in their letter to the members of the
European Parliament. In particular, they recommend MEPs to follow the
FFII voting list.  The vote on the Directive will be on Wednesday and
it is expected to be a very close one.

#jwuerm0309t: Wuermeling Promoting Fake Limits on Patentability

#jwuerm0309d: The shadow rapporteur of the European People's Party, Dr. Joachim
Wuermeling from Bavaria, has come under pressure.  An international
group of 32 EPP members, headed by Piia-Noora Kauppi from Finland, has
submitted their own amendment proposals which effectively exclude
software and business method patents from patentability and safeguard
basic freedoms of competition, publication etc.  Many EPP colleagues
have started studying them on their own, without relying on their
shadow rapporteur.  At the same time, a demonstration is taking place
in Munich today in protest of Wuermeling's policies.  To this
Wuermeling has reacted with a PR and a set of amendments.  Both pay
lipservice to the concerns of %(q:open source companies and small
developpers), but, upon close reading, only consist of redundant
rhetoric which confirms the Trilateral Standard of the US, Japanese
and European Patent Office of 2000, under which algorithms and
business methods such as Amazon One Click Shopping are without doubt
patentable subject matter.  More important than these PRs and articles
is the question of whether Wuermeling will support Kauppi's amendments
in his voting list next week.  These actually implement the goals to
which Wuermling is paying lipservice.

#CeapmeT: Alliance of 2,000,000 SMEs against Software Patents and EU Directive

#CeapmeD: An alliance representing a total of 2,000,000 small and medium-sized
businesses in Europe says that software patents are harmful for SMEs
and that in particular the software patent directive proposal as
amended by the European Parliament's Legal Affairs Commission is a
grave risk for innovation, productivity and employment in Europe.

#Wte: US Gov't Promoting Patent Extremism in the European Parliament

#btl: The %(q:Mission of the United States of America to the European Union)
in Brussels has sent a long paper %(q:by the US), titled %(q:U.S.
Comments on the Draft European Parliament Amendments to the Proposed
European Union Directive on the Patentability of Computer-Implemented
Inventions) to numerous members of the European Parliament.  %(q:The
US) warns that Europe might fall afoul of the TRIPs treaty if it
passes the proposed directive as amended by the Parliament.  In
particular, %(q:the US) believes that conversion between patented file
formats should generally not be allowed without a license, and
therefore demands deletion of Art 6a.  Moreover %(q:the US) cites the
same BSA studies and the same reasoning as found in the European
Commission's directive proposal, and warns that any failure to
wholeheartedly endorse patentablity of software in the directive might
%(q:adversely impact certain sectors of the economy), because
%(q:copyright does not protect the functionality of the software,
which is of significant value to the owner), and that lack of clarity
in the concept of %(q:technical contribution) would lead to a
continued need for negotiations with the US in WIPO and other fora. 
This warning comes shortly after a similar letter to MEPs from the UK
Government.  It is part of a US Government %(q:Action Plan) to
%(q:promote international harmonisation of substantive patent law) in
order to %(q:strengthen the rights of American intellectual property
holders by making it easier to obtain international protection for
their inventions).  This plan has been promoted aggressively by top
officials of the US Patent Office in international fora such as WIPO,
WSIS and OECD as well as through bilateral negotiations.

#0np: UK Gov't Promoting Patent Extremism in the European Parliament

#opn: The UK Government's Foreign Office is circulating a %(q:briefing to UK
MEPs), in which it instructs british members of the European
Parliament to back Arlene McCarthy's position and vote (1) against any
attempt to define what is technical or otherwise limit what is
patentable (2) against Article 6a which allows converters to be
written when standards are patented (3) for JURI Art 5 which forbids
publication of descriptions of patented inventions on the Net.  The
intervention of the government comes at a moment where McCarthy has
shown nervous reactions in view of dwindling support in her party
group.  The government statement can be attributed to the UK Patent
Office and its policy working group, consisting mainly of patent
lawyers from large corporations.  This group has been determining the
software patent policy of the UK and largely also of the EU during
recent years.

#swndemo030827t: EU Software Patent Plans Shelved Amid Massive Demonstrations

#swndemo030827d: On Aug 28th, the European Parliament postponed its vote on the
proposed EU Software Patent Directive.  The day before, approximately
500 persons had gathered for a rally beside the Parliament in
Brussels, accompanied by an online demonstration involving more than
2000 websites.  The events in and near the Parliament were reported
extensively covered in the media, including tv and radio, all over
Europe and beyond.  Within a few days, the petition calling the
European Parliament to reject software patentability accumulated
50,000 new signatures.

#tta: Aug 27 Demonstrations against EU Software Patent Plans

#woa: The Proposal for a software patent directive, which will be submitted
to the European Parliament for plenary debate and subsequent decision
on September 1st, is giving rise to another wave of protests.  Various
groups in Belgium and elsewhere are mobilising for a rally in Brussels
on August 27th and are calling on web administrators to temporarily
block their web sites.

#2se: Melullis 2002: Zur Sonderrechtsfähigkeit von Computerprogrammen

#Wco: The presiding judge of the Patent Senate of the Federal Court of
Justice, Dr. Klaus Melullis, analyses the legal and political
situation concerning software patents and concludes that the European
Commission's directive proposal %(q:with its by-and-large affirmation
of software patentability does not fit into the system of the EPC),
and that the legislator has so far failed to assess the concerned
interests and to clarify the basis of his decision.

#int: 30 Scientists 2003/05: Petition against Software Patent Directive

#ref: 30 famous computer scientists sharply criticise the European
Commission's proposal to legalise software patents in Europe.

#2on: OECD Paris 2003/08/28-29: Conference on IPR, Innovation and Economic
Performance

#rxi: The Paris-based Organisation of Economic Cooperation and Development
(OECD) is working on a project to study the impact of patents,
copyright and other exclusion rights on innovation and economic
performance.  A two-day conference is to be held where Hartmut Pilch
of FFII/Eurolinux is participating in a panel on software.

#rnp: MEPs call for %(q:Strong Sanctions) against %(q:any intellectual
property infringment)

#crm: MEP Arlene McCarthy calls for signatures to a letter that links
copyright infringement to terrorism.

#IschT: EU Patent Movement meeting top politicians in Ischia 2003/10/05-7

#ischD: The EU patent movement is meeting in Napoli (Naples) on October 5-7,
discussing among themselves and with EU politicians from the Italian
Presidency, the Commission and the Parliament subjects such as the
limits of patentability.

#eolast: .5 bn USD damages for patent on browser extensions

#eolasd: The Californian university spin-off Eolas succeded in extracting .5 bn
USD rents from Microsoft using patent US5838906.  This Eolas patent
covers, as an Illinois jury confirmed, Microsoft's ActiveX facility. 
It also covers basically any means to extend a browser by scripts. 
Eolas lawyers try to create the impression that Microsoft actually
copied their %(q:technology) and that their latest rent-seeking aims
at restoring justice in the browser war, where Microsoft leveraged its
monopoly position to put Netscape out of business in 1999. Some media
seem to be buying their story.

#iec: Bill Gates warned at an investors' conference in his typical cryptic
way that Linux will have difficulty to survive in the age of patents: 
%(q:Here you have a product without R&D controls, and it's not part of
a cross-license,) he said. %(q:Given the high level of functionality,
you'd think it would have patents.) %(q:Companies that are doing R&D
have by and large entered into cross-licensing agreements,) he said.
%(q:Microsoft and IBM did cross-licensing 10 years ago, when we were
small. But Linux is not covered by most of these cross-licenses.)
%(q:The whole IP thing is begging to get attention because it's not a
scenario that existed in the past,) Gates noted. %(q:The SCO suit is
largely related to trademark and copyright.) %(q:However, Gates said
intellectual property from SCO and other companies--including
Microsoft--has found its way into the code.) %(q:There's no question
that in cloning activities, IP from many, many companies, including
Microsoft, is being used in open-source software,) Gates said. %(q:CEO
Steve Ballmer, also on hand to answer questions at the meeting, said
customers and partners are confused about the impact of the IP issues
related to Linux.)  Some news reporters such as Heise may be counted
among the confused.  They took Microsoft's %(q:IP) talk to mean that
%(q:Linux contains Microsoft code).  Upon careful reading it becomes
clear that Gates meant %(q:Linux uses ideas which Microsoft has
patented).

#teW: Bericht über eine Rede von Bill Gates, in dieser warnt, Linux sei in
einem Zeitalter der Softwarepatente nicht überlebensfähig und die
meisten Leute hätten den Wandel des Zeitalters nicht begriffen.  Zu
diesen Nicht-Begreifern gehört vielleicht auch der Heise-Schreiber,
der von urheberrechtsgeschütztem %(q:Microsoft-Code) statt von
patentierten %(q:Microsoft-Ideen) schreibt.

#eip: In 2002, National Instruments Inc obtained a court injunction to force
its competitor Mathworks to withdraw allegedly patent-infringing math
software from the market. As of July 2003, Mathworks has appealed to
the Federal Court and obtained a decision to %(q:preserve the status
quo).  In this press release, Mathworks expresses confidence that they
will be able to show that they are not infringing and that some of
NI's patent claims are invalid.

#sil: Software Professionals challenge Arlene McCarthy in The Guardian

#mse: Le Comité des Affaires Juridiques du Parlement Européen a débattu du
rapport temporaire du rapporteur Arlene McCarthy. Notre analyse sur ce
document a été distribuée aux 70 membres de cette commission.
FFII/Eurolinux considère ce rapport comme étant un charabia juridique,
écrit avec la vision d'un service de brevet d'une grande entreprise
américaine, caractérisé par la tactique exceptionnellement frivole de
la confusion. Peu de députés ont assisté et parlé à cette réunion. Une
critique légère s'est élevée sur le manque de clarté du rapport de
McCarthy par MM. MacCormick (Verts), Gebhard (ESP) et Wuermeling
(EPP).

#hav: Jean-Michel Yolin 2003/06: Patents no longer serve Innovation

#vgW: Jean-Michel Yolin, president of the %(q:Innovation) section in the
French Ministery of Economics and author of the report %(IE), observes
in an interview about the SCO vs IBM/Linux case: %(bc:Patents once
served to make research and development efforts pay off.  Meanwhile,
instead of serving innovation, the patent system has been twisted to
become a means of mining the territory and neutralising unwanted
innovators by sending them lawyers to screw them up at a moment where
they are raising funds or acquiring customers.)

#xd2amt: XDrudis to AMccarthy 03/06/10: Your Note on Directive COM (2002) 92

#xd2amd: Xavier Drudis Ferran from Catalan Linux Users Group (CALIU) informs
Arlene McCarthy MEP of his analysis of the FAQ which she had
distributed to the participants of the FFII/Eurolinux conference in
the Dorint Hotel in Brussels on 03/05/07-8, points out some fallacies
in this document and invites McCarthy to engage in the dialogue about
whose absence she has been complaining to journalists.  This letter
remains unanswered.

#ph2amt: PHM to AMccarthy 03/06/10: Questions based on 2 Example Patent Claims

#ph2amd: Hartmut Pilch of FFII writes publicly to Arlene McCarthy, citing an
algorithm claim and a business method claim, both of which have been
granted by the European Patent Office (EPO) against the letter and
spirit of the European Patent Convention, and both of which have US
cousins which have already caused considerable damage.  Some of Arlene
McCarthy's public statements seem to exclude patents on algorithms and
business methods while others seem to make such patents unavoidable. 
Arlene McCarthy should explain this contradiction in unambiguous
terms, based on example patents.  Arlene McCarthy did not answer.  The
closest to an answer was a statement in JURI on June 16: %(bc:We have
attemted to set some limits in perhaps a moderately restrictive way,
without entirely reinventing patent law, which I would hasten to add,
we are not in ability to do that, we are legislators to create
framework and laws for interpretation by experts, but we are not
experts ourselves.)

#rot: French Report and discussion thread about submission of petition to
the parliament.

#swnlxtg030709t: Software Patents at Linuxtag 2003

#swnlxtg030709d: FFII will be present on the major European Free Software event with a
booth, lectures, discussions and a pantomime performance, centering on
the subject of software patents.  These activities are receiving
special support from the organisors of Linuxtag.

#rtP: European Parliament Rejects Attempt to Rush Vote on Software Patent
Directive

#aAh: The European Parliament has postponed the vote on the software patent
directive back to the original date of 1st of September, thereby
rejecting initially successful efforts of its rapporteur %(tp|Arlene
McCarthy|UK Labour MEP of Manchester) and her supporters to rush to
vote on June 30th, a mere twelve days after publication of the highly
controversial report and ten days after the unexpected change of
schedule.

#ar0: Vote in 8 days: 2000 IT bosses urge European Parliament to say NO to
software patents

#Win: A %(q:Petition for a Free Europe without Software Patents) has gained
more than 150000 signatures.  Among the supporters are more than 2000
company owners and chief executives and 25000 developpers and
engineers from all sectors of the European information and
telecommunication industries, as well as more than 2000 scientists and
180 lawyers.  Companies like Siemens, IBM, Alcatel and Nokia lead the
list of those whose researchers and developpers want to protect
programming freedom and copyright property against what they see as a
%(q:patent landgrab).  Currently the patent policy of many of these
companies is still dominated by their patent departments.  These have
intensively lobbied the European Parliament to support a proposal to
allow patentability of %(q:computer-implemented inventions) (recent
patent newspeak term which usually refers to software in the context
of patent claims, i.e. algorithms and business methods framed in terms
of generic computing equipment), which the rapporteur, UK Labour MEP
Arlene McCarthy, backed by %(q:patent experts) from the socialist and
conservative blocks, is trying to rush through the European Parliament
on June 30, just 13 days after she had won the vote in the Legal
Affairs Committe (JURI).

#PWb: McCarthy pushes Parliament to rush vote on McCarthy software
patentability directive

#pvW: Due to requests from the Socialist Group (PSE) of JURI rapporteur
Arlene McCarthy, the European Parliament protracted the planned vote
on software patentabilty from September 1 to July 1, just 13 days
after McCarthy won the vote in the Legal Affairs Committee (JURI).

#vln: JURI votes for Fake Limits on Patentability

#iye: The European Parliament's Committee for Legal Affairs and the Internal
Market (JURI) voted on tuesday morning about a list of proposed
amendments to the planned software patent directive.  It was the third
and last in a series of committee votes, whose results will be
presented to the plenary in early september.  The other two
commissions (CULT, ITRE) had opted to more or less clearly exclude
software patents.  The JURI rapporteur Arlene McCarthy MEP (UK
socialist) also claimed to be aiming for a %(q:restrictive
harmonisation of the status quo) and %(q:exclusion of software as
such, algorithms and business methods from patentability).  Yet
McCarthy presented a voting list to fellow MEPs which, upon closer
look, turns ideas like %(q:Amazon One-Click Shopping) into patentable
inventions.  McCarthy and her followers rejected all amendment
proposals that try to define central terms such as %(q:technical) or
%(q:invention), while supporting some proposals which reinforce the
patentability of software, e.g. by making publication of software a
direct patent infringment, by stating that %(q:computer-implemented
inventions by their very nature belong to a field of technology), or
by inserting new economic rationales (%(q:self-evident) need for
Europeans to rely on %(q:patent protection) in view of %(q:the present
trend for traditional manufacturing industry to shift their operations
to low-cost economies outside the European Union)) into the recitals. 
Most of McCarthy's proposals found a conservative-socialist 2/3
majority (20 of 30 MEPs), whereas most of the proposals from the other
committees (CULT = Culture, ITRE = Industry) were rejected.  Study
reports commissioned by the Parliament and other EU institutions were
disregarded or misquoted, as some of their authors point out (see
below).  A few socialists and conservatives voted together with Greens
and Left in favor of %(tp|real limits on patentability|such as the
CULT opinion, based on traditional definitions, that %(q:data
processing is not a field of technology) and that technical invention
is about %(q:use of controllable forces of nature)), but they were
overruled by the two largest blocks.  Most MEPs simply followed the
voting lists of their %(q:patent experts), such as Arlene McCarthy
(UK) for the Socialists (PSE) and shadow rapporteur Dr. Joachim
Wuermeling (DE) for the Conservatives (EPP).  Both McCarthy and
Wuermeling have closely followed the advice of the directive
proponents from the European Patent Office (EPO) and the European
Commission's Industrial Property Unit (CEC-Indprop, represented by
former UK Patent Office employee Anthony Howard) and declined all
offers of dialog with software professionals and academia ever since
they were nominated rapporteurs in May 2002.

#EA2: 2002-05-15 EuroParl hearing on the Directive Proposal

#Poh: An hearing about the European Commission's software patentability
directive proposal will be held at the European Parliament in
Strasbourg on the afternoon of thursday 15-16:30.  Hartmut Pilch from
FFII/Eurolinux is invited to speak for 20 minutes.  Among the other
speakers is the chief directive editor from DG Markt.

#epon023t: EPO 2002-03 on business methods and genes: %(q:By digging here you
will find no silver, 300 ounces)

#epon023d: This month the EPO has issued three press releases which make quite a
lot of noise about refusal to search/grant applications for pure genes
or pure business mehtods.  FFII welcomes these messages but warns that
they do not mean what they appear to mean and explains why business
methods and genes may nonetheless be patentable.

#BWw: Bitkom in favor of software patents

#Bar: The German IT Association Bitkom welcomes the CEC/BSA Software
Patentability directive proposal.  Bitkom is especially happy to see
that software as such is recognised as inventive subject matter by the
CEC proposal and demands that correspondingly program claims should
also be admitted so that it is assured that software patents can
actually be enforced.  In apparent contradiction to the contents of
the directive proposal, Bitkom claims that this proposal ensures a
high standard of inventiveness.  Because both the software industry
and the patent licensing business are gaining importance, it is
important that the two be brought together.  This opens a lot of
opportunities for SMEs. The Bitkom declaration was decreed by a
session of 8 members of the Bitkom Workgroup on industrial property &
copyright.  7 of these 8 members came were patent lawyers representing
large companies.  1 was a marketing person from an SME.  The draft was
written by the committee speaker, Dr. iur Kathrin Bremer and approved
due to the leadership of Fritz Teufel, the IBM patent lawyer who
dominated the session.  The paper does not care to substantiate its
claims by means of any examples from the experience of Bitkom members
nor does it spell out any examples of patents which Bitkom thinks are
good and others which they deem bad.  Also, the declaration disregards
all informed discussions and studies which have been conducted on the
subject.  The initial draft even claimed without providing any trace
of evidence that software patents create employment.  Maybe they meant
%(q:for patent lawyers) like those dominating the session.

#B4c: Bulmahn lobbying for novelty grace period in Europe

#Frh: E. Bulmahn, german minister for science, wrote a letter to her
european colleagues asking them to support her ministery's initiative
to introduce an american-style novelty grace period in order to boost
patent applications in universities.  American studies have shown that
such an institute will be used by 20% of the professors and it is
particularly helpful for universities if they can first see how the
idea is received in public and then decide to apply for a patent.

#mth: Spiegel, german weekly magazine, reports that a music notation method
has been patetned.

#Nnh: Reorientation of IT Research Financing: Patents as %(q:Primary
Indicators)

#Bii: As part of its 5-year plan IT 2006 for promoting research and
development in the area of software, the Federal Ministery of Research
and Education names patents as a %(q:primary indicator) for
benchmarking the success of the publicly financed projects.  This is
very much in line with many recent statements by minister Bulmahn.

#MUo: Microsoft bars GNU software from interoperating with CIFS

#DLe: During the 1st week of April 2002, Microsoft published a license for
its new specification CIFS which it is trying to establish as a de
facto communication standard.  This license says that free software
under GNU GPL, LGPL and similar licenses may not use CIFS.  It bases
this ban on two broad and trivial US patents with priority dates of
1989 and 1993.  Preliminary search results suggst that these patents
to not have EP (European Patent) counterparts.  But there is
nevertheless an EP patent which could possibly be used by MS for the
same purpose.  Critical network infrastructure such as Samba as well
as new projects such as Mono seem to be affected.

#rtF: The shadow rapporteur of the European People's Party, Dr. Joachim
Wuermeling from Bavaria, has come under pressure.  An international
group of 32 EPP members, headed by Piia-Noora Kauppi from Finland, has
submitted their own amendment proposals which effectively exclude
software and business method patents from patentability and safeguard
basic freedoms of competition, publication etc.  Many EPP colleagues
have started studying them on their own, without relying on their
shadow rapporteur.  At the same time, a demonstration is taking place
in Munich today in protest of Wuermeling's policies.  To this
Wuermeling has reacted with a PR and a set of amendments.  Both pay
lipservice to the concerns of %(q:open source companies and small
developpers).  Wuermeling seems to have given up resistance to Art 6a,
but still wants to make publication of software a direct patent
infringement.  More important than these PRs and articles is the
question of whether Wuermeling will support Kauppi's amendments in his
voting list next week.  These actually implement the goals to which
Wuermling is paying lipservice.

#Ukinno031218T: UK Gov't publishes innovation policy report

#Ukinno031218D: The report, apparently written by the Department of Trade and Industry
together with scholars from UK universities, compares the innovative
capacity of the UK to that of other countries and measures this
capacity by some indirect indicators, among which the number of
patents has particular weight.  There a few examples pulled out from
the software industry, and they do seem throughout to be rather
gripped by a %(q:must maximise the number of patents being achieved)
and %(q:we must teach SMEs to make better use of the patent system)
mentality.  The report also mentions a set of 7 committees called
%(q:Innovation and Growth teams) set up to advise the Govt sector by
sector on innovation.   The one on %(q:Software and digital content)
has not yet reported, though all but one of the others now have.

#Bswk031216T: Business Week on European Parliament's vote: leftists erasing billions
in intellectual property

#Bswk031216D: An editorial of the US magazine BusinessWeek reports that a group of
%(q:left-leaning politicians) and %(q:open-source advocates) by
%(q:last-minute lobbying) %(q:upended) a directive proposal in such a
way that it actually %(q:bans software patents), thereby creating an
%(q:industry-specific exemption) which violates the TRIPs treaty amd
%(q:erases billions in intellectual property granted by the EPO).  The
author gives Europe a lot of advice, demanding that Europe should set
an example by finding a formula that %(q:spurs innovation while
safeguarding intellectual property).  The article contains various
contradictions and false assertions.  Jim Bessen (innovation economics
researcher at MIT) and others have written letters to the editor.

#Optima031216T: Optima suing Roxio for CD writing algorithm

#Optima031216D: A fledgling software company is claiming ownership to all algorithms
that can be used for modifying contents in write-once-only media by
appending to the end, including those laid down in industry standards.
 The patent would be grantable under the European Commission's
directive proposal but not under that of the European Parliament.

#Intel031211T: Intel chairman: patent system unfit for information age

#Intel031211D: In an interview with the Washington Post, Intel Chaiman Andrew S.
Grove says that the USA %(q:needs to revamp not just the patent
system, but the entire system of intellectual property law. ... It
needs to redefine it for an era that is the information age as
compared to the industrial age.)

#Mice031210T: Microsoft-sponsored study: software industry needs patents

#Mice031210D: A study written at the order of Microsoft by the Muenster University
Institute for Computational Economics (MICE) says that every Microsoft
job in Germany creates about 7 non-Microsoft jobs in the downstream
industry and that software patents are good.  The reasoning is very
similar and no more sophisticated than that of the BSA studies which
the European Commission and the US Trade Representative cite in favor
of their software patentability proposal: proprietary software
directly remunerates those who write programs, and it does this by
means of %(q:intellectual property), of which patents are one
important kind.

#Msfat031209T: Microsoft takes out patents on FAT

#Msfat031209D: Microsoft has published licensing terms for a series of patents which
cover the FAT file format.  These license terms exclude free software
such as the GNU/Linux operating system.  Several of the patents on
which Microsoft's terms are based have also been granted by the
European Patent Office.  However under the European Patent Convention,
as confirmed by the recent vote of the European Parliament, these
patents are invalid.  Moreover, the European Parliament's Art 6a
assures that the use of a patented technique for the purpose of
interoperation is not a patent infringement.

#Libdem031208T: UK Libdem Youth demands strong position against software patents

#Libdem031208D: At its recent conference, the youth wing of the UK Liberal Democrats
has passed a resolution which criticizes the Libdem members of the
European Parliament (MEPs) for voting in favour of software patents. 
The UK Libdems in teh European Parliament, led by Diana Wallis and
Andrew Duff, had had followed the recommendations of Arlene McCarthy
(MEP, UK Labour) and the UK Patent Office, therebey taking an extreme
pro-patent line within the European Liberals, thereby ignoring the
party's written policy documents as well as their own lip-service paid
to these documents before the vote.   The Libdem Youth calls on the
Libdem MEPs to follow the party's line before the second reading in
the European Parliament.

#Cecua031207T: CECUA paper in favor of unlimited patentability

#Cecua031207D: The chairman of CECUA.org (Confederation of European Computer User
Associations), Alain Moscowitz, has presented a paper which pleads in
favor of patentability of software, including business methods, at a
dinner organised by the %(q:European Internet Foundation), an
organisation financed by large IT companies for the purpose of
wining-and-dining members of the European Parliament.  Among the MEPs
most active in EIF include Arlene McCarthy, Jannelly Fourtou, Erika
Mann and Elly Plooij-Van-Gorsel, all of whom campaigned and voted for
unlimited patentability in September, whereas some of them had taken
the opposite view before.  The behaviour of Mr. Moscowitz is even more
surprising, because some of the associations which he claims to
represents have expressed contrary positions and 290,000 computer
users have signed a petition which demands the contrary of what
Moscowitz is demanding.

#Cwdk031202T: Danish Campaign against Europarl Amendments

#Cwdk031202D: Computerworld.dk has an article titled %(q:Open source destroys danish
innovation) which reports about %(q:3 innovative danish SMEs) who went
to see the government to complain about the European Parliament's
amendments to the directive, saying that the EP made it impossible for
them to protect their intellectual property.  This was echoed by a FUD
campaign of major danish IT associations, similar in tone to the
Computerworld article.   Preliminary research shows that the SMEs in
question are all close allies or subsidiaries of Microsoft.   None has
publicly pointed to any case studies to demonstrate a legitimate need
of their business for any kinds of software patents.  Like the 5
Telecom CEOs, they have shunned public discourse and relied solely on
secretive lobbying.

#Eukpt031201T: New EU rules on anticompetitive patent licensing

#Eukpt031201D: The European Commission is proposing new rules about when patent
licensing and other technology-transfer agreements should and should
not be allowed by competition law.  The new law would concentrate on
the actual effect of the agreement entered into, rather than its form.

#Entemp031201T: Irish presidency schedule

#Entemp031201D: The software patent directive will be discussed by the European
Council's Patent Policy Working Party (governmental patent experts) in
the first half year of 2004 and there will be pressures to produce a
counter-proposal to the European Parliament's amendments by May 2004
and to have an inofficial consensus document at the patent expert
level in March before the formal meeting of the heads of governments
which will take place on March 25-26th.

#Msspx031201T: MS to pay 62 mn usd in patent damages to SPX for Netmeeting

#Msspx031201D: A US court in San Francisco has condemned Microsoft Corporation to a
payment of 62 million US dollars for using %(q:whiteboards) in its
video conferencing software %(q:Netmeeting).  This feature had been
patented by the plaintiff Imagexpo LLC, a daughter company of SPX.

#Eicta031127T: EICTA calls on Commission, Council to Kill Parliament Vote

#Eicta031127D: EICTA, a lobbying group representing most large telecommunication and
consumer electronics companies in Europe as well as some SMEs has
published a press release which literally echoes the letter signed by
CEOs of Nokia, Ericsson, Alcatel, Philips and Siemens earlier this
month, complaining about the European Parliament's amendments to the
proposed software patent directive and asking the Commission and the
Council to undo these amendments and immediately go on a confrontation
course with the Parliament.  The Council did not follow this call
during its meeting of today but postponed the debate to next spring,
when the Irish presidency takes over.  EICTA expresses dismay at this
postponement and directs demands of %(q:the industry) to the Irish
presidency as well as to the Belgian government.   The latter demands
are based on the false claim that the Belgian EICTA member
organisation AGORIA represents Belgian SMEs.

#Att031121T: AT&T Sues PayPal and eBay for Patent Infringement

#Att031121D: AT&T has sued EBay for infringment of a patent on %(q:mediation of
transactions by a communication system) which has been granted by the
US Patent Office as well as the European Patent Office.

#Coreper031120T: Council Decision on Swpat Directive Delayed?

#Coreper031120D: According to observers of yesterday's meeting Software Patent
Directive appears not on the agenda for the Competitiveness Council
next week.  That means that it will not be possible to transmit
anything to the European Parliament for a second reading this session.
There simply isn't time.  The Italian presidency feels it has taken
this dossier as far as it can, and it will now fall to the Irish to
produce the Council's Common Position during its six month term in the
first half of next year. That means that the Council's Common Position
will be transmitted to the next Parliament.

#Coreper031119T: Directive progress report on the agenda for Coreper Wed 19/ Fri 21

#Coreper031119D: In a report %(q:Preparation for the Competitiveness Council meeting on
26 and 27 November 2003), appearing as item 17 in part II of the
agenda (page 4), there is, amongst other topics %(q:subitem 5:
Proposal for a directive of the European Parliament and of the Council
on the patentability of computer-implemented inventions) with with two
patent-lobby documents cited for reference: the original CEC proposal
from last year, and the %(q:progress report) holding document from 13
November.  Presumably, a significant factor in the Coreper discussion
will be how the Working Party meeting yesterday turned out.  It may
well be that the worst possible document was unanimously agreed at the
Working Party yesterday, and that it is going to be given to the
Ministers to rubber-stamp.

#Jonas031119T: CEC Justifications for Rejecting Europarl Amendments Dissected

#Jonas031119D: In circles close to the European Commission's industrial property unit
and the Council patent working party, documents have recently
circulated which criticse the European Parliament's amendments and
give reasons for their rejection.  Governmental patent lawyers are
trying to kill the Parliament's amendments with technicalities.  We
have started working on a document which comments on this reasoning,
paragraph by paragraph.

#Nlurl031119T: Patents on Native Language Domain Names

#Nlurl031119D: Currently several patents are pending at the EPO on coding methods for
using special characters (e.g. accented European characters) in
internet domain names.  One such pantent was granted but withdrawn,
others are under application.

#France031119T: France asks for delay and consultation on Directive

#France031119D: It was reported today at the end of the French Council of Ministers
that the French governement will propose to his european partners a
delay before the Council's final decision of its stance, in order to
do another internal consultation.

#Msxml031117T: MS publishes Patent License for Office Formats

#Msxml031117D: Microsoft Coroporation has published a set of XML-based definitions of
office formats used in applications such as MS Word, MS Excel, MS
Powerpoint etc.  These come together with a set of conditions of use
of these formats which are imposed with help of unnamed patents on
these formats: %(q:Microsoft may have patents and/or patent
applications that are necessary for you to license in order to make,
sell, or distribute software programs that read or write files that
comply with the Microsoft specifications for the Office Schemas.)  It
is known that Microsoft has been applying for patents at the European
Patent Office (EPO) which cover current or possible future MS Office
formats.

#Telcos031107T: CEOs of big telcos sign letter against Europarl Amendments

#Telcos031107D: The chief executive officers of Alcatel, Ericsson, Nokia and Siemens
have signed a letter to the European Commission and the European
Council which complains about the European Parliament's amendments to
the proposed software patent directive, saying that these will
effectively remove the value of most of the patents of their companies
and thereby harm the competitiveness of Europe's industry and violate
the TRIPs treaty.  FFII points out that the Directive indeed threatens
the interests of the patent departments of such companies, but not of
the companies themselves: The letter is characterised by untruthful
dogmatic assertions which say much about the thinking of patent
departments and little about the interests of their companies, many of
whose employees, especially software developers, support the positions
of FFII.

#Kober031106T: EPO President to speak before European Parliament

#Kober031106D: On 27 November 2003 the European Parliament's Committees for Industry
and Legal Affairs will hold a joint meeting to hear a presentation by
Ingo KOBER, President of the European Patent Office, on the mission
and functions of EPO.

#Phelps031106T: MS %(IP leader): EU needs better lobbying for software patents

#Phelps031106D: Marshall Phelps, the new %(q:IP leader) at Microsoft who came over
from the IBM patent department, hints in an interview that MS may be
getting more aggressive on lobbying for software patents in Europe:
%(q:We'll focus on Europe that bit more...I don't think we joined the
debate [on software patents in Europe] in the right way.) This article
is an essential reading to include/understand what Microsoft intends
to do with its 3000 software patents and 5114 software patent
applications. It also informs their strategy of lobbying on the
software patentability in Europe.

#Bolk031105T: Bolkestein urges businessmen to step up lobbying %(q:to get key
proposals adopted)

#Bolk031105D: The EU's press page has the text of a speech make by Bolkestein at the
weekend, which nicely captures his world view.  The overall argument
is revealing -- do what I say, or all the jobs go to Turkey, China and
India.  He cites the Community patent, the biotech directive,
recognition of professional qualifications, and the take-over bids
directive, criticising Member States for holding up progress.  To deal
with this, he urges his audience of businessmen to step up their
lobbying efforts:  %(q:Fifth, there is a crucial role for business.
Let me ask you a few questions. How many of you as business leaders
are personally involved in the public affairs side of your companies?
How many of you have entered political debates (e.g. by writing
articles for newspapers)? How many of you are lobbying behind the
scenes to get key proposals adopted? You need to be doing all these
things.)

#Wipo031104T: Wipo bangs the Drum

#Wipo031104D: WIPO has just published a 377 page brochure, %(q:Intellectual Property
- A Power Tool for Economic Growth), aimed at policy-makers in
businesses and governments worldwide, and as the preface puts it,
%(q:written from a definite perspective -- that IP is good).

#Cdnow031104T: Patent auf Musik-Download

#Cdnow031104D: Ein US-Gericht hat entschieden, dass sich US-Patent 5,191,573 der
Firma Sightsound auch auf bezahltes Herunterladen von Musik im
Internet erstreckt.  Damit verlor die Bertelsmann-Tochter CDNow eine
Runde im Rechtsstreit.

#Giev031104T: GI-Vorstandswahlen

#Giev031104D: Die Gesellschaft fÃ¼r Informatik wÃ¤hlt in kÃ¼rze ihren neuen
Vorstand.  Einige Mitglieder haben die Kandidaten gebeten, zum Thema
Softwarepatente Stellung zu nehmen.

#Ftc031029T: FTC publishes Report on Patents and Competition

#Ftc031029D: The Federal Trade Commission of the USA has published a summary
report, based on findings which it gathered from extensive hearings. 
FTC sees grave problems with the patent system.  The report says that
extending this system to software and business methods in the 1980/90s
was a mistake, but stops short of proposing to reverse this decision. 
Instead it proposes a series of procedural reforms.

#Coreper031028T: Coreper Procedural Note

#Coreper031028D: A note was prepared for Coreper 1 by the General Secretariat of the
Consilum on 28th October, discussing the procedure for the examination
of the amendments proposed by the EP.

#psn: MPE Proposent 106 Amendments à la Directive de Brevets Logiciels

#aft: Members from all groups of the European Parliament have submitted a
total of 120 amendment proposals to the software patent directive
draft.  Virtually all proposals show an intention of wanting to avoid
software patents or limit their scope and effects.  Some amendments
serve the professed aim while others do not.  None can achieve the aim
on their own, embedded in a long directive text which codifies
unlimited patentability with many redundant safeguards.  We are
working on a tabular overview and analysis of all amendments.

#tWM: Lettre aux Députés

#leP: Cette Dernière Lettre fut envoyée à tous les membres du parlement par
courriel et fax.

#hKr: Le Gouvernment Allemand presse pour brevetabilité maximale, ignore la
critique du président de la Court de Brevet Suprème

#uWe: The FFII has reviewed one of several recent articles of judge Klaus J.
Melullis.  Unfortunately the German government is not listening to the
opinions of judge Melullis and his colleagues.  For them only the
views of IBM's corporate patent department count as the real evangile
of patent law.  At the EU level, the german government's ministry of
justice is continuing to pressure the Commission and the European
Parliament to suppress the last remnants of freedom of publication
(Art 4) and freedom of interoperation (Art 6a) from the directive
proposal.

#dcc: The EU software patent directive is a %(q:wolf in a sheep's coat).  We
have documented in detail what misleading terms such as %(q:technical
contribution) really mean.

#nsW: Gates: Linux en contrefaçon de multiples brevets, SCO n'est que le
début

#rrc: Première reponse publique de McCarthy

#rrW: Arlene McCarthy, Member of the European Parliament for UK labour and
rapporteur of the JURI committeee for the software patent directive
proposal, has for the first time directly answered arguments from
critics in a letter to the british newspaper The Guardian.  However
her answers are evasive, deceptive and polemic.  McCarthy reiterates
demagogic arguments of whose untruth she has been well aware for at
least one year, and even resorts to lies in the strictest sense of the
term.  Some of the polemics seem designed to shift focus to unrelated
subjects and incite flamewars with the free software community.  We
analyse McCarthy's fallacies and the political context of her letter.

#iWP: Pétition Soumise au Parlament Européen

#ftl: %(AD) de l'%(AEL) a conduit toutes les procédures et reçu la
confirmation que la %(EP) avec ses 140,000 signature virtuelle a été
soumise au Parlament.  Maintenant la Commission des Pétitions peut
prendre un décision en reponse.  Les personnes de contacte
enregistrées sont Alexandre Dulaunoy (AEL) et Hartmut Pilch (FFII).

#noL: Association Belge pour les Libertés Électroniques

#uot: Horaire pour Discussion sur le Brevet Logiciel au Parlament Européen

#WWy: 1ère Discussion JURI sur rapport McCarthy

#rWb: McCarthy pour brevetabilité sans limites

#Wiw: Nouvelle Base de Donnée et Statistiques des Brevets Logiciels OEB

#Wip: Nous avons analysé touts les brevets accordé par l'OEB des sa création
en 1978 et préparé ceux qui relevent du logiciel et calculé des
statistiques sur cette base.

#eii: ITRE vote pour liberté de publication et interopération

#amu: The Industry and Trade Commission (ITRE) of the European Parliament
voted for amendments to the directive proposal that give freedom of
publication and interoperability absolute priority over patents.  EPP
members (conservative, christian-democrat) proposed amendments in the
opposite direction, i.e. for a wider scope of patentability and for
treating publication as a direct infringment.  These were rejected by
the center-left majority.

#eop: CULT vote pour exclusion claire du logiciel de la brevetabilité

#wWt: The Cultural Affairs Commission of the European Parliament voted for
amendments to the directive proposal that exclude software from
patentability.  On the one hand, %(q:data processing is not a field of
technology), on the other %(q:technology) is positively defined as
%(q:controlling forces of nature to achieve a physical effect).  These
amendments were proposed by former french prime minister Michel Rocard
and supported by all parties except for the largest, the European
People's Party (EPP = conservatives and christian democrats), who,
under the guidance of their shadow rapporteur Joachim Wuermeling and
committee member Janelly Fourtou (wife of Vivendi president), voted
against everything that limited patentability and even proposed
amendments to enlarge patentability, which were voted down by the
center-left majority.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri03 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

