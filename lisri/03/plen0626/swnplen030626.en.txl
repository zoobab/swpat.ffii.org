<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#oWW: More Contacts to be supplied upon request

#goW: The %(tp|Committee for Legal Affairs and the Internal Market|JURI) was justly %(wr:labelled) a %(q:legislative sausage machine) by its vice president Willi Rothley shortly before its misguided vote on software patents.  This sausage machine has been turning out a seemingly never-ending stream of poorly crafted and shoddily reasoned special-interest legislation for many years, including directives on database exclusivity, gene patents and copying prevention.  Now perhaps for the first time the sausage machine is meeting significant public resistance at a stage where there are still chances of stopping and questioning it.  We may cautiously hope that we are part of a process of change for the better in the culture of lawmaking in Europe.

#eWe: The conference which we organised with the Greens/EFA in May and the steadily mounting pressure of public opinion have clearly created a sense of urgency among the promoters of software patentability in the Legal Affairs Committee.  Now, thanks to the postponement, we have three more session weeks for raising awareness.

#iWe: With the big pressures in European institutions it might seem that only deep pocket lobbies would be taken into account, but our experience shows that public opinion, grass-roots efforts and a little coordination and organisation can still push the interests of the majority, at least for so evident cases.

#uWr: The groundswell of public sentiment, together with a concerted lobbying effort by a group of 2000 software companies coordinated by FFII/Eurolinux, has undoubtedly helped to raise awareness among MEPs.  As FFII president Hartmut Pilch remarked:

#een: If any of you intend to vote in favour of the proposed Directive, may I ask you to be so kind as to explain to myself your reasons for concluding that this is in the interests of Europe? The eyes of many IT-literate constituents are on you, and you will undoubtedly permanently lose many of %(tp|our votes|certainly including mine) should you choose to support this assault on our livelihoods and interests.

#ofg: Almost all involved in software in Europe, bar a select few large corporations, and law firms who make money from litigation and legal complexities, are opposed to software patenting.  There is a huge groundswell of opinion amongst the real %(tp|software engineers|who understand the complex process and history of software development) which favours strong and unambiguous %(e:prohibition) of patents on software. Copyright is the right tool to protect software, not patents. By using grossly misleading and emotive language such as %(q:giving software innovators the protection they deserve) the proponents are trying to give the appearance that software developers and businesses are crying out for %(q:protection) by patents, when quite the opposite is true - %(tp|we|and society at large) actually want and need protection %(s:from) software patents!

#tWn: Many software professionals have been contacting their MEPs in recent days.  A letter by %(TJ), operations manager for Internet Assist Ltd in Chelmsford, UK, reflects the mood:

#crl: On Thursday morning, at the meeting of the secretary generals, the representatives of all political groups voted for postponment.  Their vote was confirmed by the conference of presidents (i.e. head of transnational party groups) during their session at 3 p.m.  At 8 p.m. the decision was made public on the Parliament's %(ep:schedule webpage).

#Wsr: On Wednesday the climate change became apparent.  More and more MEPs rumored that the schedule would not be upheld.  Even %(am:Arlene McCarthy) was quoted as saying that it might be too tight.  A spokesman from the %(ec:General Directorate for the Internal Market of the European Commission), which has been pushing for the directive together with Arlene McCarthy and other allies in the Parliament's Commitee for Legal Affairs and the Internal Market (JURI), meanwhile told journalists: %(q:Arlene McCarthy has tried hard to have the vote conducted on June 30th, but as things now stand, this looks rather unlikely.)

#edn: Until Wednesday, leaders of the two largest blocks, the socialists (PSE) and conservatives (PPE), seemed determined to follow the recommendations of their %(q:patent experts) and go ahead with the vote quickly.  They explained that there was no reason to wait, because all possible amendment proposals had already been submitted to the committees and translated to all languages, and there was no need for new amendments.  This view however became increasingly difficult to uphold, as more and more MEPs in all parties became aware of the schedule change and pointed out that they wanted to prepare new amendments.  Within the socialist group, a large opposition group, possibly the majority, gathered around Michel Rocard (FR), Luis Berenguer (ES), Evelyn Gebhardt (DE), Olga Zrihen (BE) and other MEPs who had played a prominent role in resisting software patentability.

#eth: Members of Parliament from all parties had complained that it was impossible to react adequately within a timeframe of 10 days.

#dokurl: Permanent URL of this Press Release

#ffii: About the FFII

#eurolinux: About the Eurolinux Alliance

#media: Media Contacts

#agu: Background

#title: European Parliament Rejects Attempt to Rush Vote on Software Patent Directive

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnplen030626 ;
# txtlang: en ;
# multlin: t ;
# End: ;

