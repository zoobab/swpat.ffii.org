<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Workgroup">
<link href="http://www.ffii.org/assoc/webstyl/news.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2003/12/10">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="expires" content="-1">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="Foundation for a Free Information Infrastructure, Software Patents, intellectual property, industrial property, IP, I2P, immaterial assets, law of immaterial goods, mathematical method, business methods, Rules of Organisation and Calculation, invention, non-inventions, computer-implemented inventions, computer-implementable inventions, software-related inventions, software-implemented inventions, software patent, computer patents, information patents, technical invention, technical character, technicity, technology, software engineering, industrial character, Patentierbarkeit, substantive patent law, Nutzungsrecht, patent inflation, quelloffen, standardisation, innovation, competition, European Patent Office, General Directorate for the Internal Market, patent movement, patent family, patent establishment, patent law, patent lawyers, lobby">
<meta name="title" content="European Parliament Rejects Attempt to Rush Vote on Software Patent Directive">
<meta name="description" content="The European Parliament has postponed the vote on the software patent directive back to the original date of 1st of September, thereby rejecting initially successful efforts of its rapporteur Arlene McCarthy (UK Labour MEP of Manchester) and her supporters to rush to vote on June 30th, a mere twelve days after publication of the highly controversial report and ten days after the unexpected change of schedule.">
<title>EP 03-06-26</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<div id="toprel">
<a href="swnplen030626.en.txl"><img src="/img/icons.other/txt.png" alt="[translatable text]" border="1" height=25></a>
<a href="/group/todo/index.en.html"><img src="/img/icons.other/questionmark.png" alt="[howto help]" border="1" height=25></a>
<a href="swnplen030626.en.pdf"><img src="/img/icons.other/pdficon.png" alt="[printable version]" border="1" height=25></a>
<a href="http://kwiki.ffii.org/Swnplen030626En"><img src="/img/icons.other/addenda.png" alt="[Addenda]" border="0"></a>
</div>
<div id="toparb">
<table width="100%">
<tr class=doklvl4><th><a href="/news/03/index.en.html">News of 2003</a></th><td class=eqdok>EP 03-06-26</td><td><a href="/news/03/plen0620/index.en.html">EP 03-06-20</a></td><td><a href="/news/03/linu0922/index.en.html">Linus 03-09-22</a></td><td><a href="/news/03/plen0924/index.en.html">EP 03-09-24</a></td></tr>
</table>
</div>
<div id="doktit">
<h1><a name="top">European Parliament Rejects Attempt to Rush Vote on Software Patent Directive</a><br>
<font size=3>Brussels 2003/06/26<br>
For immediate Release</font></h1>
</div>
<div id="dokdes">
The European Parliament has postponed the vote on the software patent directive back to the original date of 1st of September, thereby rejecting initially successful efforts of its rapporteur Arlene McCarthy (UK Labour MEP of Manchester) and her supporters to rush to vote on June 30th, a mere twelve days after publication of the highly controversial report and ten days after the unexpected change of schedule.
</div>
</div>
<div id="dokmid">
<div class="sectmenu">
<ul><li><a href="#bakgr">Background</a></li>
<li><a href="#media">Media Contacts</a></li>
<li><a href="#eurolinux">About the Eurolinux Alliance -- www.eurolinux.org</a></li>
<li><a href="#ffii">About the FFII -- www.ffii.org</a></li>
<li><a href="#url">Permanent URL of this Press Release</a></li>
<li><a href="#links">Annotated Links</a></li></ul>
</div>

<div class="secttext">
<div class="secthead">
<h2><a name="bakgr">Background</a></h2>
</div>

<div class="sectbody">
Members of Parliament from all parties had complained that it was impossible to react adequately within a timeframe of 10 days.<p>Until Wednesday, leaders of the two largest blocks, the socialists (PSE) and conservatives (PPE), seemed determined to follow the recommendations of their &quot;patent experts&quot; and go ahead with the vote quickly.  They explained that there was no reason to wait, because all possible amendment proposals had already been submitted to the committees and translated to all languages, and there was no need for new amendments.  This view however became increasingly difficult to uphold, as more and more MEPs in all parties became aware of the schedule change and pointed out that they wanted to prepare new amendments.  Within the socialist group, a large opposition group, possibly the majority, gathered around Michel Rocard (FR), Luis Berenguer (ES), Evelyn Gebhardt (DE), Olga Zrihen (BE) and other MEPs who had played a prominent role in resisting software patentability.<p>On Wednesday the climate change became apparent.  More and more MEPs rumored that the schedule would not be upheld.  Even <a href="/players/amccarthy/index.en.html">Arlene McCarthy</a> was quoted as saying that it might be too tight.  A spokesman from the <a href="/players/cec/index.en.html">General Directorate for the Internal Market of the European Commission</a>, which has been pushing for the directive together with Arlene McCarthy and other allies in the Parliament's Commitee for Legal Affairs and the Internal Market (JURI), meanwhile told journalists: &quot;Arlene McCarthy has tried hard to have the vote conducted on June 30th, but as things now stand, this looks rather unlikely.&quot;<p>On Thursday morning, at the meeting of the secretary generals, the representatives of all political groups voted for postponment.  Their vote was confirmed by the conference of presidents (i.e. head of transnational party groups) during their session at 3 p.m.  At 8 p.m. the decision was made public on the Parliament's <a href="http://www3.europarl.eu.int/ap-cgi/chargeur.pl?APP=IRIS+PRG=REPRIEF+FILE=REPRIEF+SESSION=JUL|03+DAY=1+SES=ALL+LG=FR+BACK=NONE">schedule webpage</a>.<p>Many software professionals have been contacting their MEPs in recent days.  A letter by <a href="http://www.timj.co.uk/digiculture/patents/">Tim Jackson</a>, operations manager for Internet Assist Ltd in Chelmsford, UK, reflects the mood:<p><blockquote>
Almost all involved in software in Europe, bar a select few large corporations, and law firms who make money from litigation and legal complexities, are opposed to software patenting.  There is a huge groundswell of opinion amongst the real software engineers (who understand the complex process and history of software development) which favours strong and unambiguous <em>prohibition</em> of patents on software. Copyright is the right tool to protect software, not patents. By using grossly misleading and emotive language such as &quot;giving software innovators the protection they deserve&quot; the proponents are trying to give the appearance that software developers and businesses are crying out for &quot;protection&quot; by patents, when quite the opposite is true - we (and society at large) actually want and need protection <strong>from</strong> software patents!

[...]

If any of you intend to vote in favour of the proposed Directive, may I ask you to be so kind as to explain to myself your reasons for concluding that this is in the interests of Europe? The eyes of many IT-literate constituents are on you, and you will undoubtedly permanently lose many of our votes (certainly including mine) should you choose to support this assault on our livelihoods and interests.
</blockquote><p>The groundswell of public sentiment, together with a concerted lobbying effort by a group of 2000 software companies coordinated by FFII/Eurolinux, has undoubtedly helped to raise awareness among MEPs.  As FFII president Hartmut Pilch remarked:<p><blockquote>
With the big pressures in European institutions it might seem that only deep pocket lobbies would be taken into account, but our experience shows that public opinion, grass-roots efforts and a little coordination and organisation can still push the interests of the majority, at least for so evident cases.

The conference which we organised with the Greens/EFA in May and the steadily mounting pressure of public opinion have clearly created a sense of urgency among the promoters of software patentability in the Legal Affairs Committee.  Now, thanks to the postponement, we have three more session weeks for raising awareness.

The Committee for Legal Affairs and the Internal Market (JURI) was justly <a href="http://aful.org/wws/arc/patents/2003-06/msg00091.html">labelled</a> a &quot;legislative sausage machine&quot; by its vice president Willi Rothley shortly before its misguided vote on software patents.  This sausage machine has been turning out a seemingly never-ending stream of poorly crafted and shoddily reasoned special-interest legislation for many years, including directives on database exclusivity, gene patents and copying prevention.  Now perhaps for the first time the sausage machine is meeting significant public resistance at a stage where there are still chances of stopping and questioning it.  We may cautiously hope that we are part of a process of change for the better in the culture of lawmaking in Europe.
</blockquote>
</div>

<div class="secthead">
<h2><a name="media">Media Contacts</a></h2>
</div>

<div class="sectbody">
<dl><dt>mail:</dt>
<dd>media at ffii org</dd>
<dt>phone:</dt>
<dd>Hartmut Pilch +49-89-18979927</dd></dl><p>More Contacts to be supplied upon request
</div>

<div class="secthead">
<h2><a name="eurolinux">About the Eurolinux Alliance -- www.eurolinux.org</a></h2>
</div>

<div class="sectbody">
The EuroLinux Alliance for a Free Information Infrastructure is an open coalition of commercial companies and non-profit associations united to promote and protect a vigourous European Software Culture based on copyright, open standards, open competition and open source software such as Linux.  Corporate members or sponsors of EuroLinux develop or sell software under free, semi-free and non-free licenses for operating systems such as GNU/Linux, MacOS or MS Windows.
</div>

<div class="secthead">
<h2><a name="ffii">About the FFII -- www.ffii.org</a></h2>
</div>

<div class="sectbody">
The Foundation for a Free Information Infrastructure (FFII) is a non-profit association registered in Munich, which is dedicated to the spread of data processing literacy.  FFII supports the development of public information goods based on copyright, free competition, open standards.  More than 300 members, 700 companies and 50,000 supporters have entrusted the FFII to act as their voice in public policy questions in the area of exclusion rights (intellectual property) in data processing.
</div>

<div class="secthead">
<h2><a name="url">Permanent URL of this Press Release</a></h2>
</div>

<div class="sectbody">
http://swpat.ffii.org/news/03/plen0626/index.en.html
</div>

<div class="secthead">
<h2><a name="links">Annotated Links</a></h2>
</div>

<div class="sectbody">
<div class="links">
<dl><dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/news/03/plen0620/index.en.html">Divided European Parliament rushed to vote on software patent directive</a></b></dt>
<dd>Due to requests from the Socialist Group (PSE) of JURI rapporteur Arlene McCarthy, the European Parliament protracted the planned vote on software patentabilty from September 1 to July 1, just 13 days after McCarthy won the vote in the Legal Affairs Committee (JURI).</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/news/03/epet0622/index.en.html">Vote in 8 days: 2000 IT bosses ask European Parliament to say NO to software patents</a></b></dt>
<dd>A &quot;Petition for a Free Europe without Software Patents&quot; has gained more than 150000 signatures.  Among the supporters are more than 2000 company owners and chief executives and 25000 developpers and engineers from all sectors of the European information and telecommunication industries, as well as more than 2000 scientists and 180 lawyers.  Companies like Siemens, IBM, Alcatel and Nokia lead the list of those whose researchers and developpers want to protect programming freedom and copyright property against what they see as a &quot;patent landgrab&quot;.  Currently the patent policy of many of these companies is still dominated by their patent departments.  These have intensively lobbied the European Parliament to support a proposal to allow patentability of &quot;computer-implemented inventions&quot; (recent patent newspeak term which usually refers to software in the context of patent claims, i.e. algorithms and business methods framed in terms of generic computing equipment), which the rapporteur, UK Labour MEP Arlene McCarthy, backed by &quot;patent experts&quot; from the socialist and conservative blocks, is trying to rush through the European Parliament on June 30, just 13 days after she had won the vote in the Legal Affairs Committe (JURI).</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/news/03/juri0617/index.en.html">JURI votes for Fake Limits on Patentability</a></b></dt>
<dd>The European Parliament's Committee for Legal Affairs and the Internal Market (JURI) voted on tuesday morning about a list of proposed amendments to the planned software patent directive.  It was the third and last in a series of committee votes, whose results will be presented to the plenary in early september.  The other two commissions (CULT, ITRE) had opted to more or less clearly exclude software patents.  The JURI rapporteur Arlene McCarthy MEP (UK socialist) also claimed to be aiming for a &quot;restrictive harmonisation of the status quo&quot; and &quot;exclusion of software as such, algorithms and business methods from patentability&quot;.  Yet McCarthy presented a voting list to fellow MEPs which, upon closer look, turns ideas like &quot;Amazon One-Click Shopping&quot; into patentable inventions.  McCarthy and her followers rejected all amendment proposals that try to define central terms such as &quot;technical&quot; or &quot;invention&quot;, while supporting some proposals which reinforce the patentability of software, e.g. by making publication of software a direct patent infringment, by stating that &quot;computer-implemented inventions by their very nature belong to a field of technology&quot;, or by inserting new economic rationales (&quot;self-evident&quot; need for Europeans to rely on &quot;patent protection&quot; in view of &quot;the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union&quot;) into the recitals.  Most of McCarthy's proposals found a conservative-socialist 2/3 majority (20 of 30 MEPs), whereas most of the proposals from the other committees (CULT = Culture, ITRE = Industry) were rejected.  Study reports commissioned by the Parliament and other EU institutions were disregarded or misquoted, as some of their authors point out (see below).  A few socialists and conservatives voted together with Greens and Left in favor of real limits on patentability (such as the CULT opinion, based on traditional definitions, that &quot;data processing is not a field of technology&quot; and that technical invention is about &quot;use of controllable forces of nature&quot;), but they were overruled by the two largest blocks.  Most MEPs simply followed the voting lists of their &quot;patent experts&quot;, such as Arlene McCarthy (UK) for the Socialists (PSE) and shadow rapporteur Dr. Joachim Wuermeling (DE) for the Conservatives (EPP).  Both McCarthy and Wuermeling have closely followed the advice of the directive proponents from the European Patent Office (EPO) and the European Commission's Industrial Property Unit (CEC-Indprop, represented by former UK Patent Office employee Anthony Howard) and declined all offers of dialog with software professionals and academia ever since they were nominated rapporteurs in May 2002.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://aful.org/wws/arc/patents/2003-06/msg00091.html">Transscript of Rothley's saussage machine speech</a></b></dt>
<dd>German social democrat MEP Willi Rothley expresses frustration with the European Commission's proposal, saying that it is messy and unnecessary.  The patent courts find the right balance by themselves and the legislator is hardly able to set any better rules than what the courts work out.  However the legislative apparatus is like a sausage machine that has to be kept working.  From time to time it just must spit out new laws, no matter whether needed or not, and we, the MEPs are caught in this machine and forced to somehow make the best of it.  Rothley's speech struck a chord of resonance in JURI, as the laughter shows.<p>see also <a href="http://www.digitalforbruger.dk/SWPAT/">sound recordings of JURI sessions</a></dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/index.en.html">FFII: Software Patents in Europe</a></b></dt>
<dd>For the last few years the European Patent Office (EPO) has, contrary to the letter and spirit of the existing law, granted more than 30000 patents on rules of organisation and calculation claimed in terms of general-purpose computing equipment, called &quot;programs for computers&quot; in the law of 1973 and &quot;computer-implemented inventions&quot; in EPO Newspeak since 2000.  Europe's patent movement is pressing to legitimate this practise by writing a new law.  Although the patent movement has lost major battles in November 2000 and September 2003, Europe's programmers and citizens are still facing considerable risks.  Here you find the basic documentation, starting from the latest news and a short overview.</dd></dl>
</div>
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<div align="center">[ <a href="/news/03/index.en.html">FFII Software Patent News 2003</a>&nbsp;&rarr;&nbsp;European Parliament Rejects Attempt to Rush Vote on Software Patent Directive | <a href="/news/03/plen0620/index.en.html">Divided European Parliament rushed to vote on software patent directive</a> | <a href="/news/03/linu0922/index.en.html">Linus Torvalds and Alan Cox call on MEPs to keep Europe free from software patents</a> | <a href="/news/03/plen0924/index.en.html">EU Parliament Votes for Real Limits on Patentability</a> ]</div>
</div>
<div id="pedbot">
<form action="http://www.google.com/search" method="get">
<input name="q" size=25 type="text" value="">
<input name="btnG" size=25 type="submit" value="Google Search ffii.org">
<input name="q" size=25 type="hidden" value="site:ffii.org">
</form>
</div>
<a href="http://validator.w3.org/check/referer"><img src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01!" border=0 height="31" width="88"></a>
<div id="dokadr">
http://swpat.ffii.org/news/03/plen0626/index.en.html<br>
&copy;
2003/12/10 (2003/06/26)
<a href="/group/index.en.html">Workgroup</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el -->
<!-- mode: html -->
<!-- End: -->

