<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN"
"http://www.w3.org/TR/REC-html40/strict.dtd">
<html>
<head>
<title>IPJUR</title>

<META NAME="AUTHOR" CONTENT="PA Axel H Horns">
<META NAME="KEYWORDS" CONTENT="Patentanwalt Axel H Horns, European Patent Attorney, European Trade Mark Attorney, patents, trademarks, marks, software, intellectual property, patent attorney, patent office, law, SWPAT, prosecution, litigation, Germany, Europe, United Kingdom, England, Scotland, news, newsticker, latest developments, Intellectual Property Notes, mailing list">
<META NAME="DESCRIPTION" CONTENT="IPJUR provides information on Intellectual Property law, in particular with regard to Europe">
<META NAME="GENERATOR" content="uksearchking.com">
<META NAME="ROBOTS" CONTENT="INDEX,FOLLOW">
</head>
<body text="#000000" bgcolor="#FFFFFF" link="#FF0000" alink="#FF0000" vlink="#FF0000">

<TABLE WIDTH="100%" border="0" cellspacing="0" BGCOLOR="#A0D7FF">
<TR BGCOLOR="#A0D7FF">
    <TD ALIGN="LEFT" VALIGN="CENTER" BGCOLOR="#000000">
    <A HREF="http://www.ipjur.com/"><IMG SRC="./ipjur.gif" border="0" hspace="0" vspace="0" HEIGHT="40" WIDTH="150" align="middle"></a>
    </TD>
    <TD ALIGN="CENTER" VALIGN="CENTER" colspan="4">
    <IMG SRC="./iplaw.gif" border="0" hspace="0" vspace="0" HEIGHT="40" align="middle">
        </TD>
</TR>
<TR BGCOLOR="#000000">

    <TD HEIGHT="18" WIDTH="20%" ALIGN="LEFT" VALIGN="BOTTOM">
        &nbsp;
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./02.php3"><IMG SRC="./arrow2.gif" ALT="I2P INFO" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./i2pa.gif" ALT="I2P" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./01.php3"><IMG SRC="./arrow2.gif" ALT="SOFTWARE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./software.gif" ALT="SOFTWARE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./03.php3"><IMG SRC="./arrow2.gif" ALT="MOVING SPOTS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./moving.gif" ALT="MOVING SPOTS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" VALIGN="BOTTOM" ALIGN="RIGHT">
        <A HREF="./04.php3"><IMG SRC="./arrow2.gif" ALT="PRACTICE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./practice.gif" ALT="PRACTICE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>
</TR>
</TABLE>

<TABLE WIDTH="100%" CELLPADDING="0" cellspacing="0" BORDER="0">
<TR>
<TD WIDTH="150" ALIGN="LEFT" VALIGN="TOP" BGCOLOR="#A0D7FF">

<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=3 BGCOLOR="#A0D7FF">
<TR>
    <TH COLSPAN="2" BGCOLOR="#A0D7FF">
        <B><FONT FACE="ARIAL" COLOR="#000000" SIZE="-1">
        <BR>EXTERNAL LINKS<BR>[OFFSITE]</FONT></B>
    </TH>
</TR>

<TR><TD>&nbsp;</td><td>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.wto.org/wto/eol/e/pdf/27-trips.pdf"><IMG SRC="./arrow.gif"
        ALT="WORLD TRADE ORGANISATION" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wto.org/">WTO</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wto.org/english/docs_e/legal_e/27-trips.pdf"><IMG SRC="./arrow.gif"
        ALT="AGREEMENT ON TRADE-RELATED APSECTS OF INTELLECTUAL PROPERTY RIGHTS" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wto.org/english/docs_e/legal_e/27-trips.pdf">TRIPS</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.wipo.int/"><IMG SRC="./arrow.gif"
        ALT="World Intellectual Property Organisation"
        HEIGHT=16 WIDTH=16 BORDER=0></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/">WIPO</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/members/convention/index.html"><IMG SRC="./arrow.gif"
        ALT="WIPO Convention" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/members/convention/index.html">WIPO Convention</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/treaties/ip/paris/index.html"><IMG SRC="./arrow.gif"
        ALT="Paris Convention" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/treaties/ip/paris/index.html">WIPO Paris Convention</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/treaties/ip/berne/index.html"><IMG SRC="./arrow.gif"
        ALT="Berne Convention" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/treaties/ip/berne/index.html">WIPO Berne Convention</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/pct/en/index.html"><IMG SRC="./arrow.gif"
        ALT="Patent Cooperation Treaty (PCT)" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/pct/en/index.html">WIPO PCT System</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/madrid/en/index.html"><IMG SRC="./arrow.gif"
        ALT="Madrid Agreement on International Registration of Trade Marks" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/madrid/en/index.html">WIPO Madrid System</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/hague/en/index.html"><IMG SRC="./arrow.gif"
        ALT="Hague Agreement on Protection of Models" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/hague/en/index.html">WIPO Hague System</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/clea/en/index.html"><IMG SRC="./arrow.gif"
        ALT="WIPO COLLECTION OF LAWS FOR ELECTRONIC ACCESS (CLEA)" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/clea/en/index.html">WIPO CELA</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.epo.co.at/"><IMG SRC="./arrow.gif"
        ALT="European Patent Office" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.epo.co.at">EPO</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.european-patent-office.org/legal/epc/e/index.html"><IMG SRC="./arrow.gif"
        ALT="European Patent Convention" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.european-patent-office.org/legal/epc/e/index.html">EPO EPC</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.epo.co.at/espacenet/info/access.htm"><IMG SRC="./arrow.gif"
        ALT="esp@acenet" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.epo.co.at/espacenet/info/access.htm">esp@acenet DATABASE</A>
        </FONT><BR></TD>
</TR>


<TR><TD><A HREF="http://www.european-patent-office.org/dg3/search_dg3.htm"><IMG SRC="./arrow.gif"
        ALT="EPO Case Law" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.european-patent-office.org/dg3/search_dg3.htm">EPO Case Law</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://oami.eu.int/en/"><IMG SRC="./arrow.gif"
        ALT="OHIM" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://oami.eu.int/en/">OHIM</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://oami.eu.int/en/aspects/reg.htm"><IMG SRC="./arrow.gif"
        ALT="EU Community Trade Mark Regulations" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://oami.eu.int/en/aspects/reg.htm">OHIM CTM LAW</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://oami.eu.int/search/trademark/la/en_tm_search.cfm"><IMG SRC="./arrow.gif"
        ALT="EU Community Trade Mark Database" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://oami.eu.int/search/trademark/la/en_tm_search.cfm">OHIM CTM DATA</A>
        </FONT><BR></TD>
</TR>


<TR><TD><A HREF="http://oami.eu.int/en/aspects/rtm.htm"><IMG SRC="./arrow.gif"
        ALT="OHIM Refused Marks" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://oami.eu.int/en/aspects/rtm.htm">OHIM Refused Marks</A>
        </FONT><BR></TD>
</TR>


<TR><TD><A HREF="http://oami.eu.int/decis_opp/Last_en.htm"><IMG SRC="./arrow.gif"
        ALT="OHIM Oppositions" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://oami.eu.int/decis_opp/Last_en.htm">OHIM Oppositions</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://oami.eu.int/en/aspects/cancel.htm"><IMG SRC="./arrow.gif"
        ALT="OHIM Cancellations" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://oami.eu.int/en/aspects/cancel.htm">OHIM Cancellations</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://oami.eu.int/en/aspects/decis.htm"><IMG SRC="./arrow.gif"
        ALT="OHIM Case Law" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://oami.eu.int/en/aspects/decis.htm">OHIM Case Law</A>
        </FONT><BR></TD>
</TR>


<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.dpma.de/"><IMG SRC="./arrow.gif"
        ALT="DPMA" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.dpma.de/">DPMA (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://jurcom5.juris.de/bundesrecht/patg/index.html"><IMG SRC="./arrow.gif"
        ALT="DE Patent Act (GER)" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://jurcom5.juris.de/bundesrecht/patg/index.html">DE Patent ACT (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://jurcom5.juris.de/bundesrecht/gebrmg/index.html"><IMG SRC="./arrow.gif"
        ALT="DE Utility Model Act (GER)" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://jurcom5.juris.de/bundesrecht/gebrmg/index.html">DE Utility Model Act (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://transpatent.com/gesetze/gintpue.html"><IMG SRC="./arrow.gif"
        ALT="DE Act on International Patent Agreements (GER)" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://transpatent.com/gesetze/gintpue.html">DE-IntPat&Uuml;G (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://jurcom5.juris.de/bundesrecht/markeng/index.html"><IMG SRC="./arrow.gif"
        ALT="DE Trade Marks Act (GER)" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://jurcom5.juris.de/bundesrecht/markeng/index.html">DE Trade Marks Act (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="https://dpinfo.dpma.de/"><IMG SRC="./arrow.gif"
        ALT="DPINFO DATABASE (GER)" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="https://dpinfo.dpma.de/">DPINFO DATABASE (GER)</A>
        </FONT><BR></TD>
</TR>


<TR><TD><A HREF="http://www.depatisnet.de/"><IMG SRC="./arrow.gif"
        ALT="DEPATISnet DATABASE" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.depatisnet.de/">DEPATISnet DATABASE</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.wipo.int/eng/general/links/ipo_web.htm"><IMG SRC="./arrow.gif"
        ALT="List of Patent and Trade Mark Offices with WWW site" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/eng/general/links/ipo_web.htm">List of Office Sites</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.uspto.gov/"><IMG SRC="./arrow.gif"
        ALT="United States Patent and Trade Mark Office" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.uspto.gov/">US-PTO</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://europa.eu.int/eur-lex/en/"><IMG SRC="./arrow.gif"
        ALT="EU Commission" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://europa.eu.int/eur-lex/en/">EU Commission</A>
        </FONT><BR></TD>
 </TR>

<TR><TD><A HREF="http://europa.eu.int/eur-lex/en/oj/index.html"><IMG SRC="./arrow.gif"
        ALT="EU Official Journal" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://europa.eu.int/eur-lex/en/oj/index.html">EU Official Journal</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.ipr-helpdesk.org/t_en/p/assoc/p_001_en.asp"><IMG SRC="./arrow.gif"
        ALT="EU IPR Helpdesk<BR>Associated States Pages" BORDER=0
        HEIGHT=16 WIDTH=16></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.ipr-helpdesk.org/t_en/p/assoc/p_001_en.asp">EU IPR Helpdesk<BR>Associated States Pages</A>
        </FONT><BR></TD>
</TR>

<TR>
<TD>&nbsp;</TD>
</TR>
</TABLE>

<!-- /news-left -->
<FONT SIZE="-2" FACE="Arial">
Updated: <!-- last-modified--> 2001-07-23
<!-- /last-modified--><BR>
</FONT>

<!-- common-footer -->
<FONT FACE="ARIAL" SIZE="-2">

<P><A href="mailto:horns@ipjur.com">&copy; 2001 PA Axel H Horns</A></p>
</FONT>

</TD>

<TD ALIGN="LEFT" VALIGN="TOP"> <FONT FACE="Arial" SIZE="-2">

<br>
GENERAL > NEWS

<br>&nbsp;<br>

<H3>LATEST DEVELOPMENTS [OFFSITE]</H3>

<table><FONT FACE="Arial" SIZE="-2">
<tr bgcolor="#ccccc">
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2">DATE</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">FILE</FONT></td>
<td valign="top" align="center"><FONT FACE="Arial" SIZE="-2"> TITLE</FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2002-02-20</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF<BR>HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EU]</strong>
The European Commission has presented a <a href="http://europa.eu.int/comm/internal_market/en/indprop/com02-92en.pdf">proposal for a Directive on the protection by patents of computer-implemented inventions
</a>. The proposed Directive would harmonise the way in which national patent laws deal with inventions using software.
Such inventions can already be patented by applying to either the European Patent Office (EPO) or the national patent offices of the Member States, but the detailed conditions for patentability may vary. A significant barrier to trade in patented products within the Internal Market exists as long as certain inventions can be protected by patent in some Member States but not others.
The proposed Directive will be submitted to the EU�s Council of Ministers and the European Parliament for adoption under the so-called �co-decision� procedure.
<a href="http://europa.eu.int/comm/internal_market/en/indprop/02-277.htm">More ...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2002-02-13</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EU]</strong>
Following the adoption of the Community Design Regulation in December 2001, the Commission has presented to the Member States the draft proposal for a "Commission Regulation Implementing the Council
Regulation on the Community Design" for discussion under the "Comitology Procedure". The purpose of this Regulation is to provide the Office for the Harmonisation in the Internal Market with the legal and administrative tools necessary for the registration of Community designs.
The Commission would expect to have this Regulation adopted by the Summer 2002. This should permit the Office to make the necessary preparations to accept applications for registered Community designs from next year.
<a href="http://europa.eu.int/comm/internal_market/en/indprop/draft_des_en.pdf">More ...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2002-01-28</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[DE]</strong>
Some plans of the German government to amend the German Law on Employee Inventions have now been implemented. As of February 07, 2002, inventions made by professors, lecturers and scientific assistants, in their capacity as such, at universities and higher schools of science will be governed by the new law. They will no longer be free inventions. A general reform on the
German Law on Employee Inventions is still under discussion. Probably the reform proposal will not be enacted before next General Elections for the German Parliament due in September 2002.
<a href="./data/bgbl102004s414.pdf">Publication of the Act in the Official Gazette ("Bundesgesetzblatt") [In German]</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2002-01-28</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[WIPO]</strong>
According to a notice given by WIPO, the coverage of Madrid Database has now been extended to include all international registrations that are currently in force or have expired within the past six months.
Moreover, the Hague Database includes bibliographical data and, as far as international deposits governed exclusively or partly by the 1960 Act of the Hague Agreement are concerned, reproductions of industrial designs relating to international deposits that have been recorded in the International Register and published in the International Designs Bulletin as of issue No. 1/1999.
<a href="http://ipdl.wipo.int?wipo_content_frame=/en/search/search.shtml">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-12-12</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EU]</strong>
The European Commission has welcomed the adoption by the EU's Council
of Ministers on 12th December of a Regulation introducing a single
Community system for the protection of designs. The
<a href="http://europa.eu.int/eur-lex/en/dat/2002/l_003/l_00320020105en00010024.pdf">Regulation</a> sets
up a simple and inexpensive procedure for registering designs with
the European Union's Office for Harmonisation in the Internal Market
in Alicante. Unregistered designs will also be protected. Companies
will still be able to choose to register designs under national law,
as national design protection, as harmonised by the design protection
Directive (98/71/EC),will continue to exist in parallel with
Community design protection. <a href="http://europa.eu.int/rapid/start/cgi/guesten.ksh?p_action.gettxt=gt&doc=IP/01/1803|0|RAPID&lg=EN&display=">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-12-03</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[DE]</strong>
On October 17, 2001 the 10th Senate of the Federal High Court (Bundesgerichtshof, BGH) has taken an important landmark decision (X ZB 16/00 - "Suche fehlerhafter Zeichenketten") concerning to patents on computer-implemented inventions. In particular, the court had to decide whether or not to allow claims directed to a "digital storage medium, in particular floppy disc, exhibiting electronically readable control signals which can be read out, the control signals being arranged for interaction with a programmable computer system" and to a "computer program product" as well as to a "computer program".
The BGH argues that the proposed digital storage medium is a subject-matter means ("gegenstaendliches Mittel") for performing the method proposed within the patent application; the intentional usage ("bestimmungsgemaesser Einsatz") thereof leads to the desired result. Moreover, the BGH says that if a teaching expressed as a computer program is deemed to be eligible as subject-matter of a patent claim, the "shaping instructions" ("praegenden Anweisungen") of the teaching as claimed must serve to solve a particular technical problem. <a href="http://www.topica.com/lists/intprop-l/read/message.html?mid=801230481&sort=d&start=995">More...</a> and <a href="http://www.ipjur.com/data/xzb1600.pdf">More...</a>
[In German]</p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-11-14</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[DE]</strong>
The German Government is about to make drastic changes to the German Act on Employees Inventions. In particular, the complex rules requesting the employee to report an invention and subsequently setting a term for the
employer to claim the invention are to be abandoned in favour of a more
streamlined process transferring the rights to the invention to the
employer per default. Moreover, the right of the employee to receive additional remuneration depending of the economic effects of the invention is to be substantially simplified.  <a href="http://www.ipjur.com/data/arbnerfg.pdf">More...</a>
[In German]</p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-11-14</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EPO]</strong>
The unremitting increase in its PCT search and examination workload has prompted the EPO to rationalise its procedures for international preliminary examination under Chapter II PCT with effect from 3 January 2002.
Its objective is to avoid using examining capacity unnecessarily on cases where applicants are more interested in prolonging the international phase than in the outcome of preliminary examination, and to save it for core work in search and European substantive examination.
To that end, procedures will be streamlined and international preliminary examination focused on its core elements.
A central element in the streamlined procedure is that the result of the international search will serve as the basis for international preliminary examination, without involving the substantive examiner again (procedure without detailed substantive examination). <a href="http://www.epo.co.at/epo/president/e/2001_11_13_e.htm">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-11-02</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[WIPO]</strong>
The eighth edition of the Nice classification is available. In particular, very imporant changes affecting Intl. Class 42 are worth to be known. <a href="http://classifications.wipo.int/fulltext/nice8/enmain.htm">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-11-02</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[DE]</strong>
On October 16, 2001 the Committee on Legal Affairs
("Rechtsausschuss") of the German parliament ("Bundestag") has
finalized a report on a "Bill concerning
clarification of cost regulations in the field of intellectual
property" ("Entwurf eines Gesetzes zur Bereinigung von
Kostenregelungen auf dem Gebiet des Geistigen Eigentums") which has
been published as a printed matter of the German Bundestag under No.
14/7140. The main issue was to convert Official fees of the German Patent and
Trade Mark Office as well as of the Federal Patent Court from
Deutsche Mark to Euro. Moreover, a lot of changes are to be made with regard
to terms and conditions of payment of Official fees.
However, deeply buried within this formal stuff there are a few
surprising amendments. For example, if the bill finally has passed
the parliament, the patent opposition proceedings usually held before
the Opposition Divisions of the German Patent and Trade Mark Office
will be suspended for three years due to overload and backlog
problems; instead, the Federal Patent Court will do this work. There
will be no longer any option to lodge an appeal against such decisions of first instance in view of insufficient factual
findings; only "revision" to the Federal High Court in Karlsruhe
(Bundesgerichtshof) will be allowable under certain circumstances.
Moreover, prior art searches not bound to a patent application will
be suspended for five years.
 <a href="http://www.ipjur.com/data/1407140.pdf">More...</a> [In German]</p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-10-09</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[WIPO]</strong>
The Working Group on Reform of the Patent Cooperation Treaty (PCT) will join together on a first session in Geneva from November 12 to 16, 2001.
 <a href="http://www.wipo.org/pct/en/meetings/reform_wg/index_1.htm">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-10-09</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML<BR>PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[ECJ]</strong>
The Court of Justice has dismissed the action brought by the
Netherlands seeking annulment of the Community directive on the legal
protection of biotechnological inventions. The Court of Justice takes the view that the Community directive
frames patent law in stringent enough terms to ensure that the human
body is unavailable for patenting and inalienable and to safeguard
human dignity.
 <a href="http://europa.eu.int/rapid/start/cgi/guesten.ksh?p_action.getfile=gf&doc=CJE/01/48|0|RAPID&lg=EN&type=PDF">More...</a>. The full text can be
 <a href="http://europa.eu.int/jurisp/cgi-bin/form.pl?lang=en">accessed</a> using the case number C-377/98</p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-10-01</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EU]</strong>
The Innovation Scoreboard is an assessment of innovation performance in the individual Member States of the European Union.
To measure innovation performance a set of 17 qualitative indicators are used, based on available statistics covering human resources, knowledge creation, the application of knowledge and innovation finance. The scoreboard is a "benchmarking" tool highlighting both strengths and weaknesses. <a href="http://www.cordis.lu/innovation-smes/scoreboard/scoreboard_2001.htm">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-10-01</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[OTHER]</strong>
The Executive Committee of FICPI met at Goodwood, England, from 2 to 7 September. The Committee adopted the following 4 Resolutions:
(a) Implementation of TRIPS in Relation to Pharmaceuticals, (b) Prior Art Effect of Prior Applications, (c) General Principles of PCT Reform, and (d) European Patent Translations. <a href="http://www.ficpi.org/ficpi/newsletters/49/resolGWengl.html">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-09-03</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[WIPO]</strong>
At the close of an international consultation process spanning the
past year, the World Intellectual Property Organization (WIPO) has
released its Final Report containing
recommendations dealing with the misuse of certain names and
identifiers in the Internet domain name system (DNS). WIPO finds that
the international legal framework for the protection in the domain
name system (DNS) of the naming systems examined is not yet fully
developed. The Report calls upon the international community to
decide whether to address these insufficiencies and establish a
complete legal basis for dealing with offensive online practices in
connection with the naming systems concerned.
<a href="http://wipo2.wipo.int/process2/report/index.html">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-08-31</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[epi]</strong>
Mr. Dieter K Speiser, Chairman of the Online Communications Committee of the <a href="http://www.patentepi.com/">epi</a>, has issued a notice discouraging from use of the <a href="http://www.epoline.org/">epoline</a> on-line filing faclilities of the EPO.
<a href="http://www.patentepi.com/english/200/220/">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-08-31</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[WIPO]</strong>
Draft modifications of the Administrative Instructions under the Patent Cooperation Treaty (PCT) designed to implement procedures for the
electronic filing and processing of international applications under the PCT, including the storage and records management of such
applications, are close to finalization. The necessary legal framework is proposed to be included in the Administrative Instructions as new
Part 7, and the necessary technical standard as new Annex F.
<a href="http://pcteasy.wipo.int/efiling_standards/EFPage.htm">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-08-11</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EPO]</strong>
The EPO has announced a new research programme in order to help the process of making capacity planning decisions. Researchers or teams of researchers, from Europe or elsewhere, are invited to submit proposals under five modules, namely (a) Methods to survey the intentions of applicants for future numbers of patent filings, (b) Methods to forecast numbers of patent filings at the firm level, (c) Methods to forecast numbers of patent filings at the industrial and national levels, (d) Patent transfer models (to forecast numbers of filings claiming priority of earlier filings), and  (e) Methods to forecast aggregated filing data by time series modelling. The time horizon for the programme is currently set to support projects of up to three years in duration and a total sum of up to EUR 150 000 is available for the programme. <a href="http://www.epo.co.at/news/pressrel/pdf/resblurb2.pdf">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-08-07</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EU]</strong>
A CEC Staff Working Paper No. SEC(2001) 1307 "Consultations on the impact of the Community utility model in order to update the Green Paper on the Protection of Utility Models in the Single Market (COM(95)370 final)" has been published on the CEC website. In order to give appropriate effect to the conclusions of the European Council, the Commission has suggested updating the information it obtained from the interested parties on the possible creation of a Community utility model. On 31 March 2001 the Internal Market Council welcomed the Commission�s intention of quickly organising consultations with a view to drawing up a basic document taking a closer look at the possible impact of a Community utility model in legal, practical and economic terms. Replies to the questions raised in the document must be sent by November 30, 2001 to the European Commission�s Directorate-General for the Internal Market, either by writing to the following address: European Commission, DG Internal Market (MARKT/E/2), rue de la Loi, 200 (C100 5/109), B-1049 Bruxelles, or by e-mail.
<a href="http://europa.eu.int/comm/internal_market/en/indprop/consultation_en.pdf">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-08-03</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EU]</strong>
The Commission of the European Comminities (CEC) has created
an <a href="http://www.forum.europa.eu.int/Public/irc/markt/softpat/newsgroups?n=forum">ONLINE-FORUM</a> on patenting of software-related inventions.
<a href="http://europa.eu.int/comm/internal_market/en/indprop/softforum.htm">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-08-03</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EU]</strong>
The Commission of the European Communities (CEC) had awarded to PbT Consultants, UK, a contract for reading and summarizing the
<a href="http://europa.eu.int/comm/internal_market/en/indprop/softreplies.htm">results</a> of the <a href="http://europa.eu.int/comm/internal_market/en/indprop/softpaten.htm">EU consultation on the patentability of computer implemented inventions</a>. The final report has now been published <a href="http://europa.eu.int/comm/internal_market/en/indprop/softpatanalyse.htm">on-line</a>.
<a href="http://europa.eu.int/comm/internal_market/en/indprop/softanalyse.pdf">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-08-02</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[UKPTO]</strong>
The United Kingdom Patent Office has published a new consultation paper "Meeting the Future: Consultation on Proposed Changes in Patent Practice and Procedure". Responses are sought until November 01, 2001. Topics are, inter alia, to restrict applicants to a limited number of opportunities to amend the papers of application, to allow examiners to issue an abbreviated examination report, putting a duty on applicants to file search and examination reports on corresponding cases, to cease amendment of the description, and to establish a Code of Practice between Examiners and Agents.
<a href="http://www.patent.gov.uk/about/consultations/future/future.pdf">More...</a></p></FONT>
</td>
</tr>


<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-08-02</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[OTHER]</strong>
The United Kingdom Patent Office's current view, pending and subject to clarification by the courts,
is that the mere act of sending an email to an intended recipient or recipients does not amount to public disclosure of the content of that email. This is the essential result of an enquiry recently directed to the UL-PTO by <a href="http://www.mayallj.freeserve.co.uk/">Mr. John Mayall</a>.
 <a href="http://www.mayallj.freeserve.co.uk/novelty.htm">More...</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-07-26</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML<BR>PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[BT]</strong>
The German government plans to amend the German Law on Employee Inventions. Up to now, in accordance with the provisions provided in Sect. 42 of the law and
in derogation from Sections 40 and 41 thereof, inventions made by professors, lecturers and scientific assistants, in their capacity as such, at
 universities and higher schools of science are free inventions.
 The German Government wants to revoke this "Hochschullehrerprivileg", effectively forcing professors and related staff to report their inventions
 to the university which can decide to claim them. The overall intention is to provide additional financial resources for universities coming in from patent licenses. However, the German Government replaces the pending Parliamentary Bill initiated by German Bundesrat (the chamber of the German "L&auml;nder" ("States") by some <a href="./data/1405975.pdf">different draft [In German]</a> attempting to regulate more carefully a balance between a duty to report inventions before publication and constitutional freedoms of university research and teaching.</a>
 <a href="./data/1407565.pdf">Parliamentary Bill [In German]...</a> and <a href="./data/BR74000.pdf">Bundesratsdrucksache ... [In German]</a></p></FONT>
</td>
</tr

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-07-10</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[BMWi]</strong>
On July 10, 2001 an expert workshop was held by the German Federal
Ministry for Economics and Technology in Berlin. About approximately
some 60+ attendants listened the presentation of preliminary results
of a study jointly conducted by Fraunhofer Institute for Research on
Systems and Innovation (ISI), Karlsruhe, and by Max Planck Institute
for Foreign and International Patent, Copyright and Competition Law,
Munich.  <a href="./BMWI001.php3">More ...</a> and <a href="http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2&tsid=212&tdid=1003&page=0">More ... [In German]</a></p></FONT>
</td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-06-29</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML<BR>PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EPO]</strong>
France has joined Germany, the United Kingdom and seven other countries in approving the <a href="http://www.ige.ch/E/jurinfo/pdf/epc65_e.pdf">London Protocol</a>, thereby dropping the requirement for European patent applications filed in France to be translated into French.
The London Protocol is an <a href="http://www.ige.ch/E/jurinfo/j14104.htm">optional agreement</a> by which the Member States can totally or partly waive the translation of European patents. A translation can be waived, if one of the 3 official languages of the EPO is also the official language of the state in which the patent is to be  granted. In all other cases a translation of the description can be waived, if the patent is available in an official EPO language selected by the granting state. The agreement will now come into effect on the first day of the fourth month after it has been ratified by 8 states. It is not known when this will happen. Read <a href= "http://www.latribune.fr/Archives/ArchivesProxy.nsf/SearchSimple/400AF8E42B257C51C1256A7E001325DA?OpenDocument">more</a>, <a href="http://www.cncpi.fr/htdocs/comm_presse.html">more</a>, <a href="http://www.industrie.gouv.fr/cgi-bin/industrie/frame423.pl?bandeau=/biblioth/bb_bibl.htm&gauche=/biblioth/docu/rapports/missions/lb_miss.htm&droite=/biblioth/docu/rapports/missions/sb_miss.htm">more, </a>and <a href= "http://www.industrie.gouv.fr/cgi-bin/industrie/sommaire/comm/com_contenu.pl?COM_ID=460">more ...</a></p></FONT></td>
</tr>


<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-06-15</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EU]</strong>
A new study on <a href="ftp://ftp.ipr-helpdesk.org/softstudy.pdf">Patent protection of computer programmes</a> created under Contract no. INNO-99-04 and submitted to European Commission, Directorate-General Enterprise, by <a href="http://www.cops.ac.uk/iss/people.html#puay">Dr. Puay Tang</a>, SPRU, University of Sussex, <A href="http://www.shef.ac.uk/uni/academic/I-M/law/adams.html">Prof. John Adams</a>, University of Sheffield, <a href="http://www.internetstudies.org/members/danipar.html">Dr. Daniel Par�</a>, SPRU, University of Sussex, has been published as document ECSC-EC-EAEC, Brussels-Luxembourg, 2001, on the Internet by the EU IPR Help Desk. <a href= "http://www.ipr-helpdesk.org/t_en/home.asp">More ...</a></p></FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-06-13</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML<BR>DOC</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[DK-PTO]</strong>
"Quality Assurance of Search and Examination in the European Patent System
A <a href="http://www.dkpto.dk/indhold/publikationer/rapporter/kvalitetssikring_patent_20010520.doc">new report from PA Consulting Group</a> shows that methods for quality assurance are readily available should the EPO decide to subcontract search and examination casework for european patent applications".
<a href= "http://www.dkpto.dk/english/start.htm">More ...</a></p></FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-06-12</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML<BR>PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EU] </strong>
European Patents Litigation Protocol [EPLP]: The summary of the first proposal for an EPLP was discussed at the sub-group of the Working Party on Litigation at its third meeting
in The Hague on April 4-6, 2001. Based on the results of this conference, the first proposal has been revised and is now available as
paper intitulated <a href="http://www.ige.ch/E/jurinfo/pdf/j14105_prop.pdf">"Second proposal for an EPLP"</a> for discussion among the interested circles. <a href= "http://www.ige.ch/E/jurinfo/j14105.htm">More ...</a></p></FONT></td>
</tr>


<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-06-01</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EU] </strong>
European Union: Breakthrough in the patents issue: After tough negotiations, the EU ministers with responsibility for
the internal market agreed on Thursday on a common focus for
continued efforts to introduce a Community patent.
   <a href= "http://www.eu2001.se/eu2001/news/news_read.asp?iInformationID=15512">More
</a>&nbsp;
   <a href= "http://europa.eu.int/rapid/start/cgi/guesten.ksh?p_action.gettxt=gt&doc=MEMO/01/210|0|RAPID&lg=EN">and More ...</a></p></FONT></td>
</tr>


<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-05-27</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EPO] </strong>Currently negotiations are underway to create a Protocol governing centralized litigation on the basis of European patents. The Swiss <a href="http://www.ige.ch/">IGE</a> has published a <a href="http://www.ige.ch/E/jurinfo/j14103.htm">Status Report</a> of the sub-group of the Working Party on Litigation convened for a third time at The Hague on April 4-6, 2001: "The summary of the first proposal for an EPLP was discussed. The Member States interested in a unifed patent court reached the conclusion that a European patent court of first and second instance should be created, and the well tried exisiting courts should be integrated into the system as regional chambers. The pending proceedings before the European patent court shall be distributed to the various regional chambers according to procedural rules. The delegations were in agreement that a decision by the European patent court should take effect erga omnes in all countries acceding to the EPLP. The question of language and the composition of the court is still open. However, the group agreed that an appropriate procedure with a simplified language rule should apply for minor local litigation. The question of whether patent attorneys can appear as representatives before the European patent court must be further explored. There will be an expert meeting in mid-May to discuss the procedural law of the future court. The next meeting of the subgroup has been set for Juli 2001."
   <a href= "http://www.ige.ch/E/jurinfo/pdf/j14103_prop.pdf">More ...</a></p></FONT></td>
</tr>


<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-05-21</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[WIPO]</strong> Currently delegates from WIPO Member States are being met together in order to start debating a substantial reform of the Patent Cooperation Treaty (PCT). The meeting will consider proposals for the reform of the PCT system, as decided by the Assembly of the International Patent Cooperation
Union (PCT Union) at its 29th (17th extraordinary) session, held from September 25 to October 3, 2000. <a href= "http://www.wipo.org/pct/en/reform/index_1.htm">More ...</a></p></FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-05-19</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EPO]</strong> The <a href="http://www.epo.co.at/">EPO</a> is currently teaming up to introduce its on-line portal <a href="http://www.epoline.org/">epoline</a> which allows, inter alia, on-line filing of patent applications, on-line file inspection, as well as on-line patent status checking. On May 25, 2001, from 09h30 till 12h30, a press briefing will be held at the Savoy in London to introduce epoline� to the press: Why should your readers know about epoline�? What's so special about this system, and how does it set new service standards in the world of IP? What are the benefits and advantages of epoline�? These and other points will be discussed at the press briefing. <a href="http://www.epoline.org/news.html">More ...</a></p></FONT></td>
</tr>


<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-05-09</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[OTHER]</strong> A new e-commerce service <a href="http://priorart.org/">PriorArt.org</a> has been launched. Its aim is to give inventors an easy and free way to disclose and publish inventions that they think should be in the public domain. To this end, PriorArt.org operates
a Web Site for "Defensive Publications". <a href="http://priorart.org/press1.jsp">More ...</a></p></FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-04-24</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[DPMA]</strong> The German Patent and Trade Mark Office has released <a href="http://www.depatisnet.de">DEPATISnet</a> offering a wealth of patent documents for free. An English user interface is announced to be available in May 2001. German patent documents published since 1987 are full text searchable. <a href="http://www.dpma.de/infos/pressedienst/pm010424.html">More ...</a></p></FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-04-18</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EPO]</strong> European patent applications - Germany is by far the most active Member State - Applications filed by the EU up by 40% since 1990 - In 1999, nearly 45 000 patent applications were filed at the European Patent Office (EPO), by inventors from the fifteen EU Member States, which was 40% up on the 1990 figure. Within the EU, Germany was the most active country with 44% of Community patent applications and almost 500 applications per million people in the labour force in 1999. [Caution - This statistics does not cover national patent applications which also have been filed in huge numbers. Moreover, not all patent applications filed in 1999 have been laid open up to today. And, finally, it seems not to be very clear whether it is an inventors or applicants regional analysis --AHH.] <a href="http://europa.eu.int/comm/eurostat/Public/datashop/print-product/EN?catalogue=Eurostat&product=9-18042001-EN-AP-EN&mode=download">More ...</a></p></FONT></td>
</tr>


<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-04-17</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[EPO]</strong> Enlarged Board of Appeal G 0001/99 dated 2001-04-02 "Reformatio in peius / 3M" <br>
Headnotes: In principle, an amended claim, which would put the opponent
and sole appellant in a worse situation than if it had not
appealed, must be rejected. However, an exception to this
principle may be made in order to meet an objection put
forward by the opponent/appellant or the Board during the
appeal proceedings, in circumstances where the patent as
maintained in amended form would otherwise have to be revoked
as a direct consequence of an inadmissible amendment held
allowable by the Opposition Division in its interlocutory
decision. <a href="http://www.european-patent-office.org/dg3/pdf/g990001ex1.pdf">More ...</a></p></FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-04-17</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[WIPO]</strong> Press Release PR/2001/263, Geneva, April 12, 2001: WIPO opens interim report on domain names to public comment. The World Intellectual Property Organization (WIPO) is seeking extensive public comment on an interim report which will culminate in a series of recommendations that aim to prevent abusive registration of domain names on the Internet.
<a href="http://www.wipo.org/pressroom/en/releases/2001/p263.htm">More ...</a></p></FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-04-17</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[UK-PTO]</strong> Should Patents be Granted for Computer Software or ways of Doing Business?
Consultation Responses received by the United Kingdom Patent Office
The responses are split between Companies/Organisations and Private Individuals. Each category is listed in alphabetical order
and are available to view in both HTML and PDF formats.
<a href="http://www.patent.gov.uk/about/consultations/responses/index.htm">More ...</a></p></FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-04-05</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">HTML</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[OHIM]</strong> Communication No 3/01 of the President of the OHIM of 12 March 2001 concerning the registration of Community trade marks for retail services: [...] A substantial number of applications for Community trade marks in respect of retail services are pending before examiners. These will now be accepted, for the purposes of classification, even if a notification of provisional refusal has already been issued. Where a decision has been taken and the appeal period has not expired the applicant should file an appeal. Interlocutory revision will be granted. Where a decision has been taken and no appeal has been filed within the prescribed period the decision has become final and cannot be reversed. [...] <a href="http://oami.eu.int/en/aspects/communications/03-01.htm">More ...</a></p></FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-03-05</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[WIPO]</strong> STANDING COMMITTEE ON THE LAW OF PATENTS - Fifth Session - Geneva, May 14 to 19, 2001 - DRAFT SUBSTANTIVE PATENT LAW TREATY prepared by the International Bureau [...] <a href="http://www.wipo.int/scp/en/documents/session_5/pdf/scp5_2p.pdf">More ...</a></p></FONT></td>
</tr>

<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-03-05</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[WIPO]</strong> STANDING COMMITTEE ON THE LAW OF PATENTS - Fifth Session - Geneva, May 14 to 19, 2001 - DRAFT REGULATIONS AND PRACTICE GUIDELINES
UNDER THE DRAFT SUBSTANTIVE PATENT LAW TREATY prepared by the International Bureau [...] <a href="http://www.wipo.int/scp/en/documents/session_5/pdf/scp5_3p.pdf">More ...</a></p></FONT></td>
</tr>


<tr>
<td width=12% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-03-05</FONT></td>
<td width=7% valign="top"><FONT FACE="Arial" SIZE="-2">PDF</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"><strong>[JP-PTO]</strong> The Japan Patent Office (JPO) released the final version of the revised "Examination Guidelines for Computer Software-related Inventions on December 28, 2000 on the JPO Website. The said Examination Guidelines were finally made after paying due consideration to the public comments on the draft revised Examination Guidelines invited through the Internet.
The parts of the said Examination Guidelines regarding the "computer program claim(s)" are applied to the applications filed on or after January 10, 2001. [...] <a href="http://www.jpo.go.jp/saikine/pdf/tt1304-001.pdf">More ...</a></p></FONT></td>
</tr>


<tr><td colspan=3><hr size="1"></td></tr>
</font>
</table>


<br>&nbsp;<br>

<H3>INTELLECTUAL PROPERTY NOTES</H3>

<table><FONT FACE="Arial" SIZE="-2">
<tr>
<td width=10% valign="top"><FONT FACE="Arial" SIZE="-2">No.</FONT></td>
<td width=15% valign="top"><FONT FACE="Arial" SIZE="-2"> DATE</FONT></td>
<td valign="top" align="center"><FONT FACE="Arial" SIZE="-2"> TITLE</FONT></td>
</tr>

<tr><td colspan=3><hr size="1"></td></tr>

<tr>
<td width=10% valign="top"><FONT FACE="Arial" SIZE="-2">
<A HREF="./ipn/2001-001.pdf">01/2001</A></FONT></td>
<td width=15% valign="top"><FONT FACE="Arial" SIZE="-2"> 2001-03-10</FONT></td>
<td valign="top"><FONT FACE="Arial" SIZE="-2"> Observations on Replies to the EU Consultation Paper on the Patentability of Computer-Implemented Inventions</FONT></td>
</tr>

<tr><td colspan=3><hr size="1"></td></tr>
</table>


<br>&nbsp;<br>

<H3>HOT SPOTS ON THE NET [OFFSITE]</h3>
<P>

<table>
<tr width="100%">
 <td valign="TOP">
 <FONT FACE="Arial" SIZE="-2">
<UL>
<LI><A HREF="http://europa.eu.int/geninfo/whatsnew.htm">What's new on the EU Website?</a></li>
<LI><A HREF="http://oami.eu.int/en/news.htm">What's new on the OHIM Website?</a></li>
<li><A HREF="http://www.wipo.org/news/en/2001/index.htm">What's new on the WIPO Website?</a></li>
<li><A HREF="http://www.dkpto.dk/indhold/nyheder/e_nyheder/forside_nyheder.htm">What's new on the DK-PTO Website? [In Danish]</a></li>
</UL>
</FONT>
</td>
 <td width="50%" valign="TOP"><FONT FACE="Arial" SIZE="-2">
 <UL>
 <li><A HREF="http://www.epo.co.at/updates.htm">What's new on the EPO Website?</a></li>
<li><A HREF="http://www.dpma.de/neu/neu.html">What's new on the DE-PTO Website? [In German]</a></li>
<li><A HREF="http://www.jpo.go.jp/wne/whate.htm">What's new on the JP-PTO Website? </a></li>
<li><A HREF="http://www.ige.ch/E/news/n1.htm">What's new on the CH-IGE Website? </a></li>
</ul>
 </FONT></td>
</tr>
</table>

<br>&nbsp;<br>

<H3>INTPROP-L MAILING LIST</h3>
<P>

A European Intellectual Property Law discussion mailing list INTPROP-L is available. INTPROP-L is an unmoderated list addressing IP professionals. ...

<br>&nbsp;<br>

For details, please click <A href="./intprop-l.php3">here.</a>

<br><br><br>


<HR SIZE=1>
<br>
</FONT> <br>
<table WIDTH="100%" border="0" cellspacing="0" cellpadding="0" BGCOLOR="#FFFFFF">
<tr><td colspan=2><FONT FACE="Arial" SIZE="-2">
Please read the <A HREF="./disclaimer.php3">Disclaimer &amp; About This Website (Pflichtangaben gem&auml;ss TDG)</a> section.<br>&nbsp;<br>
Feel free to contact PA Axel H Horns via e-mail <A href="mailto:horns@ipjur.com">horns@ipjur.com</a>. BEWARE: DO NOT SEND CONFIDENTIAL INFORMATION UNENCRYPTED VIA E-MAIL. USE OF ENCRYPTION SOFTWARE IS HIGHLY RECOMMENDED. PA AXEL H HORNS IS PROVIDING SUPPORT FOR ENCRYPTED E-MAIL MESSAGES USING PGP OR PGP COMPATIBLE FORMATS. THE PGP PUBLIC KEY FOR PA AXEL H HORNS IS AVAILABLE <A HREF="./pubkey.php3">HERE</a>.<br>&nbsp;<br>

<A HREF="./ahh.php3">Dipl.-Phys. Axel H Horns</a> is Patentanwalt (German Patent Attorney),
European Patent Attorney as well as European Trade Mark Attorney. In particular, he is Member of:</FONT><br><br>
</td>
</tr>
<tr>
 <td><a href="http://www.scl.org"><img src="./logo-new-296x37.gif" width="296" height="37" border="0"
 alt="Click here to visit the SCL Online web site" align="center"></a>
 </td>
 <td><a href="http://www.cla.org"><img src="./claclr2.jpg" width="75" height="75" border="0"
 alt="Click here to visit the CLA Online web site" align="center"></a>
</td>
</tr>
</table>
</font>
</td>
</tr>
</TABLE>
</BODY>
</HTML>