<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#EWW: Falls die Europäische Kommission nun doch demnächst einen Richtlinienvorschlag unterbreitet, könnte der schwarze Peter in einer Phase an die nationalen Regierungen zurückgegeben werden, in der drei von ihnen (DE, FR, NL) im Wahlkampf stehen.  Programmierer und Anwender freier und proprietärer Software tun gut daran, sich und ihre politischen Vertreter jetzt richtig zu informieren.

#UWi: Zwei im Auftrag der Bundesregierung erstellte %(fh:Gutachten) stellen, wie %(ss:alle Studien), eine schädliche volkswirtschaftliche Wirkung der Softwarepatente fest, empfehlen aber dennoch eine %(q:Klärung) im Sinne des EPA.  Kritik am EPA-Kurs wurde bisher nur %(bt:im Bundestag vernommen).

#Aet: Auch die Patentjuristen im 10. Senat des BGH streben seit 1992 eine Angleichung ihrer Rechtsprechung an die Praxis des EPA an, während die Naturwissenschaftler vom 17. Senat des BPatG standhaft dagegen halten.  So %(bp:erklärte) BPatG/17 im August 2000 Ansprüche auf Informationsgegenstände für unzulässig.  Der BGH hat inzwischen dieses Urteil unter Verweis auf EPA-Vorbilder wieder %(bg:aufgehoben).

#DWr: In Wirklichkeit ist jedoch das Patentreferat im Bundesministerium der Justiz (BMJ) nur ein dürftig ausgestattete Durchgangsstation für spätere EPA-Richter und Patentamtspräsidenten.  Für die BMJ-Patentreferenten kommt Opposition im Verwaltungsrat nur dann in Frage, wenn sie unmissverständliche Anweisungen von Politikern erhalten.  Immerhin brachten sie im November 2000 eine Mehrheit gegen die geplante Streichung des Ausschlusses der %(q:Programme für Datenverarbeitung) zustande, %(gb:beteuerten) aber zugleich, dass sie damit keineswegs die Praxis des EPA kritisieren wollten.  Bei der anschließenden %(q:Konsultationsübung) applaudierten die Vertreter der Bundesregierung dem %(q:Sondierungspapier).  Als das EPA kürzlich den Dammbruch von 1998 in einer Prüfungsrichtlinie %(pu:festschrieb), hatte die Bundesregierung nichts dagegen einzuwenden.

#Dtr: Die Hauptverantwortung für das %(gk:Gruselkabinett der Europäischen Softwarepatente) liegt bei den EPÜ-Unterzeichnerstaaten.  Deren Regierungen haben 1973 unabhängig von der Europäischen Gemeinschaft das EPA als zwischenstaatliches Verwaltungsorgan geschaffen.  Die Technischen Beschwerdekammern des EPA sind, anders als der Bundesgerichtshof (BGH) und das Bundespatentgericht (BPatG), nicht für grundlegende Rechtsauslegungsfragen zuständig.  Ihre Entscheidungen sind nur für das EPA verbindlich.  Gegen Fehlentwicklungen kann die Bundesregierung im EPA-Verwaltungsrat opponieren.

#OaU: Ähnliches sieht es bei dem %(ep:Patent) auf die Wiedergabe von Informationen durch binäre Verknüpfungshierarchien aus, mit dem ein Inkassobüro neuerdings 50 Firmen abmahnt, die den Web-Standard RDF implementiert oder angewendet haben.

#DPW: Diese Praxis unterscheidet sich, soweit die Interessen von Softwarentwicklern betroffen sind, nicht im geringsten von der des US-Patentamtes.  So erhielt z.B. Adobe im August 2001 nach 6 Jahre langer Prüfung vom EPA das %(ep:Patent) auf Palettenmenüs mit Reitern, wie man sie bei GIMP (und in gewisser Weise schon im Emacs-Dateipuffermenü) kennt.  Was das EPA während der sechs Jahre prüfte, bleibt unklar:  sowohl Beschreibung als auch Ansprüche wurden unverändert von der US-Anmeldung übernommen.  Mit diesem Patent hat Adobe in den USA bereits Klage gegen Macromedia erhoben.

#Dne: Die federführenden Gesetzgeber in Brüssel und Berlin ziehen derzeit die Möglichkeit eines Konfliktes mit dem EPA und seiner Lobby nicht in Betracht.  Andererseits möchten sie aber auch ungern Verantwortung für den Giftmüll übernehmen, den das EPA unentwegt auf der Datenautobahn ablagert.  Eine öffentliche %(ek:Konsultationsübung) versandete, nachdem klar wurde, dass sich lediglich eine klägliche %(q:wirtschaftliche Mehrheit) für das EPA-treue %(q:Sondierungspapier) mobilisieren ließ.  Auch zwischen den Generaldirektionen und Ministerien in Brüssel und Berlin wird das Thema gemieden.  Derweil schaffen die Patentbehörden weiter vollendete Tatsachen im Sinne einer Festschreibung der derzeitigen EPA-Praxis.

#VWm: Die Europäische Kommission wird alle drei Fragen vermutlich verneinen und dies zugleich durch Pressemeldungen auszugleichen suchen, in denen von einer %(q:klaren Absage an eine erweiterte Patentierbarkeit amerikanischen Stils) u. ä. die Rede sein wird.

#BWs: Bietet die Europäische Kommission Definitionen für Kernbegriffe wie %(e:technisch), %(e:Erfindung), %(e:technische Erfindung) usw an?  Halten diese Definitionen das Versprechen der %(e:Klärung und Harmonisierung)?  Könnte die Kommission einige Grenzfälle, wie sie beim EPA angemeldet wurden, zitieren und erklären, ob und warum es sich dabei um technische Erfindungen handelt oder nicht?

#Itc: 1986 begann das EPA, Patentansprüche auf Rechenvorgänge (einschließlich Geschäfts- und Programmlogik) zuzulassen, bei deren Ablauf nur bekannte Hardware in bestimmungsgemäßer Weise eingesetzt wird.  Ist die Europäische Kommission bereit, diese Politik des EPA zu kritisieren und unwirksam zu machen?

#Ima: 1998 %(et:begann) das EPA, Patentansprüche auf informationelle Gegenstände (%(q:Computerprogrammprodukt), Computerprogramm, Datenstruktur) zuzulassen.  Ist die Europäische Kommission bereit, diese Politik des EPA zu kritisieren und unwirksam zu machen?

#Tli: Die Eurolinux-Allianz %(wn:erwartet) von dem kommenden Richtlinienvorschlag Antworten auf drei kritische Fragen:

#Dte: Die %(ea:Eurolinux-Allianz), ein Bündnis von Verbänden und Software-Unternehmen, hat inzwischen %(ep:über 100.000 Unterschriften) für die Befolgung des %(bg:%(tpe:Europäischen Patentübereinkommens:EPÜ)) gesammelt, wonach %(e:Programme für Datenverarbeitungsanlagen) ebenso wie %(e:Pläne, Regeln und Verfahren für gedankliche und geschäftliche Tätigkeit), %(e:mathematische Methoden), %(e:Wiedergabe von Informationen) und andere Geistesleistungen, so originell sie auch sein mögen, keine %(e:technischen Erfindungen) sind, da sie uns, so die traditionelle Auslegung, %(tr:nichts neues über Wirkungszusammenhänge von Naturkräften lehren).

#descr: Der schwarze Peter liegt in Brüssel.  Alles schaut gespannt auf die Europäische Kommission.  Seit dem Frühjahr 2001 wird jeden Moment ein Vorschlag der Kommission für eine EG-Richtlinie zur %(q:Klärung und Harmonisierung) der Grenzen der Patentierbarkeit im Hinblick auf Computerprogramme erwartet.  Bislang scheuen die Gesetzgeber vor einem Konflikt mit dem Europäischen Patentamt (EPA) ebenso zurück wie vor einer Übernahme von Verantwortung für dessen Patenterteilungspraxis.

#title: Softwarepatente im Wahljahr

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnlxmg021 ;
# txtlang: de ;
# multlin: t ;
# End: ;

