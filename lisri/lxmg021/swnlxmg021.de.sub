\begin{subdocument}{swnlxmg021}{Softwarepatente im Wahljahr}{http://swpat.ffii.org/chronik/lxmg021/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Der schwarze Peter liegt in Br\"{u}ssel.  Alles schaut gespannt auf die Europ\"{a}ische Kommission.  Seit dem Fr\"{u}hjahr 2001 wird jeden Moment ein Vorschlag der Kommission f\"{u}r eine EG-Richtlinie zur ``Kl\"{a}rung und Harmonisierung'' der Grenzen der Patentierbarkeit im Hinblick auf Computerprogramme erwartet.  Bislang scheuen die Gesetzgeber vor einem Konflikt mit dem Europ\"{a}ischen Patentamt (EPA) ebenso zur\"{u}ck wie vor einer \"{U}bernahme von Verantwortung f\"{u}r dessen Patenterteilungspraxis.}
Die Eurolinux-Allianz, ein B\"{u}ndnis von Verb\"{a}nden und Software-Unternehmen, hat inzwischen \"{u}ber 100.000 Unterschriften f\"{u}r die Befolgung des Europ\"{a}ischen Patent\"{u}bereinkommens (EP\"{U}) gesammelt, wonach \emph{Programme f\"{u}r Datenverarbeitungsanlagen} ebenso wie \emph{Pl\"{a}ne, Regeln und Verfahren f\"{u}r gedankliche und gesch\"{a}ftliche T\"{a}tigkeit}, \emph{mathematische Methoden}, \emph{Wiedergabe von Informationen} und andere Geistesleistungen, so originell sie auch sein m\"{o}gen, keine \emph{technischen Erfindungen} sind, da sie uns, so die traditionelle Auslegung, nichts neues \"{u}ber Wirkungszusammenh\"{a}nge von Naturkr\"{a}ften lehren.

Die Eurolinux-Allianz erwartet von dem kommenden Richtlinienvorschlag Antworten auf drei kritische Fragen:
\begin{enumerate}
\item
1998 begann das EPA, Patentanspr\"{u}che auf informationelle Gegenst\"{a}nde (``Computerprogrammprodukt'', Computerprogramm, Datenstruktur) zuzulassen.  Ist die Europ\"{a}ische Kommission bereit, diese Politik des EPA zu kritisieren und unwirksam zu machen?

\item
1986 begann das EPA, Patentanspr\"{u}che auf Rechenvorg\"{a}nge (einschlie{\ss}lich Gesch\"{a}fts- und Programmlogik) zuzulassen, bei deren Ablauf nur bekannte Hardware in bestimmungsgem\"{a}{\ss}er Weise eingesetzt wird.  Ist die Europ\"{a}ische Kommission bereit, diese Politik des EPA zu kritisieren und unwirksam zu machen?

\item
Bietet die Europ\"{a}ische Kommission Definitionen f\"{u}r Kernbegriffe wie \emph{technisch}, \emph{Erfindung}, \emph{technische Erfindung} usw an?  Halten diese Definitionen das Versprechen der \emph{Kl\"{a}rung und Harmonisierung}?  K\"{o}nnte die Kommission einige Grenzf\"{a}lle, wie sie beim EPA angemeldet wurden, zitieren und erkl\"{a}ren, ob und warum es sich dabei um technische Erfindungen handelt oder nicht?
\end{enumerate}

Die Europ\"{a}ische Kommission wird alle drei Fragen vermutlich verneinen und dies zugleich durch Pressemeldungen auszugleichen suchen, in denen von einer ``klaren Absage an eine erweiterte Patentierbarkeit amerikanischen Stils'' u. \"{a}. die Rede sein wird.

Die federf\"{u}hrenden Gesetzgeber in Br\"{u}ssel und Berlin ziehen derzeit die M\"{o}glichkeit eines Konfliktes mit dem EPA und seiner Lobby nicht in Betracht.  Andererseits m\"{o}chten sie aber auch ungern Verantwortung f\"{u}r den Giftm\"{u}ll \"{u}bernehmen, den das EPA unentwegt auf der Datenautobahn ablagert.  Eine \"{o}ffentliche ``Konsultations\"{u}bung\footnote{http://swpat.ffii.org/papiere/eukonsult00/index.en.html}'' versandete, nachdem klar wurde, dass sich lediglich eine kl\"{a}gliche ``wirtschaftliche Mehrheit'' f\"{u}r das EPA-treue ``Sondierungspapier'' mobilisieren lie{\ss}.  Auch zwischen den Generaldirektionen und Ministerien in Br\"{u}ssel und Berlin wird das Thema gemieden.  Derweil schaffen die Patentbeh\"{o}rden weiter vollendete Tatsachen im Sinne einer Festschreibung der derzeitigen EPA-Praxis.

Diese Praxis unterscheidet sich, soweit die Interessen von Softwarentwicklern betroffen sind, nicht im geringsten von der des US-Patentamtes.  So erhielt z.B. Adobe im August 2001 nach 6 Jahre langer Pr\"{u}fung vom EPA das Patent auf Palettenmen\"{u}s mit Reitern, wie man sie bei GIMP (und in gewisser Weise schon im Emacs-Dateipuffermen\"{u}) kennt.  Was das EPA w\"{a}hrend der sechs Jahre pr\"{u}fte, bleibt unklar:  sowohl Beschreibung als auch Anspr\"{u}che wurden unver\"{a}ndert von der US-Anmeldung \"{u}bernommen.  Mit diesem Patent hat Adobe in den USA bereits Klage gegen Macromedia erhoben.

\"{A}hnliches sieht es bei dem Patent auf die Wiedergabe von Informationen durch bin\"{a}re Verkn\"{u}pfungshierarchien aus, mit dem ein Inkassob\"{u}ro neuerdings 50 Firmen abmahnt, die den Web-Standard RDF implementiert oder angewendet haben.

Die Hauptverantwortung f\"{u}r das Gruselkabinett der Europ\"{a}ischen Softwarepatente liegt bei den EP\"{U}-Unterzeichnerstaaten.  Deren Regierungen haben 1973 unabh\"{a}ngig von der Europ\"{a}ischen Gemeinschaft das EPA als zwischenstaatliches Verwaltungsorgan geschaffen.  Die Technischen Beschwerdekammern des EPA sind, anders als der Bundesgerichtshof (BGH) und das Bundespatentgericht (BPatG), nicht f\"{u}r grundlegende Rechtsauslegungsfragen zust\"{a}ndig.  Ihre Entscheidungen sind nur f\"{u}r das EPA verbindlich.  Gegen Fehlentwicklungen kann die Bundesregierung im EPA-Verwaltungsrat opponieren.

In Wirklichkeit ist jedoch das Patentreferat im Bundesministerium der Justiz (BMJ) nur ein d\"{u}rftig ausgestattete Durchgangsstation f\"{u}r sp\"{a}tere EPA-Richter und Patentamtspr\"{a}sidenten.  F\"{u}r die BMJ-Patentreferenten kommt Opposition im Verwaltungsrat nur dann in Frage, wenn sie unmissverst\"{a}ndliche Anweisungen von Politikern erhalten.  Immerhin brachten sie im November 2000 eine Mehrheit gegen die geplante Streichung des Ausschlusses der ``Programme f\"{u}r Datenverarbeitung'' zustande, beteuerten aber zugleich, dass sie damit keineswegs die Praxis des EPA kritisieren wollten.  Bei der anschlie{\ss}enden ``Konsultations\"{u}bung'' applaudierten die Vertreter der Bundesregierung dem ``Sondierungspapier''.  Als das EPA k\"{u}rzlich den Dammbruch von 1998 in einer Pr\"{u}fungsrichtlinie festschrieb, hatte die Bundesregierung nichts dagegen einzuwenden.

Auch die Patentjuristen im 10. Senat des BGH streben seit 1992 eine Angleichung ihrer Rechtsprechung an die Praxis des EPA an, w\"{a}hrend die Naturwissenschaftler vom 17. Senat des BPatG standhaft dagegen halten.  So erkl\"{a}rte BPatG/17 im August 2000 Anspr\"{u}che auf Informationsgegenst\"{a}nde f\"{u}r unzul\"{a}ssig.  Der BGH hat inzwischen dieses Urteil unter Verweis auf EPA-Vorbilder wieder aufgehoben.

Zwei im Auftrag der Bundesregierung erstellte Gutachten stellen, wie alle Studien, eine sch\"{a}dliche volkswirtschaftliche Wirkung der Softwarepatente fest, empfehlen aber dennoch eine ``Kl\"{a}rung'' im Sinne des EPA.  Kritik am EPA-Kurs wurde bisher nur im Bundestag vernommen.

Falls die Europ\"{a}ische Kommission nun doch demn\"{a}chst einen Richtlinienvorschlag unterbreitet, k\"{o}nnte der schwarze Peter in einer Phase an die nationalen Regierungen zur\"{u}ckgegeben werden, in der drei von ihnen (DE, FR, NL) im Wahlkampf stehen.  Programmierer und Anwender freier und propriet\"{a}rer Software tun gut daran, sich und ihre politischen Vertreter jetzt richtig zu informieren.

Die Eurolinux-Allianz, ein B\"{u}ndnis von Verb\"{a}nden und Software-Unternehmen, hat inzwischen \"{u}ber 100.000 Unterschriften f\"{u}r die Befolgung des Europ\"{a}ischen Patent\"{u}bereinkommens (EP\"{U}) gesammelt, wonach \emph{Programme f\"{u}r Datenverarbeitungsanlagen} ebenso wie \emph{Pl\"{a}ne, Regeln und Verfahren f\"{u}r gedankliche und gesch\"{a}ftliche T\"{a}tigkeit}, \emph{mathematische Methoden}, \emph{Wiedergabe von Informationen} und andere Geistesleistungen, so originell sie auch sein m\"{o}gen, keine \emph{technischen Erfindungen} sind, da sie uns, so die traditionelle Auslegung, nichts neues \"{u}ber Wirkungszusammenh\"{a}nge von Naturkr\"{a}ften lehren.

Die Eurolinux-Allianz erwartet von dem kommenden Richtlinienvorschlag Antworten auf drei kritische Fragen:
\begin{enumerate}
\item
1998 begann das EPA, Patentanspr\"{u}che auf informationelle Gegenst\"{a}nde (``Computerprogrammprodukt'', Computerprogramm, Datenstruktur) zuzulassen.  Ist die Europ\"{a}ische Kommission bereit, diese Politik des EPA zu kritisieren und unwirksam zu machen?

\item
1986 begann das EPA, Patentanspr\"{u}che auf Rechenvorg\"{a}nge (einschlie{\ss}lich Gesch\"{a}fts- und Programmlogik) zuzulassen, bei deren Ablauf nur bekannte Hardware in bestimmungsgem\"{a}{\ss}er Weise eingesetzt wird.  Ist die Europ\"{a}ische Kommission bereit, diese Politik des EPA zu kritisieren und unwirksam zu machen?

\item
Bietet die Europ\"{a}ische Kommission Definitionen f\"{u}r Kernbegriffe wie \emph{technisch}, \emph{Erfindung}, \emph{technische Erfindung} usw an?  Halten diese Definitionen das Versprechen der \emph{Kl\"{a}rung und Harmonisierung}?  K\"{o}nnte die Kommission einige Grenzf\"{a}lle, wie sie beim EPA angemeldet wurden, zitieren und erkl\"{a}ren, ob und warum es sich dabei um technische Erfindungen handelt oder nicht?
\end{enumerate}

Die Europ\"{a}ische Kommission wird alle drei Fragen vermutlich verneinen und dies zugleich durch Pressemeldungen auszugleichen suchen, in denen von einer ``klaren Absage an eine erweiterte Patentierbarkeit amerikanischen Stils'' u. \"{a}. die Rede sein wird.

Die federf\"{u}hrenden Gesetzgeber in Br\"{u}ssel und Berlin ziehen derzeit die M\"{o}glichkeit eines Konfliktes mit dem EPA und seiner Lobby nicht in Betracht.  Andererseits m\"{o}chten sie aber auch ungern Verantwortung f\"{u}r den Giftm\"{u}ll \"{u}bernehmen, den das EPA unentwegt auf der Datenautobahn ablagert.  Eine \"{o}ffentliche ``Konsultations\"{u}bung\footnote{http://swpat.ffii.org/papiere/eukonsult00/index.en.html}'' versandete, nachdem klar wurde, dass sich lediglich eine kl\"{a}gliche ``wirtschaftliche Mehrheit'' f\"{u}r das EPA-treue ``Sondierungspapier'' mobilisieren lie{\ss}.  Auch zwischen den Generaldirektionen und Ministerien in Br\"{u}ssel und Berlin wird das Thema gemieden.  Derweil schaffen die Patentbeh\"{o}rden weiter vollendete Tatsachen im Sinne einer Festschreibung der derzeitigen EPA-Praxis.

Diese Praxis unterscheidet sich, soweit die Interessen von Softwarentwicklern betroffen sind, nicht im geringsten von der des US-Patentamtes.  So erhielt z.B. Adobe im August 2001 nach 6 Jahre langer Pr\"{u}fung vom EPA das Patent auf Palettenmen\"{u}s mit Reitern, wie man sie bei GIMP (und in gewisser Weise schon im Emacs-Dateipuffermen\"{u}) kennt.  Was das EPA w\"{a}hrend der sechs Jahre pr\"{u}fte, bleibt unklar:  sowohl Beschreibung als auch Anspr\"{u}che wurden unver\"{a}ndert von der US-Anmeldung \"{u}bernommen.  Mit diesem Patent hat Adobe in den USA bereits Klage gegen Macromedia erhoben.

\"{A}hnliches sieht es bei dem Patent auf die Wiedergabe von Informationen durch bin\"{a}re Verkn\"{u}pfungshierarchien aus, mit dem ein Inkassob\"{u}ro neuerdings 50 Firmen abmahnt, die den Web-Standard RDF implementiert oder angewendet haben.

Die Hauptverantwortung f\"{u}r das Gruselkabinett der Europ\"{a}ischen Softwarepatente liegt bei den EP\"{U}-Unterzeichnerstaaten.  Deren Regierungen haben 1973 unabh\"{a}ngig von der Europ\"{a}ischen Gemeinschaft das EPA als zwischenstaatliches Verwaltungsorgan geschaffen.  Die Technischen Beschwerdekammern des EPA sind, anders als der Bundesgerichtshof (BGH) und das Bundespatentgericht (BPatG), nicht f\"{u}r grundlegende Rechtsauslegungsfragen zust\"{a}ndig.  Ihre Entscheidungen sind nur f\"{u}r das EPA verbindlich.  Gegen Fehlentwicklungen kann die Bundesregierung im EPA-Verwaltungsrat opponieren.

In Wirklichkeit ist jedoch das Patentreferat im Bundesministerium der Justiz (BMJ) nur ein d\"{u}rftig ausgestattete Durchgangsstation f\"{u}r sp\"{a}tere EPA-Richter und Patentamtspr\"{a}sidenten.  F\"{u}r die BMJ-Patentreferenten kommt Opposition im Verwaltungsrat nur dann in Frage, wenn sie unmissverst\"{a}ndliche Anweisungen von Politikern erhalten.  Immerhin brachten sie im November 2000 eine Mehrheit gegen die geplante Streichung des Ausschlusses der ``Programme f\"{u}r Datenverarbeitung'' zustande, beteuerten aber zugleich, dass sie damit keineswegs die Praxis des EPA kritisieren wollten.  Bei der anschlie{\ss}enden ``Konsultations\"{u}bung'' applaudierten die Vertreter der Bundesregierung dem ``Sondierungspapier''.  Als das EPA k\"{u}rzlich den Dammbruch von 1998 in einer Pr\"{u}fungsrichtlinie festschrieb, hatte die Bundesregierung nichts dagegen einzuwenden.

Auch die Patentjuristen im 10. Senat des BGH streben seit 1992 eine Angleichung ihrer Rechtsprechung an die Praxis des EPA an, w\"{a}hrend die Naturwissenschaftler vom 17. Senat des BPatG standhaft dagegen halten.  So erkl\"{a}rte BPatG/17 im August 2000 Anspr\"{u}che auf Informationsgegenst\"{a}nde f\"{u}r unzul\"{a}ssig.  Der BGH hat inzwischen dieses Urteil unter Verweis auf EPA-Vorbilder wieder aufgehoben.

Zwei im Auftrag der Bundesregierung erstellte Gutachten stellen, wie alle Studien, eine sch\"{a}dliche volkswirtschaftliche Wirkung der Softwarepatente fest, empfehlen aber dennoch eine ``Kl\"{a}rung'' im Sinne des EPA.  Kritik am EPA-Kurs wurde bisher nur im Bundestag vernommen.

Falls die Europ\"{a}ische Kommission nun doch demn\"{a}chst einen Richtlinienvorschlag unterbreitet, k\"{o}nnte der schwarze Peter in einer Phase an die nationalen Regierungen zur\"{u}ckgegeben werden, in der drei von ihnen (DE, FR, NL) im Wahlkampf stehen.  Programmierer und Anwender freier und propriet\"{a}rer Software tun gut daran, sich und ihre politischen Vertreter jetzt richtig zu informieren.
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

