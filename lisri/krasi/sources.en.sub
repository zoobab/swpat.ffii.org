\begin{subdocument}{swpatkrasi}{Software Patents: News Sources and Discussion Rounds}{http://swpat.ffii.org/news/sources/index.en.html}{Workgroup\\swpatag@ffii.org}{As the number of undeserved patents on program logics, business practises and all kinds of trivialities soars to hundreds of thousands and the dire consequences are gradually being felt, the activity of various mailing lists and newsletters gradually intensifies.  Here we try to keep track of the most important ones.}
\begin{sect}{mlist}{Mailing Lists}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
subscription info & lang & archive & Related Websites\\\hline
FFII-Neuigkeiten\footnote{http://lists.ffii.org/mailman/listinfo/neues} & de & +\footnote{http://lists.ffii.org/archive/mails/neues} & Neues vom FFII zum Thema Softwarepatente\footnote{http://swpat.ffii.org/neues/index.de.html}\\\hline
FFII News\footnote{http://lists.ffii.org/mailman/listinfo/news} & en & +\footnote{http://lists.ffii.org/archive/mails/news} & FFII News about Software Patents\footnote{http://swpat.ffii.org/news/index.en.html}\\\hline
FFII Softwarepatente-Diskussion\footnote{http://lists.ffii.org/mailman/listinfo/swpat} & de & +\footnote{http://lists.ffii.org/archive/mails/swpat} & Protecting Information Innovation against the Abuse of the Patent System\footnote{http://swpat.ffii.org/index.en.html}\\\hline
AFUL FreePatents Forum\footnote{http://liberte.aful.org/mailman/listinfo/patents} & en & +\footnote{http://liberte.aful.org/mailman/pipermail/patents} & Protecting Information Innovation against the Abuse of the Patent System\footnote{http://swpat.ffii.org/index.en.html} and protecting competition against the abuse of software patents\footnote{http://www.freepatents.org}\\\hline
PROSA Discussione Brevetti\footnote{http://lists.prosa.it/mailman/listinfo/no-patents} & it & +\footnote{http://lists.prosa.it/pipermail/no-patents/} & Prosa: Contro Gli Brevetti Software\footnote{http://nopatents.prosa.it/nopatents}\\\hline
EPO Discussion\footnote{http://www.european-patent-office.org/mail.htm} & en &  & European Patent Office\footnote{http://www.european-patent-office.org}\\\hline
Intellectual Property Law Discussion List\footnote{http://topica.com/lists/intprop-l}, operated by PA Axel Horns\footnote{http://swpat.ffii.org/players/horns/index.en.html} & en & +\footnote{http://www.topica.com/lists/intprop-l/read} & IPJUR.com\footnote{http://www.ipjur.com}\\\hline
IP Wire\footnote{http://www.ipr-helpdesk.org/ipwire} (Newsletter of IP lawyers at the European Commission, financed by DG Enterprise, in part operated by EPO Liason Bureau at the European Commission) & en & ? & IPR Helpdesk\footnote{http://www.ipr-helpdesk.org/}\\\hline
TIIP Newsletter\footnote{http://www.researchoninnovation.org/tiip/subscribe.htm} (maintained by leading american economists, focussing on reviews of research results on the economics of innovation) & en & +\footnote{http://www.researchoninnovation.org/tiip/archive/} & Research on Innovation\footnote{http://www.researchoninnovation.org/}\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{netc}{Other News Sources}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Software Patents: Discussion Rounds and News Channels\footnote{http://swpat.ffii.org/archive/forum/index.en.html}}}

\begin{quote}
As the number of undeserved patents on program logics, business practises and other trivialities soars to hundreds of thousands and the dire consequences are gradually being felt, the activity of various mailing lists and newsletters gradually intensifies.  Here we try to keep track of the most important ones.
\end{quote}
\filbreak

\item
{\bf {\bf Europe Shareware Newspage\footnote{http://www.europe-shareware.org/pages/brevets.html}}}
\filbreak

\item
{\bf {\bf IPJur News\footnote{http://www.ipjur.com/00.php3}}}

\begin{quote}
maintained by PA Axel Horns\footnote{http://swpat.ffii.org/players/horns/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf http://www.bustpatents.com/}}

\begin{quote}
website of Greg Aharonian\footnote{http://swpat.ffii.org/players/aharonian/index.en.html}, editor of a newsletter on software patents
\end{quote}
\filbreak

\item
{\bf {\bf EU News\footnote{http://europa.eu.int/geninfo/whatsnew.htm}}}

\begin{quote}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf European Commission / General Directorate for the Internal Market / Industrial Property office\footnote{}}}
\filbreak

\item
{\bf {\bf European Commission Software Patentability Project\footnote{}}}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{quote}
\filbreak

\item
{\bf {\bf WIPO News\footnote{http://www.wipo.org/news/en}}}
\filbreak

\item
{\bf {\bf EPO News\footnote{http://www.epo.co.at/updates.htm}}}
\filbreak

\item
{\bf {\bf BMJ: Copyright and Patents\footnote{http://www.bmj.bund.de/frames/eng/themen/urheberrecht\_und\_patente}}}

\begin{quote}
news page of the german Federal Ministery of Justice\footnote{http://swpat.ffii.org/players/bmj/index.de.html}

see also BMJ: Internet and Law\footnote{http://www.bmj.bund.de/frames/eng/themen/internet\_und\_recht}
\end{quote}
\filbreak

\item
{\bf {\bf RWS-Verlag: BGH-free\footnote{http://www.rws-verlag.de/bgh-free/indexfre.htm}}}

\begin{quote}
Current decisions of the German Federal Court of Justice as full texts

Gesetzeswidrige Wirtschaftspolitik des BGH-Patentsenates\footnote{http://swpat.ffii.org/players/bgh/index.de.html}
\end{quote}
\filbreak

\item
{\bf {\bf BPatG Eilunterrichtungen\footnote{http://www.bundespatentgericht.de/bpatg/entscheidungen/frmain.html}}}

\begin{quote}
Current Decisions of the German Federal Patent Court
\end{quote}
\filbreak

\item
{\bf {\bf Neues vom DPMA\footnote{http://www.dpma.de/neu/neu.html}}}

\begin{quote}
German Patent Office NewsPage
\end{quote}
\filbreak

\item
{\bf {\bf INPI\footnote{http://www.inpi.fr/inpi/html/actu/content.htm}}}

\begin{quote}
News from the French Patent Office\footnote{http://www.inpi.fr} (INPI.fr)

Plutarque\footnote{http://www.plutarque.com/expert/}
\end{quote}
\filbreak

\item
{\bf {\bf UK PTO News\footnote{http://www.patent.gov.uk/patent/changes.htm}}}

\begin{quote}
see The UK Patent Family and Software Patents\footnote{http://swpat.ffii.org/players/uk/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf Avisos y Noticias\footnote{http://www.oepm.es/internet/noticias/primera.htm}}}

\begin{quote}
ES-PTO\footnote{http://www.oepm.es/} (OEPM.es) News
\end{quote}
\filbreak

\item
{\bf {\bf DK PTO Nyheder\footnote{http://www.dkpto.dk/indhold/nyheder/e\_nyheder/forside\_nyheder.htm}}}

\begin{quote}
DK-PTO\footnote{http://www.dkpto.dk/} (DKPTO.dk) News
\end{quote}
\filbreak

\item
{\bf {\bf US PTO News\footnote{http://www.uspto.gov/main/newsandnotices.htm}}}

\begin{quote}
see The USPTO and the US Patent Lobby\footnote{http://swpat.ffii.org/players/us/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf JP PTO News\footnote{http://www.jpo.go.jp/wne/whate.htm}}}

\begin{quote}
see Patent Inflation in Japan\footnote{http://swpat.ffii.org/players/jp/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf AIPPI News\footnote{http://www.aippi.org/news.html}}}

\begin{quote}
see also Association Internationale Pour la Protection de la Propri\'{e}t\'{e} Industrielle (AIPPI)\footnote{http://swpat.ffii.org/players/aippi/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf FFII News\footnote{http://www.ffii.org/news/index.en.html}}}

\begin{quote}
How is the infrastructure of logical ideas and creations growing and thriving?   How is it being infringed or safeguarded by legal systems, in particular by private property claims on intangibles?  What are we doing to defend and cultivate it?
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{tasks}{Questions, Things To Do, How you can Help}
\ifmlhttasks
\begin{itemize}
\item
{\bf {\bf How you can help us end the software patent nightmare\footnote{http://swpat.ffii.org/group/todo/index.en.html}}}
\filbreak

\item
{\bf {\bf Regularly Monitor the news sources\footnote{http://swpat.ffii.org/archive/forum/index.en.html}}}

\begin{quote}
You can help by looking at 1 or 2 of them
\end{quote}
\filbreak

\item
{\bf {\bf Write or customise a script that alerts volunteers when a certain news page changes}}
\filbreak

\item
{\bf {\bf Search the web and submit lists of links in this form\footnote{http://swpat.ffii.org/archive/netadr.swpat}}}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatnews.el ;
% mode: latex ;
% End: ;

