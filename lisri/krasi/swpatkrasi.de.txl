<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Softwarepatente: Nachrichtenquellen und -Verteiler

#descr: In dem Maße wie die Zahl der unverdienten Patente auf Programmlogik,
Geschäftsverfahren Trivialitäten aller Art in die Hunderttausende
steigt und die schmerzhaften Folgen für immer mehr Leute spürbar
werden, steigt die Quantität und Qualität der diesbezüglichen Beiträge
auf diversen E-Foren und Nachrichtenorganen.  Wir versuchen, hier
einen Überblick über die wichtigsten zu geben.

#MiL: E-Post-Foren

#SNe: Weitere Nachrichtenquellen

#But: Anmeldungsinfo

#Src: Sprache

#Arh: Archiv

#Reb: Einschlägige Netzadressen

#oaW: betrieben von %1

#PAW: PA %1

#NhE: Neuigkeiten aus der Sicht der Immaterialgüterjuristen beim EPA und bei
der Europäischen Kommission, finaniert von EUK GD Unternehmen

#msr: geführt von amerikanischen Wirtschaftswissenschaftlern, rezensiert
Forschungsberichte auf dem Gebiet der Ökonomie der Innovation und der
Innovatoren-Vorrechte

#IuN: IPJur News

#miy: Blog of PA Axel Horns

#wiW: Website of Gregory Aharonian, editor of a newsletter on software
patents

#EWe: EU News

#WOe: WIPO News

#EON: EPO News

#ChP: BMJ.bund.de: Internet und Recht

#nhs: Nachrichten vom %(bm:Bundesministerium der Justiz)

#Ceo: Aktuelle BGH-Entscheidungen in Volltext seit 1999

#CWd: Aktuelle Entscheidungen des Bundespatentgerichts

#DAe: Neues vom DPMA

#Gtc: Nachrichtenseite des deutschen Patentamts

#Nha: Neues vom %(fp:Französischen Patentamt)

#UTN: UKPO News

#EOM: Neues vom Spanischen Patentamt

#DPW: DK PTO Nyheder

#UPW: US PTO News

#JPW: JPO News

#Rno: Diverse %(ns:Nachrichtenquellen) regelmäßig beobachten

#YbW: Sie können helfen, indem Sie sich für 1 oder 2 davon zuständig
erklären

#WWn: Ein Skript schreiben oder anwenden, welches einen menschlichen
Beobachter aufmerksam macht, wenn sich etwas tut.

#Ssk: Web recherchieren und Dokumentdatenbank in %(lf:dieser Form) anlegen

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatkrasi ;
# txtlang: de ;
# multlin: t ;
# End: ;

