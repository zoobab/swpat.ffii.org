<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: As the number of undeserved patents on program logics, business practises and all kinds of trivialities soars to hundreds of thousands and the dire consequences are gradually being felt, the activity of various mailing lists and newsletters gradually intensifies.  Here we try to keep track of the most important ones.
#title: Software Patents: News Sources and Discussion Rounds
#MiL: Mailing Lists
#SNe: Other News Sources
#But: subscription info
#Src: lang
#Arh: archive
#Reb: Related Websites
#oaW: operated by %1
#PAW: PA %1
#NhE: Newsletter of IP lawyers at the European Commission, financed by DG Enterprise, in part operated by EPO Liason Bureau at the European Commission
#msr: maintained by leading american economists, focussing on reviews of research results on the economics of innovation
#IuN: IPJur News
#miy: maintained by %(AHH)
#wiW: website of %(GA), editor of a newsletter on software patents
#EWe: EU News
#WOe: WIPO News
#EON: EPO News
#ChP: Internet and Law
#nhs: news page of the german %(bm:Federal Ministery of Justice)
#Ceo: Current decisions of the German Federal Court of Justice as full texts
#CWd: Current Decisions of the German Federal Patent Court
#DAe: Neues vom DPMA
#Gtc: German Patent Office NewsPage
#Nha: News from the %(fp:French Patent Office)
#UTN: UK PTO News
#EOM: %(oe:DK-PTO) News
#DPW: DK PTO Nyheder
#UPW: US PTO News
#JPW: JP PTO News
#Rno: Regularly Monitor the %(ns:news sources)
#YbW: You can help by looking at 1 or 2 of them
#WWn: Write or customise a script that alerts volunteers when a certain news page changes
#Ssk: Search the web and submit lists of links in %(lf:this form)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatkrasi ;
# txtlang: en ;
# End: ;

