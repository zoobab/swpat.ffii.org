<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: Im Entwurf eines weltweiten Vertrages über das Materielle Patentrecht wird vorgeschlagen, dass alle Ideen patentierbar sein sollen (kein Erfindungsbegriff, keine Technizität) und dem Recht der Staaten, eine Gegenleistung für die Monopolgewährung zu verlangen, enge Grenzen gesetzt werden sollen.  Der Entwurf geht wesentlich weiter als der TRIPS-Vertrag.
#title: WIPO will grenzenlose Patentierbarkeit und strenge Begrenzung der Patentqualität festschreiben.
#Ved: Verhandlungen über Patentrechtsverträge bei den Vereinten Nationen
#Bet: Betr
#BWO: Betr WIPO/SCP
#BWG: Betr WIPO/Genetik
#AeW: Amerikanischer Druck für Entgrenzung
#Wrk: Weitere Lektüre
#Dec: Das BMJ wies uns in einer Reihe von Schreiben um den 2001-04-11 auf folgende Verhandlungsrunden der %(wp:Weltorganisation für Geistiges Eigentum) hin:
#Dhz: Der FFII sollte möglichst vor Beginn der Sitzungen Stellung nehmen.
#NqW: Neuer WIPO-Vorstoß für grenzenlose Patentierbarkeit
#1WW: 1. Sitzung des Committee on Reform of the Patent Cooperation Treaty (PCT)
#EjW: Einige nationale Delegationen, u.a. US, FR, CH, CZ, KR haben %(pt:Vorschläge für Änderungen des PCT) unterbreitet.
#5iW: 5. Sitzung des Standing Committee of the Law of Patents (SCP)
#DHr: Das SCP wird einen Vertragsentwurf für die %(q:Harmonisierung) des materiellen Patentrechts (Draft Substantive Patent Law Treaty) diskutieren.
#Dee: Das Dokument SCP/5/2 Prov. enthält einen %(dr:Vertragsentwurf in zwei Fassungen).
#FTh: Fassung A bewegt sich im Rahmen der bisherigen Terminologie (TRIPS, USA, EPÜ), B will eine %(q:zeitgemäßere Sprache) vorschlagen, besteht aber zu wichtigen Teilen bisher aus Leerraum.
#DWn: Der FFII arbeitet an einer %(sp:Stellungnahme) zu Fassung A.
#Fio: Ferner wurde der %(ph:Vertragsentwurf für einen Patentrechtsharmonisierungsvertrag), der 1991 der Diplomatischen Konferenz in Den Haag vorlag, als WIPO-Dokument SCP/4/3/ (scp4_3.doc) im Netz veröffentlicht.
#U0c: Um 2001-04-30..05-03 tagt der %{IA} der WIPO.
#HWr: Hierbei geht es um Aspekte von Verwertungsvorrechten (Patentrecht u.a.) im Zusammenang mit
#ZhW: Zugang zu genetischen Ressourcen und Vorteilsausgleich
#Voa: Vorrechten an traditionellem Wissen, Innovationen und Kreativität
#Vkl: Vorrechten an Folklore, einschließlich Kunsthandwerk
#Ihe: Im April 2001 erhielten wir folgende Information:
#PWW: P.S: Are you aware of the pressure that the US is currently putting on WIPO to expand patentability subject matter? Right now, the pressure seems to be on %(sc:WIPOs Standing Committee on the Law of Patents) The US government is currently having a public hearing about this under the title %(q:Request for Comments on the International Effort to Harmonize the Substantive Requirements of Patent Laws). Deadline for replies is the end of this month. For more information, see %(q:Federal Register / Vol. 66, No. 53 / Monday, March 19, 2001 / Notices), page 15409. You can find a link to this document on the USPTO home page.
#Mtj: M.a.W.:  die amerikanische Delegation bei der WIPO hat ihre Regierung noch bedingungsloser auf ihrer Seite, als dies für die meisten anderen Delegationen gilt.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swnwipo014 ;
# txtlang: de ;
# End: ;

