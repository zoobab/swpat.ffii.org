<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: %(q:Softwarepatente sind Innovationsbremsen):  Mittelständler mahnen
zur Sorgfalt bei der Setzung des Rechtsrahmens.

#descr: Die HANNOVER MESSE gilt als das jährliche Schaustück der
Innovationskraft des deutschen Industrie.  Mehr als 70% der
Arbeitsplätze stellt in den meisten Branchen der Mittelstand. Ein
Schlüssel für die Stärke deutscher Unternehmer ist das im Ganzen
sichere rechtliche Umfeld.  Auf diesem Horizont ziehen jedoch Wolken
auf.

#nWs: Dr. N.N. von E.V.: %(q:Wir sind besorgt über Missverständnisse in der
deutschen Wirtschaftspolitik.  Es wird ein regelrechter Kult um
Innovation und Patente betrieben, und auf Entscheiderebene glaubt man
vielfach noch immer, mehr Patente bedeuteten mehr Innovation.  So ist
eine unüberschaubare Flut von Verbotsvorschriften entstanden, mit die
alltägliche Innovationsarbeit der Unternehmen behindert wird.

#ing: Besonders umstritten ist die Frage ob Software-Lösungen, im Jargon des
Europäischen Patentamtes seit 2000 auch %(q:computer-implementierte
Erfindungen) genannt, patentierbar sein sollen.

#ncW: Dr. Thomas Wuensche, Inhaber und Geschaeftsführer von EMS Dr. Thomas
Wuensche, eines Unternehmens fuer Automatisierungstechnik und
industrielle Kommunikation:

#rmW: In unseren Tätigkeitsfeldern Automatisierungstechnik und Automotive
Elektronik fließt ein erheblicher Teil des Entwicklungsaufwands in die
Software. Etwa 80% unserer Entwickler sind Software-Entwickler, die
Innovationsfähigkeit unseres Unternehmens hängt wesentlich von
Software ab. Das Urheberrecht schützt uns in diesem Bereich in
angemessener Weise vor Nachahmung.  Wer das Urheberrecht durch
Reverse-Engineering und Nachimplementierung umgehen möchte, kommt in
so dynamischen Märkten wie unserem viel zu spät.

#usW: Was wir fuer unsere Innovationsfähigkeit brauchen, sind Schutzrechte
mit geringem Verwaltungsoverhead wie das Copyright. Patente auf
Software treiben den Verwaltungsaufwand nach oben, schaffen
zusätzliche Rechtsunsicherheit und laufen damit unserer
Innovationsfähigkeit diametral entgegen. Die anstehenden Risiken durch
Softwarepatente haben bereits jetzt dazu geführt, dass wir
Ausbildungsplätze nicht wie in den Vorjahren besetzen, um Kapazität
fuer Umgestaltungen zur Risikoreduktion freizuschaufeln. Wir wuerden
allerdings lieber ausbilden als uns mit Softwarepatenten und daraus
resultierenden Risiken zu beschäftigen.

#WdW: Der Richtlinienentwurf des Europäischen Parlaments vom September 2003
stellt nicht sicher, dass wir in unserer Innovationsfähigkeit
zukünftig nicht von Patenten eingeschränkt werden, aber er verhindert
die schlimmsten Auswüchse.  Technische Erfindungen wie das
vielzitierte Antiblockiersystem wären auch unter der
Parlamentsrichtlinie grundsätzlich patentierbar, wobei sich der
Schutzumfang jedoch auf den Umgang mit den Naturkräften an der
Peripherie der Datenverarbeitungsanlage beschränkt.  Wenn jedoch der
Entwurf des Ministerrats durchgeht, wird jedoch nahezu jeder
Verfahrensablauf patentierbar, und schon seine Beschreibung in Form
eines Computerprogramms wird zur Patentverletzung.  Das würde uns eine
Situation wie in den USA bringen, wo Konzerne kleinere Wettbewerber
mit der Masse eines Portfolios weitgehend trivialer Patente erdruecken
und produktlose Abkassierer ihr Heil in Patentverletzungsklagen
suchen.

#Wrd: Bisherige Studien und Umfragen des Berliner Wirtschaftsministeriums
bestätigen, dass Wünsche mit seiner Einschätzung nicht alleine ist. 
Deutsche KMUs wollen keine Softwarepatente: Auf einer Skala der
Umfrage des Bundeswirtschaftsministeriums von -3 bis +3 siedeln 1214
deutsche Unternehmen auf die Frage %(q:Haben Softwarepatente
beinträchtigende oder unterstützende Auswirkungen auf Ihre
Programmiertätigkeit oder erwarten Sie solche?) im Schnitt bei -2,58
an.[1] Eine frühere Studie belegt, dass in der %(q:Sekundärbranche)
(d.h. programmierenden Unternehmen der klassischen Industrien) das
Interesse an Softwarepatenten etwa ebenso gering ist wie in der
%(q:Primärbranche).

#iii: Stefan Pollmeier, Leiter und Gründer der %(eg:ESR Pollmeier GmbH), mit
45 Mitarbeitern in Darmstadt im Automobil-Zulieferbereich tätig,
ergänzt:

#rin: %(q:Firmen wie Siemens kämpfen gerade auf dieser Messe für eine
gesetzliche Festschreibung der Patentierbarkeit von Software-Lösungen.
 Setzen sie sich damit durch, so würde die ganze Palette der
Patentameldungen rechtsbeständig, die etwa Siemens beim Europäischen
Patentamt eingereicht hat, so von reinen Geschäftsmethoden bis hin zu
%(q:medizintechnischen Verfahren) für die Buchführung in der
Arztpraxis.  Insbesondere wir in der Automatisierungstechnik haben
dann bei einem Technik-Begriff mit niedriger Hürde mit einem
Patentwettlauf zu kämpfen, bei dem alle Rechenregeln und
Organisationsabläufe nach dem Motto %(q:Informationstechnik xyz,
eingesetzt in der Automatisierungstechnik) von vorneherein patentfähig
wären.

#neW: Das Europaparlament hat in seiner ersten Lesung einige wichtige
Einschränkungen vorgeschlagen, die das schlimmste verhindern. 
Konzerne wie Siemens wollen aber nichts verhindern.  Sie sehen, dass
durch Konvergenz neue Märkte entstehen und wollen diese neuen Märkte
möglichst vollständig ihren eigenen Regeln unterwerfen.  Dazu
verbreiten sie auch gerne mal falsche Informationen über die
Standpunkte des Ministerrats und des Parlaments.  Sie tun so, als
vertrete der Ministerrat das, was in Wirklichkeit das Parlament
vertritt.  Denn sie wissen, dass bei den meisten Unternehmen nur die
Position des Parlaments konsensfähig ist.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/messe0504.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: messe0504 ;
# txtlang: xx ;
# End: ;

