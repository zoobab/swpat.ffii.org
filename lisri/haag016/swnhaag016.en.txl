<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#ved: äußert in einer Stellungnahme zum Haager Übereinkommen Bedenken gegen mögliche Verwirrung von Zuständigkeiten und schlägt Sonderbehandlung für Immaterialgüterrechte vor.  S. Menüpunkt %(q:aktuelles).

#DPa: Deutsche Patentanwaltskammer

#EtW: Eurolinux-Seite zum Haager Übereinkommen

#Dbn: Diskussionsverteiler über Wirtschaftsrecht und das Haager Übereinkommen

#Wga: Weltweiter Enteignungswettbewerb durch Haager Übereinkommen?

#Fh0: FFII-Nachricht 2000-12-16

#Ink: Informationen des US-Patentamtes

#MWn: Mitglieder der Verhandlungsrunde

#Wrn: Analyse von Cptech.org

#Wie: Dokumentation von cptech.org (amerikanische Verbraucherschutzorganisation)

#Ont: Originalentwürfe

#Etn: Es ist auch zu bedenken, dass durch die Aussicht auf ein solches Abkommen schon jetzt ein Zugzwang hin zu diesem Abkommen entsteht.  Denn auch wenn nur ein Teil der Staaten das Abkommen unterzeichnet, wird es auf die ganze Welt eine Sogwirkung entfalten.  Politik wird häufiger durch solche Sogwirkungen und %(q:Sachzwänge) als durch aktive und bewusste Willensbildung bestimmt.

#Ein: Es besteht offenbar die Gefahr, dass unsere Politiker die Probleme unterschätzen.  Verträge wie das Hager Übereinkommen werden im übrigen von Juristen ausgearbeitet, und für die Juristenzunft ist Rechtsunsicherheit weniger ein Problem als eine Einkommensquelle.  Wenn nicht eine laute öffentliche Reaktion hörbar ist, werden die Skeptiker innerhalb der Verhandlungsrunde vielleicht eher ruhig bleiben.

#Brd: Bisher haben wir mit dem BMJ und einigen Abgeordneten hierüber gesprochen.  In den zuständigen Regierungskreisen scheint die Ansicht vorzuherrschen, der vorgeschlagene Art 28 eröffne genug Spielraum, um gravierende Nachteile zu vermeiden.  Zugleich meint man, die Verhandlungen würden sich noch einige Jahre hinziehen.

#Vgi: Voraussetzung dieser Befürchtungen ist, dass mehrere Rechtsräume gleichzeitig gelten können oder Uneinigkeit darüber besteht, welcher Rechtsraum gelten soll.  Dies ist insbesondere im Bereich der Informationsdelikte in hohem Maße der Fall.

#IEW: Ich halte dieses Abkommen für schlicht verfassungswidrig, da der demokratische Prozess ausgehebelt wird. Es gelten für den Bürger plötzlich Gesetze, an denen er keinerlei demokratische Mitwirkmöglichkeit hatte. Zweitens kommt im Resultat eine de-facto Auslieferung heraus (unter ausländische Gerichtsbarkeit, auch wenn die Vollstreckung dann im Inland stattfindet), was gegen Art. 16 GG verstößt.

#Aom: Auch Tom Vogt, Systemanalytiker bei einem großen deutschen Telekommunikationsunternehmen, meint:

#Abn: Außerdem verstößt die Anwendung ausländischen Rechts auf inländische Personen gegen das Demokratie-Prinzip aus GG Art. 20 Abs. 2 %(q:Alle Staatsgewalt geht vom Volke aus.), denn die ausgeübte Staatsgewalt geht dann nicht mehr vom Volk aus, das den Staat konstitutiert, sondern von einem fremden Staat. Dies wäre verfassungswidrige Aufgabe und Auslieferung von Demokratie.

#DeW: Die Grundgesetz-Regel Art. 2 Abs. 1 %(q:Alle Menschen sind vor dem Gesetz gleich.) erfordert, dass Rechtssysteme für jeden anzuwendenden Fall in sich widerspruchsfrei sind.  Durch die Verschmelzung vieler Rechtssysteme in ein einziges entstehen Widersprüche, und zwar genau dort, wo sich die Rechtssysteme unterscheiden. Aus diesem Grund dürfte das Haager Übereinkommen verfassungswidrig sein.

#Dre: Da viele Gesetzesregeln als Rechtseinschränkungen und nicht als Rechtserweiterungen formuliert sind, wird sich ein Angeklagter schlecht auf eine ausdrückliche Berechtigung im Rechtssystem seines Heimatlandes berufen können, während ein Kläger sich leicht auf ein Rechtseinschränkung im Rechtssystem des Kläger-Landes berufen kann. Aus diesem Grund werden wahrscheinlich die jeweils schärfsten Bestimmungen angwendet, die verschiedene Rechtssysteme für ein und denselben Fall vorsehen.

#Itd: Ist nun jede Person in ihrem Heimatland nach jedem möglichen ausländischen Recht verklagbar, so ist das größte Rechtsunsicherheit für den Angeklagten. Denn während ein Kläger sich das Rechtssystem aussuchen kann, in dem er klagt (das geht besonders gut bei multinationalen Konzernen), muss der Beklagte die Urteile ausländischer Gerichte annehmen.

#Rrd: Rechtsprechung in einem Land basiert auf Gesetzen, und zwar auf denen, die in diesem Land erlassen wurden. Das ist gut so, und erforderlich aus dem Demokratieprinzip. Da in jedem Land andere Gesetze gelten, unterliegen Personen je nach Land unterschiedlichen Rechtssystemen.

#Xtz: Xuân Baldauf, Internet-Unternehmer in Leipzig, gibt zu bedenken:

#Ern: Es bestehen darüber hinaus Bedenken darüber, inwieweit es überhaupt mit Grundprinzipien der Volkssouveränität vereinbar sein kann, die Anwendung unterschiedlicher Rechtsprechungen auf ein und die selbe Person zuzulassen.

#DnI: Die Patentinflation verdankt schon jetzt einen Teil ihrer Dynamik einem Schneeballsystem, welches denjenigen belohnt, der als erstes die Patentierbarkeitskriterien lockert.  Das Haager Übereinkommen würde nach dem nun zur Debatte stehenden Entwurf diese Dynamik erheblich verstärken.  Man stelle sich vor, der Wettbewerb um Billigflaggen oder Steueroasen hielte auch im Patentwesen Einzug und Länder wie Liberia, Panama, Liechtenstein usw könnten der Welt die Marschrichtung diktieren und andere in Zugzwang setzen.  Es geht auch nicht an, das Internet zunächst in Rechtsunsicherheit zu stürzen und dann später von Fall zu Fall durch %(q:Ausnahmeregelungen im Interesse der öffentlichen Ordnung) (Art 28 des Entwurfes) nachzubessern.  Diese Rechtsunsicherheit träfe nämlich normale Personen, wie z.B. Entwickler freier Software, die sich solche Risiken nicht leisten können.

#IdW: Information kennt keine territorialen Grenzen und lässt sich daher schwer durch eine Vielfalt konkurrierender Rechtssysteme in den Griff kriegen.  Bisher wirkt die Konkurrenz verschiedener Territorien als Anreiz, den Umgang mit Informationen zu liberalisieren.  Das beschneidet gelegentlich die nationale Souveränität, wie etwa die Schwierigkeiten deutscher Behörden bei der Strafverfolgung volksverhetzender Internetangebote in den USA zeigen.  Doch an dem grundsätzlichen Problem würde auch das Haager Abkommen nichts ändern.  Es würde lediglich den Liberalisierungssog durch einen Zensursog ersetzen.  Statt eines Zugzwangs hin zu grenzenloser Informationsfreiheit entstünde eine Zugzwang hin zu grenzenloser Zensur.

#Hil: Der FFII schlägt daher vor, den Bereich des Internet, der Informationsdelikte und der Immaterialgüterrechte bis auf weiteres aus dem Haager Übereinkommen auszuschließen, ebenso wie heute der Bereich des Seerechts ausgeschlossen ist und bleibt.  Hartmut Pilch, derzeit Vorsitzender des FFII, meint dazu:

#Fno: Falls diese Entwürfe verabschiedet werden, könnten z.B. europäische Internet-Händler zu Schadensersatz wegen Verletzung amerikanischer Geschäftsverfahrens-Patente verklagt werden.  Z.B. könnte ein europäische Fluggesellschaft wegen Verwendung des Innenpunktverfahrens der linearen Programmierung bei der Anordnung ihrer Flugpläne, wegen Verkaufs von Flugscheinen per Internet, wegen Bestellung mit nur einem Mausklick o.ä. (in Europa nicht erteilte US-Patente von AT&T, Priceline und Amazon) verklagt werden.  Es wäre ihr dann vielleicht noch immer möglich, den Klagen dadurch aus dem Weg zu gehen, dass sie ihre Flugpläne nicht in englischer Sprache anbietet, Flugverbindungen in die USA streicht und ihr WWW-Angebot unergonomisch programmiert.  Außerdem könnten Schrankenbestimmungen nach Art 28 des Übereinkommens erlauben, dass bestimmte Typen exorbitanter amerikanischer Schadensersatzforderungen %(q:im Interesse der öffentlichen Ordnung) hier nicht vollstreckt werden.  Dennoch spricht wenig dafür, einer solchen Ausdehnung amerikanischer Internet-Patente auf die ganze Welt zuzustimmen.

#EWj: %(jm:Aktuellen Entwürfen) zufolge sollen Angelegenheiten des Wirtschaftsrechts und insbesondere der Immaterialgüterrechte (Patent-, Urheber-, Markenrecht) nun in den Bereich des Haager Übereinkommens mit aufgenommen werden.

#Aru: Am 6. Juni beginnt in Den Haag eine neue Verhandlungsrunde zur Erweiterung des %(hc:Haager Übereinkommens), welches die Vollstreckung ausländischer Urteile in bestimmten Bereichen erlaubt und regelt.

#Wrk: Weitere Lektüre

#Gmk: Gespräche mit Rechtspolitikern in DE

#Kba: FFII gegen Ausweitung des Haager Übereinkommens auf Informationsdelikte

#descr: On 2001-06-06 in The Hague a new round of negotiations for a broadening of the Hague Convention will start.  This convention allows execution of foreign court verdicts under certain conditions.  Current expansion plans would make US Internet patents enforcable in Europe.

#title: FFII against Application of Hague Convention to Information Offenses

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnhaag016 ;
# txtlang: en ;
# multlin: t ;
# End: ;

