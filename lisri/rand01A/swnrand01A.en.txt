<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: The World Wide Web Consortium (W3C) has in a discussion paper explained the difficulties which it is encountering in an environment where basic calculation tasks are being patented.  In order to motivate patent owners to cooperate, W3C proposes to allow %(q:less basic) standards to be usable only against payment of %(q:reasonable and non-discriminatory) of royalties to a consortium of patentees.  This has sparked controversy, particularly because the burden could be unjustifiably onerous and completely exclude free/opensource software.
#title: W3C to allow royalty-burdened standards
#Wee: W3C deliberating about fee-based standards. The W3C director explains what caused the W3C to envisage this possibility.  Apparently a group of companies is pushing for this, while other, namely Canon, HP and Oracle are not.
#CWg: Comment on proposed patent policy change on behalf of SSLUG
#CWg2: Comment on proposed patent policy change on behalf of AFUL
#EWW: A proposal of this kind cannot be promulgated like just another standard, because it implies a change of contract of the W3C members.  Before thinking about such a proposal, the W3C should oblige its members and friends to use all available means to defend free standards against patent extortionists.
#Awy: An economist explains why fee-based standards on information innovation are unjustified and generate perverse consequences, and why they could, at least in Europe, run afoul of competition law and be declared invalid.  Sees the W3C proposal as an attempt by a group of large US based companies to enforce artificially high prices in their favor and to exclude free/opensource software.
#eoi: explains from experience in ITU committees, how RAND procedures slowed down the adoption of new standards and made it extremely difficult for small companies to compete in the telecommunications area.
#ehe: explains trouble with RAND policies, their weakness in view of submarine patents and the problem with free software.
#Gat: Gartner Analyst advises against patented W3C standards
#Wem: W3C is caving in to BigCos who want to turn the Web into an entertainment channel, harming small developpers and in the long run themselves.
#Ibj: IBM patent lawyers behind the %(q:RAND) proposal, IBM server people opposed.
#Ied: It seems that pressure from Apple to collect license fees for some stupid graphics patents played a major role in bringing up the RAND proposal.
#AGt: Apple wants fees for its patent on alpha channel in web graphics
#Rnl: RAND neither reasonable nor fair.  Should be called UFO: Uniform Fee Only.
#AjW: A. Stohr: USA, Patente und W3C Standards
#VjW: Various links, comparisons to feudal ages

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: swpatgirzu ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swnrand01A ;
# txtlang: en ;
# End: ;

