<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#VjW: Diverse Verweise, Vergleiche mit Feudalismus

#AjW: A. Stohr: USA, Patente und W3C Standards

#Rnl: RAND neither reasonable nor fair.  Should be called UFO: Uniform Fee Only.

#AGt: Apple quält W3C durch Gebührenforderung für triviales Grafikpatent

#Ied: Offenbar hat Apple auf das W3C Druck ausgeübt, um für einige dumme Graphikpatente (Alphakanal) kassieren zu können.

#Ibj: IBM-Patentabteilung für gebührenpflichtige Standards, Server-Abteillung dagegen

#Wem: W3C is caving in to BigCos who want to turn the Web into an entertainment channel, harming small developpers and in the long run themselves.

#Gat: Garnter-Analystin rät gegen gebührenpflichtige WWW-Standards

#ehe: explains trouble with RAND policies, their weakness in view of submarine patents and the problem with free software.

#eoi: erklärt aus seiner Erfahrung in Normierungskommittees der ITU, wie RAND-Verfahren die Ausarbeitung neuer Normen behindert haben und den Wettbewerb im Bereich der Telekommunikation zugunsten weniger Großfirmen verzerrt haben.

#Awy: Wirtschaftswissenschaftler erklärt, warum gebührenpflichtige Standards auf Informationsinnovationen ungerechtfertigt sind und bedenkliche Folgen haben, und warum sie insbesondere in Europa gegen das Wettbewerbsrecht verstoßen und für ungültig erklärt werden könnten.  Sieht in den W3C-Vorschlägen einen Vorstoß großer US-Firmen, der darauf zielt, künstlich hohe Preise zu ihren Gunsten durchzusetzen und freie Software auszuschließen.

#EWW: Ein Vorschlag dieser Art kann den W3C-Mitgliedern nicht wie ein beliebiger Standard untergejubelt werden, denn er beinhaltet eine Vertragsänderung.  Bevor über einen solchen Vorschlag nachgedacht wird, solte das W3C alle Mittel seiner Mitglieder und Freunde für die Verteidigung freier Standards gegen Patenterpresser ausschöpfen.

#CWg2: Comment on proposed patent policy change on behalf of AFUL

#CWg: Comment on proposed patent policy change on behalf of SSLUG

#Wee: W3C denkt über gebührenpflichtige Standards nach.  Der Direktor erklärt, wie es zu der Debatte kam, und ruft zu Diskussionsbeiträgen auf.  Offenbar kommt der Druck von einer Gruppe von Firmen im W3C.

#descr: Das World Wide Web Consortium (W3C) hat in einem Diskussionspapier die Schwierigkeiten erklärt, denen es sich in einem Umfeld zunehmender Patentierung von grundlegenden Rechenaufgaben und -lösungen ausgesetzt sieht.  Um Patentinhaber dazu zu motivieren, bei der Standardisierung zu kooperieren, schlägt das W3C vor, dass %(q:weniger grundlegende) Standards auch gebührenpflichtig sein dürfen.  Eine solche Politik könnte die Mitarbeit am W3C zu einer lukrativen Angelegenheit für große Firmen machen und insoweit die Stellung des W3C stärken.  Andererseits droht aber der Ausschluss freier/quelloffener Software, die Verlangsamung der Dynamik des WWW, die Entstehung von Gräben und Schismen um proprietäre Standards und letztlich auch ein Autoritätsverlust des W3C.  Das W3C-Papier geht nur von der Situation in den USA aus und übersieht, dass der Kampf um die Patentierbarkeit von Software bei weitem nicht zu Ende ist.  Es wäre voreilig, sich jetzt %(q:mit der Realität zu arrangieren).  Wie die lebhaften Diskussionen über den W3C-Vorschlag zeigen, neigen die meisten angesehenen Programmierer zu der Auffassung, das W3C solle am Prinzip der gebührenfreien Standards auch dann festhalten, wenn dies mit gewissen Unannehmlichkeiten verbunden ist.  Bevor an faule Kompromisse gedacht wird, sollte das W3C das ganze Potential seiner Mitglieder und Unterstützer mobilisieren, um freie Standards gegen Patentangriffe durchzusetzen.

#title: W3C will gebührenpflichtige WWW-Standards erlauben

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnrand01A ;
# txtlang: de ;
# multlin: t ;
# End: ;

