<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#EPs2: Tel

#EPs: E-Post

#cas: %(fi:FFII) is a non-profit association which promotes the development of open interfaces, open source software and freely available public information. FFII coordinates a %(sp:workgroup on software patents) which is %(sp:sponsored by successful german software publishers). FFII is member of the %(el:EuroLinux Alliance).

#nWW: About FFII

#Dfl: The Open Letter is founded on numerous compact appendices, through which the interested reader is led to further well founded sources of information.  A %(ab:predecessor of this letter) from August 2000 had contributed to establishing an anti software patent consensus of all parties represented in the German Federal Parliament.  This time again the initiators are expecting broad support.

#DcW: The Federal Government is to be requested by parliamentary motions to work for the realisation of these goals.

#Eef: It is made possible to register patents instantly and free of charge by publishing them according to standardised requirements on the Net.  On the other hand, a market for patentbusters is created.  The polluter-pays-principle is established:  the costs of the patent system are born by the owners of unjustified patents.

#Eue: Debureaucratisation and Internationalisation of the Patent System

#Eie: Adequate systems for stimulating and rewarding information innovations and other mental labor are to be devised.  This may mean a combination of soft exclusion rights, voting-based rewarding systems and a stronger public commitment to the funding of research and education.

#mem: Adequate systems for promotion of information innovation

#Diu: The Lawmaker passes a resolution stating a %(er:consistent and concise interpretation of Art 52 EPC and corresponding articles of national law).  As far as necessary, lawcourts are brought back on the path of this interpretation, which represents the original spirit of the law, by explanatory amendments to patent laws.

#Pdb: Precisation of Patentability Criteria

#WWk: When someone %(q:sets standards) using software market power, these standards must be free of private claims.  Representants of publis functions have to base their communication with citizens on open standards.

#Fgi: Freedom of Access to Communication Standards

#Rmg: Rights derived from patents may be used against the industrial application of computer programs but not atainst their publication or distribution.  This can be achieved by a simple modification of national law.

#Fle: Freedom to Publish original Information Works

#AxW: The only democratic control function is residing in the national parliaments.  Therefore, several well known IT companies and associations have published an %(ob:open letter) to numerous politicians, in which they propose five law initiatives:

#AWi: An interdisciplinary team of researchers would take many weeks to evaluate these submissions.  The DGIM may be able to do the job much faster.  Their patent experts can focus on what they have learnt: rendering the opinion of the European Patent Office word by word and translating it into European laws.  To do that, they needn't even ask for the approval of the European parliament.  They just have to convoke the patent law experts from the national governments and reach a consensus among colleagues within the %(q:European Patent Corporation).

#AuK: In view of growing criticism from colleagues within the European Commission, the DGIM finaly published the IPI study together with a %(q:consultation paper) of its own, which restates the position of the European Patent Office.  No reference to any critical studies was given, not even those made by some European governmental organisations.  Thus the patent law experts of various industry associations submitted statements, and the whole %(q:European Patent Corporation) was mobilised.  But also the critics %(ek:published) hundereds of submissions.  An %(el:official statement of the Eurolinux Alliance) with 1000 pages of appendix was sent to Brussels.

#Str: No wonder then that the IPI report fails to take into consideration any scenario which would lead to less patents...  Yet it does conclude that any argument for the extension of the patent system can not claim to rest on solid economic reasoning.  The DGIM kept this study secret from March until October 2000.

#Dfe: The IPI is the patent establishment's mouthpiece, its board and membership being primarily composed of patent lawyers, judges, patent agents and large international firms with significant patent holdings.  Even worse - IPI admits that its mission is to %(q:fund research, the fruits of which will provide greatest benefit to the Institute's stakeholders).  Also, Robert Hart, one of the authors of the IPI report, is a famous advocate of software patents who in 1997 prepared the grounds for the Greenbook with %(rh:particularly unscientific methods).

#Aef: On the %(ip:relevant webpages) of the %{GDBM} it is written right at the top that the importance of the patent system as a motor of innovation %(q:cannot be underestimated).  The documents written by DGIM about the question of software patentability, starting from the %(gp:Greenpaper of 1997), are written in this spirit.  Just like the European Patent Office, the BMJ patent division and other members of the of the %(q:European Patent Corporation), the DGIM believes in the beneficial effect of all forms of %(q:property).  Its whole work is based on this simple belief.  But during the last few years increasingly painful experiences and scientific study reports have shown that this belief is wrong.  In 1999 the DGIM showed first reactions to growing criticism.  It ordered an %(ir:independent report) from the London-based %{IPI}.

#WdW: It is not known publicly who has been invited to attend the session in Brussels.  However it is to be expected that the %{GDBM} will present its plans and allow selected participants to speak, so as to reach a consensus for its position.  From the side of the German government, the patent experts of the Federal Ministery of Justice (BMJ) are expected to attend.  An %(lb:study) conducted under the auspices of this ministery warns about devastating effects of software patents.  A second study is scheduled to deliver results by mid of 2001.  This indicates that the Federal Government can hardly be interested in very rapid decisions on the part of DGIM.  Yet DGIM stresses the urgence of %(q:harmonising the legal situation).

#Din: The patent law experts of the European Commission have invited a group of patent experts from the national governments for a conference about the planned EC directive on the patentability of %(q:computer-implemented inventions), i.e. software.  Within the European Commission, the %{DGIM} under commissioner %{FB} is in charge of the matter.  According to public announcements made by DGIM so far, DGIM wants to explicitely make the current practise of the European Patent Office (EPO) binding for all European courts by beginning of February at the latest.

#GDB: DGIM

#Gri: Directorate General for the Internal Markt

#descr: IT professionals spell out five legislative initiatives to bring patent inflation under control

#title: Patent Officials rushing to legalise software patents via EU directive

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnpatg2C ;
# txtlang: en ;
# multlin: t ;
# End: ;

