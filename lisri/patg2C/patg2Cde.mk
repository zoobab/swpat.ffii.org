patg2Cde.lstex:
	lstex patg2Cde | sort -u > patg2Cde.lstex
patg2Cde.mk:	patg2Cde.lstex
	vcat /ul/prg/RC/texmake > patg2Cde.mk


patg2Cde.dvi:	patg2Cde.mk patg2Cde.tex
	latex patg2Cde && while tail -n 20 patg2Cde.log | grep references && latex patg2Cde;do eval;done
	if test -r patg2Cde.idx;then makeindex patg2Cde && latex patg2Cde;fi
patg2Cde.pdf:	patg2Cde.mk patg2Cde.tex
	pdflatex patg2Cde && while tail -n 20 patg2Cde.log | grep references && pdflatex patg2Cde;do eval;done
	if test -r patg2Cde.idx;then makeindex patg2Cde && pdflatex patg2Cde;fi
patg2Cde.tty:	patg2Cde.dvi
	dvi2tty -q patg2Cde > patg2Cde.tty
patg2Cde.ps:	patg2Cde.dvi	
	dvips  patg2Cde
patg2Cde.001:	patg2Cde.dvi
	rm -f patg2Cde.[0-9][0-9][0-9]
	dvips -i -S 1  patg2Cde
patg2Cde.pbm:	patg2Cde.ps
	pstopbm patg2Cde.ps
patg2Cde.gif:	patg2Cde.ps
	pstogif patg2Cde.ps
patg2Cde.eps:	patg2Cde.dvi
	dvips -E -f patg2Cde > patg2Cde.eps
patg2Cde-01.g3n:	patg2Cde.ps
	ps2fax n patg2Cde.ps
patg2Cde-01.g3f:	patg2Cde.ps
	ps2fax f patg2Cde.ps
patg2Cde.ps.gz:	patg2Cde.ps
	gzip < patg2Cde.ps > patg2Cde.ps.gz
patg2Cde.ps.gz.uue:	patg2Cde.ps.gz
	uuencode patg2Cde.ps.gz patg2Cde.ps.gz > patg2Cde.ps.gz.uue
patg2Cde.faxsnd:	patg2Cde-01.g3n
	set -a;FAXRES=n;FILELIST=`echo patg2Cde-??.g3n`;source faxsnd main
patg2Cde_tex.ps:	
	cat patg2Cde.tex | splitlong | coco | m2ps > patg2Cde_tex.ps
patg2Cde_tex.ps.gz:	patg2Cde_tex.ps
	gzip < patg2Cde_tex.ps > patg2Cde_tex.ps.gz
patg2Cde-01.pgm:
	cat patg2Cde.ps | gs -q -sDEVICE=pgm -sOutputFile=patg2Cde-%02d.pgm -
patg2Cde/patg2Cde.html:	patg2Cde.dvi
	rm -fR patg2Cde;latex2html patg2Cde.tex
xview:	patg2Cde.dvi
	xdvi -s 8 patg2Cde &
tview:	patg2Cde.tty
	browse patg2Cde.tty 
gview:	patg2Cde.ps
	ghostview  patg2Cde.ps &
print:	patg2Cde.ps
	lpr -s -h patg2Cde.ps 
sprint:	patg2Cde.001
	for F in patg2Cde.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	patg2Cde.faxsnd
	faxsndjob view patg2Cde &
fsend:	patg2Cde.faxsnd
	faxsndjob jobs patg2Cde
viewgif:	patg2Cde.gif
	xv patg2Cde.gif &
viewpbm:	patg2Cde.pbm
	xv patg2Cde-??.pbm &
vieweps:	patg2Cde.eps
	ghostview patg2Cde.eps &	
clean:	patg2Cde.ps
	rm -f  patg2Cde-*.tex patg2Cde.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} patg2Cde-??.* patg2Cde_tex.* patg2Cde*~
tgz:	clean
	set +f;LSFILES=`cat patg2Cde.ls???`;FILES=`ls patg2Cde.* $$LSFILES | sort -u`;tar czvf patg2Cde.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
