<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#EPs2: Tel

#EPs: E-Post

#cas: Der %{FFII} ist ein gemeinnütziger Verein, der die Entwicklung offener Schnittstellen, quelloffener Programme und frei verfügbarer öffentlicher Informationen fördert und sich für ein kraftvolles Zusammenwirken freier und proprietärer Software zum Zwecke eines langfristigen Aufbaus informationeller Gemeingüter auf der Grundlage offener Standards, fairen Wettbewerbs und der Achtung legitimer Urheberrechte einsetzt.  Der FFII koordiniert eine %(sp:Arbeitsgruppe zum Schutz der digitalen Innovation vor Softwarepatenten), die %(sj:von erfolgreichen deutschen Softwarefirmen unterstützt) wird.  Der FFII ist Gründungsmitglied der %(el:EuroLinux-Allianz für eine Freie Informationelle Infrastruktur).

#nWW: Über den FFII

#Dfl: Der Offene Brief stützt sich auf zahlreiche kompakte Anhänge, durch die interessierte Leser schnell zu weiteren fundierten Informationsquellen geführt werden.  Ein %(ab:Vorgänger dieses Briefes) vom August 2000 hatte mit dazu beigetragen, einen Konsens aller Bundestagsfraktionen gegen Softwarepatente herzustellen.  Auch diesmal rechnen die Initiatoren mit breiter Unterstützung.

#DcW: Die Bundesregierung soll mit parlamentarischen Anträgen aufgefordert werden, auf die Verwirklichung dieser Ziele hinzuarbeiten.

#Eef: Es wird möglich, Patente mit sofortiger Wirkung und kostenlos durch formgerechte Netzveröffentlichung anzumelden.  Im Gegenzug wird ein Markt für Patentinvalidierer geschaffen.  Die Kosten des Systems werden von den Anmeldern unberechtigter Patente getragen.

#Eue: Entbürokratisierung und Internationalisierung des Patentwesens

#Eie: Es werden angemessene Systeme zur Belohnung informatischer Innovationen und sonstiger geistiger Leistungen entworfen.  In Frage kommen u.a. sanfte Ausschlussrechte, abstimmungsbasierte Prämiensysteme und die Förderung der Bildung und Forschung.

#mem: Sachgerechte Systeme zur Förderung informationeller Innovationen

#Diu: Der Gesetzgeber gibt durch einen Resolution eine %(er:prägnante und widerspruchsfreie Auslegung von §1 PatG und Art. 52 EPÜ) vor.  Je nach Bedarf wird die Rechtsprechung nach und nach durch Präzisierung einschlägiger Gesetzestexte darauf verpflichtet.

#Pdb: Präzisierung der Patentierbarkeitsgrenzen

#WWk: Wenn jemand mit Software-Marktmacht %(q:Standards setzt), müssen diese Standards von privaten Ansprüchen frei sein.  Träger öffentlicher Funktionen dürfen nur über offene Standards mit dem Bürger kommunizieren.

#Fgi: Freiheit des Zugangs zu Kommunikationsstandards

#Rmg: Rechte aus Patenten können gegen die industrielle Anwendung von Computerprogrammen, nicht aber gegen ihre Veröffentlichung oder In-Verkehr-Bringung geltend gemacht werden.  Auf nationaler Ebene durch Änderung von §11 PatG zu erreichen.

#Fle: Freiheit der Veröffentlichung selbstgeschaffener Informationswerke

#AxW: Die einzige demokratische Kontrollfunktion üben nach wie vor die nationalen Parlamente aus.  Daher haben namhafte IT-Firmen und Verbände in einem %(ob:Offenen Brief) an zahlreiche Politiker im deutschsprachigen Raum fünf Gesetzesinitiativen formuliert:

#AWi: Ein großes interdisziplinäres Forscherteam bräuchte einige Wochen, um diese Unterlagen auszuwerten.  Bei der GDBM geht es offenbar schneller.  Denn dort arbeiten wenige Patentjuristen, die sich ganz auf das konzentrieren, was sie gelernt haben: die Rechtsmeinung des Europäischen Patentamtes wortgetreu wiedergeben und in Gesetzesregeln umsetzen.  Hierfür brauchen sie  noch nicht einmal die Zustimmung des Europäischen Parlaments.  Es genügt, sich mit den Patentreferenten der nationalen Regierungen kurz zu schließen und konzernsintern einen einstimmigen Beschluss zu fassen.

#AuK: Angesichts wachsender Kritik von Kollegen innerhalb der Europäischen Kommission veröffentlichte die GDBM schließlich am 19. Oktober die IPI-Studie zusammen mit einem eigenen %(q:Konsultationspapier), welches bis ins Detail den Standpunkt des Europäischen Patentamtes vertritt.  Die Öffentlichkeit wurde aufgefordert, zu diesen beiden Papieren Stellung zu nehmen.  Jeder Hinweis auf kritische Studien unterblieb.  Daraufhin gingen viele Schreiben von Patentjuristen diverser Gewerbeverbände in Brüssel ein.  Der %(q:Europäische Patentkonzern) wurde mobilisiert.  Aber auch die Kritiker %(ek:veröffentlichten) hunderte von Eingaben.  Eine %(el:offizielle Stellungnahme der Eurolinux-Allianz) wurde mit über 1000 Seiten Anhang nach Brüssel übersandt.

#Str: So verwundert es nicht, dass das IPI seiner Studie nur die Praxis des EPA zugrunde legte und keine Handlungsoption in Betracht zog, die zu weniger Patenten führen könnte.  Dennoch wiesen die Autoren darauf hin, dass es keine fundierten wirtschaftspolitischen Argumente für eine Ausweitung des Patentwesens gebe.  Die GDBM hielt diese Studie von März bis Oktober 2000 unter Verschluss.

#Dfe: Das IPI versteht sich als Sprachrohr des Patentwesens.  Sein Vorstand besteht vor allem aus Patentanwälten, Patentrichtern und Großkonzernen mit großem Patentportfolio.  Laut Satzung hat es sich zur Aufgabe gesetzt, %(q:Forschung zu fördern, deren Ergebnisse den Anteilseignern des Institutes zugute kommen werden).  Einer der Autoren des IPI-Berichts, Robert Hart, ist überdies ein bekannter Verfechter der Softwarepatentierung, der schon 1997 durch %(rh:besonders unwissenschaftliche Methoden) den Boden für das Grünbuch bereitete.

#Aef: Auf den %(ip:einschlägigen Webseiten) der %{GDBM} steht gleich eingangs zu lesen, die Wichtigkeit des Patentwesens als Motor der Innovation könne %(q:gar nicht überschätzt werden).  In diesem Geiste sind alle Dokumente der GDBM geschrieben, die sich mit der Patentierbarkeit von Software befassen, angefangen vom %(gp:Grünbuch von 1997).  Ebenso wie das Europäische Patentamt (EPA), das BMJ-Patentreferat und andere Mitglieder des %(q:Europäischen Patentkonzerns) glaubt die GDBM an die segensreiche Wirkung aller Formen des %(q:Eigentums).  Auf diesem einfachen Glauben beruht ihre Arbeitsweise.  Doch in den letzten Jahren haben zunehmend leidvolle Erfahrungen und wissenschaftliche Studien gezeigt, dass dieser Glaube falsch ist.  Wachsender Kritik begegnete die GDBM 1999 zunächst, indem sie eine %(q:unabhängige Studie) vom Londoner %{IPI} anfertigen ließ.

#WdW: Wer zu der Sitzung in Brüssel eingeladen wurde, ist nicht öffentlich bekannt.  Es ist jedoch davon auszugehen, dass im wesentlichen die Generaldirektion Binnenmarkt (GDBM) ihre Pläne präsentieren und ausgewählten Zuhörern das Rederecht einräumen wird, um einen Konsens für ihre Position zu erzielen.  Von Seiten der Bundesregierung werden Patentreferenten des Bundesministeriums der Justiz (BMJ) erwartet.  Derweil hat das Bundesministerium für Wirtschaft (BMWi) mehrere Studien über die wirtschaftlichen Auswirkungen von Softwarepatenten ausgeschrieben.   Eine %(lb:gerade veröffentlichte Studie) schildert die von Softwarepatenten drohenden Gefahren in schrille Farben.  Bei einer zweiten Studie ist erst Mitte 2001 mit Ergebnissen zu rechnen.  Dies deutet darauf hin, dass man auf Seiten der Bundesregierung an einer allzu schnellen Brüsseler Beschlussfassung nicht interessiert sein kann.  Demgegenüber drängt die GDBM auf schnelle %(q:Harmonisierung der Rechtslage).

#Din: Die Patentreferenten der Europäischen Kommission haben für Donnerstag, den 21. Dezember 2000, einen Kreis von Patentjuristen und Patentreferenten der europäischen Regierungen zu einem Sondierungsgespräch über die geplante EU-Richtlinie zur Patentierbarkeit von %(q:computer-implementierten Erfindungen) eingeladen.  Innerhalb der Europäischen Kommission ist die %{GDBM} unter Kommissar Frits Bolkestein für das Thema zuständig.  Bisherige Verlautbarungen deuten darauf hin, dass die Generaldirektion bis spätestens Anfang Februar die umstrittene Softwarepatentierungspraxis des Europäischen Patentamtes (EPA) legalisieren und für alle eurpäischen Gerichte als verbindlich vorschreiben will.

#GDB: GDBM

#Gri: Generaldirektion Binnenmarkt

#descr: IT-Fachleute wollen mit fünf Gesetzesinitiativen Patentinflation unter Kontrolle bringen

#title: Patenfunktionäre wollen Logikpatente per EU-Richtlinie legalisieren

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnpatg2C ;
# txtlang: de ;
# multlin: t ;
# End: ;

