<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Softwarepatenten in Europa: Een kort overzicht

#descr: In 20 minuten kan je leren wat er gaande is in de strijd over
softwarepatenten in Brussel. De meeste ingewikkelde kanten van het
debat komen voort uit een aantal eenvoudige randvoorwaarden. Wanneer
je dit inziet, kan je je hopelijk sterk genoeg voelen om goed
geïnformeerde artikels te schrijven over een fascinerend politiek
drama met verstrekkende gevolgen.

#lae: Waarom al deze heibel over softwarepatenten?

#reP: Computerprogramma's vanuit het perspectief van het octrooisysteem

#itW: Een stopgezette poging om Art 52 EOV te amenderen

#dWr: Een mislukte poging om het Europees Parlement te misleiden

#irm: De Raad onder de wankele controle van het octrooi-etablishment

#cen: Een %(e:octrooi) is het recht om een uitvinding te monopoliseren. Een
would-be uitvinder specificeert een waaier van activiteiten waarop hij
het alleenrecht wilt (de conclusies), en dient deze in bij een
octrooibureau. Daar wordt bekeken of deze conclusies een
%(e:uitvinding) belichamen in de zin van de wet, en of deze uitvinding
correct beschreven wordt en industrieel toepasbaar is (het formele
onderzoek). Sommige octrooibureaus zullen bovendien onderzoeken of de
uitvinding nieuw en niet voor de hand liggend is (het onderzoek ten
gronde). Indien de aanvraag het onderzoek doorstaat, zal het
octrooibureau de aanvrager exclusieve de rechten toekennen om de
uitvinding te produceren en te commercialiseren voor een periode van
20 jaar.

#Whh: Programmeren is gelijkaardig aan het schrijven van symfonieën. Wanneer
een programmeur software schrijft, verweeft hij duizenden ideeën
(algoritmen of rekenregels) tot een auteursrechtelijk beschermd werk.
Meestal zullen een aantal van de ideeën in het werk van de programmeur
nieuw en niet voor de hand liggend zijn volgens de (%(il:inherent
lage)) standaarden van het octrooisysteem. Indien veel van dergelijke
ideeën geoctrooieerd worden, wordt het %(ws:onmogelijk om software te
schrijven) zonder inbreuk te maken op octrooien. De software-auteurs
worden in feite beroofd van hun auteursrecht; ze werken onder de
voortdurende dreiging van %(bp:gechanteerd te worden door houders van
grote octrooiportfolio's). Het resultaat is dat minder software
geschreven wordt en minder nieuwe ideeën ontstaan.

#xxW: Europe already has uniform rules about what is patentable and what
not.  They are laid down in the %(ep:European Patent Convention of
1973). In Article 52, the Convention %(ae:states) that mathematical
methods, intellectual methods, business methods, computer programs,
presentation of information etc are not inventions in the sense of
patent law. There is a systematic reason for that: in the legal
tradition patents have been for concrete applications of natural
science (%(ti:technical inventions)), whereas patents on software
cover abstract ideas. When patents are applied to software, the result
is such that instead of patenting a specific mousetrap, you patent any
%(q:means of trapping mammals) (or, for an actual example, any
%(ee:means of trapping data in an emulated environment)).

#Wpp: In 1986 begon het Europese Octroobureau (EOB) met het toekennen van
octrooien die %(et:gericht waren op computerprogramma's), maar
voorgesteld werden onder het mom van %(e:procesconclusies). Deze
werden meestal als volgt verwoord:

#Woa: Proces om [gewone computerapparatuur te gebruikten], gekarakteriseerd
door ...

#cvm: De octrooien die op deze basis werden toegekend, werden als
hypothetisch beschouwd. De reden was dat programma's als zodanig,
wanneer ze via een schijf of via het Internet verspreid werden, geen
proces vormden en dus geen uitvinding waren. Om deze dubbelzinnigheid
op te lossen, nam het EOB de laatste stap naar de octrooieerbaarheid
van pure software in 1998 door %(e:programmaconclusies) %(et:toe te
staan), i.e. conclusies van de volgende vorm:

#asm: computerprogramma, gekarakteriseerd doordat [met zijn hulp een proces
volgens conclusie 1 kan uitgevoerd worden].

#WWo: Prior to taking this bold step, in 1997, the EPO had secured
commitment for plans to rewrite the law from the following key players
of the European patent system, below referred to as the %(q:European
Patent Establishment):

#iWm: de bestuurders van de octrooibureaus van de lidstaten, die in de Raad
van Bestuur van het EOB zitten

#Wxa: de octrooiadvocaten van een aantal grote bedrijven, die in de
%(q:Permanente Adviescommissie van het EOB) zitten, SACEPO

#Wai: de octrooiambtenaren van de Europese Commissie in de Industriële
Eigendomseenheid van het Directoraat Generaal voor de Interne Markt,
dat op dat moment onder Commissaris Mario Monti viel.

#twn: Het EOB heeft ondertussen %(st:meer dan 30.000) pure softwarepatenten
toegekend in afwachting van de nieuwe wetgeving, en dat aantal stijgt
tegenwoordig aan 3.000 per jaar.

#pWe: De meeste van deze octrooien zijn algemeen en triviaal en niet
beduidend anders dan gelijkaardige soorten octrooien die de VS en
Japan toelaten. Het is zelfs zo dat de drie octrooibureaus (van
Europa, de VS en Japan) een gemeenschappelijke %(tw:Trilaterale
Standaard) om dergelijke patenten toe te laten hebben opgesteld in mei
2000, samengevat onder de nieuwe noemer %(ci:in computers
geïmplementeerde uitvindingen). In een poging om de groeiende kritiek
in Europa te sussen, is het octrooi-establishment later begonnen met
verschillen te benadrukken in de behandeling van %(q:in computers
geïmplementeerde methoden voor bedrijfsvoering). Zelfs deze
verschillen zijn echter %(et:onbeduidend).

#iew: In augustus 2000 heeft de Europese Octrooiorganisatie, de
intergouvernementele organisatie die het EOB bestuurt, %(s:gepoogd om
alle uitzonderingen van Art 52 van het Europese Octrooiverdrag te
schrappen). Vanwege publieke tegenstand die ze blijkbaar niet verwacht
hadden, is deze poging gefaald.

#sWd: In 2002 heeft het Directoraat Generaal voor de Interne Markt van de
Europese Commissie (onder Monti's opvolger Frits Bolkestein)
%(es:voorstel 2002/0047) voor een Richtlijn %(q:over de
octrooieerbaarheid van in computers geïmplementeerde uitvindingen)
ingediend. Men beweerde dat de richtlijn bedoeld was om de wetten van
de lidstaten te harmoniseren en om sommige details te verduidelijken,
met als doel de excessen van het EOB in te dijken. Bij nader inzien
bleek echter dat het voorstel van de Commissie ontwikkeld was om
%(s:de onbeperkte octrooieerbaarheid zoals gehandhaafd door het EOB te
codificeren), met één uitzondering: het liet geen programmaconclusies
toe.

#abe: Op 24 september 2003 %(ep:stemde) het Europees Parlement in zijn
geheel (plenaire bijeenkomst) om %(ea: een reeks amendementen toe te
voegen aan de richtlijn), die werkelijk bereikten wat de Commissie had
geveinsd te willen bekomen: hierdoor werd een duidelijk stel uniforme
regels gecreëerd, die de niet-octrooieerbaarheid van programmeren en
bedrijfslogica herbevestigden en de vrijheid van publicatie en
interoperabiliteit verzekerden. Deze verzameling amendementen was
gebaseerd op een jaar werk van de Parlementaire commissies voor
%(cu:Cultuur en Onderwijs) en %(it:Industrie en Handel). De richtlijn
was echter opgesteld om onder de bevoegdheid van de commissie
Juridische Zaken (JURI) te vallen, die gedomineerd wordt door MEPs met
een grote affiniteit voor het octrooi-establishment. JURI had de
voorstellen van de andere commissies genegeerd en een verzameling
%(ju:valse beperkingen op octrooieerbaarheid) voorgesteld, in een
poging de plenaire vergadering te misleiden. Een massale uitbarsting
van de publieke opinie die honderden duizenden softwareprofessionals
en wetenschappers samenbracht, grotendeels gecoördineerd door
%(fi:FFII), sterkte de beslissing van het Parlement om voor echte
beperkingen op de octrooieerbaarheid te stemmen.

#eor: Volgens de %(cd:Codecisieprocedure) van de EU werd het voorstel
vervolgens behandeld door de Raad van Ministers. In de Raad is de
%(q:%(tp|Werkgroep Intellectueel Eigendom|Octrooien)) verantwoordelijk
voor het dossier. Deze groep bestaat uit exact dezelfde leden als de
Raad van Bestuur van de het EOB: de octrooiambtenaren van de nationale
regeringen.

#uWn: Na een aantal maanden van geheime onderhandelingen, produceerde de
%(q:Werkgroep) een %(cd:compromisdocument) dat %(s:alle beperkende
amendementen van het Parlement verwijderde, het voorstel van de
Commissie herinvoerde en bovendien programmaconclusies toeliet (Art
5(2)), alle voorzieningen voor vrijwaring van interoperabilteit binnen
het octrooirecht schrapte (Rec 17) en een paar valse beperkingen van
JURI toevoegde (Art 4A etc). Het resultaat was de meest extreme en
misleidende tekst die tot hiertoe gezien was, zonder ook maar enig
compromis. Publieke toegang tot dit document werd tot de laatste
minuut geweigerd %(q:vanwege de gevoelige aard van de onderhandelingen
en het ontbreken van een hoger publiek belang).

#XWe: De Raad van Ministers %(at:aanvaardde de tekst van de Werkgroep met
een kleine meerderheid) op 18 mei 2004, ondanks de klaarblijkelijke
intentie van een aantal landen om de belofte van Duitsland om tegen te
stemmen te volgen. In die sessie beweerde Duitsland echter plots
tevreden te zijn door een betekenisloos amendement; Nederland steunde
het document terwijl het toegaf dat het problematisch kon zijn; en
Commissaris Frits Bolkestein voegde een amendement toe in Artikel 4
dat, beweerde hij, duidelijk software niet-octrooieerbaar maakte,
terwijl zijn verwoording in feite enkel de misleidende terminologie
bevestigde. Hij vergat ook te vermelden dat %(s:Artikel %(tp|5|2),
door programmaconclusies toe te laten, in duidelijke bewoordingen het
tegendeel stelt). In de persconferentie die volgde op de stemming van
de Raad kon Bolkestein geen voorbeelden geven van software die volgens
het voorstel niet octrooieerbaar zou zijn. De stemming zelf was ook
opmerkelijk voor %(cv:de manier waarop het Ierse Voorzitterschap
Denemarken onder druk zette voor haar stem), waardoor de nipte
vereiste meerderheid gehaald werd.

#dot: Na wat routinebewerkingen en vertalingswerk, wordt verwacht dat de
Raad het dit voorstel formeel zal bekrachtigen in september 2004. Het
zal vervolgens terugkeren naar het Europees Parlement voor een
volgende lezing. In dit stadium kan het Parlement het gewoon
verwerpen, het aanvaarden zoals het is, of staan op amendementen
gelijkaardig aan deze die het vorige keer had goedgekeurd (al moeten
deze nu de steun van een absolute meerderheid worden). Er zullen dan
zonder twijfel pogingen komen van de uitlopers van het
octrooi-establishment in de JURI commissie om een nieuwe reeks valse
beperkingen voor te stellen, bewerend dat ze gebaseerd zijn op
diepgaande onderhandelingen en een moeilijk %(q:compromis) met de
Raad.

#fjs

#Wch

#Wnr

#atd

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/ShortIntro.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ShortIntro ;
# txtlang: xx ;
# End: ;

