<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Antonios Christofides and Hartmut PILCH">
<link href="http://epatents.hellug.gr" rev="made">
<link href="http://www.ffii.org/assoc/webstyl/news.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2005/04/11">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="Foundation for a Free Information Infrastructure, Logic Patents, intellectual property, industrial property, IP, I2P, immaterial assets, law of immaterial goods, mathematical method, business methods, Rules of Organisation and Calculation, invention, non-inventions, computer-implemented inventions, computer-implementable inventions, software-related inventions, software-implemented inventions, software patent, computer patents, information patents, technical invention, technical character, technicity, technology, software engineering, industrial character, Patentierbarkeit, substantive patent law, Nutzungsrecht, patent inflation, quelloffen, standardisation, innovation, competition, European Patent Office, General Directorate for the Internal Market, patent movement, patent family, patent establishment, patent law, patent lawyers, lobby">
<meta name="title" content="Software Patents in Europe: A Short Overview">
<meta name="description" content="In 20 minutes you can learn what is going on in the fight about software patents in Brussels.  Most of the complexities of the debate arise from a few simple parameters.  When you have learnt these, you can hopefully feel confident to write well-informed articles about a fascinating political drama with far-reaching implications.">
<title>Software Patents in Europe: A Short Overview</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<!--#include virtual="links.en.html"-->
<div id="doktit">
<h1>Software Patents in Europe: A Short Overview<br>
<!--#include virtual="../../banner0.en.html"--></h1>
</div>
<div id="dokdes">
<!--#include virtual="deskr.en.html"-->
</div>
</div>
<div id="dokmid">
<div class="sectmenu">
<ul><li><a href="#swpat">Why all this fury about software patents?</a></li>
<li><a href="#legal">Computer Programs from the Patent System's Perspective</a></li>
<li><a href="#reskr">Abortive Attempt to Amend Art 52 EPC</a></li>
<li><a href="#europarl">Failed Attempt to Fool the Parliament</a></li>
<li><a href="#cons">The Council under Faltering Control of the Patent Establishment</a></li></ul>
</div>

<div class="secttext">
<div class="secthead">
<h2><a name="swpat">Why all this fury about software patents?</a></h2>
</div>

<div class="sectbody">
A <em>patent</em> is a right to monopolize an invention. A would-be inventor specifies a scope of activities from which he wants to exclude others (the claims), and submits it to the Patent Office, which evaluates whether these claims depict an <em>invention</em> within the sense of the law and whether the invention is correctly disclosed and industrially applicable (formal examination).  Some patent offices will moreover examine whether the invention is new and non-obvious (substantive examination).  If the application passes the examination hurdles, the Patent Office grants the applicant exclusive rights to produce and market the invention for a period of 20 years.<p>Programming is similar to writing symphonies.  When a programmer writes software, he weaves together thousands of ideas (algorithms or calculation rules) into a copyrighted work. Usually some of the ideas in the programmer's work will be new and non-obvious according to the (<a href="/analysis/trivial/index.en.html">inherently low</a>) standards of the patent system.  When many such ideas are patented, it becomes <a href="http://webshop.ffii.org/">impossible to write software</a> without infringing on patents.  Software authors are in effect deprived of their copyright assets; they live under permanent threat of being <a href="http://www.forbes.com/asap/2002/0624/044.html">blackmailed by holders of large patent portfolios</a>.  As a result, less software is written and fewer new ideas appear.
</div>

<div class="secthead">
<h2><a name="legal">Computer Programs from the Patent System's Perspective</a></h2>
</div>

<div class="sectbody">
Europe already has uniform rules about what is patentable and what not.  They are laid down in the <a href="http://www.european-patent-office.org/legal/epc/">European Patent Convention of 1973</a>. In Article 52, the Convention <a href="/analysis/epc52/index.en.html">states</a> that mathematical methods, intellectual methods, business methods, computer programs, presentation of information etc are not inventions in the sense of patent law. There is a systematic reason for that: in the legal tradition patents have been for concrete applications of natural science (&quot;<a href="/analysis/korcu/index.en.html">technical inventions</a>&quot;), whereas patents on software cover abstract ideas. When patents are applied to software, the result is such that instead of patenting a specific mousetrap, you patent any &quot;means of trapping mammals&quot; (or, for an actual example, any &quot;<a href="/patents/samples/ep769170/index.en.html">means of trapping data in an emulated environment</a>&quot;).<p>In 1986 the European Patent Office (EPO) started granting patents that were <a href="/papers/epo-t840208/index.en.html">directed to computer programs</a> but presented in the guise of <em>process claims</em>, typically phrased as follows:<p><div class=cit>
1. process for [using general-purpose computing equipment], characterised by ...
</div><p>The patents granted on this basis were considered as hypothetical, because the program as such, when distributed on a disk or via the Internet, did not constitute a process and was not an invention.  To resolve this ambiguity, the European Patent Office took the final step toward patentability of pure software in 1998 by <a href="/papers/epo-t971173/index.en.html">authorising</a> <em>program claims</em>, i.e. claims of the following form:<p><div class=cit>
2. computer program, characterised by that [with its help a process according to claim 1 can be executed].
</div>
</div>

<div class="secthead">
<h2><a name="reskr">Abortive Attempt to Amend Art 52 EPC</a></h2>
</div>

<div class="sectbody">
Prior to taking this bold step, in 1997, the EPO had secured commitment for plans to rewrite the law from the following key players of the European patent system, below referred to as the &quot;European Patent Establishment&quot;:
<ol><li>the patent office administrators of the member states, sitting on the EPO's Administrative Council</li>
<li>the patent lawyers of large corporations, sitting on the &quot;Standing Advisory Committee of the European Patent Office&quot; SACEPO</li>
<li>the patent administrators of the European Commission in the Industrial Property Unit at the Directorate General for the Internal Market, at the time under commissioner Mario Monti.</li></ol><p>The EPO has meanwhile granted <a href="/patents/stats/index.en.html">more than 30,000</a> pure software patents in anticipation of the new legislation, and the number has recently been rising at a rate of 3,000 per year.<p>Most of these patents are broad and trivial and not significantly different from corresponding types of patents that the US and Japan have been allowing.  In fact the three patent offices have created a common <a href="/players/useujp/index.en.html">&quot;Trilateral Standard&quot;</a> for granting such patents in May 2000, summarised under the newly created buzzword <a href="/papers/epo-tws-app6/index.en.html">&quot;computer-implemented inventions&quot;</a>.  Later, in an attempt to soothe growing criticism in Europe, the patent establishment started to emphasize differences in the treatment of &quot;computer-implemented business methods&quot;.  However even these differences are <a href="/papers/eubsa-swpat0202/tech/index.en.html">insignificant</a>.<p>In August 2000 the European Patent Organisation, i.e. the intergovernmental organisation that runs the European Patent Office,  <strong>attempted to delete all the exclusions listed under Art 52 of the European Patent Convention</strong>.  Due to public resistance which they apparently did not anticipate, this effort failed.
</div>

<div class="secthead">
<h2><a name="europarl">Failed Attempt to Fool the Parliament</a></h2>
</div>

<div class="sectbody">
In 2002, the European Commission's Directorate for the Internal Market (under Monti's successor Frits Bolkestein) submitted <a href="/papers/eubsa-swpat0202/index.en.html">proposal 2002/0047</a> for a Directive &quot;on the patentability of computer-implemented inventions&quot;. The Directive was claimed to serve the purposes of harmonizing Member State laws and clarifying some details with the aim of preventing excesses of the EPO.  However, upon closer reading it becomes clear that the hat the Commission's proposal was designed to <strong>codify unlimited patentability as practiced by the EPO</strong>, with one exception: it did not allow program claims.<p>On September 24, 2003, the European Parliament as a whole (plenary assembly) <a href="/log/03/plen0924/index.en.html">voted</a> to <a href="/papers/europarl0309/index.en.html">incorporate a set of amendments into the Directive</a> that actually accomplished what the Commission had pretended to aim for: it created clear and uniform rules, reaffirming the non-patentability of programming and business logic and upholding freedom of publication and interoperation.  This set of amendments was based on a year of work in the Parliament's committees for <a href="http://wiki.ffii.org/Cult0312En">Culture</a> and <a href="http://wiki.ffii.org/Itre0312En">Industry</a>.  However the directive was designed to fall into the domain of the Legal Affairs Committee (JURI), which is dominated by MEPs with close affinity to the patent establishment.  JURI had ignored the proposals of the other committees and proposed a set of <a href="/log/03/juri0617/index.en.html">fake limits on patentability</a>, in an attempt to fool the plenary.  A groundswell of public opinion involving hundreds of thousands of software professionals and scientists, largely coordinated by the <a href="http://www.ffii.org/index.en.html">FFII</a>, helped to strengthen the Parliament's resolve to vote for real limits on patentability.
</div>

<div class="secthead">
<h2><a name="cons">The Council under Faltering Control of the Patent Establishment</a></h2>
</div>

<div class="sectbody">
Pursuant to the European Union's <a href="/papers/eubsa-swpat0202/decid/index.en.html">Codecision Procedure</a>, the amended proposal was next examined by the Council of Ministers.  Within the Council, the &quot;Working Party on Intellectual Property (Patents)&quot; is considered to be in charge of the dossier.  This group consists of exactly the same members as the Administrative Council of the European Patent Office: the patent office administrators from the national governments.<p>After a few months of secret negotiations, the &quot;Working Party&quot; produced a <a href="/papers/europarl0309/cons0401/index.en.html">compromise document</a> that <strong>removed all of the Parliament's limiting amendments, reinstated the Commission's proposal, and, in addition, allowed program claims</strong> (Art 5(2)), disallowed any interoperability privilege within patent law (Rec 17) and inserted some fake limits from JURI (Art 4A etc), resulting in the most extreme, uncompromising and deceptive proposal of all seen so far.  Access to this document was denied until the very last minute &quot;due to the sensitive nature of the negotiations and the absence of an overriding public interest.&quot;<p>On 18 May 2004, the Council of Ministers <a href="/log/04/cons0518/index.en.html">approved the Working Party's text by a slim majority</a>, despite the apparent intention of a number of countries to follow Germany's lead in promising to vote against it. In that session, Germany claimed to be satisfied by a meaningless amendment; the Netherlands supported the document while admitting that it might be problematic; and Commissioner Frits Bolkestein inserted an amendment in Article 4 which, he claimed, clearly made software unpatentable, when in fact his phrasing merely reasserted deceptive terminology, while he failed to mention that <strong>Article 5 (2), by allowing program claims, unambiguously states the opposite</strong>.  In the press conference that followed the Council vote, Bolkestein failed to offer any examples of software that would not be patentable under the proposal. The Council vote was also notable for <a href="http://wiki.ffii.org/ConsVideo0405En">the way in which the Irish Presidency pressed Denmark for her votes</a>, by which the slim majority was secured.<p>As with other intergovernmental agreements, Council decisions need to be ratified in a second stage.  In the Council's language, the &quot;political agreement&quot; of the 18th of May had to be &quot;adopted&quot; to become a &quot;common position&quot; of the Council.  It soon became dubious whether this would happen.  Shortly after the session in the Council, the Polish government <a href="http://wiki.ffii.org/Pietras040520En">explained</a> that Poland did not support the proposal, and the Dutch Parliament <a href="http://wiki.ffii.org/NlMot040701En">voted</a> for a motion of distrust against its government, which it said had <a href="http://wiki.ffii.org/NlParl050603En">misinformed the Parliament</a>, and asked the government to withdraw its support for the Council text.  In Germany likewise all parties in the Parliament voted for a <a href="/papers/europarl0309/bundestag0411/index.en.html">motion</a> which criticises the Council text and demands changes &quot;in the spirit of the Parliament's first reading&quot;.<p>In november 2004 the Council's rules for qualified majorities changed, and Poland reaffirmed its opposition to the Council's text, with the effect that the text no longer enjoyed the support of a qualified majority of member states.  However, the Dutch Council presidency insisted that it should be adopted nonetheless, without a recount of votes, arguing that in the Council's history political agreements were always adopted, and failure to do so this time would create an undesirable precedent.<p>On December 21st December 2004 the Dutch presidency scheduled the outdated political agreement for adoption without vote by the Agriculture and Fisheries Council.  This was <a href="/log/04/cons1221/index.en.html">prevented</a> by a surprise appearance of the Polish minister of science and informatisation.  A similar attempt in early February 2005 under the Luxemburg Presidency was also delayed at Poland's request.  However, Poland's government failed to officially ask for a recounting of the votes (reclassification as b-item) .  Instead they wrote a strong <a href="http://wiki.ffii.org/ConsPl0502En">unilateral statement</a> against the Council's text and supported a request by the European Parliament for a restart of the procedure.  Normally the European Commission is expected to comply with such requests by presenting a new directive proposal to the Parliament.  This time however, the Commission <a href="http://wiki.ffii.org/Com050228En">refused</a> without giving an explanation.<p>A few days later, at the <a href="http://wiki.ffii.org/Cons050307En">Council meeting of March 7th</a>, the Luxemburg Presidency declared the Political Agreement to have been adopted, in spite of requests for renegotiations which the Presidency said it had received from Denmark, Poland and Portugal.  Normally, according to the Council's own rules of procedure, such requests lead to renegotiation (reclassification of an a-item as b-item) .  It is still <a href="/letters/cons050308/index.en.html">unclear</a> how the Council managed to adopt the agreement.  The most likely explanation is that the Danish minister <a href="http://wiki.ffii.org/BendtBendtsenEn">Bendt Bentsen</a> did not really request renegotiation but was only playing theater according to script worked out the night before together with the Luxemburg Presidency.  The theater performance was needed, because the minister had <a href="http://wiki.ffii.org/Dkparl050304En">binding instructions from his Parliament</a> to ask for renegotiation.  The Dutch minister, <a href="http://wiki.ffii.org/LaurensJanBrinkhorstEn">Laurens Jan Brinkhorst</a>, was also obliged by his Parliament to support renegotiation.  When Brinkhorst spoke at the Council's &quot;public session&quot;, the microphones were switched off.  Thus we do not know what he said.<p>In early April the European Parliament decided to conduct a second reading on the basis of the Council's &quot;Uncommon Position&quot;, as it was now colloquially called.  In a second reading, the Parliament can vote for amendments similar to the ones for which it had previously voted, but the majority requirements are higher than in the 1st reading.  Any absent MEP will be counted as voting for the Council.  There will without doubt be attempts by the patent establishment's allies in the Parliament to propose another set of fake limits and to pretend that this is based on meaningful negotiation and difficult &quot;compromises&quot; with the Council.  JURI will vote in June, the plenary in July.  If the Parliament succedes in maintaining the spirit of September 2003, it can look forward to negotiating with a disunited Council from a position of moral strength.  However, the grip of the patent establishment on the national ministries and the Commission is still quite tight.  Unless it can be loosened within a few months, nothing better than a heroic failure of the directive project will be achievable for the time being.
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<!--#include virtual="doksrow.en.html"-->
</div>
<!--#include virtual="../../valid.en.html"-->
<div id="dokadr">
http://swpat.ffii.org/log/intro/index.en.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2005/04/11
<a href="http://epatents.hellug.gr">Antonios Christofides</a><br>
english version 2004/06/01 by <a href="http://www.a2e.de">Hartmut PILCH</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/ShortIntro.el -->
<!-- mode: html -->
<!-- End: -->

