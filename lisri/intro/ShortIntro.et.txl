<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Tarkvarapatendid Euroopas: lühike ülevaade

#descr: Siin saate umbes 20 minutiga teada, mis toimub Brüsselis seoses
võitlusega tarkvarapatentide teemal. Enamik kogu debati keerukusi
tuleneb vaid mõnest lihtsast üksikasjast. Kui olete need teada saanud,
võite end loodetavasti tunda piisavalt kindlalt, et kirjutada
teadlikke artikleid sellest poliitilisest draamast, millel on
kaugeleulatuv mõju.

#lae: Miks kogu see viha tarkvarapatentide vastu?

#reP: Arvutiprogrammid patendisüsteemi vaatevinklist

#itW: Nurjunud katse muuta Euroopa patendikonventsiooni artiklit 52

#dWr: Ebaõnnestunud katse lollitada Euroopa Parlamenti

#irm: Nõukogu patendiladviku vankuva kontrolli all

#cen: %(e:Patent) kujutab endast õigust monopoliseerida mingi leiutis.
Leiutaja määrab kindlaks sellise tegevusulatuse, millest ta tahab
teised välja jätta (patendinõude), ning esitab selle patendiametile,
kes hindab, kas need nõudmised kujutavad %(e:leiutist) õiguslikus
tähenduses ja kas leiutis on korrektselt esitatud ja tööstuslikult
rakendatav (formaalne läbivaatamine). Lisaks sellele kontrollivad
mõned patendiametid ka seda, kas leiutis on uudne ja
mitte-enesestmõistetav (sisuline läbivaatamine). Kui patenditaotlus
läbib kontrolli edukalt, annab patendiamet taotlejale välja
ainuõigused selle leiutise tootmiseks ja turustamiseks järgneva 20
aasta jooksul.

#Whh: Programmeerimine on nagu sümfoonia kirjutamine. Kui programmeerija
kirjutab tarkvara, põimib ta kokku tuhandeid ideid (algoritme või
arvutusreegleid) teosesse, mida kaitseb autoriõigus. Tavaliselt on
programmeerija töös mõned ideed uudsed ja ei ole enesestmõistetavad,
kui võtta aluseks patendisüsteemi (%(il:olemuslikult madalad))
standardid. Kui palju selliseid ideid patenditakse, %(ws:ei ole enam
võimalik kirjutada tarkvara) ilma olemasolevaid patente rikkumata.
Seega jäetakse tarkvara autorid tegelikult ilma nende autoriõigusega
kaitstud varadest; nad elavad alalises hirmus, et %(bp:suurte
patendiportfellide omanikud neid šantažeerima hakkavad). Selle
tulemusel kirjutatakse vähem tarkvara ja tuleb vähem uusi ideid.

#xxW: Euroopas juba on ühtsed reeglid selle kohta, mida saab patentida ja
mida mitte. Need reeglid on sätestatud %(ep:1973. aasta Euroopa
patendikonventsioonis). Selle konventsiooni artikliga 52 %(ae:on ette
nähtud), et matemaatilisi meetodeid, intellektuaalseid meetodeid,
ärimeetodeid, arvutiprogramme, info lihtsat esitamist jms. ei
käsitleta patendiõiguses leiutistena. Sellel on süsteemne põhjus:
õigustava kohaselt on patendid olnud ette nähtud loodusteaduste
konkreetsete rakenduste (%(ti:tehniliste leiutiste)) jaoks, samas kui
tarkvarapatendid hõlmavad abstraktseid ideid. Kui patente hakatakse
rakendama tarkvara suhtes, on tulemus selline, et konkreetset liiki
hiirelõksu asemel saab patentida igasuguse %(q:imetajate püüdmise
vahendi) (ehk, kui tugineda tegelikule näitele, mis tahes %(ee:vahendi
andmete püüdmiseks emuleeritud keskkonnas)).

#Wpp: 1986. aastal hakkas Euroopa Patendiamet (EPO) välja andma patente, mis
olid %(et:suunatud arvutiprogrammidele), kuid mida esitati
maskeeritult %(e:protsessidega seotud nõuete) sildi all ja mis olid
tavaliselt sõnastatud järgmiselt:

#Woa: protsess [üldotstarbelise arvutustehnika kasutamiseks], mida
iseloomustab ...

#cvm: Selle alusel välja antud patente peeti hüpoteetiliseks, sest kettal
või interneti kaudu levitatav programm kui selline ei kujuta endast
protsessi ega ole leiutis. Selle ebamäärasuse lahendamiseks astus
Euroopa Patendiamet 1998. aastal viimase sammu puhttarkvara
patenditavuse poole, %(et:lubades) esitada %(e:nõudeid programmide
suhtes), s.o. järgmise sõnastusega nõudeid:

#asm: arvutiprogramm, mida iseloomustab see, et [selle abil on võimalik
käivitada protsess vastavalt nõudele 1].

#WWo: Enne selle julge sammu astumist oli Euroopa Patendiamet 1997. aastal
võtnud endale kohustuse seaduse ümberkirjutamiseks, ning selle
kohustuse sai ta Euroopa patendisüsteemi järgmistelt olulistelt
isikutelt, kellele on edaspidi viidatud kui %(q:Euroopa
patendiladvikule):

#iWm: liikmesriikide patendiametite juhid, kes istuvad EPO nõukogus;

#Wxa: suurte korporatsioonide patendijuristid, kes istuvad SACEPOs -
%(q:Euroopa Patendiameti alalises nõuandekomitees);

#Wai: Euroopa Komisjoni patendihaldurid siseturu peadirektoraadi
tööstusomandi osakonnas, mis tol ajal töötas Komisjoni liikme Mario
Monti juhtimisel.

#twn: Sellele järgnenud ajal on EPO välja andnud %(st:rohkem kui 30000)
puhttarkvaralist patenti, eeldades uute õigusaktide jõustumist, ning
see arv on viimasel ajal kasvanud tempos 3000 uut patenti aastas.

#pWe: Enamik neid patente on laiahaardelised ja triviaalsed ning ei erine
oluliselt samalaadsetest patentidest, mida lubatakse Ameerika
Ühendriikides ja Jaapanis. Tegelikult kehtestasid need kolm
patendiametit mais 2000 ühise %(tw:kolmepoolse standardi) selliste
patentide väljaandmiseks, mis võeti kokku uudisterminiga „arvutis
rakendatavad leiutised" %(ci:computer-implemented inventions). Hiljem,
püüdes leevendada Euroopas järjest kasvavat kriitikalainet, hakkas
patendiladvik rõhutama erinevusi %(q:arvutis rakendatavate
ärimeetodite) käsitlemisel. Sellegipoolest on need erinevused
%(et:tähtsusetud).

#iew: Augustis 2000 tegi Euroopa Patendiorganisatsioon (s.o. Euroopa
Patendiameti tegevust korraldav valitsustevaheline organisatsioon)
%(s:katse tunnistada kehtetuks kõik erandid, mis on loetletud Euroopa
patendikonventsiooni artiklis 52). Avalikkuse vastuseisu tõttu, mida
nad ilmselt ei oodanud, see jõupingutus nurjus.

#sWd: 2002. aastal esitas Euroopa Komisjoni siseturu peadirektoraat (mida
nüüd juhtis Monti järeltulija Frits Bolkestein) %(es:ettepaneku
2002/0047) direktiiviks %(q:arvutis rakendatavate leiutiste
patenditavuse kohta). Seejuures väideti, et direktiivi eesmärk on
ühtlustada liikmesriikide seadusi ja selgitada mõningaid üksikasju, et
vältida EPO-poolseid liialdusi. Lähemal lugemisel saab siiski selgeks,
et Komisjoni ettepanek on esitatud selleks, et %(s:kirjutada
seadusesse sisse EPO praktiseeritav piiramatu patenditavus) ühe
erandiga: see ei võimaldanud esitada patendinõudeid programmide
suhtes.

#abe: 24. septembril 2003 %(ep:hääletas) Euroopa Parlamendi täiskogu selle
poolt, et %(ea:inkorporeerida kõnealusesse direktiivi rida muudatusi),
mis olid tegelikult suunatud selleks, mille poole püüdlemist Komisjon
teeskles: nende muudatustega loodi selged ja ühtsed reeglid, rõhutades
veelkord programmeerimis- ja äriloogika mittepatenditavust ning
kinnitades avaldamise ja koostalitluse vabadust. See muudatuste
komplekt põhines parlamendi %(cu:kultuurikomisjoni) ja
%(it:tööstuskomisjoni) aastapikkusel tööl. Paraku langes see direktiiv
EP õigusasjade komitee (JURI) pädevusalasse ning selles komitees
domineerivad patendiladvikuga tihedalt seotud parlamendisaadikud. JURI
ignoreeris teiste komiteede ettepanekuid ning pani hoopis ette rea
%(ju:võltspiiranguid patenditavuse suhtes), et parlamendi täiskogu
lolliks teha. Avaliku arvamuse laine, milles töötasid kaasa sajad
tuhanded tarkvaraprofessionaalid ja teadlased, keda suuresti
koordineeris %(fi:FFII), aitas tu  gevdada parlamendi veendumust
hääletada patenditavuse tegeliku piiramise poolt.

#eor: Euroopa Liidu %(cd:kaasotsustamismenetluse) korra kohaselt vaatas
muudetud ettepaneku järgmiseks läbi Euroopa Liidu Nõukogu (nn.
ministrite nõukogu). Nõukogus peetakse kogu selle aktipaketi eest
vastutavaks %(q:%(intellektuaalomandi (patentide) töörühma)). See rühm
koosneb täpselt samadest liikmetest, kes on Euroopa Patendiameti
nõukogus: riikide valitsuste patendiametite juhtidest.

#uWn: Pärast mõnekuulisi salajasi läbirääkimisi esitas %(q:töörühm)
%(cd:kompromissdokumendi), mis %(s: kõrvaldas kõik parlamendi piiravad
muudatused, ennistas Komisjoni ettepaneku, täiendavalt lubades ka
patendinõudeid programmide suhtes) (art 5 lg 2), disallowed any keelas
igasugused koostalitlusprivileegid patendiõiguses (põhjendus 17) ning
lisas paar JURI esitatud võltspiirangut; selle tulemusel tekkis
senistest kõige äärmuslikum, kompromissitum ja valelikum ettepanek.
Juurdepääsu sellele dokumendile takistati kuni viimase hetkeni,
%(q:põhjendades seda „läbirääkimiste tundliku laadi ja olulise avaliku
huvi puudumisega".)

#XWe: 18. mail 2004 %(at:kiitis ministrite nõukogu töörühma esitatud teksti
napi häälteenamusega heaks), hoolimata mitmete riikide selgest
kavatsusest selle vastu hääletada, järgides Saksamaa eeskuju, kes oli
lubanud seda teha. Kõnealusel istungil nõustus Saksamaa, et teda
rahuldab üks teatav tähtsusetu muudatus, Holland toetas dokumendi,
tunnustades siiski, et see võib olla problemaatiline, ning Komisjoni
liige Frits Bolkestein lisas artiklisse 4 muudatuse, mis tema väitel
selgesti muutis tarkvara mittepatenditavaks, kuid tegelikult tema
sõnastus ainult kinnitas valelikku terminoloogiat ning ta jättis
mainimata, et %(s:artikli 5 lõige 2, millega lubatakse esitada nõudeid
programmide suhtes, kinnitab sõnaselgelt vastupidist). Nõukogu
hääletamisele järgnenud pressikonverentsil ei suutnud Bolkestein tuua
ühtegi näidet sellise tarkvara kohta, mis ei oleks selle ettepaneku
kohaselt patenditav. Nõukogu hääletamine oli tähelepanuväärne ka selle
pooles  t, %(cv:mil viisil Iirimaa kui eesistuja avaldas survet Taani
hääle saamiseks), millega see napp häälteenamus kindlustatigi.

#dot: Pärast mõningast rutiinset toimetamist ja tõlkimist kinnitab Nõukogu
eeldatavasti selle ettepaneku avalikult juunis 2004. Seejärel liigub
tekst Euroopa Parlamenti järjekordsele lugemisele. Selles etapis võib
Euroopa Parlament kas eelnõu otseselt tagasi lükata, aktsepteerida
seda olemasoleval kujul või nõuda muudatusi, mis sarnanevad nendega,
mille poolt ta juba ühe korra hääletas. Ei ole kahtlust, et
patendiladviku käepikendused JURIs püüavad esitada järjekordse
komplekti võltspiiranguid ning teeselda, et need põhinevad
sisutihedatel läbirääkimistel ja Nõukogu-sisestel keerukatel
%(q:kompromissidel).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/ShortIntro.el ;
# mailto: andresaule@fastmail.fm ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ShortIntro ;
# txtlang: xx ;
# End: ;

