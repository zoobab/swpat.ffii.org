<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Software Patents in Europe: A Short Overview

#descr: In 20 minutes you can learn what is going on in the fight about
software patents in Brussels.  Most of the complexities of the debate
arise from a few simple parameters.  When you have learnt these, you
can hopefully feel confident to write well-informed articles about a
fascinating political drama with far-reaching implications.

#lae: Why all this fury about software patents?

#reP: Computer Programs from the Patent System's Perspective

#itW: Abortive Attempt to Amend Art 52 EPC

#dWr: Failed Attempt to Fool the Parliament

#irm: The Council under Faltering Control of the Patent Establishment

#cen: A %(e:patent) is a right to monopolize an invention. A would-be
inventor specifies a scope of activities from which he wants to
exclude others (the claims), and submits it to the Patent Office,
which evaluates whether these claims depict an %(e:invention) within
the sense of the law and whether the invention is correctly disclosed
and industrially applicable (formal examination).  Some patent offices
will moreover examine whether the invention is new and non-obvious
(substantive examination).  If the application passes the examination
hurdles, the Patent Office grants the applicant exclusive rights to
produce and market the invention for a period of 20 years.

#Whh: Programming is similar to writing symphonies.  When a programmer
writes software, he weaves together thousands of ideas (algorithms or
calculation rules) into a copyrighted work. Usually some of the ideas
in the programmer's work will be new and non-obvious according to the
(%(il:inherently low)) standards of the patent system.  When many such
ideas are patented, it becomes %(ws:impossible to write software)
without infringing on patents.  Software authors are in effect
deprived of their copyright assets; they live under permanent threat
of being %(bp:blackmailed by holders of large patent portfolios).  As
a result, less software is written and fewer new ideas appear.

#xxW: Europe already has uniform rules about what is patentable and what
not.  They are laid down in the %(ep:European Patent Convention of
1973). In Article 52, the Convention %(ae:states) that mathematical
methods, intellectual methods, business methods, computer programs,
presentation of information etc are not inventions in the sense of
patent law. There is a systematic reason for that: in the legal
tradition patents have been for concrete applications of natural
science (%(ti:technical inventions)), whereas patents on software
cover abstract ideas. When patents are applied to software, the result
is such that instead of patenting a specific mousetrap, you patent any
%(q:means of trapping mammals) (or, for an actual example, any
%(ee:means of trapping data in an emulated environment)).

#Wpp: In 1986 the European Patent Office (EPO) started granting patents that
were %(et:directed to computer programs) but presented in the guise of
%(e:process claims), typically phrased as follows:

#Woa: process for [using general-purpose computing equipment], characterised
by ...

#cvm: The patents granted on this basis were considered as hypothetical,
because the program as such, when distributed on a disk or via the
Internet, did not constitute a process and was not an invention.  To
resolve this ambiguity, the European Patent Office took the final step
toward patentability of pure software in 1998 by %(et:authorising)
%(e:program claims), i.e. claims of the following form:

#asm: computer program, characterised by that [with its help a process
according to claim 1 can be executed].

#WWo: Prior to taking this bold step, in 1997, the EPO had secured
commitment for plans to rewrite the law from the following key players
of the European patent system, below referred to as the %(q:European
Patent Establishment):

#iWm: the patent office administrators of the member states, sitting on the
EPO's Administrative Council

#Wxa: the patent lawyers of large corporations, sitting on the %(q:Standing
Advisory Committee of the European Patent Office) SACEPO

#Wai: the patent administrators of the European Commission in the Industrial
Property Unit at the Directorate General for the Internal Market, at
the time under commissioner Mario Monti.

#twn: The EPO has meanwhile granted %(st:more than 30,000) pure software
patents in anticipation of the new legislation, and the number has
recently been rising at a rate of 3,000 per year.

#pWe: Most of these patents are broad and trivial and not significantly
different from corresponding types of patents that the US and Japan
have been allowing.  In fact the three patent offices have created a
common %(tw:Trilateral Standard) for granting such patents in May
2000, summarised under the newly created buzzword
%(ci:computer-implemented inventions).  Later, in an attempt to soothe
growing criticism in Europe, the patent establishment started to
emphasize differences in the treatment of %(q:computer-implemented
business methods).  However even these differences are
%(et:insignificant).

#iew: In August 2000 the European Patent Organisation, i.e. the
intergovernmental organisation that runs the European Patent Office, 
%(s:attempted to delete all the exclusions listed under Art 52 of the
European Patent Convention).  Due to public resistance which they
apparently did not anticipate, this effort failed.

#sWd: In 2002, the European Commission's Directorate for the Internal Market
(under Monti's successor Frits Bolkestein) submitted %(es:proposal
2002/0047) for a Directive %(q:on the patentability of
computer-implemented inventions). The Directive was claimed to serve
the purposes of harmonizing Member State laws and clarifying some
details with the aim of preventing excesses of the EPO.  However, upon
closer reading it becomes clear that the hat the Commission's proposal
was designed to %(s:codify unlimited patentability as practiced by the
EPO), with one exception: it did not allow program claims.

#abe: On September 24, 2003, the European Parliament as a whole (plenary
assembly) %(ep:voted) to %(ea:incorporate a set of amendments into the
Directive) that actually accomplished what the Commission had
pretended to aim for: it created clear and uniform rules, reaffirming
the non-patentability of programming and business logic and upholding
freedom of publication and interoperation.  This set of amendments was
based on a year of work in the Parliament's committees for
%(cu:Culture) and %(it:Industry).  However the directive was designed
to fall into the domain of the Legal Affairs Committee (JURI), which
is dominated by MEPs with close affinity to the patent establishment. 
JURI had ignored the proposals of the other committees and proposed a
set of %(ju:fake limits on patentability), in an attempt to fool the
plenary.  A groundswell of public opinion involving hundreds of
thousands of software professionals and scientists, largely
coordinated by the %(fi:FFII), helped to strengthen the Parliament's
resolve to vote for real limits on patentability.

#eor: Pursuant to the European Union's %(cd:Codecision Procedure), the
amended proposal was next examined by the Council of Ministers. 
Within the Council, the %(q:%(tp|Working Party on Intellectual
Property|Patents)) is considered to be in charge of the dossier.  This
group consists of exactly the same members as the Administrative
Council of the European Patent Office: the patent office
administrators from the national governments.

#uWn: After a few months of secret negotiations, the %(q:Working Party)
produced a %(cd:compromise document) that %(s:removed all of the
Parliament's limiting amendments, reinstated the Commission's
proposal, and, in addition, allowed program claims) (Art 5(2)),
disallowed any interoperability privilege within patent law (Rec 17)
and inserted some fake limits from JURI (Art 4A etc), resulting in the
most extreme, uncompromising and deceptive proposal of all seen so
far.  Access to this document was denied until the very last minute
%(q:due to the sensitive nature of the negotiations and the absence of
an overriding public interest.)

#XWe: On 18 May 2004, the Council of Ministers %(at:approved the Working
Party's text by a slim majority), despite the apparent intention of a
number of countries to follow Germany's lead in promising to vote
against it. In that session, Germany claimed to be satisfied by a
meaningless amendment; the Netherlands supported the document while
admitting that it might be problematic; and Commissioner Frits
Bolkestein inserted an amendment in Article 4 which, he claimed,
clearly made software unpatentable, when in fact his phrasing merely
reasserted deceptive terminology, while he failed to mention that
%(s:Article %(tp|5|2), by allowing program claims, unambiguously
states the opposite).  In the press conference that followed the
Council vote, Bolkestein failed to offer any examples of software that
would not be patentable under the proposal. The Council vote was also
notable for %(cv:the way in which the Irish Presidency pressed Denmark
for her votes), by which the slim majority was secured.

#dot: As with other intergovernmental agreements, Council decisions need to
be ratified in a second stage.  In the Council's language, the
%(q:political agreement) of the 18th of May had to be %(q:adopted) to
become a %(q:common position) of the Council.  It soon became dubious
whether this would happen.  Shortly after the session in the Council,
the Polish government %(pt:explained) that Poland did not support the
proposal, and the Dutch Parliament %(nm:voted) for a motion of
distrust against its government, which it said had %(mp:misinformed
the Parliament), and asked the government to withdraw its support for
the Council text.  In Germany likewise all parties in the Parliament
voted for a %(bt:motion) which criticises the Council text and demands
changes %(q:in the spirit of the Parliament's first reading).

#fjs: In november 2004 the Council's rules for qualified majorities changed,
and Poland reaffirmed its opposition to the Council's text, with the
effect that the text no longer enjoyed the support of a qualified
majority of member states.  However, the Dutch Council presidency
insisted that it should be adopted nonetheless, without a recount of
votes, arguing that in the Council's history political agreements were
always adopted, and failure to do so this time would create an
undesirable precedent.

#Wch: On December 21st December 2004 the Dutch presidency scheduled the
outdated political agreement for adoption without vote by the
Agriculture and Fisheries Council.  This was %(af:prevented) by a
surprise appearance of the Polish minister of science and
informatisation.  A similar attempt in early February 2005 under the
Luxemburg Presidency was also delayed at Poland's request.  However,
Poland's government failed to officially ask for a recounting of the
votes (reclassification as b-item) .  Instead they wrote a strong
%(us:unilateral statement) against the Council's text and supported a
request by the European Parliament for a restart of the procedure. 
Normally the European Commission is expected to comply with such
requests by presenting a new directive proposal to the Parliament. 
This time however, the Commission %(cr:refused) without giving an
explanation.

#Wnr: A few days later, at the %(cm:Council meeting of March 7th), the
Luxemburg Presidency declared the Political Agreement to have been
adopted, in spite of requests for renegotiations which the Presidency
said it had received from Denmark, Poland and Portugal.  Normally,
according to the Council's own rules of procedure, such requests lead
to renegotiation (reclassification of an a-item as b-item) .  It is
still %(kv:unclear) how the Council managed to adopt the agreement. 
The most likely explanation is that the Danish minister %(BB) did not
really request renegotiation but was only playing theater according to
script worked out the night before together with the Luxemburg
Presidency.  The theater performance was needed, because the minister
had %(dk:binding instructions from his Parliament) to ask for
renegotiation.  The Dutch minister, %(LB), was also obliged by his
Parliament to support renegotiation.  When Brinkhorst spoke at the
Council's %(q:public session), the microphones were switched off. 
Thus we do not know what he said.

#atd: In early April the European Parliament decided to conduct a second
reading on the basis of the Council's %(q:Uncommon Position), as it
was now colloquially called.  In a second reading, the Parliament can
vote for amendments similar to the ones for which it had previously
voted, but the majority requirements are higher than in the 1st
reading.  Any absent MEP will be counted as voting for the Council. 
There will without doubt be attempts by the patent establishment's
allies in the Parliament to propose another set of fake limits and to
pretend that this is based on meaningful negotiation and difficult
%(q:compromises) with the Council.  JURI will vote in June, the
plenary in July.  If the Parliament succedes in maintaining the spirit
of September 2003, it can look forward to negotiating with a disunited
Council from a position of moral strength.  However, the grip of the
patent establishment on the national ministries and the Commission is
still quite tight.  Unless it can be loosened within a few months,
nothing better than a heroic failure of the directive project will be
achievable for the time being.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/ShortIntro.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ShortIntro ;
# txtlang: xx ;
# End: ;

