<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Softwarové patenty v Evropě: Stručný přehled

#descr: Během 20 minut se dozvíte o co vlastně v boji proti softwarovým
patentům v Bruselu jde. Nejsložitější aspekty této debaty vyplývají z
několika jednoduchým parametrů. Až se s nimi seznámíte, budete se
doufejme cítit fundování ke psaní článků o fascinujícím  politickém
dramatu s dalekosáhlými důsledky.

#lae: Proč všechen ten rozruch kvůli softwarovým patentům?

#reP: Počítačové programy z perspektivy patentového systému

#itW: Nezdařený pokus změnit článek 52 EPC

#dWr: Nezdařený pokus ošálit parlament

#irm: Rada pod nejistou nadvládou patentového establishmentu

#cen: %(e:Patent) je právo monopolizovat si vynález. Potenciální vynálezce
specifikuje rozsah aktivit, ze kterých chce vyloučit ostatní
(patentový návrh),  a odešle jej patentovému úřadu. Ten ověří zda tyto
návrhy popisují %(e:inovaci)  ve smyslu zákona a zdali je vynález
dostatečně popsán a průmyslově využitelný (formální posouzení).
Některé patentové úřady mohou navíc zjišťovat zda je vynález nový a
není samozřejmý (věcné posouzení). Pokud přihláška projde posouzeními,
 udělí patentový úřad žadateli exkluzivní právo vyrábět a prodávat
vynález po dobu 20 let.

#Whh: Programování je podobné psaní symfonií. Když programátor píše software
spřádá tisíce myšlenek (algoritmů či výpočetních pravidel) do
výsledného díla, chráněného autorským právem. Některé myšlenky
programátora budou obvykle  nové a nikoliv samozřejmé vzhledem ke
(%(il:ve své podstatě nízkým)) standardům patentových systémů. V
okamžiku, kdy bude mnoho podobných myšlenek patentováno, začne být
%(ws:nemožné psát software) bez porušení patentů. Autorům softwaru
jsou tímto  efektivně odpírány jejich autorsky chráněná aktiva - žijí
pod neustálým ohrožením stát se cílem %(bp:vydírání vlastníky velkých
patentových portfolií). Výsledkem toho je vytvořeno méně softwaru a
objevuje se v něm méně nových myšlenek.

#xxW: Evropa již má sjednocená pravidla toho co je patentovatelné a co není.
Jsou popsána v %(ep:Úmluvě o udělování evropských patentů (EPC) z roku
1973). V článku 52 Úmluvy %(ae:uvádí), že matematické metody,
myšlenkové metody, obchodní postupy, počítačové programy, prezentace
informací atd. nejsou objevy  ve smyslu patentového zákona.  Existuje
pro to zcela    zásadní      důvod:  tradiční patenty pokrývají
aplikace přírodních věd (%(ti:technické vynálezy)), zatímco  patenty
na software pokrývají       abstraktní ideje.    Budeme-li aplikovat
patenty na software, pak výsledkem bude, že místo patentování
konkrétní pastičky na myši, patentujeme %(q:jakoukoliv past na savce) 
(nebo, jako aktuální příklad, jakékoliv %(ee:způsoby zachycení dat v
emulovaném prostředí)).

#Wpp: V roce 1986 začal Evropský patentový úřad (EPO) udělovat patenty,
které byly přímo  %(et:vztažené k počítačovým programům), ale
obsahovaly pod rouškou %(e:procesního patentového návrhu),  typické
fráze jako například:

#Woa: proces pro [použití obecně použitelného výpočetního zařízení],
charakterizovaný ...

#cvm: Patenty udělené na tomto základě byly považovány za hypotetické, neboť
program jako takový, distribuovaný na disketě či přes internet,
netvořil proces a nebyl vynálezem. Aby vyřešil tuto nejednoznačnost,
podnikl Evropský patentový úřad v roce 1998 poslední krok k
patentovatelnosti ryzího  softwaru %(et:autorizováním) %(e:patentových
 návrhů na programy), t.j. návrhů  v následující formě:

#asm: počítačový program, charakterizovaný tím, že [jej lze vykonat s pomocí
patentně nárokovaného procesu 1].

#WWo: Před tím, než podnikl tento nestoudný krok, roku 1997, si EPO zajistil
příslib plánů pro přepsání zákona od následujících klíčových hráčů
evropského patentového systému, dále uváděných jako %(q:evropský
patentový establishment):

#iWm: úředníci patentových úřadů členských států, zasedající v
administrativní radě EPO

#Wxa: patentoví právníci velkých korporací, zasedající ve  %(q:Stálé poradní
komisi Evropského patentového úřadu) SACEPO

#Wai: patentoví správci Evropské komise v Útvaru průmyslového vlastnictví a 
generálního komisariátu pro vnitřní trh, toho času vedeného komisařem
Mario Monti.

#twn: Mezitím EPO udělil %(st:více než 30 000) ryze softwarových patentů v
očekávání nové legislativy. A počet udělovaných patentů meziročně
rostl o  3 000 ročně.

#pWe: Většina těchto patentů je velice obecná a triviální, nikoliv nepodobná
typu patentů schvalovaných v USA a Japonsku. Ve skutečnosti tyto tři
patentové úřady vytvořily v květnu 2000 společný %(tw:Trilaterální
standard)  pro udělování podobných patentů, shrnující pod nově
vytvořeným módním výrazem %(ci:vynálezy realizované na počítači). 
Později při pokusu uklidnit rostoucí kritiku v Evropě, patentový
establishment začal ve zdůrazňování rozdílů vzhledem k zacházení s 
%(q:obchodními postupy implementovanými na počítači). Tyto rozdíly
jsou nicméně  %(et:nedůležité).

#iew: V srpnu 2000 se Evropská patentová organizace, mezivládní organizace 
která zajišťuje chod Evropského patentového úřadu,  %(s:pokusila
zrušit všechny výjimky vyjmenované v článku 52 Úmluvy o udělování
evropských patentů) Vzhledem k veřejnému odporu, který organizací
nebyl očekáván), tato snaha selhala.

#sWd: V roce 2002, Evropský vrchní komisariát pro vnitřní trh  (vedený
Montiho následovníkem  Fritsem Bolkesteinem) podal %(es:návrh 
2002/0047) na  směrnici o %(q:patentovatelnosti vynálezů
implementovaných na počítači). O této směrnici se uvádělo, že má
sloužit účelům harmonizace práva členských států a že má vyjasnit
určité detaily s cílem zabránit excesům ze strany EOP. Nicméně při
pečlivém prostudování je jasné, že návrh Evropské komise je má za cíl
kodifikovat patentovatelnost tak jak je praktikována EOP a to s
jedinou výjimkou: nepovoluje patentování programů.

#abe: Plenární shromáždění Evropského parlamentu %(ep:hlasovalo) pro 
%(ea:zakomponování řady dodatků do směrnice), které vlastně dosáhly
toho,  co si Komise předsevzala za cíl: vytvořilo jasná a jednotná
pravidla, potvrzující  nepatentovatelnost programové a obchodní logiky
a potvrdilo svobodu publikovaní a  interoperability. Tato řada
pozměňovacích návrhů byla založena na jednoroční  práci parlamentních
komisí pro %(cu:kulturu) a %(it:průmysl).   Nicméně směrnice byla
nasměrována do náruče Komise pro právní záležitosti (JURI), která je
obsazena členy parlamentu s blízkým vztahem k patentovému
establishmentu. JURI ignorovala návrhy ostatních komisí a navrhla řadu
%(ju:předstíraných omezení patentovatelnosti),  v pokusu ošálit
plenární zasedání.  Mocná vlna veřejného mínění zahrnující stovky
tisíc softwarových profesionálů a vědců, povětšinou koordinovaných
%(fi:FFII), pomohla  posílit parlamentní rozhodnutí hlasovat pro
skutečná omezení patentovatelnosti.

#eor: V souladu s %(cd:rozhodovacími procedurami) Evropské unie byl změněný
návrh prozkoumán Radou ministrů. V rámci Rady je za odpovědnou v této
otázce  považována Pracovní skupina pro intelektuální
vlastnictí/patenty. Tato skupina sestáva z identických členů jako
Správní rada Evropského patentového úřadu, tj. ze členů patentových 
úředníků národních vlád.

#uWn: Po několika měsících tajných dohod, vytvořila %(q:pracovní skupina)
%(cd:kompromisní dokument) který %(s:odstranil všechny parlamentní
omezující  pozměňovací návrhy, obnovil návrh Komise, dodatečně povolil
programové patentové nároky, článek 5 odstavec 2), a zamítl jakékoliv
privilegium  interoperability v rámci patentového zákona (článek 17
preambule) a vložil některé předstírané limity z JURI (článek 4a
atd.). Výsledkem čehož je ten nejkrajnější, nekompromisní a klamný
návrh, jaký byl doposud viděn. Přístup k tomuto dokumentu byl odpírán
až do poslední minuty  %(q:vzhledem k citlivé povaze dohod a absenci
převažujícího veřejného zájmu.)

#XWe: 18. května 2004 schválila Rada ministrů %(at:slabou většinou text 
pracovní skupiny), navzdory zjevnému záměru řady zemí následovat
pozici Německa slibujícího hlasovat proti. Během tohoto zasedání,
Německo prohlásilo, že je spokojené s bezvýznamným pozměňovacím
návrhem, Nizozemí podpořilo dokument, ačkoliv prohlásilo, že může být
problematický a komisař Frits Bolkestein vložil doložku článku 4,
která, jak prohlásil, jasně zabraňuje patentovatelnosti softwaru,
zatímco ve skutečnosti jeho formulace  pouze opakuje klamnou
terminologii, zatímco zapomněl zmínit, že  %(s:odstavec 2 článku 5
připuštěním programových patentových nároků jednoznačně uvádí opak).
Na tiskové konferenci následující hlasování Rady Bolkestein nedokázal
nabídnout příklad softwaru, který by v důsledku návrhu nebyl
patentovatelný. Hlasování Rady bylo také pozoruhodné vzhledem %(cv:ke
způsobu, jakým Irský předsedající tlačil Dánsko k jejich souhlasu),
čímž zajistil těsnou většinu.

#dot: Po rutinní textové úpravě a překladu předložila v srpnu 2004 Rada
formálně návrh . Po té se návrh vrátí do Evropského parlamentu k
dalšímu čtení. V této fázi může parlament návrh buď přímo zamítnout,
akceptovat jej tak jak je, nebo trvat na řadě pozměňovacích návrhů
podobných těm, pro které již dříve hlasoval.  Bez pochyb lze očekávat,
že se patentový establishment pokusí lobovat v JURI pro předložení
další řady předstíraných omezení a předstírat, že jsou založena na
smysluplných dohodách a %(q:kompromisech) v Radě.

#fjs

#Wch

#Wnr

#atd

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/ShortIntro.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ShortIntro ;
# txtlang: xx ;
# End: ;

