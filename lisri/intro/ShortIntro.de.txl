<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Softwarepatente in Europa: Ein kurzer Überblick

#descr: Innerhalb 20 Minuten können sie sich aneignen was auf beim Kampf um
Softwarepatent in Brüssel vor sich geht. Der Grossteil der Komplexität
in der Debatte rührt von ein paar einfachen Elementen her. Wenn sie
diese verstanden haben, dann werden sie sich hoffentlich zutrauen gut
informierte Artikel über ein faszinierendes Drama mit weitreichenden
Auswkirkungen zu schreiben.

#lae: Warum die Aufregung um Softwarepatente?

#reP: Computerprogramme aus der Sicht des Patentwesens

#itW: Gescheiterter Versuch, Art 52 EPÜ zu entsorgen

#dWr: Gescheiterter Versuch, das Parlament irrezuführen

#irm: Der Rat unter schwindender Kontrolle des Patentapparats

#cen: Ein %(e:Patent) ist ein Recht, eine Erfindung zu monopolisieren. Ein
angehender Erfinder gibt einen Bereich von Aktionen an, von denen er
andere ausschließen möchte %(pe:die Ansprüche), und reicht diesen beim
Patentamt ein, welches dann untersucht, ob diese Ansprüche eine
%(e:Erfindung) im Sinne des Gesetzes beschreiben und ob diese
Erfindung ordnungsgemäß offengelegt und industriell anwandbar ist
(formale Prüfung). Einige Patentämter untersuchen darüber hinaus, ob
die Erfindung neu und nicht offensichtlich ist %(pe:materielle
Prüfung). Wenn der Antrag die Hürden der Prüfung überstanden hat,
erteilt das Patentamt dem Antragssteller für eine Periode von 20
Jahren die exklusiven Produktions- und Vermarktungsrechte für diese
Erfindung.

#Whh: Programmieren lässt sich mit dem Schreiben einer Symphonie
vergleichen. Wenn ein Programmierer Software schreibt, verknüpft er
tausende von Ideen %(pe: Algorithmen oder Rechenregeln) zu einem
urheberrechtlich geschütztem Werk. Normalerweise werden einige dieser
Ideen in der Arbeit des Programmierers nach den %(pe:%(il:von Natur
aus niedrigen)) Standards des Patent-Systems neu und
nicht-offensichtich sein. Wenn viele solcher Ideen patentiert sind,
wird es %(ws:unmöglich, Software zu schreiben), ohne Patente zu
verletzen. Software-Autoren werden dadurch im Endeffekt ihres
urheberrechtlich geschützten, geistigen Eigentums beraubt; sie leben
unter der permanenten Bedrohung, von %(bp:Besitzern großer
Patent-Portfolios erpresst) zu werden. Infolge dessen wird weniger
Software geschrieben und weniger neue Ideen kommen auf.

#xxW: Europa hat bereits einheitliche Regeln darüber, was patentierbar ist
und was nicht. Diese sind festgeschrieben in dem %(ep:Europäisches
Patentübereinkommen von 1973). In Artikel 52 dieses Übereinkommens
wird %(ae:festgelegt), dass mathematische Methoden, Verfahren für
gedankliche Tätigkeiten, Geschäftsmethoden, Programme für
Datenverarbeitungsanlagen, die Wiedergabe von Informationen usw. keine
Erfindungen im Sinne des Patentrechtes sind. Dafür gibt es einen
systematischen Grund: In der Rechtsgeschichte galten Patente für
konkrete Anwendungen von Naturwissenschaften %(pe:%(ti:technische
Erfindungen)), während Patente auf Software abstrakte Ideen abdecken.
Wenn Patente auf Software angewandt werden, ist das Ergebnis, dass
man, statt eine spezifische Mausefalle zu patentieren, alle
%(q:Mittel, Säugetiere zu fangen) %(pe:oder, mit einem existierenden
Beispiel, alle %(ee:Mittel, Daten in einer emulierten Umgebung
einzufangen)) patentiert.

#Wpp: 1986 begann das Europäische Patentamt %(pe:EPA), Patente zu erteilen,
die sich auf %(et:Computerprogramme richteten), aber als
%(e:Prozessansprüche) getarnt wurden, typischerweise folgendermaßen
formuliert:

#Woa: Vorgang [der Datenverarbeitung mit Universalrechnern], dadurch
gekennzeichnet, dass ...

#cvm: Die Patente, die auf dieser Basis erteilt wurden, wurden als
hypothetisch erachtet, da das Programm als solches, wenn es auf einem
Datenträger oder über das Internet verteilt wurde, keinen Prozess
erzeugte und keine Erfindung war. Um diese Doppeldeutigkeit
aufzulösen, unternahm des Europäische Patentamt 1998 den letzten
Schritt in Richtung Patentierbarkeit reiner Software, indem es
%(e:Programmansprüche) %(et:erlaubte), d.h. Ansprüche mit folgendem
Aufbau:

#asm: Computerprogramm[produkt], dadurch gekennzeichnet, dass [mit seiner
Hilfe ein Vorgang gemäß Anspruch 1 ausgeführt werden kann]

#WWo: Bevor es 1997 diesen dreisten Schritt unternahm, hatte das EPA
Unterstützung durch die folgenden Schlüsselfiguren des Europäischen
Patent-Systems sichergestellt, im folgenden als %(q:Europäisches
Patent-Establishment) bezeichnet:

#iWm: die Patentamtsfunktionäre der Mitgliedsstaten, die im
EPO-Verwaltungsrat sitzen

#Wxa: die Patentanwälte von großen Firmen, die im %(q:Ständigen Beratenden 
Ausschuß beim EPA) SACEPO sitzen

#Wai: die Patentfunktionäre der Europäischen Kommission in der Abteilung
Gewerbliches Eigentum der Generaldirektion Binnenmarkt, zu diesem
Zeitpunkt unter Kommissar Mario Monti.

#twn: Das Europäische Patentamt hat in der Zwischenzeit %(st:mehr als
30.000) reine Software-Patente im Vorgriff auf die neue Gesetzgebung
erteilt, und diese Zahl ist in jüngster Zeit um 3.000 pro Jahr
gestiegen.

#pWe: Die meisten dieser Patente sind breit und trivial und unterscheiden
sich maßgeblich nicht von den entsprechenden Patent-Typen, die die USA
und Japan erlaubt haben. Tatsächlich haben die drei Patentämter im Mai
2002 einen gemeinsamen %(tw:Trilateralen Standard) für des Erteilen
solche Patente ins Leben gerufen, zusammengefasst unter dem neu
geschaffenen Schlagwort %(ci:computerimplementierte Erfindungen). In
einem Versuch, die in Europa aufkommende Kritik zu beruhigen, begann
das Patent-Establishment später, Unterschiede in der Behandlung von
%(q:computerimplementierten Geschäftsmethoden) zu betonen. Jedoch sind
auch diese Unterschiede %(et:unerheblich).

#iew: Im August 2004 startete die Europäische Patentorganisation, d.h. die
zwischenstaatliche Organisation, die das Europäische Patentamt
betreibt, einen %(s:Versuch, alle im Artikel 52 aufgeführten
Ausschlüsse aus dem Europäischen Patentübereinkommen zu streichen).
Wegen öffentlichem Widerstand, mit dem sie offensichtlich nicht
gerechnet hatten, scheiterte dieser Versuch.

#sWd: Im Jahr 2002 brachte die Generaldirektion Binnenmarkt der Europäischen
Kommission %(pe:unter Montis Nachfolger Frits Bolkestein) den
%(es:Vorschlag 2002/0047) für eine Richtlinie %(q:über die
Patentierbareit computerimplementierter Erfindungen) ein. Angeblich
sollte die Richtlinie dem Zweck dienen, die Gesetze der
Mitgliedsstaaten zu vereinheitlichen und einige Details klären, um
Auswüchse seitens des EPA zu verhindern. Liest man diesen jedoch
genauer, wird klar, dass der Vorschlag der Kommission entworfen wurde,
um %(s:die vom EPA praktizierte, unbegrenzte Patentierbarkeit,
festzuschreiben), mit nur einer Ausnahme: Es wurden keine
Programmansprüche erlaubt.

#abe: Am 24. September 2003 %(ep:beschloß) das Europäische Parlament als
Ganzes (Vollversammlung), eine %(ea: Anzahl an Änderungen in die
Richtlinie einfließen zu lassen), wodurch das erreicht wurde, was die
Kommission vorgeblich hatte erreichen wollen: Es wurden klare und
einheitliche Regelungen geschaffen, die Nicht-Patentierbarkeit von
Programm- und Geschäftslogik bekräftigt und die Freiheit der
Veröffentlichung und der Interoperation aufrechterhalten. Diese
Änderungen stützen sich auf einjährige Vorbereitungen in den
Parlaments-Ausschüssen für %(cu:Kultur) und %(it:Industrie).
Allerdings war die Richtlinie so angelegt, dass sie in den
Zuständigkeitsbereich des Rechtsausschusses %(pe:JURI) fiel, welcher
von MdEPs mit besonderer Nähe zum Patent-Establishment dominiert wird.
JURI ignorierte die Vorschläge der anderen Ausschüße und schlug, in
einem Versuch das Plenum auszutricksen, selbst eine Reihe von
%(ju:Scheinbegrenzungen der Patentierbarkeit) vor. Ein starker
öffentlicher Widerstand, getragen von hunderttausenden von
IT-Beruflern und Wissenschaftlern, zum größten Teil vom %(fi:FFII)
koordiniert, stärkte die Entschlossenheit des Parlaments, für eine
echte Beschränkung der Patentierbarkeit zu stimmen.

#eor: Gemäß dem %(cd:Mitentscheidungsverfahren) der Europäischen Union wurde
der veränderte Vorschlag vom Ministerrat geprüft. Innerhalb des Rates
gilt die %(q:%(tp|Working Party on Intellectual Property|Patents)) als
zuständig für dieses Dossier. Diese Gruppe besteht aus exakt den
selben Mitgliedern wie im Verwaltungsrat des Europäischen Patentamtes:
Den Patentamtsfunktionären der Regierungen der Mitgliedsstaaten.

#uWn: Nach ein paar Monaten geheimer Verhandlungen produzierte die
%(q:Arbeitsgruppe) ein %(cd:Kompromiss-Dokument), dass %(s:alle
begrenzenden Änderungen des Parlaments entfernte, den
Kommissions-Vorschlag wieder hervorholte, zusätzlich Programmansprüche
erlaubte) %(pe:Art 5%(pe:2)), jegliches Interoperabilitäts-Privileg im
Patentrecht zurückwies (Erwägungsgrund 17), einige Scheinbegrenzen von
JURI %(pe:Art 4A usw.) einfügte und damit zu dem bisher extremsten,
kompromisslosesten und irreführendsten Vorschlag führte. Der Zugang zu
diesem Dokument wurde bis zur allerletzen Minute %(q:wegen der
sensiblen Natur der Verhandlungen und dem Fehlen eines vorrangigen
öffentliches Interesses) verweigert.

#XWe: Am 18. Mai 2004 wurde vom Ministerrat %(at:der Text der Arbeitsgruppe
mit einer knappen Mehrheit bestätigt), trotz der offensichtlichen
Absicht einer Anzahl von Ländern, dem Versprechen Deutschlands,
dagegen zu stimmen zu folgen. In dieser Sitzung behauptete
Deutschland, durch eine bedeutungslose Änderung zufriedengestelt
worden zu sein; die Niederlande unterstützten das Dokument, obwohl sie
einräumten, dass es problematisch sein könne; und Kommissar Frits
Bolkestein fügte eine Formulierung in Artikel 4 ein, die seiner
Behauptung nach Software eindeutig nicht patentierbar mache, in
Wirklichkeit aber lediglich irreführende Terminologie wiederholt,
während er es unterlässt, darauf hinzuweisen, dass %(s:Artikel
%(tp|5|2), indem dieser Programmansprüche erlaubt, eindeutig das
Gegenteil feststellt). In der Pressekonferenz nach der Abstimmung war
Bolkestein nicht in der Lage, irgendein Beispiel von Software zu
nennen, die nach diesem Vorschlag nicht patentierbar wäre. Die
Rats-Abstimmung war außerdem bemerkenswert für %(cv:die Weise, in der
die Irische Ratspräsidentschaft Dänemark zu seiner Stimme drängte),
durch die diese knappe Mehrheit gesichert wurde.

#dot: After some routine editing and translating, the Council is expected to
formally endorse this proposal in June 2004. It will then return to
the European Parliament for another reading. At this stage, the
Parliament may either reject it outright, accept it as it is, or
insist on a set amendments similar to the ones for which it had
previously voted.  There will without doubt be attempts by the patent
establishment's relays in JURI to propose another set of fake limits
and to pretend that this is based on meaningful negotiation and
difficult %(q:compromises) with the Council.

#fjs

#Wch

#Wnr

#atd

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/ShortIntro.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ShortIntro ;
# txtlang: xx ;
# End: ;

