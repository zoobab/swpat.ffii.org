<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patentes de software en Europa: breve introducción a la situación

#descr: En 20 minutos puedes aprender qué está ocurriendo actualmente sobre
las patentes de software en Bruselas.  Muchas de las complegidades del
debate surgen de unos pocos parámetros simples. Cuando has los has
leido, puedes esperar sentirte informado para escribir artéculos bien
infromados sobre un fascinante drama polético fascinante con
implicaciones de gran envergadura.

#lae: ¿Por qué tanta furia sobre las patentes de software?

#reP: Los Programas de Ordenador desde la Perspectiva del Sistema de
Patentes

#itW: Intento Abortade de Enmendar el Art 52 EPC

#dWr: Intento Fallido de Ningunear el Parlamento

#irm: El Consejo bajo el control fallido del Stablishment del Parlamento

#cen: Una %(e:patente) es un derecho que otorga un monopolio sobre una
invencién. Para obtenerlo, el inventor potencial debe describir el
alcance de las actividades de las que desea excluir a sus competidores
(reivindicacién), y presentar su solicitud ante la Oficina de
patentes. Ésta determina si su solicitud describe una %(e:invencién)
en el sentido de la ley, si la invencién está correctamente revelada y
es aplicable en términos industriales (examen de forma).  Algunas
oficinas de patentes examinarán además si la invencién es novedosa y
no evidente (examen de fondo). Si la solicitud supera estos exámenes,
la Oficina de patentes concede al candidato derechos exclusivos de
fabricacién y comercializacién de su invencién durante un periodo de
20 años.

#Whh: Programming is similar to writing symphonies.  When a programmer
writes software, he weaves together thousands of ideas (algorithms or
calculation rules) into a copyrighted work. Usually some of the ideas
in the programmer's work will be new and non-obvious according to the
(%(il:inherently low)) standards of the patent system.  When many such
ideas are patented, it becomes %(ws:impossible to write software)
without infringing on patents.  Software authors are in effect
deprived of their copyright assets; they live under permanent threat
of being %(bp:blackmailed by holders of large patent portfolios).  As
a result, less software is written and fewer new ideas appear.

#xxW: Europa ya dispone de una normativa unificada sobre lo que es posible
patentar, y lo que no:  %(ep:el Convenio Europeo de Patentes de 1973).
En su Artéculo 52, el Convenio %(ae:establece) que los procedimientos
matemáticos, intelectuales o comerciales, los programas de ordenador,
las presentaciones de informacién, etc. no se consideran invenciones
en el sentido de la Ley. Existe una razén légica para ello: dentro de
la tradicién legal, las patentes se conceden a aplicaciones concretas
del ámbito de las ciencias naturales (%(ti:invenciones técnicas),
mientras una patente de software abarcaréa ideas de naturaleza
abstracta. En efecto, cuando se recurre a una patente para proteger un
programa de ordenador, en lugar de hacerlo de un determinado modelo de
ratonera, se obtiene en realidad una patente que abarca cualquier
"%(q:procedimiento para atrapar maméferos)" (o, para ceñirnos a un
ejemplo real, cualquier "%(ee:procedimiento para capturar informacién
dentro de un entorno simulado").

#Wpp: En 1986, la  Oficina Europea de Patentes (OEP) comenzé a conceder
patentes %(et:dirigidas a programas de ordenador), formuladas como
%(e:reivindicaciones), tépicamente expresado de la siguiente forma:

#Woa: procedimiento [que opera en un equipo informático de uso general],
caracterizado por...

#cvm: Las patentes concedidas de acuerdo con esto se consideraban
hipotéticas, porque el programa como tal, cuando se distribuye en
soporte magnético o por Internet no constituyen un procedimiento y no
era asimilable a una invencién.  Para resolver esta ambigüedad, la
Oficina Europea de Patentes dio un éltimo paso hacia la patentabilidad
del software puro en 1998, al %(et:permitir) %(e:reivindicaciones de
programas), es decir demandas con la siguiente formulación:

#asm: programa de ordenador, caracterizado por [su contribucién a la
ejecucién del procedimiento reivindicado en 1].

#WWo: Antes de dar este paso decisivo, en 1997, la OEP se aseguré de que su
intento de cambiar la redaccién de la ley contaba con el apoyo de los
principales actores del sistema europea de patentes, que agrupamos en
adelante bajo el nombre de %(q:Establishment pro-patentes):

#iWm: responsables de las oficinas de patentes de los Estados miembro, con
asiento en el Consejo de administracién de la OEP

#Wxa: abogados especializados en patentes de las grandes empresas, con
asiento en el %(q:Comité consultivo de la Oficina de patentes Europea)
 SACEPO

#Wai: responsables de patentes de la Comisién Europea, del Departamento de
Propiedad Intelectual, en la Direccién General del Mercado Interior,
entonces dirigida por el comisario Mario Monti.

#twn: Entretanto, la OEP ya habéa concedido %(st:más de 30&nbsp;000)
patentes de software puro, anticipándose a la nueva legislacién, y el
ritmo de concesiones alcanzé recientemente las 3000 por año.

#pWe: La mayoréa de las patentes son triviales y no presentan diferencias
significativas con los tipos de patentes que EEUU y Japén han ido
concediendo.  De hecho, estas tres oficinas de patentes han creado en
mayo de 2000 una %(tw:Normativa Trilateral) para conceder patentes de
este tipo, denominadas %(ci:invenciones implementadas en ordenador). 
Más adelante, en un intento de apaciguar las crecientes protestas
europeas, el lobby pro-patentes comenzé a subrayar las diferencias
existentes con los %(q:procedimientos de negocios implementados en
ordenador).  Pero incluso tales diferencias son %(et:insustanciales e
insignificantes).

#iew: En agosto de 2000, la Organizacién Europea de Patentes, el organismo
intergubernamental que dirige la Oficina Europea crecientes,
%(s:intenté eliminar todas las exclusiones enumeradas bajo el Art. 52
del Convenio Europeo de Patentes).  Pero este esfuerzo fracasé, debido
a una resistencia péblica contra la que, aparentemente, no estaban
preparados.

#sWd: En 2002, la Direccién General del Mercado Interior (bajo la direccién
Frits Bolkestein, que sucedié a Monti) presenté la %(es:propuesta
2002/0047), para una Directiva %(q:sobre patentabilidad de invenciones
implementadas por ordenador). Los objetivos de esta Directiva eran la
armonizacién de las leyes de los Estados miembro y la clarificacién de
algunos detalles para impedir los excesos de la OEP.  Sin embargo, una
lectura atenta mostraba que la propuesta de la Comisién esta diseñada
para %(s:codificar la patentabilidad ilimitada puesta en práctica por
la OEP), con una excepcién: no autorizaba las reivindicaciones de
programas.

#abe: El 24 de septiembre de 2003, el Parlamento Europeo %(ep:voté) en
asamblea plenaria la %(ea:incorporacién  de una serie de enmiendas a
la Directiva) que, en efecto, hacéan realidad los objetivos anunciados
de la Comisién: clarificaba y armonizaba las normas, excluéa la
patentabilidad de los programas de ordenador y procedimientos
comerciales, y reforzaba la libertad de publicacién e
interoperabilidad.  Este conjunto de enmiendas se basaba en un año de
trabajo de los comités parlamentarios para la %(cu:Cultura) e
%(it:Industria).  Sin embargo, tal directiva se consideraba
competencia del Comité de Asuntos Legales (JURI), que se encuentra
dominado por eurodiputados cercanos al establishment pro-patentes. 
JURI ignoré las propuestas de los demás comités e introdujo
%(ju:falsas limitaciones a la patentabilidad), en un intento de
engañar a la asamblea plenaria. Un movimiento de la opinién péblica
que implica centenares de millares de profesionales y de cientéficos
del software, coordinados en gran parte por el %(fi:FFII), ha ayudado
a consolidar la resolucién del Parlamento para votar por lémites
verdaderos a la patentabilidad.

#eor: De acuerdo con el %(cd:Procedimiento de codecisién) de la Unién
Europea, la propuesta y sus enmiendas fueron examinadas a continuacién
por el Consejo de ministros.  En el Consejo, el %(q:%(tp|Grupo de
trabajo sobre propiedad intelectual|patentes)) es responsable de este
dossier.  Pero este grupo cuenta exactamente con los mismos miembros
que el Consejo de administracién de la Oficina Europea de patentes:
los administradores de las oficinas de patentes de los gobiernos
nacionales respectivos.

#uWn: Tras unos meses de negociaciones secretas, el %(q:Grupo de trabajo)
entregé un %(cd:documento de compromiso) que %(s:eliminaba todas las
enmiendas del Parlamento para limitar la patentabilidad reinstaurando
la propuesta de la Comisién. El documento restauraba la propuesta de
la Comisién y autorizaba además la reivindicacién de programas de
ordenador(Art 5(2)), descartaba cualquier excepcién en favor de la
interoperabilidad dentro de la ley de patentes (Rec 17) y restablecéa
las falsas limitaciones de JURI (Art 4A etc.). El resultado fue una
propuesta aén más radical, que no dejaba espacio para el compromiso. 
Se denegé el acceso al documento hasta el éltimo minuto, %(q:debido a
la naturaleza sensible de las negociaciones y la falta de intereses
péblicos superiores).

#XWe: El 18 de mayo de 2004, el Consejo %(at:aprobé el texto del Grupo de
trabajo (con unos pocos cambios superficiales) por una escasa
mayoréa). Para ello, conté con los votos de un cierto némero de paéses
que, con Alemania a la cabeza, incumplieron su promesa oponerse. En
aquella sesién, Alemania se satisfizo de una enmienda secundaria; los
Paéses Bajos aceptaron el documento, a pesar de advertir sus
problemas. El comisario Frits Bolkestein incluyé una enmienda al
Artéculo 4 que, segén declaré, excluéa claramente la patentabilidad
del software. En la práctica, esta nueva formulacién tan sélo
reproduce la confusién intencionada de redaccién, ya que no se altera
el %(s:Artéculo %(tp|5|2) que autoriza la reivindicaciones de
programas, y por tanto afirma sin ambigüedades lo contrario).  En la
conferencia de prensa que siguié al voto del Consejo, Bolkestein fue
incapaz de ofrecer ningén ejemplo de software no patentable, de
acuerdo con la propuesta. El voto del Consejo también fue notable por
%(cv:la forma en que la presidencia Irlandesa apuré a Dinamarca para
que votara a favor), con el fin de alcanzar la corta mayoréa.

#dot: Tras las correcciones y traducciones de rutina, se espera que el
Consejo apruebe formalmente esta propuesta en junio de 2004. A
continuacién, volverá al Parlamento Europeo para otra lectura. En esa
etapa, el Parlamente puede o bien rechazarlo directamente, aceptarlo
tal cual, o insistir en la necesidad de enmiendas similares a las que
voté previamente.  No hay duda de que el establishment pro-patentes
intentará delegar en JURI cualquier propuesta de limitaciones, de
nuevo insustanciales, que serán presentadas como fruto de sélidas
negociaciones y diféciles %(q:compromisos) con el Consejo.

#fjs

#Wch

#Wnr

#atd: Tras las correcciones y traducciones de rutina, se espera que el
Consejo apruebe formalmente esta propuesta en junio de 2004. A
continuacién, volverá al Parlamento Europeo para otra lectura. En esa
etapa, el Parlamente puede o bien rechazarlo directamente, aceptarlo
tal cual, o insistir en la necesidad de enmiendas similares a las que
voté previamente.  No hay duda de que el establishment pro-patentes
intentará delegar en JURI cualquier propuesta de limitaciones, de
nuevo insustanciales, que serán presentadas como fruto de sélidas
negociaciones y diféciles %(q:compromisos) con el Consejo.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/ShortIntro.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ShortIntro ;
# txtlang: xx ;
# End: ;

