<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Les brevets logiciels en Europe : une courte introduction

#descr: Apprenez en 20 minutes les tenants et aboutissants de la bataille en
cours à Bruxelles sur les brevets logiciels. L'essentiel du débat
réside dans quelques points très simples. Lorsque vous en aurez pris
connaissance, vous serez à même d'écrire des articles bien informés
sur un drame politique aux vastes conséquences.

#lae: Pourquoi tant de tapage autour des brevets logiciels ?

#reP: Les programmes d'ordinateurs du point de vue du Système de brevets

#itW: Tentative avortée d'amender l'article 52 de la CBE

#dWr: Tentative vaine de tromper le Parlement

#irm: Le Conseil sous le contrôle chancelant de l'establishment des brevets

#cen: Un %(e:brevet) est un droit pour avoir un monopole sur une invention.
Un inventeur potentiel indique la portée des activités desquelles il
veut exclure ses confrères (les revendications) et la soumet à
l'Office des brevets, qui évalue si ces revendications dépeignent une
%(e:invention) au sens de la loi et si l'invention est correctement
révélée et applicable industriellement (examen formel). Certains
offices des brevets examineront par ailleurs si l'invention est
nouvelle et non-évidente (examen substantiel). Si l'application passe
avec succès ces examens, l'Office des brevets accorde au demandeur le
droit exclusif de produire et commercialiser son invention pendant une
période de 20 ans.

#Whh: La programmation est comparable à l'écriture de symphonies. Quand un
programmeur écrit un logiciel, il combine des milliers d'idées (des
algorithmes ou des règles de calcul) qui constituent une oeuvre sous
droits d'auteur. Généralement, certaines des idées dans l'oeuvre du
programmeur seront nouvelles et non-évidentes selon les normes
(%(il:intrinsèquement de bas niveau)) du système de brevets. Lorsqu'un
bon nombre de ces nouvelles idées est breveté, il devient
%(ws:impossible d'écrire un logiciel) sans enfreindre des brevets. Les
auteurs de logiciel sont de ce fait privés des avantages conférés par
leurs droits d'auteur ; ils vivent sous la menace permanente %(bp:d'un
chantage de la part des détenteurs de larges portefeuilles de
brevets). En conséquence, moins de logiciels sont écrits et peu de
nouvelles idées apparaissent.

#xxW: En Europe, ce qui est brevetable et ce qui ne l'est pas est d'ores et
déjà défini par %(ep:la Convention sur le brevet européen de 1973). 
Dans son article 52, la Convention %(ae:stipule) que les méthodes
mathématiques, les méthodes dans l'exercice d'activités
intellectuelles ou dans le domaine des activités économiques, les
programmes d'ordinateurs, les présentations d'informations, etc. ne
sont pas des inventions au sens du droit des brevets. Il y a une
raison logique à cela : dans la tradition du droit, les brevets ont
toujours porté sur des applications concrètes des sciences naturelles
(%(ti:inventions techniques)), tandis que les brevets logiciels
couvrent des idées abstraites. Lorsque les brevets s'appliquent au
logiciel, le résultat est tel qu'au lieu de breveter un piège à rats
spécifique, vous brevetez tous les %(q:moyens d'attraper des
mammifères) (ou, pour un exemple concret, tout %(ee:moyen d'attraper
des données dans un environnement d'émulation)).

#Wpp: En 1986, l'Office européen des brevets (OEB) a commencé à accorder des
brevets %(et:portant sur des programmes d'ordinateur), mais présentés
comme des %(e:revendications de procédés), typiquement formulés de la
manière suivante :

#Woa: procédé [utilisant un équipement informatique générique], caractérisé
par...

#cvm: Les brevets accordés ainsi étaient considérés comme hypothétiques, car
les programmes en tant que tels, lorsqu'ils sont distribués sur un
support magnétique ou via Internet, ne forment pas de procédé et ne
sont donc pas considérés comme des inventions. Pour résoudre cette
ambiguïté, l'Office européen des brevets a fait le dernier pas vers la
brevetabilité du logiciel pur en 1998 en %(et:accordant) des
%(e:revendications de programmes), i.e. des revendications formulées
comme ceci :

#asm: un programme d'ordinateur, caractérisé par [le fait qu'au moyen de ce
programme, le procédé selon la revendication 1 puisse être exécuté].

#WWo: Avant de franchir cette étape fondamentale, l'OEB s'était assuré en
1997 que ses plans visant à récrire le droit recevaient l'approbation
des acteurs clés du système européen des brevets, que nous nommerons
dans cet article %(q:l'establishment européen des brevets) :

#iWm: les administrateurs des offices des brevets des États membres,
siégeant au Conseil d'administration de l'OEB ;

#Wxa: les avocats en brevets de grandes entreprises, siégeant au %(q:Comité
consultatif permanent de l'Office européen des brevets) (SACEPO en
anglais) ;

#Wai: les administrateurs des brevets de la Commission européenne, au sein
de l'Unité de la Propriété industrielle à la Direction générale pour
le Marché intérieur, à l'époque dirigée par le Commissaire Mario
Monti.

#twn: Pendant ce temps, l'OEB a accordé %(st:plus de 30 000) brevets
logiciels purs, en anticipation d'une nouvelle législation ; et ce
nombre s'est récemment accru à un taux de 3 000 par an.

#pWe: La plupart de ces brevets sont étendus, triviaux et peu différents du
genre de brevets autorisés par les Offices des USA ou du Japon. En
fait, ces trois offices de brevets ont fondé en mai 2000 un
%(tw:Standard trilatéral) commun pour accorder de tels brevets,
désignés par le nouveau terme %(ci:inventions mises en oeuvres par
ordinateurs). Ensuite, afin d'adoucir les critiques s'intensifiant en
Europe, le lobby des brevets a commencé à mettre l'accent sur les
différences dans le traitement des %(q:méthodes mises en oeuvre par
ordinateur). Cependant, même ces différences sont
%(et:insignifiantes).

#iew: En août 2000, l'Organisation européenne des brevets, i.e.
l'organisation intergouvernementale qui dirige l'Office européen des
brevets, %(s:a essayé de supprimer toutes les exceptions énumérées à
l'article 52 de la Convention sur le brevet européen). Mais cette
tentative a échoué, suite à une résistance publique qui n'était
apparemment pas prévue.

#sWd: En 2002, la Direction du Marché intérieur de la Commission européenne
(dirigée par le successeur de Monti, Frits Bolkestein) a soumis
%(es:la proposition de directive 2002/0047) sur %(q:la brevetabilité
des inventions mises en oeuvre par ordinateur). Les objectifs annoncés
de cette directive étaient l'harmonisation des lois des États membres
et la clarification de quelques détails dans le but d'empêcher les
excès de l'OEB. Cependant, en y regardant de plus près, il est devenu
clair que la proposition de la Commission était conçue pour
%(s:codifier la brevetabilité illimitée pratiquée par l'OEB), à une
exception près : elle n'autorisait pas les revendications de
programmes.

#abe: Le 24 septembre 2003, le Parlement européen tout entier (en séance
plénière) %(e:a voté) pour %(ea:incorporer un ensemble d'amendements à
la directive) permettant d'accomplir les objectifs que l'on avait
proclamé vouloir atteindre : clarifier et réaffirmer la non
brevetabilité des programmes d'ordinateur et des méthodes affaires et
confirmer la liberté de publication et d'interopérabilité. Cet
ensemble d'amendements s'appuyait sur toute une année de travail aux
commissions parlementaires à %(cu:la Culture) et à %(it:l'Industrie).
Cependant, la directive relevait de la compétence de la commission à
la Justice (JURI), contrôlée par des eurodéputés ayant d'étroites
affinités avec l'establishment des brevets. La commission JURI a
ignoré les propositions des autres commissions et a proposé un
ensemble de %(ju:fausses limites à la brevetabilité), dans une
tentative visant à tromper l'assemblée plénière. Un raz-de-marée de
l'opinion publique, im  pliquant des centaines de milliers de
professionnels et de scientifiques du secteur informatique, en grande
partie coordonné par %(fi:la FFII), a contribué à renforcer la
résolution du Parlement à voter pour de réelles limites à la
brevetabilité.

#eor: Selon %(cd:la procédure de Codécision) en vigueur dans l'Union
européenne, la proposition amendée fut ensuite examinée par le Conseil
des ministres. Au sein du Conseil, le %(q:%(tp|Groupe de travail sur
la Propriété intellectuelle|Brevets)) en a la responsabilité. Ce
groupe se compose exactement des mêmes membres que le Conseil
d'administration de l'Office européen des brevets : des
administrateurs de brevets des gouvernements nationaux.

#uWn: Après plusieurs mois de négociations secrètes, le %(q:Groupe de
travail) a produit %(cd:un document de compromis) qui %(s:éliminait
les amendements du Parlement qui instauraient des limites à la
brevetabilité, réintroduisait la proposition initiale de la Commission
mais en y ajoutant l'acceptation de revendications de programmes)
(article 5(2)), interdisait toute exception d'interopérabilité dans le
droits des brevets (considérant 17) et réinsérait les fausses limites
de la commission JURI (article 4bis). Ce qui a donné la proposition la
plus extrémiste jusqu'alors, laissant le moins de chances de
compromis. L'accès à ce document a été refusé jusqu'à la toute
dernière minute %(q:en raison de la nature sensible des négociations
et de l'absence d'intérêt public supérieur.)

#XWe: Le 18 mai 2004, le Conseil %(qt:a approuvé le texte du Groupe de
travail par une courte majorité), en dépit des déclarations
d'intentions d'un certain nombre de pays, prêts à suivre l'Allemagne
en promettant de voter contre le texte. Dans cette session,
l'Allemagne a prétendu se satisfaire d'un amendement dénué de sens ;
les Pays-Bas ont soutenu le document tout en admettant qu'il pourrait
être problématique ; et le Commissaire Frits Bolkestein a inséré un
amendement à l'article 4 qui, a-t-il déclaré, rendait clairement le
logiciel non brevetable. Mais la formulation de cet amendement ne
faisait en fait que réaffirmer une terminologie trompeuse, alors que
Bolkestein a oublié de mentionner que %(s:l'article %(tp|5|2), en
autorisant les revendications de programmes, affirmait exactement
l'inverse et sans ambiguïté possible). À la conférence de presse qui a
suivi le vote du Conseil, Bolkestein n'a pas réussi à donner un seul
exemple de logiciel qui n  e serait pas brevetable si l'on se
conformait la proposition. Le vote du Conseil était également frappant
par %(cv:la manière avec laquelle la présidence irlandaise a ardemment
poussé le Danemark à donner ses voix), grâce auxquelles la courte
majorité a été atteinte.

#dot: Comme tout autre accord intergouvernemental, les décisions de Conseil
doivent être ratifiées lors d'une seconde étape. Dans le vocabulaire
du Conseil, %(q:l'accord politique) du 18 mai 2004 devait être
%(q:adopté) pour devenir une %(q:position commune) du Conseil. La date
pour que ceci soit accompli est vite devenue incertaine. Peu après la
session du Conseil, le gouvernement polonais a %(pt:expliqué) que la
Pologne ne soutenait pas la proposition. Le parlement néerlandais
%(nm:a voté) une motion discréditant son gouvernement, qui affirmait
que ce dernier avait %(mp:désinformé le parlement) et demandait au
gouvernement de retirer son soutien au texte du Conseil. De même, en
Allemagne, tous les partis du parlement on voté une %(bt:motion)
critiquant le texte du Conseil et demandant des modifications %(q:dans
l'esprit de la première lecture du Parlement européen).

#fjs: En novembre 2004, le règlement du Conseil a changé avec l'entrée en
vigueur du Traité de Nice et les pondérations pour la majorité
qualifiées ont évolué. La Pologne a réaffirmé son opposition au texte
du Conseil, ce qui entraînait que le texte ne bénéficiait plus de
majorité qualifiée des États membres. Pourtant, la présidence
néerlandaise du Conseil a insisté pour qu'il soit adopté malgré tout,
sans recompte des voix, arguant qu'historiquement, les accords
politiques étaient toujours adoptés et que si ce n'était pas le cas
cette fois-ci, cela créerait un précédent indésirable.

#Wch: Le 21 décembre 2004, la présidence néerlandaise a mis à l'ordre du
jour l'accord politique périmé afin qu'il soit adopté sans vote lors
d'un Conseil sur l'Agriculture et la Pêche. L'apparition surprise du
ministre polonais de la science et de l'informatisation a
%(af:empêché) cette adoption. Une tentative similaire au début du mois
de février 2005 par la présidence luxembourgeoise a également été
retardée à la demande de la Pologne. Cependant, le gouvernement
polonais n'a pas réussi a demandé officiellement un recompte des votes
(reclassement en %(q:point B).  Au lieu de ça, les Polonais ont voté
une %(us:déclaration unilatérale) ferme contre le texte du Conseil et
supporté une demande du Parlement européen de redémarrer la procédure.
Normalement, la Commission européenne est supposée se conformer à de
telles requêtes en présentant au Parlement une nouvelle proposition de
directive. Cependant, la Commission a cette fois-ci %(cr:refusé) sans
donner d'explication.

#Wnr: Quelques jours plus tard, à la %(cm:réunion du Conseil du 7 mars
2005), la présidence luxembourgeoise a déclaré que l'accord politique
était adopté, en dépit de demandes de renégociations que la Présidence
a avoué avoir reçues du Danemark, de la Pologne et du Portugal.
Normalement, selon le propre règlement intérieur du Conseil, de telles
requêtes conduisent à des discussions (reclassement d'un %(q:point A)
en %(q:point B). La manière dont le Conseil s'est arrangé pour adopter
l'accord reste encore à éclaircir. L'explication la plus probable est
que le ministre danois, %(BB), n'a pas vraiment demander de
renégociation mais a jouer un rôle selon le script élaboré la veille
avec la Présidence luxembourgeoise. Cette comédie était nécessaire car
le ministre danois était %(dk:contraint par des instructions de son
parlement) de demander une renégociation. Le ministre néerlandais,
%(LB), était également forcé par son parlement de soutenir toute
demande de renégociation. Lorsque Brinkhorst est intervenu lors de la
%(q:session publique) du Conseil, on a éteint les micros. Nous ne
savons donc pas ce qu'il a dit.

#atd: Début avril 2005, le Parlement européen a décidé de mener une seconde
lecture sur la base de la %(q:position peu commune) du Conseil,
puisque c'est ainsi que désormais on la désigne familièrement. Lors
d'une seconde lecture, le Parlement peut voter pour des amendements
similaires çàceux qu'il avait précédemment passés, mais les exigences
de majorité sont plus élevées qu'en 1re lecture. Tout eurodéputé
absent sera compté comme acceptant le texte du Conseil. Il y aura sans
aucun doute des tentatives de la part des relais de l'establishment
des brevets au Parlement pour proposer un autre ensemble de fausses
limites et prétendre que ceci se base sur des négociations
significatives et un difficile %(q:compromis) avec le Conseil. La
commission parlementaire des affaires juridiques (JURI) votera en
juin, la session plénière aura lieu en juillet. Si le parlement
réussit à conserver l'esprit de septembre 2003, il pourra envisager de
négocier avec un Conseil désuni en bénéficiant d'une certaine force
morale. Toutefois, la mainmise de l'establishment des brevets sur les
ministres nationaux et la Commission est toujours assez ferme. À moins
de l'affaiblir dans les prochains mois, une mise en échec héroïque du
projet de directive pourrait bien être ce que l'on peut réaliser de
mieux pour l'instant.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpat/ShortIntro.el ;
# mailto: gibus@ffii.fr ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ShortIntro ;
# txtlang: xx ;
# End: ;

