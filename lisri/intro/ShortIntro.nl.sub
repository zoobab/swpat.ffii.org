\begin{subdocument}{ShortIntro}{Softwarepatenten in Europa: Een kort overzicht}{http://swpat.ffii.org/lisri/intro/index.nl.html}{Antonios Christofides\\\url{http://epatents.hellug.gr}\\\url{}\\Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\Nederlandse versie 2004/07/20 door Jonas MAEBE\footnote{\url{http://lists.ffii.org/mailman/listinfo/traduk-parl}}}{In 20 minuten kan je leren wat er gaande is in de strijd over softwarepatenten in Brussel. De meeste ingewikkelde kanten van het debat komen voort uit een aantal eenvoudige randvoorwaarden. Wanneer je dit inziet, kan je je hopelijk sterk genoeg voelen om goed ge\"{\i}nformeerde artikels te schrijven over een fascinerend politiek drama met verstrekkende gevolgen.}
\begin{sect}{swpat}{Waarom al deze heibel over softwarepatenten?}
Een \emph{octrooi} is het recht om een uitvinding te monopoliseren. Een would-be uitvinder specificeert een waaier van activiteiten waarop hij het alleenrecht wilt (de conclusies), en dient deze in bij een octrooibureau. Daar wordt bekeken of deze conclusies een \emph{uitvinding} belichamen in de zin van de wet, en of deze uitvinding correct beschreven wordt en industrieel toepasbaar is (het formele onderzoek). Sommige octrooibureaus zullen bovendien onderzoeken of de uitvinding nieuw en niet voor de hand liggend is (het onderzoek ten gronde). Indien de aanvraag het onderzoek doorstaat, zal het octrooibureau de aanvrager exclusieve de rechten toekennen om de uitvinding te produceren en te commercialiseren voor een periode van 20 jaar.

Programmeren is gelijkaardig aan het schrijven van symfonie\"{e}n. Wanneer een programmeur software schrijft, verweeft hij duizenden idee\"{e}n (algoritmen of rekenregels) tot een auteursrechtelijk beschermd werk. Meestal zullen een aantal van de idee\"{e}n in het werk van de programmeur nieuw en niet voor de hand liggend zijn volgens de (inherent lage\footnote{\url{http://swpat.ffii.org/stidi/frili/index.en.html}}) standaarden van het octrooisysteem. Indien veel van dergelijke idee\"{e}n geoctrooieerd worden, wordt het onmogelijk om software te schrijven\footnote{\url{http://webshop.ffii.org/}} zonder inbreuk te maken op octrooien. De software-auteurs worden in feite beroofd van hun auteursrecht; ze werken onder de voortdurende dreiging van gechanteerd te worden door houders van grote octrooiportfolio's\footnote{\url{http://www.forbes.com/asap/2002/0624/044.html}}. Het resultaat is dat minder software geschreven wordt en minder nieuwe idee\"{e}n ontstaan.
\end{sect}

\begin{sect}{legal}{Computerprogramma's vanuit het perspectief van het octrooisysteem}
Europe already has uniform rules about what is patentable and what not.  They are laid down in the European Patent Convention of 1973\footnote{\url{http://www.european-patent-office.org/legal/epc/}}. In Article 52, the Convention states\footnote{\url{http://swpat.ffii.org/stidi/epc52/index.en.html}} that mathematical methods, intellectual methods, business methods, computer programs, presentation of information etc are not inventions in the sense of patent law. There is a systematic reason for that: in the legal tradition patents have been for concrete applications of natural science (``technical inventions\footnote{\url{http://swpat.ffii.org/stidi/korcu/index.de.html}}''), whereas patents on software cover abstract ideas. When patents are applied to software, the result is such that instead of patenting a specific mousetrap, you patent any ``means of trapping mammals'' (or, for an actual example, any ``means of trapping data in an emulated environment\footnote{\url{http://swpat.ffii.org/pikta/mupli/ep769170/index.en.html}}'').

In 1986 begon het Europese Octroobureau (EOB) met het toekennen van octrooien die gericht waren op computerprogramma's\footnote{\url{http://swpat.ffii.org/papri/epo-t840208/index.en.html}}, maar voorgesteld werden onder het mom van \emph{procesconclusies}. Deze werden meestal als volgt verwoord:

\begin{quote}
{\it 1. Proces om [gewone computerapparatuur te gebruikten], gekarakteriseerd door ...}
\end{quote}

De octrooien die op deze basis werden toegekend, werden als hypothetisch beschouwd. De reden was dat programma's als zodanig, wanneer ze via een schijf of via het Internet verspreid werden, geen proces vormden en dus geen uitvinding waren. Om deze dubbelzinnigheid op te lossen, nam het EOB de laatste stap naar de octrooieerbaarheid van pure software in 1998 door \emph{programmaconclusies} toe te staan\footnote{\url{http://swpat.ffii.org/papri/epo-t971173/index.en.html}}, i.e. conclusies van de volgende vorm:

\begin{quote}
{\it 2. computerprogramma, gekarakteriseerd doordat [met zijn hulp een proces volgens conclusie 1 kan uitgevoerd worden].}
\end{quote}
\end{sect}

\begin{sect}{reskr}{Een stopgezette poging om Art 52 EOV te amenderen}
Prior to taking this bold step, in 1997, the EPO had secured commitment for plans to rewrite the law from the following key players of the European patent system, below referred to as the ``European Patent Establishment'':
\begin{enumerate}
\item
de bestuurders van de octrooibureaus van de lidstaten, die in de Raad van Bestuur van het EOB zitten

\item
de octrooiadvocaten van een aantal grote bedrijven, die in de ``Permanente Adviescommissie van het EOB'' zitten, SACEPO

\item
de octrooiambtenaren van de Europese Commissie in de Industri\"{e}le Eigendomseenheid van het Directoraat Generaal voor de Interne Markt, dat op dat moment onder Commissaris Mario Monti viel.
\end{enumerate}

Het EOB heeft ondertussen meer dan 30.000\footnote{\url{http://swpat.ffii.org/pikta/namcu/index.en.html}} pure softwarepatenten toegekend in afwachting van de nieuwe wetgeving, en dat aantal stijgt tegenwoordig aan 3.000 per jaar.

De meeste van deze octrooien zijn algemeen en triviaal en niet beduidend anders dan gelijkaardige soorten octrooien die de VS en Japan toelaten. Het is zelfs zo dat de drie octrooibureaus (van Europa, de VS en Japan) een gemeenschappelijke ``Trilaterale Standaard''\footnote{\url{http://swpat.ffii.org/gasnu/useujp/index.de.html}} om dergelijke patenten toe te laten hebben opgesteld in mei 2000, samengevat onder de nieuwe noemer ``in computers ge\"{\i}mplementeerde uitvindingen''\footnote{\url{http://swpat.ffii.org/papri/epo-tws-app6/index.en.html}}. In een poging om de groeiende kritiek in Europa te sussen, is het octrooi-establishment later begonnen met verschillen te benadrukken in de behandeling van ``in computers ge\"{\i}mplementeerde methoden voor bedrijfsvoering''. Zelfs deze verschillen zijn echter onbeduidend\footnote{\url{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/index.en.html}}.

In augustus 2000 heeft de Europese Octrooiorganisatie, de intergouvernementele organisatie die het EOB bestuurt, {\bf gepoogd om alle uitzonderingen van Art 52 van het Europese Octrooiverdrag te schrappen}. Vanwege publieke tegenstand die ze blijkbaar niet verwacht hadden, is deze poging gefaald.
\end{sect}

\begin{sect}{europarl}{Een mislukte poging om het Europees Parlement te misleiden}
In 2002 heeft het Directoraat Generaal voor de Interne Markt van de Europese Commissie (onder Monti's opvolger Frits Bolkestein) voorstel 2002/0047\footnote{\url{http://swpat.ffii.org/papri/eubsa-swpat0202/index.en.html}} voor een Richtlijn ``over de octrooieerbaarheid van in computers ge\"{\i}mplementeerde uitvindingen'' ingediend. Men beweerde dat de richtlijn bedoeld was om de wetten van de lidstaten te harmoniseren en om sommige details te verduidelijken, met als doel de excessen van het EOB in te dijken. Bij nader inzien bleek echter dat het voorstel van de Commissie ontwikkeld was om {\bf de onbeperkte octrooieerbaarheid zoals gehandhaafd door het EOB te codificeren}, met \'{e}\'{e}n uitzondering: het liet geen programmaconclusies toe.

Op 24 september 2003 stemde\footnote{\url{http://swpat.ffii.org/lisri/03/plen0924/index.nl.html}} het Europees Parlement in zijn geheel (plenaire bijeenkomst) om een reeks amendementen toe te voegen aan de richtlijn\footnote{\url{http://swpat.ffii.org/papri/europarl0309/index.nl.html}}, die werkelijk bereikten wat de Commissie had geveinsd te willen bekomen: hierdoor werd een duidelijk stel uniforme regels gecre\"{e}erd, die de niet-octrooieerbaarheid van programmeren en bedrijfslogica herbevestigden en de vrijheid van publicatie en interoperabiliteit verzekerden. Deze verzameling amendementen was gebaseerd op een jaar werk van de Parlementaire commissies voor Cultuur en Onderwijs\footnote{\url{http://wiki.ffii.org/Cult0312Nl}} en Industrie en Handel\footnote{\url{http://wiki.ffii.org/Itre0312Nl}}. De richtlijn was echter opgesteld om onder de bevoegdheid van de commissie Juridische Zaken (JURI) te vallen, die gedomineerd wordt door MEPs met een grote affiniteit voor het octrooi-establishment. JURI had de voorstellen van de andere commissies genegeerd en een verzameling valse beperkingen op octrooieerbaarheid\footnote{\url{http://swpat.ffii.org/lisri/03/juri0617/index.en.html}} voorgesteld, in een poging de plenaire vergadering te misleiden. Een massale uitbarsting van de publieke opinie die honderden duizenden softwareprofessionals en wetenschappers samenbracht, grotendeels geco\"{o}rdineerd door FFII\footnote{\url{http://www.ffii.org/index.nl.html}}, sterkte de beslissing van het Parlement om voor echte beperkingen op de octrooieerbaarheid te stemmen.
\end{sect}

\begin{sect}{cons}{De Raad onder de wankele controle van het octrooi-etablishment}
Volgens de Codecisieprocedure\footnote{\url{http://swpat.ffii.org/papri/eubsa-swpat0202/decid/index.en.html}} van de EU werd het voorstel vervolgens behandeld door de Raad van Ministers. In de Raad is de ``Werkgroep Intellectueel Eigendom (Octrooien)'' verantwoordelijk voor het dossier. Deze groep bestaat uit exact dezelfde leden als de Raad van Bestuur van de het EOB: de octrooiambtenaren van de nationale regeringen.

Na een aantal maanden van geheime onderhandelingen, produceerde de ``Werkgroep'' een compromisdocument\footnote{\url{http://swpat.ffii.org/papri/europarl0309/cons0401/index.nl.html}} dat {\bf alle beperkende amendementen van het Parlement verwijderde, het voorstel van de Commissie herinvoerde en bovendien programmaconclusies toeliet (Art 5(2}), alle voorzieningen voor vrijwaring van interoperabilteit binnen het octrooirecht schrapte (Rec 17) en een paar valse beperkingen van JURI toevoegde (Art 4A etc). Het resultaat was de meest extreme en misleidende tekst die tot hiertoe gezien was, zonder ook maar enig compromis. Publieke toegang tot dit document werd tot de laatste minuut geweigerd ``vanwege de gevoelige aard van de onderhandelingen en het ontbreken van een hoger publiek belang''.

De Raad van Ministers aanvaardde de tekst van de Werkgroep met een kleine meerderheid\footnote{\url{http://swpat.ffii.org/lisri/04/cons0518/index.en.html}} op 18 mei 2004, ondanks de klaarblijkelijke intentie van een aantal landen om de belofte van Duitsland om tegen te stemmen te volgen. In die sessie beweerde Duitsland echter plots tevreden te zijn door een betekenisloos amendement; Nederland steunde het document terwijl het toegaf dat het problematisch kon zijn; en Commissaris Frits Bolkestein voegde een amendement toe in Artikel 4 dat, beweerde hij, duidelijk software niet-octrooieerbaar maakte, terwijl zijn verwoording in feite enkel de misleidende terminologie bevestigde. Hij vergat ook te vermelden dat {\bf Artikel 5 (2), door programmaconclusies toe te laten, in duidelijke bewoordingen het tegendeel stelt}. In de persconferentie die volgde op de stemming van de Raad kon Bolkestein geen voorbeelden geven van software die volgens het voorstel niet octrooieerbaar zou zijn. De stemming zelf was ook opmerkelijk voor de manier waarop het Ierse Voorzitterschap Denemarken onder druk zette voor haar stem\footnote{\url{http://wiki.ffii.org/ConsVideo0405En}}, waardoor de nipte vereiste meerderheid gehaald werd.

Na wat routinebewerkingen en vertalingswerk, wordt verwacht dat de Raad het dit voorstel formeel zal bekrachtigen in september 2004. Het zal vervolgens terugkeren naar het Europees Parlement voor een volgende lezing. In dit stadium kan het Parlement het gewoon verwerpen, het aanvaarden zoals het is, of staan op amendementen gelijkaardig aan deze die het vorige keer had goedgekeurd (al moeten deze nu de steun van een absolute meerderheid worden). Er zullen dan zonder twijfel pogingen komen van de uitlopers van het octrooi-establishment in de JURI commissie om een nieuwe reeks valse beperkingen voor te stellen, bewerend dat ze gebaseerd zijn op diepgaande onderhandelingen en een moeilijk ``compromis'' met de Raad.

In early April the European Parliament decided to conduct a second reading on the basis of the Council's ``Uncommon Position'', as it was now colloquially called.  In a second reading, the Parliament can vote for amendments similar to the ones for which it had previously voted, but the majority requirements are higher than in the 1st reading.  Any absent MEP will be counted as voting for the Council.  There will without doubt be attempts by the patent establishment's allies in the Parliament to propose another set of fake limits and to pretend that this is based on meaningful negotiation and difficult ``compromises'' with the Council.  FFII has prepared an information page for MEPs\footnote{\url{http://swpat.ffii.org/papri/europarl0309/plen05/index.nl.html}}.
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/ShortIntro.el ;
% mode: latex ;
% End: ;

