\begin{subdocument}{swnepon023}{EPO 2002-03 on business methods and genes: silver not buried here, 300 ounces}{http://swpat.ffii.org/news/epon023/index.en.html}{Workgroup\\swpatag@ffii.org}{This month the EPO has issued three press releases which make quite a lot of noise about refusal to search/grant applications for pure genes or pure business mehtods.  FFII welcomes these messages but warns that they do not mean what they appear to mean and explains why business methods and genes may nonetheless be patentable.}
\begin{sect}{text}{EPO Notes and how to read them}
The following notes were published by the EPO managment this month:

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Notice from the European Patent Office concerning business methods\footnote{http://www.epo.co.at/news/info/2002\_03\_25\_e.htm}}}
\filbreak

\item
{\bf {\bf Information from the European Patent Office International Treaties PCT\footnote{http://www.epo.co.at/news/info/2002\_03\_01\_e.htm}}}
\filbreak

\item
{\bf {\bf Decision of the Opposition Division dated 20 June 2001: Genes not patentable without enabling disclosure of function\footnote{http://www.epo.co.at/news/info/2001\_06\_20\_e.htm}}}
\filbreak
\end{itemize}
\else
\dots
\fi

The EPO is sending out the message that it wants to put limits on patentability, but from this alone it is very difficult to say whether this is more than a warning that patent claims and descriptions must be crafted more carefully.  Such a warning alone is of course also helpful, because it can mean that not everything coming from the USA will be granted a patent automatically in Europe. The need to reword business methods in computing terms and to substantiate functions of genes can be a hurdle that diminishes the number of patents and helps the EPO reduce its examination backlog.

It is encouraging to see how seriously the EPO seems to be taking Art 52 when the search for business methods is concerned. They even refrain from constructing the ``XXX ``as such'''' terms which are normally used in order to make all limits on patentability appear mysterious and meaningless.  Instead we read in pleasantly straightforward terms:

\begin{quote}
{\it The EPO also wishes to remind applicants that methods of doing business per se are excluded from patentability pursuant to [5]Article 52(2)(c) and (3) EPC. Claims of European patent applications which relate to such methods or merely specify commonplace features relating to the technological implementation of such methods will not be searched if the search examiner cannot establish any technical problem which might potentially have required an inventive step for it to be overcome. In other words, in such cases it is not possible to carry out a meaningful search into the state of the art.}
\end{quote}

The same should be true if the examiner cannot establish any technical problem outside the realm of programming solutions, which are also per se not patentable inventions according to Art 52 EPC.

On the other hand there is no reason why the EPO should not act as a PCT searching authority for either programs or business methods. Yes, a ``meaningful search'' may be impossible, but if the USPTO and others grant ``meaningless patents'', related searches nevertheless acquire a meaning, ``particularly for European SMEs who lack experience in global patenting'', as our patent politicians would usually chant.

But let's be glad that the EPO at least sees a need to present itself as moderate. This need seems to be felt quite urgently. Two press releases are used on it this month, and one of almost exactly the same content and wording was already published in August 2001:

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf EPO Press Release: We will no longer conduct novelty searches for innovations that use conventional technology to implement what is essentially a method for doing business not patentable under Art 52 EPC\footnote{http://www.epo.co.at/news/pressrel/2001\_08\_13\_e.htm}}}
\filbreak
\end{itemize}
\else
\dots
\fi

This was two months before the release of new examination guidelines\footnote{http://swpat.ffii.org/news/epgl01A/index.en.html} which make all computer-implemented business methods patentable and simultaneous to the publication of an article\footnote{http://swpat.ffii.org/papers/grur-kober0106/index.de.html} by EPO president Kober\footnote{http://swpat.ffii.org/players/kober/index.en.html}, which says that ``technical processes in the field of programming and computer-implemented business methods remain patentable'' and announces a change of examination guidelines to reflect this doctrine.

Since ``computer-impemented business methods'' are a subcategory of ``computer-implemented inventions'', and ``computer-implemented inventions'', according to the EPO Newspeak which is also used in the EU/BSA directive proposal\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/index.en.html}, ``belong to a field of technology'', we can prove by a simple sequence of quotations from EU/BSA, combined with a few allowable substitution operations:

\begin{quote}
2.a. If the performance of the business method use a computer and if the business method have new features realised by a computer program it is a ``computer-implemented invention''.\\
4.1. A computer-implemented business method is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.\\
4.2. A condition of involving an inventive step is that a computer-implemented business method must make a technical contribution.\\
2.b. A ``technical contribution'' is a contribution to the state of the art in a technical field.\\
3. A ``computer-implemented business method'' is considered to belong to a field of technology.\\
Business methods are patentable.\\
Q.E.D.
\end{quote}

There is an old Chinese idiom that seems to say very accurately what the EPO is repeatedly shouting to us here:

\begin{quote}
{\it By digging here you will find no silver, 300 ounces}

{\it 此地無銀三百兩}
\end{quote}
\end{sect}

\begin{sect}{links}{Annotated Links}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf European Patent Office: High Above Legality\footnote{http://swpat.ffii.org/players/epo/index.en.html}}}

\begin{quote}
The European Patent Office finances itself by fees from the patents which it grants.  It is free to use a certain percentage of these fees.  Since the 1980s the EPO has illegally lowered the standards of technicity, novelty, non-obviousness and industrial applicability and abolished examination quality safeguards so as to increase the number of granted patents by more than 10\percent{} and the license tax on the industry by 26\percent{} per year.  As an international organisation, the EPO is not subject to criminal law or taxation.  The local police's power ends at the gates of the EPO.  High EPO officials have inflicted corporal injury on their employees and then escaped legal consequences by their right to immunity.  The work climate within the EPO is very bad, leading to several suicides per year.  The quality of examination reached a relative high in the 80s but has after that been deteriorating, partly because the EPO had to hire too many people too quickly for too low wages.  Examiners who reject patents load more work on themselves without getting more pay.  Examiners are treated by the EPO management as a kind of obstacle to the corporate goal of earning even more patent revenues.  The high-level employees of the EPO owe their jobs to political pressures from within national patent administrations and do not understand the daily work of the office.  The EPO has its own jurisdictional arm, consisting of people whose career is controlled by the EPO's managment and its internal climate. The national organs that are supposed to supervise the EPO are all part of the same closed circle, thus guaranteeing the EPO managment enjoys feudal powers in a sphere outside of any constitutional legality, and that whatever they decide is propagated to the national administrations and lawcourts.
\end{quote}
\filbreak

\item
{\bf {\bf Art 52 EPC: Interpretation and Revision\footnote{http://swpat.ffii.org/analysis/epc52/index.en.html}}}

\begin{quote}
The limits of what is patentable which were laid down in the European Patent Convention of 1973 have been blurred over the years.  Leading patent courts have interpreted Art 52 in a way that renders it almost meaningless in practise.  Numerous law scholars have shown why this is not permissible. The EPO has proposed to revise Art 52 so as to bring it in line with its practise of unlimited patentability.  One could however also do the opposite: regulate patentability along the lines of the original Art 52 but in a way that leaves fewer possibilities of abuse.  This documentation explores what has happened and what can be done.
\end{quote}
\filbreak

\item
{\bf {\bf Moses, the Ten Exclusions from Patentability and ``stealing with a further ethical effect''\footnote{http://swpat.ffii.org/analysis/epc52/moses/index.en.html}}}

\begin{quote}
Computer programs are both unpatentable and patentable in Europe.  How did the European Patent Office's Technical Boards of Appeal gradually manage to patent the unpatentable?  Where taboos and artificially induced complexity mine the road, satiric comparison is often the fastest way to a thourough understanding.
\end{quote}
\filbreak

\item
{\bf {\bf Patent Jurisprudence on a Slippery Slope -- the price for dismantling the concept of technical invention\footnote{http://swpat.ffii.org/analysis/invention/index.en.html}}}

\begin{quote}
So far computer programs and other \emph{rules of organisation and calculation} are not \emph{patentable inventions} according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant research literature.
\end{quote}
\filbreak

\item
{\bf {\bf EPO 2000-08-18: business methods patentable if they have a technical effect\footnote{http://www.european-patent-office.org/news/pressrel/2000\_08\_18\_e.htm}}}

\begin{quote}
Press Release by the European Patent Office shortly before the Diplomatic Conference
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatnews.el ;
% mode: latex ;
% End: ;

