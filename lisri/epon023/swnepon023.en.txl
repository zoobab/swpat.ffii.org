<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Pnr: Press Release by the European Patent Office shortly before the Diplomatic Conference

#Boi: By digging here you will find no silver, 300 ounces

#TeP: There is an old Chinese idiom that seems to say very accurately what the EPO is repeatedly shouting to us here:

#BeW: Business methods are patentable.

#Ane: A %(q:computer-implemented business method) is considered to belong to a field of technology.

#Asf: A %(q:technical contribution) is a contribution to the state of the art in a technical field.

#ApW: A condition of involving an inventive step is that a computer-implemented business method must make a technical contribution.

#AoW: A computer-implemented business method is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#Idm: If the performance of the business method use a computer and if the business method have new features realised by a computer program it is a %(q:computer-implemented invention).

#Seo: Since %(q:computer-impemented business methods) are a subcategory of %(q:computer-implemented inventions), and %(q:computer-implemented inventions), according to the EPO Newspeak which is also used in the %(eb:EU/BSA directive proposal), %(q:belong to a field of technology), we can prove by a simple sequence of quotations from EU/BSA, combined with a few allowable substitution operations:

#TWg: This was two months before the %(gl:release of new examination guidelines) which make all computer-implemented business methods patentable and simultaneous to the publication of an %(gk:article) by EPO president %(ik:Kober), which says that %(q:technical processes in the field of programming and computer-implemented business methods remain patentable) and announces a change of examination guidelines to reflect this doctrine.

#Bno: But let's be glad that the EPO at least sees a need to present itself as moderate. This need seems to be felt quite urgently. Two press releases are used on it this month, and one of almost exactly the same content and wording was already published in August 2001:

#OWu: On the other hand there is no reason why the EPO should not act as a PCT searching authority for either programs or business methods. Yes, a %(q:meaningful search) may be impossible, but if the USPTO and others grant %(q:meaningless patents), related searches nevertheless acquire a meaning, %(q:particularly for European SMEs who lack experience in global patenting), as our patent politicians would usually chant.

#Tnr2: The same should be true if the examiner cannot establish any technical problem outside the realm of programming solutions, which are also per se not patentable inventions according to Art 52 EPC.

#Tca: The EPO also wishes to remind applicants that methods of doing business per se are excluded from patentability pursuant to [5]Article 52(2)(c) and (3) EPC. Claims of European patent applications which relate to such methods or merely specify commonplace features relating to the technological implementation of such methods will not be searched if the search examiner cannot establish any technical problem which might potentially have required an inventive step for it to be overcome. In other words, in such cases it is not possible to carry out a meaningful search into the state of the art.

#IcW: It is encouraging to see how seriously the EPO seems to be taking Art 52 when the search for business methods is concerned. They even refrain from constructing the %(q:XXX %(q:as such)) terms which are normally used in order to make all limits on patentability appear mysterious and meaningless.  Instead we read in pleasantly straightforward terms:

#Tip: The EPO is sending out the message that it wants to put limits on patentability, but from this alone it is very difficult to say whether this is more than a warning that patent claims and descriptions must be crafted more carefully.  Such a warning alone is of course also helpful, because it can mean that not everything coming from the USA will be granted a patent automatically in Europe. The need to reword business methods in computing terms and to substantiate functions of genes can be a hurdle that diminishes the number of patents and helps the EPO reduce its examination backlog.

#Tnr: The following notes were published by the EPO managment this month:

#EaW: EPO Notes and how to read them

#descr: This month the EPO has issued three press releases which make quite a lot of noise about refusal to search/grant applications for pure genes or pure business mehtods.  FFII welcomes these messages but warns that they do not mean what they appear to mean and explains why business methods and genes may nonetheless be patentable.

#title: EPO 2002-03 on business methods and genes: silver not buried here, 300 ounces

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnepon023 ;
# txtlang: en ;
# multlin: t ;
# End: ;

