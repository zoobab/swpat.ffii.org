\begin{subdocument}{swnepgl01A}{Kober's Directive: EPO President authorises patents on computer programs and business methods}{http://swpat.ffii.org/news/epgl01A/index.en.html}{Workgroup\\swpatag@ffii.org}{While the European Commission is preparing a Directive about the limits of patentability, the European Patent Office (EPO) has announced new examination guidelines which sanction its current practise of allowing direct claims to computer programs and oblige examiners to apply this practise, which had been introduced by EPO judges at a low level in 1998 in anticipation of a change of law which later was was not carried out.}
\begin{sect}{regl}{The new Rules}
The new rules can be summarised as follows
\begin{enumerate}
\item
Art 52(2) EPC is a list of non-inventions.

\item
The ``programs for computers'' listed therein are ``computer-implementable inventions''.

\item
Programs can be patented as such, when they have a ``further technical effect''.

\item
Practically all programs have a ``further technical effect''.
\end{enumerate}
\end{sect}

\begin{sect}{ref}{References and Further Reading}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf EPO 2001-10-05 press release: Amendment of the Guidelines for Examination in the European Patent Office regarding the patentability of business methods and computer-related inventions\footnote{http://www.epo.co.at/news/pressrel/2001\_10\_05\_e.htm}}}

\begin{quote}
The EPO announces that it has changed changed its examination guidelines based on the decision T 1173/97 which made claims to ``computer program products'' and computer programs admissible.
\end{quote}
\filbreak

\item
{\bf {\bf EPO Examination Guidelines on Inventions and Art 52 EPC\footnote{http://www.european-patent-office.org/legal/gui\_lines/e/c\_iv\_2.htm}}}

\begin{quote}
The section from the Guidelines for Examination in the European Patent Office of 2001-10-05 that deals with what are Patentable Inventions and how Art 52 of the European Patent Invention is to be applied in examination.  Especially in the sections concerning computer programs and business methods, it says exactly the opposite of what the guidelines of 1978 said: today practically all rules of organisation and calculation are patentable.
\end{quote}
\filbreak

\item
{\bf {\bf Guidelines for Examination of the European Patent Office 1978-06-01\footnote{http://swpat.ffii.org/papers/epo-gl78/index.en.html}}}

\begin{quote}
These are the first examination guidelines to be drafted by the EPO after its founding by the European Patent Convention in 1973.  They contain clear explanations of Art 52 EPC, including a definition of computer programs that covers all aspects from algorithms to processes and structures stored on a medium, and a common-sense explanation of the ``as such'' clause as an exhortation to the examiner to look behind the claim wording and analyse what the contribution to progress really consists in.   The document explains that an invention per se needs to have technical features which offset it from the sphere of non-inventions.   Thus the Guidelines make it clear that the apposition ``as such'' in art 52(3) does not restrict the applicability of the exclusions in Art 52(3).  However thy fall short of defining what kind of contribution is technical.  Although the examples point to applications of physical causality, this is not clearly said.   On the other hand, the list of exclusions in art 52 is construed to refer to ``abstractions'' and to include non-inventive materialisations of mathematical methods and computer programs.  In the 1985 revision, the substantive guidelines in part C remained largely unchanged --- except for the the patentability part and particularly the sections on programs for computers, where new lengthy explanations opened further avenues for the gradual removal of all limits on patentability.
\end{quote}
\filbreak

\item
{\bf {\bf phm: Kobers Swpat-Richtlinie\footnote{http://lists.ffii.org/archive/mails/swpat/2001/Oct/0040.html}}}

\begin{quote}
Evaluation and Discussion of the New Examination Rules
\end{quote}
\filbreak

\item
{\bf {\bf phm: EPO Swpat Guideline\footnote{http://aful.org/wws/arc/patents/2001-10/msg02397.html}}}
\filbreak

\item
{\bf {\bf Ingo Kober and Software Patents\footnote{http://swpat.ffii.org/players/kober/index.en.html}}}

\begin{quote}
president of the European Patent Office (EPO) since the mid-nineties, former FDP (liberal party) secretary of state in the BMJ (ministery of justice), active promoter of patentability expansion and of international unification of the patent system.
\end{quote}
\filbreak

\item
{\bf {\bf Kober 2001: Die Rolle des Europ\"{a}ischen Patentamts\footnote{http://swpat.ffii.org/papers/grur-kober0106/index.de.html}}}

\begin{quote}
Dr. h.c. Ingo Kober, president of the European Patent Office (EPO), explains how much the european patent system has already achieved, how important it has become for the knowledge economy and that, as far as software and business methods are concerned, the EPO enjoys the full support of the national governments.  Yes, they did not fulfill the EPO's wish to remove the computer program and business method exclusions in Art 52 EPC at the Diplomatic Conference of November 2000, but that does not change the fact that computer programs or computer-implemented business methods are patentable at the EPO, the president explains, nor does it imply any criticism of this EPO practise.
\end{quote}
\filbreak

\item
{\bf {\bf Juridical Coup at the European Patent Office\footnote{http://www.eurolinux.org/news/coup01A/index.en.html}}}

\begin{quote}
The president of the European Patent Office has, in preemption of political decisions to be taken by European governments, decreed a regulation that authorises patent claims to computer programs.  The Eurolinux Alliance calls for the replacement of the board of the European Patent Office by the european governments and the establishment of democratic control over the european patent system.
\end{quote}
\filbreak

\item
{\bf {\bf EPO decision T 1173/97: IBM computer program product\footnote{http://swpat.ffii.org/papers/epo-t971173/index.en.html}}}

\begin{quote}
In 1998 the EPO's Technical Board of Appeal decides: ``A computer program product is not excluded from patentability under Article 52(2'' and (3) EPC if, when it is run on a computer, it produces a further technical effect which goes beyond the normal physical interactions between program (software) and computer (hardware).) The document closes with an ``editor's note'': ``The EPO intends to adjust its practise in the light of this decision.  The Guidelines for Examination in the EPO will be adapted accordingly; relevant information will be published in due course.''
\end{quote}
\filbreak

\item
{\bf {\bf Moses, the Ten Exclusions from Patentability and ``stealing with a further ethical effect''\footnote{http://swpat.ffii.org/analysis/epc52/moses/index.en.html}}}

\begin{quote}
Computer programs are both unpatentable and patentable in Europe.  How did the European Patent Office's Technical Boards of Appeal gradually manage to patent the unpatentable?  Where taboos and artificially induced complexity mine the road, satiric comparison is often the fastest way to a thourough understanding.
\end{quote}
\filbreak

\item
{\bf {\bf Stalin \& Software Patents\footnote{http://swpat.ffii.org/analysis/epc52/stalin/index.en.html}}}

\begin{quote}
In this contribution to the European Patent Office (EPO) mailing list, a European patent attorney cites the EPO Examination Guidelines of 1978 as clear documentary evidence for the intention of the legislator to keep computer programs on any storage medium free from any claims to the effect that their distribution, sale or use could infringe on a patent.  But the European Patent Office's Technical Board of Appeal (TBA) apparently considered itself to be a kind of modern Stalin, an ultimate sources of wisdom in matters of whatever complexity, standing high above the legislator and the peoples of Europe, and even above the EPO's own institutions for judicial review.  In this way the TBA risks to antagonise the public, to create harmful legal insecurity especially for small patent-holders and to severely damage the delicate process of building confidence in international institutions.  The TBA should see itself as a conservator rather than an innovator.
\end{quote}
\filbreak

\item
{\bf {\bf Patent Jurisprudence on a Slippery Slope -- the price for dismantling the concept of technical invention\footnote{http://swpat.ffii.org/analysis/invention/index.en.html}}}

\begin{quote}
So far computer programs and other \emph{rules of organisation and calculation} are not \emph{patentable inventions} according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant research literature.
\end{quote}
\filbreak

\item
{\bf {\bf }}
\filbreak

\item
{\bf {\bf European Consultation on the Patentability of Computer-Implementable Rules of Organisation and Calculation (= Programs for Computers)\footnote{http://swpat.ffii.org/papers/eukonsult00/index.en.html}}}

\begin{quote}
On 2000-10-19 the European Commission's Industrial Property Unit published a position paper which tries to describe a legal reasoning similar to that which the European Patent Office has during recent years been using to justify its practise of granting software patents against the letter and spirit of the written law, and called on companies and industry associations to comment on this reasoning.  The consultation was evidently conceived as a mobilisation exercise for patent departments of major corporations and associations.  The consultation paper itself stated the viewpoint of the European Patent Office and asked questions that could only be reasonably answered by patent lawyers.  Moreover, it was accompanied by an ``independent study'', carried out under the order of the EC IndProp Unit by a well known patent movement think-tank, which basically stated the same viewpoint.  Patent law experts of various associations and corporations responded, mostly by applauding the paper and explaining that patents are needed to stimulate innovation and to protect the interests of small and medium-size companies.  However there were also quite a few associations, companies and more than 1000 individuals, mostly programmers, who expressed their opposition to the extension of patentability to the realm of software, business methods, intellectual methods and other immaterial products and processes.  The EC IndProp Unit later failed to adequately publish the consultation results and moderate a discussion.  Therefore we are doing this, and you can help us.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatnews.el ;
% mode: latex ;
% End: ;

