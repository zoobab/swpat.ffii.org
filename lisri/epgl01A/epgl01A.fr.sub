\begin{subdocument}{swnepgl01A}{Directive Kober: Le pr\'{e}sident de l'OEB autorise les brevets sur programmes d'ordinateur et m\'{e}thodes de gestion}{http://swpat.ffii.org/neuf/epgl01A/index.fr.html}{Groupes de travail\\swpatag@ffii.org}{Au moment ou la Commission Europ\'{e}enne pr\'{e}pare une directive sur les limites de la brevetabilit\'{e} a l'\'{e}gard du logiciel, l'Office Europ\'{e}en de Brevets (OEB) a d\'{e}cret\'{e} des nouvelles R\`{e}gles d'Examen, dans lesquels il officialise sa r\'{e}cente pratique du brevetage directe des programmes d'ordinateur et oblige ses examinateurs a agir selon ces r\`{e}gles, qui avaient \'{e}t\'{e} introduites par une chambre juridique subalterne de l'OEB en 1998 en anticipation d'un changement de loi qui n'a pas eu lieu.}
\begin{sect}{regl}{Les nouvelles R\`{e}gles}
Les r\`{e}gles sont, simplement dites, les suivantes:
\begin{enumerate}
\item
Art 52(2) CEB constitue une liste de non-inventions.

\item
Les ``programmes pour ordinateurs'' list\'{e}s la dedans sont des ``inventions mises en oevre par ordinateur''.

\item
Les programmes d'ordinateurs peuvent \^{e}tre brevet\'{e}s en tant que tels si ils ont un ``effet technique suppl\'{e}mentaire''.

\item
Pratiquement touts les programmes d'ordinateurs ont un ``effet technique suppl\'{e}mentaire''.
\end{enumerate}
\end{sect}

\begin{sect}{ref}{R\'{e}ferences et Documents a Lire}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Communiqu\'{e} de Presse de l'OEB 2001-10-05: Changement des les nouvelles R\`{e}gles d'Examen concernant la brevetabilit\'{e} des m\'{e}thodes de gestion et des inventions li\'{e}es aux ordinateurs\footnote{http://www.epo.co.at/news/pressrel/2001\_10\_05\_f.htm}}}

\begin{quote}
L'OEB annonce qu'il a chang\'{e} ses r\`{e}gle d'examen en les adaptant a la d\'{e}cision T 1173/97, laquelle d\'{e}clare que les revendication de ``produits programme d'ordinateur'' et programme d'ordinateurs sont admissible.
\end{quote}
\filbreak

\item
{\bf {\bf R\`{e}gles d'Examen de l'OEB sur les Invention et Art 52 CEB\footnote{http://www.european-patent-office.org/legal/gui\_lines/f/c\_iv\_2.htm}}}

\begin{quote}
La partie des R\`{e}gles pour l'Examen dans l'Office Europ\'{e}en de Brevets de 2001-10-05 qui explique que est ce que c'est que les Invention Brevetables et comment art 52 de la Convention Europ\'{e}enne de Brevets doit \^{e}tre appliqu\'{e} a l'examen.  Surtout les section sur le suj\'{e}t des Programmes Pour Ordinateurs et Proc\'{e}d\'{e}s pour Activit\'{e}s Commerciales disent le contraire de ce qui \'{e}tait \'{e}crit dans les r\`{e}gles d'examen de 1978:  Au jour d'hui l'OEB consid\`{e}re pratiquement toute les r\`{e}gles d'organisation et calcul comme brevetables.
\end{quote}
\filbreak

\item
{\bf {\bf R\`{e}gles d'Examen de l'Office Europ\'{e}en des Brevets 1978-06-01\footnote{http://swpat.ffii.org/papiers/epo-gl78/index.en.html}}}

\begin{quote}
Apr\`{e}s sa fondation par la Convention Europ\'{e}enne de Brevets (CEB) en 1973, l'Office Europ\'{e}en de Brevets (OEB) commenc a expliquer le CEB en d\'{e}tail dans ses R\`{e}gles d'Examen.  Cette premi\`{e}re version, qui entre en vigeur en 1978, explique sans \'{e}quivoque qu'est-ce que sont les \emph{inventions} selon Art 52 CEB, qu'est-ce que sont les \emph{programmes d'ordinateur} et pour quoi ils ne sont, autant comme les m\'{e}thodes math\'{e}matiques, m\'{e}thodes de gestion, structures d'information etc pas des inventions brevetables (techniques).  Selon cela, l'exclusion des \emph{programmes d'ordinateurs} porte autant sur les algorithmes que sur les produit et proc\'{e}d\'{e}s informatiques et toute autre ``abstractions'' dans toutes leurs impl\'{e}mentations, et l'examinateur est exhort\'{e} \`{a} juger les revendications non pas selon leur language mais selon la contribution technique visible la dedans (= contemplation diff\'{e}rentielle / th\'{e}orie de noyeaux).  En contraste avec la doctrine allemande, les R\`{e}gles de 1978 ne d\'{e}finissent pas qu'est-ce que c'est qu'une contribution technique.  Dans la revision de 1985 cette manque n'etait pas corrig\'{e}e, mais la d\'{e}finition du programme d'ordinateur etait \'{e}ffac\'{e}e et remplac\'{e}r par des longues passages ambigues, qui ouvraient la voie a l'enl\`{e}vement graduel de toutes les limites de la brevetabilit\'{e}.
\end{quote}
\filbreak

\item
{\bf {\bf phm: Kobers Swpat-Richtlinie\footnote{http://lists.ffii.org/archive/mails/swpat/2001/Oct/0040.html}}}

\begin{quote}
Evaluation et Discussion des nouvelles R\`{e}gles
\end{quote}
\filbreak

\item
{\bf {\bf phm: EPO Swpat Guideline\footnote{http://aful.org/wws/arc/patents/2001-10/msg02397.html}}}
\filbreak

\item
{\bf {\bf Ingo Kober and Software Patents\footnote{http://swpat.ffii.org/acteurs/kober/index.en.html}}}

\begin{quote}
president of the European Patent Office (EPO) since the mid-nineties, former FDP (liberal party) secretary of state in the BMJ (ministery of justice), active promoter of patentability expansion and of international unification of the patent system.
\end{quote}
\filbreak

\item
{\bf {\bf Kober 2001: Le R\^{o}le de l'Office Europ\'{e}en de Brevets\footnote{http://swpat.ffii.org/papiers/grur-kober0106/index.de.html}}}

\begin{quote}
Dr. h.c. Ingo Kober, president of the European Patent Office (EPO), explains how much the european patent system has already achieved, how important it has become for the knowledge economy and that, as far as software and business methods are concerned, the EPO enjoys the full support of the national governments.  Yes, they did not fulfill the EPO's wish to remove the computer program and business method exclusions in Art 52 EPC at the Diplomatic Conference of November 2000, but that does not change the fact that computer programs or computer-implemented business methods are patentable at the EPO, the president explains, nor does it imply any criticism of this EPO practise.
\end{quote}
\filbreak

\item
{\bf {\bf Putsch Juridique \`{a} l'Office Europ\'{e}en de Brevets\footnote{http://www.eurolinux.org/news/coup01A/index.fr.html}}}

\begin{quote}
Le pr\'{e}sident de l'Office Europ\'{e}en de Brevets a, avant m\^{e}me d'attendre les d\'{e}cisions politiques des gouvernments europ\'{e}en, d\'{e}cret\'{e} une r\'{e}gulation qui autorise les revendications de brevet sur les programmes d'ordinateur.  L'Alliance EuroLinux exige le limogeage de la direction de l'Office Europ\'{e}en de Brevets par les gouvernements europ\'{e}ens et le renforcement de son contr\^{o}le d\'{e}mocratique.
\end{quote}
\filbreak

\item
{\bf {\bf Logiciels et Droit de Brevet: R\'{e}censions d'Articles\footnote{http://swpat.ffii.org/papiers/epo-t971173/index.fr.html}}}

\begin{quote}
En 1998-07-01 la Chambre de Recours Technique 3.5.1 de l'OEB d\'{e}cide: ``Un produit programme d'ordinateur n'est pas exclu de la brevetabilit\'{e} en application de l'article 52(2'' et (3) CBE si sa mise en oeuvre sur un ordinateur produit un effet technique suppl\'{e}mentaire, allant au-del\`{a} des interactions physiques normales entre programme (logiciel) et ordinateur (mat\'{e}riel).) Le document finisse par une ``Note de l'\'{e}diteur'': ``L'OEB a l'intention d'adapter sa pratique \`{a} la lumi\`{e}re de cette d\'{e}cision.  Les Directive relative \`{a} l'examen pratiqu\'{e} \`{a} l'OEB seront remani\'{e}es en cons\'{e}quence.  Des information \`{a} ce sujet seront publi\'{e}es ent temps utile.''
\end{quote}
\filbreak

\item
{\bf {\bf Moses, les Dix Exclusions de la Brevetabilit\'{e} et le ``vol avec un \'{e}ffet \'{e}thique suppl\'{e}mentaire''\footnote{http://swpat.ffii.org/analyse/epc52/moses/index.fr.html}}}

\begin{quote}
Les lois fondamentale de la soci\'{e}t\'{e} humaine souvent resemblent aux Dix Commandements: Tu ne doit pas ...  Des autres exemples sont: les r\`{e}gles d'ascese des Bouddhistes, les cataloges des droit de l'homme et ... la listes de non-inventions dans l'article 52 de la Convention sur le Brevet Europ\'{e}en.  Aucun de ces cataloges ne pr\'{e}tend d'\^{e}tre compl\`{e}t.  Pour suivre les r\`{e}gles il ne suffit jamais de observes lit\'{e}ralement les interdictions.  Une interpr\'{e}tation lit\'{e}rale peut aboutir a des contradictions et du pharis\'{e}isme.  Pour suivre une v\'{e}ritable voie de vertu, on a besoin d'une th\'{e}orie de l'\emph{activit\'{e} vertueuse}.  Dans le cas de la CEB cela est la th\'{e}orie de l'\emph{invention technique}.  Cette th\'{e}orie n'est pas necessairement de validit\'{e} \'{e}ternelle.  Mais pendant que les paradigmes changent et les vieux testaments sont remplac\'{e} par les nouveaux testaments, les exclusions fondamentales doivent perdurer, est le clerus doit les soutenir par une th\'{e}orie syst\'{e}matique adapt\'{e}e a ce besoin.  Nous voudrions comparer ici comment les haut-pr\`{e}tres du syst\`{e}me de brevets europ\'{e}en ont achev\'{e} leur mission en comparaison avec les successeurs de Moses.
\end{quote}
\filbreak

\item
{\bf {\bf Stalin \& Software Patents\footnote{http://swpat.ffii.org/analyse/epc52/stalin/index.en.html}}}

\begin{quote}
In this contribution to the European Patent Office (EPO) mailing list, a European patent attorney cites the EPO Examination Guidelines of 1978 as clear documentary evidence for the intention of the legislator to keep computer programs on any storage medium free from any claims to the effect that their distribution, sale or use could infringe on a patent.  But the European Patent Office's Technical Board of Appeal (TBA) apparently considered itself to be a kind of modern Stalin, an ultimate sources of wisdom in matters of whatever complexity, standing high above the legislator and the peoples of Europe, and even above the EPO's own institutions for judicial review.  In this way the TBA risks to antagonise the public, to create harmful legal insecurity especially for small patent-holders and to severely damage the delicate process of building confidence in international institutions.  The TBA should see itself as a conservator rather than an innovator.
\end{quote}
\filbreak

\item
{\bf {\bf Jurisprudence de Brevet sur Terrain Glissant: Le Prix a Payer pour le D\'{e}montage de l'Invention Technique\footnote{http://swpat.ffii.org/analyse/invention/index.fr.html}}}

\begin{quote}
Jusqu'\`{a} pr\'{e}sent les programmes d'ordinateur et en g\'{e}n\'{e}ral les \emph{r\`{e}gles d'organisation et de calcul} ne sont pas des \emph{inventions brevetables} selon la loi Europ\'{e}enne, ce qui n'exclut pas qu'un proc\'{e}d\'{e} industriel brevetable puisse \^{e}tre control\'{e} par un logiciel.  L'Office Europ\'{e}en des Brevets et quelques Cours nationales ont cependant contourn\'{e} cette r\`{e}gle, en rempla\c{c}ant un concept de d\'{e}limitation claire par une ins\'{e}curit\'{e} l\'{e}gale sans limites reconnaissables.  Cet article offre une introduction aux probl\`{e}mes et \`{a} la litt\'{e}rature juridique sur ce sujet.
\end{quote}
\filbreak

\item
{\bf {\bf }}
\filbreak

\item
{\bf {\bf Consultation Europ\'{e}enne sur la Brevetabilit\'{e} de R\`{e}gles d'Organisation et Calculaltion mettables en oevre par ordinateur (= programmes pour ordinateurs)\footnote{http://swpat.ffii.org/papiers/eukonsult00/index.en.html}}}

\begin{quote}
En 2000-10-19 l'Unit\'{e} de Propri\'{e}t\'{e} Industrielle de la Commission Europ\'{e}enne (CE-UPI) publia un papier de consultation qui propose de l\'{e}galiser la pratique de l'Office Europ\'{e}en de Brevets (OEB) de accorder des brevets logiciels contre la lettre et l'esprit de la loi en vigeur.  Ce papier se dirigait vers les d\'{e}partements de brevet des entreprises et associations et \'{e}tait concu comme un manoevre de mobilisation.  En plus il \'{e}tait soutenu par une ``\'{e}tude ind\'{e}pendante'' conduite par des lobbyistes connus du brevet logiciel.  Des conseils en brevet travaillants pour diverses organisations envoy\`{e}rent des papiers de consultation applaudissant la ligne CE-UPI/OEB et r\'{e}citant le cr\'{e}do connu du mouvement de brevet selon lequel les brevets promouvent l'innovation dans tous les domaines et servent surtout l'int\'{e}r\^{e}t des petites entreprises.  N\'{e}anmoins il y avait aussi des reponses critiques venant d'un certain nombre de grandes associations et entreprises aussi que de plus que 1000 individus et petites unit\'{e}s.  La CE-UPI a jusqu'au pr\'{e}sent publi\'{e} les papiers seulement dans une forme incompl\`{e}te et difficile a consulter.  Nous voulons resoudre ce probl\`{e}me, et vous pouvez nous aider.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatnews.el ;
% mode: latex ;
% End: ;

