\begin{subdocument}{swnepue28}{Eur. Patent Office proposes unlimited patentability}{http://swpat.ffii.org/journal/epue28/index.en.html}{Workgroup\\swpatag@ffii.org}{IT professionals demand effective control of the EPO}
The European Patent Office (EPO) wants to induce European governments to remove all legal restrictions on patentability in november 2000.  The German Federal Ministery of Justice (BMJ) distributed a ``Base Proposal for the Revision of the European Patent Convention'' dated 2000-06-27 to the ``circles interested in the patent system'' at the beginning of August.

\begin{center}
For immediate Release
\end{center}

Munich, Berlin, Frankfurt, Ilmenau, Magdeburg

The Foundation for a Free Information Infrastructure\footnote{http://www.ffii.org/index.en.html} (FFII) sent an open letter to the BMJ, in which it pointed out some inconsistencies in the EPO's proposal and warned about devastative effects of the EPO's expansive patent policy on innovation, competition, prosperity, education and civil rights.

Ralf Schwöbel, CEO of Intradat AG\footnote{http://www.intradat.de}, a Frankfurt based market leader in e-shop systems, explains his support for the public letter:
{\it \begin{quote}
With this proposal, the EPO creates a legal framework for patenting as many business methods as the US.  As a publisher of e-commerce solutions, we believe that this is harmful, since it will deter many small and medium size enterprises from developping their own e-commerce solutions.
\end{quote}}

Recent studies show, that the European Patent Office examiners tend to grant patents on immaterial objects (such as software and business methods) even more generously than the USPTO and JPO colleagues.  According to a Japanese comparative study\footnote{http://www.jpo.go.jp/old/saikine/repo242.htm}, the EPO applies even lower standards for assessing the inventivity of immaterial objects.  Moreover, for the purpose of examining the technicity of business methods, the EPO even plans\footnote{http://swpat.ffii.org/papers/epo-tws-app6/index.en.html} to abandon its traditional request that these methods should have an ``additional technical effect'' beyond the ordinary use of the computer, as soon as the limitations on patentability are removed by the legislator as proposed by the EPO.

Matthias Schlegel, CEO of Phaidros AG\footnote{http://www.phaidros.com}, an Ilmenau based pioneer in meta-modelling of commercial processes, comments:
{\it \begin{quote}
With tens of thousands of software and business method patents floating around, our customers are concerned about legal risks and asking for guarantees.  Our board is considering the formation of millions of DEM of annual reserve funds to prepare for ligitation and patent applications.  But even if we acquire extensive software and business method patents, such as the EPO seems to be eager to grant, this would be of only limited use for protecting our copyrights on the modular systems into which we have been investing from patent attacks of third parties.  It would mainly add to the drain on our ressources, which have to be shifted away from R\&D investments to legal costs.  As far as I can see, most of the typical European software SMEs share our experience.  The patent offices do not know about this, and they don't really care.  Therefore it is extremely important that the regulative competence about what can be patented stays with the parliaments and is not transferred to the patent offices.
\end{quote}}

Co-signer Jens Enders, CEO of MDLink GmbH\footnote{http://www.skyrix.de}, a technology leader of web-based e-business systems from Magdeburg, adds:
{\it \begin{quote}
Software patents are good for aging companies who want to fence in their territory for 20 years in order to repose on their laurels.  For the protection of our competitive edge, copyright and human capital serve us well.  Patent politicians who are trying to sell us further ``strong IP protection'', seem to simply ignore the rules of our trade.
\end{quote}}

The ``Base Proposal'' completes the development, which the president of software patents workgroup of the Union of European Patent Consultants, patent attorney J\"{u}rgen Betten, predicted in several publications at the beginning of this year:
{\it \begin{quote}
By means of the ... development of jurisdiction ... the patent system has detached itself from its traditional restriction to the processing industry and is now of essential importance also for service companies in the fields of commerce, banking, insurances, telecommunication etc.  Without building a suitable patent portfolio, it is to be feared that the German service companies in these sectors will find themselves in a disadvantaged position vis-a-vis their US competitors.

...

Patent Law gives the patentee a right to exclude others from using the patented invention. ... In complex fields of technology, in which the establishment of a ``standard'' is often a prerequesite for success with the consumer, such as in the area of entertainment electronics, telecommunications or the Internet, cross-licensing has become a frequent and practical form of patent exploitation:  using their own patents ``as a currency'', companies gain access to technologies which have been patented by competing companies.

...

Since the governmental conference of the member states of the European Patent Organisation has in June 1999 in Paris entrusted the EPO with the mandate to propose before 2001-01-01 a revised version of EPC 52.2 concerning the exclusion of computer programs, so that the modified version can enter into force before 2000-07-01, it is now presumably only a question of time, until the ``computer programs'' as well as the other exemption rules are removed from EPC 52. 
\end{quote}}

The chairman of the German Parliamentary Commission on the New Media, J�rg Tauss, sees an urgent need for the legislator to act:
{\it \begin{quote}
In the circles of technological policy experts, you can often here people asserting that the patent system must be extended to certain areas of information technology, whose investments would otherwise not be sufficiently protected.  This assertion has however until now always been touted like a self-evident truth, and nobody I know of ever cared to substantiate it in the light of facts from the German or European IT economy.

Even if the patent advocates succeeded in finding areas of information technology, in which patents are having or have had some positive effects, it would nonetheless still be necessary to investigate, whether these effects are not outweighed by possible detrimental side-effects of the patent system.

But while the legislative authorities are still completely clueless on this matter, the judicial authorities are already taking action, granting thousands of software patents and pressing for a change of the legal rules.  We as legislators should therefore now take up these questions with highest priority.
\end{quote}}

The FFII letter is designed to help clarify the questions for the legislator.  It suggests means of making the EPO base proposal more specific, so as to subject the EPO to an effective control by the legislator.  Thereby it turns out that currently no valid reason exists for changing the law (EPC art 52), while there are good legal reasons for introducing precise and restrictive definitions for pliable terms such as ``technicity'' and ``industrial applicability''.

The public letter moreover cites economic studies and the EP, which has meanwhile been signed by approximately 30000 citizens, including 400 executives of IT companies, and supporting statements from nearly 300 European politicians.

But even public protests of this amplitude have so far not prompted more than a ``silence in the forest'' on the part of the patent system's decisionmakers.  Already in June 1999 the two BMJ officers in charge of negotiating for the German delegation at the intergovernmental conference in Paris did not react to an appeal based on 5000 signatures, and gave the EPO the mandate to change the European Patent Convention.  Little later, both BMJ officers were promoted to new honors in Munich.  One became a leading judge at the EPO, the other the president of the German Patent Office.  And today's president of the European Patent Office and initiator of the ``Base Proposal'', Dr. Ingo Kober, also started his career in the BMJ.

The EPO finances itself from patent fees.

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Eurolinux Petition for a Software Patent Free Europe\footnote{http://petition.eurolinux.org/}}}
\filbreak

\item
{\bf {\bf FFII: Software Patents in Europe\footnote{http://swpat.ffii.org/index.en.html}}}

\begin{quote}
For the last few years the European Patent Office (EPO) has, contrary to the letter and spirit of the existing law, granted more than 30000 patents on rules of organisation and calculation claimed in terms of general-purpose computing equipment, called ``programs for computers'' in the law of 1973 and ``computer-implemented inventions'' in EPO Newspeak since 2000.  Europe's patent movement is pressing to legitimate this practise by writing a new law.  Although the patent movement has lost major battles in November 2000 and September 2003, Europe's programmers and citizens are still facing considerable risks.  Here you find the basic documentation, starting from the latest news and a short overview.
\end{quote}
\filbreak

\item
{\bf {\bf Foundation for a Free Information Infrastructure\footnote{http://www.ffii.org/index.en.html}}}

\begin{quote}
The Foundation for a Free Information Infrastructure (FFII) is a non-profit association registered in Munich, which is dedicated to the spread of data processing literacy.  FFII supports the development of public information goods based on copyright, free competition, open standards.  More than 300 members, 700 companies and 50,000 supporters have entrusted the FFII to act as their voice in public policy questions in the area of exclusion rights (intellectual property) in data processing.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{prini}{About FFII - www.ffii.org\footnote{http://www.ffii.org}}
FFII is a non-profit association which promotes the development of open interfaces, open source software and freely available public information. FFII coordinates a workgroup on software patents which is sponsored by successful german software publishers. FFII is member of the EuroLinux Alliance.
\end{sect}

\begin{sect}{press}{contact}
\begin{description}
\item[mail:]\ info at ffii org
\item[phone:]\ Hartmut Pilch +49-89-18979927
\end{description}
\end{sect}

\begin{sect}{url}{Permanent URL of this Press Release}
http://www.ffii.org/assoc/home
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

