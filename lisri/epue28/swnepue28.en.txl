<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#cas: %(fi:FFII) is a non-profit association which promotes the development of open interfaces, open source software and freely available public information. FFII coordinates a %(sp:workgroup on software patents) which is %(sp:sponsored by successful german software publishers). FFII is member of the %(el:EuroLinux Alliance).

#nWW: About FFII

#Dsn: The EPO finances itself from patent fees.

#DWw: But even public protests of this amplitude have so far not prompted more than a %(q:silence in the forest) on the part of the patent system's decisionmakers.  Already in June 1999 the two BMJ officers in charge of negotiating for the German delegation at the intergovernmental conference in Paris did not react to an appeal based on %(sm:5000 signatures), and gave the EPO the mandate to change the European Patent Convention.  Little later, both BMJ officers were promoted to new honors in Munich.  One became a leading judge at the EPO, the other the president of the German Patent Office.  And today's president of the European Patent Office and initiator of the %(q:Base Proposal), Dr. Ingo Kober, also started his career in the BMJ.

#Dyo: The public letter moreover cites economic studies and the %{EP}, which has meanwhile been signed by approximately 30000 citizens, including 400 executives of IT companies, and supporting statements from nearly 300 European politicians.

#Dwt: The FFII letter is designed to help clarify the questions for the legislator.  It suggests means of making the EPO base proposal more specific, so as to subject the EPO to an effective control by the legislator.  Thereby it turns out that currently no valid reason exists for changing the law (EPC art 52), while there are good legal reasons for introducing precise and restrictive definitions for pliable terms such as %(q:technicity) and %(q:industrial applicability).

#Aiz: But while the legislative authorities are still completely clueless on this matter, the judicial authorities are already taking action, granting thousands of software patents and pressing for a change of the legal rules.  We as legislators should therefore now take up these questions with highest priority.

#ScW: Even if the patent advocates succeeded in finding areas of information technology, in which patents are having or have had some positive effects, it would nonetheless still be necessary to investigate, whether these effects are not outweighed by possible detrimental side-effects of the patent system.

#Itm: In the circles of technological policy experts, you can often here people asserting that the patent system must be extended to certain areas of information technology, whose investments would otherwise not be sufficiently protected.  This assertion has however until now always been touted like a self-evident truth, and nobody I know of ever cared to substantiate it in the light of facts from the German or European IT economy.

#DnW: The chairman of the German Parliamentary Commission on the New Media, J�rg Tauss, sees an urgent need for the legislator to act:

#N1l: Since the governmental conference of the member states of the European Patent Organisation has in June 1999 in Paris entrusted the EPO with the mandate to propose before 2001-01-01 a revised version of EPC 52.2 concerning the exclusion of computer programs, so that the modified version can enter into force before 2000-07-01, it is now presumably only a question of time, until the %(q:computer programs) as well as the other exemption rules are removed from EPC 52. 

#DAi: Patent Law gives the patentee a right to exclude others from using the patented invention. ... In complex fields of technology, in which the establishment of a %(q:standard) is often a prerequesite for success with the consumer, such as in the area of entertainment electronics, telecommunications or the Internet, cross-licensing has become a frequent and practical form of patent exploitation:  using their own patents %(q:as a currency), companies gain access to technologies which have been patented by competing companies.

#Den: By means of the ... development of jurisdiction ... the patent system has detached itself from its traditional restriction to the processing industry and is now of essential importance also for service companies in the fields of commerce, banking, insurances, telecommunication etc.  Without building a suitable patent portfolio, it is to be feared that the German service companies in these sectors will find themselves in a disadvantaged position vis-a-vis their US competitors.

#HWW: The %(q:Base Proposal) completes the development, which the president of software patents workgroup of the Union of European Patent Consultants, patent attorney %(JB), predicted in several publications at the beginning of this year:

#Sei: Software patents are good for aging companies who want to fence in their territory for 20 years in order to repose on their laurels.  For the protection of our competitive edge, copyright and human capital serve us well.  Patent politicians who are trying to sell us further %(q:strong IP protection), seem to simply ignore the rules of our trade.

#AhW: Co-signer %(JE), CEO of %(MG), a technology leader of web-based e-business systems from Magdeburg, adds:

#Amo: With tens of thousands of software and business method patents floating around, our customers are concerned about legal risks and asking for guarantees.  Our board is considering the formation of millions of DEM of annual reserve funds to prepare for ligitation and patent applications.  But even if we acquire extensive software and business method patents, such as the EPO seems to be eager to grant, this would be of only limited use for protecting our copyrights on the modular systems into which we have been investing from patent attacks of third parties.  It would mainly add to the drain on our ressources, which have to be shifted away from R&D investments to legal costs.  As far as I can see, most of the typical European software SMEs share our experience.  The patent offices do not know about this, and they don't really care.  Therefore it is extremely important that the regulative competence about what can be patented stays with the parliaments and is not transferred to the patent offices.

#Mnn: %(MS), CEO of %(PAG), an Ilmenau based pioneer in meta-modelling of commercial processes, comments:

#Drc: Recent studies show, that the European Patent Office examiners tend to grant patents on immaterial objects (such as software and business methods) even more generously than the USPTO and JPO colleagues.  According to a %(sk:Japanese comparative study), the EPO applies even lower standards for assessing the inventivity of immaterial objects.  Moreover, for the purpose of examining the technicity of business methods, the EPO even %(a6:plans) to abandon its traditional request that these methods should have an %(q:additional technical effect) beyond the ordinary use of the computer, as soon as the limitations on patentability are removed by the legislator as proposed by the EPO.

#MfW: With this proposal, the EPO creates a legal framework for patenting as many business methods as the US.  As a publisher of e-commerce solutions, we believe that this is harmful, since it will deter many small and medium size enterprises from developping their own e-commerce solutions.

#Ree: %(RS), CEO of %(IAG), a Frankfurt based market leader in e-shop systems, explains his support for the public letter:

#Drh: The %{FFII} sent an %(sx:open letter) to the BMJ, in which it pointed out some inconsistencies in the EPO's proposal and warned about devastative effects of the EPO's expansive patent policy on innovation, competition, prosperity, education and civil rights.

#DWE: The European Patent Office (EPO) wants to induce European governments to remove all legal restrictions on patentability in november 2000.  The German Federal Ministery of Justice (BMJ) distributed a %(q:Base Proposal for the Revision of the European Patent Convention) dated 2000-06-27 to the %(q:circles interested in the patent system) at the beginning of August.

#descr: IT professionals demand effective control of the EPO

#title: Eur. Patent Office proposes unlimited patentability

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnepue28 ;
# txtlang: en ;
# multlin: t ;
# End: ;

