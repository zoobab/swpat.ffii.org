\begin{subdocument}{swnepue28}{L'Office Europ\'{e}en des Brevets propose une brevetabilit\'{e} illimit\'{e}e}{http://swpat.ffii.org/neuf/epue28/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise 2000/09/10 par Olivier Berger\footnote{http://www.april.org}}{Des professionnels de l'informatique demandent un contr\^{o}le effectif de l'OEB}
L'Office Europ\'{e}en des Brevets (OEB) veut inciter les gouvernements Europ\'{e}ens \`{a} supprimer toute restriction l\'{e}gale sur la brevetabilit\'{e} en novembre 2000. Le Minist\`{e}re F\'{e}d\'{e}ral Allemand de la Justice (BMJ) a distribu\'{e} une ``Proposition de Base pour la R\'{e}vision de la Convention Europ\'{e}enne des Brevets'' dat\'{e}e du 27-06-2000 aux ``cercles int\'{e}ress\'{e}s dans le syst\`{e}me des brevets'' au d\'{e}but du mois d'ao\^{u}t.

\begin{center}
Pour publication imm\'{e}diate
\end{center}

Munich, Berlin, Frankfurt, Ilmenau, Magdeburg

La Association pour une Infrastructure Informatique Libre\footnote{http://www.ffii.org/index.fr.html} (FFII) a envoy\'{e} une lettre ouverte au BMJ, dans laquelle elle a mis en \'{e}vidence des incoh\'{e}rences dans la proposition de l'OEB et a averti contre les effets d\'{e}vastateurs de la politique expansive de l'OEB sur l'innovation, la comp\'{e}tition, la prosp\'{e}rit\'{e}, l'\'{e}ducation et les libert\'{e}s publiques.

Ralf Schw�bel, CEO de Intradat AG\footnote{http://www.intradat.de}, un leader sur le march\'{e} des syst\`{e}mes de boutiques en ligne bas\'{e} \`{a} Francfort, explique son soutien \`{a} cette lettre ouverte:
{\it \begin{quote}
Avec cette proposition, l'OEB cr\'{e}e un cadre l\'{e}gal pour breveter autant de m\'{e}thodes d'entreprises qu'aux USA. En tant qu'\'{e}diteur de solutions d'e-commerce, nous pensons que cela est n\'{e}faste, puisque cela dissuadera de nombreuses petites et moyennes entreprises de d\'{e}velopper leurs propres solutions de e-commerce.
\end{quote}}

De r\'{e}centes \'{e}tudes montrent que les examinateurs de l'Office Europ\'{e}en des Brevets tendent m\^{e}me \`{a} accorder des brevets sur des objets immat\'{e}riels (tels que les logiciels et les m\'{e}thodes d'entreprises) de fa\c{c}on encore plus g\'{e}n\'{e}reuse que leurs coll\`{e}gues de l'USPTO et du JPO. D'apr\`{e}s une \'{e}tude comparative Japonaise\footnote{http://www.jpo-miti.go.jp/saikine/repo242.htm}, l'OEB applique m\^{e}me des standards inf\'{e}rieurs pour \'{e}tablir l'inventivit\'{e} des objets immat\'{e}riels. De plus, pour dans la fa\c{c}on d'examiner la technicit\'{e} des m\'{e}thodes d'entreprises, l'OEB projette m\^{e}me d'abandonner sa requ\^{e}te traditionnelle que ces m\'{e}thodes aient un ``effet technique suppl\'{e}mentaire'' au del\`{a} de l'utilisation normale d'un ordinateur, d\`{e}s que les limites sur la brevetabilit\'{e} auront \'{e}t\'{e} supprim\'{e}es par le l\'{e}gislateur comme le propose l'OEB.

Matthias Schlegel, CEO de Phaidros AG\footnote{http://www.phaidros.com}, un pionnier de la m\'{e}ta-mod\'{e}lisation des processus commerciaux bas\'{e} \`{a} Ilmenau, commente:
{\it \begin{quote}
Avec des dizaines de milliers de brevets sur des logiciels ou des m\'{e}thodes d'entreprise qui circulent, nos clients sont pr\'{e}occup\'{e}s par les risques juridiques et r\'{e}clament des garanties. Notre conseil d'administration envisage la constitution d'une r\'{e}serve de fonds annuelle de millions de DM pour se pr\'{e}parer aux proc\`{e}s et d\'{e}p\^{o}ts de brevets. Mais m\^{e}me si nous acqu\'{e}rions des brevets extensifs sur des logiciels ou des m\'{e}thodes d'entreprises, comme ceux que l'OEB semble dispos\'{e} \`{a} d\'{e}livrer, ce serait seulement d'une utilit\'{e} limit\'{e}e pour la protection de nos copyrights sur les syst\`{e}mes modulaires dans lesquels nous aurions investi contre les attaques par brevets de tierces parties. Cela ajouterait principalement \`{a} la consommation de nos ressources, qui doivent \^{e}tre d\'{e}plac\'{e}es des investissements de R\&D vers les co\^{u}ts juridiques. Pour autant que je puisse en juger, la plupart des PME Europ\'{e}ennes du logiciel partagent notre exp\'{e}rience. Les offices de brevets ne savent pas cela, et ils ne s'en pr\'{e}occupent pas vraiment. Il est donc ainsi extr\^{e}mement important que la comp\'{e}tence de r\'{e}gulation en ce qui concerne ce qui peut \^{e}tre brevet\'{e} reste aux parlements et ne soit pas transf\'{e}r\'{e}e aux offices de brevets.
\end{quote}}

Cosignataire Jens Enders, CEO de MDLink GmbH\footnote{http://www.skyrix.de}, un leader technologique des syst\`{e}mes de e-business bas\'{e}s sur le web de Magdeburg, ajoute:
{\it \begin{quote}
Les brevets logiciels sont une bonne chose pour les compagnies vieillissantes qui veulent cl\^{o}turer leur territoire pour 20 ans de fa\c{c}on \`{a} dormir sur leurs lauriers. Pour la protection de notre avanc\'{e}e comp\'{e}titive, le copyright et le capital humain nous satisfont parfaitement. Les politiciens des brevets qui essayent actuellement de nous vendre une ``protection forte de la Propri\'{e}t\'{e} Intellectuelle'' semblent ignorer simplement les r\`{e}gles de notre march\'{e}.
\end{quote}}

La ``Proposition de Base'' compl\`{e}te le tableau, que le pr\'{e}sident du groupe de travail sur les brevets logiciels de l'Union des Consultants en Brevets Europ\'{e}ens, l'avocat en brevets J�rgen Betten,a pr\'{e}dit dans plusieurs publications au d\'{e}but de cette ann\'{e}e:
{\it \begin{quote}
Par l'utilisation du ... d\'{e}veloppement de la juridiction ... le syst\`{e}me des brevets s'est d\'{e}tach\'{e} de sa restriction traditionnelle \`{a} l'industrie de production et est maintenant \'{e}galement d'une importance essentielle pour les soci\'{e}t\'{e}s de service dans les domaines du commerce, de la banque, des assurances, des t\'{e}l\'{e}communications, etc. Sans la constitution d'un portefeuille de brevets convenable, on est \`{a} craindre que les soci\'{e}t\'{e}s de service allemandes dans ces secteurs ne se trouvent en position d\'{e}savantag\'{e}e vis-\`{a}-vis de leurs comp\'{e}titeurs US.

...

Le droit des Brevets donne au possesseur d'un brevet un droit \`{a} interdire aux autres l'utilisation d'une invention brevet\'{e}e. ... Dans les secteurs complexes de la technologie, dans lesquels l'\'{e}tablissement d'un ``standard'' est souvent un pr\'{e}requis pour un succ\`{e}s commercial, comme le secteur de l'\'{e}lectronique de loisir, des t\'{e}l\'{e}communications ou de l'Internet, les licences crois\'{e}es sont devenues une forme fr\'{e}quente et pratique d'exploitation des brevets: en utilisant leurs propres brevets ``comme monnaie d'\'{e}change'', les soci\'{e}t\'{e}s obtiennent un acc\`{e}s \`{a} des technologies qui ont \'{e}t\'{e} brevet\'{e}es par des soci\'{e}t\'{e}s concurrentes.

...

Puisque la conf\'{e}rence gouvernementale des \'{e}tats membres de l'Organisation Europ\'{e}enne des Brevets a confi\'{e} en juin 1999 \`{a} Paris \`{a} l'OEB un mandat pour proposer avant le 01/01/2001 une version r\'{e}vis\'{e}e de l'article 5.2 de la CEB concernant l'exclusion des programmes d'ordinateur, de fa\c{c}on \`{a} ce que la version modifi\'{e}e puisse entrer en vigueur avant 01/07/2000, ce n'est maintenant probablement qu'une question de temps, avant que les ``programmes d'ordinateur'' ainsi que les autres r\`{e}gles d'exemption soient supprim\'{e}s de l'article 52 de la CEB.
\end{quote}}

Le pr\'{e}sident de la Commission Parlementaire Allemande sur les Nouveaux M\'{e}dias, JT, voit une n\'{e}cessit\'{e} urgente d'action du l\'{e}gislateur:
{\it \begin{quote}
Dans les cercles des experts de la politique technologique, vous pouvez souvent trouver des gens d\'{e}clarant que le syst\`{e}me des brevets doit \^{e}tre \'{e}tendu \`{a} certaines zones de la technologie de l'information, dont les investissements pourraient sinon se r\'{e}v\'{e}ler insuffisamment prot\'{e}g\'{e}s. Cette affirmation a par contre \'{e}t\'{e} jusqu'\`{a} maintenant toujours fourgu\'{e}e comme une v\'{e}rit\'{e} d\'{e}coulant de soi, et personne que je connaisse ne s'est jamais souci\'{e} de lui donner corps \`{a} la lumi\`{e}re des faits de l'\'{e}conomie allemande ou europ\'{e}enne des technologies de l'information.

M\^{e}me si les partisans des brevets r\'{e}ussissaient \`{a} trouver des zones des technologies de l'information, dans lesquelles les brevets ont actuellement ou ont eu des effets positifs, il serait n\'{e}anmoins encore n\'{e}cessaire d'enqu\^{e}ter, pour \'{e}tablir si ces effets ne sont pas contrebalanc\'{e}s par des effets de bord contraires potentiels du syst\`{e}me des brevets.

Mais tandis que les autorit\'{e}s l\'{e}gislatives sont compl\`{e}tement incomp\'{e}tentes sur ce sujet, les autorit\'{e}s judiciaires agissent d\'{e}j\`{a}, en accordant des milliers de brevets logiciels et en faisant pression pour une modification des r\`{e}gles juridiques. Nous les l\'{e}gislateurs devons ainsi nous saisir maintenant de ces questions avec la plus grande priorit\'{e}.
\end{quote}}

La lettre de la FFII est destin\'{e}e \`{a} clarifier ces questions pour le l\'{e}gislateur. Elle sugg\`{e}re des moyens pour rendre la proposition de base de l'OEB plus sp\'{e}cifique, de fa\c{c}on \`{a} placer l'OEB sous un contr\^{o}le effectif du l\'{e}gislateur. Il s'av\`{e}re ainsi qu'il n'existe actuellement aucune raison valide pour la modification de la loi (EPC art 52), tandis qu'il existe de bonnes raisons l\'{e}gales pour l'introduction de d\'{e}finitions pr\'{e}cises et restrictives pour des termes pliables comme ``technicit\'{e}'' et ``applicabilit\'{e} industrielle''.

La lettre ouverte cite en outre des \'{e}tudes \'{e}conomiques et la EP, qui a entre temps \'{e}t\'{e} sign\'{e}e par environ 37000 citoyens, incluant 400 directeurs de soci\'{e}t\'{e}s de technologies de l'information, et les d\'{e}clarations de soutien d'\`{a} peu pr\`{e}s 300 politiciens europ\'{e}ens.

Mais m\^{e}me une protestation publique de cette ampleur n'a pas pour l'instant d\'{e}clench\'{e} plus qu'un ``vaste silence'' de la part des preneurs de d\'{e}cision du syst\`{e}me des brevets. D\'{e}j\`{a} en juin 1999 les deux responsables du BMJ en charge des n\'{e}gociations pour la d\'{e}l\'{e}gation allemande \`{a} la conf\'{e}rence intergouvernementale de Paris n'ont pas r\'{e}agi \`{a} un appel bas\'{e} sur 5000 signatures, et ont donn\'{e} \`{a} l'OEB le mandat de modification de la Convention Europ\'{e}enne des Brevets. Un peu plus tard, les deux responsables du BMJ ont \'{e}t\'{e} promus \`{a} de nouvelles fonctions \`{a} Munich. L'un est devenu un juge principal \`{a} l'OEB, l'autre le pr\'{e}sident de l'Office Allemand des Brevets. Et le pr\'{e}sident actuel de l'Office Europ\'{e}en des Brevets et initiateur de la ``Proposition de Base'', le Dr. Ingo Kober, a aussi commenc\'{e} sa carri\`{e}re au BMJ.

L'OEB se finance lui-m\^{e}me \`{a} partir de frais pour les brevets qu'il accorde.

\begin{sect}{ref}{R\'{e}f\'{e}rences}
\begin{itemize}
\item
L'Office Europ\'{e}en des Brevets propose une brevetabilit\'{e} illimit\'{e}e\footnote{} - 

\item
Brevets Logiciels\footnote{http://swpat.ffii.org/index.fr.html} - http://swpat.ffii.org/index.fr.html

\item
FFII\footnote{http://www.ffii.org/index.fr.html} - http://www.ffii.org/index.fr.html
\end{itemize}
\end{sect}

\begin{sect}{prini}{A propos de la FFII - www.ffii.org\footnote{http://www.ffii.org}}
La FFII est une association \`{a} but non-lucratif qui fait la promotion du d\'{e}veloppement d'interfaces ouvertes, de logiciels open source et d'une information publique accessible au public. La FFII coordonne un groupe de travail sur les brevets logiciels qui est soutenu par des \'{e}diteurs de logiciels allemands reconnus. La FFII est membre de l'Alliance EuroLinux.
\end{sect}

\begin{sect}{press}{Contacts Presse}
\begin{description}
\item[courriel:]\ info@ffii.org
\item[t\'{e}l:]\ Hartmut Pilch +49-89-18979927
\end{description}
\end{sect}

\begin{sect}{url}{Adresse permanente de cette Annonce de Presse}
http://swpat.ffii.org/neuf/epue28
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatnews.el ;
% mode: latex ;
% End: ;

