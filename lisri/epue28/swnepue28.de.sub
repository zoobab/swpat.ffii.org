\begin{subdocument}{swnepue28}{Eur. Patentamt begehrt unbegrenzte Patentierbarkeit}{http://swpat.ffii.org/chronik/epue28/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{IT-Fachleute fordern wirksame Kontrolle des EPA}
Das Europ\"{a}ische Patentamt (EPA) will die europ\"{a}ischen Regierungen dazu bewegen, im November 2000 s\"{a}mtliche gesetzlichen Einschr\"{a}nkungen der Patentierbarkeit zu beseitigen.  Das Bundesministerium der Justiz (BMJ) gab einen entsprechenden auf den 27. Juni 2000 datierten ``Basisvorschlag f\"{u}r die Revision des Europ\"{a}ischen Patent\"{u}bereinkommens'' den ``am Patentwesen interessierten Kreisen'' Anfang August bekannt.

\begin{center}
Zur sofortigen Freigabe
\end{center}

M\"{u}nchen, Berlin, Frankfurt, Ilmenau, Magdeburg

Der F\"{o}rderverein f\"{u}r eine Freie Informationelle Infrastruktur e.V.\footnote{http://www.ffii.org/index.de.html} (FFII) zeigt zusammen mit einigen IT-Firmen in einem offenen Brief an das Bundesjustizministerium die Unzul\"{a}nglichkeiten des EPA-Entwurfs auf und warnt vor verheerenden Auswirkungen der expansiven Patentpolitik des EPA auf Innovation, Wettbewerb, Wohlstand, Bildung und B\"{u}rgerrechte.

Ralf Schwöbel, Vorstand der Frankfurter Intradat AG\footnote{http://www.intradat.de}, eines Marktf\"{u}hrers in Internet-Verkaufssystemen, erkl\"{a}rt seine Unterst\"{u}tzung f\"{u}r den offenen Brief:
{\it \begin{quote}
Mit diesem Vorschlag schafft das EPA die rechtlichen Rahmenbedingungen, um ebenso viele Patente auf Gesch\"{a}ftsverfahren zu gew\"{a}hren wie die USA.  Als Hersteller von E-Commerce-Systeml\"{o}sungen halten wir das f\"{u}r sch\"{a}dlich, denn es wird viele kleine und mittlere Unternehmen davon abschrecken, ihre eigenen E-Commerce-L\"{o}sungen zu entwickeln.
\end{quote}}

Neuere Studien n\"{a}hren diese Bef\"{u}rchtungen.  Das Europ\"{a}iche Patentamt geht bei der Erteilung von Patenten auf immaterielle Gegenst\"{a}nde (Software, Gesch\"{a}ftsmethoden) bereits heute schon weiter als die Kollegen in den USA und Japan.  Einem japanischen Vergleichsbericht\footnote{http://www.jpo.go.jp/old/saikine/repo242.htm} zufolge legt das EPA bei der Bewertung der Erfindungsh\"{o}he immaterieller Verfahren noch niedrigere Ma{\ss}st\"{a}be an als die anderen beiden Patent\"{a}mter.  Bei der Beurteilung der Technizit\"{a}t von Gesch\"{a}ftsmethoden plant\footnote{http://swpat.ffii.org/papiere/epo-tws-app6/index.en.html} das EPA \"{u}berdies, seine bisherige Forderung nach einem \"{u}ber die normale Nutzung des Rechners hinausgehenden ``zus\"{a}tzlichen technischen Effekt'' fallen zu lassen, sobald die vom EPA vorgeschlagene Vertrags\"{a}nderung in Kraft getreten ist.

Matthias Schlegel, Vorstand der Ilmenauer Phaidros AG\footnote{http://www.phaidros.com}, eines Pioniers in der Metamodellierung von Gesch\"{a}ftsprozessen, kommentiert:
{\it \begin{quote}
Angesichts eines Minenfeldes aus Zehntausenden von Software- und Gesch\"{a}ftsmethodenpatenten verlangen unsere Kunden von uns Garantien gegen die rechtlichen Risiken.  Unser Vorstand denkt an die Bildung von j\"{a}hrlichen Patentr\"{u}ckstellungen in Millionenh\"{o}he f\"{u}r Prozesskosten und Patentanmeldungen.  Aber auch dann, wenn wir selbst weitgehende Patentrechte auf Programmiertechniken und Algorithmen erwerben, wie das EPA sie nur allzu bereitwillig zu vergeben scheint, w\"{u}rde uns das nur begrenzt dabei helfen, unsere Urheberrechte an den modularen Systemen, in die wir investiert haben, vor Patenten Dritter zu sch\"{u}tzen.  Die Patentierung w\"{u}rde vielmehr noch weitere Ressourcen binden und von den F\&E-Investitionen hin zu den Rechtskosten transferieren. Soweit ich sehen kann, geht es den meisten kleinen und mittleren Softwareunternehmen Europas \"{a}hnlich.  Den Patent\"{a}mtern bleibt diese Sachlage verborgen, und sie h\"{a}tten f\"{u}r unsere Belange wohl auch kein offenes Ohr.  Deshalb ist es wichtig, dass die Regelungskompetenz in Sachen Patentierbarkeit bei den Parlamenten bleibt und nicht zu den Patent\"{a}mtern transferiert wird.
\end{quote}}

Mitunterzeichner Jens Enders, Gesch\"{a}ftsf\"{u}hrer der Magdeburger MDLink GmbH\footnote{http://www.skyrix.de}, eines Technologief\"{u}hrers f\"{u}r webbasierte E-Business-Systeme, f\"{u}gt hinzu:
{\it \begin{quote}
Softwarepatente sind gut f\"{u}r alternde Firmen, die ihr Territorium f\"{u}r 20 Jahre abschotten wollen, um sich auf ihren Lorbeeren auszuruhen.  Zum Schutz unseres Vorsprungs tun Urheberrecht und Humankapital uns gute Dienste.  Patentpolitiker, die uns dar\"{u}ber hinaus ``starke Eigentumsrechte'' einreden wollen, verkennen schlichtweg die Spielregeln unserer Branche.
\end{quote}}

Mit dem ``Basisvorschlag'' wird das vollzogen, was Patentanwalt J\"{u}rgen Betten, Vorsitzender des Software-Arbeitskreises der Europ\"{a}ischen Union der Patentberater, schon seit Anfang 2000 in diversen Publikationen ank\"{u}ndigt:
{\it \begin{quote}
Durch die ... Entwicklung der Rechtsprechung ... hat sich das Patentrecht von der traditionellen Beschr\"{a}nkung auf die verarbeitende Industrie gel\"{o}st und ist heute auch f\"{u}r Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu bef\"{u}rchten, dass die deutschen Dienstleistungsunternehmen in diesen Sektoren insbesondere gegen\"{u}ber der US-amerikanischen Konkurrenz ins Hintertreffen geraten.

...

Das Patentgesetz gibt dem Patentinhaber das exklusive Recht an der Benutzung der patentierten Erfindung. ... In komplexen Technologiefeldern, bei denen die Durchsetzung eines ``Standards'' h\"{a}ufig Voraussetzung f\"{u}r einen Markterfolg beim Konsumenten ist, wie beispielsweise in der Unterhaltungselektronik, der Telekommunikation oder dem Internet, sind Kreuzlizenzen eine h\"{a}ufige und praktikable Form der Patentverwertung geworden: Mit eigenen Patenten ``als W\"{a}hrung'' erwirbt man Zugang zu Technologien, die von Mitbewerbern patentgesch\"{u}tzt sind.

...

Nachdem nun auch die Regierungskonferenz der Mitgliedstaaten der Europ\"{a}ischen Patentorganisation im Juni 1999 in Paris dem EPA das Mandat erteilt hat, vor dem 1.1.2001 eine revidierte Fassung von Art. 52 Abs. 2 EP\"{U} bez\"{u}glich des Ausschlusses von Computerprogrammen vorzulegen, so dass die ge\"{a}nderte Fassung vor dem 1.7.2000 in Kraft tritt, ist es wohl nur eine Frage der Zeit, bis die Computerprogramme (und auch die anderen Ausschlussregelungen) aus Art. 52 EP\"{U} gestrichen sind.
\end{quote}}

Dem Vorsitzenden des Bundestags-Unterausschusses Neue Medien, J\"{o}rg Tauss (SPD), geht diese Entwicklung zu weit, und er sieht einen dringenden Handlungsbedarf f\"{u}r den Gesetzgeber:
{\it \begin{quote}
In technologiepolitischen Fachkreisen h\"{o}rt man immer wieder die Behauptung, das Patentsystem m\"{u}sse auf gewisse Bereiche der Informationstechnik ausgeweitet werden, weil sonst deren Investitionen nicht gen\"{u}gend gesch\"{u}tzt w\"{u}rden.  Diese Behauptung wurde bisher allerdings immer nur als abstrakte Grundwahrheit weitergegeben und niemals anhand von Tatsachen der deutschen oder europ\"{a}ischen IT-Wirtschaft belegt.

Selbst wenn es gel\"{a}nge, Bereiche der Informationstechnik zu finden, in denen Patente nachweislich vorteilhaft wirken oder gewirkt haben, m\"{u}sste man noch immer untersuchen, ob eventuelle sch\"{a}dliche Nebenwirkungen der Patentierung diese Vorteile nicht \"{u}berwiegen.

Aber w\"{a}hrend bei der Legislative noch vollkommene Unklarheit herrscht, schreitet die Judikative bereits zur Tat, gew\"{a}hrt Tausende von Softwarepatenten und dr\"{a}ngt auf \"{A}nderung der Gesetzesregeln.  Es ist daher h\"{o}chste Zeit f\"{u}r uns als Gesetzgeber, uns um diese Fragen zu k\"{u}mmern.
\end{quote}}

Hier setzt der Offene Brief an. Er weist Wege, wie man den EPA-Basisvorschlag pr\"{a}zisieren k\"{o}nnte, um der bef\"{u}rchteten inflation\"{a}ren Ausweitung des Patentwesens einen Riegel vorzuschieben und das Patentwesen einer wirksamen Kontrolle durch den Gesetzgeber zu unterwerfen.  Dabei zeigt sich, dass derzeit kein g\"{u}ltiger Grund f\"{u}r eine \"{A}nderung des Gesetzestextes (Art 52 EP\"{U}) besteht, wohl aber f\"{u}r eine pr\"{a}zise und restriktive Handhabung einiger dehnbarer Rechtsbegriffe wie ``Technizit\"{a}t'' und ``gewerbliche/industrielle Anwendbarkeit''.

Der offene Brief verweist \"{u}berdies auf \"{o}konomische Studien, auf die Eurolinux-Petition f\"{u}r ein softwarepatentfreies Europa (http://petition.eurolinux.org/index.de.html), die inzwischen von etwa 30000 B\"{u}rgern, darunter ca 400 leitenden Angestellten von IT-Unternehmen, getragen wird, sowie auf unterst\"{u}tzende Aussagen von fast 300 europ\"{a}ischen Politikern.

Doch selbst \"{o}ffentliche Proteste dieser Gr\"{o}{\ss}enordnung haben in den Kreisen der Patentjustiz bisher lediglich ein ``Schweigen im Walde'' ausgel\"{o}st.  Schon im Juni 1999 setzten die beiden BMJ-Verhandlungsf\"{u}hrer bei der Regierungskonferenz in Paris sich \"{u}ber 5000 Protestunterschriften hinweg, als sie dem EPA das Mandat zur \"{A}nderung des Europ\"{a}ischen Patent\"{u}bereinkommens erteilten.  Kurz danach gelang beiden BMJ-Patentreferenten ein beruflicher Aufstieg in M\"{u}nchen.  Einer wurde ein f\"{u}hrender Richter am EPA, der andere Pr\"{a}sident des Deutschen Patent- und Markenamtes.  Auch der heutige Pr\"{a}sident des EPA und Initiator des Basisvorschlages, Dr. Ingo Kober, begann seine Karriere im BMJ.

Das EPA finanziert sich durch Patentgeb\"{u}hren.

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Eurolinux Petition f\"{u}r ein softwarepatentfreies Europa\footnote{http://petition.eurolinux.org/index.de.html}}}
\filbreak

\item
{\bf {\bf FFII: Logikpatente in Europa\footnote{http://swpat.ffii.org/index.de.html}}}

\begin{quote}
In den letzten Jahren hat das Europ\"{a}ische Patentamt (EPA) gegen den Buchstaben und Geist der geltenden Gesetze \"{u}ber 30.000 Patente auf computer-eingekleidete Organisations- und Rechenregeln (laut Gesetz \emph{Programme f\"{u}r Datenverarbeitungsanlagen}, laut EPA-Neusprech von 2000 ``computer-implementierte Erfindungen'') erteilt.  Nun m\"{o}chte Europas Patentbewegung diese Praxis durch ein neues Gesetz festschreiben.  Europas Programmierer und B\"{u}rger sehen sich betr\"{a}chtlichen Risiken ausgesetzt.  Hier finden Sie die grundlegende Dokumentation zur aktuellen Debatte, ausgehend von einer Kurzeinf\"{u}hrung und \"{U}bersicht \"{u}ber die neuesten Entwicklungen.
\end{quote}
\filbreak

\item
{\bf {\bf F\"{o}rderverein f\"{u}r eine Freie Informationelle Infrastruktur e.V.\footnote{http://www.ffii.org/index.de.html}}}

\begin{quote}
Der FFII ist ein in M\"{u}nchen eingetragener gemeinn\"{u}tziger Verein f\"{u}r Volksbildung im Bereich der Datenverarbeitung.  Der FFII unterst\"{u}tzt die Entwicklung \"{o}ffentlicher Informationsg\"{u}ter auf grundlage des Urheberrechts, freien Wettbewerbs und offener Standards.  \"{U}ber 300 Mitglieder, 700 Firmen und 50.000 Unterst\"{u}tzer haben den FFII mit der Vertretung ihre Interessen im Bereich der Gesetzgebung zu Software-Eigentumsrechten beauftragt.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{prini}{\"{U}ber den FFII - www.ffii.org\footnote{http://www.ffii.org}}
Der F\"{o}rderverein f\"{u}r eine Freie Informationelle Infrastruktur e.V.\footnote{http://www.ffii.org/index.de.html} (FFII e.V.) ist ein gemeinn\"{u}tziger Verein, der die Entwicklung offener Schnittstellen, quelloffener Programme und frei verf\"{u}gbarer \"{o}ffentlicher Informationen f\"{o}rdert und sich f\"{u}r ein kraftvolles Zusammenwirken freier und propriet\"{a}rer Software zum Zwecke eines langfristigen Aufbaus informationeller Gemeing\"{u}ter auf der Grundlage offener Standards, fairen Wettbewerbs und der Achtung legitimer Urheberrechte einsetzt.  Der FFII koordiniert eine Arbeitsgruppe zum Schutz der digitalen Innovation vor Softwarepatenten, die von erfolgreichen deutschen Softwarefirmen unterst\"{u}tzt wird.  Der FFII ist Gr\"{u}ndungsmitglied der EuroLinux-Allianz f\"{u}r eine Freie Informationelle Infrastruktur.
\end{sect}

\begin{sect}{press}{Kontakt}
\begin{description}
\item[E-Post:]\ info at ffii org
\item[Telefon:]\ Hartmut Pilch +49-89-18979927
\end{description}
\end{sect}

\begin{sect}{url}{Permanente Netzadresse dieser Mitteilung}
http://www.ffii.org/verein/leute
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatlisri.el ;
% mode: latex ;
% End: ;

