<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#cas: La %(fi:FFII) est une association à but non-lucratif qui fait la promotion du développement d'interfaces ouvertes, de logiciels open source et d'une information publique accessible au public. La FFII coordonne un %(sp:groupe de travail sur les brevets logiciels) qui est %(sp:soutenu par des éditeurs de logiciels allemands reconnus). La FFII est membre de l'%(el:Alliance EuroLinux).

#nWW: A propos de la FFII

#Dsn: L'OEB se finance lui-même à partir de frais pour les brevets qu'il accorde.

#DWw: Mais même une protestation publique de cette ampleur n'a pas pour l'instant déclenché plus qu'un %(q:vaste silence) de la part des preneurs de décision du système des brevets. Déjà en juin 1999 les deux responsables du BMJ en charge des négociations pour la délégation allemande à la conférence intergouvernementale de Paris n'ont pas réagi à un appel basé sur %(sm:5000 signatures), et ont donné à l'OEB le mandat de modification de la Convention Européenne des Brevets. Un peu plus tard, les deux responsables du BMJ ont été promus à de nouvelles fonctions à Munich. L'un est devenu un juge principal à l'OEB, l'autre le président de l'Office Allemand des Brevets. Et le président actuel de l'Office Européen des Brevets et initiateur de la %(q:Proposition de Base), le Dr. Ingo Kober, a aussi commencé sa carrière au BMJ.

#Dyo: La lettre ouverte cite en outre des études économiques et la %{EP}, qui a entre temps été signée par environ 37000 citoyens, incluant 400 directeurs de sociétés de technologies de l'information, et les déclarations de soutien d'à peu près 300 politiciens européens.

#Dwt: La lettre de la FFII est destinée à clarifier ces questions pour le législateur. Elle suggère des moyens pour rendre la proposition de base de l'OEB plus spécifique, de façon à placer l'OEB sous un contrôle effectif du législateur. Il s'avère ainsi qu'il n'existe actuellement aucune raison valide pour la modification de la loi (EPC art 52), tandis qu'il existe de bonnes raisons légales pour l'introduction de définitions précises et restrictives pour des termes pliables comme %(q:technicité) et %(q:applicabilité industrielle).

#Aiz: Mais tandis que les autorités législatives sont complètement incompétentes sur ce sujet, les autorités judiciaires agissent déjà, en accordant des milliers de brevets logiciels et en faisant pression pour une modification des règles juridiques. Nous les législateurs devons ainsi nous saisir maintenant de ces questions avec la plus grande priorité.

#ScW: Même si les partisans des brevets réussissaient à trouver des zones des technologies de l'information, dans lesquelles les brevets ont actuellement ou ont eu des effets positifs, il serait néanmoins encore nécessaire d'enquêter, pour établir si ces effets ne sont pas contrebalancés par des effets de bord contraires potentiels du système des brevets.

#Itm: Dans les cercles des experts de la politique technologique, vous pouvez souvent trouver des gens déclarant que le système des brevets doit être étendu à certaines zones de la technologie de l'information, dont les investissements pourraient sinon se révéler insuffisamment protégés. Cette affirmation a par contre été jusqu'à maintenant toujours fourguée comme une vérité découlant de soi, et personne que je connaisse ne s'est jamais soucié de lui donner corps à la lumière des faits de l'économie allemande ou européenne des technologies de l'information.

#DnW: Le président de la Commission Parlementaire Allemande sur les Nouveaux Médias, %{JT}, voit une nécessité urgente d'action du législateur:

#N1l: Puisque la conférence gouvernementale des états membres de l'Organisation Européenne des Brevets a confié en juin 1999 à Paris à l'OEB un mandat pour proposer avant le 01/01/2001 une version révisée de l'article 5.2 de la CEB concernant l'exclusion des programmes d'ordinateur, de façon à ce que la version modifiée puisse entrer en vigueur avant 01/07/2000, ce n'est maintenant probablement qu'une question de temps, avant que les %(q:programmes d'ordinateur) ainsi que les autres règles d'exemption soient supprimés de l'article 52 de la CEB.

#DAi: Le droit des Brevets donne au possesseur d'un brevet un droit à interdire aux autres l'utilisation d'une invention brevetée. ... Dans les secteurs complexes de la technologie, dans lesquels l'établissement d'un %(q:standard) est souvent un prérequis pour un succès commercial, comme le secteur de l'électronique de loisir, des télécommunications ou de l'Internet, les licences croisées sont devenues une forme fréquente et pratique d'exploitation des brevets: en utilisant leurs propres brevets %(q:comme monnaie d'échange), les sociétés obtiennent un accès à des technologies qui ont été brevetées par des sociétés concurrentes.

#Den: Par l'utilisation du ... développement de la juridiction ... le système des brevets s'est détaché de sa restriction traditionnelle à l'industrie de production et est maintenant également d'une importance essentielle pour les sociétés de service dans les domaines du commerce, de la banque, des assurances, des télécommunications, etc. Sans la constitution d'un portefeuille de brevets convenable, on est à craindre que les sociétés de service allemandes dans ces secteurs ne se trouvent en position désavantagée vis-à-vis de leurs compétiteurs US.

#HWW: La %(q:Proposition de Base) complète le tableau, que le président du groupe de travail sur les brevets logiciels de l'Union des Consultants en Brevets Européens, l'avocat en brevets %(JB),a prédit dans plusieurs publications au début de cette année:

#Sei: Les brevets logiciels sont une bonne chose pour les compagnies vieillissantes qui veulent clôturer leur territoire pour 20 ans de façon à dormir sur leurs lauriers. Pour la protection de notre avancée compétitive, le copyright et le capital humain nous satisfont parfaitement. Les politiciens des brevets qui essayent actuellement de nous vendre une %(q:protection forte de la Propriété Intellectuelle) semblent ignorer simplement les règles de notre marché.

#AhW: Cosignataire %(JE), CEO de %(MG), un leader technologique des systèmes de e-business basés sur le web de Magdeburg, ajoute:

#Amo: Avec des dizaines de milliers de brevets sur des logiciels ou des méthodes d'entreprise qui circulent, nos clients sont préoccupés par les risques juridiques et réclament des garanties. Notre conseil d'administration envisage la constitution d'une réserve de fonds annuelle de millions de DM pour se préparer aux procès et dépôts de brevets. Mais même si nous acquérions des brevets extensifs sur des logiciels ou des méthodes d'entreprises, comme ceux que l'OEB semble disposé à délivrer, ce serait seulement d'une utilité limitée pour la protection de nos copyrights sur les systèmes modulaires dans lesquels nous aurions investi contre les attaques par brevets de tierces parties. Cela ajouterait principalement à la consommation de nos ressources, qui doivent être déplacées des investissements de R&D vers les coûts juridiques. Pour autant que je puisse en juger, la plupart des PME Européennes du logiciel partagent notre expérience. Les offices de brevets ne savent pas cela, et ils ne s'en préoccupent pas vraiment. Il est donc ainsi extrêmement important que la compétence de régulation en ce qui concerne ce qui peut être breveté reste aux parlements et ne soit pas transférée aux offices de brevets.

#Mnn: %(MS), CEO de %(PAG), un pionnier de la méta-modélisation des processus commerciaux basé à Ilmenau, commente:

#Drc: De récentes études montrent que les examinateurs de l'Office Européen des Brevets tendent même à accorder des brevets sur des objets immatériels (tels que les logiciels et les méthodes d'entreprises) de façon encore plus généreuse que leurs collègues de l'USPTO et du JPO. D'après une %(sk:étude comparative Japonaise), l'OEB applique même des standards inférieurs pour établir l'inventivité des objets immatériels. De plus, pour dans la façon d'examiner la technicité des méthodes d'entreprises, l'OEB %(a6:projette) même d'abandonner sa requête traditionnelle que ces méthodes aient un %(q:effet technique supplémentaire) au delà de l'utilisation normale d'un ordinateur, dès que les limites sur la brevetabilité auront été supprimées par le législateur comme le propose l'OEB.

#MfW: Avec cette proposition, l'OEB crée un cadre légal pour breveter autant de méthodes d'entreprises qu'aux USA. En tant qu'éditeur de solutions d'e-commerce, nous pensons que cela est néfaste, puisque cela dissuadera de nombreuses petites et moyennes entreprises de développer leurs propres solutions de e-commerce.

#Ree: %{RS}, CEO de %{IAG}, un leader sur le marché des systèmes de boutiques en ligne basé à Francfort, explique son soutien à cette lettre ouverte:

#Drh: La %{FFII} a envoyé une %(sx:lettre ouverte) au BMJ, dans laquelle elle a mis en évidence des incohérences dans la proposition de l'OEB et a averti contre les effets dévastateurs de la politique expansive de l'OEB sur l'innovation, la compétition, la prospérité, l'éducation et les libertés publiques.

#DWE: L'Office Européen des Brevets (OEB) veut inciter les gouvernements Européens à supprimer toute restriction légale sur la brevetabilité en novembre 2000. Le Ministère Fédéral Allemand de la Justice (BMJ) a distribué une %(q:Proposition de Base pour la Révision de la Convention Européenne des Brevets) datée du 27-06-2000 aux %(q:cercles intéressés dans le système des brevets) au début du mois d'août.

#descr: Des professionnels de l'informatique demandent un contrôle effectif de l'OEB

#title: L'Office Européen des Brevets propose une brevetabilité illimitée

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: oberger ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnepue28 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

