
FFII-Pressemitteilung [ Europa / Wirtschaft / IuK ]
=================================================================
Parlament sagt Nein zu Softwarepatenten
=================================================================

Stra�burg, 6. Juli 2005 -- Das Europ�ische Parlament entschied
heute mit gro�er Mehrheit, die Richtlinie "zur Patentierbarkeit
computerimplementierter Erfindungen", auch bekannt als die
Softwarepatentrichtlinie, abzulehnen.  Diese Ablehnung war
die schl�ssige Antwort auf die Weigerung der Kommission vom
Februar, den Gesetzgebungsprozess neu zu starten, und auf den
Unwillen des Ministerrates, die Beschl�sse des Europaparlaments
und der nationalen Parlamente zu ber�cksichtigen.  Der FFII
begl�ckw�nscht das Europ�ische Parlament f�r sein klares
"Nein" zu schlechten Gesetzesvorlagen und Verfahrensweisen.

Dies ist ein gro�er Sieg f�r jene, die sich daf�r eingesetzt
haben, sicher zu stellen, dass Europ�ische Innovation
und Wettbewerbsf�higkeit vor der Monopolisierung von
Software-Funktionalit�ten und Gesch�ftsmethoden gesch�tzt
bleibt.  Damit wird dem Versuch der Europ�ischen Kommission
und ministerieller Patentb�rokraten, den Mitgliedstaaten
sch�dliche und gesetzlich fragw�rdige Praktiken des
Europ�ischen Patentamtes (EPA) aufzuzwingen, ein Ende gesetzt.
Allerdings bleiben die Probleme, die von jenen Praktiken
her r�hren, bestehen.  Der FFII ist der Auffassung, dass die
Arbeit des Parlaments, insbesondere das partei�bergreifende
Kompromisspaket aus 21 �nderungsvorschl�gen, eine gute
Grundlage bietet, auf welcher zuk�nftige L�sungen, sowohl auf
nationaler als auch auf europ�ischer Ebene aufbauen k�nnen.

Jonas Maebe, FFII-Vorstandsmitglied, kommentiert das Ergebnis
der heutigen Abstimmung:

  "Dieses Ergebnis zeigt deutlich, dass gr�ndliche Analyse,
  aufrichtig besorgte B�rger und faktische Informationen
  gr��ere Wirkung haben als freie Eiskrem, Schiffsladungen
  gemieteter Lobbyisten und Auslagerungsdrohungen.
  Ich hoffe, dass diese Wende des Geschehens einigen
  Menschen wieder Vertrauen in die europ�ischen Prozesse
  der Entscheidungsfindung geben kann.  Ich hoffe auch,
  dass sie den Rat und die Kommission dazu ermutigen wird,
  es dem Parlament nach zu tun, um die Transparenz und die
  M�glichkeiten der Betroffenen, am Entscheidungsprozess
  unabh�ngig von ihrer Gr��e teilzunehmen, zu verbessern."

Hartmut Pilch, Pr�sident des FFII, erkl�rt, warum der FFII
die Ablehnungsantr�ge in seinen Abstimmungsempfehlungen
unterst�tzt hat:

  "In den letzten Tagen schlossen die Besitzer zahlreicher vom
  EPA erteilter Softwarepatente und deren Europaabgeordnete,
  die zuvor f�r die Ratsposition geworben hatten, sich den
  Vorschl�gen zur Ablehnung der Richtlinie an, weil klar wurde,
  dass die 21 partei�bergreifenden �nderungsvorschl�ge
  der Abgeordneten Rhoitov�, Buzek, Rocard und Duff gute
  Aussichten auf Annahme durch das Parlament hatten.
  Es wurde immer deutlicher, dass Unterst�tzung f�r
  dieses Kompromisspaket oder f�r gro�e Teile davon zur
  Mehrheitsmeinung in allen Fraktionen wurde.  Dennoch h�tte
  eine entsprechende Abstimmung nicht viel ausgerichtet.
  Wir stimmen der Einsch�tzung der Lage zu, wie sie in der
  gestrigen Vollversammlung von MdEP Karas und heute vom
  Berichterstatter Michel Rocard gegeben wurde: Ein "Nein"
  war die einzig schl�ssige Antwort auf die unkonstruktive
  Haltung und die gesetzlich fragw�rdigen Man�ver der
  Kommission und des Rates, durch welche dessen sogenannter
  `Gemeinsamer Standpunkt' �berhaupt zustande gekommen war."

  Der FFII m�chte sich auch bei all den Menschen bedanken, die
  sich die Zeit genommen haben, ihren Vertreter mittels E-Mail,
  Telefon oder pers�nlich anzusprechen.  Ebenso m�chten wir uns
  bei den zahlreichen Freiwilligen bedanken, die so gro�z�gig
  ihre Zeit und Arbeit investiert haben.  Dies ist genauso ihr
  Sieg wie der des Parlaments."

======================================================================
Hintergrundinformationen und weitere Nachrichten
======================================================================

* 21 partei�bergreifende Kompromissvorschl�ge:
  http://swpat.ffii.org/papers/europarl0309/amends05/komprom0506.en.pdf

* FFII-Abstimmungsempfehlungen f�r die heutige Plenarabstimmung:
  http://swpat.ffii.org/papers/europarl0309/amends05/ffiivotlst050706.pdf

* Praktiken des Europ�ischen Patentamts:
  http://webshop.ffii.org/
  http://swpat.ffii.org/patents/
  http://gauss.ffii.org/

* Karas' Rede im Plenum gestern:
  http://wiki.ffii.org/Karas05075En

* 23 unbeantwortete Fragen zu dem Schaust�ck der Verabschiedung
  einer "Gemeinsamen Position" des Rates:
  http://wiki.ffii.org/LtrFfiiCons050308En  

* Prominenter Artikel des Wallstreet Journals �ber Lehnes
  Interessenkonflikt:
  http://wiki.ffii.org/WsjLehne050705En	

* Bleiben Sie informiert mittels unseres Newstickers:
  http://wiki.ffii.org/SwpatcninoEn

======================================================================
Kontaktinformationen
======================================================================

Hartmut Pilch and Holger Blasum 
M�nchner B�ro
info at ffii org
tel. +49-89-18979927

Erik Josefsson 
Br�sseler B�ro
erjos at ffii org
tel. +32-484-082063

Jonas Maebe
FFII Belgien
jmaebe at ffii org
tel. +32-485-369645

Rufus Pollock
FFII UK
rufus pollock at ffii org uk
+44-1223-690423

G�rald S�drati-Dinet
FFII Frankreich, Vizepr�sident des FFII
gibus at ffii fr
+33-6-60-56-36-45

_______________________________________________
Nachrichtenverteiler neues 
(un)subscribe via http://petition.ffii.org/
neues@ffii.org
http://lists.ffii.org/mailman/listinfo/neues

