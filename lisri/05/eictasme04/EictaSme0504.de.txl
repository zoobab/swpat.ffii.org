<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Mega corporations sending SMEs to European Parliament

#descr: EICTA is sending 50 %(q:hightech SMEs) to visit the European
Parliament and complain about loss of patent protections.  Most of
these SMEs are losers of the software patent game.  As far as they own
any patents at all, these are either business method patents, which
their own %(q:Manifesto) says shouldn't be grantable, or patents on
solutions involving forces of nature, which are grantable under the
Parliament's position of 2003 anyway.  At least two of of the SME
bosses are high-ranking Microsoft managers.  Others are business
partners of companies like IBM.  Yet EICTA's %(q:SME lobbying) efforts
appear to have swayed several formerly critical JURI MEPs into
publicly stating that %(q:the industry), regardless of company size,
wants software patents.

#aWW: Microsoft and IBM masquerading as European SMEs

#Wed: Apparently unable to find enough independent small and medium-sized
enterprises SMEs to lobby for software patents, EICTA has resorted to
sending companies to the European Parliament in which its
multinational members hold a strong position. Additionally, several
SMEs who were inspired by this action and signed EICTA's appeal turn
out not to own any software patents at all.

#Wfw: %(q:The patent system in Europe has worked well for us in the past),
says Jan Ishøj Nielsen, CEO of PremiTech A/S. However, he forgets to
mention that his company's first European software patent is still
waiting to be granted (its US cousin is already valid) and that the
Chairman of the Board of his company is Klaus Holse Andersen, VP
Microsoft Business Solutions EMEA. Mr. Andersen was previously also
CEO of the investment company IVS, another signatory of EICTA's
demands.

#iWd: Apart from sending SMEs to Brussels, EICTA also organised a letter
writing campaign to the European Parliament. While for most companies
the CEO has signed this letter, in case of Giritech the CTO observes
the honours. One reason for this might be that the CEO of Giritech,
René Stockne, has a second job as global manager for Microsoft
Navision. This is the same Microsoft Navision that threatened the
Danish government with moving out of Denmark if the software patents
do not come through.

#WBW: Jonas Maebe, FFII Board Member, comments:

#giq: It is really sad how low EICTA is sinking here. Unable to factually
counter the resistance to software patents from real SME associations
such as UEAPME and CEAPME, they are now resorting to dressing up
Microsoft as an SME. %(q:Oh, grandmother, what big teeth you have got)
indeed.

#ntw: Some other companies that signed the letter and which are not tied to
any big firms, have simply been improperly informed about the reach of
the European Parliament's amendments. For example, Te Strake, an
innovative Dutch SME active in the field of %(q:mechatronics), owns
several patents on inventions related to automated weaving. According
to an analysis performed by the FFII, all of their patents would
remain enforceable under the directive as approved by European
Parliament in first reading.

#tds: Hartmut Pilch (president of FFII) summarises

#etd: Preliminary research shows that most of these companies own less than
a handful of patents and that the claimed solutions either involve
forces of nature and are largely unaffected by the Parliament's 1st
reading, or are in the realm of abstract problem descriptions or
business methods and thus should, according to these companies' own
%(q:Manifesto), not be patentable.

#tWc: Nevertheless, the fact that EICTA's tactics work to a certain extent
was demonstrated last week at the %(ju:European Parliament's
%(tp|Legal Affairs Committee|JURI) meeting). A number of MEPs there
was convinced that most SMEs do want software patents, and that
%(q:only programmers) oppose them. The FFII therefore urgently calls
upon the real SMEs to stand up and to make their voice heard in
Brussels.

#omt: Documents

#CRl: EICTA PR english

#CRn: EICTA PR french

#CRm: EICTA PR german

#oeM: Pro-Patent SMEs

#CEf: EICTA SME Manifesto

#eWs: Patents of EICTA's SMEs

#WWs: All patents owned by the EICTA companies would be grantable and
enforceable under the Council version of the directive, but some would
not be under the European Parliament's %(ep:amended version of
September 2003).

#opn: Company

#anN: Patent Nr.

#ait: Claimed innovation

#ndr: Patentable under EP version?

#eak: Remarks

#iot: Mantic Point Solutions Ltd, UK

#awe: No published patent applications found in EPO Database.

#gbc: Their testimony suggests they want a patent on %(q:applying business
intelligence techniques) to data collected via RFID tags.

#Wtn: Probably business method, but no actual assessment possible.

#snt: Instrumentel Ltd, UK

#tca: A wireless remote control system of which remotely controlled devices
can tap power using electromagnetic transmitted radiation (e.g., for
RFID tags)

#Wet: No direct relation with software/computers. Patent not yet granted.

#toi: Pointer Solutions Ltd, Finland

#dtS: Terminal for tracking pet animals. The description nor claims are
available online, but judging from the %(ib:IBM infomercial about
their cooperation with this company), it seems like a patent on using
a combination of GPS and SMS to locate pets, with a web based
interface.

#etn: This may be a border case depending on what is claimed and what is
actually added to the prior art, but at first sight it seems simply a
business model (track pets and offer web based interface for owners).

#see: A computer-implemented business model, whose main feature seems to be
that licensing parameters are encrypted by a key which is distinct
from the key that is used for encoding and decoding the sold software.

#omg: Business method described as %(q:computer-implemented invention).
Patent granted by the EPO.

#uuf: Searching through data by modelling the search query using simple
building blocks from a semantic web.

#eth: Patent granted by the EPO.

#alW: No patent applications found

#ene: Principle of remotely checking the status of a network service

#ukpovast: As in many cases, this software patent broadly claims a problem,
rather than a specific solution. Patent granted by UK Patent office,
not yet by EPO. Granted version not available online.

#fke: Principle of recording and playing back the mouse actions of a user
when browsing.

#Wor: Monitoring activity of a person when visiting a web page (on his own
computer), as well as determining the interactive parts of a web page.

#tWe: As in many cases, this software patents claims the problem and not the
solution. Patent granted by UK Patent office, not yet published by
EPO. Granted patent claims by UKPO unavailable.

#cwt: Keeping track of information about network connections with clients on
the server.

#tWe2: As in many cases, this software patents claims the problem and not the
solution. Patent granted by UK Patent office, not yet published by
EPO. Granted patent claims by UKPO unavailable.

#seh: A database in which each item of the database has a type.

#eWa: Patent not yet granted.

#uWW: The principle of %(q:Virtual Private Networks) (VPN's) on mobile
devices, along with the ability to specify the usage of a separate
gateway for accessing an unprotected network.

#eWa2: Patent not yet granted

#ape: Redundant data processing system having two programmed logic
controllers operating in tandem.

#WWs2: This is not a patent on data processing, but a patent on the
construction of a system to perform data processing. Where it
constitutes a new teaching regarding the controllable forces of nature
is not immediately clear however, so this is a grey zone patent.
Patent not yet granted.

#WWu: Programmable logic controller to control how long each program can run
on a processor.

#mWb: Similar as above.

#vtn: Loom (weaving machine) with electronically controlled insertion brake

#nnn: A solution for slowing down the loom using a combination of reducing
power to the engine and the pull of the yarn. Patent granted by the
EPO

#inx: Loom where the tension on the yarn (thread) is only measured when the
yarn is not moving, in order to avoid extra friction on the yarn.

#son: The automation, processing and visualisation of the measuring happens
under computer control and the displaying is pure data processing, but
the invention is the fact that the measuring is only done when the
yarn is not moving so as to avoid friction. Not yet granted.

#ngW: Mechanical braking system for a loom

#eio: No direct relation with software/computers.

#Wwy: Shaft for winding yarn around, whose diameter can vary by rotation

#twy: No direct relation with software/computers. Not yet granted.

#nui: Mechanical construction to guide yarn

#rce: No direct relation with software/computers. Patent granted by the EPO.

#dhq: Dynamically adjust the speed at which the yarn is launched in order to
have it arrive at the optimal speed, and a way to construct such a
loom without requiring additional pulse generators.

#Won: Measuring the speed and calculating the new launch speed happens under
program control and is pure data processing. However, the invention is
how to implement this in a loom without requiring additional pulse
generators. Patent granted by the EPO.

#nWW: %(q:Weaving device) (mechanical construction, no idea what exactly)

#rce2: No direct relation with software/computers. Patent granted by the EPO.

#vop: Insertion device for high-speed insertion of flexible yarns into a
yarn-processing device.

#rce3: No direct relation with software/computers. Patent granted by the EPO.

#snl: No patents found.

#aip: The description states that they have a contract to design %(q:an
innovative new engine), whereby fuel inefficiencies are fixed. This
would happen under computer/software control.

#dni: The invention is how the engine should be configured to increase the
fuel efficiency (or how it is constructed so this reconfiguration can
happen on-the-fly). The fact that this happens under computer control
is irrelevant as far as patentability is concerned.

#Wao: The principle of locating an object within an image, and then finding
other images which contian similar objects.

#eha: As in many cases, this software patents claims the problem and not the
solution. Patent not yet granted.

#sim: The principle of calculating a number of values which represent an
image, so that if you calculate such a value for another image, the
result is the same if the images are more or less the same (by trying
to make the calculation discard imperfections). Also claimed is the
use of this technique to track an object in a film.

#eha2: As in many cases, this software patents claims the problem and not the
solution. Patent not yet granted.

#yfW: Add privacy (or %(q:consensus)) information to data stored in a
database

#hkn: Business method described as %(q:computer-implemented invention).
Patent not yet granted.

#uho: A %(q:wide band sound diffuser), a mechanical construction which
because of its shape self-regulates low-frequency absorption.

#twy2: No direct relation with software/computers. Not yet granted.

#Wno: No patents found.

#man: many

#hii: As maker of low power microprocessors, ARM has both software and
regular patents and is notorious for a %(sk:troubling decision on a
software patent of theirs by the UK Patent Office) which at the same
time explains how the %(q:technical contribution) does not stop any
pure software patents.

#epW: With 500 employees, this is company is rather big for an %(q:SME).

#Wur: No patents found.

#oob: Business solution provider, appears not to develop software, but if
they did it would probably be about business processes.

#sWt: The company is certified as a %(q:Microsoft Business Solutions
independent software vendor).

#Bnu: Chairman of the Board of this company is Klaus Holse Andersen,
vice-president of Microsoft Business Solutions EMEA

#tWW: Instead of keeping track whether indiviual items in local copies of
information are up-to-date, keep track of groups of items at a time.

#tei: Not yet granted by the EPO, granted in the US.

#lWe: Monitor the quality of at least on service on at least one client of
this service, send the data about this quality to a server and keep
track of the evolution of the quality over time.

#nln: US patent, no European application filed yet. Not yet granted in the
US either.

#isc: The CEO of Giritech, René Stockne, is also the global manager for
Microsoft Navision.

#gWr: Principle of putting communication encryption/authentication on e.g. a
usb device, instead of doing it programmatically on a computer.

#nii: They are not claiming the construction of such an encryption device,
but simply the fact that you could do this. Patent not yet granted.

#Wso: 43 patents. There are about 5 %(q:grey zone) cases, the rest are pure
software patents on network communication algorithms.

#iao: This is an interesting patent. On the one hand, it claims a device to
take blood and tissue samples from animals. This is obviously not data
processing and fulfills the forces of nature requirement. On the other
hand, they claim all computer programs on their own which somehow
include the algorithm used to get these samples, even if distributed
not for use with their aparatus.

#trW: Only the claim on the program on its own would become unenforceable
under the Parliament's amendments. Patent not yet granted.

#hoW: Technique to create a hose

#rcW: No direct relation with software/computers. Withdrawn by the
applicant.

#qli: Technique to form a longitudinal fiber web

#twy3: No direct relation with software/computers. Not yet granted.

#gme: Top manager of Microsoft Denmark coming as SME representative

#Rsn: The EICTA PR quotes %(q:Jan Ishøj Nielsen, CEO, PremiTech A/S,
Denmark) as saying:

#sna: I hope that our visit to MEPs will have an impact. The patent system
in Europe has worked well for us in the past. It's true that there
could be more harmonisation, and the Council Common Position provides
for that. But harmonisation is certainly not welcome at the expense of
excluding inventions in the area of data processing as well as in
other areas as suggested by the European Parliament in first reading. 
I hope that my property rights will be safeguarded and also my freedom
of choice whether I want to protect my possible future inventions by
patents or not.

#lrW: Some simple web research however offers the following information:

#sca: Mr. Klaus Holse Andersen is the EMEA Vice president of Microsoft. 
Klaus Holse Andersen has held Danish as well as international top
management positions within large software corporations such as Oracle
and Microsoft. He is the former CEO of LicEnergy, a European software
company, and the former CEO of venture capital company IVS in Denmark.
Klaus Holse Andersen also serves as Board Member in a number of IT
companies.

#pgW: At this first session on the software patent directive in the EP,
several formerly critical MEPs said that %(q:the industry), regardless
of company size, wants patents.  The session room was packed full with
pro-patent lobbyists, so that it was almost impossible to move.

#tdo: Whoever writes on the directive should have read this tabular
comparison of key provisions/amendments first.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/EictaSme0504.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: EictaSme0504 ;
# txtlang: xx ;
# End: ;

