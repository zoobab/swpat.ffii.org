<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Hartmut PILCH und FFII">
<link href="http://localhost/phm" rev="made">
<link href="http://www.ffii.org/verein/webstyl/news.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2005-04-26">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="de">
<meta http-equiv="Reply-To" content="phm@a2e.de">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="F&ouml;rderverein f&uuml;r eine Freie Informationelle Infrastruktur, Logikpatente, Geistiges Eigentum, Gewerblicher Rechtschutz, GE, GE &amp; GR, immaterielle Verm&ouml;gensgegenst&auml;nde, Immaterialg&uuml;terrecht, mathematische Methoden, Gesch&auml;ftsverfahren, Organisations- und Rechenregeln, Erfindung, Nicht-Erfindungen, computer-implementierte Erfindungen, computer-implementierbare Erfindungen, software-bezogene Erfindungen, software-implementierte Erfindungen, Softwarepatent">
<meta name="title" content="Mega corporations sending SMEs to European Parliament">
<meta name="description" content="EICTA is sending 50 &quot;hightech SMEs&quot; to visit the European Parliament and complain about loss of patent protections.  Most of these SMEs are losers of the software patent game.  As far as they own any patents at all, these are either business method patents, which their own &quot;Manifesto&quot; says shouldn't be grantable, or patents on solutions involving forces of nature, which are grantable under the Parliament's position of 2003 anyway.  At least two of of the SME bosses are high-ranking Microsoft managers.  Others are business partners of companies like IBM.  Yet EICTA's &quot;SME lobbying&quot; efforts appear to have swayed several formerly critical JURI MEPs into publicly stating that &quot;the industry&quot;, regardless of company size, wants software patents.">
<title>Mega corporations sending SMEs to European Parliament</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<!--#include virtual="links.de.html"-->
<div id="doktit">
<h1>Mega corporations sending SMEs to European Parliament</h1>
</div>
<div id="dokdes">
<!--#include virtual="deskr.de.html"-->
</div>
</div>
<div id="dokmid">
<div class="sectmenu">
<ul><li><a href="#text">Microsoft and IBM masquerading as European SMEs</a></li>
<li><a href="#eicta">Documents</a></li>
<li><a href="#pikta">Patents of EICTA's SMEs</a></li>
<li><a href="#microsoft">Top manager of Microsoft Denmark coming as SME representative</a></li>
<li><a href="#links">Kommentierte Verweise</a></li></ul>
</div>

<div class="secttext">
<div class="secthead">
<h2><a name="text">Microsoft and IBM masquerading as European SMEs</a></h2>
</div>

<div class="sectbody">
Apparently unable to find enough independent small and medium-sized enterprises SMEs to lobby for software patents, EICTA has resorted to sending companies to the European Parliament in which its multinational members hold a strong position. Additionally, several SMEs who were inspired by this action and signed EICTA's appeal turn out not to own any software patents at all.<p>&quot;The patent system in Europe has worked well for us in the past&quot;, says Jan Ish&oslash;j Nielsen, CEO of PremiTech A/S. However, he forgets to mention that his company's first European software patent is still waiting to be granted (its US cousin is already valid) and that the Chairman of the Board of his company is Klaus Holse Andersen, VP Microsoft Business Solutions EMEA. Mr. Andersen was previously also CEO of the investment company IVS, another signatory of EICTA's demands.<p>Apart from sending SMEs to Brussels, EICTA also organised a letter writing campaign to the European Parliament. While for most companies the CEO has signed this letter, in case of Giritech the CTO observes the honours. One reason for this might be that the CEO of Giritech, Ren&eacute; Stockne, has a second job as global manager for Microsoft Navision. This is the same Microsoft Navision that threatened the Danish government with moving out of Denmark if the software patents do not come through.<p>Jonas Maebe, FFII Board Member, comments:<p><div class=cit>
It is really sad how low EICTA is sinking here. Unable to factually counter the resistance to software patents from real SME associations such as UEAPME and CEAPME, they are now resorting to dressing up Microsoft as an SME. &quot;Oh, grandmother, what big teeth you have got&quot; indeed.
</div><p>Some other companies that signed the letter and which are not tied to any big firms, have simply been improperly informed about the reach of the European Parliament's amendments. For example, Te Strake, an innovative Dutch SME active in the field of &quot;mechatronics&quot;, owns several patents on inventions related to automated weaving. According to an analysis performed by the FFII, all of their patents would remain enforceable under the directive as approved by European Parliament in first reading.<p>Hartmut Pilch (president of FFII) summarises<p><div class=cit>
Preliminary research shows that most of these companies own less than a handful of patents and that the claimed solutions either involve forces of nature and are largely unaffected by the Parliament's 1st reading, or are in the realm of abstract problem descriptions or business methods and thus should, according to these companies' own &quot;Manifesto&quot;, not be patentable.
</div><p>Nevertheless, the fact that EICTA's tactics work to a certain extent was demonstrated last week at the European Parliament's Legal Affairs Committee (JURI) meeting. A number of MEPs there was convinced that most SMEs do want software patents, and that &quot;only programmers&quot; oppose them. The FFII therefore urgently calls upon the real SMEs to stand up and to make their voice heard in Brussels.
</div>

<div class="secthead">
<h2><a name="eicta">Documents</a></h2>
</div>

<div class="sectbody">
<ul><li><a href="eictasme0504.pdf">EICTA PR english</a> (PDF)</li>
<li><a href="eictasme0504.fr.pdf">EICTA PR french</a> (PDF)</li>
<li><a href="eictasme0504.de.pdf">EICTA PR german</a> (PDF)</li>
<li><a href="pro-patent-smes.pdf">Pro-Patent SMEs</a> (PDF)</li>
<li><a href="eicta-sme-manifesto.txt">EICTA SME Manifesto</a> (PDF)</li>
<li><a href="/akteure/eicta/index.en.html">EICTA und Software-Patente</a></li></ul>
</div>

<div class="secthead">
<h2><a name="pikta">Patents of EICTA's SMEs</a></h2>
</div>

<div class="sectbody">
All patents owned by the EICTA companies would be grantable and enforceable under the Council version of the directive, but some would not be under the European Parliament's <a href="/papiere/europarl0309/index.de.html">amended version of September 2003</a>.<p><table border=1>
<tr><th align=center>Company</th><th align=center>Patent Nr.</th><th align=center>Claimed innovation</th><th align=center>Patentable under EP version?</th><th align=center>Remarks</th></tr>
<tr><th>Mantic Point Solutions Ltd, UK</th><td>No published patent applications found in EPO Database.</td><td>Their testimony suggests they want a patent on &quot;applying business intelligence techniques&quot; to data collected via RFID tags.</td><td>-?</td><td>Probably business method, but no actual assessment possible.</td></tr>
<tr><th>Instrumentel Ltd, UK</th><td><a href="../../../patente/txt/ep/1470/539">EP1470539</a></td><td>A wireless remote control system of which remotely controlled devices can tap power using electromagnetic transmitted radiation (e.g., for RFID tags)</td><td>+</td><td>No direct relation with software/computers. Patent not yet granted.</td></tr>
<tr><th>Pointer Solutions Ltd, Finland</th><td>FI5742U</td><td>Terminal for tracking pet animals. The description nor claims are available online, but judging from the <a href="http://www-306.ibm.com/software/success/cssdb.nsf/CS/CBEN-5PNFHX?OpenDocument&amp;Site=">IBM infomercial about their cooperation with this company</a>, it seems like a patent on using a combination of GPS and SMS to locate pets, with a web based interface.</td><td>-?</td><td>This may be a border case depending on what is claimed and what is actually added to the prior art, but at first sight it seems simply a business model (track pets and offer web based interface for owners).</td></tr>
<tr><th><a href="http://wiki.ffii.org/WibuDe">Wibu-Systems AG</a>, Deutschland</th><td><a href="../../../patente/txt/ep/1184/771">EP1184771</a></td><td>A computer-implemented business model, whose main feature seems to be that licensing parameters are encrypted by a key which is distinct from the key that is used for encoding and decoding the sold software.</td><td>-</td><td>Business method described as &quot;computer-implemented invention&quot;. Patent granted by the EPO.</td></tr>
<tr><th>USU AG, Deutschland</th><td><a href="../../../patente/txt/ep/1064/606">EP1064606</a></td><td>Searching through data by modelling the search query using simple building blocks from a semantic web.</td><td>-</td><td>Patent granted by the EPO.</td></tr>
<tr><th>SSF, Schweden</th><td>No patent applications found</td></tr>
<tr><th rowspan=4>Speed-trap Ltd, UK</th><td><a href="../../../patente/txt/ep/1264/261">EP1264261</a></td><td>Principle of remotely checking the status of a network service</td><td>-</td><td>As in many cases, this software patent broadly claims a problem, rather than a specific solution. Patent granted by UK Patent office, not yet by EPO. Granted version not available online.</td></tr>
<tr><td>GB2357679</td><td>Principle of recording and playing back the mouse actions of a user when browsing.</td><td>-</td><td>As in many cases, this software patent broadly claims a problem, rather than a specific solution. Patent granted by UK Patent office, not yet by EPO. Granted version not available online.</td></tr>
<tr><td>GB2357680</td><td>Monitoring activity of a person when visiting a web page (on his own computer), as well as determining the interactive parts of a web page.</td><td>-</td><td>As in many cases, this software patents claims the problem and not the solution. Patent granted by UK Patent office, not yet published by EPO. Granted patent claims by UKPO unavailable.</td></tr>
<tr><td>GB2356783</td><td>Keeping track of information about network connections with clients on the server.</td><td>-</td><td>As in many cases, this software patents claims the problem and not the solution. Patent granted by UK Patent office, not yet published by EPO. Granted patent claims by UKPO unavailable.</td></tr>
<tr><th rowspan=2>Birdstep Techn. ASA, Schweden</th><td><a href="../../../patente/txt/ep/0980/554">EP0980554</a></td><td>A database in which each item of the database has a type.</td><td>-</td><td>Patent not yet granted.</td></tr>
<tr><td><a href="../../../patente/txt/ep/1381/202">EP1381202</a></td><td>The principle of &quot;Virtual Private Networks&quot; (VPN's) on mobile devices, along with the ability to specify the usage of a separate gateway for accessing an unprotected network.</td><td>-</td><td>Patent not yet granted</td></tr>
<tr><th rowspan=2>Nyquist</th><td>WO9826350</td><td>Redundant data processing system having two programmed logic controllers operating in tandem.</td><td>?</td><td>This is not a patent on data processing, but a patent on the construction of a system to perform data processing. Where it constitutes a new teaching regarding the controllable forces of nature is not immediately clear however, so this is a grey zone patent. Patent not yet granted.</td></tr>
<tr><td><a href="../../../patente/txt/ep/0766/156">EP0766156</a></td><td>Programmable logic controller to control how long each program can run on a processor.</td><td>?</td><td>Similar as above.</td></tr>
<tr><th rowspan=8>Te Strake</th><td><a href="../../../patente/txt/ep/1147/250">EP1147250</a></td><td>Loom (weaving machine) with electronically controlled insertion brake</td><td>+</td><td>A solution for slowing down the loom using a combination of reducing power to the engine and the pull of the yarn. Patent granted by the EPO</td></tr>
<tr><td>WO03008684</td><td>Loom where the tension on the yarn (thread) is only measured when the yarn is not moving, in order to avoid extra friction on the yarn.</td><td>+</td><td>The automation, processing and visualisation of the measuring happens under computer control and the displaying is pure data processing, but the invention is the fact that the measuring is only done when the yarn is not moving so as to avoid friction. Not yet granted.</td></tr>
<tr><td>NL1022800C</td><td>Mechanical braking system for a loom</td><td>+</td><td>No direct relation with software/computers.</td></tr>
<tr><td><a href="../../../patente/txt/ep/1354/836">EP1354836</a></td><td>Shaft for winding yarn around, whose diameter can vary by rotation</td><td>+</td><td>No direct relation with software/computers. Not yet granted.</td></tr>
<tr><td><a href="../../../patente/txt/ep/1232/112">EP1232112</a></td><td>Mechanical construction to guide yarn</td><td>+</td><td>No direct relation with software/computers. Patent granted by the EPO.</td></tr>
<tr><td><a href="../../../patente/txt/ep/1259/667">EP1259667</a></td><td>Dynamically adjust the speed at which the yarn is launched in order to have it arrive at the optimal speed, and a way to construct such a loom without requiring additional pulse generators.</td><td>+</td><td>Measuring the speed and calculating the new launch speed happens under program control and is pure data processing. However, the invention is how to implement this in a loom without requiring additional pulse generators. Patent granted by the EPO.</td></tr>
<tr><td>WO2005012610</td><td>&quot;Weaving device&quot; (mechanical construction, no idea what exactly)</td><td>+</td><td>No direct relation with software/computers. Patent granted by the EPO.</td></tr>
<tr><td>WO2004059056</td><td>Insertion device for high-speed insertion of flexible yarns into a yarn-processing device.</td><td>+</td><td>No direct relation with software/computers. Patent granted by the EPO.</td></tr>
<tr><th>ParOS</th><td>No patents found.</td><td>The description states that they have a contract to design &quot;an innovative new engine&quot;, whereby fuel inefficiencies are fixed. This would happen under computer/software control.</td><td>+</td><td>The invention is how the engine should be configured to increase the fuel efficiency (or how it is constructed so this reconfiguration can happen on-the-fly). The fact that this happens under computer control is irrelevant as far as patentability is concerned.</td></tr>
<tr><th rowspan=2>LTU Technologies</th><td>WO0144887</td><td>The principle of locating an object within an image, and then finding other images which contian similar objects.</td><td>-</td><td>As in many cases, this software patents claims the problem and not the solution. Patent not yet granted.</td></tr>
<tr><td>WO2004015590</td><td>The principle of calculating a number of values which represent an image, so that if you calculate such a value for another image, the result is the same if the images are more or less the same (by trying to make the calculation discard imperfections). Also claimed is the use of this technique to track an object in a film.</td><td>-</td><td>As in many cases, this software patents claims the problem and not the solution. Patent not yet granted.</td></tr>
<tr><th>Abattia Group Ltd</th><td><a href="../../../patente/txt/ep/1223/497">EP1223497</a></td><td>Add privacy (or &quot;consensus&quot;) information to data stored in a database</td><td>-</td><td>Business method described as &quot;computer-implemented invention&quot;. Patent not yet granted.</td></tr>
<tr><th>ESR</th><td><a href="../../../patente/txt/ep/1506/541">EP1506541</a></td><td>A &quot;wide band sound diffuser&quot;, a mechanical construction which because of its shape self-regulates low-frequency absorption.</td><td>+</td><td>No direct relation with software/computers. Not yet granted.</td></tr>
<tr><th>Logotech Engineering</th><td>No patents found.</td><td>?</td><td>?</td></tr>
<tr><th>ARM Limited</th><td>many</td><td>As maker of low power microprocessors, ARM has both software and regular patents and is notorious for a <a href="http://www.softwarepatents.co.uk/current/technical-contribution.html">troubling decision on a software patent of theirs by the UK Patent Office</a> which at the same time explains how the &quot;technical contribution&quot; does not stop any pure software patents.</td><td>+-</td><td>With 500 employees, this is company is rather big for an &quot;SME&quot;.</td></tr>
<tr><th>Zoomio</th><td>No patents found.</td><td>Business solution provider, appears not to develop software, but if they did it would probably be about business processes.</td><td>-?</td><td>The company is certified as a &quot;Microsoft Business Solutions independent software vendor&quot;.</td></tr>
<tr><th rowspan=2>Premitech (Chairman of the Board of this company is Klaus Holse Andersen, vice-president of Microsoft Business Solutions EMEA)</th><td><a href="../../../patente/txt/ep/1228/454">EP1228454</a></td><td>Instead of keeping track whether indiviual items in local copies of information are up-to-date, keep track of groups of items at a time.</td><td>-</td><td>Not yet granted by the EPO, granted in the US.</td></tr>
<tr><td>US2005027858</td><td>Monitor the quality of at least on service on at least one client of this service, send the data about this quality to a server and keep track of the evolution of the quality over time.</td><td>-</td><td>US patent, no European application filed yet. Not yet granted in the US either.</td></tr>
<tr><th>Giritech (The CEO of Giritech, Ren&eacute; Stockne, is also the global manager for Microsoft Navision.)</th><td>WO2004099940</td><td>Principle of putting communication encryption/authentication on e.g. a usb device, instead of doing it programmatically on a computer.</td><td>-</td><td>They are not claiming the construction of such an encryption device, but simply the fact that you could do this. Patent not yet granted.</td></tr>
<tr><th>Net Insight</th><td>43 patents. There are about 5 &quot;grey zone&quot; cases, the rest are pure software patents on network communication algorithms.</td><td>--+</td><td>&nbsp;</td></tr>
<tr><th>Dilab</th><td><a href="../../../patente/txt/ep/1345/533">EP1345533</a></td><td><a href="http://wiki.ffii.org/DilabProg04De">This is an interesting patent. On the one hand, it claims a device to take blood and tissue samples from animals. This is obviously not data processing and fulfills the forces of nature requirement. On the other hand, they claim all computer programs on their own which somehow include the algorithm used to get these samples, even if distributed not for use with their aparatus.</a></td><td>+</td><td>Only the claim on the program on its own would become unenforceable under the Parliament's amendments. Patent not yet granted.</td></tr>
<tr><th rowspan=2>Van Wees</th><td><a href="../../../patente/txt/ep/0355/901">EP0355901</a></td><td>Technique to create a hose</td><td>+</td><td>No direct relation with software/computers. Withdrawn by the applicant.</td></tr>
<tr><td><a href="../../../patente/txt/ep/1292/520">EP1292520</a></td><td>Technique to form a longitudinal fiber web</td><td>+</td><td>No direct relation with software/computers. Not yet granted.</td></tr>
</table>
</div>

<div class="secthead">
<h2><a name="microsoft">Top manager of Microsoft Denmark coming as SME representative</a></h2>
</div>

<div class="sectbody">
The EICTA PR quotes &quot;Jan Ish&oslash;j Nielsen, CEO, PremiTech A/S, Denmark&quot; as saying:<p><div class=cit>
I hope that our visit to MEPs will have an impact. The patent system in Europe has worked well for us in the past. It's true that there could be more harmonisation, and the Council Common Position provides for that. But harmonisation is certainly not welcome at the expense of excluding inventions in the area of data processing as well as in other areas as suggested by the European Parliament in first reading.  I hope that my property rights will be safeguarded and also my freedom of choice whether I want to protect my possible future inventions by patents or not.
</div><p>Some simple web research however offers the following information:<p><ul><li><a href="http://premitech.dk/p403.htm">http://premitech.dk/p403.htm</a></li>
<li><a href="http://www.microsoft.com/danmark/pr/articles/pressemeddelelse_445.asp">http://www.microsoft.com/danmark/pr/articles/pressemeddelelse_445.asp</a></li></ul><p><div class=cit>
Mr. Klaus Holse Andersen is the EMEA Vice president of Microsoft.  Klaus Holse Andersen has held Danish as well as international top management positions within large software corporations such as Oracle and Microsoft. He is the former CEO of LicEnergy, a European software company, and the former CEO of venture capital company IVS in Denmark. Klaus Holse Andersen also serves as Board Member in a number of IT companies.
</div>
</div>

<div class="secthead">
<h2><a name="links">Kommentierte Verweise</a></h2>
</div>

<div class="sectbody">
<div class="links">
<dl><dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="http://wiki.ffii.org/Juri050421De">JURI debates Software Patents -- Recording and Transcription</a></b></dt>
<dd>At this first session on the software patent directive in the EP, several formerly critical MEPs said that &quot;the industry&quot;, regardless of company size, wants patents.  The session room was packed full with pro-patent lobbyists, so that it was almost impossible to move.</dd>
<dt><b><img src="/img/icons/gotodoc.png" alt="-&gt;"><a href="/papiere/europarl0309/cons0401/tab/index.en.html">EU-Softwarepatent-Richtlinie: Versionen von Parlament und Rat</a></b></dt>
<dd>Whoever writes on the directive should have read this tabular comparison of key provisions/amendments first.</dd></dl>
</div>
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<!--#include virtual="doksrow.de.html"-->
</div>
<!--#include virtual="../../../valid.de.html"-->
<div id="dokadr">
http://swpat.ffii.org/log/05/eictasme04/index.de.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2005-04-26
<a href="http://www.a2e.de">Hartmut PILCH</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/EictaSme0504.el -->
<!-- mode: html -->
<!-- End: -->

