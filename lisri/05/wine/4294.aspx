
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Software Patents Stifle Open Source?</title>
		<meta content=".Text" name="GENERATOR">
		<link id="MainStyle" type="text/css" rel="stylesheet" href="/skins/marvin3/style.css"></link>
		
		<link id="RSSLink" title="RSS" type="application/rss+xml" rel="alternate" href="http://blogs.borland.com/dcc/rss.aspx"></link>
	</HEAD>
	<body>
		<form name="Form1" method="post" action="4294.aspx" id="Form1">
<input type="hidden" name="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" value="" />
<script language="javascript">
<!--
	function __doPostBack(eventTarget, eventArgument) {
		var theform;
		if (window.navigator.appName.toLowerCase().indexOf("netscape") > -1) {
			theform = document.forms["Form1"];
		}
		else {
			theform = document.Form1;
		}
		theform.__EVENTTARGET.value = eventTarget.split("$").join(":");
		theform.__EVENTARGUMENT.value = eventArgument;
		theform.submit();
	}
// -->
</script>

			
<div id="top">
	
<h1><a id="Header1_HeaderTitle" class="headermaintitle" href="http://blogs.borland.com/dcc/">Delphi Compiler Core</a></h1>
<p id="tagline">Random notes on Borland's Delphi language and tools. Hosted by Borland Chief Scientist Danny Thorpe</p>
</div>
<div id="leftmenu">
	
		
<h3>My Links</h3>
<ul>
			<li><a id="MyLinks1_HomeLink" href="http://blogs.borland.com/dcc/">Home</a></li>
			<li><a id="MyLinks1_ContactLink" accesskey="9" href="http://blogs.borland.com/dcc/contact.aspx">Contact</a></li>
			<li><a id="MyLinks1_Syndication" href="http://blogs.borland.com/dcc/Rss.aspx">Syndication</a><a id="MyLinks1_XMLLink" href="http://blogs.borland.com/dcc/Rss.aspx"><img src="../../../../../images/xml.gif" border="0" /></a>
			<li><a id="MyLinks1_Admin" href="http://blogs.borland.com/dcc/login.aspx">Login</a></li>
</ul>
		
		
		<h3>Post Categories</h3>
		
				<ul>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl1_Link" href="http://blogs.borland.com/dcc/category/176.aspx">Community</a> <a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl1_RssLink" title="Subscribe to Community" href="http://blogs.borland.com/dcc/category/176.aspx/rss">(rss)</a></li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl2_Link" href="http://blogs.borland.com/dcc/category/171.aspx">Corporate</a> <a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl2_RssLink" title="Subscribe to Corporate" href="http://blogs.borland.com/dcc/category/171.aspx/rss">(rss)</a></li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl3_Link" href="http://blogs.borland.com/dcc/category/177.aspx">Events</a> <a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl3_RssLink" title="Subscribe to Events" href="http://blogs.borland.com/dcc/category/177.aspx/rss">(rss)</a></li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl4_Link" href="http://blogs.borland.com/dcc/category/175.aspx">Industry</a> <a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl4_RssLink" title="Subscribe to Industry" href="http://blogs.borland.com/dcc/category/175.aspx/rss">(rss)</a></li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl5_Link" href="http://blogs.borland.com/dcc/category/173.aspx">Product</a> <a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl5_RssLink" title="Subscribe to Product" href="http://blogs.borland.com/dcc/category/173.aspx/rss">(rss)</a></li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl6_Link" href="http://blogs.borland.com/dcc/category/174.aspx">Research</a> <a id="ArchiveLinks1_Categories_CatList__ctl0_LinkList__ctl6_RssLink" title="Subscribe to Research" href="http://blogs.borland.com/dcc/category/174.aspx/rss">(rss)</a></li>
			
				</ul>
			
	
		<h3>Archives</h3>
		
				<ul>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl1_Link" href="http://blogs.borland.com/dcc/archive/2004/01.aspx">January, 2004 (1)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl2_Link" href="http://blogs.borland.com/dcc/archive/2005/01.aspx">January, 2005 (1)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl3_Link" href="http://blogs.borland.com/dcc/archive/2005/02.aspx">February, 2005 (3)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl4_Link" href="http://blogs.borland.com/dcc/archive/2004/03.aspx">March, 2004 (8)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl5_Link" href="http://blogs.borland.com/dcc/archive/2005/03.aspx">March, 2005 (6)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl6_Link" href="http://blogs.borland.com/dcc/archive/2004/04.aspx">April, 2004 (5)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl7_Link" href="http://blogs.borland.com/dcc/archive/2005/04.aspx">April, 2005 (2)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl8_Link" href="http://blogs.borland.com/dcc/archive/2004/05.aspx">May, 2004 (6)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl9_Link" href="http://blogs.borland.com/dcc/archive/2005/05.aspx">May, 2005 (1)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl10_Link" href="http://blogs.borland.com/dcc/archive/2004/06.aspx">June, 2004 (3)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl11_Link" href="http://blogs.borland.com/dcc/archive/2003/07.aspx">July, 2003 (7)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl12_Link" href="http://blogs.borland.com/dcc/archive/2004/07.aspx">July, 2004 (5)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl13_Link" href="http://blogs.borland.com/dcc/archive/2004/08.aspx">August, 2004 (4)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl14_Link" href="http://blogs.borland.com/dcc/archive/2004/09.aspx">September, 2004 (7)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl15_Link" href="http://blogs.borland.com/dcc/archive/2004/10.aspx">October, 2004 (2)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl16_Link" href="http://blogs.borland.com/dcc/archive/2004/11.aspx">November, 2004 (2)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl17_Link" href="http://blogs.borland.com/dcc/archive/2003/12.aspx">December, 2003 (1)</a> </li>
			
				<li><a id="ArchiveLinks1_Categories_CatList__ctl1_LinkList__ctl18_Link" href="http://blogs.borland.com/dcc/archive/2004/12.aspx">December, 2004 (2)</a> </li>
			
				</ul>
			
	

	
</div>
<div id="rightmenu">
	
		
<h3>Blog Stats</h3>
	<ul>
		<li>Posts - 66
		<li>Stories - 0
		<li>Comments - 153
		<li>Trackbacks - 24
	</li>
</ul>
		
		<h3>Blog Toys</h3>
		
				<ul>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl0_LinkList__ctl1_Link" href="http://blogshares.com/blogs.php?blog=http%3A%2F%2Fblogs.borland.com%2Fdcc%2F&amp;user=14357"><img src="http://blogshares.com/images/blogshares.jpg" alt="Listed on BlogShares" width="117" height="23"/></a> </li>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl0_LinkList__ctl2_Link" href="http://www.technorati.com/claim/dm4buuf3q">Technorati</a> </li>
			
				</ul>
			
	
		<h3>Borland</h3>
		
				<ul>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl1_LinkList__ctl1_Link" href="http://bdn.borland.com">BDN</a> </li>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl1_LinkList__ctl2_Link" href="http://www.borland.com">Borland</a> </li>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl1_LinkList__ctl3_Link" href="http://blogs.borland.com">Borland Blogs</a> </li>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl1_LinkList__ctl4_Link" href="http://blogs.borland.com/team/delphi">Delphi Team Blogs</a> </li>
			
				</ul>
			
	
		<h3>Delphi Bloggers</h3>
		
				<ul>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl2_LinkList__ctl1_Link" href="http://blogs.teamb.com/craigstuntz/">Craig Stuntz</a> </li>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl2_LinkList__ctl2_Link" href="http://hallvards.blogspot.com/">Hallvard Vassbotn</a> </li>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl2_LinkList__ctl3_Link" href="http://www.lemanix.com/nick">Nick Hodges</a> </li>
			
				</ul>
			
	
		<h3>On the Radar</h3>
		
				<ul>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl3_LinkList__ctl1_Link" href="http://www.adug.org.au/meetings/Symposia/2005/">ADUG Symposium 2005</a> </li>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl3_LinkList__ctl2_Link" href="http://www.simplegeek.com/">Chris Anderson</a> </li>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl3_LinkList__ctl3_Link" href="http://blogs.msdn.com/cyrusn/">Cyrus' Blather</a> </li>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl3_LinkList__ctl4_Link" href="http://www.danielmoth.com/Blog/">Daniel Moth</a> </li>
			
				<li><a id="CategoryDisplay1_Categories_CatList__ctl3_LinkList__ctl5_Link" href="http://www.itwriting.com/blog/">Tim Anderson</a> </li>
			
				</ul>
			
	

	
</div>
<div id="main">
	
					
	<div class="post">
		<h2>
			<a id="viewpost.ascx_TitleUrl" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx">Software Patents Stifle Open Source?</a>
		</h2>
		<P>In reading <A href="http://uk.builder.com/programming/unix/0,39026612,39246157,00.htm">this article</A> at uk.builder, one might get the impression that Borland is pulling a SCO scorched earth&nbsp;manuver&nbsp;by maniacally raping and pillaging Open Source advocates over the use of a structure exception handling (SEH) technique that Borland happens to have a patent on.&nbsp; I find it particularly significant that neither the uk.builder reporter nor the Open Source advocates felt it necessary to obtain a comment from a&nbsp;Borland representative before airing their gripes.</P>
<P>Over on slashdot, <A href="http://yro.slashdot.org/yro/05/05/12/1947213.shtml?tid=155&amp;tid=8&amp;tid=106">this post</A> posits that the reason GCC doesn't support SEH is <EM>because</EM> of the Borland patent.</P>
<P>That's rich, and typical of Slashdot.&nbsp;&nbsp;Let's take a look at the time lines here:&nbsp; GCC has been around for centuries, without Windows-style SEH support.&nbsp; In fact, GCC's implementation of C++ exception handling&nbsp;produces so much code bloat&nbsp;(50 to 150% larger exes)&nbsp;that&nbsp;nobody&nbsp;enables GCC exception handling support unless they're absolutely desperate or completely clueless.&nbsp;&nbsp;</P>
<P>Next, Borland enters the Linux scene relatively recently, discovers that GCC can't cut the mustard for SEH semantics and language interop&nbsp;found on other platforms (Windows)&nbsp;(let's not harp on the code bloat issue), so Borland implements SEH on its own in its own compilers (with much less code bloat), at considerable expense (and no bloat).&nbsp; Since this represents a fairly unique nugget of technology, Borland patents its SEH technique and adds it to the Borland Defense Fund.&nbsp; (Remember that $100 million payment from Microsoft to Borland a few years ago?&nbsp; Borland didn't start that fight, but we certainly finished it.)</P>
<P>Whether software patents are the root of all evil or merely a device in our intellectual economy is really beside the point.&nbsp; Patents suck, sure, but it's the system that we're given.&nbsp; You can choose to abstain on principle, and then be walked all over by those who use the system against you.&nbsp; Or, you can choose to defend yourself and use the system to&nbsp;make the schoolyard bullies regret bothering you.&nbsp; </P>
<P>The existance of a patent does not categorically mean all software&nbsp;based on similar ideas&nbsp;open or otherwise is null and void.&nbsp; Licensing IP to specific open source projects is not unheard of, even with no monetary interest.&nbsp; I believe IBM has granted IP rights for some of its compression patents to imaging standards bodies.</P>
<P>If you're seeking reassurance that some artifact won't be used to sabotage a project, just ring the doorbell and ask.&nbsp; There are much better ways to open a discussion than&nbsp;throwing a brick through the window.</P>
<P>&nbsp;</P>
		<p class="postfoot">
			posted on Thursday, May 12, 2005 6:22 PM
		</p>
	</div>
	<link rel="pingback" href="http://blogs.borland.com/dcc/Services/Pingback.aspx">
	
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
<rdf:Description
rdf:about="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx"
dc:identifier="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx"
dc:title="Software Patents Stifle Open Source?"
trackback:ping="http://blogs.borland.com/dcc/services/trackbacks/4294.aspx" />
</rdf:RDF>
-->

	
<a name = "feedback" />
<div id="comments">
<h3>Feedback</h3>
	
	
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4299">#</a>&nbsp;<a name="4299"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 5:08 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl0_NameLink" target="_blank">virtualblackfox</a>
			</h4>
			<p>
				IBM even allowed use of lots of their patents, for opensource (OSI) softwares.<br><br>But alowing for 1 software is very very strange regarding the open source way of doing think. GCC will have problems if there is a fork and other free softwares won't be able to reuse part of his code. It totaly goes against the target of free software.
				<a id="Comments.ascx_CommentList__ctl0_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl0$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4300">#</a>&nbsp;<a name="4300"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 5:47 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl1_NameLink" target="_blank">Brian S.</a>
			</h4>
			<p>
				I believe Borland have wrong strategy on it's market, I remember one time when we talk about Borland we understand it's competitor of microsoft  ,everything, it had it's Office solution not complete but realy Borland Shutted down microsoft alongtime about programming languages, borland before micrsofot bring a solution to replace MSOLE in win3.1 , Microsoft never return to development solutions market until 1995-96,, <br><br>Borland have to look for many other projects, you cannot just stay and wait for programmers come and buy your solutions, you have to spread your market to many solutions, believe, Borland since 1983 is a lovely name for all developers and now I see borland stay in one step to work with programmers, developers, anyway, Opensource is nice but not for made money in Bilions! so! :) Borland is a greate name but it's a shop just have milkshake! :)) <br><br>
				<a id="Comments.ascx_CommentList__ctl1_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl1$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4301">#</a>&nbsp;<a name="4301"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 5:51 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl2_NameLink" href="http://droogs.org/" target="_blank">Aaron Trevena</a>
			</h4>
			<p>
				This is a pure-software patent. Borland chose to be part of the problem rather than part of the solution.<br><br>I don't have a problem with borland writing proprietary software, I do have a problem when their software patents stop others from writing software.<br><br>By patenting an algorithm borland have fenced off another piece of the mathematics, taking a bite from the public domain leaving less for everybody else.<br><br>The gcc and wine developers shouldn't have to go cap in hand to every patent holder begging their patronage, protection and permission just to write original sofwtare like a serf in a medievel fiefdom.<br><br>Unfortunately IBM and Borland like F/OSS to be kept in it's place, tolerated and patronised rather than treated as an equal.<br><br>Borland could have done many things rather than patent what should never be patented. They could have published, or they could have registered with any of the repositarys available. Borland instead chose to leave yet another mine in the already treacherous minefield in the way of every software developer who has to write a line of code.
				<a id="Comments.ascx_CommentList__ctl2_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl2$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4302">#</a>&nbsp;<a name="4302"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 6:15 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl3_NameLink" target="_blank">Koen van de Sande</a>
			</h4>
			<p>
				Have any of the commenters here actually read what Danny has written? You are immediately following the Slashdot approach: Borland holds a patent one something OSS people wanna use, so Borland is evil. Without ever contacting Borland! All they did was find out that Borland holds a patent, and Borland hasn't said anything on the matter, it just holds the patent since 7 years.<br>They just start publishing articles with Linux rocks and Borland is bad. Wake up, the system is bad, but if Borland hadn't patented it, someone else would have!
				<a id="Comments.ascx_CommentList__ctl3_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl3$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4303">#</a>&nbsp;<a name="4303"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 6:29 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl4_NameLink" href="http://iandmccreath@rogers.com" target="_blank">Ian M</a>
			</h4>
			<p>
				Comments about proprietary developers contributing to open source make me recall this old posting by Gavriel State who once headed up Corel's linux development effort. <br><br><a rel="nofollow" target="_new" href="http://linux.slashdot.org/article.pl?sid=04/04/02/011200&amp;tid=132&amp;tid=106">http://linux.slashdot.org/article.pl?sid=04/04/02/011200&amp;tid=132&amp;tid=106</a><br><br>He recounts what they went through trying to get WP Office to build on linux. It provides an interesting perspective on the receptivity of the OS community to commercial development. 
				<a id="Comments.ascx_CommentList__ctl4_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl4$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4305">#</a>&nbsp;<a name="4305"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 7:06 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl5_NameLink" target="_blank">Brian S.</a>
			</h4>
			<p>
				:) yes, I inform it but we return to a very old issue ,,,IBM and Borland  might have an  strategic works together, ... Borland have the Best solutions ever for developers,... but I means company can  focus on many other things not just special customers like us! ,,, <br><br>anyway! <br><br>you remember me to ask : WHERE IS NEW KYLIX! realy we missed it, :) <br><br>in last 13years always I follow borland always we think when they will bring a new solution into market? ... I feel danny surprised when we bring our nagging here, because realy borland is connected to our future because we have alot of code in delphi,Jbuilder, C++Builder... 
				<a id="Comments.ascx_CommentList__ctl5_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl5$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4306">#</a>&nbsp;<a name="4306"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 7:30 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl6_NameLink" target="_blank">Wolverine</a>
			</h4>
			<p>
				The reality is that Borland has failed to innovate recently; gone are the technical successes of the past (and the people who knew what they were doing too) and Delphi is stagnating and not being used enough. <br><br>Now Borland tries to become a bully too by its patent on this technique. The technique is not Borland's and prior art existed too. So let's call things like they are. Borland's behaviour is inexcusable. Will this increase the number of people using Delphi or other Borland products?<br><br>I don't think so. Better rename Borland as Bland. <br><br>Borland should give a free license to winelib since they need this technique. It's not as if Borland will suffer any financial loss, and the benefit from the community will be good. On the other hand, if Borland takes the hard line.. well we have supported Borland so much in the past this is how we are treated. So we will start switching to other systems. How about Lazarus, anyone? Lazarus will replace Delphi.<br><br><br>
				<a id="Comments.ascx_CommentList__ctl6_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl6$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4307">#</a>&nbsp;<a name="4307"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 8:03 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl7_NameLink" href="http://www.poptechserv.com" target="_blank">Theodore W. Dennis</a>
			</h4>
			<p>
				I want to address the issue of Borland not being innovative.  In my mind, Borland is the innovator.  Borland may not always get the credit for the technologies that are produced, but we see in other companies efforts that seem to gravitate toward what Borland has already proposed.  I do not necessarily consider everything that Borland produces as &quot;Proprietary&quot;.  To me, everything that the industry produces as a whole is proprietary until it becomes widely accepted.  After it becomes widely accepted, it then becomes a standard.  Ask other programmers (specifically those that have been using Microsoft development tools) if they are able to leverage their investment in a language and move it to a new platform (e.g. Linux or .NET)? That, my friends, is a perfect example of the desire to be innovative.  When I have attempted to explain this approach to other non-Delphi programmers, at first I get a &quot;deer-in-the-headlights&quot; stare.  As I continue to explain, they seem to understand.  Whether or not they truly understand I will never know, but I know that I can speak from experience.
				<a id="Comments.ascx_CommentList__ctl7_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl7$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4308">#</a>&nbsp;<a name="4308"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 8:06 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl8_NameLink" target="_blank">Sebastian Ledesma</a>
			</h4>
			<p>
				Hi Danny: <br>Just a question: If compile WineLib with a Borland compiler there is no patent violation? <br>So now I only need a BCC32 for Linux :-) (and a BCC64 for Linux on a AMD64 :-))) )<br>Saludos<br>Sebastian
				<a id="Comments.ascx_CommentList__ctl8_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl8$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4310">#</a>&nbsp;<a name="4310"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 9:47 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl9_NameLink" target="_blank">Wayne Niddery</a>
			</h4>
			<p>
				To those calling Borland that &quot;bad guy&quot; ...<br><br>While there are some software patents that clearly should never have been granted, that does not invalidate all software patents. A proper patent protects a particular implementation of an idea, not the idea itself. Exception handling is an idea. That Borland has a patent on one implementation of EH does not prevent others from developing alternative EH implementations. Indeed, as Danny noted, gcc already implements EH, it's just not as good an implementation.<br><br>Far from stifling progress, patents allow such implementations to become public *sooner* - available to anyone that wants it in return for a little profit or other considerations for the patent owner. Without patents, most owners of such solutions would keep them secret as long as possible so no-one else could benefit directly. There would be constant intellectual war with everyone trying to discover each others' secrets, instead of civilized trade for mutual benefit and profit.<br><br>If you wish to give your great ideas and innovations away freely for the &quot;public good&quot;, you are free to do so, no-one will stop you. <br><br>Patents (and copyrights) are merely a way to turn intellectual effort into tangible goods that can be traded. Thus, railing against patents, as such, is to claim that one should not be able to profit from one's intellectual abilities. Carried to its logical conclusion, that would mean all programmers should only be paid according to their degree of physical labour - pressing keys - and not for their intellectual knowledge, efforts, or innovations.<br>
				<a id="Comments.ascx_CommentList__ctl9_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl9$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: I asked Borland" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4312">#</a>&nbsp;<a name="4312"></a>I asked Borland
					<span>
						5/13/2005 11:29 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl10_NameLink" href="http://www.reactos.org" target="_blank">Steven Edwards</a>
			</h4>
			<p>
				I asked Borland about this years ago and I was told a rough estimate on licensing costs that was far beyond what any free software project could pay. Being a Wine and ReactOS its not like I am flush with cash to try and pay licenses for every patent under the Sun and clearly not patents that site prior art such as this one.
				<a id="Comments.ascx_CommentList__ctl10_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl10$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4313">#</a>&nbsp;<a name="4313"></a>re: Software Patents Stifle Open Source?
					<span>
						5/13/2005 11:37 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl11_NameLink" target="_blank">Fred Weller</a>
			</h4>
			<p>
				So the question remains - just what would Borland do if said Winelib folks &quot;rang the doorbell&quot;? I hear a lot of splatter here but no one seems to have addressed the original question. There is a lot of chatter about the angelic goodness/Demonic evilness of software patents - folks, it's what we have - deal with it. Since the infrastructure is already in place, what would Borland do? Offer an inexpensive license, offer it for free for the exclusive use of Winlib or clutch it to their collective bosom in a display of miserlyness that shames the redoubtable Mr. Scrooge? I, for one, don't really have a problem with software patents, it's one of the ways that a company can make money - as I like Delphi and want Borland to stay solvent, if they have to charge a little for their intellectual property, so be it. I do - &quot;Free as in Beer&quot; software won't pay my bills, it won't pay theirs either. The comment by Wayne Niddery sums up the situation very nicely. I agree too with Danny - ask before pointing fingers and decrying bitterly, eh? One of the first signs of the downfall of a civilization is a lack of civility - a little politeness goes a long way.<br>
				<a id="Comments.ascx_CommentList__ctl11_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl11$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: The obvious questions unasked" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4318">#</a>&nbsp;<a name="4318"></a>The obvious questions unasked
					<span>
						5/13/2005 3:27 PM
					</span>
				<a id="Comments.ascx_CommentList__ctl12_NameLink" target="_blank">Shawn Stamps</a>
			</h4>
			<p>
				&quot;at considerable expense&quot;<br><br>Can you quantify this expense? $100? $10,000?, $1,000,000?<br><br>Does this implementation truly pass the litmus of &quot;non-trivial&quot; or &quot;non-obvious to those with ordinary skill in the field&quot;?<br><br>Beyond the obvious and common stupidity of most software and *grimace* &quot;business process&quot; patents, what true &quot;innovation&quot; is the patent protecting? One of my biggest pet peeves is that many of the innovations that are claimed by many software patents are &quot;natural&quot; conclusions drawn from simple exercises in inductive or deductive logic. As a programmer, I do that every day. I have made thousands upon thousands of similar &quot;innovations&quot;. Some more complex than others. Should I be patenting everything that I come up with that is nothing more than a well-considered solution to a problem specification? Should every other programmer do that, too? Pretty soon, we all could have massive patent portfolios that we all could constantly beat each other up with on a daily basis, instead of focusing on what our real jobs are: solving problems in code, BUILDING ON what those who came before us did.<br><br>Software patents are nothing more than a stupid power grab by those with money to protect it, and they need to go away. Either that, or everything that is invented by anyone needs to be naturally patented, just like natural copyrights. The playing field is not level.<br><br>Borland, take some of that $100mil &quot;Defense Fund&quot; and apply it to the fight for IP reform. Ultimately, it is in your own best interests as well as everyone else's, ESPECIALLY your customers.<br><br>Shawn Stamps<br>Pascal/Delphi Developer since TP 1.0<br>
				<a id="Comments.ascx_CommentList__ctl12_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl12$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4319">#</a>&nbsp;<a name="4319"></a>re: Software Patents Stifle Open Source?
					<span>
						5/14/2005 4:34 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl13_NameLink" target="_blank">Gerasimos Melissaratos</a>
			</h4>
			<p>
				First and formemost, I'mm definitely against software patents. But in this situation, the question should be what is Borland's policy on the software patents it owns and OSS. I believe IBM has a publicly stated policy (I think it's &quot;we won't sue if it's used in an OSS project&quot;). What is Borland's policy? Should every OSS developer who wants to use a Borland patented technique go and ring the bell? Not exactly practical, I would say. Maybe Borland will use its patent portfolio only as a defensive measure against a suit, but how am I supposed to know that?
				<a id="Comments.ascx_CommentList__ctl13_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl13$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4322">#</a>&nbsp;<a name="4322"></a>re: Software Patents Stifle Open Source?
					<span>
						5/14/2005 9:21 AM
					</span>
				<a id="Comments.ascx_CommentList__ctl14_NameLink" target="_blank">Hubert Lepicki</a>
			</h4>
			<p>
				Well, you say that &quot;patents suck&quot;. I agree. But what Borland should do about this? Maybe you guys should consider changing your patent policy - first of all - open your patents for open source projects, and then - lobby _against_ software patents. When I look to the past, I see Borland being _very_ innovating when software patents were not so widely used. Nowdays, you guys still make good products but you must always be careful not to break any patent (which is a &quot;science-fiction&quot; if you consider that deeply). So if there were no software patents it would be better for Borland, especially while you make tools for programmers. No software patents = more freedom for programmers = more innovation = more software on the market = more copies of Borland's software sold :)
				<a id="Comments.ascx_CommentList__ctl14_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl14$EditLink','')"></a>
			</p>
		
			<h4>
				<a title="permalink: re: Software Patents Stifle Open Source?" href="http://blogs.borland.com/dcc/archive/2005/05/12/4294.aspx#4323">#</a>&nbsp;<a name="4323"></a>re: Software Patents Stifle Open Source?
					<span>
						5/14/2005 12:22 PM
					</span>
				<a id="Comments.ascx_CommentList__ctl15_NameLink" href="http://locut.us/~ian/blog/" target="_blank">Ian Clarke</a>
			</h4>
			<p>
				The solution here, if Borland really wants to be the good guy, is for Borland to commit, in a legally binding way, never to use a software patent except to defend itself against patent infringement suits.  Your suggestion that open source projects simply &quot;ring the doorbell&quot; and ask permission misses the point, that shouldn't be necessary if Borland genuinely sees software patents only as a defensive tool.
				<a id="Comments.ascx_CommentList__ctl15_EditLink" href="javascript:__doPostBack('Comments.ascx$CommentList$_ctl15$EditLink','')"></a>
			</p>
		
</div>
<div id="commentform">
<TABLE cellSpacing="1" cellPadding="1"  border="0" >
	<TR>
		<TD width="75">Title</TD>
		<TD>
			<input name="PostComment.ascx:tbTitle" type="text" value="re: Software Patents Stifle Open Source?" id="PostComment.ascx_tbTitle" Size="40" /></TD>
		<TD>
			&nbsp;</TD>
	</TR>
	<TR>
		<TD width="75">Name</TD>
		<TD>
			<input name="PostComment.ascx:tbName" type="text" id="PostComment.ascx_tbName" Size="40" /></TD>
		<TD>
			&nbsp;</TD>
	</TR>
	<TR>
		<TD>Url</TD>
		<TD>
			<input name="PostComment.ascx:tbUrl" type="text" id="PostComment.ascx_tbUrl" Size="40" /></TD>
		<TD></TD>
	</TR>
	<TR>
		<TD colSpan="3">Comments&nbsp;
			&nbsp;<BR>
			<textarea name="PostComment.ascx:tbComment" rows="10" cols="50" id="PostComment.ascx_tbComment"></textarea></TD>
	</TR>
	<TR>
		<TD colSpan="3">
			</TD>
	</TR>
	<TR>
		<TD>
			<input type="submit" name="PostComment.ascx:btnSubmit" value="Submit" onclick="if (typeof(Page_ClientValidate) == 'function') Page_ClientValidate(); " language="javascript" id="PostComment.ascx_btnSubmit" /></TD>
		<TD colspan="2">
			<span id="PostComment.ascx_Message"><font color="Red"></font></span></TD>
	</TR>
</TABLE>
</div>
				
</div>

<p id="footer">
	Powered by: 
	<br />
	<a id="Footer1_Hyperlink2" NAME="Hyperlink1" href="http://scottwater.com/blog"><img src="../../../../../images/100x30_Logo.gif" border="0" /></a>
	<a id="Footer1_Hyperlink3" NAME="Hyperlink1" href="http://ASP.NET"><img src="../../../../../images/PoweredByAsp.Net.gif" border="0" /></a>
	<br />
	Copyright &copy; Danny Thorpe
</p>
</form>
	</body>
</HTML>