<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 61 Parlamentarier drängen auf Rückkehr zur ersten Lesung

#descr: %(tp|61 Mitglieder des Europäischen Parlaments|MdEP) aus 13 Ländern
haben einen Entschließungsantrag unterzeichnet, der eine neue erste
Lesung der Softwarepatentrichtlinie fordert.  Der Antrag erfährt
besonders nachdrückliche Unterstützung von Abgeordneten aus Polen und
anderen neuen Mitgliedsstaaten.  Andererseits begrüßen auch
altgediente Abgeordnete die Initiative als einen Weg aus einer Krise,
in die der EU-Rat das Richtlinienprojekt gestürzt hat, indem er sich
weigerte, eine ordentliche erste Lesung durchzuführen.

#kWa: Hintergrundinformation

#dca: Medienkontakte

#otF: Über den FFII

#nWW: Dauerhafte Netzadresse dieser Presseerklärung

#sst: Der Entschließungsantrag wurde von %(JB) (Christdemokraten, früherer
polnischer Premierminister) und %(AG) (Sozialisten, Polen) initiiert. 
Ihm zufolge sind zwei Bedingungen erfüllt, von denen jede für sich
genommen gemäß den Verfahrensregeln des Parlamentes eine Rückkehr zur
ersten Lesung erlaubt.

#Wlh: Erstens haben sich seit der ersten Lesung die Umstände wesentlich
geändert.  Hier verweist der Antrag auf neuere Sorgen über
Patentrisiken öffentlicher Verwaltungen und Dienstleistern auferlegte
Freistellungspflichten.  Zweitens hat sich das Parlament selber durch
Neuwahlen geändert, bei denen 10 Länder erstmals teilnahmen.

#dee: Unter den Unterzeichnern befinden sich viele prominente MdEP, u.a. ein
ehemaliger EU-Kommissar, mehrere Vizepräsidenten des EP,
Vize-Vorsitzende mehrerer Gruppen (Fraktionen), Büromitglieder
mehrerer Gruppen und eine Vielzahl von Vorsitzenden und
Vizevorsitzenden verschiedener Ausschüsse, einschließlich des für die
Richtlinie zuständigen Rechtsausschusses.  Viele weitere MdEP wollten
unterzeichnen aber konnten aus Zeitmangel nicht aufgenommen werden, da
der Antrag in Eile eingereicht werden musste.  Gemäß den
Verfahrensregeln des Parlamentes muss die Frage der %(q:erneuten
Verweisung nach Regel 55) innerhalb weniger Stunden oder Tage
entschieden werden, wenn eine Richtlinie zum Parlament zurückkehrt.

#oWt: Der derzeit offizielle Vorschlag des EU-Rates vom 18. Mai 2004 wurde 
von der %(q:Arbeitsgruppe für %(tp|Geistiges Eigentum|Patente))
ausgearbeitet.  Diese Gruppe besteht aus ziemlich genau den Leuten,
die auch im Verwaltungsrat des Europäischen Patentamtes sitzen: den
für die nationalen Patentämter zuständigen Ministerialbeamten.
Zusammen mit ihren Kollegen aus der %(ci:Dienststelle für Gewerblichen
Rechtsschutz) in der Euorpäischen Kommission haben sie es verstanden, 
eine %(q:politische Vereinbarung) auf ministerieller Ebene
durchzusetzen.  Dabei zogen sie alle Register der %(dc:Täuschung) und
der %(co:Verfahrenstricks).  In den folgenden Monaten verabschiedeten
mehrere Parlamente Entschließungsanträge, die diese Vereinbarung
kritisieren, und mehrere angeblich unterstützende nationale Parlamente
distanzierten sich von ihr.  Obwohl es klar ist, hinter dieser
Vereinbarung keine qualifizierte Mehrheit steht, wurden die
unzufriedenen Regierungen mithilfe ungeschriebener diplomatischer
Gesetze davon abgehalten, die Diskussion neu zu eröffnen.  Dieses
Vorgehen warf %(kl:beunruhigende Fragen) über die %(eu:demokratische
Legitimität der Gesetzgebung auf EU-Ebene) auf.

#ZWt: Die Rückkehr zur ersten Lesung wurde zunächst von MdEP Olga Zrihen
(SPE, Belgien) am 14. April 2004 in einem Interview ins Gespräch
gebracht.  Zrihen und Kollegen sahen darin eine mögliche Antwort auf
die Weigerung des Rates, sich mit den Parlamentsvorschlägen
auseinanderzusetzen.  Sogar Abgeordnete, die zuvor einen ähnlich
softwarepatent-freundlichen Ansatz wie der Rat befürwortet hatten, wie
%(AM), kritisierten die Haltung des Rates in deutlichen Worten. 
McCarthy etwa betonte wiederholt, dass die Debatte inzwischen
fortgeschritten sei und ein Wiederaufkochen bereits abgelehnter
Rezepte (%(q:more of the same)) keine konstruktive Antwort sein könne.

#oCd: Dokument der Kommission über %(q:Computer-Implementierte Erfindungen)

#gtc: erwähnt Meinungsunterschiede, spricht offen über %(q:Softwarepatente)
und den Wunsch des Rates und der Kommission nach deren Legalisierung
und daüber, dass der Rat sämtliche wesentliche Änderungsanträge des
Parlaments abgelehnt hat, weist darauf hin, dass das Europaparlament
zu einer erneuten ersten Lesung zurückkehren kann.

#tWn: Im Juli 2004 %(pr:erklärte) die Europäische Kommission, dass das
Parlament eine Rückkehr zur ersten Lesung wünschen könnte:

#ecc: Der geänderte Richtlinienvorschlag wird im September 2004 im Parlament
in zweiter Lesung beraten. Die neu gewählten Abgeordneten werden zu
entscheiden haben, ob sie die erste Lesung ihrer Vorgänger bestätigen
oder aber neue Änderungen beschließen. Das Parlament kann ebenfalls
beschließen, die Beratungen von neuem zu beginnen, und also erneut
eine erste Lesung anzusetzen.

#tmo: Bei einer erneuten ersten Lesung wird das Europäische Parlament unter
dem Berichterstatter Michel Rocard Zeit haben, seinen früheren Text zu
überarbeiten.  Anders als bei einer zweiten Lesung wird es die
Gelgenheit erhalten, einen neuen Vorschlag der Kommission mit einer
Mehrheit aller Anwesender (und nicht aller Abgeordneter) zu ändern,
und es wird nicht nur 3 oder 4 Monate zur Erreichung eines Konsenses
haben.  Danach wird der Rat eine neue Gelegenheit erhalten, frei von
diplomatischen Zwängen eine angemessene Antwort auf den
Parlamentsvorschlag auszuarbeiten.

#umn: Der FFII unterstützt den Neustart-Antrag mit einer
%(wd:Bannerkampange).

#MeW: Jonas Maebe, Vorstandsmitglied des FFII, kommentiert:

#Wid: Ein Neustart wäre ein Gewinn für all diejenigen, die an einem auf
Sachlichkeit und demokratische Legitimität statt auf diplomatische
Zwänge gegründeten Text interessiert sind.  Die neuen Mitgliedsstaaten
erhalten damit die Chance, von Anfang mitzubestimmen, und der Rat wird
dann hoffentlich nicht mehr diejenigen mit der Ausarbeitung seines
Gegenvorschlages beauftragen, die das europäische Patentamt verwalten.

#Mto: der Entschließungsantrag

#Wri: Unterzeichner nach Nationalität

#oec: Unterzeichner nach Parteigruppierung

#oao: Pressemitteilung von Nosoftwarepatents.com

#ttn: %(q:Computer-Implemente Erfindung) = Software auf einem gewöhnlichen
Universalrechner

#iPg: Rücknahme der Politischen Vereinbarung

#sMn: enthält Stellungnahmen von MdEP über die Haltung des Rates

#tte: enthält weitere Aussagen

#nrt: diese Pressemitteilung als schlichter Text

#WuW: zur Verteilung per E-Mail

#Ceo: Weitere Kontakte auf Anfrage

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/restart0501.el ;
# mailto: phm@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: restart0501 ;
# txtlang: de ;
# multlin: t ;
# End: ;

