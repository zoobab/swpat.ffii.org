\begin{subdocument}{restart0501}{61 Parlamentarier dr\"{a}ngen auf R\"{u}ckkehr zur ersten Lesung}{http://swpat.ffii.org/log/05/restart01/index.de.html}{Guppe\\\url{swpatag@ffii.org}\\deutsche Version 2005/02/05 von PILCH Hartmut\footnote{\url{http://www.ffii.org/\~phm}}}{61 Mitglieder des Europ\"{a}ischen Parlaments (MdEP) aus 13 L\"{a}ndern haben einen Entschlie{\ss}ungsantrag unterzeichnet, der eine neue erste Lesung der Softwarepatentrichtlinie fordert.  Der Antrag erf\"{a}hrt besonders nachdr\"{u}ckliche Unterst\"{u}tzung von Abgeordneten aus Polen und anderen neuen Mitgliedsstaaten.  Andererseits begr\"{u}{\ss}en auch altgediente Abgeordnete die Initiative als einen Weg aus einer Krise, in die der EU-Rat das Richtlinienprojekt gest\"{u}rzt hat, indem er sich weigerte, eine ordentliche erste Lesung durchzuf\"{u}hren.}
\begin{sect}{story}{Geschichte}
Der Entschlie{\ss}ungsantrag wurde von Jerzy Buzek\footnote{\url{http://wwwdb.europarl.eu.int/ep6/owa/whos_mep.data?ipid=0&ilg=EN&iucd=28269&ipolgrp=PPE-DE&ictry=PL&itempl=&ireturn=&imode=}} (Christdemokraten, fr\"{u}herer polnischer Premierminister) und Adam Gierek\footnote{\url{http://wwwdb.europarl.eu.int/ep6/owa/whos_mep.data?ipid=0&ilg=EN&iucd=28379&ipolgrp=PSE&ictry=PL&itempl=&ireturn=&imode=}} (Sozialisten, Polen) initiiert.  Ihm zufolge sind zwei Bedingungen erf\"{u}llt, von denen jede f\"{u}r sich genommen gem\"{a}{\ss} den Verfahrensregeln des Parlamentes eine R\"{u}ckkehr zur ersten Lesung erlaubt.

Erstens haben sich seit der ersten Lesung die Umst\"{a}nde wesentlich ge\"{a}ndert.  Hier verweist der Antrag auf neuere Sorgen \"{u}ber Patentrisiken \"{o}ffentlicher Verwaltungen und Dienstleistern auferlegte Freistellungspflichten.  Zweitens hat sich das Parlament selber durch Neuwahlen ge\"{a}ndert, bei denen 10 L\"{a}nder erstmals teilnahmen.

Unter den Unterzeichnern befinden sich viele prominente MdEP, u.a. ein ehemaliger EU-Kommissar, mehrere Vizepr\"{a}sidenten des EP, Vize-Vorsitzende mehrerer Gruppen (Fraktionen), B\"{u}romitglieder mehrerer Gruppen und eine Vielzahl von Vorsitzenden und Vizevorsitzenden verschiedener Aussch\"{u}sse, einschlie{\ss}lich des f\"{u}r die Richtlinie zust\"{a}ndigen Rechtsausschusses.  Viele weitere MdEP wollten unterzeichnen aber konnten aus Zeitmangel nicht aufgenommen werden, da der Antrag in Eile eingereicht werden musste.  Gem\"{a}{\ss} den Verfahrensregeln des Parlamentes muss die Frage der ``erneuten Verweisung nach Regel 55'' innerhalb weniger Stunden oder Tage entschieden werden, wenn eine Richtlinie zum Parlament zur\"{u}ckkehrt.

Der derzeit offizielle Vorschlag des EU-Rates vom 18. Mai 2004 wurde  von der ``Arbeitsgruppe f\"{u}r Geistiges Eigentum (Patente)'' ausgearbeitet.  Diese Gruppe besteht aus ziemlich genau den Leuten, die auch im Verwaltungsrat des Europ\"{a}ischen Patentamtes sitzen: den f\"{u}r die nationalen Patent\"{a}mter zust\"{a}ndigen Ministerialbeamten. Zusammen mit ihren Kollegen aus der Dienststelle f\"{u}r Gewerblichen Rechtsschutz\footnote{\url{http://swpat.ffii.org/akteure/cec/index.fr.html}} in der Euorp\"{a}ischen Kommission haben sie es verstanden,  eine ``politische Vereinbarung'' auf ministerieller Ebene durchzusetzen.  Dabei zogen sie alle Register der \celsius{T\"{a}uschung} und der Verfahrenstricks.  In den folgenden Monaten verabschiedeten mehrere Parlamente Entschlie{\ss}ungsantr\"{a}ge, die diese Vereinbarung kritisieren, und mehrere angeblich unterst\"{u}tzende nationale Parlamente distanzierten sich von ihr.  Obwohl es klar ist, hinter dieser Vereinbarung keine qualifizierte Mehrheit steht, wurden die unzufriedenen Regierungen mithilfe ungeschriebener diplomatischer Gesetze davon abgehalten, die Diskussion neu zu er\"{o}ffnen.  Dieses Vorgehen warf beunruhigende Fragen\footnote{\url{http://k.lenz.name/LB/archives/000966.html}} \"{u}ber die demokratische Legitimit\"{a}t der Gesetzgebung auf EU-Ebene\footnote{\url{http://wiki.ffii.org/EuDemocracyDe}} auf.

Die R\"{u}ckkehr zur ersten Lesung wurde zun\"{a}chst von MdEP Olga Zrihen (SPE, Belgien) am 14. April 2004 in einem Interview ins Gespr\"{a}ch gebracht.  Zrihen und Kollegen sahen darin eine m\"{o}gliche Antwort auf die Weigerung des Rates, sich mit den Parlamentsvorschl\"{a}gen auseinanderzusetzen.  Sogar Abgeordnete, die zuvor einen \"{a}hnlich softwarepatent-freundlichen Ansatz wie der Rat bef\"{u}rwortet hatten, wie Arlene McCarthy\footnote{\url{http://swpat.ffii.org/akteure/amccarthy/index.en.html}}, kritisierten die Haltung des Rates in deutlichen Worten.  McCarthy etwa betonte wiederholt, dass die Debatte inzwischen fortgeschritten sei und ein Wiederaufkochen bereits abgelehnter Rezepte (``more of the same'') keine konstruktive Antwort sein k\"{o}nne.

Im Juli 2004 erkl\"{a}rte\footnote{\url{http://europa.eu.int/scadplus/leg/de/lvb/l26090.htm}} die Europ\"{a}ische Kommission, dass das Parlament eine R\"{u}ckkehr zur ersten Lesung w\"{u}nschen k\"{o}nnte:

\begin{quote}
{\it Der ge\"{a}nderte Richtlinienvorschlag wird im September 2004 im Parlament in zweiter Lesung beraten. Die neu gew\"{a}hlten Abgeordneten werden zu entscheiden haben, ob sie die erste Lesung ihrer Vorg\"{a}nger best\"{a}tigen oder aber neue \"{A}nderungen beschlie{\ss}en. Das Parlament kann ebenfalls beschlie{\ss}en, die Beratungen von neuem zu beginnen, und also erneut eine erste Lesung anzusetzen.}
\end{quote}

Bei einer erneuten ersten Lesung wird das Europ\"{a}ische Parlament unter dem Berichterstatter Michel Rocard Zeit haben, seinen fr\"{u}heren Text zu \"{u}berarbeiten.  Anders als bei einer zweiten Lesung wird es die Gelgenheit erhalten, einen neuen Vorschlag der Kommission mit einer Mehrheit aller Anwesender (und nicht aller Abgeordneter) zu \"{a}ndern, und es wird nicht nur 3 oder 4 Monate zur Erreichung eines Konsenses haben.  Danach wird der Rat eine neue Gelegenheit erhalten, frei von diplomatischen Zw\"{a}ngen eine angemessene Antwort auf den Parlamentsvorschlag auszuarbeiten.

Der FFII unterst\"{u}tzt den Neustart-Antrag mit einer Bannerkampange\footnote{\url{http://demo.ffii.org/restart0501/}}.
\end{sect}

\begin{sect}{koments}{Kommentare}
\begin{sect}{jmaebe}{Jonas Maebe}
Jonas Maebe, Vorstandsmitglied des FFII, kommentiert:

\begin{quote}
{\it Ein Neustart w\"{a}re ein Gewinn f\"{u}r all diejenigen, die an einem auf Sachlichkeit und demokratische Legitimit\"{a}t statt auf diplomatische Zw\"{a}nge gegr\"{u}ndeten Text interessiert sind.  Die neuen Mitgliedsstaaten erhalten damit die Chance, von Anfang mitzubestimmen, und der Rat wird dann hoffentlich nicht mehr diejenigen mit der Ausarbeitung seines Gegenvorschlages beauftragen, die das europ\"{a}ische Patentamt verwalten.}
\end{quote}
\end{sect}
\end{sect}

\begin{sect}{links}{Hintergrundinformation}
\begin{itemize}
\item
{\bf {\bf der Entschlie{\ss}ungsantrag\footnote{\url{http://www.ffii.org/\~jmaebe/reso/resolution_55_4.pdf}}}}
\filbreak

\item
{\bf {\bf Unterzeichner nach Nationalit\"{a}t\footnote{\url{http://www.nosoftwarepatents.com/docs/050106restartsign_nat.pdf}}}}
\filbreak

\item
{\bf {\bf Unterzeichner nach Parteigruppierung\footnote{\url{http://www.nosoftwarepatents.com/docs/050106restartsign_grp.pdf}}}}
\filbreak

\item
{\bf {\bf Pressemitteilung von Nosoftwarepatents.com\footnote{\url{http://www.nosoftwarepatents.com/phpBB2/viewtopic.php?t=282}}}}
\filbreak

\item
{\bf {\bf Future Developments concerning Software Patents\footnote{\url{http://swpat.ffii.org/log/zukunft/index.de.html}}}}
\filbreak

\item
{\bf {\bf ``Computer-Implemente Erfindung'' = Software auf einem gew\"{o}hnlichen Universalrechner\footnote{\url{http://wiki.ffii.org/CIIisSwDe}}}}
\filbreak

\item
{\bf {\bf R\"{u}cknahme der Politischen Vereinbarung\footnote{\url{http://wiki.ffii.org/ConsRevers04De}}}}
\filbreak

\item
{\bf {\bf Rat der EU will Parlamentsbeschluss ohne Diskussion verwerfen\footnote{\url{http://swpat.ffii.org/log/04/cons0507/index.de.html}}}}

\begin{quote}
enth\"{a}lt Stellungnahmen von MdEP \"{u}ber die Haltung des Rates
\end{quote}
\filbreak

\item
{\bf {\bf 2004-12-19 Klicken Sie hier, Frau K\"{u}nast: Netzaktion f\"{u}r parlamentarische Demokratie in der EU\footnote{\url{http://swpat.ffii.org/log/04/demo12/index.de.html}}}}

\begin{quote}
enth\"{a}lt weitere Aussagen
\end{quote}
\filbreak

\item
{\bf {\bf diese Pressemitteilung als schlichter Text\footnote{\url{restart0501.en.txt}}}}

\begin{quote}
zur Verteilung per E-Mail
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{Medienkontakte}
\begin{description}
\item[E-Post:]\ media at ffii org
\item[Telefon:]\ Hartmut Pilch +49-89-18979927 (Deutsch/Englisch/Franz\"{o}sisch)
Benjamin Henrion +32-498-292771 (Franz\"{o}sisch/Englisch)
Jonas Maebe +32-485-36-96-45 (Niederl\"{a}ndisch/Englisch/Franz\"{o}sisch)
Dieter Van Uytvanck +32-499-16-70-10 (Niederl\"{a}ndisch/Englisch/Franz\"{o}sisch)
Erik Josefsson +46-707-696567 (Schwedisch/Englisch)
James Heald +44 778910 7539 (Englisch)
Weitere Kontakte auf Anfrage
\end{description}
\end{sect}

\begin{sect}{ffii}{\"{U}ber den FFII -- www.ffii.org}
Der FFII ist ein in M\"{u}nchen eingetragener gemeinn\"{u}tziger Verein f\"{u}r Volksbildung im Bereich der Datenverarbeitung.  Der FFII unterst\"{u}tzt die Entwicklung \"{o}ffentlicher Informationsg\"{u}ter auf grundlage des Urheberrechts, freien Wettbewerbs und offener Standards.  \"{U}ber 500 Mitglieder, 1000 Firmen und 60000 Unterst\"{u}tzer haben den FFII mit der Vertretung ihre Interessen im Bereich der Gesetzgebung zu Software-Eigentumsrechten beauftragt.
\end{sect}

\begin{sect}{url}{Dauerhafte Netzadresse dieser Presseerkl\"{a}rung}
http://swpat.ffii.org/log/05/restart01/index.de.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/restart0501.el ;
% mode: latex ;
% End: ;

