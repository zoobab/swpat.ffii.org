<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#kWa: Background information

#dca: Media contacts

#otF: About the FFII

#nWW: Permanent URL of this Press Release

#sst: The motion, initiated by %(JB) (Christian Democrats, former prime
minister of Poland) and %(AG) (Socialists, Poland), sees two
conditions fulfilled, each of which allows a return to first reading
according to the procedural rules of the European Parliament.

#Wlh: The first reason is that since the previous first reading, the
situation has substantially changed.  Here the motion cites recent
concerns about patent risks for public administrations and
indemnification obligations imposed on service providers.  Secondly,
the Parliament itself changed by elections, in which 10 countries
voted for the first time.

#dee: Among the signatories are many prominent MEPs, including a former
Commissioner, several vice presidents of the EP, vice chairmen of
several groups, members of the bureau of several groups and a
multitude of (main and vice) chairmen and chairwomen of various
committees (including the JURI Committee, which is responsible for
this directive in the European Parliament).  According to sources in
the EP, many more MEPs wanted to sign the motion but could not be
included because the motion had to be submitted in a hurry.  According
to the Parliament's rules of procedure, the question of %(q:renewed
referral according to rule 55) must be decided immediately after a
directive returns to the Parliament.

#oWt: The 18 May 2004 text of the Council was written by the Council's
secretive %(q:%(tp|Intellectual Property|Patents) Working Party),
which consists of more or less the same people who sit on the European
Patent Office's Administrative Council: the civil servants who run the
national patent offices.  Together with their colleagues from the
%(ci:European Commission's Industrial Property Unit), they managed to
bring about a %(q:political agreement) at the ministerial level by
%(dt:deceptive maneuvering).  In subsequent months, several national
parliaments passed resolutions that criticise this agreement, and
several allegedly supporting national governments distanced themselves
from it.  Although it is clear that the %(q:political agreement) does
not enjoy a qualified majority, unwritten laws of diplomacy were used
to prevent dissenting states from reopening discussions.  This raised
%(kl:worrying questions) about the %(eu:democratic legitimacy of
lawmaking the EU).

#ZWt: The option of return to 1st reading was first proposed by Olga Zrihen
MEP (PSE, Belgium) on April 14th 2004 in an interview as the
appropriate answer to the Council's intransigence.  Even MEPs who had
previously taken a very software-patent-friendly approach, such as
%(AM), sharply criticised the Council's failure to address the
Parliament's concerns, pointing out that the debate has progressed and
that %(q:more of the same) is not a constructive answer.

#oCd: Commission Document on %(q:Computer-Implemented Inventions)

#gtc: Mentions some conflicting opinions, speaks openly about %(q:software
patents) and the Council's friendliness toward these, says that the EP
can return to another 1st reading.

#tWn: In July 2004, the European Commission %(pr:suggested) that the
Parliament might want to return to a 1st reading:

#ecc: The amended proposal for a directive will be discussed by the
Parliament at a second reading in September 2004. The new MEPs will
need to decide whether to confirm the first reading adopted by their
predecessors or to vote for new amendments. Parliament may also decide
to re-examine the dossier from scratch, and thus return to a first
reading.

#tmo: In a new first reading, the European Parliament will have time to work
on its previous text, so as to provide a more compelling case to the
Council.  The Parliament will also be able to amend the Commission's
proposal with a majority of all votes (rather than of all MEPs) and
will not be limited to 3 or 4 months to reach a consensus, as would be
the case in a second reading.  Subsequently, the Council will have a
fresh opportunity to provide an adequate response to the Parliament,
free from diplomatic constraints.

#umn: FFII is supporting the restart motion with a %(wd:banner campaign).

#MeW: Jonas Maebe, board member of FFII, comments:

#Wid: A renewed referral would be a win for all parties that want a text
based on substance and democratic legitimacy, as opposed to one based
on diplomatic rigidity. New member states will be able to have their
say from the beginning, and hopefully the Council will decide that its
next first reading text should not be written by the same people that
rule the European Patent Office.

#Mto: Motion

#Wri: List of signatories by nationality

#oec: List of signatories by political group

#oao: Nosoftwarepatents.com PR

#ttn: %(q:Computer-Implemented Invention) = Software Running on
General-Purpose Computer

#iPg: Revision of the Political Agreement

#sMn: contains statements from MEPs about the Council's plans

#tte: contains further statements

#nrt: plain text version of this PR

#WuW: for distribution by e-mail

#Ceo: More Contacts to be supplied upon request

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/restart0501.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: restart0501 ;
# txtlang: en ;
# multlin: t ;
# End: ;

