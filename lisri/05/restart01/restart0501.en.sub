\begin{subdocument}{restart0501}{61 MEPs Urge Return to 1st Reading}{http://swpat.ffii.org/log/05/restart01/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2005/01/10 by FFII\footnote{\url{http://lists.ffii.org/mailman/listinfo/traduk}}}{A motion for a resolution, signed by 61 Members of the European Parliament (MEPs) from 13 countries, calls for a new first reading of the software patent directive. The motion has received particularly warm support from MEPs from Poland and other new member states. However, many veteran MEPs also welcome the initiative as a way out of a crisis, into which the Council has plunged the directive project by failing to conduct a proper first reading.}
\begin{sect}{story}{Story}
The motion, initiated by Jerzy Buzek\footnote{\url{http://wwwdb.europarl.eu.int/ep6/owa/whos_mep.data?ipid=0&ilg=EN&iucd=28269&ipolgrp=PPE-DE&ictry=PL&itempl=&ireturn=&imode=}} (Christian Democrats, former prime minister of Poland) and Adam Gierek\footnote{\url{http://wwwdb.europarl.eu.int/ep6/owa/whos_mep.data?ipid=0&ilg=EN&iucd=28379&ipolgrp=PSE&ictry=PL&itempl=&ireturn=&imode=}} (Socialists, Poland), sees two conditions fulfilled, each of which allows a return to first reading according to the procedural rules of the European Parliament.

The first reason is that since the previous first reading, the situation has substantially changed.  Here the motion cites recent concerns about patent risks for public administrations and indemnification obligations imposed on service providers.  Secondly, the Parliament itself changed by elections, in which 10 countries voted for the first time.

Among the signatories are many prominent MEPs, including a former Commissioner, several vice presidents of the EP, vice chairmen of several groups, members of the bureau of several groups and a multitude of (main and vice) chairmen and chairwomen of various committees (including the JURI Committee, which is responsible for this directive in the European Parliament).  According to sources in the EP, many more MEPs wanted to sign the motion but could not be included because the motion had to be submitted in a hurry.  According to the Parliament's rules of procedure, the question of ``renewed referral according to rule 55'' must be decided immediately after a directive returns to the Parliament.

The 18 May 2004 text of the Council was written by the Council's secretive ``Intellectual Property (Patents) Working Party'', which consists of more or less the same people who sit on the European Patent Office's Administrative Council: the civil servants who run the national patent offices.  Together with their colleagues from the European Commission's Industrial Property Unit\footnote{\url{http://localhost/swpat/players/cec/index.fr.html}}, they managed to bring about a ``political agreement'' at the ministerial level by deceptive maneuvering\footnote{\url{http://localhost/swpat/letters/cons0406/text/index.en.html}}.  In subsequent months, several national parliaments passed resolutions that criticise this agreement, and several allegedly supporting national governments distanced themselves from it.  Although it is clear that the ``political agreement'' does not enjoy a qualified majority, unwritten laws of diplomacy were used to prevent dissenting states from reopening discussions.  This raised worrying questions\footnote{\url{http://k.lenz.name/LB/archives/000966.html}} about the democratic legitimacy of lawmaking the EU\footnote{\url{}}.

The option of return to 1st reading was first proposed by Olga Zrihen MEP (PSE, Belgium) on April 14th 2004 in an interview as the appropriate answer to the Council's intransigence.  Even MEPs who had previously taken a very software-patent-friendly approach, such as Arlene McCarthy\footnote{\url{http://localhost/swpat/players/amccarthy/index.en.html}}, sharply criticised the Council's failure to address the Parliament's concerns, pointing out that the debate has progressed and that ``more of the same'' is not a constructive answer.

In July 2004, the European Commission suggested\footnote{\url{http://europa.eu.int/scadplus/leg/en/lvb/l26090.htm}} that the Parliament might want to return to a 1st reading:

\begin{quote}
{\it The amended proposal for a directive will be discussed by the Parliament at a second reading in September 2004. The new MEPs will need to decide whether to confirm the first reading adopted by their predecessors or to vote for new amendments. Parliament may also decide to re-examine the dossier from scratch, and thus return to a first reading.}
\end{quote}

In a new first reading, the European Parliament will have time to work on its previous text, so as to provide a more compelling case to the Council.  The Parliament will also be able to amend the Commission's proposal with a majority of all votes (rather than of all MEPs) and will not be limited to 3 or 4 months to reach a consensus, as would be the case in a second reading.  Subsequently, the Council will have a fresh opportunity to provide an adequate response to the Parliament, free from diplomatic constraints.

FFII is supporting the restart motion with a banner campaign\footnote{\url{http://demo.ffii.org/restart0501/}}.
\end{sect}

\begin{sect}{koments}{Comments}
\begin{sect}{jmaebe}{Jonas Maebe}
Jonas Maebe, board member of FFII, comments:

\begin{quote}
{\it A renewed referral would be a win for all parties that want a text based on substance and democratic legitimacy, as opposed to one based on diplomatic rigidity. New member states will be able to have their say from the beginning, and hopefully the Council will decide that its next first reading text should not be written by the same people that rule the European Patent Office.}
\end{quote}
\end{sect}
\end{sect}

\begin{sect}{links}{Background information}
\begin{itemize}
\item
{\bf {\bf Motion\footnote{\url{http://www.ffii.org/\~jmaebe/reso/resolution_55_4.pdf}}}}
\filbreak

\item
{\bf {\bf List of signatories by nationality\footnote{\url{http://www.nosoftwarepatents.com/docs/050106restartsign_nat.pdf}}}}
\filbreak

\item
{\bf {\bf List of signatories by political group\footnote{\url{http://www.nosoftwarepatents.com/docs/050106restartsign_grp.pdf}}}}
\filbreak

\item
{\bf {\bf Nosoftwarepatents.com PR\footnote{\url{http://www.nosoftwarepatents.com/phpBB2/viewtopic.php?t=282}}}}
\filbreak

\item
{\bf {\bf Future Developments concerning Software Patents\footnote{\url{http://localhost/swpat/log/future/index.en.html}}}}
\filbreak

\item
{\bf {\bf ``Computer-Implemented Invention'' = Software Running on General-Purpose Computer\footnote{\url{}}}}
\filbreak

\item
{\bf {\bf Revision of the Political Agreement\footnote{\url{}}}}
\filbreak

\item
{\bf {\bf EU Council Plans to Scrap Parliamentary Vote without Discussion\footnote{\url{http://localhost/swpat/log/04/cons0507/index.en.html}}}}

\begin{quote}
contains statements from MEPs about the Council's plans
\end{quote}
\filbreak

\item
{\bf {\bf ``Ministers of Agriculture, Please Click`` -- Netaction for Parliamentary Democracy in the EU\footnote{\url{http://localhost/swpat/log/04/demo12/index.en.html}}}}

\begin{quote}
contains further statements
\end{quote}
\filbreak

\item
{\bf {\bf plain text version of this PR\footnote{\url{restart0501.en.txt}}}}

\begin{quote}
for distribution by e-mail
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{media}{Media contacts}
\begin{description}
\item[mail:]\ media at ffii org
\item[phone:]\ Hartmut Pilch +49-89-18979927 (German/English/French)
Benjamin Henrion +32-498-292771 (French/English)
Jonas Maebe +32-485-36-96-45 (Dutch/English/French)
Dieter Van Uytvanck +32-499-16-70-10 (Dutch/English/French)
Erik Josefsson +46-707-696567 (Swedish/English)
James Heald +44 778910 7539 (English)
More Contacts to be supplied upon request
\end{description}
\end{sect}

\begin{sect}{ffii}{About the FFII -- www.ffii.org}
The Foundation for a Free Information Infrastructure (FFII) is a non-profit association registered in several European countries, which is dedicated to the spread of data processing literacy.  FFII supports the development of public information goods based on copyright, free competition, open standards.  More than 500 members, 1,200 companies and 75,000 supporters have entrusted the FFII to act as their voice in public policy questions concerning exclusion rights (intellectual property) in data processing.
\end{sect}

\begin{sect}{url}{Permanent URL of this Press Release}
http://localhost/swpat/log/05/restart01/index.en.html
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/restart0501.el ;
% mode: latex ;
% End: ;

