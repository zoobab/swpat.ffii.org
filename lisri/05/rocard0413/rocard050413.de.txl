<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Michel Rocard publie sa position sur la directive des brevets
logiciels

#descr: Le rapporteur du Parlement Européen, Michel Rocard, a publié son avis
sur la directive du brevet logiciel et expliqué les principes que ses
amendments vont suivre.  L'aproche de Rocard semble contenir tous les
ingrédients pour une directive qui fourni les clarifications
nécéssaires et qui fait ce que le conséil dit qu'il doit faire:
exclure les programmes d'ordinateur de la brevetabilité tout en
permettant le brevetage des inventions techniques (solutions
impliquant les forces de la nature), indépendemment de la question
s'ils sont contrôlés par logiciel ou non. Rocard propose de remplacer
le terme contradictoire %(q:invention mise en oevre par ordinateur)
par %(q:invention contrôlée par ordinateur) our %(q:invention assistée
par ordinateur).  Le rapport plein avec les amendments va être publié
directement après le débat du 21 Avril.  Les MPEs peuvent déposes
autres amendments avant le 6 Mai.  La commission votera le 20 Juin, le
plénière le 6 Juillet.

#JURI: Commission des affaires juridiques

#dotr: DOCUMENT DE TRAVAIL

#rea: sur la brevetabilité des inventions contrôlées par ordinateur

#rapp: Rapporteur

#uqx: Le conseil des ministres a enfin adopté une position commune sur la
brevetabilité des inventions mises en oeuvre par ordinateur pour
permettre que se tienne le débat en deuxième lecture. Cinq états
membres ont voté en faisant savoir par écrit qu'ils votaient pour
débloquer la procédure, mais qu'ils souhaitaient voir le texte modifié
par le Parlement. Notre désaccord du premier tour a été entendu.

#nus: Ce texte est essentiel aussi bien économiquement (quelques dizaines de
milliards d'euros annuels sont en jeu) que politiquement ou
philosophiquement : il s'agit du statut de la diffusion du savoir et
des idées dans la société.

#afn: C'est un texte court, mais portant sur une matière extrêmement
complexe. Depuis deux ans qu'il est en débat, il apparaît clairement
que dans la difficulté à trouver des solutions consensuelles, les
désaccords sur les définitions et les malentendus sont beaucoup plus
importants que les désaccords sur le fond.

#WWp: J'ai fait établir une note d'analyse du sujet précise et détaillée.
Elle est longue. Au moment où je vous écris cette lettre, je ne suis
pas sûr de pouvoir la faire traduire en anglais.

#far: J'espère pourtant vous la donner à tous en français et en anglais.
Mais en fait, pour le débat sans texte du 21 avril à Bruxelles, je
préfère, avant de déposer officiellement mes propositions
d'amendements, vous proposer de réfléchir ensemble au problème qui
nous est posé, et à son traitement intellectuel.

#rte: Car dans ce texte court, nous n'avons en fait que deux problèmes
sérieux, susceptibles de nourrir un conflit avec la Commission et le
Conseil : celui de la délimitation de ce qui est brevetable et de ce
qui ne l'est pas, et l'interopérabilité. Si le Parlement et finalement
le Conseil suivent les orientations que nous leur proposons, le
problème de l'interopérabilité se trouvera réglé de ce fait.

#ers: Il faut donc commencer par s'occuper de la délimitation. Quelle est la
question ? Elle résulte de la contradiction entre le système légal et
la tradition héritée d'une part, et les besoins de rémunération des
investissements et de sécurité de la grande industrie appuyés par les
dérives récentes de la brevetabilité aux Etats Unis, et dans une
moindre mesure à l'office européen des brevets, d'autre part.  Tous
nos systèmes légaux, et surtout la Convention sur le brevet européen
signée en 1973 à Munich établissent clairement que les logiciels ne
sont pas brevetables (art 52.2. de la CBE). Or il existe plus de
150000 brevets de ce type aux Etats Unis, sans base légale et de
l'ordre de 50000 à l'Office européen des brevets, à base juridique
incertaine et inégalement valides devant nos droits nationaux.

#1W1: Le développement foudroyant de l'informatique s'est étendu depuis
vingt ans à toutes les branches de nos industries et de nos services.
Au delà des usages professionnels, il n'y a plus un objet de
consommation courante qui ne comporte de logiciels intégrés :
voitures, téléphones portables, télévisions, magnétoscopes, machines à
laver, commandes d'ascenseurs, etc.

#dps: Tout cela coûte cher à mettre au point.  Il est normal, et
souhaitable, que l'industrie puisse breveter les résultats de ses
investissements pour en assurer la rémunération et les protéger de la
contrefaçon et de la concurrence déloyale.  La régulation des procédés
physiques mis en oeuvre au sein des inventions est un très ancien
problème : elle a pris d'innombrables formes, mécaniques ou
pneumatiques notamment. Mettre au point de telles régulations,
brevetables lorsqu'elles étaient elles-mêmes innovantes dans leur
réalisation, était extrêmement coûteux.

#ees: Les remplacer par des logiciels, dont le coût de développement et de
production est bien plus faible, représente une énorme économie.

#aec: Cela a poussé à leur multiplication.

#use: Mais un logiciel est d'une autre nature.

#sra: Il est de l'ordre de l'immatériel.

#WWb: En fait, un logiciel est la combinaison en une oeuvre originale d'un
ou plusieurs algorithmes, c'est à dire un ensemble de formules
mathématiques.

#aWt: Or comme l'a dit Albert Einstein, une formule mathématique n'est pas
brevetable.

#eeu: Elle est de l'ordre des idées, comme un livre, ou une alliance de
mots, ou un accord musical.

#rrr: Depuis des millénaires, le savoir s'est construit et diffusé par la
copie et l'amélioration, c'est-àdire par le libre accès aux idées.

#ioi: Le fait que les savoirs modernes, du moins ceux qui ont quelque
rapport avec la logique ou la quantification, puissent plus
commodément être exprimés sous la forme de logiciels ne doit en aucun
cas conduire à renoncer au principe du libre accès qui est le seul à
garantir la capacité buissonnante qu'a l'humanité de créer constamment
de nouveaux savoirs.

#e1e: La compatibilité entre ces deux exigences contradictoires est
recherchée depuis longtemps, et cette recherche est l'objet de la
Directive en cause.

#rWi: Le sens commun, comme la jurisprudence de certains offices de Brevets,
tendent à dire que ce qui est brevetable, c'est l'invention, et non
pas le logiciel qui peut être nécessaire à son contrôle.

#Wer: Les textes de référence comme la jurisprudence de l'OEB expriment
cette différence en parlant de %(q:contribution technique), jusque là
tout le monde est absolument d'accord.

#ten: Pour être brevetable en effet, une invention : 

#iee: doit être nouvelle ;

#tWd: doit n'être pas évidente ;

#reu: doit être susceptible d'application industrielle ;

#Wac: doit avoir un caractère technique.

#Wnc: Le caractère technique est défini comme la capacité à donner une
solution technique à un problème technique, c'est à dire appartenir à
un domaine technique et avoir un effet technique.

#oee: Mais le mot de « technique » n'est pas défini, si ce n'est par
%(q:l'emploi de moyens techniques) ou pire encore par la simple
nécessité de %(q:considérations techniques).

#ert: Cette tautologie a conduit à breveter tout ce qui participait à la
réalisation de l'invention, logiciel ou pas.

#MfW: En outre et surtout, l'article 52.2 de la Convention de Munich stipule
que les logiciels ne sont pas brevetables %(q:en tant que tels), ce
qui a conduit par dérive à l'interprétation manifestement fautive
qu'il y aurait une différence entre les logiciels en tant que tels et
les logiciels incorporés à une invention ou logiciels comme
inventions, brefs logiciels utiles, et par là brevetables.

#iWo: C'est ici que nous avons le devoir d'inno ver, que nous avons innové
en première lecture, et que les cinq ou six Etats Membres qui ont fait
état au Conseil de leur attente d'amélioration souhaitent que nous
trouvions une solution.

#Wt0: La rédaction du Parlement en première lecture était peut être un peu
sèche et a surpris.

#rsu: Mais de très nombreux entretiens et discussions, notamment avec les
représentants des industriels, ont confirmé que la voie de recherche
que nous avions explorée était la bonne.

#eie: Un logiciel, formulation d'une idée, est de l'ordre de l'immatériel.

#oiW: Le travail qu'il provoque à l'intérieur de l'ordinateur lui est
interne et n'est pas directement communicable à qui ou quoi que ce
soit.

#miu: Il faut pour que ce travail soit communicable et ait un effet qu'une
pièce se mette en mouvement, qu'un signal électrique, radio ou
lumineux se produise, qu'une information apparaisse sur un écran, ou
que se déclenche n'importe quel effet physique.

#lai: Ce qui à l'évidence est brevetable ce sont tous les capteurs d'une
part et tous les effecteurs de l'autre qui alimentent l'ordinateur en
information traitable par le logiciel et qui tirent de l'information
finalement produite par le logiciel dans son langage un effet physique
constituant la solution technique au problème technique posé.

#uet: La distinction que nous cherchons sépare donc le monde immatériel du
monde matériel ou plutôt du monde physique.

#eWl: Mais chacun de ces deux mots est quelque peu insuffisant pour couvrir
tout le champ nécessaire.

#egW: Matériel renvoie trop à la matière et pas à l'énergie, physique
appelle implicitement une qualité palpable.

#edW: La préférence de votre rapporteur va à la rédaction suivante, qui
prendrait place dans l'article de la Directive, celui qui précise les
définitions;

#udW: Domaine technique désigne un domaine industriel d'application
nécessitant l'utilisation de forces contrôlables de la nature pour
obtenir des résultats prévisibles dans le monde physique.

#ldn: Si l'on admet que même un simple signal, qu'il soit électrique, radio
ou lumineux, est composé d'énergie, cette formulation englobe toutes
les manières de capter l'information immatérielle produite par
l'ordinateur sous la conduite du logiciel pour produire un effet
perceptible et utilisable par une machine ou un homme.

#ste: Je crois cette définition englobante pour tous les besoins réels de
l'industrie, sauf bien sur celui qu'ont éprouvé quelques sociétés de
contrôler une chaîne de logiciels brevetés dépendant les uns des
autres pour interdire l'accès de la concurrence aux activités en aval
concernant l'industrie et l'invention en cause, ce qu'à l'évidence
nous avons le devoir d'empêcher.

#sss: Tous les autres problèmes et donc tous les autres amendements sont des
conséquences de ce choix initial.

#oWW: Je suggère à mes collègues de n'en traiter qu'après que nous ayons pu
nous mettre d'accord, ce qui est l'objet du débat du 21.

#Wtd: Afin que la directive puisse permettre le brevetage d'inventions
contrôlées par ordinateur tout en empêchant la brevetabilité des
logiciels, il sera nécessaire d'intervenir sur les points suivants :

#lla: afin de clarifier la portée de la directive, remplacement autant que
possible du terme %(q:invention mise en oeuvre par ordinateur) par le
terme %(q:invention contrôlée par ordinateur), ou encore %(q:assistée
par ordinateur), qui illustre bien mieux que le logiciel ne peut faire
partie des caractéristiques techniques des revendications de brevet ;

#cWW: définit ion claire du %(q:domaine technique), tant positive que
négative : d'une part, il devra être spécifié qu'un domaine technique
est un %(bc:domaine industriel d'application nécessitant l'utilisation
de forces contrôlables de la nature pour obtenir des résultats
prévisibles dans le monde physique), bornant ainsi la technique au
monde physique ; d'autre part, il faudra spécifier que le traitement
de l'information ne soit pas considéré comme un domaine technique au
sens du droit des brevets et à ce que les innovations en matière de
traitement de l'information ne constituent pas des inventions au sens
du droit des brevets ; définit io n de façon non tautologique de la
notion de contribution technique et d'activité inventive, et préciser
pour cette dernière que seules les caractérist iques techniques des
inventions devront être prises en compte lors de son évaluation ;

#aeW: descript ion de la forme des revendications de façon tant positive que
négative, afin que d'une part les revendications sur les inventions
contrôlées par ordinateur ne puissent porter que sur des produits ou
des procédés techniques, et d'autre part que les revendications de
logiciels, en eux-mêmes ou sur tout support, soient interdites ;

#eeW: pour assurer l'interopérabilité, renforcement de la confirmation des
droits découlant des articles 5 et 6 de la directive 91/250, par le
fait que lorsque le recours à une technique brevetée est nécessaire à
la seule fin d'assurer l'interopérabilité entre deux systèmes, ce
recours ne soit pas considéré comme une contrefaçon de brevet.

#Wtb: En fonction du débat du 21 avril, mes amendements seront mis au point
et disponibles très vite après.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/rocard050413.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: rocard050413 ;
# txtlang: xx ;
# End: ;

