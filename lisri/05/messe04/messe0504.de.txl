<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Hannover-Messe im Zeichen der Softwarepatente

#descr: Die Auseinandersetzung um die geplante EU-Richtlinie %(q:über die
Patentierbarkeit computer-implementierter Erfindungen) hat die
Hannover-Messe erreicht.  Verbände der Großindustrie und des
Mittelstandes und weitere Aussteller und Akteure machen aus sehr
unterschiedlicher Sicht auf das Thema aufmerksam.

#nMe: Positionen von ZVEI, BVMW, FFII, Patentervein u.a.

#gdu: Ergänzende Dokumente

#otk: Kontakt

#anc: Die HANNOVER MESSE gilt als das jährliche Schaustück der
Innovationskraft des deutschen Industrie.   Mehr als 70% der
Arbeitsplätze stellt in den meisten Branchen der Mittelstand. Ein
Schlüssel für die Stärke deutscher Unternehmer ist das im Ganzen
sichere rechtliche Umfeld. Auf diesem Horizont ziehen jedoch Wolken
auf.

#nWs: Dr. Heiner Flocke, Vorsitzender des %(pv:Patentverein e.V.), stellt
fest:

#evd: Wir sind besorgt über Missverständnisse in der deutschen
Wirtschaftspolitik.  Es wird ein regelrechter Kult um Innovation und
Patente betrieben, und auf Entscheiderebene glaubt man vielfach noch
immer, mehr Patente bedeuteten mehr Innovation.  So ist eine Flut von
Verbotsvorschriften entstanden, mit denen die alltägliche
Innovationsarbeit der Unternehmen behindert wird.

#jee: Sascha Frick, Geschäftsführer der %(EG), eines Schweizer Spezialisten
für Software im Bereich der Industrie-Automation, berichtet von seiner
Erfahrung mit Patenten bei einem Großprojekt zur Entwicklung einer
neuen Generation von Maschinensteuerungssoftware:

#iif: Patente sind ein Alptraum! Wir kämpfen derzeit mit einem halben
Dutzend Software-Patenten, etwa ein Patent auf die Verwendung von
TCP/IP in Spritzgussmaschinen, ein Patent auf die Verwendung von
Softwarekomponenten in Zusammenhang mit Plastikherstellungsmaschinen,
oder ein Patent, dessen einzige Erfindungsleistung in der Verwendung
eines Zeigegerätes, z.B. einer Maus, zur Bedienung von
Maschinensteuerungssoftware besteht, und so weiter.  Keines dieser
Patente repräsentiert irgend einen Wert, es geht nur um die Anwendung
elementarer Logik-Operationen auf bekannte naturwissenschaftliche
Zusammenhänge. In den letzten Monaten haben wir unzählige Stunden
damit verbracht, kryptische Texte zu entziffern, die irgendwelche
angeblichen Erfindungen beschreiben. In all diesen Texten konnten wir
nur eine einzige Erfindung erkennen: die große Geldtransfermaschine,
die der Industrie wertvolle Ressourcen stiehlt, um einen
kontinuierlichen Geldstrom zugunsten von Patentämtern und
Patentanwälten zu erzeugen.

#ing: Besonders heftig umkämpft ist die Frage, ob Software-Lösungen, im
Jargon des Europäischen Patentamtes seit 2000 auch
%(ci:computer-implementierte Erfindungen) genannt, patentierbar sein
sollen.

#ncW: Dr. Thomas Wünsche, Inhaber und Geschaeftsführer von %(EW), eines
Unternehmens fuer Automatisierungstechnik und industrielle
Kommunikation, erklärt hierzu:

#rmW: In unseren Tätigkeitsfeldern Automatisierungstechnik und Automotive
Elektronik fließt ein erheblicher Teil des Entwicklungsaufwands in die
Software. Etwa 80% unserer Entwickler sind Software-Entwickler, die
Innovationsfähigkeit unseres Unternehmens hängt wesentlich von
Software ab. Das Urheberrecht schützt uns in diesem Bereich in
angemessener Weise vor Nachahmung.  Wer das Urheberrecht durch
Reverse-Engineering und Nachimplementierung umgehen möchte, kommt in
so dynamischen Märkten wie unserem viel zu spät.

#usW: Was wir fuer unsere Innovationsfähigkeit brauchen, sind Schutzrechte
mit geringem Verwaltungsoverhead wie das Copyright. Patente auf
Software treiben den Verwaltungsaufwand nach oben, schaffen
zusätzliche Rechtsunsicherheit und laufen damit unserer
Innovationsfähigkeit diametral entgegen. Die anstehenden Risiken durch
Softwarepatente haben bereits jetzt dazu geführt, dass wir
Ausbildungsplätze nicht wie in den Vorjahren besetzen, um Kapazität
fuer Umgestaltungen zur Risikoreduktion freizuschaufeln. Wir wuerden
allerdings lieber ausbilden als uns mit Softwarepatenten und daraus
resultierenden Risiken zu beschäftigen.

#WdW: Der Richtlinienentwurf des Europäischen Parlaments vom September 2003
stellt nicht sicher, dass wir in unserer Innovationsfähigkeit
zukünftig nicht von Patenten eingeschränkt werden, aber er verhindert
die schlimmsten Auswüchse.  Technische Erfindungen wie das
vielzitierte Antiblockiersystem wären auch unter der
Parlamentsrichtlinie grundsätzlich patentierbar, wobei sich der
Schutzumfang allerdings auf den Umgang mit den Naturkräften an der
Peripherie der Datenverarbeitungsanlage beschränkt.  Wenn der Entwurf
des Ministerrats durchgeht, wird hingegen nahezu jeder
Verfahrensablauf patentierbar, und schon seine Beschreibung in Form
eines Computerprogramms wird zur Patentverletzung.  Das würde uns eine
Situation wie in den USA bringen, wo Konzerne kleinere Wettbewerber
mit der Masse eines Portfolios weitgehend trivialer Patente erdrücken
und produktlose Abkassierer ihr Heil in Patentverletzungsklagen
suchen.

#Wrd: Bisherige Studien und Umfragen des Berliner Wirtschaftsministeriums
bestätigen, dass Wünsche mit seiner Einschätzung nicht alleine ist. 
Deutsche KMUs wollen keine Softwarepatente: Auf einer Skala der
Umfrage des Bundeswirtschaftsministeriums von -3 bis +3 siedeln 1214
deutsche Unternehmen auf die Frage %(q:Haben Softwarepatente
beinträchtigende oder unterstützende Auswirkungen auf Ihre
Programmiertätigkeit oder erwarten Sie solche?) im Schnitt bei -2,58
an. Eine %(fg:frühere Umfrage) belegt, dass in der
%(q:Sekundärbranche) (d.h. programmierenden Unternehmen der
klassischen Industrien) das Interesse an Softwarepatenten ähnlich
gering ist wie in der %(q:Primärbranche).

#WWe: Hartmut Pilch, Vorsitzender des FFII e.V., der 1400 Unternehmen in
dieser Frage vertritt, erklärt:

#rin: %(q:Firmen wie Siemens kämpfen gerade auf dieser Messe für eine
gesetzliche Festschreibung der Patentierbarkeit von Software-Lösungen.
 Setzen sie sich damit durch, so würde die ganze Palette der
Patentameldungen rechtsbeständig, die etwa Siemens beim Europäischen
Patentamt eingereicht hat, so von reinen Geschäftsmethoden bis hin zu
%(q:medizintechnischen Verfahren) für die Buchführung in der
Arztpraxis.

#zss: Das Europäische Parlament hat in seiner ersten Lesung einige wichtige
Einschränkungen vorgeschlagen, die das schlimmste verhindern. Konzerne
wie Siemens wollen aber nichts verhindern.  Sie wollen ihr
Patent-Monopoly-Spiel auf den gesamten Bereich ausdehnen, der im Zuge
der Digitalisierung in ihr Visier gerät.  Eigentlich ist der Computer
eine Maschine zur Vereinfachung des Erfindens.  Auch Siemens löst
gerne Probleme auf Software-Ebene, weil das so einfach und bequem ist.
Aber zugleich möchten sie weiter patentieren, und zwar auch dort, wo
es nichts mehr zu patentieren gibt.  Zu diesem Zweck verbreiten die
Patentanwälte von %(Siemens), %(Volvo) und %(ft:anderen Konzernen)
Desinformationen über die Standpunkte des Ministerrats und des
Parlaments.  Sie tun so, als vertrete der Ministerrat das, was in
Wirklichkeit das Parlament vertritt. Denn sie wissen, dass bei den
meisten Unternehmen nur die Position des Parlaments Zustimmung findet.

#WSa: Auf dem Stand des %(bv:Bundesverbands mittelständische Wirtschaft) auf
der Hannover Messe Industrie in Halle 2 Stand D 15/2 liegt
Informationmaterial (Plakat und Flyer) aus, welches auf gravierende
Nachteile von Softwarepatenten für die mittelständische Wirtschaft
hinweist.  Der %(zv:Zentralverband für die Elektronische Industrie)
hingegen veranstaltet an seinem Stand am Dienstag nachmittag eine
Pressekonferenz mit Botschaften im Sinne der Siemens-Position.

#ate: Patentverein

#Muc: EMS Wuensche

#WfW: BMWA-Umfrage 2004

#ig0: BMWi-Umfragen 2000-2003

#etm: Der FFII ist ein in München eingetragener gemeinnütziger Verein für
Volksbildung im Bereich der Datenverarbeitung. Der FFII unterstützt
die Entwicklung öffentlicher Informationsgüter auf Grundlage des
Urheberrechts, freien Wettbewerbs und offener Standards. Über 500
Mitglieder, 1200 Firmen und 80000 Unterstützer haben den FFII mit der
Vertretung ihrer Interessen im Bereich der Gesetzgebung zu
Software-Eigentumsrechten beauftragt.  Der FFII unterhält je ein
ständig besetztes Büro in München und Brüssel sowie nationale Verbände
und Unterstützergruppen in den meisten Ländern Europas.  Der FFII ist
Mitglied des Europäischen Dachverbandes der Verbände kleiner und
mittlerer Unternehmen, CEA-PME.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/messe0504.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: messe0504 ;
# txtlang: xx ;
# End: ;

