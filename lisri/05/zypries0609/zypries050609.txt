Brigitte Zypries
Bundesministerium der Justiz
Mohrenstr. 37
10117 Berlin
Telefon 01888-580-9000
Telefax 01888-580-9043
Email: ministerium AT bmj.bund.de

An die 											9. Juni 2005
Mitglieder des 
Europ�ischen Parlaments
Bat. Altiero Spinelli
10G146
60, rue Wiertz
1047 Bruxelles
Belgien 

per E-Mail-Verteiler

Sehr geehrte Frau Abgeordnete, sehr geehrter Herr Abgeordneter,

das Europ�ische Parlament ber�t derzeit in zweiter Lesung die geplante Richtlinie �ber die Patentierbarkeit computerimplementierter Erfindungen. Zu diesen geh�rt zum Beispiel das bekannte Kfz-Antiblockiersystem ABS. 

Mit der Richtlinie soll kein neues Recht geschaffen werden, sondern das unter anderem von der Rechtsprechung geschaffene Recht - ma�geblich gepr�gt von der Entscheidungspraxis des Bundesgerichtshofs - kodifiziert werden. Mit dieser Rechtspraxis konnte die innovative deutsche Wirtschaft bisher gut leben; insbesondere der Bereich der Open-Source-Software hat sich - auch gef�rdert durch die Bundesregierung - sehr positiv entwickelt. Dies gilt auch f�r die kleinen und mittelst�ndischen Unternehmen.

Allerdings hat eine unterschiedliche Rechtspraxis der nationalen Patent�mter und des Europ�ischen Patentamts zu teilweise abweichenden Entscheidungen dar�ber gef�hrt, was in Europa patentierbar ist und was nicht. Diese divergierende Rechtspraxis ist in einem Bereich, der wesentlich f�r das Wachstum und die weitere industriepolitische Entwicklung in Europa ist, nicht hinnehmbar. Mit der Richtlinie soll daher versucht werden, einheitlich Rahmenbedingungen in der gesamten Europ�ischen Union herzustellen.

Diesem Ziel wird der "Gemeinsame Standpunkt" des Rates vom 7. M�rz 2005 weitestgehend gerecht. Er hat, wie Sie wissen, manche Vorschl�ge des Europ�ischen Parlaments aus erster Lesung nicht �bernommen. Durch diese Vorschl�ge waren umfangreiche Technikgebiete von der Patentierbarkeit ausgenommen worden. Dies h�tte nach meiner Ansicht erhebliche negative Konsequenzen f�r weite Bereiche der deutschen Wirtschaft, die etwa ein Drittel aller europ�ischen Anmeldungen beim Europ�ischen Patentamt vornimmt, und die in diesen Bereichen t�tigen hochqualifizierten Besch�ftigten.

Auf der anderen Seite sieht der "Gemeinsame Standpunkt" des Rates trotz vieler anders lautenden Behauptungen auch keine Ausweitung der Patentierungsm�glichkeiten im Vergleich zur heute geltenden Rechtslage vor. Software als solche bleibt von der Patentierbarkeit ausgeschlossen. Der Richtlinienentwurf bekr�ftigt die in Deutschland und fast ganz Europa geltende Rechtslage.

Deshalb bitte ich Sie, bei Ihrer Entscheidung �ber die �nderungsantr�ge den "Gemeinsamen Standpunkt" des Rates, der einen ausgewogenen und interessengerechten Kompromiss darstellt, zu unterst�tzen. 

In zwei Punkten, die aufzugreifen auch der Deutsche Bundestag in seiner interfraktionellen Entschlie�ung gebeten hat, k�nnte der "Gemeinsame Standpunkt" des Rates nach meinem Daf�rhalten noch optimiert werden. Dies zeigen auch die Ergebnisse der Diskussion des von mir initiierten "Runden Tisches" am 1. Juni 2005 mit Berf�rwortern und Kritikern des Entwurfs aus der Praxis.

So w�re denkbar, den im Richtlinienentwurf enthaltenen Begriff des "technischen Beitrags" zu konkretisieren, um den Umfang der Patentierbarkeit noch pr�ziser einzugrenzen. Daf�r bietet sich die vom Bundesgerichtshof entwickelte und in der Praxis bew�hrte Definition des Begriffs "Technik" an, auf die auch die Entschliessung des Deutschen Bundestags eingeht.

Obwohl es praktisch bisher keine patentrechtlichen Probleme gegeben hat, k�nnte zur Sicherung der Interoperabilit�t, einem Kernanliegen auch und gerade der Kritiker des Richtlinienentwurfs, aus meiner Sicht das Zwangslizenzmodell, wie es der im Rechtsausschuss eingebrachte �nderungsantrag 153 formuliert, eine geeignete L�sung sein. Es gew�hrt demjenigen, der den Zugang zu patentierter Technik braucht, um verschiedene Computersysteme zu verkn�pfen, den Anspruch auf eine Lizenz zu angemessenen Bedingungen.

Mit freundlichen Gr��en, Ihre Brigitte Zypries
