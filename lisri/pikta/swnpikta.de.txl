<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Gko: GNU-Projekt

#Lii: BILD dir deine Meinung über Softwarepatente -- Vereinfachende aber wahre Einführung in die Problematik

#Sns: Schutz der Informatischen Innovation vor dem Missbrauch des Patentwesens

#Lxg4: Bundesjustizministerium fordert Moratorium in Sachen Swpat und droht mit EPÜ-Austritt

#Vnm: Vergleichsbericht über die Prüfung von Softwarepatenten am Europäischen, Amerikanischen und Japanischen Patentamt

#PaK: Probleme und Ungereimtheiten der Softwarepatentierung aus der Sicht eines Prüfers am Deutschen Patent- und Markenamt

#nWt: Über die Patentprüfung von Programmen für Datenverarbeitungsanlagen

#DKe: Dr. Swen Kiesewetter-Köbinger

#EWp: Eurolinux-Petition für eine softwarepatentfreies Europa

#EeW: Europäische Softwarepatente: Datenbank und Beispielsammlung

#Dan: Der FFII wird gemeinsam mit fördererden Softwareunternehmern und Systemarchitekten wie Richard Stallman am Dienstag den 21. November um 11-12:30 Uhr neben dem Europäischen Patentamt im Forum der Technik, Raum Helios, seine %(q:Europäische Softwarepatente-Horrorgallerie) präsentieren und Journalistenfragen beantworten.  Richard Stallman hat in den USA als Gründer der %(lp:League for Programming Freedom) und des %(gp:GNU-Projekts) Jahre lang überzeugend dargelegt, warum das Patentsystem den Fortschritt im Softwarebereich nur behindert.

#Aio: Auf der %(q:Diplomatischen Konferenz) werden derweil Patentreferenten von 20 europäischen Staaten über einen vom EPA-Präsidenten Dr. Ingo Kober entworfenen %(q:Basisvorschlag zur Revsion des Europäischen Patentübereinkommens) verhandeln.  Darin schlägt das EPA u.a. vor, eine universelle Patentierbarkeit festzuschreiben (Art 52) und dem EPA-Vorstand Gesetzgebungsvollmachten einzuräumen (Art 33).  Die Tagesordnung wurde vom EPA so festgelegt, dass nur eine 2/3-Mehrheit der nationalen Patentdelegationen sich noch in Einzelfragen durchsetzen kann.  Andernfalls wird der Wille des EPA innerhalb von wenigen Monaten in allen europäischen Staaten rechtsgültig, sofern deren Parlamente nicht beschließen, aus dem Europäischen Patentübereinkommen (EPÜ) auszusteigen.

#BWt: Bisher haben sich bereits 200 Softwarefirmen und 55000 Unterzeichner der %(ep:Eurolinux-Petition für ein softwarepatentfreies Europa) in ähnlichem Sinne geäußert.

#Dha: Ein Blick in die Patentdatenbank des FFII zeigt anschaulich, was Softwarepatente in Europa heute für den Großteil der EDV-Unternehmen bedeuten.  Auf einem solchen Minenfeld haben kleine Softwareunternehmen kaum noch Chancen.  Für mein Unternehmen habe ich die Konsequenzen bereits gezogen: Ich werde voraussichtlich ab Mitte nächsten Jahres weite Teile der Software-Entwicklung in einem Land durchführen, in welchem es kein so hoch entwickeltes Patentrecht gibt und wo eine Änderung der Rechtslage in den nächsten Jahren auch nicht zu erwarten ist. Im Bereich bestimmter Technologien wird mir die Entwicklung von Software in Deutschland einfach zu riskant, das läßt sich mit Blick auf rechtliche Risiken in der Zukunft aus der Kleinunternehmerperspektive nicht mehr verantworten.

#Ded: Für Daniel Rödding, Geschäftsführer eines Softwareunternehmens in Paderborn, ist die Lage sehr ernst:

#Wed: Wer in den Patentschriften des EPA schmökert, stellt schnell fest, dass es hier weder um Software noch um innovative Programmierlösungen geht. Hier werden einfach systematisch ganze Problemfelder in Besitz genommen.  Diese lächerlich trivialen und gruselig breiten EPA-Patente sind aber vor nationalen Gerichten bisher nicht unbedingt einklagbar.  Die amerikanischen Großkonzerne, denen die gesetzeswidrig erteilten EPA-Patente zum Großteil gehören, warten noch auf eine Änderung des Europäischen Patentübereinkommens.  Wenn die Diplomatische Konferenz nächste Woche in München das falsche Signal setzt, wird Deutschland hoffentlich die Ankündigung des BMJ wahr machen und aus dem EPÜ austreten.  Der Ernst der Lage rechtfertigt dies.  Es geht jetzt nämlich nicht mehr um die Pflege dieses oder jenes Organisationsrahmens für das europäische Patentwesen sondern um die Entschärfung von 30000 Tretminen und die Wiederherstellung elementarer Rechtssicherheit für Europas IT-Unternehmer und Bürger.

#DAa: Der Patentdatenreferent des FFII, Arnim Rupp, empfiehlt allen Diskutanten einen Blick in die Datenbank:

#Dnu: Der %{FFII} hat eine %(sp:Datenbank) veröffentlicht, die einen Überblick über die umstrittenen europäischen Patente ermöglicht.  Dazu werden einige eindrucksvolle Beispielpatente, Statistiken und Studien präsentiert.

#Drh: Das %(ep:Europäische Patentamt) (EPA) hat in den letzten Jahren entgegen dem Buchstaben und Geist der geltenden Gesetze 30000 Patente auf Programmieraufgaben, Geschäftsideen und organisatiorische Verfahren erteilt.  Würde auch die nationale Rechtsprechung konsequent dem Willen des EPA folgen, so wäre es demnach heute in Europa nicht mehr erlaubt, medizinische Diagnosen automatisiert durchzuführen.   Ebenso betroffen wären zahlreiche recht alltägliche Verfahren wie die Abhaltung von Prüfungen in Schulen, die Anbahnung von Geschäften an der Börse, die Erzeugung von Einkaufszetteln aus Kochrezepten, die dynamische Festlegung von Verkaufspreisen, das Sprachenlernen durch Vergleichen der eigenen Aussprache mit der eines Lehrers.  All diese Verfahren verletzen europäische Patente, sofern man sie auf naheliegende Weise rechnergestützt organisiert.  Nicht minder gefährlich ist aus der Sicht von Fachleuten die Blockierung von Netzwerk-Standards wie MIME und CGI, die Zupflasterung der Betriebssystem-Ebene mit Tausenden von Patenten auf hardwarenahe Rechenaufgaben wie z.B. die Differenzierung zwischen benutzten und unbenutzten Speicherblöcken.

#descr: Programmierer veröffentlichen %(q:Europäische Softwarepatent-Horrorgallerie).   Europäische Patentfunktionäre wollen nächste Woche 30000 Tretminen aktivieren

#title: Europäische Softwarepatente: %(q:Trivialer als in den USA)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnpikta ;
# txtlang: de ;
# multlin: t ;
# End: ;

