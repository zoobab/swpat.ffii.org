<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Future Developments concerning Software Patents

#descr: What will happen next in with Regard to Software Patents, in
particular the EU directive project?  What is up in India, Australia?
at WIPO, WSIS?  FFII & friends try to keep this page up to date.

#ftP: EU Software Patent Directive Project

#ini: Indian Government Ordinance

#rrg: Australia-USA Free Trade Agreement

#eea: General

#aLe: Timetable of the Luxemburg Presidency

#WsW: Although the %(sa:software patent agreement of 2004-05-18) no longer
enjoys the support of a legitimated majority of governments, efforts
to push it through one of the next Council sessions will continue. 
The Dutch government is even said to have offered to do what it
considers its %(q:finish-up job) on behalf of Luxemburg in January. 
This would mean passing as an A-item in January based on the
assumption that all countries agree.

#stt: Indeed Poland has not formally filed a note of disagreement with the
Council so far, and %(cr:article 3 of the Council's rules), which
served as a reason for %(af:delay at the Agriculture and Fishery
Meeting in December), will then no longer be citable.  The Dutch
government, by continuing to support the Agreement of May 18th, is
violating the %(nm:Dutch parliament's motion of last July); the
minister in charge, %(LB) is defying the parliament.

#ehW: Simultaneously, a group of members of the European Parliament is
preparing for a %(rp:restart of the procedure) in case the Council
continues to be unwilling to do its homework.  %(q:Homework) should
mean: analyse the interests at stake, read the Parliament's proposed
solution and formulate responses that show at least some willingness
to understand and appreciate.

#ecs: Presidency Website

#sWW: Council Presidency Priorities: Promote Patents, Conclude Software
Directive in 2005

#oCg: Important EC meeting dates

#nio: Economy & Finance Council

#cWC: Agriculture & Fishery Council

#anR: General Affairs and External Relations

#peo: Competitiveness Council

#ghv: The Indian Government is trying to legalise software patents in India
by means of a government ordinance which was passed without
parliamentary scrutiny under the pretext of implementation of the
!TRIPs treaty.  Everything must be done to challenge this ordinance.

#bib: The AUSFTA will be used to push the limits of patentability or to
stabilise caselaw created by the patent judiciary.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: SwpatFutur ;
# txtlang: en ;
# multlin: t ;
# End: ;

