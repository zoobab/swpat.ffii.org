<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: History of the Limits of Patentability

#descr: How the patent system evolved, expanded and how theories of its limits
evolved and were codified.

#lWW: Medieval Europe

#pec: Kings grant patent-like privileges in order to reciprocate favors and
collect revenues.

#ire: Venice: first patents

#QWW: England: Monopoly Act.  Queen Elisabeth I granted all kinds of
monopolies in order to create a source of revenues for the crown which
does not need the approval of the parliament.  Upon negotiations with
the Parliament, this right is limited to monopolies for inventions.

#sel: James Watt patents a %(q:condenser connected to a cylinder by a
valve).  Patents slow down development of steam engine.

#shW: US Constitution mentions patents and copyright as instruments which
congress shall %(q:have the power) to use %(q:to promote progress of
science and the useful arts).

#sfW: Thomas Jefferson 1st president of US patent office.  Few patents in
the US in the following years

#jct: French patent law, Some french revolutionaries postulate %(q:natural
right to intellectual property), others ridicule it.

#ppW: telegraphy revolution, patents come out of fashion

#nan: German patent law introduced.  According to %(fm:Machlup), it marked a
%(q:victory of lawyers and protectionists against economists).

#nWd: Japanee patent law introduced.

#nld: Patent-sceptical thinking dominant

#aei: European Patent Convention signed in Munich

#ttc: software patents excluded

#ton: Software not Patentable

#ton2: Software not Patentable

#gfg: signal of ambiguity

#nrW: new calculation rule for optimising industrial production of rolling
rods is not a technical invention

#Wln: %(q:Technical Program) patentable, function claims permissible, if
forces of nature at the center

#Wat: Court of Appeal of the Federal Circuit (CAFC) for patent matters
founded, acts as promoter of its own specialty, extends patentability
in the following years

#isW: Vicom case: begin of granting patents for algorithms and business
methods, disguised as technical apparatuses.

#poW: surge of patenting in US+JP without corresponding surge in innovation

#WWi: surge in software patent applications

#wre: Software Copyright directive

#Wtf: Green Paper for Community Patent calls for legalisation of software
patents by change of EPC and EU directive

#atW: State Street case

#pso: Technical Board of Appeal IBM T97/1173 case legalises program claims. 
Note of EPO president says that this decision is a model to be
followed

#98E: 1998 EPO

#knl: TBA judge Mark Schar %(ms:redefines) %(q:technical invention) as
%(q:practical problem solution)

#rWe: European Commission produces followup papers to the Greenpaper,
arguing that Microsoft has many patents and therefore patents are
needed for software.

#PeW: EPO Base Proposal asks for deletion of Art 52(2) in its entirety.

#rot: Start of Eurolinux Petition

#C2W: Diplomatic Conference leaves Art 52(2) untouched, decision is moved to
the EU

#neW: CEC keeps lid on consultation results, publishes manipulative report
by %(q:independent contractor) several months after its delivery

#Rap: MEPs McCarthy, Wuermeling, Plooj, Rocard etc nominated rapporteurs

#BHs: %(dg:Bakels & Hugenholtz study), critical of swpat, demands completely
new approach

#cpf: %(cp:counter proposal by FFII)

#JUh: JURI hearing

#dkp: %(dp:Danish Council presidency publishes working party position) in
favor of unlimited patentability

#CUv: CULT vote against swpat, for freedom of publication and interoperation

#ITv: ITRE vote for freedom of publication and interoperation

#mcr: McCarthy report to JURI

#cfo: FFII conference on swpat in Brussels

#JUv: JURI vote for fake limits on patentability

#bd3: FFII demo in Brussels

#pld: plenary debate in EP after %(am:129 amendments) have been tabled;
%(bt:Bolkestein's threat)

#dcp: Commission DGIM %(dc:nitpicks the parliament's decision)

#Cwp: public version of the Council's %(wp:working group proposal), soon
thereafter moved to COREPER

#Bxl: %(bx:FFII demo and conference) on swpat in Brussels, amidst Council
Presidency moves, Nokia campaign and MEP statements

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatcitri.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatcitri ;
# txtlang: en ;
# multlin: t ;
# End: ;

