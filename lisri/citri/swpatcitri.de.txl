<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Geschichte der Grenzen der Patentierbarkeit

#descr: Wie das Patentsystem sich entwickelte und ausbreitete, und wie die
Theorien über seine Grenzen entstanden und festgeschrieben wurden.

#lWW: Europäisches Mittelalter

#pec: Könige vergeben Patent-artige Privilegien zur Begünstigung von
Anhängern und als Einnahmequelle.

#ire: Venedig: Erste Patente

#QWW: Großbritannien: %(q:Monopol Act), %(pe:Monopolgesetz). Königin
Elisabeth I. vergibt Patente als Geldquelle für die Krone, die keiner
Zustimmung des Parlaments bedarf.

#sel: James Watt patentiert einen %(q:Verdichter, der über ein Ventil mit
einem Zylinder verbunden ist). Patente verlangsamen die Entwicklung
der Dampfmaschine.

#shW: Die Verfassung der USA erwähnt Patente und Copyrights als Instrumente,
über die der Kongress %(q:verfügen soll), um %(q:den Fortschritt von
Wissenschaft und nutzbringender Kunst zu befördern).

#sfW: Thomas Jefferson ist der erste Präsident des US-Patentamts. Nur wenige
Patente in den USA in den darauffolgenden Jahren.

#jct: Frankreich führt Patentwesen ein. Einige französische Revolutionäre
postulieren ein %(q:natürliches Recht auf geistiges Eigentum), andere
lachen darüber.

#ppW: Telegraphie-Revolution, Patente kommen aus der Mode

#nan: Deutschland führt Patentwesen ein. Nach %(fm:Machlup) ein %(q:Sieg der
Anwälte und Protektionisten über die Ökonomen).

#nWd: Japan führt Patentwesen ein.

#nld: Patent-kritische Ansichten dominieren

#aei: Europäische Patentübereinkunft %(pe:European Patent Convention, EPC),
in München unterzeichnet

#ttc: Softwarepatente ausgeschlossen

#ton: Software nicht patentierbar

#ton2: Software nicht patentierbar

#gfg: Anzeichen der Aufweichung

#nrW: Neue Berechnungregel für die Optimierung von Walzstäben ist keine
technische Erfindung.

#Wln: %(q:Technische Programme) patentierbar. Funktionsansprüche möglich,
wenn die Beeinflussung von Naturkräften im Mittelpunkt steht.

#Wat: Schiedskammer für Patentsachen am Federal Circuit %(pe:CAFC)
gegründet, diese agiert in den folgenden Jahren durch Ausweitung der
Patentierbarkeit zu ihrem eigenen Vorteil.

#isW: %(q:Vicom)-Fall: Der Anfang der Genehmigung von Patentansprüchen auf
Algorithmen und Geschäftsmethoden, indem diese als %(q:technische
Gerätschaften) ausgegeben werden.

#poW: Schwemme von Patenten in den USA und Japan, ohne neue Innovation in
gleichem Maße.

#WWi: Schwemme von Softwarepatent-Anmeldungen

#wre: Software-Urheberrechts-Richtlinie

#Wtf: Grünbuch für das Gemeinschaftspatent verlangt Legalisierung von
Softwarepatenten durch Änderung des EPÜ und verwandter EU-Richtlinien

#atW: %(q:State Street)-Fall.

#pso: Im Fall %(q:IBM T97/1173) werden Programmansprüche von der technischen
Schiedskammer des EPA zugelassen.  Laut Notiz des EPA-Präsidenten ein
Modell für die Zukunft.

#98E: 1998 EPA

#knl: Mark Schar, Richter an der Technischen Schiedskammer, führt eine
%(ms:Neudefinition) von %(q:Technischer Erfindung) als %(q:Praktische
Problemlösung) ein.

#rWe: Europäische Kommission produziert Folgepapiere zum Grünbuch, Argument
ist u.a., dass Microsoft viele Patente hält und folglich Patente für
Software benötigt werden.

#PeW: EPA-Grundsatzbeschluss fordert die vollständige Streichung von Art.
52(2) EPÜ.

#rot: Start der Eurolinux-Petition

#C2W: Diplomatische Konferenz lässt Art. 52(2) unberührt, Entscheidung liegt
jetzt bei der EU

#neW: CEC hält Umfrageergebnisse unter Verschluss, eine einseitige Studie
eines %(q:unabhängigen Vertragspartners) wird erst Monate nach ihrer
Fertigstellung veröffentlicht.

#Rap: Ernennung der MdEP McCarthy, Würmeling, Plooj, Rocard etc zu
Berichterstattern

#BHs: %(dg:Studie von Bakels & Hugenholtz), SWPat-kritisch, fordert
vollständig neuen Ansatz

#cpf: %(cp:Gegenvorschlag des FFII)

#JUh: Anhörung im Rechtsausschuss JURI

#dkp: %(dp:Dänische Ratspräsidentschaft veröffentlicht Positionspapier) der
Rats-Arbeitsgruppe, das auf unbegrenzte Patentierbarkeit hinausläuft

#CUv: CULT-Abstimmungsergebnis: gegen SWPat, für Freiheit der Publikation
und der Interoperation

#ITv: ITRE-Abstimmungsergebnis: für Freiheit der Veröffentlichung und der
Interoperation

#mcr: McCarthy-Bericht an JURI

#cfo: FFII-Konferenz über SWPat in Brüssel

#JUv: JURI-Abstimmung: Scheinbegrenzungen der Patentierbarkeit

#bd3: FFII-Demonstration in Brüssel

#pld: Plenardebatte im Europaparlament, bei welcher %(am:129
Änderungsanträge) zur Diskussion stehen; %(bt:Bolkesteins Drohung)

#dcp: Kommission des DGIM %(dc:mäkelt an der Parlamentsentscheidung herum)

#Cwp: öffentliche Version des %(wp:Ratsarbeitsgruppenvorschlags), bald
darauf an COREPER abgegeben

#Bxl: %(bx:FFII-Demonstration und Konferenz) über SWPat in Brüssel, zwischen
Zügen der Ratspräsidentschaft, Nokia-Kampagne und MdEP-Erklärungen

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatcitri.el ;
# mailto: mlhtimport@ffii.org ;
# login: ccorn ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatcitri ;
# txtlang: de ;
# multlin: t ;
# End: ;

