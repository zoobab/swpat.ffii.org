<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#EvW: Eine %(ah:statistische Studie der amerikanischen Swpat-Praxis) zeigt: das US-Patentamt wird 1998-99 insgesamt 40000 Softwarepatente, also 10 mal so viel wie im Vergleichszeitraum 1993-94, gewähren und dabei 50 Millionen USD verdienen.  Derweil stehen dem US-Patentamt nicht wesentlich mehr Prüfer als damals zur Verfügung und die Prüfungsverfahren sind weitgehend Makulatur.  Zur Neuheitsprüfung werden praktisch nur Patentschriften herangezogen.

#SaW: Statistik zeigt: Softwarepatente werden ohne seriöse Prüfung gewährt

#Dfe: Die %(q:einschlägigen Patentierungsverbote für Computerprogramme) müssen abgeschafft und ein %(q:umfassender Patentschutz für Softwareerfindungen) ermöglicht werden, fordert der Zentralverband Elektrotechnik- und Elektronikindustrie (ZVEI) e.V.  Spätere Recherchen ergaben, dass es sich hierbei um eine Erklärung des Chefs der Siemens-Patentabteilung Arno Körber handelte, die vom ZVEI unbesehen übernommen wurden.

#Zrn: Software muss patentierbar sein

#Zdn: Zentralverband der Elektronik-Industrie

#Dxs: Der %(LV) kritisiert die %(zd:Forderung) des %(ZV) nach Abschaffung des Patentierungsverbotes für Software.

#Lnr: Linux-Verband kritisiert ZVEI-Brief

#Mct: Mitte des Monats erreichte uns ein Brief aus dem Bundesjustizministerium, der unsere Bedenken gegenüber den EU-Plänen zu zerstreuen sucht und uns in Aussicht stellt, als eine interessierte Partei in die kommenden EU-Konsultationen mit einbezogen zu werden.  Wir haben nach vereinsinterner Diskussion geantwortet.  In unserer Antwort legen wir dar, warum wir die Pläne der EU für gefährlich halten, stellen einige Fragen und bieten weiterführende Informationen an.

#Ftn: FFII antwortet dem Patentreferenten des Bundesjustizministeriums

#Dwr: Die Delegation wurde von %(JPS) geleitet.  Die Fahrtkosten wurden von den %(fs:Sponsoren) der %(fg:Softwarepatent-Arbeitsgruppe des FFII) getragen, von denen die meisten auch Vertreter mit %(lt:Briefen ihrer Vorstände) schickten und so eindringlich ihre Sorge über die Folgen der EU-Pläne zum Ausdruck brachten.

#EkC: Eine Delegation von 8 Vertretern der Mitglieder und Förderer von Eurolinux besuchte am 15. Oktober die Patentgesetzgeber der EU-Kommission / Generaldirektorat 15 (DG XV) in Brüssel, um ihnen zu erklären, warum die EU-Pläne bezüglich Patentierbarkeit von Computerprogrammen für die Softwarehersteller gefährlich sind, und um eine Lösung vorzuschlagen, die allen Seiten entgegenkommt.

#Tie: Treffen mit EU-Gesetzgebern

#Uis: Unsere %(ag:Arbeitsgruppe) nahm an einer Studienkonferenz über neue Felder des Patentwesens im Münchner Max-Planck-Institut teilnehmen.

#Sni: SWPAT-Konferenz in München

#Dsr: Der %(fi:Förderverein für Informationstechnologie und Gesellschaft) wendet sich in einer Presseerklärung gegen die Pläne der EU und der Unterzeichnerstaaten des Europäischen Patentübereinkommens, den Patentschutz für Software auszuweiten.  Diese Erklärung trägt unterstützende Unterschriften von weiteren Verbänden, darunter dem Österreichischen Linuxverband LUGA, dem Virtuellen Ortsverein der SPD und dem Förderverein für eine Freie Informationelle Infrastruktur (FFII e.V.).

#5tn: 5 IT-Verbände kritisieren Ausweitung des Patentwesens

#descr: Was der FFII im Jahre 1999 über staatlich gewährte Erfindungsmonopole und ihre missbräuchliche Ausweitung auf rechnergestütztes Denken, Rechnen, Organisieren und Formulieren zu berichten hatte

#title: Logikpatentnachrichten 1999

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri99 ;
# txtlang: de ;
# multlin: t ;
# End: ;

