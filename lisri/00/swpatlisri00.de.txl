<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#FnC: Firma Intevation wird im Auftrag des FFII gegen Zeitaufwandsentschädigung termingerecht zur %(bk:Berliner Konferenz) eine CD zum Thema %(q:Patentrechte und Software) zusammenstellen.

#Ane: Auftrag zur Erstellung einer SWPAT-Dokumentations-CD erteilt

#DWr: Das Bundesministerium für Wirtschaft und Technologie hält am 18. Mai 2000 in Berlin eine öffentliche Tagung ab, auf der die wirtschaftlichen Auswirkungen der Patentierbarkeit von Software untersucht werden sollen.  Zahlreiche große und kleine Softwarefirmen wurden eingeladen. Auf Einladung des FFII kommt der amerikanische Patent-Rechercheur Greg Aharonian.

#Knd: BMWi lädt zu Konferenz über Auswirkungen der Patentierbarkeit von Software

#Ant: Auf Forderung von Microsoft muss der Entwickler des freier Filmbearbeitungsprogramms %(VD) die Unterstützung für das patentierte ASF-Format aus seinem Programm entfernen.  Durch die Patentierung eines trivialen Formates und gleichzeitige aggressive Vermarktung des ASF als Industriestandard dürfte es Microsoft gelingen, zwei Fliegen mit einer Klappe zu schlagen:  (1) Vertrauensgewinn bei Inhalteanbietern dank Unterbindung der Kopierbarkeit (2) Behinderung der Entwicklung konkurrierender Betriebssysteme.  Hier wird das Patentwesen nochmals zweckentfremdet.

#Mta: Microsoft untersagt Unterstützung von patentiertem Dateiormat in freier Software

#FWr: Auf der Mitgliederversammlung präsentierte Bernhard Reiter die vom FFII bestellte CD %(q:Softwarepatentierung - Aspekte der Debatte) und liefert 10 Exemplare zur Verteilung auf der Konferenz des nächsten Tages ab.

#Smk: SWPAT-Dokumentations-CD fertig

#Een: Etwa 40 Vertreter von Softwarefirmen, Patentämtern, Ministerien, IT-Verbänden und Hochschulen trafen sich im BMWi, um über die Auswirkungen von Softwarepatenten auf Wirtschaft und Informationsgesellschaft zu beraten.  Vertreter der %{LST} und einiger kleinerer Firmen kritisierten die Vergabe von Softwarepatenten durch die Patentämter und verlangten, im Softwarebereich solle nur das Urheberrecht zur Anwendung kommen.  Vertreter der Patentabteilungen von Siemens, IBM, Patentämtern und Patentanwaltsverbänden hielten mit juristischen und moralischen Argumenten dagegen.  Der FFII leistete diverse Beiträge zur Konferenz.

#Bol: BMWi-Konferenz zeigt: Softwarefirmen wollen keine Patente

#Zio: Zwei Wirtschaftswissenschaftler des %(MIT) stellen in ihrer Studie zunächst fest, dass die Patentierbarkeit von Software bei amerikanischen Unternehmen zu keiner wesentlichen Zunahme sondern eher zu einer Abnahme der Investitionen in Grundlagenforschung und Innovation geführt hat.  Sie entwickeln dann ein Rechenmodell, anhand dessen sich Innovation voraussagen lassen soll, und zeigen, dass die Einführung von Softwarepatenten zum Erlahmen der Innovationstätigkeit führt.

#MWe: MIT-Ökonomen rechnen vor: Softwarepatente bremsen Fortschritt

#Dta: Der für die %(dg:Binnenmarkt-Generaldirektion) und damit für die Patentpläne zuständige EU-Kommissar %(FB) plädiert vor einem Publikum von Patentrechtlern für eine zügige Auweitung des europäischen Patentwesens.  Die %(q:Neue Ökonomie) Europas kann laut Bolkestein nur dann gedeihen, wenn es Unternehmen viel leichter gemacht wird, Ausschlussrechte auf Softwareideen und Wissen aller Art zu erwerben.

#Ebr: EU-Kommissar bekräftigt harte SWPAT-Linie

#Iei: In einer Sendung des Österreichischen Rundfunks warnt Harvard-Verfassungsjurist Prof. Lawrence Lessig, der die US-Regierung im Microsoft-Prozess beriet, vor einer erneuten Bedrohung der offenen Gesellschaft 10 Jahre nach dem Fall der Berliner Mauer.  Eine rigide Ideologie des Geistigen Eigentums (%(q:Software-Stalinismus)) schaffe derzeit weitgehend unbehelligt Reglementierungen, die in wenigen Jahren das ehedem so lebendige Internet zu einem zweiten Fernsehen degradieren könnten.  Lessig %(ko:kritisiert die OpenSource-Szene wegen ihrer manglenden Bereitschaft, ihrerseits die Mittel der Gesetzgebung zu nutzen, um die informationelle Infrastruktur gegen Aneignungsversuche großer Interessengruppen zu schützen).  Die ORF-Webseiten verweisen auch auf den FFII.

#Oae: ORF-Sendung: Patente zerstören das Internet

#Age: Auf Einladung des FFII werden Deutschlands kompetenteste Patentjuristen Konzepte vorlegen, wie man das Patent-Ungeheuer mit seinen eigenen Mitteln zähmen kann.  Wir wollen uns zwar auf alle Möglichkeiten einlassen, laden aber zugleich mit umfangreicher Dokumentation und vorgefertigten Petitionsbriefen dazu ein, die Softwarepatent-Gesetzgebung der EU zu kippen.

#FKW: FFII plant SWPAT-Konferenzprogramm für LinuxTag 2000

#Tra: 5 europ�ischer OpenSource-Vereinigungen und 10 Softwarefirmen haben unter dem Namen %(q:Eurolinux-B�ndnis) eine Kampagne auf den Weg gebracht, um Europas Gesetzgeber vor den Gefahren der Softwarepatentierung zu warnen.  Sie antworten damit auf k�rzliche %(hs:betonhart proprietaristische Reden aus der Binnenmarkt-Generaldirektion der Europ�ischen Kommission), aus denen hervorgeht, dass die Generaldirektion eine Direktive ver�ffentlichen wird, um das Patentsystem auf Software und Geistige Verfahren auszuweiten, ohne die wirtschaftlichen Auswirkungen dieser Ausweitung studiert zu haben.  Bei %(pk:fr�heren Kampagnen) zu diesem Thema waren schnell etwa 10000 Unterschriften zusammengekommen.  Die Eurolinux-Kampagne wird weiter gehen.  Sie richtet sich an das Europaparlament und st�tzt sich auf umfangreiche Argumentationshilfen und WWW-Applikationen, mit denen Freiwillige in die Lage versetzt werden sollen, gezielte �berzeugungsarbeit zu leisten.

#EWp: Eurolinux-Petition f�r eine softwarepatentfreies Europa

#Dep: Die größte europäische Linux-Messe stand im Zeichen des Kampfes gegen die Patentierung von Programmlogik, welche derzeit von der deutschen und europäischen %(pk:Patentokratie) gesetzeswidrig und gegen den Willen der Softwareindustrie forciert wird.  In zahlreichen Vorträgen und Gesprächen zeigte sich ein Konsens gegen Softwarepatente, der selbst Politiker vom BMWi und Großfirmen wie Siemens bis zu einem gewissen Grad erfasst hat.  Das Minenfeld der Programmlogikpatente wird einhellig als ein abschreckendes Investitionsrisiko für alle gesehen, die in Softwareentwicklung investieren wollen.

#Lrt: Linuxtag 2000 für ein softwarepatentfreies Europa

#DiW: Das Europäische Patentamt ist am 20.-29. November 2000 in München Gastgeber einer %(dk:diplomatischen Konferenz), die weitreichende Änderungen der grundlegenden Gesetzesregeln des europäischen Patentwesens beschließen soll.  Dabei ist u.a. geplant, das Tor zur Patentierung immaterieller Gegenstände weit zu öffnen.  Nicht nur soll das schon seit spätestens 1997 %(q:de facto überholte) Recht von Softwareautoren, ihre in eigenständiger Arbeit entwickelten Werke unbeschadet von Patentansprüchen Dritter zu verbreiten und zu verkaufen, nun offiziell aus dem Gesetzestext gestrichen werden.  Ferner sollen auch Geschäftsverfahren, Lehrmethoden, Heilmethoden, Verfahren zur Organisation des öffentlichen Lebens, mathematische Methoden, geistige Methoden u.s.w. zum Gegenstand von Patentansprüchen werden können, soweit diese Verfahren über Computer oder andere technische Geräte abgewickelt werden.  Neuere Studien zeigen, dass das Europäiche Patentamt bei der Bewertung der Erfindungshöhe immaterieller Verfahren noch niedrigere Maßstäbe anlegt als das amerikanische und Japanische Patentamt.  Trotz Petitionen und scharfer Kritik von 27000 Bürgern, 300 IT-Unternehmen und 250 Politikern spricht das Europäische Patentamt von einem %(q:breiten Konsens für Softwarepatente) und seine Pläne gelten als beschlossene Sache.  Der FFII zeigt gemeinsam mit einigen Softwarefirmen in einem %(ob:offenen Brief) an das Justizministerium die Gefahren auf.

#Fri: KMU kritisieren EPA-%(q:Basisvorschlag) für grenzenlose Patentierbarkeit

#Dnt: Der FFII veranstaltet am 20. Oktober in Berlin gemeinsam mit der Heinrich-Böll-Stiftung eine Tagung über das %(q:digitale Dilemma) und mögliche Mechanismen zur Belohnung geistigen Schaffens von Patenten über Urheberrechtsabgaben, Mikro-Spenden bis zur direkten Förderung der Informationsallmende aus Steuermitteln.  Führende Richter, Anwälte, Patentprüfer, IT-Unternehmer und Gelehrte diskutieren über die brennende Frage der Softwarepatentierbarkeit.

#TWd: Tagung %(q:Wem gehört das Wissen?)

#Dei: Der FFII hat etwas mehr Licht in den Dschungel der europäischen Patentdaten gebracht.  In der heute veröffentlichten %(db:Softwarepatente-Datenbank) findet man erstmals die vom Europäischen Patentamt gewährten Softwarepatente in übersichtlicher Form, so dass Programmierer endlich ermessen können, worum es bei der Debatte geht.

#Gdn: Gruselkabinett der Europäischen Softwarepatente

#Ese: Einige IT-Unternehmen und Verbände fordern in einem Offenen Brief fünf Maßnahmen zur Beschränkung des Patent(un)wesens: Meinungs- und Ausdrucksfreiheit für Programmierer, Recht auf freien Zugang zu öffentlichen Kommunikationsnormen, präzise Begrenzung der Patentierbarkeit, sachgerechte Förderung informatischer Innovation und Entbürokratisierung des Patentwesens.

#Fvf: Fünf Gesetzesinitiativen zum Schutz der Informatischen Innovation

#descr: Was der FFII im Jahre 2000 über staatlich gewährte Erfindungsmonopole und ihre missbräuchliche Ausweitung auf rechnergestütztes Denken, Rechnen, Organisieren und Formulieren zu berichten hatte

#title: Logikpatentnachrichten 2000

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri00 ;
# txtlang: de ;
# multlin: t ;
# End: ;

