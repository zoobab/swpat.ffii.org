<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#FnC: Firma Intevation wird im Auftrag des FFII gegen Zeitaufwandsentschädigung termingerecht zur %(bk:Berliner Konferenz) eine CD zum Thema %(q:Patentrechte und Software) zusammenstellen.

#Ane: Auftrag zur Erstellung einer SWPAT-Dokumentations-CD erteilt

#DWr: The German Ministery of Economics is convening a public conference to investigate the economic impact of software patentability on May 18 in Berlin.

#Knd: BMWi invites to Swpat conference

#Ant: Upon request from Microsoft, the developper of a GPL'ed Windows video software %(VD) removed support for the patented Advanced Streaming Format (ASF) from his software.  By patenting a trivial format and pushing it as an industry standard, Microsoft is able to prevent the copying of content and thereby attract commercial content providers to its platform.

#Mta: Microsoft prohibits support for patented file format in free software

#FWr: At the FFII's annual member conference Bernhard Reiter presented a free CD documentation on Software patents that had been ordered by the FFII and delivered 10 copies for distribution on the next day's conference.

#Smk: FFII SWPAT Documentation CD ready

#Een: About 40 Representatives of software companies, patent offices, government agencies, IT associations and universities met in the German Ministery of Economics in order to discuss about the effects of software patents on the economy and on information society.  Representatives of %{LST} and several smaller companies criticized the granting of software patents by patent offices and demanded that in the area of software only copyright should apply.  Representatives of the patent departments of IBM and Siemens as well as patent attornies and patent lawyer associations defended the patenting practise with legal and moral arguments.  The FFII contributed to the conference in various ways.

#Bol: German Software Companies ask Government to Oppose Software Patenting

#Zio: Two economists of the %(MIT) establish a theoretical model to predict the occurence of innovation in different technologies, and show why software patents slow down innovation.

#MWe: MIT Economists calculate how software patents hamper innovation

#Dta: The EU-Commmissioner for the %(dg:Internal Market), %(FB), promises rapid expansion of the patent system before an audience of patent professionals.

#Ebr: EU commissioner restates hardline SWPAT policy approach

#Iei: In an interview with Austrian radio, Harvard's Lessig warns that 10 years after the fall of the Berlin wall, freedom is again under threat.  This time by a rigid ideology of intellectual property (%(q:software stalinism)), which is regulating Internet and rapidly turning it into a second television-like medium.  Lessig also %(ko:criticises the OpenSource movement's %(q:naive) reluctancy to promote the kind of regulation needed for protecting the public domain against appropriation by Big Business).  The ORF website also points to FFII.

#Oae: Lessig: Patents destroying the Internet

#Age: FFII has invited renowned patent law specialists to teach computer scientist how to protect themselves against patents by using patents.  At the same time FFII is inviting the public to sign petitions and make an all out effort to prevent the EU from legalising software patents in the first place.

#FKW: FFII plans swpat conference program for LinuxTag 2000

#Tra: The Eurolinux Alliance of European software companies and OpenSource associations has launched a campaign to warn European legislators against the dangers of software patents.  This campaign is a response of IT associations and software companies to recent %(hs:hardline proprietarist speeches from the European Commission's Directorate for the Internal Market), which indicated that the Directorate will issue a directive to extend the patent system to software and intellectual methods, completely ignoring the concerns raised by leading software companies and developpers, and refusing even to study the economic effects of software patenting.  %(pk:Previous campaigns) to this subject had quickly assembled approximately 10000 signatures from software developpers.  The current campaign will go further.  Based on clear demands and thourough documentation, the Eurolinux Alliance will submit its petition to the European parlament and use advanced (not yet patentable) e-techniques to let volunteers participate in lobbying the key decisionmakers.

#EWp: Eurolinux Petition for a Software Patent Free Europe

#Dep: The largest European Linux Fair was strongly influenced by the struggle against the expansion of the patent system toward programming logics, which is currently being pushed by the European %(pk:patentocracy),in defiance of the Law and against the will of the software industry.  Numerous lectures and speeches showed a broad opposition against software patentability which included to some extent even politicains from the German ministery of Economics and decisionmakers at Siemens.  Proprietary Software makers view the current patent inflation as a liability and a grave risk for anyone who invests in programming.

#Lrt: Linuxtag for a software patent free Europe

#DiW: The European Patent Office will host a %(dk:diplomatic conference) on 2000-11-20..29 in Munich, which is to decide far reaching changes of the European patent system.  A %(q:base proposal) has just been published, which proposes no-limits patentability for computer programs, business methods and anything the EPO may want to grant patents on.  The EPO has published an %(ob:open letter to the German Ministery of Justice), which has been endorsed by some leading software companies.

#Fri: SMEs criticize EPO base proposal for no-limits patentability

#Dnt: Der FFII veranstaltet am 20. Oktober in Berlin gemeinsam mit der Heinrich-Böll-Stiftung eine Tagung über das %(q:digitale Dilemma) und mögliche Mechanismen zur Belohnung geistigen Schaffens von Patenten über Urheberrechtsabgaben, Mikro-Spenden bis zur direkten Förderung der Informationsallmende aus Steuermitteln.  Führende Richter, Anwälte, Patentprüfer, IT-Unternehmer und Gelehrte diskutieren über die brennende Frage der Softwarepatentierbarkeit.

#TWd: Tagung %(q:Wem gehört das Wissen?)

#Dei: The FFII has brought some more light into the jungle of European patent data.  Via the %(db:software patent data base) published today, programmers can at last browse through lists of software patents granted by the European Patent Office and get an impression of what the discussion is really about.

#Gdn: Horror Gallery of European Software Patents

#Ese: Prominent German IT companies and associations demand measures for the limitation of the patent system:  freedom of expression for programmers, right of free access to public communication standards, precise limitation of patentability, adequeat systems for the promotion of information innovation and deburaucratisation of the patent system.

#Fvf: Five Law Initiatives for the Protection of Information Innovation

#descr: What FFII had to report in 2000 about broad property claims on inventions and their abusive extension to computer-aided reasoning, calculating, organising and formulating.

#title: FFII Logic Patent News 2000

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri00 ;
# txtlang: en ;
# multlin: t ;
# End: ;

