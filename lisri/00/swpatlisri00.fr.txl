<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#FnC: Firma Intevation wird im Auftrag des FFII gegen Zeitaufwandsentschädigung termingerecht zur %(bk:Berliner Konferenz) eine CD zum Thema %(q:Patentrechte und Software) zusammenstellen.

#Ane: Auftrag zur Erstellung einer SWPAT-Dokumentations-CD erteilt

#DWr: Le Ministère de l'Économie Allemand convient une Conférence sur l'Impacte Économique de la Brevetabilité du Logiciel

#Knd: BMWi invite a une conférence sur l'impacte de la brevetabilité et le logiciel libre

#Ant: A la demande de Microsoft, le développeur du programme de manipulation de vidéo %(VD) a dû retirer de son programme son support pour le format breveté ASF. Grâce à un brevet déposé sur un format trivial, ASF, et à un marketing agressif pour l'inposer parallèlement comme un standard industriel, l'industriel Microsoft fait d'une pierre deux coups : (1) Il gagne la confiance des offreurs de contenu en empêchant la reproduction des documents (2) Il handicape le développement d'un système d'exploitation concurrent. Voilà un nouvel exemple de détournement du brevet à d'autres fins.

#Mta: Microsoft interdit aux logiciels libres de supporter les formats de données brevetés

#FWr: Auf der Mitgliederversammlung präsentierte Bernhard Reiter die vom FFII bestellte CD %(q:Softwarepatentierung - Aspekte der Debatte) und liefert 10 Exemplare zur Verteilung auf der Konferenz des nächsten Tages ab.

#Smk: SWPAT-Dokumentations-CD fertig

#Een: Etwa 40 Vertreter von Softwarefirmen, Patentämtern, Ministerien, IT-Verbänden und Hochschulen trafen sich im BMWi, um über die Auswirkungen von Softwarepatenten auf Wirtschaft und Informationsgesellschaft zu beraten.  Vertreter der %{LST} und einiger kleinerer Firmen kritisierten die Vergabe von Softwarepatenten durch die Patentämter und verlangten, im Softwarebereich solle nur das Urheberrecht zur Anwendung kommen.  Vertreter der Patentabteilungen von Siemens, IBM, Patentämtern und Patentanwaltsverbänden hielten mit juristischen und moralischen Argumenten dagegen.  Der FFII leistete diverse Beiträge zur Konferenz.

#Bol: Consensus des PMEs allemands contre le brevet logiciel auprés du Ministère de l'Économie

#Zio: Une étude menée par deux chercheurs du %(MIT) démontre dans un premier temps que dans les entreprises américaines la brevetabilité des logiciels, loin de le favoriser, a sensiblement découragé les investissements dans les domaines de la recherche fondamentale et de l'innovation. Dans un deuxième temps, elle développe ensuite un modèle de calcul prévisionnel qui démontre que l'introduction de la brevetabilité des logiciels conduit nécessairement à un engourdissement de l'activité innovante.

#MWe: D'après les calculs des économistes du MIT, les brevets sur les logiciels freinent le progrès 

#Dta: Le commissaire pour le marché intérieur %(FB) promèt à un publique de conseils en propriété industrielle une expansion rapide du système de brevets et déclare que l'économie européenne ne peut flourir que si des droits d'exclusions sont disponibles pour toute sortes d'idées a bon prix.

#Ebr: EU-Kommissar bekräftigt harte SWPAT-Linie

#Iei: Dans une émission radio autrichienne prof. Lessig analyse l'idéologie ridige de la propriété intellectuelle comme une grande menace a la liberté similaire a celle du stalinisme.  Il parle de %(q:stalinisme de logiciel).  Le site montre a la pétition de la FFII.

#Oae: Une émission de la radio autrichienne ORF : les brevets détruisent l'Internet

#Age: Auf Einladung des FFII werden Deutschlands kompetenteste Patentjuristen Konzepte vorlegen, wie man das Patent-Ungeheuer mit seinen eigenen Mitteln zähmen kann.  Wir wollen uns zwar auf alle Möglichkeiten einlassen, laden aber zugleich mit umfangreicher Dokumentation und vorgefertigten Petitionsbriefen dazu ein, die Softwarepatent-Gesetzgebung der EU zu kippen.

#FKW: FFII plant SWPAT-Konferenzprogramm für LinuxTag 2000

#Tra: 5 associations européennes pro-OpenSource, et 10 sociétés de logiciel, sous le nom d'%(q:Alliance Eurolinux), ont lancé une campagne afin de sensibiliser le législateur européen aux dangers de la brevetabilité des logiciels. Il s'agit d'une réponse aux %(hs:arguments-massues des tenants du logiciel propriétaire à la Direction Générale du Marché Intérieur de la Commission Européenne), d'où il ressort que cette direction s'apprête à publier une directive pour étendre le système des brevets aux logiciels et autres oeuvres de l'esprit. Cette direction s'est-elle préoccupée des conséquences économiques d'une telle décision ? Lors des précédentes campagnes sur ce sujet, environ 10 000 signatures ont été réunies tout de suite. Avec la campagne Eurolinux, nous voulons aller plus loin. Nous voulons nous adresser au Parlement Européen. Nous nous appuyons sur une argumentation large et multiforme. Quelques volontaires travaillent à la traduction en plusieurs langues à l'aide d'outils logiciels performants.

#EWp: La pétition Eurolinux pour une Europe sans brevets logiciels

#Dep: La plus grande Exposition Linux d'Europe s'est tenue sous le signe du combat contre la brevetabilité des algorithmes, contre la volonté de l'actuelle %(pk:"patentocratie") européenne de l'imposer par la force, en toute illégalité et au mépris des intérêts de l'industrie du logiciel. Tout au long de nombreuses conférences et de nombreux entretiens, un consensus s'est dégagé, contre les brevets logiciels. Les représentants officiels de BMW Industries et d'autres grandes entreprises comme Siemens se sont eux aussi ralliés à cette opinion, au moins dans une certaine mesure. Le champ de mines créé par les brevets sur les algorithmes est unanimement dénoncé comme un risque effrayant pour tous les investisseurs du secteur du développement informatique.

#Lrt: Les Journées Linux 2000 à Stuttgart : délivrer l'Europe de tout brevet sur les logiciels

#DiW: Du 20 au 29 novembre 2000, à Munich, L'Office Européen des Brevets organise une %(dk:conférence diplomatique) afin de modifier de fond en comble les règles légales régissant le dépôt de brevet en Europe. Il est ainsi prévu, entre autres, d'ouvrir largement les portes à une brevetabilité des actifs immatériels. Non contents d'avoir dans les faits, depuis fin 1997, fait en sorte que le droit des auteurs de logiciels de publier et de vendre librement leurs productions personnelles à des tiers soit %(q:dépassé par la réalité), ils veulent maintenant le rayer officiellement des textes de lois. Plus encore, les concepts-métiers, les méthodes d'enseignement, les méthodes de soins, les concepts d'administration publique, les concepts et méthodes mathématiques ou spirituelles etc... tous ces domaines relèveront également des brevets parce qu'ils auront été développés sur des ordinateurs ou autres appareils. De nouvelles études l'ont montré : l'Office Européen des Brevets pose des critères encore plus défavorables que ses équivalents américain et japonais, à la valorisation de l'inventivité des processus immatériels. Malgré les pétitions et une forte contestation de 27 000 citoyens, 300 entreprises du secteur logiciel et 250 hommes politiques, l'OEB parle d'un %(q:large consensus en faveur des brevets logiciels) et présente ses projets comme une décision déjà arrêtée. En commun avec des entreprises de logiciels, la FFII publie une %(ob:lettre ouverte) au Ministère de la Justice dans laquelle elle explicite les dangers à venir.

#Fri: Contre le %(q:projet) de l'OEB d'une brevetabilité sans limitations

#Dnt: Der FFII veranstaltet am 20. Oktober in Berlin gemeinsam mit der Heinrich-Böll-Stiftung eine Tagung über das %(q:digitale Dilemma) und mögliche Mechanismen zur Belohnung geistigen Schaffens von Patenten über Urheberrechtsabgaben, Mikro-Spenden bis zur direkten Förderung der Informationsallmende aus Steuermitteln.  Führende Richter, Anwälte, Patentprüfer, IT-Unternehmer und Gelehrte diskutieren über die brennende Frage der Softwarepatentierbarkeit.

#TWd: Tagung %(q:Wem gehört das Wissen?)

#Dei: Der FFII hat etwas mehr Licht in den Dschungel der europäischen Patentdaten gebracht.  In der heute veröffentlichten %(db:Softwarepatente-Datenbank) findet man erstmals die vom Europäischen Patentamt gewährten Softwarepatente in übersichtlicher Form, so dass Programmierer endlich ermessen können, worum es bei der Debatte geht.

#Gdn: Cabinet d'Horreurs des Brevets Logiciels Européens

#Ese: Des entreprises et associations informatiques allemandes demandent 5 mesures pour arreter l'expansion du système de brevets et limiter son impacte sur l'informatique.

#Fvf: Cinque Initiatives de Loi pour la Protection de l'Innovation Informatique

#descr: Ce que la FFII avait pour rapporter en 2000 sur les monopoles d'invention accordés par l'état et leur extension abusive sur la pensée, le calcul, l'organisation et la formulation a l'aide de l'ordinateur universel.

#title: Nouvelles sur les Brevets Logiciels en 2000

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlisri00 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

