<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Asu: Am Bundesgerichtshof (BGH) ist eine Grundsatzentscheidung über die Patentierbarkeit von Computerprogrammen und %(q:Computerprogrammprodukten) anhängig.  Der unter Patentjuristen als %(q:konservativ) verschrieene 17. Senat des Bundespatentgerichts (BPatG) hat gesetzestreu die Patentierbarkeit verneint, aber Rechtsbeschwerde zum BGH zugelassen.  Dort nimmt sich ein %(q:progressiver) Software-Richter seit Jahren große Freiheit mit dem Gesetz, um gegenüber dem EPA %(q:aufzuholen).  Das BPatG hat die Widersprüchlichkeiten der BGH-Lehre %(bp:schonungslos kritisiert).  Nun aber soll wiederum der BGH in kleinem Kreis über eine grundlegende Frage der Informationsgesellschaft das Endurteil fällen.

#DWr: Die Europäische Kommission organisiert derzeit eine Konsultation über die Softwarepatentierung.  Diese Konsultation liegt in der Hand von Patentjuristen der Generaldirektion Binnenmarkt, die seit Jahren die Ausdehnung des Patentwesens forcieren.  Die GD Binnenmarkt präsentiert dabei einseitige Konsultationspapiere, die bis ins Detail den Standpunkt des EPA vertreten.  Die Position der Gegenseite wird nur bruchstückhaft erläutert und es fehlt jeglicher Hinweis auf kritische Studien und Webseiten.  Die Generaldirektion Binnenmarkt ist Streitpartei und Richter zugleich.  Die Europäische Patentorganisation plant derweil, in Kürze eine neue Diplomatische Konferenz abzuhalten, um dann endgültig die %(q:Programme für Datenverarbeitungsanlagen) von der Liste der nicht patentierbaren Gegenstände zu streichen.

#DVw: Die Konferenz hat festgeschrieben, dass Patente %(q:für beliebige Erfindungen auf allen Gebieten der Technik erhältlich) sein müssen.  Dieser Halbsatz, der im TRIPS-Vertrag als Nichtdiskriminierungsklausel im Dienst des Freihandels steht, liest sich im Kontext des neuen Europäischen Patentübereinkommens (EPÜ) wie eine Doktrin der grenzenlosen Patentierbarkeit.  Wir warnten in einem früheren %(ob:Offenen Brief) davor.

#DiW: Grossenbacher betonte ferner, die Konferenz habe %(q:den Schutzbereich europäischer Patente mit der ausdrücklichen Einbeziehung sogenannter Äquivalente präzisiert und verstärkt).  Die Äquivalenzdoktrin wird schon heute am EPA wesentlich extensiver ausgelegt als in Japan und den USA und führt dazu, dass viele Patente in Europa die größten Sperrwirkungen entfalten.

#DiA: Das Europäische Patentamt (EPA) hat bereits gegen den Buchstaben und Geist des Gesetzes ca 30000 Softwarepatente erteilt.  Das sind Patente auf %(e:Probleme), deren Lösung im Schreiben eines programmiersprachlichen Textstücks besteht.  Meistens wird dabei mit einer trivialen Idee die Umsetzung in unendlich vielen nicht-trivialen Programmen blockiert.  Zehntausende weiterer Softwarepatente warten seit 1996 auf ihre Erteilung.  Der Vorsitzende der Diplomatischen Konferenz und EPA-Funktionär Roland Grossenbacher bekräftigte in seiner Abschlussrede, dass das EPA an seiner Praxis der Softwarepatentierung unverändert festzuhalten gedenkt.

#Ret: Richter weiten Patentwesen aus und fällen Urteil über ihre Kritiker

#Ktn: Konsultation unter Gleichgesinnten in Brüssel

#Dze: Doktrin der grenzenlosen Patentierbarkeit im EPÜ

#WAn: Weltweit breiteste Anspruchswirkung dank Äquivalenzdoktrin

#Zsv: Zügiger Ausbau des Gruselkabinetts der Europäischen Softwarepatente

#descr: Auch nach der Diplomatischen Konferenz schreitet die Patentinflation ungebremst voran.  Zwar wurde der Ausschluss der %(q:Programme für Datenverarbeitungsanlagen) gegen den heftigen Widerstand des EPA aufrechterhalten, aber der Konferenzleiter und EPA-Sprecher Roland Grossenbacher stellte klar, dass das EPA mit Unterstützung der versammelten Patentfunktionäre der europäischen Regierungen weiterhin Programme auf Programme für Datenverarbeitungsanlagen erteilen wird und auch vor Geschäftsmethoden, mathematischen Methoden und den anderen Gegenständen, deren Streichung das EPA ebenfalls vergeblich gefordert hatte, nicht Halt machen wird.

#title: EPA 2000-12: Patente auf Programme und Geschäftsmethoden werden weiterhin auch ohne Legalisierung erteilt

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatlisri.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swngrossen ;
# txtlang: de ;
# multlin: t ;
# End: ;

