<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Im Januar 2001 fand das WWW-Normierungsgremium W3C heraus, dass ihre künftige Version der Hypertext-Auszeichnungssprache XML ein Patent von Sun Microsystems verletzte.  Durch Beschluss des US-Patentamtes war Sun zum Eigentümer der Idee geworden, einem Webseiten-Verweis ein Suchwort hinzuzufügen, welches das Blätterprogramm (Brauser) veranlasst, das Dokument von dort ab anzuzeigen, wo das Suchwort gefunden wurde.  Suns Lizenzbedingungen sind relativ großzügig und neuartig:  jeder darf das Konzept verwenden, solange weitere darauf basierende Konzepte offengelegt werden.  Auch wenn hierin eine gute Absicht zu erkennen ist, so schränkt Sun damit doch die Entwicklung und Verbreitung des neuen Standards ein, und viele Leute fragen sich, ob Sun dazu ein moralisches Recht hat, zumal das Patent anfechtbar ist.
title: Patent auf suchwortbasierte Hypertext-Verweise gefährdet neuen WWW-Standard
Alb: Ausführliche Analyse des Falls
Apc: Ansprüche
Bie: Bereits 1999 wurde das W3C in eine Krise gestürzt, weil %{LST} einige der grundlegenden Hypertext-Prinzipien patentiert hatten, die gerade zu einem neuen Standard formuliert wurden.  Das W3C ist seitdem gezwungen, einen beträchtlichen Teil seiner begrenzten Ressourcen für die Abwehr von Patentgefahren zu verwenden.
Tya: Der Chef einer Firma, die das W3C in die Krise gestürzt und später unter politischem Druck ihre Patente der Allgemeinheit zur freien Verfügung gestellt hatte, erklärt diesen Vorgang als einen Erfolg des Patentsystems und des %(q:Freien Marktes).
Age: Bei %(ip:anderer Gelegenheit) sagte Reed vorher, dass sogar die Autoren Freier Software lernen würden, sich das Patentsystem zu Nutze zu machen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swxai-xpointer ;
# txtlang: de ;
# End: ;

