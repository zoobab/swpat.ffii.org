<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#nrc: Übersicht und Beispiele

#Wai: Wie man Patentschriften liest

#ssr: Weitere Lektüre

#Ete: Eine Patentschrift besteht aus

#Aue: Anspruchsbereich

#Bhb2: Beschreibung

#Tur: In den Ansprüchen steht zu lesen, wass Sie nicht tun dürfen.  Jeder
Anspruch beschreibt eine Klasse verbotener Gegenstände.  Die
Beschreibung hilft bei der Auslegung der Ansprüche.  Sie soll %(e:den
durchschnittlichen Fachmann befähigen), die %(q:Erfindung)
nachzuarbeiten, ohne weiterhin erfinderisch tätig werden zu müssen. 
Die EPA-Beschreibungen enthalten jedoch im allgemeinen keine
Software-Referenzimplementation, und die eigentlich schwierige Arbeit
bleibt dem Programmierer überlassen.  Daher könnte man auch vom
Gesichtspunkt der %(e:Befähigung des Fachmanns) her gesehen die
Gültigkeit der EPA-Softwarepatente in Zweifel ziehen.

#Pto: In Patentbeschreibungen ist häufig wortreich von
Selbstverständlichkeiten wie der %(q:Zuweisung eines Blocks in einem
Speichergerät zur Speicherung einer Variablen) u.ä. die Rede.  Dies
lässt die %(q:Erfindung) besonders %(q:technisch) aussehen, kann aber
auch notwendig sein, um Verteidigungslinien für Rückzugsgefechte im
Fall von Rechtsstreit aufzubauen.  In jedem Falle muss der weniger
juristisch interessierte Leser lernen, dieses Wortgeklingel zu
überlesen.

#Acs: Ein amerikanisches Patent-Gruselkabinett, das Preise für diejenigen
ausschreibt, die ärgerlich gewordene Patente mit neuheitsschädigenden
Dokumenten zu Fall bringen.

#Tsg: Der Artikel zeigt anschaulich, wie man das Wortgeklingel in
Softwarepatentschriften überliest und sie im Geiste auf das
wesentliche reduziert.  Allerdings führt Stallmans Herangehensweise
nicht zum Verständnis der juristischen Zusammenhänge, aus denen heraus
eine Patentschrift entsteht.

#PaK: Probleme und Ungereimtheiten der Softwarepatentierung aus der Sicht
eines Prüfers am Deutschen Patent- und Markenamt -- zeigt sehr
scharfsinnig warum Softwarepatente so sind wie sie sind und welche
weiteren Gefahren eine Änderung des Art 52 bringen würde.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatpikta ;
# txtlang: de ;
# multlin: t ;
# End: ;

