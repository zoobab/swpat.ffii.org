<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#nrc: Base de Dades i Exemples

#Wai: Com llegir descripcions de patents

#ssr: Més Lectura

#Ete: Una descripció de patent consisteix en

#Aue: suposats drets

#Bhb2: descripció detallada

#Tur: Els suposats drets diuen el que no estàs autoritzat a fer.  Cadascun
d'ells defineix una classe d'objectes prohibits.  La descripció ajuda
a interpretar el significat dels suposats drets.  Se suposa que ha de
donar prou instrucions com per %(e:permetre) la %(e:persona que hi
entengui) de reimplementar l'%(q:invent) sense engegar activitats
inventives més avançades.  De tota manera, les descripcions de les
patents de programari de l'EPO generalment fallen a l'hora de proveir
una implementació de referència, i la part dura se la deixa normalment
al programador.  Així que, fins i tot des del punt de vista de la
%(e:permisibilitat), es poden posar molts dubtes en la validesa de la
major part de patents de programari de l'EPO.

#Pto: Les descripcions de patents i suposats dret fan servir molta parla
estranya sobre %(q:localització d'un bloc d'espai per a una variable
en un aparell de memòria) etc.  Això serviria només per fer que
l'%(q:invent) sembli %(q:tècnic), però també prepara línies de
retirada per possibles processos judicials.  En qualsevol cas, un
lector no interessat en termes legals haurà d'aprendre a tractar-ho
com a soroll.

#Acs: Una mena de galeria d'horrors de patents, que fa que les empreses que
fan front a demandes de patents paguin gent per trobar treballs previs
a les patents per ells.

#Tsg: Aquest article fa una bona feina mostrant com coses trivials es fan
sonar complicades a les descripcions de patents.  Però no serà de molt
ajut si no apreciem per què és això.  Les patents, després de tot, no
han de ser bons llibres de text sinó més aviat donar una definició
extremadament acurada del què no estàs autoritzat a fer.

#PaK: Problemes i inconsistències de patentar programari des de la
perspectiva d'un formador de programadors i actualment examinador a
l'Oficina Alemana de Patents i Marques -- un article molt lúcid que
mostra per què les patents de programari són el que són i demostra
sense pietat una enorme xarxa d'arguments fal·lacis de qui ha promogut
les patents de programari a Alemània.  Traducció anglesa disponible.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatpikta ;
# txtlang: ca ;
# multlin: t ;
# End: ;

