#!/usr/bin/scsh \
-e main -s
!#

;; ####### SUBR ###########

(define-syntax setq
  (syntax-rules ()
  ((setq var value)
   (begin
      (set! var value)
         var))))

(define (numpad num len . rest)
  (define pad (or (and (not (null? rest)) (car rest)) #\0))
  (let ((str (number->string num))) 
    (let ((padlen (- len (string-length str))))
      (if (> padlen 0)
	  (string-append (make-string padlen pad) str)
	  str
	  ) ) ) )

(define (rtrim str)
  (define p)
  (and (string? str)
       (begin 
       	 (set! p (string-length str))
         (let loop () (and (> p 0) (char-set-contains? char-set:whitespace (string-ref str (- p 1))) (begin (set! p (- p 1)) (loop))))
        (substring str 0 p))) )

(define (grep re file)
  (define port)
  (define line)
  (define found #f)
  (and 
   (file-exists? file)
   (begin
     (set! port (open-input-file file))
     (let loop ()
       (set! line (read-line port))
       (cond
	((not (string? line)) #f)
	((string-match re line) (set! found line))
	(else (loop)) ) )
     (close port) ) )
  found )

(define (ltrim str) (match:substring (string-match (rx (: (* blank) (submatch (* any)))) str) 1))

(define (lrtrim str) (ltrim (rtrim str)))

(define colonsplit (infix-splitter (rx (: ":" (+ " ")))))
(define linesplit (infix-splitter "\n"))

(define (safe-join-strings strings sep) (join-strings (delete #f (map (lambda (elm) (and (string? elm) elm)) strings)) sep))

(define (readable-files . files)
  (delete #f (map (lambda (file) (and (file-exists? file) (file-readable? file) file)) files)) )

;; ####### CONFIG ########

(define hostname (string->symbol (rtrim (run/string (hostname)))))
(define ffiidir (case hostname ((genba) "/var/www") (else (or (getenv "FFII") "/ul/sig/srv/res/www/ffii"))))
(define muplidir (path-list->file-name (list "swpat" "vreji" "pikta" "mupli") ffiidir))
(define force-fetch #f) ; get everything again even if it is already on disk
(define fetch-pdf-files (eq? hostname 'genba))

;; ######## MAIN ########

(define epnum (let ((match (regexp-search (rx (: "ep" (submatch (+ numeric)))) (car (reverse (split-file-name (cwd))))))) (and match (string->number (match:substring match 1)))))

(define (data2recs)
  (define file)
  (define port)
  (define linerecs '())
  (define linerec)
  (define line)
  (define key)
  (define value)
  (and
   (or
    (and (not force-fetch) (grep "Claims" "data.txt"))	
    (and 
     epnum
     (exec-path-search "getepdat" exec-path-list)
     (begin (run (getepdat ,epnum) (> data.txt)) (grep "Claims" "data.txt"))
     ) )
   (begin
     (setq port (open-input-file "data.txt"))
     (let loop ((line (read-line port)))
       (and 
	(string? line)
	(begin
	  (and
	   (> (length (setq linerec (colonsplit line))) 1)
	   (setq key (car linerec))
	   (setq key (string->symbol key))
	   (setq value (cadr linerec))
	   (setq value (string->number value))
	   (set! linerecs (alist-update key value linerecs)) )  
	  (loop (read-line port)) ) ) ) 
     (close port) ) ) 
  (and (not (null? linerecs)) linerecs) )

(define (pgpdf pg) (format #f "~a.pdf" (numpad pg 2)))

(define (recs2pages pg_from pg_tolt)
  (define pages '())
  (and
   (number? pg_from)
   (number? pg_tolt)
   (> pg_from 0)
   (> pg_tolt pg_from)
   (begin
     (setq pages '())
     (let loop ((pg pg_from))
       (and
	(< pg pg_tolt)
	(begin
	  (set! pages (append pages (list (pgpdf pg))))
	  (loop (+ 1 pg))
	  ) ) ) ) )
   pages )

(define (pdcat pdffile . files)
  (or (null? files) (begin (delete-filesys-object pdffile) (run (pdcat ,@files ,pdffile)) (for-each delete-filesys-object files))) )

(define (pdcatpages pg_from pg_tolt pdffile)
  (apply pdcat pdffile (apply readable-files (recs2pages pg_from pg_tolt))) )

(define (pdcatparts pdffile . args)
  (define parts '())
  (let loop ((args args)) (or (< (length args) 2) (begin (set! parts (append parts (list (cons (car args) (cadr args))))) (loop (cdr (cdr args))))))
  (set! parts (delete #f (map (lambda (part) (let ((file (cdr part))) (and (file-exists? file) (file-readable? file) part))) parts)))
  (or (null? parts) 
      (begin 
	(delete-filesys-object pdffile)
	(run (pdcat ,@(apply append (map (lambda (part) (list "-I" (car part) (cdr part))) parts)) ,pdffile))
	(for-each delete-filesys-object (map cdr parts)) ) ) )

(define (epget)
  (define epstr (numpad epnum 7))
  (define epfile)
  (define epurl)
  (let loop ((pg 1))
    (set! epfile (pgpdf pg))
    (set! epurl (string-append "http://l2.espacenet.com/dips/bns.pdf?PN=EP" epstr "&ID=EP+++" epstr "B1" "+I+&PG=" (number->string pg)))
    (and
     (eq? 0 (run (wget -O ,epfile ,epurl)))
     (file-exists? epfile)
     (string-match "PDF" (run/string (file ,epfile)))
     (loop (+ pg 1))
     ) )
  (delete-file epfile)
  #t )

(define (ephtml_ok) (and (grep "Priority" "abstract.html") (grep "Priority" "abstract.txt") (grep "Description" "desc.html") (grep "Claims" "claims.html") #t))

(define (epgethtml)
  (or
   (and (not force-fetch) (ephtml_ok))
   (begin
     (run (epgetdat ,(format #f "EP~a" (numpad epnum 7))))
     (for-each (lambda (sym) (let* ((base (symbol->string sym)) (html (string-append base ".html")) (txt (string-append base ".txt"))) (and (file-exists? html) (run (lynx -dump -nolist ,html) (> ,txt))))) '(claims desc abstract)) 
     (ephtml_ok) ) ) ) 

(define (epgetpdf)
  (define linerecs (data2recs))
  (define pg_dsc)
  (define pg_clm)
  (define pg_img)
  (define pg_end)
  (define (lrval key) (let ((found (assoc key linerecs))) (and found (cdr found))))
  (define (pg_ok) (> (setq pg_end (+ 1 (length (glob "{0,1,2,3,4,5,6,7,8,9}*.pdf")))) pg_clm))
  (and
   linerecs
   (or force-fetch (if (file-exists? "pikta.pdf") (begin (run (ls -l pikta.pdf)) #f) #t))
   (setq pg_dsc (lrval 'Desc))
   (setq pg_clm (lrval 'Claims))
   (or (pg_ok) (begin (epget) (pg_ok)))
   (begin
     (set! pg_img (lrval 'Drawing))
     (if (< pg_img pg_clm) (set! pg_img #f))
     (pdcatpages 1 pg_dsc "title.pdf")
     (pdcatpages pg_dsc pg_clm "descr.pdf")
     (pdcatpages pg_clm  (or pg_img pg_end) "claims.pdf")
     (pdcatpages pg_img pg_end "drawings.pdf")
     (pdcatparts "pikta.pdf" "Title" "title.pdf" "Description" "descr.pdf"  "Claims" "claims.pdf" "Drawings" "drawings.pdf") ) ) ) 

(define (epbiblio2lines)
  (define port)
  (define lines '())
  (set! port (open-input-file "abstract.txt"))
  (let loop ((line (read-line port)))
    (cond
     ((or (not (string? line))) #f)
     ((string-match "_____________" line) #t)
     (else (and (setq line (lrtrim line)) (> (string-length line) 0) (set! lines (append lines (list line)))) (loop (read-line port)))
     )
    )
  (close port)
  lines ) 

(define (lines2alist lines)
  (define nbrlines (length lines))
  (define alist '())
  (define line)
  (define match)
  (define key "Title")
  (define val #f)
  (define strings '())
  (let loop ((n 0))
    (and 
     (< n nbrlines)
     (begin
       (set! line (lrtrim (nth lines n)))
       (cond
	((setq match (string-match (rx (: (submatch (: alphanum (+ any))) ":" (* blank) (submatch (* any)))) line)) (set! alist (alist-update key val alist)) (set! key (match:substring match 1)) (set! val (match:substring match 2)))
	(else (set! val (safe-join-strings (list val line) " ")))
	)
       (loop (+ n 1)) ) ) )
  alist
  )

(define (alist2data alist)
  (define data '())
  (define tup)
  (define key)
  (define keysym)
  (define val)
  (define port (open-output-file "absdata"))
  (let loop ((alist alist))
    (and
     (not (null? alist))
     (begin
       (set! tup (car alist))
       (set! key (car tup))
       (set! keysym (cond
	  ((string-match "itle" key) 'title) 		  
	  ((string-match "quivalent" key) 'equiv)
	  ((string-match "EC Class" key) 'ecclass)
	  ((string-match "IPC Class" key) 'ipclass)
	  ((string-match "Priority" key) 'prior)
	  ((string-match "Application" key) 'applnr)
	  ((string-match "Requested" key) 'requested)
	  ((string-match "Applicant" key) 'applicant)
	  ((string-match "Inventor" key) 'inventor)
	  ((string-match "Publicat" key) 'pubdat)
	  ((string-match "Patent N" key) 'patentnr)
	  (else #f)
	  ) )
       (if keysym
	   (begin	    
	     (set! val (cdr tup))
	     (set! data (alist-update keysym val data)) )
	   (format 1 "warning: unknown abstract key ~a!!!\n" key) )
       (loop (cdr alist)) ) ) ) 
  (format port (safe-join-strings (map (lambda (tup) (format #f "~a: ~a" (symbol->string (car tup)) (cdr tup))) data) "\n"))
  (close port)
  (and (assoc 'prior data) data) )   

(define (htmlbody alist)
  (define dlist (safe-join-strings (map (lambda (tup) (format #f "<dt>~a:<dd>~a" (car tup) (cdr tup))) (append alist (delete #f (list (cons "Patent Application" (join-strings (list "<a href=\"claims.html\">claims</a>" "<a href=\"desc.html\">description</a>" "<a href=\"abstract.html\">abstract</a>") ", ")) (let ((file "img/en/pikta.pdf")) (and (file-exists? file) (cons "Granted Patent (B1)"  (format #f "The EPO has published only image files of individual pages, which we have concatenated into one huge <a href=\"~a\">PDF file</a> (~d bytes)" file (file-size file))))))))) "\n"))
  (format #f "<h1>EP ~a:<br>~a</h1>\n<dl>\n~a\n</dl>" (number->string epnum) (cdr (assoc "Title" alist)) dlist) )

(define (index_html)
  (define port)
  (define alist '())
  (define data '())
  (define (epgetdata) (and (epgethtml) (setq alist (lines2alist (epbiblio2lines))) (setq data (alist2data alist))))
  (and 
    (or (not (null? (epgetdata))) (let ((force-fetch #t)) (epgetdata)))
    (begin
      (set! port (open-output-file "index.html"))
      (format port "<html>\n<title>EP ~a</title>\n<body>\n~a\n</body>\n</html>" (number->string epnum) (htmlbody alist))
      (close port) ) ) )

(define (cdgetpdf)
  (or (file-exists? "img") (create-directory "img"))
  (with-cwd "img" 
	    (or (file-exists? "en") (create-directory "en"))
	    (with-cwd "en" (epgetpdf)) ) )

(define (main cmdline)
  (and fetch-pdf-files (cdgetpdf))
  (index_html)
  )

; Local Variables:
; mode: scheme
; coding: utf-8
; End:
