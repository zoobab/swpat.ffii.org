; -*- coding: utf-8 -*-
; buffer-file-coding-system

(require 'mlhtrc)
(vrequire 'swpatdir)

;; AT BE Cy CH IRL LIE MO GR NL

(list 'swpatnews-dir
(mlhtdoc 'swpatnews nil nil nil
(filters ((sm ahs 'swpatmrilu) (na ahs 'swpatcnino)) (ML Gxe "Getrennt hiervon finden Sie %(sm:aktuelle Nachrichten und Diskussionen in Netzforen) und %(na:Kommentierte Verweise auf fremde Nachrichten aus der SWPAT-Welt)." "We provide %(sm:current news on discussion lists) and %(na:commented links to other news sources) separately." (fr "En dehors de cette page, vous pouvez trouver des %(sm:actualité trouvée sur des listes de diffusion) %(na:liens commentés vers d'autres ressources du monde des brevets logiciels)")))
(newsitems
(DCL '(2000 12 20) 
  (ML Fvf "Fünf Gesetzesinitiativen zum Schutz der Informatischen Innovation" "Five Law Initiatives for the Protection of Information Innovation" "Cinque Initiatives de Loi pour la Protection de l'Innovation Informatique")
  'phm 'swnpatg2C
  (ML Ese "Einige IT-Unternehmen und Verbände fordern in einem Offenen Brief fünf Maßnahmen zur Beschränkung des Patent(un)wesens: Meinungs- und Ausdrucksfreiheit für Programmierer, Recht auf freien Zugang zu öffentlichen Kommunikationsnormen, präzise Begrenzung der Patentierbarkeit, sachgerechte Förderung informatischer Innovation und Entbürokratisierung des Patentwesens." "Prominent German IT companies and associations demand measures for the limitation of the patent system:  freedom of expression for programmers, right of free access to public communication standards, precise limitation of patentability, adequeat systems for the promotion of information innovation and deburaucratisation of the patent system." "Des entreprise et associations informatiques allemandes demandent 5 mesures pour arreter l'expansion du système de brevets et limiter son impacte sur l'informatique.") )
(DBL
 '(2000 11 20)
  (ML Gdn "Gruselkabinett der Europäischen Softwarepatente" "Horror Gallery of European Software Patents" "Cabinet d'Horreurs des Brevets Logiciels Européens")
 'phm 'swpatpikta
  (filters ((db ahs 'swpatpikta)) (ML Dei "Der FFII hat etwas mehr Licht in den Dschungel der europäischen Patentdaten gebracht.  In der heute veröffentlichten %(db:Softwarepatente-Datenbank) findet man erstmals die vom Europäischen Patentamt gewährten Softwarepatente in übersichtlicher Form, so dass Programmierer endlich ermessen können, worum es bei der Debatte geht." (en "The FFII has brought some more light into the jungle of European patent data.  Via the %(db:software patent data base) published today, programmers can at last browse through lists of software patents granted by the European Patent Office and get an impression of what the discussion is really about.") (en "La FFII a publié une %(db:base de données) de brevets logiciels accordés par l'Office Européens de Brevets.")))
)
(DA9
 '(2000 10 9)
 (ML TWd "Tagung %(q:Wem gehört das Wissen?)")
 'phm 'swpboell2A
 (ML Dnt "Der FFII veranstaltet am 20. Oktober in Berlin gemeinsam mit der Heinrich-Böll-Stiftung eine Tagung über das %(q:digitale Dilemma) und mögliche Mechanismen zur Belohnung geistigen Schaffens von Patenten über Urheberrechtsabgaben, Mikro-Spenden bis zur direkten Förderung der Informationsallmende aus Steuermitteln.  Führende Richter, Anwälte, Patentprüfer, IT-Unternehmer und Gelehrte diskutieren über die brennende Frage der Softwarepatentierbarkeit.")
)
(D82
 '(2000 8 2)
 (ML Fri "SMEs criticize EPO base proposal for no-limits patentability" (de "KMU kritisieren EPA-%(q:Basisvorschlag) für grenzenlose Patentierbarkeit") (fr "Contre le %(q:projet) de l'OEB d'une brevetabilité sans limitations"))
 'phm 'swnepue28
 (filters ((dk ahs 'swpepue2B) (ob ahs 'swxepue28)) (ML DiW "The European Patent Office will host a %(dk:diplomatic conference) on 2000-11-20..29 in Munich, which is to decide far reaching changes of the European patent system.  A %(q:base proposal) has just been published, which proposes no-limits patentability for computer programs, business methods and anything the EPO may want to grant patents on.  The EPO has published an %(ob:open letter to the German Ministery of Justice), which has been endorsed by some leading software companies." (de "Das Europäische Patentamt ist am 20.-29. November 2000 in München Gastgeber einer %(dk:diplomatischen Konferenz), die weitreichende Änderungen der grundlegenden Gesetzesregeln des europäischen Patentwesens beschließen soll.  Dabei ist u.a. geplant, das Tor zur Patentierung immaterieller Gegenstände weit zu öffnen.  Nicht nur soll das schon seit spätestens 1997 %(q:de facto überholte) Recht von Softwareautoren, ihre in eigenständiger Arbeit entwickelten Werke unbeschadet von Patentansprüchen Dritter zu verbreiten und zu verkaufen, nun offiziell aus dem Gesetzestext gestrichen werden.  Ferner sollen auch Geschäftsverfahren, Lehrmethoden, Heilmethoden, Verfahren zur Organisation des öffentlichen Lebens, mathematische Methoden, geistige Methoden u.s.w. zum Gegenstand von Patentansprüchen werden können, soweit diese Verfahren über Computer oder andere technische Geräte abgewickelt werden.  Neuere Studien zeigen, dass das Europäiche Patentamt bei der Bewertung der Erfindungshöhe immaterieller Verfahren noch niedrigere Maßstäbe anlegt als das amerikanische und Japanische Patentamt.  Trotz Petitionen und scharfer Kritik von 27000 Bürgern, 300 IT-Unternehmen und 250 Politikern spricht das Europäische Patentamt von einem %(q:breiten Konsens für Softwarepatente) und seine Pläne gelten als beschlossene Sache.  Der FFII zeigt gemeinsam mit einigen Softwarefirmen in einem %(ob:offenen Brief) an das Justizministerium die Gefahren auf.") (fr "Du 20 au 29 novembre 2000, à Munich, L'Office Européen des Brevets organise une %(dk:conférence diplomatique) afin de modifier de fond en comble les règles légales régissant le dépôt de brevet en Europe. Il est ainsi prévu, entre autres, d'ouvrir largement les portes à une brevetabilité des actifs immatériels. Non contents d'avoir dans les faits, depuis fin 1997, fait en sorte que le droit des auteurs de logiciels de publier et de vendre librement leurs productions personnelles à des tiers soit %(q:dépassé par la réalité), ils veulent maintenant le rayer officiellement des textes de lois. Plus encore, les concepts-métiers, les méthodes d'enseignement, les méthodes de soins, les concepts d'administration publique, les concepts et méthodes mathématiques ou spirituelles etc... tous ces domaines relèveront également des brevets parce qu'ils auront été développés sur des ordinateurs ou autres appareils. De nouvelles études l'ont montré : l'Office Européen des Brevets pose des critères encore plus défavorables que ses équivalents américain et japonais, à la valorisation de l'inventivité des processus immatériels. Malgré les pétitions et une forte contestation de 27 000 citoyens, 300 entreprises du secteur logiciel et 250 hommes politiques, l'OEB parle d'un %(q:large consensus en faveur des brevets logiciels) et présente ses projets comme une décision déjà arrêtée. En commun avec des entreprises de logiciels, la FFII publie une %(ob:lettre ouverte) au Ministère de la Justice dans laquelle elle explicite les dangers à venir."))))
(D74
 '(2000 7 4)
 (ML Lrt "Linuxtag 2000 für ein softwarepatentfreies Europa" "Linuxtag for a software patent free Europe" (fr "Les Journées Linux 2000 à Stuttgart : délivrer l'Europe de tout brevet sur les logiciels"))
 'phm 'swplxtg26
 (filters ((pk ahs 'swpatlijda)) (ML Dep "Die größte europäische Linux-Messe stand im Zeichen des Kampfes gegen die Patentierung von Programmlogik, welche derzeit von der deutschen und europäischen %(pk:Patentokratie) gesetzeswidrig und gegen den Willen der Softwareindustrie forciert wird.  In zahlreichen Vorträgen und Gesprächen zeigte sich ein Konsens gegen Softwarepatente, der selbst Politiker vom BMWi und Großfirmen wie Siemens bis zu einem gewissen Grad erfasst hat.  Das Minenfeld der Programmlogikpatente wird einhellig als ein abschreckendes Investitionsrisiko für alle gesehen, die in Softwareentwicklung investieren wollen." "The largest European Linux Fair was strongly influenced by the struggle against the expansion of the patent system toward programming logics, which is currently being pushed by the European %(pk:patentocracy),in defiance of the Law and against the will of the software industry.  Numerous lectures and speeches showed a broad opposition against software patentability which included to some extent even politicains from the German ministery of Economics and decisionmakers at Siemens.  Proprietary Software makers view the current patent inflation as a liability and a grave risk for anyone who invests in programming." (fr "La plus grande Exposition Linux d'Europe s'est tenue sous le signe du combat contre la brevetabilité des algorithmes, contre la volonté de l'actuelle %(pk:\"patentocratie\") européenne de l'imposer par la force, en toute illégalité et au mépris des intérêts de l'industrie du logiciel. Tout au long de nombreuses conférences et de nombreux entretiens, un consensus s'est dégagé, contre les brevets logiciels. Les représentants officiels de BMW Industries et d'autres grandes entreprises comme Siemens se sont eux aussi ralliés à cette opinion, au moins dans une certaine mesure. Le champ de mines créé par les brevets sur les algorithmes est unanimement dénoncé comme un risque effrayant pour tous les investisseurs du secteur du développement informatique.")))
)
(D6D 
 '(2000 6 13)
 (ML EWp "Eurolinux Petition for a Software Patent Free Europe" (de "Eurolinux-Petition für eine softwarepatentfreies Europa") (fr "La pétition Eurolinux pour une Europe sans brevets logiciels"))
 'phm 'epatD6
  (filters
   ((hs ahs 'bolkestein26) (pk ahs 'swpatpikci))
   (ML Tra "The Eurolinux Alliance of European software companies and OpenSource associations has launched a campaign to warn European legislators against the dangers of software patents.  This campaign is a response of IT associations and software companies to recent %(hs:hardline proprietarist speeches from the European Commission's Directorate for the Internal Market), which indicated that the Directorate will issue a directive to extend the patent system to software and intellectual methods, completely ignoring the concerns raised by leading software companies and developpers, and refusing even to study the economic effects of software patenting.  %(pk:Previous campaigns) to this subject had quickly assembled approximately 10000 signatures from software developpers.  The current campaign will go further.  Based on clear demands and thourough documentation, the Eurolinux Alliance will submit its petition to the European parlament and use advanced (not yet patentable) e-techniques to let volunteers participate in lobbying the key decisionmakers." (de "5 europäischer OpenSource-Vereinigungen und 10 Softwarefirmen haben unter dem Namen %(q:Eurolinux-Bündnis) eine Kampagne auf den Weg gebracht, um Europas Gesetzgeber vor den Gefahren der Softwarepatentierung zu warnen.  Sie antworten damit auf kürzliche %(hs:betonhart proprietaristische Reden aus der Binnenmarkt-Generaldirektion der Europäischen Kommission), aus denen hervorgeht, dass die Generaldirektion eine Direktive veröffentlichen wird, um das Patentsystem auf Software und Geistige Verfahren auszuweiten, ohne die wirtschaftlichen Auswirkungen dieser Ausweitung studiert zu haben.  Bei %(pk:früheren Kampagnen) zu diesem Thema waren schnell etwa 10000 Unterschriften zusammengekommen.  Die Eurolinux-Kampagne wird weiter gehen.  Sie richtet sich an das Europaparlament und stützt sich auf umfangreiche Argumentationshilfen und WWW-Applikationen, mit denen Freiwillige in die Lage versetzt werden sollen, gezielte Überzeugungsarbeit zu leisten.") (fr "5 associations européennes pro-OpenSource, et 10 sociétés de logiciel, sous le nom d'%(q:Alliance Eurolinux), ont lancé une campagne afin de sensibiliser le législateur européen aux dangers de la brevetabilité des logiciels. Il s'agit d'une réponse aux %(hs:arguments-massues des tenants du logiciel propriétaire à la Direction Générale du Marché Intérieur de la Commission Européenne), d'où il ressort que cette direction s'apprête à publier une directive pour étendre le système des brevets aux logiciels et autres oeuvres de l'esprit. Cette direction s'est-elle préoccupée des conséquences économiques d'une telle décision ? Lors des précédentes campagnes sur ce sujet, environ 10 000 signatures ont été réunies tout de suite. Avec la campagne Eurolinux, nous voulons aller plus loin. Nous voulons nous adresser au Parlement Européen. Nous nous appuyons sur une argumentation large et multiforme. Quelques volontaires travaillent à la traduction en plusieurs langues à l'aide d'outils logiciels performants."))) )
(D68 '(2000 6 8) (ML FKW "FFII plant SWPAT-Konferenzprogramm für LinuxTag 2000" "FFII plans swpat conference program for LinuxTag 2000") 'phm 'swplxtg26 (ML Age "Auf Einladung des FFII werden Deutschlands kompetenteste Patentjuristen Konzepte vorlegen, wie man das Patent-Ungeheuer mit seinen eigenen Mitteln zähmen kann.  Wir wollen uns zwar auf alle Möglichkeiten einlassen, laden aber zugleich mit umfangreicher Dokumentation und vorgefertigten Petitionsbriefen dazu ein, die Softwarepatent-Gesetzgebung der EU zu kippen." "FFII has invited renowned patent law specialists to teach computer scientist how to protect themselves against patents by using patents.  At the same time FFII is inviting the public to sign petitions and make an all out effort to prevent the EU from legalising software patents in the first place.")
)
(D64 '(2000 6 4) (ML Oae "ORF-Sendung: Patente zerstören das Internet" "Lessig: Patents destroying the Internet" (fr "Une émission de la radio autrichienne ORF : les brevets détruisent l'Internet")) 'phm 'orf-lessig-swstal (filters ((ko ahv 'orf-lessig-feind)) (ML Iei "In einer Sendung des Österreichischen Rundfunks warnt Harvard-Verfassungsjurist Prof. Lawrence Lessig, der die US-Regierung im Microsoft-Prozess beriet, vor einer erneuten Bedrohung der offenen Gesellschaft 10 Jahre nach dem Fall der Berliner Mauer.  Eine rigide Ideologie des Geistigen Eigentums (%(q:Software-Stalinismus)) schaffe derzeit weitgehend unbehelligt Reglementierungen, die in wenigen Jahren das ehedem so lebendige Internet zu einem zweiten Fernsehen degradieren könnten.  Lessig %(ko:kritisiert die OpenSource-Szene wegen ihrer manglenden Bereitschaft, ihrerseits die Mittel der Gesetzgebung zu nutzen, um die informationelle Infrastruktur gegen Aneignungsversuche großer Interessengruppen zu schützen).  Die ORF-Webseiten verweisen auch auf den FFII." "In an interview with Austrian radio, Harvard's Lessig warns that 10 years after the fall of the Berlin wall, freedom is again under threat.  This time by a rigid ideology of intellectual property (%(q:software stalinism)), which is regulating Internet and rapidly turning it into a second television-like medium.  Lessig also %(ko:criticises the OpenSource movement's %(q:naive) reluctancy to promote the kind of regulation needed for protecting the public domain against appropriation by Big Business).  The ORF website also points to FFII." (fr "http://futurezone.orf.at/futurezone.orf?read=detail&id=30747&tmp=55012"))))
(D5T '(2000 5 29) (ML Ebr "EU-Kommissar bekräftigt harte SWPAT-Linie" "EU commissioner restates hardline SWPAT policy approach") 'phm 'bolkestein26  (let ((FB "Frits Bolkestein")) (filters ((dg pet (ahv 'eu-dg15 "DG 15"))) (ML Dta "Der für die %(dg:Binnenmarkt-Generaldirektion) und damit für die Patentpläne zuständige EU-Kommissar %{FB} plädiert vor einem Publikum von Patentrechtlern für eine zügige Auweitung des europäischen Patentwesens.  Die %(q:Neue Ökonomie) Europas kann laut Bolkestein nur dann gedeihen, wenn es Unternehmen viel leichter gemacht wird, Ausschlussrechte auf Softwareideen und Wissen aller Art zu erwerben." "The EU-Commmissioner for the %(dg:Internal Market), %{FB}, promises rapid expansion of the patent system before an audience of patent professionals." (fr "http://europa.eu.int/comm/internal_market/")))))
(D5P 
'(2000 5 25) (ML MWe "MIT-Ökonomen rechnen vor: Softwarepatente bremsen Fortschritt" "MIT Economists calculate how software patents hamper innovation" (fr "D'après les calculs des économistes du MIT, les brevets sur les logiciels freinent le progrès ")) 'phm 'mit-seqinnov (let ((MIT "Massachusets Institute of Technology")) (ML Zio "Zwei Wirtschaftswissenschaftler des %{MIT} stellen in ihrer Studie zunächst fest, dass die Patentierbarkeit von Software bei amerikanischen Unternehmen zu keiner wesentlichen Zunahme sondern eher zu einer Abnahme der Investitionen in Grundlagenforschung und Innovation geführt hat.  Sie entwickeln dann ein Rechenmodell, anhand dessen sich Innovation voraussagen lassen soll, und zeigen, dass die Einführung von Softwarepatenten zum Erlahmen der Innovationstätigkeit führt." "Two economists of the %{MIT} establish a theoretical model to predict the occurence of innovation in different technologies, and show why software patents slow down innovation." (fr "Une étude menée par deux chercheurs du %{MIT} démontre dans un premier temps que dans les entreprises américaines la brevetabilité des logiciels, loin de le favoriser, a sensiblement découragé les investissements dans les domaines de la recherche fondamentale et de l'innovation. Dans un deuxième temps, elle développe ensuite un modèle de calcul prévisionnel qui démontre que l'introduction de la brevetabilité des logiciels conduit nécessairement à un engourdissement de l'activité innovante."))))
(D5I 
 '(2000 5 18) (ML Bol "BMWi-Konferenz zeigt: Softwarefirmen wollen keine Patente" "German Software Companies ask Government to Oppose Software Patenting" "Consensus des PMEs allemands contre le brevet logiciel auprés du Ministère de l'Économie") 'phm 'swpbmwi25 (let ((LST (commas  "ID-PRO AG" "Infomatec AG" "Intradat AG" "Phaidros AG" "SuSE Linux AG"))) (ML Een "Etwa 40 Vertreter von Softwarefirmen, Patentämtern, Ministerien, IT-Verbänden und Hochschulen trafen sich im BMWi, um über die Auswirkungen von Softwarepatenten auf Wirtschaft und Informationsgesellschaft zu beraten.  Vertreter der %{LST} und einiger kleinerer Firmen kritisierten die Vergabe von Softwarepatenten durch die Patentämter und verlangten, im Softwarebereich solle nur das Urheberrecht zur Anwendung kommen.  Vertreter der Patentabteilungen von Siemens, IBM, Patentämtern und Patentanwaltsverbänden hielten mit juristischen und moralischen Argumenten dagegen.  Der FFII leistete diverse Beiträge zur Konferenz." "About 40 Representatives of software companies, patent offices, government agencies, IT associations and universities met in the German Ministery of Economics in order to discuss about the effects of software patents on the economy and on information society.  Representatives of %{LST} and several smaller companies criticized the granting of software patents by patent offices and demanded that in the area of software only copyright should apply.  Representatives of the patent departments of IBM and Siemens as well as patent attornies and patent lawyer associations defended the patenting practise with legal and moral arguments.  The FFII contributed to the conference in various ways.")))
(D5H 
 '(2000 5 17)
  (ML Smk "SWPAT-Dokumentations-CD fertig" "FFII SWPAT Documentation CD ready") 'phm 'swpdoku
  (ML FWr "Auf der Mitgliederversammlung präsentierte Bernhard Reiter die vom FFII bestellte CD %(q:Softwarepatentierung - Aspekte der Debatte) und liefert 10 Exemplare zur Verteilung auf der Konferenz des nächsten Tages ab." "At the FFII's annual member conference Bernhard Reiter presented a free CD documentation on Software patents that had been ordered by the FFII and delivered 10 copies for distribution on the next day's conference.") )
(vdubD5D 
 '(2000 5 14) (ML Mta "Microsoft untersagt Unterstützung von patentiertem Dateiormat in freier Software" "Microsoft prohibits support for patented file format in free software" (fr "Microsoft interdit aux logiciels libres de supporter les formats de données brevetés")) 'phm (case lang (de 'heise-asfpat) (t 'avogado-asfpat)) (let ((VD (ahv 'virtualdub))) (ML Ant "Auf Forderung von Microsoft muss der Entwickler des freier Filmbearbeitungsprogramms %{VD} die Unterstützung für das patentierte ASF-Format aus seinem Programm entfernen.  Durch die Patentierung eines trivialen Formates und gleichzeitige aggressive Vermarktung des ASF als Industriestandard dürfte es Microsoft gelingen, zwei Fliegen mit einer Klappe zu schlagen:  (1) Vertrauensgewinn bei Inhalteanbietern dank Unterbindung der Kopierbarkeit (2) Behinderung der Entwicklung konkurrierender Betriebssysteme.  Hier wird das Patentwesen nochmals zweckentfremdet." "Upon request from Microsoft, the developper of a GPL'ed Windows video software %{VD} removed support for the patented Advanced Streaming Format (ASF) from his software.  By patenting a trivial format and pushing it as an industry standard, Microsoft is able to prevent the copying of content and thereby attract commercial content providers to its platform." (fr "A la demande de Microsoft, le développeur du programme de manipulation de vidéo %{VD} a dû retirer de son programme son support pour le format breveté ASF. Grâce à un brevet déposé sur un format trivial, ASF, et à un marketing agressif pour l'inposer parallèlement comme un standard industriel, l'industriel Microsoft fait d'une pierre deux coups : (1) Il gagne la confiance des offreurs de contenu en empêchant la reproduction des documents (2) Il handicape le développement d'un système d'exploitation concurrent. Voilà un nouvel exemple de détournement du brevet à d'autres fins."))))
(D53 
 '(2000 5 5) (ML Knd "BMWi lädt zu Konferenz über Auswirkungen der Patentierbarkeit von Software" "BMWi invites to Swpat conference" "BMWi invite a une conférence sur l'impacte de la brevetabilité du logiciel libre") 'phm 'swpbcpe25 (ML DWr "Das Bundesministerium für Wirtschaft und Technologie hält am 18. Mai 2000 in Berlin eine öffentliche Tagung ab, auf der die wirtschaftlichen Auswirkungen der Patentierbarkeit von Software untersucht werden sollen.  Zahlreiche große und kleine Softwarefirmen wurden eingeladen. Auf Einladung des FFII kommt der amerikanische Patent-Rechercheur Greg Aharonian." "The German Ministery of Economics is convening a public conference to investigate the economic impact of software patentability on May 18 in Berlin." "Le Ministère de l'Économie Allemand convient une Conférence sur l'Impacte Économique de la Brevetabilité du Logiciel"))
(D4I '(2000 4 17) (ML Ane "Auftrag zur Erstellung einer SWPAT-Dokumentations-CD erteilt") 'phm 'swpatdokucd (filters ((bk ahst 'swpbmwi25)) (ML FnC "Firma Intevation wird im Auftrag des FFII gegen Zeitaufwandsentschädigung termingerecht zur %(bk:Berliner Konferenz) eine CD zum Thema %(q:Patentrechte und Software) zusammenstellen.")))
(CCE '(1999 12 14) (ML 5tn "5 IT-Verbände kritisieren Ausweitung des Patentwesens" "5 IT Associations criticize expansion of patentability" "5 Associations TI allemandes critiquent l'expansion du sysème de brevets") 'phm 'fitugpe (flet ((fi (str) (tpe (ah "http://www.fitug.de" str) "FITUG e.V."))) (ML Dsr "Der %(fi:Förderverein für Informationstechnologie und Gesellschaft) wendet sich in einer Presseerklärung gegen die Pläne der EU und der Unterzeichnerstaaten des Europäischen Patentübereinkommens, den Patentschutz für Software auszuweiten.  Diese Erklärung trägt unterstützende Unterschriften von weiteren Verbänden, darunter dem Österreichischen Linuxverband LUGA, dem Virtuellen Ortsverein der SPD und dem Förderverein für eine Freie Informationelle Infrastruktur (FFII e.V.).")))
(CBK '(1999 11 18) (ML Sni "Software patent conference in Munich" (de "SWPAT-Konferenz in München") (fr "Conférence sur les nouvelles domaine de la brevetabilité a Munich")) 'phm nil (filters ((ag ahst 'swpatgirzu)) (ML Uis "Our %(ag:workgroup) took part in a conference of patent scholars at the Max Planck Institute in Munich." (de "Unsere %(ag:Arbeitsgruppe) nahm an einer Studienkonferenz über neue Felder des Patentwesens im Münchner Max-Planck-Institut teilnehmen.") (fr "Unsere %(ag:Arbeitsgruppe) nahm an einer Studienkonferenz über neue Felder des Patentwesens im Münchner Max-Planck-Institut teilnehmen."))))
(CAK '(1999 10 20) (ML Tie "EuroLinux delegation meets EU legislators" (de "Treffen mit EU-Gesetzgebern") (fr "Rencontre avec Législateurs UE") (it "Rencontro con gli legislatori UE")) 'phm 'euipCA (let ((EL "Eurolinux")) (ML EkC "A delegation of 8 representatives from the sponsors of EuroLinux and its member organisations met the patent legislators of the EU Commission's DG XV in Bruxelles to explain why current EU plans on software patentability are dangerous, and to propose a solution that could be acceptable to all parties." (de "Eine Delegation von 8 Vertretern der Mitglieder und Förderer von Eurolinux besuchte am 15. Oktober die Patentgesetzgeber der EU-Kommission / Generaldirektorat 15 (DG XV) in Brüssel, um ihnen zu erklären, warum die EU-Pläne bezüglich Patentierbarkeit von Computerprogrammen für die Softwarehersteller gefährlich sind, und um eine Lösung vorzuschlagen, die allen Seiten entgegenkommt.") (fr "Eine Delegation von 8 Vertretern der Mitglieder und Förderer von Eurolinux besuchte am 15. Oktober die Patentgesetzgeber der EU-Kommission / Generaldirektorat 15 (DG XV) in Brüssel, um ihnen zu erklären, warum die EU-Pläne bezüglich Patentierbarkeit von Computerprogrammen für die Softwarehersteller gefährlich sind, und um eine Lösung vorzuschlagen, die allen Seiten entgegenkommt.") (it "Una delegazione de 8 representanti delle organisazione membri e sponsori di EuroLinux ha visitato gli legislatori UE / DG XV in Bruxelles per loro spiegare per che gli piani de l' UE sono dangerosi por gli programmatori software, e per proponere una soluzione accetabile per tutti fianchi."))) (filters ((fs ahst 'swpatsarji) (fg ahst 'swpatgirzu) (lt ahst 'nopatltr)) (let ((JPS (tpe "J.P. Smets" (ahs 'aful)))) (ML Dwr "The delegation was headed by %{JPS}.  Travel expenses were paid by the %(fs:sponsors) of the %(fg:FFII software patent working group), most of whom also sent representatives with %(lt:letters from their CEOs), in order to express their deep concern about the expected results of the EU plans." (de "Die Delegation wurde von %{JPS} geleitet.  Die Fahrtkosten wurden von den %(fs:Sponsoren) der %(fg:Softwarepatent-Arbeitsgruppe des FFII) getragen, von denen die meisten auch Vertreter mit %(lt:Briefen ihrer Vorstände) schickten und so eindringlich ihre Sorge über die Folgen der EU-Pläne zum Ausdruck brachten.") (fr "Die Delegation wurde von %{JPS} geleitet.  Die Fahrtkosten wurden von den %(fs:Sponsoren) der %(fg:Softwarepatent-Arbeitsgruppe des FFII) getragen, von denen die meisten auch Vertreter mit %(lt:Briefen ihrer Vorstände) schickten und so eindringlich ihre Sorge über die Folgen der EU-Pläne zum Ausdruck brachten.") (it "La delegazione era guidata de %{JPS}.  Gli soldi di viaggio erano pagati per gli %(fs:sponsori) del %(fg:gruppo di lavoro FFII), e la pluparto di loro veneva con representanti e portava %(lt:lettere de gli loro patroni), chi s'esprimavano molto inchietati de gli piani de la commissione UE.")))))
(C8Q '(1999 8 26) (ML Ftn "FFII antwortet dem Patentreferenten des Bundesjustizministeriums" "FFII Response to BMJ" "Reponse FFII au BMJ") 'phm nil (ML Mct "Mitte des Monats erreichte uns ein Brief aus dem Bundesjustizministerium, der unsere Bedenken gegenüber den EU-Plänen zu zerstreuen sucht und uns in Aussicht stellt, als eine interessierte Partei in die kommenden EU-Konsultationen mit einbezogen zu werden.  Wir haben nach vereinsinterner Diskussion geantwortet.  In unserer Antwort legen wir dar, warum wir die Pläne der EU für gefährlich halten, stellen einige Fragen und bieten weiterführende Informationen an."))
(C7T '(1999 7 29) (ML Lnr "Linux-Verband kritisiert ZVEI-Brief" "Linux-Verband response to ZVEI" "Dialogue entre Linux-Verband et ZVEI") 'phm 'zvei-live-gnn
 (filters ((zd ahs 'zvei-propat-zdnet)) (let ((LV (ahs 'linux-verband)) (ZV (ahs 'zvei (tpe "ZVEI" (ML Zdn "Zentralverband der Elektronik-Industrie" "German Association of Electronic Industry"))))) (ML Dxs "Der %{LV} kritisiert die %(zd:Forderung) des %{ZV} nach Abschaffung des Patentierungsverbotes für Software." "%{LV} criticizes the %{ZV}'s %(zd:demand) for software patentability."))))
(C7S '(1999 7 28) (colons "ZVEI" (ML Zrn "Software muss patentierbar sein" "Software must be patentable" "Logiciels doivent être brevetables")) 'phm 'zvei-propat-gnn (ML Dfe "Die %(q:einschlägigen Patentierungsverbote für Computerprogramme) müssen abgeschafft und ein %(q:umfassender Patentschutz für Softwareerfindungen) ermöglicht werden, fordert der Zentralverband Elektrotechnik- und Elektronikindustrie (ZVEI) e.V.  Spätere FFII-Recherchen ergaben, dass es sich hierbei um eine Erklärung des Chefs der Siemens-Patentabteilung Arno Körber handelte, die vom ZVEI unbesehen übernommen wurden." "German Electronics Industry Association demands software patents.  This press statement was written by a well-known patent lobbyist from Siemens."))
(C7O '(1999 7 24) (ML SaW "Statistik zeigt: Softwarepatente werden ohne seriöse Prüfung gewährt" "Statistics show: software patents granted without serious examination" "Statistique montrent: brevets logiciels accordés sans examination sérieuse") 'phm 'aharC7O  (ML EvW "Eine %(ah:statistische Studie der amerikanischen Swpat-Praxis) zeigt: das US-Patentamt wird 1998-99 insgesamt 40000 Softwarepatente, also 10 mal so viel wie im Vergleichszeitraum 1993-94, gewähren und dabei 50 Millionen USD verdienen.  Derweil stehen dem US-Patentamt nicht wesentlich mehr Prüfer als damals zur Verfügung und die Prüfungsverfahren sind weitgehend Makulatur.  Zur Neuheitsprüfung werden praktisch nur Patentschriften herangezogen." "New statistics from Greg Aharonian show that software patents are issued without serious examination." (fr "statistische Studie der amerikanischen Swpat-Praxis")))
) )

(mlhtdoc 'swngrossen nil (ML AWe "Auch nach der Diplomatischen Konferenz schreitet die Patentinflation ungebremst voran.") nil

(sects
(epat (ML Zsv "Zügiger Ausbau des Gruselkabinetts der Europäischen Softwarepatente")
(ML DiA "Das Europäische Patentamt (EPA) hat bereits gegen den Buchstaben und Geist des Gesetzes ca 30000 Softwarepatente erteilt.  Das sind Patente auf %(e:Probleme), deren Lösung im Schreiben eines programmiersprachlichen Textstücks besteht.  Meistens wird dabei mit einer trivialen Idee die Umsetzung in unendlich vielen nicht-trivialen Programmen blockiert.  Zehntausende weiterer Softwarepatente warten seit 1996 auf ihre Erteilung.  Der Vorsitzende der Diplomatischen Konferenz und EPA-Funktionär Roland Grossenbacher bekräftigte in seiner Abschlussrede, dass das EPA an seiner Praxis der Softwarepatentierung unverändert festzuhalten gedenkt.") )	
(equiv (ML WAn "Weltweit breiteste Anspruchswirkung dank Äquivalenzdoktrin")
(ML DiW "Grossenbacher betonte ferner, die Konferenz habe %(q:den Schutzbereich europäischer Patente mit der ausdrücklichen Einbeziehung sogenannter Äquivalente präzisiert und verstärkt).  Die Äquivalenzdoktrin wird schon heute am EPA wesentlich extensiver ausgelegt als in Japan und den USA und führt dazu, dass viele Patente in Europa die größten Sperrwirkungen entfalten.") )
(univers (ML Dze "Doktrin der grenzenlosen Patentierbarkeit im EPÜ")
(filters ((ob ahs 'swxepue28)) (ML DVw "Die Konferenz hat festgeschrieben, dass Patente %(q:für beliebige Erfindungen auf allen Gebieten der Technik erhältlich) sein müssen.  Dieser Halbsatz, der im TRIPS-Vertrag als Nichtdiskriminierungsklausel im Dienst des Freihandels steht, liest sich im Kontext des neuen Europäischen Patentübereinkommens (EPÜ) wie eine Doktrin der grenzenlosen Patentierbarkeit.  Wir warnten in einem früheren %(ob:Offenen Brief) davor.")) )
(konsult (ML Ktn "Konsultation unter Gleichgesinnten in Brüssel")
(ML DWr "Die Europäische Kommission organisiert derzeit eine Konsultation über die Softwarepatentierung.  Diese Konsultation liegt in der Hand von Patentjuristen der Generaldirektion Binnenmarkt, die seit Jahren die Ausdehnung des Patentwesens forcieren.  Die GD Binnenmarkt präsentiert dabei einseitige Konsultationspapiere, die bis ins Detail den Standpunkt des EPA vertreten.  Die Position der Gegenseite wird nur bruchstückhaft erläutert und es fehlt jeglicher Hinweis auf kritische Studien und Webseiten.  Die Generaldirektion Binnenmarkt ist Streitpartei und Richter zugleich.  Die Europäische Patentorganisation plant derweil, in Kürze eine neue Diplomatische Konferenz abzuhalten, um dann endgültig die %(q:Programme für Datenverarbeitungsanlagen) von der Liste der nicht patentierbaren Gegenstände zu streichen.") )
(bgh (ML Ret "Richter weiten Patentwesen aus und fällen Urteil über ihre Kritiker")
(filters ((bp ahs 'bpatg17w6998)) (ML Asu "Am Bundesgerichtshof (BGH) ist eine Grundsatzentscheidung über die Patentierbarkeit von Computerprogrammen und %(q:Computerprogrammprodukten) anhängig.  Der unter Patentjuristen als %(q:konservativ) verschrieene 17. Senat des Bundespatentgerichts (BPatG) hat gesetzestreu die Patentierbarkeit verneint, aber Rechtsbeschwerde zum BGH zugelassen.  Dort nimmt sich ein %(q:progressiver) Software-Richter seit Jahren große Freiheit mit dem Gesetz, um gegenüber dem EPA %(q:aufzuholen).  Das BPatG hat die Widersprüchlichkeiten der BGH-Lehre %(bp:schonungslos kritisiert).  Nun aber soll wiederum der BGH in kleinem Kreis über eine grundlegende Frage der Informationsgesellschaft das Endurteil fällen.")) )
)
)

(mlhtdoc 'swpatnews99 nil (ML Wee "The FFII Workgroup on Software Patents is updating you on its activities and positions on ongoing developments." (de "Wir werden versuchen, Sie über unsere Aktivitäten zum Thema Softwarepatente auf dem laufenden zu halten") (fr "Nous essayons ici de vous informer en temps réel sur nos activités au sujet des brevets logiciels."))
 nil
(newsitems
(CCE '(1999 12 14) (ML 5tn "5 IT-Verbände kritisieren Ausweitung des Patentwesens" "5 IT Associations criticize expansion of patentability" "5 Associations TI allemandes critiquent l'expansion du sysème de brevets") 'phm 'fitugpe (flet ((fi (str) (tpe (ah "http://www.fitug.de" str) "FITUG e.V."))) (ML Dsr "Der %(fi:Förderverein für Informationstechnologie und Gesellschaft) wendet sich in einer Presseerklärung gegen die Pläne der EU und der Unterzeichnerstaaten des Europäischen Patentübereinkommens, den Patentschutz für Software auszuweiten.  Diese Erklärung trägt unterstützende Unterschriften von weiteren Verbänden, darunter dem Österreichischen Linuxverband LUGA, dem Virtuellen Ortsverein der SPD und dem Förderverein für eine Freie Informationelle Infrastruktur (FFII e.V.).")))
(CBK '(1999 11 18) (ML Sni "Software patent conference in Munich" (de "SWPAT-Konferenz in München") (fr "Conférence sur les nouvelles domaine de la brevetabilité a Munich")) 'phm nil (filters ((ag ahst 'swpatgirzu)) (ML Uis "Our %(ag:workgroup) took part in a conference of patent scholars at the Max Planck Institute in Munich." (de "Unsere %(ag:Arbeitsgruppe) nahm an einer Studienkonferenz über neue Felder des Patentwesens im Münchner Max-Planck-Institut teilnehmen.") (fr "Unsere %(ag:Arbeitsgruppe) nahm an einer Studienkonferenz über neue Felder des Patentwesens im Münchner Max-Planck-Institut teilnehmen."))))
(CAK '(1999 10 20) (ML Tie "EuroLinux delegation meets EU legislators" (de "Treffen mit EU-Gesetzgebern") (fr "Rencontre avec Législateurs UE") (it "Rencontro con gli legislatori UE")) 'phm 'euipCA (let ((EL "Eurolinux")) (ML EkC "A delegation of 8 representatives from the sponsors of EuroLinux and its member organisations met the patent legislators of the EU Commission's DG XV in Bruxelles to explain why current EU plans on software patentability are dangerous, and to propose a solution that could be acceptable to all parties." (de "Eine Delegation von 8 Vertretern der Mitglieder und Förderer von Eurolinux besuchte am 15. Oktober die Patentgesetzgeber der EU-Kommission / Generaldirektorat 15 (DG XV) in Brüssel, um ihnen zu erklären, warum die EU-Pläne bezüglich Patentierbarkeit von Computerprogrammen für die Softwarehersteller gefährlich sind, und um eine Lösung vorzuschlagen, die allen Seiten entgegenkommt.") (fr "Eine Delegation von 8 Vertretern der Mitglieder und Förderer von Eurolinux besuchte am 15. Oktober die Patentgesetzgeber der EU-Kommission / Generaldirektorat 15 (DG XV) in Brüssel, um ihnen zu erklären, warum die EU-Pläne bezüglich Patentierbarkeit von Computerprogrammen für die Softwarehersteller gefährlich sind, und um eine Lösung vorzuschlagen, die allen Seiten entgegenkommt.") (it "Una delegazione de 8 representanti delle organisazione membri e sponsori di EuroLinux ha visitato gli legislatori UE / DG XV in Bruxelles per loro spiegare per che gli piani de l' UE sono dangerosi por gli programmatori software, e per proponere una soluzione accetabile per tutti fianchi."))) (filters ((fs ahst 'swpatsarji) (fg ahst 'swpatgirzu) (lt ahst 'nopatltr)) (let ((JPS (tpe "J.P. Smets" (ahs 'aful)))) (ML Dwr "The delegation was headed by %{JPS}.  Travel expenses were paid by the %(fs:sponsors) of the %(fg:FFII software patent working group), most of whom also sent representatives with %(lt:letters from their CEOs), in order to express their deep concern about the expected results of the EU plans." (de "Die Delegation wurde von %{JPS} geleitet.  Die Fahrtkosten wurden von den %(fs:Sponsoren) der %(fg:Softwarepatent-Arbeitsgruppe des FFII) getragen, von denen die meisten auch Vertreter mit %(lt:Briefen ihrer Vorstände) schickten und so eindringlich ihre Sorge über die Folgen der EU-Pläne zum Ausdruck brachten.") (fr "Die Delegation wurde von %{JPS} geleitet.  Die Fahrtkosten wurden von den %(fs:Sponsoren) der %(fg:Softwarepatent-Arbeitsgruppe des FFII) getragen, von denen die meisten auch Vertreter mit %(lt:Briefen ihrer Vorstände) schickten und so eindringlich ihre Sorge über die Folgen der EU-Pläne zum Ausdruck brachten.") (it "La delegazione era guidata de %{JPS}.  Gli soldi di viaggio erano pagati per gli %(fs:sponsori) del %(fg:gruppo di lavoro FFII), e la pluparto di loro veneva con representanti e portava %(lt:lettere de gli loro patroni), chi s'esprimavano molto inchietati de gli piani de la commissione UE.")))))
(C8Q '(1999 8 26) (ML Ftn "FFII antwortet dem Patentreferenten des Bundesjustizministeriums" "FFII Response to BMJ" "Reponse FFII au BMJ") 'phm nil (ML Mct "Mitte des Monats erreichte uns ein Brief aus dem Bundesjustizministerium, der unsere Bedenken gegenüber den EU-Plänen zu zerstreuen sucht und uns in Aussicht stellt, als eine interessierte Partei in die kommenden EU-Konsultationen mit einbezogen zu werden.  Wir haben nach vereinsinterner Diskussion geantwortet.  In unserer Antwort legen wir dar, warum wir die Pläne der EU für gefährlich halten, stellen einige Fragen und bieten weiterführende Informationen an."))
(C7T '(1999 7 29) (ML Lnr "Linux-Verband kritisiert ZVEI-Brief" "Linux-Verband response to ZVEI" "Dialogue entre Linux-Verband et ZVEI") 'phm 'zvei-live-gnn
 (filters ((zd ahs 'zvei-propat-zdnet)) (let ((LV (ahs 'linux-verband)) (ZV (ahs 'zvei (tpe "ZVEI" (ML Zdn "Zentralverband der Elektronik-Industrie" "German Association of Electronic Industry"))))) (ML Dxs "Der %{LV} kritisiert die %(zd:Forderung) des %{ZV} nach Abschaffung des Patentierungsverbotes für Software." "%{LV} criticizes the %{ZV}'s %(zd:demand) for software patentability."))))
(C7S '(1999 7 28) (colons "ZVEI" (ML Zrn "Software muss patentierbar sein" "Software must be patentable" "Logiciels doivent être brevetables")) 'phm 'zvei-propat-gnn (ML Dfe "Die %(q:einschlägigen Patentierungsverbote für Computerprogramme) müssen abgeschafft und ein %(q:umfassender Patentschutz für Softwareerfindungen) ermöglicht werden, fordert der Zentralverband Elektrotechnik- und Elektronikindustrie (ZVEI) e.V.  Spätere FFII-Recherchen ergaben, dass es sich hierbei um eine Erklärung des Chefs der Siemens-Patentabteilung Arno Körber handelte, die vom ZVEI unbesehen übernommen wurden." "German Electronics Industry Association demands software patents.  This press statement was written by a well-known patent lobbyist from Siemens."))
(C7O '(1999 7 24) (ML SaW "Statistik zeigt: Softwarepatente werden ohne seriöse Prüfung gewährt" "Statistics show: software patents granted without serious examination" "Statistique montrent: brevets logiciels accordés sans examination sérieuse") 'phm 'aharC7O  (ML EvW "Eine %(ah:statistische Studie der amerikanischen Swpat-Praxis) zeigt: das US-Patentamt wird 1998-99 insgesamt 40000 Softwarepatente, also 10 mal so viel wie im Vergleichszeitraum 1993-94, gewähren und dabei 50 Millionen USD verdienen.  Derweil stehen dem US-Patentamt nicht wesentlich mehr Prüfer als damals zur Verfügung und die Prüfungsverfahren sind weitgehend Makulatur.  Zur Neuheitsprüfung werden praktisch nur Patentschriften herangezogen." "New statistics from Greg Aharonian show that software patents are issued without serious examination." (fr "statistische Studie der amerikanischen Swpat-Praxis")))
))

(let ((print-abstract nil) (no-sectmenu t))
(mlhtdoc 'swnpikta
 (l (ML tit "Europäische Softwarepatente: %(q:Trivialer als in den USA)" (en "European Software Patents: %(q:More Trivial than in the US)"))
    (mlval* 'tit)
    (ML Iep "Informatiker veröffentlichen %(q:Europäische Softwarepatent-Horrorgallerie)" (en "Programmers publish %(q:European Software Patent Horror Gallery)"))
    (ML PsW "Europäische Patentamtschefs wollen nächste Woche 30000 Tretminen aktivieren" (en "European patent officials want to activate 30000 mines next week")) ) 
 nil
 nil
 (center (tok for-immediate-release) )    
 (sepcc " - " (commas (ml "Munich" (de "München")))
 (filters ((ep ahs 'epo)) (ML Drh "Das %(ep:Europäische Patentamt) (EPA) hat in den letzten Jahren entgegen dem Buchstaben und Geist der geltenden Gesetze 30000 Patente auf Programmieraufgaben, Geschäftsideen und organisatiorische Verfahren erteilt.  Würde auch die nationale Rechtsprechung konsequent dem Willen des EPA folgen, so wäre es demnach heute in Europa nicht mehr erlaubt, medizinische Diagnosen automatisiert durchzuführen.   Ebenso betroffen wären zahlreiche recht alltägliche Verfahren wie die Abhaltung von Prüfungen in Schulen, die Anbahnung von Geschäften an der Börse, die Erzeugung von Einkaufszetteln aus Kochrezepten, die dynamische Festlegung von Verkaufspreisen, das Sprachenlernen durch Vergleichen der eigenen Aussprache mit der eines Lehrers.  All diese Verfahren verletzen europäische Patente, sofern man sie auf naheliegende Weise rechnergestützt organisiert.  Nicht minder gefährlich ist aus der Sicht von Fachleuten die Blockierung von Netzwerk-Standards wie MIME und CGI, die Zupflasterung der Betriebssystem-Ebene mit Tausenden von Patenten auf hardwarenahe Rechenaufgaben wie z.B. die Differenzierung zwischen benutzten und unbenutzten Speicherblöcken." (en "The %(ep:European Patent Office) (EPO) has in recent years granted 30000 patents on programming problems, business ideas and organisational procedures.  If national courts consistently followed the will of the EPO, it would no longer be legal to conduct automated medical diagnoses in Europe.  The same applies to numerous mundane activities such as conducting of examinations in schools, bringing traders together at the stock exchange, generating purchasing lists from cooking recipes, setting prices dynamically, learning languages by comparing one's pronunciation with that of a teacher.  All these activities infringe on European patents, as soon as they are implemented through software.  Other EPO patents encumber network standards such as MIME and CGI and squatter the operating system level by occupying thousands of basic methods of memory arithmetics, making programming in these fields a hazardous endeavour."))) )
 (let ((FFII (tpe (ahs 'ffii) "FFII"))) (filters ((pk ahs 'swpepue2B) (sp ahs 'swpatpikta)) (ML Dnu "Der %{FFII} hat eine %(sp:Datenbank) veröffentlicht, die einen Überblick über die umstrittenen europäischen Patente ermöglicht.  Dazu werden einige eindrucksvolle Beispielpatente, Statistiken und Studien präsentiert." (en "The %{FFII} has published a %(sp:database) of software patents granted by the EPO, together with some impressive examples, statistics and articles."))))
 (lin (ML DAa "Der Patentdatenreferent des FFII, Arnim Rupp, empfiehlt allen Diskutanten einen Blick in die Datenbank:" (en "The FFII's patent data specialist, Arnim Rupp, recommends that anybody discussing about software patents should first take a look at that database: "))
   (blockquote (ML Wed "Wer in den Patentschriften des EPA schmökert, stellt schnell fest, dass es hier weder um Software noch um innovative Programmierlösungen geht. Hier werden einfach systematisch ganze Problemfelder in Besitz genommen.  Diese lächerlich trivialen und gruselig breiten EPA-Patente sind aber vor nationalen Gerichten bisher nicht unbedingt einklagbar.  Die amerikanischen Großkonzerne, denen die gesetzeswidrig erteilten EPA-Patente zum Großteil gehören, warten noch auf eine Änderung des Europäischen Patentübereinkommens.  Wenn die Diplomatische Konferenz nächste Woche in München das falsche Signal setzt, wird Deutschland hoffentlich die Ankündigung des BMJ wahr machen und aus dem EPÜ austreten.  Der Ernst der Lage rechtfertigt dies.  Es geht jetzt nämlich nicht mehr um die Pflege dieses oder jenes Organisationsrahmens für das europäische Patentwesen sondern um die Entschärfung von 30000 Tretminen und die Wiederherstellung elementarer Rechtssicherheit für Europas IT-Unternehmer und Bürger." (en "By browsing through the EPO's patents you will quickly find out that this has nothing to do with protecting software, let alone protecting innovative solutions.  What this is really about is occupying complete problems.  Fortunately for us, these hilariously trivial and gruesomely broad EPO patent claims are so far not necessarily enforcable before European courts.  The American mega corporations, to whom most of these illegally granted patents belong, are still waiting for a change in the European Patent Convention.  If the Diplomatic Conference sets the wrong signal in Munich next week, Germany will hopefully abide by the words of the Ministery of Justice and refuse to ratify the new European Patent Convention.  The situation is serious enough to justify this.  The European patent system will work one way or another.  The issue at stake now is how to keep 30000 mines from detonating and how to give back basic legal security to European IT enterprises and citiziens.")) ) )
 (lin (ML Ded "Für Daniel Rödding, Geschäftsführer eines Softwareunternehmens in Paderborn, ist die Lage sehr ernst:" (en "For Daniel Rödding, CEO of a software enterprise in Paderborn, the situation is very serious:")) (blockquote
  (ML Dha "Ein Blick in die Patentdatenbank des FFII zeigt anschaulich, was Softwarepatente in Europa heute für den Großteil der EDV-Unternehmen bedeuten.  Auf einem solchen Minenfeld haben kleine Softwareunternehmen kaum noch Chancen.  Für mein Unternehmen habe ich die Konsequenzen bereits gezogen: Ich werde voraussichtlich ab Mitte nächsten Jahres weite Teile der Software-Entwicklung in einem Land durchführen, in welchem es kein so hoch entwickeltes Patentrecht gibt und wo eine Änderung der Rechtslage in den nächsten Jahren auch nicht zu erwarten ist. Im Bereich bestimmter Technologien wird mir die Entwicklung von Software in Deutschland einfach zu riskant, das läßt sich mit Blick auf rechtliche Risiken in der Zukunft aus der Kleinunternehmerperspektive nicht mehr verantworten." (en "By browsing the FFII's patent data base you can quickly grasp what software patents mean for most European IT companies today.  On such a minefield small software companies hardly have any chance anymore.  For my company I have already drawn the consequences:  Starting from mid of next year we will conduct large parts of our software development in a country which does not yet have such a highly developped patent law system and in which a change of the legal situation cannot be expected for the near future.  In certain fields the development of software is becoming too dangerous in Germany.  Given the longterm legal risks, continuing with this activity in Germany would be irresponsible from a small entrepreneur's point of view.")) ) )
 (filters ((ep ahs 'eulux-petition)) (ML BWt "Bisher haben sich bereits 200 Softwarefirmen und 55000 Unterzeichner der %(ep:Eurolinux-Petition für ein softwarepatentfreies Europa) in ähnlichem Sinne geäußert." (en "So far already 200 software companies and 55000 signataries of the %(ep:Eurolinux Petition) have expressed themselves in a similar way.")))
 (ML Aio "Auf der %(q:Diplomatischen Konferenz) werden derweil Patentreferenten von 20 europäischen Staaten über einen vom EPA-Präsidenten Dr. Ingo Kober entworfenen %(q:Basisvorschlag zur Revsion des Europäischen Patentübereinkommens) verhandeln.  Darin schlägt das EPA u.a. vor, eine universelle Patentierbarkeit festzuschreiben (Art 52) und dem EPA-Vorstand Gesetzgebungsvollmachten einzuräumen (Art 33).  Die Tagesordnung wurde vom EPA so festgelegt, dass nur eine 2/3-Mehrheit der nationalen Patentdelegationen sich noch in Einzelfragen durchsetzen kann.  Andernfalls wird der Wille des EPA innerhalb von wenigen Monaten in allen europäischen Staaten rechtsgültig, sofern deren Parlamente nicht beschließen, aus dem Europäischen Patentübereinkommen (EPÜ) auszusteigen." (en "Meanwhile at the %(q:Diplomatic Conference) patent representatives of 20 European countries will be negotiating about a %(q:Base Proposal for the Revision of the European Patent Convention) drafted by EPO president Dr. Ingo Kober.  Therein the EPO proposes among others to stipulate universal patentability (Art 52) and to confer special legislative rights on the administrative council of the EPO (Art 33).  The rules or procedure have been determined by the EPO in such a way that national patent delegations can overrule individual items only by a 2/3 majority.  Otherwise the will of the EPO will become legally binding in all European countries whose parliaments do not opt out of the European Patent Convention (EPC)."))
 (filters ((gp ah "http://www.gnu.org/") (lp ahs 'lpf-patents)) (ML Dan "Der FFII wird gemeinsam mit fördererden Softwareunternehmern und Systemarchitekten wie Richard Stallman am Dienstag den 21. November um 11-12:30 Uhr neben dem Europäischen Patentamt im Forum der Technik, Raum Helios, seine %(q:Europäische Softwarepatente-Horrorgallerie) präsentieren und Journalistenfragen beantworten.  Richard Stallman hat in den USA als Gründer der %(lp:League for Programming Freedom) und des %(gp:GNU-Projekts) Jahre lang überzeugend dargelegt, warum das Patentsystem den Fortschritt im Softwarebereich nur behindert." (en "The FFII will together with sponsoring software companies and system architects like Richard Stallman present its %(q:European Software Patent Horror Gallery) on November 21 11-12:30 near the EPO in Forum der Technik, Helios conference room, and respond to questions from journalists.  Richard Stallman has founded %(gp:GNU project) and the %(lp:League for Programming Freedom), which has been arguing the case against software patentability in the USA since 1990.")))

(sects
(ref (tok euluxref-tit)
(eulux-references
 (l 'swpatpikta (ML EeW "Europäische Softwarepatente: Datenbank und Beispielsammlung" (en "European Software Patents: Database and Examples")))
 (l (lang-href 'eulux-petition) (ML EWp "Eurolinux-Petition für eine softwarepatentfreies Europa" (en "Eurolinux Petition for a software patent Free Europe")))
 (l (subdirs (relurldir 'swpatprina) "patpruef.pdf") (colons (ML DKe "Dr. Swen Kiesewetter-Köbinger") (sepcc " -- " (ML nWt "Über die Patentprüfung von Programmen für Datenverarbeitungsanlagen") (ML PaK "Probleme und Ungereimtheiten der Softwarepatentierung aus der Sicht eines Prüfers am Deutschen Patent- und Markenamt"))))
 (l (href 'trilateral-repo242) (ML Vnm "Vergleichsbericht über die Prüfung von Softwarepatenten am Europäischen, Amerikanischen und Japanischen Patentamt" (en "comparative report about the examination practise for software patents at the US, European and Japanese patent offices ")))
 (l (href 'spiegel-dgmelin0010) (ML Lxg4 "Bundesjustizministerium fordert Moratorium in Sachen Swpat und droht mit EPÜ-Austritt" (en "German Ministery of Justice demands that the computer program exception not be removed at the coming conference and threatens to opt out of the EPC otherwise")))
 (l (absurldir 'swpat) (ML Sns "Schutz der Informatischen Innovation vor dem Missbrauch des Patentwesens" (en "Protecting Informational Innovation against the Abuse of the Patent System")))
 (l "http://www.save-our-software.de" (ML Lii "BILD dir deine Meinung über Softwarepatente -- Vereinfachende aber wahre Einführung in die Problematik" (en "A simplistic but true introduction to the problem (German only)")))
 (l "http://www.gnu.org" (ML Gko "GNU-Projekt" (en "GNU Project")))
 'lpf-patents
) )

(prini (sepcc " - " (ML nWW "Über den FFII" (en "About FFII") (fr "A propos de la FFII") (es "Acerca de la FFII")) (ahh "www.ffii.org")) 
 (filters ((fi ahs 'ffii) (sp ahs 'swpatgirzu) (sj ahs 'swpatsarji) (el ahs 'eurolinux))
 (let ((FFII (tpe (ahs 'ffii) "FFII e.V.")))
 (ML cas "Der %{FFII} ist ein gemeinnütziger Verein, der die Entwicklung offener Schnittstellen, quelloffener Programme und frei verfügbarer öffentlicher Informationen fördert und sich für ein kraftvolles Zusammenwirken freier und proprietärer Software zum Zwecke eines langfristigen Aufbaus informationeller Gemeingüter auf der Grundlage offener Standards, fairen Wettbewerbs und der Achtung legitimer Urheberrechte einsetzt.  Der FFII koordiniert eine %(sp:Arbeitsgruppe zum Schutz der digitalen Innovation vor Softwarepatenten), die %(sj:von erfolgreichen deutschen Softwarefirmen unterstützt) wird.  Der FFII ist Gründungsmitglied der %(el:EuroLinux-Allianz für eine Freie Informationelle Infrastruktur)." "%(fi:FFII) is a non-profit association which promotes the development of open interfaces, open source software and freely available public information. FFII coordinates a %(sp:workgroup on software patents) which is %(sp:sponsored by successful german software publishers). FFII is member of the %(el:EuroLinux Alliance)." (es "La %(fi:FFII) es una asociación sin ánimo de lucro que promueve el desarrollo de interfaces abiertas, software \"open source\" e información pública libremente disponible. La FFII coordina un %(sp:grupo de trabajo sobre patentes de software) el cual está  %(sp:patrocinado por importantes editores alemanes de software). La FFII es miembro de la %(el:Alianza EuroLinux).") (fr "La %(fi:FFII) est une association à but non-lucratif qui fait la promotion du développement d'interfaces ouvertes, de logiciels open source et d'une information publique accessible au public. La FFII coordonne un %(sp:groupe de travail sur les brevets logiciels) qui est %(sp:soutenu par des éditeurs de logiciels allemands reconnus). La FFII est membre de l'%(el:Alliance EuroLinux).")))
) )

(press  (tok euluxpress-tit)
 (dl (l (ML EPs "email" (de "E-Post") (fr "courriel")) "info@ffii.org") (l (ML EPs2 "Tel") (spaced "Hartmut Pilch" "+49-89-18979927")))
)

(url (tok euluxurl-tit)
     (absurldir docid)) ) 
))

(let ((print-abstract nil) (no-sectmenu t)) 
(mlhtdoc 'swnpatg2C
 (l (ML SAn "%(q:Europäischer Patentkonzern) will Softwarepatente im Eilverfahren per EU-Richtlinie einführen" (en "%(q:European Patent Corporation) rushing to introduce software patents via EU directive"))
    (mlval* 'SAn)
    (ML OiA "IT-Fachleute wollen mit fünf Gesetzesinitiativen Patentinflation unter Kontrolle bringen" (en "IT professionals spell out five legislative initiatives to bring patent inflation under control") ) ) 
 nil
 nil
 (center (tok for-immediate-release))
 (sepcc " - " (commas (ml "Munich" (de "München")) "Berlin" "Frankfurt" "Ilmenau" "Magdeburg") (mlval* 'DWE))
 (lset ((DG15 (ahs 'eu-dg15 (tpe (ML Gri "Generaldirektion Binnenmarkt" "Directorate General for the Internal Markt") (ML GDB "GDBM" "DGIM")))) (FB "Frits Bolkenstein")) (ML Din "Die Patentreferenten der Europäischen Kommission haben für Donnerstag, den 21. Dezember 2000, einen Kreis von Patentjuristen und Patentreferenten der europäischen Regierungen zu einem Sondierungsgespräch über die geplante EU-Richtlinie zur Patentierbarkeit von %(q:computer-implementierten Erfindungen) eingeladen.  Innerhalb der Europäischen Kommission ist die %{GDBM} unter Kommissar Frits Bolkestein für das Thema zuständig.  Bisherige Verlautbarungen deuten darauf hin, dass die Generaldirektion bis spätestens Anfang Februar die umstrittene Softwarepatentierungspraxis des Europäischen Patentamtes (EPA) legalisieren und für alle eurpäischen Gerichte als verbindlich vorschreiben will." "The patent law experts of the European Commission have invited a group of patent experts from the national governments for a conference about the planned EC directive on the patentability of %(q:computer-implemented inventions), i.e. software.  Within the European Commission, the %{DGIM} under commissioner %{FB} is in charge of the matter.  According to public announcements made by DGIM so far, DGIM wants to explicitely make the current practise of the European Patent Office (EPO) binding for all European courts by beginning of February at the latest."))
 (filters ((lb ahs 'lutterbeck-studie)) (ML WdW "Wer zu der Sitzung in Brüssel eingeladen wurde, ist nicht öffentlich bekannt.  Es ist jedoch davon auszugehen, dass im wesentlichen die Generaldirektion Binnenmarkt (GDBM) ihre Pläne präsentieren und ausgewählten Zuhörern das Rederecht einräumen wird, um einen Konsens für ihre Position zu erzielen.  Von Seiten der Bundesregierung werden Patentreferenten des Bundesministeriums der Justiz (BMJ) erwartet.  Derweil hat das Bundesministerium für Wirtschaft (BMWi) mehrere Studien über die wirtschaftlichen Auswirkungen von Softwarepatenten ausgeschrieben.   Eine %(lb:gerade veröffentlichte Studie) schildert die von Softwarepatenten drohenden Gefahren in schrille Farben.  Bei einer zweiten Studie ist erst Mitte 2001 mit Ergebnissen zu rechnen.  Dies deutet darauf hin, dass man auf Seiten der Bundesregierung an einer allzu schnellen Brüsseler Beschlussfassung nicht interessiert sein kann.  Demgegenüber drängt die GDBM auf schnelle %(q:Harmonisierung der Rechtslage)."  "It is not known publicly who has been invited to attend the session in Brussels.  However it is to be expected that the %{GDBM} will present its plans and allow selected participants to speak, so as to reach a consensus for its position.  From the side of the German government, the patent experts of the Federal Ministery of Justice (BMJ) are expected to attend.  An %(lb:study) conducted under the auspices of this ministery warns about devastating effects of software patents.  A second study is scheduled to deliver results by mid of 2001.  This indicates that the Federal Government can hardly be interested in very rapid decisions on the part of DGIM.  Yet DGIM stresses the urgence of %(q:harmonising the legal situation)."))
 (let ((IPI (tpe "Intellectual Property Institute" "IPI"))) (filters ((ip ahs 'indprop) (gp ahs 'eu-dg15-pat-gp) (ir ahs 'eu-dg15-ipi-rept)) (ML Aef "Auf den %(ip:einschlägigen Webseiten) der %{GDBM} steht gleich eingangs zu lesen, die Wichtigkeit des Patentwesens als Motor der Innovation könne %(q:gar nicht überschätzt werden).  In diesem Geiste sind alle Dokumente der GDBM geschrieben, die sich mit der Patentierbarkeit von Software befassen, angefangen vom %(gp:Grünbuch von 1997).  Ebenso wie das Europäische Patentamt (EPA), das BMJ-Patentreferat und andere Mitglieder des %(q:Europäischen Patentkonzerns) glaubt die GDBM an die segensreiche Wirkung aller Formen des %(q:Eigentums).  Auf diesem einfachen Glauben beruht ihre Arbeitsweise.  Doch in den letzten Jahren haben zunehmend leidvolle Erfahrungen und wissenschaftliche Studien gezeigt, dass dieser Glaube falsch ist.  Wachsender Kritik begegnete die GDBM 1999 zunächst, indem sie eine %(q:unabhängige Studie) vom Londoner %{IPI} anfertigen ließ."
 "On the %(ip:relevant webpages) of the %{GDBM} it is written right at the top that the importance of the patent system as a motor of innovation %(q:cannot be underestimated).  The documents written by DGIM about the question of software patentability, starting from the %(gp:Greenpaper of 1997), are written in this spirit.  Just like the European Patent Office, the BMJ patent division and other members of the of the %(q:European Patent Corporation), the DGIM believes in the beneficial effect of all forms of %(q:property).  Its whole work is based on this simple belief.  But during the last few years increasingly painful experiences and scientific study reports have shown that this belief is wrong.  In 1999 the DGIM showed first reactions to growing criticism.  It ordered an %(ir:independent report) from the London-based %{IPI}.")))
 (filters ((rh ahs 'clsr-rhart97)) (ML Dfe "Das IPI versteht sich als Sprachrohr des Patentwesens.  Sein Vorstand besteht vor allem aus Patentanwälten, Patentrichtern und Großkonzernen mit großem Patentportfolio.  Laut Satzung hat es sich zur Aufgabe gesetzt, %(q:Forschung zu fördern, deren Ergebnisse den Anteilseignern des Institutes zugute kommen werden).  Einer der Autoren des IPI-Berichts, Robert Hart, ist überdies ein bekannter Verfechter der Softwarepatentierung, der schon 1997 durch %(rh:besonders unwissenschaftliche Methoden) den Boden für das Grünbuch bereitete." "The IPI is the patent establishment's mouthpiece, its board and membership being primarily composed of patent lawyers, judges, patent agents and large international firms with significant patent holdings.  Even worse - IPI admits that its mission is to %(q:fund research, the fruits of which will provide greatest benefit to the Institute's stakeholders).  Also, Robert Hart, one of the authors of the IPI report, is a famous advocate of software patents who in 1997 prepared the grounds for the Greenbook with %(rh:particularly unscientific methods)."))
 (ML Str "So verwundert es nicht, dass das IPI seiner Studie nur die Praxis des EPA zugrunde legte und keine Handlungsoption in Betracht zog, die zu weniger Patenten führen könnte.  Dennoch wiesen die Autoren darauf hin, dass es keine fundierten wirtschaftspolitischen Argumente für eine Ausweitung des Patentwesens gebe.  Die GDBM hielt diese Studie von März bis Oktober 2000 unter Verschluss." "No wonder then that the IPI report fails to take into consideration any scenario which would lead to less patents...  Yet it does conclude that any argument for the extension of the patent system can not claim to rest on solid economic reasoning.  The DGIM kept this study secret from March until October 2000.")
 (filters ((ek ahs 'eulux-consultation) (el ah (subdirs (href 'eulux-consultation) "ec-consult.pdf"))) (ML AuK "Angesichts wachsender Kritik von Kollegen innerhalb der Europäischen Kommission veröffentlichte die GDBM schließlich am 19. Oktober die IPI-Studie zusammen mit einem eigenen %(q:Konsultationspapier), welches bis ins Detail den Standpunkt des Europäischen Patentamtes vertritt.  Die Öffentlichkeit wurde aufgefordert, zu diesen beiden Papieren Stellung zu nehmen.  Jeder Hinweis auf kritische Studien unterblieb.  Daraufhin gingen viele Schreiben von Patentjuristen diverser Gewerbeverbände in Brüssel ein.  Der %(q:Europäische Patentkonzern) wurde mobilisiert.  Aber auch die Kritiker %(ek:veröffentlichten) hunderte von Eingaben.  Eine %(el:offizielle Stellungnahme der Eurolinux-Allianz) wurde mit über 1000 Seiten Anhang nach Brüssel übersandt." "In view of growing criticism from colleagues within the European Commission, the DGIM finaly published the IPI study together with a %(q:consultation paper) of its own, which restates the position of the European Patent Office.  No reference to any critical studies was given, not even those made by some European governmental organisations.  Thus the patent law experts of various industry associations submitted statements, and the whole %(q:European Patent Corporation) was mobilised.  But also the critics %(ek:published) hundereds of submissions.  An %(el:official statement of the Eurolinux Alliance) with 1000 pages of appendix was sent to Brussels."))
 (ML AWi "Ein großes interdisziplinäres Forscherteam bräuchte einige Wochen, um diese Unterlagen auszuwerten.  Bei der GDBM geht es offenbar schneller.  Denn dort arbeiten wenige Patentjuristen, die sich ganz auf das konzentrieren, was sie gelernt haben: die Rechtsmeinung des Europäischen Patentamtes wortgetreu wiedergeben und in Gesetzesregeln umsetzen.  Hierfür brauchen sie  noch nicht einmal die Zustimmung des Europäischen Parlaments.  Es genügt, sich mit den Patentreferenten der nationalen Regierungen kurz zu schließen und konzernsintern einen einstimmigen Beschluss zu fassen." "An interdisciplinary team of researchers would take many weeks to evaluate these submissions.  The DGIM may be able to do the job much faster.  Their patent experts can focus on what they have learnt: rendering the opinion of the European Patent Office word by word and translating it into European laws.  To do that, they needn't even ask for the approval of the European parliament.  They just have to convoke the patent law experts from the national governments and reach a consensus among colleagues within the %(q:European Patent Corporation).")
 (filters ((ob ahs 'swxpatg2C)) (ML AxW "Die einzige demokratische Kontrollfunktion üben nach wie vor die nationalen Parlamente aus.  Daher haben namhafte IT-Firmen und Verbände in einem %(ob:Offenen Brief) an zahlreiche Politiker im deutschsprachigen Raum fünf Gesetzesinitiativen formuliert:" "The only democratic control function is residing in the national parliaments.  Therefore, several well known IT companies and associations have published an %(ob:open letter) to numerous politicians, in which they propose five law initiatives:"))
 (dl
 (l (ML Fle "Freiheit der Veröffentlichung selbstgeschaffener Informationswerke" "Freedom to Publish original Information Works")
    (ML Rmg "Rechte aus Patenten können gegen die industrielle Anwendung von Computerprogrammen, nicht aber gegen ihre Veröffentlichung oder In-Verkehr-Bringung geltend gemacht werden.  Auf nationaler Ebene durch Änderung von §11 PatG zu erreichen." "Rights derived from patents may be used against the industrial application of computer programs but not atainst their publication or distribution.  This can be achieved by a simple modification of national law.") )
 (l (ML Fgi "Freiheit des Zugangs zu Kommunikationsstandards" "Freedom of Access to Communication Standards")
    (ML WWk "Wenn jemand mit Software-Marktmacht %(q:Standards setzt), müssen diese Standards von privaten Ansprüchen frei sein.  Träger öffentlicher Funktionen dürfen nur über offene Standards mit dem Bürger kommunizieren." "When someone %(q:sets standards) using software market power, these standards must be free of private claims.  Representants of publis functions have to base their communication with citizens on open standards.") )
 (l (ML Pdb "Präzisierung der Patentierbarkeitsgrenzen" "Precisation of Patentability Criteria")
    (filters ((er ahs 'swpateurili)) (ML Diu "Der Gesetzgeber gibt durch einen Resolution eine %(er:prägnante und widerspruchsfreie Auslegung von §1 PatG und Art. 52 EPÜ) vor.  Je nach Bedarf wird die Rechtsprechung nach und nach durch Präzisierung einschlägiger Gesetzestexte darauf verpflichtet." "The Lawmaker passes a resolution stating a %(er:consistent and concise interpretation of Art 52 EPC and corresponding articles of national law).  As far as necessary, lawcourts are brought back on the path of this interpretation, which represents the original spirit of the law, by explanatory amendments to patent laws.")) )
 (l (ML mem "Sachgerechte Systeme zur Förderung informationeller Innovationen" "Adequate systems for promotion of information innovation")
    (ML Eie "Es werden angemessene Systeme zur Belohnung informatischer Innovationen und sonstiger geistiger Leistungen entworfen.  In Frage kommen u.a. sanfte Ausschlussrechte, abstimmungsbasierte Prämiensysteme und die Förderung der Bildung und Forschung." "Adequate systems for stimulating and rewarding information innovations and other mental labor are to be devised.  This may mean a combination of soft exclusion rights, voting-based rewarding systems and a stronger public commitment to the funding of research and education.") )
 (l (ML Eue "Entbürokratisierung und Internationalisierung des Patentwesens" "Debureaucratisation and Internationalisation of the Patent System") 
    (ML Eef "Es wird möglich, Patente mit sofortiger Wirkung und kostenlos durch formgerechte Netzveröffentlichung anzumelden.  Im Gegenzug wird ein Markt für Patentinvalidierer geschaffen.  Die Kosten des Systems werden von den Anmeldern unberechtigter Patente getragen." "It is made possible to register patents instantly and free of charge by publishing them according to standardised requirements on the Net.  On the other hand, a market for patentbusters is created.  The polluter-pays-principle is established:  the costs of the patent system are born by the owners of unjustified patents.") ) )
(ML DcW "Die Bundesregierung soll mit parlamentarischen Anträgen aufgefordert werden, auf die Verwirklichung dieser Ziele hinzuarbeiten." "The Federal Government is to be requested by parliamentary motions to work for the realisation of these goals.") 
(filters ((ab ahs 'swxepue28)) (ML Dfl "Der Offene Brief stützt sich auf zahlreiche kompakte Anhänge, durch die interessierte Leser schnell zu weiteren fundierten Informationsquellen geführt werden.  Ein %(ab:Vorgänger dieses Briefes) vom August 2000 hatte mit dazu beigetragen, einen Konsens aller Bundestagsfraktionen gegen Softwarepatente herzustellen.  Auch diesmal rechnen die Initiatoren mit breiter Unterstützung." "The Open Letter is founded on numerous compact appendices, through which the interested reader is led to further well founded sources of information.  A %(ab:predecessor of this letter) from August 2000 had contributed to establishing an anti software patent consensus of all parties represented in the German Federal Parliament.  This time again the initiators are expecting broad support."))

(sects
(ref (tok euluxref-tit)
(eulux-references
 'eulux-petition
 'swpat
 'clsr-rhart97
 'swxpatg2C
 'swpateurili
))

(prini (sepcc " - " (ML nWW "Über den FFII" (en "About FFII") (fr "A propos de la FFII") (es "Acerca de la FFII")) (ahh "www.ffii.org")) 
 (filters ((fi ahs 'ffii) (sp ahs 'swpatgirzu) (sj ahs 'swpatsarji) (el ahs 'eurolinux))
 (let ((FFII (tpe (ahs 'ffii) "FFII e.V.")))
 (ML cas "Der %{FFII} ist ein gemeinnütziger Verein, der die Entwicklung offener Schnittstellen, quelloffener Programme und frei verfügbarer öffentlicher Informationen fördert und sich für ein kraftvolles Zusammenwirken freier und proprietärer Software zum Zwecke eines langfristigen Aufbaus informationeller Gemeingüter auf der Grundlage offener Standards, fairen Wettbewerbs und der Achtung legitimer Urheberrechte einsetzt.  Der FFII koordiniert eine %(sp:Arbeitsgruppe zum Schutz der digitalen Innovation vor Softwarepatenten), die %(sj:von erfolgreichen deutschen Softwarefirmen unterstützt) wird.  Der FFII ist Gründungsmitglied der %(el:EuroLinux-Allianz für eine Freie Informationelle Infrastruktur)." "%(fi:FFII) is a non-profit association which promotes the development of open interfaces, open source software and freely available public information. FFII coordinates a %(sp:workgroup on software patents) which is %(sp:sponsored by successful german software publishers). FFII is member of the %(el:EuroLinux Alliance)." (es "La %(fi:FFII) es una asociación sin ánimo de lucro que promueve el desarrollo de interfaces abiertas, software \"open source\" e información pública libremente disponible. La FFII coordina un %(sp:grupo de trabajo sobre patentes de software) el cual está  %(sp:patrocinado por importantes editores alemanes de software). La FFII es miembro de la %(el:Alianza EuroLinux).") (fr "La %(fi:FFII) est une association à but non-lucratif qui fait la promotion du développement d'interfaces ouvertes, de logiciels open source et d'une information publique accessible au public. La FFII coordonne un %(sp:groupe de travail sur les brevets logiciels) qui est %(sp:soutenu par des éditeurs de logiciels allemands reconnus). La FFII est membre de l'%(el:Alliance EuroLinux).")))
) )

(press  (tok euluxpress-tit)
 (dl (l (ML EPs "email" (de "E-Post") (fr "courriel")) "info@ffii.org") (l (ML EPs2 "Tel") (spaced "Hartmut Pilch" "+49-89-18979927")))
)

(url (tok euluxurl-tit)
     (absurldir docid)) ) 
))

(let ((print-abstract nil) (no-sectmenu t)) 
(mlhtdoc 'swnepue28
 (l (ML SAn "Eur. Patentamt begehrt unbegrenzte Patentierbarkeit" (en "Eur. Patent Office proposes unlimited patentability") (es "La Oficina Europea de Patentes propone la patentabilidad ilimitada") (fr "L'Office Européen des Brevets propose une brevetabilité illimitée"))
    (mlval* 'SAn)
    (ML OiA "IT-Fachleute fordern wirksame Kontrolle des EPA" (en "IT professionals demand effective control of the EPO") (es "Los profesionales de las Tecnologías de la Información exigen un control más efectivo de la Oficina Europea de Patentes (EPO)") (fr "Des professionnels de l'informatique demandent un contrôle effectif de l'OEB")) ) 
 (ML DWE "Das Europäische Patentamt (EPA) will die europäischen Regierungen dazu bewegen, im November 2000 sämtliche gesetzlichen Einschränkungen der Patentierbarkeit zu beseitigen.  Das Bundesministerium der Justiz (BMJ) gab einen entsprechenden auf den 27. Juni 2000 datierten %(q:Basisvorschlag für die Revision des Europäischen Patentübereinkommens) den %(q:am Patentwesen interessierten Kreisen) Anfang August bekannt." (en "The European Patent Office (EPO) wants to induce European governments to remove all legal restrictions on patentability in november 2000.  The German Federal Ministery of Justice (BMJ) distributed a %(q:Base Proposal for the Revision of the European Patent Convention) dated 2000-06-27 to the %(q:circles interested in the patent system) at the beginning of August.") (es "La Oficina Europea de Patentes (EPO) pretende inducir a los gobiernos europeos para que retiren todas las restricciones legales a la patentabilidad en noviembre del 2000. El Ministerio Federal de Justicia alemán (BMJ) distribuyó una %(q:Propuesta Base para la Revisión de la Convención Europea de Patentes) fechada el 27-6-2000 a los %(q:círculos interesados en el sistema de patentes) a comienzos de agosto. ") (fr "L'Office Européen des Brevets (OEB) veut inciter les gouvernements Européens à supprimer toute restriction légale sur la brevetabilité en novembre 2000. Le Ministère Fédéral Allemand de la Justice (BMJ) a distribué une %(q:Proposition de Base pour la Révision de la Convention Européenne des Brevets) datée du 27-06-2000 aux %(q:cercles intéressés dans le système des brevets) au début du mois d'août."))
 nil
 (center (tok for-immediate-release))    
 (sepcc " - " (commas (ml "Munich" (de "München")) "Berlin" "Frankfurt" "Ilmenau" "Magdeburg") (mlval* 'DWE))
 (let ((FFII (tpe (ahs 'ffii) "FFII"))) (filters ((sx ahs 'swxepue28)) (ML Drh "Der %{FFII} zeigt zusammen mit einigen IT-Firmen in einem %(sx:offenen Brief an das Bundesjustizministerium) die Unzulänglichkeiten des EPA-Entwurfs auf und warnt vor verheerenden Auswirkungen der expansiven Patentpolitik des EPA auf Innovation, Wettbewerb, Wohlstand, Bildung und Bürgerrechte." (en "The %{FFII} sent an %(sx:open letter) to the BMJ, in which it pointed out some inconsistencies in the EPO's proposal and warned about devastative effects of the EPO's expansive patent policy on innovation, competition, prosperity, education and civil rights.") (es "La %{FFII} envió una %(sx:carta abierta) al BMJ, en la que se señalaban las incosistencias de la propuesta de la EPO y se advertía acerca de los efectos devastadores de la política expansiva de la EPO sobre la innovación, la competencia, la prosperidad, la educación y los derechos civiles. ") (fr "La %{FFII} a envoyé une %(sx:lettre ouverte) au BMJ, dans laquelle elle a mis en évidence des incohérences dans la proposition de l'OEB et a averti contre les effets dévastateurs de la politique expansive de l'OEB sur l'innovation, la compétition, la prospérité, l'éducation et les libertés publiques.")) ) )  
 (homvort
  (let ((RS "Ralf Schwöbel") (IAG (ah "http://www.intradat.de" "Intradat AG") )) (ML Ree "%{RS}, Vorstand der Frankfurter %{IAG}, eines Marktführers in Internet-Verkaufssystemen, erklärt seine Unterstützung für den offenen Brief:" (en "%{RS}, CEO of %{IAG}, a Frankfurt based market leader in e-shop systems, explains his support for the public letter:") (es "%{RS}, Director General de %{IAG}, una empresa de Frankfurt líder en sistemas de comercio electrónico, explica su apoyo a la carta abierta:") (fr "%{RS}, CEO de %{IAG}, un leader sur le marché des systèmes de boutiques en ligne basé à Francfort, explique son soutien à cette lettre ouverte:")))
  (ML MfW "Mit diesem Vorschlag schafft das EPA die rechtlichen Rahmenbedingungen, um ebenso viele Patente auf Geschäftsverfahren zu gewähren wie die USA.  Als Hersteller von E-Commerce-Systemlösungen halten wir das für schädlich, denn es wird viele kleine und mittlere Unternehmen davon abschrecken, ihre eigenen E-Commerce-Lösungen zu entwickeln." (en "With this proposal, the EPO creates a legal framework for patenting as many business methods as the US.  As a publisher of e-commerce solutions, we believe that this is harmful, since it will deter many small and medium size enterprises from developping their own e-commerce solutions.") (es "Con esta propuesta, la EPO crea una infraestructura legal para patentar tantos métodos comerciales como los EEUU.  Como proveedor de soluciones de comercio lectrónico, creemos que esto es dañino, pues impedirá a muchas empresas de pequeño y mediano tamaño el desarrollo de sus propias soluciones de comercio electrónico.") (fr "Avec cette proposition, l'OEB crée un cadre légal pour breveter autant de méthodes d'entreprises qu'aux USA. En tant qu'éditeur de solutions d'e-commerce, nous pensons que cela est néfaste, puisque cela dissuadera de nombreuses petites et moyennes entreprises de développer leurs propres solutions de e-commerce.")))
 (filters ((sk ah 'trilateral-repo242) (a6 ahs 'tws-epa-bizmeth)) (ML Drc "Neuere Studien nähren diese Befürchtungen.  Das Europäiche Patentamt geht bei der Erteilung von Patenten auf immaterielle Gegenstände (Software, Geschäftsmethoden) bereits heute schon weiter als die Kollegen in den USA und Japan.  Einem %(sk:japanischen Vergleichsbericht) zufolge legt das EPA bei der Bewertung der Erfindungshöhe immaterieller Verfahren noch niedrigere Maßstäbe an als die anderen beiden Patentämter.  Bei der Beurteilung der Technizität von Geschäftsmethoden %(a6: plant) das EPA überdies, seine bisherige Forderung nach einem über die normale Nutzung des Rechners hinausgehenden %(q:zusätzlichen technischen Effekt) fallen zu lassen, sobald die vom EPA vorgeschlagene Vertragsänderung in Kraft getreten ist." (en "Recent studies show, that the European Patent Office examiners tend to grant patents on immaterial objects (such as software and business methods) even more generously than the USPTO and JPO colleagues.  According to a %(sk:Japanese comparative study), the EPO applies even lower standards for assessing the inventivity of immaterial objects.  Moreover, for the purpose of examining the technicity of business methods, the EPO even %(a6:plans) to abandon its traditional request that these methods should have an %(q:additional technical effect) beyond the ordinary use of the computer, as soon as the limitations on patentability are removed by the legislator as proposed by the EPO.") (es "Estudios recientes muestran que los examinadores de la Oficina Europea de Patentes tienden a conceder patentes sobre objetos inmateriales (como software o métodos comerciales) aún más generosamente que sus colegas de la USPTO (EEUU) y la JPO (Japón).  Según un  %(sk:estudio comparativo japonés), la EPO aplica baremos aún menos exigentes para juzgar la inventividad de objetos inmateriales.  Además, para el objetivo de examinar la tecnicalidad de los métodos comerciales, la EPO %(a6:planea) incluso abandonar su tradicional petición de que estos métodos tengan un %(q:efecto técnico adicional) más allá del uso ordinario del ordenador, tan pronto como las limitaciones a la patentabilidad sean retiradas por los legisladores según la propuesta de la EPO.") (fr "De récentes études montrent que les examinateurs de l'Office Européen des Brevets tendent même à accorder des brevets sur des objets immatériels (tels que les logiciels et les méthodes d'entreprises) de façon encore plus généreuse que leurs collègues de l'USPTO et du JPO. D'après une %(sk:étude comparative Japonaise), l'OEB applique même des standards inférieurs pour établir l'inventivité des objets immatériels. De plus, pour dans la façon d'examiner la technicité des méthodes d'entreprises, l'OEB %(a6:projette) même d'abandonner sa requête traditionnelle que ces méthodes aient un %(q:effet technique supplémentaire) au delà de l'utilisation normale d'un ordinateur, dès que les limites sur la brevetabilité auront été supprimées par le législateur comme le propose l'OEB.")))
 (homvort
  (let ((MS "Matthias Schlegel") (PAG (ah "http://www.phaidros.com" "Phaidros AG"))) (ML Mnn "%{MS}, Vorstand der Ilmenauer %{PAG}, eines Pioniers in der Metamodellierung von Geschäftsprozessen, kommentiert:" (en "%{MS}, CEO of %{PAG}, an Ilmenau based pioneer in meta-modelling of commercial processes, comments:") (es "%{MS}, Director General de %{PAG}, una empresa de Ilmenau pionera en el meta-modelado de procesos comerciales, comenta: ") (fr "%{MS}, CEO de %{PAG}, un pionnier de la méta-modélisation des processus commerciaux basé à Ilmenau, commente:")))
  (ML Amo "Angesichts eines Minenfeldes aus Zehntausenden von Software- und Geschäftsmethodenpatenten verlangen unsere Kunden von uns Garantien gegen die rechtlichen Risiken.  Unser Vorstand denkt an die Bildung von jährlichen Patentrückstellungen in Millionenhöhe für Prozesskosten und Patentanmeldungen.  Aber auch dann, wenn wir selbst weitgehende Patentrechte auf Programmiertechniken und Algorithmen erwerben, wie das EPA sie nur allzu bereitwillig zu vergeben scheint, würde uns das nur begrenzt dabei helfen, unsere Urheberrechte an den modularen Systemen, in die wir investiert haben, vor Patenten Dritter zu schützen.  Die Patentierung würde vielmehr noch weitere Ressourcen binden und von den F&E-Investitionen hin zu den Rechtskosten transferieren. Soweit ich sehen kann, geht es den meisten kleinen und mittleren Softwareunternehmen Europas ähnlich.  Den Patentämtern bleibt diese Sachlage verborgen, und sie hätten für unsere Belange wohl auch kein offenes Ohr.  Deshalb ist es wichtig, dass die Regelungskompetenz in Sachen Patentierbarkeit bei den Parlamenten bleibt und nicht zu den Patentämtern transferiert wird." (en "With tens of thousands of software and business method patents floating around, our customers are concerned about legal risks and asking for guarantees.  Our board is considering the formation of millions of DEM of annual reserve funds to prepare for ligitation and patent applications.  But even if we acquire extensive software and business method patents, such as the EPO seems to be eager to grant, this would be of only limited use for protecting our copyrights on the modular systems into which we have been investing from patent attacks of third parties.  It would mainly add to the drain on our ressources, which have to be shifted away from R&D investments to legal costs.  As far as I can see, most of the typical European software SMEs share our experience.  The patent offices do not know about this, and they don't really care.  Therefore it is extremely important that the regulative competence about what can be patented stays with the parliaments and is not transferred to the patent offices.") (es "Con decenas de miles de patentes de software y de métosod comerciales por ahí, nuestros clientes están preocupados por los riesgos legales y piden garantías. Nuestro consejo está considerando la formación de fondos de reserva anuales para prepararse para el litigio y solicitud de patentes. Pero incluso si adquirimos patentes extensas sobre software y métodos comerciales, como la EPO está dispuesta a conceder, su utilidad sería tan sólo limitada para proteger nuestros copyrights sobre los sistemas modulares, en los que hemos invertido tanto, de ataques de patentes por parte de terceros. Nos supondría una carga sobre nuestros recursos, que tendríamos qeu desviar de la I+D a los costes legales. Hasta donde yo lo veo, la mayor parte de las PYMES europeas típicas comparten nuestra experiencia. Las oficinas de patentes no saben nada de esto, y realmente no les ipmorta. Por esta razón es extremadamente importante que la competencia reguladora sobre qué puede ser patentado permanezca en los parlamentos y no se transfiera a las oficinas de patentes.") (fr "Avec des dizaines de milliers de brevets sur des logiciels ou des méthodes d'entreprise qui circulent, nos clients sont préoccupés par les risques juridiques et réclament des garanties. Notre conseil d'administration envisage la constitution d'une réserve de fonds annuelle de millions de DM pour se préparer aux procès et dépôts de brevets. Mais même si nous acquérions des brevets extensifs sur des logiciels ou des méthodes d'entreprises, comme ceux que l'OEB semble disposé à délivrer, ce serait seulement d'une utilité limitée pour la protection de nos copyrights sur les systèmes modulaires dans lesquels nous aurions investi contre les attaques par brevets de tierces parties. Cela ajouterait principalement à la consommation de nos ressources, qui doivent être déplacées des investissements de R&D vers les coûts juridiques. Pour autant que je puisse en juger, la plupart des PME Européennes du logiciel partagent notre expérience. Les offices de brevets ne savent pas cela, et ils ne s'en préoccupent pas vraiment. Il est donc ainsi extrêmement important que la compétence de régulation en ce qui concerne ce qui peut être breveté reste aux parlements et ne soit pas transférée aux offices de brevets.")))
 (homvort
  (let ((JE "Jens Enders") (MG (ah "http://www.skyrix.de" "MDLink GmbH"))) (ML AhW "Mitunterzeichner %{JE}, Geschäftsführer der Magdeburger %{MG}, eines Technologieführers für webbasierte E-Business-Systeme, fügt hinzu:" (en "Co-signer %{JE}, CEO of %{MG}, a technology leader of web-based e-business systems from Magdeburg, adds:") (es "El co-firmante %{JE}, Director General de %{MG}, una empresa de Magdeburg líder en la tecnología de sistemas de comercio electrónico por Web, añade:") (fr "Cosignataire %{JE}, CEO de %{MG}, un leader technologique des systèmes de e-business basés sur le web de Magdeburg, ajoute:")))
  (ML Sei "Softwarepatente sind gut für alternde Firmen, die ihr Territorium für 20 Jahre abschotten wollen, um sich auf ihren Lorbeeren auszuruhen.  Zum Schutz unseres Vorsprungs tun Urheberrecht und Humankapital uns gute Dienste.  Patentpolitiker, die uns darüber hinaus %(q:starke Eigentumsrechte) einreden wollen, verkennen schlichtweg die Spielregeln unserer Branche." (en "Software patents are good for aging companies who want to fence in their territory for 20 years in order to repose on their laurels.  For the protection of our competitive edge, copyright and human capital serve us well.  Patent politicians who are trying to sell us further %(q:strong IP protection), seem to simply ignore the rules of our trade.") (es "Las  patentes de sofware son buenas para las viejas empresas que quieren vallar su territorio durante 20 años para dormirse en sus laureles.  Para la protección del espíritu competitivo, el copyright y el capital humano nos sirven bien. Los políticos de las patentes que intentan vendernos una %(q:protección fuerte a la Propiedad Idustrial), parecen ignorar simplemente las reglas de nuestro negocio.") (fr "Les brevets logiciels sont une bonne chose pour les compagnies vieillissantes qui veulent clôturer leur territoire pour 20 ans de façon à dormir sur leurs lauriers. Pour la protection de notre avancée compétitive, le copyright et le capital humain nous satisfont parfaitement. Les politiciens des brevets qui essayent actuellement de nous vendre une %(q:protection forte de la Propriété Intellectuelle) semblent ignorer simplement les règles de notre marché.")))
  (homvort
   (let ((JB (ml "Jürgen Betten"))) (ML HWW "Mit dem %(q:Basisvorschlag) wird das vollzogen, was Patentanwalt %{JB}, Vorsitzender des Software-Arbeitskreises der Europäischen Union der Patentberater, schon seit Anfang 2000 in diversen Publikationen ankündigt:" (en "The %(q:Base Proposal) completes the development, which the president of software patents workgroup of the Union of European Patent Consultants, patent attorney %{JB}, predicted in several publications at the beginning of this year:") (es "La %(q:Propuesta Base) completa el desarrollo predicho en distintas publicaciones a principios de este año por el presidente del grupo de trabajo de patentes de software de la Unión de Consultores Europeos de Patentes, el abogado de patentes %{JB}") (fr "La %(q:Proposition de Base) complète le tableau, que le président du groupe de travail sur les brevets logiciels de l'Union des Consultants en Brevets Européens, l'avocat en brevets %{JB},a prédit dans plusieurs publications au début de cette année:")))
  (ML Den "Durch die ... Entwicklung der Rechtsprechung ... hat sich das Patentrecht von der traditionellen Beschränkung auf die verarbeitende Industrie gelöst und ist heute auch für Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu befürchten, dass die deutschen Dienstleistungsunternehmen in diesen Sektoren insbesondere gegenüber der US-amerikanischen Konkurrenz ins Hintertreffen geraten." (en "By means of the ... development of jurisdiction ... the patent system has detached itself from its traditional restriction to the processing industry and is now of essential importance also for service companies in the fields of commerce, banking, insurances, telecommunication etc.  Without building a suitable patent portfolio, it is to be feared that the German service companies in these sectors will find themselves in a disadvantaged position vis-a-vis their US competitors.") (es "Por medio del ... desarrollo de la jurisdicción ... el sistema de patentes se ha separado de su tradicional restricción a la industria del procesado y es ahora de capital importancia también para compañías de servicios en los campos del comercio, banca, seguros, telecomunicaciones, etc. Sin construir un portfolio de patentes adecuado, es de etmer que las compañías de servico alemanas en estos sectores se encontrarán en una posición de desventaja frente a sus competidores Estadounidenses.") (fr "Par l'utilisation du ... développement de la juridiction ... le système des brevets s'est détaché de sa restriction traditionnelle à l'industrie de production et est maintenant également d'une importance essentielle pour les sociétés de service dans les domaines du commerce, de la banque, des assurances, des télécommunications, etc. Sans la constitution d'un portefeuille de brevets convenable, on est à craindre que les sociétés de service allemandes dans ces secteurs ne se trouvent en position désavantagée vis-à-vis de leurs compétiteurs US."))
   "..."
   (ML DAi "Das Patentgesetz gibt dem Patentinhaber das exklusive Recht an der Benutzung der patentierten Erfindung. ... In komplexen Technologiefeldern, bei denen die Durchsetzung eines %(q:Standards) häufig Voraussetzung für einen Markterfolg beim Konsumenten ist, wie beispielsweise in der Unterhaltungselektronik, der Telekommunikation oder dem Internet, sind Kreuzlizenzen eine häufige und praktikable Form der Patentverwertung geworden: Mit eigenen Patenten %(q:als Währung) erwirbt man Zugang zu Technologien, die von Mitbewerbern patentgeschützt sind." (en "Patent Law gives the patentee a right to exclude others from using the patented invention. ... In complex fields of technology, in which the establishment of a %(q:standard) is often a prerequesite for success with the consumer, such as in the area of entertainment electronics, telecommunications or the Internet, cross-licensing has become a frequent and practical form of patent exploitation:  using their own patents %(q:as a currency), companies gain access to technologies which have been patented by competing companies.") (es "La Legislación sobre Patentes da al patentador el derecho a excluír a otro sdel uso de la invención patentada. ... En campos complejos de la tecnología, en los que el establecimiento de un %(q:estándar) es a menudo un prerequisito para el éxito con el consumidor, como en el área de la electrónica de entretenimiento, las telecomunicaciones o la Internet, las licencias cruzadas se han convertido en una forma práctica y frecuente de explotación de patentes: usando sus propias patentes %(q:como moneda), las compañías pueden ganar acceso a tecnologías que han sido patentadas por compañías de la competencia.") (fr "Le droit des Brevets donne au possesseur d'un brevet un droit à interdire aux autres l'utilisation d'une invention brevetée. ... Dans les secteurs complexes de la technologie, dans lesquels l'établissement d'un %(q:standard) est souvent un prérequis pour un succès commercial, comme le secteur de l'électronique de loisir, des télécommunications ou de l'Internet, les licences croisées sont devenues une forme fréquente et pratique d'exploitation des brevets: en utilisant leurs propres brevets %(q:comme monnaie d'échange), les sociétés obtiennent un accès à des technologies qui ont été brevetées par des sociétés concurrentes."))
   "..."
   (ML N1l "Nachdem nun auch die Regierungskonferenz der Mitgliedstaaten der Europäischen Patentorganisation im Juni 1999 in Paris dem EPA das Mandat erteilt hat, vor dem 1.1.2001 eine revidierte Fassung von Art. 52 Abs. 2 EPÜ bezüglich des Ausschlusses von Computerprogrammen vorzulegen, so dass die geänderte Fassung vor dem 1.7.2000 in Kraft tritt, ist es wohl nur eine Frage der Zeit, bis die Computerprogramme (und auch die anderen Ausschlussregelungen) aus Art. 52 EPÜ gestrichen sind." (en "Since the governmental conference of the member states of the European Patent Organisation has in June 1999 in Paris entrusted the EPO with the mandate to propose before 2001-01-01 a revised version of EPC 52.2 concerning the exclusion of computer programs, so that the modified version can enter into force before 2000-07-01, it is now presumably only a question of time, until the %(q:computer programs) as well as the other exemption rules are removed from EPC 52. ") (es "Dado que la conferencia gubernamental de los estados miembros dela Organización Europea de Patentes Organisation hizo a la EPO en junio de 1999 en Paris  el encargo de proponer antes del 01-01-2001 una versión revisada de la EPC 52.5 que concierne a la exclusión de programas de ordenador, de modo que la versión modificada pueda entrar en vigor antes del 01-07-2000, es presumiblemente una cuestión de tiempo hasta que los %(q:programas de ordenador) y las demás exenciones sean eliminadas de la EPC 52.") (fr "Puisque la conférence gouvernementale des états membres de l'Organisation Européenne des Brevets a confié en juin 1999 à Paris à l'OEB un mandat pour proposer avant le 01/01/2001 une version révisée de l'article 5.2 de la CEB concernant l'exclusion des programmes d'ordinateur, de façon à ce que la version modifiée puisse entrer en vigueur avant 01/07/2000, ce n'est maintenant probablement qu'une question de temps, avant que les %(q:programmes d'ordinateur) ainsi que les autres règles d'exemption soient supprimés de l'article 52 de la CEB.")) )
  (homvort
   (ML DnW "Dem Vorsitzenden des Bundestags-Unterausschusses Neue Medien, Jörg Tauss (SPD), geht diese Entwicklung zu weit, und er sieht einen dringenden Handlungsbedarf für den Gesetzgeber:" (en "The chairman of the German Parliamentary Commission on the New Media, Jörg Tauss, sees an urgent need for the legislator to act:") (es "El presidente de la Comisión Parlamentaria Alemana sobre Nuevos Medios, Jörg Tauss, detecta una necesidad urgente de actuación por parte de los legisladores:") (fr "Le président de la Commission Parlementaire Allemande sur les Nouveaux Médias, %{JT}, voit une nécessité urgente d'action du législateur:"))
   (ML Itm "In technologiepolitischen Fachkreisen hört man immer wieder die Behauptung, das Patentsystem müsse auf gewisse Bereiche der Informationstechnik ausgeweitet werden, weil sonst deren Investitionen nicht genügend geschützt würden.  Diese Behauptung wurde bisher allerdings immer nur als abstrakte Grundwahrheit weitergegeben und niemals anhand von Tatsachen der deutschen oder europäischen IT-Wirtschaft belegt." (en "In the circles of technological policy experts, you can often here people asserting that the patent system must be extended to certain areas of information technology, whose investments would otherwise not be sufficiently protected.  This assertion has however until now always been touted like a self-evident truth, and nobody I know of ever cared to substantiate it in the light of facts from the German or European IT economy.") (es "En los círculos de expertos en política tecnológica, se oye a menudo a gente que afirma que el sistema de patentes debe extenderse a ciertas áreas de la tecnología de la información, cuyas inversiones no estarían bastante protegidas de otro modo. Sin embargo, esta afirmación se ha blandido hasta ahora como una verdad en sí misma, y nadie que conozca se ha preocupado en substanciarla a la luz de hechos de la economía de las TI alemana o europea.") (fr "Dans les cercles des experts de la politique technologique, vous pouvez souvent trouver des gens déclarant que le système des brevets doit être étendu à certaines zones de la technologie de l'information, dont les investissements pourraient sinon se révéler insuffisamment protégés. Cette affirmation a par contre été jusqu'à maintenant toujours fourguée comme une vérité découlant de soi, et personne que je connaisse ne s'est jamais soucié de lui donner corps à la lumière des faits de l'économie allemande ou européenne des technologies de l'information."))
   (ML ScW "Selbst wenn es gelänge, Bereiche der Informationstechnik zu finden, in denen Patente nachweislich vorteilhaft wirken oder gewirkt haben, müsste man noch immer untersuchen, ob eventuelle schädliche Nebenwirkungen der Patentierung diese Vorteile nicht überwiegen." (en "Even if the patent advocates succeeded in finding areas of information technology, in which patents are having or have had some positive effects, it would nonetheless still be necessary to investigate, whether these effects are not outweighed by possible detrimental side-effects of the patent system.") (es "Incluso si los partidarios de las patentes lograran encontar áreas de la tecnología de la información en la que las patentes tienen o han tenido efectos positivos, sería necesario, sin embargo, investigar si esos efectos no son contrapesados por los posibles efectos detrimentes del sistema de patentes.") (fr "Même si les partisans des brevets réussissaient à trouver des zones des technologies de l'information, dans lesquelles les brevets ont actuellement ou ont eu des effets positifs, il serait néanmoins encore nécessaire d'enquêter, pour établir si ces effets ne sont pas contrebalancés par des effets de bord contraires potentiels du système des brevets."))
   (ML Aiz "Aber während bei der Legislative noch vollkommene Unklarheit herrscht, schreitet die Judikative bereits zur Tat, gewährt Tausende von Softwarepatenten und drängt auf Änderung der Gesetzesregeln.  Es ist daher höchste Zeit für uns als Gesetzgeber, uns um diese Fragen zu kümmern." (en "But while the legislative authorities are still completely clueless on this matter, the judicial authorities are already taking action, granting thousands of software patents and pressing for a change of the legal rules.  We as legislators should therefore now take up these questions with highest priority.") (es "Pero mientras las autoridades legislativas aún están in albis sobre este tema, las autoridades juidiciales ya están tomando cartas en el asunto, otorgando miles de patentes de software y presionando para el cambio de las reglas legales. Como legisladores deberíamos, por tanto, encargarnos de estas cuestiones con la más alta prioridad.") (fr "Mais tandis que les autorités législatives sont complètement incompétentes sur ce sujet, les autorités judiciaires agissent déjà, en accordant des milliers de brevets logiciels et en faisant pression pour une modification des règles juridiques. Nous les législateurs devons ainsi nous saisir maintenant de ces questions avec la plus grande priorité.")) )
  (ML Dwt "Hier setzt der Offene Brief an. Er weist Wege, wie man den EPA-Basisvorschlag präzisieren könnte, um der befürchteten inflationären Ausweitung des Patentwesens einen Riegel vorzuschieben und das Patentwesen einer wirksamen Kontrolle durch den Gesetzgeber zu unterwerfen.  Dabei zeigt sich, dass derzeit kein gültiger Grund für eine Änderung des Gesetzestextes (Art 52 EPÜ) besteht, wohl aber für eine präzise und restriktive Handhabung einiger dehnbarer Rechtsbegriffe wie %(q:Technizität) und %(q:gewerbliche/industrielle Anwendbarkeit)." (en "The FFII letter is designed to help clarify the questions for the legislator.  It suggests means of making the EPO base proposal more specific, so as to subject the EPO to an effective control by the legislator.  Thereby it turns out that currently no valid reason exists for changing the law (EPC art 52), while there are good legal reasons for introducing precise and restrictive definitions for pliable terms such as %(q:technicity) and %(q:industrial applicability).") (es "La carta de la FFII está diseñada para ayudar a aclarar estas cuestiones para el legislador. Sugiere formas de hacer que la propuesta base de la EPO sea más específica, para poder someter a la EPO a un control efectivo por parte del legislador. De ella se extrae que no existen actualmente razones válidas para cambiar la ley (EPC artíc. 52), mientras que hay buenas razones legales para la introducción de definiciones precisas y restrictivas para términos amplios como %(q:tecnicidad) y %(q:aplicabilidad industrial).") (fr "La lettre de la FFII est destinée à clarifier ces questions pour le législateur. Elle suggère des moyens pour rendre la proposition de base de l'OEB plus spécifique, de façon à placer l'OEB sous un contrôle effectif du législateur. Il s'avère ainsi qu'il n'existe actuellement aucune raison valide pour la modification de la loi (EPC art 52), tandis qu'il existe de bonnes raisons légales pour l'introduction de définitions précises et restrictives pour des termes pliables comme %(q:technicité) et %(q:applicabilité industrielle)."))
  (filters 
   ((ep pet (ah (href 'eulux-petition))))
   (ML Dyo "Der offene Brief verweist überdies auf ökonomische Studien, auf die %(ep:Eurolinux-Petition für ein softwarepatentfreies Europa), die inzwischen von etwa 30000 Bürgern, darunter ca 400 leitenden Angestellten von IT-Unternehmen, getragen wird, sowie auf unterstützende Aussagen von fast 300 europäischen Politikern." (en "The public letter moreover cites economic studies and the %{EP}, which has meanwhile been signed by approximately 30000 citizens, including 400 executives of IT companies, and supporting statements from nearly 300 European politicians.") (es "Más aún, la carta abierta cita estudios económicos y habla de %{EP}, que ha sido firmada por aproximadamente 30.000 ciudadanos, incluyendo 400 ejecutivos de compañías de TI, y declaraciones de apoyo de casi 300 políticos europeos.") (fr "La lettre ouverte cite en outre des études économiques et la %{EP}, qui a entre temps été signée par environ 37000 citoyens, incluant 400 directeurs de sociétés de technologies de l'information, et les déclarations de soutien d'à peu près 300 politiciens européens.")) )
  (filters ((sm ahs 'miertltr)) (ML DWw "Doch selbst öffentliche Proteste dieser Größenordnung haben in den Kreisen der Patentjustiz bisher lediglich ein %(q:Schweigen im Walde) ausgelöst.  Schon im Juni 1999 setzten die beiden BMJ-Verhandlungsführer bei der Regierungskonferenz in Paris sich über %(sm:5000 Protestunterschriften) hinweg, als sie dem EPA das Mandat zur Änderung des Europäischen Patentübereinkommens erteilten.  Kurz danach gelang beiden BMJ-Patentreferenten ein beruflicher Aufstieg in München.  Einer wurde ein führender Richter am EPA, der andere Präsident des Deutschen Patent- und Markenamtes.  Auch der heutige Präsident des EPA und Initiator des Basisvorschlages, Dr. Ingo Kober, begann seine Karriere im BMJ." (en "But even public protests of this amplitude have so far not prompted more than a %(q:silence in the forest) on the part of the patent system's decisionmakers.  Already in June 1999 the two BMJ officers in charge of negotiating for the German delegation at the intergovernmental conference in Paris did not react to an appeal based on %(sm:5000 signatures), and gave the EPO the mandate to change the European Patent Convention.  Little later, both BMJ officers were promoted to new honors in Munich.  One became a leading judge at the EPO, the other the president of the German Patent Office.  And today's president of the European Patent Office and initiator of the %(q:Base Proposal), Dr. Ingo Kober, also started his career in the BMJ.") (es "Pero ni las protestas públicas de este calibre han conseguido extraer más que un  %(q:silencio en el bosque) por parte de los gestores del sistema de patentes.  Ya en junio de 1999 los dos funcionarios del BMJ a cargo de llevar las negociaciones de la delegación alemana en la conferencia intergubernamental de París no reaccionaron a la petición basada en (sm:5000 firmas), y dieron a la EPO el encargo de cambiar la Convención Europea de Patentes. Un poco más tarde, ambos funcionarios del BMJ fueron ascendidos a nuevos cargos en Munick. Uno se convirtió en un juez de prestigio en la EPO, el otro en el presidente de la Oficina Alemana de Patentes. Y el presidente actual de la Oficina Europea de Patentes e iniciador de la %(q:Propuesta Base), Dr. Ingo Kober, también comenzó su carrera en el BMJ. ") (fr "Mais même une protestation publique de cette ampleur n'a pas pour l'instant déclenché plus qu'un %(q:vaste silence) de la part des preneurs de décision du système des brevets. Déjà en juin 1999 les deux responsables du BMJ en charge des négociations pour la délégation allemande à la conférence intergouvernementale de Paris n'ont pas réagi à un appel basé sur %(sm:5000 signatures), et ont donné à l'OEB le mandat de modification de la Convention Européenne des Brevets. Un peu plus tard, les deux responsables du BMJ ont été promus à de nouvelles fonctions à Munich. L'un est devenu un juge principal à l'OEB, l'autre le président de l'Office Allemand des Brevets. Et le président actuel de l'Office Européen des Brevets et initiateur de la %(q:Proposition de Base), le Dr. Ingo Kober, a aussi commencé sa carrière au BMJ.")))
  (ML Dsn "Das EPA finanziert sich durch Patentgebühren." (en "The EPO finances itself from patent fees.") (es "La EPO se financia de las tarifas de patentes.") (fr "L'OEB se finance lui-même à partir de frais pour les brevets qu'il accorde."))

(sects
(ref (tok euluxref-tit)
(eulux-references
 'eulux-petition
 'swpat
 'ffii
))

(prini (sepcc " - " (ML nWW "Über den FFII" (en "About FFII") (fr "A propos de la FFII") (es "Acerca de la FFII")) (ahh "www.ffii.org")) 
 (filters ((fi ahs 'ffii) (sp ahs 'swpatgirzu) (sj ahs 'swpatsarji) (el ahs 'eurolinux))
 (let ((FFII (tpe (ahs 'ffii) "FFII e.V.")))
 (ML cas "Der %{FFII} ist ein gemeinnütziger Verein, der die Entwicklung offener Schnittstellen, quelloffener Programme und frei verfügbarer öffentlicher Informationen fördert und sich für ein kraftvolles Zusammenwirken freier und proprietärer Software zum Zwecke eines langfristigen Aufbaus informationeller Gemeingüter auf der Grundlage offener Standards, fairen Wettbewerbs und der Achtung legitimer Urheberrechte einsetzt.  Der FFII koordiniert eine %(sp:Arbeitsgruppe zum Schutz der digitalen Innovation vor Softwarepatenten), die %(sj:von erfolgreichen deutschen Softwarefirmen unterstützt) wird.  Der FFII ist Gründungsmitglied der %(el:EuroLinux-Allianz für eine Freie Informationelle Infrastruktur)." "%(fi:FFII) is a non-profit association which promotes the development of open interfaces, open source software and freely available public information. FFII coordinates a %(sp:workgroup on software patents) which is %(sp:sponsored by successful german software publishers). FFII is member of the %(el:EuroLinux Alliance)." (es "La %(fi:FFII) es una asociación sin ánimo de lucro que promueve el desarrollo de interfaces abiertas, software \"open source\" e información pública libremente disponible. La FFII coordina un %(sp:grupo de trabajo sobre patentes de software) el cual está  %(sp:patrocinado por importantes editores alemanes de software). La FFII es miembro de la %(el:Alianza EuroLinux).") (fr "La %(fi:FFII) est une association à but non-lucratif qui fait la promotion du développement d'interfaces ouvertes, de logiciels open source et d'une information publique accessible au public. La FFII coordonne un %(sp:groupe de travail sur les brevets logiciels) qui est %(sp:soutenu par des éditeurs de logiciels allemands reconnus). La FFII est membre de l'%(el:Alliance EuroLinux).")))
) )

(press  (tok euluxpress-tit)
 (dl (l (ML EPs "email" (de "E-Post") (fr "courriel")) "info@ffii.org") (l (ML EPs2 "Tel") (spaced "Hartmut Pilch" "+49-89-18979927")))
)

(url (tok euluxurl-tit)
     (absurldir docid)) ) 
))
)


(list 'swpatstidi-dir
(mlhtdoc 'swpatstidi nil nil nil
(filters ((ep ahs 'epue52) (sg ahs 'swpatbasti) (tf ahs 'swpattrips) (kc ahs 'swpatkorcu) (pr ahs 'swpatpreti))
(ML Wta "We have worked out a %(ep:proposal for a precisation of Art 52) and a %(sg:proposal for a possible third paradigm property system).  Moreover we have provided the %(tf:TRIPS fallacy) with a home and hope to create an FAQ for debunking other frequently encountered fallacies.  You can also find a short article on %(kc:how the law was bent in Europe).  Moreover we have started a %(q:survey action)." (de "Wir haben einen %(ep:Vorschlag zur Präzisierung von Art 52) und einen %(sg:Entwurf für eine Patent-Alternative im Bereich der Rechenkonzepte) erarbeitet.  Ferner bieten wir umherirrenden proprietaristischen Scheinargumenten wie etwa der %(tf:TRIPS-Lüge) eine Netzheimat.  Eigentlich sollten wir auch anderen häufig vorgetragenen Scheinargumenten schleunigst mit einer FAQ begegnen.  Immerhin haben wir begonnen, die %(kc:Geschichte der Rechtsbeugung) in diesem Bereich übersichtlich zu dokumentieren und eine %(pr:Fragebogenaktion) vorzubereiten.")))
(docmenu-large '(swpatkorcu swpattisna swpatfrili swpateurili swpattrips epue52 swpatbasti swpatlijda swpatpreti swpatdanfu))
)

(let ((texlink 'footnote))
(mlhtdoc 'swpatkorcu nil nil nil
(sects
(intro (ML Ueh "Universelle Patentierbarkeit oder Beschränkung auf Technik?")
(filters ((bp ahs 'bpatg17w6998)) 
(ML Bns "Bisher sind Computerprograme nicht patentierbar, was nicht ausschließt, dass eine patentfähiges technisches Verfahren auch programmgesteuert ablaufen kann.  Dieses Verfahren muss jedoch auf neuen Erkenntnissen über kausale Wirkungszusammenhänge von Naturkräften beruhen und eine Lösung eines industriellen Problems darstellen. Der Bereich der reinen Vernunft, d.h. des Rechnens, Abstrahierens und Programmierens, soll nach dem Willen des Gesetzgebers von Patentansprüchen frei bleiben.  In seinem wegweisenden Urteil %(q:Dispositionsprogramm) von 1976 stellt der Bundesgerichtshof fest, dass eine Ausdehnung des Patentwesens in diesen Bereich grundlegende Freiheitsrechte bedrohen würde und gleichzeitig dem Fortschritt der Technik nicht förderlich wäre.  Neuere Erkenntnisse der Wirtschafts- und Informationswissenschaften bestärken diese weise und klare Grenzziehung des Gesetzgebers und ihre systematische Auslegung durch die höchstrichterlichen Grundsatzentscheidungen seit 1976, die letztlich in den heute noch gültigen Prüfungsrichtlinien und Gesetzeskommentaren ihren Niederschlag fanden und im August 2000 noch einmal eindrücklich %(bp:vom Bundespatentgericht bestätigt) wurden.")) 

(ML GWz "Gleichzeitig gibt es seit den 70er Jahren eine zunehmend einflussreiche Gruppe von Patentjuristen, die eine grundsätzliche Beschränkung des Patentwesens auf die %(q:Welt der Dinge) nicht hinnehmen und es stattdessen über die Mikroelektronik in die bloße Informationsverarbeitung und damit letztlich in alle wirtschaftlich interessanten Bereiche des Lebens ausdehnen möchten. Diese Gruppe hat sich im Europäischen Patentamt (EPA) und einem Teil der nationalen Gerichte durchgesetzt und nach und Wege gefunden, um gesetzeswidrig Patente auf %(q:Computerprogrammprodukte), Computerprogramme, Geschäftsverfahren, Organisationsverfahren und immaterielle Gegenstände aller Art zu erteilen.  In Deutschland haben insbesondere die Patentabteilungen von Siemens und IBM im Zusammenspiel mit dem 10. Senat des BGH für eine %(q:Rechtsfortbildung) in diesem Sinne gesorgt")

(filters ((ep ahs 'eulux-petition)) (ML Dst "Da die auf dieser Grundlage gewährten Patente von ungewissem Wert sind und regelmäßig von gesetzestreuen Gerichten zurückgewiesen werden, drängen die tonangebenden Patentjuristen derzeit darauf, die Gesetze umzuschreiben.  Auf die verheerenden wirtschaftlichen und gesellschaftlichen Folgen der Softwarepatentierung, vor denen %(ep:über 60000 Unterzeichner und 200 IT-Firmen eindringlich warnen), können sie dabei keine Rücksicht nehmen.  Die Frage der volkswirtschaftlichen Auswirkungen spielte bisher keine Rolle.  Aber selbst in rechtssystematischer Hinsicht begeben sich das EPA und seine Freunde auf Glatteis.  Ehemals klaren Grenzziehungen steht heute nur noch ein %(q:dynamischer Technikbegriff) gegenüber, dessen Konturen umso mehr verschwimmen, je näher man sich mit dem EPA-Fallrecht beschäftigt.  Weitere Ausweitungen in beliebige Richtungen sind vorprogrammiert.  Als einziges verlässliches Abgrenzungskriterium verbleibt letztlich das Interesse des EPA und seiner Großkunden.  Wie ein EPA-Patentprüfer in einem Netzforum sinngemäß kommentierte:  %(q:Bei Patenten geht es um Geld.  Wo es kommerzielle Interessen gibt, wird es auch Patente geben müssen).  Aber während die Münze im Kasten klingt, verliert das Patentwesen seine Legitimität, die ihm bei sauberer Eingrenzung auf die Kernkompetenzen noch lange hätte erhalten bleiben können.  Von einem politischen Machtapparat kann man jedoch solche Weisheit nicht erwarten: eher startet der europäische Patentkonzern einen blinden Expansionsfeldzug in gefährliches Gelände, als dass er bereit wäre, seinen allmählichen Rückzug aus vielen zunehmend mathematisierten Gebieten des menschlichen Wissens selbst zu organisieren.")) )

(stud (ML Wrk "Weitere Lektüre")
(linklist
(l (kaj (ahs 'twinipat) (ahswpr 'drtw)) 
   (blockquote (cite (ML DdW "Das EPA selbst hat bisher keinerlei Systematik entwickelt.  Selbst die ausführlich diskutierte Entscheidung %(q:Computerprogrammprodukt/IBM) greift auf verschiedene Einzelfälle zurück. Die Judikatur des EPA erscheint sohin von Kasuistik geprägt, eine Definition des erforderlichen %(q:technischen Effektes) bleibt selbst die zuletzt genannte Entscheidung schuldig - dies obwohl das EPA, wie bereits erwähnt, gedenkt seine Rechtsprechung künftig an dieser Entscheidung auszurichten."))) )  
(l (ahs 'grur-kolle77)
   (ML GlW "Gert Kolle, heute Bürokrat im Europäischen Patentamt, war in den 70er Jahren der maßgebliche Rechtstheoretiker in der Frage der Patentierbarkeit von Computerprogrammen.  Er agierte als wissenschaftlicher Referent und Berichterstatter der deutschen Delegation bei verschiedenen Patentgesetzgebungskonferenzen der 70er Jahre, bemühte sich stets um einen unparteiischen wissenschaftlichen Standpunkt fernab jeglicher %(q:ideologischer Versteinerung), in der die beiden Fronten schon damals aufeinanderprallten.  Im vorliegenden GRUR-Artikel von 1977 erklärt er, warum Computerprogramme nicht als %(q:technisch) im Sinne des Patentrechts gelten können und warum eine %(q:naiv oder bewusst) herbeigeführte %(q:Lockerung des Technikbegriffs) zu unerhörten Sperrwirkungen und unverantwortbaren Machtanhäufungen führen würde.  Laut Kolle muss es daher ein %(q:Niemandsland des Geistigen Eigentums) geben, und Algorithmen sollten %(q:vergesellschaftet) werden.  Ein wegen seiner Tiefe und Klarheit sehr empfehlenswerter Artikel, der nach über 20 Jahren kaum etwas von seiner Aktualität verloren hat."))
(l (ahswpr 'skk-patpruef)
   (filters ((et ahs 'skk-patpruef)) (ML Erl "Ein Prüfer am Deutschen Patentamt nimmt die Ungesetzlichkeiten und Ungereimtheiten der aktuellen Softwarepatentierungspraxis unter die Lupe -- brilliante und profunde Analyse vom November 2000.  Wir haben auch eine %(et:englische Übersetzung) angefertigt.")))
(l (ahs 'lamy98 (ml "Lamy Droit Informatique"))
   (ML SPr "Standardnachschlagewerk des Computerrechts. Erklärt, wie das EPA schrittweise das Recht verbogen hat und von welch ungewisser Gültigkeit die auf diese Weise erteilten Softwarepatente sind."))
(l (ahs 'swpateurili)
   (filters ((ep ahs 'swpatlijda)) (ML Enn "Es bedürfte nur weniger Worte, um begriffsstutzigen Patentgerichten zu erklären, wie Art 52 EPÜ / §1 PatG zu verstehen ist.  Dieser Eurolinux-Entwurf einer EU-Richtlinie hat bequem auf einer DIN-A4-Seite Platz.  Die Vertreter des %(ep:Europäischen Patentkonzerns) hingegen reden in langen Traktaten von %(q:Klärung) und %(q:Harmonisierung) und erzeugen dabei mit jeder Zeile ein Stück mehr Begriffsverwirrung und dissonanzträchtige Unschärfe.")))
(l (ahs 'boch97-koerber) (ML Dla "Der Patentchef von Siemens erklärt, wie seine Abteilung aktiv zur %(q:Rechtsfortbildung) beigetragen hat, um die Praxis des deutschen Patentamtes dem Stand der (im globalen Geschäftsumfeld maßgeblichen) amerikanischen Rechtsprechung anzupassen."))
(l (ahs 'grur-mellu98) (ML Dae "Der Softwarepatent-Experte des BGH erklärt am Beispiel Microsoft, warum Softwarepatente wirtschaftspolitisch erwünscht sein müssen, und zeigt Wege auf, wie man sie trotz Gesetzeslage einführen kann.  Er erklärt, dass der Gesetzgeber nur untechnische Gegenstände vom Patentschutz habe ausschließen wollen, und dass Computerprogramme technisch seien.  Diesen Widerspruch löst Melullis, indem er eine metaphysische Entität namens %(q:Computerprogramm als solches) konstruiert und ihr eine obskure Bedeutung zuweist."))
(l (ahs 'swpattisna) (docdescr 'swpattisna))
) )
)))

(mlhtdoc 'swpatbghist nil nil nil
(sects
(bundpol (ML Dod "Der Wirtschaftspolitische Wille des Gesetzgebers")
(ML DDi "Die geltenden Gesetze (PatG, EPÜ) wurden in den 70er Jahren geschaffen.  Damals fand in den europäischen Flächenstaaten eine rege Diskussion über den patentrechtlichen Status von Computerprogrammen statt.  Es gab zwei Parteien.  Die einen wollten das Patentwesen für alle wirtschaftslich interessanten automatisierbaren Lösungen bereit halten.  Die anderen wollten es auf Lösungen beschränkt wissen, die einen unmittelbaren Einsatz von Naturkräften zur Herbeiführung eines kausal übersehbaren Erfolges lehren.  Letztere setzten sich in dem Moment durch, als die Gesetze beschlossen wurden.  In den 80er Jahren verschob sich jedoch allmählich das Gewicht zugunsten der ersteren.  Mehr dazu:")

(ul (ahs 'swpatkorcu) (ahs 'grur-kolle77))

(ML Lle "Lange Zeit blieb diese Entwicklung außerhalb patentjuristischer Kreise unbeachtet.  Im Jahr 2000 drang sie wieder auf die Ebene der Politik vor.  Führende Politiker alle Fraktionen des Bundestages kritisierten die expansive Patentierungspraxis.")
)

(bghwpol (ML DtW "Der Wirtschaftspolitische Wille der Patentbewegung")
(ML Due "Die Patentfachwelt führte immer wirtschaftspolitische Argumente an, um für eine Änderung der Rechtslage zu plädieren.  Vor allem handelt es sich dabei um folgende Argumente, die auch BGH-Richter Melullis als Begründung für die Regeländerungen anführt:")
(dl
(l (ML Bwf "Bedeutungszuwachs der Softwarebranche")
   (ML DPG "Die Softwarebranche sei so bedeutend geworden, dass ihren Investitionen der Patentschutz nicht länger versagt werden dürften.  Die Gesetze müssen an diese Gegebenheiten angepasst werden.  Die Entscheidung des Gesetzgebers sei falsch gewesen.")
   (filters ((gk ahs 'bgh-dispo76)) (ML Dln "Diese Argumentation beruht auf der Voraussetzung, dass alles patentierbar sein müsse, was wirtschaftlich bedeutend ist.  Sie entspricht zwar dem Interesse und der Ideologie der Patentbewegung, ist aber ansonsten völlig unbegründet und wurde bereits in den 70er Jahren %(gk:vom BGH ausdrücklich zurückgewiesen).")) )


(l (ML Vod "Urheberrecht schützt unzureichend gegen Nachahmung")
   (ML Dga "Diese Argumente wurden nie geprüft.  Eigentlich sind gerade Computerprogramme im Vergleich zu anderen Informationsgütern relativ schwer nachahmbar.  Dekompilierung bringt wenig.  Kopieren ist verboten.  Einem geringen Erfindungsaufwand steht ein hoher Nachahmungsaufwand gegenüber.  Im BGH sitzen nur Juristen, die dies offenbar nicht wissen.")
)

(l (ML PWe "Patente schützen den kleinen Innovator vor dem großen Monopolisten")
   (ML BTe "Bei Zehntausenden von jährlichen Swpat-Anmeldungen ist das graue Theorie.  Auf diesem Spielfeld profitieren nur diejenigen Kleinfirmen, die selber keine Programme schreiben und daher nicht auf Tauschgeschäfte mit dem Monopolisten angewiesen sind.  D.h. Patentverwertungsfirmen, nicht Softwarefirmen.") )
) )

(leghkon (ML Bul "Bis zum Bruch des Völkerrechts")
(ML Pie "Patente sind zwar eine nationale Angelegenheit, aber insbesondere im digitalen Bereich betreffen sie auch andere Länder.  Aus diesen Gründen gibt es viele internationale Verträge, so auch das Europäische Patentübereinkommen.  Auf die Einhaltung dieses Übereinkommen sind die deutschen Gerichte auch gegenüber dem Ausland verpflichtet.  Wenn eines Tages spanische Softwarefirmen in Deutschland durch Patente in ihren Rechten beeinträchtigt werden, könnten sie ihre Regierung dazu bewegen, beim Europäischen Gerichtshof gegen Deutschland zu klagen.  Eine solche Klage mag im Moment politisch eher unwahrscheinlich erscheinen, aber sie hätte Aussicht auf Erfolg, und die Zeiten können sich wenden.")
)

(ref (ML Wrk "Weitere Lektüre")
 (linklist
  (l (ahs 'swpatkorcu) (ML AEu "Allgemeine Einführung und Verweise"))
  (l (ahs 'grur-mellu98) (ML BiW "BGH-Softwareexperte Melullis erklärt seine Wirtschaftspolitik und erläutert, warum diese eine Änderung der Regeln erforderlich macht."))
  (l (ahs 'bpatg17w6998) (ML DAe "Das Bundespatentgericht widerlegt einige der Melullis-Argumente in einem Urteil vom August 2000, gegen welches IBM wiederum Berufung einlegte -- beim BGH natürlich."))
  (l (ahs 'skk-patpruef) (ML Ven "Ein Patentprüfer des DPMA nimmt die Melullis-Argumentation schonungslos und genüsslich auseinander."))
  (l (ah "http://www.ffii.org/archive/mails/swpat/2000/Dec/0209.html" (ML SnP "Sprachanalyse-Pingpong")) (filters ((gh ahvtan 'jurpc-bgh-sprachanalyse00) (pg ahvtan 'jurpc-bpatg-sprachanalyse98)) (ML DWh "Der BGH %(gh:erklärt) %(q:programmtechnische Vorrichtungen) für patentierbar.  Jede geistige Idee muss künftig nur noch als %(q:Vorrichtung) eingekleidet werden, und schon gibt es ein Patent.  Im vorliegenden Falle ging es um die %(q:Erfindung), bei der automatischen Analyse eines Satzes dem Leser die Möglichkeit zu geben, zwischen mehreren möglichen Treffern auszuwählen und dadurch die Analyse zu verfeinern.  Das Bundespatentgericht hatte die %(pg:besseren Argumente), der BGH jedoch die Macht.")))
  (l (ahs 'boch97-koerber) (ML Dha "Der Leiter der Patentabteilung von Siemens erklärt, dass Siemens einen aktiven Beitrag zur %(q:Rechtsfortbildung) in Sachen Softwarepatentierung geleistet habe.  Um %(q:konservative) Gerichte dazu zu bewegen, Softwarepatente anzuerkennen, wurden einige Musterprozesse vor dem BGH geführt."))
)
)
)
)

(mlhtdoc 'swpattisna nil nil nil
(sects
(arm (ML Ler "Arms Race Logics" (de "Logik des Wettrüstens"))

(ML Gam "Generally it is believed that large corporations like IBM and Siemens are interested in extending the patent system, because they profit from keeping small companies out.  But that is at best half of the truth." (de "Allgemein wird angenommen, Firmen wie IBM und Siemens seien an einer Ausweitung des Patentsystems interessiert, weil es Großunternehmen begünstigt.  Aber das ist bestenfalls die halbe Wahrheit."))
(filters ((etc "Qualcomm" "Priceline" "TechSearch")) (ML Ins "In fact, large corporations are not those who benefit most from the patent system.  Some smaller companies who focus entirely on developping patents rather than products are even better fit for survival in a patent-cluttered environment.  Companies like %{LST} and many others can be a pain in the neck for Siemens or IBM and may be causing these companies to lose more than they can gain through software patents." (den "In Wirklichkeit sind Großunternehmen nicht diejenigen, die am meisten vom Patentwesen profitieren.  Einige kleinere Unternehmen, die sich ganz auf die Entwicklung von Patenten statt von Produkten konzentrieren, sind u.U. noch besser für den Überlebenskampf auf dem Patent-Minenfeld optimiert.  Firmen wie %{LST} können Siemens oder IBM übel zusetzen und ihnen die Freude am Patentwesen gründlich verderben.")))
(ML Wtt "Why is it then that large corporations are pushing for an extension of patentability in Europe?" (de "Wie kommt es dann, dass Siemens, IBM u.a. sich unermüdlich für die Ausweitung der Patentierbarkeitsgrenzen in Europa stark machen?"))
(filters ((ak ahs 'boch97-koerber)) (ML Are "An recent %(ak:article by Arno Körber), head of the patent department of Siemens, gives the answer." (de "Ein neuerer %(ak:Artikel von Arno Körber), Chef der Patentabteilung von Siemens, liefert die Antwort.")))
(ML KWn "Körber regrets the loss of %(q:patent peace) in the area of micro-electronics and software and ascribes this to decisions by US lawcourts, namely the CAFC, in the early 1980s.  He describes how this has adversely affected interoperability, and he does not claim that it has brought any benefit either to Siemens or the economy as a whole.")
(ML Ybt "Yet Körber says that Siemens has been pushing German patent courts and patent offices to emulate the US practise.  The reason for this is that Siemens is oriented toward the global market, and this market has come under a strong impact of the US patent system.  In order to make sure Siemens obtains as many patents as its US-based competitors, the patent department must make sure Siemens employees are motivated to patent everything they develop.  According to Körber, they will not be sufficiently motivated, if they can not obtain a valid patent at home as well as in the US.")
(ML Kws "Körber's patent department developped various various incentive systems for motivating Siemens employees to patent what they can, and Körber writes that they have proven a great success.  He does not explain why incentive systems alone could not be enough of a motivation in those cases, where a patent can be obtained only in the US.  Given the global nature of Siemens and of the communication standard negotiations for which Siemens needs the US patents, it is hard to believe that the motivation gap cannot be overcome.  It is however clear that being able to file patents at home makes life somewhat easier for the Siemens patent department.")
(ML CWW "In general, global monoculture is very attractive for executives of global companies.  Just as they like to %(q:standardise on Microsoft), they also tend to orient their company toward one national patent system, viewing all others as cost-generating deviations.  And it is the patent department that decides which %(q:patenting standard) the company will push.")
(ML Krr "Körber writes that Siemens contributed its part to pushing German caselaw into the direction of software patentability.  In fact, Siemens and IBM have fought various borderline cases through to the hightest courts and obtained rule-setting decisions that extended patentability step by step.  Those companies who might suffer from an infringement lawsuit usually have no interest to push for a rule-setting decision but only in protecting themselves from a particular patent, and they will usually not fight this through to the supreme level of jurisdiction.  Before patent offices and patent courts, the voice of patentees is heard much more prominently than the voice of occasional %(q:infringers) and %(q:opponents).  The voice of the general public is not heard at all.  This has even led patent law scholars to formulate a principle of %(q:in dubio pro inventore) -- in case of doubt a lawcourt will side with the patentee.")
(ML Att "Moreover, in general it is the patent department that decides a company's policy on patentability.  And, as Körber points out, thanks to the recent extension of patentability, the patent department of Siemens has become very important.  It is no longer a mere service facility but a center of enterpreneurial activities.  It is well a known reality that any organisation will tend to opt in favor of those principles that allow its representatives to dominate the organisation.  Whether these are good for the organisation as a whole is only a secondary matter.")
(ML FWt "Similar priniciples apply to patent offices and patent lawcourts.  The more patents they grant, the more important they become.")
(linol
 (ML Tds "To summarize, we can discern several motors of patent inflation:")
 (ML tei "By %(q:standardising) on the world's most inflationary patent practise, the patent departments of large companies can get their job done most effiently.")
 (ML cwf "Due to the rules of the patent litigation game, caselaw development works mainly toward inflation, not deflation.")
 (ML pep "Patent inflation reinforces the power of those who set the rules of the patent system.")
)

)

(hist (ML Dcs "Geschichte der Patentinflation in Deutschland" "History of Patent Inflation in Germany")

(ML DWW "Das Patentsystem konnte 1877 in Deutschland nur deshalb gegen eine breite Front der Ablehnung durchgesetzt werden, weil es damals vorsichtig auf einen Kernbereich eingegrenzt wurde, in dem es wenig Schaden und nachweislich einigen Nutzen stiftet:  Erfindungen mit %(s:industrieller Anwendung), wobei nicht nur abstrakte Geistestdtigkeiten sondern auch Landwirtschaft, Bergbau uvm ausgeschlossen wurden.  Später wurde präzisiert, es müsse eine %(s:neue Lehre zum Einsatz von Naturkräften) vorliegen." (en "The patent system was established in 1877 in Germany against a very critcial public opinion.  It prevailed only because its proponents were able to limit it to a core domain, in which its restriction on freedom is not widely felt and its usefulness is often highly valued: inventions with an %(q:industrial application).  Not only were abstract logical and mental activities excluded, but also agriculture, mining and other not strictly industrial areas.  Later it was clarified that a %(q:new teaching about the use of natural forces) had to be at the core."))

(filters ((ep ahv 'epue52)) (ML Iia "In der fertigenden Industrie arbeiten nur relativ wenige Gewerbetreibende, deren Produktentwicklung mit hohem organisatorischen Aufwand und gro_en Ausr|stungsinvestitionen verbunden ist.  Diese Industriebetriebe halten ihr Wissen geheim, um die Früchte ihrer Entwicklungsarbeit vor der Ausbeutung durch weniger innovative Konkurrenten zu schützen.  Dauerhafte Geheimhaltung ist noch weniger wünschenswert als Monopole.  Somit bietet das Patentwesen einen Ausweg:  man gewährt dem Erfinder ein befristetes Monopol als Belohnung dafür, dass er sein Wissen veröffentlicht.  Die Öffentlichkeit bekommt etwas anstelle von nichts.  Sie verzichtet zeitweilig auf eine Freiheit, die sie ohne das Patent wahrscheinlich gar nicht gehabt hätte.  Mit diesem kühlen wirtschaftspolitischen Kalkül konnte Werner Siemens mit seinem %(q:Deutschen Patentschutzverein) die Regierung in einem Moment der Wirtschaftskrise überzeugen, als die Delbrücksche Freihandelspolitik diskreditiert war und stattdessen Zollschutz und staatlicher Interventionismus angesagt waren." (en "In the manufacturing industry, there are usually only a handful of companies, whose product development involves high organisational efforts and equipment costs.  Industrial companies keep their knowledge secret, in order to protect themselves against the depletion of their funds by copycat companies, who need less research and development.  Secrecy may be a greater evil than monopolies.  Thus the patent system strikes an interesting deal: the inventor is granted a temporary monopoly in return for publishing his knowldege.  The public gets something, where otherwise it would have gotten nothing.  It temporarily gives up a freedom that it wouldn't have had without the patent.  With this argument, Werner von Siemens and his %(q:German Patent Protection Association) were able to persuade the imperial government in a moment of crisis, where Delbrück's free trade policy was considered to have failed and all kinds of state interventionism, especially tariffs and protectionist policies, had become the order of the day.")))

(lin (ML USP "Um die Skepsis der Gegner zu überwinden, propagierten Siemens und sein Patentschutzverein ein streng auf wirtschaftspolitischer Zweckmäßigkeit aufbauendes Patentkonzept.  Das Patentwesen sollte ein Mittel zur Steuerung des Wettbewerbs zwischen großen Industrieunternehmen sein.  Von Versuchen naturrechtlicher Begründung eines %(q:geistigen Eigentums) sowie moralisierender %(q:Erfinder-Romantik) wurde abgesehen. Als Patentinhaber waren die wirtschaftlich verantworltichen Kollektive, nämlich die Industrieunternehmen, vorgesehen.  Ein persönlicher Erfinder musste nicht genannt werden.  Schon eine hohe Preishürde von 15000 Reichsmark sorgte dafür, dass das deutsche Patentwesen selten von Privatpersonen in Anspruch genommen wurde.  Ferner wurde es per Gesetz auf den Bereich der produzierenden Gewerbe eingeengt:")
     (blockquote    
      (ant 
       "Reichstagsdrucksache, 3. Legislatur-Periode, 1. Session 1877, Nr. 8:"
       (ML Lxg2 "Der Entwurf beschränkt die Patentfähigkeit auf solche Erfindungen, welche eine gewerbliche Verwertung gestatten.  Eine derartige Verwendung kann bestehen in der gewerbsmäßigen Herstellung des erfundenen Gegenstandes oder in seinem Gebrauch innerhalb eines gewerblichen Betriebes.  Auf diese Weise sind rein wissenschaftliche Entdeckungen, die Auffindung unbekannter Naturprodukte, die Entdeckung unbekannter Produktivkräfte, die Aufstellung neuer Methoden des Ackerbaues oder Bergbaues usw, die Kombination neuer Pläne für Unternehmungen auf dem Gebiete des Handels von dem Patentschutze ausgenommen.  Der Entwurf folgt in dieser Hinsicht den in allen Staaten ausdrücklich oder durch die Praxis anerkannten Grundsätzen.") ) ) )

(ML Nln "Nach der Verabschiedung des Patentgesetzes von 1877 verbreitete sich unter den Patentanmeldern erste Unzufriedenheit über die Einschränkungen.  Es entstand u.a. eine %(q:erfinder-romantische) Reformbewegung, die den persönlichen Erfinder ins Zentrum der Überlegungen gerückt und damit das Patentwesen aufgewertet wissen wollte.  Während des Kaiserreiches und der Weimarer Zeit gelang dieser Bewegung jedoch kein wesentlicher Durchbruch.  Sie fand Unterstützung bei den Sozialdemokraten und später insbesondere bei den Nationalsozialisten.  Adolf Hitler machte sich in %(q:Mein Kampf) ihre Forderungen zu eigen, und 1936 wurden diese Forderungen schließlich umgesetzt.  Die nationalsozialistischen Patentreformen wurdn nach dem zweiten Weltkrieg in der Bundesrepublik gegen einigen Widerstand aus der Großindustrie bestätigt und schlugen sich in dem weltweit einzigartigen und von vielen als vorbildlich betrachteten Arbeitnehmer-Erfindergesetz (ArbErfG) nieder, welches dem angestellten Erfinder starke Druckmittel gegen sein eigenes Unternehmen in die Hand gibt und daher noch heute von manchen Unternehmervertretern als Standortnachteil kritisiert wird.")


(tpe "..." (ML Feg "to be cont'd" (de "Fortsetzung folgt")))

)

(etc (ML Wrk "Weitere Lektüre" "Further Reading")
(framebox (ul
(ahs 'boch97-koerber)
(ahs 'grur-kolle77)
(kaj (ahs 'boch97-gispen-de) (ahs 'boch97-gispen-ns))
(kaj (ahs 'sffo-machlup) (ahs 'sffo-andres00-vw) (ahs 'sffo-andres00-nw))
(ahs 'firstmonday-kahin0101)
(ahs 'karn-patents) 
))
)
) )

(mlhtdoc 'swpattisna_old (ML BWe "Bis zum Bruch des Rechts -- Patentämter auf Expansionskurs" (en "In Defiance of Economy and Law"))
nil
nil ;; (setq langtxt-keyvals nil)
(lin (filters ((h ah "http://www.opensource.org/halloween/")) (MLL IgW "In den %(h:Halloween-Dokumenten) stellt ein Microsoft-Stratege bekanntlich Überlegungen an, wie man die stürmische Entwicklung der Freien Software bremsen könne.  Nach langem Prüfen aller Möglichkeiten empfiehlt er vor allem zwei Wege:" "In the %(h:Halloween Documents) a Microsoft strategist explores remedies against the threat that opensource software like Linux and Apache poses to his company.  After an exhaustive investigation into all possibilities, he proposes two paths to follow:")) (ol (ML PEe "Proprietäre Erweiterung/Ersetzung von bisher offenen Internet-Protokollen" (en "proprietary extension/replacement of so-far open internet protocols")) (ML SmS "Flächendeckende Anmeldung von %(q:Softwarepatenten)" (en "Systematic acquisition of %(q:software patents)"))))

(ML Dde "Doch bis vor wenigen Jahren waren %(q:Softwarepatente) nur eine von vielen Extravaganzen des amerikanischen Patentsystems, über die der Rest der Welt gerne staunend den Kopf schüttelte.  Rein informationelle Werke wie Computerprogramme gehören in die Domäne des Urheberrechts, meinte man.  Wenn der Staat es einem Autor verbot, eigenständig erarbeitete Informationswerke zu veröffentlichen, nannte man das bisher %(q:Zensur)." (en "But this kind of protection until very recently would have worked only in the USA, and even there patenting of informational works is rather new. Informational works are the domain of copyright.  When the state disallows someone the publication of his own work, that is usually called %(q:censorship)."))

(ML DWW "Das Patentsystem konnte 1877 in Deutschland nur deshalb gegen eine breite Front der Ablehnung durchgesetzt werden, weil es damals vorsichtig auf einen Kernbereich eingegrenzt wurde, in dem es wenig Schaden und nachweislich einigen Nutzen stiftet:  Erfindungen mit %(s:industrieller Anwendung), wobei nicht nur abstrakte Geistestätigkeiten sondern auch Landwirtschaft, Bergbau uvm ausgeschlossen wurden.  Später wurde präzisiert, es müsse eine %(s:neue Lehre zum Einsatz von Naturkräften) vorliegen." (en "The patent system was established in 1877 in Germany against a very critcial public opinion.  It prevailed only because its proponents were able to limit it to a core domain, in which its restriction on freedom is not widely felt and its usefulness is often highly valued: inventions with an %(q:industrial application).  Not only were abstract logical and mental activities excluded, but also agriculture, mining and other not strictly industrial areas.  Later it was clarified that a %(q:new teaching about the use of natural forces) had to be at the core."))

(filters ((ep ahv 'epue52)) (ML Iia "In der fertigenden Industrie arbeiten nur relativ wenige Gewerbetreibende, deren Produktion mit hohem organisatorischen Aufwand und großen Ausrüstungsinvestitionen verbunden ist.  Diese Industriebetriebe halten ihr Wissen geheim, um die Früchte ihrer Entwicklungsarbeit vor der Ausbeutung durch weniger innovative Konkurrenten zu schützen.  Geheimhaltung aber behindert den Fortschritt.  Hier bietet das Patentwesen einen Ausweg:  man gewährt den Erfindern Vorrechte als Belohnung dafür, dass sie ihr Wissen veröffentlichen." (en "In the manufacturing industry, there are usually only a handful of companies, whose production involves high organisational efforts and equipment costs.  Industrial companies keep their knowledge secret, in order to protect themselves against the depletion of their funds by copycat companies, who need less research and development.  But, since secrecy is not good for overall social progress, the patent system strikes an interesting deal: the inventive company is granted a temporary monopoly in return for publishing its knowldege.")))

(filters ((ss ah "http://www.ssh.fi/sshprotocols2/index.html")) (let ((AO (kaj (ah "http://lpf.ai.mit.edu/Patents/testimony/statements/adobe.testimony.html" "Adobe") (ah "http://lpf.ai.mit.edu/Patents/testimony/statements/oracle.statement.html" "Oracle")))) (ML OeW "Ob das Patentwesen in seinen traditionellen Grenzen sich bewährt hat, ist umstritten.  Die Front der Ablehnung ist jedoch gewichen, und die Patentjuristen haben freie Hand, um die ursprünglichen Grenzen aufzuweichen und bis zur Unkenntlichkeit aufzulösen.  Dies hat schon heute in den USA zu einer Flut von Prozessen um triviale Internet-Techniken geführt.  Selbst Branchenriesen wie %{AO} haben geäußert, dass Patente mehr Schaden als Nutzen bringen.  Wichtige Softwareprojekte wie %(ss:Secure Shell) mussten in Europa Zuflucht suchen." (en "It is up to the present day not clear, whether the patent system in its traditional limits has done more good than bad.  But, as the system is well established, the original wave of skepticism has faded, and the people within the system now have the resources and leverage to further expand the system, dissolving the borders that originally made it acceptable.  This has already induced a flood of ligitation about trivial Internet techniques in the US.  Even large innovation leaders like %{AO} have stated, that patents are more harmful than useful to them.  Important Software projects like %(ss:Secure Shell) were forced to seek refuge in Europe."))))

(filters ((un ah "http://www.undp.org/hdro/index2.html") (rm ant (ML cnu "%(q:The relentless march of intellectual property must be stopped and questioned.)"))) (ML Dnc "Dennoch hält die EU-Kommission (für den Binnenmarkt zuständige Generaldirektion 15) im Zusammenspiel mit den Europäischen Patentorganisationen (Aufsichtsrat des Europäischen Patentamtes) unbeirrt und ohne auch nur den Versuch einer ökonomischen Begründung an dem fest, was eine %(un:UNO-Studie) als %(rm:unermüdlichen Marsch des geistigen Eigentums) anprangert." (en "Nonetheless the EU Commission's patent politicians at the DG 15 and the board of directors of the European Patent Office, the %(i:European Patent Organisations), adhere to what a %(un:UNDP study) called the %(q:relentless march of intellectual property rights), without even trying to offer an economic rationale")))

)

(mlhtdoc 'swpatfrati (ML Waa "Was kann man tun?" (en "What can we do?")) nil nil
(ML Ieu "In Deutschland ist die Bundesregierung gefordert, den Kompetenzüberschreitungen der Patentämter Grenzen zu setzen.  Das EPÜ ist ein Vertrag zwischen Staaten.  Nur die Staaten haben das Recht, gegen Verletzungen des Vertrages vorzugehen.  Auf dem Wege der Klage bei einem übergeordneten Gericht ist dem EPA nicht beizukommen." (en "The governments of the European countries are called for to take up their responsibility for legislation on such grave public issues as the scope of patentability.  The EPC is a treaty among European states (including non-EU states).  Only the governments of these states have the right to act, when the patent offices violate the letter or the spirit of these treaties."))

(filters ((pm ahst 'swpbmwi25) (pm ahst 'swpbpos25) (dc ahst 'swpdokucd)) (ML Irw "Am 18. Mai 2000 fand im Bundeswirtschaftsministerium eine %(pm:Konferenz über wirtschaftspolitischen Aspekte der Softwarepatentierung) statt, für die wir ein %(pp:Positionspapier) und eine %(dc:Dokumentations-CD) erarbeiten." "This may the German Ministery of Economics convened a %(pm:conference) for which we prepared a %(pp:statement) and a %(dc:documentation CD)."))

(filters ((np ahst 'nopatltr)) (ML EsW "Es verbleibt jetzt nur noch kurze Zeit, um die öffentliche Diskussion weiter voranzutreiben.  Wir bereiten hierfür in diesen Tagen eine %(np:Briefaktion) vor, die alle Entscheider erreichen soll." "Only little time is left to give this public discussion a further boost.  We are %(np:preparing a letter action to reach all decisionmakers)."))
)


(mlhtdoc 'swpatfarit
 (ML Frt "Frühere Aktionen" "Earlier Activities")
 nil nil

(filters ((gs ahv 'greenpstud)) (ML GRs "Greenpeace wirft in einer %(gs:Studie) dem EPA Rechtsbeugung vor und hat eine Stellungnahme des Bundesjustizministeriums gegen das EPA erwirkt." (en "Greenpeace has published a %(gs:Study), in which it accuses the EPO of breaching the law, and they have induced the German Ministry of Justice to criticize the EPO for this.")))

(filters ((sp ahst 'swpatsarji) (sa ahs 'swpatgirzu) (ea ahst 'eulux) (ei ahst 'euipCA)) (ML FiW "Im Sommer 1999 haben wir mit Unterstützung %(sp:einiger Firmen) eine %(sa:Arbeitsgruppe) gebildet, die bereits im Rahmen der %(ea:EuroLinux-Allianz) ein %(ei:Treffen mit der Brüsseler Generaldirektion 15) abgehalten hat." (en "Moreover, with the support of %(sp:a few companies), we have formed a %(sa:workgroup) that has already, in the framework of the %(ea:EuroLinux Alliance), had a %(ei:meeting with the legislators in Brussels).")))

(let ((FF (ahs 'ffii))) (filters ((ml ahst 'miertltr) (nb ahs 'nopatltr)) (ML Dee "Der %{FF} schickte Ende Mai 1999 einen %(ml:Brief an EU-Wettbewerbskommissar Karel van Miert), um auf die Gefahren von Patenten für die Wettbewerbsfreiheit in dem ohnehin chronisch monopolkranken Bereich der Software hingewiesen.  Derzeit ist ein %(nb:neue Briefaktion) in Arbeit." "The %{FF} has, at the end of May, published and sent a  %(ml:letter to the EU competition commissioner Karel van Miert) and pointed out the dangers of patents for the freedom of competition in the already now monopoly-ridden area of software.  Currently a %(nb:new letter action) is under preparation.")))

(let ((EL (ahst 'eurolinux (ML Ert "Eurolinux Bündnis für eine freie informationelle Infrastruktur" (en "Eurolinux Alliance for a Free Informational Infrastructure")))) (KG (ahs 'kongress))) (filters ((js ah "http://www.smets.com/partners/jp/") (fp ah "http://www.freepatents.org")) (ML Fgr "Ferner gewannen wir %(js:Jean-Paul Smets), der in Frankreich durch %(fp:gründliche Dokumentationsarbeit) in Parlament und Regierung schon eine breite Opposition gegen die EU-Pläne mobilisiert hat, als Redner für unseren %{KG} am 13. Juni 1999 in Köln, auf dem wir gemeinsam die Gründung des %{EL} ankündigten." (en "Moreover we organised a %{KG} in Cologne, where %(js:Jean-Paul Smets), who had already done %(fp:thourough documenation and lobbying work) in France, gave a lecture."))))

(ML IbW "Im Vorfeld der Konferenz der Europäischen Patentorganisationen am 24. Juni 1999 in Paris haben wir ferner einen Brief an die beiden vom Bundesjustizministerium entsandten deutschen Delegierten geschrieben, der inzwischen beantwortet wurde.  Eine weitere Anfrage, in der wir um Herstellung von diversen Kontakten zu Verantwortlichen bitten, blieb bisher unbeantwortet." (en "Before the Conference of the European Patent Organisations of 1999-06-24 in Paris we sent a letter to the two delegates of the German Ministery of Justice, which was answered."))
)

(mlhtdoc 'swpatdamba (ML Vee "Vorbeugung gegen kommende Patenklagen" "Preemption of upcoming patent lawsuits") 
(ML Utn "Unabhängig davon, ob es uns diesmal gelingt, die Patentlobby in ihre Schranken zu weisen, müssen wir uns langfristig mit dem Patentwesen auseinandersetzen." (en "Regardless of whether we will succeed in containing the expansionism of the patent lobby, we have to become more aware of the patent systems and take measures to deal with it."))
nil 
(sects

(neu (ML Ntr "Neuheit widerlegen" (en "Refuting Novelty"))

(filters ((pa ahs 'swpatpurci)) (ML Wtl "Wir haben bereits begonnen, den %(pa:Stand der Technik bei allen auf FFII.ORG beherbergten Projekten täglich mit Zeitstempel zu dokumentieren).  Diese Dokumentation wird ein mal im Monat in einer öffentlichen Bibliothek archiviert.  Das System wird derzeit probeweise auch auf andere Server ausgeweitet." (en "FFII has already started to %(pa:document the state of the art of all FFII.ORG projects).  This documentation is archived in a public library with a time stamp once a month.")))

(ML GnW "Gegen frivole Patentklagen hilft letztlich nur der Nachweis, dass %(q:diese Technik schon dato 19xx-xx-xx bekannt und in Entgegenhaltung Dx dokumentiert) war.  Nur durch Widerlegung der Neuheit kann man ein Patent zu Fall bringen.  Andere Kriterien wie %(q:Erfindungshöhe) sind inzwischen durch die Praxis der Patentämter zur Makulatur geworden." (en "For defense against frivolous patent attacks, the only real help is the proof that %(q:this technology was already documented in 19xx-xx-xx as described in cited document Dx).  Refuting novelty is the only way to beat a patent.  Other criteria such as %(q:inventivity) / %(q:inventive step) have been softened to near-uselesness by the patent offices."))

)

(bund (ML Bns "Branchenvereinbarungen, Schutzbündnisse, Versicherungen" (en "Multilateral Company Agreements, Mutual Defense Alliances, Assurances"))

(filters ((ko ahs 'swpkoop)) (ML DWv "Die Softwarebranche ist nicht gezwungen, sich unsinnigen bürokratischen Praktiken zu unterwerfen.  Mit einem Minimum an gutem Willen kann sie sich %(ko:verbünden), um ihre eigenen %(q:Abkommen für den fairen Gebrauch von Patenten) zu schließen und dabei einen erträglichen Ausgleich zwischen den Interessen der Patentinhaber und denen der informationellen Infrastruktur schaffen und durchzusetzen." (en "Software professionals are not forced by fate to submit to bureaucratic idiocy.  With a minimum of good will, they can forge %(ko:alliances), form %(q:fair use agreements) that achieve a (relatively) tolerable harmonisation between the interests of patent owners and those of the freedom of informational infrastructure.")))

(ML FWb "Ferner wäre es möglich, im Rahmen solcher Bündnisse eine relativ günstige Rechtsschutzversicherung für Programmierer freier Software aufzubauen.  Der Schutz vor frivolen Patentattacken würde dann professionalisiert.  Wir würden zum Gegenangriff übergehen und dafür sorgen, dass sich Streithansel zwei mal überlegen, ob sie ihr Vermögen und ihren Ruf riskieren möchten." (en "It would also be possible to build, within the framework of such alliances, affordable law protection insurances for programmers of free software.  The protection against patent claims would then be professionalised.  We could move to counter-attack and become a feared force that could make patent sharks think twice."))

(ML DtW "Das sind Vorstellungen, die bisher an der Unwissenheit und mangelnden Solidarität der Softwarebranche scheitern.  Sie sollten parallel, nicht alternativ zum Kampf gegen die missbräuchliche Ausweitung des Patentrechts umgesetzt werden." (en "These ideas have been proposed before, but they have failed due to a lack of good will and solidarity in the software industry.  They are not an alternative to fighting against the abusive extension of the patent system, only a possible remedy that should be pursued simultaneously."))

)

) )

(list 'swpatlijda-dir
(mlhtdoc 'swpatlijda nil nil nil
(filters ((sm ahsti 'swplxtg26 'siemens)) (ML Brn "Zahlreiche Unternehmen haben sich deutlich gegen Patentierbarkeit von Programmlogik ausgesprochen.  Dennoch berichtet die Presse immer wieder davon, die Software-Industrie wolle die Ausweitung des Patentschutzes.  %(sm:FFII-Gespräche mit Siemens-Vertretern) ergaben, dass dies zumindest für Siemens nicht gilt.  Zahlreiche Mitarbeiter und Bereichsleiter von Siemens sind über die Patentinflation beunruhigt, aber bisher hat die Patentabteilung in dieser Frage nach außen das Sagen.  Ein normaler Konzernpolitiker traut es sich nämlich nicht zu, zu einem schwierigen Thema wie dem Patentwesen öffentliche Aussagen zu treffen." "Numerous software companies have pronounced themselves against the patentability of software.  Nevertheless the press keeps reporting that %(q:the software industry demands patent protection).  %(sm:FFII talks to with Siemens representatives) showed that even Siemens, the only German company that has pronounced itself in favor of software patents, this view does not hold.  Most techniciancs and middle managment people at Siemens are concerned about possible from software patents to their company, but, but so far it is the patent department which has been representing Siemens to the outside.  And an average Siemens board member does not dare to take an independent stance on such a complex matter as patent law."))

(let ((AK "Arno Körber")) (filters ((zi ahs 'zvei) (zp ahs 'zvei-propat-gnn)) (ML DWk "Der einzige Wirtschaftsverband, der sich %(zp:öffentlich für die Patentexpansion ausgesprochen) hat, der ZVEI, übernahm dabei lediglich eine Vorlage seines Patentexperten, des Chefs der Patentabteilung von Siemens, %{AK}.  Dies ergaben damals telefonische Nachfragen des FFII.  Das ZVEI-Sekretariat war beunruhigt über unsere Kritik, der ZVEI habe gegen das Interesse der in ihm organisierten kleineren und mittleren Unternehmen der Elektronikindustrie gehandelt.  Diese Unternehmen profitieren vielleicht teilweise vom bisherigen Patentschutz für neuartige Apparate, aber von einer Ausweitung auf die Programmlogik haben die meisten nur schlimmstes zu befürchten." "The only industry association that ever made a %(zp:public statement in favor of patent expansion), the ZVEI, just used a draft that came from its patent expert, who is at the same time the head of the patent department of Siemens, %{AK}.  This is what we found out by telephonic inquiries at the time.")))

(filters ((bw (lambda (str) (tpe (ah "http://www.bmwi.bund.de" str) "BMWi"))) (bj (lambda (str) (tpe (ah "http://www.bmj.bund.de" str) "BMJ")))) (ML Oae "Ähnlich sieht es bei der deutschen Bundesregierung aus.  Im %(bw:Bundesministerium für Wirtschaft und Technologie) wird die Tendenz zur Programmlogik-Patentierung mit einiger Skepsis beobachtet.  Aber federführend ist in Patentfragen das %(bj:Bundesjustizministerium)." "The situation in the German Federal Government is the same.  Economists in the %(bw:Ministery of Economics) view the tendency to make programming concepts patentable very critically.  But it is the %(bj:Ministery of Justice) which has the say in patent questions."))

(ML SWn "Sowohl Siemens als auch der ZVEI als auch die Bundesregierung werden von Patentexperten schlecht beraten.  Patentexperten haben wenig Grund zur Loyalität gegenüber ihrer eigenen Organisation. Ihre Karriere hängt weniger von der eigenen Organisation als von einem weltweit verflochtenen %(q:proprietärisch-informationellen Komplex) ab.  Zu diesem Komplex gehören u.a.:"  "Siemens, ZVEI and the Federal Government are all led by patent experts, whose career depends much less on the company in which they are working than on their relations to the patent world.  The Proprietary Informational Complex forms a worldwide interwoven closed system with members such as:")

(ul
(ah "http://www.jpo-miti.jp/saikine/" (ML dee "die Trilaterale Kommission der Patentaemter" "Trilateral Commission"))
(let ((LST "WTO/GATT/TRIPS")) (ML der "die %{LST}-Verhandlungsrunden der Patentspezialisten" "%{LST} negotiation rounds of patent experts"))
(ahs 'epo (ML EPA "EPA"))
(linul
 (filters ((bm (lambda (str) (tpe (ah "http://www.bmj.bund.de" str) "BMJ")))) (ML Pmr "Das %(bm:Bundesministerium der Justiz) hat in seiner Abteilung für Industrie und Handel ein eigenes Patentreferat.  Zum Geschäftsbereich des BMJ gehören ferner das DPMA, das BPatG und der BGH.  Zum EPA bestehen innige personelle Verflechtungen." "Patent Department of the %(bm:German Ministery of Justice)"))
 (ahs 'bpatg (ML Bpg "Bundespatentgericht" "Federal Patent Office"))
 (ML Prg "Der Patentsenat (10. Senat) im Bundesgerichtshof urteilt in letzter Instanz über Fragen der Patentierbarkeit und verwirft dabei oft %(q:restriktive) Urteile des Bundespatentgerichtes.  Es ist ein Vorreiter der Patentinflation in Deutschland, hinkt aber dem EPA noch immer ein wenig hinterher." "Patent Senate of the German Federal High Court")
 (ah "http://www.dpma.de" (ML DPM "Deutsches Patent- und Markenamt" "German Patent and Trademark Office")) )
(lin 
 (ML PaW "Patentpraktikerverbaende" "Associations of Patent Experts")
(dl
 (l (ah "http://www.aippi.org" (tpe "AIPPI" (ml "Association Internationale des Professioneaux en Propriété Industrielles") ) ) (ML foa "forderte 1997 von Europa Softwarepatentschutz") )
 (l (ah "http://www.ficpi.org" (tpe "FICPI" "Fédération Internationale des Conseillers en Propriété Industrielle")) (filters ((vr ahs 'ficpi-vancres0006)) (ML fnd "fordert in einer neuen %(vr:Resolution) die weltweit einheitliche Durchsetzung von Patentansprüchen gegen die Veröffentlichung von Computerprogrammen, Ausweitung des Patentschutzes auf Geschäftsmethoden, Einführung einer Neuheitsschonfrist uvm.")))
 (l (ml "Union der Europäischen Berater für den Gewerblichen Rechtschutz") (ML fWA "forderte im Dez 1997 auf Konferenz im EPA Softwarepatentschutz"))
) )
(ML Puo "Patentabteilungen der Grossunternehmen" "Patent Departments of Large Enterprises")
(linul
 (tpe (ML MlW "MPI fuer Internationales Patent-, Urheber- und Wettbewerbsrecht" "Max-Planck Institute for International Patent Law, Copyright and Comptetition Law") (ML Vro "Vorreiter aller inflationären Forderungen des EPA, Herausgeber von GRUR und anderen Zeitschriften"))
 (ah "http://www.grur.de" (ML Gib "GRUR (der Gruene Verein) und andere Patentberater-Zeitschriften" "GRUR")) )
(ahs 'intprop (ML Agi "Abteilung für Geistiges Eigentum in der Binnenmarktdirektion der EU-Kommission"))
)
 
(let ((IK "Dr. Ingo Kober")) (ML DeW "Der derzeitige Präsident des Europäischen Patentamtes, %{IK}, war früher Staatssekretär im BMJ." "The current head of the European Patent Office, %{IK}, was state secretary in the BMJ before he acceeded to is EPO post."))

(let ((PM "Peter Mühlens")) (ML Dea "Der Vertreter Deutschlands, der im Juli 1999 zusammen mit anderen europäischen Fachkollegen dem Vorstand des Europäischen Patentamtes das Mandat zur Ausarbeitung eines Gesetzesänderungsvorschlags zur Legalisierung von Programmlogik-Patenten gab, %{PM}, wechselte kurz darauf zum EPA über." "The BMJ representative who in July 1999 together with European colleagues entrusted the European Patent Office to propose a law change so as to allow software patents, %{PM}, soon thereafter changed over to a new post at the EPO."))

(ML Odl "Ohne Programmlogik-Patente kann der Europäische Patentkomplex nicht am %(q:Wachstum der IT-Industrie) teilhaben.  Oder, aus der Sicht des Patentkomplexes formuliert:  die IT-Branche ist wirtschaftlich so bedeutend geworden, dass ihr nicht weiterhin die Segnungen des Patentschutzes versagt bleiben dürfen." "The Proprietary Informational Complex needs software patents to participate in the growth of the %(q:software industry).  Or, viewed from the perspective of the Complex:  the %(q:software industry) has become so important, that it can no longer be denied the blessings of patent protection.")

)

(mlhtdoc 'swpatibm
 (ML IgW "IBM Lobbying to Extend the IBM Tax") 
 (ML Iah "IBM's patent department is actively lobbying Europe to adopt the American patent system.  They have been using fallacious arguments and strongarm tactics to corrupt European patent jurisdiction and press for a change of law.  IBM has acquired approximately 1000 European software patents that currently cannot be enforced in Europe because they were granted against the law.  Once the European patent law is changed, an IBM tax of several hundered million EUR will soon be levied on European software companies.") 
 nil 

(ML Iop "IBM has for many years sponsored the Software Patents Institute, which failed to solve the problem of software prior art while consuming a lot of state and industry money.  In 1998, IBM prevented the American government from engaging in a thourough investigation of patent quality.")

(ML Aue "American patent law organisations like the AIPLA never invite the famous patent quality critic Gregory Aharonian to give lectures at their meetings.  This, according to some rumors, is due to pressures from IBM.")

(ahs 'bpatg17w6998 (ML Iom "In Germany, IBM is pressing a ridiculously broad and trivial spellcheck patent to the Federal High Court (BGH) in an attempt to make that court overrule the German patent law which states that computer programs (as such) are not patentable.  In 1997, when European Patent Office overruled the European Patent Convention and introduced the fishy notion of %(q:computer program product), which was later rejected by German courts, it was also IBM that pressed for this change and fought it through."))

(let ((GI (ah "http://www.gi-ev.de" (ML Gan "Gesellschaft für Informatik")))) (ML Msm "Moreover IBM has pulled the %{GI} on its side.  This association's president published a silly pro swpat press release and was echoed by the vice president, Andrea Grimm, who is an IBM manager.  Mrs Grimm lied publicly to the GI meeting when she claimed that IBM is against trivial software patents and that software patents can harmoniously coexist with opensource software."))

(ML Iei "IBM patent department representatives gave strong statements in favor of software patentability at several meetings at EU and national levels.  In private meetings with government officials they pressure the government to push the European patent legislation in an expansive direction and announce that IBM will make its investments in a specific country dependent on that country's government's favorable behavior.")

(ML Brr "Business Week had an article about how IBM uses its patents to press money out of the American software industry.  This amount is enough to build many IBM business centers throughout Europe.  The question is only, whether Europe should accept the IBM tax or not rather do something on its own to foster the development of software.")

(blockquote (al
"BIG BLUE IS OUT TO COLLAR SOFTWARE SCOFFLAWS"

"Business Week: March 17, 1997"

"..."

"Big Blue holds more software patents than any other company in the world. That's great for bragging rights, but it does little for the bottom line. Now, however, IBM sees money in that trove of intellectual property--and its efforts to collect are making other software companies hopping mad. Lawyers for Big Blue are searching for software companies that it says should be paying royalties but aren't. Over the past several months, IBM has been quietly pursuing patent claims against such well-known software companies as Oracle, Computer Associates, Adobe Systems, Autodesk, Intuit, and Informix. IBM is also pressing a software claim against computer maker Sequent Computer Systems Inc."

"..."

(ML Nsk "Now, Reback is hurling charges against IBM similar to those he leveled at Microsoft. %(q:IBM shows up the same way someone might demand protection money,) he says. Officials at the companies confirm that IBM has contacted them, but most refuse to talk publicly. Collecting the patent royalties could add millions to IBM's net profits. In 1995--the last year IBM released figures--the company took in $650 million from royalties on all patents, software and hardware alike. Insiders say that senior managers believe IBM could collect $1 billion a year from its patents.")

(ML Wno "When IBM strikes a royalty agreement, it collects 1% to 5% of the retail price of a product using the covered technology. That's a sliding scale depending on the number of patents involved. If IBM can collect royalties from those companies using its approximately 2,500 U.S. software patents, it could reap almost as much from software as the $200 million in royalties it gets from PC makers. Reback says he was told IBM asked one software maker to pay $30 million to $40 million a year.")

(ML Ino "Indeed, some folks in the computer and software businesses fear that the whole industry could wind up paying a 1% to 5% tax to IBM. %(q:It's hard to be in the computing business--hardware or software--and not infringe on a couple of dozen IBM patents, if not more,) says Greg Aharonian, a patent consultant. Meanwhile, other technology companies are following IBM's lead. Says Richard A. McGinn, president of Lucent Technologies Inc.: %(q:We've seen IBM become much more aggressive, and we are, too.)")

(ML Iee "IBM has patented everything from the software to automatically return the cursor to the start of the next line on a computer screen to a state-of-the-art virus-detection program. So far, it has made claims against companies by invoking patents including a spell-check function and techniques for how a database program handles queries and runs on so-called parallel-processing computers.")
(ML Sge "Software companies aren't eager to settle. Intuit Inc., for one, rejected a patent license deal that IBM offered. Some companies are afraid that paying now will set a precedent, making it harder to say no later. %(q:If we sign up with IBM today, then what happens in three or five years, when the patent agreement expires?) asks Oracle Corp. patent attorney Allen Wagner. With all the skirmishing that lies ahead, this dispute is still in Version 1.0.")
))

(ML AWe "Although IBM earns substantial revenues from (mostly trivial) patents, it is not sure that unlimited patentability is in the best interest even of IBM.  Patent managment generates costs that were not considered in the Business Week article.  IBM could perhaps be even more profitable without patent bureaucracy.  Building the business strategy on patents is strangely incoherent with IBM's strong support for opensource software.  Maybe IBM's patent department has gone out of control.")

)

(mlhtdoc 'swpatsiemens nil (ML SnW "Siemens has always been at the forefront of the patent movement.  Yet, even those Siemens patent cadres who most actively promote software patentability do not try to argue that this is beneficial, either for Siemens or for the world.  They merely say that Europe has to follow the lead of the US, which sets the standards in the global market.  Meanwhile, many Siemens managers in IT and telecommunications believe that patents are doing great harm to their business.") nil
(apply 'linklist (mapcar 'dokdesc '(boch97-koerber swpattisna)))
) 
; swpatlijda-dir
)

(mlhtdoc 'swpkoop nil (ML Asg "Auch in Ländern mit Softwarepatenten ist theoretisch möglich, das Patentwesen zugunsten der freien Software zu benutzen und seine schlimmsten Wirkungen dadurch abzumildern.  Dazu bedürfte es sehr teurer Spezialistenarbeit.") nil (ul
(MLL Aig "Auswertung und systematische, leicht verständliche Darstellung der bisherigen Software-Patente, sofern für freie Software von Bedeutung.  Anmeldung von Einsprüchen / Nichtigkeitsklagen gegen besonders gefährliche Patente.")
(MLL Anu "Auswertung der freien Software auf patentierbare Erfindungen.  Anmeldung dieser Erfindungen im Namen des FFII (oder einer sonstigen Patentkooperative).  So kann FFII Faustpfänder sammeln und zu einem Mitspieler im Monopoly-Spiel der großen Firmen werden.")
(MLL PlF "Patentrechtliche Verfolgung von Firmen, die FFII-Patente verletzen.")
(MLL Ale "Abschluss von Lizenzverträgen mit Firmen mit dem Ziel, diese Firmen dazu zu bewegen, ihre eigenen Patente für die Verwendung in freier Software freizugeben.")
(MLL Btn "Bildung einer Genossenschaft, deren Mitglieder ihre Patente einbringen und allen anderen Genossenschaftsmitgliedern erlauben, diese Patente in freier Software zu verwenden.")
(MLL AWs "Aufklärungsarbeit für ein freier Software angemessenes Patentrecht.  Die meisten Patentspezialisten ahnen noch gar nicht, dass Industriekapitalismus nicht die einzige Triebkraft des menschlichen Erfindungsgeistes ist.")
))

(mlhtdoc 'swpatpante (ML KEk "Klagemöglichkeiten")
(ML DGW "Die Sachwalter des Patentwesens gewähren Patente auf informatische Ideen und Computerprogramme, obwohl die geltenden Gesetze dies unmissverständlich verbieten.  Bisher herrscht unter den stark verfilzten Institutionen dieses Bereiches ein Konsens des Wegschaüns.  Um das zu ändern, könnte man klagen.  Ein anonymer Patentjurist gibt hierzu guten Rat.")
nil

(ML DWm "Der ehemalige Präsident des DPMA, Norbert Haugg, wird gelegentlich mit den Worten zitiert (sinngemäß):")

(blockquote
 (ML DhW "Das Deutsche Patent- und Markenamt handelt nach Recht und Gesetz. Wird in einer gerichtlichen Überprüfung festgestellt, daß einzelne Handlungen nicht rechtmäßig sind, so werden die erforderlichen Schritte unternommen werden. Der Weg zu den Gerichten steht jedermann offen."))

(ML Dbd "Dieser Weg wurde u.a. schon einmal beschritten, um eine Herauslösung des BPatG aus dem DPMA zu erstreiten.")

(ML Ejr "Ein Patentjurist fordert uns nun auf, in eben diesem Sinne etwas zu unternehmen, und gibt dabei folgenden Rat:")

(blockquote (al
(ML SrW "Sprechen Sie ein! Klagen Sie. Nur so läßt sich an der Praxis von Verwaltungsbehörden etwas ändern.")

(ul
(ML StW "Sie beklagen meiner Ansicht nach zurecht, dass der BGH die Patentierung von %(q:Programmen mit technischem Charakter) zuläßt, obwohl im §1 BPatG Programme für Datenverarbeitungsanlagen explizit nicht als Erfindungen angesehen werden. Der Zusatz %(q:als solche) im Absatz 3 ist bestenfalls dahingehend zu beachten, dass die erfinderischen Schritte zur Problemlösung wenigstens zum Teil keine Programmnatur besitzen. Von %(q:technische Programme) ist dem Gesetzestext nicht zu entnehmen.")
(al (ML Dho "Demzufolge wären die wegweisenden Beschlüsse %(q:Dispositionsprogramm) bis %(q:Logikverifikation) Fehlurteile. Wenn sich nachweisen ließe, dass der BGH hier quasi in %(q:vorauseilendem Gehorsam) gesetzgeberisch tätig war, wäre dies ein klarer Verstoß gegen die verfassungsmäßige Gewaltenteilung.  Dies kann aber nur durch Klage beim Bundesverfassungsgericht geklärt werden.")
    (ML KgW "Klagen Sie!") )
(al
  (ML Waa "Wehren Sie sich dagegen, dass bei programmbezogenen Patenten praktisch nie die Programmtexte in Form von Quellcode (in welcher Form auch immer) angegeben werden. Ich kann mir nicht vorstellen, dass jegliche Form von Programmentwicklung lediglich naheliegende Schritte beinhaltet. Sprechen Sie ein nach §1, weil die Erfindung nicht wiederholbar ist, ohne dass der Fachmann selbst erfinderisch tätig sein müsste, bzw. lassen Sie sich anhand von nachgereichtem Quellcode nachweisen, dass die Programmierung insgesamt nur naheliegende Schritte umfasst. Wenn das Gegenargument kommt dass aber %(q:Programme als solche) nicht als Erfindung angesehen werden, so nehmen Sie das als Beweis, dass es sich um ein Programm gemäß §1 (2) 3. BPatG handelt.")
  (ML Dcr "Der nachgereichte Quellcode sollte so vollständig sein, daß er dem Fachmann erlaubt, ein ausführbares Programm, das die Aufgabe löst, zu erzeugen. Bestehen Sie darauf und erzwingen Sie ein wegweisendes Urteil.") )
(ML BeW "Bestehen Sie bei Ihren Einsprüchen und Klagen, dass Ihre Definitionen von %(q:Programmen für Datenverarbeitungsanlagen), %(q:wissenschaftliche Theorien), %(q:mathematischen Methoden), %(q:geschäftliche Tätigkeiten) und besonders Ihre Definition von Technik berücksichtigt werden, da Sie die Fachleute auf diesen %(q:technischen) Gebieten sind und sich folglich die Rechtsprechung daran zu orientieren hat, wie der Fachmann die ihn betreffenden Gesetzestexte zu interpretieren hat. Es kann ja nicht angehen, dass ein Jurist definieren kann, was ein Informatiker unter einem %(q:Programm für Datenverarbeitungsanlagen) zu verstehen hat.")
(ML Bnm "Bestehen Sie darauf, dass bei programmbezogenen Erfindungen einzig und allein Programme beansprucht werden können. Die üblichen %(q:Verfahren zum) geben üblicherweise nur eine Aufgabenstellung und damit nicht eine %(q:Erfindung) (als Lösung der Aufgabe) an, wie es im §1 gefordert wird. Erst das Programm, ausgeführt auf einer Datenverarbeitungsanlage stellt die Lösung der Aufgabe dar. Somit kann bestenfalls das Programm als solches beansprucht werden!")
(ML SWr "Sprechen Sie ein, so oft es Ihnen möglich und gerechtfertigt erscheint, denn die Arbeitsqualität der Prüfer wird maßgeblich danach beurteilt, wieviele Anmeldungen er rechtskräftig erledigt. Ob ein Verfahren nach Recht und Gesetz abgeschlossen wurde, kann nur von den Gerichten geklärt werden. Wenn niemand einspricht und klagt, ist der Weg des geringsten Widerstandes zu einer guten Beurteilung zu kommen, der der Erteilung.")
(ML BWg "Bestehen Sie bei Ihren Klagen darauf, dass zuerst nach dem Wortlaut des Gesetzes und erst in Zweifelsfällen nach der derzeitigen Rechtsprechung verhandelt wird (also nach %(q:Gesetz und Recht) und nicht umgekehrt).")
(ML Bdn "Bestehen Sie auf der Anwendung des Deutschen Patentgesetzes, falls das EPÜ verabschiedet wird und bestehen Sie darauf, dass jedwede Änderung des §1 BPatG nicht rückwirkend oder gar vorauseilend angewandt wird, außer der Gesetzgeber (das Parlament, nicht der BGH) beschließt eine rückwirkende Gültigkeit. Falls interessierte Kreise in Deutschland eine Einführung der Todesstrafe fordern, wird ja auch niemand auf die Idee kommen, schon mal Urteile mit Todesstrafe zu verhängen und zu vollziehen, ohne dass das Gesetz überhaupt verabschiedet wurde und vom Zeitraum anwendbar ist.")
(ML Bmi "Bestehen Sie darauf, dass für Programme in welcher Form auch immer das Urheberrecht (Copyright) anzuwenden ist, bei dem solche Probleme wie der Kopierschutz schon längst gelöst sind. Dann ergeben sich auch nicht die juristischen Schwierigkeiten mit Datenträgern, wie sie jüngst in der Entscheidung 17W(pat)69/98 der Anmeldung DE4323241 aufgegriffen wurden.") 
(ML FiW "Fragen Sie doch mal im BMJ nach, ob jetzt für das neue EPÜ endlich beabsichtigt ist, auch Informatiker am DPMA als Prüfer einzustellen. Ich habe noch die Auskunft erhalten, dass Informatiker kein technischer Beruf sei!")
) ) )

(filters ((up ahs 'unpoison)) (ML MnW "Man könnte einerseits über den Instanzenweg BPatG - BGH eine Nichtigkeitsklage gegen ein exemplarisches Softwarepatent wie z.B. das %(up:Netzvergiftungspatent von 7val) anstrengen.")) 

(ML Ato "Andererseits könnte man beim BVerfG wegen gesetzwidriger Rechtssprechung in den Fällen %(q:Logikverifikation) und %(q:Sprachanalyse) klagen.")

(filters ((ep ah "http://europa.eu.int/eur-lex/en/com/pdf/2000/com2000_0199en01.pdf"))  
(ML Zrt "Zudem wäre eine Klage beim BVerfG denkbar gegen die %(ep:Bestrebungen der EU-Kommission), den Instanzenzug BPatG - BGH abzuschaffen, die Gewaltenteilung im Patentwesen aufzuheben und alle Macht beim EPA zu konzentrieren."))

(filters ((bv ahs 'epo-basprop0008) (pa ah "http://www.patentanwalt.de")) (ML AWW "Auch der %(bv:Basisvorschlag des EPA) läuft auf eine Entmachtung der nationalen Parlamente und Gerichte und Konzentration aller Macht beim EPA hinaus.  Das hat selbst die %(pa:Deutsche Patentanwaltskammer) in einem Gutachten bemängelt -- Sie begrüßt zwar die grenzenlose Patentierbarkeit, mahnt aber ansonsten die Entmachtung der Parlamente und den Bruch der deutschen Verfassung an."))

(ML AWe "Allerdings ist es schwer für Bürger und Vereine, beim BVerfG zu klagen.  Das BVerfG lässt nur Verfassungsbeschwerden zu, bei denen es um Grundrechtsverletzungen geht, und es beurteilt die Fälle nur insoweit als es um solche geht.")
)

(mlhtdoc 'epue52 nil nil nil
(sects
(bas (ML Lgn "Leitgedanken" (en "Guiding Thoughts")) 

(filters ((mv ant (filters ((lm ahs 'lamy98) (tf ahs 'swpattrips)) (ML ied "insbesondere den %(lm:gewollten Missverständnissen) sowie dem %(tf:TRIPS-Scheinargument)" (en "especially %(lm:deliberate misunderstandings) such as the %(tf:TRIPS fallacy)")))))
(ML Sle "Wir schlagen vor, der Liste der Einschränkungen ein explizites theoretisches Fundament zu geben, um allen %(mv:Missverständnissen) jegliche Grundlage zu entziehen.  Eine solche Explizierung des Art. 52 könnte durch folgende Gedanken gekennzeichnet sein:" (en "We propose to fortify the list of exceptions in Art 52 with an explicit theoretical grounding, in order to remove the basis for any possible %(mv:misunderstandings).  Such an explication of art 52 could be characterised by the following thoughts:")))

(sects
(tech (ML Thz "Technizität" (en "Technicity"))
  (ML Dgw "Der Begriff %(q:technisch) wird im Sinne der klassischen Definition des Bundesgerichtshofs (BGH) erklärt:  %(e:planmäßiges Handeln unter Einsatz beherrschbarer Naturkräfte zur Erreichung eines kausal übersehbaren Erfolges, der ohne Zwischenschaltung menschlicher Verstandestätigkeit die unmittelbare Folge beherrschbarer Naturkräfte ist).")
  (ML cWi "Innovative %(e:Computerprogramme) sind immaterielle Informationsgebilde, die für sich genommen nichts bewirken und folglich niemals technisch sein können.")
  (ML ctj "Innovative %(e:Computerprozesse) sind zwar technischer Natur, aber ihr innovativer Kern liegt meistens im untechnischen Bereich des Rechnens mit gedanklichen Modellen (BGH-Kerntheorie).")
  (tan
   (ML EWt "Ein innovativer %(e:chemischer Prozess) ist technisch, das Computerprogramm, welches ihn vielleicht steuert, jedoch nicht.  Eine auf neuartigen Apparaturen und/oder neuer empirischer Erkenntnis über Naturkräfte beruhende Problemlösung ist patentierbar, aber die Veröffentlichung eines Programms zu ihrer Steuerung stellt weder eine unmittelbare noch eine mittelbare Patentverletzung dar.")
   (ML Hnn "Hier liegt manchmal eine schwierige Grenze, s. BGH-Urteile %(q:Antiblockiersystem) (1980) und %(q:Flugkostenminimierung) (1981).  Der Antiblockiersystem-Patentantrag wurde 1976 vom BPatG abgelehnt, weil die Erfindung im Bereich der programmierten Steuerung lag, aber 1980 vom BGH mit einem verengten Anspruchsbereich für zulässig erklärt, weil sie einen unmittelbaren Einsatz von Naturkräften zum Gegenstand hatte.  Im Fall %(q:Flugkostenminimierung) ging es zwar auch um einen Einsatz von Naturkräften, aber der BGH wies den Antrag zurück, da die Erfindungsleistung im Bereich des Rechnens mit bekannten Größen lag.  Diese Differenzierung ist weiter zu verfeinern.") )
 ) 

(indu (ML Isa "Industrialität" (en "Industriality")) 
  (tan 
   (ML Daa "Der Begriff %(q:gewerblich) (engl. industrial, frz. industriel, jap./chin. 産業 produzierendes Gewerbe) wird ebenfalls präzisiert, und zwar im Sinne des traditionellen Verständnisses von %(q:Industrie = gewerbliche Gütererzeugung). Dieser Begriff ist mit dem klassischen Technikbegriff fast identisch.  Er fordert, dass es um automatisierte Prozesse zur Fertigung materieller Güter unter unmittelbarem Einsatz von Naturkräften gehen muss." (en "The term %(q:industrial) is also clarified.  Industry is to be understood in the traditional sense as %(q:processing of material goods) and not in the sense of %(q:commerce) or %(q:trade).  This concept is almost identical to the classical concept of technology.  It requires automatable processes for the production of material goods by manipulation of natural forces."))
   (lin 
    (ML Lxg "Vgl die Motive zum Bundesrats-Entwurf eines Patentgesetzes, Reichstagsdrucksache, 3. Legislatur-Periode, 1. Session 1877, Nr. 8:")
    (blockquote (ML Lxg2 "Der Entwurf beschränkt die Patentfähigkeit auf solche Erfindungen, welche eine gewerbliche Verwertung gestatten.  Eine derartige Verwendung kann bestehen in der gewerbsmäßigen Herstellung des erfundenen Gegenstandes oder in seinem Gebrauch innerhalb eines gewerblichen Betriebes.  Auf diese Weise sind rein wissenschaftliche Entdeckungen, die Auffindung unbekannter Naturprodukte, die Entdeckung unbekannter Produktivkräfte, die Aufstellung neuer Methoden des Ackerbaues oder Bergbaues usw, die Kombination neuer Pläne für Unternehmungen auf dem Gebiete des Handels von dem Patentschutze ausgenommen.  Der Entwurf folgt in dieser Hinsicht den in allen Staaten ausdrücklich oder durch die Praxis anerkannten Grundsätzen.")) )
   )

  (ML Doa "Damit ist folgendem Szenario ein Riegel vorzuschieben, welches der Software-Referent der Union der Europäischen Patentberater, Patentanwalt Jürgen Betten, in einem Rundschreiben an seine Mandanten ausmalt:" (en "Thus the following scenario is to be prevented, which the software referent of the Union of European Patent Consultants, p.a. Jürgen Betten, depicts in a circular to his customers:"))

  (blockquote
   (ML DDo "Durch die ... Entwicklung der Rechtsprechung ... hat sich das Patentrecht von der traditionellen Beschränkung auf die verarbeitende Industrie gelöst und ist heute auch für Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu befürchten, dass die deutschen Dienstleistungsunternehmen in diesen Sektoren insbesondere gegenüber der US-amerikanischen Konkurrenz ins Hintertreffen geraten." (en "By means of the ... development of jurisdiction ... the patent system has detached itself from its traditional restriction to the processing industry and is now of essential importance also for service companies in the fields of commerce, banking, insurances, telecommunication etc.  Without building a suitable patent portfolio, it is to be feared that the German service companies in these sectors will find themselves in a disadvantaged position vis-a-vis their US competitors.")) )

  (lin 
   (ML Dea "Dies könnte bedeuteten, dass das Wort %(q:gewerblich) in der deutschen Fassung zu %(q:industriell) geändert wird." (en "This could mean that the German word %(q:gewerblich), on whose ambiguity the EPO has built much of its %(q:development of jurisdiction), is replaced by %(q:industriell), which is the closest equivalent to the English and French version of the text."))
   (ML F2i "Ferner sollte der Ausschluss therapeutischer und chirurgischer Verfahren in Art. 52 verbleiben und nicht nach Art. 53 transferiert werden, damit klar bleibt, dass diesem Ausschluss die Forderung nach %(q:industrieller Anwendbarkeit) zugrunde liegt.  Analog zur ärztlichen Therapiefreiheit ist auch die Freiheit der Handwerker, Bauern und diverser freiberuflicher Dienstleister bei der Ausübung ihrer Tätigkeit explizit vor dem Zugriff des Patentwesens zu schützen.  Das Steuerrecht ermutigt Programmierer, als Freiberufler %(q:nicht gewerblich) zu arbeiten, und die meisten von ihnen gehen heute tatsächlich einer nominell oder de facto freiberuflichen Tätigkeit nach.") ) )
 
(inve (ML Eiu2 "Erfindungsbegriff" (en "Inventivity"))

 (filters ((gk ahs 'grur-kolle77) (pi ahs 'smets-stimuler)) (ML SWe "Schließlich ließe sich auch der Erfindungsbegriff im Lichte der obigen Überlegungen sinnvoll definieren.  Der Begriff lässt sich am besten von mehreren Seiten gleichzeitig beleuchten.  Die vom EPA erarbeitete zirkuläre Definition %(q:technischen Lösung eines technischen Problems) ist ebenso treffend wie die Rote-Taube-Formel des BGH und die %(gk:1977 von Gert Kolle herausgeschälte) ballastfreie Version dieser Formel, bei der es nur noch auf den Einsatz von Naturkräften und das Unmittelbarkeitsprinzip ankommt.  Ferner ist die u.a. %(pi:von einer französischen Studie vorgeschlagene) Trennung der Erfindung in Problem, immaterielle Anweisung (Algorithmus) und materielle Basis erhellend.  Das Problem darf nicht patentiert werden, auch dann nicht, wenn seine richtige Formulierung beträchtliche kreative Intelligenz erfordert.  Ähnliches gilt für den Algorithmus.  Die Erfindungsleistung muss im Bereich der materiellen Basis liegen.  Diese besteht aus %(q:Ausrüstung und Wissen), was wiederum den Kreis zur %(q:Technik-Definition der Vereinten Nationen) schließt.  Es bleibt zu diskutieren, ob sowohl eine neue Ausrüstungsbasis als auch eine neue Wissensbasis oder nur eines von beiden gefordert werden soll.  Indem man eine neue Ausrüstungsbasis fordert, kann man Antiblockiersystem-Patente ausschließen, bei denen die Neuerung in der Gebrauchsanweisung oder dem Steuerungsprogramm liegt, welche beide nicht als solche beansprucht werden können, so dass die praktische Durchsetzbarkeit solcher Patente ohnehin fraglich ist.  Indem man eine neue Wissensbasis fordert, stellt man wiederum sicher, dass die Problemlösung eine erfinderische Tätigkeit erfordert und nicht etwa mithilfe künstlicher Intelligenz durch Simulation ermittelt werden kann." (en "Finally, the concept of %(e:invention) can also be defined meaningfully in the light of the above considerations.  The EPO's circular definition %(q:technical solution for a technical problem) is as valid as the German Federal High Court's Formula %(gk:reduced by Gert Kolle in 1977) to the salient features of (a) use of natural forces and (b) direct causal relation.  Moreover a %(pi:French study) has proposed a separation of the invention into a material problem, an immaterial instruction (algorithm) and a material base.  Since neither problems nor algorithms should be patentable, the material base needs to be new, and it should contain at least some new knowledge about natural forces or some new equipment.  It is to be discussed whether only a new equipment base or only a new knowledge base or either one or both should be required.  By requiring a new equipment base, one can exclude the patenting of an anti-blocking process based on a new usage instruction or a new program, neither of which can be patented as such.  By requiring a new knowledge base, one can exclude all those innovations that could have been found by a simulation program based on a problem specification, a knowledge base and a system of artificial intelligence."))) )
)
;bas 
)

(nov (ML Ues "Unser Novellierungsvorschlag" (en "Our Revision Proposal"))

(artiquote
(colons (ML Ail "Artikel 52" (de "Artikel 52")) (ML Eiu "Erfindungen" (en "Inventions")))
(ol
(flet ((ep (tem kmt) (emphas (tpe tem kmt))))
(lin
(ML Eer "Europäische Patente werden für %(e:Erfindungen) erteilt." (en "European Patents are granted for %(e:inventions)."))
(ML Elr "Eine %(e:Erfindung) ist die Verkörperung einer Lehre darüber, wie man Naturkräfte auf neuartige Weise zur unmittelbaren Verursachung einer nach bisherigem Wissensstand nicht rechnerisch vorhersehbaren Wirkung zum Zwecke der gewerblichen Herstellung materieller Güter einsetzen kann." (en "An invention embodies a teaching about a new way of using natural forces to directly cause a physical effect which could not have been predicted by calculatory inference from prior knowledge and which is applied to the industrial production of material goods."))
))

(lin
(ML Aee2 "Als Erfindungen werden insbesondere nicht angesehen:" (en "Especially the following are not considered to be inventions:"))
(ol
(ML Een "Entdeckungen sowie wissenschaftliche und mathematische Theorien und Methoden" (en "Discoveries as well as scientific and mathematical theories and methods"))
(ML vhh2 "ästhetische Formschöpfungen" (en "esthetic creations"))
(ML Pki2 "Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele oder für geschäftliche Tätigkeiten sowie Programme für Datenverarbeitungsanlagen" (en "schemes, rules and methods for performing mental acts, playing games or doing business, and programs for computers"))
(ML Ite "Informationsgebilde aller Art und ihre Wiedergabe auf Datenträgern" (en "all kinds of information structures and their representation on physical media"))
(ML hin "von Ärzten, Handwerkern, Bauern und freiberuflichen Dienstleistern aller Art in Hand- oder Kopfarbeit ausgeübte Verfahren" (en "methods of manual or mental labor exercised by physicians, handworkers, farmers or practitioners of all kinds of freelance service professions"))
))
) )
;nov
)

(old (ML Ben "Bisherige Version des §52" (en "Current Version of §52"))
(artiquote
(colons (mlval 'Ail) (ML PaI "Patentfähige Erfindungen" (en "Patentable Inventions")))
    
(ol
(ML Eev "Europäische Patente werden für Erfindungen auf allen Gebieten der Technik erteilt, sofern sie neu sind, auf einer erfinderischen Tätigkeit beruhen und gewerblich anwendbar sind." (en "European patents shall be granted for inventions in all fields of technology, as far as they are new, involve an inventive step and are susceptible of industrial application."))

(lin (ML Aee "Als Erfindungen im Sinn des Absatzes 1 werden insbesondere nicht angesehen:" (en "The following in particular shall not be regarded as inventions within the meaning of paragraph 1:"))
(ol
(ML End "Entdeckungen sowie wissenschaftliche Theorien und mathematische Methoden;" (en "discoveries,  scientific theories and mathematical methods;"))
(ML vhh "ästhetische Formschöpfungen;" (en "aesthetic creations;"))
(ML Pki "Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele oder für geschäftliche Tätigkeiten sowie Programme für Datenverarbeitungsanlagen;" (en "schemes,  rules and methods for performing mental acts,  playing games or doing business,  and programs for computers;"))
(ML dao "die Wiedergabe von Informationen." (en "presentations of information.")) ) ) 
(ML Ars "Absatz 2 steht der Patentfähigkeit der in dieser Vorschrift genannten Gegenstände oder Tätigkeiten nur insoweit entgegen, als sich die europäische Patentanmeldung oder das europäische Patent auf die genannten Gegenstände oder Tätigkeiten als solche bezieht." (en "The provisions of paragraph 2 shall exclude patentability of the subject-matter or activities referred to in that provision only to the extent to which a European patent application or European patent relates to such subject-matter or activities as such."))
(ML VW1 "Verfahren zur chirurgischen oder therapeutischen Behandlung des menschlichen oder tierischen Körpers und Diagnostizierverfahren, die am menschlichen oder tierischen Körper vorgenommen werden, gelten nicht als gewerblich anwendbare Erfindungen im Sinn des Absatzes 1. Dies gilt nicht für Erzeugnisse, insbesondere Stoffe oder Stoffgemische, zur Anwendung in einem der vorstehend genannten Verfahren." (en "Methods for treatment of the human or animal body by surgery or therapy and diagnostic methods practised on the human or animal body shall not be regarded as inventions which are susceptible of industrial application within the meaning of paragraph 1. This provision shall not apply to products,  in particular substances or compositions,  for use in any of these methods."))
)
)
;old
)

(epa (ML NsP "Neue Version des Art 52 laut EPA-Basisvorschlag" (en "New Version of Art 52 according to the EPO's Base Proposal"))

(filters ((dk ahs 'swpepue2B)) (ML Dng "Das EPA möchte auf der %(dk:Diplomatischen Konferenz im November 2000) alle Reste einer Defintion der Begriffe %(e:Erfindung), %(e:Technizität), %(e:industrielle Anwendbarkeit) usw. aus dem Gesetz tilgen und stattdessen die universelle Anwendbarkeit des Patentwesens postulieren.  Das hat es dem EPA ermöglicht, seinen Vorschlag sehr kurz zu halten:" (en "At the %(dk:Diplomatic Conference in november 2000), the EPO wants to remove all traces of restricting definitions on terms such as %(e:invention), %(e:technicity), %(e:industrial applicability) etc from the Law and instead postulate universal patentability.  This has allowed the EPO to formulate a very short proposal.")))

(artiquote
(colons (mlval 'Ail) (mlval 'PaI))
(ML Eev "Europäische Patente werden für Erfindungen auf allen Gebieten der Technik erteilt, sofern sie neu sind, auf einer erfinderischen Tätigkeit beruhen und gewerblich anwendbar sind." (en "European patents shall be granted for inventions in all fields of technology, as far as they are new, involve an inventive step and are susceptible of industrial application."))
)
)

(etc (ML Wrn "Weiterer Klärungsbedarf" (en "Further need of clarification"))

(ML Ece "In obigem Vorschlag fehlt eine Abgrenzung für den umstrittenen Bereich der Gentechnologie.  Es läge aber nahe, die Vervielfältigbarkeit zum Ausscheidungskriterium zu erheben.  Sowohl Information als auch bei Leben kann ohne industriellen Aufwand vervielfältigt werden.  Patentansprüche sollten sich demnach nur auf Gegenstände erstrecken können, deren %(e:Reproduktion) über das beanspruchte Verfahren erfolgen muss." (en "The above proposal contains no clarifications for the area of biotechnology.  It would however be possible to establish the reproductibility as a delimiting criterion.  Both information and life can be reproduced without an industrial effort.  Patent claims could thus be restricted to objects whose %(e:reproduction) requires the claimed process."))
(filters ((sg ahs 'swpatbasti)) (ML Ded "Durch eine Eingrenzung würde das Patentwesen auf den Bereich zurückgestutzt, in dem es gut funktioniert.  Dies kann in einigen Gebieten dazu führen, dass Bedarf nach neuen %(sg:Sui-Generis-Schutzsystemen) oder sonstigen sachgerechten Systemen zur Belohnung der Forschungs- und Entwicklungstätigkeit entsteht." (en "By clear delimitations such as the above-proposed, the patent system would be restricted to its original domain, in which it functions fairly well.  This could in some areas lead to demands for new %(sg:sui-generis rewarding systems) for intellectual labor or R&D.")))
)
; sects
)
;epue52 
)

(let ((toc nil))
(mlhtdoc 'swpateurili nil nil nil
(concat (ML Gte "Geleitet von der Erkenntnis, dass die Grenzziehung zwischen patentierbaren und nicht patentierbaren Gegenständen besonders klare und explizite Entscheidungen auf gesetzgeberischer Ebene erfordert" (en "Led by the understanding that drawing the borderline between patentable and non-patentable objects requires particularly clear and explicit decisions at the legislative level")) ";")

(concat (ML Isa "beeindruckt von der Erfahrung, dass Art 52 des Europäischen Patentübereinkommens (EPÜ) der Rechtsprechung Raum für Missverständnisse und Ungereimtheiten gelassen hat" (en "impressed by the experience that Art 52 EPC has left room for misunderstandings and inconsistencies at the jurisdictional level")) ";")

(ML Ihf "beunruhigt von der Aussicht, dass von diversen Gerichten neu gesetzte Regeln die Wissensausbreitung und Innovation in der Informationsgesellschaft zunehmend behindern könnten" (en "concerned by the prospect that new rules created by various lawcourts could increasingly impede knowlege diffusion and innovation in the information society"))

(ML wer "stellen wir folgendes klar:" (en "we clarify the following:"))

(lset ((etcsam (ML EWn "Entsprechendes gilt für alle in Art 52 (2) EPÜ aufgelisteten Kategorien nicht-patentierbarer Gegenstände." (en "The same applies to all categories of non-patentable objects listed in Art 52 (2) EPC."))))
(ol
(ML Unr "Unter einem %(q:Programm für Datenverarbeitungsanlagen), kurz %(q:DV-Programm) genannt, ist das Programm in all seinen Entwurfsstufen, vom gedanklichen Plan bis zu der von einem Menschen oder einem Prozessor ausführbaren Anweisung zu verstehen.  Ein DV-Programm ist Bauplan und Gebrauchsanweisung, Verfahrensbeschreibung und Problemlösung, Sprachkunstwerk und virtuelle Maschine, Erzeugnis und Verfahren zugleich." (en "The term %(q:programs for computers), in short %(q:computer programs) refers to the program at all design levels from the conceptual plan to an instruction executable by a human or by a processor.  A computer program is a building plan and an operation instruction, a description of a process and a solution to a problem, a literary work and a virtual machine, a product and a process, all in one."))

(ML Dco "Ein durch ein DV-Programm beschriebener technischer Prozess ist begrifflich von dem DV-Programm als solchem unterscheidbar.  Ein Schachbrett-Herstellungsverfahren ist von dem Schachspiel als solchem zu unterscheiden.  Die Partikel %(q:als solche) in Art 52 (3) EPÜ ist nur aus ihrer Funktion in den beiden obigen Sätzen heraus zu verstehen: sie dient der Differenzierung zwischen zwei voneinander unabhängigen Kategorien, durch die ein potentieller Patentgegenstand beschrieben werden könnte.  Ein %(q:DV-Programm mit einer zusätzlichen technischen Wirkung) gehört zur Kategorie der DV-Programme und folglich auch der %(q:DV-Programme als solche), sofern diesem Wortgefüge außerhalb eines gültigen Satzzusammenhangs überhaupt eine Bedeutung zukommen kann." (en "A technical invention described by means of a computer program can, at a conceptual level, be distinguished from the program as such.  Likewise a method for producing chessboards can be distinguished from the chess game as such.  The particle %(q:as such) in Art 52 (3) EPC is to be understood only from its function in the above sentences: it serves to differentiate between two mutually independent categories by which a potentially patentable object could be described.  A %(q:computer program with a further technical effect) belongs to the category of computer programs and hence also of %(q:computer programs as such), as far as this word group can mean anything outside of a valid sentential context."))

(lin
(ML Pdd "DV-Programme sind keine Erfindungen im Sinne des europäischen Patentrechts. Chemische Verfahren, Reifenbremsverfahren oder andere technische Verfahren können insoweit patentfähige Erfindungen sein, wie sie vom Computerprogramm nicht nur begrifflich sondern auch praktisch (im Hinblick auf die Rechtsausübung) trennbar sind.   D.h. die Lösung des Problems muss in einem technischen Bereich jenseits des Programmierens liegen, und die daraus abzuleitenden Ausschlussrechte müssen sich auf materielle Gegenstände außerhalb des Programms wie z.B. Chemikalien oder Fahrzeug-Triebwerke richten." (en "Computer Programs are not inventions in the sense of European patent law.  Chemical processes, tyre braking processes and other technical processes controlled by computer programs can be inventions, as far as they are distinguishable from the computer program not only at a conceptual but also at a practical level.  I.e. the problem and the solution must lie in a technical realm outside of the program, and the exclusion rights derived therefrom must be directed to material objects outside of the program, such as e.g. chemicals or automobile engines."))
etcsam
)

(ML UWg "Unter Technik ist der Einsatz von Naturkräften zur unmittelbaren Verursachung einer Wandlung von Materie zu verstehen.  Gegenstände, die sowohl technische als auch nicht-technische Merkmale enthalten, sind nur dann Erfindungen, wenn das als neu und erfinderisch Beanspruchte, also der Kern der Erfindung, im Technischen liegt.  Ein durch ein DV-Programm auf bekannten Geräten gesteuerter technischer Prozess ist genau dann eine Erfindung, wenn er auf neue Weise Naturkräfte zur unmittelbaren Verursachung eines nach bisherigem Wissensstand nicht rechnerisch vorhersehbaren Erfolges bei der Herstellung materieller Güter nutzt." (en "A %(q:technical) process is one that uses natural forces to directly cause a transformation of matter.  Objects that contain both technical and non-technical features are inventions only if the part that is claimed to be new and inventive, i.e. the core of the invention, lies in the technical realm.  A technical process controlled by a computer program on known hardware is an invention if and only if it uses natural forces in a new way to directly cause a success in production of material goods that could not have been predicted by mere computation based on prior knowledge."))
))
))


(mlhtdoc 'swpatbasti nil nil nil
(sects
(pubpriv (ML EUs "Property, Patents, Copyright and the Public Domain" (de "Eigentum, Patente, Urheberrecht und das %(q:Niemandsland)") (fr "Propriété, Brevets, Droit d'Auteur et le Domaine Publique"))

(filters ((gk ahs 'grur-kolle77)) (ML Asg "Abstract-logical innovations are, according to traditional property law of Europe and elswhere, situated in the public domain, also regretfully %(gk:called) %(q:nobody's land) by some intellectual property lawyers:" (de "Abstrakt-Logische Innovationen befinden sich nach traditionellem Eigentumsrecht in einem öffentlichen Bereich, den führende europäische Patentrechtsgelehrte auch %(q:Niemandsland des Geistigen Eigentums) %(gk:genannt) haben:") (fr "Les innovations abstraites-logiques sont, selon la tradition de loi de l'Europe et ailleurs, situés dans le domaine publique, qui à aussi été %(gk:appellée) %(q:terre sans propriétaire) de quelques savants de dogme juridique.")))


(flet ((subtab0 (a b c) (table '((border t) (width "90%") (align center)) '((a (width "30%")) (b (width "30%")) (c (width "30%"))) nil (l (l a b c)))) (subtab1 (a b c) (subtab0 (l (optscall 'font '((color white)) a) '(bgcolor black)) b (l c '(bgcolor white)))))
 (center (table '((border t) (cellpadding 20) (valign center)) '((tit) (form (align center)) (idee (align center))) (l (l nil (l (optscall 'font '((color white)) (nl (ML knr "concrete" (fr "concrèt") (de "konkret")) (ML For "form" (fr "forme") (de "Form")))) '(bgcolor black)) (l (nl (ML asr "abstract" (de "abstrakte") (fr "abstrait")) (ML Ide "idea" (fr "idée") (de "Idee"))) '(bgcolor white)))) (l (l '((bgcolor black)) (optscall 'font '((color white)) (bold (nl (ML MmW "physical" (fr "objét physiques") (de "Physikalien")) (ML Mtr "matter" (de "Materie") (fr "matière"))))) (l (optscall 'font '((color white)) (ML Egn "private material property" (de "Materielles Privateigentum") (fr "propriété matérielle privée"))) '(bgcolor black)) (l (nl (ML Per "patents & utility certificates" (fr "brevets et certificats d'utilité") (de "Patent & Gebrauchsmuster")) (subtab1 (ML Een "product" (fr "produit") (de "Erzeugnis")) (ML Vfr "process" (de "Verfahren") (fr "procédé")) (ML Gce "description" (fr "déscription") (de "Beschreibung")))) '(bgcolor gray))) (l (bold (nl (ML IeG "logical" (de "Logikalien") (fr "objéts logiques")) (ML GWm "mind, information" (de "Geist, Information") (fr "pensée, information")))) (l (ML Ube "copyright" (de "Urheberrecht") (fr "droit d'auteur")) '(bgcolor gray)) (nl (ML NdW "Geistiges Gemeineigentum" (en "public intellectual property") (fr "propriété intellectuelle publique")) (subtab0 (ML Uri "Programmierter Universalrechner" (en "programmed computer") (fr "ordinateur programmé")) (ML PmA "Programm-Verhalten" (en "program behaviour") (fr "comportement de programme")) (ML Prt "Programm-Text" (en "program text") (fr "texte de programme")))))))))

 (ML Bhn "Bei den physischen Abstraktionen gibt es ein Gefälle vom materiellen Erzeugnis über das mehr oder weniger eng damit verbundene Verfahren bis hin zur vollends immateriellen Verfahrensbeschreibung.  Im Bereich der logischen Gegenstände (Logikalien) steht allen dreien lediglich das Computerprogramm als Entsprechung gegenüber, welches dank seiner Immaterialität zahlreiche Aspekte in sich vereinigt." (en "In the case of physical abstractions, there is a gradation from the material product to a process more or less closely connected thereto to a fully immaterial description of that process.  In the field of logical objects, all three correspond to a computer program, which unites multiple aspects in one."))

 (ML Iee "In dem Maße, wie ein Eigentumsgegenstand sich von der Materie löst, kann er durch kostenarme Vervielfältigung Gemeingut werden.  Gebrauchsmuster gibt es nur für materielle Erzeugnisse, Patente auch für damit zusammenhängende Verfahren, nicht aber für Verfahrensbeschreibungen, obwohl letztere auch einen Marktwert haben, der durch Nachahmer gemindert werden könnte.  Die Gedanken sind frei. Informationswerke verbreiten sich noch schneller als Gedanken.  Je leichter vervielfältigbar ein Gegenstand ist, desto weniger ist man geneigt, seine Verbreitung zu verlangsamen, indem man Eigentumsansprüche darauf anerkennt.  Wenn die Vervielfältigungskosten bei Null liegen, errechnet sich der Verlangsamungsfaktor als eine Division durch Null, d.h. der Schmerz tendiert gegen unendlich." (en "To the degree that a property object is detached from matter, it tends to become a public good through cheap replication.  Utility certificates are granted only for material products while patents are also granted for processes connected thereto, but not for process descriptions, although the latter have a market value which is often diminished by imitators.  The thoughts are free.  Information spreads even faster than thoughts.   The more easily an object can be replicated, the less people are inclined to slow down its diffusion by property titles.  If the cost of replication is zero, than the slow-down factor would be %(q:something divided by zero), i.e. infinitely painful."))

 (ML Etd "Ein ähnliches Gefälle liegt zwischen den %(q:Erfindungen) und %(q:Schöpfungen) auf der einen und den %(q:Entdeckungen) und Natur-Ressourcen auf der anderen Seite.  Erstere sind konkret und einmalig, letztere werden in immer der gleichen Weise von jedem vorgefunden, der an einem bestimmten Ort vorbeikommt, sei es in der physischen Welt oder in mathematisch-logischen Räumen." (en "There is a similar gradation between %(q:creations) and %(q:inventions) on the one side and %(q:discoveries) and natural ressources on the other.  While the former are concrete and individual, the latter are found in identical form by anyone who comes across a certain place, be it in the physical world or in some mathematically constructed space."))

 (ML AiW "Auf all diesen Gefälle-Achsen liegen die typischen Software-Patente ganz rechts unten in dem Raum des naturrechtlichen Gemeineigentums, des %(q:Niemandslandes des Geistigen Eigentums).  Auf Logikalien 20 Jahre lange Monpole zu erteilen ist ebenso naturrechtswidrig wie am anderen Ende des Extrems die Vergesellschaftung aller Küchengeräte in der %(q:Volkskommunenbewegung), mit der Mao Tse-tung in China 1960 eine Hungerkatastrophe erzeugte.  Alle Eigentumsregime sind möglich, aber die Gesetzmäßigkeiten schlagen zurück und wir zahlen den Preis." (en "On all these gradation axes, the typical software patents are situated in the lower right corner, in what seems to be a public domain by natural law.  To hand out 20 year monopolies on software concepts thus seems as much counter-natural as on the other extreme the collectivization of kitchen utensils in the %(q:people's commune movement), by which Mao Tse-tung created a famine in China of 1960.  Humans can establish any property regime they want, but natural law lashes back and we pay the price."))

(ML Arv "Viele Diskutanten äußern seit jeher grundsätzliche Bedenken gegen Eigentumsrechte an abstrakt-logischen Konzepten:  einerseits belohnen solche Eigentumsrechte meist nicht den wirklich aufwendigen Teil des Software-Innovationsprozesses, andererseits schränken sie aber die Innovationsfreiheit besonders empfindlich ein." (en "Many Discutants have warned against allowing any type of appropriation of abstract-logical concepts:  on the one hand side such property rights would very often not compensate the really cost-intensive part of the software innovation process, while on the other hand side generating high public costs by limiting important freedoms."))

(ML VaW "Viele Software-Entwickler und IT-Unternehmer halten das Software-Urheberrecht für %(q:völlig ausreichend) und geradezu %(q:maßgeschneidert)." (en "Many software developpers and IT entrepreneurs consider software copyright to be %(q:quite sufficient) or even %(q:perfectly tailored to the reality of software innovation)."))

(ML Dge "However here and there we can find extreme counter-examples.  Some mathematical theorems were known for centuries before someone came up with a successful proof.  Yet even this does not mean that a simple and brute monopoly on all computer program implementations of an abstract-logical innovation is the price that society has to pay.  Besides patent-like monopolies, less disruptive remuneration systems should be envisaged for this realm." (de "Dennoch gibt es hier und da auch klare Gegenbeispiele.  So wurden etwa manche mathematische Beweise erst nach jahrhundertelanger Suche gefunden.  Die Frage, ob ein patentähnliches Ausschlussrecht auf immaterielle Implementierungen solcher Beweise der Gesellschaft mehr nützt als schadet, ist damit aber noch nicht beantwortet.  Vieles spricht dafür, die Gedanken grundsätzlich frei zu halten und stattdessen zur Förderung des Fortschritts ein System von nicht-exklusiven Vergütungsrechten einzurichten."))
  
)

(softent (ML Wnd "Weiche Patente für Hardware-Logik" (en "Soft Patents for Hardware Logics") (fr "Brevets softes pour logique durcie"))

(ML Eti "Innovative processes for whose implementation not more than a new computer program running on known hardware is needed, could still be monopolised as long as this monopoly can be enforced without impeding the dissemination of computer programs, i.e. in those cases in which the process does not run on a universal computer.  Thus, if a car maker does not choose to lay out his motor control device for customer-side reprogramming (making it more costly and less safe), he would probably prefer to pay moderate license fees to a patent owner." (de "Innovative Prozesse, zu deren Umsetzung ein neuartiges Computerprogramm auf herkömmlichen Rechenapparatur genügt, könnten insoweit monopolisiert werden, wie dieses Monopol ohne ein Verbot der Weitergabe von Computerprogrammen durchgesetzt werden kann, m.a.W. wie der Prozess nicht auf einem Universalrechner abläuft.  Wenn z.B. ein Autohersteller nicht gerade seine Motorsteuerung kundenseitig programmierbar (und damit teurer und weniger sicher) machen will, würde er es vermutlich vorziehen, einem Patentinhaber maßvolle Lizenzgebühren zu zahlen."))

)

(logileg (colons (ML Lgl "Logilege" (fr "logilège") (de "Logileg")) (ML Nee "Nutzungsprivileg auf abstrakt-logische Innovationen" (en "Use Privilege for Abstract-Logical Innovations") (fr "Privilége d'usage sur des innovation abstraites-logiques"))) 

(filters ((ll (lambda (str) (tpe (emphas str) (emphas (ML Leg "Logikaliennutzungsprivileg" (en "logical innovation use privilege")))))) (fi pet (bridi 'ekz (ML Plr "freier Quelltext" (en "free source code")))) (po pet (bridi 'ekz (ML iiS "in proprietärer Software" (en "in proprietary software")))))
 (let ((L1 (ol (ML dWs "die Anwendung des Verfahrens für geschäftliche Zwecke" (en "application of the method for commercial purposes")) (ML dei "die Umsetzung des Verfahrens in %(po:Eigentumsgegenständen)" (en "implementation of the method in %(po:property-restricted objects)"))))
       (L0 (ol (ML dEn "die Nutzung zur Erreichung von Interoperabilität" (en "the right to use the method for achieving interoperability")) (ML dfa "die Veröffentlichung in %(fi:frei weiterentwickelbaren Informationsgebilden) mit einem angemessenen Verweis auf die Nutzungseinschränkungen" (en "the right to publish the method in %(fi:freely republishable information works) with an appropriate pointer to the usage restrictions"))))
       )
   (lin
    (ML DfW "Der Inhaber eines %(ll:Logilegs) genießt ab dem Tag der Offenlegung eines innovativen logischen Verfahrens einige Jahre lang Ausschlussrechte auf %{L1}." (en "The owner of a %(ll:logilege) publishes an innovative abstract-logical method and enjoys exclusion rights on %{L1} for a fixed number of years starting from the day of publication."))
    (ML NbW "Nicht untersagbar sind %{L0}" (en "Nobody may be excluded from %{L0}"))
)))

(ML Ogt "Offenzulegen ist mindestens eine Referenzimplementation und ein daran angelehnter Anspruchsbereich.  Diese Offenlegung muss strengen formellen und sachlichen Anforderungen genügen, deren Ziel es ist, sie für die Öffentlichkeit leicht zugänglich und überprüfbar zu machen.  Der Anspruchsbereich muss so eng gefasst sein, dass er weder vorbekannte Verfahren noch künftige Innovationen beinhaltet." (en "The published logilege description comprises one or more reference implementations and a set of claims.  The logilege description must satisfy strict formal and material requirements so as to make it easy for the public to access the description and test its validity.  The scope of claims must be sufficiently narrow so as to contain neither prior art nor future innovations."))

(ML Deg "Das Logileg gilt sofort mit der Offenlegung. Die Gültigkeitsprüfung obliegt dem Logileginhaber.  Während der Laufzeit kann jedermann Einspruch erheben.  Gebühren fallen nicht an, aber der Logileginhaber hinterlegt notarisch eine Kaution von 5000 EUR, die zur Belohnung an den ersten Einspruchsführer ausgezahlt wird, dem es gelingt, die Ungültigkeit des Logilegs nachzuweisen. Bei Nachweis eines Mangels fällt das ganze Logileg.  Nachbesserungen sind nicht gestattet." (en "The logilege enters into force on the day of its publication.  There is no examination procedure.  The owner guarantees that claims are valid by paying a deposit of 5000 EUR, which will be paid to the first person who succeeds in proving the invalidity of the logilege.  One serious flaw, such as an inadmissible claim, suffices to invalidate the whole logilege.  Posterior narrowing of claims is not allowed."))

(ML Djh "Initially only the %(q:problem to be solved by the innovation) is published together with the prior art, and the whole logilege description is made available only in an unreadable encrypted form and decrypted after one month.  During this month, any person may publish solutions to the problem, which become prior art.  If such prior art invalidates the logilege, the deposit sum is paid to the author of the earliest invalidating publication." (de "Zunächst wird nur das %(q:von der Innovation zu lösende Problem) zusammen mit der vorbekannten Technik veröffentlicht, während die gesamte Logilegschrift in verschlüsselter Form bereitgestellt und erst nach einem Monat entschlüsselt wird.  Während dieses Ausschreibungsmonats kann jedermann Lösungen des Problems veröffentlichen, die dann als vorbekannte Technik gelten.  Falls diese vorbekannte Technik das Logileg ungültig macht, wird die Kautionssumme an der Urheber der ersten anspruchsbrechenden Veröffentlichung ausgezahlt."))

(ML Dei "The applicant may allow more time for the problem solving quest and in return obtain a longer exclusion period, e.g. one extra year of validity for each extra week of problem solving quest." (de "Der Anmelder darf auch eine längere Zeitdauer für das Preisausschreiben zulassen und erhält im Gegenzug bei Erfolg eine längere Ausschlussdauer, z.B. ein zusätzliches Gültigkeitsjahr für jede zusätzliche Ausschreibungswoche."))
)

(logidend (colons (ML Lin "Logidende" (en "Logidend")) (ML Laa "Logikalieninnovationsdividende" (en "Logical Innovation Dividend") (fr "dividende d'innovation logicielle")))   

(ML Irt "Instead of exclusion rights we here grant non-exclusive remuneration rights.  Such rights directly reward innovators without interfering with other people's programming freedom or with interoperability.  Our informational infrastructure thus remains a property-free Commons zone.  The logidend is payed out of funds from taxes on computing devices or the like.  The value of each logidend is determined by a sophisticated social engineering scheme rather than by the conventional %(q:free market) approach." (de "An Stelle von Ausschlussrechten könnten wir auch nicht-exklusive Vergütungsrechte vergeben.  Solche Rechte würden Innovatoren direkt belohnen, ohne Programmierfreiheit oder Interoperabilität zu beeinträchtigen.  Die Welt der immateriellen Ideen bliebe eine eigentumsfreie Allmende.  Die Vergütungen müssten jedoch aus einem Steuerfond finanziert werden, und würden somit anstelle des gewöhnlichen Marktwettbewerbs relativ aufwendige Sozialtechniken erfordern."))

(ML DfW "The owner of a %(q:logidend) publishes an innovative abstract-logical method and enjoys a dividend in a public fund for 10 years starting from the day of publication.  The money is raised through a special tax, e.g. on computing devices as currently done for copyright in Germany.  Each taxpayer has a vote, which he can cast through free and secret balloting in favor of one of several qualified innovation promoting organisations, each of which applies its own policy to determine the dividend value of each logidend, based on its estimated contribution to the direction of social progress which the organisation intends to promote." (de "Der Inhaber einer Logidende genießt ab dem Tag der Offenlegung eines innovativen logischen Verfahrens 10 Jahre lang Vergütungsrechte.  Zur Verteilung der Vergütungen wird ein Fond aus Gerätesteuern u.a. gebildet, in dem jeder Steuerzahler eine Stimme hat, die er in geheimer Wahl einer qualifizierten Verteilungsorganisation geben kann.  Jede Verteilungsorganisation bestimmt nach einem eigenen Wertungssystem den Dividendenwert jeder einzelnen Logidende."))

(ML Dae "The logidend excludes nobody from use of the idea.  However verbal tribute should be paid to the innovator wherever the idea is used." (de "Die Logidende schließt niemanden von der Nutzung der Idee aus.  Allerdings sollte dem Innovator überall dort verbale Anerkennung gezollt werden, wo seine Idee verwendet wird."))

(ML Tvf "The rules for sorting out real innovations are similar to those of the %(q:logilege) outlined above." (de "Die Spielregeln zur Ermittlung echter Innovationen sind ähnlich wie beim oben skizzierten %(q:Logileg) zu gestalten."))
)

(patref (ML Ret "Inspiration for Patent Reform" (de "Anregungen für eine Reform des Patentwesens") (fr "Inspirations pour reformes du système de brevets"))

(ML Lde "Sonderrechte obiger Art könnten in dem abstrakt-logischen Niemandsland vergeben werden, wo mangels Konkretheit und physischer Substanz (Technizität) weder das Patentsystem noch das Urheberrecht greift.  Einerseits erfordert die Leichtigkeit und Schnelllebigkeit abstrakt-logischer Innovationen eine besonders zügige und unbürokratische Verfahrensabwicklung, andererseits beeinträchtigen Privilegien in diesem Bereich die Öffentlichkeit in unerhörtem Maße.  %(q:Geistiges Eigentum) an abstrakten Ideen kommt einer %(q:Geistigen Umweltverschmutzung) gleich, für die der Logileginhaber besondere ausgleichende Leistungen zu erbringen hat." (en "%(q:Third Paradigm) rights can be granted for those innovations which due to lack of concreteness or physical substance (technicity) fall out of the classical patent system.  The lightness and shortlivedness of abstract-logical innovations requires a fast and unbureuacratic procedure, while the great public cost, that any privilege on abstract-logical innovations tends to generate, demands a high degree of responsibility on the part of the privilege owner."))

(filters ((tr ahs 'swpattrips)) (ML Vem "Many aspects of the logilege could also serve to inspire a reform of the existing patent system.  But patent lawyers will contend: %(q:if it ain't broke don't fix it).  Within the %(tr:framework of patent treaties), reforms may meet even more obstacles, starting from a minimum requirement of 20 years of patentability (TRIPS Art 32).  It will be hard to overcome the resistance from those who insist that the conventional patent system works well enough in its home-base area of physical inventions.  In the area of logical innovation however more refined means such as the invalidating quest system are indispensable, because the truly worthy innovations must be filtered out from a volatile world where innovation is daily routine and the state of the art is rarely consulted let alone documented.  Thus any abstract-logical property paradigm faces specific challenges, for which the patent system never needed to develop an answer." (de "Viele Aspekte des Logilegs könnten auch als Anregung zur Reform des traditionellen Patentwesens taugen.  Angesichts der gültigen %(tr:internationalen Verträge) ist eine solche Reform aber kaum durchzusetzen, und im Bereich der konkret-physischen Erfindungen ist ihre Notwendigkeit auch viel weniger spürbar.  So ist z.B. ein Einreichungswettbewerb speziell bei abstrakt-logischen Innovationen erforderlich, weil die privilegierungswürdigen Innovationen aus einer flüchtigen Welt herausgefiltert werden müssen, in der Innovation zur täglichen Routine gehört und der Stand der Technik selten konsultiert geschweige denn dokumentiert wird.  So ergeben sich für das abstrakt-logische Eigentumsparadigma spezifische Herausforderungen, auf die das Patentwesen nie eine Antwort entwickeln musste.")))
)

(links (ML Fea "Further Reading" (de "Weitere Lektüre") (fr "Autres textes a lire"))
(linklist
(l (ahs 'grur-kolle77) (ML Aos "A leading scholar of the software patent debate of the 1970s explains in this famous article of 1977 why, according to any coherent concept of technicity, software innovations cannot be judged as technical and therefore not be patented.  Kolle remarks that there is a %(q:nobody's land) of abstract ideas, which should stay in the public domain, because their appropriability would have enormous blocking effects while generating little benefit for the software economy." (de "Ein Wortführer der Debatte um den Patentierbarkeitsausschluss von Computerprogrammen in den 70er Jahren erklärt in diesem Artikel von 1977 warum Software-Innovationen nach jedem kohärenten Technizitätskonzept nicht als technisch beurteilt werden und daher nicht patentiert werden können.  Kolle bemerkt, dass es ein %(q:Niemandsland des geistigen Eigentums) an Algorithmen und abstrakten Ideen gibt, deren %(q:Vergesellschaftung) zu wünschen ist, da ihre Aneignbarkeit anders als bei herkömmlichen technischen Erfindungen große Sperrwirkungen entfalten würde, denen keinen nennenswerter gesamtwirtschaftlicher Nutzen gegenüberstünde.")))
(l (ahs 'sadaka-manifesto) (ML lWr "leading law experts argue against extension of the patent system to software and point out inadequacies of copyright.  The dual nature of software requires a system of short-term protection of investment against too early cloning." (de "Führende Rechtsexperten Amerikas argumentieren 1994 gegen die Ausweitung des Patentwesens auf Software und zeigen gleichzeitig Unzulänglichkeiten des Urheberrechts auf.  Die hybride Natur von Software lässt ein System des Verbots von zu schneller Nachahmung sinnvoll erscheinen.  Dabei gibt es keine Anspruchsprüfung und nur eine kurze Schutzdauer.")))
(l (ahs 'paleymark-modelact) (ML ahn "a more detailed study on how the Third Paradigm Manifesto could be integrated into the US legal system" (de "eine detaillierte Studie über dieUmsetzung der Gedanken des Manifests in amerikanisches Recht und die zu erwartenden Wirkungen auf die Praxis, die recht positiv beurteilt werden.")))
(l (ahs 'smets-stimuler) (ML Org "One chapter of this report is dedicated to the question of appropriate schemes for the protection of investment in software.  Among others, a short and weak form of algorithm monopoly is envisaged." (de "Ein Kapitel dieses umfangreichen Berichtes eines französischen Regierungsorgans, der auch ins Englische übersetzt wurde, widmet sich der Frage möglicher angemessener Systeme zur Belohnung von Software-Innovatoren.  U.a. wird auch eine kürzere und schwächere Form von Algorithmenmonopolen in Erwägung gezogen.")))
(l (ahs 'mmayer-pmsoftpa) (ML ImW "In autumn of 2000, the CDU/CSU fraction supports a motion in the German Federal Parliament which calls for a Third Paradigm type software property regime" (de "Im Herbst 2000 unterstützt die CDU/CSU-Fraktion im Deutschen Bundestag einen Antrag, der einen %(q:maßgeschneiderten Software-Rechtschutz) zwischen Patent und Urheberrecht fordert.")))
(l (ahs 'swxpatg2C) (ML IWn "Renommierte Softwareunternehmen und Verbände fordern fünferlei gesetzgeberische Maßnahmen zum Schutz der informatischen Innovation for dem Missbrauch des Patentwesens.  Initiative Nr. 4 regt die Schaffung von %(q:sachgerechten Systemen zur Förderung der informationellen Innovation) an." (en "Renowned software companies and associations call politicians to support five law initiatives for the protection of information innovation against the abuse of the patent system. Initiative no. 4 calls for the creation of %(q:adequate systems for the stimulation of information innovation).") (fr "Entreprises et associations de logiciel connus demandent au politiciens de soutenir cinque initiatives législatives pour la protection de l'innovation informatique contre l'abus du système de brevets.  La 4ème initiative fait appel pour la création des %(q:système adaptés pour la stimulation de l'innovation informatique).")))
)
)
)
)

(mlhtdoc 'swpatpreti nil nil nil

(sects
(req (ML Ani "Requirements for a Study" (de "Anforderungen an eine Studie"))
(ML DnW "Das Thema Swpat stellt die Politik vor Entscheidungen.  Es ist wichtig, vorher die wirtschaftlichen Folgen abzuschätzen.  Nicht alles kann auf philosophischer Ebene beantwortet werden.  Empirische Forschung ist notwendig.  Wenn der FFII e.V. ausschreiben würde, sollte die Studie etwa folgende Anforderungen erfüllen.")

(sects
(pub (ML Gin "Größtmögliche Öffentlichkeit")

(ML Sev "Sie sollte von Anfang an im WWW unter Einbeziehung einer größtmöglichen Öffentlichkeit stattfinden.  Insbesondere Wissenschaftler und IT-Fachleute (Entwickler, Unternehmer) sollten angeregt werden, sich einzubringen.  Dies kann dadurch geschehen, dass im Namen des BMWi oder einer Universität ein dynamischer Webserver etwa mit Zope aufgesetzt wird.  Es gibt bereits Zope-Module für die Verwaltung und Auswertung von Umfragen.")

(ML Elr "Ein Ergebnis der Übung sollte sein, dass sich ein weiter Kreis von Interessenten formiert, der in Zukunft als Netzwerk dem Gesetzgeber zur Seite steht und ihm hilft, die Entscheidung über die Kriterien der Patentierbarkeit und die angemessenen Paradigmen der Innovationsförderung wieder zurück in seine Hände zu bekommen.")
)

(opt (ML TWo "Theoriebildung und Handlungsoptionen vorweg") 

(ML Dil "Die Studie muss nicht unbedingt sehr kluge Theorien liefern.  Es kommt eher darauf an, dass sie wertvolle empirische Daten liefert.  Dazu ist die Theoriebildung voraussetzung.  Diese sollte also bereits spätestens in der Bewerbung, noch besser in der Ausschreibung, erfolgt sein.")

(ML Eeg "Es sollten von Anfang an die möglichen Handlungsoptionen herausgearbeitet werden, damit man dann deren Auswirkungen auch erforschen kann.  Der Fehler der IPI-Studie der EU-Kommission muss vermieden werden.  Dort wurden ständig juristische Fragen in die ökonomischen hineingemischt und es wurde gar nicht gefragt, was passieren würde, wenn man das Patentwesen auf seinen Kernbereich zurückschneiden und/oder DV-Programme draußen halten würde.")

(ML Dun "Die Studie sollte nicht Handlungsoptionen empfehlen sondern solche als gegeben voraussetzen und lediglich die möglichen Auswirkungen erforschen.  Es ist nicht Sache von Wissenschaftlern, Empfehlungen über Handlungsoptionen abzugeben. Das müssen Politiker letztendlich abwägen.")

(ML IWO "Im wesentlichen sind folgende Optionen gegeben:")
    
(sects
(senlim (ML Urn "Unbegrenzte Patentierbarkeit mit unbegrenzter Durchsetzbarkeit")
 (ML DWs "Das EPA und seine Freunde entscheiden lassen, d.h. Konvergenz mit den USA.  (Die von der IPI-Studie untersuchte Unterscheidung zwischen EPA-Praxis und State-Street-Praxis ist weitgehend belanglos)") )

(nonenf (ML Urt "Unbegrenzte Patentierbarkeit mit begrenzter Durchsetzbarkeit") 
 (ML wWc "wie oben, aber Softwarepatente dadurch in der Praxis entwerten, dass man der Veröffentlichung und Vermarkten von Urheberrechtsgegenständen aller Art, einschließlich Computerprogrammen, Vorrang vor der Durchsetzung von Ansprüchen aus Patentschriften einräumt.  D.h. ein Prozess mag zwar beansprucht werden, aber gegen ein entsprechendes Computerprogramm kann dieser Anspruch auf dem Markt nicht durchgesetzt werden.  Eine Variante hiervon ist, dass man nur freie/quelloffene Software freistellt.") )

(nurtech (ML Zar "Zurückschneidung des Patentwesens durch restriktiven Technikbegriff")
 (ML Iua "Innovationen, die auf dem Rechnen mit bekannten Modellen beruhen, sind keine Erfindungen.  Es wäre dann zu untersuchen, inwieweit man noch patentähnliche Schutzrechte für die Nachrichtentechnik, Bioinformatik, mechanische Trivialinnovationen o.ä. haben möchte, die bei einem restriktiven Technikbegriff außen vor bleiben.")
)
)

(ML Bag "Bei all diesen Optionen ist ferner klarzustellen, dass wir von einer Situation ausgehen, in der sich allmählich die ganze Welt für das jeweils präferierte Modell entscheidet.  Alternativ dazu ist zu fragen, was passiert, wenn nur wir uns dafür entscheiden und die USA bei ihrem derzeitigen Modell bleiben.")

(ML Uih "Unternehmen wie Siemens müssen einerseits ihre deutschen Mitarbeiter zur extensiven Patentierung motivieren, um in den USA mithalten zu können.  Dazu brauchen sie in Europa die Möglichkeit, alles zu patentieren, was in den USA patentiert werden kann.  Andererseits nehmen gerade produktentwickelnde Unternehmen wie Siemens auf Dauer erheblichen Schaden, wenn das Patentsystem ausufert und man von professionellen Patententwicklern wie Qualcomm wegen jeder Kleinigkeit erpresst werden kann.  Zwischen Patentinflationslogik und eigentlicher Folgenabschätzung ist zu trennen.")
)

(kval (ML Eut "Erhebung aussagekräftiger Daten")

  (ML Etd "Es sollte vor allem qualitativ gearbeitet werden.  Bevor einem Unternehmen ein Fragebogen präsentiert wird, ist dieses Unternehmen mit den in seinem Bereich bereits erteilten europäischen Patenten zu konfrontieren und spezifisch zur Einschätzung dieser Patente zu befragen.  Es muss davon ausgegangen werden, dass viele Unternehmen und Verbände sich zu dem Thema noch überhaupt keine Gedanken gemacht haben.  Daher muss auch eine Antwort wie %(q:konnte mir noch keine Meinung bilden) zugelassen werden.")
  (ML Del "Die Fragen sind so zu formulieren, dass sie von denjenigen Leuten beantwortet werden, die für die Forschung & Entwicklung und das damit verbundene Finanzkalkül zuständig sind.   Z.B. sind die relevanten Patente zu präsentieren, und es ist zu fragen, mit wie hohe Prozessrisiken das Unternehmen rechnet und welche Rückstellungen dafür gebildet worden sind oder werden sollten.  Für die Beantwortung ist Zeit zu gewähren und es ist auf mögliche Informationsquellen zu verweisen.  Neben einem fixen Grundrahmen ist Platz für schriftlich formulierte Stellungnahmen zu lassen.  Die Antworten sollten möglichst weitgehend veröffentlicht werden und auch nachträglich öffentlich korrigierbar sein.  Ein Fragebogen im WWW ist vorzuziehen.  Anonyme Angaben sind zu ermöglichen, sollten aber eher als Hinweise an die Forscher und Anlass für weitere Nachforschungen denn als belastbare Daten angesehen werden.")
)
) ) 



(kiel (ML Afo "Alter Entwurf: Wie würde die (Nicht)Patentierbarkeit von Software auf Ihre Firma wirken?")

(sects
(viakom (ML Hef "How would software patentability affect your company?")

(ol
(ML Hlr "How many programmers are currently working in your company?")
(ML Hlr "How many patent specialists are currently working in your company?")
(ML Hof "Have you ever been approached or attacked by a patent owner for infringement of his patents?")
(linol 
 (filters ((pi ahs 'swpatpikta)) (ML HsW "The EPO has granted %(pi:20-30,000 software patents since 1995)."))
 (ML HWt "How many of these could be relevant for your field?")
 (ML Hod "How many of these do you own?") )
(lin
 (ML hyc "How much additional money do you expect to spend on")
 (ol (ML pte "patent research")
     (ML pWc "patent applications")
     (ML pux "patent license fees (subtract the fees you expect to earn yourself)")
     (ML pft "patent infringement lawsuits")
     (ML sfp "salaries of patent specialists")
     )
 (ML oto "once software patents become enforcable in Europe?") )
(ML HpW "How much could your company gain by being able to fight off imitators?")
(ML Hlo "How much could your company lose by having to program around other people's patents?")
(linol (ML Dcd "Do you ever disclose source code of your own?")
 (ML naW "No, we do not write software at all.")
 (ML Nub "No, in general it is our principle to distribute binary files only.")
 (ML yms "Yes, customers can obtain the source code if they sign a non-disclosure agreement (NDA).")
 (ML yie "Yes, we sooner or later release much of our source code to the public.") )
(ML H0u "How would the enforcability of nx10000 European software patents affect your willingness to disclose source code?")
)
)

(dipkonf (ML WeN "What should be decided in November 2000?")

(ol
(lin 
 (ML Cre "Computer programs should be patentable, and such patents should be fully enforcable in Europe.")
 (ML TcE "The European law should be changed according to the Basic Proposal of the European Patent Office without further delay.") )
(lin
 (ML Wbd "Whether computer programs should be patentable depends mainly on the effects this would have on the European economy and society.")
 (ML Ehu "Given that European governements have not yet published an official study on these effects, Europe should allow itself a 2 year moratorium for further discussion.") )
(let ((type "a")) (linol (ML CaW "Computer programs should not be patentable.")
 (ML SiW "Software copyright is quite sufficient to protect the legitimate interests of software companies.")
 (filters ((ll ahs 'swpatbasti)) (ML Cle "Copyright alone may not be sufficient.  A %(ll:third paradigm between patent and copyright) should be worked out as an additional incentive for software innovation.")) ) ) ) 
)
)
)

(blas (ML NlW "Noch älterer Entwurf")
(colons (ahs 'blasum) (ah "epcossen.txt" (ML SpW "Software Patent Questionnaire with special regard to the European Patent Convention and Open Source Software")))
)
;sects
)
; swpatreti
)

(mlhtdoc 'swpatdanfu nil nil nil
(sects
(kio (ML Wir "Was ist ein Software-Patent?")
(colons (commas "phm" "xuan") (al
(ML EWn "Ein Software-Patent ist ein Patent, welches darauf abzielt, die Verbreitung von Datenverarbeitungsprogrammen zu untersagen.  Bei einem Software-Patent richtet sich das Verwertungsinteresse des Patentinhabers auf das Datenverarbeitungsprogramm als solches und nicht etwa auf ein von diesem Program trennbares Verfahren (z.B. technisches Verfahren oder Geschäftsverfahren).  Das Patent dient dazu, seinem Inhaber einen Gewinnanteile aus dem Verkauf von Softwarelizenzen zu sichern.")
(ML Dlz "Die Softwarepatente sind eine Unterklasse der %(e:Verfahrenspatente).  Grundsätzlich lassen sich alle automatisierbaren Verfahren als Algorithmen darstellen und durch Computerprogramme steuern.  Um ein Softwarepatent handelt es sich dann, wenn sie darin enthaltene Handlungsanweisung sich an den Programmlogik-Fachmann (Informatiker) -- und nicht etwa an den Naturkräfte-Fachmann (Techniker) oder den Betriebswirt -- richtet und ihn auf seinem Fachgebiet etwas neues lehrt.  Laut europäischem Rechtsverständnis sind nur technische Verfahren patentierbar: es muss eine %(q:Lehre zum technischen Handeln) vorliegen.  Computerprogramme und Geschäftsverfahren gehören laut Gesetz (EPÜ, PatG) nicht zu den patentfähigen Erfindungen.")
(ML Ids "In einem weiteren Sinne gehören alle nicht-technischen Verfahrenspatente zu den Softwarepatenten.  Zwischen %(q:Hardware) und %(q:Software), %(q:Logikalien) und %(q:Physikalien), %(q:Welt der Dinge) und %(q:Welt des Geistes), %(q:Lehre zum technischen Handeln) und %(q:Anweisung an den menschlichen Geist), %(q:materiellen) und %(q:immateriellen) Verfahren liegt ein kategorialer Unterschied mit starken praktischen Auswirkungen.  Zwischen Geschäftsverfahren und Computerprogrammen hingegen lässt sich schwer unterscheiden.  Die meisten Softwarepatente sind wie Geschäftsverfahrenspatente formuliert: sie beanspruchen eine nach außen sichtbaren Funktionsablauf.  Geschäftsverfahrenspatente wiederum werden fast ausnahmslos über Software realisiert, und meistens liegt zumindest ein Teil des Verwertungsinteresses im Verbot von Computerprogrammen.")
(linul (ML LkK "Further Reading" (de "Lektüre")) (ahs 'swpateurili) (ahs 'swpatkorcu) (ahs 'swpatfrili) (ahs 'skk-patpruef) (ahs 'swpatpikta))
))
)
)
)

(mlhtdoc 'swpatfrili nil nil nil
(sects
(sys (ML Aae "A problem of law, not of patent examination")

(let ((HG (ahs 'swpatpikta (ML Efn "European Software Patent Database")))) (ML Aru "By browsing through our %{HG}, you can easily compile a %(q:horror gallery) of impressively trivial and broad patents.  It will be much more difficult to find even a single patent on a method that a programmer would consider worthy of admiration."))

(ML Sqo "Some people say that this is because the patent offices do not do their job well.  If the criteria of %(q:novelty) and %(q:inventive step) aka %(q:non-obviousness) were applied 100% correctly, these people say, software patents wouldn't do much harm.  Some have been prophesying for a decade that is only a matter of time until this problem will be solved.  However the solution is further away than ever.")

(let ((PA (ah "http://www.aful.org/mailman/listinfo/patents" (ML PiW "Patents Mailing list at aful.org")))) (ML Weo "The following dialogue between Ramon Garcia Fernandez, a spanish information scientist, and Steve Probert, deputy director of the British Patent Office, conducted 2000-11-20 on the %{PA}, goes right to the heart of the problem:"))

(dl
(l "Ferndandez"
 (ML MWt "My disagreement with software patents is the philosophy of most patent offices. For me, a patent should be granted only where the research needed for the invention is expensive, so that said research would not be posible without the incentive of the monopoly. By contrast, the opinion of patent offices seems to be that the inventor should somehow have natural right to monopoly. They seem to think that the invention is intrinsically a very difficult process and that all incentive to it is small (I would like to hear Steve's opinion).  By contrast, the invention of a method to solve a problem is part of the daily work of a programmer."))

(l "Probert"
 (ML Ide "I don't really want to express an opinion because I think that would be wrong for me as a Civil Servant and an official of the Patent Office.  What I would say is that the patent system has never differentiated between inventions on the basis of how much the underlying research cost.  It doesn't even distinguish between those inventions that are truly valuable and ground-breaking, and those that are [comparatively] trivial and insignificant. As the law stands, we would not be able to do so even if we wanted to. Only if an invention is known or obvious can we raise a legal objection.")

 (ML TWa "To some extent the system should be proportionate and self-regulating - in industries where the research costs are very high (eg pharmaceuticals), the patents will be more valuable.  (So licencing costs will be higher etc.)  OTOH, where the development costs are lower it will often (but not always, I accept) be cheaper to work around a patent.  In such cases the patent will be worth less.  In other words what I am saying is that the value of a twenty year monopoly varies depending on a number of factors, one of which is the typical cost of research in the field.  Setting arbitrary thresholds based on research costs would effectively discriminate against individuals and smaller companies in favour of the bigger companies who will always spend more on research.  And who would decide how much money needed to be spent to make a particular invention?  (I presume you would not be content to determine the fate of a patent application on the basis of how much was actually spent.)") ) 

(l "Fernandez"
 (ML Psg "Probably the most important problem here is communication between non-programmers (lawyers, patent officials and so on) and programmers.  It is probably very difficult to convince the former about what kind of things are easy for programmers, such as having ideas, and what kind of things are difficult and time consuming.") )

(l "Probert"
 (ML Imr "I cannot speak for lawyers, but I can assure you that many Patent Examiners are programmers themselves.  In my group, all the Patent Examiners who deal with software applications either write computer programs in their spare time or have been employed as programmers before they became patent examiners.  They usually have a pretty good idea whether something would have been easy or time consuming for a programmer.  However, they might express the communication problem the other way around - it's very difficult to persuade programmers that just because an invention is %(q:easy), does not make it any less patentable."))
)
)

(inv (ML Whe "Why not raise the %(q:inventive step) standard?")

(ML Tni "This dialogue confirms what everybody in the patent trade knows:  the criterion of %(q:inventive step) as it stands is not designed to sort out trivial patents.")

(ML Inr "It is very hard to prove that even the most trivial new idea does not contain an inventive step.  The only way to show this currently at the European Patent Office is to prove that the idea can be obtained by simply combining the teaching of two known documents that the %(q:person skilled in the art) would normally consult.")

(ML Ine "In the case of software patents, the person skilled in the arts rarely even consults documents.  New programming problems occur all the time, and %(q:inventing) a solution to them is the normal way to go.  Most such solutions are not even worth being published in any inforomation science journal that an examiner might consult for testing their novelty.")

(ML Eer "Certainly there are here and there some really difficult mathematical or informatical tasks.  Certain mathematical proofs took hundreds of years to find.  So, shouldn't we try to raise the inventivity standard, so that those really great achievements can be singled out for rewarding by some kind of monopoly right?")

(ML Egm "Even if lawmakers wanted to radically reform the existing patent system and raise the %(q:inventivity) standard, they would encounter great difficulties:") 

(dl
(l (ML fft "fuzziness of %(q:invention height)")
 (tan
  (ML HiW "How would they define %(q:height of invention) (Erfindundhöhe), as it is still sometimes called in old-fashioned German patent jargon?  %(q:At least 20 inches above the state of the art)? %(q:At least 20 twists of the brain beyond the state of the art)?")
 (filters ((sb ahs 'swpatbasti) (tr ahs 'swpattrips)) (ML Oev "It has been %(sb:suggested) that a social game could help here:  let the patent applicant first publish only his problem and provide incentives for the public to submit solutions until a certain deadline.  All these solutions are then considered to be prior art.  This could really work, but it is a very radical reform proposal for a patent system which is governed by strong forces of inertia, who will always find good legal arguments against any even very moderate reform proposal that might diminishes the number of patents granted.")) )
)

(l (ML tye "triviality by sequentiality")
   (ML AWf "Abstract-logical constructions are usually composed of many tiny inventive steps in sequence.  Each patent will usually focus on one of these steps, thus making this patent trivial and broad, even if the innovation itself was truly ingenuous.  This has happened e.g. in the case of the MP3 patents.") )

) ) 

(tech (ML HWW "The broken regulative: technical character")

(filters ((ep (lambda (str) (quot (ahs 'swpateurili))))) (ML TyT "Traditionally there has been one other criterion that has helped to sort out trivial ideas:  the criterion of technical (aka physical, material, empirical) character.  At least in Europe and Japan, patent offices and patent courts still pay lipservice to this criterion.  We have concisely formulated this criterion in our proposed %(ep:Regulation about the invention concept of the European patent system and its interpretation with regard to computer programs).  This shows that the mind-matter borderline can be resharpened to provide a clear criterion, which excludes those %(q:post-industrial) innovations that are based only on abstract calculus and do not require experimentation.  Finding a new causal relation between natural forces and a physical effect is usually much more difficult than finding a new mathematical relation."))

(ML WiW "While mathematical relations are composed of tiny inventive steps that build on each other, physical causality is not.  In nature, the total is not equal to the sum of the parts.  While natural phenomena may be described by mathematics, they are not themselves mathematical and the description is at best an approximation.  Even a system of lego bricks will usually not work out the way you you may have built it in your mind.  The more the system becomes complex, the more deviations can accumulate into unforeseeable effeects.  Undisturbed cleanrooms exist only in the world of ideas.")

(ML PWe "Patents on untechnical innovations pave the way to an abyss of total triviality.  They create a whole range of detrimental effects on the patent system.  They do not only break the requirement of inventivity.  They also lead to the proliferation of %(q:function claims), i.e. the patenting of %(q:technical effects).  This means that problems and not solutions are claimed.  Since no new causal chain between natural forces and a transformation of matter is involved, it becomes difficult for the patent applicant to define his %(q:invention).  If it lies in purely abstract computation, it cannot be claimed because that would mean laying claims to pure thinking.  If however a %(q:technical effect) of computation is claimed, there is no inventivity in the claim, since everybody knows that a computer can do it.  The only difficulty lies in knowing %(e:how to tell the computer to do it), and that is routinely left to the thousands of creative programmers, who, if allowed to do so, could independently devise hundreds of different creative solutions, all of which produce the claimed %(q:technical effect), but none or few of which are disclosed in the patent description.")

(ML Ton "Thus the introduction of software patents has not only killed the requirement of technicity but also that of enabling disclosure.  It has removed traditional restrictions on the permissibility of %(q:function claims) and thus overturned the balance of the patent system, leaving it behind in a state of disorder, inconsistency and moral bankruptcy.")

)

(ref (ML Fea "Further Reading" (de "Weitere Lektüre"))
(linklist
(l (ahs 'seofer-report) (ML Coi "Contains a long chapter on the triviality problem.  Quotes patent lawyers who are encouraging their customers to patent anything that seems useful and forget about the idea that patents are about inventions.  They are not.  Especially in software, you have to forget that old prejudice quickly, if you want to play the patent game."))
(dokdes 'nber-hallham99)
(dokdes 'hbr-thurow97)
(dokdes 'uplr-newell86)
(dokdes 'skk-patpruef)
(l (ahs 'patexam-corrupt) (ML Tsg "The author's indignation with the patent examination process leads him to sharply analyse many present-day problems.  Whether these problems can, as Aharonian suggests, be solved by a mere improvement of the examination process, seems questionable.  The indignation may be a result of wrong expectations due to the author's firm belief in the universal applicability of the patent system."))
(dokdes 'swpatbasti)
)
)
)
)

(mlhtdoc 'swpattrips nil nil nil
(sects
(swp (ML Dea "Does TRIPS require software patents?" (de "Verlangt TRIPS die Patentierbarkeit von Computerprogrammen?"))

(filters ((tr pet "TRIPS")) (ML TiW "The %(tr:Treaty on Trade Related Aspects of Intellectual Property Rights), signed on 1993-12-15 as a constituting document of the World Trade Organisation (WTO), sets minimal rules for national intellectual property law in order to prevent member nations from using intellectual property as a hidden trade barrier against other nations." (de "Das %(tr:Übereinkommen über handelsbezogene Aspekte der Rechte des Geistigen Eigentums) wurde am 15. Dezember 1993 im Rahmen der Gründung der Welthandelsorganisation (WTO) gegründet.  Es legt minimale Anforderungen für nationale Rechtssysteme fest, um %(q:sicherzustellen, daß die Maßnahmen und Verfahren zur Durchsetzung der Rechte des geistigen Eigentums nicht selbst zu Schranken für den rechtmäßigen Handel werden) (Präambel).")))

(ML AnW "Article 27 has often been construed by patent lawyers to imply that patent claims must be allowed to extend to computer programs." (de "Artikel 27 wird seit Mitte der 90er Jahre regelmäßig von Patentjuristen zitiert, um für einen Universalitätsanspruch des Patentwesens und insbesondere eine Ausweitung des Patentschutzes auf Computerprogramme und andere immaterielle Gegenstände zu argumentieren.")) 
(filters ((ph ah "http://www.patent.gov.uk/softpat/en/1000.html")) (ML PWh "Paul Hartnack, Comptroller General of the British Patent Office, %(ph:commented) this question at the London hearing in 1997:" (de "Hierzu %(ph:bemerkte) Paul Hartnack, Comptroller General des Britischen Patentamtes, 1997 bei der Londoner Anhörung über Softwarepatentierung:"))) 

(blockquote (al
(lin
 (ML Sfl "Some have argued that the TRIPS agreement requires us to grant patents for software because it says %(q:patents shall be available for any inventions in all field of technology, provided they are capable ... of industrial application)."
  (fr "Certaines personnes ont affirmé que les accords ADPIC nous obligent à attribuer des brevets logiciels parce qu'ils disent %(q:les brevets devront être attribués sur toutes les inventions ... dans tous les domaines de la technologie, pour peu qu'elles soient susceptibles ... d'application industrielle).")
  (de "Manche Leute haben behauptet, der TRIPS-Vertrag verpflichte uns, Patente auf Software zu erteilen, da darin steht %(q:Patente sollen für alle Erfindungen auf allen Gebieten der Technik erhältlich sein, sofern sie ... einer industriellen Anwendung zugänglich sind).")
)
 (ML Hde "However, it depends on how you interpret these words." (fr "Cependant, cela dépend de la façon dont on interprète ces mots.") (de "Es kommt aber darauf an, wie man diese Formulierung interpretiert.")) )

(lin (ML IWe "Is a piece of pure software an invention?" (fr "Est-ce qu'un simple fragment de logiciel constitue une invention?") (de "Ist ein einfaches Stück Programmtext eine Erfindung?"))
     (ML Eli "European law says it isn't." (fr "La loi européenne dit que non.") (de "Nach europäischem Recht ist es das nicht.")) )
(lin (ML Ioe "Is pure software technology?" (fr "Est-ce que le logiciel pur constitue une technologie? ") (de "Ist reine Programmlogik eine Technologie?"))
     (ML Moa "Many would say no." (fr "Nombreux sont ceux qui pensent que non.") (de "Viele würden das verneinen.")) )
(lin (ML Ifl "Is it capable of %(q:industrial) application?" (fr "Est-ce qu'il est capable d'application %(q:industrielle)?") (de "Ist sie der %(q:industriellen) Anwendung zugänglich?"))
     (ML Ahy "Again, for much software many would say no." (fr "Encore, pour beaucoup de logiciels nombrueux sont ceux qui disent que non.") (de "Wiederum würden, was einen Großteil der Software betrifft, viele das verneinen.")) )
(lin (ML Tnc "TRIPS is an argument for wider protection for software." (fr "Les accord ADPIC sont une occasion de renforcer la protection du logiciel.") (de "Der TRIPS-Vertrag lässt sich als Argument für eine erweiterten Patentschutz für Software anführen."))
     (ML BWW "But the decision to do so should be based on sound economic reasons." (fr "Mais la décision d'un tel renforcement doit être prise en se fondant sur des arguments économiques réfléchis.") (de "Aber die Entscheidung, dies zu tun, sollte auf soliden wirtschaftspolitischen Überlegungen beruhen.")) 
     (ML WWn "Would it be in the interests of European industry, and European consumers, to take this step?" (fr "Est-ce l'intérêt de l'industrie européenne, et des consommateurs européens, de faire un tel choix?") (de "Liegt es im Interesse der Europäischen Wirtschaft und der Europäischen Verbraucher, diesen Schritt zu unternehmen?")) )
))

(ML IiW2 "In its ratification of GATT/TRIPS, the German legislature saw no conflict between Sec. 1(2)(3) and 1(3) of the Patent Act and Art. 27(1) of TRIPS." (de "Bei der Ratifizierung von GATT/TRIPS wies der Deutsche Bundestag ausdrücklich darauf hin, das zwischen PatG §1 und TRIPS Art 27.1 kein Konflikt bestehe, da die Informatik nach europäischem Rechtsverständnis kein %(q:Gebiet der Technik) und Programmierideen keine Erfindungen seien."))
)

(art10 (colons (pf (ML TrAX "TRIPS Article %s" (de "TRIPS Artikel %s")) 10) (ML Caa "Computer Programs and Compilations of Data" (de "Computerprogramme und Zusammenstellungen von Daten")))

(ol
(filters ((bk pet "1971")) (ML Cby "Computer programs, whether in source or object code, shall be protected as literary works under the %(bk:Berne Convention)." (de "Computerprogramme, gleichviel, ob sie in Quellcode oder in Maschinenprogrammcode ausgedrückt sind, werden als Werke der Literatur nach der Berner Übereinkunft (1971) geschützt.")))
(ML Cnx "Compilations of data or other material, whether in machine readable or other form, which by reason of the selection or arrangement of their contents constitute intellectual creations shall be protected as such.  Such protection, which shall not extend to the data or material itself, shall be without prejudice to any copyright subsisting in the data or material itself." (de "Zusammenstellungen von Daten oder sonstigem Material, gleichviel, ob in maschinenlesbarer oder anderer Form, die aufgrund der Auswahl oder Anordnung ihres Inhalts geistige Schöpfungen bilden, werden als solche geschützt.  Dieser Schutz, der sich nicht auf die Daten oder das Material selbst erstreckt, gilt unbeschadet eines an den Daten oder dem Malerial selbst bestehenden Urheberrechts."))
)
)

(art27 (colons (pf (mlval 'TrAX) 27) (ML Plc "Patentable Subject Matter" (de "Patentfähige Gegenstände")))

(ol
(al
(tan
 (ML Sit "Subject to the provisions of paragraphs 2 and 3, patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application."
 (de "Vorbehaltlich der Absätze 2 und 3 ist vorzusehen, daß Patente für Erfindungen auf allen Gebieten der Technik erhältlich sind, sowohl für Erzeugnisse als auch für Verfahren, vorausgesetzt, daß sie neu sind, auf einer erfinderischen Tätigkeit beruhen und gewerblich anwendbar sind.")
)
 (colons 
  (ML Ftt "Footnote 5" (de "Fußnote 5"))
  (ML Fqy "For the purposes of this Article, the terms %(q:inventive step) and %(q:capable of industrial application) may be deemed by a Member to be synonymous with the terms %(q:non-obvious) and %(q:useful) respectively."
      (de "Im Sinne dieses Artikels kann ein Mitglied die Begriffe %(q:erfinderische Tätigkeit) und %(q:gewerblich anwendbar) als Synonyme der Begriffe %(q:nicht naheliegend) beziehungsweise %(q:nutzbar) auffassen.") )) )
(ML Sec "Subject to paragraph 4 of Article 65, paragraph 8 of Article 70 and paragraph 3 of this Article, patents shall be available and patent rights enjoyable without discrimination as to the place of invention, the field of technology and whether products are imported or locally produced." 
    (de "Vorbehaltlich des Artikels 65 Absatz 4, des Artikels 70 Absatz 8 und des Absatzes 3 dieses Artikels sind Patente erhältlich und können Patentrechte ausgeübt werden, ohne daß hinsichtlich des Ortes der Erfindung, des Gebiets der Technik oder danach, ob die Erzeugnisse eingeführt oder im Land hergestellt werden, diskriminiert werden darf.") ) )
   
(ML Mip "Members may exclude from patentability inventions, the prevention within their territory of the commercial exploitation of which is necessary to protect ordre public or morality, including to protect human, animal or plant life or health or to avoid serious prejudice to the environment, provided that such exclusion is not made merely because the exploitation is prohibited by their law."
    (de "Die Mitglieder können Erfindungen von der Patentierbarkeit ausschließen, wenn die Verhinderung ihrer gewerblichen Verwertung innerhalb ihres Hoheitsgebiets zum Schutz der öffentlichen Ordnung oder der guten Sitten einschließlich des Schutzes des Lebens oder der Gesundheit von Menschen, Tieren oder Pflanzen oder zur Vermeidung einer ernsten Schädigung der Umwelt notwendig ist, vorausgesetzt, daß ein solcher Ausschluß nicht nur deshalb vorgenommen wird, weil die Verwertung durch ihr Recht verboten ist.") )
   
(linol
 (ML Msm "Members may also exclude from patentability:"
     (de "Die Mitglieder können von der Patentierbarkeit auch ausschließen"))
 (ML dsa "diagnostic, therapeutic and surgical methods for the treatment of humans or animals;"
     (de "diagnostische, therapeutische und chirurgische Verfahren für die
Behandlung von Menschen oder Tieren;")
     )
 (ML pdm "plants and animals other than micro-organisms, and essentially biological processes for the production of plants or animals other than non-biological and microbiological processes. However, Members shall provide for the protection of plant varieties either by patents or by an effective sui generis system or by any combination thereof. The provisions of this subparagraph shall be reviewed four years after the date of entry into force of the WTO Agreement."
(de "Pflanzen und Tiere, mit Ausnahme von Mikroorganismen, und im wesentlichen biologische Verfahren für die Züchtung von Pflanzen oder Tieren mit Ausnahme von nicht biologischen und mikrobiologischen Verfahren. Die Mitglieder sehen jedoch den Schutz von Pflanzensorten entweder durch Patente oder durch ein wirksames System sui generis oder durch eine Kombination beider vor. Die Bestimmungen dieses Buchstabens werden vier Jahre nach dem Inkrafttreten des WTO-Übereinkommens überprüft.") ) ) ) 
;art27
)

(etc (ML Fes "Futher Issues" (de "Weitere Probleme"))
(ML Tai "Article 33 mandates that all patents must have a minimum duration of 20 years." (de "Artikel 33 verlangt, dass alle Patente eine Mindestlaufzeit von 20 Jahren haben müssen."))
(ML Ttr "This is important to know, because it renders frequently recurring proposals pointless, such as that of Amazon's CEO Jeff Bezos, who advocates reducing the lifetime of software patents to 3-5 years." (de "Dies ist wichtig zu wissen, denn es erübrigt wohlmeinende Vorschläge wie den von Amazon-Gründer Jeff Bezos, man solle die Patentlaufzeit im Softwarebereich auf 3-5 Jahre kürzen."))
(ML Igl "It may also be asked whether laying claim to immaterial objects such computing logic, programming interfaces and organisational methods does not constitute a form of trade monopoly, which is contrary to the spirit of TRIPS and to the letter of earlier free trade agreements.  E.g. the GATT treaty of 1947 art II.4 prohibits the erection of trade monopolies." (de "Gleichzeitig ist zu fragen, ob die Beanspruchung immaterieller Gegenstände aus dem Bereich der Programmlogik, der Kommunikationsschnittstellen und der Organisationsmethoden nicht dazu geeignet ist, Handelsbarrieren aufzubauen, die im Gegensatz zum Geist von TRIPS und anderen Freihandels-Abkommen stehen.  Z.B. verbietet der GATT-Vertrag von 1947 in Art II.4 die Errichtung von Handelsmonopolen."))
(ML IWn "In general, the TRIPS treaty has been used as a basis for claiming the universality of the patent system and supporting its expansion beyond the scope permitted by the European patent law tradition.  It does not contains several provisions that set a minimum requirement for the scope of patentability, but none that fixes an upper limit.  These TRIPS provisions may be considered as a brilliant coup by a few IP experts in international diplomatic rounds who succeeded in inadvertently piggybacking their community's prevalent patent universalism ideology onto what was supposed to be a free trade treaty.  TRIPS has therefore little legitimacy and should not be copied into the EPC or allowed to become European community law by means of EU harmonisation directives.  On the contrary, Europe should find ways for correcting all patent universalist tendencies in TRIPS and other international treaties at the soonest possible date." (de "Der TRIPS-Vertrag wurde seit 1994 ständig als Argument zur Ausweitung des europäischen Patentwesens über seine traditionellen Grenzen hinaus verwendet.  Es setzt dem Patentwesen allerlei Untergrenzen aber ausdrücklich keinerlei Obergrenzen (Art. 1).  Er mag wie ein brillianter Handstreich einer Gruppe von Patentjuristen erscheinen, denen es auf dem internationalen diplomatischen Parkett gelang, weitgehend unbemerkt die patentuniversalistische Ideologie ihres Berufsstandes in einen Freihandelsvertrag hineinzuschmuggeln.  Bis 2000 wollen Europas tonangebende Patentjuristen die patentuniversalistischen TRIPS-Klauseln in das EPÜ umschreiben und überdies dem TRIPS-Vertrag selber auf dem Wege einer EU-Richtlinie zu gemeinschaftsrechtlicher Geltung verhelfen.  Europas Politiker sollten sich diesen Bestrebungen der Patentjuristen widersetzen und umgekehrt Wege zu einer baldigen Korrektur der patentuniversalistischen Tendenzen des TRIPS-Vertrages und anderer internationaler Verträge finden."))
;etc
)

(ref (ML FhT "Further Texts" (de "Weitere Artikel"))
(dl 
 (l (ahs 'iic-schiuma00) (ML Tea "The argumentation for universal patentability is based on a mixture of the TRIPS fallacy with some unreflected ideology.  As a side-effect, the article reveals some interesting details, such as the fact that the German Parliament ratified TRIPS under the explicit assumption that TRIPS does not mandate software patentability" (de "Diese Argumentation für ein weltweit einheitliches universelles Patentwesen stützt sich leider nur auf das TRIPS-Scheinargument in Kombination mit allerlei unreflektierten Glaubenssätzen.  Beiläufig verrät der Artikel aber interessante historische Details, wie etwa die Tatsache, dass der Deutsche Bundestag den TRIPS-Vertrag unter der expliziten Annahme unterzeichnete, dieser Vertrag fordere nicht die Patentierbarkeit von Software.")))
 (l (ahs 'smets-agenda "The Hidden Agenda of the European Commission") (ML TpW "The European Commission's patent expansion plans are based on a %(q:consistent network of fallacies), including the TRIPS fallacy." (de "Die Pläne des EPA und der Europäischen Kommission beruhen auf einem %(q:in sich stimmigen Geflecht von Scheinargumenten), unter denen das TRIPS-Scheinargument nie fehlen darf.")))
)
;ref
)
;sects
)
;swpattrips
)
;swpstidi-dir
)

(list 'swpatxatra-dir

(mlhtdoc 'swpatxatra nil nil nil
(filters ((pc ahs 'swpatpikci) (mb ahs 'nopatltr) (eb ahs 'eulux-letter)) (ML Vef "Various organisations have published %(pc:anti swpat petitions) which collected tens of thousands of signatures and aroused some attention in the press.  Our %(mb:letter template) is being used in the %(eb:Eurolinux Letter Generator), which makes it easy for anyone to put together letters to decisionmakers and print them so that they just need to be inserted into a window envelope." (de "Verschiedne Organisationen haben bereits %(pc:Petitionen) zu diesem Thema ins Netz gestellt, die dank zehntausenden von Unterschriften in der Presse einige Beachtung gefunden haben.  Unser %(mb:Musterbrief) fand Eingang in den %(eb:Eurolinux-Brieferzeugungsautomaten), der es jedem erlaubt, Briefe an Entscheider zusammenzustellen und fensterumschlaggerecht auszudrucken.") (fr "Il y a eu déja pas mal de petition et lettres de ce genre.  Notre %(mb:lettre modèle) est utilisé dans le %(eb:générateur de lettres Eurolinux) qui peut vous faciliter le travail.")))
(docmenu-large '(swxpatg2C swxepue28 nopatltr eulojban miertltr swpatkarni))
(subsection (ML Bue "Letters from other sources" (de "Briefe aus anderen Quellen") (fr "Autre Lettres"))
(linklist
(l (ahs 'karn-patents) (ML KWW "contains some very knowledgable and moving letters from an experienced telecom programmer to American politicians" (de "Kenntnisreiche und bewegende Briefe eines erfahrenen Programmierers an diverse amerikanische Abgeordnete.") (fr "des lettres émouvant et compétents d'un programmeur de télécommunication a des politiciens américains")))
(l (ahs 'eulux-consultation) (ML csi "about 1000 submissions of European programmers to the European Commission" (de "ca 1000 Eingaben europäischer Programmierer an die Europäische Kommission") (fr "environ 1000 submission de programmeurs Européens a la Commission Européenne")))
)
)
)

(mlhtdoc 'nopatltr nil nil nil
(filters ((eb ahs 'eulux-letter) (pd ahs 'swpatprina)) (let ((OL (ahs 'swxpatg2C))) (ML DWe "Der %(eb:Eurolinux-Brieferzeugungsautomat) könnte Ihnen die Arbeit dabei noch leichter machen.  Es empfiehlt sich, ferner einige %(pd:druckbare Dokumente) in den Umschlag zu stecken, wie z.B. unsere aktuellen %{OL}." "The %(eb:Eurolinux Letter Generator) could further facilitate your work.  It would moreover be recommendable to insert a few %(pd:printable documents) into the cover, e.g. our latest %{OL}.")) )

hline


(ML Sgt "Sehr geehrte..." "Dear ..." "Cher ...") 

(ML Ien "In den letzten Jahren hat das Europäische Patentamt (EPA) zehntausende von unverdienten und gesetzeswidrigen Patenten auf Programmierideen, Geschäftsmodelle und geistige Methoden erteilt.  Dieses Patentgerümpel vermint unser Arbeitsumfeld und vergiftet die Infrastruktur der Informationsgesellschaft.  Nun drängt das EPA drängt Europas Regierungen, neue Gesetze zu erlassen und alte zu ändern, um seine unwürdige Praxis im nachhinein zu legalisieren." "During the last few years, the European Patent Office (EPO) has granted tens of thousands of undeserved and illegal patents on programming ideas, business models, mathematical and intellectual methods.  This patent crap is turning our working environment into a minefield and poisoning the infrastructure of the information society.  Now the EPO is pressing Europe's governments to decree new laws and change old ones, so as to legalise its despicable practise.")

(ML Web "Wir bitten Sie, Ihren Einfluss bei der anstehenden Entscheidungsfindung auf europäischer Ebene geltend zu machen, um zu verlangen, dass" "Please use your leverage during upcoming consultations at the European level to demand that")

(ol
(ML AWs "Artikel 52 des Europäischen Patentübereinkommens bis auf weiteres nicht angerührt wird." "article 52 of the European Patent Convention be left untouched for the time being.")

(filters ((kl ahs 'swpateurili)) (ML thW "die Begriffe %(q:Technik), %(q:gewerbliche Anwendbarkeit) und %(q:Computerprogramm als solches) dahingehend %(kl:geklärt) werden, dass logische Verfahren und Erzeugnisse (Algorithmen und Programme auf Diskette) nicht patentierbar sind, während physische Erzeugnisse mit dazugehörigen Verfahren patentierbar sein können, soweit sie einen erfinderischen Beitrag zum Stand der Technik leisten." "the terms %(q:technical character), %(q:industrial application) and %(q:computer program as such) be %(kl:clarified) in such a way, that logical processes and products (such as algorithms or software on disk) are not patentable, while a physical product with corresponding processes may be patentable, if they constitute an inventive contribution to the state of the art."))
 
(ML dcm "der öffentliche Dialog über die Auswirkungen des Patentsystems auf die Informationswirtschaft und -gesellschaft verbreitert und vertieft wird" "A broad public dialogue involving in-depth scientific studies of the impact of the patent system on the information economy and society, as recently initiated by the German Ministery of Economics and Technology, be commenced and intensified on all levels.")

(ML dbU "der Stand der Technik systematisch dokumentiert und in Form quelloffener Datenbanksysteme gemeinschaftlich weiterentwickelt wird, damit Kleine und Mittlere Unternehmen für die Patentschlacht daheim und in Übersee besser gerüstet werden." "A prior art database system be built and made available publicly under OpenSource terms, so as to put SMEs in an stronger position for patent litigation at home and abroad.")

(filters ((os ahv 'osslaw-articles)) (ML enj "ein %(q:Recht, selbst erarbeitete Informationswerke zu verbreiten) und ein %(os:Recht auf Interoperabilität) auf Verfassungsebene kodifiziert wird, um zukünftigen Begehrlichkeiten des Patentwesens klare Grenzen zu setzen." "A %(q:right to distribute one's own informational works) and a %(os:right to interoperability) be enacted at a constitutional level and be given priority over any cupidities of the European Patent Organisations."))

)

(ML Sft "Wir wären Ihnen sehr verbunden, wenn Sie sich bereit fänden, das der Patentlobby zu widerstehen und das Gemeinwohl zu verteidigen." "We would feel very obliged to you if you stand up against the patent lobby and defend the public interest.")

(ML Hcg "Hochachtungsvoll" "Sincerely")

(sqbrace (ML Urr "Unterschrift" "signature"))

)

(list 'miert
(let ((print-abstract t))
(mlhtdoc 'miertltr nil nil nil
(sects
(alv (ML SrW "Sehr geehrter Herr van Miert!" (en "Dear Sir!") (fr "Monsieur,") (eo "Kara sinjoro Van Miert") (it "Egregio Signore,"))

(ML Ser "Sie haben in den letzten Monaten und Jahren immer wieder Ihrer Sorge über Monopolgefahren im Softwarebereich öffentlich Ausdruck verliehen.  Noch auf der Berliner Kartellkonferenz von Anfang Mai nannten Sie das Beispiel Microsoft und führten treffend aus, dass nicht so sehr die Unternehmensgröße sondern die Herrschaft über Kommunikationsschnittstellen eine Gefahr darstelle.  Sie versicherten dabei, Ihre Behörde beobachte diese Gefahr sehr aufmerksam." (en "Many times during recent months and years, you have publicly expressed your concern about monopoly dangers in the software area.  This month, at the Berlin kartell conference, you mentioned Microsoft as an example and correctly stated that it is not the size of the company but the dominance of communication interfaces which is dangerous.  You reassured us that your establishment is keeping a close eye on this danger.") (fr "Depuis ces derniers mois et dernières années, vous avez maintes fois exprimé publiquement vos craintes concernant le danger de monopolisation dans le domaine du logiciel. Encore au début de ce mois de mai lors de la conférence des cartels de Berlin, vous avez mentionné l`exemple de Microsoft et vous avez démontré justement que ce n`est pas la taille de l`entreprise, mais la domination sur les jonctions de communication qui présente un danger. Vous nous assuriez que votre institution s`applique à observer attentivement ce danger.") (eo "Plurfoje en la lastaj monatoj vi esprimis vian zorgon pri la monopoldanøeroj en la programara sektoro.  Ankoraý sur la Berlina kartelkonferenco de komenco de Majo vi nomcitis la ekzemplon de Mikrosofto kaj trafe eksplikis, ke danøeron kaýzas ne tiom la grandeco de la enterpreno kiom øia dominado de komunikaj interfacoj.  Vi nin certigis ke via oficejo tre atente observas tiun æi danåron.") (it "Negli ultimi mesi ed anni, Lei ha più volte espresso in pubblico la Sua preoccupazione riguardo ai pericoli di monopolizzazione nel campo dei programmi per elaboratore. Questo mese, nel corso della conferenza di Berlino, Lei ha citato l'esempio di Microsoft, mostrando giustamente come non sia la dimensione dell'impresa a costituire un pericolo, bensì il suo dominio sulle interfacce di comunicazione. Lei ci ha assicurati che la Sua istituzione sta tenendo d'occhio il problema."))

(ML Ltt "Leider scheint es aber in der EU Kollegen zu geben, die Ihr Gefahrenbewusstsein nicht teilen.  Es werden derzeit Maßnahmen geplant, die auf eine Potenzierung der schon jetzt für Europa sehr nachteilhaften Monopoltendenzen in der Softwarebranche hinauslaufen." (en "Unfortunately there seem to be some colleagues in the EU who do not share your danger consciousness.  They are currently planning measures which can only result in aggravating the anti-competitive tendencies of the software industry and its heavy bias in favor of very few, mainly American, software giants.") (fr "Hélas, il semble qu`au sein de l`UE certains collègues ne partagent pas votre conscience du danger. La commission de l`UE prévoit en ce moment des mesures qui risquent d`aggraver encore plus les tendances de monopolisation du logiciel déjà existantes et très désavantageuses pour les Européens.") (eo "Sed bedaýrinde en la EU þajnas ekzisti kolegoj kiuj ne kunhavas vian danøer-konsciecon.  Oni planas tiuæitempe leødecidojn kiuj konsekvencos al multobligo de la jam nun por Eýropo tre malavantaøaj monopoltendencoj en la programara sektoro.") (it "Sfortunatamente, sembra che alcuni colleghi in seno all'UE non condividano la Sua percezione del pericolo. La commissione dell'UE sta pianificando delle misure che non possono che aggravare le tendenze anti-competitive dell'industria del software, a solo vantaggio di pochissime grandi compagnie americane."))

(ML Aie "Am 24. Juni soll auf der Konferenz der Europäischen Patentorganisationen in Paris ein Gesetzesentwurf vorgelegt werden, der in Europa ein %(q:Softwarepatent)-System nach amerikanischem Vorbild einführt." (en "On June 24th a legislative proposal is to be presented at the Intellectual Property Conference in Paris, according to which a US-like %(q:software patent) system is to be introduced in Europe.") (fr "Le 24 juin, une proposition de loi devrait être présentée lors de la conférence sur la propriété intellectuelle à Paris introduisant ainsi en Europe un système de %(q:brevets de logiciel) à l`américaine.") (eo "Je 1999-06-24 sur konferenco pri Intelekta Poseda¼aro en Parizo oni prezentos leøproponon, kiu enkondukos en Eýropon %(q:programarpatentan) sistemon lau usona modelo.") (it "Il 24 giugno deve essere presentata, all Conferenza sulla Proprietà Intellettuale di Parigi, una proposta legislativa che tende ad introdurre anche in Europa un sistema di %(q:brevetti sul software) all'americana."))

(ML IWd "In der Softwarebranche herrschen besonders deshalb seit Jahrzehnten besonders krasse Formen der Wettbewerbsverhinderung, weil Softwarefirmen, anders als traditionelle Publikations- oder Industrieunternehmen, besonders viele Schutzmittel auf einmal in Anspruch nehmen, z.B.:" (en "To appreciate this danger, we must first understand why the software industry has been plagued for decades by especially gross anticompetitive behaviour.  The reason, we believe, lies in its heavy use of protective measures, which, unlike in the traditional manufacturing and publishing industries, are all used simultaneously and cumulatively:") (fr "Si depuis ces dernières décénnies on constate un durcissement accru de la monopolisation, c`est parce que les firmes de logiciels contrairement aux firmes traditionnelles de publication ou de firmes industrielles, emploient beaucoup de mesures protectrices en même temps comme par exemple:") (eo "Kial tio estas danøera?  Oni unue pripensu, kial jam dum multaj jardekoj en la programara sektoro regas precipe akraj formoj de konkurenc-malhelpo.  Laý ni la æefa kaýzo estas, ke la programarfirmoj, malsame kun tradiciaj publikigaj aý industriaj enterprenoj, uzas por si jam tre multajn protektilojn samtempe kaj kumule, ekzemple:") (it "Per comprendere questo pericolo, dobbiamo innanzitutto capire perchè l'industria del software è stata afflitta per decenni da comportamenti gravemente anti-competitivi. A nostro parere, ciò è dovuto al suo pesante uso di misure di protezione che, a differenza che nelle industrie manifatturiere ed editoriali tradizionali, vengono usate simultaneamente e cumulativamente:"))

(ol
(tpe (ML Uae "Urheberrecht und daraus abgeleitete Verwendungsbeschränkungen" (en "Authorship Rights and utilization restrictions derived therefrom") (fr "droits d4auteur et les restrictions d`utilisation en résultant") (eo "verkintrajtoj kaj de tiuj derivataj uzlimigoj") (it "Diritti d'autore e conseguenti restrizioni all'utilizzo")) (ML zbw "z.B. Kopierverbot, Weiterentwicklungsverbot" (en "e.g. restrictions on copying and modification") (fr "par exemple interdiction de copier et de développement") (eo "ekzemple malpermeso de kopiado aý pludisvolvigo") (it "per esempio, restrizioni sulla copiatura e sulla modifica")))
(tpe (ML Beh "Betriebsgeheimnis" (en "Trade Secret") (fr "secret d`entreprise") (eo "enterprena sekreto") (it "Segreto industriale")) (ML Vxs "Vorenthalten des Quelltextes, Verwendung undurchsichtiger Datenformate etc" (en "Withholding of source code, use of opaque data formats etc") (fr "retenue des sources, utilisation de formats de données insondables etc.") (eo "kaþo de fontoteksto, uzo de maltransparentaj datformatoj ktp") (it "Rifiuto di rivelare il codice sorgente, uso di formati oscuri per i dati, ecc.")))
(tpe (ML Poa "Plattformstrategie" (en "Platform Strategy") (fr "stratégie de plateforme") (eo "platformstrategio") (it "Strategia di piattaforma")) (ML Ftu "Festverdrahtung verschiedenartiger Systeme untereinander zu Einheitsplattformen, Beschränkung der Interoperabilität und damit der Möglichkeit unabhängiger Hersteller, aufgrund technischer Leistungen zu konkurrieren" (en "hard-linking of diverse systems together into unified and mutually exclusive platforms, limitation of interoperability, barring the way for independent manufacturers to compete on the basis of technical merit") (fr "lier entre eux des systèmes de nature différente en une plateforme commune, limiter l`interopérabilité et par là, limiter la possibilité des fabricants indépendants d`entrer en concurrence grâce à leur compétence technique") (eo "durkunigado de malsamaj sistemoj en unuecan platformon, limigo de interoperebleco kaj sekve la ebleco de sendependaj produktantoj konkurenci surbaze de teknika boneco.") (it "connessione di sistemi diversi in una piattaforma comune, limitazione dell'interoperabilità, con la conseguenza di impedire ai fabbricanti  indipendenti di entrare in concorrenza con la loro competenza tecnica")))
)

(ML Wmk "Wenn sich nun hierzu auch noch Patente auf Programmierideen als zusätzliches Schutzmittel gesellen, kann das die Kartellsituation nur noch verschärfen." (en "If now patents on programming ideas are introduced as an additional protective measure, this can only aggravate the anticompetitive situation.") (fr "Si on ajoute à cela des brevets sur idées de programmation pour une protection supplémentaire, on aggrave la situation de monopolisation.") (eo "Se nun al tio aliøas ankoraý programadkonceptaj patentoj kiel kroma protektilo, tio povas nur pliakrigi la konkurencmalhelpan situacion.") (it "Se ora vengono introdotti i brevetti sul software come ulteriore misura protettiva, ciò non può che aggravare la situazione di momopolio."))

(filters ((wc ah "http://www.w3.org/1999/05/P3P-PatentPressRelease.html")) (ML Ien "In den letzten Monaten haben Microsoft und andere Firmen sich Patentrechte auf Internet-Standards gesichert und damit %(wc:das Genfer World-Wide-Web-Konsortium W3C vor eine Zerreißprobe gestellt).  Zum Glück gilt dieser Patentschutz bisher nicht für Europa." (en "During recent months, Microsoft and other companies have secured themselves patent rights to Internet standards and, in doing so, %(wc:brought the Geneva World Wide Web Consortium W3C to the brink of disruption).") (fr "Durant ces derniers mois, Microsoft et d`autres firmes se sont assurées des droits par brevet sur les standards d`Internet et de ce fait %(wc:le consortium Genevois World-Wide-Web W3C a subi une épreuve de force difficile). Heureusement, ces brevets ne sonts pas valable pour l`Europe.") (eo "En la lastaj monatoj Mikrosofto kaj aliaj firmaoj akiris patentrajtojn pri interretaj normoj kaj tiuvoje %(wc:subdanøerigis la pluekziston de la Øeneva retnormigejon W3C).  Bonþance tiu patentprotektado øis nun ne validas por Eýropo.") (it "Negli ultimi mesi, Microsoft ed altre compagnie si sono assicurate i diritti di brevetto sugli standard Internet, ed in tal modo %(wc:hanno portato il Consorzio per il World Wide Web W3C di Ginevra sull'orlo della disgregazione).")))

(ML Bto "Bisher haben die EU-Länder den Patentschutz für Software strikt abgelehnt, soweit es sich um reine Informationswerke und nicht um Teile von industriellen Apparaten handelt.  Anders als Maschinen, Chemikalien und sonstige Gegenstände des herkömmlichen Patentschutzes, ist ein Computerprogramm nämlich zunächst, ähnlich wie ein wissenschaftlicher Artikel, nicht ein industrielles Produkt, sondern eine formalsprachliche Beschreibung von Ideen und Handlungsanweisungen.  Erst in einem zweiten Schritt kann man das reine Informationswerk zum Quasi-Industrieprodukt reduzieren, indem man es undurchsichtig macht und sein Entwicklungspotential einschränkt." (en "So far the EU countries have strictly refused to grant patent protection for software, as far as pure information works and not parts of industrial machinery are concerned.  This is because, unlike with machines, chemicals and other objects of traditional patent protection, a computer program is not an industrial product but a description of ideas and instructions in a formalized language, similar to a scientific thesis.  Only in a second step can one transform the pure information work into a virtual industry product by compiling it into an opaque machine code and removing its potential for development.") (fr "Jusqu`à présent, les pays de l`UE avaient toujours strictement refusé la protection par des brevets de logiciels dans la mesure où il s`agit purement de matériel d`information et non de pièces d`appareils industriels. À l`opposé des machines, produits chimiques et autres objets tombant sous le droit de brevet, un logiciel n`est autre qu`une oevre d'information, und déscription formelle d`idées et de modes d`emploi comparable à un article scientifique. C`est seulement au second degré que l`on peut quasiment réduire l'oevre informatique à un produit industriel en le rendant impénétrable et en limitant son potentiel de développement.") (eo "Øis nun la EU-landoj strikte malakceptis patentprotektadon por programaroj, tiugrade ke traktiøas pri puraj informverkoj kaj ne pri partoj de industriaj aparatoj.  Malsame kun maþinoj, ¶emia¼oj kaj aliaj objektoj de klasika patentprotektado, komputila programo unue nur estas formlingva priskribo de ideoj kaj agad-instrukcioj.  Nur per dua paþo oni povas redukti la informverkon al industriprodukto, igante øin maltransparenta kaj malpli disvolvigebla.") (it "Finora, i Paesi dell'UE hanno sempre rifiutato la protezione del software mediante brevetto, nella misura in cui sia coinvolto un puro lavoro informatico e non parti di apparecchiature industriali. Ciò perchè, a differenza delle macchine, dei prodotti chimici e di quant'altro cade sotto la giurisdizione dei brevetti tradizionali, un programma per elaboratore non è un prodotto  industriale, bensì la descrizione di idee ed istruzioni in un linguaggio astratto, non dissimile da una tesi scientifica. Solamente in un secondo stadio la pura opera informatica può essere trasformata in prodotto industriale compilandola in un codice oggetto opaco e rimuovendo così il suo potenziale di sviluppo."))

(let ((GF (etc (ah "http://www.gnu.org" "GNU/Linux") (ah "http://www.freebsd.org" "FreeBSD")))) (ML Dln "Die Weisheit des europäischen Ansatzes, für Programmiermethoden keine Patente zu vergeben, hat sich in den letzten Jahren besonders durch die steigende Bedeutung der %(e:quellenoffenen Software) gezeigt.  Systeme wie %{GF} sind aus der freien Kommunikation von Fachleuten im Internet entstanden und haben durch ihre Leistungsfähigkeit und Stabilität viele bekannte industrielle Erzeugnisse in den Schatten gestellt.  Ein neues post-industrielles Modell der Software-Entwicklung hat seine überlegene Produktivität bewiesen und sich inzwischen schon in vielen Bereichen des öffentlichen Lebens fest etabliert." (en "The wisdom of the European approach to accord no patent rights for programming methods has, during recent years, been shown by the increasing significance of %(e:open-source software).  Systems like %{GF} have grown out of the unrestricted communication of specialists in the Internet and have, by their computing power and stability, outperformed many well-known industrial products.  A new post-industrial mode of software development has shown its superior productivity and has become an integral part of public life.") (fr "La sagesse de l`approche européenne de ne pas accorder de brevets aux méthodes de programmation s`est avérée ces dernières années particulièrement par l`importance croissante des %(e:logiciels à source ouverte). Des systèmes comme %{GF} se sont développés en raison de la libre communication des experts dans l`Internet et se sont montrés supérieur à beaucoup de produits industriels connus, grâce à leur efficacité et leur stabilité. Un nouveau modèle post-industriel du développement des logiciels a prouvé sa productivité supérieure et s`est déjà ábli entre temps dans beaucoup de domaines de la vie publique.") (eo "La saøeco de la eýropa farmaniero de ne doni patentojn por programadmetodoj montriøis dum la lastaj jaroj per la altiøanta signifo de %(e:malkaþfontaj programaroj).  Sistemoj kiel %{GF} estiøis graý la libera komunikado de fakuloj en Interreto kaj per sia komputforteco kaj stabileco enombrigis multajn agnoskitajn industriajn programarojn.  Nova post-industria modelo de programardisvolvigo montris sian superan produktemon kaj jam etabliøis en multaj domajnoj de nia publika vivo.") (it "La saggezza della scelta europea di non concedere brevetti per metodi di programmazione è stata dimostrata dalla crescente importanza, negli ultimi anni, del %(e:software open-source). Sistemi come %{GF} sono nati e cresciuti in virtù della libera comunicazione degli esperti su Internet, mostrandosi, quanto a potenza di calcolo e stabilità, superiori a molti ben noti prodotti industriali. Un nuovo modo post-industriale di sviluppo del software ha dimostrato la sua maggiore produttività ed è entrato a far parte della vita pubblica.")))

(ML Een "Es ist verständlich, dass einige große industrielle Softwareproduzenten der USA sich angesichts dieser neuen Entwicklung, an der Europa einen Anteil von ca 50% hat, Sorgen machen." (en "It is not surprising that some large industrial software producers of the USA have in recent years become concerned about this new tidal wave, in which Europe occupies a share of about 50%.") (fr "Pas étonnant que certains grands fabricants de logiciels aux États-Unis se font des soucis face à ce nouveau développement auquel l`Europe participe à environ 50%.") (eo "Kompreneble kelkaj grandaj usonaj industriaj programarproduktantoj zorgas pri tiu nova disvolviøo, en kiu Eýropo okupas porcion de 50%.") (it "Non c'è da stupirsi se negli ultimi anni alcuni grossi produttori americani di software hanno cominciato a preoccuparsi di questa ondata di produzione indipendente, in cui l'Europa occupa una quota intorno al 50%."))

(filters ((os ah "http://www.opensource.org/halloween.html")) (ML Inh "In einer %(os:internen Studie von Ende Oktober 1998) konstatiert denn auch ein Microsoft-Stratege, dass seine Firma rein auf der Ebene der Qualität mit quellenoffenen Systemen wie Linux und Apache kaum konkurrieren könne, da letztere im Internet %(q:unvergleichlich besser skalieren).  Daher rät er seiner Firma zum Einsatz von zwei Kampfmitteln aus dem Arsenal der industriellen Produktionsweise:" (en "Thus in an %(os:internal study of October 1998), a Microsoft strategist states that his company can hardly compete with systems like Linux and Apache at the quality level, since the latter %(q:scale much better) in the Internet.  Therefore he advises his company to employ two well-proven remedies specific to the industrial mode of production:") (fr "Dans une %(os:analyse interne de fin octobre 1998) un stratège de Microsoft constate que sa firme ne peut concurrencer, au niveau de la qualité, avec les systèmes à source ouverte tel que Linux et Apache puisque ces derniers %(q:s`ajustent bien mieux) dans l`Internet. Ainsi cet expert conseille à sa firme d`utiliser deux armes de l`arsénal de l`ancien mode de production industriel:") (eo "En %(os:interna priseræriporto de 1998-10a fino) ja konstatas mikrosofta strategisto, ke sia firmao malfacilas kvalite konkurenci kun malkaþfontaj sistemoj kiel Linux kaj Apache, æar tiuj æi %(q:skaliøas multe pli bone) en Interreto.  Tial la verkinto konsultas ke sia firmao uzu du batalilojn el la armilaro de la tradicia industria produktadfasono:") (it "In uno %(os:studio confidenziale dell'ottobre 1998), uno stratega della Microsoft asserisce che la sua compagnia non è in grado di competere a livello qualitativo contro sistemi come Apache e Linux, dato che questi ultimi %(q:vanno molto meglio) su Internet. Egli consiglia perciò la sua compagnia di utilizzare due ben sperimentate armi ereditate dal vecchio modo di produzione industriale:")))

(ol
(ML PuW "Proprietäre Erweiterung/Neuerfindung von Internetprotokollen" (en "proprietary extension/reinvention of Internet protocols") (fr "Élargissement/réinvention propriétaire de protocoles Internet") (eo "posedeca eltendo/reinvento de interretaj protokoloj") (it "estensione proprietaria o reinvenzione dei protocolli Internet"))
(ML FWS "Flächendeckende Anmeldung von Softwarepatenten" (en "extensive acquisition of software patents") (fr "Application extensive de brevets de logiciel") (eo "grandskala akiro de programarpatentoj") (it "acquisizione di brevetti software su larga scala")))

(let ((AO (kaj (ah "http://lpf.ai.mit.edu/Patents/testimony/statements/adobe.testimony.html" "Adobe") (ah "http://lpf.ai.mit.edu/Patents/testimony/statements/oracle.statement.html" "Oracle")))) (ML cea "%(q:Softwarepatente) sind auch in den USA Gegenstand heftiger Kontroverse. Anders als Industriepatente dienen sie kaum der Bekanntmachung von Neuerungen, bedrohen aber die im Softwarebereich ohnehin unvermeidliche tägliche Innovationstätigkeit mit einem Damoklesschwert.  Spezialisierte Pionierunternehmen wie %{AO} haben Softwarepatente als %(q:eher schädlich denn nützlich für die Innovation) bezeichnet.  Nur einige wenige Branchenriesen mit allumfassender Plattformstrategie sind in der Lage, von der durch Softwarepatente erzeugten allgemeinen Rechtsunsicherheit zu profitieren." (en "Software patents are subject to ardent controversy even in the USA.  Unlike industrial patents, they hardly serve as a source of information about innovations, but rather as an uncalculable liability for software programmers, who by the nature of their profession have to innovate every day.  Dedicated technological pioneering enterprises like %{AO} have expressed their view that software patents are more harmful than useful for innovation.  Only some giant corporations with an all-encompassing platform strategy are in a position to really profit from the general legal insecurity caused by software patents.") (fr "Les brevets de logiciel font aussi l`objet de fortes controverses aux États-Unis. Contrairement aux brevets industriels, ils ne servent guère à faire connaître des innovations, mais menacent l`inévitable innovation quotidienne comme une épée de Damoclès. Les entreprises spécialisées pionnières comme %{AO} ont exprimé leur opinion que %(q:les brevets de logiciel sont plutot étouffants que utils pour l`innovation). Seulement certains géants de cette branche avec une stratégie de plateformes générales sont en mesure de profiter de l`insécurité juridique due aux %(q:brevets de logiciels).") (eo "programaraj patentoj ankaý en Usono okazigas samopiniecon. Malsame kun industripatentoj ili apenaý helpas la disvastigadon de novaj teknikoj kaj kontraýe minacas la en tiu profesio tute ne eviteblan æiutagan plinovigon per damoklesa gladio.  Specialigitaj pionirenterprenoj kiel %{AO} dirintas ke %(q:programarpatentoj pli malhelpas ol helpas plinovigon).  Nur kelkaj enterprenegoj kun æionampleksanta platformstrategio povas klare profiti per la jura necerteco kaýzita per programadmetodaj patentoj.") (it "I brevetti sul software sono oggetto di grandi discussioni anche negli USA. A differenza dei brevetti tradizionali, essi servono non già a far conoscere le innovazioni, ma a minacciare come una spada di Damocle i programmatori, per la cui professione l'innovazione è pratica quotidiana. Imprese come %{AO}, che hanno svolto un lavoro da pionieri in questo campo, hanno espresso l'opinione che i brevetti sul software siano più dannosi che utili per l'innovazione. Soltanto pochi giganti del software con una strategia di piattaforma globale sono in grado di trarre profitto dalla situazione di incertezza giuridica causata dai brevetti sul software.")))

(ML Eoa "Eine plötzliche Stärkung solcher Plattformstrategien durch europäische Gesetzgeber würde die gewachsene, von kleiner Unternehmensgröße, Vielfalt und Innovationsfreude gekennzeichnete europäische Softwarekultur dem übermächtigen Druck technisch stagnierender aber juristisch bestens ausgerüsteter amerikanischer Giganten aussetzen und dadurch mehr zunichte machen, als ein EU-Wettbewerbskommissar in vielen Jahren aufmerksamen Wettbewerbsschutzes für Europa erreichen kann." (en "A sudden strengthening of such platform strategies by European legislators would subject the European software culture with its thousands of small, innovative enterprises, to crushing pressures of technologically stagnant but legally well-equipped American corporate giants and thereby destroy more than an EU competition commissioner can achieve for Europe during many years of attentive competition protection work.") (fr "Un renforcement de la stratégie de plateforme pratiquée par des législateurs de l`UE détruirait probablement plus qu`un commissaire de contrôle de la concurrence de l`UE peut obtenir pour l`Europe en plusieurs années de travail consciencieux sur la protection de la compétitivité.") (eo "Tuja plifortigo de usonaj platformstrategioj per EUaj leødonistoj probable neniogos plion ol æio kion povas aæevi EUa konkurenskomisiisto por Eýropio dum longaj jaroj de atenta konkurencprotektado.") (it "Un rafforzamento di tali strategie di piattaforma da parte dei legislatori europei assoggetterebbe le migliaia di piccole imprese innovative europee alla pressione dei giganti americani, stagnanti dal punto di vista tecnologico ma ben armati da quello legale, e distruggerebbe in poco tempo ben più di ciò che un commissario europeo di controllo sulla concorrenza possa sperare di realizzare in anni di lavoro coscienzioso."))

(let ((KG (ahs 'kongress))) (ML IId "In Anbetracht der Dringlichkeit dieser Angelegenheit wären wir Ihnen für eine baldige Antwort sehr dankbar.  Insbesondere würden wir gerne erfahren, was die EU genau plant, mit wem wir noch in Kontakt treten könnten und wen wir als Dialogpartner, nicht zuletzt für den am 13. Juni in Köln stattfindenden %{KG}, gewinnen könnten." (en "In view of the urgence of this matter we would be very grateful for an early response.  We would especially be interested to know what exactly the EU is planning, with whom we may seek contact and to whom we could relate as a dialog partner, e.g. for our %{KG} planned for June 13th in Cologne.") (fr "Vu l`urgence de cette affaire, nous vous serions très reconnaissant de nous répondre au plus vite. Nous aimerions surtout savoir ce que l`UE prévoit, qui nous pourrions encore contacter et qui pourrait être notre interlocuteur, notamment pour le %{KG} du 13 juin à Cologne.") (eo "Konsiderante la urøecon de tiuæi afero, ni tre dankemus por baldaýa respondo.  Speciale ni volus baldaý lerni, kion ekzakte planas la EU kaj kun kiu ni povus ankoraý kontaktiøi kaj kiun ni povus obteni kial dialogulo, ekzemple por la %{KG} okazonta je 1999-06-13 en Kolonjo.") (it "Vista l'urgenza della materia, Le saremmo grati di una risposta sollecita. Vorremmo in particolare sapere quali siano precisamente i piani dell'UE, chi  possiamo contattare al riguardo e chi potrebbeessere nostro interlocutore, per esempio riguardo al %{KG} del 13 giugno a Colonia.")))

(ML AwW "Auf diesem Kongress werden noch zwei weitere Gefährdungen der europäische Wettbewerbsordnung zur Sprache kommen:" (en "At this conference we will be dealing with two more threats to the European competition order:") (fr "Lors de ce congrès, nous parlerons aussi de deux autres dangers pour la compétitivité européenne:") (eo "Sur tiu kongreso pritraktatos ankoraý du aliaj danøeroj al eýropa konkurencordo:") (it "Questa conferenza tratterà pure di altre due minacce per la competitività europea:"))

(ol
(ahst 'ffiiclem (ML dnW "die geplante Kooperation des Landes Nordrhein-Westfalen mit Microsoft" (en "the planned coorperation of the German Land of Northrhine-Westfalia with Microsoft") (fr "la coopération prévue entre le Land Nordrhein-Westfalen et Microsoft") (eo "la planata kunlaboro de la þtato Nordrajn-Vestfalio kun Mikrosofto") (it "la cooperazione prevista tra il Land tedesco di Nordrhein-Westfalen e Microsoft")))
(ah "http://www.heise.de/newsticker/data/cp-12.05.99-004/" (ML don "die geplante Kooperation/Fusion der Deutschen Telekom mit Microsoft" (en "the planned cooperation/fusion between the successor of the German telekommunication state monopoly company with Microsoft") (fr "la fusion/coopération prévue entre Deutsche Telekom et Microsoft") (eo "la planata kuniøo de la Germana Telekomunikejo kun Mikrosofto") (it "la cooperazione/fusione prevista fra Deutsche Telekom e Microsoft")))
)

(ML Iie "In beiden Fällen versuchen Träger von mehr oder weniger öffentlichen Funktionen, sich als Wegbereiter eines ganz Europa betreffenden Kartells zu verdingen." (en "In both cases representatives of more or less public functions are promoting a monopoly system that affects all of Europe.") (fr "Dans les deux cas, des responsables de fonctions plus ou moins publiques essaient ensemble d`ouvrir la route à une monopolisation recouvrant toute l`Europe.") (eo "En la du kazoj plenumantoj de pli aý malpli publikaj funkcioj provas servi kiel vojgvidantoj de monopolo rilatanta al tuta Eýropio.") (it "In ambo i casi, rappresentanti di funzioni più o meno pubbliche stanno promuovendo un sistema monopolistico che riguarda l'intera Europa."))

(ML Soe "Solche regionalen Wettbewerbsstörungen sind aber noch harmlos im Vergleich zu den Verheerungen, die ein EU-weites Software-Patentwesen anrichten könnte." (en "Such regional anti-competitive measures are however harmless in comparison to the devastation that a pan-European software patent system is likely to cause.") (fr "Ces perturbations régionales de compétitivité sont bénignes comparées aux dévastations que pourrait causer un système de brevets de logiciel à l`échelle européenne.") (eo "Sed tiaj regionaj konkurenc-malhelpadoj estas maldanøeraj komparate kun la katastrofo kiun povus okazigi tut-eýropa programarpatenta sistemo.") (it "Tali iniziative monopolistiche regionali sono comunque un'inezia in confronto alla devastazione che può arrecare un sistema pan-europeo di brevetti sul software."))

(ML Wng "Wir wünschen Ihnen eine robuste Gesundheit und gute Inspiration für die großen Herausforderungen Ihres Amtes." (en "We wish you good health and good inspiration for the great challenges of your office.") (fr "Nous vous souhaitons une santé de fer et une bonne inspiration pour le grand défi de votre fonction.") (eo "Ni aýguras al vi robustan sanecon kaj bonan inspiron por la grandaj defioj de votra ofico.") (it "Le auguriamo una buona salute ed una buona ispirazione per fronteggiare la grande sfida che l'attende nel Suo lavoro."))
)

(mfg (ML Mue "Mit freundlichen Grüßen" (en "Sincerely yours") (fr "Sincères salutations") (eo "Kun koraj salutoj") (it "Distinti saluti"))

 (ahst 'miertsub  (ML Uer "Unterzeichnerliste" (en "list of signatories") (fr "Liste des signataires") (eo "Subskribantaro") (it "lista dei firmatari")))
(linul (ML Pem "P.S. Weitere detaillierte Informationen zu diesem Thema finden Sie unter" (en "P.S. For more detailed reading on this subject, we recommend") (fr "P.S. Vous trouverez des informations supplémentaires à ce sujet sous") (eo "P.S. Pli detalajn informojn pri tiu temo vi povos trovi sub") (it "P.S. Per maggiori detagli sull'argomento, raccomandiamo di consultare i seguenti siti:"))
 (ah2 "http://www.freepatents.org")
 (ah2 "http://swpat.ffii.org")
)
)
)))

(let ((print-abstract t))
(mlhtdoc 'schelter nil (filters ((km ahst 'miertltr) (bs ah "http://www.ma.utexas.edu/users/wfs")) (ML cmt "%(bs:Bill Schelter) comments on our %(km:letter to Karel van Miert)")) (l "comment" "software patents")

"I would like to sign your very well thought out letter on software patents.   I regard a europoean stance against patents in software as absolutely critical to further development of free software."

"If one wants to have all software dominated by microsoft, then europe should go ahead and allow patents.    Individuals while they can contribute enormous amounts of good software and ideas, do not have the inclination nor legal backing to start issuing patents.   And if they did they would not have the revenue dollars/euros to fight the software giants in the courts."

"One has already seen a shift in software development towards europe from the USA, in part because europe now has access to the internet which american researchers had already had for 15-20 years, but also in part due to the US patent and other legal restrictions which have begun to hamper development in the USA.  For example we have important code such as ssh written in europe, because it would not be allowed in the US."

"Europe must not shoot itself in the foot, bending to the desire of the USA to allow software patents."   

"If things keep up as they are, in 10 years it will be absolutely impossible for a student to write and distribute programs based on simple common sense.   In 10 years, starting something like Linux would be impossible, if the proponents of software patents get their way."
))

(mlhtdoc 'miertsub nil (filters ((km ahst 'miertltr)) (ML lfW "Liste der Unterzeichner des %(km:Briefes an EU-Wettbewerbskommissar Karel van Miert)" (en "list of signatories to the %(km:letter to EU competition commissioner Karel Van Miert)") (fr "liste des signataires du %(km:lettre au commissaire de la compétition de l'UE, Karel Van Miert)") (eo "listo de subskribantoj al %(km:letero al Karel Van Miert)"))) nil
(sects
(pref (ML PaR "Preparatory Remarks" (de "Vorbemerkungen") (fr "Avant-Propos") (eo "Antaýaj Rimarkoj"))
 (ML AWo "Alle Unterzeichner sprechen nur für sich selbst, nicht für ihre (z.T. erläuternd in Klammern angeführten) Arbeitgeber." (en "All signatories speak for themselves and not for their employers.") (fr "Tous les signataires s'expriment en leur propre nom et non au nom de leur employeur.") (eo "Æiuj subskribantoj reprezentas nur sin mem."))
 (ML Tun "Nach Paragraph 28 Abs. 3 Bundesdatenschutzgesetz untersagen wir der Nutzung oder Übermittlung dieser Unterzeichnerliste für Werbezwecke oder für die Markt- oder Meinungsforschung.  Die Internetadressen der Unterzeichner sollen ausschließlich Rückfragen an die betreffenden Personen erleichtern und so die Glaubwürdigkeit der Liste erhöhen." (en "The list may not be used for advertisement or market research.") (fr "Il est interdit d'utiliser la liste à des fins de publicité ou recherche de marché.") (eo "Ne permeseblas uzi la liston por publikeco au merkatpriseræo."))
)

(lst (ML Sos "Bisherige Unterzeichner" (en "Signatories so far") (fr "signataires jusqu'au présent") (eo "subskribantoj øis nun")) 
(labels ((land (sym) (mltr (landnom sym))))
(ul
 (ahs 'ffiiphm)
 (ahs 'piotros)
 (ahs 'breiter)
 (ahs 'fleck)
 (ahm "seyfertd@aol.com" (famvok "Seyfert" "Dirk"))
 (commas (ahm "efasel@cs.tu-berlin.de" (famvok "Fasel" "Elmar")) (mltr "Informatikstudent") (mltr "TU Berlin"))
 (nl (commas (ahm "jens.kolberg@acm.org" "Jens G. Kolberg") (ML MlS "M.SC." (de "Dipl.Ing."))) (blockquote "I am a danish software engineer." "I am very concerned about how the threat of software patents in the EU, could limit my personal freedom as a programmer." "I completely agree with your point of views."))
 (semicolons (ah "http://hugin.ldraw.org/Jacob/" "Jacob Sparre Andersen") "cand. scient." (commas (land 'dk) (mltr "2100 København Ø") (mltr "Tåsingegade 36, 3. tv.")))
 (semicolons (ahm "zl@stardiv.de" "LUO Zaoliang") (commas (ah "http://www.staroffice.de" "StarOffice Software Entwicklungs GmbH")))
 (semicolons (ah "http://tips.pair.com" "Rune Madsen") (landnom 'dk))
 (semicolons (ah "http://www.europe-inside.com" "Jean-Paul Smets-Solanes"))
 (let ((VIBE (ah "http://www.vibe.at" (ML Vtr "Verein für Internetbenutzer Österreichs"))) (PK (ahm "peter.kuhm@plus.at"))) (ML cre "%{VIBE}, vertreten durch %{PK}" (en "%{VIBE}, represented by %{PK}") (fr "%{VIBE}, representé par %{PK}")))
 (semicolons (ahm "mi.bauer@student.uni-tuebingen.de" "Michael Bauer") (ML pcu "Physikstudent" (en "physics student")) (let ((T (mltr "Tübingen"))) (ML UWc "Uni %{T}" (en "Univ of %{T}") (fr "Univ. de %{T}"))))
)))

(nov (ML Nio "Neuunterzeichner" (en "New Signatories") (fr "Nouveaux Signataires") (eo "novaj subskribantoj"))
 (filters ((sl ahst 'miertsiglist) (sf ahst 'miertsigform)) (ML Wll "We have finally installed a %(sl:signatory guestbook), which allows you to %(sf:sign online)." (de "Wir haben jetzt ein %(sl:Unterzeichner-Gästebuch) eingerichtet, in das Sie sich %(sf:eintragen) können.") (fr "Nous avons enfin installé un %(sl:carnet de signataires) par lequel vous pouvez %(sf:vous enregistrer).") (eo "Ni havas %(sl:gastlibron de subskribantoj), al kiun vi povos %(sf:vin aligi).")))
 (linul (ML YWr "Sie können uns auch einen Kommentar zu diesem Thema schicken, den wir gerne veröffentlichen oder referenzieren, wie z.B. die folgenden:" (en "You can also send us a comment, which we may wish to publish or link to, such as the following.") (fr "Vous pouvez nous envoyer un commentaire, que nous serions interéssé de publier our référencer, commes les suivants.") (eo "Vi povus ankaý verki artikolon, kiun ni øoje publikigos au alligos, kiel la sekvantajn:"))
  (ahs 'schelter)
 )
)
))

(mlhtdoc 'miertsign nil nil nil (indexmenu))
(mlhtdoc 'miertsigform nil (ML Efz "Exact entries into form fields help us to organize the campaign. Thanks!" (en "Bitte füllen Sie, soweit zutreffend, die verschiedenen Felder unten aus. Es dürfen Felder ausgelassen werden.  Durch genaue Angaben können Sie aber u.U. helfen, die Kampagne optimal zu organisieren.  Vielen Dank!") (fr "Vous pouvez ommettre des champs, mais par des specifications exactes vous augmentez la crédibilité.") (eo "vi povas ellasi nenecesajn kampojn.")) nil 
(filters ((mb ahm "blasum@ffii.org")) (let ((HB (famvok "Blasum" "Holger"))) (ML FeS "Falls z.B. Ihr Name nicht richtig erscheinen sollte, dann schreiben Sie bitte eine %(mb:Mail an %{HB})" (en "In case entries do not show up properly please %(mb:mail to %{HB})") (fr "Si on ne voit pas correctement veuillez %(mb:contacter %{HB})") (eo "Se oni ne øuste vidas viajn enskriba¼ojn, %(mb:kontaktu kun %{HB})."))))

(let ((cgi "sigbook.pl") (method "post")) (form
   (finhid 'urlpath "http://swpat.ffii.org/xatra/miert/sign")
   (finhid 'fspath "/var/www/swpat/xatra/miert/sign")
   (finhid 'logfile "sigdata.log")
   (finhid 'htmlfile (relurl 'miertsiglist))
   (finhid 'lang (string lang))
   (finhid 'subject title)
   (tdl
    (l (MLL Nam "Name") (tdl (l (ML Fln "family name" (de "Familienname") (eo "familinomo") (fr "nom de famille")) (ftext 'famname 29 (ML Mtm "Doe" (de "Mustermann") (fr "Bonhomme") (eo "Zamenhof")))) (l (ML Pon "Personenname" (en "personal name")) (ftext 'prename 20 (ML Eri "John" (de "Erika") (fr "Jean") (eo "Lazaro")))) (l (ML Rhf "ordo" (de "Reihenfolge") (en "sequence") (fr "ordre")) (fselect 'nomtip '(vokfam (tpe (ML FeW "given name first" (de "Familienname zuletzt") (fr "nom de famille aprés") (eo "familinomo antaýe")) (ML Wep "West" (de "Westeuropa etc") (fr "Europe Ouest") (eo "Okcidenta Eýropio")))) '(famvok (tpe (ML FeW2 "family name first" (de "Familienname zuerst") (fr "nom de famille avant") (eo "familinomo malantaýe")) (etc (ML Byr "Bavaria" (de "Bayern") (fr "Bavière") (eo "Bavario")) (ML Ugr "Hungaria" (de "Ungarn") (fr "Hongrie") (eo "Hungario")) (ML Ots "East Asia" (de "Ostasien") (fr "Asie d'Est") (eo "Orientazio")))))))))
   (l (ML Gce "sex" (de "Geschlecht") (eo "sekso")) (fselect 'mf '(m (ML wsa "m")) '(f (ML Xqq "f"))))
   (l (ML BWW "occupation" (de "Beruf oder Status") (eo "profesio")) (ftext 'job 40 (MLL Pri "Programmierer" "Programmer" "programmeur" "programisto")))
    (l (ML Fmi "affiliation" (de "Firma/Institution") (eo "organizo")) (nl (ftext 'affil 30 "Ekko GbR") (colons (MLL Wsn "Webadresse der Institution" "affiliation web address" "addresse web de l'organsation" "ttt-ejo de la organizo") (concat "http://" (ftext 'afurl 30 "www.ekko.de")))))
    (l (ML Tlf "Telefon" (en "phone")) (tdl (l (MLL tag "tags" "daytime" "journée" "tagmezo") (ftext 'phoneday 20 "089-12789608")) (l (MLL aed "abends" "evening" "soir" "vespero") (ftext 'phonehome 20 "0041-8834-9080"))))
    (l (MLL SrW "Schnellste Anschrift (EMail, Fax oder gelbe Post)" "Email or snailmail address" "l'addresse plus rapide") (ftext 'email 63 "us@ekko.de"))
    (l (MLL PlW "Persönliche Webseite" "personal webpage" "page web" "ttt-paøo") (concat "http://" (ftext 'url 50 "www.ekko.de/~us/")))
    (l (ML Kmt "komento" (de "Kommentar") (en "Comments") (fr "commentaires")) (textarea 'comments 40 5 (ML vce "venceremos!")))
)
    (fsubmit)
))
)
)

(mlhtdoc 'eulojban nil nil nil
(sects
(dekl (ML Elu "Declaration" (de "Erklärung"))
(let ((EL (ahst 'eurolinux "EuroLinux"))) (ML Wru "We want to support the lobbying effort against patentability of software as such, with a special emphasis on the following points:" (de "Wir wollen die Lobbyarbeit gegen Patentierbarkeit von Software als solcher unterstützen und dabei folgende Punkte besonders betonen:"))) 
(ol
(ML Aek "Abstract ideas and programming techniques should remain non-patentable.  The Logical Language has in the past been harrassed by claims of of ownership of ideas, and it would be a severe threat to the further development of open-source Logical Language parsing tools, if the new programming ideas, which inevitably occur during the development, could be patented." (de "Abstrakte Ideen und Programmiertechniken sollten unpatentierbar bleiben.  Das Projekt einer Logiksprache hat in der Vergangenheit unter den Besitzansprüchen seines Gründers an seinen Ideen gelitten, und es würde die künftige Entwicklung von quellenoffenen Analysewerkzeugen für logiksprachliche Texte schwer bedrohen, wenn neue Programmierideen, die während der Entwicklung zwangsläufig auftreten, patentiert werden könnten."))
(ML Pfe "Patent specifications should belong to the public domain.  They should be published in an open hypertext format of high abstraction power and placed under an open content license that allows anybody to rearrange and republish them under similar conditions.  This would create an open market for patent research software and services and thereby greatly reduce patent application costs." (de "Patentspezifikationen gehören der Öffentlichkeit.  Sie sollten in einem Hypertextformat mit hoher Abstraktionskraft veröffentlicht und unter eine Inhaltsoffenheit gewährleistende Lizenz gestellt werden.  Das schüfe einen offenen Markt für Patentrecherche-Software und -Dienstleistungen und würde die Patentanmeldungskosten deutlich senken."))
(ML ErW "EU citiziens should remain entitled to be informed in their own language about what they are allowed to do and what not.  Therefore patent specifications should be published in the languages of the states where they are to be enforcable." (de "EU-Bürger sollten weiterhin berechtigt sein, in ihrer eigenen Sprache darüber informiert zu werden, was sie tun dürfen und was nicht.  Daher sollten Patentbeschreibungen in den Sprachen der Staaten, in denen sie durchsetzbar sein sollen, veröffentlicht werden."))
(filters ((lb ah "http://www.lojban.org") (pa ant (filters ((lp ah "http://www.animal.helsinki.fi/lojban/parser.html")) (ML Tat "The %(lp:Lojban Reference Parser) works quite similar to the SGML reference parser")))) (ML NnW "Beside the national languages, it should be possible to submit patent specifications in a logical language like %(lb:Lojban).  A patent description in Logical Language could serve as a syntactically unambiguous reference version (for disputes arising from ambiguity) and as a source text for error-free machine-translation into all EU languages.  Also it would facilitate patent research, since a Logical Language can be seen as the final layer of hypertext markup that makes even the meaning of the text %(pa:parsable)." (de "Neben den nationalen Sprachen sollte es möglich sein, Patentbeschreibungen in einer Logiksprache wie %(lb:Lojban) einzureichen.  Eine logiksprachliche Patentbeschreibung könnte als syntaktisch eindeutige Referenzversion (für aus der Zweideutigkeit natürlicher Sprachen resultierende Streitigkeiten) und als ein Quelltext zur fehlerfreien maschinellen Übersetzung in alle EU-Sprachen dienen.  Sie würde auch Patentrecherchen erleichtern, denn eine Logiksprache ist quasi eine letzte Ebene der Hypertextauszeichnung, die auch noch den Sinn des Textes der maschinellen Analyse erschließt.")))
(ML Did "The EU should actively support the development of open-source software in areas that are essential for the public informational infrastructure.  The current tender practice of financing only projects that lead to proprietary products should be changed." (de "Die EU sollte die Entwicklung von quellenoffener Software in Bereichen, die für die öffentliche informationelle Infrastruktur von Interesse sind, aktiv unterstützen.  Die derzeitige Ausschreibungspraxis, bei der nur solche Projekte, die zu proprietären Produkten führen, finanziert werden, sollte geändert werden."))
(ML Dcu "The EU patent organisations should help fund the development of open-source software ressources for the parsing and automatic translation of Logical Language texts.  By doing so it could in the end save very much money and trouble for patent owners while at the same time making legal information maximally accessible to the public and the enterprises." (de "Die europäischen Patentorganisationen sollten helfen, die Entwicklung von quellenoffenen Software-Ressourcen zur Analyse und automatischen Übersetzung von logiksprachlichen Texten zu finanzieren.  Auf diese Weise können sie letztendlich Patentbesitzern sehr viel Geld und Ärger sparen und gleichzeitig der Öffentlichkeit und den Unternehemen den bestmöglichen Zugang zu Patentinformationen erschließen."))
))

(sign (ML Urc "signatories" (de "Unterzeichner"))
(ol 
(ahst 'phm (famvok "Pilch" "Hartmut"))
(ahm "robin@bilkent.edu.tr" (famvok "Turner" "Robin"))
(ah "http://people.fix.no/arj/" (famvok "Johansen" "Arnt Richard"))
(ahm "KarlMusiol@aol.com" (famvok "Musiol" "Karl"))
(ahm "a100424@smail.Uni-Koeln.DE" (famvok "Mosler" "Karl"))
(ahm "helborg@muenster.de" (famvok "Poppenborg" "Helmut"))
))
))

(mlhtdoc 'fitugpe nil (ML FKW "FITUG e. V. setzt sich für mehr Rechtssicherheit bei Softwarepatenten ein." (fr "L`association FITUG lutte pour plus de sécurité juridique dans le domaine des brevets de logiciel.") (en "FITUG, a German association concerned with the social aspects of information technology, pushes for more legal security in the area of software patents.")) (l (ML PmW "program as such" (de "Programm als solches") (fr "programme en tant que tel")))
(ML FKW2 "FITUG e. V. setzt sich für mehr Rechtssicherheit bei Softwarepatenten ein." (fr "L`association FITUG lutte pour plus de sécurité juridique dans le domaine des brevets de logiciel.") (en "FITUG, a German association concerned with the social aspects of information technology, pushes for more legal security in the area of software patents."))

(lin
(ML DWe "Durch die gegenwärtige Rechtslage bei Softwarepatenten werden Bestand und weitere Entwicklung des Erfolgsmodells freie Software gefährdet, ohne daß dies nötig wäre." (fr "Le droit actuel en matière de brevets de logiciel met en danger sans nécessité apparente, le développement futur des logiciels libres qui s`avèrent pleins de succès.") (en "The current legal situation of software patenting threatens to stifle the burgeoning development of free software, without any apparent necessity."))
(ML Ige "Innovations- und Beschäftigungspotentiale bleiben ungenutzt; die Programmierer freier Software werden unkalkulierbaren, ungerechtfertigten, unübersehbar großen und unvermeidbaren Prozeßrisiken ausgesetzt." (fr "Des potentiels d`innovations et d`emplois restent inutilisés.  Les programmeurs de logiciels libres s`exposeront inévitablement à des risques de procès incalculables, imprévisibles et injustifiés.") (en "This leads to a waste of innovation and employment potentials; the programmers of free software are subjected to uncalculable, unjustifiable, and inevitable risks of litigation."))
)

(ML Aee "Auf europäischer Ebene wird derzeit eine Ausweitung der Patentierbarkeit von Software erwogen.  Dabei schadet das Patentrecht bereits in seiner gegenwärtigen Form der Softwarebranche mehr, als es ihr nützt. " (fr "En ce moment, on songe à élargir les droits de brevet sur les logiciels au niveau européen, et cela, bien que les droits de brevet actuels nuisent plus aux logiciels qu`ils ne leur servent.") (en "At the European level, planning is under way for an extension of the scope of patentability of software, although it is clear that even in its current form patent law is more harmful than useful for the software industry."))

(lin
(ML Dfe "Da sich Programmierer selbst durch sorgfältigste Recherche nicht vor der Gefahr schützen können, bei ihrer Arbeit ungewollt fremde Patente zu verletzten, wird insbesondere die Offenlegung der Quelltexte zum russischen Roulette." (fr "Même en travaillant soigneusement leurs recherches, les programmeurs ne peuvent s`assurer d`empiéter involontairement sur des brevets d`autrui, et si en plus, les programmeurs publient le code source, cela revient à jouer à la roulette russe.") (en "Since programmers can never, not even by extensive patent research, protect themselves agains the danger of involuntarily infringing on someone's patent, publishing one's software source code is tantamount to playing russian roulette."))
(ML DWE "Das ist nicht nur für den einzelnen Programmierer eine ungerechte Belastung mit fremden Risiken, sondern auch gesamtwirtschaftlich völlig unvernünftig: Das größte Problem der europäischen Softwarebranche ist der Mangel an Arbeitskräften." (fr "Cette situation n`est pas seulement un facteur de risques injustes pour le programmeur individuel, mais c`est insensé pour l`économie générale: Le manque de main d`oeuvre est le plus grand problème du logiciel européen.") (en "This situation not only places an undue burden on individual programmers, but is also macro-economically unreasonable:  the biggest problem of the European software industry is a lack of skilled personnel."))
(ML Fih "Freie Software kann dabei helfen, diesen Arbeitskräftemangel zu verringern, indem sie die Marktzutrittsschwelle für Newcomer senkt und ihnen erlaubt, sich in evolutionären Schritten zu professionalisieren." (fr "Les logiciels libres pourraient remédier à reduire ce manque de main d`oeuvre, en permettant un accès plus facile aux nouveaux arrivants, et en leur ouvrant les possibilités d`une professionalisation évolutive.") (en "Free Software can help to reduce this lack of skill by lowering the entry barrier for newcomers and allowing them to professionalise themselves step by step."))
(ML Dse "Daß freie Software keineswegs zweitklassig ist, sondern wichtige Innovationsimpulse setzen kann, ist längst durch die Praxis erwiesen." (fr "La pratique a prouvé que les logiciels libres ne sont nullement des produits de seconde classe, mais plutôt capable d`inciter d`importantes innovations.") (en "Yet, as experience shows, free software is by no means inferior but rather a source of important innovative impulses."))
(ML DHd "Der Gesetzgeber sollte deshalb keine unnötigen Hürden errichten, die es Newcomern erschweren, den Softwaremarkt um neue Produkte zu bereichern." (fr "Le législateur devrait donc éviter d`ériger des obstacles inutiles qui freinent la productivité des nouveaux arrivants, et ainsi l`enrichissement du marché des logiciels.") (en "The legislators should therefore refrain from erecting any unnecessary entry barriers to the production of software."))
(ML Dno "Der finanzielle Aufwand, der nötig ist, um das Risiko von Patentrechtsklagen zu minimieren und das bei aller Sorgfalt stets dennoch verbleibende Risiko derartiger Klagen sind ganz erhebliche Hürden mit abschreckender Wirkung." (fr "Les frais financiers nécessaires à minimiser les risques de recours en justice sur les brevets, ainsi que les risques subsistants malgré un travail de recherche minutieux, tout cela présente d`importants obstacles et un effet dissuasif.") (en "The financial effort required to minimize the risk of patent lawsuits and the unavoidable remaining risks are high barriers with dissuasive effects."))
)

(colons (ML DdW "Diese Hürden sind auch völlig unnötig, denn die gegenwärtige Rechtslage liegt auch nicht im Interesse der Patentrechtsinhalber selbst" (fr "Ces obstacles sont totalement inutiles puisque la situation juridique ne va pas non plus dans l`intérêt même du propriétaire de brévet") (en "These barriers are completely unnecessary, they do not even benefit the patent owners themselves."))
 (ML DmW "Die Gefahr ungewollter Patentrechtsverletzungen besteht nicht nur für kleine Firmen und die Entwickler freier Software, sondern ist selbst für große Firmen, die viel Geld in ihre Patentrechtsabteilungen investieren können, ein großes Problem" (fr "Le danger d`empiéter involontairement sur des brevets n`existe pas seulement pour les petites entreprises ou les créateurs de logiciels libres, mais ce danger présente aussi un grand problème pour les grandes entreprises qui investissent beaucoup d`argent dans leurs services juridiques.") (en "The danger of involuntary patent infringment is of grave concern not only to small companies and developpers of free software, but also to large companies that can afford to spend a lot of money on their patent departments"))
 (ML Doa "Der zweitgrößte Softwarehersteller der Welt, Oracle, gibt mittlerweile öffentlich zu, daß er Softwarepatente nur zu dem Zweck anmeldet, um hierdurch ein eigenes Drohpotential aufzubauen, das andere Softwarehersteller davon abschreckt, Oracle wegen Patentrechtsverletzung zu verklagen." (fr "Le deuxième producteur mondial de logiciels, Oracle, reconnaît officiellement qu`il dépose des brevets seulement dans l`intention de monter une force de dissuasion pour empêcher d`autres entreprises d`attaquer Oracle pour utilisation illégitime de brevets.") (en "The world's second largest software publisher, Oracle, meanwhile acknowledge publicly, that they are spending money on software patents for purely defensive purposes, e.g. to deter other software companies from suing Oracle for patent infringment.")) )

(lin
(ML Dir "Das spricht für sich." (fr "Cela veut tout dire.") (en "That tells everything."))
(ML Dee "Die Sach- und Rechtslage bei Softwarepatenten, insbesondere die Reichweite des gewährten Schutzes, ist so intransparent, daß das Recht in diesem Bereich derzeit reine Zufallsergebnisse produziert." (fr "La situation de fait et la situation juridique des brevets de logiciels est si obscure, en particulier sur le périmètre de la protection accordée, que le droit produit actuellement dans ce domaine, des résultats relevant du hasard.") (en "The material and legal situation of software patents, especially the scope of protection, is so intransparent, that the law in this area currently generates random results."))
(ML DnK "Das liegt nicht nur an der Komplexität des Patentrechts selbst, sondern vor allem auch an der Komplexität der zugrunde liegenden Wirklichkeit." (fr "Cela n`est pas uniquement dû à la complexité du droit de brevet même, mais surtout aussi dû à la complexité de la réalité à la base.") (en "That is not only due to the complexity of patent law itself, but even more to the complexity of the underlying subject matter."))
(ML Dtw "Das Patentrecht selbst ist langfristig gesehen nicht überlebensfähig, wenn es derartige Defizite aufweist." (fr "Le droit de brevet ne pourra survivre à long terme s`il présente de tels déficites.") (en "Patent law cannot survive in the long run, if its design continues to exhibit such flaws."))
)

(lin
(ML Ipn "Im Interesse der europäischen Softwarebranche, aber auch im Interesse einer Akzeptanz des Rechts durch die Betroffenen sollte der Gesetzgeber diesen unzumutbaren Zustand so schnell wie möglich beenden, anstatt ihn noch mehr zu verschlimmern." (fr "Le législateur devrait en finir au plus vite avec cette situation inacceptable au lieu de l`empirer plus, et cela dans l`intérêt de la branche du logiciel européen, mais aussi pour que les concernés acceptent ses lois.") (en "In the interest of the European software industry and for the sake of the acceptability of the legal system by the concerned parties, our legislators should take action to end this unacceptable situation, rather than to worsen it."))
(ML DWg "Dieses Problem muß umgehend gelöst werden." (fr "Il est nécessaire de régler ce problème au plus vite.") (en "This problem must be solved immediately."))
(ML FuK "Fitug e. V. wird versuchen, praktikable Lösungsvorschläge zu entwickeln und ruft alle übrigen Beteiligten auf, das Gleiche zu tun." (en "La FITUG essaiera de développer des propositions de solutions pratiques, et elle lance un appel à tous les concernés de faire de même.") (fr "FITUG will endeavor to develop useful proposals for solving the problem and calls upon all concerned parties to do the same."))
)

(colons (ML Urc "Unterzeichner" (en "signataries") (fr "signataires"))
(apply 'ul (mapcar (lambda (lst) (if (atom lst) lst (nl (bold (car lst)) (apply 'commas (cdr lst))))) (l
  (l (ah "http//www.fitug.de/" (tpe "Fitug e.V." (ML Ffi "Förderverein für Informationstechnologie und Gesellschaft" (en "German Association for Information Technology and Society") (fr "Association Allemande pour la Technologie d'Information et la Société")))) (ahm "JOHANNESULBRICHT@cs.com" "Johannes Ulbricht"))
    
  (l (ah "http://www.luga.at/" (tpe "LUGA" (ML LeW "Linux User Group Austria" (fr "Groupe Utilisateurs Linux Autriche")))) (ahm "michael@wsr.ac.at" "Michael Demelbauer"))
    
  (l (ah "http://www.vov.de/" (tpe "VOV" (ML Vre "Virtueller Ortsverein der SPD" (fr "%(q:Section Virtuelle) du Parti Social-Democrate Allemand") (en "%(q:Virtual Section) of the German Social Democratic Party")))) (ah "roland.dieterich@gmd.de" (tpe "Roland Dieterich" (ML Vri "Vorsitz" (en "chairman") (fr "président")))))

  (l (ah "http://swpat.ffii.org/" (tpe "FFII e.V." (ML Fnn "Förderverein für eine Freie Informationelle Infrastruktur" (en "German-speaking Association for the Promotion of a Free Informational Infrastructure") (fr "Assiociation germanophone pour la Promotion d'une Infrastructure Informationelle Libre")))) (ah "bernhard@uwm.edu" "Bernhard Reiter") (ah "wl@gnu.org" "Werner Lemberg") (ahm "phm@a2e.de" "Hartmut Pilch"))
    
  (l (ah "http://www.vibe.at/" (tpe "VIBE!AT" (ML Vte "Verein für Internet-BEnutzer Österreichs" (en "Association of Austrian Internet Users") (fr "Assiciation Utilisateurs Internet Autriche")))) (ahm "peter.kuhm@plus.at" "Peter Kuhm"))
    
  (l (ah "http://www.dante.de/" (tpe "DANTE e.V." (ML Die "Deutschsprachige Anwendervereinigung TeX" (en "German Speaking TeX User Group") (fr "Association Germanophone Utilisateurs TeX")))) (ahm "thomas@gandalf.rhein.de" "Thomas Koch"))

  (ahm "marten@milkbar.ul.bawue.de" "Marten Karl")
)))); ul lin

;fitugpe
)

(mlhtdoc 'swxbgh27 
nil 
(ML Ine "In der Frage der Patentierbarkeit von Programmlogik hat der Bundesgerichtshof (BGH) sich und ganz Deutschland ins politische und rechtliche Abseits manövriert.  Wir fordern den Bundestag auf, hier seine gesetzgeberischen Kompetenzen wahrzunehmen.")
nil
(ML EWu "1999-2000 legalisierte der Bundesgerichtshof (BGH) durch eine Reihe von Grundsatzurteilen in Deutschland die Patentierung von Konzepten der Programmlogik bis hin zu programmierten Geschäftsmethoden, Lehrmethoden, Heilmethoden, Methoden gesellschaftlicher Organisation, musikalische Kompositionstechniken sowie allen erdenklichen geistigen und zwischenmenschlichen Verfahren, sofern sie den Anforderungen einer virtuellen Maschine (Turing-Prinzip) genügen.  Damit trifft der BGH eine sehr weitreichende gesellschaftspolitische Entscheidung.   Europas Gesetzgeber haben sich im Jahre 1973 gemeinsam anders entschieden: gegen die Patentierbarkeit von Programmlogik und Verfahren der gesellschaftlichen Organisation.  An diese Entscheidung haben sie sich selbst durch einen internationalen Vertrag (EPÜ) und ihre jeweiligen Gerichtsbarkeiten durch nationalen Gesetzen (PatG) gebunden.  Daran hat sich bis vor kurzem auch der BGH ausdrücklich gehalten.")
(ML SSM "Seit 1997 hat der BGH jedoch zunehmend dem Druck einer Patentrechtler-Lobby nachgegeben, die auf eine Änderung der Gesetze hinarbeitet.  Mit seinem neuesten Urteil bezieht er zu politischen Streitfragen Stellung, die derzeit in Brüssel kontrovers diskutiert werden.  Auf der Seite der Informationstechniker, Wirtschaftswissenschaftler und IT-Unternehmer überwiegt dabei die Meinung, dass die Patentierung von Programmlogik der Innovation und dem Wettbwerb abträglich ist und wirtschaftspolitisch nicht erwünscht sein kann.") 
(ML WRW "Wir bitten die Bundesregierung und alle Abgeordneten des Deutschen Bundestages und Bundesrates, geeignete Maßnahmen zu ergreifen, um den Bundesgerichtshof wirksam an die Vorgaben des Gesetzgebers zu binden.")
)


(mlhtdoc 'swxegp27 
(ML Azc "Anregungen zum Gemeinschaftspatent" "Suggestions for the Community Patent")
(ML DWj "Die EU-Kommission möchte innerhalb des Jahres 2000 das europäische Patentwesen weiter zentralisieren, um den Nutzern des Patentsystemes %(q:viel Wert für wenig Geld) bieten zu können.  Wir fordern, dass einer solchen quantitativen Reform eine qualitative Reform vorangehen soll.  Das Patentwesen muss kontrollierbar werden.  Die Patentinflation muss wirksam und dauerhaft eingedämmt werden.  Es müssen Institutionen und Regeln geschaffen werden, die dem Patentkomplex Anreize bieten, lieber weniger und bessere Patente zu produzieren.")
nil

(ML Dlm "Dieses Jahr will die EU-Kommission das Patentwesen grundlegend reformieren und dabei die Weichen für die nächsten 20 Jahre stellen.  Hauptanliegen der bisherigen Entwürfe der EU-Kommission ist es, das Patentwesen zu zentralisieren, um Patentinhabern %(q:mehr Wert für weniger Geld) bieten und somit einen stärkeren Anreiz zur Anmeldung von möglichst vielen Patenten zu schaffen.")
(ML WeW "Wir meinen:  Weniger ist mehr.  Das Patentwesen befindet sich heute in einer Krise.  Es wird von der Gesellschaft nicht mehr unbedingt als legitim angesehen.  Es hat ein Klima der Rechtsunsicherheit und Streitsucht geschaffen.  Unternehmen sammeln Patente wie Waffen.  Sie dienen längst nicht mehr dem Schutz echter Erfindungen.  Vielmehr werden Minenfelder aufgebaut, auf denen kleine Unternehmen nicht überleben und selbst große Unternehmen sich unwohl fühlen.")
(ML Dhg "Der Grund für diese Krise liegt darin, dass das Patentwesen außer Kontrolle geraten ist.  Die Gesetze werden von Patentexperten für Patentexperten gemacht.  Je mehr Patente vergeben werden, desto besser geht es dem Berufsstand der Patentexperten.  Vor Gerichten kommt nur der Standpunkt patentsuchender Unternehmer, nicht aber der Standpunkt der terrorisierten Öffentlichkeit zu Gehör.  Die Gerichte und Gesetzgeber haben sich daraus eine proprietaristische Berufsideologie zurecht gelegt, unter deren Anleitung sie ständig nach Wegen suchen, die Anforderungen an Patentierbarkeit weiter abzusenken und aufzuweichen.")
(ML AWh "Auch das Projekt %(q:Gemeinschaftspatent) steht unter dem Vorzeichen dieser Berufsideologie.  Alle Veröffentlichungen der Generaldirektion Binnenmarkt sind von dem Glaubenssatz %(q:je mehr Patente desto mehr Innovation) durchströmt.  Es fehlt jeder Ansatz einer ausgewogenen Sicht.")
(ML WWK "Wir erkennen zwar durchaus an, dass die Straffung des europäischen Patentwesens Vorteile haben kann.  Aber es nützt uns wenig, wenn ein von Grund auf unkontrollierbares und korruptes Patentwesen lediglich gestrafft und effektiver gemacht wird.  Die Reform des Jahres 2000 muss das Übel an der Wurzel packen, wenn sie wirklich ein gutes System für die nächsten 20 Jahre schaffen will.  Es müssen Systeme und Institutionen geschaffen werden, die dem Patentsystem einen Anreiz zur Selbstverbesserung geben könnten, z.B.:") 

(sects
(kon
 (ML Ulo "Parlamentarischer Kontrollausschuss Informationseigentum")
 (ML Ide "Im Europa-Parlament ist ein Ausschuss für Fragen des Informationseigentums (meist irreführend %(q:geistiges Eigentum) genannt) zu gründen, der sich nur zu einem geringen Teil aus Patentexperten oder Juristen zusammensetzt.  90% der Ausschussmitglieder sollen aus Berufsgruppen kommen, deren Wohlergehen nicht von der Expansion des Informationseigentumskomplexes abhängt.")
 (ML Inl "Im Zeitalter des digitalen Kapitalismus entstehen dauernd Forderungen nach Ausweitung oder Rücknahme bisheriger Informationseigentumsrechte wie z.B. Eigentum an Datenbanken, Architekturentwürfen, Fußballsenderechten.   All diese Rechte sind tendenziell unbeständig und problematisch, können aber dennoch für die Wirtschaftsentwicklung nützlich sein.  Hier ist der Gesetzgeber ständig gefordert, grundlegende politische Entscheidungen zu treffen, die nicht der Judikative überlassen werden können.")
  (ML DgK "Der %(q:Parlamentarische Ausschuss für Informationseigentum) sollte sich ständig mit auftretenden Grenzverschiebungen und Grundsatzfragen beschäftigen und darüber wachen, dass die die Gerichte sich an die Vorgaben des Gesetzgebers halten.")
  (ML Oel "Ähnliche Ausschüsse könnten auch in nationalen Parlamenten gegründet werden.")
)

(mon
 (ML Agn "Abschaffung der Lizenzgebühren")
 (ML Prr "Patentämter sollten nicht mehr an der Vergabe von Patenten verdienen.  Die Patentgebühren sind abzuschaffen.  Die Patentgerichtsbarkeit ist stattdessen aus Steuergeldern zu finanzieren." "patent offices no longer receive money for granting patents.  Patent fees are abolished.") )

(cel
 (ML Nct "Neuer Zweck der Patentämter" "New Purpose of the Patent Offices")
 (ML Hie "Hauptaufgabe der Patentämter soll es sein, der Öffentlichkeit Informationen über Patente und den Stand der Technik umfassend, offen und frei zugänglich zu machen." "The chief purpose of existence of the patent offices shall be to provide valuable free information on patents and related prior art to the public.") )

(nov
 (ML NiK "Neuheitsprüfung" "novelty check") 
 (ML Dzs "Die Patentämter sollten keine Recherchen mehr durchführen.  Für die Recherchen sollen hingegen die private Prüfungsunternehmen (nach Art von Wirtschaftsprüfern) verantwortlich sein.  Diese Unternehmen sollen immer dann Schadensersatz leisten, wenn sie nicht vollständig recherchiert haben und später weitere neuheitsschädigende Dokumente auftauchen.  Der Preis der Recherche soll vom Marktwettbewerb frei bestimmt werden." "patent office no longer conduct patent examination themselves.  Instead this is given into the hands of certified companies, who are held liable later if they have not found all prior art (similar to the liability of financial audit companies).") )

(inv
 (ML Enk "Erfindungshöhe-Prüfung" "inventivity check")
 (ML DSW "Die Erfindungshöhe wird durch einen öffentlichen Wettbewerb überprüft.  Sobald die Neuheitsprüfung abgeschlossen ist, wird das rechtsverbindliche %(q:von der Erfindung zu lösende Problem) zusammen mit dem %(q:Stand der Technik) veröffentlicht.  Innerhalb einer Einreichungsfrist von 6 Monaten hat dann die gesamte Öffentlichkeit die Gelegenheit, Problemlösungsvorschläge einzureichen, die nach 6 Monaten veröffentlicht werden.  Wer die zu patentierende Lösung gefunden hat, wird Mitinhaber des Patents.  Andere Lösungen werden dem %(q:Stand der Technik) zugerechnet, an dem das Patent zu messen ist. Falls es mehr als 3 Mitinhaber gibt, gilt die Erfindung als nicht genügend erfinderisch und der Patentantrag wird verworfen." "after the novelty check, only the %(goal of the invention) and the %(q:prior art) parts are published, and anybody who submits the same solution within 6 months becomes co-patent-owner.  If there are more than 3 such co-patent-owners, the invention is deemed not inventive enough and rejected.") )

(tech
 (ML KBq "Klärung des Begriffes %(q:Technizität)" "Definition of %(q:Technicity)")
 (ML Dcc "Der Begriff der Technizität spielt heute bei der Abgrenzung dessen, was eine patentierbare Erfindung darstellt, eine entscheidende Rolle.  Daher ist dieser Begriff verbindlich zu klären.  Man kann dabei auf die BGH-Definition zurückgreifen, wonach %(q:planmäßiges Handeln unter Einsatz beherrschbarer Naturkräfte ohne zwischengeschaltete menschliche Tätigkeit) vorliegen muss.  Erfindungen im Bereich der Programmlogik sind nicht technisch, weil sie (in ihrem erfinderischen Kern) keinen Gebrauch von Naturkräften machen.")
)

(ind
 (ML Geg "Geistiges Gemeineigentum und Allgemeinheitspatent" "Public Intellectual Property and Patentleft")
 (ML WWs "Wenn bisher von %(q:geistigem Eigentum) die Rede ist, ist damit ausschließlich %(e:informationelles Privateigentum) gemeint.  Die Entwicklung der %(e:Freien Software) (OpenSource) macht aber deutlich, dass es auch Lizenzmodelle wie das %(e:Allgemeinheitsurheberrecht) (Copyleft) gibt, die zum Schutz eines %(e:informationellen Gemeineigentums) dienen.  Diese Modelle greifen letzter Zeit auch auf klassische Industriebranchen (z.B. Prozessorbau, Verbrennungsmotorenbau) über.  Es handelt sich hier um eine neue, wirtschaftspolitisch erwünscht Entwicklung, der das Patentwesen Rechnung tragen sollte.")
 (ol
  (ML Eie "Es ist klarzustellen, dass Patentansprüche sich niemals gegen informationelles Gemeineigentum richten dürfen.  Wer Informationen in das Gemeineigentum überstellt, kann damit kein Patent verletzten.  Der Begriff der %(q:gewerblichen Anwendung) ist entsprechend verbindlich zu definieren.")
  (ML Eve "Es sollte eine Sonderform des Patentschutzes für informationelles Gemeineigentum geben.  Wer eine Erfindung zum Allgemeinheitspatent, macht alle Bürger zu Eigentümern des Patentes und kann damit, ähnlich wie beim Allgemeinheitsurheberrecht, die Bedingung verbinden, dass die patentierte Erfindung nicht in Systemen verwendet werden darf, in denen unfreie (privat patentierte) Erfindungen zum Einsatz kommen.  Auf diese Weise kann der Erfinder der Öffentlichkeit ein Mittel in die Hand geben, mit dem sie sich gegen frivolen Patentterror wehren kann.  Für Gemeineigentumspatente und nur für sie sollte es eine Schonfrist nach Veröffentlichung geben, innerhalb derer sie noch angemeldet werden können.  Ferner sollten für sie alle eventuellen Patentgebühren entfallen.")
  )
 )

(tra
 (ML nur "Übersetzungsanforderungen" "Translation Requirements")
 (ML Eso "Ein Hauptanliegen des derzeitigen Kommissionsentwurfs ist es, die Übersetzungskosten zu senken, die laut Angaben der Kommission derzeit bei ca 25% der Kosten eines Patents liegen.")
 (ML Dkf "Die Übersetzungsanforderungen sollten nicht so leichtherzig geopfert werden, wie die EU-Kommission das vorschlägt.  Ein Patent dient zu mehr als nur dazu, seinem Eigentümer %(q:viel Wert für wenig Geld) zu bieten.  Es erfüllt z.B. folgende öffentlichen Funktionen" "translation requirements should not be sacrificed as light-heartedly as the EC is proposing.  Offering patent owners %(q:good value for little money) isn't everything. The patent system has some public-interest purposes such as")
    (dl 
     (l (ML Rsl "Rechtsaufklärung" "Legal Information") (ML 3Ub "jeden Bürger (nicht nur Patentsexperten, die Englisch oder andere Patentsprachen flüssig beherrschen) darüber aufzuklären, was er tun darf und was nicht" "telling everybody (not only patent specialists who are fluent in English or other patent languages) what they are allowed to do") )
     (l (ML Tkl "Technik-Aufklärung" "Technological Information") (tpe (ML pls "modernes technisches Wissen zu vermitteln" "popularising advanced technological knowledge") (bridi 'ie (filters ((aw ahi 'inv)) (ML War "Wissen über wenige echte Erfindungen, die durch %(aw:ausgewogene Wettbewerbsverfahren) ausgesucht wurden, nicht übersetzungsunwürdiger Informationsmüll" "knowledge about real inventions, selected by %(aw:balanced competition procedures), not masses of cheap trivial patents)"))) ) )
     (l (ML Scl "Sprachpflege" "Language Cultivation") (ML ccW "Technische Terminologie in verschiedenen Sprachen zu pflegen.  Das EPA wirbt gerne bei Übersetzern damit, dass gerade im Rahmen der Patentierung eine gepflegte Sprache mit gut durchdachter und normierter Terminologie entsteht, auf die technische Übersetzer sich auch außerhalb des Patentwesens stützen können." "cultivating technical terminology in multiple languages") ) ) 
    (filters ((ll ahs 'eulojban)) (ML nsu "Durch Verwendung einer %(ll:Logiksprache), die sich automatisch in viele Sprachen übersetzen lässt, könnte man die Vielsprachigkeit billig machen und gleichzeitig kostspielige Vieldeutigkeiten beseitigen, die durch Verwendung einer syntaktisch unscharfen Sprache wie Englisch entstehen.  Es erscheint widersprüchlich, in einer Zeit, wo die EU-Kommission viel Geld für eine %(q:vielsprachige Informationsgesellschaft) ausgibt und die kulturelle Heterogenität Europas als Standortvorteil anpreist, die Vielsprachigkeit im Patentwesen ohne Not beseitigen zu wollen." "Translation can be made cheap by using a %(ll:Logical Language) which allows reliable automatic translation into many languages.  It is contradictory to abolish translation requirements at a time when other directorates of the EU are investing a lot of money in making a %(q:multilingual information society) viable.")) 
)

(etc (ML WeI "Weitere Ideen" "Further Ideas") 
(sects
 (dev 
  (ML ZiW "Zwangslizenz für Teile von Normen oder komplexen Systemen" "Compulsory License for Parts of Standards or Complex Systems")
  (ML Mwl "Manche Erfindungen werden im Laufe der Zeit Teile von Normen, an die sich jeder Wettbewerbsteilnehmer halten muss, um mit anderen kompatibel zu sein.  In solchen Fällen sollte das bereits weitgehend abgeschaffte System der Zwangslizenz erneut zu Ehren kommen.") 
 (ML Oei "Ähnliches gilt für komplexe Systeme, in denen hunderte von Erfindungen verwendet werden.  Hier sind die Transaktionskosten zu hoch, wenn jeder einzelne Patentinhaber das Recht behält, das gesamte System zu blockieren.") )
)
;sects 
)

;sects
)
;swxegp27
)

(list 'swpatkarni-dir
(mlhtdoc 'swpatkarni nil nil nil)

(mlhtdoc 'swxkcz27 (ML CWA "Computer Zeitung 2000-07-27: SWPAT unterbelichtet")
(let ((AG (ahs 'swpatgirzu)) (AK (ahs 'ar-kleinert)) (CZ (ahm "cz.redaktion@konradin.de" "Computer Zeitung"))) (ML DWC "Diesen Brief sandten wir nach Diskussion in der %{AG} am 31. Juli 2000 an die %{CZ}.  Der erste Entwurf stammte von %{AK}."))
nil
(ML ShR "Sehr geehrte CZ-Redaktion,")

(ML iad "in Ihrer Ausgabe Nr. 30 vom 27. Juli 2000 würdigen Sie unter anderem das Thema der globalen Wissensökonomie (Okinawa-Konferenz) und melden, dass Sun %(q:Open Source) benutzen will, um seine Server zu vermarkten.")

(ML Lse "Leider befasst sich nur ein kurzer Artikel auf Seite 2 mit der von der EU-Kommission angestrebten Neufassung des Patentrechtes - und gerade die Auswirkungen auf die Quellenoffenheit und den freien Austausch von Programm-Informationen werden hierin nicht thematisiert.")

(let ((PET (ah (href 'eulux-petition))) (WARN (ah "http://petition.eurolinux.org/pr/pr3.html"))) (ML DWC5 "Die Diskussion in der IT-Branche ist allerdings bereits ein wenig weiter gediehen: unter %{PET} wurde eine Petition gestartet, deren Ziel es ist, die Ausdehnung von Patenten auf den Software-Bereich zu verhindern, wobei eindringlich vor möglichen Gefahren gewarnt wird. Ein weiteres Thema ist die fast schon an Desinformation grenzende Informationspolitik der zuständigen Behörden -- Presseinfo finden Sie unter %{WARN}."))

(let ((URL (ah (absurl 'euluxpr4)))) (ML DWC6 "Gegenwärtig sind bereits über 25000 Unterschriften von oftmals leitenden oder selbstständigen IT-Fachleuten zusammengekommen, darunter auch über 300 Firmenchefs (siehe detaillierte Auswertung unter %{URL}"))

(ML Ios "Insbesondere für kleinere und mittlere Unternehmen (KMU) stellen Softwarepatente denn auch eher eine grosse Hürde und ein Innovationshemmnis als eine Hilfe dar - von Neugründern ganz zu schweigen.")

(ML Otu "Opensource-Software ist besonders gefährdet.  Das Patentsystem erzwingt geradezu die Geheimhaltung der Quelltexte vor potentiellen Abmahnern sowie die generalstabsmässige Organisation des Programmierens und den genau kontrollierten Verkauf von Einzellizenzen. Auch flexible Vertriebsformen wie Shareware werden benachteiligt.")

(let ((PRN (ah (absurldir 'swpatprina)))) (ML AsW "Aufgrund der einleuchtenden Argumente haben sich jedoch auch Großfirmen wie Oracle und Adobe und mittelständische Software-Innovatoren wie Infomatec, Phaidros, Netpresenter, Intradat u.v.m. bereits deutlich gegen die Patentierbarkeit von Software ausgesprochen (vergleiche http://swpat.ffii.org/vreji/prina/)."))

(ML Vrl "Vielleicht mag dieser kurze Anriss des überaus komplexen Themenbereiches %(q:Softwarepatente) Sie anregen, die Problematik Ihrer Leserschaft in einem ausführlicheren Hintergrundbericht etwas näherzubringen und das Problembewusstsein zu schärfen - egal, wie die jeweilige Sichtweise aussehen mag, an der Thematik kommt heutzutage kein Firmenchef oder IT-Entscheider mehr vorbei.")

(ML MnW "Mit freundlichen Grüssen,")

(center
(nl (ML cWn "%(nl:Softwarepatent-Arbeitsgruppe im:Förderverein für eine Freie Informationelle Infrastruktur e.V.)") (ah (absurldir 'swpat)))
)
)
; swpatkarni
)

(mlhtdoc 'swxepue28 
 (l
  (ML SAn "Eur. Patentamt begehrt grenzenlose Patentierbarkeit" (fr "l'Office Européen des Brevets veut suppimer toute limitation a la brevetabilité"))
  (mlval* 'SAn)
  (ML BIn "Offener Brief zum %(q:Basisvorschlag für die Revision des Europäischen Patentübereinkommens)" (fr "Lettre ouverte en réponse au %(q:propos de base de révision du droit européen des brevets) ")) )

 (filters ((pv ah (prina-doknom)) (bp ahs 'epo-basprop0008) (dk ahs 'swpepue2B) (pe ahs 'swnepue28)) (ML DiW "Das Europäische Patentamt (EPA) ist am 20.-29. November 2000 in München Gastgeber einer %(dk:diplomatischen Konferenz), die weitreichende Änderungen der grundlegenden Gesetzesregeln des europäischen Patentwesens beschließen soll.  Dabei verhandeln die europäischen Regierungen über einen %(bp:Basisvorschlag für die Revision des Europäischen Patentübereinkommens), mit dem das Europäische Patentamt nicht nur die Patentierung von Computerprogrammen und Geschäftsmethoden legalisieren, sondern jedwede gesetzliche Einschränkung der patentierbaren Gegenstände beseitigen will.  Mit diesem Offenen Brief weisen mehrere IT-Verbände und Softwarefirmen die Bundesregierung auf die Irrtümer und Gefahren des %(q:Basisvorschlags) hin und skizzieren Wege zu einer klaren Eingrenzung des Patentwesens.  In einer begleitenden %(pe:Presseerklärung) kommen unterstützende Firmen und Politiker zu Wort. Wir empfehlen Ihnen, die %(pv:Papierversion des Offenen Briefes) auszudrucken.  Sie ist wesentlich aktueller und ausgefeilter und enthält zahlreiche informative Anhänge.  Sie ist inzwischen an 55 führende Politiker gesandt worden." (fr "L'Office Européen des Brevets (O.E.B.) organise à Münich, du 20 au 29 novembre prochains, une %(dk:conférence diplomatique) afin de décider de vastes modifications aux règles fondamentales du droit européen des brevets. Les gouvernements européens se mettent d'accord actuellement sur un %(bp:propos de base de révision de la Convention Européenne sur les Brevets) qui devrait permettre à l'Office Européen des Brevets, non seulement de breveter les programmes d'ordinateurs et les méthodes commerciales, mais également d'écarter toute possible limitation à la brevetabilité d'une manière générale. La Lettre Ouverte de la FFII constitue une démonstration, à l'attention des négociateurs du gouvernement allemand, des erreurs et des dangers de ce %(q:propos de base), et une esquisse d'alternatives plausibles. La FFII réclame également l'accréditation en tant qu'organisation non-gouvernementale, afin de participer à la conférence. Nous joingnons à cette lettre le texte d'une %(pe:conférence de presse), dans laquelle s'expriment des entreprises et des hommes politiques qui nous soutiennent. ")))
 nil
 (nl
   "DE-10969 Berlin"
   "Neuenburger Straße 15"
   "Bundesministerium der Justiz"
   "Abt. 3 / Referat Patentrecht"
   "Herrn Dr. Elmar Hucko, Herrn Dr. Dietrich Welp, Herrn Raimund Lutz, Herrn Johannes Karcher")

 (nl (ML SrW "Sehr geehrter Herr Dr. Welp!" (fr "Monsieur,"))
     (ML SrC "Sehr geehrter Herr Dr. Hucko!")
     (ML ShL "Sehr geehrter Herr Lutz!")
     (ML ShK "Sehr geehrter Herr Karcher!") )

 (ML VPn "Vielen Dank für Ihre Hinweise und für die Übersendung des EPO-Dokuments CA/PL 25/00 %(q:Basisvorschlag für die Revision des Europäischen Patentübereinkommens) vom 27. Juni 2000." (fr "Nous tenons à vous remercier pour vos informations et pour votre envoi du document CA/PL 25/00 %(q:propos de base de révision de la Convention Européenne sur les Brevets), du 27 juin 2000."))

 (ML Wee "Wir beobachten schon seit einiger Zeit großer Sorge die Bestrebungen der Europäischen Patentjudikative, jenseits jeglicher gesetzgeberischer Kontrolle immer großzügigere Patentrechte zu gewähren, die mittlerweile eine Bedrohung für Innovation, Wettbewerb und Wachstum der %(q:neuen Wirtschaft) sowie für grundlegende Bürgerrechte der Informationsgesellschaft darstellen." (fr "Depuis quelque temps déjà nous nous inquiétons des tentatives des juristes européens en charge des brevets pour se substituer au législateur afin de doter les détenteurs de brevets d'armes toujours plus puissantes. Les pouvoirs qui leurs seraient ainsi conférés deviendraient en effet exorbitants, comparés aux buts légitimes d'un dépôt de brevet. Il s'agit là d'une menace pour l'innovation, la concurrence et le développement des techniques de l'information, ainsi que pour les droits fondamentaux des citoyens de la société de l'information. "))

(ML Die "Der neueste Vorstoß der Europäischen Patentorganisation stellt einen vorläufigen Höhepunkt dieser Bestrebungen dar.  Es wird u.a. vorgeschlagen, die gesamte Liste der Patentierungsbeschränkungen in Art 52 EPÜ zu streichen und stattdessen zu fordern, dass die zu patentierenden Gegenstände %(q:technisch) sein müssten, wobei die Definition der %(q:Technizität) bewusst %(q:dynamisch) gehalten, d.h. dem EPA anheimgestellt wird.  Während die BGH-Rechtsprechung bis vor kurzem %(q:Technik) konsequent als %(q:automatisierte Problemlösung unter Einsatz von Naturkräften) einschränkte, dient die der Technikbegriff im EPA-%(q:Basisvorschlag) umgekehrt dazu, alle denkbaren Beschränkungen des Patentwesens von vorneherein aufzuheben." (fr "L'avancée récente de l'organisation européenne des brevets constitue une dérive particulièrement notable, en ce qu'elle propose entre autres de supprimer purement et simplement la liste de toutes les limitations à la brevetabilité figurant à l'article 52 de la loi et de promouvoir à la place la notion de %(q:technicité) des objets brevetables. Or la définition de la %(q:technicité), qualifiée de %(q:dynamique), est laissée à la libre appréciation de l'O.E.B. Alors que jusque tout récemment la jurisprudence de la Cour Fédérale de Justice donnait une définition restrictive et cohérente de la %(q:technicité) comme %(q:solution automatisée d'un problème par l'utilisation des forces naturelles), cette même notion de technicité sert au contraire à l'O.E.B. de prétexte pour lever toute possible limitation à la brevetabilité.   "))
(ML DMd "Das EPA definiert in seinem %(q:Basis-Vorschlag) wie auch sonst den %(q:technischen Charakter) zirkulär: %(q:an den Fachmann gerichte Anweisung, eine bestimmte technische Aufgabe mit bestimmten technischen Mittel zu lösen).  Die %(q:Technizität) ist nach EPA-Verständnis das %(q:grundlegende Erfordernis der Patentierbarkeit), und sie leitet sich aus der Forderung nach %(q:erfinderischer Tätigkeit) her.  M.a.W. %(q:Technizität) bedeutet nicht mehr als die mit jedem Patentantrag ohnehin implizierte und kaum widerlegbare %(e:Grundannahme) der %(e:kommerziell/gewerblichen Anwendbarkeit) und der %(e:Existenz einer Gemeinde von interessierten Fachleuten).  Somit kommt der EPA-Forderung nach %(q:Technizität) bereits heute keinerlei einschränkender Charakter mehr zu.  Im Gegenteil, die %(q:Basisvorschlag)-Formulierung %(q:Erfindungen auf allen Gebieten der Technik) bezweckt laut EPA-%(q:Erläuterungen), %(q:augenfällig auszudrücken, dass der Patentschutz grundsätzlich technischen Erfindungen aller Art offensteht).  Der %(s:Universalitätsanspruch des Patentwesens) soll gesetzlich festgeschrieben werden." (fr "D'ailleurs, dans son %(q:propos de base), l'O.E.B. propose pour cette notion de %(q:technicité) une définition circulaire: %(q:instruction donnée au spécialiste de réaliser telle tâche technique avec tels moyens techniques). Pour l'O.E.B., la %(q:technicité) est la %(q:condition de base pour la brevetabilité) et découle de l'exigence d'une %(q:activité inventive). En d'autres termes, la %(q:technicité) n'a pas d'autre signification que celle déjà portée sur chaque contrat de brevet de par l'%(e:axiome de base) de l'%(e:utilisabilité dans un cadre commercial ou industriel) et de l'%(e:existence d'une communauté de spécialistes intéressés par le sujet), réalités implicites et fort difficiles à réfuter. L'exigence de technicité selon l'O.E.B. n'apporte donc plus aucun caractère restrictif, au contraire: la formulation du %(q:premier projet) - %(q:des inventions dans tous les domaines techniques)  - vise %(q:bien évidemment à faire en sorte que la protection par le brevet s'ouvre à toute invention technique de quelque domaine que ce soit). Et cette %(s:doctrine de l'universalité du brevet) doit être inscrite dans la loi!"))
(ML Fsl "Scheinbar relativierende %(q:Erläuterungen) wie %(q:Andererseits gibt es eine lange europäische Rechtstradition, wonach der Patentschutz Schöpfungen auf dem Gebiet der Technik vorbehalten ist) können nicht darüber hinwegtäuschen, dass der %(q:Basisvorschlag) in Wirklichkeit auf die restlose Aufhebung ebenjener Rechtstradition zugunsten amerikanischer Verhältnisse hinausläuft." (fr "Certains %(q:commentaires) apparemment modérés, comme %(q:par ailleurs il existe une longue tradition juridique européenne d'après laquelle la protection par le brevet des créations est cantonnée au domaine technique) ne peuvent réussir à masquer la tendance lourde du %(q:propos de base) à éliminer systématiquement ladite tradition juridique au profit d'une pratique américanisée."))
(ML Dta "Die europäische Rechtstradition nach Art 52 EPÜ hat bewusst auf einen Universalitätsanspruch für das Patentwesen verzichtet.  Sie beruht auf einer geschickten Trennung zwischen Materie und Information.  Ein wertvolles Informationsgut wird mit der Patentschrift veröffentlicht, und im Gegenzug wird die Kontrolle über eine Klasse wertvoller materieller Güter befristet privatisiert.  Diese Trennung kam in den klassischen Definitionen von Begriffen wie %(q:Technizität) und %(q:industrielle Anwendbarkeit) ebenso wie in der Einschränkungsliste des Art 52(2) zum Ausdruck." (fr "Avec l'article 52 de la loi sur les brevets, la tradition juridique européenne fit le choix délibéré de renoncer à l'universalité de la protection par le brevet. Elle se fia à une intelligente distinction entre la matière et l'information. Lors du dépôt de brevet, le bien informationnel est publié et en contrepartie, le contrôle sur toute une catégorie de biens physiques se trouve privatisé pour une certain temps. Cette distinction repose sur les définitions classiques de concepts comme la %(q:technicité) et la %(q:possibilité d'application industrielle), ainsi que de la liste limitative figurant à l'article 52-2. "))
(ML ZeG "Zwischen Materie und Information (Hardware und Software, physischen und logischen Strukturen) verläuft eine fundamentale Grenze mit starken wirtschaftlichen Auswirkungen.  Materielle Güter sind ortsgebunden und ihre Serienfertigung erfordert industriellen Aufwand; informationelle Güter hingegen sind von ihrem Träger unabhängig und lassen sich tendenziell kostenlos vervielfältigen.  Materielle Güter sind zeitgebunden und lassen sich nach einmaligem Konsum erneut in identischer Form verkaufen; informationelle Güter hingegen sind unkonsumierbar und gewinnen nur aus Innovation (immer neue Versionen) einen wirtschaftlichen Wert.
Materielle Güter sind Geldwerte, informationelle Güter sind Machtfaktoren.
Materielle Güter bringen maximale Geldeinnahmen bei weitestgehender Verteilung, informationelle Güter erlauben maximalen Machtzuwachs bei weitestgehender Verknappung (Etablierung von exklusiven %(q:Standards), Geheimhaltung von %(q:Schlüsselinformationen)).
Materielle Güter verlangen nach privaten Besitzern, informationelle Güter hingegen gehen über kurz oder lang ins Gemeineigentum über, wo sie ihren volkswirtschaftlichen Wert oft erst richtig entfalten können.
Materielle Güter gehören meist auf unbefristete Zeit einem naturrechtlichen Eigentümer; für informationelle Güter gibt es hingegen befristete Ausschlussrechte, die der Staat aus wirtschaftspolitischen Erwägungen gewährt.  
Das Patentrecht erlaubt die befristete Monopolisierung bestimmter Klassen materieller Güter; für informationelle Güter hingegen gelten das Urheberrecht und die Ausdrucksfreiheit." (fr "Entre la matière et l'information, entre le hardware et le software, passe une frontière essentielle aux fortes implications économiques. Les biens matériels sont liés géographiquement à un lieu et leur production en série exige un équipement industriel; les biens informationnels sont au contraire indépendants de leur porteur et peuvent être dupliqués à un coût unitaire tendant vers zéro. Les biens matériels sont liés dans le temps: après les avoir consommés on peut encore les acheter plusieurs fois sous une forme identique; les biens informationnels au contraire ne sont pas consommables et n'acquièrent de nouvelle valeur économique que par l'innovation (toujours de nouvelles versions)."))

(homvort
 (ML Ivg "In den Erläuterungen zum %(q:Basisvorschlag) wird eine Überschreitung der Grenze von der Materie zur Information ausdrücklich gefordert:" (fr "Dans les commentaires du %(q:propos de base), le franchissement de la frontière entre le matériel et l'informationnel est explicitement souhaité: "))
 (ML Dbi "Des weiteren wird vorgeschlagen, %(s:Artikel 52%(pet:2) und %(pet:3) EPÜ zu streichen und in die Ausführungsordnung zu überführen), mit der Vorgabe, %(s:Computerprogramme) aus dem Ausnahmenkatalog in Artikel 52(2) EPÜ %(s:herauszunehmen).  ...  Die vorgeschlagene Überführung in die Regeln würde es .. erleichtern, diese Vorschriften bei Bedarf an rechtliche, wirtschaftliche oder technische Entwicklungen anzupassen." (fr "Il est donc proposé de %(s:supprimer l'article 52 (2) et (3) de la CBE et de modifier la numérotation des paragraphes suivants), dans le but de %(s:retirer) les %(s:programmes d'ordinateurs) de la liste des exceptions figurant à l'article 52(2). ... Si elle était adoptée, cette modification des règles du jeu faciliterait, si besoin est, l'adaptation des règlements aux développements dans des domaines comme le droit, l'économie ou la technologie. Ainsi, l'exception prévue par la loi se retrouve dans les faits ignorée."))
 (ML IWm "Inzwischen scheint sich ein breiter Konsens herauszubilden, dass %(s:Computerprogramme aus der Liste der nicht patentierbaren Erfindungen nach Artikel 52%(pet:2) EPÜ gestrichen werden sollten).  Das EPA und seine Beschwerdekammern haben das EPÜ stets so ausgelegt und angewendet, dass diese Ausnahmevorschrift einen angemessenen Schutz für softwarebezogene Erfindungen, also Erfindungen, die ein Computerprogramm zum Gegenstand haben oder einschließen, in keiner Weise verhindert.  In jüngeren Entscheidungen der Beschwerdekammern (s. T 1173/97 - Computerprogrammprodukt/IBM, ABI. EPA 1999, 609) wurde in der Tat bestätigt, dass in der Regel Computerprogramme nach dem EPÜ patentierbare Gegenstände sind.  Die geltende Ausnahmevorschrift für Computerprogramme ist damit de facto überholt.") )

(homvort
 (filters ((cr ahs 'cr-esslib00)) (ML MWh "Auch diese Formulierungen können nicht darüber hinwegtäuschen, dass das EPA nach der Verabschiedung des EPÜ 1973 bis weit in die 80er Jahren die Vorgaben des Gesetzgebers streng befolgte und die Patentierung von Erfindungen ablehnte, die %(q:ein Computerprogramm zum Gegenstand) hatten, d.h. bei denen die Veröffentlichung eines Computerprogramms genügen könnte, um das Patent zu verletzen.  Seit 1986 wurde das leidige %(q:Software-Patentierverbot) jedoch Schritt für Schritt aus wirtschafspolitischen Erwägungen heraus ausgehebelt.  Damals beschloss das EPA, dass %(q:ein Computerprogramm mit einem zusätzlichen technischen Effekt) nicht ein %(q:Computerprogramm als solches) sei und entwickelte dann eine Rechtsprechung, die kaum mehr jemand verstand.  Hierzu %(cr:schreibt) der Software-Referent der Union der Europäischen Berater für den Gewerblichen Rechtschutz, PA Jürgen Betten:" (fr "Une telle formulation ne doit pas nous faire oublier qu'après la décision de la CBE en 1973 et jusqu'aux années 80, l'O.E.B. suivait scrupuleusement les instructions du législateur, refusant de breveter toute invention %(q:comportant un programme d'ordinateuren tant qu'objet), c'est-à-dire pour laquelle la simple publication d'un programme aurait suffi à constituer une violation du brevet. Depuis 1986, la difficile %(q:interdiction de breveter les logiciels) a pourtant été grignotée peu à peu en raison de considérations d'ordre économique. A cette date, l'O.E.B. prit la décision de ne plus considérer les %(q:programmes d'ordinateurs produisant par surcroît des effets d'ordre technique) comme des %(q:programmes d'ordinateurs en tant que tels), et commença de développer une argumentation juridique incompréhensible. Ainsi %(cr:s'exprime) l'avocat Betten:")))
 (ML Dtn "%(s:Der Ausschluss von %(q:Computerprogrammen als solchen) vom Patentschutz in %(tpe:Art. 52 EPÜ:§1 PatG) wird seit langem als rechtspolitische Fehlentscheidung angesehen), zumal der Ausschluss von breiten Verkehrskreisen - bis heute - missverstanden und meist als Ausschluss von Computerprogrammen allgemein verstanden wird." (fr "L'exclusion des %(q:programmes d'ordinateurs en tant que tels) du champ de la protection par le brevet en vertu de l'article 52 de la CBE est depuis longtemps considérée comme une mauvaise décision de politique juridique, surtout que l'exclusion - jusqu'à présent - de larges circuits de distribution est le plus souvent comprise comme une exclusion générale de tous les programmes d'ordinateur. "))
 "..."    
 (filters ((un ant euip-union)) (ML lgs "Weiteren Auftrieb erhielt die europäische Diskussion durch die Resolution der AIPPI zur Frage Q133 vom 22.4.1997 in Wien, die in dem Satz zusammengefaßt werden kann: %(q:Alle auf einem Computer ablauffähigen Programme sind technischer Natur und daher patentfähig, wenn sie neu und erfinderisch sind), sowie den Round Table der %(un:UNION) am 9./10.12.1997, als im Europäischen Patentamt 100 Fachleute aus zwanzig europäischen Ländern über die Zukunft des Patentschutzes von Software in Europa diskutierten und zu einem ähnlichen Eindruck kamen wie die AIPPI.  Zudem wurde darauf hingewiesen, dass %(s:das Konzept des EPA zum %(q:technichen Charakter) weder von den Patentanmeldern noch von den nationalen Patentämtern richtig verstanden) würde.  Viele Teilnehmer machten klar, dass eigentlich alle Computerprogramme dem Wesen nach %(q:technischen Charakter) aufweisen würden.  %(s:Seit dieser Zeit wird praktisch %(q:auf allen Kanälen) daran gearbeitet, einen Weg zu finden, wie der irreführende Ausschluss von %(q:Computerprogrammen als solchen) aus dem europäischen Patentgesetz entfernt werden kann, wobei konsequenterweise auch die anderen Ausnahmeregeleungen in %(tpe:Art. 52 Abs. 2 EPÜ:§1 PatG) zur Disposition stehen.) ..." (fr "... la Table Ronde de l'%(un:UNION) qui eut lieu les 9 et 10 décembre 1997 à l'O.E.B., au cours de laquelle 100 spécialistes de vingt pays européens ont débattu de l'avenir des brevets logiciels en Europe et sont arrivés aux mêmes conclusions que l'AIPPI. Ils ont de plus fait remarquer que le concept de %(q:caractère technique) de la CBE ne serait bien compris ni des candidats au brevet, ni des Offices des Brevets de chacun des pays. Pour de nombreux participants, il était clair qu'en fait tous les programmes d'ordinateur présentaient une forme de %(q:caractère technique). Depuis lors et pratiquement %(q:sur tous les canaux), on s'efforce de trouver une solution pour éliminer de la loi européenne sur les brevets cette exclusion absurde des %(q:programmes d'ordinateur en tant que tels), %(s:et aussi, par conséquent, les autres exceptions figurant à l'art. 52 alinéa 2 de la CBE). ..."))) 
 (ML Dgi "Daneben %(s:bemüht sich die Rechtsprechung) ..., die derzeitige Gesetzesregelung so eng auszulegen, dass praktisch alle Computerprogramme - bei entsprechender Anspruchsformulierung - technischen Charakter besitzen und patentfähig sind, wenn sie neu und erfinderisch sind." (fr "Parallèlement, la jurisprudence ... s'emploie à interpréter la loi en vigueur en accordant le caractère technique grâce à une formulation appropriée, et donc la brevetabilité à pratiquement tous les programmes d'ordinateurs pourvu qu'ils soient nouveaux et innovants.")) )

(ML Deu "Diese %(q:rechtspolitisch) motivierten %(q:Bemühungen der Rechtssprechung) führten zu den vom EPA-Basisvorschlag zitierten Beschwerdekammer-Urteilen von 1999 zum Thema %(q:Computerprogrammprodukt/IBM) und %(q:Computerprogramm/IBM)." (fr "Tous ces %(q:efforts de jurisprudence) motivés par considérations de %(q:politique juridique) ont conduit aux verdicts %(q:produit programme d'ordinateur/IBM) et %(q:programme d'ordinateur/IBM) de la Chambre d'Appel de l'OEB, auxquels fait référence le %(q:propos de base) de l'O.E.B. déjà cité.")) 

(ML Mne "Patentiert werden nunmehr, anders als in der europäischen Rechtstradition des industriellen Patentwesens, nicht mehr nur bestimmte Klassen materieller %(q:Produkte und Prozesse), sondern die zur Beschreibung solcher Produkte und Prozesse dienende Information selber.  Genau genommen kann demnach schon derjenige ein Patent verletzen, der eine Patentschrift kopiert.  Denn in einer Patentschrift sollte ein Ausführungsbeispiel enthalten sein, welches %(q:den durchschnittlichen Fachmann zur vollständigen Nachvollziehung der Erfindung befähigt), d.h. im Idealfall bei Softwarepatenten ein patentverletzender Programmtext." (fr "Ainsi se trouvent monopolisées, en contradiction avec la tradition juridique européenne des brevets industriels, non seulement certaines catégories de %(q:produits et de processus) matériels, mais également l'information servant à décrire lesdits produits et processus. Pour un peu, celui qui se contenterait de recopier le texte du brevet enfreindrait alors le brevet : le texte de brevet contiendrait en effet nécessairement un exemple de réalisation, lequel pourrait %(q:permettre à un spécialiste moyen de reconstituer totalement l'invention), d'où, à la limite, un texte de brevet qui enfreint le brevet qu'il décrit.  "))

(filters ((a6 ahs 'tws-epa-bizmeth)) (ML Mnn "Hiermit ist die Grenze vom industriellen zum universellen Patentwesen überschritten.  Andere Grenzen (wie etwa die zwischen Computerprogramm und Geschäftsverfahren oder gar zwischen %(q:technischem Geschäftsverfahren) und %(q:untechnischem Geschäftsverfahren)) sauber definieren zu wollen, wäre Traumtänzerei.  Auch das EPA unternimmt erst gar nicht diesen Versuch.  Es patentiert schon heute Geschäftsmethoden und hat auch schon die Forderung nach einem %(q:zusätzlichen technischen Effekt) für überholt %(a6:erklärt).  Das EPA behält sich ferner, wie oben zitiert, die jederzeitige %(q:Anpassung an technische Entwicklungen) vor.  Darunter dürfte in der Praxis z.B. zu verstehen sein, dass etwa innovative musikalische Kompositionstechniken durch einen einfachen Beschluss des EPA-Verwaltungsrats patentierbar werden könnten, sobald genügend Anfragen beim EPA vorliegen und genügend Prüfer mit musikwissenschaftlicher Ausbildung eingestellt wurden." (fr "Ainsi que se trouve franchie la frontière entre le brevet industriel et le brevet universel. Dans un tel contexte, vouloir tracer clairement d'autres frontières comme celle qui sépare les programmes d'ordinateur des procédés commerciaux, ou encore les %(q:procédés technico-commerciaux), relève de l'utopie.D'ailleurs l'O.E.B. n'a pas cette ambition : elle brevette déjà des méthodes commerciales et %(a6:qualifie) de dépassée l'exigence d'un %(q:effet technique supplémentaire). De plus, dans le texte précedémment cité, l'O.E.B. se réserve l'%(q:adaption aux développements technologiques) qui doit être faite systématiquement. Il faut comprendre par là que dans la pratique telle ou telle technique innovante de composition musicale pourrait par exemple devenir brevetable à la suite d'une simple décision de la direction de l'O.E.B.. Il suffirait pour cela qu'il y ait suffisamment de demandes et que l'O.E.B. embauche suffisamment d'examinateurs ayant des compétences musicales.          ")))

(let ((URL (ah (absurl 'swpatsisku)))) (ML Dde "Informationsgüter und Patente bilden zusammen ein giftiges Gemisch, dessen Wirkung in mehreren diesem Schreiben beigefügten volkswirtschaftlichen Studien ausführlich beschrieben und analysiert wurde (s. %{URL}).  Der vorliegende Entgrenzungs-Vorstoß des EPA ist schon auf der begrifflichen Ebene widersprüchlich und birgt für die Praxis einige Risiken, die wir hier nur kurz aufzählen möchten:" (fr "Les biens informationnels et les brevets forment ensemble un mélange explosif, dont les conséquences sont décrites et analysées par plusieurs des études économiques jointes (s. %{URL}). Les progrès déjà accomplis par l'O.E.B. en faveur de la levée de toutes les exceptions sont déjà contestables sur le plan conceptuel, et dans la pratique ils présentent aussi des risques que nous ne pouvons qu'évoquer brièvement ici:")))

(ol
 (ML euu "einen weiteren Verlust an Gewaltenteilung, weitere Konzentration von Regelungskompetenzen, Rechtsprechungsgewalten und Zwangsmitteln in Kombination mit quasi-unternehmerischen Eigeninteressen bei einer Behörde, die schon in der Vergangenheit wenig Respekt für die Vorgaben des Gesetzgebers gezeigt und sich als de facto unkontrollierbar erwiesen hat" (en "an further loss of checks and balances, further concentration of regulative power, judicial power and coercive instruments in combination with quasi-enterpreneurial self-interest in one institution, which in recent years has already paid little respect to the will of the legislator and shown itself to be de facto uncontrollable") (fr "plus de perte de pouvoir pour la communauté, plus de concentration des compétences réglementaires, du pouvoir de jurisprudence et des moyens de contrainte, associés aux intérêts propres d'une administration qui fonctionne quasiment comme une entreprise et a déjà montré dans le passé son peu de respect envers les principes édictés par le législateur, se plaçant dans les faits en-dehors de tout contrôle."))
 (tpe (ML eeW "%(q:Patentierbarkeit ohne Grenzen)" (en "a patent system without limits") (fr "%(q:une brevetabilité sans frontières) ")) (ML pac "patentierbar werden ab sofort Informationsstrukturen, Geschäftsverfahren, Lern- und Lehrmethoden, gesellschaftliche Organisationsmethoden, geistige und mathematische Verfahren, Dienstleistungen, Banken, Handel, Handwerk, Kunstschaffen, musikalischen Kompositionstechniken und praktisch alles, wofür das Europäische Patentamt Fachprüfer einzustellen bereit ist" (en "like in the US, %(q:anything useful) will be patentable, including informational structures, business methods, education methods, methods of social organisation / social engineering, intellectual and mathematical methods, musical composition techniques etc, as long as it implies a %(q:technical) requisite such as a computer or a telephone.  Even further expansion from there is to be expected.") (fr "dorénavant, seront brevetables les structures informationnelles, les procédés commerciaux, les méthodes d'organisation d'entreprise, les algorithmes conceptuels et mathématiques, les prestations de services, les banques, le commerce, l'artisanat, les créations artistiques, les techniques de composition musicale, en fait tout ce que les examinateurs ou futurs examinateurs de l'Office Européen des Brevets se sentiront disposés à examiner ")))
 (ML dde "Aktivierung von über 10000 gesetzeswidrig angemeldeten Softwarepatenten (zu über 75% außereuropäischer Herkunft), die derzeit als Trojanische Pferde im europäischen Patentamt schlafen und auf ihre Legalisierung durch die Diplomatische Konferenz warten, bevor sie Europas Programmautoren zu Leibe rücken" (en "the unleashing of more than 10000 Trojan Horse software patents (more than 75% of which of non-european origin), which are currently sleeping at the EPO and waiting to be accorded legal validity by the Diplomatic Conference.") (fr "l'entrée en vigueur en toute illégalité tous les brevets logiciels déjà déposés - plus de dix mille, de provenance à 75 % extra-européenne - qui dorment à l'O.E.B. tels des chevaux de Troie, attendant que la conférence diplomatique les légalise pour pouvoir ensuite surveiller les auteurs européens de programmes et les poursuivre.   "))
 (tpe (ML euN "tendenzielle Aufhebung des Urheberrechts, Enteignung von Programmierern" (en "subversion of copyright, expropriation of programmers, obstruction against the diffusion of cultural techniques and the formation of new computing experts, reduction of investments in IT innovation ") (fr "une hausse tendancielle des droits d'auteur; l'expropriation des programmeurs ")) (ML EaW "Auch wer ganz eigenständig jahrelange harte Programmierarbeit geleistet hat, kann ab sofort sein Werk nicht mehr sein eigen nennen und ist stattdessen möglicherweise auf die Gnade von Patentbesitzern angewiesen, die selber nie eine Zeile Programmtext geschrieben haben.  Ebenso wie eine zu starke Besteuerung die Steuereinnahmen mindert kann, können zu rigide Eigentumsrechte auf eine Enteignung der Leistungsträger hinauslaufen." (fr "Même ceux qui, depuis plusieurs années, ont fourni tous seuls un rude travail de programmation ne pourront plus considérer leur travail comme leur appartenant. Au lieu de cela ils devront dépendre du bon vouloir de détenteurs de brevets qui n'auront eux-mêmes jamais écrit la moindre ligne de code. De même qu'une trop forte imposition peut faire baisser les rentrées fiscales, des droits de propriété trop rigides peuvent conduire à une expropriation des intéressés.  ")))
 (tpe
  (ML Emr "Einschränkung der Ausdrucksfreiheit, Umfunktionierung von Patenten zum Zwecke politischer Herrschaft" (fr "un rétrécissement de la liberté d'expression; un détournement des brevets à des fins de domination "))
  (filters ((ll ahs 'orf-lessig-swstal)) (ML Blc "Bereits heute verbietet Microsoft die Verwendung seines patentierten Videoformates ASF, um dadurch das von Filmherstellern begehrte Verbot der Privatkopie auf außergesetzlichem Wege durchsetzen zukönnen. Harvard-Verfassungsrechtler Professor Lawrence Lessig %(ll:warnt) in diesem Zusammenhang vor %(q:Software-Stalinismus).  Programmtexte wirken bisweilen wie inoffizielle Gesetzetexte der Informationsgesellschaft.  Schon heute erreichen die Großen der Branche wie z.B. Microsoft ihre Wertschöpfung vor allem durch Missbrauch der versteckten Gesetzesmacht von Software-Schnittstellen.  Diese Praxis wird durch E-Patente gefördert und gefestigt." (fr "Aujourd'hui déjà, Microsoft interdit l'utilisation de son format de vidéo breveté ASF dans le but d'imposer par des moyens illégaux l'interdiction de la copie privée souhaitée par les producteurs de cinéma. A propos de cette possibilité, Lawrence Lessig, professeur de droit constitutionnel à Harvard, %(ll:parle) de danger de %(q:stalinisme logiciel). Les textes des programmes ont parfois une valeur de textes de loi non-officiels de la société de l'information. Aujourd'hui déjà, les grands de la branche - tels Microsoft - tirent leur valeur ajoutée essentiellement de l'abus de ce \"pouvoir législatif\" caché de leurs interfaces logicielles. Les brevets logiciels encourageront et renforceront ces pratiques. "))) )
 (ML esg "Privatisierung informationstechnischer Infrastrukturen zu Lasten der allgemeinen Zugänglichkeit und öffentlichen Sicherheit, Förderung der Geheimhaltung von Programmtexten, Erstickung der wirtschaftspolitisch erwünschten Opensource-Kultur, Behinderung der (in den Koalitionsvereinbarung vom 20.10.1998 zum Ziel erklärten) %(q:beschleunigten Nutzung und Verbreitung der Informationstechniken in der Gesellschaft),  Behinderung der Heranbildung von IT-Nachwuchs, Verlangsamen des informationstechnischen Fortschritts, Verletzung von Art. 95, 151, 157 des EG-Gründungsvertrages (Vertrag von Rom) und Art 81.1b des EU-Gründungsvertrages (Vertrag von Amsterdam)" (en "privatisation of informational infrastructures at the expense of accessibility and public security") (fr "La privatisation des infrastructures de technologie de l'information au détriment de l'accessibilité et de la sécurité; la promotion du secret des textes sources des programmes; l'asphyxie de la culture Opensource pourtant utile sur le plan économique; une entrave à l'%(q:accélération de l'utilisation et de la diffusion des technologies de l'information au sein de la société), but proclamé par la communauté européenne dans l'accord du 20 octobre 1998, et à l'impulsion vers une croissance du secteur de l'informatique; un ralentissement des progrès de l'informatique; la violation des articles 95, 151 et 157 du traité fondateur de la Communauté Européenne - Traité de Rome, et de l'article 81.1b du Traité d'Amsterdam.          "))
 (ML gEe "goldene Zeiten für professionelle Abmahner und Maximalausbeuter von fragwürdigen Rechtstiteln, juristischer Bürgerkrieg, außergerichtliche Schutzgeldzahlungen mit beiderseitig gesichtswahrendem Geheimhaltungsabkommen, informelle wenn nicht gar mafiöse Strukturen des kollektiven Selbstschutzes" (en "a climate of legal insecurity, characterised by rent-seeking, extortion, out-of-court settlements, mafia-like protection hierarchies and in general high transaction costs and market entry barriers") (fr "l'âge d'or des lobbyistes professionnels et des exploiteurs maximalistes de titres juridiques douteux; la guerre civile entre juristes; des taxations hors de tout cadre juridique, avec accords de secret de pure façade; des structures informelles, voire tout à fait mafieuses, dans l'intérêt de la protection des intérêts privés d'un groupe")) 
 (ML eei "hohe Mittlungskosten und Marktzutrittsbarrieren, welche die politisch erwünschte Gründerkultur (Startups) gerade im Softwarebereich unmöglich machen, Strangulierung einer florierenden europäischen IT-Landschaft, Erzwingung wertevernichtender Aufkäufe und Fusionen zugunsten weniger i.d.R. amerikanischer Monopolisten, Abwürgen des derzeit bedeutendsten Arbeits- und Wachstumsmotors" (en "the strangulation of a prospering European IT economy, an enforcement of value-destroying buy-ups and fusions in favor of a few mainly American monopolists") (fr "des frais de courtage élevés et des seuils d'accession au marché, lesquels rendent déjà la vie impossible aux fondateurs d'entreprises \"startup\" du domaine logiciel pourtant fort utiles sur le plan économique; l'étranglement d'un paysage informatique européen florissant; des fusions ou acquisitions forcées sans aucune création de valeur, en faveur d'un petit nombre de détenteurs de monopoles - généralement américains; l'étouffement d'un moteur jusqu'à présent considérable de croissance et d'emploi  "))
)

(let ((PET (ah (lang-href 'eulux-petition))) (SWP (ah (subdirs (href 'swpat-mails) "2000" "Aug" (format "%04d.html" 6))))) (ML Uen "Der %(q:Basisvorschlag) spricht von einem %(q:breiten Konsens für die Patentierbarkeit von Computerprogrammen), während sich mittlerweile 30000 Bürger, darunter 400 leitende Angestellte von IT-Unternehmen für ein %(q:softwarepatentfreies Europa) ausgesprochen haben.  Über die Webseiten der Eurolinux-Petition %{PET} können Sie auch unschwer die Grundlagen unserer Argumentation finden.  Eine Diskussion speziell zum %(q:Basisvorschlag) hat auch im Netz stattgefunden und ist unter %{SWP} einsehbar." (fr "Le %(q:propos de base) évoque un prétendu %(q:large consensus en faveur de la brevetabilité des programmes d'ordinateurs), ignorant en cela les 30000 citoyens - parmi lesquels 400 cadres dirigeants d'entreprises d'informatique - qui se sont exprimés pour une %(q:Europe sans brevets logiciels). Sur le site Internet de la pétition Eurolinux %{PET}, vous trouverez sans peine les bases de notre argumentation. Une discussion spécialement dédiée à cette question du %(q:propos de base) a lieu également sur le net, vous pouvez vous-même la suivre à l'adresse %{SWP}. ")))

(ML Ker "Kurz zusammengefasst:  Wir fordern vorerst die unveränderte Beibehaltung des Artikels 52.  Sicherlich wäre es intellektuell reizvoll, den Artikel umzuformulieren, um ihn an Art 27 TRIPS anzunähern und gleichzeitig der Liste der Einschränkungen eine systematische Interpretation zu geben.  Diese Interpretation könnte durch folgende Gedanken gekennzeichnet sein:" (fr "En résumé: Nous voulons en premier lieu le maintien de l'article 52 inchangé. Bien sûr, ce serait intellectuellement tentant de reformuler l'article pour le rapprocher de l'article 27 du TRIPS et en même temps de donner une interprétation cohérente à la liste des exception. Une telle interprétation pourrait se reconnaître dans les lignes directrices suivantes:"))

(ol
(tan (ML Drc "Der Begriff %(q:technisch) wird im Sinne der klassischen BGH-Definition erklärt:  Anwendung beherrschbarer Naturkräfte ohne zwischengeschaltete menschliche Tätigkeit.  D.h. ein neuartiger chemischer Prozess ist technisch, das Computerprogramm, welches ihn vielleicht steuert, jedoch nicht.  Eine neuartige Naturkräfte-Anwendung ist patentierbar, aber die Veröffentlichung eines Programms zu ihrer Steuerung stellt weder eine unmittelbare noch eine mittelbare Patentverletzung dar." (fr "Dans la définition classique de la Cour Fédérale de Justice, le terme de %(q:technique) se caractérise par l'utilisation des forces naturelles contrôlables sans activité humaine interposée. Cela signifie qu'un procédé chimique original est technique, tandis que si un programme d'ordinateur le pilote il n'est pas lui-même technique.")) (ML enl "Hier liegt manchmal eine schwierige Grenze, s. BGH-Urteil %(q:Antiblockiersystem) von 1980.  Der ABS-Patentantrag wurde 1976 vom BPatG abgelehnt, weil die Erfindung im Bereich der programmierten Steuerung lag, aber 1980 vom BGH für zulässig erklärt, weil sie auf Experimenten mit Naturkräften beruhte und nicht etwa durch mathematische Operationen auf bekannte Rechenmodelle zurückgeführt werden konnte.  Gegenstand der Patentansprüche war eine Fahrzeugsteuerungsprozess, nicht ein Computerprogramm."))
(al
 (ML DTe "Der Begriff %(tpe:%(q:gewerblich):engl. industrial, frz. industriel, jap.: sangy=o = produzierendes Gewerbe) wird ebenfalls präzisiert, und zwar im Sinne des traditionellen Verständnisses von %(q:verarbeitende Industrie). Dieser Begriff ist mit dem klassischen Technikbegriff fast identisch.  Er fordert, dass es um automatisierte Prozesse zur Fertigung materieller Güter unter Einsatz von Naturkräften gehen muss." (fr "Le terme de %(tpe:%(q:industriel):en anglais industrial, en allemand gewerblich) est précisé, dans le sens où le comprend traditionnellement l'%(q:industrie de transformation). Ce terme est presque identique à celui de la définition classique du mot \"technique\". Il requiert l'existence d'un procédé automatisé de production des biens matériels (%(q:utilisation des forces naturelles)). "))
 (homvort 
  (ML Doa "Damit ist folgendem Szenario ein Riegel vorzuschieben, welches der Software-Referent der Union der Europäischen Patentberater, Patentanwalt Jürgen Betten, in einem Rundschreiben an seine Mandanten ausmalt:" (fr "Il faut mettre un terme au scénario ainsi dépeint par l'avocat Jürgen Betten, référent pour les logiciels de l'Union des Conseils en Brevets Européens dans une circulaire à ses mandants:"))
  (ML DDo "Durch die ... Entwicklung der Rechtsprechung ... hat sich das Patentrecht von der traditionellen Beschränkung auf die verarbeitende Industrie gelöst und ist heute auch für Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu befürchten, dass die deutschen Dienstleistungsunternehmen in diesen Sektoren insbesondere gegenüber der US-amerikanischen Konkurrenz ins Hintertreffen geraten." (fr "Au cours du ... développement de la jurisprudence ..., le droit des brevets s'est libéré de la traditionnelle limitation à l'industrie de transformation. Il est devenu très significatif également pour les entreprises de services des branches du commerce, de la banque, des assurances, des télécommunications etc... Il est à craindre que les entreprises allemandes de services, n'ayant pas constitué un portefeuille de brevets suffisant, se retrouvent à la traîne dans le secteur, notamment dans les situations de concurrence avec les Etats-Unis.   ")) )
 (ML DdF "Dies könnte bedeuteten, dass das Wort %(q:gewerblich) in der deutschen Fassung zu %(q:industriell) geändert wird, ähnlich wie laut %(q:Basisvorschlag) in Art 23.1.1. das Wort %(q:Funktion) in der deutschen Fassung zu %(q:Amt) geändert wird: %(q:Die englische und französische Fassung bleiben hiervon unberührt)." (fr "Ceci pourrait signifier que le mot %(q:gewerblich) dans la version allemande pourrait se changer en %(q:industriell), de même que dans le %(q:propos de base), article 23.1.1, le mot de %(q:Funktion) se voit transformé en %(q:Amt): %(q:ni la version anglaise ni la version française ne sont à modifier)."))
 (ML Nuc "NB: Der Ausschluss therapeutischer und chirurgischer Verfahren sollte ebenfalls in Art 52 verbleiben und nicht nach Art 53 transferiert werden, damit klar bleibt, dass diesem Ausschluss die Forderung nach %(q:industrieller Anwendbarkeit) zugrunde liegt und es sich nicht um eine Ad-hoc-Regelung handelt." (fr "De même, l'exclusion concernant les procédés thérapeutiques et chirurgicaux doit être maintenue dans l'article 52 et non pas être transférée à l'article 53 afin qu'il soit bien clair que cette exclusion est due à l'exigence d'%(q:utilisation industrielle), non à une distinction en tant que cas particulier. ")) )
(filters ((cs ant (bridi 'vgl (ML BCr "BGH-Urteil %(q:Chinesische Schriftzeichen) 1992")))) (ML Dsd "Die klassische %(q:Kerntheorie), die beim BGH %(cs:bis vor kurzem) zur Anwendung kam, wird bestätigt: der Kern der Erfindung muss im technischen (d.h. physischen, Naturkräfte einsetzenden) Bereich liegen.  Mithilfe der Kerntheorie wird der Bereich der Erfindungen an den Bereich der patentierbaren Gegenstände angeglichen: da informationelle Gegenstände nicht beansprucht werden können, können auch Prozesse, die sich im Einsatz einer neuartigen Informationsstruktur auf einer herkömmlichen Rechenapparatur erschöpfen (= Computerprogramme als solche) nicht als %(q:Erfindungen) im Sinne des Patentrechts gelten." (fr "La classique %(q:théorie du noyau), dont la Cour Fédérale de Justice faisait usage jusque tout récemment, le formule très précisément: le noyau de l'invention doit se situer dans le domaine technique (c'est à dire exploiter les forces naturelles), et il ne doit pas être possible de faire entrer le brevet en action (= violation du brevet) à l'aide d'un simple programme d'ordinateur. La théorie du noyau ajuste le domaine des inventions au domaine des interdictions. Elle fait en sorte que les objets informationnels ne puissent être interdits et que les procédés qui se consomment dans leur utilisation par une structure informationnelle originale au moyen d'outils traditionnels (= les logiciels en tant que tels) ne puissent valoir en tant qu'%(q:inventions) au sens du droit des brevets.")))
(ML Set "Schließlich ließe sich auch der Erfindungsbegriff im Lichte der obigen Überlegungen sinnvoll definieren.  Der vom EPA erarbeitete Begriff der %(q:technischen Lösung eines technischen Problems) wäre zu präzisieren: %(q:auf neuem Wissen über beherrschbare Naturkräfte beruhende Lösung eines durch die Herstellung materieller Güter aufgeworfenen Problems).  M.a.W.:  Problemlösungen, die mithilfe künstlicher Intelligenz aus bekannten Modellen der realen Welt abgeleitet werden könnten, sind nicht erfinderisch.  Patente sind die Belohnung für harte Laborarbeit, nicht für geschicktes Rechnen.")
(ML Dtf "Insoweit die Abgrenzung zwischen physischen und logischen Prozessinnovationen im einzelnen die Richter überfordern könnte, wäre es denkbar, hierfür die Pufferzone der %(q:weichen Patente) zu schaffen.  Erfinderische Prozesse, zu deren Umsetzung ein neuartiges Computerprogramm auf herkömmlichen Rechenapparatur genügt, könnten insoweit monopolisiert werden, wie sie nicht auf einem Universalrechner ablaufen.  Wenn z.B. ein Autohersteller nicht gerade seine Motorsteuerung kundenseitig programmierbar (und damit teurer und weniger sicher) machen will, würde er es vermutlich vorziehen, einem Patentinhaber maßvolle Lizenzgebühren zu zahlen.  Je physischer die Prozessinnovation, desto lukrativer wird das weiche Patent.  Neuere ökonomische Studien zeigen, dass %(q:weiche Schutzrechte) dieser Art wirtschaftspolitisch erwünscht sein können.  Außerdem hätte die Einführung der weichen Pufferzone den Vorteil, dass die meisten der vom EPA gesetzeswidrig gewährten Softwarepatente ohne Bruch in einen allerseits akzeptablen rechtlichen Status überführt werden könnten.")
)

(ML Ee2 "Eine Umschreibung des Art 52 in diesem Sinne wäre jedoch ein ehrgeiziges Projekt, welches diesen November kaum in angemessener Weise durchzuführen sein dürfte.  Deshalb schlagen wir vor, den Artikel 52 einfach unverändert beizubehalten und schrittweise auf eine Interpretation im obigen Sinne hinzuarbeiten." (fr "Une nouvelle rédaction de l'article 52 dans ce sens serait toutefois un objectf ambitieux, difficile à mener à bien d'ici au mois de novembre. C'est pourquoi nous proposons de conserver tout simplement l'article 52 en l'état, et de travailler peu à peu à une interprétation allant dans le sens des idées présentées ci-dessus."))

(ML OKt "Obwohl Art. 52 in seiner bestehenden Form gemäß obiger Interpretation vollkommen TRIPS-konform ist, gibt es gute Gründe, eine Revidierung von TRIPS anzustreben.  Z.B. kann die TRIPS-Vorschrift einer Mindestlaufzeit von 20 Jahren für Patente %(q:auf allen Gebieten der Technik) nicht der Weisheit letzter Schluss sein.  Ferner fehlen in TRIPS Vorschriften bezüglich der Grenzen des Patentwesens.  Gerade die Patentierung von Geschäftsverfahren wirkt der Freiheit des Welthandels entgegen, die ja das übergeordnete Ziel des TRIPS-Vertrages ist.  Und die Patentierung von Computerprogrammen untergräbt das Urheberrecht, dessen Geltung für Computerprogramme in Art 10 TRIPS festgeschrieben ist." (fr "Même si l'article 52 dans sa forme existante, dans l'interprétation que nous en donnons  ci-dessus, est en totale conformité avec le TRIPS, il existe tout de même de bonnes raisons pour réclamer sa révision. Par exemple, la règle du délai minimum de 20 ans %(q:pour tous les domaines de la technique) pourrait recevoir des aménagements. D'autre part, il manque au TRIPS des règles au sujet des limites de la brevetabilité. En particulier, la brevetabilité des procédés commerciaux entre en contradiction avec la liberté du commerce mondial, pourtant le but principal du TRIPS. Et la brevetabilité des programmes d'ordinateurs enterre le droit d'auteur dont la validité en ce qui concerne les programmes d'ordinateurs est pourtant clairement énoncée par l'article 10.  "))

(ML Dke "Die wirtschaftspolitischen Risiken der Softwarepatentierung sind altbekannt, aber selbst Stellungnahmen von prominenter Seite haben in den gesetzgebenden Kreisen des europäischen Patentwesens bisher nur ein %(q:Schweigen im Walde) ausgelöst." (fr "Les risques économiques liés à la brevetabilité des logiciels sont connus depuis longtemps. Malgré cela les prises de position de personnalités n'ont provoqué jusque là qu'un silence assourdissant parmi les personnes travaillant au problème des brevets européens."))

(filters ((sn ahs 'eu-cor99-134)) (ML FWW "Falls beim BMJ oder beim Verwaltungsrat inzwischen doch Interesse an einer Auseinandersetzung mit den wirtschaftspolitischen Risiken der Softwarepatentierung bestehen sollte, werden wir uns bemühen, unsere Argumentation vor der nächsten Verwaltungsratssitzung (5.-8. September) ausführlich auszuarbeiten.  Vorläufig möchten wir uns damit begnügen, aus einigen Stellungnahme prominenter Politiker zu zitieren:" (fr "Au cas où, au sein du Ministère de Justice ou encore du Conseil Administratif, un intérêt se ferait tout de même jour pour un débat sur les risques économiques de la brevetabilité des logiciels, nous nous efforcerons de retravailler complètement notre argumentation avant la réunion du Conseil le 4 septembre prochain. En attendant, nous nous contenterons de citer quelques prises de position de responsables politiques connus: ")))

(sects
(regikom (ML CtW "COR" (de "Ausschuss der Regionen der Europäischen Union") (fr "Le Comité des Régions de l'Union Européenne") (en "Committee of the Regions of the European Union"))

(tpe
 (ml 
  "OPINION of the Committee of the Regions of 18 November 1999 on %(q:The competitiveness of European enterprises in the face of globalisation - How it can be encouraged)"
  (de "STELLUNGNAHME des Ausschusses der Regionen zum Thema %(q:Wettbewerbsfähigkeit der europäischen Unternehmen angesichts der Globalisierung - Wie man sie fördern kann)")
  (fr "AVIS du Comité des régions du 18 novembre 1999 sur %(q:La compétitivité des entreprises européennes face à la mondialisation - comment l'encourager)")
  (it "PARERE del Comitato delle regioni del 18 novembre 1999 in merito alla Comunicazione della Commissione %(q:Incentivi a favore della competitività delle imprese europee e fronte della globalizzazione)")
  (da "REGIONSUDVALGETS UDTALELSE af 18. november 1999 om %(a:Europæisk erhvervslivs konkurrenceevne i den globale økonomi - Forslag til styrkelse af erhvervslivets konkurrenceevne)") )
 (spaced (tpe (ml "COM" (de "KOM")) "1998") "718" (ml "final" (it "def.") (de "Endfassung") (da "endelig udg."))) )

(filters ((sn ahs 'eu-cor99-134)) (ML vie "The CoR officially adopted and %(sn:published) this study report in november 1999.  This means that it was checked and approved by 200 regional politicians" (de "Der AdR hat diese Studie nach ausführlicher Prüfung im November 1999 als offizielles Standpunktpapier übernommen und %(sn:veröffentlicht).  Damit trägt sie die Unterschrift von über 200 führenden europäischen Regionalpolitikern, einschl. Stoiber, Teufel, Diepgen u.v.m.") (fr "Au mois de novembre 1999, à la suite d'un examen approfondi, le Comité des Régions a adopté et %(publié) la présente étude en tant que son point de vue officiel. L'étude porte donc la signature de plus de 200 dirigeants politiques des régions d'Europe, y compris Stoiber, Teufel, Diepgen et bien d'autres.")))

(cite (blockquote
(ml  
 "The COR would draw the Commission's attention to the dangers that might arise from systematically relying on patents in the field of intellectual property, since patent protection is not universal. This applies mainly to the new technologies, and especially to information technologies and the life sciences, which are the subject of a detailed and heated debate." 
 (fr "Le Comité des régions tient à attirer l'attention de la Commission sur le caractère non universel de la protection par les brevets et sur les dangers qui peuvent résulter d'une systématisation du recours aux brevets en matière de propriété intellectuelle. Ces questions concernent principalement les nouvelles technologies et avant tout les technologies de l'information et les sciences du vivant, lesquelles sont l'objet d'un débat riche et passionné.") 
 (de "Der Ausschuß der Regionen möchte die Aufmerksamkeit der Kommission auf die Tatsache lenken, daß der Patentschutz nicht universal ist, sowie auf die Gefahren der systematischen Patentierung des geistigen Eigentums hinweisen. Diese Fragen betreffen hauptsächlich die neuen Technologien und vor allem die Informationstechnologien und die Biowissenschaften, die Gegenstand einer inhaltsreichen und leidenschaftlichen Diskussion sind.")
 (it "Il Comitato richiama l'attenzione della Commissione sul carattere non universale della protezione tramite brevetti e sui rischi che possono derivare da un'applicazione sistematica del ricorso ai brevetti in materia di proprietà intellettuale. Tali questioni riguardano principalmente le nuove tecnologie e, in primo luogo, le tecnologie dell'informazione e le scienze della vita, che sono al centro di un dibattito ricco e animato.")
 (da "Regionsudvalget ønsker at henlede Kommissionens opmærksomhed på patentbeskyttelsens manglende universalitet og på de farer, der kan ligge i en systematisk anvendelse af patenter på intellektuel ejendom. Spørgsmålet er især aktuelt for de nye teknologier, især informations- og bioteknologierne, som er genstand for en rig og lidenskabelig debat.")
)

(ml 
 "In the case of software, the debates that have taken place since the 1970s in the main countries concerned have all led to a copyright system, although such a legal framework is not entirely suited to the sector's specific requirements. The European Directive of 1 January 1993 has shown some wisdom in encouraging interoperability among programmes so as to counteract the anti-competitive strategies of seeking a dominant position. But for several years now, US case law has been led into allowing the issuing of patents for software %(q:components), a practice to which it had previously been hostile. And the US is putting increasing pressure on Europe to allow software patenting."
 (fr "En ce qui concerne le cas du logiciel, les débats qui ont eu lieu dès les années soixante-dix dans les grands pays concernés ont tous conclu au recours au système du droit d'auteur, bien que ce système ne constitue qu'un cadre juridique imparfait quant aux spécificités de ce secteur. La Directive européenne du 1er janvier 1993 a notamment montré une voie de sagesse en vue de favoriser l'interopérabilité des programmes afin de contrecarrer les stratégies anticoncurrentielles de recherche de position dominante. Mais depuis plusieurs années, la jurisprudence américaine a été conduite à accorder la délivrance de brevets pour des %(q:composants) logiciels, à laquelle elle avait été jusque là hostile. Et la pression américaine sur l'Europe se fait de plus en plus vive pour que la brevetabilité soit acceptée au niveau européen.")
 (de "Im Fall der Software haben die seit den siebziger Jahren in den großen, von dem Thema betroffenen Staaten geführten Diskussionen alle zu dem Ergebnis geführt, daß das System der Urheberrechte gelten soll, obwohl es den Besonderheiten des Bereichs nicht umfassend gerecht wird. Die europäische Richtlinie vom 1. Januar 1993 zeigte insbesondere den goldenen Mittelweg zur Förderung der %(q:Interoperabilität) von Programmen, um wettbewerbsschädliche Strategien mit dem Ziel einer marktbeherrschenden Stellung zu konterkarieren. Seit mehreren Jahren ist die amerikanische Rechtsprechung dazu übergegangen, die Ausstellung von Patenten für %(q:Softwareelemente) zu gewähren, was sie bis dahin abgelehnt hatte. Und der Druck Amerikas auf Europa zur Gewährung der Patentfähigkeit auf europäischer Ebene nimmt immer mehr zu.")
 (it "Nel caso del software, i dibattiti svoltisi a partire dagli anni '70 nei grandi paesi interessati hanno tutti portato all'impiego del sistema del diritto d'autore, benché tale sistema costituisca soltanto un quadro giuridico imperfetto per regolare le specificità del settore. La direttiva europea del 1° gennaio 1993 ha in particolare indicato una via adeguata per favorire l'interoperabilità dei programmi al fine di contrastare le strategie anticoncorrenziali di ricerca di posizione dominante. Tuttavia, da diversi anni la giurisprudenza americana è stata indotta ad accordare la concessione di brevetti per i %(q:componenti) del software, cui si era precedentemente opposta, e diventa sempre più forte la pressione americana sull'Europa affinché la brevettabilità venga accettata a livello europeo.")
 (da "På softwareområdet har 1970'ernes debatter i de store lande alle ført til anvendelse af ophavsretsystemet, selvom dette system er en lovramme, der kun ufuldstændigt afspejler denne sektors særtræk. Med EU-direktivet af 1. januar 1993 slog man ind på en klog vej, idet man tilskyndede til interoperabilitet mellem programmerne for at bekæmpe konkurrencebegrænsende strategier, der tilstræbte en dominerende stilling på markedet. Amerikansk lovgivning har imidlertid i flere år accepteret patenter på software-%(q:komponenter), hvilket den før gik imod. Der lægges stadig større amerikansk pres på Europa for accept af denne patentmulighed på europæisk plan.")
)

(ml
 "The stakes here are extremely high. Such a practice would threaten the progress of innovation in this industry, since it would lead to a compartmentalisation of knowledge and procedures, thereby preventing any interaction. The multitude of patents registered and granted in the USA include a very large number of procedures, or even algorithms. Many of them seem a long way from satisfying the criteria of novelty and originality which, theoretically, are the basis for issuing a patent."
 (fr "Or l'enjeu est ici des plus importants. Une telle pratique menace la dynamique de l'innovation dans cette industrie, dans la mesure où elle conduit à un cloisonnement des savoirs et des procédures, qui interdit toute pratique combinatoire. Ainsi parmi la multitude de brevets déposés et accordés aux Etats-Unis, on trouve un très grand nombre de procédures, voire d'algorithmes. Beaucoup d'entre eux apparaissent éloignés des critères de nouveauté et d'originalité, qui conditionnent en principe la délivrance d'un brevet.")   
 (de "Dabei sind die Risiken hier sehr hoch. Ein derartiges Vorgehen bedroht die Innovationsdynamik in dieser Industrie, da sie zu einer Isolierung der Kenntnisse und Verfahren führt, die jegliche Kombination unterbindet. So finden sich unter der Vielzahl in den Vereinigten Staaten angemeldeter und eingetragener Patenten zahlreiche Verfahren oder sogar Algorithmen. Viele scheinen von den Merkmalen Neuheit und Originalität, die eigentlich Voraussetzung für die Ausstellung eines Patents sind, weit entfernt zu sein.")
 (it "La posta in gioco è in questo caso di importanza fondamentale. Una pratica di questo tipo minaccia la dinamica dell'innovazione nel settore, nella misura in cui conduce ad una compartimentazione delle conoscenze e delle procedure, che impedisce qualsiasi pratica combinatoria. Così, nella molteplicità dei brevetti depositati e accordati agli Stati Uniti, si trovano un gran numero di procedure, o anche algoritmi, molti dei quali appaiono lontani dai criteri di novità e originalità che sono, in linea di principio, le condizioni per la concessione di un brevetto.")
 (da "Meget står her på spil. En sådan praksis truer innovationsdynamikken i denne industri, idet den fører til adskillelse af viden og procedurer, som umuliggør en kombinatorisk praksis. Man finder således blandt mængden af patenter, der er ansøgt om og udstedt i USA, et stort antal procedurer og algoritmer. Mange af dem synes langt fra de nyheds- og originalitetskriterier, som i princippet er en betingelse for at kunne udtage patent.")
)

(ml 
 "If the issuing of patents for software became institutionalised, it would strengthen the dominant position of the biggest US market leaders in the sector. It would be a direct threat to the huge number of innovating smaller firms in Europe, the USA and in other countries. Finally, it would be a very severe handicap for the European software industry, which has a hard time remaining commercially competitive despite its high level of competence." 
 (fr "L'institutionnalisation de la délivrance de brevets dans le domaine du logiciel constituerait une arme pour le renforcement de la position dominante des plus gros leaders américains du secteur. Elle constituerait une menace directe pour l'immense population des PME innovantes dans cette activité, tant en Europe qu'aux USA et dans les pays tiers. Elle serait enfin un très lourd handicap pour l'industrie européenne du logiciel dont la compétitivité a beaucoup de mal à s'affirmer commercialement, malgré le haut niveau des compétences qui s'y déploient.")
 (de "Sollte die Ausstellung von Patenten für Software zur festen Einrichtung werden, so wäre das eine Waffe zur Stärkung der Vormachtstellung der größten amerikanischen Marktführer in diesem Bereich. Sie wäre eine direkte Bedrohung für die Masse der innovatorischen KMU, sowohl in Europa als auch in den Vereinigten Staaten und anderswo.  Schließlich würde sie ein sehr großes Handicap für die europäische Softwareindustrie darstellen, die trotz des hier eingesetzten hochwertigen Fachwissens große Schwierigkeiten hat, im Handel wettbewerbsfähig zu sein.")
 (it " L'istituzionalizzazione della concessione di brevetti nel campo del software costituirebbe un'arma per rafforzare la posizione dominante delle più importanti società americane del settore e rappresenterebbe una minaccia diretta per la moltitudine di PMI innovatrici in tale attività, sia in Europa che negli USA e nei paesi terzi. Infine, sarebbe un impedimento considerevole per l'industria europea del software, che ha grandi difficoltà a diventare competitiva dal punto di vista commerciale, nonostante il livello elevato delle competenze di cui dispone.")
 (da "Institutionalisering af patentudstedelse på softwareområdet ville styrke de førende amerikanske firmaers dominerende markedsstilling. Det ville udgøre en direkte trussel for de mange SMV'er, der er nyskabende på dette felt, både i Europa og i USA samt i tredjelande. Det ville endvidere være et tungt handicap for den europæiske softwareindustri, som har svært ved at befæste sin konkurrenceevne kommercielt på trods af det høje kvalifikationsniveau.")
)
))
;regikom
)

(wolf (commas "Margareta Wolf" "MdB" (ML Wse "Wirtschaftspolitische Sprecherin der Grünen Fraktion" (fr "porte-parole des Verts pour les questions économiques")))

(filters ((mw ahs 'mwolf-000614)) (ML F4U "Frau Wolf %(mw:sagte) am 14. Juni 2000 auf einer IT-Unternehmensgründer-Tagung:" (fr "Madame Wolf %(mw:s'est exprimée) le 14 juin 2000 lors d'une session de créateurs d'entreprises informatiques:")))

(cite (blockquote
(ML Vir "Gründer werden durch die Debatte über die mögliche Patentierbarkeit von Software und Geschäftsideen auch in Europa verunsichert.  Hier muss schnell Klarheit geschaffen werden." (fr "Les créateurs sont confrontés à une situation d'insécurité du fait de la possible brevetabilité des logiciels et des concepts commerciaux. Il faut faire la lumière au plus vite sur ce sujet."))

(ML CWa "Computerprogramme %(q:als solche) sind nach dem Europäischen Patent-Übereinkommen (EPÜ) nicht patentierbar. In den USA dagegen ist grundsätzlich alles menschengemachte patentierbar." (fr "D'après la Convention Européenne des Brevets (CBE), les programmes d'ordinateurs ne sont pas brevetables %(q:en tant que tels). Aux Etats-Unis au contraire, tout ce qui peut être créé par l'homme est par principe brevetable."))

(ML Bwd "Bei Software-Entwicklern und in der Open-Source-Szene besteht erhebliche Verunsicherung, weil befürchtet wird, dass über die Novelle des EPÜ und eine angekündigte Richtlinie der Europäischen Kommission in Europa amerikanische Verhältnisse eingeführt werden sollen." (fr "Chez les développeurs de logiciels et sur la scène Open Source, règne une grande insécurité. On craint en effet que l'amendement de la CBE et la doctrine juridique de la Commission Européenne qui s'annonce n'aboutisse à l'introduction en Europe des habitudes américaines. "))

(ML IgA "In den USA wird der Wettbewerb bereits erheblich auch durch die Patentierung von Geschäftsideen behindert. Viel zitiertes Beispiel ist das Patent von Amazon auf das one-click-Verfahren bei der Bestellung von Gütern im Internet." (fr "Aux Etats-Unis, l'exercice de la concurrence est déjà considérablement entravé par la brevetabilité des concepts commerciaux. Un exemple souvent cité est le brevet déposé par Amazon sur le procédé \"one-click\" lors de la commande de produits sur l'Internet."))

(ML Asm "Allerdings ist bereits in den letzten Jahren in Europa Software zunehmend als Bestandteil technischer Verfahren patentiert worden, denn: Technische Verfahren, die Computerprogramme beinhalten, sind patentierbar. Beispiel dafür ist eine computergesteuerte Werkzeugmaschine, die insgesamt patentierbar ist. Es mangelt derzeit an eingehenden ökonomischen Analysen, die die Wirkungen einer möglichen Patentierung von Software beschreiben. Wir sehen allerdings die Gefahr einer weiteren Verstärkung von Bürokratie mit dem Effekt, dass dringend notwendige Innovationen behindert werden." (fr "De toutes façons, ce n'est qu'au cours des dernières années en Europe que le logiciel est devenu brevetable comme composant de procédés techniques. En effet, les procédés techniques qui contiennent les programmes d'ordinateurs sont brevetables. Par exemple, une machine outil pilotée par ordinateur constitue un objet entièrement brevetable. Il nous manque pour l'instant des analyses économiques détaillées des effets d'une possible brevetabilité des logiciels. Nous voyons tout de même clairement le danger d'un renforcement supplémentaire de la bureaucratie qui aurait pour effet de freiner les innovations les plus nécessaires."))

(ML DDf "Die Ermöglichung der Patentierung von Software würde darüber hinaus erhebliche technische und administrative Probleme schaffen: in der Zeit, die Anmeldung eines Patentes derzeit benötigt (derzeit 2 Jahre), ist das Patent längst veraltet. Die Dokumentation der Patente wäre extrem aufwendig. Kleine und mittlere Unternehmen würden durch die Patentierung benachteiligt: Große Firmen, die über die Ressourcen verfügen, die Patententwicklung zu verfolgen und Patente anzumelden könnten auf diese Weise zusätzliche Erträge erwirtschaften. Der Wettbewerb würde sich von der schnellen Umsetzung von Innovationen auf juristische Streitereien verlagern und der technische Fortschritt würde behindert werden. Das muss verhindert werden!" (fr "De plus, le fait de rendre les logiciels brevetables soulèverait de considérables problèmes techniques autant qu'administratifs: avec les deux ans de délai actuel, quand le brevet serait enfin déposé il serait depuis longtemps obsolète. Le travail de documentation des brevets coûterait très cher. Les petites et moyennes entreprises seraient désavantagées par la brevetabilité: seules les grandes entreprises disposent des ressources nécessaires pour rester en phase avec le développement des brevets. Seules les grandes entreprises pourraient déposer des brevets et en tirer des revenus supplémentaires. La concurrence se jouerait sur des disputes juridiques et non plus sur la mise en oeuvre rapide des innovations. Le progrès technique s'en trouverait entravé. Il faut empêcher cela !"))
))
)

(tauss (commas "Jörg Tauss" (ML MsW "MdB" (fr "député fédéral")) (ML Vns "Vorsitzender des Parlamentarischen Unterausschusses für die Neuen Medien" (fr "Président de la Commission Parlementaire sur les nouveaux media")))

(filters ((vv ah "http://www.vov.de")) (ML Dxf "Der %(vv:Virtuelle Ortsverein der SPD) unterstützt die Eurolinux-Petition.  Sein Gründer Jörg Tauss gab am 8. August 2000 auf Anfrage des FFII folgend vorläufige Grundsatzerklärung zum %(q:Basisvorschlag) ab:" (fr "L'%(vv:association viruelle locale du SPD) soutient la pétition Eurolinux. Le 8 août dernier, en réponse à une question de la FFII, son fondateur Jörg Tauss remet le commentaire suivant au sujet du %(q:propos de base):")))

(cite (blockquote
(ML Itm "In technologiepolitischen Fachkreisen hört man immer wieder die Behauptung, das Patentsystem müsse auf gewisse Bereiche der Informationstechnik ausgeweitet werden, weil sonst deren Investitionen nicht genügend geschützt würden.  Diese Behauptung wurde bisher allerdings immer nur als abstrakte Grundwahrheit weitergegeben und niemals anhand von Tatsachen der deutschen oder europäischen IT-Wirtschaft belegt." (fr "Dans les milieux de spécialistes des questions de politique des technologies, on entend constamment affirmer que le système des brevets doit être étendu à certains domaines des technologies de l'information parce que sans cela les investissements ne seraient pas suffisamment protégés. Jusqu'ici, cette affirmation a toujours été présentée comme une vérité abstraite et n'a été étayée par aucun fait prouvé de l'économie allemande ni européenne. "))

(ML ScW "Selbst wenn es gelänge, Bereiche der Informationstechnik zu finden, in denen Patente nachweislich vorteilhaft wirken oder gewirkt haben, müsste man noch immer untersuchen, ob eventuelle schädliche Nebenwirkungen der Patentierung diese Vorteile nicht überwiegen." (fr "Et même s'il était possible de trouver des domaines des techniques de l'information pour lesquels les brevets auraient, ou auraient eu par le passé, un effet favorable manifeste, encore faudrait-il se demander si d'éventuels effets secondaires néfastes de la brevetabilité n'en annuleraient pas les avantages."))
 
(ML Aiz "Aber während bei der Legislative noch vollkommene Unklarheit herrscht, schreitet die Judikative bereits zur Tat, gewährt Tausende von Softwarepatenten und drängt auf Änderung der Gesetzesregeln.  Es ist daher höchste Zeit für uns als Gesetzgeber, uns um diese Fragen zu kümmern." (fr "Mais, alors que le pouvoir législatif est encore dans l'incapacité de faire la lumière sur ce sujet, le pouvoir judiciaire passe déjà à l'action, accordant des milliers de brevets sur des logiciels et essayant d'imposer une modification des règles légales. Il est donc grand temps pour nous les législateurs de nous occuper de ces questions. ")) 
)) )

(ledeaut (commas "Jean-Yves Le Déaut" (ML dih "député socialiste de Meurthe-et-Moselle" (de "Abgeordneter der Moselregion in der Französischen Nationalversammlung") (fr "Député de la Moselle")))

(filters ((oa ahs 'osslaw-articles) (lb ahs 'ledeaut-lettre-brevets)) (ML Dtr "Le Déaut ist Mitinitiator des französischen %(oa:Gesetzesentwurfes) für ein %(q:Recht auf Kompatibilität) und die Durchsetzung von Offenheitsstandards in der Öffentlichen Verwaltung.  Er sieht die Münchener und Brüsseler Patentpolitik als eine Gefahr für sein Anliegen und sandte daher im Juli 2000 einen %(lb:Offenen Brief) an zahlreiche zuständige Politiker in Frankreich, den wir hier übersetzen:" (fr "Le Déaut est co-initiateur du %(oa:projet de loi) français qui impose un %(q:droit à la compatibilité) ainsi que l'usage des standards du logiciel libre dans les administrations publiques. La politique des brevets, telle qu'elle est menée à Münich et à Bruxelles, menace les buts de son action. Il a d'ailleurs expédié à de nombreux responsables politiques français, en juillet dernier, une %(lb:lettre ouverte) que nous reproduisons ici:")))

(cite (blockquote
  (ML Lio "Le système de brevet s'est étendu depuis quelques années bien au-delà de son domaine de légitimité historique, économique et éthique. Cette extension est le résultat de décisions de jurisprudence de l'Office Européen des Brevets (OEB) qui sont parfois prises en contradiction avec l'esprit de la loi, telle qu'elle a été ratifiée par le législateur, et le plus souvent sans que les Etats signataires de la convention de Munich ne disposent des moyens de contrôler la portée économique et sociale de ces décisions. En particulier, je considère qu'en affirmant qu'un « programme d'ordinateur présentant des effets techniques » n'est pas « un programme d'ordinateur en tant que tel » et peut donc faire l'objet d'un brevet, l'Office des Européen des Brevets a clairement abusé de son pouvoir. L'OEB a en effet développé une jurisprudence manifestement contraire à la convention internationale qu'il est sensé appliquer, puisque tous les programmes d'ordinateurs ont un effet technique, comme l'ont très justement rappelé dès 1997 les experts européens en propriété industrielle réunis lors de la table ronde sur la « brevetabilité des logiciels » qui s'est tenue à Munich." (de "Das Patentsystem hat sich in den letzten Jahren weit über den Bereich seiner historischen, ökonomischen und moralischen Existenzberechtigung hinaus ausgedehnt.  Diese Ausdehnung ist das Ergebnis von Gerichtsentscheidungen des Europäischen Patentamts (EPA), die bisweilen im Gegensatz zum Geist der Gesetze stehen, wie sie von den Gesetzgebern verabschiedet wurden, wobei die Unterzeichnerstaaten des Münchener Übereinkommens meist nicht über die nötigen Mittel verfügen, um die wirtschaftlichen und gesellschaftlichen Auswirkungen dieser Entscheidungen zu kontrollieren.  Insbesondere glaube ich, dass das EPA, indem es allen Ernstes behauptete, ein %(q:Computerprogramm mit technischen Wirkungen) sei kein %(q:Computerprogramm als solches) und könne daher zum Gegenstand von Patentansprüchen werden, eindeutig seine Macht missbraucht hat.  Das EPA hat eine Rechtsprechung entwickelt, die in offensichtlichem Gegensatz zu dem internationalen Vertrag steht, mit dessen Einhaltung es betraut wurde.   Denn alle Computerprogramme haben einen technischen Effekt, wie die Europäischen Patentberater zu Recht anmahnten, die sich 1997 im EPA zu einem %(q:Runden Tisch) versammelten.") (fr ""))
  (ML Cts "Cette extension incontrôlée du système de brevet dans le domaine du logiciel contribue à mettre en péril de façon croissante les entreprises informatiques européennes, les auteurs de logiciels libres et les principes fondamentaux qui ont permis l'essor de la société de l'information. Plus de 10,000 brevets logiciels ont été déposés depuis 10 ans à l'Office Européen des Brevets par des astuces de procédure cautionnées par l'OEB alors même que les guides distribués depuis 10 ans par les offices nationaux de brevets rappellent clairement que les programmes d'ordinateur ne peuvent être brevetés. Plus de 75% de ces brevets ont été déposés par des entreprises non-européennes. Nombre de ces brevets logiciels portent sur des méthodes de commerce électronique, voire des méthodes d'organisation des entreprises ou des méthodes éducatives." (de "Diese unkontrollierte Ausweitung des Patentsystems in den Bereich der Software hinein gefährdet in zunehmendem Maße die europäischen IT-Unternehmen, die Autoren freier Software und die Grundarchitektur der Informationsgesellschaft.  Mehr als 10000 Softwarepatente wurden in den letzten 10 Jahren bei den nationalen Patentämtern durch vom EPA inspirierte juristische Winkelzüge angemeldet, während selbst die Anmeldungsanleitungen der Patentämter noch immer klar sagen, dass Computerprogramme nicht patentierbar sind.  Mehr als 75% dieser Patente wurden von außereuropäischen Unternehmen angemeldet.  Nicht wenige dieser Softwarepatente beziehen sich auf Verfahren des elektronischen Geschäftsverkehrs sowie Organisations- und Lehrmethoden.") (fr ""))
 (ML Mev "Mais, comme il est rappelé dans les manuels de référence juridique tels que le « Lamy Informatique », ces brevets n'ont de valeur que celle que l'on veut bien leur accorder en raison de la contradiction manifeste qui existe aujourd'hui entre le droit positif et le système jurisprudentiel de l'OEB. En cas de contentieux, il n'est pas certain qu'un juge national accepterait la validité de ces brevets en raison de leur objet, manifestement contraires à l'esprit de la loi.  Les détenteurs de brevets logiciels, de brevets sur le commerce électronique et de brevet Internet n'attendent donc qu'une chose pour attaquer les acteurs français et européens de la nouvelle économie: une révision de la convention de Munich qui supprimerait l'exception sur les programmes d'ordinateurs." (de "Juristische Standardwerke wie «Lamy Droit Informatique» weisen allerdings darauf hin, dass diese Patente angesichts der offensichtlichen Widersprüche zwischen dem geltenden Gesetz und der Rechtsprechung des EPA keinerlei Wert haben außer demjenigen, den die jeweiligen Gerichte ihnen zuzuerkennen bereit sind.  Im Falle eines Rechtsstreits besteht kein Verlass darauf, dass ein nationaler Richter die Ansprüche dieser Patente anerkennen würde, da sie sich offensichtlich auf Gegenstände beziehen, deren Patentierung dem Geist des Gesetzes widerspricht.  Daher warten die Inhaber der Softwarepatente, der E-Geschäftsmethoden-Patente und der Internet-Patente nur noch auf eines, bevor sie sich entschließen, die französischen und europäischen Protagonisten der Neuen Wirtschaft anzugreifen:  die Revision des Münchener Übereinkommens, die sich anschickt, den Ausschluss der Computerprogramme von der Patentierbarkeit zu beseitigen.") (fr ""))
 (ML AWi "Aussi, je vous serais reconnaissant de bien vouloir user dans les consultations nationales, européennes ou mondiales à venir, de tous les moyens qui sont en votre pouvoir pour exiger:" (de "Ich würde mich Ihnen verbunden fühlen, wenn Sie in den kommenden Konsultationen auf nationaler, europäischer und globaler Ebene alle Ihnen zu Gebote stehenden Mittel einsetzen würden, um zu verlangen,") (fr ""))
 (ol
  (ML dan "de ne pas modifier en novembre 2000 l'article 52 de la convention de Munich, afin de ne pas activer le %(q:cheval de troie) qui sommeille actuellement à l'OEB où de nombreux brevets Internet accordés abusivement à des entreprises non-européennes peuvent menacer du jour au lendemain la nouvelle économie française et européenne." (de "dass im November 2000 Art 52 des Münchener Übereinkommens nicht geändert wird und somit das Trojanische Pferd nicht aktiviert wird, das derzeit beim EPA schläft, wo zahlreiche missbräuchlich an außereuropäische Unternehmen vergebene Internet-Patente von einem Tag auf den anderen die Neue Wirtschaft Frankreichs und Europas bedrohen können.") (fr ""))
  (ML qlW "que soient garantis par la loi un %(q:droit à diffuser ses propres oeuvres originales) (logiciels y compris) ainsi qu'un « droit à la compatibilité » tel qu'il est défini dans la proposition de loi déposée avec MM. Paul, Cohen et Bloche (www.osslaw.org." (de "dass ein %(q:Recht, seine %(tpe:eigenen Werke:einschließlich Computerprogramme) zu verbreiten) und ein %(tpe:%(q:Recht auf Kompatibilität):wie im %(tpe:Gesetzesvorschlag der Abgeordneten Le Déaux, Paul, Cohen und Bloche:www.osslaw.org) ausgeführt) gesetzlich verankert werden") (fr ""))
  (ML qnW "que les termes « technique », « application industrielle » et « programme en tant que tel » soient clarifiés de façon à ce que toute oeuvre, tout produit informationnel immatériel (y compris un logiciel sur un support d'information) ne soit ni admis dans le champ de la brevetabilité ni dans celui de la fourniture de moyen de contrefaçon de brevet." (de "dass die Begriffe %(q:technisch), %(q:gewerbliche Anwendung) und %(q:Programm als solches) derart geklärt werden, dass kein Informationswerk und kein immaterielles Produkt (einschließlich of Informationssystemen laufende Software) als unmittelbar oder mittelbar patentverletzender Gegenstand in Betracht kommen kann.") (fr ""))
  (ML qWo "que tout produit matériel, extension d'un produit informationnel immatériel (ex. un lecteur MP3) puisse être breveté à condition que soient satisfaits les critères de nouveauté, de technicité et d'application industrielle de ce produit matériel, considéré indépendamment des éléments logiciels qu'il exploite." (de "dass materielle Produkte oder materielle Erweiterungen von immateriellen Produkten (z.B. MP3-Abspielgeräte) patentiert werden könnten, sofern die Kriterien der Neuheit, Technizität und industriellen Anwendbarkeit dieses materiellen Produktes erfüllt werden, und zwar unabhängig von den darin verwendeten Softwareelementen betrachtet.") (fr ""))
  (ML qdi "que soit lancé dans les plus brefs délais un débat ouvert et démocratique fondé sur des études scientifiques détaillées des effets économiques et sociaux induits par une extension du système des brevets à la société de l'information." (de "dass unverzüglich eine offene und demokratische Diskussion auf Grundlage ausführlicher wissenschaftlicher Studien über die wirtschaftlichen und gesellschaftlichen Wirkungen des Patentsystems auf die Informationsgesellschaft in Angriff genommen wird.") (fr ""))
  (ML qey "que soit mise en place une base de données de brevets complète, librement accessible sous forme de contenu libre et de logiciels libres, afin de donner aux PME les moyens de faire face aux risques de contentieux de brevets en Europe et dans le monde." (de "dass eine frei verfügbare und über freie Software zugängliche umfassende Patentdatenbank aufgebaut wird, um den KMU die Mittel an die Hand zu geben, mit Patentgefahren in Europa und der Welt besser fertig zu werden.") (fr ""))
)
(ML Aen "Aucune étude n'ayant été publiée par l'Office Européen des Brevets pour justifier l'intérêt économique de l'extension au logiciel de la brevetabilité, alors même que des économistes ont démontré que le système de brevet pouvait aboutir à une diminution de l'innovation dans l'économie du logiciel, il me semblerait également opportun de commanditer un audit de l'Office Européen des Brevets afin de déterminer les moyens de mieux contrôler les décisions de cet organisme et de s'assurer qu'elles sont bien conformes à l'intérêt général et au principe fondamental d'impartialité de la justice." (de "Da das Europäische Patentamt keinerlei Studie publiziert hat, um die Ausweitung der Patentierbarkeit auf Computerprogramme zu rechtfertigen, obwohl Wirtschaftswissenschaftler bewiesen haben, dass das Patentsystem zu einer Minderung der Innovation in der Softwareökonomie führen könnte, scheint es mir ferner an der Zeit, eine Betriebsprüfung des Europäischen Patentamtes in Auftrag zu geben, um nach Mitteln zu suchen, wie man die Entscheidungen dieser Institution besser kontrollieren und in Einklang mit dem Allgemeininteresse und den Grundprinzipien einer unparteiischen Rechtsprechung bringen kann.") (fr "")) ))
; ledeaut
)
; sects
)

(ML Whr "Wir glauben, dass diese Stimmen ebenso wie die Stimmen von ca 30000 Unterzeichnern der Eurolinux-Petition mehr Gewicht haben sollten als der Wunsch einiger Patentfachleute, am Wohlstand der Neuen Wirtschaft zu partizipieren." (fr "Nous pensons que cette voix, ajoutée à celle des 30000 signataires de la pétition Eurolinux, devrait avoir plus de poids que le souhait de quelques spécialistes des brevets en ce qui concerne les fruits de la nouvelle économie."))

(lin 
 (ML Wbe "Wir bitten um" (fr "Nous demandons"))
 (ul
  (ML AWr "Akkreditierung des FFII e.V. und der Eurolinux-Allianz als Teilnehmer der diplomatischen Konferenz im November" (fr "l'accréditation de l'association FFII et de l'alliance Eurolinux pour participer à la conférence diplomatique de novembre prochain"))
  (ML uul "unsere frühestmögliche Einbeziehung in die kommenden Konsultationen, Rederecht für einen Vertreter der Eurolinux-Allianz" (fr "notre présence dès que possible aux prochaines consultations, et le droit à la parole pour un représentant de l'alliance Eurolinux"))
  (ML efe "eine baldige Stellungnahme des BMJ zu den Vorschlägen in diesem Brief, einschließlich den Forderungen der zitierten Politiker" (fr " une prise de position prochaine du  Ministère de Justice sur les propositions de cette lettre, y compris sur les demandes des responsables politiques cités")) )
)

(ML Wit "Wir wären Ihnen als Bürger unendlich verbunden, wenn Sie sich bei den kommenden Konsultationen bereit fänden, der Patentlobby zu widerstehen und das Gemeinwohl zu verteidigen." (fr "En tant que citoyens, nous vous serions infiniment obligés si vous acceptiez lors des prochaines consultations de resister au lobby des brevets et défendre l'intérêt public."))

(ML Mue "Mit freundlichen Grüßen" (fr "Avec nos meilleurs sentiments,"))

(center
(nl "Hartmut Pilch" (ml "RA Dipl.-Phys. Jürgen Siepmann") (tpe "Prof. Dr. Clemens Cap" "Lehrstuhl Informations- und Kommunikationswissenschaften der Univ. Rostock") "Bernhard Reiter" "Holger Blasum" "Arnim Rupp" "Andreas Kleinert" "Xuan Baldauf" (ml "Daniel Rödding")  (ml "Jörg Freudenberger") "Markus Fleck" "Werner Lemberg" "Ralf Schwöbel" "Matthias Schlegel" "Jens Enders" (ml "Kurt Jäger") "Siegfried Piotrowski")


(ahs 'ffii (ml "Förderverein für eine Freie Informationelle Infrastruktur e.V."))
(ahs 'linux-verband)

(ah "http://www.intradat.de" "Intradat AG")
(ah "http://www.phaidros.com" "Phaidros AG")
(ah "http://www.skyrix.de" "MDLink GmbH")
(ah "http://www.frontsite.de" "Frontsite AG")
(ah "http://www.suse.de" "SuSE Linux AG")
(ah "http://www.lf.net" "LF.net GmbH")
(ah "http://www.intevation.de" "Intevation GmbH")
(ah "http://www.oberon.net" "Oberon GmbH")
(ah "http://www.europaklub.de" "Europaklub e.V.")
)
)

(mlhtdoc 'swxpatg2C (let ((V "V 0.3.7")) (ML Nef "Offener Brief: 5 Gesetzesinitiativen zum Schutz der Informatischen Innovation" "Open Letter: 5 Law Initiatives to Protect Information Innovation"))
(filters ((pe ahs 'swnpatg2C) (pf ah (prina-doknom))) (ML Mrr "Mit Bitte um Verbesserungsvorschläge und Mithilfe bei der weiteren Ausarbeitung der einzelnen Gesetzesinitiativen.  Wenn Sie im Namen eines IT-Unternehmens oder IT-Verbandes sprechen können, bitten wir ferner um Ihre Zustimmung zur Nennung als Unterzeichner.  Hier finden Sie nur den Haupttext.  Der vollständige Brief mit zahlreichen Anhängen wird in %(pf:Papierfassung) an Politiker versandt.  S. auch die begleitende %(pe:Presseerklärung)." "Please help us to improve and work out the law initiatives.  If you can speak in the name of an IT company or an IT organisation, please allow us to list you as a signatory.  Below you find only the letter body. The complete letter with numerous appendices is sent as a %(pf:paper version) to politicians.  See also the accompanying %(pe:press release).")) 
nil

(ML Stn "Sehr geehrte Damen und Herren!" "Dear ladies and gentlemen!")

(ML CWn "Computerprogramme sind auch künftig laut Gesetzeslage nicht patentierbar.  Es ist gelungen, das Patentwesen in einem Punkt vorübergehend politisch zu kontrollieren.  Dieser Erfolg ist nicht zuletzt Ihrer Unterstützung zu verdanken." "Computer program remain unpatentable according to the written law.  The patent system has been successfully subjected to political control for once on one issue.  This success is largely due to your support.")

(filters ((pv ahs 'swngrossen)) (ML Dxc "Ihre Unterstützung brauchen wir nun mehr denn je.  Denn die Patentinflation %(pv:schreitet ungebremst voran).  Die Interessengruppe der Patentjuristen versucht, über eine Europäische Richtlinie ihre Vorstellungen durchzusetzen.  Die Europäische Kommission ist eine Organ der nationalen Regierungen, das üblicherweise ohne Zustimmung des Europaparlaments europaweite Gesetzesregelungen beschließt.  Innerhalb der Kommission und Regierungen fällt die Entscheidungsgewalt generell den Patentanjuristen zu, die wiederum alle der gleichen europaweiten Interessengruppe --- wir sprechen gerne vom %(q:Europäischen Patentkonzern) --- angehören.   Ob eine demokratische Kontrolle ausgeübt wird, hängt von den Abgeordneten der nationalen Parlamente ab.  Wir bitten Sie, mit uns folgende Gesetzesinitiativen auf den Weg zu bringen:" "We now need your support more than ever.  Patent inflation is %(pv:proceeding at an unrestrained pace).  The lobby of patent lawyers is trying to push its views by means of a European Community Directive.  The European Commission is an organ of the national governments that regularly decrees europe-wide laws without having to ask for the consent of the European Parliament.  Within the Comission as within most national governments, the power of decision falls into the hands of patent lawyers who again all belong to one and the same europe-wide lobby --- we like to call them the %(q:European Patent Corporation).  Democratic control hinges mainly on the national parliaments.  We would be very obliged to you, if you could help us promote the following legislative initiatives."))

(sects
(urh (ML Rue "Recht auf Veröffentlichung von selbst erarbeiteten Informationswerken" "Right to Publish one's own information works")
   (ML DeW "Die Meinungs- und Ausdrucksfreiheit gilt auch für programmiersprachliche Werke.  Patente dürfen nicht als Zensur wirken.  Sie können sich allenfalls gegen die Verwendung der Software richten, nicht gegen ihre Veröffentlichung, Verteilung oder Weitergabe.  DV-Programme werden Verfahrenbeschreibungen und Gebrauchsanweisungen gleichgestellt.  Das EPÜ enthält zu diesem Themenkomplex keine Bestimmungen.  Wie die gewährten europäischen Patente im einzelnen durchgesetzt werden, ist Sache nationaler Regelungen. Im deutschen PatG etwa wäre folgende Absatz als §11 (7) einzufügen:" "Freedom of expression extends to works written in a programming language.  Patents may not be used to censor publications.  They may at most be directed against the use of the software, not against its publication, distribution or transmission.  Computer programs are considered equivalent to process descriptions and operating instructions.  The EPC does not contain regulations concerning this subject.  By what means European patents can be asserted is a matter of national regulation.  The German Patent Law (PatG) could be amended by inserting at §11 (7):")
   (blockquote (concat "(7)" (ML fer "das Herstellen, Anbieten, In-Verkehr-Bringen, Besitzen oder Einführen von Urheberrechtsgegenständen aller Art, insbesondere Verfahrensbeschreibungen, Gebrauchsanweisungen und Datenverarbeitungsprogrammen.  Die Rechtsverhältnisse beim Ausführen einer Gebrauchsanweisung oder eines Datenverarbeitungsprogrammes bleiben davon unberührt." "producing, offering, bringing-into-circulation, posessing or importing all kinds of copyright objects, including process descriptions, operating instructions and computer programs.  The legal conditions during execution of an operating instruction or a computer program remain untouched hereby.")))
   (ML FgN "Ferner wäre das %(e:Recht auf Veröffentlichung selbst geschaffener Informationswerke) als eine Konsequenz der Meinungs- und Ausdrucksfreiheit auf Verfassungsebene zu schützen." "Moreover the %(e:right to publish self-created information works) should be protected at the constitutional level as an outflow of freedom of expression.") )

(intop (ML Rfi "Recht auf freien Zugang zu Normen der Öffentlichen Kommunikation" "Right of Free Access to Standards of Public Communication")
   (ML Wrr "Wenn jemand durch Software-Marktmacht in der öffentlichen Kommunikation %(q:Standards setzt), muss es immer bedingungslos erlaubt sein, diese Standards einzuhalten.  Die Interoperabilität ist bereits in der EU-Urheberrechtsrichtlinie von 1991 als hohes Rechtsgut anerkannt.  Zum Zwecke der Interoperabilität muss man auch patentierte Verfahren ohne besondere Erlaubnis unentgeltlich verwenden dürfen.  Staatliche Stellen und Träger öffentlicher Funktionen dürfen nur über frei zugängliche Kommunikationsstandards mit dem Bürger kommunizieren.  Auch dieses Recht verdient Verfassungsrang, denn es folgt aus dem Recht des Bürgers auf Teilnahme an der Informationsgesellschaft." "When someone %(q:sets standards) in public communication through software market power, everybody must always be unconditionally permitted to conform to these standards.  Interoperability has already been recognized as a high legal good in the EU copyright directive of 1991.  For the purpose of interoperability, it must be allowed to use patented procedures without obtaining a permission.  Representatives of the state and carriers of public functions must use freely accessible communication standards to communicate with citizens.  This right also deserves constitutional protection, because it follows from the right of citizens of equal participation in the information society.") )

(tech (ML Iez "Präzisierung der Patentierbarkeitskriterien" "Clarification of the criteria of patentability")
   (filters ((gt ahs 'swpateurili)) (ML IWn "Bezüglich Art 52 EPÜ / §1 PatG wird der Rechtsprechung durch einen Parlamentsbeschluss eine klare und konsistente Auslegung vorgegeben, wie sie vom Eurolinux-Bündnis bereits prägnant %(gt:formuliert) wurde.  Sofern erforderlich, können auch die Gesetzestexte später präzisiert werden." "Concerning Art 52 EPÜ / §1 PatG, the parliament passes a resolution which supplies the judiciary with a clear and consistent interpretation of the law, as it has been concisely %(gt:formulated) by the Eurolinux Alliance.  If the patent judiciary still insists on misinterpreting the law, the law texts itself can later be amended."))
   (ML Irf "Zugleich wird die Regierung aufgefordert, sich bei der EU-Kommission für eine klärende Richtlinie im selben Sinne einzusetzen." "At the same time the government is asked to push for a Europe wide clarification in the same sense.") )

(sui (ML GWi "Sachgerechte Förderung für informationelle Innovationen" "Adequate Stimulation of Informational Innovation")
   (let ((URL (ahs 'swpatbasti))) (ML Zzs "Zudem wird die Entwicklung maßgeschneiderter Systeme zur Förderung Innovationen im Bereich der Programmierung und sonstiger schöpferischer Geistestätigkeiten angestrebt.  Es wird Geld für interdisziplinäre Forschung und für internationale Konferenzen zu diesem Thema ausgeschrieben." "At the same time, appropriate systems for stimulating innovation in the area of computer programs and other informational creations are to be developped.  Interdisciplinary studies and international conference on this subject are to be promoted.")) )

(debur (ML SbW "Entbürokratisierung und Internationalisierung des Patentwesens" "Debureaucratisation of the Patent System")
   (ML EPd "Es wird möglich gemacht, Patente kostenlos ohne Prüfung und ohne Jahresgebühren durch formgerechte Veröffentlichung im Internet anzumelden und sofort gültig werden zu lassen.  Im Gegenzug dazu werden zusätzliche Anreize für Nichtigkeitsklagen geschaffen.  Wer ein Patent zu Fall bringt oder einengt, erhält vom Patentinhaber über die Erstattung der Prozesskosten hinaus eine attraktive Prämie.  Statt der langwierigen und ineffektiven amtlichen Prüfung soll eine private Berufsgruppe der Patentinvalidierer die Öffentlichkeit vor ungültigen Patenten bewahren.  Die amtlichen Prüfung besteht weiter, wird aber nur auf Wunsch des Antragstellers durchgeführt.  Sie kann entweder vom Patentamt oder von zertifizierten Prüfungsgesellschaften im In- und Ausland besorgt werden." "It is made possible to register patents free of charge without examination and without annual fees by a standard-conformant publication in the Internet.  Such patents become valid immediately upon registration.  In return, additional incentives for patent invalidation procedings are created.  If someone succeeds in invalidating or narrowing a patent, he receives, in addition to the reimbursement of ligitation costs, an attractive incentive payment from the owner of the bad patent.  Instead of the long and ineffective official examination, a private professional group of patent busters shall protect the public from invalid patents.  The official examination continues to exist, but is conducted only at the request of the applicant.  It can be conducted either by the patent office or by certified examination institutes at home and in foreign countries.") )
)

(ML WnB "Wir bitten Sie, mit uns zusammen in den nächsten Tagen und Wochen entsprechende Vorschläge auszuarbeiten und in den Deutschen Bundestag sowie andere geeignete Entscheidungsgremien einzubringen." "We beg you to work out these proposals together with us and to work for their adoption by your national parliament or other suitable decisionmaking bodies.")

(ML Mue "Mit freundlichen Grüßen" "Sincerely and Respectfully")

(nl
 (ahs 'ffii)
 (ah "http://www.linux-verband.de" (ML LiVe "Linux-Verband"))
 (ah "http://www.linuxtag.de" (ML LxW "Linuxtag e.V."))
 (ah "http://www.vov.de" (ML vov "Virtueller Ortsverein der SPD"))
 (ML Gtr "Gesellschaft für Kybernetik e.V.")
; (ML Gur "Europa Klub e.V.") 
 (ah "http://www.frontsite.de" (ML Fte "Frontsite AG"))
 (ah "http://www.innominate.de" (ML imt "innominate AG"))
 (ah "http://www.intradat.com" (ML Ira "Intradat AG"))
 (ah "http://www.phaidros.com" (ML Pio "Phaidros AG"))
 (tpe "Hartmut Pilch" (lset ((vorsx (xq (ML Vsn "Vorstand %s")))) (pf vorsx "FFII e.V.")))
 (tpe (ml "RA Dipl.-Phys. Jürgen Siepmann") (spaced (ML Jst "Jusitiar") (mlval 'LiVe)))
 (tpe (ml "Oliver Zendel") (pf vorsx (mlval 'LxW)))
 (tpe (ml "Arne Brand") (pf vorsx (mlval 'vov)))
 (tpe (ml "Dr.jur. Thomas Winischhofer") (ML JpI "Patentrechtsexperte und Justitiar des FFII in Österreich"))
 (tpe "Prof. Dr. Clemens H. Cap" (ML HoW "Informatiker, Heinrich Nixdorf Stiftungsprofessor, Lehrstuhl für Information und Kommunikation, Universität Rostock"))
 (tpe (ml "Prof. Dr. Heinz Lohse") (pf vorsx (mlval 'Gtr)))
 (tpe (ml "Matthias Schlegel") (pf vorsx (mlval 'Pio)))
 (tpe (ml "Majk Kupferberg") (pf vorsx (mlval 'Fte)))
 (tpe (ml "Metin Dogan") (pf vorsx (mlval 'Fte)))
 (tpe (ml "Jens Kiefer") (lset ((vorssx (ML Vpo "Vorstandssprecher %s"))) (pf vorssx (mlval 'Fte))))
 (tpe (ml "Ralf Schwöbel") (pf vorsx (mlval 'Ira)))
 (tpe (ml "Peter Braun") (pf vorsx (mlval 'Ira)))
 (tpe (ml "Raphael Leiteritz") (pf vorsx (mlval 'imt)))
; (tpe (ml "Siegfried Piotrowski") (pf vorsx (mlval 'Gur)))
; p. A. Siegfried Piotrowski, Postfach 27 42, D- 58027 Hagen
)
)

(mlhtdoc 'swxepue31 nil nil nil
(filters ((dk ahs 'swpepue2B)) (ML Dbe "Der FFII erhielt vom Bundesministerium der Justiz (BMJ) ein Schreiben, in dem Auskunft über die Ergebnisse der %(dk:Diplomatischen Konferenz zur Revision des Europäischen Patentübereinkommens) gegeben wird."))

(filters ((bj ah "http://www.bmj.bund.de/")) (ML DhW "Die einschlägigen Dokumente scheinen im Moment auf den %(bj:einschlägigen Webseiten des BMJ) nicht vorhanden zu sein.  Daher tippe ich hier ein paar Auszüge ab und streue ein paar Bemerkungen ein:"))

(ML ZdW "Zunächst aus dem Schreiben von Dr. Welp:")

(bcite (ML IhR "In der Zeit vom 20.-29. November 2000 fand die diplomatische Konferenz  zur Revision des Europäischen Patentübereinkommens statt.  Als Anlage übersende ich Ihnen die von der Konferenz angenommene Revisionsakte MR/3/00/rev.1 sowie einen kurzen Überblick über die wesentlichen Beratungsergebnisse.")

(ML Aua "Aus hiesiger Sicht soll die Revisionsakte so zügig wie möglich gezeichnet und dann ratifiziert werden.  Für den Fall, dass Sie eine Stellungnahme beabsichtigen, bitte ich Sie, mir diese bis zum 30. März 2001 zu übersenden.") )


(filters ((ob ahs 'swxepue28)) (ML W2e "Wir haben in dem %(ob:Offenen Brief von Anfang August) unserem Wunsch Ausdruck verliehen, dass Artikel 52 nicht angetastet werde.  Der Offene Brief wurde zwar vom BMJ nicht beantwortet und unserem Wunsch wurde auch nicht ganz entsprochen, aber immerhin ist die Diskussion um die Computerprogramme offen geblieben."))

(ML WPn "Wir stehen selbstverständlich nach wie vor auf dem Standpunkt des Offenen Briefes, wonach auch die Änderungen von Abs 52.1 und 52.2e einen Rückschritt darstellen.  Der neue Abs 52.1 schreibt vor, dass Patente %(q:für Erfindungen auf allen Gebieten der Technik) erhältlich sein sollen.  Ein solches Universalitätspostulat erfüllt in einem abstrakten Freihandelsvertrag wie TRIPS eine ganz andere Funktion als im EPÜ.  Es gehört nicht in das EPÜ, und wäre auch nur dann akzeptabel, wenn es von einer engen Definition der Begriffe %(q:Technik), %(q:Erfindung) und %(q:industrielle Anwendung) begleitet würde.")

(filters ((er ahs 'swpateurili) (ep ahs 'epue52)) (ML Exo "Eine solche enge Definition haben wir in einem %(er:Vorschlag für eine EU-Richtlinie) und einem %(ep:Vorschlag für die Revision des Art 52) veröffentlicht."))

(let ((TIT (bcite "Wesentliche Ergebnisse der Diplomatischen Konferenz ..."))) (ML IWs "In seinem Begleitschreiben %{TIT} erwähnt das BMJ-Patentreferat die Änderungen in Art 52.1 und 2e nicht.  Offenbar werden sie nicht als wesentlich angesehen.  Zu Art 52.2c schreib das BMJ-Patentreferat:"))

(bcite (ML DdW "Der Basisvorschlag sah zu Artikel 52 Abs 2 Buchstab c die Streichung von %(q:Computerprogrammen als solchen) aus der Liste der nicht patentfähigen Erfindungen vor"))

(ML Lie "Laut EPÜ Art 52.2c sind %(q:Programme für Datenverarbeitungsanlagen) nicht patentfähig.  Die Partikel %(q:als solche) in 52.3 bezieht sich auf alle Ausschlussgegenstände gleichermaßen und gewinnt ihren Sinn erst in einem syntaktischen Kontext, nämlich:")

(bcite (ML IWn "Ich patentiere ein Verfahren zur Herstellung von Schachfiguren, nicht das Schachspiel als solches."))
(bcite (ML Ipn "Ich patentiere das Produkt einer programmierten chemischen Reaktion, nicht das Programm als solches."))

(ML Mah "Mit dem Wort %(q:als solche) versuchen expansionsbestrebte Wortführer der Patentgemeinde seit 10 Jahren Nebel zu werfen.  Aus diesem Grunde wäre eine Streichung des redundanten Abs 52.3 durchaus wünschenswert.  Leider stand der aber nicht zur Disposition.  Es ging nur um %(q:Programme für Datenverarbeitungsanlagen).  Der Zusatz %(q:als solche) ist in diesem Zusammenhang unmotiviert.  Aus ihm spricht das schlechte Gewissen des Patentexpansionisten, der die Bedeutung seines Expansionsschrittes kleinreden möchte.")

(ML WMa "Wir wünschen uns vom BMJ meht kritische Distanz zur Patentbewegung.")

(ML Flu "Folgende Artikel können hier aufklärend wirken:")

(ul
(ahs 'swpateurili)
(ahs 'skk-patpruef)
)

(ML Dtc "Das BMJ-Patentreferat berichtet weiter:")

(bcite (ML NWW "Noch in der vorbereitenden Sitzung des Verwaltungsrates war eine Streichung von 10 gegen 8 Mitgliedstaaten bei einer Enthaltung befürwortet worden.  Deutschland, Frankreich und Dänemark haben auf der Konferenz einen Antrag eingebracht, zum gegenwärtigen Zeitpunkt die geltende Fassung des Artikels 52 EPÜ unverändert zu belassen.  Ein intesniver Austausch mit Vertretern der anderen Mitgliedstaaten hat dazu geführt, dass sich die Mitgliedstaaten auf der diplomatischen Konferenz mit überwältigender Mehrheit gegen die Streichung der Computerprogramme als solche aussprachen."))

(ML InA "Interessant ist, dass nicht Großbritannien sondern Dänemark mit DE und FR mitzog.  Auch in den letzten Monaten hat die britische Regierung ihren Patentfunktionären freie Hand gelassen, die EU-Kommission zur schnellen Legalisierung der Softwarepatente des EPA zu drängen.  Auch die BMJ-Vertreter ermutigten m.W. in Brüssel diesen Kurs, obwohl von seiten der Bundesregierung anders lautende Anweisungen vorlagen.") 

(bcite (ML EWn "Eine Veränderung der Vorschriften über die Patentierbarkeit von computerprogrammbezogenen Erfindungen schien von der von der EU zu dieser Thematik im Rahmen der angekündigten Richtlinie angestoßenen Diskussion nicht angebracht.  Zwar waren sich die Delegationen einig in der Bewertung, dass die im Basisvorschlag vorgesehene Streichung keine wesentliche Änderung der Rechtslage bewirkt hätte."))  

(ML Dnr "Die vorgesehen Streichung brächte eine wesentliche Änderung der %(e:Gesetzeslage).  Die Rechtslage bewegt sich seit einigen Jahren nicht mehr im Rahmen des Gesetzes.  EPA und BGH haben den Boden des Gesetzes auf Drängen der Patentabteilungen von Siemens, IBM und anderen verlassen und bewegen sich nun in einem Geflecht von Widersprüchen, die regelmäßig vom 17. Senat des Bundespatentgerichtes aufgedeckt werden.  Die Gesetzeslage ist klar, die Rechtslage ist verworren.  Das EPA plante letzten Sommer mit Unterstützung der Berliner und Brüsseler Patentreferenten, per %(q:Harmonisierung) nun auch die Gesetzeslage in Widersprüchlichkeiten zu stürzen, den letzten Rest eines stimmigen Technikbegriffes zu demontieren, und damit den Weg ins Bodenlose der Patentierung trivialer geistiger Verfahren zu eröffnen.")

(ML Dzt "Die neue Gesetzeslage wäre selbstverständlich auch für das BPatG und zahllose andere europäische Gerichte verbindlich geworden.  Somit hätte sich auch die Rechtslage wesentlich geändert.  Mehr dazu s. unten.")

(filters ((pb ahs 'swpatlijda)) (ML Ese "Es ist erschreckend, zu lesen, wie das BMJ sich durch seine Rhetorik mit der %(pb:Patentbewegung) identifiziert."))

(bcite (ML Ghr "Gleichwohl ist die Gefahr gesehen, dass eine solche Streichung als Signal dahingehend missverstanden werden könnte, dass die Patentierbarkeit von Software in weiterem Umfang möglich sein solle, als dies bereits heute der Fall ist.")) 

(filters ((bg ahs 'swpatbghist)) (ML Wse "Was %(q:heute der Fall ist), wird von Menschen bestimmt.  Es ist nicht das Ergebnis einer quasi-natürlichen Rechtsfortbildung, die es nur gelehrig nachzuvollziehen gilt.  Das BMJ trägt Verantwortung für das was passiert.  Es vermittelt zwischen den Gerichten und dem Gesetzgeber.  Der BGH hat %(bg:geirrt), und es ist Sache des BMJ, Fehlentwicklungen zu widerstehen und sie korrigieren zu helfen."))

(filters ((bp ahs 'bpatg17w6998)) (ML Egg "Es stimmt, dass von einer Streichung eine Signalwirkung ausgegangen wäre.  Nicht nur bezüglich der im Moment beim BGH anstehenden Entscheidung über die von der IBM-Patentabteilung %(bp:gewünschten) Ansprüche auf %(q:Computerprogrammprodukte) und %(q:Computerprogramme)."))

(filters ((gr ahs 'swngrossen)) (ML Dtb "Doch das EPA %(gr:drängt weiter auf Expansion und möchte die zwei Jahren einen neuen Anlauf unternehmen). Dabei erfährt es die Unterstützung des BMJ-Patentreferats:"))

(bcite (ML DWn "Diese Thematik wird in einer zweiten Revisionsrunde neu aufgenommen."))

(flet ((bq (str) (bcite (quot str)))) (ML Zmi "Zum Schluss wird noch einmal ein %(bq:Weiterer Revisionsbedarf) konstatiert:"))

(bcite (quot (ML Dzi "Die diplomatische Konferenz hat sich in einer Abschlusserklärung dafür ausgesprochen, dass in den zuständigen Gremien der EPO die Diskussion über die auf eine zweite Revisionsrunde vertagten Fragen alsbald aufgenommen werde.  Betroffen sind Themen wie z.B. die Patentierbarkeit computerprogrammbezogener Erfindungen, die Überführung der Biotechnologie-Richtlinie in das EPÜ, die normative Verknüpfung von EPÜ und Gemeinschaftspatent sowie die Einführung einer Neuheitsschonfrist.")))

(ML Urr "Umso unverständlicher ist die derzeitige Haltung des BMJ.  Es hüllt sich zum Thema Swpat in Schweigen und ermutigt im stillen (z.T. im Widerspruch zu Vorgaben der Bundesregierung) die Europäische Kommission, die bisherigen Fehlentwicklungen durch eine Richtlinie zu zementieren.  Offenbar soll wieder nur eine %(q:Diskussion in den zuständigen Gremien) geführt werden.  Der schwarze Peter wird der EU-Kommission zugeschoben.  Die Gelegenheit zu einer angemessenen Konsultation, wie alle Fraktionen des Deutschen Bundestages sie gefordert haben, wird verspielt.")

(ML Dze "Das kann aber nicht funktionieren.  Die Krise des Patentwesen wird immer spürbarer.  Derzeit explodiert die Zahl der Patentanmeldungen, die Wartezeiten steigen und die Qualität sinkt ins Bodenlose.  Die Meldungen von unsinnigen Patentanmeldungen und Patentangriffen in Amerika und Europa überstürzen sich.  Erst vorgestern gab das Fraunhofer-Institut für Telematik zusammen mit dem DPMA dem Steuerzahler eine schallende Patent-Ohrfeige:")

(blockquote (ahs 'de19838253))

(filters ((gk ahs 'swpatpikta)) (ML DaE "Das EPA hat in den letzten Jahren %(gk:30000 Patente) auf triviale Probleme gewährt, deren Lösung im Verfassen eines Programmtextes besteht."))

(ML Dxh "Diese Lösungen greifen nicht unmittelbar auf Naturkräfte zu: Ihre Gültigkeit wird auf rein geistig-mathematische Weise bewiesen und nicht etwa empirisch-experimentell überprüft.  Die einzige %(q:beherrschbare Naturkraft), die bei einer solchen %(q:Erfindung) ins Spiel kommt ist die Heizenergie für die Badewanne, in der der Fachmann sich sein Problem überlegt (sofern es nicht ohnehin schon durch Kundenwünsche an ihn herangetragen wurde).  Die einzige Investition, die durch ein solches Patent %(q:geschützt) wird, ist die Investition in den Brennstoff für eine Badewannensitzung.")

(filters ((pb ahs 'benkard88) (lm ahs 'lamy98)) (ML LeW "Laut der Rechtsauffassung, die dem geltenden Gesetz und mustergültigen Referenzwerken (z.B. PatG-Kommentar Benkard 1988, Lamy Droit Informatique 1998) zugrundeliegt, hätten diese Patente niemals erteilt werden dürfen."))

(ML Aej "Angesichts dieser Sachlage ist es unverantwortlich, wenn weiterhin in irgendwelchen patentjuristischen Hinterzimmern Änderungen vorbereitet werden, die einer weiteren Senkung der Patentierbarkeitshürden Vorschub leisten.")

(filters ((pg ahs 'swxpatg2C)) (ML Der "Der Bundestag sollte auch die im November beschlossenen EPÜ-Änderungen nur dann ratifizieren, wenn gleichzeitig ernsthafte Schritte zur Kontrolle des Patentwesens und Begrenzung der Patentierbarkeit unternommen werden, wie in unserem %(pg:neuesten Offenen Brief) angeregt.")) 

(ML IPh "Insbesondere sollte die Bundesregierung schnellstmöglich ein Moratorium auf die Erteilung von Softwarepatenten durch das DPMA erwirken, um unmittelbar entstehenden Schaden abzuwenden und die Seriosität der EU-Konsultation zu unterstreichen.  Schon jetzt warten Prüfungsanträge oft 4 Jahre, bevor sie erteilt werden.  Warum können sie nicht noch 2 Jahre länger warten?")

(ML Wnt "Wäre es zu viel verlangt, die Zustimmung des Bundestages zur EPÜ-Revision mit einer solchen Maßnahme zu verknüpfen?")
)

;swpatxatra
)


(list 'swpatgirzu-dir
(mlhtdoc 'swpatgirzu nil nil nil
(filters ((ap ahs 'swpagtask) (pm ahs 'swpenmi)) (ML DrW "Die Gruppe organisiert ihre Tätigkeiten über einen offen einsehbaren %(ap:Arbeitsplan) und tritt auf %(pm:Tagungen) in Erscheinung.")))

(sects
(name (ML Nrt "Name der Arbeitsgruppe")
(commas (let ((AG (quot title)) (SAG (quot "SWPAT-AG"))) (ML DWi "Die Arbeitsgruppe nennt sich %{AG} oder, kurz %(q:%(h:Softwarepatent-Arbeitsgruppe)), %{SAG} oder %(q:AG)."))
))

(zwek (ML Zei "Zweck der Arbeitsgruppe")
(lin (ML Ztl "Zweck der Arbeitsgruppe ist Volksbildung durch")
(ol
 (ML Auu "Aufklärung der Öffentlichkeit über das Patentwesen und seine Auswirkungen auf freie Informationswerke und die informationelle Infrastruktur im allgemeinen")
 (ML SWW "Schutz freier Informationswerke und der informationellen Infrastruktur vor Angriffen durch Softwarepatente")
))

(filters ((zw ahst 'ffiizweck)) (ML Sen "Somit verfolgt die Arbeitsgruppe einen Unterbereich des %(zw:satzungsgemäßen Vereinszwecks des FFII)."))

)

(meth (ML Mws "Mittel zur Verwirklichung des %(h:AG:Zwecks)")
(ML Drg "Der Zweck der SWPAT-AG wird insbesondere durch folgende Mittel verwirklicht:") (ol
 (ML Ecd "Erstellung von wissenschaftlichen Studien und Aufklärungsschriften")
 (ML Ton "Teilnahme an und Organisation von öffentlichen Lehr- und Diskussionsveranstaltungen")
 (ML Ten "Teilnahme an Konsultationen mit gesetzgebenden Stellen der Europäischen Union oder ihrer Mitgliedsländer.")
 (ML Eeo "Erarbeitung von Lizenzverträgen, die geeignet sind, Autoren freier Informationswerke vor Patentangriffen zu schützen")
 (ML ntr "Übernahme der patentrechtlichen Verantwortung für freie Informationswerke")
 (lin (ML Een "Erwerb eines eigenen defensiven Patentportfolios durch") (ul
  (ML End "Erwerb von Anteilen an Patentrechten freundlich gesonnener Firmen")
  (ML AnP "Anmeldung von Patenten") ) )
 (ML Aae "Aufbau eines Patentschlüsselsystems, das es Patentinhabern erlaubt, in einer freien Informationswerken gegenüber relativ freundlichen Weise Gebühren einzuziehen")
)
)

(mitg (ML Zzb "Zusammensetzung der Arbeitsgruppe")
(ML Jhe "Jeder, der für die Ziele der Gruppe arbeiten möchte, kann Mitglied werden.  Alle Mitglieder haben aktives und passives Wahlrecht.  FFII-Mitgliedschaft ist nicht erforderlich.  Die interne Organisation der Gruppe richtet sich, sofern hier nicht anders vereinbart, nach der Satzung des FFII.  Demnach hat die Arbeitsgruppe ihren eigenen Vorstand, der einmal jährlich gewählt wird.")
)

(sitz (ML Srt "Sitz der Arbeitsgruppe")
(let ((ADR (apply 'kaj (mapcar 'code (list "mailto:swpat@ffii.org" "http://swpat.ffii.org")))) (EL (ah "http://www.eurolinux.org" (ML Enn "Eurolinux-Bündnisses")))) (ML Dem "Die SWPAT-AG ist im Internet unter den Adressen %{ADR} zu Hause.  Die Funktionsträger der AG bestimmen ihren Arbeitsort selber.  Der Schwerpunkt liegt im deutschsprachigen Raum.   In europäischen Angelegenheiten arbeitet die AG im Rahmen des %{EL} mit Organisationen anderer Länder zusammen.  Die rechtliche Verantwortung übernimmt der FFII, der seinen Sitz in München hat.")) )

(kont (ML BnW "Bankverbindung der SWPAT-AG")
(filters ((vk ahsti 'ffiipart 'konto)) (ML DWl "Die SWPAT-AG empfängt Spenden über das %(vk:Vereinskonto des FFII).  Der FFII verpflichtet sich, die Mitgliedsbeiträge der AG-Mitglieder sowie jede Spende an den FFII, in deren %(h:Verwendungszweck:Feld) zu Anfang das Wort %(q:SWPAT) steht, sofort und ohne Abzug der SWPAT-AG zur Verfügung zu stellen."))
)

(sign (ML Urc "Unterzeichner")
(ML Den "Diese Vereinbarung wird von einem vertretungsberechtigen Mitglied des FFII-Vorstandes und von den Gründungsmitgliedern der AG unterzeichnet") (ul
(ahm "blasum@ffii.org" (famvok "Blasum" "Holger"))
(ahm "phm@ffii.org" (famvok "Pilch" "Hartmut"))
(ahm "breiter@ffii.org" (famvok "Reiter" "Bernhard"))
(ahm "arnim@rupp.de" (famvok "Rupp" "Arnim"))
(ahm "seyfertd@aol.com" (famvok "Seyfert" "Dirk"))
))
)

(list 'swpagnenri-dir
(mlhtdoc 'swpagnenri (ML Iik "Interner Bereich der SWPAT-Arbeitsgruppe") (ML Iik2 "Manche Pläne und Thesen sind noch nicht für die Öffentlichkeit reif.  Manche externe Dokumente dürfen wegen Urheberrechten u.a. nicht veröffentlicht werden.") nil
)

(let ()
(vrequire 'readdb)
;; (readdb-nodes 'swpagpapri)
(mlhtdoc 'swpagpapri (ML Enk "Externe Dokumente" "External documents") (ML Wse "Wir bemühen uns, einschlägige Gerichtsurteile und Aufsätze zu fotokopieren, archivieren, digitalisieren, textualisieren und möglichst frei verfügbar zu machen." "We are trying to collect swpat documentation and make it as accessible as possible.") (l "Satzung")
(let ((IN (ah "http://www.intevation.de" "Intevation GmbH"))) (ML DWl "Derzeit arbeitet die ${IN} in unserem Auftrag an der Erstellung einer Dokumentations-CD." "%{IN} is putting together a documentation CD on our order."))
(filters ((sd ah "http://www.ffii.org/mailman/listinfo/swpatdoku/")) (ML FsW "Für Diskussionen wurde ein %(sd:E-Post-Verteiler) gegründet, dem täglich der Stand der Arbeit berichtet wird." "They report to a %(sd:mailing list)."))
(filters ((sa ahs 'swpatgirzu) (sd ahs 'swpatpapri)) (ML Die "Die %(sa:SWPAT-AG) hat bereits vorher einige %(sd:noch nicht veröffentlichbare Dokumente) gesammelt.  Weitere liegen bei Intevation.  Nur Projektmitarbeiter haben Zugang." "The %(sa:FFII SWPAT workgroup) has collected some %(sd:unpublishable documents) before.  More are on Intevation's server."))
))

(list 'swpagthes-dir
(mlhtdoc 'swpagthes (ML Ute "Unveröffentlichte Thesenpapiere") (ML TWW "Texte sollten zuerst der AG zur Kritik vorgelegt werden, bevor sie an die Öffentlichkeit gehen.") (l "Satzung")
)

(mlhtdoc 'swpberlinit (ML WIi "Wünsche des FFII an die Berliner Gesetzgeber") (ML ErE "Es wird Zeit für eine Umorientierung der Politik.  Karl Poppers  Welt 3, die Welt der frei weiterverwertbaren Information, emanzipiert sich aus den Fesseln der industriekapitalistischen Produktionsweise.  Dieser Emanzipationskampf stößt auf unfaire Widerstände, die zu beseitigen politisch möglich und wünschenswert ist.") (l "Satzung" "Softwarepatente")
(sects
(opnsrc (ML WSW "Warum Freie Software die Welt bewegt")
(ML Dma "Die Schlagworte OpenSource und Linux sind in aller Munde.  An der Börse boomen Redhat-Aktien, die Softwarefirmen überschlagen sich mit Ankündigungen zur Unterstützung von Linux und zur Offenlegung von Quelltexten.  Die EU setzt eine Opensource-Studienkommission ein, das Bundeswirtschaftsministerium fördert die Entwicklung der frei verfügbaren Verschlüsselungssoftware GnuPG, der französische Senat berät über ein Gesetz zur Förderung der Quellenoffenheit, die Unesco propagiert die Bedeutung freier Software für die Entwicklung der Dritten Welt, in Japan wird Turbo Linux das meistverkaufte Betriebssystem, die chinesische Regierung bringt eine Linux-Distribution namens %(q:Rote Flagge) heraus während ein %(q:Netzwerkkommunistisches Manifest) Furore macht.")

(ML Vzs "Versuchen wir mal aufzuzählen, was all diese Kreise begeistert.")
(dl
(l (ML Wtr "Wert/Preis") (ML DWW "Das Freie Unix (GNU/Linux/BSD/...) ist leistungsstark, schnell und stabil.  Man fährt es hoch und es läuft, läuft und läuft, hält (z.B. als Internet-Server) hohen Belastungen stand und kostet nichts.  Gebühren für kommerzielle Systeme und Wartungsarbeiten bei Systemabstürzen sind hingegen sehr hoch und werden oft von Steuergeldern bezahlt."))
(l (ML Sso "Schnittstellenoffenheit") (ML Wen "Während Microsoft gezielte Inkompatibiliät als Hebel einsetzt, um Konkurrenz gegen sein Monopol im Keim zu ersticken, spielt das Freie Unix mit offenen Karten.  Jeder kann es verwenden, wie er will und damit kombinieren, was er will.  Dadurch sind sehr viele kleine innovative Projekte (z.B. Spezialanwendungen bei NASA, Supercomputersystem BeoWulf) erst entstanden, die in der industriekapitalistisch dominierten Infosphäre mangels Masse keinen Platz hatten.  GNU/Linux läuft auf mehr Hardware-Architekturen als irgendein anderes Betriebssystem."))
(l (ML Qee "Quellenoffenheit") (ML Drl "Das Freie Unix ist nicht nur kostenlos sondern wirklich frei:  jeder kann den Quelltext nehmen, daraus lernen, ihn weiterentwickeln und weiterverteilen.  Die Bewohner der Infosphäre haben endlich Luft zum Atmen.  Da es keine Eigentumsschranken gibt, finden die besten Infowerke den Weg den begnadetesten Entwicklern."))
(l (ML Shh "Sicherheit") (ML Ehg "Erst dadurch, dass Tausende vernetzter Tüftler den selben Quelltext anschauen und weiter entwickeln können, wird es sehr wahrscheinlich, dass Fehler und Sicherheitslücken entdeckt werden.  Hacker und System-Einbrecher haben immer wieder vorgeführt, dass geheimniskrämerische Sicherheitskonzepte (%(q:security by obscurity)) nicht funktionieren.  Daher fördert das BMWi zuerst GnuPG."))
(l (ML Srq "Schutz vor dem Großen Bruder") (ML Gnw "Geheimniskrämerische Systeme bieten häufig einigen Wenigen (wie z.B. im Falle von Windows NT dem US-Nachrichtendienst NSA) eine Hintertür zum Lauschen.   Gleichzeitig tönen Regierungen von der Notwendigkeit, Feindesstaaten wie Jugoslawien mit informatischem Krieg (cyberwar) zu überziehen.  Länder wie China und Japan springen auf den Linux-Zug, weil sie auf ihre nationale Sicherheit und auf eine unabhängige nationale Softwareindustrie Wert legen."))
(l (ML Wle "Wert für Bildung und Beschäftigung") (ML DeW "Dank freier Verfügbarkeit der Informationen können junge Menschen ungehindert das Wissen und die Kompetenz erwerben, nach der sie dürsten.  Es entsteht eine große Schicht mündiger Infowelt-Bürger, die sich um Arbeitsplätze keine Sorgen machen müssen.  Denn selbst wenn sie das Gros ihrer Zeit damit verbringen, freie Software zu schreiben, sind sie als EDV-Dienstleister heiß begehrt."))
(l (ML Fen "Faszination eines funktionierenden Kommunismus") (ML DcW "Die Produktivität des quellenoffenen Programmierens stellt einige herkömmliche Weisheiten in Frage.  Es hat sich gezeigt, dass es in der Welt 3 neben der industriekapitalistischen (= informationsfeudalistischen) Produktionsweise eine gemeinschaftlich orientierte Alternative gibt, die vielfach technisch bessere Ergebnisse produziert.  Dabei war dies bisher ein Kommunismus armer Privatleute, die gegen die von den führenden Kreisen propagierte industriekapitalistische Produktionsweise einen schweren Stand hatten und haben."))
) )

(polit (ML WvW "Wo die Infosphäre politische Hilfe braucht")
(sects
(denk (ML Onf "Offizielle Anerkennung des Wertes freier Information")
(ML UWg "Unter den als gemeinnützig anerkannten Vereinszwecken fehlt die %(q:Schaffung von frei verfügbaren Informationen).")
(ML Dgd "Der FFII e.V. konnte seine Gemeinnützigkeit nur auf %(q:Volksbildung) gründen.  Volksbildung ist jedoch nur eine sekundäre %(e:Wirkung) der %(q:frei verfügbaren informationellen Werte), um die es uns geht.")
(ML IWz "Informationelle Werte ind eigentlich Gemeingut par excellence.  Während Bildungsaktivitäten immer nur einer begrenzten Zuhörerzahl zugute kommen, sind Informationen frei von den Begrenzungen.")
(ML hWe "Öffentlichen Funktionsträgern (darunter auch gemeinnützigen Vereine) sollten im Gegenteil dazu verpflichtet werden, informationelle Werte zu schaffen und der Allgemeinheit zur freien Verfügung zu stellen.")
(ML UdU "Universitäten sind in besonderem Maße der Welt 3 zugewandt.  Wissenschaftler und Studenten sollten das Recht haben, ihre an der Universität erarbeiteten Werke quellenoffen zu zur Verfügung zu stellen.  Bei der Bemessung öffentlicher Zuwendungen an Universitäten sind insbesondere die von diesen Universitäten geschaffenen quellenoffenen informationellen Werte zu würdigen.")
(ML Aos "Auch die Bundesbahn ist trotz Privatisierung qua Monopolstellung ein öffentlicher Funktionsträger.  Sie  sollte dazu verpflichtet werden, ihre Fahrplandaten nicht etwa verschlüsselt auf Windows-CD sondern plattformneutral und quellenoffen zur Verfügung zu stellen.")
(ML Ona "Ähnlich ist es bei der Telefonauskunft, bei geographischen Daten, Bankenstandards, Patentschriften und Behördeninformationen aller Art.  Wir müssen uns von dem falschen Denken verabschieden, welches informationelle Gemeingüter zu privaten Wirtschaftsgütern umzufunktionieren sucht.  Welt 3 gehört allen.  Ansätze die dies verkennen, sind volkswirtschaftlich unproduktiv.")
)

(ifopn (ML Oti "Offenheitszertifizierung")
(ML Pee "Programmierer stehen häufig vor dem Problem, dass die Hersteller von Computer-Zubehörgeräten keine Informationen über die Schnittstelle ihres Gerätes liefern, so dass man z.B. keinen Linux-Treiber schreiben kann.   Dies ist letztlich der Grund für die Monopol-Situation auf dem Softwaremarkt.")
(ML HeB "Hier kann der Gesetzgeber aushelfen, indem ein System zur Zertifizierung der Offenheit von Programmierschnittstellen schafft.  In manchen Bereichen des öffentlichen Lebens, insbesondere bei den Beschaffungsrichtlinien von Behörden und öffentlichen Funktionsträgern, sollten nur noch zertifizierte Geräte den Zuschlag bekommen können.")
(ML WWa "Wer ein Gerätes mit nicht-offener Schnittstelle kauft, schließt damit einerseits bestimmte Kreise der Bevölkerung aus und begünstigt andererseits nicht nur den Auftragnehmer sondern auch ein Kartell, das sich gar nicht offiziell um den Zuschlag beworben hat.  Es findet ein verstecktes Geschäft zu Gunsten/Lasten unbeteiligter Dritter statt.   Dies ist unstatthaft.")
(ML Eon "Es darf nicht sein, dass ein Bürger proprietäre Software einsetzen muss, um mit Behörden oder öffentlichen Funktionsträgern kommunizieren zu können.")
(ML IeW "Im französischen Senat wird seit November über einen Gesetzesantrag dieses Inhalts beraten.  Er wird dort insbesondere von Konservativen und Grünen unterstützt und hat ein durchweg positives Presseecho ausgelöst.")
)

(patent (ML Kam "Keine Patentierbarkeit von Information oder Leben")
(ML Dbb "Die Patentorganisationen drängen darauf, nach amerikanischem Vorbild Software patentierbar zu machen.  Sie wollen im Juni dieses Jahres den Punkt %(q:Programme für Datenverarbeitungsanlagen) von der Ausnahmenliste des Europäischen Patentübereinkommens streichen.")
(ML SWe "Sie haben keine wissenschaftliche Studie und nicht die Spur einer volkswirtschaftlihcen Folgenabschätzung vorgelegt.  Der wirkliche Grund ist, dass ihre patentinflationäre Rechtspraxis bereits heute im Widerspruch zu der lästigen EPÜ-Ausnahmenliste steht.  Greenpeace hat diese Rechtsbeugung des Europäischen Patentamtes bereits kritisiert und die Bundesjustizministerin zu einer kritischen Stellungnahme bewegt.")
(ML DWr "Die Bundesregierung ist als EPÜ-Teilnehmervertreter befugt, dem Europäischen Patentamt die Richtung vorzugeben.  Diese sollte (vereinfacht gesagt) lauten:")
(ML Pne "Patentierbar sind Gegenstände, deren %(e:Reproduktion) ein erfinderisches Verfahren erfordert.  Informationen sind kopierbar und fallen daher aus gutem Grund nicht unter das Patentrecht sondern unter das Urheberrecht.  Lebewesen reproduzieren sich selber und unterliegen daher allenfalls der Geburtenkontrolle, nicht aber dem Patentrecht.")
(ML DtW "Die Patentierbarkeit von Information erzeugt nicht nur Widersprüche auf begrifflicher Ebene, sondern sie schadet der gesamten Softwarebranche.  Am ehesten können noch Großkonzerne von einem Patentportfolio profitieren.  Aber selbst Oracle und Adobe haben sich gegen Softwarepatente ausgesprochen.  Für freie Software ist die Patentierung von Information tödlich, denn sie bedeutet, dass ein Programm nachträglich unfrei oder gar verboten werden kann, obwohl es ganz auf der eigenständigen schöpferischen Arbeit von Tausenden von wirtschaftlich uneigennützig agierenden Individuen beruht.")
(ML DrW "Der FFII e.V. hat einen detaillierten Vorschlag hierzu erarbeitet und möchte gerne zusammen mit seinen Sponsoren am Gesetzgebungsprozess beteiligt werden.")
)
)
)
)
)
; swpagthes-dir
)

(mlhtdoc 'swpagtask (ML Aal "Aktionsplan zum Schutz vor Softwarepatenten")
 (filters ((sj ahs 'swpatsarji)) (ML Dld "Die Softwarepatent-Arbeitsgruppe des FFII e.V. hat immer mehr Aufgaben als freie Hände.  Für einzelne Arbeiten können -- %(sj:Förderern) sei dank -- Aufwandsentschädigungen gezahlt werden."))
 (append swpatkeyw (l "Satzung"))
(sects
(lang (ML Drg "Daueraufgaben")
(fartab
(l (ML Dma "Papier-Digitalisierung") (ML Zeu "Zeitschriftenartikel aus Bibliotheken fotokopieren und zu Digitaltext aufbereiten, mit denen die Patentinflation dokumentiert und studiert werden kann.  Die interne digitale Dokumentation erstellen.") "holger, phm" (filters ((np ahs 'swpagpapri)) (ML HeA "Holger hat einen Stapel von Artikeln besorgt und digitalisiert.  Hartmut hat sie weiter aufbereitet und im %(np:internen Archiv) bereitgestellt.  Die Textdateien sind noch voller OCR-Fehler.  Es fehlt an VWL-Literatur.")))
(l (ML Hol "Horrorgallerie") (ML Dpc "Das EPA hat bereits zahlreiche Patente auf Organisationsverfahren erteilt, wie z.B. den Einkauf nach Kochrezept.  Die Patentschriften sind zu finden und zu sammeln.  Suchwörter auf dem EPA-Server wären %(q:computer, incentive, economic, payment etc).  Auch die Suchmethode wäre zu dokumentieren und möglichst zu automatisieren.  Neben Organisationsverfahren sind auch besonders grundlegende Algorithmen interessant.  Allerdings erfordert dies eine besonders genaue Lektüre der oft kaum verständlichen Patentansprüche."))
(l (ML Rei "Rezensionen") (filters ((np ahs 'swpagpapri) (sp ahs 'swpatpapri)) (ML Iez "Im %(np:internen digitalen Archiv) vorhandene Artikel einzeln rezensieren und in %(sp:Rezensions-Archiv der SWPAT-AG) veröffentlichen.")))
(l (ML Pid "Petitionsadressar") (filters ((lg ahs 'eulux-letter)) (ML Aga "Adressen von Polit-Entscheidern sammeln und in geeigneter Form zugänglich machen, so dass sie in den %(lg:Eurolinux-Brieferzegungsautomaten) eingebaut und auch anderweitig verwendet werden können.")) nil (ML Hmn "phm hat einige Adressen gesammelt und ein paar in den Automaten eingegeben.  Noch viel zu wenig."))
(l (ML Fms "Firmenmobilisierung") (ML Dun "Der Software-Mittelstand schläft, wärend die Patentbürokratie daran arbeitet, ihn zu enteignen und ins Abseits zu drängen.  Diese Firmen müssen durch systematisches Anschreiben auf die Gefahr aufmerksam gemacht und hinter die Petition gebracht werden.") nil (ML HWf "Hartmut hat ein paar Firmen angeschrieben und einen Musterbrief veröffentlicht.  Das reicht nicht.  Ein Plan für eine professionelle Herangehensweise ist erarbeitet."))
(l (ML SuA "Standpunkte-Archiv") (ML ZfW "Zahlreiche Firmen und Politiker haben Meinungen zum Thema veröffentlicht.  Die entsprechenden Zitate sind systematisch im WWW zu archivieren, damit darauf flexibler verwiesen werden kann."))
(l (ML werm "Werbemittel") (ML hiS6 "Werbemitteln sollten systematisch gepflegt und bereitgehalten werden.") "arnim?" (filters ((lt ahs 'swplxtg26)) (ML Fng "Für unsere %(lt:Linuxtag-Auftritt) erstellten Arnim und Felix für uns allerlei Aufkleber und Schilder, Holger druckte Hemden, und wir hatten auch ein Flugblatt.")))
(l (filters ((dc ahs 'swpdokucd)) (ML Iod "Integration der %(dc:Doku-CD)")) (ML Dne "Die von Intevation erstellte Doku-CD so überarbeiten, dass redundante Informationen konzentriert werden und eine Dokumenten-Objekt-Datenbank entsteht, die mit den sonst in unserer Dokumentation verwendeten Datenbanken integriert werden kann.  Diese Dokumentation und ihre CD-Version weiter pflegen.") "intevation? frontsite?")
(l (ML GPP "GPPL") (ML Mfe "Mögliche Patentlizenzen formulieren, mithilfe derer innerhalb des Patentdickichts Schneisen für GPL-SW geschlagen werden könnten.") nil (ah "http://ffii.org/archive/mails/swpat/2000/Jul/0104.html" (ML GDs "GPPL-Diskussion")))
(l (ML Ggt "GRUR-Gegendarstellung") (ML Dcv "Die Riege der Patentjuristen aufmischen.  In ihrem Flaggschiff GRUR o.ä. Gegenstandpunkte zu Gehör bringen.") "holger?")
(l (ML Buc "Buch") (ML DmB "Druckwürdige Dokumente zum Thema SWPAT sammeln und für die Buchform vorbereiten.  Buch-Konzept ausarbeiten.  Verleger suchen.") "arnim?")
(l (ML Ptl "Patentanmeldungen") (ML Eer "Ein Patent anmelden und unter GPPL stellen."))
(l (ML Esu "Einspruch") (ML Imh "Information über Offenlegungen beim EPA/DPMA beschaffen und eventuell Einspruch wegen Nicht-Patentierbarkeit einlegen") nil)
(l (ML Mak "Monats-CD") (ML MWi "Monatlich 1 CDROM brennen und in Bibliotheken (Stabi, DPMA) abliefern"))
(l (ML TRd "TVRadio") (ML Fhd "Fernseh- und Radiosendungen z.Th. Patentinflation entwerfen und mögliche Redakteure anschreiben"))
(l (ML GhS "Gespräche mit Siemens") (ML WSg "Wir haben einige Adressen von Siemens-Managern, die eher gegen SWPAT sind.  Wir könnten versuchen, in wieweit man eine Stellungnahme von Siemens für ein behutsameres Vorgehen erwirken könnte."))
; fartab
)
)

(kurz (ML Aef "Akute Aufgaben")
(fartab
(l (let ((URL (href 'eulux-swpatref))) (ML Vcn "Verdeutschung von %{URL}")) (let ((ADR (ahm "petition@eurolinux.org"))) (ML DWu "Die Referenzdokumente erlauben interessierten Einsteigern einen schnellen Überblick über die Problematik.  Zahlreiche Seitenbesucher haben sich begeistert über das schnelle Verständnis geäußert, das ihnen diese Seiten ermöglichten.  Sie tragen nicht unwesentlich zum Erfolg der Petition bei.  Vorgehensweise:  bestehende englische oder französische HTML-Seiten überschreiben und an %{ADR} senden.")) nil (ML EWh "Einige Seitenbesucher haben geklagt, es fehlten deutschsprachige Hintergrundinfos."))
(l (ML Fga "Flugblatt") (ML EPm "Erstellung kompakter 1 oder 2 seitiger SWPAT-Flugblätter, die jeweils einem bestimmten Zielpublikum das wichtigste mitteilen"))
(l (ML Zrn "Zeitschriftenanzeigen") (ML Glc "Grafische Gestaltung von Zeitschriftenanzeigen"))
(l (ML Pae "Popularisierung") (ML Vmn "Vereinfachung des Themas zur Erreichung eines breiteren Kreises") "xuan" (ah "http://www.save-our-software.de"))
(l (ML SnJ "Stellungnahme BMJ EPÜ 52") (ML DPl "Der Vorschlag des EPA zur Streichung aller Patentierungsausnahmen liegt vor.  Eine Stellungnahme ist bis Ende August einzureichen.") "phm" (ahs 'swxepue28))
(l (ML Ain "Arbeitssitzung in München") (ML WWe "Wir sollten uns mal an einem Wochenende in München treffen."))
(l (ML KWP "Konferenz beim EU-Parlament") (ML Ene "EU-Parlamentarier haben zugesagt, eine Anhörung organisieren lassen zu wollen.  Sie haben allerdings einen notorischen Nichtstuer mit der Organisation derselben betraut.   Wir müssten nachhaken und schauen, dass etwas geschieht."))
(l (ML KWB "Kopieren in MPI-Bibliothek") (colons (ML Aga2 "Anregung von Tauchert") (quot (ML zmt "zur allgemeinen geistigen Aufrüstung und speziell zur Systematik aus der Sicht der %(q:Patentjuristen) empfehle ich die Artikel von Busche (Schutz von Computerprogrammen) und Schickedanz (zu mathematischen Methoden und Formeln) in %(q:Mitteilungen Deutscher Patentanwälte) Heft 4/5, S. 164 - 173 und S. 173 - 180."))) "holger")
(l (ML Aek "Akkreditierung EPÜ-Konferenz") (ML BWl "Bei der diplomatischen Konferenz am 20.-29. November in München können Nicht-Regierungs-Organisationen offiziell teilnehmen.  Wir müssen uns schleunigst darum bemühen.") "phm?" (ahs 'swpepue2B))
(l (ML BMB "BMBF") (filters ((bm ahs 'bmbf-patente) (sp ahs 'bmbf-siemens-swpat)) (ML IVn "Innerhalb des BMBF wird eine %(bm:bedenkliche Patent-Ideologie) vertreten: der Kult eines allmächtigen und überall heilbringenden Marktes, dem nun auch die Steuerung der Grundlagenforschung überlassen werden soll.  Besonders kritikwürdig ist etwa der vom BMBF übernommene Vorschlag der Wiedereinführung einer Schonfrist.  Ferner wird dort eine % Pro-SWPAT-Broschüre der Patentabteilung von Siemens unkritisch zum Herunterladen angeboten.  Damit wird das BMBF unterschwellig zur Legitimierung der gesetzeswidrigen SWPAT-Rechtssprechungspraxis in Anspruch genommen.  Diese Initiative stammen aus der Kohl-Ära.  Ministerin Buhlmann hat im Bereich Genpatente hingegen eine gewisse Distanz zur Patentlobby gezeigt.  Wir sollten einen zunächst Kontakt aufnehmen und einen offenen Brief schreiben.")) "xuan?" (ah "http://ffii.org/archive/mails/swpat/2000/Jul/0134.html" "BMBF-Diskussion"))
(l (ML Air "Archivierung") (ML Mdn "Mit der Bibliothek Details der Archivierung vereinbaren und erste CDROM abliefern") "phm" (ML fiW "fürs erste getan, aber es sind noch einige Details zu vereinbaren, so dass das ganze auf fester Grundlage steht."))
(l (ML Sse "Systems") (ML hiS4 "Verhandlung mit Systems 2000 / Linux Media AG über Mitwirkung auf der Müchener IT-Messe im November.") "arnim")
(l (ML hiS "Ökonomische Studie") (ML Ezf "Wirtschaftswissenschaftler suchen, die eine Bestandsaufnahme der bisherigen wirtschaftswissenschaftlichen Fachliteratur zu SWPAT erstellen könnten.") "ark?")
(l (ML BPt "BGH-Petition") (ML Edk "Einen offenen Brief schreiben und bekannt machen, in dem Politiker dazu aufgefordert werden, den BGH zurück auf den Pfad der patentrechtlichen Tugend zu führen") "phm" (ahs 'swxbgh27))
(l (ML Iir "BGH-Artikel") (ML Inh "In einem Artikel genau dokumentieren, wie die Patentinflation am BGH Schritt für Schritt zustande kam.") "phm" (filters ((bh ahs 'swpatbghist)) (ML Eti "Ein %(bh:erster Ansatz) ist vorhanden.")))
(l (ML Pen "Patentreform-Anregungen") (ML Din "Das Patentwesen krankt nicht nur im Softwarebereich.  Vor einer weiteren Konzentration des Patentwesens brauchen wir eine Reform der Qualität.  Dies ist zwar kein Spezialgebiete des FFII, aber indem wir hierzu Anregungen veröffentlichen, können wir vielleicht eine große Koalition verschiedener am Patentsystem interessierter Gruppen anstoßen.") "phm" (ahs 'swxegp27))
(l (ML Ufa "Umfrage") (ML Aie "Anstelle der bisherigen Unterschriftenaktionen ein interessantes Meinungsumfragespiel programmieren") "holger+sfermigier?")
)
)

(done (ML Egf "Erledigte Aufgaben")

(fartab
(l (ML Cte "Computer Zeitung") (ML Dei "Die CZ hat Brüsseler Desinformationen verbreitet.") "ar.kleinert" (filters ((xt ahs 'swxkcz27)) (ML Wte "Wir haben einen %(xt:Brief) geschickt und eine ermutigende Antwort erhalten." (en "We sent a %(xt:letter) and got an encouraging answer."))))
)
)
;sects
)
; swpagtask
)
; swpagnenri-dir
)
; swpatgirzu-dir
)

(mlhtdoc 'swpatsarji nil nil nil
(let ((border 0) (width nil)) (lin
  (ahs 'centerpd (img 'centerpdlogo "Center for the Public Domain"))
  (ahs 'suse (img 'suselogo "SuSE AG"))
  (ahs 'onlkiosk (img 'onlkiosklogo "Online-Kiosk GmbH"))
  (ahs 'infomatec (img 'infomateclogo "Infomatec AG"))
  (ahs 'intradat (img 'intradatlogo "Intradat AG"))
  (ahs 'intevation (img 'intevationlogo "Intevation GmbH"))
)))


(list 'swpatpurci-dir
(mlhtdoc 'swpatpurci nil nil nil
(sects
(ftp (ML Fkc "FTP-Archiv" (en "FTP Archive"))

(lin (ML OGr "Bei allen auf FFII.ORG beherbergten Projekten werden täglich alle veränderten Dateien auf unserem öffentlichen FTP-Archiv gespeichert." (en "Of all projects hosted by FFII.ORG, every day progress deltas are recorded on our public FTP archive."))
(ML EeW "Externe Projekte können mitmachen, indem sie uns Dateien nennen, die wir täglich spiegeln." (en "External projects can participate by naming us their URLs, which are then mirrored and integrated.")))

(ul
 (ah "ftp://ftp.ffii.org/pub/priorart/" (ML FTP "FTP" (de "FTP-Archiv täglicher Neuerungen")))
 (ahs 'swpurminra (ML Pia "Participating External projects" (de "Teilnehmende Externe Projekte")))

 (ahst 'priorartconf (ML AWa "Konfigurationsdatei für die Archivierung" (en "Archiving configuration file")))
 (ahst 'priorartexe (ML AvS "Archivierungs-Skript" (en "Archiving Script")))
)

(ML OWd "Einmal monatlich wird eine CD gebrannt und in einer Bibliothek eingeliefert." (en "Once a month a CD is burned and deposited at a local library."))
;; (l "80328 München Bayerische Staatsbibliothek" "Amtsdruckschriften-Stelle" "zu Hd. von Frau G. Messmer")
)

(konc (ML Pns "Hinweise für Suchende anbringen!" (en "Place hints for searchers!"))

(ML YWi "Ihre Daten schaffen Ihnen zunächst die Möglichkeit, im Ernstfall vor dem Patentgericht zu belegen, dass Sie zuerst da waren.  Aber nicht nur Ihnen sondern allen potentiellen Patentopfern, d.h. uns allen, könnte geholfen werden." (en "Your data may give you the possibility of beating a nasty patent in court one day by proving that you were there first.  Not only you but also the rest of us will benefit."))

(ML Spn "Sorgen Sie dafür, dass Ihre Daten wirklich im Ernstfall nützlich sein können, indem Sie Ihre erfinderischen Schritte ein wenig dokumentieren.  Dazu legen Sie im Stammverzeichnis eine schlichte Textdatei namens PRIORART an, in der auf eventuell interessante Dateien verwiesen wird." (en "So please make sure your data will be as useful as possible by documenting your inventive steps a little bit by placing a file called PRIORART into the  root directory, which will contain a few hints and links to other interesting files, like a CHANGELOG or a mailing list archive."))
)

(links (ML Wrk "Weitere Lektüre" (en "Further Reading"))
(ul
  (ahs 'lutz-logfile)
  (ah "http://books.nap.edu/html/property" "time stamping")
) )
) )

(mlhtdoc 'swpurminra nil nil nil 
 (apply 'ul (mapcar
  ;; (setq lst (car priorart-sites))
  (lambda (lst) (let (URL) (setq URL (cadr lst)) (ah (substring URL 7) (ML mrc "mirror of %{URL}"))))
  priorart-sites)))

(mlhtdoc 'swpursidbo nil nil nil
(ul (ah "ide-gestaltung.txt")))

(mlhtdoc 'swpurcnino nil nil nil (LARGE (ah "cnino.html" (ML loc "list of time-stamped backups" (de "Paketliste")))))
; swpatpurci-dir
)

(list 'swpenmi-dir
(mlhtdoc 'swpenmi nil nil nil 
(table
 nil '((tmp) (lok) (tem)) nil (l
 (l "2000-11-20..29" (tok MUC) (ahs 'swpepue2B))
 (l "2000-11" (tok MUC) (ahs 'swpsyst2B))
 (l "2000-10-20" (tok MUC) (ahs 'swpboell2A))
 (l "2000-10-17" (tok MUC) (ahs 'swpverd2A))
 (l "2000-06-30..07-02" "Stuttgart" (ahs 'swplxtg26 (ML FTW "FFII-Stand mit Tagungsprogramm auf dem LinuxTag" "FFII booth and conference program at LinuxTag")))
 (l "2000-05-18" "Berlin" (ahs 'swpbmwi25 (ML Bwu "BMWi-Konferenz über die wirtschaftlichen Auswirkungen von Softwarepatenten" "German Ministery of Economics Conference on Impact of Software Patents")))
 (l "1999-11-17/18" (tok MUC) (ML Ksi "Konferenz des Max-Planck-Institutes, der Marie-Curie-Gesellschaft und des Europäischen Patentamtes über die neuen Möglichkeiten des Patentrechts in Informations- und Lebenstechnik" "Patent Organisations Conference on Information and Life Technology"))
 (l "1999-06-13" (tok MUC) (ahst 'kongress (ML FmW "FFII-Tagung %(q:Informationelle Monokultur und die Alternativen)" "FFII Conference %(q:Informational Monoculture and the Alternatives)")))
 ) )
)

(mlhtdoc 'swpepue2B
 nil
 (ML Asn "On Nov 20-29 the %(q:Diplomatic Conference) for the Revision of the European Patent Convention will take place in Munich.  The FFII is looking for ways to participate." (de "Am 20.-29. November findet in München die %(q:Diplomatische Konferenz) zur Änderung des Europäischen Patentrechts statt, auf der die bisher gesetzeswidrige Patentierung von Computerprogrammen legalisiert werden soll.  Der FFII hält neben dem Europäischen Patentamt 2 Vortragsveranstaltungen ab."))
 nil
 (dl
  (l (ML Wan2 "Wann?") (ML Dnb "Dienstag den 21. und 28. November 11-13 Uhr"))
  (l (ML Wo3 "Wo?") (tpe (ML FWT "Forum der Technik, Raum Helios") (ML Ant "Auf der Museumsinsel, S-Bahn-Haltestelle Isartor")))
  (l (ML Ren "Referenten") 
     (dl
      (l (ML AuW "Arnim Rupp und Hartmut Pilch (FFII)") (ML nti "Übersicht über die Patentdatenbank, Beispielfälle, neue politische Initiativen"))
      (l (tpe "N.N." (ML SrP "Softwareunternehmer und Programmierer")) (ML Aee "Auswirkungen dieser Patente auf Softwareunternehmer und Programmierer"))
      (l (tpe (ML DKe "Dr. Swen Kiesewetter-Köbinger") (ML PpW "Patentprüfer am DPMA") (ML Sen "Probleme und Ungereimtheiten bei der Prüfung von Patentanträgen auf Computerprogramme")))
      (l (tpe (ML UiW "Ulrich Wolf") (ML Ceu "Chef-Redakteur des Linux-Magazins")) )
      ) )
  (l (ML Por "Programm") (ML Eee "Einführungsreferate und Fragen an das Podium"))
  ) 
)

(mlhtdoc 'swpboell2A  
 (l 
   (ML WEW "Wem gehört das Wissen?")
   (mlval* 'WEW)
   (ML WnS "Wie wollen wir informationelles Schaffen belohnen?")
   (ML Lur "Das Digitale Dilemma und Antwortversuche von OpenSource bis zu Urheberrechtsabgaben und Softwarepatenten") )
 (ML EFi "Eine Tagung am 20.-21. Oktober in Berlin in der Galerie der Heinrich Böll Stiftung")

 nil

(sects
(ver 
 (ML Vti "Veranstalter")
 (let ((HBS (ah "http://www.boell.de" (ml "Heinrich Böll Stiftung"))) (FFII (ahs 'ffii))) (ML Hzr "%{HBS} in Kooperation mit Netzwerk Neue Medien und %{FFII} (FFII e.V.)"))
)

(mit
 (ML IWe "Im Mittelpunkt der Tagung stehen die Fragen:")

 (Large (ul
  (ML Wra "Wie wollen wir die Schöpfer von digitalen Werken und Innovationen künftig belohnt sehen?")
  (ML Won "Wohin führt die Verschärfung des Urheberrechts, die Ausweitung des Patentrechts, die Schaffung neuer Schutzrechte?")
  (ML Wad "Was bringt die kollektive Belohnung über Pflichtabgaben (Urheberrechtsabgabe, Rundfunkgebühren, Steuergelder etc) oder Mikro-Spenden (digitales Straßenmusikmodell)?")
  (ML Bhc "Erübrigt der Erfolg der Freien Software die Sorge um Belohnungssysteme?") ))
)

(prg (ML Pga "Pogramm")

(filters ((pg ah "http://www.boell.de/downloads/vkal/geistigeseigentum.pdf") (vk ah "http://www.boell.de/downloads/vkal/")) (ML Dor "Bem:  Dies ist ein möglicherweise veralteter Entwurf.  Das %(pg:definitive Programm) ist im %(vk:Veranstaltungskalender der Heinrich-Böll-Stiftung) zu finden."))

(sects
(tag1 (ML Fa0 "Freitag, 20.10.00")

(programmpunkte
(l "17.00" (ML Efu "Eröffnung"))
(l (ML 1k5 "17.30-18.15 Uhr")
   (ML Wib "Wissen als Eigentum? Wie kann das Recht auf freien Zugang zu Wissen und Information in der globalen Wissensgesellschaft gewährleistet werden?")
    "NN") 
; Deutsche UNESCO (oder Rainer Kuhlen, Uni Konstanz, Inhaber des UNESCO-ORBICOM-Chairs in Communications oder?) ( noch nicht angefragt)

(l (ML 1W0 "18.15 -19.00 Uhr")
   (ML Gez "Geistiges Eigentum in der Wissensgesellschaft: Herausforderungen an ein zeitgemäßes Urheber- und Patentrecht")
   "NN") 
; Prof. Bernd Lutterbeck, Institut für Informatik, TU Berlin oder Prof. Wolfgang Coy (Institut für Informatik, Humboldt Uni, Berlin) oder Prof. Dr. Josef Strauß, Leiter des Referats Ausl. Und Internat. Patentrecht... des Max-Planck Instituts f. ausl. Und Internationales Patent- Urheber- und Wettbewerbsrecht, München 
(l "19-20" (ML Ans "Abendessen"))
(l "20.00-22.00" 
   (al
    (colons (ML FwW "Fishbowldiskussion:") (ML Pid "Patentrecht, Urheberrecht oder maßgeschneidertes Softwarerecht:  womit lässt sich die Software-Innovation wirklich fördern?"))
    (small
     (lin   
      (filters ((ep ahs 'eulux-petition)) 
       (ML D0h "Das Europäische Patentamt möchte in Kürze das Patentrecht ändern, um Computerprogramme patentierbar zu machen.  Derweil unterstützen 50000 Bürger, 200 Softwareunternehmen und zahlreiche Politiker eine %(ep:Petition für ein softwarepatentfreies Europa).  Die meisten von ihnen bevorzugen die ausschließliche Anwendung des Urheberrechts auf Software.  Darüber hinaus gibt es diverse Entwürfe für softwarespezifische Alternativen zum Patentschutz."))
      (table nil '((tem) (hom)) nil (l
       (l (ML WmW "Wer will eigentlich wann auf welchem Wege was entscheiden?  Wie können wir auf diese Entscheidungen einwirken?") "N.N.")					  
       (l (ML W3s "Warum sind Computerprogramme laut PatG/EPÜ nicht patentierbar?  Der Technizitätsbegriff und seine Anwendung in alten und neuen Grenzfällen") "N.N.")
       (l (ML Wgk "Was haben wir davon, wenn wir informationelle Innovationen mit Patenten belohnen? Kurzvorstellung alter und neuer volkswirtschaftlicher Theorien über die Wirkungen des Patentwesens") "N.N.")
       (l (ML Boz "Bedarf das Urheberrecht der Ergänzung durch ein gesondertes Schutzrecht für Software-Innovationen? Kurzeinführung in die Debatte über Sui-Generis-Systeme") "N.N.")
       ))) ) )
   (nl "NN"
; Mit:
; - Margareta Wolf, MdB Bündnis 90/Die Grünen 			(angefragt)
; - Jörg Tauss, MdB,  SPD							(angefragt) 
; - Bundesministerium der Justiz: Dr. Welp, Referat III 		(noch nicht angefragt)
;   oder BMWirtschaft: Swantje Weber-Cludius) 		(noch nicht angefragt)
; - Europäisches Patentamt: Winfried Schmid 	
;   oder Deutsches Patentamt NN	
; - Daniel Riek, Vorstand ID-PRO 					(zugesagt) 
; - Hartmut Pilch, FFII e.V. 						(zugesagt)
; - VerterterIn einer großen Softwarefirma (Siemens..?) 
; - oder Patentanwaltskanzlei (Vorschläge...)
; (dazu die beiden Referenten) 
; 
(ML Maa "Moderation: Stefan Krempl (Journalist, Telepolis)"))
; 		(noch nicht angefragt)
; oder Christiane Schulzki-Haddouti (Journalistin, spiegel-online)	
) )  
; tag1
)

(tag2 (ML Sal "Samstag 21.10.00")

(programmpunkte
(l "10-12 Uhr" 
   (lin (colons "Panel I"
	   (ML C3n "Erübrigt der Erfolg des Opensource-Geschäftsmodells die Sorge um das richtige Belohnungssystem?") )
    (small (ul 
     (ML Woi "Was leistet heute Freie Software für die Volkswirtschaft?  Welche Rolle spielen dabei diverse %(q:Opensource-Geschäftsmodelle)?")
     (ML Wsr "Würden in einer Welt ohne künstliche Belohnungssysteme (staatlich sanktionierte Eigentumsrechte, staatliche Forschungs- und Kulturförderung) genügend gute Computerprogramme, Kunstwerke etc entstehen?")
     (ML Btd "Beweist der Erfolg der Freien Software, dass es eine %(q:Tragödie der digitalen Allmende) nicht gibt?")
     (ML Oev "Welche politischen Rahmenbedingungen braucht die digitale Allmende, um zu gedeihen?") ) )
    )

    (dl
     (l (ML Ilf "Impulsreferate") "NN + NN   + NN")
     (l (ML Met "Moderation") "Markus Beckedahl") ) )

(l (ML _101 "12.30 - 14 Uhr")
   (lin
    (ML Pnl "Panel II")
    (small (ul 
     (ML IeW "Ist die von der Bundesregierung geplante Urheberrechtsabgabe auf IT-Geräte der richtige Weg, private und öffentliche Interessen zu wahren?")
     (ML WMr "Wohin führt der Gegenvorschlag einiger IT-Verbandsfunktionäre, einen perfekten digitalen Kopierschutz und damit einen Markt für digitale Waren zu schaffen?  Was ist von entsprechenden Initiativen (DMCA, Ucita, IFPI-Vorstoß, DVD-Streit, ASF-Streit, NAIIN) zu halten?") 
     (ML Wit "Sind die traditionellen Verwertungsrechte angesichts der digitalen Möglichkeiten (Napster, Gnutella, FreeNet, Mojonation) noch haltbar?  Bringt die Einführung von freiwilligen Mikrogebühren (z.B. Mojonation, Street Performance Protocol) eine interessante Entlohnungsmöglichkeit für Musiker und Autoren?")
     (ML IWi "Ist die Klassische Musik von Bach bis Brahms eine Opensource-Kultur früherer Zeiten?  Wo sind dann heute die Kulturmäzene zu suchen?  In der Werbebranche?  In öffentlich-rechtlichen Systemen?")
     (ML Lrm "Lässt sich die Tradition der Urheberrechtsabgaben in etwas zeitgemäßeres umwandeln?  Etwa in ein System zur Förderung der Informationsallmende, bei dem qualifizierte Fördergesellschaften ihre Belohnungsgrundsätze selber bestimmen und der Bürger über die Zuteilung seiner Pflichtabgabe an die eine oder andere Gesellschaft frei entscheidet?")
     ))
   (dl 
    (l (ML Sln "Stellungnahmen") (ML GtW "Grietje Bettin, MdB +   NN   +   NN"))
    (l (ML Met2 "Moderation") (ML Oea "Oliver Passek")) ) ) )

(l "14 Uhr" (ML Mfd "Mittagsbüffet, Ende der Tagung")) 
(l "15 bis 19 Uhr" (ML TWN "Treffen des Netzwerks Neue Medien"))
)
;tag2
)
;sects
)
; prg
)
;sects
)
; swpboell2A
)


(mlhtdoc 'swpverd2A (sepcc " - " (ML IaE "Informationsökonomie") (ML Sat "Softwarepatente") (ML Onu "OpenSource"))
 (ML ADF "Am 17. Oktober nehmen wir an einer Tagung der Fraktion Bündnis 90 / Die Grünen in Berlin teil, auf der die Grundsatzposition der Grünen Fraktion zur Eingrenzung des Patentwesens weiter ausgearbeitet wird.")
 nil
(dl
(l (ML Pki "Podium 3: Diskutanten und ihre Referate")
 (ol
  (nl (ML WWo "Was ist Open Source ?")
      (commas "Oliver Zendel" "Linuxtag" "Chairman") ) 
  (nl (ML Dwi "Das Geschäftsmodell eines Softwaredienstleisters auf der Basis von Open-Source Daniel Riek")
      (commas "ID-PRO AG" "Vorstand")
      (ul
       (ML Ons "Open Source als Chance für die europäische Software-Branche")
       (ML GpW "Gefährdung von Open Source durch Softwarepatente") ) )
  (nl (ML Kea "Konzepte und Schritte für eine klare Eingrenzung und wirksame Kontrolle des Patentwesens")
      (commas "Hartmut Pilch" (ahs 'ffii) "Vorstand") ) 

  (nl (ML Fot "Für Wettbewerb, Transparenz und Kooperation - Open Source unterstützen, Softwarepatente zurückdrängen")
      (commas "Margareta Wolf MdB" (ML Ft0 "Fraktionsvorstand Büdnis 90/ Die Grünen")) )
  ) )

(l (ML Met "Moderation") (commas "Elisabeth Niejahr" "Die Zeit"))

(l (ML Zei "Zeit") (ML Dua "Der gesamte Kongress beginnt um 9.30 Uhr und endet gegen 19.00 Uhr. Er wird vom Fraktionsvorsitzenden Rezzo Schauch eröffnet."))

(l (ML TWW "Themen der 4 Podien")

   (ol
    (ML Vls "Virtuelle Hochschulen")
    (ML KWk "Klassische und Neue Medien:  Divergenz in der Gegenwart - Konvergenz für die Zukunft ?")
    (ML Ieo "Informationsökonomie - Open-Source - Softwarepatentierung")
    (ML EWa "E-commerce und Verbraucherschutz") ) ) 
)
)

(list 'swpbmwi25-dir
(mlhtdoc 'swpbmwi25 
(ML KWn "KMU-Konsens gegen Softwarepatente beim BMWi" "Consensus of SMEs against software patents at the German Ministry of Economy")
(ML Inr "Im BMWi in Berlin begann am 18. Mai 2000 ein Dialog zwischen Vertretern des Patentwesens und IT-Unternehmen, die sich von dessen ungezügelter Expansion bedroht fühlen.  Presseerklärung des aktiv beteiligten FFII." "The German Ministery of Economics and Technology hosted a dialog between representatives of the patent system and IT entrepreneurs who feel threatened by its apparently uncontrollable expansionism.  Press Release of the FFII, who actively participated.")
nil
(sects
(dekl (ML Pev "Presseerklärung" "Press Release")
(sepcc " - " (commas "FFII" "Berlin 2000-05-18") (ML EWi "Etwa 40 Vertreter von deutschen Softwarefirmen, Regierungsstellen, Patentämtern, Hochschulen und Verbänden trafen sich heute im Bundesministerium für Wirtschaft und Technologie in Berlin, um über die Wirkung von Softwarepatenten auf Wirtschaft und Gesellschaft zu diskutieren." "About 40 representatives of german software companies, government departments, patent offices, universities and associations met today at the German Federal Ministry of Economy in Berlin in order to discuss the effects of software patents on the economy and on the information society."))

(let ((LST (kaj (ah "http://www.intradat.de" "Intradat AG") (ah "http://www.phaidros.com" "Phaidros AG") (ah "http://www.infomatec.de" "Infomatec AG") (ah "http://www.id-pro.de" "ID-Pro AG") (ah "http://www.suse.de" "SuSE Linux AG")))) (ML VWn "Vertreter erfolgreicher deutscher Softwarefirmen wie %{LST} und einer Reihe kleinerer IT-Firmen übten scharfe Kritik an der Ausweitung des Patentsystems auf informationelle Güter." "Representatives of successful german software publishing companies such as %{LST} as well as other smaller german software companies clearly expressed their opposition to the extension of the patent system to the realm of informational goods."))

(lin
 (ML Reo "Patentrechtler von Siemens, IBM, Patentämtern und Anwaltskanzleien führten rechtliche und moralische Gründe an, um die Ausdehnung des Patentwesens auf die Sphäre der Information zu rechtfertigen.  Der FFII stellte jedoch eine große Sammlung juristischer Dokumente vor, um zu belegen, dass die Europäischen Patentämter es nur durch Rechtsbeugung geschafft haben, gegen die Bestimmungen des Gesetzes Patente auf reine Information zu gewähren.  Der FFII wies ferner nach, dass, entgegen üblichen Propagandabehauptungen der Patentrechtler, der TRIPS-Vertrag Europa nicht im geringsten dazu verpflichtet, Softwarepatente zu gewähren.  Die Konferenzteilnehmer führten dazu Beispiele von gesetzeswidrig gewährten europäischen Softwarepatenten an und kritisierten deren Trivialität und verheerende ökonomische Wirkung." "Representatives of the intellectual property department of Siemens, IBM and patent offices as well as independent patent experts invoqued legal and moral reasons to justify the extension of the patent system to the informational sphere.  However, the FFII exhibited a large collection of juridical documents which give clear evidences that european patent offices have abused the law by granting software patents. The FFII also proved that the TRIPS international agreements do not require Europe to grant patents on software.  Moreover, conference participants showed striking examples of the adverse economic effects of software patents.") 
)

(lin
 (ahst 'swpahar25 (ML Aem "Auf Einladung des FFII gab Gregory Aharonian, der Gründer und Leiter eines in Fachkreisen viel beachteten Internet-Patentnachrichtendienstes, einen Überblick über das amerikanische Patentsystem und dessen Unfähigkeit, vernünftige Prüfprozeduren anzubieten und echte Erfindungen auszuwählen." "Upon invitation of the FFII, the famous American patent researcher Gregory Aharonian gave an overview of the American patent system, its failure to provide a reasonable examination procedures and select real inventions."))
 (ML AWn "Aharonian schöpfte aus einem reichen Fundus an Beispielen und eigenen Statistiken, um zu zeigen, dass Software eines der Gebiete ist, die zur Innovation keines besonderen Anreizsystems bedürfen und in denen Patente eher hemmend als fördernd wirken." "Mr. Aharonian used a wealth of examples and statistics to show that software is one of several areas in which there is no need for a special innovation incentive system and the patent system does far more harm than good.") )

(filters 
 ((cd ahst 'swpdokucd) (jv ahst 'swpbpos25))
 (ML Dct "Die vom FFII erstellte Sammlung patentrechtlicher Dokumente wurde auf der Konferenz auf %(cd:CDROM) zusammen mit einem %(jv:Positionspapier) verteilt, das dem Europäischen Patentamt und nationalen Patentämtern Rechtsbeugung vorwirft und dafür ausführliche Belege liefert." "The collection of juridical documentation prepared by the FFII was distributed on %(cd:CD-ROM)s at the conference together with a %(jv:paper) that cites ample evidence of abuse of the European Patent Law by the European Patent Office as well as other national european patent offices."))

(ML DeW "Die Vertreter der Kleineren und Mittleren Unternehmen argumentierten, dass das Urheberrecht besser geeignet und im allgemeinen völlig ausreichend sei, um Neuerungen zu belohnen und die Interessen kleinerer und mittlerer Softwarehäuser zu schützen." "The SME representatives argued that copyright is more suitable and quite sufficient for rewarding innovation and protecting the interests of small and medium size software publishers.")
)

(kont (ML Kna "Kontakt" "Press Contact") 

(nl "Hartmut Pilch" "phm@ffii.org" "089-12789608")
)
 
(ffii (ML nrF "Mehr über den FFII" "About FFII")

(filters ((fi ahs 'ffii) (sp ahs 'swpatgirzu) (sj ahs 'swpatsarji) (el ahs 'eurolinux))
 (ML cas "Der %(fi:FFII) ist ein gemeinnütziger Verein, der die Entwicklung offener Schnittstellen, quellenoffener Software und frei verfügbarer öffentlicher Informationen fördert.  Der FFII koordiniert eine %(sp:Softwarepatent-Arbeitsgruppe), die von %(sj:erfolgreichen deutschen Softwarefirmen gefördert) wird.  Der FFII ist Mitglied des %(el:EuroLinux-Bündnisses)."  "%(fi:FFII) is a non-profit association which promotes the development of open interfaces, open source software and freely available public information. FFII coordinates a %(sp:workgroup on software patents) which is %(sp:sponsored by successful german software publishers). FFII is member of the %(el:EuroLinux Alliance)."))
)
(vreji (ML Vre "Verweise" "Links" "Liens") (ul
 (ahv 'bmwi25-pe (ML Pkd "Presseerklärung des BMWi" "Press Statement of the German Ministery of Econ. and Tech." "Déclaration de Presse du Ministère d'Econ. & Tech."))
))
; sects
)
; swpbmwi25
)

(mlhtdoc 'swpbcpe25 
 (ML DnS "Die wirtschaftlichen Auswirkungen der Software-Patentierung" "Economic Effects of Software Patenting")
 (ML DWr "Das Bundeswirtschaftsministerium hält am 18. Mai 2000 in Berlin eine öffentliche Tagung ab, auf der die wirtschaftlichen Auswirkungen der Patentierbarkeit von Computerprogrammen untersucht werden sollen." "The German Ministery of Economics is convening a public conference to investigate the economic impact of software patentability on May 18 in Berlin.") 
 nil

(filters ((sj ahst 'swpatsarji) (sa ahst 'swpatgirzu) (bw ahst 'soquat25)) (let ((FFII (ahs 'ffii))) (ML Dte "Der %{FFII} und die %(sj:Förderer) seiner %(sa:Softwarepatent-Arbeitsgruppe) sind eingeladen.  Wir werden uns an in dieser %(bw:Konferenz) aktiv beteiligen." "The %{FFII} and the %(sj:sponsors) of its %(sa:Software Patent Workgroup) are invited. We will play an active role in this %(bw:conference).")))   

(filters ((fp ahst 'swpbmwi25)) (ML Der "Der FFII wird %(fp:gegen die Ausdehnung des Europäischen Patentwesens in immer industriefernere Sphären argumentieren).  Diese Konferenz ist eine seltene Chance.  Sie muss ein Erfolg für all diejenigen werden, die von informatischem Monopolismus nichts Gutes zu erwarten haben.  Sie können auf verschiedenerlei Weise dazu beitragen." "The FFII is preparing to %(fp:argue the case against the expansion of the European patent system into more and more upstream informational realms).  This conference is a rare chance.  It must become a success for all those who have nothing to gain from informational monopolism.  You can contribute in several ways."))

(sects
(act (ML Wek "Was Sie tun können" "What you can do")
(sects
(partop 
 (ML Ane "An der Konferenz teilnehmen" "[ in Germany only: ] Attend the conference")
 (ML Dnh "Die Konferenz ist öffentlich, aber die Teilnehmer sollten sich vorher anmelden und beim Veranstalter ein kurzes Positionspapier abgeben.  Setzen Sie sich dazu mit uns in Verbindung.  Wir haben in Berlin auch ein paar Hotelbetten reserviert." "The conference is public, but participants should register and submit a short statement to the host.  Contact us.  We have also reserved some hotel beds in Berlin.") )

(varb (ML WTe "Weitere Teilnehmer werben" "[ in Germany only: ] Get others to participate")
 (ML Ddt "Dies ist eine erste und vielleicht letzte Gelegenheit für Programmierer und Firmen, sich über schicksalsentscheidende Entwicklungen kundig zu machen und darauf einzuwirken.  Bitte sprechen Sie Firmen und Personen in Ihrem Einflussbereich an.  Bringen Sie sie u.U. in Kontakt mit uns, damit wir bei der Abfassung von Positionspapieren helfen und bei der Vorbereitung der Konferenz zusammenarbeiten können." "This is a first and maybe last opportunity for programmers and executives to inform themselves and voice their opinions about some thorny issues which may decide their fate tomorrow.  Please speak to people within your realm of influence.  Bring them into contact with us, so that we may help in drafting statements and cooperate in preparing the conference.")
)

(doku 
 (ML Dnn "Dokumentations-CD zusammenstellen helfen" "[ everywhere: ] Help put together a Documentation CD")
 (filters ((sd ahst 'swpdokucd) (ml ah "http://ffii.org/mailman/listinfo/swpatdoku/")) (let ((IN (ah "http://www.intevation.de" "Intevation GmbH"))) (ML Dei "Der FFII stellt eine %(sd:Dokumentation) (gedruckt und auf CD) zusammen.  Wir müssen viele Verlagshäuser um Kopiererlaubnis bitten und manche wichtigen Texte sind von nicht-digitalen Datenträgern her zu erfassen.  Diese Arbeit wird î %(ml:öffentlich zugängliches Forum) geleistet, auf dem der Projektleiter %{IN} täglich über den Stand der Arbeit berichtet.  Helfen Sie mit Rat und Tat!" "The FFII is putting together a %(sd:documentation) (printed and on CD) for the conference.  We need to ask many publishers for copying permissions and some important texts must be gathered from non-digital media.  This work is done via a %(ml:publically accessible mailing list) with daily progress reports by the project leader, %{IN}.  There is a lot to do for everybody, and even occasional small advice can help."))) )
)
(ML DKh "Der FFII ist dank seiner Förderer in der glücklichen Lage, für geleistete Arbeit Entschädigungen auf Stundenbasis zahlen zu können." "The FFII is in the lucky position that it can offer some compensation on a per hour basis for work being done, thanks to its corporate Sponsors.")
)

(konf (ML HuK "Hintergrund der Konferenz" "Background of the Conference")

(ML DWc "Die Europäischen Patentorganisationen und die EU-Kommission beseitigen derzeit alle gesetzlichen Hürden, die bisher noch der Patentierung von Logikalien (Software) im Wege stehen." "The European Patent Organisations and the EU Commission are planning to remove all obstacles to software patentability from European patent law.")

(ML EWn "Es ist das erste Mal, dass eine Regierungsorganisation in Europa die Frage nach den Auswirkungen dieser Veränderungen auf die Wirtschaft und das Gemeinwohl stellt." "This is the first time that a governmental body in Europe is taking a closer look at the socio-economic effects of this proposed change.")

(ML Dgi "Die Bundesregierung ist Vertragsstaat des Europäischen Patentabkommens und muss als solche allen Änderungen dieses Abkommens genehmigen.  Verweigert sie ihre Zustimmung, so werden %(q:Programme für Datenverarbeitungsanlagen) weiterhin auf der Liste der Ausnahmen von der Patentierbarkeit in §52 EPÜ bleiben.  Andernfalls wird der Weg für die volle Durchsetzung von Logikalienpatenten in Europa und Deutschland noch dieses Jahr frei." "The German governement is a member state of the European Patent Convention therefore entitled to deny its approval to any change of this convention.  If it denies its approval, %(q:computer programs) will remain on the list of exceptions to patentability.  If not, the way to full enforcement of software patent claims will be paved later this year.")

(ML DkW "Der FFII befürchtet, dass Logikalienpatentierbarkeit zu proprietären Standards (d.h. Blockierung von Kompatibilität durch private Besitzansprüche) führt, Monopoltendenzen stärkt, die Erneuerungskräfte hemmt und insbesondere kleine und mittlere Softwarefirmen gefährdet." "The FFII fears that software patentability will favor proprietary standards,  reinforce monopolistic tendencies, slow down the rate of software innovation and damage small to medium size software companies.")

(ML Icr "In wenigen Monaten könnte eine lange angestaute Lawine von Drohbriefen und Patentprozessen über uns hereinbrechen, und Sie werden vielleicht sich daran gewöhnen müssen, einen Patentanwalt aufzusuchen, bevor Sie ihre Programm-Quelltexte im Netz veröffentlichen.  Es wird dann zu spät sein, etwas wirksames dagegen zu unternehmen." "In a few months an avalanche of patent ligitation and extorsion letters may roll over us, and you may no longer dare to publish your source code without first consulting with a laywer.  And it will then be too late to do anything about it.")
  
(ML Hcr "Handeln wir also jetzt.  Machen wir die Berliner Konferenz zu einem großen Erfolg." "So let's act now.  Let's make the Berlin Conference a great success.")
)

(sign (ML Urc "Unterzeichner" "signed by")
(center
(apply 'nl (mapcar (lambda (lst) (sepcc " " (car lst) (string-enclose lt gt (cadr lst)))) (l 
(l "Markus Fleck" "fleck@gnu.org")
(l "Joerg Freudenberger" "Joerg.Freudenberger@nbg9.siemens.de")
(l "Hartmut Pilch" "phm@ffii.org")
(l "Bernhard Reiter" "breiter@ffii.org")
(l "Arnim Rupp" "rupp@ffii.org")
))))

(ML pH8 "für den FFII" "on behalf of the FFII")
;sign
)

(prog (ML Krr "Konferenzprogramm" "Conference Programme")

(dl
(l (ML Tus "Tagungsort" "Conference Site")
(nl (tpe (mltr "Scharnhorststr 36") (bridi 'cf (ah "bmwi-lage.gif" (ML Lgp "Lageplan" "Position Map"))))
 (ML BmW "Bundesministerium für Wirtschaft und Technologie" "Federal Ministery for Economics and Technology")
 (ML HuW "Haus D" "Building D") (ML 1ec "1. Obergeschoss" "Floor 1") (ML Hra "Hörsaal" "Auditorium") )
)

(l (ML Krp "Konferenzsprache" "Conference language")
   (ML Dus "Deutsch" "German")
   (ML Sll "Es wird Simultandolmetschung ins Englische angeboten werden." "Simultaneous interpreting into English is provided.") )

(l (tpe (ML Zip "Zeitplan" "Schedule") (ML urd "unverbindlich" "non-committal"))

(table nil '((tmp) (tem) (hom)) (l (l (ML Wan "Wann?" "When?") (ML Was "Was?" "What?")))
(l
(l "10.15" (ML Efu "Eröffnung" "Opening"))
(l "10.30" (ML Wel "Wirtschaftspolitische Bedeutung der Freien Software sowie deren Relevanz für Sicherheit im Internet" "Economic Siginificance of Free Software" "Significance of Free Software for Economic Policy and for Internet Security"))
(l "11.00" (ML Ste "Stand der neuesten Entwicklungen im Patentrecht" "Status of Current Developments in Patent Law"))
(l "11.30" (ML Wee "Wirtschaftliche Interessenlagen hinsichtlich der Patentierung von Computerprogrammen unter besonderer Berücksichtigung von freier Software" "Economic Interests affected by the Patenting of Computer Programs, with Special Regard to Free Software"))
(l "11.30" (ahs 'swpahar25))
(l "13.00" (ML Shf "Stehbuffet" "Lunch Buffet"))
(l "16.45" (ML Sls "Schlusswort" "Concluding Speech"))
) )

(filters ((si ahst 'soquat25)) (ML OWi "Obige Angaben dienen nur der Übersicht.  Verbindlich ist im Zweifelsfall die %(si:offizielle Leitseite der Veranstaltung)." "The above statements are intended to provide an easy overview.  In case of doubt, refer to the %(si:official homepage of the event)."))
)

(l (ML TmW "Teilnehmer und Redner" "Participants and Speakers")
 (ML Afa "Allerlei interessierte Softwarefirmen aus Deutschland sind eingeladen worden." "Interested Parties, especially from German software companies, have been specially invited.")
 (ML Jcw "Jeder Teilnehmer sollte sein Referat in Form eines schriftlichen Positionspapiers abgeben.  Das spart Redezeit.  Der Schwerpunkt soll nämlich auf dem freien Gedankenaustausch liegen." "Each active participant should submit a position paper so that there is a basis on which to discuss.  Most time will be allocated to free discussion.")
 (filters ((bp ah "http://www.bustpatents.com") (pn ah "http://www.bustpatents.com/patnews/") (ap ahst 'swpahar25)) (ML Ade "Als vom FFII zugeladener Gastredner wird Gregory Aharonian %(ap:Rede und Antwort stehen)." "As a special guest invited by the FFII, Gregory Aharonian will be available to answer questions.")) )

(l (ML Ben "Bisherige Stellungnahmen" "Statements made so far")
   (ah "http://www.sicherheit-im-internet.de/themen.phtml#2" (ML Teh "Themenübersicht der Initiative Sicherheit-im-Internet" "Overview Page of the host (in German)")) )
)
)

(l (ML Uee "Unser besonderer Gast" "Our Special Guest")

(ML Esu "Eigentlich sollten sich auf dieser Konferenz die an den Wirkungen der Softwarepatentierung interessierten Kreise aus Deutschland versammeln und sich ungestört von der sonst so allgegenwärtigen internationalen Lobbyarbeit eine Meinung bilden.  Wir haben deshalb darauf verzichtet, profilierte Gäste aus dem Ausland einzuladen.  Mit einer Ausnahme." "This conference is intended to assemble interested parties in Germany and let them form an opinion independent of the omnipresent international lobbying.  Therefore we refrained from inviting famous foreign guests.  With one exception.")

(filters 
 ((bp ah "http://www.bustpatents.com"))
 (let ((GA "G. Aharonian"))
   (ML cgd "%{GA} durchleuchtet seit 1994 mit seinem Patent-Nachrichtendienst den anschwellenden Strom der Softwarepatente und hat wie kein anderer unabhängig von allgemein-philosophischen Vorgaben Unmengen von Patentansprüchen auf den Zahn gefühlt und mit statistischen Studien die Wirkungsweise des real existierenden Patentsystems zu erfassen --- und nutzbar zu machen --- versucht.  Seine Analysen bringen Aktienkurse zum Einsturz, seine Tipps machen Optionsscheinkäufer reich.  Auf diese Fähigkeit hat Aharonian ein eigenes potentes %(bp:Unternehmen) gegründet, das einzigartige Einblicke in die Ökonomie der Softwarepatente ermöglicht und zugleich eine der innovativsten Institutionen ebendieser Neuen Ökonomie ist." "%{GA} has thouroughly investigated the swelling flood of software patents since 1994 and has sought to document and utilise the (mal)functioning of the software patent system as it stands. His analyses make share values crash, his hints make stock option buyers rich.  On this ability Aharonian has built his own powerful %(bp:company), which makes unique insights into the econonomy of software patents possible and is probably one of the most innovative and exciting institutions of this New Economy.") ) )
)
)
)

(list 'swpbpos25-dir
(mlhtdoc 'swpbpos25
 (ML AeB "Anregungen des FFII zur BMWi-Konferenz" "FFII ideas for the Berlin Conference")
 (ML Eht "Erstmals fragt eine europäische Regierungsorganisation öffentlich nach den Auswirkungen geplanter Gesetzesänderungen im Patentwesen auf die Informationstechnik, die IT-Wirtschaft und das Gemeinwohl.  Der FFII möchte dazu beitragen, dass diese überfälligen Fragen mit dem gebührenden Ernst studiert werden, bevor man einschlägige Gesetze und Verträge ändert." "For the first time a European government body is inquiring into the effects of planned patent law changes on information technology, IT economy and public interest.  The FFII wants to contribute to a thoughrough investigation of these questions, which should precede any changes of law.  Much of this paper remains untranslated.")
 nil
 (ML Wih "Im Rundschreiben an ihre Mandanten vom Januar 2000 verkündet eine Münchener Patentanwaltskanzlei erregende Neuigkeiten:")
 (blockquote 
  (apply 
   'dl 
   (mapcar
    (lambda (sec) (cons (bold (car sec)) (cdr sec)))
    (l
     (l
      (ML Neg "Neue BGH-Entscheidung ermöglicht Patentierung von Computerprogrammen")
      (ML IWo "In einer noch unveröffentlichten Entscheidung vom 13. Dezember 1999 hat der Bundesgerichtshof nun endlich die Gelegenheit ergriffen und Softwareerfindungen, denen technische Überlegungen zu Grunde liegen, für patentfähig erklärt.  Damit hat der BGH mit der Entscheidungspraxis des Europäischen Patentamtes gleichgezogen und das Tor für die Erteilung von Softwarepatenten auch durch das Deutsche Patent- und Markenamt weit geöffnet.  Da unter diese technischen Überlegungen auch programmtechnische Gedanken wie etwa die Verringerung der Rechenzeit, die Einsparung von Speicherplatz usw fallen, sind in der Praxis fast alle Programme dem Patentschutz zugänglich, sofern sie neu sind und auf einer erfinderischen Tätigkeit beruhen. ...") )
     (l
      (ML Pna "Patentierung von Geschäftsverfahren in den USA")
      "..."
      (ML DWW "Die neueste Entwicklung geht dahin, dass Geschäftsverfahren sogar unabhängig von einem Computerprogramm patentiert werden.") )
     (l
      (ML Ken "Konsequenzen")
      (ML Dtr "Durch die oben skizzierte Entwicklung der Rechtsprechung sowie die zunehmende Internationalisierung des Geschäftslebens hat sich das Patentrecht von der traditionellen Beschränkung auf die verarbeitende Industrie gelöst und ist heute auch für Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu befürchten, dass die deutschen Dienstleistungsunternehmen in diesen Sektoren insbesondere gegenüber der US-amerikanischen Konkurrenz ins Hintertreffen geraten. ...") ) ) ) ) )
 (ML ccn "%(q:Das Patentrecht) hat %(q:sich) von den Vorgaben des Gesetzgebers gelöst.")
 (ML UWn "Unsere Frage ist:  Gibt es für diese %(q:Loslösung des Patentrechts) vernünftige wirtschaftspolitische Gründe?")
 (ML DnW "Diese Frage wurde von %(q:dem Patentrecht) selber nie beantwortet.  Die Loslösung wurde, wie wir unten aufzeigen, von einer Gruppe politisch aktiver Patentrechtler vorangetrieben.  Ein breiter Konsens innerhalb der beteiligten Kreise erübrigte in den Jahren 1991-97 die wirtschaftspolitischen Untersuchungen, die wir heute anstoßen wollen:")
 (ML WSg "Wie funktioniert der Softwaremarkt?  Was für Entwicklungen sollte die Wirtschaftspolitik dabei fördern?  Funktioniert das Patentwesen heute bestimmungsgemäß?  Wo wirken Softwarepatente eher aufbauend, wo eher zerstörend?  Welche Handlungsoptionen hat die Bundesregierung?")
 (ML UWe "Unsere vorläufigen Untersuchungen dieser Fragen führen uns zu dem Schluss, dass sich die %(q:Loslösung des Patentrechts vom verarbeitenden Gewerbe) wirtschaftspolitisch nicht wünschenswert sein kann, und dass sie überdies gesetzeswidrig ist und zu einer selbstverschuldeten Begriffsverwirrung in der Patentrechtsprechung geführt hat.")
 (ML Ief "Wir bitten daher die Bundesregierung, die Rechtslage zu entwirren und die von den Patentämtern angestrebten Gesetzesänderungen abzusagen.  Ferner schlagen wir geeignete Maßnahmen vor, um einige vernachlässigte Rechtsgüter vor Übergriffen durch das Patentwesen zu schützen, um die Patentprüfungspraxis zu verbessern, und um europäische Firmen für Rechtsstreitigkeiten in überseeischen Patentinflationsländern zu rüsten.")
 (indexmenu) )

(mlhtdoc 'swpbplibr 
 (ML Wir "Wirtschaftspolitische Bedeutung der Freien Software" "Significance of Free Software for Economic Policy") 
 (ML Eht "Erstmals fragt eine europäische Regierungsorganisation öffentlich nach den Auswirkungen geplanter Gesetzesänderungen im Patentwesen auf die Informationstechnik, die IT-Wirtschaft und das Gemeinwohl.  Der FFII möchte dazu beitragen, dass diese überfälligen Fragen mit dem gebührenden Ernst studiert werden, bevor man einschlägige Gesetze und Verträge ändert." "For the first time a European government body is inquiring into the effects of planned patent law changes on information technology, IT economy and public interest.  The FFII wants to contribute to a thoughrough investigation of these questions, which should precede any changes of law.  Much of this paper remains untranslated.")
 nil
   (sects
    (open 
     (ML Avr "Ausgleich von Unzulänglichkeiten des proprietären Modells" "Making up for Insufficiencies of the Proprietary Mode of Production")
     (ML Uie "Unter %(q:Freie Software) versteht man %(q:vollwertige Logikalien), d.h. logische Komponenten eines Computersystems, die nicht nur funktionieren, sondern auch überprüft, verbessert, angepasst und erweitert werden können, kurzum alle nützlichen Merkmale aufweisen, die man von einer Logikalie erwartet.  Im Gegensatz zur %(q:Freien Software) ist die %(q:proprietäre) Software in ihrer Nützlichkeit kompromittiert.  Um der besseren Vermarktbarkeit willen hat ihr Urheber sich die Logikalie zu einer virtuellen Physikalie herabgestuft, die, ähnlich wie eine Maschine für Außenstehende uneinsehbar ist und nur in einer vom Hersteller vorgegebenen Weise funktioniert.")
     (ML JWj "Jede Wirtschaftspolitik muss sich fragen, was sie eigentlich fördern will.  Sollen möglichst viele vollwertige Logikalien entstehen?  Oder möglichst viele vermarktbare Waren (virtuellen Physikalien)?  Oder vielleicht eine Mischung aus beiden: ein System, was den allmählichen Übergang von virtuellen Physikalien in vollwertige Logikalien fördert?")
     (ML Eie "Es hat sich gezeigt, dass im Bereich des %(q:Kathedralenbaus), d.h. der Programmierung von komplexen Einzelsystemen, die nur wenige Schnittstellen nach außen aufweisen müssen (z.B. Branchensoftware, Spracherkennung), virtuelle Physikalien (proprietäre Software) gute Arbeit leisten und aufgrund ihrer guten Vermarktbarkeit relativ nachhaltig weiterentwickelt werden.  Die vollwertigen Logikalien (freie Software) hingegen weisen insbesondere im Bereich der Kommunikationsnetze und der modularen Systeme überlegene Qualitäten auf und werden dort von den interessierten Fachkreisen weiterentwickelt.")
     (ML Ate "Allerdings lässt sich das Territorium des kommerziellen Kathedralenbaus schwer von dem der vollwertigen Logikalien abgrenzen.  Alle informatischen Aufgabenstellungen neigen dazu, sich im Laufe der Zeit zunehmend in Module und Kommunikationsschnittstellen zu unterteilen.  So ist es erklären, dass ein Gebiet nach dem anderen von vollwertigen Logikalien (freier Software) erschlossen wurde.  Oft spielt der proprietäre %(q:Kathedralenbau) lediglich den Vorreiter in einer Anfangszeit, in der einem hohen kommerziellen Anreiz eine geringer technologischer Entwicklungsstand gegenübersteht.")
     (ML Fer "Es hat sich gezeigt, dass die Wertschöpfung bei den virtuellen Physikalien nicht nach echten marktwirtschaftlichen Spielregeln funktioniert.  Was manche die %(q:Neue Ökonomie) nennen, könnte man auch als %(q:virtuellen Marktwirtschaft) beschreiben.  Es wird nämlich nicht echte physikalische Ware gegen Geld getauscht, sondern es wird mit der Herrschaft über Schnittstellen und Kundenbindungen gehandelt.  Wenn z.B.  Siemens ein Abkommen mit Microsoft schließt, bindet Siemens damit indirekt sehr viele unbeteiligte Dritte in eine stärkere Abhängigkeit der Microsoft-Platform.  Im Gegenzug erhält Siemens billige Lizenzen und Dienste von Microsoft.  Auf seine Kosten kommt Microsoft wiederum bei den unbeteiligten Dritten.  In der IT-Welt spricht man bei solchen Verträgen daher auch nicht von Käufen sondern von %(q:strategischen Allianzen).")
     (ML OsW "Ähnlich virtuell sind nicht nur die Tauschgeschäfte zwischen großen Spielern wie Siemens und Microsoft, sondern auch die Beziehungen zwischen kleinsten Marktteilnehmern.  Banken verheimlichen ihren Kunden die Tatsache, dass sie den HBCI-Standard unterstützen, und empfehlen stattdessen eine kundenbindende inkompatible Lösung.  Hardware-Hersteller verraten niemandem, dass ihr System eine offene Schnittstelle hat und auch unter Linux läuft.  Standardisierungsbemühungen liegen ebensowenig wie Vollwertigkeit im Interesse des %(q:Kommerzes).  Alles, was eine virtuelle Physikalie in Richtung auf eine Vollwertigkeit / Logikalität weiterentwickeln könnte, mindert die Herrschaft über Schnittstellen und Kundenbindungen und wird daher so lange hinausgezögert, bis die technologische Entwicklung durch nichts mehr aufzuhalten ist.")
     (ML Deo "Der zentrale Tauschgegenstand in der %(q:virtuellen Marktwirtschaft) ist nicht das %(q:Produkt) (Software-Glanzkarton), sondern die Verfügungsgewalt über Infrastrukturen, die von unbeteiligter Dritten um der Kompatibilität willen verwendet werden müssen.  Aus dieser Macht wird die Wertschöpfung erzeugt.  Gleichzeitig läuft diese Macht aber den Interessen der Technologie und ihrer Anwender zuwider.  Für Logikalien gilt, etwas überspitzt gesagt: Was technologisch gesehen %(q:vollwertig) ist, ist wirtschaftlich gesehen %(q:wertlos) -- und umgekehrt.")
     (ML Ges "Technologie und Wertschöpfung stehen in einem spannungsreichen aber nicht unversöhnlichen Verhältnis zueinander.   Unter dem Leidensdruck des sprichwörtlichen %(q:Kommerzes) entstand die Freie Software.  Sie wies einen Ausweg, der schließlich auch große Teile der vom %(q:Kommerz)-Kartell ins Abseits gedrängten Geschäftswelt faszinierte.  Zahlreiche Ansätze zum besseren Zusammenleben von IT und Kommerz sind entstanden.  Die Wirtschaftspolitik ist nun gefordert, ein übriges dazu zu tun, dass unproduktive Gegensätze überbrückt und langfristig immer mehr möglichst vollwertige Logikalien entwickelt werden können.")
     (linul
      (ML LkK "Lektüre")
      (ahv 'why-free (colons (ML Rra "Richard Stallman") (ML WWe "Why Free?")))
      (ahv 'opensource "OpenSource.ORG") ) )
    (seku 
     (ML Znf "Zugänglichkeit von Infrastrukturen und öffentliche Sicherheit" "Accessibility of Infrastructure and Public Security")
     (ML Bss "Bei jedem öffentlich ausgeschriebenen Grossprojekt, etwa beim Kraftwerks- oder Straßenbau müssen neben dem fertigen Produkt dem Auftraggeber auch umfangreiche Dokumentation zur Wartung des Produkts mitgeliefert werden. Diese umfasst nicht nur Bedienungsanleitungen, sondern auch Baupläne und Konstruktionen des Projektes. Allgemein werden die Offenlegungs- und Dokumentationsanforderungen mit wachsender Komplexität eines Projektes größer. Sofern in diesen Projekten sicherheitsrelevante Software eingesetzt wird, muss auch deren Qualität sichergestellt werden.") ;; [Beispiele ?] 
     (ML Aee "Auf der anderen Seite waren Sicherheitsanforderungen an den Personalcomputer, wie er seit Ende der 70er Jahre auf den Markt kam, sehr gering, da der Schaden bei einem Ausfall (etwa bei einem Privatanwender) überschaubar war. Neben Weiterentwickung der Hardware verwandelte schliesslich die Netzwerkrevolution (HTML seit 1992) die ursprünglichen Spielekonsolen in Workstations, die aus fast keinem Betrieb mehr wegzudenken sind - aus dem Gameboy wurde auf einmal Infrastruktur.")
     (ML WnW "Wenn aber statt Pingpongbällen private und Behördendaten, diplomatische Noten, Geschäftsverträge und vielleicht bald Volksabstimmungen und Wahlstimmen ausgetauscht werden, verursachen schon kleine Sicherheitslöcher hohe Kosten und selbst Virenwarnungen (Melissa, I love you) machen Schlagzeilen.")
     (ML Arh "Auch quellenoffene Software ist nicht frei von Fehlern und Sicherheitslücken, hier haben aber alle Benutzer im Prinzip die gleichen Chancen, diese zu finden und sind dabei nicht dem Wohlwollen eines Herstellers (oder eines mit diesem paktierenden Geheimdienstes) ausgeliefert.") 
)
    (okup 
     (ML Ddf "Direkte und indirekte Wirkungen auf Bildung und Beschäftigung" "Direct and Indirect Effects on Education and Employment") 
     (ML Fre "Freie Software ermöglicht informationstechnisch interessierten jungen Leuten einen direkten Zugang zu den Quellen erstklassiger Programmierkenntnisse.  Ihre ungehinderte Entfaltung trägt viel mehr zu Bildung, Wissenschaft und Wohlstand bei als durch sie an direkter privatwirtschaftlicher Wertschöpfung entsteht.  Es wäre eine interessante Aufgabe, diesen Beitrag hochzurechnen.")
     ) ) )

(mlhtdoc 'swpbpeude (ML Rak "Rechtslage und aktuelle Entwicklung in DE un EU" "Situation of Patent Law and Current Developments in DE and EU")
 (ML Eht "Erstmals fragt eine europäische Regierungsorganisation öffentlich nach den Auswirkungen geplanter Gesetzesänderungen im Patentwesen auf die Informationstechnik, die IT-Wirtschaft und das Gemeinwohl.  Der FFII möchte dazu beitragen, dass diese überfälligen Fragen mit dem gebührenden Ernst studiert werden, bevor man einschlägige Gesetze und Verträge ändert." "For the first time a European government body is inquiring into the effects of planned patent law changes on information technology, IT economy and public interest.  The FFII wants to contribute to a thoughrough investigation of these questions, which should precede any changes of law.  Much of this paper remains untranslated.")
 nil
 (sects
  (patw 
   (ML Wid "Sinn des Patentwesens")
   (ML DEe "Mit der Erteilung eines Patents gewährt der Staat einem Erfinder ein zeitlich befristetes Monopol auf die gewerbliche Anwendung einer technischen Erfindung.")
   (ML Zek "Ein Patent ist ein Tauschgeschäft zwischen einem Erfinder und der Öffentlichkeit.  Der Erfinder erhält ein befristetes Verwertungsmonopol und stellt im Gegenzug der Öffentlichkeit (vertreten durch das Patentamt) alle damit zusammenhängenden Informationen zur Verfügung, die sonst vielleicht dem Betriebsgeheimnis anheimgefallen wären.  Das beschleunigt die Verbreitung von Wissen und technischem Fortschritt.")
   (ML Prn "Patente bieten eine Möglichkeit der indirekten Forschungsförderung.  Sie erlauben es, ohne staatliche Planung Gelder in Forschungsprojekte zu lenken, und sie bieten einen starken Anreiz für die Veröffentlichung der Forschungsergebnisse.  Hierin liegt ihre wirtschaftspolitische Bedeutung.")
   (ML Pnw "Patente gehören zu den gewerblichen Schutzrechten, d.h. der Gruppe von Rechten, die man durch ein Registrierungs- und Prüfungsverfahren erwirbt.  Hieran wird nochmals deutlich, dass es sich nicht um ein bedingungslos zu gewährendes Naturrecht sondern um ein Tauschgeschäft zwischen dem Erfinder und der Öffentlichkeit handelt.")
   (ML Eai "Es ist Aufgabe der staatlichen Wirtschafts- und Technologiepolitik, darüber zu wachen, dass das Tauschgeschäft zu für die Öffentlichkeit vorteilhaften Bedingungen stattfindet.") )
  (stat
   (ML AlS "Rechtliche Situtation der Softwarepatentierung in Europa" "Current Status")
   (ML ntr "Über die Gewährung von Patentenrechten entscheidet das Deutsche Patentamt, das aber durch das Europäische Patetübereinkommen (EPÜ) verpflichtet ist, Vorentscheidungen des Europäischen Patentamtes (EPA) umzusetzen.  Auch das Patentrecht der EPÜ-Vertragsstaaten ist weitgehend vom EPÜ vorgegeben und ist dadurch eng an Entscheidungen der %(e:Europäischen Patentorganisationen) (EPO) gebunden, die wiederum in vielfacher Weise mit der EU-Kommission und dem Europäischen Parlament zusammenarbeiten, obwohl sie nicht ein Organ der EU sondern der EPÜ-Vertragsstaaten sind, zu denen auch Nicht-EU-Mitglieder wie die Schweiz zählen.")
   (ML Oef "Änderungen des EPÜ bedürfen der Zustimmung aller Vertragsstaaten, und theoretisch sind es auch die Vertragsstaaten, die darüber wachen, ob die Patentämter, allen voran das EPA, vertrags- und gesetzestreu im Interesse der Öffentlichkeit agieren.  Diese Überwachungsfunktion ist meist in den Justiz- und Wirtschaftsministerien der Vertragsstaaten beheimatet, wird dort aber nur sehr lasch gehandhabt.")
   (ML Iur "In den letzten Jahren ist es den Patentorganisationen gelungen, das EPÜ in einer Weise auszulegen, die es den Patentämtern erlaubt, viel mehr Patente zu erteilen, als ein aufmerksamer Leser des EPÜ (und des PatG sowie der Prüfungsrichtlinien u.v.m.) vermuten würde.")
   (ML Ith "In EPÜ §52 wird festgeschrieben, für welche Leistungen Patentschutz gewährt wird ist und für welche nicht.  Die dort festgeschriebene %(q:Ausnahmenliste) findet sich wortgetreu im Deutschen Patentgesetz PatG §1 ebenso wie in anderen nationalen Patentgesetzen wieder.")
   (ML Ari "Auf der europäischen Patentierungs-Ausnahmenliste in Art 52(2) findet sich Punkt 3 %(q:Programme für Datenverarbeitungsanlagen).  In einer Zusatzbestimmung (3) wird ausgeführt, dass die Ausnahmen nur für den jeweils ausgenommenen Gegenstand %(q:als solchen) gelten.") 
   ) 
  (soph 
   (ML PWz "Patentjustiz in Widersprüche verstrickt")
   (ML Due "Dennoch gewähren die europäischen Patentämter Patente auf reine Computerprogramme - nicht etwa nur auf computerprogramm-gestützte Physikalien.  Die Patentanwälte Dr. Alexander Esslinger und Jürgen Betten irren gewiss nicht, wenn sie in der Zeitschrift %(q:Computerrecht) vom Januar 2000 S.22 diesen Umstand politisch erklären:")
   (blockquote 
     (ML Dtn "Der Ausschluss von %(q:Computerprogrammen als solchen) vom Patentschutz in Art. 52 EPÜ (§1 PatG) wird seit langem als rechtspolitische Fehlentscheidung angesehen, zumal der Ausschluss von breiten Verkehrskreisen - bis heute - missverstanden und meist als Ausschluss von Computerprogrammen allgemein verstanden wird.")
     (filters ((un ant euip-union)) (ML lgs "... den Round Table der %(un:UNION) am 9./10.12.1997, als im Europäischen Patentamt 100 Fachleute aus zwanzig europäischen Ländern über die Zukunft des Patentschutzes von Software in Europa diskutierten und zu einem ähnlichen Eindruck kamen wie die AIPPI.  Zudem wurde darauf hingewiesen, dass das Konzept des EPA zum %(q:technichen Charakter) weder von den Patentanmeldern noch von den nationalen Patentämtern richtig verstanden würde.  Viele Teilnehmer machten klar, dass eigentlich alle Computerprogramme dem Wesen nach %(q:technischen Charakter) aufweisen würden.  Seit dieser Zeit wird praktisch %(q:auf allen Kanälen) daran gearbeitet, einen Weg zu finden, wie der irreführende Ausschluss von %(q:Computerprogrammen als solchen) aus dem europäischen Patentgesetz entfernt werden kann, wobei konsequenterweise auch die anderen Ausnahmeregeleungen in Art. 52 Abs. 2 EPÜ (§1 PatG) zur Disposition stehen."))
     "..."
     (ML N1l "Nachdem nun auch die Regierungskonferenz der Mitgliedstaaten der Europäischen Patentorganisation im Juni 1999 in Paris dem EPA das Mandat erteilt hat, vor dem 1.1.2001 eine revidierte Fassung von Art. 52 Abs. 2 EPÜ bezüglich des Ausschlusses von Computerprogrammen vorzulegen, so dass die geänderte Fassung vor dem 1.7.2000 in Kraft tritt, ist es wohl nur eine Frage der Zeit, bis die Computerprogramme (und auch die anderen Ausschlussregelungen) aus Art. 52 EPÜ gestrichen sind.")
     (ML Dgi "Daneben bemüht sich die Rechtsprechung - wie ausgeführt -, die derzeitige Gesetzesregelung so eng auszulegen, dass praktisch alle Computerprogramme - bei entsprechender Anspruchsformulierung - technischen Charakter besitzen und patentfähig sind, wenn sie neu und erfinderisch sind.") 
     (ML Dun "Da die Prüfung einer Patentanmeldung mindestens zwei bis drei Jahre dauert, zwingt diese Entwicklung bereits heute alle Berater dazu, ihre Mandanten darauf hinzuweisen, dass grundsätzlich alle Computerprogramme patentfähig sind und der Patentschutz - nicht nur für Computerprogramme sondern alle Innovationen im Internet genutzt werden kann.")
     ) 
   (ML ZWW "Folgende Tabelle soll aufzeigen, in welche Argumentationsnöte sich das EPA im Verlaufe der oben zitierten %(q:Arbeiten auf allen Kanälen) hineinmanövriert hat.")
   (table '((border t)) '((beh (width 30)) (wid (width 70)))
	  (l (l (ML Bat "EPA-Behauptung") (ML Wee "Widerlegung")))
	  (l
	   (l (nl (ML deh "Das EPÜ enthalte viele Widersprüche.") (ML dem "der Gesetzgeber habe nicht definiert, was unter %(q:Computerprogramm als solches) zu verstehen sei.")) (filters ((li ant (bridi 'cf (commas (ahs 'lamy-droit-ir) (bridi 'pag (bridi 'ff 177))))) (gt ant (ahv 'heise-patentfluten)) (cs pet (bridi 'cf (ML Bhc "BGH-Urteil Chinesische Schriftzeichen")))) (ML DgW "Mit dem Wörtchen %(q:als solches) wird das Computerprogramm von einer auf Computerprogrammen beruhenden physikalischen Struktur (z.B. elektronische Schreibmaschine) abgegrenzt.  D.h. der Patentinhaber kann einem Wettbewerber die gewerbliche Produktion einer Patent-Schreibmaschine untersagen, nicht aber die Weitergabe eines Computerprogramms, welches den PC zur Patent-Schreibmaschine macht.  So wurde es %(cs:bis Anfang der 90er Jahre) gehandhabt.  Die Absicht des Gesetzgebers ist auch nach %(li:Meinung von Patentjuristen) klar verständlich.  Das EPA und der BGH wollten jedoch aus wirtschaftspoligischen Erwägungen heraus die Regeln ändern.  Ihrer Meinung nach ist die %(q:Softwareindustrie) inzwischen so bedeutend geworden, dass ihr die Segnungen des Patentwesens nicht länger versagt bleiben dürfen.")))
	   (l (ML dbW "Die Patentierbarkeits-Ausnahmenliste leite sich aus einem einzigen übergeordneten Grundgedanken her, wonach eine Erfindung %(q:technisch) zu sein habe.")  (ML DWd "Dieser Behauptung fehlt zunächst jede Begründung.  Der Gesetzgeber hat durch nichts zu erkennen gegeben, dass der Ausnahmenliste der Gedanke der Technizität zugrunde liegt.  Es handelt sich hier um eine vom EPA gewählte rechtsprechungs-interne Hilfstheorie.  Diese Theorie ist nicht sehr gut, denn bei der %(q:Technizität) handelt es sich um einen beliebig dehnbaren Rechtsbegriff.  Dehnbare Begriffe können jedoch durchaus zur Interpretation von Gesetzen taugen, solange das Ergebnis mit den Intentionen des Gesetzgebers übereinstimmt, d.h. solange die Ausnahmenliste sich tatsächlich aus der gewählten Hilfstheorie herleiten lässt.  Heute ist aber das Gegenteil der Fall. Die Definition der %(q:Technizität) wird zudem von Jahr zu Jahr in Richtung %(q:Nützlichkeit) aufgeweicht.  Zunächst wurde die %(q:Kerntheorie) fallen gelassen, wonach der erfinderische Kern %(q:technisch) sein musste, d.h. auf dem Gebiet der Anwendung von Naturkräften und nicht auf dem Gebiet der der Programmlogik liegen musste.  1999 beschloss der BGH in radikaler Abkehr von bisherigen Prinzipen: %(q:Der technische Charakter einer Lehre wird nicht dadurch fraglich, daß sie von einem üblichen Rechner nur den bestimmungsgemäßen Gebrauch macht)."))
	   (l (ML urt "Unter %(q:Programm als solches) sei nur ein %(q:Programm, soweit es nicht technisch ist), zu verstehen.") (let ((L (ahv 'lamy-droit-ir))) (ML Dne "Dies ist eine aus der Luft gegriffene Fehlinterpretation des Gesetzestextes.  Mit %(q:Programm als solches) ist die %(q:von einer patentierbaren physikalischen Gesamtstruktur losgelöste Programmlogik) gemeint.  M.a.W.:  eine Industriemaschinerie ist auch dann patentierbar, wenn darin Computerprogramme zum Einsatz kommen.  Mit einem solchen %(e:Patent auf einen programmbasierten Industrieprozess) kann man aber niemanden daran hindern, das Programm selber weiterzugeben und weiterzuentwickeln.  Man kann lediglich die Vermarktung der gesamten (Hard- und Software enthaltenden) Computersystems beschränken.  Das ist eine klare und höchst sinnvolle Trennung, die auch so vom Gesetzgeber intendiert war (s. %{L} p.177 ff).")))
	   (l (ML ulx "Unter %(q:gewerbliche Anwendung) (industrial application / application industrielle) falle %(q:alles, was man kommerziell nutzen kann).") (ML Tvi "Traditionell hängt die Bedeutung von Gewerblichkeit/Industrialität jedoch eng mit der Bedeutung von %(q:Technizität) zusammen:  es geht dabei um die Produktion von Gütern %(q:unter Anwendung von Naturgesetzen und ohne zwischengeschaltete menschliche Tätigkeit) (BGH-Definition).  D.h. eine Arztpraxis ist kein %(q:Gewerbe) im Sinne des Patentrechts (ebenso wie des Steuerrechts), weil die dortigen Heilverfahren eine menschliche Tätigkeit beinhalten.  Gewerblich / industriell ist jedoch die Herstellung von Arzneimitteln und sonstigen Physikalien.  Auch virtuelle Physikalien (proprietäre Software) werden in gewerblicher Manier vermarktet.  Vollwertige Logikalien hingegen verbreitet sich hingegen ohne jeden gewerblichen Apparat.  D.h. Gewerblichkeit entspringt bei Logikalien nicht technologischen Erfordernissen.  Auch Liebe nennt man %(q:das älteste Gewerbe der Welt), aber ob man jegliche menschlichen Liebe per Gesetz den Regeln der Gewerblichkeit unterwerfen will, ist eine politische Entscheidung.") )
	   (l (ML dgI "Das Softwarepatentierungsverbot des EPÜ widerspreche dem TRIPS-Vertrag.") (ML TiW "TRIPS ist ein Vertrag zwischen Staaten, der nationale Patentsysteme im Hinblick auf den Freihandel %(q:harmonisieren), d.h. auf einen relativ abstrakten gemeinsamen Nenner bringen soll.  TRIPS §27 verlangt, dass %(e:auf allen Gebieten der Technik Patente erhältlich sein) sollen, sofern die jeweilige Erfindung neu, erfinderisch und industriell anwendbar ist.  TRIPS legt aber nicht fest, wie Technizität und Industrialität / Gewerblichkeit zu definieren sind.  Es wird noch nicht einmal ausgeschlossen, dass über Technizität und Industrialität hinaus weitere Ausschlusskriterien (z.B. %(q:nur Erfindungen aber nicht Entdeckungen)) aufgestellt werden.  Es wird lediglich gefordert, dass der jeweilige Unterzeichnerstaat seine Kriterien klar definieren und konsequent (d.h. ohne freihandelsfeindliche Ad-Hoc-Ausnahmen für bestimmte Branchen) anwenden soll.  Die Computerprogramm-Ausnahme des EPÜ ist ebenso branchenneutral wie die Ausnehmung von Entdeckungen, Spielen und mathematischen Theorien.  Es werden hier nicht bestimmte Branchen sondern bestimmte Typen und Ausformungen geistiger Leistungen ausgeklammert.  TRIPS ordnet übrigens in §10 Computerprogramme ausdrücklich dem Urheberrecht zu."))
	   ) )
   (ML Eei "Eigentlich ist es sehr leicht, die europäische Patentierbarkeits-Ausnahmenliste sinnvoll und widerspruchsfrei im Sinne der einschlägigen Gesetze zu interpretieren:")
   (blockquote (ML PtW "Patentierbar sind nicht informationelle Gegenstände (Idee, Information, Logikalie, Gleichung, Algorithmus, Software), sondern deren gewerbliche Umsetzung in die automatisierte Produktion materieller Güter.") )
   (ML Dcw "Demnach könnte man aufgrund von Patentansprüchen den gewerblich organisierten Verkauf von kompletten Rechensystemen aus Hardware und Software (z.B. MP3-Abspielgeräten oder Rechnern mit vorinstallierter Software) untersagen, nicht jedoch die Weitergabe und Anwendung der Software auf beliebigen anderen Rechnern.  Dies entspricht nicht nur dem Buchstaben und Geist des EPÜ sondern auch dem Buchstaben und Geist des TRIPS-Vertrages.")
   (ML DiW "Das EPA hat sich hingegen nach und nach in eine verworrene, selbst für Patentfachleute unverständliche Rechtslage verstrickt.  Dies konnte deshalb unkritisiert durchgehen, weil unter den diskutierenden Patentjuristen ein bestimmter wirtschaftspolitischer Konsens herrschte.   Dieser Konsens stützt sich jedoch weder auf den Willen des Gesetzgebers noch auf eine sorgfältige wirtschaftspolitische Argumentation.") )
  (diss 
   (ML Urh "Konflikte mit anderen Rechtsgütern" "Legal Inconsistencies, Conflicts with other Legal Goods")
   (sects
    (inter 
     (ML Ilh "EU-Interoperabilitätsrichtlinie vs Eigentum an Schnittstellen" "EU Interoperability Directive vs Property on Interfaces " "Interopérabilité et interfaces")
     (filters ((di ahv 'eu-interoper-1991)) (ML Spi "Softwarepatente bedrohen das Prinzip der Interoperabilität, ein Grundprinzip des Wettbewerbsrechtes, das in der %(di:EU Software-Direktive von 1991) eine besondere, weltweit viel beachtete und viel nachgeahmte Ausformung fand." "Software patents jeopardise the principle of interoperability, as laid down in the EU Software Directive of 1991." "Les brevets sur les logiciels menacent le principe d'interopérabilité de la directive de 1991 sur le logiciel. Ce principe est une principe fondamental de droit de la concurrence. Il s'agit probablement de la disposition la plus forte et la plus judicieuse de la directive de 1991. Cette disposition a inspiré de nombreux autres pays dans le monde et a souvent été copiée."))
     (ML Dfn "Dort ist ausdrücklich zu lesen, dass die Nachprogrammierung von Schnittstellen deshalb nicht untersagt werden könne, %(s:weil nur die konkrete Ausformung eines Computerprogramms, nicht aber die zugrundeliegenden Ideen als Eigentum beansprucht werden können).  Sinn der Patentierung ist es aber gerade, zugrundeliegende Ideen in Besitz zu nehmen, um so Interoperabilitätsbeschränkungen, die in der Ökonomie der virtuellen Physikalien (proprietären Software) den wichtigsten Tauschgegenstand darstellen (s.o.), zu ermöglichen.")  
     (filters ((ol ahv 'osslwaw-articles)) (ML Ufe "Um hier jeglichem Missverständnis zuvorzukommen formuliert ein %(ol:aktueller französischer Gesetzesentwurf) ein %(q:Recht auf Kompatiblität)." "In order to remove all possible ambiguity, a %(ol:current french law proposal) is introducing a %(q:right to compatibility)." "Afin de lever toute ambiguité, une %(ol:proposition de loi française) introduit un principe de %(q:droit à la compatibilité):"))
     (blockquote (ML Trl "Jede natürliche oder juristische Person hat das Recht, eine eigene Logikalie (Software) zu veröffentlichen und zu verwenden, die mit den Kommunikationsnormen einer anderen Logikalie kompatibel ist." "Every natural or legal person has the right to publish and use a piece of software that is compatible to the communication standards of another software." "Toute personne physique ou morale a le droit de développer, de publier et d'utiliser un logiciel original compatible avec les standards de communication d'un autre logiciel."))
     (ML Aeg "Auf diese Weise wird klargestellt, dass ein Prinzip des Wettbewerbsrechtes (freier Zugang zum Markt / zur informationellen Infrastruktur) den Vorrang vor bestehenden und geplanten Eigentumsrechten erhält." "This way it is made clear that a competition right (right to compatibility) will prevail over existing and future property rights." "Il est ainsi proposé de rendre un droit de concurrence (le droit à la compatibilité) prépondérant sur tout droit de propriété existant (marques) ou éventuel (brevets).")
     (ML DSe "Dieses Problem könnte auch mit dem klassischen Mittel einer Zwangslizenz gelöst werden, wie sie u.a. bei Arzneimitteln im Namen hoher öffentlicher Güter (Seuchenbekämpfung) bisweilen verhängt wird.  Von solchen Zwangslizenzen wären dann allerdings alle interessanten Softwarepatente mehr oder weniger betroffen, denn im Bereich der Informationssysteme stellt sich die Frage der Interoperabilität / Kompatibilität bei jeder erfolgreichen Technik früher oder später.  Was als kommerziell programmiertes Kathedralensystem begann, differenziert sich früher oder später in Module mit vielen Schnittstellen (s.o.).  Spätestens ab diesem Zeitpunkt müsste eine Zwangslizenz verhängt werden."  "This problem could be solved using the traditional means of a compulsory license.  Such licensing would however sooner or later affect all interesting software patents, because in the domain of information systems the question of interoperability / compatibility will occur sooner or later." "On pourrait utiliser le système de licences obligatoires, mais ca affecterait presque tous les brevets de logiciels, car dans le domaine informatique tout système créera des problèmes de interopérabilité des qu'il suscite un intérêt général.") )
    (dist 
     (ML Wzr "Wettbewerbsverzerrung zugunsten schwerfälliger Vermarktungsverfahren" "Distortion of Competition in favor of traditional modes of distribution" "Distorsions de concurrence sur la distribution de logiciel")
     (ML Sll "Sobald ein Patent auf %(q:ein Computerprogramm, dadurch gekennzeichnet, dass ...) erteilt wird, riskiert man nunmehr durch bloße %(e:Weitergabe eines Programms) (und nicht etwa nur durch die %(e:Ausführung eines Rechenverfahrens)) das Patent zu verletzen.  Das verursacht ernsthafte Probleme für neuere Distributionsmodelle wie etwa Shareware, Freeware und Freie Software." "" "Si ce sont les programmes (et non les méthodes informatiques) qui sont brevetés, alors la copie (et non l'usage) d'un programme constitue une contrefaçon. Ceci pose de très sérieux problèmes à l'industrie du partagiciel, du gratuiciel et du logiciel libre.")
     (ML Stu "Shareware ist in Europa sehr stark entwickelt und hat große kommerzielle Erfolge ermöglicht wie z.B. die deutsche Firma GoLive, die später in Adobe aufging.  Dieses Vertriebsmodell ist dadurch gekennzeichnet, dass %(ul:das Recht, ein Programm zu kopieren und:das Recht, dieses Programm einzusetzen) voneinander getrennt sind." "Shareware, a commercially successful concept highly developped in Europe (e.g. GoLive Cyber Studio, later bought by Adobe) is characterised by a separation of %(ul:the right to copy and:the right to use) a piece of software." "Le partagiciel, très développé en Europe et qui a permis de grands succès industriels (ex. Golive, racheté par Adobe) constitue à %(ul:donner le droit de copier un logiciel librement:demander le paiement d'un droit d'usage).")
     (ML Dot "Daher wird ein Shareware-Programm viel häufiger kopiert als tatsächlich verwendet." "" "Ainsi, un partagiciel est copié bien plus souvent qu'il n'est réellement utilisé.")
     (ML Jai "Jede unbenutzte Kopie eines solchen Programms könnte Patentansprüchen unterliegen (in den USA wurde hochgerechnet, dass ein Programm durchschnittlich 8 Patente verletzt).  Bei herkömmlicher kommerzieller Software hingegen würden nur tatsächlich benutzte (weil gekaufte) Kopien unter den Patentanspruch fallen.  D.h. die %(q:entgangenen Gewinne), auf die ein Patentinhaber einen Softwareautor verklagen könnte, wären bei frei kopierbaren Programmen wesentlich höher als bei kopierbeschränkter Software, und der Patentinhaber hätte grundsätzlich ein Interesse daran, seine %(q:Erfindung) an Firmen zu lizenzieren, die mit restriktiven Distributionsverfahren arbeiten.  Eine Distribution unter einer --- aus technischer Sicht oft unbedingt wünschenswerten --- freien Lizenz wie der GPL käme niemals in Frage, da wenigstens die Patentierungskosten eingefahren werden müssen.  Es handelt sich insofern bei Softwarepatenten um einen wettbewerbsverzerrenden staatlichen Eingrif zugunsten veralteter, technisch unzweckmäßiger Vermarktungsverfahren." "" "Or, chaque copie non utilisée d'un shareware constituerait une contrefaçon de brevet (tous les programmes aux Etats-Unis contiennent statistiquement aujourd'hui des contrefaçons de brevets).  Inversement, seules les copies vendues (et utilisées) des logiciels traditionnels constitueraient des contrefaçons de brevets.  Il y aurait donc beaucoup plus de contrefaçons de brevets dans le shareware, le freeware ou le logiciel libre que dans le logiciel traditionnel, simplement parce que le mode de distribution et de commercialisation du shareware, du freeware et du logiciel libre est fondé sur la copie. Les contentieux liés au système de brevet coûteraient donc plus cher au shareware, au freeware et au logiciel libre qu'au logiciel traditionnel ce que l'on peut considérer comme une distorsion de concurrence en faveur d'un modèle de distribution contre un autre.")
     (filters ((ur ahv 'useright-pdf)) (ML Eac "Es sind bereits %(ur:Lösungen) vorgeschlagen worden, durch die man im allerseitigen Interesse diesen Widerspruch auflösen könnte.  Grundlage dieser Lösungen ist eine saubere Trennung zwischen Urheber- und Patentrecht: das Urheberrecht gibt demnach dem Autor die Kontrolle über die Weiterverbreitung seines Werkes, während das Patentrecht dem Erfinder die Kontrolle nicht über die Verbreitung von Information (denn nichts anderes ist Software) sondern über die %(e:gewerbliche Verwertung) derselben gibt." "" "Des %(ur:solutions réalistes) ont été proposées. Ces solutions sont fondées sur un découplage entre le marché des droits de copie et le marché des droit d'usage de logiciels. Ces solutions, qui auraient le mérite de placer l'Europe en position de moteur et non de suiveur, sont l'équivalent dans le logiciel des découplages qui existent dans le marché de l'électricité (production, distribution), des transports ferroviaires (infrastructure, matériel, opérateur de ligne, opérateur de gare), etc.")) )
    (komc 
     (ML Keo "Besonderer Konzentrationseffekt bei komplexen Systemen" "Special Concentration Effect in case of Complex Systems" "Phénomènes de concentration et systèmes complexes")
     (ML DaW "Die meisten Programme bestehen aus Tausenden von möglicherweise patentierten oder patentierbaren Bausteinen.  Die Patentinhaber eines Bausteins können ihre Rechte nur verwerten, indem sie sich in einen schwerfälligen monopolistischen Apparat einfügen." "" "Il s'agit d'une propriété générale des brevets sur les systèmes complexes. Les inventeurs d'un sous-élément d'un système complexe n'ont que rarement la liberté d'exploitation de leur invention car cette invention n'est commercialisable qu'au sein d'un système complexe et que les autres sous-éléments nécessaires à la réalisation de ce système complexe font l'objet de brevets tiers. Les jeux commerciaux et économiques qui résultent de cette situation aboutissent à des phénomènes de concentration et à une forme de pillage des inventions.") 
     (ML DWi "Diese Geflechte wirken sich in der Autoindustrie nicht besonders nachteilhaft aus, weil dort ohnehin hohe Materialinvestitionen anfallen.  Im Bereich der Informationstechnik genügen oft geringe Investitionen, so dass sich oft kleine Hütten-Unternehmen in Kürze zu wichtigen Mitspielern mausern können." "" "Ces jeux n'ont pas été trop sensibles dans les industries traditionnelles de systèmes complexes car l'investissement nécessaire à l'industrialisation est important (ex. automobile). Mais, dans le cas du logiciel, l'investissement lié à l'industrialisation est souvent très faible, ce qui autorise aujourd'hui de toutes petites entreprises à devenir les concurrentes de multinationales.") )
    (sekur 
     (tpe (ML Qvu "Förderung der Geheimniskrämerei vs Sicherheit und Verbraucherschutz" "Promotion of Secrecy vs Security & Consumer Safety") (bridi 'cf (lset ((romv (ML Rer "Römischer Vertrag §%s" "article %s of the Treaty of Rome" "article %s traité de Rome"))) (pf romv 95))))
     (ML Vir "Vor Angriffen durch Softwarepatente schützt man sich am besten, indem man die Baupläne (Quelltexte) seines Programmes versteckt hält.  Um Lizenzen zu erhalten, muss man wiederum die Kontrolle über die Weiterverbreitung seines Programmes maximieren (s.o.).  Beides schließt die Freigabe nach OpenSource-Lizenz aus.  Sicherheitssensible Programme würden auf das bekannte Niveau kommerzieller Software (siehe I-Love-You-Virus, NSA-Hintertüren u.v.m.) herabgedrückt.  Softwarepatentierung forciert somit einen Rückschritt in Sachen Sicherheit und Verbraucherschutz.  Darüber hinaus dürften die wettbewerbsbehindernden Wirkungen der Softwarepatentierung (s.o.) auf eine geringere Wahlfreiheit des Verbrauchers hinwirken.  Gleichzeitig ist mit einer durch Patentverwaltungs- und Prozesskosten bedingten Erhöhung der Verbraucherpreise zu rechnen, zu der die Verschärfung der Monopolsituation ein übriges beitragen wird.  All dies widerspricht den Römischen Verträgen, die vorschreiben, dass neue europäische Gesetze mit den Interessen der öffentlichen Sicherheit und des Verbraucherschutzes im Einklang stehen müssen." "" "outre ses effets anti-concurrentiels (voir ci-dessus) l'introduction de brevets sur les logiciels tend à favoriser le secret du code source (il est plus difficile pour un concurrent de prouver une contrefaçon lorsque le logiciel est en binanire et que sa décompilation est illégale !). Il s'agit donc d'un régression en matière de sécurité (seule la publication du code source garantit l'absence de failles de sécurité) et de protection du consommateur (l'accès au code source permet de vérifier le respect des libertés individuelles et facilite l'interropérabilité). En outre, les effets anti-concurrentiels du brevet pourraient tendre à restreindre le choix du consommateur. Enfin, une augmentation des prix, d'environ 30%, liée au système de brevets est à prévoir, sans qu'il soit prouvé que ce système offre une quelconque utilité.") 
     (ML Mmu "Manche Leute vermuten, das Patentrecht könnte die Offenlegung der Quellen fördern, da nun Programmideen ja besser %(q:geschützt) würden.  Dies wäre zumindest in den seltenen Fällen vorstellbar, wo die Vorenthaltung des Quelltextes bisher wirklich vor allem der Geheimhaltung einer Programmidee (und nicht etwa der Blockierung von Interoperabilität o.ä.) diente.  Aber selbst in diesem eher seltenen Fall würde derjenige, der Quelltext mitliefert oder gar veröffentlicht, sich damit einer erhöhten Gefahr von (potentiell ruinösen) Patentklagen aussetzen. Selbst wenn er den Quelltext veröffentlichen würde, könnte er dadurch noch lange nicht von der weltweiten fachkompetenten Aufmerksamkeit profitieren, die quellenoffener Software zugute zu kommen pflegt.  Denn die meisten Entwickler verbringen ihre kostbare Zeit ungern mit %(q:patent-verseuchten) Quelltexten.")
     (ML Dnt "Die Veröffentlichung des Quelltextes bietet den besten Schutz vor versteckten Hintertüren in sicherheitsrelevanter Software. Daher schränken Software-Patente auch die Möglichkeiten eine nachvollziehbar sicheren Kommunikation ein.")
     (linul 
      (ML Lre "Lektüre hierzug:")
      (ah "http://www.spiegel.de/spiegel/0,1518,14871,00.html" (ML Uth "Ungeniert schnüffeln vor allem die Amerikaner die deutsche Wirtschaft aus: Mit großem Aufwand und High-Tech durchforsten sie Telefonleitungen und Computernetze."))
      (ah "http://www.heise.de/newsticker/data/jk-24.06.99-000/" (ML HKo "Hintertür in LotusNotes"))
      (tpe (ah "http://www.heise.de/newsticker/result.xhtml?url=/newsticker/data/fr-15.11.99-000" (ML HMe "Heise: BMWi fördert GnuPG")) (ML Dwz "Dort laesst sich eine hintertuer halt sehr viel schwerer einbauen als in lotus notes. GnuPG unterstuetzt derzeit kein RSA und IDEA wegen patent-problemen.") ) )
     (when nil (blockquote "La Commission, dans ses propositions prévues au paragraphe 1 en matière de santé, de sécurité, de protection de l'environnement et de protection des consommateurs, prend pour base un niveau de protection élevé en tenant compte notamment de toute nouvelle évolution basée sur des faits scientifiques. Dans le cadre de leurs compétences respectives, le Parlement européen et le Conseil s'efforcent également d'atteindre cet objectif.")) )
    (kultur 
     (tpe (ML Clu "Medienkartelle vs Kulturelle Vielfalt" "Media Cartels vs Cultural Diversity" "Culture") (bridi 'cf (pf romv 151)))
     (ML AWa "Auch das vom Römischen Vertrag in besonderer Weise geschützte Kulturleben wird gefährdet, wenn Kommunikationsnormen und sonstige grundlegende informationelle Infrastrukturen von einem oder wenigen oligopolistischen Konglomeraten kontrolliert werden.  Auch aus diesem Grund ist es wichtig, dem Recht auf Kompatibilität / Interoperabilität (s.o.) Vorrang vor Patentansprüchen einzuräumen." "" "les brevets sur les techniques de diffusion culturelle peuvent conduire à fermer l'accès à la culture en créant un marché contrôlé par les grands éditeurs, marché excluant les petits diffuseurs de contenus en raison de l'existence de brevets sur des technologies de diffusion ou d'archivage (typiquement sur des standards de communication). Ceci serait contraire à la promotion de la diversité culturelle et des échanges culturels non commerciaux ainsi qu'à la préservation du patrimoine. Là encore, le principe de non protection des interfaces prévu par la directive de 1991 sur le logiciel semble le plus judicieux.") 
     (when 
	 nil 
       (blockquote
	(ML LxW "L'action de la Communauté vise à encourager la coopération entre états membres et, si nécessaire, à appuyer et compléter leur action dans les domaines suivants: - l'amélioration de la connaissance et de la diffusion de la culture et de l'histoire des peuples européens, - la conservation et la sauvegarde du patrimoine culturel d'importance européenne, - les échanges culturels non commerciaux, - la création artistique et littéraire, y compris dans le secteur de l'audiovisuel.")
	(ML Lne "La Communauté tient compte des aspects culturels dans son action au titre d'autres dispositions du présent traité, afin notamment de respecter et de promouvoir la diversité de ses cultures.") ) ) ) 
    (indure 
     (tpe (ML IiF "Kartellförderung vs Beschleunigung des Techniktransfers" "Promotion of Oligopolism vs Acceleration of Technology Transfer" "Industrie et recherche")
	  (bridi 'cf (pf romv 157)) )
     (ML Siu "Softwarepatente schränken die Nutzbarmachung von technischen Fortschritten ein.  Kooperative Problemlösungsprozesse, wie sie im akademischen Umfeld und in der Freien Software üblich und sehr erfolgreich sind, werden zerschlagen und kleineren Unternehmen wird durch ein oligopolistisches Geflecht die Möglichkeit zur Nutzung und Weiterentwicklung vorhandener Techniken verbaut." "" "les brevets sur les logiciels menancet la liberté d'exploitation des inventeurs car les logiciels sont des systèmes complexes. L'introduction de brevets sur les logiciels peut être néfaste à l'innovation et à la valorisation de la recherche en informatique, notamment par les PME/PMI.")
     (when nil (ML Cix "" "" "La Communauté et les états membres veillent à ce que les conditions nécessaires à la compétitivité de l'industrie de la Communauté soient assurées. A cette fin, conformément à un systýme de marchés ouverts et concurrentiels, leur action vise à: - accélérer l'adaptation de l'industrie aux changements structurels; - encourager un environnement favorable à l'initiative et au développement des entreprises de l'ensemble de la Communauté, et notamment des petites et moyennes entreprises; - encourager un environnement favorable à la coopération entre entreprises; - favoriser une meilleure exploitation du potentiel industriel des politiques d'innovation, de recherche et de développement technologique."))
     )) 
  (koal (ML KgG "Koalitionsvereinbarungen zwischen SPD und Grünen von 1998-10-20" "German Federal Government Coalition Agreement" "German Coalition Agreement between Social Democrats and Greens")
   (ML HWW "Hierin verpflichtet sich die Regierung darauf, auf eine %(q:beschleunigte Nutzung und Verbreitung der Informationstechnologien in der Gesellschaft) hinzuarbeiten.  Dieser Imperativ ist mit einer Zustimmung zur Softwarepatentierung unvereinbar." "This agreement obliges the government to work for an %(q:accelerated use and popularisation of information technologies in society).  If the government agreed to make software patentable, it would breach this obligation.")) 
)  
  (debr
   (ML Wmd "Was %(s:kann) die Bundesregierung tun?" "What %(s:can) the Federal Government do?")
   (ML AsW "Als Repräsentant des am stärksten von den wirtschaftspolitischen Folgen betroffenen europäischen EPÜ-Vertragsstaates ist die Bundesregierung juristisch und moralisch berechtigt, jeder gegen ihre Interessen gerichteten Änderung des EPÜ ihre Zustimmung zu verweigern und auf eine wirtschaftspolitisch sinnvolle Regelung des Patentwesens zu drängen.")
   ) ) )
 
(mlhtdoc 'swpbpuseu (ML Dns "Wirkung von Softwarepatenten in USA und EU" "The Effects of Software Patents in US and EU as they stand")
 (ML Eht "Erstmals fragt eine europäische Regierungsorganisation öffentlich nach den Auswirkungen geplanter Gesetzesänderungen im Patentwesen auf die Informationstechnik, die IT-Wirtschaft und das Gemeinwohl.  Der FFII möchte dazu beitragen, dass diese überfälligen Fragen mit dem gebührenden Ernst studiert werden, bevor man einschlägige Gesetze und Verträge ändert." "For the first time a European government body is inquiring into the effects of planned patent law changes on information technology, IT economy and public interest.  The FFII wants to contribute to a thoughrough investigation of these questions, which should precede any changes of law.  Much of this paper remains untranslated.")
 nil
 (sects
  (util
   (ML W33 "Wo wirken Softwarepatente aufbauend?" "Whom do swpatents help?  Which economic behaviors do they encourage?")
   (sects
    (grandkomp 
     (ML Fgn "Firmen mit großen Patentportfolios.")
     (ML IeW "In den USA gehören ca 7% der Softwarepatente IBM.  S. Statistiken von Gregory Aharonian.")
     (ML DEh "Die meisten Computerprogramme sind komplexe Systeme, die gleichzeitig von vielen Patenten betroffen sein können.  Es nützt nicht unbedingt viel, wenn man nur ein Patent besitzt.  Meist besteht eine wirksame Absicherung darin, dass man mit anderen Großfirmen Nicht-Angriffs-Pakte (Kreuzlizenzierungs-Abkommen) schließt.")
     (ML Fte "Firmen wie IBM, Sun u.v.m. sind durchaus Leistungsträger, deren Forschungsinvestitionen gelegentlich nicht nur räuberischen Plattformstrategien dienen sondern auch der Öffentlichkeit zugute kommen.  Es wäre interessant, zu untersuchen, in welchem Maße dies durch das Vorhandensein von Patentportfolios gefördert wird.") )
    (novmerk 
     (ML Fce "Firmen, deren Geschäft auf wenigen patentierbaren Ideen für einen neuen Markt beruht")
     (ML UPn "Um sein 1-Click-Patent umzusetzen, muss Amazon kein komplexes Systeme bauen, das viele andere Patente verletzen könnte.  Gleichzeitig ist das 1-Click-Patent unmittelbar mit einer lukrativen Geschäftsidee verbunden.  Es ist ein in Informationstechnik gekleidetes Geschäftsideen-Patent.")
     (ML AWn "Auch ein kleines Unternehmen, das Patente dieser Art besitzt, kann sich damit eine beneidenswerte Stellung sichern.  Insbesondere Unternehmen, die als erste einen werdenden Markt betreten, können durch Patentierung der dabei entstehenden Software-Ideen den Markt gegen Nachzügler absperren.   Ein solches Unternehmen kann sich unter Umständen zu einem hohen Preis von einem Großunternehmen kaufen lassen, das damit sein Patent-Portfolio um einen wertvollen Mosaikstein erweitert.  Ferner hat es gute Chancen, Risikokapital anzuziehen.") )
    (invent 
     (ML Upc "Unternehmen hochspezialisierter technischer Erfinder")
     (ML Der "Die Ansprüche der US-Firma Stac auf eine neue Kompressionstechnik konnten in einem Prozess gegen Microsoft durchgesetzt werden.")
     (ML Eru "Einige echte Erfindungen auf technischem Gebiet wie etwa das MPeg-Verfahren konnten dank Patentrechten besser verwertet werden, und dies gelang sogar in einer fairen Weise, die auch die Interoperabilität von MPeg mit verschiedenen Systemen erlaubte.  Das Patentrecht bietet jedoch bisher keinerlei Schutzmittel gegen eine räuberische Vermarktung solcher Patentrechte.")
     (ML Dil "Diese erfreulichen Fälle sind leider selten, und ihnen könnte mit einem eigens dafür entworfenen Sui-Generis-Rechtsschutz besser geholfen werden als mit dem Patentrecht.  Das Patentrecht weist nämlich zwei Nachteile auf:")
     (dl
      (l (ML uzs "undifferenziert, aussageschwach") (ML Eet "Es wirft die echten Erfinder in einen Topf mit der Masse der Wegelagerer-Patente.  Auch der Kapitalmarkt ist gegenüber der Patentqualität zunächst blind.  Zu den ohnehin hohen Patentkosten (Gebühren für Durchfechtung vor Gericht) kommen also unnötige Werbekosten hinzu."))
      (l (ML frr "festgefahren, unverbesserlich") (ML EWz "Es lässt sich nicht auf die Besonderheiten der einzelnen Schutzgegenstände (z.B. Logikalien vs Physikalien) hin optimieren.  TRIPS schreibt vor, dass %(q:auf allen Gebieten der Technik) genau gleich gearteter Rechtsschutz zu gewähren ist.  Das bedeutet z.B. dass der Patentschutz für durchweg mindestens 17 Jahre gewährt werden muss.")) ) )
    (patenthai 
     (ML Pra "Patentberater, Patentmakler")
     (ML Din "Darin arbeiten z.B. mehrere 10 bis mehrere 100 Patentspezialisten, die Rechte von Kleinfirmen aufkaufen und an diejenigen Großfirmen weitervermitteln, die aufgrund ihres flächendeckenden Patentportfolios in der Lage sind, daraus den größten Nutzen daraus zu ziehen. ")
     (ahv 'techlaw-19991217 (ML Aqn "Andere Berater suchen in Forschungsabteilungen von Firmen nach %(q:verstecktem Gold), d.h. scheinbar trivialen Erfindungen, von denen kein Techniker angenommen hätte, dass man sie patentieren kann.")) )
    (patamt 
     (ML Pet "Patentämter und Patentanwälte")
     (ML WSe "Siehe dazu das eingangs angeführte Zitat aus einem Rundbrief einer im Softwarebereich besonders rührigen Münchener Patentanwaltskanzlei an ihre Mandanten.")
     (ML DtW "Dass diese bahnbrechende Ausdehnung des Patentwesens auch den Patentämtern mehr Geld und Macht bringt, versteht sich von selbst.") ) ) ) 
  (neutil
   (ML Wer "Wo wirken Softwarepatente zerstörerisch?" "Who is hurt by software patents")
   (sects
    (libre 
     (ML Fef "Freie Software, Informationelle Infrastruktur")
     (ML Ero "Es ist nicht möglich, patentierte Verfahren in freier Software zu verwenden.  Patentierte Logikalien sind nicht mehr vollwertig, da nicht mehr einem uneingeschränkten gemeinschaftlichen Verbesserungsprozess unterliegend.  Gleichzeitig ist freie Software besonders verwundbar, da der Quelltext offen liegt und besonders leicht auf Patentverletzungen abgesucht werden kann.")
     (ul (ahv 'truetype-patents (ML Vlt "Vektorfontsdarstellung durch Patent-U-Boote bedroht")) (ahv 'noipix-patents (ML BtP "Bildverarbeitung durch IPIX torpediert")) (ML Shi "Starke Verschlüsselung nicht in GnuPG") (ahv 'burnallgifs (ML Gur "GIF-Bildererzeugung nur mit proprietärer Software")) (tpe (ahv 'w3-p3p (ML Wnt "WWW-Normierung von Patenten torpediert")) (ML DaG "Der Präsident des W3C erklärte auf einer Softwarepatent-Anhörung in Paris, ca 30% der Mittel seines Gremiums seien derzeit durch Patentärger gebunden.")) (ML Bem "Bruce Perens wurde von Patentanmeldern gezwungen, sein ganz eigenständig entwickeltes Programmierwerkzeug EFence aus dem Verkehr zu ziehen.") ) )
    (distrib 
     (ML Doa "Distributoren von freier Software und Shareware")
     (ML Snd "Solange nur industrielle Prozesse mit Softwareanteil aber nicht %(q:Programme als solche) patentierbar sind, stellt das Weiterverteilen der Software keine Patentverletzung dar.")
     (ML Isd "In dem Moment, wo dies sich ändert, können Distributoren wegen Patentverletzung belangt und auf %(q:entgangene Gewinne) verklagt werden.")
     (ML Eti "Eine Firma wie SuSE, die Tausende von Programmen verteilt, müsste, um sich gegen solche Rechtsrisiken abzusichern, Millionen von Algorithmen mit Hundertausenden von Patenten vergleichen und auf Verletzungen absuchen.  Da dies unmöglich ist und man gerade in Logikalien-Angelegenheiten nicht den geringsten Anlass hat, auf Verständnis bei der Patentjustiz zu hoffen, steht eine Distributionsfirma immer mit einem Bein im Gefängnis.") )
    (komplexkre
     (ML ktl "kleine bis mittlere Urheber[firmen] komplexer Systeme")
     (ML Fds "Firmen wie Netpresenter haben einerseits nicht die Mittel, um die zum Alltagsgeschäft der Programmierung entstehenden Neuerungen patentrechtlich zu nutzen.  Andererseits müssen sie ständig damit rechnen, bei ihren täglichen Neuerungen auf Tretminen zu stoßen.  Einige Europäische Firmen dieses Typs wie z.B. Netpresenter (Holland), MySQL (Schweden), Infomatec (Deutschland), Sniff (Österreich), die durchaus nicht nur dem Linux-Umfeld entstammen, haben große Angst vor dem Patentsystem.  Sie müssen tatsächlich fürchten, dass ihnen angesichts der Schutzgeld-Forderungen aus zehntausenden von Patenten schnell die Luft ausgeht.") )
    (kelkgrand 
     (ML meq "manche Großfirmen")
     (ML Sge "Selbst Firmen mit großem Patentportolio profitieren nicht immer von der Existenz des Patentsystems.  Patentgegner Oracle hat sich nur zu defensiven Zwecken ein großes Patentportfolio angelegt.  D.h. Oracle musste viel Geld ausgeben, um sich vor Gefahren zu schützen, die sonst nicht existiert hätten.  Die auf Patente gegründete Firma OpenMarkets begründete 1999 umfangreiche Personal-Entlassungsmaßnahmen u.a. mit Fehlinvestitionen in Patente.  Die umfangreiche Patentesammlung hatte zu verlustreichen Prozessen mit anderen Firmen, u.a. mit IBM, geführt.") )
    (unbeteil 
     (ML Uej "Unbeteiligte Branchen, der Verbraucher, die Öffentlichkeit")
     (ML Dod "Durch Patente erhöhen sich die allgemeinen Verwaltungs- und Transaktionskosten.  Viele Unternehmen geben bereits heute 10-30% ihrer Einkünfte für den Rechtsetat aus.  Das schlägt selbstverständlich auf die Preise durch.")
     (ahv 'upside-contrinfr (ML nnd "Über den Umweg der Softwarepatente werden Geschäftsmethoden aller Branchen patentierbar, und allerlei bisher unverfängliche Handlungen werden als %(q:Beihilfe zur Patentverletzung) Gegenstand von Abmahnungen."))
     (ML HkW "Hinzu kommt die Zerstückelung der bisher relativ freien informationellen Infrastruktur und die Zerstörung freier Software, auf deren Existenz das Internet und das WWW aufbaut und von der unzählige Unternehmen in kaum schätzbarem Maße profitieren.") ) ) )
  (srch 
   (ML Svg "Sind die Patentämter ihren Aufgaben gewachsen?" "Are the Patent Offices up to their job?")
   (ahst 'swpahar25 (ML NtA "NEIN, meint Gregory Aharonian."))
   (ML Bst "BALD, versprechen Softwarepatent-Befürworter seit vielen Jahren.  U.a. schreiben Betten und Esslinger in %(q:Computerrecht) 2000/1:")
   (blockquote
    (ML EgW "Ein Problem stellt die schwierige Recherchierbarkeit von Softwareerfindungen dar, da in den Datenbanken der Patentämter in diesem vergleichsweise jungen Gebiet noch nicht sehr viel leicht recherchierbare, klassifizierte Patentliteratur vorhanden ist.  Dies hat zur Erteilung von zu breiten Patenten auf dem Softwaresektor geführt und wird sich aber spätestens dann ändern, wenn ein größerer Fundus an klassifizierter Patentliteratur vorhanden ist.") )
   (let ((SPI (ahv 'swpatinst "SPI"))) (ML PWd "Patentprüfer sehen sich täglich gezwungen, triviale Erfindungen zur Patentierung zuzulassen.  Es gelingt nämlich nicht, die zuvor veröffentlichte Technik ausfindig zu machen.  Patententhusiasten versprechen immer wieder, dass es sich hierbei nur um ein zeitweiliges Übergangsphänomen handelt, aber Gregory Aharonians Studien zeigen deutlich, dass die Qualität der Patentprüfungen eher sinkt als steigt, und dass auch zur Abhilfe gegründete Institutionen wie das %{SPI} ihren Auftrag verfehlt haben."))
   (ML Dtt "Das Problem ist auch grundsätzlicher Natur:")
   (sects
    (sprachbarr
     (ML BCd "Barriere zwischen Computersprache und Recherche-Sprache")
     (ML Cse "Computerprogramme dokumentieren sich selbst.  Sie sind als solche bereits eine Form der Information und enthalten somit ihre eigene Dokumentation.   Besonders vollwertige Systeme wie z.B. das Drucksatzsystem TeX sind oft nach dem Prinzip der %(q:literarischen Programmierung) verfasst.  D.h. die Dokumentation ist mit dem Programm verwoben und aus ihm erzeugbar.  Bei weniger vollwertigen Systemen ist die Dokumentation nicht getrennt erzeugbar und daher für Patentrecherchen kaum zugänglich.  Bei proprietärer Software ist die Bauplan-Information dem Patentrechercheur vollends verschlossen.  Dennoch gehört auch diese Software zum Stand der Technik, denn der %(q:in der Kunst geübte Fachmann) kann sie dekompilieren, egal ob das erlaubt ist oder nicht.") )
    (fachlit
     (ML Fpr "Fachliteratur spielt eine untergeordnete Rolle")
     (ML Wlr "Weil bei Logikalien keine Grenze zwischen %(q:technischer Information) und %(q:gewerblicher Anwendung) gezogen werden kann (s.o.), spielen auch Fachzeitschriften nicht die Rolle, die sie bei den klassischen Natur- und Ingenieurwissenschaften spielen.  Für Patentliteratur gilt dies a fortiori.  Denn ein Großteil der Neuerungen wird weiterhin außerhalb des Patentwesens stattfinden.") ) ) )
  (entp
   (ML Vhn "Verpassen die Europäer Chancen?" "Are European companies missing chances?")
   (ML Lda "Laut Informationen des EPA befanden sich 1997 ca 75% der (gesetzeswidrig) angemeldeten europäischen Softwarepatente im Besitz von amerikanischen und japanischen Firmen.  In den USA liegt diese Zahl bei 90%.")
   (ML Hcn2 "Hierüber machen sich diejenigen Patentexperten, die diese Entwicklung maßgeblich verursacht haben, Sorgen.  Die EU plant eine Patentaufklärungskampagne.  Ein führender Patentinflationstreiber warnt im Rundschreiben an seine Mandanten:")
   (blockquote (ML Dfa "Durch die .. Entwicklung der Rechtsprechung .. hat sich das Patentrecht von der traditionellen Beschränkung auf die verarbeitende Industrie gelöst und ist heute auch für Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu befürchten, das die deutschen Dienstleistungsunternehmen ... insbesondere gegenüber der US-amerikanischen Konkurrenz ins Hintertreffen geraten."))
   (ML Vnt "Vermutlich würde die europäische Softwarebranche tatsächlich einiges mehr an Patenten anmelden, wenn sie nicht nur in den USA sondern auch auf dem heimischen Markt durch das Vorhandensein eines allumfassenden Patentsystems dazu gezwungen würde.")
   (ML Dze "Dies ist aber von der Frage zu trennen, ob wir ein solches System auch haben wollen.")
   (ML Iei "Im internationalen Vergleich nutzt ein solches System immer eher denjenigen,deren Industrie weiter entwickelt ist.  Die USA wurden erst in dem Moment zum Vorreiter der Patentinflation, als sie selbst Spitzenpositionen in der %(q:Neuen Wirtschaft) bereits besetzt hatten.  ") ) ) )

(mlhtdoc 'swpbpfaru (ML WWs "Was sollte die Bundesregierung tun?" "What should the Government do?")
 (ML Eht "Erstmals fragt eine europäische Regierungsorganisation öffentlich nach den Auswirkungen geplanter Gesetzesänderungen im Patentwesen auf die Informationstechnik, die IT-Wirtschaft und das Gemeinwohl.  Der FFII möchte dazu beitragen, dass diese überfälligen Fragen mit dem gebührenden Ernst studiert werden, bevor man einschlägige Gesetze und Verträge ändert." "For the first time a European government body is inquiring into the effects of planned patent law changes on information technology, IT economy and public interest.  The FFII wants to contribute to a thoughrough investigation of these questions, which should precede any changes of law.  Much of this paper remains untranslated.")
 nil
 (sects
  (stud 
   (ML Smu "Studieren, Dokumentieren, Diskussion vertiefen" "Study, Document, Deepen Discussion")
   (ML Sdp "Selbst führende %(q:Meinungsbildner) in der EPÜ-Diskussion, etwa das europäische Patentamt, schwanken noch über konkrete Änderungen des EPÜ. Waehrend das EPA im März 1999 eine völlige Streichung der Ausnahmeregeln in Art 52 (2) (2) und (3) forderte (Esslinger, Computerrecht 1/2000, 17), so ist im Amtsblatt vom August d.J. nur noch von einer Streichung der %(q:Computerprogrammausnahmen) die Rede.")
   (ML Aec "Andererseits ist auch gar nicht bekannt, wie weit der Konsens ueber die schleichende Ausweitung des Technikbegriffs innerhalb der Patentämter wirklich geteilt wird - so wurden in den letzten 15 Jahren z.B. zum Thema Artikel 52(2) Softwarepatentierbarkeit alle oft-zitierten Entscheidungen der technischen Beschwerdekammer des Europäischen Patentamtes von einem Vorsitzenden und einer handvoll Beisitzer verabschiedet.")
   (ML Sma "Sofern öffentliche Diskussionen stattfanden, beschränkten diese sich auf den Welt der Patentfachleute.  Weder drangen diese Debatten in die Welt der Informatiker vor, noch hatten die Patentfachleute jemals Gelegenheit, sich in die Andersartigkeit der entstehenden Logikalienökonomie hineinzudenken.  Die Schlagworte %(q:Linux) und %(q:OpenSource) drangen erst in dem Moment an ihre Ohren, als die Pläne der Patentfachleute bereits feste Formen angenommen hatten.")
   (ML Wnu "Während 1997 viele Patentfachleute noch guten Gewissens von einem Konsens ausgehen konnten und deshalb die %(q:auf allen Kanälen) betriebene Arbeit nicht näher in Frage stellen zu müssen glaubten, ist es spätestens heute Zeit, innezuhalten und die Folgen zu studieren.")
   (ML NSv "Nach Erhalt weiteren Datenmaterials sollte das Thema Softwarepatente im Bundestag ebenso wie auch im Europäischen Parlament zur erneuten Debatte gestellt werden.")
   (sects
    (priodat 
     (colons (ML Eet2 "Europäer für die Patentschlacht in US+JP rüsten") (ML Snc "Stand der Technik systematisch aufarbeiten "))
     (ML Ang "Auch wenn sich Europa, wie wir hoffen, für eine freie informationelle Infrastruktur entscheidet, wird die europäische Softwarebranche sich in den USA und anderen Ländern zumindest einige Jahre lang häufig mit Patentansprüchen auseinandersetzen müssen.  Hierbei könnte eine leistungsfähige und vollwertige (d.h. frei zugängliche und weiterverwendbare) öffentliche Datenbank über den Stand der Technik gute Dienste leisten. ")
     (filters ((sp ahst 'priorart)) (ML cte "%(sp:Die Softwarepatent-Arbeitsgruppe des FFII liefert regelmäßig Datenträger an die Bayerische Staatsbibliothek, mit denen Entwicklern freier Software geholfen werden soll, im Falle einer Patentklage den Zeitpunkt ihrer Ideen nachzuweisen).  Das hilft allerdings nur denjenigen, die genau wissen, für welche Erfindungen sie einen Zeitnachweis brauchen.  Ziel muss es sein, den aktuellen und früheren Stand der Technik für alle transparent abfragbar zu machen.  Hierzu könnte man ein dezentrales System von Rechnern aufbauen, die sich gegenseitig entweder Plattenplatz oder Geld anbieten.  Manches ließe sich sogar kommerziell organisieren."))
     (ML Enr "Ein solches Projekt könnte in einem neuartigen internet-gerechten Verfahren der öffentlichen Ausschreibung entstehen.  In dieses Verfahren wären sowohl freie Entwickler als auch Firmen einzubeziehen.  Alle abzuliefernden Ergebnisse müssten vollwertige Logikalien sein.  Dort, wo sich für einen aufwendigen Projektteil keine Freiwilligen finden, könnten kleine bis mittelgroße Dienstleistungsfirmen nach Ausschreibung einzelne Projektteile übernehmen.") ) 
    (umfrag 
     (ML Scg "Systematische Meinungsforschung")
     (ML Sfe "Sinnvoll wäre vermutlich auch eine von der Bundesregierung in Auftrag gegebene Studie, auf der sich interessierte Kreise für oder gegen Softwarepatente aussprechen können (eventuell Neuauflage der unveröffentlichten Umfragen von Bernhard Müller (Computerrecht 1/2000).")
     (ML Dia "Diese Studie sollte sich allerdings nicht nur an 44 Patentrechtsexperten sondern vor allem an die betroffenen Kreise wenden.  Die darin gestellten Fragen sollten vielseitiger sein und sich in einem dynamischen Dialog über WWW-Umfrageformulare entwickeln.  Einen einsatzbereiten Prototyp eines solchen Systems mitsamt Fragenvorschlägen hat die Softwarepatent-Arbeitsgruppe des FFII bereits entwickelt.") )
    (efekt 
     (ML Ssf "Systematische Erforschung der Wirtschaftlichen Auswirkungen")
     (ML Iee "In unserem Positionspapier konnten wir nur einige mögliche Denkwege aufzeigen, aber viele unserer Antworten auf die aufgeworfenen Fragen befinden sich auf dem Niveau von Mutmaßungen.")
     (ML Eer "Es wäre sinnvoll, interessierte Wirtschaftswissenschaftler in die Diskussion einzubeziehen und dabei sowohl die Fragestellungen zu verfeinern als sie durch repräsentative Datenerhebung zu beantworten.  Auch hierbei könnte das von uns angeregte internet-gestützte Umfragesystem gute Dienste leisten.  Ferner könnten sofort weitere Konferenzen anberaumt werden.")
     (ML FhW "Ferner sollte auch eine ernsthafte Diskussion über mögliche Sui-Generis-Alternativen zum Patentrecht in den Fällen nachgedacht werden, wo besondere Vorrechte auf Iden wirtschaftspolitisch erwünscht sein können, wie etwa bei MPEG.   Hierbei ließe sich bei der 1994 in Columbia Law Review begonnenen Diskussion um %(q:Manifesto Concerning the Legal Protection of Computer Programs) ansetzen.  Diese Diskussion wurde damals nur deshalb beendet, weil die Patentlobby in Amerika bereits vollendete Tatsachen geschaffen hatte.") ) ) )
  (epue 
   (ML Eus "EPÜ-Revision abblasen und Rechtslage klären" "Shape the EPC Changes")
   (ML Dof "Die Empfehlungen der letzten Regierungskonferenz sind nur %(q:Empfehlungen), aber in keiner Weise bindend, und können im November 2000 von der nächsten Regierungskonferenz begründet verworfen werden. Die Bundesregierung sollte ihre Vertreter auf ein Negativvotum zur Änderung von Art 52(2) binden und diese Absicht auch rechtzeitig anderen Mitgliedsstaaten mitteilen.")
   (lin
    (filters ((pr ahv 'de-prl)) (ML GWW "Gleichzeitig könnte die Bundesregierung die deutsche Patentjustiz wegen ihrer Rechtsbeugung rügen und mit einer unmissverständlichen Richtlinie auf den Kurs zurückverweisen, der nicht nur im Gesetz sondern auch in den Prüfungsrichtlinien bis heute eigentlich schon recht unmissverständlich vorgeschrieben ist:"))
    (blockquote 
     (al
      (ML 2tA "2.6.2. Dem Patentschutz nicht zugängliche Anmeldungsgegenstände")
      (ML Ere "Ein Anmeldungsgegenstand stellt nur dann im Sinne des § 42 PatG seinem Wesen nach eine Erfindung dar, wenn er in einer Anweisung besteht, Kräfte, Stoffe oder in der Natur vorkommende Energien zur unmittelbaren Herbeiführung eines kausal übersehbaren, wiederholbaren Erfolgs zu benutzen, und wenn dieser Erfolg ohne Zwischenschaltung menschlicher Verstandestätigkeit unmittelbare Folge des Einsatzes der Naturkräfte ist.") 
      (ML EnG "Erfüllt ein Gegenstand die beschriebenen Voraussetzungen nicht oder handelt es sich um einen der in § 1 Abs. 2 PatG genannten Gegenstände, ist die Anmeldung zu beanstanden.") ) )
    (ML HWw "Hieraus ergibt sich, dass nicht eine Logikalie (Computerprogramm als solches), wohl aber eine programmgestützte Physikalie (z.B. Industrieprozess, MP3-Abspielgerät), die %(q:Kräfte, Stoffe oder in der Natur vorkommende Energien) benutzt, Gegenstand von Patentansprüchen sein kann.  Patente sollten nicht verwendet werden, um das Kopieren von Information (und letztlich auch sonstiges nicht-industrielles Duplizieren, wie z.B. die Vermehrung von Leben) zu untersagen.") ) )
  (dleg 
   (ML KiW "Rechtsgüter wie Kompatibilität, Quellenoffenheit etc durch getrennte Gesetze gegen Patentgefahren absichern" "Protect Legal Goods such as Compatibility, Source Accessibility etc by Separate Laws")
   (ML DtW2 "Die oben angeregten Klärungsdirektiven des BMJu könnten auch als eigenständige Gesetze formuliert werden.  Durch ein nationales Gesetz könnten auf diese Weise zumindest verhindert werden, dass die Patentjustiz sich an den Entwicklern freier Software vergreift, wie es die oben zitierten Patentanwälte Esslinger und Betten unter %(q:Softwarepatente und «Open Source»-Bewegung) in Aussicht stellen:")
   (blockquote
    (ML Are "Außerdem erstreckt sich nach §11 PatG die Wirkung des Patents nicht auf Handlunben, die im privaten Bereich zu nicht gewerblichen Zwecken vorgenommen werden, sowie Handlungen zu Versuchszwecken, die sich auf den Gegenstand der patentierten Erfindung beziehen.  Der Programmierer, der zu Hause und ohne kommerzielle Absicht ein Programm schriebt, braucht auf eventuellen Patentschutz nicht zu achten.  Dies ändert sich jedoch, wenn die Privatsphäre verlassen wird, ein Programm also etwa kostenlos im Internet zum Herunterladen angeboten wird.") )
   (ML Hcn "Hier könnte der deutsche Gesetzgeber klarstellen, dass das %(q:Anbieten zum Herunterladen) von vollwertigen Logikalien, d.h. Programmen mit einer OpenSource-konformen Lizenz, nicht als %(q:gewerblich) im Sinne des Patentrechts zu betrachten ist.  Die Freiheit der Informationsweitergabe soll Vorrang vor den Verwertungsansprüchen der Vermarkter konkurrierender proprietärer Software haben.  Eine solche Regelung könnte sogar dann Bestand haben, wenn die Europäische Patentorganisation ihre Reformpläne durchsetzen sollte.")
   (filters ((ol ahv 'osslaw-articles)) (ML Fai "Ferner sollte in Deutschland das Recht auf Kompatibilität, wie es von der oben zitierten EU-Direktive von 1991 im Ansatz vorgesehen ist, Vorrang vor Patentansprüchen erhalten, ebenso wie es bereits heute Vorrang vor aus dem Urheberrecht abgeleiteten Dekompilierungsverboten u.ä. hat.  Auch dies wäre mit einem getrennten Gesetz zu klären.  Dabei könnte die %(ol:laufende Gesetzesinitiative einiger französischer Parlamentarier) zu Rate gezogen werden.") )
   (lin (ML Sen "Schließlich könnten Patentämter verpflichtet werden, auf Anfrage verbindliche Auskünfte darüber zu erteilen, ob ein bestimmtes Programm ein Patent verletzt.  Dieser Auskunftsdienst sollte zumindest im Falle von Freier Software unentgeltlich geleistet werden, wie Brian Behlendorf, führender Entwickler des weltweit führenden WWW-Servers in %(e:Open Sources - Voices from the OpenSource Revolution, O'Reilly, 1999, S.166) begründet:")
	(blockquote (al (ml "[The Mozilla Public License] has several provisions protecting both the project as a whole and its developers against patent issues in contributed code. ...") (ml "Taking care of the patent issue is a Very Good Thing.  There is always the risk that a company could innocently offer code to a project, and then once that code has been implemented thoroughly, try and demand some sort of patent fee for its use.  ...") (ml "Of course it doesn't block the possibility that someone %(e:else) owns a patent that would apply; there is no legal instrument that does provide that type of protection.  I would actually advocate that this is an appropriate service for the USPTO to perform; they seem to have the authority to declare certain ideas or algorithms as property someone owns, so shouldn't they also be required to do the opposite and certify my submitted code as patent-free, granting me some protection from patent lawsuits?")))
	) ) ) )
; swpbpos25-dir
)

(mlhtdoc 'swpahar25 
 (let ((GA "Gregory Aharonian")) (ML Van "Vortrag von %{GA}" "%{GA} Presentation"))	 
 (ML Ape "%{GA} wird am 18. Mai erläutern, wie Softwarepatente sich in den USA seit Anfang der 90er Jahre auswirken, wer/was davon begünstigt/gehemmt wird." "%{GA} will explain to a public conference in the Berlin Ministery of Economics some of the effects that software patents as they stand are exerting on the economy in the US and Europe.")
 nil
 (sects
  (purci 
   (ML Frt "Frühere Artikel" "Previous Papers") 
   (lin
    (let ((MSG (quot "The Debate is Over -- Industry Demands Software Patents")) (UNION (tok euip-union)))
      (ML Aro "Aharonian's Statistiken werden gerne von Softwarepatent-Apologeten zitiert.  Peter Hanna, Software-Referent der %{UNION}, begründete damit auf dem Münchenr UNIONs-Konferenz 1997 seine zentrale Botschaft %{MSG} und empfahls sie u.a. mit den Worten:" "Aharonian's statistics are widely cited even by patent enthusiasts.  E.g. Peter Hanna, chairman of the %{UNION}, in his speech %(q:Software Patents - International Trends and Statistics) at the Munich conference of 1997, supported his core message %{MSG} with Aharonian's statistics, and recommended them:") )
    (blockquote (ML Tte "The present International Patent Classification System does not allow for any easy analysis of the most common subject areas for Software Patents, or to find some of the new emerging software technologies.  The best analysis of this that I have seen has been done by Gregory Aharonian.")) ) 
  (ul
    (ah "stat-1998.txt" "1998 Software Patent Statistics:  Who is getting the Patents?")
    (ah "search-1995.txt" "Inadequacy of Prior Art Searches in US, EU and Japan")
    (ah "gest-2000.txt" "trivial/obnoxious business method patents")
    (ah "http://www.bustpatents.com/corrupt.htm" "The Patent Examinations System is Intellectually Corrupt")
    ) )
  (velgaz
   (ML Gsr "Gerüst des Berliner Vortrags" "Outline of the Berlin Presentation")
   (sects
    (util
     (ML WWa "Wem nützen die SWPatente?  Was fördern sie?" "Whom do swpatents help?  Which economic behaviors do they encourage?")    
     (ML Se0 "Software patents basically help two different types of companies.  It helps small companies that actually have invented something new in the world of software (a hard thing to do given the tens of millions of programmers around the world) and need protection from large competitors as they enter the market with their product.  One of the few examples of this was when Stac won $90 million from Microsoft after Microsoft infringed Stac's disk file compression patent.  But large settlements like this are rare (and some argue that Microsoft should have won the lawsuit).")
     (ML Tae "The other group of entities to benefit are very large companies like IBM, which with their portfolios of hundreds and thousands of software patents, can obtain hundreds of millions of dollars, if not billions, from forcing smaller and newer companies to license part or all of their portfolios.  This is a intergenerational transfer of wealth.") )
    (neutil
     (ML WeW "Wem schaden die SWPatente?  Was hemmen sie?" "Whom do SWPatents hurt?  What kind of behaviors do they discourage?")
     (sects
      (newent 
       (ML Nbd "Neuland betretende Firmen" "New Entrants")
       (ML Fla "First, the smaller and newer (or new entrants such as European companies) companies having to pay royalties to older companies with well established patenting systems and large portfolios of issued patents.  These royalties are monies not then available to compete in the marketplace, sometimes against these larger and older companies.") )
      (investors 
       (ML Aiv "Aktionäre" "Stock Market Investors")
       (ML Aoh "A second set of losers is stock market investors.  Increasingly companies getting software patents are touting them in the press and on the Internet, even if the patent is of questionable validity.  This leads a naive investing public to bid up the price of the company's stock.  The stock price eventually drops as the market determines that the company's patent is probably of little value as a strategic weapon.  Those investing too late thus lose money as the stock price readjusts to a more realistic valuation.")
       )
      (extort 
       (ML OSe "Opfer von Schutzgelderpressungen" "Extorsion Victims") 
       (ML Acc "A third sets of losers is large numbers of small and large companies being forced to pay monies to avoid patent lawsuits.  For example, if someone with a patent of questionable validity offers you a universal license for $30,000, it is hard for a company not to pay the $30,000 because to do anything else (starting out with obtaining a invalidity opinion from their lawyers) will cost as much if not more.  The $30,000 becomes a cheap %(q:insurance policy) (compared to the alternatives), even though the patent may be completely invalid.  Some argue that companies asserting such patents are engaging in little more than extortion.") 
       (ML Wse "With the US Patent Office issuing over 20,000 software patents a year, companies could easily soon be in a position to have to pay these fees a few times each year, say $100,000 worth of licensing fees, money better spent on R&D and marketing.  These possibilities have lead to a growing market for patent infringement insurance.") )
      (inflat
       (ML Ite "Inhaber unterschätzter Patente" "Owners of understimated patents")
       (ML Apb "A four set of losers is those small companies with new software patents that are actually valid, but presumed to be invalid like so many other software patents actually are invalid.  These small companies thus have to spend more time and money trying to have their patent(s) considered more valid than public perceptions assume them to be.  Large numbers of low quality patents diminish the importance and value of the small numbers of high quality patents.") )
      (delay 
       (ML Fmi "Flinke Unternehmer, deren Arbeit verzögert wird" "Companies that experience delays")
       (ML Ati "A fifth set of losers are companies that experience delays in entering a software market while they assess the quality of patents that affect the market.  For example, Priceline.com has a patent on its reverse auction technique, a patent most likely invalid in light of non-patent prior art not discovered and consider by the US PTO.  Many believe that potential competitors of Priceline delayed in entering the online auction market while they assessed (or waited for others to assess) the validity of the Priceline patent.  The invalid patent is a barrier to entry.  This is related to the problem European companies will face as they enter the US software marketplace, when holders of American software patents use their software patents to delay or prevent the entry of the European company.") )
      (startup 
       (ML NWm "Neugründungen in patentvermintem Gelände" "Startups in patent-mined areas")
       (ML Ade "A six set of losers are startups, which sometimes find it harder to raise venture capital if there are many patents, good and bad, in their field of software technology.  Venture capitalists see all of these patents as potential mines in the future, so directed their investment dollars towards startups with lesser degrees of being affected by issued patents.") ) )
      (ML IWa "In short, large numbers of bad software patents distort marketplaces and lead to transfers of profits not necessarily in the best interests of encouraging and rewarding innovation.  These problems due to American software patents are surely to cross the Atlantic if Europe liberalizes its software patenting policies.") )
    (srch 
     (ML Svg "Sind die Patentämter ihren Aufgaben gewachsen?" "Are the Patent Offices properly equipped to do their job?")
     (ML Wht "With regards to the three major Patent Offices, PTO, EPO and JPO, the answer to this question is %(q:NO).  The United States Patent and Trademark Office has been known for over thirty years to have problems with software patent applications.  One of the most commonly shared complaints is that the PTO for the most part ignores non-patent prior art, such as conferences and journals, which for software patent applications is often the most relevant prior art.")
     (ML Fct "For example, 80% of the issued US software patents in the last ten years, effectively cite no non-patent prior art from any of the world's engineering and computer societies such as the IEEE/IEE, ACM and others. Compounding this deficiency is the problems the PTO is having hiring and retaining people with good academic and/or work experience with software. Patent examiners with bachelors degrees in electrical engineering and a few years industry experience are not qualified to examine advanced software patent applications.")
     (ML Ocs "Over the last six years, the PTO has made many announcements of new initiatives to address the problems it is having examining software patents, but it is hard to detect any improvements in their practices by analyzing recently issued software patents.  Recent court decisions allowing patents on methods of doing business have increased the flood of software patent applications already overwhelming the PTO, making it likely that the overload will lead to lower quality software patents from issuing.")
     (ML APs "Assuming that bureaucracies are globally similar, is not unreasonable that as the Japanese and European Patent Offices start experiencing levels of software patent applications similar to the 50,000+ software patent applications a year the US Patent Office receives, that the JPO and EPO will demonstrate similar levels of problems handling software patents. Indeed a study I did in 1994 comparing the quality of PCT searches done by the EPO and PTO found little difference in the quality, that the EPO searches were as inadequate as those of the PTO.  Assuming that searching conditions haven't changed much in recent years at the EPO (especially in light of last falls protests by EPO examiners about being overworked), then I suspect a study of PCT search quality for recent PCT applications will find similar levels of quality.")
     (ML Sub "So Europe should be careful in allowing expansion of software patenting rights at the EPO and regional offices, without first putting in place some monitoring mechanisms and support systems to make sure that EPO avoids many of the problems the PTO demonstrates (partly because there are no independent monitoring mechanisms for PTO performance quality).") 
     )
    (entp 
     (ML Vhn "Verpassen die Europäer Chancen?" "Are the Europeans missing Chances?")
     (ML Iwz "If one views patents as a form of a trade barrier, then in at least one aspect, European companies are missing an opportunity.  That is, in the United States, which has the most liberal software patenting policies, American and Japanese companies acquire over 90% of the issued software patents (Americans get about 60%, while the Japanese get around 30%). Given the comparably sized economics between the United States, Japan and Europe, one would naively assume that a three way ratio would be (2:1:1), with Americans getting 46%, Japanese getting 23% and Europeans 25%.")

     (ML Itr "In reality, Europeans are getting about 5% of the US software patents, and thus at a disadvantage when conducting business in the United States, because European patent portfolios are too small to trade with American companies such as IBM with very large portfolios.  The cost of entry into the American software market is thus higher, where European companies aren't outright prevented from entering the software market due to blocking patents for which European companies have little to offer other than royalties.")

     (ML Afc "Additionally, if Europe liberalizes its software patenting policies, American and Japanese companies will have an advantage at the outset due to their many decades of experiences in American and Japan acquiring and asserting software patents.  It will be easy for American and Japanese companies to take many of their patent applications already pending and adapt them for European applications (especially those patent applications pending that have filing date extensions due to PCT protection).") )
     ) ) ) ) 

(mlhtdoc 'swpblis25 nil (ML Vri "Vier FFII-Mitglieder waren bei der Konferenz anwesend.  Aus ihren Notizen wird hier in Kürze ein Bericht zusammengestellt" "Four FFII members were present at the conference.  From their notes a report will be built.") nil
(ML NSr "Notizen aus Sicht von Bernhard Reiter")
(sqbrace (ML Set "Selbstverständlich nicht vollständig, sondern nur dass, was ich aus meiner Sicht verstanden und notiert habe. Mitunter finden sich ein paar Anmerkungen von mir in eckigen Klammern. Ich bitte zu entschuldigen, dass manche Argumente nur teilweise vorhanden sind. Trotzdem habe ich Fragmente aufgenommen, weil sie weiteren Personen als Gedächnisstütze dienen können.  Es ist keine wörtliche Rede, sondern nur sinngemäß wiedergegeben."))

(ML Ahi "Auf dem Podium nahmen Platz, von links nach rechts:")

(lin "Herr Pilch" "Frau Weber-Cludius" "Herr Soquat" "Herr Hössle" "Herr Aharonian")

(dl
(l (ML Hob "Herr Soquat begann")
 (ML upg "uns liegt daran die Open-Source Entwicklung ... zu unterstützen."))
(l "Weber-Cludius" 

   (ML Ere "Es liegt an: Richtlinienvorlage diesen Sommer Ende des Jahres: Revision des EPÜs")
   
   (ML Zti "Ziel: Langfristige Förderung der Wirtschaft.... 	wichtig dabei der technische Fortschritt definiert durch.... ")
	
   (ML Eon "Es gibt genügend Anreize durch das Patentwesen. Es ist ein Monopolrecht, andere von der Nutzung des neuen Wissens auszuschliessen.  Dafür wird dieses Wissen für die Volkswirtschaft offengelegt.")	

   (ML Eto "Eine der Fragen: In wie weit ist Software eine schöpferische Leistung? Es ist schliesslich Aufwand nötig. Ist sie patentfähig, ja oder nein? Was ist mit Anwendungen im Internet? Beispiel: OneClick")
	
   (ML ine "im Grunde gibt es einen Zielkonflikt: Anreize schaffen im Gegensatz zur Behinderung der Entwicklun und des Wettbewerbs.")
)

(l "Werner Riek (Jornalist):"

   (ML SrD "Stellte bei Recherche fest: Die Wettbewerbsfrage wurde nicht genügend beachtet. Das BMWi hat sich nur wenig dazu geäußert.") )
		
(l (tpe "Wolfgang Tauchert" (ML Sth "Softwareexperte des Deutschen Patentamts"))

   (ML Ksn "überrascht durch den Widerstand gegenüber der Anpassung. Das Patentamt hat nicht darauf hingewirkt. Bisher wurde die Klausel des %(q:Ausschlusses von Computerprogrammen) nicht als Zurückweisungsgrund verwendet. Es wird sich durch Streichung der Klausel also auch nicht ändern. Insofern ist kann es aus Sicht des Patentamtes rechtlich so bleiben, wie es ist, wenn die Streichung der Klausel ein Problem ist.")
	
   (ML FWn "Für die Revision allerdings spricht: Die Übereinstimmung mit dem TRIPS. hier wird besagt, dass alle Erfinderischen Tätigkeiten patentierbar sein sollen. Andere Länder wollen das Gebiet Programm als ein Gebiet der Technik da nicht ausschliessen.")
)	

(l (tpe "Peter Gerwinski" (ML GkE "G-N-U Essen"))

   (ML DsS "Die Kernfrage ist nicht Technik, bzw. Patentierbarkeit von Software sondern Wachstum. Softwarepatente werden hier schaden. Trotzdem will ich die Frage beantworten: Software ist nicht technisch sondern eine mathematische Formel. Sie wird entdeckt und der der Entdecker bekommt meist kein Patent. Bei Software sind auch die Zeiträume viel kürzer.")
)

(l (ML RWn "Rückfrage von Herrn Soquat:") (ML Wrm "Warum?"))

(l "Gerwinski" 
   (ML Sae "Softwaresysteme sind komplex. Es gibt eine große Anzahl potentiell patentierbarer Elemente. Kann mit Softwarepatenten nichts verwenden, was vorher da war, weil es zuviel Recherche erfordern würde. über den Daumen gepeilt: Einen Monat Patentrecherche für einen Tag Softwarearbeit.")
) 

(l (tpe "Daniel Riek" (commas "ID-Pro AG" "LIVE"))
   (ML DIu "Danke für die Bereitschaft des BMWIs in der Frage zuzuhören. Für uns ist Software eine abstrakte Idee. Sie wird in einer Sprache definiert und kann auch per Hand ausgeführt werden. Es geht also um die Praxis, die sich über die Zeit entwickelt hat. Softwarepatente würden diese Praxis stören.") )
	
(l (tpe "Ralf Nack" (ML MtW "Max Plank Institut, Promoviert über Patentrecht"))
(ML EpW "Eine Änderung des Paragraphen würde nichts ändern, stimme Herrn Tauchert zu.")
(ML Ste "Software erfordert erheblichen Investitionsaufwand.") 
(ML Drj "Das Problem der Recherche besteht in allen Technikbereichen. Es ist in der Praxis nicht so problematisch, denn die Praxis reguliert sich hier dadurch selbst.")
(ML Wpn "Werden banale Patente erteilt? Es gibt hier praktische Probleme, weil der Stand der Technik erforderlich ist, also dem Datenbestand.")
)

(l (tpe "Herr Braun" "Intradat")
   (ML Feg "Frage an Herrn Tauchert: Hätte es dann OneClick Patent hier gegeben und wäre es erfinderisch?") 
   (ML TEW "Tauchert: Es gibt es hier nicht.") )

(l (tpe "Herr Schmidt" "EPA")
   (ML DlW "Die Amazon Patentanmeldung läuft. Nach Anmeldung wird das Patent erstmal ungeprüft veröffentlicht.")
   (ML WWe "Weiterhin möchte ich betonen, dass Patente (nur) regionale Gültigkeit haben.  Was in Amerika angemeldet wurde, gilt deshalb hier noch lange nicht ") )
		
(l (tpe "Prof. Lutterbeck" (ML TBl "TU Berlin"))
   (ML Dee "Das Thema ist vielschichtig, ich möchte erstmal die Meinung der Experten auf dem Podium hören. Unter Umständen können einige rechtliche Details von den Patentrechtsexperten unter sich ausgemacht werden.") )
	
(l (tpe "Herr. Hössle" (ML Pnw "Patentanwalt"))
   (sqbrace (ML Elv "Erklärte die rechtlichen Grundlagen von Softwarepatenten"))
   (ML Bjs "Belohnungstheorie: Monopolrecht auf Zeit, dafür wird das Patent Allgemeingut. Ausführliche Recherchen nötig und möglich.")
   (sqbrace (ML eee "erläutert Gesetzestexte auf Folie"))
   (ML Dhc "Das Patentwesen findet nach und nach zu einem dynamischen Technikbegriff.  Der Begriff der Technik ändert sich in Abhängigkeit von der wirtschaftlichen Entwicklung.")
   (ML NdW "Neuheit: Nirgends auf der Welt vorher bekannt.")
   (ML Vsl "Von der patentrechtlichen Seite ist es ein komplexes Thema. Es sollte systematisch diskutiert werden.")
   (ML Smr "Software-Ansprüche lassen sich als Hardware-Ansprüche umformulieren.  Beispiel: VITERBI- Alogrithmus. Wenn im Patentanspruch nur Algorithmus gestanden hätte und nicht die Anwendunge zum Senden von Signalen über gestörte Kanäle dann wäre es nicht patentierbar gewesen. So ist es aber patentiert worden für die eine Anwendung.") )
(l "Braun" (ML Are "Also kann ich den Algorithmus für eine andere Anwendung verwenden."))
(l "Hössle" (ML JKr "Ja, z.B. für Ihr Betreibssystem.")
   (ML Vhh "Viele Patentinhaber schätzen in der Praxis ihren Schutz so hoch ein.") )
(l "Braun" (ML Erv "Es ist immer noch missverständlich."))
(l "Hössle" (ML Ere2 "Ein Algorithmus ist allgemein, wie ein Kochrezept. Als Verfahren sind die auch patentierbar (Beispiel: Fertigsuppen in x Minuten)")
   (ML ZBe "Zum Vorwurf das hauptsächlich große Firmen profitieren, z.B.Siemens,IBM: Die meisten Patente gehören mittelständischen Firmen. Für sie ist es der einzige Weg in den Wettbewerb.")
   (ML Bes "Beispiel: Patent eines Stuttgarter Uni-Profs (Bauer?) über mehrdimensionale B-Bäume. Frucht schwerer geistiger Arbeit.  Dank Patentierung konnte Prof B einen japanischen Lizenznehmer finden, der den B-Baum-Algorithmus in seiner Datenbank einsetzt.") 
   (ML EWi "Es gibt auch Unterschiede zwischen dem Patentrecht der EU und dem der USA. Die USA haben immer noch ein altes Patentrecht. In der EU gibt es eine Offenlegung. Es ist also auch eine Möglichkeit mit wenig Geld (100DM ?) etwas zu veröffentlichen und damit dafür zu sorgen, dass niemand anderes das Patent darüber erhalten kann. In den USA fehlt weiterhin ein Einspruchsverfahren.") )
(l "Pilch" (sqbrace (ahst 'swpbpeude (ML zWe "zitiert und erläutert Dokumente, aus denen hervorgeht, dass die Rechtsprechung sich unter politischen Vorgaben vom Geist und Buchstaben des Gesetzes entfernt hat."))) )
(l "Greg Aharonian" (ahs 'swpahar25)
   (ML Eoe "Ein Beispiel sind die Prozesse von Apple gegen Microsoft und der andere von Lotus wegen des Spreadsheets. Beide Prozesse wurden vom Kläger verlohren, wurden aber nur auf Basis des (amerikanischen) Urheberrechts geführt.")
   (ML Ane "Amazon wusste bei dem OneClick-Patent, dass es diesen Patentanspruch nicht bis zuende durchsetzen konnte. Sie versuchten damit aber den Konkurrenten Barns&Nobles für drei Monate im Weihnachtsgeschäft durch eine Einstweilige Verfügung auszuschalten, um einen Wettbewerbsvorteil zu erlangen.") 
 (sqbrace (ML Hse "Hier fand die Mittagspause und das Mittagsbuffet statt.  Es ging weiter mit Fragen an Greg Aharonian.")) ) 
(l "Frage" (ML Whr "Was wäre ohne Softwarepatente?"))
(l "Aharonian" (ML Wei "Wenn wir die Softwarepatente abschaffen, würden die Leute nicht mit dem Erfinden aufhören. Die Leute langweilen sich und wollen etwas erfinden.  Nicht nur bei Software, auch in vielen anderen Gebieten sind Patente dabei eher hinderlich als hilfreich.  Insgesamt beeinflussen sie die Weiterentwicklung nicht wirklich wesentlich."))
(l (tpe "Volker Grassmuck" (ML Vre "Verein zur Förderung von Medienkulturen"))
   (ML Stv "Sind Softwarepatente eine Bedrohung von Freier Software?"))
(l "Aharonian:"  
   (ML SnW "Softwarepatente sind vor allem lästig.")
   (ML EeW "Es interessiert sich keiner wirklich für die Qualität der Patente. Ansonsten würden es ja Qualitätkontrollen geben und grundsätzliche Erfolgs- und Qualitätszahlen und Statistiken würden erstellt und veröffentlicht. Das passiert jetzt noch nicht.") )

(l "Pilch"
   (let ((PC (ahv 'patexam-corrupt)))
     (ML IWW "In Ihrem Artikel %{PC} dokumentieren Sie, wie schlecht das Patentprüfungssystem funktioniert.  Was müsste passieren, damit es besser funktioniert?  Ist das überhaupt möglich?")) )

(l "Aharonian"
   (ML Ent "Es gäbe sehr viel Raum für Verbesserungen.  Aber niemand ist ernsthaft daran interessiert.  Die großen Firmen und die Patentämter sind sich einig, dass möglichst viel patentiert werden soll.") 
   (sqbrace (ML DWr "Die Simultanübersetzer wurden mit Ende des Fragenblocks entlassen."))
   (sqbrace (ML kea "keine Notizen über einige der Fragen und Antworten")) )

(l (tpe "Andreas Bogk" (commas "convergence" "integrated media GmbH"))

   (ML EoW "Ein allgemeines Problem mit Patenten sind die Zeiträume.")
   (ML Ftd "Für eine kleine Softwarefirme spielt der Patentschutz kaum eine Rolle. Sie müssen den Vorteil vorher nutzen, ansonsten hat er keinen Wert. Deshalb werden sie das Patentwesen mit den langen Zeiten für eine Gewährung eines Patentschutzes in der Praxis eh nicht verwenden.")
   (ML Dai "Die Nichtpatentierparbeit von Mathematik wird unterlaufen.")
   (ML WWe2 "Was in Bezug auf Patenten großen Firmen hilft gilt nicht für kleine Firmen, die hier keinen Chance haben mitzuspielen.")	
   (ML Whi "Was ist eigentlich an Softwareentwicklung aufwendig?")
   (ML EBg "Es ist die Umsetzung der mathematischen Formel, das Beseitigen von Fehlern nach der Hauptumsetzung (Debugging) und die Unterstützung der Anwender (Support).") )

(l (tpe "Zedlitz" (commas "Siemens" "Patentabteilung"))
   (ML Vlu "Verfahrensschutz und der Schutz eines Algorithmus entsprechen eigentlich einander. Es müssen ja technische Kenntnisse vorhanden sein. Grundsätzlich gibt es einen technische Aufgabenstellung und eine Lösung.")
   (ML Doh "Die Frage ist also: Was ist tatsächlich patentfähig?") )
(l "Tauchert"
   (ML Zns "Zur Frage was durch ein spezielles Patent genau geschützt ist: Es ist genau das geschützt, was im Patentanspruch steht.")	
   (ML Wef "Wir haben doch einen guten, langen Schutz! [ Auch als Antwort auf an Bogks ersten Punkt gemeint. ]") )
(l "D. Riek"
   (ML Eaa "Es gibt verschiedene Sichtweisen. Ich möchte ein paar Beispiele aufzählen, wo Patente der Freien Software Entwicklung tatsächlich Probleme bereitet haben.") 
   (ML BDs "Beispiel: RSA Verschlüsselung, konnte nicht Frei Implementiert werden. Das war ein Problem bei der E-Mail verschlüsselung im GnuPG Projekt, was auch vom BMWi gefördert wird. Beispiel: mp3, Beispiel: DVD unter Linux") )
(l "Pilch" (ML fXe "ferner IPIX, LZW, Freetype, ..."))
(l "Riek"	
   (ML Enc "Ein Vorteil von Freier Software unter GPL ist, dass diese die Weiterentwicklung noch stärker fördert als Patente dies tun. Hier wird nicht nur die Idee veröffentlich, sondern eine funktionierende Umsetzung, die auch noch jeder ohne Lizenzgebühren direkt verwenden kann. Insofern hat die Gesellschaft davon mehr als von Patenten.")
   (ML Eme "Ein Kompromiss mit dem heutigen Patentsystem wäre es den Open Source Entwicklungen eine freie Verwendung der Patente zu gestatten.") )
(l "Hössle:"
   (ML Dta "Der [ positive ] Effekt von Patenten findet an anderer Stelle statt.")
)

(l "Nack" 
   (ML ZeW "Zur Frage der Langsamkeit des Patentprozesses: Es gibt einen vorläufigen Schutz. ")
   (ML JWe "Jedem steht weiterhin frei, wie er seine Software schützen will. Er kann dazu nach Wahl eigene Geheimhaltung, den Urheberschutz oder Patente nehmen.")
   (ML Fkn "Für jede Erfindung, auch die kleinen, muss die Frage der Entdeckungshöhe geklärt werden.")
   (ML Ikf "In Bezug auf Patente und Open Source Entwicklungs ist noch anzumerken, dass das Experimentieren im privaten Bereich jedem Weiterhin frei gestellt ist.  Patente schützen nur die gewerbliche Anwendung.") )

(l (tpe "Gehring" "TU Berlin") 
   (ML Oee "Open Source ist gekennzeichnet durch das Entwicklungsmodell und nicht das Ergebniss.")
   (ML Dir "Die Entwicklung Freier Software ist durchaus kommerziell.")
   (ML ScW "Softwarepatente sind nicht mehr zu verhindern.  TRIPS schreibt sie vor.")
   (ahv 'gehring-openpatents (ML Lgs "Lösungsvorschlag"))
   (ML Omo "Offene Patente anmelden damit die Gemeinde ein Portfolio für Verhandlungen hat.")
   (ML DnW "Die Gründung einer Instanz für die Überwachung.")
   (ML DTt "Damit wäre die Technizitätdebatte überflüssig.")
   (ML Den "Die öffentlichen Forschungsanstalten könnten so auch mitmachen. Ansonsten drohten ihnen Einschränkungen.") )
	
(l (tpe "Bernhard Reiter" (commas "FFII" "Intevation Gmbh"))
   (ML Eis "Erstmal möchte ich bemerken, dass es eine anderes Verständnis von Algorithmus und Verfahren bei den verschiedenen Gruppen gibt. Für einen Praktiker und Informatiker ist ein Verfahren grundsätzlich 	ein Algorithmus und damit eine Idee. Damit sind die Beispiele von Herrn Hössle was patentiert werden kann eben nicht klar.")
   (ML ZWn "Zur Beleuchtung weiteren Beispiele aus der Praxis:  Nehmen wir den VITERBI Algorithmus aus dem bekannten Beispiel. Herr Braun baut diesen in sein Betriebssystem ein. Laut Herrn Hössle ist das ohne Problem möglich, weil das Patent für eine Andere Anwendung angemeldet wurde. Nun wird das Betriebssystem mehrmals weitergegeben und irgendjemand baut daraus weitere Software und gibt sie weiter. Nun kommt ein Benutzer auf die Idee für einen anderen Zweck doch wieder Signale über gestörte Kanäle zu senden. Nun ist er auf eine Patentmine in seiner Software getreten. Die kann er aber auch nicht vermeiden, weil sich davon potentiell tausende in seinem Quellkode befinden.")
   (ML Fon "Für Freie Software Entwicklung bedeutetn Softwarepatente ein Minenfeld. Für jede Stunde Softwareentwicklung die gleiche Zeit an Patentrecherche zu investieren ist in der Praxis unmöglich. Und hier ist es wo Softwareentwickler sagen: Softwarepatente sind arg hinderlich.") )
	
(l "W. Riek:"
   (ML WnL "Warum verschliessen wir uns neuen Entwicklungen [ in der Software-Welt ].  Die grossen Unternehmen haben schliesslich keinen Anspruch auf Gewinn. Die Basis für unseren Wohlstand ist das Humankapital. Wir sehen, dass es beim Amazon-Patent und dem LZW-Patent starke Gegenbewegungen gibt. Das zeigt, das alte Rechtsordnung und neue Ökonomie nicht umbedingt zusammen passen.")
   (ML Dih "Das ist eine Frage der Wirtschaftspolitik, also eine politische Frage. Es geht darum, wie das Neuland der neuen Ökonomie abgesteckt wird. Ich verweise auf den Artikel von Eric Raymond der etwa lautet %(q:Inbesitznahme der Geistessphäre) -- %(q:Homesteading the Noosphere) -- aber damit etwas ganz anderes als %(q:Geistiges Eigentum) meint. Wir müssen abschätzen, was für die Weiterentwicklung gut ist.") )
   
(l (tpe "Ebers" (ML TeK "TU Berlin, Schrieb Diplomarbeit über Open Source"))
   (ML DhG "Die Problematik ist durch das Patentrecht nicht zu verstehen. Der Begriff des Geistigen Eigentums enthält Widersprüche.")
   (ML FWh "Freie Software versucht ja auf ihre Weise gerade den Diebstahl an den Ideen zu verhindern.")
   (ML Eet "Es liegt die Philosophie zugrunde, Wissen breit miteinander zu teilen.")
   (ML DeI "Der Aufwand bei der Softwareentwicklung ist nicht die Idee (also der Algorithmus).")
   (ML WWe3 "Warum gibt es Angriffe auf die Freie Software: Es ist ein neues Entwicklungsmodell. Die eigentlichen Attacken auf Freie Software beginnen jetzt erst.")
   (ML Enr "Eine wesentliche Eigenschaft von von Open Source Entwicklung ist die schritteweise und teilbare Entwicklung. Wenige zentrale Patente können die gesamte Bewegung blockieren. Beispiel: LZW im GIF Format, und Software funktioniert eben nicht ohne Standards.")
   (ML Pfw "Patente vergiften die Softwareökologie.") )
	
(l (ML Rka "Rückfrage") 
   (ML WWe4 "Wer verdient denn überhaupt mit Freier Software Geld?")
   (sqbrace (ML EWn "Es werden viele Hände gehoben"))
   (ML Mle "Mit der Dienstleistung an Freier Software.")) 
(l "Prof Lutterbeck"
   (ML Exd "Es gibt grundsätzlich zwei Herangehensweisen an unser Problem: Wir stricken ein wenig an den Bestehenden Vorschriften und der Rechtsprechung herum oder wir ändern etwas grundsätzliches.")
   (ML Dtd "Die Abgrenzung was nun bei Software patentierbar ist, und was nicht, ist mir immer noch nicht klar. Und ich habe alle Patentexperten auf dem Gebiet gehört.")
   (ML Gei "Grundsätzlich haben Patente und das Urheberrecht auch eine Beziehung zueinander, die nicht aussen vor gelassen werden kann.")
   (ML WWu "Wir haben gesehen, wie Softwarepatente als Waffe eingesetzt werden können. Der Konflikt ist anscheinend woanders und nicht einfach nur im bestehenden Recht. Er liegt in der Freiheit der Informationen.") 
   (ML Wdi "Was sollte nun der Wirtschaftminister machen?")
   (ML AeW "Auf jedenfall sollte er dem Entwicklungsmodell der freien Software fördern.")
   (ML DGr "Das Beispiel Microsoft zeigt, welche Gefahren und Sicherheitsrisiken es birgt hier nur auf eine Karte zu setzen.")
   (ML Dcu "Der verfassungrechtliche Zusammenhang ist ebenfalls wichtig.  Möglicherweise braucht die Informationsgesellschaft Absicherungen auf Verfassungsebene wie z.B. die unbedingte Freiheit, selbst erarbeitete Informationen weiterverbreiten zu dürfen.") )
	
(l "Tauchert" 
   (ML Weo "Wir Patenrechtler wissen wovon wir reden, aber ich bin mir beim Auditorium nicht sicher ob das Patentsystem genügend gut gekannt wird.")
   (ML Aip "Andererseits gibt es schon berechtigt den Anspruch, dass alles transparent und verständlich sein sollte.")
   (ML WWc "Was den Kompromiss der Bereitstellung von Open Source Software [ oder Patenten für Open Source Software ?] angeht, so betreten wir Neuland.")
   (ML VWz "Vor zehn Jahren gab es einmal den Ansatz ein eigenes Gesetz für den Schutz von Software zu entwicklen. Was ich immer befürwortet habe.") )
	
(l (tpe "Johannes Ullbricht" "FITUG")
   (ML Ier "Ich habe mir Gedanken gemacht, wie die beiden Welten zusammengehen könnten. Eine Möglichkeit wäre das Verfahren zu ändern. Z.B. das System der Zwangslizenz erneut einzuführen.  Entwickler könnten dann beantragen, für eine Technologie, die für die Interoperabilität notwendig ist, eine Zwangslizenz zu erhalben.") )	

(l "D. Riek" 
  (ML Wel "Was heisst eigentlich gewerbliche Nutzung?")
  (ML IWS "Ist das ins Netz stellen von Programmen nicht unter Umständen schon gewerbliche Nutzung?  Schliesslich wird damit indirekt Geld verdient.")
  (ML Wes "Weiterhin kann es einen Patenportfolio der Freien Software nicht geben, weil es nich eine wirkliche Interessensgemeinschaft geben wird, welche die Freie Softwaregemeinde hinreichend vertritt. Gerade bei dieser Gemeinde gibt es einen verwurzelten Widerstand gegen eine solchen institutionalisierte Organisation.")
  (ML Msh "Mit dem LIVE-Verband versuchen wir auch eine Interessenvertretung aufzubauen. Alle werden wir aber nicht vertreten können, wie ich gerade erläutert habe.")
  (ML Doi "Die neue Software und Informationen passen einfach nicht mit vielen der alten Regeln zusammen.") )
	
(l "Nack" 
   (ML VKD "Vor 129 Jahren gab es, gar nicht weit von hier eine Diskussion, die mit der heutigen viele Ähnlichkeiten hat. Es nahmen die Siemensbrüder teil und auch hier wurde über die Einführung von Patenten diskutiert. x Jahre später [ Herr Nark sagte die Zahl ] gab es dann eine ähnliche Diskussion über Patente in der Chemie. Gott sei Dank ging die Patentierbarkeit von Biologischen Prozessen [?] dann relativ leicht durch. Das hinter natürlich nicht daran, das diesmal nochmals neu zu diskutieren.")
   (ML Wue "Was die Neuheitsschonfrist angeht, die oft erwähnt wurde, so bedeutet sie nur, dass es schon eine Vorbenutzung des Verfahrens gegeben haben darf. Es heisst nicht, dass in der Schonfrist andere Gruppen das Patent nutzen dürfen.") )

(l (tpe "Herr Nemeth" "Suse Linux AG")
   (ML Wsn "Wenn ich auf die Entwicklungsgeschichte von  Freier Software blicke, so stelle ich fest, dass das GNU Projekt zum Ziel eine Nachbildung eines Betriebssystem war. Auch Linux ist eine Nachbildung des Unix-Kerns.") 
   (ML Hnr "Hätte schon früher Softwarepatente gegeben, dann wären auch viele Unix-Techniken patentiert worden, und GNU/Linux hätte nie entstehen können.")
   (ML Dne "Die GNU-Programme war natürlich dennoch nicht nur Nachbildungen, sondern sie waren auch verbessert und sicherer gemacht.")
   (ML Piu "Patente stehen eindeutig im Widerspruch zur Förderung dieser Entwicklungen.") )
(l "Bogk"
   (ML Cee "Computer sind Universalwerkzeuge. Sie helfen uns also beim Denken. Softwarepatente sind dann also verbotene Gedanken.")
)

(l "Gerwinski:"
   (ML Esf "Erstmal möchte ich klarstellen: Open Source Software ist kommerziell.")
   (ML Des "Der Widerspruch ist in unserer Runde klar zu erkennen: Es gibt entweder Softwarepatente oder Freie Software. Die Kreuzlizensierung (die hier mehrmals vorgeschlagen wurde) ist nur bei grossen Firmen machbar. Softwarepatente fördern also die grossen Unternehmen.")
   (ML Rns "Recherchieren ist angesichts der Massen an neuen Softwarepatenten unmöglich. Die Rechtsabteilung eines Softwareunternehmens müsste zehnmal so gross sein, wie die Entwicklungsabteilung.")
   (ML AWP "Auch ich bin schonmal auf eine solche %(q:Patentmine) getreten.")
   (ML Wln "Weiterhin ist mir nicht klar, was die bisher erwähnte Schonfrist sein soll.")
)	
(l "Pilch"
   (ML DAW "Die Schutzbündnis-Idee ist einer von vielen möglichen Ansätzen und auch nicht neu.  s. Stallman 1991: Mutual Defense.  Das hat aus bekannten Gründen nicht geklappt.")
   (ahs 'swpbpfaru (ML EWa "Es gibt nun mehrere Wege auf denen etwas getan werden kann."))
   (ML Ktt "Keiner dieser Vorschläge widerspricht TRIPS.  Aber es gibt einen Gedanken, der zumindest dem Geist von TRIPS widerspricht, den ich hier aber trotzdem vortragen möchte:  unsere europäische Softwarebranche zieht im internationalen Patent-Schach den Kürzeren.  Das bedeutet einen unmittelbar spürbaren Verlust an Steuergeldern.  Steuerartige Abgaben werden an amerikanische Großunternehmen statt in unseren Bildungsetat fließen.")
   )

(l "Ebers:" 
   (ML EiW "Es gibt viele Ideen die aus der Open Source Welt kommen.  Ein bisher nicht angesprochener Problembereich ist die Haftung.  Von Patenten gehen rechtliche Risiken aus, die auch die Universitäten betreffen und das Umfeld insbesondere der freien Software verunsichern. Das hilft vor allem den Rechtsanwälten.")
   (ML Ebi "Es geht hier schliesslich um Qualität. Und die spielt geselltschaftliche eine Rolle. Wenn alles über Computer läuft, geht es auch um die %(q:virtuellen Recht); Patente sind eine empfindliche Freiheitseinschränkung.  Sie rühren an grundlegende Rechtsgüter, die noch ungenügend gewürdigt werden.")
)

(l "Reiter"
   (ML EhW "Es wurden heute einige Kompromissvorschläge gemacht. Die würden einen Schaden durch Softwarepatente kleiner halten, es sollte aber klar sein, dass Softwarepatente insgesamt ein grosses Problem für die Softwarebranche sind. Die meisten kleinen und mittleren Softwareunternehmen wollen deshalb eindeutig keine Softwarepatente.")
   (ML SWW "Software ist vergleichsweise neu und die Freie Software bringt ein völlig neues Geschäftmodell. Es kann sehr gut sein, dass hier Technik und Fortschritt völlig anders funktionieren und Patente nun tatsächlich hinterlich sind, was von Softwarepatentvertretern noch nicht in Betracht gezogen wurde.") )

(l (tpe "Arnim Rupp" "FFII")
   (ML IWj "Ich möchte darauf aufmerksam machen, dass das Patentrecht im Softwarebereich kaum jemandem außer den Juristen zugute kommt.") )
	
(l "D. Riek:"
   (ML Iht "Ich spreche für die Softwareunternehmen des LIVE Verbandes: Für uns ist die Lösung: Keine Softwarepatente.") 
   (ML Dau "Die zweitbeste Lösung wäre, dass alles Softwarepatente für Open Source frei verwendet werden können.")
   (ML Ene "Erst dann stehen an dritter Stelle weitere Kompromisse.") )
	
(l (tpe "Matthias Schlegel" "Phaidros AG")
   (ML Wvz "Wir sind ein herkömmliches Softwareunternehmen. Wir stellen Komponenten für betriebswirtschaftliche Systeme her.  Dabei können wir unzählige Patente verletzen.  Schon jetzt beunruhigt das unsere Kunden und verursacht uns Kosten.  Patente bremsen erstmal. Als Konsequenz muss ich im Geschäftsplan ein paar Millionen zurückstellen, um Angriffen durch Konkurrenz auf Basis von Patenten begegnen zu können.")
   (ML Dev "Damit ist für uns klar, dass Patente eine Behinderung der Entwicklung von Wirtschaftssoftware darstellen.") )
	
(l "Weber-Cludius:"
   (ML Hlt "Herr Bogk: Selbstverständlich werden wir das Patentsystem nicht abschaffen.")
   (ML San "Sie sehen beim Patentsystm auch, dass Imitatoren andere Möglichkeiten schaffen müssen und das auch tun.")
   (ML AiW "Auch die Kreuzlizensierung wird schon praktiziert. Es gehört eben einfach dazu sich in diesem Sinne wettbewerbsfähig zu verhalten.")
   (ML Wrt "Was die Neuheitsschonfrist angeht, so ist sie hauptsächlich für Wissenschaftler gemacht worden, die erstmal Artikel veröffentlichen wollen und dann später das Patent vollständig anmelden.")
   (ML Zee "Zur Recherchierbarkeit: Hier gibt es natürlich Grenzen durch Praktische Problem.")
   (ML EWs "Es ist durchaus erlaubt den grundsätzlichen Ansatz bei Softwarepatenten in Frage zu stellen. Schliesslich ist der Rechtsrahmen auch nicht Gott-gegeben.")
   (ML Iad "In einer Aufsehen erregenden Veröffnetlichung hat ein Herr Turow [?] gefordert, dass zwei bis drei verschiedene Schutzformen entwickelt werden.")
   (ML Htd "Hier müssen auch noch prizipielle Überlegungen angestellt werden: Sollen Geschäftsmodelle patentiert werden. Denn diese sind sicher nicht technischer Natur.")
   (ML Eui "Ein interessanter Lösungsansatz ist auch die Zwangslizensierung.") )
	
(l "Soquat"
   (ML WAn "Wie sie sehen interessiert sich die Bundesregierung für diese Thema. Und wir setzen die Arbiet fort. Es gibt natürlich auch innerhalb der Bundesregierung verschiedene Interessen und Meinung, dass ist ein völlig normaler Prozess, wie sie ihn hier auch gesehen haben.")
   (ML IPm "Ich werde jetzt deshalb keine weiteren Schlussfolgerungen von uns [ gemeint ist das Podium ] anschliessen. Bitte schreiben sie weiter Pressemitteilungen und Stellungnahmen und schicken sie uns. Wir werden sie dann auf unseren Webseiten veröffentlichen.")
   (ML Wbs "Weiterhin werden wir uns überlegen, wie wir diese Diskussion weiter fortsetzen.") )
)	
)
; swpbmwi25-dir

(list 'swplxtg26-dir
(mlhtdoc 'swplxtg26 (ML LWk "FFII-Programm auf dem LinuxTag 2000")
(ML Fgi "Der FFII e.V. informierte auf dem LinuxTag 2000 in Stuttgart über neuere gesetzeswidrige Praktiken der deutschen und europäischen Patentjustiz sowie deren Pläne, Programmlogik und damit wirtschaftliche und gesellschaftliche Verfahren aller Art umfassend patentierbar zu machen." "FFII is present at this years largest German Linux event to give the fight against software patents a boost")
nil
(filters ((lf ahs 'lxtag-ffii) (lc ahs 'lxtag-coxffii) (sp ahs 'swpat)) (let ((LT (ahs 'linuxtag))) (ML Dme "Der %{LT} fand dieses Jahr vom 29. Juni bis zum 2. Juli 4 Tage lang auf dem Stuttgarter Messegelände statt und wurde von ca 20000 Menschen besucht.  FFII informierte an Stand 6.0.1.14 %(lc:auf Einladung der Veranstalter) über das Thema %(sp:Softwarepatente)." "The %{LT} is scheduled for June 29 to July 2 (4 days) in Stuttgart and expects 20000 visitors.  The FFII is %(lf:announced as an exhibitor).  %(lc:The organisers place great hopes in us), which we won't disappoint.")))
(ML Foi "Wir führten ein Veranstaltungsprogramm durch, das viele interessante Erkenntnisse zutage förderte:" "We conducted the following program:")
(sects
(stand (ML Dce "Durchgehend am FFII-Stand" "Throughout at the FFII booth") 
   (ul
    (filters ((ep ahs 'eurolinux-petition)) (ML UeW "Unterschreiben der %(ep:Eurolinux-Petition gegen Softwarepatente)"))
    (filters ((ep ahs 'eurolinux-nopatltr)) (ML VpW "Verteilung von %(ep:voraddressierten Petitionsbriefen)" "Distribution of %(ep:pre-addressed petition letters)"))
    (filters ((pd ahs 'swpatvreji)) (ML Vvk "Vorführung von %(pd:Dokumentation)" "Presentation of %(pd:documentation)"))
    (filters ((cd ahs 'swpdokucd)) (ML Vek "Verteilung unserer %(cd:SWPAT-Dokumentations-CD)." "distribition of our %(cd:SWPAT documentation CD)."))
    (ML Vtr "Verteilung von Anti-SWPAT-Aufklebern und Flugblättern")
    (let ((LST (kaj (slashes (ahs 'egeld "EGeld") (ah "http://www.nerdbank.org" "Nerdbank")) (ahs 'wortbasar)))) (ML Ine "Information über andere FFII-Projekte wie z.B. %{LST}." "Information about other FFII projects such as %{LST}.")) ) ) 
;; Halle 6 des Stuttgartermessegeländes unter der StandNr.6.0.1.40

(preleg (ML Vmz "Vortragsprogramm im Kongresszentrum A Saal 1")

(sects
(tauchert 
 (colons "Tauchert" (ML Tee "Softwarepatentierung -- Praxis des Deutschen Patentamts"))
 (dl 
  (l (ML Prep "Vorbereitende Texte" "Preparatory Texts") (ahs 'swplxtauch (ML Taus "Auszug aus der vorbereitenden Diskussion über Herrn Taucherts Vortragszusammenfassung")))
  (l (ML Zeit "Uhrzeit" "time") (spaced (ML Fri "Freitag, 30. Juni" "Friday 2000-06-30 ") "11.00-12.00"))
  (l (ML Rde "Redner" "speaker") (commas (spaced "Dr." (famvok "Tauchert" "Wolfgang") (ML Lrg "Leiter der Abteilung fuer Datenverarbeitung und Informationsspeicherung am Deutschen Patent- und Markenamt" "Head of the Department for Data Processing and Information Storage at the German Patent and Trademark Office"))))
  (l (ML Ihl "Inhalt" "Contents")
     (ML Dye "Das Deutsche Patentgesetz (PatG) verbietet in §1 die Patentierung von Computerprogrammen als solchen.  Daran, dass der Gesetzgeber die Einschränkung %(q:als solche) macht, erkennt man, dass er dieses Patentierungsverbot als problematisch ansieht.  Die Ausnahmenliste in §1 ist unsystematisch formuliert und schwer anwendbar.  Auch die Einschränkung auf Gegenstände %(q:als solche) erlaubt zahlreiche Interpretationsmöglichkeiten. Der Bundesgerichtshof (BGH) hat daher von jeher nicht den §1 direkt angewendet, sondern mit dem Begriff der %(q:Technizität) operiert, der sich indirekt aus den folgenden PatG-Paragraphen als das (neben Neuheit und Erfindungshöhe) zentrale zugrundeliegende Abgrenzungskriterium begründen lässt.  Diesen Begriff definiert das PatG zwar nicht, aber hat der BGH hat eine viel zitierte klare Definition geschaffen:")
     (blockquote (ML pbc "planmäßiges Handeln unter Anwendung beherrschbarer Naturkräfte ohne zwischengeschaltete menschliche Tätigkeit"))
     (ML AlP "Anfang der 90er Jahre lehnte der BGH Anträge auf Erteilung von Softwarepatenten wegen mangelnder Technizität ab.  Im Fall %(q:Chinesische Schriftzeichen) bemängelte der BGH, dass der Kern des Erfinderischen nicht in der Beherrschung von Naturkräften (Elektronen) sondern in der Programmlogik liege, da das von Siemens zur Patentierung vorgelegte Textverarbeitungsprogramm vom Computer nur den bestimmungsgemäßen Gebrauch mache.  Man nannte diese Denkweise des BGH die %(q:Kerntheorie).")
     (ML Dlo "Die Kerntheorie löste bei den interessierten Fachkreisen heftige Kritik aus.  Mitte der 90er Jahre wandte sich der BGH mit der Entscheidung %(q:Tauchcomputer) von der Kerntheorie ab und hob auf die Verwendung des Programms ab.")
     (ML Ain "Auf dem Münchener Kongress der UNION Ende 1997 argumentierten die versammelten Patentrechtler überzeugend, dass jedes funktionierende Computersystem technisch sei, und dass die Frage der Technizität und der Erfindungsleistung nicht vermengt werden dürfe (d.h. dass der erfinderische Kern nicht im technischen Bereich liegen müsse).  Wenig später schwenkten das Europäische und das Deutsche Patentamt auf diese Sichtweise um, und der 17te Patentsenat gab seine restriktive Haltung auf.")
     (ML Itu "Im Juli 1999 urteilte der BGH, dass ein programmgesteuerte System zur Bestimmung von Verkaufspreisen grundsätzlich patentfähig sei.  Meine Abteilung hatte den Patentantrag zunächst zurückgewiesen.  Der Kollege, der ihn zurückwies, hing nämlich noch der Kerntheorie an.  Der BGH verwarf aber in einem Grundsatzurteil die Kerntheorie und verwies den Patentantrag (in Form eines verengten Hilfsantrages) an unsere Abteilung zur erneuten Beurteilung zurück.")
     (ML IbW "In zwei neuen Urteilen zum Thema %(q:Sprachanalyse) und ... ging der BGH sogar noch weiter.  Er schuf den Begriff der %(q:programmtechnischen Vorrichtung).  Damit ist nunmehr jedes Computerprogramm patentrechtlich schützbar.  Wir im DPMA hätten auch nicht gedacht, dass der BGH so weit gehen würde.  Damit kehrt der BGH zu einer Haltung zurück, die er in den 60er Jahren bereits einmal eingenommen hatte, bevor die Mathematikerfraktion eine restriktive Rechtsauffassung durchsetzte und dafür sorgte, dass Programme lange Zeit nicht direkt patentiert werden konnten.  Es handelt sich hier um einen politischen Sieg der ingenieurwissenschaftlich orientierten Fraktion.") )
  (l (ML Frag "Antworten auf Fragen und Einwände")
     (sects-without-menu 
      (jur
       (colons "WRiek" (ML DdK "Dürfen ein paar Patentjuristen auf diese Weise Normen grundlegend verändern?  Sind das nicht politische Fragen, die über die Kompetenz der Gerichte und Patentämter hinausgehen?"))
       (ML DWS "Die Rechtsprechung hat das Recht, das Gesetz zu interpretieren.  Natürlich ist das auch eine politische Angelegenheit.  Die ingenieurwissenschaftliche Sicht hat sich durchgesetzt.  Dabei geht es aber nicht um materielle Interessen einer Gruppe, sondern darum, unerträgliche rechtssystematische Widersprüche aus dem System zu entfernen.  Wir haben die jetzigen Entscheidungen des BGH als eine Befreiung empfunden.  Ob außerdem das EPÜ geändert wird, ist völlig zweitrangig, denn wir haben bereits ein schlüssiges System.  Wenn Sie als Nicht-Juristen da nun Einfluss ausüben wollen, müssen Sie erst einmal das System verstehen.  Ihre Forderung, das Programmierungsverbot für Computer aufrecht zu erhalten, führt nicht zum Ziel.  Im Gegenteil, erst wenn es abgeschafft wird, kann es für Sie neue Chancen geben, die Gesetzgebung in Bewegung zu bringen und Ihren Vorstellungen Gehör zu verschaffen.") )
      (oek
       (colons "phm" (ML Iao "Ist es wirklich in Ordnung, wenn man Software patentierbar macht, ohne vorher eine systematische Studie der ökonomischen Auswirkungen einer solchen Änderung zu erstellen?"))
       (ML SWn "Selbstverständlich.  Wir brauchen keine ökonomische Studie.  Die Wirklichkeit spricht für sich.  Der Markt hat das Urteil bereits gesprochen.  Bei uns gehen jedes Jahr Tausende von Anträgen auf Softwarepatente ein, und unser Patentsystem wirft Gewinne ab.  Es ernährt ohne staatliche Zuschüsse 20000 Patentspezialisten.")
       )
      (raub
       (colons "Siepmann" (ML KWt "Könnte man nicht auf gleiche Weise auch argumentieren, ein Räubersyndikat sei legitim, da es ja auf dem Markt gesiegt habe und ohne staatliche Zuschüsse auskomme?"))
       (ML Niu "Nein.  Das Räubersyndikat handelt rechtswidrig, wir nicht.  Wir stützen uns auf Grundsatzurteile des BGH.  Der BGH ist befugt, die Gesetze zu interpretieren, das Räubersyndikat nicht.")
       )
      (pres
       (colons "phm" (ML Hia "Herr Tauchert, Sie sprechen hier sehr grundlegende Wahrheiten aus, die man selten in dieser Deutlichkeit zu hören bekommt.  Dürfen wir das, was Sie gerade gesagt haben, in unserer Presseerklärung zitieren?"))
       (ML Snu "Selbstverständlich.  Dazu stehe ich.")
       )
      ) )
;dl
)
;tauchert
)

(spring 
 (colons "Springorum"  (ML DiK "Zähmung des Patent-Ungeheuers -- Konzept eines Schutzbündisses" "Taming the Patent Monster -- Conception of a Mutual Defense Alliance"))
 (dl
  (l (spaced (mlval 'Fri) "15.00-16.00"))
  (l (mlval 'Rde) "PA Dipl.Inf. Dr. Harald Springorum")
  (l (mlval 'Ihl)
     (ahs 'swpatsarxe (ML SeW "Seit ca 1990 denken Programmierer Freier Software und andere Softwarepatent-Geschädigte über die Möglichkeit nach, Bündnisse zum gegenseitigen Schutz von Patenten zu schließen.  Ein solches Bündnis würde selber Patentrechte erwerben und als Verhandlungsmasse zum Schutz von %(q:geistigem Gemeineigentum) einzusetzen."))
     (ML HgD "PA Springorum stellte hierzu ein Modell vor, über dessen Machbarkeit später gewinnbringend diskutiert wurde.  An der Diskussion beteiligten sich vor allem Wolfgang Tauchert (DPMA), Swantje Weber-Cludius (BMWi), Peter Gerwinski (G.N.U. GmbH), Werner Riek (Journalist) und Hartmut Pilch (FFII).")
     (ML PaZ "PA Springorum schlug vor, zunächst solle ein Verein Softwareentwicklern Überblick über drohende Patentgefahren schaffen und gegen vermeidbare Softwarepatente Einspruch einlegen.  Ferner könnte dieser Verein in Zusammenarbeit mit einer Versicherungsgesellschaft Rechtsschutzversicherungen gegen Patentrisiken anbieten.")
     (ML Dtt "Daneben solle eine nicht-gewinnorientierte Gesellschaft (Pool GmbH oder Pool AG, da gewerblich tätig) gegründet werden, die für die freien Softwareentwickler Erfindungen zum Patent anmeldet und zum Gebrauch in freier Software freigibt, aber gleichzeitig denjenigen den Gebrauch verbietet, die freie Software mit Patenten angreifen.")
     (ML Srs "Schwierigkeiten könnte es beim Kartellamt geben, besonders wenn die Pool AG potente Mitglieder wie IBM als Gesellschafter gewinne.  Aber diese Schwierigkeiten müssten sich überwinden lassen, da es ja gerade nicht um die Schaffung von Kartellen sondern um das Gegenteil davon gehe.")
     (ML Srs2 "Es gibt bereits mögliche Vorbilder, nämliche eine Kölner Schutzvereinigung für den fairen Gebrauch von Rundfunkrechten.  Deren Satzung legte PA Springorum vor.")
)
  (l (mlval 'Frag)
     (sects-without-menu
      (restr (ML JWr "Juristen begehen häufig den Fehler, die Ausschlusskraft restriktiver Gemeineigentumslizenzen (Copyleft-Lizenzen wie GNU GPL) zu überschätzen.  Solche Lizenzen taugen nur in sehr geringem Maße als Faustpfänder.  Wenn ein Programmierverfahren erst einmal in GPL-Software implementiert ist, kann man niemanden mehr daran hindern, dieses Verfahren zu verwenden.") (ML DWl "Dann muss man die GPL eben so ändern, dass das geht.  Wer die GPL zur heiligen Kuh erhebt, ist selbst schuld, wenn er sich dann nicht gegen Patente wehren kann."))
      (modul (ML Sfr "Selbst angenommen, wir erfinden eine neue Allgemeine Öffentliche Lizenz, die es einer bestimmten Pool AG erlaubt, die GPL als Waffe einzusetzen, dann ist das noch immer eine stumpfe Waffe.  Denn freie Software steht im Internet offen zur Verfügung.  Jeder kann sie verwenden, egal ob legal oder nicht.  Wenn wir z.B. Microsoft verbieten, ein bestimmtes Programmierverfahren zu verwenden, muss Microsoft lediglich seine Software so schreiben, dass für das entsprechende Verfahren ein unter GPL stehendes Modul (z.B. über Corba) aus dem Internet geladen wird.") (ML Tja "Das könnte Microsoft aber schon genügend weh tun."))
      (patfund (ML Wse "Was tun wir, wenn unser Gegner eine Patentverwertungsgesellschaft ist, die selbst überhaupt keine Programme schreibt, sondern lediglich jede Woche mehrere Patente erzeugt, hinzukauft und den Meistbietenden (eventuell exklusiv) lizenziert?") (ML GWn "Gegen solche Gesellschaften wäre unsere Pool AG machtlos, es sei denn sie hätte genug Geld, um alle großen Konzerne zu überbieten.  Wir können nur hoffen, dass das nicht der typische Fall ist."))
      (anarch (filters ((sx ahs 'swpatsarxe)) (ML Due "Die Welt der freien Software besteht aus vielen einzelnen Programmierern, denen es gegen den Strich gehen würde, sich in einem Verein oder einer Pool AG generalstabsmäßig zu organisieren.  Unter den kleinen und mittleren Unternehmen, die auch als Teilnehmer in Frage kämen, gibt es wiederum sehr viel konkurrenzorientiertes Denken und wenig Bereitschaft, im Interesse der Solidarität Opfer zu bringen.   Der bisher geringe Erfolg %(sx:zahlreicher vergleichbarer Versuche in den USA) scheint diese Sicht zu bestätigen")) (ML SWe "Selbstverständlich sollte die Mitgliedschaft in dem Verein und der Pool AG auf Freiwilligkeit beruhen.  Man braucht auch unbedingt eine gute Initialzündung.  Eine Firma wie SuSE, die ja letztlich die Früchte der Arbeit unzähliger kostenlos arbeitender Programmier abschöpft, sollte da 1 Million DM investieren."))
      (aufwand (ML Diu "Der Arbeitsaufwand ist so gewaltig, dass selbst eine personell gut ausgestattete Pool AG keine guten Erfolgsaussichten hätte.  Sie müsste nämlich hunderttausende von Programmzeilen auf mögliche Verletzungen von hunderttausenden von Patenten absuchen.  Außerdem müsste sie unentgeltlich arbeitende Programmierer dazu motivieren, ihre Werke auf möglicherweise patentierbare Trivialitäten abzusuchen und diese zu dokumentieren.  Es ist bekannt, dass echte Programmierer ungern Dokumentation schreiben.  Schon gar nicht Patentdokumentation, die wiederum eine besondere Erfahrung erfordert.  Um eine Patentschrift zu schreiben, muss man vielleicht kein Jurist sein, aber immerhin erfordert das viel Mühe und Erfahrung, die man bei freien Entwicklern nicht antrifft, und die Personalkosten von mindestens mehreren 100000 DM monatlich bei der Pool AG erfordern würde, wenn sie ihre Aufgabe wenigstens zu 10% bewältigen will.") (ML NiW "Nicht unbedingt.  Man muss die Entwickler schulen.  Wenn die OpenSource-Gemeinde auf ihren Gewohnheiten beharren und sich nicht vom Fleck bewegen will, ist sie selber schuld."))
;sects-without-menu      
)
) ) ) 

(smets (colons "Smets" (ML STS "Software Patent Tactics for OpenSource Developpers"))
  (dl
   (l (mlval 'Zeit) (spaced (ML Sat "Samstag, 1. Juli" "2000-07-01") "16.00-17.00") )
   (l (mlval 'Prep) (ahs 'smets-gppl))
   (l (mlval 'Frag)
      (sects-without-menu
       (anreiz (ML Dti "Die Smetssche GPPL ist gegen Patentverwertungsfirmen machtlos.  Somit wirkt sie, falls sie erfolgreich wird, auf eine weitere Konzentration von Patentrechten in den Händen von Patentverwertungsfirmen hin.") (ML Jne "Ja, zweifellos.  Wenn Microsoft seine Patente an eine Patentverwertungsfirmat verkauft und dann von dieser lizenziert, sind wir mit der GPPL machtlos.  Es sei denn wir haben diejenigen Verfahren patentiert, mit denen Patentverwertungsfirmen und Patentanwaltsbüros arbeiten müssen."))
       (freipat (ML Evw "Es gibt aber eine alternative Freipatentierungslizenz (GPPL), mit der wir auch gegen Patentverwertungsfirmen vorgehen könnten.  Wir könnten eine Freipatentierungslizenz (GPPL) schaffen, die es verbietet, unsere Patente im Zusammenspiel mit proprietären Patenten zu verwenden.  D.h. überall dort, wo auf einem Rechner ein proprietär patentiertes Verfahren zum Einsatz kommt -- darf unser Verfahren nicht verwendet werden.  Und zwar bedingungslos.  Öffentliche Patente schützen offensiv das Gemeineigentum.  Darüber kann, genau wie auch im parallelen Fall des GPL-Urheberrechts, keine Privatperson verhandeln.  Da im Patentwesen mit härteren Bandagen gekämpft wird, muss die GPPL härter zuschlagen als die GPL.  Durch eine solche harte Vorgehensweise könnten wir die privaten Patente entwerten und das Geschäft der Patentverwertungsfirmen austrocknen.  Denn wer wird noch eie Patentlizenz kaufen, wenn die damit patentierte Technik in fast keiner Situation mehr legal eingesetzt werden darf?") (ML Daa "Das wäre tatsächlich ein folgerichtiger Entwurf, mit dem man auch die Patentverwertungsfirmen trifft.  Allerdings bräuchte ich mehr Zeit, um die praktischen Folgen einer solchen Strategie zu Ende zu denken."))
;sects
       )
      )
   ) )

(far (colons "FFII" (ML Sbc "Softwarepatent-Lobbyarbeit:  Was wurde erreicht und was ist zu tun?"))
  (dl 
   (l (mlval 'Rde) (kaj "Hartmut Pilch" "Holger Blasum"))  
   (l (mlval 'Ihl) 
      (ML RrW "Die Patentjustiz hat sich selbst Regeln geschaffen, die sowohl von ihrer Form als auch von ihrer Wirkung her im Gegensatz zum Buchstaben und zum Geist der geltenden Gesetze stehen.  Mit seiner Recherche- und Dokumentationsarbeit konnte der FFII dies klar belegen und dadurch den Argumentationsspielraum der Patentexpansionisten stark einengen.  Unsere Gespräche in Berlin und auf dem Linuxtag haben das deutlich gezeigt.  Die Patentexpansionisten haben auch viel weniger Rückhalt in der Wirtschaft als sie es gerne glaubhaft machen:  die Front verläuft zwischen Informationstechnikern und Patentexperten, nicht zwischen freier und proprietärer Softwareindustrie.  Wir rennen überall, sogar bei Großunternehmen wie Siemens, offene Türen ein.  Das macht Spaß, und es gibt sehr viel zu tun, z.B.:")
      (ul
       (ML Eeu "Erstellen und Versand von Aufklebern und bedruckten Gegenständen aller Art, die auf unsere Petition und Dokumentation hinweisen.")
       (ML Fds "Fortsetzung von Holgers Arbeit des Sammelns und Digitalisierens einschlägiger Papierdokumente.")
       (filters ((sp ahs 'swpatpapri)) (ML Von "Veröffentlichung von %(sp:Rezensionen dieser Dokumente)."))
       (filters ((cd ahs 'swpdokucd)) (ML WgW "Weiterentwicklung der bisherigen %(cd:Dokumentations-CD)"))
       (tpe (ML Gen "Gezieltes Ansprechen von Firmen und Politikern.") (ML Ehu "Einige Diskussionsteilnehmer haben gute Drähte zu bestimmten Unternehmen."))
       (ML Sei "Suche nach Ökonomen mit informationstechnischen Kenntnissen, die weitere Wirkungsstudien veröffentlichen könnten.")
       (ML Wnt "Weitere offene Briefe an verschiedene Institutionen, die uns helfen oder ihre bisherige Linie ändern könnten.  Z.B. könnten wir Siemens dazu veranlassen, sich von dem bisherigen Pro-SWPAT-Auftreten des Herrn Körber zu distanzieren und stattdessen eine Stellungnahme zu veröffentlichen, die zu unvoreingenommener Erforschung der ökonomischen Wirkungen rät.")
       (tpe (ML Auf "Anmelden von Patenten und Gebrauchsmustern unter Allgemeiner Öffentlicher Patentlizenz (GPPL).") (ML DbW "Die Gebühren für Gebrauchsmusteranmeldung beim Deutschen Patentamt liegen bei ca 100 DM.  Man kann außerdem Anteile an den Rechten verkaufen.  Jeder Teilinhaber kann dann den Rechtstitel gegen beliebige Dritte durchsetzen.") )
       (ML FWh "Formulieren von möglichen Allgemeinen Öffentlichen Patentlizenzen und durchsetzung günstiger Gebührenbedingungen für diese Art von Lizenz beim Gesetzgeber und im Rahmen des BGH-Fallrechts.  Weitere Gesetzesvorschläge, z.B. zur prinzipiellen Nichtgewerblichkeit von GPL-Software.")
       (ahs 'swpatpurci)
       ) ) ) 
;far
) 
;sects
)
;preleg
)

(etc (ML Nnp "Nebengespräche" "Sidetalks")
(sects
(mosdorf "Siegmar Mosdorf"
(ML Irl "Im Anschluss an seine Eröffnungsansprache äußerte Wirtschafts-Staatssekretär Siegmar Mosdorf (SPD) in einem Handelsblatt-Interview, er nehme die Ängste der Informatiker vor den Brüsseler Patentplänen sehr ernst.  %(q:Wir wollen den Innovationsprozess erhalten.  Open Source muss möglich bleiben).  Man müsse aber auch die andere Seite sehen: %(q:Autorenschutz ist wichtig.  Wer etwas entwickelt hat, muss auch etwas davon haben.  Aber möglichst viel Offenheit ist ebenfalls wichtig).  Zunächst gelte es abzuwarten, was die europäische Diskussion und die anschließenden Verhandlungen mit den USA ergeben.   %(q:Das wird eine der schwierigsten Diskussionen überhaupt), meint Mosdorf sorgenvoll.")
(lin (ML Dce2 "Diese enigmatischen Aussagen werfen Fragen auf:")
(ul 
(lin (ML VtP "Verwechselt Mosdorf, wenn er sich für den %(q:Autorenschutz) einsetzt, möglicherweise Patentrecht und Urehberrecht?  Unterliegt er dem Missverständnis, die %(q:OpenSource-Bewegung) wolle alle Programmierer zur Preisgabe ihrer Rechte zwingen?  Treffender wäre folgende Formulierung gewesen:")
 (blockquote (ML Als "Autorenschutz (Urheberrecht) ist wichtig.  Wer etwas entwickelt hat, muss auch etwas davon haben.  Möglichst viel Offenheit ist ebenfalls wichtig.  Softwarepatente schaffen ein Minenfeld, welches die Investitionsrisiken für Softwarefirmen erhöht und insbesondere die Autoren offener Software stark gefährdet.")) )
(ML Udu "Um welche %(q:europäische Diskussion) und welche %(q:Verhandlungen mit den USA) geht es hier?  Unserer Kenntnis nach steht eine Diskussion der Fachminister an, für die in Deutschland das Bundesjustizministerium zuständig ist.  Dieses hält sich bisher jedoch bedeckt und scheint unserem Einruck nach entschlossen, die EU-Pläne mitzutragen.  Von einer anstehenden Diskussion mit den USA hat der FFII noch nichts erfahren.")
)
)

(ML IdW "In dem Handelsblatt-Gespräch kündigt Mosdorf ferner die Errichtung eines %(q:Kompetenzzentrums für die Förderung Freier Software) an, das Software-Entwicklern als Diskussionsforum und Marktplatz dienen soll.  %(q:Außerdem soll es der Ausbildung und Qualifikation dienen und Unternehmensberater, Handels- und Handwerkskammern mit einbinden.  Ziel ist, die OpenSource-Gemeinde zu stärken und zu verbreitern).")

(filters ((ol ahs 'osslaw-articles)) (ML GeW "Gleichzeitig lehnt Mosdorf jedoch eine ominöse %(q:gesetzliche Regelung nach französischem Vorbild) ab.  Damit dürfte die die französische %(ol:Gesetzesinitiative für offene Software-Standards) gemeint sein, die Staatsorgane und Träger öffentlicher Funktionen dazu verpflichten soll, bei der Kommunikation mit dem Bürger offene Standards einzusetzen und darüber hinaus Quelltexte verwendeter Programme zu hinterlegen.  Mosdorf begründet seine Ablehnung dieser französischen Regelung mit der Bemerkung: %(q:Wir sehen die Rolle des Staates vor allem als die eines neutralen Moderators)."))

(ML Dmn "Dem FFII erscheint dies widersprüchlich.  Um ein neutraler Moderator sein zu können, muss der Staat sich zunächst auf offene Kommunikationsstandards verpflichten lassen.  Das Hinterlegen von Quelltexten hat wiederum nichts mit Quellenoffenheit zu tun:  es ist eine Minimalforderung, die lediglich der Sicherheit der betroffenen Behörden dient.")
(ML Ekn "Ein Kompetenzzentrum wäre natürlich ein schönes Geschenk vom Staat, aber der FFII wünscht sich von der Regierung zu aller erst klare und faire Regeln (wie sie in Frankreich eingeführt sind oder werden).  Ferner wünschen wir uns die aktive und regelmäßige Teilnahme der Regierung an einem internet-basierten Dialog zur Förderung einer freien informationellen Infrastruktur.  Die Regierung hat hierzu bereits vorbildliche Schritte unternommen, z.B. Förderung der Programmierung des GNU Privacy Guard.  Aufträge dieser Art sollten häufiger und systematischer ausgeschrieben werden.  Daran sollten Universitäten und Firmen und letztlich die gesamte Öffentlichkeit beteiligt werden.  Ein räumlich gebundenes Kompetenzzentrum wäre ein möglicher dritter Schritt.  Aber gerade wenn der Staat %(q:neutraler Moderator) sein will, bietet das Internet dafür fürs erste die beste mögliche Infrastruktur.")
)

(rms "Richard M. Stallman"
(filters ((ep ahs 'eulux-petition)) (ML RSk "RMS ließ während mehrerer Ansprachen und Podiumsdiskussionen keine Gelegenheit aus, um sein Publikum zur Aktion gegen Softwarepatente aufzurufen.  Laut Stallman ist jeder Benutzer von GNU/Linux und freier Software moralisch verpflichtet, die %(ep:Eurolinux-Petition) zu unterschreiben.  Jede Firma, die im GNU/Linux-Umfeld arbeitet ist verpflichtet, ihre Kunden auf die Patentgefahr aufmerksam zu machen.  Ein Petitions-Werbeaufkleber sollte z.B. mit dem SuSE Linux oder Redhat Linux Paket an jeden Käufer ausgeliefert werden.  Stallman kam persönlich zum FFII-Stand, um dort inmitten einer Menschentraube seine Unterzeichnungszeremonie zu zelebrieren."))
)

(cox "Alan Cox"
(ML LWe "Linuxkern-Entwickler Alan Cox, der bei Redhat arbeitet, trug durchweg unseren Eurolinux-Petitions-Aufkleber an seinem roten Hut.  Cox kennt die Problematik sowohl von der informatischen als auch von der rechtlichen Seite her gut und wird sich in den nächsten Wochen sowohl bei Redhat als auch bei einigen britischen Politikern für unsere Sache einsetzen.  Dies erzählte er uns während mehrerer Gespräche am FFII-Stand.")
)

(wclu "Swantje Weber-Cludius"
(ML FWg "Frau Weber-Cludius ist beim Bundeswirtschaftsministerium für Patentrecht zuständig.  Sie beteiligte sich am Freitag und Samstag intensiv an unseren Vortrags- und Diskussionsveranstaltungen und nutzte auch darüber hinaus die Gelegenheit zu stundenlangen Gesprächen mit Wolfgang Tauchert, PA Springorum, Peter Gerwinski, Hartmut Pilch, Jean-Paul Smets, Holger Blasum und anderen Protagonisten der SWPAT-Debatte.")
)

(tauch "Wolfgang Tauchert"
(ML Ien "In privaten Gesprächen erläuterte der Datenverarbeitungs-Experte des Deutschen Patentamtes seine Position weiter.  Ihn stört insbesondere, dass die Rechtssprechung sich jahrelang in Richtung auf Ausweitung der Patentierbarkeit entwickelte und niemand hörbar etwas dagegen sagte.  Wenn nun die betroffenen Kreise anderer Meinung seien, dann sollten sie nicht gegen angebliche Rechtsmissbräuche zetern sondern lieber die Mittel des Systems nutzen und sich in die juristische Diskussion einbringen, um die Rechtssprechung des BGH auf den alten Kurs zurückzubringen.  Das sei auch dann noch möglich, wenn die Computerprogramm-Ausnahme aus dem EPÜ/BPatG gestrichen worden sei.")
)

(spring "PA Dipl.Inf. Dr. Harald Springorum"
(ML Pke "PA Springorum hielt es für nicht ganz aussichtslos, ein Gesetz oder ein Grundsatzurteil zu erwirken, welches GPL-Software generell als nicht-gewerblich definiert.  Er kritisiert jedoch, dass bei Firmen wie SuSE eine Wertschöpfung stattfindet, an der die freien Entwickler, auf deren Schultern SuSE stehe, nicht fair beteiligt würden.  Das Patentwesen schaffe hier durch das Arbeitnehmererfindergesetz mehr Gerechtigkeit.")
(ML Hiz "Herr Springorum argumentierte auch sonst bevorzugt mit moralischen Argumenten für die Patentexpansion, nicht ohne gleichzeitig die Patentgegner als Moralisten/Ideologen darzustellen.")

(lin (ML Der "Die naheliegenden Gegenargumente sind")
(ul
(filters ((ss ahs 'orf-lessig-swstal)) (ML Uii "Undank ist der Welt Lohn.  Auch diejenigen, welche die jahrzehntelange mathematische Vorarbeit für das MP3-Patent geleistet haben, gingen leer aus (und dürfen heute noch nicht einmal die MP3-Technologie frei verwenden).  Bei der Masse der Trivialpatente ist es noch schlimmer:  unzählige Parallelerfinder werden enteignet.  Abgesehen davon belohnt der %(q:digitale Kapitalismus) generell nicht unbedingt bevorzugt diejenigen, die zum Fortschritt der Zivilisation beitragen.  Das sieht man schon an der Qualität des Privatfernsehens oder des kommerziellen Internets.  Den digitalen Kapitalismus durch Patente gerecht machen zu wollen, ist vielleicht gerade eine jener gefährlichen Utopien, die den Himmel versprechen und die Hölle schaffen: totalitäre Ideologie in Reinform.  Professor Lessig (Harvard-Verfassungsrechtler) %(ss:nennt) das nicht umsonst %(q:Software-Stalinismus)."))
(ML Fvr "Firmen wie SuSE und Redhat sind von sich aus bemüht, Beiträge zur Linux-Gemeinde zu leisten.  Sie bezahlen z.B. zahlreiche Entwickler dafür, dass sie freie Software schreiben.")
(ML DrV "Das Distributionsgeschäft ist bei der Freien Software kein notwendiges Glied der Wertschöpfungskette, sofern es eine solche überhaupt gibt.  Das Distributionsgeschäft wird möglicherweise bald ganz sogar verschwinden und durch nicht-kommerzielle verteilte Systeme wie Debians apt-get ersetzt werden.  Andererseits erfüllt die SuSE-Distribution heute eine wichtige volkswirtschaftliche Rolle: sie erleichtert zahlreichen heimischen KMUs den Zugang zum Weltmarkt.  Das Patentrecht wäre in diesem System gar nicht anwendbar:  man müsste Gebühren für Millionen von Programmkopien erheben, die niemand verwendet.  Das Patentrecht würde somit fortschrittlichere Distributionsformen wie Shareware benachteiligen und eine Rückkehr in die Zeiten des Verkaufs von Glanzkartons am Ladentisch erzwingen.  Das wäre auch nicht im Sinne des Patentrechts, denn es handelt sich hierbei nicht um ein Kopierrecht (Urheberrecht) um ein Nutzungsrecht:  die Lizenzgebühr sollte erst bei der tatsächlichen Verwendung der patentierten Technik erhoben werden.  Egal ob das Programm aus dem Internet oder von einer SuSE-CD bezogen wurde.")
(ML Uwu "Unsere Hauptargumente liegen nicht im Bereich der Moral sondern der Volkswirtschaft.  Fördern Softwarepatente die Innovation und den Wettbewerb?  Führen Sie wenigstens zu einem Anwachsen der F&E-Ausgaben bei den Softwareunternehmen?  Unsere Erfahrung legt nahe, dass beides zu verneinen ist.  Wissenschaftliche Studien bestätigen dies.")
)
)
; spring
)

(siemens (ML Gem "Gespräche am Siemens-Stand")
(ML MtW "Mehrere Vertreter der Siemens IT Services meinten, dass Arno Körber, der Chef der Siemens-Patentabteilung, der in der Öffentlichkeit immer wieder (zuletzt unter dem Mantel des ZVEI) vehement für die Patentexpansion eingetreten ist, nicht wirklich die Interessen von Siemens vertrete.  Siemens habe von der Patentexpansion durchaus mehr Nachteile als Vorteile zu erwarten.")
(ML Dar "Da Siemens jedoch derzeit noch mit Microsoft %(q:verheiratet) sei, sei es unwahrscheinlich, dass das Siemens-Logo auf der Eurolinux-Petition erscheinen dürfe.  Man könne jedoch vermutlich vom Siemens-Vorstand eine Erklärung erwirken, die zu Mäßigung bei der Patentexpansion rät und die Durchführung einer volkswirtschaftlichen Studie empfiehlt.")
(ML Duh "Die Informatiker und Forscher bei Siemens reagierten in jüngster Zeit recht irritiert auf Anweisungen von seiten der Patentabteilung, möglichst alles, was irgendwie innovativ sein könnte, der patentrechtlichen Verwertung zuzuführen.  Diese Anweisung hätten deutlich gemacht, dass Patente nicht mehr als %(q:Mittel zum Schutz unserer Erfindungen) sondern als %(q:Tauschwaren) betrachtet würden.  Den Mitarbeitern sei klar, dass mit dem Patentwesen irgendetwas faul ist, und es könne nicht angehen, dass Siemens sich in der Öffentlichkeit für ein solches System stark mache.  Solche Stellungnahmen seien, wenn sie wirklich existierten, gegen die Konzernpolitik gerichtet.  Es müsse im Interesse der Konzernleitung liegen, das klarzustellen.")
(ML Ser "Siemens verwendet laut Mitarbeiter-Aussagen gelegentlich Softwarepatente, um zu verhindern, dass Partnerfirmen sich selbstständig machen und von Siemens entwickelte Technik eigenständig vermarkten.  Doch dieses Ziel lässt sich ebenso ohne Softwarepatente erreichen.  Meistens genügt einfach der Erfahrungs-Vorsprung der Siemens-Mitarbeiter, welche die Technik in vielen Jahren und vielen kleinen Schritten entwickelt haben.  Wo das nicht reiche, habe man auch noch Urheberrecht und Betriebsgeheimnis.")
;siemens
)

(firmen (ML GWe "Gespräche an anderen Ständen")
(ML WeW "Wir führten noch Gespräche mit einem Großteil der Ausstellerfirmen.  Viele Firmen entschlossen sich daraufhin, in den nächsten Tagen an unserer Petition teilzunehmen.")
;firmen
)

(radio (ML Roe "Radiointerview")

(ML EHe "Ein Radiojournalist führte ein 20-minütiges Gespräch mit Hartmut Pilch, das gesendet werden soll.  Er hatte zuvor ebenfalls mit Stefan Meretz und Stefan Merten gesprochen.") )

;sects
)
;etc
)

(dank (ML Dkg "Danksagung")
(ML UeW2 "Unser Stand konnte nur deshalb von Anfang an gut aussehen, weil Arnim Rupp und sein Bruder schöne Schilder anfertigen ließen und am Tag vor der Messe anbrachten.")
(ML DcW "Dank Holger Blasums Bemühungen konnten wir am Stand schön bedruckte FFII-Hemden tragen.  Ihm ist auch der schnelle Rechner mit großem Bildschirm zu verdanken, mit dem wir aufwarten konnten.")
(ML Frd "Felix Nenz sorgte für weitere Hinweisschilder und Getränke.  Er beantwortete drei Tage lang geduldig unseren Besuchern Fragen und verzichtete dabei auf die Teilnahme an vielen der interessantesten Veranstaltungen.")
(ML Hvn "Holger, Felix, Bernhard Reiter, Frank Kormann und Hartmut Pilch verbrachten je 3-4 Tage am FFII-Stand, ohne jegliche Entschädigung für Arbeitszeitausfall oder Hotelkosten in Anspruch zu nehmen.")
(ML Dgs "Der Linuxtag e.V. leistete großartige Arbeit und unterstützte uns unbürokratisch.")
(let ((M (ah "http://www.mandrake.fr" "Mandrake"))) (ML JWr2 "Jean-Paul Smets nahm sich trotz drängender Aufgaben und mangelnden Schlafes einen Tag.  Seine Reisespesen zahlte %{M}."))
(let ((LST (etc (ah "http://www.infomatec.de" "Infomatec AG") (ah "http://www.suse.de" "SuSE Linux AG") (ah "http://www.sunbeam.franken.de" "Harald Welte") (ah "http://www.intradat.de" "Intradat AG")))) (ML Dnu "Diverse Spesen konnten nur deshalb bezahlt werden, weil %{LST} den FFII mit großzügigen Spenden unterstützt haben."))
(let ((LST (etc (ah "http://www.suse.de" "SuSE Linux AG") (ah "http://www.intevation.de" "Intevation GmbH")))) (ML Fit "Firmen wie %{LST} sahen großzügig darüber hinweg, dass einige ihrer bezahlten Mitarbeiter ihre Erwerbstätigkeit vernachlässigten."))
)
;sects
)
; swplxtg25
)

(mlhtdoc 'swplxtauch (ML LWk "Tauchert-Vortrag auf dem Linuxtag 2000")
(let ((WT (spaced "Dr." (famvok "Tauchert" "Wolfgang")))) (ML Vio "Vortrag und Diskussion mit %{WT}, Leiter der Patentabteilung fuer Datenverarbeitung und Informationsspeicherung am Deutschen Patent- und Markenamt.  Der FFII rezensiert hier an Taucherts Manuskript, dass es die gestellten Fragen nicht zu beantworten und nicht über bereits bekannte Artikel des Autors hinauszugehen scheint." "Presentation of %{WT}, head of the German Patent Office's computing department"))
(append (l "Offenbarung in Amtssprache" "DPMA" "Softwarepatentierung") swpatkeyw)
(lin 
 (ML Hhd "Herr Tauchert ist uns als Teilnehmer der juristischen Debatte um die Softwarepatentierung bekannt:")
 (ul (ahs 'grur-tauch99) (ahs 'mdp-tauch99)) )

(filters ((bm ahs 'swpbmwi25)) (ML FWo "Ferner kennen wir Herrn Tauchert als einen engagierten Fürsprecher des Patentwesens auf der %(bm:BMWi-Konferenz), auf der die versammelten Entwickler und Softwarefirmen kein gutes Haar am Patentwesen ließen.  Herr Tauchert schien dabei redlich um eine konsistente Theorie bemüht und gestand in der Diskussion zu, dass er schon immer der Meinung gewesen sei, für Software würde ein gesondertes (sui generis) Eigentumssystems besser taugen als das Patentsystem."))

(ML Atf "Auf dem Linuxtag wollten wir von Herrn Tauchert erfahren, wie Entwickler freier Software sich und das von ihnen geschaffene öffentliche Informationseigentum vor Patentangriffen schützen können.  Als Leiter der DV-Abteilung des Deutschen Patentamtes ist er dafür eine Fachautorität.  Herr Tauchert reichte uns folgende Vortragszusammenfassung ein, zu der wir im voraus ein paar kritische Anmerkungen machen wollen.")

(ML WTs "Wolfgang Tauchert schreibt:")

(blockquote
(ML Aic "Anmeldungen mit programm-bezogenen Verfahren und entsprechende %(q:programmtechnisch eingerichtete Vorrichtungen) können am Deutschen Patent- und Markenamt als sog. programm-bezogene Erfindungen patentiert werden. Die Anmeldung hat die Anforderungen der Anmeldeverordnung (Schriftform, Amtssprache) zu erfüllen und ist unter den üblichen Voraussetzungen patentfähig (gewerbliche Anwendbarkeit, Neuheit, erfinderische Tätigkeit, technischer Charakter). Entsprechend der Richtlinien für die Patentprüfung erfolgt die Ermittlung des technischen Charakters auf der Basis des gesamten Anspruchs, ohne Trennung der Merkmale durch den Stand der Technik."))

(ML Lra "Letzteres werden die meisten von uns nicht verstehen.  Ich verstehe es so:  die Erfindung muss insgesamt einen technischen Charakter haben.  Es kommt aber nicht darauf an, dass der %(q:erfinderische Kern) selbst im Bereich des technischen liegt.")

(ML Mza "M.a.W. die %(q:Kerntheorie) wurde (etwa seit dem Mellulis-Aufsatz von 1998) aufgegeben und damit sind der Patentierung von Software am Deutschen Patentamt die Tore %(q:weit geöffnet) (Betten).")

(blockquote 
(ML DWr "Der Begriff Technik wird nach der Definition des Bundesgerichtshofs verstanden, er orientiert sich an der maschinellen Lösung eines Problems und schließt daher Informationstechnik ein. Auf die aktuelle Rechtsprechung wird Bezug genommen.")
)

(ML fij "ffii: M.a.W. weder das PatG noch EPÜ noch die Prüfungsrichtlinien gelten unmittelbar, sondern es gelten Normen, die von der Rechtssprechung seit 1998 gesetzt wurden.")

(ML Dfe "Der Bundesgerichtshof definiert Technik als %(q:planmäßiges Handeln unter Einsatz von Naturkräften ohne zwischengeschaltete menschliche Tätigkeit).  Diese Definition ist allerdings dehnbar, wie insbesondere neuere BGH-Urteile (Verkaufsautomat) zeigen.  Demzufolge sind auch Geschäftsmethoden technisch, wenn dabei ein Computer eingesetzt wird.  Selbst dann, wenn, wie etwa beim Aushandeln von Preisen im %(q:Verkaufsautomaten), menschliche Tätigkeit zwischengeschaltet ist.")  

(blockquote
(ML Qre "Quellcode wird nicht als eine Amtssprache verstanden und kann daher keine ursprüngliche Offenbarung begründen."))

(ML Mii "M.a.W. man kann durch Patente zwar die Verbreitung von Quellcode verbieten, aber man kann nicht eine Referenzimplementation beim Patentamt einreichen, da es den Patentprüfern nicht zuzumuten ist, diese zu lesen.  Hingegen ist es der Fachöffentlichkeit sehr wohl zuzumuten, unverständliche Beschreibungen in natürlicher Sprache zu lesen, um Patentansprüche zu verstehen und zu überprüfen ob die eigene OpenSource-Software diese vielleicht verletzt. In letzterem Falle ist die quelloffene Software vom Netz zu nehmen, denn bekanntlich sind seit 1997 sowohl beim EPA als auch beim DPMA %(q:Programmansprüche) und %(q:Internetansprüche) zulässig.")

(blockquote
(ML Dof "Die Darstellung einer software-bezogenen Erfindung als Produktanspruch (Speicherelement mit einem Programm zur Ausführung von Verfahrensschritten) ist prinzipiell möglich.")) 

(ML WgW "Wie ist es mit Programmansprüche und Internetansprüchen?")
(ML SuA "Sind die nur beim EPA zulässig?")
(ML WrW "Weist das DPMA solche Ansprüche zurück, wenn sie ihm vom EPA überreicht werden?")
(ML Ors "Oder interpretiert es sie anders?")

(filters ((br ahs 'bettenresch9901)) (ML LlW "Laut %(br:Artikel von Betten & Resch) folgt das DPMA in all diesen Dingen dem EPA-Fallrecht. Nur das Britische Patentamt hält sich noch an die Vorschriften des EPÜ."))

(blockquote
(ML ArW "Allerdings wird ein Datenspeicher technisch nicht durch die auf ihm gespeicherten Schritte charakterisiert, vielmehr ist das Speicherelement ist zur Speicherung von Information aller Art vorgesehen. Dem entsprechend kann der Produktanspruch nur einen Unteranspruch darstellen, da diese Formulierung keine zum Gegenstand des Hauptanspruchs unabhängige Lösung der zugrundeliegenden Aufgabe darstellt."))

(ML Ira "Interessant.")
(ML Fih "Für den Programmierer ist es allerdings egal, ob er einen Hauptanspruch oder einen Unteranspruch verletzt.")
(ML DhW "Die meisten unserer Zuhörer kennen überhaupt nicht den Unterschied.")

(blockquote 
(ML SWr "Schutz des Quellcodes erfolgt durch das Urheberrecht. Im Hinblick darauf kann Quellcode als die Verkörperung des %(q:Programms als solchem) nach Art. 52 (2) und (3) des EPÜ gesehen werden. Er erfüllt - auch nach Revision des EPÜ - nicht die Voraussetzungen zur Patentfähigkeit (siehe 1).") )

(ML DWh "Diese Formulierung bedeutet nur:  auch nach Revision des EPÜ akzeptiert das DPMA keine Patentbeschreibung in Form einer Referenzimplementation, s.o., aber die Verbreitung von Quellcode kann aufgrund von Patentansprüchen untersagt werden.  Sowohl nach jetziger (EPÜ-widriger) Rechtsprechung als auch nach der Anpassung des EPÜ.")

(filters ((ds ahs 'iic-schiuma00)) (ML DPe "Die geplante Revision des EPÜ würde jedoch in anderen Punkten wesentliches verändern, s. dazu die %(ds:Argumentation des EPÜ-Änderungs-Verfechters Daniele Schiuma)."))
)


;swplxtg26-dir
)

(mlhtdoc 'swpsyst2B 
 (ML StS "Softwarepatente auf Systems 2000") 
 (ML DbL "Die Arbeitsgruppe des FFII zum Schutz der digitalen Innovation vor Patenten wird am 6.-10. November 2000 auf IT-Fachmesse Systems in München auftreten.  Dank freundlicher Unterstützung der Linux New Media AG sind uns zu diesem Zwecke 4-6 qm am OpenSource-Pavillon reserviert worden.") 
 nil
 (fartab
 (l (ML Akb "Aufkleber") (ML AlW "Aufkleber herstellen und auf der Messe verteilen") "arnim?" (ML Eiv "Es liegen noch einige Aufkleber vom Linuxtag vor"))
 (l (ML Shl "Schilder") (commas (ML Sat "Schilder und Plakate für den Stand herstellen") (ML AeW "Ausstellungsstücke wie z.B. eine SWPAT-Horrorgallerie")) "arnim?" (ML Evn "Ein paar Schilder von Linuxtag liegen bei phm in München"))
 (l (ML Psk "Prospekte") (commas (ML HrW "Handzettel über die Vorträge auf der Systems") (ML KuT "Kurzerläuterung der SWPAT-Problematik") (ML Fks "FFII-Broschüre")) "phm?")
 (l (ML Vao "Vortragsprogramm") (ML eer "ein paar Vorträge ins offizielle Programm zwängen oder ein getrenntes Vortragsprogramm planen") "phm?")
 (l (ML CDR "CDROM") (ML DWe "Die bisherige CDROM ist zu aktualisieren und dynamisch zu gestalten, so dass ihre Struktur leichter geändert werden kann") "breiter?")
 ) )
; swpbmwi25-dir
)
; swpenmi
)

(list 'swpatvreji-dir
(mlhtdoc 'swpatvreji nil nil nil
(filters ((lp ahs 'swpatprina) (ft ahs 'swpatminra) (cd ahs 'swpdoku) (gz ah "http://www.gzip.org") (pa ahs 'swpatpapri) (pi ahs 'swpatpikta)) (let ((GZ (ah "http://www.gzip.org" "GNU TarZip"))) (ML Diu "Die interessantesten Netzinhalte zu diesem Thema sammeln wir ständig in einem %(ft:komprimierten Paket), dass Sie mit %{GZ} auspacken und auf einem beliebigen Webserver darstellen können.  Wesentlich übersichtlicher aber auch weniger umfassend als dieses Paket ist die von uns erstellte %(cd:Dokumentations-CD).  Neuere Arbeitsschwerpunkte bilden unser %(pi:Datenbank und Gruselkabinett Europäischer Softwarepatente) und unsere %(pa:Rezensionen von einschlägigen Monographien und Fachartikeln).  Für die gemütlichere Lektüre im Bett oder auf Reisen gibt es außerdem eine %(lp:Sammlung der wichtigsten ausdruckbaren Texte)." (en "We regularly pack of these contents into a %(ft:compressed package) that you can unpack using %{GZ} and place it on your web server at home.  Recently we have worked on a %(pi:European Software Patent Databse and Horror Gallery) and a %(pa:reviews of articles and monographies in patent law and economics journals). There is also a %(lp:collection of printable documents) for your reading in bed or during travel.") (fr "Nous mettons ces contenus régulièrement dans une %(ft:emballage comprimè) par %{GZ} et de temps en temps dans une %(cd:version CDROM).  Récemment on a travaillé beaucoup sur un %(pi:Base de Donnés Musée d'Horreurs des Brevets Logiciels Européens) et des %(pa:récensions d'articles et monographies).  Il y a aussi une %(lp:collection de documents imprimables) pour votre lecture au lit ou en voyage."))))
(docmenu-large)
)					

(mlhtdoc 'swpatprina nil nil nil
 (filters ((lp ah "ftp://ftp.ffii.org/pub/swpat/swpatlpr.tgz")) (let ((GZ (ah "http://www.gzip.org" "GNU Tar/Zip")) (PP (aut (ah "http://www.cs.wisc.edu/~ghost" "Ghostscript/Ghostview") "Adobe Acrobat"))) (ML Diu "Die interessantesten druckbaren Texte zu diesem Thema sammeln wir ständig in einem %(lp:komprimierten Paket), dass Sie mit %{GZ} auspacken und mit %{PP} ausdrucken können." (en "We regularly pack of these contents into a %(lp:compressed package) that you can unpack using %{GZ} and print using %{PP}.") (fr "Nous mettons ces contenu dans un %(lp:packet comprimé) chaque nuit que vous pouvez décomprimer avec %{GZ} et regarder avec %{PP}."))))
(sects
(sisku 
 (ML Wnu "Wirkungsstudien und Gesamtdarstellungen" "Impact Studies and Overviews" "Études d'Impacte et Introductions Générales")
 (apply 'prina-subtable '(mit-seqinnov useright-pdf swxepue28 smets-stimuler drtw patexam-corrupt skk-patpruef grur-kolle77)) )
(polit 
 (ML EPi "EU-Politik" "EU Policy" "Politique de l,UE")
 (apply 'prina-subtable '(smets-agenda eu-gp-pat indprop-8682 indprop-utility)) )
(pat (ML Vtt "Vom EPA Erteilte Softwarepatente" "Software Patents granted by the EPO" "Brevets Logiciel Accordés par l'OEB")
 (bridi 'cf (ahs 'swpatpikta (ML Hia "Europäische Softwarepatente -- Datenbank und Beispiele" "European Software Patents Horror Gallery" "Galérie d'Horreur et Base de Données de Brevets Logiciels accordés par l'OEB")))
)
(epa 
 (ML FGs "Patentgerichtsentscheidungen" "Patent Court Rulings" "Décisions Court Brevet") 
 (apply 'prina-subtable '(epo-t960410 epo-t970935 epo-t971173 bpatg17w6998)) )
(jur
 (ML Fye "Häufig zitierte Rechtstexte" "Frequently Cited Legal Texts" "Textes Légaux Cités Fréquemment")
 (apply 'prina-subtable '(epc-52 trips-27))
)
(xatra
 (ML Sln "Stellungnahmen und Presseartikel" "Statements and Press Articles" "Prises de Position et Articles")
 (bridi 'cf (ah "http://petition.eurolinux.org/statements/"))
 (apply 'prina-subtable '(freepatents-pr1 euipCA infomatec-euipCA suse-euipCA prosa-euipCA live-euipCA miertltr siepmann-bmwi ossenkopf-bmwi mcquaker-london kober-london adobe-testimony oracle-testimony borland-testimony greenpstud patently-absurd patentkrieg)) )
;sects
)
;swpatprina 
)

(l 'swpatpikta-dir

(mlhtdoc 'swpatpikta nil nil nil

(sects
(dat (ML nrc "Übersicht und Beispiele" "Database and Examples" "Base de Données et Exemples")
(docmenu-large)
)

(ana (ML Wai "Wie man Patentschriften liest" "How to read patent descriptions" "Comment lire les descriptions de brevet")
 (linul (ML Ete "Eine Patentschrift besteht aus" "A patent description consists in" "Une déscription de brevet consiste en")
  (ML Aue "Anspruchsbereich" "claims" "revendications")
  (ML Bhb2 "Beschreibung" "detailed description" "déscription en détail")
 )
 (ML Tur "The claims say what you are not allowed to do.  Each claim defines one class of prohibited objects.  The description helps to interpret the meaning of the claims.  It is supposed to provide sufficient instructions to %(e:enable) the %(e:person skilled in the art) to reimplement the %(q:invention) without engaging in further inventive activity.  However, EPO software patent descriptions generally fail to provide a reference implementation, and the hard and part is usually left to the programmer.  Thus even from the point of view of %(e:enablement) doubt could be casted on the validity of most EPO software patents." (de "In den Ansprüchen steht zu lesen, wass Sie nicht tun dürfen.  Jeder Anspruch beschreibt eine Klasse verbotener Gegenstände.  Die Beschreibung hilft bei der Auslegung der Ansprüche.  Sie soll %(e:den durchschnittlichen Fachmann befähigen), die %(q:Erfindung) nachzuarbeiten, ohne weiterhin erfinderisch tätig werden zu müssen.  Die EPA-Beschreibungen enthalten jedoch im allgemeinen keine Software-Referenzimplementation, und die eigentlich schwierige Arbeit bleibt dem Programmierer überlassen.  Daher könnte man auch vom Gesichtspunkt der %(e:Befähigung des Fachmanns) her gesehen die Gültigkeit der EPA-Softwarepatente in Zweifel ziehen.") (fr "Les revendications disent ce qu'on vous interdit de faire.  Chaque revendication décrit une classe d'objets interdits.  La déscription vous aide a interpreter les revendication.  Elle doit %(e:rendre le spécialiste moyen capable) de reproduire l'%(q:invention) sans devoir inventer des choses lui même.  Malheureusement les déscriptions OEB ne continennent pas d'implémentation de référence, et la parti difficile est laissée au programmeur.  De cet aspet aussi on pourrait contester la validité de la plupart de brevets logiciels."))
 (ML Pto "Patent descriptions and claims use a lot of strange talk about %(q:allocation a block of space for a variable in a memory device) etc.  This may just serve to make the %(q:invention) look %(q:technical), but also to prepare lines of retreat for possible litigation.  In any case, a less legally interested reader will have to learn to treat it as noise." (de "In Patentbeschreibungen ist häufig wortreich von Selbstverständlichkeiten wie der %(q:Zuweisung eines Blocks in einem Speichergerät zur Speicherung einer Variablen) u.ä. die Rede.  Dies lässt die %(q:Erfindung) besonders %(q:technisch) aussehen, kann aber auch notwendig sein, um Verteidigungslinien für Rückzugsgefechte im Fall von Rechtsstreit aufzubauen.  In jedem Falle muss der weniger juristisch interessierte Leser lernen, dieses Wortgeklingel zu überlesen.") (fr "Dans les descriptions de brevets il y a beaucoup de bruit superflu comme %(q:reservation de plusieurs bloques de mémoire pour une variables dans un appareil de mémoire).  Ces chose servent peut-être a faire semblant qu'il s'agisset d'un objet %(q:technique) mais aussi de préparer les lignes de défense en cas de contentieux. Dans tout cas le lecteur moin intéressé en matière juridique doit apprendre a ignorer ce type de bruit.")) )

(etc (ML ssr "Further Reading" (de "Weitere Lektüre") (fr "Suggéstions pour lire"))
 (linklist
  (l (ahs 'rms-trivpat) (ML Tsg "This article does a good job at showing how trivial things are made to sound complicated in patent descriptions.  However it may not be of much help in appreciating why this is so.  Patents are after all not meant to be good textbooks but rather to provide an excruciatingly accurate definition of what your are not allowed to do." (de "Der Artikel zeigt anschaulich, wie man das Wortgeklingel in Softwarepatentschriften überliest und sie im Geiste auf das wesentliche reduziert.  Allerdings führt Stallmans Herangehensweise nicht zum Verständnis der juristischen Zusammenhänge, aus denen heraus eine Patentschrift entsteht.") (fr "Montre comment les descriptions de brevets logiciels sont plein de bruit inutile qu'on doit ignorer.  Or Stallman n'explique pas les raisons juridiques derrière ce phénomène.")))
  (l (ahs 'skk-patpruef (colons (ML DKe "Dr. Swen Kiesewetter-Köbinger") (ML nWt "Über die Patentprüfung von Programmen für Datenverarbeitungsanlagen"))) (ML PaK "Probleme und Ungereimtheiten der Softwarepatentierung aus der Sicht eines Prüfers am Deutschen Patent- und Markenamt -- zeigt sehr scharfsinnig warum Softwarepatente so sind wie sie sind und welche weiteren Gefahren eine Änderung des Art 52 bringen würde." "Problems and inconcsistencies of software patenting from the perspective of a former programmer and present-day examiner at the German Patent and Trademark Office -- a very lucid article that shows why software patents are what they are and mercilessly debunks a whole network of fallacious arguments of those who have been pushing software patents in Germany.  English translation available." "Un examinateur de l'Office Allemand de Brevets montre pour quoi les brevets logiciels sont comme ils sont et quels sont les dangers d'un brevetage directe des logiciels en tant que tels.  Une traduction anglaise est disponible."))
  (l (ahs 'swpatfrili (ML Wer "Why Software Patents are so trivial" (de "Warum Softwarepatente so trivial sind") (fr "Pour quoi les brevets logiciels sont-ils si trivials?")))  (docdescr 'swpatfrili) )
  (l (ahs 'bountyquest) (ML Acs "A kind of patent horror gallery, where companies that are facing patent threats pay people to find prior art for them." (de "Ein amerikanisches Patent-Gruselkabinett, das Preise für diejenigen ausschreibt, die ärgerlich gewordene Patente mit neuheitsschädigenden Dokumenten zu Fall bringen.") (fr "Un musée d'horreurs de brevets logiciels américains qui offert des prix pour ceux qui casses des brevets existants en fournissant des preuves d'art antérieure.")) ) )
;etc
) 
;sects
)
;swpatpikta
)

(mlhtdoc 'swpaperled nil nil nil
(sects
(howdone (ML HtW "How it was done" (de "Wie wir es gemacht haben"))

(ML Tke "The difficulty is that there is no explicit identification for software related patents. Therefore we had to download all patents which matched certain keywords like internet, server, client or virtual into a database. Then we assigned each patent a probability of being a software patent, again by checking for keywords in the title and abstract. Words related to algorithms and programming raised the probability, those related to physical or mechanical things lowered it.  This has led to a fairly high score rate, but still you will occasionally encounter a true hardware patent." (de "Die Schwierigkeit hierbi ist, dass es keine eindeutige Kennung für Patente auf %(q:softwarebezogene Erfindungen) gibt.  Wir mussten daher alle Patentschriften, in denen bestimmte Schlüsselwörter auftreten, herunterladen und analysieren.  Dabei gingen wir wiederum von Schlüsselwörtern aus, deren Vorkommen zu Plus- oder Minuspunkten führte.  Das Ergebnis ist einigermaßen zufriedenstellend, aber man wird sicherlich hier und da ein völlig fehlplaziertes Mechanik- oder gar Chemiepatent finden.")) )

(lists (ML Pta "Patent Lists, sorted by Software Patent Probability" (de "Nach Wahrscheinlichkeit sortierte Patentlisten"))

(let ((RP (quot "Requested Patent"))) (ML Aae "As most browsers are slow with very big tables, there is a choice between the lists in HTML tables and plain text. All patents are granted by the EPO, you can validate this by following the link under the patent number on each patent and checking for the B1-document in the row %{RP}." (de "Es gibt die Tabellen jeweils als Schlichttext und HTML.  Alle Patente sind erteilt worden.  Dies können Sie nachvollziehen, indem Sie dem Verweis unter der Patentnummer folgen und dann das B1-Dokument in der Zeile %{RP} anwählen.")))

(center (table '((border t) (cellpadding 20)) '((ptxt (filter bold)) (html (filter bold)) (zip)) (l (l (ML pit "plain text" (de "schlichter Text")) "HTML" (ML Cps "Compression"(de "Kompression")))) (l
(l (ah "swpat.pre.en.top100.html" (pf (ML Top "Top %s" (de "Oberste %s")) 100)) (ah "swpat.tab.en.top100.html" (pf (mlval 'Top) 100)) "--")
(l (ah "http://swpat.de/ffii/swpat.pre.en.top1000.html" (pf (mlval 'Top) 1000)) (ah "http://swpat.de/ffii/swpat.tab.en.top1000.html" (pf (mlval 'Top) 1000)) "--")
(l (ah "http://swpat.de/ffii/swpat.pre.en.top1000.html" (pf (mlval 'Top) 10000))  (ah "http://swpat.de/ffii/swpat.tab.en.top10000.zip" (pf (mlval 'Top) 10000)) "zip")
) )) 
)

(stats (ML Stt "Statistics" (de "Statistiken"))

(center (framebox (bold (nl
(ah "country_stat.en.html" (ML Cyi "Country Statistics" (de "meistanmeldende Länder")))
(ah "app_stat.html" (ML Cyi2 "Company Statistics" (de "meistanmeldende Firmen")))
))))
)
)
)

(mlhtdoc 'swpatepdir nil nil nil

(filters ((tz ah "../epdir.tbz")) (ML Fht "For offline study of EPO software patents, we regularly pack all available patent texts into one %(tz:bzip2-compressed tar archive).  This archive does not contain the PDF files.  Still, when unpacked, it does not fit on one CDROM.  If you come accross something interesting, please send us a comment.  We need people to help evaluating these patents, and we'd almost certainly love to publish your comment.  Also, if you have a 100 GB of free server space, we'd appreciate it if you mirror this archive for the public."))

(let ((abspafdir (abspafdir docid))) (table '((border t)) '((num) (tit) (own) (pub)) (l (mapcar (lambda (sym) (tok (cdr (assoc sym absdata-labels)))) '(patentnr title applicant pubdat))) (mapcar (lambda (num) (let* ((dir (format "ep%d" num)) (absdata (read-docdata-from-file  (subdirs abspafdir dir "absdata")))) (cons (ah dir (format "EP %d" num)) (mapcar (lambda (sym) (cadr (assoc sym absdata))) '(title applicant pubdat))))) (mapcar 'string-to-number (split-string (file-string (subdirs (abspafdir docid) "ep-swpat-nums")) "\n")))))
)


(l 'swpikmupli-dir
(mlhtdoc 'swpikmupli nil nil nil
(docmenu-large)
)

(mlhtdoc-ep 487110)
(mlhtdoc-ep 193933)
(mlhtdoc-ep 769170) 
(mlhtdoc-ep 792493)
(mlhtdoc-ep 328232)
(mlhtdoc-ep 644483) 
(mlhtdoc-ep 800142)
(mlhtdoc-ep 529915)
(mlhtdoc-ep 747840)
(mlhtdoc-ep 587827)
(mlhtdoc-ep 490624)
(mlhtdoc-ep 762304) 
(mlhtdoc-ep 688450)
(mlhtdoc-ep 807891)
(mlhtdoc-ep 823173)
(mlhtdoc-ep 522591)
(mlhtdoc-ep 242131)
(mlhtdoc-ep 359815)
(mlhtdoc-ep 592062)
(mlhtdoc-ep 756731) 
(mlhtdoc-ep 461127)
(mlhtdoc-ep 664041)
(mlhtdoc-ep 517486)
(mlhtdoc-ep 825526)
(mlhtdoc-ep 825525)

(mlhtdoc 'de19838253 nil nil nil
(sects
(prio (ML Pid "priority date" (de "Prioritätsdatum") (fr "date de priorité")) (docdata2ml 'prior))
(prop (ML Ihb "proprietor" (fr "propriétaire") (de "Inhaber")) (ahs 'fhg-ti))
(clms (ML Apc "claims" (de "Ansprüche") (fr "Revendications"))
 (env0 'ol (string-replace-regexp (ml (file-string (subdirs (abspafdir docid) "claims.txt"))) "^[0-9]+\\.$" "<li>")))
(desc (ML Bhb "description" (de "Beschreibung")) (pre (ml (file-string (subdirs (abspafdir docid) "desc.txt")))) )
(etc (ML Wrk "Further Reading" (de "Weitere Lektüre") (fr "Autre Textes"))
(ul
(ah "http://golem.de/0102/12126.html" (ML Psr "Press report" (de "Pressebericht") (fr "article de presse")))
(ahv-lang 'fhg-ti-komp-mm-selk)
(ah "http://de.espacenet.com/search97cgi/s97is.dll?Action=View&ViewTemplate=e/de/de/viewer.hts&SearchType=1&VdkVgwKey=19838253" (ML Dac "DPMA-Patentschrift" (en "German Patent Office patent description") (fr "déscription de brevet de l'Office Allemand")))
(ah "http://ffii.org/archive/mails/swpat/2001/Feb/0029.html" (let ((DR "Daniel Rödding")) (ML DsW "%{DR} erklärt, dass solche Verfahren vor 10 Jahren schon üblich waren" (en "prior art hints from %{DR}") (fr "%{DR} explique que ca existait il y a 10 ans"))))
(ah "http://ffii.org/archive/mails/swpat/2001/Feb/0032.html" (ML Hre "Bericht über erste Patentrecherche-Ergebnisse und die Schwierigkeiten dabei" "report about patent search difficulties" (fr "difficultés de recherche concernant ce brevet")))
)
)
)
)
;swpikmupli-dir
)

(l 'swpikxrani-dir

(mlhtdoc 'swpikxrani nil nil nil
 (docmenu-large)
 (filters ((pi ahm "swpatdb@ffii.org")) (ML IWe "If you are familiar with any cases, please %(pi:inform us)!" (de "Wenn Sie irgendwelche Fälle kennen, %(pi:berichten Sie uns) bitte!") (fr "Si vous connaissez quelques cas, veuillez bien %(pi:nous renseigner)!")))
)

(mlhtdoc 'swxai-openmint nil nil nil

(sects
(news (ML NWo "News Reports")
(ul
(ah "http://www.openmarket.com/cgi-bin/gx.cgi/AppLogic+FTContentServer?pagename=FutureTense/Apps/Xcelerate/View&c=Article&cid=OMI5D747SHC&live=true" (ML Ort "OpenMarket statement"))
(ah "http://www.handelsblatt.com/hbiwwwangebot/fn/relhbi/sfn/buildhbi/bmc/cn_hnavi/bmc/cn_artikel/bmc/cn_weitere/bmc/cn_internbanner/bmc/cn_firmenkasten/docid/368881/strucid/PAGE_200104/pageid/PAGE_201197/SH/0/depot/0/index.html" "Handelsblatt 2001-01-10")
(ah "http://l2.espacenet.com/dips/viewer?PN=US5708780&CY=ep&LG=en&DB=EPD" "The subject matter of the patents is trivial: session ids")
) )
(abs (ML Asr "Abstract")
   (blockquote (ML Ior "In particular, the process described in the invention includes client-server sessions over the Internet involving hypertext files. In the hypertext environment, a client views a document transmitted by a content server with a standard program known as the browser. Each hypertext document or page contains links to other hypertext pages which the user may select to traverse. When the user selects a link that is directed to an access-controlled file, the server subjects the request to a secondary server which determines whether the client has an authorization or valid account. Upon such verification, the user is provided with a session identification which allows the user to access to the requested file as well as any other files within the present protection domain."))) 
(prio (ML poa "prior art")
   (linul
    (ML wna "would be cookie convention and encrypted data exchange protocols")  
    (ah "http://l2.espacenet.com/dips/viewer?PN=US5715314&CY=ep&LG=en&DB=EPD")
    (ah "http://l2.espacenet.com/dips/viewer?PN=US5909492&CY=ep&LG=en&DB=EPD")) )

(eur (ML SiW "Situation in Europe")
   (linol
    (ML Tet "Two of the patents also have also been applied for at the European Patent office:")
    (ah "http://l2.espacenet.com/dips/bnsviewer?CY=ep&LG=en&DB=EPD&PN=EP0803105&ID=EP++++0803105A4+I+" "EP0803105")
    (ah "http://l2.espacenet.com/dips/viewer?PN=EP0803105&CY=ep&LG=en&DB=EPD" "EP0803105")) )
 
(bg (ML Bko "Background")
   (ah "http://www.heise.de/newsticker/result.xhtml?url=/newsticker/data/jk-05.01.01-001/default.shtml&words=Intershop" (ML Tpe "This attack was launched against Intershop after it had to restructure its US marketing strategy after huge losses in the US."))  
   (lin
    (let ((GA (ahs 'bustpatents "Gregory Aharonian"))) (ML cnl "%{GA} writes on this in patents@liberte.aful.org list on Fri Jan 12:")) 
    (blockquote (ML lee "...With regards to Open Market, they have had their patents for a few years, and really haven't done much with them.  I suspect they have approached a few companies, and then cut cheap licensing deals when these companies responded back with a fair amount of prior art that could be used to invalidate the patents.  Would these companies preferred not to have to spend any monies on such matters?  Sure, just like I don't enjoy spending tens of thousands of dollars defending myself from a totally stupid patent.  But the monies being spent are probably no more than the monies other companies in other industries have to spend to deal with crappy patents...")) ) ) )

)

(mlhtdoc 'swxai-stac-hifn nil nil nil 
(lin
(let ((HM (ahm "hm@kts.org" "Hellmuth Michaelis")) (I4B "isdn4bsd")) 
 (ML cWt "%{HM}, developper of %{I4B} in 1998 refrained from implementing support of the Stac compression standard into FreeBSD, thus making it difficult to communicate between FreeBSD systems and most commercial operating systems, whose owners pay license fees for being able to decompress Stac data.  Upon inquiry, he explained:"))
(blockquote (ML ToW "The software you are referring to is the PPP Stac compression which is patented by Stac/HiFn. Although it is described with the exact details required to implement it, it seems to be impossible to actually code it (or better to release that code as source or binary) because of the patent rights Stac/HiFn holds.")) )

(lin
 (let ((MW "Martin Winkler <winkler@cfos.de>"))
   (ML Ano "Another leading ISDN developper explains") )
 (blockquote
  (linol (ML Tdg "der schaden ist" (en "The damage is"))
	 (ML MlW "die meisten ISDN switches koennen nur stac als datenkompression.  dies darf aber niemand patentfrei anbieten, so dass es eigentlich nur eine loesung fuer neuere MS betriebssysteme gibt. linux & co sehen da schon schlechter aus." (en "Most ISDN switches are aware of no other data compression than Stac.  But nobody is allowed to offer Stac-capable software without paying patent royalties.  The result is that proper ISDN communication is not possible outside of Microsoft operating systems."))
	 (ML Mav "die meisten datenkompressionspatente sind trivialpatente. es wird normalerweise immer das verfahren von storer et al oder das von lempel zif welsh (z.b. bei v.42bis) patentiert. beides ist aber immer schon vorher von denen veroeffentlicht worden. da praktisch jedes verfahren patentiert ist, kann keiner mehr irgendwelche verfahren entwickeln, die taugen. dieser schaden ist viel groesser." (en "Most data compression schemes are trivial.  Usually, they are based either on Storer or on LZW.  Since the basic algorithms have all been patented, there is not much point in trying to develop a patent-free alternative to Stac.  This damage is even greater than the damage caused by the fact that Stac is a de-facto standard.")) )
)
)
; swxai-stac-hifn
)

(mlhtdoc 'swxai-schnorr nil nil nil

(table nil '((nom) (usp)) (l (l (ML nam "name") (ML Utn "US patent number"))) (l 
 (l "Diffie-Hellman" "4,200,770") 
 (l "Hellman-Merkle" "4,218,582")
 (l "RSA" "4,405,829")
 (l "Hellman-Pohlig" "4,424,414")
 (l "Schnorr" "4,995,082")
))

(ul 
(ahs 'ipns-crypto-patent-challenge)
(ahs 'schlafly-pkp)
(ahs 'gnupg-rms-schnorr)
(ahs 'uni-frankfurt-schnorr)
(ah "http://www.google.com/search?q=%2BSchnorr+%2BPatent&hl=en&lr=&safe=off&btnG=Google+Search" (ML Grh "Google Search for Schnorr Patent"))
)
)

(mlhtdoc 'swxai-mbrola nil nil nil
(pre "
> -----Message d'origine-----
> De : Richard Stallman [mailto:rms@gnu.org]
> Envoyé : Tuesday, November 28, 2000 5:44 AM
> À : thierry.dutoit@fpms.ac.be
> Objet : Re: Patent numbers
>
>
>     No, we are in Europe. France Télecom too, but they have
> pentented PSOLA
>     (mostly) everywhere.
>
> Do they have a European patent as well?  If so, can you find the
> patent number?
>
>
>
------- End of forwarded message -------
"
) )


(mlhtdoc 'swxai-gif-lzw nil nil nil 
(linklist
(l (ahs 'lzw-license) (ML DiP "The license conditions of the patentee" (de "Die Lizenzbedingungen des Patentinhabers")))
(l (ahs 'lpf-gif) (ML Ude "Detailed Documentation" (de "Umfassende Dokumentation")))
(l (ahs 'boutell-gd)  (ML Gni "GIF support may not be included in free libraries.  A developper reports about his difficulties with Unisys." (de "Freie Bibliotheken dürfen nicht GIF unterstützen.  Ein Entwickler berichtet über seine Schwierigkeiten mit Unisys.")))
(l (ah "http://www.ghostscript.org" "Ghostscript") (ML Eop "EPO patent descriptions cannot be viewed, printed or manipulated under free Unix systems, because they contain LZW compression.  The Acrobat Reader provides only a very incomplete and non-free replacement.  It is tedious to view EPO patent descriptions with Acrobat, because they are organised as one PDF document per page.  Concatenating pages can normally be done using %(e:imagemagick), but this program too can not handle the LZW compression because of patent problems."))
(l (ah "http://www.gzip.org" "ZIP") (blockquote (ML Wba "What about patents?") (ML gpW "gzip was developed as a replacement for compress because of the UNISYS and IBM patents covering the LZW algorithm used by compress.") (ML IWd "I have probably spent more time studying data compression patents than actually implementing data compression algorithms. I maintain a list of several hundred patents on lossless data compression algorithms, and I made sure that gzip isn't covered by any of them. In particular, the --fast option of gzip is not as fast it could, precisely to avoid a patented technique.") (ML TyW "The first version of the compression algorithm used by gzip appeared in zip 0.9, publicly released on July 11th 1991. So any patent granted after July 11th 1992 cannot threaten gzip because of the prior art, and I have checked all patents granted before this date.") (ML Dlo "During my search, I found two interesting patents on a process which is mathematically impossible: compression of random data. This is somewhat equivalent to patents on perpetual motion machines. Check here for a short analysis of these two patents.")))
)

(lin
 (ML OWs "One company CEO wrote to us about his troubles with the Unisys patent licensing conditions:" (de "Ein Software-Unternehmer schrieb uns über seine schwierigkeiten mit den Unisys-Lizenzbedingungen:"))
 (blockquote
; CEO = "Jos Vernon - WebSupergoo <jos@websupergoo.com>"
; X = ImageGlue, Y = IIS
  (ML WsW "I make a product called X which is a Y web server extension for dynamic image manipulation. I'd like to include GIF support in the product but its difficult to know how to do this effectively. The Unisys licensing restrictions are so severe that I wouldn't be able to offer a free trial version. Also I don't even see that I'm really responsible since I'm just providing a tool to enable others to put a solution together. It's a standard story.")
  ) )
)

(mlhtdoc 'swxai-mpeg nil nil nil
(ul
(ahs 'mpeg-patents-faq)
(ahs 'mp3licensing)
(ahs 'mp3licensing-patents)
(ahs 'mp3-lame)
(ahs 'webnoize-mpeg)
)
)

(mlhtdoc 'swxai-ttf nil nil nil
(ul
(ahs 'truetype-patents)
(ahs 'freetype-patents)
)
)

(mlhtdoc 'swxai-ipix nil nil nil
(ul
(ahs 'noipix)
(ahs 'noipix-patents)
)
)

(mlhtdoc 'swxai-asf nil nil nil
(ul
(ahs 'virtualdub)
(ahs 'advogato-asfpat)
(ahs 'heise-asfpat)
)
)

(mlhtdoc 'swxai-msys nil nil nil
(ul
(ahs 'msys-response)
(ahs 'linux-mtd)
)
)

(mlhtdoc 'swxai-hyperlink nil nil nil
(ul
(ahs 'dignatz-btprodigy0012)
(ahs 'cnet-btprodigy0012)
(ahs 'idg-btprodigy0012)
(ahs 'bbc-btprodigy0012)
(ahs 'geek-btprodigy0012)
(ahs 'theregister-btprodigy001214)
(ahs 'theregister-btprodigy001215)
(ahs 'newsbytes-btprodigy0012)
(ahs 'infoworld-btprodigy0012)
)
)

(mlhtdoc 'swxai-rozmanith nil nil nil
"US 5,253,341"
(ul
(ahs 'callaw-tsahar0011)
(ahs 'ffii-tsahar0011)
) )

(mlhtdoc 'swxai-altavista nil nil nil 
(sects
(press (ML Dtc "The Start Shot" (de "Der Startschuss") (fr "Le Début"))

(filters ((pr ahs 'altavista-pr001113) (iw ahs 'altavista-iw010115)) (let ((CMGI (ah "http://www.cmgi.com" "CMGI"))) (ML ErA "On 2000-11-13, %{CGMI}, the holding that owns Altavista and 12 more companies, announce in a highly publicized %(pr:press release) that they own a monopololy on key areas of web searching.  At this time, CGMI was in a phase of %(q:restructuring), i.e. laying off employees and trying to become profitable.  On 2001-11-15, CGMI's CEO David Wetherell gave an %(iw:interview) in which he made it clear that he intended to crack down on anyone who searched the web without license:" (de "Am Tage 2000-11-13 gab der Konzern %{CGMI}, der Altavista und 12 weitere Firmen besitzt, in einer stark beworbenen %(pr:Pressemitteilung) bekannt, dass er 38 Patente auf wesentliche Elemente der Netzindexierung besitzt.  Zu diesem Zeitpunkt schrieb CGMI rote Zahlen und versuchte, bei Investoren um Zuversicht zu werben.  Am Tage 2000-01-15 erklärte der Chef von CGMI in einem %(iw:Interview) darüber hinaus, dass er die Verletzer seiner Patente in Kürze zu verfolgen gedenke und dass praktisch jeder, der das Internet oder auch nur sein firmeninternes Intranet absuche und indexiere, zu diesen Verletztern gehöre:") (fr "Le 2000-11-13, %{CGMI}, grande groupe qui possède Altavista et 12 autre sociétés Internet, lancait un %(pr:communiqué de presse) de haut profile, dans lequel il expliqua qu'il possède 38 brevets qui couvrent a peu près toutes les méthodes possibles d'indexation du WWW.  Le 2001-111-15, le PDG de CGMI dans un %(iw:entretien) expliqua que CGMI va en bref delai attaquer juridiquement tous ceux qui utilisent telles techniques sans payer, y inclus les mainteneurs de reseaux internes dans les entreprises."))))

(blockquote (dl
(l "Internet World" (ML CWb "Can we talk a bit about some of the ideas or opportunities that you backed off of because money was an issue this past year?"))
(l "David Wetherell" "[...]" (ML EWn "Even though AltaVista s doing well in the advertising space, we just think that in order to really ensure strong growth they ought to leverage their position in search licensing to a greater extent. And we saw the opportunity to do that because we think it s a big market. They happen to own 38 patents, many of which we think are fundamental in the search area. They were the first to spider and index the Web. And Digital did a good job of recognizing the potential value of that intellectual property. And they were very thorough in filing broad and deep and narrow patents. And we have another 30 patents that are in application. So we believe that virtually everyone out there who indexes the Web is in violation of at least several of those key patents."))
(l "IW" (ML Dep "Does that mean you ll pursue that?"))
(l "DW" (ML Yis "Yes, we will. Coming up in the first quarter of 2001."))
(l "IW" (ML Ssa "So we may see some lawsuits ..."))
(l "DW" (ML Ile "If necessary, we will defend it, to the letter of the law."))
(l "IW" (ML Aih "Are there any specific examples of the types of patents?"))
(l "DW" (filters ((pr ahs 'altavista-pr001113)) (ML Ihs "If you index a distributed set of databases what the Internet is and even within intranets, corporations, that s one of the patents.  We did a %(pr:press release) on this with a list of six or ten of the key areas that the patents cover."))
)))
)

(patents (ML EWa "Patent Samples" (de "Einige der Patente") (fr "Quelques Brevets"))

(ML DWW "Most of the European applications are still awaiting examination at the EPO." (de "Die meisten europäischen Anträge warten beim EPA noch auf ihre Erteilung.") (fr "La plupart des brevets Européens n'ont pas encore été examiné."))

(linklist
(l (ahpat 'US 5864863)
  (lin (ml "I claim")
  (ol (nl (ml "A system for indexing Web pages of the Internet, the pages stored in computers connected to each other by a communications network, each page having a unique URL (universal record locator), some of the pages including URL links to other pages, comprising: a communication interface for fetching a batch of specified pages of the Web from the computers in accordance with the URLs and URL links;") 
	(ml "a parser sequentially partitioning the batch of specified pages into indexable words, each word representing a portion of one specified page or an attribute of one or more portions of the specified page, the parser sequentially assigning locations to the words as they are parsed;")
	(ml "a memory storing index entries, each index entry including a word entry representing a unique one of the words, and one or more location entries indicating where the unique word occurs in the Web;")
	(ml "a query module parsing a query into terms and operators relating the terms;")
	(ml "a search engine using object-oriented stream readers to sequentially read location of specified index entries, the specified index entries corresponding to the terms of a query;")
	(ml "and a display module for presenting qualified pages located by the search engine to users of the Web.")))) )
(l (ahpat 'US 5974455) (ol (lin (ml "A system for locating Web pages stored on remotely located computers, each Web page having a unique URL (universal resource locator), at least some of said Web pages including URL links to other ones of the Web pages, the system comprising:") (ul                             
(ml "a communications interface for fetching specified ones of the Web pages from said remotely located computers in accordance with corresponding URLs;")
(ml "a Web information file having a set of entries, each entry denoting, for a corresponding Web page, a URL and fetch status information;")
(ml "a Web information table, stored in RAM (random access memory), having a set of entries, each entry denoting a fingerprint value and fetch status information for a corresponding Web page; and")
(ml "a Web scooter procedure, executed by the system, for fetching and analyzing Web pages, said Web scooter procedure including instructions for fetching Web pages whose Web information file entries meet predefined selection criteria based on said fetch status information, for determining for each URL link in each received Web page whether a corresponding entry already exists in the Web information table, and for each URL link which does not have a corresponding entry in the Web information table adding a new entry in the Web information table and a corresponding new entry in the Web information file.") ))) )

(l (sepcc " = " (ahpat 'EP 444358) (ahpat 'US 5495608)) (ml "Dynamic optimization of a single relation access."))
(l (sepcc " = " (ahpat 'EP 458698) (ahpat 'US 5276868)) (ml "Method and apparatus for pointer compression in structured"))
(l (sepcc " = " (ahpat 'EP 520459) (ahpat 'US 5347653)) (ml "A method and apparatus for indexing and retrieval of object versions in a versioned data base."))
(l (sepcc " = " (ahpat 'EP 0522363) (ahpat 'US 5204958)) (ml "System and method for efficiently indexing and storing large database with high-data insertion frequency."))
(l (sepcc " = " (ahpat 'EP 0551243) (ahpat 'US 5519858)) (ml "Address recognition engine."))
(l (tpe (ahpat 'EP 0567355) (ML 0 "combination of 5 US applications" (de "Kombination aus 5 US-Anmeldungen") (fr "combinaison de 5 applications US"))) (ml "A method and apparatus for operating a multiprocessor computer system having cache memories.") )
(l (ahpat 'EP 0886227) (ml "Full-text indexed mail repository"))
(l (ahpat 'EP 0886228) (ml "WWW-based mail service system"))
(l (ahpat 'US 5226150) (ml "Apparatus for suppressing an error report from an address for which an error has already been reported"))
(l (ahpat 'US 5276872) (ml "Concurrency and recovery for index trees with nodal updates using multiple atomic actions by which the trees integrity is preserved during u "))
(l (ahpat 'US 5276874) (ml "Method for creating a directory tree in main memory using an index file in secondary memory"))
(l (ahpat 'US 5394143) (ml "Run-length compression of index keys"))
(l (ahpat 'US 5506984) (ml "Method and system for data retrieval in a distributed system using linked location references on a plurality of nodes"))
(l (ahpat 'US 5553258) (ml "Method and apparatus for forming an exchange address for a system with different size caches"))
(l (ahpat 'US 5671406) (ml "Data structure enhancements for in-place sorting of a singly linked list"))
(l (ahpat 'US 5717921) (ml "Concurrency and recovery for index trees with nodal updates using multiple atomic actions"))
(l (ahpat 'US 5745890) (ml "Sequential searching of a database index using constraints on word-location pairs "))
(l (ahpat 'US 5745894) (ml "Method for generating and searching a range-based index of word-locations"))
(l (ahpat 'US 5745898) (ml "Method for generating a compressed index of information of records of a database"))
(l (ahpat 'US 5745899) (ml "Method for indexing information of a database"))
(l (ahpat 'US 5745900) (ml "Method for indexing duplicate database records using a full-record fingerprint"))
(l (ahpat 'US 5765158) (ml "Method for sampling a compressed index to create a summarized index"))
(l (ahpat 'US 5765168) (ml "Method for maintaining an index"))
(l (ahpat 'US 5787435) (ml "Method for mapping an index of a database into an array of files"))
(l (ahpat 'US 5794242) (ml "Temporally and spatially organized database"))
(l (ahpat 'US 5797008) (ml "Memory storing an integrated index of database records"))
(l (ahpat 'US 5809502) (ml "Object-oriented interface for an index"))
(l (ahpat 'US 5829051) (ml "Apparatus and method for intelligent multiple-probe cache allocation"))
(l (ahpat 'US 5832500) (ml "Method for searching an index"))
(l (ahpat 'US 5852820) (ml "Method for optimizing entries for searching an index"))
(l (ahpat 'US 5864863) (ml "Method for parsing, indexing and searching world-wide-web pages"))
(l (ahpat 'US 5915251) (ml "Method and apparatus for generating and searching range-based index of word locations"))
(l (ahpat 'US 5953747) (ml "Apparatus and method for serialized set prediction"))
(l (ahpat 'US 5956758) (ml "Method for determining target address of computed jump instructions in executable programs"))
(l (ahpat 'US 5963954) (ml "Method for mapping an index of a database into an array of files"))
(l (ahpat 'US 5966710) (ml "Method for searching an index"))
(l (ahpat 'US 5966735) (ml "Array index chaining for tree structure save and restore in a process swapping system"))
(l (ahpat 'US 5970497) (ml "Method for indexing duplicate records of information of a database"))
(l (ahpat 'US 5987544) (ml "System interface protocol with optional module cache"))
(l (ahpat 'US 6016493) (ml "Method for generating a compressed index of information of records of a database"))
(l (ahpat 'US 6021409) (ml "Method for parsing, indexing and searching world-wide-web pages"))
(l (ahpat 'US 6029164) (ml "Method and apparatus for organizing and accessing electronic mail messages using labels and full text and label indexing"))
(l (ahpat 'US 6047286) (ml "Method for optimizing entries for searching an index"))
(l (ahpat 'US 6067543) (ml "Object-oriented interface for an index"))
(l (ahpat 'US 6078923) (ml "Memory storing an integrated index of database records"))
(l (ahpat 'US 6092101) (ml "Method for filtering mail messages for a plurality of client computers connected to a mail service system"))
(l (ahpat 'US 6105019) (ml "Constrained searching of an index"))
(l (ahpat 'US 6108770) (ml "Method and apparatus for predicting memory dependence using store sets"))
(l (ahpat 'US 6112203) (ml "Method for ranking documents in a hyperlinked environment using connectivity and selective content analysis"))
(l (ahpat 'US 6138113) (ml "Method for identifying near duplicate pages in a hyperlinked database")) 
)
)

(etc (ML Fea "Further Reading" (de "Weitere Lektüre") (fr "Autres Textes"))
(ul
  (ah "http://www.wired.com/news/technology/0,1282,41508,00.html?tw=wn20010131" (colons "Wired" "CMGI Claims Patently Wrong"))
  (ah "http://www.businesswire.com/webbox/bw.012901/210290071.htm" (colons "Businesswire" "archie creator willing to challenge CMGI claims"))
)
)
)
;swxai-altavista
)

(mlhtdoc 'swxai-xpointer nil nil nil

(linklist
(l (ah "http://www.xml.com/pub/a/2001/01/17/xpointer.html" (colons "XML.com 2001-01-17" "XPointer and the Patent")) (ML Alb "detailed analysis of the case" (de "Ausführliche Analyse des Falls") (fr "exposé détaillé du cas")))
(l (ah "http://164.195.100.11/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=/netahtml/srchnum.htm&r=1&f=G&l=50&s1=%275,659,729%27.WKU.&OS=PN/5,659,729&RS=PN/5,659,729" (ML Apc "claims" (de "Ansprüche") (fr "revendications"))) (ol (ml "A method executed in a network computer system for facilitating access to a specified portion of data stored at a remote location, the method comprising the steps of: %(ul:retrieving a source document, the source document including hypertext links to other data on the network;:displaying the source document;:receiving input entered on the source document;:determining whether the input comprises selection of a remotely specified named anchor;:when the input comprises selection of a remotely specified named anchor, retrieving data indicated in the remotely specified named anchor and displaying a portion of the data specified in the remotely specified named anchor, wherein the specified portion of the data does not have a position marker associated with it.)") "...")
)
(l (ahv 'w3-p3p) (let ((LST (etc "Microsoft" "Intermind" "P3P"))) (ML Bie "Already in 1999, the W3C was plunged into a crisis, because %{LST} patented some of the principles involved in the hypertext principles which they were just formulating into a new standard.  The W3C now has to devote a large part of its limited ressources to fight patents." (de "Bereits 1999 wurde das W3C in eine Krise gestürzt, weil %{LST} einige der grundlegenden Hypertext-Prinzipien patentiert hatten, die gerade zu einem neuen Standard formuliert wurden.  Das W3C ist seitdem gezwungen, einen beträchtlichen Teil seiner begrenzten Ressourcen für die Abwehr von Patentgefahren zu verwenden.") (fr "Déja en 1999 le W3C est plongé dans une crise par ce que %{LST} avait breveté quelques principes de base qui était en train d'être formulé dans un nouveaux standard.  Depuis ce temps le W3C est contraint a utiliser une partie considérable de sés ressources pour se défendre contre le danger de brevets."))))
(l (ahv 'webcaught00-reed) (tan (ML Tya "The CEO of a company that plunged the W3C into a crisis and later, under political pressure, granted everybody a free license to his patents, interprets this process as a great success of the patent system and of the %(q:Free Market)." (de "Der Chef einer Firma, die das W3C in die Krise gestürzt und später unter politischem Druck ihre Patente der Allgemeinheit zur freien Verfügung gestellt hatte, erklärt diesen Vorgang als einen Erfolg des Patentsystems und des %(q:Freien Marktes).") (fr "Le PDG d'une société qui avait plongé le W3C dans une crise et, sous pression politique, promis une license gratuite a tous pour ses brevets, interpréte cette histoire comme un grand succés du système de brevets et du %(q:marché libre).")) (filters ((ip ahs 'eu-dg15-ipi-study)) (ML Age "At %(ip:another occasion), Reed predicted that even Free Software authors will eventually benefit from the patent system." (de "Bei %(ip:anderer Gelegenheit) sagte Reed vorher, dass sogar die Autoren Freier Software lernen würden, sich das Patentsystem zu Nutze zu machen.") (fr "À une %(ip:autre occasion), Reed predit que même les auteurs de logiciels libres allait apprendre a profiter du système de brevets.")))))
)
;swxai-xpointer
)
;swpikxrani-dir
)
;swpatpikta-dir
) ; (docfiles2ml "descr" docid)

(list 'swpatminra-dir
(mlhtdoc 'swpatminra nil nil nil
(filters ((ft ah "ftp://ftp.ffii.org/pub/swpat/swpat.tgz") (lp ah "ftp://ftp.ffii.org/pub/swpat/swpatlpr.tgz") (gz ah "http://www.gzip.org"))
 (ML Diu "Die interessantesten Netzinhalte zu diesem Thema sammeln wir ständig in einem %(ft:komprimierten Paket), dass Sie mit %(gz:GNU Tar/Zip) auspacken und auf einem beliebigen Webserver darstellen können." (en "We regularly pack of these contents into a %(ft:compressed package) that you can unpack using %(gz:GNU Tar/Zip) and place it on your web server at home."))) )

(mlhtdoc 'swpatsisku
  (ML Fsu "Forschungsarbeiten über die Volkswirtschaftlichen Auswirkungen von Softwarepatenten" "Research on the MacroEconomic Effects of Software Patents")
  (lin nil (ML Vbd "Von unabhängiger Seite gibt es hingegen einige Studien, die wir hier sammeln." "There are however some independent studies which we collect here."))
  (append nil (list (ML Ptc "Patentforschung" "patent research") (ML Fnh "Folgenabschätzung" "effect estimation") (ML Wnn "Wissensökonomie" "Knowledge Economy")))
  (dl
  (l (ahv 'mit-seqinnov) (ML Fnr "Forschungspapier der Wirtschaftswissenschaftlichen Fakultät des Massachusetts Institute of Technology.  Die beiden Autoren stellen fest, dass die amerikanische Softwarepatentierungspraxis zumindest zu keinem merklichen Anstieg der Innovation oder der Forschungsinvestitionen in den letzten Jahren geführt hat und erklären ihre Feststellungen mit einem mathematischen Modell der %(q:sequentiellen Innovation).  Mit diesem Modell lässt sich nachrechnen, dass im Bereich der Information, der Software, der Beratungsdienste u.a. die technische Entwicklung schneller voranschreitet, wenn keine harten %(q:gewerblichen Schutzrechte) gewährt werden.  Das Urheberrecht hingegen erlaubt im MIT-Berechnungsmodell eine nahezu optimale Balance der verschiedenen, z.T. zueinander gegenläufigen Faktoren, von denen der technische Fortschritt bestimmt wird." "This article is written by two researcher from MIT and concludes, after giving mathematical models and experimental evidence, that in a dynamic world such as the software industry or consulting industry, firms may have plenty of incentive to innovate without patents and patents may constrict complementary innovation. It concludes that copyright protection for software programs (which has gone through its own evolution over the last decade) may have achieved a better balance than patent protection. This new model suggests another, different rationale for narrow patent breadth than the recent economic literature on this subject."))
  (l (ahv 'useright-pdf) (ML VSe "Ein mathematisches Modell zur Beschreibung der volkswirtschaftlichen Auswirkungen von Softwarepatenten und ein Konzept zur Linderung der Probleme:  Nutzungsrecht (Patentrecht) vs Kopierrecht (Urheberrecht): Patente sollen nicht beim Kopieren sondern erst bei der Anwendung ansetzen." "A mathematical model describing the economic effects of sofware patents and a concept for solving some of the problems:  the distinction between copyright and useright."))
  (l (ah "/ag/intern/doku/thurow.pdf" "Lester C. Thurow: Needed: A New System of Intellectual Property Rights") (commas "Harvard Business Review" "Reprint 97510") (quot "Squeezing today's innovations into yesterday's system simply won't work"))
) )

(mlhtdoc 'swpatlanle nil nil nil
 (dl 
   (l (ahv 'shulman-tangle) (let ((SS "Seth Shulman") (TR (tpe (citt "Technology Review") "MIT"))) (ML Gnf "%{SS} zeigt in %{TR}, wie Patente das Internet gefährden.  Zitat:" "How patents are endangering the Web" "montre les dangers des brevets sur le Web.  Quote:") (blockquote "Just because software hasn't experienced a cyber-Bhopal doesn't mean it won't ever happen. Indeed, the noxious clouds of litigation now gathering around e-commerce are renewing industry fears. ... There's ample historical evidence that overly broad patents have stifled innovation in emerging industries. ... In the early years of aviation in the United States, Orville and Wilbur Wright fought a largely successful nine-year campaign to enforce their broad patent on the airplane. While innovators helped aviation thrive in Europe, the Wright brothers' patent crippled American industry until the outbreak of World War I, when the U.S. government forced the Wrights to license their technology so that planes could be built more expeditiously for the war effort.")))
   (l (ahv 'heise) (dl
     (l (ahv 'heise-te-2911) (ML Ssm "Telepolis-Gespräch mit Richard Stallman"))
     (l (ahv 'heise-te-2856) (ML Hme "Hauptthema des Telepolis-Artikels ist Stallmans Engagement gegen Softwarepatente, wobei auch FFII zitiert wird."))
     (l (ahv 'heise-patentfluten) (ML Bnn "Bei unseren Politikern fehlt es an Verständnis für die Ökonomie der Wissensgesellschaft.  Im blinden Vertrauen auf den Markt einerseits und das Patentwesen andererseits droht die Allmende Internet in die Brüche zu gehen. Der Autor führt zahlreiche ärgerliche Patente an und analysiert insbesondere die Patentpraxis von Microsoft."))
     (l (ahs 'rmscall) (ML NWb "Richard M. Stallman ruft am Datum 1999-05-02 europäische Softwareautoren zu politischem Handeln auf.")) ) )
   (l (ahv 'aful-cp-patents) (ML DeW "Die Französischsprachige Vereinigung der Anwender von Linux und Freier Software argumentiert gegen eine Ausweitung des Patentschutzes auf Software und warnt Politiker davor, europäische Standortvorteile aufzugeben."))
   (l (ahv 'devlinux-deutsch) (ML PnW "P. Deutsch hat durch Entwicklung freier Software eine Firma zu geschäftlichem Erfolg geführt und wird geht jetzt in den Ruhestand weiter programmieren.  Ihm ist zu verdanken, dass Postscript systemübergreifend funktioniert.  Sein Lebenswerk war nur möglich, weil Software auch in den USA erst recht spät vom Patentrecht betroffen war.  Laut Deutsch bietet das Patentrecht im Gegensatz zum Urheberrecht für Software keinen fairen Investitionsschutz." "A lifelong developper of cutting-edge free software is now retiring from business.  He explains that his great productivity and success would not have been possible if software patenting had started earlier."))
   (l (ahv 'lessig-amazon) (ML Heh "Harvard-Rechtsprofessor Lessig sieht staatliche Monopolgewährungswut auf dem Wege, einen sich von selbst organisierenden Markt zu ersticken"))
   (l (ahv 'techweb-amazon) (ML Tie "Techweb über die Bedeutung des Falles Amazon"))
   (l (ahv 'lea-ms-swpat) (ML Vow "Verbraucheranwalt Nader pflichtet Richard Stallman bei, der vor Stärkung der Monopolposition von M$ durch Softwarepatente warnt und eine Verpflichtung auf defensiven Gebrauch fordert."))
   (l (ahv 'redherring-sue) (ML Dtd "Das Leib-und-Magen-Blatt amerikansicher Investoren warnt vor Gefährdung des Neuen Marktes durch eine softwarepatent-bedingte Prozessflut" "Legal strife about to shake the New Market"))
   (l (ahv 'upside-contrinfr) (ML Dms "Der breite Wirkungskreis von E-Kommerz-Patenten führt zu einer Prozessflut, vor der niemand sicher ist."))
   (l (ahv 'schumer-expansive) (ML Usd "US-Senator Schumer erklärt, das US-Patentamt fordere mit seinen selbstsüchtig-expansionstischen Praktiken die US-Bundesregierung womöglich zu kurzfristigen Notverordnungen und sonstigen ungewollten Übergriffen in den ansonsten autonomen Bereich des Patentwesens."))
   (l (ahv 'unpoison) (ML Eit "EPA-Patentanmeldung für triviale Methode zur Zweckentfremdung von HTTP-Adressen"))
   (l (ahv 'sd-y2k-pat) (ML Dtv "Diskussion über Patentierung von trivialer J2000-Lösung"))
   (l (ahv 'sd-990923) (ML Dnl "Der drittgrößte Softwarehersteller der Welt hält nichts von Softwarepatenten, hat aber dennoch viele Softwarepatente zu defensiven Zwecken erworben."))
   (l (ahv 'atlantic-991103) (ML AWA "Artikel in TheAtlantic"))
   (l (ahv 'cw-mpi9911) (ML CaW "Computerwoche berichtet über eine Tagung des Max-Planck-Institus über IT-Patentierung und kommt zu dem Schluss, Softwarepatente seien schädlicher Ballast."))
   (l (ahv 'pcweek-4153) (ML Oag "Ärger mit einem Patent auf grundlegende WWW-Techniken"))
   (l (ahv 'wired-19473) (ML Utt "US-Patentamt mit Softwarepatenten überfordert"))
   (l (ahv 'wired-21726) (ML Ere "Eine weitere E-Kommerz-Firma versucht, sich grundlegende WWW-Techniken %(q:schützen) zu lassen"))
   (l (ahv 'patently-absurd) (ML EnW "Ein Kenner beschreibt das amerikanische %(h:Software:Patentsystem) von seiner tragikomischen Seite.") )
   (l (ahv 'webnoize-mpeg) (tan (ML Wcu "Warum Patentschutz für Kathedralen-Technologien wie die Audiokompression durchaus angemessen und fair sein kann") (linol (ML Pee "Probleme hierbei sind") (ML Deo "Das Patentsystem bietet derzeit keine Möglichkeit, die Kathedralen-Technologien von anderen zu unterscheiden") (ML Wkr "Was heute eine Kathedralen-Technik ist, funktioniert vielleicht morgen bereits nach dem Basar-Modell") (ML NeW "Nichts verpflichtet den Patentinhaber, so umsichtig mit seinen Rechten umzugehen, wie im hier beschriebenen MP3-Fall geschehen.  Räuberische Monopoltaktiken sind oft besser für den Aktienkurs.") (ML EWp "Einer softwareverträglicheren Ausgestaltung des Patentrechts stehen internationale Verpflichtungen wie der TRIPS-Vertrag entgegen.")) ) ) ) )

(mlhtdoc 'swpatfapro 
 (ML Aas "Argumente von Patentinflations-Kritikern und sonstigen Informatikern" (en "opinions of patentability opponents and other informaticians"))
 nil nil
(sects
(sysdoku (ML Sse "Systematisch Dokumentationen" "Systematic Documentation")
 (dl
 (l (ahv 'freepatents)
    (linul
     (ML sme "systematische Dokumentation der Fragen, um die es geht." "sytematic documentation of the issues at stake") ) 
      )
 (l (ahv 'cptech-ip) (ML wcs "Ralph Nader warnt im Namen des Verbraucherschutzes vor monopolistischen Auswüchsen des geistigen Eigentums"))
 (l (ahv 'lpf-patents)
    (lindl
     (ML Imo "Initiative von Richard Stallman u.a., die um 1990 das Problem ausführlich beleuchtete." "Documentation by RMS and others")
     (l (ahv 'rms-amazon (ML Rft "RMS calls for boycott of Amazon" (de "Stallman ruft dazu auf, keine Bücher bei Amazon zu bestellen"))) (ML AWc "Amazon hat sein triviales %(q:1-Klick-Patent) genutzt, um Konkurrenten zu torpedieren.  Nicht alles, was legal ist, ist auch anständig.") ) ) ) 
 (l (ahv 'ipnsinfo) 
    (linul
     (commas (ML Ngu "Nachrichten-Mailingliste mit Schwerpunkt Softwarepatente") (ML wtr "wichtige Informationsquelle"))
     (colons (ahv 'bustpatents) (ML Phe "Patentinfo-Suchdienst von Gregory Aharonian")) ) )
 (l (ahv 'no-patents)
  (linul
   (ML Iae "Italienische Dokumentation über die Gefahren von Softwarepatenten")
   (ahv 'nopatents-logo (ML DWP "Das Logo %(q:No Patents)"))
   (ahv 'didone-paolo (ML DWt "Der Autor")) ) )
 (l (ahv 'iris (ml "IRIS - Imaginons un Réseaux Internet Solidaire")) 
    (lindl
     (ML OWr "Organisation für ein nicht-kommerziell orientiertes Internet")
     (l (ahv 'iris-suite-munich "suivi Convention de Munich") (ML Srt "Stellungnahmen diverser, vor allem französischer Politiker nach der letzten Konferenz der Patentorganisationen") ) ) )
 (l (ahv 'critto-antipat) (let ((KS "Krystof Sobolewszky")) (ML ciP "%{KS}s polnische Anit-Patent-Seite" "%{KS}s Polish AntiPatent page")))
 (l (ahv 'game-antipat) (ML And "Akute Patentgefahren haben Entwickler und Anwender von Computerspielen zur Bildung eines Aktionsbündnisses veranlasst." "Patents endanger the Computer Games industry."))
 ) )
) )

(mlhtdoc 'swpatpikci nil nil nil
  (dl
   (l (ahs 'miertltr) (ML HW1 "Hierbei kamen zwischen Juni und August 1999 annähernd 10000 Unterschriften zustande" "This accumulated approximately 10000 signatures between June and August." "Entre Juni et Aout de 1999 enviorn 10000 signatures etaient accumulé."))
   (l (ahs 'louvain-pet-sign) (ML EWW "Eine Kampagne belgischer Studenten, die in kurzer Zeit 2200 Unterschriften aus dem französischen Sprachraum sammelte." "A campaign of Belgian students, who collected 2200 signatures mainly from the French speaking area." "Une action d'etudiants belges qui accumulait 2200 signatures surtout dans l'Europe francophone."))
   (l (ahs 'no-patents-monti) (ML Aah "Eine italienischsprachiger Brief an Mario Monti und andere Mitglieder der EU-Kommission, der knapp 1000 Unterschriften sammelte." "An italian language letter to Mario Monti and other European Commission member, which collected approximately 1000 signatures." "Une lettre en langue Italienne a M. Monti et autres membres de la Commission Européenne qui a accumulé environ 1000 signatures."))
   (l (ahs 'eulux-petition) (filters ((el ahs 'eurolinux)) (ML Efl "Ein neuer Anlauf des %(el:Eurolinux-Bündnisses)" "A new attempt of the %(el:Eurolinux Allicance)" "Une nouvelle campagne de l'%(el:Alliance Eurolinux)")))
 ) )
 
(mlhtdoc 'swpatsarxe
 (ML Wss "Wie könnte man das SWPAT-Ungeheur zähmen?" "How to tame the SWPAT monster")
 (ML Dld "Die Idee, ein Bündnis zu bilden, um mithilfe von Patenten freie Software gegen Patentangriffe zu schützen, ist alt.  Informationelles Gemeineigentum ist jedoch als Faustpfand nur von begrenztem Wert, insbesondere wenn der Streitgegner selber gar keine Software sondern nur Patente entwickelt.  Bisher waren Bündnisse dieser Art erfolglos.  Einige Änderungen am Patentsystem könnten jedoch helfen, die Erfolgsaussichten zu erhöhen." "The idea to put patents into a pool to protect free software is old.  Public Informational Property is however hardly usable for excluding others, especially when your opponent is a professional extortionist who develops no software but only trades with patents.  So far the mutal defense idea has not been very successful.  The chances for success could improve somewhat, if the patent system itself was reformed, so as to provide some mechanisms for adequately protecting public %(q:intellectual property).")
 nil
 (dl
   (l (ahv 'lpf-mutual-defense) (let ((RMS "Richard Stallman")) (ML Ese "Ein klarsichtiger Text aus %{RMS}s Feder" "A lucid text from %{RMS}")))
   (l (ahv 'fourmilab-pato) (ML Eer "Ein Gründer einer erfolgreichen amerikanischen Softwarefirma überlegt sich, wie er gegen den %(q:Patent-Schwachsinn) seines Landes erfolgreich vorgehen kann und setzt Hoffnungen in ein Schutzbündnis" "Autodesk founder would like to ban patent nonsense, but sees a mutual defense alliance as the most realistic option."))
   (l (ahv 'openpatents) (ML Ele "Ein gut durchdachter, liebevoll konstruierter Ansatz, der theoretisch funktionieren sollte."))
   (l (ahv 'lw-199811) (let ((BP "Bruce Perens")) (ML cta "%{BP} sieht OSS-feindliche Patentkartelle am Horizont und schlägt Gegenmaßnahmen vor, u.a. ein Schutzbündnis." "%{BP} proposes remedies against an imminent flood of patent attacks on OSS.")))
   (l (ahv 'gehring-openpatents) (filters ((tr ahs 'swpbpeude) (bk ahs 'swpbmwi25)) (ML GaS "Gehring schlug bei der %(bk:Berliner Konferenz) vor, man solle einerseits Software patentierbar machen, aber andererseits ein Schutzabkommen für freie Software schaffen.  Durch kleinere Reformen am Patentrecht seien die Voraussetzungen für ein erfolgreiches Schutzbündnis der Freien Software zu verbessern.  Insbesondere sollte eine opensource-freundliche Patentvariante angeboten werden, die auch dann beantragt werden könne, wenn seit der Veröffentlichung der Technik eine gewisse maximale Zeit (Schonfrist) verstrichen sei." "Gehring proposes some minor reforms to patent law which could help make it more favorable to opensource defense alliances.")))
   (l (ahv 'smets-gppl) (filters ((gp pet "GPPL") (lt ahs 'swplxtg26) (gf ah "http://ffii.org/archive/mails/swpat/2000/Jul/0108.html")) (ML Vdn "Vorschlag einer ansteckenden %(gp:Allgemeinen Öffentlichkeits-Patentlizenz) und weiterer Kampftaktiken für den Bürgerkrieg um die Parzellierung der Geistigen Welt.  Im Anschluss an eine %(lt:Vortrags- und Diskussionsveranstaltung mit J.P. Smets auf dem Linuxtag 2000) entstanden %(gf:Überlegungen über weitergehende GPPL-Konzepte), wonach ein GPPL-Patent ausschließlich und bedingungslos in %(e:als ganzes unter GPL reimplementierbaren Systemen) genutzt werden darf, während ein FPL-Patent in allen GPL-Programmen eingesetzt darf und ansonsten seinem Eigentümer gehört.  Durch Sammlung vieler GPPL-Patente könnte ein Druck auf Patentinhaber entstehen, ihre eigenen Patente unter FPL zu stellen." "Proposal of a contagious %(gp:General Public Patent License).")) )
   )
)



(mlhtdoc 'swpatbandu 
 (ML Ots "Argumente von Patentinflations-Apologeten und sonstigen Juristen" (en "Opinions of patentability champions and other lawyers"))
 nil nil
 (ul
 (linul (ah "http://www.heckel.org" (tpe "Intellectual Property Creators" "IPC")) 
   (colons (ah "http://www.heckel.org/Heckel/ACM%20Paper/acmpaper.htm" "Debunking the Software Patent Myths") (lin (ML Htl "Heckel verteidigt das Softwarepatentsystem und %(q:kapitalistische Erfinderfirmen) wie AT&T gegen die %(q:marxistischen) Angriffe von Richard Stallman und einigen %(q:Software-Amateuren), die im wesentlichen nur anderer Leute geistiges Eigentum %(q:nachbauen) und %(q:verschenken), und deren Zeit vorbei sein wird, sobald sich das Patentsystem erfolgreich auf die Gegebenheiten der Software eingespielt hat.  Behauptungen der Patentgegner, die Gegebenheiten der %(q:Softwareindustrie) unterschieden sich wesentlich von denen der mechanischen und chemischen Industrie, entlarvt Heckel als %(q:Mythen).")
) ) )
 (linul (tpe (ah "http://www.breese.fr" (mltr "Breese & Majerowicz")) (ML ewW "eine französische Patentanberatungskanzlei, die sich für Softwarepatente stark macht"))
   (tpe (ah "http://www.breese.fr/guide/htm/Logiciel/cig.htm" (mltr "La bataille de la protection des logiciels")) (filters ((af ah "http://www.aful.org")) (ML Ire "In diesem Artikel sagt Herr Breese der %(af:Französischsprachigen Gesellschaft für Linux und Freie Software AFUL) recht böse Dinge nach.  Den Vertretern der freien Software geht es, so Breese, darum, sich die Früchte von anderer Leute Arbeit kostenlos anzueignen.  Interessanterweise gibt er unumwunden zu, dass die derzeitige europäische Software-Patentierungspraxis nur unter größten %(q:intellektuellen Verrenkungen) als legal angesehen werden kann.")))
  (tpe (ah "http://www.zdnet.fr/actu/logi/a0009838.html" (ML Peb "Pressebericht über den Streit der Patentgegner und -befürworter in Frankreich.")) (ML Bae "Breese wird als Wortführer der Patentpartei, AFUL und FFII als Vertreter der Gegenpartei genannt."))
  (tan (tpe (ah "http://www.zdnet.fr/actu/logi/a0009837.html" (ML LWW "Les logiciels peuvent déja être breveté")) (ML Ioi "Interview von Breese mit ZDNET.FR")) (lin (ML Htu "Hauptargumente") (table nil '((pro) (con)) (l (l "Breese" "FFII")) (l (l (ML DWn "Die Softwarepatentgegner vertreten die Interessen von Großkonzernen, die nur OpenSource vorschieben, um ihre Monopolstellung zu behaupten.  Patente dienen zur Verteidigung der Interessen des kleinen Mannes.") (ML Bie "Breese weiß als Patentanwalt sicherlich, dass er hier nur einen Goldgräbermythos kultiviert, den jeder Blick in eine Patentstatstik schnell widerlegt.")) (l (ML SiW "Schon bisher kann Software in Europa patentiert werden, indem man sie als %(q:industriellen Prozess) verkleidet.  Diesem falschen Treiben sollte ein Ende gesetzt werden.") (ML Bvo "Breese weiß (und gibt zu), dass er solche verkleideten Ansprüche derzeit vor Gericht nur begrenzt geltend machen kann."))))))
 (let ((FPs (ahh "www.freepatents.org")) (FP (ahh "www.freepatent.org"))) (ML NWm "Neuerdings betreiben Breese & Co als Trittbrettfahrer des bekannten Namens %{FPs} ein Netzangebot unter dem Namen %{FP}.  Verletzung %(q:geistigen Eigentums)?!" "Recently they have been taking a free-ride on the well-known site %{FPs} and build a counter-site called %{FP}, which preaches the %(q:freedom to protect one's intellectual property).  We agree with them in our moral disapproval of all kinds of free-ride and copycat behavior.")) )

 (tpe (ah "http://www.kianispringorum.de/weichware.htm" (ML ZiW "Zum patentrechtlichen Schutz von Softwareprodukten")) (filters ((sh ah "http://www.kianispringorum.de/springorum.htm")) (ML Ens "Ein %(sh:Düsseldorfer Software-Patentanwalt) erklärt seinen Mandanten, warum Patentschutz für Software de facto schon heute in Deutschland möglich und erstrebenswert ist.")))
 (tan (tpe (ah "http://dialspace.dial.pipex.com/town/street/xge21/software.htm" (colons "Fry, Heath & Spence" "Patenting of Computer Software")) (ML BWW "Britische Patentanwälte klären Kunden über die Chancen, heute und künftig Software zu patentieren, auf.  Gute Zusammenfassung umstrittener Fälle am EPA." "Chances of obtaining European software patents.  Good summary of EPO border cases.")) (pf dank (ahs 'blasum)))
 (tpe (ah "http://www.kuesterlaw.com/swpat.html" "Patents for Software-Related Inventions") (ML eei "ein amerikanischer Patentanwalt klärt seine Mandanten auf.  Recht gründlich, mit vielen Verweisen auf weitere Quellen."))
 (tpe (ahv 'techlaw-19991217) (ML Erk "Eine führende amerikanische Juristenzeitschrift berichtet begeistert über einen %(q:Goldrausch).  Amazon habe die Zeichen der Zeit erkannt.  Derzeit helfen Patentanwälte Großunternehmen bei der Hebung unterirdischer Schätze.  50 der größten 500 US-Unternehmen setzen eine für 50000 USD lizenzierte Software namens Aureka (%(q:Ich habe Gold gefunden!)) ein, die Softwarepatente auswertet und unabgedeckte Bereiche erschließt.  Die Techniker werden angeleitet und bezahlt, um patentierbare Techniken, die ihnen normalerweise trivial erscheinen würden, zu identifizieren. HP konnte durch eine gezielte Goldsuchaktion die Zahl seiner Patentanmeldungen in Kürze vervielfachen.  Vorstände die dies nicht tun, werden von ihren Aktionären zur Rechenschaft gezogen.  Die Höhe der kassierten Lizenzgebühren erscheint allerdings bisher nicht in den Unternehmensbilanzen.  Auch HP hielt es für opportun, darüber zu schweigen."))
) )

(mlhtdoc 'swpatflalu  
 (ML LtL "Gesetzestexte, Urteile und sonstige Verlautbarungen der Rechtspflegeorgane" (en "Law and Statements by Legislators")) 
 nil nil
 (dl
  (l (ahv 'eupat)
    (linul (ahv 'epc97 (ML EWC "Europäische Patentkonvention" (en "European Patent Convention")))
      (ah (ml "http://www.epo.co.at/epc97/%(ml:deutsch:english)/ar52.html") (ML Aie "Article 52" (de "Artikel 52")) ) )
  (let (url) (colons 
   (ah (subdirs (setq url (subdirs "http://www.european-patent-office.org/guidelines" (mlc "english" (de "deutsch") (fr "francais")))) "c_iv.htm") (ML Gmr "Richtlinien für die Prüfung im europäischen Patentamt" (en "Guidelines for examination in the Eurpean Patent Office") (fr "Régles pour l'examination dans l'Office Européen de Brevets")))
   (ah (subdirs url "c_iv.htm") (colons (ML Pjt "Part C, Chapter IV" (de "Teil C, Kapitel IV") (fr "IIème Partie, Chapitre IV")) (ML Pni "PATENTIERBARKEIT" (en "Patentability") (fr "Brevetabilité")))) ) ) )
  (l (ahv-lang 'eu-gp-pat) (ML AuU "%(q:Grünbuch), in dem die EU-Kommission (Generaldirektion 15) und das Europäische Patentamt europaweit einheitlichen %(q:Patentschutz für Computerprogramme) fordert." "%(q:Greenpaper) in which the EU Commission demands europe-wide uniform %(q:patent protection for computer programs)."))
  (l (ahv-lang 'indprop-8682) (ML Dei "Darin wird gefordert, auch nicht-technische Computerprogramme müssten patentierbar sein, und bereits Kopieren eines Programmes müsse einen Verletzungstatbestand darstellen, damit %(q:die Rechte in der Gemeinschaft überall effektiv durchgesetzt werden können).  Die EU-Kommission wird mit der %(q:Ausarbeitung eines Richtlinienvorschlags auf der Grundlage von Artikel 100 EG-Vertrag) beauftragt." "advocates broad patentability of computer programs and community-wide effective enforcement against copying of programs."))
  (l (ahv-lang 'indprop-99) (let ((MM "Mario Monti")) (ML Mto "Mario Monti kündigt die Ausarbeitung des Rili-Vorschlags %(q:vor Sommer 2000) an und behauptet, nach %(q:breiter Konsultation mit den betroffenen Kreisen) sei festgestellt worden, dass Software patentierbar werden müsse.  In Wirklichkeit wurden nur 44 Patentjuristen konsultiert." "%{MM} claims that after %(q:broad consulations) software patentability was deemed necessary.  In reality, only 44 IP experts were heard.")))
  (l (ahv 'eu-dg3a) (ML Dio "Darin nehmen informationelle Güter eine prominente Stellung ein." "With partial focus on informal goods."))
  (l (ahv 'de-patg))
  (l (ahv 'de-prl) (ML Snu "Schreibt recht strenge Prüfkriterien vor, u.a. dass eine Erfindung Naturgesetze auf Materie anwenden muss."))
  (l (ahv 'transpatent) (ML Ume "Umfassende und wohlgeordnete Sammlung deutschsprachige Gesetzestexte zum gewerblichen Rechtsschutz"))
  (l (ahv 'brevets-francais) (ML VfW "Vollständige Sammlung französischer Patente, aktiv seit 1999-10-04"))
  (l (ahv 'dpma-verf) )
  (l (ahv 'trips) (blockquote (tpe (mltr "patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application.") (mltr "For the purposes of this Article, the terms %(q:inventive step) and %(q:capable of industrial application) may be deemed by a Member to be synonymous with the terms %(q:non-obvious) and %(q:useful) respectively"))) (ML DfW "Die Welthandelsorgansation empfiehlt hiermit den europäischen Ansatz, Patente auf den Industriebereich zu beschränken, erlaubt es aber auch den Amerikanern, ihr System beizubehalten, indem sie den Begriff %(q:industriell) sehr weit auslegen.") )
  (l (ahv 'wipo-clea) 
     (nl 
      (ML Wnk "WARNUNG: technisch überzüchtet, man findet vor lauter Java, Javascript, ActiveX, Shockwave und sonstigem Firlefanz kaum etwas" "bloated webpages where you will have a hard time finding anything.")
      (tan (ah "http://clea.wipo.int/lpbin/lpext.dll?f=file[ibrowse-h.htm]" (ML fWd "Hier findet sich im linken Frame eine %(q:Collapsible List). Man klickt nicht den Text %(q:Collection of Laws) an, sondern die %(q:Rosette) links davon. Damit expandiert man die Liste.")) (pf dank "PA Axel Horns")) ) )
  (l (ahv 'loi-internet) (ML Gkc "Gesetzesinitiative französischer Parlamentarier, deren Artikel 4 vorsieht, dass Träger öffentlicher Funktionen sich nicht mittels patentierter Technik an die Bürger wenden dürfen."))
  (l (ahv 'osslaw-articles)  (ML Rfi "Recht auf Kompatibilität"))
  (l (ahv 'rome-treaty)  (ML WWe "Widersprüche zur Softwarepatentierung"))
  (l (ahv-lang 'eu-interoper-1991) (ML WWe2 "Widersprüche zur Softwarepatentierung"))
  (l
   (ahv 'iitf-eleccomm-ip) 
   (lin 
    (let ((IITF (ahv 'iitf))) (ML DmW "Die US-Regierung meint, E-Geschäftsverkehr erfordere Patente und will dieser Sichtweise im Rahmen von %{IITF} weltweite Geltung verschaffen:" "In its %{IITF} project, the US government says E-Commerce needs patents and asks other countries to provide them:"))
    (blockquote
     (al 
      "To create a reliable environment for electronic commerce, patent agreements should: ... require member countries to provide adequate and effective protection for patentable subject matter important to the development and success of the GII; and establish international standards for determining the validity of a patent claim."
      "The United States will pursue these objectives internationally. Officials of the European, Japanese, and United States Patent Offices meet, for example, each year to foster cooperation on patent-related issues. The United States will recommend at the next meeting that a special committee be established within the next year to make recommendations on GII-related patent issues ."
      "In a separate venue, one hundred countries and international intergovernmental organizations participate as members of WIPO's permanent committee on industrial property information (PCIPI). The United States will attempt to establish a working group of this organization to address GII-related patent issues."))
    (filters ((gb ahv-lang 'indprop-8682)) (ML DWi "Die EU kam offenbar dieser Forderung nach, als sie in ihr Grünbuch über den Elektronischen Geschäftsverkehr ähnliches schrieb." "The EU Green Paper on Electronic Commerce follows suit by echoing Gore's unproven statement.")) ) )
  (l (ahv 'ompi-p183 (ML WWh "WIPO/OMPI plant neue Schutzrechte für %(h:Internet:Autoren).")) (ML FrW "Französische Presseerklärung der WIPO/OMPI"))
  ) )

(mlhtdoc 'swpatcnino (ML PLi "Nachrichtenmeldungen über Patent-Streitfälle" (en "Patent Ligitation Cases"))
 nil nil
 (ul
 (colons
  "2000-06-07"
  (tpe (ahv 'cnet-banpat (ML UWe "US-Patent auf Bannerwerbung" "US Patent on Banner Advertising")) (ML Sri "Slashdot berichtet, CNet habe ein Patent auf Bannerwerbung im Netzwerk erhalten.  Wer in den USA Benutzerinformationen sammelt und in Abhängigkeit davon Werbebanner schaltet, muss braucht dazu die Erlaubnis von CNet." "According to Slashdot, CNet has obtained a patent on banner advertising across multiple computers.  Even Amazon will have to obtain a licence from CNet before being allowed to use its %(q:Doubleclick Technology).")))
 (lindl 
  "2000-06"
  (l (ahv 'bolkestein26) (let ((FB "Frits Bolkestein")) (filters ((dg pet (ahv 'eu-dg15 "DG 15"))) (ML Dta "Der für die %(dg:Binnenmarkt-Generaldirektion) und damit für die Patentpläne zuständige EU-Kommissar %{FB} plädiert vor einem Publikum von Patentrechtlern für eine zügige Auweitung des europäischen Patentwesens." "The EU-Commmissioner for the %(dg:Internal Market), %{FB}, promises rapid expansion of the patent system before an audience of patent professionals."))))
  (l (ahv 'dilbert-noclickpat) (ML Sra "Selbst wer in der Zeitung nur die Karikaturen liest, bekommt heutzutage schon mit, dass mit dem Patentwesen etwas nicht stimmt." "Nowadays even those who read only the caricature parts of newspapers will notice that something is wrong with the patent system."))
  (l (ahv 'orf-lessig) 
   (lindl
    (ML Dir "Der Berater der US-Regierung im Microsoft Prozess warnt im Gespräch mit dem Österreichischen Rundfunk vor einer rigiden Ideologie des %(q:Geistigen Eigentums), die das Internet einer tyrannischen Herrschaft unterwerfen werde, wenn man jetzt nichts unternehme.")
    (l (ahv 'orf-lessig-feind) (let ((ESR "Eric Raymond")) (ML Lzs "Lessig kritisiert einen in der OpenSource-Szene weit verbreiteten blauäugigen Liberalismus und zitiert %{ESR} als dessen Vertreter." "Regulation is inevitable.  Lessig criticises %{ESR}s %(q:naive) liberalism and calls on the OpenSource community to use politics for the sake of freedom and openness, rather than leave the power of regulation to Big Business, which is already exercising it in a dangerous way.")))
    (l (ahv 'orf-lessig-swstal) (ML Uts "Unter %(q:Software-Stalinismus) ist eine rigide Ideologie des Geistigen Eigentums zu verstehen, die ihren eigenen Propaganda-Neusprech pflegt und zur Durchsetzung ihres Weltbildes weder vor menschlichen Härten noch vor der Schädigung der Volkswirtschaft zurückschreckt." "By %(q:Software Stalinism) Lessig refers to an ideology of intellectual property which cultivates its own propagandist Newspeak and doesn't refrain from harshness and economic devastation for perpetrating its dogmas.")
    ) ) ) )
 (linul "1999-08-04"
   (tpe (ahv 'freetype-patents) (ML Eww "Entwickler hochwertiger freier Software warnen: über ihrer Arbeit schwebt ein juristisches Damoklesschwert" "OpenSource software developpers feel threatened"))
   (tpe (ahv 'truetype-patents) (ML 3yW "3 erst 1999 entdeckte US-Patente der Firma Apple aus dem Jahre 1990 gefährdet das frei verfügbare TrueType-Erzeugungssystem FreeType, das mittlerweile in diversen Applikationen eingesetzt wird.  Die patentierte Technik ist an sich nicht besonders wertvoll.  Aber man muss sie verwenden, um zu den Systemen von Microsoft und Apple kompatibel sein zu können." "Three patents from 1990 that were discovered only in 1999 are threating tha ability of opensource platforms to achieve compatibility with the MS/Apple mainstream.")) )
 (colons (ahv 'xge21) (ML Vee "Vicom, Sohei und andere Faelle"))
 (colons (ahv 'sd-991217-ask) (ML Dih "Die Professoren sind in der Akademischen Gemeinde offenbar erst durch ihre Patent-Zockerei aufgefallen."))
 (colons (ahv 'sd-y2k-pat) (ML Uea "US-Patent für Überbrückung des Jahres 2000: Zahlen unter 30 wird eine 20 vorangestellt, Zahlen ab 30 eine 19.  Genannt %(q:windowing technology).  Nachdem die Patentinhaber kräftig abkassiert haben, sind inzwischen doch noch neuheitsschädigende Schriftsstücke gefunden worden."))
 (colons (ahv 'borchers-199908) (ML vn9 "Detlef Borchers verweist auf swpat.ffii.org als Informationsquelle für die aktuelle Entwicklung.  Meint, die mittelständische europäische Softwareindustrie wünsche SWPAT. Enthält eine Meinungsumfrage, bei der bisher (1999-12) 60% der Befragten äußern %(q:Softwarepatente sind wichtig für den Schutz des geistigen Eigentums).") )
 (colons (ahv 'wc991103) (tan (ML Nai "Netter Artikel über den Gif-Streit mit ein paar weiteren absurden Beispielen von US-Patenten.  Herrschaft über Information als die große politische Auseinandersetzung der Zukunft.") (pf dank (setq kleinert (famvok "Kleinert" "Andreas")))))
 (tan (ahv 'oss-vs-patents) (pf dank kleinert))
 (ahv 'wired-19473)
 (colons (ahv 'uspat-5443036 (ML Krs "US-Patent auf eine Methode zur sportlichen Ertüchtigung von Katzen")) (ML Atu "Arnim: ok, das ist zwar kein software-patent aber doch ein beweis dafuer das im us-patent-office derzeit alles durchgeht"))
 (tan (ahv 'wired-21726) (pf dank (setq eych (famvok "Eyrich" "Christoph"))))
 (colons (ahv 'sd-990923) (filters ((os ah "http://www.base.com/software-patents/statements/oracle.html")) (ML bWl "berichtet über %(os:die Haltung der Firma Oracle) zur %(h:SWPAT:Frage).")))
 (colons (ahv 'noipix "http://www.virtualproperties.com/noipix/noipix.html") (tan (filters ((pt ahv 'noipix-patents)) (ML WoW "Wie eine Firma mit %(pt:Patenten) und Platformstrategien der freien Entwicklergemeinde im Bereich Bildverarbeitung den Garaus macht")) (pf dank (ah "http://www.klammeraffe.org/~brandy" (famvok "Brandstetter" "Matthias")))))
 (linul 
  (colons (ahv 'intern-990802) (ML Mat "M$ bekommt US-Patent für WWW-Strukturelemente"))
  (colons (ahv 'w3-p3p) (MLL D3r "Das WWW-Normierungsgremium W3C beklagt Behinderung seiner Arbeit durch Softwarepatente"))
  (linul
   (colons (ahv 'heise-2650) (let ((W3C (ah "http://www.w3c.org" "WWW-Konsortium"))) (MLL Haa "Microsoft hat allerlei Trivialitäten patentiert und dadurch beim %{W3C} gemeinsam erarbeitete Internetnormen in seine Abhängigkeit gebracht.")))
   (ahv 'uspat-5860073 (ML Vsk "Volltext des Microsoft-WWW-Patentes"))
   (ahv 'uspat-5860073-clms  (ML Dts "Die Patentanspüche")) )
 (colons (ahv 'lwn-ibm-patents) (MLL Mac "Manche der von IBM quelloffen zur Verfügung gestellten Programme haben Haken: man übersieht leicht die Patente, die normalerweise nicht beansprucht werden, aber dennoch irgendwann zuschlagen können."))
 (ahv 'pcweek-infospinner-dynaweb) )
 (linul 
  (colons (ahv 'lzw-license) (ML Nkn "Nach jahrelangem Schweigen kommen die Patentinhaber des GIF-Bildkompressionsalgorithmusses aus der Versenkung hervor und verlangen von WWW-Betreibern Lizenzgeühren von 5000 USD."))
  (colons (ahv 'lpf-gif) (ML EeG "Erklärung der LPF zum GIF-Problem"))
  (colons (ahv 'boutell-gd) (ML Dni "Der Programmierer Thomas Boutell erklärt, dass er auf Verlangen der Patentinhaber die GIF-Unterstützung aus seinem frei im Quelltext verfügbaren Grafikprogramm entfernen musste.") )
  (colons (ahv 'burnallgifs) (ML AGa "Anti-GIF-Kampagne"))
  (ahv 'heise-gif-990830)
  (ahv 'heise-gif-991101)
  (colons (ahv 'esr-gif2png) (ML AvW "Artikel von E.S. Raymond")) )
 (colons (ahv 'uspat-5872844) (ML MMW "Microsoft-Patent über eine Methode zur Sicherung gegen Betrug bei Egeld-Zahlungen"))
 (nl (ahv 'cw-befree)
     (ahv 'uspat-5848396)
     (ahv 'uspat-5848396-clms)
     (linol (ML Pzs "Patentschutz wird beansprucht für") (ML dWs "die automatische Erstellung von Benutzerprofilen, die unter anderem die von einem Surfer besuchten Seiten sowie die Art der dort gesuchten Informationen umfassen") (ML dto "das Senden solcher Daten an Internet-Nutzer, wenn diese Bannerwerbung oder Verweisen und Logos betrachten") (ML del "die gezielte Auswahl von Werbung auf Basis der erstellten Benutzerprofile, sowie") (ML dod "die Fähigkeit, die Benutzerprofile aufgrund des Antwortverhaltens bei bestimmter Werbung zu erweitern und darauf wiederum mit geänderter Werbung zu reagieren.")) ) ) )

(mlhtdoc 'swpatbende 
 (ML Ont "Organisationen" (en "Organisations"))
 nil nil 
 (dl
  (l (ahv 'undp-hdro) (quot (mltr "The relentless march of intellectual property rights must be stopped and questioned")))
  (l (ahv 'ibm-patent-server) (ML AW0 "Hier sind alle US-Patente der letzten >20 Jahre zu finden.  Ein nützlicher Dienst, der nebenbei IBM immer mit Informationen darüber versorgt, wer derzeit was sucht."))
  (l (ahv 'swpatinst) (commas (ML Uea2 "Umfassende Datenbank") (ML gtM "gefördert von IBM, MS u.a.") (ML git "gemeinnützig") (ML sre "Soll Überblick über Softwarepatente erleichtern helfen.  Wird von Patentlobbyisten immer angeführt, um zu beschwichtigen, man werde die möglicherweise unlösbaren Probleme der Prioritätsrecherche im Softwarebereich %(q:demnächst) in den Griff bekommen.  Dieses %(q:demnächst) rückt in immer weitere Ferne." "intended to solve the problems of priority research.  As the years pass by, such a solution seems more distant than ever.")))
  (l (ahv 'umich-swpat) (ML ned "Überblick über diverse Ressourcen zum Für und Wider von Softwarepatenten"))
  (l (ahv 'wipo (ML Wos "Weltorganisation für das Geistige Eigentum")) (ML dsn "drängt auf weltweit einheitliche Durchsetzung von Verwertungsvorrechten.") )
) ) 

(mlhtdoc 'swpatdrata
  (ML Aee "Am Rande Interessantes" (en "other interesting stuff")) 
 nil nil 
  (dl
   (l (ahv 'gp_de) 
    (let ((CT "Christoph Then")) (ML GWn "GP-Patentexperte %{CT} dokumentiert umfassend die Patentinflation im Bereich der Biologie.  Die Situation ist ähnlich wie in der Informatik.  Es geht dabei aber weniger die Frage, ob das Patentwesen zur Produktivität beiträgt, als darum, welche Wertschöpfungsmaschinerien es erzeugt, und ob wir unsere Landwirtschaft dem ökologisch blinden Wüten dieser Maschinerien unterwerfen wollen." "GP Patent Expert %{CT} thoroughly documents the frightening monopoly machinery that has been generated by patent inflation in the area of biology and accuses the EPO and the German Ministery of Justice of serving the self-seeking interests of this machinery, even at the cost of breaking European patent law."))
    (ul
     (ahv 'gp_biopat)
     (ahv 'gp_epa_rechtsbruch)
     (ahv 'gp_epa_fakten)
     (ahv 'gp_monsantrix)
     (colons (ahv 'greenpstud) (ML Wbu "Wirft dem Europäischen Patentamt Rechtsbruch in Sachen Leben vor und hat Stellungnahme des BMJu gegen das EPA erwirkt.") )
     (ahv 'gp_genraps) ) )
 (l (ahv 'nap-property) (ML Vto "Vorteile von Patenten in der Molekularbiologie"))

 (l (ahv 'rms-antitrust) (ML Rej "RMS zeigt auf, warum Kartellprozesse nicht den Kern des Monopolproblems treffen.  Dies sollten vor allem jene Patentinflationisten lesen, die ihre Hoffnungen in das Kartellrecht setzen, wenn es darum geht, die wettbewerbsfeindliche Wirkung von Logikalienpatenten auszugleichen."))
 (l (ahv 'why-free) (ML Rma "RMS erklärt, warum nur freie Software vollwertig ist."))
 (l (ahv 'mkoek-proj) (ML eel "eine diplomarbeit ueber open source licensen, hat aber eine gute erklaerung von %(q:geistigem eigentum)"))
 (l (ahv 'ucitareport) (tan (ML Nku "Neue amerikanische Pläne, Softwarekopierschutz besser durchsetzbar zu machen, lösen Proteststürme aus.") (ML Eih "Es geht hier nicht um Patentierbarkeit und die Sache scheint auf den ersten Blick weitaus weniger gefährlich.  Wir wollen keinesfalls Softwarepiraten in Schutz nehmen oder das Urheberrecht madig machen.  Das Urheberrecht ist wichtig und sollte so weit geschützt werden, wie dies mit vertretbarem Aufwand möglich ist.") ) )
 (l (ahv 'markengrabbing) (tan (ML Keh "Kampagne gegen Markenrechtmissbrauch") (ML IlW "Insgesamt bringt das Markenrecht für die Entwicklung freier Software mehr Nutzen als Schaden.  Dennoch gibt es auch in diesem relativ übersichtlichen Bereich Auswüchse, die zeigen, wie missbrauchsanfällig der gesamte proprietär-informationelle Komplex (geistiges Eigentum) ist.") ) )
 (l (ahv 'savetheweb) (ML Kis "Kampagne gegen EU-Pläne zu internet-unverträglichen Ausweitungen des Urheberrechts"))
 (l (ahv 'crd19990903) (ML Sma "Softwarepatente fördern die Geheimhaltung von Quelltext und damit auch das Einbauen von Hintertüren."))
 (l (ahv-lang 'ohim) (ML Ien "In DE, US u.a. sind die Markenämter erst kürzlich in die Patentämter eingegliedert worden.  Auf Europäischer Ebene sind beide Institutionen getrennt.  Das Markenamt ist in Alicante, das Patentamt in München." "Patents and Trademarks are in separate offices at the European level."))
 (l (ahv 'uni-sb-urecht) (ML IiW "In diesem vorbildlichen Archiv findet man alles zum Urehberrecht aber nichts zu Softwarepatenten." "German (+ multilingual) copyright law archive."))
) )

(mlhtdoc 'swpatliste
 (ML Unc "Ungeordnete Archivtexte" "Non-classified Archive Texts" "Textes archivées non-classifiés")
 nil nil
 (apply 'ul (mapcar 'ahvtpe swpat-netadrs))
)
; swpatminra-dir
)

(list 'swpdoku-dir
(mlhtdoc 'swpdoku nil nil nil
(bildtext
 'swpatcd
 "SWPAT-CD"
 (let ((IN (ah "http://www.intevation.de" "Intevation GmbH"))) (filters ((cd ah "ftp://ftp.ffii.org/pub/swpat/cd/") (bs ah "http://intevation.de/swpat/") (bm ahs 'swpbmwi25)) (ML DWl "Die %{IN} hat anlässlich der %(bm:BMWi-Anhörung) im Mai 2000 in unserem Auftrag eine %(cd:frei verfügbare Einführungs-CD) erstellt.  Sie können %(bs:bei Intevation ein Exemplar bestellen)." "%{IN} has in May 2000, for the occasion of the %(bm:hearing at the German Ministery of Economics), put together a %(c:introductory CD) (German only) for FFII.  You can %(bs:order a copy from Intevation)." "%{IN} a mis au point une %(cd:cédérom d'introduction librement disponible) a l'occasion de la %(bm:conférence au ministère d'économie) en mai 2000.  Vous pouvez %(bs:ordonner une copie chez Intévation) (allemand seulement)."))) )

(filters ((pe ahs 'swpdokupe) (pi ahs 'swpatpikta) (pu ahs 'swpatpurci) (mi ahs 'swpatminra)) (ML Zfn "Zudem erstellen wir vierteljährlich einen kompletten %(pe:Abzug unsere Swpat-Archivs) samt %(pi:Patentschriften), %(pu:Prioritätsbelegen) und %(mi:Netzspiegel).  Diese Vierteljahres-Ausgabe wird auf geeigneten Datenträgern (CD, DVD) an verschiedene Bibliotheken und interessierte Fachleute verschickt wird." "Moreover, we press a quarterly disk edition (CD, DVD) of our Swpat archive, including patent descriptions, priority proofs and net mirror, which is sent out to various libraries and interested specialists." "Enoutre, nous créons chaque quatrimestre une %(pe:version disque complète de nôtre archive Swpat), y inclus les %(pi:déscriptions de brevet), les %(pu:preuves de priorité) et le %(mi:mirroir de réseaux).  Cette Édition Quatrimestrielle est envoyée sur des média adaptés (CD, DVD, MO) a divers bibliothèques et des spécialistes intéréssés."))

)
; swpdoku-dir
)

(list 'swpatpapri-dir
(mlhtdoc 'swpatpapri nil nil nil
(sects 
(jurn (ML Zsi "Periodicals" (de "Zeitschriften und Schriftenreihen") (fr "Journaux et Séries"))
(framebox (apply 'tdl (mapcar (lambda (lst) (l (an (car lst)) (cadr lst))) (l
 (l "BPatG" (ML Vep "Verlautbarungen des Bundespatentgerichtes" (en "Publications of the German Federal Patent Court") (fr "Publications de la Cour Fédérale de Brevet Allemande")))
 (l "BGH" (ML Vge "Verlautbarungen des Bundesgerichtshofs" (en "Publications of the German Federal Court") (fr "Publications de la Court Fédérale Allemande")))
 (l "EPO" (ML Ola "Amtsblatt des Europäischen Patentamtes" "Official Journal of the European Patent Office" "Journal Officiel de l'Office Européen des Brevets"))
 (l "MDP" (ml "Mitteilungen der Deutschen Patentanwälte"))
 (l "CR" (ah "http://www.computerundrecht.de" "Computerrecht"))
 (l "GRUR" (ah "http://www.grur.de" "Gewerblicher Rechtsschutz und Urheberrecht"))
 (l "GRUR-Int" "GRUR International")
 (l "NJW" "Neue Juristische Wochenschrift")
 (l "IIC" (commas "International Intellectual Property and Copyright (?)" "MPI" "wiley-vch Verlag GmbH" "DE-69469 Weinheim") )
 (l "EIPR" "E? Intellectual Property Review")
 (l "AIPJ" "Australian Intellectual Property Journal")
 (l "HBR" "Harvard Business Review")
 (l "IPQ" "Intellectual Property Quarterly")
 (l "JJ" "Jurimetric Journal")
 (l "AIPLA" "American Intellectual Property Law Association Quarterly Journal")
 (l "ACM" "Communications of the Association of Computer Manufacturers")
 (l "SMI" (commas (ml "Schriften zum Medien- und Immaterialgüterrecht") (ml "begründet und herausgegeben von Professor Dr. Manfred Rehbinder (Zürich)")))
 (l "WuW" (commas (ml "Wirtschaftsrecht und Wirtschaftspolitik") (ml "in Verbindung mit Kurt Biedenkopf und Erich Hoppmann") (ml "herausgegeben von Ernst-Joachim Mestmäcker")))
 (l "CLSR" "The computer law and security report")
 (l "UPLR" "University of Pittsburgh Law Review")
 (l "IST" (tpe "Information and Software Technology" "Elsevier Publications"))
)))) )

(text (ML Tex "Texte" "Texts" "Textes")
(docmenu-large)
) 
)
)


(let ((verk '(nil "2000-12-21" "0.9"))) 
(mlhtdoc 'clsr-rhart97 nil nil nil
(papritab)
(filters ((la ahs 'softprot-indpatent) (dg ahs 'eu-dg15) (ip ahs 'eu-dg15-ipi-study-pdf)) (ML AWd "Robert Hart has been a %(la:long-term advocate) in favor of software patents in Europe and only recently wrote an %(q:independent) %(ip:study on software patents) for the European Commission's %(dg:Directorate for the Internal Market) (DGIM).")) 

(lin
 (filters ((gp ahs 'eu-gp-pat) (fd ahs 'indprop-8682)) (ML TnW "The Hart paper of 1997 was published in a context where the European Commission was formulating its policy on software patents as expressed in the %(gp:Greenpaper) and its %(fd:follow-up papers).  Like most patent lawyers, Hart's basic argument is that computer programs are equivalent to some currently patentable inventions and should therefore also be patentable:"))
 (blockquote
  (tpe
   (al
    (ml "Patent systems have been created to stimulate the evolution of existing and the creation of new industry by protecting the industrial application of innovations. The computer software industry needs that stimulation as much as any other industry.") 
    (ml "...To deny patent protection for such inventions would I believe remove from the patent system a significant industry thereby severely disadvantaging the innovators in that industry encouraging second comers rather than original creators upon whom any industry relies heavily for its evolution.")) "p. 252")))

(ML Ija "%(nl:In the paper, the argumentation goes:1 Introduction:boundaries of copyright:2a the need for protection beyond copyright:2b what inventions could be patented:2c position of the EPO:2d history:2e policy:2f trade secrets:3 conclusion.:From this design  it is obvious section 2b is the actual main argument whether patentability is desirable, so let us check it for scholarly consistency.)")  
(ML Ter "The main arguments are given as quotes from (I) a manifesto for sui generis protection and (II) an OTA report.")

(sects
(suigen
(ML Roa "Reinterpreting a manifesto for a sui generis protection as justification for patents")

(filters ((sm ahs 'sadaka-manifesto)) (ML HWL "Hart summarizes the Columbia Law Review paper %(sm:A Manifesto Concerning the Legal Protection of Computer Programs) as"))

(blockquote
  (ML EiW "Effectively the Manifesto considers that there should be (i) copyright protection for the literal expression of the program code, (ii) patent protection for those functional features of the computer program and their application that meet the level of protection required by the patent system and (iii) an anti-cloning system to protect the program behavior not protected by the patent system.")

  (ML Hsy "However, in the manifesto document, %(q:patents) does not occur in the TOC, nor in the conclusion, the only mention I have (passim) so far found is %(q:the issue of a large number of questionable patents for software related ideas may impede competitive development and follow-on innovation in the software industry) (p. 2422). The emphasis seems to call for a sui generis protection (like the semiconductor act, cf pp. 2413-2420). Again, from the manifesto's conclusion:")

  (blockquote (tpe (ml "We propose to remedy market-destructive appropriatins of program behavior and the industrial designs aimed at producing efficient program behavior through a period of automatic anti-cloning protection of these innovations. The period should be long enough enough to give efficient incentives to invest in the development of innovative software, yet short enough to avert the market failure that would result if second competitors and follow-on inventors were blocked from entering the market long after the first firm had recouped its initial investment.") "p. 2430"))

  (ML ItW "It is clear one does not even have to join the call for sui generis semiconducor-like protection desired by that manifesto (so may be of limited usefulness for those opposed to software protection beyond copyright), but to state they advocated patents for programs seems abusive.") ) )

(ota (ML TsW "Taking what is useful from the OTA report")

(filters ((ot ah "http://www.wws.princeton.edu/~ota/disk1/1992/9215.html")) (ML TfW "The conclusions of the %(ot:OTA report) (Section 1, pp. 32ff) are neutral:"))

(blockquote
(ml "Option 2.1: Refine the statutory definition of patentable subject matter to provide guidance for the courts and PTO. Legislation might address the extent to which processes implemented in software or mathematical algorithms are or are not statutory subject matter. Legislation might also address the issue of special exemptions, such as for research and education).")

(ml "Option 2.2: Exclude software-related inventions and/or algorithms from the patent law and create a special, sui generis protection within a patent framework for some inventions. The latter might have a shorter term, lower criteria for inventiveness, and/or special exemptions from infringement.")
)

(ML Hua "However, the section that Hart quotes at length (on p. 132 of the report in its Section 4) in a way suggesting this also would be the OTA's direct recommendation, is just a summary of the USPTO arguments and of no recommendative meaning, in the same manner one could quote nearly the entire p. 136 of said Section 4 which summarizes objections to software patentability.")

)

(klud (ML Ccs "Conclusion")

(ML TlW "To defend Hart, the OTA report is presented in a more balanced way in his section 2f (p. 251). But the fact that both pillars of the core %(q:why we need patents) section are hollow leaves a somewhat bitter taste in the argumentation's overtones %(q:the US know what they are doing).")
(ML Hll "Hart also does not tell us that at the 1994 USPTO hearings, only a fraction of software companies (among them Microsoft) argued clearly in favor of software patents, whereas a larger fraction considered software patents harmful to business interests (among them Oracle, Adobe). Of course, there also was a broad field of undecided statements and statements from law firms in favor of software patents.")
(ML Tmt "Even in 2000 the USPTO conducted a similar hearing.  Yet, however critical the opinions raised in these hearings, it was always unlikely that they would have any impact, since the decisions in the US were not taken by Congress, and not even by the USPTO, but by a few courts, namely the CAFC.   Proponents of software patents in Europe have repeatedly asserted that the US and Japan have decided in favor of software patentability after some careful and conscious public deliberation process.  The facts don't support that assertion.") 
)

(ref (ML Qde "Further Reading")

(ul
(ml "Robert Hart 1997, The Case for Patent Protection for Computer Program-Related Inventions, in: The computer law and security report, 13(4), 247-252.")

(ahs 'ota-report (ml "OTA 1992, OTA-TCT-527, Finding a Balance: Computer Software, Intellectual Property and the Challenge of Technological Change"))

(ahs 'sadaka-manifesto (ml "Pamela Samuelson et al. 1994, A Manifesto Concerning the Legal Protection of Computer, Columbia Law Review, 94(8), 2308-2431."))

(ahs 'uspto-testimony (ml "United States Patent and Trademark Office, Public Hearing on Use of the Patent System to Protect Software-Related Inventions, Transcript of Proceedings, 1994")) 

(tpe (ahs 'digidilem00) (ML T0l "This report by the US National Research Council from July 2000 criticizes that the decision in favor of software patentability was taken by lawcourts without any backing from Congress.")) 

(tpe (ahs 'isoc-fr-ce-brvt) (ML Ttt "This consultation paper from the French Section of the Internet Society criticizes the IPI study and specially mentions bad argumentation by Robert Hart within this study."))

) )

(auth 
 (ML Awe "Acknowledgements")
 (ML Ban "The body of this review was researched and written by Holger Blasum, thanks to hints from Thomas Ebinger.  Editorial responsibility is taken by Hartmut Pilch"))

) ))

(mlhtdoc 'skk-patpruef nil nil nil
(papritab nil (filters ((or ahswpr 'skk-patpruef)) (l (tok papriref) (let ((GRUR (ahsjr 'GRUR))) (ML TOo "The author placed his %(or:original German text) under an OpenContent license and submitted it to %{GRUR} for publication.  The English translation is so far only a rough draft.")))))

(sects
(aprog (ML Ara "A Programme as Such")

(ML Iad "In one of its recent decisions the 17th senate of the German %(tan:Federal Patent Court:Bundespatentgericht, BPatG) had to decide whether the following items qualify for patenting:")

(blockquote
(ML Des "Digital storage medium, in particular diskette, with electronically selectable control signals, that can cooperate with a programmable computer system in such a way that a procedure is operated according to one of the claims 1 to 17.")

(ML Chh "Computer programme product with program code for the execution of the procedure according to one of the claims 1 to 17 stored on a machine-readable carrier, if the programme product runs on a computer.")

(ML Ccc "Computer programme with program code for the execution of the procedure according to one of the claims 1 to 17 if the programme runs on a computer")
)

(ML Ihr "In the patent claim 1, to which the patent claims mentioned refer, a claim has been made for")

(blockquote
(ML PoW "Procedures for the computer-assisted search and/or correction of a defective character string Fi in a digitally stored text, which contains the appropriate error free character string Si")

(ML csW "characterised by the fact that")

(ol
(ML tfa "the occurrence frequency H (Si) of the error free character string Si is determined")

(ML tdW "the error free character string Si is changed according to a rule Rj, so that a possible defective character string fij is produced,")

(ML tif "the occurrence frequency H (ij) of the character string fij in the text is determined,")

(ML tef "the occurrence frequencies H (ij) and H (Si) are compared and")

(ML bwj "based on the comparison in step d), it is decided whether the possible defective character string fij is the looked for defective character string Fi.") ) )

(ML aPa "and has been considered as being worthy of patent in accordance with §§ 1-5 PatG (Patentgesetz, German Patent Law) by the patent examiner. This at least tacitly implies according to present comprehension that the subject of registration has a technical character. The 17th senate has no objections against this.")

(ML Tnd "The claimed subjects are to solve the task to create %(q:an improved procedure and computer system for the search and/or correction of a defective character string in a text).")

(ML Tai "The question, where objectively the invention is to be seen, whether in the procedure of the patent claim 1, the programme of the patent claim 24, or in an algorithm on which the patent claim 1 is based, has unfortunately never been posed. Since this question was not at all raised, the BGH (Bundesgerichtshof, German Federal Court) will probably not take explicitly a position regarding this issue. This could be, however, a crucial point for the comprehension of the problem of patenting programmes for data-processing systems in general.")

(sects
(obj (ML Tvo "The Objective Subject of Invention")

(ML Tpi "The 17th senate explained in its decision under 1.3.2:")

(blockquote )

(ML IWr "In the very next paragraph it says:")

(blockquote (ML AuW "A processing of the procedure can only be successfully undertaken with a computer system, which is able to quasi completely interpret the individual parts of the recording and thus to cause an execution of the desired process steps."))

(ML Tet "The %(q:desired process steps) are very clearly not regarded here themselves as solution of the task. Below under 1.4.3 it says:")

(blockquote (ML AhW "A potential for the production of a technical effect... is attributed... only to the computer system with the programme loaded from the storage medium into the main memory, which is able actually to implement, if necessary, a technical procedure."))

(ML Osi "On the basis of these remarks, however, only one conclusion can be drawn: not (the work) procedure represents the solution of the task and thus the objective registration subject, but only the computer system with the programme loaded from the storage medium into the main memory, whose recording must be interpreted and implemented quasi completely. The working procedure is only the effect of the execution of the program code by the computer system that is not in detail characterised in the patent claims 1, 22, 23 and 24. In view of the fact that in practically all cases of programme related registrations these procedures are claimed as effects and are also frequently patented, this judicial insight is of special importance!")
)

(ivsol (ML IsW "Inventions Must be Solutions")

(ML Nlr "Now §1 PatG reads literally: %(q:Patents are awarded for inventions ...).")



(ML Wel "With which wording such a computer system is claimed at present is unfortunately not yet published. From the disclosure writing, however, such a computer system can be deduced. Beside the memory means 1, 3, 4, 10, 12, 14 and 17 apparently the means 5, 6, 7, 8, 9, 11 and 13 are substantial for the execution of the procedure. This is described as follows:")

(blockquote (ML Ica "In the processor means 2 the contained means 3, 5, 6, 7, 8, 11 and 13 as well as the bus 16 do not have to be implemented as discrete electronic construction units, but can be produced by an appropriate programming of the processor 2. An appropriate programme suitable for the implementation of the invention related procedure will cooperate in such a way with the operating programme of the computer system in an actually well-known way, that the computer system takes on the configuration shown in Fig. 4."))

(ML Tpk "The claimed working procedure is to be realised therefore with priority by a suitable programme, which is processed by a processor 2 and accesses over the bus 16 its memory 1, 3, 4, 10, 12, 14 and 17. Computer systems with a processor, which accesses over a bus its memory and processes a programme from the memory, have been known at the latest since Von Neumann. The remaining characteristics (allocation of the memory and means to...) are caused therefore - as far as understandably revealed - by the programme in the memory 17. A programme for a data-processing system is however not regarded as an invention according to §1 (2) 3rd PatG. If however the programme itself is based on no inventive activity, how can then its execution on a well-known computer system lead to an inventive effect in form of an inventive procedure?") 

(ML AnI "A second solution variant, however, is also mentioned in above quotation, since the means 3, 5, 6, 7, 8, 11 and 13 can also be implemented as discrete electronic components. No reference about the constitution these components can be found. Even their exact functionality appears unclear to the author. Something could be said on this only after having knowledge of the programme code to be processed. It is well-known how Rapid Silicon Prototyping ASICs can be developed with well-known IP blocks, however the allocation between the desired functional character of the means 3, 5, 6, 7, 8, 11 and 13 and the known logic blocks would need to be recognisable. This is not possible for the author on the basis of the disclosure writing.")



(ML Twc "Therefore one would like to suppose that according to the wording of the exception catalogue of §1 PatG such a subject would not be patentable for lack of inventive activity.")
) ) )

(tech (ML ThC "The Technical Character")

(ML Ahl "As a way out of the exception catalogue of §1 PatG the jurisdiction, however, has found ways how, despite the clarity of §1 PatG, explicitly excluded subjects can nevertheless be made accessible for patenting.")

(ML IWW "In relation to §1 par. 2 and 3 PatG here the 17th senate justifies")

(blockquote (ML Tan "This so-called exclusion catalogue has been based in general opinion on the thought that the subjects specified there lack the necessary technical character (see Busse, PatG, aaO, §1 Rdn 37; Schulte, PatG, 5th ed., §1 Rdn 60, in each case mwN; also the law justification, BlPMZ 1976, 332 left frame, points into this direction, if it says there among other things that the negative catalogue excludes only subjects which %(q:in the jurisdiction of the supreme court have not been recognised as inventions up to now); see also EPA, UC 3.5.1 %(q:computer programme product/IBM), GRUR Int 1999, 1053, 4.1)."))

(ML Wlc "Which group of interest carries this general view is not discussed. The circle of the software developers concerned, however, seems to share this view only to a small percentage. In the case of the computer programmes there are completely contrary opinions even among well-known patent specialists.")

(sects
(basasum (ML Tss "The Basic Assumption")

(ML IiW "In the basic assumption it is stated that %(q:in general opinion) all subjects specified in the exclusion catalogue of the §1 par. 2 lack the %(q:necessary technical character).")

(ML Ica2 "Instead of judging whether the registration subjects are one of the of the subjects specified in the exclusion catalogue, in simplifying practice it is only examined whether the registration subject demonstrates a %(q:technical character). In §1 PatG the word technique or technology is not mentioned at all by the legislator. An indirect relation results only from the stipulations on the state of technology in the subsequent paragraphs 3 and 4 PatG, from which it is to be deduced that thereby an invention in the sense of §1 PatG must also necessarily demonstrate a technical character.") 

(ML Iue "In the case of numerous registrations of programme related theories the courts came to the conclusion that these theories demonstrate a technical character nevertheless. Therefore obviously there are subjects in the exclusion catalogue of §1 par. 2 that demonstrate a technical character.")

(ML Atg "According to all rules of logic one would have to recognise at this point that by this the basic assumption is disproved, since it shows exceptions in this general version. The technical character of the registration subject can represent thus only a necessary, not a sufficient criterion for its patentability.")
)

(revk (ML Teo "The Reversal Conclusion")

(ML IWt "Instead, however, the reversal conclusion is drawn that also with the subjects specified in §1 par. 2 an invention and thus patentability can be present, if they contain a technical theory . The basic assumption was disproved by numerous decisions of the BGH and the BPatG, as has been pointed out. Each final conclusion, which is based on a defective basic assumption, can however only be defective itself. Suddenly it is not differentiated between necessary and sufficient any more; completely in the sense of art. 27 par. 1 TRIPS, whose applicability is still to be clarified. For a programme developer, who is forced by daily practice to think strictly logically, the argumentation for the sufficient patentability of technical theories is therefore not understandable. The exclusive restriction to technical theories is not stated in this form in §1 PatG, and the argumentation which tries to limit the exclusion catalogue of §1 PatG to this is simply wrong.")
)

(exk (ML Esc "Exclusion also of Technical Theories")

(ML Tel "The logical consequence from this would be that also technical theories can be excluded from patent protection, if they fall under the exclusion catalogue. [ BGH software patentability expert judge ] Melullis explains in this regard:")

(blockquote
(ML TWt "The legal state of affairs is formulated in such a way that it can exclude even a technical theory from patent protection according to a type of fiction, if it falls under the negative catalogue of Art. 52 paragraph 2 EPA (European Patent Agreement). If one proceeds from the pure wording of the law, it appears to be more appropriate to see the reason for the exclusion of the subjects specified there not in their lacking technical contents, but in the statement that they do not deserve a protection from the point of view of the law by the patent law or are not to be subordinated to this for overriding reasons."))

(ML Tta "This is exactly the point! The legislator did not aim at the absence of a technical theory as a criterion against patentability, because this could have been formulated very easily. Rather it wanted to exclude these articles just as such from the patent protection. Additionally, the legislator in no word made technical theories accessible for patent protection, because a theory is regarded a rule for a mental activity and thus just like programmes for data-processing systems not an invention.")

(ML MWn "Melullis however calls this a fiction. After the customary comprehension of a constitutional state it is the business of the legislator to promulgate laws generally accepted; the administration has to use these laws in individual cases; the courts have to decide separately for each individual administrative action whether the respective execution has been according to the law. How the wording of the law in a constitutional state can be regarded as a fiction, and how court decisions can as a consequence transform the sense of word of the law into the opposite, is completely incomprehensible for the author.")

(ML BWa "But without these constructions computer programmes as such as well as new applications of well-known articles with new programmes would not be patentable.")
)

(progtech (ML ToW "Technicity of Computer Programmes")

(ML Wll "With his view Melullis assumes %(q:in principle the technicity of each computer programme). More exactly said he differentiates between")

(ul
(quot (ML ttn "the mental logical concept..., which is based on the problem to be solved by the software, i.e. the actual programme contents including the programme idea which is the basis for it and"))

(quot (ML the "their conversion into the individual steps necessary for the execution.")))

(quot (ML Std "So the computer programme as such is within this interpretation the non-technical concept; this means it is the conception preceding the conversion into a procedural instruction to the computer."))

(ML Sun "So far, however, the author has not seen one German or European patent writing about programme related applications, in which it was not precisely this conception in the form of %(q:procedures for ...) achieving a certain effect that was claimed.")

(ML Fst "For Melullis is in contrast to this")

(blockquote
(ML IWi "In principle patentable - also because technically- ... the implementation of this first only mental solution into a procedural instruction for the computer and its control, i.e. the technical conception serving the realisation of this programme, as it found its expression in the finished programme.") )

(ML Tcd "The finished programme is for him thus no programme as such any more and thus in principle accessible to patent protection. He also seems to regard the conception as a plan for a mental activity. Other authors however define just these coded instruction sequences as a programme as such; have however no doubts to classify as technical and patent thus the underlying programme idea or business idea despite their characteristic as a plan for mental activity.")

(ML Aac "All these final conclusions seem to be based however again on the incorrect basic assumption on the exclusion catalogue.")
) 
) )

(technik
(ML ThW "The technicity concept")

(ML Mmj "Melullis uses an argumentation line frequently to be found, by giving its own definition to the term %(q:computer programme as such) as a %(q:non-technical concept; this means that of the conception preceding conversion into a procedural instruction to the computer). However, the technical field can in effect not be exactly defined. The respective specialist speaks normally of programming techniques, painting techniques, presentation techniques, football techniques, speech techniques, cultural techniques (reading, writing, counting), running techniques, grasping techniques for the playing of a musical instrument. Even with reference the sexual act one speaks occasionally of special techniques. In Brockhaus Encyclopedia the extensive definition of technique begins with")
 
(blockquote (ML VsW "[ from technica in internat. Scholar Latin, from Greek techne >art<, >talent<] in ancient Greece had the significance of art, trade, handicraft, also science and arts and crafts as well as technical skill to reach something determined (e.g. t. of painting). In an extended sense t. today is the constructive creation of products, devices and procedures by use of the materials and forces of nature and with regard to the laws of nature. ... ") )

(ML TGf "The BGH defines:")

(blockquote (ML AsW "A theory is technical if it is for deliberate action employing controllable natural forces for the achievement of a causally surveyable success, which is the direct result of the employment of controllable natural forces without inserting human understanding activity."))

(sects
(spraver (ML TlW "The %(q:Language Analysis) and %(q:Logic Verification) cases")

(ML Twc2 "The fact that also the term technique of the BGH changes, is illustrated by the following: Instructions to the human mind were always rejected so far by the BGH as not technical. Starting with the %(q:language analysis) decision, in which the senate states")
 
(blockquote (ML tne "the fact that it can be made use of the human understanding without that alone thereby the area of the technical is left, can be deduced already by the fact that theories for deliberate action employing controllable natural forces for the achievement of a causally surveyable success are accessible to patent protection ."))

(ML tsn "the BGH has called instructions to the human mind not necessarily hindering a patent and has discarded the requirement for immediate causality in producing an effect of controllable natural forces. Also in the %(q:logic verification) decision where it is only demanded that")

(blockquote (ML InW "If a theory for a programme for data-processing systems is coined by a realisation that is based on technical considerations, a delimitation criterion is found that is also accepted elsewhere and is promoting a uniform patent right practice for Europe, which permits to state the necessary technical character of a theory for a programme for data-processing systems."))

(ML tat "the BGH has rejected the employment of controllable natural forces as a direct condition of patentability and justified this with the fact that")

(blockquote (ML tan "the term technique of the patent right can not be understood statically, i.e. once and for all it being certain. It is rather accessible to modifications, if the technological development and an effective patent protection adapted to it require this."))

(ML Wea "With some maliciousness these two decisions can be understood also in such a way that even the instruction to the programmer to write a programme which is based on technical considerations can be regarded by the senate as technical. A interpretation very friendly to claims for the synopsis of both BGH decisions is also found in Hoessle, who sees in %(q:the %(q:non-static technology concept) a worthy and handy generalisation, which will render good services in practice).")
)

(glocke (ML TeB "The Bell")

(ML Aet "Another malicious example: For Melullis is to be proceeded in principle from the technicity of each computer programme, since the programme controls the computer.  But with the help of programmes also technical information is shown , which is based on technical considerations. The difference, whether it concerns a technical CAD design in source code with control statements for an CAD or print programme, or whether it is the description of Schiller's poem %(q:Die Glocke) (The Bell), with control statements for a browser programme written in HTML, is of rather academic nature, because both contain control statements, which can be run by a computer with the programme designed for the interpretation of the control statements; even the displayed information is in each case technical, even if the metallurgical aspect is not revealed by Schiller in any language customary for the bell caster.")
)

(furtech (ML Ala "Additional Technical Effect")

(ML Wlw "With the help of programmes each computer controls its periphery. From the point of view of the programmer it is not important whether this periphery is a screen, a printer, an I/O card, one or more A/D-D/A transducer or an other article designed for the control by a processor. With a programme in assembler language the control of the underlying hardware is described most directly; each individual assembler instruction describes and produces with its execution a directly technical effect . An additional technical effect, which goes beyond the customary cooperation between programme and computer - how it is demanded recently by the technical Boards of Appeal of the EPO (European Patents Office) - can consequently only be understood as an effect which is caused by none of the implemented programme steps, which is therefore independent of the programme related theory and which is not in connection with it. Why such an additional technical effect is then to be brought at all in connection with a programme remains unclear. If the mentioned technical effect is caused however by the running of one or more program steps, it can not be rightfully spoken of an additional effect, since this is the intended effect.") )

(hardes (ML PWo "Programmes for the Description of Hardware")

(ML VWW "VHDL or VERILOG programmes describe the structure of highly integrated circuits and control the configuration of FPGAs (Field Programmable Gate Arrays) . These circuits are sketched by using these programmes, are verified with the help of further programmes and are tested on appropriate logic simulators. The masks for wafer production are also produced by programmes, which those and/or VERILOG programmes evaluate . The finished IC, processor, ASIC is in the end nothing more than a product of several computer programmes, in which the solution, how the desired functionality is to be established, is finally only based on the programmes written in the selected language for hardware description . Such programmes contain certainly technical considerations, since these programmes describe concrete hardware and concrete semiconductor topographies can be produced with them. But nearly nobody applies for a patent for the code of these programmes - although their clear succession of commands is offered for sale as a VHDL or VERILOG programme and is thus completely exposed - but nearly exclusively for their desired functionality (thus for a problem to be solved somehow, see below).")
)
 
(techlim (ML Bti "Borders of the Technicity concept")

(ML FtW "Finally everything made by people can therefore be understood as %(q:technical) depending on the spirit of the time . It would only need to be distinguished from the divine creation. About any other delimitation of the term technique is disputable, since by this a painter will understand something else than a physicist, a musician something else than a craftsman, a writer something else than an engineer and a computer scientist finally something else than a lawyer. A purely legally coined technique definition for the delimitation of patentable against non-patentable, which leaves the respective target group of the term technique unconsidered, appears arbitrary before this background. As a necessary and at the same time sufficient criterion for the decision whether a subject should or not be patentable, the technique term therefore appears unsuitable, because detrimental to the requirement of the judicial security. As a necessary characteristic beside others it appears quite meaningful and in line with the legal tradition of many years of the German patent right.")
) ) )
 
(protsub (ML Tvo2 "The Objective Subject of Protection")

(ML uie "§1 par. 3 itself already points out another way to patenting software, by declaring")

(blockquote (ML Pot "Paragraph 2 opposes the patentability only to that extent, as protection is desired for the mentioned subjects or activities as such."))

(ML Stl "Since the request for protection is specified by the patent claims (§34 par. 1 (2) PatG), the logical reaction is to claim derived subjects instead of these concrete subjects. Instead of the concrete programme only the working procedure to be caused by the programme in a computer is claimed as a requirement for effect.")

(ML Ael "Also the subject, which was the basis for the decision 17 W (pat) 69/98 of the federal patents court in the patent claim 1, was generated after this pattern. The description of the desired working procedure however always precedes the actual coding in the programme development and is mostly done at the beginning of the design phase. The description of the working procedure therefore corresponds to the %(q:non-technical concept) thus specified by Melullis, to which hr would not like to grant any patent protection. The concept for the preparation of a programme is nothing else like a plan for a mental activity . A patent on that would forbid others to think in the same way.")
 
(ML TeW "The claimed working procedure can also be rated as a requirement for effect with exclusive functional characteristics. Schulte explains:")

(blockquote (ML FWp "Functional features define parts of the subject of the protection request not by physical characteristics, but by the indication of the effect or by properties. They are to be rated as technical features which can justify the novelty. Such indications of effect or design principles are a generalisation of the theory, because all means of equal effect are placed under protection, while the concretely revealed means are only examples. They are permissible, if by the indication of the effect, the property or the principle: i) a technical theory is sufficiently revealed; ii) the state of technology permits the generalisation of the theory; iii) the clarity of the claim does not suffer; iv) the feature objectively cannot be described more precisely.") )

(ML FWl "For a computer programme however the effect caused by it cannot normally be regarded as sufficient revealing, because the solution of the task %(q:How do I teach that to my computer?) is not revealed. In any case the program code describes the solution of the task objectively and more precisely.") 

(malkash (ML RWr "Revealing the Programme Code")

(ML Peg "Possibly the practice of the declarants to reveal no source code for programme related theories goes back itself to the exclusion catalogue of §1 (2) PatG, since a feature which is not regarded as inventive does consequently not have to be revealed. If however the programming of the solution of programme related theories needs to be revealed only so far that it must %(q:put each specialist into the position, without carrying out own inventive activity, to put into practice the given theory), one comes to the paradoxical situtation that for programme related theories practically nothing must be revealed any more, since also the solution of the most difficult tasks of programming cannot justify an inventive activity.")
 
(ML Ceu "Consequently, if the exclusion of computer programmes is applied only to the duty of the declarant to reveal, but not generally any more to the patentability, a situation occurs which may appear not dissimilar to the general registration and patenting practice.")
)

(solvpat (ML Pil "Patenting Solutions")

(ML Ttf "This globally practised approach to patent effects has however fatal consequences. §1 par. 1 PatG declares that, each invention is entitled to patent protection if it is new and based on an inventive activity. It follows however from this that each new, not obvious solution of the same task is entitled to patent protection . If one grants however patent protection to the problem to be solved, one gives thereby a monopoly on all possible solutions of the problem to be solved. Thus one violates the right at the intellectual property of those, who develop another new, not obvious solution of the same task. Even if it is not explicitly the task of the examiner to consider possible future violations of a patent , he is nevertheless already on the basis of the §52 par. 1 of the German Federal Civil Service Law obliged to take into consideration the well-being of the public at his work.")

(ML Ffn "For good reason it is not the patent examiner who can decide on the protection area of a patent - i.e. the area of all obvious, not inventive subjects - since he cannot know possible later violations cases yet. This is only the task of the violations judge.")

(ML Ale "Apparently Melullis wants to patent for a comparable reason only the %(q:conversion into the steps necessary for execution). He gives reasons for this:")

(blockquote (ML Tni "This mental approach avoids the danger that patenting software could lead to a monopolisation of thinking, which would exclude all other people from certain forms of the use of their minds and thus takes into account a concern, that motivates the prohibition in Art. 52 par. 2."))

(ML Tcp "The author can only approve that.")
)

(solprog (ML PmS "Programmes as Solutions")

(ML TsW "The fact that a task is not an invention, but an invention can lie only in the solution of the task has already been discovered by the BGH . Also the federal patents court recognises that the activity of the relevant specialist begins only with the realisation . In order to specify, what represents task and solution with programme related inventions, it is worthwile a look into a dictionary regarding programmes. The author quotes here only from the 4 sources immediately available to him. Therefore the selection is purely arbitrary.")

(dl
(l (ML DyW "Dictionary of data processing")
   (quot (ML IWs "In data processing technology an instruction or a sequence of instructions for the solution of a certain task is called a programme. (The term %(q:instruction) is to be understood here in the general sense, not in the special as an instruction of a programming language.) ... The preparation of programmes is called programming.")) )
(l (ML Buc "Brockhaus Encyclopedia") 
   (quot (ML dso "data technology: complete instruction for automatically controlled machines such as tool or textile machines, computers among other things, according to the steps necessary for the processing of a task are run successively, partially also at the same time. ... For data processing with digital computers the steps of a task to be processed are to be described (programmed) in such a manner in every detail that the computer receives clear instructions for the work routine.")) )

(l (ML Dmn "Dictionary of computer science and data processing") (ML Aes "A complete instruction as well as all necessary agreements for the solution of a task. Supplement: (1) The term %(q:task) is not specified in the present definition. In practice by this usually a task of data processing or - regarded from another point of view - a section from a human data-processing process (data-processing) is understood, that is completely formalised and that, expressed in algorithms, can finally be executed by a computer.") )

(l (ML Epr "Encyclopædia Britannica") (ML cio "computer program, detailed plan or procedure for solving a problem with a computer; more specifically, an unambiguous, ordered sequence of computational instructions, necessary to achieve such a solution.") )
)

(ML Tha "There seems to be consent among the specialists that always the programme represents the solution of the task . Here the cat bites itself again into the own tail: in the case of the solution of a task by a computer the executable programme itself (as such) with its clear instructions is always the solution. Since only the solution of a task can be seen as an invention, for programmes for data-processing systems as such however no protection may to be requested, programmes for data-processing systems might therefore not be patented.")

(ML Iah "If one ignores however that patents are given only for the solutions of problems and claims the working procedure (the idea, the task or effect) of data-processing systems instead, at first glance nothing opposes patenting any more. But a patent on the working procedure of a data-processing system claims just again protection vis-a-vis all other programmes with the same effect, independent of whether these are new and not obvious or not. The right at the intellectual property of others is violated and the technical progress is obstructed.")

(sects
(idesolv (ML Iao "Idea and Solution") ; 4.3.1

(ML Fna "For furhter explanation perhaps an example is appropriate. Before his time as a patent examiner the author of this article had to develop, among other things, the instrument control software for the two FoRS focal instruments of the Very Large Telescope Project of the European Southern Observatory (ESO). Each of these instruments contains more than 50 motor drives and a multiplicity of further sensory and actuatory elements. The idea (the solution principle) how the control, co-ordination and supervision of this multiplicity of components could be managed, was born during the return journey from a committee meeting in the train from Göttingen to Munich. The temporal expenditure for this amounted to approx. one hour. (According to the regulations of the travel expenses law this time was not even recognised as work time.) The search for suitable library functions, object classes and data structures, with which this idea could be programmed, lasted approx. 2 work months. For the first executable programme version, which realised the underlying idea concretely, further 4 months of programming work passed by. Therefore after half a man-year the solution could be presented for the first time. This was done by a lecture to the client ESO, whose foils were published the same day on the Internet . For the completion of the programme including the customary bug fixing approx. 2 man-years must be estimated.")
 
 
) ) ) ) 
 
(diskl (ML TCe "The Exclusion Catalogue as Legal Disclaimer")

(ML Idd "It is also frequently tried to let the term %(q:programme as such) appear unclear or to redefine it in order to minimise the number of the subjects excluded from patent protection thereby. But the following argumentation appears more convincing to the author: If for the subjects or activities mentioned as such no patent protection can be requested, this corresponds to a legally imposed disclaimer. In the case of programme related inventions this would have as a consequence that a programme could never infringe a patent as such, since as such no protection is entitled to a programme. Also a subject well-known from the state of technology, which executes only a new (according to §1 (2) 3rd not inventive) programme, cannot infringe a patent, since %(q:the violation form does not represent any patentable invention vis-a-vis the state of technology). An infringement of a patent is thereby ruled out. Thus the possibility of a free planning of the programmers would be ensured. Much suggests that the law maker explicitly wanted to protect just this free planning possibility; just like he had it foreseen for plans, rules and procedures for mental activities.")
 
(ML Als "A comparable argumentation develops also Melullis for the mental logical concept as the basis for computer programmes and explains for this:")

(blockquote
(ML Whj "With this view the programmes for data-processing systems represent only a further manifestation of the facts, in which also conceivable regulations and proposals for the human behaviour can be recorded, with which the justification of an exclusivity right could mean a danger for the freedom of thinking.") )

(ML HWy "He grants however only a subordinated role to the guarantee of this freedom, so that he can justify the patent protection of executable programmes.")

(ML HrW "However, if one attributes at least equivalent role to the protection of this freedom of thinking and of programming - as does the author - then the exclusion list of the §1 PatG would be understood in such a way that subjects or activities mentioned may not be prevented by the patent right; independent of whether they demonstrate a theory for technical acting or not. This aspect seems to be shared also the majority of the opponents of software patenting who see the booming progress of their economic sector endangered by patenting software - no matter whether in the form of ideas, concepts, algorithms (mathematical methods) or in form of the concrete source code. The high number of signers of the EuroLinux petition confirms this. In computer sciences as well in the computer industry the driving force for development is the disclosure of the own development and the improvement of existing programmes. Without the quick and unhindered distribution and discussion of the %(q:Requests for Comments), which form the basis of the Internet, today's boom of the software industry is inconceivable.  The substantial character of the scientific development and of the formation of opinion has been described very plasticallt by Wilhelm Busch:") 

(blockquote (tan (ML Sae "Science is and remains what one copies from the other.") (ML Ihv "In German this rhymes very nicely.")))

(ML Pef "Programmes in form of source code as linguistic works live on the fact that they can be quickly adapted to the respective requirements. Only with this freedom there can be free lance programme developers - as there are free lance doctors, artists or writers.")

(ML BaW "But back to the view of the exclusion catalogue as legal disclaimer. It would be certainly interesting to see, how a violation judge would react if the defendant indicated for its defence, that he only programmed a %(q:programme as such), based on the conviction well-founded by the legal wording that %(q:programmes as such) cannot violate any patents, since on %(q:programmes as such) no patent protection can be granted.")

(ML Its "It was and is obvious that for each programming the programme must be stored, tested and thus a working procedure must be executed. Therefore a patent protection aimed at these partial aspects of the programme development can just as little be wanted by the legislator. If the aforementioned argumentation is perhaps not able to convince everybody, it must however raise at least justified doubts. If sometimes in dubio per inventore is demanded, one can demand in this case with at least the same right in dubio per reo, even if doubts in the patent right are not appropriate. Whether the argumentation mentioned is logically perfect, may be elaborated in public discussion or on a concrete individual case.")  )

(srcclm (ML Sea "Source Code in the Patent Claim")

(ML IWn "If one wants to regard programmes for data-processing systems themselves as inventions and to make these accessible for patent protection as such - as this is intended with the planned novel of the EPA - then one should think more precisely what could be the consequences.")

(ML IWj "If one regards the definitions of a programme quoted above again, one recognises that all instructions understandable for a computer which solve a task represent a programme. The specialist knows programmes in binary form and/or their description in Hexcode, as well as programmes in one of the many programming languages (Java, C++, Tcl/Tk, Perl, Lisp, Phyton, Postscript, HTML, TEX, Ada, Fortran, Pascal, Basic, Cobol,...). Also combinations of graphic programming with textual supplements are known (LabView and others). If the programme is executable on a computer and if it causes the procedure to be implemented, it is to be regarded as solution of the task, all the same whether it is executable by the processor directly - or only with the help of further well-known programmes (operating system, compiler, interpreter, emulator, virtual machine,...). Together with the indication, under which conditions this programme is executable (type of processor, operating system, program libraries or what else may influence executability) the programme code is, in the opinion of the author, even the only unambiguous revealing form of the solution of the task (by Brockhaus it is called %(q:clear cut), by britannica.com %(q:unambiguous)).")

(srcklar (ML CWr "Clarity of Source Code")

(ML Smj "Since each programme code is not at all inferior in clarity, shortness and conciseness in the description of the solution to a mathematical formula, a chemical structural diagram or a microbiological DNA sequence, it should not be objected as a characteristic of a patent claim, since all descriptions are suitable %(q:that represent a technical state of affairs, e.g. also formulas, which contain mathematical relationships between physical parameters. On the other hand non-technical characteristics are to be deleted, unless they contribute to the better comprehension of the technical theory, like in some cases an algorithm).  Insisting on the requirement to use the German language makes no sense, since each programme can also be created in German, this however is in opposition to the requirements of clarity and scarcity, which depends on the comprehension of the specialist . Schickedanz lists numerous German, US-American and European patent applications in which %(q:formula requirements) are contained. He describes also a beautiful comparison of source code in different programming languages for Euclide's algorithm.") 
)

(subchar ;6.2 
  (ML Tna "The Substantial Characteristics")

(ML Cad "Consequently, if programmes for data-processing systems are deleted from the exclusion catalogue of §1 PatG and/or Art. 52 EPA, programmes are also regarded as inventions. For each programme its individual instructions are substantial. If a programme itself or a subject that contains a programme is claimed, consequently also the substantial instructions of the programme as necessary characteristics for the solution of the task must be indicated in the patent claim . Each instruction of a programme can then justify an inventive step. The same would have to apply also to new and %(q:inventive) data declarations. These features (source text) would have to be compared with other source texts from the state of technology on novelty and inventive activity. The author can imagine this; however he cannot imagine with the best will that this is wanted by the proponents of the patentability of software.")  )

(srcprot ;6.3 
  (ML PWs "Protection Scope of Patent Claims with Source Code")

(ML Ttt "The protection scope of a patent claim with source code should depend on the form the declarant wants to reveal his solution.")

(ML IWc "If someone would claim for example a programme in binary form, its protection area would be limited exclusively to this execution form on the processor platforms indicated for this. Even if only one bit is different, one can not even speak of an obvious change, because how can the specialist know which bit is relevant and which not. The declarant would be certainly better served in this case by its copyright.")

(ML Iee "If he would claim a programme for example in Java source code, some modifications of this programme are certainly to be regarded as obvious. So the naming for variables, classes and instances may not play a role. Also some programme section for error handling might have only small importance, except this error handling would be important for the solution principle. In the case of a conversion of the Java code into C++ code the author would however have already strong doubts whether this is possible exclusively with steps which are always obvious (trivial programmes excluded). A translation into Lisp would certainly need far more than only obvious steps. Even a porting from one operating system to another or the change from one programme library to another can provide hilarious difficulties. Which changes are here still obvious in this case and which are not, can practically not be recognised any more from the outside, except for somebody who ports the code by himself. The actual protection area of a claim with source code in a high level programming language would be however certainly also very close to the programme text. Therefore it seems to be better also in this case for the declarant to refer to its copyright instead of requesting expensive patents.") 

(ML Ieh "In a practical example even the establishment of the correct end condition of an individual iteration loop can need ideas which are not obvious, so that by the establishment of the end condition of the iteration loop an inventive step was already taken. The reader may get from this a small impression, which indeterminable multiplicity of possible solutions can already provide a small cutout of a programme text. However, since practically each possible new creation of a programme itself would be patentable and could also not infringe the other patents as a not obvious solution of the same task, these patents would be practically worthless and would only protect against the copying of the own works. One would have copyright again in patent form, embroidered with additional fee incomes for the offices.")
 
(ML Hdr "However there is also the danger that it will be tried to claim each of these individual steps separately. This would however have catastrophic consequences for each programmer. He would have to examine factually with each line of programme text whether just on this one a patent was given. Free programming would practically no longer be possible.")  

(ML Iol "If one claims however only his processor, language and operating system independent programme idea as a requirement for effect, and receives for it a patent on a %(q:procedure for...), the profit will be huge. Each solution which realises this programme idea - as a plan for a mental activity - infringes the patent. For this one doesn't even have to realize one's programme idea, let alone to reveal this realisation. But then, unfortunately, no solution was claimed as patentable invention, but only a plan for a mental activity.") ) )

(fin ;7 
  (ML CuW "Concluding Remark")

(ML Aaa "As one can see at this long enumeration of problems with the present patenting of programmes for data-processing systems, there would be more than sufficient controversy reasons for objection and nullity proceedings. The fact that simultaneously with the embittered discussions between proponents and opponents no true tide of objections and processes against the present practice of the patent administration for programme related theories is happening, can only surprise. It is possible that the pain threshold of the software developers is not exceeded yet. At the latest if the patentability of software is approved by the legislator, however, this pain threshold will be reached.") 

(ML Ior "It is also possible also that none of the sides can understand the other any more, since both proceed from incompatible principles. An indication for the latter may be the following episode: At the helpless attempt of the author to explain to a befriended computer scientist, why programmes would not be patentable as such, programme related theories with an additional technical effect however would be, he reacted bluntly: %(q:You are completely nuts!) This clear pronounciation caused the author to think quite a bit.")
)
)
)

(mlhtdoc 'bmwi-luhoge00 nil nil nil (papritab)

(ML Dwn "Das Gutachten erläutert eine altbekannten Rechtsauffassung des Münchener Patentanwalts Axel H. Horns, die er bereits früheren Artikeln und umfangreichen Diskussionen dargelegt hatte.  Horns kann dem Art 52 EPÜ keine klare Bedeutung abgewinnen.")

(filters ((lt ahs 'swpatkorcu)) (ML Eds "Es folgen anstelle einer detaillierten Würdigung des Gutachtens zunächst ein paar Auszüge aus unserer Diskussion mit Axel Horns.  Zur Einstimmung auf das Thema blättere man in der %(lt:juristischen Literatur über die traditionelle Definition der Technik und der Erfindung).  Horns rezipiert diese Literatur nicht und oder blendet sie nach Kräften aus.  Z.B. meint er, Gert Kolle habe 1977 auf einer anderen rechtlichen Grundlage geurteilt als wir heute.  Das LuHoGe-Gutachten verwendet sehr viel Raum auf persönliche Meinungen, die längst kraftvoll widerlegt wurden.  Hierzu gehören auch die Vorstellungen von Robert Gehring zur Neuheitsschonfrist, mit denen eine Forderung der Patentlobby des BMBF im Namen von %(q:OpenSource) weitere unverdiente Unterstützung erhalten soll."))

(ul
 (ah "http://www.ffii.org/archive/mails/swpat/2000/Aug/0040.html" (colons "Hubertus Soquat 2000-08-03" (ml "Bundeswirtschaftsministerium vergibt Studie Open Source Software")))
 (ah "http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2&tsid=212&tdid=588&page=0" (ML Oru "Das BMWi verkündet die Veröffentlichung des Gutachtens"))
 (tpe (ah "http://www.jurpc.de/aufsatz/20000223.htm" (ml "Artikel ähnlichen Inhalts von Axel Horns in JURPC 2000-10")) (ML Eeh "Ein weiterer Artikel dieser Art erschien in GRUR 2001-01"))
 (ah "http://www.heise.de/tp/deutsch/inhalt/te/4539/1.html" (ML Tve "Telepolis zu Meinungsverschiedeneheiten über das LuHoGe-Gutachten"))
 (ah "http://www.vdi-nachrichten.de/aus_der_redaktion/akt_ausg_detail.asp?id=4207&cat=3" (ML Vub "VDI-Nachrichten zu Meinungsverschiedenheiten über das LuHoGe-Gutachten"))
 (ah "http://www.ffii.org/archive/mails/swpat/2000/Dec/0018.html" (ml "Diskussion zwischen Axel Horns und Hartmut Pilch"))
 (ah "http://www.ffii.org/archive/mails/swpat/2000/Dec/0199.html" (ml "Telepolis über Lutterbeck-Gutachten und FFII"))
 (ah "http://www.ffii.org/archive/mails/swpat/2000/Dec/0212.html" (ml "Lutterbeck-Interview in Financial Times"))
 (ah "http://www.ffii.org/archive/mails/swpat/2000/Dec/0217.html" (ml "Daniel Rödding: Rechtliche Sonderbehandlung von OpenSource nicht sinnvoll"))
 (linul 
   (ah "http://www.ffii.org/archive/mails/swpat/2001/Jan/" (ml "In den ersten Januarwochen wurde die Hornssche Rechtsauffassung besonders ausführlich diskutiert."))
   (ah "http://www.ffii.org/archive/mails/swpat/2001/Jan/0013.html" (ml "Antwort von phm auf Axel Horns"))
  (ah "http://www.ffii.org/archive/mails/swpat/2001/Jan/0016.html" (ml "xuan: warum geistige und materielle Erfindungen unterscheidbar sind"))
  (ah "http://www.ffii.org/archive/mails/swpat/2001/Jan/0051.html" (ml "noch eine Antwort auf Axel Horns")) )
  (ah "http://www.jurpc.de/aufsatz/20010040.htm" (ml "Artikel von Wolfgang Tauchert (Swpat-Chef des DPMA) mit Anmerkungen zum LuHoGe-Gutachten."))
)
)

(mlhtdoc 'boch97 nil nil nil (papritab))

(mlhtdoc 'boch97-koerber nil nil nil (papritab)	
(sects
(patpac (ML PBs "Patentfrieden im IT-Bereich 1980 durch USA aufgekündigt")

(ML KnF "Körber beginnt mit einem Rückblick auf gute alte Zeiten, in denen seine Branche, die der Elektrontechnik, Elektronik und Informationstechnik, noch in Frieden arbeiten konnte und Patente nur die ihnen gebührende Rolle spielten:")

(blockquote
"..."
(ML BHs "Bei der Nutzung der verschiedenen Möglichkeiten des Patentschutzes durch Großunternehmen gab und gibt es jedoch branchenspezifisch unterschiedliche Schwerpunkte.  So spielt für die chemische und insbesondere die pharmazeutische Industrie die Sicherung der Alleinstellung des eigenen Unternehmens durch Ausschluss von Wettbewerbern mit Hilfe des Patentschutzes nach wie vor eine bedeutende Rolle.  Dies erklärt sich daraus, dass auf breiter Basis betriebene Forschungsarbeiten in der Regel nur bei verhältnismäßig wenigen Produkten zum schließlichen Erfolg führen und der hohe Forschungsaufwand beim Vertrieb dieser Produkte über ihren Preis wieder eingespielt werden muss.  Der Patentschutz ist daher Voraussetzung für die Investition in ein Produkt.  Die betreffenden Unternehmen melden daher ihre Erfindungen in der Regel auch in wesentlich mehr Ländern zum Patent an, als dies in anderen Branchen üblich ist.  Dazu kommt, dass Sachpatente auf chemische Substanzen in vielen Fällen kaum zu umgehen sind.")
(ML IWp "Im Vergleich hierzu hat der Ausschluss von Wettbewerbern in den Schutzrechtsstrategien von Großunternehmen der Elektrotechnik- und Elektronikindustrie in den letzten Jahrzehnten nur eine verhältnismäßig geringe Rolle gespielt.  Wenn man sich mit Wettbewerbern über die Hauptmotive für die Anmeldung von Patenten unterhielt, bekam man in aller Regel die Antwort, es gehe in erster Linie um die Sicherung der nötigen Freiräume für die eigene Geschäftstätigkeit, bei der man durch Patente von Wettbewerbern möglichst nicht behindert werden wolle.  Patente wurden also vorwiegend dazu eingestzt, einerseits die eigenen Entwicklungsprojekte durch rechtzeitige Schutzrechtsanmeldungen von später entstehenden Schutzrechten Dritter freizuhaltenund, soweit dies nicht möglich war, im Austausch gegen Lizenzen an den eigenen Schutzrechten Benutzungsrechte an den Schutzrechten der Wettbewerber zu erwerben.  Der Ausschluss eines Wettbewerbers vom Markt spielte und spielt auch heute noch dem gegenüber praktisch keine wesentliche Rolle.")
(ML Udu "Unter anderem erklärt sich diese Situation daraus, dass auf den entsprechenden technischen Gebieten im Verlauf der normalen Entwicklung gegenseitige Überschneidungen und daraus entstehende schutzrechtliche Abhängigkeiten oft nicht zu vermeiden sind.  Auf verschiedenen Gebieten, beispielsweise in der Kommunikations- und Informationstechnik, ist die sogenannte Interoperabilität, also das Zusammenwirken verschiedener Systeme, sogar eine wesentliche Voraussetzung für den Markterfolg.  Dazu kommt, dass in der Elektrotechnick und Elektronik Möglichkeiten zur Umgehung von Schutzrechten, wenn auch mit einigem Aufwand, häufig leichter gefunden werden können, als dies beispielsweise beim chemischen Stoffschutz der Fall ist. ... Gerichtliche Auseinandersetzungen waren verhältnismäßig selten.")
(ML DiW "Die Zeiten des relativen Patentfriedens sind indessen auch in unserer Branche weitgehend zu Ende gegangen.  Zunächst haben gesetzliche Maßnahmen in den USA, wie die Erstreckung des Schutzes von Verfahrenspatenten auf das unmittelbare Verfahrensprodukt und die Gründung des Court of Appeal for the Federal Circuit, zu einer Stärkung des Patentschutzes geführt.  Dies hat einige Unternehmen dazu veranlasst, ihre Schutzrechte wesentlich aggressiver als bisher einzusetzen. ...")
)
)

(norm (ML Ilr "IT-Normierung ein globaler Kampfschauplatz, für den Siemens rüsten muss")

(ML KeW "Körber erläutert im folgenden, warum nun für Siemens und andere Großunternehmen die Patentstrategie zu einer Überlebensfrage und die Patentabteilung zu einer zentralen unternehmerischen Abteilung geworden ist.  Er geht dabei gar nicht auf die Frage ein, ob die von den USA eingeführten neuen Regeln gut für die Branche oder auch nur gut für Siemens sind.  Diese Frage stellt sich selbst für einen Weltkonzern nicht.  Die Regeln sind als Sachzwänge vorgegeben.  Aufgabe von Körbers Abteilung ist es, unter den gegebenen Umständen nach der besten Überlebensstrategie zu suchen.  Wer über keine schlagkräftige Patentabteilung verfügt, kann sich selbst mit viel Geld kaum den Zutritt zum Markt erkaufen.")

(blockquote
(ML Bul "Beim Erwerb und bei der Vergabe von Patentlizenzen spielt in dieser Situation der %(q:Preis) eine wesentlich bedeutender Rolle als dies vordem der Fall war.  Vor dem Abschluss von Patentlizenzverträgen werden heute üblicherweise die beiderseitigen Patentbestände in aufwendigen Verfahren im einzelnen inhaltlich bewertet.  Bei aussichtsreichen, erfolgversprechenden Zukunftstechnologien kann es für ein Unternehmen, das selbst keine Gegenlizenzen an wertvollen eigenen Schutzrechten anbieten kann, sogar schwierig werden, gegen Geld allein Patentlizenzen zu Bedingungen zu erhalten, die es selbst noch als angemessen betrachten kann.")

(ML EWs "Eine wichtige Rolle spielen Patente inzwischen auch bei internationalen und nationalen Standards.  Besonders auf technischen Gebieten, wie der Kommunikations- und Informationstechnik, bei denen es wesentlich auf die Interoperabilität ankommt, gehen technische Standards inzwischen sehr ins Detail.  Dadurch steigt die Wahrscheinlichkeit, dass für den Standard wesentliche Patente bestehen oder angemeldet werden, also solche Patente, die bei Benutzung des Standards nicht umgangen werden können.  Bei der Vereinbarung von Standards durch die Standardisierungsorganisationen ist es zwar üblich, dass die beteiligten Patentinhaber sich bereit erklären müssen, an ihren Patenten Lizenzen zu angemessenen, nicht diskriminierenden Bedingungen zu erteilen.  Dennoch ist ein Unternehmen, das selbst keine wesentlichen Patente besitzt, im Nachteil.  Es muss nicht nur von allen Patentinhabern Lizenz nehmen, sondern kann auch die Durchsetzung eigenenr Entwicklungen als Standards nicht durch entsprechende Lizenzpolitik fördern.  Bei sogenannten De-facto-Standards, die sich ohne Vereinbarung im Markt entwickeln, ist die Situation gegebenenfalls noch kritischer.  Hier besteht für die Schutzrechtsinhaber, die den Standard etabliert haben, keinerlei Verpflichtung, jedem interessierten Wettbewerber eine Lizenz einzuräumen.  Eigene wesentliche Patente können für ein Unternehmen die einzige Möglichkeit sein, nicht aus dem Markt gedrängt zu werden.")
(ML Zni "Zusammenfassend lässt sich somit feststellen, Patente sind für Großunternehmen - und nicht nur für diese - heute wichtiger denn je.")
(ML Fnw "Für die Patentabteilung eines Unternehmens ergeben sich unter diesen Aspekten über die üblichen Dienstleistungsfunktionen hinaus unternehmerische Aufgaben.  In unserem Unternehmen sehen wir diese in der %(q:Stärkung der Wettbewerbsfähigkeit des Unternehmens durch Entwicklung und Umsetzung von Strategien für den gewerblichen Rechtsschutz).")
"..."
)
)

(etend (ML Snn "Siemens treibt Amerikanisierung der deutschen Erteilungspraxis voran")

(ML Khi "Körber erläutert, dass die Patentabteilung von Siemens in Deutschland seit ca 1990 auf die Änderung der Erteilungskriterien zugunsten von Softwarepatenten gedrängt hat.  Das tat sie nicht deshalb, weil ihr die neuen Regeln für die Branche oder auch nur für Siemens besonders vorteilhaft erschienen.  Im Gegenteil, für die Mitarbeiter von Siemens bedeutete die Anmeldung von Softwarepatenten eine unverhältnismäßig schwierige und zeitraubende Aufgabe.  Aber um Siemens für den Kampf im amerikanische dominierten Weltgeschehen zu rüsten, war es nötig, sich dieser Aufgabe auch zu Hause in Deutschland zu stellen:")

(blockquote
(ML Bis "Basis für Patente und damit auch für Patentstrategien sind Erfindungen, und zwar in unserem Haus wie in anderen Großunternehmen in erster Linie die Erfindungen der eigenen Mitarbeiter.  Die möglichst systematische und vollständige Erfassung der entstehenden Erfindungen ist daher von besonderer Bedeutung.")
(ML HWu "Hier hatten wir Anfang der 90er Jahre ein gewisses Problem.  Die Zahl der jährlich bei der deutschen Patentabteilung eingehenden Erfindungsmeldungen lag über mehr als zehn Jahre jeweils um die 2000, und dies trotz steigenden F&E-Aufkommens, trotz eines zunehmenden Anteils neuer Produkte und Systeme an unserem Jahresumsatz und trotz ständiger Bemühungen der Mitarbeiter der Patentabteilung, Erfindungsmeldungen anzuregen.  Wir fanden für dieses Phänomen schließlich zwei Hauptursachen.")
(ML Dnt "Die erste Ursache war, dass speziell auf dem Gebiet der Informations-, Kommunikations- und Automatisierungstechnik zunehmend Hardwarelösungen durch Softwarelösungen ersetzt wurden und dass viele Softwareentwickler dem Missverständnis unterlagen, Softwareerfindungen seien nicht patentfähig.  Zudem waren anfänglich auch nur wenige Mitarbeiter der Patentabteilung hinreichend erfahren in der Abfassung von Patentanmeldungen auf softwarebezogene Erfindungen und in der Führung von entsprechenden Verfahren vor den Patentämtern, ganz abgesehen von der zunächst keineswegs softwarepatentfreundlichen Praxis einiger Ämter.  Zur Lösung dieser Probleme beauftragten wir eine Arbeitsgruppe erfahrener Patentingenieure, weitere ihrer Kollegen zu trainieren und Informationsveranstaltungen für die entsprechenden Entwicklungsabteilungen durchzuführen.  Dennoch bleibt die Patentierung softwarebezogener Erfindungen eine zeitraubende Aufgabe für Erfinder und Patentingenieure sowohl wegen der üblicherweise hohen Komplexität der Materie, als auch wegen der noch uneinheitlichen Praxis von Patentämtern und Gerichten.  Andererseits würde der Verzicht auf die Patentierung von softwarebezogenen Erfindungen gerade auf den technischen Gebieten mit den meistversprechenden Zukunftsaussichten zu erheblichen wettbewerblichen Nachteilen führen.  Wir sind daher entschlossen, die Patentierung softwarebezogener Erfindungen weiter voranzutreiben und wie bisher unseren Beitrag zur Weiterentwicklung von Patentamtspraxis und Rechtsprechung zu leisten.")
(ML Esg "Ein weiterer Grund für die Stagnation bei Erfindungsmeldungen war der, dass manche Entwickler unter dem ständig steigenden Termindruck nicht mehr die Zeit fanden, während der Arbeitszeit eine Erfindungsmeldung abzufassen oder sich wenigstens mit dem für sie zuständigen Patentingenieur in Verbindung zu setzen. ...")
)

(ML Hnu "Hier sei daran erinnert, dass Siemens mit IBM zu den wenigen Unternehmen gehört, die vor dem BPatG und BGH anstrengende Musterprozesse geführt haben, um die Patenterteilungskriterien auf dem Wege der Rechtsprechung zu ändern.  Z.B. wurde das BGH-Urteil %(q:Chinesische Schriftzeichen) durch eine Siemens-Klage herbeigeführt.")
)
)
)

(mlhtdoc 'boch97-gispen-de nil nil nil (papritab))
(mlhtdoc 'boch97-gispen-ns nil nil nil (papritab))
(mlhtdoc 'boch97-gering nil nil nil (papritab))
(mlhtdoc 'boch97-reiner nil nil nil (papritab))
(mlhtdoc 'boch97-rammert nil nil nil (papritab)) 
(mlhtdoc 'ist-tamai98 nil nil nil (papritab))
(mlhtdoc 'uplr-newell86 nil nil nil (papritab))
(mlhtdoc 'nber-hallham99 nil nil nil (papritab))
(mlhtdoc 'trips-staeh97 nil nil nil (papritab))
(mlhtdoc 'ifo-taeger79 nil nil nil (papritab))
(mlhtdoc 'wuw-kaufer70 nil nil nil (papritab))
(mlhtdoc 'benkard88 nil nil nil (papritab))
(mlhtdoc 'bpatg-990614 nil nil nil (papritab))
(mlhtdoc 'bgh-xzb88013 nil nil nil (papritab))
(mlhtdoc 'bgh-xzb89024 nil nil nil (papritab))

(mlhtdoc 'bgh-dispo76 nil nil nil (papritab)

(bridi 'cf (ahs 'grur-kolle77))
(blockquote (ML Sai "Stets ist aber die planmäßige Benutzung beherrschbarer Naturkräfte als unabdingbare Voraussetzung für die Bejahung des technischen Charakters einer Erfindung bezeichnet worden.  Wie dargelegt, würde die Einbeziehung menschlicher Verstandeskräfte als solcher in den Kreis der Naturkräfte, deren Benutzung zur Schaffung einer Neuerung den technischen Charakter derselben begründen, zur Folge haben, dass schlechthin allen Ergebnissen menschlicher Gedankentätigkeit, sofern sie nur eine Anweisung zum planmäßigen Handeln darstellen und kausal übersehbar sind, technische Bedeutung zugesprochen werden müsste.  Damit würde aber der Begriff des Technischen praktisch aufgegeben, würde Leistungen der menschlichen Verstandestätigkeit der Schutz des Patentrechts eröffnet, deren Wesen und Begrenzung nicht zu erkennen und übersehen ist.") (ML Ene "Es verbietet sich demnach, den Schutz von geistigen Leistungen auf dem Weg über eine Erweiterung der Grenzen des Technischen -- die auf deren Aufgabe hinauslaufen würde -- zu erlangen."))

)



(mlhtdoc 'cr-esslib00 nil nil nil (papritab)

(ML Ifg "In der einleitenden Zusammenfassung wird die Lage Anfang 2000 prägnant beschrieben:")

(blockquote
(ML Dtt "Die jüngste Entwicklung der Rechtsprechung in Deutschland und Europa hat die Erlangung und Durchsetzung von Softwarepatenten erheblich erleichtert.")
(ML Ibh "Industrie- und Dienstleistungsunternehmen sind daher gezwungen, über eine patentrechtliche Absicherung ihrer Investitionen in neue Software nachzudenken, nicht zuletzt deshalb, um  sich über Kreuzlizenzen Zugang zur patentgeschützten Technologie anderer verschaffen zu können.") 
(ML Bqn "Bei der Auswahl der geeigneteren Patentstrategie ist zu berücksichtigen, daß sich Software im Gegensatz zu den meisten traditionellen technischen Gegenständen sehr leicht kopieren und über das Internet weltweit verbreiten läBt.")
)

(ML WWd "Wie üblich stellen sich die Autoren bezüglich der Bedeutung von %(q:als solches) dumm, und erläutern, welche Gesetzesumgehung dieses Nichtverstehen seitens der Rechtsprechung ermöglicht hat:")

(blockquote
(ML Nve "Nach Art. 52 Abs. 2 lit. c des Europäischen Patentübereinkommens (EPÜ) und §1 Abs. 2 Nr. 3 Patentgesetz (PatG) sind Programme für Datenverarbeitungsanlagen unter anderem zusammen mit mathematischen Methoden, Plänen, Regeln und Verfahren für gedankliche oder geschäftliche Tätigkeiten in einer Liste von Gegenständen enthalten, die nicht als Erfindungen anzusehen sind, für die Patente erteilt werden können.")
(ML Ddn "Der darauf folgende Absatz im Gesetzestext stellt aber klar, daß die Patentfähigkeit der aufgezählten Gegenstände nur dann ausgeschlossen ist, wenn für diese Schutz %(q:als solche) begehrt wird. Programme für Datenverarbeitungsanlagen (nach heutigem Sprachgebrauch Computerprogramme oder Computersoftware) sind daher nur %(q:als solche) nicht patentfähig.") 
(ML Aap "Aus dieser interpretationsbedürftigen gesetzlichen Bestimmung, die etwa dem amerikanischen oder dem japanischen Patentgesetz fremd ist, leitet sich das weitverbreitete Vorurteil ab, Software sei überhaupt nicht oder nur in engem Zusammenhang mit bestimmten Hardwarekomponenten durch ein Patent schützbar.")
(ML Dhh "Das Europäische Patentamt und der Bundesgerichtshof haben das Gesetz vereinfacht ausgedrückt wie folgt ausgelegt: Nicht patentfähige Computerprogramme %(q:als solche) sind nicht-technische Computerprogramme, während andererseits technische Computerprogramme patentfähig sind.  Die Rechtsprechung in Deutschland und Europa hat sich - mit einigen Umwegen - im Trend dahin gehend entwickelt, den Begriff des Technischen immer weiter auszulegen, so daß heute bereits eine Vielzahl von Softwarepatenten vom Deutschen und Europäischen Patentamt erteilt worden sind.")
)

(filters ((tl ahs 'trips27)) (ML Ein "Es folgt eine weitere Formulierung der TRIPS-Lüge:"))

(blockquote
(ML Dlo "Das TRIPS-Übereinkommen (Trade Related Aspects of Intellectual Property Rights), das von allen Mitgliedsstaaten der Welthandelsorganisation (WTO) unterzeichnet wurde, sieht in Art. 27 Abs. 1 vor, daß %(q:Patente für Erfindungen auf allen Gebieten der Technik) bei Erfüllung der sonstigen Patentierungserfordernisse gewährbar sein müssen. Eine international kompatible Interpretation der TRIPS-Bestimmungen kommt zu dem Ergebnis, dass auch Software-Erfindungen der Technik zuzuordnen sind und damit dem Patentschutz zugänglich sein müssen.") )

(ML Hts "Hierdurch gerät die Rechtsauslegung jedoch in Interpretationsnöte.  Der Begriff %(q:Computerprogramm) wird zu einer belanglosen Floskel, die Hof-Philosophen mit diversen belanglosen Bedeutungen füllen dürfen, solange im Ergebnis Computerprogramme patentierbar werden.")

(blockquote
(ML Wag "Was verbleibt dann als %(q:Computerprogramm als solches)?") 
(ML IuI "In der neueren Rechtsprechung und im Schrifttum werden zwei Interpretationen vorgeschlagen.")  
(filters 
 ((ml ant (semicolons (ahs 'grur-mellu98 (ML MGW "Melullis, GRUR 1998, 843 (852)")) (ML E6u "EPA Amtsblatt 1999, 609 (619) - IBM Computerprogrammprodukt 1.")))
  (wt ant "Tauchert, GRUR 1997, 149 (154) und Mitt. 1999,248.")
  )
 (ML CeW "Computerprogramme als solche werden entweder als die abstrakte, %(ml:gedanklich-logische Konzeption eines Computerprogrammes angesehen) oder als der %(wt:urheberrechtlich geschützte Sourcecode bzw. Objektcode).")) 
(ML BgW "Bei dem ersten Ansatz ist ein patentfähiges Computerprogramm die praktische Anwendung der gedanklich-logischen Konzeption, bei dem zweiten Ansatz die Funktionalität.")  
(ML Dee "Diese beiden entgegengesetzten Ansätze fuhren jedoch zu dem übereinstimmenden Ergebnis, dass praktisch alle Computerprogramme als technisch anzusehen und patentfähig sind.") )

(ML Hrh "Hieraus folgt, dass Computerprogramme und programmgestützte Geschäftsmethoden patentierbar sind:")

(blockquote 
(ML Ius "In Rechtsprechung und Literatur setzt sich mehr und mehr die Auffassung durch, einen Computer als eine Maschine und daher als technisch anzusehen.") 
(ML DmW "Die für das Funktionieren des Computers unerlässlichen Programme müssen dann auch technisch sein.")
(ML Drr "Die Prüfung, ob eine konkrete Software-Erfindung schutzfähig ist, verlagert sich daher von der Frage der Technizität zu der Frage der erfinderischen Tätigkeit, d.h. der Frage, ob das Computerprogramm dem Fachmann durch den Stand der Technik nahegelegt ist.")
(ML Dee2 "Der für die Beurteilung der Patentfähigkeit einer Software-Erfindung heranzuziehende Fachmann ist ein Programmentwickler.")
(ML Eee "Es ist daher zu prüfen, ob einem Programmentwickler oder Programmierer eine Software-Erfindung durch den öffentlich zugänglichen Stand der Technik nahegelegt ist.")
(ML Dsb "Dieser Argumentation folgend hat das Bundespatentgericht in einer jüngeren Entscheidung? einen Patentanspruch als nicht auf erfinderischer Tätigkeit beruhend zurückgewiesen, da dieser eine Software beschrieben hat, die lediglich die Umsetzung einer bekannten Geschäftsmethode in ein Computerprogramm darstellt.")
(ML Nit "Nur wenn neue und nicht nahegelegte programmtechnische Mittel (beispielsweise Berechnungs-, Speicher- oder Codierverfahren) beschrieben und im Patentanspruch beansprucht werden, wird das Computerprogramm dadurch patentfähig.")
)

(ML Mgn "Man bemerke, dass Geschäftsmethoden auch dann patentierbar sind, wenn es %(q:lediglich die Umsetzung einer neuen und erfinderischen Geschäftsmethode in ein Computerprogramm) geht.  Das entspricht im Inhalt ebenso wie im verschämt-nebulösen Duktus auch der derzeitigen Theorie und Praxis des EPA.  Im folgenden wird dies noch einmal weniger verschämt erklärt und der noch verbleibende Unterschied zu den USA hiermit auf einen fast belanglosen Bereich eingegrenzt.  Auch dies entspricht der Wirklichkeit.")

(blockquote
(ML Inm "In den USA haben Patente auf Geschäftsmethoden seit einer grundlegenden Entscheidung des US-amerikanischen Beschwerdegerichts für Patentsachen (CAFC) eine hohe Popularität erlangt. Die Zahl auf derartige Gegenstände gerichteter Patentanmeldungen ist schlagartig angestiegen. Zwei Patentverletzungsprozesse sorgen für besonderes Aufsehen: Im ersten Verfahren zwischen Patentinhaberin priceline.com und Microsoft geht es um die Verletzung eines Patentes, das das computergestützte Verfahren einer sogenannten umgekehrten Auktion schützt, bei der ein Kunde für ein bestimmtes Produkt (beispielsweise Flugtickets) einen bestimmten Kaufpreis eingibt, den er zu zahlen bereit ist. Eine Mehrzahl von Anbietern (beispielsweise Fluggesellschaften) prüft die Gebote und verkauft einem Kunden das Produkt, wenn der gebotene Preis dem Anbieter akzeptabel erscheint.") 
(ML Inn "Im zweiten Rechtsstreit geht es um die Verletzung eines Patentes von Amazon.com durch barnesandnoble.com, das den Kauf von Produkten/Dienstleistungen an einen identifizierten Käufer mittels eines einzigen Mausklicks erlaubt.")
(ML Deh "Der Ausgang dieser Verfahren, in denen auch über die Gültigkeit der jeweiligen Patente entschieden wird, kann mit Spannung erwartet werden.")
(ML FlC "Fraglich ist, wie derartige Erfindungen von den Gerichten in Deutschland und Europa beurteilt werden. Geschäftsmethoden sind ebenso wie Computerprogramme %(q:als solche) von der Patentierung ausgeschlossen.")
(ML Nrn "Nach der oben geschilderten Sichtweise ist die reine Umsetzung eines bekannten Geschäftsverfahrens in ein Computerprogramm (beispielsweise Handel von Wertpapieren per Computer statt auf dem Parkett) oder die Wahl eines neuen Vertriebsweges (beispielsweise Verkauf von Büchern über das Internet) keine patentfähige Erfindung, da der Programmierer nur bekannte Methoden einsetzen muss, um das bekannte Geschäftsverfahren in Computercode mit den gewünschten Funktionen umzusetzen.")
(ML Eit "Eine völlig neue Geschäftsidee, die bisher weder als Computerprogramm noch auf andere Weise existiert, ist aber unabhängig von erfinderischen programmtechnischen Mitteln patentfähig, da die nicht bekannte (und auch in ähnlicher Form nicht bekannte) Geschäftsmethode dem Programmierer als dem zur Beurteilung der erfinderischen Tätigkeit heranzuziehenden Fachmann nicht nahegelegt sein kann.")
(ML Aae "Aber auch in Europa gibt es bereits erteilte Patente auf computergestützte Geschäftsverfahren, beispielsweise ein Patent der Citibank,?? das ein Verfahren zum Optionsscheinhandel per Computer schützt, und gegen das eine Vielzahl von Banken Einspruch erhoben haben.")
)

(ML HrW "Hochinteressant sind die Ausführungen zur Durchsetzbarkeit von SWPAT-Ansprüchen.  Die Autoren erklären, warum Softwarepatente zur Bildung von Kartellen sehr zweckdienlich sind, und warum das gesetzmäßige Rechtsverständnis bis 1997 die Erreichung dieses Zweckes erschwerte.")

(blockquote
(ML Dhe "Das Patentgesetz gibt dem Patentinhaber das exklusive Recht an der Benutzung der patentierten Erfindung.") 
(ML Dda "Der Patentinhaber kann es Dritten verbieten, die Erfindung gewerblich zu nutzen, oder er kann für die Nutzung Lizenzgebühren verlangen.") 
(ML Eal "Eine Patentverwertung kann somit entweder durch Ausnutzung der auf 20 Jahre nach dem Anmeldetag zeitlich begrenzten Monopolstellung oder durch Lizenzierung der Erfindung an andere erfolgen.") 
(ML Iwd "In komplexen Technologiefeldern, bei denen die Durchsetzung eines %(q:Standards) häufig Voraussetzung für einen Markterfolg beim Konsumenten ist, wie beispielsweise in der Unterhaltungs-elektronik, der Telekommunikation oder dem Internet, sind Kreuzlizenzen eine häufige und praktikable Form der Patentverwertung geworden: Mit eigenen Patenten %(q:als Währung) erwirbt man Zugang zu Technologien, die von Mitbewerbern patentgeschützt sind.")
(ML Eez "Eine erfolgreiche Patentverwertung ist jedoch nur dann möglich, wenn der Patentinhaber sein Recht im Streitfall gegen einen möglichen Verletzer durchsetzen und Unterlassungs- und Schadensersatzansprüche erfolgreich einklagen kann.")
(ML Ded "Der vom Patentschutz umfaßte Gegenstand wird durch die Patentansprüche definiert, für deren Formulierung bei der Abfassung eines Patentes durch den Patentanwalt daher besondere Sorgfalt aufgewandt werden muß.")
(ML Eei "Ein Dritter verletzt das Patent, wenn er alle Merkmale eines Patentanspruches benutzt.")
(ML Nle "Nach §9 PatG gibt es grundsätzlich zwei Arten von schutzfähigen Gegenständen, Erzeugnisse und Verfahren. Zu den Erzeugnissen zählen insbesondere Vorrichtungen (Maschinen, Apparate, Produkte) und Stoffe wie beispielsweise chemische Verbindungen. Die Verfahren umfassen Arbeitsverfahren, Herstellungsverfahren und Verwendungen. In welche Kategorie lässt sich Computersoftware einordnen?")
(ML Eas "Einerseits in Verbindung mit der Hardware als Computersystem, das heißt als Vorrichtung, und andererseits als Verfahren zur Ausführung auf einem Computer, das eine bestimmte Funktion erfüllt.") 
(ML Brn "Beide Patentkategorien haben für den Inhaber eines Softwarepatentes gravierende Nachteile. Das Computersystem-Patent schützt die Software nur in Verbindung mit der Hardware.") 
(ML Eni "Ein Hersteller, der die Software unabhängig vom Computer vertreibt, kann in der Regel nicht belangt werden und begeht dann allenfalls eine mittelbare Patentverletzung, die wesentlich schwieriger zu verfolgen ist.")
(ML Dnc "Das Verfahrenspatent hat den Nachteil, daß nur die Anwendung des Verfahrens und, wenn diese widerrechtlich ist, das Anbieten zur Anwendung rechtswidrig ist.")
(ML Wpm "Wenn der Käufer des Computerprogrammes jedoch ein Endverbraucher ist, dessen private, nicht gewerbliche Nutzung des als Verfahren patentgeschützten Programmes zulässig ist, kann gegen den Anbieter dann auch nicht vorgegangen werden.")
)

(ML Dhn "Doch 1997 hat das EPA den Durchbruch geschafft, indem es die gesetzliche Grenze von der Materie zur Information überschritt:")

(blockquote
(ML DrW "Die für Softwarepatente zuständige Beschwerdekammer 3.5.1 des Europäischen Patentamtes hat in zwei neueren Entscheidungen16 den Weg frei gemacht für einen besseren Schutz von Computerprogrammen.") 
(ML GWe "Gemäß diesen Entscheidungen ist ein Computerprogramm selbst als Produkt schützbar.") 
(ML Eli "Es sind Ansprüche auf ein Computerprogramm bzw. Computerprogrammprodukt sowohl auf einem Speichermedium als auch unabhängig davon zulässig.")
(ML Dme "Dabei unterstreicht die aus der amerikanischen Praxis stammende Formulierung %(q:Computerprogrammprodukt) die Tatsache, daß das Computerprogramm selbst als Erzeugnis, nicht als Verfahren geschützt ist.")
(ML Onr "Ähnlich wie bei einer chemischen Verbindung, die ihre technische Wirkung erst bei einer Anwendung in einem chemischen Verfahren entfaltet, enthält das Computerprogramm eine potentielle technische Wirkung, die sich erst beim Ablaufen auf einem Computer oder einem vernetzten Computersystem entfaltet.")
(ML Dli "Der Patentinhaber hat also den vollen Schutz eines Erzeugnispatentes.")
)

(ML Mae "Mit diesem interessanten Vergleich weisen die Autoren auf die Widersprüchlichkeit von Softwarepatenten hin.  Ein chemisches Erzeugnispatent kann niemals die Weitergabe einer chemischen Formel oder einer Prozessanweisung unterbinden.  Es kann nur bei der Umsetzung dieser Anweisung in ein materielles Erzeugnis ansetzen.  Ein Softwarepatent hingegen unterbindet verbietet die Weitergabe von Information.")

(ML GdW "Genau hier liegt die Grenzüberschreitung, die Softwarepatente zu einer Bedrohung für Freiheit, Wettbewerb und Innovation macht.")
(ML GtW "Gegen diese Werte muss das Interesse des Mandanten auch im Internet durchgesetzt werden:")

(blockquote
(ML Drs "Das im Handel erworbene, auf CD gespeicherte Computerprogramm wird patentrechtlich nicht anders behandelt als beispielsweise ein Fernsehgerät oder ein Mobiltelefon.")
(ML Det "Die von einem Speichermedium unabhängigen Computerprogrammansprüche sind insbesondere dann nützlich, wenn der Patentinhaber gegen einen Vertrieb seines patentgeschützten Programmes durch »Downloaden« über das Internet vorgehen will.") 
(ML Gsd "Gerade in diesen Fällen tritt jedoch ein weiteres Problem auf, da im Patentrecht das Territo-rialitätsprinzip gilt, das heißt der Patentschutz ist territorial auf diejenigen Länder beschränkt, in denen der Erfinder oder sein Rechtsnachfolger ein Patent angemeldet und nach Erteilung weiter verfolgt hat.")
(ML Nno "Nationale Barrieren sind beim E-Commerce jedoch leicht überwindbar.")
(ML Dmr "Der Patentverletzer könnte ein patentgeschütztes Programm von einem im patentfreien Ausland befindlichen Server aus über das Internet weltweit zum Downloaden anbieten.") 
(ML Wze "Wenn die Software jedoch als Erzeugnisanspruch geschützt ist, stellt nach §9 Nr. 1 PatG schon das Anbieten der patentgeschützten Software eine Patentverletzung dar.")
(ML Ned "Nach geltender Rechtsprechung ist bei Angeboten im Internet für den Ort der deliktischen Handlung nicht auf den Ort abzustellen, an dem die reale Einrichtung der Homepage erfolgt oder an dem der Server steht, sondern als Begehungsort kommt grundsätzlich jeder Ort in Betracht, an dem die Homepage bestimmungsgemäß abgerufen werden kann und einen Rechtsverstoß bewirkt.") 
(ML DmV "Das Anbieten einer in Deutschland patentgeschützten Software über eine Internet-Seite, die (auch) an deutsche Verbraucher gerichtet ist, stellt unabhängig von dem Ort des Servers (an dem die Homepage installiert ist), eine Patentverletzung dar. Falls der Patentverletzer in Deutschland oder einem anderen vom Patentschutz erfaßten Land keine Niederlassung hat, ist eine Verfolgung der Patentverletzung trotzdem schwierig, da Vernichtungs-ansprüche oder eine Zollbeschlagnahme bei über das Internet vertriebenen Softwareprogrammen natürlich ins Leere laufen.")
(ML Ehn "Es wird jedoch verhindert, daß ein im Inland ansässiger Anbieter den Patentschutz einfach dadurch unterlaufen kann, dass er die geschützte Software von einem im patentfreien Ausland angesiedelten Server über das Internet vertreibt.")
(ML DWe "Der Patentanmelder wiederum muß darauf achten, daß er Patentanmeldungen in den Ländern tätigt, in denen die Hauptkonkurrenten Sitz oder Niederlassung besitzen.")
(ML Aud "Aufgrund der neueren Entwicklung der Rechtsprechung stellt somit das Internet auch in bezug auf das Patentrecht keinen rechtsfreien Raum mehr dar.")
)

(ML MwW "Mithilfe von Patenten wird sich sicherlich auch das Monstrum Internet zähmen lassen, das bekanntlich ein Hort des Anarchismus ist, in dem eine unheimliche %(q:Opensource-Bewegung) allen Regeln der industriellen Verwertung spottet.  Der Widerstand gegen Softwarepatente kommt natürlich nur aus dieser Ecke.  Die Autoren meinen irrtümlicherweise, sie hätten ein Recht, ihre selbst erarbeiteten Informationswerke anderen %(q:zum kostenlosen Herunterladen anzubieten).  Mit diesem %(q:rechtsfreien Raum) ist jetzt Schluss:")

(blockquote
(ML Dne "Die zunehmende Stärkung des Patentrechts für Erfindungen im Softwarebereich löst in Kreisen der Open Source-Gemeinde eher ablehnende Reaktionen aus.")
(ML Dem "Die Programmierer fürchten um ihre Programmierfreiheit und die Vertriebs- und Servicefirmen für Open Source-Software um ihr Geschäft.")

(ML DkB "Das Patentrecht, das dem Erfinder unzweifelhaft eine starke Rechtsstellung verleiht, enthält jedoch auch wirksame Bestimmungen zur Begrenzung der Rechte des Patentinhabers.")
(ML ZnW "Zum einen hat derjenige, der die Erfindung schon vor der Patentanmeldung benutzt, aber nicht um ein eigenes Patent nachgesucht hat, nach §12 PatG ein Vorbenutzungs- bzw. Weiterbenutzungsrecht an seiner Erfindung.") 
(ML Dae "Der später anmeldende Patentinhaber kann sein Patent also gegen den früheren Ersterfinder nicht geltend machen.") 
(ML Aig "Außerdem erstreckt sich nach $ 11 PatG die Wirkung des Patents nicht auf Handlungen, die im privaten Bereich zu nicht gewerblichen Zwecken vorgenommen werden, sowie Handlungen zu Versuchszwecken, die sich auf den Gegenstand der patentierten Erfindung beziehen.") 
(ML DrW2 "Der Programmierer, der zu Hause und ohne kommerzielle Absicht ein Programm schreibt, braucht auf eventuellen Patentschutz nicht zu achten.")
(ML Dvs "Dies ändert sich jedoch, wenn die Privatsphäre verlassen wird, ein Programm also etwa kostenlos im Internet zum Herunterladen angeboten wird.")
)

(ML Ddh "Die Enteignung der Autoren ist allerdings nicht total.  Es bleiben für ihnen ein paar mögliche Schlupflöcher:")

(blockquote
(ML DWd "Das wirksamste Mittel, einen Patentschutz zu verhindern, ist die Veröffentlichung der Erfindung, beispielsweise im Internet.")
(ML Gts "Genauso wie das Patent einen absoluten Schutz gegenüber Nachahmung bietet, ist eine Vorveröffentlichung ein absoluter Schutz vor Patentierung, mit der Ausnahme, dass in einigen Ländern wie den USA und Japan der Erfinder eine Frist von 12 Monaten (6 Monaten) nach der Erstveröffentlichung der Erfindung hat, um eine Patentanmeldung einzureichen.")
(ML Dce "Der Programmierer hat daher die Möglichkeit, selbst zu entscheiden, ob er ein Patent anmelden möchte oder nicht.")
(ML WWW "Wenn er den Sourcecode durch Zugänglichmachen über das Internet veröffentlicht, ist in dem Moment eine Patentierung von in dem Programm enthaltenen Erfindungen für Dritte nicht mehr möglich.")
)

(ML Dof "Dies stimmt natürlich nur in der grauen Theorie.  In der Praxis wird ein Programmierer kaum nachweisen können, dass er zu einem bestimmten Datum die %(q:Erfindung) bereits veröffentlicht hatte.  Es könnte z.B. passieren, dass")
(ol
(ML Ein2 "Eine zeitgestempelte Aufzeichnung fehlt: das Netz kennt ja keine Zeitdimension.")
(ML Dsm "Die Erfindung war zum erforderlichen Zeitpunkt ungenügend dokumentiert und daher der kann der Patentinhaber glaubhaft machen, dass sie nicht zum Stand der für den durchschnittlichen Fachmann zugänglichen Technik gehörte.  In diesem Falle bekäme der Programmierer vielleicht nur noch eine Privatlizenz auf sein Werk, welche ihm die Verbreitung als freie Software unmöglich machen würde.")
(ML DrA "Der Opensource-Programmierer möchte sich keine teuren Anwälte leisten und gibt auf.")
) 

(blockquote
(ML WPP "Wie im zweiten Abschnitt erläutert wurde, erfaßt das Patentrecht nicht den Code selbst, sondern die einem Programm zugrundeliegende erfinderische Funktionalität.")
(ML Ego "Es ist also nicht so, daß ein Programmierer bei jeder Routine, die er schreibt, fürchten muss, irgendein bestehendes Patent zu verletzen. Erst wenn das Programm eine bestimmte patentierte Funktion auf die im Patentanspruch beschriebene Art und Weise erfüllt, liegt eine Patentverletzung vor.") 
(ML Url "Umgehungserfindungen, d.h. die Lösung des gleichen Problems mit anderen als den im Patent beschriebenen Mitteln, stellen keine Patentverletzung dar, sondern sind häufig sogar eine Quelle neuer Innovationen.")
)

(ML Atv "Auch diese Möglichkeit ist in der Praxis häufig versperrt, denn")
(ol
(ML moW "mathematische Probleme haben oft nur eine Lösung")
(ML Dpr "Die nachzuprogrammierenden Verfahren sind oft Standards wie z.B. TrueType, MP3, ASF, die nicht aufgrund ihrer technischen Vorzüge sondern aufgrund ihrer Wirkung als Marktzutrittsbarrieren gemeistert werden müssen.")
)

(ML SWJ "Schließlich bringen Softwarepatente noch ein paar kleine Probleme mit sich, die, wie schon vor 10 Jahren, selbstverständlich bald gelöst sein werden.")

(blockquote
(ML Eai "Ein Problem stellt die schwierige Recherchierbarkeit von Softwareerfindungen dar, da in den Datenbanken der Patentämter in diesem vergleichsweise jungen Gebiet noch nicht sehr viel leicht recherchierbare, klassifizierte Patentliteratur vorhanden ist.")
(ML Dkg "Dies hat zur Erteilung von zu breiten Patenten auf dem Softwaresektor geführt und wird sich aber spätestens dann ändern, wenn ein größerer Fundus an klassifizierter Patentlite-ratur vorhanden ist.") 
(ML Ogi "Ähnliche Probleme hat es vor ca. 20 Jahren auf dem damals jungen Gebiet der Biotech-nologie gegeben. Mittlerweile zählen Biotechnologie-Erfindungen zu den am besten recherchierbaren. ")
(ML Dng "Darüber hinaus gibt es in Deutschland und Europa anders als etwa in den USA die Möglichkeit des Einspruchs, in dem jedermann eine Überprüfung der Patentfähigkeit eines bereits erteilten Patentes beantragen kann.")
)

(ML Dbe "Die Möglichkeit des Einspruchs wird derzeit in DE/EU bei Softwarepatenten kaum genutzt.  Sie erfordert einen Aufwand, der sich für Privatpersonen selten lohnt.")

(blockquote
(ML DWd2 "Die Rechercheproblematik illustriert den eigentlichen Zweck des Patentrechtes, nämlich die Bereitstellung der Erfindungen und Innovationen für die Allgemeinheit.")
(ML Ues "Um dem Erfinder einen Anreiz zu geben, seine Erfindung nicht geheimzuhalten, sondern der Allgemeinheit preiszugeben, wird ihm als Gegenleistung ein auf 20 Jahre begrenztes Monopolrecht gewährt.")
(ML Dng2 "Der Zweck, dem das Patentrecht dient, und der, den die Open Source-Gemeinschaft verfolgt, ist somit prinzipiell der gleiche, nämlich die weltweite Verbreitung von Innovation.")
(ML Dfr "Die Open Source-Bewegung versucht dieses Ziel auf freiwilliger Basis zu erreichen, während im Patentrecht der Erfinder Anspruch auf eine Belohnung hat.")
)

(ML Hri "Hier fördern die Autoren einen populären Irrtum.  Es geht weder bei quellenoffener Programmierung noch bei Softwarepatenten im wesentlichen um die Bekanntmachung erfinderischer Verfahren.")
(ahs 'mit-seqinnov (ML DWe2 "Die Innovation im Softwarebereich ist meistens sequentiell.  D.h. es werden über lange Zeit viele kleine Schritte akkumuliert.  Die Hauptleistung besteht in einer zuverlässigen Implementation, die besser durch das Urheberrecht geschützt wird."))

(ML Ene "Es folgen weitere, in Patentkreisen beliebte Scheinargumente:")

(blockquote (ML Nre "Nach Überwindung der Rechercheprobleme (auch unter Einsatz umfangreicher Datenbanken und moderner Suchsoftware) und bei sachgerechter Beurteilung des Patentierungskriteriums %(q:erfinderische Tätigkeit) sollte das Patentrecht auf dem Gebiet der Software, wie in anderen Technologiefeldern auch, zu einem gerechten Interessenausgleich zugunsten der Förderung von Innovation führen."))

(ML Eiu "Einer sachgerechten Beurteilung der Erfindungshöhe stehen diverse unüberwindbare Hindernisse im Weg.  Das Beurteilungsverfahren ist formalisiert und darauf ausgelegt, einzelne %(q:Erfindungsschritte) (d.h. sequentielle Innovation, der Patente nicht gerecht werden) zu patentieren")

(blockquote
(ML Nin "Neben den Interessen der Entwickler und Vermarkter freier Software sind auch die Interessen derjenigen Unternehmen, insbesondere kleinerer und mittlerer Unternehmen, zu berücksichtigen, die auf eine kommerzielle Verwertung der unter hohem Entwicklungsaufwand erstellten Software angewiesen sind.")
)

(ML Gee "Genau diese Unternehmen werden durch Softwarepatente enteignet und vom Markt gefegt.")

(blockquote
(ML HiW "Hätte beispielsweise Netscape einen wirksamen Patentschutz für seinen innovativen Browser gehabt, hätte es sich wohl nicht wehrlos dem Marketinginstrument kostenloser Software des marktstärkeren Wettbewerbers Microsoft beugen müssen.")
)

(ML Nnd "Netscape wäre in diesem Falle über noch zahlreichere Microsoft-Patente gestolpert.  Patente wirken nicht einzeln sondern im Zusammenhang von Patent-Portfolios.  Emporkömmlinge haben es aus Gründen schwer, die auch die Autoren oben bereits genannt haben:  Verwehrung des Marktzutritts durch patentierte De-Facto-Standards.")
(ML ZtI "Zudem liegt die Hauptarbeit der Netscape-Programmierung in der Ausführung, nicht in den Programmierideen.  Microsoft brauchte Jahre, um Netscape einzuholen.  In der heutigen Geschäftswelt reicht ein solcher Vorsprung als Innovationsanreiz.")

(ML Ijc "Im folgenden dokumentieren die Autoren sehr freimütig, warum und wie die Patentjustiz die Gesetzesregeln Schritt für Schritt %(q:auf allen Kanälen) aushebelten.")

(blockquote
(ML DtW "Der Ausschluß von %(q:Computerprogrammen als solchen) vom Patentschutz in Art. 52 EPÜ (§1 PatG) wird seit langem als rechtspolitische Fehlentwicklung angesehen, zumal der Ausschluß von breiten Verkehrskreisen - bis heute1° - mißverstanden und meist als Ausschluß von Computerprogrammen allgemein verstanden wird.")
(ML Flc "Für die europäische Diskussion um den Patentschutz von Computerprogrammen kam die entscheidende Wende durch das am 15.12.1993 abgeschlossene GATT/TRIPS-Abkommen, da durch dieses ein internationaler Standard gesetzt wurde. In diesem Abkommen ist in Art. 27 ausdrücklich vorgesehen, daß der Patentschutz auf allen technischen Gebieten (also auch auf dem Gebiet der Computer-Software) verfügbar sein muß.")
(ML UWi "Um die europäischen Patentgesetze mit denen in den USA und Japan und mit Art. 27 TRIPS in Einklang zu bringen, wurde daher schon bald die Streichung der Computerprogramme aus Art. 52 EPÜ (§1 PatG) gefordert, zumal diese Regelung die europäische Software-Industrie von der Anmeldung von Softwareerfindungen abschreckte und damit zum Schaden der europäischen Software-Industrie gereichte, während die amerikanische und japanische Industrie fleißig Softwarepatente anmeldete und von den inzwischen über 13.000 erteilten europäischen Software-Patenten über 75% besitzen dürfte.")
(ML Wri "Weiteren Auftrieb erhielt die europäische Diskussion durch die Resolution der AIPPI zur Frage Q133 vom 22.4.1997 in Wien, die in dem Satz zusammengefaßt werden kann: %(q:Alle auf einem Computer ablauffähigen Programme sind technischer Natur und daher patentfähig, wenn sie neu und erfinderisch sind), sowie den Round Table der UNION am 9./10.12.1997, als im  Europäischen Patentamt 100 Fachleute aus zwanzig europäischen Ländern über die Zukunft des Patentschutzes von Software in Europa diskutierten und zu einem ähnlichen Ergebnis kamen wie die AIPPI. Zudem wurde darauf hingewiesen, daß das Konzept des EPA zum %(q:technischen Charakter) weder von den Patentanmeldern noch von den nationalen Patentämtern richtig verstanden würde. Viele Teilnehmer machten klar, daß eigentlich alle Computerprogramme dem Wesen nach %(q:technischen Charakter) aufweisen würden.")
(ML SAo "Seit dieser Zeit wird praktisch %(q:auf allen Kanälen) daran gearbeitet, einen Weg zu finden, wie der irreführende Ausschluß von %(q:Computerprogrammen als solchen) aus den europäischen Patentgesetzen entfernt werden kann, wobei konsequenterweise auch die anderen Ausnahmeregelungen in Art. 52 Abs. 2 EPÜ (§1 Abs. 2 PatG) zur Disposition stehen.")
(ML Ift "Im November 1998 hat das Europäische Parlament2s die Patentfähigkeit von Computerprogrammen befürwortet, sofern das betreffende Produkt die Anforderungen an eine technische Erfindung im Hinblick auf Neuheit und Anwendbarkeit erfüllt, wie dies bei den Handelspartnern in den USA und Japan der Fall ist. Im Follow-Up-Papier zum Grünbuch über das Gemeinschaftspatent hat die Europäische Kommission festgestellt, daß rund 75% der ca. 13 .OOO europäischen Software-Patente im Besitz außereuropäischer Großunternehmen sind und die kleinen und mittleren Unternehmen (und deren Berater!) %(q:schlichtweg nicht wissen, daß es möglich ist, für Software-Erfindungen Patentschutz zu erlangen).")
(ML DWW "Die Kommission sieht daher Handlungsbedarf und plant sowohl eine Informationskampagne als auch eine entsprechende Regelung durch eine Richtlinie zur Harmonisierung der nationalen Patentgesetze bezüglich des Patentschutzes von Computerprogrammen - ein erster Entwurf wird für Anfang 2000 erwartet -, durch die der bisherige Art. 52 EPÜ durch Art. 27 TRIPS ersetzt werden dürfte, d.h. %(q:Computerprogramme als solche) und die anderen Ausschl&atbestände von Art. 52 Abs. 2 EPÜ nicht mehr erwähnt werden.")

(ML N1W "Nachdem nun auch die Regierungskonferenz der Mitgliedstaaten der Europäischen Patentorganisation im Juni 1999 in Paris dem EPA das Mandat erteilt hat, vor dem 1.1.2001 eine revidierte Fassung von Art. 52 Abs. 2 EPÜ bezüglich des Ausschlusses von Computerprogrammen vorzulegen, so daß die geänderte Fassung vor dem 1.7.2002 in Kraft tritt, ist es wohl nur eine Frage der Zeit, bis die Computerprogramme (und wohl auch die anderen Ausschlussregelungen) aus Art. 52 EPÜ gestrichen sind.")
(ML Dou "Daneben bemüht sich die Rechtsprechung - wie ausgeführt -, die derzeitige Gesetzesregelung so eng auszulegen, daß praktisch alle Computerprogramme - bei entsprechender Anspruchsformulierung - technischen Charakter besitzen und patentfähig sind, wenn sie neu und erfinderisch sind.")
(ML Dar "Da die Prüfung einer Patentanmeldung mindestens zwei bis drei Jahre dauert, zwingt diese Entwicklung bereits heute alle Berater dazu, ihre Mandanten darauf hinzuweisen, daß grundsätzlich alle Computerprogramme patentfähig sind und der Patentschutz - trotz der irreführenden gesetzlichen Regelung - nicht nur für Computerprogramme, sondern alle Innovationen im Internet genutzt werden kann.")
)

(ML Diz "Vielen Dank für die Aufklärung!")
)

(mlhtdoc 'cr-bmueller00 nil nil nil (papritab)

(dl
(l (ML Ato "Author") (commas (ml "Bernhard Müller") (ml "Brüssel" "Bruxelles")))
(l (ML Agr "Anmerkung des Herausgebers") (ML DsW "Der Beitrag gibt seine persönliche Auffassung wieder." "The article represents his personal view."))
)

(smaller-font (lin
(ML Dnw "Die Rechtslage zur Patentierbarkeit von Computerprogrammen in Europa ist schwer nachvollziehbar und uneinheitlich." "The legal situation concerning patentability of computer programs in Europe is hard to comprehend and inhomogeneous.")
(ML AiU "Ausgehend von der Notwendigkeit einer europäischen Gesetzesinitiative vergleicht der Beitrag die Rechtslage in Europa und den USA und zeigt die künftigen Ansatzpunkte der EG-Richtlinie auf." "Starting out from the necessity of a European Law Initiative this article compares the legal situation in Europe to that of the USA and shows the future approaches of the EC guideline.")
))

(sects
(notw (ML NWe "Notwendigkeit einer EU-Gesetzesinitiative" "Necessity of a EU Law Initiative")

(ML DeW "Die Dienststellen der Europäischen Kommission bereiten derzeit den Vorschlag einer Richtlinie über die Patentierbarkeit von Computerprogrammen vor." "The public service organs of the European Commission are currently preparing a proposal for a directive on patentability of computer programs.")
(filters (
 (gb ah "http://www.europa.eu.int/comm/dg15/en/intprop/indprop/558.htm")
 (fm ah "http://www.europa.eu.int/comm/dg15/en/intprop/indprop/99.htm")
 ) (ML Grr "Gesetzgeberisches Handeln in diesem Bereich war in der mit dem %(gb:Grünbuch über das Gemeinschaftspatent und das Patentschutzsystem in Europa) eingeleiteten Konsultation %(fm:als vorrangig eingestuft worden)." "Legislative Action in this area was %(fm:rated a high priority) by the consultations introduced by the %(gb:Greenbook on the Community Patent and the Patent Protection System in Europe)."))
(ML Nnf "Nach der derzeitigen Regelung ist ein Programm %(q:als solches) nicht patentfähig, ein Patent kann jedoch für eine technische Erfindung, die sich auf ein Computerprogramm stützt, erteilt werden." "According to the current legal rule, a program %(q:as such) is not patentable, but a patent can be granted for a technical invention that is based on a computer program.")
(ML DMa "Diese Regelung weist einen Mangel an Transparenz und damit an Rechtssicherheit auf." "This rule lacks transparency and thereby legal security.")
(ML AcW "Außerdem sind Entscheidungspraxis nationaler Gerichte und des Europäischen Patentamtes nicht immer einheitlich, was dem Binnenmarkt abträglich ist." "Moreover the decision practise of national lawcourts and that of the European Patent Office are not always homogeneous; this harms the interior market.")
(ML EWe "Eine auf §95 EG-Vertrag gestützte Richtlinie zur Harmonisierung der nationalen Patentgesetze in diesem Bereich soll Abhilfe schaffen." "A Directive for harmonsiation of national patent laws based on EC Treaty §95 is to provide a solution to this problem.")
(ML Dmu "Das Europäische Parlament sprach sich für die Patentfähigkeit von Computerprogrammen aus, welche die an eine technische Erfindung gestellten Anforderungen an Neuheit und Anwendbarkeit erfüllen, so wie dies in den USA und Japan der Fall ist." "The European Parlament opted for the patentability of computer programs, which fulfill the requirements made to a technical invention concerning novelty and usability, as this is the case in the USA and Japan.")
)

(euus (ML Dho "Derzeitige Rechtslage in Europa und den USA" "Current Legal Situation in Europe and the USA")

(tan "..."
(ML LKr "Lange Ausführungen über gerichtliche Prozeduren in den USA." "Lengthy expositions on judicial procedures in the USA and court decisions on which they are based as well as on recent controvesies about business method quality, where the final decision was made against incorporating the quality consideration into the law.  From these unnecessary expositions it is evident, that the author is a fervent admirer of the US system.")
) )

(euans (ML Afi "Ansätze für EG-Richtlinie" "Approaches for the EC Directive")

(lin
(ML Fre "Für den Vorschlag der EG-Richtlinie der Kommission stellt sich die grundsätzliche Frage, ob die harmonisierung auf der Grundlage des status quo, so wie ihn die Rechtsprechung in Europa definiert, erfolgen oder ob sie weiter und dabei insbesondere in Richtung der US-Rechtsprechung, gehen sollte." "For the proposal of the Commission's EC Directive the basic question arises, whether the harmonisation shall be based on the basis of the status quo, as defined by jurisdiction in Europe, or whether it should go further, especially whether it should proceed further into the direction of US jurisdiction.")
(ML Btw "Bei der Klärung dieser Frage sollten insbesondere die Auswirkungen der Richtlinie auf Innovation und Wettbewerb (namentlich für KMU), die Rolle und Interessen unabhängiger Software-Entwickler sowie die Auswirkungen auf den elektronischen Geschäftsverkehr berücksichtigt werden." "In clarifying this question, special consideration should be given to the effects of this directive on innovation and competition (namely for SMEs), the role and interests of independet software developpers as well as the effects on electronic commerce.")
)

(lin
(tan
 (ML Dii "Die Komission hat den beteiligten Kreisen Gelegenheit gegeben, sich zu dem konkreten Inhalt der Richtlinie durch Beantwortung eines Fragebogens zu äußern." "The commission has given the participating circles the chance to express their views concerning the contents of the directive by answering a questionnaire.")
 (ML des "der Artikel enthält keinen weiteren Hinweis zu diesem Fragebogen" "no further pointer to this questionnaire found in the article or footnotes") )
(ML Dut "Dabei wurde im Zusammenhang mit der Frage der Patentierbarkeit von Computerprogrammen eine Definition des Erfindungsbegriffs im Sinne von %(q:technische Lösung) nur von einer Minderheit der Antwortenden befürwortet." "In this questionnaire, in connection with the question of patentability of computer programs, a definiton of the notion of %(q:invention) as %(q:technical solution) was only favored by a minority of respondents.")
(ML Taw "Teilweise wurde vorgeschlagen, statt dessen auf einen Beitrag zum Stand der Technik abzustellen oder sich auf den Nachweis einer technischen Wirkung der Erfindung zu beschränken." "Some respondents proposed to instead ask whether a contribution to the state of the art was made or to restrict or whether the invention has a technical effect.")
(ML Ema "Einer anderen Stellungnahme zufolge sollte der technische Charakter eines Computerprogramms allgemein anerkannt und die gewerbliche Anwendbarkeit weit ausgelegt werden, so dass das Kriterium eines nützlichen praktischen Ergebnisses maßgebliche Bedeutung erlangen würde." "According to another response,the technical character of a computer program should generally be regarded as given, and the term %(q:industrial applicability) should given a wide interpretation, so that it would become to be understood as the question whether a useful practical result is achieved.")
(ML EpW "Eine weitere Frage ging dahin, ob ein Unterschied zwischen verschiedenen Kategorien von Computerprogrammen gemacht werden sollte, um bestimmte von ihnen von der Patentirbarkeit auszuschließen, insbesondere Computerprogramme für Spiele oder für Tätigkeiten im Geschäfts- oder Finanzbereich." "Furthermore it was asked, whether a difference should be made between different types of computer programs, so as to exclude some of them, especially programs for games or business / finance activities, from patentability")
(ML Due "Diese Frage wurde überwiegend verneint." "This question was answered negatively by the majority of respondents.")
(ML Iez "In einzelnen Beiträgen wurde gefordert, jedes Computerprogramm, welches die allgemeinen Bedingungen der Patentierbarkeit erfüllt, als patentfähig anzusehen, ohne zwischen verschiedenen Arten von Programmen zu unterscheiden." "Some respondents demanded that any computer program which fullfilled the general conditions of patentability should be considered patentable, without discriminating between different kinds of programs.")
(ML Iiw "In einer anderen Stellungnahme hieß es, die technische Natur einer Erfindung hänge nicht von dem Zweck des Computerprogramms ab, das diese beinhalte." "Another position was that the technical nature of an invention does not depend on the purpose of the program that embodies it.")
(ML TaE "Teilweise wurde aber auch in diesem Zusammenhang ausdrücklich ein Beitrag der Erfindung zum Stand der Technik verlangt." "Some respondents however explicitly demanded that the invention should make a contribution to the state of the art.")
)

(lin
(ML Dln "Die Anregungen der beteiligten Kreise sollten bei der Fertigstellung des Richtlinienvorschlags in Betracht gezogen werden." "The opinions of the participating circles should be considered during completion of the directive proposal.")
(ML Dez "Der Vorschlag sollte auf den allgemeinen Grundsätzen des Patentrechts beruhen, so wie diese sich historisch entwickelt haben, und dabei gleichzeitig den Notwendigkeiten der Informationsgesellschaft Rechnung tragen." "The Proposal should be based on the general principles of patent law, as they have historically evolved, and at the same time take account of the necessities of Information Society.")
)
;euans
)
)
)

(mlhtdoc 'grur-mellu98 nil nil nil (papritab)
(ML Bfm "BGH-Richter Mellulis gibt in diesem Artikel seine %(q:persönliche Meinung) wieder, die %(q:sich nicht mit der Meinung des BGH-Patentsenates decken muss).")

(ML Wis "Wie auch andere Artikelschreiber sieht Mellulis, dass das deutsche Patentgesetz ebenso wie das EPÜ die Patentierung von Programmlogik-Erfindungen verbietet, aber andererseits durch Formulierungen wie %(q:Programme als solche) Interpretationsspielraum eröffnet, den es aus wirtschaftspolitischen Erwägungen heraus zu nutzen gilt:")

(blockquote
(ML DsW "Damit ist nicht nur ein akademisches Problem angesprochen.  Die Patentfähigkeit von Software ist für die beteiligten Kreise der Wirtschaft von erheblicher Bedeutung.  EDV und Software nehmen in immer stärkerem Maße auf sämtliche Bereiche des menschlichen Lebens Einfluss, wobei sich das wirtschaftliche Interesse zunehmend auf die Software verlagert.  Haben die Hersteller ihr Augenmerk ursprünglich noch auf die Hardware gerichtet, gilt dies heut in zunehmendem Maße deren Ausstattung mit Programmen: auf diesem Sektor sind - auch im Hinblick darauf, dass auf der Grundlage des vorhandenen Hardwareangebots der bestehende Markt weitestgehend gesättigt ist und keine großen Zuwachsraten erwarten lässt, die wirtschaftliche Bedeutung des Geschäfts mit Software.  Deren Entwicklung wird, wie etwa das Beispiel Microsoft erkennen lässt, das mit seinen Programmen zunehmend auch den Bedarf an Hardware beeinflusst, für die Zukunft des gesamten Marktes aus dem Bereich der EDV von erheblicher Bedeutung sein.  In gleichem Maße ist auch die Bereitschaft gestiegen, in diesen Markt zu investieren.  Investitionen sind aber nur sinnvoll, wenn sie mit einiger Wahrscheinlichkeit künftige Gewinne erwarten lassen.  Dies setzt wiederum voraus, dass die unter Eingehung von wirtschaftlichen Risiken und Inkaufnahme persönlichen und sonstigen Aufwands geschaffenen Produkte eine Refinanzierung dieses Aufwands erwarten lassen, und verlangt damit nach einer Sicherung des geschaffenen Produkts gegenüber Nachahmungen.  Dem entspricht ein dringendes Interesse auf Seiten der Autoren und sonstigen Inhaber der Rechte an der Software nach einem angemessenen Schutz ihrer Erzeugnisse.")
)

(ML Mke "Mellulis beschränkt sich auf das Beispiel Microsoft und auf wirtschaftspolitische Gemeinplätze, die sich nicht einfach von der Ökonomie der Physikalien (körperlichen Waren) auf die Ökonomie der Logikalien (informationellen Gegenstände / Software) übertragen lassen.  Er geht weder auf die Situation typischer europäischer Softwareunternehmen noch auf die Wirklichkeit der Freien Software ein.  Auch seine Aussagen über die Sättigung des Marktes zeugen von einer eng auf das Markttreiben um den Microsoft-PC fixierten, vulgärökonomischen Sichtweise.")

(ML Anz "Anschließend geht Mellulis erwartungsgemäß dazu über, die Unzulänglichkeiten des bisherigen Software-Investitionsschutzes aufzuzeigen und den Patentschutz als Lösung anzubieten:")

(blockquote
(ML Auw "Außerhalb des Patentrechts ist dieser Schutz mit den bisher angebotenen Mitteln nur schwer zu verwirklichen.  Im Anschluss an den Ausschluss der Software vom Patentschutz in Art. 52 EPÜ hat die Kommission der Europäischen Gemeinschaften zunächst versucht, dem Bedarf der beteiligten Kreise dadurch zu entsprechen, dass Computerprogramme dem Schutz des Urheberrechts unterstellt werden.  Im Ergebnis hat sich dieser Weg, wie man heute wohl feststellen darf, als nicht völlig überzeugend erwiesen:  Das wird letztlich durch den Inhalt des Grünbuches bestätigt, das nach Wegen sucht, Software den Schutz des Patentrechts zu eröffnen.")
)

(filters ((gb ahs 'gbkrit)) (ML cWo "Das %(gb:Grünbuch) ist eine Sammlung in der Patentbranche kursierender vulgärökonomischer Gemeinplätze und Vorurteile.  Es geht mit keinem Wort auf die tatsächliche Lage der europäischen Informationstechnologie ein.  Mellulis begründet Gemeinplätze durch Zitieren von Gemeinplätzen.  Allerdings geht er dabei vorsichtig vor.  Er stellt keine eigenen kuehnen Thesen auf und wagt auch nicht die Aussage, dass der bisherige Investitionsschutz (durch Urheberrecht, Betriebsgeheimnis, Vertragsrecht, Humankapital etc) nicht ausreiche.  Er findet ihn lediglich %(q:nicht überzeugend).  Auf dieser systemästhetischen Überlegung beruht letztlich sein Wunsch nach Softwarepatenten."))

(filters ((ip ahs 'swpatlijda)) (ML Inn "Immerhin erkennt auch Mellulis an, dass die Patentierung von Software vom europäischen Gesetzgeber nicht vorgesehen war, und dass ihre nachträgliche Einführung einer ökonomischen Rechtfertigung bedarf.  Er glaubt, diese gefunden zu haben und sieht sich darin offenbar von der bis Brüssel reichenden, niemals hinterfragten Einheitsmeinung des %(ip:Proprietär Informationellen Komplexes) hinreichend gestützt."))

(ML Mad "Mellulis sucht nun nach einem Weg, wie man einerseits Programmiertechniken grundsätzlich patentierbar machen, um dem vermeintlichen Bedarf der %(q:Softwareindustrie) gerecht zu werden, andererseits den in der Ausnahmenliste des BPatG/EPÜ zum Ausdruck kommenden Sorgen des Gesetzgebers Rechnung kann.")

(ML Dse "Dem Gesetzgeber geht es laut Mellulis vor allem darum, dass wichtige bürgerliche Freiheiten im Bereich des Denkens, des Ausdrucks, des gesellschaftlichen Organisierens etc gewährleistet bleiben müssen.  Die Patentierung von Computerprogrammen stellt insoweit eine Gefahr dar, wie sie diese Freiheiten berührt.  Sobald es aber um Technik, auch um Programmiertechniken, geht, ist diese Gefahr laut Mellulis nicht mehr gegeben.")

(blockquote 
 (ML WWP "Weil die Programmidee keine Erfindung darstellt, kann für eine besonders elegante Form der Buchhaltung, ein außergewöhnliches Spiel oder eine sonstige Anwendung ein Patent nicht schon deshalb erteilt werden, weil sie als Programm für eine Datenverarbeitungsanlage verwirklicht werden sollen; insbesondere die erfinderische Tätigkeit lässt sich wegen des Patentierungsverbotes nicht mit der Erwägung begründen, daß die Entwicklung oder Gestaltung dieses Programminhaltes das Können des einschlägigen Durchschnittsfachmanns überstieg.")
 (ML BeW "Bei der gebotenen ganzheitlichen Betrachtung führt nicht schon das Besondere an der dem Programminhalt zugrundeliegenden gedanklichen Leistung zu einem erfinderischen Schritt; für diesen ist der Programminhalt nur in dem Umfang von Bedeutung, in dem er die Anforderungen für die technischen Maßnahmen zu seiner Ausführung bestimmt.")
(ML Dov "Darin liegt keine Rückkehr zu dem von der herrschenden Lehre zu Recht verworfenen Konzept der Kerntheorie.")
(ML Ani "Auch bei dessen Kombination mit anderen Schöpfungen des menschlichen Geistes kann ein Patent nur erteilt werden. wenn das darin enthaltene technische Konzept neu und erfinderisch ist. Auf diese Weise läßt sich daher eine außertechnische Idee oder Lehre nicht monopolisieren. Das Verbot in Abs. 2 läßt die Erteilung eines Patents wegen der einem Programm zugrundeliegenden Spielidee, einer in der Software umgesetzt besonders eleganten Form einer Buchhaltung oder einer besonders ästhetischen Gestaltung nicht zu.")
(ML PdW "Patentfähig kann daher allenfalls eine das Können des Durchschnittsfachmanns übersteigende Umsetzung der zugrunde liegenden Idee in ein Programm sein.")
(ML Bes "Bedarf es für dessen Entwicklung keiner das Können des Durchschnittsfachmanns übersteigenden Leistung, fehlt daher Patentfähigkeit auch dann, wenn die zugrunde liegende außertechnische Idee ungewöhnlich und oberhalb des Kiinnen des Durchschnittsfachmanns angesiedelt ist.")
)

(ML Deh "Der letzte Absatz scheint Organisationsmethoden auszuschließen.  Es gibt allerdings auch unter diesen sehr viele, die %(q:Anforderungen für technischen Maßnahmen zu ihrer Ausführung) enthalten.  Zumindest das europäische Patentamt hat unter ähnlichen Vorzeichen seit 1997 bereits zahlreiche Patente auf Geschäftsmethoden gewährt.")
(ML MGi "Mellulis reinterpretiert nun vorangehende Entscheidungen des BGH und zeigt, dass die neue Lehre in der Praxis häufig zu ähnlichen Ergebnissen wie die alte %(q:Kerntheorie) führen sollte:")

(blockquote
(ML Mue "Mit dieser Maßgabe hat der Bundesgerichtshof daher zu Recht in der Entscheidung %(q:Chinesische Schriftzeichen) Patentschutz für ein Programm versagt, dessen Erfolg sich in Maßnahmen des Sammelns und Ordnens von Daten erschöpft.")
(ML Pnn "Patentschutz verdient nicht das dem Programm zugrunde liegende gedanklich logische Konzept, sondern die technische Konzeption, die seine Ausführung durch die Maschine ermöglicht. Programminhalt und -idee können die Erfindungshöhe nur als Teil eines technischen Konzeptes beeinflussen.")
(ML Deq "Dem ist der BGH auf der Grundlage seiner damaligen Wertung leider nicht mehr nachgegangen. Soweit das heute noch zu erkennen ist, spricht allerdings wenig dafür, daß die Umsetzung der außertechnischen Idee eine erfinderische Tätigkeit voraussetzte.")
(ML Ain "Allein die äußerliche Verbindung mit Hardware genügt dafür nicht; erforderlich wäre gewesen, daß die technische Konzeption zur Umsetzung der bestechenden Idee, die Schriftzeichen in Grundformen zu zerlegen und über eine Datenverwaltung den Zugriff auf sie und mögliche Kombinationen zu eröffnen, das Können des durchschnittlichen Fachmanns auf diesem Sektor überstieg. Dafür spricht nach dem mitgeteilten Sachverhalt wenig. Das Besondere des Programms war die Art und Weise, in der die Schriftzeichen in Grundformen zerlegt und anschließend geordnet wurden und damit die gedankliche Leistung des Ordnens und Sammelns. Die darin anschließende Entwicklung des Programms selbst scheint über eine übliche Datenverarbeitung nicht hinausgegangen zu sein und dürfte sich deshalb aus diesem Grunde nicht als patentfähig erweisen.")
)

(ML Die "Die Patentjuristen liefen seit 1992 bekanntlich mit vereinten Kräften gegen das Urteil %(q:Chinesische Schriftzeichen) Sturm.  Ihr Argument lautete in etwa:  auch in der Erfindung %(q:Chinesische Schriftzeichen) war ein Rechensystem als %(q:technische Anforderung) enthalten.  Ohne ein Rechensystem war diese Erfindung nicht realisierbar.")

(ML MjW "Mellulis meint jedoch, eine neue stabile Theorie gefunden zu haben, mit der die deutsche Patentjustiz ein paar Jahre arbeiten kann.  Er hält diese Theorie sogar für systemästhetisch sehr befriedigend:")

(blockquote
(ML UWa "Unter Verwendung einer üblichen Programmiersprache erstellte Software beschränkt sich in dieser Umsetzung regelmäßig auf die Implementierung und Kombination bekannter Module und Elemente. Eine erfinderische Tätigkeit wird regelmäßig fehlen.")

(ML InW "Insoweit ist daher auch %(e:Engel) beizupflichten, wenn er ein patentfähiges Programm nur bei Vorhandensein solcher Eigenschaften und Funktionen bejaht, die außerhalb der grundlegenden Eigenschaften und Funktionen aller Programme liegen. Das, was allen gemeinsam ist, gehört in der Regel zum bekannten Formenschatz; seiner Übernahme liegt regelmäßig eine erfinderische Tätigkeit nicht zugrunde.")
(ML Dia "Dabei handelt es sich jedoch weniger um eine Frage ihres technischen Charakters, der sich als solcher aus der technischen Ansteuerung des Rechners ergibt.")
(ML Drn "Die Schutzfähigkeit scheitert hier vielmehr in der Regel an der fehlenden erfinderischen Tätigkeit oder - wenn auch nur in Ausnahmefällen - der mangelnden Neuheit, da im Ergebnis nur bekannte Module neu zusammengefaßt wurden.")
(ML SgD "Sind demgegenüber weitergehende Erkenntnisse und Fähigkeiten des Fachmanns erforderlich, wird regelmäßig eine patentfähige Erfindung auch dann vorliegen, wenn sich das Programm nach seinem Inhalt auf der menschlichen Tätigkeit vergleichbare Maßnahmen des Sammelns und Ordnens von Daten beschränkt. Die äußere Übereinstimmung der Tätigkeit von Mensch und Maschine besagt in der Datenverarbeitung für die Patentfähigkeit ebenso wenig wie in anderen.")
(ML Aid "Als Nebeneffekt ermöglicht die Gleichsetzung des Begriffs vom Computerprogramm als solchem mit dessen Inhalt eine widerspruchsfreie Einordnung der Software in das System der gewerblichen Schutzrechte, die deren systematisch Verhältnis entspricht. Das Urheberrecht betrifft in erster Linie diese Programminhalte, wobei zugleich die für technische Sachverhalte ungewöhnliche Schutzdauer zu erklären ist. Soweit der Programminhalt - wie insbesondere bei Spielen, aber nicht nur bei diesen - einem Urheberrechtsschutz zugänglich ist, kann die mit dem Urheberrecht verbundene Schutzdauer hingenommen werden, weil es sich um Regelungen und Gestaltungen handelt, die regelmäßig Ausweichmöglichkeiten offenlassen. Patent- und nicht Urheberrecht unterliegt demgegenüber die technische Konzeption zur Umsetzung dieses Inhalts in eine technische Handlungsanweisung, die sich in für den Rechner verständliche und von ihm abzuarbeitende Anweisungen umsetzen läßt.")
)
)

(mlhtdoc 'daeb-wodarg00 nil nil nil (papritab))

(mlhtdoc 'cr-rauben94 nil nil nil (papritab))
(mlhtdoc 'mdp-smidn99 nil nil nil (papritab))
(mlhtdoc 'mdp-tauch99 nil nil nil (papritab))
(mlhtdoc 'grur-tauch99 nil nil nil (papritab))
(mlhtdoc 'epo-t830006 nil nil nil (papritab))
(mlhtdoc 'epo-t840208 nil nil nil (papritab))
(mlhtdoc 'epo-t850022 nil nil nil (papritab))
(mlhtdoc 'epo-t850115 nil nil nil (papritab))
(mlhtdoc 'epo-t850163 nil nil nil (papritab))
(mlhtdoc 'epo-t860026 nil nil nil (papritab))
(mlhtdoc 'epo-t900110 nil nil nil (papritab))
(mlhtdoc 'epo-t920164 nil nil nil (papritab))
(mlhtdoc 'epo-t920769 nil nil nil (papritab))
(mlhtdoc 'epo-t960410 nil nil nil (papritab))
(mlhtdoc 'epo-t970935 nil nil nil (papritab))
(mlhtdoc 'epo-t971173 nil nil nil (papritab))
(mlhtdoc 'grur-metzj99 nil nil nil (papritab))

(mlhtdoc 'hbr-thurow97 nil nil nil (papritab)

(ah "http://hbspprod.tvisions.com/products/hbr/sepoct97/97510.html" (ML 0 "advertisement and abstract" (de "Anpreisung und Kurzzusammenfassung") (fr "Annonce et Resumé")))

(sects
(quot (ML 0 "Random Quotations" (de "Kostproben") (fr "Citations"))
(blockquote
(ml "But the United States now live in a competitive world in which its economic dominance is long gone. Developing proprietary technologies and the skills that go with them is the only way to defend US workers from the downward pressure of factor price equalization.") 
"..."
(ml "As the government role in R&D fades, the need for stronger private incentives grows.  The standard incentive is to give inventors a monopoly on the right to prduce the products that can be created withtheir knowledge - a right that they can use or sell.  Whether we like it or not, the corollary of fading government efforts is the need for stronger private monopoly rights.") 
"..."
(ml "Without stronger systems of protection, companies will defend their economic positions by keeping their knowledge secret.  Articles about research papers whose publication is deliberately delayed often pop up now in the scientific press.  Secrecy is a much bigger deterrent to the expansion of knowledge than any monopolistic system of protection for intellectual property rights.  An investigator who knows what is known can go to the next step.  One who doesn't wastes time reinventing what is known or wandering in an intellectual wilderness looking for a path that someone else has already found.")
"..."

(ml "There is no single right answer about how to make that trade-off. It is a judgment call. But it is a call that should not be made by a judgee. Judges do not think about what makes sense fro the perspective of accelerating technological and economic progress. Their concern is with how new areas of technology can be inserted into the legal fring interpretations. Such lazy law-writing practices do not make for good economics or sensible technology policies. The right approach would be to investigate the underlying economics of an industry in order to determine what incentives are necessary for its successful development.")

(ml "Those are socioeconomic decisions that should be made in our legislatures, not in our courts.")

"..."

(ml "In our modern economies, private monopoly power should be less worrisome than it was when our patent system was originally set up. As alternative technologies proliferate, there are fewer and fewer with inelastic demand curves that would allow companies to raise their prices arbitrarily and earn monopoly returns.")

"..."

(ml "As monopoly power wanes and social interests in encouraging the development of new intellectual property grow, the balance in our system should shift toward encouraging the production of new knowledge and be less concerned about the free distribution of existing knowledge. Tighter or longer-term patents and copyrights would seem to be warranted. Laws on intellectual property rights must be enforcable or they should not be laws. ... Laws that cannot or will not be enforced make for neither good law nor good technology policies. ...")

"..."

(ml "... the failure to develop adequate property rights lies behind many U.S. problems with air and water pollution.  Free usage - that is, no enforceable property rights - is sensible for each individual, but it ends up depriving the whole community of clean air and water.  So, too, with intellectual property rights: free usage of knowledge ends up with societies that create too little new knowledge.")

"..."

(ml "In seeking an alternative approach, the US system for settling water rights disputes in irrigated areas might serve as a model. Federal water masters are given the authority to allocate water in dry years and to settle disputes quuickly because crops die quickly.")

"..."
(ml "The prevailing wisdom among those who earn their living within our system of intellectual property protection is that some minor tweaking here and there will fix the problem.  Much of this wisdom flows from nothing more profound than the belief that to open up the system to fundamental change would be equivalent to opening Pandora's box.  All can vividly see themselves as potential losers.  Few consider the private and public gains that might accrue from a different system.")
"..."
(ml "The optimal patent system will not be the same for all industries, all type of knowledge, or all type of inventors. Consider, for example, the electronics industry and the pharmaceutical industry. The first wants speed and short-term protection because most of its money is earned soon after new knowledge is developed. The seconde wants long-term protection because most of its money is earned after a long period of testing to prove a drug's effectiveness and the absence of adverse side effects. ...") 
(ml "Costs, speed of issuance, and dispute-settlement parameters could vary. Let filers decide what type of patent they wish to have. In no other market do we decide that everyone wants - and must buy-exactly the same product. ...")
(ml "The world's current one-dimensional system must be overhauled to crate a more deiffentiated one. Trying to squeeze today's rights simply won't work. One size does not fit all.")
) )
(mail (ML 0 "Discussion" (de "Diskussion"))
(let ((SF (ah "http://ffii.org/mailman/listinfo/swpat" "swpat.ffii.org"))) (ML 0 "This article was subject of some discussions in the %{SF} mailing list:" (de "Dieser Artikel gab zu einigen Diskussionen in %{SF} Anlass:")))
(ul
(ah "http://ffii.org/archive/mails/swpat/2000/May/0078.html")
(ah "http://ffii.org/archive/mails/swpat/2000/May/0080.html")
(ah "http://ffii.org/archive/mails/swpat/2000/May/0083.html")
)
)
)
)

(mlhtdoc 'ipq-kingston97 nil nil nil (papritab))

(mlhtdoc 'iic-schiuma00 nil nil nil (papritab)
(let ((EPC52 (ahs 'epc-52 "EPC §52")) (TRIPS (ahs 'trips "TRIPS"))) (ML ThW "This article by Daniele Schiuma in the legal journal IIC Vol 31, which vehemently argues that the %(q:computer program) exception in %{EPC52} is non-conformant to %{TRIPS} Art 27 and must be deleted."))

(filters ((tf ahs 'swpattrips) (pl ahs 'swpatlijda)) (ML cos "Schiuma, a young patent attorney and research fellow at the Max Planck Institute for International Patent Copyright and Competition Law and discussion leader of the MPI's software patent workgroup, champions the well-known %(tf:TRIPS fallacy) and, in the process, tells us a lot of interesting details about how the %(pl:patent lobby) managed to pretend that granting of software patents was legal."))

(ML Sac "Schiuma's argumentation is built on a mixture of quotations from legal documents and personal political views, such as the presumption that regional legal definitions of %(q:field of technology), %(q:invention), %(q:industrial application) etc should be avoided and a uniform global legal definition must be attempted.  According to Schiuma's view, the object of the TRIPS treaty is %(q:reducing distortions and impediments to international trade) and the non-uniformity of patent law regarding software is such a distortion.")

(ML Tcy "This is one of many unreflected presumptions that make much of Schiuma's argumentation circular:  Schiuma attempts to prove that the European software exclusion is not within the scope of variability allowed by TRIPS.  So he first asserts that TRIPS must be construed so as to allow no variability.")

(ML Icl "It can however easily be argued that TRIPS is intended to %(q:harmonise), as it was always called, not to %(q:uniformise) the systems of various countries.  It imposes more abstract rules such as that no country shall arbitrarily exclude any field of technolgy it happens to want to protect.  These rules are intended to allow the coexistence of diverse systems while minimalising frictions that could arise from regional protectionism.")

(ML Hef "Here is another example of the circularity of Schiuma's reasoning:")

(blockquote
(ML Eet "Even in light of the exceptions in Art.27(3) of TRIPS, it does not appear permissible to construe the term %(q:inventions) in Art. 27(1) differently on a country-by-country basis by using the different legal definitions in the individual member states.  Article 27(3) specifies definitely those areas of invention that a member can exclude from patentability.  The attempt to expand artificially the areas of exclusion permitted by Art. 27(3) by using a %(q:national) legal definition of the concept of the %(q:invention) to exclude from patentability subject matter that is actually to be patented pursuant to Art. 27(1), would thus represent an attempt to evade Art. 27(3) and would lead to a conflict between the provisions of Art. 27(1) and (3).")) 

(ML Hlx "Here Schiuma presupposes what he wants to prove: that software is %(q:actually to be patented pursuant to Art. 27%(pet:1)) and that European national legislation is %(q:artificially expanding the area of exclusion) by using a special %(q:legal definition of the term invention).  The opposite is true:  the scope of what is %(q:to be patented pursuant to Art 27%(pet:1)) cannot be determined without recourse to a %(q:definition of the term invention).")

(ML Swo "Schiuma proceeds to argue that patenting software is more in accordance with TRIPS than not patenting and therefore the whole world must uniformly patent software.  This argumentation, as most legal argumenation contains a lot of personal politico-philosophical assumptions mixed with a complicated set of laws and caselaw.  Here is an example of Schiuma's philosophy on software:")

(blockquote
(ML leh "... no [ TRIPS negtioation group ]  member equated or intended to equate software with mathematical methods or the like.  This would also be in conflict with the nature of software, according to which specific (program) commands cause a physical change in specific elements of the computer.  Accordingly, computer programs, i.e. software, are always of technical nature.  In contrast, mathematical methods and scientific theories (as such) represent pure knowledge that causes no physical changes in its abstract form.  Only the application of a mathematical method, for instance, in a particular field of technology (e.g. by means of a computer) causes a physical change and would thus be technical."))

(ML TWs "This is completely inevident.  Software programs by themselves (i.e. when not running) also don't cause a physical change in any element of the computer.  And even when running, this change does not go beyond the normal predisposition of the computer. Moreover, all computer programs can be represented as mathematical methods and vice versa.")  

(ML Aay "A program running on a computer has one thing in common with a machine: it behaves.  But it has other things in common with a mathematical formula and with other items that can't be patented:  it is pure information which can be copied.  Why should it be up to lawyers to decide which of these qualities is more essential.  How can one decide in just a few sentences like Schiuma is trying above?")

(ML Tah "There seems also to be a big non sequitur in the following paragraph:")

(blockquote (ML JWo "Just as pharmaceutical and agrochemical products can be included within Art 27 of TRIPS without any difficulty, and thus can be ascribed to a %(q:field of technology), software inventions can also be qualified as belonging to a %(q:field of technology).  Correspondingly, software cannot be excluded a priori from the %(q:field of technology) and thus from Art 27%(pet:1)."))

(ML IiW "If software %(e:can) be qualified as a f.o.t., that doesn't imply that it %(q:cannot be excluded ...).  Does Mr. Schiuma really ignore the difference between %(q:can) and %(q:must)?")

(linul
(ML Asg "Again there are big differences between %(q:software) and other %(q:fields of technology) such as pharmaceutics.  Just to name a few:")
(ML Str "Software is a basic cultural technique that pervades all fields of technology and even science, education, commerce etc.  Although astronomy belongs to a completely distinct %(q:field of technology), astronomical observatories need to write and share their own software in order to get their astronomical calculations done.  But they do not need to develop their own drugs.")
(ML Sir "Software can be copied without costs, and it cannot even be sold.  What can be sold are only exclusion rights.  Without exclusion rights, the price of software will tend toward zero.  Moreover, imposing exclusion rights means reducing important technical qualities of software, which is why in many cases free software is the best software.")
)

(ML TEe "The question of economic effect and desirability of software patenting does not appear in Schiuma's writing, but there is the assertion that software has become a %(q:huge industry with an annual economic growth of 13%{pc}) which %(q:deserves equal protection) with other %(q:fields of technology).  Evidently, Schiuma knows somehow that the patent system is not an end in itself but a means for promoting economic progress.  But his reasoning does not go beyond the level of postulating %(q:equality).  Moreover, there is no mention about any real-world cases where people have complained about inequal treatment.  Schiuma's inequality is not a problem of injustice in real life but rather a problem of asymetry in Schiuma's explanation model.") 

(lin
 (filters ((bl ah "http://pauillac.inria.fr/~lang/")) (ML Ird "In a comment on my article, %(bl:Bernhard Lang) pointed out:"))
(blockquote
(ML TuW "The trick of lawyers is to place the issues in their own fictitious world.  If that were justified, the law would never evolve, or would do so according to the oniric evolution of lawyers dreams.")
(ML Ben "But the law is here to express real world and social consensus. In the case of patents, it must be explained in terms of human rights, economic growth, social benefits, innovation incentives (in random order).")
(ML ToW "The job of Lawyers is syntax: to put the conclusion in usable wording, not to change or extend the semantics based on purely textual argumentation.")
(ML Tyc "The first fallacy of Schiumain's rants is that he has no legitimity to begin with, as is the case for all layers when it comes to decide whether an existing law applies to a different subject matter.")
(ML Tou "This fallacy is the origin of many problems with the current changes in economics.")
(linul
(ML Lpn "Lawyers are incompetent in both senses of the word:")
(ML tWl "they do not have the knowledge (technical, economic, social)")
(ML ttW "they do not have the legitimacy"))

(ML Bst "But, like most, they want the power.")
(ML Wtc "We should be careful not to give them legitimacy by arguing with them.")
(ML Otf "On the other hand we can use their arguments against them (as you most remarkably did).") ) )

(ML DwW "During the process argumentation he finds some contradictions between TRIPS and software patenting and even contradictions between the EPC and the EPO's legal practice.  He resolves all these by reference to his initially stated basic presumptions that the world's IP system must be uniformised.  Again we have a mixture of caselaw with implicit more or less naive presumptions.")

(ML SqW "Schiuma's article can be quoted to support some of our criticism of the EPO.")

(progn
(defun hypevi (hyp &rest evi) (dl (l (ML Hoe "Hypothesis") hyp) (l (ML Sai "Schiuma's Evidence") (blockquote (apply 'al evi)))))
(hypevi 
(ML Tcs "The German legislature, when ratifying TRIPS, assumed that software in European law is %(q:not a field of technology) and that this European view is conformant with TRIPS.  Thus, our patent lawyers are clearly trying to impose their view on that of their sovereign:") 
(ML IiW2 "In its ratification of GATT/TRIPS, the German legislature saw no conflict between Sec. 1(2)(3) and 1(3) of the Patent Act and Art. 27(1) of TRIPS...")
"..."
(ML LeW "Leaving aside this inaccuracy, the general criticism can, however, be raised that the German delegation's reply takes as its starting point the German/European interpretation of the concept of %(q:in all fields of technology) in Art. 27(1) of TRIPS, which, as shown above, is inadmissible.")
(ML WaW "Within the meaning of Art. 27(1), software must be regarded both as an %(q:invention) and as part of a %(q:field of technology), with the result that the exclusion of software %(q:as such) from patentability purssuant to Art. 52(2) and (3) of the EPC .. does not conform with TRIPS.")
))

(hypevi 
(ML Tra "The EPO's interpretation of %(q:computer programs as such) as %(q:computer programs as far as they are non-technical) is wrong.")
(ML Egc "Even if a distinction is made between technical and non-technical software programs, the provisions of Art. 52(2)(c) and (3) of the EPC ... are incompatible with Art 27(1) of TRIPS, since the former exclude %(q:programs for computers as such) entirely, i.e. irrespective of such a distinction between technical and non-technical programs.")
)

(hypevi 
(ML Tnt "The deletion of the computer programs exceptions in 52(2) will result in more software patenting even under the current regime of bent law")
(ML Tlm "The non-conformity of the provisions of the .. EPC with TRIPS in regard to software remains evident, for instance, from the decision %(q:Computer-Related Invention/VICOM), in which one and the same technical constellation of facts was interpreted differently on the basis of different wording in the patent claims: on the one hand, a %(q:process for the digital filtering of data) was regarded as a mathematical method or as a computer program %(q:as such) and thus was not patentable, while at the same time a %(q:procedure for the digital processing of images in the form of a data array) was regarded as patentable.")
(ML Iur "It appears doubtful whether such a value judgement would have been made in the same way if the prohibition on patents for computer programs %(q:as such) contained in Art. 52(2)(c) and (3) of the EPC had not existed, since an improved %(q:procedure for the digital filtering of data) (without restriction to the processing of images) can indeed be a technical contribution .. and thus be accessible to patent protection as a matter of principle.")
)

(hypevi
(ML TkE "TRIPS does not take priority over European patent law")
(ML TWW "The decision to classify GATT/TRIPS as community law or as international law continues to play a decisive role for its status in its relationship to German (domestic) law: if it is regarded as community law, it then takes priority, while it enjoys equal status if it need %(q:only) be regarded as international law.")
(filters ((mb ant "Memorandum, BT-Drucks, 12/7655 (new), at 335, 337")) (ML TPg "There continues to be disagreement concerning the direct applicability of the TRIPS provisions in the individual member states.  %(mb:On this point, the German legislature has assumed that TRIPS is directly applicable as a matter of principle)."))
(ML Ixu "In the present case .. direct applicability can, however, be excluded since Art. 27(1) of TRIPS is insufiiciently clear and unconditional on the question of the patentability of software.")
)

(hypevi
(ML TWn "TRIPS does not mandate a change of the EPC")
(ML Tsh "The European Patent Organisationis not a member of the WTO Agreement and thus not subject to any direct obligations deriving from GATT/TRIPS.  It is true that all EPC member states, with the exception of Monaco, are members of the WTO, but TRIPS merely imposes the obligation to adapt national regulations and not any international agreements.")
)

(hypevi
(ML Aro "A lobby withing the EPO is putting European legislatures under pressure to %(q:conform to TRIPS).  This lobby includes the patent offices of certain countries as well as the patent lawyers and their clients.")
(ML Att "At present, there is no specific discussion of any change to the German Patent Act with respect to Art. 27(1) of TRIPS.  On the other hand, the Standing Advisory Committee before the European Patent Office (At the insistence of Austria, Spain, Sweden, Switzerland and the %(q:users of the system)) has pointed out (at the 28th meeting in Munich on June 25-26 1998, cf the comment %(q:Points for a revision of the EPC), CACEPO 2/98) that the provisions of Art. 52(2) of the EPC with respect to computer programs do not conform with Art. 27(1) of TRIPS, and should therevore be deleted.")  
)

(hypevi
(ML TTW "The EU harmonisation directive will bring the nation states under the jurisdiction of TRIPS and WTO related international bodies.  The patent inflation lobby is pushing for a EU directive in order to deprive the nation states of their sovereignty in this area.")
(ML Aii "According to Art. 64 of TRIPS, the Understanding on the Settlement for Disputes (Vereinbarung ueber Regeln und Verfahren zur Beilegung von Streitigkeiten) is applicable to TRIPS, with the result that a GATT Member can initiate dispute settlement proceedings before the WTO in order to reconcile the provisions of the German Patent Act concerning the
patentability of software %(q:as such) with Art. 27(1) of TRIPS.  However, at present it is not possible for a natural or legal person to initiate such proceedings before the WTO.  They can only institute domestic legal proceedings and persuade their government to initiate dispute settlement proceedings.") 

(filters 
 ((eo ant "ECJ, Opinion 1/94, November 15, 1994, 27 IIC 503 (1996) - TRIPS Jurisdiction"))
 (ML IWh "It would also be conceivable for an EU Member to invoke the European Court of Justice pursuant to Art 107 of the EC Treaty for an infringement of the obligation to implement EC law or secondary law by another Member.  Proceedings on the infringement of the treaty can also be initiated by the European Commission according to Art 169 of the EC Treaty.  However, both possibilities presuppose the competence of the European Community, which, according to %(eo:Opinion 1/94 of the European Court of Justice), is divided  with respect to GATT/TRIPS between the European Community and its member states, with the European Community enjoying jurisdiction in those areas in which it has applied harmonisation measures.  In case of patent law, competence is held by the member states, since here, with the exception of a number of narrowly defined areas, no harmonisation has (as yet) taken place.  If the European Communities should subsequently apply harmonisation measures (fn: The European Commission plans a Directive for harmonising the patentability of computer software inventions), the corresponding sector would fall within its competence.") )

(ML Tse "The decision to classify GATT/TRIPS as community law or as international law continues to play a decisive role for its status in its relationship to German law: if it is regarded as community law, it then takes priority, while it enjoys equal stats if it need %(q:only) be regarded as international law.")
) 

(hypevi
(ML TWW2 "The EPO is planning to remove all legislative power as to the scope of patentability from the nation states and take it into its own hand.  Moreover, the EPO has already invented the new rules by two decisions in 1997 and now wants these decisions to be codified as binding rules.")
(ML Acs "Alternatively the Preseident of the EPO has proposed to revise Art. 52(1)-(3) to read like Art. 27(1) of TRIPS, so that the exclusion list, according to Art. 52(2) and (3) of the EPC, would be completely deleted.  On the other hand, the EPO will revise the examination guidelines for software-related inventions by including the interpretation of Art. 52(2) and 52(3) of the EPC according to decsisions T 935/97 and T 1173/97.")
)


(ML Ssi "Schiuma concludes his article by stating the conclusion that he tried to argue for all the way along:")

(blockquote (ML Tqn "There is a binding obligation in Art. 27(1) of TRIPS to provide patent protection for %(q:inventions .. in all fields of technology), whereby these terms must be construed autonomously on the basis of TRIPS, i.e. independently of national interpretation approaches."))

(ML Ase "As we have read above, TRIPS 27 contains no %(q:obligation to construe these terms autonomously on the basis of TRIPS).  And the German legislature doesn't see such an obligation in there either.")

(ML Ant "Apparently Schiuma and his mentors from the MPI, the AIPPI, the UNION and other organisations of the international patent lobby already take it for granted that they are the actual legislators who should, based on pure textual intepretation of caselaw, without any need of justification, impose uniform laws on all sovereign nations.")
)

(mlhtdoc 'bettenresch9901 nil nil nil (papritab)
(ML Tcc "The Technical Board of Appeal 3.5.1 of the European Patent Office (EPO) has decided that, in principle, media claims (covering the computer program on a storage medium ...) and Internet claims (covering the transmission or electronic distribution of the computer program) are admissible. ...")

(ML Aoc "According to the US-CAFC (%(q:In re Lowry)) and the Guidelines of the Japanese and the Korean Patent Offices, %(s:data structures) are protectable by a patent claim.  This question has not yet been decided by the Technical Boards of Appeal of the EPO.  In view of the EPO decision %(q:BBC / Colour Television Signal) we are, however, quite confident that the EPO will grant, in the long run, such claims as well.")

(ML IWo "In view of the practice of the last two to five years it can be said that, in principle, a patent will be granted for %(s:all computer programs) (including business methods) which are new and inventive. This is at least valid for the EPO and the German Patent and Trademark Office, but not yet for the UK Patent Office.  In connection with this we refer to the %(q:SOHEI) case (EP 209907 for a computer managment system), and EP patents for a trade warrant system (EP 762304), a stateless shopping cart for the web (EP 784279), and an interactive information selection apparatus (for selecting the items for a meal) (EP 756731). Thus the practice of the EPO seems to be quite similar to that of the USPTO, even if the wording of the claims differs somewhat.")

(ML Ieh "In this connection it may be interesting to know that in 1997 the number of European patent applications in the field of data processing, most of them relating to computer programs, had the highest growth of 28%{pc} compared with 1996, and that the EPO has started to establish a second division of examiners dealing with software applications.")

(ML Itr "In General the practise of the German PTO is quite similar to that of the EPO.  The UNION Round Table Convference on %(q:Patenting of Computer Software) in December 1997 obviously had a good impact not only on the European, but also on the German situation.  In 1998 the 17th Senate of the German Patents Court, who had a rather restrictive practice as to patenting of computer programs in the past, surprisingly admitted in 1998 in two cases the appeal on points of law to the German Federal Supreme Court (FSC).  Such an admission had been denied all the years before.  This will give the FSC the possibility to consider the discussion of the last years and to bring its case law of 1991/1992 in line with that of the Technical Boards of Appeal of the EPO.")

(filters ((ma ahs 'grur-mellu98)) (ML OsW "One judge of the FSC, who is the expert in the FSC for computer programs, has just published an %(ma:article) showing his %(q:personal opinion), according to which, in principle, computer programs should be considered technical.  However only the conversion of the logical concept into the operation of the computer or the realization of the logical concept (program) by the computer, but not the logical concept itself should be protected by patents.  This approach seems to be quite similar to what is known as %(q:technical application) of the computer program in the USA."))

(ML Tae "The latest state of the discussion on the amendment of the European Patent Convention (EPC) is that there seems to be a great consensus within the deciding bodies of the EPO and the European Commission that Art 52(2) and (3) EPC (including the exclusion of computer programs as such) should be cancelled and that Art. 52(1) EPC should be brought in line with Art 27(1) TRIPS Agreement.  Although such an amendment could take years, the efforts towards such an amendment may have an impact on the general practice of the EPO so that the exclusion of computer programs as such from patentability in Art. 52(2) EPC will be interpreted in a very narrow way.")
)

(mlhtdoc 'lamy98 nil nil nil
(papritab nil (l (tok nl-remark) (let ((LAMY (ml (ahs 'lamy-droit-ir)))) (ML LKy "%{LAMY} est un oevre de référence prestigieux." (de "%{LAMY} ist ein französisches Standard-Nachschlagewerk des Computerrechts.") (en "%{LAMY} is a french standard refernce work on the legal aspects of computing.")))))

nil
(ML Ve1 "Voici le Résumé final:" (de "Am Schluss wird resümiert:"))
(blockquote (al
(lin (ML Lkm "Le logiciel est-il donc finalement brevetable?" (de "Ist Software nun endlich patentierbar?"))
     (ML SuW "Sans doute pas encore." (de "Zweifellos noch nicht.")) ) 
(ML Eee "En réalité, les règles nationales et conventionnelles sont claires:  elles posent sans équivoque un principe de non-brevetabilité du logiciel.  Le jeu qui se joue aujourd'hui consiste à contourner d'une manière ou d'une autre celles-ci, par exemple en imaginant de considérer, comme on l'a vu, l'ensemble constitué par le matériel et le logiciel comme une machine virtuelle susceptible (demain ...) d'être breveteée.  À ce compte-là, on peut parler brevets.  Les brevets susceptibles d'être ainsi obtenus, par ce canal ou un autre, n'ont, toutefois, que la valeur qu'on leur prête - mais il ne faut pas écarter l'hypothèse selon laquelle on finirait par une sorte de consensus à ne pas vraiment la discuter.  De fait, l'efficacité de ce countournement des règles légales sera largement fonction du fait qu'un tel consensus se dégagera pour accepter --- contre les règles positives --- que ce nouveau jeu se joue ou non.  La question ne se situe plus sur le terrain juridique %(e:stricto sensu)." (de "In Wirklichkeit sind die Gesetzesregeln des Übereinkommens und der nationalen Gesetze klar:  Sie fordern unmissverständlich die Nicht-Patentierbarkeit von Software.  Das Spiel, das heute gespielt wird, besteht darin, in einer oder der anderen Weise diese Regeln zu verdrehen, z.B. indem man sich, wie oben beschrieben, die Gesamtheit aus Hardware und Software als eine virtuelle Maschine denkt, die (künftig ...) patentierbar sein könnte.  Unter dieser Voraussetzung kann man dann patentrechtlich argumentieren.  Die auf diese Weise auf dem einen oder anderen Wege erhältlichen Patente haben allerdings nur denjenigen Wert, den man ihnen beimisst --- oder der sich durch einen Konsens ergibt, dieser Frage nicht genauer nachgehen zu wollen.  Tatsächlich kann die Verdrehung der Gesetzesregeln nur insoweit Wirkung entfalten, wie sich ein Konsens darüber herstellen lässt, ob man dieses Spiel gegen die bestehenden Gesetzesregeln spielen soll oder nicht.  Hierbei handelt es sich nicht mehr um eine juristische Frage im strengen Sinne."))
(ML Soi "Sur ce terrain, c'est en termes d'évolution des règles écrites qu'il faudrait se situer (en termes de levée des interdits)." (de "Juristisch gesehen müsste die Entwicklung sich auf der Ebene der geschriebenen Regeln bewegen (etwa durch Aufhebung von Patentierungsausschlüssen).")) 
) )
)

(mlhtdoc 'digidilem00 nil nil nil
(papritab)

(blockquote (ah "http://books.nap.edu/html/digital_dilemma/"))

(ML ArW "As you can see from the table of contents, it is a report by the United States National Research Council, with participation from the leading scholars in all fields, including reserachers from IBM and Microsoft.")

(ML Tei "The aim of the report is to explore possible ways of protecting intellectual property in a digital world.")

(ML Tti "The report proposes ways of extending copyright, but at the same time is very critical about applying patents to %(q:information innovations) --- a good term that maintains the distinction between a patentable %(q:invention) and immaterial %(q:innovations), which should have stayed outside the scope of the patent system.")

(ML TtW "The Chapter on p 192-197")

(blockquote (ML Ttf "The Impact of Granting Patents on Information Innovation"))

(ML rWW "reports how the US patent system was gradually broadened by the patent courts (without legislative support) to include software and business methods, and analyses a few of the famous cases.  The report concludes:")

(blockquote (al
(ML Tel "The effects of this substantial de facto broadening of patent subject matter to cover information inventions are as yet unclear. Because this expansion has occurred without any oversight from the legislative branch and takes patent law into uncharted territories, it would be worthwhile to study this phenomenon to ensure that the patent expansion is promoting the progress of science and the useful arts, as Congress intended.")

(ML Tno "There are many reasons to be concerned. There is first the concern that the U.S. Patent and Trademark Office lacks sufficient information about prior art in the fields of information technology, information design, and business methods more generally to be able to make sound decisions about the novelty or nonobviousness of claims in these fields.47 A related concern is the insufficient number of adequately trained patent examiners and inadequate patent classification schemata to deal with this new subject matter. The success of the patent system in promoting innovation in a field depends on the integrity of the process for granting patents, which in turn depends on adequate information about the field. Serious questions continue to exist within the information technology field about the PTO's software-related patent decisions. A number of legal commentators have pointed out that allowing these kinds of patents potentially makes concepts, not technology, the protectable property of the patent holder, %(q:allow[ing] virtually anything under the sun to win patent protection).")

(ML Sic "Second, the tradition of independent creation in the field of computer programming may run counter to assumptions and practices associated with patents as they are applied to its traditional domains. When someone patents a component of a manufactured system, for example, it will generally be possible for the inventor to manufacture that component or license its manufacture to another firm and reap rewards from the invention by sale of that component. Rights to use the invention are cleared by buying the component for installation into a larger device.")

(ML BWW "But there is little or no market in software components. Programmers routinely design large and complex systems from scratch. They do so largely without reference to the patent literature (partly because they consider it deficient), although they generally respect copyright and trade secrecy constraints on their work. With tens of thousands of programmers writing code that could well infringe on hundreds of patents without their knowing it, there is an increased risk of inadvertent infringement.  An added disincentive to searching the patent literature is the danger that learning about an existing patent would increase the risk of being found to be a willful infringer. The patent literature may thus not be providing to the software world one of its traditional purposes--providing information about the evolving state of the art. Much the same could be said about the mismatch between patents and information inventions in general.")

(ML Tts "Third, although patents seem to have been quite successful in promoting investments in the development of innovative manufacturing and other industrial technologies and processes, it is possible that they will not be as successful in promoting innovation in the information economy. One concern is that the pace of innovation in information industries is so rapid, and the gears of the patent system are so slow, that patents may not promote innovation in information industries as well as they have done in the manufacturing economy. The market cycle for an information product is often quite short--18 months is not unusual; thus, a patent may well not issue until the product has become obsolete. If information inventions continue to fall within the scope of patents, then, at a minimum, the patent cycle-time needs to be improved significantly. Patent classification systems for information innovations may also be more difficult to develop and maintain in a way that will inform and contribute to the success of the fields they serve.")

(ML Oir "One final reason for concern is that developing and deploying software and systems may cease to be a cottage industry because of the need for access to cross-licensing agreements and the legal protection of large corporations. This in turn may have deleterious effects on the creativity of U.S. software and Internet industries.")
))
)


(mlhtdoc 'bpatg17w6998 nil nil nil (papritab)

(dl
(l (ML Lxg6 "Aktenzeichen") "17W(pat)69/98")
(l (ML Lxg7 "An Verkündungs Statt zugestellt am") "28.07.2000")
)

(center
(LARGE (ML Bcu "Beschluss"))
(ML IBr "In der Beschwerdesache")
(ML bPW "betreffend die Patentanmeldung P 4323241.8-53")
(ML dsW "der International Business Machines Corporation, Armonk, N.Y. (V.St.A.),")
)

(flushright (ML Aur "Anmelderin und Beschwerdeführerin"))

(dl
(l (ML Vnm "Verfahrensbevollmächtigter") (ML PIG "Patentanwalt Dipl.-Phys. F. Teufel, IBM Deutschland Informationssysteme GmbH, Pascalstr 100, 70569 Stuttgart"))
)

(ML hej "hat der 17. Senat (Technischer Beschwerdesenat) des Bundespatentgerichts auf die mündliche Verhandlung vom 29. Februar 2000 unter Mitwirkung des Vorsitzenden Richters Dipl.-Phys. Grimm, der Richter Dipl.-Ing. Bertl und Dipl.-Ing. Prasch sowie der Richterin Püschel beschlossen:") 

(ol
(ML DWe "Die Beschwerde der Anmelderin gegen den Beschluss der Prüfungsstelle für Klasse G 06 F des Deutschen Patentamts vom 6. Juli 1998 wird zurückgewiesen")

(ML Dsd "Die Rechtsbeschwerde wird zugelassen")
)

(headline (ML GKd "Gründe"))

(filters ((bz center)) (ML DJr "Die vorliegende Patentanmeldung ist beim Deutschen Patentamt am 12. Juli 1993 unter der Bezeichnung %(bz:Verfahren und Computersystem zur Suche fehlerhafter Zeichenketten in einem Text) angemeldet worden."))

dots

(ML Dvg "Der geltende Patentanspruch 1 vom 10. Februar 1998, eingegangen am 12. Februar 1998, lautet:")

(blockquote
(ML VWe "Verfahren zur computergestützten Suche und/oder Korrektur einer fehlerhaften Zeichenkette F_i in einem digital gespeicherten Text, der die entsprechende fehlerfreie Zeichenkette S_i enthält,")

(ML deh "dadurch gekennzeichnet, dass")

(optscall 'ol '((type "a"))
(ML dfc "die Auftretenshäufigkeit H(S_i) der fehlerfreien Zeichenkette S_i ermittelt wird")
(ML dee "die fehlerfreie Zeichenkette S_i nach einer Regel R_j verändert wird, so dass eine mögliche fehlerhafte Zeichenkette f_ij erzeugt wird,")
(ML dai "die Auftretenshäufigkeit H(_ij) der Zeichenkette f_ij in dem Text ermittelt wird,")
(ML diW "die Auftretenshäufigkeiten H(_ij) und H(S_i) verglichen werden und")
(ML bea "basierend auf dem Vergleich in Schritt (d) entschieden wird, ob die mögliche fehlerhafte Zeichenkette f(_ij) die gesuchte fehlerhafte Zeichenkette F(_j) ist.")
)
)

dots

(ML EoP "Eine Definition des Begriffs %(q:Programms als solchen) ist weder dem Gesetzeswortlaut noch der Gesetzesbegründung noch der bisherigen höchstrichterlichen Rechtsprechung zu entnehmen...  In der Entscheidung %(q:Logikverifikation) (Mitt 2000, 293) hat der BGH nunmehr jedoch hierzu Ausführungen gemacht und auf im wesentlichen drei unterschiedliche Meinungen hingewiesen.  Danach könne hierunter zum einen das gedankliche Konzept, das sich durch die jeweilige Anwendung erschließe, verstanden werde, zum anderen das Produkt der eigentlichen Programmierung, also die kodierten Befehlsfolgen für den Computer; die dritte Meinung liege den Entscheidungen %(q:Computerprogrammprodukt) und %(q:Computer program product II) der Beschwerdekammer 3.5.1 des EPA zugrunde.  Der Senat schließt sich der zweiten Meinung an.")

(ML Anu "Ausgehend davon, dass Programme für Datenverarbeitungsanlagen das Gebiet der Datenverarbeitung bzw Computertechnik betreffen, orientiert sich der Senat bei seiner Interpretation des Begriffs %(q:Programm für eine Datenverarbeitungsanlage als solches) am Verständnis des Computerfachmanns.  In der Fachsprache wird der Begriff %(q:Programm) mehrdeutig verwendet, bspw für Programmentwürfe, Programmdarstellungen in höherer Programmiersprache, ablauffähige Maschinenprogramme und auch für die unmittelbar Schaltglieder steuernden Bitmuster eines Mikroprogramms (vgl %(q:Das Computerprogramm im Recht) Dr. M.M. König, Verlag Dr. Otto Schmidt KG, 1991, Rdn 150-155).  Im weiteresten Sinne umfasst der Begriff %(q:Programm) die verschiedenen Entwurfsstufen und Ausführungsformen eines Programms und wird sowohl für dessen Aufzeichnung als auch für ein auf einem Computersystem ablaufendes, d.h. aktives Programm verwendet (vgl %(q:Lexikon Informatik und Datenverarbeitung), 4. Aufl, R. Oldenbourg Verlag, München Wien, 1998, S 652f).  Im engeren Sinne verwendet der Fachmann den Begriff %(q:Programm) jedoch für den Programmcode und dessen Aufzeichnungen auf Klarschriftdatenträgern wie Papier oder maschinenlesbaren Speichermedien.")

dots

(ML Nis "Nach alledem ist der Senat daher der Auffassung, dass unter einem %(q:Programm als solchen), das nach §1 Abs 2 Nr 3 und Abs 3 PatG vom Patentschutz ausgeschlossen ist, der Programmcode und dessen Aufzeichnung auf einem Speichermedium gleich welcher Art, sei es Papier oder ein elektronisches Medium, zu verstehen ist.  Eine in einem Programm enthaltene Lehre - idR ein Arbeitsverfahren - kann hingegen eine Erfindung im Sinne des §1 Abs 1 PatG sein, sofern diese Lehre technischen Charkter hat, dh Wirkungen entfaltet, die über das übliche Zusammenwirken von Programmaufzeichnung und Computersystem hinausgehen, und damit den Einsatz beherrschbarer Naturkräfte zur Erreichung eines Erfolges lehrt.") 

dots

(lin (ML Eod "Eine andere Meinung vom %(q:Programm als solchen) ergibt sich aus der Entscheidung %(q:Computerprogrammprodukt/IBM) bzw aus %(q:Computer program product II/IBM) (T 0935/97-3.5.1 vom 4. Februar 1999); gemäß der dort vorgenommenen Interpretation können Aufzeichnungen von Programmen auf Speichermedien patentfähig sein.") dots)

(ML Dcr "Dem dort vertretenen Verständnis eines Programms als solchen vermag der Senat jedoch nicht zu folgen.  Nach den Ausführungen in der Entscheidung %(q:Computerprogrammprodukt/IBM) wird der Begriff %(q:Computerprogramme) ebenso wie %(q:Computerprogramme als solche) unterschiedslos für Programminhalt, d.h. aktiv ablaufende Programme, und Programmaufzeichnungen auf Speichermdien verwendet.  Eine Unterscheidung zwischen dem %(q:Programm als solchen) und dem Programm im weiteren Sinn wird dort allein an Hand des rechtlichen Maßstabs des %(q:technischen Charakters) getroffen.  Danach ist einem auf einem Speichermedium gespeicherten Programm technischer Charakter zuzusprechen, wenn bei der Auführung der Programmbefehle physikalische Veränderungen bei der Hardware (d.h. bei dem ausführenden Computersystem) mit weiteren technischen Effekten einhergehen, die über das übliche Zusammenwirken von Programmaufzeichnung und Computersystem nausgehen.  Abgesehen davon, dass sich bei einer derartigen Auslegung die Frage stellt, ob überhaupt noch Anwendungsfälle für den Ausschlusstatbestand %(q:Programm für eine Datenverarbeitungsanlage als solches) verbleiben, werden aber bei einer solche Betrachtungsweise dem beanspruchten Gegenstand Eigenschaften zugeschrieben, die ihm objektiv nicht zukommen.  Gegenstand des vorliegenden Anspruchs 22 ist ein digitales Speichermedium, dessen elektronisch auslesbare Steuersignale so mit einem programmierbaren Computersystem zusammenwirken können, dass das Such- und Korrekturverfahren ausgeführt wird.  Wie unter 1.2.1 erläutert, versteht der Fachmann unter den im Anspruch genannten Steuersignalen die Signale, die der Abfolge der aufgebrachten Daten entsprechen.  Steuersignale, die repräsentativ sind für die Ausführung des Such- und Korrekturverfahrens, vermag das beanspruchte Speichermdium für sich nicht hervorzubringen.  Es vermag nicht zu überzeugen, die Patentfähigkeit eines Gegenstandes mit technischen Wirkungen oder einem zusätzlichen technischen Effekt zu begründen, die dieser Gegenstand nicht - jedenfalls nicht allein - hervorbringen kann.   Ein %(q:Potential zur Erzeugung eines technischen Effekts), wie es in der zitierten Entscheidung einer Aufzeichnung auf einem Speichermedium zugeschrieben wird kommt einem Aufzeichnungsträger allein nicht zu, sondern erst dem Computersystem mit dem vom Speichermedium in den Arbeitsspeicher geladenen Programm, das tatsächlich in der Lage ist, ein ggf technisches Verfahren auszuführen.")

(ML Eun "Ebensowenig vermag der Senat der in der Literatur vertretenen dritten grundsätzlichen Meinung zum dem Begriff %(q:Programms als solchen) zu folgen, wonach hierunter das gedankliche Konzept anzusehen sei, das keinen Schutz durch ein Patent verdiene (vgl Mellulis, GRUR 1998, 843, 850 ff).")

(ML BEs "Begründet wird diese Meinung aus der Vorstellung, dass jedes Datenverarbeitungsprogramm einen vom Verstand des Menschen unabhänigen Vorgang in der Datenverarbeitungsanlage steuern könne, so dass jedem Programm technischer Charakter zukomme.  Die hinter dem Programm stehende Konzeption hingegen sei lediglich gedanklicher Natur und verdiene deshalb keinen Patentschutz.  Eine ggf vorliegende technische und erfinderische Leistung werde durch den Fachmann bei der Umsetzung der gedankliche Konzeption in eine konkrete Programmausführung erbracht.")
 
(ML Hre "Hinsichtlich dieser Meinung hat der Senat folgende Bedenken.  Es ist als grundlegende Tatsache anzusehen, dass hinter jeder Erfindung eine geistige, dh gedankliche Leistung steckt.  Der geistige Weg zum Auffinden einer Problemlösung ist für sich als gedankliche Tätigkeit nach §1 Abs 2 und 3 PatG vom Patentschutz ausgeschlossen.  Führt eine gedankliche Leistung jedoch zur Lösung einer technischen Aufgabenstellung unter Einsatz technischer Mittel, wie bspw zu dem erteilten Verfahren zur computergestützten Suche und/oder Korrektur, so kann diese angewandte Lehre sehr wohl patentfähig sein.  Dies trifft auch dann zu, wenn die %(q:Lehre mit dem alle vorgeschlagenen Mittel kennzeichnenden Prinzip im Patentanspruch) umschrieben ist, also nur das Lösungskonzept angegeben ist (vgl BGH GRUR 1984, 849, 851 - Antiblockiersystem).   Würde der dargestellten Meinung gefolgt, so hätte dies die Konsequenz, dass eine erfinderische Leistung nicht durch die grundlegende Konzeption eines Verfahrens bzw Programminhaltes erbracht werden kann, sondern lediglich durch die Umsetzung dieser Konzeption in konkrete Programmschritte.  Aus der Praxis ergibt sich aber, dass eine erfinderische Leistung idR gerade in dem grundlegenden Entwurf des Systemdesigners zu sehen ist, während die Umsetzung dieses Entwurfs in die eine oder andere Befehlsfolge regelmäßig im Bereich des fachmännischen Handelns angesiedelt ist, wie auch der vorliegende Anspruch 1 erkennen lässt.")
 
dots

(ML Ami "Auch das Übereinkommen über handelsbezogene Aspekte der Rechte des geistigen Eigentums (Trade Related Aspects of Intellectual Property Rights = TRIPS) führt zu keiner anderen Beurteilung der Patentfähigkeit.  Abgesehen von der Frage, in welcher Form das TRIPS-Abkommen - unmittelbar oder mittelbar - anwendbar ist (vgl die Senatsentscheidung 17 W (pat) 68/98 vom 18. Januar 2000, zur Veröffentlichung vorgesehen), würde nämlich auch die Heranziehung von Art 27 Abs 1 TRIPS-Abkommen hier nicht zu einem weitergehenden Schutz führen.  Mit der dortigen Formulierung, wonach Patente für Erfindungen auf allen Gebieten der Technik erhältlich sein sollen, wird nämlich im Grunde nur die bisher schon im deutschen Patentrecht vorherrschende Auffassung bestätigt, wonach der Begriff der Technik das einzig brauchbare Kriterium für die Abgrenzung von Erfindungen gegenüber andersartigen geistigen Leistungen, mithin die Technizität Voraussetzung für die Patentfähigkeit ist (in der Entscheidung des BGH %(q:Logikverifikation) ist insoweit die Rede von %(q:nachträglicher Bestätigung) der Rechtsprechung durch die Regelung in Art 27 Abs 1 TRIPS-Abkommen).  Auch der Ausschlusstatbestand des §1 Abs 2 Nr 3 und Abs 3 PatG kann vor dem Hintergrund, dass er auf dem Gedanken des fehlenden technischen Charakters dieser Gegenstände beruht (vgl die Ausführungen unter 1.4.1) nicht im Widerspruch zu Art 27 Abs 1 TRIPS-Abkommen gesehen werden.")

(ML Znc "Zum Anspruch 23:")

(ML Gie "Gemäß seinem Wortlaut bezieht sich dr Anspruch auf ein Computerprogrammprodukt.  Der Begriff %(q:Computerprogrammprodukt) entspricht nicht dem üblichen Sprachgebrauch des Computerfachmanns.  Die Anmelderin will unter diesem Begriff jegliche zur Verbreitung von Programmen geeignete Form verstanden wissen.   Im vorliegenden Fall ist das Computerprogrammprodukt dadurch näher definiert, dass es auf einem maschinenlesbaren Träger gespeicherten Programmcode aufweist.  Dieser Programmcode soll so beschaffen sein, dass er zur Durchführung des Verfahrens nach den Ansprüchen 1 bis 16 führt, wenn er auf einem Rechner abläuft.")

(ML Ugi "Unter Zugrundelegung dieser Bedeutung stellt sich das %(q:Computerprogrammprodukt) nach dem Anspruch 23 dem Fachmann als maschinenlesbares Speichermedium mit einer Programmaufzeichnung dar, die ein Rechner so interpretieren kann, dass er das Verfahren nach den Ansprüchen 1 bis 16 ausführt.  In diesem Sinne versteht auch die Anmelderin den Anspruch 23.  Nach ihren Ausführungen soll der Anspruchsgegenstand bspw ein Programmpaket mit mehreren Disketten sein, das als handelbares Produkt angeboten wird.")

(ML DpW "Das Computerprogrammprodukt nach dem Anspruch 23 ist gemäß §1 Abs 1 und Abs 2 Nr 3 iVm Abs 3 PatG nicht patentfähig.")

(ML Dmn "Dem Verständnis des Fachmanns und den Erläuterungen der Anmelderin nach ist unter dem im Anspruch 23 spezifizierten Computerprogrammprodukt mit gespeichertem Programmcode sonach nichts anderes zu verstehen als unter dem Gegenstand des Anspruchs 22, nämlich (mindestens) ein Speichermedium auf dem ein Programm zur Ausführung des Verfahrens nach einem der Ansprüche 1 bis 16 aufgezeichnet ist.")

(ML Den "Das %(q:Computerprogrammprodukt) nach dem Anspruch 23 ist deshalb auch nicht anders zu bewerten als das Speichermedium nach dem Anspruch 22, so dass auf die Gründe unter 1.3 und 1.4 verwiesen wird.  Daher kann in dem %(q:Computerprogrammprodukt) nach dem Anspruch 23 keine Erfindung iSd §1 Abs 1 PatG erkannt werden; ebenso ist es als %(q:Programm für eine Datenverarbeitungsanlage als solches) nicht als Erfindung anzusehen (§1 Abs 2 Nr 3 und Abs 3 PatG).")

(ML Znc2 "Zum Anspruch 24:")

(ML Dmc "Dem Anspruchswortlaut nach begehrt die Anmelderin Schutz für den Programmcode eines Datenverarbeitungsprogrammes, ohne dass dieser auf einem Speichermedium aufgezeichnet zu sein braucht.  Ihren Erklärungen nach möchte sie mit dieser Anspruchsfassung gegen Verletzungen vorgehen können, bei denen der Programmcode für das Such- und/oder Korrekturverfahren nicht auf einem Speichermedium vertrieben wird, sondern über Datennetze, bspw das Internet, übertragen wird.")
 
(ML GeD "Gegenstand des Computerprogramms nach dem Anspruch 24 ist sonach jeglicher Programmcode, der ein geeignetes Computersystem dazu veranlassen kann, das Such- und/oder Korrekturverfahren auszuführen. Im Programmcode selbst kann .. nur ein %(q:Programm für eine Datenverarbeitungsanlage als solches) erkannt werden, das nach §1 Abs 2 Nr 3 und Abs 3 PatG nicht als Erfindung anzusehen ist.")

(ML Dks "Die mit den Ansprüchen 22 bis 24 beanspruchten Gegenstände sind sonach keine patentfähigen Erfindungen, so dass die gegen die Zurückweisung des Hauptantrags gerichtete Beschwerde der Anmelderin zurückzuweisen war.")

(ML Dzn "Die Rechtsbeschwerde ist gemäß §100 Abs 2 PatG zuzulassen, da die Frage des %(q:Programms als solchen) noch nicht abschließend höchstrichterlich geklärt ist.")

)

(mlhtdoc 'grur-kolle77 nil nil nil (papritab)
(sects
(hist (ML Hoc "Historisches")
 "..."
 (ML Ieh "Im Sommer 1974 kam dann die Wende.  Nach einer grundlegend veränderten Besetzung nahm der 17. Senat des Bundespatentgerichts drei weitere Sachen zum Anlass, seine bisherige Rechtsprechung zu überprüfen.  Dies führte zu einer nahezu vollständigen Abkehr von der früheren Rechtsansicht.  Fassen wir die wesentlichen Ergebnisse dieser drei Entscheidungen leitsatzartig zusammen, so stellt sich die Meinung des Senats über die Patentfähigkeit von Vorschriften zur automatischen Datenverarbeitung nunmehr wie folgt dar:")
 (ol
  (ML Lrh "Lösungsmaßnahmen zur maschinellen Bearbeitung organisatorischer oder ähnlicher Problemesowie die Angabe von Algorithmen bzw daraus abgeleiteter Rechenprogramme zur maschinellen Problemlösung durch eine Datenverarbeitungsanlage bilden jedenfalls dann keine Lehre zu technischem Handeln, wenn das Gewicht der vorgeschlagenen Maßnahmen nicht in der die Zweckerreichung erst ermöglichenden konstruktiven Weiterbildung der zu verwendenden Datenverarbeitungsanlage und/oder der besonderen Zusammenschaltung, Ausnutzung oder Anpassung ihrer Baugruppen liegt, sondern die vorgeschlagegenen Maßnahmen sich in der Angabe des mathematischen Lösungswegs, des Algorithmus oder der daraus ableitbaren Folge von Teiloperationen erschöpfen und lediglich die Bearbeitungsmöglichkeiten einer zum Stand der Technik gehörenden Datenverarbeitungsanlage genutzt werden und die Ausführung aufgrund der Anlagekonstruktion keine Schwierigkeiten bereitet.")
  (ML Vnh "Verfahren, die außer technischen Verfahrensschritten auch solche Schritte aufweisen, die eine abwägende Verstandestätigkeit des Menschen erfordern, also nicht nur eine rein mechanische Geistestätigkeit, die auch ein Automat abnehmen könnte, sind als Ganzes nicht patentfähig.")
  )
  (ML Weh "Wegen der grundsätzlichen Bedeutung der Entscheidungen hat der Senat in allen drei Fällen die Rechtsbeschwerde zugelassen, die auch eingelegt worden ist. ...")
  (ML Mho "Mit dem Dispositionsprogramm-Beschluss des Bundesgerichtshofs liegt nun die erste Entscheidung über die drei vom 17. Senat des Bundespatentgerichts zugelassenen Rechtsbeschwerden und die erste höchstrichterliche Entscheidung zu den patentrechtlichen Problemen der ADV überhaupt vor.  Wie die vorgesehene Aufnahme des Beschlusses in die amtliche Sammlung erkennen lässt, misst ihm der Patentsenat selbst grundsätzliche Bedeutung zu.  Diese hat der sorgfältig begründete Beschluss in der Tag.  Weit über die Entscheidung des konkreten Falls hinaus bildet der Beschluss einen gewichtigen Beitrag zur Fortbildung des materiellen Patentrechts, der mit seiner im Leitsatz nicht annähernd reflektierten Fülle von Aspekten und Gedanken auch für die Anwendung und Auslegung der %(q:europäischen) Regeln über die Patentfähigkeit des künftigen deutschen Patentrechts uneingeschränkt Geltung beanspruchen kann.")
  (ML Ddt "Den Kern der Entscheidung bilden die Üeberlegungen zum Begriff der %(q:technischen) Erfindung, zu seiner Abgrenzungsfunktion und Anwendung auf Lösungsmaßnahmen im Bereich der ADV.  Sodann präsentiert der Bundesgerichtshof in solcher Klarheit erstmals ausgesprochene Regeln für die Sachprüfung von Anmeldungen, deren technischer Charakter zweifelhaft ist.  Von eminent praktischer Bedeutung ist schließlich der in der Entscheidung umrissene Orientierungsrahmen, wo für Lehren auf dem Gebiet der Informatik positiv die Zone des Patentierbaren beginnt.")
)

(tech (ML Dnr "Die technische Erfindung")
(ML Mna "Mit dem Dispositionsprogramm-Beschluss legalisiert der Bundesgerichtshof gewissermaßen eine seit seiner Rote-Taube-Entscheidung ständige Praxis: Die Übernahme der dort gegebenen Definition der technischen Erfindung als gleichsam universaler Formel für die Prüfung, ob eine Erfindung im Rechtssinn vorliegt, welchem spezifischen Bereich menschlichen Wirkens sie auch immer zugehören mag.  Denn damals hatte der Bundesgerichtshof seine inzwischen berühmte Definition der technischen Erfindung als %(q:Lehre zum planmäßigen Handeln unter Einsatz beherrschbarer Naturkräfte zur Erreichung eines kausal übersehbaren Erfolgs) aus der Frage heraus entwickelt, ob Erscheinungen und Vorgänge in der belebten Natur unter einen zeitgemäß verstandenen Begriff der Technik zu subsumieren sind, und ihre Geltung grundsätzlich auf diesen Bereich beschränkt.  Obwohl der Bundesgerichtshof diese Formel, wie aus den Gründen klar hervorgeht, nicht ohne weiteres auch für die Abgrenzung der Technik von der Welt des rein Geistigen verwendet wissen wollte, hat z.B. der 17. Senat des Bundespatentgerichts sie in der Benson-Entscheidung als auch für dieses Abgrenzungsproblem maßgeblich zugrunde gelegt und die Patentfähigkeit des streitigen Verfahrens gerade daraus hergeleitet.  Dass der Bundesgerichtshof die Rote-Taube-Formel der technischen Erfindung nunmehr für %(q:allgemeinverbindlich) erklärt hat, ist zu begrüßen, jedenfalls was ihren Kernbestandteil angeht, d.h. die Umschreibung des TEchnischen als eines Gebiets, auf dem beherrschbare Naturkräfte eingesetzt werden.")
(sects
(elem (ML Dhu "Die Lehre zum technischen Handeln und ihre Elemente")
(ML UWR "Unter allen bekannten Ansätzen erscheint die Rote-Taube-Formel nach dem derzeitigen Stand der Erkenntnisse als am besten geeignet und flexibel genug, eine der technischen, wissenschaftlichen und gesellschaftlichen Entwicklung Rechnung tragende, sachgerechte Abgrenzung patentfähiger Lehren von nicht schutzfähigen Vorschlägen zu ermöglichen.")
(filters ()
 (ML DAe "Die Versuche des Reichsgerichts und des Reichspatentamts, der Technik definitorisch Herr zu werden, stellten entweder allein auf Physik und Chemie ab oder sahen das Wesen des Technischen allzu eng nur in seinem Gegensatz zur Welt des Geistigen.  Die häufig zitierte Definition der technischen Erfindung aus der Wettschein-Entscheidung des Bundesgerichtshofs, wonach unter Erfindung %(q:eine angewandte Erkenntnis auf technischem Gebiet) zu verstehen ist, also eine %(q:Anweisung, mit bestimmten technischen Mitteln zur Lösung einer technischen Aufgabe ein technisches Ergebnis zu erzielen), nennt zwar die notwendigen Elemente der Regel zu technischem Handlen, bleibt aber weitgehend eine Antwort darauf schuldig, was denn nun Technik im patentrechtlichen Sinn sei.  Immerhin findet sich in der Begründung mit der beiläufigen Erwähnung der Lindenmaier'schen Formel, dass die Technik sich als %(q:Benutzung von Kräften oder Stoffen der belebten oder unbelebten Natur) darstelle, ein richtungsweisender Ansatz.  Auch die kürzlich zum Gebrauchsmusterrecht ergangene Buchungsblatt-Entscheidung vermittelt über das spezielle Problem der Beurteilung von Flächenmustern hinaus keine tieferen Einsichten in das Wesen der Technik.  Die vom 17. Senat des Bundespatentgerichts im Typensatz-Beschluss vorgeschlagege Umschreibung des Technischen als %(q:Welt der Dinge) ist blass und für Abgrenzungszwecke kaum brauchbar."))
(ML SWh "Solche Schwächen haften der Rote-Taube-Formel nicht an, soweit sie das Gebiet der Technik im patentrechtlichen Sinn als durch den %(e:Einsatz beherrschbarer Naturkräfte) gekennzeichnet begreift und diesen zur unabdingbaren Voraussetzung der patentfähigen Erfindung erhebt.  Sie erlaubt einerseits sowohl die Einbeziehung anderer Naturkräfte als physikalischer und chemischer wie auch bisher unbekannter Naturkräfte, schließt andererseits aber die menschlichen Verstandeskräfte als solche Naturkräfte aus.  Wollte man die menschliche Verstandestätigkeit mit Rücksicht auf die primär biologisch-chemischen Vorgänge, die die gesamte Tätigkeit des Gehirns regeln und beeinflussen, den beherrschbaren Naturkräften zurechnen, so würde dies nichts anderes bedeuten als eine schrankenlose Öffnung des Erfindungsbegriffs, mithin eine Eröffnung des Patentschutzes für schlechthin alle Ergebnisse menschlichen Wirkens.  Denn die beiden anderen Elemente der Rote-Taube-Formel, die %(q:Planmäßigkeit) und %(q:kausale Übersehbarkeit), könntendieses Ergebnis in aller Regel nicht korrigieren, weil auch Regeln für geistige Tätigkeiten sich nahezu immer als Anweisungen für planmäßiges Handeln darstellen und kausal übersehbar sind.  Für die der streitigen Anmeldung zugrundeliegende Organisations- und Rechenregel räumt der Bundesgerichtshof dies ausdrücklich ein.  Daher drängen sich Zweifel auf, ob diese beiden Komponenten des Beggriffs der technischen Erfindung noch eine eigenständige, d.h. abgrenzungsrelevante Funktion haben.")
  "..."
  (ML Dei "Damit ist die entscheidende Aussage für eine sachgerechte und zuverlässige Abgrenzung der nicht-technischen Schöpfungen gewonnen.  Die beherrschbaren Naturkräfte sind ein Oberbegriff für alle Erscheinungen und Vorgänge, die durch eine nach unseren Erfahrungen und Erkenntnissen zwingende --- wenn auch nicht notwendigerweise stets erkannte --- Gesetzmäßigkeit zwischen Ursache und Wirkung charkterisiert sind.  Andere Formeln zur näheren Kennzeichnung der technischen Lehre im patentrechtliche Sinn wie etwa %(q:Nutzung von Materie und/oder Energie) oder %(q:Beeinflussung des Wechselspiels von Stoffen, Kräften und Energiearten) sind inhaltlich nichts anderes als definitorische Synonyme für eben diese beherrschbaren Naturkräfte.  Den Einsatz beherrschbarer Naturkräfte zur unabdingbaren Voraussetzung der patentfähigen Erfindung zu erheben, trifft daher dan Nagel auf den Kopf.")
)
(vers (ML Drv "Die Regeln für verstandesmäßige Tätigkeit")
(ML Dea "Die menschliche Verstandestätigkeit gehört jedenfalls nach den Anschauungen unserer Zeit nicht zu den %(e:beherrschbaren) Naturkräften.  Auch wenn die bei geistiger Tätigkeit ablaufenden physiologischen, kybernetischen und informatorischen Prozesse von Naturkräften, z.B. biologischen oder chemischen Kräften, beeinflusst werden, so ist -- unbeschadet der weitgehend noch fehlenden Erkenntnis der Kausalzusammenhänge -- die menschliche Verstandestätigkeit, das Denken in seiner Gesamtheit, eine andere Dimension, deren %(q:Gesetzlichkeit) zwar gegeben sein mag, die aber nicht als %(q:beherrschbar) im naturgesetzlichen Sinn angesehen und %(e:bewusst) den in der Natur waltenden Kräften gegenübergestellt wird.  Wenn der Bundesgerichtshof das menschliche Denken nicht dem Begriff der Technik zuordnen will, weil dieser damit seiner %(q:spezifischen und unterscheidenen Bedeutung) beraubt würde, so ist das keine willkürliche inhaltliche Begrenzung dieses Begriffs für den Bereich des Patentrechts, sondern eine konsequente Übernahme der Anschauungen, die sich in Naturwissenschaften und Technik selbst entwickelt haben.  Naturwissenschaftliches und technisches Denken wie überhaupt außerrechtliche Betrachtungsweisen pr/aejudizieren zwar nicht die Auslegung von Rechtsbegriffen,k sollten aber ohne Not nicht beiseite geschoben werden, zumal wenn es sich wie hier beim Patentrecht um ein Teilsystem des Privatrechts handelt, das für ein klar umrissenes Gebiet meschlichen Wirkens, na/mlich die industrielle Güterproduktion im weitesten Sinne, konzipiert worden ist und seine Ausformung bis heute durch ständige Interaktion mit der technischen Wirklichkeit und  Gedankenwelt erfahren hat.  Dies besagt freilich nicht -- um Missverständnissen vorzubeugen ---, dass der Richter dann auch blindlings zu akzeptieren hätte, was nach den in der Technik selber herrschenden Anschauungen als dem Gebiet der Technik zugehörend qualifiziert wird.")
(ML EiW "Eine undifferenzierte Einbeziehung aller Lehren, die eine geistige Tätigkeit des Menschen zum Gegenstand haben, in den Kreis der dem Patentschutz zugänglichen Neuerungen wäre daher nur dann möglich, wenn auf das Erfordernis, dass die patentfähige Erfindung eine Lehre zu technischem Handeln darstellen muss, gänzlich verzichtet würde. ...")
(filters ((mp ant (ML Mrr "Motive zum Patentgesetz, Verhandlungen des Deutschen Reichstags, 1877, Drucksache Nr. 8 S. 17 und Nr. 144 S. 5.  Auf den historischen Gesetzgeber beruft sich der BGH aber in der Wettschein-Entscheidung, GRUR 1968, 602"))) (ML lei "... Überzeugender wäre in diesem Zusammenhang ein Rückgriff auf den historischen Gesetzgeber gewesen, der zwar nie von %(q:Technik) oder gar %(q:technischer Erfindung) spricht, der aber, wie dies auch aus der in der %(mp:Gesetzesbegründung) -- freilich mit Rücksicht auf das heute als nicht mehr einschlägig erachtete Merkmal der gewerblichen Verwertbarkeit -- gezogenen Grenzlinie zwischen  patentfähigen und nicht patentfähigen Lehren hervorgeht, den Patentschutz schöpferischen Leistungen im Bereich der Technik, der gewerblichen Gütererzeugung, vorbehalten wollte.  Jedenfalls hege ich keinen Zweifel daran, dass sich die ständige Praxis der Patentbehörden und Gerichte, als patentfähig nur eine technische Erfindung anzuerkennen, aufgrund einer nahezu einmütigen Überzeugung und Billigung aller beteiligten Kreise über einen Zeitraum von nunmehr fast 100 Jahren zu einem gewohnheitsrechtlichen Grundsatz verfestigt hat.  Insoweit wäre allein der Gesetzgeber befugt, diese Patentierbarkeitsvoraussetzung zu beseitigen.  Solchen Willen hat aber der Bundestag auch bei der jüngsten Revision des Patentgesetzes durch das Gesetz über internationale Patentübereinkommen vom Juni 1976 nicht zu erkennen gegeben."))
(ML Deu "Der Bundesgerichtshof stützt sein Festhalten am Erfordernis des technischen Charakters der patentfähigen Erfindung schließlich darauf, dass der Begriff der Technik das %(q:einzig brauchbare Abgrenzungskriterium) gegenüber anderen Leistungen des Menschen sei und ein völliger Verzicht darauf zu einer Überschneidung mit dem sachlichen Geltungsbereich anderer Schutzsysteme führen müsse.  Dieses Argument ist in jeder Hinsicht überzeugend, selbst wenn man berücksichtigt, dass auch das Urheberrecht der ineinem Werk enthaltenen geistigen Leistung als solcher, z.B. einer neuen mathematischen Formel oder einem wissenschaftlichen Lehrsatz, keinen Schutz bietet.  Denn obwohl wir über ein recht umfassendes System von Ausschließlichkeitsrechten verschiedenster Art verfügen, bedeutet dies keineswegs, dass jeder schöpferischen menschlichen Leistung in irgendeinem dieser Schutzsysteme ein Platz eingeräumt werden müsste.  Würden nun aber die Lehren für verstandesmäßige Tätigkeiten schlechthin der Technik zugerechnet, wäre eben dies der Fall.  Das Patentgesetz würde, wie der Bundesgerichtshof es plastisch formuliert, zum %(q:Auffangbecken) für den Schutz jedweder geistigen Leistung, sofern sie nur neu und erfinderisch wäre.  Eine solche, auch nur mögliche Monopolisierung des Denkens ist uns fremd.  Schließlich ist ein lückenloses System des Ideenschutzes weder aus Gerechtigkeitsgründen geboten noch wäre es segensreich.  Denn selbst wenn viele Anmeldungen an den Hürden der Neuheit oder Erfindungshöhe  zu Fall kommen sollten, könnte kaum verhindert werden, dass ein qualitativ und quantitativ beträchtlicher Anteil von Geistesschöpfungen den Patentschutz erhalten würde, dessen Auswirkungen aber kaum zuverlässig abschätzbar sind und der mit der Sperrwirkung technischer Patente nicht vergleichbare Behinderungseffekte nach sich ziehen könnte.  Dass das im Ergebnis demnach berechtigte Festhalten am Erfordernis der %(q:technischen Erfindung) nicht verbietet, den Bereich des Technischen selber im Gefolge veränderter Anschauungen und  Verhältnisse behutsam zu erweitern und damit den Patentschutz für bisher nicht als technisch angesehene Neuerungen zu eröffnen, versteht sich von selbst.  Des Bundesgerichtshofs %(q:Rote Taube) ist ein Paradebeispiel dafür.")
 "..."
)
(fert (ML cra "%(q:Fertige) Lehre und Unmittelbarkeitsgrundsatz")
 (ML UWL "Um nun herausfiltern zu können, ob eine Lehre, die irgendwo auch den Einsatz technischer Mittel vorschlägt, tatsächlich technischer Natur ist oder in Wahrheit nur eine Anweisung für eine rein geistige Tätigkeit darstellt, führt der Bundesgerichtshof den interessanten Aspekt der %(q:fertigen Regel) ein.  Dieser Gedanke hat nichts mit der Frage nach dem Fertigsein der Erfindung zu tun, also ob etwa die Notwendigkeit weiterer Versuche der Patentierbarkeit einer Erfindung entgegensteht.  Er betrifft vielmehr allein das Problem der Kausalität des Mitteleinsatzes für den nach der Aufgabenstellung der Erfindung bezwecken Erfolg.  Hinter der Feststellung, dass die Anwendung einer als Erfindung beanspruchten Regel bzw Problemlösung, die bereits %(q:fertig) oder besser bereits vollzogen ist, %(e:bevor) es zu einem technischen Handeln kommt, nicht die Benutzung beherrschbarer Naturkräfte erfordert und daher der nachfolgende Einsatz technischer Mittel die beanspruchte Lehre grundsätzlich nicht in den Rang einer technischen Regel zu erheben vermag, verbirgt sich daher nichts anderes als die richtige Erkenntnis, dass eine Lehre eben nur dann technisch ist, wenn sie einen %(e:unmittelbaren) Einsatz beherrschbarer Naturkräfte erfordert, d.h. der Kausalzusammenhang zwischen Mitteleinsatz und Erfolg nicht unterbrochen ist.  Später präzisiert der Bundesgerichtshof denn auch seine Überlegung dahingehend, dass die Verwendung technischer Mittel %(q:Bestandteil der Problemlösung selbst) sein müsse und nicht entfallen dürfe, ohne dass zugleich der angestrebte Erfolg entfiele, und führt damit das strenge Äquivalenzprinzip der strafrechtlichen Kausalitätstheorie in das Patentrecht ein. ")      
 "..."
(ML Bfw "Begreift man das Wesen der technischen Erfindung als Lehre zum Einsatz beherrschbarer Naturkräfte, dann kann auf die weitere Bedingung, dass die erfindungsgemäße Regel unmittelbar die Benutzung solcher Naturkräfte zum Gegenstand haben und unmittelbar zum bezweckten Erfolg führen muss, nicht verzichtet werden.  Würde es nämlich für die Patentfähigkeit ausreichen, wenn im Zusammenhang mit der beanspruchten Lehre irgendwo auch technische Mittel benutzt werden, so würde der Begriff der Technik wieder %(q:seiner spezifischen und unterscheidenden Bedeutung) entkleidet und wäre damit als Abgrenzungsinstrument praktisch wertlos. ...")
(ML EWn "Einer der Hauptstreitpunkte bei der patentrechtlichen Diskussion war und ist die Frage, welche Bedeutung dem Umstand zukommt, dass datenverarbeitungsgerecht konzipierte, d.h. algorithmisierte REchenvorschriften ohne weiteres automatisierbar sind, also mit Hilfe eines Universalrechners nach entsprechender Programmierung ausgeführt werden können.  Jene, die solche Regeln als dem Gebiet der Technik zugehörig erachten, leiten dies aus dem Umstand her, dass die algorithmisierte Problemlösung ohne weiteres automatisierbar ist, nach entsprechender Umsetzung in ein Programm %(q:vollautomatisch und ohne weitere Einwirkung oder Mithilfe geistiger menschlicher Kräfte) abläuft.  Auch in anderem Zusammenhang neigt die Praxis dazu, dem Kriterium der Automatisierbarkeit %(q:patentbegründende) Bedeutung zuzuerkennen.  Jene hingegen, die die Patentfähigkeit von primär verstandesmäßig vorzunehmenden, gleichwohl automatisierbaren Operationen verneinen, argumentieren nicht zuletzt damit, dass der durch die Benutzung des Computers ermöglichte automatische Prozessablauf durch eine eigenständige Kausalkette in Gang gesetzt wird, also nicht unmittelbare Folge der algorithmisierten Problemlösung ist.")
(ML WdW "Wird der Unmittelbarkeitsgrundsatz ernst genommen, so kann nicht zweifelhaft sein, welche Betrachtungsweise die richtige ist.  Zwischen der Rechenregel und der sich anschließenden automatischen Lösung mit Hilfe des Computers besteht kein die Unmittelbarkeit begründender Kausalzusammenhang.  Daran ändert sich auch dann nichts, wenn die Verwendung einer Datenverarbeitungsanlage zweckmäßig ist oder gar -- wie dies bei den algorithmisierten Rechenvorschriften nahezu immer der Fall ist -- ihre Benutzung die einzige Möglichkeit darstellt, die jewilige Aufgabe überhaupt oder wenigstens in wirtschaftlich sinnvoller Weise zu lösen. ...")
(lin (ML Git "Grundsätzlich besteht jedoch kein Anlass, das Unmittelbarkeitsprinzip aufzugeben.  Eine bereingite, von Ballast befreite Definition der technischen Erfindung würde daher lauten:")
     (blockquote (ML Dre "Die technische Erfindung ist eine Lehre, wie beherrschbare Naturkräfte unmittelbar zur Erzielung eines bestimmten Ergebnisses eingesetzt werden sollen.")) )
"..."
)
(test (ML Pnt "Prüfungsmethode")
(ML Dtu "Die so herausgearbeitete Definition der technischen Erfindung kann ihre Abgrenzungsfunktion in der Praxis nur dann voll entfalten, wenn die Prüfung von Anmeldungen, deren technischer Charakter zweifelhaft ist, nicht rein schematisch-formal erfolgt, sondern der Eigenart solcher Anmeldungen, deren Ansprüche stets auch eine Reihe technischer Merkmale enthalten, Rechnung trägt.   ... In seinen wenigen zum Problemkreis der technischen Erfindung ergangenen Entscheidungen hat der Bundesgerichtshof zur Methode der Prüfung bisher nie Stellung genommen.  Im Dispositionsprogramm-Beschluss postuliert er nun erstmals für die Prüfung von Anmeldungen, deren Zugehörigkeit zum Gebiet der Technik nicht auf der hand liegt, zwei Regeln, die von großer praktischer Bedeutung sind und ungeteilte Zustimmung verdienen.")
(ol
 (ML Dde "Die %(e:erste Regel) besagt, dass solche Anmeldungen ungeachtet der Formulierung der Ansprüche und -- auch wenn dies nicht explicite ausgesprochen ist -- der gewählten Anspruchsart auf ihren wirklichen, materialen Gehalt zu untersuchen sind.  ... Formulierungskünste wie %(q:Datenverarbeitungsanlage ... dadurch gekennzeichnet, dass sie nach folgender Formel programmiert ist), %(q:Schaltungsanordnung ...), %(q:Verfahren zur Steuerung einer Datenverarbeitungsanlage ...), %(q:Verwendung einer Datenverarbeitungsanlage mit den Merkmalen X, Y, Z zu ...) usw werden also in Zukunft einer Anmeldung nicht mehr zum Patentschutz verhelfen können, wenn sich dahinter nur eine nicht-technische Rechenvorschrift verbirgt.  ...")
 (lin (ML AAe "Aus dieser grundsätzlich materialen Betrachtung erbibt sich beinahe zwanglos die %(e:zweite), noch wichtigere %(e:Prüfungsregel), dass Anmeldungen, die sowohl technische als auch nicht-technische Merkmale enthalten, nur dann patentfähig sind, wenn das als neu und erfinderisch Beanspruchte, also der Kern der Erfindung, im Technischen liegt.  In der Formulierung des Bundesgerichtshofs liest sich dies so:")
      (blockquote (ML led "... technische Merkmale in dem Patentanspruch können nur dann eine Patentierung rechtfertigen ..., wenn sich die erfinderische Neuheit in ihnen niederschlägt, nicht jedoch dann, wenn die Erfindung auf den nicht-technischen Teil der Lehre ... beschränkt bleibt."))
      )
)
 "..."
)
) )
(info (ML Iia "Informatik und Patentrecht")
"..."
(ML Snf "So verwundert es nicht, wenn in der jüngeren Diskussion um die Schutzfähigkeit der Software ein Begriff wie %(q:Software-Engineering) auftaucht, der die patentrechtliche Wertung gewissermaßen schon vorwegnimmt.  Solche Neuschöpfungen ändern freilich nichts daran, dass die Informatik mit ihren Arbeitsergebnissen unter immaterialgüterrechtlichem Blickwinkel im Grenzgebiet von Urheberrecht (Geisteswissenschaften), Patentrecht (Technik) und Niemandsland liegt und es daher einer sorgfältigen tatsächlichen und rechtlichen Analyse bedarf, ob eine konkrete Software-Schöpfung in den einen oder anderen Bereich fällt.")
"..."
(sects
(disp (ML Dka "Die Dispositionsprogramm-Entscheidung und ihre patentrechtliche Würdigung")
"..."
(ML HWd "Hierfür zieht der Bundsgerichtshof zwei Gesichtspunkte heran, die auch in der bisherigen Diskussion um den Patentschutz für Software-Schöpfungeneine hervorragende Stellung eingenommen haben: Zum einen die Überlegung, dass solche algorithmisierten, auf ADV-Unterstützung zugeschnittenen Organisations- und Rechenregeln im Zusammenhang mit den sog. %(q:Gebrauchsanweisungen) zu sehen sind, zum anderen den sich im Hinblick auf die Interdependenz von Hardware und Software aufdrängenden Aspekt, dass sich eine Korrektur deshalb als notwendig erweisen könnte, weil zwischen dem jeweiligen Algorithmus bzw Programm und den dadurch ausgelösten Schaltvorgängen in der Datenverarbeitungsanlage eine enge Korrelation besteht, die eine Äquivalenz von dauerhafter Festschaltung in Form eines Spezialrechners und programmeirtem Betrieb eines Universalrechners nahelegt.")
(sects
(gebr (ML Dae "Die Gebrauchsanweisungen")
)
(circ (ML DWh "Das Verhältnis von Programm und maschineller Schaltung")
"..."
(ML DWz "Die Überlegung, dass jedem Computerprogramm und weitergehend sogar einer algorithmisierten Rechenvorschrift ein bestimmter Schaltzustand bzw eine bestimmte Schaltfolge im Computer entspreche, die ihrerseits nicht anders behandelt werden dürften als eine Festschaltung in Form eines Spezialrechners für eben dieses Programm oder diesen Algorithmus, beruht in ihrem Kern auf der wohlbekannten Äquivalenzlehre und ist schon deshalb anfechtbar.  Die patentrechtliche Äquivalenztheorie ist als Hilfsmittel für die Feststellung der Erfindungshöhe und des Schutzumfangs einer patentierten technischen Lehre entwickelt wordne.  Ihre Anwendung bei der Prüfung, ob ein konkreter Vorschlag dem Patentschutz überhaupt zug/aneglich, also technischer Natur ist, verbietet sich schon deshalb, weil Prüfungsgegenstand hier allein das ist, was %(e:tatsächlich beansprucht und offenbart) ist, nicht aber das, was sein könnte.  Aber selbst wenn die Lehre von den patentrechtlichen Gleichwerten auch in diesem Zusammenhang brauchbar wäre, so könnte sie deie ihr zugedachte Stu/tzungsfunktion nur dann erfüllen, wenn ein Durchschnittsfachmann aus der bloßen Offenbarung des erfindungsgemäßen Algorithmus bzw eine speziellen Programms ohne weiteres und unmittelbar entnehmen könnte, wie die entsprechende schaltungstechnische Lösung z.B. durch den Aufbau eines Spezialrechners zu bewerkstelligen wäre.  Das ist aber, wie der 17. Senat selber nunmehr ausdrücklich und völlig zu Recht einräumt -- abgesehen vielleicht von ganz trivialen Fällen --, ohne erfinderisches Zutun nicht möglich.")
(ML TfW "Trotz der -- aus Sicht der Informatik -- grundsätzlich bestehenden Austauschbarkeit von Hardware und Software, die zur Annahme auch einer patentrechtlichen Gleichwertigkeit verführt, sind Computerprogrammierung und Computer-Engineering nach Ausgangssituation, Arbeitsweise und verwendeten Mitteln zwei Paar Stiefel.  Wie der Informatiker aus der Offenbarung eines komplexen Spezialschaltwerks nicht ersehen kann, wie er einen Universalrechner zu programmieren hätte, kann der Computeringenieur aus der bloßen Offenbarung eines Programms oder gar nur eines Algorithmus nicht ableiten, wie er einen Spezialrechner konstruieren müsste.  Da der Algorithmus als solcher niemals geschützter Gegenstand der Erfindung wäre, sondern nur die spezielle Vorrichtung mit ihren baulich-funktionellen Merkmalen, wäre im übrigen für die Patentprüfung wahre Aktrobatik vonnöten:  aus der Angabe des Algorithmus bzw der darauf aufbauenden spezifischen Steueranweisung in Form eines Programms projiziert der Prüfer als Durchschnittsfachmann gedanklich die ihm ja ohne weiteres mögliche Spezialschaltung, die er anschließend mit den im Stand der Technik vorgefundenen Schaltwerken, Computern etc. vergleicht!  Dass die Annahme einer generellen Äquivalenz ein Irrweg ist, lässt sich schließlich auch am umgekehrten Fall demonstrieren:  sie hätte nämlich zur Folge, dass jede Spezialschaltung mangels Erfindungshöhe nicht patentierbar wäre, wenn ein Universalrechner entsprechend programmiert werden könnte.  Das ist aber in aller in aller Regel der Fall.")
 "..."
)
) )
(noch (ML Pri "Patentschutz für Software: Bleibt noch ein Weg?")
"..."
(lin (ML DhW "Der Bundesgerichtshof gelangt zu der Schlussfolgerung:")
 (blockquote (ML DWg "Die Lehre, eine Datenverarbeitungsanlage nach einem bestimmten Rechenprogramm zu betreiben, kann .. nur patentfähig sein, wenn das Programm einen neuen, erfinderischen Aufbau einer solchen Anlage erfordert und lehrt oder wenn ihm die Anweisung zu entnehmen ist, die ANlage auf eine neue, bisher nicht übliche und auch nicht naheliegende Art und Weise zu benutzen.")) )
(ML Wlg "Wie aus dieser Formulierung und aus anderen Stellen der Entscheidungsbegründung hervorgeht, wird also die Grenze zur Patentierbarkeit erst in der Stufe der eigentlichen Programmierung überschritten, nicht schon im Bereich der dem jeweiligen Programm zugrundeliegenden algorithmischen Problemlösung.  Nach Auffassung des Bundesgerichtshofs wird das %(q:Reich des Technischen) erst mit der %(q:auf dem Algorithmus beruhenden Anweisung für den Betrieb) einer Datenverarbeitungsanlage betreten.  Die Unterscheidung zwischen der algorthmischen Problemlösung einerseits und ihrer Programmierung andererseits ist berechtigt.  Wie bereits früher dargelegt, ist ein Computerprogramm als fertige maschinengerechte Betriebsvorschrift %(e:nicht) gleichbedeutend mit dem Algorithmus, auch wenn dieser das Gewerbe des konkreten Programms bildet.  Selbst wenn ein durchschnittlicher Programmierer befähigt ist, einen vorgegebenen Algorithmus in eine Steueranweisung für eine bestimte Datenverarbeitungsanlage umzusetzen, so enthält die Rechenregel nicht auch und zugleich jede denkbare und mögliche Programmiervorschrift.  Vielmehr besteht in der Stufe der Programmierung ein wesentlicher Spielraum für mehr oder minder optimale, einfache und elegante Lösungen.  Algorithmus und Programm wären selbst dann nicht gleichzusetzen, wenn der Algorithmus unmittelbar in die Maschine eingegeben werden könnte, da in diesem Fall die Programmierungsfunktion von der Hardwarelogik, dem Betriebssystem und der Dienstsoftware übernommen werden, ohne dass dies am Charakter des Algorithmus etwas ändert.")
(sects
(algo (ML Aot "Algorithmen")
"..."
(ML Fsi "Für die Informatiker ist dieses Ergebnis allerdings mehr als betrüblich.  Tatsächlich stehen sie vor einem Trümmerhaufen, da sie für ihr schönstes Kind, die Algorithmen, praktisch keinerlei Ausschließlichkeitsschutz erhalten können.")
(ML Des "Das klingt zunächst paradox, sind doch die Algorithmen der eigentlich schöpferische Beitrag der Informatik oder, wie dies in der patentrechtlichen Diskussion betont worden ist, der %(q:allgemeine Erfindungsgedanke). ...  Wenn nun die Informatiker insbesondere auf eine schlaltungstechnische Realisierung neuer oder verbesserter Algorithmen verwiesen werden, so ist das wenig tröstlich für sie.  Denn abgesehen davon , dass die Hardware-Lösung allenfalls einen mittelbaren Algorithmenschutz bewirken kann, wird sie oft überaus aufwendig, umständlich, teuer und vor allem im Hinblick auf die durch den Algorithmus ermöglichte Programmierbarkeit eines beliebigen Universalrechners in vielen Fällen geradezu unsinnig sein.  Da der Urheberrechtsschutz für Algorithmen per se ebenso versagt und wettbwerbsrechtliche Ansprüche nur ausnahmsweise durchgreifen werden, sind die Informatiker letztlich ins Niemandsland geworfen.")
(ML Smf "Sucht man nach Möglichkeiten, wie den Algorithmen dennoch der Patentschutz auf breiter Basis offengehalten werden könnte, so wird nur ein einziger -- wie die Anmeldepraxis zeigt, stets erhoffter -- Ausweg aus diesem Dilemma sichtbar: Auf der einen Seite eine Lockerung des Unmittelbarkeitsgrundsatzes, wonach die technische Lehre unmittelbar zum Einsatz beherrschbarer Naturkräfte führen muss, auf der anderen Seite, gewissermaßen als Tribut für diese Wohltat, eine Beschränkung des sachlichen Schutzbereichs patentierter Algorithmen auf eine maschinelle Anwendung bzw Ausführung oder, weitergehend, auf die Anwendung mit einer spezifischen Maschinenkonfiguration.")
(ML Feu "Für eine solche Liberalisierung mag insbesondere sprechen, dass die meisten -- namentlich anwendungsorientierte -- Algorithmen ausschließlich im Zusammenhang mit einer Computeranwendung brauchbar und nützlich sind.  ...")
(ML Nlb "Nun, der Bundesgerichtshof hat sich, wie aus der eigentlichen Entscheidungsbegründung sowie den Überlegungen zu den Möglichkeiten einer Erweiterung des Begriffs der Technik oder eines völligen Verzichts hierauf klar hervorgeht, dieser Betrachtungsweise verschlossen und wird sich wohl auch in Zukunft kaum für sie erwärmen.  Auch dafür gibt es gute Gründe.  Zunächst würde ein weniger streng gehandhabter Unmittelbarkeitsgrundsatz, selbst wenn man ihn nur den datenverarbeitungsgerechten Rechenvorschriften zugutekommen lassen wollte, auf längere Sicht mit hoher Wahrscheinlichkeit dazu führen, dass die Patentierbarkeitsvoraussetzung der technischen Lehre ihre Abgrenzungsfunktion einbüßte und allen Lehren für verstandesmäßige Tätigkeiten Schritt für Schritt der Patentschutz eröffnet würde.  Vor allem aber sind erhebliche Zweifel angebracht, ob eine solche Ausnahme für das Gebiet der Informatik überhaupt vertretbar ist.  Die ADV ist heute zu einem unentbehrlichen Hilfsmittel in allen Bereichen der menschlichen Gesellschaft geworden und wird dies auch in Zukunft bleiben.  Sie ist ubiquitär.  Ist die Hardware-Industrie noch relativ leicht eingrenzbar, so gilt dies nicht mehr für die Software-Industrie, wo die Software-Hersteller ebenso sehr Anwender sind wie die überall zu findenden Software-Nutznießer auch Hersteller.  Ihre instrumendale Bedeutung, ihre Hilfs- und Dienstleistungsfunktion unterscheidet die ADV von den enger oder weiter begrenzten Einzelgebieten der Technik und ordnet sie eher solchen Bereichen zu wie z.B. der Betriebswirtschaft, deren Arbeitsergebnisse und Methoden -- beispielsweise auf den Gebieten des Managements, der Organisation, des Rechnungswesens, der Werbung und des Marketings -- von allen Wirtschaftsunternehmen benötigt werden und für die daher prima facie ein Freihaltungsbedürfnis indiziert ist.  Nach welchen Seiten hin auch immer aber ein patentrechtlicher Algorithmenschutz begrenzt würde -- durch die Bindung an eine bestimmte Maschinenkonfiguration oder sogar an eine ganz bestimmte Datenverarbeitungsanlage, durch den Anwendungszweck und das Anwendungsgebiet --, so ist doch die Gefahr offensichtlich, dass dieser Schutz eine weit über das mit herkömmlichen technischen Schutzrechten verbundene Maß hinausgehende Sperrwirkung entfalten und die Benutzung von Datenverarbeitungsanlagen nachhaltig blockieren könnte.  Das zugunsten der Patentfähigkeit von Algorithmen oft gehörte Argument, dass diese nur in Verbindung mit digitalen Rechenautomaten nützlich sind, hat eine Kehrseite, weil sich eben gerade aus dieser Tatsache ergeben kann, dass der patentierte Algorithmus dann die Benutzung von Computern überhaupt verwehrt, weil eine Substitution durch andere Mittel oder einen anderen Algorithmus nicht möglich oder nicht zumutbar ist.  So besehen erscheint der Denkansatz einer notwendigen %(q:Vergesellschaftung) der Informatik, zumindest der Algorithmen, durchaus plausibel, will man nicht den im Gefolge eines Algorithmenschutzes wahrscheinlichen ungeheueren privaten Machtzuwachs -- naiv oder bewusst -- leugnen.")
(ML DiW "Diese wenigen Gesichtspunkte zeigen, dass die Beantwortung der Frage, ob und in welchem Umfang datenverarbeitungsgerecht konzipierte Rechen- und Organisationsregeln und andere Lehren für verstandesmäßige Tätigkeiten einen Ausschließlichkeitsschutz genießen sollen, Aufgabe der Rechtspolitik ist.  Jedenfalls kann und darf es nicht Aufgabe der Gerichte sein, im Rahmen eines Einzelfalls eine rechtstechnisch und rechtspolitisch zwar vertretbare, in ihren Konsequenzen aber nicht überschaubare Entscheidung zu treffen.  Das hat der Bundesgerichtshof wohl erkannt und in der Begründung auch klar zum Ausdruck gebracht. Auch wenn nicht zu erwarten ist, dass der Gesetzgeber sich in absehbarer Zeit oder überhaupt mit dem Problem eines Ausschließlichkeitsschutzes für Algorithmen und ähnliche geistige Leistungen befassen wird, mag sich der Bundesgerichtshof den Vorwurf, konservativ, noch in der Gedankenwelt des 19. Jahrhunderts verhaftet zu sein, daher ruhig gefallen lassen.  Denn ein konservatives, Rechtssicherheit erzeugendes Urteil ist für alle Betroffenen immer noch besser als ein vermeintlich fortschrittliches, das sich später als Danaergeschenk erweist.") )
(prog (ML Cto "Computerprogramme")
 (ML Nps "Nach Auffassung des Bundesgerichtshofs kommt eine Patentierung von Lösungsmaßnahmen im Zusammenhang mit der ADV überhaupt erst in der Stufe der eigentlichen Programmierung in Betracht, d.h. dort, wo es um die auf einem Algorithmus beruhenden Anweisungen für den Betrieb einer Datenverarbeitungsanlage geht.  Diese bereits auf die patentrechtliche Wertung zugeschnittene Definition des Computerprogramms entspricht durchaus der in der Informatik gebräuchlichen und in DIN 44300 normierten Begriffsbestimmung, wonach ein Programm %(q:eine zur Lösung einer Aufgabe vollständige %(tpe:Anweisung:Arbeitsvorschrift) zusammen mit allen erforderlichen Vereinbarungen) ist. Wiederum wird es der Informatiker zunächst als paradox ansehen, dass die Patentierbarkeitszone erst im weniger schöpferischen, sich im Konstruktiven und Handwerksmäßigen bewegenden Bereich der Programmierung beginnen soll.  Vielleicht wird er sich mit der Überlegung trösten, %(q:Ist mein Algorithmus patentrechtlich nichts wert, so bekomme ich doch wenigstens Schutz für ein darauf aufbauendes Programm).  Wie der Informatiker jedoch alsbald feststellen wird, kommt er dabei vom Regen in die Traufe.")
 (ML Dte "Der Bundesgerichtshof beurteilt ein Computerprogramm als eine konkretisierte technische Gebrauchsanweisung für den Betrieb einer Datenverarbeitungsanlage, um mit ihrer Hilfe eine bestimmte Aufgabe zu lösen.  Dem ist zuzustimmen.  Jedes Programm hat als Ergebnis einer nach bestimmten, insbesondere durch die verwendete Programmiersprache vorgegebenen Regeln vorzunehmenden geistigen Ordnungstãigkeit zwar zunächst und unmittelbar keine technische Wirkung.  Seine Zweckbestimmung ist indessen mit der Vollendung des rein geistigen Schöpfungsakts und der schriftlichen Niederlegung nicht erreicht, da sein Adressat nicht der Mensch sondern eine Maschine ist.  Zweckbestimmung eines Computerprogramms ist es ausschließlich, nach Umsetzung in eine maschinenlesbare Darstellung, die eine Erkennung der Befehle durch elektromagnetische Signale und Impulse ermöglicht, die entsprechenden Schaltoperationen im Computer auszulösen und so zu steuern, bis das gewünschte Ergebnis erreicht ist.  Jedes Programm verkörpert daher die für den Betrieb einer Datenverarbeitungsanlage unabdingbare Steueranweisung, ohne die der Computer leere Hülle, tote Materie wäre, und ist somit in Übereinstimmung mit den oben dargelegten Grundsätzen %(e:echte Gebrauchsanweisung) für eine Vorrichtung, also Lehre zum technischen Handeln.")
 "..."
 (ML MWe "Mit der Qualifizierung des Programms als Lehre zum technischen Handeln ist freilich nichts gewonnen, weil die durch das Programm repräsentierte Gebrauchsanweisung nicht unabhängig von der Datenverarbeitungsanlage beurteilt werden kann und nur dann patentierbar wäre, wenn sie nicht mehr im Bereich des bestimmungsgemäßen Gebrauchs der nach ihr betriebenen Datenverarbeitungsanlage liegt.  Machen wir uns nun bewusst, dass die Universalrechner zur Lösung beliebiger Aufgaben konzipiert und ihre Einsatzmöglichkeiten nahezu unbegrenzt sind, dann erscheint diese Hürde unüberwindbar.  Schon hier wird sich jeder fragen, ob für die Patentierung von Computerprogrammen wirklich noch Raum ist.")
 (ML Der "Der Bundesgerichtshof weist zwei Wege für die Patentierung von Programmen:  Entweder den einer konstruktiven, schaltungstechnischen Lösung durch eine spezielle Schaltungseinheit oder Vorrichtung oder den der verfahrensmäßigen Lösung durch Angabe einer neuen, bisher nicht üblichen und nicht naheliegenden Betriebsvorschrift.  Beide Möglichkeiten schränkt der Bundesgerichtshof weiterhin dadurch ein, dass eine in der Problemanalyse und/oder in der Auffindung des Algorithmus liegende schöpferische Leistung patentrechtlich irrelevant ist, d.h. der darauf beruhenden speziellen Schaltung bzw Programmierungsmaßnahme weder Neuheit noch Erfindungshöhe vermitteln kann.  Diese Einschränkung ist mit Rücksicht auf die Tatsache, dass die Algorithmen nicht technischer Natur sind und deshalb auch als solche nicht Gegenstand der Erfindung sein können, ebenso konsequent wie die Feststellung, dass der fehlende technische Charakter der programmierten Rechenvorschrift der Patentfähigkeit eines bestimmten Programmierungsvorschlags nicht entgegensteht.  Die weitere Aussage schließlich, dass das Fehlen einer schöpferischen Leistung bei der Erstellung des Programms die %(q:Patentierung der auf das Programm bezogenen Betriebsanweisung) nicht zwingend ausschließt, ist nur für die zweite vom Bundesgerichtshof ins Auge gefasste Alternative der verfahrensmäßigen Lösung bedeutsam.  Sie ergibt nur dann einen Sinn, wenn man sie so versteht, dass eine an sich naheliegende, weil fachmännische Programmierungsmaßnahme jedenfalls dann nicht patenthindernd ist, wenn sie einen gewissermaßen %(q:überraschenden) Effektim Operationsablauf des Computers zur Folge hätte, d.h. eine irgendwie vorteilhafte Wirkungsweise, die aufgrund der Konzeption und Konstruktion der benutzten Datenverarbeitungsanlage nicht zu erwarten war.  Im HInblick auf den Zusammenhang zwischen Programm und bestimmungsgemäßem Gebrauch der Datenverarbeitungsanlage ist dieser Ansatz durchaus zutreffend, da eben Bezugspunkt allein der Computer und seine Arbeitsweise ist, nicht das Programm als solches, auf dessen Abweichung von vorbekannten Programmen es daher nicht ankommt.")
 (ML DcW "Damit sind wir aber schon bei der Kernfrage, die die vom Bundesgerichtshof entwickelte Konzeption eines möglichen %(q:Programm)-schutzes aufwirft: Ob nämliche die Verweisung auf die konstruktive, schaltungstechnische Lösung bzw die Angabe einer erfinderischen Gebrauchsanweisung für einen zum Stand der Technik gehörenden Computer für die Praxis eine realistische, brauchbare Alternative darstellt, um sich %(q:Programm)-Patente sichern zu können.  Ich glaube nein.")
 "..."
 (ML Mse "Mehr noch als dieser Weg scheint mir der zweite vom Bundesgerichtshof für möglich gehaltene Weg, für Programme über die %(e:Angabe einer neuen Gebrauchsanweisung) für den Computer Patentschutz zu erhalten, illusionär zu sein.  Die Patentierbarkeit einer solchen Gebrauchsanweisung setzt voraus, dass der Erfinder erkennt und angibt, auf welche neue, nicht naheliegende und fortschrittliche Art und Weise ein Computer zu steuern oder zu betreiben sei, um eine verbesserte Arbeits- oder Wirkungsweise des %(e:Computers selber) herbeizuführen.  Dass dies auch nur gelegentlich gelingen könnte, erscheint kaum denkbar.  Die Universalrechner der heutigen Generatiuon und auch schon früherer Generationen sind von der Logik und vom Design so konzipiert und strukturiert, dass sie zur Bewältigung der unterschiedlichsten Probleme eingesetzt und programmiert werden können.  Ihre schaltungstechnisch-funktionelle Flexibilität ist daher grundsätzlich unbegrenzt.  Eine geschickte Programmierung kann nun allerlei bewirken, z.B. eine Zeitersparnis, eine günstigere Speicherausnutzung und vieles andere mehr.  Eines aber kann sie nicht bewirken:  Dass der Computer selber auf neue Art und Weise arbeitet, also etwa Schaltungen aufbaut, die vom Hardware-Design her nicht vorgesehen oder möglich wären.  Die Vielfalt der möglichen Maschinenoperationen und Schaltkombinationen kann nicht zu einem anderen Ergebnis führen.  Hieraus folgt, dass ein Programm stets nur im Vergleich zu anderen Programmen anders oder besser sein kann, auf die Arbeitsweise und das Leistungsvermögen der benutzten Datenverarbeitungsanlage aber nicht verändernd einwirkt.")
(ML Dic "Die patentrechtliche Konsequenz ist lapidar: %(s:Jede Programmierung eines Computers bewegt sich immer im Rahmen des bestimmungsgemäßen Gebrauchs) der Anlage, wie er durch deren Systemstruktur und -funktion festgelegt und möglich ist.  Dies gilt für alle Typen von Programmen, also auch für die der Maschine näherstehenden Programme des Betriebssystems.  Die Vorstellung, dass ein spezielles Programm %(q:schlummernde) Fähigkeiten des Computers wecken könnte, findet in der technischen Wirklichkeit keine Stütze.  Selbst wenn ein solcher Ausnahmefall, d.h. eine die Arbeits- oder Wirkungsweise des Computers verändernde Gebrauchsanweisung via Programm denkbar wäre und auch erkannt (!) werden sollte, dann erscheint die Inanspruchnahme des Urheberrechtsschutzes für das maschinenlesbare Programmpaket erheblich zweckmäßiger als der kostspielige und langwierige Versuch, für das Programm ein Patent zu erhalten, das wegen des bloßen Verfahrensschutzes überdies geringen wirtschaftlichen Wert hätte.  Da nach der in Deutschland vorherrschenden Rechtsauffassung die maschinenlesbaren Computerprogramme mit zugehöriger Dokumentation urheberrechtlichen Schutz genießen, erübrigt sich insoweit auch eine nähere Untersuchung der Frage, ob eine Korrektur dahingehend geboten wäre, dass Programmpatente erteilt werden müssten, wenn sie zwar vom Computer selber nur bestimmungsgemäßen Gebrauch machen, sich aber von vorbekannten Programmen patentbegründend unterscheiden.")
)
)
)
)
) 
(sign (ML DnW "Die Bedeutung der Entscheidung für das künftige Patentrecht"))
(resu (ML Esu "Ergebnis und Ausblick")
 (ML Drh "Die Bilanz der Dispositionsprogramm-Entscheidung des Bundesgerichtshofs ist klar und ernüchternd.  Auf der Aktivseite ist praktisch nichts verblieben.  Versuche, Patentschutz für Software-Schöpfungen zu erhalten, haben keine Zukunft mehr.")      
 "..."
 (ML Ddr "Die vom Bundesgerichtshof vertretene Rechtsauffassung fügt sich nahtlos in die Rechtsentwicklung in den kontinentaleuropäischen Ländern ein, die für einen Patentschutz von Algorithmen und Computerprogrammen kaum mehr Raum lässt.  Der den algorithmisierten Rechen- und Organisationsvorschriften verweigerte Ausschließlichkeitsschutz durch Patente verschafft insbesondere den Herstellern von Datenverarbeitungsanlagen den aus ihrer Sicht notwendigen Freiraum, da die dem Hardware-Absatz sicher förderliche umfassende Nutzungsmöglichkeit der Computer durch Drittrechte nicht eingeschränkt werden kann, und wird von ihnen sicher begrüßt werden.  Anders sieht die Sache für die Informatiker und die Software-Hersteller, insbesondere die zahlreichen unabhängigen Service-Unternehmen aus, die an den von ihnen geschaffenen algorithmischen Problemlösungen keinen Ausschließlichkeitsschutz begründen können.  Weder das Urheberrecht noch das von der Weltorganisation für geistiges Eigentum vorbereitete besondere Schutzsystem für Computer-Software kann oder soll hier Abhilfe bringen.  Sie sind daher darauf angewiesen, für ihre verschiedenen Software-Produkte, namentlich maschinenlesbare Programme nebst zugehöriger Dokumentation, auf den Urheberschutz und den in Zukunft vielleicht verfügbaren Sonderschutz für Software zu vertrauen.")
;resu
)
)
; grur-kolle77
)
; swpatpapri-dir
)

(mlhtdoc 'swpatmrilu nil
nil nil

(filters ((ar ah "http://www.ffii.org/archive/mails/swpat/") (pl ahsti 'ffiiforum 'swpat)) (let ((SS (blockquote (verbatim "subscribe xxx@yyy.zz"))) (SR (code "swpat-request@ffii.org"))) (MLL WlW "Wenn Sie gemeinsam mit uns die informationelle Innovation vor dem Missbrauch des Patentwesens schützen oder über dieses Thema diskutieren möchten, können Sie unsere (hauptsächlich deutschsprachige) %(pl:E-Forum) (Mailingliste) abonnieren, der auch %(ar:archiviert) wird.  Angenommen Sie haben die E-Post-Adresse %{XY}, dann können sich zur Teilnahme im E-Forum %{SF} anmelden, indem Sie im E-Brieftext die Zeile %{SS} an %{SR} senden." "If you want to keep the infosphere free of patents or would like to discuss about this subject, you can subscribe to our (German-speaking) %(pl:mailing list), which is also %(ar:archived).  You can subscribe by mailing the line %{SS} to %{SR}.")))

(filters ((ii ah "http://no-patents.prosa.it") (pm ah "http://liberte.aful.org/mailman/listinfo/patents")) (ML Fte "Ferner gibt es einen %(pm:englisch- und französischsprachigen Verteiler) über die Softwarepatentthematik sowie den recht aktiven Verteiler der %(ii:Italienischen Initiative gegen Softwarepatente)." (en "Moreover, there is an %(pm:English and French speaking mailing list) about this subject as well as a very active mailing list of the %(ii:Italian Initiative against Software Patents).")))
)
; swpatvreji-dir 
)


	 

