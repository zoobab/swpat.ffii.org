<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: In 1997, with priority date 1994, an obscure canadian software company received a european patent on a basic information processing method, namely the idea of describing information by hierarchies of binary relations.  In 1999 a communication protocol called Ressource Description Framework (RDF) was adopted as a web standard.  In 2001, when some software applications gradually became available, a license collecting company started enforcing the patent, as a first step threatening 50 companies with infringement litigation.
title: RDF: web standard threatened by basic patent
Oro: One of the first articles on the subject
SWp: Specification of an XML based Protocol for describing Web Contents.  A result of collaborative work of several companies, propagated by the %{W3C} as a standard since 1999.
AWr: A license enforcement agency which the patentee entrusted to maximise revenues from the patent.  Pearl kept silence for several years and then sent an extortion letter to 50 companies after the patented method had been incoroporated into a standard.
oWi: one of the victims
TsW: The UDTL webserver is no longer found

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swxai-rdf ;
# txtlang: en ;
# End: ;

