
   Method for supporting interaction between two units  
   Patent Number: EP0825526
   Publication date: 1998-02-25
   Inventor(s): HAUW LINDA HELENE (FR); CARRE LAURENT (FR)
   Applicant(s):: ALSTHOM CGE ALCATEL (FR)
   Requested Patent: [_]  [1]EP0825526,  [2]B1
   Application Number: EP19960440064 19960820
   Priority Number(s): EP19960440064 19960820
   IPC Classification: G06F9/00 ; G06F9/46
   EC Classification: G06F9/46M
   Equivalents: AU3426197, AU718933, DE59604241D, ES2141457T, [_]
   [3]JP10171658
     _________________________________________________________________
   
   Abstract
     _________________________________________________________________
   
   A first device is specified according to a first description language
   and a second device is specified according to a second description
   language. Respective classes are provided to the two devices which
   control the processing of types. The first and second devices have one
   or more hybrid classes. These control both the processing of a type
   specified according to the first language and also a processing of a
   corresponding type converted into the second language. The first
   language may be ASN.1. The second language is CORBA-IDL. The hybrid
   class or classes carry out a conversion between types according to the
   first language and type according to the second language.
     _________________________________________________________________
   
   Data supplied from the esp@cenet database - l2

References

   1. file://localhost/dips/bnsviewer?CY=ep&LG=en&DB=EPD&PN=EP0825526&ID=EP+++0825526A1+I+
   2. file://localhost/dips/bnsviewer?CY=ep&LG=en&DB=EPD&PN=EP0825526&ID=EP+++0825526B1+I+
   3. file://localhost/dips/bnsviewer?CY=ep&LG=en&DB=EPD&PN=JP10171658&ID=JP+410171658A++J+
