
   COMPUTER VIRUS TRAP  
   Patent Number: [_]  WO9533237
   Publication date: 1995-12-07
   Inventor(s): KLEMMER TIMOTHY J; SCHNURER JOHN
   Applicant(s):: QUANTUM LEAP INNOVATIONS INC (US)
   Requested Patent: [_]  EP0769170 (WO9533237),  A4,  B1
   Application Number: WO1995US06659 19950530
   Priority Number(s): US19940252622 19940601
   IPC Classification: G06F11/00 ; G06F17/00
   EC Classification: G06F9/455H ; G06F11/00R6
   Equivalents: CA2191205, DE69511556D, JP10501354T
     _________________________________________________________________
   
   Abstract
     _________________________________________________________________
   
   A computer virus trapping device (10) is described that detects and
   eliminates computer viruses before they can enter a computer system
   and wreck havoc on its files, peripherals, etc. The trapping device
   (10) creates a virtual world that simulates the host computer system
   (28) intended by the virus to infect. The environment is made as
   friendly as possible to fool a computer virus into thinking it is
   present on the host (28), its intended target system. Within this
   virtual world, the virus is encouraged to perform its intended
   activity. The invention is able to detect any disruptive behaviour
   occurring within this simulated host computer system. It is further
   able to remove (52) the virus from the data stream before it is
   delivered to the host (28) and/or take any action previously
   instructed by a user (38).
     _________________________________________________________________
   
   Data supplied from the esp@cenet database - l2
