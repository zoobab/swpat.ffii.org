                           1 3             EP O 688 450 B1             1 4
(1)  Untersuchen  des  Blockzuord-          Aktualisieren des zweiten virtuellen Verzeich-
nungsverzeichnisses (25) von wenig-          nisses, das in dem Direktzugriffsspeicher (1 6)
stens einer anderen der Einheiten, um          gespeichert ist, derart, da� es die Seitenadres-
eine unbeschriebene Blockadresse zu          se des ge�nderten seitenadressierbaren Blok-
identifizieren,                 5       kes in der unbeschriebenen physikalischen
(2) Schreiben der Daten in den Spei-          Blockzuordnung, in welche der ge�nderte sei-
cher (1 2) zu der  unbeschriebenen          tenadressierbare Block geschrieben worden
Blockadresse,                        ist, verzeichnet.
(3) �ndern des Blockzuordnungsver-
zeichnisses (25) f�r einen Block in der  IO  3.  Speicherverwaltungsverfahren nach Anspruch 2,
Einheit, in welchem die virtuelle Adres-       umfassend die Schritte.
se (29) in Schritt (a) verzeichnet wor-       Einschreiben von Daten in den Speicher (1 2) an ei-
den ist, um sie als gel�schte physika-       ner virtuellen Adresse (29) durch.
Iische Blockadresse anzuzeigen,
(4) �ndern des Blockzuordnungsver-  15       (a) Ableiten einer Seitenadresse aus einer vir-
zeichnisses (25) f�r einen Block in der          tuellen Adresse (29),
Einheit, in welche die Daten in Schritt          (b) Verzeichnen der Seitenadresse in einem
(c) (2) geschrieben worden sind, um          seitenadressierbaren Block in dem Speicher
sie als  geschriebene,  zuvor unbe-          (1 2),
schriebene Blockadresse, an welcher  20       (c) Lesen eines Segmentes des ersten virtuel-
die Daten geschrieben worden sind,          len Verzeichnisses, das eine virtuelle Adresse
anzuzeigen,                         (29) in einer physikalischen Adresse (37) ver-
(5) �ndern des virtuellen Verzeichnis-          zeichnet,
ses (31), um virtuelle Adressen (29) in          (d) Verzeichnen der virtuellen Adresse (29) in
physikalische Adressen (37) in einer  25       einer physikalischen Adresse (37),
Einheit zu verzeichnen, derart, da�          (e) wenn sich der Block an der physikalischen
das virtuelle Verzeichnis (31) die virtu-          Blockadresse in einem geschriebenen oder ge-
elle Adresse (29) in die physikalische          l�schten Zustand befindet.
Adresse (37) des zuvor unbeschriebe-
nen Blockes, in welchen die Daten in  30          (1) Schreiben der Daten in den Speicher
Schritt (c)  (2)  geschrieben worden            (1 2)  an  einer  unbeschriebenen  Block-
sind, zu verzeichnen, derart, da� die            adresse,
virtuelle Adresse nicht in der physika-            (2) �ndern des ersten virtuellen Verzeich-
Iischen Adresse (37) des geschriebe-            nissegmentes, derart, da� das erste virtu-
nen Blockes des Schrittes (c) ver-  35          elle Verzeichnis die virtuelle Adresse (29)
zeichnet wird.                           in der physikalischen Adresse (37) des un-
beschriebenen Blockes, in welchem die
2.  Speicherverwaltungsverfahren nach Anspruch 1,            Daten in Schritt (e) (1) geschrieben worden
umfassend die Schritte.                            sind, verzeichnet,
40          (3) Schreiben des ge�nderten ersten virtu-
Speichern eines ersten virtuellen Verzeichnis-            ellen Verzeichnissegments aus Schritt (e)
ses in dem Speicher (1 2), das virtuelle Adres-            (2) in einen unbeschriebenen physikali-
sen (29) in physikalische Adressen (37) in einer            schen Blockplatz in dem Speicher (1 2),
Einheit abbildet,                              (4) Aktualisieren der in dem Direktzugriffs-
Organisieren des in dem Speicher (1 2) gespei-  45         speicher (1 6) gespeicherten zweiten Ta-
cherten  ersten virtuellen  Verzeichnisses  in            belle, derart, da� sie die Seitenadresse
Segmente von seitenadressierbaren Bl�cken,             des ge�nderten ersten virtuellen Verzeich-
Speichern eines zweiten virtuellen Verzeichnis-            nissegmentes des unbeschriebenen phy-
ses in einem Direktzugriffsspeicher (1 6), das            sikalischen Blockplatzes verzeichnet.
Seitenadressen in physikalische Adressen der  50
seitenadressierbaren Bl�cke in dem Speicher     4.  Speicherverwaltungsverfahren nach einem der An-
(1 2) abbildet,                           spr�che 1 bis 3, bei welchem das Verzeichnen der
�ndern eines seitenadressierbaren Blockes in       virtuellen Adresse (29)  in  einer physikalischen
dem ersten virtuellen Verzeichnis, das in dem       Adresse (37) eine Zwei-Schritt-Adressen�berset-
Speicher (1 2) gespeichert ist, durch Schreiben  55    zung umfa�t.
eines ge�nderten seitenadressierbaren Blok-
kes  in  eine  unbeschriebene  physikalische          �bersetzen der virtuellen Adresse (29) in eine
Blockzuordnung, und                         logische Einheitsadresse (33) durch das virtu-
8
