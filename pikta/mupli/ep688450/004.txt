                           5             EP O 688 450 B1             6
ory 1 6. It will be appreciated that controller 1 4 functions     be written into.
may be carried out in software, firmware of hardware,       Since the data moves physically during a unit trans-
and would not necessarily eKist as a physically separate     fer to an unwritten unit space, the units are assigned
unit as the drawing suggests.                    Iogical unit numbers that do not change with changes in
Referring now to Figure 2, it illustrates in part the  5  the physical location of the unit in the memory. A virtual
organization of the flash memory. The flash memory has     map 31 maps block numbers in the virtual address to
a number of zones labeled here as zone A, zone B, etc.     Iogical block address in the first step of a two level ad-
Each zone is comprised of a number of contiguous     dress translation. The logical unit address is an address
physical memory locations that can be block erased us-     relative to a logical unit number, similar to a physical ad-
ing conventional, well known, flash memoy technology.  IO  dress, which is an address relative to a physical unit
The zones are organized as units only four of which are     number. The logical unit number is the high order binay
shown, labeled in the drawing as; UNIT _1, UNIT _6,     digits of the logical address and may be derived from
UNIT N-1 and TRANSFER UNIT. Each unit is comprised     the logical address by a bit shift operation. The logical
of at least one zone, of a plurality of contiguous zones.     address 33 obtained from map 31 includes a logical unit
Here illustrated, each unit is comprised of two zones (i.  15  number along with the offset of the block within the unit.
e., UNIT _1 - zone A and zone B; UNIT _2 - zone C and       A logical unit table 35 translates the logical unit
zone D, TRANSFER UNIT - zone K2 and 2K).          number to a physical unit number for the logical unit.
Each unit is comprised of an integral number of ad-     This two-step address translation procedure obviates
dressable blocks and each block, in turn, is comprised     the need to change block addresses in the map when a
of a contiguous, fiKed length group of bytes. At all times,  20  unit is moved to a new physical location.
there will be a unit in the memory 1 2 that is unwritten (i.       In a read operation the virtual address 29 comprised
e., TRANSFER UNIT), so that active blocks in a unit that     of a block address, for eKample, initially is mapped to a
is to be erased can be written to this unwritten unit prior     logical unit number and a block offset within the unit in
to erasing the unit.                          the addressed block. Map 35 maps the unit number 33
Referring now to Figure 3, each unit contains an in-  25  to a physical address 37 for the unit along with the offset
tegral number of contiguous data blocks 21 that are in     of the addressed 37 block within the unit, and the ad-
turn comprised of contiguous byte addresses, that can     dressed data block is read from this physical location.
be addressed as a block number and offset within the     Here it is assumed that data is read and written on a
block. Each block in a unit has a unit can be addressed     block basis as is typically done. Of course, data could
by block number and offset with the unit. Each unit has  30  be written and read on a byte basis using the same prin-
a unit header 23 and a map 25 of the allocation status     ciple, if desired. Figure 5 is a flow diagram illustrating
of each block in the unit. The unit header 23 includes a     this read operation. As previously eKplained, the virtual
format identifier, and the logical unit number of the unit.     address 29 is mapped to a logical address (block 40) in
Because data must move physically during a unit trans-     the first step of a two-step address translation. In the
fer, the unit number preferably remains unchanged even  35  second step, the logical address is mapped to a physical
as the physical location of the unit in the flash memory     address in the flash memory, block 41. Data at this phys-
1 2 changes. In addition, the header may also include     ical address is read, block 42, which terminates this op-
system-wide information. The block allocation map 25     eration.
has a word for each block that denotes its status and its       ln a write operation, the virtual address 29 is again
offset in the unit. The status indications are. ''block free  40  mapped initially to a logic unit number and a block offset
and writable''; ''block deleted and not writable''; ''block     within the unit. The controller 1 4 algorithm eKamines the
allocates and contains user data''; and virtual address     block allocation map 25 for this unit. If the block corre-
of the block (back pointer).                      sponding to this address has been written, a write com-
As previously mentioned, preferably each unit is as-     mand cannot be eKecuted at the corresponding physical
signed a logical unit number that does not change, even  45  address. The control algorithm scans the block alloca-
though the physical location in the memory of the unit     tion maps 25 for each unit until a free block is located.
change. As illustrated in Figure 4, the computer 1 O gen-     The status of the block in the block map 25 at the original
erated addresses 29 are comprised of a block number     unit address is changed to deleted in the block in the
and a block offset. These address are interpreted by the     allocation map, and the status of the free block is
flash controller 1 4 as virtual addresses, and a virtual  50  changed to written. The virtual map 31 is update so that
map is used to establish a correspondence between the     the original virtual address now points to the new logical
virtual address space and physical address space. The     address where the write operation is to take place. This
virtual map changes as blocks are rewritten an the vir-     logical address is mapped to a physical address, in the
tual address space is therefore dynamic. It should be     manner previously described, and the block is written to
noted also that, at any given time, a block or blocks in  55  this address. Figure 6 is a flow diagram illustrating this
the virtual address space may be unmapped to the     write operation. In a write operation the virtual address
physical address space, and that blocks in the physical     29 is mapped to a logical unit address, block 45, and the
address space may be unwritten and, therefore, free to     unit allocation for the unit is eKamined, block 46. If in
4
