                           1             EP O 688 450 B1             2
Description                              of an allocation unit to a physical address of a sector
(allocation unit) within the memory. A further table is
BACKGROUND OF THE INVENTION               present for providing information whether a block is an
active or a reserve block, and whether the block is free
Field of the Invention                       5  or in use. A clean-up operation involves a reallocation
operation. Thereby all currently active files are moved
This invention relates to an improved system for     out of blocks that will eventually be erased. The mapping
storing and retrieving information in flash memories, and     table is a linked list, each linked-list chain within the
more particularly to a system that organizes and man-     mapping table representing a historical record of the
ages data written to a flash memoy.             IO  creation and removal of dirty sectors.
Descri tion of the Prior Art                      SUMMARY OF THE INVENTION
As will be appreciated by those skilled in the art,       An object of this invention is the provision of a meth-
electrically  erasable  and  programmable  read-only  15  od (i.e., software, firmware of hardware) to control and
memories (EEPROMs) comprised of flash-type, float-     manage access to a flash memoy so that the flash
ing-gate transistors have been described in the art and     memoy appears to the computer operating system as
are presently commercially available. These so-called     a data storage device in which it is possible to read data
flash memories are non-volatile memories similar in     from, and write data to, any flash memoy location. A
functionality and performance to EPROM memories,  20  method that allows flash memory to emulate random ac-
with an additional functionality that allows an in-circuit,     cess memories and allows eKisting computer operating
programmable, operation to erase blocks of the memo-     systems to provide all other required support in the
ry. In a flash memory, it is not practical to rewrite a pre-     same manner provided by standard random access
viously written area of the memory without a preceding     memories and independent of the emulation method.
block erase of the area. While this invention will be de-  25     Briefly, this invention contemplates the provision of
scribed in the conteKt of a flash memoy, those skilled     a flash memory, virtual mapping system that allows data
in the art will understand that its teachings are also ap-     to be continuously written to unwritten physical address
plicable to data storage devices with the same write,     locations. The virtual memoy map relates flash memory
read, and block erase before write characteristics as     physical location addresses in order to track the location
flash memories.                         30  of data in the memory.
In a typical computer system, the operating system       The flash memory physical locations are organized
program is responsible for data management of the data     as an array of bytes. Each of the bytes in the array is
storage devices that are a part of the system. A neces-     assigned a number of address by means of which the
sary, and usually sufficient, attribute of a data storage     byte is physically accessible, referred to herein as the
device to achieve compatibility with the operating sys-  35  physical address space. Each of the bytes in the array
tem program is that it can read data from, and write data     has a second address, called the virtual address space.
to, any location in the data storage medium. Thus, flash     A table, called a virtual map, converts virtual addresses
memories are not compatible with typical eKisting oper-     to physical addresses. Here it should be noted, the vir-
ating system programs, since data cannot be written to     tual address space is not necessarily the same size as
an area of flash memory in which data has previously  40  the physical address space.
been written, unless the area is first erased.              A contiguous, fiKed-length group of physical byte
Software products have been proposed in the prior     addresses from a block. For eKample, assuming a block
art to allow a flash memoy to be managed by eKisting     size of 51 2 bytes, a byte with a physical address of
computer operating programs without modification of     25621 1 is bytes number 21 1 in block 500 (25621 1. 51 2
the operating system program. However, these prior art  45  = 500 + 21 1). One or more physically contiguous flash
programs operate the flash memory as a ''write once     memoy areas (called zones) that can be physically
read many'' device. This prior art software product can-     erased using suitable prior art flash memoy technology
not recycle previously written memory locations. When     comprise a unit and each unit contains an integral
all locations are eventually written the memoy cannot     number of blocks.
be further used without specific user intervention.     50    The virtual memory map is a table in which the first
GB-A-2 251 323 discloses a flash memory that is     enty belongs to virtual block O, the second to virtual
block erasable and that includes an active blockfor stor-     block 1, and so on. Associated in the table with each
ing first data and a reserve blockfor storing second data     virtual block address there is a corresponding physical
which is a copy of the first data made during a clean-up     address. In a read from the flash memoy operation, a
operation prior to erasure of the active block. The re-  55  computer generated address is decoded as a virtual
serve blocks are blocks that provide temporary file back-     block address and a byte location within the block. The
up during said clean-up operation. The flash memory     virtual memory map is used to convert the virtual block
includes a mapping table for mapping a logical address     address to a physical block address; the byte location
2
