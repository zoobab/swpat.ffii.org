          ,,,,   o    ;;;o;;;; '___,;',e;;.,;; '               lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll
Office europ�en des brevets             (,,)      E _ O 688 450 B _
(1 2)                EUROPEAN PATENT SPECl FICATION
(45) Date of Publication and mention             (51) Int CI.6. G06F 1 2I02, G06F 3I06
of the grant of the patent.
1 1 _1 1 _1 998  BUllet,; 1 998146              (86) International application number.
PCTIUS94I01 848
(21) Application number. 9491 01 45.5             (87) lnternational publication number.
(22) Date of filing_ 28_02_1 994                    Wo 9412o9o6 (1 5.o9.1 994 Gazette 1 994121)
(54) FLASH FILE SYSTEM
SCHN ELL LOESCHBARE DATEl
SYSTEME DE MEMOIRE FLASH
(84) Designated Contracting States.             (72) lnventor. BAN, Amir
DE FR GB IT NL                        62309 Tel Aviv (IL)
(30) Priority. 08.03.1 993 US 211 31             (74) Representative. Vossius, Volker, Dr. et al
Dr. Volker Vossius,
(43) Date of publication of application.               Patentanwaltskanzlei - Rechtsanwaltskanzlei,
21.1 2.1 995  Bulletin 1 995152                 Holbeinstrasse 5
81 619 M�nchen (DE)
(73) Proprietors.
_ M-SYSTEMS LTD.                    (56) References cited.
61 580 Tel Aviv (IL)                       EP-A- O 522 180       GB-A- 2 251 323
_ M-SYSTEMS INC.                        US-A- 4 51 1 964       US-A- 5 1 93 1 84
Melville, NY 1 1 141 (US)                    US-A- 5 21 O 866       US-A- 5 301 288
__
O_
_
OOoo
_   Note. Within nine months from the Publication of the mention of the grant of the EuroPean Patent, anY Person maY give
o   n OtiCe tO the EUrOPean Patent OffiCe Of OPPOSiti On tO the EUrOPean Patent 9ranted. NOtiCe Of OPPOSiti On Shall be filed in
_   a written reasoned statement. It shall not be deemed to have been filed until the opposition fee has been paid. (Art.
w   99(1) European Patent Convention).      Printed by Jouve, 75001 PARIS (FR>
