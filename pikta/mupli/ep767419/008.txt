                           1 3             EP O 767 41 9 B1             1 4
ment or disablement of the display feature of the present     table storage media (e.g. read only memoy devices
invention.                                within a computer such as ROM 58 or CD-ROM disks
[O054]  Referring again to decision block21 2, if the vis-     18 readable by a computer IIO attachment); (b) infor-
ible space in the first window falls below a predeter-     mation alterably stored on writable storage media (e.g.
mined minimum amount of space so that the information  5  floppy disks 14 and hard drives 12); or (c) information
cannot be reconfigured for display in such minimal     conveyed to a computer through communication media
amount of window space, the process determines     such as network 94 and telephone networks via modem
whether or not the first window can be moved to an avail-     92. It should be understood, therefore, that such media,
able space in the display area, as illustrated at block     when carying computer readable instructions that di-
21 6. If the first window cannot be moved to an available  IO  rect the method functions of the present invention rep-
space in the display area, the process iteratively returns     resent alternate embodiments of the present invention.
to block 202 for detecting any changes in the display of     [O058]  One such embodiment is drawn to a computer
windows in the display screen. If, however, the first win-     program product comprising. a computer usable medi-
dow can be moved to an available space in the display     um having computer readable program code and com-
area, the information display software utilizes informa-  15  puter readable system code embodied on said medium
tion received from the system regarding available dis-     for displaying previously obscured information, said
play space to resize andlor relocate the first window in     computer program product including. computer reada-
the available display space in the display screen, as de-     ble program code means for displaying information with-
picted at block 21 8. Thereafter, the process returns to     in a first window in said display; computer readable sys-
block 202.                             20  tem code means for detecting a second window dis-
[O055]  While the invention has been described and il-     played in said display at a location that obscures a por-
Iustrated above with teKtual and graphical information,     tion of said information displayed in said first window;
other forms of information displayed within a window     computer readable system code means for notifying
may be reconfigured to become visible after being over-     said computer readable program code means for dis-
Iapped by another window. These other forms of infor-  25  playing information within a first windowthat said portion
mation may include various buttons or tool palettes or     of said information within said first window is obscured
other controls, video display areas, or areas containing     by said second window; and computer readable pro-
animated graphics. And while the invention has been     gram code means for displaying in said first window said
described and illustrated with reference to only two win-     portion of said information that had been obscured by
dows, information displayed in one window may be con-  30  said second window, wherein said information in said
figured around a plurality of windows. Similarly, a win-     first window previously obscured by said second win-
dow may be moved or resized to a location that is avail-     dow may be viewed in said first window by a data
able in a screen having multiple windows. Moreover,     processing system user.
available window space need not be limited to space     [O059]  Further, this embodiment involves the compu-
that does not contain an additional window - available  35  ter program product wherein the computer readable
space may include an area of the screen that would     system code means for notifying said computer reada-
overlay other windows that have been placed lower in     ble program code means for displaying information with-
a Z-order in response to current conditions in the data     in said first window that said portion of said information
processing system. In this situation, the process may     within said first window is obscured by said second win-
determine that a window that is being relocated or re-  40  dow further comprises computer readable system code
sized according to the present invention is more impor-     means for sending a message from said computer read-
tant than other windows that may eKist further down in     able system code means to said computer readable pro-
the Z-order.                               gram code means for displaying information within said
[O056]  The method embodiments of the present in-     first window, wherein said message includes data that
vention also include sending a message from the oper-  45  represents said portion of said information within said
ating system to information display software, where the     first window is obscured by said second window.
message includes data that represents the portion of the     [O060]  The computer readable program code means
information within the first window that is obscured by     for displaying in said first window said portion of said
the second window.                          information that had been obscured by said second win-
[O051]  As indicated above, aspects of this invention  50  dow further comprises computer readable program
pertain to specific ''method functions'' implementable on     code means for moving said portion of said information
computer systems. In an alternate embodiment, the in-     that had been obscured by said second window to a lo-
vention may be implemented as a computer program     cation within said first window that is not obscured by
product for use with a computer system. Those skilled     said second window.
in the art should readily appreciate that programs defin-  55  [O061]  The computer program product can further in-
ing the functions of the present invention can be deliv-     clude computer readable program code means for de-
ered to a computer in many forms; including, but not lim-     termining that the area of a visible portion of said first
ited to. (a) information permanently stored on non-wri-     window falls below a predetermined threshold; compu-
8
