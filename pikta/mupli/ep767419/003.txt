                           3             EP O 767 41 9 B1             4
provide a method and system for displaying information       Figure 5 depicts the reconfiguration of information
in a data processing system.                       within the window shown in Figure 4 after an over-
[O01 2]  lt is yet another object of the present invention       laying window has been resized, in accordance with
to provide a method and system for displaying informa-       the method and system of the present invention;
tion in windows in a graphical user interface.         5
[O01 3]  The foregoing objects are achieved as is now       Figure 6 illustrates a windowfor displaying informa-
described. In a data processing system having a display       tion with the display feature of the present invention
and an operating system, information is displayed within       disabled;
a first window utilizing information display software.
Thereafter, the process detects a second window dis-  IO     Figure l depicts a title bar having means for ena-
played within the display at a location that obscures a       bling and disabling the display function of the
portion of the information displayed in the first window.       present invention;
Utilizing the operating system, the process notifies the
information display software that the portion of the infor-       Figure 8 illustrates a window tiling feature in ac-
mation within the first window is obscured by the second  15    cordance with the method and system of the
window. In response to receiving this information, the       present invention;
information display software displays, in the first win-
dow, the portion of the information that had been ob-       Figure 9 illustrates another eKample of a window
scured by the second window, wherein the information       tiling feature in response to resizing a second win-
in the first window previously obscured by the second  20    dow in accordance with the method and system of
window may be viewed in the first window by the data       the present invention; and
processing system user. Information displayed in the
first window may be teKtual or graphical. The information       Figure 1 O depicts a high-level flowchart illustrating
display software may also receive information from the       the process of displaying information in a window in
system that specifies coordinates of available display  25    accordance with the method and system of the
area. In response to predetermined conditions, previ-       present invention.
ously obscured information may be displayed in availa-
ble display area in a relocated first window.            DETAILED DESCRIPTION OF THE PREFERRED
EMBODIMENT
BRIEF DESCRIPTION OF THE DRA_INGS        30  [O01 5]  With reference now to the figures, and in par-
[O01 4]  The novel features believed characteristic of     ticular to Figure 2, there is depicted a data processing
the invention are set forth in the appended claims. The     system 20, which includes processor 22, keyboard 24,
invention itself however, as well as a preferred mode of     and display 26. Keyboard 24 is coupled to processor 22
use, further objects and advantages thereof, will best be  35  by a cable 28. Display 26 includes display screen 30,
understood by reference to the following detailed de-     which may be implemented utilizing a cathode ray tube
scription of an illustrative embodiment when read in con-     (CRT), a liquid crystal display (LCD), an electrolumines-
junction with the accompanying drawings, wherein.       cent panel, or the like. Data processing system 20 also
includes pointing device 32, which may be implemented
Figure 1 A depicts a typical window in a graphical  40  utilizing a track ball, joystick, touch sensitive tablet or
user interface according to the prior art;           screen, trackpad, or as illustrated in Figure 2, a mouse.
Pointing device 32 may be utilized to move a pointer or
Figure 1 B shows a first window and a second win-     cursor on display screen 30. Processor 22 may also be
dow that overlays the first window according to the     coupled to one or more peripheral devices, such as mo-
prior art;                           45  dem 34, CD-ROM 36, network adaptor 38 and floppy
disk drive 40, each of which may be internal or eKternal
Figure 2 depicts a data processing system in ac-     to the enclosure of processor 22. An output device such
cordance with the method and system of the     as printer 42 may also be coupled to processor 22.
present invention;                        [O01 6]  Those  persons skilled  in the art of data
50  processing systems design should recognize that dis-
Figure 3 is a more detailed high-level blockdiagram     play 26, keyboard 24, and pointing device 32 may each
which further illustrates the major components of     be implemented utilizing any one of several known off-
the data processing system of Figure 2;           the-shelf components. Data processing system 20 may
be implemented utilizing any general purpose computer
Figure 4 illustrates a display containing a window  55  or so-called personal computer, such as the personal
that displays information in accordance with the     computer sold under the trademark ''PS12, '' which is
method and system of the present invention;        manufactured and distributed by lnternational Business
Machines Corporation (l BM), of Armonk, New York.
3
