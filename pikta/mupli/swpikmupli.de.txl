<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Europäische Softwarepatente: Einige Musterexemplare

#descr: Auf folgende eingängige Beispiele stießen wir beim ersten Durchstöbern
unserer Softwarepatente-Tabellen.  Sie wurden fast zufällig ausgesucht
und können somit als repräsentativ für die Maßstäbe des Europäischen
Patentamtes hinsichtlich Technizität und Erfindungshöhe gelten.  Wenn
sie in irgendeinem Punkt vom Mittelmaß abweichen, dann darin, dass sie
relativ leicht einem breiten Publikum verständlich gemacht werden
können.

#ep927945t: Amazon Geschenkbestellung

#ep927945d: Wenn Sie Ihren Internet-Kaufladen so programmieren möchten, dass er
Ihre Waren als Geschenke an Dritte liefert, täten Sie wohl gut daran,
beizeiten die Firma Amazon um eine Lizenz zu bitten.  Dieses Patent,
ein direkter Abkömmling des 1-Click-Patentes, wurde vom Europäischen
Patentamt im Mai 2003 erteilt.

#ep927945c: Verfahren in einem Computersystem zum Bestellen eines Geschenks zur
Lieferung von einem Geschenkgeber zu einem Empfänger, wobei das
Verfahren aufweist:%(ul|Empfangen eines Hinweises von dem
Geschenkgeber, dass das Geschenk zu dem Empfänger geliefert werden
soll, und einer elektronischen Mail-Adresse des Empfängers; und|Senden
eines Hinweises auf das Geschenk und die empfangene elektronische
Mail-Adresse zu einem Computersystem für Geschenklieferungen, wobei
das Computersystem für Geschenklieferungen eine Lieferung des
Geschenks koordiniert durch:|Senden einer elektronischen
Mail-Nachricht, die an die elektronische Mail-Adresse des Empfängers
adressiert ist, wobei die elektronische Mail-Adresse den Empfänger
auffordert, Lieferungsinformation umfassend eine Postadresse für das
Geschenk bereitzustellen;|Beim Empfangen der Lieferungsinformation,
elektronisches Initiieren einer Lieferung des Geschenks in
Übereinstimmung mit der empfangenen Lieferungsinformation.)

#ep902381t: Amazon 1Click

#ep902381d: Amazons Patentanmeldung seiner One-Click-Shopping-Methode am EPA.  Die
Anmeldung errichte das dritte Stadium der Prüfung (A3), d.h. sie wurde
als patentfähige Erfindung anerkannt und eine volle Neuheitsprüfung
wurde durchgeführt.  Im August 2001 wurde die Anmeldung in zwei neue
Anmeldungen aufgeteilt.  Von diesen wurde eine anschließen erteilt,
während die andere noch auf Prüfung wartet.

#ep902381c: A method for placing an order to purchase an item, the order being
placed by a purchaser at a client system and received by a server
system, the method comprising: %(ul|under control of the server
system, receiving purchaser information including identification of
the purchaser, payment information, and shipment information from the
client system;|assigning a client identifier to the client
system;|associating the assigned client identifier with the received
purchaser information;|sending to the client system the assigned
client identifier; and|sending to the client system display
information identifying the item and including an order button;|under
control of the client system, receiving and storing the assigned
client identifier;|receiving and displaying the display information;
and|in response to the selection of the order button, sending to the
server system a request to purchase the identified item, the request
including the assigned identifier; and|under control of the server
system, receiving the request; and|combining the purchaser information
associated with the client identifier included with the request to
generate an order to purchase the item in accordance with the billing
and shipment information whereby the purchaser effects the ordering of
the product by selection of the order button.)

#ep533098t: Control of an apparatus by actuator check via microcomputer

#ep533098d: Der Hauptanspruch scheint alle zustandsgesteuerten automatischen
Systeme zu umfassen, einschließlich Unversalrechnern, die Ausgabewerte
in Abhängigkeit von Eingabewerten steuern.  Der Hauptanspruch sagt,
dass solche Systeme %(q:vorzugsweise Autoradios) sein sollten,
beschränkt den Schutzumfang jedoch nicht darauf.  Das Patent wurde
nach 12-jähriger Prüfung, einschließlich mehrjährigem
Einspruchsverfahren, erteilt.  Einsprechender war eine Gemeinschaft
von Firmen, die versucht, den Bereich des Rundfunks möglichst
patentfrei zu halten.

#ep533098c: %(linul|Einrichtung zur Steuerung eines nachrichtentechnischen
Gerätes, vorzugsweise eines Autoradios, mit einem Mikrocomputer, einem
Speicher, einem Bedienteil mit Bedienelementen und mit Stellgliedern,
wobei der Mikrocomputer ausgebildet ist, mit einem im Speicher
abgelegten Programm die Stellung der Bedienelemente zu erfassen und
die entsprechenden Funktionen der Einrichtung zu steuern dadurch
gekennzeichnet,|daß das Programm eine Tabelle enthält, die
Startinformationen von Programm-Modulen zur Steuerung der Stellglieder
für alle möglichen Betriebszustände enthält, wobei jeweils ein
Betriebszustand durch eine Einstellung der Stellglieder und durch
einen Betätigungszustand der Bedienelemente gegeben ist,|daß der
Mikrocomputer derart ausgebildet ist, daß er bei Betätigung eines
Bedienelementes einen Suchvorgang in der Tabelle durchführt, bei dem
die in der Tabelle aufgeführten Betriebszustände als Sollwerte mit dem
tatsächlich vorhandenen Betriebszustand als lstwert verglichen
werden,|daß der Mikrocomputer nach erfolgreichem Suchvorgang die
gefundene Startinformation der Tabelle entnimmt und|daß der
Mikrocomputer ein oder mehrere Programm-Module unter Benutzung der
Startinformation ausführt.)

#ep287578t: Kompression Akustischer Daten -- MP3-Grundlagenpatent

#ep287578d: Man führe bekannte Berechnungen über akustischen Daten so lange immer
wieder aus, bis ein bestimmter Wert unterschritten wird.  Der
Patentbesitzer Karlheinz Brandenburg, Forscher des MP3-Projektes bei
der Max-Planck-Gesellschaft, erhielt 1989 dieses Patent.  Das Patent
und sein Besitzer wurden von der Europäischen Kommission im Jahre 2001
im Rahmen des %(q:IPR Helpdesk) als vorbildlich und %(q:Erfinder des
Monats) hervorgehoben.  Es handelt sich um ein besonders grundlegendes
und bekanntes von mehreren Dutzenden von Patenten, die den
MP3-Standard %(q:schützen).  Dieses und andere MP3-Patente werden als
Modelle %(q:technischer) und %(q:nicht-trivialer) Softwarepatente
gehandelt.

#ep287578c: %(linul|Digitales Codierverfahren für die übertragung und/oder
Speicherung von akustischen Signalen und insbesondere von
Musiksignalen, mit folgenden Schritten,|N Abtastwerte des akustischen
Signals werden in M Spektralkoeffizienten umgesetzt,|die M
Spektralkoeffizienten werden in einer ersten Stufe quantisiert,|nach
Codierung mit einem Entropiecodierer wird die zur Darstellung aller
quantisierten Spektralkoeffizienten erforderliche Bitzahl
überprüft,|entspricht die erforderliche Bitzahl nicht einer
vorgegebenen Bitzahl, so wird die Quantisierung und Codierung in
weiteren Schritten mit geänderter Quantisierungsstufe solange
wiederholt, bis die zur Darstellung erforderliche Bitzahl die
vorgegebene Bitzahl erreicht,|zusätzlich zu den Datenbits wird die
benötigte Quantisierungsstufe übertragen undloder gespeichert.)

#ep769170t: Abfangen von Viren

#ep769170d: Abfangen eines Virus in einer simulierten Systemumgebung: Der
verdächtige Datenstrom wird zunächst durch die simulierte Umgebung
geleitet.  Falls er sie zerstört, ist es ein Virus und er wird dann
von der eigentlichen Umgebung ferngehalten.

#ep767419t: IBM Verfahren und Rechnerprogrammprodukt zur Anzeige der Objekte
(Texte, Grafiken, Ikonen, ...) von überlappten Fenstern in einem
DV-System mit Rechnerfensterumgebung.

#ep767419d: Die Objekte überdeckter Fenster werden in Abhängigkeit von Parametern
(Objekte; Restgröße; Benutzersteuerung; ...) sichtbar gemacht.  Bei
ausreichend großer sichtbarer Restfensterfläche des überdeckten
Fensters werden die Objekte des überdeckten Fensters so aufbereitet
bzw. skaliert, daß sie in diesem Fenster wieder vollständig - optional
unter Verwendung von Bildlaufleisten - sichtbar werden. Die Größe des
überdeckten Fensters wird hierbei nicht verändert. Bei Textdaten
erfolgt die Darstellung der Informationen durch einen Textumbruch
unter Beibehaltung der Textreihenfolge.  Falls die sichtbare
Restfensterfläche des überdeckten Fensters für eine Darstellung der
Objekte nicht ausreichend ist wird versucht, das überdeckte Fenster
auf der Anzeige an eine freie Stelle zu verschieben. Da hierbei andere
Fenster überdeckt werden können, soll die Verschiebung in Abhängigkeit
der Aktualität möglicher, überdeckender Fenster erfolgen.  Geschützt
ist danach ein Computerprogramm (ablauffähig im Rechner oder
bereitgestellt auf einem von einem Rechner lesbaren Medium) welches
Programmcode enthält, mit dem auf Basis einer Ermittlung,
Bereitstellung und Verarbeitung von Steuerungsparametern (werden in
der Patentschrift allgemein erklärt) die o. b. Funktionen mittels
Betriebssystem und grafischer Oberfläche ausführbar sind.  In der
Patentschrift wird am Beispiel eines IBM-PC / PS2-Modells die
DV-Hardware aufgeführt. Hardwarekenntnisse sind aber für das
Verständnis des Patents völlig unbedeutend.  Die Einspruchsfrist
verstrich 2001-01-19 ungenutzt.

#ep193933t: Steuerung eines Rechners durch einen anderen

#ep193933d: Hierunter fielen zunächst Systeme wie RPC, Telnet und überhaupt alle
Arten der Aufgabenverteilung zwischen Rechnern, d.h. die gesamte
Grundarchitektur des Internet und anderer Netzwerke.  Nach 10-jähriger
Prüfung erteilte das EPA 1995 Wang Laboratories eine reduzierte
Fassung, die nur noch solche Fernaufruf-Systeme umfasst, bei denen
prozedur-übergreifende Variablen im Protokoll bereit gestellt und
nicht mit jedem einzelnen Prozedur-Aufruf als Parameter übergeben
werden können.

#ep193933c: Digitaldaten-Verarbeitungssystem mit einer
Ziel-Datenverarbeitungseinrichtung, die durch eine
Nachrichtenübertragungseinrichtung mit einer
Quellen-Datenverarbeitungseinrichtung verbunden ist, und bei dem die
Quellen-Datenverarbeitungseinrichtung eine
Fernaufruf-Bereitstellungseinrichtung zum Bereitstellen eines
Fernaufrufs für ein aufrufbares Programm in der
Ziel-Datenverarbeitungseinrichtung aufweist und die
Ziel-Datenverarbeitungseinrichtung eine Fernaufruf-Empfangseinrichtung
zum Empfangen des Fernaufrufs und zum Ausführen eines Aufrufes fürdas
aufrufbare Programm aufweist, und das ein Aufrufprotokoll aufweist,
das von  der Fernaufruf-Bereitstellungseinrichtung in der
Quellen-Datenverarbeitungseinrichtung erzeugt, durch die
Nachrichtenübertragungseinrichtung zur
Ziel-Datenverarbeitungseinrichtung  übertragen und von der
Fernaufruf-Empfangseinrichtung empfangen wird, und das Aufrufprotokoll
einen Programmidentifizierer und Aufrufparameter zum Spezifizieren des
aufrufbaren Programms umfasst, dadurch gekennzeichnet, dass %(ol|jedes
Aufrufprotokoll einen Wahl-Teil enthalten kann, der ein oder mehrere
wahlweise Datenelemente enthält, wobei das Datenelement nicht in jedem
Aufruf für das aufrufbare Programm erforderlich ist, sondern von der
Fernaufruf-Empfangseinrichtung beim Ausführen des Aufrufes benutzt
wird, wenn die wahlweisen Datenelemente im Protokoll vorhanden
sind|jedes wahlweise Datenelement einen Parameterwert und ein
Datenelement-Identifizierungskennzeichen, das die Verwendung des
Parameterwertes im  Aufruf vorschreibt, enthält,|die
Fernaufruf-Empfangseinrichtung den Parameterwert jedes wahlweisen
Datenelementes im Aufruf entsprechend dem Identifizierungskennzeichen
des wahlweisen Datenelementes benutzt.)

#ep266049t: Kodiersystem zur Reduktion von Redundanz

#ep266049d: Dieses Patent wurde 1994 vom EPA nach 7-jähriger Prüfung mit
Prioritätsdatum 1986 erteilt.  Im Jahre 2002 veranlasste es Sony und
andere Firmen, viele Millionen USD für die Nutzung des JPEG-Standards
zu zahlen, woraufhin JPEG aufhörte, ein internationaler Standard zu
sein.

#ep266049c: Verfahren zum Kodieren digitaler Signale mit geordneter Redundanz,
wobei die digitalen Signale eine Vielzahl von unterschiedlichen Werten
annehmen, unter Benutzung von %(tp|zwei Lauflängen-Kodetypen|R,R'),
welches Verfahren die Schritte umfaßt.%(ol|der %(tp|erste
Lauflängen-Kodetyp|R) wird zum Kodieren einer Lauflänge des am
häufigsten auftretenden Wertes benutzt, dem der nächsthäufig
auftretende Wert folgt;|der %(tp|zweite Lauflängen-Kodetyp|R') wird
zum Kodieren einer Lauflänge des am häufigsten auftretenden Wertes
benutzt, dem irgendein anderer Wert als der nächsthäufig auftretende
Wert folgt;|und wenn der zweite Lauflängen-Kodetyp verwendet wird,
wird der Lauflänge-Kodewert durch einen Kodewert gefolgt, der für die
Amplitude des anderen Wertes bezeichnend ist.)

#ep689133t: Adobe Patent auf Paletten mit Reitern

#dp689133d: Dieses Patent wurde vom EPA im August 2001 ohne Abstriche gegenüber
der amerikanischen Originalversion erteilt.  Die Prüfung dauerte 6
Jahre.  Adobe hat aufgrund dieses Patentes eine Klage gegen Macromedia
eingereicht.  Es deckt die Einführung einer dritten Dimension in
Menüoberflächen ab.  Die Erfindung besteht darin, dass Sätze von
anwählbaren Optionen hintereinander angeordnet und mit Reitern
versehen werden.  Dieses Prinzip wird in Bildbearbeitungsprogrammen
wie Photoshop oder GIMP verwendet, aber nicht nur dort.

#ep689133c: Ein Verfahren zum Vereinigen, auf einem Computerdisplay, eines
zusätzlichen Satzes von lnformationen, die in einem ersten Bereich des
Displays angezeigt werden und denen ein Auswahlindikator zugeordnet
ist, mit einer Gruppe mehrerer Sätze wiederholt benötigter
lnformationen, die in einem zweiten Bereich des Bildschirm angezeigt
werden, umfassend die Schritte: %(ol|Bilden des zweiten Bereichs auf
dem Computerdisplay, in dem die Gruppe mehrerer Sätze vonn
Informationen angezeigt wird, wobei der zweite Bereich ein Größe
aufweist, die geringer ist als die vollständige Fläche des
Computerdisplays, wobei der zweite Bereich einen ersten der mehreren
Sätze von lnformationen anzeigt;|Bereitstellen einer Mehrzahl von
Auswahlindikatoren, von denen jeder einem zugehörigen der mehreren
Sätze von lnformationen zugeordnet ist, in dem zweiten
Bereich;|Auswählen eines zweiten der mehreren Sätze von Informationen
zur Anzeige in dem zweiten Bereich durch Aktivieren eines
Auswahlindikators, der einem zweiten der mehreren Sätze von
lnformationen zugeordnet ist, wodurch der erste der mehreren Sätze von
lnformationen in dem Bereich des Displays durch den zweiten der
mehreren Sätze von lnformationen ausgetauscht wird; und|Vereinigen des
zusätzlichen Satzes von lnformationen, der in dem ersten Bereich des
Displays angezeigt wird, mit der Gruppe mehrerer Sätze von
lnformationen derart, daß der zusätzliche Satz von lnformationen unter
Verwendung seines Auswahlindikators auf die gleiche Weise wie die
anderen Sätze von Informationen in der Gruppe ausgewählt werden kann.)

#ep487110t: automatisierbare medizinische Diagnose

#ep487110d: Das Patent deckt alle notwendigen Merkmale einer beliebigen
automatisierbaren medizinischen Analyse ab.  Zu diesen notwendigen
Merkmalen gehört, dass ein Bild erstellt wird und zugehörige Angaben
bekannt sind oder aus dem Bild heraus automatisch gewonnen werden
können.  Wenn dies gegeben ist und die Angaben ausreichen, um mit
einem beliebigen Algorithmus eine Analyseberechnung durchzuführen,
unterliegt das Verfahren diesem Patent, egal wie der Algorithmus
beschaffen ist.  Ansprüche 2 und 3 erweitern das ganze um ein Netzwerk
und eine angeschlossene Datenbank.  Anspruch 5 ist besonders witzig.
Er fügt dem ganzen die %(q:Erfindung) hinzu, analyse-bilder als
%(q:gelesen) zu kennzeichnen, d.h. zu markieren, ob ein Arzt sie
gesehen hat oder nicht.  Weitere Ansprüche und Folgepatente machen den
Patentinhaber Shibaura (japanische Firma) zum Eigentümer zahlreicher
organisatorischer Aufgaben von Krankenhäusern und Arztpraxen.

#ep792493t: dynamische Preisfestlegung

#ep792493d: Wenn ein Kunde den Preis der Ware erfragt, bekommt er nicht einen
konstanten Wert sondern das Ergebnis einer Berechnung, wobei die
Rechenformel anwenderseitig editierbar ist.  Mit den Unteransprüchen
wird ein Großteil der Probleme abgedeckt, die jedes moderne
E-Kommerz-System zu lösen hat.

#ep328232t: digitale Unterschrift mit zusätzlichen Authentifizierungsinfos

#ep328232d: Bei einem System wie PGP werden in die Signatur noch zusätzlich
Zertifizierungsdaten zur besseren Identifikation des Unterzeichners
aufgenommen.  Der Hauptanspruch wurde nach der EPA-Prüfung scheinbar
enger formuliert, aber die angeblich einengende Zusatzinformation ist
größtenteils redundant.  Es werden nur Selbstverständlichkeiten
beschrieben, die sich aus dem von dem Patent beanspruchten Problem
notwendigerweise ergeben.

#ep644483t: Computer system and method for performing multiple tasks

#ep644483d: Zwischen verschiedene Applikationen und den Benutzer wird eine
Schnittstelle geschaltet, mit der sowohl die Applikationen als auch
der Benutzer kommunizieren können. Dadurch können Applikationen im
Hintergrund laufen.  Unter den von IBM beantragten Hauptanspruch fiel
jede Art von Multitasking. Die Prüfer gewährten nur Multitasking in
vernetzter Umgebung.

#ep800142t: Umwandlung von Dateinamen

#ep803105t: EP803105: Verkaufssystem für ein Netzwerk

#ep803105d: This patent, granted to OpenMarket Inc by the European Patent Office
in 2002 after 6 years of examination, is identical to a system which
is currently being used in the USA to squeeze money out of various
e-commerce companies.

#ep803105c: %(linul|Netzwerkbasiertes Verkaufssystem mit|wenigstens einem
Käufercomputer, zu bedienen durch einen Benutzer, der ein Produkt zu
kaufen wünscht,|wenigstens einem Händlercomputer und|wenigstens einem
Zahlungscomputer,|wobei der Käufercomputer, der Händlercomputer und
der Zahlungscomputer durch ein Computernetzwerk verbunden sind,|der
Käufercomputer programmiert ist, eine Benutzeranforderung für den Kauf
eines Produktes zu empfangen und das Versenden einer
Zahlungsnachricht, die eine Produktkennung zur Identifizierung des
Produkts enthält, an den Zahlungscomputer zu veranlassen,|der
Zahlungscomputer programmiert ist, die Zahlungsnachricht zu empfangen
und die Erzeugung einer Zugangsnachricht, die die Produktkennung und
eine Zugriffsnachricht-Authentifizierung auf der Grundlage eines
kyptographischen Schlüssels enthält, und das Versenden der
Zugangsnachricht an den Händler-computer zu veranlassen, und|der
Händlercomputer programmiert ist, die Zugangsnachricht zu empfangen,
die Zugangsnachricht-Authentifizierung zu verifizieren, um
sicherzustellen, daß die Zugangsnachricht-Authentifizierung mit Hilfe
des kyptographischen Schlüssels erzeugt wurde, und das Versenden des
Produkts an den Käufer zu veranlassen, der das Produkt zu kaufen
wünscht.)

#ep529915t: Sprachfernsteuerung

#ep529915d: Dieses Patent deckt im engeren Sinne jegliche Fernsteuerung eines
Rechners (z.B. Server) über den Rechner des Anwenders ab (z.B.
Internet-Surfen ohne Handbewegung, e-Shopping von der intelligenten
Küche aus, diktieren eines Briefes z.B. über das X-Windows-System, ...
). Im weiteren Sinne ist Sprach-Steuerung per se patentiert z.B. ein
sprachgesteuertes Tamagotchi, oder ein Sprachgesteuerter Gameboy ... .
Vorraussetzung ist lediglich, dass Benutzeroberflächenlogik und
Anwendungslogik voneinander getrennt sind, was fast immer der Fall
ist.

#ep747840t: Dynamisch erweiterbarer Webserver

#ep747840d: Hierunter fällt jeder Web-Server, der über eine Schnittstelle andere
Software aufruft, um eine Anfrage auszuführen. Ein solcher Web-Server
kann z.B. sein: Apache Web Server, Netscape Fastrack Server, Microsoft
Internet Information Server, Apache Tomcat Server. Schnittstelle ist
z.B. CGI, Servlet API, ISAPI, NSAPI. Software kann z.B. ein CGI-Skript
sein, eine Java-Klasse, eine Datenbank, ein anderer Web-Server, jedes
serverseitige Script wie JSP, ASP, PHP, ... .

#ep587827t: Vorab-Rückmeldungen

#ep587827d: Bei langsamen Netzverbindungen bekommt der Benutzer vorab eine lokal
simulierte Rückmeldung, die bei Bedarf später ersetzt wird, wenn die
eigentliche Rückmeldung vom entfernten Rechner (der z.B. eine Anlage
steuert) zurückkommt.

#ep490624t: intuitive Netzwerk-Konfiguration

#ep490624d: eine Benutzeroberfläche, bei der die einzelnen Rechnern in einem
Netzwerk und die Verbindungen zwischen ihnen als grafische Objekte
(z.B. Kreise und Pfeile) versinnbildlicht sind und auf grafischer
Ebene maniuliert werden können.  Am Schluss werden aus den Wünschen
des Benutzers Konfigurationsdateien erzeugt.  Hiermit sind alle
benutzerfreundlichen Netzwerkadministrationswerkzeuge erfasst.

#ep762304t: E-Geschäftsanbahnung an der Börse

#ep762304d: Hierunter fallen Systeme, die es zwei Personen erlauben, ins Geschäft
zu kommen, indem einer Angebote einholt und dann bei Interesse
bestätigt.

#ep688450t: Dateisystem für Flash-Speicherbausteine

#ep688450d: Dieses Patent dürfte es nahezu unmoeglich machen, Dateisysteme fuer
Flash-Speicherbausteine zu entwickeln.  In solchen Bausteinen kann man
immer nur 64k o.ä. auf einmal ändern.  Daraus ergeben sich ein paar
Einschränkungen beim Dateisystem-Entwurf, um die man nicht herumkommt,
und deren Konsequenzen hiermit patentiert sind.  Ein paar weitere
EPA-Patente im Bereich der Flash-Bausteine und der Speicherverwaltung
dürften ihr übriges dafür tun, dass es sehr schwierig sein wird,
Linux-Systeme in Haushaltsgeräten u.a. zu etablieren.  Diese Domänen
dürften dann dem dominierenden proprietären System (Windows CE?)
vorbehalten bleiben.

#ep807891t: Elektronischer Einkaufswagen

#ep807891d: im WWW einzukaufende Gegenstände in einer Liste sammeln und erst zum
Schluss kaufen.

#ep823173t: Dateneinsparung bei mobiler TCP-Kommunikation

#ep823173d: Dateneinsparung durch Multiplexen und Demultiplexen (Bündelung und
Entbündelung mehrerer Anfragen auf einem Kanal) bei
Internet-Verbindungen.  Kaum ein kommender Standard der mobilen
Internet-Kommunikation dürfte an diesem Problem vorbeikommen.

#ep522591t: Übersetzung von natürlichsprachlichen in formalsprachliche
Datenbankanfragen mithilfe von Parsern und Tabellen

#ep522591d: Dank der folgenden Ansprüche besitzt Mitsubishi das Problem der
Übersetzung von englisch-, japanisch- oder deutschsprachigen
Fragesätzen in SQL-Abfragen mithilfe von nicht näher spezifizierten
Parsern, Tabellen und Wörterbüchern.

#ep242131t: Visualisierung von Prozessen

#ep242131d: Funktionen visualisieren, indem man die Funktionsbestandteile (Ein-
und Ausgabeparameter) einzeln graphisch darstellt, Iterationen am
Bildschirm ermoeglicht und daraus ein Ablaufdiagramm erzeugt. Dank
diesem Patent önnte durchaus auch in Europa ein ähnlicher Rechtsstreit
stattfinden. Wenn das Patent denn rechtsbeständig wäre.

#ep242131c: %(nl|Verfahren zum Erzeugen eines einen physischen Prozeß
modellierenden Programmes für einen Rechner unter Verwendung eines
Datenflußdiagramms, wobei der Rechner einen Speicher, welcher mehrere
ausführbare Funktionen und mehrere Daten unterschiedlicher Arten
speichert, eine Anzeige, eine Vorrichtung zum Empfangen einer
Benutzereingabe und einen Datenprozessor aufweist, gekennzeichnet
durch die folgenden Verfahrensschritte:|Zusammensetzen eines
Datenflußdiagramms auf der Anzeige abhängig von Benutzereingaben,
wobei das Datenflußdiagramm den Prozeß spezifiziert und
Funktions-Piktogramme, welche jeweils einer der mehreren ausführbaren
Funktionen entsprechen, Terminal-Piktogramme, welche jeweils einzelnen
der mehreren Datenarten entsprechen, ein Struktur-Piktogramm, welches
den Steuerfluß des Datenflußdiagramms anzeigt, und Bögen, welche die
Funktions-Piktogramme, die Terminal-Piktogramme und das
Struktur-Piktogramm verbinden, aufweist und Erzeugen eines
ausführbaren Programmes abhängig von dem Datenfluß-diagramm, wobei das
ausführbare Programm eine oder mehrere der ausführbaren Funktionen
umfaßt, welche durch die Funktions-Piktogramme angezeigt und wie durch
die Bögen angezeigt miteinander verbunden sind um mit durch die
Terminal-Piktogramme identifizierten Daten zu arbeiten, wobei der
Steuerfluß des ausführbaren Programmes wie durch die
Struktur-Piktogramme angezeigt verläuft.)

#ep359815t: Unterscheidung zwischen benutzten und unbenutzten Blöcken bei der
Behandlung von Cache-Zuweisungsfehlern

#ep359815d: Jeder Speicherblock enthält ein Gültigkeits-Bit.  Ist dieses bei einem
fehlerhaften Speicherblock ausgeschaltet, so wird sofort eine ganze
uebergeordnete Gruppe aus meist 4 Bloecken beschrieben.  Ist es
eingeschaltet, so wird nur eine geringer Zahl von Blöcken (meist 1
Block) neu geschrieben, da man dann davon ausgeht, dass die
Vierergruppe richtig belegt ist und nur an der einen Stelle ein Fehler
aufgetreten ist.  D.h. dieses Patent beansprucht den gesamten
möglichen Nutzen des Gültigkeits-Bits.  Das EPA hat hunderte von
Patenten auf Speicherverwaltungsarithmetik vergeben.  Es fragt sich,
wer außer IBM und Sun heute noch einen Speicher programmieren darf.

#ep592062t: Übermittlung von Kompressionsanforderungen

#ep592062d: Eine Applikation übermittelt einem Kommunikationsserver die Anweisung,
eine bestimmte zu sendende Datei vor der Sendung mit einem bestimmten
Kompressionsverfahren zu komprimieren.  Der MIME-Standard verstößt
vermutlich gegen dieses Patent.

#ep756731t: Erzeugung von Einkaufszetteln aus Kochrezepten zum Zwecke der
Verkaufsförderung in einem Supermarkt

#ep756731d: Hiervon ist jedes System betroffen, das Kundenwuensche nach
Lebensmitteln aufnimmt, verarbeitet und daraus einen Einkaufszettel
mit genauen Hinweisen auf zu kaufende Waren und Lagerungsorte etc
erzeugt.

#ep461127t: Sprachlernen durch Vergleich eigener Aussprache mit der eines Lehrers

#ep461127d: Dieses Patent deckt alle Systeme ab, die dem Sprachlernenden erlauben,
ein Stück Text auszuwählen, selber vorzulesen und das Vorgelesene
danach mit einer digital gespeicherten oder synthetisierten
Muttersprachler-Version zu vergleichen.  Damit ist diese ganze Branche
patentiert.  Natürlich kommen noch allerlei Verfeinerungen hinzu, die
aber weder mit Naturkräften noch mit Programmiermethoden
zusammenhängen.  Letzteres gilt alles als selbstverständliches
Handwerkszeug.  Patentiert ist nur das Organisationsverfahren selbst. 
Nebenbei sind auch noch alle Spracherkennungssysteme mit Lernfunktion
betroffen.

#ep664041t: Prüfen von Lernstoff in Schulen

#ep664041d: Der Hauptanspruch deckt alle notwendigen Merkmale der
computergestützten Durchführung einer Prüfung in Schule oder Uni ab. 
Zahlreiche zusätzliche Möglichkeiten, eine Prüfung noch effektiver zu
organisieren, sind mit je einem Unteranspruch abgedeckt.

#ep517486t: Miteingabe von Kontextdaten

#ep517486d: Ein putziger kurzer (und breiter) Anspruchsbereich.  Die Beschreibung
offenbart einen Versuch, eine auf Oberon aufbauende
Systemsteuerungsoberflaeche zu schaffen, die Vorzuege von
Kommandozeile und GUI vereinigen soll.

#ep825526t: Übersetzung zwischen zwei Objekten

#ep825526d: Anspruch 1 ist so was wie Juristendeutsch fuer folgende anscheinend
patentwürdige Entdeckung bzw ihre Anwendung auf Software-Objekte: 
Wenn du nur Englisch sprichst und ich nur deutsch, dann können wir
trotzdem miteinander kommunizieren, wenn einer von uns beiden einen
Dolmetscher mitbringt, der beide Sprachen beherrscht.

#ep825525t: Vererbung von CORBA Objekten

#ep825525d: Einfach formuliert heisst Anspruch 1: eine Methode, welche die
Erzeugung eines CORBA-objektes aus einem anderen ermöglicht, ähnlich
der %(q:Vererbung) in C++ oder Java.  Ein Softwarepatent wie es im
Buche steht:  die Methode ist Software, das Objekt ist Software, und
die zugrundeliegende %(q:Technik) (CORBA) ist ebenfalls Software (als
solche).

#ep794705t: Brotmaschine und Kodierungssystem dafür

#ep794705d: Wer den Vorgang des Brotbackens durch ein Computerprogramm steuert,
verletzt dieses Patent.  Es kommt dabei nicht darauf an, wie man die
abtastenden und eingreifenden Geräte konzipiert und verteilt oder wie
man sie programmiert:  jeder Backofen, bei dem der Benutzer zwischen
verschiedenen Backprogrammen wählen kann (vgl Waschprogramme einer
Waschmaschine), verletzt das Patent.  Die Ansprüche richtet sich nicht
auf ein Computerprogramm als solches sondern auf eine damit verbundene
Geschäftsidee.  Der Kausalzusammenhang zwischen den eingesetzten
Mitteln und der erzielten Wirkung bleibt dabei ein rein logischer:  es
wird unabhängig von jeglicher neuen Lehre zum Einsatz von Naturkräften
eine angeblich erfinderische Funktionalität beansprucht.  Der
Hongkonger Patentinhaber darf auf Lizenzgebühren von
E-Kommerz-basierten Backwaren-Lieferdiensten aus 11 europäischen
Ländern hoffen.

#ep797806t: Binäre Datenanordnung

#ep797806d: Ein 1997 erteiltes EPA-Patent mit Prioritätsdatum 1994, auf ein
grundlegendes Verfahren der Informationsverarbeitung, welches u.a. in
neueren WWW-Standards verbreitet ist.  Nach diesem Verfahren wird alle
Information durch eindeutig gekennzeichnete Einheiten (Atome)
gekennzeichnet, die wiederum durch elementare Aussagen miteinander in
Beziehung gesetzt werden, derart dass jede Beziehungsaussage aus zwei
Atomkennzeichnern und einem Beziehungskennzeichner (bond) besteht. 
Dies erlaubt eine größere Unabhängigkeit zwischen den Daten und den
Verfahren, welche zur Verarbeitung der Daten eingesetzt werden.

#ep526034t: ATT-Patent auf eindeutige Dateibenennungskonventionen

#ep526034d: 1993 machte das Europäische Patentamt die Firma American Telephone &
Telegraph zum Besitzer aller Dateibenennungskonventionen, bei denen
die Datei auch nach einer Namensänderung noch auffindbar ist, solange
bestimmte eindeutig identifizierende Suchkriterien (wie z.B. eine
laufende Nummer) als Teil des Dateinamens geführt werden.  D.h. sowohl
der leicht wandelbare und dafür auch leicht zu merkende
Endnutzer-Dateiname als auch ein scher zu merkender aber dafür
eindeutiger Schlüssel werden zur Auffindung der Datei oder der
Dateiengruppe verwendet.

#ep526034c: Digitaldaten-Verarbeitungssystem mit einer
Ziel-Datenverarbeitungseinrichtung, die durch eine
Nachrichtenübertragungseinrichtung mit einer
Quellen-Datenverarbeitungseinrichtung verbunden ist, und bei dem die
Quellen-Datenverarbeitungseinrichtung eine
Fernaufruf-Bereitstellungseinrichtung zum Bereitstellen eines
Fernaufrufs für ein aufrufbares Programm in der
Ziel-Datenverarbeitungseinrichtung aufweist und die
Ziel-Datenverarbeitungseinrichtung eine Fernaufruf-Empfangseinrichtung
zum Empfangen des Fernaufrufs und zum Ausführen eines Aufrufes fürdas
aufrufbare Programm aufweist, und das ein Aufrufprotokoll aufweist,
das von  der Fernaufruf-Bereitstellungseinrichtung in der
Quellen-Datenverarbeitungseinrichtung erzeugt, durch die
Nachrichtenübertragungseinrichtung zur
Ziel-Datenverarbeitungseinrichtung  übertragen und von der
Fernaufruf-Empfangseinrichtung empfangen wird, und das Aufrufprotokoll
einen Programmidentifizierer und Aufrufparameter zum Spezifizieren des
aufrufbaren Programms umfasst, dadurch gekennzeichnet, dass %(ol|jedes
Aufrufprotokoll einen Wahl-Teil enthalten kann, der ein oder mehrere
wahlweise Datenelemente enthält, wobei das Datenelement nicht in jedem
Aufruf für das aufrufbare Programm erforderlich ist, sondern von der
Fernaufruf-Empfangseinrichtung beim Ausführen des Aufrufes benutzt
wird, wenn die wahlweisen Datenelemente im Protokoll vorhanden
sind|jedes wahlweise Datenelement einen Parameterwert und ein
Datenelement-Identifizierungskennzeichen, das die Verwendung des
Parameterwertes im  Aufruf vorschreibt, enthält,|die
Fernaufruf-Empfangseinrichtung den Parameterwert jedes wahlweisen
Datenelementes im Aufruf entsprechend dem Identifizierungskennzeichen
des wahlweisen Datenelementes benutzt.)

#ep538888t: Lesegebühren

#ep538888d: Im Jahre 1993 erteilte das Europäische Patentamt der japanischen Firma
Canon das alleinige Recht, Gebühren pro Einheit dekodierter
Information zu erheben.  Der Hauptanspruch umfasst alle Systeme, bei
denen eine lokale Dekodier-Applikation Informationen entschlüsselt,
die von einem entfernten Informationsvermarkter übertragen wurden.  
Wenn der Informationsvermarkter den Traum des %(q:Zahlens pro
Lesevorgang) (geringstmöglicher Preis bei größtmöglicher Kontrolle)
verwirklichen möchte, muss er bei Canon um eine Lizenz bitten. 
Allerdings beschreibt der Anspruch eine Klasse von Programmen für
Datenverarbeitungsanlagen (computer-implementierten Rechenregeln), und
die angeblich neue und erfinderische Problemlösung (Erfindung),
enthält selbst bei Miteinbeziehung der irrelevanten
Implementationsmerkmale nicht mehr als ein Programm [als solches]. 
Sämtliche Anspruchsmerkmale liegen auf dem Gebiet der Programmierung
normaler Datenverarbeitungsanlagen.

#ep538888c: %(al|Informationsverarbeitungssystem mit %(ul|einer
Empfangseinrichtung zum Empfang von gesendeten lnformationen, wobei
die Informationen kodiert oder nicht vollständig sind und in einer
nicht vennrendbaren Form vorliegen,|einem Aufzeichnungsträger, wobei
zur Demodulation der empfangenen Informationen erforderliche
Dekodierungsinformationen auf dem Aufzeichnungsträger voraufgezeichnet
sind, wobei der Aufzeichnungsträger einen ersten Bereich, in den die
empfangenen lnformationen zu schreiben sind, und einen zweiten Bereich
aufweist, in dem die Dekodierungsinformationen voraufgezeichnet
sind,|einer Schreibeinrichtung zum Schreiben der durch die
Empfangseinrichtung empfangenen Informationen in den ersten Bereich
des Aufzeichnungsträgers,|einer Leseeinrichtung zum Auslesen der
Dekodierungsinformationen aus dem Aufzeichnungsträger, und|einer
Demodulationseinrichtung zur Umwandlung der durch die
Empfangseinrichtung empfangenen und in den ersten Bereich des
Aufzeichnungsträgers geschriebenen lnformationen in eine verwendbare
Form durch Verwenden der durch die Leseeinrichtung ausgelesenen
Dekodierungsinformationen,)|wobei eine Gebühr für eine Verwendung der
Informationen in Einheiten entsprechend den Dekodierungsinformationen
definiert ist, die auf dem Aufzeichnungsträger voraufgezeichnet sind.)

#de19838253t: Mehr Sicherheit durch abwechselnde physische Trennung eines
Vermittlungsrechners von beiden Seiten

#de19838253d: Um ein Intranet besser gegen Eindringversuche zu wappnen, wird der
Schutzwallrechner (firewall) abwechselnd mal von der einen und dann
von der anderen Seite physisch abgetrennt.  Die Patentinhaberin
behauptet, hierdurch werde ein wesentlicher Fortschritt in der
Netzwerksicherheit erzielt.  Tatsache ist, dass jeder, der diese
naheliegende Funktionalität wirklich zum Funktionieren bringen möchte,
bei Fraunhofer um Erlaubnis bitten muss.

#de19747603t: Sicherer Geschäftsverkehr per Mobiltelefon

#de19747603d: Jeglicher sichere Geschäftsverkehr per Mobiltelefon erfordert jetzt
eine Erlaubnis der Brokat GmbH.  Das Patent verbietet es, eine
digitale Nachricht telefonisch an ein Signiergerät überträgt und von
diesem unterzeichnen zu lassen.

#ep497041t: informationsgesteuerte Medikamente-Infusion

#ep497041d: mit Mitteln der Datenverarbeitung steuern, wie Medikamente in den
Blutkreislauf eines Patienten eingeleitet werden.  Der Hauptanspruch
umfasst das ganze Problem, für das einige mögliche Lösungen
beschrieben werden.  Die Erfindung liegt weder in einer betimmten
Apparatur noch einer technischen Lehre (physikalischen
Wirkungskausalität).  Es handelt sich um ein rein logisches
Gedankengebäude, das keiner empirischen Verifizierung bedarf.  Das
angeblich Neue und Erfinderische liegt in der Geschäftsidee, zu deren
Umsetzung nur vorbekannte informatische und medizinische Kenntnisse
nötig sind.

#suK: Ausstellungsstücke

#ait: Kandidaten

#Wft: Patente, die wir etwas ausführlicher dokumentiert haben.

#ahx: Die folgenden Bewertungen kommen von Unterstützern des FFII, die sich
in unserem %(ps:Mitwirkungs-System) eingetragen haben.

#WmW: Sie können uns helfen, dieser Sammlung weitere Beispiele hinzuzufügen!

#Wlr: Sie können sowohl die %(s:TECH)nizität als auch die Qualität des
Handels (%(s:DEAL)), den unsere Gesellschaft bei seiner Gewährung
eingeht, bewerten.

#aeo: höhere Mathematik mit breitem Anwendungsfeld

#tlr: Anwendung des Universalrechners auf diverse Aufgaben

#mTm: Programmmierung von Spezialgeräten für Information und Kommunikation,
z.B. Kartenleser, Mobiltelefon

#foj: Steuerung von Maschinen, welche Naturkräfte anwenden, z.B. ABS,
Backofen

#hWt: Problemlösung hat mehr mit Naturkräften als mit Informatik
(Steuerungslogik, Automatentheorie) zu tun

#oif: Das Patent lehrt uns etwas neues über Wirkungszusammenhänge von
Naturkräften.  Diese Erkenntnisse konnten nicht ohne Experimente an
Naturkräften gewonnen werden.

#nae: selbst nach laschesten Patentamts-Maßstäben ungültig

#rbi: gehört ins Gruselkabinett: wertlose Offenbarung, maßlose
Blockierwirkung (breite Ansprüche)

#nuW: einigermaßen lesenswerte Offenbarung und/oder mäßige Blockierwirkung

#SvW: gehört ins Schönheitenkabinett: wertvolle Offenbarung, mäßige
Blockierwirkung

#rwW: Patente dieser Art sollten erteilt werden: wir alle gewinnen dadurch
mehr als wir verlieren

#Ptn: Patent

#nrn: Anmerkungen

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpikmupli ;
# txtlang: de ;
# multlin: t ;
# End: ;

