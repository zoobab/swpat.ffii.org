<div align=center><a name=bpg1>gocr ep0669021-001</a></div>
          (PICTURE)(PICTURE)  (PICTURE)Europäis hes Patentamt               <!-- barcode? -->
           (1 9)  (PICTURE)  European Patent Offi_e

          (PICTURE)  Offi_e euroPéen des brevets             (1 1 )      E _ O 669 O2_  B_

           (1 2)                EUROPEAN PATENT _PECl FICATlON

           (45) Date of pUbliCation and mention             (51 ) Int CI.6_. _O6F 9J44, _O6F 3/O23
              of the grant of the patent_.
              O2_O4_1997  BUlletin 1997l14              (86) InternationaI appIication number_.
                                               PCTIUS93l11 O61
           (21 ) Application number_. 949O15O4.4            (87) International publication number_.

          (PICTURE)(22) Date of filing_. 15.11 .1993                    wo g4l11811 (26.o5.1gg4 _azette 1gg4l12)

           (54) MULTl-LINGUAL COMPUTER PROGRAM_
              MEHRSPRACHIG ES COMPUTERPROG RAMM

          (PICTURE)(PICTURE)
              PROGRAMMES D'ORDINATEUR PLURILINGUES

      __

      __
      O
      _
      _   N   w. h.  .     h f   h   bI.  .   f h    .   f h     f h  E                    .
      _    Ote'_  It In nIne mOnt S rOm t e PU  ICatIOn O t e mentIOn O t e 9rant O t e  UrOPean Patent_ anY PerSOn maY 9IVe
      o   nOtiCe tO the EUrOpean Patent OffiCe Of OppOSitiOn tO the EUrOpean patent granted. NOtiCe Of OppOSitiOn Shall be filed in
      ii   a written reasoned statement. It shall not be deemed to have been filed until the opposition fee has been paid. (Art.
      w   (PICTURE)99(1 ) European Patent Convention).      Printed by Jouve, 75OO1 PARIS (FR)
<hr>
<div align=center><a name=bpg2>gocr ep0669021-002</a></div>
           (PICTURE)
             EP O 669 O21 B1

           Des_ription

           (PICTURE)Tec nicaI FieId

        5      This invention relates to a method and system for creating multi-lingual computer programs by dynamically loading
           messages.

        1O      Localization is the process of altering a computer program so it is appropriate for the intended geographic area or
           user group. For example, a computer program running on a computer system in the United States would typically
           communicate with a user in English, while the same computer program running on a computer system in France would
           typically communicate with a user in French. The two different versions of the computer program would be essentially
           the same except for the natural language employed by the program's user interface. If two different users, one German
        15   and one French, desired to use the same computer system, then computer programs running on the system wouId
           have to have to be localized for German and French users.
              In the past, computer systems accommodated multi-lingual communication by storing different natural language
           versions of computer programs. Thus, a French user would load a French version of a computer program and a German
           user would load a German version of a computer program. If a user required that the computer system's operating
       2O   system communicate in a certain natural language, then the user would initialize (re-boot) the computer system to
           install the correct natural language version of the operating system. Storing and loading two versions of the same
           operating system on a computer system is an inefficient use of resources.
              Currently, software developers spend a great deal of time localizing a computer program because each message
           that is output to a user must be translated into the appropriate language. When output messages are stored within the
       25   code of a computer program, a deveIoper must have access to the entire program to transIate messages. This access
           requirement is inconvenient because most programs consist of many modules that are linked together to make an
           executable program.
              Software developers use resource files to store output messages rather than storing the messages directly in the
           program's code. This method eases translation because the messages to be translated are together in one file. To
       3O   output a message, a program would retrieve the message from the appropriate resource file. Different resource files
           are used for each natural language. Developers create multiple natural language versions of the program by carying
           out the following steps_. 1 ) create a new resource file by translating the messages in an existing resource file into a
           desired natural language; 2) compile the new resource file; and 3) create an executable file by linking the compiled
           program modules with the translated, compiled resource file. Each natural language version of the program requires
       35   an executable file that contains all of the compiled program modules and the compiled resource file linked together.
              Figure 1 is a block diagram of sample prior art executable files 1 O1 , 1 O2, and 1 O3. Executable file 1 O1 represents
           a French version of the program Microsoft Excel. EXCEL.OBJ 1 O4 represents all of the compiled program modules
           that make up Microsoft Excel. EXLFR.Ll B 1 O5 represents a compiled resource file containing all of the messages
           associated with Microsoft Excel. The executable file 1 O1 consists of the compiled program modules 1 O4 linked with
       4O   the compiled resource file 1 O5. Executable programs 1 O2 and 1 O3 represent English and German versions, respec-
           tively, of Microsoft Excel. Note how the same compiled program modules 1 O4 are present in each of the executable
           programs 1 O1 , 1 O2, and 1 O3. Because the compiled program modules 1 O4 are quite large (approximately2 megabytes),
           it is wasteful to require that each natural language version contain the compiled program modules.
              From EP-A-335 1 39 a national language support system without external files is known. According tothe proposed
       45   system an appIication program incIudes footprint identifiers, each of which being foIIowed by a respective aIIocated
           space. Each footprint corresponds to a specific message or piece of information to be communicated to a human. Each
           allocated space has a length sufficient to contain a lingual message whether in any of various target national languages.
              Further, a plurality of language files are provided with each language file storing some N pieces of information in
           the same sequence but in a different target national language.
       5O     A program moduIe operates to write the target nationaI Ianguage entries from a seIected Ianguage fiIe into the
           allocated space for a corresponding footprint within the application program. The program module thereby includes a
           search function which searches the application program for one footprint after another in sequence. When the program
           module finds a footprint, it reads the associated entry in the language file corresponding to the desired target national
           Ianguage indicated by the user.
       55      In view of this prior art it is the object of the present invention to provide a simpIified and improved method and
           computer system for creating multi-lingual computer programs.
              This object is solved by the subject matter of claims 1 and 6.

                                              2
<hr>
<div align=center><a name=bpg3>gocr ep0669021-003</a></div>
          (PICTURE)(PICTURE)
       EP O 669 O21 B1

             Figure 1 is a block diagram of sample prior art executable programs.
             Figure 2 is a block diagram of a computer system used in a preferred embodiment of the present invention.
       5     Figure 3 is a detailed flow diagram of a method used in a preferred embodiment of the present invention.

             The present invention includes a method and system for creating multi-lingual computer programs by dynamically
       1O   loading messages. A computer program running on a computer system directs the computer to communicate with a
           user through the use of messages. Computer systems commonly communicate with a user by displaying output mes-
          sage to the user via a display device attached to the computer system and by receiving input messages from the user
          via a keyboard attached to the computer system. Other methods of receiving and outputting messages are known to
          those in the computer field.
       15     The messages associated with a computer program are not stored within the code of the program. lnstead, the
          messages are stored separately from the computer program so that the messages can be easily translated into other
          natural languages. Messages (in every supported natural language) are contained in a message file, which is stored
          on the computer system.
             The message file contains a plurality of message sets, with each set containing all messages associated with a
       2O   computer program in a unique natural language. A message identifier is used within the code of the computer program
          to refer to a message stored within the message file. In an alternate embodiment of the present invention, message
          sets can be stored in a header area of the computer program rather than in the message file. The header area is a
          block of data containing details about the program and is usually found at the beginning of the program. The header
          area is not loaded into memoy when the program is invoked.
       25     Before a user invokes a computer program on the computer system, the user specifies a preferred natural language
           in which the user would like to communicate with the computer program. When the computer program is invoked, a
           Localizer provided by the present invention searches the message file, selecting messages that are associated with
          the invoked computer program in the preferred natural language. The Localizer then loads the selected messages into
          the computer system's memoy. As the computer program executes, it references the selected messages in memory.
       3O     Because messages associated with a computer program are not stored within the program or linked tothe compiled
          program, a developer may modify ortranslate messages without having access tothe program. As long as the message
           identifier within an instruction of the program can be matched to a message identifier within a particular message set,
          the body of the executable program is independent of the messages.
             Figure 2 is a diagram of a computer system 2O1 used in a preferred embodiment of the present invention. The
       35   computer system 2O1 contains a memory device 2O2 and a storage device 2O3. A plurality of executable programs
          can be stored on the storage device 2O3. Program.exe 2O4 is an example of such an executable program. Program.
          exe 2O4 has two parts, a program header 2O4a and a program body 2O4b. As explained above, the program header
          2O4a is a block of data containing the size, location, and other details about Program.exe 2O4. The program body 2O4b
          contains instructions written in a binay format so that they can be loaded into the memory device 2O2 and executed
       4O   by the computer system.
             In a preferred embodiment, a message file containing a plurality of message sets is also stored on the storage
          device 2O3. Message.sys 2O5 is an example of such a message file. Message.sys 2O5 comprises a plurality of message
          sets, each message set containing the messages associated with a computer program. More than one message set
          can be associated with each computer program that is stored on the storage device 2O3. For example, message sets
       45   1 through N are all associated with Program.exe 2O4. Each message set (1 -N) contains the messages associated with
           Program.exe 2O4 in a unique natural language.
             Each message set is made up of a message header and a message list. Similar to a program header, a message
          header is a blockof data containing information about a message set. A message set is always preceded by a message
          header. Table 1 shows the contents of a message header used in a preferred embodiment of the present invention.
       5O                (PICTURE)
                                        TABLE 1

       55                                   3
<hr>
<div align=center><a name=bpg4>gocr ep0669021-004</a></div>
                                        EP O 669 O21 B1

                         (PICTURE)
                                       TABLE 1  (continued)

        5      Comp Size indicates the size of the message list when the message list is compressed Exp Size indicates the
        1O         -                                                 '   -
           size of the message list when the message list is not compressed. Lang-Code indicates in which natural language the
           message list is written. County-ld indicates in which country the natural language identified in Lang-Code is spoken.
           Prog-Name indicates the name of the program or utility that uses the messages associated with this message header.
           Comp-Size, Exp-Size, Lang-Code, and Country-ld are all explained in more detail below. Code-page, Signature,
           and Reserved are not relevant to the present description and will not be discussed further
        15                                                        '
              Figure 3 is a detailed flow diagram of a method used in the preferred embodiment of the present invention to allow
           a user of a computer system to communicate with a computer program in a preferred natural language. When a user
           of a computer system invokes an executable computer program, a copy of the program's body is loaded into the
           computer system's memory device. In this example, the user has invoked Program.exe 2O4 so that a copy of Program.

       _o   exe's body 2O4b has been loaded into the memory device 2O2. Also shown loaded into the memoy device 2O2 is a
           copy of a Localizer 2O7, which is a computer program used to carry out the methods of the present invention (see
           Figure 2). Of course, the Localizer 2O7 could be made a part of another program (i.e., Program.exe) instead of being
           a separate program.
              After the user has invoked Program.exe, in step 3O1 of Figure 3 the Localizer determines which natural language

       _5   is the preferred natural language. The preferred natural language is the language in which the user of the computer
           system 2O1 prefers to communicate with Program.exe. Preferably, the user always specifies the preferred natural
           Ianguage before invoking a computer program, but it is not a necessity. In step 3O2 the Localizer searches the program
           header 2O4a for a message set in the preferred natural language. The Localizer only has to check the language code
           identifier in the message header of any message sets stored in program header 2O4a. If such a message set is not
           found in the program header 2O4b then the Localizer searches the storage device 2O3 for the message file Message
       3O                       '                                                 '
           sys 2O5. If Message.sys 2O5 cannot be found on the storage device 2O3, then the Localizer will prompt the user for
           the location of the message file. If the user cannot direct the Localizer to the location of the message file, then the
           Localizer searches the program header 2O4a for a message set in a default language. In a preferred embodiment, a
           message set in a default language is stored within a program header so that the invoked program has some means
           for communicating with the user If the Localizer could not locate a message set in the preferred natural language or
       35                      '
           message set in a default natural language, then Program.exe would have no way of communicating with the user to
           tell the user that the message file 2O5 is not stored on the storage device 2O3.
              After Message.sys 2O5 is located on the storage device 2O3, in step 3O5 the Localizer searches Message.sys file
           2O5 for a message set in the preferred natural language. If such a message set is not found, the Localizer selects a
           message set in a default natural language from the program header 2O4a for the reasons stated above
       4O                                                                 '
              After steps 3O1 to 3O7 have been performed, the Localizer has located one of the following_. a message set in the
           preferred natural language in the program header 2O4a; a message set in the preferred natural language in Message.
           sys 2O5; or a message set in a default natural language in the program header 2O4a. In step 3O8, the Localizer selects
           the located message set. In step 3O9, the Localizer makes a memory allocation request, requesting a block of memory

       45   from the memory device 2O2. In step 31 O, the Localizer loads the message list from the selected message set into the
           allocated memoy. In step 31 1 the Localizer initializes Program.exe's data structures to point to the memory where the
           message list was loaded. The Localizer then returns control of the computer system to Program.exe. Program.exe
           communicates with the user of the computer system by referencing messages in the message list that is loaded into
           the computer system's memory device.

       5o      It can be seen then that the invention described herein permits easy and efficient storage and selection of multiple
           natural language versions of a program. A first user of a computer system can specify the language in which the first
           user would like to communicate with a program, and then a second user can specify a different language in which the
           second user would like to communicate with the program.

       55   C_al_S

           1 .  A method in a computer system for communicating messages in a preferred natural language to a user of the

                                              4
<hr>
<div align=center><a name=bpg5>gocr ep0669021-005</a></div>
                                        EP O 669 O21 B1

              computer system, the computer system having a storage device (2O3) and a memory device (2O2), the computer
              system having a computer program with a program header (2O4a) and a program body (2O4b), the program header
              containing a message set, the computer system having a message file (2O5) with a plurality of message sets, each
              message set having one or more messages for a unique natural language, the method comprising the steps of_.
        5        loading the program body into the memory device and executing the program body;

                retrieving an indication of the preferred natural language for the messages of the computer program (3O1 );

        1O        determining the natural language of the message set contained in the program header (3O2);

                when the determined natural language is the same as the preferred natural language, loading the message
                set of the program header into the memoy device wherein the executing program body retrieves messages
                from the loaded message set (3O8-31 1 ); and
        15        when the determined natural language is not the same as the preferred natural language,

                searching the message file for a message set for the preferred natural language (3O5); and

       2O        when a message set for the preferred natural language is found in the message file, loading the message set
                of the message file into the memoy device wherein the executing program body retrieves messages from the
                Ioaded message set (3O8-311 ).

           2.  The method of claim 1 wherein the step of retrieving the indication of the preferred natural language includes
       25      retrieving the indication from the storage device (3O7).

           3.  The method of claim 1 wherein the step of retrieving the indication of the preferred natural language includes
              retrieving the indication from a user of the computer program.

       3O   4.  The method of claim 1 further comprising the steps of_.

                receiving from the user an indication of the location of the message file, and

                Ioading the message set of the user indicated message file into the memory device wherein the executing
       35        program body retrieves messages from the loaded message set.

           5.  The method according to one of the claims 1 -4 further comprising the steps of when the message set for the
              preferred natural language is not found in the message file, loading the message set of the program header into
              the memory device as a default natural language wherein the executing program body retrieves messages from
       4O     the loaded message set.

           6.  computer system for selecting messages in a preferred natural language for communicating with a user of the
              computer system, the computer system having a storage device (2O3) and a memory device (2O2), the computer
              system having a computer program with a program header (2O4a) and a program body (2O4b), the program header
       45     containing a message set, the computer system having a message file (2O5) with a plurality of message sets, each
              message set having one or more messages for a unique natural language, the computer system comprising_.

                means for retrieving an indication of the preferred natural language forthe messages of the computer program;

       5O        means for loading the message set of the program header into the memory device when the preferred natural
                Ianguage is the same as the natural language of the message set of the program header;

                means for searching the message file for a message set for the preferred natural language when the preferred
                natural language is not the same as the natural language of the message set of the program header;
       55        means for loading the message set of the message file into the memoy device when the message set for the
                preferred natural language is found in the message file; and

                                              5
<hr>
<div align=center><a name=bpg6>gocr ep0669021-006</a></div>
                                        EP O 669 O21 B1

                means for retrieving messages from the loaded message set during execution of the computer program.

           Patentansprü_he
        5   1 .  Verfahren in einem Computersystem zum Kommunizieren von Nachrichten in einer bevorzugten natürlichen Spra-
              che zu einem Benutzer des Computersystems, wobei das Computersystem eine Speichereinrichtung (2O3) und
              eine Arbeitsspeichereinrichtung (2O2) aufweist und das Computersystem ein Computerprogramm mit einem Pro-
              gramm-Header (2O4a) und einem Programmkörper (2O4b) aufweist, wobei der Programm-Header einen Nach-
        1o      richtensatz enthält, das Computersystem ein Nachrichtenfile (2O5) mit einer Vielzahl von Nachrichtensätzen auf-
              weist, wobei jeder Nachrichtensatz eine oder mehrere Nachrichten für eine bestimmte natürliche Sprache aufweiSt
              und wobei das Verfahren folgende Schritte aufweist_.

                Laden des Programmkörpers in den Arbeitsspeicher und Ausführen des Programmkörpers;
        15        Erhalten einer Angabe der bevorzugten natürlichen Sprache für die Nachrichten des Computerprogramms
                (3O1 );

                Bestimmen der natürlichen Sprache des Nachrichtensatzes, der in dem Programm-Header enthalten ist (3O2);
       2O        für den Fall, daß die bestimmte natürliche Sprache die gleiche wie die bevorzugte natürliche Sprache ist,
                Laden des Nachrichtensatzes des Programm-Headers in den Arbeitsspeicher, wobei der ausführende Pro-
                grammkörper Nachrichten von dem geladenen Nachrichtensatz (3O8-31 1 ) erhält; und

       25        für den Fall, daß die bestimmte natürliche Sprache nicht gleich der bevorzugten natürlichen Sprache ist,

                Suchen des Nachrichtenfiles für einen Nachrichtensatz für die bevorzugte natürliche Sprache (3O5); und

                für den Fall, daß ein Nachrichtensatz für die bevorzugte natürliche Sprache in dem Nachrichtenfile gefunden
       3o        wird, Laden des Nachrichtensatzes des Nachrichtenfiles in den Arbeitsspeicher, wobei der ausführende Pro-
                grammkörper Nachrichten von dem geladenen Nachrichtensatz (3O8-31 1 ) erhält.

           2.  Verfahren nach Anspruch 1 , wobei der Schritt des Erhaltens der Anzeige der bevorzugten natürlichen Sprache
              das Erhalten der Anzeige von der Speichereinrichtung (3O7) einschließt.
       35   3.  Verfahren nach Anspruch 1 , wobei der Schritt des Erhaltens der Anzeige der bevorzugten natürlichen Sprache
              das Erhalten der Anzeige von einem Benutzer des Computerprogramms einschließt.

           4.  Verfahren nach Anspruch 1 mit folgenden weiteren Schritten_.
       4O        Empfangen einer Anzeige von dem Benutzer bezüglich des Ortes des Nachrichtenfiles, und

                Laden des Nachrichtensatzes des vom Benutzer angezeigten Nachrichtenfiles in den Arbeitsspeicher, wobei
                der ausführende Programmkörper Nachrichten von dem geladenen Nachrichtensatz erhält.
       45   5.  Verfahren nach einem der Ansprüche 1 bis 4, wobei für den Fall, daß der Nachrichtensatz für die bevorzugte
              natürliche Sprache nicht in dem Nachrichtenfile gefunden wird, der Nachrichtensatz des Programm-Headers in
              den Arbeitsspeicher als Standard-natürliche Sprache geladen wird, und wobei der ausführende Programmkörper
              Nachrichten von dem geladenen Nachrichtensatz erhält.
       5O   6.  Computersystem zum Auswählen von Nachrichten in einer bevorzugten natürlichen Sprache zum Kommunizieren
              mit einem Benutzer des Computersystems, wobei das Computersystem eine Speichereinrichtung (2O3) und einen
              Arbeitsspeicher (2O2) aufweist, wobei das Computersystem ein Computerprogramm mit einem Programm-Header
              (2O4a) und einem Programmkörper (2O4b) aufweist, wobei der Programm-Header einen Nachrichtensatz enthält,
       55     wobei das Computersystem ein Nachrichtenfile (2O5) mit einer Vielzahl von Nachrichtensätzen aufweist, wobei
              jeder Nachrichtensatz eine oder mehrere Nachrichten für eine bestimmte natürliche Sprache enthält und wobei
              das Computersystem enthält_.               6
<hr>
<div align=center><a name=bpg7>gocr ep0669021-007</a></div>
                                        EP O 669 O21 B1

                eine Einrichtung zum Erhalten einer Anzeige der bevorzugten natürlichen Sprache für die Nachrichten des
                Computerprogramms;

                eine Einrichtung zum Laden des Nachrichtensatzes des Programm-Headers in den Arbeitsspeicher, wenn die
        5        bevorzugte natürliche Sprache gleich der natürlichen Sprache des Nachrichtensatzes des Programm-Headers
                ist_,

                eine Einrichtung zum Suchen des Nachrichtenfiles für einen Nachrichtensatz der bevorzugten natürlichen
                Sprache, wenn die bevorzugte natürliche Sprache nicht gleich der natürlichen Sprache des Nachrichtensatzes
        1O        des Programm-Headers ist;

                eine Einrichtung zum Laden des Nachrichtensatzes des Nachrichtenfiles in den Arbeitsspeicher, wenn der
                Nachrichtensatz für die bevorzugte natürliche Sprache nicht in dem Nachrichtenfile gefunden wird; und

        15        eine Einrichtung zum Erhalten der Nachrichten von dem geladenen Nachrichtensatz während der Ausführung
                des Computerprogramms.

           Revendi_ations
       2O   1 .  Procédé dans un système d'ordinateur pour communiquer des messages dans un langage naturel préféré d'un
              utilisateur du système, le système d'ordinateur comprenant un dispositif de mémorisation (2O3) et un dispositif de
              mémoire (2O2), le système d'ordinateur comprenant un programme d'ordinateur avec un en-tête de programme
              (2O4a) et un corps de programme (2O4b), l'en-tête de programme comprenant un ensemble de messages, le
       25     système comprenant un fichier de messages (2O5) avec plusieurs ensembles de messages, chaque ensemble
              de messages comprenant un ou plusieurs massages pour un langage naturel spécifique, ce procédé comprenant
              Ies étapes suivantes _.

                charger le corps de programme dans le dispositif de mémoire et exécuter le corps de programme ;
       3O        retrouver une indication du langage naturel préféré pour les messages du programme d'ordinateur (3O1 ) ;
                déterminer le langage naturel de l'ensemble de messages contenu dans l'en-tête de programme (3O2) ;
                quand le langage naturel déterminé est le même que le langage naturel préféré, charger l'ensemble de mes-
                sages de l'entête de programme dans le dispositif de mémoire dans lequel le corps de programme en exécution
                retrouve des messages à partir de l'ensemble de messages chargés (3O8, 311 ), et
       35        quand le langage naturel n'est pas le même que le langage naturel préféré, rechercher dans le fichier de
                messages un ensemble de messages pour le langage naturel préféré (3O5) ; et
                quand l'ensemble de messages pour le langage naturel préféré est trouvé dans le fichier de messages, charger
                l'ensemble de messages du fichier de messages dans le dispositif de mémoire dans lequel le corps de pro-
                gramme en exécution retrouve les messages à partir de l'ensemble de messages chargé (3O8, 311 ),
       4O   2.  Procédé selon la revendication 1 , dans lequel l'étape consistant à retrouver l'indication du langage naturel préféré
              comprend l'étape consistant à retrouver l'indication à partir du dispositif de mémoire (3O7).

           3.  Procédé selon la revendication 1 , dans lequel l'etape consistant à retrouver l'indication du langage naturel préféré
       45     comprend l'étape consistant à retrouver l'indication à partir d'un utilisateur du programme d'ordinateur.

           4.  Procédé selon la revendication 1 , comprenant en outre les étapes suivantes _.

                recevoir à partir de l'utilisateur une indication de l'emplacement du fichier de messages, et
       5O        charger l'ensemble de messages du fichier de messages indiqué par l'utilisateur dans le dispositif de mémoire
                dans lequel le corps de programme en exécution retrouve des messages à partir de l'ensemble de messages
                chargé.

           5.  Procéde selon l'une des revendications 1 à 4, comprenant en outre l'étape consistant, quand l'ensemble de mes-
       55     sages pour le langage naturel préféré n'est pas trouvé dans le fichier de messages, à charger l'ensemble de
              messages de l'entête de programme dans le dispositif de mémoire en tant que langage naturel par défaut, dans
              Iequel le corps de programme en execution retrouve les mesages à partir de l'ensemble de messages chargé.

                                              7
<hr>
<div align=center><a name=bpg8>gocr ep0669021-008</a></div>
                                        EP O 669 O21 B1

           6.  Système d'ordinateur pour sélectionner des messages dans un langage naturel préféré pour communiquer avec
              un utilisateur du système d'ordinateur, le système d'ordinateur comprenant un dispositif de mémorisation (2O3) et
              un dispositif de mémoire (2O2), le système d'ordinateur comprenant un programme d'ordinateur avec un en-tête
              de programme (2O4a) et un corps de programme (2O4b), l'en-tête de programme contenant un ensemble de mes-
        5      sages, le système d'ordinateur comportant un fichier de messages (2O5) avec plusieurs ensembles de messages,
              chaque ensemble de messages comprenant un ou plusieurs messages correspondant à un langage naturel spé-
              cifique, le système d'ordinateur comprenant_.

                des moyens pour retrouver une indication du langage naturel préféré pour les messages du programme
        1O        d'ordinateur _,
                des moyens pour charger l'ensemble de messages de l'entête de programme dans le dispositif de mémoire
                quand le langage naturel préféré est identique au langage naturel de l'ensemble de messages de l'en-tête de
                programme ;
                des moyens pour rechercher le fichier de messages correspondant à un ensemble de messages pour le
        15        langage naturel préféré quand le langage naturel préféré n'est pas le même que le langage naturel de l'en-
                semble de messages de l'en-tête de programme ;
                des moyens pour charger l'ensemble de messages du fichier de messages dans le dispositif de mémoire
                quand l'ensemble de messages du langage naturel préféré est trouvé dans le fichier de messages ; et
                des moyens pour retrouver des messages à partir de l'ensemble de messages chargé pendant l'exécution du
       2O        programme d'ordinateur.

       25

       3O

       35

       4O

       45

       5O

       55                                     8
<hr>
<div align=center><a name=bpg9>gocr ep0669021-009</a></div>
     (PICTURE)
            EP O 669 O21 B1

      (PICTURE)
                     lO_

     F_G_
     FXF_

     /O4

     (PICTURE)
                      lO3

              9
<hr>
<div align=center><a name=bpg10>gocr ep0669021-010</a></div>
          EPO669O21B1

           _
           __
     (PICTURE)
           _

    __
    _
    h_
    h
    __
    _
    __
    __
    _ _       1O
<hr>
<div align=center><a name=bpg11>gocr ep0669021-011</a></div>
        (PICTURE)(PICTURE)
                 EP O 669 O21 B1
<hr>
