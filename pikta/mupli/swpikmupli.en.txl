<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: European Software Patents: Assorted Examples

#descr: We came across the following impressive examples at first reading
through our tabular listings of software patents.  They were almost
randomly chosen and thus approximately represent the technicity and
inventivity standards of the European Patent Office.  If anything
distinguishes them from other patents it is the relative ease with
which they can be understood at first sight by a fairly large public.

#ep927945t: Amazon Gift Ordering

#ep927945d: If you want to program your online shop so that it delivers your
articles as gifts to a third person specified by the customer, you
might want to negotiate with Amazon Inc for a license.  This patent,
which is a direct descendant of Amazon's One Click Patent, was granted
by the European Patent Office (EPO) in May 2003.

#ep927945c: A method in a computer system for ordering a gift for delivery from a
gift giver to a recipient, the method comprising:%(ul|receiving from
the gift giver an indication that the gift is to be delivered to the
recipient and an electronic mail address of the recipient; and|sending
to a gift delivery computer system an indication of the gift and the
received electronic mail address, wherein the gift delivery computer
system coordinates delivery of the gift by|sending an electronic mail
message addressed to the electronic mail address of the recipient
requesting that the recipient provide delivery information including a
postal address for the gift; and|upon receiving the delivery
information, electronically initiating delivery of the gift in
accordance with the received delivery information.)

#ep902381t: Amazon 1Click

#ep902381d: Amazon's application for a patent on its One-Click Shopping method at
the EPO.  The application reached the third stage of examination (A3),
i.e. it was recognised as referring to a patentable invention and a
full novelty examination was conducted.  In 2001 the patent
application was split into two new applications, of which one was
granted and one is still pending.

#ep902381c: A method for placing an order to purchase an item, the order being
placed by a purchaser at a client system and received by a server
system, the method comprising: %(ul|under control of the server
system, receiving purchaser information including identification of
the purchaser, payment information, and shipment information from the
client system;|assigning a client identifier to the client
system;|associating the assigned client identifier with the received
purchaser information;|sending to the client system the assigned
client identifier; and|sending to the client system display
information identifying the item and including an order button;|under
control of the client system, receiving and storing the assigned
client identifier;|receiving and displaying the display information;
and|in response to the selection of the order button, sending to the
server system a request to purchase the identified item, the request
including the assigned identifier; and|under control of the server
system, receiving the request; and|combining the purchaser information
associated with the client identifier included with the request to
generate an order to purchase the item in accordance with the billing
and shipment information whereby the purchaser effects the ordering of
the product by selection of the order button.)

#ep533098t: Control of an apparatus by actuator check via microcomputer

#ep533098d: The main claim seems to cover all state-controlled automatic systems,
including general purpose computer programmed in such a way that
specific output values are controlled by specific input values.  The
main claim says that this device is %(q:preferably a car radio), but
does not limit itself to this specific application of a very general
calculation rule.  This patent was granted after 12 years of
examination, including opposition filed by an association dedicated to
keeping the broadcasting industry free of patents.

#ep533098c: %(linul|Device for controlling a telecommunications apparatus,
preferably a car radio, having a microcomputer, a memory, a control
part having control elements and having actuators, the microcomputer
being designed to use a program stored in the memory to acquire the
position of the control elements and to control the corresponding
functions of the device, characterized|in that the program contains a
table which contains start information items of program modules for
controlling the actuators for all possible operating states, a
respective operating state being provided by a setting of the
actuators and by an actuation state of the control elements,|in that
the microcomputer is designed in such a way that, upon actuation of a
control element, it carries out a search operation in the table, in
the case of which the operating states listed in the table are
compared as desired values with the actually present operating state
as actual value,|in that, following a successful search operation, the
microcomputer removes the start information item which has been found
from the table, and|in that the microcomputer executes one or more
program modules using the start information item.)

#ep287578t: Acoustic Data Compression -- MP3 Base Patent

#ep287578d: Iteratively perform certain calculations on acoustic data until a
certain value is reached.  The patent owner Karlheinz Brandenburg,
core researcher of the MP3 project at Max Planck, received this patent
in 1989.  This patent and its owner were showcased by the European
Commission's %(q:IPR Helpdesk) project in 2001 as %(q:inventor of the
month).  This is one of several dozen patents which cover the MP3
audio compression standard, and perhaps the most famous and basic one.
 It has always been treated as a model of how %(q:technical) and
%(q:non-trivial) software patents can get.

#ep287578c: %(linul|Digital coding process for transmitting and/or storing
acoustic signals, specifically music signals, comprising the following
steps.|N samples of the acoustic signal are converted into M spectral
coefficients;|said M spectral coefficients are subjected to
quantisation at a first level;|after coding by means of an entropic
encoder the number of bits required to represent all the quantized
spectral coefficients is checked;|when the required number of bits
does not correspond to a specified number of bits quantization and
coding are repeated in subsequent steps, each at a modified
quantization level, until the number of bits required for
representation reaches the specified number of bits, and|additionally
to the data bits the required quantization level is transmitted and/or
stored.)

#ep769170t: Trapping Viruses

#ep769170d: Creating an emulated computer environment and testing a data stream
within that environment before accepting it into the real computer
environment is a useful and difficult thing to do.  Anyone who
endeavors to do it will have to beg for a license from %(q:Quantum
Leap Technologies).

#ep767419t: IBM method and computer program product for displaying objects from
overlapping windows

#ep767419d: IBM fought hard for this patent and even pushed the EPO Technical
Board of Appeal to create a precedent in 1998 for violating Art 52 EPC
in order to get claims to a %(q:computer program product) granted. 
The teaching itself is abstract, functional and trivial: rearrange the
contents of a partially visible window so as to fit them within the
part of the window that is currently visible on the screen rather than
letting part of the contents be obscured by another window.  This
patent fully went into force on Jan 2001 after no opposition had been
filed.

#ep193933t: controlling one computer by another

#ep193933d: Originally the claims covered systems like RPC, Telnet and all kinds
of client-server computing.  After 10 years of examination, in 1995,
the EPO granted a version with greatly reduced claims to Wang
Laboratories.  Now the patent seems to covers all remote procedure
call systems where variables are stored in the protocol rather than
transmitted separately for each procedure.

#ep193933c: A digital data processing system which has a destination data
processing means coupled by message transport means to a source data
processing meansand wherein the source data processing means has
remote call providing means for providing a remote call to a callable
program in the destination data processing means, and the destination
data processing means has remote call receiving means for receiving
the remote call and performing a call to the callable program, and
comprises a call protocol which is produced by the remote call
providing means in the source data processing means, transferred via
the message transport means to the destination data processing means,
and which is received by the remote call receiving means and the call
protocol includes a program identifier and call parameters for
specifying the callable program, characterized in that %(ol|each call
protocol may contain an option part containing one or more optional
items, said item not being required in every call to the callable
program but being used by the remote call receiving means in making
the call when the optional items are present in the protocol,|each
optional item including a parameter value and an item tag specifying
the use of the parameter value in the call,|the remote call receiving
means using each optional item's parameter value in the call as
specified by the optional item's tag.)

#ep266049t: Coding System for Reducing Redundancy

#ep266049d: This patent was granted by the EPO in 1994 after 7 years of
examination, with priority date 1986. In 2002 it prompted Sony and
other companies to pay many million USD for using the JPEG standard
and made JPEG cease to be an international standard.

#ep266049c: An ordered redundancy method for coding digital signals, the digital
signals taking a plurality of different values, using two types of
%(tp|runlength code|R, R'), the method comprising the steps
of.%(ol|utilising the %(tp|first type of runlength code|R) for coding
a runlength of the most frequently occurring value followed by the
next most frequently occurring value;|utilising the %(tp|second type
of runlength code|R') for coding a runlength of the most frequently
occurring value followed by any value other than the next most
frequently occurring value; and|when the second type of runlength code
is used, following the runlength code value by a code value indicative
of the amplitude of said other value.)

#ep689133t: Adobe Patent on Tabbed Palettes

#dp689133d: This patent, granted by the EPO in Aug 2001, has been used by Adobe to
sue Macromedia Inc in the US.  The EP version took 6 years to examine,
and it was granted in full breadth, without any modification.  It
covers the idea of adding a third dimension to a menu system by
arranging several sets of options behind each other, marked with tabs.
 This is particularly found to be useful in image processing software
of Adobe and Macromedia, but also in The GIMP and many other programs.

#ep689133c: A method for combining on a computer display an additional set of
information displayed in a first area of the display and having
associated with it a selection indicator into a group of multiple sets
of information needed on a recurring basis displayed in a second area
of the screen, comprising the steps of %(ol|establishing the second
area on the computer display in which the group of multiple sets of
information is displayed, the second area having a size which is less
than the entire area of the computer display, the second area
displaying a first of the multiple sets of information;|providing
within the second area a plurality of selection indicators, each one
associated with a corresponding one of the multiple selecting a second
of the multiple sets of information for display within the second area
by activating a selection indicator associated with a second of the
multiple sets of information, whereby the second of the multiple sets
of information is substituted for the first of the multiple sets of
information within the area of the display; and|combining the
additional set of information, displayed in the first area of the
display into the group of multiple sets of information so that the
additional set of information may be selected using its selection
indicator in the same manner as the other sets of information in the
group.)

#ep487110t: automatable medical diagnosis

#ep487110d: The main claim seems to cover all medical diagnoses that can be
calculated automatically based on input of image and text data using a
conventional computer system, regardless of what the calculation is
based on.  Claims 2-3 extend the device by a network and a database. 
Claim 5 adds the %(q:invention) of marking analysis pictures as
%(q:already read by the doctor).  Subsequent claims and further
patents allow Shibaura to own more specific problems of organising
medical diagnoses.

#ep792493t: dynamic pricing

#ep792493d: Anyone who replaces a price tag with a user-editable pricing function
is infringing on a patent in Europe.  The following dependent claims
let the patentee become the owner of a large part of the problems of
modern e-commerce.

#ep328232t: digital signature with extra info

#ep328232d: In a system like PGP/GPG, add extra authentication info from a
certifier into the signature and you will be infringing on this
patent, no matter how your crypto algorithms work.  Although the main
claim after examination is very long and detailed, it is just as broad
as it was before examination, because most of the added information is
redundant.  It follows from the problem that is owned by this
patentee.

#ep644483t: Computer system and method for performing multiple tasks

#ep644483d: Computer system and method for performing multiple tasks A separate
interface is inserted between various applications and the main
terminal, so that the application and the user can communicate
independently with this interface. This makes it possible to let
processes run in the background.

#ep800142t: transforming file names

#ep803105t: EP 803105: Network Sales System

#ep803105d: This patent, granted to OpenMarket Inc by the European Patent Office
in 2002 after 6 years of examination, is identical to a system which
is currently being used in the USA to squeeze money out of various
e-commerce companies.

#ep803105c: %(linul|A network-based sales system, comprising|at least one buyer
computer for operation by a user desiring to buy a product;|at least
one merchant computer; and|at least one payment computer;|the buyer
computer, the merchant computer, and the payment computer being
interconnected by a computer network;|the buyer computer being
programmed to receive a user request for purchasing a product, and to
cause a payment message to be sent to the payment computer that
comprises a product identifier identifying the product;|the payment
computer being programmed to receive the payment message, to cause an
access message to be created that comprises the product identifier and
an access message authenticator based on a cyptographic key, and to
cause the access message to be sent to the merchant computer;|and the
merchant computer being programmed to receive the access message, to
verify the access message authenticator to ensure that the access
message authenticator was created using said cyptographic key, and to
cause the product to be sent to the user desiring to buy the product.)

#ep529915t: control by speech

#ep529915d: The main claim seems to cover almost any form of interfacing between a
terminal and multiple hosts that run on independent systems.  The
subclaims narrow it down to a specific application, but still don't
teach a solution but rather serve to occupy the problem in specific
contexts.

#ep747840t: dynamically extensible web server

#ep747840d: This seems to cover any webserver that processes HTML forms and
invokes a program via a common gateway interface, such that this
program returns a webpage.

#ep587827t: default feedback

#ep587827d: Send a preliminary message back to the screen, if due to a slow
network connection the program in the background can't send the final
message quickly enough.

#ep490624t: intuitive network configuration

#ep490624d: Represent the nodes in the network and their relations in a graphical
manner, e.g. as circles and arrows, editable by drag & drop, and
generate configuration files from the result.  This covers all
user-friendly network administration tools that are yet to be written.

#ep762304t: e-business broking

#ep762304d: One person tenders offers and confirms them within a determined
deadline if a bidder is found.

#ep688450t: flash file system

#ep688450d: This patent seems to make it nearly impossible to develop any
non-proprietary software for an important section of the embedded
devices market.  It claims the logcial consequences of the fact that
in flash memory blocks data can only be stored and replaced in blocks
of 64k or similar.  And it is not the only EPO-granted stumbling block
in this area.

#ep807891t: electronic shopping cart

#ep807891d: collect buyable items in a list and buy all of them at the end.

#ep823173t: compression in mobile communication

#ep823173d: This lets IBM own the problem of reducing data by multiplexing.  Any
mobile communication standard for TCP communication will have a hard
time working around that.

#ep522591t: translating human language to database query language

#ep522591d: Mitsubishi now owns the problem of translating natural language
questions into database queries by using parsers and virtual tables. 
No parsers are disclosed.

#ep242131t: visualising a process

#ep242131d: Visualise functions by graphically displaying their components,
allowing iterations on the screen and creating a flow chart from these
iterations.  Thanks to this patent, the attack of National Instruments
against MathWorks could be repeated in Europe.  Math simulation
programming would become very difficult. If such patents are
considered legally valid.

#ep242131c: %(nl|A method of producing a program modelling a physical process for
a computer using data flow diagramming, wherein the computer includes
a memory which stores a plurality of executable functions and a
plurality of data of various types, a display, means for receiving
user input, and a data processor, the method characterised by the
steps of| assembling, in response to user input, a data flow diagram
on the display, the data flow diagram specifying the process and
including function-icons corresponding to respective ones of the
plurality of executable functions, terminal-icons corresponding to
respective ones of the plurality of data types, a structure-icon
indicating control flow of the data flow diagram, and arcs
interconnecting the function-icons, terminal-icons, and
structure-icon; and|generating an executable program in response to
the data flow diagram, the executable program including one or more of
the executable functions as indicated by the function-icons and
interconnected as indicated by the arcs to operate upon data
identified by the terminal-icons, the executable program having
control flow established as indicated by the structure-icon.)

#ep359815t: distinguishing between used and unused memory blocks during cache
error handling

#ep359815d: Distinguish between used and unused blocks when refilling faulty
blocks in cache, so as to avoid spending unnecessary time on rewriting
already used blocks.  There are hundreds of EPO patents of this type. 
Try searching the database for words like %(q:cache) or %(q:memory).

#ep592062t: transmitting compression requests

#ep592062d: An application specifies a compression scheme for data communication,
which is then used by an independent communication server, such as a
MIME-conformant mail system.

#ep756731t: generating buying incentives from cooking recipes

#ep756731d: Calculate lists of things to buy with buying instructions, based on
cooking recipes specified by a user.  The %(q:technical contribution)
lies in the fact that a printer and a monitor are used.

#ep461127t: language learning by comparing one's pronounciation to that of a
teacher

#ep461127d: This covers all digital language learning systems that allow a user to
compare his pronounciation of a selected piece of text to the right
pronounciation.  As a byproduct, the claim also seems to include the
learning function of voice recognition systems like ViaVoice.

#ep664041t: testing learned material in schools

#ep664041d: use a computer for testing pupils.  The main claim covers the basic
procedure, the others just specify useful things to be done. The
%(q:technical contributions) consists in the teaching that a computer
can be used to do these things more efficiently.

#ep517486t: including context data in input

#ep517486d: A help system in which commands are context specific.   This patent
consists of only one claim, short and broad.  The description behind
it refers to a command environment for the Unix system which the
company allegedly created and which is similar to Niklas Wirth's
Oberon.

#ep825526t: Translating between 2 Objects

#ep825526d: Claim 1 seems to be Legalese for the following apparently patentable
discovery or its application to software objects:  If you speak only
English and me only German, we can nevertheless communicate, if at
least one of us brings an interpreter who knows both languages.

#ep825525t: CORBA object inheritance

#ep825525d: Claim 1 covers the generation of one CORBA object by another, a method
similar to %(q:inheritance) in object oriented programming.  A
software solution to a software problem in the narrowest sense.  One
might wonder what in this patent the EPO may have considered to be
%(q:not as such).

#ep794705t: improved breadmaker and a coding system therefor

#ep794705d: If you bake bread under program control, you will infringe on this
patent.  It doesn't matter how you place the sensors and peripheral
devices and how you program them:  if you run a baking process under
program control and offering the user a menu for selecting one of
several program settings, you need a license.  This patent does not
claim a computer program [ as such ] but rather a program-based
business method.  The causality between the means and the end is a
purely logical one: functionality is claimed without reference to any
novel way of using natural forces.  The main claim was granted by the
EPO without modification.  The patentee, an individual from Hongkong,
is apparently preparing to collect money from e-commerce based bread
delivery services in 11 European countries.

#ep797806t: binary oriented set sequencing

#ep797806d: An EPO patent, granted in 1997 with priority date 1994, on a method of
information organisation commonly used in a recent world wide web
standard.  According to this method, all information is structured
into atom identifiers and statements of relationsship between two
identifiers.  This allows a greater independence between data and
methods of processing these data.

#ep526034t: ATT patent on %(q:single-object file naming conventions)

#ep526034d: An exclusive right, granted to American Telephone and Telegraph by the
European Patent Office in 1993, on retrieving files by means of file
naming conventions which where uniquely identifying search criteria
(such as the inode number and generation sequence of the file) are
part of the filename or somehow encoded into it.  To put it in a
slightly simplified manner, this patent covers the idea retrieving
files by combining theadvantages of a name (easy to manage for the
end-user) and a sequential number (easy to handle for the computer).

#ep526034c: A computer based file apparatus for accessing a plurality of
previously-stored data files, the appartus comprising %(ul|means for
receiving a user request including a purported file name;|means for
comparing the purported file name with the file names of the
previously-stored data files and, if the purported file name matches
the file name of one of the previously-stored data files, retrieving
the previously stored data file whose file name matches purported file
name;|means for, if the purported file name does not match the file
name of one of the previously-stored data files, parsing the purported
file name using a first logical syntax to generate one or more
non-file-name-substring-based first search criteria; and|means for
comparing the one or more first search criteria with the
previously-stored data files and, if the one or more first search
criteria match one or more of the previosly-stored data files,
retrieving the or each previously stored data file which matches the
one or more first search criteria.)

#ep538888t: Pay per Use

#ep538888d: In 1993, the European Patent Office (EPO) granted Canon K.K. of Japan
owns a patent on charging a fee per a unit of decoded information. 
The main claim covers all systems where a local application decodes
information received from remote information distributor and
calculates a fee based on the amount of information decoded.  If an
information vendor wants to realise a full %(q:Pay Per Use) system
where the fee arises only when the user actually reads the information
(rather than when it is transmitted), he might want to beg Canon for a
license.  Perhaps Canon will be generous, since it is clear that the
patent claim describes a class of programs for computers
(computer-implemented calculation rules), and the supposedly novel and
inventive problem solution (invention) consists in nothing but the
program [ as such ].  All features of this claim belong to the field
of data processing by means of generic computer equipment.

#ep538888c: %(al|An information processing system comprising|%(ul|receiving means
for receiving transmitted information, the information being coded or
incomplete and in a non-usable form;|a recording medium, decoding
information needed to demodulate the received information being
prerecorded on said recording medium, said recording medium comprising
a first area in which the received information is to be written and a
second area in which the decoding information is pre-recorded;|writing
means for writing the information received by said receiving means in
the first area of said recording medium;|reading means for reading out
the decoding information from said recording medium; and|demodulating
means for converting the information received by said receiving means
and written in said first area of said recording medium to a usable
form using the decoding information read out by said reading
means;)|wherein the charge for use of the information is defined in
units corresponding to the decoding information being prerecorded on
said recording medium)

#de19838253t: improving network security by alternatingly disconnecting a computer
on 2 sides

#de19838253d: In order to better equip an intranet against cracker attacks, the
firewall is always physically cut off on one of its two sides, and
two-way communication is maintained by rapid alternation.  The
patentee asserts that this principle can make a network absolutely
secure, whereas current firewalls allow only relative security.  While
this assertion seems questionable, the fact is that anybody who does
the hard work of putting this obvious functionality to work will have
to beg for a license from Fraunhofer.

#de19747603t: Secure Commerce by mobile phone

#de19747603d: The German company Brokat now owns every %(q:process for digitally
signing a message by sending it to a signing device via the telco
network).

#ep497041t: information-controlled drug infusion

#ep497041d: using data processing means to control the way drugs are infused into
a patient's blood (body).  The main claim encompasses the whole
problem, for which solutions are being descsribed.   The patent isn't
limited to certain apparatusses or physical causalities and not even
to certain calculation rules.  It covers the business idea of using a
program for a certain purpose.

#suK: Exhibited Specimens

#ait: Candidates

#Wft: Patents for which we created a dedicated page

#ahx: The following evaluations come from the supporters of the FFII who
have registered in the %(ps:participation system).

#WmW: You can add examples to our collection.

#Wlr: You can evaluate the level of %(s:TECH)nicity as well as the quality
of the %(s:DEAL) which society gets when the EPO issues this patent.

#aeo: higher mathematics with large and diverse field of application

#tlr: application of universal computer to various tasks

#mTm: programming of special gadgets for information and communication, e.g.
card reader, mobile phone

#foj: control of machines which apply forces of nature, e.g. optimising fuel
use, anti-blocking system

#hWt: As above, but the problem solution seems to rely on knowledge about
forces of nature more than on informatic (control logic, automation
theory) skills.

#oif: The patent teaches something new about physical causality.  These
insights could not have been obtained without experimentation on
forces of nature.

#nae: invalid even according to the laxest patent office standards

#rbi: belongs into the horror gallery: worthless disclosure,
incommensurately great blocking effect (broad claim scope)

#nuW: reasonably interesting disclosure and moderate blocking effect

#SvW: belongs into the beauty gallery: precious disclosure, moderate
blocking effect

#rwW: Patents like this one should be granted:  society at large would make
a good bargain.

#Ptn: patent

#nrn: remarks

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpikmupli ;
# txtlang: en ;
# multlin: t ;
# End: ;

