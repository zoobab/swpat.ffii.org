
   Claims
   
   1. A computer-based file apparatus for accessing any of a plurality of
   previously-stored data objects, the apparatus characterized by
   means for receiving a user request including a purported file name
   containing one or more non-file-name-substring-based search criteria
   and
   means for locating one or more data objects using said search
   criteria.
   2. The apparatus of claim 1 further including
   means for returning a key associated with each located data object to
   the user, each key identifying the location of an associated data
   object.
   3. The apparatus of claim 2 wherein said key value is a Macintosh file
   identifier.
   4. The apparatus of claim 2 wherein said key value is a Macintosh
   directory identifier.
   5. The apparatus of claim 2 wherein the key is an inode index value.
   6. The apparatus of claim 2 wherein the key includes an inode index
   value and a generation number.
   7. The apparatus of claim 1 wherein at least one search criteria is a
   phonetic representation of a name.
   8. The apparatus of claim 1 wherein at least one search criteria is a
   relational characteristic which exists between two or more of said
   data objects.
   9. The apparatus of claim 1 wherein at least one search criteria is a
   substring which is to be matched against the contents of the
   previously-stored data objects.
   10. In a computer-based file system, a method of accessing any of a
   plurality of previously-stored data objects, the method comprising the
   steps of
   receiving a user request including a purported file name containing
   one or more non-file-name-substring-based search criteria and
   locating one or more data objects using said search criteria.
     _________________________________________________________________
   
   Data supplied from the esp@cenet database - l2
