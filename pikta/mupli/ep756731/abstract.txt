
   INTERACTIVE INFORMATION SELECTION APPARATUS  
   Patent Number: [_]  WO9529453
   Publication date: 1995-11-02
   Inventor(s): VAN LOOSBROEK ALBERT ADRIAAN M (NL)
   Applicant(s):: RECEPT OMAAT C V (NL); LOOSBROEK ALBERT ADRIAAN MARIA
   (NL)
   Requested Patent: [_]  EP0756731 (WO9529453),  B1
   Application Number: WO1995NL00147 19950421
   Priority Number(s): NL19940000643 19940421
   IPC Classification: G06F17/60 ; G06F17/30
   EC Classification: G06F17/60B2 ; G06F17/30E
   Equivalents: AU2319295, DE69505246D, DE69505246T, [_]  NL9400643
     _________________________________________________________________
   
   Abstract
     _________________________________________________________________
   
   An interactive information selection apparatus comprises: memory means
   in which all necessary information relating to at least one dish
   consisting of a number of components is or can be stored spread over a
   number of hierarchical levels; first display means for displaying in
   each case a list of options corresponding with one level, one of which
   options at a time can be selected by a user; selection means which can
   be operated by a user and which are adapted, after a start instruction
   and on the basis of menu control, to address that part of the memory
   containing partial information relating to the selected option; which
   memory means are adapted to activate the following level after
   addressing of the said memory part until the final level is reached
   and are also adapted to store each selected option; and second display
   means for displaying all relevant information present in the memory
   corresponding with all selected options.
     _________________________________________________________________
   
   Data supplied from the esp@cenet database - l2
