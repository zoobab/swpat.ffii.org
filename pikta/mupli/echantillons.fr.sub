\begin{subdocument}{swpikmupli}{Brevets Logiciels Europ\'{e}ens:  Quelques \'{E}chantillons}{http://swpat.ffii.org/brevets/echantillons/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise 2000/12/25 par PILCH Hartmut\footnote{mailto:phm@a2e.de?subject=http://swpat.ffii.org/brevets/echantillons/index.fr.html}}{En furetant un peu dans nos archives de brevets logiciels, nous avons trouv\'{e} ces exemples impressionnants.  Ils ont \'{e}t\'{e} choisis \`{a} peu pr\`{e}s par hasard et repr\'{e}sentent le standard de technicit\'{e} et d'inventivit\'{e} appliqu\'{e} par l'OEB. S'il y a quelque chose de sp\'{e}cial dans ces exemples, c'est le fait qu'ils peuvent assez facilement \^{e}tre rendu compr\'{e}hensibles pour le grand public.}
\begin{sect}{pag}{Exemplaire Exhibit\'{e}s}
Brevets avec une page FFII d\'{e}di\'{e}e

\begin{itemize}
\item
{\bf {\bf diagnostic m\'{e}dical automatique}\footnote{http://swpat.ffii.org/brevets/echantillons/ep487110/index.fr.html}}

\begin{quote}
The main claim covers all medical diagnoses that can be calculated automatically based on input of image and text data, regardless of what the calculation is based on.  Claims 2-3 extend the device by a network and a database.  Claim 5 adds the ``invention'' of marking analysis pictures as ``already read by the doctor''.  Subsequent claims and further patents allow Shibaura to own more specific problems of organising medical diagnoses.
\end{quote}
\filbreak

\item
{\bf {\bf prise de contr\^{o}le d'un ordinateur par un autre}\footnote{http://swpat.ffii.org/brevets/echantillons/ep193933/index.fr.html}}

\begin{quote}
Des syst\`{e}me comme RPC, Telnet et toute sorte de informatique client-serveur est en contrefa\c{c}on de ce brevet europ\'{e}en.
\end{quote}
\filbreak

\item
{\bf {\bf attraper un virus}\footnote{http://swpat.ffii.org/brevets/echantillons/ep769170/index.fr.html}}

\begin{quote}
Creating an emulated computer environment and testint a data stream within that environment before accepting it into the real computer environment is a useful and difficult thing to do.  Anyone who endeavors to do it will have to beg for a license from ``Quantum Leap Technologies''.
\end{quote}
\filbreak

\item
{\bf {\bf tarification dynamique}\footnote{http://swpat.ffii.org/brevets/echantillons/ep792493/index.fr.html}}

\begin{quote}
Anyone who replaces a price tag with a user-editable pricing function is infringing on a patent in Europe.  The following dependent claims let the patentee become the owner of a large part of the problems of modern e-commerce.
\end{quote}
\filbreak

\item
{\bf {\bf signature digitale avec informations suppl\'{e}mentaires d'authentification}\footnote{http://swpat.ffii.org/brevets/echantillons/ep328232/index.fr.html}}

\begin{quote}
In a system like PGP/GPG, add extra authentication info from a certifier into the signature and you will be infringing on this patent, no matter how your crypto algorithms work.  Although the main claim after examination is very long and detailed, it is just as broad as it was before examination, because most of the added information is redundant.  It follows from the problem that is owned by this patentee.
\end{quote}
\filbreak

\item
{\bf {\bf Computer system and method for performing multiple tasks}\footnote{http://swpat.ffii.org/brevets/echantillons/ep644483/index.fr.html}}

\begin{quote}
Computer system and method for performing multiple tasks A separate interface is inserted between various applications and the main terminal, so that the application and the user can communicate independently with this interface. This makes it possible to let processes run in the background.
\end{quote}
\filbreak

\item
{\bf {\bf transformation des noms de fichiers}\footnote{http://swpat.ffii.org/brevets/echantillons/ep800142/index.fr.html}}

\begin{quote}
transformation des noms de fichiers
\end{quote}
\filbreak

\item
{\bf {\bf prise de contr\^{o}le de plusieurs ordinateurs \`{a} l'aide de signaux linguistiques}\footnote{http://swpat.ffii.org/brevets/echantillons/ep529915/index.fr.html}}

\begin{quote}
The main claim seems to cover almost any form of interfacing between a terminal and multiple hosts that run on independent systems.  The subclaims narrow it down to a specific application, but still don't teach a solution but rather serve to occupy the problem in specific contexts.
\end{quote}
\filbreak

\item
{\bf {\bf serveur web dynamiquement extensible}\footnote{http://swpat.ffii.org/brevets/echantillons/ep747840/index.fr.html}}

\begin{quote}
This seems to cover any webserver that processes HTML forms and invokes a program via a common gateway interface, such that this program returns a webpage.
\end{quote}
\filbreak

\item
{\bf {\bf feedback par d\'{e}faut}\footnote{http://swpat.ffii.org/brevets/echantillons/ep587827/index.fr.html}}

\begin{quote}
Send a preliminary message back to the screen, if due to a slow network connection the program in the background can't send the final message quickly enough.
\end{quote}
\filbreak

\item
{\bf {\bf configuration r\'{e}seau intuitive}\footnote{http://swpat.ffii.org/brevets/echantillons/ep490624/index.fr.html}}

\begin{quote}
Represent the nodes in the network and their relations in a graphical manner, e.g. as circles and arrows, editable by drag \& drop, and generate configuration files from the result.  This covers all user-friendly network administration tools that are yet to be written.
\end{quote}
\filbreak

\item
{\bf {\bf syst\`{e}me de bourse sur Internet}\footnote{http://swpat.ffii.org/brevets/echantillons/ep762304/index.fr.html}}

\begin{quote}
One person tenders offers and confirms them within a determined deadline if a bidder is found.
\end{quote}
\filbreak

\item
{\bf {\bf IBM method and computer program product for displaying objects from overlapping windows}\footnote{http://swpat.ffii.org/brevets/echantillons/ep767419/index.en.html}}

\begin{quote}
IBM fought hard for this patent and even pushed the EPO Technical Board of Appeal to create a precedent in 1998 for violating Art 52 EPC in order to get claims to a ``computer program product'' granted.  The teaching itself is abstract, functional and trivial: rearrange the contents of a partially visible window so as to fit them within the part of the window that is currently visible on the screen rather than letting part of the contents be obscured by another window.  This patent fully went into force on Jan 2001 after no opposition had been filed.
\end{quote}
\filbreak

\item
{\bf {\bf syst\`{e}me de fichiers pour \'{e}l\'{e}ments de sauvegarde flash}\footnote{http://swpat.ffii.org/brevets/echantillons/ep688450/index.fr.html}}

\begin{quote}
This patent seems to make it nearly impossible to develop any non-proprietary software for an important section of the embedded devices market.  It claims the logcial consequences of the fact that in flash memory blocks data can only be stored and replaced in blocks of 64k or similar.  And it is not the only EPO-granted stumbling block in this area.)
\end{quote}
\filbreak

\item
{\bf {\bf caddy \'{e}lectronique}\footnote{http://swpat.ffii.org/brevets/echantillons/ep807891/index.fr.html}}

\begin{quote}
collect buyable items in a list and buy all of them at the end.
\end{quote}
\filbreak

\item
{\bf {\bf compression de donn\'{e}es dans les communications mobiles TCP}\footnote{http://swpat.ffii.org/brevets/echantillons/ep823173/index.fr.html}}

\begin{quote}
This lets IBM own the problem of reducing data by multiplexing.  Any mobile communication standard for TCP communication will have a hard time working around that.
\end{quote}
\filbreak

\item
{\bf {\bf traduction d'une langue naturelle en langue formelle pour interroger des bases de donn\'{e}es \`{a} l'aide de parseurs et de tableurs}\footnote{http://swpat.ffii.org/brevets/echantillons/ep522591/index.fr.html}}

\begin{quote}
Mitsubishi now owns the problem of translating natural language questions into database queries by using parsers and virtual tables.  No parsers are disclosed.
\end{quote}
\filbreak

\item
{\bf {\bf visualisation d'un processus}\footnote{http://swpat.ffii.org/brevets/echantillons/ep242131/index.fr.html}}

\begin{quote}
Visualise functions by graphically displaying their components, allowing iterations on the screen and creating a flow chart from these iterations.
\end{quote}
\filbreak

\item
{\bf {\bf distinction entre les blocs de m\'{e}moire utilis\'{e}s et non utilis\'{e}s pour traiter les fautes d'allocation dans le cadre d'un syst\`{e}me de cache}\footnote{http://swpat.ffii.org/brevets/echantillons/ep359815/index.fr.html}}

\begin{quote}
Distinguish between used and unused blocks when refilling faulty blocks in cache, so as to avoid spending unnecessary time on rewriting already used blocks.  There are hundreds of EPO patents of this type.  Try searching the database for words like ``cache'' or ``memory''.
\end{quote}
\filbreak

\item
{\bf {\bf transmission de requ\^{e}tes de compression}\footnote{http://swpat.ffii.org/brevets/echantillons/ep592062/index.fr.html}}

\begin{quote}
An application specifies a compression scheme for data communication, which is then used by an independent communication server, such as a MIME-conformant mail system.
\end{quote}
\filbreak

\item
{\bf {\bf fabrication de listes de courses \`{a} partir de recettes de cuisine pour augmenter les ventes dans un supermarch\'{e}}\footnote{http://swpat.ffii.org/brevets/echantillons/ep756731/index.fr.html}}

\begin{quote}
Calculate lists of things to buy with buying instructions, based on cooking recipes specified by a user.  The ``technical contribution'' lies in the fact that a printer and a monitor are used.
\end{quote}
\filbreak

\item
{\bf {\bf apprentissage des langues par comparaison entre les paroles de l'\'{e}l\`{e}ve et celles du professeur}\footnote{http://swpat.ffii.org/brevets/echantillons/ep461127/index.fr.html}}

\begin{quote}
This covers all digital language learning systems that allow a user to compare his pronounciation of a selected piece of text to the right pronounciation.  As a byproduct, the claim also seems to include the learning function of voice recognition systems like ViaVoice.
\end{quote}
\filbreak

\item
{\bf {\bf examens programm\'{e}s}\footnote{http://swpat.ffii.org/brevets/echantillons/ep664041/index.fr.html}}

\begin{quote}
use a computer for testing pupils.  The main claim covers the basic procedure, the others just specify useful things to be done. The ``technical contributions'' consists in the teaching that a computer can be used to do these things more efficiently.
\end{quote}
\filbreak

\item
{\bf {\bf int\'{e}gration de donn\'{e}es contextuelles}\footnote{http://swpat.ffii.org/brevets/echantillons/ep517486/index.fr.html}}

\begin{quote}
A help system in which commands are context specific.   This patent consists of only one claim, short and broad.  The description behind it refers to a command environment for the Unix system which the company allegedly created and which is similar to Niklas Wirth's Oberon.
\end{quote}
\filbreak

\item
{\bf {\bf M\'{e}thode pour Soutenir l'Interaction entre deux Unit\'{e}s}\footnote{http://swpat.ffii.org/brevets/echantillons/ep825526/index.de.html}}

\begin{quote}
Revendication 1 sembles d'\^{e}tre l'expression l\'{e}galoise de la suivante d\'{e}couverte apparemment brevetable quand elle est appliqu\'{e}e aux objets logiciels:  Si tu parle seulement l'Anglais et moi seulement le Francais, nous pouvons n\'{e}anmoins communiquer.  Il suffit si un de nous se fait accompagner par un interpr\`{e}te.
\end{quote}
\filbreak

\item
{\bf {\bf h\'{e}ritage d'objets CORBA}\footnote{http://swpat.ffii.org/brevets/echantillons/ep825525/index.de.html}}

\begin{quote}
Claim 1 covers the generation of one CORBA object by another, a method similar to ``inheritance'' in object oriented programming.  A software solution to a software problem in the narrowest sense.  One might wonder what in this patent the EPO may have considered to be ``not as such''.
\end{quote}
\filbreak

\item
{\bf {\bf Groupement binaire de donn\'{e}es}\footnote{http://swpat.ffii.org/brevets/echantillons/ep797806/index.fr.html}}

\begin{quote}
An EPO patent, granted in 1997 with priority date 1994, on a method of information organisation commonly used in a recent world wide web standard.  According to this method, all information is structured into atom identifiers and statements of relationsship between two identifiers.  This allows a greater independence between data and methods of processing these data.
\end{quote}
\filbreak

\item
{\bf {\bf am\'{e}lioration de s\'{e}curit\'{e} de r\'{e}seaux par coupure physique d'un ordinateur interm\'{e}diaire des deux cot\'{e}s en alternance}\footnote{http://swpat.ffii.org/brevets/echantillons/de19838253/index.fr.html}}

\begin{quote}
Pour mieux prot\'{e}ger un internet contre des attaques hacker, le mur de feu et toujours coup\'{e} sur un des deux cot\'{e}s.  Ainsi une communication en deux directions est maintenue par alternation rapide.  Le propri\'{e}taire de ce brevet affirme que ce principe simple peut procurer une s\'{e}curit\'{e} absolue a un intranet, tandis que les murs de feu actuelle procurent seulement une s\'{e}curit\'{e} relative.  M\^{e}me si cette assertion semble douteuse, tous ceux qui veulent faire fonctionner ce principe doit obtenir une permission de Fraunhofer.
\end{quote}
\filbreak

\item
{\bf {\bf Commerce s\'{e}cure par t\'{e}l\'{e}phone mobile}\footnote{http://swpat.ffii.org/brevets/echantillons/de19747603/index.de.html}}

\begin{quote}
En transmettant une message digitale a un appareil de signature pour le faire signer, n'importe comment vous r\'{e}aliser ce processus, vous etes d\'{e}ja en contrefacon de ce brevet.
\end{quote}
\filbreak

\item
{\bf {\bf machine a fabriquer le pain perfectionn\'{e}e et syst\`{e}me de codage associ\'{e}}\footnote{http://swpat.ffii.org/brevets/echantillons/ep794705/index.de.html}}

\begin{quote}
If you bake bread under program control, you will infringe on this patent.  It doesn't matter how you place the sensors and peripheral devices and how you program them:  if you run a baking process under program control and offering the user a menu for selecting one of several program settings, you need a license.  This patent does not claim a computer program as such but rather a program-based business method.  The causality between the means and the end is a purely logical one: functionality is claimed without reference to any novel way of using natural forces.  The main claim was granted by the EPO without modification.  The patentee, an individual from Hongkong, is apparently preparing to collect money from e-commerce based bread delivery services in 11 European countries.
\end{quote}
\filbreak

\item
{\bf {\bf information-controlled drug infusion}\footnote{http://swpat.ffii.org/brevets/echantillons/ep497041/index.en.html}}

\begin{quote}
using data processing means to control the way drugs are infused into a patient's blood (body).  The main claim encompasses the whole problem, for which solutions are being descsribed.   The patent isn't limited to certain apparatusses or physical causalities and not even to certain calculation rules.  It covers the business idea of using a program for a certain purpose.
\end{quote}
\filbreak

\item
{\bf {\bf Adobe Patent on Tabbed Palettes}\footnote{http://swpat.ffii.org/brevets/echantillons/ep689133/index.en.html}}

\begin{quote}
This patent, granted by the EPO in Aug 2001, has been used by Adobe to sue Macromedia Inc in the US.  The EP version took 6 years to examine, and it was granted in full breadth, without any modification.  It covers the idea of adding a third dimension to a menu system by arranging several sets of options behind each other, marked with tabs.  This is particularly found to be useful in image processing software of Adobe and Macromedia, but also in The GIMP and many other programs.
\end{quote}
\filbreak

\item
{\bf {\bf ATT patent on ``single-object file naming conventions''}\footnote{http://swpat.ffii.org/brevets/echantillons/ep526034/index.en.html}}

\begin{quote}
An exclusive right, granted to American Telephone and Telegraph by the European Patent Office in 1993, on retrieving files by means of file naming conventions which where uniquely identifying search criteria (such as the inode number and generation sequence of the file) are part of the filename or somehow encoded into it.  To put it in a slightly simplified manner, this patent covers the idea retrieving files by combining theadvantages of a name (easy to manage for the end-user) and a sequential number (easy to handle for the computer).
\end{quote}
\filbreak

\item
{\bf {\bf Frais de Lecture}\footnote{http://swpat.ffii.org/brevets/echantillons/ep538888/index.en.html}}

\begin{quote}
In 1993, the European Patent Office (EPO) granted Canon K.K. of Japan owns a patent on charging a fee per a unit of decoded information.  The main claim covers all systems where a local application decodes information received from remote information distributor and calculates a fee based on the amount of information decoded.  If an information vendor wants to realise a full ``Pay Per Use'' system where the fee arises only when the user actually reads the information (rather than when it is transmitted), he might want to beg Canon for a license.  Perhaps Canon will be generous, since it is clear that the patent claim describes a class of programs for computers (computer-implemented calculation rules), and the supposedly novel and inventive problem solution (invention) consists in nothing but the program [as such].  All features of this claim belong to the field of data processing by means of generic computer equipment.
\end{quote}
\filbreak
\end{itemize}
\end{sect}\begin{sect}{epat}{Candidats}
Vous pouvez nous aider a ajouter des exemples a cette collection!

You can evaluate the level of {\bf TECH}nicity as well as the quality of the {\bf DEAL} which society gets when the EPO issues this patent.

\begin{description}
\item[TECH:]\ \begin{enumerate}
\item
haute mathes avec champ d'application vaste

\item
application of universal computer to various tasks

\item
programming of special gadgets for information and communication, e.g. card reader, mobile phone

\item
control of machines which apply forces of nature, e.g. optimising fuel use, anti-blocking system

\item
As above, but the problem solution seems to rely on knowledge about forces of nature more than on informatic (control logic, automation theory) skills.

\item
The patent teaches something new about physical causality.  These insights could not have been obtained without experimentation on forces of nature.
\end{enumerate}
\item[DEAL:]\ \begin{enumerate}
\item
invalid even according to the laxest patent office standards

\item
belongs into the horror gallery: worthless disclosure, incommensurately great blocking effect (broad claim scope)

\item
reasonably interesting disclosure and moderate blocking effect

\item
belongs into the beauty gallery: precious disclosure, moderate blocking effect

\item
Patents like this one should be granted:  society at large would make a good bargain.
\end{enumerate}
\end{description}

cf Searching and Enriching the FFII Patent Database\footnote{http://www.ffii.org/verein/knecht/epat/index.de.html}

\dots\footnote{html only}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: latex ;
% End: ;

