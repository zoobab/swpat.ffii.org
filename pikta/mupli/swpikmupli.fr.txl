<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Brevets Logiciels Européens:  Quelques Échantillons

#descr: En furetant un peu dans nos archives de brevets logiciels, nous avons
trouvé ces exemples impressionnants.  Ils ont été choisis à peu près
par hasard et représentent le standard de technicité et d'inventivité
appliqué par l'OEB. S'il y a quelque chose de spécial dans ces
exemples, c'est le fait qu'ils peuvent assez facilement être rendu
compréhensibles pour le grand public.

#ep927945t: Amazon Ordre de Cadeaux

#ep927945d: Si vous voules programmer vôtre magasin en ligne en sorte qu'il puisse
fournir vos articles comme cadeaux à autres personnes, vous avez
besoin d'une license de Amazon.  Ce brevet, un déscendant directe du
brevet original Amazon One-Click Shopping, était accordé par l'OEB en
Mai 2003.

#ep927945c: Procédé dans un système informatique pour commander un cadeau à livrer
d'un donateur de cadeau à un destinataire, le procédé
comprenant:%(ul|la réception, en provenance du donateur de cadeau,
d'une indication que le cadeau doit être livré au destinataire et
d'une adresse de courrier électronique du déstinataire; et|l'envoi à
un système informatique de livraison de cadeau, d'une indication du
cadeau et de l'adresse de courrier électronique reçue, dans lequel le
système informatique de livraison de cadeau coordonne la livraison du
cadeau par|l'envoi (1 5O1 b) d'un message de courrier électronique
adressé à l'adresse de courrier électronique du destinataire,
l'adresse de courrier électronique demandant que le destinataire
fournisse des informations de livraison incluant une adresse postale
pour le cadeau; et|après réception des informations de livraison , le
lancement électronique de la livraison du cadeau conformément aux
informations de livraison reçues.)

#ep902381t: Amazon 1Click

#ep902381d: Amazon's application for a patent on its One-Click Shopping method at
the EPO.  The application reached the third stage of examination (A3),
i.e. it was recognised as referring to a patentable invention and a
full novelty examination was conducted.  In 2001 the patent
application was split into two new applications, of which one was
granted and one is still pending.

#ep902381c: A method for placing an order to purchase an item, the order being
placed by a purchaser at a client system and received by a server
system, the method comprising: %(ul|under control of the server
system, receiving purchaser information including identification of
the purchaser, payment information, and shipment information from the
client system;|assigning a client identifier to the client
system;|associating the assigned client identifier with the received
purchaser information;|sending to the client system the assigned
client identifier; and|sending to the client system display
information identifying the item and including an order button;|under
control of the client system, receiving and storing the assigned
client identifier;|receiving and displaying the display information;
and|in response to the selection of the order button, sending to the
server system a request to purchase the identified item, the request
including the assigned identifier; and|under control of the server
system, receiving the request; and|combining the purchaser information
associated with the client identifier included with the request to
generate an order to purchase the item in accordance with the billing
and shipment information whereby the purchaser effects the ordering of
the product by selection of the order button.)

#ep533098t: Control of an apparatus by actuator check via microcomputer

#ep533098d: The main claim seems to cover all state-controlled automatic systems,
including general purpose computer programmed in such a way that
specific output values are controlled by specific input values.  The
main claim says that this device is %(q:preferably a car radio), but
does not limit itself to this specific application of a very general
calculation rule.  This patent was granted after 12 years of
examination, including opposition filed by an association dedicated to
keeping the broadcasting industry free of patents.

#ep533098c: %(linul|Installation pour commander un appareil de transmission
d'informations de préférence un autoradio comprenant un
micro-ordinateur, une mémoire, une pièce de commande avec des éléments
de commande et des organes de réglage, le micro-ordinateur étant
réalisé pour saisir avec un programme enregistré dans une mémoire, la
position des éléments de commande et commander de façon appropriée les
fonctions corres- pondantes de l'installation, caractérisée en ce
que|le programme contient un tableau avec les informations de départ
pour les modules de programme pour commander les organes de réglage
pour tous les états de fonctionnement possibles, chaque état de
fonctionnement étant donné par le réglage des organes de réglage et
par un état d'actionnement des éléments de manoeuvre,|le
microprocesseur est réalisé pour qu'à l'actionnement d'un élément de
manoeuvre il eKécute une opération de recherche dans le tableau pour
laquelle les états de fonctionnement indiqués dans le tableau sont
comparés comme valeur de consigne aux états de fonctionnement réels
comme valeur réelle,|le microprocesseur reçoit l'information de départ
trouvée, dans le tableau si l'opération de recherche a réussi et,|le
microprocesseur exécute un ou plusieurs modules de programme en
utilisant l'information de départ.)

#ep287578t: Acoustic Data Compression -- MP3 Base Patent

#ep287578d: Iteratively perform certain calculations on acoustic data until a
certain value is reached.  The patent owner Karlheinz Brandenburg,
core researcher of the MP3 project at Max Planck, received this patent
in 1989.  This patent and its owner were showcased by the European
Commission's %(q:IPR Helpdesk) project in 2001 as %(q:inventor of the
month).  This is one of several dozen patents which cover the MP3
audio compression standard, and perhaps the most famous and basic one.
 It has always been treated as a model of how %(q:technical) and
%(q:non-trivial) software patents can get.

#ep287578c: %(linul|Procédé de codage numérique à transmettre et/ou enregistrer
des signaux acoustiques et en particulier des signaux musicaux,
comprenant les opérations suivantes.|N valeurs échantillonnées du
signal acoustique sont converties dans M coefficients
spectraux;|lesdits M coefficients spectraux subissent une
quantification à un premier niveau;|après le codage moyennant un
codeur entropique, le nombre de bits requis pour la représentation de
tous les coefficients spectraux quantifiés est vérifié,|quand le
nombre de bits requis ne correspond à un nombre de bits déterminé, la
quantification et le codage sont répétés dans des opérations
ultérieures, chacune à un niveau de quantifications modifiée, jusqu'à
ce que le nombre de bits requis pour la représentation atteint le
nombre de bits déterminé, et|supplémentairement aux bits de données,
le niveau de quantification requis est transmis etlou enregistré.)

#ep769170t: attraper un virus

#ep769170d: Creating an emulated computer environment and testint a data stream
within that environment before accepting it into the real computer
environment is a useful and difficult thing to do.  Anyone who
endeavors to do it will have to beg for a license from %(q:Quantum
Leap Technologies).

#ep767419t: IBM method and computer program product for displaying objects from
overlapping windows

#ep767419d: IBM fought hard for this patent and even pushed the EPO Technical
Board of Appeal to create a precedent in 1998 for violating Art 52 EPC
in order to get claims to a %(q:computer program product) granted. 
The teaching itself is abstract, functional and trivial: rearrange the
contents of a partially visible window so as to fit them within the
part of the window that is currently visible on the screen rather than
letting part of the contents be obscured by another window.  This
patent fully went into force on Jan 2001 after no opposition had been
filed.

#ep193933t: prise de contrôle d'un ordinateur par un autre

#ep193933d: Des systèmes comme RPC, Telnet et toute sorte de informatique
client-serveur était couverte par les revendication, mais après 10 ans
d'examination l'Office Européen de Brevet accordait ce brevet avec une
gamme réduite a Wang Laboratories.  Maintenant le brevet semble
couvrir toutes les invocation à distance de procédures ou les
variables sont stockées dans le protocol et ne pas nécéssairement
transmises séparément comme parametres lors de chaque invocation d'une
procédure.

#ep193933c: Système informatique numérique qui possède un moyen informatique
récepteur relié par un moyen d'acheminement de message à un moyen
informatique émetteur et dans lequel le moyen informatique émetteur
possède un moyen générateur d'appel à distance pour générer un appel à
distance à un programme pouvant être appelé du moyen informatique
récepteur, et le moyen informatique récepteur possède un moyen de
réception d'appel à distance pour recevoir l'appel à distance et
effectuer un appel du programme pouvant être appelé, et comprend un
protocole d'appel qui est produit par le moyen générateur d'appel à
distance du moyen informatique émetteur transmis par l'intermédiaire
du moyen d'acheminement de message au moyen informatique récepteur, et
qui est reçu par le moyen de réception d'appel à distance, le
protocole d'appel comprenant un identificateur de programme et des
paramètres d'appel pour désigner le programme pouvant être appelé,
caractérisé en ce que %(ol|chaque protocole d'appel peut contenir une
partie facultative contenant un ou plusieurs éléments facultatifs,
lesdits éléments n'étant pas nécessaires dans tous les appels du
programme pouvant être appelé mais étant utilisés par le moyen de
réception d'appel à distance lors de l'appel lorsque les éléments
facultatifs sont présents dans le protocole,|chaque élément
facultatifcomprenant une valeur de paramètre et un indicateur
d'élément indiquant l'utilisation de la valeur de paramètre de
l'appel,|le moyen de réception d'appel à distance utilisant chaque
valeur de paramètre d'élément facultatif de l'appel de la façon
indiquée par l'indicateur d'élément facultatif.)

#ep266049t: Système de Codage pour Réduire la Rédondance

#ep266049d: This patent was granted by the EPO in 1994 after 7 years of
examination, with priority date 1986. In 2002 it prompted Sony and
other companies to pay many million USD for using the JPEG standard
and made JPEG cease to be an international standard.

#ep266049c: Un procédé de redondance ordonnée pour coder des signaux numériques,
les signaux numériques prenant un ensemble de valeurs différentes,
utilisant %(tp|deux types de code de plage|R, R'), le procédé
comprenant les étapes suivantes.%(ol|on utilise le %(tp|premier type
de code de plage|R) pour coder une plage de la valeur qui a la
fréquence d'apparition la plus élevée, suivie par la valeur qui a la
fréquence d'apparition immédiatement inférieure;|on utilise le
%(tp|second type de code de plage|R') pour coder une plage de la
valeur ayant la fréquence d'apparition la plus élevée, suivie par une
valeur quelconque autre que la valeur ayant la fréquence d'apparition
immédiatement supérieure;|et lorsqu'on utilise, le second type de code
de plage, on fait suivre la valeur de code de plage par une valeur de
code qui indique l'amplitude de l'autre valeur précitée.)

#ep689133t: Adobe Patent on Tabbed Palettes

#dp689133d: This patent, granted by the EPO in Aug 2001, has been used by Adobe to
sue Macromedia Inc in the US.  The EP version took 6 years to examine,
and it was granted in full breadth, without any modification.  It
covers the idea of adding a third dimension to a menu system by
arranging several sets of options behind each other, marked with tabs.
 This is particularly found to be useful in image processing software
of Adobe and Macromedia, but also in The GIMP and many other programs.

#ep689133c: Procédé de combinaison sur un affichage d'ordinateur d'un ensemble
additionnel d'information affiché dans une première zone de
l'affichage et possédant en association un indicateur de sélection
dans un groupe de plusieurs ensembles d'information requis sur une
base récurrente affiché dans une seconde zone de l'écran, comprenant
les étapes suivantes: %(ol|l'établissement de la seconde zone sur
l'affichage d'ordinateur dans laquelle le groupe de plusieurs
ensembles d'information est affiché, la seconde zone ayant une
dimension qui est inférieure à la zone total de l'affichage
d'ordinateur, la seconde zone affichant un premier parmi plusieurs
ensembles d'information;|la prévision dans la seconde zone d'une
pluralité d'indicateurs de sélection, chacun d'eux étant associé à un
ensemble correspondant de la pluralité d'ensembles d'information,|la
sélection d'un second ensemble de la pluralité d'ensembles
d'information pour un afficha ge dans la seconde zone par activation
d'un indicateur de sélection associé à un second ensemble de la
pluralité d'ensembles d'information, le second ensemble de la
pluralité d'ensembles d'information étant ainsi substitué au premier
de la pluralité d'ensembles d'information dans la zone de l'affichage;
et|la combinaison d'un ensemble additionnel d'information, affiché
dans la première zone de l'affichage dans le groupe des multiples
ensembles d'information detelle fa_on que l'ensemble additionnel
d'information puisse etre sélectionné à l'aide de son indicateur de
sélection de la meme façon que les autres ensembles du groupe.)

#ep487110t: diagnostic médical automatique

#ep487110d: The main claim seems to cover all medical diagnoses that can be
calculated automatically based on input of image and text data using a
conventional computer system, regardless of what the calculation is
based on.  Claims 2-3 extend the device by a network and a database. 
Claim 5 adds the %(q:invention) of marking analysis pictures as
%(q:already read by the doctor).  Subsequent claims and further
patents allow Shibaura to own more specific problems of organising
medical diagnoses.

#ep792493t: tarification dynamique

#ep792493d: Anyone who replaces a price tag with a user-editable pricing function
is infringing on a patent in Europe.  The following dependent claims
let the patentee become the owner of a large part of the problems of
modern e-commerce.

#ep328232t: signature digitale avec informations supplémentaires
d'authentification

#ep328232d: In a system like PGP/GPG, add extra authentication info from a
certifier into the signature and you will be infringing on this
patent, no matter how your crypto algorithms work.  Although the main
claim after examination is very long and detailed, it is just as broad
as it was before examination, because most of the added information is
redundant.  It follows from the problem that is owned by this
patentee.

#ep644483t: Computer system and method for performing multiple tasks

#ep644483d: Computer system and method for performing multiple tasks A separate
interface is inserted between various applications and the main
terminal, so that the application and the user can communicate
independently with this interface. This makes it possible to let
processes run in the background.

#ep800142t: transformation des noms de fichiers

#ep803105t: EP803105: Système de Vente sur Reseau Informatique

#ep803105d: This patent, granted to OpenMarket Inc by the European Patent Office
in 2002 after 6 years of examination, is identical to a system which
is currently being used in the USA to squeeze money out of various
e-commerce companies.

#ep803105c: %(linul|Système de vente basé sur un réseau informatique,
comprenant|au moins un ordinateur acheteur destiné à être actionné par
un utilisateur désirant acheter un produit;|au moins un ordinateur
marchand;|et au moins un ordinateur de paiement;|l'ordinateur
acheteur, l'ordinateur marchand et l'ordinateur de paiement étant
reliés entre eux par un réseau informatique;|l'ordinateur acheteur
étant programmé pour recevoir une demande d'achat d'un produit de la
part d'un utilisateur, et pour provoquer l'envoi, à l'ordinateur de
paiement, d'un message de paiement qui comprend un identificateur de
produit identifiant le produit;|l'ordinateur de paiement étant
programmé pour recevoir le message de paiement, pour provouer la
création d'un message d'accès qui comprend l'identificateur de produit
et un authentiateur de message d'accès basé sur une clé
cyptographique, et pour provoquer l'envoi du message d'accès à
l'ordinateur marchand;|et l'ordinateur marchand étant programmé pour
recevoir le message d'accès, pour vérifier l'authentificateur de
message d'accès afin de s'assurer que l'authentificateur de message
d'accès a été créé en utilisant ladite clé cyptographique, et pour
provoquer l'envoi du produit à l'utilisateur désirant acheter ce
produit.)

#ep529915t: prise de contrôle de plusieurs ordinateurs à l'aide de signaux
linguistiques

#ep529915d: The main claim seems to cover almost any form of interfacing between a
terminal and multiple hosts that run on independent systems.  The
subclaims narrow it down to a specific application, but still don't
teach a solution but rather serve to occupy the problem in specific
contexts.

#ep747840t: serveur web dynamiquement extensible

#ep747840d: This seems to cover any webserver that processes HTML forms and
invokes a program via a common gateway interface, such that this
program returns a webpage.

#ep587827t: feedback par défaut

#ep587827d: Send a preliminary message back to the screen, if due to a slow
network connection the program in the background can't send the final
message quickly enough.

#ep490624t: configuration réseau intuitive

#ep490624d: Represent the nodes in the network and their relations in a graphical
manner, e.g. as circles and arrows, editable by drag & drop, and
generate configuration files from the result.  This covers all
user-friendly network administration tools that are yet to be written.

#ep762304t: système de bourse sur Internet

#ep762304d: One person tenders offers and confirms them within a determined
deadline if a bidder is found.

#ep688450t: système de fichiers pour éléments de sauvegarde flash

#ep688450d: This patent seems to make it nearly impossible to develop any
non-proprietary software for an important section of the embedded
devices market.  It claims the logcial consequences of the fact that
in flash memory blocks data can only be stored and replaced in blocks
of 64k or similar.  And it is not the only EPO-granted stumbling block
in this area.)

#ep807891t: caddy électronique

#ep807891d: collect buyable items in a list and buy all of them at the end.

#ep823173t: compression de données dans les communications mobiles TCP

#ep823173d: This lets IBM own the problem of reducing data by multiplexing.  Any
mobile communication standard for TCP communication will have a hard
time working around that.

#ep522591t: traduction d'une langue naturelle en langue formelle pour interroger
des bases de données à l'aide de parseurs et de tableurs

#ep522591d: Mitsubishi now owns the problem of translating natural language
questions into database queries by using parsers and virtual tables. 
No parsers are disclosed.

#ep242131t: visualisation d'un processus

#ep242131d: Visualise functions by graphically displaying their components,
allowing iterations on the screen and creating a flow chart from these
iterations.

#ep242131c: %(nl|A method of producing a program modelling a physical process for
a computer using data flow diagramming, wherein the computer includes
a memory which stores a plurality of executable functions and a
plurality of data of various types, a display, means for receiving
user input, and a data processor, the method characterised by the
steps of| assembling, in response to user input, a data flow diagram
on the display, the data flow diagram specifying the process and
including function-icons corresponding to respective ones of the
plurality of executable functions, terminal-icons corresponding to
respective ones of the plurality of data types, a structure-icon
indicating control flow of the data flow diagram, and arcs
interconnecting the function-icons, terminal-icons, and
structure-icon; and|generating an executable program in response to
the data flow diagram, the executable program including one or more of
the executable functions as indicated by the function-icons and
interconnected as indicated by the arcs to operate upon data
identified by the terminal-icons, the executable program having
control flow established as indicated by the structure-icon.)

#ep359815t: distinction entre les blocs de mémoire utilisés et non utilisés pour
traiter les fautes d'allocation dans le cadre d'un système de cache

#ep359815d: Distinguish between used and unused blocks when refilling faulty
blocks in cache, so as to avoid spending unnecessary time on rewriting
already used blocks.  There are hundreds of EPO patents of this type. 
Try searching the database for words like %(q:cache) or %(q:memory).

#ep592062t: transmission de requêtes de compression

#ep592062d: An application specifies a compression scheme for data communication,
which is then used by an independent communication server, such as a
MIME-conformant mail system.

#ep756731t: fabrication de listes de courses à partir de recettes de cuisine pour
augmenter les ventes dans un supermarché

#ep756731d: Calculate lists of things to buy with buying instructions, based on
cooking recipes specified by a user.  The %(q:technical contribution)
lies in the fact that a printer and a monitor are used.

#ep461127t: apprentissage des langues par comparaison entre les paroles de l'élève
et celles du professeur

#ep461127d: This covers all digital language learning systems that allow a user to
compare his pronounciation of a selected piece of text to the right
pronounciation.  As a byproduct, the claim also seems to include the
learning function of voice recognition systems like ViaVoice.

#ep664041t: examens programmés

#ep664041d: use a computer for testing pupils.  The main claim covers the basic
procedure, the others just specify useful things to be done. The
%(q:technical contributions) consists in the teaching that a computer
can be used to do these things more efficiently.

#ep517486t: intégration de données contextuelles

#ep517486d: A help system in which commands are context specific.   This patent
consists of only one claim, short and broad.  The description behind
it refers to a command environment for the Unix system which the
company allegedly created and which is similar to Niklas Wirth's
Oberon.

#ep825526t: Méthode pour Soutenir l'Interaction entre deux Unités

#ep825526d: Revendication 1 sembles d'être l'expression légaloise de la suivante
découverte apparemment brevetable quand elle est appliquée aux objets
logiciels:  Si tu parle seulement l'Anglais et moi seulement le
Francais, nous pouvons néanmoins communiquer.  Il suffit si un de nous
se fait accompagner par un interprète.

#ep825525t: héritage d'objets CORBA

#ep825525d: Claim 1 covers the generation of one CORBA object by another, a method
similar to %(q:inheritance) in object oriented programming.  A
software solution to a software problem in the narrowest sense.  One
might wonder what in this patent the EPO may have considered to be
%(q:not as such).

#ep794705t: machine a fabriquer le pain perfectionnée et système de codage associé

#ep794705d: If you bake bread under program control, you will infringe on this
patent.  It doesn't matter how you place the sensors and peripheral
devices and how you program them:  if you run a baking process under
program control and offering the user a menu for selecting one of
several program settings, you need a license.  This patent does not
claim a computer program [ as such ] but rather a program-based
business method.  The causality between the means and the end is a
purely logical one: functionality is claimed without reference to any
novel way of using natural forces.  The main claim was granted by the
EPO without modification.  The patentee, an individual from Hongkong,
is apparently preparing to collect money from e-commerce based bread
delivery services in 11 European countries.

#ep797806t: Groupement binaire de données

#ep797806d: An EPO patent, granted in 1997 with priority date 1994, on a method of
information organisation commonly used in a recent world wide web
standard.  According to this method, all information is structured
into atom identifiers and statements of relationsship between two
identifiers.  This allows a greater independence between data and
methods of processing these data.

#ep526034t: ATT patent on %(q:single-object file naming conventions)

#ep526034d: An exclusive right, granted to American Telephone and Telegraph by the
European Patent Office in 1993, on retrieving files by means of file
naming conventions which where uniquely identifying search criteria
(such as the inode number and generation sequence of the file) are
part of the filename or somehow encoded into it.  To put it in a
slightly simplified manner, this patent covers the idea retrieving
files by combining theadvantages of a name (easy to manage for the
end-user) and a sequential number (easy to handle for the computer).

#ep526034c: Système informatique numérique qui possède un moyen informatique
récepteur relié par un moyen d'acheminement de message à un moyen
informatique émetteur et dans lequel le moyen informatique émetteur
possède un moyen générateur d'appel à distance pour générer un appel à
distance à un programme pouvant être appelé du moyen informatique
récepteur, et le moyen informatique récepteur possède un moyen de
réception d'appel à distance pour recevoir l'appel à distance et
effectuer un appel du programme pouvant être appelé, et comprend un
protocole d'appel qui est produit par le moyen générateur d'appel à
distance du moyen informatique émetteur transmis par l'intermédiaire
du moyen d'acheminement de message au moyen informatique récepteur, et
qui est reçu par le moyen de réception d'appel à distance, le
protocole d'appel comprenant un identificateur de programme et des
paramètres d'appel pour désigner le programme pouvant être appelé,
caractérisé en ce que %(ol|chaque protocole d'appel peut contenir une
partie facultative contenant un ou plusieurs éléments facultatifs,
lesdits éléments n'étant pas nécessaires dans tous les appels du
programme pouvant être appelé mais étant utilisés par le moyen de
réception d'appel à distance lors de l'appel lorsque les éléments
facultatifs sont présents dans le protocole,|chaque élément
facultatifcomprenant une valeur de paramètre et un indicateur
d'élément indiquant l'utilisation de la valeur de paramètre de
l'appel,|le moyen de réception d'appel à distance utilisant chaque
valeur de paramètre d'élément facultatif de l'appel de la façon
indiquée par l'indicateur d'élément facultatif.)

#ep538888t: Frais de Lecture

#ep538888d: In 1993, the European Patent Office (EPO) granted Canon K.K. of Japan
owns a patent on charging a fee per a unit of decoded information. 
The main claim covers all systems where a local application decodes
information received from remote information distributor and
calculates a fee based on the amount of information decoded.  If an
information vendor wants to realise a full %(q:Pay Per Use) system
where the fee arises only when the user actually reads the information
(rather than when it is transmitted), he might want to beg Canon for a
license.  Perhaps Canon will be generous, since it is clear that the
patent claim describes a class of programs for computers
(computer-implemented calculation rules), and the supposedly novel and
inventive problem solution (invention) consists in nothing but the
program [ as such ].  All features of this claim belong to the field
of data processing by means of generic computer equipment.

#ep538888c: %(al|Système de traitement d'information comprenant %(ul|des moyens de
réception pour recevoir une information émise, l'information étant
codée ou incomplète et sous une forme non utilisable;|un support
d'enregistrement, une information de décodage nécessaire pour
démoduler l'information reçue étant pré-enregistrée sur ledit support
d'enregistrement, ledit support d'enregistrement comprenant une
première zone dans laquelle l'information reçue doit etre écrite et
une deuxième zone dans laquelle l'information de décodage est
pré-enregistrée;|des moyens d'écriture pour écrire l'information reçue
par lesdits moyens de réception dans la première zone dudit support
d'enregistrement;|des moyens de lecture pour lire l'information de
décodage à partir dudit support d'enregistrement;|et des moyens de
démodulation pour convertir l'information reçue par lesdits moyens de
réception et écrite dans ladite première zone dudit support
d'enregistrement sous une forme utilisable à l'aide de l'information
de décodage par lesdits moyens de lecture;) dans lequel la charge pour
l'utilisation de l'information est définie par unités correspondant à
l'information de décodage qui est pré-enregistrée sur ledit support
d'enregistrement.)

#de19838253t: amélioration de sécurité de réseaux par coupure physique d'un
ordinateur intermédiaire des deux cotés en alternance

#de19838253d: Pour mieux protéger un internet contre des attaques hacker, le mur de
feu et toujours coupé sur un des deux cotés.  Ainsi une communication
en deux directions est maintenue par alternation rapide.  Le
propriétaire de ce brevet affirme que ce principe simple peut procurer
une sécurité absolue a un intranet, tandis que les murs de feu
actuelle procurent seulement une sécurité relative.  Même si cette
assertion semble douteuse, tous ceux qui veulent faire fonctionner ce
principe doit obtenir une permission de Fraunhofer.

#de19747603t: Commerce sécure par téléphone mobile

#de19747603d: En transmettant une message digitale a un appareil de signature pour
le faire signer, n'importe comment vous réaliser ce processus, vous
etes déja en contrefacon de ce brevet.

#ep497041t: information-controlled drug infusion

#ep497041d: using data processing means to control the way drugs are infused into
a patient's blood (body).  The main claim encompasses the whole
problem, for which solutions are being descsribed.   The patent isn't
limited to certain apparatusses or physical causalities and not even
to certain calculation rules.  It covers the business idea of using a
program for a certain purpose.

#suK: Exemplaire Exhibités

#ait: Candidats

#Wft: Brevets avec une page FFII dédiée

#ahx: Les évaluations suivantes viennent des supporters qui s'ont registré
dans nôtre système de participation.

#WmW: Vous pouvez nous aider a ajouter des exemples a cette collection!

#Wlr: You can evaluate the level of %(s:TECH)nicity as well as the quality
of the %(s:DEAL) which society gets when the EPO issues this patent.

#aeo: haute mathes avec champ d'application vaste

#tlr: application of universal computer to various tasks

#mTm: programming of special gadgets for information and communication, e.g.
card reader, mobile phone

#foj: control of machines which apply forces of nature, e.g. optimising fuel
use, anti-blocking system

#hWt: As above, but the problem solution seems to rely on knowledge about
forces of nature more than on informatic (control logic, automation
theory) skills.

#oif: The patent teaches something new about physical causality.  These
insights could not have been obtained without experimentation on
forces of nature.

#nae: invalid even according to the laxest patent office standards

#rbi: belongs into the horror gallery: worthless disclosure,
incommensurately great blocking effect (broad claim scope)

#nuW: reasonably interesting disclosure and moderate blocking effect

#SvW: belongs into the beauty gallery: precious disclosure, moderate
blocking effect

#rwW: Patents like this one should be granted:  society at large would make
a good bargain.

#Ptn: Brevet

#nrn: observations

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpikmupli ;
# txtlang: fr ;
# multlin: t ;
# End: ;

