                                        EP O 747 840 B1
or C. These routines become part of the capsule object by the reference, and these routines perform such functions
as account tracking, compression, calculation, handling specific custom outputs such as video, voice, translation, and
enable programmability of the capsule objects. The capsule objects also have standard object capability, and we will
illustrate these by way of the specific e Kamples described.
5   [OO15]  lt will be seen that the control program 73, described in detail in FIGURE 8 acts in concert with DIS capsule
eKecution. The DIS capsule is an object program with eKecutable additions which we have created to interact with the
control program. It should be also understood that the DIS capsule object can perform programmable functions on
data which is retrieved from databases. Not only can a DIS capsule get data, it can combine, reformat, and update,
the data retrieved. It can act on the data to create new data and basically act as a dedicated processor processing
IO   data gathered or created during a Web browser request to output the end result to the user under programmable
parameters determined by the creator of the DIS capsule, as they may be selected, if desired, by the user as part of
the request. Thus the user entered inputs as part of his request, either free form or by selection of variables in the
menus afforded to the user as illustrated by way of eKample in FIGURE 5.
[OO16]  DIS capsule objects are like some other objects. For instance in Microsoft's products, an eKample being the
15   EKcel (trademark of Microsoft) spreadsheet, one can click on an object portrayed on the screen and link a succession
of objects to perform a specific function, such as take data from a spreadsheet and reformat it into a variety of selectable
formats, such as teKt or graphic illustration. The kind of action to be taken is illustrated by an object on the screen, and
Iinking of routines is done by a succession of clicks on icons representing the object.
[OOll]  ln accordance with our preferred embodiment, a DIS capsule is used to invoke system resources. This is
20   done by providing a list of commands, which may be those provided by a DIS processor itself, or written in Vsual Basic
or C by the programmer. The result is a command file, like an eKec or command file in OS12 or like a x. BAT file in DOS.
These capsules perform the specific functions that are requested by the user from his initiation session. The user
further qualifies the eKecution of the DIS capsule by providing parameters which are used in the invocation.
[OO18]  Now the DIS server 1 33 supports DIS, the program processor which supports DIS capsules by processing
25   commands contained in the DIS capsule, either directly, in the case of DIS functions, or by to other system or user
supplied functions. The user supplied functions comprise mainly those DIS functions which are supplied by DIS and
illustrated in the manual ''Developing Applications with OpenDIS Access Service, Version 2.O of the OPEN Access
Service.'' Forthose not familiar with command files, this manual is fully incorporated herein bythis reference as available
at the USPTO. An eKample of a system supplied function would be the base support for SQL queries of a specific
30   database, which are invoked by the DIS capsule program.
[OO19]  ln illustrating the specific eKamples of our invention illustrated in FIGURES 9 and 1 O, both illustrate linked
objects according to a specified flow sequence within a DIS environment. The DIS environment contains numerous
functions, including the lnternetwork routing functions which the DIS capsules can invoke. Thus, a DIS object which
queries a database, as illustrated, invokes the lnternetwork routing functions to query databases where they are located
35   on the network. If the preferred eKample of DIS environment is not supplied, a similar environment with program en-
vironment means which supports reaching a destination on the lnternet by a link between systems which routes data
from one physical unit to another according to the applicable protocol should be supplied. The protocol will employ a
URL address for lnternet locations.
[O080]  FIGURE 9 illustrates by way of eKample a DIS capsule that creates a teKt report file. Referring to FIGURE 9,
40   it will be seen that the capsule, represented by a series of linked objects, is supported by lnternetwork processor
support environment means 90. Within this environment an integrated capsule creates a teKt report file as a result of
the object 95, make teKt. This object result file is the file 43 according to FIGURE 3 which is displayed at the browser.
In the illustrated eKample, the multiple DIS capsule data retrieval command file 91 (a)..91 (n) initiates as a first step
multiple queries to different databases which are specified by the parameters of the request. In this eKample, multiple
45   queries are initiates as SQL type search requests as multiple steps 91 (a)..91 (n) eKecuted by the DIS capsule server
1 33 with the Database Gateway 1 34 to select data from DB26000 databases located inside the intranet 1 40 and on
the lnternet by lnternetwork routing to database gateway 1 34' and its DB26000 databases by step 91 (a). The data is
stored in a DIS declared buffer. Similarly, in parallel or successively, additional steps 91 (b), 91 (c), 91 (d), and 91 (n)
retrieve data and store in their object buffer data retrieved from Sybase, Oracle, Redbrick, and l BM's Data Warehouse
50   databases. Thus object 91 (a) will query DB26000 and bring data back to DIS. Object 91 (b) will query Oracle and bring
data back to DIS. Object 91 (c) will quey Sybase and bring data back to DIS. Object 91 (d) (shown as a dot in FIGURE
9) will query Redbrick and bring data back to DIS, and so on. The nth object 91 (n) will query l BM's data warehouse
and bring data back to DIS.ln a subsequent linked processing step 92 data from the database queries in the first step
is joined by joining object command file 92 and stored in a buffer related to this object. Object 92 will joint the data from
55   the n locations searched in step 91. Thereafter, in a subsequent processing step performed by calculation object com-
mand file 93 on the joined data in the joined database result buffer of step 92, desired calculations performed in ac-
cordance with the parameters indicated by the request are done on the joined data. Thereafter, in accordance with the
request parameters teKt is formatted to space delimited teKt by the format object command file 94. The results are
1 2
