                                        EP O 747 840 B1
invested in being able to gather information from an intranet resource, can use their own direct resources, and also
be interrogated by the Web browser(s) 1 30, or another web browser 1 36. Remember that browser's 1 30 can also
communicate with the Web server 1 31 ' across the lnternet, just as can a Web browser 1 36 located on the intranet 1 40
inside of the firewall illustrated by the intranet 1 40 dashed line shown in FIGURE 1 1. With a browser 1 36 in place at
5   the Web Server 1 31 ' location, that browser 1 36 can make requests, if authorized across the intranet to the Web Server
1 31 which can then utilize the DIS capsules provided by the DIS Server 1 33.
[O081]  Physically, the network 1 32 will have its own access server 1 35 preferably in the form of a TCPIIP server 1 35
to make the physical connection across the lnternet. We illustrate in FIGURE 1 1 this other logical layer as located on
the network. This TCPIIP server supports the physical connections which are needed by the other logical higher levels
IO   of service supported on the network. The use of an lnterNetwork Routing Protocol (l NR) allows the logical coupling
illustrated between a application processing server 1 34 to an eKternal intranet application processing server 1 34'. On
each network there can be one or more web servers. A HyperteKt document request asking for a field to be seached,
as by a Hyperlink, could indeK to a server directly, e.g. a second web server 1 34'' on the same network which would
have its own control program agent function duplicating the control program agent resident in web server 1 34. Thus
15   at the request homepage a menu which say if ''Art&Literature search'', when selected in a Hyperlink setting, would
indeK to a particular web server and a particular document within that web server's environment. This web server 1 34''
besides being linked to its own application processing server 1 33'' has a direct link, in the environment illustrated, to
an MVS CICS, a transaction processing server for handling transaction processing. Such a solution allows CICS trans-
action processing to utilize the lnternet to save transmission costs and still be located beneath a firewall for retention
20   of data integrity. The outputs provided by the web server to the requested destination can be outside of the firewall,
and in the form of results illustrated by the possible eKamples shown in FIGURES 3, 5 and 8.
[O088]  While we have described our preferred embodiments of our invention, it will be understood that those skilled
in the art, both now and in the future, may make various improvements and enhancements which fall within the scope
of the claims which follow. These claims should be construed to maintain the proper protection for the invention first
25   disclosed.
[O089]  The following features - for themselves or in combination with other features- are also characteristics of the
invention.
-   A result according to the request command is provided during processing by said control program agent, which in
30     default is a return to the Web browser home page.
-   lf required for control by a decision support system environment for said command file, logging onto a port or
desktop for the assigned user is performed by the control program agent.
35   -   While the program agent reads the file identified, it dynamically creates new HTML statements for a report of
results according to a request of the web browser.
-   lnformation is retrieved from the file with new HTML statements so that it can be provided with the results of a
command file report.
40   -   lnformation is retrieved from the file with new HTML statements so that it can be displayed as a header accompa-
nying the report to be displayed, along with the filename.
-   lf a teKt file report is created by the DIS capsule, that determines that a teKt display is to be reported and the control
45      program agent reads the file created by the DIS capsule and dynamically creates HTML statements to display the
data lines to the Web browser.
-   lf a graphics file is created by the DIS capsule, that determines that a graphics display is to be reported and the
control program dynamically creates the HTML statement to display the graphics file to the Web browser.
50   -   The control program agent allows alternative output direction, and upon receipt of output direction and a command
file completion signal, the result is routed to the output destination.
-   lf selected data according to the request is permitted to the access authorized user at the location inside or outside
55     the lnternet, the data can be included in the results reported by the system to the Web browser.
-   The control program agent tests for the kind of report to be created by obtaining information from stored variables,
and identifies output parameters, and branches to the sequence applicable to the kind of report to be created.
1 4
