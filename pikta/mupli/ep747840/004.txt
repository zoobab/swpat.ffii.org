                                        EP O 747 840 B1
Hyper TeKt Markup Language (HTML)    HTML is the language used by Web servers to create and connect docu-
ments that are viewed by Web clients. HTML uses HyperteKt documents.
Other uses of  HyperteKt documents are described  in  U.S.  Patents
5,204,947, granted April 20, 1 993 to Bernstein et al.; 5,297,249, granted
5                             March 22, 1 994 to Bernstein et al.; 5,355,472, granted October 1 1, 1 994 to
Lewis; all of which are assigned to lnternational Business Machines Corpo-
ration, and which are referenced herein.
BACKGROUND OF THE l NVENTIONS
IO   [OO06]  The lnternet is not a single network, it has no owner or controller, but is an unruly network of networks, a
confederation of many different nets, public and private, big and small, that have agreed to connect to one another.
An intranet is a network which is restricted and while it may follow the lnternet protocol, none or only part of the network
available from outside a ''firewall'' surrounding the intranet is part of the agreed connection to the lnternet. The composite
15   network represented by these networks relies on no single transmission medium, bi-directional communication can
occur via satellite links, fiber-optic trunk lines, phone lines, cable TV wires and local radio links. When your client
computer logs onto the lnternet at a university, a corporate office or from home, eveything looks local, but the access
to the network does cost time and line charges.
[OOOl]  Until recently, ''cruising or surfing'' the lnternet was a disorienting, even infuriating eKperience, something like
20   trying to navigate without charts. The World Wide Web, a sub-network of the lnternet, introduced about two years ago,
made it easier by letting people jump from one server to another simply by selecting a highlighted word, picture or icon
(a program object representation) about which they want more information -- a maneuver known as a ''hyperlink''. In
order to eKplore the WWW today, the user loads a special navigation program, called a ''Web browser'' onto his com-
puter. While there are several versions of Web browsers, l BM's eKample is the new WebEKplorer which offers users
25   of l BM's OS12 Warp system software a consistent, easy to use desktop of pictorial icons and pull down menus. As part
of a group of integrated applications available from l BM for OS12 Warp called the IBM lnternet Connection, lets users
Iog onto the lnternet.
[OO08]  To this point the World Wide Web (Web) provided by lnternet has been used in industry predominately as a
means of communication, advertisement, and placement of orders. As background for our invention there now eKists
30   a number of lnternet browsers. Common eKamples are NetScape, Mosaic and l BM's Web EKplorer. Browsers allow a
user of a client to access servers located throughout the world for information which is stored therein and provided to
the client by the server by sending files or data packs to the requesting client from the server's resources. An eKample
of such a request might be something called GSQL (get SQL) which was a NCSA language and CGl server program
developed to getting te Ktual results for a client caller. Developed by Jason Ng at the University of lllinois, this document
35   provided a way to map SQL forms against a database, and return the teKtual results to the client caller. This system
is unlike the present invention, and presents difficulties which are overcome by our described system.
[OO09]  These servers act as a kind of Application Processing Agent, or (as they may be referred to) an ''intelligent
agent'', by receiving a function request from a client in response to which the server which performs tasks, the function,
based on received requests from a client in a distributed environment. This function shipping concept in a distributed
40   environment was first illustrated by CICS as a result of the invention described in U.S. Patent 4,274, 1 39 to Hodgkinson
et al. This kind of function, illustrated by CICS and its improvements, has been widely used in what is now known as
transaction processing. However, servers today, while performing many functions, do not permit the functions which
we have developed to be performed as we will describe.
[O01 O]  Now, ''surfing'' the lnternet with the WWW is still a time consuming affair, and the information received is not
45   generally useful in the form presented. Even with 1 4,400 baud connection to the lnternet much line time is tied up in
just keeping going an access to the lnternet, and the users don't generally know where to go. Furthermore the coupling
of resources available on a company's intranet and those available on the lnternet has not been resolved. There is a
need to reduce gateways, make better use of eKisting equipment, and allow greater and more effective usage of in-
formation which is resident in many different databases on many different servers, not only within a homogeneous
50   network but also via the lnternet and heterogeneous network systems.
[O01 1]  The problems with creating access to the world via the lnternet and still to allow internal access to databases
has been enormous. However, the need for a system which can be used across machines and operating systems and
differing gateways is strongly felt by users of the lnternet today. Anyone who has spent hours at a WWW browser doing
simple task knows how difficult it still is to navigate through arcane rules without knowing where to go and even if you
55   know what you are doing spending hours doing routine tasks. Many needs eKist. As one important instance, until now
we know of no way to access data on multiple databases of different types using a single user request from a client.
This and other difficulties are solved by our invention.   4
