                                    EP O 51 l 486 B1
Description
Background of the lnvention
5   1. Field of the lnvention
_OO01 l   The techniques disclosed herein concern user interfaces to computers generally and more specifically inter-
active user interfaces which employ windows and pointing devices.
Io  2. Description of the Prior Art
_ooo2l   The problem of designing appropriate graphical user interfaces to the uNlx_ system is vexing and largely
unsolved, even today, ten years a_er bitmap displays were first attached to UNIX systems. In those ten years, graphical
applications have become major subsystems that sidestep or even subvert some of the properties of UNIX that helped
15  make it popular, in particular its piece-parts, tool-based approach to programming. Although there have been some
encouraging recent attempts, in particular ConMan, described in Paul Haberli, ''ConMan. A Visual Programming Lan-
guage for lnteractive Graphics'', Comp. Graph., Vol. 22, _4 Aug. 1 988, pp. 1 03- 1 1 O, and Tcl, described in John Oust-
erhout, ''Tcl. An Embeddable Command Language'', Proc. USENIX Winter 1 990 Conf., pp. 1 33-1 46, they have taken
the form of providing interprocess communication within existing environments, permitting established programs to talk
2o  to one another. None has approached the problem structurally. Moreover, they are minor counterexamples to the major
trend, which is to differentiate among systems by providing ever larger, fancier, and more monolithic graphics subsys-
tems rather than by increasing the functionality or programmability of the overall system. To the so_ware developer, that
trend is problematical; modem user interface tool_ts and window systems are as complex as the systems UNIX dis-
placed with its elegant, simple approach.
25  _OO03l   The system of the invention, termed herein Help is an experimental program that combines aspects of win-
dow systems, shells, and editors to provide an improved user interface for te_ual applications. It is not a 'toolkit'; it is a
self-contained program, more like a shell than a library, that joins users and applications. From the perspective of the
application, it provides a universal communication mechanism, based on familiar UNIX file operations, that permits
small applications - even shell procedures - to exploit the graphical user interface of the system and communicate
3o  with each other. For the user, the interface is e_remely spare, consisting only of te_, scroll bars, one simple _nd of win-
dow, and a unique function for each mouse button - no widgets, no icons, not even pop-up menus. Despite these lim-
itations, help is an effective environment in which to work and, particularly, to program.
_OO04l   Help's roots lie in Wirth's and Gutknecht's Oberon system, described in N. Wirth and J. Gutknecht, ''The
Oberon System'', Sof_are Practice and Experience, Sept. 1 989, vol 1 9, no. 9, pp 857-894 and in Martin Reiser, The
35  Oberon System, Addison Wesley, New York 1 991. Oberon is an attempt to e_ract the salient features of Xerox's Cedar
environment, described in W. Teitelman, ''A Tour through Cedar'', IEEE Sof_are 1, no. 2, pp. 44-73, and implement
them in a system of manageable size. It is based on a module language, also called Oberon, and integrates an operat-
ing system, editor, window system, and compiler into a uniform environment. Its user interface is especially simple. by
using the mouse to point at te_ on the display, one indicates what subroutine in the system to execute ne_. In a normal
4o  UNIX shell, one types the name of a file to execute; instead in Oberon one selects with a particular button of the mouse
a module and subroutine within that module, such as Edit. Open to open a file for editing. Almost the entire interface
follows from this simple idea.
_OO05l   A major difficulty with the Oberon system is that it has a single process and is language oriented. Modem
computing systems are typically multi-process systems and are file oriented instead of language oriented. It is thus an
45  object of the invention to secure the advantages of Oberon's user interface in such multi-process and file-oriented com-
puting systems. M. St�bs in CHIP Zeitschri_ f�r Mikrocomputer-Technik, no 1 2, 1 December 1989, pages 1 79-1 82,
''Handbuch im Computer, Microsofts Hilfe-System'', discloses an online handbook which is accessed by calling up a
hierarchy of screens of information. Each screen displays te_ in the form of lists of te_ strings. Clicking on a te_ string,
such as a word, causes the ne_ screen to be displayed, giving more information related to the selected word or string.
5o  _OO06l   EP-A-O 393 837 discloses a method of displaying data corresponding to a procedure is provided where all
of the data required to follow the procedure are displayed together with abbreviated te_ corresponding to the proce-
dure. Te_ and data corresponding to only a portion of the procedure are displayed at any given time. Additional infor-
mation related to several consecutive steps of the procedure which appear on separate screens is displayed
continuously to one side of the screen. Additional data may be selected to be displayed prior to returning to the original
55  SCreen.                             2
