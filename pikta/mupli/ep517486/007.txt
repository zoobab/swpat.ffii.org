                                      EP O 51 l 486 B1
eva l  ' ( help Iparse  -c)
5           x_ ' ( cat  I_t IheIP InewICt l)      .
(
IO                 echo a
15                  echo  Sdir I '     Close! '
)  I  helplbuf  >  Imnt IheIPI Sx Ict l
20           (_
25                  cpp  Scppf lags  S f i le  l
,,                         helpl rcc  -w -g - i S id -nS l ine  l  sed l_
)  >  I_t Ihelp I SxlbodYa Pn
35  The first line runs a small program, helplparse, that examines $helpsel and establishes another set of environment var-
iables, file, id, and line, describing what the user is pointing at. The ne_ creates a new window I03 and sets x to its
number. The first blockwrites the directory name to the tag line 1 05; the second runs the C preprocessor on the original
4o  source file and passes the resulting te_ to a special version of the compiler. This compiler has no code generator; it
parses the program and manages the symbol table, and when it sees the declaration for the indicated identifier on the
appropriate line of the file, it prints the file coordinates of that declaration. This appears on standard output, which is
appended to the new window by writing to lmn_helpl$xlbodyapp. The user can then point at the output to direct Open
to display the appropriate line in the source. (A future change to help will be to close this loop so the Open operation
45  also happens automatically.) Thus with only three button clic_ one may fetch to the screen the declaration, from what-
ever file in which it resides, the declaration of a variable, function, type, or any other C object.
_O036l   A couple of observations about this example. First, help provided all the user interface. To turn a compiler
into a browser involved spending a few hours stripping the code generator from the compiler and then writing a half
dozen brief shell scripts to connect it up to the user interface for different browsing functions. Given another language,
5o  we would need only to modify the compiler to achieve the same result. We would not need to write any user interface
software. Second, the resulting application is not a monolith. It is instead a small suite of tiny shell scripts that may be
tuned or toyed with for other purposes or experiments.
_O031l   Other applications are similarly designed. For example, the debugger interface, lhelpldb, in window 605, is
a directory of ten or so brief shell scripts, about a dozen lines each, that connect adb to help. Adb has a notoriously
55  cryptic input language; the commands in lhelpldb package the most important functions of adb as easy-to-use opera-
tions that connect to the rest of the system while hiding the rebarbative syntax. People unfamiliar with adb can easily
use help's interface to it to examine broken processes. Of course, this is hardly a full-featured debugger, but it was writ-
ten in about an hour and illustrates the principle. It is a prototype, and help is an easy-to-program environment in which
l
