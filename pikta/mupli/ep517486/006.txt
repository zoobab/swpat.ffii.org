                                    EP O 51 l 486 B1
tions of all references to the variable n in the files lusrlroblsrc_elpl*.c are indicated by file name and line number. The
implementation of the C browser is described below; in a nutshell, it parses the C source to interpret the symbols
dynamically.
_O030l   The first item on the list in window 1 O01 is clearly the declaration in the header file. It looks like help.c.35
5   1 O03 should be an initialization. To find out, one uses Open help.c to examine that line and see that the variable is
indeed initialized. The result of the Open is shown in FIG. 1 1. Window 1 1 01 shows line 35 of help.c at 1 1 03. The line
indicates that n 905 was set; some other use of n must have cleared it. Line 252 of exec.c is the call; that's a read, not
a write, of the variable. The ne_ possibility is exec.c.21 3; on pointing to that and again executing Open, window 1 1 07
appears; as shown there at reverse video line 1 1 09, n is set to O. Sometime before xdie2 was executed, xdiel cleared
Io  n. The bug is fixed by selecting Cut 1 1 1 1 to remove the offending line, selecting Put!, which appears in the tag of a mod-
ified window, to write the file back out and then selecting mk 1 1 13 in _elplcbr to compile the program. It is worth noting
that the entire debugging operation of the example was done without touching the keyboard.
_O031 l   This demonstration illustrates several things besides the general flavor of help. It is easy to work with files
and commands in multiple directories. The rules by which help constructs file names from conte_ and, transitively, by
15  which the utilities derive the conte_ in which they execute simplify the management of programs and other systems
constructed from scattered components. Also, the few common rules about te_ and file names allow a variety of appli-
cations to interact through a single user interface. For example, none of the tool programs has any code to interact
directly with the keyboard or mouse. Instead help passes to an application the file and character offset of the mouse
position. Using the interface described in the ne_ section, the application can then examine the te_ in the window to
2o  see what the user is pointing at. These operations are easily encapsulated in simple shell scripts, an example of which
is given in the ne_ section.
The interface seen _y programs
25  _O032l   help provides its client processes access to its structure by presenting a file service, as described in Rob
Pike, et al., ''Plan 9 from Bell Labs'', Proc. of the Summer 1 990 UKUUG Conf., London, July, 1 990, pp. 1 -9. Each help
window is represented by a set of files stored in numbered directories. The number of each directory is a unique iden-
tifier, similar to UNIX process id's. Each directory contains files such as tag and body, which may be read to recover the
contents of the corresponding subwindow, and ctl, to which may be written messages to effect changes such as inser-
3o  tion and deletion of te_ in contents of the window. The help directory is conventionally mounted at lmnt lhelp, so to copy
the te_ in the body of window number 7 to a file, one may execute
cp lmn_elpl71body file.
35  To search for a te_ pattern,
grep pattern lmn_help_lbody
An ASCll file lmn_elp_ndex may be examined to connect tag file names to window numbers. Each line of this file is a
4o  window number, a tab, and the first line of the tag.
_O033l   To create a new window, a process just opens lmn_elplnewlctl, which places the new window automati-
cally on the screen near the current selected te_, and may then read from that file the name of the window created, e.g.
Imn_helpl8. The position and size of the new window is, as usual, chosen by help.
45  lmplementation of Components of the C Browser
_O034l   The directory lhelplcbr contains the C browser we used above. One of the programs there is called decl; it
finds the declaration of the viable marked by the selected te_. Thus one points at a variable with the le_ button and then
executes decl in window 909 for the file _elplcbrls_. Help executes _elplcbrldecl using the conte_ rules for the
5o  e_ecuted te_ and passes it the conte_ (window number and location) of the selected te_ through an environment via-
ble, helpsel.
_O035l   Decl is a shell (rc) script. Here is the complete program.
55                                  6
