<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patents Europees de Programari: Exemples Escollits

#descr: Vam arribar als següents impressionants exemples mitjançant la primera
lectura de les nostres llistes tabulades de patents de programari. 
Estan escollides gairebé a l'atzar i representen aproximadament
l'estàndar de técnica i inventiva de l'Oficina Europea de Patents
(EPO).  Si alguna cosa les distingueix d'altres patents és la relativa
facilitat d'entendre-les a primera vista per una gran quantitat de
gent.

#ep927945t: Amazon Gift Ordering

#ep927945d: If you want to program your online shop so that it delivers your
articles as gifts to a third person specified by the customer, you
might want to negotiate with Amazon Inc for a license.  This patent,
which is a direct descendant of Amazon's One Click Patent, was granted
by the European Patent Office (EPO) in May 2003.

#ep927945c: A method in a computer system for ordering a gift for delivery from a
gift giver to a recipient, the method comprising:%(ul|receiving from
the gift giver an indication that the gift is to be delivered to the
recipient and an electronic mail address of the recipient; and|sending
to a gift delivery computer system an indication of the gift and the
received electronic mail address, wherein the gift delivery computer
system coordinates delivery of the gift by|sending an electronic mail
message addressed to the electronic mail address of the recipient
requesting that the recipient provide delivery information including a
postal address for the gift; and|upon receiving the delivery
information, electronically initiating delivery of the gift in
accordance with the received delivery information.)

#ep902381t: Amazon 1Click

#ep902381d: Amazon's application for a patent on its One-Click Shopping method at
the EPO.  The application reached the third stage of examination (A3),
i.e. it was recognised as referring to a patentable invention and a
full novelty examination was conducted.  In 2001 the patent
application was split into two new applications, of which one was
granted and one is still pending.

#ep902381c: A method for placing an order to purchase an item, the order being
placed by a purchaser at a client system and received by a server
system, the method comprising: %(ul|under control of the server
system, receiving purchaser information including identification of
the purchaser, payment information, and shipment information from the
client system;|assigning a client identifier to the client
system;|associating the assigned client identifier with the received
purchaser information;|sending to the client system the assigned
client identifier; and|sending to the client system display
information identifying the item and including an order button;|under
control of the client system, receiving and storing the assigned
client identifier;|receiving and displaying the display information;
and|in response to the selection of the order button, sending to the
server system a request to purchase the identified item, the request
including the assigned identifier; and|under control of the server
system, receiving the request; and|combining the purchaser information
associated with the client identifier included with the request to
generate an order to purchase the item in accordance with the billing
and shipment information whereby the purchaser effects the ordering of
the product by selection of the order button.)

#ep533098t: Control of an apparatus by actuator check via microcomputer

#ep533098d: The main claim seems to cover all state-controlled automatic systems,
including general purpose computer programmed in such a way that
specific output values are controlled by specific input values.  The
main claim says that this device is %(q:preferably a car radio), but
does not limit itself to this specific application of a very general
calculation rule.  This patent was granted after 12 years of
examination, including opposition filed by an association dedicated to
keeping the broadcasting industry free of patents.

#ep533098c: %(linul|Device for controlling a telecommunications apparatus,
preferably a car radio, having a microcomputer, a memory, a control
part having control elements and having actuators, the microcomputer
being designed to use a program stored in the memory to acquire the
position of the control elements and to control the corresponding
functions of the device, characterized|in that the program contains a
table which contains start information items of program modules for
controlling the actuators for all possible operating states, a
respective operating state being provided by a setting of the
actuators and by an actuation state of the control elements,|in that
the microcomputer is designed in such a way that, upon actuation of a
control element, it carries out a search operation in the table, in
the case of which the operating states listed in the table are
compared as desired values with the actually present operating state
as actual value,|in that, following a successful search operation, the
microcomputer removes the start information item which has been found
from the table, and|in that the microcomputer executes one or more
program modules using the start information item.)

#ep287578t: Acoustic Data Compression -- MP3 Base Patent

#ep287578d: Iteratively perform certain calculations on acoustic data until a
certain value is reached.  The patent owner Karlheinz Brandenburg,
core researcher of the MP3 project at Max Planck, received this patent
in 1989.  This patent and its owner were showcased by the European
Commission's %(q:IPR Helpdesk) project in 2001 as %(q:inventor of the
month).  This is one of several dozen patents which cover the MP3
audio compression standard, and perhaps the most famous and basic one.
 It has always been treated as a model of how %(q:technical) and
%(q:non-trivial) software patents can get.

#ep287578c: %(linul|Digital coding process for transmitting and/or storing
acoustic signals, specifically music signals, comprising the following
steps.|N samples of the acoustic signal are converted into M spectral
coefficients;|said M spectral coefficients are subjected to
quantisation at a first level;|after coding by means of an entropic
encoder the number of bits required to represent all the quantized
spectral coefficients is checked;|when the required number of bits
does not correspond to a specified number of bits quantization and
coding are repeated in subsequent steps, each at a modified
quantization level, until the number of bits required for
representation reaches the specified number of bits, and|additionally
to the data bits the required quantization level is transmitted and/or
stored.)

#ep769170t: Virus decorats

#ep769170d: Crear un entorn emulat d'ordinador i provar un fluxe de dades a través
d'aquest entorn abans d'acceptar-lo a l'entorn real de l'ordinador és
una cosa útil i difícil de fer.  Qualsevol que ho aconsegueixi haurà
de pagar llicència de  %(q:Quantum Leap Technologies).

#ep767419t: Mètode IBM i programa d'ordinador per mostrar objectes de finestres
relacionades

#ep767419d: IBM va lluitar dur per aquesta patent i fins i tot va empényer el
Quadre Tècnic d'apelacions de l'EPO a crear un precedent al 1998 per
violació de l'Art 52 EPC per tal d'adquirir els drets de concessió
d'un %(q:programa d'ordinador).  L'explicació mateixa és abstracta,
functional i trivial: redistribuir els continguts d'una finestra
visible parcialment de manera que entrin a la part de la finestra
visible actualment a la pantalla en comptes de tenir part dels
continguts tapats per una altra finestra.  Aquesta patent va entrar en
vigor plenament el gener de 2001 sense cap oposició.

#ep193933t: control·lar un ordinador per un altre

#ep193933d: Això cobreix sistemes com RPC, Telnet i tota mena de computació
client-servidor.

#ep193933c: A digital data processing system which has a destination data
processing means coupled by message transport means to a source data
processing meansand wherein the source data processing means has
remote call providing means for providing a remote call to a callable
program in the destination data processing means, and the destination
data processing means has remote call receiving means for receiving
the remote call and performing a call to the callable program, and
comprises a call protocol which is produced by the remote call
providing means in the source data processing means, transferred via
the message transport means to the destination data processing means,
and which is received by the remote call receiving means and the call
protocol includes a program identifier and call parameters for
specifying the callable program, characterized in that %(ol|each call
protocol may contain an option part containing one or more optional
items, said item not being required in every call to the callable
program but being used by the remote call receiving means in making
the call when the optional items are present in the protocol,|each
optional item including a parameter value and an item tag specifying
the use of the parameter value in the call,|the remote call receiving
means using each optional item's parameter value in the call as
specified by the optional item's tag.)

#ep266049t: Coding System for Reducing Redundancy

#ep266049d: This patent was granted by the EPO in 1994 after 7 years of
examination, with priority date 1986. In 2002 it prompted Sony and
other companies to pay many million USD for using the JPEG standard
and made JPEG cease to be an international standard.

#ep266049c: An ordered redundancy method for coding digital signals, the digital
signals taking a plurality of different values, using two types of
%(tp|runlength code|R, R'), the method comprising the steps
of.%(ol|utilising the %(tp|first type of runlength code|R) for coding
a runlength of the most frequently occurring value followed by the
next most frequently occurring value;|utilising the %(tp|second type
of runlength code|R') for coding a runlength of the most frequently
occurring value followed by any value other than the next most
frequently occurring value; and|when the second type of runlength code
is used, following the runlength code value by a code value indicative
of the amplitude of said other value.)

#ep689133t: Patent d'Adobe sobre %(q:Tabbed Palettes)

#dp689133d: Aquesta patent, concedida per l'EPO a l'agost de 2001, l'ha feta
servir Adobe per fer pagar Macromedia Inc als USA.  La versió EP va
trigar 6 anys a examinar-se, i es va concedir al complet, sense cap
modificació.  Cobreix la idea d'afegir una tercera dimensió a un
sistema de menú ordenant molts conjunts d'opcions un darrere l'altre,
marcats amb tabuladors.  Això es troba particularment útil en
processament d'imatges del programari d'Adobe i Macromedia, però també
al GIMP i molts altres programes.

#ep689133c: A method for combining on a computer display an additional set of
information displayed in a first area of the display and having
associated with it a selection indicator into a group of multiple sets
of information needed on a recurring basis displayed in a second area
of the screen, comprising the steps of %(ol|establishing the second
area on the computer display in which the group of multiple sets of
information is displayed, the second area having a size which is less
than the entire area of the computer display, the second area
displaying a first of the multiple sets of information;|providing
within the second area a plurality of selection indicators, each one
associated with a corresponding one of the multiple selecting a second
of the multiple sets of information for display within the second area
by activating a selection indicator associated with a second of the
multiple sets of information, whereby the second of the multiple sets
of information is substituted for the first of the multiple sets of
information within the area of the display; and|combining the
additional set of information, displayed in the first area of the
display into the group of multiple sets of information so that the
additional set of information may be selected using its selection
indicator in the same manner as the other sets of information in the
group.)

#ep487110t: diagnosi mèdica automatitzable

#ep487110d: La principal reivindicació cobreix totes les diagnosis mèdiques que es
poden calcular automàticament basant-se en una entrada de dades
d'imatge o text, sense tenir en compte en què estan basats els
càlculs.  Les reivindicacions 2 i 3 extenen l'aparell amb una xarxa i
una base de dades.  La reivindicació 5 afegeix l' %(q:invenció) de fer
anàlisis de dibuixos com %(q:ja llegides pel doctor).  Subsegüents
plans i patents posteriors permeten Shibaura posseir més problemes
específics de l'organització de diagnosis mèdiques.

#ep792493t: preu dinàmic

#ep792493d: Qualsevol que canvïi una etiqueta de preu amb una funció editada per
l'usuari infringeix una patent a Europa.  Les següents reivindicacions
depenents deixen qui patenta com a amo d'una gran part dels problemes
del modern comerç electrònic.

#ep328232t: signatura digital amb informació extra

#ep328232d: En un sistema com PGP/GPG, afegeix informació d'autenticació extra
d'un certificat a la signatura i infringiràs aquesta patent, no
importa com funcionin els teus algoritmes de criptografia.  Malgrat
que la principal reivindicació després de l'examen és molt llarga i
detallada, és tan àmplia com abans de l'explicació, perquè la major
part de la informació afegida és redundant.  Torna a dir que qui
patenta és l'amo del problema.

#ep644483t: Sistema d'ordinador i mètode per realitzar múltiples tasques

#ep644483d: Una interfície separada s'inserta entre diverses aplicacions i el
terminal principal, per tal que l'aplicació i l'usuari es poden
comunicar independentment amb aquesta interfície. Això fa possible
deixar processos funcionant de fons.

#ep800142t: Convertir noms de fitxer de Windows95 a WindowsNT.  Una patent d'un
problema enganyós, patentat per Sun Microsystems per tal d'emprenyar
Microsoft.

#ep803105t: EP 803105: Network Sales System

#ep803105d: This patent, granted to OpenMarket Inc by the European Patent Office
in 2002 after 6 years of examination, is identical to a system which
is currently being used in the USA to squeeze money out of various
e-commerce companies.

#ep803105c: %(linul|A network-based sales system, comprising|at least one buyer
computer for operation by a user desiring to buy a product;|at least
one merchant computer; and|at least one payment computer;|the buyer
computer, the merchant computer, and the payment computer being
interconnected by a computer network;|the buyer computer being
programmed to receive a user request for purchasing a product, and to
cause a payment message to be sent to the payment computer that
comprises a product identifier identifying the product;|the payment
computer being programmed to receive the payment message, to cause an
access message to be created that comprises the product identifier and
an access message authenticator based on a cyptographic key, and to
cause the access message to be sent to the merchant computer;|and the
merchant computer being programmed to receive the access message, to
verify the access message authenticator to ensure that the access
message authenticator was created using said cyptographic key, and to
cause the product to be sent to the user desiring to buy the product.)

#ep529915t: control per veu

#ep529915d: La principal reivindicació sembla cobrir gairebé qualsevol manera
d'interacció entre un terminal i múltiples sistemes independents
connectats a la xarxa.  Les explicacions s'estrenyen a una aplicació
específica, però no parla d'una solució que s'ocupa del problema en
contexts específics.

#ep747840t: servidor web extensible dinàmicament

#ep747840d: Això sembla cobrir qualsevol servidor web que processi taules HTML i
invoqui un programa via interfície comuna gateway, de manera que el
programa retorna una pàgina web.

#ep587827t: retroalimentació per defecte

#ep587827d: Envia un missatge preliminar a la pantalla si, degut a una connexió
lenta a la xarxa, el programa de fons no pot enviar el missatge final
amb prou rapidesa.

#ep490624t: configuració intuitiva de xarxa

#ep490624d: Representa els nodes a la xarxa i les seves relacions de manera
gràfica, p.e. com cercles i fletxes, editables amb drag & drop, i
genera fitxers de configuració com a resultat.  Això cobreix totes les
eines amigables d'administració de xarxes que s'hagin d'escriure d'ara
endavant.

#ep762304t: corredor de borsa electrònic

#ep762304d: Una persona té cura d'oferir i confirmar compres amb una determinada
comanda quan es troba una oferta.

#ep688450t: sistema de fitxers flash

#ep688450d: Aquesta patent sembla fer pràcticament impossible desenvolupar
qualsevol programari no-propietari per a una part important del mercat
d'aparells incrustats.  Reclama les conseqüències lògiques del fet que
els blocs de memòria només es poden desar i reemplaçar en blocs de 64
k o similars.  I no és l'unica relliscada de les concessions de la EPO
en aquest tema.

#ep807891t: carro de compra electrònic

#ep807891d: apuntar objectes de compra en una llista i comprar-los tots al final.

#ep823173t: compressió en comunicació mòbil

#ep823173d: Permet IBM posseir el problema de reduir dades combinant-les per a
transmetre-les.  Qualsevol comunicació mòbil estàndard per TCP ho
tindrà difícil per a saltar-se això.

#ep522591t: traduir idioma humà a una base de dades lingüística de consulta

#ep522591d: Ara Mitsubishi posseeix el problema de traduir preguntes en idioma
natural a consultes de base de dades fent servir anàlisi sintàctica i
taules virtuals.  No s'explica cap anàlisi sintàctica.

#ep242131t: visualitzar un procès

#ep242131d: Visualitzar funcions de manera gràfica veient-ne els seus components,
permetent interactions a la pantalla i creant un flux de dibuix
d'aquestes interaccions.

#ep242131c: %(nl|A method of producing a program modelling a physical process for
a computer using data flow diagramming, wherein the computer includes
a memory which stores a plurality of executable functions and a
plurality of data of various types, a display, means for receiving
user input, and a data processor, the method characterised by the
steps of| assembling, in response to user input, a data flow diagram
on the display, the data flow diagram specifying the process and
including function-icons corresponding to respective ones of the
plurality of executable functions, terminal-icons corresponding to
respective ones of the plurality of data types, a structure-icon
indicating control flow of the data flow diagram, and arcs
interconnecting the function-icons, terminal-icons, and
structure-icon; and|generating an executable program in response to
the data flow diagram, the executable program including one or more of
the executable functions as indicated by the function-icons and
interconnected as indicated by the arcs to operate upon data
identified by the terminal-icons, the executable program having
control flow established as indicated by the structure-icon.)

#ep359815t: distingir entre blocs de memòria usats i no usats durant identificació
de fitxer de caché

#ep359815d: Distingir entre blocs de memòria usats i no usats quan fallen blocs al
caché, per tal d'evitar pèrdues innecessàries de temps reescrivint
blocs ja usats.  Hi ha centenars de patents de l'EPO d'aquesta mena. 
Proveu-ho cercant la base de dades amb mots com %(q:cache) o
%(q:memory).

#ep592062t: transmetre sol·licituds de compressió

#ep592062d: Una aplicació especifica un esquema de compressió per comunicació de
dades, que es fa servir llavors per un servidor de comunicacions
independent, com si fos un sistema de correu conforme a MIME.

#ep756731t: generar incentius de compra amb receptes de cuina

#ep756731d: Calcular llistes de coses per comprar amb instruccions de compra,
basant-se en receptes de cuina especificades per l'usuari.  La
%(q:contribució tècnica) recau en que es fan servir un monitor i una
impressora.

#ep461127t: ensenyament de llengües comparant la pronúncia de l'alumne amb la del
professor

#ep461127d: Cobreix tots els sistemes d'ensenyament de llengües que permeten
l'usuari de comparar la seva pronúncia d'un text seleccionat a la
correcta pronúncia.  Com a subproducte, la reivindicació també sembla
incluir la funció d'ensenyament de sistemes de reconeixement de veu de
tipus ViaVoice.

#ep664041t: provar material ensenyat a les escoles

#ep664041d: usar un ordinador per a provar els estudiants.  La principal
reivindicació cobreix el procediment bàsic, els altres només
especifiquen coses útils per fer. La %(q:contribució tècnica)
consisteix en ensenyar que un ordinador pot fer aquestes coses de
manera més eficient.

#ep517486t: incloure dades contextuals a l'entrada

#ep517486d: Un sistema d'ajuda on les comandes tenen context específic.   Aquesta
patent consisteix en una única reivindicació, curta i àmplia.  La
descripció es refereix a un entorn de comandes per sistemes Unix que
la empresa alega que va crear i que és similar al Oberon de Niklas
Wirth.

#ep825526t: Traduir entre 2 Objectes

#ep825526d: La reivindicació 1 sembla apropiar-se del següent, aparentment
patentable, descobriment o la seva aplicació a objectes de programari:
 Si tu només parles anglès i jo només català, només ens podem
comunicar si, almenys un dels dos porta un intèrpret que conegui les
dues llengües.

#ep825525t: herència d'un objecte CORBA

#ep825525d: La reivindicació 1 cobreix la generació d'un objecte CORBA per un
altre, un mètode similar a %(q:herència) en programació orientada a
objectes.  Una solució de programari per a un problema de programari
en el sentit més ample.  Un es pot preguntar què en aquesta patent
l'EPO ha de considerar que %(q:no aplica).

#ep794705t: màquina per a fer pa i un sistema de codi per a ella

#ep794705d: Si cous pa sota un programa de control, infringeixes aquesta patent. 
No importa com posis els sensors i perifèrics o com els programis: si
fas anar un procés de cocció amb un programa oferint l'usuari un menú
per seleccionar diferents configuracions, necessites una llicència. 
Aquesta patent no reivindica un programa d'ordinador com a tal sinó un
mètode de negoci basat en un programa.  La causalitat entre els
mitjans i el final és de pura lògica: es reivindica functionalitat
sense referència a cap manera nova de fer servir forces naturals.  La
reivindicació principal va ser acceptada per l'EPO sense cap
modificació.  Qui ho va patentar, una persona de Hong-kong,
aparentment s'està preparant per recollir diners de serveis de
repartiment de pa per comerç electrònic en 11 estats europeus.

#ep797806t: seqüenciació orientada en binari

#ep797806d: Una patent de l'EPO, concedida al 1997 amb data de prioritat 1994,
sobre un mètode d'organització de la informació usat habitualment en
recent estàndar de la world wide web.  D'acord amb aquest mètode, tota
la informació està estructurada en identificadors atòmics i estats de
relació entre dos identificadors.  Això permet una gran independència
entre dades i mètodes de procés de les dades.

#ep526034t: ATT patent on %(q:single-object file naming conventions)

#ep526034d: An exclusive right, granted to American Telephone and Telegraph by the
European Patent Office in 1993, on retrieving files by means of file
naming conventions which where uniquely identifying search criteria
(such as the inode number and generation sequence of the file) are
part of the filename or somehow encoded into it.  To put it in a
slightly simplified manner, this patent covers the idea retrieving
files by combining theadvantages of a name (easy to manage for the
end-user) and a sequential number (easy to handle for the computer).

#ep526034c: A computer based file apparatus for accessing a plurality of
previously-stored data files, the appartus comprising %(ul|means for
receiving a user request including a purported file name;|means for
comparing the purported file name with the file names of the
previously-stored data files and, if the purported file name matches
the file name of one of the previously-stored data files, retrieving
the previously stored data file whose file name matches purported file
name;|means for, if the purported file name does not match the file
name of one of the previously-stored data files, parsing the purported
file name using a first logical syntax to generate one or more
non-file-name-substring-based first search criteria; and|means for
comparing the one or more first search criteria with the
previously-stored data files and, if the one or more first search
criteria match one or more of the previosly-stored data files,
retrieving the or each previously stored data file which matches the
one or more first search criteria.)

#ep538888t: Pay per Use

#ep538888d: In 1993, the European Patent Office (EPO) granted Canon K.K. of Japan
owns a patent on charging a fee per a unit of decoded information. 
The main claim covers all systems where a local application decodes
information received from remote information distributor and
calculates a fee based on the amount of information decoded.  If an
information vendor wants to realise a full %(q:Pay Per Use) system
where the fee arises only when the user actually reads the information
(rather than when it is transmitted), he might want to beg Canon for a
license.  Perhaps Canon will be generous, since it is clear that the
patent claim describes a class of programs for computers
(computer-implemented calculation rules), and the supposedly novel and
inventive problem solution (invention) consists in nothing but the
program [ as such ].  All features of this claim belong to the field
of data processing by means of generic computer equipment.

#ep538888c: %(al|An information processing system comprising|%(ul|receiving means
for receiving transmitted information, the information being coded or
incomplete and in a non-usable form;|a recording medium, decoding
information needed to demodulate the received information being
prerecorded on said recording medium, said recording medium comprising
a first area in which the received information is to be written and a
second area in which the decoding information is pre-recorded;|writing
means for writing the information received by said receiving means in
the first area of said recording medium;|reading means for reading out
the decoding information from said recording medium; and|demodulating
means for converting the information received by said receiving means
and written in said first area of said recording medium to a usable
form using the decoding information read out by said reading
means;)|wherein the charge for use of the information is defined in
units corresponding to the decoding information being prerecorded on
said recording medium)

#de19838253t: milora de la seguretat de xarxa desconnectant alternativament un
ordinador a dues bandes

#de19838253d: Per tal d'equipar més bé una intranet contra atacs dels crackers, el
tallafocs sempre es talla físicament d'un dels dos extrems, i la
inter-comunicació es manté per la ràpida alternància.  Qui l'ha
patentat afirma que aquest principi pot fer una xarxa absolutament
segura, mentres que els actuals tallafocs permeten només seguretat
relativa.  Encara que aquesta afirmació sembla qüestionable, el fet és
que qualsevol que que faci la feinada que representa, haurà de pagar
per una llicència a Fraunhofer.

#de19747603t: Comerç Segur per telèfon mòbil

#de19747603d: L'empresa alemana Brokat ara posseeix cada %(q:procés per signar
digitalment un missatge enviant-lo mitjançant un aparell per a signar
via telco network).

#ep497041t: information-controlled drug infusion

#ep497041d: using data processing means to control the way drugs are infused into
a patient's blood (body).  The main claim encompasses the whole
problem, for which solutions are being descsribed.   The patent isn't
limited to certain apparatusses or physical causalities and not even
to certain calculation rules.  It covers the business idea of using a
program for a certain purpose.

#suK: Exhibited Specimens

#ait: Candidates

#Wft: Patents for which we created a dedicated page

#ahx: The following evaluations come from the supporters of the FFII who
have registered in the %(ps:participation system).

#WmW: You can add examples to our collection.

#Wlr: You can evaluate the level of %(s:TECH)nicity as well as the quality
of the %(s:DEAL) which society gets when the EPO issues this patent.

#aeo: higher mathematics with large and diverse field of application

#tlr: application of universal computer to various tasks

#mTm: programming of special gadgets for information and communication, e.g.
card reader, mobile phone

#foj: control of machines which apply forces of nature, e.g. optimising fuel
use, anti-blocking system

#hWt: As above, but the problem solution seems to rely on knowledge about
forces of nature more than on informatic (control logic, automation
theory) skills.

#oif: The patent teaches something new about physical causality.  These
insights could not have been obtained without experimentation on
forces of nature.

#nae: invalid even according to the laxest patent office standards

#rbi: belongs into the horror gallery: worthless disclosure,
incommensurately great blocking effect (broad claim scope)

#nuW: reasonably interesting disclosure and moderate blocking effect

#SvW: belongs into the beauty gallery: precious disclosure, moderate
blocking effect

#rwW: Patents like this one should be granted:  society at large would make
a good bargain.

#Ptn: patent

#nrn: remarks

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpikmupli ;
# txtlang: ca ;
# multlin: t ;
# End: ;

