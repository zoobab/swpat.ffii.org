                                        EP O 800 1 42 B1
prefiKeslsuffiKes. Any prefiK or suffiK that is contained in the directory specific request is identified, for eKample DOS,
MACl NTOSH, OTHER. When no prefiKIsuffiK is used in the request, the specified directory is searched, and it is de-
termined if the requested item is found in the specified directoy. When use of a prefiKIsuffiK is found in the directory
specific request, the found prefiKIsuffiK is stripped, and a lookup strategy is used that is related to the found prefiKI
5   suffiK. It is then determined if the requested item was found in the specified directory using the prefiKIsuffiK related
Iookup strategy of one of the previous steps. Thus, four search strategies are provided; i.e., (1) when no prefiKIsuffiK
is used, (2) when the DOS prefiKIsuffiK is used, (3) when the MACl NTOSH prefiKIsuffiK is used, and (4) when the
OTHER prefiKIsuffiK is used.
[OO09]  l BM Technical Disclosure Bulletin, Vol.32, no. 1 Oa, March 1 990, pp. 456 - 462 ''File Mapping in a heteroge-
IO   neous distributed environment'' discloses a method for converting path names between different formats. A method is
described that handles the mapping of Fl LE l Ds from one operating system to a second operating system. In order to
accomplish this, a PROFl LE must be created before mapping use. This PROFILE contains a set of RULES that de-
termine how a given Fl LE l D is mapped from a first operating system to a second operating system pair, and the details
of the RULES must differ depending upon the specific operating systems involved in the operating system pair since
15   a logical connection is established between two different operating systems for each different RULE.
[O01 O]  M. Davis et al in ''Unicode''; 1 990 IEEE lnternational Conference on Systems, Man and Cybernetics, Confer-
ence Proceedings, 4 - 7 November 1 990, Los Angeles, CA, USA, pages 499 - 504, discusses the fundamental design
principles for encoding characters in computers using Unicode.
20   SUMMARY OF THE INVENTION
[O01 1]  The present invention provides a method for converting an ASCl l path name to a parsed path name structure
in accordance with the appended claims.
[O01 2]  ln accordance with this invention, the above problem has been solved by the development of an efficient
25   method implemented in conjunction with a computing system for converting a zero-terminated ASCl l path name to a
parsed path name structure.
[O01 3]  The above computer implemented steps in another implementation of the invention are provided as an article
of manufacture, i.e., a computer storage medium containing a computer program of instructions for performing the
above described steps.
30   [O01 4]  The great advantage and utility of the present invention is the efficient conversion of ASCll path names to
parsed path name structures. The invention permits program modules designed to run under operating systems that
provide parsed path name structure inputs to run under other operating systems which provide only ASCl l path name
inputs.
[O01 5]  The foregoing and other features, utilities and advantages of the invention will be apparent from the following
35   more particular description of a preferred embodiment of the invention as illustrated in the accompanying object code
Iisting and in the drawings.
Brief Descri tion of Drawin s
40   [O01 6]
Fig. 1 illustrates a representational computing system and operating environment for performing the computer
implemented steps of the method in accordance with the invention; and
Fig. 2 is a flow chart depicting buffer contents at various stages during the conversion of a non-UNC ASCl l path
45      name to a parsed path name structure; and
Fig. 3 is a flow chart depicting buffer contents at various stages during the conversion of a UNC ASCl l path name
to a parsed path name structure.
Detailed Descri tion of Preferred Embodiments
50   [O01 l]  The embodiments of the invention described herein may be implemented as logical operations in a distributed
processing system having client and server computing systems. The logical operations of the present invention are
implemented (1) as a sequence of computer implemented steps running on the computing system and (2) as intercon-
nected machine modules within the computing system. The implementation is a matter of choice that is dependent on
55   the performance requirements of the computing system implementing the invention. Accordingly, the logical operations
making up the embodiments of the invention described herein are referred to variously as operations, steps or modules.
[O01 8]  The operating environment in which the present invention is used encompasses the general distributed com-
puting system, wherein general purpose computers, workstations, or personal computers are connected via commu-
3
