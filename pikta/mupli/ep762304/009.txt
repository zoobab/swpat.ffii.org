                           1 5             EP O 762 304 B1             1 6
display unit (3), if the transaction request is input in       tifikationsnummer eine Wertpapierkennummer ist.
time, including transaction information.            5.  Computersystem nach Anspruch 1 oder 2, in dem
26. Method according to at least one of claims 1 8 to 25,       die Handelsmaske (1 3) die Eingabe einer Anforde-
wherein                            5     rung f�r eine Kursseite (1 8) erlaubt, die eine Viel-
the trading mask (1 3) and the quotation mask (1 4)       zahl von spezifischen Daten enth�lt und die Einga-
are displayed on the display unit (3) on different       be der Anforderung f�r spezifische Daten erlaubt.
SCreenS Or On One SCreen.                   6.  Computersystem nach zumindest einem der An-
21. Method according to at least one of claims 1 8 to 26,  IO    spr�che 1 bis 5, in dem das Datenverarbeitungssy-
wherein                                 stem (1) ein Sicherheitsnetzwerk (6) enth�lt, das
the data management comprises the management       mit einer eKternen Vorrichtung (7) verbunden ist,
of data relating to the trading of stocks, bonds, de-       weiterhin eine Sicherheitsvorrichtung (8) enth�lt, ei-
rivatives or foreign eKchange, wherein the specific       ne Datenmanagementvorrichtung (9), eine Daten-
data include stock rates, bond rates, derivative  15    schnittstellenvorrichtung (1 O) und eine Ausgabe-
rates or foreign eKchange rates.                  vorrichtung (1 1), wobei die eKterne Vorrichtung (7)
die Eingabeeinheit (2) und die Anzeigeeinheit (3)
enth�lt.
Patentanspr�che                        20  l.  Computersystem nach Anspruch 6, in dem auf die
1.  Computersystem f�r das Durchf�hren eines Daten-       Eingabe der Anforderung f�r spezifische Daten
managements, das zumindest das Management       durch die Eingabeeinheit (2) hin
von Daten betreffend dem Handeln von Options-
scheinen enth�lt, mit                         -   die eKterne Vorrichtung (7) die Anforderung f�r
einem Datenverarbeitungssystem (1), einer Einga-  25       spezifische Daten ausgibt und die Anforderung
beeinheit (2), einer Anzeigeeinheit (3) und einem          zu dem Sicherheitsnetzwerk (6) �bertr�gt,
zumindest Optionsscheinkurse empfangenden Da-       -   das Sicherheitsnetzwerk (6) die Anforderung
teneingang (5), wobei                           betreffend ihrer Autorisierung �berpr�ft und
dieselbe zur
-   die Anzeigeeinheit (3) eine erste Maske dar-  30    -   Datenmanagementvorrichtung  (9)  �bertr�gt,
stellt, die ein Format aufweist, welches die Ein-          wenn die durchgef�hrte �berpr�fung eine Au-
gabe einer Anforderung f�r spezifische zumin-          torisierung der eKternen Vorrichtung (7) f�r die
dest Optionsscheinkurse enthaltende Daten          Datenmanagementvorrichtung (9) ergibt,
�ber eine Eingabeeinheit (2) erlaubt,             -   die  Datenmanagementvorrichtung  (9)  eine
-   der Dateneingang (5) gelesen wird, wenn die  35       Nachricht zu dem Sicherheitsnetzwerk (6) aus-
Anforderung durch die Eingabeeinheit (2) ein-          gibt, um auf Daten (5) von der Datenschnittstel-
gegeben wird,                             lenvorrichtung  (1 O)  zuzugreifen,  wenn  die
-   die Anzeigeeinheit (3) eine zweite Maske an-          durchgef�hrte Pr�fung eine Autorisierung der
zeigt, die die angeforderten Daten enth�lt und          eKternen Vorrichtung (7) f�r die Datenschnitt-
-   das Datenverarbeitungssystem (1) die ange-  40       stellenvorrichtung (1 O) ergibt,
forderten Daten f�r eine vorbestimmte Zeitdau-       -   die Datenschnittstellenvorrichtung (1 O) die zu-
er T,,_ h�lt und eine Transaktion betreffend den          gegriffen Daten zu der Datenmanagementvor-
spezifischen  Daten  durchf�hrt,  wenn  eine          richtung (9) �ber das Sicherheitsnetzwerk (6)
Transaktionsanforderung durch die Eingabe-          �bertr�gt und
einheit (2) w�hrend der vorbestimmten Zeit-  45    -   die Datenmanagementvorrichtung (9) die Da-
daUer T,e_ eingegeben Wird.                     ten ZUr e Kternen V_rriChtUng (7) �ber daS Si-
cherheitsnetzwerk (6) �bertr�gt.
2.  Computersystem nach Anspruch 1, in dem die erste
Maske eine Handelsmaske (1 3) und die zweite     8.  Computersystem nach Anspruch 6 oder 7, in dem
Maske eine Quotationsmaske (1 4) ist.          50    jede sendende Vorrichtung eine entsprechende
Identifikationsinformation �bertr�gt und das Sicher-
3.  Computersystem gem�� Anspruch 1 oder 2, in dem       heitsnetzwerk (6) f�r jeden Datentransfer durch
die Handelsmaske (1 3) die Eingabe einer ldentifi-       dasselbe �berpr�ft, ob die ldentifikation der ent-
kationsnummer f�r die spezifischen Daten durch       sprechenden sendenden Vorrichtung mit vorge-
die Eingabeeinheit (2) erlaubt, um spezifische Da-  55    speicherten lnformationen �bereinstimmt, welche
ten von dem Dateneingang (5) zu lesen.             w�hrend einem Autorisationsverfahren gespeichert
werden.
4.  Computersystem nach Anspruch 3, in dem die lden-  9
