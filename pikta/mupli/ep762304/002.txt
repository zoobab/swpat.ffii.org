                           1             EP O 762 304 B1             2
Description                              cluding the read data. After having received the request-
ed data, the data processing system holds the data for
The invention relates to a computer system for data     a predetermined time period and performs a transaction
management and to a method for operating said sys-     relating to the specific data, if a transaction request is
tem, and more particularly, to an automated warrant  5  input by the input unit during a predetermined time pe-
trading system (citi CATS-OS).                   riod.
Over the last years the market for warrant trading       Since the data processing system holds the re-
has been rapidly increasing. From 1 991 to 1 995 the     quested data, e.g. a warrant rate with a specific stock
amount of warrants traded grew from about 400 to     eKchange number and a volume thereof, for a predeter-
4,OOO.                               IO  mined time period, it is only possible to perform a trans-
According to the prior art, the trading of warrants is     action, e.g a buy request, based on the displayed data
very often time- and cost-intensive, since, upon a con-     during that time period. When this time period has
sumer inquiry, it is necessary for the customer in a local     elapsed, the request for a transaction cannot be input
bank to start a search by telephone or a search in pub-     any longer, so that a re-request for new specific data
Iications to find the best warrant for the consumer. Usu-  15  has to be issued. Such a re-request causes the reading
ally, if the consumer wishes to purchase warrants, he     of the data input in order to gain new actual data, e.g.
first contacts his local bank which is processing the or-     the warrant rates.
der through the stock eKchange or placing a call to a       The computer system according to the invention al-
warrant market maker. Since an on-line information and     lows to display specific data on-line including warrant
trading system about traded warrants with eKecutable  20  rates, wherein the display unit displays a trading mask
prices does not eKist, the customer in the local bank is     allowing the input of a request for the specific data. The
not able to provide the consumer with the actual on-line     design of the trading mask is independent of the type of
information. Hence, there eKists a time difference be-     the specific data. Therefore, the trading mask will be
tween the placing of the buylsell order by the customer     similar, if the data management comprises the manage-
and its implementation, resulting in a risk of eKchange  25  ment of data relating to the trading of stocks, bonds, de-
andlor price rate fluctuations in that time. It is even often     rivatives or foreign eKchange. The same applies to the
the case that the implementation of the buylsell order     quotation mask which also has a similar format for dif-
was based on a limit oriented on the rates of the day     ferent types of data. Therefore, the user does not need
before, since the order could not be implemented on the     to get used to a newformat of the trading orthe quotation
same day. Reasons for this time delay are either the fact  30  mask, if he handles different types of data. This leads
that the order is placed a couple of hours before it will     to an easy-to-use system, even if different types of data
be eKecuted at the stock eKchange or that the dealing     are to be handled.
room phones at the market maker are often busy during       The system according to an embodiment of the in-
the hectic market times. Therefore, local banks often     vention  provides  instantaneous eKecutable warrant
place blind orders at unknown prices for the customers  35  rates, so that orders can be immediately fulfilled for cus-
and they do not receive immediate deal confirmations.     tomers. Accordingly, the risk of eKchange andlor price
The object of the present invention is to realize a     rate fluctuations is eliminated, since an eKecutable price
computer system for data management and a method     is electronically provided and it is possible to act on it.
for operating said system, which realize a data manage-     Furthermore, an immediate electronic trade confirma-
ment providing instantaneous data with an improved ac-  40  tion is provided with a complete audit trail. With the in-
curacy and a reduced error probability when processing     ventive system, even smaller banks can improve their
data transactions, combined with a high security.        efficiency by providing real time rates and information,
The object is solved according to the features of the     since the system can also be provided with a mail and
independent claims. The dependent claims show ad-     request for quote (RFQ) function allowing a communi-
vantageous embodiments and further developments of  45  cation between the customer and the trader.
the invention.                                It is possible to process transactions at the price on
The computer system for data management includ-     the display unit, which is a binding contract between the
ing at least the management of data relating to the trad-     customer and the trader. This protects against any mis-
ing of warrants, comprises a data processing system,     understandings between the customer and the trader,
an input unit, a display unit and a data input. In order to  50  which can easily happen if a buylsell order is placed via
perform a transaction of data, the display unit displays     telephone.
a first mask having a format allowing the input of a re-       The system is designed to eKpand and enhance the
quest for specific data by the input unit. The specific data     qualitiy and efficiency of the user's overall warrant op-
can relate to warrant rates. If the request for specific da-     eration, enabling improved customer service and in-
ta is input, e.g. a request for a specific warrant, the data  55  creasing the capacity with minimum incremental costs.
input is read which can receive permanently a data       ln accordance with an embodiment of the invention,
stream including warrant rates. After having read the da-     the system is realized using the client-server architec-
ta stream, the display unit displays a second mask in-     ture. A number of devices communicate using a security
2
