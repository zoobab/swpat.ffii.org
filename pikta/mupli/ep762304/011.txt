                           1 9             EP O 762 304 B1             20
geben wird.                          Revendications
1 9. Verfahren nach Anspruch 1 8, in dem die erste Mas-     1.  Syst�me informatique pour la gestion de donn�es
ke eine Handelsmaske (1 3) ist, die die Eingabe ei-       dont au moins la gestion de donn�es relatives au
ner ldentifikationsnummer f�r die spezifischen Da-  5     n�goce de titres au porteur, comprenant un syst�-
ten durch die Eingabeeinheit (2) erlaubt, um die       me de traitement de donn�es (1), une unit� d'entr�e
spezifischen Daten von dem Dateneingang (5) zu       (2), une unit� d'affichage (3) et une entr�e de don-
Iesen.                                  n�es (5) recevant au moins des cours de titres au
porteur, dans lequel.
20. Verfahren nach Anspruch 1 9, in dem die ldentifika-  IO
tionsnummer eine Wertpapierkennummer ist.           -   l'unit� d'affichage (3) affiche une premi�re page
ayant un format permettant d'introduire par l'in-
21. Verfahren nach Anspruch 1 8, in dem die erste Mas-          term�diaire de l'unit� d'entr�e (2) une demande
ke die Eingabe einer Anforderung f�r eine Kursseite          de donn�es sp�cifiques, lesquelles compren-
(1 8) ist, die eine Vielzahl von spezifischen Daten  15       nent au moins des cours de titres au porteur,
enth�lt und die Eingabe der Anforderung f�r spezi-       -   l'entr�e de donn�es (5) est lue si la demande
fische Daten erlaubt.                            est introduite par l'interm�diaire de l'unit� d'en-
tr�e (2),
22. Verfahren nach Anspruch 21, in dem die Kursseite       -   l'unit� d'affichage (3) affiche une seconde page
(1 8) die Eingabe einer Kurserneuerungsanforde-  20       comprenant les donn�es demand�es, et
rung erlaubt, was ein Lesen der aufgelisteten Mehr-       -   le syst�me de traitement des donn�es (1) main-
zahl von spezifischen Daten zur Folge hat und da�          tient les donn�es demand�es pendant une p�-
dieselben in der Kursseite (1 8) auf der Anzeigeein-          riode de temps pr�d�termin�e T,,_ et effectue
heit (3) dargestellt werden.                        une transaction se rapportant auK donn�es
25       sp�cifiques, si une demande de transaction est
23. Verfahren nach zumindest einem der Anspr�che 1 8          introduite dans l'unit� d'entr�e (2) pendant la
biS 22, in dem die Eingabe einer AnfOrderUng f�r          peri Ode de tempS predeterminee T,e_.
spezifische Daten oder die Kurserneuerungsanfor-
derung das Lesen eines Datenstroms verursacht,     2.  Syst�me informatique selon la revendication 1,
der permanent von dem System durch den Daten-  30    dans lequel la premi�re page est une page de n�-
eingang (5) empfangen wird.                    goce (1 3) et la seconde page est une page de co-
tation (1 4).
24. Verfahren nach zumindest einem der Anspr�che 1 8
bis 23, in dem die zweite Maske eine Quotations-     3.  Syst�me informatique selon la revendication 2,
maske (1 4) ist, die die Eingabe einer Wiederanfor-  35    dans lequel la page de n�goce (1 3) permet l'intro-
derung f�r spezifische Daten erlaubt, wenn die       duction par l'interm�diaire de l'unit� d'entr�e (2)
Transaktionsanforderung nicht w�hrend der vorbe-       d'un num�ro d'identification relatif auK donn�es
Stimmten ZeitdaUer T,e_ eingegeben Wird.             SpeCifiqUeS, en VUe de lire leS dOnneeS SpeCifiqUeS
provenant de l'entr�e de donn�es (5).
25. Verfahren nach zumindest einem der Anspr�che 1 8  40
bis 24, in dem ein Anzeigen einer die Transaktions-     4.  Syst�me informatique selon la revendication 3,
informationen enthaltenden Handelsbest�tigungs-       dans lequel le num�ro d'identification est un num�-
maske (1 5) auf der Anzeigeeinheit (3), wenn die       ro de bourse.
Transaktionsanforderung  rechtzeitig  eingegeben
wird.                             45  5.  Syst�me informatique selon la revendication 2,
dans lequel la page de n�goce (1 3) permet l'intro-
26. Verfahren nach zumindest einem der Anspr�che 1 8       duction d'une demande pour une page de cours
bis 25, in dem die Handelsmaske (1 3) und die Quo-       (1 8) comportant une pluralit� de donn�es sp�cifi-
tationsmaske (1 4) auf der Anzeigeeinheit (3) auf       ques, et dans lequel la page de cours (1 8) permet
verschiedenen Bildschirmen oder auf einem Bild-  50     l'introduction de la demande pour les donn�es sp�-
schirm angezeigt werden.                      cifiques.
21. Verfahren nach zumindest einem der Anspr�che 1 8     6.  Syst�me informatique selon l'une au moins des re-
bis 26, in dem das Datenmanagement das Mana-       vendications 1 � 5, dans lequel le syst�me de trai-
gement von Daten betreffend dem Handel von Ak-  55    tement de donn�es (1) comprend un r�seau de s�-
tien, Bondes, Derivativen oder Devisen enth�lt, wo-       curit� (6) connect� � un circuit eKterne (7), un circuit
bei die spezifischen Daten Aktienkurse, Bondkurse,       de s�curit� (8), un circuit de gestion de donn�es (9),
Derivativkurse oder Devisenpreise enthalten.           un circuit d'interface de donn�es (1 O) et un circuit
1 1
