                           7             EP O 762 304 B1             8
again displayed with updated data (step 1 06). If the pre-     and allows the input of a customer reference number.
determined time periOd T_e_ at Step 1 08 did n Ot elapSe     ThiS SCreen giVeS the CUStOmer an in StantaneOUS eleC-
before the buy request was input in step 1 07, a trans-     tronic confirmation of the eKecuted warrant trade.
action of warrant is performed (step 1 1 2). Thereafter a       Fig. 1 O shows the transaction screen 1 3 with the
trade confirmation screen 1 5 is displayed (step 1 1 3).    5  pull-down menu allowing the input of the view rates re-
Fig. 4 shows a flow chart relating to another embod-     quest.
iment of the present invention. When displaying the cus-       ln Fig. 1 1 there is shown the blank view rates page
tomer trading screen 1 3, it is possible to input a view     1 6 displayed on the display unit 3, if the view rates re-
rates request (step 1 1 4). The view rates request can be     quest is issued.
input, e.g., by opening a pull-down menu on the trading  IO     ln Fig. 1 2 there is shown the rates page screen 1 9
screen 1 3. This can be performed, if the customer is, e.     which displays pages of warrant rates to the customer
g., not eKactly sure about the stock eKchange number     and trader. This screen also permits the user to display
of the warrant to be bought or sold. Furthermore, it is     a particular page by entering the page number. Further-
possible, that the customer only wants to get an over-     more, the user may enter details and search for a par-
view of actual rates and data of warrants. After having  15  ticular warrant by name. Moreover, the user may use
input the view rates request, a rates page screen 1 6 is     the plus or minus input to view pages sequentially. In
displayed (step 1 1 5). On the rates page screen 1 6 a par-     addition, a refresh rates request may be input by actu-
ticular page 1 7 can be selected (step 1 1 6) and a partic-     ating the arrow button. If a certain instrument is selected
ular warrant can be accessed by selecting a warrant     on the rates page screen 1 7 (the same can be selected,
stock eKchange number 1 8 (step 1 1 7). Thereafter, the  20  e.g., by a double click on the stock eKchange number
customer trading screen 1 4 is displayed (step 1 1 8) and     with the mouse), the quotation screen 1 4 is displayed
the volume and whether to buy or sell is input via the     (Fig. 8).
input unit 3 (step 1 1 9). Thereafter, the procedure goes       Fig. 1 3 shows the trade detail screen displaying all
back to the process shown in Fig. 3 between steps 1 05     the transaction details about a particular trade to the
and 1 06.                              25  customer or trader.
Fig. 5 shows the access control screen 1 2 which is       Fig. 1 4 shows a transaction history screen which is
used for inputting the user name and the security pass-     displayed on the display unit 3 when the past deals are
word. After the user name and the security password     to be displayed. The system allows to set the criteria of
have been input, a logon request can be issued.         the deals which are to be displayed, e.g. the date range,
Fig. 6 shows the customer trading screen 1 3. The  30  the customer reference number, the trade identification
customer trading screen is preset to the trading of war-     and the instrument. The data parameters may span a
rants. However, this screen can also be used for the     maKimum of 60 days, since this is the maKimum storage
trading of other securities, e.g. stocks, bonds, deriva-     time of trade data. However, it is possible to set a differ-
tives or foreign eKchange. Furthermore, it is possible to     ent storage time, so that it is possible to review deals
input in the customer trading screen 1 3 whether a war-  35  which were performed earlier.
rant (or another security) is to be bought or sold.           The system as described before with reference to
Fig. 7 shows the customer trading screen 1 3 with     the Figures 5 - 1 4 is designed for being used by a cus-
an instrument number and a volume input via the input     tomer sitting at a local bank and a trader being located
unit (3).                                 at the warrant market maker. However, there do eKist
Fig. 8 shows the quotation screen 1 4 after the get  40  also features which are only accessible by the trader.
price request has been input, while displaying the cus-     EKamples of these features are described in the follow-
tomer trading screen (Fig. 7). The quotation screen 1 4     ing.
shows all the data necessary for a warrant transaction.       Fig. 1 5 shows a sanity check screen allowing the
Upon the display of the quotation screen 1 4, the timer     input of a maKimum rise and fall amount of the rates in-
is started (step 1 1 1) which only allows the input of a buy  45  put in the system. More particularly, with the sanity
reqUeSt Within the predetermined time peri Od T_e_. If the     CheCk it iS pOSSible tO preSet a maKimUm VariatiOn Of the
bUy reqUeSt iS n Ot inpUt dUring the time peri Od T_,_ a     inpUt data 5. If, e.g., the rate Of a SpeCifiC Warrant Chang-
time-out notice will be displayed in the quotation screen     es from one view rates request to the ne Kt view rates
1 4. If such a time-out notice appears on the quotation     request by O.2 and the sanity check is set to O. 1, this
screen 1 4, it is no longer possible to issue a buy request  50  specific instrument will be suspended. Furthermore, it is
for specific warrants. In order to load new data onto the     possible to input a bandwidth for the rates. The sanity
quotation screen, it is necessay to input a re-request.     check introduces a high security in the system, since it
If such a re-request is input, the quotation screen 1 4 will     makes it possible to filter out system faults like data
be re-filled with actual data and the timer will be started     transmission errors.
again.                               55     Fig. 1 6 shows the price time-out screen. In the price
Fig. 9 ShOWS the trade COnfirmatiOn SCreen 1 5 WhiCh     time-OUt SCreen it iS pOSSible tO inpUt the time T_e_ WhiCh
is displayed when the buy request is input in time. The     is used to determine if a buy request input in the quota-
trade confirmation screen 1 5 includes a trade number     tion screen 1 4 is performed or not (Fig. 3, steps
5
