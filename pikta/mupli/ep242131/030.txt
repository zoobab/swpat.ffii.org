                                         EP O 2_2 _3_ B_
Eingangsterminal-Piktogramme in dem Bedienungsfeld eingestellt werden, um die zugeordneten Werte
anZUZeigen.
_   Revendications
_.  Proc�d� pour produire un programme de mod�lisation d'un processus physique pour un calculateur uti-
Iisant des ordinogrammes de donn�es, dans lequel le calculateur comprend une m�moire qui stocke une
pluralit� de fonctions ex�cutables et une pluralit� de donn�es de types divers, un affichage, des moyens
Io      pour recevoir une entr�e d'un utilisateur, et un processeur de donn�es, le proc�d� �tant caract�ris� par
Ies �tapes suivantes.
assembler, en r�ponse � une entr�e d'un utilisateur, un ordinogramme de donn�es sur l'affichage,
l'ordinogramme de donn�es sp�cifiant le processus et comprenant des icones de fonction correspondant
� celles correspondantes d'une pluralit� de fonctions ex�cutables, des icones terminales correspondant
1_      � celles correspondantes d'une pluralit� de types de donn�es, une icone de structure indiquant un trajet
de commande de l'ordinogramme de donn�es, et des arcs reliant les icones de fonction, icones terminales
et icones de structure; et
produire un programme ex�cutable en r�ponse � l'ordinogramme de donn�es, le programme ex�-
cutable comprenant une ou plusieurs des fonctions ex�cutables telles qu'indiqu�es par les icones de fonc-
2o      tion et reli�es ainsi qu'indiqu� par les arcs pour fonctionner avec des donn�es identifi�es par les icones
terminales, le programme ex�cutable ayant un trajet de commande �tabli ainsi qu'indiqu� par l'icone de
structure.
2.  Proc�d� selon la revendication 1, dans lequel l'icone de structure comprend une icone d'it�ration, et l'�ta-
2_      pe de production du programme ex�cutable comprend en outre l'�tape consistant � �tablir des constru_
tions d'it�ration dans le programme ex�cutable tel qu'indiqu� par l'icone d'it�ration.
3.  Proc�d� selon la revendication 2, dans lequel l'�tape d'assemblage de l'ordinogramme de donn�es
comprend en outre une �tape consistant � positionner une icone de fonction dans l'ordinogramme de don-
3o      n�es pratiquement � l'int�rieur d'une icone d'it�ration dans l'ordinogramme de donn�es.
_.  Proc�d� selon la revendication 2 ou 3, dans lequel l'icone de structure comprend en outre une icone de
registre � d�calage associ�e � l'icone d'it�ration, et l'�tape de production du programme ex�cutable
comprend en outre l'�tape consistant � inclure des constructions de commande it�rative en boucle ferm�e
3_      dans le programme ex�cutable ainsi qu'indiqu� par l'icone de registre � d�calage et l'icone d'it�ration.
5.  Proc�d� selon l'une quelconque des revendications pr�c�dentes, dans lequel l'icone de structure
comprend une icone de condition, et l'�tape de production du programme ex�cutable comprend en outre
l'�tape consistant � inclure des constructions de s�quences conditionnelles de donn�es dans le program-
me ex�cutable ainsi qu'indiqu� par l'icone de condition.
40   6.  Proc�d� selon la revendication 5, dans lequel l'�tape d'assemblage de l'ordinogramme de donn�es
comprend en outre l'�tape consistant � positionner une ou plusieurs icones de fonction dans l'ordinogram-
me de donn�es pratiquement � l'int�rieur de l'icone de condition dans l'ordinogramme de donn�es.
4_   l.  Proc�d� selon l'une quelconque des revendications pr�c�dentes, dans lequel l'icone de structure
comprend une icone de s�quence, et l'�tape de production du programme ex�cutable comprend en outre
l'�tape consistant � inclure des constructions de commande de s�quence dans le programme ex�cutable
ainsi qu'indiqu� par l'icone de s�quence.
_o   8.  Proc�d� selon la revendication 7, dans lequel l'�tape d'assemblage de l'ordinogramme de donn�es
comprend en outre l'�tape consistant � afficher une des icones de fonction pratiquement � l'int�rieur de
l'icone de s�quence.
9.  Proc�d� selon l'une quelconque des revendications pr�c�dentes, dans lequel l'icone de structure
__      comprend une icone de boucle ind�finie, et l'�tape d'assemblage de l'ordinogramme de donn�es
comprend en outre l'�tape consistant � inclure des constructions � boucle ind�finie dans le programme
ex�cutable ainsi qu'indiqu� par l'icone de boucle ind�finie.
3O
