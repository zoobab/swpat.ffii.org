                                      EP O 2_2 _3_ B_
ofgenerating the executable program furthercomprises the step of including sequence control constructs
in the executable program as indicated by the sequence-icon.
8.  The method of Claim 7, wherein the step of assembling the data flow diagram further comprises the step
5      Of diSplaying One Of the funCtiOn-iCOnS SubStantially within the SequenCe-iCOn.
9.  The method of any preceding claim, wherein the structure-icon comprises an indefinite loop icon, and
the step ofgenerating the executable program further comprises the step of including indefinite loop con-
structs in the executable program as indicated by the indefinite loop icon.
IO   _ O.  The meth Od Of Cla'lm 9, Whefe'ln the Step Of aSSembl'lng the data fIOW d'lagfam fUfthef COmpf'ISeS the Step
of positioning one of the function-icons substantially within the indefinite loop icon.
__.  The method of any preceding claim, further comprising the step of providing a library ofone or more user-
defined functions.
15   _2.  The method of any preceding claim, wherein the function-icons include user-defined-function-icons, and
the step of generating the executable program further comprises the step of including at least one of the
one or more user-defined functions in the executable program as indicated by the user-defined-function-
'ICOnS.
20   _3.  The method of any preceding claim, wherein the data flow diagram is acyclic.
__.  The method ofany preceding claim, wherein-the executable program comprises a program for performing
real-time data analysis.
25   _ 5.  The method of any preceding claim, wherein the executable program comprises a program for simulating
an instrument.
_ 6.  The method of any preceding claim, wherein the executable program and the display comprise a virtual
3o     instrument.
_ l.  The method of any preceding claim, further comprising the step of assembling a plurality of data flow di-
agfamS.
_ 8.  The method of Claim 1 7, wherein the step of assembling the plurality of data flow diagrams further com-
35     prises the steps of including one or more data-flow-diagram-icons in at least one of the plurality of data
flow diagrams, the one or more data-flow-diagram-icons corresponding to respective ones ofthe plurality
of data flow diagrams, and interconnecting the function-icons, terminal-icons, structure-icons, and data-
flow-diagram-icons in the at least one of the plurality of data flow diagrams, by arcs in the at least one of
the plurality of data flow diagrams; and
40         the step ofgenerating the executable program further comprises the steps ofgenerating a plurality
of executable programs in response to the plurality of data flow diagrams, and linking the executable pro-
grams in response to the data-flow-diagram-icons in the at least one of the data flow diagrams.
_9.  The method of any preceding claim, further comprising the step of assembling a panel on the display in
45     response to user input, the panel representing a user interface for the executable program, and wherein
the step of generating the executable program is further responsive to the panel.
20.  The method of Claim 1 9, further comprising the step of simultaneously displaying the data flow diagram
and the panel on the display.
50   2_.  The method of Claim 1 9 or 20, further comprising the step of executing the executable program in re-
sponse to input values provided through the panel.
22.  The method of Claim 1 9, 20 or 21, wherein the step of assembling the panel further comprises the steps
55      of.   including input terminal-icons in the panel, the input terminal-icons indicating input values to ter-
minal-icons in the data flow diagram; and
including output terminal-icons in the panel, the output terminal-icons indicating output values for
2_
