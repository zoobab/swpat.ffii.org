                                      EP O 2_2 _3_ B_
to a ''Ramp'' icon. This icon is built-in function which takes input minimum and maximum values, number of
steps, and a flag to indicate linear or log steps, and produces as output an array of sample points, in this case
the frequencies of interest. The output is a bold one, which indicates that the data type is an array.
The large rectangular region in the center ofthe diagram is an iteration loop structure. The diagram placed
5   inside this region will be executed multiple times. for i = O to N-1. The inputs to the iteration loop are amplitude
and the array of frequencies, shown on the left. The amplitude remains the same data type as it crosses the
Ioop boundary. The array, however, as shown by the change in line type, is automatically indexed as it crosses
the boundary to provide one array element per iteration. Since there is no connection to the loop-count (the
N at the upper left corner), the length of the input array is used by default as the number of iterations.
Io     lnside the iteration loop are two virtual instrument icons. The first takes as input an amplitude and a fre-
quency and performs the appropriate IEEE-488 operations to set the function generator 208 of Fig 21. The
second performs the appropriate IEEE-488 operations to obtain a voltage measurement from the multimeter
21 O of Fig. 21. The dotted line indicates that there is no data flow, but ensures that they execute sequentially.
These two icons represent simple virtual instruments that are easily designed using built-in high level IEEE-
15   488 functions to communicate with the multimeter 21 O.
Each iteration of the iteration loop produces one voltage measurement. This results in an array of values,
as shown by the line change as it exits the loop at the right. The graph function takes this array and the array
offrequencies as input and produces as output the data structure for the front panel graph indicator. Note the
self-documenting effect of the graphical language, with the iteration loop structure contributing to the read-
2o   ability of the program.
With the front panel and block diagram complete, the instrument is ready to be used. The instrument is
operated from the front panel. To execute the instrument, the user simply configures the input controls and
''clicks'' the GO button on the top of the screen (as will be appreciated from the description below).
25   Operation of the Preferred Embodiments
The presently preferred embodiments ofthe invention are implemented in software. The following descrip-
tion explains the operation of the presently preferred embodiment as implemented using either an Apple Mac-
intosh Plus Computer or an Apple Macintosh Computer. The explanation is intended to be illustrative and not
3o   exhaustive and uses specific examples to explain the principles of operation of the preferred embodiments.
Itwill be appreciated that the principles ofoperation explained belowwill serve as a basis forthe heuristic learn-
ing process necessary to fully appreciate and practice the full scope of the present invention. Thus, it is not
intended that the scope and content of protection afforded to the present invention as claimed be limited in
any way by the following explanation.
35     The following explanation explains how to use the implemented functions of the preferred embodiments.
A walk through of the display menus is provided first, with working functions described. This presentation is
followed by a walk through of an example application program that computes Fibonacci numbers.
To start, double click on PROTO icon. The PROTO icon is illustrated in Fig. 23a.
The opening screen is shown in Fig. 23b. This screen is the front panel window on which controls can be
40   placed. The tools and controls below the menu bar will be described later.
Figure 24 shows the contents of the FILE menu. In this and subsequent screen pictures, items in clear
surrounds can be selected. Stippled items do not yet work. The NEW, OPEN, CLOSE, SA_E and SA_E AS...
items work as in any other Maclntosh program that follows Apple's user-interface guidelines. NEW opens a
new instrument, OPEN brings up a dialog box with existing instruments' names, CLOSE closes the current
45   instrument, SA_E saves the instrument with the current file name, and SA_E As... saves a copy of the current
instrument under the user-specified instrument name. OPEN DIAGRAM opens the block diagram window. Its
use will be describe later. QUIT exits from PROTO and returns the user to the Finder.
Figure 25 shows the EDIT menu selections. This one is easy. Only CLEAR works. CLEAR is useful for
removing items from the active window, e.g., selected wires and structures from the block diagram window,
50   or controls from the front panel window.
Figure 26 shows the FORMAT menu. The FORMAT menu has five working items. The first two FULL
SCREEN and TILE alter the screen display. The FULL SCREEN options produces over-lapping windows. The
TILE option produces non-overlapping windows. Both the FULL SCREEN and TILE selections can be reverted
by selecting them again. The TILE option is dimmed if a block diagram window is not open.
55     Figure 27 shows the contents of the CONTROL menu. The CONTROL menu is only available when the
front panel window is active. The NUMERIC, STRING and GRAPHICAL items all work to some degree. Choos-
ing the NUMERIC item brings up the dialog box shown in Figure 28. This box offers the user many choices,
however, only a few are implemented at this time. The small glyphs half-way down the left side of the box offer
_2
