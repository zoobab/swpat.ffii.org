                                         EP O 2_2 _3_ B_
32.  Proc�d� selon la revendication 29, 30 ou 31, dans lequel.
l'�tape d'assemblage de l'ordinogramme de donn�es comprend en outre d'inclure une icone de
structure dans l'ordinogramme de donn�es, l'icone de structure indiquant un trajet de commande de l'or-
dinogramme de donn�es; et
5          l'�tape de cr�ation du programme ex�cutable comprend en outre d'�tablir un trajet de commande
dans le programme ex�cutable ainsi qu'indiqu� par l'icone de structure.
33.  Proc�d� selon la revendication 32, dans lequel l'icone de structure comprend une ou plusieurs parmi une
icone d'it�ration, une icone de condition, une icone de s�quence, et une icone de boucle ind�finie dans
_o      l'ofdinogfamme de donn�eS.
3_.  Proc�d� selon l'une quelconque des revendications 29 � 33, dans lequel le programme ex�cutable fournit
une proc�dure pour commander un instrument, le proc�d� comprenant en outre les �tapes suivantes.
assigner des valeurs � une ou plusieurs des icones terminales d'entr�e dans le panneau afin de
_5      fournir des valeurs aux icones terminales dans l'ordinogramme de donn�es; et
ex�cuter le programme ex�cutable pour commander l'instrument et produire les valeurs de sortie.
35.  Proc�d� selon la revendication 34, comprenant en outre l'�tape consistant � placer celle ou celles des
icones terminales d'entr�e dans le panneau afin d'afficher les valeurs assign�es.
20   36.  proce_de_ selon l,une quelconque des revendications 29 � 33, dans lequel le programme exe_cutable rournit
une proc�dure pour simuler un instrument virtuel, le proc�d� comprenant en outre les �tapes suivantes.
assigner des valeurs � une ou plusieurs des icones terminales d'entr�e sur le panneau afin de four-
nir des valeurs aux icones terminales dans l'ordinogramme de donn�es; et
ex�cuter le programme ex�cutable pour simuler le fonctionnement de l'instrument virtuel et pro-
25      duire les valeurs de sortie.
31.  Proc�d� selon la revendication 36, comprenant en outre l'�tape consistant � placer celle ou celles des
icones terminales d'entr�e dans le panneau afin d'afficher les valeurs assign�es.
30
35
40
45
50
55                                 33
