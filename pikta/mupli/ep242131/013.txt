                                    EP O 2_2 _3_ B_
the choice of display style. clicking on the chosen glyphs produces a reverse-video display of that pictograph.
MINIMUM, MAXIMUM and DEFAULT work by selecting their adjacent frames and filling in the desired values.
Arrays can be specified by choosing the number of dimensions and inserting that number into the frame to
the leftofARRA_ DIMENSIONS. The numberofelements in each array dimension is then specified by entering
5   appropriate values into the dimension frames numbered 1 to 8. Selecting the INDICATOR ONL_ box makes
the control a passive indicatoron the front panel. Selecting SHOW DIGITAL READOUT provides a digital read-
out of the value of the control. The current value of the control can be altered by adjusting the control itself or
by altering the value in the digital read out by selecting and entering values from the keyboard.
Note the CONTROL menu is dimmed when the block diagram window is the active window. It is made avail-
Io  able by making the front panel window the active window.
Figure 29 shows the dialog box for the STRING control. String controls can be filled with values for use
as controls or indicators. This capability is useful for sending command or address strings for writing to and
reading from a GPIB.
Figure 30 shows the dialog box for the GRAPHICAL control. This control is used to display graphs. It must
15   be used with the INDICATOR ONL_ box selected.
The FUNCTIONS menu name is dimmed when the front panel window is active. It becomes available when
the block diagram window is made the active window.
Figure 31 shows the FILE menu displayed from the front panel window and the OPEN DIAGRAM item se-
Iected. Selecting this item opens a block diagram window. Figure 32 shows the screen for a new block diagram
2o  window.
Notice that the untitled block diagram window is distinguishable from an untitled front panel window by
the presence of the black bullets (_____) in the title bar and the dimming of the CONTROL menu name. A block
diagram is created by choosing items from the FUNCTIONS menu. Figure 33 shows the contents of the Fun_
tions menu. Only a subset offunctions from the items SPECIAL, ARITHMETIC, COMPARATIVE, INPUT-OUT-
25   PUT are working. Single functions are also available from STRING, SIGNAL PROCESS and CURVE FIT. The
working functions from each of the menu options will be described in turn.
The functions available from the SPECIAL option of FUNCTIONS menu include STRUCTURES, SPECIAL
ITEMS, and CONSTANTS. Each of the available functions is described below.
3o   Structures
To remedy shortcomings of the conventional data flow, which makes it substantially unusable as a pro-
gramming language, the present invention includes graphical operators. These structures implement in a data
flow environment what typical structured programming control do.
35   Sequence Structure
The sequence structure in its simplest form serves to divide the data flow diagram into two subdiagrams,
the inside of the structure and the outside of the structure. The outside diagram behaves exactly as if the
4o  sequence structure and its contents were replaced by an icon with hotspots foreach line crossing the structure
border.
Figure 34 shows a 3D view of a three diagram sequence; however, to minimize screen space, only one
diagram at a time is visible. Inside the structure border are multiple diagrams which execute in sequence. This
sequence is indicated by the numeral in the upper leftcorner. When the first diagram in the sequence completes
45  execution, the next one begins. The process is repeated until all the diagrams in the sequence have been exe-
cuted.
Each diagram in the sequence uses a subset of the incoming arcs and produces a subset of the outgoing
arcs (the outgoing subsets must be mutually exclusive to avoid arc fan in, but the incoming subsets are arbi-
trary). Constants may be used within any of the diagrams without any constraints. Variables used within a di-
50  agram are strictly local to the sequence structure and may be assigned only once. Variables can be used mul-
tiple times in the diagrams following the diagram where the variable was defined.
The sequence structure does not begin execution until all the incoming arcs have data available, and none
of the outgoing arcs produce data until all the diagrams have completed execution. There may be at most one
arc that terminates at the sequence structure border itself, acting as a trigger for the sequence (it is just one
55   more input which must be present for the sequence to begin). An arc originating at the sequence structure bor-
der may be used as a ready signal indicating that the sequence has completed execution.
_3
