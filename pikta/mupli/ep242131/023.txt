                                      EP O 2_2 _3_ B_
panel display permits a user to easily understand how data is provided to a system being modelled and how
data is provided by the system. The block diagram editor permits a user to construct a graphical representation
of a procedure for producing output data from input data using icons which reference modularized procedural
units. A user may use the icon editor to construct hislher own icons; or helshe may call upon a ready-made
5   library of icons. The execution subunit executes execution instructions which are constructed in response to
the graphical images produced by a user to model a process. Thus, a user can program a computer substan-
tially by construsing a hierarchy of icons connected to one-another so as to model a process. A user, therefore,
can use the system and method ofthe present invention to program a computerto model a process using graph-
ical techniques which generally are easier to comprehend.
Io     Furthermore, the method of the present invention advantageously can use data flow techniques. The use
ofthe structures, illustrated in Figs. 8-1 7, facilitates the use of such data flow techniques. By using such tech-
niques, a system modelled in block diagram form can operate in a parallel fashion, since each individual icon
in a hierarchy comprising such a block diagram, operates as soon as all input data provided to it are available.
In addition, such structures render graphical representations of block diagrams using such data flow techni-
15   ques more comprehensible to a user, and, therefore, simplify the task of using such techniques. It will be un-
derstood that the above-described embodiments and methods are merely illustrative ofmany possible specific
embodiments and methods which can represent the principles of the invention. Numerous and varied other
arrangements and methods can be readily devised in accordance with these principles without departing from
the scope ofthe invention claimed. Thus, the foregoing description is not intended to limit the invention claimed.
20   Claims
_.  A method of producing a program modelling a physical process for a computer using data flow diagram-
25     ming, wherein the computer includes a memory which stores a plurality ofexecutable functions and a plur-
ality of data ofvarious types, a display, means for receiving user input, and a data processor, the method
characterised by the steps of.
assembling, in response to user input, a data flow diagram on the display, the data flow diagram
specifying the process and including function-icons corresponding to respective ones of the plurality of
3o     executable functions, terminal-icons corresponding to respective ones of the plurality of data types, a
structure-icon indicating control flow of the data flow diagram, and arcs interconnecting the function-
icons, terminal-icons, and structure-icon; and
generating an executable program in response to the data flow diagram, the executable program
including one or more of the executable functions as indicated by the function-icons and interconnected
35     as indicated by the arcs to operate upon data identified by the terminal-icons, the executable program
having control flow established as indicated by the structure-icon.
2.  The method of Claim 1, wherein the structure-icon comprises an iteration-icon and the step ofgenerating
the executable program further comprises the step of establishing iteration constructs in the executable
go     program as indicated by the iteration-icon.
3.  The method of Claim 2, wherein the step of assembling the data flow diagram further comprises the step
of positioning a function-icon in the data flow diagram substantially within an iteration-icon in the data
flow diagram.
45   _.  The meth Od Of Claim 2 Of 3, Whefein the StfUCtUfe-iCOn fUfthef COmpfiSeS a Shift fegiStef-iCOn aSSOCiated
with the iteration-icon, and the step of generating the executable program further comprises the step of
including iterative feedbackcontrol constructs in the executable program as indicated by the shift register-
icon and the iteration-icon.
''   _.  The method or any preceding claim, wherein the structure-icon comprises a conditional-icon, and the
step of generating the executable program further comprises the step of including conditional flow
constructs in the executable program as indicated by the conditional-icon.
6.  The method of Claim 5, wherein the step of assembling the data flow diagram further comprises the step
55      of positioning one or more of the function-icons in the data flow diagram substantially within the condi-
tional-icon in the data flow diagram.
l.  The method of any preceding claim, wherein the structure-icon comprises a sequence-icon, and the step
23
