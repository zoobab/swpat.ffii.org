                                      EP O 2_2 _3_ B_
the cursor to the box labelled OK and click. This action will close the dialog box and return you to the front
panel window where you should see a larger representation of the pointer control you selected from the dialog
box inside a moving dashed rectangle. Controls can be moved about the front panel window just as objects
were moved in the block diagram window. Notice that the behaviorofthe controls is more like the non-structure
5   objects in the block diagram window. the entire control is electable, there is not a limited select region like on
the structures. Click-drag the pointer control to the upper left-hand region of the Window to a position similar
to the one shown in Fig. 51. This control will determine the length of the Fibonacci sequence that your instru-
ment calculates.
Return to the CONTROLS menu and again choose the NUMERIC option. This time select the box labelled
Io   INDICATOR ONL_ and choose the DIGITAL CONTROL glyph (the right-most control glyph), then click in the
OK box. The front panel window should now display a rectangle within a rectangle with the interior rectangle
containing a O. This control is a passive indicator. It will be used to display intermediate was well as final values
ofour Fibonacci instrument. We need a second indicator display. Controls can be duplicated in the same man-
ner as objects in the block diagram window. Use the grabber tool. First select the control to be duplicated, de-
15   press the OPTION key and click-drag a copy off of the original. Arrange these two indicators to the left of the
front panel window similar to the positions shown in Fig. 52.
We need one more front panel control to complete or instrument, a graphical display. Return to the CON-
TROLS menu one more time and choose the GRAPHICA1 option. This option will produce a dialog box like
the one shown in Fig. 29. Choose INDICATOR ONL_. Do this and click the OK box to return to the front panel
2o   window. There should now be a rectangular-shaped graph with linear coordinates setting in the middle of the
window. This is the graphical display control. It can be moved just like the objects in the block diagram window
and the other controls on the front panel. It can also be resized, as can the other controls. The procedure for
resizing is identical to the procedure used to resize objects in the block diagram. select the control and then
click-drag with the grabber tool from the lower right-hand corner of the selected area. The situation with the
25   NUMERIC controls is the same but because the fullness ofthe select region the area to ''grab'' with the grabber
is less obvious, but it is the same region just interior to the lower right-hand corner of the select area. It may
take a little practice to be able to reliably resize these controls. Move the graphical display to the upper right-
hand corner of the window. Your screen should now look like the one shown in Fig. 53. At this point we can
return to the blockdiagram window to ''wire-up'' the components ofour block diagram into a working instrument
3o   that will be controlled by the front panel and display its output there as well.
You can return to the block diagram window either by clicking on the exposed edge of that window or by
choosing OPEN DIAGRAM from the F11 E menu. The first think you will notice is that your block diagram has
changed. There are now four new objects in the block diagram window that were not there when we left. These
block diagram representations of the front panel controls are called ''terminals''. These objects can be moved
35   about the block diagram just like the original block diagram elements. Notice that with the exception of the
constants, each of these elements bears a resemblance to the control laid down on the front panel. The con-
stants are distinguished by their numerical contents representing the values they hold. When a control is ''se-
Iected on the front panel, its terminal equivalent on the block diagram is ''high-lighted'', and vice-versa. Before
we begin wiring up the block diagram we need to move the control terminals into closer proximity to their points
40   of action. Using the grabber tool, click-drag each of the control terminals into positions similar to those shown
in Fig. 54. We are now ready to begin to wire the elements into a working instrument. Note. If your original
positioning of the block diagram objects differed significantly from our example, now is the time to rearrange
your block diagram so that it more closely resembles Fig. 54. Eventually it should be relative easy to move
about pieces of a wired diagram, but at present this is not possible.
45     To connect component in the block diagram you must use the wiring tool. This tool is represented in the
tool palette in the upper left-hand corner of the block diagram window by the wire-roll glyph. The hot spot on
this took is the end of the unrolled stretch of wire. Move the cursor to the tool palette and click to select the
connector, when you move the cursor back into the active-window area your cursor will now be the connector
tool. Connections are established between block diagram components by clicking in a hot spot of one element
50   and pulling the flickering line to a hot spot of component to which you want to connect. Figure 55 shows the
connection between the slide-pointer control used to set number of iterations in the loop and the iteration-va-
riable box in the loop (labelled N). The ''wire'' is represented by a flickering line during the connecting operation.
Clicking in a hot spot of the terminal component will change the wire to a non-flickering representation of the
connection. The interior ofobjects, orobject elements, represented by heavy black borders are ''hot'' anywhere
55   within their borders. Other objects, for example, the LINEAR FIT object, have localized hot spots representing
specific inputs and outputs. Figure 56 shows the arrangement of hot spots for all available functions.
Connecting wires can be ''forced'' to turn corners where the user wants them to for purposes of neatness
and legibility ofthe diagram. Asingle clickwhile dragging a wire outside ofa structure or otherobject, will allow
_l
