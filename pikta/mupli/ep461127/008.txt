                        1 3             EP O _6_ _21 B_             14
may adjust the recording level using the left arrow and      key to control system 50 to compare the text typed in
right arrow keys). Upon depressing the space bar,      by the student with model text and indicate any errors
speech processor 60 begins converting audio applied      in the student-generated text.
at its audio input (e.g., from the microphone in head-        FIGURE 4 is a graphical illustration ofthe options
set 62a) into digitized speech information and storing   _   available to the student in the AudioWrite function
the digitized speech (on a virtual disk). When the stu-      (FIGURE 2, block 114). Upon initiating the AudioWrite
dent is through speaking, he depresses the space bar      function, the student may depress the F2 key to listen
again to stop recording. The student may then de-      to the entire text, or simply depress the ENTER key
press the F4 key to instantly play back his own just-      to start the exercise without listening to the whole text.
recorded speech -- or depress the F2 key to listen   1o   Once the exercise has begun, system 50 controls
again to the model pronunciation of the selected      speech processor 60 to produce the audio corre-
phrase. Depressing the ESC key returns system 50      sponding to the first phrase ofan exercise without dis-
to the PREVIEW mode (FIGURE 2, block 1 08), while      playing the corresponding text on display 58. The stu-
depressing the F5 key controls system 52 to perform      dent may depress the F3 key to control speech proc-
the Audio Lab CLIP mode (FIGURE 2, block 11 2).     1_   essor 60 to replay the spoken phrase (the phrase may
The CLIP mode in the preferred embodiment al-      be replayed as many times as the student wishes).
Iows the student to analyze any section or part of a      The student then enters text by depressing the keys
phrase selected in the LAB mode. Agraphical illustra-      on keyboard 54 (in the preferred embodiment, system
tion of options presented to the student in the CLIP      50 adds spaces, punctuation and capitalization so the
mode are shown in FIGURE 3C. In the preferred em-   2o   student may concentrate on spelling and grammar).
bodiment, the CLIP mode permits the student to ex-      The student may depress the ENTER key at any time
amine any section or part of the model digitized      to check his progress. Upon depressing the ENTER
speech recording down to a single phoneme (O.1 sec-      key, system 50 compares the text keyed in by the stu-
onds in duration). In addition, the complete phrase      dent with the text version of the phrase being spoken
can be heard by depressing a control key (e.g., F2).   2_   by speech processor60 -- and highlights any portions
In the CLIP mode, the cursor control keys (up ar-      of the student-typed text which do not correspond to
row, down arrow, left arrow, right arrow) are used to      the model text. The student may use his cursor con-
select which part ofthe current phrase is to be played      trol keys to move the cursor to the erroneous charac-
back. A graphical illustration of the length of the cur-      ters and correct his mistakes by retyping over the in-
rently selected portion of the phrase is displayed at   3o   correct characters already there. The student may
the bottom ofdisplay 58 in the preferred embodiment.      depress the F9 key at any time to control system 50
In the preferred embodiment, this graphical illustra-      to display the correct word corresponding to the stu-
tion includes a horizontal line having a length propor-      dent's inputted word the cursor points to. The dis-
tional to the length ofthe selected phrase portion. The      played model word disappears when any other key is
Iength and position ofthis horizontal line change in re-   3_   depressed. When the student has correctly entered a
sponse to cursor controls to change the length and      phrase, he depresses the enter key to hear the next
position of the selected portion relative to the current      phrase. When the student finishes the exercise, he
phrase.                                may depress the F2 key to listen to the entire text, or
Once the student has selected the portion of the      depress the ESC key to return to the main menu (FIG-
phrase he wishes to concentrate on, he depresses   4o   URE 2, block 1 06).
the F5 key to listen to the selected portion. As in the        The Sound Sort function provided by the prefer-
LAB mode, the student may record and play back his      red embodiment ofthe present invention presents the
own voice using the F3 and F4 keys, and alternate      the student with an audio puzzle to solve. The ''pieces
playback of his voice with playback of the selected      of the puzzle'' are the phrases in the model text -- but
clip by toggling the F5 and F4 keys. Depressing the   4_   jumbled in a randomized order. The student must put
F8 key resets the clip to allow the student to select a      the phrases back in the correct order based only on
different part of the phrase. Depressing the ESC key      aural clues. In the preferred embodiment, each
returns system 50 to the LAB mode (FIGURE 2, block      phrase is identified by a symbol (e.g., the letters A,
1 1 O).                                 B, C) displayed on display 58. The student controls
The AudioWrite function (FIGURE 2, block 114)   _o   system 50 to move the letters from a ''_umbled'' or-
provides the student with an exercise in listening and      dered list displayed on the left-hand column ofdisplay
writing by requiring the student to type phrases he      58 to a correctly ordered list in the right-hand display
hears. The student may listen to each phrase as many      column based on context of the associated phrases.
times as he wishes, and may also listen to the entire      Thus, system 50's Sound Sort function associates a
text before concentrating on each phrase (since in the   __   randomly ordered sequence of phrases with dis-
preferred embodiment the typing exercise operates      played symbols, and then requires the student to re-
on a phrase-by-phrase basis). Once the student has      order the displayed symbols to correspond to the cor-
typed a particular phrase, he can depress the ENTER      rect order of the phrase sequence. The student may
8
