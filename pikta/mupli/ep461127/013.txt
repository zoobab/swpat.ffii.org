                        23             EP O _6_ _21 B_             24
tem 50 then displays on display screen 58 the sym-      ample, ratherthan the entire sequence (which may be
bols corresponding to the reordered phrase sequence      of arbitrary length).
in the left-hand column ofthe display (see FIGURE 5)        ln the preferred embodiment, the left arrow and
so that the phrases, if heard in the order of the sym-      right arrow cursor control keys only have the effect of
bols displayed on the display, would be in the random-   _   changing the phrase sequence playback beginning
ized order (block 460). System 50 then waits for the      point after the F2 key has been depressed. Other-
student to depress a key to select the next function      wise, they control movement ofthe displayed symbols
to be performed (blocks 462, 464).                on display screen 58. Depressing the right arrow key
The Sound Sort function 1 78 in the preferred em-      causes system 50 to first determine whetherthe curson
bodiment provides a help screen giving the student   1o   is in the left column or the center column (decision
instructions for what to do next if he gets confused      blocks 498, 502, respectively). The objective in the
(blocks 472-476). The student may exit the Sound-      SoundSortfunction is to move the symbols displayed in
Sort function 1 78 at any time by depressing the ESC      the left column furtherto a center column -- and then to
key (decode block 464). If the student confirms he      move those symbols into a right-hand column in the cor-
wishes to leave the SoundSort function, a return to   1_   rect order based upon aural clues. Ifthe cursor is in the
main routine block 1 70, FIGURE 6A is performed      left column (and thus is pointing to a symbol displayed
(blocks 468, 470). On the other hand, if the student      in the left column), and the user depresses the right
does not confirm he wishes to leave the Sound Sort      arrow key, the symbol pointed to by the cursor is re-
function, he is returned back to the get key block 462      moved from the left column and displayed in the cen-
to select the next function (decision block 468).      2o   ter column (block 500), using conventional screen
If the student depresses the F3 key in the prefer-      control techniques. Similarly, if the cursor is pointing
red embodiment, system 50 plays backthe phrase as-      to a symbol in the middle column and the user de-
sociated with the symbol the cursor is presently point-      presses the right arrow key, the symbol is moved to
ing to (block 478). Upon depressing the F2 key, sys-      a right column position so long as there isn't already
tem 50 displays on display 58 a promptwhich prompts   2_   a symbol displayed immediately to the right in the
the user for ''starting point?'' (block 480), and then      right column (decision block 502, 506). Striking the
waits for the user to input another selection (blocks      left arrow key permits the student to move a symbol
482, 484). By striking the F2 key, the student may      in the right column back to the middle column or from
playback the entire sequence of phrases in their cor-      the middle column back to the left column (blocks
rect order -- or can select a portion ofthe correctly or-   3o   51 O-520).
dered sequence of phrases to hear the audio corre-        ln  the  preferred  embodiment,  the  student
sponding to. After the depressing the F2 key, if the      changes the order of symbols by moving them to the
student again depresses F2 (or depresses the EN-      centercolumn and then moving them vertically before
TER key), system 50 plays back a phrase sequence      placing them into ''slots'' in the right column (these
beginning at a portion of the sequence pointed to by   3_   slots correspond to entries in an array maintained in
a pointer called a ''start point'' which is initially set at      memory). Upon depressing the up arrow key, for ex-
the beginning of the correctly ordered phrase se-      ample, system 50 first determines whether the cursor
quence (but may be changed by the student) (block      is pointing to a symbol in the center column (decision
494). Once the phrase sequence playback has be-      block 522). If so, the pointed to symbol is moved up
gun, it will continue to the end of the sequence of   4o   one row in the center column (thus, the symbol is al-
phrases or until the student again hits the F2 (deci-      ready in the top row in which case it is wrapped
sion 496). If, instead of striking the F2 key or the EN-      around to the bottom) (blocks 524-528). If the cursor
TER key, the student depresses the left arrow or right      points to a symbol in the left-hand or right-hand col-
arrow keys in the preferred embodiment, the effect      umn, on the other hand, the cursor is moved up one
will be to change the value ofstart point. In particular,   4_   row (block 530) and then system 50 determines
if the student depresses the right arrow key, the start      whether the new cursor position is on the letter in the
point pointer value is advanced in the phrase se-      left or right column (decision block 532). If the cursor
quence and its new value is displayed (blocks 486,      does not point to a letter in its new position, it is either
488). Similarly, by depressing the left arrow key, the      moved up or wrapped around (decision block 534,
start point pointervalue is retracted toward the begin-   _o   536). Similar symbol movement occurs upon de-
ning of the phrase sequence (blocks 490, 492). This      pressing the down arrow key (blocks 540-560).
allows the student to concentrate on the last portion         Depressing the space bar in the preferred em-
of the correctly ordered phrase sequence, for exam-      bodiment controls the cursor to move between left
ple, (or on any portion of the phrase sequence since      and right columns. For example, if there are symbols
he can strike the F2 key to discontinue phrase se-   __   displayed in both the left column and the right column
quence playback) and is especially useful for long      and the cursor is presently in the centercolumn, strik-
phrase sequences since it permits the student to lis-      ing the space bar will do nothing. Space only has an
ten to three or four phrases in the sequence, for ex-      effect if the cursor is in either the left or right column.
_3
