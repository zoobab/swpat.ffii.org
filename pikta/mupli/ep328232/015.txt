                                      EP O 328 232 B_
_3. Amethod according to Claim 1, wherein said identifying step includes the step ofspecifying in digital fields
in said authorizing certificate a hierarchy of certificates, whereby a recipient of the message can elec-
tronically verify in accordance with a predetermined validation algorithm the authority ofthe signer based
upon an analysis of the signed message.
5   Patentanspr�che
_.  Verfahren zur Handhabung der Zugriffsberechtigung durch digitale Bezeichnung und Zulassung einer di-
Io     gitalen Nachricht, die an einen unabh�ngigen Empf�nger �bermittelt werden soll, bei einem Kommuni-
kationssystem mit einer Vielzahl von Datenstationen (Datenstationen A bis N), die mit einem Kanal (12)
verbunden sind, �ber den Benutzer der Datenstationen Nachrichten austauschen k�nnen, wobei wenig-
stens einige der Benutzer eine �ffentliche Verschl�sselung (30) und eine zugeordnete private Verschl�s-
selung (32) besitzen, weist folgende Schritte auf.
15     Erzeugung wenigstens eines Bereichs der digitalen Nachricht (20);
digitale Bezeichnung wenigstens dieses Bereiches der Nachricht (40); und gekennzeichnet durch
Verkn�pfung der Nachricht mit einer berechtigenden digitalen Zulassung (28, 11 6) mit einer Vielzahl von
durch eine Zulassungseinheit erzeugten digitalen Felder, wobei die berechtigende Zulassung durch fol-
gende Schritte erzeugt wird.
2o     Festsetzung der Zugriffsberechtigung, die durch die Zulassungseinheit verliehen wurde und die im Be-
zeichner der Nachricht angeordnet worden ist, durch die Zulassungseinheit in wenigstens einem der di-
gitalen Felder, durch Einbinden geeigneter digitaler lnformation, um dem unabh�ngigen Empf�nger der
Nachricht zu erm�glichen durch elektronische Analyse der Nachricht gem�_ eines vorbestimmten Be-
rechtigungsalgorithmus zu pr�fen, da_ die Zugriffsberechtigung, die durch den Bezeichner beim Bezeich-
25     nen des lnhalts der durch den Bezeichner erzeugten Nachricht, gem�_ der durch die Zulassungseinheit
angeordneten Zugriffsberechtigung passend durch den Bezeichner vergeben wurde; und
Identifizierung der Zulassungseinheit, der die Zulassung des Bezeichners in anderen digitalen Feldern
durch Einbinden geeigneter digitaler lnformation f�r den Empf�nger der Nachricht erzeugt hat, um durch
elektronische Analyse der Nachricht, die die Zulassungseinheit erteilt hat, die Zugriffsberechtigung zu be-
3o     stimmen, um die angeordnete Zugriffsberechtigung zu erteilen.
2.  Ein Verfahren nach Anspruch 1, welches au_erdem den Schritt aufweist, wenigstens ein Feld in der Nach-
richt vorzusehen, um die Art der �bermittelten digitalen Daten zu identifizieren.
35   3.  Verfahren nach Anspruch 1, dadurch gekennzeichnet, da_ der Formulierungsschritt einen Schritt auf-
weist, der ein Feld vorsieht, das dem Benutzer das Einf�gen eines vorbestimmen Kommentan (26) be-
z�glich der �bermittelten Daten gestattet.
_.  Verfahren nach Anspruch 1, welches zus�tzlich den Schritt aufweist, eine Kontrollfunktion (34) auf we-
go     nigstens einen Bereich der zu �bermittelnden Nachricht anzuwenden, um eine vorbezeichnete Kontrolle
(36) zu bilden; und wobei der digitale Bezeichnungsschritt einen Schritt zur Erzeugung der vorbezeich-
neten Kontrolle mit der privaten Verschl�sselung (32) des Bezeichners aufweist, um die digitale Kennung
zu bilden.
5.  Verfahren nach Anspruch 4, welches zus�tzlich den Schritt zur Zusammenstellung eines digitalen Ken-
''     nungspakets (42) aurweist, dereine digitale Kennung und eine Repra__sentation wenigstens eines Bereichs
der zu �bermittelnden Nachricht enth�lt.
6.  Verfahren nach Anspruch 1, dadurch gekennzeichnet, da_ die Zugriffsberechtigungs-Zulassung (11 6) di-
gitale Felderaufweist, die Zusatzbezeichnungserfordernisse, die zur Kennung des Bezeichners geh�ren,
50     definieren, so da_ die Kennung des Bezeichners so behandelt werden kann, als ob sie geeignet zugriffs-
berechtigt ist.
l.  Verfahren nach Anspruch 6, dadurch gekennzeichnet, da_ die digitalen Felder, die die Zusatzkennungs-
erfordernisse definieren, eine erforderliche digitale Kennung durch eine spezifische dritte Partei bekannt
55      machen, die das Zulassen der Bezeichnerkennung (116) anzeigen, um dadurch eine Z�hleranforderungs-
kennung zu definieren.
8.  Verfahren nach Anspruch 7, dadurch gekennzeichnet, da_ die dritte Partei (86) durch digitales Bezeich-
_5
