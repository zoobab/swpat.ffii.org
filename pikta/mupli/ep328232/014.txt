                                         EP O 328 232 B_
Claims
_.  In a communication system having a plurality of terminal devices (terminals A to N) coupled to a channel
(12) over which users ofsaid terminal devices may exchange messages, at least some of said users hav-
5      ing a public key (30) and an associated private key (32), a method for managing authority by digitally sign-
ing and certifying a digital message to be transmitted to an independent recipient comprising the steps
of.   generating at least a portion of said digital message (20);
digitally signing at least said portion of said message (40); and characterised by
Io          associating with said message an authorizing digital certificate (28,1 16) having a plurality of digital
fields created by a certifier, said authorizing certificate being created by the steps of.
specifying by the certifier in at least one of said digital fields, the authority which is vested in the
certifier and which has been delegated to the signer of said message, by including suffcient digital in-
formation to enable said independent recipient of said message to verify, by electronically analyzing said
15      message in accordance with a predetermined validation algorithm, that the authority exercised by the
signer in signing the content of said message created by the signer was properly exercised by the signer
in accordance with the authority delegated by the certifier; and
identifying the certifier who has created the signer's certificate in other of said digital fields by in-
cluding sufficient digital information for said recipient of the message to determine by electronically ana-
2o      lyzing said message that the certifier has been granted the authority to grant said delegated authority.
2.  A method according to Claim 1, further including the step of providing at least one field in said message
identifying the nature of the digital data being transmitted.
25   3.  A method according to Claim 1, wherein the formulating step includes the step of providing a field allowing
the user to insert a predetermined comment (26) regarding the date being transmitted.
_.  A method according to Claim 1, further including the step of applying a hashing function (34) to at least
a portion of the message to be transmitted to form a presignature hash (36); and wherein the digitally
3o      signing step includes the step of processing said presignature hash with the signer's private key (32) to
form said digital signature.
5.  A method according to Claim 4, further including the step of forming a digital signature packet (42) com-
prising the digital signature and a representation of said at least a portion of the message to be transmit-
ted.
35   6.  A method according to Claim 1, wherein said authorizing certificate (11 6) includes digital fields defining
the cosignature requirements which must accompany the signer's signature in order for the signer's sig-
nature to be treated as properly authorized.
4o   l.  A method according to Claim 6, wherein said digital fields defining co-signature requirements set forth a
required digital signature by a specified third party indicating approval of the signer's signature (11 6) to
thereby define a counter signature requirement.
8.  A method according to Claim 7, wherein the third party countersigns (86) by digitally signing the signer's
g5      digital Signatufe.
9.  A method according to Claim 6, wherein the cosignature requirements include a digital field specifying
at least one other digital signature which is required to appear in the digital message thereby defining a
joint signature requirement (1 16).
50   _ O.  A meth Od aCCOfding tO Claim 1, Whefein Said aUth OfiZing Ceftifi Cate inCIUdeS at leaSt One digital field de-
fining limitations as to the authority granted by the certificate (1 16).
__.  A method according to Claim 1 O, further including the step of specifying a monetary limit for the signer
in a digital field in said certificate (1 16).
55   _2.  A method according to Claim 1, wherein said authorizing certificate (116) includes at least one digital field
defining a trust level indicative of the degree of responsibility delegated to the signer by the certifier.
__
