                                         EP O 328 232 B_
indicates that the users of registered public keys are properly associated with General Motors. For example,
General Motors may decide to have three ''high level'' keys certified, e.g., corporate officers, such as the vice
president, financial officer, and the security officer. At General Motors' request each of the three certificates
indicate the public keys of the other two as required joint signatures.
5      Thus, once having obtained the highest level certificate(s) from the meta-certifier, several officials within
General Motors may have to jointly sign certificates at the next lower level and such joint signatures. Each of
these high level General Motors' certificates would mutually reference each other as required co-signers Athis
Ievel no single corporate officer acting alone may authorize anything because embedded within each of the
three certificates is a requirement forthe signature ofothers who are specifically identified. In turn then, these
Io   3 officers create and sign public keys for the other General Motors' employees, that define exactly the level
of authority, responsibility and limitations each employee is to have. One of these certificates may belong to
user A, or will be an antecedent to user's A's certificate.
Each of these three high level certificates may digitally sign terminal B user's certificate preferably after
a face to face ortelephone verification. Aftereach ofthe required signatures has been created, the certificate's
15   signatures by the vice president, financial officer and security officer as well as their respective 3 certificates,
as well as those certificates' respective signatures by the meta-certifier are ultimately returned to the General
Motors' supervisor at terminal B to be stored for ongoing use, such as in our example for subcertifying terminal
user A. In this manner, the signed message unequivocally contains the ladder or hierarchy of certificates and
signatures verifying terminal A user's identify and his authority.
2o     When a party B in a ladder of certifications creates an authorizing certificate for party A, the certificate
includes a specification of A's identity together with A's public encryption key. Additionally, the certificate in-
dicates the authority, capabilities and limitations which B wishes to grant A. By granting this certificate B ex-
plicitly assumes responsibility for both A's identity and authority.
B's certificate for A also permits a specification of other parties who are required to cosign actions taken
25   by A when using this certificate as will be explained further below. Cosignatures may take the form of either
joint signatures orcountersignatures. Additionally party B can define in the certificate forAthe degree towhich
B will recognize subcertifications performed by A.
In accordance with an exemplary embodiment of the present invention, trust levels which are granted by
the certifier to the certifiee are specified in the certificate by a predetermined digital code. Such a trust level
3o   is used by the recipient ofthe message as an indicator of the authority granted to the certifiee and the respon-
sibility assumed by the certifier for the certifiee's actions with respect to the use of the public key being cer-
tified.
By way of example only trust levels may be indicated by trust level values O, 1, 2, and 3.
Trust level O indicates that the certifier vouches that the certified public key belongs to the individual
35   named in the certificate; but that the certifierwill not assume responsibility for the accuracy ofany certificates
produced by the certifiee. The essence of this would be a statement by the certifier that. ''l warrant the user
named in this certificate is known to me and is being certified to use the associated public key -- however l
do not trust him to make any further indentifications on my behaIF'.
Trust level 1 empowers the certifiee to make level O certifications on behalf of the certifier. The essence
40   of this would be a statement by the certifier that. ''l know the user of this public key and l trust himlher to ac-
curately identify other persons on my behalf. l will take responsibility for such identifications. However, l do
not trust this person to identify persons as trustworthy.''
Trust level 2 empowers the certifiee to make level O, 1 and 2 certifications on behalf of the certifier. The
essence ofthis would be a statement by the certifier that. ''l know the user ofthis public key and l trust himlher
45   to accurately identify other persons on my behalf, and l furthermore trust them to delegate this authority as
they see fit. l assume due responsibility for any certifications done by them or any duly authorized agent cre-
ated by them or by other generation of duly created agents''.
Trust level 3 is reserved exclusively for an ultimate meta certifier whose public key and certificate is es-
tablished and also well known (possibly by repetitive and widespread media publication) and whose accuracy
50   is universally respected. This certifier takes responsibility only for accurately identifying the entities whose
public keys it certifies. It assumes no responsibility for the use of these keys.
Additionally, each certification may specify the monetary limit, i.e., the maximum amount of money value
which the certifiee is authorized to deal with. The monetary limit must not of course exceed the limit in the
certifier's own certificate to insure that the certifier does not delegate more than he is allowed to handle.
55      Before discussing further details of the digital signature and certification techniques of the present inven-
tion, it may be helpful to first define certain terminology. As noted above, the term ''object'' is generically used
to describe any aggregation ofdata that can be ultimately represented in a manner suitable for processing with
whatever public key method is being utilized for signatures andlor encryption. The term object may apply to
l
