                           7             EP O 529 91 5 B1             8
ities, numbers of input telephone lines, etc. are input as     application is primed, eKtremely rapid responses to user
initialization constants. Then, for each application (BoK     inquiries are provided.
54), log-in and log-out sequences are defined, the       When all usertransactions are finished, the channel
number of virtual terminal sessions are specified so as     process submodule 31 sends a termination indication to
to respond to anticipated user calls; and a refresh pro-  5  the assigned eKecutor submodule 35 which then de-
cedure is established.                        taches from the session(s) and informs eKecutive con-
The refresh procedure provides interface processor     trol module 32 of its termination of function. At this point,
1 O with the capability to maintain a virtual terminal ses-     eKecutive control module 32 regains control of the ses-
sion in operation and the associated application in a     sion(s) just released and updates session control table
primed state. Without refresh, most applications will au-  IO  36 to indicate that the sessions are available.
tomatically terminate a session if no activity has oc-
curred within a predetermined time. To prevent such a
session termination, the refresh procedure maintains a     Claims
time count and will, prior to time-out, generate a screen
to the application which will reset its time-out and enable  15  1.  A voice processing method for operating an inter-
a continuance of the virtual session.                  face processor to provide access to applications
At the termination of the preparationltraining inter-       running on a plurality of host processors, each said
val, all of the inputted configuration data is saved in the       host processor communicating with said interface
database (BoK 54) and the system is then prepared for       processor by providing screens to said interface
operation. At initiation time (BoK 56), eKecutive control  20    processor, comprising the steps of.
module 32 causes virtual terminal sessions module 38
to commence log-in procedures for all sessions defined          (a) establishing at least one virtual terminal
in the database. Session control table 36 is updated with          session with each said host processor and ini-
the current status of all sessions. At this point, the sys-          tializing said applications running on said host
tem is prepared for user inquiries.               25       processor to ready them for further commands;
Until a user inquiry is received (BoK 58), controller
32 performs the refresh procedure to maintain the virtual          (b) responding to a user request that requires
sessions in effect and connected application in then          a process to be performed by an application, by
primed states. In addition, all free virtual sessions are          accessing a virtual terminal session with a host
allocated for use by script eKecutor module 34. As  30       processor on which said application is running,
shown in BoK 60 VPU control module 30 then receives          entering user data in a stored screen from said
a user inquiry, assigns it to a channel process submod-          application and transmitting said data to said
ule 31, which in turn, requests eKecutive control module          host processor, whereby said host processor
32 to assign a virtual session or sessions for communi-          immediately processes said data; and
cating with hosts containing the required application(s)  35
(BoK 62).                                     (c) receiving a screen from said host processor
EKecutive control module 32 then allocates an eK-          containing data for said user, and transmitting
ecutor submodule 35 to communicate with channel          said data to said user, whereby said interface
processor submodule 31 (BoK 64). The assigned eKec-          processor enables a plurality of host proces-
utor submodule 35 obtains assignment of a virtual ses-  40       sors to rapidly respond to user-supplied data
sion and proceeds to communicate with the connected          without requiring modification of host proces-
host processorlapplication by sending keystrokes to the          sor-controlling software.
host processor. The application in the host immediately
responds to the entered data and provides a response     2.  A method as claimed in Claim 1 wherein step (b)
screen. Upon receiving the screen back from the host  45     includes the further steps of. creating a table of host
processor, eKecutor submodule 35 reads the data from       applicationlvirtual terminal sessions; updating said
the screen by finding the requisite field and eKtracting       table to indicate available host applicationlvirtual
the data. It then returns that data to channel process       terminal sessions; and choosing from said table, an
submodule 31 in VPU control module 30, where it is       available host applicationlvirtual terminal session in
played out by voice to the user (BoK 66).           50     response to said user data, whereby said user data
It is to be understood that if the user provides a plu-       may be directed to an available host application vir-
rality of inputs that require plural applications, an as-       tual terminal session that is not already busy.
signed eKecutor submodule 34 is allocated a plurality of
virtual sessions, each session interacting with a host     3.  A method as claimed in Claim 1 or Claim 2, respon-
containing the required application. As a result, a single  55    sive to the provision by said user in step (b) of a
telephone inquiry can result in a plurality of applications       plurality of input data that require a plurality of ap-
providing information back to the user, thereby negating       plications, including the further steps of. accessing
the need for multiple calls. Furthermore, since each host       a plurality of virtual terminal sessions, one for each
5
