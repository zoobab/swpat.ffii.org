Mne: Sprachfernsteuerung, Sprachgesteuertes Internet-Surfen
descr: Dieses Patent deckt im engeren Sinne jegliche Fernsteuerung eines Rechners (z.B. Server) über den Rechner des Anwenders ab (z.B. Internet-Surfen ohne Handbewegung, e-Shopping von der intelligenten Küche aus, diktieren eines Briefes z.B. über das X-Windows-System, ... ). Im weiteren Sinne ist Sprach-Steuerung per se patentiert z.B. ein sprachgesteuertes Tamagotchi, oder ein Sprachgesteuerter Gameboy ... . Vorraussetzung ist lediglich, dass Benutzeroberflächenlogik und Anwendungslogik voneinander getrennt sind, was fast immer der Fall ist.
Nie: Neues
Aci: Archiv
Nzi: Netzspiegel
Dak: Druckbare Dokumente
Ptn: Patente
Tbl: Tabellen
Bse: Beispiele
abi: automatisierbare medizinische Diagnose
Ssc: Steuerung eines Rechners durch einen anderen
Vea: Abfangen von Viren
dhf: dynamische Preisfestlegung
dtu: digitale Unterschrift mit zusätzlichen Authentifizierungsinfos
Cdr27: Computer system and method for performing multiple tasks
AWt: Umwandlung von Dateinamen
Mne: Sprachfernsteuerung, Sprachgesteuertes Internet-Surfen
Dse: Dynamisch erweiterbarer Webserver
VRl: Vorab-Rückmeldungen
iNo: intuitive Netzwerk-Konfiguration
Ean: E-Geschäftsanbahnung an der Börse
DKc: Dateisystem für Flash-Speicherbausteine
Esk: Elektronischer Einkaufswagen
DWP: Dateneinsparung bei mobiler TCP-Kommunikation
nna: Übersetzung von natürlichsprachlichen in formalsprachliche Datenbankanfragen mithilfe von Parsern und Tabellen
VeW: Visualisierung von Prozessen
Uuh: Unterscheidung zwischen benutzten und unbenutzten Blöcken bei der Behandlung von Cache-Zuweisungsfehlern
cui27: compression requests from a client to a server
spi27: select cooking recipes to generate a list of items to buy
Sle: Sprachlernen durch Vergleich eigener Aussprache mit der eines Lehrers
PWW: Prüfen von Lernstoff in Schulen
Met: Miteingabe von Kontextdaten
Hda: Hybride Klassen
VWA: Vererbung von CORBA Objekten
Shd: Schäden
geo: Geheimdoku
Gea: Gesammelte Papiere
Nzr: Netzforen
Air: Arbeitsgruppe
MdW: Förderer
Tgn: Tagungen
Kus: Debatte
Beu: Briefe
PiB: Priorität
claim1: A voice processing method for operating an interface processor to provide access to applications running on a plurality of host processors, each said host processor communicating with said interface processor by providing screens to said interface processor, comprising the steps of: (a) establishing at least one virtual terminal session with each said host processor and initializing said applications running on said host processor to ready them for further commands; (b) responding to a user request that requires a process to be performed by an application, by accessing a virtual terminal session with a host processor on which said application is running, entering user data in a stored screen from said application and transmitting said data to said host processor, whereby said host processor immediately processes said data; and (c) receiving a screen from said host processor containing data for said user, and transmitting said data to said user, whereby said interface processor enables a plurality of host processors to rapidly respond to user-supplied data without requiring modification of host processor-controlling software.

# Local Variables:
# mailto: mlhtimport@a2e.de
# login: xuan
# passwd: XXXX
# srcfile: /usr/share/emacs/20.7/site-lisp/mlht/el2x.el
# feature: swpatdir
# doc: ep529915
# txtlang: de
# coding: utf-8
# End:
