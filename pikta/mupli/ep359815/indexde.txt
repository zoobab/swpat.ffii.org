Uuh: Unterscheidung zwischen benutzten und unbenutzten Blöcken bei der Behandlung von Cache-Zuweisungsfehlern
descr: Jeder Speicherblock enthält ein Gültigkeits-Bit.  Ist dieses bei einem fehlerhaften Speicherblock ausgeschaltet, so wird sofort eine ganze uebergeordnete Gruppe aus meist 4 Bloecken beschrieben.  Ist es eingeschaltet, so wird nur eine geringer Zahl von Blöcken (meist 1 Block) neu geschrieben, da man dann davon ausgeht, dass die Vierergruppe richtig belegt ist und nur an der einen Stelle ein Fehler aufgetreten ist.  D.h. dieses Patent beansprucht den gesamten möglichen Nutzen des Gültigkeits-Bits.  Das EPA hat hunderte von Patenten auf Speicherverwaltungsarithmetik vergeben.  Es fragt sich, wer außer IBM und Sun heute noch einen Speicher programmieren darf.
Nie: Neues
Aci: Archiv
Nzi: Netzspiegel
Dak: Druckbare Dokumente
Ptn: Patente
Tbl: Tabellen
Bse: Beispiele
abi: automatisierbare medizinische Diagnose
Ssc: Steuerung eines Rechners durch einen anderen
Vea: Abfangen von Viren
dhf: dynamische Preisfestlegung
dtu: digitale Unterschrift mit zusätzlichen Authentifizierungsinfos
Cdr47: Computer system and method for performing multiple tasks
AWt: Umwandlung von Dateinamen
Mne: Sprachfernsteuerung, Sprachgesteuertes Internet-Surfen
Dse: Dynamisch erweiterbarer Webserver
VRl: Vorab-Rückmeldungen
iNo: intuitive Netzwerk-Konfiguration
Ean: E-Geschäftsanbahnung an der Börse
DKc: Dateisystem für Flash-Speicherbausteine
Esk: Elektronischer Einkaufswagen
DWP: Dateneinsparung bei mobiler TCP-Kommunikation
nna: Übersetzung von natürlichsprachlichen in formalsprachliche Datenbankanfragen mithilfe von Parsern und Tabellen
VeW: Visualisierung von Prozessen
Uuh: Unterscheidung zwischen benutzten und unbenutzten Blöcken bei der Behandlung von Cache-Zuweisungsfehlern
cui47: compression requests from a client to a server
spi47: select cooking recipes to generate a list of items to buy
Sle: Sprachlernen durch Vergleich eigener Aussprache mit der eines Lehrers
PWW: Prüfen von Lernstoff in Schulen
Met: Miteingabe von Kontextdaten
Hda: Hybride Klassen
VWA: Vererbung von CORBA Objekten
Shd: Schäden
geo: Geheimdoku
Gea: Gesammelte Papiere
Nzr: Netzforen
Air: Arbeitsgruppe
MdW: Förderer
Tgn: Tagungen
Kus: Debatte
Beu: Briefe
PiB: Priorität
claim1: A method of sending information to a cache in a computer, the method comprising the steps of: searching the cache for the requested information; generating a miss signal when the requested information is not found; examining a valid bit of a data block in said cache where requested information should be located, when said miss signal is generated; filling said cache with N data blocks during a specified time interval if the valid bit is not on, said N data blocks including a data block containing said requested information; and filling said cache with P data blocks at one time if the valid bit is on, where P is less than N, said P blocks including a data block containing said requested information.

# Local Variables:
# mailto: mlhtimport@a2e.de
# login: swpatgirzu
# passwd: XXXX
# srcfile: /usr/share/emacs/20.7/site-lisp/mlht/el2x.el
# feature: swpatdir
# doc: ep359815
# txtlang: de
# coding: utf-8
# End:
