                          1 3              EP O 359 8_5 B_              14
t�m�moire pr�vue dans un ordinateur, le proc�d�           (d) � remplir l'ant�m�moire de blocs de don-
comportant les �tapes consistant.                    n�es suivant un premier d�bit si le bit valide a
(a) � rechercher des informations demand�es           un �tat repr�sentatif d'une condition d'invali-
dans l'ant�m�moire;                          dit�, les blocs de donn�es comprenant un bloc
(b) � g�n�rer un signal d'absence si les infor-   _        de donn�es qui renferme les informations de-
mations demand�es sont absentes de l'ant�-           mand�es;
m�moire;                                 (e) � comparer la valeur de comptage des si-
(c) � d�terminer un �tat d'un bit valide d'un           gnaux d'absence � un premier nombre seuil
bloc de donn�es se trouvant dans l'ant�m�-           d'absences;
moire qui devrait renfermer les informations   1o       (_ � remplir l'ant�m�moire de blocs de don-
demand�es si un signal d'absence est g�n�r�           n�es suivant un premier d�bit si la valeur de
selon l'�tape (b), et que les informations de-           comptage de signaux d'absence d�passe un
mand�es sont absentes de l'ant�m�moire, le-           premier nombre pr�d�termin�;
dit bit valide ayant un �tat repr�sentatif d'une           (g) � introduire au moins un bloc de donn�es
condition d'invalidit� si aucune donn�e n'a �t�   1_       dans l'ant�m�moire suivant un second d�bit si
introduite dans ledit bloc depuis le dernier vi-           la valeur de comptage de signaux d'absence
dage;                                   est inf�rieure ou �gale au premier nombre
(d) � remplir l'ant�m�moire de blocs de don-           seuil et que le bit valide a un �tat qui est re-
n�es suivant un premier d�bit si le bit valide a           pr�sentatif d'une condition de validit�, le pre-
un �tat repr�sentatif d'une condition d'invali-   2o       mier d�bit �tant plus rapide que le second d�-
dit�, les blocs de donn�es comprenant un bloc           bit et ledit au moins un bloc de donn�es
de donn�es qui renferme les informations de-           comprenant les informations demand�es; et
mand�es;                                (h) � d�cr�menter la valeur de comptage de
(e) si le bit valide a un �tat repr�sentatif d'une           signaux d'absence chaque fois qu'une recher-
condition de validit�, � comparer un num�ro   2_       che d'un bloc de donn�es aboutit � la d�ter-
d'identification de processus du bloc de don-           mination d'une pr�sence, et � continuer �
n�es dans lequel les informations demand�es           remplir l'ant�m�moire de blocs de donn�es
devraient se trouver � un num�ro d'identifica-           suivant le premier d�bit jusqu'� ce que la va-
tion de processus d'un processus qui est train           leur de comptage de signaux d'absence soit
d'etre ex�cut� par l'ordinateur;            3o       inf�rieur � un second nombre pr�d�termin�.
(_ � remplir l'ant�m�moire de blocs de don-
n�es suivant le premier d�bit si les num�ros      _.  Proc�d� selon la revendication 1, comportant en
d'identification de processus sont autres que         outre l'�tape consistant � comparer un num�ro
Ies memes; et                            d'identification de processus du bloc de donn�es
(g) � remplir l'ant�m�moire d'au moins un bloc   3_     de l'ant�m�moire dans lequel les informations
de donn�es suivant un second d�bit si les nu-         demand�es devraient se trouver � un num�ro
m�ros d'identification de processus sont les         d'identification de processus d'un processus qui
memes, ledit au moins un bloc de donn�es         est en train d'etre ex�cut� par l'ordinateur, l'an-
comprenant  les  informations  demand�es,         t�m�moire �tant remplie suivant le second d�bit
dans lequel le premier d�bit est plus rapide   4o     si les deux num�ros d'identification de processus
que le second d�bit.                        sont les memes.
3.  Proc�d� de remplissage d'informations d'une an-      5.  Proc�d� selon les revendications 1, 2 ou 3, dans
t�m�moire pr�vue dans un ordinateur, le proc�d�         lequel le premier d�bit remplit l'ant�m�moire �
comportant les �tapes consistant.            4_     une cadence de 4 blocs de donn�es par p�riode
(a) rechercher un bloc de donn�es de l'ant�-         de temps pr�d�termin�e et le second d�bit rem-
m�moire qui devrait renfermer les informa-         plit l'ant�m�moire suivant une cadence de 1 bloc
tions demand�es et � g�n�rer un signal d'ab-         de donn�es par p�riode de temps pr�d�termin�e.
sence si les informations demand�es sont ab-
sentes de l'ant�m�moire;               _o   6.  Proc�d� selon les revendications 1, 2 ou 3,
(b) emmagasiner une valeurde comptage des         comportant en outre les �tapes consistant.
signaux d'absence g�n�r�s � l'�tape (a);                � emmagasiner un emplacement d'une
(c) � d�terminer un �tat d'un bit valide du bloc         absence lorsque la recherche dontfait l'objet l'an-
de donn�es qui a fait l'objet de la recherche,         t�m�moire aboutit � la g�n�ration du signal d'ab-
Iedit bit valide ayant un �tat repr�sentatif   __     sence;
d'une condition d'invalidit� si aucune donn�e             � comparer l'emplacement d'absence em-
n'a �t� introduite dans ce bloc depuis le der-         magasin� � un emplacement d'une absence se
nier vidage;                             produisant subs�quemment; et
8
