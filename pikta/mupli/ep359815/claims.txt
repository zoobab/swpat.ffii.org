
   Claims
   
   Claims: 1. A method of sending information to a cache in a
   computer, the method comprising the steps of:
   searching the cache for the requested information;
   generating a miss signal when the requested
   information is not found;
   examining a valid bit of a data block in said cache
   where requested information should be located, when
   said miss signal is generated;
   filling said cache with N data blocks during a
   specified time interval if the valid bit is not on,
   said N data blocks including a data block
   containing said requested information; and
   filling said cache with P data blocks at one time
   if the valid bit is on, where P is less than N,
   said P blocks including a data block containing
   said requested information.
   2. A method of filling a cache in a computer with
   information, the method comprising the steps of:
   searching the cache for the requested information;
   generating a miss signal when the requested
   information is not found;
   evaluating a valid bit of a data block in said
   cache where requested information should be
   located, when said requested information is not
   within said cache;
   filling said cache with N data blocks during a
   specified time interval if the valid bit is off,
   said N data blocks including a data block
   containing said requested information;
   comparing a process identification number of said
   data block where said requested information should
   be located with a process identification number of
   a process being run by said computer;
   filling said cache with said N data blocks during a
   specified time interval if said process
   identification numbers do not match; and
   filling said cache with P data blocks during a
   specified time interval if said process
   identification numbers match, where P is less than
   N, said P including a data block containing said
   requested information.
   3. A method of filling a cache with information in a
   computer with information, the method comprising
   the steps of
   searching a data block in said cache for requested
   information and generating a miss signal when said
   requested information is not found;
   storing a count of generated miss signals;
   evaluating a valid bit of said data block which was
   searched;
   filling said cache with N data blocks during a
   specified time interval if the valid bit is off,
   said N data blocks including a data block
   containing said requested information;
   filling said cache with said N data blocks during a
   specified time interval if said process
   identification numbers do not match;
   comparing said count to a first threshold number of
   misses;
   filling said cache with said N data blocks during a
   specified time interval if said count exceeds said
   first threshold; ;
   writing P data blocks to said cache during a
   specified time interval when: said count does not
   exceed said first threshold; and said valid bit is
   on; wherein P is less than N, and said P blocks
   include a data block containing said requested
   information; and
   decrementing said count each time the search for
   said data block does not produce a miss signal, and
   continuing filling said cache with said N data
   blocks during a specified time interval until said
   count is below a second threshold.
   4. The method of claim 3, wherein said decrementing
   step decrements said count to zero each time a
   search for said data block does not product a miss
   signal.
   5. The method of claims 1, 2 or 3 further comprising
   the steps of:
   storing a location of said miss when searching said
   cache results in said miss signal being generated;
   comparing said stored miss location with a location
   of a next occurring miss; and
   filling the cache with said N data blocks during a
   specified time interval if said stored miss
   location and said next occurring miss location are
   within a preset distance from one another.
   6. A method according to claim 5, wherein the present
   distance is a same aligned group of blocks.
   7. An apparatus for filling a cache of a computer with
   information, comprising:
   means for evaluating valid bits of a data block in
   said cache; and
   means for filling said cache with different sized
   blocks of data during a specified time interval in
   response to evaluation of said valid bit by said
   means for evaluating.
   8. The apparatus of claim 6, further comprising means
   for comparing a process identification number of
   said data block with a process identification
   number of a process being run by-said computer,
   wherein said means for filling fills said cache
   with different sized blocks in response to said
   comparison of said process identification numbers
   by said means for comparing.
   9. The apparatus of Claim 6, wherein said cache is a
   translation buffer.
   10. The method of Claims 1, 2 or 3, wherein N is 4 and
   P is 1.
   11. The method of claim 3, further comprising the step
   of comparing a process identification number of
   said data block which was searched with a process
   identification number of a process being run by
   said computer, and wherein said process
   identification numbers must match as a condition
   for the performance of P data blocks.
   12. The method of claims 1, 2 or 3 further comprising
   the steps of:
   storing a location of said miss when searching said
   cache results in said miss signal being generated;
   comparing said stored miss location with a location
   of a next occurring miss; and
   filling the cache with said N data blocks during a
   specified time interval if said stored miss
   location is a first block in an aligned group of
   blocks.
     _________________________________________________________________
   
   Data supplied from the esp@cenet database - l2
