
   CACHE WITH AT LEAST TWO FILL SIZES  
   Patent Number: [_]  WO8909444
   Publication date: 1989-10-05
   Inventor(s): STEELY SIMON CARL JR (US); RAMANUJAN RAJ KANNAN (US);
   BANNON PETER JOSEPH (US); BEACH WALTER ALOYSIUS (US)
   Applicant(s):: DIGITAL EQUIPMENT CORP (US)
   Requested Patent: [_]  EP0359815 (WO8909444),  A4,  B1
   Application Number: WO1989US01314 19890330
   Priority Number(s): US19880176596 19880401
   IPC Classification: G06F12/12
   EC Classification: G06F12/08B4T
   Equivalents: CA1314107, DE68924896D, DE68924896T, JP2500552T,
   JP2700148B2
     _________________________________________________________________
   
   Abstract
     _________________________________________________________________
   
   Optimizing the performance of a cache memory system is disclosed.
   During operation of a computer system whose processor (120) is
   supported by virtual cache memory (100), the cache must be cleared and
   refilled to allow replacement of old data with more current data. The
   cache is filled with either P or N (N>P) blocks of data. Numerous
   methods for dynamically selecting N or P blocks of data are possible.
   For instance, immediately after the cache is flushed, the miss is
   refilled with N blocks, moving data to the cache at high speed. Once
   the cache is mostly full, the miss tends to be refiled with P blocks.
   This maintains the currency of the data in the cache, while
   simultaneously avoiding writing-over of data already in the cache. The
   invention is useful in a multi-user/multi-tasking system where the
   program being run changes frequently, necessitating flushing and
   clearing the cache frequently.
     _________________________________________________________________
   
   Data supplied from the esp@cenet database - l2
