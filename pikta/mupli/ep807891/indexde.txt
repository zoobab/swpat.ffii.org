Esk: Elektronischer Einkaufswagen
descr: im WWW einzukaufende Gegenstände in einer Liste sammeln und erst zum Schluss kaufen.
Nie: Neues
Aci: Archiv
Nzi: Netzspiegel
Dak: Druckbare Dokumente
Ptn: Patente
Tbl: Tabellen
Bse: Beispiele
abi: automatisierbare medizinische Diagnose
Ssc: Steuerung eines Rechners durch einen anderen
Vea: Abfangen von Viren
dhf: dynamische Preisfestlegung
dtu: digitale Unterschrift mit zusätzlichen Authentifizierungsinfos
Cdr39: Computer system and method for performing multiple tasks
AWt: Umwandlung von Dateinamen
Mne: Sprachfernsteuerung, Sprachgesteuertes Internet-Surfen
Dse: Dynamisch erweiterbarer Webserver
VRl: Vorab-Rückmeldungen
iNo: intuitive Netzwerk-Konfiguration
Ean: E-Geschäftsanbahnung an der Börse
DKc: Dateisystem für Flash-Speicherbausteine
Esk: Elektronischer Einkaufswagen
DWP: Dateneinsparung bei mobiler TCP-Kommunikation
nna: Übersetzung von natürlichsprachlichen in formalsprachliche Datenbankanfragen mithilfe von Parsern und Tabellen
VeW: Visualisierung von Prozessen
Uuh: Unterscheidung zwischen benutzten und unbenutzten Blöcken bei der Behandlung von Cache-Zuweisungsfehlern
cui39: compression requests from a client to a server
spi39: select cooking recipes to generate a list of items to buy
Sle: Sprachlernen durch Vergleich eigener Aussprache mit der eines Lehrers
PWW: Prüfen von Lernstoff in Schulen
Met: Miteingabe von Kontextdaten
Hda: Hybride Klassen
VWA: Vererbung von CORBA Objekten
Shd: Schäden
geo: Geheimdoku
Gea: Gesammelte Papiere
Nzr: Netzforen
Air: Arbeitsgruppe
MdW: Förderer
Tgn: Tagungen
Kus: Debatte
Beu: Briefe
PiB: Priorität
claim1: A computer-executable process, embedded in a computer-useable medium, for supplying items on a network (46), the network having at least one computer-server (20) for communicating with users employing a browser program on a terminal/computer (35) at a location remote from said computer-server, said embedded process comprising the steps of: receiving (152), at the computer-server, a transmitted command from said browser program for a shopping page (40); in response to said transmitted command, generating (154) a shopping page file and transmitting (156) the shopping page file to said browser program; receiving (168), at the computer-server, at least one user selected item from the shopping page received (158) by the browser program; creating (174) a list at the computer server; at the computer server, adding (178) to the list each user selected item received by said receiving step; returning (184) from the computer server the list of items in an entry of a shopping page file to said browser program; and continuing user selection (200), receiving (168) data strings, adding (178) items to said list and returning (184) said list until termination by the user.

# Local Variables:
# mailto: mlhtimport@a2e.de
# login: swpatgirzu
# passwd: XXXX
# srcfile: /usr/share/emacs/20.7/site-lisp/mlht/el2x.el
# feature: swpatdir
# doc: ep807891
# txtlang: de
# coding: utf-8
# End:
