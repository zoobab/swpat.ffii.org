                           1 3             EP O 807 891 B1             1 4
if said second field was sent by said sending       comprise.
step, generating (1 82) a shopping list contain-
ing identification of items previously selected          retrieving the software programs identified as
by said user, and if not, creating (1 74) a shop-          selected items in said shopping list; and
ping list;                         5       after the retrieving step, downloading (225) at
adding (1 78) new selected items from the first          least one of said software programs to said
field to the shopping list;                       browser program.
returning (1 84) the shopping list of items in a  IO
shopping field of a shopping page file to said     Patentanspr�che
browser program;                      1.  Computerprogrammprodukt, das auf einem durch
storing at least the shopping list by said brows-       einen Computer nutzbaren Medium gespeichert ist,
er program at a terminal computer; and      15    zur Lieferung von Gegenst�nden �ber ein Netzwerk
(46), wobei das Netzwerk mindestens einen Com-
continuing cyclical transmitting (1 50), invoking       puter-Server (20) zur Kommunikation mit Anwen-
(1 54), building (1 60), receiving (1 68), and gen-       dern aufweist, die ein Browserprogramm auf einem
erating (1 82) the shopping list and adding (1 78)       TerminaIIComputer (35), welches �rtlich entfernt
items to the shopping list and returning (1 84) to  20    von dem Computer-Server gelegen ist, verwenden,
and storing the shopping list at the terminaII       wobei das Computerprogrammprodukt computer-
computer until termination by the browser pro-       lesbare Programmmittel umfa�t zumlzur.
gram.                                  a) Empfang (1 52) eines von dem Browserpro-
8.  A computer program product in accordance with  25       gramm �bertragenen Befehls f�r eine Ein-
claim 7, wherein said computer readable program          kaufsseite (40) auf dem Computer-Server;
means for determining, adding and returning com-          b) Erzeugung (1 54) einer Einkaufsseitendatei
prise.                                     und �bertragung (1 56) der Einkaufsseitendatei
an das Browserprogramm als Reaktion auf den
separating (1 70) data strings in the first field  30       �bertragenen Befehl;
and the second field into namelvalue pairs;            c) Empfang (1 68) auf dem Computer-Server
von mindestens einem vom Anwender aus der
determining (1 72) if there are any previous val-          vom  Browserprogramm  empfangenen  Ein-
ues in the second field, and if so,                  kaufsseite gew�hlten Gegenstands;
35       d) Erzeugung (1 74) einer Liste auf dem Com-
converting (1 76) said previous values into said          puter-Server;
shopping list of previously selected items;             e) Hinzuf�gen (1 78) auf dem Computer-Server
jedes vom Anwender gew�hlten Gegenstan-
converting values in the first field to new select-          des, der durch den Empfangsschritt empfan-
ed items;                        40       gen wurde, zu der Liste, um eine aktualisierte
Liste zur Verf�gung zu stellen;
adding (1 78) the new selected items to said          f) R�ckgabe (1 84) der aktualisierten Gegen-
shopping list; and                           standsliste von dem Computer-Server an das
Browserprogramm; und
converting (1 80) said shopping list to a field da-  45       g) Empfang der Liste und eines n�chsten ge-
ta string and then sending the field data string          w�hlten Gegenstands vom Browserprogramm
as the shopping field in a newly generated          auf dem Computer-Server; und
shopping page file.                          h) Wiederholung der Schritte e), f) und g), um
die aktualisierte Liste bis zur Beendigung des
9.  A computer program product in accordance with  50       Einkaufs durch das Browserprogramm zwi-
claim 7, further comprising computer readable pro-          schen Computer-Server und TerminaIICompu-
gram means for.                              ter umlaufen zu lassen.
deleting (235) a previous selected item from
the shopping list.                         2.  Computerprogrammprodukt nach Anspruch 1, wei-
55    terhin umfassend computerlesbare Programmmit-
1 O. A computer program product in accordance with       tel zum.
claim 7, wherein said items are software programs,           Senden (21 5) der gew�hlten Gegenst�nde
and said computer readable program means further       von dem Computer-Server an den TerminaIICom-
8
