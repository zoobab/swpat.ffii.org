                           1 5             EP O 522 591 B1             16
tions that are necessary to determine the total sales of     no restriction on the executing order of these two),
the last month. Similarly, the total sales of this month's     ( interrogation 3).
table POintS tO thiS mOnth'S intermediate reSult table     _O063l  The system proceeds to process each of the
1 46_ BOth Of the intermediate reSUlt tableS 1 44 and 1 46     interrogations as indicated in Fig. 1 9. In particular, for
Seek tO ha Ve infOrmatiOn regarding the CUStOmer COde  5   interrogation  1,  which  is interrogation for the last
and the tOtal SaleS fOr their reSPeCtiVe mOnthS_ ln Order     month,s intermediate result table, the customer table in
tO CaICUlate the tOtal SaleS Of the laSt mOnth, it iS neCeS-     the database 9 (Fig. 1 7c) is retrieved using retrieval unit
Sary tO determine the CaICulatiOn Obje Ct (i_e_, what kind     34 to obtain the customer code information. Further-
Of infOrmatiOn iS being SOught)_ ln additiOn, it iS neCeS-     more, the system seeks to sum the amount fields in the
SarY tO determine the amOUnt Of OrderS that Were  1o  received order file of the database 9. In order to perform
reCei Ved dUring the mOnth frOm that CUStOmer_ ACCOrd-     this calculation, the system sums the amount entries
ingIY, there iS an additiOnal table, the t Otal SaleS Of the     having the appropriate customer code and which meet
IaSt mOnth'S intermediate reSUlt table 1 48_ AnaIOgOUSIY,     the date limitations of last month. The EQ table 3 is
a tOtal SaleS in thiS mOnth'S intermediate reSult table 1 51     used to ensure that the date requirements are fulfilled.
that SeekS Similar infOrmatiOn fOr thiS mOnth'S Sale, iS  15  ln this fashion, the intermediate result table is filled in
aISO prOVided_ HenCe, the amOUnt Of reCeiVed Order fOr     with the relevant information.
thiS mOnth and la St mOnth fOr the SPeCified CUStOmer     _oo64l  lnterrogation 2 involves the processing for this
COde are re4Ue Sted and PaSSed tO the databaSe fOrmUla     month,s intermediate result table. The processing is the
generatiOn Unit 32 WhiCh COnVertS the IOgiC fOrmUla intO     same as interrogation  1  except that different date
a databaSe retrieval fOrmula 1 57 uSing the databaSe  2o  requirements are utilized. Specifically, the date must
retrieval wOrd grammar definitiOn table 155_ The reSult     correspond to the limitations for this month. In this fash-
table and the VariOUS intermediate reSUlt tableS 144,     ion, the information for this month,s intermediate result
1 46, 1 48 and 151 are PaSSed tO the databaSe fOrmUla     table iS Completed.
generatiOn Unit 32_ ln additiOn, e4UalitY tableS Cden Oted     _oo6_l  Lastly, interrogation 3 is processed. The inter-
aS EQ tableS) are PaSSed tO the databaSe fOrmula gen-  25  rogation 3 is the interrogation for the result table. As Fig.
eration unit 32_ SPeCifiCalIY, EQ T_bleS 3 and 4, aS     1 9 indicates, the customer table in the database cus-
Shown in Fig_ 1 7b, are PaSSed to the databaSe formula     tomer and name are selected, as are the total sales of
generatiOn unit 32, EQ T_ble 3 SeekS tO determine if the     this Iast month table and the total sales of this month
reCei Ved Order file date iS e4Ual tO the la St mOnth date,     table. This information is retrieved from the customer
and EQ T_ble 4 SeekS tO determine if the reCeived Order  3o  table in the database 9 (Fig. 1 7c) and from the Iast
file date iS _Ual tO tOdaY'S date_                  month,s intermediate result table 1 44 (Fig. 1 7b) and this
_O061 l  The entity table logic formula 1 40 is processed     month's intermediate result table 1 46. In order for the
by the database formula generation unit 32 (Fig. 1 7c)     customer name to be output, the sales of this month
which uses the database retrieval word grammar defini-     table must be greater than the sales of last month table
tion table to process the logic formula 140. The data-  35  and the customer code of this month's intermediate
base  retrieval  word  grammar  definition  table  is     result table must equal the customer table and code.
examined by the database formula generation unit 32     _O066l  ln this manner, automatic generation of data-
with respect to the retrieval logic formula 1 40. The data-     base retrieval formula is possible. Operations are con-
base retrieval word definition table initially processes     nected by means of pointer and a logic unit for judging
result table as indicated in Fig. 1 8. In particular, the sys-  4o  executing order is provided in the database formula
tem is directed to select the SELECT (item) FROM (ref-     generator unit 32 in Fig. 6.
erence table) WHERE (condition). Thus, the result table     _O061l  Further, this approach provides the additional
is converted  into a database  retrieval  formula of     advantage a plurality of sequenced data retrievals are
( interrogation 3) of Fig. 1 9. The retrieval word grammar     possible by way of intermediate results. The system
definition table 1 55 has a similar entry for the intermedi-  45  also provides the advantage that it is possible to readily
ate result tables 144 and 1 46. Further, the database for-     conform to a different database retrieval language by
mula generation unit 32 investigates the executing order     altering the grammar definition table.
of the specified operations with respect to another. In     _O068l   Specifically, when the retrieval language is
this case, since the result table 1 42 designates last     changed, the database retrieval formula for a new
month's intermediate result table 144 and this month's  5o  retrieval language may be generated and an e_ensive
intermediate result table 1 46 as ''le_ side _ right side'' in     rewriting thereof is not necessary. Rather, a simple
the GT table, it is learned that the operation of left side     change in the description of (item), (reference table),
and right side must be performed before the GT table     (condition) or SELECT, FROM, WHERE of the desig-
can be processed. In other words, it is seen that deter-     nated item to the result table of the grammar definition
mination of the intermediate result tables must be per-  55  table is all that is required.
formed first.                              _O069l  For some natural Japanese queries, a compli-
_O062l   ln this manner the execution order is deter-     cated or plurality of processing must be performed to
mined as ( interrogation 1), ( interrogation 2) (there is     analyze the query. For example, there are instances
9
