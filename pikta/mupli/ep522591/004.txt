                       5            EP O 522 591 B1            6
selecting a virtual table which a term has in common       virtual table 28 of Fig. 6;
with an Other term Of the 4uerY, a retrieval exeCuti On unit       Figs. 1 6a and 1 6b are diagrams illustrating the
fOr retrieving data frOm the data baSe On the baSiS Of       operation of the system with a query that employs
Said databaSe retrieVal fOrmUla_                    the seasonal time period;
_O01 9l   The information retrieval system may also  _     Figs. 1 7a - 1 7c illustrate the processing of an entity
include an additional table for converting an undeter-       table logic formula;
mined value phrase in the natural language query into a       Fig. 1 8 is a depiction of a database retrieval word
determined value phrase in the database based on the       grammar definition table 1 55 that is contained in the
syntax analysis result. Still further, the information       virtual table 28 of Fig. 6;
retrieval system may include a terminology dictionary  1o    Fig. 1 9 is an example of a database retrieval for-
for identifying entries in the virtual table that are to be       mula processing for the entity table logic formula of
used in converting phrases of the natural language       Figs. 1 7a - 1 7c;
query. The dictionary includes words representing times       Figs. 20a and 20b illustrate the grouping in syntac-
and the dictionary is used by the parser in obtaining the       tic trees of _o complex queries; and
syntax analysis result. When the terminology dictionary  1_    Figs. 21 a and 21 b depict additional virtual tables
is used, the system may also include a time interval def-       employed for the processing of the queries of Figs.
inition table in the virtual table for defining dates corre-       20a and 20b.
sponding to words representing time. Lastly, the system
may include a database retrieval formula conversion     _O020l  A preferred embodiment of the present inven-
unit for generating a formula in a database retrieval lan-  2o  tion will now be described with reference to the draw-
guage from the database retrieval formula.            ings.  Fig.  6 shows the construction and flow of
processing of a first preferred embodiment of the
Fig. 1 is a blockdiagram of a first conventional data-    present invention which provides a database retrieval
base retrieval system illustrating the processing    system that responds to a natural language query 1.
performed by the system;                2_  Like the first conventional system of Fig. 1, the system
Fig. 2 is a block diagram of a data processing sys-    may be implemented on a data processing system as
tem suitable for implementing the first conventional    shown in  Fig. 2.  This first preferred embodiment
system;                            includes an input unit 2, a conversation control unit 3
Fig. 3a is a more detailed depiction of the word dic-    and a database 9 like that employed in the conventional
tionary 4 of Fig. 1;                    3o  system of Fig. 1. These components are implemented
Fig. 3b is a more detailed depiction of the hierarchi-    in the data processing system 2 as discussed for the
cal table model 6 of Fig. 1;                  first conventional system. The preferred embodiment,
Fig. 3c is a more detailed depiction of the database    however, differs from the conventional system in several
9 of Fig. 1;                           respects. These distinctions are highlighted below.
Figs. 4a - 4c illustrate dictionaries in a second con-  3_  _O021 l  The first preferred embodiment also includes a
ventional database retrieval system;             parser 22 for parsing an input natural language query
Fig. 5 illustrates the input format for queries with the    into its constituent parts. The parser 22 uses a grammar
second conventional database retrieval system;      table 24 and a terminology dictionary 26. The grammar
Fig. 6 is a block diagram of an embodiment of the    table 24 holds information for regulating the relation in a
present invention illustrating the processing per-  4o  Japanese sentence, and the terminology dictionary 26
formed by the embodiment;                 ddines the part of speech and meaning of each word in
Fig. 7 is a more detailed depiction of the terminol-    the query 22. While the terminology dictionary 26 is
ogy dictionary 26 of Fig. 6;                  similar to the conventional word dictionary 4 shown in
Figs. 8a - 8c are more detailed depictions of tables    Fig. 1, the terminology dictionary of Fig. 6 differs in that
held in the virtual table 28 of Fig. 6;           4_  is includes a column for a semantic marker (see Fig. 7).
Fig. 9 is an illustration of a syntax tree that is output    The role of the semantic marker is described in more
by the parser 22;                       detail below. A column for a semantic ID (see Fig. 7) and
Fig. IO is a flowchart of steps performed by the sys-    a column for a correspondence item are also provided.
tem and processing a natural language query;       The parser analyzes the input query 22 to determine the
Fig. 1 1 is a more detailed depiction of a definition  _o  subject, predicates and other parts of speech in the
table in the virtual table 28;                  input natural language query 22.
Fig. 1 2 is a depiction of an example natural lan-     _O022l  The system of Fig. 6 differs substantially from
guage correspondence logic formula;            the conventional system of Fig. 1 in that the system of
Fig. 1 3 is a depiction of the modified version of the    Fig. 6 includes a virtual table 28. The virtual table is a
formula of Fig. 12                    __  natural language conversion virtual table held in mem-
Fig. 1 4 is a more detailed depiction of the collating    ory 1 2 (Fig. 2), for designating which table in the data-
unit 30 of Fig. 6;                        base 9 is to be searched to find the data requested in
Fig. 1 5 is a depiction of a Definition Table A in the    the query 22.
4
