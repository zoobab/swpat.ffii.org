                           3             EP O 522 591 B1             4
name, respectively. The word dictionary 4 indicates that     system, the input query for the second conventional
both of these phrases are nouns. The dictionary 2 is not     system would be as follows. The first noun field would
referenced for the zyoshi ''ha'' and ''no''.              be entered as ''chokoreeto rui'' and the corresponding
_ool ol   Syntax and semantic analysis is then per-     PartiCle field WOUld be entered aS ''nO''_ FUrther, the SeC-
formed on the query. In particular, syntactic analysis is  5   Ond nOUn field WOUld be entered aS ''Uriage'' and the
performed to process the syntax or the query in order to     PartiCle field WOUld be entered aS ''ha''_
understand the role each phrase serves in the query.     _O014l  ln this second conventional system, queries in
Semantic analysis, on the other hand, is performed to     a natural Japanese format cannot be analyzed. Like-
understand what is being requested by the query.        wise, the retrieval object is determined in view of the
_O01 1 l  Subsequently, semantic analysis is performed  1o  restriction of the designated format shown in Fig. 5. A
to relate the meaning of the query to the database     pertinent data file may, thus, be accessed only by lim-
entries. The semantic analysis relies on the hierarchical     ited terminology including synonyms recorded in the
table model 6 (see Fig. 3b) to ascertain that ''chokoreeto     dictionaries.
rui'' (chocolates and the like) is an attribute data expres-     _O01 5l   ln the first conventional information retrieval
sion word of a commodity group in table 1 8 (i.e., table C  15  system described above, it is necessary to have previ-
in Fig. 3c) and ''uriage'' (sales) is an item name in the     ously constructed a hierarchical table model. Since,
table 1 4 (i.e. table A in Fig. 1 3c). Moreover, the hierar-     however, in general, it is not always possible to place
chical table model 6 (Fig. 3b) indicates that table 1 4 is a     the content of a database into a hierarchy, input sen-
higher order table than table 1 8. Since the attribute item     tences which do not fall under the defined hierarchical
appearing in the low order table is a noun, and a zyoshi  2o  structure cannot be processed. Further, there is no flex-
''no'' is added thereto, it is recognized that the attribute     ibility in receiving natural language phrases or words,
''chokoreeto rui'' in table  1 8 modifies the attribute     such as ''sengetsu'' (last month) which are not in the
''uriage'' (sales), which appears in a higher order table     database. The system is limited solely to the phrases
1 4. Using these results, a retrieval formula ''retrieval     included in the database. Still further no information is
condition. (commodity group name = cho_reeto rui),  25  provided on ''zyoshi'' (particles). Thus, there is also the
retrieval object. uriage'' is obtained and is output from     problem that the ommission of a ''zyoshi'' cannot be
the retrieval sentence analysis unit 5. Subsequently,     detected.
retrieval from the database 9 is performed by the     _O01 6l  ln addition, when there is an ambiguous word
retrieval processing unit 8 to obtain the desired data.      (for example, time periods or seasons), syntactic analy-
_O01 2l  Figs. 4a, 4b and 4c show dictionaries used in a  3o  sis is impossible unless the definition of the ambiguous
second conventional database retrieval system, as dis-     word is recorded in detail. In some cases, each interro-
closed in Japanese Patent bid-Open Publication No.     gator must record the definition on an individual basis
59-99539. In these dictionaries, information on column     according to his usage of the ambiguous term.
name in a file, information on data item name, and infor-     _O01 ll   lnformation retrieval is performed for each of
mation on a file name that possesses a common col-  35  the items recorded in a file. Thus, an answer cannot be
umn name or data name, are stored according to file     obtained for a question in which a plurality of files are
names of a data file that is contained in a database. Fig.     retrieved as a result of analyzing the input sentence and
4a represents a dictionary in which one of the database     in which it is necessary to process such a retrieval result
files contains the column name of a file. The dictionary     to obtain a final result.
also holds information regarding the order in which the  4o  _O018l   The foregoing problems in the prior art are
column is contained in the file and additionally holds     overcome by the present invention of an information
information regarding synonyms of the column name     retrieval system as defined in in claim 1. The information
(i.e., file numbers and column attribute numbers of col-     retrieval system for retrieving information from a data-
umns that are synonymous with the named column).     base comprising an analysis unit using a dictionary for
Fig. 4b shows an analogous dictionary in which one of  45  analyzing a natural language query according to the
the files contains a data column name, and the diction-     present invention for this reason is characterized by said
ary stores a position at which the named column is con-     analysis unit being a parser for parsing said natural lan-
tained in the file. Lastly, the dictionary stores information     guage query into its constituent parts to determine a
regarding synonyms of the data column name. Fig. 4c     syntax analyzing result as to the construction of the
shows a dictionary holding information as to semanti-  5o  query; said dictionary including a column for a semantic
cally identical data columns that are connected as syn-     ID which determines the semantic meaning of terms of
onyms.                                 said constitutent parts in a manner which can be under-
_O01 3l  Fig. 5 is the designated format for input queries     stood by the database; virtual tables for identifying the
for  the  second  conventional  system.  This  format     terms of said constituent parts in the database, each
requires that queries be entered as a number of entries,  55  term being associated with one or more virtual tables,
wherein each entry includes _o fields; a noun filed and     said virtual tables accounting for particles that modify
a particle or auxiliary field. Thus, for the example query     the parts, a collating unit for preparing a database
1 (Fig. 1) used in the discussion of the first conventional     retrieval formula from the syntax analysis result by
3
