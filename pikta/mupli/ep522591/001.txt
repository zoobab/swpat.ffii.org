         ,,,,   o    E,;;;p,;;; h,;;__,,,en,_,;;,              l lllllll llllll lll lllll lllll lllll lllll lllll lllll lllll lllll llllll llllllll llll
off,ce eu;opeen des _;evets            (1 1)     E P O 522 59 1  B 1
(1 2)              EUROPEAN PATENT SPECIFICATION
(45) Date of Publication and mention            (51) lnt. Cl.7; _06F 1 ll30
of the grant of the patent.
22.03.2000 Bulletin 200011 2
(21) Application number. 921 1 1820.4
(22) Date of filing. 1 O.Ol.1 992
(54) Data_ase retrie_al system _or responding to natural language queries with corresponding
ta_les
Datenbankauffindungssystem zur Beantwortung natursprachlicher Fragen mit dazugeh�rigen
Tabellen
Syst�me de recouvrement de donn�es pour r�pondre aux interrogations en langage naturel avec des
tables oorrespondantes
(84) Designated Contracting States.              _ lta_ashi, Yoshiko,
DE FR _B                         clo Mitsu_ishi Denki
_makura-shi, _nagawa-ken (JP)
(30) Priority. 1 1.Ol.1991 JP 1 11 21 191            _ Kimura, Chikako,
clo Mitsu_ishi Denki
(43) Date Of PUbli CatiOn Of aPPliCatiOn_              _makura-sh,, _nagawa-ken (JP)
1 3.01.1 993 BUllet,; 1 993I02              _ l;a_a, Naohi,o,
(73)                              clo Mitsu_ishi Denki
MPrIOTPSr_egt OlrSHl DEN_ KAguSHl_ KAISHA         _makufa-sh,, _;agawa-ke; (Jp)
T__O (JP)                      (74) Representative.
(72)                               Pfenning, Meinig & Partner
InVentOrS_                         Mozaf,s,fasse 1 l
_ T_ka;aSh,, lkUkO,                     go336 Mu__;;he; (DE)
CIO M_ltSU__ISh_l De;k_l
_makUfa-Sh,, _;a4aWa-ke; (JP)         (56) References cited.
_ Kondo, Shozo,                      Ep-A- o 421 24o       wo-A-gg)og455
Clo MitSu_i Shi Denki                   uS-A- 4 gg4 g61
_makura-shi, _nagawa-ken (JP)
_ Suzuki, Katsushi,                    _ PATENT ABSTRACTS OF JAPAN vol. 013, no.
clo Mitsu_ishi Denki                   263 (P-886)1 9 June 1 989 & JP-A-1 058 01 9 (
_makura-shi, _nagawa-ken (JP)            FUJITSU LTD) 6 March 1 989
_ Naganuma, _zutomo,                 _ PATENT ABSTRACTS OF JAPAN vol. 1 35, no.
clo Mitsu_ishi Denki                   OOO (P-919)20 Decem_er 1 989 & JP-A-1 243 1 1 6 (
_makura-shi, _nagawa-ken (JP)            HITACHl LTD) 21 Septem_er 1989
_
_
_
__
_
__   Note. Within nine months from the publication of the mention of the grant of the European patent, any person may give
o   notice to the European Patent Office of opposition to the European patent granted. Notice of opposition shall be filed in
a written reasoned statement. It shall not be deemed to have been filed until the opposition fee has been paid. (Art.
_w   99(1) European patent convention).     Prin_ed by Kerox2.<_U6K._)_3B.u6siness Services
