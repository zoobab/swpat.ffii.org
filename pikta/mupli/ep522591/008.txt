                         1 3             EP O 522 591 B1             1 4
directs the user to entry 3 in Definition Table A as shown     _O056l   The time interval definition reference unit 80
in Fig. 1 5. This entry indicates that spring e_ends from     contains the actual dates corresponding to ''fuyu'' (win-
03I01 to 05131. In this manner, the word ''kotoshi no     ter). It obtains these dates by referring the time interval
haru'' (the spring of this year) contained in the syntax     ddinition table 84. Hence, as shown in Fig. 15, ''fuyu'' is
analysis result is replaced by ''1 990 nen 3 gatsu 1 nichi  _   ddined as starting at ''OOO01 201 '' (i.e., December 1)
- 1 990 nen 5 gatsu 31 nichi'' (March 1 1990 - May 31     and ending at ''OO01 0331 '' i.e., March 31 of the ne_
1 990).                                  year). The time interval definition table reference unit 80
_O052l  ln this example, however, any combination of     SubStituteS the retrieved value 86 fOr ''fuYu'' Cwinter) in
time words to be usgd must be recorded on a terminol-     the pOint in time CaICUlatiOn reSUlt 24 tO Obtain a time
ogy dictionary as a single word. For example, when it is  1o  inter Val definiti On table referenCe reSUlt 76_
desired that ''kotoshi'' (this year) and ''haru'' (spring) be     _O051l   The combining unit 82 combines the actual
combined ''kotoshi no haru'' (the spring of this year), it is     dates corresponding to ''sakunen'' (the last year) and
necessary to previously record ''kotoshi no haru'' (the     ''fuyu'' (winter) by addition to obtain a complete 8 digit
spring of this year) in the terminology dictionary 26 (Fig.     range for dates for the interval as shown in the calcula-
6). Further, since the definition of a seasonal word or  1_  tion result 78. Specifically, the year ''19890000'' is added
the like differs from user to user, a terminology diction-     to the dates of ''fuyu'' ''OOO01 201 '' - ''OO0I0331 '' to obtain
ary must be prepared for each user.                ''1 9891 201 ''  -  ''1 9900331 ''.  The  calculation  result
_O053l  As such, an alternative embodiment as shown     ''1 9891 201 -1 9900331 '' means ''from December 1, 1 989
in Figs. 1 6a and 1 6b may be employed. This alternative     to March 31, 1 990''. The calculation result 78 is then
embodiment differs from the first embodiment in that it  2o  processed as discussed in the first embodiment.
includes. a point in time calculation unit 70, for calculat-     _O058l  By changing the definition of each time word
ing a specific point in time from the current date, a time     described in the time interval definition table 84 (Fig.
interval definition table reference unit 80, and a combin-     1 6b), the user may obtain a calculation result in accord-
ing unit 82 for adding the reference result of the time     ance with definition without altering the terminology dic-
interval definition table reference unit 80 and the calcu-  2_  tionary 26 (Fig. 1 6a). That is, it is possible for users to
Iated result of a point in time. Further, a system timer 68     share a terminology dictionary and manage the time
is provided.                               interval definition table individually. This benefit of shar-
_O054l  Suppose that ''sa_nen no fuyu no uriage ha''     ing a terminology dictionary is more apparent when it is
(Sales during the winter of the last year?) is entered     appreciated that a terminology dictionary is large in size
from the input unit 2 as the input query 66 (Fig. 1 6a).  3o  and amendment of a terminology dictionary is difficult.
The parser 22 generate a syntax analysis result 72 (i.e.,     Moreover, if words containing many modifiers are to be
a syntax tree) by employing the grammar table 24 and     ddined, storage requirements are large. Hence, provid-
the terminology dictionary 26. The syntax analysis     ing a separate terminology dictionary for every user is
result contains ''sakunen'' (last year) and ''fuyu'' (winter),     cumbersome.
which are time words.  The definition of the word  3_  _O059l  The example input natural language queries 1
''sakunen'' (the last year) is obtained by time calculation,     (Fig. 6) and 66 (Fig. 1 6a) requested sales information
and the definition of the word ''fuyu'' (winter) is desig-     that could be readily reproduced by the system. The
nated to be described in the time interval definition table     system, however, is capable of handling more sophisti-
82 (Fig. 1 6b).                              cated queries that require reasoning. For example, sup-
_O055l  The syntax analysis result 72 is passed to the  4o  pose that the Japanese input query is a sentence
collating unit 30, where the result is received by the     ''Sengetsu no uriage yori kongetsu no uriage ga ooi
point in time calculation unit 70. At the point in time cal-     tokuisaki ha'' (What customer had more sales in this
culation unit 70, a point in time calculation is performed     month than sales in the last month?). For such an input
with respect to the current date (e.g., ''1 9901 224'') that     natural language query, the system produces a retriev-
is obtained by a system timer 68. The actual calculation  4_  ing logic formula, also _own as the entity table logic
method performed is selected from the definition pro-     formula 1 4, in the form 140 shown in Fig. 1 7a. The for-
vided in Definition Table B in Fig. 1 1. The definition that     mula 1 40 includes a result table 1 42 for storing the final
is chosen depends on the value in the argument column     results of the retrieved data. The result table 1 42
in the terminology dictionary. In this example, an 8-digit     includes a location for storing the customer's name and
integer value indicating the year ''sakunen'' (last year),  _o  tables for storing the total sales of this month and the
''19890000'', is obtained from the calculation method,     total sales of last month. In addition, the entity table
corresponding to the value ''1 1 '' in the argument column     logic formula 1 40 includes a GT table, which is a table in
of ''sakunen'' (the last year), which states, ''Subtract 1     the virtual table that performs a logical operation on
from the four high order digits and replace the four low     parameters to determine if one parameter (the le_ side)
order digits with ''OOOO''. Subsequently, the calculated  __  is greater than the other (the right side).
integer value is substituted for the portion of ''sakunen''     _O060l  The total sales of the last month table includes
(the last year) in the syntax analysis result 72 to obtain     a pointer pointing to a last month's intermediate result
a point in time calculation result 74.                 table 1 44 that holds the results of intermediate calcula-
8
