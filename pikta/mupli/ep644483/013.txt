                             23              EP O 644 483 B1              24
Iedit terminal virtuel r�serv� VT_, (4) comporte        de simulation d'utilisateur et ledit quatri�me poin-
un bloc de commande r�serv� B_, qui lui est af-        teur P,_, pointant sur ledit bloc de commande sp�ci-
fect�,                                  fique du programme B_,.
Iedit terminal T comporte un bloc de commande   5  l.  Syst�me informatique selon la revendication 5 ou
de terminal reSerVe BT qUi IUi eSt affeCte, et         6, danS leqUel ladite COnneKiOn IOgiqUe CT entre le-
dit terminal T et ledit terminal virtuel r�serv� VT, du-
Iadite COnneKiOn IOgiqUe L_,T entre ledit terminal        dit prOgramme de SimUlatiOn d'UtiliSateUr peUt etre
T et ledit terminal virtuel r�serv� VT_, peut etre        �tablie par la m�morisation d'un cinqui�me pointeur
�tablie par la m�morisation d'un premier poin-  IO     PT_ dans ledit bloc de commande de terminal BT
teUr P_,T danS ledit bIOC de COmmande B_, et la        et par la memOriSatiOn d'Un SiKieme pOinteUr P_T
memOriSatiOn d'Un SeCOnd pOinteUr P_ danS le-        danS Un emplaCement de memOriSatiOn affeCte
dit bloc de commande de terminal BT, ledit pre-        audit programme de simulation d'utilisateur, ledit
mier pOinteUr P_,T pOintant SUr ledit bIOC de COm-        CinqUieme pOinteUr PT_ pOintant SU r ledit prOgram-
mande de terminal BT et ledit second pointeur  15     me de simu lation d'utilisateu r et ledit siKi�me poin-
P_,T pOintant SUr ledit bIOC de COmmande B_,.          teUr P_T pOintant SU r ledit bIOC de COmmande Spe-
cifique du terminal BT.
3.  Syst�me informatique selon la revendication 2,
danS leqUel ledit premier pOinteUr P_,T eSt Un pOin-     8.  PrOCede deStine a eKeCUter deS taCheS mUltipleS
teur null si ladite conneKion logique l_,T entre ledit  20     dans un syst�me informatique, ledit syst�me infor-
terminal T et ledit terminal Vi_Uel reSerVe V_ n'eSt        matiqUe COmprenant Un terminal T, Une pIUralite
pas �tablie.                                 d'ordinateurs hotes, une pluralit� de programmes
d'applications m�moris�s sur des ordinateurs diff�-
4.  Syst�me informatique selon la revendication 2 ou        rents parmi lesdits ordinateurs hotes, et un sous-
3, dans lequel un terminal virtuel r�serv� VTT est  25     syst�me de communications (3), ledit proc�d� com-
affect� audit terminal T et ledit bloc de commande        prenant les �tapes suivantes dans lesquelles les
de terminal BT comprend de pr�f�rence un pointeur        conneKions logiques sont �tablies au moyen d'un
de terminal VirtUel PT qUi pOinte SUr ledit terminal        mOde d'aCCeS de teleCOmmUniCatiOnS VirtUelleS.
Vi_Uel reSerVe VTT dUdit terminal T.            30       a) s�lectionner un programme d'application A_,
5.  Syst�me informatique selon l'une quelconque des           parmi ladite pluralit� de programmes d'applica-
revendications pr�c�dentes, dans lequel ledit sys-          tions,
t�me informatique comprend en outre un program-
me de simulation d'utilisateur (7) et un terminal vir-           b) �tablir une conneKion logique L_, par l'inter-
tuel r�serv� VT, (1 4) qu i est affect� audit program-  35        m�diaire dudit sous-syst�me de communica-
me de simulation d'utilisateur,                        tions entre lesdits programmes d'applications
A_, et un terminal virtuel r�serv� VT_, (4, 9),
Une COnneKiOn IOgiqUe C_, (C_, C2) entre ledit
prOgramme de SimUlatiOn d'UtiliSateUr et ledit          C) etablir Une COnneKiOn IOgiqUe l_,T entre ledit
terminal virtuel r�serv� VT_, dudit programme  40       terminal T et ledit terminal virtuel r�serv� VT_,,
d'application peut etre �tablie sous la comman-
de dudit programme de simulation d'utilisateur,           d) mainten ir ladite conneKion logique L_, ind�-
et                                       pendamment de  l'eKistence  de  ladite con-
neKiOn IOgiqUe l_,T.
une conneKion logique CT entre ledit terminal  45
T et ledit terminal virtuel r�serv� VT, peut etre     9.  Proc�d� selon la revendication 8, dans lequel ladite
�tablie.                                 conneKion logique L_, est �tablie
6.  Syst�me informatique selon la revendication 5,           a) en affectant un bloc de commande r�serv�
dans lequel ladite conneKion logique C_, entre ledit  50        B_, (B_, B2) audit terminal virtuel r�serv� VT_,,
programme de simulation d'utilisateur et ledit termi-
nal virtuel r�serv� VT_, dudit programme d'applica-           b) en affectant un bloc de commande de termi-
tions A_, peut etre �tablie par la m�morisation d'un           nal r�serv� BT audit terminal T,
troisi�me pointeur P_,, dans ledit bloc de commande
B_, et la m�morisation d'un quatri�me pointeur P,_, �  55       c) en m�morisant un premier pointeur P_,T dans
un emplacement de m�morisation qui est affect�           ledit bloc de commande B_,,
audit programme de simulation d'utilisateur, ledit
trOiSieme pOinteUr P_,, pOintant SUr ledit prOgramme           d) en memOriSant Un SeCOnd pOinteUr P_ danS
1 3
