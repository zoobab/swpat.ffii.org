
   Claims
   
   CLAIMS
   What is claimed is: 1. A mechanism for specifying fees for an online
   service, comprising:
   means associated with an object of the online service for defining a
   triggering action for a fee;
   means associated with the triggering action for defining a fee; and
   means for editing a plurality of such specifications.
   2. A mechanism for determining a fee for an online service,
   comprising:
   means for detecting an action on an object of the online service;
   means, operative in response to detection of the action, for
   identifying a fee specification for the action and the associated
   object; and
   means for utilizing the fee specification to define the fee.
   3. A mechanism for specifying fees for an online service as defined in
   claim 2, comprising:
   means associated with an object of the online service for defining a
   triggering action for a fee;
   means associated with the triggering action for defining a fee; and
   means for editing a plurality of such specifications.
   4. The online service development tool as claimed by claim 1 wherein
   each of said fees are triggered by a defined user action.
   5. The online service development tool as claimed by claim 4 wherein
   one of said defined user actions comprises access by said user to one
   of said visual objects on said online system 6. The mechanism as
   claimed by claim 4 wherein one of said defined user actions comprises
   submittal of an object for inclusion in said data store of said online
   service.
   7. The mechanism as claimed by claim 4 wherein one of said defined
   user actions comprises a traverse of a hyperlink.
   8. The mechanism as claimed by claim 4 wherein one of said defined
   user actions comprises a connection to an online service.
   9. The mechanism as claimed by claim 1 wherein each of said fees are
   triggered by passage of a defined amount of time.
   10. The mechanism as claimed by claim 1 wherein both said means for
   defining define a fee specifier, wherein the fee specifier comprises a
   first field specifying a triggering action that triggers said fee.
   11. The mechanism as claimed by claim 10 wherein said fee specifier
   further comprises a second field specifying an entity to who the fee
   is directed.
   1 2. The mechanism as claimed by claimsl O or 11 wherein said fee
   specifier further comprises a third field specifying an object
   associated with said fee event.
   13. The mechanism as claimed by claimsl0, 11 or 12 wherein said fee
   specifier further comprises a fourth field defining a fee computation
   formula.
   14. The mechanism as claimed by claim 13 wherein said fourth field
   comprises a script specifying said fee computation formula.
   1 5. The mechanism as claimed by claim 1 4 wherein said script
   comprises at least one fee setting script primitive.
   1 6. The mechanism as claimed by claim 1 wherein if said fee is
   positive, a fee is charged to said entity, and if said fee is
   negative, a payment is made to said entity.
   1 7. The mechanism as claimed by claim 1 3 or 1 4 further comprising:
   a script editor, said script editor for editing a script specifying
   said fee computation.
   1 8. The mechanism as claimed by claim 1, wherein a document object is
   HyperText Markup Language (HTML) document.
   1 9. A computer system for designing an online service, comprising:
   a first editing module for displaying and allowing editing of
   relationships among document objects of the online service;
   a second editing module for editing individual document objects of the
   online service; and
   a mechanism for invoking the second editing module in response to
   selection of a document object in the first editing module.
   20. A computer system for designing an online service, comprising:
   a viewing module for displaying relationships among and allowing
   selection of document objects of the online service;
   an editing module for editing individual document objects of the
   online service; and
   a linking mechanism for invoking the editing module in response to
   selection of a document object in the viewing module.
   21. A computer system for allowing editing of fee structures of an
   online service, comprising:
   means for displaying a visual representation of a fee specification
   having user-modifiable portions, wherein a user-modifiable portion is
   provided for entries for an indication of a document object of the
   online service, an indication of an event in connection with the
   document object, and a fee formuala;
   means for receiving user input to edit a fee specification using the
   visual representation and to store edited fee specifications; and
   means for storing a plurality of fee specifications defined using the
   means for displaying and means for receiving.
   22. The computer system of claim 21 wherein the visual representation
   is a template.
   23. The computer system of claim 21 or 22 wherein the fee formula is
   defined using a scripting language.
   24. The computer system of claims 21 or 22 or 23 wherein the fee
   specifications are stored in a list.
     _________________________________________________________________
   
   Data supplied from the esp@cenet database - l2
