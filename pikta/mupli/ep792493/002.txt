                                        EP O 792 493 B1
Description
FIELD OF THE INVENTION
5   [OO01]  The present invention relates to the field of online computer services. In particular, the present invention
discloses a software tool for setting fees in an online service, as part of a visually oriented tool for creating online
SerV_ICeS.
BACKGROUND OF THE INVENTION
IO   [OO02]  With the increasing popularity of computer communications, many companies are becoming interested in
advertising and supporting their products using an online computer service that can be accessed by customers. How-
ever, creating a large online computer service is an eKtensive task. To develop a sophisticated online service, such as
America Online_, CompuServe_, Genie_, or Prodigy_, a company must have a large mainframe computer and cus-
15   tomized software. Developing the customized software requires a competent programming staff and a good deal of
time. Most companies do not have the resources required to develop such systems, and thus cannot easily develop
and maintain an online presence.
[OO03]  One way a company can contact millions of potential customers is to use the global lnternet. The global
Internet is a network of computer networks that links together millions of computer systems using the well defined TCPI
20   l P protocol.
[OO04]  A new method of distributing and viewing information known as the World-Wide Web has recently become
very popular on the global lnternet. The World-Wide Web is a collection of servers connected to the lnternet that provide
multi-media information to users that request the information. The users access the information using client programs
called ''browsers'' to display the multi-media information.
25   [OO05]  World-Wide Web servers store multi-media information in a document format known as Hyper TeKt Markup
Language (HTML). The World-Wide Web servers distribute the HTML formatted documents using a specific commu-
nication protocol known as the Hyper TeKt Transfer Protocol (HTTP).
[OO06]  To access the multi-media information available on World-Wide Web servers, a user runs a client browser
program that accesses the HTML formatted documents stored on the HTTP servers connected to the global lnternet.
30   The client browser program retrieves the formatted information and provides the information in an appropriate manner
to the user. For eKample, the client browser program displays graphical image information as images on the user's
graphical display screen; plays video information as video animation on the user's graphical display screen; displays
teKt information as teKt on the user's screen; and plays sound samples using the speakers on the user's computer
system. ''Mosaic'', one popular client browser program, is widely available to the users of the global lnternet.
35   [OOOl]  For a company that wishes to develop an online presence, creating a World-Wide Web Server would provide
a feature rich online service available to customers and clients. A World-Wide Web Server can store images, te Kt,
animation, and sounds that provide information about the company. Furthermore, World-Wide Web Servers can be
implemented on relatively simple computer systems, including personal computers.
[OO08]  Most World-Wide Web Servers are coupled to the global lnternet. By deploying a World-Wide Web Server
40   on the global lnternet a company would create online service that is accessible to the millions of global lnternet users.
[OO09]  Alternatively, a company can deploy a HTTP server that is available to customers through dial-up phone
service. A dial-up HTTP server would be accessible to customers and clients that do not have lnternet access. Thus,
by creating a simple HTTP server, any organization or corporation can create an online presence.
[O01 O]  However, quickly creating the HTML formatted documents required for a World-Wide Web Server is not a
45   trivial task. Moreover, the standard HTTP server software, without any additional programming, is very limited. For
eKample, without custom eKtensions, an HTTP server cannot accommodate compleK transactions between a user and
the HTTP server or integrate a database system into an online service. Although it is possible to write custom eKtensions
to the HTTP server software using a conventional programming language, such custom eKtensions are difficult to write
eKcept by eKperienced programmers. Thus, to be able to quickly deploy full-featured HTTP servers, it would be desir-
50   able to have a development tool usable by non-programmers that allows a developer to quickly and easily create a
full-featured online service based upon the HTTP and HTML standards.
[O01 1]  Many programming development tools are known in the art. These programming development tools range
from tools which are developed and marketed as general purpose programming development tools to sophisticated
special purpose development tools for developing specific types of applications.
55   [O01 2]  For eKample, the lnformation EKchange Facility (l EF) general development tool, which is available from TeKas
Instruments, is used by professional programmers to develop application programs. Essentially, l EF provides a facility
that allows a programmer to write ''pseudo code'' and l EF generates an intermediate source code program in a high
Ievel programming language (such as COBOL or C code) based on the ''pseudo code''. IEF is an eKample of what will
2
