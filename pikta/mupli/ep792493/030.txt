                                        EP O 792 493 B1
[021 5]  The Metering subtool allows the developer to manipulate a list of Metering Specifiers. Each Metering Specifier
is a pair consisting of. (1) an online service entity (document, hyperlink, subservice, service, etc.), and (2) the particular
property of that entity that should be metered. The properties that can be metered include. number of users who access
the entity, number of minutes that they use the entity, total number of times that the entity was accessed, number of
5   times the entity was viewed vs. downloaded, number of times the entity was requested by another service, times of
day that entity is accessed, etc.
[021 6]  After gathering metering information, the online service provider can view the metering information in graph,
chart, and numeric form, using separately provided analysis software. The metering information can be used to tune
the performance of a server. For eKample, a developer can eKpand certain service areas that receive heavy use.
IO   Similarly, a developer can discard portions of an online service that are infrequently used, cost-justify a more powerful
server to run the service, assess how often a user is ''referred'' to this service from another service, etc.
The Debu  er Subtool
15   [021 l]  To facilitate the development of an online service, a Debugger subtool eKists. The Debugger subtool provides
a means for the developer to ''run'' an online service that is being developed. The Debugger subtool simulates an
access to an online service from user client software such that a developer can test an online service by accessing
the online service in the same manner that a user would,
[021 8]  The Debugger subtool, can be stopped at any point. When the Debugger is stopped, the developer can use
20   an appropriate Utility Subtool to modify the currently displayed hyperdocument or subservice infrastructure (or any
other part of the service). After modifying the subservice, the developer can resume the simulation of the online service
from the stopping place. The Debugger also allows the developer to single-step through scripts, inspect and change
script variables, and even modify the scripts themselves, like conventional debuggers for interpreted languages in
other application domains.
25   Claims
1.  A system for specifying fees for an entity associated with an online service, comprising.
30        (a) means associated with an object of the online service for defining at least one of a plurality of triggering
actions for a fee_,
(b) means associated with a triggering action for defining a fee specification for the entity;
(c) means for editing a plurality of fee specifications for the entity; and
35        (d) means for storing the plurality of fee specifications using the editing means, characterised in that the editing
meanS COmpriSeS.
(e) means for displaying a visual representation of the fee specification having user-modifiable portions, where-
in a user-modifiable portion is provided for entry of an indication of an object of the online service, an indication
of a triggering action in connection with the object, and a fee specification; and
40        (f) means for receiving user input to edit the fee specification using the visual representation and for storing
edited fee specifications.
2.  A system for determining a fee for an entity associated with an online service according to Claim 1, further char-
acterised by comprising.
45        (a) means for detecting at least one of a plurality of actions on an object of the online service, said object being
associated with an action_,
(b) means, operative in response to detection of the action, for identifying a fee specification for the action and
the object associated with the action; and
50        (c) means for utilizing the fee specification to define the fee for the entity.
3.  The system of Claim 1 or Claim 2, characterised in that the visual representation is a template.
4.  The System of Claim 1 or Claim 2 or Claim 3, characterised in that the fee formula is defined using a scripting
55      language.
5.  The system of any preceding claim, characterised in that the plurality of fee specifications are stored in a list.
3O
