                                        EP O 792 493 B1
checked, all entries in a common category are grouped together under that category name for browsing
by users. If a single entry is in more than one category, it appears under each of those categories.
Typically, this boK is checked for ''yellow pages'' Directory Lookup subservices and Classified Adver-
tisement subservices. Whether or not this boK is checked, the user may still perform standard queries
5             against the database of entries using the Query Form for the subservice.
Sorting     A drop-down list boK that indicates how entries are sorted within a categoy for user browsing. Entries
may be sorted in forward or reverse order based on the contents of any of the enty fields, and sec-
ondary and tertiay sort keys are supported with additional drop-down list boKes in the tool. In addition,
IO             the entry's date of posting is available as a sort key, even if that date is not displayed as part the enty
itself. Typically, a ''yellow pages'' Directory Lookup subservice will sort in forward order based on the
contents of the Name field, and a Classified Advertisement subservice will typically sort in reverse
order of posting date.
15   E_piration    The number of days that each enty remains listed on the subservice before it is automatically removed.
This option may also be left blank, in which case there is no automatic eKpiration date for entries. If
an entry has an individually specified Removal Date that occurs before automatic eKpiration, the entry's
Removal Date is honored. Otherwise, the automatic eKpiration date is used.
20   [01 64]  The Script View and the Link View in the Lookup Designer are analogous to the Script View and the Link View
in the Online Designer tool itself and in the Hyperdocument Designer.
[01 65]  The Fee View of the Lookup Designer is an optional feature that invokes the Fee Setter subtool, allowing the
developer to specify the formula for computing the cost of viewing an entry (if any), and submitting an entry (if any).
The Fee Setter subtool is described in greater detail in a separate section below.
25   Utilit  Subtools
[01 66]  The Utility Subtools provide capabilities that are useful in the design of multiple types of subservices. These
subtools are accessed from the Designer Subtools, and from the Online Designer itself. The most significant and
30   original Utility Subtools are. (1) the Hyperlink Editor, for manipulating hyperlinks within an online service; (2) the Script
Editor, for editing the various scripts that control the behavior of an online service; (3) the Fee Setter, which allows the
developer to specify any fees that should be charged to users or advertisers; (4) the Metering Tool, which provides
instructions to the online service server regarding the usage statistics that should be tracked; and (5) the Debugger,
which provides interactive running and debugging capabilities for an online service. This section provides the details
35   on the five Utility Subtools.
The H  erlink Editor Subtool
[01 61]  The Hyperlink Editor Subtool is a Utility Subtool that is used to display and manipulate hyperlinks within an
40   online service. The hyperlinks within an online service can be viewed and modified at various levels of abstraction. For
eKample the Hyperlink Editor Subtool can display and manipulate the links between different subservices within an
online service, the links between different documents within a subservice, the links within a single document, or the
individual attributes of the links themselves.
[01 68]  With the Hyperlink Editor, the developer can assign attributes to hyperlinks. Some important eKamples of
45   hyperlink attributes include. (1) whether the hyperlink leads to the same documen Ufile or to a different documen Ufile;
(2) whether the hyperlink leads to the same online service or a different online service; (3) whether the hyperlink leads
to a service that the developer controls or doesn't; (4) the size of the documentlfile a link points to; (5) whether the link
Ieads to a free service or one that has additional charges; and (6) the semantics of the hyperlink. The latter attribute
is a semantics tag taken from a known list of possibilities, which includes simple linking, making a purchase, returning
50   to the home page of the service, initiating a search, linking to a form that requests shipping address information, etc.
The semantics attribute of hyperlinks provides some additional structure to online services, and encourages a degree
of standardization in hyperlink usage.
[01 69]  The Hyperlink Editor supports both a Graphical View and a List View of hyperlinks. The Graphical View dis-
plays the hyperlinks as a directed graph, with the source and target of a hyperlink represented by visual icons, and
55   displays the hyperlink itself as a directed arc connecting them. An arc's particular appearance (color, width, arrow
design, and other visual cues) depends on the various attributes (above) associated with that hyperlink. The List Vew
displays a list of the hyperlinks, showing the names of the linked entities and visual cues indicating the attributes
associated with each hyperlink. The developer can modify the hyperlinks and attributes from either view.
2O
