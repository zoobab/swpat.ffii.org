          ,,,,   o    ;;;o;;;; '___,;',e;;.,;; '              lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll lllll
Office europ�en des brevets            (,,)     E _ O l92 493 B _
(1 2)               EUROPEAN PATENT SPECl FICATION
(45) Date of Publication and mention            (51) Int CI.6. G06F 1 ll30, G06F 1 7160
of the grant of the patent.
1 1 _08_1 999  BUllet,; 1 999132             (86) International application number.
PCTIUS9511 4I01
(21) Application number. 95939902.3            (87) lnternational publication number.
(22) Date of filing_ 08_1 1 _1 995                   Wo 9611 55o5 (23.o5.1 996 Gazette 1 996123)
(54) AN ONLINE SERVICE DEVELOPMENT TOOL WITH FEE SETTING CAPABILITIES
HERSTELLUNGSHILFE F�R ONLINE-Dl ENSTE MIT G EB�H RENFESTSTELLUNG
OUTIL DE DEVELOPPEMENT DE SERVICES EN LIGN E A FONCTIONS D'ETABLISSEMENT DE
TAXATION
(84) Designated Contracting States.            (74) Representative. Spall, Christopher John
DE FR GB                          BARKER BRETTELL
1 38 Hagley Road
(30) Priority. 08.1 1.1 994 US 336300              Edgbaston Birmingham B1 6 9PW (GB)
(43) Date of publication of application.           (56) References cited.
03.09.1 991  Bulletin 1 991136                EP-A- O 483 516       WO-A-93I08661
WO-A-94128480       US-A- 5 204 891
(73) Proprietor. VERMEER TECHNOLOGIES, INC.       US-A- 5 359 508
Cambridge, MA 021 38 (US)               _ IBM TECHNICAL DISCLOSURE BULLETIN, vol.
(72) lnventors.                           34, no. 1 1, 1 April 1 992, pages 425-421,
_ FERGUSON, Charles, H.                  XPOO030331 5 ''Link Web Class Hierarchy''
Cambridge, MA 021 39 (US)               _ IBM TECHNICAL DISCLOSURE BULLETIN, vol.
_ FORGAARD, Randy, J.                   31, no. 6B, June 1 994, NEW YORK, US, pages
Le_ington, MA 021 13 (US)                 451 -460, XPO0200941 5 ''Multimedia Audio on
Demand''
_
_
__
_
__
_   Note. Within nine months from the Publication of the mention of the grant of the EuroPean Patent, anY Person maY give
o   n OtiCe tO the EUrOPean Patent OffiCe Of OPPOSiti On tO the EUrOPean Patent 9ranted. NOtiCe Of OPPOSiti On Shall be filed in
_   a written reasoned statement. It shall not be deemed to have been filed until the opposition fee has been paid. (Art.
w   99(1) European Patent Convention).      Printed by Jouve, 75001 PARIS (FR>
