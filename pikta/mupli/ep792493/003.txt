                                        EP O 792 493 B1
be referred to herein as a ''general purpose development tool'' because it allows development of programs for essentially
any purpose or application dependent on the input provided by the programmer.
[O01 3]  ln contrast to general purpose software development tools, many application programs themselves provide
special purpose ''development tool'' capability. An eKample is the ParadoKTM database program available from Borland
5   lnternational of Scotts Valley, California. The ParadoKTM database allows end users to develop sophisticated database
applications which would have been developed by professional programmers a fewyears ago. The ParadoKTM database
is but one eKample of a special purpose development tool.
[O01 4]  Another eKample of a special purpose development tool, perhaps more pertinent to the present invention, is
the Application Development Environment of Lotus NotesTM which is available from Lotus Development Corporation
IO   of Cambridge, Massachusetts. The Application Development Environment of Lotus Notes provides features which are
said to allow for rapid development of workgroup applications such as sharing of documents between users over a
network. Generally, Lotus Notes and, thus, its Application Development Environment, is directed at sharing of docu-
ments among persons in an authorized work group. For eKample, a Lotus Notes application can be envisioned which
would allow for sharing of key patent applications among patent eKaminers in a particular art group at the United States
15   Patent Office.
[O01 5]  The Lotus Notes Application Development Environment provides for such features as (i) application design
templates which are said to allow sophisticated applications to be built by customizing pre-built applications such as
document libraries, form-based approval systems, project tracking applications and status reporting systems; (ii) se-
curity; (iii) database access; and (iv) discussion groups. However, while these features are useful, the Lotus Notes
20   Application Development Environment, as well as Lotus Notes itself, has its shortcomings as admitted to by even Lotus
Development Corporation itself.
Lotus Notes was not intended to be used as a transaction-processing front-end to an operational database sys-
tem. Operational systems are those which support transactions that are essential to the operation of an organization.
25   EKamples of these systems would be traditional order entry...
Lotus Notes. An Overview October, 1 993, pg. 1 1
[O01 6]  lt has been recognized by the present invention that many of these functions neglected by Lotus Notes are
very important when developing publicly accessible online systems. Specifically, the ability to perform commercial
transactions that involve order enty systems would allow an online system to sell goods and services to computer
30   users. It is now recognized by the present invention that many functions such as traditional order enty systems and
the like will someday be carried out over computer networks by allowing a customer to place orders for goods and
services directly with an online service. By way of eKample, even today, food orders can be placed with restaurants
over computer networks; videos can be reserved at the local video store; and banking transactions can be carried out
simply by logging onto a computer network.
35   [O01 l]  Four different types of commercial transactions might commonly occur in a commercial online service. First,
a user may be charged for the right to access all or parts of a useful publicly accessible online system. Second, the
online service may pay the user for performing some type of action such as winning a contest or completing a marketing
survey. Third, an online service may charge a content provider for placing certain information on the online service.
For eKample, a content provider can be charged for placing an advertisement on the online service. Finally, a content
40   provider can be paid by the online service for providing information that users may wish to access. That be can be
provided on a for-fee basis. Conversely, an online service provider may wish to pay third party content providers for
placing useful material on the online service.
[O01 8]  Thus, when creating a publicly accessible online system, it is desirable to include the ability to define fee
structures for accessing parts of the online system andlor ordering other goods or services. However, creating a so-
45   phisticated commercial online service with such features usually requires specialized programming.
[O01 9]  The ability to set fees to be paid by the user for an amount of data accessed, the time spent ''logged on'' to
the online service, or the purchase of particular merchandise is one eKample of distinction from Lotus Notes. Lotus
Notes is not only admitted (by even Lotus Development Corporation) as lacking transaction oriented capability as may
be required by such applications, but it also does not provide the metering functions to keep track of the information
50   necessary to assign such fees as is required by these applications. As such, the video store, restaurant or bank (by
way of eKample) is left with the need to employ professional programmers for their individual applications.
[O020]  Thus, it has been discovered that there eKists a need to create online system development tools that include
features, functions and capabilities to support commercial online services such as the aforementioned fee setting
function.
55   [O021]  These and other aspects of the present invention will be described in greater detail with reference to the below
detailed description and the accompanying figures.
[O022]  A computer system for realising an online service is known from US-A-5 204 897, EP-A-O 483 576 and l BM
Technical Disclosure Bulletin, Vol. 37 No. 6B, June 1 994, New York, US, pages 451 -460. These documents teach the
3
