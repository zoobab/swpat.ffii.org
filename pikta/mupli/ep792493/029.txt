                                        EP O 792 493 B1
ProviderAttrSet(<provider-num>, <attr-name>, <value>)
Associates, with the content whose provider identifier is <provider-num>, an attributed name <attr-name>
having value <value>. If that attribute already eKists for that provider, replaces the value of the attribute with this
new value. One eKample of using attributes on content providers might be to record in a ''Non-profit'' attribute the
5      value ''Yes'' or ''No, '' depending on whether the provider is a non-profit organization.
ProviderAttrGet$(<provider-num>, <attr-name>)
Gets the value of the attribute named <attr-name> for the content provider whose provider identifier is
<provider-num>. The value is returned as a string, but can be converted to any other appropriate type using the
IO     data type conversion functions provided by the Computation Language and the Script Language. TeKt to data
conversion functions are well known in the art and are not discussed in this document.
USerT_taIACCeSSCOUntOI_(<USer-nUm>)
Returns the total number of files that have ever been accessed bythe user whose user identifier is <user-num>.
15     The User TotaIAccessCountFunctionOIo is useful for computing quantity discounts.
USerT_taIACCeSSSiZeOI_(<USer-nUm>)
Returns the total size of the files that have ever been accessed by the user whose user identifier is <user-num>.
The User TotaIAccessSizeFunction is useful for computing quantity discounts.
20      User AttrSet(<user-num>, <attr-name>, <value>)
Associates, with the user whose user identifier is <user-num>, an attribute name <attr-name> having value
<value>. If that attribute already eKists for that user, replaces the value of the attribute with this new value. One
eKample of using attributes on users might be to use an ''Age'' attribute to record the age of the user. This information
25      might be used to offer senior citizen discounts on downloading fees, for eKample.
User AttrGet$(<user-num>, <attr-name>)
Gets the value of the attribute named <attr-name> for the user whose user identifier is <user-num>. The
value is returned as a string, but can be converted to any other appropriate type using the data type conversion
30     functions provided by the Computation Language and the Script Language.
UserSearchTime_(<user-num>, <provider-num>)
Returns the total amount of time that the user whose user identifier is <user-num> has been searching the
content databases of the content provider whose provider identifier is <provider-num>, in this session.
35      EntryCategOryCOUntOI_(<path>)
In a ''yellow pages'' or classified advertisement style service, returns the total number of categories under
which the entry, whose file path name is <path>, has been listed. Entries that are listed under many categories
can be charged a higher fee than entries that are listed in only a few categories.
40      Server Load! ()
Returns the current load on the server as a value between O.OO and 1.OO, with O.OO meaning no server load
and 1.OO meaning that the server is fully loaded. The Server Load function can be used to set fees depending on
the current load on the server. To discourage access during peak usage periods, higher prices can be assigned
45     during peak usage times.
The Meterin  Subtool
[021 3]  A wide variety of metering capabilities are provided by the Molisa online service platform. The metering ca-
50   pabilities track the usage patterns of an online service, and the usage by users and other services. The metering
information can provide invaluable feedback on the volume and duration of access to documents, subservices, and
the online service as a whole. Furthermore, the metering information is available from the Fee Computation language
using defined functions such that fees can be based on user usage.
[021 4]  lt would be an unnecessay performance burden for the server to gather all possible statistics on all possible
55   online service entities. With the Metering Tool, the developer indicates specifically which statistics should be gathered,
and on which parts of the online service. The online service server tracks service usage in the ways specified in the
metering subtool. (The server also gathers the specific usage data required by the fees indicated in the Fee Setter
subtool.)                             29
