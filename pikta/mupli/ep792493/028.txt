                                           EP O 792 493 B1
the Fee @ predefined global variable is the fee that is levied on the entity (if the value is positive) or paid
to the entity (if the value is negative).
Arg$      The value of the argument element of the Fee Specifier. The Arg$ is provided as a notational convenience.
5    PrOViderOI_   The prOVider identifier nUmber Of the COntent prOVider aSSOCiated With the aCtiOn that triggered the Fee
Specifier. Th is predefined global variable is available when the entity element of the Fee Specifier is
''PrOVider''. If the aCtiOn iS ''ACCeSS'', the VaIUe Of PrOViderOI_ iS the identifier nUmber Of the COntent prOVider
that owns the information that was accessed. If the action is ''Daily'', ''Weekly'', ''Monthly'', or ''Annually''
IO            (and the Fee Specifier is ''Provider''), the Fee Specifier is evaluated once for each content provider of
the Online SeNiCe. In thiS CaSe, the PrOViderOI_ VaIUe iS the prOVider identifier nUmber Of the CUrrent COntent
provider being referenced in th is iteration of the Fee Specifier computation.
USerOI_     The USer identifier nUmber Of the USer aSSOCiated With the aCtiOn that triggered the Fee SpeCifier. ThiS
15            predefined global variable is available when the entity element of the Fee Specifier is ''User''. If the action
iS ''ACCeSS'' Or ''SUbmit'', the VaIUe Of USerOI_ iS the l D nUmber Of the USer that aCCeSSed Or SUbmitted the
information. If the action is ''Daily'', ''Weekly'', ''Monthly'', or ''Annually'' (and the Fee Specifier is ''User''),
the Fee SpeCifier iS eVaIUated OnCe fOr eaCh USer Of the Online SeNiCe. l n thiS CaSe, the USerOI_ VaIUe iS
the user identifier number of the current user being referenced in this iteration of the Fee Specifier com-
20            putation.
AccessTime_The amount of elapsed time that was required to access the current object. Th is predefined global variable
is valid on ly in ''Access'' or ''Submit'' Fee Specifiers.
25   Available Bu ilt-In Functions
[021 2]  All of the primitives of the Script Language are available in the Computation Language. These primitives
include bu ilt-in functions for general computing purposes (e.g., ''Now()'' to obtain the current dateltime, ''FileLen(<path>)
'' to determine the length of a file, etc.), as well as built-in functions that are specific to online services. The online
30   service primitives that are of particular interest for creating Fee Computations are detailed below.
PrOViderFileCOUntOI_(<prOVider-nUm>)
Returns the total n umber of files, carried on the online seNice, belonging to the content provider whose provider
identifier is <provider-num>.
35      ProviderFilePath$(<provider-num>, <indeK>)
Returns the path of the file at indeK <indeK> in the list of files associated with the content provider whose
prOVider identifier iS <prOVider-nUm>.  The alIOWable  range Of <indeK>  iS  1  thrOUgh  PrOViderFileCOUntOI_
(<provider-num>), inclusive.
40      PrOViderT_taIACCeSSCOUntOI_(<prOVider-nUm>)
Returns the total number of files, belonging to the content provider whose provider identifier is <provider-num>,
that haVe eVer been aCCeSSed by any USerS On thiS Online SerViCe. The PrOViderT_taIACCeSSCOUntOI_ fUn CtiOn iS
useful for computing quantity discounts.
45      PrOViderT_taIACCeSSSiZeOI_(<prOVider-nUm>)
Returns the total size of the files, belonging to the content provider whose provider identifier is <provider-num>,
that haVe eVer been aCCeSSed by any USerS On thiS Online SerViCe. The PrOViderT_taIACCeSSSiZeOI_ fUn CtiOn iS
useful for computing quantity discounts.
50      PrOViderT_taICOntentCOUntOI_(<prOVider-nUm>)
Returns the total number of files on this online service belonging to the content provider whose provider iden-
tifier is <provider-num>.
55      Provider TotaIContentSizeOIo(<provider-num>)
Returns the total size of all files on this online seNice belonging to the content provider whose provider identifier
is <provider-num>.                      28
