                                        EP O 792 493 B1
register a new online service with a service-of-services or ''yellow pages'' service; (8) check whether another server
is running a particular online service or type of service; and (9) eKchange usage and metering information, for
aggregation and later analysis.
5   _   Su  ort for an Online Desi ner Scri t Lan ua e. The Online Designer supports two different types of scripts.
Event Scripts and Function Scripts. An Event Script is associated with a particular event for a particular visual
object in the online service. For eKample, there could be an Event Script associated with a ''Mouse Down'' event
for a ''Search'' button on a ''ListingQueryForm'' hypermedia document in an electronic ''white pages'' service. The
event script associated with the mouse-down event would specify how to convert the user input fields on the
IO      ''ListingQueryForm'' form into a quey for the teKt searchlretrieval engine on the server. In general, there can be
several Event Scripts associated with a single visual object, potentially one script for each type of event that is
defined for that visual object. A Function Script contains a single named function (subroutine) that can be shared
and invoked by multiple other scripts in the same service.
15   _   Launchin  and control of other software a  Iications. These capabilities are achieved using inter-application com-
munication techniques such as Windows DDE, Windows OLE, Open Doc, keystroke stuffing, terminal emulation,
command-line invocation, batch file invocation, and the like. For eKample, an online service can compute the
quantity discount for a catalog item by automatically launching a spreadsheet program, plugging the item number
and quantity into certain prearranged spreadsheet cells, invoking a spreadsheet macro to compute the discount,
20     and obtaining the item price from a prearranged result cell. Other eKamples include launching and controlling
applications for payroll, inventory, purchasing, and Manufacturing Resources Planning (MRP).
_   Directl  and trans arentl  accessin  real-time data sources. Structured Query Language (SQL), Open Database
Connectivity (ODBC), and other published and proprietary data access methods can be used to access real time
25     data sources. For eKample, a catalog shopping online service can check the available stock on a certain merchan-
dise item by issuing an appropriate SQL query to the inventory database. The inventory database would return
the information to the online service software such that the online service could provide the information to the user
or perform an electronic transaction.
30   _   Accessin  and mani ulatin  control e ui ment. Equipment such as heatinglventilationlair-conditioning systems,
security systems, and lighting can be accessed and controlled.
_   Re Iication of online service content. The service's content and structure can be replicated to other online services
on-demand or on an automatic, regularly scheduled basis.
35   _   Meterin  of user usa e  atterns for the online service. This can include the number of users who access the
service, the duration of each user's connection time, the number of times that a certain part of the service is
accessed, the number of times that a user was ''referred'' to this service by hyperlinking from another service, etc.
This data can be used to levy fees for users, advertisers, or information providers, or to tune the service itself.
40   _   ControIIin  access to information. The available information on an online service can be controlled utilizing pass-
words, encyption, and assigning specific access rights to specific users.
_   Real-time coo erative activit. Support of real-time cooperative activity between two or more users, or between
45      users and a representative of the online service provider. For eKample, a multi-person game between users, or a
user entering an online query and receiving a real-time response from a service representative.
_   Ca turin  and Editin  Ima es. Allowing a user or service operator to capture an image to be displayed as part of
an online service (for eKample, a logo for a ''vellow pages'' directoy listing, or a photograph to accompany an
50     online classified advertisement), by faKing an image directly to the server, sending an image to the server using
electronic mail, or scanning an image at the client workstation and electronically transmitting the image to the
server. If a user does not have access to facsimile or scanning equipment, the user may physically send or deliver
the photograph or graphic to a service operator, who will electronically capture the image on behalf of the user
and transmit the image to the online service server.
55   _   Buildin  an electronic service store. Allows a user to download entire online services (the structure andlor content),
usually for a fee. The user can then deploy those services on the user's own computer equipment.
1 O
