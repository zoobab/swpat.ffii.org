                                        EP O 792 493 B1
event to identify the fee associated with the triggering event, the individual against whom the fee is levied and the
document object in relation to which the event occurred. The result of this search is a fee formula which is then computed
to define the fee, either a payment or a debit. The fee calculation mechanism is part of the server which makes the
online service available.
5   [O029]  Accordingly, another aspect of the invention is a mechanism for determining a fee for an online service. This
mechanism detects an event associated with an object of the online service. In response to detection of the event, a
fee specification for the event and the associated object is then identified. The fee specification is then used to define
the fee. This aspect of the invention is used in combination with a fee specification. This fee specification associates
an object of the online service with a triggering event and a fee computation which defines the fee for the object and
IO   event. An editor allows for editing a plurality of such fee specifications.
[O030]  lt is another object of the present invention to provide a fast, user-friendly method of designing and deploying
a distributed online service. In particular, it is an object of the present invention to allow a developer to create customized
HTTP server software, such as scripts, and accompanying HTML documents for deployment on a World-Wide Web
server. This object is achieved by providing a visual editor that allows a developer to easily create the subservices that
15   constitute the online service. These subservices generally include scripts and documents which are designed to meet
a specific need. A variety of templates of suitable scripts and documents are provided for a variety of typical kinds of
online services. For eKample, such subservices may include a HyperdocumentICommerce subservice for displaying
hyperdocuments and performing electronic transactions, a Classified Advertisement subservice for implementing elec-
tronic classified advertisements, a Reference subservice for implementing online reference works, a Director Lookup
20   subservice for implementing online searchable directories of information, a Bulletin Board subservice for providing a
means for allowing users to post and view messages, a Document Retrieval subservice to provide a means for retrieving
documents, an Electronic Publishing subservice that provides electronic editions of newspapers or magazines that
may be downloaded, and Meta-Service subservice that provides access to other eKternal online services.
[O031]  Accordingly, another aspect of the present invention is a computer system for designing online services. The
25   system includes an editing module for manipulating a collection of documents as a service. This editing module pref-
erably contains a number of sets of templates of scripts and documents which can be used to generate an online
service quickly. A viewing module allows for display of summary views of the service.
[O032]  Another aspect of the invention is a computer system for editing an online service that includes a first editing
module that has editing functions for visually editing relationships between document objects of a service. For eKample,
30   a hyperlink editor with a link view, which is a graphical display of document objects and hyperteKt links between them,
may be provided. A second editing module is used to allow editing of particular document objects. Such a system is
provided with the fee setting tool of this invention and the associated fee specifications.
[O033]  ln any of the foregoing aspects of the invention fees may be triggered by a defined user action, such as access
by said user to one of said visual objects on said online system, or submittal of an object for inclusion in said data store
35   of said online service, or a traverse of a hyperlink, or connection to an online service. Fees may also be triggered by
passage of a defined amount of time.
[O034]  Additionally, in any of the foregoing aspects of the invention, a fee specification particularly may have a first
field specifying a fee action that triggers said fee. Another field defines the fee computation. An optional field is a field
specifying an entity to whom the fee is directed. A field specifying an object associated with said fee event allows
40   different objects within an online service to have different fees. A script may be used to specify the fee computation.
The script preferably has at least one fee setting script primitive, but may have more. By allowing both negative and
positive fee values and credit and debit system can be implemented. For eKample, if a fee is positive, a fee is charged
to the entity, and if a fee is negative, a payment is made to the entity. A script editor is preferably used to edit scripts
specifying fee computations.
45   [O035]  The foregoing aspects of the invention are particularly useful in the World-Wide Web of the global lnternet
for use in generating HTML documents and scripts.
BRIEF DESCRIPTION OF THE DRA_INGS
50   [O036]  The objects, features, and advantages of the present invention will be apparent from the following detailed
description of the preferred embodiment of the invention with references to the following drawings.
[O031]  Figure 1 illustrates a block diagram overview of an online service that is implemented with the Molisa platform.
[O038]  Figure 2 lists the general steps for an electronic commerce transaction with an online service.
[O039]  Figure 3a illustrates how the Online Designer is used to create an online service.
55   [O040]  Figure 3b illustrates how a hyperdocument is edited using the Online Designer.
[O041]  Figure 4 lists the available subservice design programs.
[O042]  Figure 5 illustrates a hyperlink from a Reference subservice to an order form in a HyperdocumenUCommerce
subservice.                            5
