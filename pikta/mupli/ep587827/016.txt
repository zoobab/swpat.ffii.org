                                        EP O 587 827 B1
1 O. Dispositif selon la revendication 8 ou 9, caract�ris� par les caract�ristiques suivantes, en ce qu'il comprend une
premi�re interface d'entr�elsortie (II01) et un appareil de transmission de donn�es (SW; COM) qui sont con_us
pour recevoir des instructions de commande de la station d'instructionlcontrole (C1) et les transmettre � (auK)
l'unit� (unit�s) (C2) � commander respectivement et pour le retour en temps r�el des param�tres de service effectifs
5      de l'unit� (des unit�s) � commander � la station d'instructionlcontrole (C1), �galement des moyens de simulation
qui simulent, ind�pendamment des param�tres de service effectifs, une modification des param�tres de service
ob�issant auK instructions de commande sur une image des donn�es (SIM) simul�e dans la station d'instructionl
controle (C1) en meme temps que les entr�es des instructions de commande.
IO   1 1. Dispositif selon l'une quelconque des revendications 8 � 1 O, caract�ris� en ce que appareil d'entr�e (MEM) pr�-
sente un clavier etlou un Pointing Device (appareil de pointage) pour la modification de l'image des donn�es en
temps r�el (MEM) et un �cran est utilis� pour la repr�sentation de l'images des donn�es en temps r�el.
1 2. Dispositif selon l'une quelconque des revendications 8 � 1 1, caract�ris� en ce qu'une image des donn�es en temps
15      r�el (MEM) peut etre repr�sent�e pour le secteur de la technique domestique et pour une installation industrielle
avec des donn�es en temps r�el delpour machines, appareils, moteurs et vannes ainsi que appareils de mesurage
et de controle.
20
25
30
35
40
45
50
55                                     1 6
