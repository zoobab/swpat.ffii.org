                           5             EP O 490 624 B1             6
of machine type, node type and data link card (DLC) ca-     represents the relatively all inclusive combination of AP-
pability. As embodied, the dialog window 1 2 provides for     PC, SQL LOO, and netbios protocol capability network.
name and address defaults together with resources for     Again, it should be clearly evident that a network admin-
describing two distinct data link cards.               istrator is burdened with an inordinate selection not only
[O023]  The invention is preferably practised using a  5  as to the node functions but now also as to the media
PS12 workstation having an OS12 operating system and     of connection and communication between the nodes.
an l BM brand mouse pointing and control device. Using     [O021]  According to the preferred system and method
such mouse, the network administrator selects the     of the present invention, the administrator selects icons
workstation icon 1 3 from pane 9 and places it into work     representing diverse workstation capabilities and inter-
space 8 to indicate a node in the network. Once the di-  IO  connects such workstation nodes by selecting commu-
alog information for window 1 2 has been entered by the     nication protocols. The nodes and connections are
administrator, the functional characteristics of node 1 3     graphically depicted by icons and connecting lines or ar-
are established.                            row patterns. The network can be manipulated as need-
[O024]  Fig. 3 illustrates the image of the screen at a     ed and is particularized by dialog windows with broad
Iater stage in the definition of the network. At this stage  15  default capability. As depicted in Fig. 5, by the presence
an additional workstation 1 4 as well as database server     of window 37, the network configuration information can
1 6 have been added to the network being defined. Fig.     be stored and subsequently recalled for selecting refine-
3 also illustrates the commencement of a connection to     ment. The invention also contemplates a spreadsheet
be defined between workstation 1 4 and database server     interface for very large networks, to permit easy replica-
1 6. A connection dialog window is not necessary in that  20  tion of corresponding node functions and connection
the connection parameters are derivable by matching     protocols. Direct transformation between spreadsheet
the connection network selected from pane 1 1 to the link     parameters and graphical representations is readily ac-
card capabilities ascribed to each workstation node. The     complished by known transformation programs and
three node network depicted in Fig. 4 corresponds to     methods.
the token-ring LAN identified at 1 in Fig. 1.         25  [O028]  As a further refinement of the invention, the
[O025]  Pane 9 in Fig. 4 includes icons suitable to se-     node and connection characteristics are preferably val-
Iect an OS12 operating system database requester     idated at the time they are specified in the workstation
workstation 1 7, an OS12 operating system database     used by the network administrator. Such validation will
server workstation 1 8, an OS12 operating system data-     likely include a comparison of workstation resources
base requester and server workstation 1 9, a DOS data-  30  and network requirements, a confirmation of unique-
base requester workstation 21, an OS12 or DOS oper-     ness in node addresses and names, a verification of
ating system requester (whichever is booted) worksta-     consistency between machine types and node functions
tion 22, a DOS requester with OS12 operating system     ascribed thereto, and a comparison of connection pro-
database server workstation 23, an OS12 operating sys-     tocols to workstation card functionality. Systematic and
tem requester and server with DOS requester worksta-  35  automated validation avoids the common prior eKperi-
tion 24, and a ''no function'' overlay 26. The ''no function''     ence of discovering configuration errors after configur-
overlay icon identifies nodes without specific functional-     ing all the workstations and enabling the network.
ity in the network mode, e.g., communication or data-     [O029]  Preferably validation eKtends to the whole of
base, being defined. Such a diversity of node capability     the network, thereby including all nodes and connec-
alone suggests a monumental effort for a network ad-  40  tions. Validation should be performed at the earliest op-
ministrator not having the resources of the present in-     portunity, e.g., byvalidating the structure of a node when
vention.                                 the node name is defined or by validating that two nodes
[O026]  The communication protocols available to link     can communicate with a given protocol when the con-
the node workstations are similarly diverse. The icons     nection is defined. Thus, the validation process is dis-
representing the various forms of connection protocol  45  tributed among the operations which can be performed
are depicted in pane 1 1 of Fig. 4. Icon 27 represents a     and invoked when first feasible. Validation also applies
no connection mode of operation, and invokes the mode     to the generation of configuration files, performed to en-
use when selecting and dragging node icons from pane     sure that an installation would be successful if undertak-
9 into workspace pane 8. Connection icon 28 represents     en.
a SQL LOO communication LAN. The APPC (advance  50  [O030]  The invention also contemplates the creation
program to program communication) network repre-     and ensuing distribution of the configuration files to the
sented by icon 29 corresponds to an IBM Corporation     respective node workstations. The comprehensive def-
SNA communication network. The network of icon 31     inition of the complete network, including a wide area
represents the combined capability of the APPC and the     network, allows for the immediate generation of config-
SQL LOO protocols. Icon 32 corresponds to a netbios  55  uration files for each of the node workstations. Files are
network, while network icon 33 represents a network     related to workstation directories. In a network having a
combining SQL LOO with netbios. Icon 34 represents a     requesterlserver capability, the present program and
network having APPC combined with netbios. Icon 36     process contemplates the automated and direct distri-
4
