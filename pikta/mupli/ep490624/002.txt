                           1             EP O 490 624 B1             2
Description                              greater numbers of nodes, diverse communication pro-
tocols, and different functional nodes, together with the
[OO01]  The present invention relates generally to the     bridging of multiple local area networks into wide area
configuration of data processing networks. More partic-     networks, has created an environment in which there is
ularly, the invention defines a computer system and  5  a need for graphically depicting networks having numer-
method of use for graphically defining a network and de-     ous nodes, for interactively specifying the connecting
riving configuration parameters for the node terminals     protocols between the nodes, and for automating the
of the network.                             configuration of each workstation in the network based
[OO02]  The configuration of multiple personal comput-     upon its capabilities, the nodes of operation defined for
ers and workstations into networks, and with increasing  IO  the workstation, and the protocols specified for such
frequency hierarchically ordered sets of networks, pro-     nodes of operation.
vides communication and information retention resourc-     [OOOl]  Hewlet-Packard Journal, vol. 41, no. 2, 1 990,
es not available to independent workstations. Conse-     pages 60-65 describes a graphics based user interface
quently, there eKists a significant trend toward network     for users of network management applications. The pre-
use of workstations. Unfortunately, the industy remains  15  ambles to the independent claims are based on this doc-
fluid as to network protocols and includes as prevailing     ument.
and representative communication networks Ethernet,     [OO08]  According to a first aspect of the present inven-
Token-Ring, PC Network (trademark of IBM Corpora-     tion, there is provided a system for generating configu-
tion), IEEE 802.2, Netbios, X.25, SDLC, and APPC. Fur-     ration parameters for a network of multiple workstations,
thermore, given that the network users eKpect database  20  comprising. means for graphically depicting a network
capability in addition to communication capability, data-     of three or more nodes on a workstation display using
base requester and database server functions in work-     icon-type objects for the nodes; characterised by further
stations and bridges are similarly subject to particulari-     comprising. means for graphically depicting path and
zation in the course of defining the composite network.     protocol connections relating the network nodes on the
Network definition is even further complicated by the  25  workstation display using graphical  representations
common use of multiple communication boards interfac-     which relate to selected icon-type objects; means for a
ing distinctly differing networks in one or more of the     user of the workstation display to interactively manipu-
node workstations.                          Iate by drag-drop manipulation the icon-type objects and
[OO03]  The configuration of the individual worksta-     connections as depicted on the workstation display; and
tions in networks to match communication and database  30  means for deriving network configuration parameters
protocols has routinely been the responsibility of a net-     from information represented by the combination of the
work administrator. As the number of network nodes, the     depicted icon-type objects and connections; and means
number of internetwork bridges and variety of network     for distributing the network configuration parameters.
protocols increase, it has become apparent that the net-     [OO09]  According to a second aspect of the present
work administrator needs computer assistance to define  35  invention, there is provided a method of generating con-
and revise networks, and to generate the appropriate     figuration parameters for a network of workstations,
configuration files for each of the workstations within the     comprising the steps of. generating icons for each of
network.                                three or more first network objects on a graphical video
[OO04]  U.S. Patent 4,864,492 recognized the need for     display; and characterised by further comprising. posi-
assisting a network administrator. The patent thus pro-  40  tioning by drag-drop manipulation icons for said first net-
vides a system and method for applying a knowledge     work objects; generating graphical representations of
based eKpert system to the creation of configuration pa-     paths and protocols connecting the first network objects
rameters individualized to the workstations of compleK     on the video display; generating configuration parame-
networks. The knowledge of the eKpert system is used     ters for configuring devices in a physical network as de-
to provide a menu and control the selections available  45  fined by a first combination of network objects and con-
to the network administrator.                     nections; and distributing the configuration parameters
[OO05]  Another reference of some relevance is U.S.     to the respective devices in the physical network.
Patent 4,942,540. The subject matter in that patent re-     [O01 O]  The present invention provides a system and
Iates to creating and selecting a communication path be-     method of operation by which a network administrator
tween a user's terminal and a destination terminal by  50  can, as a first feature, graphically depict a network by
selecting the communication parameters from a scrol-     defining a multiplicity of workstation nodes with respec-
Iable menu. Graphical representations of the terminals     tive hardware and operating system characteristics, can
and path are depicted in response to different menu se-     then define the protocols of the communication paths
Iections. Though network usage is noted, the teachings     between the network workstations, and based upon
relate to the definition of a communication path between  55  such network of workstations and communication path
a pair of terminals, namely, between the user's local ter-     constraints can thereafter generate configuration pa-
minal and a single remote terminal.                rameters for the operating systems of the respective
[OO06]  The increased prevalence of networks with     workstations. A further feature of the invention provides
2
