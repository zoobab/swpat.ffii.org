                           9             EP O 490 624 B1             1 O
A. Clear the current network(s).                B. If all data items entered OK...
B. Load the new network(s) into mem-
ory.                                   1. If data accepted by user...
C. Display the new network(s).       5               A. Put new node on screen. Add
E. Generate configuration files menu item.                    to data structure.
1. Generate all necessay files for all work-          J. Change view menu item.
stations.
2. If using LAN distribution...          IO          1. Change the screen view to the selected
V_leW.
A. Send files over LAN to proper work-
stations.                           K. Delete menu item.
B. Run a remote program on each
workstation to install the files.       15          1. Delete current nodelconnection from
C. Each workstation must be rebooted            screen memory.
to use changes.                       L. View a nodelconnection menu item.
3. Else...                      20          1. Put up view nodelconnection dialog.
A. Create one diskette for each work-
station.                            M. One of the default items menu items.
B. Put each diskette into proper work-
station.                               1. Put proper default dialog boK.
C. Run a program to install files at each  25         2. If data changed...
workstation.
D. Each workstation must be rebooted               A. If data is OK...
to use changes.                               1. Change the default values.
F. Click on a node or connection in the right  30
pane.                                  N. One of the options menu items.
1. Make that node or connection the ''cur-            1. Put up proper option dialog boK.
rent'' one (highlight it).                        2. If data changed...
35
G. Click on an icon below the line in the left               A. If data is OK...
pane.                                          1. Change the option values.
1. Change the connection mode to that
mode.                       40       O. Save options menu item.
H. Drag an icon from above the line in the left            1. Write options to options file.
pane.                                  P. Restore options menu item.
1. If icon ends up in right pane...        45          1. Read in and change options from op-
A. If all data items are auto-defaulted...            tions file.
1. Put new node on screen. Add to          Q. Drag a node in the right pane.
data structure.              50          1. If in a connection mode...
B. Else...                                A. Draw a rubber band line.
2. Goto main item l.                       B.  If end location is in a different
55            node...
l. Create a node menu item.                            1. If connection is valid...
A. Put up the create node dialog.        6
