                           1 3             EP O 592 062 B1             1 4
ferner Mittel zum Bestimmen, ob ein Besch�di-          messages, concernant la fa_on selon laquelle
gungsdatenpaket (70), das einem Datenstrom zu-          le message devrait etre comprim�;
gewiesen ist, in die Warteschlange zur �bertragung
�ber das Netzwerk gelegt wurde, und Mittel auf-          et le sous-syst�me de communications com-
weist, die auf diese Bestimmung mit dem Erstellen  5       prend des moyens r�agissant � ladite informa-
und lnitialisieren eines dem Datenstrom zugeord-          tion, pour traiter de mani�re correspondante le
neten Z�hlers (1 40) ansprechen.                    message.
8.  Der Computer-Arbeitsplatz gem�� Anspruch 6 bzw.     2.  La station de travail d'ordinateur selon la revendi-
7, der ferner Mittel zum Herausnehmen von Daten-  IO    cation 1, dans laquelle l'information ajout�e au mes-
paketen aus der Warteschlange und �bertragen       sage est une fonction de rappel automatique (92)
derselben �ber das Netzwerk je nach der M�glich-       incluse dans les moyens d'application pour compri-
keit zur Besch�digung dieser Pakete aufweist.          mer le message, et les moyens r�agissant � l'infor-
mation comprennent des moyens pour appeler la
9.  Der Computer-Arbeitsplatz gem�� einem beliebi-  15    fonction de rappel automatique.
gen der Anspr�che 6 bis 8, der ferner Mittel enth�lt
zum Aufrufen eines Zeit-aus-Zeitgebers beim Er-     3.  La station de travail d'ordinateur selon la revendi-
fassen eines Datenstroms, der keine Besch�di-       cation 2, dans laquelle les moyens d'appel de la
gungsdatenpakete aufweist.                     fonction de rappel automatique comprennent des
20    moyens pour faire passer de l'information sur la
1 O. Der Computer-Arbeitsplatz gem�� Anspruch 9, der       fonction de rappel automatique, afin de fournir une
ferner Mittel enth�lt, die auf den Ablauf des Zeitge-       certaine indication du fait qu'il est souhaitable de
bers ansprechen, um Betriebsmittel freizugeben,       compresser le message, et la fonction de rappel
die von einem Z�hler benutzt werden, der einem       automatique comprend des moyens destin�s � r�-
Datenstrom zugeordnet ist.                25    pondre � cette indication pour d�cider s'il faut ou
non comprimer le message.
1 1. Ein Verfahren zum Betreiben eines Computer-Ar-
beitsplatzes, der in ein Netzwerk eingebunden ist,     4.  La station de travail d'ordinateur selon la revendi-
wobei der Arbeitsplatz Applikationsmittel (1 2) und       cation 3, dans laquelle l'indication fournie � la fonc-
ein Kommunikations-Untersystem (1 4) beinhaltet,  30    tion de rappel automatique est une valeur de temps
wobei von dem Applikationsmittel generierte Mel-       (64) dans la limite de laquelle la compression de-
dungen (22) auf das Kommunikations-Untersystem       vrait etre effectu�e, et la fonction de rappel automa-
zur �bertragung auf das Netzwerk weitergegeben       tique comprend des moyens pour d�terminer si le
werden,                                 message peut etre comprim� dans les limites de la
dadurch gekennzeichnet, da�              35    valeur de temps et, dans la n�gative, pour retourner
Ie message au sous-syst�me de communications,
die Applikationsmittel lnformationen (26) zur       � l'�tat non-compress�.
Meldung hinzuf�gen, wie die Meldung kompri-
miert werden soll_,                      5.  La station de travail d'ordinateur selon la revendi-
und das Kommunikations-Untersystem auf die-  40    cation 1, dans laquelle le sous-syst�me de commu-
se lnformationen anspricht und die Meldungen       nications comprend des moyens pour comprimer
entsprechend bearbeitet.                    Ies donn�es, selon une ou plusieurs technique(s)
de compression (40) diff�rente(s) et l'information
ajout�e auK messages indique quelle technique de
Revendications                         45    compression est la mieuK appropri�e pour ce mes-
S�ge.
1.  Une station de travail d'ordinateur, pour assurer la
conneKion dans un r�seau, ladite station de travail     6.  La station de travail d'ordinateur selon l'une quel-
comprenant des moyens d'application (1 2) et un       conque des revendications pr�c�dentes, dans la-
sous-syst�me de communications (1 4), de mani�re  50    quelle le sous-syst�me de communications classe
que des messages (22), g�n�r�s par des moyens       des messages en vue d'une transmission, dans une
d'application, soient pass�s au sous-syst�me de       file d'attente (50), et le sous-syst�me de communi-
communications (1 4) pour effectuer une transmis-       cations comprend en outre des moyens r�agissant
sion sur le r�seau,                          � l'information ajout�e auK messages, pour d�ter-
caract�ris� en ce que.                   55    miner s'ils sont susceptibles d'etre susceptibles de
subir une alt�ration de donn�es.
Ies moyens d'application comprennent des
moyens pour ajouter de l'information (26) auK     l.  La station de travail d'ordinateur selon la revendi-
8
