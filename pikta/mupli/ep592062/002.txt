                           1             EP O 592 062 B1             2
Description                              [OO04]  A central aspect of such real-time interactive
applications is the need for rapid communications over
[OO01]  Over the last few years it has become increas-     the network. For eKample, in collaborative processing,
ingly common to connect personal computers or work-     any modifications to the display on which the shared
stations together into networks so that different ma-  5  program is actually running (for eKample a change in a
chines can communicate with one another. A typical ap-     graph) must appear practically simultaneously on the
proach has been the introduction of local area networks     screens of all the other workstations involved. Likewise
(LANs) that provide file transfer and other facilities be-     in video conferencing, it is essential for a video image
tween computers. Other types of link, for eKample lnte-     to be displayed as soon as possible after capture on all
grated Services Digital Network (ISDN), are also known.  IO  the screens. Too large a delay overthe network removes
Most such digital networks are based on packet switch-     the spontaneity of collaborative remote terminal, as op-
ing, in which data is broken down into packets or seg-     posed to being processed at that workstation (an impor-
ments before transmission. Each segment has its own     tant eKception to the above is in video conferencing, in
identification, address information, and so on to allow it     which the volumes of data generated are so large that
to be transmitted across the network. On receipt, this  15  hardware compression techniques are almost always
address information etc is stripped out (and verified), to     used automatically).
allow the original data to be reconstituted.             [OO05]  As a result, the communication subsystem re-
[OO02]  Modern networks have allowed the develop-     ceives uncompressed data from most applications. The
ment of facilities that provide real-time interaction be-     communications subsystem can then decide, based on
tween users. One eKample of this is collaborative work-  20  its knowledge of the links over which the data is to be
ing as described in EPA 475581, in which users of two     transmitted, whether or not it would be quicker to com-
or more different machines work simultaneously and in     press the data. However, a problem arises here in that
combination on a single program (for eKample a spread-     the communications subsystem is designed to be inde-
sheet). This program will be running on just one of the     pendent of the application, so that it knows very little
machines, but its output will be displayed at the other  25  about the data that is passed to it from the application.
workstations, so that multiple users can interact with the     ln particular, it cannot determine which compression
program. Another eKample of a real-time interactive fa-     technique is most suitable for that particular data, or in-
cility would be video conferencing, in which a video im-     deed whether the data has already been compressed
age of a user captured at one terminal appears on the     (for eKample in the case of video conferencing), in which
screen at a second terminal, and vice versa.        30  case any further attempt at compression is likely to be
[OO03]  ln general a workstation involved in such col-     counter-productive.
Iaborative processing or video conferencing includes a     [OO06]  Thus the application which knows about the
communication subsystem separate from the user ap-     data does not know about the network, and the commu-
plication, to handle the transmission and receipt of data     nications subsystem, which does know about the net-
to and from other terminals in the network. The applica-  35  work, does not know about the data. This presents a
tion and the communications subsystem interact via a     problem for the efficient management of such a digital
predefined interface. Thus the application passes mes-     data system, in that it is unclear when and how to com-
sages for transmission to the communication subsys-     press data, whilst maintaining the desired independ-
tem, which is responsible for actually sending them over     ence of the application and communications subsystem.
the network, and likewise incoming messages are re-  40  [OOOl]  W090I05422  discloses  a  digital  image
ceived first by the communication subsystem before be-     processing and display system which supports multiple
ing forwarded to the application. For eKample, the ap-     compression algorithms. A type field is added to the
plication may simply request that a message be trans-     compressed data to indicate which compression algo-
mitted to another user ''FRED'' (for eKample), leaving the     rithm has been used for that data.
communication subsystem to be responsible for identi-  45  [OO08]  Accordingly, the invention provides a computer
fying ''FRED'' with a valid network address and for con-     workstation for connection into a network, said worksta-
verting the message into correctly-sized packets com-     tion including application means and a communications
plete with address information and so on. The purpose     subsystem, whereby messages generated by the appli-
of such a split between the application and the commu-     cation means are passed to the communications sub-
nication subsystem is to allow the application to be as  50  system for transmission onto the network, and charac-
independent as possible from any details about the net-     terised in that.
work itself. It is therefore possible (in theory at least) to
run the same application on many different networks,       the application means includes means for adding
providing that the communication subsystems used of-       information to the message concerning how the
fer the same interface. Likewise, the communication  55    message should be compressed;
subsystem is designed to be independent of any one       and  the  communications  subsystem  includes
particular application, but rather can be used with a va-       means responsive to said information for process-
riety of applications.                            ing the message accordingly.
2
