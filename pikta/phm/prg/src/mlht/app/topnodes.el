;; -*- coding: utf-8 -*-
;; Time-stamp: <2000-04-26 18:19:47 phm>
;; buffer-file-coding-system save-buffer-coding-system 

(set-default 'save-buffer-coding-system nil)
(message "requiring %s" 'el2html)
(require 'el2html)
(message "provided %s" 'el2html)

(progn (message "topnodes body")
(setq mlht-mail-server "mlhtimport@a2e.de")

(list
(setq wwwdir (or (getenv "WWW") "/ul/sig/srv/res/www"))
(setq ffiidir (or (getenv "FFII") (expand-file-name "ffii" wwwdir)))
(setq a2edir (or (getenv "A2E") (expand-file-name "a2e" wwwdir)))
(setq lrzdir (or (getenv "LRZ") (expand-file-name "lrz" wwwdir)))
(setq localhost "localhost")
(mapcar (lambda (sym) (put sym 'imgfmt 'png)) '(swpat swpatdb ffii li18nux wortbasar a2edoku susebuch))
(progn
 (putverk 'phm '(mlk "PILCH Hartmut" (wd "裴寒牧") (gb "裴寒牧") (ja "ピルヒ・ハルトムート")) 'phm "phm@a2e.de" '(pgp . pgpphm) '(adr . "DE 80636 Blutenburgstr. 17")) 
 (putverk 'seyfertd "Dirk Seyfert" "http://synexpo.a2e.de" "seyfertd@aol.com")
)
)

(progn
(nodetit 'a2e (xq (mlc "A2E" (zh "亞通歐") (ja "亞通歐"))) '((zh-lkod . wd) (docstyle . eulux-style) (logo . a2elogo) (htdig . t) (verk . a2e) (extract-text . t) (imgfmt . gif)))
(nodetit 'ning (xq (mlc "Chinareisen Ning" (en "Ning's China Travel Agency") (zh "寧氏 中國 旅游 公司"))) '((lingvoj de zh) (verk . ning) (docstyle . eulux-style) (logo . ninglogo) (imgfmt . gif)))
(nodetit 'sauseng (xq (mlc "Hans Sauseng" (de "Hans Sausengs China-Dias") (zh "韓壽森 絲綢之路 遊記"))) '((extra-arrows . t)))
(nodetit 'disheng (xq (mlc "Disheng e.V." (zh "棣生協會") (ja "棣生協会"))))
(nodetit 'yuli (xq (mlc "YuLi Asia Gesundheitsartikel Import & Export GmbH" (zh "余李亚洲保健品进出口公司"))) '((lingvoj de zh) (zh-lkod . gb)))
(nodetit 'qigong (xq (mlc "Qigong-Zentrum München" (en "Munich Qigong Center") (zh "寧氏慕尼黑氣功中心"))) '((lingvoj de zh) (verk . qigong)))
(nodetit 'synexpo (xq (mlc "Synexpo Messe-Service" (zh "綜展公司"))) '((verk . seyfertd) (lingvoj de en fr zh) (dir . t)))
(nodetit 'st-johanser (xq (mlc "St-Johanser" (zh "聖 約瀚 宗") (ja "聖ヨハネ宗"))) '((verk . oas) (lingvoj de en es zh ja) (dir . t) (logo . a2elogo) (docstyle . eulux-style)))
(nodetit 'euchi (xq (mlc "EU-CHI Europäisch-Chinesische Gesellschaft für Stilles Qigong e.V." (zh "歐華 氣功 協會"))) '((lingvoj de zh) (verk . euchi)))

(nodetit 'qingpu (xq (mlc "Shanghai Qingpu" (zh "上海 青浦 工業 園區") (ja "上海青浦工業園区"))) '((verk . qingpu) (lingvoj de en fr eo zh ja)))
(verk-nodetit 'kito (xq (mlc "KITO s.r.o" (ja "キット社"))) "kito@a2e.de" '((verk . milan) (lingvoj de) (dir . t)))
(nodesub (nodeput 'mlht '((lingvoj de en zh eo fr ja da) (dir . t) (topdoc . t)))
  (nodesub (nodeput 'mlhtdemo '((lingvoj eo de en fr zh ja) (dir . t) (name . demo))) 
   (nodeput 'anybr '((lingvoj en zh eo)  (dir . t)))
   (nodeput 'gpl '((lingvoj en fr de zh ja) (zh-lkod . gb))) )
  (nodeput 'langtxt '((lingvoj eo de en fr zh ja in) (attrib privat))) )
)

(progn
(message "init server topnodes")
(init-server-topnode 'a2e (list localhost "www.a2e.de") (list "a2e" nil) (subdirs a2edir "www" "www") 'a2e '(de en fr eo zh ja) nil '(img)) 
(init-server-topnode 'mlht (list localhost "mlht.ffii.org") "mlht" (subdirs ffiidir "mlht") 'phm '(de) nil '(img)) 
(init-server-topnode 'wortbasar (list localhost "wortbasar.ffii.org") "wortbasar" (subdirs ffiidir "wortbasar") 'phm '(de) nil '(img))
(init-server-topnode 'disheng (list localhost "disheng.a2e.de") (list "disheng" nil) (subdirs a2edir "disheng" "www") 'oas '(de eo en zh ja) nil '(img))
(init-server-topnode 'sauseng (list localhost "sauseng.a2e.de") (list "sauseng" nil) (subdirs a2edir "sauseng" "www") 'sauseng '(de en zh) nil '(img))
(init-server-topnode 'yuli (list localhost "yuli.a2e.de") (list "yuli" nil) (subdirs a2edir "yuli" "www") 'yuli '(de zh) nil '(img))
(init-server-topnode 'ning (list localhost "ning.a2e.de") (list "ning" nil) (subdirs a2edir "ning" "www") 'ning '(de zh) nil '(img))
(init-server-topnode 'qigong (list localhost "qigong.a2e.de") (list "qigong" nil) (subdirs a2edir "qigong" "www") 'qigong '(de zh) nil '(img))
(init-server-topnode 'qingpu (list localhost "qingpu.a2e.de") (list "qingpu" nil) (subdirs a2edir "qingpu" "www") 'qingpu '(de en fr eo zh ja) nil '(img))
(init-server-topnode 'euchi (list localhost "euchi.a2e.de") (list "euchi" nil) (subdirs a2edir "euchi" "www") 'euchi '(de zh) nil '(img))
(init-server-topnode 'disheng (list localhost "disheng.a2e.de") (list "disheng" nil) (subdirs a2edir "disheng" "www") 'disheng '(de eo en fr zh) nil '(img))
(init-server-topnode 'synexpo (list localhost "synexpo.a2e.de") (list "synexpo" nil) (subdirs a2edir "synexpo" "www") 'seyfertd '(de en fr zh) nil '(img))
(init-server-topnode 'st-johanser (list localhost "st-johanser.a2e.de") (list "st-johanser" nil) (subdirs a2edir "st-johanser" "www") 'seyfertd '(de en fr zh) nil '(img))
(init-server-topnode 'kito (list localhost "kito.a2e.de") (list "kito" nil) (subdirs a2edir "kito" "www") 'kito '(de zh) nil '(img))
(init-server-topnode 'ssmpatent (list localhost "ssm.a2e.de") (list "ssm" nil) (subdirs a2edir "ssm" "www") 'ssmpatent '(de en ja) nil '(img))
(init-server-topnode 'phm (list localhost "www.ffii.org") "~phm" (subdirs ffiidir "phm") 'phm '(de en fr eo zh ja) nil '(img))
(init-server-topnode 'tao (list localhost "www.a2e.de") "~tao" (subdirs a2edir "tao" "www") 'tao '(de en zh) nil '(img))
(init-server-topnode 'ffii (list localhost "www.ffii.org") "ffii" (subdirs ffiidir "www") 'phm '(de en fr eo zh ja) nil '(img))
(init-server-topnode 'kongress (list localhost "kongress.ffii.org") "kongress" (subdirs ffiidir "kongress") 'phm '(de en fr eo) nil '(img))
(init-server-topnode 'swpat (list localhost "swpat.ffii.org") "swpat" (subdirs ffiidir "swpat") 'phm '(de en fr) nil '(img))
(init-server-topnode 'swpatdb (list localhost "swpatdb.ffii.org") "swpatdb" (subdirs ffiidir "swpatdb") 'phm '(en) nil '(img))
(init-server-topnode 'mulix (list localhost "mulix.ffii.org") "mulix" (subdirs ffiidir "mulix") 'phm '(de) nil '(img))
(init-server-topnode 'egeld (list localhost "egeld.ffii.org") "egeld" (subdirs ffiidir "egeld") 'phm '(de) nil '(img))
(init-server-topnode 'li18nux (list localhost "www.li18nux.org") "li18nux" (subdirs ffiidir "li18nux" "www") 'phm nil nil '(img))
(init-server-topnode 'eurolinux (list localhost "www.eurolinux.org") "eurolinux" (subdirs ffiidir "eurolinux") 'eurolinux nil nil '(img))
(message "init server topnodes done")
)

;; server-topnodes
;; (ahs 'tra)

(defun def-pgpkey-doc (doc upnode) (mlc "define DOC as the name of a document under node UPNODE to be generated by PUBLISH-PGPKEY" (zh "將 DOC 定義為 UPNODE 上層紐 下面 用 PUBLISH-PGPKEY 所生成 的 一個 文件 的 名字.")) (nodetit doc (mlc "PGP" (zh "我的簽名鑰匙") (ja "私の署名用公開鍵")) (cons (cons 'upnode upnode) '((name . pgpkey) (lingvoj en de fr eo zh ja)))))

(progn (message "images")
(nodesub (nodetit 'img nil '((dir . t)))
  (nodetit 'flags nil '((dir . t)))
  (nodesub (nodetit 'logos nil '((dir . t)))
   (nodetit 'anybrlogo nil '((name . anybr) (app . png)))
   (nodetit 'euluxlogo nil '((name . eurolinux) (app . jpg)))
   (nodetit 'mulixlogo nil '((name . mulix) (app . jpg)))
   (nodetit 'a2elogo nil '((name . a2e) (app .png))) )
  (nodesub (nodetit 'icons nil '((dir . t) (name . "icons.other")))
    (nodetit 'texticon nil '((name . txt) (app . png))) )
  (nodetit 'arrows nil '((dir . t)))
  (nodetit 'backgrounds nil '((dir . t) (name . bakgr)))
  (nodesub (nodetit 'lines nil '((dir . t)))
   (nodetit 'blulini nil '((name . blu) (app . jpg))) ) ) 

(progn
(apply 'graphic-files 'arrows 'png (append '(uarr darr larr rarr) '(uarrb darrb larrb rarrb) '(uarr3 darr3 larr3 rarr3) '(udarr lrarr uldrarr dlurarr udlrarr) '(uarro darro larro rarro ularro) '(ularrm ularr3d rdarr) '(uarrh darrh larrh rarrh rarrh2) '(uarrc darrc larrc rarrc)))
(setq logo-node 'logos)
(apply 'logo-files 'png '(ffii suse intradat intevation infomatec mandrake ning prosa opendvd centerpd netpresenter mysql osslaw))
(apply 'graphic-files 'logos 'png '(multilingual enhanced msfree bsdevil teamos2 ffiirj openhwl vwdseu swpatux swpatcd  klaresprache_verd klaresprache_bla klaresprache_ref klaresprache_blaref sprachsalat noepat_brick noepat_square))
(apply 'graphic-files 'logos 'gif '(sprachsalat_nd klaresprache klaresprachexu sprachgulasch sprachpansch vwds_rosa noepat_banner))
(apply 'logo-files 'jpg '(rapi freepatents aful april freepatents lpf onlkiosk strixner))
(apply 'graphic-files 'logos 'jpg '(philosophical-gnu-sm linuxpeng tux ffiirjtxt smetslivre nopatents globtux sprachlinse wortbasar_klar wortbasar_vwds basarworte netpresenterbanner))
(apply 'graphic-files 'backgrounds 'jpg '(blublank))
(apply 'graphic-files 'backgrounds 'png '(wortbasarbg))
(apply 'graphic-files 'lines 'jpg '(blulini))
(apply 'graphic-files 'flags 'jpg '(eo7 eo8 jp-jis jp-sjis))
(apply 'graphic-files 'icons 'png '(questionmark construction original printer xgmail folder gotodoc))
;; (absurl 'philosophical-gnu-sm)
;; (absurl 'uarrc)
;; (absurl 'a2elogo)
;; (absurl 'printer)
)

(progn
 (defun msfree () (let ((border nil)) (lin (ah (lokurl 'anybr) (img (lokurl 'anybrlogo) (ml "any browser" (de "jeder Browser") (eo "iu ajn krozilo") (fr "n'importe quel fureteur") (zh "任何先生瀏覽器") (ja "ドチラデモ社のブラウザー")))) (ah (relurl 'microsoft) (img (lokurl 'msfree) (ml "proud to be 100% MS free" (de "100% MS-freies System") (eo "sen-MS-a libera sistemo") (zh "無 微軟 自由 系統") (ja "マイクロソフト無し自由システム")))))))

 (defun fartab (&rest rec) "Aufgabentabelle" (table '((border 1)) '((nom) (xpl) (hom) (res)) (l (l (ml "Aufgabe" (en "task")) (ml "Beschreibung" (en "description")) (ml "Wer?" (en "who?")) (ml "Stand" (en "status")))) rec))

 (defun @doknom (str) (concat "@" (string str) "-" (string docid))) ) 
 (message "images done") )
 (message "topnodes body done")
)

(provide 'topnodes) 

(message "provided %s" 'topnodes)

;; (alget 'mlht-passwords 'phm)