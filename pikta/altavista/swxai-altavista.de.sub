\begin{subdocument}{swxai-altavista}{Keine WWW-Indexierung mehr ohne Erlaubnis von CMGI?}{http://swpat.ffii.org/patente/altavista/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Im Januar 2001 erkl\"{a}rte der Chef der Firma, die Altavista gekauft hat, Altavista besitze ca 50 Patente auf grundlegende Prinzipien, ohne die zu verletzen niemand das WWW indexieren k\"{o}nne, und k\"{u}ndigte an, in den kommenden Monaten Firmen, die das Internet oder auch ihr Intranet indexieren, zur Kasse und notfalls vor Gericht bitten zu wollen, um den maximalen Gewinn aus diesen Patenten herauszuholen.}
\begin{sect}{press}{Der Startschuss}
Am Tage 2000-11-13 gab der Konzern CGMI, der Altavista und 12 weitere Firmen besitzt, in einer stark beworbenen Pressemitteilung\footnote{http://biz.yahoo.com:80/prnews/001113/ca\_altavis.html} bekannt, dass er 38 Patente auf wesentliche Elemente der Netzindexierung besitzt.  Zu diesem Zeitpunkt schrieb CGMI rote Zahlen und versuchte, bei Investoren um Zuversicht zu werben.  Am Tage 2000-01-15 erkl\"{a}rte der Chef von CGMI in einem Interview\footnote{http://www.internetworld.com/011501/01.15.01interview.jsp} dar\"{u}ber hinaus, dass er die Verletzer seiner Patente in K\"{u}rze zu verfolgen gedenke und dass praktisch jeder, der das Internet oder auch nur sein firmeninternes Intranet absuche und indexiere, zu diesen Verletztern geh\"{o}re:

\begin{quote}
\begin{description}
\item[Internet World:]\ Can we talk a bit about some of the ideas or opportunities that you backed off of because money was an issue this past year?
\item[David Wetherell:]\ [...]
Even though AltaVista s doing well in the advertising space, we just think that in order to really ensure strong growth they ought to leverage their position in search licensing to a greater extent. And we saw the opportunity to do that because we think it s a big market. They happen to own 38 patents, many of which we think are fundamental in the search area. They were the first to spider and index the Web. And Digital did a good job of recognizing the potential value of that intellectual property. And they were very thorough in filing broad and deep and narrow patents. And we have another 30 patents that are in application. So we believe that virtually everyone out there who indexes the Web is in violation of at least several of those key patents.
\item[IW:]\ Does that mean you ll pursue that?
\item[DW:]\ Yes, we will. Coming up in the first quarter of 2001.
\item[IW:]\ So we may see some lawsuits ...
\item[DW:]\ If necessary, we will defend it, to the letter of the law.
\item[IW:]\ Are there any specific examples of the types of patents?
\item[DW:]\ If you index a distributed set of databases what the Internet is and even within intranets, corporations, that s one of the patents.  We did a press release\footnote{http://biz.yahoo.com:80/prnews/001113/ca\_altavis.html} on this with a list of six or ten of the key areas that the patents cover.
\end{description}
\end{quote}
\end{sect}

\begin{sect}{patents}{Einige der Patente}
Die meisten europ\"{a}ischen Antr\"{a}ge warten beim EPA noch auf ihre Erteilung.

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf US 5864863\footnote{http://www.delphion.com/details?\&pn=US05864863\_\_}}}

\begin{quote}
I claim
\begin{enumerate}
\item
A system for indexing Web pages of the Internet, the pages stored in computers connected to each other by a communications network, each page having a unique URL (universal record locator), some of the pages including URL links to other pages, comprising: a communication interface for fetching a batch of specified pages of the Web from the computers in accordance with the URLs and URL links;\\
a parser sequentially partitioning the batch of specified pages into indexable words, each word representing a portion of one specified page or an attribute of one or more portions of the specified page, the parser sequentially assigning locations to the words as they are parsed;\\
a memory storing index entries, each index entry including a word entry representing a unique one of the words, and one or more location entries indicating where the unique word occurs in the Web;\\
a query module parsing a query into terms and operators relating the terms;\\
a search engine using object-oriented stream readers to sequentially read location of specified index entries, the specified index entries corresponding to the terms of a query;\\
and a display module for presenting qualified pages located by the search engine to users of the Web.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf US 5974455\footnote{http://www.delphion.com/details?\&pn=US05974455\_\_}}}

\begin{quote}
\begin{enumerate}
\item
A system for locating Web pages stored on remotely located computers, each Web page having a unique URL (universal resource locator), at least some of said Web pages including URL links to other ones of the Web pages, the system comprising:
\begin{itemize}
\item
a communications interface for fetching specified ones of the Web pages from said remotely located computers in accordance with corresponding URLs;

\item
a Web information file having a set of entries, each entry denoting, for a corresponding Web page, a URL and fetch status information;

\item
a Web information table, stored in RAM (random access memory), having a set of entries, each entry denoting a fingerprint value and fetch status information for a corresponding Web page; and

\item
a Web scooter procedure, executed by the system, for fetching and analyzing Web pages, said Web scooter procedure including instructions for fetching Web pages whose Web information file entries meet predefined selection criteria based on said fetch status information, for determining for each URL link in each received Web page whether a corresponding entry already exists in the Web information table, and for each URL link which does not have a corresponding entry in the Web information table adding a new entry in the Web information table and a corresponding new entry in the Web information file.
\end{itemize}
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf EP 444358\footnote{../txt/EP} = US 5495608\footnote{http://www.delphion.com/details?\&pn=US05495608\_\_}}}

\begin{quote}
Dynamic optimization of a single relation access.
\end{quote}
\filbreak

\item
{\bf {\bf EP 458698\footnote{../txt/EP} = US 5276868\footnote{http://www.delphion.com/details?\&pn=US05276868\_\_}}}

\begin{quote}
Method and apparatus for pointer compression in structured
\end{quote}
\filbreak

\item
{\bf {\bf EP 520459\footnote{../txt/EP} = US 5347653\footnote{http://www.delphion.com/details?\&pn=US05347653\_\_}}}

\begin{quote}
A method and apparatus for indexing and retrieval of object versions in a versioned data base.
\end{quote}
\filbreak

\item
{\bf {\bf EP 522363\footnote{../txt/EP} = US 5204958\footnote{http://www.delphion.com/details?\&pn=US05204958\_\_}}}

\begin{quote}
System and method for efficiently indexing and storing large database with high-data insertion frequency.
\end{quote}
\filbreak

\item
{\bf {\bf EP 551243\footnote{../txt/EP} = US 5519858\footnote{http://www.delphion.com/details?\&pn=US05519858\_\_}}}

\begin{quote}
Address recognition engine.
\end{quote}
\filbreak

\item
{\bf {\bf EP 567355\footnote{../txt/EP} (Kombination aus 5 US-Anmeldungen)}}

\begin{quote}
A method and apparatus for operating a multiprocessor computer system having cache memories.
\end{quote}
\filbreak

\item
{\bf {\bf EP 886227\footnote{../txt/EP}}}

\begin{quote}
Full-text indexed mail repository
\end{quote}
\filbreak

\item
{\bf {\bf EP 886228\footnote{../txt/EP}}}

\begin{quote}
WWW-based mail service system
\end{quote}
\filbreak

\item
{\bf {\bf US 5226150\footnote{http://www.delphion.com/details?\&pn=US05226150\_\_}}}

\begin{quote}
Apparatus for suppressing an error report from an address for which an error has already been reported
\end{quote}
\filbreak

\item
{\bf {\bf US 5276872\footnote{http://www.delphion.com/details?\&pn=US05276872\_\_}}}

\begin{quote}
Concurrency and recovery for index trees with nodal updates using multiple atomic actions by which the trees integrity is preserved during u 
\end{quote}
\filbreak

\item
{\bf {\bf US 5276874\footnote{http://www.delphion.com/details?\&pn=US05276874\_\_}}}

\begin{quote}
Method for creating a directory tree in main memory using an index file in secondary memory
\end{quote}
\filbreak

\item
{\bf {\bf US 5394143\footnote{http://www.delphion.com/details?\&pn=US05394143\_\_}}}

\begin{quote}
Run-length compression of index keys
\end{quote}
\filbreak

\item
{\bf {\bf US 5506984\footnote{http://www.delphion.com/details?\&pn=US05506984\_\_}}}

\begin{quote}
Method and system for data retrieval in a distributed system using linked location references on a plurality of nodes
\end{quote}
\filbreak

\item
{\bf {\bf US 5553258\footnote{http://www.delphion.com/details?\&pn=US05553258\_\_}}}

\begin{quote}
Method and apparatus for forming an exchange address for a system with different size caches
\end{quote}
\filbreak

\item
{\bf {\bf US 5671406\footnote{http://www.delphion.com/details?\&pn=US05671406\_\_}}}

\begin{quote}
Data structure enhancements for in-place sorting of a singly linked list
\end{quote}
\filbreak

\item
{\bf {\bf US 5717921\footnote{http://www.delphion.com/details?\&pn=US05717921\_\_}}}

\begin{quote}
Concurrency and recovery for index trees with nodal updates using multiple atomic actions
\end{quote}
\filbreak

\item
{\bf {\bf US 5745890\footnote{http://www.delphion.com/details?\&pn=US05745890\_\_}}}

\begin{quote}
Sequential searching of a database index using constraints on word-location pairs 
\end{quote}
\filbreak

\item
{\bf {\bf US 5745894\footnote{http://www.delphion.com/details?\&pn=US05745894\_\_}}}

\begin{quote}
Method for generating and searching a range-based index of word-locations
\end{quote}
\filbreak

\item
{\bf {\bf US 5745898\footnote{http://www.delphion.com/details?\&pn=US05745898\_\_}}}

\begin{quote}
Method for generating a compressed index of information of records of a database
\end{quote}
\filbreak

\item
{\bf {\bf US 5745899\footnote{http://www.delphion.com/details?\&pn=US05745899\_\_}}}

\begin{quote}
Method for indexing information of a database
\end{quote}
\filbreak

\item
{\bf {\bf US 5745900\footnote{http://www.delphion.com/details?\&pn=US05745900\_\_}}}

\begin{quote}
Method for indexing duplicate database records using a full-record fingerprint
\end{quote}
\filbreak

\item
{\bf {\bf US 5765158\footnote{http://www.delphion.com/details?\&pn=US05765158\_\_}}}

\begin{quote}
Method for sampling a compressed index to create a summarized index
\end{quote}
\filbreak

\item
{\bf {\bf US 5765168\footnote{http://www.delphion.com/details?\&pn=US05765168\_\_}}}

\begin{quote}
Method for maintaining an index
\end{quote}
\filbreak

\item
{\bf {\bf US 5787435\footnote{http://www.delphion.com/details?\&pn=US05787435\_\_}}}

\begin{quote}
Method for mapping an index of a database into an array of files
\end{quote}
\filbreak

\item
{\bf {\bf US 5794242\footnote{http://www.delphion.com/details?\&pn=US05794242\_\_}}}

\begin{quote}
Temporally and spatially organized database
\end{quote}
\filbreak

\item
{\bf {\bf US 5797008\footnote{http://www.delphion.com/details?\&pn=US05797008\_\_}}}

\begin{quote}
Memory storing an integrated index of database records
\end{quote}
\filbreak

\item
{\bf {\bf US 5809502\footnote{http://www.delphion.com/details?\&pn=US05809502\_\_}}}

\begin{quote}
Object-oriented interface for an index
\end{quote}
\filbreak

\item
{\bf {\bf US 5829051\footnote{http://www.delphion.com/details?\&pn=US05829051\_\_}}}

\begin{quote}
Apparatus and method for intelligent multiple-probe cache allocation
\end{quote}
\filbreak

\item
{\bf {\bf US 5832500\footnote{http://www.delphion.com/details?\&pn=US05832500\_\_}}}

\begin{quote}
Method for searching an index
\end{quote}
\filbreak

\item
{\bf {\bf US 5852820\footnote{http://www.delphion.com/details?\&pn=US05852820\_\_}}}

\begin{quote}
Method for optimizing entries for searching an index
\end{quote}
\filbreak

\item
{\bf {\bf US 5864863\footnote{http://www.delphion.com/details?\&pn=US05864863\_\_}}}

\begin{quote}
Method for parsing, indexing and searching world-wide-web pages
\end{quote}
\filbreak

\item
{\bf {\bf US 5915251\footnote{http://www.delphion.com/details?\&pn=US05915251\_\_}}}

\begin{quote}
Method and apparatus for generating and searching range-based index of word locations
\end{quote}
\filbreak

\item
{\bf {\bf US 5953747\footnote{http://www.delphion.com/details?\&pn=US05953747\_\_}}}

\begin{quote}
Apparatus and method for serialized set prediction
\end{quote}
\filbreak

\item
{\bf {\bf US 5956758\footnote{http://www.delphion.com/details?\&pn=US05956758\_\_}}}

\begin{quote}
Method for determining target address of computed jump instructions in executable programs
\end{quote}
\filbreak

\item
{\bf {\bf US 5963954\footnote{http://www.delphion.com/details?\&pn=US05963954\_\_}}}

\begin{quote}
Method for mapping an index of a database into an array of files
\end{quote}
\filbreak

\item
{\bf {\bf US 5966710\footnote{http://www.delphion.com/details?\&pn=US05966710\_\_}}}

\begin{quote}
Method for searching an index
\end{quote}
\filbreak

\item
{\bf {\bf US 5966735\footnote{http://www.delphion.com/details?\&pn=US05966735\_\_}}}

\begin{quote}
Array index chaining for tree structure save and restore in a process swapping system
\end{quote}
\filbreak

\item
{\bf {\bf US 5970497\footnote{http://www.delphion.com/details?\&pn=US05970497\_\_}}}

\begin{quote}
Method for indexing duplicate records of information of a database
\end{quote}
\filbreak

\item
{\bf {\bf US 5987544\footnote{http://www.delphion.com/details?\&pn=US05987544\_\_}}}

\begin{quote}
System interface protocol with optional module cache
\end{quote}
\filbreak

\item
{\bf {\bf US 6016493\footnote{http://www.delphion.com/details?\&pn=US06016493\_\_}}}

\begin{quote}
Method for generating a compressed index of information of records of a database
\end{quote}
\filbreak

\item
{\bf {\bf US 6021409\footnote{http://www.delphion.com/details?\&pn=US06021409\_\_}}}

\begin{quote}
Method for parsing, indexing and searching world-wide-web pages
\end{quote}
\filbreak

\item
{\bf {\bf US 6029164\footnote{http://www.delphion.com/details?\&pn=US06029164\_\_}}}

\begin{quote}
Method and apparatus for organizing and accessing electronic mail messages using labels and full text and label indexing
\end{quote}
\filbreak

\item
{\bf {\bf US 6047286\footnote{http://www.delphion.com/details?\&pn=US06047286\_\_}}}

\begin{quote}
Method for optimizing entries for searching an index
\end{quote}
\filbreak

\item
{\bf {\bf US 6067543\footnote{http://www.delphion.com/details?\&pn=US06067543\_\_}}}

\begin{quote}
Object-oriented interface for an index
\end{quote}
\filbreak

\item
{\bf {\bf US 6078923\footnote{http://www.delphion.com/details?\&pn=US06078923\_\_}}}

\begin{quote}
Memory storing an integrated index of database records
\end{quote}
\filbreak

\item
{\bf {\bf US 6092101\footnote{http://www.delphion.com/details?\&pn=US06092101\_\_}}}

\begin{quote}
Method for filtering mail messages for a plurality of client computers connected to a mail service system
\end{quote}
\filbreak

\item
{\bf {\bf US 6105019\footnote{http://www.delphion.com/details?\&pn=US06105019\_\_}}}

\begin{quote}
Constrained searching of an index
\end{quote}
\filbreak

\item
{\bf {\bf US 6108770\footnote{http://www.delphion.com/details?\&pn=US06108770\_\_}}}

\begin{quote}
Method and apparatus for predicting memory dependence using store sets
\end{quote}
\filbreak

\item
{\bf {\bf US 6112203\footnote{http://www.delphion.com/details?\&pn=US06112203\_\_}}}

\begin{quote}
Method for ranking documents in a hyperlinked environment using connectivity and selective content analysis
\end{quote}
\filbreak

\item
{\bf {\bf US 6138113\footnote{http://www.delphion.com/details?\&pn=US06138113\_\_}}}

\begin{quote}
Method for identifying near duplicate pages in a hyperlinked database
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{etc}{Weitere Lekt\"{u}re}
\begin{itemize}
\item
Wired: CMGI Claims Patently Wrong\footnote{http://www.wired.com/news/technology/0,1282,41508,00.html?tw=wn20010131}

\item
Businesswire: archie creator willing to challenge CMGI claims\footnote{http://www.businesswire.com/webbox/bw.012901/210290071.htm}
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: mlatex ;
% End: ;

