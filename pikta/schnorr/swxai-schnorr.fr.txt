<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Les algorithmes cryoptographique ont empeché longtemps la diffusion de techniques de signature digitale etc. Il a etait difficile d'etablir des alternatives au méthodes brevetés autour de RSA. Les alternatives ont subit des attaques juridiques par des professionaux exploitateurs de brevets comme PKP, qui affirmaient que tout algorithmes crypto tombe sous ses revendication autour de RSA et d'autres brevets dans leur portfolio, surtout celui du professeur allemand Schnorr.
title: RSA & Schnorr retardent le Commerce Électronique
nam: name
Utn: US patent number
clrsad: Patent lawyers analyse the weaknesses of the RSA patent.  On the one hand side, it can be attacked as non-statutory, because the only way it goes beyond a pure algorithm is by claiming standard computer implementations of this algorithm, which is, at least according to the law, not permissible.  On the other hand side, the RSA algorithm is very similar to a previously known formula.  The authors find that the patent has done disproportionate harm, partially because its strength was over-estimated.
Grh: Google Search for Schnorr Patent

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swxai-schnorr ;
# txtlang: fr ;
# End: ;

