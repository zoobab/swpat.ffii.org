\begin{subdocument}{swpiktxt98}{Top Software Probability Batch nr 98: 9701-9800}{http://swpat.ffii.org/patents/txt/ep98/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0354705\footnote{/patents/txt/ep/0354/705} & Tape synchronising apparatus for tape reproducer. & SONY CORP (JP) & 1988-08-12\\\hline
ep0353737\footnote{/patents/txt/ep/0353/737} & Complex multiplexer/demultiplexer apparatus. & FUJITSU LTD (JP) & 1988-08-03\\\hline
ep0379324\footnote{/patents/txt/ep/0379/324} & Method and apparatus for reading or writing on tape using a servo positioned multiple channel head. & MINNESOTA MINING \& MFG (US) & 1989-01-17\\\hline
ep0377340\footnote{/patents/txt/ep/0377/340} & Recording/reproducing device. & SHARP KK (JP) & 1988-12-29\\\hline
ep0384537\footnote{/patents/txt/ep/0384/537} & Method to improve directional survey accuracy. & ANADRILL INT SA (PA); SCHLUMBERGER PROSPECTION (FR) & 1989-02-21\\\hline
ep0380759\footnote{/patents/txt/ep/0380/759} & Method and apparatus for measuring entrained gas bubble content of flowing fluid. & MITSUBISHI OIL CO (JP) & 1989-01-10\\\hline
ep0391261\footnote{/patents/txt/ep/0391/261} & Method and apparatus for implementing electronic cash. & NIPPON TELEGRAPH \& TELEPHONE (JP) & 1989-05-18\\\hline
ep0401820\footnote{/patents/txt/ep/0401/820} & Recording density correction apparatus in printer. & MITSUBISHI ELECTRIC CORP (JP) & 1989-06-08\\\hline
ep0400653\footnote{/patents/txt/ep/0400/653} & Electronic cash register. & SHARP KK (JP) & 1989-05-31\\\hline
ep0434050\footnote{/patents/txt/ep/0434/050} & Plant operating and monitoring apparatus. & HITACHI LTD (JP) & 1995-05-30\\\hline
ep0434990\footnote{/patents/txt/ep/0434/990} & Charged-particle beam exposure method and apparatus. & FUJITSU LTD (JP) & 1989-11-30\\\hline
ep0442470\footnote{/patents/txt/ep/0442/470} & Ink jet recording apparatus & CANON KK (JP) & 1990-08-02\\\hline
ep0343639\footnote{/patents/txt/ep/0343/639} & Bit and symbol timing recovery for sequential decoders. & NIPPON ELECTRIC CO (JP) & 1988-05-24\\\hline
ep0447048\footnote{/patents/txt/ep/0447/048} & Method and apparatus for gaging bodies such as threaded fasteners and blanks. & RES ENG \& MFG (US) & 1990-07-10\\\hline
ep0453930\footnote{/patents/txt/ep/0453/930} & Method for checking the regular processing of banknotes. & GAO GES AUTOMATION ORG (DE) & 1990-04-27\\\hline
ep0475759\footnote{/patents/txt/ep/0475/759} & Phoneme discrimination method & OKI ELECTRIC IND CO LTD (JP) & 1990-09-13\\\hline
ep0482989\footnote{/patents/txt/ep/0482/989} & Telephone exchange apparatus. & CANON KK (JP) & 1990-10-22\\\hline
ep0482341\footnote{/patents/txt/ep/0482/341} & Rotary or linear position encoder. & SIEMENS AG (DE) & 1990-09-26\\\hline
ep0478926\footnote{/patents/txt/ep/0478/926} & Television signal receiving device for receiving television signals with a plurality of language channels. & GRUNDIG EMV (DE) & 1990-10-05\\\hline
ep0508826\footnote{/patents/txt/ep/0508/826} & On-vehicle navigation apparatus. & PIONEER ELECTRONIC CORP (JP) & 1991-04-12\\\hline
ep0508470\footnote{/patents/txt/ep/0508/470} & Infrared radiator. & BRAEHLER HELMUT (DE) & 1991-04-11\\\hline
ep0512612\footnote{/patents/txt/ep/0512/612} & A container and related sample collection tube & BECTON DICKINSON CO (US) & 1991-05-03\\\hline
ep0531703\footnote{/patents/txt/ep/0531/703} & Method for locating the position of electrophysiological activities & SIEMENS AG (DE) & 1991-08-16\\\hline
ep0547524\footnote{/patents/txt/ep/0547/524} & Method and apparatus for laminating identity cards and the like & INTERLOCK AG (CH) & 1991-12-19\\\hline
ep0569246\footnote{/patents/txt/ep/0569/246} & Navigation device. & PIONEER ELECTRONIC CORP (JP) & 1992-05-08\\\hline
ep0582290\footnote{/patents/txt/ep/0582/290} & Electron density storage device & MITSUBISHI PETROCHEMICAL CO (JP); UNIV JOHNS HOPKINS (US) & 1992-08-04\\\hline
ep0587224\footnote{/patents/txt/ep/0587/224} & Extended television signal receiver. & KONINKL PHILIPS ELECTRONICS NV (NL) & 1993-01-26\\\hline
ep0601290\footnote{/patents/txt/ep/0601/290} & Regular Jack connector & KRONE AG (DE) & 1992-12-09\\\hline
ep0622611\footnote{/patents/txt/ep/0622/611} & Navigation apparatus. & PIONEER ELECTRONIC CORP (JP) & 1993-04-26\\\hline
ep0630021\footnote{/patents/txt/ep/0630/021} & Audio signal recording apparatus with an index number signal generating function. & PIONEER ELECTRONIC CORP (JP) & 1993-06-16\\\hline
ep0646914\footnote{/patents/txt/ep/0646/914} & Cassettes with memories. & SONY CORP (JP) & 1993-10-04\\\hline
ep0474091\footnote{/patents/txt/ep/0474/091} & Pothead device for signal transmission cables, particularly for glass fibres cables. & REICHLE \& DE MASSARI FA (CH) & 1990-09-03\\\hline
ep0488751\footnote{/patents/txt/ep/0488/751} & Signal reproducing device for reproducing voice signals. & SHARP KK (JP) & 1990-11-28\\\hline
ep0712133\footnote{/patents/txt/ep/0712/133} & Method for the anticipated reading of serial access memory, and memory pertaining thereto & SGS THOMSON MICROELECTRONICS (FR) & 1994-11-10\\\hline
ep0505038\footnote{/patents/txt/ep/0505/038} & Improvements relating to television receivers. & AMSTRAD PLC (GB) & 1991-02-22\\\hline
ep0732813\footnote{/patents/txt/ep/0732/813} & FM multiplex broadcast receiving apparatus comprising detachable storing medium & CASIO COMPUTER CO LTD (JP) & 1995-03-16\\\hline
ep0527423\footnote{/patents/txt/ep/0527/423} & Electronic cash register. & TOKYO ELECTRIC CO LTD (JP) & 1991-10-07\\\hline
ep0529556\footnote{/patents/txt/ep/0529/556} & Vector-quatizing device. & FUJITSU LTD (JP) & 1991-08-30\\\hline
ep0540114\footnote{/patents/txt/ep/0540/114} & Method and apparatus for reducing track switch latency in a disk drive. & IBM (US) & 1991-11-01\\\hline
ep0811510\footnote{/patents/txt/ep/0811/510} & A method and an apparatus for processing received postal items & HADEWE BV (NL) & 1996-12-31\\\hline
ep0551210\footnote{/patents/txt/ep/0551/210} & Seismic surveying and interpolation of aliased seismic traces. & HALLIBURTON GEOPHYS SERVICE (US) & 1992-01-10\\\hline
ep0740414\footnote{/patents/txt/ep/0740/414} & Device for programmable delay of an analog signal and corresponding programmable acoustic antenna & FRANCE TELECOM (FR) & 1995-04-25\\\hline
ep0617518\footnote{/patents/txt/ep/0617/518} & Variable length codeword packer. & GI CORP (US) & 1993-03-26\\\hline
ep0661228\footnote{/patents/txt/ep/0661/228} & Procedure and apparatus for determining the position of an elevator car. & KONE OY (FI) & 1993-12-28\\\hline
ep0180100\footnote{/patents/txt/ep/0180/100} & Direction-constrained ternary codes using peak and polarity detection & IBM (US) & 1984-10-29\\\hline
ep0336273\footnote{/patents/txt/ep/0336/273} & Pulse Doppler radar. & LICENTIA GMBH (DE) & 1988-04-02\\\hline
ep0320589\footnote{/patents/txt/ep/0320/589} & Position-measuring device with several detectors. & HEIDENHAIN GMBH DR JOHANNES (DE) & 1987-12-14\\\hline
ep0661814\footnote{/patents/txt/ep/0661/814} & End-of-count detecting device, particularly for nonvolatile memories. & SGS THOMSON MICROELECTRONICS (IT) & 1993-12-28\\\hline
ep0389720\footnote{/patents/txt/ep/0389/720} & Radar detection of targets at short and long range. & MARCONI CO CANADA (CA) & 1989-03-28\\\hline
ep0427927\footnote{/patents/txt/ep/0427/927} & Track position syncopation cancellation in a disk drive. & HEWLETT PACKARD CO (US) & 1989-11-13\\\hline
ep0555507\footnote{/patents/txt/ep/0555/507} & Position measuring apparatus. & HEIDENHAIN GMBH DR JOHANNES (DE) & 1992-02-14\\\hline
ep0284383\footnote{/patents/txt/ep/0284/383} & Synthetic antigens for the detection of aids-related disease caused by LAV-2. & PASTEUR INSTITUT (FR); GENETIC SYSTEMS CORP (US) & 1987-04-07\\\hline
ep0566005\footnote{/patents/txt/ep/0566/005} & Overheat sensor for a disc brake. & KNORR BREMSE AG (DE) & 1992-04-13\\\hline
ep0315486\footnote{/patents/txt/ep/0315/486} & Aircraft engine frame construction. & GEN ELECTRIC (US) & 1987-11-05\\\hline
ep0477688\footnote{/patents/txt/ep/0477/688} & Voice recognition telephone dialing. & TEXAS INSTRUMENTS INC (US) & 1990-09-28\\\hline
ep0316317\footnote{/patents/txt/ep/0316/317} & SEISMIC APPARATUS AND METHOD INCLUDING USE OF TRANSPUTERS FOR REAL TIME PROCESSING OF DATA & ARMITAGE PETER ROBERT (GB) & 1986-07-19\\\hline
ep0886818\footnote{/patents/txt/ep/0886/818} & MODULAR ARRANGEMENT FOR SUPPLEMENTING PORTABLE COMPUTERS & SCHNEIDER BERND (DE) & 1996-10-07\\\hline
ep0953900\footnote{/patents/txt/ep/0953/900} & Distributed object based systems. & HEWLETT PACKARD CO (US) & 1989-07-21\\\hline
ep0782725\footnote{/patents/txt/ep/0782/725} & Computer with two-dimensional merge tournament sort using offset-value coding & AMDAHL CORP (US) & 1994-09-19\\\hline
ep0331048\footnote{/patents/txt/ep/0331/048} & Method and apparatus for reading bar code of photographic film and method of determining the orientation of photographic film. & FUJI PHOTO FILM CO LTD (JP) & 1988-08-08\\\hline
ep0562845\footnote{/patents/txt/ep/0562/845} & Variable length code recording/playback apparatus for a video recorder. & TOSHIBA AVE KK (JP); TOKYO SHIBAURA ELECTRIC CO (JP) & 1992-03-25\\\hline
ep0609935\footnote{/patents/txt/ep/0609/935} & Method and apparatus for smoothing code measurements in a global positioning system receiver. & MAGNAVOX ELECTRONIC SYSTEMS (US) & 1993-02-01\\\hline
ep0427863\footnote{/patents/txt/ep/0427/863} & DNA CODING FOR PROTEIN CAPABLE OF COMBINING WITH ENHANCER OF ALPHA-FETOPROTEIN GENE. & SNOW BRAND MILK PROD CO LTD (JP) & 1990-04-27\\\hline
ep0416170\footnote{/patents/txt/ep/0416/170} & Coding band, in particular for electric cables, and coding method. & CO E P T E COSTRUZIONI ELETTRO (IT) & 1989-08-09\\\hline
ep0406507\footnote{/patents/txt/ep/0406/507} & Block coding scheme for fractional-bit transmission. & IBM (US) & 1989-07-07\\\hline
ep0511498\footnote{/patents/txt/ep/0511/498} & Encoding apparatus for digital signal with improved block channel coding. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1991-03-30\\\hline
ep0479511\footnote{/patents/txt/ep/0479/511} & Motion compensated predictive coding/decoding system of picture signal. & VICTOR COMPANY OF JAPAN (JP) & 1990-09-29\\\hline
ep0576290\footnote{/patents/txt/ep/0576/290} & Picture signal coding and decoding. & SONY CORP (JP) & 1992-10-26\\\hline
ep0548415\footnote{/patents/txt/ep/0548/415} & Coding system allowing auxiliary data transmission. & BELL TELEPHONE MFG (BE); ALCATEL NV (NL) & 1991-12-24\\\hline
ep0536784\footnote{/patents/txt/ep/0536/784} & An adaptive coding method for interlaced scan digital video sequences. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1991-10-11\\\hline
ep0615223\footnote{/patents/txt/ep/0615/223} & Method and apparatus for the coding and display of overlapping windows with transparency. & AT \& T CORP (US) & 1993-03-10\\\hline
ep0588653\footnote{/patents/txt/ep/0588/653} & Image data coding and restoring method and appatatus for coding and restoring the same. & FUJITSU LTD (JP) & 1992-09-16\\\hline
ep0652678\footnote{/patents/txt/ep/0652/678} & Method and apparatus for improving motion compensation in digital video coding. & AT \& T CORP (US) & 1993-11-04\\\hline
ep0229821\footnote{/patents/txt/ep/0229/821} & Sar image encoding for data compression & HUGHES AIRCRAFT CO (US) & 1985-06-17\\\hline
ep0345841\footnote{/patents/txt/ep/0345/841} & Piston engine, and a compression device provided with two piston engines and a cryogenic cooler. & PHILIPS NV (NL) & 1988-05-19\\\hline
ep0404535\footnote{/patents/txt/ep/0404/535} & Band compression device. & SONY CORP (JP) & 1989-06-20\\\hline
ep0394768\footnote{/patents/txt/ep/0394/768} & Method and device for data acquisition and compression for offnormal recording in monitoring systems. & SIEMENS AG (DE) & 1989-04-27\\\hline
ep0469648\footnote{/patents/txt/ep/0469/648} & Data compression using a feedforward quantization estimator. & AMPEX (US) & 1990-07-31\\\hline
ep0533707\footnote{/patents/txt/ep/0533/707} & Digital video signal compression & NORTHERN TELECOM LTD (CA) & 1990-06-13\\\hline
ep0582648\footnote{/patents/txt/ep/0582/648} & VIDEO SIGNAL COMPRESSION APPARATUS FOR INDEPENDENTLY COMPRESSING ODD AND EVEN FIELDS & GEN ELECTRIC (US) & 1991-04-29\\\hline
ep0242909\footnote{/patents/txt/ep/0242/909} & Computer tomography apparatus including a combined recursive and convolution filter. & PHILIPS NV (NL) & 1986-04-21\\\hline
ep0425183\footnote{/patents/txt/ep/0425/183} & Mounting structure for a railmountable device employed in a computer. & IBM (US) & 1989-10-27\\\hline
ep0411679\footnote{/patents/txt/ep/0411/679} & Computer for reducing the execution time required for branch instructions. & WESTERN ELECTRIC CO (US) & 1982-08-23\\\hline
ep0469966\footnote{/patents/txt/ep/0469/966} & Computer-aided surgery apparatus. & FARO MEDICAL TECHNOLOGIES US I (US) & 1990-07-31\\\hline
ep0463288\footnote{/patents/txt/ep/0463/288} & Apparatus and method for integrating pointing functions into a computer keyboard. & IBM (US) & 1990-06-28\\\hline
ep0567492\footnote{/patents/txt/ep/0567/492} & \#f &  & 1991-01-16\\\hline
ep0193153\footnote{/patents/txt/ep/0193/153} & Digital data recording and reproducing method & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1985-05-16\\\hline
ep0192944\footnote{/patents/txt/ep/0192/944} & Processor I/O and interrupt filters allowing a co-processor to run software unknown to the main processor & IBM (US) & 1985-02-28\\\hline
ep0173364\footnote{/patents/txt/ep/0173/364} & Method, station and system for the transmission of messages in the form of data packets & PHILIPS CORP (US) & 1984-07-27\\\hline
ep0234831\footnote{/patents/txt/ep/0234/831} & Preserving a record of timecodes used in editing a recorded digital video signal & SONY CORP (JP) & 1986-02-24\\\hline
ep0226991\footnote{/patents/txt/ep/0226/991} & Apparatus for using an ALU as a temporary storage for data during an otherwise idle cycle of the ALU & TOKYO SHIBAURA ELECTRIC CO (JP) & 1985-12-16\\\hline
ep0252930\footnote{/patents/txt/ep/0252/930} & Data compression method and apparatus & NCR CO (US) & 1985-12-09\\\hline
ep0251626\footnote{/patents/txt/ep/0251/626} & Transmission system for audio-data signals to accompany transmitted video signals. & THORN EMI HOME ELECTRON (GB) & 1986-06-23\\\hline
ep0261927\footnote{/patents/txt/ep/0261/927} & Patient monitoring system having transportable data module and display unit & MARQUETTE ELECTRONICS INC (US) & 1986-09-26\\\hline
ep0257739\footnote{/patents/txt/ep/0257/739} & Transmission system for audio-data signals to accompany transmitted video signals. & THORN EMI HOME ELECTRON (GB) & 1986-06-23\\\hline
ep0262605\footnote{/patents/txt/ep/0262/605} & Improved method and arrangement for recording digital data on a magnetic recording medium by use of binary data signals and magnetic bias signals & TANDBERG DATA (NO) & 1986-09-30\\\hline
ep0287586\footnote{/patents/txt/ep/0287/586} & Trellis codes with spectral nulls & AMERICAN TELEPHONE \& TELEGRAPH (US); AT \& T BELL LAB (US); AT \& T INFORMATION SYSTEMS INC (US) & 1986-10-02\\\hline
ep0285986\footnote{/patents/txt/ep/0285/986} & Data processing system with means for contiguously addressing memory. & IBM (US) & 1987-04-01\\\hline
ep0288479\footnote{/patents/txt/ep/0288/479} & \#f &  & 1986-01-29\\\hline
ep0346783\footnote{/patents/txt/ep/0346/783} & Method and device for transmitting digital telemetry signals in a digital data flow. & HASLER AG ASCOM (CH) & 1988-06-14\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

