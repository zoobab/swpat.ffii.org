\begin{subdocument}{swpiktxt89}{Top Software Probability Batch nr 89: 8801-8900}{http://swpat.ffii.org/patents/txt/ep89/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0578045\footnote{/patents/txt/ep/0578/045} & An automatic system for teaching and for monitoring learning by real-time analysis of responses to questions. & PROMETEO SPA (IT) & 1992-07-09\\\hline
ep0656540\footnote{/patents/txt/ep/0656/540} & Dyeing agent and apparatus for image analysis of flow type stain particles. & HITACHI LTD (JP) & 1993-10-08\\\hline
ep0268984\footnote{/patents/txt/ep/0268/984} & Image orientation and animation using quaternions & GRASS VALLEY GROUP (US) & 1986-11-19\\\hline
ep0207534\footnote{/patents/txt/ep/0207/534} & System for storing and distributing keys for cryptographically protected communication & PHILIPS CORP (US) & 1985-04-29\\\hline
ep0292287\footnote{/patents/txt/ep/0292/287} & Asynchronous communication systems. & BRITISH AEROSPACE (GB) & 1987-05-21\\\hline
ep0277247\footnote{/patents/txt/ep/0277/247} & SYSTEM FOR GENERATING A SHARED CRYPTOGRAPHIC KEY AND A COMMUNICATION SYSTEM USING THE SHARED CRYPTOGRAPHIC KEY. & ADVANCE KK (JP) & 1986-10-24\\\hline
ep0314203\footnote{/patents/txt/ep/0314/203} & System for enabling descrambling on a selective-subscription basis in a subscriber communication network. & MA COM GOV SYSTEMS (US); CABLE HOME COMMUNICATION CORP (US) & 1984-06-08\\\hline
ep0385695\footnote{/patents/txt/ep/0385/695} & Communication system. & FIRST PACIFIC NETWORKS INC (US) & 1989-02-28\\\hline
ep0433206\footnote{/patents/txt/ep/0433/206} & Combined keyboard and mouse computer entry. & IBM (US) & 1989-12-15\\\hline
ep0495092\footnote{/patents/txt/ep/0495/092} & COMPUTER NEURAL NETWORK REGULATORY PROCESS CONTROL SYSTEM AND METHOD & DU PONT (US) & 1990-08-03\\\hline
ep0495080\footnote{/patents/txt/ep/0495/080} & COMPUTER NEURAL NETWORK SUPERVISORY PROCESS CONTROL SYSTEM AND METHOD & DU PONT (US) & 1990-08-03\\\hline
ep0633523\footnote{/patents/txt/ep/0633/523} & Processorsystem comprising a processor and a memory field for containing a computer interface. & NEDERLAND PTT (NL) & 1993-07-08\\\hline
ep0490742\footnote{/patents/txt/ep/0490/742} & Process for the regulation of working conditions of a reactor for the catalytic conversion of an oxygenous compound using a source of ionizing radiations. & INST FRANCAIS DU PETROL (FR) & 1990-12-10\\\hline
ep0347934\footnote{/patents/txt/ep/0347/934} & Data recording and/or reproducing method and data recording medium. & SONY CORP (JP) & 1988-06-23\\\hline
ep0311929\footnote{/patents/txt/ep/0311/929} & Data transfer bus in profile and/or dimension measuring system. & OLYMPUS OPTICAL CO (JP) & 1987-10-12\\\hline
ep0407311\footnote{/patents/txt/ep/0407/311} & SYSTEM FOR REARRANGING SEQUENTIAL DATA WORDS FROM AN INITIAL ORDER TO AN ARRIVAL ORDER IN A PREDETERMINED ORDER & SGS THOMSON MICROELECTRONICS (FR) & 1989-07-03\\\hline
ep0423730\footnote{/patents/txt/ep/0423/730} & Electronic device with data transmission function. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1989-10-17\\\hline
ep0483441\footnote{/patents/txt/ep/0483/441} & System arrangement for storing data on a FIFO basis. & ST MICROELECTRONICS SRL (IT) & 1990-11-02\\\hline
ep0500383\footnote{/patents/txt/ep/0500/383} & Data processing system. & NCR INT INC (US) & 1991-02-22\\\hline
ep0420280\footnote{/patents/txt/ep/0420/280} & Digital information signal reproducing apparatus for reproducing digital audio signal at the reproducing speed different from the recording speed. & SONY CORP (JP) & 1989-11-21\\\hline
ep0501655\footnote{/patents/txt/ep/0501/655} & Reducing power consumption in a digital processor. & IBM (US) & 1991-02-25\\\hline
ep0497777\footnote{/patents/txt/ep/0497/777} & METHODS OF REALIZING DIGITAL SIGNAL PROCESSORS USING A PROGRAMMED COMPILER & VLSI TECHNOLOGY INC (FR) & 1989-10-23\\\hline
ep0553948\footnote{/patents/txt/ep/0553/948} & Digital speech encryption using encryption memory. & ADVANCED MICRO DEVICES INC (US) & 1992-01-06\\\hline
ep0632395\footnote{/patents/txt/ep/0632/395} & Transferring instructions into DSP memory including testing instructions to determine if they are to be processed by an instruction interpreter or a first kernel & PHILIPS CORP (US) & 1993-06-30\\\hline
ep0443753\footnote{/patents/txt/ep/0443/753} & Method and apparatus for producing order independent signatures for error detection. & HEWLETT PACKARD CO (US) & 1990-02-14\\\hline
ep0840864\footnote{/patents/txt/ep/0840/864} & Error recognition process for automatic gear boxes transmission & ZAHNRADFABRIK FRIEDRICHSHAFEN (DE) & 1996-07-20\\\hline
ep0314080\footnote{/patents/txt/ep/0314/080} & Image paralleling and rotating system. & SHARP KK (JP) & 1987-10-29\\\hline
ep0493990\footnote{/patents/txt/ep/0493/990} & Display image scroll control and method. & IBM (US) & 1990-12-31\\\hline
ep0704676\footnote{/patents/txt/ep/0704/676} & System for displaying information in an automobile & PEUGEOT (FR); CITROEN SA (FR) & 1994-09-28\\\hline
ep0388436\footnote{/patents/txt/ep/0388/436} & Network and intelligent cell for providing sensing, bidirectional communications and control & ECHELON SYSTEMS (US) & 1987-11-10\\\hline
ep0549235\footnote{/patents/txt/ep/0549/235} & Local area network system. & NCR INT INC (US) & 1991-12-24\\\hline
ep0717895\footnote{/patents/txt/ep/0717/895} & Key distribution in a multiple access network using quantum cryptography & BRITISH TELECOMM (GB) & 1994-09-08\\\hline
ep0318858\footnote{/patents/txt/ep/0318/858} & Connected word recognition system including neural networks arranged along a signal time axis. & NIPPON ELECTRIC CO (JP) & 1987-11-25\\\hline
ep0424302\footnote{/patents/txt/ep/0424/302} & Method for controlling the multi-frame transmission on token ring networks. & IBM (US) & 1989-10-20\\\hline
ep0365285\footnote{/patents/txt/ep/0365/285} & Method of diagnosing sequence program for use in sequence controller. & NGK INSULATORS LTD (JP) & 1988-10-18\\\hline
ep0348563\footnote{/patents/txt/ep/0348/563} & A system for generating program object modules. & IBM (US) & 1988-06-27\\\hline
ep0680213\footnote{/patents/txt/ep/0680/213} & A method for controlling execution of an audio video interactive program. & THOMSON CONSUMER ELECTRONICS (US) & 1994-04-28\\\hline
ep0667075\footnote{/patents/txt/ep/0667/075} & Signaling protocol supporting multimedia services employing an automation with multiple states and a state matrix indicating the states of multiple entities & CIT ALCATEL (FR) & 1994-09-01\\\hline
ep0178671\footnote{/patents/txt/ep/0178/671} & Distributed control store word architecture & HONEYWELL INF SYSTEMS (US) & 1984-10-19\\\hline
ep0282520\footnote{/patents/txt/ep/0282/520} & Non-volatile memory with floating grid and without thick oxide & SGS THOMSON MICROELECTRONICS (FR) & 1986-09-16\\\hline
ep0369773\footnote{/patents/txt/ep/0369/773} & Queue buffer memory control system. & FUJITSU LTD (JP) & 1988-11-16\\\hline
ep0525382\footnote{/patents/txt/ep/0525/382} & Method and apparatus for scanning a receiving medium. & EASTMAN KODAK CO (US) & 1991-07-01\\\hline
ep0332765\footnote{/patents/txt/ep/0332/765} & Graphical input/output system and method. & TAKENAKA CORP (JP) & 1988-03-18\\\hline
ep0672980\footnote{/patents/txt/ep/0672/980} & Keyboard-touchpad combination in a bivalve enclosure. & IBM (US) & 1994-03-18\\\hline
ep0258227\footnote{/patents/txt/ep/0258/227} & Control system for mobile transporting units on transporting lines & BOSCH GMBH ROBERT (DE) & 1985-05-02\\\hline
ep0551595\footnote{/patents/txt/ep/0551/595} & Visualization techniques for temporally acquired sequences of images. & EASTMAN KODAK CO (US) & 1991-12-17\\\hline
ep0609596\footnote{/patents/txt/ep/0609/596} & Multi-function pre-processor for target tracking. & LORAL AEROSPACE CORP (US) & 1989-09-21\\\hline
ep0228047\footnote{/patents/txt/ep/0228/047} & State control for a real-time system utilizing a nonprocedural language & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1985-12-23\\\hline
ep0283193\footnote{/patents/txt/ep/0283/193} & Method of spare capacity use for fault detection in a multiprocessor system. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1987-03-18\\\hline
ep0306602\footnote{/patents/txt/ep/0306/602} & Self-controlled vision system. & VEITCH SIMON JOHN & 1984-03-06\\\hline
ep0319183\footnote{/patents/txt/ep/0319/183} & Parity regeneration self-checking. & TANDEM COMPUTERS INC (US) & 1987-11-30\\\hline
ep0377548\footnote{/patents/txt/ep/0377/548} & Plant for the sorting of suspended articles and the use hereof. & DANSK VASKERI TEKNIK AS (DK) & 1989-01-04\\\hline
ep0472939\footnote{/patents/txt/ep/0472/939} & Cryptographic key version control facility. & IBM (US) & 1990-08-31\\\hline
ep0736821\footnote{/patents/txt/ep/0736/821} & Heater control device & SHARP KK (JP) & 1995-04-07\\\hline
ep0146594\footnote{/patents/txt/ep/0146/594} & VECTOR ATTRIBUTE GENERATING METHOD AND APPARATUS & RAMTEK CORP (US) & 1983-05-25\\\hline
ep0660232\footnote{/patents/txt/ep/0660/232} & Method and system for selectively applying an appropriate object ownership model. & MICROSOFT CORP (US) & 1993-12-15\\\hline
ep0603632\footnote{/patents/txt/ep/0603/632} & Method for controlling a processor device. & SIEMENS AG (DE) & 1992-12-23\\\hline
ep0642090\footnote{/patents/txt/ep/0642/090} & Electronic module comprising at least one decentralized processor in a multiprocessor system. & SIEMENS AG (DE) & 1993-08-30\\\hline
ep0492199\footnote{/patents/txt/ep/0492/199} & Automatic transmission gearshift control having feedforward response of clutch and its hydraulic actuation. & FORD WERKE AG (DE); FORD FRANCE (FR); FORD MOTOR CO (GB) & 1990-12-24\\\hline
ep0488849\footnote{/patents/txt/ep/0488/849} & Automatic brightness adjusting device of images generated by a camera and camera with such a device. & THOMSON TRT DEFENSE (FR) & 1990-11-23\\\hline
ep0558172\footnote{/patents/txt/ep/0558/172} & Multiple solvent delivery system and method. & ISCO INC (US) & 1992-02-27\\\hline
ep0409562\footnote{/patents/txt/ep/0409/562} & Television receivers having a picture-in-picture function. & SONY CORP (JP) & 1989-07-18\\\hline
ep0367388\footnote{/patents/txt/ep/0367/388} & Elevator diagnostic monitoring apparatus. & OTIS ELEVATOR CO (US) & 1988-10-31\\\hline
ep0472775\footnote{/patents/txt/ep/0472/775} & Programm-controlled communications system, especially switching exchange. & SIEMENS AG (DE) & 1990-08-31\\\hline
ep0488706\footnote{/patents/txt/ep/0488/706} & Transmission line selection system. & FUJITSU LTD (JP) & 1990-11-28\\\hline
ep0519106\footnote{/patents/txt/ep/0519/106} & Bus link control. & SIEMENS AG (DE) & 1991-06-20\\\hline
ep0490805\footnote{/patents/txt/ep/0490/805} & Automated helicopter maintenance monitoring. & UNITED TECHNOLOGIES CORP (US) & 1990-12-07\\\hline
ep0305824\footnote{/patents/txt/ep/0305/824} & Serial interface. & THOMSON BRANDT GMBH (DE) & 1987-08-28\\\hline
ep0395408\footnote{/patents/txt/ep/0395/408} & Adaptive architecture for video effects. & GRASS VALLEY GROUP (US) & 1989-04-26\\\hline
ep0377939\footnote{/patents/txt/ep/0377/939} & Robot control system for controlling a set of industrial robots. & KOBE STEEL LTD (JP) & 1989-01-10\\\hline
ep0436818\footnote{/patents/txt/ep/0436/818} & Diagnostic system for digitally controlled devices. & HEIDELBERGER DRUCKMASCH AG (DE) & 1994-10-11\\\hline
ep0547810\footnote{/patents/txt/ep/0547/810} & Aircraft engine management system. & ROLLS ROYCE PLC (GB) & 1991-12-17\\\hline
ep0530504\footnote{/patents/txt/ep/0530/504} & Device for making concrete products. & WECKENMANN ANLAGENTECHNIK GMBH (DE) & 1991-09-04\\\hline
ep0398801\footnote{/patents/txt/ep/0398/801} & Detection unit for infrared guarding system. & TELECOMMUNICATIONS SA (FR) & 1989-05-18\\\hline
ep0241574\footnote{/patents/txt/ep/0241/574} & Fire alarm system & MATSUSHITA ELECTRIC WORKS LTD (JP) & 1986-03-31\\\hline
ep0565431\footnote{/patents/txt/ep/0565/431} & Improvements in diesel engines. & RENAULT (FR) & 1992-04-07\\\hline
ep0362010\footnote{/patents/txt/ep/0362/010} & Downhole tool and method for determination of formation properties. & SCHLUMBERGER LTD (US); SCHLUMBERGER PROSPECTION (FR) & 1988-09-23\\\hline
ep0430615\footnote{/patents/txt/ep/0430/615} & Speech recognition system. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1989-11-28\\\hline
ep0568792\footnote{/patents/txt/ep/0568/792} & Call progress monitoring. & ROLM CO (US) & 1992-03-17\\\hline
ep0539272\footnote{/patents/txt/ep/0539/272} & Method and apparatus for detecting and quantifying hydrocarbon bearing laminated reservoirs on a workstation. & SCHLUMBERGER HOLDINGS (@@); SCHLUMBERGER SERVICES PETROL (FR); SCHLUMBERGER TECHNOLOGY BV (NL); SCHLUMBERGER LTD (US) & 1991-10-21\\\hline
ep0619535\footnote{/patents/txt/ep/0619/535} & Process controls system architecture. & XEROX CORP (US) & 1993-02-24\\\hline
ep0697502\footnote{/patents/txt/ep/0697/502} & Downhole tool for determination of formation properties & SCHLUMBERGER SERVICES PETROL (FR); SCHLUMBERGER TECHNOLOGY BV (NL); SCHLUMBERGER LTD (US); SCHLUMBERGER HOLDINGS (VG) & 1988-09-23\\\hline
ep0780331\footnote{/patents/txt/ep/0780/331} & Apparatus and method for aligning webs & EASTMAN KODAK CO (US) & 1995-12-20\\\hline
ep0519360\footnote{/patents/txt/ep/0519/360} & Apparatus and method for speech recognition. & SEL ALCATEL AG (DE); ALCATEL NV (NL) & 1991-06-20\\\hline
ep0429824\footnote{/patents/txt/ep/0429/824} & Secure key management using control vector translation. & IBM (US) & 1989-11-29\\\hline
ep0279747\footnote{/patents/txt/ep/0279/747} & Controlling, regulating and monitoring device for a proceeding cold unit. & BONNET REFRIGERATION (FR) & 1987-02-20\\\hline
ep0278588\footnote{/patents/txt/ep/0278/588} & System for controlling torque transmission in a four wheel drive vehicle. & BORG WARNER AUTOMOTIVE (US) & 1987-01-23\\\hline
ep0883841\footnote{/patents/txt/ep/0883/841} & System and method for waveform synthesis & ADVANCED MICRO DEVICES INC (US) & 1996-02-09\\\hline
ep0240921\footnote{/patents/txt/ep/0240/921} & BCH code signal correcting system. & KENWOOD CORP (JP) & 1986-04-03\\\hline
ep0300139\footnote{/patents/txt/ep/0300/139} & Error correcting code for B-bit-per-chip memory with reduced redundancy. & IBM (US) & 1987-07-20\\\hline
ep0600576\footnote{/patents/txt/ep/0600/576} & Code-based, electromagnetic-field-responsive graphic data-acquisition system. & MICROFIELD GRAPHICS INC (US) & 1992-10-07\\\hline
ep0289080\footnote{/patents/txt/ep/0289/080} & System for subband coding of a digital audio signal. & PHILIPS NV (NL) & 1987-04-27\\\hline
ep0242117\footnote{/patents/txt/ep/0242/117} & Distributed timing control for a distributed digital communication system & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1986-04-16\\\hline
ep0212701\footnote{/patents/txt/ep/0212/701} & Ring shaped digital communication system and method for accessing a message channel in such system & PHILIPS CORP (US) & 1985-07-15\\\hline
ep0412799\footnote{/patents/txt/ep/0412/799} & Telephone communication system. & BOSTON TECH INC (US) & 1989-08-10\\\hline
ep0513232\footnote{/patents/txt/ep/0513/232} & Method of error recovery in a data communication system & IBM (US) & 1991-02-19\\\hline
ep0351654\footnote{/patents/txt/ep/0351/654} & High accuracy coordinate conversion method for air traffic control applications. & IBM (US) & 1988-07-21\\\hline
ep0701745\footnote{/patents/txt/ep/0701/745} & Direct conversion tuner & RCA THOMSON LICENSING CORP (US) & 1994-06-06\\\hline
ep0251889\footnote{/patents/txt/ep/0251/889} & Method for the programming of data in an electrically programmable read-only memory & THOMSON SEMICONDUCTEURS (FR) & 1986-06-27\\\hline
ep0251593\footnote{/patents/txt/ep/0251/593} & Power source system for data terminal equipment & KOKUSAI DENSHIN DENWA CO LTD (JP) & 1986-06-23\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

