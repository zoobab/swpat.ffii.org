\begin{subdocument}{swpiktxt95}{Top Software Probability Batch nr 95: 9401-9500}{http://swpat.ffii.org/patents/txt/ep95/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0690590\footnote{/patents/txt/ep/0690/590} & Communication apparatus for TDMA system with transmission of speech and control data & SONY CORP (JP) & 1994-06-29\\\hline
ep0286240\footnote{/patents/txt/ep/0286/240} & Computer system. & TOYO COMMUNICATION EQUIP (JP) & 1987-12-28\\\hline
ep0427466\footnote{/patents/txt/ep/0427/466} & Computer plotter control. & XEROX CORP (US) & 1989-11-01\\\hline
ep0479408\footnote{/patents/txt/ep/0479/408} & Computer-assisted instructional delivery system. & HAMILTON ERIC R (US) & 1990-04-17\\\hline
ep0477385\footnote{/patents/txt/ep/0477/385} & METHOD OF RESETTING ADAPTER MODULE AT FAILING TIME AND COMPUTER SYSTEM EXECUTING SAID METHOD. & FUJITSU LTD (JP) & 1990-04-13\\\hline
ep0719478\footnote{/patents/txt/ep/0719/478} & DIGITAL TO ANALOG CONVERSION USING NONUNIFORM SAMPLE RATES & SOBOL JAMES M (US); WILSON JAMES (US); ANALOG DEVICES INC (US); CELLINI RONALD A (US) & 1993-09-13\\\hline
ep0360613\footnote{/patents/txt/ep/0360/613} & Game machine data transfer system. & BALLY MFG CORP (US) & 1988-09-22\\\hline
ep0383911\footnote{/patents/txt/ep/0383/911} & SYSTEM FOR FORMING KNOWLEDGE DATA. & HITACHI LTD (JP) & 1988-06-17\\\hline
ep0551923\footnote{/patents/txt/ep/0551/923} & Consensus sync`` data-sampling systems and methods. & EASTMAN KODAK CO (US) & 1992-01-17\\\hline
ep0633669\footnote{/patents/txt/ep/0633/669} & Method and apparatus for the detection of transmitted data in an electrical power distribution network. & ENERMET OY (FI) & 1993-07-05\\\hline
ep0721619\footnote{/patents/txt/ep/0721/619} & Execution of data processing instructions & ADVANCED RISC MACH LTD (GB) & 1994-08-16\\\hline
ep0392246\footnote{/patents/txt/ep/0392/246} & Monitoring and control system for digital information transmission systems with master and substitution master. & SIEMENS AG (DE) & 1989-04-13\\\hline
ep0392245\footnote{/patents/txt/ep/0392/245} & Automatic addressing of monitoring and/or control processing units comprised in a digital information transmission system. & SIEMENS AG (DE) & 1989-04-13\\\hline
ep0410376\footnote{/patents/txt/ep/0410/376} & Image reproducing system. & MINOLTA CAMERA KK (JP) & 1989-07-25\\\hline
ep0458926\footnote{/patents/txt/ep/0458/926} & DUAL PORT, DUAL SPEED IMAGE MEMORY ACCESS ARRANGEMENT & EASTMAN KODAK CO (US) & 1989-12-15\\\hline
ep0490505\footnote{/patents/txt/ep/0490/505} & Media control module for a multimedia system & IBM (US) & 1990-12-11\\\hline
ep0498574\footnote{/patents/txt/ep/0498/574} & Waveform equalizer apparatus formed of neural network, and method of designing same. & VICTOR COMPANY OF JAPAN (JP) & 1991-01-31\\\hline
ep0623262\footnote{/patents/txt/ep/0623/262} & Cordless local area network having a fixed central control device & SIXTEL SPA (IT) & 1991-10-07\\\hline
ep0617860\footnote{/patents/txt/ep/0617/860} & Inbound communications using electricity distribution network & DISTRIBUTION CONTROL SYST INC (US) & 1991-12-17\\\hline
ep0581828\footnote{/patents/txt/ep/0581/828} & Neural networks &  & 1992-04-20\\\hline
ep0459488\footnote{/patents/txt/ep/0459/488} & Radio telecommunication apparatus. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1990-05-31\\\hline
ep0724362\footnote{/patents/txt/ep/0724/362} & Priority controlled transmission of multimedia streams via a telecommunication line & IBM (US) & 1995-01-30\\\hline
ep0255903\footnote{/patents/txt/ep/0255/903} & Processor-controlled telephone exchange with a central operation device and operation terminals. & SIEMENS AG (DE) & 1986-08-08\\\hline
ep0619899\footnote{/patents/txt/ep/0619/899} & SOFTWARE CONTROL OF HARDWARE INTERRUPTIONS & AMDAHL CORP (US) & 1992-01-02\\\hline
ep0249575\footnote{/patents/txt/ep/0249/575} & Computerized communications system. & CALL IT CO (US) & 1986-04-16\\\hline
ep0305987\footnote{/patents/txt/ep/0305/987} & Self-correcting semiconductor memory device and microcomputer incorporating the same. & OKI ELECTRIC IND CO LTD (JP) & 1987-08-31\\\hline
ep0493881\footnote{/patents/txt/ep/0493/881} & Bus architecture for a multimedia system. & IBM (US) & 1990-12-11\\\hline
ep0492795\footnote{/patents/txt/ep/0492/795} & Multimedia system. & IBM (US) & 1990-12-11\\\hline
ep0446077\footnote{/patents/txt/ep/0446/077} & A control system for multi-processor system. & FUJITSU LTD (JP) & 1990-03-09\\\hline
ep0074390\footnote{/patents/txt/ep/0074/390} & Apparatus and method for maintaining cache memory integrity in a shared memory environment & BURROUGHS CORP (US) & 1981-03-24\\\hline
ep0271276\footnote{/patents/txt/ep/0271/276} & Method which provides debounced inputs from a touch screen panel by waiting until each x and y coordinates stop altering & TEKTRONIX INC (US) & 1986-12-08\\\hline
ep0747804\footnote{/patents/txt/ep/0747/804} & Selection facilitation on a graphical interface & IBM (US) & 1995-06-07\\\hline
ep0167454\footnote{/patents/txt/ep/0167/454} & Centralized command transfer control system for connecting processors which independently send and receive commands & FUJITSU LTD (JP) & 1984-06-29\\\hline
ep0033228\footnote{/patents/txt/ep/0033/228} & Industrial control system. & FORNEY INTERNATIONAL (US) & 1980-10-31\\\hline
ep0480744\footnote{/patents/txt/ep/0480/744} & Facsimile apparatus. & CANON KK (JP) & 1990-10-11\\\hline
ep0359178\footnote{/patents/txt/ep/0359/178} & Load control system. & MITSUBISHI ELECTRIC CORP (JP) & 1988-09-14\\\hline
ep0380904\footnote{/patents/txt/ep/0380/904} & Solid state microscope. & MICROSCAN IMAGING AND INSTRUME (CA) & 1987-08-20\\\hline
ep0628897\footnote{/patents/txt/ep/0628/897} & Automatic piloting device for aerodynes & SEXTANT AVIONIQUE (FR) & 1993-06-07\\\hline
ep0645932\footnote{/patents/txt/ep/0645/932} & Method and apparatus for display of video images in a video conferencing system. & AT \& T GLOBAL INF SOLUTION (US) & 1993-09-28\\\hline
ep0725283\footnote{/patents/txt/ep/0725/283} & Display apparatus & MITSUBISHI ELECTRIC CORP (JP) & 1995-01-31\\\hline
ep0382044\footnote{/patents/txt/ep/0382/044} & Electronic camera system with detachable printer. & POLAROID CORP (US) & 1989-02-10\\\hline
ep0414158\footnote{/patents/txt/ep/0414/158} & Control signal generator for a television system. & THOMSON CONSUMER ELECTRONICS (US) & 1989-08-25\\\hline
ep0589753\footnote{/patents/txt/ep/0589/753} & Method of transmitting, together with a handover command in a cellular system, a timing advance signal to a mobile. & ALCATEL RADIOTELEPHONE (FR) & 1992-09-15\\\hline
ep0681177\footnote{/patents/txt/ep/0681/177} & Method and apparatus for cell counting and cell classification. & BIOMETRIC IMAGING INC (US) & 1994-05-02\\\hline
ep0466404\footnote{/patents/txt/ep/0466/404} & Size markers for electrophoretic analysis of DNA. & LIFE TECHNOLOGIES INC (US) & 1990-07-13\\\hline
ep0743634\footnote{/patents/txt/ep/0743/634} & Method of adapting the noise masking level in an analysis-by-synthesis speech coder employing a short-term perceptual weighting filter & FRANCE TELECOM (FR) & 1995-05-17\\\hline
ep0475697\footnote{/patents/txt/ep/0475/697} & Animation drawing apparatus. & SONY CORP (JP) & 1990-09-10\\\hline
ep0500277\footnote{/patents/txt/ep/0500/277} & Film cartridge bar code scanner and controller for a digital imaging system. & MINNESOTA MINING \& MFG (US) & 1991-02-19\\\hline
ep0550124\footnote{/patents/txt/ep/0550/124} & Oven controlled by an optical code reader. & MENUMASTER INC (US) & 1992-01-03\\\hline
ep0399520\footnote{/patents/txt/ep/0399/520} & A radio communication system and a portable wireless terminal. & HITACHI LTD (JP) & 1989-05-26\\\hline
ep0395428\footnote{/patents/txt/ep/0395/428} & Communication protocol for statistical data multiplexers arranged in a wide area network & DIGITAL EQUIPMENT CORP (US) & 1989-04-28\\\hline
ep0622927\footnote{/patents/txt/ep/0622/927} & Data transmission apparatus and a communication path management method therefor. & MITSUBISHI ELECTRIC CORP (JP) & 1993-03-04\\\hline
ep0885423\footnote{/patents/txt/ep/0885/423} & Classification of time series with sampling values for evaluating EEG or ECG & SIEMENS AG (DE) & 1996-03-06\\\hline
ep0298216\footnote{/patents/txt/ep/0298/216} & Circuit for PCM conversion of an analogic signal, with improvement in gain-tracking. & SGS THOMSON MICROELECTRONICS (IT) & 1987-05-07\\\hline
ep0266800\footnote{/patents/txt/ep/0266/800} & Data processor having different interrupt processing modes & NIPPON ELECTRIC CO (JP) & 1986-11-07\\\hline
ep0270057\footnote{/patents/txt/ep/0270/057} & Apparatus for reproducing data from magnetic recording medium. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1986-11-29\\\hline
ep0338496\footnote{/patents/txt/ep/0338/496} & Method and circuit for detecting data error. & SANYO ELECTRIC CO (JP) & 1988-04-20\\\hline
ep0359607\footnote{/patents/txt/ep/0359/607} & SYSTEM HAVING CONSTANT NUMBER OF TOTAL INPUT AND OUTPUT SHIFT REGISTERS STAGES FOR EACH PROCESSOR TO ACCESS DIFFERENT MEMORY MODULES & BULL SA (FR) & 1988-08-12\\\hline
ep0585438\footnote{/patents/txt/ep/0585/438} & Parallel interface for connecting data processing devices to one another over bidirectional control lines & SIEMENS NIXDORF INF SYST (DE) & 1993-03-08\\\hline
ep0616924\footnote{/patents/txt/ep/0616/924} & Apparatus and method for wireless data and energy transmission & SIEMENS AG (DE) & 1993-03-24\\\hline
ep0420745\footnote{/patents/txt/ep/0420/745} & Digital signal encoding apparatus. & SONY CORP (JP) & 1989-10-25\\\hline
ep0491563\footnote{/patents/txt/ep/0491/563} & Information reproducing apparatus. & PIONEER ELECTRONIC CORP (JP) & 1990-12-18\\\hline
ep0575086\footnote{/patents/txt/ep/0575/086} & An information recording and reproducing apparatus. & CANON KK (JP) & 1992-06-18\\\hline
ep0624971\footnote{/patents/txt/ep/0624/971} & Telecommunication network. & PHILIPS ELECTRONICS NV (NL) & 1993-05-10\\\hline
ep0643517\footnote{/patents/txt/ep/0643/517} & Terminal adapter capable of reducing a memory capacity of a buffer memory. & NIPPON ELECTRIC CO (JP) & 1993-09-09\\\hline
ep0205767\footnote{/patents/txt/ep/0205/767} & Printing apparatus which can specify a font memory irrespective of a mounting position thereof, and a method of specifying a font memory & TOKYO SHIBAURA ELECTRIC CO (JP) & 1985-03-30\\\hline
ep0269147\footnote{/patents/txt/ep/0269/147} & Time-clustered cardio-respiratory encoder and method for clustering cardio-respiratory signals & PHILIPS CORP (US) & 1986-10-22\\\hline
ep0787330\footnote{/patents/txt/ep/0787/330} & Method of locating and representing circuit elements of an electronic circuit for the functional testing of the circuit & TEST PLUS ELETRONIC GMBH (DE) & 1995-10-20\\\hline
ep0772770\footnote{/patents/txt/ep/0772/770} & Determining the biodegradability of iminodiacetic acid derivatives & DOW CHEMICAL CO (US) & 1994-07-27\\\hline
ep0724753\footnote{/patents/txt/ep/0724/753} & \#f & RECIF SA (FR) & 1993-10-21\\\hline
ep0545907\footnote{/patents/txt/ep/0545/907} & Improved synchronous/asynchronous modem. & HAYES MICROCOMPUTER PROD (US) & 1985-11-18\\\hline
ep0685796\footnote{/patents/txt/ep/0685/796} & Device for speeding up the reading of a memory by a processor & SEXTANT AVIONIQUE (FR) & 1994-05-27\\\hline
ep0701717\footnote{/patents/txt/ep/0701/717} & Methods and apparatus relating to the formulation and trading of risk management contracts & SHEPHERD IAN KENNETH & 1993-05-28\\\hline
ep0400218\footnote{/patents/txt/ep/0400/218} & Recording medium playing apparatus with automatic tonal response setting function. & PIONEER ELECTRONIC CORP (JP) & 1989-06-01\\\hline
ep0715412\footnote{/patents/txt/ep/0715/412} & Method and arrangement for determining phase changes of a reference input signal of a phase-locked loop & SIEMENS AG (DE) & 1994-11-28\\\hline
ep0295893\footnote{/patents/txt/ep/0295/893} & Ultrasonic analyzers. & FUJITSU LTD (JP) & 1987-06-19\\\hline
ep0569591\footnote{/patents/txt/ep/0569/591} & POLYSACCHARIDE AND PRODUCTION THEREOF. & STATE OF JAPAN AS REPRESENTED (JP); HAKUTO KK (JP); KYOWA HAKKO KOGYO KK (JP) & 1991-11-29\\\hline
ep0267989\footnote{/patents/txt/ep/0267/989} & Installation for transferring freight, in particular from containers. & HILLMER HELMUT KG (DE); STOLZMANN KLAUS DIETER (DE) & 1986-11-18\\\hline
ep0300502\footnote{/patents/txt/ep/0300/502} & High speed page turning control system. & SHARP KK (JP) & 1987-07-22\\\hline
ep0839434\footnote{/patents/txt/ep/0839/434} & \#f & SIEMENS AG (DE) & 1995-07-18\\\hline
ep0520876\footnote{/patents/txt/ep/0520/876} & Method and system for communicating information within a dwelling or a property & SGS THOMSON MICROELECTRONICS (FR) & 1991-06-24\\\hline
ep0638222\footnote{/patents/txt/ep/0638/222} & PROCESS FOR ORGANISING CONTROL DATA FOR CONTROL PROCEDURES TAKING PLACE BETWEEN AT LEAST ONE TERMINAL AND A SHARED CENTRAL STATION IN COMMUNICATION SYSTEMS & BOZEK GERHARD (AT); SIEMENS AG (DE); BAUMEISTER JOSEF (DE); LINDEMANN WERNER (DE) & 1992-04-30\\\hline
ep0519795\footnote{/patents/txt/ep/0519/795} & A modem having an improved line interface circuit, in particular for a computer. & APPLE COMPUTER (US) & 1991-06-17\\\hline
ep0633547\footnote{/patents/txt/ep/0633/547} & Error detection in seismic data. & WESTERN ATLAS INT INC (US); IBM (US) & 1993-07-02\\\hline
ep0386585\footnote{/patents/txt/ep/0386/585} & Multiplexed digital control device & TOKYO SHIBAURA ELECTRIC CO (JP) & 1989-03-10\\\hline
ep0508098\footnote{/patents/txt/ep/0508/098} & Image forming apparatus. & SHARP KK (JP) & 1991-03-11\\\hline
ep0627103\footnote{/patents/txt/ep/0627/103} & IMAGE TEXTURING SYSTEM HAVING THEME CELLS & EVANS \& SUTHERLAND COMPUTER CO (US) & 1992-02-18\\\hline
ep0675631\footnote{/patents/txt/ep/0675/631} & Anti-counterfeiting device for use in an image-processing apparatus. & SHARP KK (JP) & 1994-03-29\\\hline
ep0189089\footnote{/patents/txt/ep/0189/089} & Radio paging system capable of transmitting common information and receiver therefor & NIPPON ELECTRIC CO (JP) & 1985-01-14\\\hline
ep0222691\footnote{/patents/txt/ep/0222/691} & Arrangement for storage of the fibers of glass-fiber cables in distribution devices in a telecommunication network & KRONE AG (DE) & 1985-11-12\\\hline
ep0298625\footnote{/patents/txt/ep/0298/625} & Method of and apparatus for establishing a servicing mode of an electronic apparatus. & SONY CORP (JP) & 1987-07-08\\\hline
ep0510924\footnote{/patents/txt/ep/0510/924} & Output method and apparatus employing the same. & CANON KK (JP) & 1991-04-23\\\hline
ep0465704\footnote{/patents/txt/ep/0465/704} & Process for preparing characters. & KAROW RUBOW WEBER GMBH (DE) & 1990-07-11\\\hline
ep0651341\footnote{/patents/txt/ep/0651/341} & Circuit designing system. & NIPPON ELECTRIC CO (JP) & 1993-11-01\\\hline
ep0423653\footnote{/patents/txt/ep/0423/653} & Method and apparatus for compensating for color in color images. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1989-10-13\\\hline
ep0359234\footnote{/patents/txt/ep/0359/234} & Display control apparatus for converting CRT resolution into PDP resolution by hardware. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1988-09-13\\\hline
ep0017950\footnote{/patents/txt/ep/0017/950} & Method and device for the identification of objects. & SCANTRON GMBH (DE) & 1979-04-19\\\hline
ep0310876\footnote{/patents/txt/ep/0310/876} & Radio arrangement having two radios sharing circuitry. & MOTOROLA INC (US) & 1987-10-09\\\hline
ep0250608\footnote{/patents/txt/ep/0250/608} & Method and device for azimuth determination using a strap-down gyro. & LITEF LITTON TECH HELLIGE (DE) & 1986-06-18\\\hline
ep0383291\footnote{/patents/txt/ep/0383/291} & Transmission failure diagnosis apparatus. & FURUKAWA ELECTRIC CO LTD (JP); MAZDA MOTOR (JP) & 1989-02-15\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

