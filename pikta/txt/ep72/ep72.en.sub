\begin{subdocument}{swpiktxt72}{Top Software Probability Batch nr 72: 7101-7200}{http://swpat.ffii.org/patents/txt/ep72/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0137414\footnote{/patents/txt/ep/0137/414} & Information processing system and method for use in computer systems suitable for production system & HITACHI LTD (JP) & 1983-09-28\\\hline
ep0520454\footnote{/patents/txt/ep/0520/454} & Display control system for determining connection of optional display unit by using palette. & TOKYO SHIBAURA ELECTRIC CO (JP); TOSHIBA COMPUTER ENG (JP) & 1991-06-26\\\hline
ep0533196\footnote{/patents/txt/ep/0533/196} & Version-up method and system. & HITACHI LTD (JP) & 1991-09-20\\\hline
ep0553098\footnote{/patents/txt/ep/0553/098} & EDI TRANSLATION SYSTEM USING PLURALITY OF COMMUNICATION PROCESSES AND DE-ENVELOPING PROCEDURE CORRESPONDING TO TRANSMITTED COMMUNICATION PROCESS & PREMENOS CORP (US) & 1990-07-13\\\hline
ep0120387\footnote{/patents/txt/ep/0120/387} & Method and apparatus for vectorizing documents and symbol recognition & ANA TECH CORP (US) & 1983-03-14\\\hline
ep0576646\footnote{/patents/txt/ep/0576/646} & Method of ordering, shipping and merchandising goods and shipping/display assembly therefor & DURACELL INC (US) & 1992-01-10\\\hline
ep0603880\footnote{/patents/txt/ep/0603/880} & Method and system for aggregating objects. & MICROSOFT CORP (US) & 1992-12-24\\\hline
ep0591419\footnote{/patents/txt/ep/0591/419} & Method and apparatus for expanding a backplane interconnecting bus in a multiprocessor computer system without additional byte select signals & AST RESEARCH INC (US) & 1991-06-26\\\hline
ep0597013\footnote{/patents/txt/ep/0597/013} & APPARATUS AND METHOD FOR FRAME SWITCHING & TANDEM COMPUTERS INC (US) & 1991-07-26\\\hline
ep0601659\footnote{/patents/txt/ep/0601/659} & Method for protecting a smart card system. & NEDERLAND PTT (NL) & 1992-12-07\\\hline
ep0165381\footnote{/patents/txt/ep/0165/381} & Portable information device having an output related to natural physical events & DOULTON ROMM (MC); CHAPMAN ROBERT A (GB) & 1981-06-16\\\hline
ep0073781\footnote{/patents/txt/ep/0073/781} & Portable information device having an output related to natural physical events & DOULTON ROMM (MC); CHAPMAN ROBERT A (GB) & 1981-06-16\\\hline
ep0113720\footnote{/patents/txt/ep/0113/720} & COLOR CODED SYMBOLIC ALPHANUMERIC SYSTEM & KREMPEL RALF & 1982-07-22\\\hline
ep0089596\footnote{/patents/txt/ep/0089/596} & Digital timing unit & HONEYWELL INF SYSTEMS (IT) & 1982-03-22\\\hline
ep0718781\footnote{/patents/txt/ep/0718/781} & Device for connecting two different computersystems & SIEMENS AG (DE) & 1994-12-23\\\hline
ep0826174\footnote{/patents/txt/ep/0826/174} & Method and apparatus using a tree structure for the dispatching of interrupts & APPLE COMPUTER (US) & 1995-05-05\\\hline
ep0231452\footnote{/patents/txt/ep/0231/452} & Microprocessor systems for electronic postage arrangements. & PITNEY BOWES (US) & 1982-01-29\\\hline
ep0397472\footnote{/patents/txt/ep/0397/472} & Rotary head recording and playback apparatus and method. & MITSUBISHI ELECTRIC CORP (JP) & 1989-12-26\\\hline
ep0454585\footnote{/patents/txt/ep/0454/585} & An antenna selection diversity reception system. & NIPPON TELEGRAPH \& TELEPHONE (JP) & 1990-04-27\\\hline
ep0449303\footnote{/patents/txt/ep/0449/303} & Phase difference auto focusing for synthetic aperture radar imaging & HUGHES AIRCRAFT CO (US) & 1990-03-29\\\hline
ep0507579\footnote{/patents/txt/ep/0507/579} & Multiplexed transmission between nodes with cyclic redundancy check calculation. & MAZDA MOTOR (JP); FURUKAWA ELECTRIC CO LTD (JP) & 1991-08-23\\\hline
ep0736846\footnote{/patents/txt/ep/0736/846} & Microprocessor systems for electronic postage arrangements & PITNEY BOWES (US) & 1982-01-29\\\hline
ep0257585\footnote{/patents/txt/ep/0257/585} & Key distribution method & NIPPON ELECTRIC CO (JP) & 1986-08-22\\\hline
ep0246791\footnote{/patents/txt/ep/0246/791} & Transducer control in vehicle braking systems & LUCAS IND PLC (GB) & 1986-05-17\\\hline
ep0264148\footnote{/patents/txt/ep/0264/148} & Flow measurement and monitoring system for positive-displacement pumps and pumps equipped with this system. & PUMPTECH NV (BE) & 1986-10-08\\\hline
ep0367177\footnote{/patents/txt/ep/0367/177} & Multiplex transmission system for automotive vehicles. & FURUKAWA ELECTRIC CO LTD (JP); MAZDA MOTOR (JP) & 1988-10-31\\\hline
ep0341889\footnote{/patents/txt/ep/0341/889} & Automated vehicle control. & GEN ELECTRIC CO PLC (GB) & 1988-05-13\\\hline
ep0319268\footnote{/patents/txt/ep/0319/268} & Clinical configuration of multimode medication infusion system. & PACESETTER INFUSION LTD (US) & 1987-12-04\\\hline
ep0392055\footnote{/patents/txt/ep/0392/055} & Priority mapper for initiation of radar tracking of projectiles. & INT STANDARD ELECTRIC CORP (US) & 1984-09-21\\\hline
ep0489929\footnote{/patents/txt/ep/0489/929} & TRANSMITTING DEVICE FOR CATV. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1990-07-02\\\hline
ep0448287\footnote{/patents/txt/ep/0448/287} & Method and apparatus for pixel clipping source and destination windows in a graphics system. & HEWLETT PACKARD CO (US) & 1990-03-16\\\hline
ep0456265\footnote{/patents/txt/ep/0456/265} & Procedure for the selection of an elevator in an elevator group. & KONE ELEVATOR GMBH (CH) & 1990-05-10\\\hline
ep0514736\footnote{/patents/txt/ep/0514/736} & Advisory display board for airport taxi- and runways & TELEFUNKEN SYSTEMTECHNIK (DE) & 1991-05-23\\\hline
ep0525440\footnote{/patents/txt/ep/0525/440} & Stroke control device for an apparatus for damping and stacking moulded blocks. & LANGENSTEIN \& SCHEMANN GMBH (DE) & 1991-07-19\\\hline
ep0534565\footnote{/patents/txt/ep/0534/565} & An implement for and a method of milking animals automatically. & LELY NV C VAN DER (NL) & 1991-09-27\\\hline
ep0534564\footnote{/patents/txt/ep/0534/564} & An implement for and a method of milking animals automatically. & LELY NV C VAN DER (NL) & 1991-09-27\\\hline
ep0556641\footnote{/patents/txt/ep/0556/641} & Compressor clutch cut-out in an automotive air conditioning system. & EATON CORP (US) & 1992-02-19\\\hline
ep0554640\footnote{/patents/txt/ep/0554/640} & Process and a device for determining the erosion caused by cavitation in components through which fluid flows & GUELICH JOHANN FRIEDRICH (CH) & 1992-02-07\\\hline
ep0596513\footnote{/patents/txt/ep/0596/513} & Ultrasound brain lesioning system. & LAB EQUIPMENT CORP (US) & 1988-03-02\\\hline
ep0625782\footnote{/patents/txt/ep/0625/782} & A re-edit function allowing bi-directional rippling. & SONY ELECTRONICS INC (US) & 1993-04-15\\\hline
ep0645603\footnote{/patents/txt/ep/0645/603} & Land vehicle multiple navigation route apparatus. & MOTOROLA INC (US) & 1990-11-08\\\hline
ep0650866\footnote{/patents/txt/ep/0650/866} & System and device for vehicle to get out of a parking place. & BOSCH GMBH ROBERT (DE) & 1993-09-29\\\hline
ep0660385\footnote{/patents/txt/ep/0660/385} & Intelligent test line system. & TOSHIBA MICRO ELECTRONICS (JP); TOKYO SHIBAURA ELECTRIC CO (JP) & 1993-12-14\\\hline
ep0770944\footnote{/patents/txt/ep/0770/944} & Method for the automated generation of structures of technical processes & ABB PATENT GMBH (DE) & 1995-10-24\\\hline
ep0310045\footnote{/patents/txt/ep/0310/045} & Ophthalmologic apparatus. & CANON KK (JP) & 1987-09-30\\\hline
ep0339929\footnote{/patents/txt/ep/0339/929} & Recording medium playing apparatus. & PIONEER ELECTRONIC CORP (JP); PIONEER VIDEO CORP (JP) & 1988-04-25\\\hline
ep0283927\footnote{/patents/txt/ep/0283/927} & Display adapter. & IBM (US) & 1987-03-27\\\hline
ep0284905\footnote{/patents/txt/ep/0284/905} & Display system. & IBM (US) & 1987-04-02\\\hline
ep0378694\footnote{/patents/txt/ep/0378/694} & RESPONSE CONTROL SYSTEM. & FUJITSU LTD (JP) & 1988-06-14\\\hline
ep0507500\footnote{/patents/txt/ep/0507/500} & Engine protection system. & CUMMINS ENGINE CO INC (US) & 1991-03-29\\\hline
ep0604755\footnote{/patents/txt/ep/0604/755} & Color matching method and apparatus. & DU PONT (US) & 1992-12-15\\\hline
ep0632599\footnote{/patents/txt/ep/0632/599} & Purging apparatus having a battery saving function. & MOTOROLA INC (US) & 1988-09-12\\\hline
ep0647546\footnote{/patents/txt/ep/0647/546} & CAR-MOUNTED SOUND DEVICE. & SHINTOM KK (JP) & 1993-04-30\\\hline
ep0467685\footnote{/patents/txt/ep/0467/685} & Automatic white-balance controlling apparatus. & MITSUBISHI ELECTRIC CORP (JP) & 1990-11-13\\\hline
ep0471615\footnote{/patents/txt/ep/0471/615} & Control apparatus for systematically operating audio and/or video sets. & SONY CORP (JP) & 1990-08-10\\\hline
ep0699540\footnote{/patents/txt/ep/0699/540} & Printer system with automatic ink ribbon cassette exchange function & SHARP KK (JP) & 1994-09-22\\\hline
ep0631433\footnote{/patents/txt/ep/0631/433} & Video camera and method of controlling the same. & CANON KK (JP) & 1993-06-21\\\hline
ep0648645\footnote{/patents/txt/ep/0648/645} & An electric control apparatus for an air-bag system. & FUJITSU TEN LTD (JP) & 1993-10-15\\\hline
ep0700203\footnote{/patents/txt/ep/0700/203} & Video signal recording apparatus using a touchscreen & SONY CORP (JP) & 1994-09-05\\\hline
ep0181517\footnote{/patents/txt/ep/0181/517} & Demodulator for an asynchronous binary signal & AMPEX (US) & 1982-04-02\\\hline
ep0524235\footnote{/patents/txt/ep/0524/235} & Method of synchronizing the pseudo-random binary sequence in a descrambler & BRITISH TELECOMM (GB) & 1990-04-10\\\hline
ep0252499\footnote{/patents/txt/ep/0252/499} & Method, apparatus and article for identification and signature & YEDA RES \& DEV (IL) & 1986-07-09\\\hline
ep0622797\footnote{/patents/txt/ep/0622/797} & Control system for disc playing device. & SANYO ELECTRIC CO (JP) & 1993-04-28\\\hline
ep0532322\footnote{/patents/txt/ep/0532/322} & Television apparatus with hierarchy menu operation. & SONY CORP (JP) & 1991-09-10\\\hline
ep0109306\footnote{/patents/txt/ep/0109/306} & Cache memory apparatus for computer. & STORAGE TECHNOLOGY CORP (US) & 1982-11-15\\\hline
ep0859995\footnote{/patents/txt/ep/0859/995} & METHOD FOR SELECTING JPEG QUANTIZATION FACTOR & SIEMENS MEDICAL SYSTEMS INC (US) & 1995-11-06\\\hline
ep0891589\footnote{/patents/txt/ep/0891/589} & DEVICE FOR COMPILING A DIGITAL DICTIONARY AND PROCESS FOR COMPILING A DIGITAL DICTIONARY BY MEANS OF A COMPUTER & SIEMENS AG (DE); KLEINSCHMIDT PETER (DE) & 1996-04-02\\\hline
ep0567711\footnote{/patents/txt/ep/0567/711} & Support to connectionless services in ATM network using partial connections & IBM (US) & 1992-04-27\\\hline
ep0438168\footnote{/patents/txt/ep/0438/168} & A business monitoring system and method. & HITACHI LTD (JP) & 1990-01-19\\\hline
ep0203601\footnote{/patents/txt/ep/0203/601} & Magnetic disk controller incorporating a cache system adopting an LRU system. & TOKYO SHIBAURA ELECTRIC CO (JP); TOSHIBA COMPUTER ENG (JP) & 1985-08-29\\\hline
ep0192202\footnote{/patents/txt/ep/0192/202} & Simplified cache with automatic update & WANG LABORATORIES (US) & 1985-02-22\\\hline
ep0452991\footnote{/patents/txt/ep/0452/991} & Magnetic disk controller incorporating a cache system adopting an LRU system. & TOKYO SHIBAURA ELECTRIC CO (JP); TOSHIBA COMPUTER ENG (JP) & 1985-08-29\\\hline
ep0452990\footnote{/patents/txt/ep/0452/990} & Magnetic disk controller incorporating a cache system adopting an LRU system. & TOKYO SHIBAURA ELECTRIC CO (JP); TOSHIBA COMPUTER ENG (JP) & 1985-08-29\\\hline
ep0452989\footnote{/patents/txt/ep/0452/989} & Magnetic disk controller incorporating a cache system adopting an LRU system. & TOKYO SHIBAURA ELECTRIC CO (JP); TOSHIBA COMPUTER ENG (JP) & 1985-08-29\\\hline
ep0461832\footnote{/patents/txt/ep/0461/832} & Method and apparatus for generating character pattern. & CANON KK (JP) & 1990-06-11\\\hline
ep0431787\footnote{/patents/txt/ep/0431/787} & Self-synchronizing servo control system and servo data code for high density disk drives. & DIGITAL EQUIPMENT CORP (US) & 1989-12-05\\\hline
ep0427528\footnote{/patents/txt/ep/0427/528} & Bar code readers. & FUJITSU LTD (JP) & 1989-11-07\\\hline
ep0479944\footnote{/patents/txt/ep/0479/944} & Photographic film with latent image multi-field bar code and eye-readable symbols & EASTMAN KODAK CO (US) & 1989-06-28\\\hline
ep0220101\footnote{/patents/txt/ep/0220/101} & Transport apparatus for transporting part-carrying members to various work stations and for reading data encoded on said part-carrying member & JICE AUTOMAT SOC (FR) & 1985-10-01\\\hline
ep0346766\footnote{/patents/txt/ep/0346/766} & Method for reducing blocking artifacts in video scene coding with discrete cosine transformation (DCT) at a low data rate & SIEMENS AG (DE) & 1988-06-14\\\hline
ep0334868\footnote{/patents/txt/ep/0334/868} & DIGITAL SIGNAL CODING & BRITISH TELECOMM (GB) & 1986-12-01\\\hline
ep0321318\footnote{/patents/txt/ep/0321/318} & Method and coding and decoding devices for picture transmission through a variable rate network & FRANCE ETAT (FR) & 1987-12-16\\\hline
ep0388889\footnote{/patents/txt/ep/0388/889} & Block transformation coding system. & FUJITSU LTD (JP) & 1989-03-20\\\hline
ep0431319\footnote{/patents/txt/ep/0431/319} & Video signal coding apparatus, coding method and video signal coding transmission system. & FUJITSU LTD (JP) & 1989-11-06\\\hline
ep0502287\footnote{/patents/txt/ep/0502/287} & Method and apparatus for removing coding/decoding distortion from moving-picture image data. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1991-03-04\\\hline
ep0467678\footnote{/patents/txt/ep/0467/678} & Variable length coding apparatus and variable length decoding apparatus. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1990-07-18\\\hline
ep0549813\footnote{/patents/txt/ep/0549/813} & CODING AND DECODING DEVICE FOR TIME-VARYING IMAGE. & SONY CORP (JP) & 1991-07-19\\\hline
ep0584865\footnote{/patents/txt/ep/0584/865} & Coding for a multilevel transmission system. & PHILIPS ELECTRONICS UK LTD (GB); KONINKL PHILIPS ELECTRONICS NV (NL) & 1992-08-21\\\hline
ep0709005\footnote{/patents/txt/ep/0709/005} & Computationally efficient adaptive bit allocation for coding method and apparatus & DOLBY LAB LICENSING CORP (US) & 1993-07-16\\\hline
ep0341800\footnote{/patents/txt/ep/0341/800} & Electronic audio communication system with user controlled message address. & VMX INC (US) & 1982-09-29\\\hline
ep0336524\footnote{/patents/txt/ep/0336/524} & Electronic audio communication system with user controlled message address. & VMX INC (US) & 1982-09-29\\\hline
ep0396090\footnote{/patents/txt/ep/0396/090} & System for communication between a master and slave processing units. & SGS THOMSON MICROELECTRONICS (IT) & 1989-05-04\\\hline
ep0394596\footnote{/patents/txt/ep/0394/596} & Interconnection system for the attachment of user equipments to a communication processing unit. & IBM (US) & 1989-04-25\\\hline
ep0382362\footnote{/patents/txt/ep/0382/362} & Methods and apparatus for performing time interleaved multiplexed rate adaptation for sub-rate channels in a digital data communication system. & DATA GENERAL CORP (US) & 1989-02-09\\\hline
ep0365885\footnote{/patents/txt/ep/0365/885} & Satellite cellular telephone and data communication system. & MOTOROLA INC (US) & 1988-10-28\\\hline
ep0364447\footnote{/patents/txt/ep/0364/447} & Signalling method for establishing trunked communication & MOTOROLA INC (US) & 1987-06-19\\\hline
ep0359183\footnote{/patents/txt/ep/0359/183} & System and method of transmitting a complex waveform over a communication channel utilizing lincompex techniques. & AMAF IND INC (US) & 1988-09-12\\\hline
ep0500450\footnote{/patents/txt/ep/0500/450} & Apparatus and method for establishing communication between ISDN data terminals and other terminals served by a switched telephone network & ALCATEL BUSINESS SYSTEMS (FR) & 1991-02-20\\\hline
ep0614323\footnote{/patents/txt/ep/0614/323} & Method and apparatus for transmitting a high bit rate data flow over N independent digital communication channels. & IBM (US) & 1993-03-02\\\hline
ep0577435\footnote{/patents/txt/ep/0577/435} & Common interface for a communication network. & DIGITAL EQUIPMENT CORP (US) & 1992-07-02\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

