\begin{subdocument}{swpiktxt82}{Top Software Probability Batch nr 82: 8101-8200}{http://swpat.ffii.org/patents/txt/ep82/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0266911\footnote{/patents/txt/ep/0266/911} & SYSTEM FOR CHANGING PRINT FORMAT & SHARP KK (JP) & 1986-10-06\\\hline
ep0346388\footnote{/patents/txt/ep/0346/388} & \#f &  & 1987-02-27\\\hline
ep0411921\footnote{/patents/txt/ep/0411/921} & Image processing apparatus. & CANON KK (JP) & 1989-08-02\\\hline
ep0416886\footnote{/patents/txt/ep/0416/886} & Image recording apparatus. & CANON KK (JP) & 1989-09-06\\\hline
ep0452613\footnote{/patents/txt/ep/0452/613} & Image information display apparatus. & PIONEER ELECTRONIC CORP (JP) & 1990-04-17\\\hline
ep0469882\footnote{/patents/txt/ep/0469/882} & Image processing apparatus. & CANON KK (JP) & 1990-08-03\\\hline
ep0502369\footnote{/patents/txt/ep/0502/369} & Method of and apparatus for processing image by setting up image processing conditions on the basis of finishing information. & FUJI PHOTO FILM CO LTD (JP) & 1991-02-21\\\hline
ep0569896\footnote{/patents/txt/ep/0569/896} & Three-dimensional image. & POLAROID CORP (US) & 1992-05-11\\\hline
ep0605210\footnote{/patents/txt/ep/0605/210} & Image processing method and apparatus and facsimile. & CANON KK (JP) & 1992-12-28\\\hline
ep0642060\footnote{/patents/txt/ep/0642/060} & Image processing apparatus. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1993-09-03\\\hline
ep0653724\footnote{/patents/txt/ep/0653/724} & Fingerprint image cutout processing device for tenprint card. & NIPPON ELECTRIC CO (JP) & 1993-11-12\\\hline
ep0683596\footnote{/patents/txt/ep/0683/596} & Card type camera with image processing function. & SHARP KK (JP) & 1994-05-25\\\hline
ep0690613\footnote{/patents/txt/ep/0690/613} & System for compressing a half-tone image and method therefor & SEIKO EPSON CORP (JP) & 1994-06-27\\\hline
ep0170336\footnote{/patents/txt/ep/0170/336} & Data transmission arrangement including a reconfiguration facility & PHILIPS CORP (US) & 1984-07-28\\\hline
ep0245996\footnote{/patents/txt/ep/0245/996} & Method of and switch for switching information & NORTHERN TELECOM LTD (CA) & 1986-05-14\\\hline
ep0235525\footnote{/patents/txt/ep/0235/525} & Statistical information access system. & IBM (US) & 1986-02-14\\\hline
ep0230027\footnote{/patents/txt/ep/0230/027} & Magnet shimming using information derived from chemical shift imaging. & GEN ELECTRIC (US) & 1986-01-03\\\hline
ep0289914\footnote{/patents/txt/ep/0289/914} & Method of recording and reproducing information on and from a recording disk. & PIONEER ELECTRONIC CORP (JP) & 1987-05-07\\\hline
ep0281225\footnote{/patents/txt/ep/0281/225} & Secure information storage. & HEWLETT PACKARD CO (US) & 1987-03-03\\\hline
ep0277380\footnote{/patents/txt/ep/0277/380} & System and method of adjusting the interstation delay in an information transmission system & TRT TELECOM RADIO ELECTR (FR) & 1986-12-19\\\hline
ep0274255\footnote{/patents/txt/ep/0274/255} & Video disc reproducing device and method of reproducing video information. & PIONEER ELECTRONIC CORP (JP) & 1986-12-16\\\hline
ep0442566\footnote{/patents/txt/ep/0442/566} & Information recording device. & PHILIPS NV (NL) & 1990-02-12\\\hline
ep0438298\footnote{/patents/txt/ep/0438/298} & Information processing apparatus. & SONY CORP (JP) & 1990-01-19\\\hline
ep0437731\footnote{/patents/txt/ep/0437/731} & TV receiver apparatus with a transmitter scanning device and a memory for storing individual items of TV transmitter information. & BOSCH SIEMENS HAUSGERAETE (DE) & 1989-12-15\\\hline
ep0410137\footnote{/patents/txt/ep/0410/137} & Control method and-device for an information reproducing apparatus dependant on location. & BOSCH GMBH ROBERT (DE) & 1989-07-28\\\hline
ep0519361\footnote{/patents/txt/ep/0519/361} & CONTINUOUS-WAVE RADAR SET USABLE AS A TRANSMITTER FOR INFORMATION TRANSMISSION & ALCATEL N V THE NETHERLANDS (NL) & 1991-06-21\\\hline
ep0497479\footnote{/patents/txt/ep/0497/479} & Method of and apparatus for generating auxiliary information for expediting sparse codebook search. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1991-01-28\\\hline
ep0477874\footnote{/patents/txt/ep/0477/874} & Information recording method. & CANON KK (JP) & 1990-09-25\\\hline
ep0522047\footnote{/patents/txt/ep/0522/047} & METHOD FOR IMBEDDING INFORMATION IN MODEM HANDSHAKE PROCEDURE AND MODEMS INCORPORATING THE SAME & GEN DATACOMM IND INC (US) & 1990-03-28\\\hline
ep0621957\footnote{/patents/txt/ep/0621/957} & Method of and apparatus for obtaining spatial NMR information & BRITISH TECH GROUP (GB) & 1993-01-12\\\hline
ep0583368\footnote{/patents/txt/ep/0583/368} & METHOD AND APPARATUS FOR TRANSLATING SIGNALING INFORMATION & RAYNET CORP (US) & 1991-05-09\\\hline
ep0715287\footnote{/patents/txt/ep/0715/287} & Method and device for obtaining information about the zone around a vehicle & MANNESMANN AG (DE) & 1995-06-09\\\hline
ep0813727\footnote{/patents/txt/ep/0813/727} & \#f & LEICA SENSORTECHNIK GMBH (DE) & 1995-03-09\\\hline
ep0769186\footnote{/patents/txt/ep/0769/186} & Winding hub for information carriers in tape form & EMTEC MAGNETICS GMBH (DE) & 1995-06-21\\\hline
ep0368269\footnote{/patents/txt/ep/0368/269} & Magnetic media containing reference feature and methods for referencing magnetic head position to the reference feature. & INSITE PERIPHERALS (US) & 1988-11-10\\\hline
ep0792032\footnote{/patents/txt/ep/0792/032} & Network restoration method & AT \& T CORP (US) & 1996-02-23\\\hline
ep0769237\footnote{/patents/txt/ep/0769/237} & Method and apparatus for key transforms to discriminate between different networks & ERICSSON GE MOBILE INC (US) & 1994-07-05\\\hline
ep0673159\footnote{/patents/txt/ep/0673/159} & Scheduling policies with grouping for providing VCR control functions in a video server. & IBM (US) & 1994-03-15\\\hline
ep0712370\footnote{/patents/txt/ep/0712/370} & Device for decollating cards from a stack of cards & BOEWE SYSTEC AG (DE) & 1994-08-09\\\hline
ep0483788\footnote{/patents/txt/ep/0483/788} & A synchronous terminal station system. & NIPPON ELECTRIC CO (JP) & 1990-10-31\\\hline
ep0109574\footnote{/patents/txt/ep/0109/574} & Text representation system. & SIEMENS AG (DE) & 1982-10-27\\\hline
ep0274406\footnote{/patents/txt/ep/0274/406} & Self-configuration of nodes in a distributed message-based operating system. & COMPUTER X INC (US) & 1987-01-05\\\hline
ep0229317\footnote{/patents/txt/ep/0229/317} & System for reading and writing information & BROOKTREE CORP (US) & 1985-12-18\\\hline
ep0458304\footnote{/patents/txt/ep/0458/304} & Direct memory access transfer controller and use & NIPPON ELECTRIC CO (JP) & 1990-05-22\\\hline
ep0420660\footnote{/patents/txt/ep/0420/660} & A disc memory apparatus. & SONY CORP (JP) & 1989-09-29\\\hline
ep0567563\footnote{/patents/txt/ep/0567/563} & Mixed-resolution, N-dimensional object space method and apparatus & WALKER ESTES CORP (US) & 1991-01-16\\\hline
ep0614192\footnote{/patents/txt/ep/0614/192} & Memory element. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1993-02-18\\\hline
ep0559336\footnote{/patents/txt/ep/0559/336} & Apparatus for controlling a current supply device. & ADVANCED MICRO DEVICES INC (US) & 1992-03-02\\\hline
ep0489511\footnote{/patents/txt/ep/0489/511} & Method and apparatus for diagnosing failures during boundary-scan tests. & HEWLETT PACKARD CO (US) & 1990-12-04\\\hline
ep0357383\footnote{/patents/txt/ep/0357/383} & Output apparatus. & CANON KK (JP) & 1988-08-31\\\hline
ep0063151\footnote{/patents/txt/ep/0063/151} & MANUAL DATA STORAGE MEANS FOR DETERMINING CONNECTIONS BETWEEN SETS OF DATA & LICENCIA TALALMANYOKAT (HU); BALINT GEZA (HU); BOZSOKY SANDOR (HU); DOMJAN LASZLO (HU) & 1980-10-22\\\hline
ep0147858\footnote{/patents/txt/ep/0147/858} & Vector processing apparatus including means for identifying the occurrence of exceptions in the processing of vector elements & HITACHI LTD (JP) & 1983-12-26\\\hline
ep0128156\footnote{/patents/txt/ep/0128/156} & Data processor version validation & MOTOROLA INC (US) & 1982-12-07\\\hline
ep0344497\footnote{/patents/txt/ep/0344/497} & Design procedure supporting method and system. & HITACHI LTD (JP) & 1988-05-13\\\hline
ep0318176\footnote{/patents/txt/ep/0318/176} & Imaging methods and apparatus. & PICKER INT INC (US) & 1987-11-27\\\hline
ep0224957\footnote{/patents/txt/ep/0224/957} & Method of and device for estimating motion in a sequence of pictures & PHILIPS CORP (US) & 1985-11-22\\\hline
ep0392411\footnote{/patents/txt/ep/0392/411} & A control apparatus for automobiles. & HITACHI LTD (JP) & 1989-04-14\\\hline
ep0461878\footnote{/patents/txt/ep/0461/878} & Non-contact IC card and signal processing method thereof. & MITSUBISHI ELECTRIC CORP (JP) & 1990-10-01\\\hline
ep0303856\footnote{/patents/txt/ep/0303/856} & Maintaining duplex-paired devices by means of a dual copy function. & IBM (US) & 1987-08-25\\\hline
ep0323228\footnote{/patents/txt/ep/0323/228} & Route end node series preparing system of navigation apparatus. & AISIN AW CO (JP); SHINSANGYO KAIHATSU KK (JP) & 1987-12-28\\\hline
ep0401992\footnote{/patents/txt/ep/0401/992} & Method and apparatus for speeding branch instructions. & ADVANCED MICRO DEVICES INC (US) & 1989-06-06\\\hline
ep0455922\footnote{/patents/txt/ep/0455/922} & Method and apparatus for deriving mirrored unit state when re-initializing a system. & IBM (US) & 1990-05-11\\\hline
ep0578207\footnote{/patents/txt/ep/0578/207} & Method and system for naming and binding objects. & MICROSOFT CORP (US) & 1992-07-06\\\hline
ep0649105\footnote{/patents/txt/ep/0649/105} & Word/number and number/word mapping. & XEROX CORP (US) & 1993-10-19\\\hline
ep0483991\footnote{/patents/txt/ep/0483/991} & Training system. & HUGHES AIRCRAFT CO (US) & 1990-10-30\\\hline
ep0506594\footnote{/patents/txt/ep/0506/594} & Bi-directional parallel printer interface. & IBM (US) & 1991-03-26\\\hline
ep0523652\footnote{/patents/txt/ep/0523/652} & Electronic apparatus with resume function. & CANON KK (JP) & 1991-07-16\\\hline
ep0538020\footnote{/patents/txt/ep/0538/020} & A chair with a supplemental keyboard and a keyboard system. & FUJITSU LTD (JP) & 1991-10-14\\\hline
ep0588339\footnote{/patents/txt/ep/0588/339} & Method and apparatus for settlement of accounts by IC cards. & NIPPON TELEGRAPH \& TELEPHONE (JP) & 1992-11-26\\\hline
ep0610999\footnote{/patents/txt/ep/0610/999} & X-ray device & PHILIPS CORP (US) & 1993-02-09\\\hline
ep0739509\footnote{/patents/txt/ep/0739/509} & Arrangement with master and slave units & SIEMENS AG (DE) & 1995-06-06\\\hline
ep0373277\footnote{/patents/txt/ep/0373/277} & Multifrequency modem using trellis-coded modulation. & IBM (US) & 1988-12-13\\\hline
ep0280827\footnote{/patents/txt/ep/0280/827} & Pitch detection process and speech coder using said process. & IBM (US) & 1987-03-05\\\hline
ep0395125\footnote{/patents/txt/ep/0395/125} & A PCM recording and reproducing apparatus. & MITSUBISHI ELECTRIC CORP (JP) & 1985-10-11\\\hline
ep0409516\footnote{/patents/txt/ep/0409/516} & Apparatus for deriving synchronizing signal from pre-formed marks on record carrier. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1989-07-19\\\hline
ep0256498\footnote{/patents/txt/ep/0256/498} & Write compensator for magnetic disk apparatus & TOKYO SHIBAURA ELECTRIC CO (JP) & 1986-08-20\\\hline
ep0720142\footnote{/patents/txt/ep/0720/142} & Automatic performance device & YAMAHA CORP (JP) & 1994-12-26\\\hline
ep0389689\footnote{/patents/txt/ep/0389/689} & Method for transmitting a transmission signal and a transmitting device and a receiving device for use in the method. & PHILIPS \& DU PONT OPTICAL (DE) & 1989-03-28\\\hline
ep0750304\footnote{/patents/txt/ep/0750/304} & Recording medium and reproducing apparatus thereof & TOSHIBA AVE KK (JP); TOKYO SHIBAURA ELECTRIC CO (JP) & 1995-03-15\\\hline
ep0461279\footnote{/patents/txt/ep/0461/279} & Method for routing packets by squelched flooding. & METRICOM INC (US) & 1989-04-11\\\hline
ep0573192\footnote{/patents/txt/ep/0573/192} & Method and apparatus for muting switching noise while switching between CODECS. & CANON KK (JP) & 1992-05-25\\\hline
ep0613280\footnote{/patents/txt/ep/0613/280} & Technique for administering personal telephone numbers. & AT \& T CORP (US) & 1993-02-26\\\hline
ep0175255\footnote{/patents/txt/ep/0175/255} & Transmission method in staff location systems. & TATECO AB (SE) & 1984-09-17\\\hline
ep0170638\footnote{/patents/txt/ep/0170/638} & Method of and apparatus for transmitting a clock signal via cable over long distances. & BORDONI UGO FONDAZIONE (IT) & 1984-08-02\\\hline
ep0250138\footnote{/patents/txt/ep/0250/138} & Servo system for a hard disk drive. & SONY CORP (JP) & 1986-06-16\\\hline
ep0247772\footnote{/patents/txt/ep/0247/772} & Thermal printers. & SONY CORP (JP) & 1986-05-24\\\hline
ep0279693\footnote{/patents/txt/ep/0279/693} & Multi-plane video ram. & FUJITSU LTD (JP) & 1987-02-20\\\hline
ep0279053\footnote{/patents/txt/ep/0279/053} & Method for the transmission and reproduction of sequences of television pictures. & ANT NACHRICHTENTECH (DE) & 1987-02-16\\\hline
ep0292962\footnote{/patents/txt/ep/0292/962} & Header driven type packet switching system. & FUJITSU LTD (JP) & 1987-05-26\\\hline
ep0314398\footnote{/patents/txt/ep/0314/398} & Navigation apparatus based on present position calculating system. & AISIN AW CO (JP) & 1987-10-30\\\hline
ep0324886\footnote{/patents/txt/ep/0324/886} & Control flow reduction in selective repeat protocols. & IBM (US) & 1988-01-22\\\hline
ep0331107\footnote{/patents/txt/ep/0331/107} & Method for transcribing music and apparatus therefore. & NIPPON DENKI HOME ELECTRONICS (JP); NIPPON ELECTRIC CO (JP) & 1988-02-29\\\hline
ep0338455\footnote{/patents/txt/ep/0338/455} & Elektronische Registrierkasse mit Speicherdefekterkennung. & SHARP KK (JP) & 1988-04-16\\\hline
ep0335209\footnote{/patents/txt/ep/0335/209} & Method for printing and applying labels. & HOBART CORP (US) & 1988-03-31\\\hline
ep0350918\footnote{/patents/txt/ep/0350/918} & Electronic switching system having call-forwarding function. & FUJITSU LTD (JP) & 1988-07-13\\\hline
ep0355217\footnote{/patents/txt/ep/0355/217} & A hand-held video recording camera having internal audio source with synchronized restart. & BLAZEK JOHN MATTHEW (US); REYNOLDS TIPTON JOHN JR (US) & 1987-06-10\\\hline
ep0358275\footnote{/patents/txt/ep/0358/275} & Pseudo line locked write clock for picture-in-picture video applications. & PHILIPS CORP (US) & 1988-09-07\\\hline
ep0365306\footnote{/patents/txt/ep/0365/306} & Disc recording apparatus. & SONY CORP (JP) & 1988-10-20\\\hline
ep0363122\footnote{/patents/txt/ep/0363/122} & Transaction authentication system. & FUJITSU LTD (JP) & 1988-10-03\\\hline
ep0292639\footnote{/patents/txt/ep/0292/639} & Method for giving a risk of slipperiness formation prewarning signal on a traffic surface. & ANT NACHRICHTENTECH (DE) & 1987-02-17\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

