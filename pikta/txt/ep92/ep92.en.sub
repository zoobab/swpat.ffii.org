\begin{subdocument}{swpiktxt92}{Top Software Probability Batch nr 92: 9101-9200}{http://swpat.ffii.org/patents/txt/ep92/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0570983\footnote{/patents/txt/ep/0570/983} & Hardware encryption apparatus using multiple linear transformations. & NIPPON ELECTRIC CO (JP) & 1992-05-21\\\hline
ep0647527\footnote{/patents/txt/ep/0647/527} & An ink container, an ink jet cartridge and ink jet recording apparatus. & CANON KK (JP) & 1993-10-04\\\hline
ep0345452\footnote{/patents/txt/ep/0345/452} & Method of manufacturing ball joints. & TRW EHRENREICH GMBH (DE) & 1988-06-08\\\hline
ep0424167\footnote{/patents/txt/ep/0424/167} & Particle classification. & DE BEERS IND DIAMOND (ZA) & 1989-10-19\\\hline
ep0474379\footnote{/patents/txt/ep/0474/379} & Method and apparatus for analyzing nodes on a computer network. & HEWLETT PACKARD CO (US) & 1990-09-04\\\hline
ep0474283\footnote{/patents/txt/ep/0474/283} & Switching network for an asynchronous time-division multiplex transmission system & PHILIPS CORP (US) & 1990-08-31\\\hline
ep0274944\footnote{/patents/txt/ep/0274/944} & CHARACTER RECOGNITION METHOD AND APPARATUS & FRANCE ETAT (FR) & 1986-12-12\\\hline
ep0388755\footnote{/patents/txt/ep/0388/755} & Magnetic ink character recognition system. & FUJITSU LTD (JP) & 1989-03-16\\\hline
ep0573665\footnote{/patents/txt/ep/0573/665} & IMAGE DATA CODING METHOD, IMAGE DATA DECODING METHOD, IMAGE DATA CODING DEVICE, IMAGE DATA DECODING DEVICE, AND IMAGE RECORDING MEDIUM. & SONY CORP (JP) & 1991-12-27\\\hline
ep0246055\footnote{/patents/txt/ep/0246/055} & Digital communication system & IKEGAMI FUMIO (JP); MITSUBISHI ELECTRIC CORP (JP) & 1986-05-13\\\hline
ep0450853\footnote{/patents/txt/ep/0450/853} & Color image communication apparatus and method. & CANON KK (JP) & 1990-03-27\\\hline
ep0442581\footnote{/patents/txt/ep/0442/581} & INTERCONNECTION ELEMENT FOR AN ASYNCHRONOUS TIME-DIVISION MULTIPLEX TRANSMISSION SYSTEM & PHILIPS CORP (US) & 1990-02-16\\\hline
ep0530098\footnote{/patents/txt/ep/0530/098} & System and method of communication between circuit mode communication installation core units & ALCATEL NV (NL) & 1991-08-29\\\hline
ep0624297\footnote{/patents/txt/ep/0624/297} & Communication system with monitoring means connected in parallel to signal carrying medium & MADGE NETWORK LIMITED (GB) & 1992-01-20\\\hline
ep0259053\footnote{/patents/txt/ep/0259/053} & Variable data compression announcement circuit & AMERICAN TELEPHONE \& TELEGRAPH (US); AT \& T INFORMATION SYSTEMS INC (US) & 1986-08-27\\\hline
ep0565219\footnote{/patents/txt/ep/0565/219} & Image compression and decompression. & SAMSUNG ELECTRONICS CO LTD (KR) & 1992-04-07\\\hline
ep0350402\footnote{/patents/txt/ep/0350/402} & Microcomputer integrating a digital subscriber terminal for an integrated service digital network & J S TELECOMMUNICATIONS (FR) & 1988-07-08\\\hline
ep0465511\footnote{/patents/txt/ep/0465/511} & Dynamic correction of servo following errors in a computer-numerically controlled system and fixed cycle utilizing same & CINCINNATI MILACRON INC (US) & 1989-03-22\\\hline
ep0879447\footnote{/patents/txt/ep/0879/447} & PROCESS FOR GENERATING CONTROL PARAMETERS FROM A RESPONSE SIGNAL OF A CONTROLLED SYSTEM USING A COMPUTER & SIEMENS AG (DE); WEINZIERL KLAUS (DE) & 1996-02-09\\\hline
ep0328352\footnote{/patents/txt/ep/0328/352} & Ultrasound power generating system with sampled-data frequency control. & METTLER ELECTRONICS CORP (US) & 1988-02-09\\\hline
ep0342756\footnote{/patents/txt/ep/0342/756} & HIGH DEFINITION TELEVISION TRANSMISSION AND RECEPTION SYSTEM WITH REDUCED DATA THROUGHPUT & PHILIPS CORP (US) & 1988-05-20\\\hline
ep0412234\footnote{/patents/txt/ep/0412/234} & Demodulated data recognition and decision device. & MITSUBISHI ELECTRIC CORP (JP) & 1989-08-08\\\hline
ep0425839\footnote{/patents/txt/ep/0425/839} & Data processing system channel. & IBM (US) & 1989-10-30\\\hline
ep0557500\footnote{/patents/txt/ep/0557/500} & DETECTING AND DISREGARDING INVALID TEMPERATURE DATA IN A SYSTEM FOR CONTROLLING THE TEMPERATURE IN AN AUTOMATIC FILM PROCESSOR & EASTMAN KODAK CO (US) & 1991-09-13\\\hline
ep0578203\footnote{/patents/txt/ep/0578/203} & Method and apparatus for forming image data metrics from cascaded photographic imaging systems. & EASTMAN KODAK CO (US) & 1992-07-06\\\hline
ep0882286\footnote{/patents/txt/ep/0882/286} & PC AUDIO SYSTEM WITH FREQUENCY COMPENSATED WAVETABLE DATA & ADVANCED MICRO DEVICES INC (US) & 1996-02-21\\\hline
ep0204248\footnote{/patents/txt/ep/0204/248} & Digital transmission link with series-connected telemetry channels. & SIEMENS AG (DE) & 1985-05-31\\\hline
ep0241569\footnote{/patents/txt/ep/0241/569} & Picture reproduction system for producing a digital picture. & MAURER ELECTRONICS GMBH (DE) & 1986-04-15\\\hline
ep0232181\footnote{/patents/txt/ep/0232/181} & Digital information reproducing apparatus & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1986-11-11\\\hline
ep0342530\footnote{/patents/txt/ep/0342/530} & SWITCHING MATRIX NETWORK FOR DIGITAL AUDIO SIGNALS & SIEMENS AG OESTERREICH (AT) & 1988-05-11\\\hline
ep0374220\footnote{/patents/txt/ep/0374/220} & SWITCHING MATRIX NETWORK FOR DIGITAL AUDIO SIGNALS & SIEMENS AG OESTERREICH (AT) & 1988-05-11\\\hline
ep0368144\footnote{/patents/txt/ep/0368/144} & Digital computing system with low power mode. & MOTOROLA INC (US) & 1988-11-10\\\hline
ep0437515\footnote{/patents/txt/ep/0437/515} & Integrated telecommunication system with improved digital voice response & PRECISION SOFTWARE INC (US) & 1988-10-05\\\hline
ep0504546\footnote{/patents/txt/ep/0504/546} & Synchronization and matching method for a binary baseband transmission system & LITEF GMBH (DE) & 1991-03-18\\\hline
ep0470294\footnote{/patents/txt/ep/0470/294} & Notch filter for digital transmission system & SIEMENS MEDICAL ELECTRONICS (US) & 1990-08-10\\\hline
ep0847195\footnote{/patents/txt/ep/0847/195} & ENCODING METHOD AND DEVICE FOR GIVING SEARCHING/REPRODUCING ROUTE INFORMATION TO A BIT STREAM & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1995-09-29\\\hline
ep0204299\footnote{/patents/txt/ep/0204/299} & Modular unitary disk file subsystem & PLUS DEV CORP (US) & 1985-06-04\\\hline
ep0066048\footnote{/patents/txt/ep/0066/048} & Method of controlling a printer in an interactive text processing system to print records from stored files of spatially related data & IBM (US) & 1981-05-18\\\hline
ep0244141\footnote{/patents/txt/ep/0244/141} & Technique for interpolating a color image for image enlargement or reduction based on look-up tables stored in memory & KONISHIROKU PHOTO IND (JP) & 1986-05-12\\\hline
ep0255828\footnote{/patents/txt/ep/0255/828} & Image recording apparatus in which exposure levels are a function of image contents & EASTMAN KODAK CO (US) & 1986-01-17\\\hline
ep0441305\footnote{/patents/txt/ep/0441/305} & A color image processing apparatus. & KONISHIROKU PHOTO IND (JP) & 1990-02-05\\\hline
ep0476873\footnote{/patents/txt/ep/0476/873} & Method of and apparatus for separating image regions. & CANON KK (JP) & 1990-09-03\\\hline
ep0196094\footnote{/patents/txt/ep/0196/094} & Multi-element information transmission system & HITACHI LTD (JP) & 1985-03-27\\\hline
ep0543118\footnote{/patents/txt/ep/0543/118} & Information display system for electronically reading a book. & SEGA ENTERPRISES KK (JP) & 1991-11-21\\\hline
ep0640972\footnote{/patents/txt/ep/0640/972} & Recording of video information. & SONY CORP (JP) & 1993-08-24\\\hline
ep0322075\footnote{/patents/txt/ep/0322/075} & Switching network and switching network control for a transmission system & PHILIPS CORP (US) & 1987-12-23\\\hline
ep0307931\footnote{/patents/txt/ep/0307/931} & Interface module for connecting collision detection LAN user terminals to a switching network having a different access mode. & NIPPON ELECTRIC CO (JP) & 1987-09-18\\\hline
ep0655126\footnote{/patents/txt/ep/0655/126} & \#f & BIZERBA WERKE KRAUT KG WILH (DE) & 1992-08-12\\\hline
ep0245041\footnote{/patents/txt/ep/0245/041} & System for saving previous format parameters and using the saved format parameters and changed format parameters to format data output & IBM (US) & 1986-05-05\\\hline
ep0383232\footnote{/patents/txt/ep/0383/232} & Computerized tomographic apparatus. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1989-02-13\\\hline
ep0083455\footnote{/patents/txt/ep/0083/455} & View interpolation in a tomography system using pipe line processing & TOKYO SHIBAURA ELECTRIC CO (JP) & 1982-01-06\\\hline
ep0309993\footnote{/patents/txt/ep/0309/993} & Multimedia mail system. & HITACHI LTD (JP) & 1987-09-29\\\hline
ep0430500\footnote{/patents/txt/ep/0430/500} & System and method for atomic access to an input/output device with direct memory access. & DIGITAL EQUIPMENT CORP (US) & 1989-11-17\\\hline
ep0102434\footnote{/patents/txt/ep/0102/434} & Device for reporting error conditions occurring in adapters, to the data processing equipment central control unit & IBM (US) & 1982-08-30\\\hline
ep0598434\footnote{/patents/txt/ep/0598/434} & Method and circuit arrangement for displaying characters with contour & PHILIPS PATENTVERWALTUNG (DE); PHILIPS ELECTRONICS NV (NL) & 1992-11-14\\\hline
ep0440359\footnote{/patents/txt/ep/0440/359} & Output apparatus. & CANON KK (JP) & 1990-01-31\\\hline
ep0298660\footnote{/patents/txt/ep/0298/660} & Font cartridge. & CANON KK (JP) & 1987-07-06\\\hline
ep0591739\footnote{/patents/txt/ep/0591/739} & Working storage management in medical imaging systems. & EASTMAN KODAK CO (US) & 1992-09-21\\\hline
ep0555590\footnote{/patents/txt/ep/0555/590} & Message processing in medical instruments. & PHYSIO CONTROL CORP (US) & 1992-01-10\\\hline
ep0334230\footnote{/patents/txt/ep/0334/230} & Method for detecting prospective contour points of an irradiation field. & FUJI PHOTO FILM CO LTD (JP) & 1988-03-19\\\hline
ep0373854\footnote{/patents/txt/ep/0373/854} & Apparatus and method for detecting internal structures contained within the interior region of a solid object & GEN ELECTRIC (US) & 1988-12-12\\\hline
ep0454129\footnote{/patents/txt/ep/0454/129} & System for generating a texture mapped perspective view. & HONEYWELL INC (US) & 1990-04-26\\\hline
ep0560942\footnote{/patents/txt/ep/0560/942} & METHOD FOR BORDERLESS MAPPING OF TEXTURE IMAGES & PIXAR (US) & 1990-12-04\\\hline
ep0223570\footnote{/patents/txt/ep/0223/570} & Vector access control system & FUJITSU LTD (JP) & 1985-11-15\\\hline
ep0243644\footnote{/patents/txt/ep/0243/644} & Interpolated display characteristic value generator & IBM (US) & 1986-04-23\\\hline
ep0520446\footnote{/patents/txt/ep/0520/446} & Recognizing and judging apparatus. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1992-01-23\\\hline
ep0654755\footnote{/patents/txt/ep/0654/755} & A system and method for automatic handwriting recognition with a writer-independent chirographic label alphabet. & IBM (US) & 1993-11-23\\\hline
ep0728301\footnote{/patents/txt/ep/0728/301} & Cytological screening method & TAFAS TRIANTAFILLOS P (US); TSIPOURAS PETROS (US) & 1993-10-07\\\hline
ep0506330\footnote{/patents/txt/ep/0506/330} & A communications system and a system control method. & HITACHI LTD (JP); HITACHI MICROCOMPUTER SYST (JP) & 1991-03-29\\\hline
ep0632390\footnote{/patents/txt/ep/0632/390} & Processor circuit comprising a first processor, and system comprising the processor circuit and a second processor. & NEDERLAND PTT (NL) & 1993-06-29\\\hline
ep0657804\footnote{/patents/txt/ep/0657/804} & Overflow control for arithmetic operations. & HEWLETT PACKARD CO (US) & 1993-12-08\\\hline
ep0684539\footnote{/patents/txt/ep/0684/539} & Computer system employing an improved real time clock alarm & ADVANCED MICRO DEVICES INC (US) & 1994-05-20\\\hline
ep0100152\footnote{/patents/txt/ep/0100/152} & Method and apparatus for storage and accessing of characters, and electronic printer employing same & DECISION DATA COMPUTER CORP (US) & 1982-07-01\\\hline
ep0542054\footnote{/patents/txt/ep/0542/054} & Reading apparatus for eyesight handicapped person. & CANON KK (JP) & 1991-11-26\\\hline
ep0173647\footnote{/patents/txt/ep/0173/647} & Enciphering/deciphering method. & GRETAG AG (CH) & 1984-08-10\\\hline
ep0314514\footnote{/patents/txt/ep/0314/514} & Transducer. & HEWLETT PACKARD CO (US) & 1987-10-30\\\hline
ep0355850\footnote{/patents/txt/ep/0355/850} & Sequential decoder. & FUJITSU LTD (JP) & 1988-08-25\\\hline
ep0418409\footnote{/patents/txt/ep/0418/409} & Method and device to avoid prevailing weather effects on automatic fire alarms. & SIEMENS AG (DE) & 1989-09-19\\\hline
ep0624031\footnote{/patents/txt/ep/0624/031} & Managing letterbox signals with logos. & THOMSON CONSUMER ELECTRONICS (US) & 1993-05-05\\\hline
ep0559979\footnote{/patents/txt/ep/0559/979} & Subscriber call routing process system. & SEARS TECHNOLOGY SERVICES INC (US) & 1992-03-09\\\hline
ep0241764\footnote{/patents/txt/ep/0241/764} & Process and devices for detecting and localizing damages in electrical installations. & SIEMENS AG (DE) & 1986-04-14\\\hline
ep0323681\footnote{/patents/txt/ep/0323/681} & Telerobotic tracker. & SPAR AEROSPACE LTD (CA) & 1988-01-05\\\hline
ep0401188\footnote{/patents/txt/ep/0401/188} & Control system for a paper or board machine. & VALMET PAPER MACHINERY INC (FI) & 1989-06-01\\\hline
ep0450231\footnote{/patents/txt/ep/0450/231} & Neutron flux mapping system for nuclear reactors. & WESTINGHOUSE ELECTRIC CORP (US) & 1990-03-16\\\hline
ep0629087\footnote{/patents/txt/ep/0629/087} & Recording/reproducing apparatus and reproducing apparatus. & SONY CORP (JP) & 1993-06-08\\\hline
ep0770943\footnote{/patents/txt/ep/0770/943} & Method for automated generation of control circuits & ABB PATENT GMBH (DE) & 1995-10-24\\\hline
ep0210088\footnote{/patents/txt/ep/0210/088} & Wide-field plural-source display apparatus. & SFENA (FR) & 1985-07-12\\\hline
ep0461880\footnote{/patents/txt/ep/0461/880} & Video tape recorder. & SONY CORP (JP) & 1990-06-12\\\hline
ep0531942\footnote{/patents/txt/ep/0531/942} & Vending system with a certain number of cartons. & ACCUMULATA VERWALTUNGS GMBH (DE) & 1991-09-10\\\hline
ep0575773\footnote{/patents/txt/ep/0575/773} & Thermal printer for printing labels & ESSELTE METO INT GMBH (DE) & 1992-06-19\\\hline
ep0575622\footnote{/patents/txt/ep/0575/622} & EQUIPMENT FOR MEASURING QUANTITY OF ELECTRIC LOAD, AND METHOD AND APPARATUS FOR USING THE SAME. & FURUKAWA ELECTRIC CO LTD (JP) & 1991-12-13\\\hline
ep0625425\footnote{/patents/txt/ep/0625/425} & Thermal printer and printing method thereof & SAMSUNG ELECTRONICS CO LTD (KR) & 1993-05-17\\\hline
ep0642015\footnote{/patents/txt/ep/0642/015} & Method and apparatus for measuring and controlling refracted angle of ultrasonic waves. & GEN ELECTRIC (US) & 1993-09-07\\\hline
ep0718611\footnote{/patents/txt/ep/0718/611} & Digitally reconfigurable engine knock detecting system & DELCO ELECTRONICS CORP (US) & 1994-12-19\\\hline
ep0292312\footnote{/patents/txt/ep/0292/312} & Character processing apparatus. & CANON KK (JP) & 1987-05-22\\\hline
ep0621554\footnote{/patents/txt/ep/0621/554} & Method and apparatus for automatic determination of text line, word and character cell spatial features. & XEROX CORP (US) & 1993-04-19\\\hline
ep0523571\footnote{/patents/txt/ep/0523/571} & Advanced code error detection apparatus and system using maximal-length pseudorandom binary sequence. & ANRITSU CORP (JP) & 1991-09-30\\\hline
ep0304081\footnote{/patents/txt/ep/0304/081} & Digital communication system using partial response and bipolar coding techniques. & NIPPON ELECTRIC CO (JP) & 1987-08-21\\\hline
ep0245868\footnote{/patents/txt/ep/0245/868} & Signal communication capable of avoiding an audible reproduction of a sequence of information signals & NIPPON ELECTRIC CO (JP) & 1986-05-16\\\hline
ep0215526\footnote{/patents/txt/ep/0215/526} & Data communication system having a packet switching network & ALCATEL NV (NL) & 1985-09-19\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

