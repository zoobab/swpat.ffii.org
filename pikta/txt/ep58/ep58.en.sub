\begin{subdocument}{swpiktxt58}{Top Software Probability Batch nr 58: 5701-5800}{http://swpat.ffii.org/patents/txt/ep58/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0435700\footnote{/patents/txt/ep/0435/700} & Image processing system. & CANON KK (JP) & 1989-12-29\\\hline
ep0477388\footnote{/patents/txt/ep/0477/388} & DEVICE FOR PRODUCING IMAGE. & SEIKO EPSON CORP (JP) & 1990-04-17\\\hline
ep0508088\footnote{/patents/txt/ep/0508/088} & Virtual image display for indicators, such as an automotive instrument panel. & VEGLIA BORLETTI SRL (IT) & 1991-03-08\\\hline
ep0514301\footnote{/patents/txt/ep/0514/301} & Device for the data processing of linear images of generatrices of an article having an axis of revolution & EST GILLES LEROUZ (FR) & 1992-04-23\\\hline
ep0547245\footnote{/patents/txt/ep/0547/245} & IMAGE PROCESSING METHOD FOR INDUSTRIAL VISUAL SENSOR. & FANUC LTD (JP) & 1991-07-05\\\hline
ep0630481\footnote{/patents/txt/ep/0630/481} & Image neurography and diffusion anisotropy imaging & UNIV WASHINGTON (US) & 1993-01-22\\\hline
ep0643314\footnote{/patents/txt/ep/0643/314} & Image Display Apparatus. & SONY CORP (JP) & 1993-09-14\\\hline
ep0203562\footnote{/patents/txt/ep/0203/562} & Information recording and reproducing apparatus which detects deterioration of a medium in each sector before recording & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1985-05-27\\\hline
ep0172038\footnote{/patents/txt/ep/0172/038} & Information processor capable of data transfer among plural digital data processing units by using an active transmission line having locally controlled storage of data & SHARP KK (JP); MATSUSHITA ELECTRIC IND CO LTD (JP); SANYO ELECTRIC CO (JP); MITSUBISHI ELECTRIC CORP (JP) & 1984-08-16\\\hline
ep0230351\footnote{/patents/txt/ep/0230/351} & Information processing system on-off computer system powering &  & 1986-01-17\\\hline
ep0208536\footnote{/patents/txt/ep/0208/536} & Apparatus for recording and/or reproducing an additional information signal & SONY CORP (JP) & 1985-07-09\\\hline
ep0296862\footnote{/patents/txt/ep/0296/862} & Multiprocessor information exchange. & WESTINGHOUSE ELECTRIC CORP (US) & 1987-06-24\\\hline
ep0422656\footnote{/patents/txt/ep/0422/656} & Information processing system & NIPPON ELECTRIC CO (JP) & 1989-10-12\\\hline
ep0520572\footnote{/patents/txt/ep/0520/572} & Information processing apparatus. & PHILIPS ELECTRONIQUE LAB (FR); KONINKL PHILIPS ELECTRONICS NV (NL) & 1991-06-28\\\hline
ep0510452\footnote{/patents/txt/ep/0510/452} & A knowledge base management system for an information reasoning apparatus. & MITSUI PETROCHEMICAL IND (JP) & 1991-04-26\\\hline
ep0507589\footnote{/patents/txt/ep/0507/589} & System for processing lithographic information. & NIPPON KOGAKU KK (JP) & 1991-04-02\\\hline
ep0498307\footnote{/patents/txt/ep/0498/307} & Information processing apparatus. & SONY CORP (JP) & 1991-02-04\\\hline
ep0492808\footnote{/patents/txt/ep/0492/808} & On-line restoration of redundancy information in a redundant array system. & ARRAY TECHNOLOGY CORP (US) & 1990-12-21\\\hline
ep0569244\footnote{/patents/txt/ep/0569/244} & Method of processing information of recording disc and apparatus for processing the same. & PIONEER ELECTRONIC CORP (JP) & 1992-05-08\\\hline
ep0553830\footnote{/patents/txt/ep/0553/830} & Display driving apparatus and information processing system. & CANON KK (JP) & 1992-01-31\\\hline
ep0584895\footnote{/patents/txt/ep/0584/895} & Fail-safe displaying method of traffic technical information for a traffic route system. & SIEMENS AG (DE) & 1992-08-28\\\hline
ep0786746\footnote{/patents/txt/ep/0786/746} & Method and device for processing coded information with an IC-card & MARKETLINK (FR) & 1996-01-22\\\hline
ep0252176\footnote{/patents/txt/ep/0252/176} & A method and a system for processing logic programs. & EUROP COMPUTER IND RES (DE) & 1986-07-08\\\hline
ep0343968\footnote{/patents/txt/ep/0343/968} & Programmable logic device. & FUJITSU LTD (JP); FUJITSU VLSI LTD (JP) & 1988-05-25\\\hline
ep0331551\footnote{/patents/txt/ep/0331/551} & Programmable logic controller using a structured programming language. & APRIL SA (FR) & 1988-02-26\\\hline
ep0307912\footnote{/patents/txt/ep/0307/912} & Programmable logic device. & FUJITSU LTD (JP); FUJITSU VLSI LTD (JP) & 1987-09-18\\\hline
ep0537885\footnote{/patents/txt/ep/0537/885} & Programmable logic devices. & ADVANCED MICRO DEVICES INC (US) & 1991-10-18\\\hline
ep0583872\footnote{/patents/txt/ep/0583/872} & Flexible synchronous/asynchronous cell structure for a programmable logic device. & ADVANCED MICRO DEVICES INC (US) & 1992-08-03\\\hline
ep0583371\footnote{/patents/txt/ep/0583/371} & OUTPUT LOGIC MACROCELL WITH ENHANCED FUNCTIONAL CAPABILITIES & LATTICE SEMICONDUCTOR CORP (US) & 1991-05-06\\\hline
ep0739136\footnote{/patents/txt/ep/0739/136} & Multi-node media server with efficient work scheduling & IBM (US) & 1995-04-07\\\hline
ep0173508\footnote{/patents/txt/ep/0173/508} & Local area network. & XEROX CORP (US) & 1984-08-31\\\hline
ep0246428\footnote{/patents/txt/ep/0246/428} & Method and system for addressing and controlling a network of modems & IBM (US) & 1986-05-20\\\hline
ep0241958\footnote{/patents/txt/ep/0241/958} & Broadband/narrow-band switching network of the time-space-time type and time and space switching stages for broadband/narrow-band channels & AT \& T \& PHILIPS TELECOMM (NL) & 1986-03-10\\\hline
ep0215463\footnote{/patents/txt/ep/0215/463} & Routing control method in a packet switching network & HITACHI LTD (JP); HITACHI SEIBU SOFTWARE KK (JP) & 1985-09-20\\\hline
ep0383660\footnote{/patents/txt/ep/0383/660} & BIT RATE RESERVATION IN AN ASYNCHRONOUS PACKET NETWORK & FRENCH STATE REPRESENTED BY MI (FR) & 1989-02-17\\\hline
ep0571424\footnote{/patents/txt/ep/0571/424} & Method for receiving and delivering section overheads of and for STM-1 signals in a section-overhead server of a network node & SIEMENS AG (DE) & 1992-02-05\\\hline
ep0550191\footnote{/patents/txt/ep/0550/191} & Copier/duplicator network. & XEROX CORP (US) & 1991-12-31\\\hline
ep0580652\footnote{/patents/txt/ep/0580/652} & Wireless coupling of devices to wired network & SLW TELESYSTEMS INC (CA) & 1991-04-11\\\hline
ep0569913\footnote{/patents/txt/ep/0569/913} & Connection path selection method for cross-connect communications networks. & ALCATEL NV (NL) & 1992-05-14\\\hline
ep0315172\footnote{/patents/txt/ep/0315/172} & Automatic program generation method with a visual date structure display. & HITACHI LTD (JP) & 1987-11-06\\\hline
ep0354793\footnote{/patents/txt/ep/0354/793} & IC card and method for rewriting its program. & HITACHI MAXELL (JP) & 1988-08-12\\\hline
ep0515699\footnote{/patents/txt/ep/0515/699} & INSTRUCTION METHOD FOR ROBOT OPERATION PROGRAM. & FANUC LTD (JP) & 1991-12-16\\\hline
ep0509623\footnote{/patents/txt/ep/0509/623} & Program processing system and method. & MITSUBISHI ELECTRIC CORP (JP) & 1991-04-19\\\hline
ep0825501\footnote{/patents/txt/ep/0825/501} & Program controlled switching device with menu command & LEGRAND GMBH (DE) & 1996-08-16\\\hline
ep0393664\footnote{/patents/txt/ep/0393/664} & User programmable switching arrangement & THOMSON CONSUMER ELECTRONICS (US) & 1989-04-20\\\hline
ep0548286\footnote{/patents/txt/ep/0548/286} & USER INTERFACE FOR TELEVISION SCHEDULE SYSTEM & INSIGHT TELECAST INC (US) & 1990-09-10\\\hline
ep0683563\footnote{/patents/txt/ep/0683/563} & Control for a television set, programmable by the user. & KONINKL PHILIPS ELECTRONICS NV (NL); PHILIPS ELECTRONICS NV (NL) & 1994-05-18\\\hline
ep0568438\footnote{/patents/txt/ep/0568/438} & Method for securing of executable programs against utilisation by an unauthorized person and security system for its application. & GEMPLUS CARD INT (FR) & 1992-04-27\\\hline
ep0423294\footnote{/patents/txt/ep/0423/294} & COMPUTERIZED UNIT FOR THE STUDY AND EVALUATION OF FCC CATALYSTS & CONSEJO SUPERIOR INVESTIGACION (ES) & 1989-04-13\\\hline
ep0309069\footnote{/patents/txt/ep/0309/069} & Computerized sewing apparatus. & SADEH YAACOV (IL); MAKOVER YAACOV (IL); MARDIX BAR COCHVA (IL) & 1987-09-25\\\hline
ep0187713\footnote{/patents/txt/ep/0187/713} & System memory for a reduction processor evaluating programs stored as binary directed graphs employing variable-free applicative language codes & BURROUGHS CORP (US) & 1985-01-11\\\hline
ep0237654\footnote{/patents/txt/ep/0237/654} & X-and-OR memory array & NCR CO (US) & 1983-01-10\\\hline
ep0225642\footnote{/patents/txt/ep/0225/642} & Memory test pattern generator & ADVANTEST CORP (JP) & 1985-12-13\\\hline
ep0585479\footnote{/patents/txt/ep/0585/479} & Method and apparatus for monitoring a distributed system & HEWLETT PACKARD CO (US) & 1992-08-31\\\hline
ep0646876\footnote{/patents/txt/ep/0646/876} & Message passing system for distributed shared memory multiprocessor system and message passing method using the same. & NIPPON TELEGRAPH \& TELEPHONE (JP) & 1994-08-26\\\hline
ep0781479\footnote{/patents/txt/ep/0781/479} & Distributed processing ethernet switch with adaptive cut-through switching & AMBER WAVE SYSTEMS INC (US) & 1994-09-12\\\hline
ep0223781\footnote{/patents/txt/ep/0223/781} & METHOD AND APPARATUS FOR CREATING ENCRYPTED AND DECRYPTED TELEVISION SIGNALS & SCIENTIFIC ATLANTA (US) & 1985-05-21\\\hline
ep0402912\footnote{/patents/txt/ep/0402/912} & Disk memory system. & SONY CORP (JP) & 1989-09-30\\\hline
ep0400475\footnote{/patents/txt/ep/0400/475} & Storage management system for memory card using memory allocation table. & FUJI PHOTO FILM CO LTD (JP) & 1989-05-29\\\hline
ep0458516\footnote{/patents/txt/ep/0458/516} & Memory access bus arrangement & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1990-05-25\\\hline
ep0446081\footnote{/patents/txt/ep/0446/081} & METHOD OF LOADING APPLICATIONS PROGRAMS INTO A MEMORY CARD READER HAVING A MICROPROCESSOR, AND A SYSTEM FOR IMPLEMENTING THE METHOD & GEMPLUS CARD INT (FR) & 1990-01-25\\\hline
ep0384155\footnote{/patents/txt/ep/0384/155} & Combination of a medical device with a redundant controller for infusion therapy or blood treatmentand a programming device. & BRAUN MELSUNGEN AG (DE) & 1989-02-22\\\hline
ep0503756\footnote{/patents/txt/ep/0503/756} & Method of programming flash EEPROM cell arrays. & ADVANCED MICRO DEVICES INC (US) & 1991-03-13\\\hline
ep0553433\footnote{/patents/txt/ep/0553/433} & Tool specifying method and apparatus for an NC automatic programming system. & MITSUBISHI ELECTRIC CORP (JP) & 1992-01-28\\\hline
ep0601554\footnote{/patents/txt/ep/0601/554} & Television receiver with a device for automatic channel programming. & GRUNDIG EMV (DE) & 1992-12-11\\\hline
ep0530682\footnote{/patents/txt/ep/0530/682} & Method and means for addressing a very large memory. & IBM (US) & 1991-09-04\\\hline
ep0749126\footnote{/patents/txt/ep/0749/126} & Output interfacing device programmable among three states for a memory in CMOS technology & MATRA MHS (FR) & 1995-06-12\\\hline
ep0756746\footnote{/patents/txt/ep/0756/746} & \#f & GEMPLUS CARD INT (FR) & 1995-02-16\\\hline
ep0221509\footnote{/patents/txt/ep/0221/509} & Apparatus providing improved diagnosability & HONEYWELL INF SYSTEMS (US) & 1985-11-04\\\hline
ep0633520\footnote{/patents/txt/ep/0633/520} & Remote control having voice input. & PHILIPS ELECTRONICS NV (NL) & 1993-07-01\\\hline
ep0107050\footnote{/patents/txt/ep/0107/050} & Signal generator for digital spectrum analyzer & TAKEDA RIKEN IND CO LTD (JP) & 1982-09-24\\\hline
ep0612846\footnote{/patents/txt/ep/0612/846} & G-CSF analog compositions and methods. & AMGEN INC (US) & 1993-01-28\\\hline
ep0204449\footnote{/patents/txt/ep/0204/449} & Method for multiprocessor communications & TANDEM COMPUTERS INC (US) & 1985-06-05\\\hline
ep0171141\footnote{/patents/txt/ep/0171/141} & Method of modifying programs stored in cash register & SHARP KK (JP) & 1984-05-17\\\hline
ep0182126\footnote{/patents/txt/ep/0182/126} & Directing storage requests prior to address comparator initialization with a reference address range & IBM (US) & 1984-11-13\\\hline
ep0402182\footnote{/patents/txt/ep/0402/182} & Payment system using bi-modular IC-card. & PARIENTI RAOUL (FR) & 1988-06-08\\\hline
ep0236744\footnote{/patents/txt/ep/0236/744} & Reconfigurable automatic tasking system. & IBM (US) & 1986-03-10\\\hline
ep0257655\footnote{/patents/txt/ep/0257/655} & MULTITASK PROCESSING APPARATUS UTILIZING A CENTRAL PROCESSING UNIT EQUIPPED WITH A MICRO-PROGRAM MEMORY WHICH CONTAINS NO SOFTWARE INSTRUCTIONS & NIPPON ELECTRIC CO (JP) & 1986-08-28\\\hline
ep0278526\footnote{/patents/txt/ep/0278/526} & Graphics diplay controller having clipping function. & NIPPON ELECTRIC CO (JP) & 1987-02-13\\\hline
ep0288043\footnote{/patents/txt/ep/0288/043} & Simulation method for programs. & HITACHI LTD (JP) & 1987-04-24\\\hline
ep0285634\footnote{/patents/txt/ep/0285/634} & Method and device to execute two instruction sequences in an order determined in advance & ERICSSON TELEFON AB L M (SE) & 1986-10-03\\\hline
ep0295646\footnote{/patents/txt/ep/0295/646} & Arithmetic operation processing apparatus of the parallel processing type and compiler which is used in this apparatus. & HITACHI LTD (JP) & 1987-07-03\\\hline
ep0309196\footnote{/patents/txt/ep/0309/196} & Page planning system. & CROSFIELD ELECTRONICS LTD (GB) & 1987-09-21\\\hline
ep0335424\footnote{/patents/txt/ep/0335/424} & Improved parity checking apparatus. & WANG LABORATORIES (US) & 1988-03-31\\\hline
ep0379768\footnote{/patents/txt/ep/0379/768} & Read-modify-write operation. & DIGITAL EQUIPMENT CORP (US) & 1989-01-27\\\hline
ep0475729\footnote{/patents/txt/ep/0475/729} & Improved buffering for read-modify-write operation. & DIGITAL EQUIPMENT CORP (US) & 1990-09-14\\\hline
ep0572564\footnote{/patents/txt/ep/0572/564} & MASS STORAGE ARRAY WITH EFFICIENT PARITY CALCULATION & MICROPOLIS CORP (US) & 1991-02-20\\\hline
ep0347929\footnote{/patents/txt/ep/0347/929} & Parallel processor. & HITACHI LTD (JP) & 1988-06-23\\\hline
ep0355287\footnote{/patents/txt/ep/0355/287} & Method for producing a knowledge based system. & IBM (US) & 1988-08-19\\\hline
ep0432076\footnote{/patents/txt/ep/0432/076} & High performance shared main storage interface. & IBM (US) & 1989-12-04\\\hline
ep0447685\footnote{/patents/txt/ep/0447/685} & Programmable controller. & HITACHI LTD (JP) & 1990-03-19\\\hline
ep0446117\footnote{/patents/txt/ep/0446/117} & Fast determination of subtype relationship in a single inheritance type hierarchy & DIGITAL EQUIPMENT CORP (US) & 1990-03-06\\\hline
ep0472433\footnote{/patents/txt/ep/0472/433} & Firmware modification system wherein older version can be retrieved. & FUJITSU LTD (JP) & 1990-08-23\\\hline
ep0469756\footnote{/patents/txt/ep/0469/756} & Method for economically building complex, large-scale, rule-based systems and sub-systems. & DIGITAL EQUIPMENT CORP (US) & 1990-07-30\\\hline
ep0485090\footnote{/patents/txt/ep/0485/090} & Transaction approval system. & VISA INT SERVICE ASS (US) & 1990-11-09\\\hline
ep0497443\footnote{/patents/txt/ep/0497/443} & Static ram based microcontroller. & ADVANCED MICRO DEVICES INC (US) & 1991-02-01\\\hline
ep0528139\footnote{/patents/txt/ep/0528/139} & Programmable interrupt priority encoder method and apparatus & STRATUS COMPUTER INC (US) & 1991-08-12\\\hline
ep0540383\footnote{/patents/txt/ep/0540/383} & Multiprocessor system with microprogrammed means for partitioning the processes between the processors. & BULL SA (FR) & 1991-10-30\\\hline
ep0578013\footnote{/patents/txt/ep/0578/013} & Improved input/output control system and method. & STRATUS COMPUTER INC (US) & 1992-06-15\\\hline
ep0577410\footnote{/patents/txt/ep/0577/410} & Output apparatus and method with adjustable connection feature. & CANON KK (JP) & 1992-07-01\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

