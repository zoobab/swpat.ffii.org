\begin{subdocument}{swpiktxt56}{Top Software Probability Batch nr 56: 5501-5600}{http://swpat.ffii.org/patents/txt/ep56/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0331118\footnote{/patents/txt/ep/0331/118} & Switching arrangement capable of switching burst time plan tables/channel assignment pattern tables of a specific terrestrial station. & NIPPON ELECTRIC CO (JP) & 1988-03-01\\\hline
ep0489214\footnote{/patents/txt/ep/0489/214} & Coupling device to be connected to a dce for allowing the connection to different public switched telephone networks, dce and workstation including the same. & IBM (US) & 1990-12-05\\\hline
ep0598597\footnote{/patents/txt/ep/0598/597} & Method and apparatus for scripting a text-to-speech-based multimedia presentation. & CANON INFORMATION SYST INC (US) & 1992-11-18\\\hline
ep0310782\footnote{/patents/txt/ep/0310/782} & Failing resource manager in a multiplex communication system. & IBM (US) & 1987-10-05\\\hline
ep0313576\footnote{/patents/txt/ep/0313/576} & Gateway system and method for interconnecting telephone calls with a digital voice protected radio network & MOTOROLA INC (US) & 1986-07-03\\\hline
ep0439801\footnote{/patents/txt/ep/0439/801} & Digital power controller. & ROCKWELL INTERNATIONAL CORP (US) & 1989-12-27\\\hline
ep0302444\footnote{/patents/txt/ep/0302/444} & Laser Doppler anemometer. & BUNDESREP DEUTSCHLAND (DE) & 1987-10-30\\\hline
ep0428663\footnote{/patents/txt/ep/0428/663} & Process and system for rapid analysis of the spectrum of a signal at one or several points of measuring & BRUST HANS DETLEF (DE) & 1989-05-29\\\hline
ep0472961\footnote{/patents/txt/ep/0472/961} & Coding method for increasing compressing efficiency of data in transmitting or storing picture signals & SAMSUNG ELECTRONICS CO LTD (KR) & 1990-08-31\\\hline
ep0471130\footnote{/patents/txt/ep/0471/130} & Coding method and apparatus for pipelined and parallel processing. & IBM (US) & 1990-08-16\\\hline
ep0444918\footnote{/patents/txt/ep/0444/918} & Data compression apparatus. & VICTOR COMPANY OF JAPAN (JP) & 1990-03-19\\\hline
ep0394309\footnote{/patents/txt/ep/0394/309} & \#f &  & 1987-12-24\\\hline
ep0570139\footnote{/patents/txt/ep/0570/139} & Method for forming colour images using hue-plus-gray colour model and error diffusion. & HEWLETT PACKARD CO (US) & 1992-05-04\\\hline
ep0823271\footnote{/patents/txt/ep/0823/271} & Apparatus for and method of designating a point on displayed image, and readable recording medium storing program for designating a point on displayed image & KONAMI CO LTD (JP) & 1996-08-05\\\hline
ep0580976\footnote{/patents/txt/ep/0580/976} & Method for transmitting regionally different information in a common-wave network. & GRUNDIG EMV (DE) & 1992-07-11\\\hline
ep0189105\footnote{/patents/txt/ep/0189/105} & Output stacker for a document processing facility & SIEMENS AG (DE) & 1985-01-25\\\hline
ep0201833\footnote{/patents/txt/ep/0201/833} & Instruction processor for processing branch instruction at high speed & HITACHI LTD (JP) & 1985-05-07\\\hline
ep0292284\footnote{/patents/txt/ep/0292/284} & Method and apparatus for processing display colour signal. & SONY CORP (JP) & 1987-09-24\\\hline
ep0333496\footnote{/patents/txt/ep/0333/496} & Avoiding fine line disappearance in picture reduction processes. & FUJITSU LTD (JP) & 1988-08-30\\\hline
ep0310057\footnote{/patents/txt/ep/0310/057} & Decoder. & NIPPON ELECTRIC CO (JP) & 1987-09-30\\\hline
ep0561454\footnote{/patents/txt/ep/0561/454} & Method and apparatus for editing an audio signal. & PHILIPS ELECTRONICS UK LTD (GB); KONINKL PHILIPS ELECTRONICS NV (NL) & 1992-03-18\\\hline
ep0414218\footnote{/patents/txt/ep/0414/218} & Method of making a porous roll assembly. & MASUDA SEISAKUSHO CO LTD (JP) & 1989-11-24\\\hline
ep0697672\footnote{/patents/txt/ep/0697/672} & Procedure to correct a measurement curve or a signal variation, system for carrying out the method, and use for reconstruction of the error position in railway tracks by means of geometrical relative measurements & MAN TECHNOLOGIE GMBH (DE) & 1994-08-19\\\hline
ep0395495\footnote{/patents/txt/ep/0395/495} & Adaptive network routing for power line communications. & SCHLUMBERGER IND INC (US) & 1989-04-27\\\hline
ep0624859\footnote{/patents/txt/ep/0624/859} & Method of, and system for, describing a geographical area to a communications network. & PHILIPS ELECTRONICS UK LTD (GB); PHILIPS ELECTRONICS NV (NL) & 1993-05-14\\\hline
ep0718769\footnote{/patents/txt/ep/0718/769} & Device for the protection of the access to memory words & SGS THOMSON MICROELECTRONICS (FR) & 1994-12-20\\\hline
ep0266065\footnote{/patents/txt/ep/0266/065} & Programmable sequence generator & TEXAS INSTRUMENTS INC (US) & 1986-09-30\\\hline
ep0574282\footnote{/patents/txt/ep/0574/282} & Coding and decoding device for the frequency subband transmission. & SOC ET DE TELE INFORMATIQUES E (FR) & 1992-06-10\\\hline
ep0204226\footnote{/patents/txt/ep/0204/226} & Method for realising an encrypted radio communication. & SIEMENS AG (DE) & 1985-06-04\\\hline
ep0426739\footnote{/patents/txt/ep/0426/739} & Method for acquiring network knowledge about a digital transmission network & ANT NACHRICHTENTECH; KOLBE \& CO HANS; KRONE AG; QUANTE AG; SIEMENS AG; STANDARD ELEKTRIK LORENZ AG; PHILIPS CORP (US) & 1988-07-26\\\hline
ep0581033\footnote{/patents/txt/ep/0581/033} & Data transfer system using memory with fifo structure. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1992-06-30\\\hline
ep0551973\footnote{/patents/txt/ep/0551/973} & Triple orthogonally interleaved error correction system. & AMPEX (US); E SYSTEMS INC (US) & 1992-01-15\\\hline
ep0601704\footnote{/patents/txt/ep/0601/704} & Method and apparatus for remotely altering programmable firmware stored in an interface board coupled to a network peripheral. & CANON INFORMATION SYST INC (US) & 1992-11-18\\\hline
ep0403294\footnote{/patents/txt/ep/0403/294} & Medicine compliance and patient status monitoring system and method. & DESSERTINE ALBERT L (US); HUDSON THOMAS P (US) & 1989-06-14\\\hline
ep0731942\footnote{/patents/txt/ep/0731/942} & METHODS AND APPARATUS FOR CREATING AND MANAGING DYNAMIC PROPERTY SHEETS & WANG LABORATORIES (US) & 1993-11-29\\\hline
ep0251240\footnote{/patents/txt/ep/0251/240} & Line illumination system & IBM (US) & 1986-06-27\\\hline
ep0569716\footnote{/patents/txt/ep/0569/716} & De-interleave circuit for regenerating digital data. & SONY CORP (JP) & 1992-04-13\\\hline
ep0482154\footnote{/patents/txt/ep/0482/154} & DEVICE FOR THE CONVERSION OF A DIGITAL BLOCK AND USE OF SAME & ASCOM TECH AG (CH) & 1990-05-18\\\hline
ep0297301\footnote{/patents/txt/ep/0297/301} & Programmable controller vision system. & ALLEN BRADLEY CO (US) & 1987-06-03\\\hline
ep0389683\footnote{/patents/txt/ep/0389/683} & Apparatus and method for regulating a measurement system. & HEWLETT PACKARD CO (US) & 1989-03-29\\\hline
ep0250951\footnote{/patents/txt/ep/0250/951} & Method and system for data transmission & HITACHI LTD (JP) & 1981-07-31\\\hline
ep0351437\footnote{/patents/txt/ep/0351/437} & CNC CONTROL SYSTEM. & FANUC LTD (JP) & 1987-12-10\\\hline
ep0856181\footnote{/patents/txt/ep/0856/181} & Method and device for evaluating an EEG carried out in the context of anaesthesia or intensive care & ARTHUR SCHULTZ (DE) & 1996-10-17\\\hline
ep0675458\footnote{/patents/txt/ep/0675/458} & Automatic process for labelling and checking blood bags returning from analysis and machine for implementing same & AETSRN (FR) & 1994-03-31\\\hline
ep0409718\footnote{/patents/txt/ep/0409/718} & Videotex and telescopy terminal with automatic reception. & SAGEM (FR) & 1989-07-18\\\hline
ep0386296\footnote{/patents/txt/ep/0386/296} & Assembly of a microcontroller. & SIEMENS AG (DE) & 1989-03-09\\\hline
ep0650624\footnote{/patents/txt/ep/0650/624} & Process and system for automated running of sports contests &  & 1993-07-09\\\hline
ep0897579\footnote{/patents/txt/ep/0897/579} & MEMORY DEVICE & SINCLAIR ALAN WELSH (GB); MEMORY CORP PLC (GB) & 1996-05-10\\\hline
ep0411692\footnote{/patents/txt/ep/0411/692} & Last-in first-out memory and data repacker for use therein. & KONINKL PHILIPS ELECTRONICS NV (NL) & 1989-07-31\\\hline
ep0465066\footnote{/patents/txt/ep/0465/066} & Method and apparatus for transferring data between a data bus and a data storage device. & DIGITAL EQUIPMENT CORP (US) & 1990-06-29\\\hline
ep0542658\footnote{/patents/txt/ep/0542/658} & Method and apparatus utilizing data icons. & IBM (US) & 1991-11-15\\\hline
ep0535560\footnote{/patents/txt/ep/0535/560} & Modulator circuit for regeneration of a digital recording medium. & SONY CORP (JP) & 1991-09-30\\\hline
ep0483984\footnote{/patents/txt/ep/0483/984} & ASCII to ASCII transfer using fax protocol. & HEWLETT PACKARD CO (US) & 1990-10-31\\\hline
ep0652506\footnote{/patents/txt/ep/0652/506} & Interrupt generator and speech reproduction device incorporating same. & SONY CORP (JP) & 1993-10-27\\\hline
ep0598402\footnote{/patents/txt/ep/0598/402} & Word processing apparatus with handwriting input function. & SHARP KK (JP) & 1992-11-18\\\hline
ep0425990\footnote{/patents/txt/ep/0425/990} & Cell exchanging apparatus. & MITSUBISHI ELECTRIC CORP (JP) & 1989-10-23\\\hline
ep0085672\footnote{/patents/txt/ep/0085/672} & THRASHING REDUCTION IN DEMAND ACCESSING OF A DATA BASE THROUGH AN LRU PAGING BUFFER POOL & IBM (US) & 1981-08-18\\\hline
ep0697656\footnote{/patents/txt/ep/0697/656} & Method of scheduling successive tasks & CEGELEC (FR) & 1994-08-11\\\hline
ep0198178\footnote{/patents/txt/ep/0198/178} & Method for testing telephone sets, line paths and parts of the control of telephone exchanges, especially private telephone exchanges. & TELEFONBAU \& NORMALZEIT GMBH (DE) & 1985-04-10\\\hline
ep0554826\footnote{/patents/txt/ep/0554/826} & Facsimile apparatus and its maintenance charge control apparatus. & SHARP KK (JP) & 1992-02-28\\\hline
ep0885416\footnote{/patents/txt/ep/0885/416} & Encoding technique for software and hardware & NORTHERN TELECOM LTD (CA) & 1996-03-07\\\hline
ep0651588\footnote{/patents/txt/ep/0651/588} & Method for communication in a bidirectional bus system. & SONY CORP (JP); MATSUSHITA ELECTRIC IND CO LTD (JP) & 1993-10-30\\\hline
ep0548645\footnote{/patents/txt/ep/0548/645} & Method and apparatus for efficiently displaying windows on a computer display screen. & IBM (US) & 1991-12-20\\\hline
ep0237805\footnote{/patents/txt/ep/0237/805} & Method and arrangement for preventing the transmission of unciphered data. & SIEMENS AG (DE) & 1986-02-19\\\hline
ep0274703\footnote{/patents/txt/ep/0274/703} & Method and arrangement in which capacity of a related storage medium is checked when buffering data & TANDBERG DATA (NO) & 1986-12-22\\\hline
ep0344082\footnote{/patents/txt/ep/0344/082} & Method for accessing visually obscured data in a multi-tasking system. & IBM (US) & 1988-05-23\\\hline
ep0349967\footnote{/patents/txt/ep/0349/967} & Method for data transmission between different stations. & SIEMENS AG (DE) & 1988-07-06\\\hline
ep0368779\footnote{/patents/txt/ep/0368/779} & Method for concurrent data entry and manipulation in multiple apllications. & IBM (US) & 1988-11-07\\\hline
ep0430291\footnote{/patents/txt/ep/0430/291} & A data transfer system. & SONY CORP (JP) & 1989-11-30\\\hline
ep0496429\footnote{/patents/txt/ep/0496/429} & Error correction coded digital data reproducing apparatus. & SONY CORP (JP) & 1991-01-25\\\hline
ep0613077\footnote{/patents/txt/ep/0613/077} & Method for generating a reset signal in a data processing system. & SIEMENS AG (DE) & 1993-01-25\\\hline
ep0643892\footnote{/patents/txt/ep/0643/892} & A DATA TRANSFER METHOD FOR AN ACTUATOR SYSTEM OPERATING IN AN INTERFERING ENVIRONMENT & ABB STROEMBERG KOJEET OY (FI); SUUTARI JUKKA (FI); SOLANTI PETRI (FI); RITONIEMI ERKKI (FI); PAULASAARI VIRPI (FI) & 1992-05-27\\\hline
ep0661621\footnote{/patents/txt/ep/0661/621} & Graphical user interface which supports data transfer between one source and a plurality of target objects. & IBM (US) & 1993-12-30\\\hline
ep0580474\footnote{/patents/txt/ep/0580/474} & Method and device for assisting the piloting of an aircraft from a voluminous set of memory-stored documents & SEXTANT AVIONIQUE (FR) & 1992-07-24\\\hline
ep0458468\footnote{/patents/txt/ep/0458/468} & A multi-level error correction system. & DIGITAL EQUIPMENT CORP (US) & 1990-05-09\\\hline
ep0564871\footnote{/patents/txt/ep/0564/871} & Image forming apparatus. & RICOH KK (JP) & 1992-03-19\\\hline
ep0320876\footnote{/patents/txt/ep/0320/876} & Fault information collection processing system. & MITSUBISHI ELECTRIC CORP (JP) & 1987-12-14\\\hline
ep0453081\footnote{/patents/txt/ep/0453/081} & Information signal processing method and apparatus. & CANON KK (JP) & 1990-03-20\\\hline
ep0590967\footnote{/patents/txt/ep/0590/967} & Wait-state control in an information processing system bus. & HUDSON SOFT CO LTD (JP) & 1992-10-07\\\hline
ep0194091\footnote{/patents/txt/ep/0194/091} & Programmable logic device with limited sense currents and noise reduction & LATTICE SEMICONDUCTOR CORP (US) & 1985-03-04\\\hline
ep0295311\footnote{/patents/txt/ep/0295/311} & Text processing system having search-and-replace function for both character and attribute data & BROTHER IND LTD (JP) & 1986-12-29\\\hline
ep0537097\footnote{/patents/txt/ep/0537/097} & Method and system for incrementally changing window size on a display. & IBM (US) & 1991-07-10\\\hline
ep0522224\footnote{/patents/txt/ep/0522/224} & High speed buffer management. & IBM (US) & 1991-07-10\\\hline
ep0642934\footnote{/patents/txt/ep/0642/934} & A method and system for preparing items to be mailed & HADEWE BV (NL) & 1993-09-15\\\hline
ep0442531\footnote{/patents/txt/ep/0442/531} & Array disk apparatus. & FUJITSU LTD (JP) & 1990-02-16\\\hline
ep0423735\footnote{/patents/txt/ep/0423/735} & Microprocessor having parity check function. & NIPPON ELECTRIC CO (JP) & 1989-10-17\\\hline
ep0672283\footnote{/patents/txt/ep/0672/283} & Forward and reverse Boyer-Moore string searching of multilingual text having a defined collation order & TALIGENT INC (US) & 1993-03-25\\\hline
ep0294533\footnote{/patents/txt/ep/0294/533} & Method for protecting coded signal integrity. & IBM (US) & 1987-06-12\\\hline
ep0497116\footnote{/patents/txt/ep/0497/116} & RDS broadcast receiver. & BLAUPUNKT WERKE GMBH (DE) & 1991-02-01\\\hline
ep0862753\footnote{/patents/txt/ep/0862/753} & \#f & BASF AG (DE) & 1995-11-25\\\hline
ep0894399\footnote{/patents/txt/ep/0894/399} & Device and method for forwarding electronic messages & KININKLIJKE KPM N V (NL) & 1996-04-15\\\hline
ep0039145\footnote{/patents/txt/ep/0039/145} & Code system in a multi-channel analysis equipment and a device related to the system & LABSYSTEMS OY (FI) & 1980-04-28\\\hline
ep0843844\footnote{/patents/txt/ep/0843/844} & METHOD OF ANALYSING PROCESS SIGNALS IN A TECHNICAL INSTALLATION, ESPECIALLY A POWER STATION & SIEMENS AG (DE); FUEHRING THORSTEN (DE); MEDERER HANS GERD (DE); PANYR JIRI (DE); POLITIADIS BEHRENS ALEXANDER (DE); PREISER ULRICH (DE) & 1995-08-09\\\hline
ep0308304\footnote{/patents/txt/ep/0308/304} & Method for sample analysis by sputtering with a particle beam, and device to implement said method & CAMECA (FR); IMEC INTER UNI MICRO ELECT (BE) & 1987-09-11\\\hline
ep0050018\footnote{/patents/txt/ep/0050/018} & Particle agglutination analyzing plate & OLYMPUS OPTICAL CO (JP) & 1980-10-09\\\hline
ep0244274\footnote{/patents/txt/ep/0244/274} & 3-Dimensional digitizer for skeletal analysis. & FARO MEDICAL TECHNOLOGIES INC (CA) & 1986-03-31\\\hline
ep0230001\footnote{/patents/txt/ep/0230/001} & Method of and device for speech signal coding and decoding by subband analysis and vector quantization with dynamic bit allocation & CSELT CENTRO STUDI LAB TELECOM (IT) & 1985-12-17\\\hline
ep0220289\footnote{/patents/txt/ep/0220/289} & Non-invasive blood flow measurements utilizing autoregressive analysis with averaged reflection coefficients & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1985-05-06\\\hline
ep0262217\footnote{/patents/txt/ep/0262/217} & SOLID PHASE ANALYSIS METHOD & BIOPOOL INT INC (US) & 1986-04-15\\\hline
ep0452825\footnote{/patents/txt/ep/0452/825} & Method and apparatus for background correction in analysis of a specimen surface. & SHIMADZU CORP (JP) & 1990-04-19\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

