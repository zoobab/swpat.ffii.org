\begin{subdocument}{swpiktxt14}{Top Software Probability Batch nr 14: 1301-1400}{http://swpat.ffii.org/patents/txt/ep14/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{longtable}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
\endhead
ep0427066\footnote{/patents/txt/ep/0427/066} & Pattern injector & NAT SEMICONDUCTOR CORP (US) & 1989-11-08\\\hline
ep0667538\footnote{/patents/txt/ep/0667/538} & Non-contact type wave signal observation apparatus. & ADVANTEST CORP (JP) & 1994-01-21\\\hline
ep0361497\footnote{/patents/txt/ep/0361/497} & Program/data memory employed in microcomputer system. & NIPPON ELECTRIC CO (JP) & 1988-09-29\\\hline
ep0563190\footnote{/patents/txt/ep/0563/190} & Interactive data visualization with smart object & SARNOFF DAVID RES CENTER (US) & 1990-12-18\\\hline
ep0487855\footnote{/patents/txt/ep/0487/855} & Analysis method of eyeball control system. & ATR AUDITORY VISUAL PERCEPTION (JP) & 1990-11-27\\\hline
ep0228794\footnote{/patents/txt/ep/0228/794} & Intelligent synchronous modem and communication system incorporating the same & GEN DATACOMM IND INC (US) & 1985-11-18\\\hline
ep0599261\footnote{/patents/txt/ep/0599/261} & Multi-user digital laser imaging system. & MINNESOTA MINING \& MFG (US) & 1992-11-25\\\hline
ep0261789\footnote{/patents/txt/ep/0261/789} & Feedback-controlled method and apparatus for processing signals. & PHYSIO CONTROL CORP (US) & 1986-08-18\\\hline
ep0403117\footnote{/patents/txt/ep/0403/117} & Feature board with automatic adjustment to slot position. & IBM (US) & 1989-06-12\\\hline
ep0540329\footnote{/patents/txt/ep/0540/329} & Method for storing a multichannel audio signal on a compact disc. & SALON TELEVISIOTEHDAS OY (FI) & 1991-10-30\\\hline
ep0504945\footnote{/patents/txt/ep/0504/945} & Method and apparatus for diagnosis of sleep disorders & MADAUS SCHWARZER MEDTECH (DE) & 1991-11-26\\\hline
ep0471246\footnote{/patents/txt/ep/0471/246} & \#f & MOTOROLA INC (US) & 1990-08-16\\\hline
ep0883847\footnote{/patents/txt/ep/0883/847} & DATABASES & UNIV STRATHCLYDE (GB); MCGREGOR DOUGLAS ROBERT (GB); COCKSHOTT WILLIAM PAUL (GB); WILSON JOHN NUGENT (GB) & 1996-03-02\\\hline
ep0577773\footnote{/patents/txt/ep/0577/773} & VOCABULARY MEMORY ALLOCATION FOR ADAPTIVE DATA COMPRESSION OF FRAME-MULTIPLEXED TRAFFIC & CODEX CORP (US) & 1991-04-29\\\hline
ep0522750\footnote{/patents/txt/ep/0522/750} & A multiple track data storage disk, a method of formatting and a data recording disk file. & IBM (US) & 1991-07-10\\\hline
ep0535820\footnote{/patents/txt/ep/0535/820} & Method and apparatus for a register providing atomic access to set and clear individual bits of shared registers without software interlock. & SUN MICROSYSTEMS INC (US) & 1991-09-27\\\hline
ep0375330\footnote{/patents/txt/ep/0375/330} & Centralized mail use database. & PITNEY BOWES (US) & 1988-12-16\\\hline
ep0517486\footnote{/patents/txt/ep/0517/486} & Global user interface. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1991-06-07\\\hline
ep0480637\footnote{/patents/txt/ep/0480/637} & Image processing apparatus, and method of reversing color therein. & CANON KK (JP) & 1990-10-09\\\hline
ep0186872\footnote{/patents/txt/ep/0186/872} & Terminal protocols & WANG LABORATORIES (US) & 1984-12-31\\\hline
ep0211849\footnote{/patents/txt/ep/0211/849} & WATCH PAGER SYSTEM AND COMMUNICATION PROTOCOL & ATE CORP (US) & 1985-11-27\\\hline
ep0585677\footnote{/patents/txt/ep/0585/677} & Method for positioning. & SIEMENS AG (DE) & 1992-08-28\\\hline
ep0678805\footnote{/patents/txt/ep/0678/805} & Multiple display pointers for computers graphical user interfaces. & IBM (US) & 1994-04-15\\\hline
ep0388160\footnote{/patents/txt/ep/0388/160} & Output apparatus & CANON KK (JP) & 1989-03-15\\\hline
ep0645031\footnote{/patents/txt/ep/0645/031} & Method and apparatus for writing files on nonerasable storage medium & UNISYS CORP (US) & 1992-06-12\\\hline
ep0189202\footnote{/patents/txt/ep/0189/202} & Microprogram control system & HITACHI LTD (JP) & 1985-01-24\\\hline
ep0664028\footnote{/patents/txt/ep/0664/028} & Object-oriented system locator system & TALIGENT INC (US) & 1993-08-04\\\hline
ep0414930\footnote{/patents/txt/ep/0414/930} & Method and circuit arrangement for preventing misrouting of data blocks received on incoming lines via a block exchange. & SIEMENS AG (DE) & 1989-08-28\\\hline
ep0580509\footnote{/patents/txt/ep/0580/509} & Using an embedded interpreted language to develop an interactive user-interface description tool & BULL SA (FR) & 1993-07-21\\\hline
ep0520083\footnote{/patents/txt/ep/0520/083} & Digital telecommunication system with multiple databases having assured data consistency & SIEMENS AG (DE) & 1991-06-28\\\hline
ep0684721\footnote{/patents/txt/ep/0684/721} & Data bus communication. & SONY CORP (JP) & 1994-05-24\\\hline
ep0793828\footnote{/patents/txt/ep/0793/828} & System for communication of image information between multiple-protocol imaging devices & MINNESOTA MINING \& MFG (US) & 1994-11-22\\\hline
ep0663135\footnote{/patents/txt/ep/0663/135} & Programmable telecommunication switch for personal computer & EXCEL IND (US) & 1992-09-29\\\hline
ep0817102\footnote{/patents/txt/ep/0817/102} & Circular size-bounded file technique for a computer operating system & SUN MICROSYSTEMS INC (US) & 1996-07-03\\\hline
ep0540630\footnote{/patents/txt/ep/0540/630} & PROGRAMMABLE DIGITAL ACQUISITION AND TRACKING CONTROLLER & UNISYS CORP (US) & 1990-07-26\\\hline
ep0503202\footnote{/patents/txt/ep/0503/202} & Method of controlling RDS receiver. & PIONEER ELECTRONIC CORP (JP) & 1991-03-08\\\hline
ep0369962\footnote{/patents/txt/ep/0369/962} & Method for processing signals in a channel. & IBM (US) & 1988-11-14\\\hline
ep0454884\footnote{/patents/txt/ep/0454/884} & Telephone exchange, in particular a private branch exchange with a central stored program computer control. & SIEMENS AG (DE) & 1990-05-02\\\hline
ep0752135\footnote{/patents/txt/ep/0752/135} & Computerized stock exchange trading system automatically formatting orders from a spreadsheet to an order entry system & BELZBERG FINANCIAL MARKETS \& N (CA) & 1995-03-03\\\hline
ep0325885\footnote{/patents/txt/ep/0325/885} & Direct cursor-controlled access to multiple application programs. & IBM (US) & 1988-01-26\\\hline
ep0293225\footnote{/patents/txt/ep/0293/225} & Colour picture processing system. & FUJITSU LTD (JP) & 1987-05-29\\\hline
ep0502500\footnote{/patents/txt/ep/0502/500} & Method of tuning a radio receiver using RDS information & BECKER GMBH (DE) & 1992-03-04\\\hline
ep0059666\footnote{/patents/txt/ep/0059/666} & Display device for graphic information transmitted by a video text system. & FRANCE ETAT (FR); TELEDIFFUSION FSE (FR) & 1981-03-03\\\hline
ep0623885\footnote{/patents/txt/ep/0623/885} & Classification information acquisition device. & AUTRONIC BILDVERARBEITUNG (DE) & 1993-05-07\\\hline
ep0294187\footnote{/patents/txt/ep/0294/187} & Computer system for advanced financial applications. & CORPORATE CLASS SOFTWARE (US) & 1987-06-01\\\hline
ep0799455\footnote{/patents/txt/ep/0799/455} & Data compression for seismic signal data & SCHLUMBERGER TECHNOLOGY CORP (US) & 1995-12-15\\\hline
ep0471638\footnote{/patents/txt/ep/0471/638} & Problem prevention on a computer system in a service network of computer systems. & IBM (US) & 1990-08-17\\\hline
ep0667084\footnote{/patents/txt/ep/0667/084} & Information transmission system & SEMCOTEC HANDEL (AT) & 1993-10-27\\\hline
ep0497203\footnote{/patents/txt/ep/0497/203} & Interactive roamer contact system for cellular mobile radiotelephone network. & BELLSOUTH CORP (US) & 1991-01-28\\\hline
ep0473390\footnote{/patents/txt/ep/0473/390} & Superimposition of moving characters on generated still pictures. & NINTENDO CO LTD (JP); RICOH KK (JP) & 1990-08-27\\\hline
ep0589191\footnote{/patents/txt/ep/0589/191} & Non-invasive glucose measurement method and apparatus. & STARK EDWARD W (US) & 1992-09-04\\\hline
ep0377203\footnote{/patents/txt/ep/0377/203} & SYSTEM FOR RECEIVING AND PROCESSING HDLC FRAMES ON A TIME DIVISION MULTIPLEX PCM TYPE LINK, ESPECIALLY FOR A DATA SWITCH & CIT ALCATEL (FR) & 1988-12-30\\\hline
ep0471636\footnote{/patents/txt/ep/0471/636} & Flexible service network for computer systems. & IBM (US) & 1990-08-17\\\hline
ep0595354\footnote{/patents/txt/ep/0595/354} & Billing system for radio communications. & RICOS KK (JP) & 1992-11-27\\\hline
ep0888597\footnote{/patents/txt/ep/0888/597} & Process for data communications and a data communications system for carrying out the process &  & 1997-03-12\\\hline
ep0204389\footnote{/patents/txt/ep/0204/389} & Guide wire communications system and method. & EATON KENWAY INC (US) & 1985-05-02\\\hline
ep0449715\footnote{/patents/txt/ep/0449/715} & APPARATUS FOR THE CODING/DECODING OF IMAGE SIGNALS & FRANCE TELECOM (FR) & 1990-03-26\\\hline
ep0412809\footnote{/patents/txt/ep/0412/809} & Data compression system. & FUJITSU LTD (JP) & 1989-08-22\\\hline
ep0389109\footnote{/patents/txt/ep/0389/109} & Data loading and distributing process and apparatus for control of a patterning process & MILLIKEN RES CORP (US) & 1989-03-23\\\hline
ep0693854\footnote{/patents/txt/ep/0693/854} & System for controlling updates of extended data services (EDS) data & THOMSON CONSUMER ELECTRONICS (US) & 1995-05-23\\\hline
ep0104332\footnote{/patents/txt/ep/0104/332} & Graphic data compression & IBM (US) & 1982-09-27\\\hline
ep0325772\footnote{/patents/txt/ep/0325/772} & Method of recording and reproducing audio information in relation to graphics information. & PIONEER ELECTRONIC CORP (JP) & 1988-11-07\\\hline
ep0583053\footnote{/patents/txt/ep/0583/053} & Graphical database access. & IBM (US) & 1992-07-20\\\hline
ep0575358\footnote{/patents/txt/ep/0575/358} & DATABASE MANAGEMENT SYSTEM GRAPHICAL QUERY FRONT END & WANG LABORATORIES (US) & 1991-03-12\\\hline
ep0418742\footnote{/patents/txt/ep/0418/742} & METHOD AND A DEVICE FOR THE PROTECTED TRANSMISSION OF DATA & BUEHN WILLI (CH); NAEF ERICH (CH) & 1989-09-18\\\hline
ep0824830\footnote{/patents/txt/ep/0824/830} & Real-time video and animation playback process & NOVALOGIC INC (US) & 1995-05-08\\\hline
ep0608252\footnote{/patents/txt/ep/0608/252} & Apparatus for communicating price changes including printer and display devices & ELECTRONIC RETAILING SYST (US) & 1992-08-24\\\hline
ep0471090\footnote{/patents/txt/ep/0471/090} & MESSAGE COMMUNICATION PROCESSING SYSTEM. & FUJITSU LTD (JP) & 1990-06-18\\\hline
ep0522636\footnote{/patents/txt/ep/0522/636} & Communication protocol for main and mobile stations where time slots are used for data packet transmission. & KONINKL PHILIPS ELECTRONICS NV (NL) & 1991-07-08\\\hline
ep0615384\footnote{/patents/txt/ep/0615/384} & Adaptive compression of digital video data. & GI CORP (US) & 1993-03-11\\\hline
ep0479691\footnote{/patents/txt/ep/0479/691} & Process for materializing virtual interactivity between an individual and a data support & INFO TELECOM (FR) & 1991-04-12\\\hline
ep0637174\footnote{/patents/txt/ep/0637/174} & Digital data transmission method and apparatus. & VICTOR COMPANY OF JAPAN (JP) & 1993-10-22\\\hline
ep0894404\footnote{/patents/txt/ep/0894/404} & VIDEO DATA ENCODER AND DECODER & NOKIA MOBILE PHONES LTD (FI); TUERKER MUSTAFA ALI (FI); KALEVO OSSI (FI); HAAVISTO PETRI (FI); NIEWEGLOWSKI JACEK (FI) & 1996-04-18\\\hline
ep0479997\footnote{/patents/txt/ep/0479/997} & Distributed information system having automatic invocation of key management negotiations protocol and method & HUGHES AIRCRAFT CO (US) & 1990-04-27\\\hline
ep0483546\footnote{/patents/txt/ep/0483/546} & Methods for polling mobile users in a multiple cell wireless network & IBM (US) & 1990-10-29\\\hline
ep0373971\footnote{/patents/txt/ep/0373/971} & Up/down loading of databases. & PITNEY BOWES (US) & 1988-12-16\\\hline
ep0594355\footnote{/patents/txt/ep/0594/355} & Multirate, SONET-ready, switching arrangement. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1992-10-20\\\hline
ep0566895\footnote{/patents/txt/ep/0566/895} & File manager for files shared by heterogeneous clients. & IBM (US) & 1992-04-20\\\hline
ep0407050\footnote{/patents/txt/ep/0407/050} & Computer systems including a process database. & INTELLUTION INC (US) & 1989-06-15\\\hline
ep0583107\footnote{/patents/txt/ep/0583/107} & Image processing method and apparatus. & CANON KK (JP) & 1992-07-31\\\hline
ep0426184\footnote{/patents/txt/ep/0426/184} & Bus master command protocol & COMPAQ COMPUTER CORP (US) & 1989-11-03\\\hline
ep0597395\footnote{/patents/txt/ep/0597/395} & Multiple graphical user interface on a single display. & IBM (US) & 1992-11-13\\\hline
ep0840927\footnote{/patents/txt/ep/0840/927} & Compact disc changer utilizing disc database & SONY CORP (JP); SONY ELECTRONICS INC (US) & 1995-07-26\\\hline
ep0314292\footnote{/patents/txt/ep/0314/292} & Concurrent record access database system. & IBM (US) & 1987-10-30\\\hline
ep0657825\footnote{/patents/txt/ep/0657/825} & Non-volatile memory array controllers. & ADVANCED MICRO DEVICES INC (US) & 1993-12-10\\\hline
ep0179936\footnote{/patents/txt/ep/0179/936} & Method and apparatus for bus arbitration in a data processing system & IBM (US) & 1984-10-31\\\hline
ep0554613\footnote{/patents/txt/ep/0554/613} & Barcode indentification system. & MONARCH MARKING SYSTEMS INC (US) & 1991-10-28\\\hline
ep0810539\footnote{/patents/txt/ep/0810/539} & Barcode printing system & MONARCH MARKING SYSTEMS INC (US) & 1991-10-28\\\hline
ep0529844\footnote{/patents/txt/ep/0529/844} & Method and apparatus for deriving object type and obtaining object type attribute values. & SUN MICROSYSTEMS INC (US) & 1991-08-28\\\hline
ep0665497\footnote{/patents/txt/ep/0665/497} & Method and apparatus for a multilayer system quiescent suspend and resume operation. & SUN MICROSYSTEMS INC (US) & 1994-01-31\\\hline
ep0676738\footnote{/patents/txt/ep/0676/738} & System and method for printing overlays for electronic displays devices. & AT \& T GLOBAL INF SOLUTION (US) & 1994-04-04\\\hline
ep0411231\footnote{/patents/txt/ep/0411/231} & Method for compressing and decompressing forms by means of very large symbol matching. & IBM (US) & 1989-08-04\\\hline
ep0442349\footnote{/patents/txt/ep/0442/349} & Minibar system. & TADIRAN ELECTRICAL APPLIANCES (IL) & 1990-02-07\\\hline
ep0493915\footnote{/patents/txt/ep/0493/915} & A switching system using identical switching nodes. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1990-12-31\\\hline
ep0284511\footnote{/patents/txt/ep/0284/511} & Image information code processing system. & FUJITSU LTD (JP) & 1987-03-25\\\hline
ep0349677\footnote{/patents/txt/ep/0349/677} & Image coding system. & MITSUBISHI ELECTRIC CORP (JP) & 1988-07-04\\\hline
ep0619036\footnote{/patents/txt/ep/0619/036} & Communications controller utilizing an external buffer memory with plural channels between a host and network interface operating independently for transferring packets between protocol layers & STANDARD MICROSYST SMC (US) & 1992-09-28\\\hline
ep0219685\footnote{/patents/txt/ep/0219/685} & Method for entrapping unauthorized computer access & IBM (US) & 1985-10-23\\\hline
ep0535966\footnote{/patents/txt/ep/0535/966} & Image processing method and system & NCR INT INC (US) & 1991-10-03\\\hline
ep0564538\footnote{/patents/txt/ep/0564/538} & Method for the computer-assisted control of a machine or process & HMR GMBH (DE) & 1991-12-24\\\hline
\end{longtable}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: latex ;
% End: ;

