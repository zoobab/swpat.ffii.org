\begin{subdocument}{swpiktxt20}{Top Software Probability Batch nr 20: 1901-2000}{http://swpat.ffii.org/patents/txt/ep20/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{longtable}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
\endhead
ep0645700\footnote{/patents/txt/ep/0645/700} & Programmable logic array. & TOSHIBA MICRO ELECTRONICS (JP); TOKYO SHIBAURA ELECTRIC CO (JP) & 1993-09-29\\\hline
ep0323438\footnote{/patents/txt/ep/0323/438} & Programmable word length memory in a gate array with bidirectional symmetry. & HUGHES AIRCRAFT CO (US) & 1984-07-18\\\hline
ep0319522\footnote{/patents/txt/ep/0319/522} & Programmable word length memory in a gate array with bidirectional symmetry. & HUGHES AIRCRAFT CO (US) & 1984-07-18\\\hline
ep0448980\footnote{/patents/txt/ep/0448/980} & Apparatus and method for memory device fault repair & TEXAS INSTRUMENTS INC (US) & 1990-03-29\\\hline
ep0307660\footnote{/patents/txt/ep/0307/660} & Method for programming domestic video apparatuses. & THOMSON BRANDT GMBH (DE) & 1987-08-26\\\hline
ep0368928\footnote{/patents/txt/ep/0368/928} & Method for programming domestic video apparatuses. & THOMSON BRANDT GMBH (DE) & 1987-08-26\\\hline
ep0701365\footnote{/patents/txt/ep/0701/365} & Method for programming a receiver of television programmes & THOMSON MULTIMEDIA SA (FR) & 1994-08-17\\\hline
ep0398681\footnote{/patents/txt/ep/0398/681} & Output apparatus. & CANON KK (JP) & 1989-05-17\\\hline
ep0426199\footnote{/patents/txt/ep/0426/199} & Trap selector. & TLV CO LTD (JP) & 1989-11-02\\\hline
ep0188431\footnote{/patents/txt/ep/0188/431} & Gate array with bidirectional symmetry & HUGHES AIRCRAFT CO (US) & 1984-07-18\\\hline
ep0608708\footnote{/patents/txt/ep/0608/708} & Automatic handwriting recognition using both static and dynamic parameters. & IBM (US) & 1993-01-27\\\hline
ep0007417\footnote{/patents/txt/ep/0007/417} & Input key device for electronic calculators, particularly microcomputers, for entering instructions and/or sequential instructions. & SIEMENS AG (DE) & 1978-07-10\\\hline
ep0122626\footnote{/patents/txt/ep/0122/626} & Apparatus for the time or event controlled recording of television signals. & RUNDFUNKSCHUTZ INTERESSENGEM (DE) & 1983-04-15\\\hline
ep0737953\footnote{/patents/txt/ep/0737/953} & Vehicular emergency message system & FORD MOTOR CO (US) & 1995-04-10\\\hline
ep0507119\footnote{/patents/txt/ep/0507/119} & Improved video cassette recorder. & EDICO SRL (IT) & 1991-03-15\\\hline
ep0424725\footnote{/patents/txt/ep/0424/725} & System for the automatic recording of a television programme & SISVEL SPA (IT) & 1989-10-23\\\hline
ep0448764\footnote{/patents/txt/ep/0448/764} & Programmable electrical hearing aid. & SIEMENS AG (DE) & 1990-03-30\\\hline
ep0515714\footnote{/patents/txt/ep/0515/714} & Computer-aided binary image processing method. & SIEMENS AG (DE) & 1991-05-28\\\hline
ep0659292\footnote{/patents/txt/ep/0659/292} & DIGITAL COMMUNICATIONS SYSTEM, A TRANSMITTING APPARATUS AND A RECEIVING APPARATUS FOR USE IN THE SYSTEM & PHILIPS ELECTRONICS NV (NL); PHILIPS ELECTRONICS UK LTD (GB); PHILIPS NORDEN AB (SE) & 1993-07-13\\\hline
ep0354031\footnote{/patents/txt/ep/0354/031} & Graphical image editing. & XEROX CORP (US) & 1988-08-04\\\hline
ep0483299\footnote{/patents/txt/ep/0483/299} & IMAGE PATTERN EXTRACTION & GOUGE JAMES O (US) & 1990-01-22\\\hline
ep0218046\footnote{/patents/txt/ep/0218/046} & Arrangement for a traffic guiding and information system. & SIEMENS AG (DE) & 1985-08-14\\\hline
ep0605345\footnote{/patents/txt/ep/0605/345} & Contacting recording slider for use in a lubricant containing information storage system. & IBM (US) & 1992-12-29\\\hline
ep0285154\footnote{/patents/txt/ep/0285/154} & System for connecting a plurality of terminals to a network-connecting equipment in a broadband network. & SIEMENS AG (DE); PHILIPS PATENTVERWALTUNG (DE); SEL ALCATEL AG (DE); PHILIPS NV (NL) & 1987-04-01\\\hline
ep0529787\footnote{/patents/txt/ep/0529/787} & Network management agent with user created objects. & HEWLETT PACKARD CO (US) & 1991-08-29\\\hline
ep0500100\footnote{/patents/txt/ep/0500/100} & Video signal synthesizing system for synthesizing system's own signal and external signal. & FUJITSU LTD (JP) & 1991-03-14\\\hline
ep0613390\footnote{/patents/txt/ep/0613/390} & Compressed storage of data in cardiac pacemakers & MEDTRONIC INC (US) & 1992-08-26\\\hline
ep0599653\footnote{/patents/txt/ep/0599/653} & Digital sound level control in a telephone system. & NIPPON ELECTRIC CO (JP) & 1992-11-26\\\hline
ep0482116\footnote{/patents/txt/ep/0482/116} & IMAGE-BASED DOCUMENT PROCESSING SYSTEM & UNISYS CORP (US) & 1989-10-10\\\hline
ep0509430\footnote{/patents/txt/ep/0509/430} & Error correcting system. & HITACHI LTD (JP) & 1991-04-15\\\hline
ep0218113\footnote{/patents/txt/ep/0218/113} & Protocol converting apparatus for videotex system & SONY CORP (JP) & 1985-09-13\\\hline
ep0388843\footnote{/patents/txt/ep/0388/843} & Remote enabling of software controllable features of an external device coupled with an electronic franking machine. & ALCATEL SATMAM (FR) & 1989-03-23\\\hline
ep0578257\footnote{/patents/txt/ep/0578/257} & System and method for mode switching. & MICROSOFT CORP (US) & 1992-11-16\\\hline
ep0646887\footnote{/patents/txt/ep/0646/887} & Method and system for managing character recognition of a plurality of document form images having common data types. & IBM (US) & 1993-10-04\\\hline
ep0421482\footnote{/patents/txt/ep/0421/482} & Television and market research data collection system and method. & CONTROL DATA CORP (US) & 1984-10-05\\\hline
ep0454985\footnote{/patents/txt/ep/0454/985} & Scalable compound instruction set machine architecture. & IBM (US) & 1990-05-04\\\hline
ep0254352\footnote{/patents/txt/ep/0254/352} & A programmable machine system. & UNILEVER NV (NL); UNILEVER PLC (GB) & 1986-07-10\\\hline
ep0433073\footnote{/patents/txt/ep/0433/073} & Document formatting apparatus. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1989-12-14\\\hline
ep0538059\footnote{/patents/txt/ep/0538/059} & System and method for document processing. & RICOH KK (JP) & 1991-10-17\\\hline
ep0260462\footnote{/patents/txt/ep/0260/462} & Arithmetic coding data compression/de-compression by selectively employed, diverse arithmetic coding encoders and decoders & IBM (US) & 1986-09-15\\\hline
ep0236828\footnote{/patents/txt/ep/0236/828} & Programmable-memory control. & SIEMENS AG (DE) & 1986-03-07\\\hline
ep0644553\footnote{/patents/txt/ep/0644/553} & Circuit and method of interconnecting content addressable memory. & CODEX CORP (US) & 1993-09-20\\\hline
ep0490980\footnote{/patents/txt/ep/0490/980} & MULTIPLE FACILITY OPERATING SYSTEM ARCHITECTURE & AUSPEX SYSTEMS INC (US) & 1989-09-08\\\hline
ep0198176\footnote{/patents/txt/ep/0198/176} & Method for informing a telephone subscriber of the existing states of his connection. & TELEFONBAU \& NORMALZEIT GMBH (DE) & 1985-04-10\\\hline
ep0199863\footnote{/patents/txt/ep/0199/863} & Visual display unit with character overstrike & IBM (US) & 1985-04-26\\\hline
ep0278722\footnote{/patents/txt/ep/0278/722} & Document composition system using named formats and named fonts. & FISCHER ADDISON M & 1987-02-13\\\hline
ep0850453\footnote{/patents/txt/ep/0850/453} & Presentation system for individual personal computers in a personal computer network & WISEVISION AS (NO) & 1995-09-05\\\hline
ep0387146\footnote{/patents/txt/ep/0387/146} & Programmable analog neural network & THOMSON CSF (FR) & 1989-03-10\\\hline
ep0466091\footnote{/patents/txt/ep/0466/091} & Fault processing system for processing faults of producing points. & FUJITSU LTD (JP) & 1990-07-10\\\hline
ep0620051\footnote{/patents/txt/ep/0620/051} & Method and device for generating colorimetric data for use in the automatic sorting of products, notably fruits or vegetables & MATERIEL ARBORICULTURE (FR) & 1993-04-16\\\hline
ep0485081\footnote{/patents/txt/ep/0485/081} & Data compression dictionary access minimization logic. & HEWLETT PACKARD CO (US) & 1990-11-07\\\hline
ep0379336\footnote{/patents/txt/ep/0379/336} & Display device and display system incorporating such a device. & HITACHI LTD (JP) & 1989-01-18\\\hline
ep0414565\footnote{/patents/txt/ep/0414/565} & Portable graphic computer apparatus. & SONY CORP (JP) & 1989-08-25\\\hline
ep0509996\footnote{/patents/txt/ep/0509/996} & COMPUTER APPARATUS FOR BRUSH STYLED WRITING & WANG LABORATORIES (US) & 1990-01-08\\\hline
ep0655714\footnote{/patents/txt/ep/0655/714} & Low level flight flyability transformation of digital terrain elevation data. & HONEYWELL INC (US) & 1994-01-18\\\hline
ep0655698\footnote{/patents/txt/ep/0655/698} & Low level flight flyability transformation of digital terrain elevation data. & HONEYWELL INC (US) & 1994-01-18\\\hline
ep0655699\footnote{/patents/txt/ep/0655/699} & Low level flight flyability transformation of digital terrain elevation data. & HONEYWELL INC (US) & 1994-01-18\\\hline
ep0540403\footnote{/patents/txt/ep/0540/403} & Motion-dependent image classification for editing purposes & TELEDIFFUSION FSE (FR) & 1991-10-30\\\hline
ep0238537\footnote{/patents/txt/ep/0238/537} & System for preventing software piracy employing multi-encrypted keys and single decryption circuit modules & BURROUGHS CORP (US) & 1985-09-03\\\hline
ep0679028\footnote{/patents/txt/ep/0679/028} & Inverse transport processor with memory address circuitry. & THOMSON CONSUMER ELECTRONICS (US) & 1994-04-22\\\hline
ep0356065\footnote{/patents/txt/ep/0356/065} & Secure management of keys using control vectors. & IBM (US) & 1988-08-29\\\hline
ep0236349\footnote{/patents/txt/ep/0236/349} & Digital speech coder with different excitation types & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1985-08-28\\\hline
ep0511420\footnote{/patents/txt/ep/0511/420} & A cryptographic system based on information difference. & OMNISEC AG (CH) & 1991-04-29\\\hline
ep0260459\footnote{/patents/txt/ep/0260/459} & Terminal communications circuit & IBM (US) & 1986-09-17\\\hline
ep0201001\footnote{/patents/txt/ep/0201/001} & System for providing reprogramming data to an embedded processor & GEN DYNAMICS POMONA DIV (US) & 1985-04-29\\\hline
ep0529945\footnote{/patents/txt/ep/0529/945} & Method and apparatus for programmable memory control with error regulation and test functions. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1991-08-29\\\hline
ep0010140\footnote{/patents/txt/ep/0010/140} & Reconfigurable cluster of data-entry terminals & CHAMOFF MARTIN E; MILLER IRVING L; THORSON DONALD L & 1980-01-04\\\hline
ep0390880\footnote{/patents/txt/ep/0390/880} & Handtool provided with flexion detection means of the extremity element. & FISCHER GUY & 1988-06-30\\\hline
ep0349443\footnote{/patents/txt/ep/0349/443} & Handtool provided with flexion detection means of the extremity element. & FISCHER GUY & 1988-06-30\\\hline
ep0594276\footnote{/patents/txt/ep/0594/276} & Apparatus and method for designing a three-dimensional free-form surface. & NAT INST AGRO ENVIRONMENT (JP) & 1992-10-22\\\hline
ep0607412\footnote{/patents/txt/ep/0607/412} & Network adapter with host indication optimization & 3COM CORP (US) & 1992-07-28\\\hline
ep0429054\footnote{/patents/txt/ep/0429/054} & Data representation and protocol. & DIGITAL EQUIPMENT CORP (US) & 1989-11-20\\\hline
ep0742922\footnote{/patents/txt/ep/0742/922} & Method of dispatching documents by converting the documents from a customer specific data format to a standardized data format, and then transmitting the documents via most favorable way to recipients & ALCATEL NV (NL) & 1995-01-25\\\hline
ep0326580\footnote{/patents/txt/ep/0326/580} & Programmable logic cell and array & FURTEK FREDERICK C (US) & 1986-11-07\\\hline
ep0589369\footnote{/patents/txt/ep/0589/369} & Video tape recorder which allows preset program recording. & SANYO ELECTRIC CO (JP) & 1992-09-22\\\hline
ep0373042\footnote{/patents/txt/ep/0373/042} & Integrated circuit for dynamic programming. & BULL SA (FR) & 1988-11-30\\\hline
ep0610426\footnote{/patents/txt/ep/0610/426} & Input/output (I/O) bidirectional buffer for interfacing I/O ports of a field programmable interconnection device with array ports of a cross-point switch & CUBE INC I (US) & 1991-10-30\\\hline
ep0497593\footnote{/patents/txt/ep/0497/593} & On-the-fly error correction with embedded digital controller. & QUANTUM CORP (US) & 1991-02-01\\\hline
ep0206704\footnote{/patents/txt/ep/0206/704} & System for determining authenticity of an external memory used in an information processing apparatus & NINTENDO CO LTD (JP) & 1985-06-28\\\hline
ep0473066\footnote{/patents/txt/ep/0473/066} & Inter-local area network connecting system. & MITSUBISHI ELECTRIC CORP (JP) & 1990-08-27\\\hline
ep0397138\footnote{/patents/txt/ep/0397/138} & Embedded control technique for distributed control systems. & ALCATEL NV (NL) & 1989-05-10\\\hline
ep0196751\footnote{/patents/txt/ep/0196/751} & Microprocessor with block move instruction. & TEXAS INSTRUMENTS INC (US) & 1985-02-12\\\hline
ep0120977\footnote{/patents/txt/ep/0120/977} & Card image data processing system & FUJITSU LTD (JP) & 1982-10-11\\\hline
ep0652508\footnote{/patents/txt/ep/0652/508} & Microprocessor with block move instruction. & TEXAS INSTRUMENTS INC (US) & 1985-02-12\\\hline
ep0862761\footnote{/patents/txt/ep/0862/761} & Data error detection and correction for a shared SRAM &  & 1995-10-10\\\hline
ep0020507\footnote{/patents/txt/ep/0020/507} & Method and apparatus for electronic image processing of documents for accounting purposes & NCR CO & 1978-10-06\\\hline
ep0373969\footnote{/patents/txt/ep/0373/969} & Central postage data communication network. & PITNEY BOWES (US) & 1988-12-16\\\hline
ep0268792\footnote{/patents/txt/ep/0268/792} & Data multiplex transmission system & MITSUBISHI ELECTRIC CORP (JP) & 1986-11-28\\\hline
ep0463471\footnote{/patents/txt/ep/0463/471} & Image processing system for documentary data. & NAT COMPUTER SYSTEMS INC (US) & 1990-06-13\\\hline
ep0645722\footnote{/patents/txt/ep/0645/722} & A data processing apparatus. & L \& P SYSTEMS LTD (IE) & 1993-09-28\\\hline
ep0823087\footnote{/patents/txt/ep/0823/087} & Methods and apparatus for performing heap management and protecting data structure integrity in non-volatile memory & HONEYWELL INC (US) & 1995-04-26\\\hline
ep0738972\footnote{/patents/txt/ep/0738/972} & Fault information notification system & FUJI XEROX CO LTD (JP) & 1995-04-19\\\hline
ep0456249\footnote{/patents/txt/ep/0456/249} & System for integrating application programs in a heterogeneous network enviroment. & HEWLETT PACKARD CO (US) & 1990-05-10\\\hline
ep0349459\footnote{/patents/txt/ep/0349/459} & Method for accessing selected windows in a multi-tasking system. & IBM (US) & 1988-06-30\\\hline
ep0472921\footnote{/patents/txt/ep/0472/921} & Fuzzy inference system. & OMRON TATEISI ELECTRONICS CO (JP) & 1990-07-26\\\hline
ep0494576\footnote{/patents/txt/ep/0494/576} & Method and system for controlling the degree of compression applied by a node of a data communication network. & IBM (US) & 1991-01-09\\\hline
ep0305172\footnote{/patents/txt/ep/0305/172} & Radio data system receivers. & BRITISH BROADCASTING CORP (GB) & 1987-08-24\\\hline
ep0714529\footnote{/patents/txt/ep/0714/529} & MULTIMEDIA DATA ROUTING SYSTEM & TALIGENT INC (US) & 1993-09-13\\\hline
ep0238988\footnote{/patents/txt/ep/0238/988} & Digital signal transmitting system. & THOMSON BRANDT GMBH (DE) & 1986-03-27\\\hline
ep0458362\footnote{/patents/txt/ep/0458/362} & Low power consumption precharge type programmable logic array (PLA) & TOKYO SHIBAURA ELECTRIC CO (JP) & 1990-05-24\\\hline
\end{longtable}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: latex ;
% End: ;

