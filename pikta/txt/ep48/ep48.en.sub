\begin{subdocument}{swpiktxt48}{Top Software Probability Batch nr 48: 4701-4800}{http://swpat.ffii.org/patents/txt/ep48/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0241071\footnote{/patents/txt/ep/0241/071} & A method of generating and processing models of two-dimensional or three-dimensional objects in a computer and of reproducing said models on a display. & OCE NEDERLAND BV (NL) & 1986-04-02\\\hline
ep0515016\footnote{/patents/txt/ep/0515/016} & Instruction scheduler for a computer. & IBM (US) & 1991-05-23\\\hline
ep0617799\footnote{/patents/txt/ep/0617/799} & Method of seismic time migration using a massively parallel computer & SCHLUMBERGER TECHNOLOGY CORP (US) & 1991-12-20\\\hline
ep0595824\footnote{/patents/txt/ep/0595/824} & Computer work station & RAMEY III THOMAS B (US); BROWN THOMAS J (US); BROWN JAMES C (US) & 1991-04-29\\\hline
ep0645709\footnote{/patents/txt/ep/0645/709} & Computer memory backup arrangement. & AT \& T CORP (US) & 1993-09-23\\\hline
ep0274392\footnote{/patents/txt/ep/0274/392} & Improved relational data base system. & WANG LABORATORIES (US) & 1987-01-08\\\hline
ep0280795\footnote{/patents/txt/ep/0280/795} & Method and apparatus for storing a digital data base divided into parcels. & PHILIPS NV (NL) & 1986-10-23\\\hline
ep0301707\footnote{/patents/txt/ep/0301/707} & Apparatus and method for providing an extended processing environment on nonmicrocoded data processing system. & DIGITAL EQUIPMENT CORP (US) & 1987-07-01\\\hline
ep0359395\footnote{/patents/txt/ep/0359/395} & Data input in an expert system. & HITACHI LTD (JP) & 1988-08-12\\\hline
ep0367221\footnote{/patents/txt/ep/0367/221} & Drop/insert multiplexer for data channel access units. & FUJITSU LTD (JP) & 1988-10-31\\\hline
ep0396285\footnote{/patents/txt/ep/0396/285} & Recording/reproducing compressed data on a rotatable record medium. & SONY CORP (JP) & 1989-05-16\\\hline
ep0416153\footnote{/patents/txt/ep/0416/153} & Method for dividing any-length operands respectively normalized at the beginning for data processing equipment and digital divider for carrying out the method. & SIEMENS AG (DE) & 1989-09-07\\\hline
ep0482864\footnote{/patents/txt/ep/0482/864} & An image data processing apparatus. & FUJITSU LTD (JP) & 1990-10-30\\\hline
ep0494575\footnote{/patents/txt/ep/0494/575} & Cut and paste filtering of unbounded, dynamic, non-modifiable data streams. & IBM (US) & 1991-01-11\\\hline
ep0571171\footnote{/patents/txt/ep/0571/171} & Video data encoding and decoding method and apparatus. & SAMSUNG ELECTRONICS CO LTD (KR) & 1992-05-18\\\hline
ep0572228\footnote{/patents/txt/ep/0572/228} & Data processing method and apparatus thereof. & CANON KK (JP) & 1992-05-27\\\hline
ep0595440\footnote{/patents/txt/ep/0595/440} & Method for modeling and simulating data traffic on networks. & COMDISCO SYSTEMS INC (US) & 1992-10-27\\\hline
ep0594969\footnote{/patents/txt/ep/0594/969} & Data processing system and method for calculating the sum of a base plus offset. & MOTOROLA INC (US) & 1992-10-27\\\hline
ep0614137\footnote{/patents/txt/ep/0614/137} & Data processing system providing an extensible register and method thereof & MOTOROLA INC (US) & 1992-12-24\\\hline
ep0619932\footnote{/patents/txt/ep/0619/932} & RECEIVING SUBSCRIBER DATA FROM HLR IN GSM MSC/VLR & ERICSSON TELEFON AB L M (SE) & 1992-10-27\\\hline
ep0624301\footnote{/patents/txt/ep/0624/301} & APPARATUS AND METHOD FOR PROCESSING OF IMAGE DATA & EASTMAN KODAK CO (US) & 1992-11-27\\\hline
ep0739107\footnote{/patents/txt/ep/0739/107} & Public key method of encoding data & DEUTSCHE TELEKOM AG (DE) & 1995-04-12\\\hline
ep0267577\footnote{/patents/txt/ep/0267/577} & Decoding device capable of producing a decoded video signal with a reduced delay & KOKUSAI DENSHIN DENWA CO LTD (JP); NIPPON TELEGRAPH \& TELEPHONE (JP); NIPPON ELECTRIC CO (JP) & 1986-11-10\\\hline
ep0618722\footnote{/patents/txt/ep/0618/722} & Video decoder with five page memory for decoding of intraframes, predicted frames and bidirectional frames & PHILIPS CORP (US) & 1993-03-31\\\hline
ep0602621\footnote{/patents/txt/ep/0602/621} & Image encoding and decoding apparatus and method. & SONY CORP (JP) & 1992-12-15\\\hline
ep0467040\footnote{/patents/txt/ep/0467/040} & Adaptive motion compensation for digital television. & GEN INSTRUMENT CORP (US) & 1990-06-15\\\hline
ep0666652\footnote{/patents/txt/ep/0666/652} & Shifter stage for variable-length digital code decoder & SGS THOMSON MICROELECTRONICS (FR) & 1994-02-04\\\hline
ep0509020\footnote{/patents/txt/ep/0509/020} & Time division multiplexed selective call system & MOTOROLA INC (US) & 1990-01-02\\\hline
ep0383331\footnote{/patents/txt/ep/0383/331} & Color document image processing apparatus. & HITACHI LTD (JP) & 1989-05-08\\\hline
ep0541313\footnote{/patents/txt/ep/0541/313} & Video signal encoding apparatus. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1991-11-05\\\hline
ep0685963\footnote{/patents/txt/ep/0685/963} & Image processing apparatus and method. & CANON KK (JP) & 1994-05-31\\\hline
ep0389886\footnote{/patents/txt/ep/0389/886} & Ring reduction logic mechanism. & BULL HN INFORMATION SYST (US) & 1989-03-31\\\hline
ep0272821\footnote{/patents/txt/ep/0272/821} & Method and apparatus for computation stack recovery in a calculator. & HEWLETT PACKARD CO (US) & 1986-12-24\\\hline
ep0444364\footnote{/patents/txt/ep/0444/364} & Physical database design system & DIGITAL EQUIPMENT CORP (US) & 1990-02-26\\\hline
ep0336035\footnote{/patents/txt/ep/0336/035} & Tree structure database system. & IBM (US) & 1988-04-08\\\hline
ep0590922\footnote{/patents/txt/ep/0590/922} & Improved decompression of standard ADCT-compressed images. & XEROX CORP (US) & 1992-10-02\\\hline
ep0637387\footnote{/patents/txt/ep/0637/387} & Magnetic resonance imaging color composites & UNIV SOUTH FLORIDA (US) & 1992-04-21\\\hline
ep0747856\footnote{/patents/txt/ep/0747/856} & Data stream decoding device & SGS THOMSON MICROELECTRONICS (FR) & 1995-06-09\\\hline
ep0434415\footnote{/patents/txt/ep/0434/415} & Method of measuring skew angles. & XEROX CORP (US) & 1989-12-21\\\hline
ep0509337\footnote{/patents/txt/ep/0509/337} & Track ball mounted on keyboard. & LOGITECH INC (US) & 1992-03-25\\\hline
ep0379998\footnote{/patents/txt/ep/0379/998} & Divider for carrying out high speed arithmetic operation & OKI ELECTRIC IND CO LTD (JP) & 1989-01-24\\\hline
ep0413485\footnote{/patents/txt/ep/0413/485} & Performance improvement tool for rule based expert systems. & IBM (US) & 1989-08-14\\\hline
ep0411747\footnote{/patents/txt/ep/0411/747} & Multiple instruction decoder. & ADVANCED MICRO DEVICES INC (US) & 1989-06-06\\\hline
ep0457000\footnote{/patents/txt/ep/0457/000} & Method and apparatus for performing patient documentation. & HEWLETT PACKARD CO (US) & 1990-05-15\\\hline
ep0602886\footnote{/patents/txt/ep/0602/886} & Masks for selecting multi-bit components in a composite operand. & XEROX CORP (US) & 1992-12-18\\\hline
ep0547246\footnote{/patents/txt/ep/0547/246} & Microprocessor architecture with a switch network for data transfer between cache, memory port, and IOU & SEIKO EPSON CORP (JP) & 1991-07-08\\\hline
ep0558326\footnote{/patents/txt/ep/0558/326} & Enhanced call-back authentication method and apparatus. & HUGHES AIRCRAFT CO (US) & 1992-02-27\\\hline
ep0460751\footnote{/patents/txt/ep/0460/751} & Method of transmitting audio and/or video signals. & KONINKL PHILIPS ELECTRONICS NV (NL) & 1990-06-05\\\hline
ep0568081\footnote{/patents/txt/ep/0568/081} & Ophthalmological apparatus for examination and/or treatment. & RODENSTOCK INSTR (DE) & 1992-04-30\\\hline
ep0756218\footnote{/patents/txt/ep/0756/218} & Miniaturized operating panel for a printer & SEIKO EPSON CORP (JP) & 1995-07-27\\\hline
ep0286383\footnote{/patents/txt/ep/0286/383} & Multiplex transmission system. & FURUKAWA ELECTRIC CO LTD (JP) & 1987-04-06\\\hline
ep0548046\footnote{/patents/txt/ep/0548/046} & Musical tone generating apparatus. & YAMAHA CORP (JP) & 1986-04-15\\\hline
ep0548045\footnote{/patents/txt/ep/0548/045} & Musical tone generating apparatus. & YAMAHA CORP (JP) & 1986-04-15\\\hline
ep0493648\footnote{/patents/txt/ep/0493/648} & Synchronized lyric display device. & RICOS KK (JP) & 1991-01-01\\\hline
ep0548384\footnote{/patents/txt/ep/0548/384} & Method of improving the seismic resolution of geologic structures. & CHEVRON RES \& TECH (US) & 1990-06-26\\\hline
ep0631443\footnote{/patents/txt/ep/0631/443} & Device for the transmission of images collected by a satellite. & CENTRE NAT ETD SPATIALES (FR) & 1993-06-22\\\hline
ep0635835\footnote{/patents/txt/ep/0635/835} & Recording medium, recording and/or reproducing apparatus and recording and/or reproducing method. & SONY CORP (JP) & 1994-05-30\\\hline
ep0327533\footnote{/patents/txt/ep/0327/533} & Radio communication receiver with apparatus for altering bit rate of the receiver & MOTOROLA INC (US) & 1986-10-21\\\hline
ep0625103\footnote{/patents/txt/ep/0625/103} & Communication system & MADGE NETWORKS LTD (GB) & 1992-02-07\\\hline
ep0412422\footnote{/patents/txt/ep/0412/422} & Self-test system and method for external programming device. & SIEMENS ELEMA AB (SE); SIEMENS AG (DE) & 1989-08-08\\\hline
ep0487743\footnote{/patents/txt/ep/0487/743} & MICROCOMPUTER PROVIDED WITH BUILT-IN CONVERTER. & OKI ELECTRIC IND CO LTD (JP) & 1990-06-11\\\hline
ep0153333\footnote{/patents/txt/ep/0153/333} & Method and apparatus for communicating variable length messages between a primary station and remote stations of a data communications system & MOTOROLA INC (US) & 1983-07-11\\\hline
ep0610180\footnote{/patents/txt/ep/0610/180} & DOCUMENT SECURITY SYSTEM & SIGNATURE VERIFICATION SYSTEMS (GB) & 1990-09-07\\\hline
ep0461899\footnote{/patents/txt/ep/0461/899} & Coordinate data processing apparatus using pointing means. & SONY CORP (JP) & 1990-06-14\\\hline
ep0533343\footnote{/patents/txt/ep/0533/343} & System and method for data management & ADVANCED MICRO DEVICES INC (US) & 1991-09-16\\\hline
ep0738951\footnote{/patents/txt/ep/0738/951} & Data processing method and apparatus to input and output trace data & CANON KK (JP) & 1995-04-18\\\hline
ep0725354\footnote{/patents/txt/ep/0725/354} & A game machine having play-by-play announcement & KONAMI CO LTD (JP) & 1995-01-31\\\hline
ep0446886\footnote{/patents/txt/ep/0446/886} & Information input/output apparatus. & CANON KK (JP) & 1990-03-14\\\hline
ep0309302\footnote{/patents/txt/ep/0309/302} & Interchange system between logic modules. & RILEY ROBERT E JR & 1987-08-27\\\hline
ep0360899\footnote{/patents/txt/ep/0360/899} & Queue comprising a plurality of memory elements. & SIEMENS AG (DE) & 1988-09-28\\\hline
ep0665967\footnote{/patents/txt/ep/0665/967} & Programming machine & SIEMENS AKTIENGESSELSCHAFT (DE) & 1993-10-13\\\hline
ep0484652\footnote{/patents/txt/ep/0484/652} & First-in-first-out buffer. & INT COMPUTERS LTD (GB) & 1990-11-06\\\hline
ep0506685\footnote{/patents/txt/ep/0506/685} & Method and apparatus for simulation of a physical process & UNIV STRATHCLYDE (GB) & 1990-11-27\\\hline
ep0698864\footnote{/patents/txt/ep/0698/864} & Hire vehicle transportation system & DAIMLER BENZ AG (DE) & 1994-08-23\\\hline
ep0438877\footnote{/patents/txt/ep/0438/877} & Method of reducing data storage requirements associated with computer windowing environments. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1990-01-22\\\hline
ep0722139\footnote{/patents/txt/ep/0722/139} & Input system for computer & DOKA IND GMBH (AT) & 1995-01-13\\\hline
ep0539696\footnote{/patents/txt/ep/0539/696} & Method for contactless energy and data transmission & BKS GMBH (DE); SIEMENS AG (DE) & 1991-09-17\\\hline
ep0532643\footnote{/patents/txt/ep/0532/643} & METHOD FOR OPTIMIZING SOFTWARE FOR ANY ONE OF A PLURALITY OF VARIANT ARCHITECTURES & 3COM CORP (US) & 1990-06-04\\\hline
ep0740816\footnote{/patents/txt/ep/0740/816} & Method of sampling a terrain data base & HONEYWELL INC (US) & 1994-01-18\\\hline
ep0028333\footnote{/patents/txt/ep/0028/333} & Plural null digital interconnections & IBM & 1979-11-02\\\hline
ep0465054\footnote{/patents/txt/ep/0465/054} & Communications processor. & ADVANCED MICRO DEVICES INC (US) & 1990-07-06\\\hline
ep0856225\footnote{/patents/txt/ep/0856/225} & \#f & FRAUNHOFER GES FORSCHUNG (DE) & 1996-06-26\\\hline
ep0877309\footnote{/patents/txt/ep/0877/309} & Virtual vehicle sensors based on neural networks trained using data generated by simulation models & FORD GLOBAL TECH INC (US) & 1997-05-07\\\hline
ep0215515\footnote{/patents/txt/ep/0215/515} & Wave form analyser, especially transient recorder. & BAKKER ELECTRONICS DONGEN BV (NL) & 1985-09-04\\\hline
ep0446817\footnote{/patents/txt/ep/0446/817} & Method for reducing the search complexity in analysis-by-synthesis coding & GTE LABORATORIES INC (US) & 1990-03-15\\\hline
ep0481971\footnote{/patents/txt/ep/0481/971} & SIGNATURE ANALYSIS CONTROL SYSTEM FOR A STAMPING PRESS & JOHNSON SERVICE CO (US) & 1988-03-14\\\hline
ep0463982\footnote{/patents/txt/ep/0463/982} & APPARATUS FOR THE ANALYSIS IN CONTINUOUS MODE AND IN IMPULSE MODE OF THE DISTRIBUTION OF ENERGY WITHIN A POWER LASER BEAM AND THE ALIGNMENT OF THIS LATTER & STRASBOURG ELEC (FR) & 1990-06-26\\\hline
ep0557150\footnote{/patents/txt/ep/0557/150} & System and method of introducing and controlling compressed gases for impurity analysis & AIR LIQUIDE (FR) & 1992-01-31\\\hline
ep0549905\footnote{/patents/txt/ep/0549/905} & Method and apparatus for automated cell analysis. & CELL ANALYSIS SYSTEMS INC (US) & 1991-12-06\\\hline
ep0549858\footnote{/patents/txt/ep/0549/858} & Substance quantitative analysis method. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1991-12-18\\\hline
ep0736186\footnote{/patents/txt/ep/0736/186} & Analysis of velocity data &  & 1994-06-06\\\hline
ep0598127\footnote{/patents/txt/ep/0598/127} & METHOD AND APPARATUS FOR ANALYZING PHYSICAL QUANTITIES, AND APPARATUS FOR REMOVING LINE SPECTRUM NOISE. & DAIKIN IND LTD (JP) & 1991-11-25\\\hline
ep0822538\footnote{/patents/txt/ep/0822/538} & Method of transforming periodic signal using smoothed spectrogram, method of transforming sound using phasing component and method of analyzing signal using optimum interpolation function & ATR HUMAN INF PROCESSING (JP) & 1996-12-24\\\hline
ep0176972\footnote{/patents/txt/ep/0176/972} & Multiprocessor shared pipeline cache memory with split cycle and concurrent utilization & HONEYWELL INF SYSTEMS (US) & 1984-09-27\\\hline
ep0598535\footnote{/patents/txt/ep/0598/535} & Pending write-back controller for a cache controller coupled to a packet switched memory bus. & XEROX CORP (US); SUN MICROSYSTEMS INC (US) & 1992-11-09\\\hline
ep0847555\footnote{/patents/txt/ep/0847/555} & System and method for sequential detection in a cache management system & STORAGE TECHNOLOGY CORP (US) & 1995-08-29\\\hline
ep0742498\footnote{/patents/txt/ep/0742/498} & Implementation of a single channel code program in a system with a two-channel safety-oriented structure & SIEMENS AG (DE) & 1995-05-11\\\hline
ep0393614\footnote{/patents/txt/ep/0393/614} & Speech coding and decoding apparatus. & MITSUBISHI ELECTRIC CORP (JP) & 1989-04-21\\\hline
ep0359729\footnote{/patents/txt/ep/0359/729} & Encryption with subsequent source coding. & TELEVERKET (SE) & 1988-09-15\\\hline
ep0563229\footnote{/patents/txt/ep/0563/229} & Generating the variable control parameters of a speech signal synthesis filter & BRITISH TELECOMM (GB) & 1991-12-20\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

