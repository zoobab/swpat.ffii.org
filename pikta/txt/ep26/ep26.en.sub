\begin{subdocument}{swpiktxt26}{Top Software Probability Batch nr 26: 2501-2600}{http://swpat.ffii.org/patents/txt/ep26/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{longtable}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
\endhead
ep0408361\footnote{/patents/txt/ep/0408/361} & Electronic equipment operable by a plurality of control programs. & CANON KK (JP) & 1989-07-13\\\hline
ep0600939\footnote{/patents/txt/ep/0600/939} & Method and apparatus for automated tissue assay & BIO TEK INSTR (US) & 1991-08-05\\\hline
ep0798654\footnote{/patents/txt/ep/0798/654} & Converting handle-based find first/find next/find close to non-handle based find first/find next & SUN MICROSYSTEMS INC (US) & 1996-03-29\\\hline
ep0190216\footnote{/patents/txt/ep/0190/216} & Computer system fault recovery based on historical analysis & AT \& T BELL LAB (US) & 1984-07-26\\\hline
ep0805395\footnote{/patents/txt/ep/0805/395} & Method for caching network and CD-ROM file accesses using a local hard disk & SUN MICROSYSTEMS INC (US) & 1996-05-01\\\hline
ep0245027\footnote{/patents/txt/ep/0245/027} & Data compression & GEN ELECTRIC CO PLC (GB) & 1986-05-08\\\hline
ep0582373\footnote{/patents/txt/ep/0582/373} & Method and apparatus for implementing self-organization in a wireless local area network. & SUN MICROSYSTEMS INC (US) & 1992-07-17\\\hline
ep0665974\footnote{/patents/txt/ep/0665/974} & Virtual retinal display & UNIV WASHINGTON (US) & 1992-10-22\\\hline
ep0380849\footnote{/patents/txt/ep/0380/849} & Preprocessing implied specifiers in a pipelined processor. & DIGITAL EQUIPMENT CORP (US) & 1989-02-03\\\hline
ep0474025\footnote{/patents/txt/ep/0474/025} & Automatic A/D converter operation using programmable sample time. & MOTOROLA INC (US) & 1990-09-04\\\hline
ep0272070\footnote{/patents/txt/ep/0272/070} & Input apparatus for computer. & OMRON TATEISI ELECTRONICS CO (JP) & 1986-12-17\\\hline
ep0800668\footnote{/patents/txt/ep/0800/668} & Expansion module address method and apparatus for a programmable logic controller & SIEMENS ENERGY \& AUTOMAT (US) & 1994-12-29\\\hline
ep0533905\footnote{/patents/txt/ep/0533/905} & Bus arbitrator circuit & BULL SA (FR) & 1992-04-14\\\hline
ep0574950\footnote{/patents/txt/ep/0574/950} & Image communication apparatus having a communication error check function. & CANON KK (JP) & 1992-06-19\\\hline
ep0637808\footnote{/patents/txt/ep/0637/808} & Buttonless memory system for an electronic measurement device. & MILES INC (US) & 1993-07-27\\\hline
ep0469476\footnote{/patents/txt/ep/0469/476} & Information transmission system with speech, image and text data communication between subscriber units. & GRUNDIG EMV (DE) & 1990-07-30\\\hline
ep0423381\footnote{/patents/txt/ep/0423/381} & Apparatus for transmitting audio and/or digital information. & FALTER \& FUNK GMBH CO (DE) & 1989-10-16\\\hline
ep0823272\footnote{/patents/txt/ep/0823/272} & Apparatus for and method of determining win for competitive video game, and recording medium storing win determining program & KONAMI CO LTD (JP) & 1996-08-06\\\hline
ep0386835\footnote{/patents/txt/ep/0386/835} & Method of processing a radio data signal, and receiver for performing said method. & PHILIPS NV (NL) & 1989-03-08\\\hline
ep0178608\footnote{/patents/txt/ep/0178/608} & Subband encoding method and apparatus. & GEN ELECTRIC (US) & 1984-10-17\\\hline
ep0664907\footnote{/patents/txt/ep/0664/907} & Disk array controller having command descriptor blocks utilized by bus master and bus slave for respectively performing data transfer operations & COMPAQ COMPUTER CORP (US) & 1992-10-13\\\hline
ep0702313\footnote{/patents/txt/ep/0702/313} & A combined totalizator and fixed odds betting system and method & TOTALIZATOR AGENCY BOARD (AU) & 1994-09-13\\\hline
ep0188928\footnote{/patents/txt/ep/0188/928} & System using one or more remotely controlled boats for conducting operations at sea. & INST FRANCAIS DU PETROL (FR) & 1984-12-06\\\hline
ep0366702\footnote{/patents/txt/ep/0366/702} & INTEGRATED CIRCUIT INTEGRITY TESTING APPARATUS & CATT IVOR (GB) & 1988-01-07\\\hline
ep0819281\footnote{/patents/txt/ep/0819/281} & Electronic competition system and method for using same &  & 1996-03-28\\\hline
ep0477862\footnote{/patents/txt/ep/0477/862} & Spread spectrum communications system. & PITTWAY CORP (US) & 1990-09-27\\\hline
ep0457990\footnote{/patents/txt/ep/0457/990} & Interactive data processing apparatus using a pen. & SONY CORP (JP) & 1989-12-05\\\hline
ep0218751\footnote{/patents/txt/ep/0218/751} & Stereoscopic remote viewing system. & SCHOOLMAN ARNOLD & 1985-10-17\\\hline
ep0446589\footnote{/patents/txt/ep/0446/589} & ATM EXCHANGE WITH COPYING CAPABILITY & ALCATEL NV (NL) & 1990-03-14\\\hline
ep0448232\footnote{/patents/txt/ep/0448/232} & Burst time division multiplex interface for integrated data link controller. & IBM (US) & 1990-03-15\\\hline
ep0408070\footnote{/patents/txt/ep/0408/070} & Method for allocating real pages to virtual pages having different page sizes therefrom. & HITACHI LTD (JP) & 1989-07-14\\\hline
ep0513511\footnote{/patents/txt/ep/0513/511} & Method for checking the admissibility of setting up virtual connections & SIEMENS AG (DE) & 1991-04-09\\\hline
ep0483397\footnote{/patents/txt/ep/0483/397} & Method for monitoring a bit rate of at least one virtual connection & SIEMENS AG (DE) & 1990-10-29\\\hline
ep0848886\footnote{/patents/txt/ep/0848/886} & Determining the position of a television camera for use in a virtual studio employing chroma keying & ORAD HI TEC SYSTEMS LTD (IL) & 1995-09-08\\\hline
ep0496288\footnote{/patents/txt/ep/0496/288} & Variable page size per entry translation look-aside buffer. & MIPS COMPUTER SYSTEMS INC (US) & 1991-01-23\\\hline
ep0348543\footnote{/patents/txt/ep/0348/543} & Method and device for announcing animal birth. & BAUMER RICHARD & 1988-06-30\\\hline
ep0572053\footnote{/patents/txt/ep/0572/053} & Contact reactor for a quasi-isothermal catalytic oxidation of SO2 to SO3 and method of operating same & METALLGESELLSCHAFT AG (DE); BASF AG (DE) & 1992-05-26\\\hline
ep0248138\footnote{/patents/txt/ep/0248/138} & External dynamic bone fixation device. & JAQUET ORTHOPEDIE (CH) & 1986-05-28\\\hline
ep0225299\footnote{/patents/txt/ep/0225/299} & Improved bar for plane lattice spatial structures without junction knots. & BURATTI MARIA MADDALENA (IT); BONGIORNI ANNABELLA (IT); MORI LAMBERTO (IT); SPINELLI ALBERTO (IT) & 1985-12-05\\\hline
ep0454275\footnote{/patents/txt/ep/0454/275} & Color separation method. & AGFA CORP (US) & 1990-04-26\\\hline
ep0449231\footnote{/patents/txt/ep/0449/231} & Process and apparatus for measuring the biocurrent distribution. & HITACHI LTD (JP) & 1990-04-27\\\hline
ep0546776\footnote{/patents/txt/ep/0546/776} & Method of determining kinematic parameters for robot wrist having an offset. & RES INST IND SCIENCE \& TECH (KR); PO HANG IRON \& STEEL (KR) & 1991-12-07\\\hline
ep0596480\footnote{/patents/txt/ep/0596/480} & Locking cylinder and method of producing this locking cylinder. & WINKHAUS FA AUGUST (DE) & 1992-11-06\\\hline
ep0585774\footnote{/patents/txt/ep/0585/774} & Driver's cab suspension. & DEERE \& CO (US) & 1993-09-02\\\hline
ep0683334\footnote{/patents/txt/ep/0683/334} & Pretensioning force adjustment of continuously variable transmission & TELEFUNKEN MICROELECTRON (DE) & 1994-05-18\\\hline
ep0678403\footnote{/patents/txt/ep/0678/403} & Pneumatic tyre & SUMITOMO RUBBER IND (JP) & 1994-04-13\\\hline
ep0661183\footnote{/patents/txt/ep/0661/183} & A suspension for a vehicle steered wheel adopting a multiple-rod arrangement & FIAT AUTO SPA (IT) & 1993-12-29\\\hline
ep0653600\footnote{/patents/txt/ep/0653/600} & Method for determining the rotation speed of the aiming line with a strapped down seeker head. & MAFO SYSTEMTECH GMBH \& CO KG (DE) & 1993-11-16\\\hline
ep0648785\footnote{/patents/txt/ep/0648/785} & Preparation of carboxyl-containing copolymers of ethylene & BASF AG (DE) & 1993-10-13\\\hline
ep0636873\footnote{/patents/txt/ep/0636/873} & Device for collecting dust. & FRAUNHOFER GES FORSCHUNG (DE) & 1993-07-30\\\hline
ep0503947\footnote{/patents/txt/ep/0503/947} & An electric connector. & YAZAKI CORP (JP) & 1991-03-14\\\hline
ep0241154\footnote{/patents/txt/ep/0241/154} & Method for soldering arrayed terminals and an automatic soldering device & APOLLO SEIKO LTD (JP) & 1986-07-18\\\hline
ep0616318\footnote{/patents/txt/ep/0616/318} & Magnetoresistive head. & FUJITSU LTD (JP) & 1993-12-02\\\hline
ep0574832\footnote{/patents/txt/ep/0574/832} & Gripper for containers, especially for bottles. & KHS VERPACKUNGSTECHNIK GMBH (DE) & 1992-06-19\\\hline
ep0548646\footnote{/patents/txt/ep/0548/646} & Method and apparatus for distinctively displaying windows on a computer display screen. & IBM (US) & 1991-12-20\\\hline
ep0313967\footnote{/patents/txt/ep/0313/967} & Method for testing the authenticity of a data carrier having an integrated circuit & GAO GES AUTOMATION ORG (DE) & 1988-10-18\\\hline
ep0108261\footnote{/patents/txt/ep/0108/261} & Logic analyzer. & ROHDE \& SCHWARZ (DE) & 1982-11-05\\\hline
ep0348031\footnote{/patents/txt/ep/0348/031} & Display system. & IBM (US) & 1988-06-10\\\hline
ep0487266\footnote{/patents/txt/ep/0487/266} & Background picture display apparatus and external storage unit used therefor. & NINTENDO CO LTD (JP); RICOH KK (JP) & 1990-11-19\\\hline
ep0827608\footnote{/patents/txt/ep/0827/608} & Process for the computer-assisted measurement and testing of electric circuits, especially electronic modules, and testing station for implementing the process & SIEMENS AG (DE) & 1996-05-23\\\hline
ep0559488\footnote{/patents/txt/ep/0559/488} & Handling data in a system having a processor for controlling access to a plurality of data storage disks. & DATA GENERAL CORP (US) & 1992-03-10\\\hline
ep0559487\footnote{/patents/txt/ep/0559/487} & Handling data in a system having a processor for controlling access to a plurality of data storage disks. & DATA GENERAL CORP (US) & 1992-03-06\\\hline
ep0536374\footnote{/patents/txt/ep/0536/374} & METHOD AND APPARATUS FOR PERFORMING ON-LINE INTEGRATED DECODING AND EVALUATION OF BAR CODE DATA & EASTMAN KODAK CO (US) & 1991-04-25\\\hline
ep0628207\footnote{/patents/txt/ep/0628/207} & Data recording system having longitudinal tracks with recordable segments & AMPEX SYSTEMS CORP (US) & 1992-02-28\\\hline
ep0654910\footnote{/patents/txt/ep/0654/910} & Method for detecting information bits processed by concatenated block codes & FRANCE TELECOM (FR) & 1993-11-19\\\hline
ep0655688\footnote{/patents/txt/ep/0655/688} & Program memory expansion using a special-function register & PHILIPS CORP (US) & 1993-11-29\\\hline
ep0454143\footnote{/patents/txt/ep/0454/143} & Microwave oven with a microcomputer operated according to cooking programs stored in a memory. & SHARP KK (JP) & 1990-04-26\\\hline
ep0141232\footnote{/patents/txt/ep/0141/232} & Vector processing unit. & IBM (US) & 1983-10-24\\\hline
ep0631396\footnote{/patents/txt/ep/0631/396} & Real-time convolutional decoder with block synchronising function. & OKI ELECTRIC IND CO LTD (JP) & 1993-06-25\\\hline
ep0357309\footnote{/patents/txt/ep/0357/309} & Personnel monitoring system. & BI INC (US) & 1988-08-29\\\hline
ep0374907\footnote{/patents/txt/ep/0374/907} & Method for recording and/or reproducing a signal. & SONY CORP (JP) & 1988-12-21\\\hline
ep0581087\footnote{/patents/txt/ep/0581/087} & Method for representing subscriber's individual data when transmitting signalling signals and information signals between narrow band networks and ATM-networks & SIEMENS AG (DE) & 1992-07-31\\\hline
ep0436345\footnote{/patents/txt/ep/0436/345} & Voice and image communicating apparatus. & CANON KK (JP) & 1989-12-20\\\hline
ep0227852\footnote{/patents/txt/ep/0227/852} & Local area communication system for integrated services based on a token-ring transmission medium & IBM (US) & 1985-12-23\\\hline
ep0326559\footnote{/patents/txt/ep/0326/559} & \#f &  & 1986-09-12\\\hline
ep0300350\footnote{/patents/txt/ep/0300/350} & Communication system. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1987-11-10\\\hline
ep0416235\footnote{/patents/txt/ep/0416/235} & Data-communication method for loop-type network having portable slave stations connectable to addressable junction boxes permanently connected in the network. & MITSUBISHI ELECTRIC CORP (JP) & 1989-09-05\\\hline
ep0601766\footnote{/patents/txt/ep/0601/766} & Audiovisual communication system using variable length packets. & SHARP KK (JP) & 1992-11-30\\\hline
ep0695088\footnote{/patents/txt/ep/0695/088} & Video storage type communication device & SHARP KK (JP); NIPPON TELEGRAPH \& TELEPHONE (JP) & 1995-04-07\\\hline
ep0192243\footnote{/patents/txt/ep/0192/243} & Secure data processing system architecture with format control & HONEYWELL INC (US) & 1985-02-21\\\hline
ep0179979\footnote{/patents/txt/ep/0179/979} & Synchoronous packet voice/data communication system & STRATACOM INC (US) & 1984-10-29\\\hline
ep0226734\footnote{/patents/txt/ep/0226/734} & Communication for version management in a distributed information service & IBM (US) & 1985-11-26\\\hline
ep0222133\footnote{/patents/txt/ep/0222/133} & Digital data separator & HONEYWELL INF SYSTEMS (IT) & 1985-10-10\\\hline
ep0417587\footnote{/patents/txt/ep/0417/587} & SYSTEM FOR CHECKING UNDEFINED ADDRESSING PRESCRIBED FOR EACH INSTRUCTION OF VARIABLE LENGTH USING TAG INFORMATION TO DETERMINE ADDRESSING FIELD DECODED IN PRESENT OR PRECEDING CYCLE & FUJITSU LTD (JP) & 1989-09-11\\\hline
ep0573541\footnote{/patents/txt/ep/0573/541} & System for broadcasting and receiving digital data, receiver and transmitter for use in such system & PHILIPS CORP (US) & 1992-02-27\\\hline
ep0431699\footnote{/patents/txt/ep/0431/699} & METHOD OF AND DEVICE FOR DECODING ANIMATED IMAGES & PHILIPS CORP (US) & 1989-12-08\\\hline
ep0195421\footnote{/patents/txt/ep/0195/421} & Method and arrangement for the synchronisation of digital information signals. & TELEFUNKEN FERNSEH \& RUNDFUNK (DE) & 1985-03-22\\\hline
ep0166741\footnote{/patents/txt/ep/0166/741} & Technique for insertion of digital data bursts into an adaptively encoded information bit stream & AT \& T BELL LAB (US) & 1983-12-27\\\hline
ep0621975\footnote{/patents/txt/ep/0621/975} & Format for recording digital audio onto magnetic tape with enhanced editing and error correction capability & ALESIS (US) & 1992-01-17\\\hline
ep0602817\footnote{/patents/txt/ep/0602/817} & Digital video signal processing apparatus. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1992-12-18\\\hline
ep0624291\footnote{/patents/txt/ep/0624/291} & Error protection system for a sub-band coder suitable for use in an audio signal processor & THOMSON BRANDT GMBH (DE) & 1992-01-31\\\hline
ep0398741\footnote{/patents/txt/ep/0398/741} & Image information transmitting system & CANON KK (JP) & 1989-05-19\\\hline
ep0467461\footnote{/patents/txt/ep/0467/461} & Image display. & PHILIPS ELECTRONICS UK LTD (GB); PHILIPS NV (NL) & 1990-07-20\\\hline
ep0511467\footnote{/patents/txt/ep/0511/467} & Apparatus and method of operation for a facsimile subsystem in an image archiving system. & IBM (US) & 1991-04-30\\\hline
ep0186594\footnote{/patents/txt/ep/0186/594} & Information signal reproducing apparatus & SONY CORP (JP) & 1984-12-28\\\hline
ep0441168\footnote{/patents/txt/ep/0441/168} & System, packet structuring and device for processing output information from a signal encoder. & TELETTRA LAB TELEFON (IT) & 1990-03-23\\\hline
ep0492455\footnote{/patents/txt/ep/0492/455} & An information reading device. & NIPPON DENSO CO (JP) & 1990-12-20\\\hline
ep0280020\footnote{/patents/txt/ep/0280/020} & Operator access to monitoring applications. & IBM (US) & 1987-01-23\\\hline
ep0242142\footnote{/patents/txt/ep/0242/142} & System for adjusting signal transmission timing to prevent signal collisions & TOKYO SHIBAURA ELECTRIC CO (JP) & 1986-04-14\\\hline
ep0488823\footnote{/patents/txt/ep/0488/823} & Broadcasting of packets in an RF system. & MOTOROLA INC (US) & 1990-11-30\\\hline
\end{longtable}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: latex ;
% End: ;

