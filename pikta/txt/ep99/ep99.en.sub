\begin{subdocument}{swpiktxt99}{Top Software Probability Batch nr 99: 9801-9900}{http://swpat.ffii.org/patents/txt/ep99/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0344213\footnote{/patents/txt/ep/0344/213} & Viterbi decoder with reduced number of data move operations & RACAL DATA COMMUNICATIONS INC (US) & 1987-01-28\\\hline
ep0354813\footnote{/patents/txt/ep/0354/813} & Seismic data processing. & WESTERN ATLAS INT INC (US) & 1988-08-11\\\hline
ep0358270\footnote{/patents/txt/ep/0358/270} & Method and electrical system for recording and processing time-related data. & IND CONTROL SYSTEMS BV (NL) & 1988-09-06\\\hline
ep0356453\footnote{/patents/txt/ep/0356/453} & Interpolator for compressed video data & TECHNOLOGY INC 64 (US) & 1987-04-13\\\hline
ep0364099\footnote{/patents/txt/ep/0364/099} & Data storage and retrieval in a calculator. & SHARP KK (JP) & 1988-09-12\\\hline
ep0367626\footnote{/patents/txt/ep/0367/626} & Data entry and control arrangement, E.G.,for an appliance. & GEN ELECTRIC (US) & 1988-11-04\\\hline
ep0387644\footnote{/patents/txt/ep/0387/644} & Multiprocessor system with global data replication and two levels of address translation units. & BULL HN INFORMATION SYST (IT) & 1989-03-15\\\hline
ep0387013\footnote{/patents/txt/ep/0387/013} & Data encoding. & CROSFIELD ELECTRONICS LTD (GB) & 1989-03-10\\\hline
ep0399744\footnote{/patents/txt/ep/0399/744} & Method and apparatus for maintaining referential integrity within a relational database in a data processing system. & IBM (US) & 1989-05-24\\\hline
ep0405760\footnote{/patents/txt/ep/0405/760} & System for synchronizing data frame groups in a serial bit stream. & SGS THOMSON MICROELECTRONICS (US) & 1989-06-30\\\hline
ep0429306\footnote{/patents/txt/ep/0429/306} & Data compaction system. & IBM (US) & 1989-11-22\\\hline
ep0427322\footnote{/patents/txt/ep/0427/322} & A method of and means for processing image data & OCE NEDERLAND BV (NL) & 1989-11-06\\\hline
ep0425496\footnote{/patents/txt/ep/0425/496} & METHOD AND APPARATUS FOR STATISTICALLY ENCODING DIGITAL DATA & INTEL CORP (US) & 1988-03-31\\\hline
ep0432806\footnote{/patents/txt/ep/0432/806} & Method of and apparatus for compressing image data. & DAINIPPON SCREEN MFG (JP) & 1989-12-15\\\hline
ep0438152\footnote{/patents/txt/ep/0438/152} & Data processing apparatus with display device. & CANON KK (JP) & 1990-01-19\\\hline
ep0441980\footnote{/patents/txt/ep/0441/980} & TIME-DIVISION MULTIPLEX DATA RELAY EXCHANGE SYSTEM. & FUJITSU LTD (JP) & 1989-12-14\\\hline
ep0449716\footnote{/patents/txt/ep/0449/716} & Device for transforming a digital data sequence into a condensed digital data block using tables and logic operators & FRANCE TELECOM (FR) & 1990-03-26\\\hline
ep0458720\footnote{/patents/txt/ep/0458/720} & Method for conditional deletion of data objects within a data processing system & IBM (US) & 1990-05-24\\\hline
ep0458719\footnote{/patents/txt/ep/0458/719} & Method for automatic deletion of temporary document relationships within a data processing system & IBM (US) & 1990-05-24\\\hline
ep0455267\footnote{/patents/txt/ep/0455/267} & Data demodulation system. & SHARP KK (JP) & 1986-01-31\\\hline
ep0459065\footnote{/patents/txt/ep/0459/065} & TELEPHONE INSTALLATION FOR THE REMOTE LOADING OF TELEPHONE RENTAL DATA OF AN INDEPENDENT STATION & FRANCE TELECOM (FR) & 1990-05-29\\\hline
ep0470851\footnote{/patents/txt/ep/0470/851} & Image data processing method and apparatus & FUJITSU LTD (JP) & 1990-08-10\\\hline
ep0501475\footnote{/patents/txt/ep/0501/475} & A keyring methaphor for users' security keys on a distributed multiprocess data system. & BULL HN INFORMATION SYST (US) & 1991-03-01\\\hline
ep0513989\footnote{/patents/txt/ep/0513/989} & Raster imaging device speed-resolution product multiplying method and resulting pixel image data structure. & HEWLETT PACKARD CO (US) & 1991-04-17\\\hline
ep0519733\footnote{/patents/txt/ep/0519/733} & Data transmitter and receiver and the data control method thereof. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1991-06-21\\\hline
ep0517483\footnote{/patents/txt/ep/0517/483} & Fault tolerant RLL data sector address mark decoder. & QUANTUM CORP (US) & 1991-06-04\\\hline
ep0534139\footnote{/patents/txt/ep/0534/139} & A new video mixing technique using JPEG compressed data. & IBM (US) & 1991-09-20\\\hline
ep0536964\footnote{/patents/txt/ep/0536/964} & Active matrix-type display device having a reduced number of data bus lines. & FUJITSU LTD (JP) & 1991-10-05\\\hline
ep0540147\footnote{/patents/txt/ep/0540/147} & Multi-status multi-function data processing key and key array. & LIN WALLACE E (US); LIN EDWARD D (US) & 1991-09-03\\\hline
ep0549218\footnote{/patents/txt/ep/0549/218} & A memory apparatus and method for use in a data processing system & MOTOROLA INC (US) & 1991-12-23\\\hline
ep0555832\footnote{/patents/txt/ep/0555/832} & Data modulating method and apparatus and data demodulating method and apparatus. & SONY CORP (JP) & 1992-02-14\\\hline
ep0555282\footnote{/patents/txt/ep/0555/282} & DATA INPUT DEVICE HAVING MORE THAN TWO DEGREES OF FREEDOM & QUEEN MARY \& WESTFIELD COLLEGE (GB) & 1991-01-31\\\hline
ep0565469\footnote{/patents/txt/ep/0565/469} & Contact-free data exchange between a terminal and modular portable set having two different protocols for exchange which is selected based on portable set type & INNOVATRON IND SA (FR) & 1992-04-08\\\hline
ep0579922\footnote{/patents/txt/ep/0579/922} & Data transmission device for a local area network. & LANDIS \& GYR BUSINESS SUPPORT (CH) & 1992-07-14\\\hline
ep0577202\footnote{/patents/txt/ep/0577/202} & Data transmission method & PHILIPS CORP (US) & 1992-07-02\\\hline
ep0584657\footnote{/patents/txt/ep/0584/657} & Method and apparatus to protect data within portable read/write media. & SIEMENS AG (DE) & 1992-08-24\\\hline
ep0590974\footnote{/patents/txt/ep/0590/974} & Coded data editing apparatus and edited coded data decoding apparatus. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1992-09-30\\\hline
ep0602943\footnote{/patents/txt/ep/0602/943} & Apparatus for reproducing multiplexed data from a record medium. & SONY CORP (JP) & 1992-12-18\\\hline
ep0607484\footnote{/patents/txt/ep/0607/484} & Method and apparatus for scanning image data. & SAMSUNG ELECTRONICS CO LTD (KR) & 1993-02-01\\\hline
ep0605347\footnote{/patents/txt/ep/0605/347} & Method and apparatus for manipulating a full motion video presentation in a data processing system & IBM (US) & 1992-12-31\\\hline
ep0616713\footnote{/patents/txt/ep/0616/713} & Method and system for creating and maintaining multiple document versions in a data processing system library & IBM (US) & 1991-12-09\\\hline
ep0615236\footnote{/patents/txt/ep/0615/236} & Data recording method. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1993-03-08\\\hline
ep0637796\footnote{/patents/txt/ep/0637/796} & Data transmitting and receiving method and apparatus therefor. & SONY CORP (JP) & 1993-08-03\\\hline
ep0654733\footnote{/patents/txt/ep/0654/733} & Parallel data processing in a single processor. & HEWLETT PACKARD CO (US) & 1993-11-23\\\hline
ep0678818\footnote{/patents/txt/ep/0678/818} & System for updating the display of resource assignments in a data processing system. & MINNESOTA MINING \& MFG (US) & 1994-04-22\\\hline
ep0675484\footnote{/patents/txt/ep/0675/484} & Magnetic data storage system. & IBM (US) & 1994-04-01\\\hline
ep0728337\footnote{/patents/txt/ep/0728/337} & PARALLEL DATA PROCESSOR & LOCKHEED CORP (US) & 1994-09-13\\\hline
ep0760130\footnote{/patents/txt/ep/0760/130} & DATA MANAGEMENT SYSTEM FOR A REAL-TIME SYSTEM & SIEMENS AG (DE); KRUSCHE STEFAN (DE); LUKAS DIRK (DE); LANG STEFAN (DE); LANTERMANN JUERGEN (DE) & 1994-05-10\\\hline
ep0835590\footnote{/patents/txt/ep/0835/590} & System for processing data in variable segments and with variable data resolution & THOMSON MULTIMEDIA SA (FR) & 1995-06-29\\\hline
ep0240209\footnote{/patents/txt/ep/0240/209} & Method and apparatus for encoding component digital video signals so as to compress the bandwidth thereof, and for decoding the same & SONY CORP (JP) & 1986-04-02\\\hline
ep0288281\footnote{/patents/txt/ep/0288/281} & ADPCM encoding and decoding systems. & OKI ELECTRIC IND CO LTD (JP) & 1987-04-21\\\hline
ep0253326\footnote{/patents/txt/ep/0253/326} & Method and apparatus for encoding and decoding dither signals for reproduction of the halftone of a picture & HITACHI LTD (JP) & 1986-07-10\\\hline
ep0380847\footnote{/patents/txt/ep/0380/847} & Decoding multiple specifiers in a variable length instruction architecture. & DIGITAL EQUIPMENT CORP (US) & 1989-02-03\\\hline
ep0598995\footnote{/patents/txt/ep/0598/995} & Image encoding method and image encoding/decoding method. & MITSUBISHI ELECTRIC CORP (JP) & 1992-11-26\\\hline
ep0746160\footnote{/patents/txt/ep/0746/160} & Video signal decoding method & SONY CORP (JP) & 1989-09-27\\\hline
ep0274647\footnote{/patents/txt/ep/0274/647} & Method and arrangement for transmitting a digital signal with a low bit rate in a time section, provided for higher bit rates, of a time division multiplexed signal & SIEMENS AG (DE) & 1986-12-19\\\hline
ep0303450\footnote{/patents/txt/ep/0303/450} & Digital signal transmission apparatus. & CANON KK (JP) & 1987-12-18\\\hline
ep0398737\footnote{/patents/txt/ep/0398/737} & Digital signal reproducing apparatus. & SONY CORP (JP) & 1989-05-19\\\hline
ep0392506\footnote{/patents/txt/ep/0392/506} & Digital modulation method. & JAPAN BROADCASTING CORP (JP) & 1989-04-12\\\hline
ep0372008\footnote{/patents/txt/ep/0372/008} & Digital speech coder having improved vector excitation source & MOTOROLA INC (US) & 1988-01-07\\\hline
ep0351055\footnote{/patents/txt/ep/0351/055} & Digital modulator and demodulator. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1988-07-08\\\hline
ep0506446\footnote{/patents/txt/ep/0506/446} & Digital modulating method and digital modulating apparatus. & SHARP KK (JP) & 1991-03-29\\\hline
ep0493501\footnote{/patents/txt/ep/0493/501} & Digital phase-locked loop biphase demodulating method and apparatus & SCHLUMBERGER TECHNOLOGY CORP (US) & 1989-09-19\\\hline
ep0471118\footnote{/patents/txt/ep/0471/118} & A video signal digital recording and reproducing apparatus. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1990-08-13\\\hline
ep0469395\footnote{/patents/txt/ep/0469/395} & Digital low-power programmable alarm clock for use with reflectance photometer instruments and the like. & MILES INC (US) & 1990-07-31\\\hline
ep0553841\footnote{/patents/txt/ep/0553/841} & Method and apparatus for digital signal transmission using orthogonal frequency division multiplexing. & JAPAN BROADCASTING CORP (JP) & 1992-01-31\\\hline
ep0629089\footnote{/patents/txt/ep/0629/089} & Device for transmitting or storing digital television pictures, and device for receiving said pictures. & PHILIPS ELECTRONICS NV (NL) & 1993-06-07\\\hline
ep0613574\footnote{/patents/txt/ep/0613/574} & Digital adder having a high-speed low-capacitance carry bypass signal path & UNISYS CORP (US) & 1991-11-21\\\hline
ep0609206\footnote{/patents/txt/ep/0609/206} & Borehole digital geophone tool & MOBIL OIL CORP (US) & 1991-10-23\\\hline
ep0637172\footnote{/patents/txt/ep/0637/172} & Apparatus for scrambling a digital video signal. & SONY CORP (JP) & 1993-07-30\\\hline
ep0339809\footnote{/patents/txt/ep/0339/809} & An asynchronous time division network. & PLESSEY TELECOMM (GB) & 1988-04-28\\\hline
ep0474616\footnote{/patents/txt/ep/0474/616} & Apparatus and method for variable ratio frequency division & ERICSSON GE MOBILE COMMUNICAT (US) & 1990-09-06\\\hline
ep0185905\footnote{/patents/txt/ep/0185/905} & Document creation & IBM (US) & 1984-12-26\\\hline
ep0185904\footnote{/patents/txt/ep/0185/904} & Tailored document building. & IBM (US) & 1984-12-26\\\hline
ep0632415\footnote{/patents/txt/ep/0632/415} & Document transaction apparatus. & AT \& T GLOBAL INF SOLUTION (US) & 1993-07-01\\\hline
ep0632402\footnote{/patents/txt/ep/0632/402} & Method for image segmentation and classification of image elements for document processing. & IBM (US) & 1993-06-30\\\hline
ep0679539\footnote{/patents/txt/ep/0679/539} & Scanning documents in a method and in systems for assembling postal items & HADEWE BV (NL) & 1992-02-18\\\hline
ep0633553\footnote{/patents/txt/ep/0633/553} & APPARATUS FOR RECORDING SYMBOLS PRINTED ON DOCUMENTS OR THE LIKE. & NIPPON KINSEN KIKAI KK (JP) & 1993-12-24\\\hline
ep0166592\footnote{/patents/txt/ep/0166/592} & ENCODING METHOD & KING REGINALD A (GB) & 1984-06-28\\\hline
ep0621724\footnote{/patents/txt/ep/0621/724} & Method and apparatus for encoding progressive build-up image date. & CANON KK (JP) & 1993-04-21\\\hline
ep0597733\footnote{/patents/txt/ep/0597/733} & Image encoding device. & CANON KK (JP) & 1992-11-13\\\hline
ep0383367\footnote{/patents/txt/ep/0383/367} & Graphic pattern processing apparatus. & HITACHI LTD (JP); HITACHI ENG CO LTD (JP) & 1983-12-26\\\hline
ep0239940\footnote{/patents/txt/ep/0239/940} & Special image effect producing apparatus with memory selection & TOKYO SHIBAURA ELECTRIC CO (JP) & 1986-03-31\\\hline
ep0235456\footnote{/patents/txt/ep/0235/456} & Image processing apparatus. & CANON KK (JP) & 1985-12-14\\\hline
ep0370679\footnote{/patents/txt/ep/0370/679} & Image forming apparatus. & MITA INDUSTRIAL CO LTD (JP) & 1988-11-19\\\hline
ep0447937\footnote{/patents/txt/ep/0447/937} & Image memory. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1990-03-13\\\hline
ep0474444\footnote{/patents/txt/ep/0474/444} & Image processing method and apparatus. & CANON KK (JP) & 1990-11-05\\\hline
ep0486185\footnote{/patents/txt/ep/0486/185} & Image signal recording apparatus and methods. & SONY CORP (JP) & 1990-11-16\\\hline
ep0491556\footnote{/patents/txt/ep/0491/556} & Image processing method and apparatus & CANON KK (JP) & 1990-12-19\\\hline
ep0493130\footnote{/patents/txt/ep/0493/130} & Image processing apparatus. & CANON KK (JP) & 1990-12-28\\\hline
ep0584758\footnote{/patents/txt/ep/0584/758} & Image display device. & CASIO COMPUTER CO LTD (JP) & 1992-12-28\\\hline
ep0591974\footnote{/patents/txt/ep/0591/974} & Image processing apparatus. & SHARP KK (JP) & 1992-11-27\\\hline
ep0597698\footnote{/patents/txt/ep/0597/698} & Image processing apparatus. & CANON KK (JP) & 1992-11-13\\\hline
ep0665512\footnote{/patents/txt/ep/0665/512} & An image processing method and apparatus. & CANON KK (JP) & 1994-02-01\\\hline
ep0763925\footnote{/patents/txt/ep/0763/925} & Image processing apparatus & CANON KK (JP) & 1991-04-17\\\hline
ep0926883\footnote{/patents/txt/ep/0926/883} & Image processing method and apparatus. & CANON KK (JP) & 1990-11-05\\\hline
ep0194902\footnote{/patents/txt/ep/0194/902} & Process and method for wide band transmission, particularly for data transmission over an electricity distribution network & ENERTEC (FR) & 1985-02-14\\\hline
ep0169597\footnote{/patents/txt/ep/0169/597} & Apparatus for automatically reproducing preferred selection from a record carrier & PHILIPS CORP (US) & 1984-07-02\\\hline
ep0449353\footnote{/patents/txt/ep/0449/353} & Data processing device and method for selecting data words contained in a dictionary & PHILIPS CORP (US) & 1990-03-20\\\hline
ep0515342\footnote{/patents/txt/ep/0515/342} & Casing for at least one high density data disk & THULIN CARTONNERIES (BE) & 1991-09-06\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

