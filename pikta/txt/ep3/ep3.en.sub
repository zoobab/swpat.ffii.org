\begin{subdocument}{swpiktxt3}{Top Software Probability Batch nr 3: 201-300}{http://swpat.ffii.org/patents/txt/ep3/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{longtable}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
\endhead
ep0438476\footnote{/patents/txt/ep/0438/476} & Self-clocking encoding/decoding film information exchange system using dedicated magnetic tracks on film & EASTMAN KODAK CO (US) & 1988-10-07\\\hline
ep0243312\footnote{/patents/txt/ep/0243/312} & Method of interactive communication between a subscriber and a decoder of a system of pay-television and decoder thereof & KUDELSKI S A FABRIQUE D ENGREG (CH) & 1986-04-18\\\hline
ep0215440\footnote{/patents/txt/ep/0215/440} & Signalling information translation centre for a TDM transmission system. & SIEMENS AG (DE) & 1985-09-20\\\hline
ep0514477\footnote{/patents/txt/ep/0514/477} & INTERFACE CHIP DEVICE & GEN DYNAMICS LAND SYSTEMS INC (US) & 1990-02-07\\\hline
ep0354590\footnote{/patents/txt/ep/0354/590} & Instruction buffer for a microcomputer. & NIPPON ELECTRIC CO (JP) & 1988-08-12\\\hline
ep0237014\footnote{/patents/txt/ep/0237/014} & Method for implementing an on-line presentation in an information processing system & IBM (US) & 1986-03-10\\\hline
ep0880743\footnote{/patents/txt/ep/0880/743} & Emulation repair system & SYMANTEC CORP (US) & 1996-02-09\\\hline
ep0437615\footnote{/patents/txt/ep/0437/615} & HIERARCHICAL PRESEARCH-TYPE DOCUMENT RETRIEVAL METHOD, APPARATUS THEREFOR, AND MAGNETIC DISC DEVICE FOR THIS APPARATUS. & HITACHI LTD (JP) & 1990-06-14\\\hline
ep0135888\footnote{/patents/txt/ep/0135/888} & Process for preventing the execution of a programme in a computer which is not allowed to execute said programme & SIEMENS AG (DE) & 1983-09-29\\\hline
ep0712218\footnote{/patents/txt/ep/0712/218} & A method and an arrangement for sound reconstruction during erasures & ERICSSON BUSINESS MOBILE NETWO (NL) & 1994-11-10\\\hline
ep0417293\footnote{/patents/txt/ep/0417/293} & DATA MANAGEMENT SYSTEM. & SONY CORP (JP) & 1988-12-14\\\hline
ep0573406\footnote{/patents/txt/ep/0573/406} & PROGRAM TRANSMISSION OPTIMISATION & DELTA BETA PTY LTD (AU) & 1989-08-23\\\hline
ep0495085\footnote{/patents/txt/ep/0495/085} & ON-LINE PROCESS CONTROL NEURAL NETWORK USING DATA POINTERS & DU PONT (US) & 1990-08-03\\\hline
ep0531177\footnote{/patents/txt/ep/0531/177} & Method and apparatus for fault detection and repair of a data processing system. & BULL SA (FR) & 1991-08-14\\\hline
ep0824732\footnote{/patents/txt/ep/0824/732} & \#f & INFO TELECOM (FR) & 1995-04-28\\\hline
ep0827607\footnote{/patents/txt/ep/0827/607} & METHOD FOR MANAGING GLOBALLY DISTRIBUTED SOFTWARE COMPONENTS & NOVELL INC (US) & 1995-09-12\\\hline
ep0553285\footnote{/patents/txt/ep/0553/285} & OBJECT-ORIENTED ARCHITECTURE FOR FACTORY FLOOR MANAGEMENT & CONSILIUM INC (US) & 1990-10-16\\\hline
ep0217168\footnote{/patents/txt/ep/0217/168} & Method for processing address translation exceptions in a virtual memory system. & IBM (US) & 1985-10-01\\\hline
ep0441509\footnote{/patents/txt/ep/0441/509} & Single-operation focus shift between user applications in a virtual terminal environment. & IBM (US) & 1990-02-01\\\hline
ep0377431\footnote{/patents/txt/ep/0377/431} & Apparatus and method for address translation of non-aligned double word virtual addresses. & BULL HN INFORMATION SYST (US) & 1989-01-05\\\hline
ep0292791\footnote{/patents/txt/ep/0292/791} & Microprogrammed systems software instruction undo. & HONEYWELL BULL (US) & 1987-05-19\\\hline
ep0525632\footnote{/patents/txt/ep/0525/632} & Accounting system and method for ATM network and ATM network. & FUJITSU LTD (JP) & 1991-07-24\\\hline
ep0487235\footnote{/patents/txt/ep/0487/235} & Bandwidth and congestion management in accessing broadband ISDN networks. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1990-11-21\\\hline
ep0543991\footnote{/patents/txt/ep/0543/991} & IMPROVING COMPUTER PERFORMANCE BY SIMULATED CACHE ASSOCIATIVITY & DIGITAL EQUIPMENT CORP (US) & 1991-06-17\\\hline
ep0496927\footnote{/patents/txt/ep/0496/927} & Procedure for restarting the multiprocessor computer of a telecommunications switching system after a failure. & SIEMENS AG (DE) & 1991-02-01\\\hline
ep0094623\footnote{/patents/txt/ep/0094/623} & Circuit for telecommunication exchanges, especially telephone switching exchanges, with multiprocessor systems for call processing purposes. & SIEMENS AG (DE) & 1982-05-14\\\hline
ep0764299\footnote{/patents/txt/ep/0764/299} & A METHOD FOR HANDLING OBSCURED ITEMS ON COMPUTER DISPLAYS & MINNESOTA MINING \& MFG (US) & 1994-05-10\\\hline
ep0711485\footnote{/patents/txt/ep/0711/485} & METHOD AND APPARATUS FOR PROVIDING USER CONTROLLED CALL MANAGEMENT SERVICES & NORTHERN TELECOM LTD (CA) & 1993-06-11\\\hline
ep0416685\footnote{/patents/txt/ep/0416/685} & Method for monitoring, by means of a monitoring device, a downstream transmission medium containing a multiplicity of virtual, asynchronously time-shared transmission channels. & NEDERLAND PTT (NL) & 1989-09-05\\\hline
ep0294583\footnote{/patents/txt/ep/0294/583} & System and method for the generalized topological mapping of an information base. & KUECHLER WILLIAM L (US); KUECHLER DAVID W (US) & 1987-05-08\\\hline
ep0809402\footnote{/patents/txt/ep/0809/402} & External security module for a television signal decoder & SCIENTIFIC ATLANTA (US) & 1990-02-01\\\hline
ep0423453\footnote{/patents/txt/ep/0423/453} & Address translation and copying process. & IBM (US) & 1989-10-20\\\hline
ep0231574\footnote{/patents/txt/ep/0231/574} & Cup chip having tag comparator and address translation unit on chip and connected to off-chip cache and main memories & MIPS COMPUTER SYSTEMS INC (US) & 1986-02-06\\\hline
ep0539120\footnote{/patents/txt/ep/0539/120} & Source code analyzer. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1991-10-22\\\hline
ep0252229\footnote{/patents/txt/ep/0252/229} & Apl-to-fortran translator. & IBM (US) & 1986-07-07\\\hline
ep0436799\footnote{/patents/txt/ep/0436/799} & Communication network with key distribution. & ALCATEL STK AS (NO) & 1989-11-13\\\hline
ep0815508\footnote{/patents/txt/ep/0815/508} & Multi-sequential computer for real-time applications & SECR DEFENCE BRIT (GB) & 1996-03-19\\\hline
ep0262759\footnote{/patents/txt/ep/0262/759} & User interface simulation and management for program-controlled apparatus. & HEWLETT PACKARD CO (US) & 1986-09-01\\\hline
ep0910841\footnote{/patents/txt/ep/0910/841} & INFORMATION TRANSMISSION SYSTEM & WILHELM SIEGFRIED (DE); ROOST HOLGER (DE); TRESOR TV PRODUKTIONS GMBH (DE) & 1996-06-19\\\hline
ep0516682\footnote{/patents/txt/ep/0516/682} & Method and apparatus for controlling access to and corruption of information in computer systems & ARENDEE LTD & 1991-02-20\\\hline
ep0295837\footnote{/patents/txt/ep/0295/837} & Customer account online servicing system. & INVENTIONS INC (US) & 1987-06-16\\\hline
ep0267259\footnote{/patents/txt/ep/0267/259} & Security file system for a portable data carrier & AMERICAN TELEPHONE \& TELEGRAPH (US); AT \& T INFORMATION SYSTEMS INC (US) & 1986-05-16\\\hline
ep0270571\footnote{/patents/txt/ep/0270/571} & \#f &  & 1986-05-16\\\hline
ep0268615\footnote{/patents/txt/ep/0268/615} & Improved security system for a portable data carrier & AMERICAN TELEPHONE \& TELEGRAPH (US); AT \& T INFORMATION SYSTEMS INC (US) & 1986-05-16\\\hline
ep0511737\footnote{/patents/txt/ep/0511/737} & Method and apparatus for generating calibration information for an electronic engine control module. & CUMMINS ELECTRONICS (US) & 1991-03-29\\\hline
ep0668553\footnote{/patents/txt/ep/0668/553} & Method and apparatus for generating calibration information for an electronic engine control module. & CUMMINS ELECTRONICS (US) & 1991-03-29\\\hline
ep0753225\footnote{/patents/txt/ep/0753/225} & Apparatus and method for integrating downstream data transfer over a cable television channel with upstream data carrier by other media & AT \& T CORP (US) & 1994-03-31\\\hline
ep0437513\footnote{/patents/txt/ep/0437/513} & FRAME-BY-FRAME DATA RECORDING FILM INFORMATION EXCHANGE SYSTEM USING DEDICATED MAGNETIC TRACKS ON FILM & EASTMAN KODAK CO (US) & 1988-10-07\\\hline
ep0296190\footnote{/patents/txt/ep/0296/190} & \#f &  & 1986-12-08\\\hline
ep0397664\footnote{/patents/txt/ep/0397/664} & Personal identification encryptor system with error-correcting code and method & ATALLA CORP (US) & 1987-11-02\\\hline
ep0280549\footnote{/patents/txt/ep/0280/549} & Data compression method and apparatus. & OKI ELECTRIC IND CO LTD (JP) & 1987-02-25\\\hline
ep0584839\footnote{/patents/txt/ep/0584/839} & Method for the use at the reception end of additional information within a radio signal. & TELEFUNKEN FERNSEH \& RUNDFUNK (DE) & 1987-12-21\\\hline
ep0267930\footnote{/patents/txt/ep/0267/930} & Power line communication apparatus & ADAPTIVE NETWORKS INC (US) & 1986-04-16\\\hline
ep0253956\footnote{/patents/txt/ep/0253/956} & Addressing technique for providing read, modify and write operations in a single data processing cycle with serpentine configured RAMs & MOTOROLA INC (US) & 1986-06-26\\\hline
ep0099469\footnote{/patents/txt/ep/0099/469} & System for the representation of text, graphics and symbols on monitor screens and/or with matrix printers. & LOEWE OPTA GMBH (DE) & 1982-10-29\\\hline
ep0565472\footnote{/patents/txt/ep/0565/472} & Method and apparatus for sharing a telecommunications channel among multiple users. & IBM (US) & 1992-04-10\\\hline
ep0530443\footnote{/patents/txt/ep/0530/443} & Time division multiplex switching system for interconnecting telephone circuits which operate in accordance with different signalling systems and call formats. & REDCOM LAB INC (US) & 1991-08-23\\\hline
ep0615370\footnote{/patents/txt/ep/0615/370} & System for communicating with network having first operating system in charge of upper communication layers and second operating system in charge of lower communication layers & BULL SA (FR) & 1994-03-09\\\hline
ep0726004\footnote{/patents/txt/ep/0726/004} & System for activating new service in client server network by reconfiguring the multilayer network protocol stack dynamically within the server node & TALIGENI INC (US) & 1993-12-17\\\hline
ep0429518\footnote{/patents/txt/ep/0429/518} & \#f & IMS FRANCE (FR) & 1988-08-18\\\hline
ep0916136\footnote{/patents/txt/ep/0916/136} & GRAPHICAL USER INTERFACE FOR A MOTION VIDEO PLANNING AND EDITING SYSTEM FOR A COMPUTER & AVID TECHNOLOGY INC (US) & 1996-08-02\\\hline
ep0916225\footnote{/patents/txt/ep/0916/225} & GRAPHICAL USER INTERFACE FOR A MOTION VIDEO PLANNING AND EDITING SYSTEM FOR A COMPUTER & AVID TECHNOLOGY INC (US) & 1996-08-02\\\hline
ep0805399\footnote{/patents/txt/ep/0805/399} & Method for creating a single binary virtual device driver for a windowing operating system & SUN MICROSYSTEMS INC (US) & 1996-05-01\\\hline
ep0871136\footnote{/patents/txt/ep/0871/136} & Method for transmitting information using a matrix coding of data & CLAVIEZ HOMBERG PATRICE (FR); RICORDEL MARC OLIVIER (FR) & 1997-04-11\\\hline
ep0697640\footnote{/patents/txt/ep/0697/640} & Data arrangement for an apparatus connectable to a communication network, and process for generating the data arrangement & LANDIS \& GYR TECH INNOVAT (CH) & 1994-08-04\\\hline
ep0254115\footnote{/patents/txt/ep/0254/115} & Modular ISDN communication system with formation and display of error texts & SIEMENS AG (DE) & 1986-09-30\\\hline
ep0575144\footnote{/patents/txt/ep/0575/144} & A method of coupling open systems to a proprietary network. & HONEYWELL INC (US) & 1992-06-16\\\hline
ep0769748\footnote{/patents/txt/ep/0769/748} & Dedicated DDC integrable multimode communications cell & SGS THOMSON MICROELECTRONICS (FR) & 1995-10-19\\\hline
ep0736239\footnote{/patents/txt/ep/0736/239} & ATM NETWORKS FOR NARROW BAND COMMUNICATIONS & AT \& T CORP (US) & 1993-12-20\\\hline
ep0441508\footnote{/patents/txt/ep/0441/508} & Data storage using a cache. & IBM (US) & 1990-02-09\\\hline
ep0387247\footnote{/patents/txt/ep/0387/247} & Fast packetized data delivery for digital networks & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1987-11-24\\\hline
ep0422717\footnote{/patents/txt/ep/0422/717} & Method for monitoring a transmission system which comprises a plurality of virtual, asynchronously time-shared transmission channels via which a data flow can be transmitted. & NEDERLAND PTT (NL) & 1989-10-09\\\hline
ep0571167\footnote{/patents/txt/ep/0571/167} & High addressability image generator using pseudo interpolation of video and screen data. & XEROX CORP (US) & 1992-05-21\\\hline
ep0603994\footnote{/patents/txt/ep/0603/994} & Method and apparatus for transferring and processing data. & FUJITSU AUTOMATION (JP) & 1992-12-18\\\hline
ep0663089\footnote{/patents/txt/ep/0663/089} & Virtual reality generator for use with financial information &  & 1992-09-30\\\hline
ep0323013\footnote{/patents/txt/ep/0323/013} & Method of operating a multiprocessor system employing a shared virtual memory. & IBM (US) & 1987-11-30\\\hline
ep0509994\footnote{/patents/txt/ep/0509/994} & CENTRALIZED REFERENCE AND CHANGE TABLE FOR A MULTIPROCESSOR VIRTUAL MEMORY SYSTEM & WANG LABORATORIES (US) & 1990-01-11\\\hline
ep0498967\footnote{/patents/txt/ep/0498/967} & Bandwidth allocation for permanent virtual connections. & BELL TELEPHONE MFG (BE) & 1991-02-13\\\hline
ep0712509\footnote{/patents/txt/ep/0712/509} & Virtual emissions monitor for automobile & PAVILION TECH INC (US) & 1993-08-05\\\hline
ep0712463\footnote{/patents/txt/ep/0712/463} & Virtual emissions monitor for automobile & PAVILION TECH INC (US) & 1993-08-05\\\hline
ep0344881\footnote{/patents/txt/ep/0344/881} & Personal head mounted display. & REFLECTION TECHNOLOGY INC (US) & 1988-05-31\\\hline
ep0422945\footnote{/patents/txt/ep/0422/945} & Parallel processing trace data manipulation. & IBM (US) & 1989-10-13\\\hline
ep0652521\footnote{/patents/txt/ep/0652/521} & Rapid data retrieval from a physically addressed data storage structure using memory page crossing predictive annotations. & SUN MICROSYSTEMS INC (US) & 1993-11-04\\\hline
ep0407119\footnote{/patents/txt/ep/0407/119} & Apparatus and method for reading, writing and refreshing memory with direct virtual or physical access & TANDEM COMPUTERS INC (US) & 1989-07-03\\\hline
ep0391517\footnote{/patents/txt/ep/0391/517} & Method and apparatus for ordering and queueing multiple memory access requests & DIGITAL EQUIPMENT CORP (US) & 1989-02-03\\\hline
ep0262301\footnote{/patents/txt/ep/0262/301} & Paging supervisor. & IBM (US) & 1987-05-04\\\hline
ep0609030\footnote{/patents/txt/ep/0609/030} & Method and apparatus for browsing information in a computer database. & SUN MICROSYSTEMS INC (US) & 1993-01-26\\\hline
ep0261690\footnote{/patents/txt/ep/0261/690} & Control structures file used by a dump program. & HONEYWELL BULL (US) & 1986-09-26\\\hline
ep0663091\footnote{/patents/txt/ep/0663/091} & Virtual reality imaging system & UNIV CORP ATMOSPHERIC RES (US) & 1992-10-01\\\hline
ep0619900\footnote{/patents/txt/ep/0619/900} & A SYNCHRONOUS SERIAL COMMUNICATION NETWORK FOR CONTROLLING SINGLE POINT I/O DEVICES & SQUARE D CO (US) & 1991-12-23\\\hline
ep0194721\footnote{/patents/txt/ep/0194/721} & Apparatus for receiving digital sound/data information & PHILIPS CORP (US) & 1985-11-15\\\hline
ep0402683\footnote{/patents/txt/ep/0402/683} & Method and apparatus for updating firmware resident in an electrically erasable programmable read-only memory. & DIGITAL EQUIPMENT CORP (US) & 1989-06-14\\\hline
ep0282357\footnote{/patents/txt/ep/0282/357} & Letter processing apparatus. & PITNEY BOWES (US) & 1987-03-13\\\hline
ep0577328\footnote{/patents/txt/ep/0577/328} & Secure toll collection system for moving vehicles. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1992-07-02\\\hline
ep0706685\footnote{/patents/txt/ep/0706/685} & ADVANCED PROGRAM-TO-PROGRAM COMMUNICATION SERVER & DOW BENELUX (NL) & 1993-06-28\\\hline
ep0342140\footnote{/patents/txt/ep/0342/140} & Method for presenting compressed electronic day calendar information in an interactive information handling system. & IBM (US) & 1988-05-02\\\hline
ep0193933\footnote{/patents/txt/ep/0193/933} & Apparatus for control of one computer system by another computer system. & WANG LABORATORIES (US) & 1985-03-05\\\hline
ep0823093\footnote{/patents/txt/ep/0823/093} & Methods, systems and computer program products for the synchronization of time coherent caching system & IBM (US) & 1996-02-15\\\hline
ep0570123\footnote{/patents/txt/ep/0570/123} & Computer system security method and apparatus having program authorization information data structures. & FISCHER ADDISON M (US) & 1992-05-15\\\hline
ep0753224\footnote{/patents/txt/ep/0753/224} & Customer premises equipment receives high-speed downstream data over a cable television system and transmits lower speed upstream signaling on a separate channel & AT \& T CORP (US) & 1994-03-31\\\hline
\end{longtable}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: latex ;
% End: ;

