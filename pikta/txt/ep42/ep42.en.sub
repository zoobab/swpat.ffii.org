\begin{subdocument}{swpiktxt42}{Top Software Probability Batch nr 42: 4101-4200}{http://swpat.ffii.org/patents/txt/ep42/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0319034\footnote{/patents/txt/ep/0319/034} & Method of recovering failure of online control program. & HITACHI LTD (JP) & 1987-12-04\\\hline
ep0411584\footnote{/patents/txt/ep/0411/584} & Method and apparatus for development of control program. & JAPAN ELECTRONIC CONTROL SYST (JP) & 1989-07-31\\\hline
ep0577393\footnote{/patents/txt/ep/0577/393} & Method for executing a program. & CANON KK (JP) & 1992-06-29\\\hline
ep0303868\footnote{/patents/txt/ep/0303/868} & Stack control. & MUELLER OTTO & 1987-08-06\\\hline
ep0231472\footnote{/patents/txt/ep/0231/472} & Editing system for virtual machines. & IBM (US) & 1985-12-30\\\hline
ep0414264\footnote{/patents/txt/ep/0414/264} & Virtual microphone apparatus and method. & SONY CORP (JP) & 1989-08-25\\\hline
ep0467448\footnote{/patents/txt/ep/0467/448} & Processing device and method of programming such a processing device. & PHILIPS NV (NL) & 1990-07-16\\\hline
ep0033833\footnote{/patents/txt/ep/0033/833} & Method for operating a transaction execution system having improved verification of personal identification & IBM & 1980-02-11\\\hline
ep0689752\footnote{/patents/txt/ep/0689/752} & ISDN adapter card for a computer & ELSA GMBH (DE) & 1994-03-08\\\hline
ep0288820\footnote{/patents/txt/ep/0288/820} & A slip and method of and apparatus for automatic reading of the slip. & ICS TSUSHO KK (JP) & 1987-10-14\\\hline
ep0015531\footnote{/patents/txt/ep/0015/531} & Data recording system & SCHOENHUBER MAX J (CH) & 1979-03-06\\\hline
ep0316144\footnote{/patents/txt/ep/0316/144} & Method and apparatus for classifying graphics segments to facilitate pick and display operation. & TEKTRONIX INC (US) & 1987-11-09\\\hline
ep0449618\footnote{/patents/txt/ep/0449/618} & Video graphics systems & QUANTEL LTD (GB) & 1990-03-28\\\hline
ep0637794\footnote{/patents/txt/ep/0637/794} & Apparatus and configuration method for a small, hand-held computing device. & XEROX CORP (US) & 1993-07-29\\\hline
ep0335494\footnote{/patents/txt/ep/0335/494} & Watchdog timer. & ADVANCED MICRO DEVICES INC (US) & 1988-03-29\\\hline
ep0445799\footnote{/patents/txt/ep/0445/799} & Fault recovery processing for supercomputer. & NIPPON ELECTRIC CO (JP) & 1990-03-08\\\hline
ep0470322\footnote{/patents/txt/ep/0470/322} & Message-based debugger. & BULL HN INFORMATION SYST (IT) & 1990-08-07\\\hline
ep0583348\footnote{/patents/txt/ep/0583/348} & Smart card locking process & GEMPLUS CARD INT (FR) & 1995-08-29\\\hline
ep0689171\footnote{/patents/txt/ep/0689/171} & Coin tester & NAT REJECTORS GMBH (DE) & 1994-06-03\\\hline
ep0422553\footnote{/patents/txt/ep/0422/553} & Precipitation measuring device. & EUROP COMMUNITIES (LU) & 1989-10-09\\\hline
ep0435139\footnote{/patents/txt/ep/0435/139} & A programmable controller for executing SFC language programs. & MITSUBISHI ELECTRIC CORP (JP) & 1989-12-26\\\hline
ep0416330\footnote{/patents/txt/ep/0416/330} & Programmable controller. & OMRON TATEISI ELECTRONICS CO (JP) & 1989-09-06\\\hline
ep0599330\footnote{/patents/txt/ep/0599/330} & RDS receiver. & CLARION CO LTD (JP) & 1992-11-27\\\hline
ep0390417\footnote{/patents/txt/ep/0390/417} & Spread-spectrum identification signal for communications systems. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1989-03-31\\\hline
ep0214609\footnote{/patents/txt/ep/0214/609} & Electronic transaction method and system. & HITACHI LTD (JP) & 1985-09-04\\\hline
ep0613105\footnote{/patents/txt/ep/0613/105} & Process for the control of secret keys between two smart cards & FRANCE TELECOM (FR) & 1993-02-25\\\hline
ep0541097\footnote{/patents/txt/ep/0541/097} & Protection circuit. & SUMITOMO ELECTRIC INDUSTRIES (JP) & 1991-11-08\\\hline
ep0483862\footnote{/patents/txt/ep/0483/862} & Control apparatus controlling process. & MITSUBISHI ELECTRIC CORP (JP) & 1990-10-31\\\hline
ep0922252\footnote{/patents/txt/ep/0922/252} & \#f & SIEMENS AG (DE) & 1996-08-30\\\hline
ep0541567\footnote{/patents/txt/ep/0541/567} & Method for analyzing movements in temporal sequences of digital images & SIEMENS AG (DE) & 1990-07-27\\\hline
ep0509917\footnote{/patents/txt/ep/0509/917} & Method for analysing a currency or security document representing a printed image and two superposed security patterns, and device to implement this method. & BANQUE DE FRANCE (FR) & 1991-04-18\\\hline
ep0186304\footnote{/patents/txt/ep/0186/304} & Signal analysis apparatus including recursive filter for electromagnetic surveillance system & PROGRESSIVE DYNAMICS (US) & 1984-11-19\\\hline
ep0421780\footnote{/patents/txt/ep/0421/780} & Multifunction control of a prosthetic limb using syntactic analysis of the dynamic myoelectric signal patterns associated with the onset of muscle contraction. & STEEPER HUGH LTD (GB) & 1989-10-04\\\hline
ep0512719\footnote{/patents/txt/ep/0512/719} & Method and apparatus for performing mapping-type analysis including use of limited electrode sets. & PHYSIO CONTROL CORP (US) & 1991-05-09\\\hline
ep0473664\footnote{/patents/txt/ep/0473/664} & ANALYSIS OF WAVEFORMS & MEDICAL RES COUNCIL (GB) & 1989-05-18\\\hline
ep0572604\footnote{/patents/txt/ep/0572/604} & Capillary zone electrophoretic analysis of isoenzymes & BECKMAN INSTRUMENTS INC (US) & 1991-11-14\\\hline
ep0568114\footnote{/patents/txt/ep/0568/114} & Data analysis systems and methods. & FIRST DATA RESOURCES INC (US) & 1985-07-10\\\hline
ep0830481\footnote{/patents/txt/ep/0830/481} & Soil analysis and sampling system & FUGRO ENG BV (NL) & 1996-01-10\\\hline
ep0510677\footnote{/patents/txt/ep/0510/677} & Analyzing system for operating condition of electrical apparatus. & HITACHI LTD (JP) & 1991-04-25\\\hline
ep0403121\footnote{/patents/txt/ep/0403/121} & Computer controlled screen animation. & IBM (US) & 1989-06-16\\\hline
ep0457403\footnote{/patents/txt/ep/0457/403} & Multilevel instruction cache, method for using said cache, method for compiling instructions for said cache and micro computer system using such a cache. & KONINKL PHILIPS ELECTRONICS NV (NL) & 1990-05-18\\\hline
ep0449368\footnote{/patents/txt/ep/0449/368} & Method for compiling computer instructions for increasing cache efficiency. & KONINKL PHILIPS ELECTRONICS NV (NL) & 1990-03-27\\\hline
ep0488567\footnote{/patents/txt/ep/0488/567} & Cache controller. & SUN MICROSYSTEMS INC (US) & 1990-11-27\\\hline
ep0568221\footnote{/patents/txt/ep/0568/221} & Methods and apparatus for implementing a pseudo-LRU cache memory replacement scheme with a locking feature. & SUN MICROSYSTEMS INC (US) & 1992-04-29\\\hline
ep0603669\footnote{/patents/txt/ep/0603/669} & Character input method and apparatus. & CANON KK (JP) & 1992-12-24\\\hline
ep0723681\footnote{/patents/txt/ep/0723/681} & Code sequence detection. & NORTHERN TELECOM LTD (CA) & 1993-07-20\\\hline
ep0443808\footnote{/patents/txt/ep/0443/808} & Image communicating apparatus. & CANON KK (JP) & 1990-02-23\\\hline
ep0382296\footnote{/patents/txt/ep/0382/296} & Public communication system comprising distributed stations, and station and sub-station for use in such a communication system. & PHILIPS NV (NL) & 1989-02-08\\\hline
ep0365905\footnote{/patents/txt/ep/0365/905} & Process for controlling centrally controlled communication exchanges. & SIEMENS AG (DE) & 1989-02-21\\\hline
ep0355714\footnote{/patents/txt/ep/0355/714} & Method and apparatus for initiating communication services. & SIEMENS NIXDORF INF SYST (DE) & 1988-08-17\\\hline
ep0482233\footnote{/patents/txt/ep/0482/233} & Cryptographic system allowing encrypted communication between users with a secure mutual cipher key determined without user interaction. & OMNISEC AG (CH) & 1990-10-24\\\hline
ep0538946\footnote{/patents/txt/ep/0538/946} & Method for authenticating communication participants, and system for application of the method. & NEDERLAND PTT (NL) & 1991-10-25\\\hline
ep0684533\footnote{/patents/txt/ep/0684/533} & Processor master/slave for the management of a communication protocol. & SGS THOMSON MICROELECTRONICS (FR) & 1994-04-28\\\hline
ep0734634\footnote{/patents/txt/ep/0734/634} & Mobile telecommunication system base station, mobile terminal & NEDERLAND PTT (NL) & 1993-12-13\\\hline
ep0776580\footnote{/patents/txt/ep/0776/580} & PROCESS FOR CALCULATING THE STORAGE CAPACITY OF A COMMUNICATION SYSTEM CONFIGURATION & SIEMENS AG (DE); BECKMANN KLAUS (DE); KARNATZ HANS JUERGEN (DE) & 1994-08-17\\\hline
ep0223849\footnote{/patents/txt/ep/0223/849} & SUPER-COMPUTER SYSTEM ARCHITECTURES & SHEKELS HOWARD D & 1985-05-20\\\hline
ep0360352\footnote{/patents/txt/ep/0360/352} & Work station for a computer system comprising a display element, an input system, and an audio communicaton system, and computer system comprising such work stations. & PHILIPS NV (NL) & 1988-09-23\\\hline
ep0425187\footnote{/patents/txt/ep/0425/187} & Drawing 'polygon with edge'-type primitives in a computer graphics display system. & IBM (US) & 1989-10-23\\\hline
ep0408689\footnote{/patents/txt/ep/0408/689} & System and method of protecting integrity of computer data and software & LENTZ STEPHEN A (US) & 1988-11-03\\\hline
ep0520893\footnote{/patents/txt/ep/0520/893} & Method and system for computer-assisted welding, based on the welding area vision. & COMMISSARIAT ENERGIE ATOMIQUE (FR) & 1991-06-28\\\hline
ep0518623\footnote{/patents/txt/ep/0518/623} & Apparatus and method for suspending and resuming software on a computer. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1991-06-10\\\hline
ep0570137\footnote{/patents/txt/ep/0570/137} & Icon driven computer apparatus. & IBM (US) & 1992-05-11\\\hline
ep0543820\footnote{/patents/txt/ep/0543/820} & HIGHLY SAFE MULTI-COMPUTER SYSTEM WITH THREE COMPUTERS & SIEMENS AG (DE) & 1990-08-14\\\hline
ep0541980\footnote{/patents/txt/ep/0541/980} & Control elements for a computer unit. & SIEMENS AG (DE) & 1991-10-22\\\hline
ep0674784\footnote{/patents/txt/ep/0674/784} & Method for testing at least one class of an object-oriented program on a computer & SIEMENS AG (DE) & 1993-12-08\\\hline
ep0710942\footnote{/patents/txt/ep/0710/942} & A computer-assisted system for instructing students & AT \& T GLOBAL INF SOLUTION (US) & 1994-11-04\\\hline
ep0842468\footnote{/patents/txt/ep/0842/468} & Virus protection in computer systems & IBM (US) & 1995-07-31\\\hline
ep0243821\footnote{/patents/txt/ep/0243/821} & Digital servo control system for a data recording disk file & IBM (US) & 1994-10-13\\\hline
ep0221558\footnote{/patents/txt/ep/0221/558} & Data encryptor & NIPPON ELECTRIC CO (JP) & 1985-11-07\\\hline
ep0308070\footnote{/patents/txt/ep/0308/070} & A data recording disk file with digital servo control. & IBM (US) & 1987-09-16\\\hline
ep0360135\footnote{/patents/txt/ep/0360/135} & Method of handling interrupts in a data processing system. & SIEMENS NIXDORF INF SYST (DE) & 1988-09-12\\\hline
ep0366583\footnote{/patents/txt/ep/0366/583} & Method of exchanging data between programs in a data processing system. & IBM (US) & 1988-10-24\\\hline
ep0407031\footnote{/patents/txt/ep/0407/031} & Apparatus for transmitting digital data in analog form. & ADVANCED MICRO DEVICES INC (US) & 1989-07-06\\\hline
ep0433350\footnote{/patents/txt/ep/0433/350} & Method of handling interrupts in a data processing system. & SIEMENS NIXDORF INF SYST (DE) & 1988-09-12\\\hline
ep0444368\footnote{/patents/txt/ep/0444/368} & Data input system for single-instruction multiple-data processor. & TEXAS INSTRUMENTS FRANCE (FR); TEXAS INSTRUMENTS INC (US) & 1990-02-28\\\hline
ep0489227\footnote{/patents/txt/ep/0489/227} & Data storage system having removable media and equipped todownload a control program from the removable media. & TANDBERG DATA (NO) & 1990-12-06\\\hline
ep0616746\footnote{/patents/txt/ep/0616/746} & Error correction protocol of the network between a radio data terminal modem and a remote data modem using negotiated compression parameters & NOKIA TELECOMMUNICATIONS OY (FI) & 1993-08-16\\\hline
ep0671686\footnote{/patents/txt/ep/0671/686} & Synchronous remote data duplexing. & IBM (US) & 1994-02-22\\\hline
ep0715731\footnote{/patents/txt/ep/0715/731} & METHOD AND SYSTEM FOR SCROLLING THROUGH DATA & MICROSOFT CORP (US) & 1994-06-24\\\hline
ep0804765\footnote{/patents/txt/ep/0804/765} & \#f & ZAHNRADFABRIK FRIEDRICHSHAFEN (DE) & 1995-01-21\\\hline
ep0314456\footnote{/patents/txt/ep/0314/456} & Apparatus and method for recording and/or reproducing a digital signal. & SONY CORP (JP) & 1987-12-14\\\hline
ep0354487\footnote{/patents/txt/ep/0354/487} & Electronic digital switching and/or control apparatus, especially a time switch. & WEG LEGRAND GMBH (DE) & 1988-08-11\\\hline
ep0512881\footnote{/patents/txt/ep/0512/881} & Method and device for selecting information usable by a local unit connected to a digital transmission system & SEXTANT AVIONIQUE (FR) & 1991-05-07\\\hline
ep0480682\footnote{/patents/txt/ep/0480/682} & Digital audio signal recording apparatus. & PIONEER ELECTRONIC CORP (JP) & 1990-10-09\\\hline
ep0547818\footnote{/patents/txt/ep/0547/818} & Method and apparatus for controlling the processing of digital image signals. & XEROX CORP (US) & 1991-12-18\\\hline
ep0579795\footnote{/patents/txt/ep/0579/795} & DIGITAL IMAGE INTERPOLATION SYSTEM FOR ZOOM AND PAN EFFECTS & EASTMAN KODAK CO (US) & 1992-01-06\\\hline
ep0644474\footnote{/patents/txt/ep/0644/474} & A method for utilising medium nonuniformities to minimize unauthorized duplication of digital information. & UNIV SINGAPORE (SG) & 1993-09-13\\\hline
ep0745239\footnote{/patents/txt/ep/0745/239} & Method and apparatus to secure digital directory object changes & NOVELL INC (US) & 1994-12-15\\\hline
ep0428005\footnote{/patents/txt/ep/0428/005} & Microcontroller with non-volatile memory error repair. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1989-10-30\\\hline
ep0575422\footnote{/patents/txt/ep/0575/422} & Error burst detection & BRITISH TELECOMM (GB) & 1991-03-11\\\hline
ep0208054\footnote{/patents/txt/ep/0208/054} & Servo control system using a varying frequency servo pattern for read/write head positioning in a magnetic recording disk file & IBM (US) & 1985-03-18\\\hline
ep0484043\footnote{/patents/txt/ep/0484/043} & Translation of midi files. & IBM (US) & 1990-11-01\\\hline
ep0217448\footnote{/patents/txt/ep/0217/448} & Raster image processor & OCE NEDERLAND BV (NL) & 1985-09-27\\\hline
ep0618737\footnote{/patents/txt/ep/0618/737} & Neural network video image processor. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1993-09-10\\\hline
ep0179612\footnote{/patents/txt/ep/0179/612} & Cryptographic system for direct broadcast satellite network. & GEN INSTRUMENT CORP (US) & 1984-10-26\\\hline
ep0228830\footnote{/patents/txt/ep/0228/830} & Communications network & THORN EMI ELECTRONICS LTD (GB) & 1985-12-18\\\hline
ep0493157\footnote{/patents/txt/ep/0493/157} & Serial network topology generator. & IBM (US) & 1990-12-20\\\hline
ep0658023\footnote{/patents/txt/ep/0658/023} & Dynamic user registration method in a mobile communications network. & IBM (US) & 1993-12-08\\\hline
ep0320274\footnote{/patents/txt/ep/0320/274} & An initial program load control system in a multiprocessor system. & FUJITSU LTD (JP) & 1987-12-09\\\hline
ep0394939\footnote{/patents/txt/ep/0394/939} & Program transmission system and method. & SONY CORP (JP) & 1989-04-27\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

