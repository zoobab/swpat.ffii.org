\begin{subdocument}{swpiktxt100}{Top Software Probability Batch nr 100: 9901-10000}{http://swpat.ffii.org/patents/txt/ep100/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0240061\footnote{/patents/txt/ep/0240/061} & Domino-type MOS logic gate having an MOS sub-network & PHILIPS CORP (US) & 1986-03-28\\\hline
ep0318738\footnote{/patents/txt/ep/0318/738} & Logic path length reduction using boolean minimization. & IBM (US) & 1987-12-02\\\hline
ep0589553\footnote{/patents/txt/ep/0589/553} & Register to enable and disable built-in testing logic. & ADVANCED MICRO DEVICES INC (US) & 1992-08-27\\\hline
ep0536348\footnote{/patents/txt/ep/0536/348} & Security device for ring network & 3COM CORP (US) & 1991-03-28\\\hline
ep0653886\footnote{/patents/txt/ep/0653/886} & Video on demand network. & BELL TELEPHONE MFG (BE) & 1993-11-17\\\hline
ep0906667\footnote{/patents/txt/ep/0906/667} & \#f & ALTVATER AIR DATA SYSTEMS GMBH (DE) & 1996-04-25\\\hline
ep0169324\footnote{/patents/txt/ep/0169/324} & Switching protocol with retry. & IBM (US) & 1984-06-29\\\hline
ep0625858\footnote{/patents/txt/ep/0625/858} & Video server memory management method. & BELL TELEPHONE MFG (BE); ALCATEL NV (NL) & 1993-05-19\\\hline
ep0625857\footnote{/patents/txt/ep/0625/857} & Video server. & BELL TELEPHONE MFG (BE); ALCATEL NV (NL) & 1993-05-19\\\hline
ep0523618\footnote{/patents/txt/ep/0523/618} & Picture codec and teleconference terminal equipment. & HITACHI LTD (JP) & 1991-07-15\\\hline
ep0635774\footnote{/patents/txt/ep/0635/774} & Hand-held terminal for performing purchasing, debit, credit and drawing operations. & BANCARI SERVIZI SSB SPA (IT) & 1993-07-12\\\hline
ep0477754\footnote{/patents/txt/ep/0477/754} & System for selection of RF input terminals and associated scan lists & THOMSON CONSUMER ELECTRONICS (US) & 1990-09-24\\\hline
ep0732446\footnote{/patents/txt/ep/0732/446} & Calender for treating both sides of a paper web & VOITH SULZER FINISHING GMBH (DE) & 1995-03-09\\\hline
ep0657813\footnote{/patents/txt/ep/0657/813} & Distributed database management. & IBM (US) & 1993-12-06\\\hline
ep0639286\footnote{/patents/txt/ep/0639/286} & System for extracting knowledge of typicality and exceptionality from a database of case records & PERCEPTIVE DECISION SYSTEMS IN (US) & 1992-05-07\\\hline
ep0595925\footnote{/patents/txt/ep/0595/925} & Multilevel transaction recovery in a database system which loss parent transaction undo operation upon commit of child transaction & DIGITAL EQUIPMENT CORP (US) & 1991-07-11\\\hline
ep0593354\footnote{/patents/txt/ep/0593/354} & Query optimisation method for a relational database management system. & BULL SA (FR) & 1992-10-12\\\hline
ep0592074\footnote{/patents/txt/ep/0592/074} & System for processing a database relocation. & FUJITSU LTD (JP) & 1992-09-25\\\hline
ep0541381\footnote{/patents/txt/ep/0541/381} & Managing database recovery from failure. & IBM (US) & 1991-11-08\\\hline
ep0425412\footnote{/patents/txt/ep/0425/412} & Process and apparatus for handling time consuming and reusable queries in an object oriented database management system. & IBM (US) & 1989-10-23\\\hline
ep0421408\footnote{/patents/txt/ep/0421/408} & Joining two database relations on a common field in a parallel relational database field. & IBM (US) & 1989-10-05\\\hline
ep0378660\footnote{/patents/txt/ep/0378/660} & INFERENCE SYSTEM USING A STABLE STORAGE RULE AND FACT DATABASE & DIGITAL EQUIPMENT CORP (US) & 1988-06-30\\\hline
ep0191036\footnote{/patents/txt/ep/0191/036} & Database backup method & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1984-07-26\\\hline
ep0198010\footnote{/patents/txt/ep/0198/010} & Packet switched multiport memory NXM switch node and processing method & HUGHES AIRCRAFT CO (US) & 1984-10-18\\\hline
ep0177816\footnote{/patents/txt/ep/0177/816} & Non-volatile dynamic random access memory cell & IBM (US) & 1984-09-27\\\hline
ep0172574\footnote{/patents/txt/ep/0172/574} & Memory address location system for an electronic postage meter having multiple non-volatile memories & PITNEY BOWES (US) & 1984-08-22\\\hline
ep0408812\footnote{/patents/txt/ep/0408/812} & Distributed object based systems. & HEWLETT PACKARD CO (US) & 1989-07-21\\\hline
ep0492049\footnote{/patents/txt/ep/0492/049} & Distributed telephone conference control device. & RICOS KK (JP) & 1990-12-21\\\hline
ep0207439\footnote{/patents/txt/ep/0207/439} & FIFO memory with decreased fall-through delay & WANG LABORATORIES (US) & 1985-06-28\\\hline
ep0462708\footnote{/patents/txt/ep/0462/708} & Video random access memory. & IBM (US) & 1990-06-19\\\hline
ep0444601\footnote{/patents/txt/ep/0444/601} & Memory access control. & NIPPON ELECTRIC CO (JP) & 1990-02-26\\\hline
ep0421693\footnote{/patents/txt/ep/0421/693} & Memory self-test. & IBM (US) & 1989-10-06\\\hline
ep0566014\footnote{/patents/txt/ep/0566/014} & Multi port memory system. & THOMSON CONSUMER ELECTRONICS (US) & 1992-04-28\\\hline
ep0814455\footnote{/patents/txt/ep/0814/455} & Scalable three-dimensional window borders & MICROSOFT CORP (US) & 1993-05-14\\\hline
ep0808501\footnote{/patents/txt/ep/0808/501} & Filtered serial event controlled command port for memory & MICRON QUANTUM DEVICES INC (US) & 1995-02-10\\\hline
ep0825779\footnote{/patents/txt/ep/0825/779} & Moving picture cosing apparatus/method with output buffer memory control & VICTOR COMPANY OF JAPAN (JP) & 1996-08-19\\\hline
ep0045645\footnote{/patents/txt/ep/0045/645} & Document retrieval system & OK PARTNERSHIP LTD & 1980-08-01\\\hline
ep0094976\footnote{/patents/txt/ep/0094/976} & Time-measuring adapter for logic analyzer & IBM (US) & 1982-05-24\\\hline
ep0503902\footnote{/patents/txt/ep/0503/902} & Output method and apparatus using the same. & CANON KK (JP) & 1991-04-23\\\hline
ep0528597\footnote{/patents/txt/ep/0528/597} & Apparatus and methods for moving/copying objects using destination and/or source bins. & SUN MICROSYSTEMS INC (US) & 1991-08-16\\\hline
ep0133726\footnote{/patents/txt/ep/0133/726} & Video translation system for translating a binary coded data signal into a video signal and vice-versa & PHILIPS CORP (US) & 1983-08-08\\\hline
ep0197173\footnote{/patents/txt/ep/0197/173} & Measuring device for a figure & KUBO AKIO (JP) & 1983-12-26\\\hline
ep0198214\footnote{/patents/txt/ep/0198/214} & Branch control in a three phase pipelined signal processor. & IBM (US) & 1985-04-15\\\hline
ep0219930\footnote{/patents/txt/ep/0219/930} & Storing and searching a representation of topological structures. & ETAK INC (US) & 1985-07-25\\\hline
ep0477904\footnote{/patents/txt/ep/0477/904} & Method and apparatus for generating images of reduced size & DAINIPPON SCREEN MFG (JP) & 1990-09-26\\\hline
ep0263055\footnote{/patents/txt/ep/0263/055} & Equalization in redundant channels & UNITED TECHNOLOGIES CORP (US) & 1986-10-02\\\hline
ep0284327\footnote{/patents/txt/ep/0284/327} & Shared micro-ram with micro-rom linking. & TANDEM COMPUTERS INC (US) & 1987-03-27\\\hline
ep0291353\footnote{/patents/txt/ep/0291/353} & Microcomputers. & SONY CORP (JP) & 1987-05-14\\\hline
ep0303009\footnote{/patents/txt/ep/0303/009} & Signal generator for circular addressing. & IBM (US) & 1987-06-29\\\hline
ep0306197\footnote{/patents/txt/ep/0306/197} & A method for computing transitive closure. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1987-08-31\\\hline
ep0341256\footnote{/patents/txt/ep/0341/256} & Modem and method using multidimensional coded modulation & RACAL DATA COMMUNICATIONS INC (US) & 1987-01-15\\\hline
ep0565290\footnote{/patents/txt/ep/0565/290} & Object tracking system. & IBM (US) & 1992-04-06\\\hline
ep0327002\footnote{/patents/txt/ep/0327/002} & Apparatus and method for generating high-quality pattern. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1988-11-18\\\hline
ep0644515\footnote{/patents/txt/ep/0644/515} & A transponder system. & TEXAS INSTRUMENTS DEUTSCHLAND (DE) & 1993-09-15\\\hline
ep0641450\footnote{/patents/txt/ep/0641/450} & ELECTRONIC SYSTEM AND METHOD FOR REMOTE IDENTIFICATION OF CODED ARTICLES AND THE LIKE & DINGWALL ANDREW GORDON FRANCIS (US); SARNOFF DAVID RES CENTER (US); SCHEPPS JONATHAN LLOYD (US) & 1992-05-21\\\hline
ep0654747\footnote{/patents/txt/ep/0654/747} & Tape printer. & BROTHER IND LTD (JP) & 1993-12-27\\\hline
ep0359809\footnote{/patents/txt/ep/0359/809} & Apparatus and method for floating point normalization prediction & DIGITAL EQUIPMENT CORP (US) & 1988-04-01\\\hline
ep0370018\footnote{/patents/txt/ep/0370/018} & Lookahead bus arbitration system with override of conditional access grants by bus cycle extensions for multicycle data transfers & DIGITAL EQUIPMENT CORP (US) & 1987-05-01\\\hline
ep0418498\footnote{/patents/txt/ep/0418/498} & Block averaging of time varying signal attribute measurements. & HEWLETT PACKARD CO (US) & 1989-09-19\\\hline
ep0435805\footnote{/patents/txt/ep/0435/805} & Method of forming a search criteria and saving a search result. & IBM (US) & 1989-12-28\\\hline
ep0441089\footnote{/patents/txt/ep/0441/089} & Using command similarity in an intelligent help system. & IBM (US) & 1990-02-08\\\hline
ep0464708\footnote{/patents/txt/ep/0464/708} & High speed bus system. & DIGITAL EQUIPMENT CORP (US) & 1990-06-29\\\hline
ep0476990\footnote{/patents/txt/ep/0476/990} & Dynamic bus arbitration. & IBM (US) & 1990-09-21\\\hline
ep0478141\footnote{/patents/txt/ep/0478/141} & Keypad status reporting system. & ADVANCED MICRO DEVICES INC (US) & 1990-09-27\\\hline
ep0486037\footnote{/patents/txt/ep/0486/037} & A combination problem solving apparatus. & FUJITSU LTD (JP) & 1990-11-14\\\hline
ep0484875\footnote{/patents/txt/ep/0484/875} & Parcel processing system with end of day rating. & PITNEY BOWES (US) & 1990-11-05\\\hline
ep0491290\footnote{/patents/txt/ep/0491/290} & IC Tester. & ADVANTEST CORP (JP) & 1990-12-19\\\hline
ep0500048\footnote{/patents/txt/ep/0500/048} & Orthogonal transform apparatus for video signal processing. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1991-02-19\\\hline
ep0506988\footnote{/patents/txt/ep/0506/988} & Method of bus arbitration in a multi-master system. & ITT IND GMBH DEUTSCHE (DE) & 1991-03-30\\\hline
ep0521486\footnote{/patents/txt/ep/0521/486} & Hierarchical structure processor. & HITACHI LTD (JP) & 1991-07-03\\\hline
ep0529220\footnote{/patents/txt/ep/0529/220} & Method for acquiring the identifier of a node in an input/output system. & IBM (US) & 1991-08-27\\\hline
ep0539116\footnote{/patents/txt/ep/0539/116} & Method and apparatus for generating correction signals for forming low distortion analog signals. & HALLIBURTON GEOPHYS SERVICE (US) & 1991-10-25\\\hline
ep0537721\footnote{/patents/txt/ep/0537/721} & Hardware-configured operating system kernel for a multitasking processor. & HEWLETT PACKARD CO (US) & 1991-10-15\\\hline
ep0616707\footnote{/patents/txt/ep/0616/707} & A METHOD OF FAST PATTERN MATCH DETERMINATION BY EQUIVALENCE CLASS PROJECTION MEANS & YERAZUNIS WILLIAM S (US); KIRK STEVEN A (US); BARABASH WILLIAM (US); DIGITAL EQUIPMENT CORP (US) & 1991-12-09\\\hline
ep0664902\footnote{/patents/txt/ep/0664/902} & Concurrent processing apparatus with incremental command objects & TALIGENT INC (US) & 1993-02-26\\\hline
ep0592213\footnote{/patents/txt/ep/0592/213} & Synchronous/asynchronous partitioning of an asynchronous bus interface. & DIGITAL EQUIPMENT CORP (US) & 1992-10-07\\\hline
ep0611171\footnote{/patents/txt/ep/0611/171} & System for synchronizing replicated tasks & CEGELEC (FR) & 1993-01-08\\\hline
ep0622732\footnote{/patents/txt/ep/0622/732} & Microcomputer. & TOSHIBA MICRO ELECTRONICS (JP); TOKYO SHIBAURA ELECTRIC CO (JP) & 1993-04-27\\\hline
ep0658256\footnote{/patents/txt/ep/0658/256} & High performance mantissa divider & CRAY RESEARCH INC (US) & 1992-09-01\\\hline
ep0842463\footnote{/patents/txt/ep/0842/463} & REDUCED KEYBOARD DISAMBIGUATING SYSTEM & GROVER DALE L (US); KING MARTIN T (US); GRUNBOCK CHERYL ARLENE (US); KUSHLER CLIFFORD A (US) & 1996-06-10\\\hline
ep0318227\footnote{/patents/txt/ep/0318/227} & Frame synchronising method and system. & SONY CORP (JP) & 1987-11-27\\\hline
ep0558118\footnote{/patents/txt/ep/0558/118} & Transmission system comprising receiver with improved timing means. & KONINKL PHILIPS ELECTRONICS NV (NL) & 1992-02-24\\\hline
ep0461451\footnote{/patents/txt/ep/0461/451} & Wireless telephone service subscriber call routing method & ALCATEL NV (NL) & 1990-05-30\\\hline
ep0595409\footnote{/patents/txt/ep/0595/409} & A construction for milking cows. & LELY NV C VAN DER (NL) & 1992-10-29\\\hline
ep0672382\footnote{/patents/txt/ep/0672/382} & An electronic blood pressure monitor. & OMRON TATEISI ELECTRONICS CO (JP) & 1994-02-28\\\hline
ep0682893\footnote{/patents/txt/ep/0682/893} & Workstation equipment container. & SPECIALISED BANKING FURNITURE (GB) & 1994-04-22\\\hline
ep0222943\footnote{/patents/txt/ep/0222/943} & Variable word length encoder. & NORTHERN TELECOM LTD (CA) & 1985-11-04\\\hline
ep0246714\footnote{/patents/txt/ep/0246/714} & Pseudo-random binary sequence generators. & TELEDIFFUSION FSE (FR); FRANCE ETAT (FR); BRITISH BROADCASTING CORP (GB); MARCONI CO LTD (GB); PLESSEY CO PLC (GB); INDEP BROADCASTING AUTHORITY (GB); PHILIPS NV (NL) & 1984-05-16\\\hline
ep0244869\footnote{/patents/txt/ep/0244/869} & Facsimile apparatus & TOKYO ELECTRIC CO LTD (JP) & 1986-05-08\\\hline
ep0244001\footnote{/patents/txt/ep/0244/001} & Hybrid coder for videosignals. & PHILIPS PATENTVERWALTUNG (DE); PHILIPS NV (NL) & 1986-11-08\\\hline
ep0247703\footnote{/patents/txt/ep/0247/703} & Pseudo-random binary sequence generators. & TELEDIFFUSION FSE (FR); FRANCE ETAT (FR); BRITISH BROADCASTING CORP (GB); MARCONI CO LTD (GB); PLESSEY CO PLC (GB); INDEP BROADCASTING AUTHORITY (GB); PHILIPS NV (NL) & 1984-05-16\\\hline
ep0309280\footnote{/patents/txt/ep/0309/280} & Video coder. & BRITISH TELECOMM (GB) & 1987-09-23\\\hline
ep0254271\footnote{/patents/txt/ep/0254/271} & Multiplexer for multiplexing insertion codes and a digital data signal train & NIPPON ELECTRIC CO (JP) & 1986-07-23\\\hline
ep0285024\footnote{/patents/txt/ep/0285/024} & Hand-held manually operable printing apparatus. & CASIO COMPUTER CO LTD (JP) & 1987-04-20\\\hline
ep0290932\footnote{/patents/txt/ep/0290/932} & Magnification changing system for a copier. & RICOH KK (JP) & 1987-05-09\\\hline
ep0294191\footnote{/patents/txt/ep/0294/191} & Reception control apparatus for a television receiver. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1987-06-02\\\hline
ep0305864\footnote{/patents/txt/ep/0305/864} & Improved sampling frequency converter for converting a lower sampling frequency to a higher sampling frequency and a method therefor. & SANYO ELECTRIC CO (JP) & 1987-08-31\\\hline
ep0318212\footnote{/patents/txt/ep/0318/212} & Magnetic resonance imaging methods and apparatus. & PICKER INT INC (US) & 1987-11-23\\\hline
ep0317159\footnote{/patents/txt/ep/0317/159} & Clock recovery arrangement. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1987-11-19\\\hline
ep0327289\footnote{/patents/txt/ep/0327/289} & Magnetic recording and playback apparatus. & SONY CORP (JP) & 1988-01-30\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

