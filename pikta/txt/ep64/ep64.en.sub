\begin{subdocument}{swpiktxt64}{Top Software Probability Batch nr 64: 6301-6400}{http://swpat.ffii.org/patents/txt/ep64/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0506165\footnote{/patents/txt/ep/0506/165} & Method and apparatus for controlling spark timing. & GEN MOTORS CORP (US); DELCO ELECTRONICS CORP (US) & 1991-03-27\\\hline
ep0608614\footnote{/patents/txt/ep/0608/614} & System for control of subscriber programmability. & AT \& T CORP (US) & 1992-12-28\\\hline
ep0582370\footnote{/patents/txt/ep/0582/370} & Disk drive controller with a posted write cache memory. & COMPAQ COMPUTER CORP (US) & 1992-06-05\\\hline
ep0384078\footnote{/patents/txt/ep/0384/078} & Network and protocol for real-time control of machine operations. & PITNEY BOWES (US) & 1988-12-28\\\hline
ep0888588\footnote{/patents/txt/ep/0888/588} & \#f & SIEMENS AG (DE) & 1996-03-18\\\hline
ep0250276\footnote{/patents/txt/ep/0250/276} & Interface between a data circuit terminating equipment and several data terminal equipments. & MONTAUDOIN PATRICE & 1986-05-27\\\hline
ep0527203\footnote{/patents/txt/ep/0527/203} & Method and device for effecting a transaction between a first and at least one second data carrier and carrier used for this purpose & STORCK JEAN RENE & 1991-05-03\\\hline
ep0532509\footnote{/patents/txt/ep/0532/509} & BUFFERING SYSTEM FOR DYNAMICALLY PROVIDING DATA TO MULTIPLE STORAGE ELEMENTS & MICRO TECHNOLOGY INC (US) & 1990-03-14\\\hline
ep0556138\footnote{/patents/txt/ep/0556/138} & A bus for connecting extension cards to a data processing system and test method. & HEWLETT PACKARD CO (US) & 1992-02-13\\\hline
ep0363465\footnote{/patents/txt/ep/0363/465} & METHOD AND APPARATUS FOR TESTING DIGITAL CIRCUITS & HEWLETT PACKARD CO (US) & 1988-03-03\\\hline
ep0520495\footnote{/patents/txt/ep/0520/495} & Microcomputer and program development apparatus. & MITSUBISHI ELECTRIC CORP (JP) & 1991-11-25\\\hline
ep0336435\footnote{/patents/txt/ep/0336/435} & Memory diagnostic apparatus and method. & WANG LABORATORIES (US) & 1988-04-08\\\hline
ep0477760\footnote{/patents/txt/ep/0477/760} & Programming method and apparatus for programmable controller. & HITACHI LTD (JP) & 1990-11-26\\\hline
ep0555034\footnote{/patents/txt/ep/0555/034} & Interactive display system and method for menu selection. & IBM (US) & 1992-02-07\\\hline
ep0602438\footnote{/patents/txt/ep/0602/438} & RDS broadcast receiver. & CLARION CO LTD (JP) & 1992-12-14\\\hline
ep0544542\footnote{/patents/txt/ep/0544/542} & Elevator management system time based security & OTIS ELEVATOR CO (US) & 1991-11-27\\\hline
ep0436242\footnote{/patents/txt/ep/0436/242} & Method of analysing and controlling a fluid influx during the drilling of a borehole. & SCHLUMBERGER SERVICES PETROL (FR) & 1989-12-20\\\hline
ep0065455\footnote{/patents/txt/ep/0065/455} & Method and device for analyzing a very high frequency radiation beam of electromagnetic waves & RADANT ETUDES (FR) & 1981-05-18\\\hline
ep0651567\footnote{/patents/txt/ep/0651/567} & Thermal image analysis system. & MITSUBISHI ELECTRIC CORP (JP) & 1993-10-29\\\hline
ep0650045\footnote{/patents/txt/ep/0650/045} & Process and apparatus for automatically characterizing, optimizing and checking a crack detection analysis method & SNECMA (FR) & 1993-10-20\\\hline
ep0551289\footnote{/patents/txt/ep/0551/289} & COMMUNICATION RECEIVER PROVIDING DISPLAYED OPERATING INSTRUCTIONS & MOTOROLA INC (US) & 1990-09-04\\\hline
ep0360256\footnote{/patents/txt/ep/0360/256} & Fuzzy computer system. & OMRON TATEISI ELECTRONICS CO (JP) & 1988-09-30\\\hline
ep0182963\footnote{/patents/txt/ep/0182/963} & Digital processor with floating point multiplier and adder suitable for digital signal processing & HITACHI ELECTRONICS (JP); HITACHI LTD (JP) & 1980-10-31\\\hline
ep0565856\footnote{/patents/txt/ep/0565/856} & Programmable start-of-sector pulse generator for a disk drive using embedded servo bursts and split data fields. & HEWLETT PACKARD CO (US) & 1992-04-16\\\hline
ep0484137\footnote{/patents/txt/ep/0484/137} & Digital filter for a music synthesizer. & IBM (US) & 1990-11-01\\\hline
ep0673578\footnote{/patents/txt/ep/0673/578} & ADVANCED SET TOP TERMINAL FOR CABLE TELEVISION DELIVERY SYSTEMS & DISCOVERY COMMUNICAT INC (US) & 1992-12-09\\\hline
ep0655868\footnote{/patents/txt/ep/0655/868} & Device for encoding a video signal. & PHILIPS ELECTRONICS NV (NL) & 1993-11-30\\\hline
ep0449075\footnote{/patents/txt/ep/0449/075} & Image reproduction apparatus. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1990-03-30\\\hline
ep0187420\footnote{/patents/txt/ep/0187/420} & A system for processing and transmitting information, and array for same. & ROMEIJN P ELECTRO TECH BV (NL) & 1985-01-03\\\hline
ep0673579\footnote{/patents/txt/ep/0673/579} & ADVANCED SET TOP TERMINAL FOR CABLE TELEVISION DELIVERY SYSTEMS & DISCOVERY COMMUNICAT INC (US) & 1992-12-09\\\hline
ep0674824\footnote{/patents/txt/ep/0674/824} & ADVANCED SET TOP TERMINAL FOR CABLE TELEVISION DELIVERY SYSTEMS & DISCOVERY COMMUNICAT INC (US) & 1992-12-09\\\hline
ep0673582\footnote{/patents/txt/ep/0673/582} & ADVANCED SET TOP TERMINAL FOR CABLE TELEVISION DELIVERY SYSTEMS & DISCOVERY COMMUNICAT INC (US) & 1992-12-09\\\hline
ep0673580\footnote{/patents/txt/ep/0673/580} & ADVANCED SET TOP TERMINAL FOR CABLE TELEVISION DELIVERY SYSTEMS & DISCOVERY COMMUNICAT INC (US) & 1992-12-09\\\hline
ep0252038\footnote{/patents/txt/ep/0252/038} & Apparatus for detecting received signals in a telecommunication system. & ERICSSON TELEFON AB L M (SE) & 1986-06-17\\\hline
ep0673583\footnote{/patents/txt/ep/0673/583} & ADVANCED SET TOP TERMINAL FOR CABLE TELEVISION DELIVERY SYSTEMS & DISCOVERY COMMUNICAT INC (US) & 1992-12-09\\\hline
ep0673581\footnote{/patents/txt/ep/0673/581} & ADVANCED SET TOP TERMINAL FOR CABLE TELEVISION DELIVERY SYSTEMS & DISCOVERY COMMUNICAT INC (US) & 1992-12-09\\\hline
ep0733245\footnote{/patents/txt/ep/0733/245} & Microprocessor-based memory card that limits memory accesses by application programs and method of operation & GEMPLUS CARD INT (FR) & 1994-12-06\\\hline
ep0114948\footnote{/patents/txt/ep/0114/948} & Scan correction for a line printer having multi-pitch type carriers & IBM (US) & 1982-12-27\\\hline
ep0177616\footnote{/patents/txt/ep/0177/616} & System for selecting an address in an input/output board & FANUC LTD (JP) & 1984-03-28\\\hline
ep0194129\footnote{/patents/txt/ep/0194/129} & Electronic apparatus control system & SONY CORP (JP) & 1985-03-05\\\hline
ep0574831\footnote{/patents/txt/ep/0574/831} & Product inspection method and apparatus. & KEY TECHNOLOGY INC (US) & 1992-06-16\\\hline
ep0223130\footnote{/patents/txt/ep/0223/130} & Electronic meter circuitry & FME CORP (US) & 1985-10-31\\\hline
ep0363086\footnote{/patents/txt/ep/0363/086} & Mask programmable bus control gate array. & ADVANCED MICRO DEVICES INC (US) & 1988-10-07\\\hline
ep0473444\footnote{/patents/txt/ep/0473/444} & Scheduling method for multiprocessor operating system & TEXAS INSTRUMENTS INC (US) & 1990-08-31\\\hline
ep0500151\footnote{/patents/txt/ep/0500/151} & Microprogram control unit. & NIPPON ELECTRIC CO (JP) & 1985-11-08\\\hline
ep0634030\footnote{/patents/txt/ep/0634/030} & Selective power-down for high performance CPU/system & SEIKO EPSON CORP (JP) & 1992-03-31\\\hline
ep0130131\footnote{/patents/txt/ep/0130/131} & Plan pick-up system. & COMMISSARIAT ENERGIE ATOMIQUE (FR) & 1983-06-28\\\hline
ep0600194\footnote{/patents/txt/ep/0600/194} & Access control system. & WINKHAUS FA AUGUST (DE) & 1992-12-02\\\hline
ep0461798\footnote{/patents/txt/ep/0461/798} & Configurable interconnect structure & ADVANCED MICRO DEVICES INC (US) & 1990-06-14\\\hline
ep0636955\footnote{/patents/txt/ep/0636/955} & Control unit for vehicle and total control system therefor. & HITACHI LTD (JP) & 1993-11-19\\\hline
ep0412747\footnote{/patents/txt/ep/0412/747} & Magnetic resonance systems. & PICKER INT INC (US) & 1989-08-11\\\hline
ep0451476\footnote{/patents/txt/ep/0451/476} & Secure key management using programmable control vector checking. & IBM (US) & 1990-04-09\\\hline
ep0569815\footnote{/patents/txt/ep/0569/815} & Evaluation system for an electrofilter & SIEMENS AG (DE) & 1992-05-14\\\hline
ep0575220\footnote{/patents/txt/ep/0575/220} & Multi-standard observation camera and a surveillance system using the camera & THOMSON CSF (FR) & 1992-06-16\\\hline
ep0329290\footnote{/patents/txt/ep/0329/290} & Improved supercritical fluid chromatography. & HEWLETT PACKARD CO (US) & 1988-02-17\\\hline
ep0490471\footnote{/patents/txt/ep/0490/471} & Set addressing process and apparatus. & XEROX CORP (US) & 1990-12-14\\\hline
ep0623806\footnote{/patents/txt/ep/0623/806} & Method and apparatus for calibrating a vehicle compass system. & LECTRON PRODUCTS (US) & 1993-05-07\\\hline
ep0283889\footnote{/patents/txt/ep/0283/889} & Digital processing device for analog input & KLOECKNER MOELLER (DE) & 1987-03-25\\\hline
ep0435475\footnote{/patents/txt/ep/0435/475} & High-performance frame buffer and cache memory system and method. & DIGITAL EQUIPMENT CORP (US) & 1989-12-22\\\hline
ep0408041\footnote{/patents/txt/ep/0408/041} & A communication unit comprising caller identification function and caller identifying method in a digital communication network. & SHARP KK (JP) & 1989-07-13\\\hline
ep0423933\footnote{/patents/txt/ep/0423/933} & Personal computer memory bank parity error indicator. & IBM (US) & 1989-10-16\\\hline
ep0498213\footnote{/patents/txt/ep/0498/213} & X-ray computerized tomographic image data acquisition circuitry capable of performing high speed data acquisition & TOKYO SHIBAURA ELECTRIC CO (JP) & 1991-02-07\\\hline
ep0202132\footnote{/patents/txt/ep/0202/132} & Process and apparatus for the insertion of insets into the image supplied by a digital scan converter & THOMSON CSF (FR) & 1985-04-02\\\hline
ep0259859\footnote{/patents/txt/ep/0259/859} & Information processing system capable of reducing invalid memory operations by detecting an error in a main memory & NIPPON ELECTRIC CO (JP) & 1986-09-10\\\hline
ep0418868\footnote{/patents/txt/ep/0418/868} & Image processing system & FUJI PHOTO FILM CO LTD (JP) & 1989-09-21\\\hline
ep0171784\footnote{/patents/txt/ep/0171/784} & Circuit arrangement for telecommunication installations, in particular telephone exchanges with centralized and/or decentralized information-processing control circuits. & SIEMENS AG (DE) & 1984-08-16\\\hline
ep0388272\footnote{/patents/txt/ep/0388/272} & SYSTEM FOR THE CONTROL OF THE PROGRESSION OF SEVERAL RAILWAY TRAINS IN A NETWORK & AIGLE AZUR CONCEPT (FR) & 1989-03-17\\\hline
ep0557210\footnote{/patents/txt/ep/0557/210} & OSI transport relay system between a network in connected mode and a network in non-connected mode & BULL SA (FR) & 1992-02-21\\\hline
ep0440197\footnote{/patents/txt/ep/0440/197} & Method and apparatus for inputting text. & HITACHI LTD (JP) & 1990-01-30\\\hline
ep0096628\footnote{/patents/txt/ep/0096/628} & Apparatus for combining a video signal with graphics and text from a computer & DIGITAL EQUIPMENT CORP (US) & 1982-06-02\\\hline
ep0200974\footnote{/patents/txt/ep/0200/974} & Programming device for a stored-program controller & BBC BROWN BOVERI \& CIE (CH) & 1985-04-25\\\hline
ep0451986\footnote{/patents/txt/ep/0451/986} & Tracking control method between two servo systems. & YASKAWA DENKI SEISAKUSHO KK (JP) & 1990-03-27\\\hline
ep0529875\footnote{/patents/txt/ep/0529/875} & Continuous time interpolator. & HEWLETT PACKARD CO (US) & 1991-08-29\\\hline
ep0612067\footnote{/patents/txt/ep/0612/067} & Optical disc recording apparatus. & KENWOOD CORP (JP) & 1992-11-19\\\hline
ep0195786\footnote{/patents/txt/ep/0195/786} & Telephone switching system adjunct call processing arrangement & AT \& T CO (US); AT \& T INFORMATION SYSTEMS INC (US) & 1984-09-27\\\hline
ep0366160\footnote{/patents/txt/ep/0366/160} & Non intrusive channel impairment analyser. & HEWLETT PACKARD LTD (GB) & 1986-03-06\\\hline
ep0366159\footnote{/patents/txt/ep/0366/159} & Non intrusive channel impairment analyser. & HEWLETT PACKARD LTD (GB) & 1986-03-06\\\hline
ep0233679\footnote{/patents/txt/ep/0233/679} & Non intrusive channel impairment analyser. & HEWLETT PACKARD LTD (GB) & 1986-03-06\\\hline
ep0202937\footnote{/patents/txt/ep/0202/937} & Surface analysis spectroscopy apparatus & TEKSCAN LTD (IE) & 1985-05-21\\\hline
ep0212020\footnote{/patents/txt/ep/0212/020} & Data processing card system and method of forming same. & HORIZON TECHNOLOGIES INC (US) & 1985-08-16\\\hline
ep0487267\footnote{/patents/txt/ep/0487/267} & Colour image processing for overlapping image parts & NINTENDO CO LTD (JP); RICOH KK (JP) & 1990-11-17\\\hline
ep0460520\footnote{/patents/txt/ep/0460/520} & Programming device for video recorder using a videotext program parade page. & GRUNDIG EMV (DE) & 1990-06-08\\\hline
ep0268289\footnote{/patents/txt/ep/0268/289} & Semiconductor memory device & NIPPON ELECTRIC CO (JP) & 1986-11-19\\\hline
ep0297131\footnote{/patents/txt/ep/0297/131} & Address transform method and apparatus for transferring addresses & HONEYWELL BULL (US) & 1987-01-07\\\hline
ep0408002\footnote{/patents/txt/ep/0408/002} & A programmable semiconductor memory apparatus. & FUJITSU LTD (JP) & 1989-07-11\\\hline
ep0378718\footnote{/patents/txt/ep/0378/718} & System for stabilizing dimensional properties of cured composite structures. & FORD AEROSPACE CORP (US); HARTFORD STEAM BOILER INSPECTI (US) & 1986-01-21\\\hline
ep0646926\footnote{/patents/txt/ep/0646/926} & Edge transition detection disable circuit to alter memory device operating characteristics. & SGS THOMSON MICROELECTRONICS (US) & 1993-09-30\\\hline
ep0377706\footnote{/patents/txt/ep/0377/706} & System and method for providing for secure encryptor key management & NCR CO (US) & 1988-06-17\\\hline
ep0474275\footnote{/patents/txt/ep/0474/275} & Automatic test equipment system using pin slice architecture. & SCHLUMBERGER TECHNOLOGIES INC (US) & 1990-09-05\\\hline
ep0472818\footnote{/patents/txt/ep/0472/818} & Built-in self test for integrated circuits. & IBM (US) & 1990-08-31\\\hline
ep0525068\footnote{/patents/txt/ep/0525/068} & INTEGRATED CIRCUIT I/O USING A HIGH PREFORMANCE BUS INTERFACE & RAMBUS INC (US) & 1990-04-18\\\hline
ep0261859\footnote{/patents/txt/ep/0261/859} & IC input circuitry programmable for realizing multiple functions from a single input & ADVANCED MICRO DEVICES INC (US) & 1986-09-23\\\hline
ep0229615\footnote{/patents/txt/ep/0229/615} & Flow meter. & ZIEGLER HORST & 1986-01-13\\\hline
ep0859985\footnote{/patents/txt/ep/0859/985} & Local and zone time data conversion processing for date-dependent information which spans one or two centuries & TURN OF THE CENTURY SOLUTION (US) & 1995-11-14\\\hline
ep0492326\footnote{/patents/txt/ep/0492/326} & Test carrier analysis system & BOEHRINGER MANNHEIM GMBH (DE) & 1990-12-27\\\hline
ep0267628\footnote{/patents/txt/ep/0267/628} & Microprocessor with a cache memory in which validity flags for first and second data areas are simultaneously readable & HITACHI LTD (JP); HITACHI MICROCUMPUTER ENG (JP) & 1986-11-14\\\hline
ep0635790\footnote{/patents/txt/ep/0635/790} & Client/server based secure timekeeping system. & IBM (US) & 1993-07-22\\\hline
ep0471148\footnote{/patents/txt/ep/0471/148} & Device for disconnecting and/or connecting of bus lines at a computer and circuit for a multiuser/multitask system with graphic capability. & TECHNOSALES COMPANY ESTABLISHM (LI) & 1990-08-16\\\hline
ep0834106\footnote{/patents/txt/ep/0834/106} & COMPUTER SYSTEM WITH VIDEO DISPLAY CONTROLLER HAVING POWER SAVING MODES & S MOS SYSTEMS INC (US) & 1995-06-07\\\hline
ep0187945\footnote{/patents/txt/ep/0187/945} & Method for the transmission of signals between a data exchange and a telephone exchange, especially a private branch exchange. & SIEMENS AG (DE) & 1985-01-17\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

