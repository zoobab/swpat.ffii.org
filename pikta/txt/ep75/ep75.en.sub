\begin{subdocument}{swpiktxt75}{Top Software Probability Batch nr 75: 7401-7500}{http://swpat.ffii.org/patents/txt/ep75/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0241381\footnote{/patents/txt/ep/0241/381} & Process for compression of a data set & BERRUYER YVES (FR) & 1986-04-10\\\hline
ep0553361\footnote{/patents/txt/ep/0553/361} & IMAGE DATA COMPRESSION APPARATUS & TOKYO SHIBAURA ELECTRIC CO (JP) & 1991-08-21\\\hline
ep0601819\footnote{/patents/txt/ep/0601/819} & Image compression apparatus. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1992-12-09\\\hline
ep0192966\footnote{/patents/txt/ep/0192/966} & Method for transmitting data signals between subscriber stations of a data exchange. & SIEMENS AG (DE) & 1985-02-20\\\hline
ep0189734\footnote{/patents/txt/ep/0189/734} & Method of, and apparatus for, transforming a digital data sequence into an encoded form & BORER ELECTRONICS AG (CH) & 1984-11-02\\\hline
ep0252511\footnote{/patents/txt/ep/0252/511} & Method and arrangement to combine variable-length operands in data processors. & SIEMENS AG (DE) & 1986-07-11\\\hline
ep0489993\footnote{/patents/txt/ep/0489/993} & Data throughput enhancement. & MOTOROLA INC (US) & 1989-12-05\\\hline
ep0488676\footnote{/patents/txt/ep/0488/676} & Data disc and method for retrieving data stored thereon. & SONY CORP (JP) & 1990-11-28\\\hline
ep0537361\footnote{/patents/txt/ep/0537/361} & High efficiency digital data encoding and decoding apparatus & SONY CORP (JP) & 1991-03-29\\\hline
ep0574074\footnote{/patents/txt/ep/0574/074} & Data storage unit. & KONINKL PHILIPS ELECTRONICS NV (NL) & 1992-06-09\\\hline
ep0658261\footnote{/patents/txt/ep/0658/261} & Procedure and equipment for production of line data & EDS ELECTRONIC DATA SYSTEMS FE (DE) & 1993-08-26\\\hline
ep0237382\footnote{/patents/txt/ep/0237/382} & Discrete cosine transformer & DUHAMEL PIERRE (FR) & 1986-02-06\\\hline
ep0546795\footnote{/patents/txt/ep/0546/795} & Digital modulator or demodulator circuit. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1991-12-09\\\hline
ep0451036\footnote{/patents/txt/ep/0451/036} & A document acknowledge system having horizontal/vertical-run length smoothing algorithm circuits and a document region divide circuit & GOLD STAR CO (KR) & 1990-08-27\\\hline
ep0324445\footnote{/patents/txt/ep/0324/445} & Method and apparatus for formatting document. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1988-01-11\\\hline
ep0536040\footnote{/patents/txt/ep/0536/040} & Method and system for recording a new coded message in place of an old coded message recorded on a magnetic stripe carried by a support document by manual displacement of the document & CGA HBS (FR) & 1991-10-04\\\hline
ep0654435\footnote{/patents/txt/ep/0654/435} & Method, apparatus and container for shipping printed matter & GRAPHA HOLDING AG (CH) & 1993-10-27\\\hline
ep0633703\footnote{/patents/txt/ep/0633/703} & Recording and/or encoding of video signals. & SONY CORP (JP) & 1993-07-08\\\hline
ep0239760\footnote{/patents/txt/ep/0239/760} & Method for selecting the desired language information at the reception of television signals having a plurality of sound channels. & GRUNDIG EMV (DE) & 1986-03-29\\\hline
ep0557552\footnote{/patents/txt/ep/0557/552} & Method and apparatus for erasing information from a magnetic tape. & TANDBERG DATA (NO) & 1992-02-26\\\hline
ep0555316\footnote{/patents/txt/ep/0555/316} & METHOD FOR AUTHORING, FORMATING AND PRESENTING INFORMATION TO BE LEARNED OR MEMORIZED & KAHN RICHARD G (US) & 1990-10-25\\\hline
ep0548414\footnote{/patents/txt/ep/0548/414} & Logical machine for processing control information of telecommunication transmission frames & ALCATEL NV (NL); BELL TELEPHONE MFG (BE) & 1991-12-24\\\hline
ep0196083\footnote{/patents/txt/ep/0196/083} & Logic circuit & TOKYO SHIBAURA ELECTRIC CO (JP) & 1985-03-26\\\hline
ep0352011\footnote{/patents/txt/ep/0352/011} & Method for establishing pixel colour probabilities for use in OCR logic. & IBM (US) & 1988-07-19\\\hline
ep0596078\footnote{/patents/txt/ep/0596/078} & Telecommunication device for the deaf with interrupt and pseudo-duplex capability & ULTRATEC INC (US) & 1992-05-20\\\hline
ep0482741\footnote{/patents/txt/ep/0482/741} & Database search processor. & INT COMPUTERS LTD (GB) & 1990-10-24\\\hline
ep0374512\footnote{/patents/txt/ep/0374/512} & Monitoring database objects & HEWLETT PACKARD CO (US) & 1988-12-19\\\hline
ep0224639\footnote{/patents/txt/ep/0224/639} & Method for controlling memory access on a chip card and apparatus for carrying out the method & SIEMENS AG (DE) & 1985-07-08\\\hline
ep0577967\footnote{/patents/txt/ep/0577/967} & Integrated circuit memory. & MOTOROLA INC (US) & 1992-07-06\\\hline
ep0708447\footnote{/patents/txt/ep/0708/447} & Memory insensitive to disturbances & THOMSON CSF SEMICONDUCTEURS (FR) & 1994-10-18\\\hline
ep0121126\footnote{/patents/txt/ep/0121/126} & Character generator & IBM (US) & 1983-03-07\\\hline
ep0169010\footnote{/patents/txt/ep/0169/010} & Systolic array for solving cyclic loop dependent algorithms & FORD AEROSPACE \& COMMUNICATION (US) & 1984-07-13\\\hline
ep0473186\footnote{/patents/txt/ep/0473/186} & Dictionary generator for machine translation system. & FUJITSU LTD (JP) & 1990-08-31\\\hline
ep0198216\footnote{/patents/txt/ep/0198/216} & Three phased pipelined signal processor & IBM (US) & 1985-04-15\\\hline
ep0196794\footnote{/patents/txt/ep/0196/794} & Output apparatus with suppression of underscoring of blank characters & CANON KK (JP) & 1985-03-05\\\hline
ep0296835\footnote{/patents/txt/ep/0296/835} & Colour printer apparatus. & CANON KK (JP) & 1987-06-26\\\hline
ep0449255\footnote{/patents/txt/ep/0449/255} & Micro controller. & TOSHIBA MICRO ELECTRONICS (JP); TOKYO SHIBAURA ELECTRIC CO (JP) & 1990-03-27\\\hline
ep0488128\footnote{/patents/txt/ep/0488/128} & Printing apparatus. & CANON KK (JP) & 1990-11-26\\\hline
ep0506577\footnote{/patents/txt/ep/0506/577} & Circuit managing numbers of accesses to logic resources & FRANCE TELECOM (FR) & 1991-03-29\\\hline
ep0319178\footnote{/patents/txt/ep/0319/178} & Speech synthesis. & BRITISH TELECOMM (GB) & 1987-11-19\\\hline
ep0192071\footnote{/patents/txt/ep/0192/071} & Method of determining the duration of the existence of a connection. & SIEMENS AG (DE) & 1985-02-20\\\hline
ep0298201\footnote{/patents/txt/ep/0298/201} & Disc cartridge. & YAMAHA CORP (JP) & 1984-06-01\\\hline
ep0378939\footnote{/patents/txt/ep/0378/939} & Rotory seal with integrated magnetic coder, especially for rolling bearings with sensors. & ROULEMENTS SOC NOUVELLE (FR) & 1989-04-21\\\hline
ep0418189\footnote{/patents/txt/ep/0418/189} & Gas turbine stall/surge identification and recovery. & UNITED TECHNOLOGIES CORP (US) & 1989-09-15\\\hline
ep0361142\footnote{/patents/txt/ep/0361/142} & Universal standard bus. & SIEMENS AG (DE) & 1988-09-29\\\hline
ep0516572\footnote{/patents/txt/ep/0516/572} & Process for the pseudo-absolute determination of the angular position of a shaft and an autonomous sensor for performing the process. & HOHNER AUTOMATION SA (FR); ADVANCED TECH RES (FR) & 1991-05-31\\\hline
ep0403004\footnote{/patents/txt/ep/0403/004} & Teletext decoders. & PHILIPS ELECTRONICS UK LTD (GB); PHILIPS NV (NL) & 1989-06-16\\\hline
ep0400551\footnote{/patents/txt/ep/0400/551} & Coded transmission system with initializing sequence. & FUJITSU LTD (JP) & 1989-05-27\\\hline
ep0607810\footnote{/patents/txt/ep/0607/810} & Tuning and memorising method compatible with a plurality of television signal receivers. & EDICO SRL (IT) & 1993-01-22\\\hline
ep0642760\footnote{/patents/txt/ep/0642/760} & ELECTRONIC BLOOD PRESSURE MEASURING INSTRUMENT. & OSACHI CO LTD (JP); NAGANO KEN (JP) & 1993-04-02\\\hline
ep0725542\footnote{/patents/txt/ep/0725/542} & Television devices & SONY CORP (JP) & 1995-02-03\\\hline
ep0676160\footnote{/patents/txt/ep/0676/160} & Device for exhibiting flat articles & HAHN GLASBAU (DE) & 1994-04-01\\\hline
ep0526621\footnote{/patents/txt/ep/0526/621} & METHOD OF CONSTRUCTING A CONSTANT-FOLDING MECHANISM IN A MULTILANGUAGE OPTIMIZING COMPILER & DIGITAL EQUIPMENT CORP (US) & 1991-02-27\\\hline
ep0606322\footnote{/patents/txt/ep/0606/322} & BROADBAND INPUT BUFFERED ATM SWITCH & NORTHERN TELECOM LTD (CA) & 1991-10-03\\\hline
ep0390162\footnote{/patents/txt/ep/0390/162} & An apparatus for reading a bar code. & FUJITSU LTD (JP) & 1989-03-29\\\hline
ep0411526\footnote{/patents/txt/ep/0411/526} & Device for the correction of transmission distorsions of a data signal according to transmission code violations. & ALCATEL BUSINESS SYSTEMS (FR) & 1989-08-04\\\hline
ep0529049\footnote{/patents/txt/ep/0529/049} & METHOD OF CONSTRUCTING A CONSTANT-FOLDING MECHANISM IN A MULTILANGUAGE OPTIMIZING COMPILER & DIGITAL EQUIPMENT CORP (US) & 1991-02-27\\\hline
ep0437002\footnote{/patents/txt/ep/0437/002} & Region and texture coding. & PHILIPS ELECTRONICS UK LTD (GB); KONINKL PHILIPS ELECTRONICS NV (NL) & 1990-11-30\\\hline
ep0544258\footnote{/patents/txt/ep/0544/258} & Image data compression method. & EZEL INC (JP) & 1991-11-27\\\hline
ep0199908\footnote{/patents/txt/ep/0199/908} & Definition of line unit size & IBM (US) & 1985-04-16\\\hline
ep0190622\footnote{/patents/txt/ep/0190/622} & Generating storage reference instructions in an optimizing compiler & IBM (US) & 1985-02-04\\\hline
ep0522332\footnote{/patents/txt/ep/0522/332} & Computer for the control station of a machine, more particularly a printing press & SCHONFELD FRANK (DE); RUPPERTZ MICHAEL (DE); SCHEBESTA DIETRICH (DE) & 1991-07-12\\\hline
ep0205009\footnote{/patents/txt/ep/0205/009} & Byte-wide encoder and decoder system for RLL (1,7) code & IBM (US) & 1985-06-13\\\hline
ep0365309\footnote{/patents/txt/ep/0365/309} & A data unification system. & XEROX CORP (US) & 1988-10-19\\\hline
ep0389697\footnote{/patents/txt/ep/0389/697} & Serial data receiver. & IBM (US) & 1989-03-29\\\hline
ep0483874\footnote{/patents/txt/ep/0483/874} & Apparatus for recording and reproducing digital picture data. & SONY CORP (JP) & 1990-11-13\\\hline
ep0554056\footnote{/patents/txt/ep/0554/056} & Frequency division multiplexed transmission of synchronous and start-stop data. & FUJITSU LTD (JP) & 1992-01-27\\\hline
ep0715288\footnote{/patents/txt/ep/0715/288} & Method and device for reducing the amount of data to be transmitted from vehicles of a fleet of sample vehicles & MANNESMANN AG (DE) & 1995-06-09\\\hline
ep0733220\footnote{/patents/txt/ep/0733/220} & Data medium for identifying objects, scanning device for interrogation of the data medium and process for controlling the data medium & SIEMENS AG AUSTRIA (AT) & 1993-12-10\\\hline
ep0855803\footnote{/patents/txt/ep/0855/803} & Method and system for compressing data based upon context tree algorithm & EINDHOVEN TECH HOCHSCHULE (NL); NEDERLAND PTT (NL) & 1997-01-24\\\hline
ep0242509\footnote{/patents/txt/ep/0242/509} & Apparatus, method and algorithm for encoding and decoding characters in a message to be transmitted between a first system and a second system & IBM (US) & 1986-03-10\\\hline
ep0318374\footnote{/patents/txt/ep/0318/374} & System for recording and/or transmitting binary information at a high data rate via known means for recording and/or transmitting video information, and for decoding the digital data & DROPSY PATRICK J (FR) & 1987-11-25\\\hline
ep0613107\footnote{/patents/txt/ep/0613/107} & Document deposit apparatus. & INTER INNOVATION AB (SE) & 1993-02-23\\\hline
ep0766858\footnote{/patents/txt/ep/0766/858} & PRINTER FOR TYPING DESIRED CHARACTERS ON MANUALLY ENTERED DOCUMENTS, SUCH AS TRAVEL AND TRANSPORT DOCUMENTS, INVOICES, ETC., WHILE IN MOVEMENT & IS ITALSERVICE ELECTRONIC EQUI (IT); CHIERUZZI ROBERTO (IT); TEGHESI ALFREDO (IT) & 1994-04-15\\\hline
ep0385717\footnote{/patents/txt/ep/0385/717} & Pattern image processing apparatus & TOKYO SHIBAURA ELECTRIC CO (JP) & 1989-02-28\\\hline
ep0434429\footnote{/patents/txt/ep/0434/429} & Image processing apparatus. & CANON KK (JP) & 1989-12-21\\\hline
ep0510933\footnote{/patents/txt/ep/0510/933} & Image processing apparatus and method. & CANON KK (JP) & 1991-04-23\\\hline
ep0513156\footnote{/patents/txt/ep/0513/156} & Image handling facilitating computer aided design and manufacture of documents & DE LA RUE THOMAS \& CO LTD (GB) & 1991-01-31\\\hline
ep0686939\footnote{/patents/txt/ep/0686/939} & Image display apparatus & HITACHI LTD (JP) & 1994-06-10\\\hline
ep0667971\footnote{/patents/txt/ep/0667/971} & Method and device for acknowledgement of transmitted information & PRICER INC (US) & 1993-11-08\\\hline
ep0208192\footnote{/patents/txt/ep/0208/192} & Call instruction for ring crossing architecture & HONEYWELL INF SYSTEMS (US) & 1985-06-27\\\hline
ep0166045\footnote{/patents/txt/ep/0166/045} & Graphics display terminal and method of storing alphanumeric data therein & IBM (US) & 1984-06-25\\\hline
ep0075733\footnote{/patents/txt/ep/0075/733} & Method for performing a multiple page get operation in a text processing system & IBM (US) & 1981-09-24\\\hline
ep0140957\footnote{/patents/txt/ep/0140/957} & Audio digital recording and playback system & COMPUSOUND INC (US) & 1983-04-19\\\hline
ep0461811\footnote{/patents/txt/ep/0461/811} & Pattern processing method. & CANON KK (JP) & 1990-06-11\\\hline
ep0321547\footnote{/patents/txt/ep/0321/547} & Electronic collation & EASTMAN KODAK CO (US) & 1987-06-19\\\hline
ep0208453\footnote{/patents/txt/ep/0208/453} & Operator interactive device verification system & LIGHT SIGNATURES INC (US) & 1985-07-10\\\hline
ep0246898\footnote{/patents/txt/ep/0246/898} & Method of compressing character or pictorial image data using curve approximation & FUJITSU LTD (JP) & 1986-09-18\\\hline
ep0528008\footnote{/patents/txt/ep/0528/008} & METHOD OF CONSTRUCTING A CONSTANT-FOLDING MECHANISM IN A MULTILANGUAGE OPTIMIZING COMPILER & DIGITAL EQUIPMENT CORP (US) & 1991-02-27\\\hline
ep0526622\footnote{/patents/txt/ep/0526/622} & METHOD OF CONSTRUCTING A CONSTANT-FOLDING MECHANISM IN A MULTILANGUAGE OPTIMIZING COMPILER & DIGITAL EQUIPMENT CORP (US) & 1991-02-27\\\hline
ep0532731\footnote{/patents/txt/ep/0532/731} & METHOD OF CONSTRUCTING A CONSTANT-FOLDING MECHANISM IN A MULTILANGUAGE OPTIMIZING COMPILER & DIGITAL EQUIPMENT CORP (US) & 1991-02-27\\\hline
ep0597706\footnote{/patents/txt/ep/0597/706} & Solid state peripheral storage device. & SILICON STORAGE TECH INC (US) & 1992-11-13\\\hline
ep0720157\footnote{/patents/txt/ep/0720/157} & Video compact disc drive & MITSUMI ELECTRIC CO (JP) & 1994-12-29\\\hline
ep0171579\footnote{/patents/txt/ep/0171/579} & Arrangement for the serial transmission of measured values of at least one transducer. & STEGMANN UHREN ELEKTRO (DE) & 1984-07-13\\\hline
ep0315983\footnote{/patents/txt/ep/0315/983} & Longitudinal differential protection system. & ASEA BROWN BOVERI (SE) & 1987-11-12\\\hline
ep0294133\footnote{/patents/txt/ep/0294/133} & Protocols for very high-speed optical LANs. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1987-06-05\\\hline
ep0343741\footnote{/patents/txt/ep/0343/741} & Teletext decoders. & PHILIPS ELECTRONICS UK LTD (GB); PHILIPS NV (NL) & 1989-05-05\\\hline
ep0436472\footnote{/patents/txt/ep/0436/472} & System for transmitting and receiving television signals. & SISVEL SPA (IT) & 1998-06-22\\\hline
ep0555017\footnote{/patents/txt/ep/0555/017} & Geometric vector quantization. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1992-02-07\\\hline
ep0257162\footnote{/patents/txt/ep/0257/162} & Device for the composition of colour component signals from luminance and chrominance signals and video display device comprising the application thereof. & TEXAS INSTRUMENTS FRANCE (FR); TEXAS INSTRUMENTS INC (US) & 1986-08-06\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

