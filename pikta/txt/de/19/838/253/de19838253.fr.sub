\begin{subdocument}{de19838253}{Mehr Sicherheit durch abwechselnde physische Trennung eines Vermittlungsrechners von beiden Seiten}{http://swpat.ffii.org/pikta/mupli/de19838253/index.fr.html}{Groupe de travail\\\url{swpatag@ffii.org}\\version fran\c{c}aise 2005/01/06 par Gerald SEDRATI-DINET\footnote{\url{http://gibuskro.lautre.net/}}}{Pour mieux prot\'{e}ger un internet contre des attaques hacker, le mur de feu et toujours coup\'{e} sur un des deux cot\'{e}s.  Ainsi une communication en deux directions est maintenue par alternation rapide.  Le propri\'{e}taire de ce brevet affirme que ce principe simple peut procurer une s\'{e}curit\'{e} absolue a un intranet, tandis que les murs de feu actuelle procurent seulement une s\'{e}curit\'{e} relative.  M\^{e}me si cette assertion semble douteuse, tous ceux qui veulent faire fonctionner ce principe doit obtenir une permission de Fraunhofer.}
\begin{sect}{abs}{Feuille de Donn\'{e}es}
\begin{description}
\item[classe EC:]\ H04L9/32S
\item[classe IPC:]\ H04M11/00 ; H04L9/30 ; H04Q7/32
\item[date d'ant\'{e}riorit\'{e}:]\ DE19971047603 19971028
\item[identifiant du d\'{e}p\^{o}t:]\ DE19971047603 19971028
\item[demand\'{e}:]\ [\_]  DE19747603
\item[postulant:]\ BROKAT INFORMATIONSSYSTEME GMB (DE)
\item[inventeur:]\ ROEVER STEFAN (DE); GROFFMANN HANS-DIETER DR (DE)
\item[date de publication:]\ 1999-05-20
\item[num\'{e}ro de brevet:]\ DE19747603
\item[D\'{e}signation de l'invention:]\ \#f
\item[d\'{e}p\^{o}t de brevet (A):]\ revendications\footnote{\url{claims.html}}, desciption\footnote{\url{desc.html}} et r\'{e}sum\'{e}\footnote{\url{abstract.html}}
\item[version accord\'{e}e (B1):]\ L'OEB publie le brevet final sous la forme d'un recueil de  fichiers images.  Pour les rendre plus lisibles, nous les avons r\'{e}unis  dans un seul gros fichier PDF\footnote{\url{de19838253.pdf}} et par un processus de  reconnaissance optique de caract\`{e}res\footnote{\url{http://localhost/swpat/girzu/epatext/index.en.html}}, les avons r\'{e}duits en un  fichier texte\footnote{\url{de19838253.txt}}.
\end{description}
\end{sect}

\begin{sect}{clms}{Revendications}
{\ol <li>
Datenverbindung zwischen einem ersten Rechner (1) und einem zweiten
Rechner (2) zum Zweck der Daten\"{u}bertragung, dadurch gekennzeichnet,
dass in der Datenverbindung ein Schleusenelement (6) angeordnet ist,
wobei zwischen dem ersten Rechner (1) und dem Schleusenelement (6) ein
erstes Schleusentor (7) und zwischen dem zweiten Rechner (2) und dem
Schleusenelement (6) ein zweites Schleusentor (8) angeordnet ist, und
wobei das erste Schleusentor (7) geschlossen ist, wenn das zweite
Schleusentor (8) ge\"{o}ffnet ist und umgekehrt das zweite Schleusentor
(8) geschlossen ist, wenn das erste Schleusentor (7) ge\"{o}ffnet ist.

<li>
Datenverbindung nach Anspruch 1, dadurch gekennzeichnet, dass der
erste Rechner (1) in einem ersten Rechnernetzwerk (9) angeordnet ist.

<li>
Datenverbindung nach Anspruch 1 oder 2, dadurch gekennzeichnet,
dass der zweite Rechner (2) in einem zweiten Rechnernetzwerk (10)
angeordnet ist.

<li>
Datenverbindung nach Anspruch 2 oder 3, dadurch gekennzeichnet,
dass der erste Rechner (1) als ein Server eines Rechnernetzes und das
erste Rechnernetzwerk (9) als ein internes, unternehmensweites
Rechnernetz ausgebildet ist.

<li>
Datenverbindung nach Anspruch 3 oder 4, dadurch gekennzeichnet,
dass der zweite Rechner (2) als ein Internet-Server ausgebildet ist
und das zweite Rechnernetzwerk (10) das Internet ist.

<li>
Datenverbindung nach Anspruch 4 oder 5, dadurch gekennzeichnet,
dass in dem ersten Rechnernetzwerk (9) ein dritter Rechner (3) und in
dem Schleusenelement (6) ein vierter Rechner (4) angeordnet ist.

<li>
Datenverbindung nach einem der Anspr\"{u}che 1 bis 6, dadurch
gekennzeichnet, dass die Verbindung zwischen dem ersten Rechner (1)
und dem zweiten Rechner (2) als eine
Integrated-Services-Digital-Network(ISDN)-Verbindung nach dem
Net-Terminal-Basis-Adapter(NTBA)-Standard ausgebildet ist.

<li>
Verfahren zum \"{U}bertragen von Daten zwischen einem ersten Rechner
(1) zu einem zweiten Rechner (2) \"{u}ber eine Datenverbindung, dadurch
gekennzeichnet, dass die Daten in der einen Richtung von dem ersten
Rechner (1) \"{u}ber ein ge\"{o}ffnetes erstes Schleusentor (7) in ein
Schleusenelement (6) \"{u}bertragen werden, das erste Schleusentor (7)
geschlossen und dann ein zweites Schleusentor (8) ge\"{o}ffnet wird und
die Daten \"{u}ber das ge\"{o}ffnete zweite Schleusentor (8) zu dem zweiten
Rechner (2) und in der entgegengesetzten Richtung in umgekehrter
Reihenfolge \"{u}bertragen werden.

<li>
Verfahren nach Anspruch 8, dadurch gekennzeichnet, dass die Daten
von dem ersten Rechner (1) \"{u}ber einen dritten Rechner (3), der mit dem
ersten Rechner (1) in einem gemeinsamen Rechnernetzwerk (7) angeordnet
ist, und \"{u}ber das Schleusenelement (6) an den zweiten Rechner (2) und
umgekehrt \"{u}bertragen werden.

<li>
Verfahren nach Anspruch 9, dadurch gekennzeichnet, dass zum Aufbau
einer Datenverbindung und zur Daten\"{u}bertragung zwischen dem dritten
Rechner (3) und dem Schleusenelement (6) beide B-Kan\"{a}le einer
ISDN-Verbindung nach dem Net-Terminal-Basis-Adapter(NTBA)-Standard
verwendet werden.

<li>
Verfahren nach Anspruch 9 oder 10, dadurch gekennzeichnet, dass in
dem dritten Rechner (3) eine Analyse der zu \"{u}bertragenden Daten
durchgef\"{u}hrt wird.

<li>
Verfahren nach Anspruch 11, dadurch gekennzeichnet, dass die
Analyse nach semantischen Gesichtspunkten erfolgt.

<li>
Verfahren nach Anspruch 11 oder 12, dadurch gekennzeichnet, dass
die Tiefe und die Dauer der Analyse individuell eingestellt werden.

<li>
Verfahren nach einem der Anspr\"{u}che 10 bis 13, dadurch
gekennzeichnet, dass zum Senden von Daten
- die zu sendenden Daten von dem ersten Rechner (1) zu dem dritten
Rechner (3) gesendet werden,
- der dritte Rechner (3) die Daten analysiert und \"{u}berpr\"{u}ft,
- das erste Schleusentor (7) ge\"{o}ffnet wird,
- die Daten von dem dritten Rechner (3) zu dem Schleusenelement (6)
gesendet werden,
- das erste Schleusentor (7) geschlossen wird,
- das zweite Schleusentor (8) ge\"{o}ffnet wird,- die Daten von dem
Schleusentor (6) an den zweiten Rechner (2) gesendet werden, und
- das zweite Schleusentor (8) geschlossen wird.

<li>
Verfahren nach einem der Anspr\"{u}che 10 bis 13, dadurch
gekennzeichnet, dass zum Empfangen von Daten
- das zweite Schleusentor (8) ge\"{o}ffnet wird,
- die zu empfangenden Daten von dem zweiten Rechner (2) zu dem
Schleusenelement (6) gesendet werden,
- das zweite Schleusentor (8) geschlossen wird,
- das erste Schleusentor (7) ge\"{o}ffnet wird,
- die Daten von dem Schleusenelement (6) zu dem dritten Rechner (3)
gesendet werden,
- das erste Schleusentor (7) geschlossen wird,
- der dritte Rechner (3) die Daten analysiert und \"{u}berpr\"{u}ft und
- die Daten von dem dritten Rechner (3) an den ersten Rechner (1)
gesendet werden.

<li>
Verfahren nach Anspruch 14 oder 15, dadurch gekennzeichnet, dass
die zu empfangenden Daten von dem Schleusenelement (6) zu dem dritten
Rechner (3) zu dem Zeitpunkt gesendet werden, zu dem auch die zu
sendenden Daten von dem dritten Rechner (3) zu dem Schleusenelement
(6) gesendet werden.

<li>
Verfahren nach einem der Anspr\"{u}che 14 bis 16, dadurch
gekennzeichnet, dass die zu empfangenden Daten von dem zweiten Rechner
(2) zu dem Schleusenelement (6) zu dem Zeitpunkt gesendet werden, zu
dem auch die zu sendenden Daten von dem Schleusenelement (6) zu dem
zweiten Rechner (2) gesendet werden.

<li>
Verfahren nach einem der Anspr\"{u}che 14 bis 17, dadurch
gekennzeichnet, dass die Analyse der empfangenen Daten zeitgleich mit
der Analyse der zu sendenden Daten durchgef\"{u}hrt wird.

}
\end{sect}

\begin{sect}{desc}{description}
\begin{verbatim}
Die vorliegende Erfindung betrifft eine Datenverbindung zwischen einem
ersten Rechner und einem zweiten Rechner zum Zweck der
Daten\"{u}bertragung. Ausserdem betrifft sie ein Verfahren zum \"{U}bertragen
von Daten zwischen einem ersten Rechner zu einem zweiten Rechner \"{u}ber
eine Datenverbindung.

Der erste Rechner kann bspw. Teil eines internen unternehmensweiten
Rechnernetzwerkes sein. Der zweite Rechner k\"{o}nnte als ein Rechner im
weltumspannenden Internet ausgebildet sein.

Jede Daten\"{u}bertragung zwischen zwei Rechnern wirft Fragen nach der
Sicherheit einer solchen \"{U}bertragung gegen unbefugtes Mith\"{o}ren oder
gar Manipulieren der Daten oder des \"{U}bertragungsvorgangs durch
unberechtigte Dritte auf. F\"{u}r Unternehmen, Banken und Beh\"{o}rden kommt
ein weiterer Sicherheitsaspekt hinzu, wenn das eigene
unternehmensweite Rechnernetz gegen Angriffe von ausserhalb, bspw. aus
dem Internet, zu sch\"{u}tzen ist. Insbesondere dann, wenn eine aktive
Verbindung zwischen Unternehmen und Internet besteht, kann durch
unbefugte Zugriffe auf das interne Rechnernetz eines Unternehmens die
Datensicherheit in diesem Rechnernetz gef\"{a}hrdet werden.

Aus dem Stand der Technik sind eine Vielzahl von unterschiedlich
ausgestalteten Datenverbindungen zur Daten\"{u}bertragung bekannt. Zur
L\"{o}sung der Sicherheitsproblematik werden sog. Firewalls eingesetzt.
Bei den Firewalls werden die (TCP/IP-)Datenpakete analysiert,
unberechtigte Zugriffe verwehrt und berechtigte Anforderungen
zugelassen. Die Firewalls stellen jedoch keine physikalische Trennung
zwischen dem internen Rechnernetz eines Unternehmens und der
Aussenwelt her. Durch Manipulation der Firewalls von ausserhalb oder
durch anderweitig unberechtigten Zugriff ist es deshalb nach wie vor
m\"{o}glich, sich von ausserhalb Zugang zu dem internen Rechnernetz zu
verschaffen und die Datensicherheit in dem unternehmensweiten
Rechnernetz zu gef\"{a}hrden. Wenn die Sicherheitsbed\"{u}rfnisse eines
Unternehmens besonders hoch sind, k\"{o}nnen die bekannten Firewalls keine
ausreichende Sicherheit bieten.

Es ist deshalb die Aufgabe der vorliegenden Erfindung, eine
Datenverbindung der eingangs genannten Art dahingehend auszugestalten
und weiterzubilden, dass es unberechtigten Dritten nicht m\"{o}glich ist,
sich w\"{a}hrend einer Daten\"{u}bertragung durch unberechtigten Zugriff
Zugang zu dem ersten Rechner zu verschaffen und dort die Daten zu
manipulieren.

Zur L\"{o}sung dieser Aufgabe schl\"{a}gt die Erfindung ausgehend von der
Datenverbindung der eingangs genannten Art vor, dass in der
Datenverbindung ein Schleusenelement angeordnet ist, wobei zwischen
dem ersten Rechner und dem Schleusenelement ein erstes Schleusentor
(inner flood-gate, IFG) und zwischen dem zweiten Rechner und dem
Schleusenelement ein zweites Schleusentor (outer flood-gate, OFG)
angeordnet ist, und wobei das erste Schleusentor geschlossen ist, wenn
das zweite Schleusentor ge\"{o}ffnet ist und umgekehrt das zweite
Schleusentor geschlossen ist, wenn das erste Schleusentor ge\"{o}ffnet
ist.

Erfindungsgem\"{a}ss ist erkannt worden, dass erst durch eine
physikalische Trennung der beiden Rechner w\"{a}hrend der Daten\"{u}bertragung
ein Zugriff auf einen der Rechner von aussen durch einen
unberechtigten Dritten wirksam und zuverl\"{a}ssig verhindert werden kann.
Durch das Schleusenelement erfolgt eine physikalische Trennung der
beiden Rechner voneinander. Zu keinem Zeitpunkt der Daten\"{u}bertragung
sind die beiden Rechner miteinander verbunden, sondern je nach Zustand
der Schleusentore findet der Informationsaustausch im Rahmen der
Daten\"{u}bertragung nur jeweils mit einer Seite der Kommunikationspartner
statt. Dadurch k\"{o}nnen mit vergleichsweise geringem Aufwand h\"{o}chste
Sicherheitsvorgaben erf\"{u}llt werden.

Das Schleusenelement ist bspw. als ein Rechner ausgebildet. Die
erfindungsgem\"{a}sse Datenverbindung f\"{u}hrt zu einer geringen, f\"{u}r einen
Anwender kaum bemerkbaren Zeitverz\"{o}gerung bei der Daten\"{u}bertragung.
W\"{a}hrend dieser Zeitverz\"{o}gerung werden die Schleusentore ge\"{o}ffnet und
geschlossen und die zu \"{u}bertragenden Daten analysiert. Durch eine
geeignete Ablaufsteuerung der einzelnen Schritte der Daten\"{u}bertragung
kann die Zeitverz\"{o}gerung auf ein Minimum reduziert werden.

Gem\"{a}ss einer vorteilhaften Weiterbildung der Erfindung wird
vorgeschlagen, dass der erste Rechner in einem ersten Rechnernetzwerk
angeordnet ist. Der erste Rechner ist vorzugsweise als ein Server
eines Rechnernetzes und das erste Rechnernetzwerk als ein internes
unternehmensweites Rechnernetz ausgebildet. Bei derartigen
unternehmensinternen Rechnernetzwerken ist die Datensicherheit
besonders wichtig. Viele Unternehmen wickeln inzwischen einen
Grossteil ihrer Betriebsabl\"{a}ufe komplett elektronisch \"{u}ber ihre
internen Rechnernetze ab. Durch einen unbefugten Zugang zu diesen
Rechnernetzen von ausserhalb und durch eine Manipulation der darin
enthaltenen Daten kann einem Unternehmen sehr grosser Schaden
erwachsen. Hier sorgt die erfindungsgem\"{a}sse Datenverbindung f\"{u}r
Abhilfe.

Gem\"{a}ss einer anderen vorteilhaften Weiterbildung der
erfindungsgem\"{a}ssen Datenverbindung wird vorgeschlagen, dass der zweite
Rechner in einem zweiten Rechnernetzwerk angeordnet ist. Der zweite
Rechner ist vorzugsweise als ein Internet-Server ausgebildet und das
zweite Rechnernetzwerk ist das Internet. Die Angriffe von Dritten \"{u}ber
das Internet auf an das Internet angeschlossene unternehmensinterne
Rechnernetzwerke stellen eine besonders grosse Gefahr f\"{u}r die
Datensicherheit in solchen Unternehmen dar.

Bei der Daten\"{u}bertragung von einem Rechner eines internen
Rechnernetzes zu einem Rechner des Internets ist die Datensicherheit
von ganz besonderer Bedeutung, da theoretisch viele Millionen von
Internetnutzern unerlaubterweise in das unternehmensinterne
Rechnernetz eindringen und dort abgelegte Daten manipulieren k\"{o}nnten.
Ausserdem sind in dem weltumspannenden und f\"{u}r jedermann zug\"{a}nglichen
Internet eine Vielzahl von sog. Viren und Trojanischen Pferden in
Umlauf, die zu einer ernst zunehmenden Gefahr f\"{u}r den Datenbestand
eines Unternehmens werden k\"{o}nnen, wenn sie erst einmal in das interne
Rechnernetz des Unternehmens eingedrungen sind. Die erfindungsgem\"{a}sse
Datenverbindung bietet eine geeignete Plattform, um einen sicheren und
zuverl\"{a}ssigen Schutz vor Viren etc. zu gew\"{a}hrleisten. Dazu m\"{u}ssen in
der Datenverbindung, vorzugsweise in dem Schleusenelement, geeignete
Analysemittel angeordnet werden.

Gem\"{a}ss einer anderen vorteilhaften Weiterbildung der Erfindung wird
vorgeschlagen, dass in dem ersten Rechnernetzwerk ein dritter Rechner
und in dem Schleusenelement ein vierter Rechner angeordnet ist.
Der vierte Rechner kann sich innerhalb des Schleusenelements in einer
eigenen Netzwerkumgebung befinden, die jedoch physikalisch sowohl von
dem ersten Rechnernetzwerk als auch von dem zweiten Rechnernetzwerk
getrennt sein muss. Der Sinn des vierten Rechners besteht darin,
verschiedene Analyseprozesse innerhalb des Schleusenelements
durchzuf\"{u}hren und somit eine gewisse Vorselektion zu treffen.

Zum Senden von Daten werden die zu sendenden Daten von dem ersten
Rechner zu dem dritten Rechner gesendet. In dem dritten Rechner werden
die Daten analysiert und \"{u}berpr\"{u}ft. Die Analyse der zu sendenden Daten
kann auch auf dem ersten Rechner erfolgen. Erst wenn die \"{U}berpr\"{u}fung
keine Beanstandungen ergeben hat, wird das erste Schleusentor
ge\"{o}ffnet. Dann werden die Daten von dem dritten Rechner zu dem
Schleusenelement gesendet, und anschliessend wird das erste
Schleusentor wieder geschlossen. Erst nach vollst\"{a}ndigem Schliessen
des ersten Schleusentors wird das zweite Schleusentor ge\"{o}ffnet. Dann
werden die Daten von dem Schleusenelement an den zweiten Rechner
gesendet, und danach wird das zweite Schleusentor wieder geschlossen.
Zum Empfangen von Daten wird zun\"{a}chst das zweite Schleusentor ge\"{o}ffnet
und die zu empfangenden Daten werden von dem zweiten Rechner zu dem
Schleusenelement gesendet. Dann wird das zweite Schleusentor
geschlossen und erst wenn es vollst\"{a}ndig geschlossen ist, wird das
erste Schleusentor ge\"{o}ffnet. Anschliessend werden die Daten von dem
Schleusenelement zu dem dritten Rechner gesendet. Dann wird das erste
Schleusentor geschlossen und danach analysiert und \"{u}berpr\"{u}ft der
dritte Rechner die Daten. Erst wenn die \"{U}berpr\"{u}fung keine
Beanstandungen ergeben hat, werden die Daten von dem dritten Rechner
an den ersten Rechner gesendet.

Durch die physikalische Trennung der beiden Rechner bzw.
Rechnernetzwerke voneinander werden Online-Angriffe von aussen auf
einen der Rechner in einem Rechnernetzwerk verhindert und es ist
unm\"{o}glich, die Analyseprozesse, die in dem dritten Rechner
durchgef\"{u}hrt werden, von aussen zu manipulieren.
Vorteilhafterweise ist die Verbindung zwischen dem ersten Rechner und
dem zweiten Rechner als eine
Integrated-Services-Digital-Network(ISDN)-Verbindung nach dem
Net-Terminal-Basis-Adapter(NTBA)-Standard ausgebildet. An diese
ISDN-Verbindung wird auch der dritte Rechner angeschlossen. Der vierte
Rechner h\"{a}ngt nicht unmittelbar an dem ISDN-NTBA, da er - \"{u}ber ein
eigenes Netz - mit dem Schleusenelement verbunden ist.

Auf diese Weise kann die Funktion des Schleusenelements einfach und
wirkungsvoll realisiert werden. Eine ISDN-Verbindung nach dem
NTBA-Standard weist zwei Daten\"{u}bertragungskan\"{a}le (B-Kan\"{a}le) und einen
Steuerkanal (D-Kanal) auf. Somit gestattet ein ISDN-NTBA maximal zwei
Daten\"{u}bertragungsverbindungen gleichzeitig. Die Datenverbindung ist so
aufgebaut, dass wenn der dritte Rechner zu dem Schleusenelement eine
Verbindung zum Zwecke der Daten\"{u}bertragung aufbaut, hierf\"{u}r beide
B-Kan\"{a}le des ISDN-NTBA ben\"{o}tigt werden: \"{U}ber den einen B-Kanal erfolgt
die Anwahl des Schleusenelements, und \"{u}ber den anderen B-Kanal wird
die Daten\"{u}bertragungsverbindung zu dem Schleusenelement hergestellt
(erstes Schleusentor ge\"{o}ffnet). Eine gleichzeitige Verbindung des
Schleusenelements zu dem zweiten Rechner ist also ausgeschlossen, da
der ISDN-NTBA keinen freien B-Kanal mehr zur Verf\"{u}gung hat (zweites
Schleusentor kann nicht ge\"{o}ffnet werden).

Wenn umgekehrt \"{u}ber einen der beiden B-Kan\"{a}le bereits eine Verbindung
zwischen dem zweiten Rechner und dem Schleusenelement besteht (zweites
Schleusentor ge\"{o}ffnet), kann der dritte Rechner keine Verbindung mehr
zu dem Schleusenelement herstellen (erstes Schleusentor kann nicht
ge\"{o}ffnet werden), da dazu, wie oben erl\"{a}utert, beide B-Kan\"{a}le des
ISDN-NTBA ben\"{o}tigt werden. Durch die doppelte Verwendung desselben
NTBA, einerseits an dem dritten Rechner und andererseits an dem
Schleusenelement, kann die Schleusenfunktion der erfindungsgem\"{a}ssen
Datenverbindung auf einfache Weise realisiert werden.

Eine weitere Aufgabe der vorliegenden Erfindung besteht darin, ein
Verfahren der eingangs genannten Art dahingehend auszugestalten und
weiterzubilden, dass es unberechtigten Dritten nicht m\"{o}glich ist, die
Daten\"{u}bertragung zu beeinflussen oder sich zu einem der Rechner Zugang
zu verschaffen und dort die Daten zu manipulieren.

Zur L\"{o}sung dieser Aufgabe schl\"{a}gt die Erfindung ausgehend von dem
Verfahren der eingangs genannten Art vor, dass die Daten von dem
ersten Rechner \"{u}ber ein ge\"{o}ffnetes erstes Schleusentor in ein
Schleusenelement \"{u}bertragen werden, das erste Schleusentor geschlossen
und dann ein zweites Schleusentor ge\"{o}ffnet wird und die Daten \"{u}ber das
ge\"{o}ffnete zweite Schleusentor zu dem zweiten Rechner und in der
entgegengesetzten Richtung in umgekehrter Reihenfolge \"{u}bertragen
werden.

Vorzugsweise werden die Daten von dem ersten Rechner \"{u}ber einen
dritten Rechner, der mit dem ersten Rechner in einem gemeinsamen
Rechnernetzwerk angeordnet ist, und \"{u}ber das Schleusenelement an den
zweiten Rechner und umgekehrt \"{u}bertragen.

Vorteilhafterweise werden zum Aufbau einer Datenverbindung und zur
Daten\"{u}bertragung zwischen dem dritten Rechner und dem Schleusenelement
beide B-Kan\"{a}le einer ISDN-Verbindung nach dem
Net-Terminal-Basis-Adapter(NTBA)-Standard verwendet. Dadurch kann die
Funktion des Schleusenelements auf einfache und wirkungsvolle Weise
realisiert werden.

Gem\"{a}ss einer vorteilhaften Weiterbildung der vorliegenden Erfindung
wird vorgeschlagen, dass in dem dritten Rechner eine Analyse der zu
\"{u}bertragenden Daten durchgef\"{u}hrt wird. Vorzugsweise erfolgt die
Analyse nach semantischen Gesichtspunkten.

Der dritte Rechner steht zu keinem Zeitpunkt der Daten\"{u}bertragung mit
dem zweiten Rechnernetzwerk bzw. mit dem zweiten Rechner in direktem
Kontakt. Dies wird durch die Schleusentore verhindert, die w\"{a}hrend der
Daten\"{u}bertragung niemals beide gleichzeitig ge\"{o}ffnet sind. Somit ist
es unberechtigten Dritten nicht m\"{o}glich, w\"{a}hrend einer
Daten\"{u}bertragung einen direkten Zugriff auf den dritten Rechner zu
erhalten und den in dem dritten Rechner enthaltenen Analysemechanismus
zu manipulieren.

In dem Schleusenelement selbst findet dagegen keine Analyse der zu
\"{u}bermittelnden Daten statt, da das Schleusenelement zur \"{U}bermittlung
von Daten f\"{u}r eine bestimmte Zeitdauer in direktem Kontakt mit dem
zweiten Rechnernetzwerk bzw. mit dem zweiten Rechner steht. W\"{a}hrend
dieser Zeitdauer k\"{o}nnte ein in dem Schleusenelement enthaltener
Analysemechanismus durch unberechtigte Dritte manipuliert werden.

Wenn die Daten \"{u}ber das ge\"{o}ffnete erste Schleusentor von dem
Schleusenelement zu dem dritten Rechner gesendet werden, k\"{o}nnen zwar
infizierte Dateien, d. h. Dateien, die Vireni oder Trojanische Pferde
enthalten, in dem dritten Rechner abgelegt werden. Dennoch besteht
hier ein entscheidender Unterschied zu der Funktionsweise der
bekannten Firewalls. Anstatt online alle Analyseprozesse
durchzuf\"{u}hren, kann der dritte Rechner ohne Bedrohung durch einen
Zugriff von aussen und interaktive Manipulation die passiven Daten,
die das Schleusenelement aus dem zweiten Rechnernetzwerk erhalten hat,
je nach gew\"{u}nschter skalierbarer Analysetiefe und Analysedauer
untersuchen und ggf. vernichten.

Im Rahmen der semantischen Analyse der Daten kann \"{u}berpr\"{u}ft werden, ob
der Inhalt bestimmter Dateien das unternehmensweite Rechnernetzwerk
verlassen und nach aussen gelangen darf. Bei der semantischen Analyse
von Dateien werden insbesondere die Anlagen zu elektronischen
Nachrichten (e-Mails) \"{u}berpr\"{u}ft, da hier\"{u}ber Dokumente beliebigen Typs
versendet werden k\"{o}nnen.

Eine semantische Analyse ist bei dem erfindungsgem\"{a}ssen Verfahren
m\"{o}glich, da die Analysezeitr\"{a}ume flexibel gestaltet werden k\"{o}nnen.

Vorzugsweise erfolgt das Senden von Daten von dem ersten Rechner zu
dem zweiten Rechner in den nachfolgenden Schritten:

- Die zu sendenden Daten werden von dem ersten Rechner zu dem dritten
Rechner gesendet.
- Der dritte Rechner analysiert und \"{u}berpr\"{u}ft die Daten.
- Das erste Schleusentor wird ge\"{o}ffnet.
- Die Daten werden von dem dritten Rechner (INS) zu dem
Schleusenelement gesendet.
- Das erste Schleusentor wird geschlossen.
- Das zweite Schleusentor wird ge\"{o}ffnet.
- Die Daten werden von dem Schleusenelement an den zweiten Rechner
gesendet.
- Und das zweite Schleusentor wird geschlossen.

Vorzugsweise erfolgt das Empfangen von Daten von dem zweiten Rechner
durch den ersten Rechner in den nachfolgenden Schritten:

- Das zweite Schleusentor wird ge\"{o}ffnet.
- Die zu empfangenden Daten werden von dem zweiten Rechner zu dem
Schleusenelement gesendet.
- Das zweite Schleusentor wird geschlossen.
- Das erste Schleusentor wird ge\"{o}ffnet.
- Die Daten werden von dem Schleusenelement zu dem dritten Rechner
gesendet.
- Das erste Schleusentor wird geschlossen.
- Der dritte Rechner analysiert und \"{u}berpr\"{u}ft die Daten.
- Und die Daten werden von dem dritten Rechner an den ersten Rechner
gesendet.

Das erste Schleusentor wird bevorzugt von dem dritten Rechner
angesteuert, das zweite Schleusentor von dem Schleusenelement.

Gem\"{a}ss einer vorteilhaften Weiterbildung des erfindungsgem\"{a}ssen
Verfahrens werden die zu empfangenden Daten von dem Schleusenelement
zu dem dritten Rechner zu dem Zeitpunkt gesendet, zu dem auch die zu
sendenden Daten von dem dritten Rechner zu dem Schleusenelement
gesendet werden. Dadurch k\"{o}nnen in einem Zeitschritt zwei
unterschiedliche Schritte der Daten\"{u}bertragung durchgef\"{u}hrt werden.

Voraussetzung daf\"{u}r ist, dass zu diesem Zeitpunkt die Positionen der
Schleusentore gleich sind. Im Fall dieser Weiterbildung ist das erste
Schleusentor n\"{a}mlich ge\"{o}ffnet und das zweite Schleusentor geschlossen.

Gem\"{a}ss einer anderen vorteilhaften Weiterbildung der Erfindung werden
die zu empfangenden Daten von dem zweiten Rechner zu dem
Schleusenelement zu dem Zeitpunkt gesendet, zu dem auch die zu
sendenden Daten von dem Schleusenelement zu dem zweiten Rechner
gesendet werden. Zu diesem Zeitpunkt sind das erste Schleusentor
geschlossen und das zweite Schleusentor ge\"{o}ffnet.

Gem\"{a}ss noch einer anderen Weiterbildung des erfindungsgem\"{a}ssen
Verfahrens wird die Analyse der empfangenen Daten zeitgleich mit der
Analyse der zu sendenden Daten durchgef\"{u}hrt. Die Analyse der Daten
erfolgt vorzugsweise in dem dritten und/oder in dem vierten Rechner.

Die Analyse der zu sendenden Daten kann aber auch in dem ersten
Rechner erfolgen.

Grunds\"{a}tzlich ist es m\"{o}glich, jeweils diejenigen Schritte einer
Daten\"{u}bertragung in einem Zeitschritt durchzuf\"{u}hren, bei denen die
Position der Schleusentore gleich ist.

Ein bevorzugtes Ausf\"{u}hrungsbeispiel der vorliegenden Erfindung wird im
Folgenden anhand der Zeichnung n\"{a}her erl\"{a}utert. Es zeigt
Fig. 1 eine erfindungsgem\"{a}sse Datenverbindung.

In Fig. 1 ist eine Datenverbindung zwischen einem ersten Rechner 1 und
einem zweiten Rechner 2 zum Zweck der Daten\"{u}bertragung dargestellt. In
der Datenverbindung ist ein Schleusenelement 6 angeordnet, wobei
zwischen dem ersten Rechner 1 und dem Schleusenelement 6 ein erstes
Schleusentor 7 und zwischen dem zweiten Rechner 2 und dem
Schleusenelement 6 ein zweites Schleusentor 8 angeordnet ist. Das
erste Schleusentor 7 ist geschlossen, wenn das zweite Schleusentor 8
ge\"{o}ffnet ist, und umgekehrt ist das zweite Schleusentor 8 geschlossen,
wenn das erste Schleusentor 7 ge\"{o}ffnet ist.

Der erste Rechner 1 ist in einem ersten Rechnernetzwerk 9 angeordnet,
wobei der erste Rechner 1 als ein Server eines Rechnernetzes und das
erste Rechnernetzwerk 9 als ein internes unternehmensweites
Rechnernetz ausgebildet ist. Der zweite Rechner 2 ist in einem zweiten
Rechnernetzwerk 10 angeordnet, wobei der zweite Rechner 2 als ein
Internet-Server ausgebildet ist und das zweite Rechnernetzwerk 10 das
Internet ist. In dem ersten Rechnernetzwerk 9 ist ein dritter Rechner
3 und in dem Schleusenelement 6 ein vierter Rechner 4 angeordnet. Der
Sinn des vierten Rechners 4 besteht darin, verschiedene
Analyseprozesse innerhalb des Schleusenelements 6 durchzuf\"{u}hren und
somit eine gewisse Vorselektion zu treffen.

Um nun Daten von dem ersten Rechner 1 zu dem zweiten Rechner 2 zu
senden, werden die zu sendenden Daten zun\"{a}chst von dem ersten Rechner
1 zu dem dritten Rechner 3 gesendet. In dem dritten Rechner 3 werden
die Daten analysiert und \"{u}berpr\"{u}ft. Die Analyse erfolgt vorzugsweise
nach semantischen Gesichtspunkten. Erst wenn die Analyse keine
Beanstandungen ergeben hat, wird das erste Schleusentor 7 ge\"{o}ffnet.

Dann werden die Daten von dem dritten Rechner 3 zu dem
Schleusenelement 6 gesendet, und anschliessend wird das erste
Schleusentor 7 wieder geschlossen. Erst nach vollst\"{a}ndigem Schliessen
des ersten Schleusentors 7 wird das zweite Schleusentor 8 ge\"{o}ffnet.

Dann werden die Daten von dem Schleusenelement 6 an den zweiten
Rechner 2 gesendet, und danach wird das zweite Schleusentor 8 wieder
geschlossen.

Zum Empfangen von Daten von dem zweiten Rechner 2 durch den ersten
Rechner 1 wird zun\"{a}chst das zweite Schleusentor 8 ge\"{o}ffnet, und die zu
empfangenden Daten werden von dem zweiten Rechner 2 zu dem
Schleusenelement 6 gesendet. Dann wird das zweite Schleusentor 8
geschlossen, und erst wenn es vollst\"{a}ndig geschlossen ist, wird das
erste Schleusentor 7 ge\"{o}ffnet. Anschliessend werden die Daten von dem
Schleusenelement 6 zu dem dritten Rechner 3 gesendet. Dann wird das
erste Schleusentor 7 geschlossen. Danach analysiert und \"{u}berpr\"{u}ft der
dritte Rechner 3 die Daten. Erst wenn die Analyse keine Beanstandungen
ergeben hat, werden die Daten von dem dritten Rechner 3 an den ersten
Rechner 1 gesendet.

Durch das Schleusenelement 6 zwischen dem ersten Rechner 1 und dem
zweiten Rechner 2 erfolgt eine physikalische Trennung der beiden
Rechner 1, 2 bzw. der beiden Rechnernetzwerke 9, 10. Dadurch k\"{o}nnen
Online-Angriffe von aussen auf den ersten Rechner 1 in dem
Rechnernetzwerk 9 verhindert werden, und es ist unm\"{o}glich, die
Analyseprozesse, die in dem dritten Rechner 3 durchgef\"{u}hrt werden, von
aussen zu manipulieren, da ein direkter Zugriff von aussen auf den
dritten Rechner 3 dank des Schleusenelements 6 nicht m\"{o}glich ist.

Die Datenverbindung zwischen dem ersten Rechner 1 und dem zweiten
Rechner 2 ist als eine
Integrated-Services-Digital-Network(ISDN)-Verbindung 5 nach dem
Net-Terminal-Basis-Adapter(NTBA)-Standard ausgebildet. An die
ISDN-Verbindung 5 ist auch der dritte Rechner 3 angeschlossen. Der
vierte Rechner 4 h\"{a}ngt nicht unmittelbar an dem ISDN-NTBA, da er -
\"{u}ber ein eigenes Netz - mit dem Schleusenelement 6 verbunden ist. Auf
diese Weise kann die Funktion des Schleusenelements 6 einfach und
wirkungsvoll realisiert werden. Die ISDN-Verbindung 5 nach dem
NTBA-Standard weist zwei Daten\"{u}bertragungskan\"{a}le (B-Kan\"{a}le) und einen
Steuerkanal (D-Kanal) auf. Somit gestattet ein ISDN-NTBA maximal zwei
Daten\"{u}bertragungsverbindungen gleichzeitig.

Wenn der dritte Rechner 3 zu dem Schleusenelement 6 eine Verbindung
zum Zwecke der Daten\"{u}bertragung aufbaut, werden hierf\"{u}r beide B-Kan\"{a}le
ben\"{o}tigt: \"{U}ber den einen B-Kanal erfolgt die Anwahl des
Schleusenelements 6, und \"{u}ber den anderen B-Kanal wird die
Daten\"{u}bertragungsverbindung zu dem Schleusenelement 6 hergestellt. Die
zwischen dem dritten Rechner 3 und dem Schleusenelement 6 hergestellte
ISDN-Verbindung 5 entspricht einer Schleusentorstellung, bei der das
erste Schleusentor 7 ge\"{o}ffnet und das zweite Schleusentor 8
geschlossen ist. Eine gleichzeitige Verbindung des Schleusenelements 6
zu dem zweiten Rechner 2 (zweites Schleusentor 8 ge\"{o}ffnet) ist also
aufgrund der technischen Gegebenheiten bei ISDN-NTBAs ausgeschlossen.

Wenn umgekehrt \"{u}ber einen der beiden B-Kan\"{a}le bereits eine Verbindung
zwischen dem zweiten Rechner 2 in dem zweiten Rechnernetzwerk 10 und
dem Schleusenelement 6 besteht (zweites Schleusentor 8 ge\"{o}ffnet), kann
der dritte Rechner 3 keine Verbindung mehr zu dem Schleusenelement 6
herstellen (erstes Schleusentor 7 kann nicht ge\"{o}ffnet werden), da
dazu, wie oben erl\"{a}utert, beide B-Kan\"{a}le des ISDN-NTBA ben\"{o}tigt
werden.


\end{verbatim}
\end{sect}

\begin{sect}{etc}{Autre Textes}
\begin{itemize}
\item
article de presse\footnote{\url{http://golem.de/0102/12126.html}}

\item
explications\footnote{\url{http://www.ti.fhg.de/frame/komp-mm-selk.de.html}} du brevetaire\footnote{\url{http://www.ti.fhg.de/}}

\item
d\'{e}scription de brevet de l'Office Allemand\footnote{\url{http://de.espacenet.com/search97cgi/s97is.dll?Action=View&ViewTemplate=e/de/de/viewer.hts&SearchType=1&VdkVgwKey=19838253}}

\item
Daniel Rödding explique que ca etait d\'{e}ja utilis\'{e} il y a 10 ans\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/Feb/0029.html}}

\item
difficultis de recherche concernant ce brevet\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/Feb/0032.html}}

\item
Die Fraunhofer-Gesellschaft als Bastion der Patentbewegung\footnote{\url{http://localhost/swpat/gasnu/fhg/index.de.html}} (Mit ihren MP3-Patenten hat die Fraunhofer-Gesellschaft ein Vorbild f\"{u}r relativ anspruchsvolle und zugleich lukrative Softwarepatente geschaffen, durch die der Staat bei der Finanzierung von Forschungsinstituten ein wenig entlastet wird.  Dieses Modell ist zwar nicht unproblematisch und auch nicht ohne weiteres beliebig ausweit- und wiederholbar, aber es ist zu einem Erfolgssymbol der Patentbewegung im Hochschulbereich (s. BMBF) geworden.  Die Fraunhofer-Gesellschaft betreibt zugleich eine zentrale Patentstelle f\"{u}r die deutschen Hochschulen, die eine \"{a}hnliche Pilotfunktion aus\"{u}bt.  Das Fraunhofer-Institut f\"{u}r Innovationsforschung verfasst regelm\"{a}{\ss}ig auf Bestellung des BMBF Gutachten, in denen die unfortschrittliche Methodik der Softwarebranche beklagt und die patentorientierte Fraunhofer-Forschung als Hoffnungstr\"{a}ger dargestellt wird.  Ihre Pilotfunktion in der Hochschul-Patentbewegung verleiht den Fraunhofer-Leuten ein starkes Sendungsbewusstsein.)
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

