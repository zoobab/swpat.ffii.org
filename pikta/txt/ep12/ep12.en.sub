\begin{subdocument}{swpiktxt12}{Top Software Probability Batch nr 12: 1101-1200}{http://swpat.ffii.org/patents/txt/ep12/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{longtable}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
\endhead
ep0382764\footnote{/patents/txt/ep/0382/764} & SIGNAL PROCESSING APPARATUS AND METHODS & HARVEY JOHN C (US) & 1987-09-11\\\hline
ep0807891\footnote{/patents/txt/ep/0807/891} & Stateless shopping cart for the web & SUN MICROSYSTEMS INC (US) & 1996-01-11\\\hline
ep0784279\footnote{/patents/txt/ep/0784/279} & Stateless shopping cart for the web & SUN MICROSYSTEMS INC (US) & 1996-01-11\\\hline
ep0191492\footnote{/patents/txt/ep/0191/492} & Computer-controlled electro-acoustic device. & BRAHLER HELMUT & 1985-02-15\\\hline
ep0731675\footnote{/patents/txt/ep/0731/675} & Process for producing endoprostheses &  & 1994-12-02\\\hline
ep0464766\footnote{/patents/txt/ep/0464/766} & System for processing parcel shipping & ALCATEL SATMAM (FR) & 1990-07-03\\\hline
ep0562410\footnote{/patents/txt/ep/0562/410} & Method and apparatus for synchronizing a real time clock. & THOMSON CONSUMER ELECTRONICS (FR) & 1992-03-25\\\hline
ep0733238\footnote{/patents/txt/ep/0733/238} & Method for storing a database in extended attributes of a file system & NOVELL INC (US) & 1993-12-10\\\hline
ep0721624\footnote{/patents/txt/ep/0721/624} & Architecture and method for data reduction in a system for analyzing geometric databases & CADENCE DESIGN SYSTEMS INC (US) & 1993-09-20\\\hline
ep0203165\footnote{/patents/txt/ep/0203/165} & Queueing protocol & UNIV WESTERN AUSTRALIA (AU) & 1984-12-03\\\hline
ep0847566\footnote{/patents/txt/ep/0847/566} & \#f & BAYERISCHES FORSCHUNGSZENTRUM (DE) & 1995-08-30\\\hline
ep0296764\footnote{/patents/txt/ep/0296/764} & Code excited linear predictive vocoder and method of operation. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1987-06-26\\\hline
ep0500076\footnote{/patents/txt/ep/0500/076} & Method and arrangement of determining coefficients for linear predictive coding & NIPPON ELECTRIC CO (JP) & 1991-02-19\\\hline
ep0381275\footnote{/patents/txt/ep/0381/275} & Method for transmitting, via a plurality of asynchronously time-divided transmission channels, a flow of data cells, the state of a counter for each transmission channel being kept up to date in accordance with the number of data cells per unit of time. & NEDERLAND PTT (NL) & 1989-02-03\\\hline
ep0486617\footnote{/patents/txt/ep/0486/617} & METHODS AND APPARATUS FOR DATA INPUT & GERPHEIDE GEORGE E (US) & 1989-08-16\\\hline
ep0529960\footnote{/patents/txt/ep/0529/960} & Hair retaining device. & NORTHGATE HOLDINGS LTD (GB) & 1991-08-23\\\hline
ep0618735\footnote{/patents/txt/ep/0618/735} & Subscriber device for videophone. & SEL ALCATEL AG (DE) & 1993-04-01\\\hline
ep0640493\footnote{/patents/txt/ep/0640/493} & Anticopying film & BASF MAGNETICS GMBH (DE) & 1993-08-24\\\hline
ep0286239\footnote{/patents/txt/ep/0286/239} & Production and purification of a protein fused to a binding protein. & NEW ENGLAND BIOLABS INC (US); UNIV TEMPLE (US) & 1987-03-10\\\hline
ep0463250\footnote{/patents/txt/ep/0463/250} & Data processing apparatus user interface and data processing apparatus with such an interface. & IBM (US) & 1990-06-28\\\hline
ep0396368\footnote{/patents/txt/ep/0396/368} & Perceptually-adapted image coding system. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1989-05-04\\\hline
ep0620960\footnote{/patents/txt/ep/0620/960} & Process for encoding and transmitting information &  & 1993-11-05\\\hline
ep0283079\footnote{/patents/txt/ep/0283/079} & Asynchronous time division communication system. & ALCATEL NV (NL); BELL TELEPHONE MFG (BE) & 1987-03-18\\\hline
ep0275273\footnote{/patents/txt/ep/0275/273} & Digital system and method for compressing speech signals for storage and transmission & NCR CO (US) & 1986-07-21\\\hline
ep0343790\footnote{/patents/txt/ep/0343/790} & Video telecommunication system and method for compressing and decompressing digital colour video data. & UVC CORP (US) & 1988-04-27\\\hline
ep0339947\footnote{/patents/txt/ep/0339/947} & Method and system for decompressing colour video encoded data. & UVC CORP (US) & 1988-04-27\\\hline
ep0649254\footnote{/patents/txt/ep/0649/254} & Method of storing coded image information. & PHILIPS ELECTRONICS NV (NL) & 1993-11-15\\\hline
ep0713332\footnote{/patents/txt/ep/0713/332} & Method for compressing and decompressing moving picture information and video signal processing system & SEGA ENTERPRISES KK (JP); HITACHI LTD (JP) & 1994-11-17\\\hline
ep0398987\footnote{/patents/txt/ep/0398/987} & COMMUNICATIONS NETWORK STATE AND TOPOLOGY MONITOR & NETWORK EQUIPMENT TECH (US) & 1988-01-29\\\hline
ep0360583\footnote{/patents/txt/ep/0360/583} & A stack system. & FUJITSU LTD (JP) & 1988-09-20\\\hline
ep0627703\footnote{/patents/txt/ep/0627/703} & On-line barcode printer system. & MONARCH MARKING SYSTEMS INC (US) & 1993-06-03\\\hline
ep0248533\footnote{/patents/txt/ep/0248/533} & Method, apparatus and system for recognizing broadcast segments & CONTROL DATA CORP (US) & 1986-05-02\\\hline
ep0705442\footnote{/patents/txt/ep/0705/442} & Differential global positioning system using radio data system & DIFFERENTIAL CORRECTIONS INC (US) & 1993-04-13\\\hline
ep0475749\footnote{/patents/txt/ep/0475/749} & ISDN terminal having diagnostic function. & NIPPON ELECTRIC CO (JP) & 1990-09-12\\\hline
ep0607405\footnote{/patents/txt/ep/0607/405} & NON-CONTACTING TRANSACTION SYSTEM FOR TICKETING & WHINHALL LIMITED (GB); TAIT ROBERT ALLEN REID (GB); TAIT ELIZABETH MARY (GB) & 1992-07-23\\\hline
ep0898756\footnote{/patents/txt/ep/0898/756} & FRAMEWORK FOR CONSTRUCTING SHARED DOCUMENTS THAT CAN BE COLLABORATIVELY ACCESSED BY MULTIPLE USERS & OBJECT TECH LICENSING CORP (US) & 1996-06-20\\\hline
ep0469658\footnote{/patents/txt/ep/0469/658} & Method of analysing cylinder performance in an internal combustion engine. & GEN MOTORS CORP (US); SATURN CORP (US); ELECTRONIC DATA SYST CORP (US) & 1990-07-30\\\hline
ep0848872\footnote{/patents/txt/ep/0848/872} & \#f & NISL KLAUS DIPL ING (DE); MEINHOLD MATTHIAS DR MED DIPL (DE); GUENTHER HARTMUT (DE) & 1995-09-04\\\hline
ep0170398\footnote{/patents/txt/ep/0170/398} & Stored logic program scanner for a data processor having internal plural data and instruction streams & BURROUGHS CORP (US) & 1984-06-28\\\hline
ep0222132\footnote{/patents/txt/ep/0222/132} & Digital data separator & HONEYWELL INF SYSTEMS (IT) & 1985-10-10\\\hline
ep0309669\footnote{/patents/txt/ep/0309/669} & Method for scene-model-assisted reduction of image data for digital television signals & SIEMENS AG (US) & 1987-09-30\\\hline
ep0836191\footnote{/patents/txt/ep/0836/191} & Method and apparatus for reproducing data from a recording medium & TOKYO SHIBAURA ELECTRIC CO (JP) & 1993-10-29\\\hline
ep0725541\footnote{/patents/txt/ep/0725/541} & Image information encoding/decoding system & TOKYO SHIBAURA ELECTRIC CO (JP) & 1995-02-03\\\hline
ep0278312\footnote{/patents/txt/ep/0278/312} & Distributed file and record locking. & IBM (US) & 1987-02-13\\\hline
ep0330834\footnote{/patents/txt/ep/0330/834} & Method and apparatus for linking an SNA host to a remote SNA host over a packet switched communications network. & IBM (US) & 1988-02-29\\\hline
ep0595331\footnote{/patents/txt/ep/0595/331} & Sound effect imparting apparatus. & YAMAHA CORP (JP) & 1992-10-30\\\hline
ep0570922\footnote{/patents/txt/ep/0570/922} & Reproducing apparatus. & SONY CORP (JP) & 1992-05-20\\\hline
ep0597483\footnote{/patents/txt/ep/0597/483} & Disc playback method. & SONY CORP (JP) & 1992-11-12\\\hline
ep0275789\footnote{/patents/txt/ep/0275/789} & Device having a modifiable code for protecting against theft of automobile vehicles & PEUGEOT (FR); CITROEN SA (FR) & 1986-12-31\\\hline
ep0687087\footnote{/patents/txt/ep/0687/087} & Secure data transmission method & AT \& T CORP (US) & 1994-06-07\\\hline
ep0402758\footnote{/patents/txt/ep/0402/758} & Portable electronic device having versatile program storage. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1989-06-12\\\hline
ep0320708\footnote{/patents/txt/ep/0320/708} & Video recording device with a memory for preferred programmes. & GRAETZ NOKIA GMBH (DE) & 1987-12-15\\\hline
ep0520400\footnote{/patents/txt/ep/0520/400} & A method of extracting a logical description of a sampled analog signal. & ASEA BROWN BOVERI (SE) & 1991-06-26\\\hline
ep0175557\footnote{/patents/txt/ep/0175/557} & Processing device and method. & FIFIELD KENNETH JOHN & 1984-09-20\\\hline
ep0336584\footnote{/patents/txt/ep/0336/584} & Sort merge output. & IBM (US) & 1988-04-07\\\hline
ep0584498\footnote{/patents/txt/ep/0584/498} & Wait state apparatus and method for high speed busses. & HEWLETT PACKARD CO (US) & 1992-08-21\\\hline
ep0413225\footnote{/patents/txt/ep/0413/225} & Electric time-controlled combination lock for protection against unauthorized use. & GRUNDIG EMV (DE) & 1989-08-16\\\hline
ep0388031\footnote{/patents/txt/ep/0388/031} & Reliability enhancement of nonvolatile tracked data storage devices. & IBM (US) & 1989-03-13\\\hline
ep0388841\footnote{/patents/txt/ep/0388/841} & Emergency post office setting for remote setting meter. & ALCATEL SATMAM (FR) & 1989-03-23\\\hline
ep0752660\footnote{/patents/txt/ep/0752/660} & Client-server computer system and method utilizing a local client disk drive as a data cache & SUN MICROSYSTEMS INC (US) & 1995-07-03\\\hline
ep0398493\footnote{/patents/txt/ep/0398/493} & File extension by client processors in a distributed data processing system. & IBM (US) & 1989-05-15\\\hline
ep0459192\footnote{/patents/txt/ep/0459/192} & Computer code optimization method. & HEWLETT PACKARD CO (US) & 1990-06-01\\\hline
ep0496986\footnote{/patents/txt/ep/0496/986} & Packet data communication system. & SYMBOL TECHNOLOGIES INC (US) & 1990-12-28\\\hline
ep0201064\footnote{/patents/txt/ep/0201/064} & Computer system with data residency transparency and data access transparency. & COMPUTER X INC (US) & 1985-05-06\\\hline
ep0236412\footnote{/patents/txt/ep/0236/412} & SECURE COMPUTER SYSTEM & PHILIPSZ BASIL ELISEUS & 1985-09-12\\\hline
ep0527213\footnote{/patents/txt/ep/0527/213} & Distributed computer system log-on device for storing and retrieving a user's view of objects at log-off & HEWLETT PACKARD CO (US) & 1991-04-30\\\hline
ep0290111\footnote{/patents/txt/ep/0290/111} & Digital data processing system. & DATA GENERAL CORP (US) & 1981-05-22\\\hline
ep0428021\footnote{/patents/txt/ep/0428/021} & Method for data distribution in a disk array. & COMPAQ COMPUTER CORP (US) & 1989-11-03\\\hline
ep0544337\footnote{/patents/txt/ep/0544/337} & Packet data communications. & SYMBOL TECHNOLOGIES INC (US) & 1991-11-27\\\hline
ep0764294\footnote{/patents/txt/ep/0764/294} & Information display system for actively redundant computerized process control & DOW CHEMICAL CO (US) & 1993-12-23\\\hline
ep0242808\footnote{/patents/txt/ep/0242/808} & Method and system for confirming user in modem communications. & MYUKOMU KK (JP) & 1986-10-14\\\hline
ep0089797\footnote{/patents/txt/ep/0089/797} & Digital image information compression and decompression method and apparatus & OLIVETTI \& CO SPA (IT) & 1982-03-19\\\hline
ep0787334\footnote{/patents/txt/ep/0787/334} & MULTI-STAGE PARCEL TRACKING SYSTEM & UNITED PARCEL SERVICE INC (US) & 1994-10-14\\\hline
ep0653643\footnote{/patents/txt/ep/0653/643} & Method and system for monitoring vehicles. & CARDION INC (US) & 1993-11-16\\\hline
ep0650271\footnote{/patents/txt/ep/0650/271} & Frequency reuse technique for a high data rate satellite communication system. & HUGHES AIRCRAFT CO (US) & 1993-10-21\\\hline
ep0797801\footnote{/patents/txt/ep/0797/801} & METHOD AND APPARATUS FOR PROVIDING SIMPLE, SECURE MANAGEMENT OF REMOTE SERVERS & NICOLET JIM (US); NOVELL INC (US) & 1994-12-13\\\hline
ep0481031\footnote{/patents/txt/ep/0481/031} & SYSTEM FOR COMPOUNDING INSTRUCTIONS FOR HANDLING INSTRUCTION AND DATA STREAM FOR PROCESSOR WITH DIFFERENT ATTRIBUTES & IBM (US) & 1991-01-15\\\hline
ep0840909\footnote{/patents/txt/ep/0840/909} & \#f & KUKA SCHWEISSANLAGEN \& ROBOTER (DE) & 1995-07-22\\\hline
ep0545927\footnote{/patents/txt/ep/0545/927} & SYSTEM FOR COMPOUNDING INSTRUCTIONS FOR HANDLING INSTRUCTION AND DATA STREAM FOR PROCESSOR WITH DIFFERENT ATTRIBUTES & IBM (US) & 1991-01-15\\\hline
ep0520488\footnote{/patents/txt/ep/0520/488} & Method and apparatus for integrating a dynamic lexicon into a full-text information retrieval system. & DIGITAL EQUIPMENT CORP (US) & 1991-06-28\\\hline
ep0333029\footnote{/patents/txt/ep/0333/029} & Rapid access teletext decoder arrangement & RCA LICENSING CORP (US) & 1988-05-23\\\hline
ep0530009\footnote{/patents/txt/ep/0530/009} & Method of characterising genomic DNA. & ICI PLC (GB) & 1992-06-17\\\hline
ep0252199\footnote{/patents/txt/ep/0252/199} & Installation for the point-to-point transmission of data between a track and a vehicle passing over it. & ACEC (BE) & 1986-06-24\\\hline
ep0544430\footnote{/patents/txt/ep/0544/430} & Method and apparatus for determining the frequency of words in a document without document image decoding. & XEROX CORP (US) & 1991-11-19\\\hline
ep0515168\footnote{/patents/txt/ep/0515/168} & Graphical communication device. & XEROX CORP (US) & 1991-05-20\\\hline
ep0419932\footnote{/patents/txt/ep/0419/932} & Apparatus and method for controlling the time assignment of the processing power of a data processing system & ALCATEL NV (NL) & 1989-09-25\\\hline
ep0613117\footnote{/patents/txt/ep/0613/117} & Means for defining the display in a display apparatus of a computer terminal. & WANG LABORATORIES (US) & 1986-09-12\\\hline
ep0259815\footnote{/patents/txt/ep/0259/815} & Terminal with viewports, auxiliary device attachment, and host-terminal flan control & WANG LABORATORIES (US) & 1986-09-12\\\hline
ep0274571\footnote{/patents/txt/ep/0274/571} & Electronic document filing apparatus. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1987-01-31\\\hline
ep0497022\footnote{/patents/txt/ep/0497/022} & Conference system. & HEWLETT PACKARD CO (US) & 1991-01-31\\\hline
ep0323698\footnote{/patents/txt/ep/0323/698} & System and method for dividing encoded data for display in a plurality of even columns. & IBM (US) & 1987-12-18\\\hline
ep0382237\footnote{/patents/txt/ep/0382/237} & Multiprocessing system having a single translation lookaside buffer with reduced processor overhead and operating method therefor & NIPPON ELECTRIC CO (JP) & 1989-02-10\\\hline
ep0243671\footnote{/patents/txt/ep/0243/671} & Menu management system. & WANG LABORATORIES (US) & 1986-03-27\\\hline
ep0675426\footnote{/patents/txt/ep/0675/426} & Computer system with touchpad support in operating system. & IBM (US) & 1994-03-18\\\hline
ep0349463\footnote{/patents/txt/ep/0349/463} & Method of simultaneously entering data into overlapped windows. & IBM (US) & 1988-06-30\\\hline
ep0589657\footnote{/patents/txt/ep/0589/657} & Network system and terminal apparatus. & CANON KK (JP) & 1992-09-21\\\hline
ep0537100\footnote{/patents/txt/ep/0537/100} & A method of implementing a preview window in an object oriented programming system. & IBM (US) & 1991-10-08\\\hline
ep0547784\footnote{/patents/txt/ep/0547/784} & Managing display windows of interrelated applications. & SUN MICROSYSTEMS INC (US) & 1991-12-18\\\hline
ep0414314\footnote{/patents/txt/ep/0414/314} & Method of generating a unique number for a smart card and its use for the cooperation of the card with a host system & PHILIPS CORP (US) & 1989-08-22\\\hline
ep0114368\footnote{/patents/txt/ep/0114/368} & Data protection system. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1982-12-28\\\hline
\end{longtable}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: latex ;
% End: ;

