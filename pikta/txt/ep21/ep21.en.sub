\begin{subdocument}{swpiktxt21}{Top Software Probability Batch nr 21: 2001-2100}{http://swpat.ffii.org/patents/txt/ep21/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{longtable}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
\endhead
ep0517269\footnote{/patents/txt/ep/0517/269} & Microcomputer with test mode switching function. & NIPPON ELECTRIC CO (JP) & 1991-06-06\\\hline
ep0298523\footnote{/patents/txt/ep/0298/523} & Resource Control System. & NIPPON ELECTRIC CO (JP) & 1987-07-09\\\hline
ep0572865\footnote{/patents/txt/ep/0572/865} & Method for implementing a data communication protocol stack. & ALCATEL NV (NL) & 1992-05-26\\\hline
ep0376342\footnote{/patents/txt/ep/0376/342} & Data processing apparatus for electronic musical instruments. & CASIO COMPUTER CO LTD (JP) & 1988-12-29\\\hline
ep0199252\footnote{/patents/txt/ep/0199/252} & Thermal label printer. & SATO KK (JP) & 1985-04-26\\\hline
ep0615635\footnote{/patents/txt/ep/0615/635} & Method and apparatus for implementing TDM fuzzy control & THOMSON BRANDT GMBH (DE) & 1992-12-04\\\hline
ep0795158\footnote{/patents/txt/ep/0795/158} & Bridge between two buses of a computer system that latches signals from the bus for use on the bridge and responds according to the bus protocols & IBM (US) & 1994-11-30\\\hline
ep0267280\footnote{/patents/txt/ep/0267/280} & Interactive television and data transmission system & RADIO TELCOM \& TECHNOLOGY INC (US) & 1986-05-14\\\hline
ep0686284\footnote{/patents/txt/ep/0686/284} & Computer networking system including central chassis with processor and input/output modules, remote transceivers, and communication links between the transceivers and input/output modules &  & 1993-01-22\\\hline
ep0495044\footnote{/patents/txt/ep/0495/044} & Neural network process measurement and control & DU PONT (US) & 1990-08-03\\\hline
ep0856176\footnote{/patents/txt/ep/0856/176} & Database management system and data transmission method &  & 1996-10-11\\\hline
ep0209441\footnote{/patents/txt/ep/0209/441} & Transcoding method of a video text image to render it compatible with several video text systems. & NAHON GEORGES & 1985-07-08\\\hline
ep0689742\footnote{/patents/txt/ep/0689/742} & Radio receiver for information dissemenation using subcarrier & MACROVISION CORP (US) & 1993-03-15\\\hline
ep0166466\footnote{/patents/txt/ep/0166/466} & Intelligent processor/memory elements and systems. & STERN AUGUST (NL); STERN VICTOR (NL) & 1984-03-29\\\hline
ep0616455\footnote{/patents/txt/ep/0616/455} & Computer network using data compression. & IBM (US) & 1992-09-19\\\hline
ep0530954\footnote{/patents/txt/ep/0530/954} & Image processing method and apparatus. & CANON KK (JP) & 1991-07-09\\\hline
ep0609846\footnote{/patents/txt/ep/0609/846} & Vibration/noise control system. & HONDA MOTOR CO LTD (JP) & 1993-03-22\\\hline
ep0020931\footnote{/patents/txt/ep/0020/931} & Programme interrupt processor for computer with instruction pre-fetch. & IBM (US) & 1979-06-21\\\hline
ep0127745\footnote{/patents/txt/ep/0127/745} & A word processing system based on a data stream having integrated alphanumeric and graphic data. & IBM (US) & 1983-05-11\\\hline
ep0266001\footnote{/patents/txt/ep/0266/001} & Parser for natural language text & IBM (US) & 1986-10-29\\\hline
ep0309878\footnote{/patents/txt/ep/0309/878} & Programming method and/or equipment for audio or video appliances & THOMSON BRANDT GMBH (DE) & 1987-09-30\\\hline
ep0554998\footnote{/patents/txt/ep/0554/998} & Image processing method and apparatus. & CANON KK (JP) & 1992-01-29\\\hline
ep0249494\footnote{/patents/txt/ep/0249/494} & System for facilitating the control of audio information within a word processing document with audio input & IBM (US) & 1986-06-13\\\hline
ep0748482\footnote{/patents/txt/ep/0748/482} & MEDICAL DECISION-MAKING AID AND DEVICE FOR DELIVERING AT LEAST ONE DRUG & HESS JOSEPH (CH); ENSYMA S A (CH) & 1994-03-04\\\hline
ep0373895\footnote{/patents/txt/ep/0373/895} & Magnetic recording and reproducing apparatus and method of recording and reproducing. & MITSUBISHI ELECTRIC CORP (JP) & 1988-12-22\\\hline
ep0644541\footnote{/patents/txt/ep/0644/541} & Magnetic recording and reproducing apparatus and method of recording and reproducing. & MITSUBISHI ELECTRIC CORP (JP) & 1989-12-07\\\hline
ep0308571\footnote{/patents/txt/ep/0308/571} & Data accumulator and transponder. & ROCKWELL INTERNATIONAL CORP (US) & 1982-06-10\\\hline
ep0195569\footnote{/patents/txt/ep/0195/569} & Adaptive processor array capable of learning variable associations useful in recognizing classes of inputs & XEROX CORP (US) & 1985-03-15\\\hline
ep0378697\footnote{/patents/txt/ep/0378/697} & MULTI-WINDOW COMMUNICATION SYSTEM & FUJITSU LTD (JP) & 1988-09-20\\\hline
ep0543860\footnote{/patents/txt/ep/0543/860} & Digital processor for simulating operation of a parallel processing array & SECR DEFENCE BRIT (GB) & 1991-08-15\\\hline
ep0326879\footnote{/patents/txt/ep/0326/879} & Electrically-erasable, electrically-programmable read-only memory. & TEXAS INSTRUMENTS INC (US) & 1988-07-15\\\hline
ep0326877\footnote{/patents/txt/ep/0326/877} & Electrically-erasable, electrically-programmable read-only memory cell. & TEXAS INSTRUMENTS INC (US) & 1988-07-15\\\hline
ep0772824\footnote{/patents/txt/ep/0772/824} & Object-oriented operating system enhancement for filtering items in a window & OBJECT TECH LICENSING CORP (US) & 1994-07-25\\\hline
ep0248989\footnote{/patents/txt/ep/0248/989} & Communication bit pattern detection circuit & IBM (US) & 1986-06-13\\\hline
ep0788632\footnote{/patents/txt/ep/0788/632} & Computerized conversion of tables & IBM (US) & 1995-05-23\\\hline
ep0388700\footnote{/patents/txt/ep/0388/700} & METHOD FOR GENERATING RANDOM NUMBER FOR THE ENCODED TRANSMISSION OF DATA & SIEMENS AG (DE) & 1989-03-08\\\hline
ep0464191\footnote{/patents/txt/ep/0464/191} & Data compression method and apparatus utilizing an adaptive dictionary & HEWLETT PACKARD CO (US) & 1990-02-08\\\hline
ep0599490\footnote{/patents/txt/ep/0599/490} & Method and apparatus for storing a media access control address in a remotely alterable memory. & CANON INFORMATION SYST INC (US) & 1992-11-18\\\hline
ep0455442\footnote{/patents/txt/ep/0455/442} & Fault detection in link-connected systems. & IBM (US) & 1990-04-30\\\hline
ep0574691\footnote{/patents/txt/ep/0574/691} & Method for sharing I/O resources by a plurality of operating systems and programs & IBM (US) & 1992-06-15\\\hline
ep0468116\footnote{/patents/txt/ep/0468/116} & Signal detector. & PIONEER ELECTRONIC CORP (JP) & 1990-07-25\\\hline
ep0660587\footnote{/patents/txt/ep/0660/587} & Reversible video compression method. & HEWLETT PACKARD CO (US) & 1993-12-20\\\hline
ep0639033\footnote{/patents/txt/ep/0639/033} & Video/audio compression and reproduction device. & NIPPON ELECTRIC CO (JP) & 1993-08-13\\\hline
ep0484070\footnote{/patents/txt/ep/0484/070} & Editing compressed voice information. & IBM (US) & 1990-10-30\\\hline
ep0521487\footnote{/patents/txt/ep/0521/487} & Information recording medium and reproducing device therefor. & SONY CORP (JP) & 1991-07-05\\\hline
ep0408188\footnote{/patents/txt/ep/0408/188} & Compressed prefix matching database searching. & DIGITAL EQUIPMENT CORP (US) & 1989-07-12\\\hline
ep0358293\footnote{/patents/txt/ep/0358/293} & Local area system transport. & DIGITAL EQUIPMENT CORP (US) & 1988-09-08\\\hline
ep0501697\footnote{/patents/txt/ep/0501/697} & Mediation of transactions by a communications system. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1991-02-27\\\hline
ep0489915\footnote{/patents/txt/ep/0489/915} & NAVIGATION APPARATUS AND METHOD. & TSUYUKI TOSHIO (JP) & 1989-12-13\\\hline
ep0602868\footnote{/patents/txt/ep/0602/868} & Distance recording apparatus for vehicles. & BEALE SIDNEY ARTHUR (GB) & 1992-12-16\\\hline
ep0656729\footnote{/patents/txt/ep/0656/729} & Method and apparatus for editing or mixing compressed pictures. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1994-03-10\\\hline
ep0703435\footnote{/patents/txt/ep/0703/435} & Moving object navigation apparatus & NIPPON ELECTRIC CO (JP) & 1994-09-21\\\hline
ep0215984\footnote{/patents/txt/ep/0215/984} & Graphic display apparatus with combined bit buffer and character graphics store & IBM (US) & 1985-09-10\\\hline
ep0650264\footnote{/patents/txt/ep/0650/264} & Byte aligned data compression & DIGITAL EQUIPMENT CORP (US) & 1993-10-25\\\hline
ep0230590\footnote{/patents/txt/ep/0230/590} & Clock regeneration method for the decoding of digital data signals. & GRUNDIG EMV (DE) & 1986-01-29\\\hline
ep0855069\footnote{/patents/txt/ep/0855/069} & PROCESS FOR PAYING WITHOUT CASH SERVICES THAT CAN BE REQUESTED FROM A DISTRIBUTED, NON-CONNECTION ORIENTED DATA NETWORK & SENG ULRICH (DE) & 1996-07-12\\\hline
ep0435476\footnote{/patents/txt/ep/0435/476} & Database system & INT COMPUTERS LTD (GB) & 1989-12-23\\\hline
ep0406780\footnote{/patents/txt/ep/0406/780} & Office communication system & GRUNDIG EMV (DE) & 1989-07-04\\\hline
ep0632625\footnote{/patents/txt/ep/0632/625} & Programmable high performance data communication adapter for high speed packet transmission networks. & IBM (US) & 1993-06-30\\\hline
ep0209079\footnote{/patents/txt/ep/0209/079} & Method for making an enciphered radio communication. & SIEMENS AG (DE) & 1985-07-12\\\hline
ep0193835\footnote{/patents/txt/ep/0193/835} & Apparatus for collecting monitoring information in transmission systems. & SIEMENS AG (DE) & 1985-02-27\\\hline
ep0724490\footnote{/patents/txt/ep/0724/490} & Logistics network for processing of mailed articles and method for controlling this network & LICENTIA GMBH (DE) & 1995-08-17\\\hline
ep0754319\footnote{/patents/txt/ep/0754/319} & DASD capacity in excess of 528 megabytes apparatus and method for personal computers & IBM (US) & 1994-04-07\\\hline
ep0337702\footnote{/patents/txt/ep/0337/702} & Code-error correcting device. & VICTOR COMPANY OF JAPAN (JP) & 1988-04-08\\\hline
ep0415853\footnote{/patents/txt/ep/0415/853} & Digital data transmission system having error detecting and correcting function. & SONY CORP (JP) & 1989-08-31\\\hline
ep0207038\footnote{/patents/txt/ep/0207/038} & Towed densimeter system. & D O S DREDGING COMPANY LIMITED (GB) & 1985-05-15\\\hline
ep0460935\footnote{/patents/txt/ep/0460/935} & Attitude control system for momentum-biased spacecraft & HUGHES AIRCRAFT CO (US) & 1990-06-07\\\hline
ep0723686\footnote{/patents/txt/ep/0723/686} & Device for integrating diagnostic imaging and data-processing devices systems into EDP systems & CSB SYST SOFTWARE ENTWICKLUNG (DE) & 1994-09-23\\\hline
ep0485690\footnote{/patents/txt/ep/0485/690} & Parallel associative processor system. & IBM (US) & 1990-11-13\\\hline
ep0378325\footnote{/patents/txt/ep/0378/325} & Circuit test method and apparatus. & SCHLUMBERGER TECHNOLOGIES LTD (GB) & 1989-01-09\\\hline
ep0447841\footnote{/patents/txt/ep/0447/841} & METHOD FOR SETTING UP VIRTUAL CONNECTIONS IN SWITCHING EQUIPMENT OPERATING ACCORDING TO AN ASYNCHRONOUS TRANSFER MODE & SIEMENS AG (DE) & 1990-03-23\\\hline
ep0566961\footnote{/patents/txt/ep/0566/961} & Method and arrangement for checking the observance of prescribed transmission bit rates in an ATM switching equipment & SIEMENS AG (DE) & 1992-04-23\\\hline
ep0716404\footnote{/patents/txt/ep/0716/404} & Process for the serial transmission of digital measurement values &  & 1994-12-09\\\hline
ep0226750\footnote{/patents/txt/ep/0226/750} & Method of electrostatic color proofing by image reversal. & COULTER SYSTEMS CORP (US) & 1985-10-31\\\hline
ep0362344\footnote{/patents/txt/ep/0362/344} & Solid-state image sensor & EASTMAN KODAK CO (US) & 1988-03-21\\\hline
ep0405540\footnote{/patents/txt/ep/0405/540} & Dispersion-compensated windshield hologram virtual image display. & HUGHES AIRCRAFT CO (US) & 1989-06-29\\\hline
ep0495806\footnote{/patents/txt/ep/0495/806} & DEVICE FOR DRYING IMAGE SUPPORTS IN INK PRINTING INSTALLATIONS & SIEMENS AG (DE) & 1989-10-10\\\hline
ep0592578\footnote{/patents/txt/ep/0592/578} & VIRTUAL IMAGE DISPLAY DEVICE & VPL RESEARCH INC (US) & 1991-07-03\\\hline
ep0229336\footnote{/patents/txt/ep/0229/336} & A virtual terminal subsystem. & IBM (US) & 1986-01-17\\\hline
ep0288146\footnote{/patents/txt/ep/0288/146} & Garbage collection in a virtual memory system. & HEWLETT PACKARD CO (US) & 1987-03-20\\\hline
ep0491498\footnote{/patents/txt/ep/0491/498} & Apparatus and method for a space saving translation lookaside buffer for content addressable memory. & SUN MICROSYSTEMS INC (US) & 1990-12-18\\\hline
ep0766617\footnote{/patents/txt/ep/0766/617} & Method and an apparatus for the production of a fibre reinforced three-dimensional product &  & 1995-06-22\\\hline
ep0303051\footnote{/patents/txt/ep/0303/051} & Side-fed superlattice for the production of linear predictor and filter coefficients. & ADLER RES ASSOC (US) & 1987-08-12\\\hline
ep0431463\footnote{/patents/txt/ep/0431/463} & Two-level translation look-aside buffer using partial addresses for enhanced speed. & MIPS COMPUTER SYSTEMS INC (US) & 1989-12-01\\\hline
ep0492838\footnote{/patents/txt/ep/0492/838} & Apparatus for increasing the number of hits in a translation lookaside buffer. & SUN MICROSYSTEMS INC (US) & 1990-12-21\\\hline
ep0542442\footnote{/patents/txt/ep/0542/442} & Patient monitoring unit and care station. & CRITIKON INC (US) & 1991-10-24\\\hline
ep0369701\footnote{/patents/txt/ep/0369/701} & Thrust bearing arrangement for a power tool transmission. & BLACK \& DECKER INC (US) & 1988-11-14\\\hline
ep0246916\footnote{/patents/txt/ep/0246/916} & Boundary layer flow control. & ROLLS ROYCE PLC (GB) & 1987-03-19\\\hline
ep0284187\footnote{/patents/txt/ep/0284/187} & Boundary layer devices. & ROLLS ROYCE PLC (GB) & 1987-03-19\\\hline
ep0403076\footnote{/patents/txt/ep/0403/076} & Magnetic medium for longitudinal recording. & DIGITAL EQUIPMENT CORP (US) & 1989-06-12\\\hline
ep0393642\footnote{/patents/txt/ep/0393/642} & Engine speed controlling apparatus for internal combustion engine. & TOYODA AUTOMATIC LOOM WORKS (JP); TOYODA CHUO KENKYUSHO KK (JP) & 1989-04-20\\\hline
ep0420196\footnote{/patents/txt/ep/0420/196} & Display apparatus. & CANON KK (JP) & 1989-09-27\\\hline
ep0528783\footnote{/patents/txt/ep/0528/783} & Device for the support of a car body on a bogie especially for railway vehicles. & SGP VERKEHRSTECHNIK (AT); REXROTH MANNESMANN GMBH (DE) & 1991-08-16\\\hline
ep0523614\footnote{/patents/txt/ep/0523/614} & A seal assembly, particularly for a rolling element bearing. & SKF IND SPA (IT) & 1991-07-16\\\hline
ep0576750\footnote{/patents/txt/ep/0576/750} & Modified leaky bucket method & SIEMENS AG (DE) & 1992-06-30\\\hline
ep0385082\footnote{/patents/txt/ep/0385/082} & Device for winding webs onto core tubes provided with an adhesive coating. & WINDMOELLER \& HOELSCHER (DE) & 1989-02-28\\\hline
ep0395573\footnote{/patents/txt/ep/0395/573} & Device for the magnetic treatment of a fluid. & ISOLA ALICE (IT) & 1989-03-30\\\hline
ep0618398\footnote{/patents/txt/ep/0618/398} & Device for the treatment of the inner surface of a non-accessible pipeline & SIEMENS AG (DE) & 1993-04-02\\\hline
ep0678387\footnote{/patents/txt/ep/0678/387} & Inkjet recording apparatus and method of producing an inkjet head. & SEIKO EPSON CORP (JP) & 1994-04-20\\\hline
ep0722745\footnote{/patents/txt/ep/0722/745} & Apparatus for supplying fluid & AUBEX CORP (JP); OPTO TECH CO LTD (JP) & 1995-01-24\\\hline
\end{longtable}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: latex ;
% End: ;

