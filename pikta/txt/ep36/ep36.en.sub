\begin{subdocument}{swpiktxt36}{Top Software Probability Batch nr 36: 3501-3600}{http://swpat.ffii.org/patents/txt/ep36/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
ep0507601\footnote{/patents/txt/ep/0507/601} & Image processing method and apparatus. & CANON KK (JP) & 1991-04-04\\\hline
ep0581971\footnote{/patents/txt/ep/0581/971} & IMAGE SCANNER. & FUJITSU LTD (JP); PFU LTD (JP) & 1992-05-13\\\hline
ep0437207\footnote{/patents/txt/ep/0437/207} & Information processing system. & FUJITSU LTD (JP) & 1990-01-06\\\hline
ep0680643\footnote{/patents/txt/ep/0680/643} & Apparatus and method for compressing information & SARNOFF DAVID RES CENTER (US) & 1993-01-22\\\hline
ep0654199\footnote{/patents/txt/ep/0654/199} & RATIONAL INPUT BUFFER ARRANGEMENTS FOR AUXILIARY INFORMATION IN VIDEO AND AUDIO SIGNAL PROCESSING SYSTEMS & SONY CORP (JP) & 1993-06-10\\\hline
ep0220536\footnote{/patents/txt/ep/0220/536} & Method and apparatus for communicating data between a host and a plurality of parallel processors & IBM (US) & 1985-10-31\\\hline
ep0616466\footnote{/patents/txt/ep/0616/466} & Horizontal panning for wide screen television. & THOMSON CONSUMER ELECTRONICS (US) & 1993-12-21\\\hline
ep0190489\footnote{/patents/txt/ep/0190/489} & Speaker-independent speech recognition method and system. & TEXAS INSTRUMENTS INC (US) & 1984-12-27\\\hline
ep0325433\footnote{/patents/txt/ep/0325/433} & Wireless transmission-reception control system. & MATSUSHITA ELECTRIC WORKS LTD (JP) & 1988-01-21\\\hline
ep0396415\footnote{/patents/txt/ep/0396/415} & A video processing system. & QUANTEL LTD (GB) & 1989-05-05\\\hline
ep0562129\footnote{/patents/txt/ep/0562/129} & Method and means for extracting motion errors of a platform carrying a coherent imaging radar system. & DEUTSCHE FORSCH LUFT RAUMFAHRT (DE); ISTITUTO RICERCA ELETTROMAGNET (IT) & 1992-03-03\\\hline
ep0622639\footnote{/patents/txt/ep/0622/639} & Meteorological workstation. & IBM (US) & 1993-04-30\\\hline
ep0593968\footnote{/patents/txt/ep/0593/968} & Cache-based data compression/decompression. & HEWLETT PACKARD CO (US) & 1992-10-19\\\hline
ep0304608\footnote{/patents/txt/ep/0304/608} & Multi-mode dynamic code assignment for data compression. & IBM (US) & 1987-08-24\\\hline
ep0647916\footnote{/patents/txt/ep/0647/916} & Bar code scan stitching. & SYMBOL TECHNOLOGIES INC (US) & 1994-08-22\\\hline
ep0411264\footnote{/patents/txt/ep/0411/264} & Compression method. & TELETTRA LAB TELEFON (IT) & 1989-06-07\\\hline
ep0410062\footnote{/patents/txt/ep/0410/062} & Dynamic selection of logical element data format. & IBM (US) & 1988-07-22\\\hline
ep0464190\footnote{/patents/txt/ep/0464/190} & DATA STORAGE ON TAPE & HEWLETT PACKARD LTD (GB) & 1990-01-19\\\hline
ep0572638\footnote{/patents/txt/ep/0572/638} & Method and apparatus for encoding of data using both vector quantization and runlength encoding and using adaptive runlength encoding & MASSACHUSETTS INST TECHNOLOGY (US) & 1991-12-24\\\hline
ep0637817\footnote{/patents/txt/ep/0637/817} & Apparatus and method for the characterisation of data relating to the contents of a container. & FOLDENAUER WILLI (DE); HORST HEIRLER PROJEKTE (DE) & 1992-01-30\\\hline
ep0669015\footnote{/patents/txt/ep/0669/015} & \#f & SCHIEBER UNIVERSAL MASCHF (DE) & 1992-11-16\\\hline
ep0425046\footnote{/patents/txt/ep/0425/046} & METHOD OF TRANSMITTING A SEQUENCE OF M BINARY WORDS OF ``P`` BITS AND TRANSMISSION ARRANGEMENT IMPLEMENTING THIS METHOD & PHILIPS CORP (US) & 1989-10-27\\\hline
ep0533049\footnote{/patents/txt/ep/0533/049} & Printer for printing bold characters. & SEIKO EPSON CORP (JP) & 1991-09-18\\\hline
ep0664901\footnote{/patents/txt/ep/0664/901} & ATOMIC COMMAND SYSTEM & TALIGENT INC (US) & 1992-12-23\\\hline
ep0343740\footnote{/patents/txt/ep/0343/740} & Teletext decoders. & PHILIPS ELECTRONICS UK LTD (GB); PHILIPS NV (NL) & 1989-05-05\\\hline
ep0459041\footnote{/patents/txt/ep/0459/041} & Tape storage. & HEWLETT PACKARD LTD (GB) & 1990-05-29\\\hline
ep0465932\footnote{/patents/txt/ep/0465/932} & Method for indication of natural gas potential in sedimentary basins and oil potential resulting therefrom. & KETTEL DIRK (DE) & 1990-07-05\\\hline
ep0665527\footnote{/patents/txt/ep/0665/527} & Flat panel display interface for a high resolution computer graphics system. & SUN MICROSYSTEMS INC (US) & 1994-01-28\\\hline
ep0256701\footnote{/patents/txt/ep/0256/701} & Crosspoint circuitry for data packet space division switches & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1986-08-06\\\hline
ep0667619\footnote{/patents/txt/ep/0667/619} & INFORMATION PROCESSING APPARATUS. & SEGA ENTERPRISES KK (JP) & 1992-10-30\\\hline
ep0637807\footnote{/patents/txt/ep/0637/807} & Recording and retrieval of information relevant to the activities of a user. & RANK XEROX LTD (GB) & 1993-07-26\\\hline
ep0527890\footnote{/patents/txt/ep/0527/890} & OILFIELD EQUIPMENT IDENTIFICATION APPARATUS & PERKIN GREGG S (US); DENNY LAWRENCE A (US) & 1990-05-04\\\hline
ep0284980\footnote{/patents/txt/ep/0284/980} & Method for generating character images for dot printing. & IBM (US) & 1987-04-01\\\hline
ep0285520\footnote{/patents/txt/ep/0285/520} & Method of dispatching secret keys to security modules and user cards in a data processing network & CII HONEYWELL BULL (FR) & 1987-04-03\\\hline
ep0545447\footnote{/patents/txt/ep/0545/447} & Dot matrix printer and method for storing and retrieving bit map data & SEIKO EPSON CORP (JP) & 1991-12-06\\\hline
ep0229524\footnote{/patents/txt/ep/0229/524} & Two-wire full duplex frequency division multiplex modem system having echo cancellation means & FUJITSU LTD (JP) & 1985-12-23\\\hline
ep0667716\footnote{/patents/txt/ep/0667/716} & DEVICE AND METHOD FOR ENCODING IMAGE. & SONY CORP (JP) & 1993-10-04\\\hline
ep0183564\footnote{/patents/txt/ep/0183/564} & Image forming apparatus and method & SONY CORP (JP) & 1984-11-30\\\hline
ep0311265\footnote{/patents/txt/ep/0311/265} & Method and apparatus for processing picture element (pel) signals of an image. & IBM (US) & 1987-10-09\\\hline
ep0498594\footnote{/patents/txt/ep/0498/594} & Telephone connection to a nearby dealer. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1991-02-07\\\hline
ep0635779\footnote{/patents/txt/ep/0635/779} & User interface having movable sheet with click-through tools. & XEROX CORP (US) & 1993-07-21\\\hline
ep0664904\footnote{/patents/txt/ep/0664/904} & System and method for implementing an interface between an external process and transaction processing system & DOW CHEMICAL CO (US) & 1992-10-15\\\hline
ep0503417\footnote{/patents/txt/ep/0503/417} & Method and means for verification of write data. & FUJITSU LTD (JP) & 1991-03-11\\\hline
ep0441823\footnote{/patents/txt/ep/0441/823} & Automatic fee collecting and receipt dispensing system & AMERICAN REGISTRATION SYSTEMS (US) & 1988-11-01\\\hline
ep0396346\footnote{/patents/txt/ep/0396/346} & Error check or error correction code coding device. & CANON KK (JP) & 1989-04-28\\\hline
ep0341175\footnote{/patents/txt/ep/0341/175} & Multiple access by distributed traffic control in a local area communication network. & SABOURIN HERVE JEAN FRANCOIS & 1988-06-01\\\hline
ep0462759\footnote{/patents/txt/ep/0462/759} & Coordinate data processing apparatus using a pen. & SONY CORP (JP) & 1990-06-18\\\hline
ep0450196\footnote{/patents/txt/ep/0450/196} & Data processing system using gesture-based input data. & KONINKL PHILIPS ELECTRONICS NV (NL) & 1990-04-02\\\hline
ep0390164\footnote{/patents/txt/ep/0390/164} & Image display system. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1989-03-31\\\hline
ep0472389\footnote{/patents/txt/ep/0472/389} & Method and apparatus for programming a voice services system. & TEXAS INSTRUMENTS INC (US) & 1990-08-20\\\hline
ep0629953\footnote{/patents/txt/ep/0629/953} & Graphical manipulation of encryption. & IBM (US) & 1993-06-14\\\hline
ep0438512\footnote{/patents/txt/ep/0438/512} & \#f & MAX PLANCK GESELLSCHAFT (DE) & 1988-10-11\\\hline
ep0419958\footnote{/patents/txt/ep/0419/958} & CIRCUIT FOR CALCULATING THE QUANTITY OF MESSAGE SIGNALS SUPPLIED TO AN ATM SWITCHING DURING VIRTUAL CONNECTIONS & SIEMENS AG (DE) & 1990-04-27\\\hline
ep0400734\footnote{/patents/txt/ep/0400/734} & PROGRAMMABLE DIGITAL SIGNAL DELAY DEVICE AND ITS USE FOR A ERROR CORRECTION CODE DEVICE & PHILIPS CORP (US) & 1989-05-30\\\hline
ep0446194\footnote{/patents/txt/ep/0446/194} & Continous cipher synchronization for cellular communication system. & ERICSSON TELEFON AB L M (SE) & 1990-07-20\\\hline
ep0433699\footnote{/patents/txt/ep/0433/699} & Circuit arrangement for communication devices with asynchronous transfer mode. & SIEMENS AG (DE) & 1989-12-22\\\hline
ep0419721\footnote{/patents/txt/ep/0419/721} & Multiprocessor communication system having a paritioned main memory where individual processors write to exclusive portions of the main memory and read from the entire main memory & SIEMENS AG (DE) & 1989-09-29\\\hline
ep0458941\footnote{/patents/txt/ep/0458/941} & CONTROL MEANS FOR A COMPUTER SYSTEM AND DISPLAY UNIT WITH SUCH CONTROL MEANS & NOKIA DATA SYSTEMS (SE) & 1990-02-26\\\hline
ep0768619\footnote{/patents/txt/ep/0768/619} & Computer mouse with sensory alerting to prevent human injury & AT \& T CORP (US) & 1995-10-16\\\hline
ep0723726\footnote{/patents/txt/ep/0723/726} & System and apparatus for blockwise encryption/decryption of data & IRDETO BV (NL) & 1994-10-07\\\hline
ep0241255\footnote{/patents/txt/ep/0241/255} & Digital image processing system and method. & CROSFIELD ELECTRONICS LTD (GB) & 1986-04-07\\\hline
ep0476262\footnote{/patents/txt/ep/0476/262} & Error handling in a VLSI central processor unit employing a pipelined address and execution module. & BULL HN INFORMATION SYST (US) & 1990-09-18\\\hline
ep0419959\footnote{/patents/txt/ep/0419/959} & CIRCUIT FOR CHECKING THE DEFINED TRANSMISSION BIT RATES & SIEMENS AG (DE) & 1989-09-29\\\hline
ep0311187\footnote{/patents/txt/ep/0311/187} & Interface device for interfacing a network station to a physical network medium. & PHILIPS NV (NL) & 1987-10-08\\\hline
ep0513807\footnote{/patents/txt/ep/0513/807} & Switchboard terminal apparatus having a plurality of loop circuits. & NIPPON ELECTRIC CO (JP) & 1991-05-17\\\hline
ep0444655\footnote{/patents/txt/ep/0444/655} & Controller programming apparatus and process using ladder diagram technique. & MITSUBISHI ELECTRIC CORP (JP) & 1990-03-02\\\hline
ep0614142\footnote{/patents/txt/ep/0614/142} & System and method for detecting and correcting memory errors. & MOTOROLA INC (US) & 1993-03-05\\\hline
ep0410502\footnote{/patents/txt/ep/0410/502} & Method and apparatus for emulating interaction between application specific integrated circuit (asic) under development and target system & LSI LOGIC CORP (US) & 1989-07-27\\\hline
ep0664021\footnote{/patents/txt/ep/0664/021} & MENU STATE SYSTEM & TALIGENT INC (US) & 1992-12-23\\\hline
ep0696014\footnote{/patents/txt/ep/0696/014} & Pressure sensitive input device wearable around a human finger & HEWLETT PACKARD CO (US) & 1994-07-28\\\hline
ep0385404\footnote{/patents/txt/ep/0385/404} & Clock failure recovery system. & NIPPON ELECTRIC CO (JP) & 1989-02-28\\\hline
ep0534884\footnote{/patents/txt/ep/0534/884} & Task timeout prevention in a multi-task, real-time system. & IBM (US) & 1991-09-26\\\hline
ep0632377\footnote{/patents/txt/ep/0632/377} & Method for testing a message-driven operating system. & MICROSOFT CORP (US) & 1993-06-30\\\hline
ep0829071\footnote{/patents/txt/ep/0829/071} & Smart IC card system and smart IC card with transaction management program stored therein & SYSECA SA (FR) & 1996-05-28\\\hline
ep0202768\footnote{/patents/txt/ep/0202/768} & Technique for reducing RSA crypto variable storage. & IBM (US) & 1985-04-30\\\hline
ep0454443\footnote{/patents/txt/ep/0454/443} & Spectacles incorporating display means for a stereoscopic viewing system. & SONY CORP (JP) & 1990-04-24\\\hline
ep0562477\footnote{/patents/txt/ep/0562/477} & Printing head and its drive timing control circuit for impact printer & SEIKO EPSON CORP (JP) & 1992-08-25\\\hline
ep0578519\footnote{/patents/txt/ep/0578/519} & Vibrating beam gyroscopic measuring apparatus. & SAGEM (FR) & 1993-05-10\\\hline
ep0510487\footnote{/patents/txt/ep/0510/487} & Automated synthesis apparatus and method of controlling the apparatus. & TAKEDA CHEMICAL INDUSTRIES LTD (JP) & 1991-04-17\\\hline
ep0252085\footnote{/patents/txt/ep/0252/085} & Signal processing device with a level adapter circuit & CREATEC ELEKTRONIK GMBH (DE) & 1985-03-27\\\hline
ep0585237\footnote{/patents/txt/ep/0585/237} & Dynamical system analyser & SECRETARY OF THE STATE FOR DEF (GB) & 1992-03-03\\\hline
ep0375153\footnote{/patents/txt/ep/0375/153} & Detection system for chemical analysis of zinc phosphate coating solutions. & FORD MOTOR CO (GB); FORD FRANCE (FR); FORD WERKE AG (DE); FORD MOTOR CO (US) & 1988-12-22\\\hline
ep0595700\footnote{/patents/txt/ep/0595/700} & System for acquisition and reproduction of a video image sequence with real time animation. & JEUX FRANC DES (FR) & 1992-10-26\\\hline
ep0671092\footnote{/patents/txt/ep/0671/092} & Method and apparatus for providing cryptographic protection of a data stream in a communication system & MOTOROLA INC (US) & 1993-08-26\\\hline
ep0543821\footnote{/patents/txt/ep/0543/821} & Device for monitoring the functioning of external synchronization modules in a multicomputer system & SIEMENS AG (DE) & 1991-06-03\\\hline
ep0194268\footnote{/patents/txt/ep/0194/268} & SYSTEM AND METHOD FOR MAPPING GEOSYNCHRONOUS REAL IMAGE DATA INTO IDEALIZED IMAGES & HUGHES AIRCRAFT CO (US) & 1984-08-24\\\hline
ep0244665\footnote{/patents/txt/ep/0244/665} & Method and apparatus for stressing the data window in magnetic storage devices & IBM (US) & 1986-04-30\\\hline
ep0278317\footnote{/patents/txt/ep/0278/317} & A system and method for using cached data at a local node after re-opening a file at a remote node in a distributed networking environment. & IBM (US) & 1987-02-13\\\hline
ep0312865\footnote{/patents/txt/ep/0312/865} & Space management system for a data access system of a file access processor. & IBM (US) & 1987-10-19\\\hline
ep0187282\footnote{/patents/txt/ep/0187/282} & Digital drive system for pulse width modulated power control & ALLIED CORP (US) & 1985-01-07\\\hline
ep0282825\footnote{/patents/txt/ep/0282/825} & Digital signal processor. & MITSUBISHI ELECTRIC CORP (JP) & 1987-03-05\\\hline
ep0343083\footnote{/patents/txt/ep/0343/083} & Loss of synchronisation detection device, and its use in a digital transmission network. & THOMSON CSF (FR) & 1988-05-20\\\hline
ep0448534\footnote{/patents/txt/ep/0448/534} & Method and apparatus for encryption/decryption of digital multisound in television. & TELEVERKET (SE) & 1990-03-23\\\hline
ep0499719\footnote{/patents/txt/ep/0499/719} & Digital image reproduction system. & OCE NEDERLAND BV (NL) & 1991-02-20\\\hline
ep0390770\footnote{/patents/txt/ep/0390/770} & Method of electronically correcting position errors in an incremental measuring system and measuring system for carrying out the method & RSF ELEKTRONIK GMBH (AT) & 1989-03-29\\\hline
ep0598511\footnote{/patents/txt/ep/0598/511} & A method and apparatus for remotely downloading and executing files in a memory. & CANON INFORMATION SYST INC (US) & 1992-11-18\\\hline
ep0363828\footnote{/patents/txt/ep/0363/828} & Method and apparatus for adaptive learning type general purpose image measurement and recognition. & AGENCY IND SCIENCE TECHN (JP); OYO KEISOKU KENKYUSHO KK (JP) & 1988-10-11\\\hline
ep0450929\footnote{/patents/txt/ep/0450/929} & Image outputting apparatus. & CANON KK (JP) & 1990-04-05\\\hline
ep0566300\footnote{/patents/txt/ep/0566/300} & Image processing system and method employing adaptive scanning of halftones to provide better printable images. & XEROX CORP (US) & 1992-04-17\\\hline
ep0471373\footnote{/patents/txt/ep/0471/373} & Information processing apparatus with replaceable security element. & GEN INSTRUMENT CORP (US) & 1990-08-17\\\hline
\end{tabular}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

