\begin{subdocument}{swpiktxt1}{Top Software Probability Batch nr 1: 1-100}{http://swpat.ffii.org/patents/txt/ep1/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{longtable}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
\endhead
ep0587827\footnote{/patents/txt/ep/0587/827} & \#f & PROLINK AG (CH) & 1992-01-31\\\hline
ep0823173\footnote{/patents/txt/ep/0823/173} & Method and apparatus of using virtual sockets for reducing data transmitted over a wireless communication link between a client web browser and a host web server using a standard TCP protocol & IBM (US) & 1996-02-15\\\hline
ep0747845\footnote{/patents/txt/ep/0747/845} & Computer network for WWW server data access over internet & IBM (US) & 1995-06-07\\\hline
ep0747840\footnote{/patents/txt/ep/0747/840} & A method for fulfilling requests of a web browser & IBM (US) & 1995-06-07\\\hline
ep0821817\footnote{/patents/txt/ep/0821/817} & CONTROL SYSTEMS BASED ON SIMULATED VIRTUAL MODELS & INTERTECH VENTURES LTD (US); THALHAMMER REYERO CRISTINA (US) & 1995-01-17\\\hline
ep0437422\footnote{/patents/txt/ep/0437/422} & Communication system for forming virtual, annular networks in a time-division multiplex packet switching network & SIEMENS AG (DE) & 1988-09-30\\\hline
ep0700000\footnote{/patents/txt/ep/0700/000} & System and method combining a global object identifier with a local object address in a single object pointer & IBM (US) & 1994-08-30\\\hline
ep0748474\footnote{/patents/txt/ep/0748/474} & PREBOOT PROTECTION FOR A DATA SECURITY SYSTEM & INTEGRATED TECH AMERICA (US); MOONEY DAVID M (US); WOOD DAVID E (US); KIMLINGER JOSEPH A (US) & 1995-02-28\\\hline
ep0792493\footnote{/patents/txt/ep/0792/493} & AN ONLINE SERVICE DEVELOPMENT TOOL WITH FEE SETTING CAPABILITIES & VERMEER TECH INC (US) & 1994-11-08\\\hline
ep0613080\footnote{/patents/txt/ep/0613/080} & Graphical user interface incorporating a horizontal panning workspace. & SONY ELECTRONICS INC (US) & 1993-02-24\\\hline
ep0597592\footnote{/patents/txt/ep/0597/592} & Distributed system and associated control method. & HITACHI SEIBU SOFTWARE KK (JP); HITACHI LTD (JP) & 1992-10-12\\\hline
ep0052755\footnote{/patents/txt/ep/0052/755} & Single screen display system with multiple virtual display having prioritized service programs and dedicated memory stacks & IBM (US) & 1980-11-20\\\hline
ep0490973\footnote{/patents/txt/ep/0490/973} & PARALLEL I/O NETWORK FILE SERVER ARCHITECTURE & AUSPEX SYSTEMS INC (US) & 1989-09-08\\\hline
ep0359815\footnote{/patents/txt/ep/0359/815} & CACHE WITH AT LEAST TWO FILL SIZES & DIGITAL EQUIPMENT CORP (US) & 1988-04-01\\\hline
ep0106664\footnote{/patents/txt/ep/0106/664} & Method and apparatus for initiating the execution of instructions using a central pipeline execution unit & WILHITE JOHN E (US); SHELLY WILLIAM A (US); GUENTHNER RUSSELL W (US); TRUBISKY LEONARD G (US); CIRCELLO JOSEPH C (US) & 1982-10-13\\\hline
ep0442297\footnote{/patents/txt/ep/0442/297} & Processor with an overwriteable microcode memory. & SONY CORP (JP) & 1990-01-24\\\hline
ep0644483\footnote{/patents/txt/ep/0644/483} & Computer system and method for performing multiple tasks. & IBM (US) & 1993-09-14\\\hline
ep0591193\footnote{/patents/txt/ep/0591/193} & STORED PROGRAM CONTROLLED DIGITAL PUBLIC EXCHANGE & ERICSSON TELEFON AB L M (SE) & 1991-06-28\\\hline
ep0254247\footnote{/patents/txt/ep/0254/247} & Method and storage device for saving the computer status during interrupt & BBC BROWN BOVERI \& CIE (CH) & 1984-04-26\\\hline
ep0232526\footnote{/patents/txt/ep/0232/526} & Virtual cache system using page level number generating CAM to access other memories for processing requests relating to a page & HONEYWELL BULL (US) & 1985-12-19\\\hline
ep0242131\footnote{/patents/txt/ep/0242/131} & Graphical system for modelling a process and associated method & NAT INSTR INC (US) & 1986-04-14\\\hline
ep0600979\footnote{/patents/txt/ep/0600/979} & Method and apparatus for consolidating software module linkage information used for starting a multi-module program & REC SOFTWARE INC (CA) & 1991-08-22\\\hline
ep0879459\footnote{/patents/txt/ep/0879/459} & Process for obtaining traffic data & MANNESMANN AG (DE) & 1996-02-08\\\hline
ep0660252\footnote{/patents/txt/ep/0660/252} & Computer simulation of live organ. & BEAVIN WILLIAM C (US) & 1991-12-03\\\hline
ep0522591\footnote{/patents/txt/ep/0522/591} & Database retrieval system for responding to natural language queries with corresponding tables. & MITSUBISHI ELECTRIC CORP (JP) & 1991-07-11\\\hline
ep0301275\footnote{/patents/txt/ep/0301/275} & Logical resource partitioning of a data processing system. & IBM (US) & 1987-07-29\\\hline
ep0447145\footnote{/patents/txt/ep/0447/145} & User scheduled direct memory access using virtual addresses. & HEWLETT PACKARD CO (US) & 1990-03-12\\\hline
ep0528028\footnote{/patents/txt/ep/0528/028} & Successive translation, execution and interpretation of computer program having code at unknown locations due to execution transfer instructions having computed destination addresses & DIGITIAL EQUIPMENT CORP (US) & 1991-03-07\\\hline
ep0645757\footnote{/patents/txt/ep/0645/757} & Semantic co-occurrence filtering for speech recognition and signal transcription applications. & XEROX CORP (US) & 1993-09-23\\\hline
ep0642102\footnote{/patents/txt/ep/0642/102} & Apparatus and method for image processing. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1993-08-31\\\hline
ep0176941\footnote{/patents/txt/ep/0176/941} & Circuit arrangement for clearing storage entries in an address translation memory. & SIEMENS AG (DE) & 1984-09-28\\\hline
ep0365960\footnote{/patents/txt/ep/0365/960} & Computer animation production system. & WALT DISNEY PROD (US) & 1988-10-24\\\hline
ep0803100\footnote{/patents/txt/ep/0803/100} & Interactive database query system and method for prohibiting the selection of semantically incorrect query parameters & SOFTWARE AG (DE) & 1994-03-24\\\hline
ep0663771\footnote{/patents/txt/ep/0663/771} & Method of transmitting signals between communication stations. & PHILIPS ELECTRONICS NV (NL) & 1994-01-18\\\hline
ep0651895\footnote{/patents/txt/ep/0651/895} & Sequential information integration service for automatically transferring a most recent data entity between a plurality of program modules and a storage in a computer & HUGHES AIRCRAFT CO (US) & 1993-05-24\\\hline
ep0549677\footnote{/patents/txt/ep/0549/677} & NETWORK MANAGEMENT SYSTEM USING MODEL-BASED INTELLIGENCE & CABLETRON SYSTEMS INC (US) & 1990-09-17\\\hline
ep0798657\footnote{/patents/txt/ep/0798/657} & Virtual shop computer network system which displays member shops and member shop certification method & TOKYO SHIBAURA ELECTRIC CO (JP) & 1996-03-29\\\hline
ep0005179\footnote{/patents/txt/ep/0005/179} & Method for authenticating the identity of a user of an information system & IBM & 1978-05-05\\\hline
ep0541535\footnote{/patents/txt/ep/0541/535} & BILLING SYSTEM & COMPUCOM COMMUNICATIONS CORP (US) & 1989-08-14\\\hline
ep0858635\footnote{/patents/txt/ep/0858/635} & System for generating a virtual reality scene in response to a database search & WEGENER INTERNET PROJECTS BV (NL) & 1995-10-26\\\hline
ep0706140\footnote{/patents/txt/ep/0706/140} & Intelligent data warehouse & HEWLETT PACKARD CO (US) & 1994-10-04\\\hline
ep0820630\footnote{/patents/txt/ep/0820/630} & METHOD AND DEVICE FOR STORING, SEARCHING FOR AND PLAYING BACK AUDIOVISUAL DATA AND DATA FILES & SIEMENS AG (DE); SEBESTYEN ISTVAN (DE) & 1995-04-13\\\hline
ep0786109\footnote{/patents/txt/ep/0786/109} & Object-oriented system for configuration history management with a project workspace and project history database for draft identification & OBJECT TECH LICENSING CORP (US) & 1995-12-05\\\hline
ep0398494\footnote{/patents/txt/ep/0398/494} & Maintenance of file attributes in a distributed data processing system & IBM (US) & 1989-05-15\\\hline
ep0254140\footnote{/patents/txt/ep/0254/140} & Method for forming data block protection information for serial data bit sequences by means of cyclical binary codes & SIEMENS AG (DE) & 1986-07-21\\\hline
ep0490624\footnote{/patents/txt/ep/0490/624} & Graphical configuration of data processing networks. & IBM (US) & 1990-12-10\\\hline
ep0798641\footnote{/patents/txt/ep/0798/641} & Imbedding virtual device driver calls in a dynamic link library & SUN MICROSYSTEMS INC (US) & 1996-03-29\\\hline
ep0538288\footnote{/patents/txt/ep/0538/288} & DELETED DATA FILE SPACE RELEASE SYSTEM FOR A DYNAMICALLY MAPPED VIRTUAL DATA STORAGE SUBSYSTEM & STORAGE TECHNOLOGY CORP (US) & 1990-06-18\\\hline
ep0655684\footnote{/patents/txt/ep/0655/684} & Branch decision encoding scheme. & ROCKWELL INTERNATIONAL CORP (US) & 1993-11-23\\\hline
ep0425999\footnote{/patents/txt/ep/0425/999} & Transmitter and receiver circuit for local computer networks with collision detection. & UNIV DRESDEN TECH (DE) & 1989-11-02\\\hline
ep0440445\footnote{/patents/txt/ep/0440/445} & System memory initialization with presence detect encoding. & HEWLETT PACKARD CO (US) & 1990-01-31\\\hline
ep0752786\footnote{/patents/txt/ep/0752/786} & Apparatus and method for authenticating transmitted applications in an interactive information system & THOMSON CONSUMER ELECTRONICS (US) & 1995-07-07\\\hline
ep0391706\footnote{/patents/txt/ep/0391/706} & A method encoding text. & XEROX CORP (US) & 1989-04-05\\\hline
ep0329779\footnote{/patents/txt/ep/0329/779} & SESSION CONTROL IN NETWORK FOR DIGITAL DATA PROCESSING SYSTEM WHICH SUPPORTS MULTIPLE TRANSFER PROTOCOLS & DIGITAL EQUIPMENT CORP (US) & 1987-09-04\\\hline
ep0667063\footnote{/patents/txt/ep/0667/063} & Process for transmitting and/or storing digital signals of multiple channels & FRAUNHOFER GESELLESCHAFT ZUR F (DE) & 1993-11-02\\\hline
ep0880739\footnote{/patents/txt/ep/0880/739} & Method of load balancing across the processors of a server & IBM (US) & 1996-01-26\\\hline
ep0365928\footnote{/patents/txt/ep/0365/928} & Process for the evaluation of cell pictures & ZEISS STIFTUNG (DE) & 1988-10-28\\\hline
ep0421025\footnote{/patents/txt/ep/0421/025} & Data processing system with a touch screen and a digitizing tablet, both integrated in an output device. & KONINKL PHILIPS ELECTRONICS NV (NL) & 1989-10-02\\\hline
ep0297893\footnote{/patents/txt/ep/0297/893} & Apparatus and method for recovering from missing page faults in vector data processing operations. & DIGITAL EQUIPMENT CORP (US) & 1987-07-01\\\hline
ep0907925\footnote{/patents/txt/ep/0907/925} & COMMUNICATION SYSTEM FOR TRANSMITTING ACCOUNTING INSTRUCTIONS & MANNESMANN AG (DE); MIHATSCH PETER (DE) & 1996-06-27\\\hline
ep0362105\footnote{/patents/txt/ep/0362/105} & Method for processing program threads of a distributed application program by a host computer and an intelligent work station in an SNA LU 6.2 network environment. & IBM (US) & 1988-09-29\\\hline
ep0705503\footnote{/patents/txt/ep/0705/503} & Frame relay protocol-based multiplex switching scheme for satellite & SKYDATA CORP (US) & 1993-06-17\\\hline
ep0453937\footnote{/patents/txt/ep/0453/937} & Channel selection method and system thereof for receiving a video signal in multi TV broadcasting system & SAMSUNG ELECTRONICS CO LTD (KR) & 1990-04-21\\\hline
ep0573248\footnote{/patents/txt/ep/0573/248} & One-time logon means and methods for distributed computing systems. & HUGHES AIRCRAFT CO (US) & 1992-06-02\\\hline
ep0176939\footnote{/patents/txt/ep/0176/939} & Data-processing systems with virtual storage addressing for a plurality of users. & SIEMENS AG (DE) & 1984-09-28\\\hline
ep0288906\footnote{/patents/txt/ep/0288/906} & Computer input by color coding. & IBM (US) & 1987-05-01\\\hline
ep0600235\footnote{/patents/txt/ep/0600/235} & Cooperative processing interface and communication broker for heterogeneous computing environments. & SOFTWARE AG (DE) & 1992-10-30\\\hline
ep0769170\footnote{/patents/txt/ep/0769/170} & COMPUTER VIRUS TRAP & QUANTUM LEAP INNOVATIONS INC (US) & 1994-06-01\\\hline
ep0284764\footnote{/patents/txt/ep/0284/764} & Improved user transaction guidance. & IBM (US) & 1987-03-02\\\hline
ep0248436\footnote{/patents/txt/ep/0248/436} & System for processing data with multiple virtual address and data word lengths & HITACHI LTD (JP) & 1986-06-04\\\hline
ep0347032\footnote{/patents/txt/ep/0347/032} & Record format emulation. & IBM (US) & 1988-05-20\\\hline
ep0272835\footnote{/patents/txt/ep/0272/835} & Virtual execution of programs on a multiprocessor system. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1986-12-22\\\hline
ep0406077\footnote{/patents/txt/ep/0406/077} & COMPLEMENTARY COMMUNICATION SYSTEM IN THE NO-CONNECTION MODE FOR ASYNCHRONOUS TIME-DIVISION NETWORK & FRANCE ETAT (FR) & 1989-06-30\\\hline
ep0519814\footnote{/patents/txt/ep/0519/814} & Apparatus for measuring the data rate of virtual channels on an asynchronous time division multiplex communication link. & ALCATEL NV (NL); CIT ALCATEL (FR) & 1991-06-18\\\hline
ep0409604\footnote{/patents/txt/ep/0409/604} & Processing method by which continuous operation of communication control program is obtained. & FUJITSU LTD (JP) & 1989-07-20\\\hline
ep0858633\footnote{/patents/txt/ep/0858/633} & Virtual maintenance network in multiprocessing system having a non-flow controlled virtual maintenance channel & CRAY RESEARCH INC (US) & 1995-10-31\\\hline
ep0441032\footnote{/patents/txt/ep/0441/032} & Keyboard emulation system. & HEWLETT PACKARD CO (US) & 1989-12-29\\\hline
ep0328232\footnote{/patents/txt/ep/0328/232} & Public key/signature cryptosystem with enhanced digital signature certification. & FISCHER ADDISON M & 1988-02-12\\\hline
ep0617871\footnote{/patents/txt/ep/0617/871} & In-station television program encoding and monitoring system and method & NIELSEN A C CO (US) & 1991-11-22\\\hline
ep0425163\footnote{/patents/txt/ep/0425/163} & Customer programmable automated integrated voice/data technique for communication systems. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1989-10-24\\\hline
ep0756420\footnote{/patents/txt/ep/0756/420} & Information provider apparatus enabling selective playing of multimedia information by interactive input based on displayed hypertext information & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1996-05-30\\\hline
ep0230664\footnote{/patents/txt/ep/0230/664} & Coprocessor having a slave processor capable of checking address mapping & NIPPON ELECTRIC CO (JP) & 1985-12-25\\\hline
ep0340613\footnote{/patents/txt/ep/0340/613} & Integrated Modem which operates without a dedicated controller. & NAT SEMICONDUCTOR CORP (US) & 1988-05-02\\\hline
ep0594304\footnote{/patents/txt/ep/0594/304} & Opcode specific compression for window system. & TEKTRONIX INC (US) & 1992-10-20\\\hline
ep0455402\footnote{/patents/txt/ep/0455/402} & Automatic discovery of network elements. & HEWLETT PACKARD CO (US) & 1990-05-03\\\hline
ep0250876\footnote{/patents/txt/ep/0250/876} & Apparatus and method for implementation of a page frame replacement algorithm in a data processing system having virtual memory addressing & BULL HN INFORMATION SYST (US) & 1986-05-30\\\hline
ep0425421\footnote{/patents/txt/ep/0425/421} & Process and apparatus for manipulating a boundless data stream in an object oriented programming system. & IBM (US) & 1989-10-23\\\hline
ep0469709\footnote{/patents/txt/ep/0469/709} & System and method for emulating a window management environment having a uniform windowing interface. & DIGITAL EQUIPMENT CORP (US) & 1990-07-31\\\hline
ep0722589\footnote{/patents/txt/ep/0722/589} & VIRTUAL GRAPHICS PROCESSOR FOR EMBEDDED, REAL TIME DISPLAY SYSTEMS & HONEYWELL INC (US) & 1993-10-06\\\hline
ep0526034\footnote{/patents/txt/ep/0526/034} & Method and apparatus for accessing a computer-based file system. & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1991-07-24\\\hline
ep0529915\footnote{/patents/txt/ep/0529/915} & Voice processing interface for a computer. & IBM (US) & 1991-08-27\\\hline
ep0667011\footnote{/patents/txt/ep/0667/011} & Operating system for use with computer networks incorporating one or more data processors linked together for parallel processing and incorporating improved dynamic binding and/or load-sharing techniques & TAO GROUP LTD (GB) & 1993-07-01\\\hline
ep0507884\footnote{/patents/txt/ep/0507/884} & TRANSMITTING ENCODED DATA ON UNRELIABLE NETWORKS & CODEX CORP (US) & 1989-12-29\\\hline
ep0890264\footnote{/patents/txt/ep/0890/264} & System and method for inserting interactive program content within a television signal originating at a remote network & OPENTV INC (US); SUN MICROSYSTEMS INC (US) & 1996-03-11\\\hline
ep0254854\footnote{/patents/txt/ep/0254/854} & Method for multiple programs management within a network having a server computer and a plurality of remote computers & IBM (US) & 1986-07-29\\\hline
ep0415280\footnote{/patents/txt/ep/0415/280} & Method for on-line monitoring of an electric arc furnace and method of control. & UCAR CARBON TECH (US) & 1989-08-28\\\hline
ep0764381\footnote{/patents/txt/ep/0764/381} & Multi-cast digital video data server using synchronization groups & UNISYS CORP (US) & 1994-06-07\\\hline
ep0518500\footnote{/patents/txt/ep/0518/500} & Video mixers. & SONY CORP AMERICA (US) & 1991-06-14\\\hline
ep0723247\footnote{/patents/txt/ep/0723/247} & Document image assessment system and method & EASTMAN KODAK CO (US) & 1995-01-17\\\hline
ep0380211\footnote{/patents/txt/ep/0380/211} & Method for information communication between concurrently operating computer programs. & LANDMARK GRAPHICS CORP (US) & 1989-01-17\\\hline
\end{longtable}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: latex ;
% End: ;

