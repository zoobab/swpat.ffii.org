\begin{subdocument}{swpiktxt16}{Top Software Probability Batch nr 16: 1501-1600}{http://swpat.ffii.org/patents/txt/ep16/index.en.html}{Workgroup\\swpatag@ffii.org}{During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reason (mathematical proof) rather than verified by means of experimentation with natural forces.  Below you find a table of 100 patents granted by the EPO for software principles and problems.  They were selected mechanically on the basis of probability calculations based on key words.  They still need to be reviewed by humans.}
\begin{center}
\begin{longtable}{|C{21}|C{21}|C{21}|C{21}|}
\hline
patent number & Name of the Invention & applicant & priority date\\\hline
\endhead
ep0228053\footnote{/patents/txt/ep/0228/053} & Control of real-time systems utilizing a nonprocedural language & AMERICAN TELEPHONE \& TELEGRAPH (US) & 1985-12-23\\\hline
ep0260433\footnote{/patents/txt/ep/0260/433} & Command controlled multi-storage space protection key pretesting system permitting access regardless of test result if selected key is predetermined value & HITACHI LTD (JP) & 1986-09-19\\\hline
ep0408684\footnote{/patents/txt/ep/0408/684} & Turbine engine monitoring system & SUNDSTRAND CORP (US) & 1988-11-16\\\hline
ep0505706\footnote{/patents/txt/ep/0505/706} & Alternate processor continuation of the task of a failed processor & IBM (US) & 1991-03-26\\\hline
ep0385511\footnote{/patents/txt/ep/0385/511} & Cipher-key distribution system. & NIPPON ELECTRIC CO (JP) & 1989-03-30\\\hline
ep0501920\footnote{/patents/txt/ep/0501/920} & Apparatus for controlling weft insertion in jet loom. & TOYODA AUTOMATIC LOOM WORKS (JP) & 1991-02-25\\\hline
ep0558170\footnote{/patents/txt/ep/0558/170} & Tape volume partitioning. & AMPEX (US); E SYSTEMS INC (US) & 1992-02-28\\\hline
ep0586897\footnote{/patents/txt/ep/0586/897} & System for selecting route-relevant information when using the radio data system (RDS) & BOSCH GMBH ROBERT (DE) & 1992-09-10\\\hline
ep0846991\footnote{/patents/txt/ep/0846/991} & Central control and monitoring apparatus for household appliances with wireless display unit & MIELE \& CIE (DE) & 1996-12-04\\\hline
ep0491121\footnote{/patents/txt/ep/0491/121} & Movable video camera for surveillance. & SAMSUNG ELECTRONICS CO LTD (KR) & 1990-12-15\\\hline
ep0284635\footnote{/patents/txt/ep/0284/635} & A control system for cooking apparatus. & FOOD AUTOMATION SERVICE TECH (US) & 1986-03-17\\\hline
ep0588107\footnote{/patents/txt/ep/0588/107} & Method for function definition and control unit device for an oven. & ELECTROLUX AB (SE) & 1992-09-17\\\hline
ep0189456\footnote{/patents/txt/ep/0189/456} & Multipurpose digital IC for communication and control network & WESTINGHOUSE ELECTRIC CORP (US) & 1984-06-28\\\hline
ep0389339\footnote{/patents/txt/ep/0389/339} & NETWORK FOR INTERACTIVE DISTRIBUTION OF VIDEO, AUDIO AND TELEMATIC INFORMATION & THOMSON CONSUMER ELECTRONICS (FR) & 1989-03-21\\\hline
ep0419201\footnote{/patents/txt/ep/0419/201} & Communication control system between parallel computers. & FUJITSU LTD (JP) & 1989-10-26\\\hline
ep0361387\footnote{/patents/txt/ep/0361/387} & Automatic operation control system for computer system. & HITACHI LTD (JP); HITACHI SOFTWARE ENG (JP) & 1988-09-26\\\hline
ep0556295\footnote{/patents/txt/ep/0556/295} & PROTECTED HOT KEY FUNCTION FOR MICROPROCESSOR-BASED COMPUTER SYSTEM & AST RESEARCH INC (US) & 1990-11-09\\\hline
ep0654726\footnote{/patents/txt/ep/0654/726} & Computer power management systems. & ADVANCED MICRO DEVICES INC (US) & 1993-11-23\\\hline
ep0648352\footnote{/patents/txt/ep/0648/352} & System for dynamic run-time binding of software modules in a computer system & ERICSSON TELEFON AB L M (SE) & 1992-07-01\\\hline
ep0238364\footnote{/patents/txt/ep/0238/364} & Console unit for clustered digital data processing system & DIGITAL EQUIPMENT CORP (US) & 1986-01-13\\\hline
ep0462180\footnote{/patents/txt/ep/0462/180} & Method of reading and writing files on nonerasable storage media & DREXLER TECH (US) & 1989-03-07\\\hline
ep0508412\footnote{/patents/txt/ep/0508/412} & Microcomputer with rapid-access memory and compiler optimised for use therewith. & NIPPON ELECTRIC CO (JP) & 1991-04-09\\\hline
ep0487415\footnote{/patents/txt/ep/0487/415} & Dimensional quality control method for cast parts & HISPANO SUIZA SA (FR) & 1990-11-21\\\hline
ep0168029\footnote{/patents/txt/ep/0168/029} & Word processor with type through mode & MINOLTA CAMERA KK (JP) & 1984-07-09\\\hline
ep0282123\footnote{/patents/txt/ep/0282/123} & Crytographic system and process and its application & PHILIPS CORP (US) & 1987-03-06\\\hline
ep0382391\footnote{/patents/txt/ep/0382/391} & An operation state responsive automatic display selection system. & WESTINGHOUSE ELECTRIC CORP (US) & 1989-02-07\\\hline
ep0318507\footnote{/patents/txt/ep/0318/507} & METHOD AND APPARATUS FOR COMMUNICATION OF VIDEO, AUDIO, TELETEXT, AND DATA TO GROUPS OF DECODERS IN A COMMUNICATION SYSTEM & SCIENTIFIC ATLANTA (US) & 1986-08-14\\\hline
ep0473367\footnote{/patents/txt/ep/0473/367} & Digital signal encoders. & SONY CORP (JP) & 1990-08-24\\\hline
ep0655683\footnote{/patents/txt/ep/0655/683} & Circuit architecture and corresponding method for testing a programmable logic matrix. & ST MICROELECTRONICS SRL (IT) & 1993-11-30\\\hline
ep0588446\footnote{/patents/txt/ep/0588/446} & Programmable computer with automatic translation between source and object code with version control. & AMDAHL CORP (US) & 1989-12-13\\\hline
ep0211438\footnote{/patents/txt/ep/0211/438} & Device for transmitting packets in an asynchronous time-division network, and method of encoding silences & CIT ALCATEL (FR) & 1985-08-07\\\hline
ep0173171\footnote{/patents/txt/ep/0173/171} & Method and arrangement for controlling the access to a group of shared devices as pooled elements in a data processing system or in a communication system. & SIEMENS AG (DE) & 1984-08-16\\\hline
ep0588445\footnote{/patents/txt/ep/0588/445} & Programmable computer with automatic translation between source and object code with version control. & AMDAHL CORP (US) & 1989-12-13\\\hline
ep0544431\footnote{/patents/txt/ep/0544/431} & Methods and apparatus for selecting semantically significant images in a document image without decoding image content. & XEROX CORP (US) & 1991-11-19\\\hline
ep0800670\footnote{/patents/txt/ep/0800/670} & Dynamic user interrupt scheme in a programmable logic controller & SIEMENS ENERGY \& AUTOMAT (US) & 1994-12-29\\\hline
ep0726003\footnote{/patents/txt/ep/0726/003} & Mapping and analysis system for precision farming applications & TRW INC (US) & 1993-12-17\\\hline
ep0040219\footnote{/patents/txt/ep/0040/219} & Digital data processor providing for monitoring, changing and loading of RAM instruction data & BURROUGHS CORP & 1979-10-16\\\hline
ep0295469\footnote{/patents/txt/ep/0295/469} & Method for a calculator controlled switching device, especially for a so-called key telephone switching device. & SIEMENS AG (DE) & 1987-06-16\\\hline
ep0210094\footnote{/patents/txt/ep/0210/094} & Training device for dental hygiene. & ARNAUD DANIEL & 1985-07-09\\\hline
ep0490511\footnote{/patents/txt/ep/0490/511} & Nonvolatile serially programmable devices. & HUGHES AIRCRAFT CO (US) & 1990-12-10\\\hline
ep0237958\footnote{/patents/txt/ep/0237/958} & Device at the receiver side with an operation mode controlled from the transmitter side. & THOMSON BRANDT GMBH (DE) & 1986-03-17\\\hline
ep0253698\footnote{/patents/txt/ep/0253/698} & Method for the analysis of immunogenic peptide sequences for the production of vaccines. & PASTEUR INSTITUT (FR); INST NAT SANTE RECH MED (FR) & 1986-06-19\\\hline
ep0196244\footnote{/patents/txt/ep/0196/244} & Cache MMU system. & FAIRCHILD SEMICONDUCTOR (US) & 1985-02-22\\\hline
ep0505771\footnote{/patents/txt/ep/0505/771} & Communication satellite system having an increased power output density per unit of bandwidth. & HUGHES AIRCRAFT CO (US) & 1991-03-26\\\hline
ep0491342\footnote{/patents/txt/ep/0491/342} & Method for fast communication between user program and operating system. & CRAY RESEARCH INC (US) & 1990-12-19\\\hline
ep0312194\footnote{/patents/txt/ep/0312/194} & Data processor having two modes of operation. & HITACHI LTD (JP) & 1987-10-09\\\hline
ep0342732\footnote{/patents/txt/ep/0342/732} & CONTROL UNIT FOR PROCESSING INSTRUCTION USING CHAINED PROCESSING MODULES BY PROVIDING DOWNSTREAM FLOW OF OPERATIVE COMMANDS AND UPSTREAM VALIDATION INFORMATION & PHILIPS CORP (US) & 1988-05-11\\\hline
ep0402542\footnote{/patents/txt/ep/0402/542} & Method of removing uncommitted changes to stored data by a database management system. & IBM (US) & 1989-06-13\\\hline
ep0259662\footnote{/patents/txt/ep/0259/662} & Method and apparatus for isolating faults in a digital logic circuit & DIGITAL EQUIPMENT CORP (US) & 1986-09-02\\\hline
ep0308450\footnote{/patents/txt/ep/0308/450} & \#f &  & 1987-03-17\\\hline
ep0337336\footnote{/patents/txt/ep/0337/336} & Applications for information transmitted in the vertical retrace interval of a television signal. & RCA LICENSING CORP (US) & 1988-04-15\\\hline
ep0661875\footnote{/patents/txt/ep/0661/875} & Program table displaying apparatus. & MATSUSHITA ELECTRIC IND CO LTD (JP) & 1993-12-27\\\hline
ep0182678\footnote{/patents/txt/ep/0182/678} & Terminal for communication with a remote data processing system & MATRA COMMUNICATION (US) & 1984-09-19\\\hline
ep0555818\footnote{/patents/txt/ep/0555/818} & Device and process for electrical-discharge machining of a three-dimensional cavity with a thin rotating tool electrode & CHARMILLES TECHNOLOGIES (CH) & 1992-02-12\\\hline
ep0450341\footnote{/patents/txt/ep/0450/341} & Cardiac analyzer with rem sleep detection. & HEWLETT PACKARD CO (US) & 1990-04-05\\\hline
ep0209693\footnote{/patents/txt/ep/0209/693} & Method of maintaining compatibility with different I/O types. & IBM (US) & 1985-07-22\\\hline
ep0218176\footnote{/patents/txt/ep/0218/176} & Programmable portable electronic device & TOKYO SHIBAURA ELECTRIC CO (JP) & 1985-10-07\\\hline
ep0239283\footnote{/patents/txt/ep/0239/283} & Microcomputer. & HITACHI LTD (JP) & 1986-03-26\\\hline
ep0399897\footnote{/patents/txt/ep/0399/897} & SYSTEM AND INDIVIDUAL DEVICE FOR PARTICIPATION IN A BROADCAST PROGRAM & ADVENTURE (FR) & 1990-02-09\\\hline
ep0589743\footnote{/patents/txt/ep/0589/743} & Modular device for coupling and multiplexing different type buses & SEXTANT AVIONIQUE (FR) & 1992-09-24\\\hline
ep0597339\footnote{/patents/txt/ep/0597/339} & Disc recording/reproducing apparatus. & KENWOOD CORP (JP) & 1992-10-29\\\hline
ep0600739\footnote{/patents/txt/ep/0600/739} & Video ghost cancellation. & SAMSUNG ELECTRONICS CO LTD (KR) & 1992-12-02\\\hline
ep0852375\footnote{/patents/txt/ep/0852/375} & Speech coder methods and systems & LUCENT TECHNOLOGIES INC (US) & 1996-12-19\\\hline
ep0166595\footnote{/patents/txt/ep/0166/595} & Information communication system & TOKYO SHIBAURA ELECTRIC CO (JP) & 1984-06-25\\\hline
ep0753184\footnote{/patents/txt/ep/0753/184} & BUMP MAPPING IN 3-D COMPUTER GRAPHICS & WARNES PETER ROBERT (GB); ARGONAUT TECHN LTD (GB) & 1994-03-31\\\hline
ep0468535\footnote{/patents/txt/ep/0468/535} & Microcomputer having ROM data protection function. & NIPPON ELECTRIC CO (JP) & 1990-07-27\\\hline
ep0617866\footnote{/patents/txt/ep/0617/866} & Apparatus and method for formatting variable length data packets for a transmission network & RAYNET CORP (US) & 1991-12-20\\\hline
ep0321290\footnote{/patents/txt/ep/0321/290} & Color cell texture. & GEN ELECTRIC (US) & 1987-12-18\\\hline
ep0372734\footnote{/patents/txt/ep/0372/734} & Name pronunciation by synthesizer. & DIGITAL EQUIPMENT CORP (US) & 1988-11-23\\\hline
ep0579359\footnote{/patents/txt/ep/0579/359} & Display control method and apparatus & CANON KK (JP) & 1992-05-19\\\hline
ep0249487\footnote{/patents/txt/ep/0249/487} & Apparatus and method for locating a vehicle in a working area and for the on-board measuring of parameters indicative of vehicle performance. & HAGENBUCH ROY GEORGE LE & 1986-06-13\\\hline
ep0567660\footnote{/patents/txt/ep/0567/660} & Device for the guiding of vehicles. & PIETZSCH IBP GMBH (DE) & 1992-04-21\\\hline
ep0178550\footnote{/patents/txt/ep/0178/550} & Data communication system comprising a bus and a plurality of units connected thereto. & ELXSI (US) & 1981-10-21\\\hline
ep0285381\footnote{/patents/txt/ep/0285/381} & Computer image generation with topographical response & GEN ELECTRIC (US) & 1987-03-30\\\hline
ep0436365\footnote{/patents/txt/ep/0436/365} & Method and system for securing terminals. & DIGITAL EQUIPMENT CORP (US) & 1989-12-26\\\hline
ep0834105\footnote{/patents/txt/ep/0834/105} & System for providing a host computer with access to a memory on a PCMCIA card in a power down mode & ADVANCED MICRO DEVICES INC (US) & 1995-06-07\\\hline
ep0624876\footnote{/patents/txt/ep/0624/876} & Recording and reproduction of digital video and audio data. & SONY CORP (JP) & 1993-05-10\\\hline
ep0125411\footnote{/patents/txt/ep/0125/411} & Data and text processing system having terminals with dual emulation capability. & IBM (US) & 1983-05-11\\\hline
ep0620654\footnote{/patents/txt/ep/0620/654} & Decoder. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1993-08-06\\\hline
ep0677843\footnote{/patents/txt/ep/0677/843} & RECORDING MEDIUM WHICH CAN COPE WITH VARIOUS LANGUAGES AND REPRODUCTION APPARATUS. & TOKYO SHIBAURA ELECTRIC CO (JP) & 1993-10-29\\\hline
ep0429283\footnote{/patents/txt/ep/0429/283} & Image processing apparatus. & CANON KK (JP) & 1989-11-20\\\hline
ep0606121\footnote{/patents/txt/ep/0606/121} & Automatic apparatus for inspecting powdery product. & SHINETSU CHEMICAL CO (JP) & 1993-01-06\\\hline
ep0634716\footnote{/patents/txt/ep/0634/716} & Low-level direct connect protocol for PCL printers. & HEWLETT PACKARD CO (US) & 1993-06-15\\\hline
ep0251296\footnote{/patents/txt/ep/0251/296} & Portable communication terminal for remote database query & WANG LABORATORIES (US) & 1986-06-30\\\hline
ep0549315\footnote{/patents/txt/ep/0549/315} & Method of encoding multi-bit digital information. & XEROX CORP (US) & 1991-12-27\\\hline
ep0601566\footnote{/patents/txt/ep/0601/566} & Digital signal processing apparatus and method and recording medium. & SONY CORP (JP) & 1992-12-11\\\hline
ep0690595\footnote{/patents/txt/ep/0690/595} & Encoding of digital information & HEWLETT PACKARD CO (US) & 1994-06-30\\\hline
ep0249212\footnote{/patents/txt/ep/0249/212} & Hand held manually sweeping printing apparatus. & CASIO COMPUTER CO LTD (JP) & 1986-12-11\\\hline
ep0325794\footnote{/patents/txt/ep/0325/794} & ISDN system with a subscriber line multiplexer for establishing different D-channel links. & NIPPON ELECTRIC CO (JP) & 1987-12-28\\\hline
ep0618744\footnote{/patents/txt/ep/0618/744} & Watch pager system and communication protocol. & SEIKO CORP (JP); SEIKO EPSON CORP (JP) & 1987-11-16\\\hline
ep0618743\footnote{/patents/txt/ep/0618/743} & Watch pager system and communication protocol. & SEIKO CORP (JP); SEIKO EPSON CORP (JP) & 1987-11-16\\\hline
ep0618742\footnote{/patents/txt/ep/0618/742} & Watch pager system and communication protocol. & SEIKO CORP (JP); SEIKO EPSON CORP (JP) & 1987-11-16\\\hline
ep0534710\footnote{/patents/txt/ep/0534/710} & Computer controlled lighting system with intelligent data distribution networks. & VARI LITE INC (US) & 1991-09-26\\\hline
ep0223551\footnote{/patents/txt/ep/0223/551} & Main storage control system for virtual computing function system with plural address modes in main storage access operations & FUJITSU LTD (JP) & 1985-11-13\\\hline
ep0641610\footnote{/patents/txt/ep/0641/610} & Paper object sorting method and apparatus comprising means for erasing printed bar codes. & NIPPON ELECTRIC CO (JP) & 1993-09-06\\\hline
ep0309373\footnote{/patents/txt/ep/0309/373} & Interactive animation of graphics objects. & IBM (US) & 1987-09-21\\\hline
ep0601814\footnote{/patents/txt/ep/0601/814} & Disc recording and/or reproducing apparatus having a buffer memory. & SONY CORP (JP) & 1992-12-09\\\hline
ep0664909\footnote{/patents/txt/ep/0664/909} & TEXT INPUT FONT SYSTEM & TALIGENT INC (US) & 1993-04-05\\\hline
ep0300166\footnote{/patents/txt/ep/0300/166} & Cache memory resiliency in processing a variety of address faults. & HONEYWELL BULL (US) & 1987-05-28\\\hline
ep0640920\footnote{/patents/txt/ep/0640/920} & Boundary-scan-based system and method for test and diagnosis. & AT \& T CORP (US) & 1993-08-30\\\hline
\end{longtable}
\end{center}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: latex ;
% End: ;

