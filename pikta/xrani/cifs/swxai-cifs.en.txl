<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Microsoft bars GNU software from interoperating with CIFS

#descr: During the 1st week of April 2002, Microsoft published a license for
its new specification CIFS which it is trying to establish as a de
facto communication standard.  This license says that free software
under GNU GPL, LGPL and similar licenses may not use CIFS.  It bases
this ban on two broad and trivial US patents with priority dates of
1989 and 1993.  Preliminary search results suggst that these patents
to not have EP (European Patent) counterparts.  But there is
nevertheless an EP patent which could possibly be used by MS for the
same purpose.  Critical network infrastructure such as Samba as well
as new projects such as Mono seem to be affected.

#MiW: Licencsing Terms

#foW: excerpted from %1

#TWt: The original on Microsoft's website.

#lac: local copy

#Aar: Advogato Article

#qoe: quotes Microsoft's licensing terms.

#Pts: Pseudo Patent-Free Microsoft Standard

#dWa: discussion based on the Advogato article and patent database browsing

#Mxt: MS-Patentlizenz: Keine GNU-Softare für MS-CIFS

#ssi: simila discussion in German

#Teb: The two US patents do not have EP (european) counterparts, but EP
0438571 could be used as a weapon for a similar purpose.

#BtD: Bericht von D. Riek

#Enb: Erste Recherchen: noch nichts beim EPA gefunden

#HaW: Heise report, see also discussions

#RPi: Research the Patent Situation in Europe!

#Iid: It seems that the same stuff might be patented at the EPO under
somewhat different headings

#FWo: Find out more about CIFS etc

#Itr: In what way is it a product of %(q:extending and embracing) previous
standards?

#Hoi: How important is CIFS?  What is its relation to .NET and other
Microsoft concepts?

#WWe: What parts of it are covered by patents?

#Whl: Where in CIFS is the core of the patented teaching found? i.e. which
parts would still be covered if after an invalidity litigation the
claim scope was narrowed down and only a few subclaims were left?

#Wao: What would a patent-unencumbered variant or alternative to CIFS have
to look like? OpenAFS? CODA? Modified CIFS?

#Iwt: It is very likely that most of the claims of the MS patents would not
stand an invalidity litigation test and that what is left over
consists of methods which would not need to be used, if it were not
for that Microsoft incorporated them into its CIFS specification. An
effort similar to PNG vs GIF may need to be undertaken and effectively
propagated.  MSOS (Windows) clients for this CIFS alternative also
need to be written.  Companies like IBM should play a leading role in
this, because they have the strength needed to litigate and to
propagate standards in the commercial environment.

#Sbk: Study the feasibility of this kind of project!

#HpW: How does competition law come in here?

#PWa: Patent lawyers will love to demonstrate that this case can be solved
by applying competition law.  If they do puruse that course, see to it
that we get some really useful legal precedents that let
interoperability considerations override patents, and not just an
adhoc solution.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-cifs ;
# txtlang: en ;
# multlin: t ;
# End: ;

