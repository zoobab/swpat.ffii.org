\begin{subdocument}{swxai-cifs}{Microsoft verbietet GNU-Software Kommunikation mit CIFS-Anwendungen}{http://swpat.ffii.org/pikta/xrani/cifs/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2003/12/18 von FFII\footnote{\url{http://lists.ffii.org/mailman/listinfo/traduk}}}{Anfang April 2002 ver\"{o}ffentlichte Microsoft eine Lizenz f\"{u}r ihre neue Spezifikation f\"{u}r CIFS, der zu einem umfassenden Kommunikationsstandard werden soll. Diese Lizenz verbietet freier Software, die unter der GNU GPL oder \"{a}hnlichen Lizenzen steht, die Verwendung des Standards.  Grundlage f\"{u}r diese Bestimmung sind zwei US-Patente von Microsoft mit Stichtag 1989 bzw 1993, die ein weites Problemfeld der Rechnervernetzung ebenso wie einige in CIFS festgelegten Spezialf\"{a}lle abdecken.)  Vorl\"{a}ufige Untersuchungen zeigen, dass diese Patente keine EP-Entsprechung haben, d.h. vom Europ\"{a}ischen Patentamt (EPA) nicht erteilt wurden.  Allerdings besitzt Micro\S{}oft ein EP-Patent \"{u}ber verteiltes Bearbeiten von Dateien, welches ebenso dazu dienen k\"{o}nnte, wichtigen Infrastrukturprojekten wie Samba und Mono zu Leibe zu r\"{u}cken.}
\begin{sect}{msla}{Lizenzbedingungen}
\begin{description}
\item[die entscheidenden Passagen aus \url{http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnkerb/html/Finalcifs_LicenseAgrmnt_032802.asp}:]\ \begin{quote}
{\it 1.4. ``IPR Impairing License'' shall mean the GNU General Public License, the GNU Lesser/Library General Public License, and any license that requires in any instance that other software distributed with software subject to such license (a) be disclosed and distributed in source code form; (b) be licensed for purposes of making derivative works; or (c) be redistributable at no charge.}

{\it 1.2 ``Company Implementation'' shall mean only those portions of the software developed by Company that implement CIFS for use on Non-Microsoft Platforms.}

{\it 1.6 ``Necessary Claims'' shall mean those claims of a patent or patent application, including without limitation, United States Patents Nos. 5,265,261 and 5,437,013, which (a) are owned, controlled or sublicenseable by Microsoft without payment of a fee to an unaffiliated third party; and (b) are necessarily infringed by implementing the CIFS communication protocol as set forth in the Technical Reference, wherein a claim is necessarily infringed only when there are no technically reasonable alternatives to such infringement.}

{\it 3.3 IPR Impairing License Restrictions. For reasons, including without limitation, because (i) Company does not have the right to sublicense its rights to the Necessary Claims and (ii) Company's license rights hereunder to Microsoft's intellectual property are limited in scope, Company shall not distribute any Company Implementation in any manner that would subject such Company Implementation to the terms of an IPR Impairing License.}
\end{quote}
\end{description}
\end{sect}

\begin{sect}{clms}{Patentanspr\"{u}che}
\begin{description}
\item[US 5265261:]\ \begin{enumerate}
\item
A computer implemented method in a computer system for transmitting data from a server computer to a consumer computer connected by a virtual circuit, the consumer computer having an application program requesting a read from the server computer, having a redirector, and having a transport, the application program having access to a data buffer allocated by the application program, comprising the steps of: allocating and initializing a receive network control block for directing the transport to store the next data it receives directly in the data buffer; transmitting from the redirector to the transport a read request to read data from the server and said receive network control block for directing the transport to store the read data directly in the data buffer, the read request indicating that the read data should be transmitted without a header; in response to the step of transmitting, sending the read request from the transport to the server   computer; examining and recognizing that the read request indicates that the read data should be transmitted without a header; storing the read data in a data block without the header; transferring the data block from the server computer to the transport in response to the step of sending the read request; and in response to the read request and the receive network control block and in response to the step of transferring, storing the data block directly from the transport into the data buffer.

\item
The method of claim 1 that includes the step of ensuring that no requests to transmit data on the virtual circuit are pending before the read request is transmitted.

\item
The method of claim 1, including the steps of: locking the data buffer before transmitting the read request to ensure that the data buffer remains accessible until the read request is satisfied; and unlocking the data buffer after the read data has been stored in the data buffer.

\item
\dots
\end{enumerate}
\item[US 5437013:]\ \begin{enumerate}
\item
A computer implemented method in a computer system for transferring data on a network from a first computer to a second computer connected by a virtual circuit, the second computer having an application program, a transport implementing network communications and a redirector implementing a system message block protocol, the application program having access to a data buffer, the method comprising the steps of: under control of the redirector, initializing a network control block; and directing the transport of the second computer to store the data that it receives next directly in the data buffer pointed to by the network control block instead of a system message block buffer in the redirector; under control of a transport of the first computer, transferring the data without a system message block header containing information about the data from the first computer over the virtual circuit to the transport of the second computer; and under control of the transport of the second computer, and upon receiving the transferred data, storing the received data directly into the data buffer of the application program.

\item
The method of claim 1, including the step of under control of the redirector, ensuring that no requests to transfer data from the first computer to the second computer are pending so that data without a system message block header can be transferred from the first computer to the second computer.

\item
\dots
\end{enumerate}
\item[EP 0438571:]\ \begin{enumerate}
\item
A method of locally catching data of a file at a remote terminal, the file being stored at the file server, comprising the steps of:\\
when the remote terminal initially requests to open the file, requesting that the file server open the file; and retrieving data from the file server to the remote terminal;\\
updating the retrieved data at the remote terminal without updating data of the file at the file server;\\
when the remote terminal request to close the opened file and when no other remote terminal has requested the file server to open the file, maintaining the retrieved data of the file at the remote terminal without requesting the file server to close the file and without sending the updated file to the server;\\
when the remote terminal requests to re-open the closed file and when no other remote terminal has requested the file server to open the file since the remote terminal requested to close the opened file, using the maintained data without requesting the file server to open the file and without retrieving the data of the file from the file server whereby so long as no other remote terminal has requested the file server to open the file all requests by the remote terminal to close and then open the file after the initial request to the file server to open the file are handled locally at the remote terminal even though the retrieved data may have been updated at the remote terminal.

\item
The method of claim 1 wherein the file is a batch file with multiple commands and the remote terminal locally processes the commands so long as no other remote terminal had requests to open the file.

\item
The method of claim 2 wherein before locally processing a command, the remote terminal requests to close the opened file and after processing the command, the remote terminal requests to re-open the file.
\end{enumerate}
\end{description}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf \url{http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnkerb/html/Finalcifs_LicenseAgrmnt_032802.asp}}}

\begin{quote}
Das original bei Microsoft.
siehe auch lokale Kopie\footnote{\url{mscifsla020328.txt}}
\end{quote}
\filbreak

\item
{\bf {\bf Advogato Article\footnote{\url{http://www.advogato.org/article/453.html}}}}

\begin{quote}
erste Diskussion Microsofts neue Lizenzbedingungen
\end{quote}
\filbreak

\item
{\bf {\bf Pseudo Patent-Free Microsoft Standard\footnote{\url{http://aful.org/wws/arc/patents/2002-04/msg00007.html}}}}

\begin{quote}
discussion based on the Advogato article and patent database browsing
\end{quote}
\filbreak

\item
{\bf {\bf MS-Patentlizenz: Keine GNU-Softare f\"{u}r MS-CIFS\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2002/Apr/0007.html}}}}

\begin{quote}
simila discussion in German
\end{quote}
\filbreak

\item
{\bf {\bf J. Halbersztadt: CIFS patent situation in Europe\footnote{\url{http://aful.org/wws/arc/patents/2002-04/msg00016.html}}}}

\begin{quote}
The two US patents do not have EP (european) counterparts, but EP 0438571 could be used as a weapon for a similar purpose.
\end{quote}
\filbreak

\item
{\bf {\bf Bericht von D. Riek\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2002/Apr/0012.html}}}}

\begin{quote}
Erste Recherchen: noch nichts beim EPA gefunden
\end{quote}
\filbreak

\item
{\bf {\bf David Marus: Softwarepatente - Pat.  5,265,261 = FTP?\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2002/Apr/0029.html}}}}

\begin{quote}
Erste Recherchen: noch nichts beim EPA gefunden
\end{quote}
\filbreak

\item
{\bf {\bf Microsoft und die GPL: Freiheit die ich meine\footnote{\url{http://www.heise.de/newsticker/data/ps-09.04.02-000/}}}}

\begin{quote}
Heise-Bericht, s. auch anschlie{\ss}ende Diskussionsbeitr\"{a}ge

\begin{itemize}
\item
{\bf {\bf Softwarepatente - Pat.  5,265,261 = FTP?\footnote{\url{http://www.heise.de/newsticker/foren/go.shtml?read=1&msg_id=1612405&forum_id=27941}}}}
\filbreak

\item
{\bf {\bf DARUM sind Monopole schlecht\footnote{\url{http://www.heise.de/newsticker/foren/go.shtml?read=1&amp;msg_id=1612441&amp;forum_id=27941}}}}
\filbreak

\item
{\bf {\bf Microsofts Strategie gegen alle ``Mitbewerber''\footnote{\url{http://www.heise.de/newsticker/foren/go.shtml?read=1&amp;msg_id=1612332&amp;forum_id=27941}}}}
\filbreak
\end{itemize}
\end{quote}
\filbreak

\item
{\bf {\bf (Steve Ballmer: Kein Tänzchen an der Leine)\footnote{\url{http://www.heise.de/newsticker/data/jk-12.03.02-000/}}}}

\begin{quote}
Heise-Bericht \"{u}ber Steve Ballmers Rede auf CeBit 2002.  Bei einem Redeauftritt zusammen mit Bundeskanzler Schr\"{o}der sagt Ballmer, Microsoft besitze zahlreiche Patente auf Aspekte des DotNet-Standards und beabsichtige, diese einzusetzen, um freie/quelloffene Umsetzungen von DotNet zu verhindern. U.a. sagt er: \begin{quote}
{\it Auf die \"{O}ffnung des .NET-Framework angesprochen, k\"{u}ndigte Ballmer an, dass es sicherlich eine Common-Language-Runtime-Implementation f\"{u}r Unix geben werde, schr\"{a}nkte diese Entwicklung jedoch als Subset ein, der ``nur f\"{u}r den akademischen Einsatz gedacht sei''. \"{U}berlegungen zur Unterst\"{u}tzung freier .NET-Implementationen wie Mono erteilte Ballmer eine Absage: ``Wir haben so viele Millionen in .NET gesteckt, wir haben so viele Patente auf .NET, die wir pflegen wollen.''}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Microsoft and Patents\footnote{\url{http://localhost/swpat/gasnu/microsoft/index.en.html}}}}

\begin{quote}
Microsoft Corporation grew large and successful without patents, relying instead on copyright.  In 1991 , Bill Gates warned that software patents could lead the software industry to a standstill but could also be very useful for defending monopoly positions.  At the USPTO hearings of 1994, Microsoft was the only software company that argued in favor of software patentability.   Microsoft has been involved in promoting software patentability in Europe.  Simultaneously Microsoft's has invested ample ressources into a campaign to dissuade governments and corporations from using free operating system.  Pointing out the insecurity caused to Free Software by patents and contributing to this insecurity by occasional threats has become an important part of the campaign.  Microsoft appears to be a favorite victim of patent attacks.  In June 2003, Microsoft hired formar IBM patent strategist Marshall Phelps, the father of the ``IBM tax'', to embark on an aggressive rent-extraction program.  Phelps also announced to step up efforts to campaign for software patentability in Europe.  Much of this campaigning appears to have been done indirectly by Microsoft partners.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
\begin{itemize}
\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Recherchieren Sie bitte die Patentsituation in Europa}}

\begin{quote}
Das gleiche k\"{o}nnte beim EPA unter etwas anderen \"{U}berschriften patentiert sein.
\end{quote}
\filbreak

\item
{\bf {\bf Finden Sie mehr \"{u}ber CIFS heraus!}}

\begin{quote}
In what way is it a product of ``extending and embracing'' previous standards?

Welche Bedeutung hat CIFS?  Wie verh\"{a}lt es sich zu .NET und sonstigen Microsoft-Konzepten?

Welche Teile von CIFS sind von Patenten belegt?

Wo in CIFS liegt der Kern der patentierten Lehre?  Inwieweit w\"{u}rden die Patente noch immer greifen, wenn man sie durch Nichtigkeitsklagen weitgehend einengen k\"{o}nnte?
\end{quote}
\filbreak

\item
{\bf {\bf What would a patent-unencumbered variant or alternative to CIFS have to look like? OpenAFS? CODA? Modified CIFS?}}

\begin{quote}
It is very likely that most of the claims of the MS patents would not stand an invalidity litigation test and that what is left over consists of methods which would not need to be used, if it were not for that Microsoft incorporated them into its CIFS specification. An effort similar to PNG vs GIF may need to be undertaken and effectively propagated.  MSOS (Windows) clients for this CIFS alternative also need to be written.  Companies like IBM should play a leading role in this, because they have the strength needed to litigate and to propagate standards in the commercial environment.

Study the feasibility of this kind of project!
\end{quote}
\filbreak

\item
{\bf {\bf How does competition law come in here?}}

\begin{quote}
Patent lawyers will love to demonstrate that this case can be solved by applying competition law.  If they do puruse that course, see to it that we get some really useful legal precedents that let interoperability considerations override patents, and not just an adhoc solution.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

