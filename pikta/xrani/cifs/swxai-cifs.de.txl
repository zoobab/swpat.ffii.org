<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Microsoft verbietet GNU-Software Kommunikation mit CIFS-Anwendungen

#descr: Anfang April 2002 veröffentlichte Microsoft eine Lizenz für ihre neue
Spezifikation für CIFS, der zu einem umfassenden
Kommunikationsstandard werden soll. Diese Lizenz verbietet freier
Software, die unter der GNU GPL oder ähnlichen Lizenzen steht, die
Verwendung des Standards.  Grundlage für diese Bestimmung sind zwei
US-Patente von Microsoft mit Stichtag 1989 bzw 1993, die ein weites
Problemfeld der Rechnervernetzung ebenso wie einige in CIFS
festgelegten Spezialfälle abdecken.)  Vorläufige Untersuchungen
zeigen, dass diese Patente keine EP-Entsprechung haben, d.h. vom
Europäischen Patentamt (EPA) nicht erteilt wurden.  Allerdings besitzt
Micro§oft ein EP-Patent über verteiltes Bearbeiten von Dateien,
welches ebenso dazu dienen könnte, wichtigen Infrastrukturprojekten
wie Samba und Mono zu Leibe zu rücken.

#MiW: Lizenzbedingungen

#foW: die entscheidenden Passagen aus %1

#TWt: Das original bei Microsoft.

#lac: lokale Kopie

#Aar: Advogato Article

#qoe: erste Diskussion Microsofts neue Lizenzbedingungen

#Pts: Pseudo Patent-Free Microsoft Standard

#dWa: discussion based on the Advogato article and patent database browsing

#Mxt: MS-Patentlizenz: Keine GNU-Softare für MS-CIFS

#ssi: simila discussion in German

#Teb: The two US patents do not have EP (european) counterparts, but EP
0438571 could be used as a weapon for a similar purpose.

#BtD: Bericht von D. Riek

#Enb: Erste Recherchen: noch nichts beim EPA gefunden

#HaW: Heise-Bericht, s. auch anschließende Diskussionsbeiträge

#RPi: Recherchieren Sie bitte die Patentsituation in Europa

#Iid: Das gleiche könnte beim EPA unter etwas anderen Überschriften
patentiert sein.

#FWo: Finden Sie mehr über CIFS heraus!

#Itr: In what way is it a product of %(q:extending and embracing) previous
standards?

#Hoi: Welche Bedeutung hat CIFS?  Wie verhält es sich zu .NET und sonstigen
Microsoft-Konzepten?

#WWe: Welche Teile von CIFS sind von Patenten belegt?

#Whl: Wo in CIFS liegt der Kern der patentierten Lehre?  Inwieweit würden
die Patente noch immer greifen, wenn man sie durch Nichtigkeitsklagen
weitgehend einengen könnte?

#Wao: What would a patent-unencumbered variant or alternative to CIFS have
to look like? OpenAFS? CODA? Modified CIFS?

#Iwt: It is very likely that most of the claims of the MS patents would not
stand an invalidity litigation test and that what is left over
consists of methods which would not need to be used, if it were not
for that Microsoft incorporated them into its CIFS specification. An
effort similar to PNG vs GIF may need to be undertaken and effectively
propagated.  MSOS (Windows) clients for this CIFS alternative also
need to be written.  Companies like IBM should play a leading role in
this, because they have the strength needed to litigate and to
propagate standards in the commercial environment.

#Sbk: Study the feasibility of this kind of project!

#HpW: How does competition law come in here?

#PWa: Patent lawyers will love to demonstrate that this case can be solved
by applying competition law.  If they do puruse that course, see to it
that we get some really useful legal precedents that let
interoperability considerations override patents, and not just an
adhoc solution.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-cifs ;
# txtlang: de ;
# multlin: t ;
# End: ;

