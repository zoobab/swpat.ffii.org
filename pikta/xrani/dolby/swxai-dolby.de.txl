<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Dolby Standard tolerates no OpenSource implementation

#descr: Auf Drohung einer Gruppe von Patentinhabern hin wurde ein Projekt,
quelloffene Software zur Unterstützung der Audio-Standards rund um
Dolby zu entwickeln, im März 201 vom Netz genommen.  Die Patentinhaber
verlangen eine Lizenzgebühr für jede verteilte Kopie des zu
erstellenden Programms.  Dadurch wird es unmöglich, dieses Programm in
offener Manier zu erstellen.  Da Dolby durch die Macht der Gewohnheit
fest etabliert ist und da zudem die Patente (und zu erwartende
Nachfolgepatente) nicht nur den Standard selber sondern ganze
umliegende Problemfelder abdecken, könnte somit die Entstehung einer
offenen und sicheren Audio-Infrastruktur auf Jahrzehnte hin ein Traum
bleiben.

#tWs: Press report about how an opensource project was closed down due to
pressures from the AAC license consortium which requires a lumpsum
payment of 10,000 USD plus a per-copy payment of 1.35 USD, thus
effectively banning free software implementations.  The policies
surrounding AAC also harm interoperability.

#tys: News report about pressures of Dolby on developpers of open source
encoders.

#AWW: Dolby/AAC is largely based on MPEG patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-dolby ;
# txtlang: de ;
# multlin: t ;
# End: ;

