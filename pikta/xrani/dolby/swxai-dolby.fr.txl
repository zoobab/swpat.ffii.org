<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Le Standard Dolby refuse toute implémentation Open Source

#descr: Sous la pression d'un groupe de détenteurs de brevets, un projet  de
développement de logiciels open source autour des standards audio 
Dolby a dû être abandonné en mars 2001. Les détenteurs des brevets 
exigeaient le paiement d'une redevance pour chaque copie distribuée.
De  ce fait, le développement open source de ce projet n'a pu se
faire.  Comme Dolby est, par la force de l'habitude, très largement
utilisé, et  que les brevets existants (et ceux à venir) couvrent non
seulement le  standard mais aussi de nombreux champs annexes, la
naissance d'une  infrastructure audio sécurisée et libre de droits
pourrait rester à  l'état de rêve pendant encore quelques décennies.

#tWs: Press report about how an opensource project was closed down due to
pressures from the AAC license consortium which requires a lumpsum
payment of 10,000 USD plus a per-copy payment of 1.35 USD, thus
effectively banning free software implementations.  The policies
surrounding AAC also harm interoperability.

#tys: News report about pressures of Dolby on developpers of open source
encoders.

#AWW: Dolby/AAC is largely based on MPEG patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: mgaroche ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-dolby ;
# txtlang: fr ;
# multlin: t ;
# End: ;

