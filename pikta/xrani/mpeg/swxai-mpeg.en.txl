<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: MPEG-related patents on compression of acoustic data

#descr: Acoustic compression requires knowledge of auditive perception, which
had to be acquired through experimentation.  Thus this field is close
to the borderline of technical inventions which could be patentable. 
Yet most of the research results were published in the 60s and 70s,
and the patented processes based thereon are pure informational
processes, some of them quite basic and trivial, when viewed against
the background of available theoretical knowledge.  The whole field of
audio compression is cluttered with dozens of basic patents, thus
making it very difficult to develop alternatives.  Ogg Vorbis seems to
have succeeded in developping patent-free audio compression, but is
being threatened by the patent holders, who have formed various
consortia such as MP3 and MPEG2.  In order to develop free software
for MP3, one must pay an upfront payment of 1 million USD.  Otherwise
money must be charged per copy, thus barring the possibility of
opensource development.  Moreover, recently MPEG-LA, a consortium of
MPEG patent holders, also proposed charging fees from content
producers.

#ToW: Relevant Texts

#FeW: Fraunhofer Audio Patents

#Men: More MPEG related patents coming up

#Est: EU IPR Helpdesk Patent Of the Month 2001

#dat: In early 2003, Tord Jansson, developper of a streaming software called
BladeEnc, wrote to a member of the European Parliament:

#eml: I'm a professional software developer who early summer 1998 wrote a
computer program that I decided to put on my homepage. The program
turned out to be a tremendous success and was quickly distributed in
millions of copies, obviously filling a need among many computer
users. I quickly started to improve my program and release new
versions. That same autumn I was contacted by a large company with a
competing product, who claimed that my program infringed on certain
patents they had been granted. Consulting SEPTO gave no reason to take
infringement claims seriously since computer programs are not
patentable as such, but in early 1999 my legal advisor explained that
the legal uncertainty lately introduced by EPO would perhaps make the
claims valid. That eventually forced me to stop making my program
available.

#atc: Do you believe a corporation should have the right to control what
computer programs I can write and publish?

#Tsl: The author of this free audio encoding sourcecode was threatened by
Thomson Multimedia Inc and chose to stop publishing his work, although
he wrote nothing but a computer program [ as such ].

#eWa: explains why development of audio software is not stimulated but
rather stifled by software patents.

#D3s: Dolby noise reduction also involves MP3, and developpers of free
alternatives have been threatened in a similar way, partially based on
Fraunhofer patents.

#ent: explains that the groundbreaking concepts of MP3 were well known and
used in 1980, long before Fraunhofer applied for patents on some of
the more mundane details of MP3 programming.

#iisfhg: Website of the MP3 researchers from Fraunhofer Institute

#Wui: Evaluation of MP3 patents by a group of german computer science
students.

#Aim: A private mail from 2001/02 tells us:

#Yyn: You are probably already aware of this -- some important patents
regarding video compression are coming up, particularly relating to
MPEG4; I've talked to the attorney who is the primary examiner on this
patent cluster, and he actually rejected some of them last September
(from major multinationals) based on over-breadth. It's wait-and-see. 
PacketVideo has just gotten an important patent on an error reduction
algorithm relating to video compression in low-bandwidth situations
that could have been applied very usefully, had it been freely
distributed.

#Oyr: One particularly broad and already much discussed patent in this area
is at the basis of RealAudio:

#AEh: A monthly bulletin of the IPR Helpdesk project, financed by the
European Commission's Enterprise Directorate, nominated one of the MP3
patents %(q:European Patent of the Month) in summer 2001:

#Iee: It is doubtful whether the calculation rule covered by DE3629434
really took a long time to find.  Also it is somewhat strange that a
12 year old patent was nominated %(q:patent of the month).  But it
seems clear that the MP3 patents are showcased as cases of %(q:good
software patents), since they cover solutions to difficult problems
and may involve some empirical knowledge.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-mpeg ;
# txtlang: en ;
# multlin: t ;
# End: ;

