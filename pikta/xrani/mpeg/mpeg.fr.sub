\begin{subdocument}{swxai-mpeg}{MPEG et Brevets sur la Compression des Donn\'{e}es Acoustiques}{http://swpat.ffii.org/brevets/effets/mpeg/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise 2000/12/25 par PILCH Hartmut\footnote{mailto:phm@a2e.de?subject=http://swpat.ffii.org/brevets/effets/mpeg/index.fr.html}}{La compression acoustique demande des connaissance de la psychologie auditive, qui est bas\'{e}e sur des experiences et donce proche au domaine classique de la brevetabilit\'{e}.   Or, le models psychoacoustique sur lesquels les m\'{e}thodes MPEG/MP3 sont bas\'{e}es etaient d\'{e}ja connue, et les brevets bas\'{e}es ci-dessus donc constituent des brevets logiciels en sense stricte, qui apparaissent m\^{e}me triviales, si on les consid\`{e}re en relation avec les th\'{e}ories connues.   Tout le domaine de la compression acoustique est couvrie par des douzaines de brevets fondamentaux.  Le projet Ogg Vorbis semble avoir succ\'{e}d\'{e} en developpant une alternative non-brevet\'{e}e, mais ils sont menac\'{e}s par des consortium de propri\'{e}taires de brevets.  Pour obtenir le droit de diffuser des logiciels libres de ces consortiums, il faut payer 1 million de USD.  Sinon on peut publier seulement des logiciels propri\'{e}taire avec un controle stricte de diffusion et un paiement par copie.}
\begin{sect}{lig}{Relevant Texts}
\begin{itemize}
\item
FAQ: MPEG, Patents, and Audio Coding\footnote{http://sound.media.mit.edu/~eds/mpeg-patents-faq}

\item
mp3licensing.com - Home\footnote{http://www.mp3licensing.com/}

\item
mp3licensing.com - Patent Portfolio\footnote{http://www.mp3licensing.com/patents.html}

\item
LAME = LAME Ain't an MP3 Encoder\footnote{http://www.mp3dev.org/mp3/}

\item
MPEG patent issues\footnote{http://news.webnoize.com/item.rs?ID=4155}

\item
Thomson ./ Ogg\footnote{http://news.cnet.com/news/0-1005-200-4101023.html} (Ogg Vorbis is a patent-free opensource algernative written from scratch, achieving high quality while carefully avoiding MP3 patents.  Yet how patent claims are interpreted is not always easy to predict.  In December 2000, Thomson manager Henri Linde threatens the Ogg project: ``We doubt very much that they are not using Fraunhofer and Thomson intellectual property. We think it is likely they are infringing.'', apparently in an attempt to prevent this format from gaining ground.)

\item
BladeEnc\footnote{http://bladeenc.mp3.no/} (The author of this free audio encoding sourcecode was threatened by Thomson Multimedia Inc and chose to stop publishing his work, although he wrote nothing but a computer program as such.)

\item
The BladeEnc author about Software Patents\footnote{http://bladeenc.mp3.no/articles/software\_patents.html} (explains why development of audio software is not stimulated but rather stifled by software patents.)

\item
Dolby Standard tolerates no OpenSource implementation\footnote{http://swpat.ffii.org/brevets/effets/dolby/index.fr.html} (Dolby noise reduction also involves MP3, and developpers of free alternatives have been threatened in a similar way, partially based on Fraunhofer patents.)

\item
Erich Bieramperl: MP3 und Ogg\footnote{http://lists.ffii.org/archive/mails/swpat/2001/Aug/0165.html} (explains that the groundbreaking concepts of MP3 were well known and used in 1980, long before Fraunhofer applied for patents on some of the more mundane details of MP3 programming.)

\item
Die Fraunhofer-Gesellschaft als Bastion der Patentbewegung\footnote{http://swpat.ffii.org/acteurs/fhg/index.de.html}
\end{itemize}
\end{sect}\begin{sect}{pat}{Fraunhofer Audio Patents}
\begin{description}
\item[ep1149480:]\ method and device for inserting information into an audio signal, and method and device for detecting information inserted into an aufio signal
\item[ep1145227:]\ method and device for error concealment in an encoded audio-signal and method and device for decoding an encoded audio signal
\item[ep1025646:]\ methods and devices for encoding audio signals and methods and devices for decoding a bit stream
\item[ep1005695:]\ method and device for detecting a transient in a discrete-time audiosignal, and device and method for coding an audiosignal
\item[ep1123638:]\ system and method for evaluating the quality of multi-channel audiosignals
\item[ep0978172:]\ method for masking defects in a stream of audio data
\item[ep0954909:]\ method for coding an audio signal
\item[ep1133849:]\ method and device for generating an encoded user data stream and method and device for decoding such a data stream
\item[ep1099197:]\ device for supplying output data in reaction to input data, method for checking authenticity and method for encrypted data transmission
\item[ep1141890:]\ method for marking a polygon-based binary data set of a three-dimensional model
\item[ep0978172:]\ method for masking defects in a stream of audio data
\item[ep0965102:]\ output device for digitally stored data on a data carrier
\item[ep1050186:]\ communication network, method for transmitting a signal, network connecting unit and method for adjusting the bit rate of scaled data flow
\item[ep1052938:]\ process and device for obtaining 3d ultrasonic data
\end{description}
\end{sect}\begin{sect}{mp4}{More MPEG related patents coming up}
A private mail from 2001/02 tells us:
\begin{quote}
You are probably already aware of this -- some important patents regarding video compression are coming up, particularly relating to MPEG4; I've talked to the attorney who is the primary examiner on this patent cluster, and he actually rejected some of them last September (from major multinationals) based on over-breadth. It's wait-and-see.  PacketVideo has just gotten an important patent on an error reduction algorithm relating to video compression in low-bandwidth situations that could have been applied very usefully, had it been freely distributed.
\end{quote}

One particularly broad and already much discussed patent in this area is at the basis of RealAudio:
\begin{quote}
US 6,151,634\\
Audio-on-demand communication system
\end{quote}
\end{sect}\begin{sect}{eup}{EU IPR Helpdesk Patent Of the Month 2001}
A monthly bulletin of the IPR Helpdesk project, financed by the European Commission's Enterprise Directorate, nominated one of the MP3 patents ``European Patent of the Month'' in summer 2001:

\begin{quote}
4.1. MP3 compression format

The Moving Picture Experts Group Audio Layer III compression format, more generally known as MP3, began life in the mid-1980s at the Fraunhofer Institut in Germany.
\end{quote}

It is doubtful whether the calculation rule covered by DE3629434 really took a long time to find.  Also it is somewhat strange that a 12 year old patent was nominated ``patent of the month''.  But it seems clear that the MP3 patents are presented as cases of ``good software patents'', since they cover solutions to difficult problems and may involve some empirical knowledge, even if not based on controllable physical forces.
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
% mode: latex ;
% End: ;

