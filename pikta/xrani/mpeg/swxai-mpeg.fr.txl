<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: MPEG et Brevets sur la Compression des Données Acoustiques

#descr: La compression acoustique demande des connaissance de la psychologie
auditive, qui est basée sur des experiences et donce proche au domaine
classique de la brevetabilité.   Or, le models psychoacoustique sur
lesquels les méthodes MPEG/MP3 sont basées etaient déja connue, et les
brevets basées ci-dessus donc constituent des brevets logiciels en
sense stricte, qui apparaissent même triviales, si on les considère en
relation avec les théories connues.   Tout le domaine de la
compression acoustique est couvrie par des douzaines de brevets
fondamentaux.  Le projet Ogg Vorbis semble avoir succédé en
developpant une alternative non-brevetée, mais ils sont menacés par
des consortium de propriétaires de brevets.  Pour obtenir le droit de
diffuser des logiciels libres de ces consortiums, il faut payer 1
million de USD.  Sinon on peut publier seulement des logiciels
propriétaire avec un controle stricte de diffusion et un paiement par
copie.

#ToW: Relevant Texts

#FeW: Fraunhofer Audio Patents

#Men: More MPEG related patents coming up

#Est: EU IPR Helpdesk Patent Of the Month 2001

#dat: In early 2003, Tord Jansson, developper of a streaming software called
BladeEnc, wrote to a member of the European Parliament:

#eml: I'm a professional software developer who early summer 1998 wrote a
computer program that I decided to put on my homepage. The program
turned out to be a tremendous success and was quickly distributed in
millions of copies, obviously filling a need among many computer
users. I quickly started to improve my program and release new
versions. That same autumn I was contacted by a large company with a
competing product, who claimed that my program infringed on certain
patents they had been granted. Consulting SEPTO gave no reason to take
infringement claims seriously since computer programs are not
patentable as such, but in early 1999 my legal advisor explained that
the legal uncertainty lately introduced by EPO would perhaps make the
claims valid. That eventually forced me to stop making my program
available.

#atc: Do you believe a corporation should have the right to control what
computer programs I can write and publish?

#Tsl: The author of this free audio encoding sourcecode was threatened by
Thomson Multimedia Inc and chose to stop publishing his work, although
he wrote nothing but a computer program [ as such ].

#eWa: explains why development of audio software is not stimulated but
rather stifled by software patents.

#D3s: Dolby noise reduction also involves MP3, and developpers of free
alternatives have been threatened in a similar way, partially based on
Fraunhofer patents.

#ent: explains that the groundbreaking concepts of MP3 were well known and
used in 1980, long before Fraunhofer applied for patents on some of
the more mundane details of MP3 programming.

#iisfhg: Website of the MP3 researchers from Fraunhofer Institute

#Wui: Evaluation of MP3 patents by a group of german computer science
students.

#Aim: A private mail from 2001/02 tells us:

#Yyn: You are probably already aware of this -- some important patents
regarding video compression are coming up, particularly relating to
MPEG4; I've talked to the attorney who is the primary examiner on this
patent cluster, and he actually rejected some of them last September
(from major multinationals) based on over-breadth. It's wait-and-see. 
PacketVideo has just gotten an important patent on an error reduction
algorithm relating to video compression in low-bandwidth situations
that could have been applied very usefully, had it been freely
distributed.

#Oyr: One particularly broad and already much discussed patent in this area
is at the basis of RealAudio:

#AEh: A monthly bulletin of the IPR Helpdesk project, financed by the
European Commission's Enterprise Directorate, nominated one of the MP3
patents %(q:European Patent of the Month) in summer 2001:

#Iee: It is doubtful whether the calculation rule covered by DE3629434
really took a long time to find.  Also it is somewhat strange that a
12 year old patent was nominated %(q:patent of the month).  But it
seems clear that the MP3 patents are showcased as cases of %(q:good
software patents), since they cover solutions to difficult problems
and may involve some empirical knowledge.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-mpeg ;
# txtlang: fr ;
# multlin: t ;
# End: ;

