<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patente auf unscharfe Kompression in JPEG u.a.

#descr: Wenn Sie JPEG-Dateien erzeugen oder verwenden, verletzen Sie
möglicherweise Patente.  Einige Erweiterungen von JPEG sind
patentiert.  Grundlegende Kodierungsprinzipien, die u.a. in JPEG zum
Einsatz kommen, sind in den USA und (illegalerweise auch) in Europa
patentiert.  Ein neuer Patentinhaber hat kürzlich begonnen, seine
Vorrechte einzuklagen.  Das JPEG-Konsortium erklärte daraufhin, dass
JPEG nicht mehr ein Standard sei.

#Jnn: Patent auf JPEG-Datenübertragung

#Din: Die Firma Compression LABS hatte einige (ca 20) EP-Patente und hielt
sich bei der Verwertung dieses Patents zurück.  Inzwischen hat es
jedoch den Besitzer gewechselt.  Die Videofirma Forgent hat es schon
verstanden, 15 Millionen USD aus Sony herauszupressen und schreitet
nun weiter.

#T4W: Das EPA hat in etwa das gleiche Patent 1994 erteilt (angemeldet 1987)
und es wurde in %(LANDS) umgesetzt.  Es wird ein grundlegendes Prinzip
der Informationskodierung beansprucht.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-jpeg ;
# txtlang: de ;
# multlin: t ;
# End: ;

