<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Softwarepatente in Aktion

#descr: In den letzten Jahren sind einige Streitfälle um Softwarepatente durch
die Medien bekannt geworden.  Es handelt sich hierbei um die Spitze
des Eisbergs. Die meisten Entwickler und Firmen werden nur außerhalb
der Gerichte mit Patentforderungen konfrontiert, und Schweigen liegt
im beiderseitigen Interesse.  Viele Projekte werden zurückgeschraubt
oder aufgegeben.  Es ist schwer, verhinderte Entwicklungen zu
dokumentieren.  Hier wollen wir diesen Versuch unternehmen.

#Sln: Einige gut dokumentierte Fälle

#allvoicet: From AllVoice to AllPatent: Milking the Speech Recognition Business
with Parliamentary Support

#allvoiced: Allvoice Computing PLC, originally a text-processor service company
based in Devon, UK, has obtained two broad and trivial patents in US
and UK on the logics of interfacing between speech recognition and
word processing.  Allvoice tried to sell this interface as a
standalone software product, but was apparently more successful in
extracting rents from producers of full-fledged speech recognition
software, such as IBM and Lernout & Hauspie, by means of patent
litigation.  Meanwhile Allvoice's business seems to be focussing on
patent enforcement.  Allvoice's director John Mitchell has also become
a patent-political activist and an archetype of a business model which
british parliamentarians are promoting in UK and EU.

#simulinkt: National Instruments ./ The Mathworks

#simulinkd: Nach einem zweijährigen Prozess verurteilte ein US-Gericht Mathworks,
sein Programm Simulink wegen Verletzung dreier Patente von National
Instruments (NI) aus dem Verkehr zu ziehen.  Mathworks hat Berufung
eingelegt.  Die NI-Patente wurden auch vom Europäischen Patentamt
erteilt.  Wenn sie rechtsbeständig würden, könnte auch in Europa ein
ähnlicher Rechtsstreit stattfinden.

#Lcv: Leider gibt es noch einen Haufen schlecht dokumentierter oder nicht
veröffentlichbarer Fälle.

#IWe: Wenn Sie irgendwelche Fälle kennen, %(pi:berichten Sie uns) bitte!

#DnW: Deutsches Unternehmen bläst Implementation von Outlook-Erweiterungen
auf Linux ab, nachdem Rechtsabteilung 20 MS-Patente aufgelistet hat.

#Fft: Führender Deutscher Kommunikationssoftwarehersteller von bayerischem
Weltkonzern mit Trivialpatent angegriffen

#Ppf: Planned GNU e-cash system stalled for years, because basic
cryptographic methods are patented and committments to allow GPL
implementations could not be obtained.  The difficulties in
establishing trust in e-money and e-commerce are aggravated by
patents.  Some basic e-payment procedures are patented, see
Microsoft's SET patent.

#MWW: Die meisten der Patente, welche die Krise am W3C auslösten, sind
trivial und breit und wurden auch vom EPA (gesetzeswidrig) erteilt.

#Hlf: Hersteller von Druckern, Skännern und anderen Peripheriegeräten
liefern oft keine Spezifikationen für Treiberschnittstellen und keine
quelloffenen Treiber, weil sie Angst haben, dass dies zur Entdeckung
von Patentverletzungen führen könnte.  Insbesondere einige große
Hersteller, die an sich offene Systeme und quelloffene Plattformen
systematisch unterstützen, rücken aus diesem Grunde nicht so schnell
von ihrer Politik ab, nur quellverschlossene MSWin-Treiber
bereitzustellen.

#Iar: IBM patent lawyer %(FT) has repeatedly declared in public declared the
IBM would not hesitate to sue Open Source projects that %(q:free-ride)
on IBM %(q:technology), thus forcing them to close down and become
proprietary.  Microsoft speakers have %(ms:said) the same.

#Wfs: Wir haben einige der berühmten amerikanischen
Geschäftsverfahrenspatente noch nicht dokumentiert.

#MfU: Microsoft Web Umfrage

#bna: beantragt

#eti: erteilt

#Tit: Titel

#Oua: Oppenheimer list of current cases pending about internet patents

#spW: sehr umfassende Übersicht, aber Javascript-Fehler könnten zum Absturz
Ihres Blätterprogramms führen

#nrb: names various cases of the early 90s where companies have been harmed
by software patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpikxrani ;
# txtlang: de ;
# multlin: t ;
# End: ;

