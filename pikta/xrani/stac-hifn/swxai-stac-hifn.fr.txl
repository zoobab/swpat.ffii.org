<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Compression Haute-Fréquence Stac pour ISDN et PPP

#descr: Le schème de compression Stac est un d'une multitude d'algoritmes
brevetés qui empechent l'interopérabilité entre des systèmes et, à
cause du grand nombre de brevets proliférant dans ce domaine, est
peut-être impossible a circonvenir par des innovations alternatives.

#cWt: %{HM}, developper of %{I4B} in 1998 refrained from implementing
support of the Stac compression standard into FreeBSD, thus making it
difficult to communicate between FreeBSD systems and most commercial
operating systems, whose owners pay license fees for being able to
decompress Stac data.  Upon inquiry, he explained:

#ToW: The software you are referring to is the PPP Stac compression which is
patented by Stac/HiFn. Although it is described with the exact details
required to implement it, it seems to be impossible to actually code
it (or better to release that code as source or binary) because of the
patent rights Stac/HiFn holds.

#Ano: Another leading ISDN developper explains

#Tdg: The damage is

#MlW: Most ISDN switches are aware of no other data compression than Stac. 
But nobody is allowed to offer Stac-capable software without paying
patent royalties.  The result is that proper ISDN communication is not
possible outside of Microsoft operating systems.

#Mav: Most data compression schemes are trivial.  Usually, they are based
either on Storer or on LZW.  Since the basic algorithms have all been
patented, there is not much point in trying to develop a patent-free
alternative to Stac.  This damage is even greater than the damage
caused by the fact that Stac is a de-facto standard.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-stac-hifn ;
# txtlang: fr ;
# multlin: t ;
# End: ;

