<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Hochfrequenz-Kompression von Stac für ISDN und PPP

#descr: Das Kompressionsverfahren Stac beherrscht die Kommunikation im
ISDN-Bereich und hat dort eine Kompatibilitäts-Barriere zugunsten von
Microsoft-Betriebssystemen errichtet.  Es beruht auf trivialen
Patenten, die aber wegen ihrer großen Anzahl das Feld der Kompression
so zudecken, dass kaum Hoffnung auf eine patentfreie Alternative zu
Stac aufkommen kann.

#cWt: %{HM}, developper of %{I4B} in 1998 refrained from implementing
support of the Stac compression standard into FreeBSD, thus making it
difficult to communicate between FreeBSD systems and most commercial
operating systems, whose owners pay license fees for being able to
decompress Stac data.  Upon inquiry, he explained:

#ToW: The software you are referring to is the PPP Stac compression which is
patented by Stac/HiFn. Although it is described with the exact details
required to implement it, it seems to be impossible to actually code
it (or better to release that code as source or binary) because of the
patent rights Stac/HiFn holds.

#Ano: Another leading ISDN developper explains

#Tdg: Der Schaden ist

#MlW: Die meisten ISDN switches koennen nur stac als datenkompression.  dies
darf aber niemand patentfrei anbieten, so dass es eigentlich nur eine
loesung fuer neuere MS betriebssysteme gibt. linux & co sehen da schon
schlechter aus.

#Mav: Die meisten Datenkompressionspatente sind Trivialpatente.  Es wird
normalerweise immer das Verfahren von Storer et al oder das von
Lempel-Zif-Welsh (z.b. bei v.42bis) patentiert.  Beides ist aber immer
schon vorher von denen veröffentlicht worden.  Da praktisch jedes
Verfahren patentiert ist, kann keiner mehr irgendwelche Verfahren
entwickeln, die taugen. Dieser Schaden ist viel größer.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-stac-hifn ;
# txtlang: de ;
# multlin: t ;
# End: ;

