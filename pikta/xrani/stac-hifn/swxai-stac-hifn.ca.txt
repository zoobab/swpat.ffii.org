<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: The Stac compression scheme is one of many patented compression algorithms which severely impede interoperability between systems and, because of the sheer number of patents cluttering their field, are probably impossible to circumvent.
title: HiFn PPP Stac compression
cWt: %{HM}, developper of %{I4B} in 1998 refrained from implementing support of the Stac compression standard into FreeBSD, thus making it difficult to communicate between FreeBSD systems and most commercial operating systems, whose owners pay license fees for being able to decompress Stac data.  Upon inquiry, he explained:
ToW: The software you are referring to is the PPP Stac compression which is patented by Stac/HiFn. Although it is described with the exact details required to implement it, it seems to be impossible to actually code it (or better to release that code as source or binary) because of the patent rights Stac/HiFn holds.
Ano: Another leading ISDN developper explains
Tdg: der schaden ist
MlW: die meisten ISDN switches koennen nur stac als datenkompression.  dies darf aber niemand patentfrei anbieten, so dass es eigentlich nur eine loesung fuer neuere MS betriebssysteme gibt. linux & co sehen da schon schlechter aus.
Mav: die meisten datenkompressionspatente sind trivialpatente. es wird normalerweise immer das verfahren von storer et al oder das von lempel zif welsh (z.b. bei v.42bis) patentiert. beides ist aber immer schon vorher von denen veroeffentlicht worden. da praktisch jedes verfahren patentiert ist, kann keiner mehr irgendwelche verfahren entwickeln, die taugen. dieser schaden ist viel groesser.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swxai-stac-hifn ;
# txtlang: ca ;
# End: ;

