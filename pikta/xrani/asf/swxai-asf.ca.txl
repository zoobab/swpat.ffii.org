<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#TFa: Avery Lee, author of VirtualDub reports the sad news: %(bq:Today I received a polite phone call from a fellow at Microsoft who works in the Windows Media group. He informed me that Microsoft has intellectual property rights on the ASF format and told me that, although the implementation was still illegal since it infringed on Microsoft patents. I have asked for the specific patent numbers, since I find patenting a file format a bit strange. At his request, and much to my own sadness, I have removed support for ASF in VirtualDub 1.3d, since I cannot risk a legal confrontation.)

#descr: Microsoft has prohibited a Free Software programmer from writing import/export filters for its Advanced Streaming Format (ASF).  The programmer wanted interoperability with a format that Microsoft is promoting.  But for Microsoft, interoperability is in this case doubly disadvantageous:  besides reducing the lock-in effect, on which Microsoft's platform strategy relies, it also can circumvent the locks on unauthorized copying, by which Microsoft wants to attract content providers to its ASF platform.  Whereas in the DeCSS case a court ruling was necessary to enforce new draconian copyright provisions of the highly disputed Digital Millenium Act, in the ASF case a simple patent suffices to achieve the same legislative goal.

#title: ASF: changing copyright rules by means of patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-asf ;
# txtlang: ca ;
# multlin: t ;
# End: ;

