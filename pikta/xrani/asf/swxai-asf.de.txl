<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: ASF: Urheberrechts-Neuregelung durch ein Patent

#descr: Microsoft hat einem Programmierer freier Software verboten,
Import/Export-Filter für sein Advanced Streaming Format (ASF) zu
schreiben.  Für Microsoft ist die Interoperabilität, um die es dem
Programmierer ging, doppelt nachteilhaft:  sie untergräbt nicht nur
die Einschluss-Effekte, auf denen Microsofts Plattformstrategie
beruht, sondern sie erlaubt auch die Umgehung des in ASF eingebauten
Kopierschutzes, mit dem Microsoft die Inhaltsanbieter auf seine
Plattform locken will.  Anders als im Falle DeCSS ist nun gar kein
Prozess und keine umstrittene Gesetzesnovelle (DMCA) mehr notwendig,
um Privatkopien zu verbieten und das kollektive Gedächtnis zu
beseitigen.  Ein Softwarepatent tut es auch.

#TFa: Avery Lee, author of VirtualDub reports the sad news: %(bq:Today I
received a polite phone call from a fellow at Microsoft who works in
the Windows Media group. He informed me that Microsoft has
intellectual property rights on the ASF format and told me that,
although the implementation was still illegal since it infringed on
Microsoft patents. I have asked for the specific patent numbers, since
I find patenting a file format a bit strange. At his request, and much
to my own sadness, I have removed support for ASF in VirtualDub 1.3d,
since I cannot risk a legal confrontation.)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-asf ;
# txtlang: de ;
# multlin: t ;
# End: ;

