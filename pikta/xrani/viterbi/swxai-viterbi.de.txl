<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Viterbi: 1 Algorithmus, 4470 Patente

#descr: Um den Viterbi-Algorithmus herum wurde in den frühen 90er Jahren in
Deutschland ein Exempel der Software-Patentierbarkeit statuiert.  Es
wurde gesagt, dieser Algorithmus löse ein %(q:technisches Problem) der
Datenübertragung unter Umschiffung von störenden Einwirkungen in
schwachen Leitungen und gehöre daher zu einer patentierbaren
Kategorie, die man von %(q:Datenverarbeitung als solcher) oder
%(q:reinen Geschäftsmethoden) abgrenzen könne.

#7D7: US 1974, EP 1992, DE 1996 ... 4470 Patente

#Exh: BPatG-Entscheidung %(q:Viterbi-Algorithmus) 1996

#iru: Juristisches Schrifttum

#cWg: Informatisches Schrifttum

#Vnz: Das erste Viterbi-Patent kam in den USA zur Anmeldung und zur
Erteilung:

#net: Erfinder

#sin: Rechteinhaber

#aor: Erteilungsdatum

#tAa: Anmelungsdatum

#snt: Insgesamt gibt es inzwischen 4470 Patente und Anmeldungen auf
Viterbi-Decoder und Viterbi-Algorithmus.....

#rWi: Corpus Delicti

#eni: Technizität

#eWi: Hintergrund der Patenterteilungen

#Tit: Titel

#tee: erteilte Patente

#rid: Erfinder

#Nam: Name

#des: Adresse

#ned: Anmelder

#eta: Deutsche Forschungsanstalt fuer Luft- und Raumfahrt eV

#wir: Verfahren zur Verallgemeinerung des Viterbi-Algorithmus, bei welchem
in einer Metrik-Inkrement-Einheit (TMU) die Übergangskosten gebildet
werden und in der nachgeschalteten Addier-Vergleich-Auswähl-(ACS-)Einh
eit ein Addieren, ein Vergleichen sowie ein Auswählen vorgenommen
werden, dadurch gekennzeichnet, daß%(ul|für jeden Zustand die
Differenzkosten von zwei eintreffenden Pfaden berechnet werden, wobei
eine Zuverlässigkeitsinformation am Anfang jedes Pfades auf den
höchsten Wert festgelegt wird,|dann der Zuverlässigkeitswert des
Pfades mit den kleinsten Kosten an den Stellen aufgefrischt wird, wo
die Informationsstellen von dem konkurrierenden Pfad abweichen, wobei
das Auffrischen gemäß einer Tabelle vorgenommen wird und wobei der
vorherige Wert der Zuverlässigkeitsinformation und die Differenzkosten
als Eingangsgröße in der Tabelle angelegt werden,|hierauf der neue
Wert aus der Tabelle entnommen wird und zusammen mit harten
Entscheidungen als Pfadgedächtnis abgespeichert wird, wobei das
Abspeichern in Form von Fest- oder Gleitpunkt-Werten erfolgt, und
schließlich die Analogwertentscheidung aus der Stelle herausgelesen
wird, die sich nach einer Entscheidungsverzögerung für den Pfad mit
den kleinsten Kosten ergibt, wo bei das Vorzeichen der
Analogwert-Entscheidungen die harten Entscheidungen des bekannten
Viterbi-Algorithmus sind.)

#tbc: Aufbauend auf dem bekanten Viterbi-Algorithmus (1967) mit dem
Kanal-Verzerrungen als %(q:Sendedatenfolgen) nachgebildet und die sich
ergebenden Zeitverläufe (%(q:Pfade)) mit dem tatsächlichen Signal
verglichen werden, wobei der ähnlichste Verlauf als signifikantester
Verlauf interpretiert wird.

#aee: Wie man sieht, weist auch der Patent-Anspruch nichts anderes auf als
einen mathematischen Algorithmus und virtuelle %(q:Tabellen), in denen
Zahlen stehen.

#Pul: Es gab beim DP schon 1975 eine Patentanmeldung die sich auf den
Viterbi-Algorithmus stützte:

#nWi: Synchronisationsschaltung fuer einen Viterbi-Dekodierer

#WmW: Diese kam aber nie zur Erteilung, weil der Patentanwalt wahrscheinlich
vom Prüfungsantrag beim DP abriet -- man hätte damals mit Sicherheit
NICHT erteilt.

#krE: Interessanterweise kam aber diese Patentanmeldung in den USA unter
US3872432: SYNCHRONIZATION CIRCUIT FOR A VITERBI DECODER zur
Erteilung.

#WWm: Ähnlich auch bei Hagenauer: Sowohl US-Pat als auch EP-Pat kamen
problemlos zur Erteilung:

#hee: Nicht so die DE-Anmeldung.

#gns: Hier gab es folgendes Prüfungsverlauf:

#Wa4: Prüfungsantrag gemäß §44 PatG

#nrn: Erteilung

#saW: Einspruch

#tfp: Patent mit Einschränkung bekräftigt

#erj: Patent an Fa. Samsung (KR Seoul) verkauft.

#cce: Im BPatG war dem Hörensagen nach der 21. Senat unter Richter %(WA)
zuständig, der sich als %(q:fortschrittlicher) Vorkämpfer der
grenzenlosen Patentierbarkeit bei der Patentanwaltschaft einen Namen
gemacht hat.

#ngu: Nach der vom BPatG. erfolgten Bestätigung der Etscheidung gab es in
Bezug auf Viterbi-Anmeldungen weltweit KEIN HALTEN mehr.  Inzwischen
gibt es hunderte, ja Tausende angemeldete und erteilte Patente.

#imW: Bei diesem Fall kommt ganz klar zu Tage:

#WWi: Beginnen die USA mit Patenterteilungen auf abstrakte Ideen, wächst der
Druck auf das EPA, ebenfalls Patente in diesen Bereichen zu erteilen.

#AcW: Erteilt das EPA Patente auf abstrakte Ideen, wächst beim Deutschen
Patentamt der Druck, nachzuziehen.

#Wra: GRUR schrieb erst 1996 darüber.

#Tin: %(WT) schreibt zu dieser BPatG-Entscheidung unter %(URL)

#nrs: Auf längere Sicht wird sich die Diskussion vom der Interpretation des
Begriffs %(q:Programm als solches) auf das Verständnis des Begriffs
%(q:Technik) und auf die Bewertung der Merkmale im Zusammenhang mit
der zur erfinderischen Tätigkeit verlagern. Beispielsweise die
Abgrenzung zwischen rein mathematischen Verfahren und technischen
Algorithmen wird eine systematische und allgemein handhabbare Regelung
erfordern. Ansätze dazu sind bereits vorhanden.

#acw: Interessant dazu auch unter %(URL) auf Seite 37 wo es heißt:

#pWn: ..zum Empfang von gestörten Kanal übertragenden Signalen..

#tfc: Das Technizitäts-Erfordernis wird hier offenbar durch eine bloße
Zweckangabe erfüllt.

#unl: Auch ein Computer Law Association Bulletin in Engl. gibt es dazu:
%(URL), s. Seite 10 (von 40)

#bcs: Weniger ergibig zu diesem Thema ist der %(bp:Jahresbericht des
Bundespatentgerichts)

#erd: Am Lehrstuhl für Nachrichtentechnik der Universität Kaiserslautern
wird intensiv über Weiterentwicklungen des Viterbi-Algorithmus und
anderer Algorithmen zur Entstörung von Informationen geforscht.  Die
Seite bietet eine Einführung in die Leistung des Viterbi-Algorithmus
und die heutigen Forschungsaufgaben.

#mot: Eine Abschlussarbeit im Fach Informatik über Varianten des Viterbi
Algorithmus.  Der Viterbi-Algorithmus ermittelt aus einem empfangenen,
codierten Datenblock die Daten, die der Sender mit höchster
Wahrscheinlichkeit gesendet hat. Auf dem Viterbi Algorithmus basieren
die List Viterbi Algorithmen, die nicht nur die wahrscheinlichste
Lösung, sondern eine Liste der n wahrscheinlichsten Lösungen (Pfade)
finden.  Die Arbeit betrachtet speziell deren Anwendung auf
Bilddatenübertragung und schlägt hier einige Verbesserungen vor.

#har: Künstliche Intelligenz: Spracherkennung und Sprachverstehen

#iWi: Der Viterbi-Algorithmus dient hier dazu, zu ermitteln, wie
wahrscheinlich es ist, dass ein bestimmt Wort durch einer bestimmte
Folge beobachteter Laute (oder ein Satz einer bestimmten Folge
beobachteter Sätze) entspricht.  Er ist in diesem Zusammenhang jedoch
nicht unbedingt das effektivste mögliche Verfahren.

#mre: Die deutsche Gerichtsentscheidung dokumentieren.

#efW: Wirkungen der Viterbi-Patente dokumentieren

#aer: Forscher und Unternehmer in betroffenen Bereichen befragen.

#tpT: Einige Viterbi-Patente der Testsuite unterziehen

#ytg: Genau untersuchen, wie die Viterbi-Patente gemäß verschiedenen
möglichen Patentierbarkeits-Kriterien abschneiden und ob dieses
Ergebnis dem Fortschritt dienen würde.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-viterbi ;
# txtlang: de ;
# multlin: t ;
# End: ;

