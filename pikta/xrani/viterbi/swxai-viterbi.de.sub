\begin{subdocument}{swxai-viterbi}{Viterbi: 1 Algorithmus, 4470 Patente}{http://swpat.ffii.org/pikta/xrani/viterbi/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2003/11/23 von Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{Um den Viterbi-Algorithmus herum wurde in den fr\"{u}hen 90er Jahren in Deutschland ein Exempel der Software-Patentierbarkeit statuiert.  Es wurde gesagt, dieser Algorithmus l\"{o}se ein ``technisches Problem'' der Daten\"{u}bertragung unter Umschiffung von st\"{o}renden Einwirkungen in schwachen Leitungen und geh\"{o}re daher zu einer patentierbaren Kategorie, die man von ``Datenverarbeitung als solcher'' oder ``reinen Gesch\"{a}ftsmethoden'' abgrenzen k\"{o}nne.}
\begin{sect}{hist}{US 1974, EP 1992, DE 1996 ... 4470 Patente}
Das erste Viterbi-Patent kam in den USA zur Anmeldung und zur Erteilung:

\begin{quote}
{\it US3872432: SYNCHRONIZATION CIRCUIT FOR A VITERBI DECODER}

{\it \begin{description}
\item[Erfinder:]\ Bismarck, Otto Herbert; Fords, NJ
\item[Rechteinhaber:]\ International Telephone and Telegraph Corporation, Nutley, NJ
\item[Erteilungsdatum:]\ 1975-03-18
\item[Anmelungsdatum:]\ 1974-04-10
\end{description}}
\end{quote}

Insgesamt gibt es inzwischen 4470 Patente und Anmeldungen auf Viterbi-Decoder und Viterbi-Algorithmus.....
\end{sect}

\begin{sect}{bpatg}{BPatG-Entscheidung ``Viterbi-Algorithmus'' 1996}
\begin{sect}{corpus}{Corpus Delicti}
\begin{description}
\item[Titel:]\ VERFAHREN ZUM VERALLGEMEINERN DES VITERBI-ALGORITHMUS UND EINRICHTUNGEN ZUR DURCHFUEHRUNG DES VERFAHRENS
\item[erteilte Patente:]\ DE3910739C3
EP 0391354B1\footnote{\url{../../txt/ep/0391/354}}
US 5181209\footnote{\url{http://www.delphion.com/details?&pn=us05181209__}}
\item[Erfinder:]\ \begin{description}
\item[Name:]\ Hagenauer, Joachim, Dr.-Ing; Hoeher, Peter, Dipl.-Ing.
\item[Adresse:]\ DE-8031 Seefeld
\end{description}
\item[Anmelder:]\ Deutsche Forschungsanstalt fuer Luft- und Raumfahrt eV
DE 5300 Bonn
\item[Anspruch 1:]\ \begin{quote}
{\it Verfahren zur Verallgemeinerung des Viterbi-Algorithmus, bei welchem in einer Metrik-Inkrement-Einheit (TMU) die \"{U}bergangskosten gebildet werden und in der nachgeschalteten Addier-Vergleich-Ausw\"{a}hl-(ACS-)Einheit ein Addieren, ein Vergleichen sowie ein Ausw\"{a}hlen vorgenommen werden, dadurch gekennzeichnet, da{\ss}\begin{itemize}
\item
f\"{u}r jeden Zustand die Differenzkosten von zwei eintreffenden Pfaden berechnet werden, wobei eine Zuverl\"{a}ssigkeitsinformation am Anfang jedes Pfades auf den h\"{o}chsten Wert festgelegt wird,

\item
dann der Zuverl\"{a}ssigkeitswert des Pfades mit den kleinsten Kosten an den Stellen aufgefrischt wird, wo die Informationsstellen von dem konkurrierenden Pfad abweichen, wobei das Auffrischen gem\"{a}{\ss} einer Tabelle vorgenommen wird und wobei der vorherige Wert der Zuverl\"{a}ssigkeitsinformation und die Differenzkosten als Eingangsgr\"{o}{\ss}e in der Tabelle angelegt werden,

\item
hierauf der neue Wert aus der Tabelle entnommen wird und zusammen mit harten Entscheidungen als Pfadged\"{a}chtnis abgespeichert wird, wobei das Abspeichern in Form von Fest- oder Gleitpunkt-Werten erfolgt, und schlie{\ss}lich die Analogwertentscheidung aus der Stelle herausgelesen wird, die sich nach einer Entscheidungsverz\"{o}gerung f\"{u}r den Pfad mit den kleinsten Kosten ergibt, wo bei das Vorzeichen der Analogwert-Entscheidungen die harten Entscheidungen des bekannten Viterbi-Algorithmus sind.
\end{itemize}}
\end{quote}
\end{description}
\end{sect}

\begin{sect}{anlays}{Technizit\"{a}t}
Aufbauend auf dem bekanten Viterbi-Algorithmus (1967) mit dem Kanal-Verzerrungen als ``Sendedatenfolgen'' nachgebildet und die sich ergebenden Zeitverl\"{a}ufe (``Pfade'') mit dem tats\"{a}chlichen Signal verglichen werden, wobei der \"{a}hnlichste Verlauf als signifikantester Verlauf interpretiert wird.

Wie man sieht, weist auch der Patent-Anspruch nichts anderes auf als einen mathematischen Algorithmus und virtuelle ``Tabellen'', in denen Zahlen stehen.
\end{sect}

\begin{sect}{bakgr}{Hintergrund der Patenterteilungen}
Es gab beim DP schon 1975 eine Patentanmeldung die sich auf den Viterbi-Algorithmus st\"{u}tzte:

\begin{description}
\item[DE2515038A1:]\ Synchronisationsschaltung fuer einen Viterbi-Dekodierer
\end{description}

Diese kam aber nie zur Erteilung, weil der Patentanwalt wahrscheinlich vom Pr\"{u}fungsantrag beim DP abriet -- man h\"{a}tte damals mit Sicherheit NICHT erteilt.

Interessanterweise kam aber diese Patentanmeldung in den USA unter US3872432: SYNCHRONIZATION CIRCUIT FOR A VITERBI DECODER zur Erteilung.

\"{A}hnlich auch bei Hagenauer: Sowohl US-Pat als auch EP-Pat kamen problemlos zur Erteilung:

\begin{description}
\item[US-Patent:]\ 1993-01-19
US5181209
Method for generalizing the viterbi algorithm and devices for executing the method
\item[EP-Patent:]\ 1994-11-09
EP0391354B1
Method of generalizing the Viterbi-algorithm and apparatus to carry out the method
\end{description}

Nicht so die DE-Anmeldung.

Hier gab es folgendes Pr\"{u}fungsverlauf:

\begin{quote}
{\it \begin{center}
\begin{longtable}{R{44}L{44}}
1990-10-11 & OP8 + Pr\"{u}fungsantrag gem\"{a}{\ss} \S{}44 PatG\\
1992-05-21 & D2 + Erteilung\\
1992-10-29 & 8363 - Einspruch\\
1996-09-26 & 8366 + Patent mit Einschr\"{a}nkung bekr\"{a}ftigt\\
2002-05-23 & 8327 Patent an Fa. Samsung (KR Seoul) verkauft.\\
\end{longtable}
\end{center}}
\end{quote}

Im BPatG war dem H\"{o}rensagen nach der 21. Senat unter Richter Wilfried Anders\footnote{\url{http://localhost/swpat/gasnu/anders/index.de.html}} zust\"{a}ndig, der sich als ``fortschrittlicher'' Vork\"{a}mpfer der grenzenlosen Patentierbarkeit bei der Patentanwaltschaft einen Namen gemacht hat.

Nach der vom BPatG. erfolgten Best\"{a}tigung der Etscheidung gab es in Bezug auf Viterbi-Anmeldungen weltweit KEIN HALTEN mehr.  Inzwischen gibt es hunderte, ja Tausende angemeldete und erteilte Patente.

Bei diesem Fall kommt ganz klar zu Tage:

Beginnen die USA mit Patenterteilungen auf abstrakte Ideen, w\"{a}chst der Druck auf das EPA, ebenfalls Patente in diesen Bereichen zu erteilen.

Erteilt das EPA Patente auf abstrakte Ideen, w\"{a}chst beim Deutschen Patentamt der Druck, nachzuziehen.
\end{sect}
\end{sect}

\begin{sect}{jurlit}{Juristisches Schrifttum}
GRUR schrieb erst 1996 dar\"{u}ber.: BPatG GRUR 1996, 866 - ``Viterbi-Algorithmus''

Wolfgang Tauchert\footnote{\url{http://localhost/swpat/gasnu/tauchert/index.de.html}} schreibt zu dieser BPatG-Entscheidung unter \url{http://www.jurpc.de/aufsatz/20010040.htm\#rfn33}

\begin{quote}
{\it Auf l\"{a}ngere Sicht wird sich die Diskussion vom der Interpretation des Begriffs ``Programm als solches'' auf das Verst\"{a}ndnis des Begriffs ``Technik'' und auf die Bewertung der Merkmale im Zusammenhang mit der zur erfinderischen T\"{a}tigkeit verlagern. Beispielsweise die Abgrenzung zwischen rein mathematischen Verfahren und technischen Algorithmen wird eine systematische und allgemein handhabbare Regelung erfordern. Ans\"{a}tze dazu sind bereits vorhanden.}
\end{quote}

Interessant dazu auch unter \url{http://www.innovation.co.at/IMG/TM_SOFTWARE.PDF} auf Seite 37 wo es hei{\ss}t:

\begin{quote}
{\it VITERBI-Algorithmus}

{\it ..zum Empfang von gest\"{o}rten Kanal \"{u}bertragenden Signalen..}
\end{quote}

Das Technizit\"{a}ts-Erfordernis wird hier offenbar durch eine blo{\ss}e Zweckangabe erf\"{u}llt.

Auch ein Computer Law Association Bulletin in Engl. gibt es dazu: \url{http://www.cla.org/007844_10CLA_jj.pdf}, s. Seite 10 (von 40)

Weniger ergibig zu diesem Thema ist der Jahresbericht des Bundespatentgerichts\footnote{\url{http://www.cla.org/007844_10CLA_jj.pdf}}
\end{sect}

\begin{sect}{inflit}{Informatisches Schrifttum}
\begin{itemize}
\item
{\bf {\bf Viterbi-Algorithmus und Sequentielle Detektionsalgorithmen\footnote{\url{http://nt.eit.uni-kl.de/forschung/mlse.html}}}}

\begin{quote}
Am Lehrstuhl f\"{u}r Nachrichtentechnik der Universit\"{a}t Kaiserslautern wird intensiv \"{u}ber Weiterentwicklungen des Viterbi-Algorithmus und anderer Algorithmen zur Entst\"{o}rung von Informationen geforscht.  Die Seite bietet eine Einf\"{u}hrung in die Leistung des Viterbi-Algorithmus und die heutigen Forschungsaufgaben.
\end{quote}
\filbreak

\item
{\bf {\bf \url{http://www.dip.ee.uct.ac.za/\~wajnel/example1.html}}}
\filbreak

\item
{\bf {\bf R\"{o}der Martin: Effiziente Viterbi-Dekodierung und Anwendung auf die Bild\"{u}bertragung in gest\"{o}rten Kan\"{a}len\footnote{\url{http://dol.uni-leipzig.de/pub/2001-48}}}}

\begin{quote}
Eine Abschlussarbeit im Fach Informatik \"{u}ber Varianten des Viterbi Algorithmus.  Der Viterbi-Algorithmus ermittelt aus einem empfangenen, codierten Datenblock die Daten, die der Sender mit h\"{o}chster Wahrscheinlichkeit gesendet hat. Auf dem Viterbi Algorithmus basieren die List Viterbi Algorithmen, die nicht nur die wahrscheinlichste L\"{o}sung, sondern eine Liste der n wahrscheinlichsten L\"{o}sungen (Pfade) finden.  Die Arbeit betrachtet speziell deren Anwendung auf Bilddaten\"{u}bertragung und schl\"{a}gt hier einige Verbesserungen vor.
\end{quote}
\filbreak

\item
{\bf {\bf K\"{u}nstliche Intelligenz: Spracherkennung und Sprachverstehen\footnote{\url{http://www.fask.uni-mainz.de/user/warth/Ki.html}}}}

\begin{quote}
Der Viterbi-Algorithmus dient hier dazu, zu ermitteln, wie wahrscheinlich es ist, dass ein bestimmt Wort durch einer bestimmte Folge beobachteter Laute (oder ein Satz einer bestimmten Folge beobachteter S\"{a}tze) entspricht.  Er ist in diesem Zusammenhang jedoch nicht unbedingt das effektivste m\"{o}gliche Verfahren.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
Wenn Sie Fragen zum Projekt swpat\footnote{\url{http://localhost/ffii/index.de.html\#swpat}} haben, z\"{o}gern Sie bitte nicht, sich mit swpat-help@ffii.org in Verbindung zu setzen.

\begin{itemize}
\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Die deutsche Gerichtsentscheidung dokumentieren.}}
\filbreak

\item
{\bf {\bf Wirkungen der Viterbi-Patente dokumentieren}}

\begin{quote}
Forscher und Unternehmer in betroffenen Bereichen befragen.
\end{quote}
\filbreak

\item
{\bf {\bf Einige Viterbi-Patente der Testsuite unterziehen}}

\begin{quote}
siehe Patentability Legislation Benchmarking Test Suite\footnote{\url{http://localhost/swpat/stidi/manri/index.de.html}}

Genau untersuchen, wie die Viterbi-Patente gem\"{a}{\ss} verschiedenen m\"{o}glichen Patentierbarkeits-Kriterien abschneiden und ob dieses Ergebnis dem Fortschritt dienen w\"{u}rde.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

