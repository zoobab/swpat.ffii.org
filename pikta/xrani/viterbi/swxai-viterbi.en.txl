<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Viterbi: 1 Algorithm, 4470 Patents

#descr: The Viterbi Algorithm helps calculate the probability that a sequence
of observations represents an intended ideal construction (e.g. that a
mispronounced word is actually that word).  It was published in 1967
and has a vast field of applications.  An exemplary case of software
patentability was established around  algorithm in 1992-1996 in
Germany.  At the EPO and USPTO the dam broke earlier, resulting in a
flood of 4470 patents which clutter various fields from
telecommunication, data transmission to speech recognition, text
processing and business process optimisation.  These patents are
sometimes cited as an example of a kind of data processing that solves
%(q:technical problems) and can be distinguished from %(q:data
processing as such) or %(q:pure business methods).

#7D7: US 1974, EP 1992, DE 1996... 4470 Patents

#Exh: German Opposition against Viterbi Patent 1992-1996

#iru: Legal Literature

#cWg: Informatic Literature

#Vnz: The first Viterbi patent was applied for and granted in the USA in the
1970s:

#net: Inventor

#sin: Assignee

#aor: Date of Grant

#tAa: Date of Application

#snt: Meanwhile there is a total of 4470 patents and patent applications on
viterbi decoders and viterbi-based algorithms

#rWi: Corpus Delicti

#eni: Technicity

#eWi: Background of Patent Granting

#Tit: Titel

#tee: erteilte Patente

#rid: Erfinder

#Nam: Name

#des: Adresse

#ned: Anmelder

#eta: Deutsche Forschungsanstalt fuer Luft- und Raumfahrt eV

#wir: Verfahren zur Verallgemeinerung des Viterbi-Algorithmus, bei welchem
in einer Metrik-Inkrement-Einheit (TMU) die Übergangskosten gebildet
werden und in der nachgeschalteten Addier-Vergleich-Auswähl-(ACS-)Einh
eit ein Addieren, ein Vergleichen sowie ein Auswählen vorgenommen
werden, dadurch gekennzeichnet, daß%(ul|für jeden Zustand die
Differenzkosten von zwei eintreffenden Pfaden berechnet werden, wobei
eine Zuverlässigkeitsinformation am Anfang jedes Pfades auf den
höchsten Wert festgelegt wird,|dann der Zuverlässigkeitswert des
Pfades mit den kleinsten Kosten an den Stellen aufgefrischt wird, wo
die Informationsstellen von dem konkurrierenden Pfad abweichen, wobei
das Auffrischen gemäß einer Tabelle vorgenommen wird und wobei der
vorherige Wert der Zuverlässigkeitsinformation und die Differenzkosten
als Eingangsgröße in der Tabelle angelegt werden,|hierauf der neue
Wert aus der Tabelle entnommen wird und zusammen mit harten
Entscheidungen als Pfadgedächtnis abgespeichert wird, wobei das
Abspeichern in Form von Fest- oder Gleitpunkt-Werten erfolgt, und
schließlich die Analogwertentscheidung aus der Stelle herausgelesen
wird, die sich nach einer Entscheidungsverzögerung für den Pfad mit
den kleinsten Kosten ergibt, wo bei das Vorzeichen der
Analogwert-Entscheidungen die harten Entscheidungen des bekannten
Viterbi-Algorithmus sind.)

#tbc: The %(q:invention) consists in a generalisation on the Viterbi
algorithm of 1967, by which channel distortions are imitated as
%(q:sequences of transmission data) and the resulting time curves
(%(q:paths)) are compared to the real signal, such that the most
similar curve is interpreted as the most significant one.

#aee: As can be seen, the patent claim contains nothing but a mathematical
method, adding some lookup tables of numerals to the Viterbi
algorithm.

#Pul: Es gab beim DP schon 1975 eine Patentanmeldung die sich auf den
Viterbi-Algorithmus stützte:

#nWi: Synchronisationsschaltung fuer einen Viterbi-Dekodierer

#WmW: Diese kam aber nie zur Erteilung, weil der Patentanwalt wahrscheinlich
vom Prüfungsantrag beim DP abriet -- man hätte damals mit Sicherheit
NICHT erteilt.

#krE: Interessanterweise kam aber diese Patentanmeldung in den USA unter
US3872432: SYNCHRONIZATION CIRCUIT FOR A VITERBI DECODER zur
Erteilung.

#WWm: Ähnlich auch bei Hagenauer: Sowohl US-Pat als auch EP-Pat kamen
problemlos zur Erteilung:

#hee: Nicht so die DE-Anmeldung.

#gns: Hier gab es folgendes Prüfungsverlauf:

#Wa4: Request for examination as to paragraph 44 patent law

#nrn: Grant after examination

#saW: Opposition against the patent

#tfp: Restricted maintained after opposition proceedings

#erj: Change in the person/name/address of the patent owner (New owner:
SAMSUNG ELECTRONICS CO., LTD., SEOUL/SOUL, KR

#cce: Im BPatG war dem Hörensagen nach der 21. Senat unter Richter %(WA)
zuständig, der sich als %(q:fortschrittlicher) Vorkämpfer der
grenzenlosen Patentierbarkeit bei der Patentanwaltschaft einen Namen
gemacht hat.

#ngu: Nach der vom BPatG. erfolgten Bestätigung der Etscheidung gab es in
Bezug auf Viterbi-Anmeldungen weltweit KEIN HALTEN mehr.  Inzwischen
gibt es hunderte, ja Tausende angemeldete und erteilte Patente.

#imW: Bei diesem Fall kommt ganz klar zu Tage:

#WWi: Beginnen die USA mit Patenterteilungen auf abstrakte Ideen, wächst der
Druck auf das EPA, ebenfalls Patente in diesen Bereichen zu erteilen.

#AcW: Erteilt das EPA Patente auf abstrakte Ideen, wächst beim Deutschen
Patentamt der Druck, nachzuziehen.

#Wra: GRUR schrieb erst 1996 darüber.

#Tin: %(WT) schreibt zu dieser BPatG-Entscheidung unter %(URL)

#nrs: Auf längere Sicht wird sich die Diskussion vom der Interpretation des
Begriffs %(q:Programm als solches) auf das Verständnis des Begriffs
%(q:Technik) und auf die Bewertung der Merkmale im Zusammenhang mit
der zur erfinderischen Tätigkeit verlagern. Beispielsweise die
Abgrenzung zwischen rein mathematischen Verfahren und technischen
Algorithmen wird eine systematische und allgemein handhabbare Regelung
erfordern. Ansätze dazu sind bereits vorhanden.

#acw: Interessant dazu auch unter %(URL) auf Seite 37 wo es heißt:

#pWn: ..zum Empfang von gestörten Kanal übertragenden Signalen..

#tfc: Das Technizitäts-Erfordernis wird hier offenbar durch eine bloße
Zweckangabe erfüllt.

#unl: Auch ein Computer Law Association Bulletin in Engl. gibt es dazu:
%(URL), s. Seite 10 (von 40)

#bcs: Weniger ergibig zu diesem Thema ist der %(bp:Jahresbericht des
Bundespatentgerichts)

#erd: Am Lehrstuhl für Nachrichtentechnik der Universität Kaiserslautern
wird intensiv über Weiterentwicklungen des Viterbi-Algorithmus und
anderer Algorithmen zur Entstörung von Informationen geforscht.  Die
Seite bietet eine Einführung in die Leistung des Viterbi-Algorithmus
und die heutigen Forschungsaufgaben.

#mot: Eine Abschlussarbeit im Fach Informatik über Varianten des Viterbi
Algorithmus.  Der Viterbi-Algorithmus ermittelt aus einem empfangenen,
codierten Datenblock die Daten, die der Sender mit höchster
Wahrscheinlichkeit gesendet hat. Auf dem Viterbi Algorithmus basieren
die List Viterbi Algorithmen, die nicht nur die wahrscheinlichste
Lösung, sondern eine Liste der n wahrscheinlichsten Lösungen (Pfade)
finden.  Die Arbeit betrachtet speziell deren Anwendung auf
Bilddatenübertragung und schlägt hier einige Verbesserungen vor.

#har: Künstliche Intelligenz: Spracherkennung und Sprachverstehen

#iWi: Der Viterbi-Algorithmus dient hier dazu, zu ermitteln, wie
wahrscheinlich es ist, dass ein bestimmt Wort durch einer bestimmte
Folge beobachteter Laute (oder ein Satz einer bestimmten Folge
beobachteter Sätze) entspricht.  Er ist in diesem Zusammenhang jedoch
nicht unbedingt das effektivste mögliche Verfahren.

#mre: Document the german cout decision

#efW: Document effects of the Viterbi patents

#aer: Ask researchers and entrepreneurs in the concerned fields

#tpT: Subject some Viterbi patents to the Test Suite

#ytg: Examine closely to what extent patents in the Viterbi would be
patentable according to different candidate patentability standards
and whether that would serve to promote progress of science and the
useful arts.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-viterbi ;
# txtlang: en ;
# multlin: t ;
# End: ;

