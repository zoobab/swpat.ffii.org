<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Rozmanith: Utilisation des brevets logiciels pour faire taire les
critiques

#descr: À l'automne 2000, TechSearch Inc, société spécialisée dans
l'acquisition et l'exploitation de brevets, a attaqué en justice
Gregory Aharonian, critique virulent des %(q:brevets sans fondement)
de TechSearch, pour contrefaçon supposée de l'un de ces %(q:brevets
sans fondement) : le %(q:brevet Rozmanith) US 5253341 sur la
compression des données transmises à partir de serveurs web. La
société a également accusé Aharonian de diffamation envers TechSearch,
l'Office des Brevets et le Gouvernement des États-Unis. Mais elle n'a
pas réussi à montrer comment Aharonian avait empiété sur les droits
attachés au brevet. Apparemment, toute personne gérant un serveur web
empiète sur ces droits et c'est à TechSearch qu'il revient de choisir
ses victimes. Aharonian est la première personne privée, utilisatrice
de Linux, à être poursuivi pour contrefaçon de brevet.

#eei: En 2003, Aharonian a rapporté dans %(q:PatNews) qu'il a gagné le
procès contre TechSearch.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: mgaroche ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-rozmanith ;
# txtlang: fr ;
# multlin: t ;
# End: ;

