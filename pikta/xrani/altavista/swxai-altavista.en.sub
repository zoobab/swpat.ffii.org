\begin{subdocument}{swxai-altavista}{No more WWW indexing without permission from CMGI?}{http://swpat.ffii.org/pikta/xrani/altavista/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{In Jan 2001, the CEO of CMGI, the company that currently owns Altavista, explained: ``Altavista owns 38 patents, many of which we think are fundamental in the search area. They were the first to spider and index the Web. ... And we have another 30 patents that are in application. So we believe that virtually everyone out there who indexes the Web is in violation of at least several of those key patents.'' and made it clear that he will go to court in early 2001 to maximize revenues from those patents.}
\begin{sect}{press}{The Start Shot}
On 2000-11-13, CGMI, the holding that owns Altavista and 12 more companies, announce in a highly publicized press release\footnote{\url{http://biz.yahoo.com:80/prnews/001113/ca_altavis.html}} that they own a monopololy on key areas of web searching.  At this time, CGMI was in a phase of ``restructuring'', i.e. laying off employees and trying to become profitable.  On 2001-11-15, CGMI's CEO David Wetherell gave an interview\footnote{\url{http://www.internetworld.com/011501/01.15.01interview.jsp}} in which he made it clear that he intended to crack down on anyone who searched the web without license:

\begin{quote}
\begin{description}
\item[Internet World:]\ Can we talk a bit about some of the ideas or opportunities that you backed off of because money was an issue this past year?
\item[David Wetherell:]\ [...]
Even though AltaVista s doing well in the advertising space, we just think that in order to really ensure strong growth they ought to leverage their position in search licensing to a greater extent. And we saw the opportunity to do that because we think it s a big market. They happen to own 38 patents, many of which we think are fundamental in the search area. They were the first to spider and index the Web. And Digital did a good job of recognizing the potential value of that intellectual property. And they were very thorough in filing broad and deep and narrow patents. And we have another 30 patents that are in application. So we believe that virtually everyone out there who indexes the Web is in violation of at least several of those key patents.
\item[IW:]\ Does that mean you ll pursue that?
\item[DW:]\ Yes, we will. Coming up in the first quarter of 2001.
\item[IW:]\ So we may see some lawsuits ...
\item[DW:]\ If necessary, we will defend it, to the letter of the law.
\item[IW:]\ Are there any specific examples of the types of patents?
\item[DW:]\ If you index a distributed set of databases what the Internet is and even within intranets, corporations, that s one of the patents.  We did a press release\footnote{\url{http://biz.yahoo.com:80/prnews/001113/ca_altavis.html}} on this with a list of six or ten of the key areas that the patents cover.
\end{description}
\end{quote}
\end{sect}

\begin{sect}{patents}{Patent Samples}
Most of the European applications are still awaiting examination at the EPO.

\begin{itemize}
\item
{\bf {\bf US 5864863\footnote{\url{http://www.delphion.com/details?&pn=us05864863__}}}}

\begin{quote}
I claim
\begin{enumerate}
\item
A system for indexing Web pages of the Internet, the pages stored in computers connected to each other by a communications network, each page having a unique URL (universal record locator), some of the pages including URL links to other pages, comprising: a communication interface for fetching a batch of specified pages of the Web from the computers in accordance with the URLs and URL links;\\
a parser sequentially partitioning the batch of specified pages into indexable words, each word representing a portion of one specified page or an attribute of one or more portions of the specified page, the parser sequentially assigning locations to the words as they are parsed;\\
a memory storing index entries, each index entry including a word entry representing a unique one of the words, and one or more location entries indicating where the unique word occurs in the Web;\\
a query module parsing a query into terms and operators relating the terms;\\
a search engine using object-oriented stream readers to sequentially read location of specified index entries, the specified index entries corresponding to the terms of a query;\\
and a display module for presenting qualified pages located by the search engine to users of the Web.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf US 5974455\footnote{\url{http://www.delphion.com/details?&pn=us05974455__}}}}

\begin{quote}
\begin{enumerate}
\item
A system for locating Web pages stored on remotely located computers, each Web page having a unique URL (universal resource locator), at least some of said Web pages including URL links to other ones of the Web pages, the system comprising:
\begin{itemize}
\item
a communications interface for fetching specified ones of the Web pages from said remotely located computers in accordance with corresponding URLs;

\item
a Web information file having a set of entries, each entry denoting, for a corresponding Web page, a URL and fetch status information;

\item
a Web information table, stored in RAM (random access memory), having a set of entries, each entry denoting a fingerprint value and fetch status information for a corresponding Web page; and

\item
a Web scooter procedure, executed by the system, for fetching and analyzing Web pages, said Web scooter procedure including instructions for fetching Web pages whose Web information file entries meet predefined selection criteria based on said fetch status information, for determining for each URL link in each received Web page whether a corresponding entry already exists in the Web information table, and for each URL link which does not have a corresponding entry in the Web information table adding a new entry in the Web information table and a corresponding new entry in the Web information file.
\end{itemize}
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf EP 444358\footnote{\url{../../txt/ep/0444/358}} = US 5495608\footnote{\url{http://www.delphion.com/details?&pn=us05495608__}}}}

\begin{quote}
Dynamic optimization of a single relation access.
\end{quote}
\filbreak

\item
{\bf {\bf EP 458698\footnote{\url{../../txt/ep/0458/698}} = US 5276868\footnote{\url{http://www.delphion.com/details?&pn=us05276868__}}}}

\begin{quote}
Method and apparatus for pointer compression in structured
\end{quote}
\filbreak

\item
{\bf {\bf EP 520459\footnote{\url{../../txt/ep/0520/459}} = US 5347653\footnote{\url{http://www.delphion.com/details?&pn=us05347653__}}}}

\begin{quote}
A method and apparatus for indexing and retrieval of object versions in a versioned data base.
\end{quote}
\filbreak

\item
{\bf {\bf EP 522363\footnote{\url{../../txt/ep/0522/363}} = US 5204958\footnote{\url{http://www.delphion.com/details?&pn=us05204958__}}}}

\begin{quote}
System and method for efficiently indexing and storing large database with high-data insertion frequency.
\end{quote}
\filbreak

\item
{\bf {\bf EP 551243\footnote{\url{../../txt/ep/0551/243}} = US 5519858\footnote{\url{http://www.delphion.com/details?&pn=us05519858__}}}}

\begin{quote}
Address recognition engine.
\end{quote}
\filbreak

\item
{\bf {\bf EP 567355\footnote{\url{../../txt/ep/0567/355}} (combination of 5 US applications)}}

\begin{quote}
A method and apparatus for operating a multiprocessor computer system having cache memories.
\end{quote}
\filbreak

\item
{\bf {\bf EP 886227\footnote{\url{../../txt/ep/0886/227}}}}

\begin{quote}
Full-text indexed mail repository
\end{quote}
\filbreak

\item
{\bf {\bf EP 886228\footnote{\url{../../txt/ep/0886/228}}}}

\begin{quote}
WWW-based mail service system
\end{quote}
\filbreak

\item
{\bf {\bf US 5226150\footnote{\url{http://www.delphion.com/details?&pn=us05226150__}}}}

\begin{quote}
Apparatus for suppressing an error report from an address for which an error has already been reported
\end{quote}
\filbreak

\item
{\bf {\bf US 5276872\footnote{\url{http://www.delphion.com/details?&pn=us05276872__}}}}

\begin{quote}
Concurrency and recovery for index trees with nodal updates using multiple atomic actions by which the trees integrity is preserved during u 
\end{quote}
\filbreak

\item
{\bf {\bf US 5276874\footnote{\url{http://www.delphion.com/details?&pn=us05276874__}}}}

\begin{quote}
Method for creating a directory tree in main memory using an index file in secondary memory
\end{quote}
\filbreak

\item
{\bf {\bf US 5394143\footnote{\url{http://www.delphion.com/details?&pn=us05394143__}}}}

\begin{quote}
Run-length compression of index keys
\end{quote}
\filbreak

\item
{\bf {\bf US 5506984\footnote{\url{http://www.delphion.com/details?&pn=us05506984__}}}}

\begin{quote}
Method and system for data retrieval in a distributed system using linked location references on a plurality of nodes
\end{quote}
\filbreak

\item
{\bf {\bf US 5553258\footnote{\url{http://www.delphion.com/details?&pn=us05553258__}}}}

\begin{quote}
Method and apparatus for forming an exchange address for a system with different size caches
\end{quote}
\filbreak

\item
{\bf {\bf US 5671406\footnote{\url{http://www.delphion.com/details?&pn=us05671406__}}}}

\begin{quote}
Data structure enhancements for in-place sorting of a singly linked list
\end{quote}
\filbreak

\item
{\bf {\bf US 5717921\footnote{\url{http://www.delphion.com/details?&pn=us05717921__}}}}

\begin{quote}
Concurrency and recovery for index trees with nodal updates using multiple atomic actions
\end{quote}
\filbreak

\item
{\bf {\bf US 5745890\footnote{\url{http://www.delphion.com/details?&pn=us05745890__}}}}

\begin{quote}
Sequential searching of a database index using constraints on word-location pairs 
\end{quote}
\filbreak

\item
{\bf {\bf US 5745894\footnote{\url{http://www.delphion.com/details?&pn=us05745894__}}}}

\begin{quote}
Method for generating and searching a range-based index of word-locations
\end{quote}
\filbreak

\item
{\bf {\bf US 5745898\footnote{\url{http://www.delphion.com/details?&pn=us05745898__}}}}

\begin{quote}
Method for generating a compressed index of information of records of a database
\end{quote}
\filbreak

\item
{\bf {\bf US 5745899\footnote{\url{http://www.delphion.com/details?&pn=us05745899__}}}}

\begin{quote}
Method for indexing information of a database
\end{quote}
\filbreak

\item
{\bf {\bf US 5745900\footnote{\url{http://www.delphion.com/details?&pn=us05745900__}}}}

\begin{quote}
Method for indexing duplicate database records using a full-record fingerprint
\end{quote}
\filbreak

\item
{\bf {\bf US 5765158\footnote{\url{http://www.delphion.com/details?&pn=us05765158__}}}}

\begin{quote}
Method for sampling a compressed index to create a summarized index
\end{quote}
\filbreak

\item
{\bf {\bf US 5765168\footnote{\url{http://www.delphion.com/details?&pn=us05765168__}}}}

\begin{quote}
Method for maintaining an index
\end{quote}
\filbreak

\item
{\bf {\bf US 5787435\footnote{\url{http://www.delphion.com/details?&pn=us05787435__}}}}

\begin{quote}
Method for mapping an index of a database into an array of files
\end{quote}
\filbreak

\item
{\bf {\bf US 5794242\footnote{\url{http://www.delphion.com/details?&pn=us05794242__}}}}

\begin{quote}
Temporally and spatially organized database
\end{quote}
\filbreak

\item
{\bf {\bf US 5797008\footnote{\url{http://www.delphion.com/details?&pn=us05797008__}}}}

\begin{quote}
Memory storing an integrated index of database records
\end{quote}
\filbreak

\item
{\bf {\bf US 5809502\footnote{\url{http://www.delphion.com/details?&pn=us05809502__}}}}

\begin{quote}
Object-oriented interface for an index
\end{quote}
\filbreak

\item
{\bf {\bf US 5829051\footnote{\url{http://www.delphion.com/details?&pn=us05829051__}}}}

\begin{quote}
Apparatus and method for intelligent multiple-probe cache allocation
\end{quote}
\filbreak

\item
{\bf {\bf US 5832500\footnote{\url{http://www.delphion.com/details?&pn=us05832500__}}}}

\begin{quote}
Method for searching an index
\end{quote}
\filbreak

\item
{\bf {\bf US 5852820\footnote{\url{http://www.delphion.com/details?&pn=us05852820__}}}}

\begin{quote}
Method for optimizing entries for searching an index
\end{quote}
\filbreak

\item
{\bf {\bf US 5864863\footnote{\url{http://www.delphion.com/details?&pn=us05864863__}}}}

\begin{quote}
Method for parsing, indexing and searching world-wide-web pages
\end{quote}
\filbreak

\item
{\bf {\bf US 5915251\footnote{\url{http://www.delphion.com/details?&pn=us05915251__}}}}

\begin{quote}
Method and apparatus for generating and searching range-based index of word locations
\end{quote}
\filbreak

\item
{\bf {\bf US 5953747\footnote{\url{http://www.delphion.com/details?&pn=us05953747__}}}}

\begin{quote}
Apparatus and method for serialized set prediction
\end{quote}
\filbreak

\item
{\bf {\bf US 5956758\footnote{\url{http://www.delphion.com/details?&pn=us05956758__}}}}

\begin{quote}
Method for determining target address of computed jump instructions in executable programs
\end{quote}
\filbreak

\item
{\bf {\bf US 5963954\footnote{\url{http://www.delphion.com/details?&pn=us05963954__}}}}

\begin{quote}
Method for mapping an index of a database into an array of files
\end{quote}
\filbreak

\item
{\bf {\bf US 5966710\footnote{\url{http://www.delphion.com/details?&pn=us05966710__}}}}

\begin{quote}
Method for searching an index
\end{quote}
\filbreak

\item
{\bf {\bf US 5966735\footnote{\url{http://www.delphion.com/details?&pn=us05966735__}}}}

\begin{quote}
Array index chaining for tree structure save and restore in a process swapping system
\end{quote}
\filbreak

\item
{\bf {\bf US 5970497\footnote{\url{http://www.delphion.com/details?&pn=us05970497__}}}}

\begin{quote}
Method for indexing duplicate records of information of a database
\end{quote}
\filbreak

\item
{\bf {\bf US 5987544\footnote{\url{http://www.delphion.com/details?&pn=us05987544__}}}}

\begin{quote}
System interface protocol with optional module cache
\end{quote}
\filbreak

\item
{\bf {\bf US 6016493\footnote{\url{http://www.delphion.com/details?&pn=us06016493__}}}}

\begin{quote}
Method for generating a compressed index of information of records of a database
\end{quote}
\filbreak

\item
{\bf {\bf US 6021409\footnote{\url{http://www.delphion.com/details?&pn=us06021409__}}}}

\begin{quote}
Method for parsing, indexing and searching world-wide-web pages
\end{quote}
\filbreak

\item
{\bf {\bf US 6029164\footnote{\url{http://www.delphion.com/details?&pn=us06029164__}}}}

\begin{quote}
Method and apparatus for organizing and accessing electronic mail messages using labels and full text and label indexing
\end{quote}
\filbreak

\item
{\bf {\bf US 6047286\footnote{\url{http://www.delphion.com/details?&pn=us06047286__}}}}

\begin{quote}
Method for optimizing entries for searching an index
\end{quote}
\filbreak

\item
{\bf {\bf US 6067543\footnote{\url{http://www.delphion.com/details?&pn=us06067543__}}}}

\begin{quote}
Object-oriented interface for an index
\end{quote}
\filbreak

\item
{\bf {\bf US 6078923\footnote{\url{http://www.delphion.com/details?&pn=us06078923__}}}}

\begin{quote}
Memory storing an integrated index of database records
\end{quote}
\filbreak

\item
{\bf {\bf US 6092101\footnote{\url{http://www.delphion.com/details?&pn=us06092101__}}}}

\begin{quote}
Method for filtering mail messages for a plurality of client computers connected to a mail service system
\end{quote}
\filbreak

\item
{\bf {\bf US 6105019\footnote{\url{http://www.delphion.com/details?&pn=us06105019__}}}}

\begin{quote}
Constrained searching of an index
\end{quote}
\filbreak

\item
{\bf {\bf US 6108770\footnote{\url{http://www.delphion.com/details?&pn=us06108770__}}}}

\begin{quote}
Method and apparatus for predicting memory dependence using store sets
\end{quote}
\filbreak

\item
{\bf {\bf US 6112203\footnote{\url{http://www.delphion.com/details?&pn=us06112203__}}}}

\begin{quote}
Method for ranking documents in a hyperlinked environment using connectivity and selective content analysis
\end{quote}
\filbreak

\item
{\bf {\bf US 6138113\footnote{\url{http://www.delphion.com/details?&pn=us06138113__}}}}

\begin{quote}
Method for identifying near duplicate pages in a hyperlinked database
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{etc}{Further Reading}
\begin{itemize}
\item
Wired: CMGI Claims Patently Wrong\footnote{\url{http://www.wired.com/news/technology/0,1282,41508,00.html?tw=wn20010131}}

\item
Businesswire: archie creator willing to challenge CMGI claims\footnote{\url{http://www.businesswire.com/webbox/bw.012901/210290071.htm}}
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

