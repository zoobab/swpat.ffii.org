<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Im Januar 2001 erklärte der Chef der Firma, die Altavista gekauft hat, Altavista besitze ca 50 Patente auf grundlegende Prinzipien, ohne die zu verletzen niemand das WWW indexieren könne, und kündigte an, in den kommenden Monaten Firmen, die das Internet oder auch ihr Intranet indexieren, zur Kasse und notfalls vor Gericht bitten zu wollen, um den maximalen Gewinn aus diesen Patenten herauszuholen.
title: Keine WWW-Indexierung mehr ohne Erlaubnis von CMGI?
Dtc: Der Startschuss
EWa: Einige der Patente
Fea: Weitere Lektüre
ErA: Am Tage 2000-11-13 gab der Konzern %{CGMI}, der Altavista und 12 weitere Firmen besitzt, in einer stark beworbenen %(pr:Pressemitteilung) bekannt, dass er 38 Patente auf wesentliche Elemente der Netzindexierung besitzt.  Zu diesem Zeitpunkt schrieb CGMI rote Zahlen und versuchte, bei Investoren um Zuversicht zu werben.  Am Tage 2000-01-15 erklärte der Chef von CGMI in einem %(iw:Interview) darüber hinaus, dass er die Verletzer seiner Patente in Kürze zu verfolgen gedenke und dass praktisch jeder, der das Internet oder auch nur sein firmeninternes Intranet absuche und indexiere, zu diesen Verletztern gehöre:
CWb: Can we talk a bit about some of the ideas or opportunities that you backed off of because money was an issue this past year?
EWn: Even though AltaVista s doing well in the advertising space, we just think that in order to really ensure strong growth they ought to leverage their position in search licensing to a greater extent. And we saw the opportunity to do that because we think it s a big market. They happen to own 38 patents, many of which we think are fundamental in the search area. They were the first to spider and index the Web. And Digital did a good job of recognizing the potential value of that intellectual property. And they were very thorough in filing broad and deep and narrow patents. And we have another 30 patents that are in application. So we believe that virtually everyone out there who indexes the Web is in violation of at least several of those key patents.
Dep: Does that mean you ll pursue that?
Yis: Yes, we will. Coming up in the first quarter of 2001.
Ssa: So we may see some lawsuits ...
Ile: If necessary, we will defend it, to the letter of the law.
Aih: Are there any specific examples of the types of patents?
Ihs: If you index a distributed set of databases what the Internet is and even within intranets, corporations, that s one of the patents.  We did a %(pr:press release) on this with a list of six or ten of the key areas that the patents cover.
DWW: Die meisten europäischen Anträge warten beim EPA noch auf ihre Erteilung.
cna: Kombination aus 5 US-Anmeldungen

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swxai-altavista ;
# txtlang: de ;
# End: ;

