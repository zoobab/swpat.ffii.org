<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Hässliche TrueType- und Opentype-Schrifwiedergabe dank Patenten

#descr: Schriften werden auf freien Systemen wie XFree86 langsam und
unansehnlich wiedergegeben.  Grund hierfür sind Patente.  Als Apple
und Microsoft sich auf das Schrifterzeugungsformat TrueType einigten,
hielten sie noch einige Patent in den Händen, die sie bisher nie
durchzusetzen versucht haben.  Dennoch behalten sie sich eine
Durchsetzung jederzeit vor.  Auf Anforderung beunruhigter Großkunden
stellen daher Distributoren wie SuSE und Redhat Funktionalitäten wie
Entzerrung (anti-aliasing) aus.  TrueType ist der dominierende
Font-Standard und ist auch in OpenType enthalten.  Auf letzteren
Standard einigte sich Microsoft mit Adobe, und auch hier sorgen
Adobe-Patente für Unsicherheit.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-ttf ;
# txtlang: de ;
# multlin: t ;
# End: ;

