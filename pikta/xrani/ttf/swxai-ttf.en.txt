<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Rendering of Fonts is ugly and slow on Free Software Systems.  This is because when the TrueType standard was promoted by Apple and Microsoft, they held a few patents which they never asserted.  The FreeType project has asked Apple to clarify the situation, but did not get an answer.  Instead, fearful customers of Linux distributors such as SuSE and Redhat have demanded that any possibly infringing FreeType features be disabled on these distributions.  TrueType is the dominating font standard and it is also a part of new standards such as OpenType, in which Adobe participates.  Adobe also holds a few patents on which OpenType infringes.  These formats must be supported if GNU/Linux/XFree users are to be able to use existing fonts on their platform of choice.
title: Ugly TrueType and OpenType Font Display thanks to Patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swxai-ttf ;
# txtlang: en ;
# End: ;

