<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Amazon (librairie en ligne) a reçu un brevet américain sur la réduction de la nécessité de saisir des données lors de commandes répétées sur un réseau tel le WWW.  Fort de ce brevet, Amazon a déposé une requête en injonction contre une librairie en ligne concurrente. En septembre 1998, Amazon avait déposé une demande de brevet similaire auprès de l'EPO sous le numéro  EP0902381 sous le titre %(q:Méthode et système de placement d'un ordre d'achat via un réseau de communications). Lors d'un rapport d'enquête publié par l'EPO, ce brevet avait déjà déclenché une tempête de protestations aux USA. Ceci avait conduit à la découverte d'antériorité incluant des brevets similaires qu'Amazon aurait pu contrefaire. L'EPO avait conclu que la méthode d'Amazon était en principe brevetable, mais avait publié la liste des antériorités dans un rapport d'enquête en 2001. Amazon avait alors décidé de scinder le brevet en deux. L'un de ceux-ci, dénommé Méthode simplifiée de commandes d'articles via Internet, a été accordé par l'EPO en mai 2003 sous le numéro EP0927945. L'autre est toujours en suspens.
title: Achat 1-Clic: 
cea: Article instructif au sujet de la controverse 2000 sur la brevetabilité des logiciels et les récents cas qui y ont contribué.
ael: L'auteur de l'article s'inquiète du fait que le cas Amazon pourrait déclencher une discussion sur la réforme du système de brevets tendant à réduire le nombre d'années pendant lequel un brevet couvrirait un logiciel ou une méthode commerciale, telle 1-Clic ; en effet, le système judiciaire américain a déjà accordé un grand nombre de brevets dont la période de validité n'est que de 20 ans. Jeff Bezos d'Amazon a proposé de faire une distinction entre les différents types de brevets, mais l'auteur de l'article rétorque qu'Internet ne doit pas constitué une exception et que toutes les décisions doivent être prises par une cour de justice et non par le législateur. Cet article a été repris par certains juristes spécialisés en matière de brevet pour étayer leur argumentation.
ecn: Documents sur le brevet 1-Clic d'Amazon (un parmi d'autres cas célèbres) rassemblés par un groupe d'étudiants en informatique à l'université de Stanford.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@a2e.de ;
# login: mgaroche ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swxai-1click ;
# txtlang: fr ;
# End: ;

