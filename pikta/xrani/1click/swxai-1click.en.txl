<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: One-Click Shopping

#descr: Amazon (internet bookstore) received a US patent on reducing the need
for data input in case of repeated ordering through a network like the
WWW.  Based on this patent, Amazon sought an injunction against a
competing bookstore.  Amazon had applied for the same patent at the
EPO under EP0902381 in Sep. 1998 under the name %(q:Method and system
for placing a purchase order via a communications network).  By the
time a search report was issued by the EPO, this patent had already
aroused an uproar in the USA, leading to the discovery of new prior
art, including similar patents which Amazon might be infringing.  The
EPO found the Amazon method patentable in principle, but listed new
prior art in an examination report of 2001.  Amazon decided to split
the patent into two new applications.  Of these, one, EP0927945, a
method for simplified ordering of articles via Internet, was granted
by the EPO in May 2003.  The other is still pending.

#cea: Informative article about the software patent controversy of 2000 and
how various recent cases contributed to it.

#ael: The writer is afraid that the Amazon case could spark off a discussion
about reforming the patent system by reducing the number of years for
software and business method patents such as 1click, which the
american judicial system is granting in great numbers with a term of
20 years.  Jeff Bezos of Amazon has proposed to differentiate between
different types of patents, but the writer argues that there should be
no special exception for the internet and all decisions must be taken
by lawcourts, not by the legislator.  This article has been quoted by
some german patent lawyers in support of their views.

#ecn: Materials about the Amazon One Click Patent (one of several famous
cases) collected by a group of computer science students from
Stanford.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-1click ;
# txtlang: en ;
# multlin: t ;
# End: ;

