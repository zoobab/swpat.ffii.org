<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: OpenMarket asserts Network Sales System monopoly against Intershop

#descr: On 2001-01-09, Open Market attacked Intershop, the largest
Germany-based shopping application company, for violating its patents
on a network sales system on the US market.  Meanwhile some of the
concerned OpenMarket have been granted by the EPO as well.

#NWo: News Reports

#Asr: Abstract

#poa: prior art

#SiW: Situation in Europe

#Ort: OpenMarket statement

#Ior: In particular, the process described in the invention includes
client-server sessions over the Internet involving hypertext files. In
the hypertext environment, a client views a document transmitted by a
content server with a standard program known as the browser. Each
hypertext document or page contains links to other hypertext pages
which the user may select to traverse. When the user selects a link
that is directed to an access-controlled file, the server subjects the
request to a secondary server which determines whether the client has
an authorization or valid account. Upon such verification, the user is
provided with a session identification which allows the user to access
to the requested file as well as any other files within the present
protection domain.

#wna: would be cookie convention and encrypted data exchange protocols

#Tet: Two of the patents also have also been applied for at the European
Patent office:

#Tpe: This attack was launched against Intershop after it had to restructure
its US marketing strategy after huge losses in the US.

#cnl: %(GA) writes on this in the patents mailing list on 2001/01/12:

#lee: With regards to Open Market, they have had their patents for a few
years, and really haven't done much with them.  I suspect they have
approached a few companies, and then cut cheap licensing deals when
these companies responded back with a fair amount of prior art that
could be used to invalidate the patents.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-openmint ;
# txtlang: en ;
# multlin: t ;
# End: ;

