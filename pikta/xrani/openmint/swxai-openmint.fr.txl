<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: OpenMarket accuse Intershop de contrefaçon de son Système de Vente en
Réseau

#descr: Le 9 janvier 2001, Open Market attaque en justice Intershop, la plus
grosse société d'applications de ventes en ligne basée en Allemagne,
pour avoir violé ses brevets sur un système de ventes en réseau sur le
marché américain. Concurremment, OpenMarket a obtenu l'accord de
l'Office Européen des Brevets sur quelques uns des mêmes brevets.

#NWo: Bulletins d'informations

#Asr: Résumé

#poa: Antériorité

#SiW: Situation en Europe

#Ort: Déclaration d'OpenMarket

#Ior: Le processus décrit dans l'invention inclut, notamment, des sessions
clients-serveurs sur Internet mettant en jeu des fichiers hypertextes.
Dans l'environnement hypertexte, un client voit un document transmis
par un serveur de contenu à l'aide d'un programme standard connu sous
le nom de navigateur. Chaque document ou page hypertexte contient des
liens vers d'autres pages hypertextes que l'utilisateur peut
sélectionner pour les atteindre. Quand un utilisateur sélectionne un
lien assujetti à un fichier de contrôle d'accès, le serveur soumet la
requête à un serveur secondaire qui détermine si le client possède
l'autorisation adéquate ou un compte valide. À l'issue de cette
vérification, l'utilisateur reçoit un identifiant de session qui lui
permet d'accéder au fichier demandé ainsi qu'à tout autre fichier dans
le même domaine de protection.

#wna: probablement usage de cookie et protocoles d'échanges de données
encryptées

#Tet: Deux de ces brevets ont aussi été déposés auprès de l'Office Européen
des Brevets :

#Tpe: Cette attaque contre Intershop a été lancée après un changement de
stratégie commerciale de celle-ci consécutif à de lourdes pertes sur
le marché américain.

#cnl: %(GA) écrit, à ce propos, 2001/01/12, sur la liste de diffusion
patents:

#lee: En ce qui concerne OpenMarket, cela fait déjà quelques années que la
société possède ces brevets et qu'elle n'en a pas tiré grand partie.
Je la soupçonne d'avoir contacté quelques sociétés pour leur proposer
une transaction sur la base de droits de licence réduits et d'avoir
coupé court à ces tractations lorsque ces sociétés ont opposé un bon
nombre d'antériorités qui auraient pu invalider les brevets.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: mgaroche ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-openmint ;
# txtlang: fr ;
# multlin: t ;
# End: ;

