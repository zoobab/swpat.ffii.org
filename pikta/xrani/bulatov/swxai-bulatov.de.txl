<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Xerox ./ Bulatov: Baumvisualisierungspatent behindert
Optimierungsforschung

#descr: Ein Patent der Firma Xerox hat den Informatiker Prof. Bulatov dazu
veranlasst, sein freies und quelloffenes Programm HyperProf aus dem
Netz zurückzuziehen.  HyperProf war als Hilfsmittel zur Analyse von
Optimierungspotentialen von Software geschätzt und vielfach
unabkömmlich.  Dieses Programm ist Teil der Forschung und dient wieder
der Forschung.  Selbst wenn Xerox ähnliches bieten würde, wäre das
kein Ersatz für die verlorene Entwicklungsfreiheit.  Uns ist bisher
nicht bekannt, um welches Patent es sich handelt und ob es auch am EPA
angemeldet ist.

#Eoo: Explanations by Prof. Bulatov

#Atr: Hier ist die graphische Darstellung zu sehen, die offensichtlich
patentiert ist.

#HWn: hints from %{XB}

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-bulatov ;
# txtlang: de ;
# multlin: t ;
# End: ;

