<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Internet-Telephonie: No Voice over IP

#descr: Führende Fachleute halten es für unmöglich, in den nächsten 15 Jahren
freie oder unabhängige Software für die Internet-Telephonie zu
schaffen.  Zu viele grundlegende Rechenregeln sind patentiert.  Diese
Regeln sind nicht nur schwer bis unmöglich zu umgehen, sie sind auch
Teil von Standards, die ein IP-Telefonie-System einhalten muss, um zu
marktdominierenden Systemen kompatibel zu sein.  So können die
Telefongesellschaften die bisherige proprietäre Struktur dieses
Bereiches weiter aufrecht erhalten.  Dennoch gibt es Versuche,
patentfreie Alternativen zu etablieren.

#Bgk: IP-Telefonie: eine bröckelnde proprietäre Festung

#JWW: Jean-Marc Valin writes %(q:We would like to announce the first release
of the Speex project. Speex is an open-source (LGPL), patent-free
compression format allowing an alternative to expensive proprietary
codecs. Unlike Ogg Vorbis which compresses general audio, Speex is
designed especially for speech. For that reason, Speex is meant to be
a complement to Vorbis. Since it is specialized for voice
communications, it is possible to attain lower (compared to Ogg
Vorbis/MP3) bit-rates in the 8-32 kbps/channel range. Possible
applications include Voice over IP (VoIP) applications, Internet audio
streaming at low bit-rate and archiving of speech data (e.g. voice
mail).

#Trc: This first version of Speex supports fixed bit-rate encoding at 14.5
kbps for speech sampled at 8 kHz (narrowband) and at 28.5 kbps for 16
kHz (wideband) speech. Future releases will likely provide a wider
choice of bit-rates, better quality, as well as variable bit-rate
(VBR) and discontinuous transmission (DTX).

#DWe: Die Sache ist vor allem deshalb interessant, weil der boomende Sektor
IT-Telephonie und Voice over IP in proprietäre Strukturen einbricht. 
Telefonanlagen etwa sind hochgradig herstellerabhängig. Gerade in
Zeiten stärkerer Bandbreite ein interessantes Projekt. Und leider ist
Internettelefonie nicht gerade eine starke Plattform für offene
Standards.

#NWi: Nun wird das Ganze als %(q:patentfrei) beworben. Wie die Macher sich
das rechtlich vorstellen weiss ich nicht... wahrscheinlich denken Sie,
weil wir kein Patent anmelden sind Patente kein Problem für uns...

#Ptr: %(HS), Hauptentwickler diverser IETF-Entwürfe und
%(ri:Referenzimplementierungen zu IP-Multicast und Voice Over Internet
Protocol (VoIP)) erklärt, dass das Feld der Internet-Telefonie so von
patentierten Standards und Grundlagenpatenten verbaut sei, so dass man
wohl 17 Jahre warten müsse, bevor wir über das Internet frei
miteinander telefonieren können.

#Asa: A GNU project which is concerned in many ways with Voice over IP

#Peg: Patentgefährlich dürfte besonders der Markt der Soft-PBX werden, die
an die Stelle der traditionellen Hardware-PBX (Grosstelefonanlagen)
treten. Jedes größere Unternehmen dürfte über seine eigene
Telefonanlage verfügen. Der Traum der IP-Telefonie ist, dass man sein
Telefon ins Netzwerk stöpselt, so dass PC und Telefon über die
gleichen Netze gehen. Problem bei der Sache: Für Telefonanlagen wird
Hochverfügbarkeit verlangt, traditionelle Telefonanlageninstallateure
haben kaum Ahnung von Computer-Netzwerktechnik und Computer-Netze
gelten gemeinhin als sicherheitsempfindlicher. Unixartige
Betriebssysteme geniessen hier in Sachen Stabilität und Sicherheit
einen besseren Ruf, so dass VoIP vermutlich sehr heiss für Freie
Software werden kann. Da hat man dann halt auch noch seinen SOFT-PBX
Server...

#Wel: Was windowsbasierte VOIP angeht, so möchte ich kurz erwähnen, dass
bereits meine SB AWE 64 mit NetPhone-SW ausgeliefert wurde. Das hat
sich aber wegen der heute noch geringen Bandbreiten eigentlich nur in
Entwicklungsländern wie z.B. Indien durchgesetzt. Die Qualität
dürfte im Allgeminen wie beim Satellitentelefon sein.  In Ghana ist
VoIP verboten.

#Iva: In größeren Unternhehmen: VoIP erlaubt es Computernetzwerke (etwa
gebuchte Standleitungen) parallel für internen Telefonverkehr zu
nutzen. Das bedeutet für viele Unternehmen Kostenersparnisse. Die
Geräte sind recht teuer, ab 2000 Euro ist man dabei. Hinzu kommt der
Service. Alles zur Zeit noch eine boomende Nische. Europäische
Mini-Firmen domieren den Markt.

#Iut: Insgesamt bedeutet VoIP für traditionelle Telefonanlagenhersteller
eine grosse Herausforderung, die Hersteller präsentieren zwar
Lösungen, aber im Markt sind vor allem die Zwerge sehr innovativ und
wachsend. Die Branchenriesen hinken eher hinterher, als dass sie die
richtugn bestimmen. Insbesondere für Hard-PBX - Distributeure eine
große Herausforderung, denn Telefonanlagen sind proprietär mit allem
was dazu gehört. Systemtelefone von Siemens kann man nicht an einer
Panasonic-PBX benutzen. Auch das Zusammenstöpseln unterschiedlicher
Produkte ist problematisch. Umgekehrt ist man aber den technischen
Begrenzungen des jeweiligen proprietären Systems unterworfen. Offene
Standards nutzen vor allem den Kleinen, die keine Lösung aus einem
Guss anbieten können. So bedeutet VoIP einen Umbruch im Markt,
vergleichbar mit dem Aussterben der Schreibmaschine... Offene
Standards sind für die Branchenriesen also eine reale Bedrohung, sie
haben den Markt aber nicht mehr im Griff.

#CWy: Contact the Speex people, ask them whether they have really checked
the patent situation

#CWW: Contact Prof. Schulzrinne, ask his view

#Lat: List the relevant patents, particular those granted by EPO

#Loc: List some of the concerned SMEs

#Msb: Most of these seem to be european

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-voip ;
# txtlang: de ;
# multlin: t ;
# End: ;

