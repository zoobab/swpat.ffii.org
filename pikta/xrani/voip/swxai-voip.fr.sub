\begin{subdocument}{swxai-voip}{Internet Telephony: no voice over IP}{http://swpat.ffii.org/pikta/xrani/voip/index.fr.html}{Groupe de travail\\\url{swpatag@ffii.org}\\version fran\c{c}aise 2005/01/06 par Gerald SEDRATI-DINET\footnote{\url{http://gibuskro.lautre.net/}}}{Leading experts consider it impossible to create free or independent software for Internet Protocol (IP) Telephony during the next 15 years.  The reason is ``intellectual property'' (IP):  the field is cluttered with compression patents which not only cover almost all possible compression algorithms but also standards with which any implementor must comply.  This way the telephone companies can stifle innovation and keep their monopolistic grip on the field of telephony.  Nevertheless people are trying to establish patent-free alternatives.}
\begin{sect}{links}{Liens annot\'{e}s}
\begin{itemize}
\item
{\bf {\bf 2002-03-28: Speex Compression Format\footnote{\url{http://newsvac.newsforge.com/article.pl?sid=02/03/28/083216}}}}

\begin{quote}
Jean-Marc Valin writes ``We would like to announce the first release of the Speex project. Speex is an open-source (LGPL'', patent-free compression format allowing an alternative to expensive proprietary codecs. Unlike Ogg Vorbis which compresses general audio, Speex is designed especially for speech. For that reason, Speex is meant to be a complement to Vorbis. Since it is specialized for voice communications, it is possible to attain lower (compared to Ogg Vorbis/MP3) bit-rates in the 8-32 kbps/channel range. Possible applications include Voice over IP (VoIP) applications, Internet audio streaming at low bit-rate and archiving of speech data (e.g. voice mail).

This first version of Speex supports fixed bit-rate encoding at 14.5 kbps for speech sampled at 8 kHz (narrowband) and at 28.5 kbps for 16 kHz (wideband) speech. Future releases will likely provide a wider choice of bit-rates, better quality, as well as variable bit-rate (VBR) and discontinuous transmission (DTX).

Die Sache ist vor allem deshalb interessant, weil der boomende Sektor IT-Telephonie und Voice over IP in propriet\"{a}re Strukturen einbricht.  Telefonanlagen etwa sind hochgradig herstellerabh\"{a}ngig. Gerade in Zeiten st\"{a}rkerer Bandbreite ein interessantes Projekt. Und leider ist Internettelefonie nicht gerade eine starke Plattform f\"{u}r offene Standards.

Nun wird das Ganze als ``patentfrei'' beworben. Wie die Macher sich das rechtlich vorstellen weiss ich nicht... wahrscheinlich denken Sie, weil wir kein Patent anmelden sind Patente kein Problem f\"{u}r uns...
\end{quote}
\filbreak

\item
{\bf {\bf Schulzrinne 1997-04-30: The Problem with Voice over IP (internet protocol) is that we have no voice over IP (intellectual property)\footnote{\url{http://groups.google.com/groups?oi=djq&selm=an_238525795}}}}

\begin{quote}
Prof. Henning Schulzrinne\footnote{\url{http://www.cs.columbia.edu/\~hgs/}}, chief developper of various IETF Drafts and reference implementations for IP Mulitcast and Voice over IP (RTP,RTSP,SIP\ etc), explains that the field of internet telephony is cluttered by basic patents and patented standards to such a degree that we have to wait for 1-2 decades before we can freely communicate by telephone over the Internet.

\begin{quote}
\texmath{>} Perhaps we ought to rename this group ``Voice over Intellectual Property''!

Except that we don't have a voice over intellectual property...
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf GNU Comm\footnote{\url{http://www.gnu.org/software/gnucomm/overview.html}}}}

\begin{quote}
A GNU project which is concerned in many ways with Voice over IP
\end{quote}
\filbreak

\item
{\bf {\bf \url{http://www.eweek.com/article/0,3658,s\%253D701\%2526a\%253D13915,00.asp}}}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{intro}{IP Telephony -- a crumbling proprietary market}
Patentgef\"{a}hrlich d\"{u}rfte besonders der Markt der Soft-PBX werden, die an die Stelle der traditionellen Hardware-PBX (Grosstelefonanlagen) treten. Jedes gr\"{o}{\ss}ere Unternehmen d\"{u}rfte \"{u}ber seine eigene Telefonanlage verf\"{u}gen. Der Traum der IP-Telefonie ist, dass man sein Telefon ins Netzwerk st\"{o}pselt, so dass PC und Telefon \"{u}ber die gleichen Netze gehen. Problem bei der Sache: F\"{u}r Telefonanlagen wird Hochverf\"{u}gbarkeit verlangt, traditionelle Telefonanlageninstallateure haben kaum Ahnung von Computer-Netzwerktechnik und Computer-Netze gelten gemeinhin als sicherheitsempfindlicher. Unixartige Betriebssysteme geniessen hier in Sachen Stabilit\"{a}t und Sicherheit einen besseren Ruf, so dass VoIP vermutlich sehr heiss f\"{u}r Freie Software werden kann. Da hat man dann halt auch noch seinen SOFT-PBX Server...

Was windowsbasierte VOIP angeht, so m\"{o}chte ich kurz erw\"{a}hnen, dass bereits meine SB AWE 64 mit NetPhone-SW ausgeliefert wurde. Das hat sich aber wegen der heute noch geringen Bandbreiten eigentlich nur in Entwicklungsl\"{a}ndern wie z.B. Indien durchgesetzt. Die Qualit\"{a}t d\"{u}rfte im Allgeminen wie beim Satellitentelefon sein.  In Ghana ist VoIP verboten.

In gr\"{o}{\ss}eren Unternhehmen: VoIP erlaubt es Computernetzwerke (etwa gebuchte Standleitungen) parallel f\"{u}r internen Telefonverkehr zu nutzen. Das bedeutet f\"{u}r viele Unternehmen Kostenersparnisse. Die Ger\"{a}te sind recht teuer, ab 2000 Euro ist man dabei. Hinzu kommt der Service. Alles zur Zeit noch eine boomende Nische. Europ\"{a}ische Mini-Firmen domieren den Markt.

Insgesamt bedeutet VoIP f\"{u}r traditionelle Telefonanlagenhersteller eine grosse Herausforderung, die Hersteller pr\"{a}sentieren zwar L\"{o}sungen, aber im Markt sind vor allem die Zwerge sehr innovativ und wachsend. Die Branchenriesen hinken eher hinterher, als dass sie die richtugn bestimmen. Insbesondere f\"{u}r Hard-PBX - Distributeure eine gro{\ss}e Herausforderung, denn Telefonanlagen sind propriet\"{a}r mit allem was dazu geh\"{o}rt. Systemtelefone von Siemens kann man nicht an einer Panasonic-PBX benutzen. Auch das Zusammenst\"{o}pseln unterschiedlicher Produkte ist problematisch. Umgekehrt ist man aber den technischen Begrenzungen des jeweiligen propriet\"{a}ren Systems unterworfen. Offene Standards nutzen vor allem den Kleinen, die keine L\"{o}sung aus einem Guss anbieten k\"{o}nnen. So bedeutet VoIP einen Umbruch im Markt, vergleichbar mit dem Aussterben der Schreibmaschine... Offene Standards sind f\"{u}r die Branchenriesen also eine reale Bedrohung, sie haben den Markt aber nicht mehr im Griff.
\end{sect}

\begin{sect}{tasks}{Questions, choses \`{a} faire, comment vous pouvez aider}
\begin{itemize}
\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Contact the Speex people, ask them whether they have really checked the patent situation}}
\filbreak

\item
{\bf {\bf Contact Prof. Schulzrinne, ask his view}}
\filbreak

\item
{\bf {\bf List the relevant patents, particular those granted by EPO}}
\filbreak

\item
{\bf {\bf List some of the concerned SMEs}}

\begin{quote}
Most of these seem to be european
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

