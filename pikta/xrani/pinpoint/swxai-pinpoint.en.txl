<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Pinpoint sues Amazon over collaborative filtering

#descr: Collaborative filtering, best known in the form of %(q:People who
liked/bought this also liked/bought that), is widely used in the
e-commerce world.  Pinpoint Incorporated, a patent litigation startup
company whose activity has so far been confined to the US, holds
several US patents on the use of collaborative filtering for different
purposes. On July 18th 2003, Pinpoint filed a lawsuit against
Amazon.com, alleging violation of several of its patents.

#rrW: A short news.com article on the lawsuit.

#oet: An owner of similar patents on collaborative filtering (used for a
dating line) obtained during the 1980s analyses Pinpoint's claims and
concludes that his patents are almost certainly prior art.  Lists the
patents which Amazon violates according to Pinpoint:

#ach: Provides a short analyses of each at the %(eo:end of his article).

#iTW: The company's website.  Not to be confused with pinpoint.com. The
latter also own a lot of patents but are less focussed on patents.

#oin: Pinpoint's own overview of its US patent portfolio. Most of its
patents on applications of collaborative filtering (television
broadcast selection, identify desirable objects, deliver information
and advertisments), but there are a few others as well, such as one on
%(lz:Lempel-Ziv data compression).  As of 2003/08/15, the Pinpoint
website contained no other information than these links to patents.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-pinpoint ;
# txtlang: en ;
# multlin: t ;
# End: ;

