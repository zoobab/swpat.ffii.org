<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patents de Programari en Acció

#descr: Aquests últims anys, més i més casos de litigi de patents de
programari es coneixen pels mitjans de comunicació.  Però això és
només la punta de l'iceberg.  La major part de les empreses de
programari i els desenvolupadors de programari són amenaçats fora dels
jutjats i el silenci és part obligatòria d'aquest %(e:arranjament fora
del jutjat).  Molts projectes s'aturen o no comencen perquè el camp
està abarrotat de patents.  És difícil documentar camins de
desenvolupament bloquejats.  Aquí intentarem fer el millor que puguem.

#Sln: Some Well Documented Cases

#allvoicet: From AllVoice to AllPatent: Milking the Speech Recognition Business
with Parliamentary Support

#allvoiced: Allvoice Computing PLC, originally a text-processor service company
based in Devon, UK, has obtained two broad and trivial patents in US
and UK on the logics of interfacing between speech recognition and
word processing.  Allvoice tried to sell this interface as a
standalone software product, but was apparently more successful in
extracting rents from producers of full-fledged speech recognition
software, such as IBM and Lernout & Hauspie, by means of patent
litigation.  Meanwhile Allvoice's business seems to be focussing on
patent enforcement.  Allvoice's director John Mitchell has also become
a patent-political activist and an archetype of a business model which
british parliamentarians are promoting in UK and EU.

#simulinkt: National Instruments ./ The Mathworks

#simulinkd: After two years of litigation National Instruments obtained a court
decision which finds Simulink, the world's leading math software, to
have fallen afoul of three patents owned by National Instruments. 
National Instruments has attacked Mathworks in other courts with 6
more charges of patent infringment.  Mathworks has appealed.  Some of
the NI patents have also been granted by the European Patent Office
against the letter and spirit of the written law.

#Lcv: Malauradament, encara estem observant una pila de casos pobrement
documentats o no-publicables, com ara

#IWe: Si coneixes algun cas, sisplau ?(pi:informa'ns)!

#DnW: Una empresa alemana ofereix implementació d'extensions MS IIS en un
servidor basat en la plataforma Linux després que el seu departament
legal va fer una llista de patents de més de 20 M$ en aquesta àrea

#Fft: Una empresa líder de programari de comunicacions alemana va ser
atacada per una multinacional bavaresa amb patents de programari
frívoles.

#Ppf: Planned GNU e-cash system stalled for years, because basic
cryptographic methods are patented and committments to allow GPL
implementations could not be obtained.  The difficulties in
establishing trust in e-money and e-commerce are aggravated by
patents.  Some basic e-payment procedures are patented, see
Microsoft's SET patent.

#MWW: La major part de patents que van causar la crisi al W3C són trivials i
amples i han estat concedides (il·legalment) per l'EPO.

#Hlf: Frabricants d'impressores, escàners i d'altres aparells perifèrics 
per a ordinadors sovint no fan especificacions de controladors o codi
font disponible, perquè tenen por que això pugui dur a descobrir
infringiments de patents. Especialment, alguns grans fabricants amb
una política de suport a sistemes oberts o plataformes de font oberta
encara tenen una política d'oferir només controladors MSWin de codi
tancat.

#Iar: IBM patent lawyer %(FT) has repeatedly declared in public declared the
IBM would not hesitate to sue Open Source projects that %(q:free-ride)
on IBM %(q:technology), thus forcing them to close down and become
proprietary.  Microsoft speakers have %(ms:said) the same.

#Wfs: Encara no tenim documentats alguns dels famosos casos judicials de
mètodes de patents dels negocis als USA, p.e.

#MfU: Votació per Web de Microsoft

#bna: arxivat

#eti: concedit

#Tit: titolat

#Oua: Llista Oppenheimar de casos actuals pendents de patents sobre Internet

#spW: una revisió molt entenedora, però errors deJavascript poden fer que el
teu navegador es pengi

#nrb: anomena diversos casos de principis dels 90 on diverses empreses van
ser prejudicades per patents de programari

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpikxrani ;
# txtlang: ca ;
# multlin: t ;
# End: ;

