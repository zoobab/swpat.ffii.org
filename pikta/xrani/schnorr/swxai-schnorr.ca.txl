<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: RSA & Schnorr stifle cryptography-based e-commerce

#descr: After widespread use of cryptography had been impeded for years by
patents like that on RSA, finally an alternative was found that seemed
to be available for free use by the public. But just at that moment,
Professor Schnorr from Germany asserted that this free cryptography
scheme infringed on his recently obtained crypto patent. The licensing
rights of RSA and the Schnorr patent were later exclusively acquired
by PKP. PKP harrassed crypto programmers by claiming that %(q:These
patents cover all known methods of practicing the art of Public Key,
including the variations collectively known as El Gamal).

#nam: name

#Utn: US patent number

#clrsad: Patent lawyers analyse the weaknesses of the RSA patent.  On the one
hand side, it can be attacked as non-statutory, because the only way
it goes beyond a pure algorithm is by claiming standard computer
implementations of this algorithm, which is, at least according to the
law, not permissible.  On the other hand side, the RSA algorithm is
very similar to a previously known formula.  The authors find that the
patent has done disproportionate harm, partially because its strength
was over-estimated.

#Grh: Google Search for Schnorr Patent

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-schnorr ;
# txtlang: ca ;
# multlin: t ;
# End: ;

