<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: RSA & Schnorr behindern Kryptographie und schnüren E-Geschäftsverkehr
ein

#descr: Der Bereich der Kryptographie hat seine Besonderheiten. Einen
Krypto-Algorithmus zu finden erfordert relativ viel Gehirnschmalz und
die durch Patentierung der wesentlichen Schritte zu erreichende
Blockierwirkung ist sehr hoch. So war es besonders beim RSA-Patent. In
den 90er Jahren wurden schließlich unblockierte Alternativen gefunden
und z.T. von der US-Regierung frei gekauft. Aber ein Patent des
Frankfurter Professors Claus Schnorr machte dem wieder einen Strich
durch die Rechnung. Die Patentverwertungsfirma PKP, die sich dieses
Patent aneignete, behauptete, dass das gesamte Gebiet der
Kryptographie von ihrem Patentportfolio abgedeckt sei und auch
Algorithmen wie ElGamal und DSA nicht mehr frei verwendet werden
dürften. Erst nach einigen Prozessen und nach Ablauf einiger Patente
entstand schließlich etwas mehr Freiraum für die Verbreitung der
Kryptograhpie in Systemen wie GnuPG.

#nam: name

#Utn: US patent number

#clrsad: Patent lawyers analyse the weaknesses of the RSA patent.  On the one
hand side, it can be attacked as non-statutory, because the only way
it goes beyond a pure algorithm is by claiming standard computer
implementations of this algorithm, which is, at least according to the
law, not permissible.  On the other hand side, the RSA algorithm is
very similar to a previously known formula.  The authors find that the
patent has done disproportionate harm, partially because its strength
was over-estimated.

#Grh: Google Search for Schnorr Patent

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-schnorr ;
# txtlang: de ;
# multlin: t ;
# End: ;

