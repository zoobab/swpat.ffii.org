<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Brevets Logiciels en Action

#descr: Ces dernières années, de plus en plus de cas de contentieux de brevets
ont été médiatisés. Mais ce n'est que la partie émergée de l'iceberg.
La plupart des SSII et des développeurs de logiciels sont menacés hors
cours de justice et le silence fait partie intégrante de ces accords
hors cours. De nombreux projets sont arrêtés ou ne sont pas démarrés
car leur champ est pavé de brevets. Il est difficile de documenter les
voies de développement bloquées.  Nous essayerons de le faire ici.

#Sln: Quelques Cas bien documentés

#allvoicet: From AllVoice to AllPatent: Milking the Speech Recognition Business
with Parliamentary Support

#allvoiced: Allvoice Computing PLC, originally a text-processor service company
based in Devon, UK, has obtained two broad and trivial patents in US
and UK on the logics of interfacing between speech recognition and
word processing.  Allvoice tried to sell this interface as a
standalone software product, but was apparently more successful in
extracting rents from producers of full-fledged speech recognition
software, such as IBM and Lernout & Hauspie, by means of patent
litigation.  Meanwhile Allvoice's business seems to be focussing on
patent enforcement.  Allvoice's director John Mitchell has also become
a patent-political activist and an archetype of a business model which
british parliamentarians are promoting in UK and EU.

#simulinkt: National Instruments ./ The Mathworks

#simulinkd: Après deux années de litige, National Instruments a obtenu une
décision du tribunal constatant que Simulink, logiciel de
mathématiques de premier plan, contrefaisait trois brevets détenus par
National Instruments. National Instruments a porté six autres plaintes
pour contrefaçon de brevets logiciels contre Mathworks devant d'autres
cours de justice. Mathworks a fait appel. Certains des brevets de
National Instruments ont aussi été accordés par l'Office Européen des
Brevets contre la lettre et l'esprit du droit écrit.

#Lcv: Malheureusement nous connaissons encore une multitude d'affaires mal
documentées ou non publiables.

#IWe: Si vous êtes au fait de certaines affaires, merci de %(pi:nous en
faire part) !

#DnW: Une entreprise allemande abandonne l'implémentation d'extensions MS
IIS sur une plate-forme serveur LINUX après que son service juridique
a recensé 20 brevets Microsoft dans ce domaine.

#Fft: Un éditeur de logiciels de communication allemand de premier plan est
attaqué par une multinationale bavaroise avec des brevets triviaux.

#Ppf: Le système de paiement électronique GNU a stagné pendant des années,
parce que des méthodes cryptographiques de base sont brevetées et
qu'il était impossible d'obtenir des astreintes pour permettre une
implémentation GPL. Les difficultés rencontrées pour établir la
confiance dans la monnaie et le commerce électroniques sont aggravées
par les brevets. Certaines procédures basiques de paiement
électroniques sont brevetées, voir le brevet SET de Microsoft.

#MWW: La plupart des brevets qui ont déclenché la crise du W3C relèvent de
simples règles de calcul et ont aussi été accordés (illégalement) par
l'OEB.

#Hlf: Les constructeurs d'imprimantes, scanners et autres périphériques
révèlent rarement les spécifications de leurs pilotes ou le code
source car ils craignent que cela n'aboutisse à la découverte de
contrefaçon de brevets. Quelques gros constructeurs supportant des
systèmes ouverts ou des plate-formes open source livrent, néanmoins,
des pilotes MS-WIN sous forme de source fermé. L'une des raisons de
cette pratique est la politique de plate-forme agressive de Microsoft.
Microsoft a toujours exercé des pressions sur les constructeurs de
matériel pour qu'ils ne supportent que son système opératoire et a
récemment %(gl:acheté des brevets sur graphiques 3D) vraisemblablement
dans ce but.

#Iar: %(FT), juriste IBM spécialisé en matière de brevets, a déclaré, à
maintes reprises, en public qu'IBM n'hésiterait pas à poursuivre en
justice les projets Open Source qui %(q:transféreraient gratuitement)
la %(q:technologie) IBM, les forçant ainsi à un arrêt définitf et à
devenir projets propriétaires. Les porte-paroles de Microsoft ont
%(ms:dit) la même chose.

#Wfs: Nous n'avons pas encore documenté certaines affaires judiciaires
célèbres concernant des brevets logiciels américains de méthode
commerciale :

#MfU: Enquête Web Microsoft

#bna: dépôt de la requête

#eti: octroi du brevet

#Tit: titre

#Oua: Liste Oppenheimer des affaires en cours concernant des brevets
Internet

#spW: très détaillée, mais avec erreurs JavaScript qui peuvent faire planter
votre navigateur

#nrb: cite différentes affaires du début des années 90 dans lesquelles des
sociétés ont été ruinées par des brevets logiciels

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: mgaroche ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpikxrani ;
# txtlang: fr ;
# multlin: t ;
# End: ;

