<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Zwiebelrouter: US-Militär patentiert Problemfeld der Anonymen
Kommunikation

#descr: In 2001/07 the US navy received patent US 6266704 for the principle of
arranging public key encryption in several layers so that identities
are concealed.  Colleagues of the patentee say that they are very
surprised at this sudden move of one of their peers who apparently
succeded in obtaining a patent on largely known methods.  The patent
covers large problem fields and leaves little free room for
development of anonymous communications.  Some experimental endeavors
(without the deep pockets of commercial enterprises) are under threat,
with a great potential impact on how people are able to communicate. 
We have yet to find out whether this patent has been filed in Europe.

#TWt: Das Patent

#GWW: German AN.ON project

#AWa: ein betroffenes Projekt, das unmittelbar auf dem Zwiebelrouterprinzip
beruht

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-anon ;
# txtlang: de ;
# multlin: t ;
# End: ;

