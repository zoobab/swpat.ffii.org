<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Brevet British Telecom sur des liens hypertextes

#descr: Dans les années 70-80, British Telecom avait déposé une série de
brevets américains sur le concept de références croisées dans un
hypertexte. En l'an 2000, British Telecom exhuma un de ces trésors
cachés (US 4,873,662) et décida de l'utiliser pour soutirer de
l'argent aux fournisseurs d'accès Internet américains. L'affaire a été
porté devant les tribunaux en février 2002.

#Neh: Ce brevet n'a été déposé qu'aux États-Unis. Si l'on se réfère à la
pratique actuelle de l'OEB, il pourrait aussi être accordé en Europe.
Tout comme avec l'Office Américain des Marques et Brevets, la seule
chose qui pourrait empêcher que cette sorte de brevets soit accordée
par l'OEB est que le principe de nouveauté soit enfreint (il faut
prouver qu'exactement la même chose existait auparavant).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: mgaroche ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-hyperlink ;
# txtlang: fr ;
# multlin: t ;
# End: ;

