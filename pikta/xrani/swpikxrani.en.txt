<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: In recent years, more and more software patent litigation cases are becoming known through the media.  But this is only the tip of the iceberg.  Most software companies and software developpers are being threatened informally outside of courts, and silence is an obligatory part of these %(e:out-of-court settlements).  Many projects are cut back or don't get started because the field is cluttered with patents.  It is difficult to document blocked paths of development.  Here we will try our best.
title: Software Patents in Action
Sln: Some Well Documented Cases
allvoicet: From AllVoice to AllPatent: Milking the Speech Recognition Business with Parliamentary Support
allvoiced: Allvoice Computing PLC, originally a text-processor service company based in Devon, UK, has obtained two broad and trivial patents in US and UK on the logics of interfacing between speech recognition and word processing.  Allvoice tried to sell this interface as a standalone software product, but was apparently more successful in extracting rents from producers of full-fledged speech recognition software, such as IBM and Lernout & Hauspie, by means of patent litigation.  Meanwhile Allvoice's business seems to be focussing on patent enforcement.  Allvoice's director John Mitchell has also become a patent-political activist and an archetype of a business model which british parliamentarians are promoting in UK and EU.
simulinkt: National Instruments ./ The Mathworks
simulinkd: After two years of litigation National Instruments obtained a court decision which finds Simulink, the world's leading math software, to have fallen afoul of three patents owned by National Instruments.  National Instruments has attacked Mathworks in other courts with 6 more charges of patent infringment.  Mathworks has appealed.  Some of the NI patents have also been granted by the European Patent Office against the letter and spirit of the written law.
Lcv: Unfortunately we are still witness to a pile of poorly documented or non-publishable cases, such as
IWe: If you are familiar with any cases, please %(pi:inform us)!
DnW: German company gives up implementation of MS IIS extensions on Linux based server platform after its legal department listed up 20 Microsoft patents in this area
Fft: Leading german communication software company attacked by Bavarian multinational with frivolous software patents.
Ppf: Planned GNU e-cash system stalled for years, because basic cryptographic methods are patented and committments to allow GPL implementations could not be obtained.  The difficulties in establishing trust in e-money and e-commerce are aggravated by patents.  Some basic e-payment procedures are patented, see Microsoft's SET patent.
MWW: Most of the patents that caused the crisis at the W3C are trivial and broad and have also been granted (illegally) by the EPO.
Hlf: Manufacturers of printers, scanners and other peripheral computer devices often do not make driver specifications or source code available, because they are afraid that this many lead to the discovery of patent infringements.  Especially some large manufacturers with a policy of supporting open systems or open source platforms still have a policy of delivering closed-source MSWin drivers only.  One of the reasons for this is the aggressive platform policy of Microsoft.  Microsoft has consistently pressure on hardware producers to support only its operating system and it has recently been %(gl:buying 3D graphics related patents) apparently for this purpose.
Iar: IBM patent lawyer %(FT) has repeatedly declared in public declared the IBM would not hesitate to sue Open Source projects that %(q:free-ride) on IBM %(q:technology), thus forcing them to close down and become proprietary.  Microsoft speakers have %(ms:said) the same.
Wfs: We have not yet documented some of the famous US business method software patent court cases, e.g.
MfU: Microsoft Web Polling
bna: filed
eti: granted
Tit: entitled
Oua: Oppenheimer list of current cases pending about internet patents
spW: very comprehensive overview, but Java script errors may make your browser crash
nrb: names various cases of the early 90s where companies have been harmed by software patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpikxrani ;
# txtlang: en ;
# End: ;

