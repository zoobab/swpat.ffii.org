<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


descr: Allvoice Computing PLC, originally a text-processor service company based in Devon, UK, has obtained two broad and trivial patents in US and UK on the logics of interfacing between speech recognition and word processing.  Allvoice tried to sell this interface as a standalone software product, but was apparently more successful in extracting rents from producers of full-fledged speech recognition software, such as IBM and Lernout & Hauspie, by means of patent litigation.  Meanwhile Allvoice's business seems to be focussing on patent enforcement.  Allvoice's director John Mitchell has also become a patent-political activist and an archetype of a business model which british parliamentarians are promoting in UK and EU.

title: From AllVoice to AllPatent: Milking the Speech Recognition Business with Parliamentary Support

ata: Patent Claims

omn: Comments

osr: Lernout & Hauspie Bankruptcy

ecn: In November 2002, AllVoice attacked the new owner of %(q:Naturally Speaking), ScanSoft, again in a US court.  To date, AllVoice has asserted its patents only in US courts, even when the infringers were European companies such as Lernout & Hauspie (L&H).  During the UK software patent consultation in 2000, Allvoice director John Mitchell %(up:explained) that patent enforcement in UK is difficult due to misunderstandings about lack of patentability of software and other traditional obstacles.

nWr: In public statements, AllVoice has pretended to be a producer of voice recognition software.  They are not.  They are a consulting firm which wrote a small interface managment program and is now using interface patents to extract rents from those who write real voice recognition software.

fat: The director of AllVoice, John Mitchell, has taken an active role in the patent-political process in UK.

foW: While AllVoice was fighting L&H, they got a motion of support for their patents and their fight put down by MPs in the UK parliament.

hsi: Since then Mitchell has appeared on Patent Office panels and discussions when they have wanted an archetypal %(q:little guy who won through).

ola: Mitchell is also involved in a scheme for state subsidized patent litigation which is being promoted by UK patent activists in the EU.

uhW: Allvoice's home town Devon is an unemployment blackspot, and the description of them as a tiny company in speech-recognition in an unemployment blackspot who were saved by their patents and have now licensed their technology is bang on.

tsh: In the context of the discussions about the proposed Software Patent Directive in the European Parliament in 2002/03, the rapporteur Arlene McCarthy MEP (UK Labour), who is closely cooperating with the pro-patent forces in the UK government, has repeatedly referred Allvoice as a model case which demonstrates that %(q:the small patently needs protection).  This was also written into her %(em:report to the European Parliament):

let: Nobody in Europe can have any interest in seeing the destruction of small European software developers. On the contrary, large corporations are often dependent upon the innovativeness of small businesses and patents allow them to turn their creativity to good account, as witness the world-wide non-exclusive licence recently granted to a US multinational by a ten-person company located in an employment blackspot in south-west England in respect of all of their voice-recognition software patents.

ao3: Claim 1 of GB2302199

cWW: Data processing apparatus comprising %(ul|input means for receiving recognition data and corresponding audio data from a speech recognition engine, said recognition data including a string of recognised characters and audio identifiers identifying audio components corresponding to a character component of the recognised characters;|storage means for storing said audio data received from said input means;|processing means for receiving and processing the input recognised characters to replace, insert and/or move characters in the recognised characters and/or to position the recognised characters;|link means for forming link data linking the audio identifiers to the character component positions in the character string even after processing;|display means for displaying the characters being processed by the said processing means;|user operable selection means for selecting characters in the displayed characters for audio playback, where said link data identifies any selected audio components, if present, which are linked to the selected characters; and|audio playback means for playing back the selected audio components in the order of the character component positions in the character string.)

sng: This claim scope was only slightly narrowed during examination and litigation, as can be seen in the %(up:original patent description) (111 pages PDF graphics from espacenet.com database).

ao32: Claim 1 of GB2303955

naa: A data processing apparatus comprising:%(ul|input means for receiving recognition data and corresponding audio data from a speech recognition engine, said recognition data including a string of recognised characters and audio identifiers identifying audio components corresponding to character components of the recognised characters;|link means for forming link data linking the audio identifiers to the character component positions in the character string;|storage means for storing said audio data received from said input means said link data, and said characters; and|display means for displaying the recognised characters; and|an editor work station comprising data reading means for obtaining the characters, link data, and audio data from said data processing apparatus;|editor processing means for processing the characters;|editor link means for linking the audio data to the character component position using the link data;|editor display means for displaying the characters being processed;|editor correction means for selecting and correcting any displayed characters which have been incorrectly recognised;|editor audio playback means for playing back any audio component corresponding to the selected characters to aid correction;|editor speech recognition update means for storing the corrected characters and the audio identifier for the audio component corresponding to the corrected character in a character correction file; and|data transfer means for transferring the character correction file to said data processing apparatus for later updating of models used by said speech recognition engine;|said data processing apparatus including correction file reading means for reading said character correction file to pass the data contained therein to said speech recognition engine for later updating of models used thereby.)

WWr: %(XD) comments on the Allvoice business model:

ora: The Allvoice example illustrates two typical patent myths:

arW: SME can benefit from software patents

aml: In fact to benefit in software patents means to behave as a %(q:patent parasite) that does not get money from the solutions it contributes -WordExpress- but from the obstacles it legally imposes on others to solve useful problems.

Whd: Having software patents in the US and not having them in the EU is a competitive disadvantage for the EU.

oyU: In fact AllVoice has taken advantage of the fact the US market is vulnerable to software patents and the EU not (so much), by attacking US companies from UK. So the situation is a competitive advantage for the EU, at least from an inmoral business point of view.

ahh: After a quick read of the 2 claims below I can hardly see the difference between the 2 patents, one should be prior art for the other, and any is as worthy of monopoly as the notion of proofreading with a karaoke.

Wnn: You can say that implementing these claims may really be a worthwhile task, perfectly covered by copyright on the code, while just publishing the idea of using a multimedia computer for storing written and oral text together to allow human proofreading of automated speech recognition is no contribution at all, and certainly not worth the power to stop anyone from using their multimedia computers in such a way. Everybody knows computers can do this, the meat is getting them to do it (the code, the part copytright covers).

aho: These poor little companies so successful thanks to the patent system are as worthy of praise as the Lazarillo de Tormes (the character, not the anonymous ancient Spanish novel). They are the typical opportunist geniuses that find a great way of making money without any work to earn it (or besides anything else they do worthy of earning money with). And then people start to worship those smart guys and you get a whole society of lazy people trying to emulate them by competing in bribery instead of productivity.  It should be a concern of economic policy to let people have a honest way of living, rather than to degrade the market to mere %(pi:picaresque).

Woo: Lernout & Hauspie went bankrupt during litigation with Allvoice, but this litigation was probably not more than a nail in their coffin.  The following links contain details about the bankruptcy and associated scandals:

aAa: Report about outcome of AllVoice lawsuit against L&H.  Apparently based on Allvoice PR only.

mbi: Quotes collected by Greg Aharonian.  Through newspaper articles in the Independent and the Financial Times, Patrick Nicholls, Tory MP of Devon, spreads the myth of the small man asserting his intellectual property against big business with help of patents. AllVoice is presented as an inventor of some great new %(q:speech recognition technology) who had his product copied by large companies.  %(q:The way US business has rallied round to destroy Allvoice would make a pack of sharks look like a group of nuns,) Nicholls is reported as saying.

fnU: Complains that software patents are difficult to enforce in UK and EU, situation is much better in US, offers advice to UKPO: %(orig|My company has 4 patents that have been implemented by software. We are currently in litigation in the US against US corporations who have chosen quite deliberately to ignore our patents. Our case has been raised in Parliament %(tp|twice|so far) - further debates seem highly likely in the near future. We understand that are situations may shortly be the main theme of a full scale %(q:Panorama) style TV documentary.|I personally have found that the vast majority of lawyers and a large proportion of UK patent attorneys do not understand patenting once software is involved. Some of the debate I have seen so far continues to display this ignorance eg software protection is already covered by copyright.|...|Hardly anyone seems to have practical experience of enforcement ...|Having to pay a patent office to process an application and have it checked for prior art, validity etc taking many months or years is pointless if the Courts are allowed to challenge each and every patent examiners' decisions - irrespective of fact. It becomes even more of a hurdle when high tech is involved where speed is of the essence.|I would be delighted to offer my practical and valuable expertise to the debate. ...)

fto: Cites Mitchell's plans for %(q:Patent Defence Union) as an example of how patents are prompting anti-competitive behavior among companies rather than innovation:%(orig|The inventor John Mitchell, managing director of AllVoice Computing plc, told the audience at 'Intellectual Property Enforcement' about the urgent need to 'cure the loss of intellectual property'. He cited a recent European Commission funded report, in which William Kingston proposed the creation of a 'Patent Defence Union' - 'an EU-wide voluntary grouping of SME patentees to defend their patents'.  This safety-in-numbers mentality is also expressed in the phenomenon of cross-licensing, in which businesses share the licences for their patented inventions with other businesses. Here, again, patents cease to be a spur to innovation, and become a bargaining chip between the cautious.)

ele: Mitchell promotes his project to the IPRTalk forum.  The website also features an article about the Danish Patent Office's %(q:Patent Insurance) scheme, which is a very similar project.

lsW: Mitchell presented his story at a conference in the UK Patent Office on 24th September 2002.  The conference schedule presents him as the archetype of the %(q:real inventor) who owes his success to the patent system: %(orig|John Mitchell is Managing Director of AllVoice Computing plc and an inventor on the companys UK and US patents.  He has international experience in IP enforcement including US patent litigation and a formal complaint to the EC Competition Commission. Having had to explore the various enforcement options open to an SME in different jurisdictions, he instigated the current Private Members Bill that seeks to award additional damages for blatant UK patent infringement. John is committed to establishing a %(tp|Patents Defence Union|PDU) in order to encourage Britains real innovators.)

Wps: mentions how Lernout was assailed by Allvoice after filing bankruptcy, uncritically echoes Allvoice's propaganda that they too make voice recognition software:%(orig|Reuters reports that AllVoice Computing may seek a preliminary injunction to stop L&H from selling a voice recognition software product, NaturallySpeaking, which accounts for $50 million in annual revenue. AllVoice makes a similar software.|A bankruptcy court judge earlier had stayed AllVoice's two patent infringement lawsuits in Massachusetts, one in a U.S. district court and another in the First U.S. Circuit Court of Appeals, Reuters reports.)

frn: Slightly more exact news, although not free from myth that AllVoice is a %(q:speech recognition systems developper): %(orig|ALLVOICE SEEKS TO KEEP THE DRAGON AT BAY|At the end of last week UK speech recognition systems developer AllVoice Computing applied to the US Court of Appeals for the Federal Circuit for preliminary injunctions preventing Dragon Systems from %(q:further releasing, marketing, distributing or offering for sale its latest - and soon to be widely distributed - version 5.0 of Dragon NaturallySpeaking software) containing playback features allegedly protected by an AllVoice US patent.|The patent in question relates to an %(q:Automated Proofreading Using Interface Linking Recognized Words To Their Audio Data While Text Is Being Changed) that was issued to AllVoice in August 1998. In a separate dispute, AllVoice is also pursuing legal action against IBM over alleged infringement of AllVoice intellectual property rights in versions of IBM's speech recognition software.)

tWf: Innovation Exeter, part of a regional development council for Southwestern England, exhibits Allvoice as an example of successful innovation in the Southwest, uncritically reproducing rhetoric from Allvoice.

pgd: reports about the parliamentary bill asking for punitive damages in case of patent infringement, inspired by the Allvoice example.

oan: The standard story: Tiny Devon-based company gains 400,000 pounds in damages from big bully:%(orig|A tiny Devon-based software company has received settlement of a two-year-old patent claim against a former speech technology giant. AllVoice said it finally won its claim against Lernout & Hauspie, once the world's leading speech technology company and now bankrupt, for about £400,000 ($575,959). The British company alleged that the larger rival had stolen some of its technology, which helps computers to recognise human speech, and used it in several products.)

eWI: Reports that the AllVoice-inspired punitive damages bill is meeting resistance from an IP lobby organisation.

cal: ScanSoft Inc reports about its fight against meritless patent claims by AllVoice: %(orig|On November 27, 2002, AllVoice Computing plc filed an action against us in the United States District Court for the Southern District of Texas claiming patent infringement. In the lawsuit, AllVoice alleges that we are infringing %(tp|United States Patent No. 5,799,273 entitled %(q:Automated Proofreading Using Interface Linking Recognized Words to Their Audio Data While Text Is Being Changed)|the %(q:'273 Patent)). The '273 Patent generally discloses techniques for manipulating audio data associated with text generated by a speech recognition engine. Although we have several products in the speech recognition technology field, we believe that these products do not infringe the '273 Patent because our products do not use the claimed techniques. We believe this claim has no merit, and we intend to defend the action vigorously.)

WWe: The Draft Punitive Damages Bill, inspired by AllVoice, version of 2002, retabled after withdrawal.  Actually contains only a statement of intention and an extensive textbook on the blessings of %(q:intellectual property) but not the bill itself, it seems.

teW: Patent Defence Union

sag: John Mitchell, CEO of AllVoice, is also the president of the PDU, a would-be government-funded association for helping %(q:innovative companies) like Allvoice.co.uk to extract patent licenses and punitive damages from infringers of their patents and to avoid having them challenged in court.  AllVoice, as of 2003/08/10, was actively promoting the PDU on its home page.

svW: Explains the history of the company, misleading the naive reader into believing that they are really experts in the development of voice recognition software, gives some interesting details.  AllVoice was created in 1992 as a division of AllTypes, a wordprocessing service firm. %(orig|AllVoice was founded in 1980 by John & Teresa Mitchell as Alltypes, a specialist word processing services firm.  The business developed a strong customer base over the years with personal computers as they replaced dedicated word-processing systems.|As more and more computers were introduced into the workplace, the ways that people interacted with them became increasing important. Realising the potential for voice recognition systems, a specialist speech division, AllVoice, was created in 1992.|Dragon Systems Inc. appointed AllVoice as their %(tp|first UK business partner|and first computer reseller world-wide) and AllVoice developed %(tp|user-oriented software to complement the available technology|AllVoice Assistant & Assistant Plus).  Providing comprehensive installation, training and support services, AllVoice built up an impressive portfolio of customers and the business was renamed AllVoice Computing plc to reflect the increasing importance of speech technology to the company.|When IBM entered the speech recognition market, they approached AllVoice to become their first UK specialist reseller.  Adding IBM's products while retaining independence, AllVoice continued to develop value-added software and services and in 1995 launched %(tp|WordExpress|TM).|%(tp|WordExpress|TM) was widely acclaimed and was awarded a Confederation of British Industry award for innovation.  Some features and functions were granted patents in the %(tp|UK and US|other countries pending).  In December 2001, AllVoice granted a licence for the use of its worldwide patents to Dictaphone and Lernout & Hauspie for its past sales of Dragon NaturallySpeaking.)

avwd: A program developped in 1995, apparently not for sale today, important mainly for its history of being %(q:awarded) prizes and patents: %(orig|%(tp|WordExpress|TM) was developed in 1995 as an interface to sits between a speech recognition engine and a 3rd party application into which text is dictated. At that point, and for some time after, speech recognition products only allowed dictation into their own specific %(q:dictation aware) applications. So when dictated text was required in other applications the only method available was to copy the text from the dictation application into the desired application, a somewhat tiresome chore. WordExpress removed this chore, and others, by allowing dictation directly into the chosen application.|%(tp|WordExpress|TM) has received much praise over the years including an innovation award from the %(tp|Confederation of British Industry|CBI) acknowledging AllVoice's efforts.|Certain features and functions of AllVoice software developments have now been awarded patents and others are pending in many countries.)  The latter sentence suggests that Allvoice has indeed been extracting money with help of its two patents and is looking forward to a future of suing companies in many countries.

eoo: Beginning in 2000, Allvoice gained prominent attention in the UK national media due to its litigation against L&H and IBM.

Mei: Reports unilaterally from the viewpoint of AllVoice, falsely treating %(q:WordExpress) as real speech recognition software: %(orig|Voice recognition software developer AllVoice has launched a fresh appeal against rival Dragon Systems in its wrangle over patent infringements.|AllVoice, which released its WordExpress voice recognition software in 1995, has filed a suit in a US court for alleged infringement against Dragon.|The company has claimed that Dragon, which is owned by Lernout & Hauspie, infringed part of a WordExpress technology patent in the development of its NaturallySpeaking voice recognition software, costing AllVoice %(q:millions) in lost turnover.|The complaint was originally filed in February 1999, but according to AllVoice, suffered from %(q:inordinate delay in the US).|John Mitchell, managing director of AllVoice, said it had secured both UK and US patents for its WordExpress software, which allows users to play back dictation as well as checking wording on screen. However, he claimed that Dragon released similar software soon after.|Since the original complaint, Mitchell claimed Dragon has continued to develop its NaturallySpeaking software and recently shipped its latest version which he said %(q:still shows infringement of the AllVoice software).|%(q:AllVoice will not give up until the patent infringement issue has been sorted out,) Mitchell said.|The Department of Trade and Industry has become involved because of a World Trade Organisation agreement between the UK and US, which AllVoice claims has been violated.)

ipm: Report about the crisis at Lernout and Hauspie of 2001, mentions patent litigation with AllVoice as one of the major strains on the company: %(orig|Meanwhile, L&H's voice recognition subsidiary Dragon is still involved in a legal battle with UK software developer AllVoice. The firm, which released WordExpress voice recognition software in 1995, filed two more injunctions against Dragon in the US courts last week.|Despite L&H's Chapter 11 bankruptcy filing, Dragon still ships its Naturally Speaking Version 5 software, which AllVoice claims breaches its US and UK patents.|John Mitchell, managing director, said AllVoice secured UK and US patents for its software, but alleged that Dragon released similar software shortly afterwards, infringing patents.|%(q:This has continued for more than two years,) he said. %(q:We cannot sell our software when Dragon is including it in its products for free.))  Dragon of course was not including software from AllVoice but at best incling features in its user interface which AllVoice, a company who has not developped any speech recognition software of its own, had patented.  Indeed it is difficult to sell a standalone user interface.

WWW: reports with slight irony about Allvoice's strategy and their legal dispute with IBM: %(orig|ALLVOICE ALL PATENTED|Devon-based AllVoice Computing has successfully applied for US patent law protection for the innovations within its WordExpress software that links IBM's speech recognition engine directly to Microsoft Word. AllVoice is involved in a long running legal dispute with IBM over allegations that IBM engaged in predatory pricing, misused confidential information and mounted a dirty tricks campaign against AllVoice. IBM refuse to comment on these allegations.)

uaW: Lawyer of Lernout & Hauspie report about their success in fending off the claims of Allvoice and obtaining a settlement: %(orig|AllVoice Computing v. L&H Holdings USA, Inc.|software for preserving playback of dictated speech; case settled after defeating two preliminary injunction attempts and winning appeal) and %(orig|AllVoice Computing plc v. %(tp|L&H Holdings USA, Inc.|formerly Dragon Systems, Inc.)|Successfully handled a federal appeal for its client L&H Holdings, which had been sued by AllVoice Computing for patent infringement. After Fish & Richardson defeated AllVoice's motion for a preliminary injunction, AllVoice appealed. A day after the Federal Circuit heard oral argument on the appeal, the Court summarily affirmed the district court's decision. Boston principal Jolynn Lussier handled the appeal with assistance from associate Craig Smith and trial counsel Jack Skenyon.)

tWg: A trade organisation quotes from a PR of AllVoice: %(orig|AllVoice Computing PLC of Newton Abbot, England and IBM Corporation have entered into a licence agreement with regard to certain voice recognition software. Under the terms of the recently concluded deal, IBM has acquired for an undisclosed sum a world-wide non-exclusive licence to all of AllVoice's voice recognition software patents. John Mitchell, Managing Director of AllVoice, said %(q:I am delighted to have concluded this deal and to have obtained this further recognition for AllVoice's contribution to this exciting field.))

tas: AllVoice is a sponsor of this zone in the Exeter university library which provides special facilities of IT access for the disabled.  AllVoice itself offers government-sponsored courses to help the disabled access such facilities, e.g. by means of dicating software.

PAI: Today Programme %(q:A Question of Invention)

ose: AllVoice calls for patent aid

CPW: SEC to Probe L&H

uaE: The UK press uncritically took up the myth of the small guy fighting big evildoers, as spread by AllVoice and their parliamentary representative:

mrm: A small British computer firm with patents on an invention potentially worth millions is being ripped off by one of the world's biggest companies and stymied by American courts, an MP claimed yesterday.

ucl: Allvoice, a Devon-based company, could even go out of business because it alleges that IBM has infringed its patents for using voice recognition technology with computers, while in the US a similar infringement claim against Dragon Systems has taken more than a year to conclude its preliminary hearing - which usually takes only a few days.

ooa: Allvoice's system means people can talk to their computers, rather than typing into keyboards, and enter words directly into documents, correct them, and play back what they actually said.

sat: In Westminster Hall, Patrick Nicholls, Conservative MP for Teignbridge, said during a short debate that rather than being a multi-million-pound firm, Allvoice was struggling for its very existence. He blamed this on the anti-competitive behaviour of certain US companies and the procrastination of the US judiciary. He told MPs that US firms IBM and Dragon Systems had been developing their own voice recognition system but it had many faults and problems which the Allvoice system had managed to iron out.

stt: The US firms invited Allvoice representatives to meet them on the premise that they were interested in buying the system. But IBM later announced that they were developing a new voice recognition system based on many of the Allvoice applications.

nuf: %(q:The way US business has rallied round to destroy Allvoice would make a pack of sharks look like a group of nuns,) Mr Nicholls said.

tmi: John Mitchell, managing director of Allvoice, said afterwards: %(q:We started our patent infringement case against Dragon in February last year. But the preliminary injunction has been delayed and delayed.)

tbW: Indeed the injunction was not only delayed but defeated, because the patents of AllVoice were not found to be very strong.

rai: Some excerpts from the Financial Times article by Fiona Harvey.

dpr: AllVoice has filed a complaint against IBM with the European Commission, the European Union's Brussels-based executive.

its: Patrick Nicholls, MP for Teignbridge in Devon, said the case showed the difficulty small companies faced in enforcing patents against multinationals. Patricia Hewitt, small business minister, said the trade department would 'determine whether the US was fully compliant with its obligations to enforce patents' under intellectual property law.

wnn: A tiny Devon-based software company has received settlement of a two-year-old patent claim against a former speech technology giant. AllVoice said it finally won its claim against Lernout & Hauspie, once the world's leading speech technology company and now bankrupt, for about £400,000 ($575,959). The British company alleged that the larger rival had stolen some of its technology, which helps computers to recognise human speech, and used it in several products.

slW: This text is copied from a PR on the Allvoice website which adds, as of 2003/08/10:

alt: AllVoice was awarded the CBI award for technical innovation for its voice recognition products in 1996.

sWf: Other statements by AllVoice say that AllVoice was %(q:awarded a patent for its contribution to voice recognition technology).  Upon closer look however it becomes clear that AllVoice has contributed nothing to speech recognition itself.  Moreover, patents are not %(q:awarded for contributions) nor are they a proof of excellence in any field.

egW: Speech recognition technology excites large US corporations

oce: Imagine you want to record information from a prospective customer without the need for human intervention. Generic speech recognition technology focuses almost entirely on improving speed and accuracy.  However, AllVoice looks at the technology from another angle - the user's perspective.

irW: The company's innovations cover the need to streamline error handling i.e. mis-recognitions. 'WordExpress' is a highly successful PC flagship product and they have developed 'CallVoice', speech recognition technology to automate conversations. A test demonstration system that will gather your name and address can be called on 01626333558. You can alternatively choose two other demonstrations, by voice naturally, to check if your lottery numbers have won or to find out past lottery results.

hWr: AllVoice's Managing Director, John Mitchell, first became involved in speech recognition in 1992, noticing that US products were seriously lacking in real user facilities. Although surprised initially by this crucial oversight, they first added simple macros. Later, they wrote complex software that made AllVoice hugely successful with users and their US suppliers, e.g. IBM, approached them to make them their only UK main partner - recognising their national and international success. Further innovations followed from there.

nit: Funding for product development has been entirely from sales profits until DTI SPUR and then SMART awards were obtained in recent years.  AllVoice has been the subject of national and international media coverage. This has caused a dilemma - how to cope with the massive demand and maintain an adequate service to potential clients? The solution has been to provide time limited demonstration software on to their web site. Payment results in an unlocking key - thus saving a delivery cost.

rWi: Since their entry into speech recognition, AllVoice have grown at an impressive rate - 20 staff from 6 in two years with exponential growth. AllVoice are continuously developing their technology and its applications.

rab: They have learnt about IPR the hard way, as so many do. Initial technology was included in other software without recognition or royalty. Despite UK and US patents on recent innovations AllVoice have to actively protect their technology through legal action. A key obstacle has been the lack of support to help protect UK innovations against patent infringements and anti-competitive activities by corporate giants. Mr Mitchell argues that we need Government support or a change in legislation to prevent these sort of attacks. A simple legislation change would be to require anyone bringing a case to put at risk a percentage of their turnover. Thus the large corporate would have to compete on an equal basis to the SME.

iec: John Mitchell said: %(q:No one had ever achieved what we set out to achieve - thus we never really knew if it could be done. It is easy for another to then copy your lead in the knowledge that a solution is possible. We are proud of what we have achieved for the advancement and use of speech recognition.)

tlt: Proposal to introduce additional damages for UK Patent Infringement

aiW: The Bill to amend the Patents Act 1977 to enable a court to award additional damages in an action for patent infringement, taking into account the flagrancy of the infringer, has been reintroduced to Parliament. Its aim is to help patent owners, especially SMEs, to enforce their patent rights and to provide a deterrent to potential infringers.

lwW: This very short private member's bill was apparently inspired by a legal fight of the UK-based AllVoice computer software company against the giant US computer company, IBM. AllVoice claimed that IBM had exploited its voice recognition technology.

okh: The Bill was originally introduced last autumn (see our October newsletter by [18]clicking here) but was withdrawn due to a technicality. It has now been reintroduced as a Ten-Minute Rule Bill. If enacted, it will bring patent law into line with that of copyright and design right under the Copyright, Designs and Patents Act 1988 which provides for additional damages.

jet: Last autumn, the Patent Office was seeking the views from industry on the Bill.

see: A key motivation for the Bill as presented in the House of Commons Research Paper 02/23 appears to be to give SMEs greater leverage in enforcing their patents against big companies. The bill was apparently inspired by the legal fight of the UK-based AllVoice computer software company against IBM. However, the Bill has received opposition from both big organisations and SMEs.

doW: Arlene McCarthy has repeatedly been touting AllVoice and its founders as a model of the kind of software SME which the EU should be helping with the proposed software patents legislation.  McCarthy however did not mention them by name.  She spoke only of a %(q:british speech recognition software SME founded by a couple in an unemplyment blackspot in UK, which, thanks to patent protection, succeded in protecting its innovation against nasty big business competitors and thereby growing from 2 to 50 employees.)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpikta.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swxai-allvoice ;
# txtlang: en ;
# multlin: t ;
# End: ;

