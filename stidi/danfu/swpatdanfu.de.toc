\contentsline {section}{\numberline {1}Was ist ein Software-Patent?}{2}{section.1}
\contentsline {section}{\numberline {2}Was ist so schlimm an Softwarepatenten? Warum sollten Software-Innovationen nicht ebenso patentierbar sein wie andere Innovationen, wenn sie wirklich neu und erfinderisch sind?}{3}{section.2}
\contentsline {section}{\numberline {3}Verdient die Investition eines Programmierers denn keinen Schutz?}{4}{section.3}
\contentsline {section}{\numberline {4}Ist das Urheberrecht nicht viel zu leicht zu umgehen, um wirklich n\"{u}tzlich zu sein?}{4}{section.4}
\contentsline {section}{\numberline {5}Softwareentwicklung ist doch teuer. Manche Gro{\ss }projekte kosten hunderte von Mannjahren. In der Industrie werden solche Investitionen mit Patenten abgesichert. Wenn Informatiker sich mit dem Urheberrecht zufriedengeben m\"{u}ssen, wie kann das Software-Engineering denn dann jemals ein industrielles Niveau erreichen?}{5}{section.5}
\contentsline {section}{\numberline {6}Das Urheberrecht passt doch nur f\"{u}r Werke wie Goethes Faust!}{5}{section.6}
\contentsline {section}{\numberline {7}Gibt es nicht auch im Bereich der Informatik wirklich gro{\ss }artige Ideen, die nach einem st\"{a}rkeren Schutz verlangen als ihn das Urheberrecht bieten kann?}{6}{section.7}
\contentsline {section}{\numberline {8}Was ist so besonders an Software?}{6}{section.8}
\contentsline {section}{\numberline {9}Seit wann ist Software in Europa patentierbar?}{8}{section.9}
\contentsline {section}{\numberline {10}Laut EP\"{U} sind doch nur ``Programme als solche'' von der Patentierung ausgeschlossen, und an dieser vagen Formulierung zeigt sich, dass der Gesetzgeber 1973 nicht genau wusste, was er wollte, und daher dem EPA die Freiheit geben wollte, die Regeln im Lichte neuer technischer Entwicklungen allm\"{a}hlich zu \"{a}ndern, nicht wahr?}{9}{section.10}
\contentsline {section}{\numberline {11}Wer will Softwarepatente? Wer ist dagegen?}{11}{section.11}
\contentsline {subsection}{\numberline {11.1}Dagegen}{11}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Daf\"{u}r}{12}{subsection.11.2}
\contentsline {section}{\numberline {12}Weitere Lekt\"{u}re}{12}{section.12}
