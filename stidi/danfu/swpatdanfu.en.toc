\contentsline {section}{\numberline {1}What is a Software Patent?}{2}{section.1}
\contentsline {section}{\numberline {2}If the principal objections to software patents are (1) obviousness and (2) lack of novelty, then why all this ruckus about whether software is patentable per se?}{3}{section.2}
\contentsline {section}{\numberline {3}Don't the investments of an innovatived programmer deserve protection?}{4}{section.3}
\contentsline {section}{\numberline {4}Isn't copyright far too easy to circumvent to be of any real use?}{4}{section.4}
\contentsline {section}{\numberline {5}Software development is expensive. Large-scale projects cost hundreds of man-years. In the hardware engineering world, such investements are secured by patents. If similar protection isn't available for software engineering, how can this young industry ever achieve industrial strength?}{5}{section.5}
\contentsline {section}{\numberline {6}Copyright is appropriate only for artistic works like Shakespeare's Hamlet!}{5}{section.6}
\contentsline {section}{\numberline {7}Aren't there some great ideas in the field of computing that deserve a greater reward than what can be offered by copyright?}{6}{section.7}
\contentsline {section}{\numberline {8}What is so special about software?}{7}{section.8}
\contentsline {section}{\numberline {9}Since when is software patentable in Europe?}{8}{section.9}
\contentsline {section}{\numberline {10}Laut EP\"{U} sind doch nur ``Programme als solche'' von der Patentierung ausgeschlossen, und an dieser vagen Formulierung zeigt sich, dass der Gesetzgeber 1973 nicht genau wusste, was er wollte, und daher dem EPA die Freiheit geben wollte, die Regeln im Lichte neuer technischer Entwicklungen allm\"{a}hlich zu \"{a}ndern, nicht wahr?}{9}{section.10}
\contentsline {section}{\numberline {11}Who wants software patents? Who is against them}{11}{section.11}
\contentsline {subsection}{\numberline {11.1}Against}{11}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Pro}{12}{subsection.11.2}
\contentsline {section}{\numberline {12}Further Reading}{12}{section.12}
