
   [1]ESR Pollmeier [2][USEMAP:kopf_welcome.gif]
   [3]Software-Patente
   
   [4]Einf�hrung - [5]Beispiele - [6]Heute angemeldet, morgen Patent? -
   Statement
   Diese Seite enth�lt Links, die auf Angebote au�erhalb unserer Website
   www.esr-pollmeier.de/... verweisen. Die ESR Pollmeier GmbH und ihre
   Autoren sind f�r Inhalte au�erhalb der Website
   www.esr-pollmeier.de/... nicht verantwortlich, auch wenn von hier per
   Link darauf verwiesen wird. Inhalte au�erhalb der Website
   www.esr-pollmeier.de/... geben nicht unbedingt die Meinung der ESR
   Pollmeier GmbH wieder, auch wenn von hier per Link darauf verwiesen
   wird. Dokumente und Zitate auf der Website www.esr-pollmeier.de/...,
   die nicht von der ESR Pollmeier GmbH stammen, geben nicht unbedingt
   die Meinung der ESR Pollmeier GmbH wieder. Anregungen, Kommentare,
   Kritik zu www.esr-pollmeier.de/swpat/... bitte an [7]Stefan Pollmeier.
   Nach ausf�hrlicher Analyse der derzeitigen Patentierungspraxis, des
   EU-Richtlinien-Vorschlags, der Literatur, vieler Beispiel-Patente und
   -Patent-Anmeldungen, nach ausf�hrlichen Diskussionen mit Bef�rwortern
   und Gegnern der Software-Patentierung haben wir das folgende Statement
   und die daraus resultierenden Forderungen erarbeitet.
   Statement zu Software-Patenten
       
    1. Software-Patente bieten keine Innovations-Anreize (kennt jemand
       auch nur einen Unternehmer, der gesagt h�tte, er w�rde mehr in
       Software-Entwicklung investieren, wenn er die Software denn nur
       patentieren k�nne?).
    2. Software-Patente werden oft zum "Schutz" trivialer Details oder
       sehr allgemein gehaltener Ideen eingesetzt (oft einfache
       Kombination vorhandenen Wissens); die Ver�ffentlichung leistet
       damit keinen Beitrag zum Gemeinwohl (Patente stellen ja
       bekanntlich ein Monopol dar, das dadurch gerechtfertigt wird, da�
       die Ver�ffentlichung einen Beitrag zum Gemeinwohl leistet).
    3. Die gegenw�rtige Patentierungs-Praxis des EPA hat zu einer
       "Vielzahl zweifelhafter [Software-]Patente" gef�hrt (so z. B.
       GI-Pr�sident Prof. Dr. Mayr).
    4. Eine EU-Richtlinie f�r Software-Patente zu erlassen, die es
       erlaubt, noch mehr Software-Patente zu erteilen, ohne da� die
       Probleme der jetzigen Patentierungs-Praxis ernsthaft angegangen
       werden, produziert letztlich zweifelhafte Software-Patente in
       Potenz.
    5. Der Technik-Begriff der gegenw�rtigen Patentierungspraxis (eine
       Software-Erfindung mu� "technisch" sein, damit sie patentiert
       werden kann) ist so degeneriert, da� notfalls alleine "technische
       �berlegungen" im Erfindungsproze� ausreichen, um ein Patent
       erteilt zu bekommen (ein Patentanwalt, der bei Software keine
       "technischen �berlegungen" konstruieren kann, w�re sein Geld nicht
       wert).
       Der Technik-Begriff des EU-Richtlinienvorschlags reduziert sich im
       wesentlichen darauf, da� Software per se als technisch und damit
       patentierbar definiert wird.
    6. Wenn praktisch jede Software patentiert werden kann, dann kann
       auch jede Gesch�ftsmethode oder jeder Algorithmus patentiert
       werden, wenn die Patentanmeldung sie mit etwas Software garniert.
       Evtl. noch vorhandene minimale Schranken gegen die Patentierung
       von Gesch�ftsmethoden oder Algorithmen werden im Verlauf der
       praktischen Anwendung der EU-Richtlinie fallen, das lehrt die
       Aush�hlung des Technik-Begriffs bis auf eine Worth�lse in den
       letzten 10 Jahren.
    7. Software-Patente auf Basis der gegenw�rtigen Patentierungspraxis
       und des EU-Richtlinienentwurfs f�hren zu weitreichenden Monopolen
       in nicht umgehbaren Schl�sselfeldern aktueller und kommender
       Software-Anwendung. Diese Monopole verhindern Wettbewerb und
       Innovation und k�nnen andere Software-Anbieter zerst�ren.
    8. Einzige m�gliche Gegenma�nahme ist flei�iges Patentieren, um
       Patente als Verhandlungsmasse aufzubauen, f�r den Fall das man von
       einem Wettbewerber angegriffen wird. Im ausgeglichenen Fall ein
       Nullsummenspiel mit Geldvernichtung, ansonsten teuer oder t�dlich
       f�r den Unterlegenen. (Gibt es im Handel schon ein
       Patent-Portfolio-Spiel und wo k�nnte ich mir die Idee ggf.
       sch�tzen lassen?)
    9. Die (inzwischen auch in den USA selbst beklagte) Situation bei
       Software-Patenten in den USA zwingt Europa unter keinem
       Gesichtspunkt, sein Patentrecht den USA anzun�hern. Naheliegend
       w�re doch, hier in Europa unbehindert von Software-Patenten
       innovativ t�tig zu sein, w�hrend die US-Amerikaner in
       Verhandlungszimmern oder in Gerichtss�len sitzen. Wenn man will
       (oder mu�) kann man Patente in den USA anmelden.
       W�rde die EU nur einen Bruchteil der Gelder, die im Gefolge einer
       weitreichenden Software-Patentierung in Europa verbraten w�rden,
       in die F�rderung von US-Patentanmeldungen investieren, dann h�tte
       sie mehr f�r die Wettbewerbsf�higkeit der Europ�er in USA getan
       als durch die EU-Richtlinie.
       
       Diese Argumente enthalten das Wort "Mittelstand" oder "KMU" nicht,
       weil sie unabh�ngig von Mittelstand gelten. Gro�unternehmen sind
       u. E. oft deshalb f�r Software-Patente, weil Sie glauben, das
       Spiel (Ziffer 8 oben) gewinnen zu k�nnen und/oder weil sich deren
       Patentabteilungen verselbst�ndigt haben und dem Unternehmen die
       Richtung vorgeben (statt umgekehrt). Da entsprechende Unternehmen
       meist nicht zugeben wollen, da� sie Patent-Portfolio-Spiele
       spielen wollen, wurden die hinreichend bekannten Argumentationen
       aufgebaut.
       Eine Schl�sselrolle in der Argumentation der
       Software-Patent-Bef�rworter spielt der Technik-Begriff. Immer
       wieder wird Kritikern vorgehalten, da� Software-Patente jetzt und
       k�nftig nur f�r "technische" Erfindungen erteilt werden. Darauf
       folgt meist eine abstrakte, endlos-erm�dende und ergebnislose
       Diskussion �ber die Auslegung des Technik-Begriffs.
       
   Deshalb fordern wir
       
    a. weitere Diskussionen anhand von Beispielen (Patenten,
       Patentanmeldungen) zu f�hren und dort insbesondere die Technizit�t
       zu betrachten
    b. bei den Diskussionen mehr Personen aus der
       Softwareentwicklungs-Praxis einzubeziehen
    c. die Bef�rworter von Software-Patenten an der Patentpraxis und
       Patentanmeldepraxis ihrer Unternehmen zu messen: Wer in Europa
       Patentanmeldungen auf Gesch�ftsmethoden und Algorithmen einreicht
       und parallel behauptet, der Technik-Begriff der
       Patentierungspraxis oder der EU-Richtlinie verhindere Patente auf
       Gesch�ftsmethoden und Algorithmen, der spielt grob falsch um sich
       Vorteile f�r das Spiel nach Ziffer 8 zu verschaffen.
    d. zur�ck zum klassischen Technik-Begriff einer Erfindung als
        "einer Lehre zum planm��igen Handeln unter Einsatz beherrschbarer
            Naturkr�fte zur Erreichung eines kausal �bersehbaren
            Erfolges"
       Die "Naturkr�fte" (physikalisch, chemisch, biologisch ...) sind
       hier der Knackpunkt, der Begriff des Technischen mu� eindeutig
       durch seine Beziehung zur Welt der Materie (im Gegensatz zur Welt
       des Geistes) definiert werden.
       In einem solchen Kontext ist Software patentierbar und in einem
       solchen Kontext soll sie es auch bleiben (Beispiel Antiblockier-
       System ABS: wenn eine ABS-Erfindung etwas neues �ber
       Kausalzusammenh�nge beherrschbarer Naturkr�fte lehrt, dann ist sie
       auch dann patentierbar, wenn bei ihrer Ausfuehrung ein
       Datenverarbeitungsprogramm eingesetzt wird.)
       
   [8]Pl�doyer f�r die Anwendung eines klaren Technik-Begriffs
   (PDF, 245 KB)
   
   Folien zu einem Vortrag beim ZVEI GA Kommunikation in der
       Automatisierungstechnik am 05.06.02
       
   Zur Beachtung: Die in diesem Text verwendeten Soft- und
   Hardware-Bezeichnungen, Firmen- und Markennamen k�nnen warenzeichen-,
   marken- oder patentrechtlichem Schutz unterliegen.
   Anregungen, Kommentare, Kritik zu www.esr-pollmeier.de/swpat/... an
   [9]Stefan Pollmeier.
   ESR. The Dynamic Solution
     _________________________________________________________________
   
   Alle Seiten [10]� 20002002 ESR Pollmeier GmbH [11][Impressum]

References

   1. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/spollm-swpat/www.esr-pollmeier.de/index.html
   2. LYNXIMGMAP:file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/spollm-swpat/www.esr-pollmeier.de/swpat/statement.html#willkommen-titel
   3. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/spollm-swpat/www.esr-pollmeier.de/swpat/index.html
   4. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/spollm-swpat/www.esr-pollmeier.de/swpat/einfuehrung.html
   5. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/spollm-swpat/www.esr-pollmeier.de/swpat/beispiele.html
   6. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/spollm-swpat/www.esr-pollmeier.de/swpat/anmeldungen.html
   7. mailto:Software-Patente<gl@esr-pollmeier.de>
   8. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/spollm-swpat/www.esr-pollmeier.de/swpat/ESR_Pollmeier_SWPat_ZVEI_2002-06-05.pdf
   9. mailto:Software-Patente<gl@esr-pollmeier.de>
  10. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/spollm-swpat/www.esr-pollmeier.de/de/Impressum.html
  11. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/spollm-swpat/www.esr-pollmeier.de/de/Impressum.html
