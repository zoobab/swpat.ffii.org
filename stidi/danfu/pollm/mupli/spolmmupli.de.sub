\begin{subdocument}{spolmmupli}{Beispiele}{http://swpat.ffii.org/analyse/antworten/pollm/beispiele/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Beispiele f\"{u}r sehr allgemeine Software-Patente und f\"{u}r Software-Patente, die f\"{u}r die Automatisierungstechnik relevant sind. Spezieller Abschnitt zu Feldbus-Patenten.}
Diese Seite enth\"{a}lt Links, die auf Angebote au{\ss}erhalb unserer Website www.esr-pollmeier.de/... verweisen. Die ESR Pollmeier GmbH und ihre Autoren sind f\"{u}r Inhalte au{\ss}erhalb der Website www.esr-pollmeier.de/... nicht verantwortlich, auch wenn von hier per Link darauf verwiesen wird. Inhalte au{\ss}erhalb der Website www.esr-pollmeier.de/... geben nicht unbedingt die Meinung der ESR Pollmeier GmbH wieder, auch wenn von hier per Link darauf verwiesen wird. Dokumente und Zitate auf der Website www.esr-pollmeier.de/... die nicht von der ESR Pollmeier GmbH stammen, geben nicht unbedingt die Meinung der ESR Pollmeier GmbH wieder.

Anregungen, Kommentare, Kritik zu www.esr-pollmeier.de/swpat/... an Stefan Pollmeier\footnote{mailto:gl@esr-pollmeier.de?subject=http://swpat.ffii.org/analyse/antworten/pollm/beispiele/index.de.html}

Tip: Sie k\"{o}nnen Patente selbst recherchieren bei DepatisNet\footnote{http://www.depatisnet.de}.

\begin{itemize}
\item
{\bf {\bf Europ\"{a}ische Softwarepatente: Einige Musterexemplare\footnote{http://swpat.ffii.org/patente/muster/index.de.html}}}

\begin{quote}
Zitat von o. a. Link des FFII: \begin{quote}
{\it Auf folgende eing\"{a}ngige Beispiele stie{\ss}en wir beim ersten Durchst\"{o}bern unserer Softwarepatente-Tabellen. Sie wurden fast zuf\"{a}llig ausgesucht und k\"{o}nnen somit als repr\"{a}sentativ f\"{u}r die Ma{\ss}st\"{a}be des Europ\"{a}ischen Patentamtes hinsichtlich Technizit\"{a}t und Erfindungsh\"{o}he gelten.  Wenn sie in irgendeinem Punkt vom Mittelma{\ss} abweichen, dann darin, da{\ss} sie relativ leicht einem breiten Publikum verst\"{a}ndlich gemacht werden k\"{o}nnen.}
\end{quote} Diese Kritik k\"{o}nnen Sie selbst \"{u}berpr\"{u}fen: der FFII stellt die Patente im Original zur Verf\"{u}gung.
\end{quote}
\filbreak

\item
{\bf {\bf Softwarepatente in Aktion\footnote{http://swpat.ffii.org/patente/wirkungen/index.de.html}}}

\begin{quote}
Zitat von o. a. Link des FFII

\begin{quote}
{\it In den letzten Jahren sind einige Streitf\"{a}lle um Softwarepatente durch die Medien bekannt geworden.  Es handelt sich hierbei um die Spitze des Eisbergs. Die meisten Entwickler und Firmen werden nur au{\ss}erhalb der Gerichte mit Patentforderungen konfrontiert, und Schweigen liegt im beiderseitigen Interesse. Viele Projekte werden zur\"{u}ckgeschraubt oder aufgegeben. Es ist schwer, verhinderte Entwicklungen zu dokumentieren. Hier wollen wir diesen Versuch unternehmen.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Steuerung mit integriertem Webserver}}

\begin{quote}
US 5,805,442 - Distributed Interface Architecture for Programmable Industrial Control Systems (PDF, 362 KB)\footnote{US5805442A.pdf}

Dieses Patent ist bekannt durch den Rechtsstreit zwischen Schneider Electric und Opto 22 in den USA, siehe den Artikel in control.com\footnote{http://www.control.com/983813199/index\_html}. Schneider Electric hatte das Patent im Jahr 2000 vom Erfinder Ken Crater (Control Technology Corporation) gekauft. In Europa, Deutschland ist uns bisher keine Anmeldung bekannt. Frage: K\"{o}nnte dieses Patent auf Basis der jetzigen Praxis oder des Richtlinien-Vorschlags auch in Europa erteilt werden?
\end{quote}
\filbreak

\item
{\bf {\bf \"{U}bertragen von Daten aus einer SPS in eine Tabellenkalkulation}}

\begin{quote}
US 5,038,318Device for Communicating Real Time Data Between a Programmable Logic Controller and a Program Operating in a Central Controller\footnote{US5038318A.pdf} (PDF, 1003 KB)

``Ber\"{u}hmt'' geworden durch den Verkauf von Schneider Electric an Solaia und die Klage Solaias gegen Anwender von OPC wie z. B. BMW, siehe z. B. Artikel in Control Engineering\footnote{http://www.controleng.com/archives/news/2002/April/jm0429b.htm}. Dieses Patent hat inzwischen auch europ\"{a}ische und deutsche Geschwister bekommen:

\begin{itemize}
\item
EP 0 346 441 B1\footnote{EP0346441B1.pdf} (PDF, 874 KB)

\item
DE 38 55 219 T2\footnote{DE3855219T2.pdf} (\"{U}bersetzung von EP 0 346 441 B1) (PDF, 1228 KB)
\end{itemize}

Die OPC-Foundation dokumentiert inzwischen auf Ihrer Homepage\footnote{http://www.opcfoundation.org/} in der Rubrik Solaia Update Gegenklagen z. B. von Rockwell und BMW.

Frage: Ist bekannt, ob inzwischen auch in Europa Klagen anh\"{a}ngig sind?
\end{quote}
\filbreak

\item
{\bf {\bf National Instruments (US) \"{u}bernahm GFS und Datalog (DE) nach Softwarepatent-Streit}}

\begin{quote}
Die beiden deutsche Unternehmen GFS und Datalog wurden 1998/99 nach Patentstreitigkeiten von dem US-Unternehmen National Instruments \"{u}bernommen.

Zitat aus CI:
\begin{quote}
{\it National Instruments (NI, Austin, Tex.) acquired Datalog (Monchengladbach, Germany) in Sept. '98 and GFS (Aachen, Germany) in September '99. The German firms manufacture software products that NI felt were infringing on its LabView patents, and so, after considering numerous options, NI decided to buy both.}
\end{quote}
Frage: Um welche Patent-Nummern ging es damals? Die Liste der Patente auf der National Instruments Website ist sehr lang. National Instruments hat lt. DepatisNet\footnote{http://www.depatisnet.de/} mindestens 5 weitere europ. Patentanmeldungen laufen.
\end{quote}
\filbreak

\item
{\bf {\bf PROFInet (Ethernet als Feldbus)}}

\begin{quote}
Die Profibus Nutzerorganisation\footnote{http://www.profibus.com/} (PNO) (PNO) stellt ihren Mitgliedern Quellcode und Dokumentation f\"{u}r die Implementierung von PROFInet zur Verf\"{u}gung (PROFInet Runtime Software).
Die Lizenzvereinbarung\footnote{http://www.profibus.com/metanavigation/downloads/texte/02427/index.html} f\"{u}r diese Software erlaubt den PNO-Mitgliedern unter Punkt 1.5 das Recht, 5 Patente zu nutzen. Zu den ersten 4 Nummern in der Lizenzvereinbarung haben wir die folgenden Patente und Patentfamilienmitglieder gefunden.

\begin{itemize}
\item
System, Funktionsobjekte, COM/DCOM (Patentfamilienmitglieder DE 198 50 469)
\begin{itemize}
\item
DE 198 50 469 A1\footnote{DE1b9850469A1.pdf} - Automatisierungssystem und Verfahren zum Zugriff auf die Funktionalit\"{a}t von Hardwarekomponenten - PDF, 266 KB

\item
WO 00/26731 A1 - Automation System and Method for Accessing the Functionality of Hardware Components

\item
US 2001/0052041 A1 - Titel wie WO 00/26731 A1
\end{itemize}

\item
Projektierung (Verschaltung) (Patentfamilienmitglieder DE 199 10 544)
\begin{itemize}
\item
DE 199 10 544 A1\footnote{DE19910544A1.pdf} - Verfahren zur impliziten Projektierung von Kommunikationsverbindungen (PDF, 181 KB)

\item
WO 00/54146 A2 - Method for Implicitly Configuring Communications Links

\item
WO 00/54146 A3 - Recherchebericht zu WO 00/54146 A2
\end{itemize}

\item
Kommunikationsmechanismen, Synchronisation von Anwendungen (Patentfamilienmitglieder DE 199 29 751)
\begin{itemize}
\item
DE 199 29 751 A1 - System und Verfahren zur \"{U}bertragung von Daten, insbesondere zwischen einem Anwender- und einem Serverprogramm im Bereich der Automatisierungstechnik mit verteilten Objekten\footnote{DE19929751A1.pdf} (PDF, 164 KB)

\item
WO 01/101365 A2 - System and Method for Transmitting Data, Especially Between a User Programme and a Server Programme in the Field of Automation Technology with Distributed Objects

\item
WO 01/101365 A3 - Search Report for WO 01/101365 A2
\end{itemize}

\item
Proxy, Gateway zwischen Ethernet und Profibus (Patentfamilienmitglieder DE 199 55 306)
\begin{itemize}
\item
DE 199 55 306 C1\footnote{DE19955306C1.pdf} - Kommunikationsteilnehmer bzw. Kommunikationsverfahren (PDF, 251 KB)

\item
WO 01/37492 B1 - Communication Subscriber or Communication Method for Communication with a Field Bus and a Network Transport of Diagnostics Information

\item
101 16 756 - transport of diagnostics information (dieses 5. Patent in der Lizenzvereinbarung\footnote{http://www.profibus.com/metanavigation/downloads/texte/02427/index.html} war bei DepatisNet\footnote{http://www.depatisnet.de} nicht zu finden)
\end{itemize}
\end{itemize}
\end{quote}
\filbreak

\item
{\bf {\bf Feldbus-Patente (Warrior-Patente)}}

\begin{quote}
Die folgenden Patente stammen von Fisher-Rosemount (heute Emerson Process Management) und werden besonders im Umfeld von Foundation Fieldbus\footnote{http://www.rosemount.com/news/pressrel/warrior.html} und HART diskutiert. Ein Teil der Patente wird nach dem Erfinder auch ``Warrior-Patente'' genannt. In der Fachwelt wurde bei fr\"{u}heren Diskussionen angemerkt, da{\ss} diese Patente in Europa so nicht m\"{o}glich w\"{a}ren. Inzwischen gibt es aber auch europ\"{a}ische und/oder deutsche Patente oder Anmeldungen, wie Patentfamilienrecherchen bei DepatisNet\footnote{http://www.depatisnet.de} zeigen.

Frage: wie sieht bei den u. a. Patenten der ``technische Charakter'' aus?

Zitat DPMA\footnote{http://www.dpma.de/infos/schutzrechte/verfahren11.html}: Eine programmbezogene Erfindung hat technischen Charakter, wenn zur L\"{o}sung der Aufgabe, die der Erfindung zugrunde liegt, von Naturkr\"{a}ften, technischen Ma{\ss}nahmen oder Mitteln (z.B. von hydraulischen Kr\"{a}ften, elektrischen Str\"{o}men in Schaltelementen und Regeleinrichtungen oder von Signalen in DV-Anlagen) unmittelbar Gebrauch gemacht wird.

\begin{itemize}
\item
Field-Mounted Control Unit (Patentfamilienmitglieder)
\begin{itemize}
\item
US 5,825,664 - Field-Mounted Control Unit\footnote{US5825664A.pdf} (PDF, 941 KB)

\item
US 5,485,400\footnote{US5485400A.pdf} - Field-Mounted Control Unit (PDF, 591 KB)

\item
US 5,333,114\footnote{US5333114A.pdf} - Field-Mounted Control Unit (PDF, 457 KB)

\item
EP 0 495 001 B1\footnote{EP0495001B1.pdf} - Field-Mounted Control Unit (PDF, 472 KB)

\item
DE 690 32 954 T2\footnote{DE69032954T2.pdf} - In einer Arbeitsumgebung montierte Steuereinheit (\"{U}bersetzung von EP 0 495 001 B1) (PDF, 519 KB)
\end{itemize}

\item
Transmitter with Improved Compensation (Patentfamilienmitglieder)
\begin{itemize}
\item
US 5,642,301\footnote{US5642301A.pdf} - Transmitter with Improved Compensation (PDF, 608 KB)

\item
EP 0 741 858 B1\footnote{EP0741858B1.pdf} - Transmitter with Improved Compensation (PDF, 519 KB)

\item
DE 695 04 036 T2\footnote{DE69504036T2.pdf} - Wandler mit verbesserter Kompensation (\"{U}bersetzung von EP 0 741 858 B1) (PDF, 742 KB)
\end{itemize}

\item
Field Based Process Control System With Auto-Tuning (Patentfamilienmitglieder)
\begin{itemize}
\item
US 5,691,896 - Field Based Process Control System With Auto-Tuning\footnote{US5691896A.pdf} (PDF, 549 KB)

\item
EP 1 122 625 A1 - Field Based Process Control System With Auto-Tuning\footnote{EP1122625A1.pdf} (PDF, 456 KB)

\item
EP 0 845 118 B1 - Field Based Process Control System With Auto-Tuning\footnote{EP0845118B1.pdf} (PDF, 625 KB)
\end{itemize}

\item
Process I/O to Fieldbus Interface Circuit (Patentfamilienmitglieder)
\begin{itemize}
\item
US 5,764,891 - Process I/O to Fieldbus Interface Circuit\footnote{US5764891A.pdf} (PDF, 551 KB)

\item
DE 197 05 734 A1 - Verbesserte Proze{\ss}eingabe-/ausgabe an einer Feldbus-Schnittstellenschaltung\footnote{DE19705734A1.pdf} (PDF, 370 KB)
\end{itemize}

\item
Interface Controls for use in an Field Device Management System/Device Description (Patentfamilienmitglieder)
\begin{itemize}
\item
US 5,903,455 - Interface Controls for use in an Field Device Management System\footnote{US5903455A.pdf} (PDF, 1158 KB)

\item
US 5,796,602 - Method and Apparatus Using a Device Description for a Conventional Device\footnote{US5796602A.pdf} (PDF, 465 KB)

\item
EP 0 885 412 B1 - Interface Controls for a Field Device Management System\footnote{EP0885412B1.pdf} (PDF, 2523 KB)

\item
EP 0 879 444 B1 - Method and Apparatus Using a Device Description for a Conventional Device\footnote{EP0879444B1.pdf} (PDF, 586 KB)
\end{itemize}

\item
Local Device and Process Diagnostics in a Process Control Network ... (Patentfamilienmitglieder)
\begin{itemize}
\item
US 6,026,352 - Local Device and Process Diagnostics in a Process Control Network Having Distributed Control Functions\footnote{US6026352A.pdf} (PDF, 1286 KB)

\item
US 5,970,430 - Local Device and Process Diagnostics in a Process Control Network Having Distributed Control Functions\footnote{US5970430A.pdf} (PDF, 1324 KB)

\item
EP 1 022 626 A2 - Local Device and Process Diagnostics in a Process Control Network Having Distributed Control Functions\footnote{EP1022626A2.pdf} (PDF, 1262 KB)
\end{itemize}
\end{itemize}
\end{quote}
\filbreak
\end{itemize}

\begin{itemize}
\item
{\bf {\bf Unterst\"{u}tzer und Mitglieder des FFII}\footnote{http://www.ffii.org/verein/leute/index.de.html}}

\begin{quote}
Aktuelle Angaben \"{u}ber die Zusammensetzung des F\"{o}rdervereins f\"{u}r eine Freie Informationelle Infrastruktur e.V.
\end{quote}
\filbreak

\item
{\bf {\bf Satzung des FFII}\footnote{http://www.ffii.org/verein/statut/index.de.html}}

\begin{quote}
Die Satzung wurde bereits vom M\"{u}nchener Finanzamt f\"{u}r K\"{o}rperschaften als f\"{u}r gemeinn\"{u}tzige Vereine geeignet anerkannt und von den Gr\"{u}ndungsmitgliedern Holger Blasum, Christian Hiesl, Bernhard Kuhn, Stephan L\"{o}sl, Hartmut Pilch, Christian Reiser, Friedrich Sch\"{a}uffelhut, Thomas Tanner, WANG Tao und Thorsten Weigl am 17. Februar 1999 in M\"{u}nchen beschlossen und unterzeichnet.  Durch Beschluss der Mitgliederversammlung vom 18. Mai 2000 wurde sie geringf\"{u}gig ge\"{a}ndert.
\end{quote}
\filbreak

\item
{\bf {\bf FFII Kassenberichte}\footnote{http://www.ffii.org/verein/geld/index.de.html}}

\begin{quote}
Was der F\"{o}rderverein f\"{u}r eine Freie Informationelle Infrastruktur (FFII) e.V. seit 1999 eingenommen und ausgegeben hat.
\end{quote}
\filbreak

\item
{\bf {\bf FFII Unterst\"{u}tzerfunktionen und Profilpflege mit KNECHT}\footnote{http://www.ffii.org/verein/knecht/index.de.html}}

\begin{quote}
Die Basisfunktionen, die dem FFII-Eintragsformular zugrunde liegen, lassen sich von der Kommandozeile und via E-Post (mail) steuern.  Hier versuchen wir eine vollst\"{a}ndige \"{U}bersicht zu bieten.
\end{quote}
\filbreak

\item
{\bf {\bf Verteiler, Foren, Mailinglisten}\footnote{http://www.ffii.org/verein/forum/index.de.html}}

\begin{quote}
Wo wir die Entwicklung der informationelle Infrastruktur beobachten und mitzugestalten versuchen.  Neben dem Betrieb eigener Foren unterst\"{u}tzt der FFII auch andere Gruppen bei der Arbeit an gemeinn\"{u}tzigen Informationswerken.
\end{quote}
\filbreak

\item
{\bf {\bf FFII-Beitrittsformular}\footnote{http://www.ffii.org/verein/beitritt/index.de.html}}

\begin{quote}
F\"{u}llen Sie das gedruckte Formular aus und schicken Sie es in einem Fensterbriefumschlag per Papierpost ab.  Erteilen Sie uns bitte wenn m\"{o}glich eine Abbuchungsgenehmigung.  Ein leicht veraltetes Verfahren.
\end{quote}
\filbreak

\item
{\bf {\bf Verwaltung des FFII e.V.: Probleme und L\"{o}sungen}\footnote{http://www.ffii.org/verein/adm/index.de.html}}

\begin{quote}
Wer sind die Mitglieder?  Wie ist das Vereinskonto zug\"{a}nglich?  Wie sind ausstehende Zahlungen einzutreiben?  Wie ist der Verkehr mit dem Finanzamt zu handhaben?  Wo sind die Unterlagen und Angaben?  Wie lassen sich zur Vereinsverwaltung erforderlichen Daten am rationellsten aufbereiten?  Was ist zu tun, damit eine gut ge\"{o}lter Apparat entsteht?  Wie kann dieser Apparat dem Vereinszweck untertan bleiben, statt ihn mit seiner Eigendynamik zu dominieren?  Was l\"{a}sst sich dabei f\"{u}r eine zunkunftsweisende Organisation der ``Informationsgesellschaft'' lernen?
\end{quote}
\filbreak

\item
{\bf {\bf profil.txt}\footnote{http://www.ffii.org/verein/profil.txt}}
\filbreak

\item
{\bf {\bf }\footnote{http://www.ffii.org/verein/ffiiprofil}}
\filbreak
\end{itemize}

Zur Beachtung: Die in diesem Text verwendeten Soft- und Hardware-Bezeichnungen, Firmen- und Markennamen k\"{o}nnen warenzeichen-, marken- oder patentrechtlichem Schutz unterliegen.

Alle Seiten \copyright{} ESR Pollmeier GmbH\footnote{/de/Impressum.html} (Impressum)

Anregungen, Kommentare, Kritik zu www.esr-pollmeier.de/swpat/... an Stefan Pollmeier\footnote{mailto:gl@esr-pollmeier.de?subject=http://swpat.ffii.org/analyse/antworten/pollm/beispiele/index.de.html}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

