\begin{subdocument}{spolmaplik}{Heute angemeldet, morgen Patent?}{http://swpat.ffii.org/analyse/antworten/pollm/anmeldungen/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Ideen, die heute zum Patent angemeldet werden, sollen schlie{\ss}lich auch patentiert werden. An Beispielen von Patentanmeldungen eines gro{\ss}en deutschen Elektrokonzerns, der eine sehr weitgehende Software-Patentierung bef\"{u}rwortet, kann man erkennen wohin die Reise geht.}
Diese Seite enth\"{a}lt Links, die auf Angebote au{\ss}erhalb unserer Website www.esr-pollmeier.de/... verweisen. Die ESR Pollmeier GmbH und ihre Autoren sind f\"{u}r Inhalte au{\ss}erhalb der Website www.esr-pollmeier.de/... nicht verantwortlich, auch wenn von hier per Link darauf verwiesen wird. Inhalte au{\ss}erhalb der Website www.esr-pollmeier.de/... geben nicht unbedingt die Meinung der ESR Pollmeier GmbH wieder, auch wenn von hier per Link darauf verwiesen wird. Dokumente und Zitate auf der Website www.esr-pollmeier.de/... die nicht von der ESR Pollmeier GmbH stammen, geben nicht unbedingt die Meinung der ESR Pollmeier GmbH wieder.

Tip: Sie k\"{o}nnen Patente selbst recherchieren bei DepatisNet\footnote{http://www.depatisnet.de}.

Ideen, die heute zum Patent angemeldet werden, sollen morgen auch patentiert werden - das ist jedenfalls der Wunsch des Anmelders. Hier haben wir einige Patentanmeldungen zusammengestellt, die aus den Jahren 1998 bis 2000 stammen. Sie stammen alle von einem deutschen Elektrokonzern, der f\"{u}r eine sehr weitgehende Patentierung von Software eintritt. An diesen Beispielen von Patentanmeldungen kann man wohl erkennen, was nach Ansicht dieses Anmelders in den n\"{a}chsten Jahren patentiert werden sollte.

\begin{sect}{bizm}{Verfahren f\"{u}r gesch\"{a}ftliche T\"{a}tigkeiten und mathematische Methoden?}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf EP 1 193 624 A1 - Qualit\"{a}tsmanagementverfahren\footnote{EP1193624A1.pdf} (PDF, 984 KB)}}

\begin{quote}
Auf S. 18 schreibt das Europ. Patentamt

\begin{quote}
{\it Die Rechercheabteilung ist der Auffassung, da{\ss} die vorliegende Patentanmeldung den Vorschriften des EP\"{U} ... nicht entspricht, ... Grund: Der Gegenstand der eingereichten Anspr\"{u}che 1-9 f\"{a}llt unter die Bestimmungen des Artikels 52\footnote{http://www.european-patent-office.org/legal/epc/d/ar52.html} (2) und (3) EP\"{U} (Verfahren f\"{u}r gesch\"{a}ftliche T\"{a}tigkeiten als solche, Verfahren f\"{u}r gedankliche T\"{a}tigkeiten als solche).}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf WO 02/01429 A2 - Verfahren zum Versenden von bestellten Artikeln\footnote{WO02\_01429A2.pdf} (PDF, 598 KB)}}

\begin{quote}
Auf S. 15 schreibt das Europ. Patentamt:

\begin{quote}
{\it In Anbetracht dessen, da{\ss} der beanspruchte Gegenstand entweder nur derartige nichttechnische Sachverhalte oder allgemein bekannte Merkmale zu deren technologischen Umsetzung anf\"{u}hrt, stellte der Recherchepr\"{u}fer keine technische Aufgabe fest, deren L\"{o}sung eventuell eine erfinderische T\"{a}tigkeit beinhalten w\"{u}rde.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf WO 01/69417 A2 - Plant Maintenance Technology Architecture\footnote{WO01\_69417A2.pdf} (PDF, 1681 KB)}}

\begin{quote}
Abstract (Seite 1)

An overall plan for providing maintanance and technical services for businesses and plants, as broadly defined, includes generic procedures for the service written as a manual of standard practices. A knowledge base or experience database of data and people is utilized, and both hardware and software tools are selected and used in providing the services.
\end{quote}
\filbreak

\item
{\bf {\bf WO 00/04470 A1 - Kosteng\"{u}nstige Auftragsbearbeitung f\"{u}r Finanztransaktionen\footnote{WO00\_04470A1.pdf} (PDF, 469 KB)}}

\begin{quote}
Das Dokument nennt zwei Patentanspr\"{u}che (S. 5):

\begin{enumerate}
\item
Verfahren zur Bearbeitung von Kundenauftr\"{a}gen an Finanzunternehmen im Rahmen von Home-Banking \"{u}ber ein Kommunikationsnetz, bei dem ein Auftrag beim Kunden und beim Unternehmen gespeichert ist, bei dem eine \"{A}nderung des Auftrags beim Kunden gespeichert wird, bei dem eine Kommunikationsverbindung zum Unternehmen aufgebaut wird und der aktuelle Auftrag beim Kunden und beim Unternehmen miteinander verglichen werden, wobei bei \"{U}bereinstimmung der Auftrag beim Kunden und beim Unternehmen ge\"{a}ndert wird, und ansonsten beim Kunden eine Meldung generiert wird.

\item
Verfahren nach Anspruch 1, bei dem der Vergleich und die \"{A}nderung bei jeder Kommunikationsverbindung durchgef\"{u}hrt werden.
\end{enumerate}
\end{quote}
\filbreak

\item
{\bf {\bf WO 00/34850 A2 - Verfahren und Anordnung zum Entwurf eines technischen Systems\footnote{WO00\_34850A2.pdf} (PDF, 740 KB)}}

\begin{quote}
Zusammenfassung (S. 1):

\begin{quote}
{\it Die Erfindung erm\"{o}glicht den Entwurf eines technischen Systems, das mehrere Zielfunktionen umfa{\ss}t, indem f\"{u}r die Zielfunktionen eine Suchrichtung ermittelt wird, entlang derer eine Verbesserung der Werte der Zielfunktion bewirkt wird. Eine iterative Ermittlung der Suchrichtung f\"{u}hrt zu einem effizienten Arbeitspunkt, der zum Entwurf des technischen Systems geeignet ist.}
\end{quote}

Die Patentanspr\"{u}che 1 bis 12 beschreiben nach unserer Analyse reine mathematische Methoden, Anspruch 13 bezieht sich anscheinend auf eine ``Anordnung'' die in Fig. 3 als Blockschaltbild eines Computers skizziert ist und auf S. 13 f. als solche beschrieben wird.
\end{quote}
\filbreak

\item
{\bf {\bf EP 1 211 619 A2 - Verfahren und Vorrichtung zur mobilen \"{U}berwachung eines Interaktions-Dienstleistungszentrums\footnote{EP1211619A2.pdf} (PDF, 254 KB)}}

\begin{quote}
Patentanspruch (Seite 3)

\begin{quote}
{\it Verfahren in einem Interaktions-Dienstleistungszentrum ..., wobei unter Verwaltung einer zentralen Anlage ... zumindest eine Person (Agent) Anfragen bearbeitet mit folgenden Schritten:
\begin{itemize}
\item
Erfassen von Daten, welche die Bearbeitung der Anfragen oder die Verwaltung der Bearbeitung betreffen

\item
Anzeigen der erfa{\ss}ten Daten

\item
\"{U}berwachen der angezeigten Daten durch eine weitere Person (Supervisor)
\end{itemize}}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf EP 1 211 470 A2 - K\"{a}lteger\"{a}t mit einer Verwaltungseinheit\footnote{EP1211470A2.pdf} (PDF, 574 KB)}}

\begin{quote}
Ein K\"{u}hlschrank, der Nachschub bestellen kann. Die Anmeldung setzt auf einer fr\"{u}heren Anmeldung eines anderen Unternehmens auf:

EP 0 974 798 A2\footnote{EP0974798A2.pdf} (PDF, 249 KB), EP 0 974 798 A3 - Recherchebericht\footnote{EP0974798A3.pdf} (PDF, 52 KB)
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{feld}{Software-Patent-Anmeldungen im Feldbus-, Intranet-, Internet-Umfeld}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf EP 1 199 846 A1 - Verfahren zur automatischen Ger\"{a}tekonfiguration in einem Feldbus-System\footnote{EP1199846A1.pdf} (PDF, 369 KB)}}

\begin{quote}
In einem Feldbus-System, z. B. Profibus-DP, scannt der Master alle Slave-Adressen, um festzustellen, welche Slaves vorhanden sind. Er \"{u}bertr\"{a}gt Parameterdatens\"{a}tze und aktiviert die Slaves. Die ersten beiden Patentanspr\"{u}che (S. 4) beschreiben das so:

\begin{quote}
{\it \begin{enumerate}
\item
Verfahren zur automatischen Ger\"{a}tekonfiguration in einem Feldbus-System, insbesondere in EWSD [Elektronisches W\"{a}hlsystem Digital], mit mehreren Netzwerk-Adressen f\"{u}r mehrere Slave-Stationen, an dem eine Master-Station und mindestens eine Slave-Station mit einer bestimmten Netzwerk-Adresse angeschlossen sind, mit folgende Schritten: Abfragen, ob an einer beliebigen Netzwerk-Adresse eine Slave-Station angeschlossen ist, nach deren Betriebszustand sowie deren Identifikationsnummer; Beantworten der Diagnoseabfrage durch die Slave-Station; Ermitteln der Konfigurationsdaten der Slave-Station; Aufbauen eines Parametersatzes der Slave-Station; Aktivieren der Slave-Station durch \"{U}berf\"{u}hren in den Nutzdatenaustausch.

\item
Verfahren nach dem vorstehenden Anspruch, dadurch gekennzeichnet, da{\ss} als Feldbus-System ein Profibus-DP, als Master-Station ein Profibus-DP-Master und als Slave-Station ein Profibus-DP-Slave verwendet werden.
\end{enumerate}}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf EP 1 187 011 A2 - Verfahren zur Programmierung eines Steuerger\"{a}ts\footnote{EP1187011A2.pdf} (PDF, 216 KB)}}

\begin{quote}
Programmierung eines Ger\"{a}ts z. B. \"{u}ber den CAN-Bus (Fig. 2); wenn das Ger\"{a}t kurz nach dem Einschalten eine spezielle Nachricht empf\"{a}ngt, dann schaltet es in einen Modus um, in dem es ganz oder teilweise neu programmiert werden kann.

Der 1. Patentanspruch beschreibt das so:

\begin{quote}
{\it Verfahren zur Programmierung eines Steuerger\"{a}tes, insbesondere eines Kraftfahrzeuges, welches durch ein extern dem Steuerger\"{a}t zugef\"{u}hrtes Signal von einem Betriebsmode in einen Programmiermode umgeschaltet wird, dadurch gekennzeichnet, da{\ss} das externe Signal vor der Systeminitialisierung des Steuerger\"{a}ts, in welcher mindestens ein Funktionsprogramm aufgerufen wird, an das Steuerger\"{a}t gesendet wird, wodurch die Ausf\"{u}hrung des Funktionsprogramms unterbunden und ein neuer Steuer-/und/oder Regelablauf und/oder neue Daten \"{u}bertragen und gespeichert werden.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf WO 00/11552 A2 - System, Verfahren und Steuerungsvorrichtung zur Erzeugung einer Meldung als E-Mail \"{u}ber Internet und/oder Intranet\footnote{WO00\_11552A2.pdf} (PDF, 377 KB)}}

\begin{quote}
Steuerung (SPS, NC, RC) sendet und empf\"{a}ngt E-Mails
\end{quote}
\filbreak

\item
{\bf {\bf DE 198 48 618 A1 - System und Verfahren zur Fernwartung und/oder Ferndiagnose eines Automatisierungssystems mittels E-Mail\footnote{DE19848618A1.pdf} (PDF, 237 KB)}}

\begin{quote}
Fernwartung, -diagnose, indem ein Automatisierungssystem E-Mails \"{u}ber eine Firewall hinweg sendet und empf\"{a}ngt
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{mikr}{Software-Patent-Anmeldungen f\"{u}r kleinere Software-Bausteine, -Konzepte}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf EP 1 065 596 A1 - process for organising data records in computer memory\footnote{EP1065596A1.pdf} (PDF, 345 KB)}}
\filbreak

\item
{\bf {\bf WO 00/54147 A2 - Automatisierungssystem mit Automatisierungsobjekten mit Verzeichnisstruktur und Verfahren zur Verwaltung von Automatisierungsobjekten in einer Verzeichnisstruktur\footnote{WO00\_54147A2.pdf} (PDF, 371 KB)}}
\filbreak

\item
{\bf {\bf WO 01/09725 A2 - Verfahren zum Aufbau eines Bildes f\"{u}r eine Prozessvisualisierung\footnote{WO01\_09725A2.pdf} (PDF, 298 KB)}}
\filbreak

\item
{\bf {\bf EP 1 211 588 A1 - Verfahren zum Nutzen einer Datenverarbeitungsanlage abh\"{a}ngig von einer Berechtigung, zugeh\"{o}rigen Datenverarbeitungsanlage und zugeh\"{o}riges Programm\footnote{EP1211588A1.pdf} (PDF, 710 KB)}}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{itemize}
\item
{\bf {\bf Einf\"{u}hrung}\footnote{http://swpat.ffii.org/analyse/antworten/pollm/einfuehrung/index.de.html}}

\begin{quote}
Kommentierte Einf\"{u}hrung mit vielen Verweisen auf allgemeine Informationen zu Patenten und zur aktuellen Software-Patent-Diskussion.
\end{quote}
\filbreak

\item
{\bf {\bf Beispiele}\footnote{http://swpat.ffii.org/analyse/antworten/pollm/beispiele/index.de.html}}

\begin{quote}
Beispiele f\"{u}r sehr allgemeine Software-Patente und f\"{u}r Software-Patente, die f\"{u}r die Automatisierungstechnik relevant sind. Spezieller Abschnitt zu Feldbus-Patenten.
\end{quote}
\filbreak

\item
{\bf {\bf Heute angemeldet, morgen Patent?}\footnote{}}

\begin{quote}
Ideen, die heute zum Patent angemeldet werden, sollen schlie{\ss}lich auch patentiert werden. An Beispielen von Patentanmeldungen eines gro{\ss}en deutschen Elektrokonzerns, der eine sehr weitgehende Software-Patentierung bef\"{u}rwortet, kann man erkennen wohin die Reise geht.
\end{quote}
\filbreak

\item
{\bf {\bf Statement und Forderungen}\footnote{http://swpat.ffii.org/analyse/antworten/pollm/statement/index.de.html}}

\begin{quote}
Nach ausf\"{u}hrlicher Analyse der derzeitigen Patentierungspraxis, des EU-Richtlinien-Vorschlags, der Literatur, vieler Beispiel-Patente und -Patent-Anmeldungen, nach ausf\"{u}hrlichen Diskussionen mit Bef\"{u}rwortern und Gegnern der Software-Patentierung haben wir ein Statement und die daraus resultierenden Forderungen erarbeitet.
\end{quote}
\filbreak

\item
{\bf {\bf Pl\"{a}doyer f\"{u}r die Anwendung eines klaren Technik-Begriffs (PDF, 245 KB)}}

\begin{quote}
Folien zu einem Vortrag beim ZVEI GA Kommunikation in der Automatisierungstechnik am 05.06.02
\end{quote}
\filbreak
\end{itemize}

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Patentbewegungs-Aktivit\"{a}ten im Namen von Siemens\footnote{http://swpat.ffii.org/akteure/siemens/index.de.html}}}

\begin{quote}
Siemens war insbesondere in Deutschland schon immer an der Spitze der Patentbewegung.  Dennoch behaupten die Vertreter der Siemens-Patentabteilung, die in Verb\"{a}nden und regierungsnahen Organisationen f\"{u}r die Ausweitung der Patentierbarkeit eintreten, nicht ernsthaft, dass die Patentflut dem technischen Fortschritt diene oder f\"{u}r das Gesch\"{a}ftsmodell von Siemens notwendig sei.  Sie betonen lediglich, dass Europa den von den USA geschaffenen globalen Regeln zu folgen habe.  Nicht wenige Verantwortungstr\"{a}ger im technischen und gesch\"{a}ftlichen Bereich von Siemens st\"{o}hnen derweil \"{u}ber die bremsende Wirkung, welche von der Patentflut ausgeht und die Branche ebenso wie die gesch\"{a}ftlichen Aktivit\"{a}ten von Siemens oft schmerzhaft beeintr\"{a}chtigt.  Aber diese Leute sind nicht berechtigt, im Namen von Siemens \"{u}ber ``Patentfragen'' sprechen.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi

Zur Beachtung: Die in diesem Text verwendeten Soft- und Hardware-Bezeichnungen, Firmen- und Markennamen k\"{o}nnen warenzeichen-, marken- oder patentrechtlichem Schutz unterliegen.

Alle Seiten \copyright{} ESR Pollmeier GmbH\footnote{/de/Impressum.html} (Impressum)

Anregungen, Kommentare, Kritik zu www.esr-pollmeier.de/swpat/... an Stefan Pollmeier\footnote{mailto:gl@esr-pollmeier.de?subject=http://swpat.ffii.org/analyse/antworten/pollm/anmeldungen/index.de.html}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

