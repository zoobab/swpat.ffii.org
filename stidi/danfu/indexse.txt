Wir: Vad är ett mjukvarupatent?
Dcc: Diese Frage lässt sich auf unterschiedliche Art beantworten:
EWe: Ein Software-Patent ist ein Patent auf eine %(e:pseudo-technische Lehre) (Pseudo-Erfindung), die sich in Wirklichkeit in einer %(e:Rechenregel) (Algorithmus), ausdrückbar durch ein paar Zeilen Programmtext, erschöpft.  Der Programmtext kann normalerweise viel kürzer sein als der Patentanspruch.  Etwa lässt sich die Lehre des RSA-Patentes in einer Programmierzeile ausdrücken.  Dabei wird möglicherweise auf Funktionalitäten (Softwarebibliotheken, Hardware) zurückgegriffen, die nicht patentbegründend (z.B. nicht offenbart, nicht neu, naheliegend) sind.  Eine echte %(e:technische Lehre), wie z.B. die zur Ausführung einer neuartigen chemische Reaktion, lässt sich zwar auch programmiersprachlich formulieren, aber die %(e:Erfindung) liegt nicht in der Rechenregel selber sondern im empirischen Wissen darüber, dass diese Regel sich erfolgreich auf die %(e:Welt der beherrschbaren Naturkräfte) anwenden lässt.
EWn: Ein Software-Patent ist ein Patent, welches darauf abzielt, die Verbreitung von Datenverarbeitungsprogrammen zu untersagen.  Bei einem Software-Patent richtet sich das Verwertungsinteresse des Patentinhabers auf das Datenverarbeitungsprogramm als solches und nicht etwa auf ein von diesem Programm trennbares Verfahren (z.B. Verfahren zur Erzeugung einer materiellen Ware).  Das Softwarepatent dient dazu, seinem Inhaber Anteile an der Wertschöpfung im Softwarelizenzgeschäft zu sichern.  Softwarepatente stehen in Konkurrenz zum Software-Urheberrecht.
Dlz: Die Softwarepatente sind eine Unterklasse der %(e:Verfahrenspatente).  Grundsätzlich lassen sich alle automatisierbaren Verfahren als Algorithmen darstellen und durch Computerprogramme steuern.  Um ein Softwarepatent handelt es sich dann, wenn sie darin enthaltene Handlungsanweisung sich an den Programmlogik-Fachmann (Informatiker) -- und nicht etwa an den Naturkräfte-Fachmann (Techniker) oder den Betriebswirt -- richtet und ihn auf seinem Fachgebiet etwas neues lehrt.  Laut europäischem Rechtsverständnis sind nur technische Verfahren patentierbar: es muss eine %(q:Lehre zum technischen Handeln), d.h. zum %(e:planmäßigen Handeln unter Einsatz beherrschbarer Naturkräfte zur unmittelbaren Herbeiführung eines kausal übersehbaren Erfolges) vorliegen.  Computerprogramme und Geschäftsverfahren gehören nach diesem Rechtsverständnis nicht zu den %(e:technischen Erfindungen), und in den geltenden Gesetzen (Art 52 EPÜ, §1 PatG) werden sie ausdrücklich als Beispiele für %(e:Nicht-Erfindungen im Sinne des Patentrechts) genannt.
Ids: In einem weiteren Sinne lassen sich immaterielle Innovationsleistungen, vom BGH auch %(e:Organisations- und Rechenregeln) genannt, als %(q:Software) beschreiben.  Zwischen %(q:Hardware) und %(q:Software), %(q:Logikalien) und %(q:Physikalien), %(q:Welt der Dinge) und %(q:Welt des Geistes), %(q:Lehre zum technischen Handeln) und %(q:Anweisung an den menschlichen Geist), %(q:materiellen) und %(q:immateriellen) Verfahren liegt ein kategorialer Unterschied mit starken praktischen Auswirkungen.  Zwischen %(e:Geschäftsverfahren) und %(e:Datenverarbeitungsprogrammen) hingegen lässt sich schwer eine Grenze etablieren.  Die meisten Softwarepatente sind wie Geschäftsverfahrenspatente formuliert: sie beanspruchen eine nach außen sichtbaren Funktionsablauf.  Geschäftsverfahren wiederum werden bevorzugt in den Kategorien der Informatik beschrieben und laufen in der Praxis als Datenverarbeitungsprogramme auf Universalrechnern ab.  Häufig sind sie zudem Bestandteil von betrieblicher Software, die zugleich Gegenstand des Urheberrechts und des urheberrechtsbedingten Lizenzgeschäfts ist.
LkK: Lektüre
Vie: Verdient die Investition eines Programmierers denn keinen Schutz?
DDW: Doch, durchaus.  Dazu gibt es u.a. das Urheberrecht.
Ivu: Ist das Urheberrecht nicht viel zu leicht zu umgehen, um wirklich nützlich zu sein?
Nei: Nein.
DuW: Das Urheberrecht verhindert, dass jemand ohne meine Zustimmung mein Programm kopiert oder in plumper Weise abschreibt.  Es verbietet Kopieren, erlaubt aber Kapieren und Nachbauen.
Sbi: Solches Nachbauen ist aber ähnlich aufwendig wie das Schreiben selber.
Iru: Insbesondere dann, wenn ich, wie bei kommerzieller Software üblich, den Quelltext meines Programms nicht freigebe, haben es Nachahmer sehr schwer.  Dekompilieren ist mühsam und liefert nur einen undurchdringlichen Zahlensalat.  Den verstehen zu wollen, lohnt sich überhaupt nicht.
Aoe: Aber selbst Firmen wie Trolltech, die gut lesbaren Quelltext mitliefern, können dank Urheberrecht noch von hohen Lizenzgebühren leben, denn das Studium des Quelltextes ist aufwendig, und ein Konkurrent investiert seine Zeit meistens besser, indem er von vorneherein etwas eigenständiges entwickelt.
Sjs: Softwarepatente versuchen diejenigen Lücken zu füllen, welche das Urheberrecht aus guten Gründen (Ausdrucksfreiheit, Wettbewerbsförderung etc) frei gelassen hat.
Mbe: Mehr zum Urheberrecht findet sich unter
Sei: Softwareentwicklung ist doch teuer.  Manche Großprojekte kosten hunderte von Mannjahren.  In der Industrie werden solche Investitionen mit Patenten abgesichert.  Wenn Informatiker sich mit dem Urheberrecht zufriedengeben müssen, wie kann das Software-Engineering denn dann jemals ein industrielles Niveau erreichen?
GWr: Gerade die Investitionen in ein informatisches Großprojekt werden durch das Urheberrecht am besten geschützt.
Ans: Angenommen, wir wollen ein neues Office-Paket programmieren.  Dann müssen wir in jedem Falle zig Mannjahre veranschlagen, egal wie viel Inspiration wir uns von existierenden Vorbildern holen.  Die vielen kleinen Innovationsschritte, die bis zum ausgereiften Paket und vielleicht zu 1000 Patentanmeldungen führen könnten, erfordern jeweils fuer sich genommen nur wenige Minuten oder Stunden an geistiger Arbeit.  Da ist eine Neuerfindung meistens weniger aufwendig als der Versuch, anderer Leute Programme zu kapieren.
GoW: Große Softwareprojekte sind wie monumentale symphonische Werke.  Die Schwierigkeit liegt nicht im Ersinnen einer Melodie, einer Akkordfolge oder einer bestimmten Orchestrierungstechnik.   Es kommt darauf an, dass alle Teile zusammen stimmen und ein wohlgeformtes Gesamtwerk ergeben.  Wenn Haydn einzelne Elemente seiner Musik patentiert hätte, hätten Mozart und Beethoven keine eigenen Symphonien mehr schreiben können.  Gerade für ein komplexes Großprojekt entstehen untragbare Belastungen, wenn man alle Einzelteile patentieren oder auf mögliche Patente Dritter abklopfen muss.
Dsr: Das Urheberrecht passt doch nur für Werke wie Goethes Faust!
Esr: Eine alte und besonders zählebige Patentanwaltsmär, die sich bis in neueste %(lb:Regierungsgutachten) hinein hält.
Sbo: Sicherlich passt das Urheberrecht besonders gut für Sprachwerke, bei denen die schöpferische Leistung weitgehend in der Ausdrucksform liegt, wie dies etwa bei der Wortkunst (Poesie) der Fall ist.
Aho: Aber es findet ebenso bei Gebrauchsanweisungen, Landkarten, Musikpartituren, Gemälden, Pantomime und vielem mehr Anwendung, wo die Ausdrucksform zum Teil hinter der Funktion zurücktritt oder außerhalb der Information liegt.  Bei Musikwerken ist auch der Klangeindruck, der am Ohr ankommt, mit vom Urheberrecht umfasst.  Auch bei Programmen (und Gebrauchsanweisungen) kann das %(e:Verhalten des Programms) mit eingeschlossen sein.  Die Umsetzung der Funktionalität ist gerade bei Programmen nicht deterministisch vorgegeben sondern trägt genügend Züge individuell-schöpferischer Kunstfertigkeit und Fantasie, um dem Urheberrecht zugänglich zu sein.
MWa: Mit dem flexiblen Prinzip, dass nur individuelle Schöpfungen dem Urheberrecht unterliegen, kann das Urheberrecht in zahlreichen unterschiedlichen Bereichen eine vernünftige, den Besonderheiten des jeweiligen Gegenstandes angespasste Balance zwischen den Rechten des Urhebers und den Freiheitsbedürfnissen der anderen.
Giu: Gibt es nicht auch im Bereich der Informatik wirklich großartige Ideen, die nach einem stärkeren Schutz verlangen als ihn das Urheberrecht bieten kann?
MWe: Manche hochspezialisierten mathematischen Rechenregeln (Algorithmen, z.B. im Bereich der linearen Optimierung, Kompression, Verschlüsselung, Bildverarbeitung u.a.) sind tatsächlich die Frucht relativ intensiver Bemühungen.  Wer solche Algorithmen nachahmt, muss u.U. erheblich weniger Aufwand treiben als der Erstentwickler.  Manche Erstentwickler ärgern sich in solchen Fällen über zu schnelle Nachahmung und hätten gerne ein Schutzrecht, welches über das Urheberrecht hinausgeht.
HWt: Hieraus folgt allerdings noch nicht, dass ein solches Schutzrecht gut funktionieren und die Innovation im Bereich hochspezialisierter mathematischer Rechenregeln fördern würde.  Um diese Frage zu beurteilen, muss man das Gesamtsystem des informatischen Schaffens betrachten.  Alles deutet darauf hin, dass zumindest Softwarepatente auch dann mehr Schaden als Nutzen stiften würden, wenn man sie wirksam auf den kleinen Teilbereich der hochspezialisierten Mathematik begrenzen könnte.
Eti: Es bleibt dann immer noch die Frage offen, ob sich ein spezialisiertes Besitzrecht für bestimmte Algorithmen schaffen ließe und wie dies beschaffen sein sollte.  Diese Frage ist immer wieder von klugen Köpfen diskutiert worden.  Ein solches %(q:maßgeschneidertes Algorithmen-Besitzrecht) hat nur dann eine Chance, wenn auf Softwarepatente verzichtet wird.
Wbn: Was ist so besonders an Software?
EWW2: Enthält einen Abschnitt zu diesem Thema
Sop: Seit wann ist Software in Europa patentierbar?
LtS: Laut Gesetzeslage Anfang 2002 dürfte es in Europa keine Softwarepatente geben.
Sht: Seit ca. 1986 erteilt das Europäische Patentamt jedoch Patente auf Prozesse, bei denen die innovative Leistung in einer Organisations- und Rechenregel besteht.
Smg: Seit 1998 gewährt das Europäische Patentamt außerdem Ansprüche auf %(q:Computerprogrammprodukte) und %(q:Computerprogramme).
Iea: In den USA fanden beide Dammbrüche jeweils ca 4 Jahre früher statt.
Ijh: In Deutschland beugte sich der Bundesgerichtshof (BGH) mit ca 6 Jahren Verspätung dem Druck des EPA und seiner Freunde, die bis heute den öffentlichen Diskurs in der patentjuristischen Fachwelt beherrschen.  Die Computerprogramm-Ansprüche des EPA werden bislang nicht umgesetzt.  Dies stellte ein %(bp:Urteil des Bundespatentgerichts vom August 2000) klar, welches jedoch im Oktober 2001 vom BGH unter Hinweis auf das EPA-Vorbild %(bg:umgestoßen) wurde.  Etwa gleichzeitig wurde die EPA-Praxis durch neue Prüfungsrichtlinien %(gl:kodifiziert) und, z.T. auch ohne gerichtlichen Segen, von den Patentämtern Großbritanniens und Frankreichs eingeführt.  Dabei wartete man nicht das Ende der offiziell einberaumten %(ek:europäischen Konsultation) über dieses Thema ab.
LoF: Laut EPÜ sind doch nur %(q:Programme als solche) von der Patentierung ausgeschlossen, und an dieser vagen Formulierung zeigt sich, dass der Gesetzgeber 1973 nicht genau wusste, was er wollte, und daher dem EPA die Freiheit geben wollte, die Regeln im Lichte neuer technischer Entwicklungen allmählich zu ändern, nicht wahr?
Flc: Falsch.
Aem: Ausgeschlossen sind %(q:Programme für Datenverarbeitungsanlagen) ebenso wie %(q:ästhetische Formschöpfungen), %(q:mathematische Methoden) u.a.  Der Hinweis in Art 52(3), diese Gegenstände seien nur insoweit von der Patentierbarkeit ausgeschlossen, wie sie als solche beansprucht werden, bezieht sich auf all diese Gegenstände gleichermaßen.  D.h. ein Regenschirm mit einem neuen Bespannungsmuster ist nicht patentierbar, weil die angebliche Erfindung in einer ästhetischen Formschöpfung liegt.  Eine chemisches Verfahren zur Herstellung schöner Muster auf Regenschirmen ist hingegen patentierbar, da es sich hierbei nicht um eine ästhetische Formschöpfung als solche handelt.  Genau so ist auch zwischen technischen Erfindungen und Organisations- und Rechenregeln zu differenzieren.  Art 52(3) hält lediglich dazu an, hier differenzierend vorzugehen.
Doh: Der Gesetzgeber wusste 1973 genau was er tat, da die wesentlichen Entwicklungen der Informatik damals bereits abgeschlossen waren und da der damaligen Gesetzgebung eine 10 Jahre lange intensive Diskussion über die Grenzen der Patentierbarkeit vorausgegangen war.
Dbt: Die Freiheit der Patentämter, die Grenzen der Patentierbarkeit %(q:neuen technischen Entwicklungen) anzupassen, beschränkt sich auf den Bereich der %(e:beherrschbaren Naturkräfte).  Es stellt sich z.B. die Frage, ob die Gesetze der Biologie beherrschbar sind, ob Information eine Naturkraft ist.  Letzteres lässt sich nicht bejahen.
Wrk: Weitere Lektüre
DaW: Diese Antwortensammlung hat erst gerade begonnen.
Emn: Es gibt aber einige weitere Dokumente, die z.T. den Charakter einer Antwortensammlung aufweisen.
Diu: Dabei sind zweierlei Arten zu unterscheiden:
Aag: Einführende Antworten auf Fragen wissbegieriger Nichtfachleute
WWa: Widerlegungen von Behauptungen der %(pm:Patentbewegung)
Dne: Die meisten der folgenden Dokumente gehören ganz oder teilweise einer dieser beiden Kategorien an:

# Local Variables:
# mailto: mlhtimport@a2e.de
# login: phm
# passwd: XXXX
# srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el
# feature: swpatdir
# doc: swpatdanfu
# txtlang: de
# coding: utf-8
# End:

# Local Variables:
# mailto: mlhtimport@a2e.de
# login: swpatgirzu
# passwd: XXXX
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpatdir.el
# feature: swpatdir
# doc: swpat
# txtlang: de
# coding: utf-8
# End:
