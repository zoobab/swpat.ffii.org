<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Häufige Fragen und Populäre Irrtümer

#descr: Die Diskussion in diversen Foren zum Thema Softwarepatente kreist periodisch um die selben Fragen.  Seit langem ist eine systematische Antwortensammlung fällig.

#Wir: Was ist ein Software-Patent?

#WrW: Wer will Softwarepatente? Wer ist dagegen?

#WwW: Was ist so schlimm an Softwarepatenten?  Warum sollten Software-Innovationen nicht ebenso patentierbar sein wie andere Innovationen, wenn sie wirklich neu und erfinderisch sind?

#Vie: Verdient die Investition eines Programmierers denn keinen Schutz?

#Ivu: Ist das Urheberrecht nicht viel zu leicht zu umgehen, um wirklich nützlich zu sein?

#Sei: Softwareentwicklung ist doch teuer.  Manche Großprojekte kosten hunderte von Mannjahren.  In der Industrie werden solche Investitionen mit Patenten abgesichert.  Wenn Informatiker sich mit dem Urheberrecht zufriedengeben müssen, wie kann das Software-Engineering denn dann jemals ein industrielles Niveau erreichen?

#Dsr: Das Urheberrecht passt doch nur für Werke wie Goethes Faust!

#Giu: Gibt es nicht auch im Bereich der Informatik wirklich großartige Ideen, die nach einem stärkeren Schutz verlangen als ihn das Urheberrecht bieten kann?

#Wbn: Was ist so besonders an Software?

#Sop: Seit wann ist Software in Europa patentierbar?

#LoF: Laut EPÜ sind doch nur %(q:Programme als solche) von der Patentierung ausgeschlossen, und an dieser vagen Formulierung zeigt sich, dass der Gesetzgeber 1973 nicht genau wusste, was er wollte, und daher dem EPA die Freiheit geben wollte, die Regeln im Lichte neuer technischer Entwicklungen allmählich zu ändern, nicht wahr?

#Wrk: Weitere Lektüre

#Dcc: Diese Frage lässt sich auf unterschiedliche Art beantworten:

#Rlp: Richard Stallman, founder of the GNU project and speaker of the League for Programming Freedom explains in easy-to-understand terms to a hearing at the US Patent Office in 1994 why the extension of the patent system to software / algorithms is harmful to all software development, no matter whether free or proprietary, and why copyright provides a fairly adequate framework for both.  This speech is very clear in explaining some of the basic issues that often cause confusion.

#EWn: Ein Software-Patent ist ein Privileg, welches es seinem Inhaber erlaubt, die Verbreitung von Datenverarbeitungsprogrammen zu untersagen.  Bei einem Software-Patent richtet sich das Verwertungsinteresse des Patentinhabers auf das Datenverarbeitungsprogramm als solches und nicht etwa auf ein von diesem Programm trennbares Verfahren (z.B. Verfahren zur Erzeugung einer materiellen Ware).  Das Softwarepatent dient dazu, seinem Inhaber Anteile an der Wertschöpfung im Softwarelizenzgeschäft zu sichern.  Softwarepatente stehen in Konkurrenz zum Software-Urheberrecht.

#Bfs: Beginnt mit einer Definition des Begriffs %(q:Softwarepatent)

#eWW: eingehende Erklärung der traditionellen Lehre vom Unterschied zwischen technischen Erfindungen und Organisations- und Rechenregeln

#eWa: Beispiele von Softwarepatenten (Logikpatenten)

#Aan: Dagegen

#Pro: Dafür

#1ae: 117000 Unterzeichner (vor allem Programmierer, größte bisherige Online-Petition)

#8ru: 800 Vorstände von IT-Unternehmen, meist KMU aber auch einige Großunternehmen

#3ri: 300 Förderer der Eurolinux-Allianz

#Fne: Frankreich

#Rke: Regierung Frankreichs (Kabinettsbeschluss)

#Sas: Sozialistische Partei Frankreichs (Parteiprogramm)

#Acn: Alle Präsidentschaftskandidaten Frankreich 2002

#Hla: Holland

#Slk: Sozialdemokraten

#fie: für IT zuständige Abgeordnete aller Parteien

#Dth: Deutschland

#foW: für IT zuständige Abgeordnete aller Parteien im deutschen Bundestag

#PcD: Parteitagsbeschlüsse von FDP und Grünen

#szk: sonstige vereinzelte Politiker-Stellungnahmen

#Are: Ausschuss der Regionen der Eur Union

#Eko: EUK Generaldirektion Informationsgesellschaft

#LWh: Liikanen wurde hart bearbeitet, unter ihm sind alle unglücklich, ähnlich sieht es beim BMWi aus

#Knl: Konsens der (angesehenen) Programmierer (s. auch Umfragen hierzu)

#KWn: Konsens der Volkswirte (alle Studien urteilen neutral bis negativ)

#ePe: einige Patentrechtler

#zce: zahlreiche Patentprüfer

#1ns: 17. Senat des BPatG

#sjD: sonstige Richter, auch am BGH und EPA, die inzwischen die Problematik des Dammbruchs der 90er Jahre erkannt haben

#NgW: Normalbürger, die sich ohne näheres Hinsehen zunächst wünschen, dass jede intellektuelle Leistung irgendwie belohnt werden sollte (auch %(q:Belohnungstheorie) genannt) und dass der Staat sicherstellen sollte, dass sich nicht irgendwelche Großkonzerne die Früchte der Arbeit des kleinen Mannes aneignen

#Dct: Das Präsidium der Gesellschaft für Informatik sowie einige Informatiker insbesondere an Hochschulen, die sich für ihren Bereich ähnliche Wertschöpfungsmöglichkeiten wünschen, wie man sie aus dem Ingenieurwesen kennt, und die daher %(q:Software-Engineering), %(q:Datentechnik) etc beschwören und Hoffnungen in Patente setzen

#Pee: Patentrechtsexperten im diversen Organisationen

#aet: ausschließlich auf ihr Patent-Credo gestützt, ohne betriebs- oder gar volkswirtschaftliche Argumentation, aber die Flagge ihrer Organisation hissend

#Vve: Vertreter von Patentämtern und Patentreferaten in Regierungen

#Esm: Europäische Kommission

#ejj: eigentlich nur die federführende Geraldirektion Binnenmarkt, dort auch weniger Kommissar Bolkestein als die von Patentjuristen geleitete Dienststelle für Gewerblichen Rechtschutz

#Ehd: Einige akademische Denkfabriken der Patentbewegung

#eig: explizite aggressive Monopolstrategie gegen GNU/Linux

#Iut: Im Hardwarebereich beheimatete Firmen mit großer Patentabteilung wie %(IBM) und einige Telekommunikationsriesen, deren politisch allein aktive Patentabteilung ohne Softwarepatente ihre Bedeutung verlieren würde (meist ohne dass dies allerdings dem Unternehmen als ganzem schaden würde).

#Hlb: die Hochschul-Patentbewegung

#Med: Manche Rechtstitel-Buchhalter und Börsenanalysten

#Hbi: Hartmut Pilch responds to the above question, explains why the broadness objection directly leads to the requirement that there be a new and non-obvious teaching about cause-effect relations of controllable forces of nature.

#DDW: Doch, durchaus.  Dazu gibt es u.a. das Urheberrecht.

#Nei: Nein.

#DuW: Das Urheberrecht verhindert, dass jemand ohne meine Zustimmung mein Programm kopiert oder in plumper Weise nachahmt.  Es verbietet Kopieren, erlaubt aber Kapieren und unabhängiges Nachbauen.

#Sbi: Solches Nachbauen ist aber ähnlich aufwendig wie das Schreiben selber.

#Iru: Insbesondere dann, wenn ich, wie bei kommerzieller Software üblich, den Quelltext meines Programms nicht freigebe, haben es Nachahmer sehr schwer.  Dekompilieren ist mühsam und liefert nur einen undurchdringlichen Zahlensalat.  Den verstehen zu wollen, lohnt sich überhaupt nicht.

#Aoe: Aber selbst Firmen wie Trolltech, die gut lesbaren Quelltext mitliefern, können dank Urheberrecht noch von hohen Lizenzgebühren leben, denn das Studium des Quelltextes ist aufwendig, und ein Konkurrent investiert seine Zeit meistens besser, indem er von vorneherein etwas eigenständiges entwickelt.

#Sjs: Softwarepatente versuchen diejenigen Lücken zu füllen, welche das Urheberrecht aus guten Gründen (Ausdrucksfreiheit, Wettbewerbsförderung etc) frei gelassen hat.

#Mbe: Mehr zum Urheberrecht findet sich unter

#GWr: Gerade die Investitionen in ein informatisches Großprojekt werden durch das Urheberrecht am besten geschützt.

#Ans: Angenommen, wir wollen ein neues Office-Paket programmieren.  Dann müssen wir in jedem Falle zig Mannjahre veranschlagen, egal wie viel Inspiration wir uns von existierenden Vorbildern holen.  Die vielen kleinen Innovationsschritte, die bis zum ausgereiften Paket und vielleicht zu 1000 Patentanmeldungen führen könnten, erfordern jeweils fuer sich genommen nur wenige Minuten oder Stunden an geistiger Arbeit.  Da ist eine Neuerfindung meistens weniger aufwendig als der Versuch, anderer Leute Programme zu kapieren.

#GoW: Große Softwareprojekte sind wie monumentale symphonische Werke.  Die Schwierigkeit liegt nicht im Ersinnen einer Melodie, einer Akkordfolge oder einer bestimmten Orchestrierungstechnik.   Es kommt darauf an, dass alle Teile zusammen stimmen und ein wohlgeformtes Gesamtwerk ergeben.  Wenn Haydn einzelne Elemente seiner Musik patentiert hätte, hätten Mozart und Beethoven keine eigenen Symphonien mehr schreiben können.  Gerade für ein komplexes Großprojekt entstehen untragbare Belastungen, wenn man alle Einzelteile patentieren oder auf mögliche Patente Dritter abklopfen muss.

#Esr: Eine alte und besonders zählebige Patentanwaltsmär, die sich bis in neueste %(lb:Regierungsgutachten) hinein hält.

#Sbo: Sicherlich passt das Urheberrecht besonders gut für Sprachwerke, bei denen die schöpferische Leistung weitgehend in der Ausdrucksform liegt, wie dies etwa bei der Wortkunst (Poesie) der Fall ist.

#Aho: Aber es findet ebenso bei Gebrauchsanweisungen, Landkarten, Musikpartituren, Gemälden, Pantomime und vielem mehr Anwendung, wo die Ausdrucksform zum Teil hinter der Funktion zurücktritt oder außerhalb der Information liegt.  Bei Musikwerken ist auch der Klangeindruck, der am Ohr ankommt, mit vom Urheberrecht umfasst.  Auch bei Programmen (und Gebrauchsanweisungen) kann das %(e:Verhalten des Programms) mit eingeschlossen sein.  Die Umsetzung der Funktionalität ist gerade bei Programmen nicht deterministisch vorgegeben sondern trägt genügend Züge individuell-schöpferischer Kunstfertigkeit und Fantasie, um dem Urheberrecht zugänglich zu sein.

#MWa: Mit dem flexiblen Prinzip, dass nur individuelle Schöpfungen dem Urheberrecht unterliegen, kann das Urheberrecht in zahlreichen unterschiedlichen Bereichen eine vernünftige, den Besonderheiten des jeweiligen Gegenstandes angespasste Balance zwischen den Rechten des Urhebers und den Freiheitsbedürfnissen der anderen.

#MWe: Manche hochspezialisierten mathematischen Rechenregeln (Algorithmen, z.B. im Bereich der linearen Optimierung, Kompression, Verschlüsselung, Bildverarbeitung u.a.) sind tatsächlich die Frucht relativ intensiver Bemühungen.  Wer solche Algorithmen nachahmt, muss u.U. erheblich weniger Aufwand treiben als der Erstentwickler.  Manche Erstentwickler ärgern sich in solchen Fällen über zu schnelle Nachahmung und hätten gerne ein Schutzrecht, welches über das Urheberrecht hinausgeht.

#HWt: Hieraus folgt allerdings noch nicht, dass ein solches Schutzrecht gut funktionieren und die Innovation im Bereich hochspezialisierter mathematischer Rechenregeln fördern würde.  Um diese Frage zu beurteilen, muss man das Gesamtsystem des informatischen Schaffens betrachten.  Alles deutet darauf hin, dass zumindest Softwarepatente auch dann mehr Schaden als Nutzen stiften würden, wenn man sie wirksam auf den kleinen Teilbereich der hochspezialisierten Mathematik begrenzen könnte.

#Eti: Es bleibt dann immer noch die Frage offen, ob sich ein spezialisiertes Besitzrecht für bestimmte Algorithmen schaffen ließe und wie dies beschaffen sein sollte.  Diese Frage ist immer wieder von klugen Köpfen diskutiert worden.  Ein solches %(q:maßgeschneidertes Algorithmen-Besitzrecht) hat nur dann eine Chance, wenn auf Softwarepatente verzichtet wird.

#rne: Warning: this question is often used in an attempt to reverse the burden of proof.  The question assumes that the patent system is good by default and that those who want to exclude software must prove that software is different.  Although it is not difficult to argue the difference of software, in doing so you will appear like a philosopher, and the pro-patent lobbyist will utilise that in order to discredit you and position himself as the real down-to-earth businessman.

#EWW2: Enthält einen Abschnitt zu diesem Thema

#Eua: Ein Software-Unternehmer und Publizist erklärt bei einer Anhörung 1994 vor dem US-Patentamt aufgrund prinzipieller und pragmatischer Überlegungen, warum Algorithmen sakrosankt sein müssen.

#Aud: Ausführliche Erklärung und Dokumentation zur der Geist-Materie-Unterscheidung, ohne die das Patentwesen nicht existieren kann.

#LtS: Laut Gesetzeslage Anfang 2002 dürfte es in Europa keine Softwarepatente geben.

#Sht: Seit ca. 1986 erteilt das Europäische Patentamt jedoch Patente auf Prozesse, bei denen die innovative Leistung in einer Organisations- und Rechenregel besteht.

#Smg: Seit 1998 gewährt das Europäische Patentamt außerdem Ansprüche auf %(q:Computerprogrammprodukte) und %(q:Computerprogramme).

#Iea: In den USA fanden beide Dammbrüche jeweils ca 4 Jahre früher statt.

#Ijh: In Deutschland beugte sich der Bundesgerichtshof (BGH) mit ca 6 Jahren Verspätung dem Druck des EPA und seiner Freunde, die bis heute den öffentlichen Diskurs in der patentjuristischen Fachwelt beherrschen.  Die Computerprogramm-Ansprüche des EPA werden bislang nicht umgesetzt.  Dies stellte ein %(bp:Urteil des Bundespatentgerichts vom August 2000) klar, welches jedoch im Oktober 2001 vom BGH unter Hinweis auf das EPA-Vorbild %(bg:umgestoßen) wurde.  Etwa gleichzeitig wurde die EPA-Praxis durch neue Prüfungsrichtlinien %(gl:kodifiziert) und, z.T. auch ohne gerichtlichen Segen, von den Patentämtern Großbritanniens und Frankreichs eingeführt.  Dabei wartete man nicht das Ende der offiziell einberaumten %(ek:europäischen Konsultation) über dieses Thema ab.

#Flc: Falsch.

#Aem: Ausgeschlossen sind %(q:Programme für Datenverarbeitungsanlagen) ebenso wie %(q:ästhetische Formschöpfungen), %(q:mathematische Methoden) u.a.  Der Hinweis in Art 52(3), diese Gegenstände seien nur insoweit von der Patentierbarkeit ausgeschlossen, wie sie als solche beansprucht werden, bezieht sich auf all diese Gegenstände gleichermaßen.  D.h. ein Regenschirm mit einem neuen Bespannungsmuster ist nicht patentierbar, weil die angebliche Erfindung in einer ästhetischen Formschöpfung liegt.  Eine chemisches Verfahren zur Herstellung schöner Muster auf Regenschirmen ist hingegen patentierbar, da es sich hierbei nicht um eine ästhetische Formschöpfung als solche handelt.  Genau so ist auch zwischen technischen Erfindungen und Organisations- und Rechenregeln zu differenzieren.  Art 52(3) hält lediglich dazu an, hier differenzierend vorzugehen.

#Doh: Der Gesetzgeber wusste 1973 genau was er tat, da die wesentlichen Entwicklungen der Informatik damals bereits abgeschlossen waren und da der damaligen Gesetzgebung eine 10 Jahre lange intensive Diskussion über die Grenzen der Patentierbarkeit vorausgegangen war.

#Dbt: Die Freiheit der Patentämter, die Grenzen der Patentierbarkeit %(q:neuen technischen Entwicklungen) anzupassen, beschränkt sich auf den Bereich der %(e:beherrschbaren Naturkräfte).  Es stellt sich z.B. die Frage, ob die Gesetze der Biologie beherrschbar sind, ob Information eine Naturkraft ist.  Letzteres lässt sich nicht bejahen.

#DaW: Diese Antwortensammlung hat erst gerade begonnen.

#Emn: Es gibt aber einige weitere Dokumente, die z.T. den Charakter einer Antwortensammlung aufweisen.

#Diu: Dabei sind zweierlei Arten zu unterscheiden:

#Aag: Einführende Antworten auf Fragen wissbegieriger Nichtfachleute

#WWa: Widerlegungen von Behauptungen der %(pm:Patentbewegung)

#Dne: Die meisten der folgenden Dokumente gehören ganz oder teilweise einer dieser beiden Kategorien an:

#cen: %(q:Save Our Software) oder %(q:BILD dir deine Meinung über Softwarepatente)

#EnW: Eine leicht verständliche Einführung in die Problematik der Softwarepatente, in einer ansprechenden Dialogform geschrieben.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatdanfu ;
# txtlang: de ;
# multlin: t ;
# End: ;

