<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Romeo und Julia und das Neue Zeitalter des Geistigen Eigentums

#descr: %(q:Sollen großartige Programmierleistungen wie Office oder Photoshop
etwa nicht patentwürdig sein?), fragte ein bekannter Künder eines
%(q:Neuen Zeitalters der Eigentumsrechte) bei einer Anhörung im
Berliner Wirtschaftsministerium im Mai 2000.  %(q:Ein Patent auf
%(q:Romeo und Julia) wäre kein Trivialpatent.  Gegen so etwas hätte
ich wenig einzuwenden), meinte ein Beobachter.  Es lohnt sich, diese
beiden Aussagen hier hier mit uns zu Ende zu denken.  Danach werden
Sie gut verstehen, was %(q:Softwarepatente) sind und an welchem
Scheideweg unsere Zivilisation steht.

#Var: Viele Diskussionen über Softwarepatente gehen am Thema vorbei, weil
niemand sich so recht vorstellt, was eigentlich beansprucht werden
soll.

#Irg: Im Falle eines %(q:Rome-und-Julia-Patents) würde es wohl kaum um den
Text von Shakespeare gehen sondern um etwa folgende Patentansprüche:

#SWc: System und Methode zur Erregung von Mitleid und Furcht durch
Vorführung einer dramatischen Handlung, dadurch gekennzeichnet, dass
zwei liebende Parteien L1 und L2, die jeweils einer von zwei
miteinander verfeindeten Organisationen O1 und O2 angehören, ihre
Beziehung geheim halten müssen

#znW: zwei liebende Parteien L1 und L2, die jeweils einer von zwei
miteinander verfeindeten Organisationen O1 und O2 angehören, ihre
Beziehung unter Geheimhaltungsbedingungen GB vor F1 und F2 geheim
halten müssen

#bi1: besagte Geheimhaltungsbedingungen GB so kompliziert werden, dass eine
der liebenden Parteien L1 der Außenwelt den eigenen Tod vortäuschen
muss

#Sde: System und Methode nach Anspruch 1, zusätzlich dadurch gekennzeichnet,
dass die andere liebende Partei L2 der Täuschung erliegt und sich
tötet.

#Sht: System und Methode nach Anspruch 2, zusätzlich dadurch gekennzeichnet,
dass die liebende Partei L1 aus dem vorgetäuschten Todeszustand
zurückkehrt, den Tod von L2 entdeckt und sich ebenfalls tötet

#Ssu: System und Methode nach Anspruch 3, zusätzlich dadurch gekennzeichnet,
dass die Organisationen O1 und O2 unter dem Eindruck des Todes ihrer
Mitglieder L1 und L2 miteinander Frieden schließen.

#SAi: System und Methode nach einem der obigen Ansprüche, dadurch
gekennzeichnet, dass die Organisationen O1 und O2 Familien sind.

#Dsg: Datenträger, auf dem die dramatische Handlung nach einem der obigen
Ansprüche aufgezeichnet ist.

#Dii: Das ist nicht unbedingt komplizierter als die meisten heutigen
Softwarepatent-Ansprüche.

#Woe: Wenn man davon spricht, man wolle %(q:ein Softwareprodukt
patentieren), meint man in etwa obige %(q:Patentierung von Romeo und
Julia).  Natürlich verletzt eine Vorführung des Films %(q:Shakespeare
in Love) die meisten der ca 600 Patentansprüche, und viele Romane
fallen zumindest in den Anspruch 1 (den %(e:Hauptanspruch)). 
Vielleicht gibt es unter den 600 Ansprüchen 30 %(e:unabhängige
Ansprüche), die ähnlich breit sind wie der Hauptanspruch.  In diesem
Falle würde das Patentamt dieses Patent aufspalten und daraus etwa 30
Patente machen.  Ein Werk wie %(q:Romeo und Julia) oder %(q:Photoshop)
schlägt sich normalerweise nicht in einem sondern in vielen Patenten
nieder.

#UnM: Ursprünglich beantragte Shakespeares Patentanwalt sicherlich etwa
folgenden Hauptanspruch:

#Dgj: Die anderen Merkmale des Hauptanspruchs fanden sich in
%(e:Unteransprüchen).  Doch das Patentamt ihrer Majestät Elisabeth
recherchierte im %(e:Stand der Technik) und fand mehrere griechische
Tragödien.  Hierin zeigte sich, wie wunderbar das Patentwesen
funktioniert, wenn man nur den %(e:Stand der Technik) gut genug
%(e:recherchiert).

#NEc: Nach neuesten Erkenntnissen des Europäischen Patentamtes und der
Europäischen Kommission handelt es sich bei der
Romeo-und-Julia-Erfindung nicht ein %(q:Drama als solches) oder eine
%(q:ästhetische Formschöpfung als solche), sondern eine
%(q:ästhetische Formschöpfung mit einem weiteren technischen Effekt),
genauer gesagt um eine %(q:medien-implementierte Erfindung), die einem
%(q:Gebiet der Technik) nach %(tr:TRIPs) angehört und einen
%(e:technischen Beitrag) leistet.

#Erd: Einige lautstarke Kritiker aus den Reihen der Brotlose-Kunst-Bewegung
weisen unermüdlich darauf hin, dass das %(q:Erregen von Mitleid und
Furcht) gemäß dem %(ti:Technikbegriff der 70er Jahre) nicht zu den
%(q:technischen Erfindungen) gehöre, da es sich nicht um einen
%(q:Einsatz beherrschbarer Naturkräfte zur Erreichung eines kausal
übersehbaren Erfolges) handele.  Dieser Technikbegriff wurde jedoch zu
Recht verworfen, denn die Einwirkung auf das menschliche Gemüt ist
hinreichend kalkulierbar und beruht auf materiellen Vorgängen (z.B.
der Ausschüttung von Hormonen).  Für den strengen
naturwissenschaftlichen Determinismus eines Newton gibt es seit
Heisenberg nicht einmal mehr in der Physik viel Raum.  Auch der als
%(e:Kerntheorie) in Verruf geratene Versuch, zwischen einer bereits
bekannten %(e:technischen) Kausalität der Hormonausschüttung und einer
%(e:untechnischen) Logik der Literatur zu unterscheiden, ist nach
jahrelangem Schlingerkurs nunmehr endgültig einer ganzheitlichen
Betrachtung gewichen, welche alleine der Wirklichkeit des modernen
Erfindens gerecht wird.

#Wle: Wir sollten nun einen beherzten Schritt in die Zukunft wagen. Die
Erregung von Mitleid und Furcht gehört zu den Schlüsseltechnologien
des anbrechenden Intellectual Property Zeitalters, in dem Neue Medien
für sich nicht umsonst Mottos wie %(q:powered by emotion) wählen.  Das
Patentwesen würde seinem Auftrag, die Innovation zu fördern,
sicherlich nicht gerecht, wenn milliardenschwere Zukunftsbranchen der
New Economy ausgeschlossen würden.

#Moe: Mehr zum postmodernen Patentwesen entnehmen Sie bitte dem Schrifttum
von Prof. Dr. Dr. Josef Straus und seinen Mitstreitern vom Münchener
%(mp:Max-Planck-Institut für das New Age of IPR).

#Zao: Für die etwas rückständigeren Zeitgenossen bieten sich
Patentgleichnisse an.  Es gibt in dieser bescheidenen Gattung
medien-implementierter Erfindungen immerhin schon folgenden Stand der
Technik (prior art):

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: phm@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatromeo ;
# txtlang: xx ;
# End: ;

