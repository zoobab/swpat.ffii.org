<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: The discussion on software patents in various forums on the Net keeps revolving around the same questions and errors.  A systematic collection of answers is necessary.
title: Frequently Asked Questions and Popular Errors
Wir: What is a Software Patent?
Vie: Don't the investments of an innovatived programmer deserve protection?
Ivu: Isn't copyright far too easy to circumvent to be of any real use?
Sei: Software development is expensive.  Large-scale projects cost hundreds of man-years.  In the hardware engineering world, such investements are secured by patents.  If similar protection isn't available for software engineering, how can this young industry ever achieve industrial strength?
Dsr: Copyright is appropriate only for artistic works like Shakespeare's Hamlet!
Giu: Aren't there some great ideas in the field of computing that deserve a greater reward than what can be offered by copyright?
Wbn: What is so special about software?
Sop: Since when is software patentable in Europe?
WrW: Who wants software patents?  Who is against them
Wrk: Further Reading
Dcc: This question can be answered in various ways:
Rlp: Richard Stallman, founder of the GNU project and speaker of the League for Programming Freedom explains in easy-to-understand terms to a hearing at the US Patent Office in 1994 why the extension of the patent system to software / algorithms is harmful to all software development, no matter whether free or proprietary, and why copyright provides a fairly adequate framework for both.  This speech is very clear in explaining some of the basic issues that often cause confusion.
EWn: A software patentis a patent which aims to forbid the dissemination or execution of computer programs.  The owner of a software patent is interested in capturing revenues from programs as such rather than from material results of production processes that run under program control.  The software patent is designed to secure its owner a right to block other people from distributing or executing their own copyrighted programs, usually used for the purpose of extracting a share in license fee earnings from such programs.  Software patents thus stand in a relation of competition with software copyright.
EWe: In a broad sense, all immaterial innovations, also called %(e:rules of organisation and calculation) by German courts, are referred to as %(q:software).  Between %(q:hardware) and %(q:software), %(q:physics) and %(q:logics), %(q:matter) and %(q:mind), %(q:technical teachings) and %(q:rules of organisation and calculation) etc, there is a sharp borderline: in one case nature is questioned and the only reliable answer is obtained by experimenting with causally determined physical forces, in the other case the human mind is questioned and the only reliable answer is obtained by pure reason, e.g. mathematical proof, or by non-deterministic social science empiry.
Bfs: starts with a definition of the term %(q:software patents)
eWW: explains in-depth the traditional legal philosophy about rules of organisation and calculation vs technical inventions
eWa: examples of real software patents
WwW: If the principal objections to software patents are (1) obviousness and (2) lack of novelty, then why all this ruckus about whether software is patentable per se?
Hbi: Hartmut Pilch responds to the above question, explains why the broadness objection directly leads to the requirement that there be a new and non-obvious teaching about cause-effect relations of controllable forces of nature.
DDW: Indeed they do.  That's why we have copyright.
Nei: No.
DuW: Copyright prevents other people from copying or brutely imitating my program.  It disallows direct rip-off but does allow independent reimplementation.
Sbi: Such independent reimplementation usually requires about as much effort as the first author's creation.
Iru: This is especially true when I, as done in most commercial software, hide the source code of my program by compiling or scrambling.  Decompiling is cumbersome and yields only an impenetrable jungle of numbers which is by far too difficult to wade through.
Aoe: Some companies, such as Trolltech, even manage to publish well documented and readable source code and still charge quite high license fees.  This is because studying other people's source code and then writing something that does not infringe on copyright is not really worthwhile.  It is usually preferable to write an original work in the first place.
Sjs: Software patents try to close those gaps, which copyright has left open for good reasons (freedom of expression, competition, consistency).
Mbe: More about software copyright can be found under
GWr: Investments in large-scale projects are particularly well protected by copyright.
Ans: Let's assume we want to program a new Office package.  Then we should in any case be ready to invest many man-decades, no matter to what degree we draw inspiration from existing programs.  The many creative steps necessary for arriving at a mature software package, together with perhaps 1000 patent applications, demand only a few minutes or hours each.  Reinventing is usually less expensive than trying to copy others' works.
GoW: Large software projects are like monumental symphonic works.  The difficulty does not consist in conceiving a melody or a chord sequence or an orchestration technique, but rather in creating a consistent and well-formed whole.  If Haydn had patented creative elements of his music, Mozart and Beethoven could no longer have written any symphonies.  Largescale projects are particularly adversely affected when the patenting of building blocks is allowed.  The transaction costs involved in negotiating with hundreds of patent owners tend to become prohibitive, so that innovation is actually blocked and less ideas are produced than without patents.
Esr: An old and particularly tenacious patent lawyer myth, which is even found in recent %(lb:government ordered studies) on software patents.
Sbo: No doubt copyright is particularly adapted to language works, whose creative achievement largely consists in their expressive form, as is the case in the art of words (poetry).
Aho: But it is also successfully applied to musical scores, musical sounds, pictures, sculptures, pantomime, geographical maps, scientific articles, operation manuals and cookbooks.  In many of these cases, the verbal expression is less important than the described functionality.  In the case of music, both the instructions (scores) and the resulting sounds are copyrighted.  The same is true of programs, where the %(e:behavior) can be covered together with the verbal expression.  However, such %(q:behavior) tends to become part of de-facto standards or other kinds of hard reality (such as in a GUI or in the behavior taught by an MSWord 6.0 instruction manual) which are then usually no awarded copyright protection.  The limits of copyright protection have been fine-tuned through many court verdicts.  Software copyright tends to be stronger in Europe than in the US, but the common principle is that it extends to the individual creation and stops short of common principles that are used independently by many authors.  Usually all programs of any reasonable complexity carry the mark of individual competence and imagination and therefore enjoy of copyright protection.  Polls in Europe and the US show that most software developpers and companies are quite happy with copyright and the degree of protection which it offers to them.
MWa: By flexibly applying the basic principle, that only individual creation is protected, copyright achieves a fair balance between the interests of the author and the freedom nees of others.  It achieves this balance not only in the fields of artistic expression but also in all fields where complex tissues are created by combining many elements of functional logic.
MWe: Manche hochspezialisierten mathematischen Rechenregeln (Algorithmen, z.B. im Bereich der linearen Optimierung, Kompression, Verschlüsselung, Bildverarbeitung u.a.) sind tatsächlich die Frucht relativ intensiver Bemühungen.  Wer solche Algorithmen nachahmt, muss u.U. erheblich weniger Aufwand treiben als der Erstentwickler.  Manche Erstentwickler ärgern sich in solchen Fällen über zu schnelle Nachahmung und hätten gerne ein Schutzrecht, welches über das Urheberrecht hinausgeht.
HWt: Hieraus folgt allerdings noch nicht, dass ein solches Schutzrecht gut funktionieren und die Innovation im Bereich hochspezialisierter mathematischer Rechenregeln fördern würde.  Um diese Frage zu beurteilen, muss man das Gesamtsystem des informatischen Schaffens betrachten.  Alles deutet darauf hin, dass zumindest Softwarepatente auch dann mehr Schaden als Nutzen stiften würden, wenn man sie wirksam auf den kleinen Teilbereich der hochspezialisierten Mathematik begrenzen könnte.
Eti: Es bleibt dann immer noch die Frage offen, ob sich ein spezialisiertes Besitzrecht für bestimmte Algorithmen schaffen ließe und wie dies beschaffen sein sollte.  Diese Frage ist immer wieder von klugen Köpfen diskutiert worden.  Ein solches %(q:maßgeschneidertes Algorithmen-Besitzrecht) hat nur dann eine Chance, wenn auf Softwarepatente verzichtet wird.
LtS: According to the written law as of early 2002, there should be no software patents in Europe.
Sht: However in 1986 the European Patent Office decided to grant patents for processes whose innovative achievement consists in a rule of organisation and calculation.
Smg: Since 1998 the European Patent Office moreover grants patents for claims to information objects such as %(q:computer program product), %(q:computer program) and %(q:data structure).
Iea: In the US both of these dig breaks occurred about 4 years earlier.
Ijh: In Germany the Federal Court (BGH) gave in to the international pressure (from USPTO + EPO + patent lobby in Germany) six years later, always by breaking the resistance of the 17th senate of the Federal Patent Court, which in august 2000 %(bp:made it clear) once more that EPO jurisdiction is wrong and information objects cannot be claimed.  However in nov 2001 the BGH %(bg:decided) that the EPO argumentation must be followed in Germany also.  The patent offices of France and Great Britain proceded to emulate the EPO at around the same time, even without court support and in the midst of a %(ek:public consultation) which had produced overwhelming and well-reasoned public sentiment against this illegal and anti-constitutional agenda, which was apparently driven by a small but influential group of patent experts, also referred to as %(q:the economic majority) by the European Commission's consultation summary report.
LoF: Laut EPÜ sind doch nur %(q:Programme als solche) von der Patentierung ausgeschlossen, und an dieser vagen Formulierung zeigt sich, dass der Gesetzgeber 1973 nicht genau wusste, was er wollte, und daher dem EPA die Freiheit geben wollte, die Regeln im Lichte neuer technischer Entwicklungen allmählich zu ändern, nicht wahr?
Flc: Falsch.
Aem: Ausgeschlossen sind %(q:Programme für Datenverarbeitungsanlagen) ebenso wie %(q:ästhetische Formschöpfungen), %(q:mathematische Methoden) u.a.  Der Hinweis in Art 52(3), diese Gegenstände seien nur insoweit von der Patentierbarkeit ausgeschlossen, wie sie als solche beansprucht werden, bezieht sich auf all diese Gegenstände gleichermaßen.  D.h. ein Regenschirm mit einem neuen Bespannungsmuster ist nicht patentierbar, weil die angebliche Erfindung in einer ästhetischen Formschöpfung liegt.  Eine chemisches Verfahren zur Herstellung schöner Muster auf Regenschirmen ist hingegen patentierbar, da es sich hierbei nicht um eine ästhetische Formschöpfung als solche handelt.  Genau so ist auch zwischen technischen Erfindungen und Organisations- und Rechenregeln zu differenzieren.  Art 52(3) hält lediglich dazu an, hier differenzierend vorzugehen.
Doh: Der Gesetzgeber wusste 1973 genau was er tat, da die wesentlichen Entwicklungen der Informatik damals bereits abgeschlossen waren und da der damaligen Gesetzgebung eine 10 Jahre lange intensive Diskussion über die Grenzen der Patentierbarkeit vorausgegangen war.
Dbt: Die Freiheit der Patentämter, die Grenzen der Patentierbarkeit %(q:neuen technischen Entwicklungen) anzupassen, beschränkt sich auf den Bereich der %(e:beherrschbaren Naturkräfte).  Es stellt sich z.B. die Frage, ob die Gesetze der Biologie beherrschbar sind, ob Information eine Naturkraft ist.  Letzteres lässt sich nicht bejahen.
Aan: Against
Pro: Pro
1ae: 117000 Unterzeichner (vor allem Programmierer, größte bisherige Online-Petition)
8ru: 800 Vorstände von IT-Unternehmen, meist KMU aber auch einige Großunternehmen
3ri: 300 Förderer der Eurolinux-Allianz
Fne: Frankreich
Rke: Regierung Frankreichs (Kabinettsbeschluss)
Sas: Sozialistische Partei Frankreichs (Parteiprogramm)
Acn: Alle Präsidentschaftskandidaten Frankreich 2002
Hla: Holland
Slk: Sozialdemokraten
fie: für IT zuständige Abgeordnete aller Parteien
Dth: Deutschland
foW: für IT zuständige Abgeordnete aller Parteien im deutschen Bundestag
PcD: Parteitagsbeschlüsse von FDP und Grünen
szk: sonstige vereinzelte Politiker-Stellungnahmen
Are: Ausschuss der Regionen der Eur Union
Eko: EUK Generaldirektion Informationsgesellschaft
LWh: Liikanen wurde hart bearbeitet, unter ihm sind alle unglücklich, ähnlich sieht es beim BMWi aus
Knl: Konsens der (angesehenen) Programmierer (s. auch Umfragen hierzu)
KWn: Konsens der Volkswirte (alle Studien urteilen neutral bis negativ)
ePe: einige Patentrechtler
zce: zahlreiche Patentprüfer
1ns: 17. Senat des BPatG
sjD: sonstige Richter, auch am BGH und EPA, die inzwischen die Problematik des Dammbruchs der 90er Jahre erkannt haben
NgW: Normalbürger, die sich ohne näheres Hinsehen zunächst wünschen, dass jede intellektuelle Leistung irgendwie belohnt werden sollte (auch %(q:Belohnungstheorie) genannt) und dass der Staat sicherstellen sollte, dass sich nicht irgendwelche Großkonzerne die Früchte der Arbeit des kleinen Mannes aneignen
Dct: Das Präsidium der Gesellschaft für Informatik sowie einige Informatiker insbesondere an Hochschulen, die sich für ihren Bereich ähnliche Wertschöpfungsmöglichkeiten wünschen, wie man sie aus dem Ingenieurwesen kennt, und die daher %(q:Software-Engineering), %(q:Datentechnik) etc beschwören und Hoffnungen in Patente setzen
Pee: Patentrechtsexperten im diversen Organisationen
aet: ausschließlich auf ihr Patent-Credo gestützt, ohne betriebs- oder gar volkswirtschaftliche Argumentation, aber die Flagge ihrer Organisation hissend
Vve: Vertreter von Patentämtern und Patentreferaten in Regierungen
Esm: Europäische Kommission
ejj: eigentlich nur die federführende Geraldirektion Binnenmarkt, dort auch weniger Kommissar Bolkestein als die von Patentjuristen geleitete Dienststelle für Gewerblichen Rechtschutz
Ehd: Einige akademische Denkfabriken der Patentbewegung
eig: explizite aggressive Monopolstrategie gegen GNU/Linux
Iut: Im Hardwarebereich beheimatete Firmen mit großer Patentabteilung wie %(IBM) und einige Telekommunikationsriesen, deren politisch allein aktive Patentabteilung ohne Softwarepatente ihre Bedeutung verlieren würde (meist ohne dass dies allerdings dem Unternehmen als ganzem schaden würde).
Hlb: The Universitarian Patent Movement
Med: Some IPR accountants and financial market analysts who like to have some visible assets to count
DaW: This collection of answers (FAQ) has just begun.
Emn: There are a few more documents that show some features of a FAQ
Diu: Two kinds can be distinguished:
Aag: introductory replies to questions of curious laymen.
WWa: refutations of %(pm:patent movement) propaganda.
Dne: Most of the following documents belong at least in part to one of these categories:

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatdanfu ;
# txtlang: en ;
# End: ;

