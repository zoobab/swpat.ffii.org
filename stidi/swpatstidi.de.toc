\contentsline {section}{\numberline {1}Die Grundlegenden Fragen}{2}{section.1}
\contentsline {section}{\numberline {2}Patentjurisprudenz auf Schlitterkurs -- der Preis f\"{u}r die Demontage des Technikbegriffs}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}Einstimmende Zitate}{7}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Freiheit alles Geistigen oder Patentierbarkeit alles Maschinenhaften?}{10}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Von der Technizit\"{a}t zur Beliebigkeit}{13}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Sinnentleerte Begriffe und ihr technischer Beitrag zum Politischen Engineering in der EU}{15}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Literatur\"{u}bersicht}{17}{subsection.2.5}
\contentsline {section}{\numberline {3}Weitere ernsthafte Versuche, Fragestellungen zu erarbeiten}{44}{section.3}
