<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Drittes Paradigma: Maßgeschneidertes Software-Schutzrecht

#descr: Ein Datenverarbeitungsprogramm ist Sprachwerk und virtuelle Maschine zugleich.  Weder das Urheberrecht- noch das Patentrecht wurden für Computerprogramme geschaffen.  Einige Wissenschaftler und Politiker haben daher für ein %(q:drittes Paradigma) zwischen Patent- und Urheberrecht argumentiert.  Andere haben die abstrakt-logischen Ideen als ein %(q:Niemandsland des geistigen Eigentums) bezeichnet und dessen Freihaltung von Eigentumsansprüchen gefordert.  Neben Ausschlussrechten kommen auch Vergütungsrechte und sonstige Förderungssysteme in Betracht.

#EUs: Eigentum, Patente, Urheberrecht und das %(q:Niemandsland)

#Wnd: Weiche Patente für Hardware-Logik

#Lgl: Logileg

#Nee: Nutzungsprivileg auf abstrakt-logische Innovationen

#Lin: Logidende

#Laa: Logikalieninnovationsdividende

#Ret: Anregungen für eine Reform des Patentwesens

#Fea: Weitere Lektüre

#Asg: Abstrakt-Logische Innovationen befinden sich nach traditionellem Eigentumsrecht in einem öffentlichen Bereich, den führende europäische Patentrechtsgelehrte auch %(q:Niemandsland des Geistigen Eigentums) %(gk:genannt) haben:

#knr: konkrete

#For: Form

#asr: abstrakte

#Ide: Idee

#MmW: Physikalien

#Mtr: Materie

#Egn: Materielles Privateigentum

#Per: Patent & Gebrauchsmuster

#Een: Erzeugnis

#Vfr: Verfahren

#Gce: Beschreibung

#IeG: Logikalien

#GWm: Geist, Information

#Ube: Urheberrecht

#NdW: Geistiges Gemeineigentum

#Uri: Programmierter Universalrechner

#PmA: Programm-Verhalten

#Prt: Programm-Text

#Bhn: Bei den physischen Abstraktionen gibt es ein Gefälle vom materiellen Erzeugnis über das mehr oder weniger eng damit verbundene Verfahren bis hin zur vollends immateriellen Verfahrensbeschreibung.  Im Bereich der logischen Gegenstände (Logikalien) steht allen dreien lediglich das Computerprogramm als Entsprechung gegenüber, welches dank seiner Immaterialität zahlreiche Aspekte in sich vereinigt.

#Iee: In dem Maße, wie ein Eigentumsgegenstand sich von der Materie löst, kann er durch kostenarme Vervielfältigung Gemeingut werden.  Gebrauchsmuster gibt es nur für materielle Erzeugnisse, Patente auch für damit zusammenhängende Verfahren, nicht aber für Verfahrensbeschreibungen, obwohl letztere auch einen Marktwert haben, der durch Nachahmer gemindert werden könnte.  Die Gedanken sind frei. Informationswerke verbreiten sich noch schneller als Gedanken.  Je leichter vervielfältigbar ein Gegenstand ist, desto weniger ist man geneigt, seine Verbreitung zu verlangsamen, indem man Eigentumsansprüche darauf anerkennt.  Wenn die Vervielfältigungskosten bei Null liegen, errechnet sich der Verlangsamungsfaktor als eine Division durch Null, d.h. der Schmerz tendiert gegen unendlich.

#Etd: Ein ähnliches Gefälle liegt zwischen den %(q:Erfindungen) und %(q:Schöpfungen) auf der einen und den %(q:Entdeckungen) und Natur-Ressourcen auf der anderen Seite.  Erstere sind konkret und einmalig, letztere werden in immer der gleichen Weise von jedem vorgefunden, der an einem bestimmten Ort vorbeikommt, sei es in der physischen Welt oder in mathematisch-logischen Räumen.

#AiW: Auf all diesen Gefälle-Achsen liegen die typischen Software-Patente ganz rechts unten in dem Raum des naturrechtlichen Gemeineigentums, des %(q:Niemandslandes des Geistigen Eigentums).  Auf Logikalien 20 Jahre lange Monpole zu erteilen ist ebenso naturrechtswidrig wie am anderen Ende des Extrems die Vergesellschaftung aller Küchengeräte in der %(q:Volkskommunenbewegung), mit der Mao Tse-tung in China 1960 eine Hungerkatastrophe erzeugte.  Alle Eigentumsregime sind möglich, aber die Gesetzmäßigkeiten schlagen zurück und wir zahlen den Preis.

#Arv: Viele Diskutanten äußern seit jeher grundsätzliche Bedenken gegen Eigentumsrechte an abstrakt-logischen Konzepten:  einerseits belohnen solche Eigentumsrechte meist nicht den wirklich aufwendigen Teil des Software-Innovationsprozesses, andererseits schränken sie aber die Innovationsfreiheit besonders empfindlich ein.

#VaW: Viele Software-Entwickler und IT-Unternehmer halten das Software-Urheberrecht für %(q:völlig ausreichend) und geradezu %(q:maßgeschneidert).

#Dge: Dennoch gibt es hier und da auch klare Gegenbeispiele.  So wurden etwa manche mathematische Beweise erst nach jahrhundertelanger Suche gefunden.  Die Frage, ob ein patentähnliches Ausschlussrecht auf immaterielle Implementierungen solcher Beweise der Gesellschaft mehr nützt als schadet, ist damit aber noch nicht beantwortet.  Vieles spricht dafür, die Gedanken grundsätzlich frei zu halten und stattdessen zur Förderung des Fortschritts ein System von nicht-exklusiven Vergütungsrechten einzurichten.

#Eti: Innovative Prozesse, zu deren Umsetzung ein neuartiges Computerprogramm auf herkömmlichen Rechenapparatur genügt, könnten insoweit monopolisiert werden, wie dieses Monopol ohne ein Verbot der Weitergabe von Computerprogrammen durchgesetzt werden kann, m.a.W. wie der Prozess nicht auf einem Universalrechner abläuft.  Wenn z.B. ein Autohersteller nicht gerade seine Motorsteuerung kundenseitig programmierbar (und damit teurer und weniger sicher) machen will, würde er es vermutlich vorziehen, einem Patentinhaber maßvolle Lizenzgebühren zu zahlen.

#dWs: die Anwendung des Verfahrens für geschäftliche Zwecke

#dei: die Umsetzung des Verfahrens in %(po:Eigentumsgegenständen)

#iiS: in proprietärer Software

#dEn: die Nutzung zur Erreichung von Interoperabilität

#dfa: die Veröffentlichung in %(fi:frei weiterentwickelbaren Informationsgebilden) mit einem angemessenen Verweis auf die Nutzungseinschränkungen

#Plr: freier Quelltext

#DfW: Der Inhaber eines %(ll:Logilegs) genießt ab dem Tag der Offenlegung eines innovativen logischen Verfahrens einige Jahre lang Ausschlussrechte auf %{L1}.

#Leg: Logikaliennutzungsprivileg

#NbW: Nicht untersagbar sind %{L0}

#Ogt: Offenzulegen ist mindestens eine Referenzimplementation und ein daran angelehnter Anspruchsbereich.  Diese Offenlegung muss strengen formellen und sachlichen Anforderungen genügen, deren Ziel es ist, sie für die Öffentlichkeit leicht zugänglich und überprüfbar zu machen.

#Deg: Das Logileg gilt sofort mit der Offenlegung. Die Gültigkeitsprüfung obliegt dem Logileginhaber.  Während der Laufzeit kann jedermann Einspruch erheben.  Gebühren fallen nicht an, aber der Logileginhaber hinterlegt notarisch eine Kaution von 5000 EUR, die zur Belohnung an den ersten Einspruchsführer ausgezahlt wird, dem es gelingt, die Ungültigkeit des Logilegs nachzuweisen. Bei Nachweis eines Mangels fällt das ganze Logileg.  Nachbesserungen sind nicht gestattet.

#Djh: Zunächst wird nur das %(q:von der Innovation zu lösende Problem) zusammen mit der vorbekannten Technik veröffentlicht, während die gesamte Logilegschrift in verschlüsselter Form bereitgestellt und erst nach einem Monat entschlüsselt wird.  Während dieses Ausschreibungsmonats kann jedermann Lösungen des Problems veröffentlichen, die dann als vorbekannte Technik gelten.  Falls diese vorbekannte Technik das Logileg ungültig macht, wird die Kautionssumme an der Urheber der ersten anspruchsbrechenden Veröffentlichung ausgezahlt.

#Dei: Der Anmelder darf auch eine längere Zeitdauer für das Preisausschreiben zulassen und erhält im Gegenzug bei Erfolg eine längere Ausschlussdauer, z.B. ein zusätzliches Gültigkeitsjahr für jede zusätzliche Ausschreibungswoche.

#Irt: An Stelle von Ausschlussrechten könnten wir auch nicht-exklusive Vergütungsrechte vergeben.  Solche Rechte würden Innovatoren direkt belohnen, ohne Programmierfreiheit oder Interoperabilität zu beeinträchtigen.  Die Welt der immateriellen Ideen bliebe eine eigentumsfreie Allmende.  Die Vergütungen müssten jedoch aus einem Steuerfond finanziert werden, und würden somit anstelle des gewöhnlichen Marktwettbewerbs relativ aufwendige Sozialtechniken erfordern.

#Dae: Die Logidende schließt niemanden von der Nutzung der Idee aus.  Allerdings sollte dem Innovator überall dort verbale Anerkennung gezollt werden, wo seine Idee verwendet wird.

#Tvf: Die Spielregeln zur Ermittlung echter Innovationen sind ähnlich wie beim oben skizzierten %(q:Logileg) zu gestalten.

#Lde: Sonderrechte obiger Art könnten in dem abstrakt-logischen Niemandsland vergeben werden, wo mangels Konkretheit und physischer Substanz (Technizität) weder das Patentsystem noch das Urheberrecht greift.  Einerseits erfordert die Leichtigkeit und Schnelllebigkeit abstrakt-logischer Innovationen eine besonders zügige und unbürokratische Verfahrensabwicklung, andererseits beeinträchtigen Privilegien in diesem Bereich die Öffentlichkeit in unerhörtem Maße.  %(q:Geistiges Eigentum) an abstrakten Ideen kommt einer %(q:Geistigen Umweltverschmutzung) gleich, für die der Privilegsinhaber besondere ausgleichende Leistungen zu erbringen hat.

#Vem: Viele Aspekte des Logilegs könnten auch als Anregung zur Reform des traditionellen Patentwesens taugen.  Angesichts der gültigen %(tr:internationalen Verträge) ist eine solche Reform aber kaum durchzusetzen, und im Bereich der konkret-physischen Erfindungen ist ihre Notwendigkeit auch viel weniger spürbar.  So ist z.B. ein Einreichungswettbewerb speziell bei abstrakt-logischen Innovationen erforderlich, weil die privilegierungswürdigen Innovationen aus einer flüchtigen Welt herausgefiltert werden müssen, in der Innovation zur täglichen Routine gehört und der Stand der Technik selten konsultiert geschweige denn dokumentiert wird.  So ergeben sich für das abstrakt-logische Eigentumsparadigma spezifische Herausforderungen, auf die das Patentwesen nie eine Antwort entwickeln musste.

#Aos: Ein Wortführer der Debatte um den Patentierbarkeitsausschluss von Computerprogrammen in den 70er Jahren erklärt in diesem Artikel von 1977 warum Software-Innovationen nach jedem kohärenten Technizitätskonzept nicht als technisch beurteilt werden und daher nicht patentiert werden können.  Kolle bemerkt, dass es ein %(q:Niemandsland des geistigen Eigentums) an Algorithmen und abstrakten Ideen gibt, deren %(q:Vergesellschaftung) zu wünschen ist, da ihre Aneignbarkeit anders als bei herkömmlichen technischen Erfindungen große Sperrwirkungen entfalten würde, denen keinen nennenswerter gesamtwirtschaftlicher Nutzen gegenüberstünde.

#lWr: Führende Rechtsexperten Amerikas argumentieren 1994 gegen die Ausweitung des Patentwesens auf Software und zeigen gleichzeitig Unzulänglichkeiten des Urheberrechts auf.  Die hybride Natur von Software lässt ein System des Verbots von zu schneller Nachahmung sinnvoll erscheinen.  Dabei gibt es keine Anspruchsprüfung und nur eine kurze Schutzdauer.

#ahn: eine detaillierte Studie über dieUmsetzung der Gedanken des Manifests in amerikanisches Recht und die zu erwartenden Wirkungen auf die Praxis, die recht positiv beurteilt werden.

#Org: Ein Kapitel dieses umfangreichen Berichtes eines französischen Regierungsorgans, der auch ins Englische übersetzt wurde, widmet sich der Frage möglicher angemessener Systeme zur Belohnung von Software-Innovatoren.  U.a. wird auch eine kürzere und schwächere Form von Algorithmenmonopolen in Erwägung gezogen.

#ImW: Im Herbst 2000 unterstützt die CDU/CSU-Fraktion im Deutschen Bundestag einen Antrag, der einen %(q:maßgeschneiderten Software-Rechtschutz) zwischen Patent und Urheberrecht fordert.

#IWn: Renommierte Softwareunternehmen und Verbände fordern fünferlei gesetzgeberische Maßnahmen zum Schutz der informatischen Innovation for dem Missbrauch des Patentwesens.  Initiative Nr. 4 regt die Schaffung von %(q:sachgerechten Systemen zur Förderung der informationellen Innovation) an.

#AWt: Auch hier wird empfohlen, die Möglichkeiten eines gesonderten Rechtsschutzes zu erwägen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbasti ;
# txtlang: de ;
# multlin: t ;
# End: ;

