\begin{subdocument}{swpatbasti}{Drittes Paradigma: Ma{\ss}geschneidertes Software-Schutzrecht}{http://swpat.ffii.org/analyse/suigen/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Ein Datenverarbeitungsprogramm ist Sprachwerk und virtuelle Maschine zugleich.  Weder das Urheberrecht- noch das Patentrecht wurden f\"{u}r Computerprogramme geschaffen.  Einige Wissenschaftler und Politiker haben daher f\"{u}r ein ``drittes Paradigma'' zwischen Patent- und Urheberrecht argumentiert.  Andere haben die abstrakt-logischen Ideen als ein ``Niemandsland des geistigen Eigentums'' bezeichnet und dessen Freihaltung von Eigentumsanspr\"{u}chen gefordert.  Neben Ausschlussrechten kommen auch Verg\"{u}tungsrechte und sonstige F\"{o}rderungssysteme in Betracht.}
\begin{sect}{pubpriv}{Eigentum, Patente, Urheberrecht und das ``Niemandsland''}
Abstrakt-Logische Innovationen befinden sich nach traditionellem Eigentumsrecht in einem \"{o}ffentlichen Bereich, den f\"{u}hrende europ\"{a}ische Patentrechtsgelehrte auch ``Niemandsland des Geistigen Eigentums'' genannt haben:

\begin{center}
\begin{center}
\begin{tabular}{|C{22}|C{22}|C{36}|}
\hline
 & konkrete\par

Form & abstrakte\par

Idee\\\hline
{\bf Physikalien\par

Materie} & Materielles Privateigentum & Patent \& Gebrauchsmuster\par

\begin{center}
\begin{tabular}{|C{22}|C{22}|C{36}|}
\hline
Erzeugnis & Verfahren & Beschreibung\\\hline
\end{tabular}
\end{center}\\\hline
{\bf Logikalien\par

Geist, Information} & Urheberrecht & Geistiges Gemeineigentum\par

\begin{center}
\begin{tabular}{|C{22}|C{22}|C{36}|}
\hline
Programmierter Universalrechner & Programm-Verhalten & Programm-Text\\\hline
\end{tabular}
\end{center}\\\hline
\end{tabular}
\end{center}
\end{center}

Bei den physischen Abstraktionen gibt es ein Gef\"{a}lle vom materiellen Erzeugnis \"{u}ber das mehr oder weniger eng damit verbundene Verfahren bis hin zur vollends immateriellen Verfahrensbeschreibung.  Im Bereich der logischen Gegenst\"{a}nde (Logikalien) steht allen dreien lediglich das Computerprogramm als Entsprechung gegen\"{u}ber, welches dank seiner Immaterialit\"{a}t zahlreiche Aspekte in sich vereinigt.

In dem Ma{\ss}e, wie ein Eigentumsgegenstand sich von der Materie l\"{o}st, kann er durch kostenarme Vervielf\"{a}ltigung Gemeingut werden.  Gebrauchsmuster gibt es nur f\"{u}r materielle Erzeugnisse, Patente auch f\"{u}r damit zusammenh\"{a}ngende Verfahren, nicht aber f\"{u}r Verfahrensbeschreibungen, obwohl letztere auch einen Marktwert haben, der durch Nachahmer gemindert werden k\"{o}nnte.  Die Gedanken sind frei. Informationswerke verbreiten sich noch schneller als Gedanken.  Je leichter vervielf\"{a}ltigbar ein Gegenstand ist, desto weniger ist man geneigt, seine Verbreitung zu verlangsamen, indem man Eigentumsanspr\"{u}che darauf anerkennt.  Wenn die Vervielf\"{a}ltigungskosten bei Null liegen, errechnet sich der Verlangsamungsfaktor als eine Division durch Null, d.h. der Schmerz tendiert gegen unendlich.

Ein \"{a}hnliches Gef\"{a}lle liegt zwischen den ``Erfindungen'' und ``Sch\"{o}pfungen'' auf der einen und den ``Entdeckungen'' und Natur-Ressourcen auf der anderen Seite.  Erstere sind konkret und einmalig, letztere werden in immer der gleichen Weise von jedem vorgefunden, der an einem bestimmten Ort vorbeikommt, sei es in der physischen Welt oder in mathematisch-logischen R\"{a}umen.

Auf all diesen Gef\"{a}lle-Achsen liegen die typischen Software-Patente ganz rechts unten in dem Raum des naturrechtlichen Gemeineigentums, des ``Niemandslandes des Geistigen Eigentums''.  Auf Logikalien 20 Jahre lange Monpole zu erteilen ist ebenso naturrechtswidrig wie am anderen Ende des Extrems die Vergesellschaftung aller K\"{u}chenger\"{a}te in der ``Volkskommunenbewegung'', mit der Mao Tse-tung in China 1960 eine Hungerkatastrophe erzeugte.  Alle Eigentumsregime sind m\"{o}glich, aber die Gesetzm\"{a}{\ss}igkeiten schlagen zur\"{u}ck und wir zahlen den Preis.

Viele Diskutanten \"{a}u{\ss}ern seit jeher grunds\"{a}tzliche Bedenken gegen Eigentumsrechte an abstrakt-logischen Konzepten:  einerseits belohnen solche Eigentumsrechte meist nicht den wirklich aufwendigen Teil des Software-Innovationsprozesses, andererseits schr\"{a}nken sie aber die Innovationsfreiheit besonders empfindlich ein.

Viele Software-Entwickler und IT-Unternehmer halten das Software-Urheberrecht f\"{u}r ``v\"{o}llig ausreichend'' und geradezu ``ma{\ss}geschneidert''.

Dennoch gibt es hier und da auch klare Gegenbeispiele.  So wurden etwa manche mathematische Beweise erst nach jahrhundertelanger Suche gefunden.  Die Frage, ob ein patent\"{a}hnliches Ausschlussrecht auf immaterielle Implementierungen solcher Beweise der Gesellschaft mehr n\"{u}tzt als schadet, ist damit aber noch nicht beantwortet.  Vieles spricht daf\"{u}r, die Gedanken grunds\"{a}tzlich frei zu halten und stattdessen zur F\"{o}rderung des Fortschritts ein System von nicht-exklusiven Verg\"{u}tungsrechten einzurichten.
\end{sect}

\begin{sect}{softent}{Weiche Patente f\"{u}r Hardware-Logik}
Innovative Prozesse, zu deren Umsetzung ein neuartiges Computerprogramm auf herk\"{o}mmlichen Rechenapparatur gen\"{u}gt, k\"{o}nnten insoweit monopolisiert werden, wie dieses Monopol ohne ein Verbot der Weitergabe von Computerprogrammen durchgesetzt werden kann, m.a.W. wie der Prozess nicht auf einem Universalrechner abl\"{a}uft.  Wenn z.B. ein Autohersteller nicht gerade seine Motorsteuerung kundenseitig programmierbar (und damit teurer und weniger sicher) machen will, w\"{u}rde er es vermutlich vorziehen, einem Patentinhaber ma{\ss}volle Lizenzgeb\"{u}hren zu zahlen.
\end{sect}

\begin{sect}{logileg}{Logileg: Nutzungsprivileg auf abstrakt-logische Innovationen}
Der Inhaber eines \emph{Logilegs} (\emph{Logikaliennutzungsprivileg}) genie{\ss}t ab dem Tag der Offenlegung eines innovativen logischen Verfahrens einige Jahre lang Ausschlussrechte auf \begin{enumerate}
\item
die Anwendung des Verfahrens f\"{u}r gesch\"{a}ftliche Zwecke

\item
die Umsetzung des Verfahrens in Eigentumsgegenst\"{a}nden (e.g. in propriet\"{a}rer Software)
\end{enumerate}.
Nicht untersagbar sind \begin{enumerate}
\item
die Nutzung zur Erreichung von Interoperabilit\"{a}t

\item
die Ver\"{o}ffentlichung in frei weiterentwickelbaren Informationsgebilden (e.g. freier Quelltext) mit einem angemessenen Verweis auf die Nutzungseinschr\"{a}nkungen
\end{enumerate}

Offenzulegen ist mindestens eine Referenzimplementation und ein daran angelehnter Anspruchsbereich.  Diese Offenlegung muss strengen formellen und sachlichen Anforderungen gen\"{u}gen, deren Ziel es ist, sie f\"{u}r die \"{O}ffentlichkeit leicht zug\"{a}nglich und \"{u}berpr\"{u}fbar zu machen.

Das Logileg gilt sofort mit der Offenlegung. Die G\"{u}ltigkeitspr\"{u}fung obliegt dem Logileginhaber.  W\"{a}hrend der Laufzeit kann jedermann Einspruch erheben.  Geb\"{u}hren fallen nicht an, aber der Logileginhaber hinterlegt notarisch eine Kaution von 5000 EUR, die zur Belohnung an den ersten Einspruchsf\"{u}hrer ausgezahlt wird, dem es gelingt, die Ung\"{u}ltigkeit des Logilegs nachzuweisen. Bei Nachweis eines Mangels f\"{a}llt das ganze Logileg.  Nachbesserungen sind nicht gestattet.

Zun\"{a}chst wird nur das ``von der Innovation zu l\"{o}sende Problem'' zusammen mit der vorbekannten Technik ver\"{o}ffentlicht, w\"{a}hrend die gesamte Logilegschrift in verschl\"{u}sselter Form bereitgestellt und erst nach einem Monat entschl\"{u}sselt wird.  W\"{a}hrend dieses Ausschreibungsmonats kann jedermann L\"{o}sungen des Problems ver\"{o}ffentlichen, die dann als vorbekannte Technik gelten.  Falls diese vorbekannte Technik das Logileg ung\"{u}ltig macht, wird die Kautionssumme an der Urheber der ersten anspruchsbrechenden Ver\"{o}ffentlichung ausgezahlt.

Der Anmelder darf auch eine l\"{a}ngere Zeitdauer f\"{u}r das Preisausschreiben zulassen und erh\"{a}lt im Gegenzug bei Erfolg eine l\"{a}ngere Ausschlussdauer, z.B. ein zus\"{a}tzliches G\"{u}ltigkeitsjahr f\"{u}r jede zus\"{a}tzliche Ausschreibungswoche.
\end{sect}

\begin{sect}{logidend}{Logidende: Logikalieninnovationsdividende}
An Stelle von Ausschlussrechten k\"{o}nnten wir auch nicht-exklusive Verg\"{u}tungsrechte vergeben.  Solche Rechte w\"{u}rden Innovatoren direkt belohnen, ohne Programmierfreiheit oder Interoperabilit\"{a}t zu beeintr\"{a}chtigen.  Die Welt der immateriellen Ideen bliebe eine eigentumsfreie Allmende.  Die Verg\"{u}tungen m\"{u}ssten jedoch aus einem Steuerfond finanziert werden, und w\"{u}rden somit anstelle des gew\"{o}hnlichen Marktwettbewerbs relativ aufwendige Sozialtechniken erfordern.

Der Inhaber einer Logidende genie{\ss}t ab dem Tag der Offenlegung eines innovativen logischen Verfahrens 10 Jahre lang Verg\"{u}tungsrechte.  Zur Verteilung der Verg\"{u}tungen wird ein Fond aus Ger\"{a}testeuern u.a. gebildet, in dem jeder Steuerzahler eine Stimme hat, die er in geheimer Wahl einer qualifizierten Verteilungsorganisation geben kann.  Jede Verteilungsorganisation bestimmt nach einem eigenen Wertungssystem den Dividendenwert jeder einzelnen Logidende.

Die Logidende schlie{\ss}t niemanden von der Nutzung der Idee aus.  Allerdings sollte dem Innovator \"{u}berall dort verbale Anerkennung gezollt werden, wo seine Idee verwendet wird.

Die Spielregeln zur Ermittlung echter Innovationen sind \"{a}hnlich wie beim oben skizzierten ``Logileg'' zu gestalten.
\end{sect}

\begin{sect}{patref}{Anregungen f\"{u}r eine Reform des Patentwesens}
Sonderrechte obiger Art k\"{o}nnten in dem abstrakt-logischen Niemandsland vergeben werden, wo mangels Konkretheit und physischer Substanz (Technizit\"{a}t) weder das Patentsystem noch das Urheberrecht greift.  Einerseits erfordert die Leichtigkeit und Schnelllebigkeit abstrakt-logischer Innovationen eine besonders z\"{u}gige und unb\"{u}rokratische Verfahrensabwicklung, andererseits beeintr\"{a}chtigen Privilegien in diesem Bereich die \"{O}ffentlichkeit in unerh\"{o}rtem Ma{\ss}e.  ``Geistiges Eigentum'' an abstrakten Ideen kommt einer ``Geistigen Umweltverschmutzung'' gleich, f\"{u}r die der Privilegsinhaber besondere ausgleichende Leistungen zu erbringen hat.

Viele Aspekte des Logilegs k\"{o}nnten auch als Anregung zur Reform des traditionellen Patentwesens taugen.  Angesichts der g\"{u}ltigen internationalen Vertr\"{a}ge ist eine solche Reform aber kaum durchzusetzen, und im Bereich der konkret-physischen Erfindungen ist ihre Notwendigkeit auch viel weniger sp\"{u}rbar.  So ist z.B. ein Einreichungswettbewerb speziell bei abstrakt-logischen Innovationen erforderlich, weil die privilegierungsw\"{u}rdigen Innovationen aus einer fl\"{u}chtigen Welt herausgefiltert werden m\"{u}ssen, in der Innovation zur t\"{a}glichen Routine geh\"{o}rt und der Stand der Technik selten konsultiert geschweige denn dokumentiert wird.  So ergeben sich f\"{u}r das abstrakt-logische Eigentumsparadigma spezifische Herausforderungen, auf die das Patentwesen nie eine Antwort entwickeln musste.
\end{sect}

\begin{sect}{links}{Weitere Lekt\"{u}re}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs\footnote{http://swpat.ffii.org/papiere/grur-kolle77/index.de.html}}}

\begin{quote}
Ein Wortf\"{u}hrer der Debatte um den Patentierbarkeitsausschluss von Computerprogrammen in den 70er Jahren erkl\"{a}rt in diesem Artikel von 1977 warum Software-Innovationen nach jedem koh\"{a}renten Technizit\"{a}tskonzept nicht als technisch beurteilt werden und daher nicht patentiert werden k\"{o}nnen.  Kolle bemerkt, dass es ein ``Niemandsland des geistigen Eigentums'' an Algorithmen und abstrakten Ideen gibt, deren ``Vergesellschaftung'' zu w\"{u}nschen ist, da ihre Aneignbarkeit anders als bei herk\"{o}mmlichen technischen Erfindungen gro{\ss}e Sperrwirkungen entfalten w\"{u}rde, denen keinen nennenswerter gesamtwirtschaftlicher Nutzen gegen\"{u}berst\"{u}nde.
\end{quote}
\filbreak

\item
{\bf {\bf A Manifesto Concerning the Legal Protection of Computer Programs\footnote{http://wwwsecure.law.cornell.edu/commentary/intelpro/manifint.htm\#intro}}}

\begin{quote}
F\"{u}hrende Rechtsexperten Amerikas argumentieren 1994 gegen die Ausweitung des Patentwesens auf Software und zeigen gleichzeitig Unzul\"{a}nglichkeiten des Urheberrechts auf.  Die hybride Natur von Software l\"{a}sst ein System des Verbots von zu schneller Nachahmung sinnvoll erscheinen.  Dabei gibt es keine Anspruchspr\"{u}fung und nur eine kurze Schutzdauer.
\end{quote}
\filbreak

\item
{\bf {\bf A Model Software Petite Patent Act\footnote{http://members.aol.com/paleymark/ModelAct.htm}}}

\begin{quote}
eine detaillierte Studie \"{u}ber dieUmsetzung der Gedanken des Manifests in amerikanisches Recht und die zu erwartenden Wirkungen auf die Praxis, die recht positiv beurteilt werden.
\end{quote}
\filbreak

\item
{\bf {\bf Stimuler la concurrence et l'innovation dans la Soci\'{e}t\'{e} d'Information\footnote{http://www.pro-innovation.org/rapport\_brevet/brevets\_plan.pdf}}}

\begin{quote}
Ein Kapitel dieses umfangreichen Berichtes eines franz\"{o}sischen Regierungsorgans, der auch ins Englische \"{u}bersetzt wurde, widmet sich der Frage m\"{o}glicher angemessener Systeme zur Belohnung von Software-Innovatoren.  U.a. wird auch eine k\"{u}rzere und schw\"{a}chere Form von Algorithmenmonopolen in Erw\"{a}gung gezogen.
\end{quote}
\filbreak

\item
{\bf {\bf Dr. Mayer: Moratorium statt Ausweitung der Software-Patente\footnote{http://www.m4m.de/internet/pmsoftpa.htm}}}

\begin{quote}
Im Herbst 2000 unterst\"{u}tzt die CDU/CSU-Fraktion im Deutschen Bundestag einen Antrag, der einen ``ma{\ss}geschneiderten Software-Rechtschutz'' zwischen Patent und Urheberrecht fordert.
\end{quote}
\filbreak

\item
{\bf {\bf Offener Brief: 5 Gesetzesinitiativen zum Schutz der Informatischen Innovation\footnote{http://swpat.ffii.org/briefe/patg2C/index.de.html}}}

\begin{quote}
Renommierte Softwareunternehmen und Verb\"{a}nde fordern f\"{u}nferlei gesetzgeberische Ma{\ss}nahmen zum Schutz der informatischen Innovation for dem Missbrauch des Patentwesens.  Initiative Nr. 4 regt die Schaffung von ``sachgerechten Systemen zur F\"{o}rderung der informationellen Innovation'' an.
\end{quote}
\filbreak

\item
{\bf {\bf 2001-02-28: Eurolinux-Appell an die Br\"{u}sseler Patentgesetzgeber\footnote{http://www.eurolinux.org/news/101/index.en.html}}}

\begin{quote}
Auch hier wird empfohlen, die M\"{o}glichkeiten eines gesonderten Rechtsschutzes zu erw\"{a}gen.
\end{quote}
\filbreak

\item
{\bf {\bf Remarks on the Patentability of Computer Software -- History, Status, Developments\footnote{http://swpat.ffii.org/termine/2001/linuxtag/jh/index.en.html}}}

\begin{quote}
Ein Aufsatz von Jozef Halbersztadt, Patentpr\"{u}fer am Polnischen Patentamt, f\"{u}r ein Seminar in Stuttgart im Juli 2001 vorbereitet.  Der Aufsatz dr\"{u}ckt seine private Sichtweise aus, wonach Software ein ma{\ss}geschneidertes System der Sch\"{o}pferbelohnung erfordert und nicht durch blo{\ss}e Anpassung des Urheberrechts oder, schlimmer noch, Patentrechts angemessen behandelt werden kann.  Er zeichnet die Geschichte des Ringens um die richtige Form der Sch\"{o}pferrechte im Softwarebereich als die Reise einer Kutsche nach, deren Fahrtrichtung von den Pferden bestimmt wurde.  Nachdem die Kutsche nun an die Wand gefahren wurde, sei es Zeit, aufzuwachen und fr\"{u}here Ans\"{a}tze eines softwarespezifischen Rechts neu zu beleben.  Halbersztadt zeigt dabei verschiedene m\"{o}gliche Optionen auf und beschreibt Wege, wie dies im Rahmen der derzeit geplanten europ\"{a}ischen Rechtsetzung geschehen k\"{o}nnte.
\end{quote}
\filbreak

\item
{\bf {\bf Wenning 2002-07-23: Spezialregeln f\"{u}r Software m\"{o}glich\footnote{http://www.fitug.de/debate/0207/msg00405.html}}}

\begin{quote}
Technische Probleml\"{o}sungen z.B. bei Rolltreppen lassen sich sehr wohl von reiner Informationsverarbeitung (Programmen als solchen) unterscheiden, und der Unterschied ist auch volkswirtschaftlich von Bedeutung. W\"{a}hrend Patente auf Rolltreppentechnik in dem kleinen Kreis, den sie betreffen, akzeptiert sind, schaffen Patente auf Rechenregeln einer gro{\ss}en \"{O}ffentlichkeit Probleme, ohne welche zu l\"{o}sen.  Rigo Wenning, der beim W3C als Jurist arbeitet, meint, Software hinreichend leicht von technischen Vorg\"{a}ngen abgrenzbar.  Da die generelle Ablehnung von Algorithmen-Eigentum nicht konsensf\"{a}hig sei, solle man Spezialregeln f\"{u}r Algorithmen anstreben.  Den von PA Horns \"{u}ber ein Regierungsgutachten in die Diskussion geworfenen ``Vorschlag, bei OpenSource die Verwender zu belasten'', h\"{a}lt Wenning hingegen f\"{u}r ein ``Herumdoktern an den Symptomen''.

siehe auch PA Axel Horns und Softwarepatente\footnote{http://swpat.ffii.org/akteure/horns/index.de.html}
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

