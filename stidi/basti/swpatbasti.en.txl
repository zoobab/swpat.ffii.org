<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Third Paradigm between Patent and Copyright Law

#descr: A computer program is a linguistic work and a virtual machine at the same time.  Neither copyright nor patent law were designed with computer programs in mind.  Some scholars and politicians have therefore argued for a %(q:Third Paradigm between patent and copyright law), also called %(e:specially tailored right) or ius sui generis.  Others have called abstract-logical ideas a %(q:nobody's land of intellectual property) and demanded that it should be kept free of all property claims.  Beside the granting of exclusion rights, there are other ways in which information innovation can be stimulated and rewarded.

#EUs: Property, Patents, Copyright and the Public Domain

#Wnd: Soft Patents for Hardware Logics

#Lgl: Logilege

#Nee: Use Privilege for Abstract-Logical Innovations

#Lin: Logidend

#Laa: Logical Innovation Dividend

#Ret: Inspiration for Patent Reform

#Fea: Further Reading

#Asg: Abstract-logical innovations are, according to traditional property law of Europe and elswhere, situated in the public domain, also regretfully %(gk:called) %(q:nobody's land) by some intellectual property lawyers:

#knr: concrete

#For: form

#asr: abstract

#Ide: idea

#MmW: physical

#Mtr: matter

#Egn: private material property

#Per: patents & utility certificates

#Een: product

#Vfr: process

#Gce: description

#IeG: logical

#GWm: mind, information

#Ube: copyright

#NdW: public intellectual property

#Uri: programmed computer

#PmA: program behaviour

#Prt: program text

#Bhn: In the case of physical abstractions, there is a gradation from the material product to a process more or less closely connected thereto to a fully immaterial description of that process.  In the field of logical objects, all three correspond to a computer program, which unites multiple aspects in one.

#Iee: To the degree that a property object is detached from matter, it tends to become a public good through cheap replication.  Utility certificates are granted only for material products while patents are also granted for processes connected thereto, but not for process descriptions, although the latter have a market value which is often diminished by imitators.  The thoughts are free.  Information spreads even faster than thoughts.   The more easily an object can be replicated, the less people are inclined to slow down its diffusion by property titles.  If the cost of replication is zero, than the slow-down factor would be %(q:something divided by zero), i.e. infinitely painful.

#Etd: There is a similar gradation between %(q:creations) and %(q:inventions) on the one side and %(q:discoveries) and natural ressources on the other.  While the former are concrete and individual, the latter are found in identical form by anyone who comes across a certain place, be it in the physical world or in some mathematically constructed space.

#AiW: On all these gradation axes, the typical software patents are situated in the lower right corner, in what seems to be a public domain by natural law.  To hand out 20 year monopolies on software concepts thus seems as much counter-natural as on the other extreme the collectivization of kitchen utensils in the %(q:people's commune movement), by which Mao Tse-tung created a famine in China of 1960.  Humans can establish any property regime they want, but natural law lashes back and we pay the price.

#Arv: Many Discutants have warned against allowing any type of appropriation of abstract-logical concepts:  on the one hand side such property rights would very often not compensate the really cost-intensive part of the software innovation process, while on the other hand side generating high public costs by limiting important freedoms.

#VaW: Many software developpers and IT entrepreneurs consider software copyright to be %(q:quite sufficient) or even %(q:perfectly tailored to the reality of software innovation).

#Dge: However here and there we can find extreme counter-examples.  Some mathematical theorems were known for centuries before someone came up with a successful proof.  Yet even this does not mean that a simple and brute monopoly on all computer program implementations of an abstract-logical innovation is the price that society has to pay.  Besides patent-like monopolies, less disruptive remuneration systems should be envisaged for this realm.

#Eti: Innovative processes for whose implementation not more than a new computer program running on known hardware is needed, could still be monopolised as long as this monopoly can be enforced without impeding the dissemination of computer programs, i.e. in those cases in which the process does not run on a universal computer.  Thus, if a car maker does not choose to lay out his motor control device for customer-side reprogramming (making it more costly and less safe), he would probably prefer to pay moderate license fees to a patent owner.

#dWs: application of the method for commercial purposes

#dei: implementation of the method in %(po:property-restricted objects)

#iiS: in proprietary software

#dEn: the right to use the method for achieving interoperability

#dfa: the right to publish the method in %(fi:freely republishable information works) with an appropriate pointer to the usage restrictions

#Plr: free source code

#DfW: The owner of a %(ll:logilege) publishes an innovative abstract-logical method and enjoys exclusion rights on %{L1} for a fixed number of years starting from the day of publication.

#Leg: logical innovation use privilege

#NbW: Nobody may be excluded from %{L0}

#Ogt: The published logilege description comprises one or more reference implementations and a set of claims.  The logilege description must satisfy strict formal and material requirements so as to make it easy for the public to access the description and test its validity.

#Deg: The logilege enters into force on the day of its publication.  There is no examination procedure.  The owner guarantees that claims are valid by paying a deposit of 5000 EUR, which will be paid to the first person who succeeds in proving the invalidity of the logilege.  One serious flaw, such as an inadmissible claim, suffices to invalidate the whole logilege.  Posterior narrowing of claims is not allowed.

#Djh: Initially only the %(q:problem to be solved by the innovation) is published together with the prior art, and the whole logilege description is made available only in an unreadable encrypted form and decrypted after one month.  During this month, any person may publish solutions to the problem, which become prior art.  If such prior art invalidates the logilege, the deposit sum is paid to the author of the earliest invalidating publication.

#Dei: The applicant may allow more time for the problem solving quest and in return obtain a longer exclusion period, e.g. one extra year of validity for each extra week of problem solving quest.

#Irt: Instead of exclusion rights we here grant non-exclusive remuneration rights.  Such rights directly reward innovators without interfering with other people's programming freedom or with interoperability.  Our informational infrastructure thus remains a property-free Commons zone.  The logidend is payed out of funds from taxes on computing devices or the like.  The value of each logidend is determined by a sophisticated social engineering scheme rather than by the conventional %(q:free market) approach.

#Dae: The logidend excludes nobody from use of the idea.  However verbal tribute should be paid to the innovator wherever the idea is used.

#Tvf: The rules for sorting out real innovations are similar to those of the %(q:logilege) outlined above.

#Lde: %(q:Third Paradigm) rights can be granted for those innovations which due to lack of concreteness or physical substance (technicity) fall out of the classical patent system.  The lightness and shortlivedness of abstract-logical innovations requires a fast and unbureuacratic procedure, while the great public cost, that any privilege on abstract-logical innovations tends to generate, demands a high degree of responsibility on the part of the privilege owner.

#Vem: Many aspects of the logilege could also serve to inspire a reform of the existing patent system.  But patent lawyers will contend: %(q:if it ain't broke don't fix it).  Within the %(tr:framework of patent treaties), reforms may meet even more obstacles, starting from a minimum requirement of 20 years of patentability (TRIPS Art 32).  It will be hard to overcome the resistance from those who insist that the conventional patent system works well enough in its home-base area of physical inventions.  In the area of logical innovation however more refined means such as the invalidating quest system are indispensable, because the truly worthy innovations must be filtered out from a volatile world where innovation is daily routine and the state of the art is rarely consulted let alone documented.  Thus any abstract-logical property paradigm faces specific challenges, for which the patent system never needed to develop an answer.

#Aos: A leading scholar of the software patent debate of the 1970s explains in this famous article of 1977 why, according to any coherent concept of technicity, software innovations cannot be judged as technical and therefore not be patented.  Kolle remarks that there is a %(q:nobody's land) of abstract ideas, which should stay in the public domain, because their appropriability would have enormous blocking effects while generating little benefit for the software economy.

#lWr: leading law experts argue against extension of the patent system to software and point out inadequacies of copyright.  The dual nature of software requires a system of short-term protection of investment against too early cloning.

#ahn: a more detailed study on how the Third Paradigm Manifesto could be integrated into the US legal system

#Org: One chapter of this report is dedicated to the question of appropriate schemes for the protection of investment in software.  Among others, a short and weak form of algorithm monopoly is envisaged.

#ImW: In autumn of 2000, the CDU/CSU fraction supports a motion in the German Federal Parliament which calls for a Third Paradigm type software property regime

#IWn: Renowned software companies and associations call politicians to support five law initiatives for the protection of information innovation against the abuse of the patent system. Initiative no. 4 calls for the creation of %(q:adequate systems for the stimulation of information innovation).

#AWt: Here too the authors recommend to consider a sui generis protection

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbasti ;
# txtlang: en ;
# multlin: t ;
# End: ;

