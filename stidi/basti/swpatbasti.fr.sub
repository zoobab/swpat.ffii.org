\begin{subdocument}{swpatbasti}{Troisi\`{e}me Paradigme hors de Brevet et Droit d'Auteur}{http://swpat.ffii.org/analyse/suigen/swpatbasti.fr.html}{Groupe de travail\\swpatag@ffii.org\\version fran\c{c}aise 2003/10/13 par MichÃ?Â?Ã?Â?Ã?Â?Ã?Â¨le Garoche\footnote{http://micmacfr.homeunix.org}}{Un programme d'ordinateur est un oevre linguistique et une machine virtuelle en m\^{e}me temps.  Ni le droit d'auteur ni la loi de brevets \'{e}taient cr\'{e}\'{e}s en visant au programmes d'ordinateur.  Des chercheurs et politiciens ont donc argument\'{e} pour un ``Troisi\`{e}me Paradigme entre brevet et droit d'auteur''.  Des autre ont affirm\'{e} que les id\'{e}es abstraites-logiques appartiennent a une ``terre publique hors de la propri\'{e}t\'{e} intellectuelle''.  A partir des droits d'exclusion il y a encore d'autres moyen pour stimuler l'innovation.}
\begin{sect}{pubpriv}{Propri\'{e}t\'{e}, Brevets, Droit d'Auteur et le Domaine Publique}
Les innovations abstraites-logiques sont, selon la tradition de loi de l'Europe et ailleurs, situ\'{e}s dans le domaine publique, qui \`{a} aussi \'{e}t\'{e} appell\'{e}e\footnote{http://swpat.ffii.org/papiers/grur-kolle77/grur-kolle77.de.html} ``terre sans propri\'{e}taire'' de quelques savants de dogme juridique.

\begin{center}
\begin{center}
\begin{tabular}{|C{22}|C{22}|C{36}|}
\hline
 & concr\`{e}te\par

forme & abstrait\par

id\'{e}e\\\hline
{\bf obj\'{e}t physiques\par

mati\`{e}re} & propri\'{e}t\'{e} mat\'{e}rielle priv\'{e}e & brevets et certificats d'utilit\'{e}\par

\begin{center}
\begin{tabular}{|C{22}|C{22}|C{36}|}
\hline
produit & proc\'{e}d\'{e} & d\'{e}scription\\\hline
\end{tabular}
\end{center}\\\hline
{\bf obj\'{e}ts logiques\par

pens\'{e}e, information} & droit d'auteur & propri\'{e}t\'{e} intellectuelle publique\par

\begin{center}
\begin{tabular}{|C{22}|C{22}|C{36}|}
\hline
ordinateur programm\'{e} & comportement de programme & texte de programme\\\hline
\end{tabular}
\end{center}\\\hline
\end{tabular}
\end{center}
\end{center}

Bei den physischen Abstraktionen gibt es ein Gef\"{a}lle vom materiellen Erzeugnis \"{u}ber das mehr oder weniger eng damit verbundene Verfahren bis hin zur vollends immateriellen Verfahrensbeschreibung.  Im Bereich der logischen Gegenst\"{a}nde (Logikalien) steht allen dreien lediglich das Computerprogramm als Entsprechung gegen\"{u}ber, welches dank seiner Immaterialit\"{a}t zahlreiche Aspekte in sich vereinigt.

In dem Ma{\ss}e, wie ein Eigentumsgegenstand sich von der Materie l\"{o}st, kann er durch kostenarme Vervielf\"{a}ltigung Gemeingut werden.  Gebrauchsmuster gibt es nur f\"{u}r materielle Erzeugnisse, Patente auch f\"{u}r damit zusammenh\"{a}ngende Verfahren, nicht aber f\"{u}r Verfahrensbeschreibungen, obwohl letztere auch einen Marktwert haben, der durch Nachahmer gemindert werden k\"{o}nnte.  Die Gedanken sind frei. Informationswerke verbreiten sich noch schneller als Gedanken.  Je leichter vervielf\"{a}ltigbar ein Gegenstand ist, desto weniger ist man geneigt, seine Verbreitung zu verlangsamen, indem man Eigentumsanspr\"{u}che darauf anerkennt.  Wenn die Vervielf\"{a}ltigungskosten bei Null liegen, errechnet sich der Verlangsamungsfaktor als eine Division durch Null, d.h. der Schmerz tendiert gegen unendlich.

Ein \"{a}hnliches Gef\"{a}lle liegt zwischen den ``Erfindungen'' und ``Sch\"{o}pfungen'' auf der einen und den ``Entdeckungen'' und Natur-Ressourcen auf der anderen Seite.  Erstere sind konkret und einmalig, letztere werden in immer der gleichen Weise von jedem vorgefunden, der an einem bestimmten Ort vorbeikommt, sei es in der physischen Welt oder in mathematisch-logischen R\"{a}umen.

Auf all diesen Gef\"{a}lle-Achsen liegen die typischen Software-Patente ganz rechts unten in dem Raum des naturrechtlichen Gemeineigentums, des ``Niemandslandes des Geistigen Eigentums''.  Auf Logikalien 20 Jahre lange Monpole zu erteilen ist ebenso naturrechtswidrig wie am anderen Ende des Extrems die Vergesellschaftung aller K\"{u}chenger\"{a}te in der ``Volkskommunenbewegung'', mit der Mao Tse-tung in China 1960 eine Hungerkatastrophe erzeugte.  Alle Eigentumsregime sind m\"{o}glich, aber die Gesetzm\"{a}{\ss}igkeiten schlagen zur\"{u}ck und wir zahlen den Preis.

Viele Diskutanten \"{a}u{\ss}ern seit jeher grunds\"{a}tzliche Bedenken gegen Eigentumsrechte an abstrakt-logischen Konzepten:  einerseits belohnen solche Eigentumsrechte meist nicht den wirklich aufwendigen Teil des Software-Innovationsprozesses, andererseits schr\"{a}nken sie aber die Innovationsfreiheit besonders empfindlich ein.

Viele Software-Entwickler und IT-Unternehmer halten das Software-Urheberrecht f\"{u}r ``v\"{o}llig ausreichend'' und geradezu ``ma{\ss}geschneidert''.

However here and there we can find extreme counter-examples.  Some mathematical theorems were known for centuries before someone came up with a successful proof.  Yet even this does not mean that a simple and brute monopoly on all computer program implementations of an abstract-logical innovation is the price that society has to pay.  Besides patent-like monopolies, less disruptive remuneration systems should be envisaged for this realm.
\end{sect}

\begin{sect}{softent}{Brevets softes pour logique durcie}
Innovative processes for whose implementation not more than a new computer program running on known hardware is needed, could still be monopolised as long as this monopoly can be enforced without impeding the dissemination of computer programs, i.e. in those cases in which the process does not run on a universal computer.  Thus, if a car maker does not choose to lay out his motor control device for customer-side reprogramming (making it more costly and less safe), he would probably prefer to pay moderate license fees to a patent owner.
\end{sect}

\begin{sect}{logileg}{logil\`{e}ge: Privil\'{e}ge d'usage sur des innovation abstraites-logiques}
Der Inhaber eines \emph{Logilegs} (\emph{Logikaliennutzungsprivileg}) genie{\ss}t ab dem Tag der Offenlegung eines innovativen logischen Verfahrens einige Jahre lang Ausschlussrechte auf \begin{enumerate}
\item
die Anwendung des Verfahrens f\"{u}r gesch\"{a}ftliche Zwecke

\item
die Umsetzung des Verfahrens in Eigentumsgegenst\"{a}nden (ex. in propriet\"{a}rer Software)
\end{enumerate}.
Nicht untersagbar sind \begin{enumerate}
\item
die Nutzung zur Erreichung von Interoperabilit\"{a}t

\item
die Ver\"{o}ffentlichung in frei weiterentwickelbaren Informationsgebilden (ex. freier Quelltext) mit einem angemessenen Verweis auf die Nutzungseinschr\"{a}nkungen
\end{enumerate}

Offenzulegen ist mindestens eine Referenzimplementation und ein daran angelehnter Anspruchsbereich.  Diese Offenlegung muss strengen formellen und sachlichen Anforderungen gen\"{u}gen, deren Ziel es ist, sie f\"{u}r die \"{O}ffentlichkeit leicht zug\"{a}nglich und \"{u}berpr\"{u}fbar zu machen.

Das Logileg gilt sofort mit der Offenlegung. Die G\"{u}ltigkeitspr\"{u}fung obliegt dem Logileginhaber.  W\"{a}hrend der Laufzeit kann jedermann Einspruch erheben.  Geb\"{u}hren fallen nicht an, aber der Logileginhaber hinterlegt notarisch eine Kaution von 5000 EUR, die zur Belohnung an den ersten Einspruchsf\"{u}hrer ausgezahlt wird, dem es gelingt, die Ung\"{u}ltigkeit des Logilegs nachzuweisen. Bei Nachweis eines Mangels f\"{a}llt das ganze Logileg.  Nachbesserungen sind nicht gestattet.

Initially only the ``problem to be solved by the innovation'' is published together with the prior art, and the whole logilege description is made available only in an unreadable encrypted form and decrypted after one month.  During this month, any person may publish solutions to the problem, which become prior art.  If such prior art invalidates the logilege, the deposit sum is paid to the author of the earliest invalidating publication.

The applicant may allow more time for the problem solving quest and in return obtain a longer exclusion period, e.g. one extra year of validity for each extra week of problem solving quest.
\end{sect}

\begin{sect}{logidend}{Logidende: dividende d'innovation logicielle}
Instead of exclusion rights we here grant non-exclusive remuneration rights.  Such rights directly reward innovators without interfering with other people's programming freedom or with interoperability.  Our informational infrastructure thus remains a property-free Commons zone.  The logidend is payed out of funds from taxes on computing devices or the like.  The value of each logidend is determined by a sophisticated social engineering scheme rather than by the conventional ``free market'' approach.

Der Inhaber eines Logilegs genie{\ss}t ab dem Tag der Offenlegung eines innovativen logischen Verfahrens einige Jahre lang Ausschlussrechte auf L1.

The logidend excludes nobody from use of the idea.  However verbal tribute should be paid to the innovator wherever the idea is used.

The rules for sorting out real innovations are similar to those of the ``logilege'' outlined above.
\end{sect}

\begin{sect}{patref}{Inspirations pour reformes du syst\`{e}me de brevets}
Sonderrechte obiger Art k\"{o}nnten in dem abstrakt-logischen Niemandsland vergeben werden, wo mangels Konkretheit und physischer Substanz (Technizit\"{a}t) weder das Patentsystem noch das Urheberrecht greift.  Einerseits erfordert die Leichtigkeit und Schnelllebigkeit abstrakt-logischer Innovationen eine besonders z\"{u}gige und unb\"{u}rokratische Verfahrensabwicklung, andererseits beeintr\"{a}chtigen Privilegien in diesem Bereich die \"{O}ffentlichkeit in unerh\"{o}rtem Ma{\ss}e.  ``Geistiges Eigentum'' an abstrakten Ideen kommt einer ``Geistigen Umweltverschmutzung'' gleich, f\"{u}r die der Privilegsinhaber besondere ausgleichende Leistungen zu erbringen hat.

Many aspects of the logilege could also serve to inspire a reform of the existing patent system.  But patent lawyers will contend: ``if it ain't broke don't fix it''.  Within the framework of patent treaties\footnote{http://swpat.ffii.org/analyse/trips/swpattrips.fr.html}, reforms may meet even more obstacles, starting from a minimum requirement of 20 years of patentability (TRIPS Art 32).  It will be hard to overcome the resistance from those who insist that the conventional patent system works well enough in its home-base area of physical inventions.  In the area of logical innovation however more refined means such as the invalidating quest system are indispensable, because the truly worthy innovations must be filtered out from a volatile world where innovation is daily routine and the state of the art is rarely consulted let alone documented.  Thus any abstract-logical property paradigm faces specific challenges, for which the patent system never needed to develop an answer.
\end{sect}

\begin{sect}{links}{Autres textes a lire}
\begin{itemize}
\item
{\bf {\bf Kolle 1977: Technik, Datenverarbeitung und Patentrecht\footnote{http://swpat.ffii.org/papiers/grur-kolle77/grur-kolle77.de.html}}}

\begin{quote}
A leading scholar of the software patent debate of the 1970s explains in this famous article of 1977 why, according to any coherent concept of technicity, software innovations cannot be judged as technical and therefore not be patented.  Kolle remarks that there is a ``nobody's land'' of abstract ideas, which should stay in the public domain, because their appropriability would have enormous blocking effects while generating little benefit for the software economy.
\end{quote}
\filbreak

\item
{\bf {\bf A Manifesto Concerning the Legal Protection of Computer Programs\footnote{http://wwwsecure.law.cornell.edu/commentary/intelpro/manifint.htm\#intro}}}

\begin{quote}
leading law experts argue against extension of the patent system to software and point out inadequacies of copyright.  The dual nature of software requires a system of short-term protection of investment against too early cloning.
\end{quote}
\filbreak

\item
{\bf {\bf A Model Software Petite Patent Act\footnote{http://members.aol.com/paleymark/ModelAct.htm}}}

\begin{quote}
a more detailed study on how the Third Paradigm Manifesto could be integrated into the US legal system
\end{quote}
\filbreak

\item
{\bf {\bf Stimuler la concurrence et l'innovation dans la Soci\'{e}t\'{e} d'Information\footnote{http://www.pro-innovation.org/rapport\_brevet/brevets\_plan.pdf}}}

\begin{quote}
One chapter of this report is dedicated to the question of appropriate schemes for the protection of investment in software.  Among others, a short and weak form of algorithm monopoly is envisaged.
\end{quote}
\filbreak

\item
{\bf {\bf Dr. Mayer: Moratorium statt Ausweitung der Software-Patente\footnote{http://www.m4m.de/internet/pmsoftpa.htm}}}

\begin{quote}
In autumn of 2000, the CDU/CSU fraction supports a motion in the German Federal Parliament which calls for a Third Paradigm type software property regime
\end{quote}
\filbreak

\item
{\bf {\bf Lettre ouverte : 5 initiatives de lois pour prot\'{e}ger l'innovation en informatique\footnote{http://swpat.ffii.org/lettres/patg2C/swxpatg2C.fr.html}}}

\begin{quote}
Entreprises et associations de logiciel connus demandent au politiciens de soutenir cinque initiatives l\'{e}gislatives pour la protection de l'innovation informatique contre l'abus du syst\`{e}me de brevets.  La 4\`{e}me initiative fait appel pour la cr\'{e}ation des ``syst\`{e}me adapt\'{e}s pour la stimulation de l'innovation informatique''.
\end{quote}
\filbreak

\item
{\bf {\bf 2001-02-28: Eurolinux letter to the EU patent legislators\footnote{http://www.eurolinux.org/news/101/euluxpr0101.fr.html}}}

\begin{quote}
Ici aussi les auteurs proposent de consid\'{e}rer la possibilit\'{e} d'un syst\`{e}me sui generis.
\end{quote}
\filbreak

\item
{\bf {\bf Remarks on the Patentability of Computer Software -- History, Status, Developments\footnote{http://swpat.ffii.org/dates/2001/linuxtag/jh/swplxtg017jh.en.html}}}

\begin{quote}
Un essai de Jozef Halbersztadt, examinateur de l'Office Polonais de Brevets, pr\'{e}par\'{e} pour un s\'{e}minaire a Stuttgart en Juillet 2001.  L'essai exprime son point de vue priv\'{e}.  Il raconte l'histoire de la lutte pour la forme juste des droits d'exclusion/r\'{e}mun\'{e}ration au profit des cr\'{e}ateurs de logiciels et avoue que le droit de brevet est tr\`{e}s mal adapt\'{e} mais il ne suffit pas non plus de adapter le droit d'auteur.  Il propose de relancer les id\'{e}es du Manifesto de Samuelson a l'occasion du proj\`{e}t de l\'{e}gislation europ\'{e}en actuel.
\end{quote}
\filbreak

\item
{\bf {\bf Wenning 2002-07-23: Spezialregeln f\"{u}r Software m\"{o}glich\footnote{http://www.fitug.de/debate/0207/msg00405.html}}}

\begin{quote}
Technische Probleml\"{o}sungen z.B. bei Rolltreppen lassen sich sehr wohl von reiner Informationsverarbeitung (Programmen als solchen) unterscheiden, und der Unterschied ist auch volkswirtschaftlich von Bedeutung. W\"{a}hrend Patente auf Rolltreppentechnik in dem kleinen Kreis, den sie betreffen, akzeptiert sind, schaffen Patente auf Rechenregeln einer gro{\ss}en \"{O}ffentlichkeit Probleme, ohne welche zu l\"{o}sen.  Rigo Wenning, der beim W3C als Jurist arbeitet, meint, Software hinreichend leicht von technischen Vorg\"{a}ngen abgrenzbar.  Da die generelle Ablehnung von Algorithmen-Eigentum nicht konsensf\"{a}hig sei, solle man Spezialregeln f\"{u}r Algorithmen anstreben.  Den von PA Horns \"{u}ber ein Regierungsgutachten in die Diskussion geworfenen ``Vorschlag, bei OpenSource die Verwender zu belasten'', h\"{a}lt Wenning hingegen f\"{u}r ein ``Herumdoktern an den Symptomen''.

voir aussi PA Axel Horns et les Brevets Logiciels\footnote{http://swpat.ffii.org/acteurs/horns/swpathorns.en.html}
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

