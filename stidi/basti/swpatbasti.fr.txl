<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Troisième Paradigme hors de Brevet et Droit d'Auteur

#descr: Un programme d'ordinateur est un oevre linguistique et une machine virtuelle en même temps.  Ni le droit d'auteur ni la loi de brevets étaient créés en visant au programmes d'ordinateur.  Des chercheurs et politiciens ont donc argumenté pour un %(q:Troisième Paradigme entre brevet et droit d'auteur).  Des autre ont affirmé que les idées abstraites-logiques appartiennent a une %(q:terre publique hors de la propriété intellectuelle).  A partir des droits d'exclusion il y a encore d'autres moyen pour stimuler l'innovation.

#EUs: Propriété, Brevets, Droit d'Auteur et le Domaine Publique

#Wnd: Brevets softes pour logique durcie

#Lgl: logilège

#Nee: Privilége d'usage sur des innovation abstraites-logiques

#Lin: Logidende

#Laa: dividende d'innovation logicielle

#Ret: Inspirations pour reformes du système de brevets

#Fea: Autres textes a lire

#Asg: Les innovations abstraites-logiques sont, selon la tradition de loi de l'Europe et ailleurs, situés dans le domaine publique, qui à aussi été %(gk:appellée) %(q:terre sans propriétaire) de quelques savants de dogme juridique.

#knr: concrète

#For: forme

#asr: abstrait

#Ide: idée

#MmW: objét physiques

#Mtr: matière

#Egn: propriété matérielle privée

#Per: brevets et certificats d'utilité

#Een: produit

#Vfr: procédé

#Gce: déscription

#IeG: objéts logiques

#GWm: pensée, information

#Ube: droit d'auteur

#NdW: propriété intellectuelle publique

#Uri: ordinateur programmé

#PmA: comportement de programme

#Prt: texte de programme

#Bhn: Bei den physischen Abstraktionen gibt es ein Gefälle vom materiellen Erzeugnis über das mehr oder weniger eng damit verbundene Verfahren bis hin zur vollends immateriellen Verfahrensbeschreibung.  Im Bereich der logischen Gegenstände (Logikalien) steht allen dreien lediglich das Computerprogramm als Entsprechung gegenüber, welches dank seiner Immaterialität zahlreiche Aspekte in sich vereinigt.

#Iee: In dem Maße, wie ein Eigentumsgegenstand sich von der Materie löst, kann er durch kostenarme Vervielfältigung Gemeingut werden.  Gebrauchsmuster gibt es nur für materielle Erzeugnisse, Patente auch für damit zusammenhängende Verfahren, nicht aber für Verfahrensbeschreibungen, obwohl letztere auch einen Marktwert haben, der durch Nachahmer gemindert werden könnte.  Die Gedanken sind frei. Informationswerke verbreiten sich noch schneller als Gedanken.  Je leichter vervielfältigbar ein Gegenstand ist, desto weniger ist man geneigt, seine Verbreitung zu verlangsamen, indem man Eigentumsansprüche darauf anerkennt.  Wenn die Vervielfältigungskosten bei Null liegen, errechnet sich der Verlangsamungsfaktor als eine Division durch Null, d.h. der Schmerz tendiert gegen unendlich.

#Etd: Ein ähnliches Gefälle liegt zwischen den %(q:Erfindungen) und %(q:Schöpfungen) auf der einen und den %(q:Entdeckungen) und Natur-Ressourcen auf der anderen Seite.  Erstere sind konkret und einmalig, letztere werden in immer der gleichen Weise von jedem vorgefunden, der an einem bestimmten Ort vorbeikommt, sei es in der physischen Welt oder in mathematisch-logischen Räumen.

#AiW: Auf all diesen Gefälle-Achsen liegen die typischen Software-Patente ganz rechts unten in dem Raum des naturrechtlichen Gemeineigentums, des %(q:Niemandslandes des Geistigen Eigentums).  Auf Logikalien 20 Jahre lange Monpole zu erteilen ist ebenso naturrechtswidrig wie am anderen Ende des Extrems die Vergesellschaftung aller Küchengeräte in der %(q:Volkskommunenbewegung), mit der Mao Tse-tung in China 1960 eine Hungerkatastrophe erzeugte.  Alle Eigentumsregime sind möglich, aber die Gesetzmäßigkeiten schlagen zurück und wir zahlen den Preis.

#Arv: Viele Diskutanten äußern seit jeher grundsätzliche Bedenken gegen Eigentumsrechte an abstrakt-logischen Konzepten:  einerseits belohnen solche Eigentumsrechte meist nicht den wirklich aufwendigen Teil des Software-Innovationsprozesses, andererseits schränken sie aber die Innovationsfreiheit besonders empfindlich ein.

#VaW: Viele Software-Entwickler und IT-Unternehmer halten das Software-Urheberrecht für %(q:völlig ausreichend) und geradezu %(q:maßgeschneidert).

#Dge: However here and there we can find extreme counter-examples.  Some mathematical theorems were known for centuries before someone came up with a successful proof.  Yet even this does not mean that a simple and brute monopoly on all computer program implementations of an abstract-logical innovation is the price that society has to pay.  Besides patent-like monopolies, less disruptive remuneration systems should be envisaged for this realm.

#Eti: Innovative processes for whose implementation not more than a new computer program running on known hardware is needed, could still be monopolised as long as this monopoly can be enforced without impeding the dissemination of computer programs, i.e. in those cases in which the process does not run on a universal computer.  Thus, if a car maker does not choose to lay out his motor control device for customer-side reprogramming (making it more costly and less safe), he would probably prefer to pay moderate license fees to a patent owner.

#dWs: die Anwendung des Verfahrens für geschäftliche Zwecke

#dei: die Umsetzung des Verfahrens in %(po:Eigentumsgegenständen)

#iiS: in proprietärer Software

#dEn: die Nutzung zur Erreichung von Interoperabilität

#dfa: die Veröffentlichung in %(fi:frei weiterentwickelbaren Informationsgebilden) mit einem angemessenen Verweis auf die Nutzungseinschränkungen

#Plr: freier Quelltext

#DfW: Der Inhaber eines %(ll:Logilegs) genießt ab dem Tag der Offenlegung eines innovativen logischen Verfahrens einige Jahre lang Ausschlussrechte auf %{L1}.

#Leg: Logikaliennutzungsprivileg

#NbW: Nicht untersagbar sind %{L0}

#Ogt: Offenzulegen ist mindestens eine Referenzimplementation und ein daran angelehnter Anspruchsbereich.  Diese Offenlegung muss strengen formellen und sachlichen Anforderungen genügen, deren Ziel es ist, sie für die Öffentlichkeit leicht zugänglich und überprüfbar zu machen.

#Deg: Das Logileg gilt sofort mit der Offenlegung. Die Gültigkeitsprüfung obliegt dem Logileginhaber.  Während der Laufzeit kann jedermann Einspruch erheben.  Gebühren fallen nicht an, aber der Logileginhaber hinterlegt notarisch eine Kaution von 5000 EUR, die zur Belohnung an den ersten Einspruchsführer ausgezahlt wird, dem es gelingt, die Ungültigkeit des Logilegs nachzuweisen. Bei Nachweis eines Mangels fällt das ganze Logileg.  Nachbesserungen sind nicht gestattet.

#Djh: Initially only the %(q:problem to be solved by the innovation) is published together with the prior art, and the whole logilege description is made available only in an unreadable encrypted form and decrypted after one month.  During this month, any person may publish solutions to the problem, which become prior art.  If such prior art invalidates the logilege, the deposit sum is paid to the author of the earliest invalidating publication.

#Dei: The applicant may allow more time for the problem solving quest and in return obtain a longer exclusion period, e.g. one extra year of validity for each extra week of problem solving quest.

#Irt: Instead of exclusion rights we here grant non-exclusive remuneration rights.  Such rights directly reward innovators without interfering with other people's programming freedom or with interoperability.  Our informational infrastructure thus remains a property-free Commons zone.  The logidend is payed out of funds from taxes on computing devices or the like.  The value of each logidend is determined by a sophisticated social engineering scheme rather than by the conventional %(q:free market) approach.

#Dae: The logidend excludes nobody from use of the idea.  However verbal tribute should be paid to the innovator wherever the idea is used.

#Tvf: The rules for sorting out real innovations are similar to those of the %(q:logilege) outlined above.

#Lde: Sonderrechte obiger Art könnten in dem abstrakt-logischen Niemandsland vergeben werden, wo mangels Konkretheit und physischer Substanz (Technizität) weder das Patentsystem noch das Urheberrecht greift.  Einerseits erfordert die Leichtigkeit und Schnelllebigkeit abstrakt-logischer Innovationen eine besonders zügige und unbürokratische Verfahrensabwicklung, andererseits beeinträchtigen Privilegien in diesem Bereich die Öffentlichkeit in unerhörtem Maße.  %(q:Geistiges Eigentum) an abstrakten Ideen kommt einer %(q:Geistigen Umweltverschmutzung) gleich, für die der Privilegsinhaber besondere ausgleichende Leistungen zu erbringen hat.

#Vem: Many aspects of the logilege could also serve to inspire a reform of the existing patent system.  But patent lawyers will contend: %(q:if it ain't broke don't fix it).  Within the %(tr:framework of patent treaties), reforms may meet even more obstacles, starting from a minimum requirement of 20 years of patentability (TRIPS Art 32).  It will be hard to overcome the resistance from those who insist that the conventional patent system works well enough in its home-base area of physical inventions.  In the area of logical innovation however more refined means such as the invalidating quest system are indispensable, because the truly worthy innovations must be filtered out from a volatile world where innovation is daily routine and the state of the art is rarely consulted let alone documented.  Thus any abstract-logical property paradigm faces specific challenges, for which the patent system never needed to develop an answer.

#Aos: A leading scholar of the software patent debate of the 1970s explains in this famous article of 1977 why, according to any coherent concept of technicity, software innovations cannot be judged as technical and therefore not be patented.  Kolle remarks that there is a %(q:nobody's land) of abstract ideas, which should stay in the public domain, because their appropriability would have enormous blocking effects while generating little benefit for the software economy.

#lWr: leading law experts argue against extension of the patent system to software and point out inadequacies of copyright.  The dual nature of software requires a system of short-term protection of investment against too early cloning.

#ahn: a more detailed study on how the Third Paradigm Manifesto could be integrated into the US legal system

#Org: One chapter of this report is dedicated to the question of appropriate schemes for the protection of investment in software.  Among others, a short and weak form of algorithm monopoly is envisaged.

#ImW: In autumn of 2000, the CDU/CSU fraction supports a motion in the German Federal Parliament which calls for a Third Paradigm type software property regime

#IWn: Entreprises et associations de logiciel connus demandent au politiciens de soutenir cinque initiatives législatives pour la protection de l'innovation informatique contre l'abus du système de brevets.  La 4ème initiative fait appel pour la création des %(q:système adaptés pour la stimulation de l'innovation informatique).

#AWt: Ici aussi les auteurs proposent de considérer la possibilité d'un système sui generis.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbasti ;
# txtlang: fr ;
# multlin: t ;
# End: ;

