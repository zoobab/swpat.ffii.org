\begin{subdocument}{swpatbasti}{Third Paradigm between Patent and Copyright Law}{http://swpat.ffii.org/analysis/suigen/swpatbasti.en.html}{Workgroup\\swpatag@ffii.org\\english version 2003/12/16 by PILCH Hartmut\footnote{http://www.ffii.org/~phm}}{A computer program is a linguistic work and a virtual machine at the same time.  Neither copyright nor patent law were designed with computer programs in mind.  Some scholars and politicians have therefore argued for a ``Third Paradigm between patent and copyright law'', also called \emph{specially tailored right} or ius sui generis.  Others have called abstract-logical ideas a ``nobody's land of intellectual property'' and demanded that it should be kept free of all property claims.  Beside the granting of exclusion rights, there are other ways in which information innovation can be stimulated and rewarded.}
\begin{sect}{pubpriv}{Property, Patents, Copyright and the Public Domain}
Abstract-logical innovations are, according to traditional property law of Europe and elswhere, situated in the public domain, also regretfully called\footnote{http://swpat.ffii.org/papers/grur-kolle77/grur-kolle77.de.html} ``nobody's land'' by some intellectual property lawyers:

\begin{center}
\begin{center}
\begin{tabular}{|C{22}|C{22}|C{36}|}
\hline
 & concrete\par

form & abstract\par

idea\\\hline
{\bf physical\par

matter} & private material property & patents \& utility certificates\par

\begin{center}
\begin{tabular}{|C{22}|C{22}|C{36}|}
\hline
product & process & description\\\hline
\end{tabular}
\end{center}\\\hline
{\bf logical\par

mind, information} & copyright & public intellectual property\par

\begin{center}
\begin{tabular}{|C{22}|C{22}|C{36}|}
\hline
programmed computer & program behaviour & program text\\\hline
\end{tabular}
\end{center}\\\hline
\end{tabular}
\end{center}
\end{center}

In the case of physical abstractions, there is a gradation from the material product to a process more or less closely connected thereto to a fully immaterial description of that process.  In the field of logical objects, all three correspond to a computer program, which unites multiple aspects in one.

To the degree that a property object is detached from matter, it tends to become a public good through cheap replication.  Utility certificates are granted only for material products while patents are also granted for processes connected thereto, but not for process descriptions, although the latter have a market value which is often diminished by imitators.  The thoughts are free.  Information spreads even faster than thoughts.   The more easily an object can be replicated, the less people are inclined to slow down its diffusion by property titles.  If the cost of replication is zero, than the slow-down factor would be ``something divided by zero'', i.e. infinitely painful.

There is a similar gradation between ``creations'' and ``inventions'' on the one side and ``discoveries'' and natural ressources on the other.  While the former are concrete and individual, the latter are found in identical form by anyone who comes across a certain place, be it in the physical world or in some mathematically constructed space.

On all these gradation axes, the typical software patents are situated in the lower right corner, in what seems to be a public domain by natural law.  To hand out 20 year monopolies on software concepts thus seems as much counter-natural as on the other extreme the collectivization of kitchen utensils in the ``people's commune movement'', by which Mao Tse-tung created a famine in China of 1960.  Humans can establish any property regime they want, but natural law lashes back and we pay the price.

Many Discutants have warned against allowing any type of appropriation of abstract-logical concepts:  on the one hand side such property rights would very often not compensate the really cost-intensive part of the software innovation process, while on the other hand side generating high public costs by limiting important freedoms.

Many software developpers and IT entrepreneurs consider software copyright to be ``quite sufficient'' or even ``perfectly tailored to the reality of software innovation''.

However here and there we can find extreme counter-examples.  Some mathematical theorems were known for centuries before someone came up with a successful proof.  Yet even this does not mean that a simple and brute monopoly on all computer program implementations of an abstract-logical innovation is the price that society has to pay.  Besides patent-like monopolies, less disruptive remuneration systems should be envisaged for this realm.
\end{sect}

\begin{sect}{softent}{Soft Patents for Hardware Logics}
Innovative processes for whose implementation not more than a new computer program running on known hardware is needed, could still be monopolised as long as this monopoly can be enforced without impeding the dissemination of computer programs, i.e. in those cases in which the process does not run on a universal computer.  Thus, if a car maker does not choose to lay out his motor control device for customer-side reprogramming (making it more costly and less safe), he would probably prefer to pay moderate license fees to a patent owner.
\end{sect}

\begin{sect}{logileg}{Logilege: Use Privilege for Abstract-Logical Innovations}
The owner of a \emph{logilege} (\emph{logical innovation use privilege}) publishes an innovative abstract-logical method and enjoys exclusion rights on \begin{enumerate}
\item
application of the method for commercial purposes

\item
implementation of the method in property-restricted objects (e.g. in proprietary software)
\end{enumerate} for a fixed number of years starting from the day of publication.
Nobody may be excluded from \begin{enumerate}
\item
the right to use the method for achieving interoperability

\item
the right to publish the method in freely republishable information works (e.g. free source code) with an appropriate pointer to the usage restrictions
\end{enumerate}

The published logilege description comprises one or more reference implementations and a set of claims.  The logilege description must satisfy strict formal and material requirements so as to make it easy for the public to access the description and test its validity.

The logilege enters into force on the day of its publication.  There is no examination procedure.  The owner guarantees that claims are valid by paying a deposit of 5000 EUR, which will be paid to the first person who succeeds in proving the invalidity of the logilege.  One serious flaw, such as an inadmissible claim, suffices to invalidate the whole logilege.  Posterior narrowing of claims is not allowed.

Initially only the ``problem to be solved by the innovation'' is published together with the prior art, and the whole logilege description is made available only in an unreadable encrypted form and decrypted after one month.  During this month, any person may publish solutions to the problem, which become prior art.  If such prior art invalidates the logilege, the deposit sum is paid to the author of the earliest invalidating publication.

The applicant may allow more time for the problem solving quest and in return obtain a longer exclusion period, e.g. one extra year of validity for each extra week of problem solving quest.
\end{sect}

\begin{sect}{logidend}{Logidend: Logical Innovation Dividend}
Instead of exclusion rights we here grant non-exclusive remuneration rights.  Such rights directly reward innovators without interfering with other people's programming freedom or with interoperability.  Our informational infrastructure thus remains a property-free Commons zone.  The logidend is payed out of funds from taxes on computing devices or the like.  The value of each logidend is determined by a sophisticated social engineering scheme rather than by the conventional ``free market'' approach.

The owner of a logilege publishes an innovative abstract-logical method and enjoys exclusion rights on L1 for a fixed number of years starting from the day of publication.

The logidend excludes nobody from use of the idea.  However verbal tribute should be paid to the innovator wherever the idea is used.

The rules for sorting out real innovations are similar to those of the ``logilege'' outlined above.
\end{sect}

\begin{sect}{patref}{Inspiration for Patent Reform}
``Third Paradigm'' rights can be granted for those innovations which due to lack of concreteness or physical substance (technicity) fall out of the classical patent system.  The lightness and shortlivedness of abstract-logical innovations requires a fast and unbureuacratic procedure, while the great public cost, that any privilege on abstract-logical innovations tends to generate, demands a high degree of responsibility on the part of the privilege owner.

Many aspects of the logilege could also serve to inspire a reform of the existing patent system.  But patent lawyers will contend: ``if it ain't broke don't fix it''.  Within the framework of patent treaties\footnote{http://swpat.ffii.org/analysis/trips/swpattrips.en.html}, reforms may meet even more obstacles, starting from a minimum requirement of 20 years of patentability (TRIPS Art 32).  It will be hard to overcome the resistance from those who insist that the conventional patent system works well enough in its home-base area of physical inventions.  In the area of logical innovation however more refined means such as the invalidating quest system are indispensable, because the truly worthy innovations must be filtered out from a volatile world where innovation is daily routine and the state of the art is rarely consulted let alone documented.  Thus any abstract-logical property paradigm faces specific challenges, for which the patent system never needed to develop an answer.
\end{sect}

\begin{sect}{links}{Further Reading}
\begin{itemize}
\item
{\bf {\bf Kolle 1977: Technik, Datenverarbeitung und Patentrecht\footnote{http://swpat.ffii.org/papers/grur-kolle77/grur-kolle77.de.html}}}

\begin{quote}
A leading scholar of the software patent debate of the 1970s explains in this famous article of 1977 why, according to any coherent concept of technicity, software innovations cannot be judged as technical and therefore not be patented.  Kolle remarks that there is a ``nobody's land'' of abstract ideas, which should stay in the public domain, because their appropriability would have enormous blocking effects while generating little benefit for the software economy.
\end{quote}
\filbreak

\item
{\bf {\bf A Manifesto Concerning the Legal Protection of Computer Programs\footnote{http://wwwsecure.law.cornell.edu/commentary/intelpro/manifint.htm\#intro}}}

\begin{quote}
leading law experts argue against extension of the patent system to software and point out inadequacies of copyright.  The dual nature of software requires a system of short-term protection of investment against too early cloning.
\end{quote}
\filbreak

\item
{\bf {\bf A Model Software Petite Patent Act\footnote{http://members.aol.com/paleymark/ModelAct.htm}}}

\begin{quote}
a more detailed study on how the Third Paradigm Manifesto could be integrated into the US legal system
\end{quote}
\filbreak

\item
{\bf {\bf Stimuler la concurrence et l'innovation dans la Soci\'{e}t\'{e} d'Information\footnote{http://www.pro-innovation.org/rapport\_brevet/brevets\_plan.pdf}}}

\begin{quote}
One chapter of this report is dedicated to the question of appropriate schemes for the protection of investment in software.  Among others, a short and weak form of algorithm monopoly is envisaged.
\end{quote}
\filbreak

\item
{\bf {\bf Dr. Mayer: Moratorium instead of Extension of Software Patents\footnote{http://www.m4m.de/internet/pmsoftpa.htm}}}

\begin{quote}
In autumn of 2000, the CDU/CSU fraction supports a motion in the German Federal Parliament which calls for a Third Paradigm type software property regime
\end{quote}
\filbreak

\item
{\bf {\bf Open Letter: 5 Law Initiatives to Protect Information Innovation\footnote{http://swpat.ffii.org/letters/patg2C/swxpatg2C.en.html}}}

\begin{quote}
Renowned software companies and associations call politicians to support five law initiatives for the protection of information innovation against the abuse of the patent system. Initiative no. 4 calls for the creation of ``adequate systems for the stimulation of information innovation''.
\end{quote}
\filbreak

\item
{\bf {\bf 2001-02-28: Eurolinux letter to the EU patent legislators\footnote{http://www.eurolinux.org/news/101/euluxpr0101.en.html}}}

\begin{quote}
Here too the authors recommend to consider a sui generis protection
\end{quote}
\filbreak

\item
{\bf {\bf Remarks on the Patentability of Computer Software -- History, Status, Developments\footnote{http://swpat.ffii.org/events/2001/linuxtag/jh/swplxtg017jh.en.html}}}

\begin{quote}
An essay by Jozef Halbersztadt, patent examiner at the Polish Patent Office, prepared for a lecture in Stutgart in July 2001.  The essay expresses his private views on the needs to accomodate the peculiarities of software by specially tailored exclusion/reward rights rather than by just adapting copyright or, even worse, patent law.  It narrates the history of software law as a chaotic evolution where the horses determined the direction of the cart until the cart ran against a wall and the driver woke up.  Halbersztadt proposes a relaunch of the Samuelson-Kappor-Davis Manifesto of 1994 with various optional modifications and ways to introduce those ideas into the current European legislation process.
\end{quote}
\filbreak

\item
{\bf {\bf Wenning 2002-07-23: Spezialregeln f\"{u}r Software m\"{o}glich\footnote{http://www.fitug.de/debate/0207/msg00405.html}}}

\begin{quote}
Technische Probleml\"{o}sungen z.B. bei Rolltreppen lassen sich sehr wohl von reiner Informationsverarbeitung (Programmen als solchen) unterscheiden, und der Unterschied ist auch volkswirtschaftlich von Bedeutung. W\"{a}hrend Patente auf Rolltreppentechnik in dem kleinen Kreis, den sie betreffen, akzeptiert sind, schaffen Patente auf Rechenregeln einer gro{\ss}en \"{O}ffentlichkeit Probleme, ohne welche zu l\"{o}sen.  Rigo Wenning, der beim W3C als Jurist arbeitet, meint, Software hinreichend leicht von technischen Vorg\"{a}ngen abgrenzbar.  Da die generelle Ablehnung von Algorithmen-Eigentum nicht konsensf\"{a}hig sei, solle man Spezialregeln f\"{u}r Algorithmen anstreben.  Den von PA Horns \"{u}ber ein Regierungsgutachten in die Diskussion geworfenen ``Vorschlag, bei OpenSource die Verwender zu belasten'', h\"{a}lt Wenning hingegen f\"{u}r ein ``Herumdoktern an den Symptomen''.

see also PA Axel H. Horns and Software Patents\footnote{http://swpat.ffii.org/players/horns/swpathorns.en.html}
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

