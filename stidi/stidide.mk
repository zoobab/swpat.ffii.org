stidide.lstex:
	lstex stidide | sort -u > stidide.lstex
stidide.mk:	stidide.lstex
	vcat /ul/prg/RC/texmake > stidide.mk


stidide.dvi:	stidide.mk stidide.tex
	latex stidide && while tail -n 20 stidide.log | grep references && latex stidide;do eval;done
	if test -r stidide.idx;then makeindex stidide && latex stidide;fi
stidide.pdf:	stidide.mk stidide.tex
	pdflatex stidide && while tail -n 20 stidide.log | grep references && pdflatex stidide;do eval;done
	if test -r stidide.idx;then makeindex stidide && pdflatex stidide;fi
stidide.tty:	stidide.dvi
	dvi2tty -q stidide > stidide.tty
stidide.ps:	stidide.dvi	
	dvips  stidide
stidide.001:	stidide.dvi
	rm -f stidide.[0-9][0-9][0-9]
	dvips -i -S 1  stidide
stidide.pbm:	stidide.ps
	pstopbm stidide.ps
stidide.gif:	stidide.ps
	pstogif stidide.ps
stidide.eps:	stidide.dvi
	dvips -E -f stidide > stidide.eps
stidide-01.g3n:	stidide.ps
	ps2fax n stidide.ps
stidide-01.g3f:	stidide.ps
	ps2fax f stidide.ps
stidide.ps.gz:	stidide.ps
	gzip < stidide.ps > stidide.ps.gz
stidide.ps.gz.uue:	stidide.ps.gz
	uuencode stidide.ps.gz stidide.ps.gz > stidide.ps.gz.uue
stidide.faxsnd:	stidide-01.g3n
	set -a;FAXRES=n;FILELIST=`echo stidide-??.g3n`;source faxsnd main
stidide_tex.ps:	
	cat stidide.tex | splitlong | coco | m2ps > stidide_tex.ps
stidide_tex.ps.gz:	stidide_tex.ps
	gzip < stidide_tex.ps > stidide_tex.ps.gz
stidide-01.pgm:
	cat stidide.ps | gs -q -sDEVICE=pgm -sOutputFile=stidide-%02d.pgm -
stidide/stidide.html:	stidide.dvi
	rm -fR stidide;latex2html stidide.tex
xview:	stidide.dvi
	xdvi -s 8 stidide &
tview:	stidide.tty
	browse stidide.tty 
gview:	stidide.ps
	ghostview  stidide.ps &
print:	stidide.ps
	lpr -s -h stidide.ps 
sprint:	stidide.001
	for F in stidide.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	stidide.faxsnd
	faxsndjob view stidide &
fsend:	stidide.faxsnd
	faxsndjob jobs stidide
viewgif:	stidide.gif
	xv stidide.gif &
viewpbm:	stidide.pbm
	xv stidide-??.pbm &
vieweps:	stidide.eps
	ghostview stidide.eps &	
clean:	stidide.ps
	rm -f  stidide-*.tex stidide.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} stidide-??.* stidide_tex.* stidide*~
tgz:	clean
	set +f;LSFILES=`cat stidide.ls???`;FILES=`ls stidide.* $$LSFILES | sort -u`;tar czvf stidide.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
