<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII-Interessenlage zur EU-Softwarepatentrichtlinie

#descr: Was sind die wichtigsten Interessen der Softwareschaffenden, ihre
Wünsche nach Freiheiten und Ausschlussrechten?  Wie können sie in der
Softwarepatentrichtlinie realisiert werden?  Was für andere Interessen
existieren? Wo gibt es Verhandlungspielraum?

#uit: Nur Patentierbarkeitsausschlüsse harmonisieren den %(q:Status quo)

#Wti: Veröffentlichungsfreiheit

#tWW: Freiheit, Computer im Büro- und Netzwerkumfeld einzusetzen

#yWP: Klärung der Einschlussausdrücke, die TRIPs auferlegt

#uoW: Untersuchungsformalia gehören nicht ins Gesetz

#Foo: Interoperationsrecht als Konkretisierung von Art 30 TRIPs

#Wfi: Wir haben bereits einen guten Rechtsrahmen, einige Patentgerichte
respektieren ihn jedoch nicht.  Aber sie sind unter starkem und
wachsenden öffentlichen Druck, wieder zu einer vernünftigen Praxis in
Übereinstimmung mit dem Gesetz zurückzukehren.  Der Druck wird
wachsen, auch wenn es keine Richtlinie gibt.  Er kann schneller
wachsen, falls wir eine gute Richtlinie verabschieden.  Allein durch
eine schlechte Richtlinie kann das europäische Patentwesen (d.h. die
nationalen Patentbeamten, welche die Arbeitsgruppe des Ministerrats
stellen, und der Verwaltungsrat des Europäischen Patentamts) seine
verhängnisvolle Praxis festschreiben.

#min: Wir können fast jede Richtlinie akzeptieren, solange sie nur aus
klaren und einfachen Patientierbarkeitsausschlüssen besteht.

#tii: Artikel 52(2) EPÜ besteht aus solchen Ausschlüssen. Er sagt in klaren
und einfachen Worten, was %(e:nicht) eine %(e:Erfindung) im Sinne des
Patentrechts ist.  Wir können keine Richtlinie akzeptieren, die diese
klaren Worte durch zu wirkungslosen Leerformeln umdeutet, wie es etwa
durch Art 4A des Ratstextes vom Mai 2004 geschieht, der nur enge
Software-Patentansprüche (auf %(q:Ausdruck) in %(q:Quell- oder
Binärform)) verhindert aber breite erlaubt.

#ued: Wir können keine einschließenden Formulierungen akzeptieren, wie
%(bc:X soll patentierbar sein), wo X nicht durch klare und einfache
Ausschlusskriterien abgegrenzt ist. Formulierungen im Rats-Stil der
Art %(bc:X soll nicht patentierbar sein, außer wenn [Bedingung, die
immer erfüllt ist]) sind sogar noch weniger duldbar.

#eWW: Von keiner Richtlinie, die Formulierungen des obenstehenden Typs
enthält, kann behauptet werden, sie %(q:harmonisiere den Status quo)
oder stoppe ein %(q:Abdriften zur Patentierung von Geschäftsmethoden).

#trp: Das Wichtiste zuerst.  Unten versuchen wir, die Ausschlusskriterien
für Patentierbarkeit und Patentdurchsetzbarkeit in ihrer Priorität
anzugeben, da sie aufeinander aufbauen.  Auf der Zweiten zu punkten,
setzt die Erfüllung der Ersten voraus.

#rmp: Das Hauptinteresse unserer Unterstützer liegt darin, Software frei von
Patenten zu halten, allein durch das Urheberrecht geregelt.  Selbst
wenn es z. B. Patente auf das vielzitierte %(q:Antiblockiersystem),
%(q:Waschmaschinen), %(q:intelligente Staubsauger) gibt, so sollen sie
nur für die Hersteller und Nutzer dieser Geräte anwendbar sein, nicht
aber für die Personen, die Software für diese Geräte herstellen oder
bereitstellen (= Kontrollogik, ähnlich zu Handbüchern).

#rpi: Unsere erste und grundlegendste Forderung ist %(q:keine
Programmansprüche, keine direkte oder indirekte Verletzung durch
Softwarevertrieb).  Das Europäische Parlament hat diese Forderung
mittels Paragraph 5A erfüllt.  Im Gegensatz dazu hat der Rat durch
Einführung von Programmansprüchen in Art 5(2) die einzige Brücke zu
konstruktiven Verhandlungen eingerissen, welche die Europäische
Kommission je gebaut hat.  Wenn sie darauf bestehen, diese Brücke
eingerissen zu lassen, ist alles aus.

#eec: Wir können keine Ausschlussrechte auf die Verwendung von Gerät
akzeptieren, das ausschließlich aus allgemein verwendbaren Computern
besteht. Patentansprüche jeglicher Art (Prozess, Gerät, System und
Methode, u.dgl.m.) sind nicht akzeptabel, wenn der Beitrag zum Stand
der Technik aus reiner Datenverarbeitung besteht (z. B. Anweisungen
zum Betrieb von allgemein verwendbaren Datenverarbeitungsanlagen).

#hnW: Das Parlament hat dieses Problem mit einer einfachen und klaren
Aussage im Geist von Art 52 EPÜ gelöst:

#rot: Datenverarbeitung ist kein Gebiet der Technik im Sinne des
Patentrechts.

#WWn: In einem ähnlichen Paragraphen hat das EP einige Merkmale angegeben,
die keinen %(q:technischen Beitrag) darstellen: z. B. %(q:Verbesserung
der Rechenleistung).

#soe: Da laut Paragraph 27 %(tr:TRIPs), %(q:Patente auf allen Gebieten der
Technik verfügbar sein sollen, vorausgesetzt, dass ... sie für die
industrielle Anwendung einsetzbar sind), müssten die Begriffe
%(q:Technologie), %(q:technisch), %(q:Erfindung) und %(q:industriell)
vom Gesetzgeber definiert werden. Wegen des einschliessenden
Charakters der TRIPs-Aussage ist es nicht länger ausreichend, diese
Begriffe durch Fallrecht zu klären. Das Europaparlament hat die
benötigten Definitionen in seiner ersten Lesung bereitgestellt.

#cdo: Das Parlament hat auch den Einschlussbegriff
%(q:computer-implementierte Erfindung), der ohne Not vom Europäischen
Patentwesen eingeführt wurde, geklärt.  Die Definition des EP muss
beibehalten werden, der Begriff selbst sollte nach Möglichkeit
vermieden werden.  Im Richtlinientitel sollte direkter formuliert
werden, wie etwa %(q: über die Grenzen der Patentierbarkeit in Bezug
auf Datenverarbeitung und ihre Anwendungsgebiete).

#tco: Ein Anwalt kann %(q:einen Computer, gekennzeichnet dadurch, dass er
Fenster auf seinem Bildschirm nach Schema X anordnet,) beanspruchen.
In diesem Fall ist die wirkliche %(q:Erfindung) (= %(q:Beitrag)) das
%(q:Schema X), und solche Schemata sind nach Paragraph 52 EPÜ keine
Erfindungen.

#nno: Unter dem neuen %(pb:formalistischen Ansatz) der vom Europäischen 
Patentamt (EPA) im Jahr 2000 eingeführt wurde, besteht eine
%(q:Erfindung) immer aus dem %(q:Anspruch als Ganzen), d. h. in dem
obenstehenden Beispiel aus dem %(q:Computer), der natürlich technisch
und patentierbar ist, wogegen der Begriff %(q:technischer Beitrag)
sich möglicherweise auf das bezieht, was zuvor %(q:Erfindung) genannt
wurde.  Aber selbst der Begriff %(q:technischer Beitrag) wird
unintuitiv verwendet, was wenige Personen ausserhalb des EPA
verstehen.  Z. B. in dem obigen Beispiel kann es einen %(q:technischen
Beitrag in der erfinderischen Tätigkeit) geben, da das %(q:technische
Problem) der %(q:Verbesserung der Nutzung der Bildschirmfläche) durch
die %(q:erfinderische Tätigkeit) zwischen %(q:nächststehendem Stand
der Technik) und der %(q:Erfindung) gelöst wird.

#lfi: Entscheidung Pensionssystem von 2000

#ouW: Diese obskure Schlussweise des EPA wird nicht im Patentrecht insgesamt
verwendet, sondern nur bei den %(q:computer-implementierten
Erfindungen). Sie entstand in den späten 90ern als eine Methode zur
Verdrehung des existierenden Rechts, um Softwarepatente zuzulassen.
Mit der Festschreibung dieser Logik in einer Richtlinie erlaubt die EU
dem EPA willkührliche Praktiken.  Darüberhinaus macht die Vermischung
von %(q:technischem Beitrag) und %(q:erfinderischem Schritt) in der
neuen EPA-Doktrin es den nationalen Patentämtern, die keine Prüfung
zum Stand der Technik machen, unmöglich, Software- oder
Geschäftsmethodenpatente zurückzuweisen.  Zuletzt führt die neue Lehre
ein %(e:Sui-generis-Softwarepatentrecht) ein, welches nicht mit den
generellen Prinzipien des Patentrechts anderer Gebiete harmoniert und
in den Augen einiger Rechtsgelehrter in den USA und Europa als ein
Bruch des TRIPs-Vertrags gesehen werden kann.

#Wat: Der Versuch des Ministerrats, jedes Anspruchsobjekt eine
%(q:Erfindung) zu nennen, die %(q:Erfindung) mit dem %(q:Anspruch als
Ganzen) gleichzusetzen oder zwischen %(q:Erfindung) und
%(q:technischem Beitrag) zu unterscheiden, müssen abgelehnt werden.
Wie noch in den %(eg:Prüfungsrichtlinien des EPA von 1978) betont,
soll der Prüfer %(q:die Form oder Art der Ansprüche ignorieren und
sich auf den Inhalt konzentrieren, um den neuen Beitrag zu
identifizieren, welche die angenommene %(c:Erfindung) zum Stand der
Technik beisteuert. Wenn dieser Beitrag keine Erfindung ist, dann gibt
es keine patentierbaren Inhalte).  Es wäre ausreichend, dieses Prinzip
erneut zu bestätigen.  Die genaue Methode, wie der Prüfer den
Beitrag/die Erfindung bestimmt, muss nicht festgeschrieben werden.
Sollte eine Definition dennoch gewünscht sein, kann nur der Vorschlag
des Parlaments in der ersten Lesung genommen werden, d. h.
%(q:Identifikation des technischen Beitrags durch Abzug aller
nicht-technischen und nicht-neuartigen Eigenschaften des beanspruchten
Objekts).

#ioo: Paragraph 30 TRIPs verlangt Klarstellung bezüglich Interoperabilität
auf der Ebene des Gesetzgebers, nicht auf der Ebene des Fallrechts.

#WWW: Kommission und Rat täuschen vor, z. B. in Artikel 6, dass sie
Interoperationsfreiheit im Geiste der Softwareurheberrechtslinie von
1991 wollen.

#Wsp: Tatsächlich besteht ein Klärungsbedarf, und das Parlament hat die
richtige Antwort gegeben: Paragraph 6a.  Dessen Version von
CULT/ITRE/JURI (Kultur-, Industrie- und Rechtsausschuss), der von der
Luxemburger Delegation im Rat unterstützt wurde, dient diesem Zweck
genausogut wie die des parlamentarischen Beschlusses.  In beiden
Fällen können einige Formulierungsdetails verbessert werden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: blasum ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatnitcu ;
# txtlang: de ;
# multlin: t ;
# End: ;

