\select@language {french}
\contentsline {section}{\numberline {1}Seules les exclusions de la brevetabilit\'{e} peuvent aider \`{a} ``harmoniser le statu quo''}{1}{section.1}
\contentsline {section}{\numberline {2}Libert\'{e} de publication}{2}{section.2}
\contentsline {section}{\numberline {3}Libert\'{e} d'utiliser des ordinateurs au bureau et dans des environnements connect\'{e}s}{2}{section.3}
\contentsline {section}{\numberline {4}Clarifier les termes inclusifs que les ADPIC imposent}{3}{section.4}
\contentsline {section}{\numberline {5}Ne pas codifier les formalismes pour l'examen}{3}{section.5}
\contentsline {section}{\numberline {6}\'{E}tablir la libert\'{e} d'interop\'{e}ration comme une concr\'{e}tisation de l'article 30 des ADPIC}{4}{section.6}
