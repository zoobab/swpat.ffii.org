<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Intérêts de la FFII sur la directive de l'UE concernant les brevets
logiciels

#descr: Que sont la liberté fondamentale et l'exclusivité des intérêts des
créateurs et utilisateurs de logiciels Comment se traduisent-ils dans
le langage de la Directive sur les brevets logiciels ? Quels autres
intérêts sont en jeu ? Où peut-on trouver un terrain pour des
négociations significatives ?

#uit: Seules les exclusions de la brevetabilité peuvent aider à
%(q:harmoniser le statu quo)

#Wti: Liberté de publication

#tWW: Liberté d'utiliser des ordinateurs au bureau et dans des
environnements connectés

#yWP: Clarifier les termes inclusifs que les ADPIC imposent

#uoW: Ne pas codifier les formalismes pour l'examen

#Foo: Établir la liberté d'interopération comme une concrétisation de
l'article 30 des ADPIC

#Wfi: Nous avons d'ores et déjà une bonne loi, mais certains tribunaux des
brevets ne la respectent pas. Ils subissent pourtant une pression
forte et grandissante de la part du public pour revenir à des
pratiques sensées, en conformité avec le droit. La pression grossira
si nous ne passons pas de directive. Elle peut grossir plus rapidement
si nous passons une bonne directive. La seule manière pour
l'Establishment européen des brevets (i.e. les fonctionnaires
nationaux des brevets qui siègent au Groupe de travail sur la
politique des brevets au Conseil de l'UE ou au Conseil
d'administration de l'Office européen des brevets) de consolider ses
mauvaise pratiques est de passer une mauvaise directive.

#min: Nous pouvons accepter à peu près n'importe quelle directive, tant
qu'elle réside seulement dans des exclusions claires et simples de la
brevetabilité.

#tii: L'article 52(2) CBE se compose de telles exclusions. Il exprime dans
des termes clairs et simples ce qui n'est %(e:pas) une %(e:invention)
au sens du droit des brevets.

#ued: Nous ne pouvons pas accepter de langage inclusif, tel que %(bc:X
devrait être brevetable) dans lequel X n'est pas à son tour délimité
par des exclusions claires et simples. Le langage dans le style de
celui du conseil, du type %(bc:X ne devrait pas être brevetable, que
si [condition qui est toujours vérifiée]) est encore moins acceptable.

#eWW: Toute directive contenant des propos de la sorte ne peut se réclamer
d'être conçue pour %(q:harmoniser le statu quo) ou pour %(q:empêcher
une dérive vers la brevetabilité des méthodes d'affaires).

#trp: Les choses les plus importantes viennent en premier lieu. Nous allons
essayer ci-dessous de définir les grandes lignes des exclusions de la
brevetabilité et de l'applicabilité des brevets par ordre de priorité,
puisqu'elles s'appuient l'une sur l'autre. Cela aurait peu de sens de
marquer des points sur la seconde si la première n'est pas garantie.

#rmp: Notre premier intérêt est de garder le logiciel libre de tout brevet,
régulé uniquement par le droit d'auteur. I.e. même s'il existe des
brevets sur les fameux %(q:système anti-blocage (ABS)), %(q:machine à
laver), %(q:aspirateur intelligent), etc., ils ne doivent s'appliquer
qu'aux fabricants et usagers de ces équipements, pas aux gens qui
créent ou fournissent des logiciels (= des logiques de contrôle,
comparables à des notices d'utilisation) pour ces équipements.

#rpi: Notre première demande, qui est primordiale, est %(q:pas de
revendications de programmes, pas d'infraction directe ou indirecte en
distribuant des logiciels). Le Parlement européen a satisfait cette
demande avec son article 5 bis. Malheureusement le Conseil, en
introduisant les revendications de programmes dans son article 5(2), a
fait sauter le seul pont vers des négociations constructives que la
Commission européenne avait jamais construit. S'ils insistent dans
cette destruction, l'affaire est jouée.

#eec: Nous ne pouvons pas accepter de restrictions quant à l'utilisation
d'équipements qui ne sont que des ordinateurs à usage générique. Les
revendications de programmes, sous quelque forme qu'elles se
présentent (procédé, équipement, système et méthode, etc.) sont
inacceptables lorsque la contribution à l'état de l'art réside dans du
pur traitement de données (i.e. des instructions pour des opérations
sur un équipement de traitement de données à usage générique).

#hnW: Le parlement a résolu ce problème par une disposition d'exclusion
claire et simple dans l'esprit de l'article 52 CBE :

#rot: Le traitement de données n'appartient pas à un domaine technique au
sens du droit des brevets.

#WWn: dans un article similaire, il a spécifié certaines choses qui ne
constituaient pas de %(q:contributions techniques) : par ex.
%(q:l'amélioration de l'efficacité du calcul).

#soe: Étant donné ceci, et selon l'article 27 des %(tr:ADPIC), %(q:un brevet
pourra être obtenu pour toute invention (...) dans tous les domaines
technologiques, à condition qu'elle soit (...) susceptible
d'application industrielle), les termes %(q:technologie),
%(q:technique), %(q:domaine technique), %(q:invention) et
%(q:industrielle) doivent être définis au niveau législatif. De par la
nature inclusive de la disposition des ADPIC, il n'est plus approprié
maintenant de laisser la jurisprudence définir ces termes. Le
Parlement européen a apporté les définitions nécessaires lors de sa
première lecture.

#cdo: Le Parlement a également clarifié le terme inclusif d'%(q:invention
mise en oeuvre par ordinateur); qui était imposé par l'Establishment
européen des brevets sans aucun besoin légitime. La définition du
Parlement européen doit être retenue mais le terme devrait être éviter
autant que possible. Le titre de la directive devrait être amendé pour
quelque chose de plus direct, comme %(q:sur les limites de la
brevetabilité concernant le traitement de données et ses domaines
d'application).

#tco: Un avocat peut pondre une revendication sur %(q:un ordinateur,
caractérisé par le fait  qu'il classe les fenêtres sur un écran selon
le schéma X). Dans ce cas, la véritable %(q:invention) (=
%(q:contribution)) est le %(q:schéma X) et de tels schémas ne sont pas
des inventions selon l'article 52 CBE.

#nno: Selon la nouvelle %(pb:approche formelle) introduite par l'Office
européen des brevets (OEB) en 2000, l'%(q:invention) réside toujours
dans les %(q:revendications dans leur ensemble). Dans l'exemple
précédent, elle reside dans l'%(q:ordinateur), qui bien entendu est de
nature technique et par conséquent brevetable. Tandis que le terme
%(q:contribution technique) pourrait sembler se référer à ce qu'on a
auparavant appelé l'%(q:invention). Cependant, même le terme
%(q:contribution technique) en est venu à être employé de manière
contre-intuitive que peu de gens hors de l'OEB arrivent à comprendre.
Dans l'exemple précédent, il pourrait y avoir une %(q:contribution
technique dans l'activité inventive) parce que le %(q:problème
technique) consistant à %(q:rendre plus efficace l'utilisation de
l'espace sur l'écran) est résolu durant l'%(q:activité inventive)
entre %(q:l'état de l'art le plus proche) et l'%(q:invention).

#lfi: Décision sur le contrôle des caisses de retraites en 2000

#ouW: Ce raisonnement obscur de l'OEB n'est pas utilisé dans le droit
général des brevets mais seulement dans le cas des %(q:inventions
mises en oeuvre par ordinateur). Il a émergé à la fin des années 90
comme une méthode pour contourner le droit existant afin d'autoriser
les brevets logiciels. En écrivant un tel raisonnement dans une
directive, l'UE autoriserait des pratiques arbitraires que seul l'OEB
lui-même peut contrôler. Plus encore, en mélangeant la question de la
%(q:contribution technique) avec celle de l'%(q:activité inventive),
la nouvelle doctrine de l'OEB rendrait pratiquement impossible pour la
plupart des offices de brevets nationaux, qui ne conduisent pas de
recherche d'antériorité, de rejeter des brevets logiciels ou des
brevets sur des méthodes d'affaire. Enfin, la nouvelle doctrine
introduit un %(q:droit des brevets sui generis), qui ne correspond pas
aux principes généraux du droit des brevets tel qu'utilisé dans
d'autres domaines, et pa  r conséquent, comme l'ont averti le
gouvernement des États-Unis et certains universitaires en droit
européen, pourrait être vu comme une brèche dans l'Accord sur les
ADPIC.

#Wat: Les tentatives du Conseil pour appeler %(q:invention) tout objet
revendiqué, pour assimiler %(q:invention) avec %(q:les revendications
dans leur ensemble) ou de faire une distinction entre %(q:invention)
et %(q:contribution technique), doivent être rejetées. Comme le
soulignent les %(eg:Directives relatives à l'examen de 1978 de l'OEB),
l'examinateur %(q:ne devrait pas tenir compte de la forme ou du type
de revendication et se concentrer sur le fond afin d'identifier la
nouvelle contribution que la prétendue %(c:invention) déclare apporter
à l'état de l'art. Si cette contribution ne constitue pas une
invention, il n'existe pas de matière brevetable.) Il suffirait de
reconfirmer ce principe. L'exacte méthodologie avec laquelle
l'examinateur identifie la contribution/l'invention n'a pas besoin
d'être formalisée. Si elle doit l'être, alors ce ne peut être fait que
selon la manière proposée par le Parlement en première lecture, i.e.
%(q:identifier la contrib  ution technique en retranchant tout les
aspects non-techniques et non-nouveaux de l'objet revendiqué).

#ioo: L'article 30 des ADPIC nécessite une clarification vis à vis de
l'interopérabilité au niveau législatif, pas au niveau de la
jurisprudence.

#WWW: La Commission et le Conseil prétendent, par ex. dans l'article 6,
qu'ils veulent garantir la liberté d'interopération dans l'esprit de
la directive de 1991 sur les droits d'auteurs.

#Wsp: Mais une clarification est nécessaire et le Parlement a donné la bonne
réponse : l'article 6 bis. Les versions de CULT/ITRE/JURI pour cette
proposition, soutenue par la délégation luxembourgeoise au Conseil,
remplissent cet objectif tout autant que la version du vote en
plénière. Dans les deux cas, certains détails de formulation peuvent
être améliorés.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatnitcu ;
# txtlang: fr ;
# multlin: t ;
# End: ;

