<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII interests and the EU Software Patent Directive

#descr: What are the central freedom and exclusivity interests of software
creators and users how do they translate into the language of the
Software Patent Directive? What other interests exist? Where can space
for meaningful negotiations be found?

#uit: Only Exclusions from Patentability can help %(q:harmonise the status
quo)

#Wti: Freedom of Publication

#tWW: Freedom to use computers in office and network environments

#yWP: Clarify the inclusive terms that TRIPs imposes

#uoW: Do not codify examination formalisms

#Foo: Establish Freedom of Interoperation as a Concretisation of Art 30
TRIPs

#Wfi: We have a good law already, but some patent courts are not respecting
it.  However they are under strong and growing public pressure to
return to a sound practise, in conformance to the law.  The pressure
will grow if we do not pass a directive.  It can grow more quickly if
we pass a good directive.  The only way for the European patent
establishment (i.e. the national patent administrators who sit on the
EU Council's patent policy working party and on the Administrative
Council of the European Patent Office) to stabilise its bad practise
is by passing a bad directive.

#min: We can accept almost any directive, as long as it consists only of
clear and simple exclusions from patentability.

#tii: Art 52(2) EPC consists of such exclusions.   It says in clear and
simple terms what is %(e:not) an %(e:invention) in the sense of patent
law.

#ued: We can not accept inclusive language, such as %(bc:X shall be
patentable) where X is not in turn delimited in clear and simple
exclusions.  Council-style language of the sort %(bc:X shall not be
patentable, unless [condition which is always true]) is even less
acceptable.

#eWW: Any directive that contains statements of the above type can not claim
to be intended to %(q:harmonise the status quo) or to %(q:prevent a
drift toward patentability of business methods).

#trp: First things come first.  Below we try to outline the exclusions on
patentability and patent enforcability in their order of priority, as
they build on one another.  There is little sense in scoring points on
the second if the first is not assured.

#rmp: Our constituents' basic interest is to keep the software free from
patents, regulated by copyright only.  I.e. even if there are patents
on the much cited %(q:anti-lock braking system), %(q:washing machine),
%(q:intelligent vacuum cleaner) etc, they must apply only to the
makers and users of the devices, not to people who create or provide
software (= control logic, similar to user manuals) for these devices.

#rpi: Our first and most basic demand is %(q:no program claims, no direct or
indirect infringment by software distribution).  The European
Parliament has fulfilled this demand with Art 5A.  Unfortunately the
Council, by introducing program claims in their Art 5(2), has torn
down the only bridge to constructive negotiations that the European
Commission ever built.  If they insist on keeping it torn down, that
was it.

#eec: We can not accept restrictions on the use of equipment that consists
of general-purpose computers only.  Patent claims of whatever form
(process, device, system and method, ...) are unacceptable when the
contribution to the prior art consists of pure data processing (i.e.
instructions for operation of general purpose data processing
equipment).

#hnW: The Parliament has solved this problem by a clear and simple exclusion
statement in the spirit of Art 52 EPC:

#rot: Data processing is not a field of technology in the sense of patent
law.

#WWn: In a similar article, it has specified some items that do not
constitute a %(q:technical contributions): e.g. %(q:improvement of
calculation efficiency).

#soe: Given that, according to Art 27 %(tr:TRIPs), %(q:patents shall be
available for inventions in all fields of technology, provided that
they are .. susceptible industrial application), the terms
%(q:technology), %(q:technical), %(q:fields of technology),
%(q:invention) and %(q:industrial) need to be defined at the
legislative level.  Because of the inclusive nature of the TRIPs
statement, it is now no longer adequate to let caselaw define these
terms.   The European Parliament has provided the needed definitions
in its first reading.

#cdo: The Parliament has also clarified the inclusive term
%(q:computer-implemented invention), which was imposed by the European
Patent Establishment without any legitimate need.   The EP definition
must be retained, but the term should be discarded as far as possible.
 The directive title should be amended to something more
straightforward, such as %(q:on the limits of patentability with
regard to data processing and its fields of application).

#tco: An attorney may lay claim to %(q:a computer, characterised by that it
orders windows on a screen according to scheme X).   In this case, the
real %(q:invention) (= %(q:contribution)) is the %(q:scheme X), and
such schemes are not inventions according to Art 52 EPC.

#nno: Under the new %(pb:formalistic approach) introduced by the European
Patent Office (EPO) in 2000, the %(q:invention) always consists in the
%(q:claim as a whole), e.g. in the above example the %(q:computer),
which is of course technical and therefore patentable, whereas the
term %(q:technical contribution) might seem to refer to what was
previously called the %(q:invention).  However, even the term
%(q:technical contribution) has come to be used in counter-intuitive
ways which few outside the EPO understand.  E.g. in the above example,
there might be a %(q:technical contribution in the inventive step),
because the %(q:technical problem) of %(q:improving efficiency in the
use of space on the screen) is solved during the %(q:inventive step)
from the %(q:closest prior art) to the %(q:invention).

#lfi: Controlling Pension Benefits System decision of 2000

#ouW: This obscure reasoning of the EPO is not used in general patent law,
but only in the case of %(q:computer-implemented inventions).  It
emerged in the late 1990s as a scheme for twisting the existing law so
as to allow software patents. By writing such reasoning into a
directive, the EU would authorise arbitrary practises which only the
EPO itself can control.  Moreover, by mingling the question of
%(q:technical contribution) with that of %(q:inventive step), the
EPO's new doctrine makes it nearly impossible for most national patent
offices, which do not conduct prior art searches, to reject software
or business method patents.  Finally, the new doctrine introduces a
%(e:sui generis software patent law), which does not correspond to the
general principles of patent law as used in other fields, and
therefore, as the US government and some European law scholars have
warned, could be seen as a breach of the TRIPs treaty.

#Wat: The Council's attempts to call any claim object an %(q:invention), to
equate %(q:invention) with %(q:the claim as a whole), or to
distinguish between %(q:invention) and %(q:technical contribution),
must be rejected.  As stressed by the %(eg:EPO's Examination
Guidelines of 1978), the examiner %(q:should disregard the form or
kind of claim and concentrate on the content in order to identify the
novel contribution which the alleged %(c:invention) claimed makes to
the known art. If this contribution does not constitute an invention,
there is not patentable subject matter.)  It would be enough to
reconfirm this principle.  The exact methodology of how the examiner
identifies the contribution/invention needn't be formalised. If it is
to be formalised, then this can be done only in the way proposed by
the Parliament in the first reading, i.e. %(q:identify the technical
contribution by subtracting all non-technical and non-novel features
from the claimed object).

#ioo: Art 30 TRIPs requires clarification with regard to interoperability at
the legislative level, not at the caselaw level.

#WWW: Commission and Council pretend, e.g. by Art 6, that they want to
guarantee freedom of interoperation in the spirit of the 1991 software
copyright directive.

#Wsp: Indeed there is a need of clarification, and the Parliament has given
the right answer: Art 6a.  The CULT/ITRE/JURI version of this
proposal, which was endorsed by the Luxemburg delegation in the
Council, serves the purpose as well as the plenary version.  In both
cases, some wording details can be improved.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatnitcu ;
# txtlang: en ;
# multlin: t ;
# End: ;

