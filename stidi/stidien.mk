stidien.lstex:
	lstex stidien | sort -u > stidien.lstex
stidien.mk:	stidien.lstex
	vcat /ul/prg/RC/texmake > stidien.mk


stidien.dvi:	stidien.mk stidien.tex
	latex stidien && while tail -n 20 stidien.log | grep references && latex stidien;do eval;done
	if test -r stidien.idx;then makeindex stidien && latex stidien;fi
stidien.pdf:	stidien.mk stidien.tex
	pdflatex stidien && while tail -n 20 stidien.log | grep references && pdflatex stidien;do eval;done
	if test -r stidien.idx;then makeindex stidien && pdflatex stidien;fi
stidien.tty:	stidien.dvi
	dvi2tty -q stidien > stidien.tty
stidien.ps:	stidien.dvi	
	dvips  stidien
stidien.001:	stidien.dvi
	rm -f stidien.[0-9][0-9][0-9]
	dvips -i -S 1  stidien
stidien.pbm:	stidien.ps
	pstopbm stidien.ps
stidien.gif:	stidien.ps
	pstogif stidien.ps
stidien.eps:	stidien.dvi
	dvips -E -f stidien > stidien.eps
stidien-01.g3n:	stidien.ps
	ps2fax n stidien.ps
stidien-01.g3f:	stidien.ps
	ps2fax f stidien.ps
stidien.ps.gz:	stidien.ps
	gzip < stidien.ps > stidien.ps.gz
stidien.ps.gz.uue:	stidien.ps.gz
	uuencode stidien.ps.gz stidien.ps.gz > stidien.ps.gz.uue
stidien.faxsnd:	stidien-01.g3n
	set -a;FAXRES=n;FILELIST=`echo stidien-??.g3n`;source faxsnd main
stidien_tex.ps:	
	cat stidien.tex | splitlong | coco | m2ps > stidien_tex.ps
stidien_tex.ps.gz:	stidien_tex.ps
	gzip < stidien_tex.ps > stidien_tex.ps.gz
stidien-01.pgm:
	cat stidien.ps | gs -q -sDEVICE=pgm -sOutputFile=stidien-%02d.pgm -
stidien/stidien.html:	stidien.dvi
	rm -fR stidien;latex2html stidien.tex
xview:	stidien.dvi
	xdvi -s 8 stidien &
tview:	stidien.tty
	browse stidien.tty 
gview:	stidien.ps
	ghostview  stidien.ps &
print:	stidien.ps
	lpr -s -h stidien.ps 
sprint:	stidien.001
	for F in stidien.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	stidien.faxsnd
	faxsndjob view stidien &
fsend:	stidien.faxsnd
	faxsndjob jobs stidien
viewgif:	stidien.gif
	xv stidien.gif &
viewpbm:	stidien.pbm
	xv stidien-??.pbm &
vieweps:	stidien.eps
	ghostview stidien.eps &	
clean:	stidien.ps
	rm -f  stidien-*.tex stidien.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} stidien-??.* stidien_tex.* stidien*~
tgz:	clean
	set +f;LSFILES=`cat stidien.ls???`;FILES=`ls stidien.* $$LSFILES | sort -u`;tar czvf stidien.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
