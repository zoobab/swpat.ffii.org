\contentsline {section}{\numberline {1}The Basic Questions}{2}{section.1}
\contentsline {section}{\numberline {2}Patent Jurisprudence on a Slippery Slope -- the price for dismantling the concept of technical invention}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Introductory Quotes}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Freedom of everything intellectual or patentability of everything machine-like?}{9}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}From Technicity to Arbitrariness}{11}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Voided Terminology and its Technical Contribution to Legislative Engineering in the EU}{13}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Literature Overview}{14}{subsection.2.5}
\contentsline {section}{\numberline {3}Other serious attempts at formulating questions}{40}{section.3}
