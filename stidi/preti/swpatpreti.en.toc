\contentsline {section}{\numberline {1}Requirements for a Study}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Gr\"{o}{\ss }tm\"{o}gliche \"{O}ffentlichkeit}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Theoriebildung und Handlungsoptionen vorweg}{2}{subsection.1.2}
\contentsline {subsubsection}{\numberline {1.2.1}Unbegrenzte Patentierbarkeit mit unbegrenzter Durchsetzbarkeit}{2}{subsubsection.1.2.1}
\contentsline {subsubsection}{\numberline {1.2.2}Unbegrenzte Patentierbarkeit mit begrenzter Durchsetzbarkeit}{3}{subsubsection.1.2.2}
\contentsline {subsubsection}{\numberline {1.2.3}Zur\"{u}ckschneidung des Patentwesens durch restriktiven Technikbegriff}{3}{subsubsection.1.2.3}
\contentsline {subsection}{\numberline {1.3}Erhebung aussagekr\"{a}ftiger Daten}{3}{subsection.1.3}
\contentsline {section}{\numberline {2}Alter Entwurf: Wie w\"{u}rde die (Nicht)Patentierbarkeit von Software auf Ihre Firma wirken?}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}How would software patentability affect your company?}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}What should be decided in November 2000?}{5}{subsection.2.2}
\contentsline {section}{\numberline {3}Noch \"{a}lterer Entwurf}{5}{section.3}
