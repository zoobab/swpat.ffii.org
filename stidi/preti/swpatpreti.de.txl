<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Konzepte für Empirische Erhebungen

#descr: Einige Anforderungen an eine Studie der ökonomischen Auswirkungen von Softwarepatenten.  Es werden auch Fragebögen vorgeschlagen, durch die wir ermitteln könnten, was Firmen in Europa mit unterschiedlichen Geschäftsmodellen von Softwarepatenten zu gewinnen oder verlieren haben.

#Ani: Anforderungen an eine Studie

#Afo: Alter Entwurf: Wie würde die (Nicht)Patentierbarkeit von Software auf Ihre Firma wirken?

#NlW: Noch älterer Entwurf

#DnW: Das Thema Swpat stellt die Politik vor Entscheidungen.  Es ist wichtig, vorher die wirtschaftlichen Folgen abzuschätzen.  Nicht alles kann auf philosophischer Ebene beantwortet werden.  Empirische Forschung ist notwendig.  Wenn der FFII e.V. ausschreiben würde, sollte die Studie etwa folgende Anforderungen erfüllen.

#Gin: Größtmögliche Öffentlichkeit

#TWo: Theoriebildung und Handlungsoptionen vorweg

#Eut: Erhebung aussagekräftiger Daten

#Sev: Sie sollte von Anfang an im WWW unter Einbeziehung einer größtmöglichen Öffentlichkeit stattfinden.  Insbesondere Wissenschaftler und IT-Fachleute (Entwickler, Unternehmer) sollten angeregt werden, sich einzubringen.  Dies kann dadurch geschehen, dass im Namen des BMWi oder einer Universität ein dynamischer Webserver etwa mit Zope aufgesetzt wird.  Es gibt bereits Zope-Module für die Verwaltung und Auswertung von Umfragen.

#Elr: Ein Ergebnis der Übung sollte sein, dass sich ein weiter Kreis von Interessenten formiert, der in Zukunft als Netzwerk dem Gesetzgeber zur Seite steht und ihm hilft, die Entscheidung über die Kriterien der Patentierbarkeit und die angemessenen Paradigmen der Innovationsförderung wieder zurück in seine Hände zu bekommen.

#Dil: Die Studie muss nicht unbedingt sehr kluge Theorien liefern.  Es kommt eher darauf an, dass sie wertvolle empirische Daten liefert.  Dazu ist die Theoriebildung voraussetzung.  Diese sollte also bereits spätestens in der Bewerbung, noch besser in der Ausschreibung, erfolgt sein.

#Eeg: Es sollten von Anfang an die möglichen Handlungsoptionen herausgearbeitet werden, damit man dann deren Auswirkungen auch erforschen kann.  Der Fehler der IPI-Studie der EU-Kommission muss vermieden werden.  Dort wurden ständig juristische Fragen in die ökonomischen hineingemischt und es wurde gar nicht gefragt, was passieren würde, wenn man das Patentwesen auf seinen Kernbereich zurückschneiden und/oder DV-Programme draußen halten würde.

#Dun: Die Studie sollte nicht Handlungsoptionen empfehlen sondern solche als gegeben voraussetzen und lediglich die möglichen Auswirkungen erforschen.  Es ist nicht Sache von Wissenschaftlern, Empfehlungen über Handlungsoptionen abzugeben. Das müssen Politiker letztendlich abwägen.

#IWO: Im wesentlichen sind folgende Optionen gegeben:

#Urn: Unbegrenzte Patentierbarkeit mit unbegrenzter Durchsetzbarkeit

#Urt: Unbegrenzte Patentierbarkeit mit begrenzter Durchsetzbarkeit

#Zar: Zurückschneidung des Patentwesens durch restriktiven Technikbegriff

#DWs: Das EPA und seine Freunde entscheiden lassen, d.h. Konvergenz mit den USA.  (Die von der IPI-Studie untersuchte Unterscheidung zwischen EPA-Praxis und State-Street-Praxis ist weitgehend belanglos)

#wWc: wie oben, aber Softwarepatente dadurch in der Praxis entwerten, dass man der Veröffentlichung und Vermarkten von Urheberrechtsgegenständen aller Art, einschließlich Computerprogrammen, Vorrang vor der Durchsetzung von Ansprüchen aus Patentschriften einräumt.  D.h. ein Prozess mag zwar beansprucht werden, aber gegen ein entsprechendes Computerprogramm kann dieser Anspruch auf dem Markt nicht durchgesetzt werden.  Eine Variante hiervon ist, dass man nur freie/quelloffene Software freistellt.

#Iua: Innovationen, die auf dem Rechnen mit bekannten Modellen beruhen, sind keine Erfindungen.  Es wäre dann zu untersuchen, inwieweit man noch patentähnliche Schutzrechte für die Nachrichtentechnik, Bioinformatik, mechanische Trivialinnovationen o.ä. haben möchte, die bei einem restriktiven Technikbegriff außen vor bleiben.

#Bag: Bei all diesen Optionen ist ferner klarzustellen, dass wir von einer Situation ausgehen, in der sich allmählich die ganze Welt für das jeweils präferierte Modell entscheidet.  Alternativ dazu ist zu fragen, was passiert, wenn nur wir uns dafür entscheiden und die USA bei ihrem derzeitigen Modell bleiben.

#Uih: Unternehmen wie Siemens müssen einerseits ihre deutschen Mitarbeiter zur extensiven Patentierung motivieren, um in den USA mithalten zu können.  Dazu brauchen sie in Europa die Möglichkeit, alles zu patentieren, was in den USA patentiert werden kann.  Andererseits nehmen gerade produktentwickelnde Unternehmen wie Siemens auf Dauer erheblichen Schaden, wenn das Patentsystem ausufert und man von professionellen Patententwicklern wie Qualcomm wegen jeder Kleinigkeit erpresst werden kann.  Zwischen Patentinflationslogik und eigentlicher Folgenabschätzung ist zu trennen.

#Etd: Es sollte vor allem qualitativ gearbeitet werden.  Bevor einem Unternehmen ein Fragebogen präsentiert wird, ist dieses Unternehmen mit den in seinem Bereich bereits erteilten europäischen Patenten zu konfrontieren und spezifisch zur Einschätzung dieser Patente zu befragen.  Es muss davon ausgegangen werden, dass viele Unternehmen und Verbände sich zu dem Thema noch überhaupt keine Gedanken gemacht haben.  Daher muss auch eine Antwort wie %(q:konnte mir noch keine Meinung bilden) zugelassen werden.

#Del: Die Fragen sind so zu formulieren, dass sie von denjenigen Leuten beantwortet werden, die für die Forschung & Entwicklung und das damit verbundene Finanzkalkül zuständig sind.   Z.B. sind die relevanten Patente zu präsentieren, und es ist zu fragen, mit wie hohe Prozessrisiken das Unternehmen rechnet und welche Rückstellungen dafür gebildet worden sind oder werden sollten.  Für die Beantwortung ist Zeit zu gewähren und es ist auf mögliche Informationsquellen zu verweisen.  Neben einem fixen Grundrahmen ist Platz für schriftlich formulierte Stellungnahmen zu lassen.  Die Antworten sollten möglichst weitgehend veröffentlicht werden und auch nachträglich öffentlich korrigierbar sein.  Ein Fragebogen im WWW ist vorzuziehen.  Anonyme Angaben sind zu ermöglichen, sollten aber eher als Hinweise an die Forscher und Anlass für weitere Nachforschungen denn als belastbare Daten angesehen werden.

#Hef: How would software patentability affect your company?

#WeN: What should be decided in November 2000?

#Hlr: How many programmers are currently working in your company?

#Hof: Have you ever been approached or attacked by a patent owner for infringement of his patents?

#HsW: The EPO has granted %(pi:20-30,000 software patents since 1995).

#HWt: How many of these could be relevant for your field?

#Hod: How many of these do you own?

#hyc: How much additional money do you expect to spend on

#pte: patent research

#pWc: patent applications

#pux: patent license fees (subtract the fees you expect to earn yourself)

#pft: patent infringement lawsuits

#sfp: salaries of patent specialists

#oto: once software patents become enforcable in Europe?

#HpW: How much could your company gain by being able to fight off imitators?

#Hlo: How much could your company lose by having to program around other people's patents?

#Dcd: Do you ever disclose source code of your own?

#naW: No, we do not write software at all.

#Nub: No, in general it is our principle to distribute binary files only.

#yms: Yes, customers can obtain the source code if they sign a non-disclosure agreement (NDA).

#yie: Yes, we sooner or later release much of our source code to the public.

#H0u: How would the enforcability of nx10000 European software patents affect your willingness to disclose source code?

#Cre: Computer programs should be patentable, and such patents should be fully enforcable in Europe.

#TcE: The European law should be changed according to the Basic Proposal of the European Patent Office without further delay.

#Wbd: Whether computer programs should be patentable depends mainly on the effects this would have on the European economy and society.

#Ehu: Given that European governements have not yet published an official study on these effects, Europe should allow itself a 2 year moratorium for further discussion.

#CaW: Computer programs should not be patentable.

#SiW: Software copyright is quite sufficient to protect the legitimate interests of software companies.

#Cle: Copyright alone may not be sufficient.  A %(ll:third paradigm between patent and copyright) should be worked out as an additional incentive for software innovation.

#SpW: Software Patent Questionnaire with special regard to the European Patent Convention and Open Source Software

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatpreti ;
# txtlang: de ;
# multlin: t ;
# End: ;

