\begin{subdocument}{swpatpreti}{Concepts for Empirical Surveys}{http://swpat.ffii.org/analysis/enquete/index.en.html}{Workgroup\\swpatag@ffii.org}{Some requirements for a study on economic effects of software patentability.  Includes questionnaires which may used to find out what the European software industry and consumers may stand to gain or lose from software patents.}
\begin{sect}{req}{Requirements for a Study}
Das Thema Swpat stellt die Politik vor Entscheidungen.  Es ist wichtig, vorher die wirtschaftlichen Folgen abzusch\"{a}tzen.  Nicht alles kann auf philosophischer Ebene beantwortet werden.  Empirische Forschung ist notwendig.  Wenn der FFII e.V. ausschreiben w\"{u}rde, sollte die Studie etwa folgende Anforderungen erf\"{u}llen.

\begin{sect}{pub}{Gr\"{o}{\ss}tm\"{o}gliche \"{O}ffentlichkeit}
Sie sollte von Anfang an im WWW unter Einbeziehung einer gr\"{o}{\ss}tm\"{o}glichen \"{O}ffentlichkeit stattfinden.  Insbesondere Wissenschaftler und IT-Fachleute (Entwickler, Unternehmer) sollten angeregt werden, sich einzubringen.  Dies kann dadurch geschehen, dass im Namen des BMWi oder einer Universit\"{a}t ein dynamischer Webserver etwa mit Zope aufgesetzt wird.  Es gibt bereits Zope-Module f\"{u}r die Verwaltung und Auswertung von Umfragen.

Ein Ergebnis der \"{U}bung sollte sein, dass sich ein weiter Kreis von Interessenten formiert, der in Zukunft als Netzwerk dem Gesetzgeber zur Seite steht und ihm hilft, die Entscheidung \"{u}ber die Kriterien der Patentierbarkeit und die angemessenen Paradigmen der Innovationsf\"{o}rderung wieder zur\"{u}ck in seine H\"{a}nde zu bekommen.
\end{sect}

\begin{sect}{opt}{Theoriebildung und Handlungsoptionen vorweg}
Die Studie muss nicht unbedingt sehr kluge Theorien liefern.  Es kommt eher darauf an, dass sie wertvolle empirische Daten liefert.  Dazu ist die Theoriebildung voraussetzung.  Diese sollte also bereits sp\"{a}testens in der Bewerbung, noch besser in der Ausschreibung, erfolgt sein.

Es sollten von Anfang an die m\"{o}glichen Handlungsoptionen herausgearbeitet werden, damit man dann deren Auswirkungen auch erforschen kann.  Der Fehler der IPI-Studie der EU-Kommission muss vermieden werden.  Dort wurden st\"{a}ndig juristische Fragen in die \"{o}konomischen hineingemischt und es wurde gar nicht gefragt, was passieren w\"{u}rde, wenn man das Patentwesen auf seinen Kernbereich zur\"{u}ckschneiden und/oder DV-Programme drau{\ss}en halten w\"{u}rde.

Die Studie sollte nicht Handlungsoptionen empfehlen sondern solche als gegeben voraussetzen und lediglich die m\"{o}glichen Auswirkungen erforschen.  Es ist nicht Sache von Wissenschaftlern, Empfehlungen \"{u}ber Handlungsoptionen abzugeben. Das m\"{u}ssen Politiker letztendlich abw\"{a}gen.

Im wesentlichen sind folgende Optionen gegeben:

\begin{sect}{senlim}{Unbegrenzte Patentierbarkeit mit unbegrenzter Durchsetzbarkeit}
Das EPA und seine Freunde entscheiden lassen, d.h. Konvergenz mit den USA.  (Die von der IPI-Studie untersuchte Unterscheidung zwischen EPA-Praxis und State-Street-Praxis ist weitgehend belanglos)
\end{sect}

\begin{sect}{nonenf}{Unbegrenzte Patentierbarkeit mit begrenzter Durchsetzbarkeit}
wie oben, aber Softwarepatente dadurch in der Praxis entwerten, dass man der Ver\"{o}ffentlichung und Vermarkten von Urheberrechtsgegenst\"{a}nden aller Art, einschlie{\ss}lich Computerprogrammen, Vorrang vor der Durchsetzung von Anspr\"{u}chen aus Patentschriften einr\"{a}umt.  D.h. ein Prozess mag zwar beansprucht werden, aber gegen ein entsprechendes Computerprogramm kann dieser Anspruch auf dem Markt nicht durchgesetzt werden.  Eine Variante hiervon ist, dass man nur freie/quelloffene Software freistellt.
\end{sect}

\begin{sect}{nurtech}{Zur\"{u}ckschneidung des Patentwesens durch restriktiven Technikbegriff}
Innovationen, die auf dem Rechnen mit bekannten Modellen beruhen, sind keine Erfindungen.  Es w\"{a}re dann zu untersuchen, inwieweit man noch patent\"{a}hnliche Schutzrechte f\"{u}r die Nachrichtentechnik, Bioinformatik, mechanische Trivialinnovationen o.\"{a}. haben m\"{o}chte, die bei einem restriktiven Technikbegriff au{\ss}en vor bleiben.
\end{sect}

Bei all diesen Optionen ist ferner klarzustellen, dass wir von einer Situation ausgehen, in der sich allm\"{a}hlich die ganze Welt f\"{u}r das jeweils pr\"{a}ferierte Modell entscheidet.  Alternativ dazu ist zu fragen, was passiert, wenn nur wir uns daf\"{u}r entscheiden und die USA bei ihrem derzeitigen Modell bleiben.

Unternehmen wie Siemens m\"{u}ssen einerseits ihre deutschen Mitarbeiter zur extensiven Patentierung motivieren, um in den USA mithalten zu k\"{o}nnen.  Dazu brauchen sie in Europa die M\"{o}glichkeit, alles zu patentieren, was in den USA patentiert werden kann.  Andererseits nehmen gerade produktentwickelnde Unternehmen wie Siemens auf Dauer erheblichen Schaden, wenn das Patentsystem ausufert und man von professionellen Patententwicklern wie Qualcomm wegen jeder Kleinigkeit erpresst werden kann.  Zwischen Patentinflationslogik und eigentlicher Folgenabsch\"{a}tzung ist zu trennen.
\end{sect}

\begin{sect}{kval}{Erhebung aussagekr\"{a}ftiger Daten}
Es sollte vor allem qualitativ gearbeitet werden.  Bevor einem Unternehmen ein Fragebogen pr\"{a}sentiert wird, ist dieses Unternehmen mit den in seinem Bereich bereits erteilten europ\"{a}ischen Patenten zu konfrontieren und spezifisch zur Einsch\"{a}tzung dieser Patente zu befragen.  Es muss davon ausgegangen werden, dass viele Unternehmen und Verb\"{a}nde sich zu dem Thema noch \"{u}berhaupt keine Gedanken gemacht haben.  Daher muss auch eine Antwort wie ``konnte mir noch keine Meinung bilden'' zugelassen werden.

Die Fragen sind so zu formulieren, dass sie von denjenigen Leuten beantwortet werden, die f\"{u}r die Forschung \& Entwicklung und das damit verbundene Finanzkalk\"{u}l zust\"{a}ndig sind.   Z.B. sind die relevanten Patente zu pr\"{a}sentieren, und es ist zu fragen, mit wie hohe Prozessrisiken das Unternehmen rechnet und welche R\"{u}ckstellungen daf\"{u}r gebildet worden sind oder werden sollten.  F\"{u}r die Beantwortung ist Zeit zu gew\"{a}hren und es ist auf m\"{o}gliche Informationsquellen zu verweisen.  Neben einem fixen Grundrahmen ist Platz f\"{u}r schriftlich formulierte Stellungnahmen zu lassen.  Die Antworten sollten m\"{o}glichst weitgehend ver\"{o}ffentlicht werden und auch nachtr\"{a}glich \"{o}ffentlich korrigierbar sein.  Ein Fragebogen im WWW ist vorzuziehen.  Anonyme Angaben sind zu erm\"{o}glichen, sollten aber eher als Hinweise an die Forscher und Anlass f\"{u}r weitere Nachforschungen denn als belastbare Daten angesehen werden.
\end{sect}
\end{sect}

\begin{sect}{kiel}{Alter Entwurf: Wie w\"{u}rde die (Nicht)Patentierbarkeit von Software auf Ihre Firma wirken?}
\begin{sect}{viakom}{How would software patentability affect your company?}
\begin{enumerate}
\item
How many programmers are currently working in your company?

\item
How many patent specialists are currently working in your company?

\item
Have you ever been approached or attacked by a patent owner for infringement of his patents?

\item
The EPO has granted 20-30,000 software patents since 1995.
\begin{enumerate}
\item
How many of these could be relevant for your field?

\item
How many of these do you own?
\end{enumerate}

\item
How much additional money do you expect to spend on
\begin{enumerate}
\item
patent research

\item
patent applications

\item
patent license fees (subtract the fees you expect to earn yourself)

\item
patent infringement lawsuits

\item
salaries of patent specialists
\end{enumerate}
once software patents become enforcable in Europe?

\item
How much could your company gain by being able to fight off imitators?

\item
How much could your company lose by having to program around other people's patents?

\item
Do you ever disclose source code of your own?
\begin{enumerate}
\item
No, we do not write software at all.

\item
No, in general it is our principle to distribute binary files only.

\item
Yes, customers can obtain the source code if they sign a non-disclosure agreement (NDA).

\item
Yes, we sooner or later release much of our source code to the public.
\end{enumerate}

\item
How would the enforcability of nx10000 European software patents affect your willingness to disclose source code?
\end{enumerate}
\end{sect}

\begin{sect}{dipkonf}{What should be decided in November 2000?}
\begin{enumerate}
\item
Computer programs should be patentable, and such patents should be fully enforcable in Europe.
The European law should be changed according to the Basic Proposal of the European Patent Office without further delay.

\item
Whether computer programs should be patentable depends mainly on the effects this would have on the European economy and society.
Given that European governements have not yet published an official study on these effects, Europe should allow itself a 2 year moratorium for further discussion.

\item
Computer programs should not be patentable.
\begin{enumerate}
\item
Software copyright is quite sufficient to protect the legitimate interests of software companies.

\item
Copyright alone may not be sufficient.  A third paradigm between patent and copyright should be worked out as an additional incentive for software innovation.
\end{enumerate}
\end{enumerate}
\end{sect}
\end{sect}

\begin{sect}{blas}{Noch \"{a}lterer Entwurf}
Holger Blasum\footnote{http://www.blasum.net}: Software Patent Questionnaire with special regard to the European Patent Convention and Open Source Software\footnote{epcossen.txt}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

