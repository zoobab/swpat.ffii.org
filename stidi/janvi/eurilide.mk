eurilide.lstex:
	lstex eurilide | sort -u > eurilide.lstex
eurilide.mk:	eurilide.lstex
	vcat /ul/prg/RC/texmake > eurilide.mk


eurilide.dvi:	eurilide.mk eurilide.tex
	latex eurilide && while tail -n 20 eurilide.log | grep references && latex eurilide;do eval;done
	if test -r eurilide.idx;then makeindex eurilide && latex eurilide;fi
eurilide.pdf:	eurilide.mk eurilide.tex
	pdflatex eurilide && while tail -n 20 eurilide.log | grep references && pdflatex eurilide;do eval;done
	if test -r eurilide.idx;then makeindex eurilide && pdflatex eurilide;fi
eurilide.tty:	eurilide.dvi
	dvi2tty -q eurilide > eurilide.tty
eurilide.ps:	eurilide.dvi	
	dvips  eurilide
eurilide.001:	eurilide.dvi
	rm -f eurilide.[0-9][0-9][0-9]
	dvips -i -S 1  eurilide
eurilide.pbm:	eurilide.ps
	pstopbm eurilide.ps
eurilide.gif:	eurilide.ps
	pstogif eurilide.ps
eurilide.eps:	eurilide.dvi
	dvips -E -f eurilide > eurilide.eps
eurilide-01.g3n:	eurilide.ps
	ps2fax n eurilide.ps
eurilide-01.g3f:	eurilide.ps
	ps2fax f eurilide.ps
eurilide.ps.gz:	eurilide.ps
	gzip < eurilide.ps > eurilide.ps.gz
eurilide.ps.gz.uue:	eurilide.ps.gz
	uuencode eurilide.ps.gz eurilide.ps.gz > eurilide.ps.gz.uue
eurilide.faxsnd:	eurilide-01.g3n
	set -a;FAXRES=n;FILELIST=`echo eurilide-??.g3n`;source faxsnd main
eurilide_tex.ps:	
	cat eurilide.tex | splitlong | coco | m2ps > eurilide_tex.ps
eurilide_tex.ps.gz:	eurilide_tex.ps
	gzip < eurilide_tex.ps > eurilide_tex.ps.gz
eurilide-01.pgm:
	cat eurilide.ps | gs -q -sDEVICE=pgm -sOutputFile=eurilide-%02d.pgm -
eurilide/eurilide.html:	eurilide.dvi
	rm -fR eurilide;latex2html eurilide.tex
xview:	eurilide.dvi
	xdvi -s 8 eurilide &
tview:	eurilide.tty
	browse eurilide.tty 
gview:	eurilide.ps
	ghostview  eurilide.ps &
print:	eurilide.ps
	lpr -s -h eurilide.ps 
sprint:	eurilide.001
	for F in eurilide.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	eurilide.faxsnd
	faxsndjob view eurilide &
fsend:	eurilide.faxsnd
	faxsndjob jobs eurilide
viewgif:	eurilide.gif
	xv eurilide.gif &
viewpbm:	eurilide.pbm
	xv eurilide-??.pbm &
vieweps:	eurilide.eps
	ghostview eurilide.eps &	
clean:	eurilide.ps
	rm -f  eurilide-*.tex eurilide.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} eurilide-??.* eurilide_tex.* eurilide*~
tgz:	clean
	set +f;LSFILES=`cat eurilide.ls???`;FILES=`ls eurilide.* $$LSFILES | sort -u`;tar czvf eurilide.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
