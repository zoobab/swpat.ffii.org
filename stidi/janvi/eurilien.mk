eurilien.lstex:
	lstex eurilien | sort -u > eurilien.lstex
eurilien.mk:	eurilien.lstex
	vcat /ul/prg/RC/texmake > eurilien.mk


eurilien.dvi:	eurilien.mk eurilien.tex
	latex eurilien && while tail -n 20 eurilien.log | grep references && latex eurilien;do eval;done
	if test -r eurilien.idx;then makeindex eurilien && latex eurilien;fi
eurilien.pdf:	eurilien.mk eurilien.tex
	pdflatex eurilien && while tail -n 20 eurilien.log | grep references && pdflatex eurilien;do eval;done
	if test -r eurilien.idx;then makeindex eurilien && pdflatex eurilien;fi
eurilien.tty:	eurilien.dvi
	dvi2tty -q eurilien > eurilien.tty
eurilien.ps:	eurilien.dvi	
	dvips  eurilien
eurilien.001:	eurilien.dvi
	rm -f eurilien.[0-9][0-9][0-9]
	dvips -i -S 1  eurilien
eurilien.pbm:	eurilien.ps
	pstopbm eurilien.ps
eurilien.gif:	eurilien.ps
	pstogif eurilien.ps
eurilien.eps:	eurilien.dvi
	dvips -E -f eurilien > eurilien.eps
eurilien-01.g3n:	eurilien.ps
	ps2fax n eurilien.ps
eurilien-01.g3f:	eurilien.ps
	ps2fax f eurilien.ps
eurilien.ps.gz:	eurilien.ps
	gzip < eurilien.ps > eurilien.ps.gz
eurilien.ps.gz.uue:	eurilien.ps.gz
	uuencode eurilien.ps.gz eurilien.ps.gz > eurilien.ps.gz.uue
eurilien.faxsnd:	eurilien-01.g3n
	set -a;FAXRES=n;FILELIST=`echo eurilien-??.g3n`;source faxsnd main
eurilien_tex.ps:	
	cat eurilien.tex | splitlong | coco | m2ps > eurilien_tex.ps
eurilien_tex.ps.gz:	eurilien_tex.ps
	gzip < eurilien_tex.ps > eurilien_tex.ps.gz
eurilien-01.pgm:
	cat eurilien.ps | gs -q -sDEVICE=pgm -sOutputFile=eurilien-%02d.pgm -
eurilien/eurilien.html:	eurilien.dvi
	rm -fR eurilien;latex2html eurilien.tex
xview:	eurilien.dvi
	xdvi -s 8 eurilien &
tview:	eurilien.tty
	browse eurilien.tty 
gview:	eurilien.ps
	ghostview  eurilien.ps &
print:	eurilien.ps
	lpr -s -h eurilien.ps 
sprint:	eurilien.001
	for F in eurilien.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	eurilien.faxsnd
	faxsndjob view eurilien &
fsend:	eurilien.faxsnd
	faxsndjob jobs eurilien
viewgif:	eurilien.gif
	xv eurilien.gif &
viewpbm:	eurilien.pbm
	xv eurilien-??.pbm &
vieweps:	eurilien.eps
	ghostview eurilien.eps &	
clean:	eurilien.ps
	rm -f  eurilien-*.tex eurilien.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} eurilien-??.* eurilien_tex.* eurilien*~
tgz:	clean
	set +f;LSFILES=`cat eurilien.ls???`;FILES=`ls eurilien.* $$LSFILES | sort -u`;tar czvf eurilien.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
