# -*- mode: makefile -*-

analyse.fr.lstex:
	lstex analyse.fr | sort -u > analyse.fr.lstex
analyse.fr.mk:	analyse.fr.lstex
	vcat /ul/prg/RC/texmake > analyse.fr.mk


analyse.fr.dvi:	analyse.fr.mk
	rm -f analyse.fr.lta
	if latex analyse.fr;then test -f analyse.fr.lta && latex analyse.fr;while tail -n 20 analyse.fr.log | grep -w references && latex analyse.fr;do eval;done;fi
	if test -r analyse.fr.idx;then makeindex analyse.fr && latex analyse.fr;fi

analyse.fr.pdf:	analyse.fr.ps
	if grep -w '\(CJK\|epsfig\)' analyse.fr.tex;then ps2pdf analyse.fr.ps;else rm -f analyse.fr.lta;if pdflatex analyse.fr;then test -f analyse.fr.lta && pdflatex analyse.fr;while tail -n 20 analyse.fr.log | grep -w references && pdflatex analyse.fr;do eval;done;fi;fi
	if test -r analyse.fr.idx;then makeindex analyse.fr && pdflatex analyse.fr;fi
analyse.fr.tty:	analyse.fr.dvi
	dvi2tty -q analyse.fr > analyse.fr.tty
analyse.fr.ps:	analyse.fr.dvi	
	dvips  analyse.fr
analyse.fr.001:	analyse.fr.dvi
	rm -f analyse.fr.[0-9][0-9][0-9]
	dvips -i -S 1  analyse.fr
analyse.fr.pbm:	analyse.fr.ps
	pstopbm analyse.fr.ps
analyse.fr.gif:	analyse.fr.ps
	pstogif analyse.fr.ps
analyse.fr.eps:	analyse.fr.dvi
	dvips -E -f analyse.fr > analyse.fr.eps
analyse.fr-01.g3n:	analyse.fr.ps
	ps2fax n analyse.fr.ps
analyse.fr-01.g3f:	analyse.fr.ps
	ps2fax f analyse.fr.ps
analyse.fr.ps.gz:	analyse.fr.ps
	gzip < analyse.fr.ps > analyse.fr.ps.gz
analyse.fr.ps.gz.uue:	analyse.fr.ps.gz
	uuencode analyse.fr.ps.gz analyse.fr.ps.gz > analyse.fr.ps.gz.uue
analyse.fr.faxsnd:	analyse.fr-01.g3n
	set -a;FAXRES=n;FILELIST=`echo analyse.fr-??.g3n`;source faxsnd main
analyse.fr_tex.ps:	
	cat analyse.fr.tex | splitlong | coco | m2ps > analyse.fr_tex.ps
analyse.fr_tex.ps.gz:	analyse.fr_tex.ps
	gzip < analyse.fr_tex.ps > analyse.fr_tex.ps.gz
analyse.fr-01.pgm:
	cat analyse.fr.ps | gs -q -sDEVICE=pgm -sOutputFile=analyse.fr-%02d.pgm -
analyse.fr/analyse.fr.html:	analyse.fr.dvi
	rm -fR analyse.fr;latex2html analyse.fr.tex
xview:	analyse.fr.dvi
	xdvi -s 8 analyse.fr &
tview:	analyse.fr.tty
	browse analyse.fr.tty 
gview:	analyse.fr.ps
	ghostview  analyse.fr.ps &
print:	analyse.fr.ps
	lpr -s -h analyse.fr.ps 
sprint:	analyse.fr.001
	for F in analyse.fr.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	analyse.fr.faxsnd
	faxsndjob view analyse.fr &
fsend:	analyse.fr.faxsnd
	faxsndjob jobs analyse.fr
viewgif:	analyse.fr.gif
	xv analyse.fr.gif &
viewpbm:	analyse.fr.pbm
	xv analyse.fr-??.pbm &
vieweps:	analyse.fr.eps
	ghostview analyse.fr.eps &	
clean:	analyse.fr.ps
	rm -f  analyse.fr-*.tex analyse.fr.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} analyse.fr-??.* analyse.fr_tex.* analyse.fr*~
tgz:	clean
	set +f;LSFILES=`cat analyse.fr.ls???`;FILES=`ls analyse.fr.* $$LSFILES | sort -u`;tar czvf analyse.fr.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
