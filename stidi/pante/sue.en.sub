\begin{subdocument}{swpatpante}{What can we do when supreme courts decide against Law and Liberty?}{http://swpat.ffii.org/analysis/sue/index.en.html}{Workgroup\\swpatag@ffii.org}{The German Patent Office is handing out patents on computer-implementable rules of organisation and calculation (programs for computers), although the written law, when understood either literally or by the only valid systematic interpretation, clearly disallows this.  This illegal caselaw has been made possible by a series of decisions of the German Federal Court (BGH), which ovrerruled the Federal Patent Court (BPatG) in order to void the written law and replace it by new rules, which effectively remove all limits to patentability and all freedoms of programming.   To reverse these court decisions, we need to ask clarification from the legislator and at the same time appeal to constitutional courts.  Anonymous law experts give good advice.}
\begin{sect}{klag}{Klagen Sie!}
Der ehemalige Pr\"{a}sident des DPMA, Norbert Haugg, wird gelegentlich mit den Worten zitiert (sinngem\"{a}{\ss}):

\begin{quote}
Das Deutsche Patent- und Markenamt handelt nach Recht und Gesetz. Wird in einer gerichtlichen \"{U}berpr\"{u}fung festgestellt, da{\ss} einzelne Handlungen nicht rechtm\"{a}{\ss}ig sind, so werden die erforderlichen Schritte unternommen werden. Der Weg zu den Gerichten steht jedermann offen.
\end{quote}

Dieser Weg wurde u.a. schon einmal beschritten, um eine Herausl\"{o}sung des BPatG aus dem DPMA zu erstreiten\footnote{Dies geschah um 1961, zu einer Zeit, als das Patentwesen sich der rechtsstaatlichen Kontrolle in viel geringerem Ma{\ss}e als heute entzogen hatte}.

Ein Patentjurist fordert uns nun auf, in eben diesem Sinne etwas zu unternehmen, und gibt dabei folgenden Rat:

\begin{quote}
Sprechen Sie ein!

Nur so l\"{a}{\ss}t sich an der Praxis von Verwaltungsbeh\"{o}rden etwas \"{a}ndern.

\begin{sect}{fehl}{Ungesetzliche Fehlurteile}
Sie beklagen meiner Ansicht nach zurecht, dass der BGH die Patentierung von ``Programmen mit technischem Charakter'' zul\"{a}{\ss}t, obwohl im \S{}1 PatG Programme f\"{u}r Datenverarbeitungsanlagen explizit nicht als Erfindungen angesehen werden. Der Zusatz ``als solche'' im Absatz 3 ist bestenfalls dahingehend zu beachten, dass die erfinderischen Schritte zur Probleml\"{o}sung wenigstens zum Teil keine Programmnatur besitzen. Von ``technische Programme'' ist dem Gesetzestext nicht zu entnehmen.

Demzufolge w\"{a}ren die wegweisenden Beschl\"{u}sse von ``Seitenpuffer'' bis ``Logikverifikation'' Fehlurteile. Wenn sich nachweisen lie{\ss}e, dass der BGH hier quasi in ``vorauseilendem Gehorsam'' gesetzgeberisch t\"{a}tig war, w\"{a}re dies ein klarer Versto{\ss} gegen die verfassungsm\"{a}{\ss}ige Gewaltenteilung.  Dies kann aber nur durch Klage beim Bundesverfassungsgericht gekl\"{a}rt werden.
\end{sect}

\begin{sect}{offb}{Warum werden Programmierl\"{o}sungen beansprucht aber nicht offenbart?}
Wehren Sie sich dagegen, dass bei programmbezogenen Patenten praktisch nie die Programmtexte in Form von Quellcode (in welcher Form auch immer) angegeben werden. Ich kann mir nicht vorstellen, dass jegliche Form von Programmentwicklung lediglich naheliegende Schritte beinhaltet. Sprechen Sie ein nach \S{}1, weil die Erfindung nicht wiederholbar ist, ohne dass der Fachmann selbst erfinderisch t\"{a}tig sein m\"{u}sste, bzw. lassen Sie sich anhand von nachgereichtem Quellcode nachweisen, dass die Programmierung insgesamt nur naheliegende Schritte umfasst. Wenn das Gegenargument kommt dass aber ``Programme als solche'' nicht als Erfindung angesehen werden, so nehmen Sie das als Beweis, dass es sich um ein Programm gem\"{a}{\ss} \S{}1(2)3 PatG handelt.

Der nachgereichte Quellcode sollte so vollst\"{a}ndig sein, da{\ss} er dem Fachmann erlaubt, ein ausf\"{u}hrbares Programm, das die Aufgabe l\"{o}st, zu erzeugen. Bestehen Sie darauf und erzwingen Sie ein wegweisendes Urteil.
\end{sect}

\begin{sect}{defn}{Eigenm\"{a}chtige Definitionen der Juristen}
Bestehen Sie bei Ihren Einspr\"{u}chen und Klagen, dass Ihre Definitionen von ``Programmen f\"{u}r Datenverarbeitungsanlagen'', ``wissenschaftliche Theorien'', ``mathematischen Methoden'', ``gesch\"{a}ftliche T\"{a}tigkeiten'' und besonders Ihre Definition von Technik ber\"{u}cksichtigt werden, da Sie die Fachleute auf diesen ``technischen'' Gebieten sind und sich folglich die Rechtsprechung daran zu orientieren hat, wie der Fachmann die ihn betreffenden Gesetzestexte zu interpretieren hat. Es kann ja nicht angehen, dass ein Jurist definieren kann, was ein Informatiker unter einem ``Programm f\"{u}r Datenverarbeitungsanlagen'' zu verstehen hat.
\end{sect}

\begin{sect}{aufg}{Beanspruchung von Problemen unzul\"{a}ssig}
Bestehen Sie darauf, dass bei programmbezogenen Erfindungen einzig und allein Programme beansprucht werden k\"{o}nnen. Die \"{u}blichen ``Verfahren zum'' geben \"{u}blicherweise nur eine Aufgabenstellung und damit nicht eine ``Erfindung'' (als L\"{o}sung der Aufgabe) an, wie es im \S{}1 gefordert wird. Erst das Programm, ausgef\"{u}hrt auf einer Datenverarbeitungsanlage stellt die L\"{o}sung der Aufgabe dar. Somit kann bestenfalls das Programm als solches beansprucht werden!
\end{sect}

\begin{sect}{gesr}{Gesetz und Recht, nicht umgekehrt!}
Sprechen Sie ein, so oft es Ihnen m\"{o}glich und gerechtfertigt erscheint, denn die Arbeitsqualit\"{a}t der Pr\"{u}fer wird ma{\ss}geblich danach beurteilt, wieviele Anmeldungen er rechtskr\"{a}ftig erledigt. Ob ein Verfahren nach Recht und Gesetz abgeschlossen wurde, kann nur von den Gerichten gekl\"{a}rt werden. Wenn niemand einspricht und klagt, ist der Weg des geringsten Widerstandes zu einer guten Beurteilung zu kommen, der der Erteilung.

Bestehen Sie bei Ihren Klagen darauf, dass zuerst nach dem Wortlaut des Gesetzes und erst in Zweifelsf\"{a}llen nach der derzeitigen Rechtsprechung verhandelt wird (also nach ``Gesetz und Recht'' und nicht umgekehrt).

Bestehen Sie auf der Anwendung des Deutschen Patentgesetzes, falls das EP\"{U} verabschiedet wird und bestehen Sie darauf, dass jedwede \"{A}nderung des \S{}1 PatG nicht r\"{u}ckwirkend oder gar vorauseilend angewandt wird, au{\ss}er der Gesetzgeber (das Parlament, nicht der BGH) beschlie{\ss}t eine r\"{u}ckwirkende G\"{u}ltigkeit. Falls interessierte Kreise in Deutschland eine Einf\"{u}hrung der Todesstrafe fordern, wird ja auch niemand auf die Idee kommen, schon mal Urteile mit Todesstrafe zu verh\"{a}ngen und zu vollziehen, ohne dass das Gesetz \"{u}berhaupt verabschiedet wurde und vom Zeitraum anwendbar ist.
\end{sect}

\begin{sect}{urhr}{Konflikt mit Urheberrecht}
Bestehen Sie darauf, dass f\"{u}r Programme in welcher Form auch immer das Urheberrecht (Copyright) anzuwenden ist, bei dem solche Probleme wie der Kopierschutz schon l\"{a}ngst gel\"{o}st sind. Dann ergeben sich auch nicht die juristischen Schwierigkeiten mit Datentr\"{a}gern, wie sie j\"{u}ngst in der Entscheidung 17W(pat69/98 der Anmeldung DE4323241) aufgegriffen wurden.
\end{sect}

\begin{sect}{info}{Informatiker als technischer Beruf?}
Fragen Sie doch mal im BMJ nach, ob jetzt f\"{u}r das neue EP\"{U} beabsichtigt ist, auch Informatiker am DPMA als Pr\"{u}fer einzustellen. Ich habe noch die Auskunft erhalten, dass Informatiker kein technischer Beruf sei!
\end{sect}

\begin{sect}{verl}{Softwarepatente gegen ``Verletzer'' nicht durchsetzbar}
Softwarepatente d\"{u}rften bei einer Verletzungsklage nicht durchsetzbar sein.  Wenn der beanspruchte Gegenstand eines Patents durch Programme f\"{u}r Datenverarbeitungsanlagen verletzt werden kann, wird durch diesen beanspruchten Gegenstand Schutz gegen\"{u}ber Programmen f\"{u}r Datenverarbeitungsanlagen als solche begehrt. Nach \S{}1 (3) ist ein solcher Gegenstand nicht patentf\"{a}hig. Das Patent w\"{a}re damit nichtig (\S{}22(1) unter Bezug auf \S{}21(1)1 und \S{}21(2)).
\end{sect}
\end{quote}
\end{sect}

\begin{sect}{ges}{Gesetzlichkeitsprinzip vs Kriminalisierung der B\"{u}rger durch Gerichte}
Neben Grundrechten / Freihaltungsbed\"{u}rfnissen kommt auch das Gesetzlichkeitsprinzip als Klagegrund in Betracht.  Hierzu schreibt ein Rechtskenner:

\begin{quote}
Zur Begr\"{u}ndung einer Verfassungsbeschwerde ist nicht nur Art. 5 des Grundgesetzes (Meinungsfreiheit, Forschungsfreiheit) sondern auch dessen Art. 103 Absatz 2 (Gesetzlichkeitsprinzip) in Betracht zu ziehen. Durch dieses Prinzip wird die Einf\"{u}hrung von Strafvorschriften dem Gesetzgeber vorbehalten und die Auslegung von Strafgesetzen an enge Wortlautgrenzen gebunden. Damit steht es nicht im Einklang, wenn ein Gericht weit entfernt vom Wortlaut des Gesetzes Patente erteilt und damit die Strafbarkeit von Verletzungshandlungen begr\"{u}ndet, die nach dem Wortlaut des Patentgesetzes nicht strafbar sein k\"{o}nnen. Dies gilt vor allem dann, wenn durch ein dichtes Netz von Softwarepatenten praktisch keine M\"{o}glichkeit mehr verbleibt, Software zu schreiben ohne damit automatisch Straftaten zu begehen. Schlagwortartig: ``Kriminalisierung der gesamten Softwarebranche durch die Rechtsprechung? Und das soll mit dem Gesetzlichkeitsprinzip vereinbar sein?''
\end{quote}

Sicherlich geht es um mehr als die Softwarebranche.  Der Universalrechner wird zunehmend zu einem universellen Zivilisationswerkzeug, mit dem sich abstrakte Organisations- und Rechenregeln (menschliche Verstandest\"{a}tigkeit) ohne dar\"{u}ber hinausgehenden gewerblich-technischen Aufwand umsetzen lassen und standardm\"{a}{\ss}ig umgesetzt werden, \"{a}hnlich wie diese fr\"{u}her mit Papier, Bleistift und Abakus geschah.  Somit lie{\ss}e sich argumentieren, die Rechtsprechung habe alle Branchen und alle B\"{u}rger kriminalisiert.
\end{sect}

\begin{sect}{bverf}{Direkte Verfassungsbeschwerde?}
Man k\"{o}nnte einerseits \"{u}ber den Instanzenweg BPatG - BGH - BVerfG eine Nichtigkeitsklage gegen ein exemplarisches Softwarepatent wie z.B. das Netzvergiftungspatent von 7val anstrengen.

Andererseits k\"{o}nnte man beim BVerfG wegen gesetzes- und verfassungswidriger Rechtssprechung in den F\"{a}llen ``Logikverifikation'' und ``Sprachanalyse'' klagen.  Durch solche Urteile wird das vom Gesetzgeber vorgesehene Freihaltungsbed\"{u}rfnis f\"{u}r abstrakt-logische Neuerungen ebenso wie die damit zusammenh\"{a}ngende Meinungs- und Ausdrucksfreiheit nach Art 5 GG unter Strafandrohung gestellt.  Niemandem ist zuzumuten, sich zun\"{a}chst strafbar zu machen, bevor er zum BVerfG weiter schreitet.

Ein weiterer Rechtskenner empfiehlt uns hierzu:

\begin{quote}
Das w\"{u}rde allerdings mehrere Jahre in Anspruch nehmen, bis die Sache \"{u}berhaupt zum Verfassungsgericht kommt, wo sie dann weitere Jahre liegenbleibt.

Zum Bundesverfassungsgericht k\"{o}nnen Sie m\"{o}glicherweise auch mit einer direkten Verfassungsbeschwerde.

Grunds\"{a}tzlich muss ein Beschwerdef\"{u}hrer erst einmal alle anderen Instanzen ersch\"{o}pft haben. Etwas anderes gilt, wenn die behandelte Frage f\"{u}r die Allgemeinheit von erheblicher Bedeutung ist oder dem Beschwerdef\"{u}hrer der Instanzenweg nicht zuzumuten ist.

Erhebliche Bedeutung f\"{u}r die Allgemeinheit l\"{a}sst sich m\"{o}glicherweise mit der hohen Anzahl von Unterschriften unter die Eurolinux-Petition begr\"{u}nden, ebenso auch mit der erheblichen Anzahl von Stellungnahmen aus der Politik.

Unzumutbarkeit des Instanzenweges kann sich aus der Tatsache ergeben, dass alle Softwarepatente auch strafbewehrt sind (Par. 142 Patentgesetz). Niemandem ist es zuzumuten, sich erst einmal wegen Verletzung eines gesetzeswidrig erteilten Patentes strafrechtlich verurteilen zu lassen, bevor eine Verfassungsbeschwerde erhoben wird.

Diese Fragen des direkten Zugangs zum Verfassungsgericht bed\"{u}rften allerdings wohl noch n\"{a}herer Pr\"{u}fung.
\end{quote}
\end{sect}

\begin{sect}{norm}{Normenkontrollverfahren, Europ\"{a}ischer Gerichtshof f\"{u}r Menscherechte, ...?}
Es l\"{a}sst sich sowohl die Beschr\"{a}nkung von wichtigen Freiheiten als auch richterliche Willk\"{u}r des BGH r\"{u}gen.

Ein Normenkontrollverfahren l\"{a}sst sich in Deutschland kaum anstrengen, da \begin{enumerate}
\item
nur Regierungen und Parlamente dazu befugt sind

\item
wir nicht eine Gesetzesnorm sondern deren Veruntreuung durch den BGH zu r\"{u}gen haben.
\end{enumerate}  In Frankreich g\"{a}be es hingegen die M\"{o}glichkeit einer Popularklage.

Ein Jurist empfiehlt als Ersatz f\"{u}r ein Normenkontrollverfahren die Erstattung eines Rechtsgutachtens beim Europ\"{a}ischen Gerichtshof f\"{u}r Menschenrechte:

\begin{quote}
Wenn Du \emph{Sachargumente} hast, also mal ganz losgel\"{o}st von der gegenw\"{a}rtigen Rechtslage argumentieren k\"{o}nntest, einfach Pro und Contra SWPAT, k\"{o}nntest Du theoretisch anregen, dass das Ministerkomitee des Europarates beim Europ\"{a}ischen Gerichtshof f\"{u}r Menschenrechte die Erstattung eines Rechtsgutachtens beantragt (Art. 47 EMRK). Du m\"{u}sstest aber behaupten, kannst Du ja mal versuchen, dass SWPAT als solche menschenrechtswidrig sind. Einen Anspruch darauf, dass das Ministerkomitee Dich anh\"{o}rt, hast Du nicht. Aber kostet nichts. Und ist an sich genau die Art von pr\"{a}ventiver Normenkontrolle, die Dir vorschwebt. Ist ja eine zentrale Frage in Europa. Rauskommen k\"{o}nnte, dass SWPAT in gewissen F\"{a}llen gegen die Menschenrechte versto{\ss}en.
\end{quote}

Vielleicht w\"{a}re eine Klage beim BVerfG denkbar gegen die Bestrebungen der EU-Kommission\footnote{http://europa.eu.int/eur-lex/en/com/pdf/2000/com2000\_0199en01.pdf}, ein einheitliches europ\"{a}isches Patentgericht zu schaffen, welches au{\ss}erhalb der bisher nur auf nationaler Ebene gew\"{a}hrleisteten demokratischen Gewaltenteilung steht.

Auch der Basisvorschlag des EPA l\"{a}uft auf eine Entmachtung der nationalen Parlamente und Gerichte und Konzentration aller Macht beim EPA hinaus.  Das hat selbst die Deutsche Patentanwaltskammer\footnote{http://www.patentanwalt.de} in einem Gutachten bem\"{a}ngelt -- Sie begr\"{u}{\ss}t zwar die grenzenlose Patentierbarkeit, mahnt aber ansonsten die Entmachtung der Parlamente und den Bruch der deutschen Verfassung an.
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

