<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: What can we do when supreme courts decide against Law and Liberty?

#descr: The German Patent Office is handing out patents on computer-implementable rules of organisation and calculation (programs for computers), although the written law, when understood either literally or by the only valid systematic interpretation, clearly disallows this.  This illegal caselaw has been made possible by a series of decisions of the German Federal Court (BGH), which ovrerruled the Federal Patent Court (BPatG) in order to void the written law and replace it by new rules, which effectively remove all limits to patentability and all freedoms of programming.   To reverse these court decisions, we need to ask clarification from the legislator and at the same time appeal to constitutional courts.  Anonymous law experts give good advice.

#KgW: Klagen Sie!

#Gis: Gesetzlichkeitsprinzip vs Kriminalisierung der Bürger durch Gerichte

#Dre: Direkte Verfassungsbeschwerde?

#NWf: Normenkontrollverfahren, Europäischer Gerichtshof für Menscherechte, ...?

#DWm: Der ehemalige Präsident des DPMA, Norbert Haugg, wird gelegentlich mit den Worten zitiert (sinngemäß):

#DhW: Das Deutsche Patent- und Markenamt handelt nach Recht und Gesetz. Wird in einer gerichtlichen Überprüfung festgestellt, daß einzelne Handlungen nicht rechtmäßig sind, so werden die erforderlichen Schritte unternommen werden. Der Weg zu den Gerichten steht jedermann offen.

#Dbd: Dieser Weg wurde u.a. schon einmal beschritten, um eine Herauslösung des BPatG aus dem DPMA zu erstreiten

#Dti: Dies geschah um 1961, zu einer Zeit, als das Patentwesen sich der rechtsstaatlichen Kontrolle in viel geringerem Maße als heute entzogen hatte

#Ejr: Ein Patentjurist fordert uns nun auf, in eben diesem Sinne etwas zu unternehmen, und gibt dabei folgenden Rat:

#SrW: Sprechen Sie ein!

#Nrg: Nur so läßt sich an der Praxis von Verwaltungsbehörden etwas ändern.

#ulh: Ungesetzliche Fehlurteile

#WeW: Warum werden Programmierlösungen beansprucht aber nicht offenbart?

#EWW: Eigenmächtige Definitionen der Juristen

#Bgm: Beanspruchung von Problemen unzulässig

#GRt: Gesetz und Recht, nicht umgekehrt!

#KWe: Konflikt mit Urheberrecht

#Iri: Informatiker als technischer Beruf?

#SgW: Softwarepatente gegen %(q:Verletzer) nicht durchsetzbar

#StW: Sie beklagen meiner Ansicht nach zurecht, dass der BGH die Patentierung von %(q:Programmen mit technischem Charakter) zuläßt, obwohl im §1 PatG Programme für Datenverarbeitungsanlagen explizit nicht als Erfindungen angesehen werden. Der Zusatz %(q:als solche) im Absatz 3 ist bestenfalls dahingehend zu beachten, dass die erfinderischen Schritte zur Problemlösung wenigstens zum Teil keine Programmnatur besitzen. Von %(q:technische Programme) ist dem Gesetzestext nicht zu entnehmen.

#Dho: Demzufolge wären die wegweisenden Beschlüsse von %(q:Seitenpuffer) bis %(q:Logikverifikation) Fehlurteile. Wenn sich nachweisen ließe, dass der BGH hier quasi in %(q:vorauseilendem Gehorsam) gesetzgeberisch tätig war, wäre dies ein klarer Verstoß gegen die verfassungsmäßige Gewaltenteilung.  Dies kann aber nur durch Klage beim Bundesverfassungsgericht geklärt werden.

#Waa: Wehren Sie sich dagegen, dass bei programmbezogenen Patenten praktisch nie die Programmtexte in Form von Quellcode (in welcher Form auch immer) angegeben werden. Ich kann mir nicht vorstellen, dass jegliche Form von Programmentwicklung lediglich naheliegende Schritte beinhaltet. Sprechen Sie ein nach §1, weil die Erfindung nicht wiederholbar ist, ohne dass der Fachmann selbst erfinderisch tätig sein müsste, bzw. lassen Sie sich anhand von nachgereichtem Quellcode nachweisen, dass die Programmierung insgesamt nur naheliegende Schritte umfasst. Wenn das Gegenargument kommt dass aber %(q:Programme als solche) nicht als Erfindung angesehen werden, so nehmen Sie das als Beweis, dass es sich um ein Programm gemäß §1(2)3 PatG handelt.

#Dcr: Der nachgereichte Quellcode sollte so vollständig sein, daß er dem Fachmann erlaubt, ein ausführbares Programm, das die Aufgabe löst, zu erzeugen. Bestehen Sie darauf und erzwingen Sie ein wegweisendes Urteil.

#BeW: Bestehen Sie bei Ihren Einsprüchen und Klagen, dass Ihre Definitionen von %(q:Programmen für Datenverarbeitungsanlagen), %(q:wissenschaftliche Theorien), %(q:mathematischen Methoden), %(q:geschäftliche Tätigkeiten) und besonders Ihre Definition von Technik berücksichtigt werden, da Sie die Fachleute auf diesen %(q:technischen) Gebieten sind und sich folglich die Rechtsprechung daran zu orientieren hat, wie der Fachmann die ihn betreffenden Gesetzestexte zu interpretieren hat. Es kann ja nicht angehen, dass ein Jurist definieren kann, was ein Informatiker unter einem %(q:Programm für Datenverarbeitungsanlagen) zu verstehen hat.

#Bnm: Bestehen Sie darauf, dass bei programmbezogenen Erfindungen einzig und allein Programme beansprucht werden können. Die üblichen %(q:Verfahren zum) geben üblicherweise nur eine Aufgabenstellung und damit nicht eine %(q:Erfindung) (als Lösung der Aufgabe) an, wie es im §1 gefordert wird. Erst das Programm, ausgeführt auf einer Datenverarbeitungsanlage stellt die Lösung der Aufgabe dar. Somit kann bestenfalls das Programm als solches beansprucht werden!

#SWr: Sprechen Sie ein, so oft es Ihnen möglich und gerechtfertigt erscheint, denn die Arbeitsqualität der Prüfer wird maßgeblich danach beurteilt, wieviele Anmeldungen er rechtskräftig erledigt. Ob ein Verfahren nach Recht und Gesetz abgeschlossen wurde, kann nur von den Gerichten geklärt werden. Wenn niemand einspricht und klagt, ist der Weg des geringsten Widerstandes zu einer guten Beurteilung zu kommen, der der Erteilung.

#BWg: Bestehen Sie bei Ihren Klagen darauf, dass zuerst nach dem Wortlaut des Gesetzes und erst in Zweifelsfällen nach der derzeitigen Rechtsprechung verhandelt wird (also nach %(q:Gesetz und Recht) und nicht umgekehrt).

#Bdn: Bestehen Sie auf der Anwendung des Deutschen Patentgesetzes, falls das EPÜ verabschiedet wird und bestehen Sie darauf, dass jedwede Änderung des §1 PatG nicht rückwirkend oder gar vorauseilend angewandt wird, außer der Gesetzgeber (das Parlament, nicht der BGH) beschließt eine rückwirkende Gültigkeit. Falls interessierte Kreise in Deutschland eine Einführung der Todesstrafe fordern, wird ja auch niemand auf die Idee kommen, schon mal Urteile mit Todesstrafe zu verhängen und zu vollziehen, ohne dass das Gesetz überhaupt verabschiedet wurde und vom Zeitraum anwendbar ist.

#Bmi: Bestehen Sie darauf, dass für Programme in welcher Form auch immer das Urheberrecht (Copyright) anzuwenden ist, bei dem solche Probleme wie der Kopierschutz schon längst gelöst sind. Dann ergeben sich auch nicht die juristischen Schwierigkeiten mit Datenträgern, wie sie jüngst in der %(bp:Entscheidung 17W(pat)69/98 der Anmeldung DE4323241) aufgegriffen wurden.

#FiW: Fragen Sie doch mal im BMJ nach, ob jetzt für das neue EPÜ beabsichtigt ist, auch Informatiker am DPMA als Prüfer einzustellen. Ich habe noch die Auskunft erhalten, dass Informatiker kein technischer Beruf sei!

#SnW: Softwarepatente dürften bei einer Verletzungsklage nicht durchsetzbar sein.  Wenn der beanspruchte Gegenstand eines Patents durch Programme für Datenverarbeitungsanlagen verletzt werden kann, wird durch diesen beanspruchten Gegenstand Schutz gegenüber Programmen für Datenverarbeitungsanlagen als solche begehrt. Nach §1 (3) ist ein solcher Gegenstand nicht patentfähig. Das Patent wäre damit nichtig (§22(1) unter Bezug auf §21(1)1 und §21(2)).

#NaB: Neben Grundrechten / Freihaltungsbedürfnissen kommt auch das Gesetzlichkeitsprinzip als Klagegrund in Betracht.  Hierzu schreibt ein Rechtskenner:

#Zvj: Zur Begründung einer Verfassungsbeschwerde ist nicht nur Art. 5 des Grundgesetzes (Meinungsfreiheit, Forschungsfreiheit) sondern auch dessen Art. 103 Absatz 2 (Gesetzlichkeitsprinzip) in Betracht zu ziehen. Durch dieses Prinzip wird die Einführung von Strafvorschriften dem Gesetzgeber vorbehalten und die Auslegung von Strafgesetzen an enge Wortlautgrenzen gebunden. Damit steht es nicht im Einklang, wenn ein Gericht weit entfernt vom Wortlaut des Gesetzes Patente erteilt und damit die Strafbarkeit von Verletzungshandlungen begründet, die nach dem Wortlaut des Patentgesetzes nicht strafbar sein können. Dies gilt vor allem dann, wenn durch ein dichtes Netz von Softwarepatenten praktisch keine Möglichkeit mehr verbleibt, Software zu schreiben ohne damit automatisch Straftaten zu begehen. Schlagwortartig: %(q:Kriminalisierung der gesamten Softwarebranche durch die Rechtsprechung? Und das soll mit dem Gesetzlichkeitsprinzip vereinbar sein?)

#Ssi: Sicherlich geht es um mehr als die Softwarebranche.  Der Universalrechner wird zunehmend zu einem universellen Zivilisationswerkzeug, mit dem sich abstrakte Organisations- und Rechenregeln (menschliche Verstandestätigkeit) ohne darüber hinausgehenden gewerblich-technischen Aufwand umsetzen lassen und standardmäßig umgesetzt werden, ähnlich wie diese früher mit Papier, Bleistift und Abakus geschah.  Somit ließe sich argumentieren, die Rechtsprechung habe alle Branchen und alle Bürger kriminalisiert.

#MnW: Man könnte einerseits über den Instanzenweg BPatG - BGH - BVerfG eine Nichtigkeitsklage gegen ein exemplarisches Softwarepatent wie z.B. das %(up:Netzvergiftungspatent von 7val) anstrengen.

#Ato: Andererseits könnte man beim BVerfG wegen gesetzes- und verfassungswidriger Rechtssprechung in den Fällen %(q:Logikverifikation) und %(q:Sprachanalyse) klagen.  Durch solche Urteile wird das vom Gesetzgeber vorgesehene Freihaltungsbedürfnis für abstrakt-logische Neuerungen ebenso wie die damit zusammenhängende Meinungs- und Ausdrucksfreiheit nach Art 5 GG unter Strafandrohung gestellt.  Niemandem ist zuzumuten, sich zunächst strafbar zu machen, bevor er zum BVerfG weiter schreitet.

#Eci: Ein weiterer Rechtskenner empfiehlt uns hierzu:

#Dng: Das würde allerdings mehrere Jahre in Anspruch nehmen, bis die Sache überhaupt zum Verfassungsgericht kommt, wo sie dann weitere Jahre liegenbleibt.

#Zne: Zum Bundesverfassungsgericht können Sie möglicherweise auch mit einer direkten Verfassungsbeschwerde.

#Gue: Grundsätzlich muss ein Beschwerdeführer erst einmal alle anderen Instanzen erschöpft haben. Etwas anderes gilt, wenn die behandelte Frage für die Allgemeinheit von erheblicher Bedeutung ist oder dem Beschwerdeführer der Instanzenweg nicht zuzumuten ist.

#Esy: Erhebliche Bedeutung für die Allgemeinheit lässt sich möglicherweise mit der hohen Anzahl von Unterschriften unter die Eurolinux-Petition begründen, ebenso auch mit der erheblichen Anzahl von Stellungnahmen aus der Politik.

#Uhw: Unzumutbarkeit des Instanzenweges kann sich aus der Tatsache ergeben, dass alle Softwarepatente auch strafbewehrt sind (Par. 142 Patentgesetz). Niemandem ist es zuzumuten, sich erst einmal wegen Verletzung eines gesetzeswidrig erteilten Patentes strafrechtlich verurteilen zu lassen, bevor eine Verfassungsbeschwerde erhoben wird.

#Dei: Diese Fragen des direkten Zugangs zum Verfassungsgericht bedürften allerdings wohl noch näherer Prüfung.

#Enh: Es lässt sich sowohl die Beschränkung von wichtigen Freiheiten als auch richterliche Willkür des BGH rügen.

#Enh2: Ein Normenkontrollverfahren lässt sich in Deutschland kaum anstrengen, da %(ol:nur Regierungen und Parlamente dazu befugt sind:wir nicht eine Gesetzesnorm sondern deren Veruntreuung durch den BGH zu rügen haben.)  In Frankreich gäbe es hingegen die Möglichkeit einer Popularklage.

#Ers: Ein Jurist empfiehlt als Ersatz für ein Normenkontrollverfahren die Erstattung eines Rechtsgutachtens beim Europäischen Gerichtshof für Menschenrechte:

#WWi: Wenn Du %(e:Sachargumente) hast, also mal ganz losgelöst von der gegenwärtigen Rechtslage argumentieren könntest, einfach Pro und Contra SWPAT, könntest Du theoretisch anregen, dass das Ministerkomitee des Europarates beim Europäischen Gerichtshof für Menschenrechte die Erstattung eines Rechtsgutachtens beantragt (Art. 47 EMRK). Du müsstest aber behaupten, kannst Du ja mal versuchen, dass SWPAT als solche menschenrechtswidrig sind. Einen Anspruch darauf, dass das Ministerkomitee Dich anhört, hast Du nicht. Aber kostet nichts. Und ist an sich genau die Art von präventiver Normenkontrolle, die Dir vorschwebt. Ist ja eine zentrale Frage in Europa. Rauskommen könnte, dass SWPAT in gewissen Fällen gegen die Menschenrechte verstoßen.

#Zrt: Vielleicht wäre eine Klage beim BVerfG denkbar gegen die %(ep:Bestrebungen der EU-Kommission), ein einheitliches europäisches Patentgericht zu schaffen, welches außerhalb der bisher nur auf nationaler Ebene gewährleisteten demokratischen Gewaltenteilung steht.

#AWW: Auch der %(bv:Basisvorschlag des EPA) läuft auf eine Entmachtung der nationalen Parlamente und Gerichte und Konzentration aller Macht beim EPA hinaus.  Das hat selbst die %(pa:Deutsche Patentanwaltskammer) in einem Gutachten bemängelt -- Sie begrüßt zwar die grenzenlose Patentierbarkeit, mahnt aber ansonsten die Entmachtung der Parlamente und den Bruch der deutschen Verfassung an.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatpante ;
# txtlang: en ;
# multlin: t ;
# End: ;

