\begin{subdocument}{swpatstidi}{Brevets Logiciels: Questions, Analyses, Propos}{http://swpat.ffii.org/stidi/index.fr.html}{Groupe de travail\\\url{swpatag@ffii.org}\\version fran\c{c}aise 2005/01/06 par Gerald SEDRATI-DINET\footnote{\url{http://gibuskro.lautre.net/}}}{Quel est l'effet du syst\`{e}me de brevets sur l'\'{e}conomie en g\'{e}n\'{e}rale et dans le cas du logiciel?  Pour quoi est-ce que les brevets logiciels sont si triviales?  Quelles ont \'{e}t\'{e} les r\`{e}gles de la brevetabilit\'{e} en Europe et comment ont ils chang\'{e}?  Sous quelles contraintes est-ce que le syst\`{e}me op\'{e}re?  Entre quels propos pouvons-nous choisir?  Voici une collection de textes qui essaient de repondre a ces questions.}
\begin{sect}{kver}{Les Questions Fondamentales}
Diverse organismes gouvernmentaux on conduit des consultations sur le brevetabilit\'{e} du logiciel, mais en beaucoup de cas on demandait des questions inappropri\'{e}es, ce qu'a provoquer des d\'{e}bats virtuels et des \'{e}tudes douteuses.  Nous croyons qu'une discussion s\'{e}rieuse doit repondre aux questions suivantes.

\begin{enumerate}
\item
Bei wieviel \percent{} der bislang vom EPA gew\"{a}hrten ca 30000 Softwarepatente ist die beanspruchte geistige Leistung so beeindruckend, dass es sich f\"{u}r uns als Gesetzgeber lohnen k\"{o}nnte, dar\"{u}ber zu diskutieren, ob wir f\"{u}r diese Leistung ein zeitbefristetes Monopolrecht gew\"{a}hren m\"{o}chten? (Bitte zitieren Sie die Hauptanspr\"{u}che von ein paar EPA-Softwarepatenten, um Ihre Aussage zu erl\"{a}utern?)\\
Was f\"{u}r Ideen werden typischerweise in EPA-Softwarepatenten beansprucht, welcher Erfindungsaufwand steckt dahinter und wie viel sp\"{a}ter (fr\"{u}her?) w\"{u}rden diese Ideen bekannt und fruchtbar werden, wenn daf\"{u}r keine Patente erh\"{a}ltlich w\"{a}ren?

\item
Inwieweit leidet die Innovationsfreude der Softwarebranche an zu schneller Nachahmung?\\
Bedarf das Software-Urheberrecht einer Verbesserung oder einer Erg\"{a}nzung durch das Patentrecht o.\"{a}.?\\
Wie m\"{u}sste ein optimales ``ma{\ss}geschneidertes Software-Verg\"{u}tungsrecht'' (lex sui generis) aussehen?

\item
Warum gibt es Freie Software aber nicht freie Hardware?\\
Wie unterscheidet sich die \"{O}konomie der immateriellen G\"{u}ter von der der materiellen G\"{u}ter?\\
Welche Rolle kann/soll freie Software f\"{u}r die Informationsgesellschaft spielen?\\
Unter welchen Regeln k\"{o}nnen propriet\"{a}re und freie Software produktiv zusammenwirken?

\item
Nach welchen Regeln beurteilen EPA, BGH und BPatG heute die Patentierbarkeit von Software?\\
Sind diese Regeln klar?\\
Wo wurden sie am klarsten formuliert?

\item
Nach welchen Regeln beurteilten EPA, BGH und BPatG um 1985 die Patentierbarkeit von Software?\\
Waren diese Regeln klar?\\
Wo wurden sie am klarsten formuliert?

\item
Gibt es internationale Vertr\"{a}ge oder andere rechtliche Beschr\"{a}nkungen, welche unseren gesetzgeberischen Handlungsspielraum bez\"{u}glich Softwarepatenten einengen, etwa indem sie eine Patentierbarkeit von Software erfordern, verbieten oder an Bedingungen kn\"{u}pfen?

\item
Welche klaren Abgrenzungsregeln zur Patentierbarkeit oder Patentdurchsetzbarkeit stehen derzeit zur Debatte?\\
Wie viel \percent{} der bisher erteilten EPA-Patente (Hard- und Software) w\"{u}rden durch die jeweilige Abgrenzungsregel als rechtsbest\"{a}ndig und durchsetzbar best\"{a}tigt?\\
Was halten Sie von den folgenden m\"{o}glichen Optionen?
\begin{itemize}
\item
{\bf Bereich der patentierbaren Ideen}

\begin{quote}
\begin{itemize}
\item
{\bf ``Praktische und wiederholbare Probleml\"{o}sungen''\footnote{\url{http://swpat.ffii.org/vreji/papri/jwip-schar98/}}}

\begin{quote}
Handlungsanweisungen aller Art k\"{o}nnen patentiert werden, sofern sie objektivierbar (subjektunabh\"{a}ngig wiederholbar) sind und sich in der materiellen Welt (au{\ss}erhalb des menschlichen Geistes) ereignen.  Reine Organisations- und Rechenregeln sind patentierbar, m\"{u}ssen aber auf bestimmte praktische Anwendungen festgelegt werden.  Dies kann z.B. durch eine lange Liste von Patentanspr\"{u}chen geschehen.  Diese Doktrin wird sowohl vom US-Patentamt als auch von f\"{u}hrenden Rechtsdogmatikern des EPA bevorzugt.
\end{quote}
\filbreak

\item
{\bf Technik = angewandte Naturwissenschaft\footnote{\url{http://localhost/swpat/stidi/korcu/index.fr.html}}}

\begin{quote}
Die Erfindung muss neue Wirkungszusammenh\"{a}nge von Naturkr\"{a}ften lehren und darf sich nicht in ``Organisations- und Rechenregeln'' ersch\"{o}pfen.  Wer nicht Probleme des unmittelbaren Naturkr\"{a}fteeinsatzes sondern nur Probleme innerhalb einer abstrakten Maschine oder eines bekannten Modells l\"{o}st, tr\"{a}gt nichts zum ``Stand der Technik'' bei.  Diese Doktrin geh\"{o}rte bis vor kurzem in Europa, Japan, USA u.a. zum unumstrittenen Gewohnheitsrecht des Patentwesens.  Sie wurde vom BGH bis in die 80er Jahre St\"{u}ck f\"{u}r St\"{u}ck begrifflich verfeinert.  Sie findet sich in Rechtslehrb\"{u}chern, Gesetzeskommentaren, Pr\"{u}fungsrichtlinien und BPatG-Entscheidungen bis zum Jahr 2000 wieder. Die Eurolinux-Allianz fordert\footnote{\url{http://petition.eurolinux.org/consultation/ec-consult.pdf}} f\"{u}r die Zukunft eine konsequente Anwendung und Weiterentwicklung dieser Doktrin.
\end{quote}
\filbreak

\item
{\bf Abstraktionen in Reinform\footnote{\url{http://localhost/swpat/papri/irle-laat00/index.en.html}}}

\begin{quote}
Diese gelegentlich von Freunden der \"{a}u{\ss}ersten Konsequenz ins Gespr\"{a}ch gebrachte Doktrin erlaubt die Patentierung rein abstrakt-mathematischer Methoden ohne Bindung an bestimmte praktische Anwendungen.  Die Frage der Patentierbarkeit wird nicht mehr gestellt.  Das lenkt den Blick auf die zu oft vernachl\"{a}ssigte Frage, gegen welche konkreten Realit\"{a}ten die abstrakten Anspr\"{u}che denn durchsetzbar sein sollen.
\end{quote}
\filbreak

\item
{\bf ``Dynamischer Technikbegriff''\footnote{\url{http://swpat.ffii.org/stidi/korcu}}}

\begin{quote}
``Man r\"{u}hre alle drei obigen Doktrinen durcheiander und treibe von Urteil zu Urteil in Zickzack-Bewegungen auf einen Zustand grenzenloser Patentierbarkeit zu.
Um der ``Rechtssicherheit'' (= Verhinderung des Widerstandes ``konservativer'' Gerichte) willen lasse man sich diesen Kurs gelegentlich vom Gesetzgeber durch allerlei biegsame Gesetze und Richtlinien best\"{a}tigen.''
B\"{o}se Zungen behaupten, diese Realit\"{a}t sei gemeint, wenn interessierte Patentjuristen die Vorz\"{u}ge eines ``dynamischen Technikbegriffs'' preisen.
\end{quote}
\filbreak
\end{itemize}
\end{quote}
\filbreak

\item
{\bf Bereich der Verletzungshandlungen}

\begin{quote}
Informationsgebilde und andere Immaterialg\"{u}ter tendieren dazu, Gemeingut zu sein oder zu werden.  F\"{u}r sie gelten \"{a}hnliche \"{o}konomische Regeln und Freiheitsbed\"{u}rfnisse wie f\"{u}r menschliche Gedanken.  Es w\"{a}re m\"{o}glich, sie grunds\"{a}tzlich nicht als Verletzungsgegenst\"{a}nde zu betrachten, egal was im Patentanspruch steht.  Nicht nur wie bisher die ``Anwendung im privaten Bereich oder f\"{u}r Forschungszwecke'' sondern auch die Informationsallmende w\"{u}rde zur patentbefreiten Zone erkl\"{a}rt.  Patente k\"{o}nnten dann nicht genutzt werden, um die Verbreitung von Immaterialg\"{u}tern zu unterbinden.  Lutterbeck, Horns et Gehring\footnote{\url{http://localhost/swpat/papri/bmwi-luhoge00/index.de.html}} formulieren \"{a}hnliches als ``Quelltextprivileg'', aber auch eine grunds\"{a}tzliche ``Privilegierung'' aller Informationsgebilde (z.B. Informationsstrukturen jedweder Art auf Datentr\"{a}ger, sowie das Auslesen solcher Informationsstrukturen auf Universalrechnern, Musikabspielger\"{a}ten u.dgl.) w\"{a}re denkbar. Der Raum der potentiellen Verletzungsgegenst\"{a}nde w\"{u}rde auf die Sph\"{a}re der (ihrem Wesen nach f\"{u}r den privaten Besitz bestimmten, industriell hergestellten) materiellen G\"{u}ter eingegrenzt.  Die Frage, ob Patente auf ``Organisations- und Rechenregeln'' u.dgl. zul\"{a}ssig sein sollen oder nicht, r\"{u}ckt in den Hintergrund.  Beliebige Kombinationen mit lascheren oder strengeren Patentierbarkeitsdoktrinen sind denkbar.
\end{quote}
\filbreak

\item
{\bf Erfindungh\"{o}he}

\begin{quote}
Einige Leute suchen nach wirksamen Kriterien und Spielregeln, die es erlauben, mit der Forderung nach Erfindungsh\"{o}he ernst zu machen und triviale Patente zu eliminieren.  Das holl\"{a}ndische Parlament hat etwa gefordert, dass dieses Problem gel\"{o}st werden m\"{u}sse, bevor \"{u}ber die Patentierbarkeit von Software diskutiert werden k\"{o}nne.
\end{quote}
\filbreak

\item
{\bf Organisation des Patentwesens}

\begin{quote}
Seit Jahrzehnten wird oft kritisiert, das Patentpr\"{u}fungssystem sei seiner Aufgabe, ungerechtfertigte Patentanspr\"{u}che schnell und zuverl\"{a}ssig auszusortieren, nicht gewachsen, und es habe sich eine Eigendynamik entwickelt, die zu immer mehr und immer fragw\"{u}rdigeren Patenten f\"{u}hre.  Der franz\"{o}sische Abgeordnete Le D\'{e}aux fordert gar, eine Kommission einzurichten, die Fehlsteuerungen im europ\"{a}ischen Patentwesen untersuchen und Empfehlungen zu einer institutionellen Reform geben soll.  Manche Leute meinen, der Schl\"{u}ssel zur Verbesserung liege weniger in den Gesetzesregeln der Patentierung als in dem institutionellen Rahmen, in dem diese Regeln angewendet werden.
\end{quote}
\filbreak
\end{itemize}

\item
Was passiert mit den Patenten, die nach einer neuen Richtlinie keinen Bestand mehr haben?
\end{enumerate}
\end{sect}

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/korcu/swpatkorcu.fr.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/javni/swpatjavni.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/trips/swpattrips.fr.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/frili/swpatfrili.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/fukpi/swpatfukpi.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/nitcu/swpatnitcu.fr.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/manri/swpatmanri.fr.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/epc52/epue52.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/gacri/swpatgacri.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/preti/swpatpreti.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/pleji/swpatpleji.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/basti/swpatbasti.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/tisna/swpattisna.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/cteki/swpatcteki.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/sektor/sektor.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/pante/swpatpante.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/danfu/swpatdanfu.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/unwort/swpatunwort.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/lijda/swpatlijda.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/catlu/swpatcatlu.fr.sub}
\else
\dots
\fi

\begin{sect}{altr}{Autres efforts de poser des questions serieusement}
\begin{itemize}
\item
{\bf {\bf (Aigrain 2002: 11 questions on software patentability issues in the US and Europe)\footnote{\url{http://cip.umd.edu/Aigrain.htm}}}}

\begin{quote}
a paper representing the private views of Philippe Aigrain, head of a department in the Infosoc Directorate of the European Commission.  Asks some questions which the patent movement probably doesn't like.  In some cases the the answer is almost evident once the taboo-breaking question is asked, in other cases the answer requires some research that is yet waiting to be done.  The paper was prepared for a conference on open software and information policy research hosted by the US National Science Foundation in Arlington near Washington.  H. Pilch from FFII is also attending.
\end{quote}
\filbreak

\item
{\bf {\bf Konferenz über Informationelle Infrastrukturpolitik 2002-01-28 Arlington USA\footnote{\url{http://localhost/swpat/penmi/2002/arli01/index.en.html}}}}

\begin{quote}
Hartmut Pilch vom FFII wird auf dieser Konferenz \"{u}ber Swpat-Fragen berichten.  Weitere Beitr\"{a}ge zum Thema werden von Philippe Aigrain (Eur. Kommission), Brian Kahin (Univ. Maryland) u.a. erwartet.  Hoffentlich wird die Konferenz einige lange ben\"{o}tigte Forschung in Gang bringen helfen.
\end{quote}
\filbreak

\item
{\bf {\bf \url{}}}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

