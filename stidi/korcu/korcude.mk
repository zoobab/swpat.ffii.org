korcude.lstex:
	lstex korcude | sort -u > korcude.lstex
korcude.mk:	korcude.lstex
	vcat /ul/prg/RC/texmake > korcude.mk


korcude.dvi:	korcude.mk korcude.tex
	latex korcude && while tail -n 20 korcude.log | grep references && latex korcude;do eval;done
	if test -r korcude.idx;then makeindex korcude && latex korcude;fi
korcude.pdf:	korcude.mk korcude.tex
	pdflatex korcude && while tail -n 20 korcude.log | grep references && pdflatex korcude;do eval;done
	if test -r korcude.idx;then makeindex korcude && pdflatex korcude;fi
korcude.tty:	korcude.dvi
	dvi2tty -q korcude > korcude.tty
korcude.ps:	korcude.dvi	
	dvips  korcude
korcude.001:	korcude.dvi
	rm -f korcude.[0-9][0-9][0-9]
	dvips -i -S 1  korcude
korcude.pbm:	korcude.ps
	pstopbm korcude.ps
korcude.gif:	korcude.ps
	pstogif korcude.ps
korcude.eps:	korcude.dvi
	dvips -E -f korcude > korcude.eps
korcude-01.g3n:	korcude.ps
	ps2fax n korcude.ps
korcude-01.g3f:	korcude.ps
	ps2fax f korcude.ps
korcude.ps.gz:	korcude.ps
	gzip < korcude.ps > korcude.ps.gz
korcude.ps.gz.uue:	korcude.ps.gz
	uuencode korcude.ps.gz korcude.ps.gz > korcude.ps.gz.uue
korcude.faxsnd:	korcude-01.g3n
	set -a;FAXRES=n;FILELIST=`echo korcude-??.g3n`;source faxsnd main
korcude_tex.ps:	
	cat korcude.tex | splitlong | coco | m2ps > korcude_tex.ps
korcude_tex.ps.gz:	korcude_tex.ps
	gzip < korcude_tex.ps > korcude_tex.ps.gz
korcude-01.pgm:
	cat korcude.ps | gs -q -sDEVICE=pgm -sOutputFile=korcude-%02d.pgm -
korcude/korcude.html:	korcude.dvi
	rm -fR korcude;latex2html korcude.tex
xview:	korcude.dvi
	xdvi -s 8 korcude &
tview:	korcude.tty
	browse korcude.tty 
gview:	korcude.ps
	ghostview  korcude.ps &
print:	korcude.ps
	lpr -s -h korcude.ps 
sprint:	korcude.001
	for F in korcude.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	korcude.faxsnd
	faxsndjob view korcude &
fsend:	korcude.faxsnd
	faxsndjob jobs korcude
viewgif:	korcude.gif
	xv korcude.gif &
viewpbm:	korcude.pbm
	xv korcude-??.pbm &
vieweps:	korcude.eps
	ghostview korcude.eps &	
clean:	korcude.ps
	rm -f  korcude-*.tex korcude.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} korcude-??.* korcude_tex.* korcude*~
tgz:	clean
	set +f;LSFILES=`cat korcude.ls???`;FILES=`ls korcude.* $$LSFILES | sort -u`;tar czvf korcude.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
