korcuen.lstex:
	lstex korcuen | sort -u > korcuen.lstex
korcuen.mk:	korcuen.lstex
	vcat /ul/prg/RC/texmake > korcuen.mk


korcuen.dvi:	korcuen.mk korcuen.tex
	latex korcuen && while tail -n 20 korcuen.log | grep references && latex korcuen;do eval;done
	if test -r korcuen.idx;then makeindex korcuen && latex korcuen;fi
korcuen.pdf:	korcuen.mk korcuen.tex
	pdflatex korcuen && while tail -n 20 korcuen.log | grep references && pdflatex korcuen;do eval;done
	if test -r korcuen.idx;then makeindex korcuen && pdflatex korcuen;fi
korcuen.tty:	korcuen.dvi
	dvi2tty -q korcuen > korcuen.tty
korcuen.ps:	korcuen.dvi	
	dvips  korcuen
korcuen.001:	korcuen.dvi
	rm -f korcuen.[0-9][0-9][0-9]
	dvips -i -S 1  korcuen
korcuen.pbm:	korcuen.ps
	pstopbm korcuen.ps
korcuen.gif:	korcuen.ps
	pstogif korcuen.ps
korcuen.eps:	korcuen.dvi
	dvips -E -f korcuen > korcuen.eps
korcuen-01.g3n:	korcuen.ps
	ps2fax n korcuen.ps
korcuen-01.g3f:	korcuen.ps
	ps2fax f korcuen.ps
korcuen.ps.gz:	korcuen.ps
	gzip < korcuen.ps > korcuen.ps.gz
korcuen.ps.gz.uue:	korcuen.ps.gz
	uuencode korcuen.ps.gz korcuen.ps.gz > korcuen.ps.gz.uue
korcuen.faxsnd:	korcuen-01.g3n
	set -a;FAXRES=n;FILELIST=`echo korcuen-??.g3n`;source faxsnd main
korcuen_tex.ps:	
	cat korcuen.tex | splitlong | coco | m2ps > korcuen_tex.ps
korcuen_tex.ps.gz:	korcuen_tex.ps
	gzip < korcuen_tex.ps > korcuen_tex.ps.gz
korcuen-01.pgm:
	cat korcuen.ps | gs -q -sDEVICE=pgm -sOutputFile=korcuen-%02d.pgm -
korcuen/korcuen.html:	korcuen.dvi
	rm -fR korcuen;latex2html korcuen.tex
xview:	korcuen.dvi
	xdvi -s 8 korcuen &
tview:	korcuen.tty
	browse korcuen.tty 
gview:	korcuen.ps
	ghostview  korcuen.ps &
print:	korcuen.ps
	lpr -s -h korcuen.ps 
sprint:	korcuen.001
	for F in korcuen.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	korcuen.faxsnd
	faxsndjob view korcuen &
fsend:	korcuen.faxsnd
	faxsndjob jobs korcuen
viewgif:	korcuen.gif
	xv korcuen.gif &
viewpbm:	korcuen.pbm
	xv korcuen-??.pbm &
vieweps:	korcuen.eps
	ghostview korcuen.eps &	
clean:	korcuen.ps
	rm -f  korcuen-*.tex korcuen.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} korcuen-??.* korcuen_tex.* korcuen*~
tgz:	clean
	set +f;LSFILES=`cat korcuen.ls???`;FILES=`ls korcuen.* $$LSFILES | sort -u`;tar czvf korcuen.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
