# -*- mode: makefile -*-

invention.en.lstex:
	lstex invention.en | sort -u > invention.en.lstex
invention.en.mk:	invention.en.lstex
	vcat /ul/prg/RC/texmake > invention.en.mk


invention.en.dvi:	invention.en.mk
	rm -f invention.en.lta
	if latex invention.en;then test -f invention.en.lta && latex invention.en;while tail -n 20 invention.en.log | grep -w references && latex invention.en;do eval;done;fi
	if test -r invention.en.idx;then makeindex invention.en && latex invention.en;fi

invention.en.pdf:	invention.en.ps
	if grep -w '\(CJK\|epsfig\)' invention.en.tex;then ps2pdf invention.en.ps;else rm -f invention.en.lta;if pdflatex invention.en;then test -f invention.en.lta && pdflatex invention.en;while tail -n 20 invention.en.log | grep -w references && pdflatex invention.en;do eval;done;fi;fi
	if test -r invention.en.idx;then makeindex invention.en && pdflatex invention.en;fi
invention.en.tty:	invention.en.dvi
	dvi2tty -q invention.en > invention.en.tty
invention.en.ps:	invention.en.dvi	
	dvips  invention.en
invention.en.001:	invention.en.dvi
	rm -f invention.en.[0-9][0-9][0-9]
	dvips -i -S 1  invention.en
invention.en.pbm:	invention.en.ps
	pstopbm invention.en.ps
invention.en.gif:	invention.en.ps
	pstogif invention.en.ps
invention.en.eps:	invention.en.dvi
	dvips -E -f invention.en > invention.en.eps
invention.en-01.g3n:	invention.en.ps
	ps2fax n invention.en.ps
invention.en-01.g3f:	invention.en.ps
	ps2fax f invention.en.ps
invention.en.ps.gz:	invention.en.ps
	gzip < invention.en.ps > invention.en.ps.gz
invention.en.ps.gz.uue:	invention.en.ps.gz
	uuencode invention.en.ps.gz invention.en.ps.gz > invention.en.ps.gz.uue
invention.en.faxsnd:	invention.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo invention.en-??.g3n`;source faxsnd main
invention.en_tex.ps:	
	cat invention.en.tex | splitlong | coco | m2ps > invention.en_tex.ps
invention.en_tex.ps.gz:	invention.en_tex.ps
	gzip < invention.en_tex.ps > invention.en_tex.ps.gz
invention.en-01.pgm:
	cat invention.en.ps | gs -q -sDEVICE=pgm -sOutputFile=invention.en-%02d.pgm -
invention.en/invention.en.html:	invention.en.dvi
	rm -fR invention.en;latex2html invention.en.tex
xview:	invention.en.dvi
	xdvi -s 8 invention.en &
tview:	invention.en.tty
	browse invention.en.tty 
gview:	invention.en.ps
	ghostview  invention.en.ps &
print:	invention.en.ps
	lpr -s -h invention.en.ps 
sprint:	invention.en.001
	for F in invention.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	invention.en.faxsnd
	faxsndjob view invention.en &
fsend:	invention.en.faxsnd
	faxsndjob jobs invention.en
viewgif:	invention.en.gif
	xv invention.en.gif &
viewpbm:	invention.en.pbm
	xv invention.en-??.pbm &
vieweps:	invention.en.eps
	ghostview invention.en.eps &	
clean:	invention.en.ps
	rm -f  invention.en-*.tex invention.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} invention.en-??.* invention.en_tex.* invention.en*~
invention.en.tgz:	clean
	set +f;LSFILES=`cat invention.en.ls???`;FILES=`ls invention.en.* $$LSFILES | sort -u`;tar czvf invention.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
