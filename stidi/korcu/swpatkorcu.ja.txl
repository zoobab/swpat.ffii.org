<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patentjurisprudenz auf Schlitterkurs -- der Preis für die Demontage
des Technikbegriffs

#descr: Bisher gehören Computerprogramme ebenso wie andere %(e:Organisations-
und Rechenregeln) in Europa nicht zu den %(e:patentfähigen
Erfindungen), was nicht ausschließt, dass ein patentierbares
Herstellungsverfahren durch Software gesteuert werden kann. Das
Europäische Patentamt und einige nationale Gerichte haben diese
zunächst klare Regel jedoch immer weiter aufgeweicht.  Dadurch droht
das ganze Patentwesen in einem Morast der Beliebigkeit,
Rechtsunsicherheit und Funktionsuntauglichkeit zu versinken.  Dieser
Artikel gibt eine Einführung in die Thematik und einen Überblick über
die rechtswissenschaftliche Fachliteratur.

#PWc: Wenn Sie ganz eigenständig etwas entwickeln, können Sie niemals
anderer Leute Urheberrecht verletzen.  Sie können sich jedoch sehr
wohl an Patenten stoßen.  Patente sind mitunter weit reichende
%(e:Ausschlussrechte) (Monopole), die im Gegenzug für
%(e:Offenbarungen) von technischem Wissen erteilt werden.  Es besteht
jedoch wenig Anlass zu der Annahme, dass Monopole auf neue Ideen aller
Art dem Gemeinwohl dienen.  Wenn der Eingriff in die %(e:allgemeine
Handlungsfreiheit) (Art 2 GG) überhaupt zu rechtfertigen ist, dann nur
unter allerlei einschränkenden Bedingungen.  Die europäische
Patentjurisprudenz hat zur Eingrenzung der patentfähigen Erfindungen
folgende Unterscheidungen entwickelt:

#kern: Technik vs Geist, Kern vs Umsetzung

#Mes: Welt der Dinge (blau) vs Welt des Geistes (rot)

#fvd: physische Kausalität vs logische Funktionalität

#ksr: konkret vs abstrakt

#htf: leichte Nachahmung aufwendiger Innovation vs aufwendige Nachahmung
leichter Innovation

#TjW: Kern vs Umsetzung

#Aeo: Jede neue Lehre, egal wie abstrakt und immateriell, wird letztlich mit
bekannten Verfahren in etwas physisches umgesetzt.  Es genügt nicht,
dass das Ergebnis als ganzes neu und physisch ist.  Der Kern muss die
Prüfungen bestehen.

#nei: Einstimmende Zitate

#GDW: Freiheit alles Geistigen oder Patentierbarkeit alles Maschinenhaften?

#Ueh: Von der Technizität zur Beliebigkeit

#ohn: Sinnentleerte Begriffe und ihr technischer Beitrag zum Politischen
Engineering in der EU

#Wrk: Literaturübersicht

#rnW: Verfassung der USA

#hpm: Anspruch 1 wie vom Europäischen Patentamt im Jahre 2002 erteilt.

#tte: Viele im Zusammenhang mit Softwarepatenten diskutierten Probleme
entspringen dem Widerspruch zwischen den wesentlichen Eigenschaften
der Software, nämlich Abstraktheit, Allgemeinheit und logische Natur,
und den Anforderungen an Patentansprüche, nämlich Konkretheit,
Besonderheit und physische Substanz.

#Szt: Soweit die Gesetze der Mathematik sich auf die Wirklichkeit beziehen,
sind sie nicht sicher.  Soweit die Gesetze der Mathematik sicher sind,
beziehen sie sich nicht auf die Wirklichkeit.

#WlW: Was gesteuert werden kann, ist nie vollkommen wirklich.  Was wirklich
ist, kann nie vollkommen gesteuert werden.

#bio: angelehnt an %(VN)

#Tsa: Gegenstand der reinen Physik ist die Entfaltung der Gesetze der
verstehbaren Welt; Gegenstand der reinen Mathematik ist die Entfaltung
der Gesetze des menschlichen Verstandes.

#mWo: Informatik (computer science) hat nicht mehr mit Rechenmaschinen zu
tun als Astronomie mit Teleskopen.

#nqs: Die menschliche Verstandestätigkeit gehört jedenfalls nach den
Anschauungen unserer Zeit nicht zu den beherrschbaren Naturkräften.
... Wenn der Bundesgerichtshof das menschliche Denken nicht dem
Begriff der Technik zuordnen will, weil dieser damit seiner
%(q:spezifischen und unterscheidenden Bedeutung) beraubt würde, so ist
das keine willkürliche inhaltliche Begrenzung dieses Begriffs für den
Bereich des Patentrechts, sondern eine konsequente Übernahme der
Anschauungen, die sich in Naturwissenschaften und Technik selbst
entwickelt haben.

#Sai: Stets ist aber die planmäßige Benutzung beherrschbarer Naturkräfte als
unabdingbare Voraussetzung für die Bejahung des technischen Charakters
einer Erfindung bezeichnet worden.  Wie dargelegt, würde die
Einbeziehung menschlicher Verstandeskräfte als solcher in den Kreis
der Naturkräfte, deren Benutzung zur Schaffung einer Neuerung den
technischen Charakter derselben begründen, zur Folge haben, dass
schlechthin allen Ergebnissen menschlicher Gedankentätigkeit, sofern
sie nur eine Anweisung zum planmäßigen Handeln darstellen und kausal
übersehbar sind, technische Bedeutung zugesprochen werden müsste. 
Damit würde aber der Begriff des Technischen praktisch aufgegeben,
würde Leistungen der menschlichen Verstandestätigkeit der Schutz des
Patentrechts eröffnet, deren Wesen und Begrenzung nicht zu erkennen
und übersehen ist.

#Ene: Es verbietet sich demnach, den Schutz von geistigen Leistungen auf dem
Weg über eine Erweiterung der Grenzen des Technischen -- die auf deren
Aufgabe hinauslaufen würde -- zu erlangen.  Es muss vielmehr dabei
verbleiben, dass eine reine Organisations- und Rechenregel, deren
einzige Beziehung zum Reich der Technik in ihrer Benutzbarkeit für den
bestimmungsgemäßen Betrieb einer bekannten Datenverarbeitungsanlage
besteht, keinen Patentschutz verdient.  Ob ihr auf andere Weise, etwa
mit Hilfe des Urheber- oder des Wettbewerbsrechts, Schutz zuteil
werden kann, ist hier nicht zu erörtern.

#B1p: Bundesgerichtshof 1976: Dispositionsprogramm-Beschluss

#Aso: Programme für Datenverarbeitungsanlagen können verschiedene Formen
haben, beispielsweise Algorithmen, Flussdiagramme oder Serien
codierter Befehle, die auf einem Band oder anderen maschinenlesbaren
Aufzeichnungsträgern gespeichert werden können; sie können als
Sonderfall entweder für eine %(e:mathematische Methode) oder eine
%(e:Wiedergabe von Informationen) betrachtet werden.  Wenn der Beitrag
zum bisherigen Stand der Technik lediglich in einem Programm für
Datenverarbeitungsanlagen besteht, ist der Gegenstand nicht
patentierbar, unabhängig davon, in welcher Form er in den Ansprüchen
dargelegt ist.  So wäre z.B. ein Patentanspruch für eine
Datenverarbeitungsanlage, die dadurch gekennzeichnet ist, dass das
besondere Programm in ihr gespeichert ist, oder für ein Verfahren zum
Betrieb einer durch dieses Programm gesteuerten
Datenverarbeitungsanlage in Steuerabhängigkeit von diesem Programm
ebenso zu beanstanden wie ein Patentanspruch für das Programm als
solches oder für das auf Magnettonband aufgenommene Programm.

#IWn: Bei der Prüfung der Frage, ob eine Erfindung vorliegt, muss der Prüfer
%(dots) die Form oder die Art des Patentanspruchs außer acht lassen
und sich auf den Inhalt konzentrieren, um festzustellen, welchen neuen
Beitrag die beanspruchte angebliche %(qc:Erfindung) zum Stand der
Technik leistet.  Stellt dieser Beitrag keine Erfindung dar, so liegt
kein patentierbarer Gegenstand vor.  Dieser Sachverhalt ist durch
[obige] Beispiele anhand verschiedener Wege zur Beanspruchung eines
Programms für eine Datenverarbeitungsanlage erläutert.

#Lkm: Ist Software nun schließlich patentierbar?

#SuW: Zweifellos noch nicht.

#Eee: In Wirklichkeit sind die Gesetzesregeln des Übereinkommens und der
nationalen Gesetze klar:  Sie fordern unmissverständlich die
Nicht-Patentierbarkeit von Software.  Das Spiel, das heute gespielt
wird, besteht darin, in einer oder der anderen Weise diese Regeln zu
verdrehen, z.B. indem man sich, wie oben beschrieben, die Gesamtheit
aus Hardware und Software als eine virtuelle Maschine denkt, die
(künftig ...) patentierbar sein könnte.  Unter dieser Voraussetzung
kann man dann patentrechtlich argumentieren.  Die auf diese Weise auf
dem einen oder anderen Wege erhältlichen Patente haben allerdings nur
denjenigen Wert, den man ihnen beimisst --- oder der sich durch einen
Konsens ergibt, dieser Frage nicht genauer nachgehen zu wollen. 
Tatsächlich kann die Verdrehung der Gesetzesregeln nur insoweit
Wirkung entfalten, wie sich ein Konsens darüber herstellen lässt, ob
man dieses Spiel gegen die bestehenden Gesetzesregeln spielen soll
oder nicht.  Hierbei handelt es sich nicht mehr um eine juristische
Frage im strengen Sinne.

#Aol: Ein bloßer Blick auf die %(ep:Ergebnisse der Softwarepatentierung in
Europa) und die %(ne:wirtschaftswissenschaftlichen Studien ihrer
Wirkungen auf Innovation und Wettbewerb) genügt, um die meisten Leute
davon zu überzeugen, dass hier irgend etwas schwer in Unordnung ist.
Einige versprechen, all dies sei nur eine Übergangserscheinung, die
durch bessere Methoden der Patentrecherche überwunden werden könne. 
Andere weisen darauf hin, dass selbst wenn dieses Jahrzehnte alte
Dauerproblem der Neuheitsprüfung morgen gelöst werden könnte, uns
damit kaum geholfen wäre:  es gab noch nie wirklich brauchbare
%(no:Erfindungshöhe-Standards) und, selbst wenn man sie morgen
etablieren könnte, hätten sie einen schweren Stand gegen die Neigung
der Patentämter, %(q:unseren Kunden einen guten Gegenwert für ihr Geld
zu liefern), wie ein hoher europäischer Patentfunktionär es kürzlich
ausdrückte.

#Ylq: Aber auch hier liegt nicht der Kern des Problems.  Selbst wenn hohe
Standards der Neuheit und Erfindungshöhe zum Funktionieren gebracht
werden könnten, bliebe uns die Frage nicht erspart, wofür Patente
erteilt werden sollen und wie dieser Bereich abzugrenzen ist.  Patente
sind recht grobe Monopolrechte, die wir nicht unbedingt für alle
pfiffigen Neuerungen erteilt sehen wollen.  Oder soll es Patente für
politische Methoden geben?  Geschäftsverfahren?  Argumentationsketten?
 Alles irgendwie geschäftlich verwertbare?  Alles, wofür Patentämter
Prüfer einzustellen bereit sind?  Diese Frage wurde bislang mit dem
Begriff der %(q:technischen Erfindung) beanwortet, d.h. mit der
Forderung, dass eine patentfähige Erfindung uns nicht einfach eine
neue Idee lehren soll sondern eine neue Art, Ideen in Naturkräfte
umzusetzen, also eine physische und nicht eine logische Problemlösung.

#Tor: Vielfältige Gründe sprechen für diese traditionelle Grenzziehung
zwischen logischer Idee und physischer Umsetzung.

#Ebh: Es gibt auf der einen Seite Fragen, die wir an die %(e:Welt der
Dinge), oder, etwas enger, die Welt der %(e:beherrschbaren
Naturkräfte), richten.  Jede neue Antwort auf eine solche Frage wird
relativ schwierig zu finden sein und die Verwendung materieller
Gegenstände sowohl beim Suchen nach der Lösung als auch bei ihrem
Einsatz erfordern.  Sowohl die Erforschung als auch die Umsetzung
solcher %(q:technischer Problemlösungen) erfordert i.d.R. eine
aufwendig regulierte industrielle Organisation.  Es entstehen
materielle Erzeugnisse, deren minimaler Stückpreis (Grenzkosten) über
Null liegt.  Diesem Stückpreis lassen sich Lizenzgebühren hinzufügen. 
Die zusätzliche Reglementierung des Marktgeschehens durch Patente kann
zwar zu Überteuerungen und anderen schmerzhaften Marktstörungen
führen, aber zwischen dem industriellen Grundaufwand und dem
patentbedingten Zusatzaufwand bleibt zumindest eine gewisse
Verhältnismäßigkeit gewahrt.

#Osr: Auf der anderen Seite gibt es Fragen, die wir an die %(e:Welt des
Geistes), d.h. die menschlichen Verstandeskräfte und die von ihnen
aufgebauten axiomatischen Systeme richten.  Die Antwort auf eine
solche Frage liegt in einer neuen %(e:Organisations- und Rechenregel),
die mit rein geistigen Mitteln auf ihren Wahrheitsgehalt zu überprüfen
ist.  Oft gelingt dabei ein 100% zuverlässiger mathematischer
%(q:Beweis), wie es ihn in der Welt der Dinge nicht gibt.

#Tgc: Die Trennung dieser beiden Welten bereitete vor 50 Jahren noch in den
meisten Fällen keine auffälligen Probleme.  Dennoch gab es schon immer
schwierige Fälle, und hochentwickelte Methoden zur Trennung des
Geistigen vom Dinglichen wurden von Patentrechtsgelehrten bereits im
19ten Jahrhundert veröffentlicht und diskutiert.

#Hwn: Allmählich griffen die beiden Welten immer weiter ineinander.  Die
%(q:Maschinenlogik) (software) wurde gegenüber der
%(q:Maschinenphysik) (hardware) immer eigenständiger und gewann
spektakulär an wirtschaftlicher Bedeutung -- ohne dass deshalb der
Fortschritt im Bereich des Physischen unbedeutend geworden wäre.  Dank
der Mittlung durch den %(e:Neumannschen Universalrechner) wurde die
Trennung zwischen Logikalien und Physikalien jedoch klarer, und immer
mehr Probleme konnten auf Logik reduziert werden.

#Ssr: Schon früh wurden deshalb einige Patentanwälte unruhig und meinten: 
Wenn maschinelle Vorgänge patentierbar sind, warum soll es dann für
die Patentierbarkeit etwas ausmachen, ob sie auf analogen oder auf
digitalen Geräten ablaufen?  Soll das Patentwesen etwa in der
digitalen Zukunft an den Rand gedrängt werden?

#Oan: Dem hielten andere entgegen: wenn Organisations- und Rechenregeln
keine technischen Erfindungen sind, warum soll es dann etwas
ausmachen, ob ich sie in meinem Kopf, mit Bleistift und Papier, mit
dem Abakus oder mit dem üblichen Werkzeug der digitalen Zivilisation,
dem Universalrechner ausführe?  Soll in der digitalen Zukunft etwa die
Freiheit des Geistes an den Rand gedrängt werden?

#Arr: Bei genauerer Betrachtung muss man dem zweiten Argument recht geben. 
Denn die ökonomische Begründung für die Begrenzung des Patentwesens
greift auch hier:  abstrakte Ideen (Algorithmen) werden, egal ob sie
auf technische Vorgänge abbildbar sind, ohne Experimentierkosten
geschaffen, können auf eine unbegrenzte Menge von Problemen angewandt
werden und lassen sich dank der Informatik noch schneller und billiger
verbreiten als menschliche Gedanken:  die Grenzkosten des
Informationsproduktes liegen bei Null, und das Verhältnis zwischen
Patentgebühren und den Grundkosten, auf die sie draufzuschlagen wären,
stellt eine Division durch Null dar.  Ferner wird der im Patent
implizierte Vertrag %(q:Monopol auf die Umsetzung gegen Offenbarung
der Idee) ad absurdum geführt:  da zwischen der Information und ihrer
Umsetzung keine Erfindung liegt, droht jede sachgerechte Offenbarung
der Idee in turing-kompletter Syntax zu einer Verletzungshandlung zu
werden.

#TWb: Diese Probleme wurden in den 60er und 70er Jahren unter
Patentrechtsgelehrten, insbesondere in Deutschland, intensiv
diskutiert.  Es wurde in Patentrechtslehrbüchern erläutert und von
führenden deutschen Gerichten in der Anwendung verfeinert.  Selbst
mögliche künftige Irrwege und deren Konsequenzen wurden von
Rechtsprechung und Schrifttum prophetisch warnend aufgezeigt.  Der
einzige ersichtliche Grund dafür, dass diese Irrwege dann dennoch vom
EPA und BGH beschritten wurden, liegt wohl in der Schwäche
(Unwissenheit und Gier) des Adam.  Patentjuristen sind im Schnitt, was
diese Probleme anbetrifft, ziemlich unbedarft.  Sie glauben gerne an
populäre Irrtümer und kolportieren solche Irrtümer dankbar in
Fachzeitschriften.  Echtes Wissen über den Technikbegriff und ähnliche
interdisziplinäre Randbereiche bringt kein Geld in die Kasse, weder
die der Patentanwaltskanzlei noch die des Europäischen Patentamtes,
welches sich durch Patentgebühren finanziert.

#Bns: Nach geltendem europäischem Recht sind Programme für
Datenverarbeitungsanlagen keine Erfindungen im Sinne des Patentrechts,
was nicht ausschließt, dass eine patentfähiges technisches Verfahren
auch programmgesteuert ablaufen kann.  Dieses Verfahren muss jedoch
mehr beinhalten als eine Rechenanweisung für herkömmliche
Universalrechner.   Die Erfindung muss außerhalb des Rechners, an
seiner Peripherie, etwa beim ABS-Verfahren im Einwirken von
Bremsrkäften auf Autoreifen, liegen.  Die Erfindung %(q:steht und
fällt mit dem Einsatz beherrschbarer Naturkräfte).  Die Naturkräfte
müssen ein Teil der erfinderischen Leistung sein, die mit dem Monopol
belohnt wird.  Es muss eine Lehre über Wirkungszusammenhänge von
Naturkräften vorliegen.  Auf bloßer Logik beruhende Problemlösungen
gelten nicht als %(e:technische Erfindungen) sondern als
%(q:Organisations- und Rechenregeln).  Ob solche Rechenregeln
letztlich auch mithilfe eines Energie verbrennenden Prozessors
ausgeführt werden, ist dabei unerheblich:  %(q:die Lösung des Problems
ist bereits vollendet, bevor im Zuge seiner Umsetzung auf einer
herkömmlichen Datenverarbeitungsanlage das Feld der Technik betreten
wird).  In seinem wegweisenden Urteil %(dp:Dispositionsprogramm) von
1976 stellt der Bundesgerichtshof fest, dass eine Ausdehnung des
Patentwesens in den Bereich der Funktionslogik grundlegende
Freiheitsrechte bedrohen würde und gleichzeitig dem Fortschritt der
davon betroffenen Disziplinen nicht unbedingt förderlich wäre.  Alte
und neue Erkenntnisse bestärken diese weise und klare Grenzziehung des
Gesetzgebers und ihre systematische Auslegung durch die
höchstrichterlichen Grundsatzentscheidungen seit 1976, die letztlich
in den heute noch gültigen Prüfungsrichtlinien und Gesetzeskommentaren
und Entscheidungen der meisten Gerichtbeschlüsse bis heute ihren
Niederschlag findet.  Dabei wird regelmäßig der
Dispositionsprogramm-Beschluss zitiert, sei es als Vorbild oder als
Zeichen eines schlechten Gewissens.

#GWz: Gleichzeitig gibt es seit den 60er Jahren eine zunehmend
einflussreiche Gruppe von Patentjuristen, die eine grundsätzliche
Beschränkung des Patentwesens auf die %(q:Welt der Dinge) mit dem
Hinweis ablehnen, dadurch müsse das Patentwesen den Anschluss an
zunehmend bedeutende Bereiche des Wirtschaftslebens verlieren.  Unter
dem Einfluss dieser Denkweise verfielen die Patentierbarkeitsstandards
Stück um Stück.  Zunächst wurde in den frühen 80er Jahren industrielle
Steuerungslogik patentiert, und die ehemals verpönten
Funktionsansprüche (d.h. Beanspruchung abstrakter Funktionalitäten
unabhängig von einer naturkräftegebundenen Implementationsweise)
wurden akzeptabel.  Allmählich schien die vom Gesetz geforderte
%(q:Diskriminierung gegen Computerprogramme) nicht mehr zeitgemäß zu
sein.  Patentabteilungen von Großfirmen wie Siemens und IBM
investierten viel Geld und Zeit, um die zunächst %(q:gegenüber den
Neuen Technologien unaufgeschlossene) Denkweise der Patentämter durch
Grundsatzurteile des EPA/BGH in Bewegung zu bringen.  Mit der
Funktionslogik wurden zunehmend alle wirtschaftlich interessanten
Vorgänge unseres Lebens patentierbar.  Bereits in den frühen 90er
Jahren war am EPA die gesamte Liste der Patentierbarkeitsausschlüsse
in Art 52 EPÜ, von der Mathematik bis zu den %(q:Verfahren für
geschäftliche Tätigkeit) und der %(q:Wiedergabe von Information),
praktisch zur Makulatur geworden.  Für die Rechtsprechungspraxis des
Europäischen Patentamtes war es seitdem belanglos, ob ein begehrter
Patentierungsgegenstand auf dieser Liste steht oder nicht.  Es kann
allerdings passieren, dass Anspruch auf ein
%(q:Pensionsberechnungssystem) o.ä. vor dem EPA scheitert, weil der
Antragsteller vergessen hat, rechtzeitig zum Prioritätsdatum genügend
%(q:technische Effekte) zu offenbaren.  Was patentierbar ist, ist im
wesentlichen eine Frage der Sorgfalt bei der Vorbereitung einer
Patentschrift, wie nicht nur ein bekannter Patentanwalt richtig
bemerkte.

#Uua: Bis Mitte der 90er Jahre scheute man allerdings noch vor der letzten
Konsequenz zurück.  Organisations- und Rechenregeln wurden nur
indirekt patentiert, sprachlich eingekleidet als %(q:technische)
Prozesse.  Wer ein Programm auf den Markt brachte, konnte damit immer
noch keinen Patentanspruch verletzen.  Nach intensivem jahrelangen
Trommelfeuer durch patentjuristische Fachzeitschriften schien 1998 der
Zeitpunkt gekommen, die letzte Hemmschwelle zu überwinden.  Durch eine
%(et:Entscheidung) einer (in Rechtsfragen nicht zuständigen)
technischen Beschwerdekammer und Folgeentscheidungen ließ das Amt
verlautbaren, dass künftig wörtliche Ansprüche auf
Informationsgegenstände (%(q:Computerprogrammprodukt),
Computerprogramm, Datenstruktur etc) direkt beansprucht werden können.
 Per %(q:Anmerkung des Herausgebers) stellte die Amtsleitung klar,
dass sie beabsichtige %(q:künftig die Praxis des Amtes an dieser
Entscheidung auszurichten) und die Prüfungsrichtlinien entsprechend
umzuschreiben.

#hii: Meanwhile, the caselaw of the EPO has indeed been pushing the
boundaries of what is technical ever wider.  According to its
Examination Guidelines of 2002 %(q:A further technical effect which
lends technical character to a computer program may be found e.g. in
the control of an industrial process or in processing data which
represent physical entities or in the internal functioning of the
computer itself or its interfaces under the influence of the program
and could, for example, affect the efficiency or security of a
process, the management of computer resources required or the rate of
data transfer in a communication link.)

#iir: According to rulings on EP 0689133, even the %(q:economical use of the
resource area on the screen) is %(q:technical).

#bto: And if that doesn't cover the program, then %(q:processing which is
based on considerations of how a computer works is technical).  Thus
in the Sohei case a patent the EPO Board of Appeal upheld a patent for
using the same input form to update two databases, namely an inventory
database and a billing database, because it %(e:implied) (but didn't
actually limit itself by specifying how to do it) the handling of
files containing different types of information, which is
%(e:technical).

#Dst: Allerdings sind die auf dieser Grundlage gewährten Patente von
ungewissem Wert.  Gesetzestreue Gerichte (wie z.B. der Schwedische
Oberste Gerichtshof und der %(bp:17. Senat des Bundespatentgerichtes))
finden regelmäßig Widersprüche in der Argumentation des EPA und des
BGH und weisen Ansprüche auf logische Funktionalitäten und
informationelle Gegenstände zurück.  Auch bei den nationalen Gerichten
in anderen Ländern gibt es solche Reibungen oder Unabwägbarkeiten. 
Daher ist der %(pm:Patentbewegung) sehr daran gelegen, das Europäische
Patentübereinkommen zu ändern oder mithilfe neuer Gesetzgebung
endgültig unwirksam zu machen.  Ein entsprechendes Vorhaben galt seit
ca 1997 als beschlossene Sache, scheiterte aber vorerst im November
2000 am öffentlichen Widerstand.  %(gl:Dennoch hält das EPA an seiner
1998 voreilig beschlossenen Rechtsänderung fest) und setzt damit die
Regierungen und die Europäische Kommission unter Druck.  Derzeit steht
eine Europäische %(q:Richtlinie über die Patentierbarkeit von
computer-implementierbaren Erfindungen) an, bei der es offenbar darum
geht, die %(pk:gegenwärtige Praxis des EPA, Patente auf
computer-implementierte Organisations- und Rechenregeln und
informationelle Gegenstände zu erteilen), zu legalisieren.  Unter dem
Namen der %(q:Harmonisierung) und %(q:Beseitigung von
Rechtsunsicherheiten) soll die wackelige Grundlage, auf der sich die
Rechtsprechung des EPA derzeit bewegt, stabilisiert werden.  Bisherige
Entwürfe der EU-Patentjuristen zeigen jedoch, dass dadurch bestenfalls
die Widersprüche zwischen Rechtsprechung und Gesetz, nicht jedoch die
inneren Widersprüche der EPA-Judikatur aufgehoben würden.  Wenn die
Patentbewegung von %(q:Klärung der verworrenen Rechtslage) spricht,
meint sie damit %(e:Ersetzung der klaren Gesetzesregeln des EPÜ durch
die verworrene Praxis des EPA), m.a.W. %(e:grenzenlose
Patentierbarkeit amerikanischen Stils).  Da das EPA seine Praxis
jedoch aus politischen Gründen weiterhin mit einem rechtsdogmatischen
Schleier verhüllen muss, werden wohl auch in den kommenden Jahren vor
allem unbeeindruckte amerikanische und japanische Unternehmen den
Großteil derjenigen Patente erhalten, die das EPA theoretisch nicht
erteilt.

#iei: Gegen Ende der 90er Jahre war der Begriffsapparat der %(q:Technischen
Erfindung) beim EPA noch in Gebrauch.  Er diente jedoch nur noch
selten dazu, Erfindungen von nicht-patentfähigen Neuerungen zu
unterscheiden.  Meistens diente es dazu, die Öffentlichkeit zu
täuschen.  Es musste der Eindruck erweckt werden, dass das EPA das
tat, was das Gesetz und die Politiker von ihm forderten, während genau
das Gegenteil statt fand.   Es entwickelte sich eine komplexe
doppelbödige Sprache um Begriffe wie %(q:technisches Problem),
%(q:technischer Effekt), %(q:technische Überlegungen),
%(q:computer-implementierte Erfindungen) etc.  Zum Verständnis dieses
Doppelsprechs erfordert eine soziolinguistische Analyse, die den
Rahmen dieses Artikels sprengen würde.

#ahj: The author of this treatise is a  UK-educated barrister at Honkong
University, specialised in computing.  Lee analyses the history of
major shifts in EPO practise.  Lee starts by exaggerating the
restrictive character of the EPO's first examination guidelines of
1978: %(bc:The implication of this approach would be to severely
narrow the scope of patentability for software-related inventions.
Inventive process control %(tp|mechanisms|e.g. those used in a
conventional chemical plant to produce new polymers) that would
otherwise be standard patent material would fall outside the scope of
patentable subject-matter simply because a program was used in
implementing the inventive process control scheme.)  This, according
to Lee, apparently did not disturb the chemical industry as much as
certain other customers of the EPO:  %(bc:However, in response to
pressure from the computer industry and trends emerging in the US, the
European Patent Office reviewed its guidelines in 1985 ...).  Lee then
explains the ensuing drift toward wider patentability in terms of
landmark cases at the Technical Board of Appeal and the UK courts from
1986 until the early 90s.  The text offers a fairly good overview over
these cases.  Lee concludes with general observations about the
weaknesses of the EPO's reasoning such as the lack of definitions of
what is %(q:technical).  Lee proposes to replace this word with
%(q:physical).

#itp: Prof. Dr. Ulrich Löwenheim gives a detailed overview of the
applicability of copyright, patent and competition law to software in
Germany at the end of the 1980s.  Reports about efforts at the EPO to
extend patentability to software and predicts that there will be a
further drift in the coming years.  Concludes that coypright is the
most appropriate system for software, because it allows software
creators to reap fruits from their work and keeps ideas free for reuse
by legitimate competition.

#chd: A Polish patent attorney points out that the Commission's directive
proposal uses the term %(q:technical) in a way that raises more
questions than it answers, thereby creating legal insecurity.  The
paper also gives details about the developments in Poland, explaining
that a recently repealed Polish law has insisted that inventions must
be directed to physical devices and that this has lead the Polish
Patent Office to be stricter than others in rejecting function claims,
i.e. claims to an indefinite %(q:means for doing something).  Yet even
at the PLPTO people have found ways to successfully circumvent these
restrictions in many cases.  The german approach to %(q:technical
invention) has been the most well-reasoned, but it no longer provides
a meaningful guidance because information has become a force of
nature, Laszewski somewhat carelessly asserts, relying on Tauchert
statements distributed in the name of the german patent office as his
source.

#Nvs: Nach Untersuchung zahlreicher richtungsweisender EPA-Urteile über die
Patentierbarkeit von Programmlogik erklärt Winischhofer, dass diese
Urteile höchst fragwürdig und widersprüchlich sind.  Es bestehen nicht
nur widersprüche zum Gesetz sondern auch Widersprüche zwischen den
verschiedenen Urteilen.  Winischhofer fasst zusammen:

#DdW: Das EPA selbst hat bisher keinerlei Systematik entwickelt.  Selbst die
ausführlich diskutierte Entscheidung %(q:Computerprogrammprodukt/IBM)
greift auf verschiedene Einzelfälle zurück. Die Judikatur des EPA
erscheint sohin von Kasuistik geprägt, eine Definition des
erforderlichen %(q:technischen Effektes) bleibt selbst die zuletzt
genannte Entscheidung schuldig - dies obwohl das EPA, wie bereits
erwähnt, gedenkt seine Rechtsprechung künftig an dieser Entscheidung
auszurichten.

#Enn: Es bedürfte nur weniger Worte, um begriffsstutzigen Patentjuristen zu
erklären, wie Art 52 EPÜ / §1 PatG zu verstehen ist.  Dieser
Eurolinux-Entwurf einer EU-Richtlinie hat bequem auf einer
DIN-A4-Seite Platz.  Die Vertreter der %(ep:Patentbewegung) hingegen
reden in langen Traktaten von %(q:Klärung) und %(q:Harmonisierung) und
erzeugen dabei mit jeder Zeile ein Stück mehr Begriffsverwirrung und
dissonanzträchtige Unschärfe.

#Dla: Der Patentchef von Siemens erklärt, wie seine Abteilung aktiv zur
%(q:Rechtsfortbildung) beigetragen hat, um die Praxis des deutschen
Patentamtes dem Stand der (im globalen Geschäftsumfeld maßgeblichen)
amerikanischen Rechtsprechung anzupassen.

#Dae: Der Softwarepatent-Experte des BGH erklärt am Beispiel Microsoft,
warum Softwarepatente wirtschaftspolitisch erwünscht sein müssen, und
zeigt Wege auf, wie man sie trotz Gesetzeslage einführen kann.  Er
führt aus, dass der Gesetzgeber nur untechnische Gegenstände vom
Patentschutz habe ausschließen wollen, und dass Computerprogramme
technisch seien.  Diesen Widerspruch löst Melullis, indem er eine
Entität namens %(q:Computerprogramm als solches) konstruiert und ihr
die Bedeutung %(q:Computerprogramm im menschlichen Hirn) zuweist. 
Kollege Tauchert vom Deutschen Patentamt hingegen plädiert für
%(q:Sourcecode).  Egal, Hauptsache die Worte des Gesetzgebers werden
dadurch belanglos.

#TWa: Dieser Artikel zeigt allerlei rechtliche und praktische
Widersprüchlichkeiten der Softwarepatentierung auf.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatkorcu ;
# txtlang: ja ;
# multlin: t ;
# End: ;

