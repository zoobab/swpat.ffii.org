korcufr.lstex:
	lstex korcufr | sort -u > korcufr.lstex
korcufr.mk:	korcufr.lstex
	vcat /ul/prg/RC/texmake > korcufr.mk


korcufr.dvi:	korcufr.mk korcufr.tex
	latex korcufr && while tail -n 20 korcufr.log | grep references && latex korcufr;do eval;done
	if test -r korcufr.idx;then makeindex korcufr && latex korcufr;fi
korcufr.pdf:	korcufr.mk korcufr.tex
	pdflatex korcufr && while tail -n 20 korcufr.log | grep references && pdflatex korcufr;do eval;done
	if test -r korcufr.idx;then makeindex korcufr && pdflatex korcufr;fi
korcufr.tty:	korcufr.dvi
	dvi2tty -q korcufr > korcufr.tty
korcufr.ps:	korcufr.dvi	
	dvips  korcufr
korcufr.001:	korcufr.dvi
	rm -f korcufr.[0-9][0-9][0-9]
	dvips -i -S 1  korcufr
korcufr.pbm:	korcufr.ps
	pstopbm korcufr.ps
korcufr.gif:	korcufr.ps
	pstogif korcufr.ps
korcufr.eps:	korcufr.dvi
	dvips -E -f korcufr > korcufr.eps
korcufr-01.g3n:	korcufr.ps
	ps2fax n korcufr.ps
korcufr-01.g3f:	korcufr.ps
	ps2fax f korcufr.ps
korcufr.ps.gz:	korcufr.ps
	gzip < korcufr.ps > korcufr.ps.gz
korcufr.ps.gz.uue:	korcufr.ps.gz
	uuencode korcufr.ps.gz korcufr.ps.gz > korcufr.ps.gz.uue
korcufr.faxsnd:	korcufr-01.g3n
	set -a;FAXRES=n;FILELIST=`echo korcufr-??.g3n`;source faxsnd main
korcufr_tex.ps:	
	cat korcufr.tex | splitlong | coco | m2ps > korcufr_tex.ps
korcufr_tex.ps.gz:	korcufr_tex.ps
	gzip < korcufr_tex.ps > korcufr_tex.ps.gz
korcufr-01.pgm:
	cat korcufr.ps | gs -q -sDEVICE=pgm -sOutputFile=korcufr-%02d.pgm -
korcufr/korcufr.html:	korcufr.dvi
	rm -fR korcufr;latex2html korcufr.tex
xview:	korcufr.dvi
	xdvi -s 8 korcufr &
tview:	korcufr.tty
	browse korcufr.tty 
gview:	korcufr.ps
	ghostview  korcufr.ps &
print:	korcufr.ps
	lpr -s -h korcufr.ps 
sprint:	korcufr.001
	for F in korcufr.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	korcufr.faxsnd
	faxsndjob view korcufr &
fsend:	korcufr.faxsnd
	faxsndjob jobs korcufr
viewgif:	korcufr.gif
	xv korcufr.gif &
viewpbm:	korcufr.pbm
	xv korcufr-??.pbm &
vieweps:	korcufr.eps
	ghostview korcufr.eps &	
clean:	korcufr.ps
	rm -f  korcufr-*.tex korcufr.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} korcufr-??.* korcufr_tex.* korcufr*~
tgz:	clean
	set +f;LSFILES=`cat korcufr.ls???`;FILES=`ls korcufr.* $$LSFILES | sort -u`;tar czvf korcufr.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
