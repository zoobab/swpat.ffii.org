<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patent Jurisprudence on a Slippery Slope

#descr: So far computer programs and other %(e:rules of organisation and
calculation) are not %(e:patentable inventions) according to European
law.  This doesn't mean that a patentable manufacturing process may
not be controlled by software.  However the European Patent Office and
some national courts have gradually blurred the formerly sharp
boundary between material and immaterial innovation, thus risking to
break the whole system and plunge it into a quagmire of arbitrariness,
legal insecurity and dysfunctionality.  This article offers an
introduction and an overview of relevant research literature.

#PWc: If you develop something completely on your own, you can be sure that
you are not violating anyone's copyright.  However you may run afoul
of patents.  Patents are broad exclusion rights (monopolies) granted
by the state in return for a disclosure of knowhow.  Patent rights
tend to be incompatible with the constitutional %(e:freedom
principle), on which democracy and market economy are built.  There is
little reason to assume that granting monopolies for all kinds of new
ideas will be beneficial for society as a whole.  Thus, if patents are
to be acceptable at all, the conditions for granting and enforcing
them must be carefully designed.  European patent jurisprudence has
delimited the %(e:patentable invention) by means of the following two
distinctions:

#kern: Matter vs Mind, Core vs ImplemenEmbodiment

#Mes: Matter (blue) vs Mind (red)

#fvd: physical causality vs logical functionality

#ksr: concrete vs abstract

#htf: easy imitation of difficult innovation vs difficult imitation of easy
innovation

#TjW: core vs implementation

#Aeo: Any new teaching, no matter how abstract, is ultimately materialised
by known methods.  It is not enough if the whole is new and physical. 
The core must pass the tests.

#nei: Introductory Quotes

#GDW: Freedom of everything intellectual or patentability of everything
machine-like?

#Ueh: From Technicity to Arbitrariness

#ohn: Voided Terminology and its Technical Contribution to Legislative
Engineering in the EU

#Wrk: Literature Overview

#rnW: Constitution of the USA

#hpm: Claim 1 as granted by the European Patent Office to Open Markets Inc
(US) in 2002

#tte: Many issues debated in relation to software patents have their origin
in the inconsistency between the innate properties of software, i.e.
abstractness, generality and logical nature, and the required
properties for patent claims, i.e. concreteness, specificity and
physical substance.

#Szt: As far as the laws of mathematics refer to reality, they are not
certain.  As far as the laws of mathematics are certain, they do not
refer to reality.

#WlW: What can be controlled is never completely real.  What is real can
never be completely controlled

#bio: borrowing from %(VN)

#Tsa: The object of pure physics is the unfolding of the laws of the
intelligible world; the object of pure mathematics that of unfolding
the laws of human intelligence.

#mWo: Computer science is no more about computers than astronomy is about
telescopes.

#nqs: Human intellectual activity is not, according to the views of our
time, among the controllable forces of nature.  ... The Federal Court
of Justice (BGH) refuses to subsume human thinking under the concept
of %(q:technology), because that would deprive the concept of
%(q:technology) of its %(q:specific and distinctive meaning).  By
insisting on this point, the BGH is not introducing an arbitrary
definition of %(q:technology) for the sake of patent law but rather
consistently adopting views which have been developped by the natural
sciences and technologies themselves.

#Sai: However in all cases the plan-conformant utilisation of controllable
natural forces has been named as an essential precondition for
asserting the technical character of an invention.  As shown above,
the inclusion of human mental forces as such into the realm of the
natural forces, on whose utilisation in creating an innovation the
technical character of that innovation is founded, would lead to the
consequence that virtually all results of human mental activity, as
far as they constitute an instruction for plan-conformant action and
are causally overseeable, would have to be attributed a technical
meaning.  In doing so, we would however de facto give up the concept
of the technical invention and extend the patent system to a vast
field of achievements of the human mind whose essence and limits can
neither be recognized nor overseen.

#Ene: Any attempt to attain the protection of mental achievements by means
of extending the limits of the technical invention -- and thereby in
fact giving up this concept -- leads onto a forbidden path.  We must
therefore insist that a pure rule of organisation and calculation,
whose sole relation to the realm of technology consists in its
usability for the normal operation of a known computer, does not
deserve patent protection.  Whether it can be awarded protection under
some other regime, e.g. copyright or competition law, is outside the
scope of our discussion.

#B1p: Federal Court of Justice 1976: %(q:Disposition Program) Decision

#Aso: A computer program may take various forms, e.g. an algorithm, a
flow-chart or a series of coded instructions which can be recorded on
a tape or other machine-readable record-medium, and can be regarded as
a particular case of either a %(e:mathematical method) or a
%(e:presentation or information).  If the contribution to the known
art resides solely in a computer program then the subject matter is
not patentable in whatever manner it may be presented in the claims. 
For example, a claim to a computer characterised by having the
particular program stored in its memory or to a process for operating
a computer under control of the program would be as objectionable as a
claim to the program %(e:per se) or the program when recorded on
magnetic tape.

#IWn: In considering whether an invention is present, [the examiner] should
disregard the form or kind of claim and concentrate on the content in
order to identify the novel contribution which the alleged
%(qc:invention) claimed makes to the known art.  If this contribution
does not constitute an invention, there is not patentable subject
matter.  This point is illustrated by the examples %(dots) of
different ways of claiming a computer program.

#Lkm: Is software now finally patentable?

#SuW: Without doubt not yet.

#Eee: In reality, the national and conventional rules are clear:  they
stipulate without ambiguity a principle of non-patentability of
software.  The game which is being played today consists in twisting
these rules one way or another, e.g. by imagining to consider, as we
have seen, the totality of software and hardware as a virtual machine
which is potentially patentable (tomorrow ...).  From that point on
one can speak about software in patent language.  The patents which
may be obtained this way, by this channel or by another, however still
do not have any value beyond what we lend to them - but of course it
is possible that they will finally acquire a value simply through an
informal consensus to stop discussing the question.  In fact, the
efficiency of this twisting of rules of law is largely dependent on
whether this consensus evolves to take for granted -- against the
rules of written law -- that we will play this game or not.  This
question is no longer a legal question in the strict sense of the
term.

#Aol: A simple look at the %(ep:results of patenting software in Europe) and
the %(ne:economic studies of their effects on innovation and
competition) is enough to make most people realise that something is
going utterly wrong here.  Now some people will claim that all this is
just a problem of improving prior art search capabilities of patent
offices.  Others point out that, even if the decade-old novelty
examination problem could be solved tomorrow, this would not help
much:  meaningful %(no:non-obviousness standards) can hardly be
formulated and would, even if they were found, be difficult to uphold
against the normal tendencies of patent offices to %(q:deliver good
value for the money of our customers), as a high european patent
official recently put it.

#Ylq: Yet, even if reliable standards of novelty and non-obviousness could
be defined and consistently upheld in practice, that would not be
enough.  We will not get around the question what should be patentable
and what not.  The patent system needs a limitation of statutory
subject matter.  Should new schemes of social engineering be
patentable?  Musical ideas?  Ways of doing business?  Argumentation
chains?  Anything useful?  Anything practical?  Anything economically
exploitable?  Anything for which patent offices decide to hire
examiners?  Where exactly are the limits?   Traditionally this
question has been answered by the concept of %(q:technical invention),
i.e. the requirement that an invention should not teach us a new idea
but rather a new way of putting an idea to work by using controllable
forces of nature.

#Tor: There are many reasons for this traditional line-drawing between
between logical ideas and physical implementations.

#Ebh: We have on one side questions asked to the real world, or, more
narrowly, the world of controllable natural forces.  Any new solution
that is an answer to such a question will be fairly difficult to find,
and it will entail the use of material objects both for
experimentation and for utilisation, so that there will be an overhead
cost to which patent fees can be added without much of a hassle.

#Osr: On the other side we have questions asked to human reason, where the
answer is a new rule of organisation or calculation, verifiable solely
by logical deduction.

#Tgc: The separation between these two worlds was usually fairly
straightforward 50 years ago and didn't create spectacular problems. 
Yet difficult borderline cases have always existed, and sophisticated
reasoning for separating the mental from the material was already
published and discussed by patent law experts in the 19th century.

#Hwn: However, gradually the two worlds, also called %(q:world of matter)
and %(q:world of mind) by German patent law philosophy, have come
closer together, and the most common mediating element between them is
the universal computer.

#Ssr: So on one side there are people who say:  if physical processes are
patentable, why should it make any difference whether they run on
special hardware or on general-purpose hardware?

#Oan: On the other there are people who say: if rules or organisation and
calculation are not patentable, why should it make any difference
whether I run them in my head, with pencil and paper or with the
normal tool of today's civilisation, which is the universal computer?

#Arr: And in fact the second argument is right, because the economic
rationale behind not granting patents on thoughts applies also here: 
abstract ideas are, regardless of their applicability to technical
problems, produced without experimentation cost, applicable to an
infinite range of problems, and replicated at zero cost with no
overhead to which patent license fees could be added.  The division of
the extra cost of patents by the marginal cost (and long-term ideal
price) of information goods is a division by zero.  Moreover the deal
between the inventor and the public, characterised as %(q:monopoly on
commercial implementation in return for disclosure of idea), is led ad
absurdum: since between the idea and the application there is no
invention, any adequate disclosure of the idea in turing-complete
syntax risks to become an act of patent infringement.

#TWb: These problems were intensely debated in the 60s and 70s among patent
law scholars, especially in Germany.  It has been explained in patent
law text books and worked out by leading German law courts.  Even
possible future aberrations and their consequences were predicted by
some of those texts.  The only reason that the aberrations nonetheless
occurred lies in the well-known weakness (ignorance and greed) of man.
 Patent lawyers are on the average quite ignorant about these
questions, because being knowledgable about them does not bring any
extra revenues into the law office.  The same applies to patent
offices such as the EPO, who exercise control over patent jurisdiction
and at the same time finance themselves by revenues from the patents
they grant.

#Bns: According to the European Patent Convention, computer programs are not
patentable inventions.  This does not exclude that a patentable
technical process may run under program control.  However this process
must be more than a rule (program) for using conventional computing
equipment:  it could reside in computer periphery which applies brake
forces to tyres, and it should teach some new knowledge about causal
relations in the use of forces of nature.  The realm of pure reason,
i.e. calculation, abstraction and programmation, must, according to
the will of the legislators, remain free from patent claims.  In its
landmark decisions %(dp:Dispositionsprogramm) of 1976, the German
Federal Court (BGH) discussed this subject extensively and confirmed
the dichotomy between the %(q:realm of reason) and the %(q:realm of
natural forces), saying that the patent system must stay confined to
the latter and lawcourts do not have the right to extend it into the
former, since this would be against the will of the legislator and
could pose a serious danger to freedom and basic interests of society
while not necessarily helping to promote the progress of mathematics
or any other of the non-patentable realms of reasoning.   The wisdom
of the doctrine of %(q:technical invention), as formulated in the
Dispositionsprogramm decision and other decisions in Germany and other
countries, has been corroborated by old and new studies in the
economics of technological innovation. It still serves as an
authoritative source for Patent Law commentaries such as the standard
work of Dr. Georg Benkard, who explained this concept of technical
character in his commentary of 1988 and even in later editions and in
many treatises and court decisions about the limits of patentability
until this day, be it as a model or as a sign of bad conscience.

#GWz: At the same time, since the 1960s there has been an increasingly
influential group of patent lawyers who were not willing to accept a
limitation of the patent systems to the %(q:world of matter), arguing
that this would keep patents out of some of the most interesting
spheres of future economic development.  Under their influence, the
erosion of patentability standards proceded at a quick pace.  Control
logic in the area of microelectronics was patented, and abstract
claims to functionality gradually became acceptable, making it
inconsistent to uphold the %(q:discrimination against computer
programs).  The patent departments of Siemens and IBM made special
contributions to the %(q:evolution of caselaw), investing great
efforts to bring about several landmark decisions by the EPO and the
German Federal Court.  Already by the early 90s, all items in the
exclusion list of Art 52 EPC, including mathematical methods, computer
programs, organisational methods and methods of presenting
information, became de facto patentable, and the exclusion list was
interpreted to apply only to meaningless %(q:as such) entities.   This
means that on the one hand anything, even chemicals or pharmaceutics,
could be put on the exclusion list without influencing the
patentability of such items, and on the other hand there is no more
any limit to the expansion of the patent system into the whole range
of logical innovation, from software to music to social engineering or
political schemes or business models.  However these have to be
claimed using the terminology of %(q:information technology).  Thus it
can happen that a carelessly worded %(q:pension benefit system) claim
will fail before the EPO, because the applicant forgot to disclose
enough %(q:technical effects) at the application date.  What is
patentable becomes mainly a question of claim wording, as one leading
expert attorney correctly remarked.

#Uua: Until the mid 90s, the EPO was however still shying away from the
utmost consequence of this adventure.  Rules of organisation and
calculation were patented only indirectly, more or less carefully
dressed up as %(q:technical) processes.  Whoever distributed a program
on the market could still not infringe on any patent claim.  The
public at large therefore did not notice what was happening.  
Meanwhile the patent community gradually became more bold.  In 1998
the EPO's technical board of appeal (not competent in questions of
law) %(et:defined) that a %(q:computer program with a technical
effect) is not a %(q:computer program as such) and started granting
patent claims to informational entities (%(q:computer program
product), computer program, data structure).  By an %(q:editor's
remark) under the document the EPO's president made it clear that this
was the new policy of the Office.

#hii: Meanwhile, the caselaw of the EPO has indeed been pushing the
boundaries of what is technical ever wider.  According to its
Examination Guidelines of 2002 %(q:A further technical effect which
lends technical character to a computer program may be found e.g. in
the control of an industrial process or in processing data which
represent physical entities or in the internal functioning of the
computer itself or its interfaces under the influence of the program
and could, for example, affect the efficiency or security of a
process, the management of computer resources required or the rate of
data transfer in a communication link.)

#iir: According to rulings on EP 0689133, even the %(q:economical use of the
resource area on the screen) is %(q:technical).

#bto: And if that doesn't cover the program, then %(q:processing which is
based on considerations of how a computer works is technical).  Thus
in the Sohei case a patent the EPO Board of Appeal upheld a patent for
using the same input form to update two databases, namely an inventory
database and a billing database, because it %(e:implied) (but didn't
actually limit itself by specifying how to do it) the handling of
files containing different types of information, which is
%(e:technical).

#Dst: However the patents granted on this basis are of an uncertain value. 
Independent lawcourts (such as the Swedish Highest Court as well as
the %(bp:17th senate of the German Federal Patent Court)) regularly
find contradictions in the reasoning of EPO and BGH and reject claims
to computer programs or logical ideas.  Other European lawcourts can
be expected to point out these contradictions any time, as there is
not very much authoritative case law e.g. in infringement procedings. 
Thus the patent community is very keen to change the existing laws so
as to adapt them to the new practice of the EPO.  In fact, at the time
of the introduction of this practice, they had already assured
themselves of the European Commission's support for a planned change
of law.  This failed however, at least preliminarily, in November
2000, due to %(ep:vocal opposition from software creators) and lack of
support from European governments.  Nevertheless, the EPO seems
unimpressed.  In 2001-10, it %(gl:published new examination
guidelines) adapted to the 1998 decision, as if nothing had happened. 
Moreover, in the upcoming months a directive proposal from the
European Commission is expected, and so far few in Brussels or in the
national governments have shown any willingness to criticise the
policy of the EPO.  However even if the EPO and its friends eventually
succeed in pushing their agenda through, this will not clarify the
rules, but only bring the law in line with EPO jurisprudence, i.e.
institutionalise legal uncertainty:  US-style unlimited patentability
veiled in a mist of politically motivated patent newspeak, with the
effect that unimpressed American and Japanese companies will continue
to obtain most of those patents which the EPO in theory does not
grant.

#iei: At the end of the nineties, the terminology around the %(q:technical
invention) was still in use at the EPO.  However it only rarely served
to exclude patentable subject matter.  Most of the time it served to
play political games.  It allowed the EPO to pretend that it was doing
what the law and the politicians wanted it to do.  From this purpose,
a complex Doublespeak developped around terms such as %(q:technical
problem), %(q:technical contribution), %(q:computer-implemented
invention) etc.  The analysis of this Doublespeak is a task of
sociolinguistics which is beyond the scope of this overview.

#ahj: The author of this treatise is a  UK-educated barrister at Honkong
University, specialised in computing.  Lee analyses the history of
major shifts in EPO practice.  Lee starts by exaggerating the
restrictive character of the EPO's first examination guidelines of
1978: %(bc:The implication of this approach would be to severely
narrow the scope of patentability for software-related inventions.
Inventive process control %(tp|mechanisms|e.g. those used in a
conventional chemical plant to produce new polymers) that would
otherwise be standard patent material would fall outside the scope of
patentable subject-matter simply because a program was used in
implementing the inventive process control scheme.)  This, according
to Lee, apparently did not disturb the chemical industry as much as
certain other customers of the EPO:  %(bc:However, in response to
pressure from the computer industry and trends emerging in the US, the
European Patent Office reviewed its guidelines in 1985 !  ...).  Lee
then explains the ensuing drift toward wider patentability in terms of
landmark cases at the Technical Board of Appeal and the UK courts from
1986 until the early 90s.  The text offers a fairly good overview over
these cases.  Lee concludes with general observations about the
weaknesses of the EPO's reasoning such as the lack of definitions of
what is %(q:technical).  Lee proposes to replace this word with
%(q:physical).

#itp: Prof. Dr. Ulrich Löwenheim gives a detailed overview of the
applicability of copyright, patent and competition law to software in
Germany at the end of the 1980s.  Reports about efforts at the EPO to
extend patentability to software and predicts that there will be a
further drift in the coming years.  Concludes that coypright is the
most appropriate system for software, because it allows software
creators to reap fruits from their work and keeps ideas free for reuse
by legitimate competition.

#chd: A Polish patent attorney points out that the Commission's directive
proposal uses the term %(q:technical) in a way that raises more
questions than it answers, thereby creating legal insecurity.  The
paper also gives details about the developments in Poland, explaining
that a recently repealed Polish law has insisted that inventions must
be directed to physical devices and that this has lead the Polish
Patent Office to be stricter than others in rejecting function claims,
i.e. claims to an indefinite %(q:means for doing something).  Yet even
at the PLPTO people have found ways to successfully circumvent these
restrictions in many cases.  The german approach to %(q:technical
invention) has been the most well-reasoned, but it no longer provides
a meaningful guidance because information has become a force of
nature, Laszewski somewhat carelessly asserts, relying on Tauchert
statements distributed in the name of the german patent office as his
source.

#Nvs: After investigation of numerous EPO landmark decisions on software
patentability, Winischhofer writes in this law PhD thesis that the
decisions are highly questionable from a legal point of view, because
they are not only in conflict with the written law but even
contradictory within and between themselves.  Winischhofer summarizes:

#DdW: The EPO itself has so far failed to develop any systematic
interpreting framework.  Even the much-discussed %(q:computer program
product / IBM) decision bases its argumentation on individual cases. 
The EPO case law is thus a disparate collection of individual cases. 
Even the above-mentioned %(q:computer program product) decision falls
short of giving a definition of %(q:technical character), although the
EPO has stated that this is the decision on which it wants to base its
future judiciary practice.

#Enn: Only a few words are needed to explain to patent lawyers how 52 EPC
can be interpreted in a meaningful and consistent manner.  This
Eurolinux proposal for a EU directive fits into one letter paper page.
 The advocates of patent inflation on the other hand write long
%(cp:EU consultation papers) supposedly aiming to %(q:harmonise) and
%(q:clarify), but in reality creating more inconsistencies and
confusion with each line of text.

#Dla: The longtime chairman of the Siemens patent department, Arno Körber,
explains, how his department actively contributed to the %(q:evolution
of patent law) in order to adapt the %(q:stubbornly conservative)
practice of the German Patent Office to the %(q:modern) rules of the
global telecommunications and software business, which are set by the
USA, namely the Court of Appeal for the Federal Circuit (CAFC).  A
mega corporation like Siemens can cope with any rules, be they good or
bad, but it wants them to be uniform across the whole world.

#Dae: The software patent expert of the German Federal Court, Klaus
Melullis, cites Microsoft as an example of a company that has acquired
a dominant position by means of software and explains that since
software has entered the center-stage of economic activity, it must be
awarded patent protection.  The list of patentability exclusions in
Art 52 EPC is a non-exclusive list, meant to include only
%(q:non-technical) items.  But software is, according to Melullis,
technical.  He solves this contradiction by assigning the term
%(q:computer program as such) an obscure meaning that makes it
irrelevant in practice.  This highly-debated article forms the basis
for the new BGH caselaw of 1999/2000, which goes even beyond the EPO
in postulating unlimited patentability.

#TWa: This article points out numerous legal and practical
self-contradictions of software patenting.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatkorcu ;
# txtlang: en ;
# multlin: t ;
# End: ;

