<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Jurisprudence de Brevet sur Terrain Glissant: Le Prix a Payer pour le
Démontage de l'Invention Technique

#descr: Jusqu'à présent les programmes d'ordinateur et en général les
%(e:règles d'organisation et de calcul) ne sont pas des %(e:inventions
brevetables) selon la loi Européenne, ce qui n'exclut pas qu'un
procédé industriel brevetable puisse être controlé par un logiciel. 
L'Office Européen des Brevets et quelques Cours nationales ont
cependant contourné cette règle, en remplaçant un concept de
délimitation claire par une insécurité légale sans limites
reconnaissables.  Cet article offre une introduction aux problèmes et
à la littérature juridique sur ce sujet.

#PWc: Pour délimiter le domaine des inventions techniques, la jurisprudence
de brevet européenne a developpé deux distinctions fondamentales:

#kern: Matiére vs Information, Noyeaux vs Mise en Oevre

#Mes: Matériel (bleu) vs Immatériel (rouge)

#fvd: causalité physique vs fonctionalité logique

#ksr: concrèt vs abstrait

#htf: imitation facile d'une innovation difficile vs imitation difficile
d'une innovation facile

#TjW: noyeaux et mise en oevre

#Aeo: Chaque nouveaux enseignement est matérialisé par des méthodes connues.
 Il ne suffit pas que l'ensemble soit nouveaux et physique.  C'est le
noyeaux qui doit passer tous les examens.

#nei: Citations Introductoires

#GDW: Liberté de tout ce qui est intellectuel, ou brevetabilité de tout ce
qui ressemble à une machine ?

#Ueh: De la technicité au n'importe quoi

#ohn: De Notions Vidées de Sense et leur Contribution Technique a
l'Engineering Politique dans l'UE

#Wrk: Bibliographie Commentée

#rnW: Constitution des États Unis d'Amérique

#hpm: Revendication 1 accordée par l'Office Européen de Brevets en 2002

#tte: Beaucoup des problèmes discuetés en relation avec le logiciel ont leur
origine dans la contradiction entre les traits charactéristiques du
logiciel, c.a.d. charactère abstrait, général et logique, et les
traits nécéssaires des revendications de brevets, c.a.d. charactère
concrèt, spécifique et physique.

#Szt: En tant que les lois de la mathématique sont liés a la réalité, elles
sont incertaines.   En tant que les lois de la mathématique sont
certaines, elles ne sont pas liés a la rélité.

#WlW: Ce qui peut être controlé n'est jamais complètement réel.  Ce qui est
réel ne peut jamais être complètement controlé.

#bio: empruntant de %(VN)

#Tsa: The object of pure physics is the unfolding of the laws of the
intelligible world; the object of pure mathematics that of unfolding
the laws of human intelligence.

#mWo: L'informatique ne s'occupe pas plus des ordinateurs que l'astronomie
des téléscopes.

#nqs: Die menschliche Verstandestätigkeit gehört jedenfalls nach den
Anschauungen unserer Zeit nicht zu den beherrschbaren Naturkräften.
... Wenn der Bundesgerichtshof das menschliche Denken nicht dem
Begriff der Technik zuordnen will, weil dieser damit seiner
%(q:spezifischen und unterscheidenden Bedeutung) beraubt würde, so ist
das keine willkürliche inhaltliche Begrenzung dieses Begriffs für den
Bereich des Patentrechts, sondern eine konsequente Übernahme der
Anschauungen, die sich in Naturwissenschaften und Technik selbst
entwickelt haben.

#Sai: Cependant, dans tous ces textes, l'utilisation planifiée de forces de
la nature contrôlables est considérée comme une condition nécessaire
pour que l'agrément soit donné au caractère technique d'une invention.
Ainsi que nous l'avons exposé plus haut, l'inclusion des forces de la
raison humaine en tant que telles dans le domaine des forces de la
nature dont l'utilisation pour la création d'une innovation fonde son
caractère technique, aurait pour conséquence directe l'attribution
d'une signification technique à toutes les activités de la pensée qui
en tant que série d'instructions sont susceptibles de causer un
résultat d'une manière prévisible. A partir de là, le concept de
technicité perdrait son rôle de critère, l'ensemble des réalisations
de l'intelligence humaine - dont les l'envergure et les limites sont
inconnues et imprévisibles - se verraient ouvrir les portes du droit
des brevets.

#Ene: Par conséquent, il s'interdit de atteindre une protection des
achevements intellectuels par la voie d'une extension des limites de
la technicité, concepte qui serait alors détourné de son rôle. Il doit
rester clair, bien au contraire, qu'une règle d'organisation ou de
calcul en elle-même ne mérite pas de se voir protégée par un brevet si
sa relation au domaine technique ne repose que dans son applicabilité
pour l'emploi d'un ordinateur selon sa prédisposition normale. Il ne
nous appartient pas de débattre ici de l'éventuelle protection qui
peut lui être accordée soit par le droit d'auteur, soit par le droit
de la concurrence.

#B1p: Court Fédéral de Justice en 1976: Décision %(q:Programme de
Disposition)

#Aso: A computer program may take various forms, e.g. an algorithm, a
flow-chart or a series of coded instructions which can be recorded on
a tape or other machine-readable record-medium, and can be regarded as
a particular case of either a %(e:mathematical method) or a
%(e:presentation or information).  If the contribution to the known
art resides solely in a computer program then the subject matter is
not patentable in whatever manner it may be presented in the claims. 
For example, a claim to a computer characterised by having the
particular program stored in its memory or to a process for operating
a computer under control of the program would be as objectionable as a
claim to the program %(e:per se) or the program when recorded on
magnetic tape.

#IWn: En délibérant si il s'agit la d'une invention, l'examinateur doit
ignorer la forme ou catégorie de revendication et se concentrer sur le
contenu pour identifier la contribution nouvelle laquelle la
telle-dite %(qc:invention) fait a l'art connue.  Si cette contribution
n'est pas une invention, il n'y a pas la de la matière brevetable.  Ce
point est illustré par les exemples %(dots) de différentes façons de
revendiquer un program d'ordinateur.

#Lkm: Le logiciel est-il donc finalement brevetable?

#SuW: Sans doute pas encore.

#Eee: En réalité, les règles nationales et conventionnelles sont claires: 
elles posent sans équivoque un principe de non-brevetabilité du
logiciel.  Le jeu qui se joue aujourd'hui consiste à contourner d'une
manière ou d'une autre celles-ci, par exemple en imaginant de
considérer, comme on l'a vu, l'ensemble constitué par le matériel et
le logiciel comme une machine virtuelle susceptible (demain ...)
d'être breveteée.  À ce compte-là, on peut parler brevets.  Les
brevets susceptibles d'être ainsi obtenus, par ce canal ou un autre,
n'ont, toutefois, que la valeur qu'on leur prête - mais il ne faut pas
écarter l'hypothèse selon laquelle on finirait par une sorte de
consensus à ne pas vraiment la discuter.  De fait, l'efficacité de ce
countournement des règles légales sera largement fonction du fait
qu'un tel consensus se dégagera pour accepter --- contre les règles
positives --- que ce nouveau jeu se joue ou non.  La question ne se
situe plus sur le terrain juridique %(e:stricto sensu).

#Aol: un regard rapide sur les %(ep:résultats des examens en brevetabilité
de logiciels en Europe) et sur les %(ne:études économiques de leurs
conséquences sur l'innovation et sur la concurrence) suffit à
convaincre la plupart des gens que dans ce domaine les choses ne
tournent pas rond. Certaines personnes promettent que ce n'est qu'un
phénomène passager qu'il faut surmonter par une amélioration des
méthodes de recherche d'antériorité. D'autres au contraire font
remarquer que même si ces problèmes, remontant à une dizaine d'années,
de la recherche d'antériorité pouvaient être résolus dès demain cela
n'arrangerait pas grand'chose : il n'y a jamais eu de %(no:normes
quantifiant le degré d'innovation) vraiment utilisables  et même si on
les établissait dès demain, de telles normes auraient bien du mal à
contenir le penchant prononcé des technocrates en brevets à %(q:en
donner à nos clients pour leur argent), comme dit fort efficacement un
haut fonctionnaire des brevets européens.

#Ylq: Cependant, le problème n'est pas là non plus. Même si l'ont pouvait
réellement mettre en oeuvre des normes plus sévères pour juger du
caractère innovant et de l'inventivité, la question demeurerait de
savoir dans quel but les brevets sont délivrés et de comment l'on doit
délimiter le domaine brevetable. Les brevets sont des droits de
monopole forts, que nous ne voulons pas nécessairement voir attribués
à tous les innovateurs rusés. Ou alors, faut-il breveter les méthodes
politiques ? Les procédés commerciaux ? Les chaînes argumentatives ?
Tout ce que les examinateurs des offices de brevets sont prêts à
examiner ? Jusqu'à présent la réponse à cette question se référait au
concept d'%(q:invention technique), c'est-à-dire qu'on exigeait d'une
invention brevetable qu'elle nous enseigne non seulement une idée
nouvelle, mais aussi une nouvelle manière de mettre en oeuvre des
idées à l'aide des forces de la nature, donc une solution physique et
pas seulement logique.

#Tor: Diverses raisons plaident en faveur de cette distinction
traditionnelle entre les idées logiques et les mises en oeuvre
physiques.

#Ebh: D'un côté les questions que nous adressons au %(e:monde physique) ou
bien, plus précisément, aux %(e:forces contrôlables de la nature).
Chaque réponse nouvelle à l'une de ces questions est assez difficile à
trouver et nécessite l'intervention d'objets matériels aussi bien
pendant les phases de recherche que pour son utilisation. Tant pour la
recherche que pour la mise en oeuvre de ces %(q:solutions techniques),
une organisation industrielle coûteuse est en réalité nécessaire. Il
s'agit de produits matériels, dont le coût unitaire minimal (coût
marginal) est supérieur à zéro. A ce coût unitaire s'ajoutent des
frais de licence. La règlementation supplémentaire du marché due aux
brevets peut certes conduire à des prix surévalués ainsi qu'à d'autres
douloureux dérèglements du marché, mais il existe du moins une petite
relation entre l'investissement industriel consenti au départ et le
gain supplémentaire dû au mécanisme des brevets.

#Osr: De l'autres côté les questions que nous adressons au %(e:monde
intellectuel), c'est-à-dire aux forces rationnelles des hommes et aux
systèmes axiomatiques érigés par elles. La réponse à de telles
questions se trouve dans les %(e:règles d'organisation et
algorithmes), dont la preuve peut être apportée grâce aux simples
moyens intellectuels. Souvent cette preuve est apportée par une
%(q:démonstration) mathématique fiable à 100 %, ce qui ne pourrait pas
arriver dans le monde physique.

#Tgc: Il y a seulement 50 ans, la délimitation de ces deux mondes se faisait
en général naturellement. Parfois cependant, certains cas difficiles
posaient problème, ce qui donna lieu à des méthodes élaborées de
déparation de l'immatériel et du matériel, publiés déja au 19ème
siècle.

#Hwn: Mais au cours du temps les deux mondes se sont de plus en plus
entremêlés. La %(q:logique des machines) (le logiciel ou software)
s'est séparée de plus en plus clairement de la %(q:physique des
machines) (le matériel informatique ou hardware) et son importance
économique a augmenté d'une manière spectaculaire - ce qui ne veut pas
dire pour autant que le progrès dans le domaine des sciences physiques
ait été inexistant. Avec l'entrée en scène du %(e:calculateur
universel), la séparation entre la logique et la physique est devenu
encore plus claire, de plus en plus de problèmes ont pu être ramenés à
des problèmes de logique.

#Ssr: C'est pour cela que très tôt, quelques avocats en droit des brevets
sont devenus nerveux et ont commencé à penser que puisque les procédés
automatiques (robotiques) étaient brevetables, on devrait pas faire de
distinction entre les procédés se déroulant sur support analogique et
ceux se déroulant sur support numérique. Pourquoi en effet les
professionnels des brevets devraient-ils rester sur le bord de la
route vers notre avenir numérique ?

#Oan: Cependant d'autres pensaient au contraire que puisque les algorithmes
et les règles d'organisation ne constituent pas les inventions
techniques, on ne devrait pas faire de distinction entre les
découvertes faites sur le papier, à la règle à calcul ou avec d'autres
instruments numériques comme les calculatrices, et celles qui sont
faites à l'aide des ordinateurs. Pourquoi en effet la liberté de
pensée devrait-elle être laissée sur le bord de la route vers notre
avenir numérique ?

#Arr: une observation plus exacte conduit à donner raison au second
argument. Car les fondations économiques de la délimitation des
inventions brevetables se trouvent dans l'argument suivant. Qu'elles
représentent ou non des procédés techniques, les idées abstraites
(algorithmes) sont élaborées sans coûts d'expérimentation, sont
applicables à une étendue non limitée de problèmes et grâce à
l'informatique elles peuvent se répandre encore plus vite et à moindre
coût que les autres formes de pensée humaine. On peut dire alors que
le coût marginal des produits de l'information est voisin de zéro, et
dans le prix total du produit, le rapport entre les frais de brevet et
le prix de revient produit une erreur de division par zéro. De plus,
le contrat implicite qui sous-tend le brevet %(q:monopole de
l'application contre publication de l'idée) est étendu jusqu'à
l'absurde. Car entre l'information et son application il n'y a rien du
tout, aucune espèce d'invention, et toute publication complète de
l'idée en syntaxe de Turing devient une contrefaçon.

#TWb: Pendant les années 60/70, ces problèmes ont été beaucoup discutés par
les spécialistes du droit des brevets, particulièrement en Allemagne.
Dans des ouvrages spécialisés, le droit et la jurisprudence sur les
brevets ont été expliqués et affinés. Ce sont usqu'aux voies erronées
qui auraient pu être choisies dans l'avenir, ainsi que leurs
conséquences fâcheuses, qui ont été mises en évidence d'une manière
prophétique, dans la jurisprudence comme dans les écrits. La seule
raison pour laquelle ces voies dangereuses ont été tout de même
empruntées par l'OEB et la BGH s'appelle Faiblesse (Ignorance et
Envie) de l'Homme. Les juristes des brevets ont péché par naïveté. Ils
croient volontiers les erreurs les plus répandues et s'en font
gentiement l'écho dans les publications spécialisées. Car le savoir
véritable, celui sur le concept de technicité ainsi que sur les autres
modes de délimitation interdisciplinaires entre les domaines de la
science, ne rapporte pas d'argent ni du côté des offices de brevets
nationaux ni auprès de l'office européen, lequel se finance
directement sur l'argent des brevets.

#Bns: Jusqu'à présent, les programmes d'ordinateurs sont non brevetables en
Europe, ce qui n'exclut pas qu'un procédé technique brevetable puisse
aussi être contrôlé par un programme. Un tel  procédé doit tout de
même se baser sur un exposé innovant montrant un rapport de cause à
effet de nature physique. Les forces physiques mises en jeu doivent
faire partie intégrante de la solution du problème. Des solutions de
problèmes qui reposent uniquement sur la logique fonctionnelle ne
peuvent pas valoir en tant qu'%(inventions techniques), mais en tant
que %(q:règles d'organisation ou algorithmes). Le texte de l'arrêt
%(dp:Dispositionsprogramm), 1976 de la Cour Fédérale de Justice
démontre de manière exemplaire que toute extension du domaine
brevetable vers la logique fonctionnelle représente une menace pour
les libertés fondamentales, tout en ne favorisant pas spécialement le
progrès des disciplines concernées. Diverses études anciennes et
récentes viennent renforcer cette sage et claire délimitation énoncée
par le législateur, et l'interprétation systématique à travers la très
juridique décision fondamentale de 1976, qui ces derniers temps se
retrouve dans des jurisprudences et des commentaires juridiques de la
plupart des juridictions. C'est pourquoi cette décision
%(q:Dispositionsprogramm) est régulièrement citée, soit comme exemple,
soit comme signe d'une certaine mauvaise conscience.

#GWz: Dans le même temps, depuis les années 60, un groupe de spécialistes en
droit des brevets de plus en plus en influent refuse la limitation au
%(q:monde matériel) du domaine brevetable au motif qu'alors celui-ci
perdrait de plus en plus de domaines importants de la vie économique.
A cause de cette façon de penser, les normes de brevetabilité se sont
effondrées petit à petit. Ensuite, au début des années 80, des
méthodes de management industriel ont été brevetées, et du coup des
revendications qui étaient auparavant mal reçues - des revendications
concernant des fonctionnalités abstraites indépendantes de toute
implémentation liée aux forces physiques - sont devenues acceptables.
Progressivement, la %(q:discrimination envers les logiciels) inscrite
dans la loi a commencé à paraître démodée. Les services juridiques de
grandes entreprises comme Siemens ou IBM ont investi beaucoup de temps
et d'argent pour infléchir les modes de pensée des agents des offices
de brevets %(q:envers les nouvelles technologies sans exclusives),
grâce à quelques décisions de fond qu'ils réussirent à obtenir de
l'OEB ou de la BGH. Avec la logique fonctionnelle, ce sont de plus en
plus tous les procédés économiques intéressant nos vies qui sont
devenus brevetables. Dès le début des années 90, toute la liste des
exceptions à la brevetabilité de l'article 52 de la Convention de
Münich, depuis les mathématiques jusqu'aux %(q:méthodes d'affaires) et
à la %(q:reproduction de l'information), ne revêtait plus pour l'OEB
qu'une utilité purement décorative. Pour la pratique juridique de
l'Office Européen des Brevets, le fait qu'un objet candidat à un
brevet figure, ou non, sur la liste en question est devenu
complètement sans importance. Il peut cependant se produire que telle
revendication, pour tel ou tel %(q:système de calcul de pension),
échoue devant l'OEB parce que le demandeur a oublié d'exposer à la
date qu'il fallait suffisamment d'%(q:effets techniques). La
brevetabilité est pour l'essentiel une question de soin dans la
préparation du document de présentation du brevet, ainsi que le fait
remarquer à juste titre un spécialiste de renom.

#Uua: jusqu'au milieu des années 90 on hésitait encore devant les dernières
conséquences. Les règles d'organisation et les algorithmes n'étaient
brevetés qu'indirectement, habillés verbalement comme des procédés
%(q:techniques). Celui qui mettait un programme sur le marché ne
risquait donc pas encore de contrevenir à un brevet existant. Ce n'est
qu'après quelques années de battage à travers les périodiques de droit
des brevets qu'en 1998 il est devenu possible de surmonter les
derniers blocages. La chambre des plaintes techniques (technischen
Beschwerdekammer) n'est pas compétente sur les questions juridiques.
Mais c'est une de ses décisions, bientôt suivie par d'autres décisions
du même genre, qui, communiquée à l'OEB, ouvrit à ce dernier la
possibilité d'accepter les demandes de brevets portant directement sur
des objets immatériels (%(q:les produits logiciels), les logiciels,
les structures de données, etc...). Dans une %(q:remarque de
l'éditeur), la direction de l'Office explique qu'elle a l'intention
d'%(q:infléchir à l'avenir la pratique de l'Office dans l'esprit de
cette décision) et de réécrire en conséquence ses directives d'examen.

#hii: Meanwhile, the caselaw of the EPO has indeed been pushing the
boundaries of what is technical ever wider.  According to its
Examination Guidelines of 2002 %(q:A further technical effect which
lends technical character to a computer program may be found e.g. in
the control of an industrial process or in processing data which
represent physical entities or in the internal functioning of the
computer itself or its interfaces under the influence of the program
and could, for example, affect the efficiency or security of a
process, the management of computer resources required or the rate of
data transfer in a communication link.)

#iir: According to rulings on EP 0689133, even the %(q:economical use of the
resource area on the screen) is %(q:technical).

#bto: And if that doesn't cover the program, then %(q:processing which is
based on considerations of how a computer works is technical).  Thus
in the Sohei case a patent the EPO Board of Appeal upheld a patent for
using the same input form to update two databases, namely an inventory
database and a billing database, because it %(e:implied) (but didn't
actually limit itself by specifying how to do it) the handling of
files containing different types of information, which is
%(e:technical).

#Dst: Pourtant, les brevets accordés sur ces bases ont une valeur
incertaine. Les tribunaux fidèles à la loi (par exemple la Court
Suprème Suédoise et la %(bp:17ème chambre fédérale des brevets)
détectent régulièrement des contradictions dans l'argumentation de
l'OEB et de la BGH et rejettent les revendications sur les
fonctionnalités logiques et sur les objets de l'information. Les
tribunaux nationaux d'autres pays sont aussi susceptibles de soulever
des difficultés, par exemple faute de cas d'espèce faisant autorité en
ce qui concerne les pénalités à appliquer. Par conséquent, le
%(pm:mouvement pro-brevets) est très tenté de faire modifier l'accord
européen sur les brevets ou de le rendre inopérant à l'aide de
nouvelles légilations. Un projet en ce sens existe depuis 1997
environ, et a d'abord échoué, en novembre 2000, devant une opposition
publique manifeste. %(gl:Ce qui n'empêche pas l'OEB de décider
précipitamment, en 2001, de changer ses règeles d'examen) et de faire
pression sur les gouvernements et sur la commission européenne. C'est
à ce moment-là qu'apparaît un projet de %(q:directive européenne sur
la brevetabilité des inventions que l'on peut mettre en oeuvre sur les
ordinateurs) dans laquelle il s'agit manifestement de légaliser la
%(pk:pratique actuelle de l'OEB qui consiste à accorder des brevets
sur les règles d'organisation, algorithmes et autres objets de
l'information mis en oeuvre sur ordinateur). Dans l'intérêt de
l'%(q:harmonisation) et de l'%(q:élimination d'incertitudes
juridiques), on vise à stabiliser les bases vascillantes sur
lesquelles la jurisprudence de l'OEB évolue actuellement. Des projets
de juristes en brevets de la Communauté Européenne montrent pourtant
que dans le meilleur des cas certaines contradications entre la
jurisprudence et le droit positif pourront être éliminées, mais en
aucun cas les contradications internes de la jurisprudence. Là où le
mouvement pro-brevet parle de %(q:clarification d'un état juridique
confus), il faut comprendre %(e:remplacement d'une règle légale claire
mais gènante Art 52 CBE par une jurisprudence confuse, qui n'impose
plus aucune limitation sur la brevetabilité), autrement dit %(e:une
brevetabilité sans limites a l'américaine), même si drapée avec des
mots comme %(q:effet tecnique), qui visent à cacher la rupture.  Du
fait que l'OEB est obligé, pour des raisons politiques, d'avancer dans
sa pratique sous le voile pudique du dogmatisme légal, de nombreuses
entreprises américaines et japonaises, pas du tout impressionnées,
vont obtenir des brevets que l'OEB n'est pas censé accorder.

#iei: At the end of the nineties, the terminology around the %(q:technical
invention) was still in use at the EPO.  However it only rarely served
to exclude patentable subject matter.  Most of the time it served to
play political games.  It allowed the EPO to pretend that it was doing
what the law and the politicians wanted it to do.  From this purpose,
a complex Doublespeak developped around terms such as %(q:technical
problem), %(q:technical contribution), %(q:computer-implemented
invention) etc.  The analysis of this Doublespeak is a task of
sociolinguistics which is beyond the scope of this overview.

#ahj: The author of this treatise is a  UK-educated barrister at Honkong
University, specialised in computing.  Lee analyses the history of
major shifts in EPO practise.  Lee starts by exaggerating the
restrictive character of the EPO's first examination guidelines of
1978: %(bc:The implication of this approach would be to severely
narrow the scope of patentability for software-related inventions.
Inventive process control %(tp|mechanisms|e.g. those used in a
conventional chemical plant to produce new polymers) that would
otherwise be standard patent material would fall outside the scope of
patentable subject-matter simply because a program was used in
implementing the inventive process control scheme.)  This, according
to Lee, apparently did not disturb the chemical industry as much as
certain other customers of the EPO:  %(bc:However, in response to
pressure from the computer industry and trends emerging in the US, the
European Patent Office reviewed its guidelines in 1985 ...).  Lee then
explains the ensuing drift toward wider patentability in terms of
landmark cases at the Technical Board of Appeal and the UK courts from
1986 until the early 90s.  The text offers a fairly good overview over
these cases.  Lee concludes with general observations about the
weaknesses of the EPO's reasoning such as the lack of definitions of
what is %(q:technical).  Lee proposes to replace this word with
%(q:physical).

#itp: Prof. Dr. Ulrich Löwenheim gives a detailed overview of the
applicability of copyright, patent and competition law to software in
Germany at the end of the 1980s.  Reports about efforts at the EPO to
extend patentability to software and predicts that there will be a
further drift in the coming years.  Concludes that coypright is the
most appropriate system for software, because it allows software
creators to reap fruits from their work and keeps ideas free for reuse
by legitimate competition.

#chd: A Polish patent attorney points out that the Commission's directive
proposal uses the term %(q:technical) in a way that raises more
questions than it answers, thereby creating legal insecurity.  The
paper also gives details about the developments in Poland, explaining
that a recently repealed Polish law has insisted that inventions must
be directed to physical devices and that this has lead the Polish
Patent Office to be stricter than others in rejecting function claims,
i.e. claims to an indefinite %(q:means for doing something).  Yet even
at the PLPTO people have found ways to successfully circumvent these
restrictions in many cases.  The german approach to %(q:technical
invention) has been the most well-reasoned, but it no longer provides
a meaningful guidance because information has become a force of
nature, Laszewski somewhat carelessly asserts, relying on Tauchert
statements distributed in the name of the german patent office as his
source.

#Nvs: Après une analyse approfondie de nombreuses décisions exemplaires de
l'OEB sur la brevetablité des logiciels, Winischhofer explique que ces
décisions sont sujettes à caution et contradictoires. Non seulement en
contradiction avec la loi, mais aussi en contradiction l'une avec
l'autre. Ainsi que le résume Winischhofer :

#DdW: Même l'OEB n'a jusqu'à présent rien développé de systématique. Même la
décision %(q:produit logiciel/IBM), discutée en détail, s'appuie sur
différents cas isolés. La jurisprudence de l'OEB semble tellement
porter la marque d'une sorte de casuistique, qu'une définition de
l'%(q:effet technique) nécessaire reste elle-même redevable de la
décision pré-citée - ceci bien que l'OEB, comme cité plus haut,
envisage d'orienter sa jurisprudence plutôt vers cette décision.

#Enn: Il ne faut que quelques mots pour expliquer à des juristes en brevets,
même pointilleux, de quelle manière il faut comprendre l'article 52 de
la convention de Münich ou l'article 1 de la loi sur les brevets. Le
projet de directive européenne d'Eurolinux tient confortablement dans
une page A4. Les représentants du mouvement pro-brevet, eux, parlent à
longueur de page d'%(q:éclaircissement) et d'%(q:harmonisation), mais
chacune de leurs crée une confusion de concepts et ce flou porteur de
dissonances.

#Dla: Le directeur du département des brevets de Siemens explique que son
service a contribué activement au %(q:progrès continu du droit), afin
de faire s'accorder la pratique de l'office allemand des brevets avec
l'actualité de la jurisprudence américaine (déterminante dans un
contexte de globalisation des marchés).

#Dae: En s'appuyant sur l'exemple de Microsoft, l'expert en brevets
logiciels de la BGH expose les raisons de politique économique pour
lesquelles les brevets logiciels sont désirables, et met en évidence
des moyens de les promouvoir en dépit du droit en vigueur. Il explique
que le législateur n'a voulu exclure du champ de la brevetabilité que
les objets non-techniques, et que les programmes d'ordinateurs sont
techniques. Afin de résoudre cette contradiction, Melullis construit
une entité nommée %(q:programme d'ordinateur en tant que tel), et en
lui attribuant la signification de %(q:programme d'ordinateur dans le
cerveau humain). Son collègue Tauchert de l'office allemand des
brevets plaide au contraire pour le %(q:code source). Peu importe,
l'essentiel c'est de minimiser l'importance de ce que le législateur a
écrit.

#TWa: Cet article montre de numbreuses inconséquences et contradictions
pratiques et légales quant au brevet logiciel.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: obenassy ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatkorcu ;
# txtlang: fr ;
# multlin: t ;
# End: ;

