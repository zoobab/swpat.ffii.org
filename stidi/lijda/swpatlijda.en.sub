\begin{subdocument}{swpatlijda}{The Patent Movement}{http://swpat.ffii.org/analysis/movement/swpatlijda.en.html}{Workgroup\\swpatag@ffii.org\\english version 2003/12/16 by PILCH Hartmut\footnote{http://www.ffii.org/~phm}}{The patent system has created its own powerful quasi-religious movement, which is promoting and expanding it.  The movement's members are united by a common faith in the beneficiality of ``intellectual property'', and equipped with rule-setting power in all important large organisations, including patent offices, patent courts, governments and large companies.  Usually, CEOs and leading politicians don't have any opinion about patents and, when asked, are afraid of exposing their lack of expertise.  Therefore they ask their patent department to speak on behalf of them, not noticing that the patent movement is pursuing its own agenda, which has very little in common with business or government interests.}
\begin{sect}{guru}{Distorting Coroporate Opinion}
Numerous software companies have pronounced themselves against the patentability of software.  Nevertheless the press keeps reporting that ``the software industry demands patent protection''.  FFII talks to with Siemens representatives showed that even with Siemens, the only German company that has pronounced itself in favor of software patents, this view does not hold.  Many techniciancs and middle managment people at Siemens are wary about the bureaucratic overhead associated with patenting and concerned about dangers from external software patents and say that all this ``enormously slows down the pace of the industry''.   However these voices are not heard.  Whenever the top executives are asked about the impact of patents, they hand the question over to their patent department.  And this patent department is not only a part of Siemens, but also a part of a large secular religion:  the patent movement.

The only industry association that ever made a public statement in favor of patent expansion\footnote{http://www.gnn.de/9907/99072804-ji.html}, the ZVEI, just used a draft that came from its patent expert, who is at the same time the head of the patent department of Siemens, Arno K\"{o}rber.  This is what we found out by telephonic inquiries at the time.

The situation in the German Federal Government is the same.  Economists in the Ministery of Economics\footnote{http://www.bmwi.bund.de} (BMWi) view the tendency to make programming concepts patentable very critically.  But it is the Ministery of Justice\footnote{http://swpat.ffii.org/players/bmj/swpatbmj.de.html} (BMJ) which has the say in patent questions.

Selbst innerhalb der Patentbewegung sind Verf\"{a}lschungen durch das Partikularinteresse der zum jeweiligen Thema befragten Experten zu beobachten.  In der Softwarepatente-Debatte haben sich alle Organisationen dieser Bewegung auf die Seite der Patentmaximalisten geschlagen.  Die kritischen Stimmen waren kaum zu h\"{o}ren.  Einerseits mag dies nicht verwundern, da es zun\"{a}chst zu einer Expansion des T\"{a}tigkeitsfeldes und einem Zuwachs an Bedeutung f\"{u}r den gesamten Berufsstand f\"{u}hrt und dem Selbstverst\"{a}ndnis der Patentbewegung entgegenkommt.  Andererseits besteht durch Softwarepatente die Gefahr, dass anst\"{o}{\ss}ige Patentstreitigkeiten im Internet und in der Presse intensiv beachtet und durchleuchtet werden, so dass die Zahl der Kritiker des Patentwesens wachsen und der gesamte Berufsstand eventuell wegen 3\percent{} der derzeit vergebenen Patente in Misskredit geraden k\"{o}nnte.  Es ist bei etwas distanzierter Betrachtung keineswegs sicher, ob die Aufweichung der Patentierbarkeitskriterien der Patentbewegung mehr n\"{u}tzt oder schadet.  Aber die meisten Patentanw\"{a}lte haben keine Zeit, sich diese Frage zu stellen.  Sie verlassen sich auf die Glaubenss\"{a}tze der Bewegung und auf das Urteil derjenigen Patentjuristen, die von Softwarepatenten leben.

\"{A}hnlich wie die Standpunkte der Unternehmen vom Gruppeninteresse der Patentbewegung verzerrt werden, unterliegt die Meinungsbildung innerhalb der Patentbewegung offenbar einer Verzerrung durch diejenigen Patentanw\"{a}lte, die sich auf Software spezialisiert haben.
\end{sect}

\begin{sect}{credo}{Der Glaube an das Geistige Eigentum}
Ein Patentanwalt formulierte den ersten Kernsatz des Glaubensbekenntnisses in einer Diskussion auf dem FFII-Verteiler:  ``Ich glaube an das Geistige Eigentum''.

Dieser Glaube beruht auf Mythenbildung und verleiht der Patentbewegung sowohl ihre Durchsetzungskraft als auch ihre sektiererische Natur.  Er erlaubt es ihr, guten Gewissens Patente auch dort gesetzlich und v\"{o}lkerrechtlich vorzuschreiben, wo diese nicht, wie von der US-Verfassung gefordert ``dem Fortschritt der Wissenschaft und der n\"{u}tzlichen K\"{u}nste dienen''.  Fritz Machlup berichtet\footnote{http://swpat.ffii.org/papers/machlup58/machlup58.en.html} \"{u}ber die Urspr\"{u}nge der Mythenbildung um das Geistige Eigentum:

\begin{quote}
Die Naturrechtstheorie wurde von der franz\"{o}sischen Verfassunggebenden Versammlung feierlich \"{u}bernommen, als sie in der Pr\"{a}ambel zum Patentgesetz von 1791 verk\"{u}ndete, da{\ss}

\begin{quote}
jede neue Idee, deren Verwirklichung oder Entwicklung der Gesellschaft n\"{u}tzlich werden kann, in erster Linie demjenigen geh\"{o}rt, der sie konzipiert hat und da{\ss} es eine Verletzung des Wesens der Menschenrechte darstellte, wenn eine gewerbliche Erfindung nicht als Eigentum ihres Sch\"{o}pfers anerkannt w\"{u}rde.
\end{quote}

Diese Auffassung der franz\"{o}sischen Juristen, wonach eine Idee ebenso Gegenstand eines Eigentumsrechts sein k\"{o}nne wie eine k\"{o}rperliche Sache, wurde von vielen Seiten kritisiert, zur\"{u}ckgewiesen und l\"{a}cherlich gemacht. Man fragte sich, wie das Eigentum an Ideen, wenn es sich um ein ``Naturrecht'' handele, auf 14 oder 17 Jahre beschr\"{a}nkt werden k\"{o}nne, anstatt es f\"{u}r ewige Zeiten anzuerkennen.

Tats\"{a}chlich stritten einige Hartn\"{a}ckige f\"{u}r ein ewiges und unver\"{a}u{\ss}erliches Eigentumsrecht an Ideen. Andere wiesen darauf hin, da{\ss} niemand eine Idee ausschlie{\ss}lich besitzen k\"{o}nne, weder f\"{u}r begrenzte noch f\"{u}r unbegrenzte Zeit, sobald er sie mitgeteilt hat und sie demzufolge mit anderen teilt. Die logischen Elemente des Eigentumsbegriffs, wie er auf k\"{o}rperliche Gegenst\"{a}nde Anwendung findet - Inbesitznahme, Besitz, Verf\"{u}gung, Aneignung, usw. - seien auf Ideen unanwendbar, die sich nicht in materiellen Dingen verk\"{o}rpern. Wer sich \"{u}ber den ''Diebstahl`` an seiner Idee beschwert, ``gibt etwas als entwendet an, das er noch besitzt, und er w\"{u}nscht etwas zur\"{u}ck, das ihm, tausendf\"{a}ltig zur\"{u}ckerstattet, durchaus nicht mehr n\"{u}tzt, als der einmalige bereits vorhandene Besitz.''

Im Gegensatz zum Eigentum an Sachen bedeutet das sogenannte geistige Eigentum weder die Verf\"{u}gbarkeit \"{u}ber einen Gegenstand noch \"{u}ber eine Idee, sondern vielmehr die Beherrschung des Marktes f\"{u}r die Gegenst\"{a}nde, in denen sich die Idee verk\"{o}rpert. Ein k\"{o}rperlicher Gegenstand geh\"{o}rt jemandem, der bestimmen kann, wie er zu benutzen ist; es w\"{a}re notwendig, ihn seinem Besitzer wegzunehmen, ehe ein anderer ihn benutzen kann. Im Gegensatz dazu kann eine Idee einer unbeschr\"{a}nkten Anzahl von Personen geh\"{o}ren und ihre Benutzung durch einige verhindert nicht ihre Verwendung durch andere. Es ist interessant, da{\ss} einige franz\"{o}sische Juristen zugaben, da{\ss} sie es haupts\"{a}chlich aus Gr\"{u}nden der Propaganda vorzogen, von ``nat\"{u}rlichen Eigentumsrechten'' zu sprechen, besonders deswegen, weil einige der sonst zur Verf\"{u}gung stehenden Ausdr\"{u}cke, wie ``Monopolrecht'' oder ``Privileg'', gar so unbeliebt waren.
\end{quote}

Der Propagandamythos vom ``Geistigen Eigentum'' wird auch heute noch eifrig kultiviert, und f\"{u}r ihn werden Staaten und internationale Organisationen eingespannt.  Hier seien einige Beispiele genannt:

\begin{itemize}
\item
Das franz\"{o}sische Patentamt\footnote{http://www.inpi.fr} nennt sich ``Institut pour la Propri\'{e}t\'{e} Intellectuelle''.

\item
Das Schweizer Patenamt\footnote{http://www.ige.ch} nennt sich ``Insitut f\"{u}r Geistiges Eigentum''.

\item
Die Patentorganisation der Vereinten Nationen nennt sich ``Weltorganisation f\"{u}r geistiges Eigentum''

\item
Das Patentamt der VR China wurde k\"{u}rzlich in ``Staatsamt f\"{u}r Geistiges Eigentum'' (Guojia Zhishi Chanquan Ju 國家知識産權局) umbenannt.

\item
Im Herbst 2000 beschlossen die Vereinten Nationen auf Anregung chinesischer und algerischer Patentjuristen, den 26. April jedes Jahres als ``Tag des Geistigen Eigentums'' zu feiern.

\item
Im Fr\"{u}hjahr 2001 startete das Britische Patentamt gemeinsam mit dem Kultusministerium eine Kampagne, um Kindern von Klein auf das Evangelium des ``Geistigen Eigentums'' nahezubringen.\footnote{http://aful.org/wws/arc/patents/2001-06/msg02034.html}
\end{itemize}
\end{sect}

\begin{sect}{erfrom}{Inventor Romanticism and the Utopia of the Small Pesant}
Die Idee vom ``Geistigen Eigentum'' spricht zutiefst menschliche Sehns\"{u}chte an:

\begin{itemize}
\item
Ich sollte ebenso abkassieren d\"{u}rfen, wie die Reichen und M\"{a}chtigen, deren Privatleben ich t\"{a}glich in der Boulevardpresse verfolge.

\item
Ich bin ein verkanntes Genie und mithilfe des Patentwesens werde ich meinen gerechten Platz in der ersten Liga finden.

\item
In dieser Welt sollte es keine komplizierten Verteilungshierarchien und keine Politik geben.  Ich will nur meinen privaten Acker pfl\"{u}gen m\"{u}ssen und die volle Wertsch\"{o}pfung sollte sich dann ganz von selbst einstellen.
\end{itemize}

Das Patentwesen er\"{o}ffnet einen Weg der vertikalen Mobilit\"{a}t.  Es bietet dem ``kleinen Erfinder'' die Aussicht, mit einem Satz \"{u}ber alle M\"{a}chte und Barrieren dieser Welt hinweg senkrecht in die erste Liga hoch zu springen.

Es gibt in der Menschheitsgeschichte immer wieder gro{\ss}angelegte Gewinnspiele, die sich \"{a}nhlicher Beliebtheit erfreuten.  Das System der j\"{a}hrlichen Beamtenpr\"{u}fungen im kaiserlichen China erlaubte den direkten Sprung vom ``kalten Fenster'' des Schriftgelehrten zu den h\"{o}chsten \"{A}mtern und W\"{u}rden.  Gelang dieser Sprung, so ``stiegen die H\"{u}hner und Hunde des Hofes mit einem Satz in den Himmel auf''.  Dieses System hielt sich \"{u}ber fast 2000 Jahre hinweg und \"{u}berlebte noch lange, nachdem es zu einem fast lotto-artigen Gewinnspiel degeneriert war.

Gewinnpiele haben den unsch\"{a}tzbaren Vorteil, dass ein objektiver Mechanismus dar\"{u}ber entscheidet, wer gewinnt.  Undurchsichtige Verteilungsmechanismen und Mobilit\"{a}tsbarrieren werden ausgeschaltet.  Die Politik wird abgeschafft.  Der Kleinbauer kann seine Scholle beackern, ohne sich mit der oft unappetitlichen Last des gesellschaftlichen Lebens herumplagen zu m\"{u}ssen.

Selbst seri\"{o}se Wirtschaftswissenschaftler haben bisweilen angef\"{u}hrt, dass ein besonderer Verdienst und vielleicht sogar der eigentliche Sinn des Patentwesen darin liege, dass es durch eine Art gro{\ss}angelegte Illusionerzeugung die Menschen zum Erfinden verf\"{u}hre und dadurch gesamtgesellschaftlichen Nutzen bringe.

W\"{a}hrend solche Theorien nicht unbedingt einer Pr\"{u}fung standhalten, ist der propagandistische Wert des Mythos vom ``kleinen Erfinder'' nicht zu untersch\"{a}tzen.  Er brachte um die Jahrhundertwende in Deutschland eine ``Erfinderromantik''-Bewegung hervor, die sich gegen das volkswirtschaftlich orientierte und ideologieskeptische ``konservative Patentwesen'' wandte und forderte, das Patentwesen als Mittel zur F\"{o}rderung der ``kleinen Erfinder'' und zum Schutz ihres ``geistigen Eigentums'' zu verstehen.  Diese Bewegung verb\"{u}ndete sich in den 30er Jahren mit dem Nationalsozialismus, der eine systematisch am kleinen Erfinder orientierte Patentpolitik betrieb.  Nach dem Krieg schlug sich das Gedankengut der Erfinderromantik im Arbeitnehmer-Erfindergesetz nieder.  \"{A}hnlich wie andere Gewinnspiele wurde Patentwesen zum Tr\"{a}ger weitgehend illusorischer Hoffnungen des kleinen Mannes.

Auf diese Weise konnte es der Patentbewegung gelingen, nicht nur die Juristen sondern auch die Herzen der breiten Masse f\"{u}r sich zu gewinnen.  Ohne die Mythen vom ``geistigen Eigentum'' und vom ``Schutz des kleinen Erfinders'' w\"{a}re der ``Siegeszug der Juristen gegen die Volkswirte'' (Machlup \"{u}ber die Patentbewegung) wohl viel fr\"{u}her an seine Grenzen gesto{\ss}en.
\end{sect}

\begin{sect}{transf}{Mythen um ``Innovation'' und ``Technologietransfer''}
Die meisten der zahlreichen Versammlungen der Patentbewegung beginnen mit motivierenden Reden eines Technologiepolitikers, der das Glaubensbekenntnis der versammelten Gemeinde formuliert und ihm dadurch besondere Legitimit\"{a}t verleiht, dass er es als Nicht-Mitglied dieser Gemeinde (Heide) bekr\"{a}ftigt.

So erkl\"{a}ren\footnote{http://swpat.ffii.org/papers/boch97-reiner/boch97-reiner.de.html} Staatssekret\"{a}re des BMBF\footnote{http://swpat.ffii.org/players/bmbf/swpatbmbf.en.html} regelm\"{a}{\ss}ig, dass eine moderne Forschung und Entwicklung nur dann m\"{o}glich sei, wenn zuvor f\"{u}r einen angemessenen patentrechtlichen Schutz gesorgt worden sei.  Auch \"{u}ber solche Glaubenss\"{a}tze der Patentbewegung machten sich schon Machlup und vor ihm zahlreiche Volkswirte lustig.  Sie sprachen vom ``Patentglauben'' (the patent faith).

Um die Einf\"{u}hrung des Patentwesens an den Hochschulen zu erleichtern, wurde ein weiterer Mythos in die Welt gesetzt und durch offizielle Benennung von diversen staatlichen Stellen in das Bewusstsein der breiten \"{O}ffentlichkeit geschoben:  der Mythos von der N\"{u}tzlichkeit des Patentwesens bei der praktischen Umsetzung wissenschaftlicher Forschungsergebnisse.  Ein ``Technologietransfer'' von der Hochschule in die Wirtschaft ist demnach nur dann zu erwarten, wenn Monopolrechte auf diese Technologie gew\"{a}hrt werden.

Dieser Mythos ist offensichtlich und nachweislich falsch, wird aber dennoch in amtlichen Texten und in der Benennung der Patentstellen an den Hochschulen verewigt.  Durch tausendfache beschw\"{o}rende Wiederholung hat er sich in vielen K\"{o}pfen festgesetzt.

Der Technologietransfer-Mythos tr\"{a}gt Orwellsche Z\"{u}ge.  Ebenso wie das ``Ministerium der Wahrheit'' sich haupts\"{a}chlich mit Geschichtsf\"{a}lschung besch\"{a}ftigt, ist es Hauptaufgabe der ``Technologietransferstellen'', Monopole zu errichten, um die Verbreitung n\"{u}tzlicher Technologien zu behindern.
\end{sect}

\begin{sect}{fund}{Zeitgeist, Privatisierungsreflex, Marktfundamentalismus}
Seit den 80er Jahren erf\"{a}hrt die Patentbewegung eine betr\"{a}chtliche R\"{u}ckenst\"{a}rkung durch den Zeitgeist.  W\"{a}hrend in den 50er Jahren eine unkritische Verstaatlichung aller Branchen betrieben wurde, hat sich in den 80er Jahren ein ebenso unkritischer Privatisierungsreflex durchgesetzt.  Zu seiner St\"{a}rkung trug die Zahlungsunf\"{a}higkeit der westlichen Staaten ebenso wie der Bankrott des Sowjetsozialismus bei.  Hinzu kam das Bed\"{u}rfnis, heimische industrielle Besitzst\"{a}nde gegen die Konkurrenz schnell aufholender fern\"{o}stlicher Empork\"{o}mmlinge zu sch\"{u}tzen.  Nicht umsonst erlebte das Patentwesen gerade in dieser Phase eine st\"{u}rmische Expansion, w\"{a}hrend es vorher Jahrzehnte lang eher ein Schattendasein gef\"{u}hrt hatte.

Der Zeitgeist der 1980er Jahre beargw\"{o}hnt alle staatliche F\"{o}rderung.  Sowohl Wirtschaftsliberale als auch viele Kinder der 1968er Bewegung sind sich einig, dass eine staatliche Finanzierung der ``Elitekultur'', zu der auch die Grundlagenforschung und universit\"{a}re Wissenschaft geh\"{o}ren, ``in der Demokratie nicht konsensf\"{a}hig'' sei, nach ``Obrigkeitsstaat'' rieche und ``nicht vom ganzen Volk sondern nur von den direkten Nutznie{\ss}ern finanziert werden sollte''.  Dass bei informationellen Gemeing\"{u}tern die Nutznie{\ss}er nicht dingfest zu machen sind, f\"{a}llt dabei zun\"{a}chst nicht auf.  Das Urteil des Marktes, so blind und inkompetent er im Hinblick auf Informationsg\"{u}ter auch sein mag, bleibt als einziger Wertma{\ss}stab unbestritten stehen.  Einige Leute haben dieses neue Wertsystem verinnerlicht und systematisiert.  Hieraus ergibt sich der ``Marktfundamentalismus'', gegen den Ende der 1990er Jahre wiederum einige Stimmen wie die des wohlt\"{a}tigen B\"{o}rsenmoguls George Soros laut wurden.  Der Marktfundamentalismus m\"{o}chte das Gemeinwesen m\"{o}glichst weit zur\"{u}ckdr\"{a}ngen und die gesamte Gesellschaft ausschlie{\ss}lich auf den Prinzipien von ``Eigentum'' und Austausch aufbauen.  Dies erfordert, dass f\"{u}r jedes Gut, dem irgendein Wert zuerkannt wird, ein Eigentumsrecht zu schaffen ist.

Die letztendliche Folge m\"{u}sste dann sein, dass selbstverst\"{a}ndlich die gesamte informatische und mathematische Grundlagenforschung und letztlich alle Geisteswissenschaften und K\"{u}nste auf Patente aufgebaut werden.  Ihnen bliebe vielleicht auch wenig anderes \"{u}brig, denn ohne jeden ``Schutz'' werden sie in einem System, in dem die Aufwendung von Steuermitteln f\"{u}r ``elit\"{a}re'' Zwecke nicht vorgesehen ist, unter dem ``Diktat des Marktes'' an den Rand gedr\"{a}ngt.  Zumindest in einigen F\"{a}llen, wie etwa im Falle der MP3-Patente der Fraunhofer-Gesellschaft, gelang es, betr\"{a}chtliche Lizenzeinnahmen durch informatische Patente zu erzielen.  Auf der anderen Seite sind Forschung und Lehre von der schmerzenden Wirkung der Patentmonopole nicht direkt betroffen.  Da kann geradezu ein Gef\"{u}hl der Revanche gegen\"{u}ber der sonst vom Zeitgeist so einseitig bevorzugten Privatwirtschaft aufkommen.  So wurde ein erweitertes Patentwesen zum Hoffnungstr\"{a}ger eines Teils der Wissenschaft.

Dieser Zeitgeist gab der Patentbewegung starken Auftrieb.  Er erm\"{o}glichte nicht nur ihre Expansion aus dem Bereich der angewandten Naturwissenschaften in die grenzenlose Sph\"{a}re der praktischen und wiederholbaren Probleml\"{o}sungen\footnote{http://swpat.ffii.org/papers/jwip-schar98/jwip-schar98.en.html}.  Er f\"{u}hrte auch zu einem Kampf um die Freiheit der Hochschulprofessoren\footnote{http://swpat.ffii.org/players/n014/swnbmbf014.de.html}, die, wenn es nach dem Willen der Patentbewegung geht, das Recht verlieren sollen, ihre Leistungen nach der Ethik eines Benjamin Franklin der \"{O}ffentlichkeit zur Verf\"{u}gung zu stellen.
\end{sect}

\begin{sect}{soc}{Hohepriester, Geheimlehren und Riten der Patentbewegung}
Ein Patentanwalt dr\"{u}ckte auf dem FFII-Diskussionsverteiler seinen Unmut dar\"{u}ber aus, dass zahlreiche Softwareentwickler begonnen haben, sich in Angelegenheiten einzumischen, von denen sie nichts verstehen.  Kompetent, um \"{u}ber den Nutzen von Softwarepatenten zu urteilen, sind nach Meinung dieses Gemeindemitgliedes nur die anerkannten Gurus seiner Gemeinde.  Das sind einerseits Rechtswissenschaftler, andererseits die Leiter der Patentabteilungen erfolgreicher Unternehmen.

Innerhalb der Patentbewegung herrscht ein starker Konformit\"{a}tsdruck, den man in geringerem Ma{\ss}e auch in einigen anderen Bereichen des Juristenstandes kennt.  Doktoranden sind in hohem Ma{\ss}e vom Wohlwollen der Gemeinde abh\"{a}ngig, und abweichende Meinungen werden ungn\"{a}dig geahndet.  Einige mit uns sympathisierende Patentjuristen halten oder hielten ihre Meinungen lange Zeit zur\"{u}ck, um ihre Karriere nicht zu gef\"{a}hrden.

Die Kehrseite ist, dass \"{u}ber einige Fragen ein interessenbedingter Konsens besteht, \"{u}ber den nicht kritisch diskutiert werden kann.  So kommen Argumentationslinien zustande, die f\"{u}r Au{\ss}enstehende unbegreiflich erscheinen.  Man vergegenw\"{a}rtige sich die l\"{o}cherige Argumentation von h\"{o}chsten Richtern wie Mark Schar\footnote{http://swpat.ffii.org/papers/jwip-schar98/jwip-schar98.en.html} und Klaus Melullis\footnote{http://swpat.ffii.org/papers/grur-mellu98/grur-mellu98.de.html} so wie die Schlussbemerkungen im kritischen Artikel eines Patentpr\"{u}fers, der als ehemaliger Programmierer zwischen zwei Welten zu wandern versucht:

\begin{quote}
Gleichfalls m\"{o}glich ist aber auch, dass keine der Seiten die andere mehr verstehen kann, da beide von unvereinbaren Grunds\"{a}tzen ausgehen. Ein Indiz f\"{u}r letzteres mag folgende Episode sein: Bei dem hilflosen Versuch des Autors einem befreundeten Informatiker zu erkl\"{a}ren, wieso Programme als solche nicht patentierbar w\"{a}ren, programmbezogene Lehren mit einem zus\"{a}tzlichen technischen Effekt jedoch schon, erkl\"{a}rte dieser unverbl\"{u}mt: ``Ihr spinnt's ja komplett!''. Diese deutliche Aussprache hat dem Autor doch einiges zu Denken gegeben.
\end{quote}

Die Patentbewegung ist ein Kreis, der schon durch seine esoterische Begrifflichkeiten sowohl Volkswirte als auch Technologen ausschlie{\ss}t und gleichzeitig dank guter finanzieller Ausstattung sich in zahlreichen Verb\"{a}nden organisiert hat, die regelm\"{a}{\ss}ig gro{\ss}e Kongresse veranstalten.  Fast jeder Kongress beginnt mit Festreden, in denen prominente externe G\"{a}ste das Glaubensbekenntnis der Bewegung best\"{a}rken helfen.

Weder die Unternehmen noch die Regierungen verf\"{u}gen bisher \"{u}ber die n\"{o}tigen organisatorischen und geistigen Ressourcen, um der Patentbewegung etwas entgegenzusetzen.
\end{sect}

\begin{sect}{etc}{Weitere Lekt\"{u}re}
Britisches Patentamt will Schulkindern die Lehre vom ``Geistigen Eigentum'' einimpfen.\footnote{http://www.patent.gov.uk/about/press/releases/2001/copyrightcrime.htm}
\begin{itemize}
\item
UK plans mandatory IP indoctrination for children\footnote{http://www.cluebot.com/article.pl?sid=01/06/05/2338246} (Weitere Diskussionen und Verweise auf Cluebot.com)

\item
http://www.intellectual-property.gov.uk\footnote{http://www.intellectual-property.gov.uk} (Portal der Britischen Regierung f\"{u}r den Kult des Geistigen Eigentums)

\item
UKPTO to extend IP cult to schools\footnote{http://aful.org/wws/arc/patents/2001-06/msg00004.html} (Diskussion auf Patents @ AFUL.ORG)
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

