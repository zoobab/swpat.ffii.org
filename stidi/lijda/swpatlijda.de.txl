<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Die Patentbewegung

#descr: Das Patentwesen hat eine mächtige weltliche Religion erzeugt.  Die Anhänger der Patentbewegung sind durch ein gemeinsames Glaubensbekenntnis vereinigt und mit regelsetzender Gewalt in allen wichtigen Organisationen -- von Patentämtern und Patentgerichten bis zu Regierungen und Großkonzernen -- ausgestattet.  Regierungs- und Konzernchefs sehen das Patentwesen meist als ein nebensächliches Thema an.  Sobald man sie nach ihrer Meinung fragt, übergeben sie sofort dem Leiter ihrer Patentabteilung das Wort.   Sie übersehen, dass es sich dabei um ein bekennendes Mitglied der Patentbewegung handelt.

#VVd: Verfälschte Verbandsmeinungen

#Dat: Der Glaube an das Geistige Eigentum

#Eii: Erfinderromantik und die Kleinbauern-Utopie

#Mvc: Mythen um %(q:Innovation) und %(q:Technologietransfer)

#Zsr: Zeitgeist, Privatisierungsreflex, Marktfundamentalismus

#Had: Hohepriester, Geheimlehren und Riten der Patentbewegung

#Wrk: Weitere Lektüre

#Zei: Zahlreiche Unternehmen haben sich deutlich gegen Patentierbarkeit von Programmlogik ausgesprochen.  Dennoch berichtet die Presse immer wieder davon, die Software-Industrie wolle die Ausweitung des Patentschutzes.  %(sm:FFII-Gespräche mit Siemens-Vertretern) ergaben, dass dies zumindest für Siemens nicht gilt.  Zahlreiche Mitarbeiter und Bereichsleiter von Siemens halten das inflationierte Patentsystem in letzter Zeit für eine unerträgliche Bremse.  Nicht nur verlieren sie selbt viel Zeit mit dem Anmelden von allerlei Kleckerkram zum Patent.  Auch die ins Kraut schießenden Patentverwertungsfirmen, die allerlei Patente zusammenkaufen und dann warten, bis sie eine reiche Firma verklagen können, machen den Siemensianern zu schaffen.  Zudem ist es ein offenes Geheimnis, dass das Patentwesen nur wenig zur Wahrung der unternehmerischen Interessen in Bereichen wie Telekommunikation beiträgt.  Es geht vor allem um den Aufbau von Waffenarsenalen zum Überleben in einem künstlichen System des Wettrüstens.  Dennoch dringen diese Erkenntnisse nicht an die Öffentlichkeit.  Denn nur der Vorstand nimmt öffentlich Stellung.  Und er reicht die Fragen an die Patentabteilung weiter.  Diese wiederum besteht aus treuen Gläubigen einer weltlichen Religion:  der Patentbewegung.

#DWk: Der einzige deutsche Wirtschaftsverband, der sich vor der EU-Konsultation %(zp:öffentlich für die Patentexpansion ausgesprochen) hat, der ZVEI, übernahm dabei lediglich eine Vorlage seines Patentexperten, des Chefs der Patentabteilung von Siemens, %{AK}.  Dies ergaben damals telefonische Nachfragen des FFII.  Das ZVEI-Sekretariat war beunruhigt über unsere Kritik, der ZVEI habe gegen das Interesse der in ihm organisierten kleineren und mittleren Unternehmen der Elektronikindustrie gehandelt.  Es ist %(es:bekannt und durch unverdächtige Studien untermauert), dass für kleine und mittlere Unternehmen selbst im Kerngebiet des Patentwesens, den angewandten Naturwissenschaften, das Patentwesen im allgemeinen kaum positive Beiträge zur Innovationsfreudigkeit leistet.

#Orp: Ähnlich sieht es bei der deutschen Bundesregierung aus.  Im %(bw:Bundesministerium für Wirtschaft und Technologie) wird die Tendenz zur Programmlogik-Patentierung mit einiger Skepsis beobachtet.  Aber federführend ist in Patentfragen das %(bj:Bundesjustizministerium).

#Sen: Selbst innerhalb der Patentbewegung sind Verfälschungen durch das Partikularinteresse der zum jeweiligen Thema befragten Experten zu beobachten.  In der Softwarepatente-Debatte haben sich alle Organisationen dieser Bewegung auf die Seite der Patentmaximalisten geschlagen.  Die kritischen Stimmen waren kaum zu hören.  Einerseits mag dies nicht verwundern, da es zunächst zu einer Expansion des Tätigkeitsfeldes und einem Zuwachs an Bedeutung für den gesamten Berufsstand führt und dem Selbstverständnis der Patentbewegung entgegenkommt.  Andererseits besteht durch Softwarepatente die Gefahr, dass anstößige Patentstreitigkeiten im Internet und in der Presse intensiv beachtet und durchleuchtet werden, so dass die Zahl der Kritiker des Patentwesens wachsen und der gesamte Berufsstand eventuell wegen 3% der derzeit vergebenen Patente in Misskredit geraden könnte.  Es ist bei etwas distanzierter Betrachtung keineswegs sicher, ob die Aufweichung der Patentierbarkeitskriterien der Patentbewegung mehr nützt oder schadet.  Aber die meisten Patentanwälte haben keine Zeit, sich diese Frage zu stellen.  Sie verlassen sich auf die Glaubenssätze der Bewegung und auf das Urteil derjenigen Patentjuristen, die von Softwarepatenten leben.

#Ozr: Ähnlich wie die Standpunkte der Unternehmen vom Gruppeninteresse der Patentbewegung verzerrt werden, unterliegt die Meinungsbildung innerhalb der Patentbewegung offenbar einer Verzerrung durch diejenigen Patentanwälte, die sich auf Software spezialisiert haben.

#EGI: Ein Patentanwalt formulierte den ersten Kernsatz des Glaubensbekenntnisses in einer Diskussion auf dem FFII-Verteiler:  %(q:Ich glaube an das Geistige Eigentum).

#DWW: Dieser Glaube beruht auf Mythenbildung und verleiht der Patentbewegung sowohl ihre Durchsetzungskraft als auch ihre sektiererische Natur.  Er erlaubt es ihr, guten Gewissens Patente auch dort gesetzlich und völkerrechtlich vorzuschreiben, wo diese nicht, wie von der US-Verfassung gefordert %(q:dem Fortschritt der Wissenschaft und der nützlichen Künste dienen).  Fritz Machlup %(fm:berichtet) über die Ursprünge der Mythenbildung um das Geistige Eigentum:

#Dsi: Die Naturrechtstheorie wurde von der französischen Verfassunggebenden Versammlung feierlich übernommen, als sie in der Präambel zum Patentgesetz von 1791 verkündete, daß

#Dcw: Diese Auffassung der französischen Juristen, wonach eine Idee ebenso Gegenstand eines Eigentumsrechts sein könne wie eine körperliche Sache, wurde von vielen Seiten kritisiert, zurückgewiesen und lächerlich gemacht. Man fragte sich, wie das Eigentum an Ideen, wenn es sich um ein %(q:Naturrecht) handele, auf 14 oder 17 Jahre beschränkt werden könne, anstatt es für ewige Zeiten anzuerkennen.

#TdE: Tatsächlich stritten einige Hartnäckige für ein ewiges und unveräußerliches Eigentumsrecht an Ideen. Andere wiesen darauf hin, daß niemand eine Idee ausschließlich besitzen könne, weder für begrenzte noch für unbegrenzte Zeit, sobald er sie mitgeteilt hat und sie demzufolge mit anderen teilt. Die logischen Elemente des Eigentumsbegriffs, wie er auf körperliche Gegenstände Anwendung findet - Inbesitznahme, Besitz, Verfügung, Aneignung, usw. - seien auf Ideen unanwendbar, die sich nicht in materiellen Dingen verkörpern. Wer sich über den »Diebstahl« an seiner Idee beschwert, %(q:gibt etwas als entwendet an, das er noch besitzt, und er wünscht etwas zurück, das ihm, tausendfältig zurückerstattet, durchaus nicht mehr nützt, als der einmalige bereits vorhandene Besitz.)

#Iin: Im Gegensatz zum Eigentum an Sachen bedeutet das sogenannte geistige Eigentum weder die Verfügbarkeit über einen Gegenstand noch über eine Idee, sondern vielmehr die Beherrschung des Marktes für die Gegenstände, in denen sich die Idee verkörpert. Ein körperlicher Gegenstand gehört jemandem, der bestimmen kann, wie er zu benutzen ist; es wäre notwendig, ihn seinem Besitzer wegzunehmen, ehe ein anderer ihn benutzen kann. Im Gegensatz dazu kann eine Idee einer unbeschränkten Anzahl von Personen gehören und ihre Benutzung durch einige verhindert nicht ihre Verwendung durch andere. Es ist interessant, daß einige französische Juristen zugaben, daß sie es hauptsächlich aus Gründen der Propaganda vorzogen, von %(q:natürlichen Eigentumsrechten) zu sprechen, besonders deswegen, weil einige der sonst zur Verfügung stehenden Ausdrücke, wie %(q:Monopolrecht) oder %(q:Privileg), gar so unbeliebt waren.

#Dol: Der Propagandamythos vom %(q:Geistigen Eigentum) wird auch heute noch eifrig kultiviert, und für ihn werden Staaten und internationale Organisationen eingespannt.  Hier seien einige Beispiele genannt:

#Dtr: Das %(in:französische Patentamt) nennt sich %(q:Institut pour la Propriété Intellectuelle).

#Dmt: Das %(ig:Schweizer Patenamt) nennt sich %(q:Insitut für Geistiges Eigentum).

#Dtr2: Die Patentorganisation der Vereinten Nationen nennt sich %(q:Weltorganisation für geistiges Eigentum)

#Der: Das Patentamt der VR China wurde kürzlich in %(gz:Staatsamt für Geistiges Eigentum) umbenannt.

#INc: Im Herbst 2000 beschlossen die Vereinten Nationen auf Anregung chinesischer und algerischer Patentjuristen, den 26. April jedes Jahres als %(q:Tag des Geistigen Eigentums) zu feiern.

#Ign: Im Frühjahr 2001 startete das Britische Patentamt gemeinsam mit dem Kultusministerium eine Kampagne, um Kindern von Klein auf das Evangelium des %(q:Geistigen Eigentums) nahezubringen.

#Dr0: Die Idee vom %(q:Geistigen Eigentum) spricht zutiefst menschliche Sehnsüchte an:

#IiW: Ich sollte ebenso abkassieren dürfen, wie die Reichen und Mächtigen, deren Privatleben ich täglich in der Boulevardpresse verfolge.

#Ifr: Ich bin ein verkanntes Genie und mithilfe des Patentwesens werde ich meinen gerechten Platz in der ersten Liga finden.

#Iku: In dieser Welt sollte es keine komplizierten Verteilungshierarchien und keine Politik geben.  Ich will nur meinen privaten Acker pflügen müssen und die volle Wertschöpfung sollte sich dann ganz von selbst einstellen.

#DmB: Das Patentwesen eröffnet einen Weg der vertikalen Mobilität.  Es bietet dem %(q:kleinen Erfinder) die Aussicht, mit einem Satz über alle Mächte und Barrieren dieser Welt hinweg senkrecht in die erste Liga hoch zu springen.

#Ebe: Es gibt in der Menschheitsgeschichte immer wieder großangelegte Gewinnspiele, die sich änhlicher Beliebtheit erfreuten.  Das System der jährlichen Beamtenprüfungen im kaiserlichen China erlaubte den direkten Sprung vom %(q:kalten Fenster) des Schriftgelehrten zu den höchsten Ämtern und Würden.  Gelang dieser Sprung, so %(q:stiegen die Hühner und Hunde des Hofes mit einem Satz in den Himmel auf).  Dieses System hielt sich über fast 2000 Jahre hinweg und überlebte noch lange, nachdem es zu einem fast lotto-artigen Gewinnspiel degeneriert war.

#nWn: Gewinnpiele haben den unschätzbaren Vorteil, dass ein objektiver Mechanismus darüber entscheidet, wer gewinnt.  Undurchsichtige Verteilungsmechanismen und Mobilitätsbarrieren werden ausgeschaltet.  Die Politik wird abgeschafft.  Der Kleinbauer kann seine Scholle beackern, ohne sich mit der oft unappetitlichen Last des gesellschaftlichen Lebens herumplagen zu müssen.

#SvW: Selbst seriöse Wirtschaftswissenschaftler haben bisweilen angeführt, dass ein besonderer Verdienst und vielleicht sogar der eigentliche Sinn des Patentwesen darin liege, dass es durch eine Art großangelegte Illusionerzeugung die Menschen zum Erfinden verführe und dadurch gesamtgesellschaftlichen Nutzen bringe.

#Wsm: Während solche Theorien nicht unbedingt einer Prüfung standhalten, ist der propagandistische Wert des Mythos vom %(q:kleinen Erfinder) nicht zu unterschätzen.  Er brachte um die Jahrhundertwende in Deutschland eine %(q:Erfinderromantik)-Bewegung hervor, die sich gegen das volkswirtschaftlich orientierte und ideologieskeptische %(q:konservative Patentwesen) wandte und forderte, das Patentwesen als Mittel zur Förderung der %(q:kleinen Erfinder) und zum Schutz ihres %(q:geistigen Eigentums) zu verstehen.  Diese Bewegung verbündete sich in den 30er Jahren mit dem Nationalsozialismus, der eine systematisch am kleinen Erfinder orientierte Patentpolitik betrieb.  Nach dem Krieg schlug sich das Gedankengut der Erfinderromantik im Arbeitnehmer-Erfindergesetz nieder.  Ähnlich wie andere Gewinnspiele wurde Patentwesen zum Träger weitgehend illusorischer Hoffnungen des kleinen Mannes.

#AKS: Auf diese Weise konnte es der Patentbewegung gelingen, nicht nur die Juristen sondern auch die Herzen der breiten Masse für sich zu gewinnen.  Ohne die Mythen vom %(q:geistigen Eigentum) und vom %(q:Schutz des kleinen Erfinders) wäre der %(q:Siegeszug der Juristen gegen die Volkswirte) (Machlup über die Patentbewegung) wohl viel früher an seine Grenzen gestoßen.

#DTc: Die meisten der zahlreichen Versammlungen der Patentbewegung beginnen mit motivierenden Reden eines Technologiepolitikers, der das Glaubensbekenntnis der versammelten Gemeinde formuliert und ihm dadurch besondere Legitimität verleiht, dass er es als Nicht-Mitglied dieser Gemeinde (Heide) bekräftigt.

#Soa: So %(br:erklären) Staatssekretäre des %(bf:BMBF) regelmäßig, dass eine moderne Forschung und Entwicklung nur dann möglich sei, wenn zuvor für einen angemessenen patentrechtlichen Schutz gesorgt worden sei.  Auch über solche Glaubenssätze der Patentbewegung machten sich schon Machlup und vor ihm zahlreiche Volkswirte lustig.  Sie sprachen vom %(q:Patentglauben) (the patent faith).

#UWu: Um die Einführung des Patentwesens an den Hochschulen zu erleichtern, wurde ein weiterer Mythos in die Welt gesetzt und durch offizielle Benennung von diversen staatlichen Stellen in das Bewusstsein der breiten Öffentlichkeit geschoben:  der Mythos von der Nützlichkeit des Patentwesens bei der praktischen Umsetzung wissenschaftlicher Forschungsergebnisse.  Ein %(q:Technologietransfer) von der Hochschule in die Wirtschaft ist demnach nur dann zu erwarten, wenn Monopolrechte auf diese Technologie gewährt werden.

#Diu: Dieser Mythos ist offensichtlich und nachweislich falsch, wird aber dennoch in amtlichen Texten und in der Benennung der Patentstellen an den Hochschulen verewigt.  Durch tausendfache beschwörende Wiederholung hat er sich in vielen Köpfen festgesetzt.

#Dge: Der Technologietransfer-Mythos trägt Orwellsche Züge.  Ebenso wie das %(q:Ministerium der Wahrheit) sich hauptsächlich mit Geschichtsfälschung beschäftigt, ist es Hauptaufgabe der %(q:Technologietransferstellen), Monopole zu errichten, um die Verbreitung nützlicher Technologien zu behindern.

#Sic: Seit den 80er Jahren erfährt die Patentbewegung eine beträchtliche Rückenstärkung durch den Zeitgeist.  Während in den 50er Jahren eine unkritische Verstaatlichung aller Branchen betrieben wurde, hat sich in den 80er Jahren ein ebenso unkritischer Privatisierungsreflex durchgesetzt.  Zu seiner Stärkung trug die Zahlungsunfähigkeit der westlichen Staaten ebenso wie der Bankrott des Sowjetsozialismus bei.  Hinzu kam das Bedürfnis, heimische industrielle Besitzstände gegen die Konkurrenz schnell aufholender fernöstlicher Emporkömmlinge zu schützen.  Nicht umsonst erlebte das Patentwesen gerade in dieser Phase eine stürmische Expansion, während es vorher Jahrzehnte lang eher ein Schattendasein geführt hatte.

#Dee: Der Zeitgeist der 1980er Jahre beargwöhnt alle staatliche Förderung.  Sowohl Wirtschaftsliberale als auch viele Kinder der 1968er Bewegung sind sich einig, dass eine staatliche Finanzierung der %(q:Elitekultur), zu der auch die Grundlagenforschung und universitäre Wissenschaft gehören, %(q:in der Demokratie nicht konsensfähig) sei, nach %(q:Obrigkeitsstaat) rieche und %(q:nicht vom ganzen Volk sondern nur von den direkten Nutznießern finanziert werden sollte).  Dass bei informationellen Gemeingütern die Nutznießer nicht dingfest zu machen sind, fällt dabei zunächst nicht auf.  Das Urteil des Marktes, so blind und inkompetent er im Hinblick auf Informationsgüter auch sein mag, bleibt als einziger Wertmaßstab unbestritten stehen.  Einige Leute haben dieses neue Wertsystem verinnerlicht und systematisiert.  Hieraus ergibt sich der %(q:Marktfundamentalismus), gegen den Ende der 1990er Jahre wiederum einige Stimmen wie die des wohltätigen Börsenmoguls George Soros laut wurden.  Der Marktfundamentalismus möchte das Gemeinwesen möglichst weit zurückdrängen und die gesamte Gesellschaft ausschließlich auf den Prinzipien von %(q:Eigentum) und Austausch aufbauen.  Dies erfordert, dass für jedes Gut, dem irgendein Wert zuerkannt wird, ein Eigentumsrecht zu schaffen ist.

#Dnv: Die letztendliche Folge müsste dann sein, dass selbstverständlich die gesamte informatische und mathematische Grundlagenforschung und letztlich alle Geisteswissenschaften und Künste auf Patente aufgebaut werden.  Ihnen bliebe vielleicht auch wenig anderes übrig, denn ohne jeden %(q:Schutz) werden sie in einem System, in dem die Aufwendung von Steuermitteln für %(q:elitäre) Zwecke nicht vorgesehen ist, unter dem %(q:Diktat des Marktes) an den Rand gedrängt.  Zumindest in einigen Fällen, wie etwa im Falle der MP3-Patente der Fraunhofer-Gesellschaft, gelang es, beträchtliche Lizenzeinnahmen durch informatische Patente zu erzielen.  Auf der anderen Seite sind Forschung und Lehre von der schmerzenden Wirkung der Patentmonopole nicht direkt betroffen.  Da kann geradezu ein Gefühl der Revanche gegenüber der sonst vom Zeitgeist so einseitig bevorzugten Privatwirtschaft aufkommen.  So wurde ein erweitertes Patentwesen zum Hoffnungsträger eines Teils der Wissenschaft.

#Dnr: Dieser Zeitgeist gab der Patentbewegung starken Auftrieb.  Er ermöglichte nicht nur ihre Expansion aus dem Bereich der angewandten Naturwissenschaften in die grenzenlose Sphäre der %(ms:praktischen und wiederholbaren Problemlösungen).  Er führte auch zu einem %(kf:Kampf um die Freiheit der Hochschulprofessoren), die, wenn es nach dem Willen der Patentbewegung geht, das Recht verlieren sollen, ihre Leistungen nach der Ethik eines Benjamin Franklin der Öffentlichkeit zur Verfügung zu stellen.

#Enu: Ein Patentanwalt drückte auf dem FFII-Diskussionsverteiler seinen Unmut darüber aus, dass zahlreiche Softwareentwickler begonnen haben, sich in Angelegenheiten einzumischen, von denen sie nichts verstehen.  Kompetent, um über den Nutzen von Softwarepatenten zu urteilen, sind nach Meinung dieses Gemeindemitgliedes nur die anerkannten Gurus seiner Gemeinde.  Das sind einerseits Rechtswissenschaftler, andererseits die Leiter der Patentabteilungen erfolgreicher Unternehmen.

#ItW: Innerhalb der Patentbewegung herrscht ein starker Konformitätsdruck, den man in geringerem Maße auch in einigen anderen Bereichen des Juristenstandes kennt.  Doktoranden sind in hohem Maße vom Wohlwollen der Gemeinde abhängig, und abweichende Meinungen werden ungnädig geahndet.  Einige mit uns sympathisierende Patentjuristen halten oder hielten ihre Meinungen lange Zeit zurück, um ihre Karriere nicht zu gefährden.

#DWg: Die Kehrseite ist, dass über einige Fragen ein interessenbedingter Konsens besteht, über den nicht kritisch diskutiert werden kann.  So kommen Argumentationslinien zustande, die für Außenstehende unbegreiflich erscheinen.  Man vergegenwärtige sich die löcherige Argumentation von höchsten Richtern wie %(ms:Mark Schar) und %(km:Klaus Melullis) so wie die Schlussbemerkungen im %(kk:kritischen Artikel eines Patentprüfers), der als ehemaliger Programmierer zwischen zwei Welten zu wandern versucht:

#Gsm: Gleichfalls möglich ist aber auch, dass keine der Seiten die andere mehr verstehen kann, da beide von unvereinbaren Grundsätzen ausgehen. Ein Indiz für letzteres mag folgende Episode sein: Bei dem hilflosen Versuch des Autors einem befreundeten Informatiker zu erklären, wieso Programme als solche nicht patentierbar wären, programmbezogene Lehren mit einem zusätzlichen technischen Effekt jedoch schon, erklärte dieser unverblümt: %(q:Ihr spinnt's ja komplett!). Diese deutliche Aussprache hat dem Autor doch einiges zu Denken gegeben.

#Dut: Die Patentbewegung ist ein Kreis, der schon durch seine esoterische Begrifflichkeiten sowohl Volkswirte als auch Technologen ausschließt und gleichzeitig dank guter finanzieller Ausstattung sich in zahlreichen Verbänden organisiert hat, die regelmäßig große Kongresse veranstalten.  Fast jeder Kongress beginnt mit Festreden, in denen prominente externe Gäste das Glaubensbekenntnis der Bewegung bestärken helfen.

#Whs: Weder die Unternehmen noch die Regierungen verfügen bisher über die nötigen organisatorischen und geistigen Ressourcen, um der Patentbewegung etwas entgegenzusetzen.

#Bki: Britisches Patentamt will Schulkindern die Lehre vom %(q:Geistigen Eigentum) einimpfen.

#Uyo: UK plans mandatory IP indoctrination for children

#Wkn: Weitere Diskussionen und Verweise auf Cluebot.com

#PRW: Portal der Britischen Regierung für den Kult des Geistigen Eigentums

#DaW: Diskussion auf Patents @ AFUL.ORG

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlijda ;
# txtlang: de ;
# multlin: t ;
# End: ;

