<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Software Patentability with Compensatory Regulation: a Cost Evaluation

#descr: Europe is preparing major changes in its patent system.  The European Patent Office (EPO) has proposed to remove limitations on patentability, such as the exclusion of computer programs in Art. 52 of the European Patent Convention (EPC).  A report by the French Academy of Technologies supports this proposal but suggests additional regulation measures in order to reduce potential abuses of software patents.  In this article, we try to assess the costs of such compensatory regulation.  They add up to an estimated 1-5 billion EUR per year for the European Union.  Various regulation approaches and cheaper legislative approaches are compared in this article.

#Blr: Bibliographical Reference for this Article

#JWg: Jean Paul Smets and Hartmut Pilch: %(q:Software Patentability with Compensatory Regulation: a Cost Evaluation), published in %{UM}, vol II, issue no. 6, December 2001, also see online version at %{URL}

#SWW: A Drive for Patenting Logical Functionalities in Europe

#PWn: Patenting Logical Functionalities under the European Patent Convention

#Stu: Software Patents with Compensatory Regulation: System Goals

#SCo: Software Patents with Compensatory Regulation: Measures and Costs

#OWo: Other Approaches to Reducing Risks of Software Patentability

#Wsh: What is so special about Software?

#Ccs: Conclusion

#EWe: Europe is preparing major changes in its patent system.  Among %(bp:proposed changes), the European Patent Office (EPO) has asked for a removal of the exclusion of computer programs in Art 52(2) EPC together with all other explicit exceptions to patentability (intellectual methods, business methods, mathematical methods etc), in order to put the written Law in line with its recent policy of granting patents for all %(pr:practical and repeatable problem solutions), so as to %(e:clarify) the legal status of %(sp:approximately 30000 software patents granted under this policy) and to %(e:harmonise) the practise of national jurisdictions, some of which have been reluctant to join the EPO on its quest for new frontiers of patentability.

#MWe: Multinational companies in the fields of telecommunications (e.g. Alcatel, Siemens), computer electronics (ex. Thomson Multimedia), Aerospace &  Defense (ex. Dassault, Matra, Thales), software (e.g. IBM) have been continuously lobbying European governments in order to get software patents fully legalised in Europe.  The European Patent Office, most national patent offices as well as industrial property professionals are %(el:equally lobbying European governments).

#Pvt: Plans to legalise software patents in Europe have met opposition from various groups. 300 mostly small software companies from the Eurolinux Alliance have raised more than 90.000 signatures through an %(ep:electronic petition).  %(pp:Most political parties in Europe have taken public positions against software patents.) %(pa:In some countries, parliaments have blocked governments from taking pro-patent positions within international bodies).

#Oot: On 2001-03-14, at a parliamentary hearing a majority of coalition and opposition party MPs instructed the Dutch secretary of state of economic affairs to:

#TNd: The test criteria were to be drafted by FENIT, the dutch IT industry organisation and VOSN, the dutch Open Source organisation.

#Opu: On 26th July 2001, the proposed criteria were %(vf:published) by FENIT and VOSN:

#dtf: Distinguish between technical and non technical problem solutions in terms of %(q:planned use of controllable natural forces to directly achieve a causally oversseable effect).

#ipt: If programs are part of an invention: publish the source code along with the claim, also the program should work.

#rom: As far as programs are concerned, the patent protects only the implementation submitted in (2).

#ten: The claim should be accompanied by a proof that an experiment (again defined in terms of natural forces) was necessary to arrive at the solution and that it was actually carried out.

#aui: Create a suitable incentive structure for preventing undesirable patents, avoid that rejecting is more expensive than granting etc

#Mto: %(ms:Multiple government-sponsored studies have been conducted in Europe to assess the economic and legal impact of software patents). Reports written from an economic perspective tend to conclude that the introduction of patents in the software economy would either generate no noticeable impact or lead to less innovation and competition. Reports written from a patent law perspective tend to conclude that there should be no explicit limitation of patentability and that the European practice should come in line with that of the United States of America. Some reports (IPI and the 2 German studies) combine both conclusions: software patents probably have a negative economic impact but should be legalised. Three reports (DG-MKT, IPR Helpdesk, Fraunhofer) include a review of opinion among software companies which shows that an overwhelming majority (80% to 90%) of software companies is happy with copyright and more or less hostile to software patents, with the noticeable exception of large software corporations (ex. IBM, Microsoft).  Only %(pi:one report) explores alternative scenarios to the patentability of software: full exclusion, reduced enforcability, sui generis rights.

#AqW: Among recent emerging solutions to the ongoing debate, some groups such as the French Academiy of Technologies have %(aa:proposed) to quickly and fully legalise the patenting of %(q:computer-implementable inventions) (logical functionalities) and at the same time adopt compensatory regulation in order to reduce the potential abuses of such patents. The purpose of this article is to assess the financial costs of this approach and compare it with other approaches.

#Mow: Most people believe that software patents relate to software.  Most people also believe that software patents are meant for software developers who wish to protect their software from plagiarism.

#TWo: These two common ideas are quite wrong.  Software patents are granted to people who do not necessarily develop and publish software.  Also, software patents do not protect software authors against imitation

#Aee: An explanation of this feature of software patents can be found in Chapter 3 of the %(ss:Conseil Général des Mines) report.

#CgW: Copyright vs Patents

#Dpd: Description & Claims

#Sre: Describing Logical Ideas and Claiming Physical Objects

#SWW2: Software patents in Europe

#Bts: Business method patents in Europe

#Tce: The debate about software copyright vs. software patents is not a debate about whether the programmer should be entitled to control the use of his intellectual achievement but about where this achievement lies -- in the functionalities or in their creative combination into a complex work -- and how it can be protected in such a way that the protection does not defeat itself.

#Atr: For example, what the reader is currently reading is a textual composition of concepts, such as argumentation chains and rhetorical forms. Much effort went both into designing these concepts and combining them into a structured work. Copyright protects the orginal combination of concepts which defines this work: we hereby grant the reader a license to produce integral facsimile copies of this article but we forbid plagiarism or reusing parts of this article without permission. Copyright does not protect however the innovative or less-innovative concepts which this article in based on: readers are free to write original articles based on the same argumentation chains or rhetorical forms. The main argument for not granting monopolies on concepts or ideas in our societies is: promoting creation.  Copyright would actually become quite useless if authors had to ask permission to hundreds of concept owners each time they wanted to create and publish an original work.  Protecting ideas and concepts would just act as a barrier to creation.

#Trc: Just like this article, a computer program is also a textual composition of concepts. Rather than argumentation or rhetorics, computer programs are based on logical functionalities.  Software copyright protects the orginal combination of logical functionalities but not the logical functionalities per se.  Advocates of software patents %(gi:suggest) that designing a functionality is the important part of a computer program, the rest consisting mainly of easy %(q:coding). Opponents to software patents on the contrary %(fe:argue) that logical functionalities tend to be fairly easy to design, whereas the bulk of the skill and sweat of a programmer goes into weaving these logical functionalities together into a well-formed %(ts:tissue).  In either case, subjecting software to both patents and copyright means allowing the lesser achievement to stand in the way of the greater one, thereby risking to stifle innovation rather than promote it.

#Tne: This is regularly suggested not only by patent lawyers but also by some representatives of universitarian informatic research, such as the president of the German %{GI} and his colleague Prof. Endres as well as some people who have transformed university research projects into private software patent licensing businesses.

#Tqh: In software, %(q:reinventing is commonplace), because devising an algorithm is usually easier than analysing someone else's work -- patent advocates %(q:overemphasize inventing), it is %(lp:argued).  A former Microsoft system architect and current day IT consulting guru %(jc:exhorts investors) to discard the widespread %(q:building-the-better-mousetrap belief): The key to success in software business is not in getting one's hand on great ideas, but in building a team of talented programmers who will know by themselves how to %(q:convert capital into software that works).

#Tmk: The latter view is shared and explained in detail by the German Federal Supereme Court's %(bs:Betriebssystem) decision of 1990, which also states that operating systems are not technical inventions, because they make use of known physical devices within a framework of logics which is already predefined by these devices.

#Its: In order to understand what a software patent is, one should first understand how patents work. A patent contains two important parts:

#Dcp: Description

#Tir: This part teaches the person skilled in the art how to rework the invention.  For example it may disclose how to bring about a certain chemical reaction (process) and obtain a certain result, e.g. a molecule (product).

#Cam: Claims

#Tiv: This part tells the citizen, usually a commercial competitor, what kind of things he may not do.  The claim section typically consists of a main claim, which describes the inventive teaching in its most pure form, and a series of %(q:dependent claims).  The main claim usually refer to a process, such as a way to bring about a particular reaction.  Some of the dependent claims may refer to products, such as the molecule produced by means of the reaction.  Dependent claims are more or less redundant.  They do not add much new teaching, but rather specify particular objects which can be monopolised based on the teaching of the main claim.  This redundancy can serve the patentee as a line of retreat in case his claims are challenged by prior art and need to be narrowed down.

#Cnt: Claims are the most important part of a patent since they define how the patent may be enforced in case of dispute. For example, new applications of an existing patented molecule may be claimed in a new patent by another inventor. The owner of the initial patent may produce the patented molecule but will need a license in order to be allowed to use it for his newly patented application.

#Oen: One should note that patentable inventions in chemistry, mechanics etc. may contain in their description some steps which are achieved under program control. %(s:Such inventions are not considered here to refer to software innovations), as long as the invention-relevant problem solution involves physical causality and not merely logical functionality.

#PpW: Patenting software innovation often leads to patents where the teaching in the description and the claimed objects are only loosely related to each other.

#tWb: the teaching may consist of a series of steps to logically transform numbers, data or other schematic entities

#amc: algorithm, functionality, mathematical function, data format, communication protocol, language

#tng: the claims may refer to %(q:methods) (ex. image display, medical diagnosis, ressource allocation, fuel saving, steel cutting, oil drilling), %(q:systems) (ex. programmed computer, integrated circuit, telephone, missile), %(q:computer program products) (e.g software on disk or offered for download), or even computer programs as text structures independent of storage media.

#Wep: While the description and the main claim often disclose an abstract %(e:rule of organisation and calculation), the dependent claims may describe a wide range of concrete objects or processes.  Usually the description will use terminology of microelectronics (ex. %(q:associative memory)) or %(q:software engineering) (ex. %(q:database)) rather than their mathematical equivalents (e.g. %(q:indexed set)) or their business administration equivalents (e.g. %(q:directory book)).  In practise this does not restrict the application of the patent claims, because software is the standard way of putting program and business logic to work, and the metaphorical language of %(q:software engineering) is an equivalent of mathematical language.  Where this still is not enough to cover a competitor's similar solution, patent courts may further enlarge the claim scope by applying the %(q:doctrine of equivalence).

#Tmi: Thus, software patents on the description side teach us nothing but intellectual methods (logical functionalities, rules of organisation and calculation), while on the claim side they may succeed in monopolising a broad range of material objects and processes.

#Tor: The legal rules of patentability in Europe are quite simple. The European Patent Convention (EPC), which defines substantive patent law in Europe, contains explicit exclusions to patentability in the field of software, mathematics and other abstract innovations. The EPC does not however exclude inventions which make use of software, mathematics, etc.  The drafters of the EPC convention had in mind the fact that technical inventions (e.g. in chemistry, mechanics etc) which imply the use of a computer should be patentable as long as it teaches us a new relation of physical causality rather than of logical functionality.  This approach was clearly explained in the EPO's %(gl:Examination Guidelines of 1978), written shortly after the EPC went into force and the EPO was founded:

#Aso: Several of the exclusions in this list comprise abstractions as well as materialisations thereof (... a computer program as an abstract entity or defined in terms of a process for operating a computer or as a record, e.g. on magnetic tape). ...  A computer program may take various forms, e.g. an algorithm, a flow-chart or a series of coded instructions which can be recorded on a tape or other machine-readable record-medium, and can be regarded as a particular case of either a mathematical method (see above) or a presentation or information (see below).  If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims.  For example, a claim to a computer characterised by having the particular program stored in its memory or to a process for operating a computer under control of the program would be as objectionable as a claim to the program %(e:per se) or the program when recorded on magnetic tape.

#TrW: The EPO has never had a definition of what is %(q:technical).  The question was treated as a matter of examiners' intuition.  %(q:It is hard to define what is a camel, but when you see it you know it) was one common excuse for this lack of rigor.  Yet there seemed to be a reliable consensus of customary law on what is technical.  German courts were not satisfied with intution and created a definition, which has been in use for 30 years now: %(bc:An invention is a teaching on how to use controllable natural forces to achieve a causally overseeable success which is without mediation by human reason the immediate result of controllable natural forces.)  If applied seriously, this rule excludes any patents on what German courts %(dp:call) %(q:rules of organisation and calculation) or %(q:programs for computers).  However applying this rule in practise is not always easy.  The EPO Guidelines of 1978 exhort the examiner to %(bc:disregard the form or kind of claim and concentrate on the content in order to identify the novel contribution which the alleged %(qe:invention) claimed makes to the known art.  If this contribution does not constitute an invention, there is not patentable subject matter.  This point is illustrated by the examples ... of different ways of claiming a computer program.)  Thus, even if a technical process of %(wt:cutting steel) is claimed, there is not necessarily a %(e:technical invention).  The examiner must disregard the claim language and look where the claimed contribution really lies.  Are the physical intricacies of steel part of the problem solution?  Or is there only a logical problem, which is already solved at the level of logics before it is applied to steel in the same way as a method for adding numbers may be applied to apples or tomatoes?  In the latter case there is no technical invention and therefore nothing to examine for %(q:novelty) and %(q:inventive step).  This examination method of seeing through the claim language and penetrating right to the core of the contribution, also called %(q:core theory) or %(q:differential examination), has helped patent offices to avoid a lot of tedious and costly searching work.

#MWW: Meanwhile, the EPO has however adopted a %(da:different approach) to the EPC.  In 1985, the EPO deleted the above-cited definition of %(q:computer program) from the Examination Guidelines and replaced it with ambiguous wordings.  The intuition on what is technical also faded away.  In 1998, Mark Schar, a leading EPO judge, %(ep:redefined) the %(q:technical invention) as %(q:any practical and repeatable solution), explicitely rejecting the German definition which required a direct use of physical forces.  Thereby he correctly described the EPO's de facto practise of patenting business methods and mathematical methods of even the %(ak:abstractest kind).  Since the early 90s, patents for such methods have regularly been granted, as long as they were dressed up as %(q:system and process for ...) and no explicit mention of %(q:computer program) was made in the claims.  Starting in 1997, even claims to %(q:computer program products) were granted, and %(q:computer programs) were no longer considered to be %(q:computer programs as such) if they could be said to have a %(q:technical effect) such as running on a computer, or a %(q:further technical effect), such as helping to improve computing efficiency.

#stg: such as methods of linear optimisation or theorem proving, see examples under %{REF}.

#HlW: However there is also a tendency in the EPO to refuse at least some software patents on grounds of not having a %(q:technical effect).  This was the case with a %(q:pension benefit system) in 2000.  In such cases, the EPO first conducts a prior art search.  Then, in the context of assessing %(q:inventive step) (non-obviousness), it demands that there be a %(q:technical solution to a technical problem), based on EPO intuitions.  In a recent %(pr:press release), the EPO has furthermore signalled that it is overloaded with search requests for business methods and therefore will not conduct novelty searches if it is evident from the beginning that the contribution is not %(q:technical).  This again seems to indicate a certain willingness to return to %(q:differential examination), i.e. the common sense of looking for a technical teaching before examining the novelty of that teaching, and to return to an understanding of %(q:technical character) which no longer includes all %(q:practical repeatable solutions) but on an intuitively perceived %(q:technical character), which is somehow vaguely related to the mind-matter boundary and to Art 52 EPC.  Therefore, at present, words should be carefully chosen whenever filing software patents at the European Patent Office.  %(kb:For example, it is better to use the word %(q:database) than equivalents like %(q:indexed set) or %(q:directory book)).

#Oat: One should note that software patents granted by the European Patent Office have currently little value.  Disputes show that high level national courts have largely enforced, even recently, the original interpretation of the EPC which excludes software patents. Some national judges, patent examinors, academics and patent attorneys, all known for their deep involvement in the patent system, have %(pe:publicly expressed) their worries that the current practice of the EPO could be illegal.  Owners of European software patents tend not to use and enforce them because the risk of having the patent voided by a national court is much too high. This situation may sound %(ms:absurd) but it has a great advantage since it creates some kind of self-regulation which prevents abuses from software patent owners.

#Scg: See %(sk:documentation), especially %{KFL} and %{MV}

#TnW: Typical European %(q:software patents) are really patents on logical functionalities.  Their main claims are directed to ideas such as %(co:controlling one computer by another), %(md:reading data from a medical data input source and directly displaying an analysis result to the patient) or %(bs:connecting buyers to potential sellers by letting them specify an acceptable price range through an input terminal).  These claims represent at the same time a functionality, a business idea, a problem solution, a programming problem and a computer program.  Apart from prior art there is hardly any limit to the %(fr:breadth and triviality) of such claims.  This is because computer programs, unlike traditional patentable inventions, are not limited by the constraints of matter (physical causality) but only by the laws of the human mind (logical functionality).

#Tcd: The only limitation currently lies in the requirement that the business method must be automatable, i.e. %(q:repeatable) according to %(mi:Mark Schar's invention concept).  Thus, while the EPO granted a patent on the principle of %(ll:learning languages by comparing student pronounciation with a digitally stored master sample), this covers only program logic which automates this principle, not application of the principle by human interaction between teachers and pupils.

#Tea: This view has been explained in an %(ap:EPO document) of summer 2000 and reaffirmed by the EPO's president Dr. Kober in a journal article this year.  Some writers pronounce themselves even more clearly in saying that %(q:all business methods have a technical effect and therefore must be patentable), see %{LST}

#Toi: The strongest argument to legalising software patents in Europe is to put European Patent Law in line with the United States and provide European financial markets the same tools as in the United States in order to harmonise assessments methods for intangible assets in the %(q:new economy).  Many government officials in Europe still %(br:believe that there is no other way but to imitate the United States and give multinational companies what they are asking for). Promoting innovation, competition, small software companies or opensource software may still be an issue, although probably not a priority for them.

#Eir: Enforce interoperability

#PlW: Protect small software publishers

#Ple: Protecting small development service providers

#Pro: Protecting free / open source software

#Rrt: Discouraging legal terrorism

#Ior: It is widely acknowledged that fair competition in the software economy requires interoperability, that is compatibility of file formats, network protocols and interfaces between competing products.

#Sey: Software patents tend to block interoperability because they allow to create monopolies on file formats, network protocols or interfaces. For example, Apple Computer was granted an exclusive license on the %(so:Sorenson) patents on digital video compression.  Apple markets a product called %(qt:Quicktime) which allows to view digital video compressed according to the Sorenson method on Windows and MacOS only. Other competing products (ex. Real, Windows Media Player, KDE Multimedia, XMMS, etc.) can not view digital video compressed this way because of the exclusive licensing of the Sorenson patents to Apple.

#Iec: It is therefore desirable that software patent regulation measures enforce fair licensing practices on all patents required for interoperability.

#Too: The introduction of patents in the software economy probably generates an average 30% extra cost to software development for industrial property assesment. This extra cost can be very high for a small software publisher because it requires new skills and it includes a highly unpredictable risk factors.

#Alt: As a reminder, most packaged software is developed by small teams of developers, typically 1 to 5 engineers. Training existing engineers to industrial property is long and reduces their availability for innovation tasks. Hiring a full time industrial property expert is too costly. Sharing a  full time industrial property expert is difficult. Fully outsourcing industrial property is often inefficient thus too expensive. Industrial property is often experienced as a useless burden rather than ahnything else by small software publishers as the Fraunhofer study showed.

#Tve: The risk for a small software publisher is tremendous. All currently published software likely include software patent infringements without their author being aware of it. Simple universal methods, such as publishing a database through a Web server, are covered by more than 10 patents, some of which are very similar. Patent attorneys are often %(id:unable to assess the risk of litigation on those patents). Although probably small, this risk relates to disputes costing millions of Euros. Small software publishers suffer much more than big corporations from this risk, not only because of their smaller financial size, but also because they do not usually own large patent portfolios which large corporations use to eliminate litigations risks with their competitors.

#Isa: It is therefore desirable that software patent regulation measures allow to lower the access costs to industrial property and eliminate litigation risks for small software publishers.

#Swo: Software patent regulation should also protect small software development service companies.  As we explained, simple universal techniques, such as publishing a database through a Web server, are covered by more than 10 patents.  A service company which provides some kind of specific integration of a database server, a web server and scripting language may face a patent infringement dispute and make its client face it. Large development service corporations (ex. IBM, CAP Gemini) are likely to provide some kind of patent infringement insurance to their clients, thanks to their own patent portfolio. Also, large service shops can negociate flat rate licenses with large patent portfolios. This is not however the case of small service shops for obvious reasons.

#Rns: Compensatory regulation should provide cheap or near-free insurance services to small and independent service shops in order to let them compete with large service shops.

#Odv: Open source / free software is a key technology to promote innovation, competition, freedom and democracy in the information society.  Opensource / free software requires individuals to share source code and publish freely downloadable programmes on the internet.

#SWo: Software patents are not a threat to free / opensource software developped by companies such as IBM, HP, etc., which constitutes the minority of opensource software. Software patents are however a threat to the vast majority of free / opensource software which is developped by individuals.  A substantial amount of free / opensource software has already been taken off the net because of patent threats.  Even in Europe, large corporations have threatened individual programmers.  %(mp:E.g. Thomson Multimedia threatened a Norwegian who published original implementations of patented algorithms (MP3)).  This case is quite typical: Thomson Multimedia had probably no legal grounds to start a dispute and win because the Norwegian programmer had written nothing but a computer program as such, which cannot be a patentable invention according to European law. However, a legal dispute with Thomson Multimedia is very costly and frightening. This is why Thomson Multimedia succeeded in forcing an individual programmer to remove a program from the Internet. This is also why European Linux distributions such as SuSE and Mandrake come without MP3 encoders and with ugly font rendering mechanisms for %(tt:TrueType fonts).

#Thm: Threatening opensource / free software developers with software patents is quite common in the United States. Sourceforge, a web site which hosts most opensource / free software projects, receives every week dozens of letters threatening to start a dispute for patent infringement.  Up to now, Sourceforge has used the first amendment act in the United States, which protects freedom of speech, to reject such claims.

#Hin: However, it is not certain that there is anything such as the first amendment act in Europe to protect the publication of free / opensource software. Exceptions to infringement, namely private use or research, may not apply to the publication of free/ opensource software because free / opensource software often constitutes a serious commercial competitor. Companies such as Microsoft %(st:consider) software patents as a strategic tool to fight against opensource software.

#Tob: This strategy of Microsoft became known through the %(hp:Halloween Documents) and has since then been publicly elaborated by Microsoft representatives at several occasions, see %{LST}.

#Ias: It is therefore desirable that software patent regulation measures at least make sure that opensource software can be freely distributed and eliminate legal risks for individual developers.

#Meg: Most software patent trials end up with a patent cancellation because most software patents are granted for innovations which can be shown to be old, if you only search for prior art long enough.  However, usually software patent disputes do not end in court.  Large patent portfolio holders cross-license to each other.  Small software publishers tend to accept whatever they are asked for, because litigation costs are too high.  Moreover, specialised patent litigation companies who don't write any software make a good living by buying patents from small patent holders and using them to extract well-calculated license fees from successful software companies, which regularly range from 30,000 to 2,000,000.  These %(q:out of court settlements) generally remain confidential.

#Rna: Rather than financing innovation, software patents seem to finance a litigation economy based on the filing of more or less trivial (and often even old) software ideas at patent offices.  Small software publishers, who are essential for competition and innovation in the software economy, are the most vulnerable targets.  In order to garantee that software patents do not excessively impede innovation and competition, compensatory regulation would need to address this problem.

#Tdb: The French Academy of Technology has proposed four regulation measures which could possibly allow to reach the goals described above:

#AaW: A garantee fund

#AWi: A database of known computing solutions

#Iem: Improved examination

#Iio: Incentives for SMEs

#Wac: The French Academy of Technology did not provide any detail on what those measures really mean. In order to assess their cost, we shall introduce hereafter original details for those 4 measures and add a fifth one: a patent insurance, which could be considered as an incentive for SMEs in combination with a garantee fund.

#GnW: Garantee Fund

#Att: Algorithm Database

#Iem2: Improved examination

#Ira: Incentives for small software publishers

#Ptu: Patent insurance

#Sae: Summary and hidden costs

#InW: It is possible for a state to regulate software patents abuses by introducing a garantee fund in charge of implementing a state defined policy. For example, a state owned fund could buy 1000 software patents every year, hire a patent busting (prior art search) team and act as follows:

#use: use its patent portfolio to start a dispute against any patent owner who refuses to grant fair licenses on innovations required for interoperability

#utW: use its patent portfolio to start a dispute against any patent owner who threatens developers of open source / free software

#uaW: use its patent busting team against any company in the cases described above

#awr: attack companies which practice legal terrorism

#Isr: If patents are bought at 200,000 EUR from national research centers and universities, 50,000 EUR of which are given to researchers as an incentive, if 20 persons are hired to select patents which deserve to be bought and 20 other persons are in charge of patent busting, a Garantee Fund costs about 200,000,000 EUR a year. Such a garantee fund would also optimise the effect of patents on innovation since it allows to %(cr:combine property approaches with recompense approaches).

#AnW: Shavell, van Ypersele, 1998, Rewards versus Intellectual Property Rights.

#Ogs: One should note that 1,000 patents a year is not that much. According to the USPTO database, a company such as IBM owns more than 30,000 patents and files more than 3,000 patents every year in the United States. Those figures are probably underestimated if one considers patents owned by IBM through a subsidiary or bought by IBM from third parties. It is very likely that IBM owns as much a 100,000 patents. Therefore, 1,000 patents a year is required to cover the basic technologies of software within a few years. 5,000 patents a year would probably be required, especially if the overall number and scope of software patents increases, which is to be expected.

#Aei: A large database of algorithms can help reducing litigation if it takes into account articles and software distributed around the world.  Maintaining a useful database requires methodologies that go beyond mere indexing of descriptions, because the same logical method can be described in many equivalent ways.  At least 100 people who analyse one patent application per person/day each, 200 people who analyse publications of specialist publications, 500 people who analyse actual computer programs and several 100 people who make sure that the database is maintained in a way that makes it reasonably well searchable.  This will cost more than 100,000,000 EUR per year. If at least the most important publications in languages like Japanese, Chinese, Russian etc is not to be ignored, the figure may have to be doubled.  It must be noted that the American Software Patents Institute (SPI.org) project, which major software companies such as IBM sponsored with large amounts of money, tried to undertake this work and is meanwhile generally considered to have failed.

#Igs: Improving the examination process would also help reducing litigation.  To reach a fair level of examination, it is required to add at least 5 days to the reviewing process and to duplicate the reviewing process in order to create some kind of competition and incentives between reviewer teams to eliminate as many as possible software patent applications.  This costs about 500,000,000 EUR a year for 50,000 software patents filed (but not necessarily granted) every year. It is likely that this number of patents will be much higher if the number and scope of software patents further explodes, as may be expected.

#SfW: Simple incentives for small software publishers include subsidies to file wordwide software patents (10,000 EUR for a valid patent) and training sessions on industrial property (200,000 EUR for each region in Europe every year). Such incentives may eventually allow small software publishers to understand industrial property and create tight relations with patent attorneys. This is also a way to increase the number of patent attorneys in all European regions. One can expect at least 5000 patents to be subsidised every year for a yearly cost of about 120,000,000 EUR.

#PaW: Patent insurance consists in providing full protection against patent litigations for a certain rate of a company's revenue. A patent insurance negociates flatrate licenses for all its members. Members in turn are required to check that they are not infringing on a few risky patents. Members' clients are provided a full insurance against litigation.  The cost of patent insurance is %(rp:unknown).  Patent infringement litigation costs at least several 100,000 EUR and usually several millions.  Some companies such as IBM require a 3% rate off the company turnover in return for their portfolio. If this can be taken as an indicator, it would make patent insurance cost up to 30% of each company's revenue.  Patent insurance could also cost only 1% of revenue thanks to cross licensing agreements.

#IoW: In the figures above, we have set up a patent insurance together with the garantee fund and state financed the management costs of that fund. This is a reasonable solution considering that private insurances do not want at present to insure software patent litigations.  We have also included subsidies for SMEs in order to generate a software patent insurance market in the mid term.

#Trd: The cost of the four regulation measures proposed by the French Academy of Technology can be estimated at about 1 billion EUR per year for the European Union and could grow up to 5 billion Euros or more within 10 years as the number and scope of software patents increases.  Such an increase is to be expected, considering the fact that all human ideas can be represented in the form of algorithms and put to practical use by means of Neumann's universal computer, and that computing power is continuing to double every two years and to pervade society to a degree which has by far not been exhausted.

#SWn: Such costs do not include the 30% increase in software development generated by the requirement for an industrial property strategy for software publishers, nor do they include the future cost of patent insurance for service companies. Those costs are the same for everyone and are paid by the end consumer.

#Csm: Costs Summary

#i10: in 1000 EUR

#Rfj: Compensatory regulation allows to put European Law in line with US Law, which may be useful to satisfy financial markets while preserving the European identity, innovation and competition. However, it is quite expensive. Other cheaper approaches exist if one considers adopting a different Law.

#Eat: Explicit boundaries on rights derivable from patents

#Eii: Limitations on what can be claimed

#Snr: Sui generis rights

#TlW: The use of patented methods for non-commercial purposes such as private use and research has traditionally been free.  Moreover medical therapy and farming enjoy certain exemptions. Two more such boundaries on patent enforceability could easily be introduced: interoperability and open source / free software.

#Taa: The interoperability exception should extend to software patents the interoperability principle defined in the European software copyright Law. The interoperability exception should garantee some kind of automatic free licensing for interoperability purposes. It could be decreed for example that a free license is automatically granted for importing / viewing data from another programme, that a free license is automatically granted for exporting data to another programme unless fully interoperable open formats exist and that a free license is automatically granted to communicate to another programme through a network protocol unless fully interoperable open formats exists to communicate to that other programme. This way, owners of patents on formats and protocols can either enforce their property by guaranteing that all implementations include fully open and interoperable import fil or network protocols or let competitors use their property for interoperability.

#ToW: The free / open source software exception could explicitely stipulate that the open source / free software process is a kind of R&D or even a parallel system for the promotion of innovation and diffusion and can therefore not be considered as a patent infringement.  Various degrees of exemption are possible.

#Scw: Such explicit exemptions produce near equivalent effects to what the guarantee fund provides.

#OWm: One may consider that only claims on physical device or physical processes may be accepted and that claims on software on a media carrier should be rejected.

#Ttm: Thus the publication of software could no longer be viewed as a direct infringement.  Even if it could still constitute a contributory infringement, this reduction in liablity could significantly contribute to protecting small software publishers from abusive litigations because contributory infringement provisions are much more flexible than direct infringement provisions.

#Sai: Such an explicit limitation could state for example that software distributed for free on the Internet can never be considered as a contributory patent infringement as long as all patents required to use it commercialy are clearly listed. Such an approach is required in our opinion in order to make the international nature of software distribution on the Internet compatible with the national nature of patent law.  On the other hand, matters could be different for software distributed on physical media or to software sold on the Internet for which some kind of national nature exists (ex. credit card number).  Physical devices (ex. PABX, missile) and industrial applications of algorithms (e.g. steel cutting) could receive yet another kind of treatment.

#See: Such explicit limitations produce near equivalent effects to what an algorithm database and better examination provide against legal terrorism.  Owners of valid software patents are encouraged by this system to reach an agreement with software publishers.

#Ase: An even more friendly variant would be to restrict enforcability to complete devices with embedded systems, i.e. devices which are not laid out for reprogramming by the user.  This method would protect much of the traditional interests of the telecommunications and industrial software patent owners while keeping those spheres free of patents where an ever-increasing number of freelancers and small companies are making significant contributions to new developments.

#TWw: This approach has been suggested by the Conseil General des Mines in September 2000 as well as by %(mm:Dr. Martin Mayer) (spokesman of the Christian Democrat/Social Union in the German parliament) and %(mo:many others).  It consists in creating a special-tailored property right (ius sui generis) for algorithms (teachings of logical functionality).  At the same time, it would have to be made clear that patent law is also a special-tailored right, namely for %(dp:technical inventions) (teachings of physical causality).  The relationship between copyright and the algorithm law still seems unclear.  One approach is to consider copyright as applicable to all intellectual creations and at the same time extend or replace it by some specialised laws in niche areas, such as software behavior or industrial design.  Some arguments for this approach are of political nature: it provides a link between an American software patent system and a European system where patents are limited to technical inventions.  Moreover it provides an outlet for accomodating legitimate interests in algorithm property, as far as they may exist, in a balanced and adequate manner.

#sWW: see the concluding paragraphs of the %(dp:Dispositionsprogramm) decision of the German Federal Supreme Court, which explain this very convincingly and demands that the various specialised property systems need to be kept clearly separate.

#TWt: The software economy includes properties which do not exist in any traditional industry:

#Sat: Software is pure information: it can be published on the Internet with zero marginal distribution costs.

#Sxi: Software developped by one man (ex. the Linux operating system, the Konqueror web browser) or a group of friends (ex. the Apache web server, the Ogg music compression format) can compete with equivalent software developped by multinational companies such as Microsoft, Thomson Multimedia or Netscape (ex. Windows, Internet Explorer, iPlanet, MP3).

#Hps: Highly multidimensional network effects: multiple interoperability issues are coupled in software which generates much stronger network effects than in any other field.

#TiW: The first two features explain why much software innovation comes from individual developers and very small software publishers based on the Internet. It is a justification for protection measures in favour those two groups. The last feature is an argument for stronger competition protection measures.

#Tni: The epistemologic nature of software is equally striking:

#SrW: Software is logics, a hierarchy of abstract functions.  Before the computer existed, software already existed.  Patent law scholars were careful not to allow the patenting of the logical aspects inherent in all apparatusses, such as %(q:operation instructions).  With the advent of the computer, these logical aspects emancipated themselves from the apparatusses and the humans operating them.

#Sng: Software is equivalent to human reason and to human language.  It consists in describing a reproducible series of steps to manipulate data (abstract information).  Algorithms can describe all human human reasoning.  While speaking Logical Language (Loglan/Lojban), human speech becomes turing-complete.  Computer programs are equivalent to mathematical proofs and are validated by means of logics rather than by physical experiments.

#TWW: The value of software ideas lies in their %(ab:abstractness). Ingenuous software innovations, such as the %(ko:Karmarkar inner-point method), are extremely general, leading to patents with an unoverseeably broad scope of applications.  Less abstract software ideas tend to be trivial.  Most software patent claims are both trivial and broad, and this phenomenon does not significantly depend on how strictly patent offices apply the rules of %(q:novelty) and %(q:non-obviousness).  Moreover, abstractness makes novelty search all but impossible.

#SWi: Software is reflexive (it is its own description): it can be self generated without human intervention, it can duplicate itself just like a conscious life form, it can coordinate itself with other software just like a social form.  It is an objectivation of human intelligence and lives through the communication of those who understand the language in which it is written.  Many software companies have names like %(q:Active Knowledge), %(q:Thinking Objects) and other terms drawn from %(q:Artificial Intelligence), which reveal that software is increasingly working as the brain within our social organism and thereby takes on functions which previously belonged to human intelligence or even to law, as Prof. Lessig's book %(q:Code is Law) convincingly shows.

#Sit: Software is intellectual creation and even art.   There are as many ways to implement a patented functionality as there are ways to apply algoritms like chromatic modulation or twelve-tone music to the creation of symphonies.  Programs are often even more complex and delicate than symphonic creations. The difficulty usually does not consist in devising appropriate algorithms (building elements) but in building a well-designed %(q:tissue), a sustainable multi-layer hierarchy of functionalities with an infinite number of choices that require skill and imagination.  Therefore copyright is at least as appropriate to software as to construction plans, scientific articles, operation manuals, pieces of music and most of the other types of intellectual creations for which it is used.  Even artistic creations such as multimedia works and games include software programming.  Moreover, poems written in Logical Language have a particularly high aesthetic value, and even pure programming languages like Perl are occasionally used for poetry.

#Tnt: These properties deserve a lot of attention from a legal point of view. They require to consider with great care the meaning of infringement and contributory infringement induced by the use of single software or multiple software.  Moreover they make it necessary to define a clear limit to the patent system in order not to let crude control mechanisms which were designed for material objects reach out into the sphere of the human mind.  Some writers like Dr. Kiesewetter-Köbinger, patent examiner at the German Patent Office, have therefore %(kk:argued) that software patents constitute a radical breach of basic values of our civilisation and an attack on central tenets of our (German) constitution, ranging from Art 1 %(q:The Dignity of the Human Being is Untouchable) and Art 2 (equality principle) to freedom of expression, freedom of contract design and the requirement that the intellectual property, which in the case of software consists in the tissue, be protected rather than expropriated.

#Rtc: Regulating software patent abuses has a cost : between about 1 billion and 5 billions Euros every year for the Europe Union. Only public money can finance this cost, most of which consists in improving the patent reviewing process and implementing a garantee fund.  Unless a Software Patent Regulation Authority (SPRA) is explicitely created and budgeted within the the European regulations to come on software patents, we doubt that European governements will be able to finance such costs.

#Oia: Other approaches to regulation exist and provide similar effects: introducing explicit exceptions and limitations in order to enforce interoperability, to protect open source / free software and to protect small publishers against legal terrorism. Such approaches should be seriously considered and compared to regulation approaches.

#Etn: Economy however is not the only issue in the case of software patents.  Law consistency issues should be addressed too. For example, any extension of the patent system to software without justifications of a positive economic impact may be in violation with Article 10 of the European Convention on Human Rights. Up to now, no one has ever seriously argued let alone proven that software patents have any positive %(ee:economic effects).

#EWn: Existing economic arguments show that software patents promote concentrations in the software creating sector as well as in the software consulting services.  They also show that software patents are needed for competing in economic environments such as that of the US, where software patents have become an important factor.  Comfortable mechanisms for international patent filing (PCT) already exist and could be improved.   European companies can use them regardless of whether the particular subject matter is patentable in Europe or not.

#Anm: Also, the epistemologic nature of software creates unique issues.  For example, the ownership of software innovations generated by %(q:invention machines) is yet %(uk:unknown).  Failing to design a consistent and adequate patent law may achieve the opposite of %(q:clarification) and %(q:harmonisation): higher legal uncertainty than ever before.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatpleji ;
# txtlang: en ;
# multlin: t ;
# End: ;

