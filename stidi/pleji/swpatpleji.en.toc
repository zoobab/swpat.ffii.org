\contentsline {section}{\numberline {1}A Drive for Patenting Logical Functionalities in Europe}{2}{section.1}
\contentsline {section}{\numberline {2}Patenting Logical Functionalities under the European Patent Convention}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Copyright vs Patents}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Description \& Claims}{6}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Describing Logical Ideas and Claiming Physical Objects}{6}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Software patents in Europe}{7}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Business method patents in Europe}{9}{subsection.2.5}
\contentsline {section}{\numberline {3}Software Patents with Compensatory Regulation: System Goals}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Enforce interoperability}{10}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Protect small software publishers}{11}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Protecting small development service providers}{12}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Protecting free / open source software}{12}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Discouraging legal terrorism}{13}{subsection.3.5}
\contentsline {section}{\numberline {4}Software Patents with Compensatory Regulation: Measures and Costs}{13}{section.4}
\contentsline {subsection}{\numberline {4.1}Garantee Fund}{14}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Algorithm Database}{14}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Improved examination}{15}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Incentives for small software publishers}{15}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Patent insurance}{15}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Summary and hidden costs}{16}{subsection.4.6}
\contentsline {section}{\numberline {5}Other Approaches to Reducing Risks of Software Patentability}{18}{section.5}
\contentsline {subsection}{\numberline {5.1}Explicit boundaries on rights derivable from patents}{18}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Limitations on what can be claimed}{18}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Sui generis rights}{19}{subsection.5.3}
\contentsline {section}{\numberline {6}What is so special about Software?}{19}{section.6}
\contentsline {section}{\numberline {7}Conclusion}{21}{section.7}
