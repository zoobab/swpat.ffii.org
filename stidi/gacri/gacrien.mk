gacrien.lstex:
	lstex gacrien | sort -u > gacrien.lstex
gacrien.mk:	gacrien.lstex
	vcat /ul/prg/RC/texmake > gacrien.mk


gacrien.dvi:	gacrien.mk gacrien.tex
	latex gacrien && while tail -n 20 gacrien.log | grep references && latex gacrien;do eval;done
	if test -r gacrien.idx;then makeindex gacrien && latex gacrien;fi
gacrien.pdf:	gacrien.mk gacrien.tex
	pdflatex gacrien && while tail -n 20 gacrien.log | grep references && pdflatex gacrien;do eval;done
	if test -r gacrien.idx;then makeindex gacrien && pdflatex gacrien;fi
gacrien.tty:	gacrien.dvi
	dvi2tty -q gacrien > gacrien.tty
gacrien.ps:	gacrien.dvi	
	dvips  gacrien
gacrien.001:	gacrien.dvi
	rm -f gacrien.[0-9][0-9][0-9]
	dvips -i -S 1  gacrien
gacrien.pbm:	gacrien.ps
	pstopbm gacrien.ps
gacrien.gif:	gacrien.ps
	pstogif gacrien.ps
gacrien.eps:	gacrien.dvi
	dvips -E -f gacrien > gacrien.eps
gacrien-01.g3n:	gacrien.ps
	ps2fax n gacrien.ps
gacrien-01.g3f:	gacrien.ps
	ps2fax f gacrien.ps
gacrien.ps.gz:	gacrien.ps
	gzip < gacrien.ps > gacrien.ps.gz
gacrien.ps.gz.uue:	gacrien.ps.gz
	uuencode gacrien.ps.gz gacrien.ps.gz > gacrien.ps.gz.uue
gacrien.faxsnd:	gacrien-01.g3n
	set -a;FAXRES=n;FILELIST=`echo gacrien-??.g3n`;source faxsnd main
gacrien_tex.ps:	
	cat gacrien.tex | splitlong | coco | m2ps > gacrien_tex.ps
gacrien_tex.ps.gz:	gacrien_tex.ps
	gzip < gacrien_tex.ps > gacrien_tex.ps.gz
gacrien-01.pgm:
	cat gacrien.ps | gs -q -sDEVICE=pgm -sOutputFile=gacrien-%02d.pgm -
gacrien/gacrien.html:	gacrien.dvi
	rm -fR gacrien;latex2html gacrien.tex
xview:	gacrien.dvi
	xdvi -s 8 gacrien &
tview:	gacrien.tty
	browse gacrien.tty 
gview:	gacrien.ps
	ghostview  gacrien.ps &
print:	gacrien.ps
	lpr -s -h gacrien.ps 
sprint:	gacrien.001
	for F in gacrien.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	gacrien.faxsnd
	faxsndjob view gacrien &
fsend:	gacrien.faxsnd
	faxsndjob jobs gacrien
viewgif:	gacrien.gif
	xv gacrien.gif &
viewpbm:	gacrien.pbm
	xv gacrien-??.pbm &
vieweps:	gacrien.eps
	ghostview gacrien.eps &	
clean:	gacrien.ps
	rm -f  gacrien-*.tex gacrien.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} gacrien-??.* gacrien_tex.* gacrien*~
tgz:	clean
	set +f;LSFILES=`cat gacrien.ls???`;FILES=`ls gacrien.* $$LSFILES | sort -u`;tar czvf gacrien.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
