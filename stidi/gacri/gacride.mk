gacride.lstex:
	lstex gacride | sort -u > gacride.lstex
gacride.mk:	gacride.lstex
	vcat /ul/prg/RC/texmake > gacride.mk


gacride.dvi:	gacride.mk gacride.tex
	latex gacride && while tail -n 20 gacride.log | grep references && latex gacride;do eval;done
	if test -r gacride.idx;then makeindex gacride && latex gacride;fi
gacride.pdf:	gacride.mk gacride.tex
	pdflatex gacride && while tail -n 20 gacride.log | grep references && pdflatex gacride;do eval;done
	if test -r gacride.idx;then makeindex gacride && pdflatex gacride;fi
gacride.tty:	gacride.dvi
	dvi2tty -q gacride > gacride.tty
gacride.ps:	gacride.dvi	
	dvips  gacride
gacride.001:	gacride.dvi
	rm -f gacride.[0-9][0-9][0-9]
	dvips -i -S 1  gacride
gacride.pbm:	gacride.ps
	pstopbm gacride.ps
gacride.gif:	gacride.ps
	pstogif gacride.ps
gacride.eps:	gacride.dvi
	dvips -E -f gacride > gacride.eps
gacride-01.g3n:	gacride.ps
	ps2fax n gacride.ps
gacride-01.g3f:	gacride.ps
	ps2fax f gacride.ps
gacride.ps.gz:	gacride.ps
	gzip < gacride.ps > gacride.ps.gz
gacride.ps.gz.uue:	gacride.ps.gz
	uuencode gacride.ps.gz gacride.ps.gz > gacride.ps.gz.uue
gacride.faxsnd:	gacride-01.g3n
	set -a;FAXRES=n;FILELIST=`echo gacride-??.g3n`;source faxsnd main
gacride_tex.ps:	
	cat gacride.tex | splitlong | coco | m2ps > gacride_tex.ps
gacride_tex.ps.gz:	gacride_tex.ps
	gzip < gacride_tex.ps > gacride_tex.ps.gz
gacride-01.pgm:
	cat gacride.ps | gs -q -sDEVICE=pgm -sOutputFile=gacride-%02d.pgm -
gacride/gacride.html:	gacride.dvi
	rm -fR gacride;latex2html gacride.tex
xview:	gacride.dvi
	xdvi -s 8 gacride &
tview:	gacride.tty
	browse gacride.tty 
gview:	gacride.ps
	ghostview  gacride.ps &
print:	gacride.ps
	lpr -s -h gacride.ps 
sprint:	gacride.001
	for F in gacride.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	gacride.faxsnd
	faxsndjob view gacride &
fsend:	gacride.faxsnd
	faxsndjob jobs gacride
viewgif:	gacride.gif
	xv gacride.gif &
viewpbm:	gacride.pbm
	xv gacride-??.pbm &
vieweps:	gacride.eps
	ghostview gacride.eps &	
clean:	gacride.ps
	rm -f  gacride-*.tex gacride.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} gacride-??.* gacride_tex.* gacride*~
tgz:	clean
	set +f;LSFILES=`cat gacride.ls???`;FILES=`ls gacride.* $$LSFILES | sort -u`;tar czvf gacride.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
