<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Kollektive Schutzschilder gegen Softwarepatente?

#descr: Die Vorstellung, die Gemeinde der Entwickler freier/quelloffener
Software könnten ein eigenes Patentportfolio aufbauen, muss am
Widerspruch zwischen den Grundregeln der freien Software und dem
Verwertung erzwingenden Ansatz des Patentwesens weitgehend scheitern. 
Andererseits ist die freie Welt gegenüber der proprietären in einem
Punkt im Vorteil:  bei der Dokumentierung und Verwertung des Standes
der Technik.  Die Gemeinde könnte bei kluger Vorgehensweise
erheblichen Nutzen aus ihrer offenen Entwicklungsweise und aus der im
Patentwesen eingebauten Rechtsunsicherheit ziehen.  Sogenanntes
Defensives Veröffentlichen, wie es Foresight Institute und andere
vorschlagen, ist hingegen wiederum eine unglaublich törichte Idee. 
Die wichtigste Aufgabe zu diesem Zeitpunkt (Frühjahr 2001) ist eine
konsequente Bewehrung aller Stätten der Softwareentwicklung mit
Mechanismen der Zeitstempelung.

#BWi: Schlechte Karten beim Defensiven Patentieren

#Gtn: Gute Karten beim Nutzen vorbekannter Technik

#DPN: %(q:Defensives Veröffentlichen) Nein Danke!

#Zri: Zeitstempelung proprietärer Entwicklung hilft auch

#Web: Was zu tun ist

#Wrk: Weitere Lektüre

#Oni: Der Erwerb und Erhalt von Patenten ist mit hohen Kosten verbunden

#cfv: Motivationskosten

#DeW: Developpers find new ideas every day and are usually not interested in
the tedious work of distilling these ideas into patent applications.
They want to develop products and not patents, and often resent
patenting on various grounds.  Even though some laws guarantee them a
share in revenues from the patent, this usually is not enough of a
motiviation.  Large companies like Siemens, IBM or SAP have therefore
installed well-staffed committees of patent application experts and
ample bonusses for developpers in order to overcome this problem and
provide suffient incentives for developping patents.

#cot: Erwerbskosten

#Ass: According to EPO figures, a European patent costs an average of 30000
EUR in office fees, assuming that it is upheld for only 10 years and
not translated to all European languages.  Costs for patent attorneys
are much higher and often calculated on a basis of 300 EUR per hour.

#cft: Aufrechterhaltungskosten

#Eev: Even simple litigation against one infringer or opponent will cost
more than 100000 EUR/USD in lawyer fees.  Often the cost is in the
range of millions, and many inventors even of important basic patents
have gone bankrupt over this (e.g. Goodyear).  Insurance policies for
patent litigation either don't exist or are astronomically high.

#Ter: Diese Kosten müssen durch die gnadenlose Ausübung des Monopolrechts
refinanziert werden.  Das erfordert eine Kombination aus Sockelpreis
und Stückpreis.  Freie/quelloffene Software kennt nicht den Begriff
der Stückzahl.  Somit kann nur ein hoher Sockelpreis anfallen, wie
z.B. 1 Million USD im Falle der MP3-Lizenzen.  Es ist
unwahrscheinlich, dass jemand einen solchen Preis aufbringt, nur um
der Öffentlichkeit freie Software schenken zu dürfen.  So etwas täte
nur ein Staat oder eine potente Stiftung, nicht aber ein privater
Teilnehmer des Wirtschaftslebens.  Somit bleibt noch die Möglichkeit,
zwar freie Software (z.B. GPL) zu erlauben, aber für proprietäre
Implementationen Lizenzgebühren zu verlangen.  In diesem Falle werden
aber meist keine hohen Lizenzeinnahmen zu erzielen sein, da man eine
mächtige freie Konkurrenz zu sich selbst begünstigt.  Mit einem
solchen Modell der Patentverwertung lässt sich schwer gegen diejenigen
ankämpfen, die ohne jegliche idealistische Belastung direkt auf
Profitmaximierung unter den Regeln des Patentwesens zielen.

#Was: Wie wir sahen macht die proprietäre Natur des Patentsystems es fast
unmöglich, dieses System zum Aufbau eines Schutzschildes für die
Entwickler freier Software zu nutzen.

#Hus: Anderseits bedingt gerade die proprietäre Natur des Systems eine
Schwachstelle, die man nutzen kann.

#Bde: Die Entwickler freier Software publizieren besonders umfangreiche
Mengen von Ideen in einem frühen Stadium.  Solche Publikationen sind
selbstverständlich als %(q:Stand der Technik) anerkannt.  Darauf
aufgebaute spätere Patente werden entkräftet, sobald die vorbekannte
Idee gefunden wird.

#Ivo: Genau genommen berührt dies die Hauptgefahrenquell nicht im
geringsten:  es wird trotzdem zahlreiche breite und triviale
Patentansprüche geben, gegen die kein %(q:Stand der Technik) zitiert
werden kann.  Doch es ist für beide Seiten schwierig, den wirklichen
%(q:Stand der Technik) zu ermitteln.  Niemand weiß genau, ob sein
Patent wirklich gültig ist.  Daher wird in dem Maße, wie ein großes
unübersichtliches Meer von Archiven existiert, aus dem die Geschichte
der Software-Ideen mit genauem Datum rekonstruierbar ist, niemand mehr
sicher sagen können, ob und inwieweit ein bestimmtes Patent gültig
ist.  Hieraus allein ergibt sich ein rationaler Grund für egoistische
Unternehmen, die Gemeinde der Entwickler Freier Software nicht allzu
unvorsichtig herauszufordern.  Sie hält nämlich folgende starke Karte
in den Händen:

#Ald: Ein großes Arsenal möglicherweise neuheitsschädigender Publikationen,
die kein Einzelner überblicken kann.

#Ano: Eine grundsätzliche Unsicherheit bezüglich der Gültigkeit der
Patentportfolios aller Marktteilnehmer.  Viele davon sind
Kartenhäuser, die zum Einsturz gebracht werden könnten, was dann die
Investoren verscheucht und daher von den meist kurzfristig denkenden
Entscheidern um jeden Preis vermieden werden muss.

#Aid: Eine große Gemeinde kompetenter und motivierter Softwareentwickler,
die in der Lage ist, durch kollektive Kampagnen das Geschütz %(q:Stand
der Technik) gegen unfreundliche Firmen in Stellung zu bringen.

#Uma: Unter diesen Umständen erscheint es im Moment als vordringliche
Aufgabe, den Entwicklungsprozess freier Software, wie er sich in
CVS-Bäumen, Mailinglisten u.v.m. zeigt, mit Zeitstempel zu
dokumentieren.  Wo das bisher noch nicht gelingt, könnte man viele
Diskussionen ins Usenet verlagern.  Dort steht mit Dejanews ein
glaubwürdiger Archivierungsdienst zur Verfügung.  Es schadet auch
nichts, vermehrt in öffentlichen Diskussionsrunden in den
verschiedensten Sprachen exhibitionistische Fantasien über mögliche
Softwareentwicklungen auszutauschen.  All das gilt aus der Sicht von
Patentprüfern als %(q:neu und erfinderisch) und kann daher eines Tages
nützlich werden.

#Tsi: Das dümmste was Entwickler freier Software im Moment tun könnten,
wäre, sich von Patentämtern in den Dienst nehmen zu lassen, um deren
Suchdatenbanken aufbauen zu helfen, %(q:damit ungültige Patente erst
gar nicht gewährt werden), wie von %(q:Foresight Institute) mit
unterstützung einiger der %(q:höchsten Opensource-Wortführer)
vorgeschlagen wurde.  Diese sogenannte %(q:Defensive Veröffentlichen)
ist ein Bumerang, der sowohl gegen diejenigen, die es versuchen, als
auch gegen die ganze Gemeinde zurückschlägt.  Vergleichen wir einmal
die Wirkungen des %(q:Defensiven Veröffentlichens) mit denen der
dezentralen Zeitstempelung des Standes der Technik.

#Fse: Formvollendetes Defensives Veröffentlichen

#ZWE: Zeitstempelung des normalen offenen Entwicklungsvorgangs

#Pve: Patentrelevante Ideen leicht zu recherchieren

#Pvh: Patentrelevante Ideen schwer zu recherchieren

#GaW: Großer Aufwand für den Entwickler, geringer Aufwand für den
Rechercheur und Patentanmelder

#GWr: Geringer Aufwand für den Entwickler, Großer Aufwand für die
Rechercheure und Patentanmelder

#ljv: Niedrigere Patentierungskosten, mehr (breite und triviale)
Softwarepatente

#htd: Höhere Patentierungskosten, weniger (breite und triviale)
Softwarepatente

#Ywe: Unsere Softwareidee wird wahrscheinlich von niemandem patentiert
werden.

#Yhl: Unsere Softwareidee wird vielleicht patentiert, aber der Patentinhaber
wird einen Schreck bekommen, sobald  wir ihm unsere
Prioritätsnachweise vorzeigen.  Dann wird er einen hohen Bogen nicht
nur um unser Projekt machen, sondern es auch tunlichst vermeiden, sich
unnötig mit anderen Entwicklern freier Software anzulegen.  Denn das
könnte einen solidarischen Angriff nach sich ziehen, der auf einmal
sein Patentportfolio schlecht aussehen lässt.  Dann könnten Investoren
abspringen und Aktienkurse sinken.  Da ist es dann besser, das
Patentportfolio nur als Bluffpotential beizubehalten und allenfalls
selektiv gegen öffentlichkeitsscheue KMU einzusetzen.

#AkW: Allerlei Glücksritter versammeln sich um die Datenbank der Devensiven
Veröffentlichungen herum, um zu schauen, wo gerade patentrelevante
Ideen entwickelt werden.  Sie lassen sich dann ein paar komplementäre
Ideen einfallen und patentieren diese.  Diese Nachfolgepatente sind
dann gültig.  Unser Arbeitsfeld wird mit einem Patentdickicht
blockiert.

#NWW: Kaum ein Glücksritter findet rechtzeitig die gesuchten Hinweise, und
es bleibt genug Zeit, die komplementären Ideen selber rechtzeitig zu
dokumentieren und damit zu befreien.

#Peb: Patentämter können mit einiger Glaubwürdigkeit sagen:  %(q:Wir haben
alles getan um Datenbanken aufzubauen und den Entwicklern freier
Software die bestmöglichen Bedingungen zu schaffen.  Den kostenlosen
Zugriff auf das Geistige Eigentum anderer hätte jeder gerne... Statt
sich weiter zu beklagen, sollten die Entwickler sich mal lieber ein
paar mehr eigene Ideen einfallen lassen und schön brav in unsere
Datenbank eintragen.)

#Pto: Patentämter müssen vielleicht anerkennen, dass der Stand der Technik
im Softwarebereich nicht %(q:bloß eine Frage der Zeit) ist.

#Ane: Auch Entwickler proprietärer Software sollten ihre Arbeit Tag für Tag
genau rekonstruierbar machen.  Dies können sie tun, indem sie die
gleichen zeitgestempelten Entwicklungsumgebungen verwenden wie die
Entwickler freier Software und gelegentlich MD5-Prüfsummen in
Zeitungen veröffentlichen oder in Bibliotheken hinterlegen.

#IrW: In diesem Falle entsteht kein vorbekannter %(q:Stand der Technik), der
das Patent zu Fall bringen könnte, sondern lediglich ein privates
Vorbenutzungsrecht.  D.h. der Entwickler darf seine Ideen weiterhin
selber vermarkten.  Er darf dieses Recht allerdings nicht veräußern.

#Ntn: Nicht ganz klar ist bisher, was mit dem Vorbenutzungsrecht passiert,
wenn der Entwickler eines Tages seine proprietäre Software unter
Opensource-Lizenz veröffentlicht.  Stellt das eine Veräußerung des
Rechtes auf Vermarktung der vorbenutzten Idee dar?  Steht es nicht
andererseits dem Vorbenutzer frei, sein %(q:Produkt) in beliebiger
Weise auf den %(q:Markt) zu bringen und an beliebige Kunden zu
%(q:verkaufen), ohne dass diese Kunden wiederum jeder für sich eine
Patentlizenz erwerben m|ssen?  Man sieht, dass hier die Prämissen des
Patentsystems nicht mit der Realität des Gemeingutes Information
zusammenpassen.

#Sbe: Solange keine gegenteiligen Richtersprüche vorliegen, darf man
allerdings begründet hoffen, dass es dem Vorbenutzer tatsächlich
freisteht, sein Vorbenutzungsrecht im Rahmen freier Software so zu
verbreitern, dass jeder das Patent umgehen kann, sofern er seine
Entwicklungsarbeit wiederum in freie Software einfließen lässt.

#Sns: Somit hat der Vorbenutzer einer patentierten Softwareidee eine
besonders starke Stellung.  Er kann entweder diese Idee für alle frei
machen oder mit dem Patentinhaber eine Gegenleistung dafür aushandeln,
dass er dies nicht tut.  Solche Aussichten können erheblich den Anreiz
zur Anmeldung schwacher Patente mindern.  Für die Allgemeinheit ist
somit ein Vorbenutzungsrecht immerhin etwa halb so viel wert wie eine
echte Vorveröffentlichung.  Auch um dieses halben Wertes willen lohnt
es sich für den Vorbenutzer, seine zeitgestempeltes
Entwicklungsgeschichtsarchiv mit einem gewissen Zeitverzug der
Allgemeinheit zur Verfügung zu stellen.  Dieses Spiel kann, wenn man
es wirksam spielt, die Patentierung trivialer Innovationen riskant
machen.  Denn je trivialer die Innovation ist, mit desto höherer
Wahrscheinlichkeit ist anzunehmen, dass sich irgendwo ähnliches in
einem privaten Entwicklungsgeschichtsarchiv findet.

#AWw: Ausstattung aller Netzorte, an denen Software entwickelt und
besprochen wird, mit Zeitstempelungs-Mechanismen.

#Aig: Leicht verständliche öffentliche Dokumentation derjenigen
Software-Patente, deren Inhaber oder Lizenznehmer besonders skrupellos
damit umgehen.

#Btn: Bildung von mehr oder weniger fest organisierten Genossenschaften,
deren Mitstreiter den Stand der Technik für ihre Zwecke dokumentieren,
ihre Patente einbringen, und sich gegenseitig dabei helfen, diese
Kampfmittel für sich und im Interesse der freien Software einzusetzen.

#AWs: Kontinuierlich auf Öffentlichkeit und Politiker einwirken, um die
Patentinflation unter Kontrolle zu bringen und dafür zu sorgen, dass
keine Patente auf Softwareideen (abstrakt-logische Innovationen) mehr
erteilt werden.

#Sri: Skeptischer Artikel in LinuxWeek

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatgacri ;
# txtlang: ja ;
# multlin: t ;
# End: ;

