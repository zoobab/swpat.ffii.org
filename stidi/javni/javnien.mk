javnien.lstex:
	lstex javnien | sort -u > javnien.lstex
javnien.mk:	javnien.lstex
	vcat /ul/prg/RC/texmake > javnien.mk


javnien.dvi:	javnien.mk javnien.tex
	latex javnien && while tail -n 20 javnien.log | grep references && latex javnien;do eval;done
	if test -r javnien.idx;then makeindex javnien && latex javnien;fi
javnien.pdf:	javnien.mk javnien.tex
	pdflatex javnien && while tail -n 20 javnien.log | grep references && pdflatex javnien;do eval;done
	if test -r javnien.idx;then makeindex javnien && pdflatex javnien;fi
javnien.tty:	javnien.dvi
	dvi2tty -q javnien > javnien.tty
javnien.ps:	javnien.dvi	
	dvips  javnien
javnien.001:	javnien.dvi
	rm -f javnien.[0-9][0-9][0-9]
	dvips -i -S 1  javnien
javnien.pbm:	javnien.ps
	pstopbm javnien.ps
javnien.gif:	javnien.ps
	pstogif javnien.ps
javnien.eps:	javnien.dvi
	dvips -E -f javnien > javnien.eps
javnien-01.g3n:	javnien.ps
	ps2fax n javnien.ps
javnien-01.g3f:	javnien.ps
	ps2fax f javnien.ps
javnien.ps.gz:	javnien.ps
	gzip < javnien.ps > javnien.ps.gz
javnien.ps.gz.uue:	javnien.ps.gz
	uuencode javnien.ps.gz javnien.ps.gz > javnien.ps.gz.uue
javnien.faxsnd:	javnien-01.g3n
	set -a;FAXRES=n;FILELIST=`echo javnien-??.g3n`;source faxsnd main
javnien_tex.ps:	
	cat javnien.tex | splitlong | coco | m2ps > javnien_tex.ps
javnien_tex.ps.gz:	javnien_tex.ps
	gzip < javnien_tex.ps > javnien_tex.ps.gz
javnien-01.pgm:
	cat javnien.ps | gs -q -sDEVICE=pgm -sOutputFile=javnien-%02d.pgm -
javnien/javnien.html:	javnien.dvi
	rm -fR javnien;latex2html javnien.tex
xview:	javnien.dvi
	xdvi -s 8 javnien &
tview:	javnien.tty
	browse javnien.tty 
gview:	javnien.ps
	ghostview  javnien.ps &
print:	javnien.ps
	lpr -s -h javnien.ps 
sprint:	javnien.001
	for F in javnien.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	javnien.faxsnd
	faxsndjob view javnien &
fsend:	javnien.faxsnd
	faxsndjob jobs javnien
viewgif:	javnien.gif
	xv javnien.gif &
viewpbm:	javnien.pbm
	xv javnien-??.pbm &
vieweps:	javnien.eps
	ghostview javnien.eps &	
clean:	javnien.ps
	rm -f  javnien-*.tex javnien.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} javnien-??.* javnien_tex.* javnien*~
tgz:	clean
	set +f;LSFILES=`cat javnien.ls???`;FILES=`ls javnien.* $$LSFILES | sort -u`;tar czvf javnien.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
