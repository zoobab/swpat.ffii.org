<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Gesetzesregel über den Erfindungsbegriff im Europäischen Patentwesen
und seine Auslegung unter besonderer Berücksichtigung der Programme
für Datenverarbeitungsanlagen

#descr: Wir schlagen dem Gesetzgeber vor, beim Entwurf einer Richtlinie zur
Frage der Patentierbarkeit von Software auf dem folgenden kurzen und
klaren Text aufzubauen.

#Gte: Geleitet von der Erkenntnis, dass die Grenzziehung zwischen
patentierbaren und nicht patentierbaren Gegenständen besonders klare
und explizite Entscheidungen auf gesetzgeberischer Ebene erfordert

#Isa: beeindruckt von der Erfahrung, dass Art 52 des Europäischen
Patentübereinkommens (EPÜ) der Rechtsprechung Raum für
Missverständnisse und Ungereimtheiten gelassen hat

#Ihf: beunruhigt von der Aussicht, dass von diversen Gerichten neu gesetzte
Regeln die Wissensausbreitung und Innovation in der
Informationsgesellschaft zunehmend behindern könnten

#wer: stellen wir folgendes klar:

#Unr: Ein %(e:Programm für Datenverarbeitungsanlagen), kurz %(e:DV-Programm)
genannt, ist eine Rechenregel für eine Turing-Maschine oder eine
sonstige abstrakte Maschine, die sich auf verschiedenen
Entwurfsstufen, vom gedanklichen Plan bis zu der von einem Menschen
oder einem Prozessor ausführbaren Anweisung, ausdrücken lässt.  Ein
DV-Programm ist Bauplan und Gebrauchsanweisung, Verfahrensbeschreibung
und Problemlösung, Sprachkunstwerk und virtuelle Maschine, Erzeugnis
und Verfahren zugleich.

#Dco: Eine programmgesteuertes patentierbares Verfahren lässt sich von dem
steuernden DV-Programm [als solchem] unterscheiden.  Unter einem
%(q:DV-Programm mit [zusätzlichen] patentierbaren Merkmalen) ist
hingegen nur ein DV-Programm [als solches] zu verstehen,
möglicherweise in Vermengung mit programmfremden Elementen, deren
Patentierbarkeit getrennt zu prüfen wäre.  Art 52 (3) ist auf alle in
(2) genannten Gegenstände in gleicher Bedeutung anzuwenden.

#Pdd: DV-Programme sind keine Erfindungen im Sinne des Patentrechts.  Ein
programmgesteuertes technisches Verfahren (z.B. chemisches
Herstellungsverfahren) kann eine Erfindung sein.   Die aus einer
solchen Erfindung abzuleitenden Ausschlussrechte beschränken sich auf
die Nutzung des Verfahrens zur gewerblichen Erzeugung materieller
Güter (z.B. Chemikalien).  Kein Patentanspruch kann dazu berechtigen,
jemanden von Schaffung, Verbreitung, Verkauf oder Ausführung eines
DV-Programms auszuschließen.

#UWg: %(e:Technik) ist %(e:angewandte Naturwissenschaft), %(q:Lösung von
Problemen durch Einsatz von Naturkräften) oder, gemäß einer
traditionellen Definition, %(q:planmäßiges Handeln unter Einsatz
beherrschbarer Naturkräfte zur Herbeiführung eines kausal übersehbaren
Erfolges, der ohne Zwischenschaltung menschlicher Verstandestätigkeit
die unmittelbare Folge beherrschbarer Naturkräfte ist).  Eine
%(e:Erfindung) ist eine %(e:Lehre über beherrschbare
Wirkungszusammenhänge von Naturkräften).  Eine Lehre, die sowohl
technische (physische, materielle, konkrete) als auch geistige
(logische, immaterielle, abstrakte) Merkmale aufweist, ist nur dann
eine Erfindung, wenn der als neu und nicht naheliegend beanspruchte
%(e:Kern der Lehre) im Technischen liegt. 
Datenverarbeitungs-Neuerungen sind als Problemlösung innerhalb einer
abstrakten Maschine schon fertig, bevor bei ihrer Ausführung auf einem
Prozessor das Feld der Technik betreten wird.  Ein durch ein
DV-Programm auf bekannten Geräten gesteuerter technischer Prozess
verkörpert genau dann eine patentfähige Erfindung, wenn er auf neue
und nicht naheliegende Weise Naturkräfte zur unmittelbaren
Verursachung einer vorteilhaften Veränderung materieller Gegenstände
nutzt, wobei der Zusammenhang zwischen Ursache und Wirkung nur durch
Versuche an Naturkräften (empirische Verifizierung) und nicht durch
rechnerische Ableitung aus vorbekannten Voraussetzungen
(mathematischen Beweis) zuverlässig überprüft werden kann.

#VKn: Vorschlag von EU-Kommission/BSA für Grenzenlose Patentierbarkeit und
Gegenvorschlag im Geiste des EPÜ

#Ito: In 2002-02-20 the European Commission (CEC) adopted a directive
proposal drafted by BSA (Business Software Alliance), which, by
introducing chaotic legal concepts, removes all limitations on
patentability and thus allows the EPO to grant software and business
method patents as it likes without exposing itself to embarassing
legality discussions.  This is done under a guise of
%(q:clarification) and %(q:harmonisation).  We have written a
counter-proposal which replaces each paragraph of EPO/BSA junktalk
with an honest wording based on the correct interpretation of the EPC
as found in the EPO guidelines of 1978, the BGH caselaw of the 70/80s
and other uncorrupted legal literature.

#Dlu: Dieses vielzitierte Lehrbuch erklärt den Erfindungsbegriff des EPÜ und
die ihm zugrundeliegende Systematik, wie sie sich in zahlreichen
Gerichtsurteilen bis 1986 wiederspiegelt.  Man achte auch auf
Feinheiten wie den Unterschied zwischen einer %(q:Erfindung im Sinne
des Patentrechts) und einer %(q:patentfähigen Erfindung) und die
zwischen einem %(q:technischen Anspruchsgegenstand) und einer
%(q:technischen Erfindung).  Der obige Regelungsvorschlag lehnt sich
in Geist und Formulierung eng an dieses Lehrwerk an, dem wiederum die
Rechtsprechung des BGH und BPatG zum Technikbegriff zugrundeliegt. 
Letztere inspirierte wiederum die Prüfungsrichtlinien des EPA von 1978
und, in leicht verwässerter Form, 1985.

#cvW: enthält eine frühere Fassung des obigen Regelungsvorschlages.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatjavni ;
# txtlang: ja ;
# multlin: t ;
# End: ;

