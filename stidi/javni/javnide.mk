javnide.lstex:
	lstex javnide | sort -u > javnide.lstex
javnide.mk:	javnide.lstex
	vcat /ul/prg/RC/texmake > javnide.mk


javnide.dvi:	javnide.mk javnide.tex
	latex javnide && while tail -n 20 javnide.log | grep references && latex javnide;do eval;done
	if test -r javnide.idx;then makeindex javnide && latex javnide;fi
javnide.pdf:	javnide.mk javnide.tex
	pdflatex javnide && while tail -n 20 javnide.log | grep references && pdflatex javnide;do eval;done
	if test -r javnide.idx;then makeindex javnide && pdflatex javnide;fi
javnide.tty:	javnide.dvi
	dvi2tty -q javnide > javnide.tty
javnide.ps:	javnide.dvi	
	dvips  javnide
javnide.001:	javnide.dvi
	rm -f javnide.[0-9][0-9][0-9]
	dvips -i -S 1  javnide
javnide.pbm:	javnide.ps
	pstopbm javnide.ps
javnide.gif:	javnide.ps
	pstogif javnide.ps
javnide.eps:	javnide.dvi
	dvips -E -f javnide > javnide.eps
javnide-01.g3n:	javnide.ps
	ps2fax n javnide.ps
javnide-01.g3f:	javnide.ps
	ps2fax f javnide.ps
javnide.ps.gz:	javnide.ps
	gzip < javnide.ps > javnide.ps.gz
javnide.ps.gz.uue:	javnide.ps.gz
	uuencode javnide.ps.gz javnide.ps.gz > javnide.ps.gz.uue
javnide.faxsnd:	javnide-01.g3n
	set -a;FAXRES=n;FILELIST=`echo javnide-??.g3n`;source faxsnd main
javnide_tex.ps:	
	cat javnide.tex | splitlong | coco | m2ps > javnide_tex.ps
javnide_tex.ps.gz:	javnide_tex.ps
	gzip < javnide_tex.ps > javnide_tex.ps.gz
javnide-01.pgm:
	cat javnide.ps | gs -q -sDEVICE=pgm -sOutputFile=javnide-%02d.pgm -
javnide/javnide.html:	javnide.dvi
	rm -fR javnide;latex2html javnide.tex
xview:	javnide.dvi
	xdvi -s 8 javnide &
tview:	javnide.tty
	browse javnide.tty 
gview:	javnide.ps
	ghostview  javnide.ps &
print:	javnide.ps
	lpr -s -h javnide.ps 
sprint:	javnide.001
	for F in javnide.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	javnide.faxsnd
	faxsndjob view javnide &
fsend:	javnide.faxsnd
	faxsndjob jobs javnide
viewgif:	javnide.gif
	xv javnide.gif &
viewpbm:	javnide.pbm
	xv javnide-??.pbm &
vieweps:	javnide.eps
	ghostview javnide.eps &	
clean:	javnide.ps
	rm -f  javnide-*.tex javnide.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} javnide-??.* javnide_tex.* javnide*~
tgz:	clean
	set +f;LSFILES=`cat javnide.ls???`;FILES=`ls javnide.* $$LSFILES | sort -u`;tar czvf javnide.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
