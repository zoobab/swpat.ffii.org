\begin{subdocument}{swpatjavni}{Regulaci\'{o} sobre el concepte d'invent dels sistema europeu de patents i la seva interpretaci\'{o} amb especial consideraci\'{o} als programes per a ordinadors}{http://swpat.ffii.org/stidi/javni/swpatjavni.ca.html}{Grup de treball\\swpatag@ffii.org\\versi\'{o} catalana 2003/06/18 per Rafael Carreras\footnote{http://lists.ffii.org/mailman/listinfo/traduk/TradukListinfo.ca.html}}{Proposem que el legislador projecti alguna regulaci\'{o} sobre patentabilitat de programari a trav\'{e}s de les l\'{\i}nies del seg\"{u}ent curt i clar texte.}
\begin{sect}{text}{El Text}
Moguts per entendre que dibuixar la frontera entre el que \'{e}s patentable i el que no ho \'{e}s requereix decisions particularment clares i explicites a nivell legislatiu;

impressionats per l'experi\`{e}ncia que l'Art 52 EPC ha donat lloc a malentesos i inconsist\`{e}ncies a nivell jurisdiccional;

interessats per la perspectiva que noves regles creades per diferents jutjats poden, cada cop m\'{e}s, impedir la difusi\'{o} del coneixement i la innovaci\'{o} a la societat de la informaci\'{o}

clarifiquem el seg\"{u}ent:

\begin{enumerate}
\item
Un ``programa per ordinadors'' \'{e}s una regla de c\`{a}lcul per a una m\`{a}quina Turing o una altra m\`{a}quina abstracta, que es pot expressar a molts nivells de disseny, des d'un pla conceptual a una instrucci\'{o} executable per un hum\`{a} o per un processador. Un programa d'ordinador \'{e}s un pla de construcci\'{o} i una instrucci\'{o} operativa, una descripci\'{o} de proc\'{e}s i una soluci\'{o} a un problema, un treball literari i una m\`{a}quina virtual, un producte i un proc\'{e}s, tot en un.

\item
Un proc\'{e}s patentable controlat per un programa es pot distingir d'un programa de control d'ordinador com a tal.  En contrast, maneres de parlar com ara  ``programa d'ordinador amb [a m\'{e}s] caracter\'{\i}stiques patentables'' nom\'{e}s pot ser ent\'{e}s referit a un programa d'ordinador com a tal, possiblement barrejat amb elements que no s\'{o}n un programa, la patentabilitat dels quals ha de ser examinada per separat.  L'Art 52 (3) s'aplica en id\`{e}ntic sentit a tots els objectes llistats sota (2).

\item
Els programes d'ordinador no s\'{o}n invents en el sentit de patents.  Un proc\'{e}s t\`{e}cnic controlat per un programa (p.e. un proc\'{e}s qu\'{\i}mic) pot ser un invent patentable.  Els drets d'exclusi\'{o} derivats d'aquesta mena d'invent estan limitats a l'\'{u}s industrial del proc\'{e}s de producci\'{o} de b\'{e}ns materials (p.e. qu\'{\i}mics).  En cap cas, una patent es pot fer servir per excloure ning\'{u} de crear, distribuir, vendre o executar un programa d'ordinador.

\item
``La tecnologia'' ens remet a \emph{ci\`{e}ncia natural aplicada} or a \emph{resoldre problemes fent servir forces naturals}, o, d'acord amb una definici\'{o} tradicional: \emph{activitat conforme a un pla per fer servir forces naturals controlables per a aconseguir un \`{e}xit constatable causalment, que sigui, sense mediaci\'{o} de la ra\'{o} humana, l'immediat resultat de forces naturals controlables}.  Un \emph{invent} \'{e}s un \emph{ensenyament sobre relacions controlables causa-efecte de forces naturals}.  Un ensenyament que cont\'{e} caracter\'{\i}stiques tant t\`{e}cniques (f\'{\i}siques, materials) com mentals (l\`{o}giques, inmaterials) \'{e}s un invent nom\'{e}s si la part declarada com a nova i no-\`{o}bvia, \'{e}s a dir, el \emph{cor de l'ensenyament}, pertany al regne de la t\`{e}cnica.  Les innovacions de programari ja estan completes com a solucions que autocontenen el problema dins d'una m\`{a}quina abstracta, abans de comen\c{c}ar la seva execuci\'{o} en un processador de tecnologia prou coneguda.   Un proc\'{e}s t\`{e}cnic controlat per un programa en un maquinari conegut cont\'{e} un invent patentable si i nom\'{e}s si fa servir forces naturals de manera nova i no-\`{o}bvia, per tal de causar directament una transformaci\'{o} avantatjosa d'objectes materials, tal que la relaci\'{o} entre causa i efecte pugui ser validada de manera fiable nom\'{e}s per experimentaci\'{o} amb forces naturals (verificaci\'{o} emp\'{\i}rica) i no per deducci\'{o} computacional de coneixement previ (demostraci\'{o} matem\`{a}tica).
\end{enumerate}
\end{sect}

\begin{sect}{links}{Enlla\c{c}os anotats}
\begin{itemize}
\item
{\bf {\bf Enlla\c{c}os anotats}}
\filbreak

\item
{\bf {\bf \begin{itemize}
\item
{\bf {\bf CEC/BSA Proposal for Unlimited Patentability and Counter-Proposal in the Spirit of the EPC\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/eubsa-swpat0202.ca.html\#prop}}}

\begin{quote}
In 2002-02-20 the European Commission (CEC) adopted a directive proposal drafted by BSA (Business Software Alliance), which, by introducing chaotic legal concepts, removes all limitations on patentability and thus allows the EPO to grant software and business method patents as it likes without exposing itself to embarassing legality discussions.  This is done under a guise of ``clarification'' and ``harmonisation''.  We have written a counter-proposal which replaces each paragraph of EPO/BSA junktalk with an honest wording based on the correct interpretation of the EPC as found in the EPO guidelines of 1978, the BGH caselaw of the 70/80s and other uncorrupted legal literature.
\end{quote}
\filbreak

\item
{\bf {\bf Bernhardt \& Kra{\ss}er 1986: Lehrbuch des Patentrechts\footnote{http://swpat.ffii.org/papri/krasser86/krasser86.de.html}}}

\begin{quote}
Aquest citad\'{\i}ssim manual de lleis de patents explica el concepte d'invent de l'EPC i sistema conceptual subjacent, tal com ha estat expressat en numeroses decisions judicials fins el 1986.   Cont\'{e} tamb\'{e} intrincats detalls com ara la difer\`{e}ncia entre un ``invent en the sentit de patent'' i un ``invent patentable'' i entre un ``objecte de t\'{\i}tol t\`{e}cnic'' i una ``invenci\'{o} t\`{e}cnica''.  La regulaci\'{o} abans citada est\`{a} modelada de manera molt semblant a com est\`{a} fet aquest manual, que novament est\`{a} basat en la jurisprud\`{e}ncia de la Cort Federal Alemana i la Cort Federal de Patents, que tamb\'{e} est\`{a} basada en les l\'{\i}nies mestres del dictamen de l'EPO del 1978 i lleugerament dilu\"{\i}da, del 1985.
\end{quote}
\filbreak

\item
{\bf {\bf Technical Invention\footnote{http://swpat.ffii.org/stidi/korcu/swpatkorcu.de.html}}}

\begin{quote}
So far computer programs and other \emph{rules of organisation and calculation} are not \emph{patentable inventions} according to European law.  This doesn't mean that a patentable manufacturing process may not be controlled by software.  However the European Patent Office and some national courts have gradually blurred the formerly sharp boundary between material and immaterial innovation, thus risking to break the whole system and plunge it into a quagmire of arbitrariness, legal insecurity and dysfunctionality.  This article offers an introduction and an overview of relevant research literature.
\end{quote}
\filbreak

\item
{\bf {\bf BGH-Beschluss ``Dispositionsprogramm'' 1976-06-22\footnote{http://swpat.ffii.org/papri/bgh-dispo76/bgh-dispo76.de.html}}}

\begin{quote}
A landmark decision of the German Federal Court (BGH): 'organisation and calculation programs for computing machines used for disposition tasks, during whose execution a computing machine of known structure is used in the prescribed way, are not patentable.'  This is the first and most often quoted of a series of decisions of the BGH's 10th Civil Senate, which explain why computer-implementable rules of organisation and calculation (programs for computers) are not technical inventions, and elaborates a methodology for analysing whether a patent application pertains to a technical invention or to a computer program.  The Dispositionsprogramm verdict is especially famous for general and almost prophetic terms in which it explains that patent law is a variant of copyright for a specialised context, namely that of solving problems by the use of controllable forces of nature.  Any attempt to ``loosen and thereby in fact abolish'' the concept of technical invention would lead onto a forbidden path, the judges warn.
\end{quote}
\filbreak

\item
{\bf {\bf EPO 1978: Examination Guidelines\footnote{http://swpat.ffii.org/papri/epo-gl78/epo-gl78.en.html}}}

\begin{quote}
Adopted by the President of the European Patent Office in accordance with EPC 10.2a with effect from 1978-06-01.  Excerpts concerning the question of technical invention, limits of patentability, computer programs, industrial application etc.
\end{quote}
\filbreak

\item
{\bf {\bf Art 52 CPE:  Interpretaci\'{o} i Revisi\'{o}.\footnote{http://swpat.ffii.org/stidi/cpe52/epue52.ca.html}}}

\begin{quote}
Els limits del que es patentable que van ser establerts a la Convenci\'{o} por il Patent Europeu (CPE) de 1973, han estat ignorats al llarg dels anys. Els principals tribunals de patents han interpretat l'article 52 d'una manera que pr\`{a}cticament el fan ja inutil. Nombrosos professors de dret han intentat demostrar perque no s'hauri\`{a} de permetre aix\`{o}. L'OEP ha proposat de revisar l'article 52 per posar-lo d'acord amb la seva l\'{\i}nia de patentibilitat il$\cdot$limitada. Hom podria fer no obstant el contrari, regular la patentabilitat segons les l\'{\i}nies originals de l'article 52, per\`{o} per limitar les possibilitats d'ab\'{u}s. Aquest document explora el que ja ha passat i que podem fer.
\end{quote}
\filbreak

\item
{\bf {\bf European Consultation on the Patentability of Computer-Implementable Rules of Organisation and Calculation (= Programs for Computers)\footnote{http://swpat.ffii.org/papri/eukonsult00/eukonsult00.en.html}}}

\begin{quote}
On 2000-10-19 the European Commission's Industrial Property Unit published a position paper which tries to describe a legal reasoning similar to that which the European Patent Office has during recent years been using to justify its practise of granting software patents against the letter and spirit of the written law, and called on companies and industry associations to comment on this reasoning.  The consultation was evidently conceived as a mobilisation exercise for patent departments of major corporations and associations.  The consultation paper itself stated the viewpoint of the European Patent Office and asked questions that could only be reasonably answered by patent lawyers.  Moreover, it was accompanied by an ``independent study'', carried out under the order of the EC IndProp Unit by a well known patent movement think-tank, which basically stated the same viewpoint.  Patent law experts of various associations and corporations responded, mostly by applauding the paper and explaining that patents are needed to stimulate innovation and to protect the interests of small and medium-size companies.  However there were also quite a few associations, companies and more than 1000 individuals, mostly programmers, who expressed their opposition to the extension of patentability to the realm of software, business methods, intellectual methods and other immaterial products and processes.  The EC IndProp Unit later failed to adequately publish the consultation results and moderate a discussion.  Therefore we are doing this, and you can help us.
\end{quote}
\filbreak

\item
{\bf {\bf}}

\begin{quote}
cont\'{e} una versi\'{o} anterior de la proposta de regulaci\'{o} citada.
\end{quote}
\filbreak

\item
{\bf {\bf European Commission will propose to replace clear limits on patentability with empty words\footnote{http://www.eurolinux.org/news/warn01C/warn01C.ca.html}}}

\begin{quote}
The Eurolinux Alliance of software companies and non-profit associations has been informed by reliable sources that the European Commission (EC) will publish within a few days a draft proposal for a European Community Directive on the limits of patentability with regard to computer programs.  Most programmers want not patents but only copyright to apply to software.  Evidence from economic studies suggests that software patents stifle innovation and reduce productivity.  The EC will pay verbal tribute to this reality in its press release.  However, in its directive draft, the EC will propose to legalise US-style software patents in Europe and to remove all effective limits on patentability.  With some reading skills in european patent lingo, you will easily notice this discrepancy.  If you can afford 20 minutes, we will teach you the basics and introduce you to a debate, which is likely to stir unusual political passions at least for the next 1-2 years, as the directive draft attempts to pass through the European Parliament and the European Council.
\end{quote}
\filbreak

\item
{\bf {\bf Peticions d'acci\'{o}\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/peticions/eubsa-cpedu.ca.html}}}

\begin{quote}
Llista d'accions que ha de fer el Parlament Europeu i d'altres institucions
\end{quote}
\filbreak
\end{itemize}}}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

