<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Regulació sobre el concepte d'invent dels sistema europeu de patents i la seva interpretació amb especial consideració als programes per a ordinadors

#descr: Proposem que el legislador projecti alguna regulació sobre patentabilitat de programari a través de les línies del següent curt i clar texte.

#Gte: Moguts per entendre que dibuixar la frontera entre el que és patentable i el que no ho és requereix decisions particularment clares i explicites a nivell legislatiu

#Isa: impressionats per l'experiència que l'Art 52 EPC ha donat lloc a malentesos i inconsistències a nivell jurisdiccional

#Ihf: interessats per la perspectiva que noves regles creades per diferents jutjats poden, cada cop més, impedir la difusió del coneixement i la innovació a la societat de la informació

#wer: clarifiquem el següent:

#Unr: Un %(q:programa per ordinadors) és una regla de càlcul per a una màquina Turing o una altra màquina abstracta, que es pot expressar a molts nivells de disseny, des d'un pla conceptual a una instrucció executable per un humà o per un processador. Un programa d'ordinador és un pla de construcció i una instrucció operativa, una descripció de procés i una solució a un problema, un treball literari i una màquina virtual, un producte i un procés, tot en un.

#Dco: Un procés patentable controlat per un programa es pot distingir d'un programa de control d'ordinador com a tal.  En contrast, maneres de parlar com ara  %(q:programa d'ordinador amb [a més] característiques patentables) només pot ser entés referit a un programa d'ordinador com a tal, possiblement barrejat amb elements que no són un programa, la patentabilitat dels quals ha de ser examinada per separat.  L'Art 52 (3) s'aplica en idèntic sentit a tots els objectes llistats sota (2).

#Pdd: Els programes d'ordinador no són invents en el sentit de patents.  Un procés tècnic controlat per un programa (p.e. un procés químic) pot ser un invent patentable.  Els drets d'exclusió derivats d'aquesta mena d'invent estan limitats a l'ús industrial del procés de producció de béns materials (p.e. químics).  En cap cas, una patent es pot fer servir per excloure ningú de crear, distribuir, vendre o executar un programa d'ordinador.

#UWg: %(q:La tecnologia) ens remet a %(e:ciència natural aplicada) or a %(e:resoldre problemes fent servir forces naturals), o, d'acord amb una definició tradicional: %(e:activitat conforme a un pla per fer servir forces naturals controlables per a aconseguir un èxit constatable causalment, que sigui, sense mediació de la raó humana, l'immediat resultat de forces naturals controlables).  Un %(e:invent) és un %(e:ensenyament sobre relacions controlables causa-efecte de forces naturals).  Un ensenyament que conté característiques tant tècniques (físiques, materials) com mentals (lògiques, inmaterials) és un invent només si la part declarada com a nova i no-òbvia, és a dir, el %(e:cor de l'ensenyament), pertany al regne de la tècnica.  Les innovacions de programari ja estan completes com a solucions que autocontenen el problema dins d'una màquina abstracta, abans de començar la seva execució en un processador de tecnologia prou coneguda.   Un procés tècnic controlat per un programa en un maquinari conegut conté un invent patentable si i només si fa servir forces naturals de manera nova i no-òbvia, per tal de causar directament una transformació avantatjosa d'objectes materials, tal que la relació entre causa i efecte pugui ser validada de manera fiable només per experimentació amb forces naturals (verificació empírica) i no per deducció computacional de coneixement previ (demostració matemàtica).

#VKn: CEC/BSA Proposal for Unlimited Patentability and Counter-Proposal in the Spirit of the EPC

#Ito: In 2002-02-20 the European Commission (CEC) adopted a directive proposal drafted by BSA (Business Software Alliance), which, by introducing chaotic legal concepts, removes all limitations on patentability and thus allows the EPO to grant software and business method patents as it likes without exposing itself to embarassing legality discussions.  This is done under a guise of %(q:clarification) and %(q:harmonisation).  We have written a counter-proposal which replaces each paragraph of EPO/BSA junktalk with an honest wording based on the correct interpretation of the EPC as found in the EPO guidelines of 1978, the BGH caselaw of the 70/80s and other uncorrupted legal literature.

#Dlu: Aquest citadíssim manual de lleis de patents explica el concepte d'invent de l'EPC i sistema conceptual subjacent, tal com ha estat expressat en numeroses decisions judicials fins el 1986.   Conté també intrincats detalls com ara la diferència entre un %(q:invent en the sentit de patent) i un %(q:invent patentable) i entre un %(q:objecte de títol tècnic) i una %(q:invenció tècnica).  La regulació abans citada està modelada de manera molt semblant a com està fet aquest manual, que novament està basat en la jurisprudència de la Cort Federal Alemana i la Cort Federal de Patents, que també està basada en les línies mestres del dictamen de l'EPO del 1978 i lleugerament diluïda, del 1985.

#cvW: conté una versió anterior de la proposta de regulació citada.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatjavni ;
# txtlang: ca ;
# multlin: t ;
# End: ;

