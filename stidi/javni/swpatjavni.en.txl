<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Regulation about the invention concept of the European patent system and its interpretation with special regard to programs for computers

#descr: We propose that the legislator draft any regulations on the question of software patentability along the lines of the following short and clear text.

#Gte: Led by the understanding that drawing the borderline between patentable and non-patentable objects requires particularly clear and explicit decisions at the legislative level

#Isa: impressed by the experience that Art 52 EPC has left room for misunderstandings and inconsistencies at the jurisdictional level

#Ihf: concerned by the prospect that new rules created by various lawcourts could increasingly impede knowledge diffusion and innovation in the information society

#wer: we clarify the following:

#Unr: A %(q:program for computers) or %(q:computer program) is a calculation rule for a Turing machine or other abstract machine, which can be exressed at many design levels, from a conceptual plan to an instruction executable by a human or by a processor.  A computer program is a building plan and an operating instruction, a description of a process and a solution to a problem, a literary work and a virtual machine, a product and a process, all in one.

#Dco: A program-controlled patentable process can be distinguished from the controlling computer program [as such].  In contrast, wordings such as %(q:computer program with [further] patentable features) can only be understood to refer to a computer program [as such], possibly mingled with non-program elements whose patentability need to be examined separately.  Art 52 (3) applies in an identical sense to all objects listed under (2).

#Pdd: Computer Programs are not inventions in the sense of patent law.  A program-controlled technical process (e.g. chemical process) can be a patentable invention.  The exclusion rights derived from such an invention are limited to the industrial use of the process for production of material goods (e.g. chemicals).  In no case may a patent claim be used to exclude anyone from creating, distributing, selling or executing a computer program.

#UWg: %(q:Technology) refers to %(e:applied natural science) or %(e:solving problems by utilising natural forces), or, according to a traditional definition: %(e:plan-conformant activity of using controllable natural forces to achieve a causally overseeable success which is, without mediation by human reason, the immediate result of controllable natural forces).  An %(e:invention) is a %(e:teaching about controllable cause-effect relations of natural forces).  A teaching that contains both technical (physical, material) and mental (logical, immaterial) features is a invention only if the part that is claimed to be new and non-obvious, i.e. the %(e:core of the teaching), lies in the technical realm.  Software innovations are already completed as self-contained problem solutions within an abstract machine before during their execution on a processor the technical realm is set foot upon.   A technical process controlled by a computer program on known hardware embodies a patentable invention if and only if it uses natural forces in a new and non-obvious way to directly cause an advantageous transformation of material objects, such that the relation between cause and effect can be reliably validated only by experimentation with natural forces (empirical verification) and not by computational deduction from prior knowledge (mathematical proof).

#VKn: CEC/BSA Proposal for Unlimited Patentability and Counter-Proposal in the Spirit of the EPC

#Ito: In 2002-02-20 the European Commission (CEC) adopted a directive proposal drafted by BSA (Business Software Alliance), which, by introducing chaotic legal concepts, removes all limitations on patentability and thus allows the EPO to grant software and business method patents as it likes without exposing itself to embarassing legality discussions.  This is done under a guise of %(q:clarification) and %(q:harmonisation).  We have written a counter-proposal which replaces each paragraph of EPO/BSA junktalk with an honest wording based on the correct interpretation of the EPC as found in the EPO guidelines of 1978, the BGH caselaw of the 70/80s and other uncorrupted legal literature.

#Dlu: This much-cited German patent law manual explains the invention concept of the EPC and the underlying conceptual system, as it has been expressed in numerous court decisions until 1986.   Note also intricate details such as the difference between an %(q:invention in the sense of patent law) and a %(q:patentable invention) and that between a %(q:technical claim object) and a %(q:technical invention).  The above-cited regulation proposal is closely modelled on the teaching and wording of this manual, which again is based on the jurisdiction of the German Federal Court and Federal Patent Court, which again is the basis of the EPO's examination guidelines of 1978 and, slightly diluted, 1985.

#cvW: contains an earlier version of the above regulation proposal.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatjavni ;
# txtlang: en ;
# multlin: t ;
# End: ;

