<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Software Patents: Questions, Analyses, Proposals

#descr:: What is the effect of patents on the economy in general and on
software in particular?  Why do software patents tend to be so
trivial?  What exactly have the rules of patentability in Europe been
and how did they change?  Under what constraints is the patent system
moving?  What are our choices?  With this collection of articles,
members and friends of the FFII workgroup on software patents try to
give answers.

#Dcn: The Basic Questions

#WeF: Other serious attempts at formulating questions

#VKd: Various governmental agencies have conducted consultations about the
patentability of computer programs, but so far in most cases wrong
questions were asked, leading to virtual debates and largely useless
studies.  We think that a serious debate should answer the following
questions.

#Bia: In how many % of the %(ep:app. 30000 software patents granted so far
by the EPO) is the claimed achievement is so impressive, that it seems
at least worth discussing whether we as legislators might want to
grant a temporary monopoly for such an achievement?

#FKW: Please cite the main claims of a few typical EPO software patents to
illustrate your statement!

#Wei: What do the EPO software patents usually claim, what effort does the
development of the claimed idea take and how much later (earlier?)
would it spread and sprout if we didn't reward its publication with a
patent?

#Llu: Is the software industry's innovativity suffering from too rapid
imitation?

#BtW: Does software copyright need to be improved or supplemented by patent
law or similar?

#Wge: What would an optimal %(sg:specially-tailored software rewarding law)
look like?

#WSc: Why is there a phenomenon of Free Software but not free hardware?

#Wnv: How does the economics of immaterial goods differ from that of
material goods?

#dea: Which role could/should Free Software play in the information society?

#Upr: Under which rules can proprietary and free software prosper together
productively?

#NWP: According to which rules do European courts (EPO Technical Board of
Appeal, German Federal Court and Federal Patent Court) judge the
patentability of software?

#See: Art these rules clear?

#Wen: Where have they been formulated in the clearest terms?

#NWW: According to which rules did European courts judge the patentability
of software in 1985?

#Wel: Were these rules clear?

#Ggi: Are there any international treaties or other legal constraints that
limit our legislative options concerning software patents, either by
requiring software patentability or by limiting it or linking it to
unalterable conditions?

#Wet: Which clear delimiting rules of patentability or patent enforcability
are there for us to choose from today?

#WaA: How many % of the total number of EPO patents (hard and software)
would be confirmed as valid and enforcable, if each of these rules was
applied?

#Wee: What do you think of the following possible options?

#Brr: Scope of patentable ideas

#PWW: practical and repeatable problem solutions

#aun: All kinds of ideas can be patented, provided that they are repeatable
independently of participating persons and achieve a determined effect
in the material world.  Purely mathematical solutions are acceptable,
but the claims must be tied to practical applications, potentially
leading to very long lists of claims.  This doctrine is favored by
leading theoreticians of both the USPTO and the EPO.

#Tne: The Technical Field = Applied Natural Science

#LtE: The %(q:technical field) is the field of engineering aka applied
natural science.  An invention must teach a new way of using natural
forces to directly achieve a tangible result.  %(q:Computational or
organisational rules) designed to solve problems posed by mathematical
models (such as the Turing machine) are not technical in this sense,
no matter whether the model in question can be mapped onto a physical
structure or not.  This doctrine was developped mainly by German
lawcourts up to the 80s, and it can be found in law commentaries,
examination guidelines and court decisions up to the year 2000.  The
Eurolinux Alliance %(ek:demands) that this doctrine be systematically
applied in the future.

#Gan: Abstractions in their Original Form

#Dni: This doctrine allows even the patenting of purely abstract
mathematical methods without a practical application.  The question of
patentability is no longer asked.  The discussion is shifted to
enforcability:  what kind of objects can constitute a patent
infringement?

#Sir: Dynamic Concept of Technical Character

#Bar: Apply all three doctrines in an case-by-case approach, drift toward
unlimited patentability in helical movements.

#Zfr: For the sake of %(q:legal security) (= prevention of resistance from
%(q:conservative) lawcourts), confirm this course from time to time by
fuzzy laws and directives.

#BWg: Some unfriendly commentators claim that this is what interested patent
lawyers mean when they praise the advantages of a %(q:dynamic concept
of technical character).

#BWs: Scope of Infringement Activities

#Iee: Information goods are recognised as part of public communication
process.  They may not be touched by patents, no matter what is
written in the patent claim and no matter whether they are technical
or not.  Only industrially produced material goods (marginal
production cost > 0) can infringe on patents.  Not only the private
sphere but also the sphere of public communication, including direct
application of information goods on an ordinary computer, is
off-limits to patent litigation.  The question on whether claims to
abstract-logical constructions should be permissible or not is kept
separate from this issue.  Any combination with one of the above
doctrines is possible.

#Eng: Difficulty of Invention

#End: Some peole are looking for criteria and game rules that would make it
possible to eliminate trivial patents.  E.g. the Dutch parliament has
requested that this problem must be solved, before the patentability
of software can be discussed.

#Oia: Organisation of the Patent System

#SWe: For decades it has often been criticised that the patent examination
system is not up to its task of quickly and efficiently sorting out
unjustified patent claims, and that patent institutions have
developped an expansive tendency that leads to more and more
questionable patents.  The French deputy Jean Le Déaux has demanded
that a commission be appointed with investigating the abuses in the
European patent institutions and working out recommendations for an
institutional reform.  According to this view, the key to improvement
is not so much in the legal rules or patenting as in the institutional
framework in which thes rules are applied.

#Wji: Assuming these rules were codified as European Directives, what would
happen to those patents that could no longer be confirmed as valid?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatstidi ;
# txtlang: en ;
# multlin: t ;
# End: ;

