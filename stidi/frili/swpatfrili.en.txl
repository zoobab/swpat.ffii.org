<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Why are Software Patents so Trivial?

#descr: Most software patents are trivial and broad.  The reasons are to be found in the patent system itself rather than in the insufficiencies of the examination process.  On the one hand, the requirement of %(q:non-obviousness) is not and can not be as performant as many %(q:patent reform) proponents seem to wish.  It can work only as one (weak) filter among several, which in combination serve to raise the overall score rate of the patent system, i.e. the ratio of %(q:good patents).  The extension of patentability to software comes at the expense of several needed filters.  The system has thereby been so badly compromised, that, no matter how the non-obviousness screw is turned, it is unlikely that its score ratio can be raised back to a level that could justify its continued existence.

#Aae: A problem of law, not of patent examination

#Whe: Why not just raise the %(q:inventive step) standard?

#HWW: The broken regulative: technicity, industriality

#Fea: Further Reading

#Aru: By browsing through our %(hg:European Software Patent Database), you can easily compile a %(q:horror gallery) of impressively trivial and broad patents.  It will be much more difficult to find even a single claim object that a programmer would find admirable and possibly worthy of a special exclusion right.

#Sqo: Some people say that this is because the patent offices do not do their job well.  If the criteria of %(q:novelty) and %(q:inventive step) aka %(q:non-obviousness) were applied 100% correctly, these people say, software patents wouldn't do much harm.  Some have been prophesying for a decade that is only a matter of time until this problem will be solved.  However the solution seems further away than ever.

#Weo: The following dialogue between Ramon Garcia Fernandez, a spanish information scientist, and Steve Probert, deputy director of the British Patent Office, %(pa:conducted 2000-12-20 on a publicly archived mailing list), goes right to the heart of the problem:

#MWt: My disagreement with software patents is the philosophy of most patent offices. For me, a patent should be granted only where the research needed for the invention is expensive, so that said research would not be posible without the incentive of the monopoly. By contrast, the opinion of patent offices seems to be that the inventor should somehow have natural right to monopoly. They seem to think that the invention is intrinsically a very difficult process and that all incentive to it is small (I would like to hear Steve's opinion).  By contrast, the invention of a method to solve a problem is part of the daily work of a programmer.

#Ide: I don't really want to express an opinion because I think that would be wrong for me as a Civil Servant and an official of the Patent Office.  What I would say is that the patent system has never differentiated between inventions on the basis of how much the underlying research cost.  It doesn't even distinguish between those inventions that are truly valuable and ground-breaking, and those that are [comparatively] trivial and insignificant. As the law stands, we would not be able to do so even if we wanted to. Only if an invention is known or obvious can we raise a legal objection.

#TWa: To some extent the system should be proportionate and self-regulating - in industries where the research costs are very high (eg pharmaceuticals), the patents will be more valuable.  (So licencing costs will be higher etc.)  OTOH, where the development costs are lower it will often (but not always, I accept) be cheaper to work around a patent.  In such cases the patent will be worth less.  In other words what I am saying is that the value of a twenty year monopoly varies depending on a number of factors, one of which is the typical cost of research in the field.  Setting arbitrary thresholds based on research costs would effectively discriminate against individuals and smaller companies in favour of the bigger companies who will always spend more on research.  And who would decide how much money needed to be spent to make a particular invention?  (I presume you would not be content to determine the fate of a patent application on the basis of how much was actually spent.)

#Psg: Probably the most important problem here is communication between non-programmers (lawyers, patent officials and so on) and programmers.  It is probably very difficult to convince the former about what kind of things are easy for programmers, such as having ideas, and what kind of things are difficult and time consuming.

#Imr: I cannot speak for lawyers, but I can assure you that many Patent Examiners are programmers themselves.  In my group, all the Patent Examiners who deal with software applications either write computer programs in their spare time or have been employed as programmers before they became patent examiners.  They usually have a pretty good idea whether something would have been easy or time consuming for a programmer.  However, they might express the communication problem the other way around - it's very difficult to persuade programmers that just because an invention is %(q:easy), does not make it any less patentable.

#ftp: As can be learnt from this dialog, the non-obviousness criterion is not what the naive engineer or programmer thinks it is.

#jie: Criteria such as %(q:novelty), %(q:non-obviousness), %(q:technicity), %(q:industriality) may seem intuitively reasonable, and the naive patent professional may believe that they are the very yardsticks of reason.

#Wna: The naiveté of both the engineer and the patent professional consists in forgetting that %(q:the law is an ass), as the proverb goes.

#nip: The primary function of the %(q:non-obviousness) criterion is not to assure what an engineer or programmer thinks is a reasonable %(q:inventive height), but to provide a filter that can be applied by patent examiners in a predictable way.

#sst: Applicability and reasonability are two entirely different requirements, which need not not match.

#cWi: It is enough if the %(q:non-obviousness) filter helps, in combination with other filters, to yield a reasonable amount of %(q:good patents) in the end.  Patents will be deemed %(q:good) if the scope of exclusion is felt to be %(ol|sufficiently broad to allow the patent owner to at least earn back the %(tp|patenting costs|costs of patent acquisition and enforcement, disadvantage incurred by disclosure)|sufficiently narrow to %(tp|avoid imposing unjustified costs on competitors|e.g. to make inadvertent infringment unlikely).)  The %(q:non-obviousness) filter is a means of raising the score rate of the patent system, i.e. the percentage of %(q:good patents).

#atW: We have proposed %(ts:means of estimating this score rate for various fields).  It seems to be well below 50% in most fields and below 1% when it comes to software and business methods.

#Tni: The Fernandez-Probert dialog illustrates what everybody in the patent trade knows:  the criterion of %(q:inventive step) as it stands is not designed to sort out trivial patents by itself, but to help, in combination with other filters, to raise the score rate of the patent system.

#Inr: It is very difficult to prove that even the most trivial new idea does not contain an inventive step.  The %(ep:EPO's Examination Guidelines of 2001) even admonish examiners to be very critical of such proofs, apparently for good reasons:

#WWs: It should be remembered that an invention which at first sight appears obvious might in fact involve an inventive step. Once a new idea has been formulated it can often be shown theoretically how it might be arrived at, starting from something known, by a series of apparently easy steps. The examiner should be wary of ex post facto analysis of this kind.

#Wlt: The EPO basically treats %(q:inventive step) as an extension of %(q:novelty).  In order to prove %(q:lack of novelty), an opponent must point to one single prior art document whose teaching falls into the scope of the claim.   If no single document is found, the opponent will try to show that the person skilled in the art would have arrived at the teaching by combining two documents.  In this case the claimed invention is said to be unpatentable due to %(q:lack of inventive step).

#Ine: In the case of software patents, the person skilled in the arts rarely even consults documents.  New programming problems occur all the time, and %(q:inventing) a solution on the fly is the normal way to go.  Most such solutions are not even published in any information science journal that an examiner might consult for testing their novelty.  Rather, the program will usually also function as its own publication --- a form of publication which can pose a severe challenge for the novelty concept of the patent system.

#Eer: Yet not every innovation in the field of computing is produced on the fly, and some major advances are discussed in some highly-respected journals.  So, shouldn't we try to raise the inventivity standard, so that those really resepectable achievements can be singled out for rewarding by a patent (or a patent-like exclusion right) ?

#off: Maybe.  It sounds almost as tempting as %(q:lasting world peace) and %(q:real socialism).

#Egm: Even if lawmakers wanted to seriously tinker with the existing patent system and %(q:raise the inventivity standard), as has become a mantra in some %(q:patent reform) proposals, they would encounter enormous practical difficulties:

#fft: fuzziness of %(q:invention height)

#HiW: How would they define %(q:inventive height) (Erfindundhöhe), as it is still sometimes called in old-fashioned German patent jargon?  %(q:At least 20 inches above the state of the art)? %(q:At least %(tp|20 twists of the brain|= 20 Lemelson ?) beyond the state of the art)?

#Oev: It has been %(sb:suggested) that a social game could help here:  let the patent applicant first publish only his problem and provide incentives for the public to submit solutions until a certain deadline.  All these solutions are then considered to be prior art.  This could really work, but it is a very radical reform proposal for a patent system which is governed by strong forces of inertia, who will always find forceful legal arguments against any even very moderate reform proposal that has a true potential of diminishing the number of patents granted.

#tye: triviality by sequentiality

#AWf: Abstract-logical constructions are usually composed of many small innovation steps that neatly build on each other to form a perfect whole.  Each patent will usually focus on one of these logical steps, thus making this patent trivial and broad, even if the innovation itself was truly ingenuous.  This has happened e.g. in the case of the %(mp:MP3 patents).  Even assuming it was feasible to narrow down the claim scope of the MP3 patents by applying %(q:strong non-obviousness requirements), this would then probably result in too narrow claims, i.e. claims that don't allow the rightholder to earn back the patenting costs.

#nek: Incommensurate blocking effect due to non-triviality

#WWW: In the (very rare) case of fundamental breakthroughs in abstraction (e.g. the %(kn:Karmarkar inner-point optimisation method)), it may be impossible to define a claim scope that is both rewarding to the applicant and not too burdensome on follow-on innovation at the same time.  This is the main reason why mathematics and discoveries have been excluded from patentability.

#TyT: Traditionally there has been one other criterion that has helped if not to sort out trivial ideas then at least to significantly lower the ratio of trivial patents:  the requirement of %(q:technicity) or %(q:industriality), which limits the patent system to applied natural science and matter-producing industries, more precisely defined as the %(e:requirement that forces of nature be part of the problem solution which is rewarded by the patent).  In most countries of the world this requirment has in one or the other wordings been part of the patent law tradition at least until recently.  The European Parliament has reaffirmed this requirement in September 2003 by %(ep:voting) for a strict definition of the concepts %(q:invention), %(q:technical) and %(q:industrial) along these lines.  This criterion excludes those %(q:post-industrial) innovations that are based only on abstract calculus and do not require experimentation.  Finding a new causal relation between natural forces and a physical effect is usually much more costly than finding a new mathematical relation.

#WiW: While mathematical relations are composed of tiny functional elements that combine to a perfect whole, the physical world is causal rather than functional, and the whole is not the sum of the parts.  Material phenomena may be described by mathematics, but such description is at best an approximation.  Even a system of lego bricks will usually not work out the way you you may have built it in your mind.  The more the system becomes complex, the more deviations can accumulate into unforeseeable effects.  Undisturbed cleanrooms exist only in the world of ideas.

#PWe: When the patent system is no longer limited to industrially applied natural science, its score rate plummets.  Software is an art of abstraction and software patents come as a result of an opening of the patent system toward the abstract and functional, a proliferation of %(q:function claims), i.e. the patenting of unspecified %(q:means) to achieve some so-called %(q:technical effects).  This means that problems and not solutions are claimed.   Since no new causal chain between material means and material results is involved, it becomes difficult for the patent applicant to claim his %(q:invention).  It will usually be neither permissible nor economically rewarding to claim the specific mental steps by which a computing problem is solved.  Rather, the patentee will try to claim the input and output (i.e. the %(q:technical effects)) of the operation.  However, unlike in chemical patents, there are no %(q:surprising effects) to be claimed.  Everybody knows that a computer can output calculation results to the screen.   The only difficulty lies in knowing %(e:how to tell the computer to do it), and that is routinely left to the thousands of creative programmers, who, if allowed to do so, could independently devise hundreds of different creative solutions, all of which produce the claimed %(q:technical effect), but none or few of which are disclosed in the patent description.  These and similar problems have been analysed in detail by some of the %(kk:patent examiners who are struggling with them).  There is moreover a literature of mathematicians and epistemologists who explain in detail why %(pn:the models are broken) when the requirement of technical character (concreteness and physical substance) is given up.  The german Federal Court of Justice already warned of this in the concluding remarks of its %(dp:Disposition Program decision) of 1976, which laid the foundations for the non-patentability of software in Germany:

#Sai: However in all cases the plan-conformant utilisation of controllable forces of nature has been named as an essential precondition for asserting the technical character of an invention.  As shown above, the inclusion of human mental forces as such into the realm of the forces of nature, on whose utilisation in creating an innovation the technical character of that innovation is founded, would lead to the consequence that virtually all results of human mental activity, as far as they constitute an instruction for plan-conformant action and are causally overseeable, would have to be attributed a technical meaning.  In doing so, we would however de facto give up the concept of the technical invention and extend the patent system to a vast field of achievements of the human mind whose essence and limits can neither be recognized nor overseen.

#Dnu: [...] from a purely objective point of view the concept of technical character seems to be the only usable criterion for delimiting inventions against other human mental achievements, for which patent protection is neither intended nor appropriate.  If we gave up this delimitation, there would for example no longer be a secure possibility of distinguishing patentable achievements from achievements, for which the legislator has provided other means of protection, especially copyright protection.  The system of German industrial property and copyright protection is however founded upon the basic assumption that for specific kinds of mental achievements different specially adapted protection regulations are in force, and that overlappings between these different protection rights need to be excluded as far as possible.  The patent system is also not conceived as a reception basin, in which all otherwise not legally privileged mental achievements should find protection.  It was on the contrary conceived as a special law for the protection of a delimited sphere of mental achievements, namely the technical ones, and it has always been understood and applied in this way.

#Ene: Any attempt to attain the protection of mental achievements by means of extending the limits of the technical invention -- and thereby in fact giving up this concept -- leads onto a forbidden path.  We must therefore insist that a pure rule of organisation and calculation, whose sole relation to the realm of technology consists in its usability for the normal operation of a known computer, does not deserve patent protection.  Whether it can be awarded protection under some other regime, e.g. copyright or competition law, is outside the scope of our discussion.

#Ton: As foreseen by the Federal Court in 1976, the introduction of software patents has opened a can of worms.  It has not only removed the only viable criteria for limiting the scope of patentable subject matter, but also broken the models of the patent system on which requirements such as novelty, non-obviousness and enabling disclosure are built.  It has overturned the balance of the patent system, leaving it behind in a state of inconsistency and dysfunctionality.

#WWm: The European Parliament's vote has given the patent community a chance to repair its system.  By reintroducing the requirement of concreteness and physical substance (technical character), the score rate of the system can perhaps be brought back to acceptable levels.  Due to the unwieldiness of the system, there may not be many such chances.  If this chance is missed, people in a large majority of disciplines may soon be voicing doubts about the legitimacy of the whole system and pressing for radical reform in unforeseeable directions.  Judging from the cyclical movements of the patent system during the last two centuries, it would not be surprising if a failure by the patent institutions to seize their chance could open the way to the strongest anti-patent wave that history has seen so far.

#Coi: Contains a long chapter on the triviality problem.  Quotes patent lawyers who are encouraging their customers to patent anything that seems useful and forget about the idea that patents are about inventions.  They are not.  Especially in software, you have to forget that old prejudice quickly, if you want to play the patent game.

#Eoo: Enlightening Advice of a Patent Lawyer to Software Companies

#Tsg: The author's indignation with the patent examination process leads him to sharply analyse many present-day problems.  Whether these problems can, as Aharonian suggests, be solved by a mere improvement of the examination process, seems questionable.  The indignation may be a result of wrong expectations due to the author's firm belief in the universal applicability of the patent system.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatfrili ;
# txtlang: en ;
# multlin: t ;
# End: ;

