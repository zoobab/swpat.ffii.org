\begin{subdocument}{swpatfrili}{Warum sind Softwarepatente so trivial?}{http://swpat.ffii.org/analyse/frili/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Die meisten Softwarepatente sind trivial und breit.  Die Ursachen hierf\"{u}r ist im Patentsystem selbst und nicht etwa in Unzul\"{a}nglichkeiten bei der Patentpr\"{u}fung zu suchen.  Einerseits l\"{a}sst das Erfordernis der Erfindungsh\"{o}he (Nichtnaheliegen) schwer fassen, und bisherige Versuche einer Formalisierung dieses Erfordernisses haben lediglich zu seiner tendenziellen Abschaffung beigetragen.  Andererseits haben die Gerichte mit der Einf\"{u}hrung von Softwarepatenten den Marsch in die totale Trivialit\"{a}t freigegeben, indem sie das einzige verbleibende Bollwerk niederrissen: das Erfordernis der Technizit\"{a}t.}
\begin{sect}{sys}{A problem of law, not of patent examination}
By browsing through our European Software Patent Database\footnote{http://swpat.ffii.org/patente/index.de.html}, you can easily compile a ``horror gallery'' of impressively trivial and broad patents.  It will be much more difficult to find even a single patent on a method that a programmer would consider worthy of admiration.

Some people say that this is because the patent offices do not do their job well.  If the criteria of ``novelty'' and ``inventive step'' aka ``non-obviousness'' were applied 100\percent{} correctly, these people say, software patents wouldn't do much harm.  Some have been prophesying for a decade that is only a matter of time until this problem will be solved.  However the solution is further away than ever.

The following dialogue between Ramon Garcia Fernandez, a spanish information scientist, and Steve Probert, deputy director of the British Patent Office, conducted 2000-11-20 on the Patents Mailing list at aful.org\footnote{http://www.aful.org/mailman/listinfo/patents}, goes right to the heart of the problem:

\begin{description}
\item[Ferndandez:]\ My disagreement with software patents is the philosophy of most patent offices. For me, a patent should be granted only where the research needed for the invention is expensive, so that said research would not be posible without the incentive of the monopoly. By contrast, the opinion of patent offices seems to be that the inventor should somehow have natural right to monopoly. They seem to think that the invention is intrinsically a very difficult process and that all incentive to it is small (I would like to hear Steve's opinion).  By contrast, the invention of a method to solve a problem is part of the daily work of a programmer.
\item[Probert:]\ I don't really want to express an opinion because I think that would be wrong for me as a Civil Servant and an official of the Patent Office.  What I would say is that the patent system has never differentiated between inventions on the basis of how much the underlying research cost.  It doesn't even distinguish between those inventions that are truly valuable and ground-breaking, and those that are [comparatively] trivial and insignificant. As the law stands, we would not be able to do so even if we wanted to. Only if an invention is known or obvious can we raise a legal objection.
To some extent the system should be proportionate and self-regulating - in industries where the research costs are very high (eg pharmaceuticals), the patents will be more valuable.  (So licencing costs will be higher etc.)  OTOH, where the development costs are lower it will often (but not always, I accept) be cheaper to work around a patent.  In such cases the patent will be worth less.  In other words what I am saying is that the value of a twenty year monopoly varies depending on a number of factors, one of which is the typical cost of research in the field.  Setting arbitrary thresholds based on research costs would effectively discriminate against individuals and smaller companies in favour of the bigger companies who will always spend more on research.  And who would decide how much money needed to be spent to make a particular invention?  (I presume you would not be content to determine the fate of a patent application on the basis of how much was actually spent.)
\item[Fernandez:]\ Probably the most important problem here is communication between non-programmers (lawyers, patent officials and so on) and programmers.  It is probably very difficult to convince the former about what kind of things are easy for programmers, such as having ideas, and what kind of things are difficult and time consuming.
\item[Probert:]\ I cannot speak for lawyers, but I can assure you that many Patent Examiners are programmers themselves.  In my group, all the Patent Examiners who deal with software applications either write computer programs in their spare time or have been employed as programmers before they became patent examiners.  They usually have a pretty good idea whether something would have been easy or time consuming for a programmer.  However, they might express the communication problem the other way around - it's very difficult to persuade programmers that just because an invention is ``easy'', does not make it any less patentable.
\end{description}
\end{sect}

\begin{sect}{inv}{Why not raise the ``inventive step'' standard?}
This dialogue confirms what everybody in the patent trade knows:  the criterion of ``inventive step'' as it stands is not designed to sort out trivial patents.

It is very hard to prove that even the most trivial new idea does not contain an inventive step.  The only way to show this currently at the European Patent Office is to prove that the idea can be obtained by simply combining the teaching of two known documents that the ``person skilled in the art'' would normally consult.

In the case of software patents, the person skilled in the arts rarely even consults documents.  New programming problems occur all the time, and ``inventing'' a solution to them is the normal way to go.  Most such solutions are not even worth being published in any inforomation science journal that an examiner might consult for testing their novelty.

Certainly there are here and there some really difficult mathematical or informatical tasks.  Certain mathematical proofs took hundreds of years to find.  So, shouldn't we try to raise the inventivity standard, so that those really great achievements can be singled out for rewarding by some kind of monopoly right?

Even if lawmakers wanted to radically reform the existing patent system and raise the ``inventivity'' standard, they would encounter great difficulties:

\begin{description}
\item[fuzziness of ``invention height'':]\ How would they define ``height of invention'' (Erfindundh\"{o}he), as it is still sometimes called in old-fashioned German patent jargon?  ``At least 20 inches above the state of the art''? ``At least 20 twists of the brain (= 20 Lemelson ?'' beyond the state of the art)?\footnote{It has been suggested that a social game could help here:  let the patent applicant first publish only his problem and provide incentives for the public to submit solutions until a certain deadline.  All these solutions are then considered to be prior art.  This could really work, but it is a very radical reform proposal for a patent system which is governed by strong forces of inertia, who will always find forceful legal arguments against any even very moderate reform proposal that has a true potential of diminishing the number of patents granted.}
\item[triviality by sequentiality:]\ Abstract-logical constructions are usually composed of many tiny inventive steps in sequence.  Each patent will usually focus on one of these steps, thus making this patent trivial and broad, even if the innovation itself was truly ingenuous.  This has happened e.g. in the case of the MP3 patents.
\end{description}
\end{sect}

\begin{sect}{tech}{The broken regulative: technical character}
Traditionally there has been one other criterion that has helped to sort out trivial ideas:  the criterion of technical (aka physical, material, empirical) character.  At least in Europe and Japan, patent offices and patent courts still pay lipservice to this criterion.  We have concisely formulated this criterion in our proposed ``Gesetzesregel \"{u}ber den Erfindungsbegriff im Europ\"{a}ischen Patentwesen und seine Auslegung unter besonderer Ber\"{u}cksichtigung der Programme f\"{u}r Datenverarbeitungsanlagen\footnote{http://swpat.ffii.org/analyse/eurili/index.de.html}''.  This shows that the mind-matter borderline can be resharpened to provide a clear criterion, which excludes those ``post-industrial'' innovations that are based only on abstract calculus and do not require experimentation.  Finding a new causal relation between natural forces and a physical effect is usually much more difficult than finding a new mathematical relation.

While mathematical relations are composed of tiny inventive steps that build on each other, physical causality is not.  In nature, the total is not equal to the sum of the parts.  While natural phenomena may be described by mathematics, they are not themselves mathematical and the description is at best an approximation.  Even a system of lego bricks will usually not work out the way you you may have built it in your mind.  The more the system becomes complex, the more deviations can accumulate into unforeseeable effects.  Undisturbed cleanrooms exist only in the world of ideas.

Patents on untechnical innovations pave the way to an abyss of total triviality.  They create a whole range of detrimental effects on the patent system.  They do not only break the requirement of inventivity.  They also lead to the proliferation of ``function claims'', i.e. the patenting of ``technical effects''.  This means that problems and not solutions are claimed.  Since no new causal chain between natural forces and a transformation of matter is involved, it becomes difficult for the patent applicant to define his ``invention''.  If it lies in purely abstract computation, it cannot be claimed because that would mean laying claims to pure thinking.  If however a ``technical effect'' of computation is claimed, there is no inventivity in the claim, since everybody knows that a computer can do it.  The only difficulty lies in knowing \emph{how to tell the computer to do it}, and that is routinely left to the thousands of creative programmers, who, if allowed to do so, could independently devise hundreds of different creative solutions, all of which produce the claimed ``technical effect'', but none or few of which are disclosed in the patent description.

Thus the introduction of software patents has not only killed the requirement of technicity but also that of enabling disclosure.  It has removed traditional restrictions on the permissibility of ``function claims'' and thus overturned the balance of the patent system, leaving it behind in a state of disorder, inconsistency and moral bankruptcy.
\end{sect}

\begin{sect}{ref}{Weitere Lekt\"{u}re}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Report on Software Patents by Joaquin Seoane (professor of the Universidad Polit\'{e}cnica de Madrid) and Ramon Garcia Fernandez\footnote{http://www.dit.upm.es/~joaquin/report\_en.pdf}}}

\begin{quote}
Contains a long chapter on the triviality problem.  Quotes patent lawyers who are encouraging their customers to patent anything that seems useful and forget about the idea that patents are about inventions.  They are not.  Especially in software, you have to forget that old prejudice quickly, if you want to play the patent game.
\end{quote}
\filbreak

\item
{\bf {\bf Erhellende Ratschl\"{a}ge einer Patentanwaltskanzlei an Softwareunternehmen\footnote{http://lists.ffii.org/archive/mails/swpat/2001/Jun/0224.html} (vgl. Claus Dendorfer 1998: Patente und der Schutz softwarebezogener Erfindungen\footnote{http://www.patent.de/swp\_d.htm})}}

\begin{quote}
Ein Patentanwalt erkl\"{a}rt seinen Mandanten, dass es in Deutschland inzwischen sehr einfach und lohnend geworden ist, Patente mit breiten Anspr\"{u}chen f\"{u}r triviale Softwareideen zu erlangen:  Das Erfordernis nach einer nicht naheliegenden (erfinderischen) Leistung steht der Trivialit\"{a}t nicht entgegen.  Es gen\"{u}gt, wenn der erfinderische Beitrag \"{u}ber die kogniviten F\"{a}higkeiten eines standardisierten unkreativen Suchroboters hinausgeht.  Daher stehen insbesondere in solchen Gebieten, wo noch nicht viele Vorarbeiten geleistet wurden, sehr gute M\"{o}glichkeiten, triviale und breite Patente zu erwerben.  Dies ist insbesondere auf vielen Gebieten der Softwareentwicklung heute der Fall.  Bis vor kurzem stand der Softwarepatentierung noch die Anforderung der Technizit\"{a}t entgegen.  Doch dieses Kriterium wurde durch neuere Gerichtsentscheidungen praktisch aufgegeben.  Da Patente entscheidend zum Investitionsschutz, zur Werbung und zum Kaufwert eines Unternehmens beitragen k\"{o}nnen, kann sich die Investition in eine umfangreiche Patentierungsaktivit\"{a}t f\"{u}r Softwareunternehmen schnell lohnen.
\end{quote}
\filbreak

\item
{\bf {\bf Bronwyn H. Hall \& Rose Marie Ham: The Patent Paradox Revisited\footnote{http://swpat.ffii.org/papiere/nber-hallham99/index.en.html}}}

\begin{quote}
Research work done at Univ. of California, Berkely, published 1999 by National Bureau of Economic Research Inc. Finds that the surge in patents in the semiconductor industry in the 1980-90s does not reflect a surge in R\&D activity.
\end{quote}
\filbreak

\item
{\bf {\bf Bronwyn H. Hall \& Rose Marie Ham: The Patent Paradox Revisited\footnote{http://swpat.ffii.org/papiere/hbr-thurow97/index.en.html}}}
\filbreak

\item
{\bf {\bf Newell 1986: The models are broken, the models are broken!\footnote{http://swpat.ffii.org/papiere/uplr-newell86/index.en.html}}}

\begin{quote}
Paul Newell, professor of computer science, in response to law professor Donald Chisum's proposal to affirm the patentability of algorithms, does not directly say whether algorithms should be patentable, but rather describes how both affirmation and negation of this proposal would clash with the underlying assumptions of the patent system and how this will inevitably challenge the foundations of the patent system.  As more and more problems are solved by purely mathematical means, the patent system will either have to become less relevant for innovation, or it will have to completely review its model of what an invention is and how it should be appropriated.  In particular, Newell explains some basic concepts of informatics and points out that, with algorithms, there can be no meaningful differentiation between discovery and invention, between application and theory, between abstract and concrete, between numeric and symbolic etc.  Moreover he explains by a model of game theory that sharing algorithms, as currently practised by programmers, may lead to more innovation than making them appropriatable, so that a crude application of the patent system to algorithms could very well stifle rather than stimulate innovation.
\end{quote}
\filbreak

\item
{\bf {\bf }}
\filbreak

\item
{\bf {\bf Gregory Aharonian: The Patent Examination System is Intellectually Corrupt\footnote{http://www.bustpatents.com/corrupt.htm}}}

\begin{quote}
The author's indignation with the patent examination process leads him to sharply analyse many present-day problems.  Whether these problems can, as Aharonian suggests, be solved by a mere improvement of the examination process, seems questionable.  The indignation may be a result of wrong expectations due to the author's firm belief in the universal applicability of the patent system.
\end{quote}
\filbreak

\item
{\bf {\bf Drittes Paradigma: Ma{\ss}geschneidertes Software-Schutzrecht\footnote{http://swpat.ffii.org/analyse/suigen/index.de.html}}}

\begin{quote}
Ein Datenverarbeitungsprogramm ist Sprachwerk und virtuelle Maschine zugleich.  Weder das Urheberrecht- noch das Patentrecht wurden f\"{u}r Computerprogramme geschaffen.  Einige Wissenschaftler und Politiker haben daher f\"{u}r ein ``drittes Paradigma'' zwischen Patent- und Urheberrecht argumentiert.  Andere haben die abstrakt-logischen Ideen als ein ``Niemandsland des geistigen Eigentums'' bezeichnet und dessen Freihaltung von Eigentumsanspr\"{u}chen gefordert.  Neben Ausschlussrechten kommen auch Verg\"{u}tungsrechte und sonstige F\"{o}rderungssysteme in Betracht.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

