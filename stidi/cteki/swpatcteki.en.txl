<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patents and Finances

#descr: As patents are increasingly being granted for methods of organisation and calculation (which may be described in terms of computer machinery but are nonetheless nothing but abstract principles), it becomes very difficult for tax inspectors to assess whether a patent is actually being used or not.  Thus patents can be used to almost arbitrarily increase or decrease company assets and to create revenue streams from one corporate entity to another, preferably between affiliated companies or (overt or hidden) partner companies.  In some countries, such as Ireland, patent license revenues are even exempt from taxation.   In others, such as France, reduced tax rates apply.  These possibilities are increasingly being used.  They may also be coming into the reach of ordinary people who, by networking in a smart way, can shelter their money in safe distance from tax inspectors.  Maybe we can put together a consulting work group that will help people realise this potential.

#Fea: Software entrepreneur's view

#Mrt: Accounting games with immaterial goods

#Pne: Patents and Venture Capital

#Hmn: Here's an excerpt from a software entrepreneur's private letter:

#Ef3: Thereare many many companies whose balance sheet value consists to 75% or more of immaterial goods.  These can comprise license rights for films, self-developped software or of course patents. Even in the case of non-patented developments you can write any amount of crap you like into the books:  what after all is the value of a software package?  The creation value (manyears x salary) or what can be earned in the market place?  How does the deprecation curve procede?  When are value diminutions necessary?

#Dnr: The nice thing about patents is that final decisions can be postponed into the future for a long time, at least if you engage in a sufficient amount of litigation.   Even if this generates a lot of lawyer and court fees, it still can be quite profitable for companies, because the bubble in which you live continues to exist.

#Ehc: There are apparently not many companies who really conduct business on a %(q:serious) basis nowadays.

#Osr: Outsourcing

#Emm: Entwicklung von Software in Tochterunternehmung, Verkauf mit Gewinn an Mutterkonzern, der die Entwicklung damit zum Kauf- und nicht zum Herstellungspreis in den Büchern hat

#Hgn: Hohe Kostenangabe bei Eigenentwicklungen

#Web: Wer will beweisen, daß vor 5 Jahren 5 Entwickler an Kundenprojekten und 3 in der FuE gesessen haben, oder ob es doch 2 Entwickler beim Kunden und 6 in der FuE waren, wie in den Büchern ausgewiesen?

#Wrr: Wertminderungen verschleppen

#Inb: Immaterielle Güter länger mit großen Beträgen in den Büchern stehen haben, als eigentlich angemessen wäre

#Gtn: Gruppeninterne Transaktionen

#Mrr: Mehrfachverkauf von Lizenzrechten innerhalb der eigenen Unternehmensgruppe ergibt eine wundersame Buchgeldvermehrung

#Wlb: Weitere Möglichkeiten bei Patenten

#Mon: Man stelle sich vor, die börsennotierte Zock und Nepp AG sammelt von den Anlegern 20 Mio Euro ein. Die Zulieferfirma Z entwickelt eine Software und meldet ein themenverwandtes Patent an. Das läßt sich im Bundle wunderbar marktwirksam verkaufen. Lass die Software 50K Euro kosten ('n Raum voll mit Informatikstudenten, von aussen zusperren und 3*täglich Pizza anliefern), das Patent nochmal 10K Euro.  Das Paket geht jetzt für 1M Euro über den Tisch und in ein paar Jahren hat niemand Zweifel daran, daß der Preis wohl angemessen war. Denn immerhin hing ein Patent dran, das ganze war eine fertige Lösung. Das fragt man halt nach ein paar Jahren, weil die AG dann platt ist. Die Zulieferfirma Z, an der idealerweise die AG'ler auch noch privat mitbeteiligt sind, hat ihren Reibach des Lebens schon lange gemacht.

#FhW: Folgendes Gespräch führten wir mit einem Software-Unternehmer, der in der in der Wagniskapital-Szene (auch Venture Capital oder VC-Szene genannt) Erfahrungen gesammelt hat.

#Wnp: Was für Erfahrungen haben Sie mit Kapitalgebern gemacht?

#SeP: Sind die VC-Leute besonders auf Patente versessen?

#IPh: In wieweit taugen Patente tatsächlich zur Absicherung?

#Gyw: Gäbe es ohne Patentsystem im Sw-Bereich weniger Risikokapital?

#Inl: Ich bin das ein oder andere Mal schon mal mit in Entscheidungsprozesse involviert gewesen, wo es um die Frage der Finanzierung von Unternehmen durch VC-Gesellschaften ging. Meine Rolle war dabei, aus DV-sachlicher Sicht Geschäftsmodelle zu prüfen und den VC-lern das ganze mit seinen positiven und negativen Seiten begreiflich zu machen, also letztendlich eine Entscheidung vorzubereiten.

#cfK: %(q:Versessen) wäre wohl das falsche Wort. Aber die Fragestellung, ob eine zu finanzierende Firma Patente besitzt, ist sehr häufig Bestandteil der internen Kriterienkataloge (und teilweise auch unter den Top-5-Entscheidungskriterien).

#DiK: Das hat zwei ganz banale und im heutigen Gefüge auch nachvollziehbare Gründe, ob man sie nun mag oder nicht:

#EWt: Ein Patent steckt Terrain am Markt ab und erleichtert es damit, Marktanteile zu erlangen und zu sichern.

#Eai: Ein erteiltes Patent ist ein Ergebnis eines externen Prüfungsprozesses.    Es hat sich halt schon mal jemand anderes hingesetzt, die Patentanmeldung kritisch begutachtet und für neu und patentwürdig erachtet. Das ist eine positive Entscheidungshilfe - das, worum es geht, kann zumindest nicht völlig schlecht sein. Glaubt der VC.

#VhP: VCs haben keine wirkliche Vorstellung davon, was Patente im IT-Bereich sind und was für Auswirkungen sie haben. Sie wissen nur, was für Wirkungen - ganz grob betrachtet - ein Patent allgemein hat, und daß diese Wirkungen schädlich für die Konkurrenz und gut für das begutachtete Unternehmen sind. Wenn VC-ler die Claims verstehen und ansonsten eine Menge technisches Gebrabbel in der Patentschrift enthalten ist, dann reicht das völlig aus. Ich vermute, daß nur ein sehr geringer Prozentsatz von den VCs gegenüber angeführten Patenten tatsächlich tiefergehend überprüft werden.

#AiW: Auch was den zweiten Punkt angeht darf man nicht vergessen, daß VC-ler aus der Finanzwirtschaft kommen und in der Regel %(s:null) Ahnung von dem haben, womit die von ihnen finanzierten Firmen später ihre Gelder verdienen wollen. Sie sind also dringend auf externe Entscheidungshilfen angewiesen, andererseits teilweise aber hinreichend arrogant, sich nicht offen am Markt beraten zu lassen. Man will sich schließlich nicht in die Karten schauen lassen. Die eigenen Bewertungskriterien werden teilweise als halbes Staatsgeheimnis gehütet, was zwei Gründe haben kann: %(ol|es gibt keine und es wird %(q:frei nach Schnauze entschieden), oder|man glaubt, daß die Entscheidungsstrategie ein Firmengeheimnis im Wettbewerb mit anderen VC's ist.)

#UeW: Und dann noch etwas ganz anders: Die meisten VC-Gesellschaften, die in den gloriösen Jahren des ausgehenden letzten Jahrtausends entstanden sind, wurden ja quasi auf die grüne Wiese gestellt. Die Leute, die dort Entscheidungen zu fällen hatten, waren oft noch unerfahren und mußten sich sehr schnell irgendwelche Kriterien erarbeiten, die für sie auch mit geringer Praxis- und Branchenerfahrungen greifbar sind.

#Msw: Mir ist kein Fall bekannt, wo diese Frage genau beleuchtet worden wäre.  Das soll aber nichts heißen.

#Iwo: Interessanter ist meines Erachtens aber, daß Patente für sich eine Art künstliche Währung darstellen. Man kann damit handeln und hat wieder ein immaterielles Etwas, womit Umsatz und große Bilanzsummen generiert werden können. Beides zusammen sorgt wiederum für Kreditwürdigkeit. Gleichzeitig sind immaterielle Posten die Stelle, wo in der Buchführung eines Unternehmens am leichtesten manipuliert werden kann. Das gilt aber nicht nur für Patent-Deals, sondern auch z. B. für Film-Lizenzgeschäfte.

#NWa: Nein, die VC-Gesellschaften müßten lediglich ihr Weltbild ein wenig nachjustieren. Dafür hätten sie aber genug Zeit - im Moment wird eh faktisch nichts neu finanziert, weil die VCs ihr Geld soweit noch vorhanden lieber in Rettungsaktionen vorangegangener Experimente stecken oder gleich auf der Cashseite bleiben, bis die Rezession vorüber ist...

#dxi: discussion of an article from the Wall Street Journal, which reported about the rise of patent-based tax sheltering practises among US corporations.  Often the patent rights are assigned to holdings or subsidiaries on the Cayman Islands or in Ireland, where patent royalty revenues enjoy preferential treatment.

#nne: Why are companies keen on collecting so many questionable patents?  Could it be that additional accounting leverage, which can be turned into shareholder value or tax savings, is an important factor?  Could it be that the importance of this factor is growing in Germany due to the introduction of US style accounting practises?

#IWe: Expected to become the new european accounting standard

#Bru: Berichtet sehr reißerisch aber gut informiert über die Pleitewelle der Internetfirmen und verweist auf allerlei Originalmeldungen.

#WIe: Where, other than in Ireland, are patent revenues tax-exempted?

#Wci: Where, other than France, do they enjoy particularly low taxation?

#Wce: How do patents show up in balance sheets according to different accounting  systems (HGB, US GAAP, IAS etc)

#RtW: Report to us about the tax policies in different countries!

#Wox: Wanted: microeconomists and taxation experts!

#Wfa: Work on a %(q:Software Patents Tax Evasion Howto)

#Bse: Build a consulting business and informal networks on this basis

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatcteki ;
# txtlang: en ;
# multlin: t ;
# End: ;

