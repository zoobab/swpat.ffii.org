stidifr.lstex:
	lstex stidifr | sort -u > stidifr.lstex
stidifr.mk:	stidifr.lstex
	vcat /ul/prg/RC/texmake > stidifr.mk


stidifr.dvi:	stidifr.mk stidifr.tex
	latex stidifr && while tail -n 20 stidifr.log | grep references && latex stidifr;do eval;done
	if test -r stidifr.idx;then makeindex stidifr && latex stidifr;fi
stidifr.pdf:	stidifr.mk stidifr.tex
	pdflatex stidifr && while tail -n 20 stidifr.log | grep references && pdflatex stidifr;do eval;done
	if test -r stidifr.idx;then makeindex stidifr && pdflatex stidifr;fi
stidifr.tty:	stidifr.dvi
	dvi2tty -q stidifr > stidifr.tty
stidifr.ps:	stidifr.dvi	
	dvips  stidifr
stidifr.001:	stidifr.dvi
	rm -f stidifr.[0-9][0-9][0-9]
	dvips -i -S 1  stidifr
stidifr.pbm:	stidifr.ps
	pstopbm stidifr.ps
stidifr.gif:	stidifr.ps
	pstogif stidifr.ps
stidifr.eps:	stidifr.dvi
	dvips -E -f stidifr > stidifr.eps
stidifr-01.g3n:	stidifr.ps
	ps2fax n stidifr.ps
stidifr-01.g3f:	stidifr.ps
	ps2fax f stidifr.ps
stidifr.ps.gz:	stidifr.ps
	gzip < stidifr.ps > stidifr.ps.gz
stidifr.ps.gz.uue:	stidifr.ps.gz
	uuencode stidifr.ps.gz stidifr.ps.gz > stidifr.ps.gz.uue
stidifr.faxsnd:	stidifr-01.g3n
	set -a;FAXRES=n;FILELIST=`echo stidifr-??.g3n`;source faxsnd main
stidifr_tex.ps:	
	cat stidifr.tex | splitlong | coco | m2ps > stidifr_tex.ps
stidifr_tex.ps.gz:	stidifr_tex.ps
	gzip < stidifr_tex.ps > stidifr_tex.ps.gz
stidifr-01.pgm:
	cat stidifr.ps | gs -q -sDEVICE=pgm -sOutputFile=stidifr-%02d.pgm -
stidifr/stidifr.html:	stidifr.dvi
	rm -fR stidifr;latex2html stidifr.tex
xview:	stidifr.dvi
	xdvi -s 8 stidifr &
tview:	stidifr.tty
	browse stidifr.tty 
gview:	stidifr.ps
	ghostview  stidifr.ps &
print:	stidifr.ps
	lpr -s -h stidifr.ps 
sprint:	stidifr.001
	for F in stidifr.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	stidifr.faxsnd
	faxsndjob view stidifr &
fsend:	stidifr.faxsnd
	faxsndjob jobs stidifr
viewgif:	stidifr.gif
	xv stidifr.gif &
viewpbm:	stidifr.pbm
	xv stidifr-??.pbm &
vieweps:	stidifr.eps
	ghostview stidifr.eps &	
clean:	stidifr.ps
	rm -f  stidifr-*.tex stidifr.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} stidifr-??.* stidifr_tex.* stidifr*~
tgz:	clean
	set +f;LSFILES=`cat stidifr.ls???`;FILES=`ls stidifr.* $$LSFILES | sort -u`;tar czvf stidifr.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
