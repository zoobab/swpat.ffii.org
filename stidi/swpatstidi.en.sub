\begin{subdocument}{swpatstidi}{Software Patents: Questions, Analyses, Proposals}{http://swpat.ffii.org/stidi/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{Wie wirkt das Patentsystem auf die Wirtschaft im allgemeinen und wie auf die Informations\"{o}konomie im besonderen? Warum sind Softwarepatente so trivial? Wo liegen die Grenzen des Patentsystems, wie wurden sie fr\"{u}her formuliert und wie haben sie sich verschoben?  Unter welchen Einschr\"{a}nkungen bewegt sich das Patentsystem?  Welche Handlungsoptionen haben wir? Mit den hier gesammelten Artikeln wollen Mitglieder und Freunde des FFII Antworten geben.}
\begin{sect}{kver}{The Basic Questions}
Various governmental agencies have conducted consultations about the patentability of computer programs, but so far in most cases wrong questions were asked, leading to virtual debates and largely useless studies.  We think that a serious debate should answer the following questions.

\begin{enumerate}
\item
In how many \percent{} of the app. 30000 software patents granted so far by the EPO is the claimed achievement is so impressive, that it seems at least worth discussing whether we as legislators might want to grant a temporary monopoly for such an achievement? (Please cite the main claims of a few typical EPO software patents to illustrate your statement!)\\
What do the EPO software patents usually claim, what effort does the development of the claimed idea take and how much later (earlier?) would it spread and sprout if we didn't reward its publication with a patent?

\item
Is the software industry's innovativity suffering from too rapid imitation?\\
Does software copyright need to be improved or supplemented by patent law or similar?\\
What would an optimal ``specially-tailored software rewarding law'' (lex sui generis) look like?

\item
Why is there a phenomenon of Free Software but not free hardware?\\
How does the economics of immaterial goods differ from that of material goods?\\
Which role could/should Free Software play in the information society?\\
Under which rules can proprietary and free software prosper together productively?

\item
According to which rules do European courts (EPO Technical Board of Appeal, German Federal Court and Federal Patent Court) judge the patentability of software?\\
Art these rules clear?\\
Where have they been formulated in the clearest terms?

\item
According to which rules did European courts judge the patentability of software in 1985?\\
Were these rules clear?\\
Where have they been formulated in the clearest terms?

\item
Are there any international treaties or other legal constraints that limit our legislative options concerning software patents, either by requiring software patentability or by limiting it or linking it to unalterable conditions?

\item
Which clear delimiting rules of patentability or patent enforcability are there for us to choose from today?\\
How many \percent{} of the total number of EPO patents (hard and software) would be confirmed as valid and enforcable, if each of these rules was applied?\\
What do you think of the following possible options?
\begin{itemize}
\item
{\bf Scope of patentable ideas}

\begin{quote}
\begin{itemize}
\item
{\bf ``practical and repeatable problem solutions''\footnote{\url{http://swpat.ffii.org/vreji/papri/jwip-schar98/}}}

\begin{quote}
All kinds of ideas can be patented, provided that they are repeatable independently of participating persons and achieve a determined effect in the material world.  Purely mathematical solutions are acceptable, but the claims must be tied to practical applications, potentially leading to very long lists of claims.  This doctrine is favored by leading theoreticians of both the USPTO and the EPO.
\end{quote}
\filbreak

\item
{\bf The Technical Field = Applied Natural Science\footnote{\url{http://localhost/swpat/stidi/korcu/index.en.html}}}

\begin{quote}
The ``technical field'' is the field of engineering aka applied natural science.  An invention must teach a new way of using natural forces to directly achieve a tangible result.  ``Computational or organisational rules'' designed to solve problems posed by mathematical models (such as the Turing machine) are not technical in this sense, no matter whether the model in question can be mapped onto a physical structure or not.  This doctrine was developped mainly by German lawcourts up to the 80s, and it can be found in law commentaries, examination guidelines and court decisions up to the year 2000.  The Eurolinux Alliance demands\footnote{\url{http://petition.eurolinux.org/consultation/ec-consult.pdf}} that this doctrine be systematically applied in the future.
\end{quote}
\filbreak

\item
{\bf Abstractions in their Original Form\footnote{\url{http://localhost/swpat/papri/irle-laat00/index.en.html}}}

\begin{quote}
This doctrine allows even the patenting of purely abstract mathematical methods without a practical application.  The question of patentability is no longer asked.  The discussion is shifted to enforcability:  what kind of objects can constitute a patent infringement?
\end{quote}
\filbreak

\item
{\bf ``Dynamic Concept of Technical Character''\footnote{\url{http://swpat.ffii.org/stidi/korcu}}}

\begin{quote}
``Apply all three doctrines in an case-by-case approach, drift toward unlimited patentability in helical movements.
For the sake of ``legal security'' (= prevention of resistance from ``conservative'' lawcourts), confirm this course from time to time by fuzzy laws and directives.''
Some unfriendly commentators claim that this is what interested patent lawyers mean when they praise the advantages of a ``dynamic concept of technical character''.
\end{quote}
\filbreak
\end{itemize}
\end{quote}
\filbreak

\item
{\bf Scope of Infringement Activities}

\begin{quote}
Information goods are recognised as part of public communication process.  They may not be touched by patents, no matter what is written in the patent claim and no matter whether they are technical or not.  Only industrially produced material goods (marginal production cost \texmath{>} 0) can infringe on patents.  Not only the private sphere but also the sphere of public communication, including direct application of information goods on an ordinary computer, is off-limits to patent litigation.  The question on whether claims to abstract-logical constructions should be permissible or not is kept separate from this issue.  Any combination with one of the above doctrines is possible.
\end{quote}
\filbreak

\item
{\bf Difficulty of Invention}

\begin{quote}
Some peole are looking for criteria and game rules that would make it possible to eliminate trivial patents.  E.g. the Dutch parliament has requested that this problem must be solved, before the patentability of software can be discussed.
\end{quote}
\filbreak

\item
{\bf Organisation of the Patent System}

\begin{quote}
For decades it has often been criticised that the patent examination system is not up to its task of quickly and efficiently sorting out unjustified patent claims, and that patent institutions have developped an expansive tendency that leads to more and more questionable patents.  The French deputy Jean Le D\'{e}aux has demanded that a commission be appointed with investigating the abuses in the European patent institutions and working out recommendations for an institutional reform.  According to this view, the key to improvement is not so much in the legal rules or patenting as in the institutional framework in which thes rules are applied.
\end{quote}
\filbreak
\end{itemize}

\item
Assuming these rules were codified as European Directives, what would happen to those patents that could no longer be confirmed as valid?
\end{enumerate}
\end{sect}

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/korcu/swpatkorcu.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/javni/swpatjavni.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/trips/swpattrips.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/frili/swpatfrili.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/fukpi/swpatfukpi.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/nitcu/swpatnitcu.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/manri/swpatmanri.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/epc52/epue52.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/gacri/swpatgacri.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/preti/swpatpreti.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/pleji/swpatpleji.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/basti/swpatbasti.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/tisna/swpattisna.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/cteki/swpatcteki.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/sektor/sektor.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/pante/swpatpante.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/danfu/swpatdanfu.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/unwort/swpatunwort.de.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/lijda/swpatlijda.en.sub}
\else
\dots
\fi

\ifmoreinputs
\ifsubinputs
\else
\moreinputsfalse
\fi
\input{/ul/sig/srv/res/www/ffii/swpat/stidi/catlu/swpatcatlu.fr.sub}
\else
\dots
\fi

\begin{sect}{altr}{Other serious attempts at formulating questions}
\begin{itemize}
\item
{\bf {\bf (Aigrain 2002: 11 questions on software patentability issues in the US and Europe)\footnote{\url{http://cip.umd.edu/Aigrain.htm}}}}

\begin{quote}
a paper representing the private views of Philippe Aigrain, head of a department in the Infosoc Directorate of the European Commission.  Asks some questions which the patent movement probably doesn't like.  In some cases the the answer is almost evident once the taboo-breaking question is asked, in other cases the answer requires some research that is yet waiting to be done.  The paper was prepared for a conference on open software and information policy research hosted by the US National Science Foundation in Arlington near Washington.  H. Pilch from FFII is also attending.
\end{quote}
\filbreak

\item
{\bf {\bf Konferenz über Informationelle Infrastrukturpolitik 2002-01-28 Arlington USA\footnote{\url{http://localhost/swpat/penmi/2002/arli01/index.en.html}}}}

\begin{quote}
Hartmut Pilch from FFII will speak about software patentability issues at this conference.  Other contributions on this subject are expected from Philippe Aigrain (European Commission), Brian Kahin (Maryland University, NSF) and others.  Hopefully the conference will spark off some long-needed research.
\end{quote}
\filbreak

\item
{\bf {\bf \url{}}}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

