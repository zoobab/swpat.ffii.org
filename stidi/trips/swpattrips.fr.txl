<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: L’Accord sur les ADPIC et les brevets logiciels

#descr: Les autorités européennes en matière de brevet n'ont de cesse de citer l'Accord sur les ADPIC comme argument impliquant la brevetabilité des programmes d'ordinateur et des méthodes pour l'exercice d'activités économiques et rendant de tel brevets applicables dans des conditions des plus indécentes. Ce raisonnement est fallacieux et peut être aisément démonté.  De plus, il apparaît que l'establishment européen des brevets viole lui-même systématiquement l'Accord sur les ADPIC.

#Bnt: Christian Beauprez: article 10 ADPIC et %(q:Inventions mises en oeuvre par ordinateur)

#eos: Les experts en droit informatique britannique argumentent que selon l'article 10 des ADPIC, les programmes d'ordinateur doivent être %(q:protégé en tant qu'oeuvres littéraires) et cela signifie que les idées qui y sont incorporées sont libre, comme dans les oeuvres littéraire.

#TWI: OMC: ADPIC

#feO: Texte de l'Accord sur les ADPIC sur le site de l'OMC

#rql: Dan L. Burk et Mark Lemley, %(q:Le droit des brevets est-il spécifiquement technique ?)

#yWo: Journal du droit des techniques de Berkeley (2002) et %(q:Leviers politiques dans le droit des brevets), 89 Virginia Law Review (Décembre 2003). L'expert étasunien en droit, Mark Lemley, explique le contexte historique des ADPIC et affirme que l'article 27 ne peut être interprété de manière aussi rigide que le prétendent tous ces avocats interchangeables.

#Dea: Les ADPIC impliquent-ils des brevets logiciels ?

#TrAX: ADPIC Article %s

#Caa: Programmes d'ordinateur et les compilations de données

#Plc: Objet brevetable

#pRf: Exceptions aux droits conférés

#Tai: L'article 33 stipule que tous les brevets doivent avoir une durée minimale de 20 ans.

#Ske: Art 7-8: Un accord pour le libre échange et son interprétation

#iWe: Infractions aux ADPIC par l'OEB et les propositions de directive de l'UE

#TiW: L'Accord sur les aspects des droits de propriété intellectuelle qui touchent au commerce (ADPIC), signé le 15/12/1993 comme document constitutif de l'Organisation mondiale du commerce (OMC), définit les règles minimales pour le droit national de la propriété intellectuelle dans le but d'empêcher les nations membres d'utiliser la propriété intellectuelle comme une barrière commerciale cachée contre les autres nations.

#AnW: L'article 27 a souvent été analysé par les juristes des brevets comme impliquant que les revendications de programmes devaient être autorisées pour s'étendre aux programmes d'ordinateur.

#PWh: Paul Hartnack, Inspecteur général a l'Office des Brevets Britannique, %(ph:a commenté) cette question dans une audition à Londre en 1997 :

#Sfl: Certaines personnes ont affirmé que les accords ADPIC nous obligent à accorder des brevets logiciels parce qu'ils disent %(q:les brevets devront être accordé sur toutes les inventions dans tous les domaines techniques, pour peu qu'elles soient susceptibles ... d'application industrielle).

#Hde: Cependant, cela dépend de la façon dont on interprète ces mots.

#IWe: Un fragment de pur logiciel constitue-t-il une invention ?

#Eli: La loi européenne dit que non.

#Ioe: Le logiciel pur constitue-t-il une technique ?

#Moa: Nombreux sont ceux qui pensent que non.

#Ifl: Est-il susceptible d'application %(q:industrielle) ?

#Ahy: Ici encore, pour beaucoup de logiciels nombreux sont ceux qui diraient que non.

#Tnc: Les ADPIC sont une occasion de renforcer la protection du logiciel.

#BWW: Mais la décision d'un tel renforcement doit être prise en se fondant sur des arguments économiques réfléchis.

#WWn: Est-ce l'intérêt de l'industrie européenne et des consommateurs européens de faire un tel choix ?

#IiW2: Dans sa ratifications des accords du GATT et ADPIC, les instances législatives allemandes n'ont pas vu de contradictions entre les sections 1(2)(3) and 1(3) de la Loi sur les brevets et l'article 27(1) des ADPIC.

#Wpl: Dans une %(bs:décision en 2000), dans laquelle elle rejetait une revendication sur un programme d'ordinateur, la Cour fédérale allemande sur les brevets a réfuté explicitement la supercherie des ADPIC :

#Ami: L'Accord sur les aspects des droits de propriété intellectuelle qui touchent au commerce (ADPIC) n'entraîne pas de différence de jugement sur la brevetabilité. Indépendamment de la question selon la forme dans laquelle l'Accord sur les ADPIC est ici applicable - directement ou indirectement -, l'application de l'article 27 des ADPIC ne conduirait ici à aucune extension de la brevetabilité. La formulation, selon laquelle les brevets devraient être accordés pour les inventions dans tous les domaines techniques, confirme simplement la vision dominante dans la jurisprudence allemande sur les brevets, d'après laquelle le concept de technique (Technik) constitue le seul critère valable pour distinguer les inventions de toutes les autres sortes de réalisations intellectuelles, et par conséquent la technicité est une condition nécessaire à la brevetabilité (la décision de %(q:Logikverifikation) de la Court fédérale de justice (BGH) voit dans l'article 2  7 d  es ADPIC une %(q:confirmation a posteriori) de cette jurisprudence). La provision d'exclusion des articles 52 (2) et (3) de la CBE, ne peut pas non plus être analysée comme étant en contradiction avec l'article 27 des ADPIC, puisqu'elle se base sur la notion de carence de caractère technique des exceptions.

#Wti: La Court fédérale des brevets se réfère ici à la doctrine de %(dp:Dispositionsprogramm), selon laquelle la présence ou non de %(e:forces contrôlables de la nature) dans la solution du problème est le seul critère valable pour délimiter le domaine des inventions brevetables. Selon cette doctrine, le traitement de données n'est pas un domaine technique, comme l'explique Gert Kolle, le plus grand spécialiste contemporain de la question , %(gk:dans son analyse souvent citée de la décision Dispositionsprogramm en 1977):

#Wmr: Le Traitement automatique des données est devenu de nos jours un outil auxilliaire indispensable dans tous les domaines de la société et le restera à l'avenir.  Il est omniprésent.  ... De par son sens instrumental, sa fonction auxilliaire et subsidiaire, le traitement automatique des données se distingue des ... domaines particuliers de la technique et se rapproche davantage de domaines comme la gestion d'entreprise, dont les résultats et les méthodes de travail ... sont nécessaires à toutes les entreprises et pour laquelle, par conséquent, un besoin de libre disponibilité est au premier regard tout indiqué.

#lhi: C'est exactement ce qu'a fixé le Parlement européen dans sa %(ad:proposition de directive amendée):

#Am32: En vertu de la Convention sur la délivrance de brevets européens signée à Munich, le 5 octobre 1973, et du droit des brevets des États membres, les programmes d'ordinateurs ainsi que les découvertes, théories scientifiques, méthodes mathématiques, créations esthétiques, plans, principes et méthodes dans l'exercice d'activités intellectuelles, en matière de jeu ou dans le domaine des activités économiques et les présentations d'informations, ne sont pas considérés comme des inventions et sont donc exclus de la brevetabilité. Cette exception s'applique parce que lesdits objets et activités n'appartiennent à aucun domaine technique.

#Am107: %(q:contribution technique) , également appelée %(q:invention), désigne une contribution à l'état de la technique dans un domaine technique. Le caractère technique de la contribution est une des quatre conditions de la brevetabilité. En outre, pour mériter un brevet, la contribution technique doit être nouvelle, non évidente et susceptible d'application industrielle. L'utilisation des forces de la nature afin de contrôler des effets physiques au delà de la représentation numérique des informations appartient à un domaine technique. Le traitement, la manipulation et les présentations d'informations n'appartiennent pas à un domaine technique, même si des appareils techniques sont utilisés pour les effectuer.

#Am45: Les États membres veillent à ce que le traitement des données ne soit pas considéré comme un domaine technique au sens du droit des brevets et à ce que les innovations en matière de traitement des données ne constituent pas des inventions au sens du droit des brevets.

#Cby: Les programmes d'ordinateur, qu'ils soient exprimés en code source ou en code objet, seront protégés en tant qu'oeuvres littéraires en vertu de la %(bk:Convention de Berne).

#Cnx: Les compilations de données ou d'autres éléments, qu'elles soient reproduites sur support exploitable par machine ou sous toute autre forme, qui, par le choix ou la disposition des matières, constituent des créations intellectuelles seront protégées comme telles. Cette protection, qui ne s'étendra pas aux données ou éléments eux-mêmes, sera sans préjudice de tout droit d'auteur subsistant pour les données ou éléments eux-mêmes.

#Sit: Sous réserve des dispositions des paragraphes 2 et 3, un brevet pourra être obtenu pour toute invention, de produit ou de procédé, dans tous les domaines technologiques, à condition qu'elle soit nouvelle, qu'elle implique une activité inventive et qu'elle soit susceptible d'application industrielle.

#Ftt: Note

#Fqy: Aux fins de cet article, les expressions %(q:activité inventive) et %(q:susceptible d'application industrielle) pourront être considérées par un Membre comme synonymes, respectivement, des termes %(q:non évidente) et %(q:utile).

#Sec: Sous réserve des dispositions du paragraphe 4 de l'article 65, du paragraphe 8 de l'article 70 et du paragraphe 3 du présent article, des brevets pourront être obtenus et il sera possible de jouir de droits de brevet sans discrimination quant au lieu d'origine de l'invention, au domaine technologique et au fait que les produits sont importés ou sont d'origine nationale.

#eee: Il faut noter que le texte encourage explicitement à distinguer les interprétations de termes abstraits employés, tels que %(q:non évidence) and %(q:application industrielle).

#nec: Alors que le paragraphe interdit la %(q:discrimination) dans l'intérêt de conditions favorables à l'échange libre et égalitaire, il ne stipule aucun concept spécifique d'invention.  Il pourrait toutefois être interprété en faveur d'un concept d'invention qui serait favorable au libre échange et au développement économique dans lesquels les termes %(q:technologie), %(q:industrie), etc. sont lourds de sens.

#trV: Même à l'intérieur des limites du domaine des %(q:techniques) brevetables, l'article 27(1) peut difficilement être interprété comme un cadre rigide proscrivant tout règlage fin.  S'il devait être interprété aussi rigidement, comme certains avocats des brevets le propose, la loi des USA sur le copyright enfreindrait les ADPIC dans au moins trois domaines : les produits pharmaceutiques [35 USC 155,156, extension du terme; 35 USC 271(e), utilisation expérimentale]; les procédés biotechnologiques [35 USC 103(b), fournir un standard de non évidence particulier]; les procédures médicales et chirurgicales [35 USC 287(c), limiter les remèdes] et les méthodes d'affaire [35 USC 273(a)(3), fournir des droits antérieurs aux utilisateurs].

#ere: Les Membres pourront prévoir des exceptions limitées aux droits exclusifs conférés par un brevet, à condition que celles-ci ne portent pas atteinte de manière injustifiée à l'exploitation normale du brevet ni ne causent un préjudice injustifié aux intérêts légitimes du titulaire du brevet, compte tenu des intérêts légitimes des tiers.

#pen: Cette clause est citée par les lobbies des détenteurs de brevets dès que quelqu'un essaie de restreindre leurs droits, que ce soit de manière %(q:raisonnable) ou %(q:non).

#Ttr: C'est important à savoir car cela rend sans objet les propositions récurrentes, comme celle du PDG d'Amazon, Jeff Bezos, qui prône une réduction de la durée de vie des brevets logiciels entre 3 et 5 ans.

#nui: L'accord sur les ADPIC n'est pas limité dans le temps. Il reste valide tant que l'Organisation mondiale du commerce (OMC) dans son ensemble ne peut le changer. L'organisation de l'OMC est très éloignée d'une participation démocratique et nombre ses membres sont des dictatures. Si un pays veut choisir de sortir des ADPIC, il devra quitter l'OMC, risquant par conséquent un effondrement de ses industries d'exportation. L'accord a été négocié en coulisses entre fonctionnaires ministériels et sans qu'il existe de traduction pour la plupart des langues de la planète. Toutes ces considérations rendent impératif que l'accord sur les ADPIC soit interprété avec la plus grande précaution et qu'on exploite intensivement la flexibilité qu'il autorise, dans le but d'arriver à un juste équilibre entre les droits et les obligations, dans l'ojectif global de libre échange que sert l'accord.

#tsn: Les rédacteurs de l'accord étaient au courant de ces problèmes. Dans les Dispositions générales, ils ont inclus des articles tel que celui-ci :

#bcv: Objectifs

#pea: La protection et le respect des droits de propriété intellectuelle devraient contribuer à la promotion de l'innovation technologique et au transfert et à la diffusion de la technologie, à l'avantage mutuel de ceux qui génèrent et de ceux qui utilisent des connaissances techniques et d'une manière propice au bien-être social et économique, et à assurer un équilibre de droits et d'obligations.

#rcl: Principes

#aWu: Les Membres pourront, lorsqu'ils élaboreront ou modifieront leurs lois et réglementations, adopter les mesures nécessaires pour protéger la santé publique et la nutrition et pour promouvoir l'intérêt public dans des secteurs d'une importance vitale pour leur développement socio-économique et technologique, à condition que ces mesures soient compatibles avec les dispositions du présent accord.

#eaa: Des mesures appropriées, à condition qu'elles soient compatibles avec les dispositions du présent accord, pourront être nécessaires afin d'éviter l'usage abusif des droits de propriété intellectuelle par les détenteurs de droits ou le recours à des pratiques qui restreignent de manière déraisonnable le commerce ou sont préjudiciables au transfert international de technologie.

#Igl: Il est bien connu que les brevets logiciels sont un désastre en termes d'innovation, de concurrence et d'équilibre des droits. Plus encore, les brevets sur les méthodes pour l'exercice d'activités économiques servent systématiquement à restreindre les échanges et ces restrictions sont jugées inacceptables par la plupart des gens de ce secteur.

#ero: Les ADPIC fournissent des méta-règles pour le droit des brevets, conçues pour promouvoir le libre échange et réduire les prérogatives des gouvernements de favoriser leurs industries locales vis-à-vis de la concurrence étrangères. Ils se prononcent sur la manière dont les lois devraient être structurées, par ex. %(q:pas de discrimination en faveur d'industries locales particulières), %(q:pas de limitation arbitraire sur l'applicabilité). Ils encouragent ainsi deslimitation qui sont fondées sur des considérations %(e:systématiques), par ex. mettre dans la balance les droits des titulaires de brevets et d'autres droits de poids équivalent, comme la propriété du droit d'auteur (article 10 des ADPIC), la liberté de publication (article 10 de la Convention européenne des droits de l'homme CEDH) ou le droit d'accès aux standards de communication.

#WWt: Il est très important pour n'importe quel projet de loi sur les brevets de concrétiser les règles abstraites de l'accord sur les ADPIC. Tout projet de loi qui échouerait dans cette concrétisation ne peut prétendre servir un objectif de clarification.

#str: Alors que les politiques économiques devraient être justifiées selon les termes des concepts abstraits inscrits dans les ADPIC, ils ne peuvent pas dériver uniquement des ADPIC.

#Wre: C'est un bien pauvre travail de rédaction que de copier/coller les doctrines abstraites des ADPIC dans le lois européennes, qui sont supposées fournir des directions à un niveau plus concret.  %(ep:L'article 52%(pe:1) de la CBE a été revi et corrigé) dans ce sens malavisé par la Conférence diplomatique de 2000 et le rapporteur du Parlement européen sur le %(sd:projet de directive sur les brevets logiciels), %(am:la MPE Arlene McCarthy MEP), %(am:a proposé) d'écrire directement l'article 30 des ADPIC dans l'article 6 bis de la directive. De tels actes ne font pas qu'obscurcir les lois proposées. Ils privent également l'Europe de marge de manoeuvre dans les futures négociations des ADPIC, ce qui pourrait bien être nécessaire pour éviter que le cadre des ADPIC ne soit plus du tout façonnable.

#adi: Les ADPIC ont été négociés par des délégations qui représentaient les intérêtes dominant d'une autre époque. Le logiciel était considéré comme non brevetable dans une grande majorité et le développement et la distribution de logiciels libres étaient pratiquement inconnus. Les ADPIC devraient être interprétés d'une manière qui ne profite pas à certaines techniques de productions, à certains modèles économiques et à certaines industries au dépend des autres. L'important est de renforcer la productivité dans toutes les industries.

#awW: Nombre de limites dans le systèmes des ADPIC, tout au moins dans ses interprétations plus rigides, sont devenues si manifestes que même les États-Unis, qui étaient les organisateurs en chef de l'accord sur les ADPIC, l'ont façonné à leur avantage et ont ainsi, de manière soutenable, violé certaines de ses dispositions.

#teo: Il y a maintenant moins d'inquiétude à avoir quant à une plainte des USA à propos d'un quelconque aspect du système de brevets dans l'UE, car l'UE pourrait alors riposter en se pleignant de la préférence étasunienne dans le système de brevets des USA.

#oot: Tout jugement qui éclaterait sur le sujet risquerait fort d'ouvrir un nombre suffisant de questions politiquement épineuses des deux côtés de l'atlantique pour que personne ne veuille s'y lancer.

#aeW: L'Office européen des brevets a commencé à accorder %(pc:des revendication de programmes) en 1998.  Il est indiqué dans les justifications des décisionsIn the justifying decisions %(t1:T 1173/97) and %(t2:T 935/97) it is stated:

#otW: Un produit %(q:programme d'ordinateur) n'est pas exclu de la brevetabilité en application de l'article 52(2) et (3) CBE si sa mise en oeuvre sur un ordinateur produit un effet technique supplémentaire, allant au-delà des interactions physiques %(q:normales) entre programme (logiciel) et ordinateur (matériel).

#9ir: Les programmes d'ordinateur, tels que décrits dans des revendications de programmes accordées par l'OEB depuis 1988, sont des structures d'information, composées seulement d'entités symboliques. Le seul %(q:caractère technique) qu'on peut y trouver ne se trouve que du côté des entités symboliques. On pourrait de même parler du %(q:caractère technique) d'un ensemble de formules chimiques, d'un collection des dessins industriels ou même d'un roman de science-fiction et habiliter tout détenteur de brevet à monopoliser la distribution de toutes les informations qui décriraient son %(q:invention).

#iWv: Cependant, l'OEB ne va pas aussi loin que ça. Il crée plutôt une classe spéciale %(q:d'inventions) qui peuvent être revendiquées sous forme de structures d'information. Ces structures, que depuis 2000 l'OEB appelle %(ci:inventions mises en oeuvre par ordinateur), peuvent être appropriées à la fois par es droitzs d'auteur et des brevets.

#cto: Les programmes d'ordinateur sont ainsi %(q:protégés comme les oeuvres littéraires) (i.e. sujets au droit d'auteur), comme stipulé par l'article 10 des ADPIC et, en sus, brevetable comme des inventions techniques.

#ccn: Ceci suffit à violer de manière soutenable les ADPIC. Normalement une réalisation intellectuelle ne devrait pas tomber sous les deux régimes en même temps. Et l'article 10 affirmer que les programmes d'ordinateur tombe sous le droit d'auteur. S'ils sont %(q:protégés à la fois comme des oeuvres littéraires et comme des inventions), alors en pratique ils ne sont plus %(q:protégés comme les oeuvres littéraires), puisque les ouvres sous le droit d'auteurs sont caractérisé par le fait que les idées qui les soutiennent doivent rester libres de parcours. Si à la fois le droit d'auteur et le brevet s'appliquent au logiciel, la propriété acquise par un régime s'expose à être dévaluée par l'autre.

#Wab: L'OEB et la Commission européenne ont été encore plus loin dans le viol des ADPIC.

#aiW: En partant de la création de la classe spéciale des %(q:inventions mises en oeuvre par ordinateur) qui peuvent être revendiquées d'une manière particulière, habituellement non autorisée (à savoir sous la forme d'une structure d'information décrivant %(q:l'invention)), ils se sont efforcés de créer un corpus de droits %(e:sui generis) des brevets.

#naw: En 2000, la Commission européenne et l'OEB ont tous deux rapidement adopté les doctrines issues d'une nouvelle décision de la Chambre de recours de l'OEB, appelée %(pb:Contrôle du sytème de caisses de retraite). Cette décision établit des règles particulières pour l'examen du caractère technique des %(q:inventions mises en oeuvre par ordinateur), telles que l'évaluation de la %(q:revendication dans son ensemble) plutôt que la réalisation derrière cette revendication, entraînant ainsi que tout programme d'ordinateur peut passer l'exigence d'invention technique, et, au lieu de cette exigence dénudée de sens, établissant une nouvelle exigence de %(q:contribution technique dans l'activité inventive), qui n'a aucun fondement dans l'article 27 des ADPIC.

#opW: Le groupe de travail du Conseil de l'Union européenne a fait un pas de plus dans ses %(co:documents secrets de novembre 2002 et janvier 2004). Il a laissé décider ceux qui déposent des brevets lequel des deux régimes ils préféreraient voir appliquer à leur réalisation : les doctrines standards du droit des brevets ou les doctrines sui generis pour les %(q:inventions mises en oeuvre par ordinateur).

#gds: À l'opposé, le Parlement européen a proposé de clarifier les ADPIC en %(ep:affirmant), inter alia, que le traitement de données (l'informatique) n'est pas juste une autre discipline appliquée des sciences naturelles (%(q:un domaine technique)) mais plutôt un niveau d'abstraction, applicable à tous les champs des sciences naturelles, comme des sciences sociales. Ces clarifications intègrent superbement les articles 10 et 27 des ADPIC et la CBE. Les propositions du Parlement sont ignorées et %(cs:discrédités sans raison valable) par la communauté des administrateurs de brevets et des avocats des grosses entreprises, qui, au printemps 2004, continue de monopoliser les prises de décisions à l'Office européen des brevets, au Conseil de l'UE (Consilium) et à la Commission européenne (CEC).

#ytt: En résumé, on peut dire que l'establishment européen des brevets :

#gmI: refuse de clarifier et de concrétiser le sens de l'accord sur les ADPIC ;

#Igr: assimile à tort l'accord sur les ADPIC avec %(q:les pratiques des USA), en utilisant la menace d'une prétendue incompatibilité avec les ADPIC pour encourager la Peur, l'incertitude et le doute (Fear, Uncertainty and Doubt -FUD) ;

#iah: tente d'imposer en Europe un régime sui generis des brevets, qui est incompatible avec l'accord sur les ADPIC.

#Tea: L'argumentation pour une brevetabilité universelle est basée sur un assortiment de supercheries à propos des ADPIC et d'idéologie irréfléchie.  L'effet de bord de cet article est qu'il révèle des détails intéressants, comme le fait que le Parlement allemand a ratifié les ADPIC à la condition explicite que les ADPIC n'entraînent pas la brevetabilité du logiciel.

#tbt: Dans une décision récente la Chambre de recours des brevets suédoise a rejeté un brevet sur une méthode d'affaire :

#O7W: Comme souligné par le plaignant, la Suède est contrainte de suivre les règles de l'accord sur les ADPIC depuis qu'elle a rejoint l'OMC en 1995. Ce qui est significatif dans cette affaire est que l'article 27 (1) dit que la possibilité d'obtenir un brevet devrait exister pour tout domaine technique. Cette décision n'a pas forcément changé quoique ce soit dans le § 1 PL [Droit des brevets suédois].  Ni l'article 27 (1) ni aucun autre passage de l'accord ne donne une définition léfale du concept %(q:d'invention). Il n'y a pas d'explication sur ce que l'on est censé considérer par %(q:domaine technique), à ce sujet et pour une toile de fond sur cet article, voir %(js:Joseph Straus) dans GRUR Int. 1996 p. 179: %(q:Bedeutung des TRIPs für das Patentrecht), part V b)) iii, items 35 to 37, dans lequel est également décrite la relation avec l'article 52 de la CBE.

#ejn: Un verdict allemand de 2000 qui rejette de façon identique la supercherie des ADPIC

#caI: Cette étude commandée par l'UE rejette également la supercherie des ADPIC.

#law: Les défenseurs des brevets logiciels ont soutenu que l'article 27(1) n'autorisait pas l'exclusion des logiciels de la brevetabilité, puisqu'un logiel est considéré comme faisant partie d'un %(q:domaine technique). Les discussions ayant précédé l'adoption de l'accord sur les ADPIC, ne confirment cependant pas cette lecture. En l'absence d'une définition légale d'une %(q:invention), on peur raisonnablement penser que l'accord laisse les états membres décider de ce qui constitue une invention brevetable, et si oui ou non cela inclue les logiciels en tant que tels.

#sbi: Les avocats des brevets du gouvernement des USA se sont durement battu contre l'incorporation de l'article 27 des ADPIC dans le Traité sur le droit matériel des brevets, car ils voient une restriction del abrevetabilité dans les termes %(q:technique) et %(q:industriel).

#ogn: L'article 30 des ADPIC a été utilisé par des avocats des brevets de grosses entreprises et de leurs soutiens gouvernementaux pour faire pression contre les exceptions d'interopérabilité soutenus par toutes les commissions concernées du Parlement européen.

#TpW: Les plans de développements du brevet de la Commission européenne sont basés sur un %(q:tissu rationnel de mensonges), comprenant la supercherie des ADPIC.

#Mtt: Nils Baggehufwudt, du ministère de l'économie allemand, utilise l'illusion des ADPIC dans une lettre dans laquelle il justifie la politique du gouvernement allemand de soutien à la brevetabilité du logiciel au-delà même de la position prônée par la Commission européenne.

#ae0: Conférence de Louvain du 11 au 13 mars 2004

#iWW: À cette conférence, le professeur Alberto Bercovitz a souligné que l'OEB et la Commission européenne tentaient de créer un droit des brevets sui generis incompatible avec les ADPIC.  Jean-Charles Van Eeckhaude, de la Direction générale au Commerce de la Commission européenne a signalé que %(q:la communauté de la Propriété Intellectuelle) à l'OMC détruisait le système des ADPIC en encourageant des interpretations extrèmes des ADPIC dans les intérêts des lobbies des détenteurs de droits. L'un des problèmes du système des ADPIC est que la procédure d'arbitrage semble elle-même être aux mains de la %(q:communauté de la PI, comme de récents exemples l'ont montré.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpattrips ;
# txtlang: fr ;
# multlin: t ;
# End: ;

