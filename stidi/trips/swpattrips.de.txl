<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Der TRIPs-Vertrag und Softwarepatente

#descr: Europas gesetzgebende Patentjuristen zitieren oft den TRIPs-Vertrag
als Grund für die angebliche Notwendigkeit, Computerprogramme
patentierbar zu machen.  Hier finden Sie alles, was Sie über das
%(q:TRIPs-Scheinargument) wissen müssen.

#Bnt: Christian Beauprez: Art 10 TRIPs und %(q:Computer-Implementierte
Erfindungen)

#eos: Ein britischer Experte für Software-Gesetzgebung argumentiert, dass
Computerprogramme entsprechend TRIPs Art 10 als %(q:literarische Werke
geschützt) werden müssen, und dies bedeutet dass die darin enthaltenen
Ideen frei sind, wie in literarischen Werken.

#TWI: OMC: ADPIC

#feO: Text des TRIPs-Vertrages auf den WTO-Webseiten

#rql: Dan L. Burk und Mark Lemley, %(q:Hängt das Patentrecht von Technologie
ab?)

#yWo: Berkeley Technology Law Journal (2002) und %(q:Stellmöglichkeiten des
Patentrechts), 89 Virginia Law Review (erschienen Dezember 2003). Der
US-Rechtswissenschaftler Mark Lemley erklärt den historischen
Zusammenhang von TRIPs und vertritt die Ansicht, dass Art 27ff
keineswegs so strikt ausgelegt werden muß wie die Eine-Lösung-für-alle
Verfechter behaupten, dass es sein muss.

#Dea: Verlangt TRIPs die Patentierbarkeit von Computerprogrammen?

#TrAX: TRIPs Artikel %s

#Caa: Computerprogramme und Zusammenstellungen von Daten

#Plc: Patentfähige Gegenstände

#pRf: Ausnahmen für die gewährten Rechten

#Tai: Artikel 33 verlangt, dass alle Patente eine Mindestlaufzeit von 20
Jahren haben müssen.

#Ske: Art 7-8: Ein Freihandelsvertrag und seine Auslegung

#iWe: TRIPs Verletzungen durch das EPA und den EU Richtlinienvorschlag

#TiW: Das %(tr:Übereinkommen über handelsbezogene Aspekte der Rechte des
Geistigen Eigentums) wurde am 15. Dezember 1993 im Rahmen der Gründung
der Welthandelsorganisation (WTO) gegründet.  Es legt minimale
Anforderungen für nationale Rechtssysteme fest, um
%(q:sicherzustellen, daß die Maßnahmen und Verfahren zur Durchsetzung
der Rechte des geistigen Eigentums nicht selbst zu Schranken für den
rechtmäßigen Handel werden) (Präambel).

#AnW: Artikel 27 wird seit Mitte der 90er Jahre regelmäßig von
Patentjuristen zitiert, um für einen Universalitätsanspruch des
Patentwesens und insbesondere eine Ausweitung des Patentschutzes auf
Computerprogramme und andere immaterielle Gegenstände zu
argumentieren.

#PWh: Hierzu %(ph:bemerkte) Paul Hartnack, Comptroller General des
Britischen Patentamtes, 1997 bei der Londoner Anhörung über
Softwarepatentierung:

#Sfl: Manche Leute haben behauptet, der TRIPs-Vertrag verpflichte uns,
Patente auf Software zu erteilen, da darin steht %(q:Patente sollen
für alle Erfindungen auf allen Gebieten der Technik erhältlich sein,
sofern sie ... einer industriellen Anwendung zugänglich sind).

#Hde: Es kommt aber darauf an, wie man diese Formulierung interpretiert.

#IWe: Ist ein einfaches Stück Programmtext eine Erfindung?

#Eli: Nach europäischem Recht ist es das nicht.

#Ioe: Ist reine Programmlogik eine Technologie?

#Moa: Viele würden das verneinen.

#Ifl: Ist sie der %(q:industriellen) Anwendung zugänglich?

#Ahy: Wiederum würden, was einen Großteil der Software betrifft, viele das
verneinen.

#Tnc: Der TRIPs-Vertrag lässt sich als Argument für eine erweiterten
Patentschutz für Software anführen.

#BWW: Aber die Entscheidung, dies zu tun, sollte auf soliden
wirtschaftspolitischen Überlegungen beruhen.

#WWn: Liegt es im Interesse der Europäischen Wirtschaft und der Europäischen
Verbraucher, diesen Schritt zu unternehmen?

#IiW2: Bei der Ratifizierung von GATT/TRIPs wies der Deutsche Bundestag
ausdrücklich darauf hin, das zwischen PatG §1 und TRIPs Art 27(1) kein
Konflikt bestehe.

#Wpl: In einer %(bs:Entscheidung von 2000), in der es einen Anspruch auf ein
Computer-Programm zurückweist, widerlegt das Bundespatentgericht
nebenbei auch die TRIPs-Lüge:

#Ami: Auch das Übereinkommen über handelsbezogene Aspekte der Rechte des
geistigen Eigentums (Trade Related Aspects of Intellectual Property
Rights = TRIPS) führt zu keiner anderen Beurteilung der
Patentfähigkeit.  Abgesehen von der Frage, in welcher Form das
TRIPS-Abkommen - unmittelbar oder mittelbar - anwendbar ist (...),
würde nämlich auch die Heranziehung von Art 27 Abs 1 TRIPS-Abkommen
hier nicht zu einem weitergehenden Schutz führen.  Mit der dortigen
Formulierung, wonach Patente für Erfindungen auf allen Gebieten der
Technik erhältlich sein sollen, wird nämlich im Grunde nur die bisher
schon im deutschen Patentrecht vorherrschende Auffassung bestätigt,
wonach der Begriff der Technik das einzig brauchbare Kriterium für die
Abgrenzung von Erfindungen gegenüber andersartigen geistigen
Leistungen, mithin die Technizität Voraussetzung für die
Patentfähigkeit ist (in der Entscheidung des BGH
%(q:Logikverifikation) ist insoweit die Rede von %(q:nachträglicher
Bestätigung) der Rechtsprechung durch die Regelung in Art 27 Abs 1
TRIPS-Abkommen).  Auch der Ausschlusstatbestand des §1 Abs 2 Nr 3 und
Abs 3 PatG kann vor dem Hintergrund, dass er auf dem Gedanken des
fehlenden technischen Charakters dieser Gegenstände beruht, nicht im
Widerspruch zu Art 27 Abs 1 TRIPS-Abkommen gesehen werden.

#Wti: Das Bundespatentgericht bezieht sich hierbei auf die
Grundsatzentscheidung %(dp:Dispositionsprogramm) nach der das
Vorhandensein oder die Abwesenheit von %(e:beherrschbaren
Naturkräften) bei der Lösung eines Problems das einzige hiflreiche
Kriterium zur Begrenzung der patentierbaren Erfindungen ist.
Entsprechend dieser Lehrmeinung ist die Datenverarbeitung kein Feld
der Technik wie Gert Knolle, der führende Wissenschaftler aus der Zeit
dieser Fragestellung, %(gk:in seiner vielzitierten Analyse der
Dispositionsprogramm Entscheidung von 1977) erklärt:

#Wmr: Die Automatische Datenverarbeitung (ADV) ist heute zu einem
unentbehrlichen Hilfsmittel in allen Bereichen der menschlichen
Gesellschaft geworden und wird dies auch in Zukunft bleiben.  Sie ist
ubiquitär.  ...  Ihre instrumendale Bedeutung, ihre Hilfs- und
Dienstleistungsfunktion unterscheidet die ADV von den ...
Einzelgebieten der Technik und ordnet sie eher solchen Bereichen zu
wie z.B. der Betriebswirtschaft, deren Arbeitsergebnisse und Methoden
... von allen Wirtschaftsunternehmen benötigt werden und für die daher
prima facie ein Freihaltungsbedürfnis indiziert ist.

#lhi: Dies ist genau das was das europäische Parlament in seinem
%(ad:verbesserten Vorschlag der Direktive) aussagt:

#Am32: Nach dem Übereinkommen über die Erteilung europäischer Patente
(Europäisches Patentübereinkommen) vom 5. Oktober 1973 (EPÜ) und den
Patentgesetzen der Mitgliedstaaten gelten Programme für
Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien,
mathematische Methoden, ästhetische Formschöpfungen, Pläne, Regeln und
Verfahren für gedankliche Tätigkeiten, für Spiele oder für
geschäftliche Tätigkeiten sowie die Wiedergabe von Informationen
ausdrücklich nicht als Erfindungen, weshalb ihnen die Patentierbarkeit
abgesprochen wird. Diese Ausnahme gilt, da die besagten Gegenstände
und Tätigkeiten keinem Gebiet der Technik zugehören.

#Am107: Ein %(q:technischer Beitrag), auch %(q:Erfindung) genannt, bedeutet
einen Beitrag zum Stand der Kunst in einem technischen Bereich. Der
technische Charakter des Beitrags ist eine der vier Anforderungen für
die Patentierbarkeit. Weiterhin muss der technische Beitrag, um ein
Patent zu erhalten, neu, nicht offensichtlich und im industriellen
Bereich einsetzbar sein. Der Gebrauch der Naturkräfte um damit
physikalische Effekte hervor zu rufen, die über die rein digitale
Repräsentation von Informationen hinausgeht gehört in den technischen
Sektor. Die Verarbeitung, der Umgang und die Anzeige von Informationen
gehören nicht zu einem Feld der Technik, selbst dort nicht wo
technische Geräte an solch einem Vorgang beteiligt sind.

#Am45: Die Mitgliedsstaaten sollen sicherstellen, dass die Datenverarbeitung
im Sinne des Patentrechts keinem Feld der Technik zugerechnet werde,
und dass Neuerungen im Bereich der Datenverarbeitung nicht als
Erfindeungen im Sinne des Patentrechts betrachtet werden.

#Cby: Computerprogramme, egal ob sie als Quellcode oder als
Maschinenprogrammcode ausgedrückt wurden, werden als Werke der
Literatur nach der Berner Übereinkunft (1971) geschützt.

#Cnx: Zusammenstellungen von Daten oder sonstigem Material, egal ob in
maschinenlesbarer oder anderer Form, die aufgrund der Auswahl oder
Anordnung ihres Inhalts geistige Schöpfungen bilden, werden als solche
geschützt. Dieser Schutz, der sich nicht auf die Daten oder das
Material selbst erstreckt, gilt unbeschadet eines an den Daten oder
dem Malerial selbst bestehenden Urheberrechts.

#Sit: Vorbehaltlich der Absätze 2 und 3 ist vorzusehen, daß Patente für
Erfindungen auf allen Gebieten der Technik erhältlich sind, sowohl für
Erzeugnisse als auch für Verfahren, vorausgesetzt, daß sie neu sind,
auf einer erfinderischen Tätigkeit beruhen und gewerblich anwendbar
sind.

#Ftt: Fußnote 5

#Fqy: Im Sinne dieses Artikels kann ein Mitglied die Begriffe
%(q:erfinderische Tätigkeit) und %(q:gewerblich anwendbar) als
Synonyme der Begriffe %(q:nicht naheliegend) beziehungsweise
%(q:nutzbar) auffassen.

#Sec: Vorbehaltlich des Artikels 65 Absatz 4, des Artikels 70 Absatz 8 und
des Absatzes 3 dieses Artikels sind Patente erhältlich und können
Patentrechte ausgeübt werden, ohne daß hinsichtlich des Ortes der
Erfindung, des Gebiets der Technik oder danach, ob die Erzeugnisse
eingeführt oder im Land hergestellt werden, diskriminiert werden darf.

#eee: Es soll festgehalten werden, dass der Text ausdrücklich dazu
auffordert für die darin verwendeten abstrakten Definitionen von
einander abweichende Interpretationen zu entwickeln, wie etwa für
%(q:nicht offensichtlich) und %(q:industrielle Anwendbarkeit).

#nec: Während der Absatz %(q:Diskriminierung) im Interesse freier und
gleicher Handelskonditionen verbietet, fordert er aber kein konkretes
Erfindungskonzept. Es kann jedoch abgeleitet werden, dass ein
Erfindungskonzept bevorzugt werden sollte, das förderlich für einen
freien handel und wirtschaftliche Entwicklung ist, und in dem die
Begriffe %(q:Technologie), %(q:Industrie) usw. keine leeren Worte
sind.

#trV: Selbst in einer Welt der patentierbaren %(q:Technologie) kann Art
27(1) nur schwerlich als als starres Gerüst interpretiert werden, das
jeglichen Feinabgleich verbietet. Wenn es auf diese strenge Art
interpretiert werden müsste, wie manche Patentanwälte vorschlagen,
dann würde das US-Patentrecht über TRIPs in mindestens vier Bereichen
stolpern: Medikamente [35 USC 155,156, Begriffsaufweitung; 35 USC
271(e), experimenteller Einsatz]; biotechnologische Prozesse [35 USC
103(b), definiert besonderen nicht-offensichtlichen Standard];
medizinische und operative Methoden [35 USC 287(c), Begrenung bei
Heilmitteln], und Geschäftsmethoden [35 USC 273(a)(3), definiert
Rechte von Erstbenutzern].

#ere: Die Mitglieder dürfen begrnzt Ausnahmen zu den Exclusivrechten die ein
Patent verleiht definieren, vorausgesetzt, dass solche Ausnahmen nicht
unverhältnismässig im Koflikt mit der normalen Nutzung des Patents
stehen und die rechtmässige Interessen des Patenteigners nicht
übermässig beschneiden, indem sie sie berechtigten Interessen von
Dritten berücksichtigen.

#pen: Dieser Abschnitt wird von der Lobby der Patenteigentümer immer dann
zitiert wenn jemand versucht die Rechte der Patenteigner
einzuschränken, egal ob dies nun in %(q:vernünftiger) oder
%(q:unzumutbarer) Art geschieht.

#Ttr: Dies ist wichtig zu wissen, denn es erübrigt wohlmeinende Vorschläge
wie den von Amazon-Gründer Jeff Bezos, man solle die Patentlaufzeit im
Softwarebereich auf 3-5 Jahre kürzen.

#nui: Der TRIPs Vertrag hat keine zeitliche Begrenzung. Er ist gültig, so
lange die Welthandelsorganisation (WTO) als Gesamtheit keine Einingung
über eine Änderung erreicht. Die Organisation der WTO ist weit
entfernt von demokratischer Teilnahme und viele WTO Mitglieder sind
diktatorisch regierte Staaten. Sollte irgend ein Land aus TRIPs
aussteigen wollen, so muss es die WTO verlassen, womit es den
Zusammenbruch seiner exportierenden industrie riskieren würde. Der
Vertrag wurde in Hinterzimmer zwischen Ministerialbeamten ausgehandelt
und für die meisten Sprachen der Welt gibt es noch nicht einmal
Übersetzungen. All diese Aspekte machen es erforderlich den TRIPs
Vertrag mit größter Vorsicht zu interpretieren sowie in weitem Umfang
die erlaubte Flexibilität zu strapazieren damit ein grechter Ausgleich
von Rechten und Pflichten im Sinne des Zielkomplexes des freien
Handels erreicht werden kann, dem der Vertrag dient.

#tsn: Die Verfasser des Vertrages waren sich dieses Problems bewusst. In den
allgemeinen Bestimmungen nahmen sie Abschnitte wie etwa den folgenden
auf:

#bcv: Ziele

#pea: Der Schutz und die Durchsetzung von Rechten auf geistiges Eigentum
soll dazu beitragen, dass die technologische Innovation gefördert
wird, und sie soll zum Transfer und der Verbreitung von Technologie
beitragen, zum gegenseitigen Vorteil von Herstellern und Anwendern von
technologischem Wissen  beitragen, und den sozialen und
wirtschaftlichen Wohlstand fördern, und zu einem Gleichgewicht der
Rechte und Pflichten führen.

#rcl: Prinzipien

#aWu: Die Mitglieder können, durch Ausformulierung und Weiterentwicklung
ihrer Gesetze und Vorschriften, Massnahmen ergreifen die notwendig
sind um die öffentliche Gesundheit und die Versorgung mit
Nahrungsmitteln zu schützen, und für die Vertretung der allgemeinen
Interesse in Bereichen mit Kernbedeutung für die sozio-ökonomische und
technologische Entwicklung, vorausgesetzt, dass solche Massnahmen mit
den Bestimmungen der Vereinbarungen in Einklang stehen.

#eaa: Geeignete Massnahmen, vorausgesetzt dass sie konsistent sind mit den
Regelungen dieser Vereinbarungen, können notwendig sein um den
Missbrauch der Rechte auf geistigen Eigentums durch den Rechteinhaber
zu verhindern oder um auf Praktiken zu reagieren, die
unverhältnissmässig den Handeln behindern oder nachteiligen Einfluss
auf den internationalen Technologietranfer haben.

#Igl: Gleichzeitig ist zu fragen, ob die Beanspruchung immaterieller
Gegenstände aus dem Bereich der Programmlogik, der
Kommunikationsschnittstellen und der Organisationsmethoden nicht dazu
geeignet ist, Handelsbarrieren aufzubauen, die im Gegensatz zum Geist
von TRIPs und anderen Freihandels-Abkommen stehen.

#ero: TRIPs definiert Umschreibungen für das Patentrecht, die entworfen
wurden um den freien Handel zu fördern und um Eingriffe der
Regierungen zur Bevorzugung der heimischen Industrie gegenüber der
ausländischen zu reduzieren. Es wird etwas darüber gesagt wie Gesetze
strukturiert sein sollen, z.B. %(q:keine Diskriminierung zum Vorteil
der jeweiligen lokalen Industrie), %(q:keine willkürliche Begrenzung
der Durchsetzbarkeit). Es werden hiermit Grenzen gefördert, die auf
%(e:systematischen) Betrachtungen beruhen, z.B. die Gewichtung der
Rechte der Patentierer gegen andere Rechte gleicher Schwere, wie etwa
das Eigentum nach Urheberrecht (Art 10 TRIPs), die
Veröffentlichungsfreiheit (Art 10 Europäische Konvention der
Menschenrechte ECHR) oder das Recht auf Zugriff zu
Kommunikationsstandards.

#WWt: Es ist sehr wichtig für jedes Patentrechtsvorhaben dass die abstrakten
Regeln des TRIPs-Vertrages konkretisiert werden. Jedes Rechtsvorhaben
diesen Teil nicht erfüllt darf nicht dafür in Anspruch genommen werden
diesen Klarstellungszweck zu erfüllen.

#str: Obwohl die wirtschaftlichen Verfahrensweisen im Sinne der abstrakten
Bedingungen, wie sie in TRIPs dargelegt werden, gerechtfertigt werden
sollten, können sie nicht nur von TRIPs alleine abgeleitet werden.

#Wre: It is poor draftsmanship to copy&paste abstract doctrines from TRIPs
into European laws, which are supposed to provide guidance at a more
concrete level.  %(ep:Art 52%(pe:1) EPC was revised) in this
ill-advised way by the Diplomatic Conference of 2000, and the European
Parliament's rapporteur on the %(sd:software patent directive
project), %(am:Arlene McCarthy MEP), %(am:proposed) to directly write
Art 30 TRIPs into Art 6a of the Directive.   Such actions do not only
make make the proposed laws unclear.  They also deprive European of
maneuvering freedom in future renegotiations of TRIPs, which may well
be needed in order to keep the TRIPs framework workable at all.

#adi: TRIPs was negotiated by delegations that represented the dominant
interests of another era.  Software was largely considered
unpatentable, and opensource development and distribution was almost
unknown.  TRIPs should be interpreted in a way that does not benefit
some production technologies, business models, and industries at the
expense of others.  The important thing is to enhance productivity in
all industries.

#awW: Many limits of the TRIPs system, at least in its more rigid
interpretations, have become so apparent, that even the Americans, who
were the chief promoters of the TRIPs treaty, are tailoring it to
their advantage and thereby arguably violating some of its provisions.

#teo: There is now a lot less worry that the US might make a complaint about
any aspects of the patent system in the EU, because the EU would hit
straight back with complaints about US preference in the US patent
system.

#oot: Any judgments coming out of such a row would be likely to open a
sufficient number of politically difficult issues on both sides of the
Atlantic that nobody wants to go there.

#aeW: The European Patent office started allowing %(pc:program claims) in
1998.  In the justifying decisions %(t1:T 1173/97) and %(t2:T 935/97)
it is stated:

#otW: Computerprogramme können als patentfähige Erfindung betrachtet werden
wenn sie einen technischen Charakter aufweisen.

#9ir: Computer programs, as described in program claims granted by the EPO
since 1998, are information structures, consisting of symbolic
entities only.  Any %(q:technical character) which they might have can
be found only on the meaning side of the symbolic entities.  Likewise
one could speak of the %(q:technical character) of a set of chemical
formulas, of a collection of construction drawings or even of a
science-fiction novel, and empower every patent owner to monopolise
the distribution of any information which describes his
%(q:invention).

#iWv: However the EPO does not go as far as this.  Instead it creates a
special class of %(q:inventions) which can be claimed in the form of
information structures.  These structures, since 2000 called
%(ci:computer-implemented inventions) by the EPO, can be appropriated
both by copyright and by patents.

#cto: Computer programs are thus %(q:protected as literary works) (i.e.
subjected to copyright), as stipulated by Art 10 TRIPs, and, in
addition, patentable as technical inventions.

#ccn: This alone is arguably a violation of TRIPs.  Normally one
intellectual achievement should not fall under two different regimes
at the same time, and Art 10 states that computer programs fall under
copyright.  If they are %(q:protected both as literary works and as
inventions) then they are in effect no longer %(q:protected as
literary works), since it is a characteristic of copyrighted works
that the ideas embodied therein remain free.  If both copyright and
patents apply to software, property that was acquired one regime is
exposed to devaluation by the other.

#Wab: Das EPA und die Europäische Kommission sind dennoch weiter gegangen
beim Verletzen von TRIPs.

#aiW: Starting from the creation of a special class of
%(q:computer-implemented inventions) which can be claimed in a
special, usually impermissible way (namely in the form of an
information structure describing the %(q:invention)), they have
endeavored to create a body of sui generis software patent law.

#naw: In 2000, both the EPO and the European Commission quickly adopted the
doctrines of a new decision by the EPO's Technical Board of Appeal,
called %(pb:Controlling Pension Benefits System).  This decision
establishes special rules for examining the technical character of
%(q:computer-implemented inventions), such as assessing the %(q:claim
as a whole) rather than the achievement behind this claim, thereby
making any computer program pass the requirement of technical
invention, and, instead of this voided requirement, establishing a new
requirement of %(q:technical contribution in the inventive step),
which has no basis in Art 27 TRIPs.

#opW: The Working Party of the Council of the European Union went even one
step further in its %(co:secret papers of November 2002 and January
2004).  They leave it to the patent applicant to decide which of the
two regimes he wants to see applied to his achievement: the standard
doctrines of patent law or the sui generis doctrines for
%(q:computer-implemented inventions).

#gds: By contrast, the European Parliament has proposed to clarify TRIPs by
%(ep:stating), inter alia, that data processing (informatics) is not
just another discipline applied natural science (%(q:field of
technology)) but rather a layer of abstraction, applicable to all
fields of natural as well as social science.  These clarifications
beautifully integrate Art 10, Art 27 and the EPC.  The Parliament's
proposals are ignored and %(cs:unreasonably discredited) by the
community of patent administrators and corporate patent lawyers, which
is, as of spring 2004, continuing to monopolise the decisionmaking at
the European Patent Office (EPO), the European Council (Consilium) and
the European Commission (CEC).

#ytt: Zusammenfassend kann gesagt werden, dass das europäische
Patent-Establishment

#gmI: sich weigert die Bedeutung des TRIPs-Vertrages zu verdeutlichen und zu
konkretisieren;

#Igr: fälschlicherweise den TRIPs-Vertrag mit der %(q:US Umsetzung)
gleichsetzt, wobei die Bedrohung von angeblicher
TRIPs-Unverträglichkeit zum Zweck des Säens von Angst, Unsicherheit
und Misstrauen (FUD) benutzt wird;

#iah: versucht über Europa eine sui generis Softwarepatentherschafft zu
stülpen die inkompatbiel zum TRIPs-Vertrag ist.

#Tea: Diese Argumentation für ein weltweit einheitliches universelles
Patentwesen stützt sich leider nur auf das TRIPs-Scheinargument in
Kombination mit allerlei unreflektierten Glaubenssätzen.  Beiläufig
verrät der Artikel aber interessante historische Details, wie etwa die
Tatsache, dass der Deutsche Bundestag den TRIPs-Vertrag unter der
expliziten Annahme unterzeichnete, dieser Vertrag fordere nicht die
Patentierbarkeit von Software.

#tbt: In einer aktuellen Entscheidung weist der schwedische
Patentgerichtshof für Berufungen folgendes zurück:

#O7W: As pointed out by the plaintiff, Sweden is bound to follow the rules
in the TRIPs agreement since it joined the WTO in 1995. What's
relevant to this case is that article 27 (1) says that the possibility
to get a patent should be available for every invention in any
technical field. This decision has not made any change in § 1 PL
[Swedish Patent Law] necessary.  Neither article 27 (1) nor any other
part of the agreement gives a legal definition of the concept
%(q:invention). There is no explanation of what is supposed to be
considered a %(q:technical) field, in this regard and for background
on the article see %(js:Joseph Straus) in GRUR Int. 1996 p. 179:
%(q:Bedeutung des TRIPs für das Patentrecht), part V b)) iii, items 35
to 37, in which the relationship to article 52 in the EPC is also
discussed.

#ejn: Ein deutsches Urteil aus dem Jahr 2000 das den TRIPs-Irrtum in
ähnlicher Weise zurückweist

#caI: Diese EU-Auftragsstudie weist die TRIPs-Irrtum ebenfalls zurück.

#law: Proponents of software patenting have argued that Article 27(1) does
not allow software from being excluded from patentability, since
computer software is to be considered a %(q:field of technology). The
discussions preceding adoption of the TRIPs agreement, however, do not
confirm such a reading. In the absence of a legal definition of
%(q:invention), the agreement arguably leaves it to the member states
to determine what constitutes a patentable invention, and whether or
not that includes computer software as such.

#sbi: The US government's patent lawyers have been fighting hard against
incorporation of Art 27 TRIPs into the new Substantive Patent Law
Treaty, because they see the words %(q:technical) and %(q:industrial)
as a restriction on patentability.

#ogn: Art 30 TRIPs is being used by corporate patent lawyers and their
governmental supporters for lobbying against interoperability
exemptions which have been favored by all concerned committees in the
European Parliament.

#TpW: Die Pläne des EPA und der Europäischen Kommission beruhen auf einem
%(q:in sich stimmigen Geflecht von Scheinargumenten), unter denen das
TRIPs-Scheinargument nie fehlen darf.

#Mtt: Nils Baggehufwudt from the German Ministery of Economics uses the
TRIPs fallacy in a letter in which he justifies the German
Government's policy of supporting software patentability even beyond
the level advocated by the European Commission.

#ae0: Louvain Conference 2004/03/11-13

#iWW: At the conference, Prof. Alberto Bercovitz pointed out that EPO and
European Commission is trying to create a TRIPs-incompatible sui
generis patent law.  Jean-Charles Van Eeckhaude from the European
Commission's DG Commerce pointed out that the %(q:IP Community) at WTO
is undermining the TRIPs system by promoting extreme interpretations
of TRIPs.  One of the problems of the TRIPs system is that the
arbitration process itself seems to be in the hands of the %(q:IP
Community), as can be seen from recent examples.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.3/site-lisp/mlht/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpattrips ;
# txtlang: de ;
# multlin: t ;
# End: ;

