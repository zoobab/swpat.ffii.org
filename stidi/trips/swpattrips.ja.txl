<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: The TRIPs Treaty and Software Patents

#descr: European patent authorities often cite the TRIPs treaty as a reason
for making computer programs and business methods patentable and for
making such patents enforcable in the most indecent ways.  This
reasoning is fallacious and easy to refute.  It appears moreover that
the European patent establishment itself is systematically violating
the TRIPs treaty.

#Bnt: Christian Beauprez: Art 10 TRIPs and %(q:Computer-Implemented
Inventions)

#eos: UK software law expert argues that according to TRIPs Art 10 computer
programs must be be %(q:protected as literary works), and this means
that the ideas embodied therein are free, as in literary works.

#TWI: OMC: ADPIC

#feO: Text of the TRIPs treaty on the WTO website

#rql: Dan L. Burk and Mark Lemley, %(q:Is Patent Law Technology-Specific?)

#yWo: Berkeley Technology Law Journal (2002) and %(q:Policy Levers in Patent
Law), 89 Virginia Law Review (forthcoming December 2003). US law
scholar Mark Lemley explains the historical context of TRIPs and
argues that Art 27ff can not be interpreted as rigidly as the
one-size-fits-all advocates pretend it should be.

#Dea: Does TRIPs require software patents?

#TrAX: TRIPs Article %s

#Caa: Computer Programs and Compilations of Data

#Plc: Patentable Subject Matter

#pRf: Exceptions to Rights Conferred

#Tai: Article 33 mandates that all patents must have a minimum duration of
20 years.

#Ske: Art 7-8: A Treaty for Free Trade and its Interpretation

#iWe: TRIPs violations by EPO and EU directive proposals

#TiW: The %(tr:Treaty on Trade Related Aspects of Intellectual Property
Rights), signed on 1993-12-15 as a constituting document of the World
Trade Organisation (WTO), sets minimal rules for national intellectual
property law in order to prevent member nations from using
intellectual property as a hidden trade barrier against other nations.

#AnW: Article 27 has often been construed by patent lawyers to imply that
patent claims must be allowed to extend to computer programs.

#PWh: Paul Hartnack, Comptroller General of the British Patent Office,
%(ph:commented) this question at the London hearing in 1997:

#Sfl: Some have argued that the TRIPs agreement requires us to grant patents
for software because it says %(q:patents shall be available for any
inventions in all field of technology, provided they are capable ...
of industrial application).

#Hde: Cependant, cela dépend de la façon dont on interprète ces mots.

#IWe: Is a piece of pure software an invention?

#Eli: La loi européenne dit que non.

#Ioe: Is pure software technology?

#Moa: Nombreux sont ceux qui pensent que non.

#Ifl: Is it capable of %(q:industrial) application?

#Ahy: Again, for much software many would say no.

#Tnc: TRIPs is an argument for wider protection for software.

#BWW: Mais la décision d'un tel renforcement doit être prise en se fondant
sur des arguments économiques réfléchis.

#WWn: Would it be in the interests of European industry, and European
consumers, to take this step?

#IiW2: In its ratification of GATT/TRIPs, the German legislature saw no
conflict between Sec. 1(2)(3) and 1(3) of the Patent Act and Art.
27(1) of TRIPs.

#Wpl: In a %(bs:decision of 2000), in which it rejects a claim to a computer
program, the German Federal Patent Court explicitely refutes the TRIPs
fallacy:

#Ami: The Treaty on Trade Related Aspects of Intellectual Property Rights
(TRIPs) does not entail any different judgment of patentability.  
Independently of the question as to in what form - directly or
indirectly - the TRIPs treaty is applicable here, the application of
Art 27 TRIPs would not lead to any extension of patentability here.  
The wording, according to which patents shall be available for
inventions in all fields of technology, merely confirms the dominating
view of german patent jurisprudence, according to which the concept of
technology (Technik) constitutes the only usable criterion for
delimiting inventions against other kinds of intellectual
achievements, and therefore technicity is a precondition for
patentability (the %(q:Logikverifikation) decision of the Federal
Court of Justice (BGH) sees Art 27 TRIPs as %(q:posterior
confirmation) of this jurisprudence).  The exclusion provision of Art
52 (2) and (3) EPC can also not be construed to be in conflict with
Art 27 TRIPs, since it is based on the notion of lacking technical
character of the excluded items.

#Wti: The Federal Patent Court here refers to the %(dp:Dispositionsprogramm)
doctrine, according to which the presence or not of %(e:controllable
forces of nature) in the solution of the problem is the only usable
criterion for delimiting the realm of patentable inventions. 
According to this doctrine, data processing is not a field of
technology, as Gert Kolle, the leading scholar of the time on this
question, %(gk:explains in his much-cited analysis of the
Dispositionsprogramm decision in 1977):

#Wmr: Automatic Data Processing (ADP) has today become an indispensable
auxiliary tool in all domains of human society and will remain so in
the future.  It is ubiquitous.  ... Its instrumental meaning, its
auxiliary and ancillary function distinguish ADP from the ...
individual fields of technology and liken it to such areas as
enterprise administration, whose work results and methods ... are
needed by all enterprises and for which therefore prima facie a need
of free availability (Freihaltungsbedürfnis) is indicated.

#lhi: This is exactly what the European Parliament has stated in its
%(ad:amended directive proposal):

#Am32: Under the Convention on the Grant of European Patents signed in Munich
on 5 October 1973 and the patent laws of the Member States, programs
for computers together with discoveries, scientific theories,
mathematical methods, aesthetic creations, schemes, rules and methods
for performing mental acts, playing games or doing business, and
presentations of information are expressly not regarded as inventions
and are therefore excluded from patentability. This exception applies
because the said subject-matter and activities do not belong to a
field of technology.

#Am107: %(q:technical contribution), also called %(q:invention), means a
contribution to the state of the art in technical field. The technical
character of the contribution is one of the four requirements for
patentability. Additionally, to deserve a patent, the technical
contribution has to be new, non-obvious, and susceptible of industrial
application. The use of natural forces to control physical effects
beyond the digital representation of information belongs to a
technical field. The processing, handling, and presentation of
information do not belong to a technical field, even where technical
devices are employed for such purposes.

#Am45: Member states shall ensure that data processing is not considered to
be a field of technology in the sense of patent law, and that
innovations in the field of data processing are not considered to be
inventions in the sense of patent law.

#Cby: Computer programs, whether in source or object code, shall be
protected as literary works under the %(bk:Berne Convention).

#Cnx: Compilations of data or other material, whether in machine readable or
other form, which by reason of the selection or arrangement of their
contents constitute intellectual creations shall be protected as such.
 Such protection, which shall not extend to the data or material
itself, shall be without prejudice to any copyright subsisting in the
data or material itself.

#Sit: Subject to the provisions of paragraphs 2 and 3, patents shall be
available for any inventions, whether products or processes, in all
fields of technology, provided that they are new, involve an inventive
step and are capable of industrial application.

#Ftt: Footnote 5

#Fqy: For the purposes of this Article, the terms %(q:inventive step) and
%(q:capable of industrial application) may be deemed by a Member to be
synonymous with the terms %(q:non-obvious) and %(q:useful)
respectively.

#Sec: Subject to paragraph 4 of Article 65, paragraph 8 of Article 70 and
paragraph 3 of this Article, patents shall be available and patent
rights enjoyable without discrimination as to the place of invention,
the field of technology and whether products are imported or locally
produced.

#eee: It should be noted that the text explicitely encourages differing
interpretations of some of the abstract terms used therein, such as
%(q:non-obviousness) and %(q:industrial application).

#nec: While the paragraph forbids %(q:discrimination) in the interest of
free and equal trading conditions, it does not mandate a specific
invention concept.  It could however be construed to favor an
invention concept which is favorable to free trade and economic
development and in which the terms %(q:technology), %(q:industry) etc
are not empty words.

#trV: Even within the realm of patentable %(q:technology), Art 27(1) can
hardly be interpreted as a rigid framework that outlaws all
fine-tuning.  If it was to be interpreted in this rigid way, as some
patent lawyers propose, U.S. law would fall afoul of TRIPs in at least
four areas:  pharmaceuticals [35 USC 155,156, term extensions; 35 USC
271(e), experimental use]; biotechnology processes [35 USC 103(b),
providing special non-obviousness standard]; medical and surgical
procedures [35 USC 287(c), limiting remedies], and methods of doing
business [35 USC 273(a)(3), providing prior user rights].

#ere: Members may provide limited exceptions to the exclusive rights
conferred by a patent, provided that such exceptions do not
unreasonably conflict with a normal exploitation of the patent and do
not unreasonably prejudice the legitimate interests of the patent
owner, taking account of the legitimate interests of third parties.

#pen: This clause is cited by patent owner lobbies whenever anyone tries to
restrict the rights of patent owners, no matter whether in
%(q:reasonable) or %(q:unreasonable) ways.

#Ttr: This is important to know, because it renders frequently recurring
proposals pointless, such as that of Amazon's CEO Jeff Bezos, who
advocates reducing the lifetime of software patents to 3-5 years.

#nui: The TRIPs treaty has no time limitation.  It is valid as long as the
World Trade Organisation (WTO) as a whole can not agree to change it. 
The organisation of WTO is far removed from democratic participation,
and many WTO members are dictatorial states.  If any country wants to
opt out of TRIPs, it will have to leave WTO, thereby risking a
collapse of its exporting industries.  The treaty was negotiated in
backrooms between ministerial officials, and for most of the world's
languages translations do not even exist.  All these considerations
make it imperative to interpret the TRIPs treaty with greatest care
and to make extensive use of the flexibility which it allows, so as to
achieve a fair balance of rights and obligations under the overall
objective of Free Trade which the treaty serves.

#tsn: The treaty drafters were aware of these problems.  In the General
Provisions, they included articles such as the following:

#bcv: Objectives

#pea: The protection and enforcement of intellectual property rights should
contribute to the promotion of technological innovation and to the
transfer and dissemination of technology, to the mutual advantage of
producers and users of technological knowledge and in a manner
conducive to social and economic welfare, and to a balance of rights
and obligations.

#rcl: Principles

#aWu: Members may, in formulating or amending their laws and regulations,
adopt measures necessary to protect public health and nutrition, and
to promote the public interest in sectors of vital importance to their
socio-economic and technological development, provided that such
measures are consistent with the provisions of this Agreement.

#eaa: Appropriate measures, provided that they are consistent with the
provisions of this Agreement, may be needed to prevent the abuse of
intellectual property rights by right holders or the resort to
practices which unreasonably restrain trade or adversely affect the
international transfer of technology.

#Igl: Software patents are well known to be a disaster in terms of
innovation, competition and balance of rights.  Patents on business
methods moreover systematically serve to restrain trade, and these
restrictions appear unreasonable to most people in the field.

#ero: TRIPs provides meta-rules for patent law, designed to promote free
trade and reduce the leverage of governments in favoring domestic
industries over foreign ones.  It says something about how laws should
be structured, e.g. %(q:no discrimination in favor of specific local
industries), %(q:no arbitrary limitation on enforcability).  It
thereby encourages limitations that are based on %(e:systematic)
considerations, e.g. weighing patentee rights against other rights of
equal weight, such as copyright property (Art 10 TRIPs), freedom of
publication (Art 10 European Convention of Human Rights ECHR) or the
right of access to communication standards.

#WWt: It is very important for any patent law project to concretise the
abstract rules of the TRIPs treaty.  Any law project that fails to do
so can not be claimed to serve the purpose of clarification.

#str: While economic policies should be justified in terms of the abstract
concepts laid down in TRIPs, they can not be derived from TRIPs alone.

#Wre: It is poor draftsmanship to copy&paste abstract doctrines from TRIPs
into European laws, which are supposed to provide guidance at a more
concrete level.  %(ep:Art 52%(pe:1) EPC was revised) in this
ill-advised way by the Diplomatic Conference of 2000, and the European
Parliament's rapporteur on the %(sd:software patent directive
project), %(am:Arlene McCarthy MEP), %(am:proposed) to directly write
Art 30 TRIPs into Art 6a of the Directive.   Such actions do not only
make make the proposed laws unclear.  They also deprive European of
maneuvering freedom in future renegotiations of TRIPs, which may well
be needed in order to keep the TRIPs framework workable at all.

#adi: TRIPs was negotiated by delegations that represented the dominant
interests of another era.  Software was largely considered
unpatentable, and opensource development and distribution was almost
unknown.  TRIPs should be interpreted in a way that does not benefit
some production technologies, business models, and industries at the
expense of others.  The important thing is to enhance productivity in
all industries.

#awW: Many limits of the TRIPs system, at least in its more rigid
interpretations, have become so apparent, that even the Americans, who
were the chief promoters of the TRIPs treaty, are tailoring it to
their advantage and thereby arguably violating some of its provisions.

#teo: There is now a lot less worry that the US might make a complaint about
any aspects of the patent system in the EU, because the EU would hit
straight back with complaints about US preference in the US patent
system.

#oot: Any judgments coming out of such a row would be likely to open a
sufficient number of politically difficult issues on both sides of the
Atlantic that nobody wants to go there.

#aeW: The European Patent office started allowing %(pc:program claims) in
1998.  In the justifying decisions %(t1:T 1173/97) and %(t2:T 935/97)
it is stated:

#otW: Programs for computers could be considered as patentable inventions if
they have a technical character.

#9ir: Computer programs, as described in program claims granted by the EPO
since 1998, are information structures, consisting of symbolic
entities only.  Any %(q:technical character) which they might have can
be found only on the meaning side of the symbolic entities.  Likewise
one could speak of the %(q:technical character) of a set of chemical
formulas, of a collection of construction drawings or even of a
science-fiction novel, and empower every patent owner to monopolise
the distribution of any information which describes his
%(q:invention).

#iWv: However the EPO does not go as far as this.  Instead it creates a
special class of %(q:inventions) which can be claimed in the form of
information structures.  These structures, since 2000 called
%(ci:computer-implemented inventions) by the EPO, can be appropriated
both by copyright and by patents.

#cto: Computer programs are thus %(q:protected as literary works) (i.e.
subjected to copyright), as stipulated by Art 10 TRIPs, and, in
addition, patentable as technical inventions.

#ccn: This alone is arguably a violation of TRIPs.  Normally one
intellectual achievement should not fall under two different regimes
at the same time, and Art 10 states that computer programs fall under
copyright.  If they are %(q:protected both as literary works and as
inventions) then they are in effect no longer %(q:protected as
literary works), since it is a characteristic of copyrighted works
that the ideas embodied therein remain free.  If both copyright and
patents apply to software, property that was acquired one regime is
exposed to devaluation by the other.

#Wab: The EPO and the European Commission have still gone further in
violating TRIPs.

#aiW: Starting from the creation of a special class of
%(q:computer-implemented inventions) which can be claimed in a
special, usually impermissible way (namely in the form of an
information structure describing the %(q:invention)), they have
endeavored to create a body of sui generis software patent law.

#naw: In 2000, both the EPO and the European Commission quickly adopted the
doctrines of a new decision by the EPO's Technical Board of Appeal,
called %(pb:Controlling Pension Benefits System).  This decision
establishes special rules for examining the technical character of
%(q:computer-implemented inventions), such as assessing the %(q:claim
as a whole) rather than the achievement behind this claim, thereby
making any computer program pass the requirement of technical
invention, and, instead of this voided requirement, establishing a new
requirement of %(q:technical contribution in the inventive step),
which has no basis in Art 27 TRIPs.

#opW: The Working Party of the Council of the European Union went even one
step further in its %(co:secret papers of November 2002 and January
2004).  They leave it to the patent applicant to decide which of the
two regimes he wants to see applied to his achievement: the standard
doctrines of patent law or the sui generis doctrines for
%(q:computer-implemented inventions).

#gds: By contrast, the European Parliament has proposed to clarify TRIPs by
%(ep:stating), inter alia, that data processing (informatics) is not
just another discipline applied natural science (%(q:field of
technology)) but rather a layer of abstraction, applicable to all
fields of natural as well as social science.  These clarifications
beautifully integrate Art 10, Art 27 and the EPC.  The Parliament's
proposals are ignored and %(cs:unreasonably discredited) by the
community of patent administrators and corporate patent lawyers, which
is, as of spring 2004, continuing to monopolise the decisionmaking at
the European Patent Office (EPO), the European Council (Consilium) and
the European Commission (CEC).

#ytt: In summary it can be said that the European patent establishment is

#gmI: refusing to clarify and concretise the meaning of the TRIPs treaty;

#Igr: wrongly equating the TRIPs treaty with %(q:US practise), using threats
of alleged TRIPs-incompatibility for purposes of fostering Fear,
Uncertainty and Distrust (FUD);

#iah: trying to impose a sui generis software patent regime on Europe which
is incompatible with the TRIPs treaty.

#Tea: The argumentation for universal patentability is based on a mixture of
the TRIPs fallacy with some unreflected ideology.  As a side-effect,
the article reveals some interesting details, such as the fact that
the German Parliament ratified TRIPs under the explicit assumption
that TRIPs does not mandate software patentability

#tbt: In a recent decsision the Swedish Patent Court of Appeals reject:

#O7W: As pointed out by the plaintiff, Sweden is bound to follow the rules
in the TRIPs agreement since it joined the WTO in 1995. What's
relevant to this case is that article 27 (1) says that the possibility
to get a patent should be available for every invention in any
technical field. This decision has not made any change in § 1 PL
[Swedish Patent Law] necessary.  Neither article 27 (1) nor any other
part of the agreement gives a legal definition of the concept
%(q:invention). There is no explanation of what is supposed to be
considered a %(q:technical) field, in this regard and for background
on the article see %(js:Joseph Straus) in GRUR Int. 1996 p. 179:
%(q:Bedeutung des TRIPs für das Patentrecht), part V b)) iii, items 35
to 37, in which the relationship to article 52 in the EPC is also
discussed.

#ejn: A German verdict from 2000 which rejects the TRIPs fallacy in a
similar way

#caI: This EU-commissioned study also rejects the TRIPs fallacy.

#law: Proponents of software patenting have argued that Article 27(1) does
not allow software from being excluded from patentability, since
computer software is to be considered a %(q:field of technology). The
discussions preceding adoption of the TRIPs agreement, however, do not
confirm such a reading. In the absence of a legal definition of
%(q:invention), the agreement arguably leaves it to the member states
to determine what constitutes a patentable invention, and whether or
not that includes computer software as such.

#sbi: The US government's patent lawyers have been fighting hard against
incorporation of Art 27 TRIPs into the new Substantive Patent Law
Treaty, because they see the words %(q:technical) and %(q:industrial)
as a restriction on patentability.

#ogn: Art 30 TRIPs is being used by corporate patent lawyers and their
governmental supporters for lobbying against interoperability
exemptions which have been favored by all concerned committees in the
European Parliament.

#TpW: The European Commission's patent expansion plans are based on a
%(q:consistent network of fallacies), including the TRIPs fallacy.

#Mtt: Nils Baggehufwudt from the German Ministery of Economics uses the
TRIPs fallacy in a letter in which he justifies the German
Government's policy of supporting software patentability even beyond
the level advocated by the European Commission.

#ae0: Louvain Conference 2004/03/11-13

#iWW: At the conference, Prof. Alberto Bercovitz pointed out that EPO and
European Commission is trying to create a TRIPs-incompatible sui
generis patent law.  Jean-Charles Van Eeckhaude from the European
Commission's DG Commerce pointed out that the %(q:IP Community) at WTO
is undermining the TRIPs system by promoting extreme interpretations
of TRIPs.  One of the problems of the TRIPs system is that the
arbitration process itself seems to be in the hands of the %(q:IP
Community), as can be seen from recent examples.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpattrips ;
# txtlang: ja ;
# multlin: t ;
# End: ;

