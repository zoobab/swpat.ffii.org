epue52de.lstex:
	lstex epue52de | sort -u > epue52de.lstex
epue52de.mk:	epue52de.lstex
	vcat /ul/prg/RC/texmake > epue52de.mk


epue52de.dvi:	epue52de.mk epue52de.tex
	latex epue52de && while tail -n 20 epue52de.log | grep references && latex epue52de;do eval;done
	if test -r epue52de.idx;then makeindex epue52de && latex epue52de;fi
epue52de.pdf:	epue52de.mk epue52de.tex
	pdflatex epue52de && while tail -n 20 epue52de.log | grep references && pdflatex epue52de;do eval;done
	if test -r epue52de.idx;then makeindex epue52de && pdflatex epue52de;fi
epue52de.tty:	epue52de.dvi
	dvi2tty -q epue52de > epue52de.tty
epue52de.ps:	epue52de.dvi	
	dvips  epue52de
epue52de.001:	epue52de.dvi
	rm -f epue52de.[0-9][0-9][0-9]
	dvips -i -S 1  epue52de
epue52de.pbm:	epue52de.ps
	pstopbm epue52de.ps
epue52de.gif:	epue52de.ps
	pstogif epue52de.ps
epue52de.eps:	epue52de.dvi
	dvips -E -f epue52de > epue52de.eps
epue52de-01.g3n:	epue52de.ps
	ps2fax n epue52de.ps
epue52de-01.g3f:	epue52de.ps
	ps2fax f epue52de.ps
epue52de.ps.gz:	epue52de.ps
	gzip < epue52de.ps > epue52de.ps.gz
epue52de.ps.gz.uue:	epue52de.ps.gz
	uuencode epue52de.ps.gz epue52de.ps.gz > epue52de.ps.gz.uue
epue52de.faxsnd:	epue52de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo epue52de-??.g3n`;source faxsnd main
epue52de_tex.ps:	
	cat epue52de.tex | splitlong | coco | m2ps > epue52de_tex.ps
epue52de_tex.ps.gz:	epue52de_tex.ps
	gzip < epue52de_tex.ps > epue52de_tex.ps.gz
epue52de-01.pgm:
	cat epue52de.ps | gs -q -sDEVICE=pgm -sOutputFile=epue52de-%02d.pgm -
epue52de/epue52de.html:	epue52de.dvi
	rm -fR epue52de;latex2html epue52de.tex
xview:	epue52de.dvi
	xdvi -s 8 epue52de &
tview:	epue52de.tty
	browse epue52de.tty 
gview:	epue52de.ps
	ghostview  epue52de.ps &
print:	epue52de.ps
	lpr -s -h epue52de.ps 
sprint:	epue52de.001
	for F in epue52de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	epue52de.faxsnd
	faxsndjob view epue52de &
fsend:	epue52de.faxsnd
	faxsndjob jobs epue52de
viewgif:	epue52de.gif
	xv epue52de.gif &
viewpbm:	epue52de.pbm
	xv epue52de-??.pbm &
vieweps:	epue52de.eps
	ghostview epue52de.eps &	
clean:	epue52de.ps
	rm -f  epue52de-*.tex epue52de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} epue52de-??.* epue52de_tex.* epue52de*~
tgz:	clean
	set +f;LSFILES=`cat epue52de.ls???`;FILES=`ls epue52de.* $$LSFILES | sort -u`;tar czvf epue52de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
