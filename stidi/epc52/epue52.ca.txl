<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Art 52 CPE:  Interpretació i Revisió.

#descr: Els limits del que es patentable que van ser establerts a la Convenció
por il Patent Europeu (CPE) de 1973, han estat ignorats al llarg dels
anys. Els principals tribunals de patents han interpretat l'article 52
d'una manera que pràcticament el fan ja inutil. Nombrosos professors
de dret han intentat demostrar perque no s'haurià de permetre això.
L'OEP ha proposat de revisar l'article 52 per posar-lo d'acord amb la
seva línia de patentibilitat il·limitada. Hom podria fer no obstant el
contrari, regular la patentabilitat segons les línies originals de
l'article 52, però per limitar les possibilitats d'abús. Aquest
document explora el que ja ha passat i que podem fer.

#otv: European Patent Convention

#epc97text: Text of EPO edition

#5ee: Art 52 EPC: Patentable Inventions

#WtW: full text from the EPO web server

#Tao: The contracting states are responsible for shortcomings of the EPO.

#rep: A Technical Board of Appeal of the European Patent Office (EPO)
rejects a patent application which is directed to a program for
computers.  In 1984, the EPO's examiners had rejected the patents
based on the original Examination Guidelines of 1978, saying that the
claims referred to a %(q:program for computers).  The appellant argued
on the basis of newer Guidelines and caselaw that his claims are
directed to technical effects and not a program as such.  The Board of
Appeal rejects the appeal by arguing indirectly that the use of
general-purpose computer hardware does not confer technicity on an
abstract method:  %(q:Abstracting a document, storing the abstract,
and retrieving it in response to a query falls as such within the
category of schemes, rules and methods for performing mental acts and
constitutes therefore non-patentable subject-matter under Article 52
EPC) and %(q:The mere setting out of the sequence of steps necessary
to perform an activity, excluded as such from patentability under
Article 52 EPC, in terms of functions or functional means to be
realised with the aid of conventional computer hardware elements does
not import any technical considerations and cannot, therefore, lend a
technical character to that activity and thereby overcome the
exclusion from patentability.)

#Ben: Versió actual de l'article 52.

#Doi: La conferència de revisió de l'CPE de 11/2000 proposava insertar
%(q:en tots els camps de la teconològia) al paragraf 1 i esborrar el
paragraf 4.

#PaI: Invencions patentables

#Eev: Les patents europees hauràn de ser concedides als invents [ en tots
els camps de la tecnològia ], sempre i quan aquests siguin nous,
representin un avenç o sig+uin susceptibles de la seva aplicació
industrial.

#Aee: Els següent particulars no poden ser reconeguts com a invencions dins
dels significat del paragraf 1:

#End: descobriments, teories cièntifiques i metodes matemàtics;

#vhh: creacions estètiques;

#Pki: esquemes, normes i mètodes per portar a terme actes mentals, jugar a
jocs o fer negocis, i programes per ordinadors;

#dao: presentacions d'informació.

#Ars: Les directrius del paragraf 2 han d'excloure la patentibilitat dels
temes substancials o activitats referides en aquestes directrius només
en tant que una sol·licitud de patent europea o patent europea tingui
relació amb aquests temes o activitats com a tals.

#VW1: Els metodes per al tractament del cos humà o animal, per a cirugia,
terapia i els metodes diagnòstics practicats al cos humà o animal no
podràn esser considerats com a invencions que siguin susceptibles de
la seva aplicació en la industria en el sentit del paragraf 1. Aquesta
directriu no pot èsser aplicada a productes, en particular substàncies
o compostos, fets per utilitzar-se en els metodes anteriorment
esmentats.

#rpf: The Initial Interpretation and its Erosion

#tdl: Until the late 80s, this was unanimously interpreted as clearly
excluding software patents, as they are usually understood in the
discussion today.  E.g. in 1990 the Technical Board of Appeal of the
EPO %(ep:explains) its refusal of 1984 to allow a document processing
system on the basis of Art 52.2c:

#eWe: The reason given for the refusal was that the contribution to the art
resided solely in a computer program as such within the meaning of
Article 52 EPC, paragraphs 2(c) and Consequently, this subject-matter
was not a patentable invention within the meaning of Article 52(1)
EPC, in whatever form it was claimed.

#nsr: In arriving at this conclusion the Examining Division argued on the
basis that Claims 1 and 2 related to a method for automatically
abstracting and storing an input document in an information storage
and retrieval system and Claims 3-6 to a corresponding method for
retrieving a document from the system. The claims specifically
referred to a dictionary memory, input means, a main memory and a
processor. These hardware elements were classical elements of an
information and retrieval system (...) and objectionable under Article
54(2) EPC as lacking novelty. According to the present description
(...) the method steps were implemented by programming such a
classical system. The claimed combination of steps did not imply an
unusual use of the individual hardware elements involved. The claims
merely defined a collocation of known hardware and new software
concerned with document information to be stored but not with an
unexpected or unconventional way of operating the known hardware. The
differences between the prior art and the subject-matter of the
present application were defined by functions to be realised by a
computer program which was used to implement a particular algorithm,
or mathematical method, for analysing a document. In other words the
steps of the method defined operations which were based on the content
of the information and were independent of the particular hardware
used.

#aul: In other words, a collocation of standard computing hardware with new
computing rules (algorithms), in whatever form it is presented in the
claim, would be excluded from patentability.

#lat: This was also clearly expressed in the %(ep:Examination Guidelines of
the European Patent Office of 1978).

#ooo: However, in 1985 the Guidelines were revised and in particular the
limits of patentability with respect to programs for computers were
blurred.  In two decisions of 1986, the EPO's Technical Board of
Appeal reinterpreted the list of exclusions to mean that only
%(q:non-technical) innovations should be excluded, but refused to
define %(q:technical) -- a concept that was not mentioned in the law. 
From thereon the EPO embarked on a %(ss:slippery slope) by gradually
widening the meaning of what could be understood to be %(q:technical).

#oWt: The EPO's reinterpretation of 1985/1986 and the subsequent loosening
were criticised by law scholars such as %(LST) and have led to a
schism of judicial practise, which a new EU directive is supposed to
overcome.

#cte: The decisions at the EPO were understood to have been taken %(q:in
response to pressure from the computer industry and trends emerging in
the US).

#iti: German manual of patent law of 1986, explains the correct
interpretation of Art 52 EPC, as used by the German courts, and
explains that the German courts are thereby resisting %(q:pressure
from the software industry).  Krasser also mentions the revision of
the EPO's examination guidelines in 1985 and explains that they, while
still unclear, seem to be moving into the direction demanded by
%(q:the industry).

#NsP: Nova versió de l'article 52 d'acord amb la proposta del l'OEP de 2000.

#Dng: A la %(dk:conferència diplomatica que va tenir lloc el novembre de
l'any 2000), la OEP va intentar esborrar tot rastre de les definicions
restrictives en termes com ara %(e:invenció), %(e:tecnicitat),
%(e:aplicabilitat industrial), etc de la llei i en el seu lloc obrir
el camí per a la patentatibilitat de totes les  %(pw:solucions
pràctiques i repetibles de problemes). Això ha permés a la OEP de
formular una proposta molt curta:

#Woy: As a result of an uproar of public opinion, politicians from major
countries prevented this planned change of Art 52.  Yet the %(q:Base
Proposal) version was accepted as a new wording for Art 52(1), and Art
52(4) was deleted (whereby the concept of %(q:industrial application)
was further weakened).

#for: Thus the revised version of Art 52 EPC, which is not yet in force,
contains the %(TRIPs) formula %(q:in all fields of technology), but
fails to define the new term %(q:technology), which doesn't exist in
the old EPC.  Thus clause (2) seems relativised by an indeterminate
concept from an international treaty.   It would have been in the
interest of clarity and legal security to concretise this concept,
e.g. by explaining clearly what is to be understood by a %(q:technical
invention) and why algorithms, business methods and rules for
operating known data processing equipment do not belong into this
category.  Instead the legislators opted for introducing indeterminate
concepts and potential contradictions into the law, which are then
likely to be resolved by putting the appositive %(q:as such) from
52(3) in quotes to make it appear mysterious and unclear, so as to
allow the patent judiciary to rely on its own favored interpretation
of %(q:fields of technology) or even to point to alleged WTO
constraints, thus giving up the clarity and integrity of national law
in favor of arbitrary decisionmaking by the international patent
lawyer community.

#qos: Art 52(4) about surgery on the human body was %(q:only) reworded and
moved to Art 53.  This however implies that surgery methods are no
longer considered to be non-inventions, non-technical or
non-industrial.  In this way, the Diplomatic Conference further
weakened the TRIPs concepts on which it decided to rely for limiting
patentability.

#Lgn: The Amended Directive and the EPC

#Dji: La regulació legal actual sobre els límits de la patentatibilitat és
clara i inequívoca. Hi ha, no obstant això, tribunals que consideren
aquesta norma inadequada i l'han substituït per una regulació diferent
en anticipació d'un canvi de la llei.

#Eee: En réalité, les règles nationales et conventionnelles sont claires: 
elles posent sans équivoque un principe de non-brevetabilité du
logiciel.  Le jeu qui se joue aujourd'hui consiste à contourner d'une
manière ou d'une autre celles-ci, par exemple en imaginant de
considérer, comme on l'a vu, l'ensemble constitué par le matériel et
le logiciel comme une machine virtuelle susceptible (demain ...)
d'être breveteée.  À ce compte-là, on peut parler brevets.  Les
brevets susceptibles d'être ainsi obtenus, par ce canal ou un autre,
n'ont, toutefois, que la valeur qu'on leur prête - mais il ne faut pas
écarter l'hypothèse selon laquelle on finirait par une sorte de
consensus à ne pas vraiment la discuter.  De fait, l'efficacité de ce
countournement des règles légales sera largement fonction du fait
qu'un tel consensus se dégagera pour accepter --- contre les règles
positives --- que ce nouveau jeu se joue ou non.  La question ne se
situe plus sur le terrain juridique %(e:stricto sensu).

#Ncj: Després d'un intens debat públic resulta que la norma legal actual és
l'adequada i que la jurisprudència recent de l'OEP contradiu tan la
llei com l'interés públic. Cal cridar els tribunals a corregir la seva
pràctica actual i aplicar la llei.

#sfo: The European Parliament has passed an %(ad:amended directive) which
reconfirms the system of Art 52 EPC and makes it more explicit.  Frits
Bolkestein and some people in the Council do not like this
clarification and propose to opt instead for a revision of the EPC or
some other kind of inter-governmental agreement.  The UK Patent Office
has %(uk:proposed) to rewrite Art 52(3) in a way that allows anything
deemed %(q:technical) to be patented.  On the other hand it would also
be possible to concretise Art 52 EPC itself further in the spirit of
the amended directive.  Putting positive definitions of %(q:technical
field), %(q:technology), %(q:industry) etc, as found in the amended
directive, into Art 52ff EPC or its national versions could become a
way of implementing the directive.

#Wer: Proposem opcionalment d'esborrar l'apartat 3 de l'article 52 perquè és
purament explicatiu i no agrega cap significat a l'article. El seu
lloc és al reglament d'examen de sol·licituds de patents. Suprimir-lo
de la llei seria una manera convenient de dir als tribunals que tornin
a la interpretació correcta de la llei, que era la predominant durant
els anys 70/80.

#del523: Proposes to delete Art 52(3) EPC.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/epue52.el ;
# mailto: mlhtimport@ffii.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: epue52 ;
# txtlang: xx ;
# End: ;

