<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Why can't I patent my movie (as such)?

#descr: A comment on software patents, actually.

#tne: If you have just made a truly innovative movie, and you go to see a
patent attorney about taking out a patent for your movie, you will be
laughed right out of his office.

#etW: Film and other aesthetic creations are protected by copyright, not
patents, he will patiently explain to you, while he chuckles inwardly
at the silliness of people who seem to believe that %(q:intellectual
property) is some unified big fuzzy blob that encompasses everything,
and don't realize that there are completely separate pieces of
legislation that regulate copyrights, patents, trademarks, designs,
and trade secrets, respectively, and that they would often be in
conflict with each other if they overlapped, which means they rarely
do.

#onh: Begone now, foolish film person, for I have important inventors to
meet with to discuss how we can prevent other people from using their
ideas, he will conclude, as he starts raising himself from his chair.

#WmW: But you have traveled a long way for this meeting, and you know that
you will be charged for a full hour anyway, so you demand a more
detailed explanation as to exactly what it is in the patent law that
makes it so unthinkable to get a patent for your movie.

#iih: It's a great film, you insist, it's new, it hasn't been shown in
public yet, it's truly innovative and it will revolutionize the
entertainment industry as we know it! Surely that's what patents are
all about, isn't it?

#Wtf: The patent attorney, who has not seen your film and is therefore
unable to dispute any of your claims on factual grounds, resigns
himself to delivering a legal lecture.

#EWr: What is patentable or not in Europe is governed by the European Patent
Convention EPC, he starts, as he brings up the PowerPoint presentation
that he has used in introductory classes so many times before.

#eWo: The convention quite deliberately restricts the subject matter that
can be patented to certain specific areas. For example, pure
mathematics can't be patented, because what you would really be doing
then would be to try to restrict what ideas people are allowed to have
in their heads. This is not only notoriously tricky to enforce from a
practical point of view, but more importantly, it would be
diametrically opposed to the fundamental purpose of the patent
legislation, which is to promote the flow of ideas and information,
not to restrict it.

#raW: So mathematicians are excluded from being rewarded by the patent
system, not because they are evil people --- in fact, most of them
probably aren't --- but because the lawmakers didn't believe that
progress in the field of mathematics would benefit from having
patentability extended into that area.

#slh: For this reason, there are certain basic criteria that an idea has to
fulfill before it is even considered to be an %(q:invention), in the
special sense that the word is used within patent law: it has to be
%(q:technical), and it has to be susceptible of industrial
application, among other things. Ideas that do not fulfill these
criteria --- a storyline for a film, a set of rules for playing a
game, a foolproof way to ensure world peace --- may still be
spectacularly good and totally new ideas, but they are not
%(q:inventions) in the narrow sense of patent legislation, and thus
they can never receive patent protection.

#oWt: In addition to the general criteria for deciding if an idea falls
outside the patentable domain, there is also Article 52 of the EPC,
which enumerates certain things that are not inventions, and therefore
not patentable. Paragraph (1) to (3) of Article 52 read:

#lbh: European patents shall be granted for any inventions which are
susceptible of industrial application, which are new and which involve
an inventive step.

#idn: The following in particular shall not be regarded as inventions within
the meaning of paragraph 1:

#ioc: discoveries, scientific theories and mathematical methods

#sct: aesthetic creations;

#nWp: schemes, rules and methods for performing mental acts, playing games
or doing business, and programs for computers;

#eWm: presentations of information.

#eia: The provisions of paragraph 2 shall exclude patentability of the
subject-matter or activities referred to in that provision only to the
extent to which a European patent application or European patent
relates to such subject-matter or activities as such.

#hWW: See there, your patent attorney says with a slightly patronizing
smile. Right there in black and white: a film is an aesthetic creation
which is not technical in nature, it is not susceptible of industrial
application, and even if it were, it would still be listed as one of
the things that are explicitly not patentable. Sorry, sir, but no.

#lyf: You are about to leave in disappointment, when you suddenly hit upon a
bright idea to get a patent for your film after all.

#aWf: Okay, so maybe that's true, but suppose we do it like this instead.
Instead of patenting the movie as such, I would like to patent the
idea of a film projector showing my movie. The movie is quite new and
fulfills the criteria for inventive step quite well, I'm sure, and the
mechanical projector will fulfill all the technical criteria in its
own right, so the film and the projector showing it would have what it
takes if they are added together.

#orn: Yes, that's right! A projector showing my film is definitely a
technical device using the controllable forces of nature to achieve
something that is definitely new, namely, the showing of my new film!

#atl: No, no, no, says the patent lawyer says an shakes his suddenly very
weary head. That doesn't cut it at all --- you can only get a patent
on things that are new, so when you remove the old projector and look
at what's new, it is only unpatentable things left, no matter how new
they are.

#sot: If I promise to only sell copies of the film together with a
projector?

#Nol: No.

#Wee: If I glue the film to the projector, so that it can't be changed?

#Nol2: No.

#Wie: If I modify the...

#Nol3: No.

#fIl: If I...

#egu: No. I'm really very sorry, but there is really nothing we can do for
you here. A film as such is not patentable.

#twW: You reluctantly accept his answer, and walk home as a broken man.

#rdm: When you wake up the next morning, you find to your great surprise
that you have been magically transformed into a software developer.
After giving it due consideration, you decide that you do, after all,
prefer what has happened to the cockroach alternative, and proceed to
set up a meeting with the same patent attorney to discuss a piece of
software that you have written.

#uWW: Come in, come in! the patent attorney shines at you as he greets you
at the door, you are indeed a welcome sight, for I have spent all
morning talking to bimbo brained film people who can't tell the
difference between patents and copyright, so I was in a foul mood
before your blessed arrival. What great inventions have you got for me
today, pray Sir?

#icd: You explain, a little warily at first, because the attorney is an
imposing figure and you wouldn't want him to call you
%(q:bimbo-brained) if you have misunderstood something, that you would
like to have a patent on a computer program.

#lWr: No problem at all, the attorney says as the reaches for the
application forms to the European Patent Office.

#nWt: The EPO has already granted over 30,000 software patents since praxis
changed in the nineties, and here comes another one! Now, what is it
that you have invented?

#Wjt: But before you start describing your software, which, to be perfectly
honest, you yourself wouldn't really consider to be all that
spectacularly innovative, but which your friends have told you will
probably be patentable anyway, you feel that you want to be a little
bit more certain about the legal basis for software patents.

#enc: Because you have recently been in frequent informal consultations with
you alter ego in the movie business, you direct the attorney's
attention to Article 52 of the EPC again, and point out that the list
of things that appear to be explicitly excluded from patentability
includes not only aesthetic creations like movies, but also computer
programs. If aesthetic creations like films can't be patented because
they are explicitly listed in Article 52, oughtn't the same apply for
the other things that are also listed there, such as computer
programs?

#fah: No, no, no, the attorney answers with a reassuring smile, this is a
perfect example of why you need a patent lawyer to explain things to
you, rather than just rely on what you might read. You have to read
the entire Article 52 to understand it properly, see? If you go on to
read the rest, you'll find that in paragraph 3 it says that the ban on
patents for these things only applies to the respective thing %(q:as
such). As long as they're not %(q:as such), we can patent them as much
as we want to!

#dWo: If you come to us with your software as such, I'm afraid we can't help
you, that's true. But as long as it's not %(q:as such), we can! This
means that you can only get a patent if the software is part of a
technical system like, well... perhaps --- you guessed it! --- a
computer system, for example. Voila! A computer program as such is not
patentable, no sir, not as such. No way, no.

#rpm: But a computer program that runs on a computer is! And the best part
of it, do you know what that is? Most computer programs actually do!

#uhl: You hear the clock on the church tower outside the window strike noon,
and you can feel how the effects of the magic spell are wearing off,
and how your normal movie-making persona is coming back.

#pe2: Would the same apply if I wanted to patent something else that is
mentioned in Article 52(2), like, say, a film?

#mtt: No, a film as such is not patentable, so that wouldn't work at all.

#cei: But a mechanical movie projector that was showing my film?

#njh: If the movie projector in itself was innovative you could get a patent
on the projector, but that patent wouldn't cover the film anyway. The
film as such is not patentable.

#tWf: But if I patent the film and the movie projector together, then it
isn't a patent for the film as such.

#wjt: I understand that you are referring to the %(q:as such) phrase in
paragraph 3, but you can't read it like that when you are talking
about aesthetic creations. What it says then, is simply that a proper
invention shall not be disqualified just because it also contains
things listed in paragraph 2, as long as the invention is acceptable
otherwise. An innovative movie projector can very well be patented
even though it requires an aesthetic creation in the form of a movie
to be of any use to anybody. But the patent doesn't cover the movie as
such. Look, think of it like this: you can't patent gray plastic boxes
as such, but that doesn't mean that an invention is disqualified
because it comes in a gray plastic box. That's all that that paragraph
means in this context, really!

#oen: So if I create something very new by running a very new film in a very
old projector, I can't get a patent on it?

#Wja: No, because all the very new things are in the film, and the film as
such is not patentable even if it is new.

#osc: Not as such?

#jWu: No, not as such.

#ooW: But if I create something very new by running a very new computer
program on a very old computer, I can get a patent on it?

#jou: Yes, but not as such.

#Wga: But I can get a patent?

#Yes: Yes.

#Wpa: On the program?

#Yes2: Yes.

#ssc: As such?

#jWu2: No, not as such.

#soh: But how is new software on old hardware different from new software as
such, except for the parts that are old?

#tWa: It still isn't software as such.

#Wif: But with films, if...

#emu: Anyway, that'll have to be all for today! he says when you are just
about to ask him again to please explain when %(q:as such) means
%(q:as such) and when it doesn't, as such.

#enn: It's been real nice talking to you but now you really have to excuse
me, I have a plane to catch, your patent attorney continues.   I'm
popping over to Munich to have a chat with the boys at the European
Patent Office (EPO), over dinner and drinks, of course. All paid for
by software patents --- did you know that the EPO is funded directly
by the patent fees, so the more patents we grant, the more money for
everyone in the business! We're going to rewrite the law so that there
is no question that we can patent whatever we bloody want to in the
software arena in the future --- and elsewhere too, for that matter.
About time too, say I, there have been far too many of them noisy
impertinent troublemakers running around shouting about Article 52
here lately. Damned stupid Article, if you ask me. Why would you want
to limit patents, isn't a patent lawyer allowed to eat any more?

#rtW: And those Members of Parliament, who had the nerve to try and come and
think they could change things, when we had already decided! Some
people, well, I tell you...

#Whe: Anyway, ciao, ciao, and tudeloo! he says as he picks up his briefcase
and heads for the door.

#scb: But to change the laws, doesn't that mandate actually belong to the
elected politicians in parliament? you ask yourself, but evidently do
it out loud.

#WWW: He is already halfway out the doorway, but he stops and looks at you
with the kind of smile that you normally reserve for very young
children who haven't really understood anything.

#aeo: Of course it does, they and they alone have a mandate from the
citizens to exercise the power to make new laws. But the way we see
it, they only have that power %(q:as such).

#WWq: The real decisions we prefer to make ourselves, quite frankly.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/epue52.el ;
# mailto: phm@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: epue52film ;
# txtlang: xx ;
# End: ;

