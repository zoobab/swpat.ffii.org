<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Moses, die Zehn Patentierungsverbote und das %(q:Stehlen mit einem
weiteren ethischen Effekt)

#descr: Programme für Datenverarbeitungsanlagen sind heute in Europa
unpatentierbar und patentierbar zugleich.  Wie gelang es den
Technischen Beschwerdekammern des Europäischen Patentamtes im Laufe
der Zeit, das unpatentierbare zu patentieren?  Bei komplizierten
Themen bietet oft der satirische Vergleich den schnellsten Zugang zum
gründlichen Verständnis.

#GnW: Grundlegende Gesetze der menschlichen Gesellschaft ähneln in ihrer
Form oft den Zehn Geboten:  Du sollst nicht ...  Weitere Beispiele
sind:  die buddhistischen Askeseregeln, die Grundrechtskataloge in den
Verfassungen moderner Staaten, und ... die Liste der nicht
patentierbaren Gegenstände in %(ar:Artikel 52%(pe:2) des Europäischen
Patentübereinkommens) (EPÜ).  Keiner dieser Kataloge erhebt Anspruch
auf Vollständigkeit.  Um die Regeln zu befolgen, genügt es nicht, sie
wörtlich an die Verbote zu halten.  Wörtliche Auslegung kann zu
Widersprüchen oder Pharisäertum führen.  Um den Pfad der Tugend zu
beschreiten, bedarf es einer Theorie des %(e:Rechtschaffenen
Handelns).  Im Falle des EPÜ ist dies die Lehre der %(e:Technischen
Erfindung).  Diese Lehre ist nicht unbedingt für die Ewigkeit
geschaffen.  Während jedoch die Paradigmen wechseln und alte
Testamente durch neue ersetzt werden, sollten die grundlegenden
Verbote überdauern, und es ist Aufgabe der Priesterschaft, sie durch
eine passende systematische Lehre auszulegen und zu unterstützen.  Wir
wollen hier einmal vergleichend betrachten, wie dies den Hohen
Priestern des Europäischen Patentwesens im Vergleich zu den
Nachfolgern des Moses gelungen ist.

#Tim: Die Biblische Lehre

#Tsa: Die Zehn Ausschlüsse und die Technische Beschwerdekammer

#Ten: Die Zehn Gebote und der Heilige Rat

#cgt: Mathematische Methoden, Pläne und Regeln für gedankliche und
geschäftliche Tätigkeit, Programme für Datenverarbeitungsanlagen,
Anordnungen von Informationen u.a. sind nicht patentierbar.

#lsl: Götzendienst, Betrug, Ehebruch, Diebstahl, Mord u.a. verdienen keinen
Segen.

#i1l: Erfindung = technische Lehre = Lehre zum planmäßigen Handeln unter
Einsatz beherrschbarer Naturkräfte zur Herbeiführung eines kausal
übersehbaren Erfolges der ohne zwischengeschaltete menschlichte
Verstandestätigkeit unmittelbare Folge beherrschbarer Naturkräfte ist

#mWo: Wohltat = rechtschaffene Tat = von einer Ehrfurcht vor dem Leben
getragenes freiwilliges planmäßiges Handeln nach Regeln, deren
allgemeine Befolgung dem Gemeinwohl dienen würde (vgl Kant und
Schweitzer)

#ptn: Patent

#bes: Segen

#thW: Du sollst nicht deinen Nachbarn bei Schaffung, Veröffentlichung und
Vertrieb seines selbst geschaffenen urheberrechtlichen Eigentums zu
behindern trachten.

#tWW: Du sollst nicht begehren deines Nachbarn Haus, Weib und was sein ist.

#Isd: Da Rechenregeln nicht patentierbar sind, könnte jemand versuchen, auch
die Patentierbarkeit technischer Erfindungen (z.B. chemischer
Verfahren) in Zweifel zu ziehen, indem er zeigt, dass die Erfindungen
durch Rechenregeln gesteuert oder beschrieben werden können.  Solche
denkfaulen formalistischen Auslegungen sind zu vermeiden.  Es ist
vielmehr genau zu prüfen, wo die zu beurteilende Leistung wirklich
liegt.  Während eine Rechenregel %(s:als solche) niemals patentierbar
sein kann, verliert eine technische Erfindung (z.B. chemisches
Verfahren) nicht dadurch ihren technischen Charakter, dass sie
programmgesteuert abläuft oder auf sonstige Weise mit untechnischen
Elementen verbunden ist.  Umgekehrt wird eine Rechenregel auch nicht
dadurch zu einer technischen Erfindung, dass bei ihrer praktischen
Anwendung technische Mittel zum Einsatz kommen können oder müssen.

#IlW: Da dem Lügen, Stehlen, Töten usw jeder Segen versagt bleiben muss,
könnte man auf die irrige Annahme verfallen, dass wir selbst dann,
wenn es darum geht, das Leben unseres Nachbarn gegen einen drohenden
Angriff zu verteidigen, dem Angreifer gegenüber unbedingt auf Lüge und
Gewalt verzichten müssten.  Solchen formalistischen Auslegungen der
Moralregeln sind aber ihrerseits unmoralisch, da sie auf Denkfaulheit
beruhen.   Wir müssen uns schon die Mühe machen, genau zu schauen,
welche Tat wirklich zur Beurteilung ansteht.  Einer Lüge kann %(e:als
solcher) kein Segen zuteil werden, aber eine mutige Tat der
Nachbarschaftshilfe kann auch dann für rechtschaffen und
segnungswürdig befunden werden, wenn zur Überlistung des Angreifers
auf Lügen und sonstige Mittel zurückgegriffen wurde, die für sich
genommen nie als rechtschaffen gelten können.  Umgekehrt wird eine
Lüge nicht dadurch zu einer rechtschaffenen Handlung, dass ihr
praktischer Einsatz möglicherweise oder zwangsläufig mit
rechtschaffenen Taten wie z.B. der Nachbarschaftshilfe verbunden ist.

#Wop: Neuere Exegetische Entwicklungen

#HWe: Haftungsausschluss:  Dieser Abschnitt dient nicht der
Verächtlichmachung eines religiösen Bekenntnisses.  Die darin
dargelegten theologischen Lehren stammen weder von der
Römisch-Katholischen / Societas Jesu noch von irgendeinem anderen
christlichen, buddhistischen, islamischen oder sonstigen religösen
Bekenntnis.

#IoW: Ich will ein Patent für diese Programmierlösung, denn es handelt sich
dabei nicht um ein Programm als solches.

#Ist: Ich bitte um einen Segen für diesen Bankraub, denn es handelt sich
dabei nicht um einen Raub als solchen.

#TrW: Das Patent wird erteilt, denn dem Programm kann eine weitere
technische Wirkung nicht abgesprochen werden.

#Tbe: Der Segen wird erteilt, denn dem Raub kann eine weitere rechtschaffene
Wirkung nicht abgesprochen werden.

#scW: [Unsere Untersuchung] kann zur Identifizierung derjenigen Programme
für Datenverarbeitungsanlagen führen, die, da sie nicht als Programme
für Datenverarbeitungsanlagen als solche angesehen werden, für die
Patentierung offen stehen.

#eca: Our investigation may result in the identification of those bank
robberies which, as a result of not being considered bank robberies as
such, may receive a blessing.

#Wfe: In der Frage, welchen computerimplementierten Erfindungen
%(q:Technizität) zugesprochen werden kann, lässt sich aus dem kürzlich
verhandelten Fall %(q:Controlling pension benefits system) schließen,
dass alle Programme, die auf einem Computer ablaufen, per Definition
als technisch anzusehen sind (da es sich bei dem Computer um eine
Maschine handelt). Sie überwinden somit die erste Hürde auf dem Weg
zur Patentierbarkeit.

#ekW: In der Frage, welchen eigentumsbezogenen Wohltaten %(q:Ethizität)
zugesprochen werden kann, lässt sich aus dem kürzlich verhandelten
Fall %(PBS) schließen, dass alle Banküberfälle, die dem Umverteilung
von Eigentum an die Bedürftigen dienen, per Definition als ethisch
anzusehen sind (da es sich bei der Verteilung von Eigentum an die
Bedürftigen um eine ethisch wertvolle Tat handelt).  Sie überwinden
somit die erste Hürde auf dem Weg zur Qualifikation als
segnungswürdige rechtschaffene Wohltat.

#Inn: Damit das Programm patentiert werden kann, muss über die normale
Wechselwirkung mit dem Rechner hinaus eine weitere technische Wirkung
vorhanden sein.  Darunter ist z.B. eine Erhöhung der Effizienz beim
Gebrauch des Rechners zu verstehen.

#IeW: Damit dem Diebstahl der Segen zugesprochen werden kann, muss über die
normale Bereicherung des Diebes hinaus eine weitere rechtschaffene
Wirkung feststellbar sein.  Darunter verstehen wir z.B. eine
Verbesserung der Fähigkeit des Diebes, weitere rechtschaffene
Handlungen vorzunehmen.

#BAW: Aber was ist technisch?  Die Technische Beschwerdekammer hat eine
vorbildliche Definiton geschaffen: technisch ist alles, was eine
technische Lösung eines technischen Problems darstellt.

#Bro: Aber was ist rechtschaffen?  Der Rechtschaffene Rat bietet eine klare
Definition an:  Rechtschaffen ist alles, was mit rechtschaffenen
Mitteln einen rechtschaffenen Zweck verfolgt.

#Ihm: In seiner letzten Entscheidung lockert das Episkopat der Deutschen
Kirche den bewährten Unmittelbarkeitsgrundsatz

#IWo: Betrifft die Tat einen Zwischenschritt in einer Handlungskette, die
mit dem Reichwerden rechtschaffener Personen endet, so kann sie von
der Segnung nicht deshalb ausgeschlossen werden, weil sie auf den
unmittelbaren Einsatz moralisch unbedenklicher Mittel verzichtet und
stattdessen die gerechte Sache durch andere auf rechtschaffenen
Überlegungen beruhende Tätigkeiten voranzubringen sucht.

#Tao: Die Praxis des EPA beweist, dass nach Europäischem Recht
Computerprogramme im allgemeinen patentierbar sind.  Nur Programme für
Datenverarbeitungsanlagen %(q:als solche) sind nicht patentierbar.

#Tel: Die Praxis des Heiligen Rates beweist, dass nach dem Mosaischen Gesetz
Diebstahl im allgemeinen als gottgefällige Tat angesehen wird.  Nur
Diebstahl %(q:als solcher) ist eine Sünde.

#Ikr: In einem %(be:juristischen Fachartikel) argumentieren zwei
Meinungsführer der Patentbewegung folgendermaßen

#WtW: Patentrechtler versuchen noch immer vergeblich, sich zu einigen, was
ein %(q:Programm für Datenverarbeitungslagen als solches) sein soll. 
%(wt:Die einen) verstehen darunter den Quelltext, %(pa:die anderen)
das unmittelbar von der Maschine ausführbare Programm, %(km:wieder
andere) das gedankliche Konzept.  Um dieser Begriffsverwirrung ein
Ende zu bereiten, arbeiten die Experten derzeit auf allen Kanälen
daran, die missverständlichen %(q:Programme für
Datenverarbeitungsanlagen als solche) aus dem Gesetzestext zu
entfernen.  In Erwartung einer baldigen Gesetzesänderung legen die
Ämter bereits heute das Gesetz so eng aus, dass praktisch jedem
Programm Patentschutz zuteil werden kann, sofern es neu und nicht
naheliegend ist.

#Lir: Leiter der Abteilung Datenverarbeitung des DPMA

#dne: die Patentanwälte %(LST)

#Sut: Vorsitzender Richter am Patentsenat des %(bg:BGH)

#Wds: Wie viele Engel auf einer Nadelspitze tanzen können, ist noch nicht
entschieden.  Noch umstrittener ist die Defininition von %(q:Stehlen
als solches).  Aquaeus verstand darunter die Tatabsicht, Tartullian
die ausgeführte Tat, Raaner den abstrakten Tatplan.  Dem Begriff ist
keine klare Bedeutung abzugewinnen.  Deshalb beten Tausende von
Geistlichen zum Herrn, er möge dieses verfehlte %(q:Du sollst nicht
Stehlen Als Solches) aus dem Gebotekatalog entfernen.  In Erwartung
der Erhörung dieser Gebete legen die Konzilien bereits heute den
Dekalog so eng aus, dass praktisch jedem Bankraub eine Segnung zuteil
werden kann, sofern er freiwillig unternommen und sorgfältig
ausgeführt wurde.

#sWe: In einem %(ka:Kurzgutachten für die Bundesregierung und weiteren
Texten) argumentiert %(AHH) etwa folgendermaßen

#Ptf: Die Prüfungsbehörde kann grundsätzlich nicht beurteilen, ob ein zur
Beurteilung anstehender Patentanspruch sich auf ein %(q:Programm als
solches) richtet.  Viele Erfindungen sind ambivalent: sie können
sowohl durch Hardware als auch durch Software verwirklicht werden. 
Daher kann jedes Rechenprogramm Patentschutz erlangen, solange der
Anmelder nicht ausdrücklich ein Stück Programmtext beansprucht.   Die
Frage, ob eine Anmeldung sich auf ein %(q:Programm als solches)
richtet, ist offenbar auf einen gesetzgeberischen Missgriff
zurückzuführen.  Um Missverständnisse zu vermeiden, sollten wir
künftig nur noch von %(e:rechner-implementierten Erfindungen) oder
%(e:rechner-implementierbaren Erfindungen) sprechen.

#TtW: Das Konzil kann grundsätzlich nicht erkennen, ob ein Segnungsantrag
sich auf einen %(q:Diebstahl als solchen) richtet.  Viele Wohltaten
sind ambivalent: ob bei ihrer Ausführung Eigentumsrechte Dritter
berührt werden, hängt im wesentlichen vom situationellen Kontext ab. 
Daher kann jedem Diebstahl ein Segen zuteil werden, solange der
Antragsteller nicht im Rahmen der Anmeldung einen Diebstahl begeht
(z.B. indem er dem Konzil einen Füllfederhalter entwendet um damit das
Formular auszufüllen).  Die Frage, ob ein Segnungsantrag sich auf
einen %(q:Diebstahl als solchen) richtet, ist offenbar auf einen
Irrtum des Herrn oder seines Dieners Moses zurückzuführen.  Um
Missverständnisse zu vermeiden, sollten wir stattdessen von
%(e:eigentumsverändernden Wohltaten) oder %(e:potentiell
eigentumsverändernden Wohltaten) sprechen.

#ToW: Die Abtei kann grundsätzlich nicht beurteilen, ob ein Antrag auf
Genehmigung von Mönchsexerzitien sich auf eine %(q:geschlechtliche
Tätigkeit als solche) bezieht oder nicht.  Viele geistliche Übungen
sind ambivalent:  sie können mit oder ohne Einsatz geschlechtlicher
Funktionen ausgeführt werden.  Daher kann praktisch jede Sexualpraktik
eine Genehmigung erhalten, sofern die Antragsteller ihren Antrag nicht
in Form eindeutiger bildlicher Darstellungen präsentieren.  Die
gesamte Frage, ob eine geistliche Übung sich auf eine
%(q:geschlechtliche Tätigkeit als solche) richtet oder nicht, kann nur
auf einen regulatorischen Fehlgriff unseres Ordensgründers
zurückgeführt werden.  Um Missverständnisse zu vermeiden sollten wir
künftig nur noch von %(q:körper-ausgeführten geistlichen Übungen) oder
%(q:körper-ausführbaren geistlichen Übungen) sprechen.

#Tef: Das einzige ersichtliche Motiv für die Unterscheidung zwischen
%(q:technischer Wirkung) und %(q:weiterer technischer Wirkung) in der
Entscheidung %(c:IBM Programmprodukt) lag in der missverständlichen
Nennung von %(q:Programmen für Datenverarbeitungsanlagen) in %(ar:Art
52 EPÜ) begründet.  Wenn erwartungsgemäß dieses Vorschrift von der
Diplomatischen Konferenz gestrichen wird, dann entfällt die Grundlage
dieser Unterscheidung.

#Tpd: Der Heilige Rat erklärt, warum Mönche und Nonnen nunmehr auf die
bislang für %(e:exercitia sexualia) erforderlichen kryptischen
Rechtfertigungsformeln verzichten können

#TWW: Das einzige ersichtliche Motiv für die %(ag:Unterscheidung zwischen
%(q:klerischer Wirkung) und %(q:weiterer klerischer Wirkung)) in der
Entscheidung %(SJ) lag in der missverständlichen Erwähnung von
%(q:geschlechtliche Tätigkeit) in Art 52 der Ordensregel begründet. 
Wenn dieses Verbot erwartungsgemäß von der nächsten Bischofskonferenz
gestrichen wird, dann entfällt die Grundlage dieser Unterscheidung.

#cWs: %s vs %s

#TfW: Das EPA finanziert sich heute durch Gebühren für die Patente, die es
erteilt.

#Tni: Die Kirche finanzierte sich zu Luthers Zeiten durch Ablassbriefe.

#Wju: Obwohl viele Leute Geistiges Eigentum in Form urheberrechtlich
geschützter Programme besitzen, können sie jederzeit mithilfe eines
Patentes auf eine logische Funktionalität enteignet werden,
vorausgesetzt diese logische Funktionalität weist über ihre normale
technische Wirkung (Ablauf auf einem Rechner) hinaus noch eine
%(e:weitere technische Wirkung) auf, wie z.B. eine Beschleunigung oder
Verbesserung bisheriger technischer Vorgänge.

#WWj: Wenn das EPA gemeinsam mit seinen Großkunden Tausenden von
Programmierern und Softwareunternehmen ihr urheberrechtlich
begründetes Geistiges Eigentum entwendet, kann diesem Diebstahl ein
nachträglicher Segen der Europäischen Kommission zuteil werden, sofern
er über die Förderung des Wachstums von Patentportfolios hinaus noch
eine %(e:weitere eigentumsfördernde Wirkung) aufweist, wie z.B. die
Schaffung neuer Märkte für Patentexperten.

#Fea: Weitere Lektüre

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/epue52.el ;
# mailto: phm@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: epue52moses ;
# txtlang: xx ;
# End: ;

