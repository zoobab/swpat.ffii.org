<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#GnW: Les lois fondamentale de la société humaine souvent resemblent aux Dix Commandement: Tu ne doit pas ...  Des autres exemples sont: les règles d'ascese des Bouddhistes, les cataloges des droit de l'homme et ... la listes de non-inventions dans article 52 de la Convention Européenne des Brevets (CEB).  Aucun de ces cataloges ne prétend d'être complèt.  Pour suivre les règles il ne suffit jamais de observes litéralement les interdictions.  Une interprétation litérale peut aboutir a des contradictions et du phariséisme.  Pour suivre une véritable voie de vertu, on a besoin d'une théorie de l'%(e:activité vertueuse).  Dans le cas de la CEB cela est la théorie de l'%(e:invention technique).  Cette théorie n'est pas necessairement de validité éternelle.  Mais pendant que les paradigmes changent et les vieux testaments sont remplacé par les nouveaux testaments, les exclusions fondamentales doivent perdurer, est le clerus doit les soutenir par une théorie systématique adaptée a ce besoin.  Nous voudrions comparer ici comment les haut-prètres du système de brevets européen ont achevé leur mission en comparaison avec les successeurs de Moses.

#Tim: L'Enseignement Biblique

#Wop: Nouveaux Developments Exégétiques

#Fea: Autre Lecture

Tsa: The Ten Exclusions and the Technical Board of Appeal

Ten: The Ten Commandments and the Holy Council

cgt: Mathematical methods, schemes and rules for performing mental acts or doing business, programs for computers, arrangements of information etc are not patentable.

lsl: Idolatry, Deception, Theft, Adultery, Murdering etc do not deserve blessing.

i1l: invention = technical teaching = teaching for planned action using controllable forces of nature to achieve an causally overseeable effect which is without intervention of mental steps the immediate result of controllable natural forces

mWo: merit = ethical deed = voluntary planned action based on a spirit of reverence for life according to rules whose adoption by other people would serve the public interest (cf Kant and Schweitzer)

ptn: patent

bes: blessing

thW: Thou shalt not attempt to obstruct thy neighbor's work of creating, publishing and distributing his own copyrighted programs.

tWW: Thou shalt not desire thy neighbor's wife, house and what is his.

Isd: Given that computer programs and mathematical methods are not patentable, someone could try to infer that technical inventions are not patentable because their implementation could involve a computer program.  This kind of lazy formalistic interpretation of the patentability rules is not acceptable.  The examiner must diregard the claim wording and carefully analyse where the allegedly novel and non-obvious teaching lies, whose patentability is under consideration.  A patent may not be granted for a computation rule %(s:as such), but it may be granted for a technical invention (e.g. new chemical process) whose implementation may involve a computer program.  By the same token, a computation rule does not become a technical invention by the fact that technical means can or must be used during its practical application.

IlW: Given that lying, stealing, killing and all sort of other actions are sinful, someone could argue that even in a situation of defending my neighbor against a gangster, I must not use lies to fool the gangster.  This kind of lazy formalistic approach to ethics is evidently unethical. We must take care to look closely what act is really under consideration.  A blessing may not granted for a lie %(s:as such), but it may be granted for an ethical action of defending one's neighbor, even when this action involved lying (e.g. fooling the aggressor) or some other activity which by itself could never be considered to be ethical.  Inversely, a lie does not become an ethical deed by the fact that its practical application is possibly or necessarily connected to ethical deeds such as helping one's neighbor.

HWe: Disclaimer:  This section is not meant to offend any religious faith.  The theological doctrines developped therein are %(e:not) those of the Roman Catholic Church or Societas Jesu nor of any Christian, Buddhist, Muslim or other confession.

IoW: I want a patent for this programming solution, because it is not a program as such.

Ist: I want a blessing for this appropriation of bank money, because it is not robbery as such.

TrW: The patent is granted, because the program cannot be denied to have a further technical effect.

Tbe: The blessing is given, because this robbery cannot be denied to have a further ethical effect.

scW: [Our investigation] may result in the identification of those programs for computers which, as a result of not being considered programs for computers as such, are open to patentability.

eca: Our investigation may result in the identification of those bank robberies which, as a result of not being considered bank robberies as such, may receive a blessing.

#Wsc: En ce qui concerne la question de savoir quelles inventions mises en oeuvre par ordinateur présentent %(q:un caractère technique), la conclusion à tirer de l'affaire récente Controlling pension benefits system20, est que tous les programmes exécutés sur un ordinateur sont par définition techniques (parce qu'un ordinateur est une machine). Ils répondent donc à cette première condition d'être une %(q:invention).

Inn: In order for the program to be patentable, there needs to be a further technical effect, such as an improvement of efficiency of using the computer.

IeW: In order for the stealing to be blessable, there needs to be a further ethical effect, such as an improvement of the ability of the ethical person to perform ethical acts.

BAW: But what is technical?  The EPO's Technical Board of Appeal has created a clear definition:  anything is technical that is a technical solution to a technical problem.

Bro: But what is ethical?  The Holy Council's Ethical Board of Appeal has a clear definition:  Anything is ethical that is an ethical way of pursuing an ethical cause.

Ihm: In its most recent decision, the Episcopate of the Church of Germany loosens the time-honored immediacy principle

IWo: Betrifft die Tat einen Zwischenschritt in einer Handlungskette, die mit dem Reichwerden rechtschaffener Personen endet, so kann sie von der Segnung nicht deshalb ausgeschlossen werden, weil sie auf den unmittelbaren Einsatz moralisch unbedenklicher Mittel verzichtet und stattdessen die gerechte Sache durch andere auf rechtschaffenen Überlegungen beruhende Tätigkeiten voranzubringen sucht.

Tao: The practise of the EPO proves that under European Law computer programs are patentable.  Only computer programs for computers %(q:as such) are not patentable.

Tel: The practise of the Holy Council proves that under the Ten Commandments stealing is generally considered worthy of blessing.  Only stealing %(q:as such) is sinful.

Ikr: In an influential %(be:law journal article), opinion leaders of the patent movement argue as follows

WtW: Patentrechtler versuchen noch immer vergeblich, sich zu einigen, was ein %(q:Programm für Datenverarbeitungslagen als solches) sein soll.  %(wt:Die einen) verstehen darunter den Quelltext, %(ah:die anderen) das unmittelbar von der Maschine ausführbare Programm, %(km:wieder andere) das gedankliche Konzept.  Um dieser Begriffsverwirrung ein Ende zu bereiten, arbeiten die Experten derzeit auf allen Kanälen daran, die missverständlichen %(q:Programme für Datenverarbeitungsanlagen als solche) aus dem Gesetzestext zu entfernen.  In Erwartung einer baldigen Gesetzesänderung legen die Ämter bereits heute das Gesetz so eng aus, dass praktisch jedem Programm Patentschutz zuteil werden kann, sofern es neu und nicht naheliegend ist.

Lir: head of computing department of German Patent Office

Sut: software specialist and judge at the Federal Supreme Court's Patent Senate

Wds: Wie viele Engel auf einer Nadelspitze tanzen können, ist noch nicht entschieden.  Noch umstrittener ist die Defininition von %(q:Stehlen als solches).  Aquaeus verstand darunter die Tatabsicht, Tartullian die ausgeführte Tat, Raaner den abstrakten Tatplan.  Dem Begriff ist keine klare Bedeutung abzugewinnen.  Deshalb beten Tausende von Geistlichen zum Herrn, er möge dieses verfehlte %(q:Du sollst nicht Stehlen Als Solches) aus dem Gebotekatalog entfernen.  In Erwartung der Erhörung dieser Gebete legen die Konzilien bereits heute den Dekalog so eng aus, dass praktisch jedem Bankraub eine Segnung zuteil werden kann, sofern er freiwillig unternommen und sorgfältig ausgeführt wurde.

sWe: In a %(ka:government-ordered expertise and other texts), %{AHH} argues approximately as follows

Ptf: The examining office has no way to tell, whether a claim under consideration is directed to a %(q:program for computers as such).  Many inventions are ambivalent: they can be implemented both as hardware or as software.  Therefore any computer program can receive patent protection, as long as the applicant does not explicitely include a piece of program text in his claim.  The whole question of whether an application refers to a %(q:computer program as such) or not can only be attributed to an error of the legislator.  To clarify matters, we should only speak about %(e:computer-implemented inventions) or %(e:computer-implementable inventions).

TtW: The Council has no way to judge whether a blessing request under consideration is directed to %(q:stealing as such).  Many merits are ambivalent:  whether or not they involve an infringement of someone else's property is determined by the situational context.  Therefore any theft can receive a blessing, as long as the applicant does not include an act of theft in his application (e.g. write with a pen stolen from the Council).  The whole question of whether a merit refers to %(q:stealing as such) or not can only be attributed to a lapsus on the part of the Lord or his servant Moses.  To clarify matters, we should avoid their terminology and instead speak about %(e:property-relevant merits) or %(e:potentially property-relevant merits).

ToW: The Abbey has no way to judge whether a request under consideration is directed to %(q:sexual practise as such).  Many spiritual exercises are ambivalent: they can be carried out with or without sexual means.  Therefore practically any sexual activity can receive an authorisation, as long as the applicants do no present their request by means of explicit pictorial material.  The whole question of whether a spiritual exercise refers to a %(q:sexual practise as such) or not can only be attributed to a misconception on the part of St. Ignatius.  To clarify matters we should refer only to %(q:body-performed spiritual exercises) or %(q:body-performable spiritual exercises).

Tef: The only apparent reason for distinguishing %(q:technical effect) from %(q:further technical effect) in the %(c:IBM Computer Program Product) decision was because of the confusing presence of %(q:programs for computers) in the list of exclusions under Article %(tp:52:2) EPC.  If, as is to be anticipated, this element is dropped by the Diplomatic Conference, there will no longer be any basis for such a distinction.

Tpd: Last year, the Monasterial Council explained why monks and nuns can now omit the cryptic justification formulas that were needed for %(e:exercitia sexualia) in earlier years

TWW: The only apparent reasoning for %(ag:distinguishing %(q:clerical effect) from %(q:further clerical effect)) in the %{SJ} decision was because of the confusing presence of %(q:sexual activity) in the list of prohibitions under Article 52 of the Monasterial Catechism.  If, as is to be anticipated, this element is dropped by the next Episcopal Conference, there will no longer be any basis for such a distinction.

cWs: %s vs %s

TfW: Today the EPO is financed by fees for the patents which it grants.

Tni: In Luther's times, the Church was financed by letters of redemption.

Wju: While many people own intellectual property in the form of copyrighted programs which they created, they may be expropriated any time by means of a patent on a program functionality, provided that this functionality has, in addition to the technical effect of running on a computer, a %(e:further technical effect), such as making a computer do a better job.

WWj: When the EPO, in collusion with its large corporate customers, nullifies the copyright property of thousands of programmers and small companies, this theft can receive a legislative blessing from the European Commission, if in addition to stimulating the growth of patent portfolios it has a %(e:further property-promoting effect), such as creating new markets for patent experts.

#MosesT: Moses, les Dix Exclusions de la Brevetabilité et le %(q:vol avec un éffet éthique supplémentaire)

#MosesD: Les lois fondamentale de la société humaine souvent resemblent aux Dix Commandements: Tu ne doit pas ...  Des autres exemples sont: les règles d'ascese des Bouddhistes, les cataloges des droit de l'homme et ... la listes de non-inventions dans l'article 52 de la Convention sur le Brevet Européen.  Aucun de ces cataloges ne prétend d'être complèt.  Pour suivre les règles il ne suffit jamais de observes litéralement les interdictions.  Une interprétation litérale peut aboutir a des contradictions et du phariséisme.  Pour suivre une véritable voie de vertu, on a besoin d'une théorie de l'%(e:activité vertueuse).  Dans le cas de la CEB cela est la théorie de l'%(e:invention technique).  Cette théorie n'est pas necessairement de validité éternelle.  Mais pendant que les paradigmes changent et les vieux testaments sont remplacé par les nouveaux testaments, les exclusions fondamentales doivent perdurer, est le clerus doit les soutenir par une théorie systématique adaptée a ce besoin.  Nous voudrions comparer ici comment les haut-prètres du système de brevets européen ont achevé leur mission en comparaison avec les successeurs de Moses.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epue52moses ;
# txtlang: fr ;
# multlin: t ;
# End: ;

