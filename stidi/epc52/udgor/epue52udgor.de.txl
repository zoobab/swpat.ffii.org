<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Skandinavien: auch ohne %(q:als solches) Klausel kann Diebstahl einen
weiteren rechtlichen Effekt haben

#descr: Art 52 of the European Patent Convention (EPC) stipulates that
programs for computers as well as mental rules, mathematical methods,
ways of presenting information etc are not patentable inventions and
may therefore not be claimed as such.  The wording %(q:as such) from
Art 52(3) has however been used to undo all explicit limits on
patentability.  The European Patent Office (EPO) has in 1997 begun to
subdivide computer programs into two groups, %(q:as-such programs) and
%(q:not-as-such programs), and has tried to justify this by historical
claims about how art 52 EPC came about.  This reasoning is at odds
with grammar as well as with history.  One particularly nice set of
evidence comes from Scandinavia:  the Danish and Swedish national
versions of art 52 EPC do not literally render Art 52(3) but rather
incorporate its meaning in the first line of their version of Art
52(2), coming to exactly the same common sense conclusions to which
independent grammatical analyses have come and which are also stated
in the early EPO examination guidelines: that a %(q:program as such)
is %(q:something that constitutes only a program).   It confirms that
Art 52(3) merely exhorts examiners to look carefully where the novel
achievement really lies, e.g. in a programming solution or in a
chemical process which may happen to run under program control.

#Ihn: In referring to the %(mk:historical records of the Munich Conference
for the creation of a european patent examination system of nov 1973),
Hartmut Pilch %(ph:remarks):

#Toa: The %(q:as such) clause has nothing to do with any debate about
programs for computers.

#Iit: I think there are good reasons for this argument also from the fact
that the famous %(q:as such) clause is not even present in Swedish and
Danish law.  The surrounding legal text is identical both in form and
content. EPC 52.3 is simply inserted between the list of exclusions
and the remarks regarding medical treatment and surgery. This can
easily be seen from comparing:

#Ios: In swedish and danish law the whole meaning of EPC 52.3 is contained
in the words %(q:vad som utgör enbart) and %(q:hvad der alene udgør)
(%(q:what only is) or %(q:what alone constitutes)) and they are a part
of the exclusion header:

#Woh: Which translates word by word into %(q:As inventions are never
regarded what alone constitutes mathematical methods ... programs for
computers ...)

#Isp: It's very unlikely that the swedish and danish delegations at the
negotiations in 1973 did consider EPC 52.3 as important as EPO did in
the famous %(et:decision T 1173/97):

#4aa: 4.1 ... The combination of the two provisions (Article 52(2) and (3)
EPC) demonstrates that the legislators did not want to exclude from
patentability all programs for computers. In other words the fact that
only patent applications relating to programs for computers as such
are excluded from patentability means that patentability may be
allowed for patent applications relating to programs for computers
where the latter are not considered to be programs for computers as
such.

#4os: 4.2 In order to establish the scope of the exclusion from
patentability of programs for computers, it is necessary to determine
the exact meaning of the expression %(q:as such). This may result in
the identification of those programs for computers which, as a result
of not being considered programs for computers as such, are open to
patentability.

#Idn: It's funny how they have tried to find out the intentions of the
legislators... but the tragedy is that this EPO interpretation of a
clause that does not exist in Swedish and Danish patent law affects
our national caselaw and inserts extranational legislation through a
back door.

#IaW: Intranational organisations are not allowed to execute power in
matters regarding basic constitutional rights like freedom of speech
and professional and property rights (this is expressed explicitly in
the swedish constitution: SFS 1974:152 10 kap 5 § 2 stycket). Yet it
is happening before our eyes.

#tte: Recent Swedish Patent Court Decision

#tro: In a yet to be documented recent decision, the Swedish Patent Court
interprets the Swedish correspondant of Art 52 EPC as follows
(unauthorised translation):

#hLa: When trying the case, the first consideration is the question of how
the system and the method according to the application is relating to
the the judical invention concept in 1 § first paragraph Swedish
Patent Law (PL) with the specification expressed in paragraph two item
3, according to which an invention is never considered such as solely
consists of %(q:a plan, a rule or method for intellectual activity,
gaming or busisness or a computer program).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/epue52.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: epue52udgor ;
# txtlang: xx ;
# End: ;

