\begin{subdocument}{epue52udgor}{Scandinavia: even without the ``as such'' clause, stealing can have a further legal effect}{http://swpat.ffii.org/analysis/epc52/udgor/index.en.html}{Erik JOSEFSSON\\\url{http://www.sslug.dk}\\\url{}\\english version 2005-03-20 by Hartmut PILCH\footnote{\url{http://www.a2e.de}}}{Art 52 of the European Patent Convention (EPC) stipulates that programs for computers as well as mental rules, mathematical methods, ways of presenting information etc are not patentable inventions and may therefore not be claimed as such.  The wording ``as such'' from Art 52(3) has however been used to undo all explicit limits on patentability.  The European Patent Office (EPO) has in 1997 begun to subdivide computer programs into two groups, ``as-such programs'' and ``not-as-such programs'', and has tried to justify this by historical claims about how art 52 EPC came about.  This reasoning is at odds with grammar as well as with history.  One particularly nice set of evidence comes from Scandinavia:  the Danish and Swedish national versions of art 52 EPC do not literally render Art 52(3) but rather incorporate its meaning in the first line of their version of Art 52(2), coming to exactly the same common sense conclusions to which independent grammatical analyses have come and which are also stated in the early EPO examination guidelines: that a ``program as such'' is ``something that constitutes only a program''.   It confirms that Art 52(3) merely exhorts examiners to look carefully where the novel achievement really lies, e.g. in a programming solution or in a chemical process which may happen to run under program control.}
In referring to the historical records of the Munich Conference for the creation of a european patent examination system of nov 1973\footnote{\url{http://swpat.ffii.org/papers/muckonf73/index.en.html}}, Hartmut Pilch remarks\footnote{\url{http://liberte.aful.org/pipermail/patents/2002-January/002606.html}}:

\begin{quote}
The ``as such'' clause has nothing to do with any debate about programs for computers.
\end{quote}

I think there are good reasons for this argument also from the fact that the famous ``as such'' clause is not even present in Swedish and Danish law.  The surrounding legal text is identical both in form and content. EPC 52.3 is simply inserted between the list of exclusions and the remarks regarding medical treatment and surgery. This can easily be seen from comparing:

\begin{quote}
\url{http://www.european-patent-office.org/legal/epc/e/ar52.html}\\
\url{http://www.prv.se/patent/06_patentlagen_kap01.html}\\
\url{http://www.retsinfo.dk/DELFIN/HTML/A2001/0078129.htm\#K1}
\end{quote}

In swedish and danish law the whole meaning of EPC 52.3 is contained in the words ``vad som utg\"{o}r enbart'' and ``hvad der alene udg\o{}r'' (``what only is'' or ``what alone constitutes'') and they are a part of the exclusion header:

\begin{center}
\begin{tabular}{C{44}C{44}}
se & S\aa{}som uppfinning anses aldrig vad som utg\"{o}r enbart\\
dk & Som opfindelser anses især ikke, hvad der alene udgør\\
\end{tabular}
\end{center}

Which translates word by word into ``As inventions are never regarded what alone constitutes mathematical methods ... programs for computers ...''

It's very unlikely that the swedish and danish delegations at the negotiations in 1973 did consider EPC 52.3 as important as EPO did in the famous decision T 1173/97\footnote{\url{http://swpat.ffii.org/papers/epo-t971173/index.en.html}}:

\begin{quote}
4.1 ... The combination of the two provisions (Article 52(2) and (3) EPC) demonstrates that the legislators did not want to exclude from patentability all programs for computers. In other words the fact that only patent applications relating to programs for computers as such are excluded from patentability means that patentability may be allowed for patent applications relating to programs for computers where the latter are not considered to be programs for computers as such.

4.2 In order to establish the scope of the exclusion from patentability of programs for computers, it is necessary to determine the exact meaning of the expression ``as such''. This may result in the identification of those programs for computers which, as a result of not being considered programs for computers as such, are open to patentability.
\end{quote}

It's funny how they have tried to find out the intentions of the legislators... but the tragedy is that this EPO interpretation of a clause that does not exist in Swedish and Danish patent law affects our national caselaw and inserts extranational legislation through a back door.

Intranational organisations are not allowed to execute power in matters regarding basic constitutional rights like freedom of speech and professional and property rights (this is expressed explicitly in the swedish constitution: SFS 1974:152 10 kap 5 \S{} 2 stycket). Yet it is happening before our eyes.

\begin{itemize}
\item
{\bf {\bf K\"{o}nig 2001: Patentf\"{a}hige Datenverarbeitungsprogramme - ein Widerspruch in sich\footnote{\url{http://swpat.ffii.org/papers/grur-koenig01/index.de.html}}}}

\begin{quote}
Dr. K\"{o}nig, patent attorney from D\"{u}sseldorf, points out inconsistencies in the software patent caselaw of the EPO and BGH, criticises ``circular conclusions'' and argues that the EPO has ``done violence to art 52 EPC''.  Through a ``grammatical interpretation'' of ``programs for computers as such'' he finds that this can refer only to all kinds of computer programs without exception, as far as they are claimed alone.   The EPC of 1973, transcribed into German law in 1978, no longer allows a distinction between technical and untechnical programs.  However it ist still possible to patent program-related combination inventions, which then have to be examined for technicity, novelty, non-obviousness and industrial applicability.  Courts have often shown more imagination in ``helping themselves over the obstacles of art 52''.  There is an elegant indirect way to effectively grant full patent protection for computer programs as such while avoiding the recent incoherences of the EPO and BGH jurisdiction.  As parts of combination inventions, programs for computers may, just as is frequently the case with discoveries and scientific theories, enjoy full ``usage protection'', if their distribution can be construed as contributory infringement of a combination invention.
\end{quote}
\filbreak

\item
{\bf {\bf Recent Swedish Patent Court Decision}}

\begin{quote}
In a yet to be documented recent decision, the Swedish Patent Court interprets the Swedish correspondant of Art 52 EPC as follows (unauthorised translation):

\begin{quote}
{\it When trying the case, the first consideration is the question of how the system and the method according to the application is relating to the the judical invention concept in 1 \S{} first paragraph Swedish Patent Law (PL) with the specification expressed in paragraph two item 3, according to which an invention is never considered such as solely consists of ``a plan, a rule or method for intellectual activity, gaming or busisness or a computer program''.}
\end{quote}
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/epue52.el ;
% mode: latex ;
% End: ;

