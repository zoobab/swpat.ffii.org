\begin{subdocument}{epue52}{Art. 52 EP\"{U}: Auslegung und Novellierung}{http://swpat.ffii.de/stidi/epue52/index.de.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\deutsche Version 2005-03-25 von Hartmut PILCH\footnote{\url{http://www.a2e.de}}}{Die im Europ\"{a}ischen Patent\"{u}bereinkommen (EP\"{U}) 1973 festgelegten Grenzen des Patentwesens sind im Laufe der Jahre verwischt worden.   F\"{u}hrende Patentgerichte haben Art. 52 in einer Weise ausgelegt, die ihn in der Praxis fast bedeutungslos macht.  Zahlreiche Rechtsgelehrte haben gezeigt, warum dies unzul\"{a}ssig ist.  Das Europ\"{a}ische Patentamt (EPA) hatte die Ungereimtheiten in Kauf genommen, weil es fest mit einer \"{A}nderung des Art. 52 rechnete.  Es hat selbst \"{A}nderungsvorschl\"{a}ge vorgelegt, die das Gesetz ganz an die EPA-Praxis der grenzenlosen Patentierbarkeit anpassen.  Man k\"{o}nnte jedoch auch den umgekehrten Weg gehen:  die Patentierbarkeit erneut im Sinne des urspr\"{u}nglichen Art. 52 regeln, und zwar in einer Weise, die weniger M\"{o}glichkeiten des Missbrauchs offen l\"{a}sst.  Diese Dokumentation erkundet, was passiert ist und was f\"{u}r die Zukunft getan werden kann.}
\begin{sect}{ep73}{Derzeitige Version des Art. 52}
Die EP\"{U}-Revisionskonferenz von 2000/11 schlug vor, den Zusatz ``auf allen Gebieten der Technik'' in Abs 1 einzuf\"{u}gen und Abs 4 zu streichen.

\begin{quote}
{\it Artikel 52: Patentf\"{a}hige Erfindungen}

{\it \begin{enumerate}
\item
Europ\"{a}ische Patente werden f\"{u}r Erfindungen [auf allen Gebieten der Technik] erteilt, sofern sie neu sind, auf einer erfinderischen T\"{a}tigkeit beruhen und gewerblich anwendbar sind.

\item
Als Erfindungen im Sinn des Absatzes 1 werden insbesondere nicht angesehen:
\begin{enumerate}
\item
Entdeckungen sowie wissenschaftliche Theorien und mathematische Methoden;

\item
\"{a}sthetische Formsch\"{o}pfungen;

\item
Pl\"{a}ne, Regeln und Verfahren f\"{u}r gedankliche T\"{a}tigkeiten, f\"{u}r Spiele oder f\"{u}r gesch\"{a}ftliche T\"{a}tigkeiten sowie Programme f\"{u}r Datenverarbeitungsanlagen;

\item
die Wiedergabe von Informationen.
\end{enumerate}

\item
Absatz 2 steht der Patentf\"{a}higkeit der in dieser Vorschrift genannten Gegenst\"{a}nde oder T\"{a}tigkeiten nur insoweit entgegen, als sich die europ\"{a}ische Patentanmeldung oder das europ\"{a}ische Patent auf die genannten Gegenst\"{a}nde oder T\"{a}tigkeiten als solche bezieht.

\item
Verfahren zur chirurgischen oder therapeutischen Behandlung des menschlichen oder tierischen K\"{o}rpers und Diagnostizierverfahren, die am menschlichen oder tierischen K\"{o}rper vorgenommen werden, gelten nicht als gewerblich anwendbare Erfindungen im Sinn des Absatzes 1. Dies gilt nicht f\"{u}r Erzeugnisse, insbesondere Stoffe oder Stoffgemische, zur Anwendung in einem der vorstehend genannten Verfahren.
\end{enumerate}}
\end{quote}
\end{sect}

\begin{sect}{ep78}{Die urspr\"{u}ngliche Auslegung und ihre Aufweichung}
Bis Ende der 80er Jahre wurde dies einhellig als klarer Ausschluss von Software-Patenten verstanden, wie sie heute diskutiert werden.  Beispielsweise erkl\"{a}rt\footnote{\url{http://swpat.ffii.de/papiere/epo-t850022/index.en.html}} die technische Beschwerdekammer des EPA dessen Weigerung von 1984, ein Dokumentverarbeitungssystem zuzulassen, auf Grundlage von Art. 52.2c:

\begin{quote}
{\it Als Begr\"{u}ndung der Ablehnung wurde angef\"{u}hrt, dass der Beitrag zum Stand der Technik allein in einem Computerprogramm als solchem im Sinne von Artikel 52 Absatz 2(c) EP\"{U} bestand und dass demzufolge der Antragsgegenstand keine patentierbare Erfindung im Sinne von Artikel 52 Absatz 1 EP\"{U} sei, in welcher Form auch immer er beansprucht werde.}

{\it Zur Urteilsfindung ging die Pr\"{u}fungsabteilung davon aus, dass Anspr\"{u}che 1 und 2 sich auf eine Methode bezogen, ein Dokument von dem System abzurufen.  Die Anspr\"{u}che verwiesen insbesondere auf einen W\"{o}rterbuchspeicher, Eingabevorrichtungen, Hauptspeicher und einen Prozessor.  Diese Rechnerbestandteile waren klassische Elemente eines Informations- und Ablagesystems [...] und gem\"{a}{\ss} Artikel 54(2) EP\"{U} wegen Mangels an Neuigkeit zu beanstanden.  Nach der vorgelegten Beschreibung [...] wurden die Verfahrensschritte durch Programmierung eines solch klassischen Systems implementiert. Die beanspruchte Kombination der Schritte ergab keinen ungew\"{o}hnlichen Gebrauch der einzelnen Rechnerbestandteile.  Die Anspr\"{u}che legen ein Zusammenspiel bekannter Hardware und neuer Software fest, das sich damit befasst, Dokumentinformationen zu speichern, aber nicht mit einer unerwarteten oder ungew\"{o}hnlichen Weise, die bekannte Hardware zu betreiben.  Die Unterschiede zwischen dem Stand de  r Technik und dem Anspruchsgegenstand der vorliegenden Anmeldung wurden durch Funktionen definiert, die durch ein Computerprogramm zu realisieren waren, das dazu verwendet wurde, einen bestimmten Algorithmus oder eine mathematische Methode zur Dokumentenanalyse zu implementieren.  Mit anderen Worten, die Verfahrensschritte definierten Operationen, die auf dem Inhalt der Information beruhten und unabh\"{a}ngig von der jeweiligen Hardware waren.}
\end{quote}

Mit anderen Worten, ein Zusammenspiel gewohnter Rechner-Hardware mit neuen Rechenregeln (Algorithmen), in welcher Form auch immer sie in den Anspr\"{u}chen dargestellt werden, w\"{a}re von der Patentierbarkeit ausgeschlossen.

Dies wurde ebenfalls klar in den Pr\"{u}fungsrichtlinien des Europ\"{a}ischen Patentamts von 1978\footnote{\url{http://swpat.ffii.de/papiere/epo-gl78/index.de.html}} ausgedr\"{u}ckt.

Allerdings wurden 1985 die Pr\"{u}fungsrichtlinien \"{u}berarbeitet, und insbesondere die Grenzen der Patentierbarkeit hinsichtlich Computerprogrammen wurden verwischt.  In zwei Entscheidungen von 1986 erkl\"{a}rte die technische Beschwerdekammer des EPA die Liste der Ausschlussgegenst\"{a}nde dahingehend, dass nur ``nichttechnische'' Neuerungen ausgeschlossen sein sollten, weigerte sich aber, ``technisch'' zu definieren -- ein Begriff, der im Gesetz nicht vorkam.  Von da an begab sich das EPA auf einen Schlitterkurs\footnote{\url{http://swpat.ffii.de/stidi/erfindung/index.de.html}}, indem es den Bereich dessen, was als ``technisch'' verstanden werden k\"{o}nnte, immer weiter ausdehnte.

Die Neuauslegung durch das EPA von 1985/1986 und die nachfolgende Aufweichung wurden von Rechtsgelehrten wie Krasser\footnote{\url{http://swpat.ffii.de/papiere/krasser86/index.de.html}}, Benkard\footnote{\url{http://swpat.ffii.de/papiere/benkard88/index.de.html}} und Vivant\footnote{\url{http://swpat.ffii.de/papiere/lamy98/index.de.html}} kritisiert und haben zu einer Spaltung der Rechtspraxis gef\"{u}hrt, deren \"{U}berwindung von einer neuen EU-Richtlinie erhofft wird.

Siehe auch BPatG 2002-03-26: Suche fehlerhafter Zeichenketten\footnote{\url{http://swpat.ffii.de/papiere/bpatg17-suche02/index.de.html}}, Melullis 2002: Zur Sonderrechtsf\"{a}higkeit von Computerprogrammen\footnote{\url{http://swpat.ffii.de/papiere/melullis02/index.de.html}} und BGH-Entscheidung Betriebssystem 1990 : Entscheidung des 1. Zivilsenats des BGH zur Abgrenzung von Urheber- und Patentrecht: ein Betriebssystem ist kein technisches Programm\footnote{\url{http://swpat.ffii.de/papiere/bgh1-bs90/index.de.html}}

Die Entscheidungen am EPA wurden als ``Antwort auf Druck der Computer-Industrie und der sich anbahnenden Entwicklungen in den USA'' angesehen.

\begin{itemize}
\item
{\bf {\bf Hk-cityu-is-euswpat\footnote{\url{http://www.is.cityu.edu.hk/Research/WorkingPapers/paper/9420.pdf}}}}
\filbreak

\item
{\bf {\bf Bernhardt \& Kra{\ss}er 1986: Lehrbuch des Patentrechts\footnote{\url{http://swpat.ffii.de/papiere/krasser86/index.de.html}}}}

\begin{quote}
Deutsches Handbuch des Patentrechts von 1986, erkl\"{a}rt die korrekte Interpretation von Art. 52 EP\"{U}, wie sie von deutschen Gerichten verwendet wird, und erkl\"{a}rt, dass die deutschen Gerichte damit einem ``Druck von der Software-Industrie'' widerstehen w\"{u}rden.  Krasser erw\"{a}hnt auch die \"{U}berarbeitung der Pr\"{u}fungsrichtlinien des EPA von 1985 und erkl\"{a}rt, dass sie, noch in unklarem Zustand, in die von ``der Industrie'' geforderte Richtung gehen w\"{u}rden.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{eb00}{Neue Version des Art. 52 laut EPA-Basisvorschlag von 2000}
Das EPA m\"{o}chte auf der Diplomatischen Konferenz im November 2000\footnote{\url{http://swpat.ffii.de/termine/2000/epue11/index.de.html}} alle Reste einer Definition der Begriffe \emph{Erfindung}, \emph{Technizit\"{a}t}, \emph{industrielle Anwendbarkeit} usw. aus dem Gesetz tilgen und stattdessen die grenzenlose Patentierbarkeit aller praktischen und wiederholbaren Probleml\"{o}sungen\footnote{\url{http://swpat.ffii.de/papiere/jwip-schar98/index.en.html}} verankern.  Das hat es dem EPA erm\"{o}glicht, seinen Vorschlag sehr kurz zu halten:

\begin{quote}
{\it Patentfähige Erfindungen}

{\it Europ\"{a}ische Patente werden f\"{u}r Erfindungen [auf allen Gebieten der Technik] erteilt, sofern sie neu sind, auf einer erfinderischen T\"{a}tigkeit beruhen und gewerblich anwendbar sind.}
\end{quote}

Der darauf erfolgende Aufschrei in der \"{O}ffentlichkeit brachte Politiker der gro{\ss}en L\"{a}nder dazu, diese vorgesehene \"{A}nderung von Art. 52 zu verhindern.  Allerdings wurde die Version des ``Basisvorschlags'' zur Neuformulierung von Art. 52(1) akzeptiert, und Art. 52(4) wurde gestrichen (wodurch der Begriff der ``gewerblichen Anwendbarkeit'' an Substanz verlor).

Daher enth\"{a}lt die Neufassung von Art. 52 EP\"{U}, die noch nicht in Kraft ist, die TRIPs\footnote{\url{http://swpat.ffii.de/stidi/trips/index.de.html}}-Formel ``auf allen Gebieten der Technik'', l\"{a}sst aber eine Definition des neuen Begriffs ``Technik'' vermissen, der im alten EP\"{U} nicht vorkommt.  Somit scheint Absatz (2) relativiert zu werden durch einen unbestimmten Begriff aus einem internationalen Vertrag.  Es w\"{a}re im Interesse an Klarheit und Rechtssicherheit, diesen Begriff zu konkretisieren, beispielsweise indem klar dargelegt wird, was unter einer ``technischen Erfindung'' zu verstehen ist und warum Algorithmen, Gesch\"{a}ftsmethoden und Regeln zum Betreiben bekannter Datenverarbeitungsanlagen nicht zu dieser Kategorie geh\"{o}ren. Stattdessen entschieden die Gesetzgeber sich daf\"{u}r, unbestimmte Begriffe und denkbare Widerspr\"{u}che in das Gesetz einzuf\"{u}hren, die dann eher dadurch aufgel\"{o}st werden, dass man die Beif\"{u}gung ``als solche'' in 52(3) in Anf\"{u}hrungszeichen setzt, um sie geheimnisvoll und unklar erscheinen zu lass  en und so der Patentgerichtsbarkeit zu erlauben, ihre eigene Lieblingsinterpretation von ``Gebiet der Technik'' anzuwenden oder sogar auf angebliche WTO-Erfordernisse zu verweisen und damit die Klarheit und Stimmigkeit nationalen Rechts zu Gunsten willk\"{u}rlicher Entscheidungen der internationalen Patentanwaltsgemeinschaft aufzugeben.

Art. 52(4) \"{u}ber Chirurgie am menschlichen K\"{o}rper wurde ``nur'' umformuliert und in Art. 53 verschoben.  Dies bedeutet allerdings, dass chirurgische Methoden nicht mehr als Nicht-Erfindungen, nichttechnisch oder nichtgewerblich angesehen werden.  Auf diese Weise schw\"{a}chte die diplomatische Konferenz wiederum die TRIPs-Begrifflichkeiten, auf die sie sich zur Begrenzung der Patentierbarkeit zu st\"{u}tzen entschieden hatte.
\end{sect}

\begin{sect}{eu03}{Die ge\"{a}nderte Richtlinie und Art. 52 EP\"{U}}
Die derzeitige Gesetzesregel \"{u}ber die Grenzen der Patentierbarkeit ist klar und eindeutig.  Es gibt jedoch einige Gerichte, welche diese Regel f\"{u}r unzweckm\"{a}{\ss}ig halten und sie im Vorgriff auf eine angestrebte Gesetzes\"{a}nderung verworfen haben.  Wie Prof. Michel Vivant 1998 schreibt\footnote{\url{http://swpat.ffii.de/papiere/lamy98/index.de.html}}:

\begin{quote}
{\it In Wirklichkeit sind die Gesetzesregeln des \"{U}bereinkommens und der nationalen Gesetze klar:  Sie fordern unmissverst\"{a}ndlich die Nicht-Patentierbarkeit von Software.  Das Spiel, das heute gespielt wird, besteht darin, in einer oder der anderen Weise diese Regeln zu verdrehen, z.B. indem man sich, wie oben beschrieben, die Gesamtheit aus Hardware und Software als eine virtuelle Maschine denkt, die (k\"{u}nftig ...) patentierbar sein k\"{o}nnte.  Unter dieser Voraussetzung kann man dann patentrechtlich argumentieren. Die auf diese Weise auf dem einen oder anderen Wege erh\"{a}ltlichen Patente haben allerdings nur denjenigen Wert, den man ihnen beimisst -- oder der sich durch einen Konsens ergibt, dieser Frage nicht genauer nachgehen zu wollen.  Tats\"{a}chlich kann die Verdrehung der Gesetzesregeln nur insoweit Wirkung entfalten, wie sich ein Konsens dar\"{u}ber herstellen l\"{a}sst, ob man dieses Spiel gegen die bestehenden Gesetzesregeln spielen soll oder nicht.  Hierbei handelt es sich   nicht mehr um eine juristische Frage im strengen Sinne.}
\end{quote}

Nach einer intensiven \"{o}ffentlichen Debatte hat sich herausgestellt, dass die derzeit bestehende Gesetzesregel zweckm\"{a}{\ss}ig ist und das davon abweichende neuere EPA-Fallrecht nicht dem \"{o}ffentlichen Interesse entspricht.  Die Gerichte sind aufgerufen, ihre Praxis zu korrigieren und das Gesetz anzuwenden.

Das Europ\"{a}ische Parlament hat eine ge\"{a}nderte Richtlinie\footnote{\url{http://swpat.ffii.de/papiere/europarl0309/index.de.html}} angenommen, die das System von Art. 52 EP\"{U} bekr\"{a}ftigt und es genauer ausf\"{u}hrt.  Frits Bolkestein und einige Leute im Rat m\"{o}gen diese Klarstellung nicht und schlagen vor, lieber das EP\"{U} abzu\"{a}ndern oder eine andere Art der zwischenstaatlichen Vereinbarung zu finden.  Das UK-Patentamt hat vorgeschlagen\footnote{\url{http://swpat.ffii.de/papiere/ukpo-swpat0202/index.en.html}}, Art. 52(3) so umzuschreiben, dass alles, was man f\"{u}r ``technisch'' zu halten beliebt, patentiert werden kann.  Andererseits w\"{a}re es auch m\"{o}glich, Art. 52 EP\"{U} selbst im Sinne der vom Parlament ge\"{a}nderten Richtlinie zu konkretisieren.  Positive Definitionen von ``technischem Gebiet'', ``Technik'', ``Industrie'' usw., wie sie in der ge\"{a}nderten Richtlinie zu finden sind, in Art. 52ff EP\"{U} oder dessen nationale Versionen einzubringen, k\"{o}nnte ein Weg sein, die Richtlinie umzusetzen.

Wir schlagen optional die Streichung von Art. 52 Abs (3) vor, weil dieser Absatz nur erl\"{a}utert, was sich von selbst verstehen sollte.  Er geh\"{o}rt nicht in das Gesetz sondern in die Pr\"{u}fungsrichtlinien.  Durch eine Streichung kann die Rechtsprechung auf relativ unkomplizierte Weise veranlasst werden, zu einer korrekten Gesetzesauslegung zur\"{u}ckzukehren, wie in den 1970/80er Jahren vorherrschte.
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Auslegung von Artikel 52 des Europ\"{a}ischen Patent\"{u}bereinkommens hinsichtlich der Frage, inwieweit Software patentierbar ist\footnote{\url{http://swpat.ffii.de/stidi/epue52/exeg/index.de.html}}}}

\begin{quote}
Dr. Karl Friedrich Lenz, Professor f\"{u}r Deutsches Recht und Europarecht an der Universit\"{a}t Aoyama Gakuin in Tokio untersucht mit den allgemein anerkannten Methoden juristischer Auslegung, welche Bedeutung dem heute geltenden Text des Art 52 EP\"{U} beizumessen ist und gelangt zu dem Schluss, dass die Technischen Beschwerdekammern des EPA seit einiger Zeit regelm\"{a}{\ss}ig Patente auf Programme f\"{u}r Datenverarbeitungsprogrammen als solche erteilen und eine beunruhigende Bereitschaft zeigen, die eigenen Wertungen an die Stelle der Wertungen des Gesetzgebers zu setzen.
\end{quote}
\filbreak

\item
{\bf {\bf Moses, die Zehn Patentierungsverbote und das ``Stehlen mit einem weiteren ethischen Effekt''\footnote{\url{http://swpat.ffii.de/stidi/epue52/moses/index.de.html}}}}

\begin{quote}
Programme f\"{u}r Datenverarbeitungsanlagen sind heute in Europa unpatentierbar und patentierbar zugleich.  Wie gelang es den Technischen Beschwerdekammern des Europ\"{a}ischen Patentamtes im Laufe der Zeit, das unpatentierbare zu patentieren?  Bei komplizierten Themen bietet oft der satirische Vergleich den schnellsten Zugang zum gr\"{u}ndlichen Verst\"{a}ndnis.
\end{quote}
\filbreak

\item
{\bf {\bf Stalin \& Software Patents\footnote{\url{http://swpat.ffii.de/stidi/epue52/stalin/index.de.html}}}}

\begin{quote}
In this contribution to the European Patent Office (EPO) mailing list, a European patent attorney cites the EPO Examination Guidelines of 1978 as clear documentary evidence for the intention of the legislator to keep computer programs on any storage medium free from any claims to the effect that their distribution, sale or use could infringe on a patent.  But the European Patent Office's Technical Board of Appeal (TBA) apparently considered itself to be a kind of modern Stalin, an ultimate sources of wisdom in matters of whatever complexity, standing high above the legislator and the peoples of Europe, and even above the EPO's own institutions for judicial review.  In this way the TBA risks to antagonise the public, to create harmful legal insecurity especially for small patent-holders and to severely damage the delicate process of building confidence in international institutions.  The TBA should see itself as a conservator rather than an innovator.
\end{quote}
\filbreak

\item
{\bf {\bf Skandinavien: auch ohne ``als solches'' Klausel kann Diebstahl einen weiteren rechtlichen Effekt haben\footnote{\url{http://swpat.ffii.de/stidi/epue52/udgor/index.de.html}}}}

\begin{quote}
Art 52 of the European Patent Convention (EPC) stipulates that programs for computers as well as mental rules, mathematical methods, ways of presenting information etc are not patentable inventions and may therefore not be claimed as such.  The wording ``as such'' from Art 52(3) has however been used to undo all explicit limits on patentability.  The European Patent Office (EPO) has in 1997 begun to subdivide computer programs into two groups, ``as-such programs'' and ``not-as-such programs'', and has tried to justify this by historical claims about how art 52 EPC came about.  This reasoning is at odds with grammar as well as with history.  One particularly nice set of evidence comes from Scandinavia:  the Danish and Swedish national versions of art 52 EPC do not literally render Art 52(3) but rather incorporate its meaning in the first line of their version of Art 52(2), coming to exactly the same common sense conclusions to which independent grammatical analyses have come and which are also stated in the early EPO examination guidelines: that a ``program as such'' is ``something that constitutes only a program''.   It confirms that Art 52(3) merely exhorts examiners to look carefully where the novel achievement really lies, e.g. in a programming solution or in a chemical process which may happen to run under program control.
\end{quote}
\filbreak

\item
{\bf {\bf Why can't I patent my movie (as such)?\footnote{\url{http://swpat.ffii.de/stidi/epue52/film/index.en.html}}}}

\begin{quote}
A comment on software patents, actually.
\end{quote}
\filbreak

\item
{\bf {\bf Europ\"{a}isches Patent\"{u}bereinkommen\footnote{\url{http://www.european-patent-office.org/legal/epc97/d/}}}}

\begin{quote}
Text der EPA-Version
\end{quote}
\filbreak

\item
{\bf {\bf Art. 52 EP\"{U}: Patentf\"{a}hige Erfindungen\footnote{\url{http://www.european-patent-office.org/legal/epc/d/ar52.html}}}}

\begin{quote}
Volltext auf dem Webserver des EPA
\end{quote}
\filbreak

\item
{\bf {\bf EP\"{U} 172: Eispruchsrecht der Staaten\footnote{\url{http://www.european-patent-office.org/legal/epc/d/ar172.html}}}}

\begin{quote}
Die Mitgliedsstaaten sind f\"{u}r Fehler des EPA verantwortlich.
\end{quote}
\filbreak

\item
{\bf {\bf Berichte der M\"{u}nchener Diplomatischen Konferenz von 1973\footnote{\url{http://swpat.ffii.de/papiere/muckonf73/index.de.html}}}}

\begin{quote}
1973 konferierten Patentreferenten der europ\"{a}ischen Regierungen einen ganzen Monat lang in M\"{u}nchen, um ein einheitliches europ\"{a}isches Patenterteilungsverfahren einzuf\"{u}hren.  Diese Konferenz f\"{u}hrte zum Abschluss des Europ\"{a}ischen Patent\"{u}bereinkommens (EP\"{U}) und zur Gr\"{u}ndung des Europ\"{a}ischen Patentamtes (EPA).  In Art 52 EP\"{U} werden Programme f\"{u}r Datenverarbeitungsanlagen von der Patentierbarkeit ausgeschlossen.  Im Anschluss waren Softwareinnovationen einige Jahre lang tats\"{a}chlich nicht patentierbar, aber seit etwa 1986 haben Richter des EPA und einiger nationaler Gerichte den Ausschluss der Programme f\"{u}r Datenverarbeitungsanlagen Schritt f\"{u}r Schritt ausgeh\"{o}hlt.  Dabei \"{u}bergingen sie den Gesetzeswortlaut und wandten eine gewagte teleologische und historische Methode der Gesetzesauslegung an.  Zur St\"{u}tzung der historischen Auslegung wurden im wesentlichen auf die Konferenzberichte verwiesen.  Daher haben wir uns dieses Dokument per Fernleihe besorgt und uns den (relativ kurzen) Bericht \"{u}ber die Verhandlungen zu Artikel 52 n\"{a}her angeschaut.  Man lese und staune.
\end{quote}
\filbreak

\item
{\bf {\bf EPA 1978: Pr\"{u}fungsrichtlinien\footnote{\url{http://swpat.ffii.de/papiere/epo-gl78/index.de.html}}}}

\begin{quote}
Adopted by the President of the European Patent Office in accordance with EPC 10.2a with effect from 1978-06-01.  Excerpts concerning the question of technical invention, limits of patentability, computer programs, industrial application etc.
\end{quote}
\filbreak

\item
{\bf {\bf EPO 1990: T 0022/85\footnote{\url{http://swpat.ffii.de/papiere/epo-t850022/index.en.html}}}}

\begin{quote}
Eine technische Beschwerdekammer des Europ\"{a}ischen Patentamtes (EPA) weist eine Patentanmeldung zur\"{u}ck, die sich auf ein Programm f\"{u}r Datenverarbeitungsanlagen richtet.  1984 hatten die Pr\"{u}fer des EPA die Anmeldung aufgrund der urspr\"{u}nglichen Pr\"{u}fungsrichtlinien von 1978 zur\"{u}ckgewiesen und festgestellt, dass die Anspr\"{u}che sich auf ein Programm f\"{u}r Datenverarbeitungsanlagen bezogen.  Der Beschwerdef\"{u}hrer argumentierte auf der Grundlage neuerer Richtlinien und Gerichtsentscheidungen, dass seine Anspr\"{u}che sich auf technische Wirkungen und nicht auf ein Programm als solches richteten.  Die Beschwerdekammer weist die Berufung zur\"{u}ck, indem sie indirekt argumentiert, dass der Gebrauch einer Allzweck-Datenverarbeitungsanlage einer abstrakten Methode keine Technizit\"{a}t verleiht: ``Das Zusammenfassen eines Dokumentes, Speichern der Zusammenfassung und Abrufen derselben als Antwort auf eine Anfrage f\"{a}llt als solche in die Kategorie der Pl\"{a}ne, Regeln und Verfahren f\"{u}r gedankliche T\"{a}tigkeiten und stellt damit einen nicht patentierbaren Gegenstand gem\"{a}{\ss} Artikel 52 EP\"{U} dar'' und ``Die blo{\ss}e Darlegung der Folge von Schritten, die n\"{o}tig sind, eine Handlung durchzuf\"{u}hren, die als solche von der Patentierbarkeit gem\"{a}{\ss} Artikel 52 EP\"{U} ausgenommen ist, in Begriffen von Funktionen oder funktionellen Vorrichtungen, die mit herk\"{o}mmlichen Bestandteilen einer Datenverarbeitungsanlage realisiert werden sollen, f\"{u}hrt keine technischen Betrachtungen ein und kann deswegen dieser Handlung keine technische Eigenschaft verleihen und damit den Ausschluss von der Patentierbarkeit \"{u}berwinden.''
\end{quote}
\filbreak

\item
{\bf {\bf EPO TBA 2002/03 T 49/99: information modelling not technical, computer-implementation not new\footnote{\url{http://swpat.ffii.de/papiere/epo-t990049/index.en.html}}}}

\begin{quote}
In March 2002, a Technical Board of Appeal at the European Patent Office (EPO) rejects a patent application for a computerised information modelling system on the grounds that the subject matter is not an invention according to Art 52 EPC.  The Board argues largely in the original spirit of the EPO and differs significantly from some other recent EPO caselaw.  This is an important reason why industrial patent lawyers are pressing for new patentability legislation.  Under a CEC/McCarthy directive, EPO decisions such as this one would no longer be possible.
\end{quote}
\filbreak

\item
{\bf {\bf Gesetzesregel \"{u}ber den Erfindungsbegriff im Europ\"{a}ischen Patentwesen und seine Auslegung unter besonderer Ber\"{u}cksichtigung der Programme f\"{u}r Datenverarbeitungsanlagen\footnote{\url{http://swpat.ffii.de/stidi/eurili/index.de.html}}}}

\begin{quote}
Wir schlagen dem Gesetzgeber vor, beim Entwurf einer Richtlinie zur Frage der Patentierbarkeit von Software auf dem folgenden kurzen und klaren Text aufzubauen.
\end{quote}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis f\"{u}r die Demontage des Technikbegriffs\footnote{\url{http://swpat.ffii.de/stidi/erfindung/index.de.html}}}}

\begin{quote}
Bisher geh\"{o}ren Computerprogramme ebenso wie andere \emph{Organisations- und Rechenregeln} in Europa nicht zu den \emph{patentf\"{a}higen Erfindungen}, was nicht ausschlie{\ss}t, dass ein patentierbares Herstellungsverfahren durch Software gesteuert werden kann. Das Europ\"{a}ische Patentamt und einige nationale Gerichte haben diese zun\"{a}chst klare Regel jedoch immer weiter aufgeweicht.  Dadurch droht das ganze Patentwesen in einem Morast der Beliebigkeit, Rechtsunsicherheit und Funktionsuntauglichkeit zu versinken.  Dieser Artikel gibt eine Einf\"{u}hrung in die Thematik und einen \"{U}berblick \"{u}ber die rechtswissenschaftliche Fachliteratur.
\end{quote}
\filbreak

\item
{\bf {\bf Lamy Droit Informatique 1998\footnote{\url{http://swpat.ffii.de/papiere/lamy98/index.de.html}}}}

\begin{quote}
Der Artikel \"{u}ber Patente analysiert die Geschichte der franz\"{o}sischen und europ\"{a}ischen Patentrechtsprechung.  Er erkl\"{a}rt, warum die europ\"{a}ischen Parlamente und Gerichte sich in den 60-70er Jahren gegen die Patentierbarkeit von Computerprogrammen und Programmierideen entschieden und wie das Europ\"{a}ische Patentamt diese Entscheidungen seit 1986 schrittweise r\"{u}ckg\"{a}ngig gemacht hat.  Er warnt aber, dass auf diese Weise vom EPA gew\"{a}hrte Patente in der Praxis von ungewissem Wert sind.
\end{quote}
\filbreak

\item
{\bf {\bf Dr. Swen Kiesewetter - K\"{o}binger 2000: \"{U}ber die Patentpr\"{u}fung von Programmen f\"{u}r Datenverarbeitungsanlagen\footnote{\url{http://swpat.ffii.de/papiere/grur-skk01/index.de.html}}}}

\begin{quote}
Ein Patentpr\"{u}fer zeigt die Ungereimtheiten der Pr\"{u}fung von Software-Anmeldungen auf.  In ihrem Bem\"{u}hen, ein Gesetz umzuinterpretieren, welches unmissverst\"{a}ndlich die Patentierung von Datenverarbeitungsprogrammen verbietet, hat die Rechtsprechung im Laufe der Jahre Funktionsanspr\"{u}che zugelassen, die es dem Anmelder erlauben, ein Programm zu verkleiden.  Aber diese Funktionsanspr\"{u}che stellen eher Probleme als L\"{o}sungen dar, und die L\"{o}sung zu diesen Problemen besteht in einem (nicht patentierbaren) Datenverarbeitungsprogramm (als solchem).  Probleme zu patentieren ist aber noch weniger zul\"{a}ssig und in seinen Auswirkungen noch bedenklicher als Programme zu patentieren.
\end{quote}
\filbreak

\item
{\bf {\bf K\"{o}nig 2001: Patentf\"{a}hige Datenverarbeitungsprogramme - ein Widerspruch in sich\footnote{\url{http://swpat.ffii.de/papiere/grur-koenig01/index.de.html}}}}

\begin{quote}
Der D\"{u}sseldorfer Patentanwalt Dr. K\"{o}nig zeigt allerlei Ungereimtheiten in der Softwarepatent-Rechtsprechung des BGH und EPA auf, kritisiert ``Zirkelschl\"{u}sse'' und argumentiert, das EPA habe Art 52 EP\"{U} ``Gewalt angetan''.  Durch eine ``grammatische Auslegung des Begriffs ``Datenverarbeitungsprogramme als solche'''' gelangt er zu der Erkenntnis, dass damit nur alle Datenverarbeitungsprogramme ohne Ausnahme gemeint sein k\"{o}nnen, allerdings nur insoweit sie alleine Gegenstand des Patentbegehrens seien.  Seit der Umsetzung des EP\"{U} in die PatG-Novelle von 1978 gibt es keine Grundlage mehr f\"{u}r eine Unterscheidung zwischen technischen und untechnischen Programmen.  Beim Verst\"{a}ndnis des Begriffes ``DV-Programm'' habe sich die Rechtsprechung nach allgemeinen au{\ss}erjuristischen Definitionen zu richten, wie sie etwa von DIN-Fachleuten geleistet wurden.  Danach ist ein Programm die Gesamtheit aus Sprachdefinitionen und Text und nicht etwa nur der urheberrechtlich sch\"{u}tzbare Text.  Das EP\"{U}/PatG erlaube es aber, ``Kombinationserfindungen'' zu patentieren, die als ganzes auf Technizit\"{a}t, Neuheit, Nichtnaheliegen und gewerbliche Anwendbarkeit zu pr\"{u}fen seien.  Gerichte h\"{a}tten schon immer Kreativit\"{a}t entfaltet, wenn es darum ging, ``sich \"{u}ber Patentierbarkeitsausschl\"{u}sse hinwegzuhelfen''.  Datenverarbeitungsprogrammen (als solchen) k\"{o}nne auf dem Wege \"{u}ber ein Kombinationspatent mittelbar der volle Schutz eines Sachpatents zukommen.  Datenverarbeitungsprogramme seien ebenso wie etwa naturwissenschaftliche Entdeckungen (als solche) dem Verwendungsschutz zug\"{a}nglich.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl 2003-08-24: Ge\"{a}nderte Softwarepatent-Richtlinie\footnote{\url{http://swpat.ffii.de/papiere/europarl0309/index.de.html}}}}

\begin{quote}
Konsolidierte Version der wesentlichen Bestimmungen (Art 1-6) der ge\"{a}nderten Richtlinie ``\"{u}ber die Patentierbarkeit computer-implementierter Erfindungen'', f\"{u}r die das Europ\"{a}ische Parlament am 24. September 2003 gestimmt hat.
\end{quote}
\filbreak

\item
{\bf {\bf No\"{e}l Mam\`{e}re 2002-20-28: weg mit der ``als-solche''-Klausel!\footnote{\url{http://swpat.ffii.de/papiere/eubsa-swpat0202/mamere020228/index.en.html}}}}

\begin{quote}
Schl\"{a}gt Streichung von Art. 52(3) vor.
\end{quote}
\filbreak

\item
{\bf {\bf CEC \& BSA trying to impose unlimited patentability on Sweden\footnote{\url{http://swpat.ffii.de/papiere/eubsa-swpat0202/sslug0205/index.sv.html}}}}

\begin{quote}
In a statement submitted to the Swedish Ministry of Justice on behalf of SSLUG, a group of 6100 programmers and users of free software in the area around Copenhagen and Malm\"{o}, Erik Josefsson shows how an influential group at the European Commission and the European Patent Office has eroded the standards of patentability and is trying to impose a regime of patentability on all achievements of the human mind that can help to solve some practical problem.  This influential group has also, by overstretching the competence of the EPO's Technical Boards of Appeal, illegally overruled the Swedish courts and damaged the Swedish constitutional order.  Even in their most recent decisions in the mid-nineties, the Swedish courts did not agree with the EPO's illegal practice, but now the European Commission is set to force this practice on Sweden by means of ``european harmonisation''.  It was the duty of the EPO to abide by a role of ``cold harmonisation'' in the first place: act as a conservative follower and summarizer of national caselaw rather than as an innovative trendsetter pursuing its own agenda.  Josefsson cites ample examples of patents granted by the EPO and rejected by Swedish courts.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/epue52.el ;
% mode: latex ;
% End: ;

