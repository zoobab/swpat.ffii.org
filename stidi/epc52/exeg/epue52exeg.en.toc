\contentsline {section}{\numberline {1}Preface}{2}{section.1}
\contentsline {section}{\numberline {2}Wording of the Provisions}{2}{section.2}
\contentsline {section}{\numberline {3}Systematic Interpretation}{4}{section.3}
\contentsline {section}{\numberline {4}Teleologic Interpretation}{5}{section.4}
\contentsline {section}{\numberline {5}Historical interpretation}{5}{section.5}
\contentsline {section}{\numberline {6}Constitutional Interpretation}{6}{section.6}
\contentsline {section}{\numberline {7}Intermediate Result}{7}{section.7}
\contentsline {section}{\numberline {8}Discussion of the practise of the European Patent Office}{7}{section.8}
\contentsline {section}{\numberline {9}Further Reading (Editor's Recommendations)}{9}{section.9}
