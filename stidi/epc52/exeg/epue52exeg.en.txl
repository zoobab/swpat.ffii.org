<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Interpretation of Article 52 of the European Patent Convention in view
of the question, to what extent software is patentable

#descr: Dr. Karl Friedrich Lenz, professor for German and European Law at
Aoyama Gakuin University in Tokyo, investigates using the various
universally accepted methods of law interpretation which meaning has
to be attributed to the text of art 52 EPC today and reaches the
conclusion that the Technical Boards of Appeal of the European Patent
Office have for some time now regularly granted patents on programs
for computers as such and are showing a disturbing willingness to
substitute their own value judgements for those of the legislator.

#isa: Editor's Remark

#lcW: Copyright for the article by Karl-Friedrich Lenz. Translated from the
German original by Hartmut Pilch and confirmed as correct by
Karl-Friedrich Lenz.  Please feel free to copy, but ask first if you
want to publish a translation to another language.

#Vek: Preface

#DnW: The author has a certain legal policy opinion on whether software
patents make sense or not.  But this view is not what this paper is
about.  Rather, the purpose is to investigate solely by means of the
common methods or interpretation of laws what meaning has to be
attached to the text which is in force today.

#Wua: Moroever, also the existing caselaw will only be discussed shortly at
the end.   Our purpose is to determine the content of the Convention,
not to explain court verdicts.

#Wte: Wording of the Provisions

#Dgt: The %(ar:wording) is the starting point for any interpretation.  
Paragraph 2 says: programs for computers are not considered as
inventions. Paragraph 3 limits Paragraph 2 in the sense that Paragraph
2 excludes patentability of the subject-matter or activities referred
to in that provision only to the extent to which the patent relates to
such subject-matter or activities as such.

#DWe: This paragraph 3 particularly needs interpretation.

#Zeg: First of all: Paragraph 3 does not say forthright that only software
is non-patentable as such.  Rather it says that this applies to all
the mentioned %(q:subject-matter and activities).  Software is among
them, but it has to be noted that we are dealing with a general
limitation for all of the mentioned cases, which is not limited to
software.  In paragraph 2 fifteen case groups are mentioned.  It is
however conceivable that the words %(q:as such) might not apply to all
case groups to the same extent.

#Eri: A critical remark toward the writers of Article 52 should be allowed
here.  The limitation of all fifteen quite different case groups in
paragraph 2 by a unified formula %(q:as such) leads almost inevitably
to situations, in which this formula does not apply very well to some
of these case groups.  This may have contributed to the difficulties
that have occurred in the interpretation of the phrase %(q:software as
such).  If the authors of the document had found a wording tailored
and applicable to the field of software only, this would possibly have
been much more easier to understand. In contrast, the existing
universal fact of exclusion for all case groups has less chances to be
understandable and usable for all of the case groups.

#WbW: From the wording %(q:shall exclude...only to the extent) it has
moreover to be concluded that paragraph 3 is a partial limitation of
paragraph 2. This cannot be reconciled with an interpretation that
would unilaterally lead either to no limitation at all or to a total
exclusion on one of the case groups in paragraph 3.

#NWW: Now how we do have to understand software as such and, especially,
what is the opposite of this term?

#Zjt: In preparation to this question we may first want to examine whether
programs for computers are %(q:subject-matter) or %(q:activities) in
the meaning of paragraph 3.

#Pkl: Programs are based on activities, but they are no activities as such
and thus a %(q:subject-matter).

#DrW: This subject-matter may be existend in different forms. First of all a
program is composed in a language that can be understood by humans.
This source text is then transformed in a computer-executable form.
This source code is transformed in a executable form (object code).
Both forms can be stored on a data carrier (such as a CD-Rom) or
printed on paper.  An executable version can additionally be executed.

#Eog: It is not discernible that according to common language usage one of
these different forms would be called %(q:software as such), while for
the others a different term would apply.  If In order to argue to the
contrary, one would have to affirm which term that would be and to
which of those different forms it would apply.

#EeW: A further possibility you could think of would be to differentiate the
interaction  of software with other subject-matters. According to
that, software as such would be the domain in which software does not
or not to a significant degree interact with other objects
(subject-matters).

#SuW: For example one might differentiate between software as such and
software which is running on a computer.  The computer would be the
object with which software normally interacts, but it would be
considered to be outside the scope of the software as such.

#DuA: However this view is difficult to reconcile with the wording of
paragraph 3. Because every piece of software is determined to run on a
computer, this understanding excludes nothing at all and voids the
exclusion in paragraph 2 of all meaning.   However you draw the line:
a totally one-sided observation contradicts the wording %(q:only to
the extent that ...).

#Bds: The possibility is left to require a further effect outside the
execution of software on a computer. If you agree upon this,
immediately the question arises how far away from the computer this
effect has to be.  Is an effect on a device attached to a computer,
e.g. a screen, sufficient?  We cannot derive an answer from the
wording. Therefore an understand of a program %(q:as such) in this
spirit does not appear very cogent either.

#DWt: Thus by interpretation based on wording we do not immediately get a
clear picture.  This has to be recorded as preliminary result of the
wording-based interpretation.  The only clear conclusion is that
paragraph 3 is applicable to all case groups of parapgrah 2 with the
same wording and that the interpretation of this wording should have
comparable effects on all case groups.

#StA: Systematic Interpretation

#Dio: Systematic interpretation tries to determine the sense of a wording by
the context, in which a special wording is used and by the overall
concept of a law.

#Eee: A first step with this method is to determine the meaning of the
formula %(q:as such) for other case gropus mentioned in paragraph 2.

#Dlh: The first of three case groups found in paragraph 2 are discoveries.
Excluded als only discoveries as such. Does this mean, that we have to
split discoveries in two subsets %(q:discoveries as such) and
%(q:discoveries)? I do not think it made sense.  Rather all
discoveries are not objects (subject-matter) of patentability.  For
all inventions this applies only to the discovery as such.  It does
not exclude an invention based on an discovery, because this invention
is not the discovery but only uses this discovery.

#Wrr: If this understanding of the case group of discoveries is correct,
this would mean for the case group of software that software is not be
divised into subsets %(q:software as such) and %(q:other Software),
but that each form of software is not patentable.  Thus, the statement
of limitation only means  that other subject matters are patentable
even if software is used in their development.

#DKe: The second case groups mentioned in paragraph 2 are scientific
theories. Also here I suppose it is impossible to single out a certain
subset of %(q:scientic theories as such) as opposed to another subset
of %(q:patentable scientific theories). The same applies to the third
case group (mathematical methods). For all three case groups in number
1 of paragraph 2 the limitation formula %(q:as such) quite clearly
means that the object in question, be it a discovery, a scientific
theory or a mathematical method, could be mentioned as means to an end
of an invention in a patent application, but that nobody may
monopolize any such object. The antonym of %(q:as such) in all three
cases would be something like %(q:other subject-matter that was
invented with help of the mentioned subject-matter).  In contrast to
the case of software, the phrase %(q:as such) is easily understood in
these case groups.

#DuW: The fourth case group are aestetic creations (paragraph 2 item b).
Here it is difficult to see what meaning could be attributed to the
limitation %(q:as such).  For this case group too, one would not
assume that one special subset of %(q:aesthetic creations as such)
exists and could be opposed to %(q:patentable aesthetic creations).

#Dhe: The next 10 case groups are subsumed unter letter c of section 2.
Software is included, as the last case group.

#Dtt: The first one from those ten case groups are plans for mental
activities. Case groups two and three (rules and processes for mental
activities) do not differ very much from the first case group and can
be examined together.  How is the meaning of the limitation %(q:as
such) to be understood here? As in the previously examined case
groups, the limitation %(q:as such) does not demand to split mental
acts in two subsets: all mental acts are not patentable.

#DWe2: The next three case groups are plans, rules and processes for games.
Of these the group of rules for games can be understood most easily.
All games have rules.  Whoever proposes a new rule or develops a new
game, cannot attain patent protection therefor. The limitation to
%(q:rules as such) by 52(3) is in this case group is especially
difficult to determine.  It is impossible to derive precise criterion
for the case of software. This applies even more to the case groups of
%(q:plans for games) and %(q:processes for games), which are already
by themselves difficult to understand.

#AsV: Paragraph 2 regulates a few more case groups whose investigation we
may omit, since they do not promise much further gain of knowledge.
Therefore now I conclude as a result of a systematic interpretation:
In important other case groups the subject matter is not to be split
in two subsets (%(q:as such) and %(q:other)). Especially in the cases
under letter (a), this understanding of the phrase %(q:as such) is
evident. It must therefore also be assumed to be applicable to the
case of software.

#TgA: Teleologic Interpretation

#Dee: The method of teleological interpretation searches for the purpose
(greek telos) of a law.  Subsequently it choses among several possible
interpretations the one which is most conducive to putting this
purpose into practice.

#Vnq: This requires that the limitation of the exclusion of the mentioned
subject-matters and activities to the subject-matters and activities
%(q:as such) serves a purpose that can be directly recognised from the
law.

#AWe2: Section 3 relates to fifteen quite different case groups. Therefore it
is difficult to deternmine an evident purpose for paragraph 3 alone.

#DsW: Thus a teleological interpretation of paragraph 3 does not lead to any
discernible gain of knowledge.

#Hss: Historical interpretation

#Dei: A historical interpretation determines the meaning of a legal wording
based on what the persons involved in legislation thought. Other than
the methods used above it does not only use the legal text in
question, but also other texts (such as drafts and discussion
protocols).  These sources are well documented in a recent book of
%(kb:Beresford). The examination that follows is based on his
presentation.

#Afi: From the legislation process we get that in early draft documents the
universal exlcusion %(q:as such) in paragraph 3 was only applied to
the subject-matters in letter (a) (discoveries, scientific theories,
mathematical methods). For subject-matters mentioned in letter (c) the
statement of limitation was %(q:of purely abstract nature) (draft
1965) or %(q:of a purely intellectual nature) (draft 1969). This
explains the difficulties we face with the current wording. It does
not surprise that the limitation %(q:as such) is not to be understood
very well, given that it was initially not intented to be applied to
these.

#EGi: A later draft of 1971 incoporates an exclusion of software
patentability for the first time.  This exclusion is not limited in
any way. In that draft, the objects (subject matter) now mentioned in
letter (c) are worded as follows: %(q:schemes, rules or methods of
doing business, performing purely mental acts or playing games). A
limitation is only set for the case groups of mental acts (purely). 
When interpreting the text, one does not have to answer the question
what a method or a game as such might be.

#Dtw: At first the working group accepted the unconditional exclusion of
software from patentability. Then, under the impression of statements
from stakeholders, the current version was agreed upon, which only
excludes software from patentability only %(q:as such).

#Ael: As result of the historical interpretation we can note: the
understanding of the wording %(q:as such) has to oriented towards the
case groups mentioned in letter (a), because the phrase was originally
applied to these and thus is best understood in their context. 
Therefore the result from above systematic interpretation gains
special weight. Software is not to be separated into two subsets
(software as such and other software), but software is universally
excluded from patentability. However, this exclusion does not extend
to inventions , which were developed with help software, just as the
exclusion of discoveries and scientific theories does not extend to
inventions which make use of discoveries or scientific theories.

#Vge: Constitutional Interpretation

#DwW: The method of constitutional interpretation asks about the
constitutional effects of different results and selects the result
which is most closely in line with the values of the constitution.
Here especially basic rights are to be considered.

#Dih: For legal practise the most important basic right in the German
constitution is the principle of equality in  Article 3.1 of the Basic
Law (GG).

#Dwc: The rule of equality forbids to treat generally equal matters
differently without a practical reason.

#VAr: From those 15 case goups mentioned in paragraph 2 software and
aesthetic creations are equal to the extent that they are already
protected by copyright law. This is not the case for the other
subject-matters and activities. Therefore an intepretation according
to Article 3.1 GG has to take care that the interpretation of software
does not unreasonably diverge from the interpretation of aesthetic
creations. Vice versa a factual cause has to be specified if you
assume that software, in contrast to all the other subject matters of
inventions have to be double-protected by patent law and copyright.
Given that patents for aesthetic creations (e.g. criminal stories and
movies) are hardly ever granted, the equality perspective therefore
reinforces a narrow understanding of the limiting effect of paragraph
3 with regard to software.

#Wee: Further a constitutional interpretation would consider Article 103(2)
GG, which stipulates that an act may only be sanctioned when the
criminal action was determined by law. This forbids the extension of
criminal sanctions by case law or common use. It is relevant in this
context because paragrpah 142 of the German Patent Act (PatG) includes
criminal sanctions for patent infringement. This means the each 
patent grant can result in the imposition of criminal sanctions as
specified there.  However these sanctions can only be admissible to
the extent that the exclusions in the German equivalent of Art 52(2)
EPC do not apply. A grant of a patent based on interpretations which
are not reconcilable with the wording of the law would violate the
principle of lawfulness. The contitutional interpretation has to hold
the wording of law in high esteem.  An interpretation which completely
rebuilds this wording to suit the taste of a court is not only
unlawful but also unconstitutional.

#SgW: Finally probably Art. 14 of the basic law plays a role here because it
contains a institutional guarantee of property.  Provision 1 protects
property, but content and limits have to be determined by the
lawmaker. This would probably stand in the way of a total ex-post
abolishment of the patent system (unlike an abolishment which disabled
new patent applications beginning from a certain date). The question
of interpretation of Art 52 EPC regarding the patentability of
software is not about such a radical cut. If the working group from
1971 excluded software without any limitations from patentability, Art
14 GG did not stand in the way of this at all. And when today the
interpretation leads to the result, that software to a large extent is
excluded from patentability, it will not mean a unconstitutional
interception in the property of inventors under German Constitutional
law. This is further affirmed by the fact that software is already
protected by copyright and thus an exclusion of patentability does not
lead to an exclusion of all economic exploitability.

#Zhg: Intermediate Result

#Bne: The literal interpretation already forbids any interpretation that
would have the effect that no software is excluded from patentability,
since this is incompatible with the term %(q:only to the extent).

#DWe3: The systematic interpretation, corroborated by the historical
interpretation,  that software is not to be split in to subsets
(software %(q:as such) and other software), but that, as in the case
of discoveries and scientific theories, all software is exluded from
patentability but inventions developed with help of software are
patentable.

#Det: The constitutional interpretation forbids an unequal treatment in
comparison to esthetic creations without further justification and an
extension of patentability beyond the limits of the literal
interpretation by caselaw development.

#Dri: Discussion of the practise of the European Patent Office

#Dzl: Die Praxis des Europäischen Patentamtes orientiert sich derzeit an der
%(ib:Entscheidung der Technischen Beschwerdekammer 3.5.1 vom 1. Juli
1998, Stichwort Computerprogrammprodukt/IBM).

#EWt: Entscheidung der Technischen Beschwerdekammer des EPA mit Aktenzeichen
T 1173/97-3.5.1

#Aud: On page 12 the Technical Board of Appeal opines that computer programs
are to be divided in the two subsets %(q:software as such) and
%(q:other software). This opinion can not be reconciled with the
results derived above by a systematic interpretation.  The Board  does
not explain its reasoning but merely derives it from a superficial
overview of paragraphs 2 and 3.  A use of the common methods of legal
interpretation as outlined above is not found in this decision.

#AWe3: On the next page the Board asserts that the limitation %(q:as such)
has to be understood in the sense that computer programs as such are
only computer programs without a technical character.

#DWx: This is as far removed from the wording, that a litigation based on a
patent granted under this unlawful interpretation stands oppposed to
the pinciple of lawfulness (Art. 104.2 GG). It is a total redefinition
if the limitation in paragraph 3 that had nothing in common with the
meaning of the law. The Technical board of Appeal clearly transgresses
the boundaries of judicial competence.  Whoever wants to replace the
wording %(q:as such) by %(q:without a technical character) has to do
it by a change of the Convention according to the required legislative
procedures. Jurisdiction cannot do this.

#DnK: In autumn 200 the intergovernantal Conference had decided to enter a
requirement of a technical character in paragraph 1 of Art 52.
Paragraph 1 will be worded like this: %(q:European patents shall be
granted for any inventions, in all fields of technology, provided that
they are new, involve an inventive step and are susceptible of
industrial application.". This time the requirement %(q:in all fields
of technology) was not newly added to paragraph 3 in place of the
existing limitation %(q:as such), as EPO caselaw had done for
software, in transgression of its competence. According to the new
text the technical character can only be seen as an decisive criterion
when you share the view that this examination shall be applied two
times, first according to paragraph 1 then additionally according to
paragraph 3. For systematic reasons it makes little sense.

#Atr: Also a short systematic examination shows why the opinion of the
technical borad of appeal was questionable from the start.  To be
consistent, it would have to lead to a division of the field of
discoveries and scientific theories into two subsets each, one
%(q:with technical character) and %(q:without tehcnical character),
and to open a part of science for patent monopolisation.

#DrS: The subsequent deliberations of the Technical board of Appeal relate
to the question of how the field of software should be split in two
subsets of acccording to the critieria of %(q:technical character)
developed by the Board in violation of the wording and systematics.
These deliberations result in a total and unlimited recognition of
patentability of software. The subset %(q:software as such) in the
concept of the Technical Board of Appeal in of an extremely limited
scope.  This once more is incompatible with the literal meaning of the
wording %(q:only to the extent) in paragraph 3.

#Aec: Finally the attempt of teleological interpretaion, on which the
Technical Board of Appeal embarks on page 21, is very telling.  There
they say: %(q:In particular, the object and purpose of the EPC is the
grant of patents for inventions and thus to promote technical progress
by giving proper protection to these inventions. With this in mind,
the Board has arrived at its interpretation in the light of
developments in information technology. This technology tends to
penetrate most branches of society and leads to very valuable
inventions.)

#Des: This is in opposition to the negative result about a teleological
interpretation obove. This is about the interpretation of paragraph 3
of the European Patent Convention.  The purpose of the various
exclusions from patentability is, as concluded above, difficult to
recognise fromt the text. However the assertion that the limitation of
the exclusion of software from patentability to software as such in
paragraph 3 served the purpose of supporting progress in the light of
the development of information tehcnology, is not convincing. If the
lawmaker had intended such a purpose, he would not have introduced the
exclusion in paragraph 2 in the first place. The assumption of a legal
purpose that equals the wanted legislative result is no correct use of
the method of teleological interpretation, but it it clearly the
willingness of the the Technical Boards of Appeal to substitute its
own opinions for those of the lawmaker.

#Wrk: Further Reading

#Ege: Editor's Recommendations

#LenzBlog: Lenz Blog

#Vai: This article by the presiding judge of Germany's highest patent senate
approvingly quotes the text and arguments of Prof. Lenz.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/epue52.el ;
# mailto: mlhtimport@ffii.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: epue52exeg ;
# txtlang: xx ;
# End: ;

