<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Auslegung von Art. 52 des Europäischen Patentübereinkommens
hinsichtlich der Frage, inwieweit Software patentierbar ist

#descr: Dr. Karl Friedrich Lenz, Professor für Deutsches Recht und Europarecht
an der Universität Aoyama Gakuin in Tokio untersucht mit den allgemein
anerkannten Methoden juristischer Auslegung, welche Bedeutung dem
heute geltenden Text des Art 52 EPÜ beizumessen ist und gelangt zu dem
Schluss, dass die Technischen Beschwerdekammern des EPA seit einiger
Zeit regelmäßig Patente auf Programme für Datenverarbeitungsprogrammen
als solche erteilen und eine beunruhigende Bereitschaft zeigen, die
eigenen Wertungen an die Stelle der Wertungen des Gesetzgebers zu
setzen.

#Vek: Vorbemerkung

#Wte: Wortlaut der Regelung

#StA: Systematische Auslegung

#TgA: Teleologische Auslegung

#Hss: Historische Auslegung

#Vge: Verfassungskonforme Auslegung

#Zhg: Zwischenergebnis

#Dri: Diskussion der Praxis des Europäischen Patentamtes

#Wrk: Weitere Lektüre

#Ege: Empfehlungen des Herausgebers

#DnW: Der Autor hat bestimmte rechtspolitische Vorstellungen davon, ob
Softwarepatente sinnvoll sind oder nicht. Um diese geht es hier aber
nicht. Vielmehr soll allein mit den allgemein anerkannten Methoden
juristischer Auslegung ermittelt werden, welche Bedeutung dem heute
geltenden Text beizumessen ist.

#Wua: Weiter wird auch die bisherige Rechtsprechung nur zum Schluss kurz
diskutiert. Es geht hier vor allem darum, den Inhalt des Abkommens zu
ermitteln, nicht Urteile darzustellen.

#Dgt: Der %(ar:Wortlaut) ist der Ausgangspunkt für jede Auslegung. Danach
gilt (Absatz 2): Programme für Datenverarbeitungsanlagen werden nicht
als Erfindungen angesehen. Dies beschränkt Absatz 3 dahingehend, dass
Absatz 2 nur insoweit der Patentfähigkeit entgegensteht, als sich das
Patent auf die genannten Gegenstände oder Tätigkeiten als solche
bezieht.

#DWe: Dieser Absatz 3 bedarf vor allem der Auslegung.

#Zeg: Zunächst einmal: Absatz 3 sagt nicht direkt, dass nur Software als
solche nicht patentierbar ist. Vielmehr sagt er, dass die genannten
%(q:Gegenstände und Tätigkeiten) so zu behandeln sind. Dazu gehört
auch Software, aber es bleibt doch festzuhalten, dass es sich um eine
pauschale Einschränkung für alle genannten Fälle handelt, die nicht
allein für Software gedacht ist. In Absatz 2 sind insgesamt fünfzehn
Fallgruppen genannt. Es ist durchaus möglich, dass die Worte %(q:als
solche) nicht in allen Fallgruppen die gleiche Reichweite haben.

#Eri: Es sei hier auch eine kritische Bemerkung gegenüber dem Verfasser von
Art. 52 erlaubt. Die Beschränkung aller fünfzehn durchaus
unterschiedlichen Fallgruppen in Absatz 2 mit einer einheitlichen
Formel %(q:als solche) führt nahezu zwangsläufig dazu, dass diese
Formel für manche der Fallgruppen nicht besonders gut passt. Der Grund
für die Schwierigkeit, den Begriff %(q:Software als solche) zu
verstehen, liegt möglicherweise hier mit begründet. Wenn der Verfasser
des Textes für den Bereich Software eine hierauf zugeschnittene und
nur hierfür gültige Formulierung gefunden hätte, wäre diese
möglicherweise sehr viel leichter zu verstehen. Die geltende
Einheitseinschränkung für alle Fallgruppen dagegen hat weniger
Aussichten, für alle Fallgruppen in gleicher Weise gut verständlich
und anwendbar zu sein.

#WbW: Weiter ergibt sich aus dem Wortlaut %(q:steht nur insoweit entgegen),
dass Absatz 3 eine Beschränkung von Absatz 2 bedeutet, die teilweise
sein muss. Damit ist jede Auslegung nicht vereinbar, die einseitig
entweder zu überhaupt keiner Beschränkung oder zu einem völligen
Ausschluss einer der Fallgruppen in Absatz 2 führt.

#NWW: Nun zu der Frage, was unter Software als solcher zu verstehen ist, und
vor allem was der Gegensatz zu diesem Begriff ist.

#Zjt: Zur Einstimmung auf diese Frage sei zunächst untersucht, ob Programme
für Datenverarbeitungsanlagen %(q:Gegenstände) oder %(q:Tätigkeiten)
im Sinne von Absatz 3 sind.

#Pkl: Programme beruhen auf einer Tätigkeit, sind aber keine Tätigkeit als
solche und daher ein Gegenstand.

#DrW: Dieser Gegenstand kann in verschiedenen Formen vorliegen. Ein Programm
wird zunächst einmal in einer für Menschen verständlichen Sprache
geschrieben. Dieser ursprüngliche Text wird dann in eine für den
Computer ausführbare Form übersetzt. Beide Formen können auf einem
Datenträger (zB einer CD) gespeichert sein und auf Papier ausgedruckt
werden. Eine ausführbare Form kann zusätzlich noch eben das, nämlich
ausgeführt werden.

#Eog: Es ist nicht erkennbar, dass nach allgemeinem Sprachgebrauch eine
dieser verschiedenen Formen als %(q:Software als solche) bezeichnet
wird, während für andere Formen ein anderer Begriff gilt. Wer das
Gegenteil behauptet, müsste belegen, welcher andere Begriff das ist
und welche der verschiedenen Formen des Gegenstandes Software damit
gemeint ist.

#EeW: Eine weitere denkbare Möglichkeit wäre nach dem Zusammenwirken von
Software mit anderen Gegenständen zu unterscheiden. Danach wäre
Software als solche der Bereich, in dem Software nicht oder nicht in
genügendem Umfang mit anderen Gegenständen zusammenwirkt.

#SuW: So wäre etwa die Theorie denkbar, Software als solche von Software zu
unterscheiden, die auf einem Computer gerade läuft. Denn in diesem
Fall wirkt die Software bestimmungsgemäß mit dem Computer zusammen.

#DuA: Diese Auffassung ist allerdings schwer mit dem Wortlaut von Absatz 3
zu vereinbaren. Da jede Software dazu bestimmt ist, auf einem Computer
zu laufen, schließt das oben genannte Verständnis überhaupt nichts aus
und führt damit zu einem völligen Leerlauf der Ausnahme in Absatz 2.
Wie immer die Grenze zu ziehen ist: eine völlig einseitige Betrachtung
steht im Widerspruch zu dem Wortlaut %(q:nur insoweit).

#Bds: Bleibt die Möglichkeit, eine über das bloße Laufen auf dem Computer
hinausgehende Wirkung auf andere Gegenstände für den patentierbaren
Bereich zu verlangen. Wenn man dies will, stellt sich aber sofort die
Frage, wie weit diese Wirkung gehen muss. Reicht schon die Wirkung auf
ein an den Rechner angeschlossenes Gerät, etwa einen Bildschirm? Aus
dem Wortlaut lässt sich für diese Frage nichts entnehmen. Daher halte
ich es nicht für zwingend, ein Programm %(q:als solches) in diesem
Sinne zu verstehen.

#DWt: Damit ergibt sich aus dem Wortlaut zunächst keine sehr klares Bild.
Dies ist als Zwischenergebnis der Wortlautauslegung festzuhalten. Fest
steht nur, dass Absatz 3 für alle fünfzehn Fallgruppen von Absatz 2
mit einer einheitlichen Formulierung gilt und dass die Auslegung nicht
einseitig ausfallen darf.

#Dio: Die systematische Auslegung versucht aus dem Zusammenhang, in dem eine
bestimmte Formulierung gebraucht wird, sowie aus der gesamten
Konzeption eines Gesetzes den Sinn dieser Formulierung zu ermitteln.

#Eee: Ein erster Schritt mit dieser Methode ist es, die Bedeutung der Formel
%(q:als solche) für andere in Absatz 2 genannte Fallgruppen zu
ermitteln.

#Dlh: Die erste in Absatz zwei genannte Fallgruppe sind Entdeckungen.
Ausgeschlossen sind nur Entdeckungen als solche. Bedeutet dies,  dass
Entdeckungen in die zwei Teilmengen Entdeckungen als solche und andere
Entdeckungen aufzuteilen sind? Das halte ich nicht für sinnvoll.
Vielmehr sind alle Entdeckungen nicht Gegenstand der Patentierbarkeit.
Für alle Entdeckungen gilt dies jedoch nur für die Entdeckung als
solche. Nicht ausgeschlossen ist eine Erfindung, die zwar auf einer
Entdeckung beruht, aber eben nicht die Entdeckung als solche ist,
sondern diese nur benutzt.

#Wrr: Wenn dieses Verständnis für die Fallgruppe der Entdeckung richtig ist,
so würde dies für die Fallgruppe Software übertragen bedeuten, dass
Software nicht etwa in zwei Teilmengen Software als solche und andere
Software aufzuteilen ist, sondern dass alle Formen von Software nicht
patentierbar sind, und die Einschränkung nur bedeutet, dass andere
Gegenstände patentierbar sind, selbst wenn zu ihrer Entwicklung
Software verwendet wurde.

#DKe: Die zweite in Absatz 2 genannte Fallgruppe sind wissenschaftliche
Theorien. Auch hier halte ich es nicht für möglich, eine bestimmte
Teilmenge von %(q:wissenschaftlichen Theorien als solche) zu bilden
und dieser eine andere Teilmenge von patentierbaren wissenschaftlichen
Theorien gegenüberzustellen. Dies gilt entsprechend auch für die
dritte Fallgruppe (mathematische Methoden). Für alle drei Fallgruppen
in der Nummer eins von Absatz zwei ist die Einschränkung %(q:als
solche) relativ klar in dem Sinne zu verstehen, dass die Entdeckungen,
wissenschaftlichen Theorien oder mathematischen Methoden als Mittel
zum Zweck einer Erfindung in einem Patentantrag genannt sein können,
dass aber niemand auch nur einen Teilbereich hieraus monopolisieren
darf. Der Gegenbegriff zu %(q:als solche) ist für diese drei
Fallgruppen %(q:mit Hilfe des genannten Gegenstandes entstanden).
Anders als im Fall von Software ist der Begriff %(q:als solche) in
diesen Fallgruppen auch relativ klar verständlich.

#DuW: Die vierte Fallgruppe sind ästhetische Formschöpfungen (Absatz 2
Buchstabe b). Hier ist schwer zu sehen, welchen Sinn die Einschränkung
%(q:als solche) haben kann. Jedenfalls lässt sich auch für diese
Fallgruppe nicht behaupten, dass einer bestimmten Teilmenge von
ästhetischen Formschöpfungen %(q:als solche) eine ander Teilmenge
patentierbarer ästhetischer Formschöpfungen gegenüberzustellen sei.

#Dhe: Die nächsten zehn Fallgruppen sind unter Buchstabe c) von Absatz zwei
aufgezählt. Darunter fällt auch Software, als letzte Fallgruppe.

#Dtt: Die erste unter diesen zehn Fallgruppen ist die der Pläne für
gedankliche Tätigkeiten. Fallgruppen zwei und drei (Regeln und
Verfahren für gedankliche Tätigkeiten) unterscheiden sich von der
ersten Fallgruppe nicht wesentlich und können daher zusammen mit
dieser auf einmal untersucht werden. Wie ist die Einschränkung %(q:als
solche) hier zu verstehen? Ebenso wie in den bisher untersuchten
Fallgruppen verlangt die Einschränkung %(q:als solche) auch hier
nicht, gedankliche Tätigkeiten in zwei Teilmengen aufzuteilen,
vielmehr sind alle gedanklichen Tätigkeiten nicht patentierbar.

#DWe2: Die nächsten drei Fallgruppen sind Pläne, Regeln und Verfahren für
Spiele. Davon ist die Gruppe der Regeln für Spiele am einfachsten zu
verstehen. Alle Spiele haben Regeln. Wer eine neue Regel vorschlägt
oder gar ein völlig neues Spiel entwickelt, kann dafür keinen
Patentschutz erreichen. Die Beschränkung auf Regeln %(q:als solche)
durch Absatz 3 ist bei dieser Fallgruppe besonders schwer zu
verstehen. Jedenfalls lässt sich keine klare Richtschnur für den Fall
von Software ableiten. Das gilt erst recht für die Fallgruppen der
%(q:Pläne für Spiele) und der %(q:Verfahren für Spiele), die bereits
als solche unverständliche Formulierungen sind.

#AsV: Absatz zwei regelt noch weitere Fallgruppen. Ein erheblicher weiterer
Gewinn an Erkenntnis ist aber von deren Untersuchung nicht zu
erwarten. Daher halte ich jetzt als Ergebnis einer systematischen
Auslegung fest: In wichtigen anderen Fallgruppen ist der betreffende
Gegenstand nicht in zwei Teilmengen (%(q:als solche) und %(q:andere))
aufzuteilen. Vor allem in den in Buchstabe a) geregelten Fällen ist
dieses Verständnis des Begriffes %(q:als solche) relativ klar. Dies
ist daher auch für Software eine naheliegende Annahme.

#Dee: Die teleologische Auslegung fragt nach dem Zweck (griechisch telos)
des Gesetzes. Dann wird unter mehreren Alternativen zur Auslegung
diejenige ausgewaehlt, die diesen Zweck am ehesten zu verwirklichen
hilft.

#Vnq: Voraussetzung dafür ist, dass die Beschränkung des Ausschlusses der
genannten Gegenstände und Tätigkeiten auf die Gegenstände und
Tätigkeiten %(q:als solche) einen direkt aus dem Gesetz klar
erkennbaren Zweck verfolgt.

#AWe2: Absatz 3 betrifft fünfzehn durchaus verschiedene Fallgruppen. Daher
fällt es schwer, einen klar erkennbaren Zweck nur für Absatz 3
festzustellen.

#DsW: Daher bringt eine teleologische Auslegung von Absatz 3 keinen
erkennbaren Ertrag.

#Dei: Die historische Auslegung ermittelt, was die an der Gesetzgebung
beteiligten Personen sich gedacht haben, um den Sinn einer
Formulierung zu erschliessen. Anders als die oben verwendeten Methoden
arbeitet sie nicht allein mit dem Text des Gesetzes, sondern verwendet
andere Texte (Entwürfe und Diskussionsprotokolle). Diese Texte sind in
einem neueren Buch von %(kb:Beresford) gut erschlossen. Die folgende
Untersuchung beruht auf dieser Darstellung.

#Afi: Aus der Entstehungsgeschichte ergibt sich zunächst, dass die
universale Einschränkung %(q:als solche) in Absatz drei sich in den
früheren Entwürfen nur auf die in Buchstabe a) genannten Gegenstände
bezog (Entdeckungen, wissenschaftliche Theorien und mathematische
Methoden). Für die in Buchstabe c) genannten Gegenstände war die
Einschränkung dagegen %(q:of a purely abstract nature) (Entwurf 1965)
bzw. %(q:of a purely intellectual nature) (Entwurf 1969). Dies erklärt
die Schwierigkeiten mit der Auslegung der gegenwärtigen Formulierung.
Es ist kein Wunder, dass die Einschränkung %(q:als solche) für die
Fallgruppen nicht besonders klar ist, für die sie ursprünglich nicht
gedacht war.

#EGi: Ein späterer Entwurf (1971) enthält zum ersten Mal einen Ausschluss
der Patentierbarkeit von Software. Dieser enthält keinerlei
Einschränkung. Die jetzt in Buchstabe c) genannten anderen Gegenstände
sind in diesem Entwurf wie folgt formuliert: %(q:schemes, rules or
methods of doing business, performing purely mental acts or playing
games). Eine Einschränkung ist nur für die Fallgruppe der %(q:mental
acts) vorgesehen (%(q:purely)). Dem Anwender dieses Textes bleibt z.B.
die Frage erspart, was unter einem Verfahren für ein Spiel als solches
zu verstehen ist.

#Dtw: Die Arbeitsgruppe hat den unbedingten Ausschluss der Patentierbarkeit
von Software zunächst akzeptiert. Dann wurde aber auch unter dem
Eindruck von Stellungnahmen interessierter Kreise die gegenwärtige
Fassung beschlossen, die Software nur %(q:als solche) von der
Patentierbarkeit ausschließt.

#Ael: Als Ergebnis der historischen Auslegung läßt sich festhalten: Das
Verständnis des Begriffes %(q:als solche) hat sich vor allem an den in
Buchstabe a) genannten Fallgruppen zu orientieren, da der Begriff
ursprünglich für diese gedacht und daher in dem Zusammenhang am
klarsten verständlich ist. Daher erhält das oben entwickelte Ergebnis
der systematischen Auslegung besonderes Gewicht: Software ist nicht
etwa in zwei Teilmengen aufzuteilen (Software als solche und andere
Software), sondern Software ist umfassend von der Patentierbarkeit
ausgeschlossen. Dieser Ausschluss erstreckt sich jedoch nicht auf
Erfindungen, die mit Hilfe von Software entwickelt sind, ebenso wie
der Ausschluss für Entdeckungen und wissenschaftliche Theorien sich
nicht auf Erfindungen erstreckt, die Entdeckungen oder
wissenschaftliche Theorien anwenden.

#DwW: Die Methode der verfassungskonformen Auslegung fragt nach
verfassungsrechtlichen Auswirkungen verschiedener Ergebnisse und wählt
das Ergebnis, das mit den Wertungen der Verfassung am besten vereinbar
ist. Dabei sind vor allem die Grundrechte zu beachten.

#Dih: Das für die Praxis wichtigste Grundrecht in der deutschen Verfassung
ist der Gleichheitssatz, Artikel 3 Absatz 1 Grundgesetz.

#Dwc: Der Gleichheitssatz verbietet, wesentlich Gleiches ohne sachlichen
Grund ungleich zu behandeln.

#VAr: Von den fünfzehn Fallgruppen in Absatz 2 sind Software und ästhetische
Formschöpfungen insofern gleich, als sie bereits durch das
Urheberrecht geschützt sind. Dies ist für alle anderen Gegenstände
oder Tätigkeiten nicht der Fall. Daher hat eine an Artikel 3 Absatz 1
des Grundgesetzes orientierte Auslegung vor allem darauf zu achten,
dass die Behandlung von Software nicht ohne sachlichen Grund von der
Behandlung ästhetischer Formschöpfungen abweicht. Umgekehrt ist ein
sachlicher Grund anzugeben, wenn man annehmen möchte, dass Software
anders als alle anderen Gegenstände von Erfindungen durch Patentrecht
und Urheberrecht doppelt geschützt werden muss. Da Patente für
ästhetische Formschöpfungen (z.B. Kriminalromane oder Filme) so gut
wie nie erteilt werden, verlangt dieser Gesichtspunkt daher ein eng
begrenztes Verständnis für den Ausnahmebereich des Absatz 3 in Bezug
auf Software.

#Wee: Weiter ist für die verfassungskonforme Auslegung Art. 103 Abs. 2 des
Grundgesetzes zu beachten. Dort ist angeordnet, dass eine Tat nur
bestraft werden kann, wenn die Strafbarkeit gesetzlich bestimmt war,
bevor die Tat begangen wurde. Dies verbietet die Ausweitung von
strafrechtlichen Verboten durch die Rechtsprechung oder durch
Gewohnheitsrecht. Das ist in diesem Zusammenhang relevant, weil ? 142
des deutschen Patentgesetzes Strafvorschriften für die Verletzung von
Patenten vorsieht. Dies bedeutet, dass jede Erteilung eines Patentes
die dort angeordneten strafrechtlichen Folgen nach sich ziehen kann.
Diese Folgen sind jedoch vom Gesetzgeber nur insoweit angeordnet, als
die mit Art. 52 des Europäischen Patentübereinkommens identischen
Ausschlusstatbestände in ? 1 Absatz 2 des deutschen Patentgesetzes
nicht greifen. Eine mit dem Wortlaut dieser Ausschlusstatbestände
nicht vereinbare Erteilung von Patenten verletzt damit das
Gesetzlichkeitsprinzip. Daher ist in verfassungskonformer Auslegung
dem Wortlaut des Gesetzes hier ein besonderer Stellenwert einzuräumen.
Eine Auslegung, die diesen Wortlaut nach Belieben des Gerichts völlig
umbaut, ist nicht nur einfach gesetzwidrig, sondern gleichzeitig auch
verfassungswidrig.

#SgW: Schließlich spielt möglicherweise Art. 14 des Grundgesetzes hier eine
Rolle, der eine institutionelle Garantie des Eigentums enthält. Absatz
1 bestimmt, dass Eigentum gewährleistet wird, Inhalt und Schranken
aber vom Gesetzgeber zu bestimmen sind. %(bv:Damit wäre es vermutlich
nicht ohne weiteres zu vereinbaren, das Patentwesen insgesamt
rückwirkend abzuschaffen) (im Gegensatz zu einer Abschaffung, die ab
einem bestimmten Stichtag keine neuen Patentanträge mehr zulässt). Bei
der Frage der Auslegung von Art. 52 des Europäischen
Patentübereinkommens in Bezug auf die Patentierbarkeit von Software
geht es jedoch nicht um einen derart radikalen Schnitt. Wenn die
Arbeitsgruppe entsprechend dem Entwurf 1971 Software ohne
Einschränkung von der Patentierbarkeit ausgeschlossen hätte, wäre sie
daran wohl kaum durch Art. 14 des Grundgesetzes gehindert gewesen. Und
wenn heute die Auslegung zu dem Ergebnis kommt, dass Software in
weiten Bereichen vom Patentschutz ausgenommen ist, bedeutet das noch
keinen verfassungsrechtlich unzulässigen Eingriff in das Eigentum von
Erfindern. Das gilt um so mehr, als Software ja bereits durch das
Urheberrecht geschützt ist, also ein Ausschluss von der
Patentierbarkeit keineswegs einen Ausschluss jeglicher
wirtschaftlichen Verwertbarkeit bedeutet.

#VfS: Vgl. auch die Entscheidungen des Bundesverfassungsgerichts in Band 36
der amtlichen Sammlung, Seite 281 und Band 31, Seite 229.

#Bne: Bereits die Wortlautauslegung schließt ein Verständnis aus, nach dem
für den Ausschluss von Software von der Patentierbarkeit kein
Anwendungsbereich verbleibt, da dies mit der Formulierung %(q:nur
insoweit) nicht vereinbar ist.

#DWe3: Die systematische Auslegung ergibt, besonders gestützt auch durch die
historische Auslegung, dass Software nicht etwa in zwei Teilmengen
(Software %(q:als solche) und andere Software) aufzuteilen ist,
sondern dass wie im Fall von Entdeckungen und wissenschaftlichen
Theorien alle Software von der Patentierbarkeit ausgeschlossen ist,
wohl aber mit Hilfe von Software entwickelte Erfindungen patentierbar
sind.

#Det: Die verfassungskonforme Auslegung verbietet eine Ungleichbehandlung
mit ästhetischen Formschöpfungen ohne sachlichen Grund und eine
Ausweitung der Patentierbarkeit ohne Rücksicht auf die oben
entwickelten Grenzen des Wortlautes.

#Dzl: Die Praxis des Europäischen Patentamtes orientiert sich derzeit an der
%(ib:Entscheidung der Technischen Beschwerdekammer 3.5.1 vom 1. Juli
1998, Stichwort Computerprogrammprodukt/IBM).

#EWt: Entscheidung der Technischen Beschwerdekammer des EPA mit Aktenzeichen
T 1173/97-3.5.1

#Aud: Auf Seite 12 des Umdruckes meint die Technische Beschwerdekammer, dass
Computerprogramme in die zwei Teilmengen Software als solche und
andere Software aufzuteilen sei. Diese Auffassung ist mit dem oben
gefundenen Ergebnis einer systematischen Auslegung nicht vereinbar.
Sie wird auch nicht näher begründet, sondern nur aus einer
oberflächlichen Gesamtbetrachtung der Absätze 2 und 3 abgeleitet. Eine
den obigen Ausführungen entsprechende Anwendung der allgemein
anerkannten Methoden juristischer Auslegung findet sich in dieser
Entscheidung nicht.

#AWe3: Auf der folgenden Seite behauptet die Kammer dann, die Einschränkung
%(q:als solche) sei dahingehend zu verstehen, dass Computerprogramme
als solche nur Computerprogramme ohne technischen Charakter seien.

#DWx: Dies ist so weit vom Wortlaut entfernt, dass eine Bestrafung aufgrund
eines infolge dieser gesetzwidrigen Auslegung erteilten Patentes mit
dem Gesetzlichkeitsprinzip (Art. 103 Absatz 2 des Grundgesetzes) in
Widerspruch steht. Es ist eine völlige Neuformulierung der Schranke in
Absatz 3, die mit dem Wortlaut des Gesetzes nichts mehr gemein hat.
Die technische Beschwerdekammer überschreitet damit klar die Grenzen
richterlicher Tätigkeit. Wer die Formulierung %(q:als solche) durch
die Formulierung %(q:ohne technischen Charakter) ersetzen möchte, muss
dies durch eine entsprechende Änderung des Vertragstextes nach den
dafür erforderlichen Verfahren bewirken. Die Rechtsprechung kann dies
nicht.

#DnK: Die Regierungskonferenz im Herbst 2000 hat beschlossen, in Absatz 1
von Art. 52 das Erfordernis eines technischen Charakters aufzunehmen.
Absatz 1 soll in Zukunft lauten: %(dq:European patents shall be
granted for any inventions, in all fields of technology, provided that
they are new, involve an inventive step and are susceptible of
industrial application.) Das Erfordernis %(q:in all fields of
technology) wurde nicht etwa in Absatz 3 neu eingefügt und die
bisherige Einschränkung %(q:as such) aufgegeben, wie dies die
Rechtsprechung in Überschreitung ihrer Kompetenzen für die Fallgruppe
Software getan hat. Nach dem neuen Text kann der technische Charakter
nur dann noch als entscheidendes Kriterium angesehen werden, wenn man
der Ansicht ist, dass diese Prüfung in Absatz 1 und Absatz 3 doppelt
vorgenommen werden soll, was systematisch wenig sinnvoll wäre.

#Atr: Auch eine kurze systematische Betrachtung zeigt, dass die Auffassung
der technischen Beschwerdekammer schon im Ausgangspunkt fragwürdig
ist. Sie müsste nämlich auch den Bereich der Entdeckungen und der
wissenschaftlichen Theorien in die Teilmengen %(q:mit technischem
Charakter) und %(q:ohne technischen Charakter) aufteilen und einen
Teil der Wissenschaft damit der Monopolisierung durch Patente
zugänglich machen.

#DrS: Die weiteren Überlegungen der Beschwerdekammer betreffen die Frage,
wie nach dem von der Kammer im Gegensatz zum Wortlaut und zur
Systematik entwickelten Kriterium %(q:technischer Charakter) der
Bereich der Software in zwei Teilmengen aufzuteilen sei. Diese
Überlegungen führen im Ergebnis zu einer völlig unbeschränkten
Anerkennung der Patentierbarkeit von Software. Die Teilmenge
%(q:Software als solche) ist in der Konzeption der technischen
Beschwerdekammer von verschwindend geringer Bedeutung, was wiederum
mit der Formulierung %(q:nur insoweit) in Absatz 3 unvereinbar ist.

#Aec: Aufschlussreich ist schließlich der Versuch einer teleologischen
Begründung, den die technische Beschwerdekammer auf Seite 21
unternimmt. Dort führt die technische Beschwerdekammer aus: %(q:Ziel
und Zweck des EPÜ ist es ja, Patente für Erfindungen zu erteilen und
durch einen angemessenen Schutz dieser Erfindungen den technischen
Fortschritt zu fördern. Vor diesem Hintergrund ist die Kammer im
Lichte der Entwicklung in der Informationstechnik zu ihrer
vorstehenden Auslegung gelangt, zumal die Informationstechnik
zunehmend fast alle Bereiche der Gesellschaft durchdringt und überaus
wertvolle Erfindungen liefert.)

#Des: Dies steht im Gegensatz zu der Fehlanzeige oben zum Punkt
teleologische Auslegung. Es geht hier um die Auslegung von Absatz 3
des Europäischen Patentübereinkommens. Dessen Zweck ist, wie oben
bereits festgestellt, im Hinblick auf die Zahl und die
Unterschiedlichkeit der Ausschlusstatbestände nicht objektiv und klar
ersichtlich. Die Behauptung, die Beschränkung des Ausschlusses von
Software von der Patentierbarkeit auf Software als solche in Absatz 3
habe den Zweck, im Lichte der Entwicklung der Informationstechnik
durch Anerkennung der Patentierbarkeit den technischen Fortschritt zu
fördern, überzeugt nicht. Falls der Gesetzgeber einen solchen Zweck
verfolgt haben sollte, hätte er den Ausschluss in Absatz 2 von
vornherein nicht vorgesehen. Die Unterstellung eines mit dem
gewünschten Ergebnis übereinstimmenden Gesetzeszweckes ist zwar keine
korrekte Anwendung der teleologischen Auslegungsmethode, zeigt aber
deutlich die Bereitschaft der technischen Beschwerdekammer, die
eigenen Wertungen an die Stelle der Wertung des Gesetzgebers zu
setzen.

#Vai: Dieser Artikel des Vorsitzenden Richters des höchsten deutschen
Patentsenates zitiert zustimmend den Text und die Argumente von Prof.
Lenz.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epue52exeg ;
# txtlang: ja ;
# multlin: t ;
# End: ;

