# -*- mode: makefile -*-

epue52exeg.de.lstex:
	lstex epue52exeg.de | sort -u > epue52exeg.de.lstex
epue52exeg.de.mk:	epue52exeg.de.lstex
	vcat /ul/prg/RC/texmake > epue52exeg.de.mk


epue52exeg.de.dvi:	epue52exeg.de.mk
	rm -f epue52exeg.de.lta
	if latex epue52exeg.de;then test -f epue52exeg.de.lta && latex epue52exeg.de;while tail -n 20 epue52exeg.de.log | grep -w references && latex epue52exeg.de;do eval;done;fi
	if test -r epue52exeg.de.idx;then makeindex epue52exeg.de && latex epue52exeg.de;fi

epue52exeg.de.pdf:	epue52exeg.de.ps
	if grep -w '\(CJK\|epsfig\)' epue52exeg.de.tex;then ps2pdf epue52exeg.de.ps;else rm -f epue52exeg.de.lta;if pdflatex epue52exeg.de;then test -f epue52exeg.de.lta && pdflatex epue52exeg.de;while tail -n 20 epue52exeg.de.log | grep -w references && pdflatex epue52exeg.de;do eval;done;fi;fi
	if test -r epue52exeg.de.idx;then makeindex epue52exeg.de && pdflatex epue52exeg.de;fi
epue52exeg.de.tty:	epue52exeg.de.dvi
	dvi2tty -q epue52exeg.de > epue52exeg.de.tty
epue52exeg.de.ps:	epue52exeg.de.dvi	
	dvips  epue52exeg.de
epue52exeg.de.001:	epue52exeg.de.dvi
	rm -f epue52exeg.de.[0-9][0-9][0-9]
	dvips -i -S 1  epue52exeg.de
epue52exeg.de.pbm:	epue52exeg.de.ps
	pstopbm epue52exeg.de.ps
epue52exeg.de.gif:	epue52exeg.de.ps
	pstogif epue52exeg.de.ps
epue52exeg.de.eps:	epue52exeg.de.dvi
	dvips -E -f epue52exeg.de > epue52exeg.de.eps
epue52exeg.de-01.g3n:	epue52exeg.de.ps
	ps2fax n epue52exeg.de.ps
epue52exeg.de-01.g3f:	epue52exeg.de.ps
	ps2fax f epue52exeg.de.ps
epue52exeg.de.ps.gz:	epue52exeg.de.ps
	gzip < epue52exeg.de.ps > epue52exeg.de.ps.gz
epue52exeg.de.ps.gz.uue:	epue52exeg.de.ps.gz
	uuencode epue52exeg.de.ps.gz epue52exeg.de.ps.gz > epue52exeg.de.ps.gz.uue
epue52exeg.de.faxsnd:	epue52exeg.de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo epue52exeg.de-??.g3n`;source faxsnd main
epue52exeg.de_tex.ps:	
	cat epue52exeg.de.tex | splitlong | coco | m2ps > epue52exeg.de_tex.ps
epue52exeg.de_tex.ps.gz:	epue52exeg.de_tex.ps
	gzip < epue52exeg.de_tex.ps > epue52exeg.de_tex.ps.gz
epue52exeg.de-01.pgm:
	cat epue52exeg.de.ps | gs -q -sDEVICE=pgm -sOutputFile=epue52exeg.de-%02d.pgm -
epue52exeg.de/epue52exeg.de.html:	epue52exeg.de.dvi
	rm -fR epue52exeg.de;latex2html epue52exeg.de.tex
xview:	epue52exeg.de.dvi
	xdvi -s 8 epue52exeg.de &
tview:	epue52exeg.de.tty
	browse epue52exeg.de.tty 
gview:	epue52exeg.de.ps
	ghostview  epue52exeg.de.ps &
print:	epue52exeg.de.ps
	lpr -s -h epue52exeg.de.ps 
sprint:	epue52exeg.de.001
	for F in epue52exeg.de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	epue52exeg.de.faxsnd
	faxsndjob view epue52exeg.de &
fsend:	epue52exeg.de.faxsnd
	faxsndjob jobs epue52exeg.de
viewgif:	epue52exeg.de.gif
	xv epue52exeg.de.gif &
viewpbm:	epue52exeg.de.pbm
	xv epue52exeg.de-??.pbm &
vieweps:	epue52exeg.de.eps
	ghostview epue52exeg.de.eps &	
clean:	epue52exeg.de.ps
	rm -f  epue52exeg.de-*.tex epue52exeg.de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} epue52exeg.de-??.* epue52exeg.de_tex.* epue52exeg.de*~
epue52exeg.de.tgz:	clean
	set +f;LSFILES=`cat epue52exeg.de.ls???`;FILES=`ls epue52exeg.de.* $$LSFILES | sort -u`;tar czvf epue52exeg.de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
