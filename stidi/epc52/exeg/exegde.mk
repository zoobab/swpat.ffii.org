exegde.lstex:
	lstex exegde | sort -u > exegde.lstex
exegde.mk:	exegde.lstex
	vcat /ul/prg/RC/texmake > exegde.mk


exegde.dvi:	exegde.mk exegde.tex
	latex exegde && while tail -n 20 exegde.log | grep references && latex exegde;do eval;done
	if test -r exegde.idx;then makeindex exegde && latex exegde;fi
exegde.pdf:	exegde.mk exegde.tex
	pdflatex exegde && while tail -n 20 exegde.log | grep references && pdflatex exegde;do eval;done
	if test -r exegde.idx;then makeindex exegde && pdflatex exegde;fi
exegde.tty:	exegde.dvi
	dvi2tty -q exegde > exegde.tty
exegde.ps:	exegde.dvi	
	dvips  exegde
exegde.001:	exegde.dvi
	rm -f exegde.[0-9][0-9][0-9]
	dvips -i -S 1  exegde
exegde.pbm:	exegde.ps
	pstopbm exegde.ps
exegde.gif:	exegde.ps
	pstogif exegde.ps
exegde.eps:	exegde.dvi
	dvips -E -f exegde > exegde.eps
exegde-01.g3n:	exegde.ps
	ps2fax n exegde.ps
exegde-01.g3f:	exegde.ps
	ps2fax f exegde.ps
exegde.ps.gz:	exegde.ps
	gzip < exegde.ps > exegde.ps.gz
exegde.ps.gz.uue:	exegde.ps.gz
	uuencode exegde.ps.gz exegde.ps.gz > exegde.ps.gz.uue
exegde.faxsnd:	exegde-01.g3n
	set -a;FAXRES=n;FILELIST=`echo exegde-??.g3n`;source faxsnd main
exegde_tex.ps:	
	cat exegde.tex | splitlong | coco | m2ps > exegde_tex.ps
exegde_tex.ps.gz:	exegde_tex.ps
	gzip < exegde_tex.ps > exegde_tex.ps.gz
exegde-01.pgm:
	cat exegde.ps | gs -q -sDEVICE=pgm -sOutputFile=exegde-%02d.pgm -
exegde/exegde.html:	exegde.dvi
	rm -fR exegde;latex2html exegde.tex
xview:	exegde.dvi
	xdvi -s 8 exegde &
tview:	exegde.tty
	browse exegde.tty 
gview:	exegde.ps
	ghostview  exegde.ps &
print:	exegde.ps
	lpr -s -h exegde.ps 
sprint:	exegde.001
	for F in exegde.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	exegde.faxsnd
	faxsndjob view exegde &
fsend:	exegde.faxsnd
	faxsndjob jobs exegde
viewgif:	exegde.gif
	xv exegde.gif &
viewpbm:	exegde.pbm
	xv exegde-??.pbm &
vieweps:	exegde.eps
	ghostview exegde.eps &	
clean:	exegde.ps
	rm -f  exegde-*.tex exegde.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} exegde-??.* exegde_tex.* exegde*~
tgz:	clean
	set +f;LSFILES=`cat exegde.ls???`;FILES=`ls exegde.* $$LSFILES | sort -u`;tar czvf exegde.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
