<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Art. 52 EPÜ: Auslegung und Novellierung

#descr: Die im Europäischen Patentübereinkommen (EPÜ) 1973 festgelegten
Grenzen des Patentwesens sind im Laufe der Jahre verwischt worden.  
Führende Patentgerichte haben Art. 52 in einer Weise ausgelegt, die
ihn in der Praxis fast bedeutungslos macht.  Zahlreiche Rechtsgelehrte
haben gezeigt, warum dies unzulässig ist.  Das Europäische Patentamt
(EPA) hatte die Ungereimtheiten in Kauf genommen, weil es fest mit
einer Änderung des Art. 52 rechnete.  Es hat selbst
Änderungsvorschläge vorgelegt, die das Gesetz ganz an die EPA-Praxis
der grenzenlosen Patentierbarkeit anpassen.  Man könnte jedoch auch
den umgekehrten Weg gehen:  die Patentierbarkeit erneut im Sinne des
ursprünglichen Art. 52 regeln, und zwar in einer Weise, die weniger
Möglichkeiten des Missbrauchs offen lässt.  Diese Dokumentation
erkundet, was passiert ist und was für die Zukunft getan werden kann.

#otv: Europäisches Patentübereinkommen

#epc97text: Text der EPA-Version

#5ee: Art. 52 EPÜ: Patentfähige Erfindungen

#WtW: Volltext auf dem Webserver des EPA

#Tao: Die Mitgliedsstaaten sind für Fehler des EPA verantwortlich.

#rep: Eine technische Beschwerdekammer des Europäischen Patentamtes (EPA)
weist eine Patentanmeldung zurück, die sich auf ein Programm für
Datenverarbeitungsanlagen richtet.  1984 hatten die Prüfer des EPA die
Anmeldung aufgrund der ursprünglichen Prüfungsrichtlinien von 1978
zurückgewiesen und festgestellt, dass die Ansprüche sich auf ein
Programm für Datenverarbeitungsanlagen bezogen.  Der Beschwerdeführer
argumentierte auf der Grundlage neuerer Richtlinien und
Gerichtsentscheidungen, dass seine Ansprüche sich auf technische
Wirkungen und nicht auf ein Programm als solches richteten.  Die
Beschwerdekammer weist die Berufung zurück, indem sie indirekt
argumentiert, dass der Gebrauch einer
Allzweck-Datenverarbeitungsanlage einer abstrakten Methode keine
Technizität verleiht: %(q:Das Zusammenfassen eines Dokumentes,
Speichern der Zusammenfassung und Abrufen derselben als Antwort auf
eine Anfrage fällt als solche in die Kategorie der Pläne, Regeln und
Verfahren für gedankliche Tätigkeiten und stellt damit einen nicht
patentierbaren Gegenstand gemäß Artikel 52 EPÜ dar) und %(q:Die bloße
Darlegung der Folge von Schritten, die nötig sind, eine Handlung
durchzuführen, die als solche von der Patentierbarkeit gemäß Artikel
52 EPÜ ausgenommen ist, in Begriffen von Funktionen oder funktionellen
Vorrichtungen, die mit herkömmlichen Bestandteilen einer
Datenverarbeitungsanlage realisiert werden sollen, führt keine
technischen Betrachtungen ein und kann deswegen dieser Handlung keine
technische Eigenschaft verleihen und damit den Ausschluss von der
Patentierbarkeit überwinden.)

#Ben: Derzeitige Version des Art. 52

#Doi: Die EPÜ-Revisionskonferenz von 2000/11 schlug vor, den Zusatz %(q:auf
allen Gebieten der Technik) in Abs 1 einzufügen und Abs 4 zu
streichen.

#PaI: Patentfähige Erfindungen

#Eev: Europäische Patente werden für Erfindungen [auf allen Gebieten der
Technik] erteilt, sofern sie neu sind, auf einer erfinderischen
Tätigkeit beruhen und gewerblich anwendbar sind.

#Aee: Als Erfindungen im Sinn des Absatzes 1 werden insbesondere nicht
angesehen:

#End: Entdeckungen sowie wissenschaftliche Theorien und mathematische
Methoden;

#vhh: ästhetische Formschöpfungen;

#Pki: Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele
oder für geschäftliche Tätigkeiten sowie Programme für
Datenverarbeitungsanlagen;

#dao: die Wiedergabe von Informationen.

#Ars: Absatz 2 steht der Patentfähigkeit der in dieser Vorschrift genannten
Gegenstände oder Tätigkeiten nur insoweit entgegen, als sich die
europäische Patentanmeldung oder das europäische Patent auf die
genannten Gegenstände oder Tätigkeiten als solche bezieht.

#VW1: Verfahren zur chirurgischen oder therapeutischen Behandlung des
menschlichen oder tierischen Körpers und Diagnostizierverfahren, die
am menschlichen oder tierischen Körper vorgenommen werden, gelten
nicht als gewerblich anwendbare Erfindungen im Sinn des Absatzes 1.
Dies gilt nicht für Erzeugnisse, insbesondere Stoffe oder
Stoffgemische, zur Anwendung in einem der vorstehend genannten
Verfahren.

#rpf: Die ursprüngliche Auslegung und ihre Aufweichung

#tdl: Bis Ende der 80er Jahre wurde dies einhellig als klarer Ausschluss von
Software-Patenten verstanden, wie sie heute diskutiert werden. 
Beispielsweise %(ep:erklärt) die technische Beschwerdekammer des EPA
dessen Weigerung von 1984, ein Dokumentverarbeitungssystem zuzulassen,
auf Grundlage von Art. 52.2c:

#eWe: Als Begründung der Ablehnung wurde angeführt, dass der Beitrag zum
Stand der Technik allein in einem Computerprogramm als solchem im
Sinne von Artikel 52 Absatz 2(c) EPÜ bestand und dass demzufolge der
Antragsgegenstand keine patentierbare Erfindung im Sinne von Artikel
52 Absatz 1 EPÜ sei, in welcher Form auch immer er beansprucht werde.

#nsr: Zur Urteilsfindung ging die Prüfungsabteilung davon aus, dass
Ansprüche 1 und 2 sich auf eine Methode bezogen, ein Dokument von dem
System abzurufen.  Die Ansprüche verwiesen insbesondere auf einen
Wörterbuchspeicher, Eingabevorrichtungen, Hauptspeicher und einen
Prozessor.  Diese Rechnerbestandteile waren klassische Elemente eines
Informations- und Ablagesystems [...] und gemäß Artikel 54(2) EPÜ
wegen Mangels an Neuigkeit zu beanstanden.  Nach der vorgelegten
Beschreibung [...] wurden die Verfahrensschritte durch Programmierung
eines solch klassischen Systems implementiert. Die beanspruchte
Kombination der Schritte ergab keinen ungewöhnlichen Gebrauch der
einzelnen Rechnerbestandteile.  Die Ansprüche legen ein Zusammenspiel
bekannter Hardware und neuer Software fest, das sich damit befasst,
Dokumentinformationen zu speichern, aber nicht mit einer unerwarteten
oder ungewöhnlichen Weise, die bekannte Hardware zu betreiben.  Die
Unterschiede zwischen dem Stand de  r Technik und dem
Anspruchsgegenstand der vorliegenden Anmeldung wurden durch Funktionen
definiert, die durch ein Computerprogramm zu realisieren waren, das
dazu verwendet wurde, einen bestimmten Algorithmus oder eine
mathematische Methode zur Dokumentenanalyse zu implementieren.  Mit
anderen Worten, die Verfahrensschritte definierten Operationen, die
auf dem Inhalt der Information beruhten und unabhängig von der
jeweiligen Hardware waren.

#aul: Mit anderen Worten, ein Zusammenspiel gewohnter Rechner-Hardware mit
neuen Rechenregeln (Algorithmen), in welcher Form auch immer sie in
den Ansprüchen dargestellt werden, wäre von der Patentierbarkeit
ausgeschlossen.

#lat: Dies wurde ebenfalls klar in den %(ep:Prüfungsrichtlinien des
Europäischen Patentamts von 1978) ausgedrückt.

#ooo: Allerdings wurden 1985 die Prüfungsrichtlinien überarbeitet, und
insbesondere die Grenzen der Patentierbarkeit hinsichtlich
Computerprogrammen wurden verwischt.  In zwei Entscheidungen von 1986
erklärte die technische Beschwerdekammer des EPA die Liste der
Ausschlussgegenstände dahingehend, dass nur %(q:nichttechnische)
Neuerungen ausgeschlossen sein sollten, weigerte sich aber,
%(q:technisch) zu definieren -- ein Begriff, der im Gesetz nicht
vorkam.  Von da an begab sich das EPA auf einen %(ss:Schlitterkurs),
indem es den Bereich dessen, was als %(q:technisch) verstanden werden
könnte, immer weiter ausdehnte.

#oWt: Die Neuauslegung durch das EPA von 1985/1986 und die nachfolgende
Aufweichung wurden von Rechtsgelehrten wie %(LST) kritisiert und haben
zu einer Spaltung der Rechtspraxis geführt, deren Überwindung von
einer neuen EU-Richtlinie erhofft wird.

#cte: Die Entscheidungen am EPA wurden als %(q:Antwort auf Druck der
Computer-Industrie und der sich anbahnenden Entwicklungen in den USA)
angesehen.

#iti: Deutsches Handbuch des Patentrechts von 1986, erklärt die korrekte
Interpretation von Art. 52 EPÜ, wie sie von deutschen Gerichten
verwendet wird, und erklärt, dass die deutschen Gerichte damit einem
%(q:Druck von der Software-Industrie) widerstehen würden.  Krasser
erwähnt auch die Überarbeitung der Prüfungsrichtlinien des EPA von
1985 und erklärt, dass sie, noch in unklarem Zustand, in die von
%(q:der Industrie) geforderte Richtung gehen würden.

#NsP: Neue Version des Art. 52 laut EPA-Basisvorschlag von 2000

#Dng: Das EPA möchte auf der %(dk:Diplomatischen Konferenz im November 2000)
alle Reste einer Definition der Begriffe %(e:Erfindung),
%(e:Technizität), %(e:industrielle Anwendbarkeit) usw. aus dem Gesetz
tilgen und stattdessen die grenzenlose Patentierbarkeit aller
%(pw:praktischen und wiederholbaren Problemlösungen) verankern.  Das
hat es dem EPA ermöglicht, seinen Vorschlag sehr kurz zu halten:

#Woy: Der darauf erfolgende Aufschrei in der Öffentlichkeit brachte
Politiker der großen Länder dazu, diese vorgesehene Änderung von Art.
52 zu verhindern.  Allerdings wurde die Version des
%(q:Basisvorschlags) zur Neuformulierung von Art. 52(1) akzeptiert,
und Art. 52(4) wurde gestrichen (wodurch der Begriff der
%(q:gewerblichen Anwendbarkeit) an Substanz verlor).

#for: Daher enthält die Neufassung von Art. 52 EPÜ, die noch nicht in Kraft
ist, die %(TRIPs)-Formel %(q:auf allen Gebieten der Technik), lässt
aber eine Definition des neuen Begriffs %(q:Technik) vermissen, der im
alten EPÜ nicht vorkommt.  Somit scheint Absatz (2) relativiert zu
werden durch einen unbestimmten Begriff aus einem internationalen
Vertrag.  Es wäre im Interesse an Klarheit und Rechtssicherheit,
diesen Begriff zu konkretisieren, beispielsweise indem klar dargelegt
wird, was unter einer %(q:technischen Erfindung) zu verstehen ist und
warum Algorithmen, Geschäftsmethoden und Regeln zum Betreiben
bekannter Datenverarbeitungsanlagen nicht zu dieser Kategorie gehören.
Stattdessen entschieden die Gesetzgeber sich dafür, unbestimmte
Begriffe und denkbare Widersprüche in das Gesetz einzuführen, die dann
eher dadurch aufgelöst werden, dass man die Beifügung %(q:als solche)
in 52(3) in Anführungszeichen setzt, um sie geheimnisvoll und unklar
erscheinen zu lass  en und so der Patentgerichtsbarkeit zu erlauben,
ihre eigene Lieblingsinterpretation von %(q:Gebiet der Technik)
anzuwenden oder sogar auf angebliche WTO-Erfordernisse zu verweisen
und damit die Klarheit und Stimmigkeit nationalen Rechts zu Gunsten
willkürlicher Entscheidungen der internationalen
Patentanwaltsgemeinschaft aufzugeben.

#qos: Art. 52(4) über Chirurgie am menschlichen Körper wurde %(q:nur)
umformuliert und in Art. 53 verschoben.  Dies bedeutet allerdings,
dass chirurgische Methoden nicht mehr als Nicht-Erfindungen,
nichttechnisch oder nichtgewerblich angesehen werden.  Auf diese Weise
schwächte die diplomatische Konferenz wiederum die
TRIPs-Begrifflichkeiten, auf die sie sich zur Begrenzung der
Patentierbarkeit zu stützen entschieden hatte.

#Lgn: Die geänderte Richtlinie und Art. 52 EPÜ

#Dji: Die derzeitige Gesetzesregel über die Grenzen der Patentierbarkeit ist
klar und eindeutig.  Es gibt jedoch einige Gerichte, welche diese
Regel für unzweckmäßig halten und sie im Vorgriff auf eine angestrebte
Gesetzesänderung verworfen haben.  Wie Prof. Michel Vivant 1998
%(mv:schreibt):

#Eee: In Wirklichkeit sind die Gesetzesregeln des Übereinkommens und der
nationalen Gesetze klar:  Sie fordern unmissverständlich die
Nicht-Patentierbarkeit von Software.  Das Spiel, das heute gespielt
wird, besteht darin, in einer oder der anderen Weise diese Regeln zu
verdrehen, z.B. indem man sich, wie oben beschrieben, die Gesamtheit
aus Hardware und Software als eine virtuelle Maschine denkt, die
(künftig ...) patentierbar sein könnte.  Unter dieser Voraussetzung
kann man dann patentrechtlich argumentieren. Die auf diese Weise auf
dem einen oder anderen Wege erhältlichen Patente haben allerdings nur
denjenigen Wert, den man ihnen beimisst -- oder der sich durch einen
Konsens ergibt, dieser Frage nicht genauer nachgehen zu wollen. 
Tatsächlich kann die Verdrehung der Gesetzesregeln nur insoweit
Wirkung entfalten, wie sich ein Konsens darüber herstellen lässt, ob
man dieses Spiel gegen die bestehenden Gesetzesregeln spielen soll
oder nicht.  Hierbei handelt es sich   nicht mehr um eine juristische
Frage im strengen Sinne.

#Ncj: Nach einer intensiven öffentlichen Debatte hat sich herausgestellt,
dass die derzeit bestehende Gesetzesregel zweckmäßig ist und das davon
abweichende neuere EPA-Fallrecht nicht dem öffentlichen Interesse
entspricht.  Die Gerichte sind aufgerufen, ihre Praxis zu korrigieren
und das Gesetz anzuwenden.

#sfo: Das Europäische Parlament hat eine %(ad:geänderte Richtlinie)
angenommen, die das System von Art. 52 EPÜ bekräftigt und es genauer
ausführt.  Frits Bolkestein und einige Leute im Rat mögen diese
Klarstellung nicht und schlagen vor, lieber das EPÜ abzuändern oder
eine andere Art der zwischenstaatlichen Vereinbarung zu finden.  Das
UK-Patentamt hat %(uk:vorgeschlagen), Art. 52(3) so umzuschreiben,
dass alles, was man für %(q:technisch) zu halten beliebt, patentiert
werden kann.  Andererseits wäre es auch möglich, Art. 52 EPÜ selbst im
Sinne der vom Parlament geänderten Richtlinie zu konkretisieren. 
Positive Definitionen von %(q:technischem Gebiet), %(q:Technik),
%(q:Industrie) usw., wie sie in der geänderten Richtlinie zu finden
sind, in Art. 52ff EPÜ oder dessen nationale Versionen einzubringen,
könnte ein Weg sein, die Richtlinie umzusetzen.

#Wer: Wir schlagen optional die Streichung von Art. 52 Abs (3) vor, weil
dieser Absatz nur erläutert, was sich von selbst verstehen sollte.  Er
gehört nicht in das Gesetz sondern in die Prüfungsrichtlinien.  Durch
eine Streichung kann die Rechtsprechung auf relativ unkomplizierte
Weise veranlasst werden, zu einer korrekten Gesetzesauslegung
zurückzukehren, wie in den 1970/80er Jahren vorherrschte.

#del523: Schlägt Streichung von Art. 52(3) vor.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/epue52.el ;
# mailto: mlhtimport@ffii.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: epue52 ;
# txtlang: xx ;
# End: ;

