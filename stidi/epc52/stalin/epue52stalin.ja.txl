<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Stalin & Software Patents

#descr: In this contribution to the European Patent Office (EPO) mailing list,
a European patent attorney cites the EPO Examination Guidelines of
1978 as clear documentary evidence for the intention of the legislator
to keep computer programs on any storage medium free from any claims
to the effect that their distribution, sale or use could infringe on a
patent.  But the European Patent Office's Technical Board of Appeal
(TBA) apparently considered itself to be a kind of modern Stalin, an
ultimate sources of wisdom in matters of whatever complexity, standing
high above the legislator and the peoples of Europe, and even above
the EPO's own institutions for judicial review.  In this way the TBA
risks to antagonise the public, to create harmful legal insecurity
especially for small patent-holders and to severely damage the
delicate process of building confidence in international institutions.
 The TBA should see itself as a conservator rather than an innovator.

#Irt: In the %(gl:Guidelines for Examination in the European Patent Office
[1978]) the EPO interprets the term %(q:programs for computers) of Art
52(2):

#IWa: In the Stalin era, no post office in the then Soviet Union would
accept a telegram to Stalin unless it began:

#Tgy: This %(q:person cult) was good for the economies of the post office
but bad for people who just had to rely on the eminent wisdom of
comrade Stalin to make the right decisions. The great %(q:purges) from
1936 to 38 do not attest to this putative wisdom, and any clever human
society does well in distributing power and the making of important
decisions between several persons. In legal systems, this is done by
the process of judicial review which is pretty similar in all
countries, plus the system of making judges independent so that they
cannot be punished for their decisions.

#Ted: The EPO mailing list, in a way, is the opposite of a %(q:person cult)
in that it allows overt criticism of the EPO in matters such as
patenting software.  However, the practice of the EPO is quite
stalinistic in that it has pretty much inactivated the system of
judicial review built into the EPC (European Patent Convention).
Apparently, this is being done by a tacit understanding of the members
of the TBAs not to refer cases to the EBA (Enlarged Board of Appeal)
any longer (No cases were referred in 2000). In this way, the
presidents of the TBAs (Technical Boards of Appeal) have promoted
themselves into %(q:radiant lights of the people of Europe), into
%(q:flowers of European patent knowledge), into ultimate sources of
wisdom in matters like patenting software.  Nowadays, the buying of
any diskette containing SW might constitute patent infringement, but
this far-reaching consequence is only based upon decisions of one TBA
which had no authority whatsoever to issue binding decisions on this
basic matter without first referring the question to the EBA. Such a
referral was more than appropriate since what could be closer to %(q:a
computer program per se) than a diskette containing that selfsame
program? If that is not the %(q:program per se), what is? Then, the
term becomes without any meaning, and the above citations clearly show
that it was understood to be meaningful for some time and to mean just
that: A program stored on a memory device.

#Wpd: Whatever the EPO decides may have important consequences for hundreds
of millions of people in Europe and Cyprus, and my subjective feeling
is that the methods of Stalinism no longer have a place in modern
Europe, and that the judges of the TBAs should voluntarily subject
themselves to the rule of law and of judicial review.

#Idt: If this is not done, sooner or later a national court will decide that
it is not bound by this or that decision of the EPO as having no basis
in the EPC, and this will run through the European Patent System like
a shock wave and cause a great deal of harm to small firms who then no
longer can assert their patents because of legal uncertainty. It will
cause no such harm to big firms who usually have thousands of patents
so that loss of some has no meaning for them.

#TTs: The European Patent System is a giant experiment of unifying law in a
large and economic important part of the world. This can only be done
with extreme care and diplomacy, and not with brachial methods. The
members of the TBAs should always be aware of their function plus
their duties. For them, the emphasis is not on innovation but rather
on conservatism, careful deliberations, gaining full acceptance of
their decisions by long public discussions, and allowing all
interested circles to be heard. The present practice where final
decisions on such important matters are taken behind closed doors is
not acceptable to the people of Europe and will antagonize them.

#IWt: It may very well be that in the long run, the EBA will come to the
same result namely that program + diskette may constitute patent
infringement in certain cases. But the EBA may also arrive at the
contrary conclusion saying that the result might be desirable but is
impossible under the present wording of the EPC and thus a matter for
the parliaments of the member states to change. My feeling leans
toward the latter possiblity - you cannot decide that those who wrote
the EPC were morons and used a term which had no meaning at all.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: chengst ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epue52stalin ;
# txtlang: ja ;
# multlin: t ;
# End: ;

