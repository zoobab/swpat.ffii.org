<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Art 52 EPC: Interpretation and Revision

#descr: The limits of what is patentable which were laid down in the European
Patent Convention of 1973 have been eroded over the years. 
Influential patent courts have interpreted Art 52 in a way that
renders it obscure and meaningless.  Not all courts have followed this
interpretation, and numerous law scholars have shown why it is not
permissible.  The EPO had accepted the inconsistencies in anticipation
of an expected change of law.  However this expectation was frustrated
in 2000 by the governments and in 2003 by the European Parliament.  
The Parliament voted for a clarification which gives Art 52 back its
meaning.  Meanwhile, proponents from all sides have proposed to modify
Art 52(3) EPC in one or the other way, of course while claiming that
this merely serves to %(q:clarify the status quo) or to implement a
directive which serves this purpose, and, since the European
Commission and the Council have not signalled support for the
Parliament's approach, there is still no common understanding of which
%(q:status quo) we are talking about.

#otv: European Patent Convention

#epc97text: Text of EPO edition

#5ee: Art 52 EPC: Patentable Inventions

#WtW: full text from the EPO web server

#Tao: The contracting states are responsible for shortcomings of the EPO.

#rep: A Technical Board of Appeal of the European Patent Office (EPO)
rejects a patent application which is directed to a program for
computers.  In 1984, the EPO's examiners had rejected the patents
based on the original Examination Guidelines of 1978, saying that the
claims referred to a %(q:program for computers).  The appellant argued
on the basis of newer Guidelines and caselaw that his claims are
directed to technical effects and not a program as such.  The Board of
Appeal rejects the appeal by arguing indirectly that the use of
general-purpose computer hardware does not confer technicity on an
abstract method:  %(q:Abstracting a document, storing the abstract,
and retrieving it in response to a query falls as such within the
category of schemes, rules and methods for performing mental acts and
constitutes therefore non-patentable subject-matter under Article 52
EPC) and %(q:The mere setting out of the sequence of steps necessary
to perform an activity, excluded as such from patentability under
Article 52 EPC, in terms of functions or functional means to be
realised with the aid of conventional computer hardware elements does
not import any technical considerations and cannot, therefore, lend a
technical character to that activity and thereby overcome the
exclusion from patentability.)

#Ben: Current Version of Art 52

#rpf: The Initial Interpretation and its Erosion

#NsP: New Version of Art 52 according to the EPO's Base Proposal of 2000

#Lgn: The Amended Directive and the EPC

#Doi: The European Patent Convention (EPC) was signed by its core member
states in 1973 and went into force in 1978, when the European Patent
Office (EPO) was established on its basis.  At their revision
conference in 2000, the EPC member states proposed to insert %(q:in
all fields of technology) into Art 52(1) and to delete paragraph Art
52(4).

#PaI: Patentable Inventions

#Eev: European patents shall be granted for inventions [ in all fields of
technology ], as far as they are new, involve an inventive step and
are susceptible of industrial application.

#Aee: The following in particular shall not be regarded as inventions within
the meaning of paragraph 1:

#End: discoveries,  scientific theories and mathematical methods;

#vhh: aesthetic creations;

#Pki: schemes,  rules and methods for performing mental acts, playing games
or doing business, and programs for computers;

#dao: presentations of information.

#Ars: The provisions of paragraph 2 shall exclude patentability of the
subject-matter or activities referred to in that provision only to the
extent to which a European patent application or European patent
relates to such subject-matter or activities as such.

#VW1: Methods for treatment of the human or animal body by surgery or
therapy and diagnostic methods practised on the human or animal body
shall not be regarded as inventions which are susceptible of
industrial application within the meaning of paragraph 1. This
provision shall not apply to products,  in particular substances or
compositions,  for use in any of these methods.

#tdl: Until the late 80s, this was unanimously interpreted as clearly
excluding software patents, as they are usually understood in the
discussion today.  E.g. in 1990 the Technical Board of Appeal of the
EPO %(ep:explains) its refusal of 1984 to allow a document processing
system on the basis of Art 52.2c:

#eWe: The reason given for the refusal was that the contribution to the art
resided solely in a computer program as such within the meaning of
Article 52 EPC, paragraphs 2(c) and Consequently, this subject-matter
was not a patentable invention within the meaning of Article 52(1)
EPC, in whatever form it was claimed.

#nsr: In arriving at this conclusion the Examining Division argued on the
basis that Claims 1 and 2 related to a method for automatically
abstracting and storing an input document in an information storage
and retrieval system and Claims 3-6 to a corresponding method for
retrieving a document from the system. The claims specifically
referred to a dictionary memory, input means, a main memory and a
processor. These hardware elements were classical elements of an
information and retrieval system (...) and objectionable under Article
54(2) EPC as lacking novelty. According to the present description
(...) the method steps were implemented by programming such a
classical system. The claimed combination of steps did not imply an
unusual use of the individual hardware elements involved. The claims
merely defined a collocation of known hardware and new software
concerned with document information to be stored but not with an
unexpected or unconventional way of operating the known hardware. The
differences between the prior art and the subject-matter of the
present application were defined by functions to be realised by a
computer program which was used to implement a particular algorithm,
or mathematical method, for analysing a document. In other words the
steps of the method defined operations which were based on the content
of the information and were independent of the particular hardware
used.

#aul: In other words, a collocation of standard computing hardware with new
computing rules (algorithms), in whatever form it is presented in the
claim, would be excluded from patentability.

#lat: This was also clearly expressed in the %(ep:Examination Guidelines of
the European Patent Office of 1978).

#ooo: However, in 1985 the Guidelines were revised and in particular the
limits of patentability with respect to programs for computers were
blurred.  In two decisions of 1986, the EPO's Technical Board of
Appeal reinterpreted the list of exclusions to mean that only
%(q:non-technical) innovations should be excluded, but refused to
define %(q:technical) -- a concept that was not mentioned in the law. 
From thereon the EPO embarked on a %(ss:slippery slope) by gradually
widening the meaning of what could be understood to be %(q:technical).

#oWt: The EPO's reinterpretation of 1985/1986 and the subsequent loosening
were criticised by law scholars such as %(LST) and have led to a
schism of judicial practise, which a new EU directive is supposed to
overcome.

#cte: The decisions at the EPO were understood to have been taken %(q:in
response to pressure from the computer industry and trends emerging in
the US).

#iti: German manual of patent law of 1986, explains the correct
interpretation of Art 52 EPC, as used by the German courts, and
explains that the German courts are thereby resisting %(q:pressure
from the software industry).  Krasser also mentions the revision of
the EPO's examination guidelines in 1985 and explains that they, while
still unclear, seem to be moving into the direction demanded by
%(q:the industry).

#Dng: At the %(dk:Diplomatic Conference in november 2000), the EPO sought to
remove all traces of restricting definitions of %(q:invention) from
the Law and instead open the way for patentability of %(pw:all
practical and repeatable problem solutions).  This has allowed the EPO
to formulate a very short proposal.

#Woy: As a result of an uproar of public opinion, politicians from major
countries prevented this planned change of Art 52.  Yet the %(q:Base
Proposal) version was accepted as a new wording for Art 52(1), and Art
52(4) was deleted (whereby the concept of %(q:industrial application)
was further weakened).

#for: Thus the revised version of Art 52 EPC, which is not yet in force,
contains the %(TRIPs) formula %(q:in all fields of technology), but
fails to define the new term %(q:technology), which doesn't exist in
the old EPC.  Thus clause (2) seems relativised by an indeterminate
concept from an international treaty.   It would have been in the
interest of clarity and legal security to concretise this concept,
e.g. by explaining clearly what is to be understood by a %(q:technical
invention) and why algorithms, business methods and rules for
operating known data processing equipment do not belong into this
category.  Instead the legislators opted for introducing indeterminate
concepts and potential contradictions into the law, which are then
likely to be resolved by putting the appositive %(q:as such) from
52(3) in quotes to make it appear mysterious and unclear, so as to
allow the patent judiciary to rely on its own favored interpretation
of %(q:fields of technology) or even to point to alleged WTO
constraints, thus giving up the clarity and integrity of national law
in favor of arbitrary decisionmaking by the international patent
lawyer community.

#qos: Art 52(4) about surgery on the human body was %(q:only) reworded and
moved to Art 53.  This however implies that surgery methods are no
longer considered to be non-inventions, non-technical or
non-industrial.  In this way, the Diplomatic Conference further
weakened the TRIPs concepts on which it decided to rely for limiting
patentability.

#Dji: The current legal regulation about the limits of patentability is,
contrary to what its violators %(uk:say), clear and unambiguous. 
There are however lawcourts which consider this regulation inadequate
and have replaced it by a different regulation in anticipation of a
change of law.  As Prof. Michel Vivant %(mv:writes) in 1998:

#Eee: In reality, the national and conventional rules are clear:  they
stipulate without ambiguity a principle of non-patentability of
software.  The game which is being played today consists in twisting
these rules one way or another, e.g. by imagining to consider, as we
have seen, the totality of software and hardware as a virtual machine
which is potentially patentable (tomorrow ...).  From that point on
one can speak about software in patent language.  The patents which
may be obtained this way, by this channel or by another, however still
do not have any value beyond what we lend to them - but of course it
is possible that they will finally acquire a value simply through an
informal consensus to stop discussing the question.  In fact, the
efficiency of this twisting of rules of law is largely dependent on
whether this consensus evolves to take for granted -- against the
rules of written law -- that we will play this game or not.  This
question is no longer a legal question in the strict sense of the
term.

#Ncj: After an intensive public debate it has turned out that the current
legal rule is adequate and that recent EPO caselaw is at odds with
both the law and the public interest.  The courts are called upon to
correct their practise and apply the law.

#sfo: The European Parliament has passed an %(ad:amended directive) which
reconfirms the system of Art 52 EPC and makes it more explicit.  Frits
Bolkestein and some people in the Council do not like this
clarification and propose to opt instead for a revision of the EPC or
some other kind of inter-governmental agreement.  The UK Patent Office
has %(uk:proposed) to rewrite Art 52(3) in a way that allows anything
deemed %(q:technical) to be patented.  On the other hand it would also
be possible to concretise Art 52 EPC itself further in the spirit of
the amended directive.  Putting positive definitions of %(q:technical
field), %(q:technology), %(q:industry) etc, as found in the amended
directive, into Art 52ff EPC or its national versions could become a
way of implementing the directive.

#Wer: Any rewriting would probably entail a change in Art 52(3), since this
has been used to make the law appear unclear.  If rewritten in the
spirit of the amended directive, Art 52(3) might be removed
altogether, because it is to be considered purely explanatory.  A
%(q:program as such) is a %(q:program as a program) (in contrast with
%(q:a program as implementation feature of an claim object whose
inventive part is not a program)).   It stresses a self-evident
differentiation which belongs in the Examination Guidelines.  Deleting
it from the law would be a convenient way of telling lawcourts to
return to the correct interpretation of the law, which was predominant
during the 1970/80s.

#del523: Proposes to delete Art 52(3) EPC.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epue52 ;
# txtlang: ja ;
# multlin: t ;
# End: ;

