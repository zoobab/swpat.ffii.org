<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#descr: Die im Europäischen Patentübereinkommen (EPÜ) 1973 festgelegten Grenzen des Patentwesens im Laufe der Jahre verwischt worden.   Einflussreiche Patentgerichte haben Art 52 in einer Weise ausgelegt, die ihn in der Praxis fast bedeutungslos macht.  Andere sind dieser Auslegung nicht gefolgt, und zahlreiche Rechtsgelehrte haben gezeigt, warum sie unzulässig ist.  Das Europäische Patentamt (EPA) nahm die Ungereimtheiten in Kauf, weil es fest mit einer Änderung des Art 52 rechnete.  Diese wurde jedoch zunächst im Jahr 2000 von den Regierungen, dann im Jahr 2003 vom Europäischen Parlament verweigert.  Das Parlament entschied sich für eine Klarstellung, die dem Art 52 EPÜ seinen Sinn zurückgibt.  Von allen Seiten gibt es derweil immer wieder neue Vorschläge zur Änderung von Art 52, die freilich immer nur dazu dienen sollen, %(q:den Status Quo klar zu stellen) oder eine diesem Ziel dienende Richtlinie umzusetzen.  Da die Europäische Kommission und der Rat den Ansatz des Parlamentes bislang nicht zu unterstützen scheinen, stehen dabei jedoch nach wie vor zweierlei Bedeutungen von %(q:Status Quo) im Raum.

#title: Auslegung von Art. 52 des Europäischen Patentübereinkommens hinsichtlich der Frage, inwieweit Software patentierbar ist

#Ben: Derzeitige Version des Art 52

#Lgn: Die Geänderte Ricthlinie und Art 52 EPÜ

#NsP: Neue Version des Art 52 laut EPA-Basisvorschlag von 2000

#Doi: Die EPÜ-Revisionskonferenz von 2000/11 schlug vor, den Zusatz %(q:auf allen Gebieten der Technik) in Abs 1 einzufügen und Abs 4 zu streichen.

#PaI: Patentfähige Erfindungen

#Eev: Europäische Patente werden für Erfindungen auf allen Gebieten der Technik erteilt, sofern sie neu sind, auf einer erfinderischen Tätigkeit beruhen und gewerblich anwendbar sind.

#Aee: Als Erfindungen im Sinn des Absatzes 1 werden insbesondere nicht angesehen:

#End: Entdeckungen sowie wissenschaftliche Theorien und mathematische Methoden;

#vhh: ästhetische Formschöpfungen;

#Pki: Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele oder für geschäftliche Tätigkeiten sowie Programme für Datenverarbeitungsanlagen;

#dao: die Wiedergabe von Informationen.

#Ars: Absatz 2 steht der Patentfähigkeit der in dieser Vorschrift genannten Gegenstände oder Tätigkeiten nur insoweit entgegen, als sich die europäische Patentanmeldung oder das europäische Patent auf die genannten Gegenstände oder Tätigkeiten als solche bezieht.

#VW1: Verfahren zur chirurgischen oder therapeutischen Behandlung des menschlichen oder tierischen Körpers und Diagnostizierverfahren, die am menschlichen oder tierischen Körper vorgenommen werden, gelten nicht als gewerblich anwendbare Erfindungen im Sinn des Absatzes 1. Dies gilt nicht für Erzeugnisse, insbesondere Stoffe oder Stoffgemische, zur Anwendung in einem der vorstehend genannten Verfahren.

#Dji: Die derzeitige Gesetzesregel über die Grenzen der Patentierbarkeit ist klar und eindeutig.  Es gibt jedoch einige Gerichte, welche diese Regel für unzweckmäßig halten und sie im Vorgriff auf eine angestrebte Gesetzesänderung verworfen haben.  Wie Prof. Michel Vivant 1998 %(mv:schreibt):

#Ncj: Nach einer intensiven öffentlichen Debatte hat sich herausgestellt, dass die derzeit bestehende Gesetzesregel zweckmäßig ist und das davon abweichende neuere EPA-Fallrecht nicht dem öffentlichen Interesse entspricht.  Die Gerichte sind aufgerufen, ihre Praxis zu korrigieren und das Gesetz anzuwenden.

#Wer: Wir schlagen optional die Streichung von Art 52 Abs (3) vor, weil dieser Absatz nur erläutert, was sich von selbst verstehen sollte.  Er gehört nicht in das Gesetz sondern in die Prüfungsrichtlinien.  Durch eine Streichung kann die Rechtsprechung auf relativ unkomplizierte Weise veranlasst werden, zu einer korrekten Gesetzesauslegung zurückzukehren, wie in den 1970/80er Jahren vorherrschte.

#Dng: Das EPA möchte auf der %(dk:Diplomatischen Konferenz im November 2000) alle Reste einer Definition der Begriffe %(e:Erfindung), %(e:Technizität), %(e:industrielle Anwendbarkeit) usw. aus dem Gesetz tilgen und stattdessen die grenzenlose Patentierbarkeit aller %(pw:praktischen und wiederholbaren Problemlösungen) verankern.  Das hat es dem EPA ermöglicht, seinen Vorschlag sehr kurz zu halten:

#epc97text: Text der EPA-Version

#Tao: Die Mitgliedsstaaten sind für Fehler des EPA verantwortlich.

#rep: A Technical Board of Appeal of the European Patent Office (EPO) rejects a patent application which is directed to a program for computers.  In 1984, the EPO's examiners had rejected the patents based on the original Examination Guidelines of 1978, saying that the claims referred to a %(q:program for computers).  The appellant argued on the basis of newer Guidelines and caselaw that his claims are directed to technical effects and not a program as such.  The Board of Appeal rejects the appeal by arguing indirectly that the use of general-purpose computer hardware does not confer technicity on an abstract method:  %(q:Abstracting a document, storing the abstract, and retrieving it in response to a query falls as such within the category of schemes, rules and methods for performing mental acts and constitutes therefore non-patentable subject-matter under Article 52 EPC) and %(q:The mere setting out of the sequence of steps necessary to perform an activity, excluded as such from patentability under Article 52 EPC, in terms of functions or functional means to be realised with the aid of conventional computer hardware elements does not import any technical considerations and cannot, therefore, lend a technical character to that activity and thereby overcome the exclusion from patentability.)

#sfo: The European Parliament has passed an %(ad:amended directive) which reconfirms the system of Art 52 EPC and makes it more explicit.  Frits Bolkestein and some people in the Council do not like this clarification and propose to opt instead for a revision of the EPC or some other kind of inter-governmental agreement.  The UK Patent Office has %(uk:proposed) to rewrite Art 52(3) in a way that allows anything deemed %(q:technical) to be patented.  On the other hand it would also be possible to concretise Art 52 EPC itself further in the spirit of the amended directive.  Putting positive definitions of %(q:technical field), %(q:technology), %(q:industry) etc, as found in the amended directive, into Art 52ff EPC or its national versions could become a way of implementing the directive.

#rpf: Die Ursprüngliche Auslegung und ihre Aufweichung

#tdl: Until the late 80s, this was unanimously interpreted as clearly excluding software patents, as they are usually understood in the discussion today.  E.g. in 1990 the Technical Board of Appeal of the EPO %(ep:explains) its refusal of 1984 to allow a document processing system on the basis of Art 52.2c:

#eWe: The reason given for the refusal was that the contribution to the art resided solely in a computer program as such within the meaning of Article 52 EPC, paragraphs 2(c) and Consequently, this subject-matter was not a patentable invention within the meaning of Article 52(1) EPC, in whatever form it was claimed.

#nsr: In arriving at this conclusion the Examining Division argued on the basis that Claims 1 and 2 related to a method for automatically abstracting and storing an input document in an information storage and retrieval system and Claims 3-6 to a corresponding method for retrieving a document from the system. The claims specifically referred to a dictionary memory, input means, a main memory and a processor. These hardware elements were classical elements of an information and retrieval system (...) and objectionable under Article 54(2) EPC as lacking novelty. According to the present description (...) the method steps were implemented by programming such a classical system. The claimed combination of steps did not imply an unusual use of the individual hardware elements involved. The claims merely defined a collocation of known hardware and new software concerned with document information to be stored but not with an unexpected or unconventional way of operating the known hardware. The differences between the prior art and the subject-matter of the present application were defined by functions to be realised by a computer program which was used to implement a particular algorithm, or mathematical method, for analysing a document. In other words the steps of the method defined operations which were based on the content of the information and were independent of the particular hardware used.

#aul: In other words, a collocation of standard computing hardware with new computing rules (algorithms), in whatever form it is presented in the claim, would be excluded from patentability.

#ooo: However, in 1985 the Guidelines were revised and in particular the limits of patentability with respect to programs for computers were blurred.  In two decisions of 1986, the EPO's Technical Board of Appeal reinterpreted the list of exclusions to mean that only %(q:non-technical) innovations should be excluded, but refused to define %(q:technical) -- a concept that was not mentioned in the law.  From thereon the EPO embarked on a %(ss:slippery slope) by gradually widening the meaning of what could be understood to be %(q:technical).

#oWt: The EPO's reinterpretation of 1985/1986 and the subsequent loosening were criticised by law scholars such as %(LST) and have led to a schism of judicial practise, which a new EU directive is supposed to overcome.

#Woy: As a result of an uproar of public opinion, politicians from major countries prevented this planned change of Art 52.  Yet the %(q:Base Proposal) version was accepted as a new wording for Art 52(1), and Art 52(4) was deleted (whereby the concept of %(q:industrial application) was further weakened).

#cte: The decisions at the EPO were understood to have been taken %(q:in response to pressure from the computer industry and trends emerging in the US).

#iti: German manual of patent law of 1986, explains the correct interpretation of Art 52 EPC, as used by the German courts, and explains that the German courts are thereby resisting %(q:pressure from the software industry).  Krasser also mentions the revision of the EPO's examination guidelines in 1985 and explains that they, while still unclear, seem to be moving into the direction demanded by %(q:the industry).

#for: Thus the revised version of Art 52 EPC, which is not yet in force, contains the %(TRIPs) formula %(q:in all fields of technology), but fails to define the new term %(q:technology), which doesn't exist in the old EPC.  Thus clause (2) seems relativised by an indeterminate concept from an international treaty.   It would have been in the interest of clarity and legal security to concretise this concept, e.g. by explaining clearly what is to be understood by a %(q:technical invention) and why algorithms, business methods and rules for operating known data processing equipment do not belong into this category.  Instead the legislators opted for introducing indeterminate concepts and potential contradictions into the law, which are then likely to be resolved by putting the appositive %(q:as such) from 52(3) in quotes to make it appear mysterious and unclear, so as to allow the patent judiciary to rely on its own favored interpretation of %(q:fields of technology) or even to point to alleged WTO constraints, thus giving up the clarity and integrity of national law in favor of arbitrary decisionmaking by the international patent lawyer community.

#qos: Art 52(4) about surgery on the human body was %(q:only) reworded and moved to Art 53.  This however implies that surgery methods are no longer considered to be non-inventions, non-technical or non-industrial.  In this way, the Diplomatic Conference further weakened the TRIPs concepts on which it decided to rely for limiting patentability.

#lat: This was also clearly expressed in the %(ep:Examination Guidelines of the European Patent Office of 1978).

#otv: Europäisches Patentübereinkommen

#5ee: Art 52 EPÜ: Patentfähige Erfindungen

#WtW: Volltext auf dem Webserver des EPA

#Eee: In Wirklichkeit sind die Gesetzesregeln des Übereinkommens und der nationalen Gesetze klar:  Sie fordern unmissverständlich die Nicht-Patentierbarkeit von Software.  Das Spiel, das heute gespielt wird, besteht darin, in einer oder der anderen Weise diese Regeln zu verdrehen, z.B. indem man sich, wie oben beschrieben, die Gesamtheit aus Hardware und Software als eine virtuelle Maschine denkt, die (künftig ...) patentierbar sein könnte.  Unter dieser Voraussetzung kann man dann patentrechtlich argumentieren.  Die auf diese Weise auf dem einen oder anderen Wege erhältlichen Patente haben allerdings nur denjenigen Wert, den man ihnen beimisst --- oder der sich durch einen Konsens ergibt, dieser Frage nicht genauer nachgehen zu wollen.  Tatsächlich kann die Verdrehung der Gesetzesregeln nur insoweit Wirkung entfalten, wie sich ein Konsens darüber herstellen lässt, ob man dieses Spiel gegen die bestehenden Gesetzesregeln spielen soll oder nicht.  Hierbei handelt es sich nicht mehr um eine juristische Frage im strengen Sinne.

#del523: Schlägt Streichung von Art 52(3) vor.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epue52 ;
# txtlang: de ;
# multlin: t ;
# End: ;

