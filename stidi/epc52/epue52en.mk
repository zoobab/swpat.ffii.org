epue52en.lstex:
	lstex epue52en | sort -u > epue52en.lstex
epue52en.mk:	epue52en.lstex
	vcat /ul/prg/RC/texmake > epue52en.mk


epue52en.dvi:	epue52en.mk epue52en.tex
	latex epue52en && while tail -n 20 epue52en.log | grep references && latex epue52en;do eval;done
	if test -r epue52en.idx;then makeindex epue52en && latex epue52en;fi
epue52en.pdf:	epue52en.mk epue52en.tex
	pdflatex epue52en && while tail -n 20 epue52en.log | grep references && pdflatex epue52en;do eval;done
	if test -r epue52en.idx;then makeindex epue52en && pdflatex epue52en;fi
epue52en.tty:	epue52en.dvi
	dvi2tty -q epue52en > epue52en.tty
epue52en.ps:	epue52en.dvi	
	dvips  epue52en
epue52en.001:	epue52en.dvi
	rm -f epue52en.[0-9][0-9][0-9]
	dvips -i -S 1  epue52en
epue52en.pbm:	epue52en.ps
	pstopbm epue52en.ps
epue52en.gif:	epue52en.ps
	pstogif epue52en.ps
epue52en.eps:	epue52en.dvi
	dvips -E -f epue52en > epue52en.eps
epue52en-01.g3n:	epue52en.ps
	ps2fax n epue52en.ps
epue52en-01.g3f:	epue52en.ps
	ps2fax f epue52en.ps
epue52en.ps.gz:	epue52en.ps
	gzip < epue52en.ps > epue52en.ps.gz
epue52en.ps.gz.uue:	epue52en.ps.gz
	uuencode epue52en.ps.gz epue52en.ps.gz > epue52en.ps.gz.uue
epue52en.faxsnd:	epue52en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo epue52en-??.g3n`;source faxsnd main
epue52en_tex.ps:	
	cat epue52en.tex | splitlong | coco | m2ps > epue52en_tex.ps
epue52en_tex.ps.gz:	epue52en_tex.ps
	gzip < epue52en_tex.ps > epue52en_tex.ps.gz
epue52en-01.pgm:
	cat epue52en.ps | gs -q -sDEVICE=pgm -sOutputFile=epue52en-%02d.pgm -
epue52en/epue52en.html:	epue52en.dvi
	rm -fR epue52en;latex2html epue52en.tex
xview:	epue52en.dvi
	xdvi -s 8 epue52en &
tview:	epue52en.tty
	browse epue52en.tty 
gview:	epue52en.ps
	ghostview  epue52en.ps &
print:	epue52en.ps
	lpr -s -h epue52en.ps 
sprint:	epue52en.001
	for F in epue52en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	epue52en.faxsnd
	faxsndjob view epue52en &
fsend:	epue52en.faxsnd
	faxsndjob jobs epue52en
viewgif:	epue52en.gif
	xv epue52en.gif &
viewpbm:	epue52en.pbm
	xv epue52en-??.pbm &
vieweps:	epue52en.eps
	ghostview epue52en.eps &	
clean:	epue52en.ps
	rm -f  epue52en-*.tex epue52en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} epue52en-??.* epue52en_tex.* epue52en*~
tgz:	clean
	set +f;LSFILES=`cat epue52en.ls???`;FILES=`ls epue52en.* $$LSFILES | sort -u`;tar czvf epue52en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
