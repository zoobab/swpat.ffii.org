<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Politische Ökonomie des Patentwesens: die Mechanismen der Patentinflation

#descr: In den letzten 200 Jahren hat sich das Patentsystem kontinuierlich ausgeweitet.  Diese Expansion ist nicht das Ergebnis einer planmäßigen Wirtschaftspolitik sondern vielmehr eines selbstverstärkenden Mechanimsusses, den man etwa der Geldwert-Inflation oder dem Rüstungswettlauf vergleichen kann.  Dieser Artikel analysiert die Mechanismen der Patentinflation und verfolgt ihre Entwicklung.

#Ler: Weltkonzerne-Patentabteilungen erheben %(q:liberalstes) Patentsystem zum %(q:Standard)

#psi: Patentgesetzgeber stärken ihre eigene Stellung

#pup: Patentämter und Gerichte an der Perspektive des Patentanmelders ausgerichtet

#Pue: Patent-Goldrausch und konjunkturelle Strohfeuer

#SWv: Schnellballsystem begünstigt Initiator

#Sdt: Abkapselung der Patentwelt, Geistige Inzucht, Formalisierung des Rechtsdenkens

#Eoe: Erschöpfung der erfinderischen Ressourcen

#Gam: Allgemein wird angenommen, Firmen wie IBM und Siemens seien an einer Ausweitung des Patentsystems interessiert, weil es Großunternehmen begünstigt.  Aber das ist bestenfalls die halbe Wahrheit.

#Ins: In Wirklichkeit sind Großunternehmen nicht diejenigen, die am meisten vom Patentwesen profitieren.  Einige kleinere Unternehmen, die sich ganz auf die Entwicklung von Patenten statt von Produkten konzentrieren, sind u.U. noch besser für den Überlebenskampf auf dem Patent-Minenfeld optimiert.  Firmen wie %{LST} können Siemens oder IBM übel zusetzen und ihnen die Freude am Patentwesen gründlich verderben.

#Wtt: Wie kommt es dann, dass Siemens, IBM u.a. sich unermüdlich für die Ausweitung der Patentierbarkeitsgrenzen in Europa stark machen?

#Are: Ein neuerer %(ak:Artikel von Arno Körber), Chef der Patentabteilung von Siemens, liefert die Antwort.

#KWn: Körber regrets the loss of %(q:patent peace) in the area of micro-electronics and software and ascribes this to decisions by US lawcourts, namely the CAFC, in the early 1980s.  He describes how this has adversely affected interoperability, and he does not claim that it has brought any benefit either to Siemens or the economy as a whole.

#Ybt: Yet Körber says that Siemens has been pushing German patent courts and patent offices to emulate the US practise.  The reason for this is that Siemens is oriented toward the global market, and this market has come under a strong impact of the US patent system.  In order to make sure Siemens obtains as many patents as its US-based competitors, the patent department must make sure Siemens employees are motivated to patent everything they develop.  According to Körber, they will not be sufficiently motivated, if they can not obtain a valid patent at home as well as in the US.

#Kws: Körber's patent department developped various various incentive systems for motivating Siemens employees to patent what they can, and Körber writes that they have proven a great success.  He does not explain why incentive systems alone could not be enough of a motivation in those cases, where a patent can be obtained only in the US.  Given the global nature of Siemens and of the communication standard negotiations for which Siemens needs the US patents, it is hard to believe that the motivation gap cannot be overcome.  It is however clear that being able to file patents at home makes life somewhat easier for the Siemens patent department.

#CWW: In general, global monoculture is very attractive for executives of global companies.  Just as they like to %(q:standardise on Microsoft), they also tend to orient their company toward one national patent system, viewing all others as cost-generating deviations.  There can be little reason for the corporate patent deparment not to orient its work to the most expansive regional patent system, especially when that is found in a country like the US which has been acting as the world's trend setter in many areas for many decades.

#Krr: Körber writes that Siemens contributed its part to pushing German caselaw into the direction of software patentability.  In fact, Siemens and IBM have fought various borderline cases through to the hightest courts and obtained rule-setting decisions that extended patentability step by step.  Those companies who might suffer from an infringement lawsuit usually have no interest to push for a rule-setting decision but only in protecting themselves from a particular patent, and they will usually not fight this through to the supreme level of jurisdiction.  Before patent offices and patent courts, the voice of patentees is heard earlier and more prominently than the voice of occasional %(q:infringers) and %(q:opponents).  The voice of the general public is not heard at all.  This has even led patent law scholars to formulate a principle of %(q:in dubio pro inventore) -- in case of doubt a lawcourt will side with the patentee.

#Att: Moreover, in general it is the patent department that decides a company's policy on patentability.  And, as Körber points out, thanks to the recent extension of patentability, the patent department of Siemens has become very important.  It is no longer a mere service facility but a center of enterpreneurial activities.  It is a well known reality that any department will tend to opt in favor of those principles that allow its representatives to dominate the organisation.  Whether these are good for the organisation as a whole is only a secondary matter.

#FWt: Similar priniciples apply to patent offices and patent lawcourts.  The more patents they grant, the more important they become.

#Ple: Patent inflation has been pushed ahead at the level of caselaw, and legislatures were, if consulted at all, usually just lobbied to rubberstamp the established facts created by lawcourts and patent offices.

#Lal: Lawcourts and patent offices have for some decades consistently rejected attempts from companies and individuals to obtain patents on non-technical mental achievements such as %(q:rules of computation and organisation).

#Es6: Even in the US the patent office stood up against this pressure and bravely refused software patents throughout the 1960s and (in part) 70s.  In Germany they persisted until 1992.

#DWc: During all these years, the courts faced pressure from only one side:  the applicant, a dissatisfied large corporation and associated published opinion in patent law journals.  The other side, the large public which needs to preserve its freedom of ideas, was not represented in the courtroom.

#Iew: In patent offices, the matter was even worse.

#Iet: In the late 80s and 90s, in the wake of a %(q:lean administration) campaign, public administrations were exhorted to behave like service agencies rather than like wielders of state power, i.e. to consider citizens as their %(q:customers) rather than their subjects.  While this may be an attractive metaphor for citiziens who have been subjected to long waiting and unfriendly treatment by bureaucrats, it is essentially misleading, because the public administration does have to consider the whole public interest and not just the perspective of the visible %(q:customer) with whom it happens to be dealing.

#PWt: Patent offices took the exhortation to treat applicants as its %(q:customers) very seriously and literally.

#Iti: It became commonplace in speeches and publicized slogans to portray the patent office as an agency whose sole aim it is to deliver patents to its customers, the applicants.

#Toh: The president of the US patent office even connected this to the following mission statement:

#Wfn: We are the patent office, not the rejection office.

#WjW: When one customer has, even by chance, received particularly friendly treatment by the service provider, the other customers usually want a %(q:most-favored-customer status) for themselves.  There is no way back.  Service quality can only be improved, not cut back.  This inevitably leads to patent inflation.

#Wso: When a national patent administration decides to loosen the standards of patentability, this can create a short-term rush to the patent office and thus stimulate business activity and even R&D investments, such as e.g. in gene sequencing.

#Ivs: In the long-term, the gold-rush may well calm down and give way to a suffocating situation of monopolism, which discourages business and investment in an over-patented area.

#Bnm: But for politicians who want to win the next election, the short-term perspective is regularly more interesting.

#Twu: Thus patent inflation policies can always expect to get support in the incumbent administration of the country.

#TWs: This adds to the already formidable power that patent expert lobbies tend to wield in various departments of governmental administrations.

#IjW: It has been suspected that at several junctures in recent history, including the Plaza conference of 1985 and the Uruguay round 1992-3, the patentability screw was loosened precisely for this reason.

#Wiv: When it can be expected that patentability standards will soon erode, it is advantageous for a country to move first.

#Tti: The first-moving country will be able to mobilise its local industry to get most of the basic patents in the area it has just opened up to patentability.

#TWe: This policy is especially attractive for a country like the US, which has for a long time acted as an independent trend-setter whom the whole world tends to imitate.

#Fhu: For a country like Japan, which does not have this record, such a policy is not an option.

#Moa: Moreover, since Japanese and, to a slightly lesser extent, European politicians, tend to see themselves as followers of a pre-conceived global trend rather than as originators of any policy, it is highly likely that Japan and Europe, and thereby the whole world, will compete to be rapid second-movers to any patent inflation decision that the US takes.

#Tua: The US reaps the full first-mover advantage, and by the time Japan and Europe have come along, the markets are already monopolised to the advantage of the US, as far as this really constitutes an advantage for anybody.

#TWn: The system can as a whole then serve to keep newly rising tiger states and under-developped countries at bay.

#IWT: It seems that in the 80s this was in fact one of the political considerations in the US which gave rise to the %(q:pro patent) movement.  It was found that market economics alone no longer was able to secure high wages and a privileged position of the west.  Especially the rise of Japan and East-Asian %(q:tiger states) was seen as a threat.  Therefore, under the name of free trade, a new network of informal trade barriers had to be established, and a snowball system starting from the USA was the natural way to establish it.

#Tse: The patent system has been driven by a specialised set of organisations, including patent offices, legal departments of large companies, patent lawyers and, to a lesser extent, so called inventors' organisations.

#Teb: These people have mainly determined the policies of the patent system by an internal discussion among their own.  There were certain phases in history when a larger public took interest in the patent system, but in recent decades the patent world was largely on its own and free to indulge in mental in-breeding.

#Msv: Mental in-breeding generally leads to a departure from generally valid philosophical concepts into an esoteric language, an elevation of what had once been auxiliary concepts to the level of central concepts, confusion of means and ends, formalisation of decision methodologies.

#Foh: Formalised methodologies work great for ensuring that a BigMac will be the same everywhere in the world.  They may also help make patent granting decisions predictable in an environment where independent thinking by patent examiners can not be taken for granted.  But the predictability benefits mainly those who take advantage of the formalisms in order to push unmerited patents through the meshes of the net and thereby gradually render this net useless.

#Aem: An striking example of formalisation can be found in the approaches of the EPO to make patentability more and more dependent on the claim language and certain formalised rules of how to interpret this claim language  (e.g. what %(q:comprising) vs %(q:consisting) should be deemed to mean), as well as certain sequences of how the validity of claims should be tested.  The EPO has meanwhile become so preoccupied with these esoteric secondary concepts that it tends to lose sight of what it is really granting patents for.

#Iml: In contrast to current EPO jurisprudence, the German law before 1978 used more general concepts.  The law encouraged examiners to disregard the claim language and instead analyse the %(q:general inventive thought) (allgemeiner Erfinungsgedanke).  This lead to stronger and fewer patents.  On the one hand it was less easy to circumvent a patent by a method that formally fell outside the claim scope, e.g. by creating a product that did not literally %(q:consist of) the claimed ingredients but used the same teaching.  On the other, the teaching itself was isolated more stringently from the prior art and tested equally stringently for its physical substance.  There was no way of patenting abstract rules of calculation by dressing them up in some claim language that muddled the borders between the prior art and the allegedly new teaching.  Likewise examiners found it easier to reject an application as obvious, since there was no formalised procedure of examining %(q:inventive step).

#Cdn: Currently patent law in Europe seems to have reached a climax of formalisation.  Patent law scholars have, unlike in the 70s, plunged deeply into procedural concepts and become incapable of arguing in general philosophical terms in a form that could claim to have any validity or value outside the narrow circle of patent lawyers.  They are pressing to increasingly impose this result of intellectual in-breeding on European citizens.  The process is similar to the gradual de-functionalisation and self-orientation which can be observed in many large organisations in history.  Expansive juggernauts of this type wil stop only, when they meet a strong external opposition.  By that time, they may be in such bad shape that they suddenly collapse.

#Ote: One reason for the degradation of patentability standards may lie in the fact that after centuries of progress mankind has already gone to the limits of harnessing the forces of nature and not many real inventions are left for us to make.  In order to continue keep the patent business working, it becomes necessary to grant patents for every trifling idea and shade of a shade of an invention.   Patent departments of large companies start shooting real fireworks of trivial inventions and building card-houses from thousands of petty little patents.   For a normal small inventor, such patents would tend to be unenforcable and worthless.   But in an area of complex systems, where most of these patents just consists in a logical functionality, it may become possible to turn a large portfolio into a powerful weapon for deterring newcomers, exacting taxes and keeping up a certain advantage in the market.  Although this situation is highly unsatisfactory for the market as a whole, it may rationally be the only way out for certain forces of inertia who want to keep the wheels of the patent system rolling on at normal pace.  The result is however that the number of patents explodes and the system is inflated.

#Esi: EPA-Statistiken über Lizenzeinnahmen

#snt2: Laut diesem Bericht vom Europäischen Patentamt sind die weltweiten Lizenzeinnahmen aus Patenten von 1990 bis 2000 von 10 auf 100 Milliarden USD gestiegen.  Da empirischen Studien zufolge die Forschungs- und Entwicklungstätigkeit im gleichen Zeitraum nicht wesentlich zugenommen hat, können wir die Patentinflationsrate somit auf 1000% in 10 Jahren oder 26% pro Jahr beziffern.

#snt: Thanks to Prof. Lenz for the %(kl:hint).

#csW: enthält detaillierte Angaben über die Stadien des Verfalls der Patentierbarkeitsstandards in Europa in den letzten Jahrzehnten.

#EuW: Erich Bieramperl about the exhaustion of inventive ressources

#Pro: Phenomena of patent inflation observed from the perspective of a long-time individual inventor and holder of patents in the measuring technology field.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpattisna ;
# txtlang: de ;
# multlin: t ;
# End: ;

