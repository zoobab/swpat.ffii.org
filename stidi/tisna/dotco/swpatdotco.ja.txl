<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Dcs: Geschichte der Patentinflation in Deutschland

#DWW: Das Patentsystem konnte 1877 in Deutschland nur deshalb gegen eine
breite Front der Ablehnung seitens der Volkswirte durchgesetzt werden,
weil es damals vorsichtig auf einen Kernbereich eingegrenzt wurde, in
dem es wenig Schaden und, zumindest laut Argumentation des Deutschen
Patentvereins und seines rührigen Vorsitzenden Werner v. Siemens,
einigen volkswirtschaftlichen Nutzen stiftet:  Erfindungen mit
%(s:industrieller Anwendung), wobei nicht nur abstrakte
Geistestdtigkeiten sondern auch Landwirtschaft, Bergbau uvm
ausgeschlossen wurden.  Später wurde präzisiert, die patentierte
Problemlösung müsse mit dem Einsastz von Naturkräften stehen und
fallen.  Zugleich entwickelte sich das Patentwesen schrittweise vom
%(q:preußischen Patentverhinderungssystem) zu einer zunehmend
verselbstständigten und im allgemeinen Moralbewusstsein verankerten
Eigentumsform.

#PWw: Patentkonservativismus als Kompromiss zwischen Volkswirten und
Patentbewegung

#Eeg: Erste Ausdehnungen und Gegenbewegungen 1877-1960

#Alg: Ausbau und Verfall des Technikbegriffs 1960-2000

#Wrk: Weitere Lektüre

#Iia: In der fertigenden Industrie arbeiten nur relativ wenige
Gewerbetreibende, deren Produktentwicklung mit hohem organisatorischen
Aufwand und großen Ausrüstungsinvestitionen verbunden ist.  Diese
Industriebetriebe halten ihr Wissen geheim, um die Früchte ihrer
Entwicklungsarbeit vor Trittbrettfahrern zu schützen.  Dauerhafte
Geheimhaltung ist noch weniger wünschenswert als Monopole, und zu
schnelles Bekanntwerden könnte von manchen großen Investitionen
abschrecken.  Das Patentwesen hilft hier mit einem Handel zwischen dem
Erfinder und der Allgemeinheit ab:  man gewährt dem Erfinder ein
befristetes Monopol als Belohnung dafür, dass er sein Wissen
veröffentlicht.  Die Öffentlichkeit bekommt etwas anstelle von nichts.
 Sie verzichtet zeitweilig auf eine Freiheit, die sie ohne das Patent
wahrscheinlich gar nicht gehabt hätte.  Mit diesem kühlen
wirtschaftspolitischen Kalkül konnte Werner Siemens mit seinem
%(q:Deutschen Patentschutzverein) die Regierung in einem Moment der
Wirtschaftskrise überzeugen, als die Delbrücksche Freihandelspolitik
diskreditiert war und stattdessen Zollschutz und staatlicher
Interventionismus angesagt waren.

#USP: Um die Skepsis der Gegner zu überwinden, propagierten Siemens und sein
Patentschutzverein ein streng auf wirtschaftspolitischer
Zweckmäßigkeit aufbauendes Patentkonzept.  Das Patentwesen sollte ein
Mittel zur Steuerung des Wettbewerbs zwischen großen
Industrieunternehmen sein.  Von Versuchen naturrechtlicher Begründung
eines %(q:geistigen Eigentums) sowie moralisierender
%(q:Erfinder-Romantik) wurde abgesehen. Als Patentinhaber waren die
wirtschaftlich verantwortlichen Kollektive, nämlich die
Industrieunternehmen, vorgesehen.  Ein persönlicher Erfinder musste
nicht genannt werden.  Schon eine hohe Preishürde von 15000 Reichsmark
sorgte dafür, dass das deutsche Patentwesen selten von Privatpersonen
in Anspruch genommen wurde.  Ferner wurde es per Gesetz auf den
Bereich der produzierenden Gewerbe eingeengt:

#Lxg2: Der Entwurf beschränkt die Patentfähigkeit auf solche Erfindungen,
welche eine gewerbliche Verwertung gestatten.  Eine derartige
Verwendung kann bestehen in der gewerbsmäßigen Herstellung des
erfundenen Gegenstandes oder in seinem Gebrauch innerhalb eines
gewerblichen Betriebes.  Auf diese Weise sind rein wissenschaftliche
Entdeckungen, die Auffindung unbekannter Naturprodukte, die Entdeckung
unbekannter Produktivkräfte, die Aufstellung neuer Methoden des
Ackerbaues oder Bergbaues usw, die Kombination neuer Pläne für
Unternehmungen auf dem Gebiete des Handels von dem Patentschutze
ausgenommen.  Der Entwurf folgt in dieser Hinsicht den in allen
Staaten ausdrücklich oder durch die Praxis anerkannten Grundsätzen.

#Dst: Das preußische Patentsystem, in dessen Erbe W. Siemens mit seinem
%(q:Patentkonservativismus) stand, wurde von einigen Kritikern als
%(q:Patentverhinderungssystem) kritisiert.  Es arbeitete in einer
Zeit, in der Deutschland ein spektakulärer Aufstieg zu einer führenden
Stellung in Technik und Wissenschaft gelang.  Ob hier ein kausaler
Nexus besteht und wie er beschaffen ist, soll dahingestellt bleiben. 
Das deutsche Patentamt verweist durch sinen Patentatlas gelegentlich
auf Zusammenhänge zwischen Patentdichte und Wohlstand, aus denen
hervorgehen soll, dass die preußischen Stammgebiete weniger reich sind
als Schwaben und andere Gebiete mit einer hemmungsloseren
Patenttradition.

#Nln: Nach der Verabschiedung des Patentgesetzes von 1877 verbreitete sich
unter den Patentanmeldern erste Unzufriedenheit über die
Einschränkungen.  Es entstand u.a. eine %(q:erfinder-romantische)
Reformbewegung, die den persönlichen Erfinder ins Zentrum der
Überlegungen gerückt und damit das Patentwesen aufgewertet wissen
wollte.  Während des Kaiserreiches und der Weimarer Zeit gelang dieser
Bewegung jedoch kein wesentlicher Durchbruch.  Sie fand Unterstützung
bei den Sozialdemokraten und später insbesondere bei den
Nationalsozialisten.  Adolf Hitler machte sich in %(q:Mein Kampf) ihre
Forderungen zu eigen, und 1936 wurden diese Forderungen schließlich
umgesetzt.  Die nationalsozialistischen Patentreformen wurdn nach dem
zweiten Weltkrieg in der Bundesrepublik gegen einigen Widerstand aus
der Großindustrie bestätigt und schlugen sich in dem weltweit
einzigartigen und von vielen als vorbildlich betrachteten
Arbeitnehmer-Erfindergesetz (ArbErfG) nieder, welches dem angestellten
Erfinder starke Druckmittel gegen sein eigenes Unternehmen in die Hand
gibt und daher heute von manchen Unternehmervertretern als
Standortnachteil im Zeitalter der Globalisierung kritisiert wird.

#Zng: Zugleich entwickelte sich seit etwa 1900 eine zunehmend systematische
Theorie zur Abgrenzung der %(q:technischen Erfindungen) von dem
Bereich der Ideen, für den kein patentrechtlicher Monopolschutz zur
Verfügung stehen soll, da dort das Freihaltungsbedürfnis gegenüber dem
Belohnungsinteresse ein höheres Gewicht hat.

#Kjh: Kohler, Piezcker, Lindenmaier und andere Gelehrte definierten
%(q:Technik) als Anwendung von Naturgesetzen.  Diese Formel fand auch
Eingang in das japanische Patentgesetz.  Manche Leute in Japan
behaupten, sie sei während der Zeit der Achse Berlin-Rom-Tokio aus
Deutschland nach Japan importiert worden.  Diese Behauptungen wären
noch näher zu überprüfen.  Klar ist jedoch, dass die Technikdefinion
in Deutschland und Japan expliziter formuliert wurde als in den
meisten anderen Ländern.  In Frankreich gab es unter dem Namen
%(q:industrieller Charakter) (charactère industriel) eine
vergleichbare Begriffsbildung.

#ZWa: Zur Erfordernis des technischen Charakters kamen noch die Begriffe
%(q:Erfindungshöhe) und %(q:Beitrag zum technischen Fortschritt)
(Fortschrittlichkeit) hinzu.  Beide wurden in den 70er Jahren im Zuge
der Gründung des Europäischen Patentamts der Harmonisierung mit
lascheren Patentjurisdiktionen abgeschafft bzw abgeschwächt.  In Japan
besteht die Forderung nach %(q:Fortschrittlichkeit) (進歩性 shinposei)
zumindest als Worthülse noch heute.

#Dlo: Das frühere Erfordernis nach %(q:Fortschrittlichkeit) hatte einen
entscheidenden Vorteil, den man sich gerade im Umfeld der heute
verbreiteten untechnischen Patente zurückwünscht:  eine
Umgehungslösung war früher nicht patentierbar.  Es musste gezeigt
werden, dass die Lösung im Vergleich zu vorbekannten, auch
patentierten, Problemlösungen wesentliche technische Vorteile aufwies.
 Nur insoweit sie das tat, kam sie als patentierbare technische Lehre
in Betracht.  So konnte z.T. dafür gesorgt werden, dass das
Patentdickicht um ein Problem herum noch etwas durchlässig bleiben
konnte.

#WWs: War die Politik des Dritten Reiches relativ patentfreundlich gewesen,
so verstärkte sich nach dem Kriege zeitweilig wieder die
patentskeptische Tendenz.  Die Freiburger Schule der ordoliberalen
Volkswirtschaftslehre (Hayek u.a.) wandte sich ebenso gegen das
Patentsystem wie einflussreiche Wirtschaftswissenschaftler in aller
Welt, allen voran der amerikanische Österreicher Fritz Machlup, der
1958 in einem Bericht an den US-Kongress eine kenntisreiche und
vernichtende Bilanz des Patentwesens zog.  Für Machlup war das
Patentwesen als Mittel zur Förderung des technischen Fortschrittes auf
ganzer Linie gescheitert und er charakterisierte es als eine
%(q:Bewegung der Juristen und Protektionisten gegen die Volkswirte).

#D0n: Diese Well der Kritik am Patentwesen begünstigte eine erneute
Verstärkung der Traditionen des %(q:preußischen
Patentverhinderungssystems), welche jedoch in den 70er Jahren wieder
abebbte und in den 80er Jahren zunächst in den USA durch eine
entgegengesetzte, erneut von Patentjuristen und Protektionisten
getriebene Welle namens %(q:Pro Patent) getrieben wurde.  Diesmal ging
es u.a. darum, den kometenhaften Aufstieg von Japan und einigen
ostasiatischen %(q:Tigerstaaten) zu bremsen.

#Trv: Trotz scheibchenweiser Abstriche am %(q:preußischen
Patentverhinderungssystem) (etwa bei der %(q:Fortschrittlichkeit))
konnte in den 70er Jahren der BGH den Technikbegriff noch einmal zu
einem Höhebunkt an Klarheit führen.  Man lese dazu die einschlägigen
Kapitel in Kraßers %(pl:Lehrbuch des Patentrechts) von 1986 und
Benkards %(pk:PatG-Kommentar) von 1988.

#Nsi: Nach Meinung einiger Kritiker der %(q:stockkonservativen)
preußisch-deutschen Patenttradition begann mit der Gründung des
Europäischen Patentamtes ein heilsamer %(q:frischer Wind) den
%(q:deutschen Mief) wegzuwehen.  Das europäische Patentamt hat zwar
den deutschen Technikbegriff in seine Prüfungsrichtlinien von 1985
übernommen und auch weitgehend angewendet, und dieser Begriff liegt
auch dem Europäischen Patentübereinkommen von 1973 zugrunde.  Aber die
Lehre von der Technischen Erfindung war schon innerhalb der deutschen
Patentrechtsgemeinde ein Spezialgebiet gewesen, welches nur eine
Minderheit der Rechtsgelehrten voll verstand und würdigte.  Viele
andere sahen darin nicht mehr als eine altmodisches und
geschäftsschädigendes Erbe des 19. Jahrhunderts.  Mit der
Internationalisierung des Patentwesens nahm diese Sichtweise an
Gewicht zu.  Soweit man aus Veröffentlichungen von EPA-Richtern
unschwer erkennen kann, haben maßgebliche Entscheider den
Technikbegriff weder verstanden noch gewürdigt.  Der seit 1986 vom EPA
bezüglich der Computerprogramme eingeschlagene %(sk:Schlitterkurs) ist
weitgehend auf Unverständnis zurückzuführen.  Selbstverständlich kam
dieses Unverständnis dem EPA nicht ungelegen, erlaubte es doch eine
beträchtliche Erweiterung des Bereiches der Patentierbarkeit und damit
der Einnahmen des EPA, welches, anders als das Deutsche Patentamt, wie
ein Wirtschaftsunternehmen finanziell völlig autonom agiert.

#Dat: Der Kommentator Benkard und eine Reihe wackerer Patentjuristen
äußerten Verwunderung und z.T. herbe Kritik am Kurs des EPA.  Doch
nicht nur vom EPA sondern auch aus den USA und von internationalen
Patentrechtler-Organisationen wie AIPPI wehte ein starker Wind in
Richtung Patentinflation.  Es entstand ein Klima, in dem Kenner des
Gesetzes und des Technikbegriffes wie Benkard, Kraßer, Kolle u.a.
zunehmend resignierten und andere ruhig blieben.  Stattdessen schlug
den (ge)wissenlosen Interessenvertretern ihre Stunde.  Patentanwälte,
die sich auf Software spezialisiert hatten, gaben in der öffentlichen
Diskussion in GRUR und anderen Zeitschriften den Ton an und bestimmten
die Position großer Verbände, Kammern und Institute.   Unter dem
Eindruck dieses Zeitgeistes fanden sich schließlich auch wagemutige
BGH-Richter bereit, zunächst unauffällig vom Gesetz wegzuirren und
später, unter Berufung auf diese Fehlurteile, eine neue Rechtsdoktrin
zu etablieren, die sich nicht mehr im Rahmen des Gesetzes bewegt.  Mit
den Entscheidungen %(q:Seitenpuffer), %(q:Tauchcomputer) und
schließlich %(q:Sprachanalyse) und %(q:Logikverifikation) schlug sich
der BGH auf die Seite der Gesetzesverächter, ohne dass die noch
geltende Gesetzesregelung in irgendeiner Weise durch Widersprüche oder
Unklarheiten dazu Anlass geboten hätte.  Die BGH-Richter machten das
wahr, wovor ihre Vorgänger 1976 hellsichtig gewarnt hatten:

#Sas: Stets ist aber die planmäßige Benutzung beherrschbarer Naturkräfte als
unabdingbare Voraussetzung für die Bejahung des technischen Charakters
einer Erfindung bezeichnet worden. Wie dargelegt, würde die
Einbeziehung menschlicher Verstandeskräfte als solcher in den Kreis
der Naturkräfte, deren Benutzung zur Schaffung einer Neuerung den
technischen Charakter derselben begründen, zur Folge haben, dass
schlechthin allen Ergebnissen menschlicher Gedankentätigkeit, sofern
sie nur eine Anweisung zum planmäßigen Handeln darstellen und kausal
übersehbar sind, technische Bedeutung zugesprochen werden müsste.
Damit würde aber der Begriff des Technischen praktisch aufgegeben,
würde Leistungen der menschlichen Verstandestätigkeit der Schutz des
Patentrechts eröffnet, deren Wesen und Begrenzung nicht zu erkennen
und übersehen ist.

#Eii: Es ließe sich ferner mit guten Gründen die Auffassung vertreten, dass
angesichts der Einhelligkeit, mit der Rechtsprechung und Literatur
seit jeher die Beschränkung des Patentschutzes auf technische
Erfindungen vertreten haben, von einem gewohnheitsrechtlichen Satz
dieses Inhalts gesprochen werden kann.

#Deu: Das mag aber letztlich dahinstehen. Denn der Begriff der Technik
erscheint auch sachlich als das einzig brauchbare Abgrenzungskriterium
gegenüber andersartigen geistigen Leistungen des Menschen, für die ein
Patentschutz weder vorgesehen noch geeignet ist. Würde man diese
Grenzziehung aufgeben, dann gäbe es beispielsweise keine sichere
Möglichkeit mehr, patentierbare Leistungen von solchen zu
unterscheiden, denen nach dem Willen des Gesetzgebers andere Arten des
Leistungsschutzes, insbesondere Urheberrechtsschutz, zuteil werden
soll. Das System des deutschen gewerblichen und Urheberrechtsschutzes
beruht aber wesentlich darauf, dass für bestimmte Arten geistiger
Leistungen je unterschiedliche, ihnen besonders angepasste
Schutzbestimmungen gelten und dass Überschneidungen zwischen diesen
verschiedenen Leistungsschutzrechten nach Möglichkeit ausgeschlossen
sein sollten. Das Patentgesetz ist auch nicht als ein Auffangbecken
gedacht, in welchem alle etwa sonst nicht gesetzlich begünstigten
geistigen Leistungen Schutz finden sollten. Es ist vielmehr als ein
Spezialgesetz für den Schutz eines umgrenzten Kreises geistiger
Leistungen, eben der technischen, erlassen und stets auch als solches
verstanden und angewendet worden.

#Eet: Es verbietet sich demnach, den Schutz von geistigen Leistungen auf dem
Weg über eine Erweiterung der Grenzen des Technischen -- die auf deren
Aufgabe hinauslaufen würde -- zu erlangen. Es muss vielmehr dabei
verbleiben, dass eine reine Organisations- und Rechenregel, deren
einzige Beziehung zum Reich der Technik in ihrer Benutzbarkeit für den
bestimmungsgemäßen Betrieb einer bekannten Datenverarbeitungsanlage
besteht, keinen Patentschutz verdient. Ob ihr auf andere Weise, etwa
mit Hilfe des Urheber- oder des Wettbewerbsrechts, Schutz zuteil
werden kann, ist hier nicht zu erörtern.

#Anh: Ausführliche Dokumentation der Geschichte des Patentwesens von den
Anfängen bis zum 20. Jahrhundert.  Derzeit weiterhin in Entwicklung.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: jpsmets ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatdotco ;
# txtlang: ja ;
# multlin: t ;
# End: ;

