<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Testsuite für die Gesetzgebung über die Grenzen der Patentierbarkeit

#descr: Um eine Patentierbarkeitsrichtlinie auf Tauglichkeit zu prüfen,
sollten wir sie an Beispiel-Innovationen ausprobieren.  Für jedes
Beispiel gibt es einen Stand der Technik, eine technische Lehre und
eine Reihe von Ansprüchen.  In der Annahme, dass die Beispiele
zutreffend beschrieben wurden, probieren wir dann unsere neue
Gesetzesregel daran aus.  Unser Augenmerkt liegt auf (1) Klarheit (2)
Angemessenheit:  führt die vorgeschlagene Regelung zu einem
vorhersagbaren Urteil?  Welche der Ansprüche würden erteilt? 
Entspricht dieses Ergebnis unseren Wünschen?  Wir probieren
verschiedene Gesetzesvorschläge an der gleichen Beispielserie
(Testsuite) aus und vergleichen, welches am besten abschneidet.  Für
Programmierer ist es Ehrensache, dass man %(q:die Fehler beseitigt,
bevor man das Programm freigibt) (first fix the bugs, then release the
code).  Testsuiten sind ein bekanntes Mittel zur Erreichung dieses
Ziels.  Gemäß Art. 27 TRIPS gehört die Gesetzgebung zu einem
%(q:Gebiet der Technik) namens %(q:Sozialtechnik) (social
engineering), nicht wahr?  Technizität hin oder her, es ist Zeit an
die Gesetzgebung mit derjenigen methodischen Strenge heran zu gehen,
die überall dort angesagt ist, wo schlechte
Konstruktionsentscheidungen das Leben der Menschen stark
beeinträchtigen können.

#Sap: Einige Beispielpatente

#aea: Beispiel-Ansprüche

#Qoe: Fragen, die für jedes Patent zu beantworten sind

#CrW: Vergleichstabelle

#LWW: Zu prüfende Kandidaten: Gesetzesvorschläge von EUK/BSA, FFII/Eurolinux
u.a.

#wna: New Patent Database

#Ptn: Patent

#nrh: Anspruch 1

#nrn: Anmerkung

#ep266049: Lauflängenkodierung

#ep803105: Netzwerk-Verkaufssystem

#ep287578: Audio-Kodierung

#ep538888: Lesegebühren

#ep689133: Paletten mit Reitern

#ep101552: Fe-B-R-Magnet

#Att: An alloy comprising Fe-B-R characterized by containing at least one
stable compound of the ternary Fe-B-R type, which compound can be
magnetized to become a permanent magnet at room temperature and above,
where R stands for at least one rare earth element inclusive yttrium.

#Wld: This is the strongest known magnet.  Sumitomo found it by
experimentation in the early 1980s.  Subsequent experiments failed to
turn out anything nearly as strong.  No mobile phone can be
competitive today without this magnet.

#lrt: Klarheit

#dqa: Angemessenheit

#Aet: Angenommen der Stand der Technik und der Beitrag sind hier richtig
offenbart, wie müsste ein Richter die folgenden Fragen beantworten,
wenn das vorgeschlagene Gesetz in Kraft träte?

#ItW: Liegt eine patentfähige Erfindung (Beitrag / Lehre) vor?  Warum
(nicht)?

#WWW: Würde jeder Richter zu den gleichen Ratschlüssen kommen?  Wo liegen
Ermessensspielräume und Unsicherheitsbereiche?

#Atn: Sind diese Ergebnisse angemessen?

#lfe: In wieweit würden sie die Innovation anspornen/bremsen?

#ujW: In wieweit würden sie sonstigen ordnungspolitischen Zielen, wie z.B.
den in den Römischen Verträgen oder in e.Europe beschriebenen,
entsprechen?

#Wim: Was für ein Aufwand ist erforderlich, um zu der beanspruchten
Innovation zu gelangen?  Was für ein Aufwand ist nötig, um die
Innovation nachzuahmen, ohne Urheberrechte zu verletzen?  Wie
verhalten sich beide zueinander (Kostenverhältnis Innovation zu
Imitation)?  Was für ein Aufwand ist nötig, um eine typisches
Gesamtsystem zu entwickeln (z.B. Software-Applikation, eingebettetes
System), in der die beanspruchte Innovation als Bestandteil vorkommt? 
Wie verhält sich dieser Aufwand zum Innovationsaufwand
(Kostenverhältnis Innovation zu Entwicklung)?

#Wtf: Welches andere Recht könnte angemessen sein, falls Patente für zu
stark und das Urheberrecht für zu schwach erachtet werden sollte? 
Gebrauchsmuster?  %(sb:maßgeschneidertes Vorrecht / Vergütungsrecht)?

#tta: Prüfling

#imW: Kostenverhältnis Neuerung zu Nachahmung

#idW: Kostenverhältnis Neuerung zu Entwicklung

#oWc: sonstige Indikatoren

#sbn: sollte patentierbar sein?

#ibW: Erfindung nach %(sa:Messlatte A)?

#ibI: Erfindung nach %(sb:Messlatte B)

#wve: Kandidat für maßgeschneidertes Eigentumsrecht

#nepat: %1 keine technische Erfindung

#jespat: %1 ist technische Erfindung

#hkt: check item

#ilW: Klare Zweckbestimmung in Präambel

#oma: Keine Informationsansprüche

#bie: Garantie der Veröffentlichungsfreiheit

#afu: Garantie des (urheberrechtlichen) Eigentums an Software

#oie: Software Teil der Offenbarung

#eii: Interoperabilitätsprivileg

#ooW: %(q:Technik) definiert

#vfW: Negative Definition der %(q:Technischen Erfindung)

#eof: durch Bezug auf Naturkräfte

#aoW: 4 Getrennte Prüfungen für 1 Gegenstand

#eWu: Erfindung = Beitrag = neuer und technischer Kern

#opo: EPA-Soziolekt entrümpelt, tendenziöse Begriffe wie
%(q:computer-implementierte Erfindung) gemieden oder neu definiert.

#Wtd: Überwachung der Durchführung

#lun: Eines der Pamflete ist eine 1-Seiten-Version der Testsuite.  Es werden
zwei Beispielansprüche und ein vereinfachter Satz von Fragen
präsentiert.  Bislang sind die Verfechter der Softwarepatente, wie
etwa MdEP Arlene McCarthy und Dr. Joachim Würmeling, eine Antwort auf
diese Fragen schuldig geblieben.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatmanri ;
# txtlang: de ;
# multlin: t ;
# End: ;

