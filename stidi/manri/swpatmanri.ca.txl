<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Banc de proves de sèries d'assaigs de legislació de patentabilitat

#descr: Per tal d'assajar una proposta de llei, la provem amb un conjunt de
mostres d'innovacions.  Per cada innovació, donem una descripció de
l'art previ, una descripció de l'invent declarat i un petit conjunt de
reivindicacions.  Donat que les descripcions són correctes, assagem la
nostra legislació proposada en ell.  El focus està en la claredat i
l'adequació:  condueix la regla proposada cap a un veredicte
predecible?  Quina de les reivindicacions acceptarem, si n'acceptem
alguna?  Aquest resultat és el que volíem?  Provem d'altres propostes
de llei amb la mateixa sèrie d'assaigs i veiem quina obté la millor
puntuació.  Els professionals del programari creuen que hauries de
%(q:primer fixar les errades, després alliberar el codi).  Les sèries
d'assaigs són una manera habitual d'aconseguir-ho.  La legislació és
un camp de la tecnologia anomenat %(q:enginyeria social), oi? 
Tecnologia o no, ja és hora d'encarar la legislació amb el mateix
rigor metodològic aplicable on sigui que decisions mal dissenyades
puguin afectar significantment la vida de la gent.

#Sap: Algunes mostres de patents

#aea: Sample Claims

#Qoe: Preguntes per respondre per cada

#CrW: Comparison Table

#LWW: Legislacions candidates a ser assajades

#wna: New Patent Database

#Ptn: Patent

#nrh: Claim 1

#nrn: Comment

#ep266049: Runlength Coding

#ep803105: Network Sales System

#ep287578: Audio Coding

#ep538888: Pay per Read

#ep689133: Tabbed Palettes

#ep101552: Fe-B-R Magnet

#Att: An alloy comprising Fe-B-R characterized by containing at least one
stable compound of the ternary Fe-B-R type, which compound can be
magnetized to become a permanent magnet at room temperature and above,
where R stands for at least one rare earth element inclusive yttrium.

#Wld: This is the strongest known magnet.  Sumitomo found it by
experimentation in the early 1980s.  Subsequent experiments failed to
turn out anything nearly as strong.  No mobile phone can be
competitive today without this magnet.

#lrt: Clarity

#dqa: Adequacy

#Aet: Donat que l'art previ i la contribució estan correctament revelades,
com s'haurien de respondre les següents preguntes, si la llei
proposada estés en vigor:

#ItW: Hi ha una contribució patentable (invent / ensenyament)?  Per què
(no)?

#WWW: Algun jutge arribaria a les mateixes conclusions?  On hi ha àrees
d'incertesa?

#Atn: Són adequades aquestes conclusions?

#lfe: Fins a on haurien de promoure/reprimir l'invent?

#ujW: Fins a on haurien de coincidir amb la política d'objectius públics,
com els citats al Tractat de Roma, a e.Europa, etc?

#Wim: What effort is needed to arrive at the claimed innovation?  What
effort is needed to imitate the claimed innovation without violating
copyright?  How does this compare to the innovation effort (innovation
vs imitation cost ratio)?  What effort is needed to develop and
distribute an average system (e.g. software application, embedded
system) of which the claimed innovation would typically be a part? 
How does this compare to the innovation effort (innovation vs
development cost ratio)?

#Wtf: What special right might be adequate in case patents are deemed too
heavy and copyright too light?  Utility cerfiticate?  %(sb:Specially
tailored innovator's privilege / reward)?

#tta: test sample

#imW: innovation vs imitation cost ratio

#idW: innovation vs development cost ratio

#oWc: other indicators

#sbn: should be patentable?

#ibW: invention by %(sa:standard A)?

#ibI: invention by %(sb:standard B)?

#wve: candidate for sui generis property

#nepat: %1 not a technical invention

#jespat: %1 is a technical invention

#hkt: check item

#ilW: clear goal statement in preambule

#oma: No Information Claims

#bie: Publication Freedom Guarantee

#afu: Software Property (copyright) Guarantee

#oie: Software on the Disclosure side

#eii: Interoperability privilege

#ooW: %(q:Technical) defined

#vfW: Negative Definition of %(q:Technical Invention)

#eof: by reference to forces of nature

#aoW: 4 Separate Tests on 1 Unified Object

#eWu: Invention = Contribution = novel and technical core

#opo: EPO slang debugged, biased terminology such as
%(q:computer-implemented invention) etc eliminated or redefined

#Wtd: Supervision of Execution

#lun: One of the pamphlets is a one-page version of the test suite.  Two
example claims are presented to politicians with a simplified set of
questions.  So far the proponents of software patents, such as Arlene
McCarthy, have failed to answer.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatmanri ;
# txtlang: ca ;
# multlin: t ;
# End: ;

