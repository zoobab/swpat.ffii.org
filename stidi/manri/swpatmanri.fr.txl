<meta http-equiv=Content-Type content=text/plain; charset=utf-8>

#title: Ensemble de tests pour la législation sur les limites de la
brevetabilité

#descr: Pour tester la capacité d'un loi sur la brevetabilité, nous devons
essayer des innovations exemples.  Chaque exemple est décrit par un
état de la technique, un enseignement technique et une série de
revendications.  Dans l'hypothèse que ces déscriptions sont
pertinentes, nous essayons notre nouvelle règle de loi.  Notre
attention se porte sur (1) la clarté (2) l'éffet macro-économique du
resultat:  la réglementation proposée mène-t-elle à une décision
prévisible?  Quelles revendications seront acceptées?  Ce résultat
exprime-t-il nos souhaits?  Nous essayons différentes propositions de
lois sur la même série d'exemples (Testsuite) et comparons lesquelles
réussissent le mieux.  Pour un programmeur c'est une question
d'honneur que de %(q:supprimer les erreurs avant de diffuser le
programme) (first fix the bugs, then release the code).  Les ensembles
de tests sont un moyen connu pour atteindre ce but.  D'après l'article
27 ADPIC (TRIPS) la législation appartient à un %(q:domaine de la
technique) notamment %(q:d'ingénierie sociale) (social engineering),
n'est-ce pas ?  Technicité ici ou là, il est temps d'aborder de ce
côté la législation avec cette rigueur méthodique, qui est partout
annoncée, où les mauvaises décisions de construction peuvent fortement
porter atteinte à la vie des individus.

#Sap: Quelques exemples de brevets

#wna: New Patent Database

#aea: Revendications Exemplaires

#Ptn: Brevet

#nrh: Revendication 1

#nrn: Commentaire

#ep266049: Code de Plage

#ep803105: Système de Vente sur Reseaux

#ep287578: Codage Audio

#ep538888: Frais de Lecture

#ep689133: Tabbed Palettes

#ep101552: Magnet Fe-B-R

#Att: An alloy comprising Fe-B-R characterized by containing at least one
stable compound of the ternary Fe-B-R type, which compound can be
magnetized to become a permanent magnet at room temperature and above,
where R stands for at least one rare earth element inclusive yttrium.

#Wld: This is the strongest known magnet.  Sumitomo found it by
experimentation in the early 1980s.  Subsequent experiments failed to
turn out anything nearly as strong.  No mobile phone can be
competitive today without this magnet.

#Qoe: Questions auxquelles chaque brevet doit répondre

#lrt: Clarté

#Aet: Supposé que l'état de la technique et la contribution sont ici
assurément évidents, comment un juge devrait répondre aux questions
suivantes quand la loi proposée entrerait en vigueur?

#ItW: Une contribution (invention / enseignement) brevetable est-elle
présente?  Pourquoi (pas)?

#WWW: Chaque juge rendra-t-il le même arrêt?  Où se trouvent les latitudes
de jugement et le domain d'incertitude?

#dqa: Désirabilité du Resultat

#Atn: Ces résultats sont-ils appropriés?

#lfe: Dans quelle mesure stimuleront/freineront-ils l'innovation? Quelles
éffêts auront ils sur la compétition? sur les intérêts des
consommateurs?

#ujW: Dans quelle mesure répondent-ils aux divers objectifs politiques,
comme par expl. ceux décrits dans le traité de Rome ou le projet
e.Europe?

#Wim: Qu'est-il nécessaire comme dépenses pour parvenir à l'innovation
revendiquée?  Quelle dépense est nécessaire pour imiter l'innovation,
sans violer de droits d'auteur?  Comment se contiennent les deux
vis-à-vis de l'autre (rapport des coûts entre l'innovation et
l'imitation)?  Quelle dépense est nécessaire pour développer un
système entier typique (par expl. une application, un système
intégré), dans lequel l'innovation revendiquée se présente comme un
élément?  Comment cette dépense retient les dépenses d'innovation
(rapport des coûts entre l'innovation et le développement)?

#Wtf: Quel autre droit peut être approprié, dans le cas où les brevets
doivent être jugés trop forts et le droit d'auteur trop faible?  La
marque déposée?  %(sb:un privilège / droit d'indemnités taillé sur
mesure)?

#CrW: Tableau comparatif

#tta: Candidat

#imW: Rapport des coûts entre l'innovation et l'imitation

#idW: Rapport des coûts entre l'innovation et le développement

#oWc: divers indicateurs

#sbn: doit être brevetable?

#ibW: Invention d'après la %(sa:règle A)?

#ibI: Invention d'après la %(sb:règle B)

#wve: candidat pour droit de propriété sui generis

#LWW: Candidats à essayer: Propositions de loi de CEC/BSA, FFII/Eurolinux et
d'autres

#nepat: %1 n'est pas une invention technique

#jespat: %1 est une invention technique

#hkt: check item

#ilW: préambule avec objectif claire

#oma: Pas de Revendication d'Information

#bie: Garantie de Lilberté de Publication

#afu: Garantie de la Propriété Logicielle acquise par droit d'auteur

#oie: Logiciels sur le conté de Divulgation

#eii: Privilège d'Inteopérabilité

#ooW: Définition de ce qui est %(q:technique)

#vfW: Negative Definition of %(q:Technical Invention)

#eof: par référence aux forces de nature

#aoW: 4 Examens Séparés pour 1 Objèt Unique

#eWu: Invention = Contribution = Noyeaux Nouveaux et Technique

#opo: Terminologie pro-brevet évitée ou dépoisonnée

#Wtd: Contôl de l'Execution

#lun: One of the pamphlets is a one-page version of the test suite.  Two
example claims are presented to politicians with a simplified set of
questions.  So far the proponents of software patents, such as Arlene
McCarthy, have failed to answer.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatmanri ;
# txtlang: fr ;
# multlin: t ;
# End: ;

