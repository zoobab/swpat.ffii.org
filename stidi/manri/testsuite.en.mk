# -*- mode: makefile -*-

testsuite.en.lstex:
	lstex testsuite.en | sort -u > testsuite.en.lstex
testsuite.en.mk:	testsuite.en.lstex
	vcat /ul/prg/RC/texmake > testsuite.en.mk


testsuite.en.dvi:	testsuite.en.mk
	rm -f testsuite.en.lta
	if latex testsuite.en;then test -f testsuite.en.lta && latex testsuite.en;while tail -n 20 testsuite.en.log | grep -w references && latex testsuite.en;do eval;done;fi
	if test -r testsuite.en.idx;then makeindex testsuite.en && latex testsuite.en;fi

testsuite.en.pdf:	testsuite.en.ps
	if grep -w '\(CJK\|epsfig\)' testsuite.en.tex;then ps2pdf testsuite.en.ps;else rm -f testsuite.en.lta;if pdflatex testsuite.en;then test -f testsuite.en.lta && pdflatex testsuite.en;while tail -n 20 testsuite.en.log | grep -w references && pdflatex testsuite.en;do eval;done;fi;fi
	if test -r testsuite.en.idx;then makeindex testsuite.en && pdflatex testsuite.en;fi
testsuite.en.tty:	testsuite.en.dvi
	dvi2tty -q testsuite.en > testsuite.en.tty
testsuite.en.ps:	testsuite.en.dvi	
	dvips  testsuite.en
testsuite.en.001:	testsuite.en.dvi
	rm -f testsuite.en.[0-9][0-9][0-9]
	dvips -i -S 1  testsuite.en
testsuite.en.pbm:	testsuite.en.ps
	pstopbm testsuite.en.ps
testsuite.en.gif:	testsuite.en.ps
	pstogif testsuite.en.ps
testsuite.en.eps:	testsuite.en.dvi
	dvips -E -f testsuite.en > testsuite.en.eps
testsuite.en-01.g3n:	testsuite.en.ps
	ps2fax n testsuite.en.ps
testsuite.en-01.g3f:	testsuite.en.ps
	ps2fax f testsuite.en.ps
testsuite.en.ps.gz:	testsuite.en.ps
	gzip < testsuite.en.ps > testsuite.en.ps.gz
testsuite.en.ps.gz.uue:	testsuite.en.ps.gz
	uuencode testsuite.en.ps.gz testsuite.en.ps.gz > testsuite.en.ps.gz.uue
testsuite.en.faxsnd:	testsuite.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo testsuite.en-??.g3n`;source faxsnd main
testsuite.en_tex.ps:	
	cat testsuite.en.tex | splitlong | coco | m2ps > testsuite.en_tex.ps
testsuite.en_tex.ps.gz:	testsuite.en_tex.ps
	gzip < testsuite.en_tex.ps > testsuite.en_tex.ps.gz
testsuite.en-01.pgm:
	cat testsuite.en.ps | gs -q -sDEVICE=pgm -sOutputFile=testsuite.en-%02d.pgm -
testsuite.en/testsuite.en.html:	testsuite.en.dvi
	rm -fR testsuite.en;latex2html testsuite.en.tex
xview:	testsuite.en.dvi
	xdvi -s 8 testsuite.en &
tview:	testsuite.en.tty
	browse testsuite.en.tty 
gview:	testsuite.en.ps
	ghostview  testsuite.en.ps &
print:	testsuite.en.ps
	lpr -s -h testsuite.en.ps 
sprint:	testsuite.en.001
	for F in testsuite.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	testsuite.en.faxsnd
	faxsndjob view testsuite.en &
fsend:	testsuite.en.faxsnd
	faxsndjob jobs testsuite.en
viewgif:	testsuite.en.gif
	xv testsuite.en.gif &
viewpbm:	testsuite.en.pbm
	xv testsuite.en-??.pbm &
vieweps:	testsuite.en.eps
	ghostview testsuite.en.eps &	
clean:	testsuite.en.ps
	rm -f  testsuite.en-*.tex testsuite.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} testsuite.en-??.* testsuite.en_tex.* testsuite.en*~
tgz:	clean
	set +f;LSFILES=`cat testsuite.en.ls???`;FILES=`ls testsuite.en.* $$LSFILES | sort -u`;tar czvf testsuite.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
