\begin{subdocument}{swpatmanri}{Testsuite f\"{u}r die Gesetzgebung \"{u}ber die Grenzen der Patentierbarkeit}{http://swpat.ffii.org/analyse/testsuite/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Um eine Patentierbarkeitsrichtlinie auf Tauglichkeit zu pr\"{u}fen, sollten wir sie an Beispiel-Innovationen ausprobieren.  F\"{u}r jedes Beispiel gibt es einen Stand der Technik, eine technische Lehre und eine Reihe von Anspr\"{u}chen.  In der Annahme, dass die Beispiele zutreffend beschrieben wurden, probieren wir dann unsere neue Gesetzesregel daran aus.  Unser Augenmerkt liegt auf (1) Klarheit (2) Angemessenheit:  f\"{u}hrt die vorgeschlagene Regelung zu einem vorhersagbaren Urteil?  Welche der Anspr\"{u}che w\"{u}rden erteilt?  Entspricht dieses Ergebnis unseren W\"{u}nschen?  Wir probieren verschiedene Gesetzesvorschl\"{a}ge an der gleichen Beispielserie (Testsuite) aus und vergleichen, welches am besten abschneidet.  F\"{u}r Programmierer ist es Ehrensache, dass man ``die Fehler beseitigt, bevor man das Programm freigibt'' (first fix the bugs, then release the code).  Testsuiten sind ein bekanntes Mittel zur Erreichung dieses Ziels.  Gem\"{a}{\ss} Art. 27 TRIPS geh\"{o}rt die Gesetzgebung zu einem ``Gebiet der Technik'' namens ``Sozialtechnik'' (social engineering), nicht wahr?  Technizit\"{a}t hin oder her, es ist Zeit an die Gesetzgebung mit derjenigen methodischen Strenge heran zu gehen, die \"{u}berall dort angesagt ist, wo schlechte Konstruktionsentscheidungen das Leben der Menschen stark beeintr\"{a}chtigen k\"{o}nnen.}
\begin{sect}{comp}{Einige Beispielpatente}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Europ\"{a}ische Softwarepatente: Umfassende Dokumentation\footnote{http://swpat.ffii.org/patente/txt/index.en.html}}}

\begin{quote}
In den letzten Jahren hat das Europ\"{a}ische Patentamt (EPA) einige 10000 Patente auf abstrakte Rechenregeln erteilt.   Wir sammeln diese Patente und machen sie zug\"{a}nglich.
\end{quote}
\filbreak

\item
{\bf {\bf Europ\"{a}ische Softwarepatente: Einige Musterexemplare\footnote{http://swpat.ffii.org/patente/muster/index.de.html}}}

\begin{quote}
Auf folgende eing\"{a}ngige Beispiele stie{\ss}en wir beim ersten Durchst\"{o}bern unserer Softwarepatente-Tabellen.  Sie wurden fast zuf\"{a}llig ausgesucht und k\"{o}nnen somit als repr\"{a}sentativ f\"{u}r die Ma{\ss}st\"{a}be des Europ\"{a}ischen Patentamtes hinsichtlich Technizit\"{a}t und Erfindungsh\"{o}he gelten.  Wenn sie in irgendeinem Punkt vom Mittelma{\ss} abweichen, dann darin, dass sie relativ leicht einem breiten Publikum verst\"{a}ndlich gemacht werden k\"{o}nnen.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{kver}{Fragen, die f\"{u}r jedes Patent zu beantworten sind}
Angenommen der Stand der Technik und der Beitrag sind hier richtig offenbart, wie m\"{u}sste ein Richter die folgenden Fragen beantworten, wenn das vorgeschlagene Gesetz in Kraft tr\"{a}te?
\begin{enumerate}
\item
Liegt einen patentf\"{a}higer Beitrag (Erfindung / Lehre) vor?  Warum (nicht)? 

\item
Handelt es sich bei dem Beitrag (der Erfindung / der Lehre) um einen technischen?  Warum (nicht)? 

\item
Kommen irgendwelche von den Anspr\"{u}chen zur Erteilung?  Welche?  Warum?

\item
W\"{u}rde jeder Richter zu den gleichen Ratschl\"{u}ssen kommen?  Wo liegen Ermessensspielr\"{a}ume und Unsicherheitsbereiche?

\item
Sind diese Ratschl\"{u}sse angemessen?  In wieweit w\"{u}rden sie die Innovation anspornen/bremsen? In wieweit w\"{u}rden sie sonstigen ordnungspolitischen Zielen, wie z.B. den in den R\"{o}mischen Vertr\"{a}gen oder in e.Europe beschriebenen, entsprechen? 
\begin{itemize}
\item
Was f\"{u}r ein Aufwand ist erforderlich, um zu der beanspruchten Innovation zu gelangen?  Was f\"{u}r ein Aufwand ist n\"{o}tig, um die Innovation nachzuahmen, ohne Urheberrechte zu verletzen?  Wie verhalten sich beide zueinander (Kostenverh\"{a}ltnis Innovation zu Imitation)?  Was f\"{u}r ein Aufwand ist n\"{o}tig, um eine typisches Gesamtsystem zu entwickeln (z.B. Software-Applikation, eingebettetes System), in der die beanspruchte Innovation als Bestandteil vorkommt?  Wie verh\"{a}lt sich dieser Aufwand zum Innovationsaufwand (Kostenverh\"{a}ltnis Innovation zu Entwicklung)?

\item
Welches andere Recht k\"{o}nnte angemessen sein, falls Patente f\"{u}r zu stark und das Urheberrecht f\"{u}r zu schwach erachtet werden sollte?  Gebrauchsmuster?  ma{\ss}geschneidertes Vorrecht / Verg\"{u}tungsrecht\footnote{http://swpat.ffii.org/analyse/suigen/index.de.html}?
\end{itemize}
\end{enumerate}
\end{sect}

\begin{sect}{tab}{Vergleichstabelle}
\begin{center}
\begin{tabular}{|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|}
\hline
Pr\"{u}fling & Kostenverh\"{a}ltnis Neuerung zu Nachahmung & Kostenverh\"{a}ltnis Neuerung zu Entwicklung & sonstige Indikatoren & sollte patentierbar sein? & Erfindung nach Messlatte A (KEG/BSA)? & Erfindung nach Messlatte B (EP\"{U}/FFII) & verdient Sonderrecht X\\\hline
Adobe Patent auf Paletten mit Reitern\footnote{http://swpat.ffii.org/patente/muster/ep689133/index.de.html} & 1 & 0.00001 & \dots & - & + & - & -\\\hline
Pr\"{u}fen von Lernstoff in Schulen\footnote{http://swpat.ffii.org/patente/muster/ep664041/index.de.html} & 0.5 & 0.005 & \dots & - & + & - & -\\\hline
Kodierung Audio\footnote{http://swpat.ffii.org/patente/wirkungen/mpeg/index.de.html} & 2 & 0.05 & \dots & - & + & - & o\\\hline
Fe-B-R Alloy & 10 & 0.5 & \dots & + & + & + & o\\\hline
\dots &  &  &  &  &  &  & \\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{test}{Zu pr\"{u}fende Kandidaten: Gesetzesvorschl\"{a}ge von EUK/BSA, FFII/Eurolinux u.a.}
Benennen Sie bitte weitere Kandidaten!  Wir werden darauf verweisen.

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf EUK \& BSA 2002-02-20: Vorschlag, alle n\"{u}tzlichen Ideen patentierbar zu machen\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/index.de.html}}}

\begin{quote}
Die Europ\"{a}ische Kommission (EUK) schl\"{a}gt vor, die Patentierung von Patenten auf Datenverarbeitungsprogrammen als solchen zu legalisieren und sicher zu stellen, dass breite und triviale Patente auf Programm- und Gesch\"{a}ftslogik, wie sie derzeit vor allem in den USA von sich reden machen, k\"{u}nftig auch hier in Europa Bestand haben und von keinem Gericht mehr zur\"{u}ckgewiesen werden k\"{o}nnen.  ``Aber hallo, die EUK sagt in ihrer Presseerkl\"{a}rung etwas ganz anderes!'', m\"{o}chten Sie vielleicht einwenden.  Ganz richtig!  Um herauszufinden, was die EUK wirklich sagt, m\"{u}ssen Sie n\"{a}mlich nicht die PE sondern den Vorschlag selbst lesen.  Aber Vorsicht, der ist in einem Neusprech vom Europ\"{a}ischen Patentamt (EPA) verfasst, in dem gew\"{o}hnliche W\"{o}rter oft das Gegenteil dessen bedeuten, was Sie erwarten w\"{u}rden. Zur Verwirrung tr\"{a}gt noch ein langer werbender Vorspann bei, in dem die Wichtigkeit von Patenten und propriet\"{a}rer Software beschworen wird, wobei dem software-unerfahrenen Zielpublikum ein Zusammenhang zwischen beiden suggeriert wird.  Dieser Text ignoriert die Meinungen von allen geachteten Programmierern und Wirtschaftswissenschaftlern und st\"{u}tzt seine sp\"{a}rlichen Aussagen \"{u}ber die \"{O}konomie der Software-Entwicklung nur auf zwei unver\"{o}ffentlichte Studien aus dem Umfeld von BSA (von Microsoft und anderen amerikanischen Gro{\ss}unternehmen dominierter Verband zur Durchsetzung des Urheberrechts) \"{u}ber die Wichtigkeit propriet\"{a}rer Software.  Diese Studien haben \"{u}berhaupt nicht Softwarepatente zum Thema!  Der Werbe-Vorspann und der Vorschlag selber wurden offensichtlich f\"{u}r die EUK von einem Angestellten von BSA redigiert.  Unten zitieren wir den vollst\"{a}ndigen Vorschlag zusammen mit Belegen f\"{u}r die Rolle von BSA, einer Analyse des Inhalts und einer tabellarischen Gegen\"{u}berstellung der BSA- und EUK-Version sowie einer EP\"{U}-Version, d.h. eines Gegenvorschlages im Geiste des Europ\"{a}ischen Patent\"{u}bereinkommen von 1973 und der aufgekl\"{a}rten Patentliteratur.  Die EP\"{U}-Version sollte Ihnen helfen, die Klarheit und Weisheit der heute g\"{u}ltigen gesetzlichen Regelung verstehen, an deren Aushebelung die Patentanw\"{a}lte der Europ\"{a}ischen Kommission gemeinsam mit EPA, BSA u.a. in den letzten Jahren hart gearbeitet haben.
\end{quote}
\filbreak

\item
{\bf {\bf Gesetzesregel \"{u}ber den Erfindungsbegriff im Europ\"{a}ischen Patentwesen und seine Auslegung unter besonderer Ber\"{u}cksichtigung der Programme f\"{u}r Datenverarbeitungsanlagen\footnote{http://swpat.ffii.org/analyse/eurili/index.de.html}}}

\begin{quote}
Wir schlagen dem Gesetzgeber vor, beim Entwurf einer Richtlinie zur Frage der Patentierbarkeit von Software auf dem folgenden kurzen und klaren Text aufzubauen.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

