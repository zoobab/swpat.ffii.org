\begin{subdocument}{swpatmanri}{Banc de proves de s\`{e}ries d'assaigs de legislaci\'{o} de patentabilitat}{http://swpat.ffii.org/stidi/testsuite/index.ca.html}{Grup de treball\\swpatag@ffii.org\\versi\'{o} anglesa 2002/04/24 per CALIU\footnote{http://www.caliu.org/}}{Per tal d'assajar una proposta de llei, la provem amb un conjunt de mostres d'innovacions.  Per cada innovaci\'{o}, donem una descripci\'{o} de l'art previ, una descripci\'{o} de l'invent declarat i un petit conjunt de reivindicacions.  Donat que les descripcions s\'{o}n correctes, assagem la nostra legislaci\'{o} proposada en ell.  El focus est\`{a} en la claredat i l'adequaci\'{o}:  condueix la regla proposada cap a un veredicte predecible?  Quina de les reivindicacions acceptarem, si n'acceptem alguna?  Aquest resultat \'{e}s el que vol\'{\i}em?  Provem d'altres propostes de llei amb la mateixa s\`{e}rie d'assaigs i veiem quina obt\'{e} la millor puntuaci\'{o}.  Els professionals del programari creuen que hauries de ``primer fixar les errades, despr\'{e}s alliberar el codi''.  Les s\`{e}ries d'assaigs s\'{o}n una manera habitual d'aconseguir-ho.  La legislaci\'{o} \'{e}s un camp de la tecnologia anomenat ?(q:enginyeria social), oi?  Tecnologia o no, ja \'{e}s hora d'encarar la legislaci\'{o} amb el mateix rigor metodol\`{o}gic aplicable on sigui que decisions mal dissenyades puguin afectar significantment la vida de la gent.}
\begin{sect}{comp}{Algunes mostres de patents}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Patents Europees de Programari: Documentaci\'{o} Entenedora\footnote{http://swpat.ffii.org/pikta/txt/index.ca.html}}}

\begin{quote}
Durant els \'{u}ltims anys, l'Oficina Europea de Patents (EPO) ha concedit m\'{e}s de 10 000 patents de programari, \'{e}s a dir, pantents de regles de c\`{a}lcul la validesa de les quals s'ha de provar amb raonament pur, per comptes de verificar-les amb l'experimentaci\'{o} amb forces naturals. Sistem\`{a}ticament, estem col$\cdot$leccionant aquestes patents i publicant-les de nou de manera m\'{e}s accessible.
\end{quote}
\filbreak

\item
{\bf {\bf Patents Europees de Programari: Exemples Escollits\footnote{http://swpat.ffii.org/pikta/mupli/index.ca.html}}}

\begin{quote}
Vam arribar als seg\"{u}ents impressionants exemples mitjan\c{c}ant la primera lectura de les nostres llistes tabulades de patents de programari.  Estan escollides gaireb\'{e} a l'atzar i representen aproximadament l'est\`{a}ndar de t\'{e}cnica i inventiva de l'Oficina Europea de Patents (EPO).  Si alguna cosa les distingueix d'altres patents \'{e}s la relativa facilitat d'entendre-les a primera vista per una gran quantitat de gent.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{kver}{Preguntes per respondre per cada}
Donat que l'art previ i la contribuci\'{o} estan correctament revelades, com s'haurien de respondre les seg\"{u}ents preguntes, si la llei proposada est\'{e}s en vigor:
\begin{enumerate}
\item
Hi ha una contribuci\'{o} patentable (invent / ensenyament)?  Per qu\`{e} (no)?

\item
La contribuci\'{o} (invent / ensenyament) \'{e}s t\`{e}cnica?  Per qu\`{e} (no)?

\item
S'acceptar\`{a} alguna reivindicaci\'{o}? Quina? Per qu\`{e}?

\item
Algun jutge arribaria a les mateixes conclusions?  On hi ha \`{a}rees d'incertesa?

\item
S\'{o}n adequades aquestes conclusions?  Fins a on haurien de promoure/reprimir l'invent?  Fins a on haurien de coincidir amb la pol\'{\i}tica d'objectius p\'{u}blics, com els citats al Tractat de Roma, a e.Europa, etc?
\begin{itemize}
\item
What effort is needed to arrive at the claimed innovation?  What effort is needed to imitate the claimed innovation without violating copyright?  How does this compare to the innovation effort (innovation vs imitation cost ratio)?  What effort is needed to develop and distribute an average system (e.g. software application, embedded system) of which the claimed innovation would typically be a part?  How does this compare to the innovation effort (innovation vs development cost ratio)?

\item
What special right might be adequate in case patents are deemed too heavy and copyright too light?  Utility cerfiticate?  Specially tailored innovator's privilege / reward\footnote{http://swpat.ffii.org/stidi/basti/index.de.html}?
\end{itemize}
\end{enumerate}
\end{sect}

\begin{sect}{tab}{Comparison Table}
\begin{center}
\begin{tabular}{|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|}
\hline
test sample & innovation vs imitation cost ratio & innovation vs development cost ratio & other indicators & should be patentable? & invention by standard A (CEC/BSA)? & invention by standard B (CPE/FFII)? & eligible for special right X\\\hline
Patent d'Adobe sobre ``Tabbed Palettes``\footnote{http://swpat.ffii.org/pikta/mupli/ep689133/index.en.html} & 1 & 0.00001 & \dots & - & + & - & -\\\hline
provar material ensenyat a les escoles\footnote{http://swpat.ffii.org/pikta/mupli/ep664041/index.ca.html} & 0.5 & 0.005 & \dots & - & + & - & -\\\hline
Audio Coding\footnote{http://swpat.ffii.org/pikta/xrani/mpeg/index.en.html} & 2 & 0.05 & \dots & - & + & - & o\\\hline
Fe-B-R Alloy & 10 & 0.5 & \dots & + & + & + & o\\\hline
\dots &  &  &  &  &  &  & \\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{test}{Legislacions candidates a ser assajades}
Sisplau, proposeu altres candidates, hi posarem un enlla\c{c}!

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf CEC \& BSA 2002-02-20: proposal to make all useful ideas patentable\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/index.en.html}}}

\begin{quote}
The European Commission (CEC) proposes to legalise the granting of patents on computer programs as such in Europe and ensure that there is no longer any legal foundation for refusing american-style software and business method patents in Europe.   ``But wait a minute, the CEC doesn't say that in its press release!'' you may think.  Quite right!  To find out what they are really saying, you need to read the proposal itself.  But be careful, it is written in an esoteric Newspeak from the European Patent Office (EPO), in which normal words often mean quite the opposite of what you would expect.  Also you may get stuck in a long and confusing advocacy preface, which mixes EPO slang with belief statements about the importance of patents and proprietary software, implicitely suggesting some kind of connection between the two.  This text disregards the opinions of virtually all respected software developpers and economists, citing as its only source of information about the software reality two unpublished studies from BSA \& friends (alliance for copyright enforcement dominated by Microsoft and other large US companies) about the importance of proprietary software.  These studies do not even deal with patents!  The advocacy text and the proposal itself were apparently drafted on behalf of the CEC by an employee of BSA.  Below we cite the complete proposal, adding proofs for BSA's role as well as an analysis of the content, based on a tabular comparison of the BSA and CEC versions with a debugged version based on the European Patent Convention (EPC) and related doctrines as found in the EPO examination guidelines of 1978 and the caselaw of the time.  This EPC version help you to appreciate the clarity and wisdom of the patentability rules in the currently valid law, which the CEC's patent lawyer friends have worked hard to deform during the last few years.
\end{quote}
\filbreak

\item
{\bf {\bf Regulaci\'{o} sobre el concepte d'invent dels sistema europeu de patents i la seva interpretaci\'{o} amb especial consideraci\'{o} als programes per a ordinadors\footnote{http://swpat.ffii.org/stidi/javni/index.ca.html}}}

\begin{quote}
Proposem que el legislador projecti alguna regulaci\'{o} sobre patentabilitat de programari a trav\'{e}s de les l\'{\i}nies del seg\"{u}ent curt i clar texte.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

