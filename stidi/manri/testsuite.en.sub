\begin{subdocument}{swpatmanri}{Patentability Legislation Benchmarking Test Suite}{http://swpat.ffii.org/analysis/testsuite/index.en.html}{Workgroup\\swpatag@ffii.org}{In order to test a law proposal, we try it out on a set of sample innovations.   Each innovation is described in terms of prior art, a technical contribution (invention) and a small set of claims.  Assuming that the descriptions are correct, we then test our proposed legislation on them.  The focus is on clarity and adequacy:  does the proposed rule lead to a predictable verdict?  Which of the claims, if any, will be accepted?  Is this result what we want?   We try out different law proposals for the same test series and see which scores best.  Software professionals believe that you should ``first fix the bugs, then release the code''.  Test suites are a common way of achieving this.  Pursuant to Art 27 TRIPS, legislation belongs to a ``field of technology'' called ``social engineering'', doesn't it?  Technology or not, it is time to approach legislation with the same methodological rigor that is applicable wherever bad design decisions can significantly affect people's lives.}
\begin{sect}{comp}{Some sample patents}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf European Software Patents: Comprehensive Documentation\footnote{http://swpat.ffii.org/patents/txt/index.en.html}}}

\begin{quote}
During the last few years, the European Patent Office (EPO) has granted several 10000 software patents, i.e. patents on rules of calculation whose validity can be proven by means of pure reasoning rather than verified by means of experimentation with natural forces. We are systematically collecting these patents and republishing them in a more accessible form.
\end{quote}
\filbreak

\item
{\bf {\bf European Software Patents: Assorted Examples\footnote{http://swpat.ffii.org/patents/samples/index.en.html}}}

\begin{quote}
We came across the following impressive examples at first reading through our tabular listings of software patents.  They were almost randomly chosen and thus approximately represent the technicity and inventivity standards of the European Patent Office.  If anything distinguishes them from other patents it is the relative ease with which they can be understood at first sight by a fairly large public.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{kver}{Questions to be answered for each}
Assuming that the prior art and the contribution are correctly disclosed, how would the following questions be answered, if the proposed law was in force:
\begin{enumerate}
\item
Is there a patentable contribution (invention / teaching) in here?  Why (not)?

\item
Is the contribution (invention / teaching) a technical one?  Why (not)?

\item
Will any of the claims be accepted? Which? Why?

\item
Would any judge reach the same conclusions?  Where are areas of incertainty?

\item
Are these conclusions adequate?  To what extent would they promote/stifle innovation?  To what extent would they conform to public policy goals, such as those spelled out in the Rome Treaty, in e.Europe etc?
\begin{itemize}
\item
What effort is needed to arrive at the claimed innovation?  What effort is needed to imitate the claimed innovation without violating copyright?  How does this compare to the innovation effort (innovation vs imitation cost ratio)?  What effort is needed to develop and distribute an average system (e.g. software application, embedded system) of which the claimed innovation would typically be a part?  How does this compare to the innovation effort (innovation vs development cost ratio)?

\item
What special right might be adequate in case patents are deemed too heavy and copyright too light?  Utility cerfiticate?  Specially tailored innovator's privilege / reward\footnote{http://swpat.ffii.org/analysis/suigen/index.en.html}?
\end{itemize}
\end{enumerate}
\end{sect}

\begin{sect}{tab}{Comparison Table}
\begin{center}
\begin{tabular}{|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|}
\hline
test sample & innovation vs imitation cost ratio & innovation vs development cost ratio & other indicators & should be patentable? & invention by standard A (CEC/BSA)? & invention by standard B (EPC/FFII)? & eligible for special right X\\\hline
Adobe Patent on Tabbed Palettes\footnote{http://swpat.ffii.org/patents/samples/ep689133/index.en.html} & 1 & 0.00001 & \dots & - & + & - & -\\\hline
testing learned material in schools\footnote{http://swpat.ffii.org/patents/samples/ep664041/index.en.html} & 0.5 & 0.005 & \dots & - & + & - & -\\\hline
Audio Coding\footnote{http://swpat.ffii.org/patents/effects/mpeg/index.en.html} & 2 & 0.05 & \dots & - & + & - & o\\\hline
Fe-B-R Alloy & 10 & 0.5 & \dots & + & + & + & o\\\hline
\dots &  &  &  &  &  &  & \\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{test}{Legislation candidates to be tested}
Please propose other candidates, we will link to them!

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf CEC \& BSA 2002-02-20: proposal to make all useful ideas patentable\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/index.en.html}}}

\begin{quote}
The European Commission (CEC) proposes to legalise the granting of patents on computer programs as such in Europe and ensure that there is no longer any legal foundation for refusing american-style software and business method patents in Europe.   ``But wait a minute, the CEC doesn't say that in its press release!'' you may think.  Quite right!  To find out what they are really saying, you need to read the proposal itself.  But be careful, it is written in an esoteric Newspeak from the European Patent Office (EPO), in which normal words often mean quite the opposite of what you would expect.  Also you may get stuck in a long and confusing advocacy preface, which mixes EPO slang with belief statements about the importance of patents and proprietary software, implicitely suggesting some kind of connection between the two.  This text disregards the opinions of virtually all respected software developpers and economists, citing as its only source of information about the software reality two unpublished studies from BSA \& friends (alliance for copyright enforcement dominated by Microsoft and other large US companies) about the importance of proprietary software.  These studies do not even deal with patents!  The advocacy text and the proposal itself were apparently drafted on behalf of the CEC by an employee of BSA.  Below we cite the complete proposal, adding proofs for BSA's role as well as an analysis of the content, based on a tabular comparison of the BSA and CEC versions with a debugged version based on the European Patent Convention (EPC) and related doctrines as found in the EPO examination guidelines of 1978 and the caselaw of the time.  This EPC version help you to appreciate the clarity and wisdom of the patentability rules in the currently valid law, which the CEC's patent lawyer friends have worked hard to deform during the last few years.
\end{quote}
\filbreak

\item
{\bf {\bf Regulation about the invention concept of the European patent system and its interpretation with special regard to programs for computers\footnote{http://swpat.ffii.org/analysis/directive/index.en.html}}}

\begin{quote}
We propose that the legislator draft any regulations on the question of software patentability along the lines of the following short and clear text.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

