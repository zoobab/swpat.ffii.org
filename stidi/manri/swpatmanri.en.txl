<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patentability Legislation Benchmarking Test Suite

#descr: In order to test a law proposal, we try it out on a set of sample
innovations.   Each innovation is described in terms of prior art, a
technical contribution (invention) and a small set of claims. 
Assuming that the descriptions are correct, we then test our proposed
legislation on them.  The focus is on clarity and adequacy:  does the
proposed rule lead to a predictable verdict?  Which of the claims, if
any, will be accepted?  Is this result what we want?   We try out
different law proposals for the same test series and see which scores
best.  Software professionals believe that you should %(q:first fix
the bugs, then release the code).  Test suites are a common way of
achieving this.  Pursuant to Art 27 TRIPS, legislation belongs to a
%(q:field of technology) called %(q:social engineering), doesn't it? 
Technology or not, it is time to approach legislation with the same
methodological rigor that is applicable wherever bad design decisions
can significantly affect people's lives.

#Sap: Some sample patents

#aea: Sample Claims

#Qoe: Questions to be answered for each

#CrW: Comparison Table

#LWW: Legislation candidates to be tested

#wna: New Patent Database

#Ptn: Patent

#nrh: Claim 1

#nrn: Comment

#ep266049: Runlength Coding

#ep803105: Network Sales System

#ep287578: Audio Coding

#ep538888: Pay per Read

#ep689133: Tabbed Palettes

#ep101552: Fe-B-R Magnet

#Att: An alloy comprising Fe-B-R characterized by containing at least one
stable compound of the ternary Fe-B-R type, which compound can be
magnetized to become a permanent magnet at room temperature and above,
where R stands for at least one rare earth element inclusive yttrium.

#Wld: This is the strongest known magnet.  Sumitomo found it by
experimentation in the early 1980s.  Subsequent experiments failed to
turn out anything nearly as strong.  No mobile phone can be
competitive today without this magnet.

#lrt: Clarity

#dqa: Adequacy

#Aet: Assuming that the prior art and the contribution are correctly
disclosed, how would the following questions be answered, if the
proposed law was in force:

#ItW: Would this kind of innovation be patentable under the proposed rules? 
Why (not)?

#WWW: Would any judge reach the same conclusions?  Where are areas of
incertainty?

#Atn: Are these conclusions adequate?

#lfe: To what extent would they promote/stifle innovation?  What effects
would they have on competition? on the interests of consumers?

#ujW: To what extent would they conform to public policy goals, such as
those spelled out in the Rome Treaty, in eEurope etc?

#Wim: What effort is needed to arrive at the claimed innovation?  What
effort is needed to imitate the claimed innovation without violating
copyright?  How does this compare to the innovation effort (innovation
vs imitation cost ratio)?  What effort is needed to develop and
distribute an average system (e.g. software application, embedded
system) of which the claimed innovation would typically be a part? 
How does this compare to the innovation effort (innovation vs
development cost ratio)?

#Wtf: What special right might be adequate in case patents are deemed too
heavy and copyright too light?  Utility cerfiticate?  %(sb:Specially
tailored innovator's privilege / reward)?

#tta: test sample

#imW: innovation vs imitation cost ratio

#idW: innovation vs development cost ratio

#oWc: other indicators

#sbn: should be patentable?

#ibW: invention by %(sa:standard A)?

#ibI: invention by %(sb:standard B)?

#wve: candidate for sui generis property

#nepat: %1 not a technical invention

#jespat: %1 is a technical invention

#hkt: check item

#ilW: clear goal statement in preambule

#oma: No Information Claims

#bie: Publication Freedom Guarantee

#afu: Software Property (copyright) Guarantee

#oie: Software on the Disclosure side

#eii: Interoperability privilege

#ooW: %(q:Technical) defined

#vfW: Negative Definition of %(q:Technical Invention)

#eof: by reference to forces of nature

#aoW: 4 Separate Tests on 1 Unified Object

#eWu: Invention = Contribution = novel and technical core

#opo: EPO slang debugged, biased terminology such as
%(q:computer-implemented invention) etc eliminated or redefined

#Wtd: Supervision of Execution

#lun: One of the pamphlets is a one-page version of the test suite.  Two
example claims are presented to politicians with a simplified set of
questions.  So far the proponents of software patents, such as Arlene
McCarthy, have failed to answer.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatmanri ;
# txtlang: en ;
# multlin: t ;
# End: ;

