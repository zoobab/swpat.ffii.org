<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Workgroup">
<link href="/group/index.en.html" rev="made">
<link href="http://www.ffii.org/assoc/webstyl/ffii.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2004/11/07">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Reply-To" content="swpatag@ffii.org">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="title" content="Patentability Legislation Benchmarking Test Suite">
<meta name="description" content="In order to test a law proposal, we try it out on a set of sample innovations.   Each innovation is described in terms of prior art, a technical contribution (invention) and a small set of claims.  Assuming that the descriptions are correct, we then test our proposed legislation on them.  The focus is on clarity and adequacy:  does the proposed rule lead to a predictable verdict?  Which of the claims, if any, will be accepted?  Is this result what we want?   We try out different law proposals for the same test series and see which scores best.  Software professionals believe that you should &quot;first fix the bugs, then release the code&quot;.  Test suites are a common way of achieving this.  Pursuant to Art 27 TRIPS, legislation belongs to a &quot;field of technology&quot; called &quot;social engineering&quot;, doesn't it?  Technology or not, it is time to approach legislation with the same methodological rigor that is applicable wherever bad design decisions can significantly affect people's lives.">
<title>Patentability Legislation Benchmarking Test Suite</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<div id=toparb>
<table width="100%">
<tr><td align=left colspan=4><div id="toprel">
<a href="index.de.html"><img src="/img/flags/de.png" alt="[DE Deutsch]" border="1" height=25></a>
<a href="index.ca.html"><img src="/img/flags/catalan.png" alt="[CA Catalan]" border="1" height=25></a>
<a href="index.fr.html"><img src="/img/flags/fr.png" alt="[FR Francais]" border="1" height=25></a>
<a href="swpatmanri.en.txl"><img src="/img/icons.other/txt.png" alt="[translatable text]" border="1" height=25></a>
<a href="/group/todo/index.en.html"><img src="/img/icons.other/questionmark.png" alt="[howto help]" border="1" height=25></a>
<a href="swpatmanri.en.pdf"><img src="/img/icons.other/pdficon.png" alt="[printable version]" border="1" height=25></a>
<a href="http://kwiki.ffii.org/SwpatmanriEn"><img src="/img/icons.other/addenda.png" alt="[Addenda]" border="0"></a>
</div></td><td align=center colspan=2 rowspan=2><form action="http://www.google.com/search" method="get">
<input name="q" size=20 type="text" value=""><br>
<img src="/img/logos/google80.png" alt="Google"><input name="btnG" type="submit" value="Search ffii.org">
<input name="q" type="hidden" value="site:ffii.org">
</form></td></tr>
<tr><td align=left colspan=4><div id=toparb1 align=left>
<a href="/index.en.html">Software Patents</a> &gt; <a href="/analysis/index.en.html">Analysis</a> &gt; Test Suite
</div></td></tr>
<tr class=doklvl3><td><a href="/analysis/index.en.html">Analysis</a></td><td><a href="/analysis/korcu/index.en.html">Invention</a></td><td class=eqdok>Test Suite</td><td><a href="/analysis/epc52/index.en.html">EPC</a></td><td><a href="/analysis/shield/index.en.html">Shield?</a></td><td><a href="/analysis/cost/index.en.html">Compensate?</a></td></tr>
</table>
</div>
<div id="doktit">
<h1><a name="top">Patentability Legislation Benchmarking Test Suite</a></h1>
</div>
<div id="dokdes">
In order to test a law proposal, we try it out on a set of sample innovations.   Each innovation is described in terms of prior art, a technical contribution (invention) and a small set of claims.  Assuming that the descriptions are correct, we then test our proposed legislation on them.  The focus is on clarity and adequacy:  does the proposed rule lead to a predictable verdict?  Which of the claims, if any, will be accepted?  Is this result what we want?   We try out different law proposals for the same test series and see which scores best.  Software professionals believe that you should &quot;first fix the bugs, then release the code&quot;.  Test suites are a common way of achieving this.  Pursuant to Art 27 TRIPS, legislation belongs to a &quot;field of technology&quot; called &quot;social engineering&quot;, doesn't it?  Technology or not, it is time to approach legislation with the same methodological rigor that is applicable wherever bad design decisions can significantly affect people's lives.
</div>
</div>
<div id="dokmid">
<div class="sectmenu">
<ul><li><a href="#comp">Some sample patents</a></li>
<li><a href="#clms">Sample Claims</a></li>
<li><a href="#kver">Questions to be answered for each</a></li>
<li><a href="#tab">Comparison Table</a></li>
<li><a href="#test">Legislation candidates to be tested</a></li>
<li><a href="#links">Annotated Links</a></li></ul>
</div>

<div class="secttext">
<div class="secthead">
<h2><a name="comp">Some sample patents</a></h2>
</div>

<div class="sectbody">
<div class="links">
<dl><dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/patents/txt/ep/index.en.html">Software Patents of the European Patent Office</a></b></dt>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://gauss.ffii.org/">New Patent Database</a></b></dt>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/patents/samples/index.en.html">European Software Patents: Assorted Examples</a></b></dt></dl>
</div>
</div>

<div class="secthead">
<h2><a name="clms">Sample Claims</a></h2>
</div>

<div class="sectbody">
<table border=1>
<tr><th align=center>Patent</th><th align=center>Claim 1</th><th align=center>Comment</th></tr>
<tr><th><a href="/patents/samples/ep266049/index.en.html">Runlength Coding</a> (<a href="/patents/effects/jpeg/index.en.html">JPEG</a>)</th><td>An ordered redundancy method for coding digital signals, the digital signals taking a plurality of different values, using two types of runlength code (R, R'), the method comprising the steps of.<ol><li>utilising the first type of runlength code (R) for coding a runlength of the most frequently occurring value followed by the next most frequently occurring value;</li>
<li>utilising the second type of runlength code (R') for coding a runlength of the most frequently occurring value followed by any value other than the next most frequently occurring value; and</li>
<li>when the second type of runlength code is used, following the runlength code value by a code value indicative of the amplitude of said other value.</li></ol></td><td>This patent was granted by the EPO in 1994 after 7 years of examination, with priority date 1986. In 2002 it prompted Sony and other companies to pay many million USD for using the JPEG standard and made JPEG cease to be an international standard.</td></tr>
<tr><th><a href="Network Sales System">Network Sales System</a> (OpenMarket)</th><td>A network-based sales system, comprising
<ul><li>at least one buyer computer for operation by a user desiring to buy a product;</li>
<li>at least one merchant computer; and</li>
<li>at least one payment computer;</li>
<li>the buyer computer, the merchant computer, and the payment computer being interconnected by a computer network;</li>
<li>the buyer computer being programmed to receive a user request for purchasing a product, and to cause a payment message to be sent to the payment computer that comprises a product identifier identifying the product;</li>
<li>the payment computer being programmed to receive the payment message, to cause an access message to be created that comprises the product identifier and an access message authenticator based on a cyptographic key, and to cause the access message to be sent to the merchant computer;</li>
<li>and the merchant computer being programmed to receive the access message, to verify the access message authenticator to ensure that the access message authenticator was created using said cyptographic key, and to cause the product to be sent to the user desiring to buy the product.</li></ul></td><td>This patent, granted to OpenMarket Inc by the European Patent Office in 2002 after 6 years of examination, is identical to a system which is currently being used in the USA to squeeze money out of various e-commerce companies.</td></tr>
<tr><th><a href="/patents/samples/ep287578/index.en.html">Audio Coding</a> (<a href="/patents/effects/mpeg/index.en.html">MPEG</a>)</th><td>Digital coding process for transmitting and/or storing acoustic signals, specifically music signals, comprising the following steps.
<ul><li>N samples of the acoustic signal are converted into M spectral coefficients;</li>
<li>said M spectral coefficients are subjected to quantisation at a first level;</li>
<li>after coding by means of an entropic encoder the number of bits required to represent all the quantized spectral coefficients is checked;</li>
<li>when the required number of bits does not correspond to a specified number of bits quantization and coding are repeated in subsequent steps, each at a modified quantization level, until the number of bits required for representation reaches the specified number of bits, and</li>
<li>additionally to the data bits the required quantization level is transmitted and/or stored.</li></ul></td><td>Iteratively perform certain calculations on acoustic data until a certain value is reached.  The patent owner Karlheinz Brandenburg, core researcher of the MP3 project at Max Planck, received this patent in 1989.  This patent and its owner were showcased by the European Commission's &quot;IPR Helpdesk&quot; project in 2001 as &quot;inventor of the month&quot;.  This is one of several dozen patents which cover the MP3 audio compression standard, and perhaps the most famous and basic one.  It has always been treated as a model of how &quot;technical&quot; and &quot;non-trivial&quot; software patents can get.</td></tr>
<tr><th><a href="/patents/samples/ep538888/index.en.html">Pay per Read</a> (Canon)</th><td>An information processing system comprising<p><ul><li>receiving means for receiving transmitted information, the information being coded or incomplete and in a non-usable form;</li>
<li>a recording medium, decoding information needed to demodulate the received information being prerecorded on said recording medium, said recording medium comprising a first area in which the received information is to be written and a second area in which the decoding information is pre-recorded;</li>
<li>writing means for writing the information received by said receiving means in the first area of said recording medium;</li>
<li>reading means for reading out the decoding information from said recording medium; and</li>
<li>demodulating means for converting the information received by said receiving means and written in said first area of said recording medium to a usable form using the decoding information read out by said reading means;</li></ul><p>wherein the charge for use of the information is defined in units corresponding to the decoding information being prerecorded on said recording medium</td><td>In 1993, the European Patent Office (EPO) granted Canon K.K. of Japan owns a patent on charging a fee per a unit of decoded information.  The main claim covers all systems where a local application decodes information received from remote information distributor and calculates a fee based on the amount of information decoded.  If an information vendor wants to realise a full &quot;Pay Per Use&quot; system where the fee arises only when the user actually reads the information (rather than when it is transmitted), he might want to beg Canon for a license.  Perhaps Canon will be generous, since it is clear that the patent claim describes a class of programs for computers (computer-implemented calculation rules), and the supposedly novel and inventive problem solution (invention) consists in nothing but the program [ as such ].  All features of this claim belong to the field of data processing by means of generic computer equipment.</td></tr>
<tr><th><a href="/patents/samples/ep689133/index.en.html">Tabbed Palettes</a> (<a href="/patents/effects/palette/index.en.html">Adobe</a>)</th><td>A method for combining on a computer display an additional set of information displayed in a first area of the display and having associated with it a selection indicator into a group of multiple sets of information needed on a recurring basis displayed in a second area of the screen, comprising the steps of <ol><li>establishing the second area on the computer display in which the group of multiple sets of information is displayed, the second area having a size which is less than the entire area of the computer display, the second area displaying a first of the multiple sets of information;</li>
<li>providing within the second area a plurality of selection indicators, each one associated with a corresponding one of the multiple selecting a second of the multiple sets of information for display within the second area by activating a selection indicator associated with a second of the multiple sets of information, whereby the second of the multiple sets of information is substituted for the first of the multiple sets of information within the area of the display; and</li>
<li>combining the additional set of information, displayed in the first area of the display into the group of multiple sets of information so that the additional set of information may be selected using its selection indicator in the same manner as the other sets of information in the group.</li></ol></td><td>This patent, granted by the EPO in Aug 2001, has been used by Adobe to sue Macromedia Inc in the US.  The EP version took 6 years to examine, and it was granted in full breadth, without any modification.  It covers the idea of adding a third dimension to a menu system by arranging several sets of options behind each other, marked with tabs.  This is particularly found to be useful in image processing software of Adobe and Macromedia, but also in The GIMP and many other programs.</td></tr>
<tr><th><a href="../../patents/txt/ep/0101/552">EP0101552</a> (Sumitomo)</th><td>An alloy comprising Fe-B-R characterized by containing at least one stable compound of the ternary Fe-B-R type, which compound can be magnetized to become a permanent magnet at room temperature and above, where R stands for at least one rare earth element inclusive yttrium.</td><td>This is the strongest known magnet.  Sumitomo found it by experimentation in the early 1980s.  Subsequent experiments failed to turn out anything nearly as strong.  No mobile phone can be competitive today without this magnet.</td></tr>
</table>
</div>

<div class="secthead">
<h2><a name="kver">Questions to be answered for each</a></h2>
</div>

<div class="sectbody">
<div class="sectmenu">
<ul><li><a href="#klar">Clarity</a></li>
<li><a href="#ekvec">Adequacy</a></li></ul>
</div>

<div class="secttext">
<div class="secthead">
<h3><a name="klar">Clarity</a></h3>
</div>

<div class="sectbody">
Assuming that the prior art and the contribution are correctly disclosed, how would the following questions be answered, if the proposed law was in force:<p>Would this kind of innovation be patentable under the proposed rules?  Why (not)?<p>Would any judge reach the same conclusions?  Where are areas of incertainty?
</div>

<div class="secthead">
<h3><a name="ekvec">Adequacy</a></h3>
</div>

<div class="sectbody">
<ul><li>Are these conclusions adequate?</li>
<li>To what extent would they promote/stifle innovation?  What effects would they have on competition? on the interests of consumers?</li>
<li>To what extent would they conform to public policy goals, such as those spelled out in the Rome Treaty, in eEurope etc?</li>
<li>What effort is needed to arrive at the claimed innovation?  What effort is needed to imitate the claimed innovation without violating copyright?  How does this compare to the innovation effort (innovation vs imitation cost ratio)?  What effort is needed to develop and distribute an average system (e.g. software application, embedded system) of which the claimed innovation would typically be a part?  How does this compare to the innovation effort (innovation vs development cost ratio)?</li>
<li>What special right might be adequate in case patents are deemed too heavy and copyright too light?  Utility cerfiticate?  <a href="/analysis/basti/index.en.html">Specially tailored innovator's privilege / reward</a>?</li></ul>
</div>
</div>
</div>

<div class="secthead">
<h2><a name="tab">Comparison Table</a></h2>
</div>

<div class="sectbody">
<table border=1>
<tr><th align=center>test sample</th><th align=center>innovation vs imitation cost ratio</th><th align=center>innovation vs development cost ratio</th><th align=center>other indicators</th><th align=center>should be patentable?</th><th align=center>invention by standard A (CEC/BSA)?</th><th align=center>invention by standard B (EPC/FFII)?</th><th align=center>candidate for sui generis property</th></tr>
<tr><th><a href="/patents/samples/ep689133/index.en.html">Tabbed Palettes</a></th><td>1</td><td>0.00001</td><td>...</td><td>-</td><td>+</td><td>-</td><td>-</td></tr>
<tr><th><a href="/patents/samples/ep803105/index.en.html">Network Sales System</a></th><td>0.5</td><td>0.005</td><td>...</td><td>-</td><td>+</td><td>-</td><td>-</td></tr>
<tr><th><a href="/patents/samples/ep287578/index.en.html">Audio Coding</a></th><td>1</td><td>0.05</td><td>...</td><td>-</td><td>+</td><td>-</td><td>o</td></tr>
<tr><th><a href="../../patents/txt/ep/0101/552">EP101552</a></th><td>10</td><td>0.5</td><td>...</td><td>+?</td><td>+</td><td>+</td><td>o</td></tr>
<tr><th>...</th><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>
</div>

<div class="secthead">
<h2><a name="test">Legislation candidates to be tested</a></h2>
</div>

<div class="sectbody">
<dl><dt>CEC:</dt>
<dd><a href="/papers/eubsa-swpat0202/index.en.html">CEC &amp; BSA 2002-02-20: proposal to make all useful ideas patentable</a></dd>
<dt>CULT:</dt>
<dd><a href="/papers/eubsa-swpat0202/cult0212/index.en.html">Rocard/CULT 2002-12-09: Data Processing is Not a Field of Technology</a></dd>
<dt>ITRE:</dt>
<dd><a href="/papers/eubsa-swpat0202/itre0212/index.en.html">Plooij/ITRE Counter-Proposal: Publication and Interoperation do not Infringe</a></dd>
<dt>JURI:</dt>
<dd><a href="/papers/eubsa-swpat0202/juri0304/index.en.html">JURI 2003/04-6 Amendments: Real and Fake Limits on Patentability</a></dd>
<dt>PARL:</dt>
<dd><a href="/papers/europarl0309/index.en.html">Europarl 2003-09-24: Amended Software Patent Directive</a></dd>
<dt>CONS:</dt>
<dd><a href="/papers/europarl0309/cons0401/index.en.html">EU Council 2004 Proposal on Software Patents</a></dd>
<dt>FFII:</dt>
<dd><a href="/papers/eubsa-swpat0202/prop/index.en.html">EU Software Patent Directive Amendment Proposals</a></dd></dl><p><table border=1>
<tr><th align=center>check item</th><th align=center>CEC</th><th align=center>&nbsp;</th><th align=center>CULT</th><th align=center>ITRE</th><th align=center>PARL</th><th align=center>FFII</th></tr>
<tr><th><a href="/patents/samples/ep689133/index.en.html">Tabbed Palettes</a> not a technical invention</th><td>-</td><td>-</td><td>?+</td><td>?</td><td>+</td><td>+</td></tr>
<tr><th><a href="/patents/samples/ep803105/index.en.html">Network Sales System</a> not a technical invention</th><td>-</td><td>-</td><td>?+</td><td>?</td><td>+</td><td>+</td></tr>
<tr><th><a href="/patents/samples/ep287578/index.en.html">Audio Coding</a> not a technical invention</th><td>-</td><td>-</td><td>?+</td><td>?</td><td>+</td><td>+</td></tr>
<tr><th><a href="/papers/bgh-dispo76/index.en.html">disposition program 1976</a> not a technical invention</th><td>-</td><td>-</td><td>+?</td><td>?</td><td>+</td><td>+</td></tr>
<tr><th><a href="/papers/bgh-walzst80/index.en.html">Rod Splitting 1980</a> not a technical invention</th><td>-</td><td>-</td><td>+?</td><td>?</td><td>+</td><td>+</td></tr>
<tr><th><a href="/papers/bgh-flug86/index.de.html">Flight Costs 1986</a> not a technical invention</th><td>-</td><td>-</td><td>+?</td><td>?</td><td>+</td><td>+</td></tr>
<tr><th><a href="/papers/bgh-abs80/index.de.html">ABS 1980</a> not a technical invention</th><td>-</td><td>-</td><td>?</td><td>?</td><td>+?</td><td>+?</td></tr>
<tr><th><a href="../../patents/txt/ep/0101/552">EP0101552</a> is a technical invention</th><td>+</td><td>+</td><td>+</td><td>+</td><td>+</td><td>+</td></tr>
<tr><th>clear goal statement in preambule</th><td>-</td><td>-</td><td>?</td><td>?</td><td>?</td><td>+</td></tr>
<tr><th>No Information Claims</th><td>+</td><td>-</td><td>+</td><td>+</td><td>+</td><td>+</td></tr>
<tr><th>Publication Freedom Guarantee</th><td>-</td><td>-</td><td>-</td><td>+</td><td>+</td><td>+</td></tr>
<tr><th>Software Property (copyright) Guarantee</th><td>--</td><td>--</td><td>-</td><td>-</td><td>-</td><td>+</td></tr>
<tr><th>Software on the Disclosure side</th><td>-</td><td>-</td><td>-</td><td>+</td><td>+</td><td>+</td></tr>
<tr><th>Software on the Disclosure side</th><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>+</td></tr>
<tr><th>Interoperability privilege</th><td>-</td><td>-</td><td>-</td><td>+</td><td>+</td><td>+</td></tr>
<tr><th>&quot;Technical&quot; defined</th><td>-</td><td>-</td><td>+</td><td>-</td><td>+</td><td>+</td></tr>
<tr><th>Negative Definition of &quot;Technical Invention&quot; (e.g. &quot;Technical&quot; defined)</th><td>-</td><td>-</td><td>+</td><td>-</td><td>+</td><td>+</td></tr>
<tr><th>&quot;Technical&quot; defined (e.g. by reference to forces of nature)</th><td>-</td><td>-</td><td>+</td><td>-</td><td>+</td><td>+</td></tr>
<tr><th>4 Separate Tests on 1 Unified Object (Invention = Contribution = novel and technical core)</th><td>-</td><td>-</td><td>?</td><td>-</td><td>+?</td><td>+</td></tr>
<tr><th>EPO slang debugged, biased terminology such as &quot;computer-implemented invention&quot; etc eliminated or redefined</th><td>-</td><td>-</td><td>?</td><td>-</td><td>+</td><td>+</td></tr>
<tr><th>Supervision of Execution</th><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>+</td></tr>
</table>
</div>

<div class="secthead">
<h2><a name="links">Annotated Links</a></h2>
</div>

<div class="sectbody">
<div class="links">
<dl><dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://aful.org/wws/arc/patents/2003-06/msg00047.html">http://aful.org/wws/arc/patents/2003-06/msg00047.html</a></b></dt>
<dd>see also <a href="/events/2003/europarl/05/remna/amccarthy/index.en.html">amccarthy</a> and <a href="http://aful.org/wws/arc/patents/2003-06/msg00040.html">http://aful.org/wws/arc/patents/2003-06/msg00040.html</a></dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://www.ffii.org/proj/kunst/swpat/pamflet/index.en.html">Pamphlets on Software Patents</a></b></dt>
<dd>One of the pamphlets is a one-page version of the test suite.  Two example claims are presented to politicians with a simplified set of questions.  So far the proponents of software patents, such as Arlene McCarthy, have failed to answer.</dd></dl>
</div>
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<div align="center">[ <a href="/analysis/index.en.html">Software Patents: Questions, Analyses, Proposals</a> | <a href="/analysis/korcu/index.en.html">Patent Jurisprudence on a Slippery Slope -- the price for dismantling the concept of technical invention</a> | Patentability Legislation Benchmarking Test Suite | <a href="/analysis/epc52/index.en.html">Art 52 EPC: Interpretation and Revision</a> | <a href="/analysis/shield/index.en.html">Collective Shields against Software Patents?</a> | <a href="/analysis/cost/index.en.html">Software Patentability with Compensatory Regulation: a Cost Evaluation</a> ]</div>
</div>
<div id="dokadr">
http://swpat.ffii.org/analysis/manri/index.en.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2004/11/07 (2004/08/24)
<a href="/group/index.en.html">Workgroup</a><br>
english version 2004/08/16 by <a href="http://www.a2e.de">PILCH Hartmut</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el -->
<!-- mode: html -->
<!-- End: -->

