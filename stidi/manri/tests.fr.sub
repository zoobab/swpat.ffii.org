\begin{subdocument}{swpatmanri}{Ensemble de tests pour la l\'{e}gislation sur les limites de la brevetabilit\'{e}}{http://swpat.ffii.org/analyse/tests/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise 2002/05/22 par Europe-Shareware.org\footnote{http://www.europe-shareware.org/}}{Pour tester la capacit\'{e} d'un loi sur la brevetabilit\'{e}, nous devons essayer des innovations exemples.  Chaque exemple est d\'{e}crit par un \'{e}tat de la technique, un enseignement technique et une s\'{e}rie de revendications.  Dans l'hypoth\`{e}se que ces d\'{e}scriptions sont pertinentes, nous essayons notre nouvelle r\`{e}gle de loi.  Notre attention se porte sur (1) la clart\'{e} (2) l'\'{e}ffet macro-\'{e}conomique du resultat:  la r\'{e}glementation propos\'{e}e m\`{e}ne-t-elle \`{a} une d\'{e}cision pr\'{e}visible?  Quelles revendications seront accept\'{e}es?  Ce r\'{e}sultat exprime-t-il nos souhaits?  Nous essayons diff\'{e}rentes propositions de lois sur la m\^{e}me s\'{e}rie d'exemples (Testsuite) et comparons lesquelles r\'{e}ussissent le mieux.  Pour un programmeur c'est une question d'honneur que de ``supprimer les erreurs avant de diffuser le programme'' (first fix the bugs, then release the code).  Les ensembles de tests sont un moyen connu pour atteindre ce but.  D'apr\`{e}s l'article 27 ADPIC (TRIPS) la l\'{e}gislation appartient \`{a} un ``domaine de la technique'' notamment ``d'ing\'{e}nierie sociale'' (social engineering), n'est-ce pas ?  Technicit\'{e} ici ou l\`{a}, il est temps d'aborder de ce c\^{o}t\'{e} la l\'{e}gislation avec cette rigueur m\'{e}thodique, qui est partout annonc\'{e}e, o\`{u} les mauvaises d\'{e}cisions de construction peuvent fortement porter atteinte \`{a} la vie des individus.}
\begin{sect}{comp}{Quelques exemples de brevets}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Brevets Logiciels Europ\'{e}ens: Documentation D\'{e}taill\'{e}e\footnote{http://swpat.ffii.org/brevets/txt/index.en.html}}}

\begin{quote}
Pendant les derni\`{e}res ann\'{e}es l'Office Europ\'{e}en de Brevets (OEB) a accord\'{e} quelques 10000 de brevets sur des r\`{e}gles de calcul abstrait.  Nous essayons de collectioner ces brevets et les rendre facilements accessible.
\end{quote}
\filbreak

\item
{\bf {\bf Brevets Logiciels Europ\'{e}ens:  Quelques \'{E}chantillons\footnote{http://swpat.ffii.org/brevets/echantillons/index.fr.html}}}

\begin{quote}
En furetant un peu dans nos archives de brevets logiciels, nous avons trouv\'{e} ces exemples impressionnants.  Ils ont \'{e}t\'{e} choisis \`{a} peu pr\`{e}s par hasard et repr\'{e}sentent le standard de technicit\'{e} et d'inventivit\'{e} appliqu\'{e} par l'OEB. S'il y a quelque chose de sp\'{e}cial dans ces exemples, c'est le fait qu'ils peuvent assez facilement \^{e}tre rendu compr\'{e}hensibles pour le grand public.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{kver}{Questions auxquelles chaque brevet doit r\'{e}pondre}
Suppos\'{e} que l'\'{e}tat de la technique et la contribution sont ici assur\'{e}ment \'{e}vidents, comment un juge devrait r\'{e}pondre aux questions suivantes quand la loi propos\'{e}e entrerait en vigueur?
\begin{enumerate}
\item
Une contribution (invention / enseignement) brevetable est-elle pr\'{e}sente?  Pourquoi (pas)? 

\item
S'agit-il d'une contribution (l'invention / l'enseignement) technique?  Pourquoi (pas)? 

\item
N'importe lesquelles des revendications re\c{c}oicent l'attribution?  Lesquelles?  Pourquoi?

\item
Chaque juge rendra-t-il le m\^{e}me arr\^{e}t?  O\`{u} se trouvent les latitudes de jugement et le domain d'incertitude?

\item
Ces arr\^{e}ts sont-ils appropri\'{e}s?  Dans quelle mesure stimuleront/freineront-ils l'innovation? Dans quelle mesure r\'{e}pondent-ils aux divers objectifs politiques, comme par expl. ceux d\'{e}crits dans le trait\'{e} de Rome ou le projet e.Europe? 
\begin{itemize}
\item
Qu'est-il n\'{e}cessaire comme d\'{e}penses pour parvenir \`{a} l'innovation revendiqu\'{e}e?  Quelle d\'{e}pense est n\'{e}cessaire pour imiter l'innovation, sans violer de droits d'auteur?  Comment se contiennent les deux vis-\`{a}-vis de l'autre (rapport des co\^{u}ts entre l'innovation et l'imitation)?  Quelle d\'{e}pense est n\'{e}cessaire pour d\'{e}velopper un syst\`{e}me entier typique (par expl. une application, un syst\`{e}me int\'{e}gr\'{e}), dans lequel l'innovation revendiqu\'{e}e se pr\'{e}sente comme un \'{e}l\'{e}ment?  Comment cette d\'{e}pense retient les d\'{e}penses d'innovation (rapport des co\^{u}ts entre l'innovation et le d\'{e}veloppement)?

\item
Quel autre droit peut \^{e}tre appropri\'{e}, dans le cas o\`{u} les brevets doivent \^{e}tre jug\'{e}s trop forts et le droit d'auteur trop faible?  La marque d\'{e}pos\'{e}e?  un privil\`{e}ge / droit d'indemnit\'{e}s taill\'{e} sur mesure\footnote{http://swpat.ffii.org/analyse/suigen/index.fr.html}?
\end{itemize}
\end{enumerate}
\end{sect}

\begin{sect}{tab}{Tableau comparatif}
\begin{center}
\begin{tabular}{|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|C{10}|}
\hline
Candidat & Rapport des co\^{u}ts entre l'innovation et l'imitation & Rapport des co\^{u}ts entre l'innovation et le d\'{e}veloppement & divers indicateurs & doit \^{e}tre brevetable? & Invention d'apr\`{e}s la r\`{e}gle A (CCE/BSA)? & Invention d'apr\`{e}s la r\`{e}gle B (CBE/FFII) & m\'{e}rite privil\`{e}ge X\\\hline
Adobe Patent on Tabbed Palettes\footnote{http://swpat.ffii.org/brevets/echantillons/ep689133/index.en.html} & 1 & 0.00001 & \dots & - & + & - & -\\\hline
examens programm\'{e}s\footnote{http://swpat.ffii.org/brevets/echantillons/ep664041/index.fr.html} & 0.5 & 0.005 & \dots & - & + & - & -\\\hline
Codage Audio\footnote{http://swpat.ffii.org/brevets/effets/mpeg/index.fr.html} & 2 & 0.05 & \dots & - & + & - & o\\\hline
Fe-B-R Alloy & 10 & 0.5 & \dots & + & + & + & o\\\hline
\dots &  &  &  &  &  &  & \\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{test}{Candidats \`{a} essayer: Propositions de loi de CEC/BSA, FFII/Eurolinux et d'autres}
Nous proposons au l\'{e}gislateur de baser son propos de directive sur la question de la brevetabilit\'{e} du logiciel sur ce texte court et clair.

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf CCE \& BSA 2002-02-20: Proposition pour rendre toutes les id\'{e}es utiles brevetables\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/index.en.html}}}

\begin{quote}
La Commission Europ\'{e}enne (CCE) propose d'ent\'{e}riner la pratique de l'Office Europ\'{e}en des Brevets (OEB), lequel a ill\'{e}galement accord\'{e} plus que 30000 brevets sur des r\`{e}gles d'organisation et de calcul (programmes d'ordinateur) en tant que tels depuis 1986.  Si le parlement europ\'{e}en adopte cette proposition de loi, il deviendra d\'{e}sormais impossible pour les Cours europ\'{e}ennes de contester la l\'{e}galit\'{e} de tels brevets, et la brevetabilit\'{e} laxiste \`{a} l'am\'{e}ricaine s'imposera d\'{e}finitivement en Europe.  ``Mais attendez une minute, la CCE ne dit pas cela dans son communiqu\'{e} de presse!'' vous voulez dire, n'est-ce pas?  Vous avez raison!  Pour savoir ce que la CCE dit en r\'{e}alit\'{e}, il vous faut lire la proposition elle-m\^{e}me.  Mais faites attention: cette proposition est \'{e}crite dans un langage \'{e}sot\'{e}rique de l'Office Europ\'{e}en des Brevets (OEB), dans lequel les mots ordinaires signifient souvent le contraire de ce que vous attendriez.  Aussi vous devez sauter toute suite \`{a} la fin du texte.  La partie pertinente du propos se trouve dans les 3 derni\`{e}res  pages, qui sont pr\'{e}c\'{e}d\'{e}es par une longue et confuse introduction qui m\'{e}lange le langage de l'OEB avec des confessions de foi sur l'importance des brevets et du logiciel propri\'{e}taire, en insinuant une corr\'{e}lation entre les deux.   Ce texte ignore les opinions d'\`{a} peu pr\`{e}s tous les developpeurs de logiciel respect\'{e}s, en citant comme sa seule source d'information sur la r\'{e}alit\'{e} du logiciel deux \'{e}tudes non-publi\'{e}es de BSA et amis (alliance pour l'application du droit d'auteur domin\'{e}e par Microsoft et autres grandes entreprises am\'{e}ricaines) sur l'importance du logiciel propri\'{e}taire.  Ces \'{e}tudes ne tra\^{\i}tent m\^{e}me pas du th\`{e}me brevet!  L'introduction et la proposition elle-m\^{e}me a apparemment \'{e}t\'{e} \'{e}crite pour la CCE par un employ\'{e} de BSA.  Ci-dessous nous citons le propos complet, en ajoutant des preuves sur le r\^{o}le de BSA aussi qu'une analyse du contenu, bas\'{e}e sur une comparaison en tableau des versions de BSA et de la CCE avec une version d\'{e}crypt\'{e}e, bas\'{e}e sur la Convention sur le Brevet Europ\'{e}en et des doctrines y \'{e}tant li\'{e}es qui se trouvent dans les r\`{e}gles d'examen de l'OEB de 1978 aussi que dans la juridiction de l'\'{e}poque.  Cette version OEB vous aidera \`{a} appr\'{e}cier la clart\'{e} et la sagesse des r\`{e}gles de brevetabilit\'{e} de la loi actuelle, qui a fait l'objet d'intenses efforts de d\'{e}tournement par l'OEB et le cercle d'amis des juristes du brevet qui dominent le dossier au sein la Commission Europ\'{e}enne.
\end{quote}
\filbreak

\item
{\bf {\bf R\'{e}gle de loi sur le concepte d'invention du syst\`{e}me Europ\'{e}en de brevets et son interpr\'{e}tation avec consideration sp\'{e}ciale pour les logiciels\footnote{http://swpat.ffii.org/analyse/javni/index.de.html}}}

\begin{quote}
Nous proposons au l\'{e}gislateur de baser son propos de directive sur la question de la brevetabilit\'{e} du logiciel sur ce texte court et clair.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

