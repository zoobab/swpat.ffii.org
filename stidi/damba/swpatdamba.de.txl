<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Vee: Vorbeugung gegen kommende Patenklagen

#Utn: Unabhängig davon, ob es uns diesmal gelingt, die Patentlobby in ihre Schranken zu weisen, müssen wir uns langfristig mit dem Patentwesen auseinandersetzen.

#Ntr: Neuheit widerlegen

#Bns: Branchenvereinbarungen, Schutzbündnisse, Versicherungen

#Wtl: Wir haben bereits begonnen, den %(pa:Stand der Technik bei allen auf FFII.ORG beherbergten Projekten täglich mit Zeitstempel zu dokumentieren).  Diese Dokumentation wird ein mal im Monat in einer öffentlichen Bibliothek archiviert.  Das System wird derzeit probeweise auch auf andere Server ausgeweitet.

#GnW: Gegen frivole Patentklagen hilft letztlich nur der Nachweis, dass %(q:diese Technik schon dato 19xx-xx-xx bekannt und in Entgegenhaltung Dx dokumentiert) war.  Nur durch Widerlegung der Neuheit kann man ein Patent zu Fall bringen.  Andere Kriterien wie %(q:Erfindungshöhe) sind inzwischen durch die Praxis der Patentämter zur Makulatur geworden.

#DWv: Die Softwarebranche ist nicht gezwungen, sich unsinnigen bürokratischen Praktiken zu unterwerfen.  Mit einigem gutem Willen kann sie sich %(ko:verbünden), um ihre eigenen %(q:Abkommen für den fairen Gebrauch von Patenten) zu schließen und dabei einen erträglichen Ausgleich zwischen den Interessen der Patentinhaber und denen der informationellen Infrastruktur schaffen und durchzusetzen.

#FWb: Ferner wäre es möglich, im Rahmen solcher Bündnisse eine relativ günstige Rechtsschutzversicherung für Programmierer freier Software aufzubauen.  Der Schutz vor frivolen Patentattacken würde dann professionalisiert.  Wir würden zum Gegenangriff übergehen und dafür sorgen, dass sich Streithansel zwei mal überlegen, ob sie ihr Vermögen und ihren Ruf riskieren möchten.

#DtW: Das sind Vorstellungen, die bisher an der Unwissenheit und mangelnden Solidarität der Softwarebranche scheitern.  Sie sollten parallel, nicht alternativ zum Kampf gegen die missbräuchliche Ausweitung des Patentrechts umgesetzt werden.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatdamba ;
# txtlang: de ;
# multlin: t ;
# End: ;

