<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: News</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>News</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse6.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="intro.php3" alt="Introduction"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="index.php3" alt="The Freedom CPU Project"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<table>
  <tr bgcolor="#c0d0f0"><td><b>25 October 1998
</b></td></tr>
  <tr><td><blockquote>AlphaRISC has <a href="http://www.egroups.com/list/f-cpu/632.html">released</a>
version -0.1 of an SMT TTA simulator. You can download it via 
<a href="download/tta-sim.tar.gz">HTTP</a> or
<a href="ftp://f-cpu.dyn.ml.org/pub/f-cpu/sim/tta-sim.tar.gz">FTP</a>
and help to complete it if you are so inclined.
<p>Please be advised that the <a href="http://f-cpu.dyn.ml.org">west coast</a>
server may continue to have occasional service outages. If this happens please
use the <a href="mirrors.php3">other mirrors</a>.
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>22 October 1998
</b></td></tr>
  <tr><td><blockquote>The <a href="http://f-cpu.dyn.ml.org">west coast mirror</a> is now up again
after over two days of <tt>DHCP HELL</tt>. If the new IP address hasn't reached
your DNS yet, continue using the <a href="mirrors.php3">other mirrors </a>
(it's a name-based virtual server so a URL with only the IP address won't work).
<p>Also remember - if you want to see any particular information on this
site, please <a href="mailto:brion@pobox.com">let me know</a>, I'm not a
mind reader (believe me, I've tried)!
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>21 October 1998
</b></td></tr>
  <tr><td><blockquote>The <a href="http://f-cpu.dyn.ml.org">west coast mirror</a> is having
network problems, please use one of the
<a href="mirrors.php3">other mirrors</a> for now.
<p>Also remember - if you want to see any particular information on this
site, please <a href="mailto:brion@pobox.com">let me know</a>, I'm not a
mind reader (believe me, I've tried)!
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>5 October 1998
</b></td></tr>
  <tr><td><blockquote>The problems with the 
<a href="../index.html">east coast mirror</a> have been resolved.
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>4 October 1998
</b></td></tr>
  <tr><td><blockquote>We are experiencing some problems with our
<a href="../index.html">east coast mirror</a>, please use one of our
<a href="mirrors.php3">other mirrors</a> until it is resolved.
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>26 September, 1998
</b></td></tr>
  <tr><td><blockquote>We now have an IRC channel up for real-time discussion: #f-cpu on
digital.malicia.com. An IRC meeting may be arranged in the next week or so...
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>24 September, 1998
</b></td></tr>
  <tr><td><blockquote>The current state of the F-CPU architecture, including many of the ideas
discussed on the <a href="feedback.php3">mailing lists</a>, is described in
<a href="F-CPU_ISA.php3">The Freedom CPU ISA and Register Organization</a>.
This document supercedes <b>all</b> other information on the site, so until
the rest of the documents are updated remember to look here!
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>17 September, 1998
</b></td></tr>
  <tr><td><blockquote>A new server has come online as the US west coast mirror! Please use
<a href="http://f-cpu.dyn.ml.org/">f-cpu.dyn.ml.org</a> instead of
siva.usc.edu, or look for a closer <a href="mirrors.php3">mirror site</a>.
<p>The <a href="feedback.php3">feedback</a> page now contains more information
on the mailing lists.
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>14 September, 1998
</b></td></tr>
  <tr><td><blockquote>The mailing list has been split up to reduce traffic. In addition to the
<a href="http://www.egroups.com/list/f-cpu/">main list</a> there is now an
<a href="http://www.egroups.com/list/f-admin/">admin list</a> for discussing
issues related to the administration of the Freedom project as a whole
(financial, organizational, etc.), a
<a href="http://www.egroups.com/list/f-comm/">communications list</a> for
discussing project communications issues (the web site, CVS, mailing lists,
etc), and a
<a href="http://www.egroups.com/list/f-tools/">tools list</a> for discussion
of the EDA tools to be used to design the F1. You can subscribe/unsubscribe
to/from the lists from the above links.
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>13 September, 1998
</b></td></tr>
  <tr><td><blockquote>Just a reminder - the mailing list is archived at
<a href="http://www.egroups.com/list/f-cpu/">http://www.egroups.com/list/f-cpu/</a>.
Please take a look at the archive before posting.
<p>The latest innovative idea for the CPU is I/O-mapped registers -
<a href="http://www.egroups.com/list/f-cpu/234.html">read about it</a>!
<p><a href="http://euro.f-cpu.ml.org/about.php3">European mirror</a> now up! There are
still a few kinks in the system to be worked out but it's working...
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>8 September, 1998
</b></td></tr>
  <tr><td><blockquote>News of the project leaked to <a href="http://slashdot.org">Slashdot</a>!
Welcome fellow /.ers - please read with an open mind and remember the project
is just beginning and we welcome your <a href="feedback.php3">feedback</a>!
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>4 September, 1998
</b></td></tr>
  <tr><td><blockquote>The main site is down due to network troubles. Use one of the
<a href="mirrors.php3">mirrors</a> instead.
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>22 August, 1998
</b></td></tr>
  <tr><td><blockquote>Higher-bandwidth temporary site now available at
<a href="http://siva.usc.edu/~brion/f/">http://siva.usc.edu/~brion/f/</a>.
</blockquote><br></td></tr>

  <tr bgcolor="#c0d0f0"><td><b>12 August, 1998
</b></td></tr>
  <tr><td><blockquote>Web site is up for internal testing. Public
unveiling will be 30 August, 1998.
</blockquote><br></td></tr>

</table>
<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 06 1998 22:47:10 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse6.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
