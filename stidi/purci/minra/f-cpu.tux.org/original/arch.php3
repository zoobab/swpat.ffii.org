<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: The F Architecture</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>The F Architecture</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="faq.php3" alt="Frequently Asked Questions"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="f1.php3" alt="The F1 Implementation"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="docs.php3" alt="Documents"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<P>The exact details of the F architecture are not yet all nailed down, but
here are some of the basics:</P>

<UL>
<LI>64-bit datapath and address space
<LI>Speculative and parallel execution pipelines
<LI>Three cache levels:
    <UL>
    <LI>L0: 8KB, 4-ported, line lockable - internal
    <LI>L1: 64KB data plus 64KB intruction (dual ported, line lockable) - internal
    <LI>L2: &gt; 1MB (possibly!) - external (on motherboard)
    </UL>
<LI>Instead of general-purpose registers, use "virtual registers" which
    correspond to locations in memory, as specified by memory window registers.
    Thus we get the advantages of a memory-to-memory architecure without
    the huge instruction sizes and slow access.
<LI>NUMA-style multiprocessing (not Intel SMP, which is proprietary)
<LI>FPU design is currently in flux, but is currently planned to be internal
(the diagram below is now incorrect)
</UL>

<p><a href="diagrams/new_fcpuarch.gif"><img src="diagrams/new_fcpuarch.gif"
border=0 alt="[Basic CPU architecture]"></A><br>
<font size="-1"><i>Napkin drawing cleaned up by
<a href="seanh@enter.net">Sean</a></i></font></p>

<p><a href="diagrams/F_registers.gif"><img src="diagrams/F_registers.gif"
border=0 alt="[Registers]"></A></p>

<p>You'll find some more details in <a href="Freedom.php3">The Freedom CPU
Architecture</a> and <a href="F-mem.php3">F-CPU Architecture: Register
Organization</a>.</p>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 11 1998 00:32:12 AM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
