<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: Join the team!</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>Join the team!</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse4.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="team.php3" alt="The F-CPU Team"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<h2>Help Wanted!</h2>

<p>We are urgently in need of volunteers with the following competences:</p>

<ul>
<li>A general manager for all our documentation.
<li>A person with legal/patents expertise.
<li>A PR person who will be responsible for our sponsorship work and fundraising.
<li>A treasurer - will need to be a US citizen
<li>A gcc team member who will act as our coordination/relay with the gcc community.
<li>Somebody with some experience with VLSI simulation tools.
<li>The F1 implementation team leader.
<li>The F architecture team leader.
<li>Our foundries specialist. Will negotiate with foundries in the future. Must
    track foundry (technological, quality) performance and prices.
<li>The Benchmarking team leader.
<li>The BIOS/boot team leader.
<li>The x86 emulation team leader.
<li>Our packaging/mechanical team leader.
<li>A Thermal design Guy/Gal. Will also manage all CPU power down functionality.
<li>The FPU design team leader.
<li>The Multiprocessing Guy/Gal.
<li>Our DMA controller Guy/Gal.
<li>Our Memory Technology watcher.
<li>We also need somebody with L0/L1/L2 cache design experience.</ul>

<p>People need not be locked into any one position, nor is there any requirement
that there can be only one of each.</p>

<p>Do you want to join our Freedom Project? Yes? Good! Please read on.</p>

<h2>Requirements</h2>

<p>Are there any specific requirements? No. Basically, all we want is that
people who join us be able to work in teams. That means a lot in terms of
character, cultural tolerance, respect for others, politeness, ability to share
your knowledge with others, and the ability to listen and learn from others.</p>

<h2>Guaranteeing your Freedom</h2>

<p>We have a few "organizational rules" that guarantee the freedom to create and
the freedom to express yourself:
<ol>
<li>Our main mailing list is totally open. Anybody can subscribe, anybody can
post to it. It's not moderated.
<li>All Good Ideas (tm) will be considered. Nobody (really, nobody) can decide
singlehandedly on what what goes in and what gets rejected. However, if you have
a Good Idea (tm), at least you should make an effort to explain why it is good.
<li>The entire Project informational content is available in various CVS trees
(not yet implemented)
which are all available from the Web (on a read-only basis for non-members).
Every single bit of data concerning the project is placed under the GNU/GPL.
<li>There is no "superior" knowledge in this project. If you don't know anything
about VHDL, but have some very good ideas about how to handle negotiations with
suppliers, then by all means join the Project: we need you to handle the
manufacturing phase of the project, when we'll contact various foundries.
Similarly, if you care about good Web site design, but are not really interested
in CPU architecture "per se", we <i>want</i> you :-)
<a href="team.php3#brion">Brion</a> could use some help, given the amount of
work he has been faced with.
Summarizing: we want competence and the good will to use for our Project.
<li>There are no "Knowledge Feuds" or "Competence Compartments". Nobody "owns"
any part of the project. Cross-field knowledge is encouraged and <i>needed</i>.
Note that on the other hand excellence is usually only achieved at the expense
of some degree of specialization...
<li>Whenever you feel there is something wrong in any part of the Project,
express yourself! We'll listen. :-)
<li>Please be kind to newcomers. It takes some time to "get up to speed" with
the rest of the project. If you learned something from your participation in
the Project, also teach it to somebody else.
<li>IBM used to have a single word that defined the recommended mental attitude
for all its employees: "Think". We would like to propose a different one:
"Learn". :-)
</ol>

<h2>The Learning Curve</h2>

<p>The Freedom Project is about complexity. To master this complexity, we had to
standardize on some tools, lest we would fall prey to the "Babel effect". To
really integrate your work with the main body of the Freedom Project
informational content, you'll need access to some hardware and you'll have to
learn to use some software tools.</p>

<p>First and foremost, you need access to a microcomputer connected to the Web.
You'll also need an email address. Our recommended configuration is a GNU/Linux
machine with X, although users of other operating systems are welcome -
especially other open OSes which could be ported to the Freedom architecture.</p>

<p>Then you need to learn some basic CVS principles. This is really <i>very</i>,
<i>very</i> easy. There are some tutorials on the Web, so learning the basic CVS
operations takes just 15 minutes. Having learned the basic principles of CVS,
you can use a GUI client to access our CVS trees; this avoids having to learn
and memorize the usual assortment of cryptic commands.</p>

<p>Our documentation is entirely maintained in SGML DocBook DTD format. This
sounds complicated, doesn't it? Well, it's not. You'll need to learn to use some
very simple translation tools, but once you have the translation steps ready,
you can even use an HTML editor to work on any part of our documentation.</p>

<p>Or you can use a plain and simple text editor. Or, you can use our
recommended tools for documentation work: LyX (or its KDE cousin, Klyx). LyX can
be described as a document processor, which is a word processor with a twist,
really. It takes about one hour to get used to the basic operations, and after
you get to write a few pages with it you'll feel it's a great tool.</p>

<p>Note that SGML is the documentation format of the future. It's also an
International Standard.</p>

<p>Finally, if you are going to work on the F1 implementation, you'll have to
learn VHDL. VHDL is not too tough, either. There are many tutorials on the Web,
and excellent books and Web sites dedicated entirely to VHDL. More on VHDL tools
as the Project progresses.</p>


<h2>Return on Investment</h2>

<p>What do you get back from the Project? A lot!</p>

<p>First you get the opportunity to meet and discuss ideas with a lot of very
intelligent people all over the world, people that you would never meet
otherwise.</p>

<p>You also get to work in a free environment, which is designed to stimulate
your creativity. You can set the amount of pressure that fits your working
habits better. :-)</p>

<p>You'll also learn a lot, in two ways:</p>
<ul>
<li>by listening to others, and
<li>by doing things yourself.
</ul>

<p>Finally, Project members get early access to our first batch(es) of Freedom
F1 CPUs. Imagine plugging one of these jewels in your trusty Socket 7
motherboard and launching Linux on it! Especially if you have contributed to the
Project's success. :-)</p>


<h2>Actual steps for joining</h2>

<ol>
<li>Perhaps the first step is to join the main mailing list, listen to the posts
for some days, and then decide on what part of the project interests you most.
<li>Then you can either join one of the teams, or create your own. In both cases,
there will be a specific mailing list. At this point you should request write
access to the CVS tree that matters to you.
<li>Now you are all set. "Just do it"!
</ol>


<h3>Mailing List</h3>

<p>If you join the team, you'll want to join our mailing list so we can
communicate! See the <a href="feedback.php3">feedback</a> page for more
information on the lists.</p>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 17 1998 20:03:20 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse4.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
