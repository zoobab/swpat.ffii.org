<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: Mirror Sites</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>Mirror Sites</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse3.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="about.php3" alt="About This Server"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="feedback.php3" alt="Feedback"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<h2>United States - West Coast</h2>
<ul>
<li><b><a href="http://f-cpu.dyn.ml.org/">http://f-cpu.dyn.ml.org/</a></b> (<a href="http://f-cpu.dyn.ml.org/about.php3">About site</a>)
</ul>

<h2>United States - East Coast</h2>
<ul>
<li><b><a href="../index.html">http://f-cpu.tux.org/</a></b> (<a href="http://f-cpu.tux.org/about.php3">About site</a>)
</ul>

<h2>Europe</h2>
<ul>
<li><b><a href="http://euro.f-cpu.ml.org/">http://euro.f-cpu.ml.org/</a></b> (<a href="http://euro.f-cpu.ml.org/about.php3">About site</a>)
</ul>


<hr>

<h2>Becoming a Mirror Site</h2>

<p>If you'd like to provide a location for an F-CPU Project mirror site, we'd
love to hear from you! Keep in mind though that we already have two sites in the
United States and are in the process of setting one up in Europe.
First make sure you can meet these basic requirements:</p>

<ul>
<li>A permanent, fairly high-speed connection to the internet
<li>A static IP address or hostname (<a href="http://ml.org">.dyn.ml.org</a>
    or similar is okay)
<li><a href="http://www.apache.org">Apache</a> 1.2 or higher or another web
    server capable of running CGI programs
<li><a href="http://www.php.net">PHP</a> 3.0.2a or higher
<li><a href="http://www.ssh.fi">SSH</a> 1.2.25 or higher
    and <a href="http://samba.anu.edu.au/rsync/">rsync 2.1.0</a> (for secure
    transfer of site contents)
<li>Enough control over your server that you can add a directory for us and our
    own user account
<li>Also, we'd like to be able to collect web server statistics, if possible in
    Apache's combined log format.
</ul>

<p>We really don't care what hardware, processor, or operating system you use,
although SSH pretty much limits it to UNIX-like platforms. Anyway, if you think
you can handle it, e-mail our <a href="mailto:brion@pobox.com">webmaster</a>
and we'll work it out.</p>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated October 04 1998 21:58:49 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse3.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
