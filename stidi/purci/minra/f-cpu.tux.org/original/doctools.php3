<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: Documentation Tools</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>Documentation Tools</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse5.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="F-mem.php3" alt="F-CPU Architecture: Register Organization"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="docs.php3" alt="Documents"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<p>All <a href="cvsdocs.php3">F-CPU Project documentation</a> is written in
SGML using the DocBook DTD. Or maybe just plain LyX, it's still up in the air
right now... Project members writing documentation may benefit
from the use of these tools:</i></p>
<ul>
<li><a href="http://www.lyx.org/">LyX</a> - a WYSIWYM document processor
<li><a href="http://www-pu.informatik.uni-tuebingen.de/users/ettrich/klyx/klyx.html"
    >KLyX</a> - the same with a KDE interface
<li><a href="http://www.sgmltools.org/">SGMLtools</a> - SGML syntax checker and
    SGML-to-anything converter
<li><a href="http://www.oreilly.com/davenport/">The Davenport Group</a> -
    maintainers of the DocBook DTD
<li><a href="http://nis-www.lanl.gov/~rosalia/mydocs/docbook-intro.html"
    >DocBook Intro</a> - Mark Galassi's tutorial on DocBook
<li><a href="http://wwwiti.cs.uni-magdeburg.de/~sattler/killustrator.html"
    >KIllustrator</a> - a vector drawing program, may be useful for diagrams
</ul>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated August 23 1998 17:19:17 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse5.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
