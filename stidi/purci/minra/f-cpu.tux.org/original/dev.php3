<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: Development</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>Development</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="intro.php3" alt="Introduction"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="docs.php3" alt="Documents"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="index.php3" alt="The Freedom CPU Project"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<h2>Declaration of Principles</h2>

<!--<P>Open, free exchange of information blah blah blah. Will fill in later.
</P>-->

<ul>

<li>This Project exists primarily as an experiment to answer the question
    "Can a CPU be developed openly in a bazaar-style distributed development
    environment?" Technological innovation is always welcome but should in
    general err on the side of practicality - a really neat new idea for the
    architecture won't do any good if we can't actually build it.

<li>The name of the game is Freedom, so our designs are being developed openly
    and will be openly distributable under a GNU GPL-like license, so anyone will
    be able to (if they have the funding at least) take our designs and
    manufacture and sell their own F1 or derivative chips, but any changes
    will have to be made freely available again.

<li>We are aware of the extreme ambitiousness of this Project, but we believe
    it to be necessary for the continued existence of free software in a world
    of increasingly proprietary hardware, so we will persevere until we are
    successful.

<li>We are not an ivory tower or a "cathedral". Anyone may join the team and
    contribute - or even contribute without officially "joining" in any way.
    Even those with limited or no knowledge of CPU development can have
    something to contribute. After all, people with no knowledge of how to
    design a CPU will want to use our CPU once it is complete, so we should
    pay attention to what they want in a CPU. From the mouths of babes and all...

<li>Remember, here at the Freedom CPU Project we are <b>not</b> anti-Intel,
    anti-Microsoft, or in fact anti-anything. We are only pro-Freedom!

<li>Never flame, never respond to flame bait, but please do make and take
    constructive criticism.

</ul>

<hr>

<h2>Schedule</h2>

<P>An exact schedule has not yet been worked out; however the basic tasks are
as follows:</p>
<blockquote>
0) Document<br>
1) Design<br>
2) VHDL<br>
3) Simulate.<br>
4) FPGA prototype<br>
5) Implement<br>
6) BIOS work<br>
7) gcc work<br>
8) Linux kernel work<br>
9) Benchmark<br>
10) Tape out<br>
11) Finance<br>
12) Distribute<br>
13) Enjoy :-)
</blockquote>

<p>Documentation is a basic thing that needs to happen throughout, hence it is
given number 0. Since we intend to
catch up with Intel's Merced and ship in 2000, it will be important to do as
much of tasks 1-11 as possible in parallel - much like the CPU itself, we
must be able to take advantage of multiple team members and groups who can
perform relatively independant tasks simultaneously.</p>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 09 1998 02:32:12 AM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
