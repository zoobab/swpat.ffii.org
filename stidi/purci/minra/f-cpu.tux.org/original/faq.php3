<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: FAQ</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>FAQ</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse6.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="arch.php3" alt="The F Architecture"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="docs.php3" alt="Documents"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<style type="text/css"><!--
.q {
  font-family: "Times New Roman", "Times", "serif";
  font-style: italic;
  color: #8b4513;
  margin-left: .25in;
}
.a {
  font-family: "Times New Roman", "Times", "serif";
  font-style: italic;
  color: #6b8e23;
  margin-left: .5in;
}
.m {
  font-family: "Times New Roman", "Times", "serif";
  font-style: italic;
  margin-left: .5in;
}
--></style>

<H3>Philosophy</H3>

<p class=q><B>Q1:</B> What does the F in F-CPU stand for?</p>

<P class=a><B>A:</B> It stands for Freedom,
which is the original name of the architecture, or Free, in the GNU/GPL
sense.</p>

<P class=m>The F does <B>not</B> stand for
free in a monetary sense. You will have to pay for the F1 chip, just as
you have to pay nowadays for a copy of a GNU/Linux distribution on CD-ROMs.
Of course, you're free to take the design and masks to
your favorite fab and have a few batches manufactured for your own
use.</p>

<P class=q><B>Q2:</B> Why not call it an
O-CPU (where O stands for Open)?</p>

<P class=a><B>A: </B>There are some fundamental
philosophical differences between the Open Source movement and the original
Free Software movement. We abide by the latter, hence the F.</p>

<P class=m>The fact that a piece of code is labeled Open Source
doesn't mean that your freedom to use it, understand it and improve upon
it is guaranteed. Further discussion of these matters can be found
<A href="http://www.gnu.org">here</A>.</p>


<H3>Architecture</H3>

<P class=q><B>Q1:</B> Why did you choose
a memory-to-memory architecture? Why not a register-to-register architecture
like all other RISC processors?</p>

<P class=a><B>A:</B> Basically, for performance:
we wanted to improve on RISC, so we had to look for a different solution.
One of the critical performance bottlenecks in modern Operating Systems
is the context switch latency, basically caused by saving and restoring
register sets. The F architecture provides near-zero context switch latencies.
However, there are many RISC-like ideas in the F architecture.</p>

<P class=m>We are working on a White Paper which discusses this
issue in detail. A preliminary version can be found
<A href="F-mem.php3">here</A>.</P>


<P class=q><B>Q2:</B> Why an external FPU?</P>

<P class=a><B>A:</B> This is implementation
specific and depends entirely on how many gates we can fit in a single
122 mm2 die. The F2 could have up to 4 FPUs on chip, since it will be manufactured
using 0.18 micron technology. The architecture only states that up to four
FPUs can be used in parallel with the main CPU.</P>


<P class=q><B>Q3:</B> Why don't you support SMP?</P>

<P class=a><B>A:</B> SMP is an Intel-proprietary
implementation of Symmetric Multi-Processing. In our case, it is not even
desirable to implement shared-bus multiprocessing, because the F1-CPU already
has a very high rate of bus utilization. Adding more processors would inevitably
mean creating a bottleneck that would severely limit performance. We opted
for a novel implementation of NUMA-style multiprocessing, which allows
both shared memory (with cache-coherency) and message-passing.</P>


<H3>Performance</H3>

<P class=q><B>Q1:</B> Wat can we expect
in terms of performance from the F1 CPU?</P>

<P class=a><B>A:</B> Merced-killer. :-).</P>

<P class=m>Although the F1 CPU will probably run at a lower clock
rate compared to the Merced, due to improvements in design (geared to achieving
very high levels of parallelism at execution time) and gcc and kernel-specific
optimizations, it is expected that integer performance will be comparable
to or better than the Merced.</P>

<P class=m>FPU performance will basically depend on the number
of FPUs plugged on the CPU bus. With two FPUs the F1 will probably provide
better performance than a Merced. With four FPUs it should leave the Merced
in the dust.</P>

<P class=m>Beowulf F supercomputer implementations should provide
superior performance, because of the exceptional bandwidth and low latencies
of our NUMA-style multiprocessing mechanism.</P>

<P class=m>Yeah, we know that sounds a little bit ambitious...</P>


<H3>Compatibility</H3>

<P class=q><B>Q1:</B> Will the F1 run my DOS software?</P>

<P class=a><B>A:</B> Maybe. We are planning
to build in an x86 emulator in the F1 BIOS. But we only want to boot the
CPU on standard x86 motherboards. We doubt the F1 will support sophisticated
DOS games and other x86-specific packages.</P>


<P class=q><B>Q2:</B> Is the F1 CPU Windows (98, NT) compatible?</P>

<P class=a><B>A:</B> No. It will not run WINE either.</P>

<P class=m>It should however run Windows emulators that include x86 CPU
emulators such as Twin, as well as Windows itself under whole-PC emulators
such as Bochs. In either case however you will need to run another operating
system, such as GNU/Linux, and emulation will likely be fairly slow.</P>

<P class=m>And of course there is the slight possibility that Microsoft will
port Windows NT to the F1, but I wouldn't hold my breath. ;-)</P>

<P class=q><B>Q3:</B> Will I be able to
plug the F1 in a standard Socket 7, Super 7 or Slot 1 motherboard?</p>

<P class=a><B>A: </B>In principle, yes.
This is one of its design parameters.</p>

<P class=m>Your motherboard will have to support the 2.2V rating
of the F1 CPU, and there may be other requirements/limitations. But in
principle there is enough bus bandwidth in a 100 MHz Super 7 or Slot 1
motherboard to support a high-performance CPU like the F1. 66MHz Socket
7 motherboards will be supported too, but their performance will be somewhat
limited.</p>


<P class=q><B>Q4:</B> What OS kernels will the F-CPU support?</p>

<P class=a><B>A:</B> Linux will be ported
first, and MkLinux will perhaps be ported too later on. A port of Linux
will be developed simultaneously with the F1 CPU development.</p>

<P class=m>We will first port gcc to the
F architecture. Basically the F1 will run all the software available for
a standard GNU/Linux distribution.</p>


<H3>Features</H3>

<P class=q><B>Q1:</B> What kind and size of caches will the F1 CPU have?</p>

<P class=a><B>A:</B> Internally, the F1
will have Harvard-style separate caches for instructions and data. We are
planning on 64KB 4-way set associative L1 caches (one each for data and
instructions). We also have small L0 caches for instructions (512 bytes)
and data/virtual registers (8KB).</P>


<H3>Cost/Price/Purchasing</H3>

<P class=q><B>Q1:</B> How much will the F1 CPU cost?</P>

<P class=a><B>A:</B> Our estimates are that an F1 CPU will cost approximately
US$100.</P>

<P class=m>See our <a href="F1-costs.php3">detailed cost estimate</a> if you
don't believe us!</P>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 17 1998 20:03:51 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse6.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
