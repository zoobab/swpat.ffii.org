<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: Feedback</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>Feedback</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse3.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="mirrors.php3" alt="Mirrors"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="sitemap.php3" alt="Site Map"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<p>If you have questions or comments, or want to <a href="join.php3">join</a>
the Project and help out, we recommend joining one of our mailing lists.
Please take a look at the archives before you post, your questions may have
been answered already.</p>

<p>You can view list archives and subscribe to the lists from these sites:</p>

<ul>
<li><a href="http://www.egroups.com/list/f-cpu/">f-cpu</a> - main list
<li><a href="http://www.egroups.com/list/f-admin/">f-admin</a> - discussion
    pertaining to the administration of the project as a whole
<li><a href="http://www.egroups.com/list/f-comm/">f-comm</a> - discussion
    of project communication issues - web site, FTP, mailing list admin,
    documentation, that sort of thing
<li><a href="http://www.egroups.com/list/f-tools/">f-tools</a> - discussion
    of the EDA tools that we will be using to design the F1
<li><a href="http://www.egroups.com/list/f-mainboard/">f-mainboard</a> -
    designing a new motherboard to take advantage of the F-CPU architecture is
    outside of the goals of this project, but a lot of people are interested
    in doing it. If you are, please discuss it here and not in the main
    project list.
</ul>

<p>We also have an IRC channel up for real-time discussion: #f-cpu on
digital.malicia.com.</p>

<p>Questions/comments that pertain only to the web site itself should be
directed to <a href="mailto:brion@pobox.com">our webmaster</a>. Please note
that we are having trouble with some parts of the site at the moment - we
didn't expect the leak to <a href="http://slashdot.org">Slashdot</a> so soon and
haven't worked out all the bugs yet.</p>

<!--<p>Feedback: we want it!</p>

<p><b>Huge honkin' note: This form doesn't work right now since the computer
it sends mail to is DOWN.</b> Instead consider sending e-mail to the list.</p>

<form action="processfb.php3" method="post">
Your Name: <input type=text size=40 name="name"><br>
Your e-mail address: <input type=text size=40 name="email"><br>
This is your <input type="radio" name="time" value="first">first
/ <input type="radio" name="time" value="notfirst">not first visit to this
site.<br>

</form>
-->

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated October 25 1998 01:53:23 AM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse3.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
