<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: The Team</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>The Team</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse15.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="links.php3" alt="Links and Tools"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="admin.php3" alt="Administrivia"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="index.php3" alt="The Freedom CPU Project"></area>
<area shape=polygon coords="31,45, 45,45, 45,60, 31,60, 31,45" href="join.php3" alt="Join the Team!"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<p>These are the people working to bring you the Freedom CPU architecture:</p>

<P><a name="andrebalsa"></a>
<B><A href="mailto:andrebalsa@altern.org">Andrew Derrick Balsa</A></B></P>
<table><tr>
<td><IMG src="images/andrewbalsa.jpg" width=100 height=100 alt="Picture"></td>
<td>
&quot;I  built my first microcomputer at 15, studied CS at the
<A href="http://www.unige.ch/" target=_top>University of Geneva</A>
for three years, worked as a firmware engineer in Brazil, then studied Public
Management for 5 years, working as a IT consultant at the same time, then
worked full-time as a hardware/firmware consultant, and finally went back to
studying Marketing and Management (Human Ressources) at Ph.D. level. I still
intend to write my thesis someday on `Ethical issues in multi-cultural
organizations'. I now live in France and work on Internet-related security and
commercial matters.&quot;
</td></tr></table>

<P><a name="Richard.Gooch"></a>
<B><A href="mailto:Richard.Gooch@atnf.CSIRO.AU">Richard Gooch</A>:
Linux Kernel Guy</B></P>
<blockquote>
Richard is an Australian astrophysicist preparing his Ph.D. on astronomic
visualization; he developed the Pentium Pro MTRR support in the Linux 2.1.x
kernels (as well as other novel kernel routines), and is also a hardware
developer. 
</blockquote>

<P><a name="rreilova"></a>
<B><A href="mailto:rreilova@ececs.uc.edu">Rafael Reilova</A></B></P>
<blockquote>
Rafael is currently pursuing a master's degree in computer engineering at
the <a href="http://www.uc.edu/">University of Cincinnati</a>. His interests
include VLSI design, reconfigurable/adaptive computing, FPGA's, operating
systems (esp. Linux kernel hacking), computer architecture, and enjoying the
sun and the beach at his beutiful island home of Puerto Rico. Currently his real
reseach-work involves writting an EDA place-and-route tool for hierachical
FPGA's.
</blockquote>

<P><a name="brion"></a>
<B><A href="mailto:brion@pobox.com">Brion Vibber</A>: Web Guy</B></p>
<table><tr>
<td><IMG src="images/brion.jpg" width=100 height=100 alt="Picture"></td>
<td>
&quot;I am a <A href="http://cinema-tv.usc.edu/" target=_top>film student</A> with no formal
training in computers aside from touch-typing, which I threw away when I went
<A href="http://www.dvorakint.org/" target=_top>Dvorak</A>. I dabble in
<A href="http://pobox.com/~brion/linux/" target=_top>Perl and Linux kernel hacking</A> (but
rarely at the same time), maintain <A href="http://www.pobox.com/~brion/" target=_top>a few
small websites</A>, and am employed as a part-time sysadmin in a
<A href="http://siva.usc.edu/coglab/" target=_top>research lab</A> at the
<A href="http://www.usc.edu/" target=_top>University of Southern California</A>.&quot;
</td></tr></table>

<p>We're always looking for new members to help us out! If you're interested,
<a href="join.php3">please join us</a>!</p>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated August 23 1998 17:23:00 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse15.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
