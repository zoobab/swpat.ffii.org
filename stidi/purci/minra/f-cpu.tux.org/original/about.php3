<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: About This Server: f-cpu.tux.org</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>About This Server: f-cpu.tux.org</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse3.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="index.php3" alt="The Freedom CPU Project"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="mirrors.php3" alt="Mirrors"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<p>
<a href="http://www.tux.org">Tux.org</a> maintains mirrors of a number of
Linux-related projects, and they are kind enough to be lending a little of
their server space to the Freedom CPU Project. Thanks guys! Even if we did
sort of take it up first before asking. ;)
</p>
<p>
This server is located on the east coast of the United States; if you are
elsewhere you may want to look for a <a href="mirrors.php3">closer
mirror</a>.
</p>
<p>This site has had <img src="../cgi-bin/Count.cgi?df=fcpu|ft=0|dd=B|incr=F"> hits to the <a
href="index.php3">main page</a>.</p>

<hr>
<h3 align=center>
The F-CPU Project's web site is powered by the following Free software:
</h3>

<table>
<tr><td><a href="http://www.linux.org"><img src="images/powered_linux.gif" width=110 height=119 border=0 align=middle alt="Linux Powered"></a></td>
<td>This site is developed exclusively on <a
href="http://www.linux.org">GNU/Linux</a> systems, and currently
all of our web mirrors are running GNU/Linux as well. Why? Because it is
reliable, portable, and fun to tinker with.</td></tr>

<tr><td><a href="http://www.apache.org"><img src="images/apache_pb.gif" width=259 height=32 border=0 align=middle alt="Powered by Apache"></a></td>
<td>Our web servers run the Apache HTTP daemon - it is flexible and reliable
and we can't imagine a world without it.</td></tr>

<tr><td><a href="http://www.perl.com"><img src="images/sm_perl_id_313_wt.gif" width=97
 height=97 border=0 align=middle alt="Programming Republic of Perl"></a></td>
<td>Several <a href="http://www.perl.com">Perl</a> scripts are used to convert
the HTML-ized <a href="docs.php3">SGML documents</a> to fit into the site for
easier online viewing and to generate the list of their contents.</td></tr>

<tr><td><a href="http://www.php.net"><img src="images/phplogolt.gif" width=88 height=31 border=0 align=middle alt="Powered by PHP"></a></td>
<td>This site heavily uses <a href="http://www.php.net">PHP</a> scripting to
maintain a consistent look with dynamically generated headers and footers and
to generate some information on the fly, such as the
<a href="news.php3">news page</a>.</td></tr>

<tr><td><a href="http://www.gimp.org"><img src="images/gfx_by_gimp.gif" width=90 height=36 border=0 align=middle alt="Graphics by Gimp"></a></td>
<td>The <a href="http://www.gimp.org">GNU Image Manipulation Program</a> is
nearly equivalent to <a href="http://www.adobe.com">Adobe Photoshop</a>. So
near in fact that Adobe recommends it to Linux users who cannot use Photoshop
themselves!</td></tr>
</table>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 17 1998 19:49:40 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse3.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
