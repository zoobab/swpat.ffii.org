<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: Introduction to the F-CPU Project</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>Introduction to the F-CPU Project</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="news.php3" alt="News"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="dev.php3" alt="Development"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="index.php3" alt="The Freedom CPU Project"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<p>The F-CPU Project was created with two ideas:</p>

<UL>
<LI>That hardware and software are really two instances of the same concept:
    an information processing mechanism.</LI>
<LI>That intellectual freedom, as the underlying idea behind Free Software
    and the GNU/GPL license, should be extended to encompass all the desktop.</LI>
</UL>

<p>From these two basic ideas followed the mission of the F-CPU Project: to
design an advanced, high-performance 64-bit architecture that will easily
take us well into the next century, and to work towards the first implementation
of this new architecture: the F1 processor, compatible with x86 commodity
hardware.</p>

<p>Not only will the architecture and its implementation be entirely placed
under the GNU/GPL (or a license very similar to it - due to <a
href="legal.php3">legal issues</a> it may not be applicable as is), but also
the development process itself is designed to follow the principle of freedom:
freedom to create, freedom to contribute, freedom to learn.</p>

<p>For more details on how this project came to be, read
<a href="Freedom.php3">The Freedom CPU Architecture: A GNU/GPL'ed
high-performance 64-bit microprocessor developed in an open, Web-wide
collaborative environment.</a></p>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 06 1998 20:19:25 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
