<lingoteach>
<title name="test">
 <translation language="english">A simple Test File containing a LingoTeach lesson</translation>
 <translation language="german">Eine einfache Testdatei die eine LingoTeach Lektion enthaelt</translation>
</title>
<meaning name="1">
 <translation language="english">A meaning</translation>
 <translation language="german">Eine Bedeutung</translation>
</meaning>
<meaning name="2">
 <translation language="english">Another meaning</translation>
 <translation language="german">Eine weitere Bedeutung</translation>
</meaning>
</lingoteach>
