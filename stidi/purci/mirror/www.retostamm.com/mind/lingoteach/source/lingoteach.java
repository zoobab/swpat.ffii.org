/*
  This program teaches you another language.
  Copyright (C) 1999 name of author
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
  02111-1307, USA.

*/
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

import java.applet.*;

public class lingoteach extends Dialog{

    /** The Last question's status **/
    Label status;

    /** The Question **/
    Label question;

    /** The number of answer buttons **/
    public static final int ANSWERS = 4;

    /** The buttons with the answers **/
    Button[] answer;

    /** Ask another question **/
    Button ask;

    /** Swaps the Question and the Answers **/
    Button swap;

    /** Contains all the Translations **/
    Hashtable data;

    /** This knows what has been thought before **/
    Enumeration enum;

    /** Contains the correct answer for the current Question **/
    String correct = "";


    /** Initializes everything **/
    public lingoteach(Frame parent, String language){
        super(parent, "-",
              false);

        Properties props = new Properties(System.getProperties());
        try{
            try{
                props.load
                    (new BufferedInputStream
                     (new
                      FileInputStream
                      (System.getProperty("system.language")+".localized")));
            }
            catch (Exception e){
                props.load
                    (new BufferedInputStream
                     (new
                      FileInputStream("en.localized")));
            }
                
            System.setProperties(props);
        }
        catch (Exception e){
            System.out.println(e.toString());
            System.exit(10);
        }

        setTitle(System.getProperty("localize.AppTitle"));

        this.setLayout(new GridLayout(0,1,10,10));

        status = new Label
            (System.getProperty("localize.Status"));        

        this.add(status);

        question = new Label
            (System.getProperty("localize.Question"));
        
        this.add(question);
        
        setSize(500, 300);        

        answer = new Button[ANSWERS];
        
        for (int i = 0; i< ANSWERS; i++){
            answer[i] = new Button
                (System.getProperty("localize.Answer"));

            this.add(answer[i]);
            answer[i].addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    Evaluate(e.getActionCommand());
                }
            });

        }

        // A spacer for looks
        this.add(new Label(""));

        ask = new Button
            (System.getProperty("localize.AskMe"));
        this.add(ask);
        
        ask.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Ask();
            }
        });
        
             
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e) 
                {System.exit(10);}
        });

        swap = new Button
            (System.getProperty("localize.Swap"));
        this.add(swap);
        
        swap.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Swap();
            }
        });
        
             
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e) 
                {System.exit(10);}
        });

        data = new Hashtable();

        readFile(language);
        
        Ask();

        show();
    }

    /** Asks a new question **/
    public void Ask(){
        if (enum == null){
            enum = data.keys();
        }
        if (!enum.hasMoreElements()){
            enum = data.keys();
        }
        String q = (String)enum.nextElement();
        
        question.setText(q);
        correct = (String)data.get(q);
        
        Random rnd = new Random();
        // Put noise on the button
        for (int i = 0; i< ANSWERS; i++){
            Enumeration noise = data.keys();
            int limit = (int)(rnd.nextFloat()*data.size());
            for(int e=0; e<limit; e++){
                answer[i].setLabel
                    ((String)data.get((String)noise.nextElement()));
            }
        }
        // Put the correct one somewhere
        answer[ (int)(rnd.nextFloat()*ANSWERS)].setLabel((String)data.get(q));

    }

    /** Swaps the Hash around **/
    public void Swap(){
        Hashtable temp = new Hashtable();
        Enumeration all = data.keys();
        while (all.hasMoreElements()){
            String i = (String)all.nextElement();
            temp.put((String)data.get(i),i);


        }
	enum = temp.keys();
	all = temp.keys();
        while (enum.hasMoreElements()){
            String i = (String)all.nextElement();
            if (i.equals(correct))
                break;
            enum.nextElement();
	}

        data = temp;
        status.setText(System.getProperty("localize.Status"));
        Ask();
    }

    /** Checks if the Button that was pressed (which has the value in
        selected) was the right button, tells the student, and asks a
        new question if it was right. **/

    public void Evaluate(String selected){
        if (selected.equals(correct)){
            status.setText(System.getProperty("localize.Correct"));
            Ask();
        }
        else{
            status.setText(System.getProperty("localize.Wrong"));
        }
    }

    /** Reads the file that contains the Words and sentences that
        should be learned **/
    public void readFile(String language){
        BufferedReader lang = null;
        try{
            lang = new BufferedReader
                (new InputStreamReader
                 (new FileInputStream 
                  (new File(language+".lingo"))));
            while (lang.ready()){
                String line = lang.readLine();
                StringTokenizer t = new StringTokenizer(line, "|", false);
                data.put(t.nextToken(), t.nextToken());
            }
        }
        catch (Exception e){
            System.out.println("Reading Data File: "+e.toString());
            System.exit(10);
        }
        finally{
            if (lang!=null){
                try{
                    lang.close();
                }
                catch (java.io.IOException e){
                    System.out.println(e.toString());
                    System.exit(10);
                }
            }
        }
        
    }

    /** Starts the Application **/
    public static void main(String argv[]){

        Frame f = new Frame("LingoTeach");
        lingoteach d = new lingoteach(f, argv[0]);
        f.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e) 
                {System.exit(10);}
        });
    }

}
//  LocalWords:  AppTitle AskMe LingoTeach
