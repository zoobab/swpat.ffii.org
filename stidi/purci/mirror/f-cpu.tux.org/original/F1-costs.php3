<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
 <META NAME="GENERATOR" CONTENT="SGML-Tools 1.0.6">
 <TITLE>Estimating final end-user price of the F1 CPU</TITLE>


</HEAD>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: Estimating final end-user price of the F1 CPU</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>Estimating final end-user price of the F1 CPU</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="Freedom.php3" alt="The Freedom CPU Architecture"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="F-mem.php3" alt="F-CPU Architecture: Register Organization"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="docs.php3" alt="Documents"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<H1>Estimating final end-user price of the F1 CPU</H1>

<H2>Andrew D. Balsa</H2>August 1998
<P><HR>
<EM>An estimate of the end-user price of the F1 CPU. GNU/GPL</EM>
<HR>
<H2><A NAME="s1">1. Introduction</A></H2>

<P>
<P>Although I am not an experienced cost analyst for the microprocessor industry, I do have a few years of university studies on my
shoulders, and many years of experience in the microprocessor industry.
Also keep in mind that actual chip fabrication costs are closely guarded secrets by all the major players in this industry. I only
have some rough data and estimates published in industry magazines. I don't have (yet) any price quotes from foundries.
<P>So: we'll only have final prices in Q4 1999, and the person in charge of watching foundry performance for our first F1 batches will
work out better estimates than these, by then.
<P>But roughly, and with a safety margin, the estimated end-user price of the F1 CPU in Q1 2000 will be around $ 100. This price
includes a safe anti-static packaging, and is FOB our main distribution center which will probably be in the US. We may
arrange to have a European distributor and a Hong-Kong distributor.
<P>
<P>
<H2><A NAME="s2">2. Hypothesis</A></H2>

<P>
<P>
<OL>
<LI>Projected die area of the F1 CPU: 122 mm2 (11 x 11 mm die). This hypothesis is justified by our transistor count estimate:
roughly, comparing the F1 to the original Cyrix 6x86. The 6x86 at 0.5 micron 5-layer was 3.4 million transistors, die area was 
14.5 x 14.5 mm. We want to pack 10 - 11 million transistors in a 11 x 11 mm die using 0.25 micron 5 layer technology. And our chip
will have a much higher percentage of transistors in the caches, compared to the 6x86 (the caches have a highly regular pattern,
and so take less space than the irregular logic). The 10-11 million transistors can be divided as follows: 6-7 million transistors
for the caches, 4-5 million for the rest...

Similar comparisons with the IDT/Centaur C6 and the AMD K6 (both the "old" 0.35 micron version and the new 0.25 micron version) lead
to the same conclusion: 10-11 million transistors is OK for a 122 mm2 die using 0.25 micron 5-layer technology and the cache sizes
that we have chosen.
</LI>
<LI>Packaging similar to the Intel Celeron. We are still working on this idea, but it seems the Celeron packaging (basically a PII
without the cache chips and without the plastic/metal casing) would be the ideal solution. Something similar can be worked on for
the Super 7 motherboards (except we don't need the L2 cache chips and external logic). Mechanical drawings available shortly on the
F-Project Web site. These two form-factors already have widely available motherboards, mounting hardware and cooling solutions.
Both provide 100MHz FSB, AGP video cards, efficient motherboard chipsets and well-known bus interfaces.
</LI>
<LI>Roughly the cost structure for the dies described in Hennessy and Patterson, "Computer Architecture", 2nd Ed., Chapter 1 (there
is some data found in the exercises, too; BTW I found a mistake at the end of paragraph 2 on page 63, it should say "at most"
instead of "at least"; H&amp;P's editors owe me $1).
</LI>
<LI>8-inch wafer costs around $ 3.500 in year 2000. This is based on data I had collected in Q3 and Q4 1997 for 0.5 and 0.35 micron
5-inch wafers, it is possible (and even probable) that costs may be much lower than that in 2000. Right now there is worldwide fab
overcapacity, silicon prices are really low.
</LI>
<LI>Our first F1 batch will count around 5.000 - 10.000 good CPUs. More on this below.
</LI>
<LI>Good masks. Obviously, if the masks are flawed... we could try to sell the dies as key rings, souvenirs, etc. ;-)</LI>
</OL>
<P>
<P>
<H2><A NAME="s3">3. Calculation</A></H2>

<P>
<P>Our 122 mm2 die area gives us 200 dies/8-inch wafer (see an example of such a wafer on Hennessy and Patterson,
page 11).
<P>Roughly, die yield = 0.5 for our 122 mm2 5-layer 0.25 micron CPU (H&amp;P, page 13, updated to reflect better fabs in the year 2000).
<P>We also assume wafer yield = 95%, final test yield = 95%. Testing costs of $500/hour, 20 seconds/CPU. All these are from H&amp;P.
<P>Packaging costs = $25-50. This includes the BGA packaging for the F1 chip itself, the SMD EEPROM for the F1 x86 compatibility BIOS,
a few ssi SMD glue logic chips, decoupling caps and the PCB (4 layer? 6 layer?).
<P>The calculation is really simple then.
<P>Wafer cost: $ 3.500. Good dies: 200 x 0.5 (die yield) x 0.95 (wafer yield) = 95 dies /wafer.
<P>Die cost is now: $ 3500 / 95 &nbsp;= $ 36.85. Testing costs: 500 / 180 (dies tested per hour) = $ 2.80.
<P>So, die + packaging costs: $ 36.85 (good die) + $ 2.80 (testing) + $ 50 (packaging) = $ 89.65.
<P>Final cost: $ 89.65 / 0.95 (final test yield) + $ 2 (anti-static packaging) &nbsp;= $ 96.40.
<P>Shipping to the US distribution center accounts for the remaining $ 3.60. :-)
<P>Roughly, following H&amp;P, this gives us a unit cost of $75-100/good CPU (depending on the cost of packaging), tested, boxed in
anti-static packaging and shipped to the US. Compare that to the estimated initial selling price of the Merced: $ 5.000! Or to the
actual selling price of Intel Xeon processors (which are just glorified Celeron processors) of around $ 2.000. Also compare that to
the $80 selling price of the 266 MHz Celeron...
<P>Yes, it can be done. :-)
<P>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 02 1998 19:00:35 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>

</HTML>
