<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: External Documents and Tools</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>External Documents and Tools</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="docs.php3" alt="Documents"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="team.php3" alt="The F-CPU Team"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="index.php3" alt="The Freedom CPU Project"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<p><i>These are links to documents and tools on other web sites and references
to relevant books.
</i></p>

<h2>Related Web sites</h2>

<UL>
<LI><A href="http://www.cs.wisc.edu/~arch/www/">WWW Computer Architecture Home Page</A>
<LI><A href="ftp://www.cs.adelaide.edu.au/pub/VHDL/VHDL-Cookbook/VHDL-Cookbook.ps.tar.Z"
    >The VHDL Cookbook</A> (PostScript - ??k)
<LI><A href="http://www.ececs.uc.edu/~rmiller/"
    >Rick's Miller homepage</A> (local VHDL guru)
<LI><A href="http://www.vhdl.org/">VHDL International</A>
<li><A href="http://www.cs.uregina.ca:80/~bayko/cpu.html"
    >The Great Microprocessors of the Past and Present page</a> -
    This is a nice list of microprocessor architectures, with some nice
    descriptions and comparisons.
    Among other things, it has a short description of the TMS 9900 microprocessor,
    a memory-to-memory architecture like our Freedom architecture.
<LI><A href="http://cs.et.tudelft.nl/MOVE/documents.html">Documents on the Move
    project</a> -
    includes some docs on transfer-triggered architecture (TTA), which is being
    considered at least for the FPU and possibly the whole CPU
</UL>

<H2>Books</H2>

<dl>

<p><dt><a name="hnp"></a>
"Computer Architecture - a Quantitative Approach" by John L. Hennessy and David
A. Patterson, 2nd Ed., Morgan Kaufmann, 1996.

<dd>This is <i>the</i> reference book on Microprocessor Design, the Bible
of CPU creation.

<p><dt><a name="tanenbaum"></a>
"Structured Computer Organization", Andrew S. Tanenbaum, 3rd. Ed., Prentice Hall
1990.

<dd>Tanenbaum bases his book on the fundamental idea that "Hardware and Software
are logically equivalent." His work also suggests that complexity can be managed
by "layering" the virtual computer system.

<p><dt><a name="stone"></a>
"High-Performance Computer Architecture", Harold S. Stone, 3rd. Ed.,
Addison-Wesley, 1993.

<dd>This book has excellent discussions of various advanced CPU design techniques,
with plenty of examples: pipelines, ALUs, set-associative caches, etc...

<p><dt><a name="osborne"></a>
"Introduction to Microcomputers - Volume 1: Basic Concepts"
by Adam Osborne (Osborne/McGraw-Hill).

<dd>
An excellent introductory book that explains how microprocessors are built
internally. It's a bit outdated, but
well-structured. All concepts are clearly explained, with diagrams.

</dl>

<h2>Tools to download over the Web</h2>

<h3>Simulation Tools:</h3>
<ul>
<li><a href="http://www.cs.washington.edu/homes/pardo/sim.d/index1.d/Index.html"
>Lotsa tools</a>
<li><a href="http://www.freehdl.seul.org">FreeHDL</a>
</ul>


<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 13 1998 15:48:37 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
