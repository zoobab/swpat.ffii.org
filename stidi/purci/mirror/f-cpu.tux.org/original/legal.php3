<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: IP/Legal Issues</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>IP/Legal Issues</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="performance.php3" alt="Performance"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="Freedom.php3" alt="The Freedom CPU Architecture"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="docs.php3" alt="Documents"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<P>What are the legal implication of this kind of undertaking, especially as
regards Intellectual Property law? We are studying the applicability of the
GNU/GPL to hardware designs and masks, but have not yet arrived at any
conclusions.</p>

<p>Of principal concern to this project:</p>

<ul>
<li>Are there any patents that we would need to license? If so, can we do so
    in a manner that will still allow a GPL-like license for the CPU design?
<li>The GPL makes provisions to allow use of non-GPL libraries that are an
    integral part of the target system or compiler, so this could be allowed
    under a similar clause in that the patents are an integral part of the
    target, but how would it affect the ability to redistribute the information
    itself (the design of the CPU)?
<li>If some company takes a freely developed hardware design and makes
    proprietary modifications to it, will we be able to force them to put the
    changes under our license? (That is, will a GPL-like license be enforceable
    on hardware?)
<li>In some cases due to patents it may be impossible to do something within
    GPL-like terms. If a single entity retains copyright of the design it can
    be relicensed to allow someone to make proprietary enhancements with
    permission (and probably with payment that would go to help the continuing
    free development and related free software projects). This would be more
    similar to "cathedral"-style <a href="http://www.fsf.org">FSF</a>
    development than "bazaar"-style <a href="http://www.linux.org">Linux</a>
    development. Is this desirable? It certainly simplifies things but seems
    "less free" and may or may not inhibit contributors who would are forced
    to give up copyright on their contributions.
<li>Should the project incorporate (like the <a href="http://www.fsf.org">FSF</a>
    and <a href="http://www.xfree86.org">XFree86</a>?) If so, should we be a
    non-profit organization concerned only with designing the chip and allowing
    others to pick up the manufacturing, or a for-profit company that shares every
    design of our product with anyone who wants it? Or should we form separate
    non-profit design and for-profit manufacturing companies? Or can we create a
    non-profit company that does deal with manufacturing? (Of course in any case we
    would not physically do the manufacturing: we would give money and the chip
    masks to a company that had a plant, and they would give us the chips, and then
    we'd sell them to customers and - hopefully one day - OEMs.)
</ul>

<p>We don't have any lawyers on <a href="team.php3">the team</a> yet, so we
can't really answer these questions to our satisfaction. If you think you can
help, please <a href="join.php3">join us</a>!</p>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 06 1998 23:58:57 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
