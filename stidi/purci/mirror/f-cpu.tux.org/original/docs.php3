<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: Documents</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>Documents</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse15.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="dev.php3" alt="Development"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="links.php3" alt="Links and Tools"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="index.php3" alt="The Freedom CPU Project"></area>
<area shape=polygon coords="31,45, 45,45, 45,60, 31,60, 31,45" href="faq.php3" alt="Frequently Asked Questions"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<h2>Online Docs</h2>

<blockquote>
<p><b><a href="faq.php3">Frequently Asked Questions</a></b><br>
<a href="team.php3#andrebalsa">Andrew D. Balsa</a> and
<a href="team.php3#brion">Brion Vibber</a>,
September 17, 1998</p>
<blockquote><i>A list of questions that we hope to avoid being asked frequently
by answering them here.</i></blockquote>

<p><b><a href="arch.php3">The F Architecture</a></b><br>
<a href="team.php3#andrebalsa">Andrew D. Balsa</a> and
<a href="team.php3#brion">Brion Vibber</a>,
September 11, 1998</p>
<blockquote><i>An overview of the Freedom CPU architecture.</i></blockquote>

<p><b><a href="f1.php3">The F1 Implementation</a></b><br>
<a href="team.php3#andrebalsa">Andrew D. Balsa</a> and
<a href="team.php3#brion">Brion Vibber</a>,
September 07, 1998</p>
<blockquote><i>An overview of the F1 processor, the initial implementation of
the F-CPU architecture.</i></blockquote>

<p><b><a href="tech.php3">Technology</a></b><br>
<a href="team.php3#andrebalsa">Andrew D. Balsa</a>,
August 23, 1998</p>
<blockquote><i>A brief description of the technology that will go into the F1
processor.</i></blockquote>

<p><b><a href="performance.php3">Performance</a></b><br>
<a href="team.php3#andrebalsa">Andrew D. Balsa</a>,
August 23, 1998</p>
<blockquote><i>Notes on the kind of performance to be expected from the F1 and
its successors.</i></blockquote>

<p><b><a href="legal.php3">IP/Legal issues</a></b><br>
<a href="team.php3#brion">Brion Vibber</a>,
September 06, 1998</p>
<blockquote><i>Questions (and hopefully answers at some point) about the legal
intellectual property issues surrounding the creation of a free CPU design.
</i></blockquote>
</blockquote>

<hr>


<h2>CVS Docs</h2>

<p>These F-CPU project documents are written in SGML (currently LinuxDoc DTD
though we hope to migrate to DocBoox in the near future) and will be stored on
our CVS server when we get one set up.
Available here are the current versions of all documents, available for online
viewing and for download as SGML source or converted into HTML, LyX, PostScript,
and plain text.</p>

<p>If you'd like to contribute to the F-CPU documentation, please read the page
on <a href="doctools.php3">Documentation Tools</a>.</p>

<blockquote>
<p><a href="F-CPU_ISA.php3">The Freedom CPU ISA and Register Organization</a> - [ Download: <a href="docs/F-CPU_ISA.sgml">SGML</a> | <a href="docs/F-CPU_ISA.lyx">LyX</a> | <a href="docs/F-CPU_ISA.html.tar.gz"><tt>tar</tt>ed HTML</a> | <a href="docs/F-CPU_ISA.ps.gz"><tt>gzip</tt>ed PostScript</a> | <a href="docs/F-CPU_ISA.txt">text</a> ]<br>
Andrew D. Balsa (maintainer/coordinator), Rev. 0.0.2, 24 September 1998</p>
<blockquote><i>Describes the F-CPU ISA (Instruction Set Architecture) and Register Organization, discusses current choices. Supercedes all earlier documents!</i></blockquote>
<p><a href="F-mem.php3">F-CPU Architecture: Register Organization</a> - [ Download: <a href="docs/F-mem.sgml">SGML</a> | <a href="docs/F-mem.lyx">LyX</a> | <a href="docs/F-mem.html.tar.gz"><tt>tar</tt>ed HTML</a> | <a href="docs/F-mem.ps.gz"><tt>gzip</tt>ed PostScript</a> | <a href="docs/F-mem.txt">text</a> ]<br>
Andrew D. Balsa (<htmlurl name="andrebalsa@altern.org" url="mailto:andrebalsa@altern.org">), August 1998</p>
<blockquote><i>A description of the register organization of the Freedom CPU architecture.</abstract></i></blockquote>
<p><a href="F1-costs.php3">Estimating final end-user price of the F1 CPU</a> - [ Download: <a href="docs/F1-costs.sgml">SGML</a> | <a href="docs/F1-costs.lyx">LyX</a> | <a href="docs/F1-costs.html.tar.gz"><tt>tar</tt>ed HTML</a> | <a href="docs/F1-costs.ps.gz"><tt>gzip</tt>ed PostScript</a> | <a href="docs/F1-costs.txt">text</a> ]<br>
Andrew D. Balsa, August 1998</p>
<blockquote><i>An estimate of the end-user price of the F1 CPU. GNU/GPL</abstract></i></blockquote>
<p><a href="Freedom.php3">The Freedom CPU Architecture</a> - [ Download: <a href="docs/Freedom.sgml">SGML</a> | <a href="docs/Freedom.lyx">LyX</a> | <a href="docs/Freedom.html.tar.gz"><tt>tar</tt>ed HTML</a> | <a href="docs/Freedom.ps.gz"><tt>gzip</tt>ed PostScript</a> | <a href="docs/Freedom.txt">text</a> ]<br>
Andrew D. Balsa w/ many contributions from Rafael Reilova and Richard Gooch., 5 August 1998</p>
<blockquote><i>A GNU/GPL'ed high-performance 64-bit microprocessor developed in an open, Web-wide collaborative environment.</abstract></i></blockquote>
</blockquote>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 06 1998 23:32:32 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse15.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
