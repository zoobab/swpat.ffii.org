<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: Performance</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>Performance</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="tech.php3" alt="Technology"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="legal.php3" alt="IP/Legal Issues"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="docs.php3" alt="Documents"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<P>The Freedom class CPUs will have their CPU performance measured by a free,
Web-available, GNU/GPL'ed benchmark called nbench-byte. This benchmark
suite is <i>much</i> better than SPEC95, since it provides three kinds of
performance measurements. It also has advanced error rejection mechanisms. Like
SPEC, it uses standard algorithms for performance measurement. However, SPEC
takes many hours to run. Nbench-byte produces accurate data in a matter of
minutes.</P>

<p>Expected integer performance for the F1 implementation is Merced-competitive.
</P>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated August 23 1998 17:22:43 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
