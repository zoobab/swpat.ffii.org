<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: Miscellany</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>Miscellany</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse5.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="admin.php3" alt="Administrivia"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="index.php3" alt="The Freedom CPU Project"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<h2>Disclaimer</h2>

<p>All opinions expressed in this site belong to their respective authors. No
guarantees of any kind offered or implied. The F-CPU has no affiliation with
either IBM, Intel, AMD, Cyrix, or any other microprocessor manufacturer,
foundry, software company or corporation.</p>

<h2>Trademarks</h2>

<p>Cyrix, IBM, AMD, Digital, Microsoft and Intel are registered trademarks of
their respective companies.</p>

<p>6x86 and 6x86MX are trademarks of Cyrix Corporation, K5 and K6 are
trademarks of AMD Corporation, Windows 95 and Windows NT are trademarks of
Microsoft corporation, Alpha is a trademark of Digital Equipment Corporation
(now Compaq Corporation) and Pentium, Pentium MMX, Celeron, Pentium II and
Pentium Pro are trademarks of Intel Corporation.
</p>

<p>Linux is a trademark of Linus B. Torvalds.</p>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated August 23 1998 17:22:14 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse5.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>
