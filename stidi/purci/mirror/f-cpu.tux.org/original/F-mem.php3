<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
 <META NAME="GENERATOR" CONTENT="SGML-Tools 1.0.6">
 <TITLE>F-CPU Architecture: Register Organization</TITLE>


</HEAD>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <TITLE>The F-CPU Project: F-CPU Architecture: Register Organization</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#00008B" VLINK="#8B0000" ALINK="#FF0000">

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    <h1>F-CPU Architecture: Register Organization</h1>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap">
<map name=browsemap>
<area shape=polygon coords="20,1, 56,1, 56,12, 20,12, 20,1" href="sitemap.php3" alt="Site Map"></area>
<area shape=polygon coords="18,62, 59,62, 59,74, 18,74, 18,62" href="search.php3" alt="Search [NYI]"></area>
<area shape=polygon coords="1,31, 17,31, 17,45, 1,45, 1,31" href="F1-costs.php3" alt="Estimating final end-user price of the F1 CPU"></area>
<area shape=polygon coords="61,31, 77,31, 77,45, 61, 45, 61,31" href="doctools.php3" alt="Documentation Tools"></area>
<area shape=polygon coords="31,15, 45,15, 45,30, 31,30, 31,15" href="docs.php3" alt="Documents"></area>
</map>
</td>
</tr>
</table>
</td></tr></table>

<H1>F-CPU Architecture: Register Organization</H1>

<H2>Andrew D. Balsa (
<A HREF="mailto:andrebalsa@altern.org">andrebalsa@altern.org</A>)</H2>August 1998
<P><HR>
<EM>A description of the register organization of the Freedom CPU architecture.</EM>
<HR>
<H2><A NAME="s1">1. Introduction</A></H2>

<P>
<P>One of the most important features defining a CPU architecture is the internal register organization, and the F-CPU
architecture breaks with tradition here: we have chosen a memory-to-memory architecture (with some twists).
<P>Among the advantages of this architecture, one of the most important ones is the fact that any patents relative to register-register
architectures are automatically avoided. Also, we are innovating, and innovation has a value by itself.
<P>But the choice of a memory-to-memory architecture was also motivated by technical, performance reasons:
<P>
<OL>
<LI>We wanted to avoid the usual cycle where variables are loaded from memory into registers, then processed, then returned to
memory. These memory-to-CPU-to-memory cycles have three disadvantages: a) they increase the latency for processing any variable,
b) they are useless, but increase the number of instructions needed for even the simplest assignment and c) the compiler has to
work harder at optimizing away these register load/unload cycles.
</LI>
<LI>The presence of large, fast L1 caches inside CPUs and the constant improvements in VLSI technology allow more choices when it
comes to an efficient memory hierarchy, compared to the "traditional" memory hierarchy in Von Neumann machines:
main memory &lt;---&gt; caches &lt;---&gt; CPU registers. In fact, we skip an entire level, since all we have now is: main memory &lt;---&gt; caches.
</LI>
<LI>A large register set means a longer context switch latency, because of the time needed to save the clobbered registers. By
avoiding the use of registers, the issue is entirely avoided.
</LI>
<LI>The issue of register windows has been previously researched and the conclusion is that the benefits brought by this technique
are independent of either the CISC or RISC basic architectural choice. So we decided to include this "twist" in our memory-to-memory
architecture. In fact, it's very easy to implement a "memory window" feature on top of a memory-to-memory architecture.
</LI>
<LI>Since now all data move instructions are basically memory-to-memory move instructions, the resulting instruction set is simpler
and more orthogonal. The general purpose registers are <I>truly</I> general purpose. Greatly simplifies the user-visible machine.
</LI>
<LI>We also have the advantages of low register pressure and graceful performance degradation in those special cases where many
variables are needed.
</LI>
<LI>Simplifies the internal CPU architecture. We have the L0 data cache and the ALU directly connected to the internal CPU bus. Also
simplifies control unit structure. We get the advantages of a large register set without spending any silicon real estate for this.
</LI>
<LI>We can now tie our basic CPU clock frequency to how fast we can make the L0 data cache operate. This simplifies the basic job of
the F-1 VLSI implementation team.</LI>
</OL>
<P>
<P>
<H2><A NAME="s2">2. Register Organization Implementation</A></H2>

<P>
<P>The F-CPU architecture has the standard Program Counter or instruction pointer (PC) 64-bit register, and also a standard Status or
flags (ST) 32-bit register.
<P>And it has a Memory Window (MW) 64-bit register, which contains the base address of the active memory window. This memory window
is a set of 32 8-byte blocks that can be accessed using short versions of the standard instructions. Data pointed to by a memory
window is usually already in the L0 data cache, providing zero-latency accesses. At any instant, there are 32 possible memory
windows that can be accessed (with an 8KB L0 data cache).
<P>The F-CPU architecture also has many dedicated registers to control:
<P>a) CPU Configuration
<P>b) Memory Regions
<P>c) FPU Control
<P>d) Multiprocessing
<P>e) Paging
<P>f) Segmentation
<P>g) Interrupt Processing
<P>h) Coprocessor Control
<P>i) Performance Monitoring
<P>j) TimeStamp Counter Control
<P>k) Reconfigurable Logic Control
<P>
<P>
<H2><A NAME="s3">3. Instruction Set</A></H2>

<H2>3.1 Addressing Modes</H2>

<P>
<P>In a memory-to-memory architecture there is obviously no distinction between a register-based addressing mode and a memory-based
addressing mode. An interesting feature is also that there is not much sense in including immediate addressing modes, since these
can just as well be thought of as PC-relative memory-to-memory operations.
<P>Consequently, the addressing modes of the F-CPU architecture are few and simple:
<P>
<UL>
<LI>direct.</LI>
<LI>indirect.</LI>
<LI>PC-relative direct.</LI>
<LI>PC-relative displacement.</LI>
</UL>
<P>Other addressing modes can be synthesized using parallelizable instruction sequences, hence with near zero-cost in terms of
performance.
<P>
<H2>3.2 Instruction Format</H2>

<P>
<P>With a 64-bit instruction format, few addressing modes, external FPU and coprocessors and generally regular instruction set, the
F-CPU architecture has a very simple instruction format. All instructions are 64-bit long.
<P>Two bits decode into the following four classes of instructions:
<P>
<UL>
<LI>Standard ALU and branch instructions.</LI>
<LI>Control instructions.</LI>
<LI>DMA instructions.</LI>
<LI>FPU and coprocessor instructions.</LI>
</UL>
<P>Since the F-CPU instruction set is regular with respect to the size of data, all instructions can address either a byte (8 bits), a
word (16 bits), a double word (or double, meaning 32 bits) or a quad word (or quad, meaning 64 bits). Two bits are thus spent. There
are no alignment requirements.
<P>Referencing any one of the memory addresses in the current active window takes 5 bits per argument. In any of the other 31 windows
takes an extra 5 bits. Anywhere in memory takes obviously a full 64-bit address.
<P>
<P>

<table bgcolor="#004000" background="images/circuit3.jpg"
cellspacing=0 cellpadding=0 width="100%"><tr><td>
<table width="100%" cellspacing=0 cellpadding=0>
<tr>
  <td>
    <a href="index.php3"><img src="images/logo4sm.gif" width=78 height=78 border=0 alt="The F-CPU Project"></a>
  </td>
  <th align=center valign=center width="100%">
    <font color=white><span style="link: #c0d0ff; vlink: #a0b0e0; alink: #ff0000">
    Last updated September 02 1998 19:00:35 PM. Copyright 1998 The F-CPU Project.<br>
<a href="about.php3">About This Site</a> | <a href="mirrors.php3">Mirrors</a> | <a href="feedback.php3">Feedback</a>
    </span></font>
  </th>
  <td><img src="images/cache/browse7.gif" width=78 height=78 border=0 ismap usemap="#browsemap"></td>
</tr>
</table>
</td></tr></table>
</HTML>

</HTML>
