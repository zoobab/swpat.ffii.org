VrW: Vorschlag zur Neufassung von EPÜ §52
Dno: Die im Europäischen Patentübereinkommen festgelegten Grenzen des Patentwesens im Laufe der Jahre verwischt worden.   Die Patentämter nutzten jede vermeintliche Unklarheit im Buchstaben des Artikel 52, um ihren Einflussbereich auszuweiten.  Die hier vorgeschlagene Neuformulierung soll eine klare Grenzziehung im Sinne des ursprünglichen Geistes des EPÜ wiederherstellen helfen.
Lgn: Leitgedanken
Sle: Sicherlich wäre es reizvoll, Art. 52 EPÜ umzuformulieren, um der Liste der Einschränkungen ein explizites theoretisches Fundament zu geben und allen %(mv:Missverständnissen) jegliche Grundlage zu entziehen.  Eine solche Explizierung des Art. 52 könnte durch folgende Gedanken gekennzeichnet sein:
ied: insbesondere den %(lm:gewollten Missverständnissen) sowie dem %(tf:TRIPS-Scheinargument)
Dgw: Der Begriff %(q:technisch) wird im Sinne der klassischen BGH-Definition erklärt:  Anwendung beherrschbarer Naturkräfte ohne zwischengeschaltete menschliche Tätigkeit.  D.h. ein neuartiger chemischer Prozess ist technisch, das Computerprogramm, welches ihn vielleicht steuert, jedoch nicht.  Eine neuartige Naturkräfte-Anwendung ist patentierbar, aber die Veröffentlichung eines Programms zu ihrer Steuerung stellt weder eine unmittelbare noch eine mittelbare Patentverletzung dar.
Hmc: Hier liegt manchmal eine schwierige Grenze, s. BGH-Urteil %(q:Antiblockiersystem) von 1980.  Der ABS-Patentantrag wurde 1976 vom BPatG abgelehnt, weil die Erfindung im Bereich der programmierten Steuerung lag, aber 1980 vom BGH für zulässig erklärt, weil sie auf Experimenten mit Naturkräften beruhte und nicht etwa durch mathematische Operationen auf bekannte Rechenmodelle zurückgeführt werden konnte.  Gegenstand der Patentansprüche war eine Fahrzeugsteuerungsprozess, nicht ein Computerprogramm.
Daa: Der Begriff %(q:gewerblich) (engl. industrial, frz. industriel, jap. sangy=o: produzierendes Gewerbe) wird ebenfalls präzisiert, und zwar im Sinne des traditionellen Verständnisses von %(q:verarbeitende Industrie). Dieser Begriff ist mit dem klassischen Technikbegriff fast identisch.  Er fordert, dass es um automatisierte Prozesse zur Fertigung materieller Güter unter Einsatz von Naturkräften gehen muss.
Doa: Damit ist folgendem Szenario ein Riegel vorzuschieben, welches der Software-Referent der Union der Europäischen Patentberater, Patentanwalt Jürgen Betten, in einem Rundschreiben an seine Mandanten ausmalt:
DDo: Durch die ... Entwicklung der Rechtsprechung ... hat sich das Patentrecht von der traditionellen Beschränkung auf die verarbeitende Industrie gelöst und ist heute auch für Dienstleistungsunternehmen in den Bereichen Handel, Banken, Versicherungen, Telekommunikation usw. von essentieller Bedeutung.  Ohne Aufbau eines entsprechenden Patentportfolios ist zu befürchten, dass die deutschen Dienstleistungsunternehmen in diesen Sektoren insbesondere gegenüber der US-amerikanischen Konkurrenz ins Hintertreffen geraten.
Dea: Dies könnte bedeuteten, dass %(tan|das Wort %(q:gewerblich) in der deutschen Fassung zu %(q:industriell) geändert wird|ähnlich wie laut %(q:Basisvorschlag) in Art 23.1.1. das Wort %(q:Funktion) in der deutschen Fassung zu %(q:Amt) geändert wird: %(q:Die englische und französische Fassung bleiben hiervon unberührt)|.)
Ues: Unser Novellierungsvorschlag
Ail: Artikel 52
Eiu: Erfindungen
Eer: Europäische Patente werden für %(e:Erfindungen) erteilt.
Uxe: Unter einer %(e:Erfindung) ist eine %(e:hochgradig kreative technische Lösung eines technischen Problems) zu verstehen.
Uur: Unter einer %(e:technischen Lösung} ist %(e:eine auf neu gewonnenem Wissen über beherrschbare Naturkräfte beruhende automatisierte Lösung) zu verstehen.
Ben: Bisherige Version des §52
AWe2: Artikel 52 - Patentfähige Erfindungen
Eev: Europäische Patente werden für Erfindungen erteilt, die neu sind, auf einer erfinderischen Tätigkeit beruhen und gewerblich anwendbar sind.
Aee: Als Erfindungen im Sinn des Absatzes 1 werden insbesondere nicht angesehen:
End: Entdeckungen sowie wissenschaftliche Theorien und mathematische Methoden;
vhh: ästhetische Formschöpfungen;
Pki: Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele oder für geschäftliche Tätigkeiten sowie Programme für Datenverarbeitungsanlagen;
dao: die Wiedergabe von Informationen.
Ars: Absatz 2 steht der Patentfähigkeit der in dieser Vorschrift genannten Gegenstände oder Tätigkeiten nur insoweit entgegen, als sich die europäische Patentanmeldung oder das europäische Patent auf die genannten Gegenstände oder Tätigkeiten als solche bezieht.
VW1: Verfahren zur chirurgischen oder therapeutischen Behandlung des menschlichen oder tierischen Körpers und Diagnostizierverfahren, die am menschlichen oder tierischen Körper vorgenommen werden, gelten nicht als gewerblich anwendbare Erfindungen im Sinn des Absatzes 1. Dies gilt nicht für Erzeugnisse, insbesondere Stoffe oder Stoffgemische, zur Anwendung in einem der vorstehend genannten Verfahren.
Wrn: Weiterer Klärungsbedarf
DmG: Die Prüfungsrichtlinien und Gesetzeskommentare müssen obigen Leitgedanken und Gesetzesformulierungen angepasst werden.
Ece: In obigem Vorschlag fehlt eine Abgrenzung für den umstrittenen Bereich der Gentechnologie.  Es läge aber nahe, die Vervielfältigbarkeit zum Ausscheidungskriterium zu erheben.  Sowohl Information als auch bei Leben kann ohne industriellen Aufwand vervielfältigt werden.  Patentansprüche sollten sich demnach nur auf Gegenstände erstrecken können, deren %(e:Reproduktion) über das beanspruchte Verfahren erfolgen muss.

# Local Variables:
# mailto: mlhtimport@a2e.de
# login: swpatgirzu
# passwd: XXXX
# srcfile: /usr/share/emacs/20.6/site-lisp/mlht/app/swpat.el
# feature: swpatdir
# doc: epue52
# txtlang: de
# coding: utf-8
# End:
