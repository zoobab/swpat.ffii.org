<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Ist das Software-Urheberrecht zu schwach?

#descr: In einem Brief an die Mitglieder des Europäischen Parlamentes
widerlegt RA Jürgen Siepmann Versuche einiger parlamentarischer
%(q:Experten für Geistiges Eigentum) und Industrie-Patentanwälte, das
Urheberrecht schlecht zu reden.  Siepmann zitiert diese Argumente und
stellt sie den einschlägigen Gesetzen, Kommentaren und
Gerichtsentscheidungen gegenüber.  Der Text eignet sich als Einführung
in die Grundelemente des Software-Urheberrechts.

#jur: Wenn man argumentiert, dass das Urheberrecht nicht ausreichend zum
Schutz von Software sei, dann sollte man das Urheberrecht eigentlich
genau kennen.

#cns: Umschreiben zulässig?

#WWW: Es wurde in letzter Zeit gezielt die Behauptung verbreitet, dass das
Urheberrecht nur 1:1 Kopien verbiete. Ich zitiere aus einem Schreiben
einer Abgeordneten Ihrer Fraktion:

#ouV: Das Urheberrecht verbietet nur die Kopie eines Werkes. Übertragen auf
Softwareprodukte bedeutet dies, dass ohne Patentschutz nur eine 1:1
Kopie verfolgbar wäre, ein leichtes Umschreiben des Programms unter
Verwendung einer fremden Idee jedoch zulässig wäre.

#nec: Wie Sie den gängigen Kommentaren zum Urheberrecht entnehmen können,
ist diese Ansicht völlig falsch. In Deutschland verbietet § 69c UrhG

#tno: die Bearbeitung, das Arrangement und andere Umarbeitungen eines
Computerprogramms.

#cue: Eine %(e:Umarbeitung) ist nach Schricker/Loewenheim, Urheberrecht, 2.
Aufl. 1999, § 69c Rn 13 %(bc|grundsätzlich jede Abänderung eines
Computerprogramms [...]|Unter § 69c Nr. 2 fallen beispielsweise
Fehlerbeseitigungen, Änderungen zur Anpassung an individuelle
Benutzerwünsche, an eine neue Benutzeroberfläche oder an neue
gesetzliche, organisatorische oder technische Anforderungen,
Programmverbesserungen, Erweiterungen des Funktionsumfangs, die
Übertragung des Quellprogramms in eine andere Programmiersprache, die
Umwandlung des Quellprogramms in das Objektprogramm und umgekehrt
[...])

#WWe: Der Nachweis einer ungesetzlichen Umarbeitung ist auch nicht besonders
schwierig, siehe Schricker/Loewenheim aaO, § 69c Rn 16:

#rWf: In der Rechtsprechung haben vor allem die Übernahme von
Eigentümlichkeiten des Programms, die nicht durch Zufälligkeiten oder
durch ein freies Nachschaffen erklärt werden können, zur Bejahung
einer abhängigen Nachschöpfung geführt, etwa von Programmfehlern
[...], von überflüssigen Programmbefehlen [...], Konstruktion von
Programmverzweigungen, Aufteilung von Formeln sowie Anordnung und
Reihenfolge der Variablen innerhalb von Formeln, Anordnung und
Aufteilung von Unterprogrammen und deren Auftreten innerhalb der
Hauptprogramme, Wahl der Variablennamen [...]

#wWe: Kurz gesagt: Die Umarbeitung eines Programms ist illegal und ähnlich
leicht nachweisbar wie das Abschreiben vom Nachbarn in einer
Klassenarbeit. In den meisten Fällen ist der Quellcode für Dritte
sowieso nicht verfügbar, so dass ein Umarbeiten nicht möglich ist.
Gerade dann, wenn der Quellcode verfügbar ist, vermeiden es
Konkurrenzunternehmen tunlichst, ihren Entwicklern fremden Quellcode
bei der Entwicklung eines Konkurrenzprogramms in die Hand zu geben, da
das Risiko einer unbewussten Übernahme von Eigentümlichkeiten des
Programms, welche dann als Urheberrechtsverletzung gewertet werden
würde, viel zu groß ist.

#ewe: Verbesserungen der Position des Urhebers wären in erster Linie im
Prozessrecht möglich.

#sur: Rechtsklarheit durch Registrierung?

#ieh: Weiter heißt es in dem Schreiben der Abgeordneten Ihrer Fraktion:

#aWe: Ein weiterer Unterschied besteht darin, dass das Patent ein
sogenanntes Registerrecht darstellt. Somit steht von vorneherein fest,
was und in welchem Umfang geschützt wird. Beim Urheberrecht ist dies
nicht der Fall. Hier herrscht zunächst keine Rechtsklarheit, denn um
eine urheberrechtliche Verletzung festzustellen, bedarf es
gerichtlicher Klärung.

#Wek: Auch diese Ausführungen sind falsch. Es steht von vorneherein fest,
dass eine Urheberrechtsverletzung nicht dadurch begangen werden kann,
dass ein Unternehmen eigenständig ohne Zuhilfenahme fremden Quellcodes
oder Objektcodes ein Konkurrenzprogramm entwickelt. Eine
Urheberrechtsverletzung ist also stets vermeidbar.

#arr: Auf der anderen Seite kann gerade eine Patentrechtsverletzung trotz
der Registerrechtseigenschaft nur durch eine gerichtliche Klärung
festgestellt werden, weil das Patentrecht auf unbestimmten
Rechtsbegriffen wie

#fuh: Erfindungshöhe

#ehi: Technik

#bOu: aufbaut und weil der Begriff der Neuheit -- obwohl kein unbestimmter
Rechtsbegriff -- auch problematisch ist, weil die Ideen der im
Allgemeinen nur als Objektcode veröffentlichten Software nirgendwo
katalogisiert sind, denn diese Ideen können dem Objektcode nur mit
wirtschaftlich unvertretbarem Aufwand entnommen werden.

#Gru: Ein Patentrechtsstreit ist ein Glücksspiel, dass sich nur reiche
Unternehmen leisten können, eine Patentrechtsverletzung steht erst
nach einer rechtskräftigen Entscheidung fest und nicht durch die
Vergabe eines Patentes.

#gec: Bei einer vermuteten Urheberrechtsverletzung dagegen, kann diese
leicht durch einen Vergleich des Quellcodes beider Programme bewiesen
werden.  Dass der Quellcode des mutmaßlichen Rechtsverletzers zunächst
weder in einem Urheberrechtsstreit noch in einem Patentrechtsstreit
zur Verfügung steht, ist ein Problem des %(s:Prozessrechts) und nicht
des materiellen Rechts. Die Rechtsprechung betrachtet es teilweise als
%(q:Beweisvereitelung), wenn der Prozessgegner seinen Quellcode nicht
herausrückt, wobei dieser selbstverständlich auch ein legitimes
Interesse daran haben kann, seinen Quellcode geheim zu halten.

#tfl: Im Bereich des Prozessrechts besteht möglicherweise
%(s:Handlungsbedarf) dahingehend, einheitliche Regelungen zu schaffen,
unter welchen Voraussetzungen und an welche Personen (z.B. Gutachter)
der Quellcode eines Programms herausgegeben werden muss.

#srh: Investitionsanreize gefährdet?

#ral: Man muss sich immer vor Augen halten, dass das normale und legitime
Betrachten von Konkurrenzprodukten %(s:in jeder Branche) erlaubt sein
muss und dass gerade dieses Verhalten Innovation und Fortschritt
fördert. Nachahmungen dürfen nur dann verboten werden, wenn größere
Investitionen des Innovators gefährdet werden. Bei Software sind die
Investitionen des Innovators nicht gefährdet, weil ein potentieller
Imitator sich diese nur durch einen Verstoß gegen das Urheberrecht zu
eigen machen könnte.

#ege: In diesem Zusammenhang verweise ich auf eine neue Studie von James
Bessen und Robert M. Hunt mit dem Titel %(bq:An Empirical Look at
Software Patents), die belegt, dass im Bereich der Softwareherstellung
Patente nicht Innovation und Fortschritt fördern, sondern, dass
Mittel, die für F&E zur Verfügung standen, in das Patentwesen wandern.

#We4: Die Studie ist unter %(URL1) im Internet erhältlich und knüpft an das
Arbeitspapier %(q:Sequential Innovation, Patents and Imitation) von
James Bessen und Eric Maskin an, welches in einer überarbeiteten
Version unter %(URL2) erhältlich ist.

#mem: Ich kann mir nicht vorstellen, dass man ohne ein Verständnis der
tieferen betriebs- und volkswirtschaftlichen Zusammenhänge, so wie es
in diesen Studien vermittelt wird, eine brauchbare Richtlinie über
%(q:computerimplementierte Erfindungen) anfertigen kann.

#cWk: Keine Rechtssicherheit ohne klassischen Technikbegriff

#hge: Ich möchte zum Schluss nochmals bekräftigen, dass der Entwurf von Frau
Mc Carthy nicht geeignet ist, Rechtssicherheit, Transparenz oder sonst
irgendeinen positiven Effekt zu erzeugen.

#efx: Ich zitiere aus einer Entscheidung des Bundesgerichtshofes vom
22.06.1976 (Az X ZB 23/74, %(dp:Dispositionsprogramm)):

#rnw: Danach ist als patentierbar anzusehen eine Lehre zum planmäßigen
Handeln unter Einsatz beherrschbarer Naturkräfte zur Erreichung eines
kausal übersehbaren Erfolges. Dass auch eine bloße Organisations- und
Rechenregel, wie sie vorstehend als der Patentanmeldung entsprechend
niedergelegt ist, eine Anweisung zu planmäßigem Handeln darstellt und
dass die Befolgung dieser Anweisung zu einem kausal übersehbaren
Ergebnis führt, kann nicht bezweifelt werden. Es fehlt indes an einem
Einsatz beherrschbarer Naturkräfte zur Erreichung dieses Erfolges.

#Wwc: Wie dargelegt, würde die Einbeziehung menschlicher Verstandeskräfte
als solcher in den Kreis der Naturkräfte, deren Benutzung zur
Schaffung einer Neuerung den technischen Charakter derselben
begründen, zur Folge haben, dass schlechthin allen Ergebnissen
menschlicher Gedankentätigkeit, sofern sie nur eine Anweisung zum
planmäßigen Handeln darstellen und kausal übersehbar sind, technische
Bedeutung zugesprochen werden müsste. Damit würde aber der Begriff des
Technischen praktisch aufgegeben, würde Leistungen der menschlichen
Verstandestätigkeit der Schutz des Patentrechts eröffnet, deren Wesen
und Begrenzung nicht zu erkennen und übersehen ist.

#xir: Ohne einen Rückgriff auf Formulierung %(q:Einsatz beherrschbarer
Naturkräfte zur Erreichung eines kausal übersehbaren Erfolges) und
ohne die Begriffe %(q:Organisationsregel) und %(q:Rechenregel) oder
äquivalente Formulierungen bzw. Begriffe werden Sie keine
Rechtssicherheit erreichen können.

#ele: Ich halte es für sehr gefährlich, wenn man die %(q:Überwindung) der
Rechtsprechung des Bundesgerichtshofes von 1976 als %(q:Befreiung von
fortschrittsfeindlichem Denken) ansehen würde und die Warnungen des
Bundesgerichtshofes in den Wind schlägt.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatfukpi ;
# txtlang: xx ;
# End: ;

