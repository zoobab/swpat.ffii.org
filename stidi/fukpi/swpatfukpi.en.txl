<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Is Copyright too weak?

#descr: In a letter to the members of the European Parliament, Juergen
Siepmann rebuts attempts of some %(q:experts for Intellectual
Property) and patent attorneys to badmouth copyright. He opposes dull
%(q:expert) prejudices with %(pe:copyright law), law comments and
court decisions. The text is useable as introduction into the basics
of copyright for software.

#jur: You should learn a few things about copyright for software before
telling that copyright is not sufficient for software.

#cns: Is transforming of a program allowed?

#WWW: Recently the claim was propagated, that copyright law only covers 1:1
copies. A letter from a MEP stated:

#ouV: Copyright law covers only copying a work. Applied to software products
this means, that without patent protection only a 1:1 copy could be
prosecuted, but a slight rewrite of a program using a foreign idea
would be allowed.

#nec: You can see from established comments to copyright law and real legal
cases, that this is absolutly wrong. In Germany, Ƕ9c Urhg covers

#tno: the adaption, the arrangement and other reworking of a computer
program.

#cue: According to "Schricker/Loewenheim, Urheberrecht, 2. edition 1999, Ƕ9c
Rn 13" an adaption is %(bc|basically every modification of a computer
programm [...]|Under Ƕ9c No 2 come for example error corrections,
modifications for adjustment to custom user needs, a new user
interface or to new legal, organisatoric or technic demands, program
improvements, the transfer of the source program in another
programming language, the compilation of the source program into the
object program and reverse. [...])

#WWe: Der Nachweis einer ungesetzlichen Umarbeitung ist auch nicht besonders
schwierig, siehe Schricker/Loewenheim aaO, § 69c Rn 16:

#rWf: In the legal practice particulary the adoption of peculiarities, which
cannot be explained by contingencies or through free reimplementation,
have led to the acception of a dependant work, for example of program
errors [...], of redundant program commands [...], construction of
program branches, subdivision of formulas as well as alignment and
subdivision of subprograms und their occurence inside the main
program, the choice of variable names [...]

#wWe: In short: The adaption of a program is illegal and as easy as to prove
the write off from a neighbour in a class test. In most cases, the
source code is not available anyway to third partys, so an adaption is
not possible anyway. Especially if the source code is available, rival
businesses avoid to give their developers foreign source code on
developing an rival program, as the risk of a unconscious transfer of
peculiarities, which would be assesed as copyright violations, is too
high.

#ewe: Verbesserungen der Position des Urhebers wären in erster Linie im
Prozessrecht möglich.

#sur: Legal certainty through registration?

#ieh: In addition, the MEP wrote:

#aWe: Another difference is that a patent is a so-called "registered right"
%(pe:Registerrecht). Thus it is clear from the patent, what and to
which extent it is protected. This is different with copyright. Here
you don't have legal certainty at first, as you need a judicial
clarification to state a copyright violation.

#Wek: These remarks are wrong, too. It is clear that copyright violation
cannot be commited, if a company develops a rival program
self-contained, without the aid of foreign source or object code. So
it is always possible to avoid a copyright violation.

#arr: On the other hand especially a patent violation, although it's
attribute of being register right, can only be stated trough judicial
clarifications, as patent law is based on undefined law concepts like

#fuh: Inventive step

#ehi: Technical character

#bOu: and as the concept of "novelty" -- although now undefined law concept
-- is problematic, because the ideas of the software, generally
available only as object code, are nowhere catalogued, as extraction
of these ideas is only possible at a uneconomic cost.

#Gru: A patent law suit is a game of pure chance, which only rich companies
can afford, a patent violation is only clear after a legally binding
court decision and not after the patent is granted.

#gec: An assumed copyright violation in contrast can be easily proved by
comparing the source code of both programs. That the source code of
the supposed copyright infringer is at first not available neither in
a copyright or a patent law suit is a problem of the %(s:procedural
law) and not of the substantive law. The judicature sometimes
considers it frustration of evidence %(pe:Beweisvereitelung), if the
opposing party does not make their source code available, whereas it
as a matter of course can have a legitimate interest to keep its
source code secret.

#tfl: In regard to procedural law, there is possibly a %(s:call for action)
to make uniform rules, under which conditions which persons %(pe:e.g.
experts) must get access to the source code.

#srh: Investment incentives endangered?

#ral: One has to see, that the normal and legitimate look at rival products
must be allowed %(s:in all sectors) and that just this behaviour
stimulates innovation and progress. Imitiations may only be banned, if
they endanger major investions of the innovator. With the regard to
software, investions of the invovator are not endangered, as a
potential imitator has to infringe copyright to embrace these.

#ege: In diesem Zusammenhang verweise ich auf eine neue Studie von James
Bessen und Robert M. Hunt mit dem Titel %(bq:An Empirical Look at
Software Patents), die belegt, dass im Bereich der Softwareherstellung
Patente nicht Innovation und Fortschritt fördern, sondern, dass
Mittel, die für F&E zur Verfügung standen, in das Patentwesen wandern.

#We4: This study is available at %(URL1) and ties up to the working draft
%(q:Sequential Innovation, Patents and Imitiation) by James Bessen and
Eric Maskin, which is available in revised version at %(URL2).

#mem: I cannot believe, that it is possible without a understanding of the
deeper business and national economic connections to produce a useable
directive about %(q:computerimplemented inventions).

#cWk: No legal certainty without classic definition of technical

#hge: In conclusion, I want to reinforce, that the draft by Miss McCarthy is
not be suited to create legal certainty, transparency or any other
positive effect.

#efx: I quote from a decision of the german Federal Court at 22.06.1976
%(pe:file number X ZB 23/74, %(dp:Dispositionsprogramm)):

#rnw: According to that a doctrine for methodic action under use of
controllable forces of nature for an achievement of a causally
manageable result is to regard as patentable. A bare business or
computing method, how it is described in this patent application, is
obviously a command for methodic action and the strict obedience for
this command leads to a causally manageable result. However it lacks a
use of controllable forces of nature to achieve this result.

#Wwc: How argued, the inclusion of human intellect forces as such in the
forces of nature, whose use for the creation of a innovation
constitutes its technical character, would lead to the effect, that
plainly all results of human thinking, which just contain a command
for methodics action and are causally manageable, would get technical
character. This would practically mean to abandon the concept of
Technical and would open patent protection for achievements of human
thinking, whose character and bounds are not to be realized or
surveyed.

#xir: Without a use of the formulation %(q:use of controllable forces of
nature for the achievement of causally managable result) and without
the concepts %(q:business method) and %(q:computing method) or
aequivalent formulations/concepts you will not achieve legal
certainty.

#ele: I think it is very dangerous, to see %(q:overcoming) of the judicature
of the German Federal Court from 1976 as %(q:liberation of
anti-progressive thinking) and ignore the warnings from the German
Federal Court.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatfukpi ;
# txtlang: xx ;
# End: ;

