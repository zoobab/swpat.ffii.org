<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Structure and Interests of the Software Sector

#descr: Overview of current research on the persons working on creation of software independently or in companies of various sizes, their business models, interests, productivity, earnings, taxes etc

#mie: APOM 2004/04/21: Lettre au Premier Ministre sur le Brevet Logiciel

#enr: The French Association of Producers of Multimedia Works (Association des Producteurs d'Oevres Multimédia) condemns the Commission's and Council's approach to software patents and supports the amendments of the European Parliament.

#stt: The Economic Majority in the Current Debate about Software Patents

#Wup: Anlässlich ihrer %(ek:Konsultation von 2001) resümierte die Europäische Kommission, es hätten sich zwar 94% der Konsultationsteilnehmer gegen Softwarepatente ausgesprochen, aber die restlichen 6% repräsentierten die %(q:wirtschaftliche Mehrheit).  Auch in der Begründung ihres darauf folgenden %(cb:Richtlinienentwurfes von 2002) findet sich diese Aussage wieder.

#ews: Wir haben das Bundesamt für Statistik um eine aktuelle Umsatzsteuer-Aufschlüsselung gebeten. Dabei berücksichtigten wir die Wirtschaftszweige 72.2 (Softwarehäuser), 72.3 (Datenverarbeitungsdienste), 72.4 (Datenbanken) und 72.6 (sonstige mit der Datenverarbeitung verbundene Tätigkeiten).

#sre: Das Ergebnis: 69% (ca. 25 Mrd. Euro) des %(um:Umsatzsteueraufkommens des Jahres 2001) sind von KMUs (max. 50 Mio. Euro Umsatz p.a.) erbracht worden. Wie fein der IT-Sektor strukturiert ist, läßt sich daran erkennen, dass immer noch 47% (ca. 17 Mrd. Euro) von kleinen Unternehmen stammen, die nicht mehr als 10 Mio. Euro pro Jahr umsetzen.

#ceg: Darüber hinaus haben wir auch beim Arbeitsamt um die Zustellung einer Statistik gebeten. Hier wurden die gleichen Wirtschaftszweige (72.2, 72.3, 72.4, 72.6) wie bei der Umsatzsteuer betrachtet.

#Wae: Das Ergebnis: Per 30. Juni 2003 waren 80,9% aller im IT-Bereich Beschäftigten bei KMUs tätig.

#euv: Laut Angaben aus %(pk:Patentverwerterkreisen) ist jedem Unternehmen ein Investitionsvolumen von ca 10% seines Jahresgewinnes für Patentanmeldungen zu empfehlen.  Mittelständische Software-Unternehmen können jedoch auch durch solche Investitionen selten ein Patentportfolio aufbauen, das die Kosten wieder herein bringen könnte.  Auch bei Verzicht auf regelmäßige Investitionen in Patente ergeben sich beträchltiche Kosten für Patent-Recherchen, Prozessrisiken, Lizenzverhandlungen und Wettbewerbsnachteile.

#eui: S. z.B. die von MdEP Arlene McCarthy in ihrem %(am:Bericht an den Rechtsausschuss des Europäischen Parlamentes) zitierte Angabe von 10%, die aus Nokias Patentabteilung stammen dürfte und in ähnlicher Form regelmäßig als Empfehlung von Großkonzern-Patentanwälten an KMU gegeben wird.

#ege: Da Patentstreitigkeiten bei keinem Versicherungsunternehmen versicherbar sind, müßten die Risiken von den Firmen selbst getragen werden. Die Projekt-Volumina fast aller Aufträge bewegen sich bei KMU in der Datenverarbeitungsbranche lediglich im 4 bis 5-stelligen Bereich. Hingegen können die Kosten eines Gerichtsstreits leicht 6-stellig werden. Softwarepatente stellen somit eher eine Bedrohung als eine Chance für die große Mehrheit der deutschen IT-Unternehmen dar.

#WWd: In der Softwarebranche wird beträchtliche Innovation durch den Wettbewerb um schnelle Ausbreitung im Markt erzeugt.  Zum Schutz des Vorsprungs dienen Urheberrecht, Knowhow und die natürliche Komplexität der Datenverarbeitungssysteme.  Selbst innovative Großunternehmen wie %(ci:Cisco) und %(ab:Airbus) geben an, dass ihre Innovation von dieser Art des Wettbewerbs und nicht von den jährlichen ca 1000 Patentanmeldungen getrieben sei.

#atg: Eine im Auftrag der Europäischen Kommission von der Universität Sussex im Jahr 2001 durchgeführte %(su:Studie) zeigt, dass KMUs nicht an formellen Schutzmechanismen interessiert sind. Auch die im Auftrag der Bundesregierung im Jahr 2001 durchgeführte Studie des %(fr:Fraunhofer Institutes) weist nach, dass die Mehrheit der betroffenen Firmen der Patentierung von Software skeptisch gegenübersteht.  Drei europaweite Mittelstandsorganisationen (CEA-PME, ESBA, CEDI) haben sich zum %(bu:Europäischen Aktionsbündnis Mittelstand) gegen Softwarepatente zusammengeschlossen. Auch die DIHK, die Österreichische Wirtschaftskammer und zahlreiche Softwareunternehmerverbände haben sich immer wieder deutlich in der gleichen Richtung geäußert.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: Sektor ;
# txtlang: en ;
# multlin: t ;
# End: ;

