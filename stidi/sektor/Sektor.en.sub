\begin{subdocument}{Sektor}{Structure and Interests of the Software Sector}{http://swpat.ffii.org/analysis/sektor/Sektor.en.html}{Workgroup\\swpatag@ffii.org\\english version 2004/04/27 by FFII\footnote{http://lists.ffii.org/mailman/listinfo/traduk/TradukListinfo.en.html}}{Overview of current research on the persons working on creation of software independently or in companies of various sizes, their business models, interests, productivity, earnings, taxes etc}
\begin{sect}{ekom}{The Economic Majority in the Current Debate about Software Patents}
Anl\"{a}sslich ihrer Konsultation von 2001\footnote{http://swpat.ffii.org/papers/eukonsult00/eukonsult00.en.html} res\"{u}mierte die Europ\"{a}ische Kommission, es h\"{a}tten sich zwar 94\percent{} der Konsultationsteilnehmer gegen Softwarepatente ausgesprochen, aber die restlichen 6\percent{} repr\"{a}sentierten die ``wirtschaftliche Mehrheit''.  Auch in der Begr\"{u}ndung ihres darauf folgenden Richtlinienentwurfes von 2002\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/eubsa-swpat0202.en.html} findet sich diese Aussage wieder.

Wir haben das Bundesamt f\"{u}r Statistik um eine aktuelle Umsatzsteuer-Aufschl\"{u}sselung gebeten. Dabei ber\"{u}cksichtigten wir die Wirtschaftszweige 72.2 (Softwareh\"{a}user), 72.3 (Datenverarbeitungsdienste), 72.4 (Datenbanken) und 72.6 (sonstige mit der Datenverarbeitung verbundene T\"{a}tigkeiten).

Das Ergebnis: 69\percent{} (ca. 25 Mrd. Euro) des Umsatzsteueraufkommens des Jahres 2001\footnote{see http://plone.ffii.org/Members/nlmarco/Umsatzsteuer\_2001\_WZdiv.\_it\_grkl.xls} sind von KMUs (max. 50 Mio. Euro Umsatz p.a.) erbracht worden. Wie fein der IT-Sektor strukturiert ist, l\"{a}{\ss}t sich daran erkennen, dass immer noch 47\percent{} (ca. 17 Mrd. Euro) von kleinen Unternehmen stammen, die nicht mehr als 10 Mio. Euro pro Jahr umsetzen.

Dar\"{u}ber hinaus haben wir auch beim Arbeitsamt um die Zustellung einer Statistik gebeten. Hier wurden die gleichen Wirtschaftszweige (72.2, 72.3, 72.4, 72.6) wie bei der Umsatzsteuer betrachtet.

Das Ergebnis: Per 30. Juni 2003 waren 80,9\percent{} aller im IT-Bereich Besch\"{a}ftigten bei KMUs t\"{a}tig.\footnote{see http://plone.ffii.org/Members/nlmarco/Arbeitsagentur\_Betriebsgroessen\_IT.xls}

Laut Angaben aus Patentverwerterkreisen\footnote{S. z.B. die von MdEP Arlene McCarthy in ihrem Bericht an den Rechtsausschuss des Europ\"{a}ischen Parlamentes\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy030327/amccarthy030327.en.html} zitierte Angabe von 10\percent{}, die aus Nokias Patentabteilung stammen d\"{u}rfte und in \"{a}hnlicher Form regelm\"{a}{\ss}ig als Empfehlung von Gro{\ss}konzern-Patentanw\"{a}lten an KMU gegeben wird.} ist jedem Unternehmen ein Investitionsvolumen von ca 10\percent{} seines Jahresgewinnes f\"{u}r Patentanmeldungen zu empfehlen.  Mittelst\"{a}ndische Software-Unternehmen k\"{o}nnen jedoch auch durch solche Investitionen selten ein Patentportfolio aufbauen, das die Kosten wieder herein bringen k\"{o}nnte.  Auch bei Verzicht auf regelm\"{a}{\ss}ige Investitionen in Patente ergeben sich betr\"{a}chltiche Kosten f\"{u}r Patent-Recherchen, Prozessrisiken, Lizenzverhandlungen und Wettbewerbsnachteile.

Da Patentstreitigkeiten bei keinem Versicherungsunternehmen versicherbar sind, m\"{u}{\ss}ten die Risiken von den Firmen selbst getragen werden. Die Projekt-Volumina fast aller Auftr\"{a}ge bewegen sich bei KMU in der Datenverarbeitungsbranche lediglich im 4 bis 5-stelligen Bereich. Hingegen k\"{o}nnen die Kosten eines Gerichtsstreits leicht 6-stellig werden. Softwarepatente stellen somit eher eine Bedrohung als eine Chance f\"{u}r die gro{\ss}e Mehrheit der deutschen IT-Unternehmen dar.

In der Softwarebranche wird betr\"{a}chtliche Innovation durch den Wettbewerb um schnelle Ausbreitung im Markt erzeugt.  Zum Schutz des Vorsprungs dienen Urheberrecht, Knowhow und die nat\"{u}rliche Komplexit\"{a}t der Datenverarbeitungssysteme.  Selbst innovative Gro{\ss}unternehmen wie Cisco\footnote{http://swpat.ffii.org/papers/ftc02/cisco/ftc020228-cisco.en.html} und Airbus\footnote{http://www.innovation2004.org/} geben an, dass ihre Innovation von dieser Art des Wettbewerbs und nicht von den j\"{a}hrlichen ca 1000 Patentanmeldungen getrieben sei.

Die von der University of Sussex im Jahr 2001 durchgef\"{u}hrte Studie\footnote{http://www.aerosme.com/download/softstudy.pdf} zeigt, dass KMUs nicht an formellen Schutzmechanismen interessiert sind. Auch die im Jahr 2001 durchgef\"{u}hrte Studie des Fraunhofer Institutes\footnote{bmwi-fhgmpi01} weist nach, dass die Mehrheit der betroffenen Firmen der Patentierung von Software skeptisch gegen\"{u}bersteht. Dar\"{u}ber hinaus haben sich drei europaweite Mittelstandsorganisationen (CEA-PME, ESBA, CEDI) zum Europ\"{a}ischen Aktionsb\"{u}ndnis Mittelstand\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/ceapme0309/ceapme0309.en.html} gegen Softwarepatente zusammengeschlossen. Auch die IHK hat sich vehement in der gleichen Richtung ge\"{a}u{\ss}ert.
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Alliance of 2,000,000 SMEs against Software Patents and EU Directive\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/ceapme0309/ceapme0309.en.html}}}

\begin{quote}
An alliance representing a total of 2,000,000 small and medium-sized businesses in Europe says that software patents are harmful for SMEs and that in particular the software patent directive proposal as amended by the European Parliament's Legal Affairs Commission is a grave risk for innovation, productivity and employment in Europe.
\end{quote}
\filbreak

\item
{\bf {\bf Research on the MacroEconomic Effects of Patents\footnote{http://swpat.ffii.org/archive/mirror/impact/swpatsisku.en.html}}}

\begin{quote}
Since Fritz Machlups report to the US congress of 1958, a considerable number of studies about the economic effects of the patent system has accumulated.  Some studies deal with certain types of innovation (sequential, complex systems) or with special areas such as semiconductors, genetics or computing rules (algorithms, mathematics).  None seems to claim that the patent system has a positive effect on innovation in these fields.  Most find strong indications for negative effects.  Some governmental studies (e.g. by intellectual property institutes and the like) combine such negative findings with a recommendation to legalise software patents.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

