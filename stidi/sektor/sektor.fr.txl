<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Structure et Intérêts du Secteur Logiciel

#descr: Vue d'ensemble de l'état de la recherche sur les créateurs de logiciels, qu'ils soient indépendants ou au sein d'entreprises de diverses tailles, sur leurs modèles économiques (business models), leurs intérêts, productivité, gains, taxes, etc.

#mie: APOM 2004/04/21: Lettre au Premier ministre sur les brevets logiciels

#enr: L'Association des producteurs d'oeuvres multimédia, d'origine française, condamne l'approche de la Commission et du Conseil sur les brevets logiciels et soutient les amendements du Parlement européen.

#stt: La majorité économique dans le débat en court au sujet des brevets logiciels

#Wup: Pendant sa %(ek:période de consultations en 2000/2001), la Commission européenne a maintenu que bien que 94% des participants se soient exprimés contre les brevets logiciels, les 6% restants représentaient %(q:la majorité économique). Le raisonnement dont découle la proposition de directive de 2002 réitère cette position.

#ews: Nous avons demandé au German Bundesamt für Statistik (Département fédéral des statistiques) une ventilation à jour de la TVA pour les secteurs économiques suivants : 72.2 (entreprises de logiciels), 72.3 (services informatiques), 72.4 (bases de données) et 72.6 (autres métiers relatifs  à l'informatique).

#sre: Le résultat : 69% (environ 25 000 000 000 d'euros) des %(um:revenus de la TVA en 2001) viennent des PME (entreprises de moins de 50 millions d'euros de chiffre d'affaires annuel). On peut également voir combien ces organisations plus petites dominent le secteur des TIC (technologies de l'information et de la communication) dans le fait que même aujourd'hui, 47% (environ 17 000 000 000 d'euros) viennent des compagnies avec un chiffre d'affaires annuel inférieur à 10 millions d'euros.

#ceg: Nous avons également demandé au Bureau fédéral de l'emploi des statistiques sur l'emploi dans les branches mentionnées ci-dessus (72.2, 3, 4, et 6).

#Wae: Le résultat : au 30 juin 2003, 80.9% de tous les employés dans le secteur informatique ont travaillé dans une PME, telle que définie ci-dessus.

#euv: Selon %(pk:un axiome de base souvent cité dans le monde des brevets), chaque entreprise devrait utiliser environ 10% de ses bénéfices annuels pour payer ses dépôts de brevets. Cependant, les petites et moyennes entreprises ne peuvent ainsi que se protéger derrière un bouclier percé. Si à l'inverse, une PME décide d'économiser cet argent et de se concentrer entièrement sur la R&D et le marketing, comme le font bien des entreprises, elle doit encore dilapider des quantités considérables de ses revenus en recherche de brevets, en risque de contentieux, en négociation de licences et en compensation des désavantages concurrentiels.

#eui: Comme exemple d'application de ces recommendations dans le discours législatif, la députée européenne Arlene McCartht a cité ce chiffre de 10% dans son %(am:rapport de la commission aux affaires juridiques au Parlement européen). Ce chiffre, qui semble venir du département des brevets de Nokia, est identique aux recommendations données par les experts en brevets des grandes entreprises à leurs collègues des PME.

#ege: Comme les procès sur les brevets ne sont pas couverts par les assurances, les risques doivent être absorbés par les entreprises elles-mêmes. Les projets de PME dans le secteur des TIC sont presque toujours en-dessous de 100 000 euros ; les coûts potentiels en justice atteignent facilement des millions. Les brevets logiciels sont donc une menace plutôt qu'un jeu pour la grande majorité des entreprises informatiques allemandes.

#WWd: Sur le marché du logiciel, les facteurs d'innovation sont la réactivité aux besoins des clients et l'augmentation des parts de marché. Le droit d'auteur, le savoir-faire et la complexité naturelle des systèmes informatiques sont les critères de choix pour une entreprise voulant protéger son avance concurrentielle. Même les grandes compagnies comme %(ci:Cisco) et %(ab:Airbus) affirment que leur innovation est davantage influencée de cette manière plutôt que par leurs quelques 1 000 dépôts de brevets annuels.

#atg: Une %(su:enquête) menée par l'université du Sussex en 2001, commanditée par la Commission européenne, a démontré que les PME ne sont pas intéressées par des modèles de protection formelles. On peut trouver les mêmes constatations dans une enquête de %(fr:l'institut Fraunhofer) en 2001, commanditée par le gouvernement allemand, montrant le peu d'enthousiasme des entreprises sondées pour les brevets. Trois organismes pan-européens de petites et moyennes entreprises (CEA-PME, ESBA, et CEDI) ont formé %(bu:une alliance européenne de PME) contre les brevets logiciels. L'Association des chambres allemandes de l'industrie et du commerce (DIHK), la Chambre autrichienne des affaires et beaucoup d'entreprises informatiques et d'associations commerciales ont exprimé des positions semblables.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatstidi.el ;
# mailto: mlhtimport@a2e.de ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: sektor ;
# txtlang: fr ;
# multlin: t ;
# End: ;

