<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Proposals to EPP on Software Patents

#descr: Software Patents: How to achieve the aims of the %(q:Common Position)
-- Options and Compromise Potential

#Wti: Why do we want to exclude patents on software and business methods?

#ctu: Most of us will agree that creative people should be able to reap
rewards from their activity and that legislators should help by
granting appropriate exclusion rights (intellectual property).

#tle: Patents are one possible ruleset for intellectual property, but a
rather special one.  A patent is a monopoly on a new concept.  The
range of forbidden actions can be very broad.  Independently created
intellectual property can be destroyed by patents.  It can become
quite difficult to reconcile this kind of monopoly with the freedom of
action and freedom of competition, which are at the heart of modern
democracy and market economy.

#eWW: Therefore, when allowing patentability of any kind of subject matter,
special care must be taken.

#ars: Traditionally the patent system has been limited to the field of
applied natural science.  Not any practical problem solution, but only
concrete and physical ones are patentable.  This has helped to limit
the scope of patent claims.

#nii: The %(ep:European Patent Convention of 1973) excludes software,
mathematics and business methods from patentability.  In the
%(eg:Examination Guidelines of 1978) it is said that the excluded
subject matter is of %(q:abstract) nature. Indeed data processing is a
field in which %(ta:ingenuity lies in abstraction).  If there is
anything %(q:technical) (i.e. concrete and physical, related to forces
of nature) in data processing, then it is at best some trivial,
already well-known aspect, such as e.g. the use of generic computing
equipment.

#Wwm: The European Patent Office has tinkered with granting software patents
since 1986 (Vicom decision) and gradually enlarged the scope of patent
granting through subsequent decisions, especially those of 1998, which
brought program claims, and those of 2000 and later, which essentially
removed the test of patentable subject matter and transferred its
remnants into the non-obviousness (inventive step) test in an opaque
and unstable way.

#Wym: The result of this practise has been the granting of more than 30000
broad and trivial patents.  Experience has proven once more what was
already known: opening the patent system toward abstract subject
matter leads to undesirable results.  Innovation in this sphere should
be rewarded by narrower property rights, such as copyright, and by the
market mechanisms that are already functioning well in the software
industry.  Usually it is possible for software creators to reap
returns from their work.  Imitation is not easy at all.  It is not
prohibited, but it is very costly under the existing regime.

#laa: Today, after nearly 10 years of public discussion of the directive
project, there seems to be a wide consensus that patents in the
%(q:pure software) sector (e.g. the one where Microsoft and SAP are
active) and on business methods are undesirable.

#Weo: What's wrong with the %(q:Common Position)?

#iwu: The %(q:Common Position) of May 2004 is often presented as a way to
prevent patents on %(q:software as such) and business methods.

#tte: We agree with this stated aim of the %(q:Common Position), but it is
clear to us that the content is radically different from the
packaging.  Given the heavy lobbying of Microsoft, SAP, BSA etc for
the %(q:Common Position), this should not really need much further
explanation, but since Mr. Lehne, in his closing speech at the meeting
of 2nd of June, cited some reassuring statements about %(q:software as
such) from the %(q:Common Position) and %(eh:declared his lack of
understanding as to why these do not exclude pure software), let us
try to go into a bit more detail on this point.

#huc: The thoughts as such are free%(dots)

#sip: The directive project started in 1997 with the Commission's
%(q:Greenpaper), which explicitely stated harmonisation of European
practise with that of the USA as its aim.  The European Patent Office
has since then adopted basically the same reasoning for software
patentability as the USA. The Trilateral Project (a joint initiative
of the US, Japanese and European patent offices) of 2000 makes this
very clear in its reports, and it introduced the term
%(q:computer-implemented invention) in order justify the patenting of
%(q:computer-implemented business methods), as it is practised today
by the European Patent Office and sanctioned by the %(q:Common
Position).

#oit: The %(q:Common Position) contains some reassuring statements like

#rob: A computer program as such can not constitute a patentable invention.

#itW: which seem to go against this intention.  However the above statement,
in combination with the subsequennt claus, is really equivalent to
saying:

#get: The thoughts as such are free. We just imprison the thinkers.

#msm: This kind of statement serves no discernible regulatory purpose, it
seems to be little more than an attempt to fool the legislator.

#tdW: At the hearing of the Polish Ministry of Science and Informatization
in November 2004 all speakers, including those from Microsoft and the
patent lawyer association, agreed that %(q:computer-implemented
inventions) are really just software solutions in the context of
patent claims.  Even the European Patent Office (EPO) acknowledges
this in its %(gl:Examination Guidelines of 2001), where it explains
Art 52 EPC as follows:

#pee: Programs for computers are a form of %(q:computer-implemented
invention), an expression intended to cover claims which involve
computers, computer networks or other conventional programmable
apparatus whereby prima facie the novel features of the claimed
invention are realised by means of a program or programs.

#teW: The Commission and Council have in fact used very similar language in
their proposed definition for %(q:computer-implemented invention),
except that they pretend that a %(q:program for computers) is
something radically different from a %(q:computer-implemented
invention).

#ate: A program claim shall not be allowed, unless%(dots)

#tin: Art 5.2 is typical of the style in which the %(q:Common Position) is
written: 

#iWi: A claim to a program on its own or on a carrier shall not be allowed
unless .... [ long and complicated requirement which, upon closer
scrutiny, turns out to be always met ].

#oWi: Such attempts at fooling the legislator are in themselves a scandal. 
And the way in which this position was pushed through the Council
against the will of national parliaments, against the renegotiation
requests of three countries and against the Council's own procedural
rules, was another scandal, not to mention the flat rejection of the
Parliament's restart request of February by the Commission and the
unreasoned bulldozer-type lobbying by big industry groups posing as
SMEs.  We are dealing here with an attempt to impose the EPO's regime
on Europe with all means, exploiting the weaknesses of the European
Union's democracy deficits to the fullest, so as to avoid a fair
discussions of the interests at stake.

#wWl: It is clear that if we want to create opportunities for the stated
aims of the %(q:Common Position) to be achieved, we need to strongly
amend the %(q:Common Position).

#Wea: The final result should be a clear set of rules that excludes data
processing patents while allowing patents on inventions in the
automotive or applied medical field, household appliances etc,
regardless of whether data processing means are used in their
implementation.

#ueE: These rules should be as simple as possible, and they should make good
sense of Art 52 EPC and Art 27 TRIPs.

#afb: The rules do not need to regulate every detail of patent law.  Some
parts will inevitably left to the judiciary.  But key terms such as
%(q:technology), which are known to be used in several different
meanings with vastly different implications for patentability and for
Europe's freedom to set its own rules under TRIPs, need to be
concretised, if any %(q:harmonisation and clarification) is to be
achieved.

#xtv: Below we explain some basic steps that are needed to achieve these
aims.

#onW: If the Parliament fails to vote for a set of amendments roughly in
line with the principles outlined below (i.e. the spirit of the 1st
reading), the Council's Uncommon Position will be adopted
automatically and a dark age of US-style software patent enforcement
will begin.  Software patent enforcement companies from the US have
already set up their European operations and are waiting for the start
signal.  Once given, there is no way back any time soon.  How
difficult it is to revoke granted property rights can be seen from the
heavy lobbying of those who believe to have already obtained valid
software patents from the European Patent Office.

#nxc: The Fundamental Choice: Natural vs Exact Science

#uve: In the general discussion, two definitions of %(q:technology) have
emerged: %(e:applied exact science) and %(e:applied natural science).

#ttW: There are various wording variants which further elaborate these two
options, but there does not seem to be any third option.  Either you
count applied mathematics as a %(q:field of technology) or you do not.
 If you have found a new way to optimise the calculation for the
shortest possible route of the travelling salesman, is that a
technical invention?  It is an innovation in an exact science, for
sure.  EICTA has been proposing %(q:exact science) in their comments
on the JURI amendments.  This shift from %(q:natural) to %(q:exact)
has been implicitely happening in some EPO caselaw, and one
high-ranking EPO judge has made it explicit in a %(ms:very interesting
article).  The author correctly infers from his choice that %(q:all
practical solutions are technical inventions).

#eow: If we agree that %(q:technology) in the sense of patent law is to be
%(q:applied natural science), then questions of whether to use the
wording of %(q:controllable forces of nature) or whether to
explicitely state that data processing is not one of the natural
sciences and how exactly to state that are a matter of detail on which
compromises will be found sooner or later.  If not within the
Parliament's 2nd reading then in the Conciliation negotiations with
the Council.

#eao: The German caselaw (including %(KL) and other cases of 2004) uses the
%(q:controllable forces of nature) definitions of %(q:technology) and
the presiding judge of the highest court, Dr. Klaus-Jürgen Melullis,
stressed at a governmental hearing on 2005-06-01 in Berlin that
without this definition the courts have no reliable way to exclude
patents on abstract concepts and business methods these days.  At the
same time, Melullis explained that the definition alone does not
exclude much.  It is just a basis.  On this basis, almost any patent
can be granted.  To really exclude software and business methods,
additional elements need to come in, namely the concept of
%(q:invention), also known as %(q:contribution).

#Wur: Patenting computer tomography under the %(q:Ten Clarifications)

#ntp: If we can reach consensus on our intentions, then we will also be able
to resolve most of the other issues.  The legal principles needed to
exclude software and business methods from patentability are quite
clear and proven in many years of patent examination practise.  The
FFII has attempted to summarise them as %(dn:Ten Core Clarifications),
most of which are simplified versions of provisions approved by the
European Parliament in its 1st reading of 2003-09-24.

#hWd: These principles are basically the same as those used in the
%(bg:famous %(tp|anti-lock-braking system|ABS) decision) of the German
Federal Court of Justice (BGH) of 1980, where they were used to
justify the granting of a patent on a %(bg:computer-aided
anti-lock-braking system, and they are approximately the same which
the BGH and other national patent courts have been using for decades
and still use today.

#WWW: Let us shortly just explain here how an anti-lock-braking system or a
computer tomography solution is granted a patent under these rules.

#rsl: Process Claim

#eWW2: Automotive braking and computer tomography are fields of technology.

#alc: A patent claim such as

#liW: Claim 1

#ngt: process running on a computer tomography apparatus, characterised by
that

#uWp: the human liver is scanned for pattern X,

#aag: the obtained pattern is analysed according to rule Y,

#eua: the result is output to a display device.

#bpo: is prima facie a claim to an object which lies in the field of
tomography, not in the field of data processing.  This becomes clear
from the proposed definitions of %(q:computer program) and %(q:data
processing).

#tll: However it is not enough to find that the process as a whole is in the
field of tomography.  It must also be examined whether there is a
%(q:contribution) in the field of computer tomography, i.e. whether
the new knowledge which is embodied in this solution is knowledge in
the medical/biological field or just data processing knowledge.  The
question to be asked would be: did the alleged %(q:inventor) find out
anything new about how the liver works?  Was the %(q:inventor) a
specialist in biology/medicine?  Or was he a programmer who merely
used medical schoolbook knowledge as a basis for writing a more
effective program with better memory managment, so that processing
speed would increase?

#sTW: In some cases this question might not be easy to answer.  There is
moreover some room for judges to develop appropriate rules.

#ral: Program Claim

#ent: Assuming the above claim was found to embody a technical invention,
the next question might be about a program claim, such as

#liW2: claim 20

#Wed: computer program on a carrier which, when loaded into memory and
executed on a computer, puts the process according to claim 1 into
effect.

#iWW: This kind of claim would be rejected under the Ten Core
Clarifications.  Moreover, the freedom to distribute programs which
execute the patented process would be guaranteed in the same way as
the freedom to publish instruction manuals for the operation of the
computer tomography machine.  However, in order to execute the process
which is described in the manual or on the diskette, the operator of
the computer tomography machine would have to obtain a license from
the patentee.  Therefore, in practise, the software vendor would want
to inform his customers about the need to obtain a license, and might
even be induced to cooperate with the patentee in selling the license
to the customer.

#Wnf: This choice is justified both for economic reasons (competition) and
for reasons of clarity of the law.

#ocs: economic reasons

#mWv: The software market is an independent downstream market.  Similar to
the case of vehicle spare parts, there is no good reason for giving
hardware makers too much control over this independent market.  The
software producers, like the manual publishers, are best served with
the existing intellectual property regime for software.

#eWs: legal reasons

#Wna: when the innovation embodied in a computer program is in a field such
as biology/medicine (outside data processing), then the invention does
not lie in the programming work.  Only what was invented should be
claimed.  Breaking this principle moreover leads to a clash with other
important legal values, such as freedom of publication, which serve as
one of the few reliable boundaries to the patent system.

#Whm: If the above is the result that we have in mind, then I am sure we can
reach a compromise on the wording.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/ffiiepp0506.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: ffiiepp0506 ;
# txtlang: xx ;
# End: ;

