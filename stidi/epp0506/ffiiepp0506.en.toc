\contentsline {section}{\numberline {1}Why do we want to exclude patents on software and business methods?}{2}{section.1}
\contentsline {section}{\numberline {2}What's wrong with the ``Common Position''?}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}The thoughts as such are free\dots }{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}A program claim shall not be allowed, unless\dots }{4}{subsection.2.2}
\contentsline {section}{\numberline {3}The Fundamental Choice: Natural vs Exact Science}{5}{section.3}
\contentsline {section}{\numberline {4}Patenting computer tomography under the ``Ten Clarifications''}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}Process Claim}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Program Claim}{6}{subsection.4.2}
