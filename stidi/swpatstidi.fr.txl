<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Brevets Logiciels: Questions, Analyses, Propos

#descr:: Quel est l'effet du système de brevets sur l'économie en générale et
dans le cas du logiciel?  Pour quoi est-ce que les brevets logiciels
sont si triviales?  Quelles ont été les règles de la brevetabilité en
Europe et comment ont ils changé?  Sous quelles contraintes est-ce que
le système opére?  Entre quels propos pouvons-nous choisir?  Voici une
collection de textes qui essaient de repondre a ces questions.

#Dcn: Les Questions Fondamentales

#WeF: Autres efforts de poser des questions serieusement

#VKd: Diverse organismes gouvernmentaux on conduit des consultations sur le
brevetabilité du logiciel, mais en beaucoup de cas on demandait des
questions inappropriées, ce qu'a provoquer des débats virtuels et des
études douteuses.  Nous croyons qu'une discussion sérieuse doit
repondre aux questions suivantes.

#Bia: Bei wieviel % der %(ep:bislang vom EPA gewährten ca 30000
Softwarepatente) ist die beanspruchte geistige Leistung so
beeindruckend, dass es sich für uns als Gesetzgeber lohnen könnte,
darüber zu diskutieren, ob wir für diese Leistung ein zeitbefristetes
Monopolrecht gewähren möchten?

#FKW: Bitte zitieren Sie die Hauptansprüche von ein paar
EPA-Softwarepatenten, um Ihre Aussage zu erläutern?

#Wei: Was für Ideen werden typischerweise in EPA-Softwarepatenten
beansprucht, welcher Erfindungsaufwand steckt dahinter und wie viel
später (früher?) würden diese Ideen bekannt und fruchtbar werden, wenn
dafür keine Patente erhältlich wären?

#Llu: Inwieweit leidet die Innovationsfreude der Softwarebranche an zu
schneller Nachahmung?

#BtW: Bedarf das Software-Urheberrecht einer Verbesserung oder einer
Ergänzung durch das Patentrecht o.ä.?

#Wge: Wie müsste ein optimales %(sg:maßgeschneidertes
Software-Vergütungsrecht) aussehen?

#WSc: Warum gibt es Freie Software aber nicht freie Hardware?

#Wnv: Wie unterscheidet sich die Ökonomie der immateriellen Güter von der
der materiellen Güter?

#dea: Welche Rolle kann/soll freie Software für die Informationsgesellschaft
spielen?

#Upr: Unter welchen Regeln können proprietäre und freie Software produktiv
zusammenwirken?

#NWP: Nach welchen Regeln beurteilen EPA, BGH und BPatG heute die
Patentierbarkeit von Software?

#See: Sind diese Regeln klar?

#Wen: Wo wurden sie am klarsten formuliert?

#NWW: Nach welchen Regeln beurteilten EPA, BGH und BPatG um 1985 die
Patentierbarkeit von Software?

#Wel: Waren diese Regeln klar?

#Ggi: Gibt es internationale Verträge oder andere rechtliche Beschränkungen,
welche unseren gesetzgeberischen Handlungsspielraum bezüglich
Softwarepatenten einengen, etwa indem sie eine Patentierbarkeit von
Software erfordern, verbieten oder an Bedingungen knüpfen?

#Wet: Welche klaren Abgrenzungsregeln zur Patentierbarkeit oder
Patentdurchsetzbarkeit stehen derzeit zur Debatte?

#WaA: Wie viel % der bisher erteilten EPA-Patente (Hard- und Software)
würden durch die jeweilige Abgrenzungsregel als rechtsbeständig und
durchsetzbar bestätigt?

#Wee: Was halten Sie von den folgenden möglichen Optionen?

#Brr: Bereich der patentierbaren Ideen

#PWW: Praktische und wiederholbare Problemlösungen

#aun: Handlungsanweisungen aller Art können patentiert werden, sofern sie
objektivierbar (subjektunabhängig wiederholbar) sind und sich in der
materiellen Welt (außerhalb des menschlichen Geistes) ereignen.  Reine
Organisations- und Rechenregeln sind patentierbar, müssen aber auf
bestimmte praktische Anwendungen festgelegt werden.  Dies kann z.B.
durch eine lange Liste von Patentansprüchen geschehen.  Diese Doktrin
wird sowohl vom US-Patentamt als auch von führenden Rechtsdogmatikern
des EPA bevorzugt.

#Tne: Technik = angewandte Naturwissenschaft

#LtE: Die Erfindung muss neue Wirkungszusammenhänge von Naturkräften lehren
und darf sich nicht in %(q:Organisations- und Rechenregeln)
erschöpfen.  Wer nicht Probleme des unmittelbaren Naturkräfteeinsatzes
sondern nur Probleme innerhalb einer abstrakten Maschine oder eines
bekannten Modells löst, trägt nichts zum %(q:Stand der Technik) bei. 
Diese Doktrin gehörte bis vor kurzem in Europa, Japan, USA u.a. zum
unumstrittenen Gewohnheitsrecht des Patentwesens.  Sie wurde vom BGH
bis in die 80er Jahre Stück für Stück begrifflich verfeinert.  Sie
findet sich in %(kr:Rechtslehrbüchern), %(bh:Gesetzeskommentaren),
Prüfungsrichtlinien und %(pg:BPatG-Entscheidungen) bis zum Jahr 2000
wieder. Die Eurolinux-Allianz %(ek:fordert) für die Zukunft eine
konsequente Anwendung und Weiterentwicklung dieser Doktrin.

#Gan: Abstraktionen in Reinform

#Dni: Diese gelegentlich von Freunden der äußersten Konsequenz ins Gespräch
gebrachte Doktrin erlaubt die Patentierung rein
abstrakt-mathematischer Methoden ohne Bindung an bestimmte praktische
Anwendungen.  Die Frage der Patentierbarkeit wird nicht mehr gestellt.
 Das lenkt den Blick auf die zu oft vernachlässigte Frage, gegen
welche konkreten Realitäten die abstrakten Ansprüche denn durchsetzbar
sein sollen.

#Sir: Dynamischer Technikbegriff

#Bar: Man rühre alle drei obigen Doktrinen durcheiander und treibe von
Urteil zu Urteil in Zickzack-Bewegungen auf einen Zustand grenzenloser
Patentierbarkeit zu.

#Zfr: Um der %(q:Rechtssicherheit) (= Verhinderung des Widerstandes
%(q:konservativer) Gerichte) willen lasse man sich diesen Kurs
gelegentlich vom Gesetzgeber durch allerlei biegsame Gesetze und
Richtlinien bestätigen.

#BWg: Böse Zungen behaupten, diese Realität sei gemeint, wenn interessierte
Patentjuristen die Vorzüge eines %(q:dynamischen Technikbegriffs)
preisen.

#BWs: Bereich der Verletzungshandlungen

#Iee: Informationsgebilde und andere Immaterialgüter tendieren dazu,
Gemeingut zu sein oder zu werden.  Für sie gelten ähnliche ökonomische
Regeln und Freiheitsbedürfnisse wie für menschliche Gedanken.  Es wäre
möglich, sie grundsätzlich nicht als Verletzungsgegenstände zu
betrachten, egal was im Patentanspruch steht.  Nicht nur wie bisher
die %(q:Anwendung im privaten Bereich oder für Forschungszwecke)
sondern auch die Informationsallmende würde zur patentbefreiten Zone
erklärt.  Patente könnten dann nicht genutzt werden, um die
Verbreitung von Immaterialgütern zu unterbinden.  %{LB} formulieren
ähnliches als %(q:Quelltextprivileg), aber auch eine grundsätzliche
%(q:Privilegierung) aller Informationsgebilde (z.B.
Informationsstrukturen jedweder Art auf Datenträger, sowie das
Auslesen solcher Informationsstrukturen auf Universalrechnern,
Musikabspielgeräten u.dgl.) wäre denkbar. Der Raum der potentiellen
Verletzungsgegenstände würde auf die Sphäre der (ihrem Wesen nach für
den privaten Besitz bestimmten, industriell hergestellten) materiellen
Güter eingegrenzt.  Die Frage, ob Patente auf %(q:Organisations- und
Rechenregeln) u.dgl. zulässig sein sollen oder nicht, rückt in den
Hintergrund.  Beliebige Kombinationen mit lascheren oder strengeren
Patentierbarkeitsdoktrinen sind denkbar.

#Eng: Erfindunghöhe

#End: Einige Leute suchen nach wirksamen Kriterien und Spielregeln, die es
erlauben, mit der Forderung nach Erfindungshöhe ernst zu machen und
triviale Patente zu eliminieren.  Das holländische Parlament hat etwa
gefordert, dass dieses Problem gelöst werden müsse, bevor über die
Patentierbarkeit von Software diskutiert werden könne.

#Oia: Organisation des Patentwesens

#SWe: Seit Jahrzehnten wird oft kritisiert, das Patentprüfungssystem sei
seiner Aufgabe, ungerechtfertigte Patentansprüche schnell und
zuverlässig auszusortieren, nicht gewachsen, und es habe sich eine
Eigendynamik entwickelt, die zu immer mehr und immer fragwürdigeren
Patenten führe.  Der französische Abgeordnete Le Déaux fordert gar,
eine Kommission einzurichten, die Fehlsteuerungen im europäischen
Patentwesen untersuchen und Empfehlungen zu einer institutionellen
Reform geben soll.  Manche Leute meinen, der Schlüssel zur
Verbesserung liege weniger in den Gesetzesregeln der Patentierung als
in dem institutionellen Rahmen, in dem diese Regeln angewendet werden.

#Wji: Was passiert mit den Patenten, die nach einer neuen Richtlinie keinen
Bestand mehr haben?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatstidi ;
# txtlang: fr ;
# multlin: t ;
# End: ;

