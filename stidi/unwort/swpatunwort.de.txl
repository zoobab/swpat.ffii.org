<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Unwort des Jahres: %(q:Computer-Implementierte Erfindung)

#descr: Die Bundesbahn verkauft das %(q:Produkt) Supersparpreis, die Telekom das %(q:Produkt) GermanCall.  Manche Informatiker ziehen das %(q:Software-Engineering) dem Programmieren vor.  Tonträgerverkäufer nennen sich %(q:Musikindustrie).   Ein Lehrerstreik heißt %(q:industrial action).   Schon seit einigen Jahren gibt es Bestrebungen, prestigeträchtige Begriffe wie %(q:Industrie), %(q:Technik), %(q:Produkt), %(q:Engineering) etc so auszuweiten, dass möglichst auch immaterielle Aspekte des Lebens zu geldträchtigen materiellen Waren geformt werden können.  In ihrem Kampf um Patente auf Gensequenzen und Rechenregeln zeigen die Patentanwälte sprachlichen Innovationsgeist.  In den letzten Jahren entstanden in kurzer Folge Neologismen wie %(q:Computerprogramm nicht als solches sondern mit technischem Effekt), %(q:programmtechnische Vorrichtung), %(q:Computerprorgammprodukt), %(q:Algorithmus mit physischen Bezugspunkten), %(q:computer-implementierte Erfindung).   Mit unklaren bis zu widersinnigen Begriffen beruhigen %(q:Experten) die Gewissen verunsicherter Gesetzgeber.  Insbesondere der Begriff %(q:computer-implementierte Erfindung) hat seit seiner Einführung am Europäischen Patentamt im Mai 2000 seinen Weg in den amtlichen Sprachgebrauch und die politische Diskussion weit über die Kreise der Patentämter hinaus gebahnt.  Er wurde verwendet, um das Europäische Parlament zu einer unehrenhaften Entscheidung zu verführen.  Das Parlament widerstand jedoch den Irreführungen und Drohungen und stimmte für einschneidende Änderungsanträge, u.a. eine Umdefinition des Begriffs, die ihm die Zähne zieht.  Die Irreführungen gehen jedoch auf Ebene des EU-Ministerrates weiter.

#Uqn: Unwort-Kandidat %(q:Computer-Implementierte Erfindung)

#Stn: Sehr geehrte Damen und Herren,

#ZWf: Zu dieser alljährlichen Übung sollte diesmal insbesondere %(q:Computer-Implementierte Erfindung) ein aussichtsreicher Kandidat sein.

#Dkt: Der Begriff %(q:Computer-Implementierte Erfindung) wurde vom Europäischen Patentamt (EPA) im Jahre 2000 mit einem sehr unrühmlichen Dokument geschaffen (s. die Parodie in %(URL)) und von der Europäischen Kommission, die sich in dieser Sache als Sprachrohr des EPA betätigt, am 20. Februar 2002 in eine große Öffentlichkeit gebracht, so dass er nun in aller Munde ist, wie eine Google-Suche belegen kann.

#DLs: Diese speziell als Instrument zur patentrechtlichen Begriffsverwirrung und zum öffentlichen Lügen dienende Orwellsche Konstruktion hat einige Vorläufer, gegen die schon Jim Warren, Vorstandsmitglied des CAD-Konzerns Autodesk, auf einer Anhörung 1994 %(ad:beredt protestierte):

#TWp: Thus, I respectfully object to the title for these hearings -- %(q:Software-Related Inventions) -- since you are not primarily concerned with gadgets that are controlled by software. The title illustrates an inappropriate and seriously-misleading bias. In fact, in more than a quarter-century as a computer professional and observer and writer in this industry, I don't recall ever hearing or reading such a phrase -- except in the context of legalistic claims for monopoly, where the claimants were trying to twist the tradition of patenting devices in order to monopolize the execution of intellectual processes.

#Zii: Zudem ist das Wort im Deutschen auch noch grammatisch unschön und unnötig prätentiös (im Gewande eines informatischen Fachwortes, welches es nicht ist).  Die derzeitige Nummer der iX macht sich im Leitartikel von Hennig Behme auch darüber lustig.  Sie verweist dabei auch auf unsere Webseiten.

#DrW: Dass so ein Wort sich dieses Jahr einen zunehmend festen Platz im regierungsamtlichen Sprachgebrauch zu erorbern scheint, ist eine neue Version von %(q:Des Kaisers neue Kleider), ein folgenschwerer sprachlicher Kniefall öffentlicher Instanzen, für dessen Anprangerung die Ausschreibung %(q:Unwort des Jahres) da ist.

#Arn: Als %(q:computer-implementierte Erfindungen) bezeichnen die Softwarepatent-Befürworter Computerprogramme (Programmierkonzepte), wie man an Art 1 Ihres RiLi-Vorschlages ablesen kann.  Sie benennen die laut Gesetz nicht patentierbaren %(q:Programme für Datenverarbeitungsanlagen) einfach zu %(q:Computer-Implementierte Erfindungen) um und hoffen damit die umstrittene patentrechtliche Wertung einfach vorwegnehmen zu können.

#NoF: Neologismen wie %(q:Computer-implementierte Erfindung) oder %(q:Computerprogrammprodukt) kommen im Gewande von Fachwörtern daher, sind aber in Wirklichkeit für den Fachmann der Programmierung/Informatik ungeläufig und nichtssagend bis widersinnig.

#Ded: Die %(q:Computer-implementierte Erfindung) müsste, vordergründigem Verständnis zufolge, eine Erfindung (technische Lehre) sein, bei der die Idee im Bereich der Technik (Ingenieurwesen, angewandte Naturwissenschaft) liegt und eine Maschine (in Wirklichkeit das mathematische Modell des Universalrechners) nur als eines unter vielen möglichen Umsetzungsmittel dienen soll.   Solche Erfindungen kann es jedoch gar nicht geben.  Wenn die Erfindung in der angewandten Naturwissenschaft liegt, ist ihre Patentierbarkeit ohnehin unbestritten und der Universalrechner spielt keine prägende Rolle.  Die Patentlobby versteht daher in Wirklichkeit unter %(q:C.-i. E.) genau das Gegenteil von dem, was der vorurteilsfreie Programmierer darunter verstehen würde, nämlich eine Organisations- und Rechenregel, bei der der Computer das naheliegende und praktisch einzige sinnvolle Mittel der Umsetzung ist.  Da Organisations- und Rechenregeln aber keine Erfindungen im Sinne des Patentrechts sind und auch ihre Umsetzung auf einem Universalrechner sie nicht dazu macht, muss die Patentlobby sprachliches Versteck spielen.

#Wee: Wer Patente auf musikalische Ideen vergeben wollte, würde von %(q:musikinstrument-implementierten Erfindungen) sprechen.  Allerdings würden ihm wenige Leute glauben, dass dies ein Fachwort der Musikwissenschaftler sei.  Diese Sprachlist funktioniert nur im Bereich der Datenverarbeitung.

#Zrs: Zur Entwirrrung würde folgende Unterscheidung beitragen:

#cpe: computer-implementierte Erfindung

#cii: computer-implementierte Lehre

#Lcr: Lehre zum Gebrauch von Datenverarbeitungsanlagen

#tsB: technischer Beitrag

#tsr: technische Erfindung

#Eiu: Erfindung

#EuW: Ein ähnlicher Sprachgebrauch ist, abgesehen von dem irreführenden Wort %(q:computer-implementiert), in klassischen patentrechtlichen Lehrbüchern (Kraßer 1986, Benkard 1988) und in der Diktion der deutschen Rechtsprechung bis heute üblich.

#AdS: Aber mit solcher klaren Begrifflichkeit könnten das Europäische Patentamt und seine Brüsseler Sprachrohre ihr Ziel (die Eroberung der Zuständigkeit für Software/Rechenregeln, die bisher dem Urheberrecht gehörte) schwer erreichen.

#Abu: An %(URL) schreiben und Unwörter vorschlagen, wobei sich Ihr Beitrag nicht auf das Wort %(q:computer-implementierte Erfindung) beschränken muss.

#Edi: Erklärt sehr schön, warum die Ausdehnung des Technikbegriffes auf Geistige Dinge sich gegen die Würde des Menschen richtet.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatunwort ;
# txtlang: de ;
# multlin: t ;
# End: ;

