\begin{subdocument}{swpatunwort}{Unwort des Jahres: ``Computer-Implementierte Erfindung''}{http://swpat.ffii.org/analyse/unwort/swpatunwort.de.html}{Arbeitsgruppe\\swpatag@ffii.org\\deutsche Version 2003/12/16 von PILCH Hartmut\footnote{http://www.ffii.org/~phm}}{Die Bundesbahn verkauft das ``Produkt'' Supersparpreis, die Telekom das ``Produkt'' GermanCall.  Manche Informatiker ziehen das ``Software-Engineering'' dem Programmieren vor.  Tontr\"{a}gerverk\"{a}ufer nennen sich ``Musikindustrie''.   Ein Lehrerstreik hei{\ss}t ``industrial action''.   Schon seit einigen Jahren gibt es Bestrebungen, prestigetr\"{a}chtige Begriffe wie ``Industrie'', ``Technik'', ``Produkt'', ``Engineering'' etc so auszuweiten, dass m\"{o}glichst auch immaterielle Aspekte des Lebens zu geldtr\"{a}chtigen materiellen Waren geformt werden k\"{o}nnen.  In ihrem Kampf um Patente auf Gensequenzen und Rechenregeln zeigen die Patentanw\"{a}lte sprachlichen Innovationsgeist.  In den letzten Jahren entstanden in kurzer Folge Neologismen wie ``Computerprogramm nicht als solches sondern mit technischem Effekt'', ``programmtechnische Vorrichtung'', ``Computerprorgammprodukt'', ``Algorithmus mit physischen Bezugspunkten'', ``computer-implementierte Erfindung''.   Mit unklaren bis zu widersinnigen Begriffen beruhigen ``Experten'' die Gewissen verunsicherter Gesetzgeber.  Insbesondere der Begriff ``computer-implementierte Erfindung'' hat seit seiner Einf\"{u}hrung am Europ\"{a}ischen Patentamt im Mai 2000 seinen Weg in den amtlichen Sprachgebrauch und die politische Diskussion weit \"{u}ber die Kreise der Patent\"{a}mter hinaus gebahnt.  Er wurde verwendet, um das Europ\"{a}ische Parlament zu einer unehrenhaften Entscheidung zu verf\"{u}hren.  Das Parlament widerstand jedoch den Irref\"{u}hrungen und Drohungen und stimmte f\"{u}r einschneidende \"{A}nderungsantr\"{a}ge, u.a. eine Umdefinition des Begriffs, die ihm die Z\"{a}hne zieht.  Die Irref\"{u}hrungen gehen jedoch auf Ebene des EU-Ministerrates weiter.}
\begin{sect}{kinvunw}{Unwort-Kandidat ``Computer-Implementierte Erfindung''}
To: unwoerter2002@tu-braunschweig.de\\
Subject: ``Computer-Implementierte Erfindung''\\
From: PILCH Hartmut \texmath{<}phm@a2e.de\texmath{>}\\
Date: 06 Oct 2002 12:45:03 +0200

Sehr geehrte Damen und Herren,

Zu dieser allj\"{a}hrlichen \"{U}bung sollte diesmal insbesondere ``Computer-Implementierte Erfindung'' ein aussichtsreicher Kandidat sein.

Der Begriff ``Computer-Implementierte Erfindung'' wurde vom Europ\"{a}ischen Patentamt (EPA) im Jahre 2000 mit einem sehr unr\"{u}hmlichen Dokument geschaffen (s. die Parodie in \begin{quote}
Moses, die Zehn Patentierungsverbote und das ``Stehlen mit einem weiteren ethischen Effekt''\footnote{http://swpat.ffii.org/analyse/epue52/moses/epue52moses.de.html}
\end{quote}) und von der Europ\"{a}ischen Kommission, die sich in dieser Sache als Sprachrohr des EPA bet\"{a}tigt, am 20. Februar 2002 in eine gro{\ss}e \"{O}ffentlichkeit gebracht, so dass er nun in aller Munde ist, wie eine Google-Suche belegen kann.

Diese speziell als Instrument zur patentrechtlichen Begriffsverwirrung und zum \"{o}ffentlichen L\"{u}gen dienende Orwellsche Konstruktion hat einige Vorl\"{a}ufer, gegen die schon Jim Warren, Vorstandsmitglied des CAD-Konzerns Autodesk, auf einer Anh\"{o}rung 1994 beredt protestierte\footnote{http://swpat.ffii.org/archiv/zitate/swpatcusku.de.html\#autodesk94}:

\begin{quote}
{\it Thus, I respectfully object to the title for these hearings -- ``Software-Related Inventions'' -- since you are not primarily concerned with gadgets that are controlled by software. The title illustrates an inappropriate and seriously-misleading bias. In fact, in more than a quarter-century as a computer professional and observer and writer in this industry, I don't recall ever hearing or reading such a phrase -- except in the context of legalistic claims for monopoly, where the claimants were trying to twist the tradition of patenting devices in order to monopolize the execution of intellectual processes.}
\end{quote}

Zudem ist das Wort im Deutschen auch noch grammatisch unsch\"{o}n und unn\"{o}tig pr\"{a}tenti\"{o}s (im Gewande eines informatischen Fachwortes, welches es nicht ist).  Die derzeitige Nummer der iX macht sich im Leitartikel von Hennig Behme auch dar\"{u}ber lustig.  Sie verweist dabei auch auf unsere Webseiten.

Dass so ein Wort sich dieses Jahr einen zunehmend festen Platz im regierungsamtlichen Sprachgebrauch zu erorbern scheint, ist eine neue Version von ``Des Kaisers neue Kleider'', ein folgenschwerer sprachlicher Kniefall \"{o}ffentlicher Instanzen, f\"{u}r dessen Anprangerung die Ausschreibung ``Unwort des Jahres'' da ist.

Als ``computer-implementierte Erfindungen'' bezeichnen die Softwarepatent-Bef\"{u}rworter Computerprogramme (Programmierkonzepte), wie man an Art 1 Ihres RiLi-Vorschlages ablesen kann.  Sie benennen die laut Gesetz nicht patentierbaren ``Programme f\"{u}r Datenverarbeitungsanlagen'' einfach zu ``Computer-Implementierte Erfindungen'' um und hoffen damit die umstrittene patentrechtliche Wertung einfach vorwegnehmen zu k\"{o}nnen.

Neologismen wie ``Computer-implementierte Erfindung'' oder ``Computerprogrammprodukt'' kommen im Gewande von Fachw\"{o}rtern daher, sind aber in Wirklichkeit f\"{u}r den Fachmann der Programmierung/Informatik ungel\"{a}ufig und nichtssagend bis widersinnig.

Die ``Computer-implementierte Erfindung'' m\"{u}sste, vordergr\"{u}ndigem Verst\"{a}ndnis zufolge, eine Erfindung (technische Lehre) sein, bei der die Idee im Bereich der Technik (Ingenieurwesen, angewandte Naturwissenschaft) liegt und eine Maschine (in Wirklichkeit das mathematische Modell des Universalrechners) nur als eines unter vielen m\"{o}glichen Umsetzungsmittel dienen soll.   Solche Erfindungen kann es jedoch gar nicht geben.  Wenn die Erfindung in der angewandten Naturwissenschaft liegt, ist ihre Patentierbarkeit ohnehin unbestritten und der Universalrechner spielt keine pr\"{a}gende Rolle.  Die Patentlobby versteht daher in Wirklichkeit unter ``C.-i. E.'' genau das Gegenteil von dem, was der vorurteilsfreie Programmierer darunter verstehen w\"{u}rde, n\"{a}mlich eine Organisations- und Rechenregel, bei der der Computer das naheliegende und praktisch einzige sinnvolle Mittel der Umsetzung ist.  Da Organisations- und Rechenregeln aber keine Erfindungen im Sinne des Patentrechts sind und auch ihre Umsetzung auf einem Universalrechner sie nicht dazu macht, muss die Patentlobby sprachliches Versteck spielen.

Wer Patente auf musikalische Ideen vergeben wollte, w\"{u}rde von ``musikinstrument-implementierten Erfindungen'' sprechen.  Allerdings w\"{u}rden ihm wenige Leute glauben, dass dies ein Fachwort der Musikwissenschaftler sei.  Diese Sprachlist funktioniert nur im Bereich der Datenverarbeitung.

Zur Entwirrrung w\"{u}rde folgende Unterscheidung beitragen:

\begin{enumerate}
\item
computer-implementierte Erfindung ==\texmath{>} computer-implementierte Lehre ==\texmath{>} Lehre zum Gebrauch von Datenverarbeitungsanlagen

\item
technischer Beitrag = technische Erfindung = Erfindung
\end{enumerate}

Ein \"{a}hnlicher Sprachgebrauch ist, abgesehen von dem irref\"{u}hrenden Wort ``computer-implementiert'', in klassischen patentrechtlichen Lehrb\"{u}chern (Kra{\ss}er 1986, Benkard 1988) und in der Diktion der deutschen Rechtsprechung bis heute \"{u}blich.

Aber mit solcher klaren Begrifflichkeit k\"{o}nnten das Europ\"{a}ische Patentamt und seine Br\"{u}sseler Sprachrohre ihr Ziel (die Eroberung der Zust\"{a}ndigkeit f\"{u}r Software/Rechenregeln, die bisher dem Urheberrecht geh\"{o}rte) schwer erreichen.
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
\begin{itemize}
\item
{\bf {\bf Wie Sie helfen k\"{o}nnen, die Patentinflation zu bek\"{a}mpfen\footnote{http://swpat.ffii.org/gruppe/aufgaben/swpatgunka.de.html}}}
\filbreak

\item
{\bf {\bf An unwoerter2002@tu-braunschweig.de schreiben und Unw\"{o}rter vorschlagen, wobei sich Ihr Beitrag nicht auf das Wort ``computer-implementierte Erfindung'' beschr\"{a}nken muss.}}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
swpatcusku

(swptbkiesew Erkl\"{a}rt sehr sch\"{o}n, warum die Ausdehnung des Technikbegriffes auf Geistige Dinge sich gegen die W\"{u}rde des Menschen richtet.)
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatstidi.el ;
% mode: latex ;
% End: ;

