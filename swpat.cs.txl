<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: FFII: Softwarové patenty v Evropě

#descr: Za posledních několik let udělil Evropský patentní úřad (EPO) v
rozporu s dikcí existujícího zákona více než 30 000 patentů na
organizační a výpočetní pravidla převlečená do hávu obecných
prostředků počítačů. Ty byly v zákoně z roku 1973 nazývané jako
%(q:počítačové programy), v newspeaku EPO jsou od roku 2000 nazývané
jako %(q:vynálezy implementované prostřednictvím počítačů). Evropské
patentní hnutí se snaží prosadit legitimnost této praxe vytvořením
nového zákona.  Ačkoliv patentní hnutí ztratilo hlavní bitvy v
listopadu 2000 a září 2003, evropští programátoři a občané stále čelí
významnému riziku.  Na této stránce naleznete základní dokumentaci,
počínaje posledními novinkami a krátkým přehledem.

#lae: Proč všechen ten rozruch kvůli softwarovým patentům?

#dsW: If Haydn had patented %(q:a symphony, characterised by that sound is
produced [ in extended sonata form ]), Mozart would have been in
trouble.

#ltd: Unlike copyright, patents can block independent creations.  Software
patents can render software copyright useless.  One copyrighted work
can be covered by hundreds of patents of which the author doesn't even
know but for whose infringement he and his users can be sued.  Some of
these patents may be impossible to work around, because they are broad
or because they are part of communication standards.

#eas: Evidence from %(es:economic studies) shows that software patents have
lead to a decrease in R&D spending.

#iWd: %(it:Advances in software are advances in abstraction).  While
traditional patents were for concrete and physical %(e:inventions),
software patents cover %(e:ideas).  Instead of patenting a specific
mousetrap, you patent any %(q:means of trapping mammals) or %(ep:means
of trapping data in an emulated environment).  The fact that the
universal logic device called %(q:computer) is used for this does not
constitute a limitation.  %(s:When software is patentable, anything is
patentable).

#tek: In most countries, software has, like mathematics and other abstract
subject matter, been explicitely considered to be outside the scope of
patentable inventions.  However these rules were broken one or another
way.  The patent system has gone out of control.  A closed community
of patent lawyers is creating, breaking and rewriting its own rules
without much supervision from the outside.

#oOv: Současná situace v Evropě

#and: Co můžeme dělat?

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpatdir.el ;
# mailto: mlhtimport@ffii.org ;
# login: brablc ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpat ;
# txtlang: cs ;
# multlin: t ;
# End: ;

