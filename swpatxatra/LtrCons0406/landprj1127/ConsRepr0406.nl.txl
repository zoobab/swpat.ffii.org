<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Raad 2004-05-18: Een Niet-Gekwalificeerde Meerderheid

#descr: De moderators van de Raad voor Mededinging pushten de deelnemers om het voorstel aan te nemen, gebruik makend van misleiding, druk en verrassingstaktieken, waardoor het zelfs twijfelachtig is of er wel een geldige meerderheid bekomen werd. Het kan met zekerheid gezegd worden dat slechts een minderheid van de regeringen echt akkoord is met wat onderhandeld werd, maar verschillende regeringen werden verkeerd vertegenwoordigd door hun onderhandelaars, dewelke inter-mininsteriële overeenkomsten verbraken of zelfs instructies van hun superieuren negeerden.

#bal: Luxemburg, Estland, Letland, Slovenië

#nap: Commissie: DG Interne Markt liegt om andere DG's programmaconclusies te laten aanvaarden

#vta: Misleidend maneuver om andere landen te overtuigen voor Bolkestein's positie, verbroken inter-ministeriële overeenkomsten, MPs van regerende coalities en van opposities protesteren tegen uitgesloten en misleid te zijn.

#nai: Vooraanstaande politici van beide regerende partijen, Sociaal-Democraten en Groenen, verwierpen het gedrag van de octrooiambtenaren van de regering, opmerkend dat deze ambtenaren beloften gebroken hadden die gegeven waren aan een werkgroep bestaande uit parlementsleden van de coalitie.

#ajc: De Premier had beloofd het voorstel niet te steunen. De vertegenwoordiger in de Raad steunde het niettemin, en legde dit later uit door te zeggen dat zijn faxmachine de instructies niet had ontvangen.

#vdW: The Hungarian representative later explained that they hadn't received the instructions du to a broken fax machine.  In later public debates, the government said the Council proposal was OK, because of Art 4A (reinterpretation of Art 52 EPC as forbidding only narrow software claims but allowing broad ones). Government's work on the dossier is handled by the president of the Hungarian patent office.

#uWj: De vertegenwoordiger was opgedragen om het Duitse amendement 2b te steunen, maar werd niet om zijn positie gevraagd omdat een meerderheid reeds zou bereikt zijn, %(pt:meldde) later dat Polen zich wou onthouden.

#aim: Minister was niet ter plaatse, diplomaat wist niet wat te doen, resulteerde in tragikomische dialoog:

#iCr: Ierse Voorzitter

#lxt: België: onthouding.

#ner: En Denemarken? Kan ik Denemarken horen alstublieft?

#emr: Denemarken

#WtW: Ik zou echt graag willen vragen aan de Commissie waarom ze de laatste zin naar voren geschoven door de Italianen niet konden aanvaarden, dewelke in het oorspronkelijk voorstel stond.

#rln: Ierland

#muW: Ik denk dat de Commissaris deze vraag reeds beantwoord heeft, sorry Denemarken. Dus bent u een ja, nee, onthouding?

#emr2: Denemarken

#net: Ik denk dat we niet, we zijn niet bl...

#rln2: Ireland

#sex: Ik veronderstel dat u een %(q:ja) bent.

#emr3: Denmark

#eop: We're not happy.

#rln3: Ierland

#e8p: Bent u voor 80% blij?

#emr4: Denemarken

#tWW: Maar... Ik denk dat we...

#rln4: Ierland

#nWW: Je moet niet helemaal blij zijn. Niemand van ons is helemaal blij.

#emr5: Denemarken

#Iaw: Oh, ik weet dat, ik weet dat.

#rln5: Ierland

#eWb: Als we dat waren, zouden we niet hier zijn!

#emr6: Denemarken

#ebW: Ik denk niet dat we erg blij zijn, maar ik denk dat we, dat we...

#rln6: Ierland

#auW: Heel erg bedankt.

#emr7: Denemarken

#WWn: ... dat we vandaag een oplossing zouden willen zien.

#rln7: Ierland

#kye: Heel erg bedankt, Denemarken.

#IWy: Dames en Heren, ik ben blij te kunnen zeggen dat we een gekwalificeerde meerderheid hebben, dus heel erg bedankt iedereen, en danku aan Commissaris Bolkestein.

#rae: Minister Brinkhorst, die de %(q:octrooiexperten) op zijn kabinet vertrouwde, verstrekte verschillende keren foutieve informatie aan het Parlement en handelde tegen de wil van het Parlement in. Dit heeft geleid tot %(np:moeilijke vragen van het Parlement) en eisen dat hij de steun van Nederland zou terugtrekken). Eén van Brinkhorst's excuses voor de desinformatie aan het Parlement was een %(q:fout in de tekstverwerker), een beetje zoals de Hongaarse %(q:kapotte faxmachine).

#nee: Steun in tegenspraak met eerdere beloften van de regeringen.

#AnW: De Luxemburgse minister beloofde tegen te stemmen indien Art 6a van het Parlement, waarvoor zijn diplomaten geijverd hadden, niet aanvaard werd. Hoewel het voorstel van de Raad in recital 17 net het tegenovergestelde zegt van waar Luxemburg beweerde voor te staan, aanvaardde de minister niettemin het %(q:compromis).

#tns: De situatie in Estland en Letland is gelijkaardig.

#lip: In Slovenië had het %(q:Intellectueel Eigendomsbureau) het Informatieministerie beloofd om de Duitse aanpassingen te steunen, maar liet deze eis samen met Duitsland vallen tijdens de Raad.

#Wiw: Ministers handelen tegen beloften van de President, maken valse beweringen over waarvoor ze gestemd hebben.

#rrn: De Minister capituleerde voor het doorzettingsvermogen van de %(q:octrooiexperten) van INPI, die voortdurend beweerden dat zwart wit was. Minister van Wetenschapsbeleid Haignère stelde dat het Raadscompromis enkel de octrooieerbaarheid van technische uitvindingen zou toelaten en niet van bureausoftware, wat duidelijk niet zo is.

#ete: De situatie in de meeste landen is hetzelfde: Ministers die het uithoudingsvermogen missen om om het publieke belang te verdedigen tegen een combinatie van druk en leugens komende van hun octrooibureaus die, standaard, het dossier reeds behandelden in de %(q:Werkgroep Intellectueel Eigendom). Zij zijn het eveneens die deze praktijk oorspronkelijk ingevoerd hebben en waren dus zowel overtreder, wetgever om hun overtredingen te legaliseren als adviseur bij de stemming over hun eigen tekst.

#oma: Het Raadsvoorstel gaat beduidend verder dan dat van de Commissie door programmaconclusies toe te laten. Ook op alle andere gebieden is het een  voorstel voor onbeperkte octrooieerbaarheid en grenzeloze afdwingbaarheid van octrooien, meer extreem en compromisloos dan enig ander voorstel gezien tot nu toe. Niettemin probeerde Bolkestein dit voorstel te verkopen aan zijn collega's als één dat %(q:het evenwicht van het oorspronkelijke voorstel behield). Dit was blijkbaar genoeg om Liikanen over te halen.

#tpW: Bolkestein spaarde bovendien geen moeite tijdens de Raadssessie om de misleidende verwoordingen van de %(q:compromistekst) uit te buiten, die zo makkelijk geslikt werd door de Duitse delegatie:

#WWW: De Commissie heeft een lange tijd gedebatteerd over de mogelijkheid dat een computerprogramma als zodanig octrooieerbaar zou kunnen of moeten zijn, en heeft besloten dat dit niet zo mag zijn. En om dit absoluut en volkomen duidelijk te maken, zou de Commissie twee amendementen in Artikel 4a willen toevoegen, wat getiteld is %(q:uitzonderingen van octrooieerbaarheid). De Commissie zou een nieuw deel 1 willen voorstellen voor dit Artikel 4a, en dit nieuwe deel 1 moet simpelweg stellen: %(q:Een computerprogramma als zodanig kan geen octrooieerbare uitvinding zijn)". Ik zou denken dat dit duidelijk is, en dit zou alle terughoudendheden moeten wegnemen dat, op een of andere magische wijze, een computerprogramma als zodanig toch nog octrooieerbaar zou kunnen worden; laat mij het nogmaals lezen voor alle duidelijkheid: %(q:Een computerprogramma als zodanig kan geen octrooieerbare uitvinding zijn).

#ggc: %(aa:Appendix A) in fact explains the %(q:magic) of the commission.  According to Bolkestein's magic, only a narrow claim is a claim to a %(q:program as such), whereas a broad claim, characterised not by a specific individual expression but by some kind of undefined %(q:technical contribution) is a program %(q:not as such).  Or, as Art 5(2) says:

#olW: Een conclusie op een computerprogramma ... zal niet worden toegelaten, tenzij [voorwaarde die altijd waar gemaakt gemaakt kan worden].

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/LtrCons0406.el ;
# mailto: mlhtimport@a2e.de ;
# login: jmaebe ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ConsRepr0406 ;
# txtlang: nl ;
# multlin: t ;
# End: ;

