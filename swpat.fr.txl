<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Brevets logiciels contre démocratie parlementaire

#descr: Nous expliquons la situation actuelle au regard des monopoles sur les
idées accordés par les États, particulièrement dans le contexte du
projet de la directive %(q:sur la brevetabilité des inventions mises
en œuvre par ordinateur) (directive sur les brevets logiciels), qui
est devenue un cas d'école sur l'influence des parlements sur la
législation européenne contemporaine.

#rWa: Situation actuelle

#eaW: Alors que le Parlement européen %(ep:a proposé une exclusion claire)
des %(si:brevets logiciels), La Commission et le Conseil ont
%(ci:ignoré la proposition du Parlement) et %(ri:rétabli) %(pp:le
texte pro-brevets le plus radical) en mai 2004 dans %(ri:des manœuvres
trompeuses de dernière minute, orchestrées par la Commission et le
gouvernement allemand). Les demandes répétées de plusieurs
gouvernements et parlements nationaux pour que cette décision soit
renégociée ont permis un long surcis. Mais le Conseil a refusé de
procéder à un second vote comme on le lui avait demandé et le 7 mars
2005, il %(co:a déclaré que l'accord était adopté). Une semaine
auparavant, la %(ce:Commission) %(rs:avait décliné) une demande
unanime du Parlement européen d'un redémarrage de la procédure.

#odW: Le parlement européen va sans doute commencer en avril sa %(e:seconde
lecture) de la directive en partant d'une %(q:position commune)
juridiquement discutable. On s'attend à ce que le rapporteur désigné,
l'ancien premier ministre français %(MR), se batte pour reinstaurer la
%(ep:position de septembre 2003) du Parlement (i.e. la liberté de
publication et d'interopérabilité, la restriction de la brevetabilité
au domaine du physique). Le vote se tiendra certainement en juillet
2005. Les conditions pour obtenir une majorité sont plus difficiles
qu'en 2003 : les eurodéputés absents seront comptés comme acceptant la
position du Conseil. Si Rocard réussi à passer les amendements clés de
2003 dans cette seconde lecture, il aura ensuite beaucoup plus de
poids de négociation dans la procédure de %(e:conciliation).
Toutefois, on peut aboutir à une directive acceptable si l'on peut se
débarrasser du monopole des fonctionnaires ministériels des brevets
(le groupe qui dirige l'Office européen des brevets). Ainsi, les
libertés fondamentales de la société de l'information et la
démocratisation de l'Union européenne sont devenues étroitement
interconnectées.

#Idj: Un drame similaire se déroule en %(in:India) dans le même temps. Les
fonctionnaires de l'administration des brevets se sont servi du
prétexte de l'adaptation à %(tr:l'Accord sur les ADPIC) pour
soudainement, dans un décret administratif unilatéral, %(na:déclarer
légalement valides les brevets logiciels). Ceci étant soumis à
l'approbation du parlement au premier semestre 2005. Les
fonctionnaires indiens se sont révélés êtres de bons élèves envers
leurs homologues européen et l'analyse critiques des médias a été
encore plus discrète qu'en Europe.

#ott: Courte introduction

#tae: Derniers brevets logiciels accordés par l'EPO

#pra: Brevets en cours

#nnn: %(ua:Appel urgent) à la réouverture des négociations au Conseil

#jt0: >%(NS) signatures, >%(NC) PDG

#oIt: Protéger l'innovation

#noW: Témoignages du monde des affaires sur les brevets logiciels

#cse: Introduction aux masses, campagne financée par %(LST)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.4/site-lisp/mlht/app/swpatdir.el ;
# mailto: gibus@ffii.fr ;
# login: mgaroche ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpat ;
# txtlang: fr ;
# multlin: t ;
# End: ;

