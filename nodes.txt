<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

dirs swpat : 1
attr swpat banner0
searchdomains swpat "ffii.org"
addenda swpat : "http://wiki.ffii.org/%1"
wikifmt swpat ffii-cgi "kwikimj04/nodebase/%s.n"
datbas swpat pg ffii 3333 localhost
dbport ffii genba 5432
htaccess swpat dir "var" "www" "adm" grp "auth_group" usr vo "auth_user" usr ci "auth_asolvl3" usr re "auth_asolvl2" usr pa "auth_asolvl1" usr no "auth_asolvl0"
htusr swpat ci
ssi swpat links langs doksrow google deskr banner0 valid
ssi1 swpat banner0 banners deskr links langs google cnino cpedu doksrow
ssiabs swpat banner0 banners cpedu cnino google valid
ssiref swpat google valid banner0
validhtml swpat : 1
wikilangs swpatpenmi en de
wikilangs swpatcnino en de fr pt
menudoks swpat swpatcnino swpatpikta swpatpapri swpatstidi swpatpenmi swpatgasnu 
sub swpat swpatintro swpatlisri swpatstidi swpatpikta swpatpapri swpatpenmi swpatgasnu swpatvreji swpatxatra swpatgirzu
externtarget swpat
favicon swpat : "/favicon.ico"
rss swpat swpatcnino SwpatcninoRss
rss swpat ffiiswpatcnino FfiiSwpatcninoRss
attr swxdefl016 privat
url swpatnews anch swpat news
label swpatnews cnino
logo centerpd : centerpdlogo
logo osslaw-articles : osslawlogo
logo freepatents : freepatentslogo
logo lpf-patents : lpf120logo
logo anybr : anybrlogo
logo softwarepatenter : softwarepatenterlogo
logo eurolinux : euluxlogo
logo euluxpet : noepatents_liberty120
logo swpat welten120 de welten128de en welten128en fr welten128fr ca welten128ca 
style swpat html valid : t
style swpat html topbuttons current
style swpat html botbuttons _ noepatents
style swpat html css : ffiicss
style swpat eulux sidebuttons euluxpet ffii eurolinux proinnova softwarepatenter osslaw freepatents lpf-patents centerpd
style swpat latex toc : t
style swpat latex linknote : footnote
style swpat latex nonseks tasks links
style swpat html relorabsurl : lokurl 'swpat
style swpat html relorabsurldir : lokurldir
copy swpat : gnu-fdl
htaliases swpat img langtxt
dir img
outfmts swpat latex html
# (apply 'mlht-ciska-doklist-db 'swpat 'outfmts '(latex html))
langs swpat en
langs swpatintro en de fr nl pt cs ca ja 
# (apply 'mlht-ciska-langs-db 'swpat '(en de fr nl pt cs ca ja))
# (apply 'mlht-ciska-langs-db 'swpatintro (get 'swpat 'langs))
on swpat dir
cgidir swpat : "/ffii-cgi"
docstyle swpat : plain
datumformat swpat "%04d/%02d/%02d"
style swpat eulux menuwidth : 128
style swpat eulux docscolwidth : 120
style swpat eulux bgcolor : "#F4FEF8"
style swpat eulux menucolor : "#F4FEF8"
style swpat eulux doclevelcolors "#F4FEF8" "#E0FBE8" "#D0F9DC" "#C0F7D0" "#B0F5C5" "#A0F3B9" "#90F1AD" "#80EEA0" "#70EC94"
style swpat plain doclevelcolors "#F4FEF8" "#E0FBE8" "#D0F9DC" "#C0F7D0" "#B0F5C5" "#A0F3B9" "#90F1AD" "#80EEA0" "#70EC94"
style swpat plain bodyopts -- bgcolor "#F4FEF8" link "#0000AA" vlink "#0000AA" text "#004010" alink "#0000AA"
extract swpat : t
help swpat : swpatgunka
feature swpat : ffiirc
datanode swpat : swpatminra
author swpat phm ffii
traduk swpat en phm "2004-08-16" fr mgaroche "2003-10-13" ca rcarrera "2003-06-18" es barahona "2002-04-24" nl reinout "2003-07-15" pt rmseabra "2003-08-19" ja phm "2004-01-25" cs brablc "2004-08-16"
makefnom swpat : base_lk_sfx
citlangs swpat : NIL
dok tacke040825
langs tacke040825 de
datum tacke040825 2004 8 25
keyws swpat ffii swpat intprop indprop IP I2P immprop immjur mathpat busmeth orgkalk invent noninv kidinvs kibinvs srlinvs sidinvs swpat1 komppats itpats infpats techinv techchar technec technik engining swengng induchar patebl matpatjur uzrajt patinfl opensrc stdrdig innov kompet PatEj DGIM patmov patfam patest patjur patjurul lobby
tasks swpat : swpatgunka
outfmt-funsyms swpat fartab kompar l2col l1col
author grur-skk01 swetkor
datum grur-skk01 1211
alias bpatg17w6998 : bpatg17-suche00
label eukonsult00 consult_org_when CEC 2000
label epo-gl78 gl_pto_when EPO 1978
langs eukonsult00 en de fr
traduk bgh-dispo76 fr obenassy "2001-08-16" en phm "2001-06-01"
url chevenement0202 mail swpat 2002 2 66
url softwarepatenter : "http://www.softwarepatenter.dk/"
mllst tivosonciblue0202 url de mail swpat 2002 2 5
mltxt tivosonicblue0202 title de "heise: Tivo ./ Sonicblue"
mllst heise-bt0202 url de mail swpat 2002 2 20
mltxt heise-bt0202 title de "Heise: BT-Gerichtsverhandlungen über Hyperlink-Patent"
mllst heise-bmbf0202 url de mail swpat 2002 2 21
mltxt heise-bmbf0202 title de "Heise: Hochschulen sollen mehr von Patenten profitieren"
mllst heise-mssys0202 url de mail swpat 2002 2 22
mltxt heise-mssys0202 title de "Heise: Microsofts Treiberzertifizierung soll Patente verletzen"
mllst golem-bmbf0202 url de mail swpat 2002 2 25
mltxt golem-bmbf0202 title de "Golem: Neues Recht für Hochschulerfindungen in Kraft"
mllst czhent0202 url de mail swpat 2002 2 27
mltxt czhent0202 title de "Swpat-Propaganda in der Computerzeitung von 2002-01-28"
mllst cz0202 url de mail swpat 2002 2 50
mltxt cz0202 title de "Hintergründe zu CZ"
mltxt wpt-se-ppc url en "http://www.wpt.se/ppc/introeng.html"
mltxt wpt-se-ppc title en "Patents on Patenting"
mltxt wpt-se-ppcnr url de "http://www.wpt.se/ppc/ppcnr.html"
mltxt wpt-se-ppcnr title de "Patents on Patenting: detailed list of patents"
mltxt sz-felder0202 url de "http://www.sueddeutsche.de/aktuell/sz/artikel121695.php" "Erfundene Felder"
mltxt sz-felder0202 title de "Dr. Dietrich Klein, Rechtsexperte beim Deutschen Bauernverband, zur Patentierung ganzer Äcker"
mltxt hpinfo-patents02 url de "http://www.hp.com/hpinfo/newsroom/feature_stories/patents02.htm" "HP inventors are encouraged and rewarded for disclosing inventions."
mltxt hpinfo-patents02 title de "patent managment at HP, which has been doubling its application numbers in a few years"
mltxt eet-ftc0202 url de "http://www.eet.com/sys/news/OEG20020208S0079" "U.S. Patent Debate to Pit IP Rights vs. Competition"
mltxt eet-ftc0202 title de "report about FTC hearings on patent inflation, also quotes USPTO president arguing against a return to viewing IP rights witha 1970s-era suspicion"
mllst ffii-ftc0202 url de mail swpat 2002 2 54
mltxt ffii-ftc0202 title de "US-Wettbewerbshüter gegen Patentinflation"
mllst heise-kodaksun0202 url de mail swpat 2002 2 61
mltxt heise-kodaksun0202 title de "heise: Kodak verklagt Sun wegen Patentverletzungen in Java"
mllst android0202 url de mail neues 2002 2 12
mltxt android0202 title de "USPTO erteilt Patent auf das Problem der perfekten maschinelle Übersetzung, lösbar mittels Androiden, Buddhismus, Perpetuum Mobile uvm"
mllst epa-android0202 url de mail swpat 2002 2 62
mltxt epa-android0202 title de "Androiden-Übersetzungspatent liegt auch beim EPA an"
mllst cecpr-vs-dir url de mail news 2002 2 7
mltxt cecpr-vs-dir title de "EC Press Release inconsistent with EC/BSA directive content"
mllst cecpr-vs-dir-de url de mail neues 2002 2 7
mltxt cecpr-vs-dir-de title de "Eurolinux-Warnung: EUK-Presseerklärung steht im Widerspruch zum Inhalt des RiLi-Vorschlages"
sectstyle eubsa-swpat0202 report
mllst goltzsch-eubsa0202 url de mail fitug 2002 2 489
mltxt goltzsch-eubsa0202 title de "Goltzsch 2002-02-20: Kommission sagt, der BSA-Entwurf stamme aus der Industrie"
person mcabanas "Miquel Cabanas" "Miquel.Cabanas@uab.es" TradukListinfo
person mbrinkm "Marcus Brinkmann" "Marcus.Brinkmann@ruhr-uni-bochum.de" "http://www.marcus-brinkmann.de"
person ccornels "Christian Cornelsson" "ccorn@cs.tu-berlin.de" "http://www.cs.tu-berlin.de/~ccorn/opinions/swpat/"
person dhilleb "Dirk Hillebrecht" "dh@cantamen.de" "http://www.cantamen.de"
person obudden "Olaf Buddenhagen" "OlafBuddenhagen@web.de" "http://lists.ffii.org/mailman/listinfo/berlin-parl/"
person neogeek "NeoGeek" "NeoGMatrix@tin.it" TradukListinfo
person yleist "Yven Leist" "leist@debian.org" TradukListinfo
person gharfang "Gilles Sadowski" "gilles@harfang.homelinux.org" TradukListinfo
person guybrand "Guy Brand" "gb@isis.u-strasbg.fr" TradukListinfo
person telenetforum "Telenet Forum" "jacek.szafranski@telenetforum.pl"
person ffiitraduk "traduk" "traduk@ffii.org" TradukListinfo
url telenetforum : "http://www.telenetforum.pl/"
person glouge "George Louge" "glouge@club-internet.fr"
person dido "Paolo Didone" "dido@prosa.it" assoli
person baharona "Jesus González Baharona" "jgb@gsyc.escet.urjc.es" proinnova jgb
person xdrudis "Xavier Drudis Ferran" "xdrudis@tinet.org" caliu-patents
tel baharona "+34 91 664 74 67"
person petoft "Peter Toft" "pto@sslug.dk" "http://www.sslug.dk/~pto"
person johulbr "Johannes Ulbricht" "johannesulbricht@cs.com"
person hraible "PA Hans Raible" "raible@z.zgs.de"
person erikj "Erik Josefsson" "erikjosefsson@telia.com"
person kflenz "Prof Dr iur Karl Friedrich Lenz" "dr_lenz@ziplip.co.jp" "http://lenz.als.aoyama.ac.jp/"
person dprobst "Dr. rer. pol. Daniel Probst" "daniel.probst@vwl.uni-mannheim.de"
person jhalber "Józef Halbersztadt" "halber@timsi.com.pl"
person arupp "Arnim Rupp" "arnim@ffii.org" "http://www.rupp.de"
person blasum "Holger Blasum" "blasum@ffii.org" "http://www.blasum.net"
person tomvogt "Tom Vogt" "tom@lemuria.org" "http://www.lemuria.org/Software/unpoison"
person pelegrin "Francois Pelegrini" "pelegrin@labri.u-bordeaux.fr" abul-patents
person xuan "Xuan Baldauf" "xuan@baldauf.org" "http://www.save-our-software.de/"
person mfvm "Michael Fischer von Mollard" "mfvm@ffii.org" "http://mfvm.ffii.org"
person swetkor "Swen Kiesewetter-Köbinger" "elisabeth-koebinger@t-online.de"
person towoell "Torsten Wöllert" "towoell@ffii.org"
person jcandeira "Javier Candeira" "javier@candeira.com" "http://www.candeira.com"
person oberger "Olivier Berger" "oberger@april.org" "http://www.april.org"
person makopav "MAKOVEC Pavel" "pavelm@debian.cz" "http://www.debian.cz"
person ezanardi "ZANARDI Enrique" nil "ezanardi@ull.es"
person cumaldo "CUMANI "Aldo" "cumani@is.ien.it"
person fleck "FLECK Markus" "fleck@ffii.org"
person obenassy "Odile Bénassy" "obenassy@free.fr" "http://obenassy.free.fr/"
person rumise "Rui Miguel Seabra" "rms@1407.org" "http://www.ansol.org/"
person jheald "James HEALD" "j.heald@ucl.ac.uk"	
person caliu "CALIU" "info@caliu.org" "http://patents.caliu.info/"
person jcarvalho "Joaquim Carvalho" "jdc@x64.com" TradukListinfo
person jfreimann "Jens Freimann" "freimann@ffii.org" TradukListinfo
person reinout "Reinout Van Schouwen" "reinout@cs.vu.nl" "http://www.cs.vu.nl/~reinout/"
person lcaprani "Laurent CAPRANI" "laurent.caprani@laposte.net" "http://laurent.caprani.free.fr"
person eweigelt "Enrico Weigelt" "weigelt@metux.de" "http://www.metux.de/"
person jpsmets "Jean-Paul Smets" "jp@smets.com" "http://www.nexedi.com"
person jmlibs "Jean-Marc Libs" "libs@mail.dotcom.fr" TradukListinfo
person jmaebe "Jonas Maebe" "jmaebe@ffii.org" TradukListinfo
person paigrain "Philippe Aigrain" "philippe.aigrain@wanadoo.fr" TradukListinfo
person anonymus "anonymus" "anonymus@anonymus.kom" "www.anonymus.kom"
person phm "PILCH Hartmut" "phm@a2e.de"
tel phm : "+49-89-18979927"
adr phm : "DE 80636 Blutenburgstr. 17"
person seyfertd "Dirk Seyfert" "seyfertd@ffii.org" "http://synexpo.a2e.de/"
person jsiepm "Jürgen Siepmann" "siepmann@kanzlei-siepmann.de" "http://www.kanzlei-siepmann.de/"
url proinnova : "http://proinnova.hispalinux.es/"
label swpatpenmi penmi
name swpatlisri lisri en log de log fr journal
name swpatintro intro
label swpatintro intro
datum swpatintro 2004 2 5
label swpatlisri lisri
include swpatlisri
name swpatpenmi penmi de termine en events fr dates
langs swpatpenmi en de fr
traduk swpatpenmi fr obenassy "2000-08-25" 
datum swpatpenmi 20102
author swpatpenmi phm ffii
brev swpatgirzu : ffii
name swnerparl025 nenri de intern en internal
langs swnerparl025 de en
attr swnerparl025 privat
name swnercios3 cios3
langs swnercios3 en
name swpatstidi stidi en analysis fr analyse de analyse
include swpatstidi
sub rms-zer0111 szafranski-rms0111
names szafranski-rms0111 jsrms
dir swpatgirzu
name swpatgirzu girzu en group de gruppe fr groupe
langs swpatgirzu de en fr
mail swpatgirzu : "swpatag@ffii.org"
include swpatgirzu
traduk swpatgirzu fr obenassy "2000-09-04"
name swpatvreji vreji de archiv en archive fr archive
dir swpatvreji : t
include swpatvreji
name epologo_jpg : epo
name swpatkamni : bende
name epologo_png : epo
app epologo_jpg : jpg
app epologo_png : png
sub ffiiimg ffiieps epologo_jpg epologo_png
alias epologo : epologo_png
logo swpatepo epologo
name swpatpikta : pikta
style swpatpikta html topbuttons current
dir swpatpikta
include swpatpikta
dir swpatgasnu
name swpatgasnu gasnu de akteure en players fr acteurs
include swpatgasnu
name swpatpapri papri en papers de papiere fr papiers
langs swpatpapri de en fr
numtit swpatpapri 1
include swpatpapri
langs eubsa-swpat0202 en de fr
# supdok eubsa-swpat0202 swpatpapri
# dir eubsa-swpat0202
author ftc02 caliu ffii
datum ftc02 2002 8 8
attr swxbgh27 privat
attr swxegp27 privat
label swxbitk025 ltral "2002/05" "Bitkom"
label swxdefl016 novpet
sub miertltr miertsub schelter miertsign
dirs miertltr -1
label miertltr ltral "1999/05" "Karel Van Miert"
label miertsub subskr
label schelter "Bill Schelter"
sub miertsign miertsiglist miertsigform
label miertsign subskrnov
label miertsiglist siglist
label miertsigform sigform
name miertsigform : sigform
langs miertsigform de en fr eo
label fitugpe pr_orgdat "2000/02" "FITUG"
sub swpatkarni swxkcz27
label swxkcz27 "CZ 00-07-27"
langs swxkcz27 de
name swxkcz27 : cz27
datum swxkcz27 2000 7 31
sub ftc02 ftc020228-cisco
name ftc020228-cisco : cisco
label ftc020228-cisco : "CISCO"
app epatpdfs2txts : ""
include swpatpenmi
include eubsa-swpat0202
include europarl0309
include swpatpollm
name swpatxatra xatra en letters de briefe fr lettres
include swpatxatra
datum clsr-rhart97 2000 12 21

# Local Variables: ;
# coding: utf-8 ;
# mode: fundamental ;
# srcfile: // ;
# End: ;

