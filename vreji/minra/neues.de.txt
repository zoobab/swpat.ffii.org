PLi: Nachrichtenmeldungen über Patent-Streitfälle
Nia: In den letzten Jahren hat das Europäische Patentamt gegen den Buchstaben und den Geist der geltenden Gesetze ca 30000 Patente für computer-implementierbare Organisations- und Rechenregeln (Programme für Datenverarbeitungsanlagen) erteilt.  Nun möchte Europas Patentbewegung diese Patente nachträglich legalisieren und zugleich alle wirksamen Begrenzungen der Patentierbarkeit aufheben.  Programmierer sollen sich nicht mehr frei ausdrücken und nicht mehr frei über ihre eigenen Werke verfügen dürfen.  Bürger soll ihre Kommunikationsformen nicht mehr selbst gestalten dürfen.  Als Entschädigung für diese %(e:Geistige Enteignung) winkt uns allen ... weniger Innovation, weniger Kompatibilität, weniger gute Software.
UWe: US-Patent auf Bannerwerbung
Sri: Slashdot berichtet, CNet habe ein Patent auf Bannerwerbung im Netzwerk erhalten.  Wer in den USA Benutzerinformationen sammelt und in Abhängigkeit davon Werbebanner schaltet, muss braucht dazu die Erlaubnis von CNet.
Dta: Der für die %(dg:Binnenmarkt-Generaldirektion) und damit für die Patentpläne zuständige EU-Kommissar %{FB} plädiert vor einem Publikum von Patentrechtlern für eine zügige Auweitung des europäischen Patentwesens.
Sra: Selbst wer in der Zeitung nur die Karikaturen liest, bekommt heutzutage schon mit, dass mit dem Patentwesen etwas nicht stimmt.
Dir: Der Berater der US-Regierung im Microsoft Prozess warnt im Gespräch mit dem Österreichischen Rundfunk vor einer rigiden Ideologie des %(q:Geistigen Eigentums), die das Internet einer tyrannischen Herrschaft unterwerfen werde, wenn man jetzt nichts unternehme.
Lzs: Lessig kritisiert einen in der OpenSource-Szene weit verbreiteten blauäugigen Liberalismus und zitiert %{ESR} als dessen Vertreter.
Uts: Unter %(q:Software-Stalinismus) ist eine rigide Ideologie des Geistigen Eigentums zu verstehen, die ihren eigenen Propaganda-Neusprech pflegt und zur Durchsetzung ihres Weltbildes weder vor menschlichen Härten noch vor der Schädigung der Volkswirtschaft zurückschreckt.
Eww: Entwickler hochwertiger freier Software warnen: über ihrer Arbeit schwebt ein juristisches Damoklesschwert
3yW: 3 erst 1999 entdeckte US-Patente der Firma Apple aus dem Jahre 1990 gefährdet das frei verfügbare TrueType-Erzeugungssystem FreeType, das mittlerweile in diversen Applikationen eingesetzt wird.  Die patentierte Technik ist an sich nicht besonders wertvoll.  Aber man muss sie verwenden, um zu den Systemen von Microsoft und Apple kompatibel sein zu können.
Vee: Vicom, Sohei und andere Faelle
Dih: Die Professoren sind in der Akademischen Gemeinde offenbar erst durch ihre Patent-Zockerei aufgefallen.
Uea: US-Patent für Überbrückung des Jahres 2000: Zahlen unter 30 wird eine 20 vorangestellt, Zahlen ab 30 eine 19.  Genannt %(q:windowing technology).  Nachdem die Patentinhaber kräftig abkassiert haben, sind inzwischen doch noch neuheitsschädigende Schriftsstücke gefunden worden.
vn9: Detlef Borchers verweist auf swpat.ffii.org als Informationsquelle für die aktuelle Entwicklung.  Meint, die mittelständische europäische Softwareindustrie wünsche SWPAT. Enthält eine Meinungsumfrage, bei der bisher (1999-12) 60% der Befragten äußern %(q:Softwarepatente sind wichtig für den Schutz des geistigen Eigentums).
Nai: Netter Artikel über den Gif-Streit mit ein paar weiteren absurden Beispielen von US-Patenten.  Herrschaft über Information als die große politische Auseinandersetzung der Zukunft.
dank: Danke an %1 für diesen und weitere Hinweise
Krs: US-Patent auf eine Methode zur sportlichen Ertüchtigung von Katzen
Atu: Arnim: ok, das ist zwar kein software-patent aber doch ein beweis dafuer das im us-patent-office derzeit alles durchgeht
bWl: berichtet über %(os:die Haltung der Firma Oracle) zur %(h:SWPAT:Frage).
WoW: Wie eine Firma mit %(pt:Patenten) und Platformstrategien der freien Entwicklergemeinde im Bereich Bildverarbeitung den Garaus macht
Mat: M$ bekommt US-Patent für WWW-Strukturelemente
D3r: Das WWW-Normierungsgremium W3C beklagt Behinderung seiner Arbeit durch Softwarepatente
Haa: Microsoft hat allerlei Trivialitäten patentiert und dadurch beim %{W3C} gemeinsam erarbeitete Internetnormen in seine Abhängigkeit gebracht.
Vsk: Volltext des Microsoft-WWW-Patentes
Dts: Die Patentanspüche
Mac: Manche der von IBM quelloffen zur Verfügung gestellten Programme haben Haken: man übersieht leicht die Patente, die normalerweise nicht beansprucht werden, aber dennoch irgendwann zuschlagen können.
Nkn: Nach jahrelangem Schweigen kommen die Patentinhaber des GIF-Bildkompressionsalgorithmusses aus der Versenkung hervor und verlangen von WWW-Betreibern Lizenzgeühren von 5000 USD.
EeG: Erklärung der LPF zum GIF-Problem
Dni: Der Programmierer Thomas Boutell erklärt, dass er auf Verlangen der Patentinhaber die GIF-Unterstützung aus seinem frei im Quelltext verfügbaren Grafikprogramm entfernen musste.
AGa: Anti-GIF-Kampagne
AvW: Artikel von E.S. Raymond
MMW: Microsoft-Patent über eine Methode zur Sicherung gegen Betrug bei Egeld-Zahlungen
Pzs: Patentschutz wird beansprucht für
dWs: die automatische Erstellung von Benutzerprofilen, die unter anderem die von einem Surfer besuchten Seiten sowie die Art der dort gesuchten Informationen umfassen
dto: das Senden solcher Daten an Internet-Nutzer, wenn diese Bannerwerbung oder Verweisen und Logos betrachten
del: die gezielte Auswahl von Werbung auf Basis der erstellten Benutzerprofile, sowie
dod: die Fähigkeit, die Benutzerprofile aufgrund des Antwortverhaltens bei bestimmter Werbung zu erweitern und darauf wiederum mit geänderter Werbung zu reagieren.

### Local Variables: ***
### coding: utf-8 ***
### mailto: mlhtimport@a2e.de ***
### login: swpatgirzu ***
### passwd: XXXX ***
### srcfile: /ul/prg/src/mlht/app/swpat/swpatvreji.el ***
### feature: swpatdir ***
### doc: swpatcnino ***
### txtlang: de ***
### End: ***
