
   [blank.gif]

   Your browser does not support script



   Training and support - Because of the personal nature of this
   technology, the most effective training is one-to-one and carried out
   on the customer's own computers at the work-place. Typically covering
   a normal work-day for each user, services can include assistance with
   installation and set-up, enrolment, basic dictation and control,
   useful short cuts and techniques, building a personal vocabulary and
   maintaining voice files. Further time is often provided a week or two
   later, when more personalisation can be provided such as the creation
   of voice-linked templates, macro writing and links to other
   applications.

   Where several users are being introduced to the software, some
   class-room based training can be provided for small groups of up to 5
   people where suitable facilities can be provided. Our Head Office has
   training facilities on site and is available for customers who prefer
   to receive training outside the normal work environment.

   Support services are offered to trained customers, with telephone
   access to our Support Team during normal working hours as well as via
   e-mail or fax. We will not normally offer telephone support to
   customers who have not previously received professional training from
   us. Existing contracts are renewable annually.

   AllVoice training, support or consultancy services are a requirement
   for new customers purchasing solutions based on Dragon
   NaturallySpeaking products.


    Send mail to [1]webmaster@allvoice.co.uk with questions or comments
                            about this web site.
                  Copyright � 2002 AllVoice Computing plc
                                  [2]legal

Verweise

   1. mailto:webmaster@allvoice.co.uk
   2. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/allvoice030810/legal.htm
