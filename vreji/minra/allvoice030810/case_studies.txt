
   [blank.gif]

   Your browser does not support script



   Case Studies 

   Voice recognition can help people with a range of disabilities and
   conditions, both progressive and chronic. These can include multiple
   sclerosis, paraplegia and quadriplegia, visual impairment and
   dyslexia. Combined with other equipment and [1]software applications,
   voice recognition can become a lifeline to people who would otherwise
   be very limited in what they can achieve without computer access.
   AllVoice can arrange [2]assessments for people with a range of
   conditions and recommend possible solutions for individual needs.


                                 Glyn Evans

   Glyn worked at the Royal Naval Dockyard in Devonport and was a keen
   rugby player - until his neck was broken in a heavy tackle, leaving
   him paralysed from the neck down.

   Despite having no hand or arm movement available, Glyn has been able
   to learn to use a computer effectively, joining the internet community
   and corresponding with friends around the world by e-mail as well as
   gaining the independence to run his own affairs - all by using his
   voice. This is doubly remarkable as Glyn's breathing was also affected
   by his accident and he needs to use a ventilator, making his voice
   "unusual" by computer voice recognition standards.

   With regular updates, visits and support from AllVoice, in 1999 Glyn
   was awarded the [3]Oxford Cambridge and RSA "Recognising Achievement
   Award" for vocational qualifications, having gained the CLAIT
   qualification through Plymouth College of Further Education. The
   judging panel praised his "commitment and determination in addressing
   challenges, and in successfully returning to learning to gain new IT
   skills". As Glyn himself says, "I'm permanently on a ventilator, but
   with the use of voice recognition and the fantastic backup of AllVoice
   I've been given a new lease of life".


                             The Community Fund

   In early 1998 we were asked to provide software and training for a
   person with RSI at an office of the National Lottery Charities Board
   (now the Community Fund). The success and effectiveness of this
   installation so impressed senior management that they contracted us to
   make an assessment of the potential benefits for other users.

   Following our consultancy report and recommendations, the Community
   Fund ordered Dragon NaturallySpeaking software for a number of
   managers around the UK. Several systems were installed in their London
   HQ with others in Exeter, Nottingham, Leicester and Belfast. An
   integral part of the project was personal one-to-one training and
   customisation of the software to integrate with the Fund's bespoke
   fund administration system. Individual users also benefited from
   particular aspects of the service - for example, some people made
   extensive use of the macro capability in NaturallySpeaking
   Professional while others combined the fast, accurate dictation of the
   Preferred edition with remote dictation using a digital recorder,
   perhaps with later transcription and editing by a secretary or PA.
   Installations were made on both desktop and laptop computers, with
   some voice files being shared over a local network, allowing a degree
   of "hot-desking" for some users.

   Since the original installations in 1998 and 1999, new users have
   taken up the software and are now making effective use of it.
   Continuing support has been provided including options to upgrade to
   later versions of NaturallySpeaking as they were released and
   technical guidance for the internal IT Support Desk as well as further
   user and refresher training as required.
   The NLCB changed its name to The Community Fund in April 2001.



   Television News

   Television news and current affairs programmes demand immediate input
   and constant change. Writing and updating news reports, synopses and
   running orders means intensive keyboard use - which can mean bad news
   for anyone who develops RSI.

   Since 1996, AllVoice has been the preferred, expert supplier of voice
   recognition solutions to help overcome this problem for a major
   international broadcasting organisation. Firstly using DragonDictate
   and later NaturallySpeaking Professional, we were able to use our
   in-house programming and development resources to adapt the software
   to the customer's news processing system, which is written and
   maintained by it's own IT Department.

   Based originally in DOS and running in Windows, considerable work was
   needed to provide and maintain an effective solution as both the
   processing and the voice software packages evolved. The customer
   commented "AllVoice provided unbiased advice combined with
   enthusiastic support, which enable us together to adapt voice systems
   to our most complex software".

   From the main news-room at their London HQ to regional and local
   studios around the UK, the system has proved effective in a wide
   variety of environments - in open-plan offices, interview rooms and on
   the road. As well as helping those with severe RSI, AllVoice has been
   able to offer NaturallySpeaking at an early stage as a means to
   prevent it becoming worse. This has enabled some people to continue
   working without long leaves of absence and to avoid increasing
   discomfort or pain caused by continuing to struggle with a keyboard.

   As well as users at several sites in London, we train, help and
   support people all around the UK, in Plymouth, Bristol, Leeds,
   Norwich, Cardiff, Swindon, Manchester, Reading, Newcastle, Aberdeen
   and Glasgow.



    Send mail to [4]webmaster@allvoice.co.uk with questions or comments
                            about this web site.
                  Copyright � 2002 AllVoice Computing plc
                                  [5]legal

Verweise

   1. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/allvoice030810/what_to_buy.htm
   2. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/allvoice030810/consultancy.htm#assessment
   3. http://www.ocr.org.uk/news/awards/winn1999.htm
   4. mailto:webmaster@allvoice.co.uk
   5. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/allvoice030810/legal.htm
