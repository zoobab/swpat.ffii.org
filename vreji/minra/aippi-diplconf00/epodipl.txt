
                          AIPPI [1]News   REPORTS
   
                    Report on the Diplomatic Conference
             for the Revision of the European Patent Convention
                                      
                       Munich, November 20 - 29, 2000
                                      
                   By Ralph Nack (1) and Bruno Ph�lip (2)
                                      
   A.
   
   Background of the Diplomatic Conference
   
   In 1998, less than 30 years after the signing of the European Patent
   Convention (EPC), the Administrative Council of the European Patent
   Organization (EPO) initiated a major process of reform of the EPC. The
   intergovernmental conference in Paris (June 1999, see OJ EPO 1999,
   545) requested the EPO to prepare a EPC revision conference according
   to Art. 172 EPC to be convened in the year 2000.
   
   After consultation with the interested circles and the Standing
   Advisory Committee (SACEPO), the Administrative Council of the EPO
   worked out a Basic Proposal which was made available to interested
   members of the public (document MR/ 2/ 00). The proposals for revision
   related to many different aspects of the EPC. According to the Basic
   Proposal, almost 100 articles of the Convention were subject to be
   either amended or deleted. Most of the changes are not very
   spectacular in themselves, but seen as a whole they are significant in
   both in practical and institutional terms.
   
   The aim of the revision was to subject the European patent system to
   cautious modernization, while maintaining the proven principles of
   substantive patent law and procedural law which underlie the 1973 EPC.
   Many different issues have been discussed in the interested circles
   with regard to this process of reform.
   
   However, it became clear at a very early stage of the preparatory
   consultations that not all of these issues could be considered in this
   part of the revision process: The schedule for preparing the
   Diplomatic Conference requested by the intergovernmental mandate was
   very tight. Therefore, the idea of a "first" and a "second basket" was
   born: The more difficult issues like biotech patents or the grace
   period were postponed to the "second basket".
   
   In August 2000, the European Commission proposed a Council Regulation
   on the Community patent including a Common European patent court (COM
   [2000] 412 final). This issue had been discussed for many years, but
   came to the fore again only with the publication of the Commission's
   "Green Paper on the Community patent and the European patent system"
   in 1997 (COM [97] 314 final). According to the proposal, the unitary
   EU patent will, after grant, no longer be subject to the national law
   of the member states, but only to Community Law in the form of the
   Council Regulation. Therefore, the implementation of the EC Community
   Patent System in the EPC was postponed to the second basket.
   
   Regarding the problem of the costs of translating European patents, an
   optional protocol was concluded at the intergovernmental conference in
   London (October 2000) enabling member states to waive, wholly or in
   part, the requirement for translation of European patents into their
   official language. The protocol is subject to ratification and can
   only enter into force when it has been ratified by eight states,
   including the three for which the EPO granted the largest number of
   European patents in 1999.
   
   B.
   
   The composition of the conference
   
   The Diplomatic Conference was chaired by Dr. Roland Grossenbacher,
   president of the Administrative Council. The conference consisted of
   the delegations of the contracting states, the delegation of the
   European Community, and observer delegations (states permitted to
   accede the EPC), WIPO, and invited non-governmental organizations like
   the AIPPI. Except for the Credentials Committee and the Drafting
   Committee, the conference always met in plenary sessions.
   
   [2][Officers] 
   
   C.
   
   The outcome of the Diplomatic Conference
   
   The Basic Proposal formed the basis for the discussions of the
   conference. Proposed amendments to this Basic Proposal required for
   their adoption a majority of two third of the votes cast by the
   Ordinary Member Delegations present. On the other hand, a three
   quarter majority of the votes cast by the Ordinary Member Delegations
   present was required for adoption of the entire Revision Act, Article
   172 (1) and (2) EPC.
   
   The conference was very well prepared by the EPO. Not even the
   permanent demonstration of Greenpeace in front of the EPO building
   substantially disturbed the proceedings. Thanks to the excellent
   chairmanship of Dr. Grossenbacher, the negotiations were mainly
   successful. Only a few provisions of the Basic Proposal have been
   subject to amendments or deletions. The following issues are the most
   important elements of the revision:
     * Computer programs have not been deleted from Article 52 (2) (c)
       EPC; the respective provision of the Basic Proposal was overruled
       by 16 of 20 votes.
     * The patentability of a known pharmaceutical for a new specific use
       has been affirmed (2nd medical use), Article 54 (4) and (5) rev.
       version.
     * The doctrine of equivalents is now mentioned in Article 2 of the
       protocol on Article 69 EPC, but there is no definition of
       "equivalence", and the file wrapper estoppel is not mentioned.
     * The wording of Article 52 (1) has been brought in line with
       Article 27 (1) of the TRIPS-agreement.
     * A petition for review of the Boards' of Appeal decisions by the
       Enlarged Board of Appeals is now possible. The petition may only
       be filed on the grounds expressly named in the EPC and the
       Implementing Regulations.
     * The central limitation procedure for European Patents has been
       adopted.
     * Patent applications can be filed in any language (see the recently
       signed Patent Law Treaty); a translation into one of the official
       languages of the EPO will not be required until a later date in
       accordance with the Implementing Regulations.
     * The separation of search and examination has been removed from the
       EPC. Both tasks can be performed by the same examiner located
       either in The Hague, Berlin or Munich (BEST-project). A protocol
       on the staff complement of the EPO at The Hague has been annexed
       to the EPC.
     * The Administrative Council has been authorized to adapt the EPC to
       international treaties or Community Law, albeit with wide
       restrictions.
     * The ministerial conference has been made a permanent institution
       of the EPC.
     * Special agreements concerning European patent law between two or
       more contracting states are now explicitly recognized.
     * Many provisions have been transferred from the EPC to the
       Implementing Regulations.
       
   The new provisions will not enter into force immediately, as they
   first have to be ratified by the parliaments of the member states.
   Therefore, the revised convention will not have any legal effect for
   another four or five years.
   
   In the "Conference Resolution", the Administrative Council is urged as
   a priority to make preparations for another Diplomatic Conference
   which will deal in particular, with the question of software and
   biotech patents as well as Community patents.
   
   D.
   
   The "hot" issues discussed at the Diplomatic Conference
   
   As mentioned above, only a few provisions of the Basic Proposal have
   been subject to amendments or deletions. However, there has been a
   controversial discussion on many issues. In the following, the most
   important proceedings of the conference are summarized in
   chronological order.
   
   I.
   
   Implementation procedure with regard to EC Law and international
   treaties, Article 33 (1) (b) and (5), Article 35 (3)
   
   The traditional way to implement EC Law and international treaties in
   the EPC is convening a Diplomatic Conference of the member states for
   the adoption of a revised convention, a very time consuming and
   expensive procedure. Therefore, the Administrative Council has been
   empowered to amend EPC provisions concerning patent law and procedural
   law by a unanimous vote of all member states. In order to ensure the
   sovereignty of the member states and the rights of the national
   legislative bodies in particular, each Contracting State has a period
   of 12 months from the time the decision is adopted in which to declare
   that it wishes not to be bound by the decision.
   
   However, these guarantees did not satisfy the Swedish delegation for
   "constitutional reasons": Under the Swedish constitution, a three
   quarter majority in parliament is apparently required for the
   ratification of this provision. Therefore, Sweden proposed to postpone
   this matter to the "second basket". This proposal was supported by
   some member states.
   
   Germany, Switzerland and most other member states opposed this
   proposal very strongly, however, arguing that the national sovereignty
   was not endangered as the national legislative bodies have one year to
   induce the Government to revoke their vote.
   
   However, as a mutual concession, all member states agreed to amend
   Article 33 as follows: International treaties cannot be implemented in
   the EPC before they enter into force of the respective treaty; EC Law
   cannot be implemented before it enters into force or before the expiry
   of the implementation period respectively. The German delegation
   agreed to this compromise "with a bad grace" and the Swiss delegation
   called it a "self-castigation".
   
   [3][Officers] 
   
   II.
   
   Computer programs, Article 52 (2) (c)
   
   In the first draft of the Basic Proposal (CA/100/00), it was stated
   that "in any event, the deletion of computer programs from Article 52
   (2) (c) EPC has met with broad consensus". Furthermore, this draft
   discussed the question whether the entire paragraph (2) should be
   deleted or transferred to the Implementing Regulations.
   
   In contrast, the wording of the final version of the Basic Proposal
   (MR/2/00) is much more restraint; it is merely stated that "the
   committee on patent law and the Administrative Council have advocated
   the deletion of programs for computers from Article 52 (2) (c) EPC.
   The deletion of the entire paragraph (2) is no longer proposed.
   
   The Basic Proposal makes clear that broadening the scope of patentable
   subject matter was not intended by the deletion of computer programs:
   The deletion should merely reflect the current jurisprudence of the
   EPO's Boards of Appeal.
   
   At the Diplomatic Conference, France, Denmark, and Germany proposed to
   postpone the deletion of computer programs and all other EC member
   states except for Austria supported this proposal. The background of
   this initiative were the massive protests against software patents by
   a number of software developers. The delegations of the EC member
   states had met in Brussels on November 17 2000 (i.e. three days before
   the Diplomatic Conference) in order to agree on a common standpoint
   and to coordinate their voting with regard to computer programs. As
   the 15 EC member states represent a majority of two third of the votes
   cast by the Ordinary Member Delegations (which is needed for
   amendments of the Basic Proposal), there was no real discussion on
   this issue during the Diplomatic Conference. Apart from the Austrian
   delegation, all EC member states reasoned their voting by more or less
   identical statements:
     * Further consideration with the interested circles is needed.
     * The European Commission has recently launched a consultation
       within the member states with the purpose of having a thorough
       discussion of the issue and possibly establishing proper means for
       harmonization on this issue within the Community. The outcome of
       this initiative should be awaited before any further action on
       this matter is taken in relation to the EPC.
     * A deletion of computer programs from Article 52 (2) might be
       misunderstood as broadening the scope of patentable subject
       matter.
       
   The Austrian delegation emphasized that their interested circles
   supported patenting of computer programs. Therefore, this delegation
   abstained.
   
   The Swiss delegation vehemently criticized the behavior of the EC
   member states:
     * The postponement might be misunderstood as criticism of the
       current EPO practice concerning computer related inventions.
     * In future, Article 52 (2) (c) might be a disadvantageous
       locational factor for Europe.
     * The preparation of another Diplomatic Conference ("second basket")
       is very time consuming. This will significantly delay the revision
       process.
       
   The other delegations were not impressed by the statements of the
   Swiss delegation. However, all delegation emphasized that the
   postponement is definitely not a criticism of the current EPO practice
   concerning computer related inventions. The delegations mandated the
   chairman Dr. Grossenbacher to make this clear at the final press
   conference.
   
   All non governmental organizations representing the users were in
   favour of the deletion of the exclusion of computer programs per se
   from patentability. The AIPPI representative made a strong statement
   in referring to the resolution taken almost unanimously at the
   Executive Committee of Vienna.
   
   Finally, 18 member states supported the postponement.
   
   [4][Officers] 
   
   III.
   
   First and second medical use, Article 54 (4) and (5) EPC
   
   The exclusion of methods of treatments and diagnostic methods in
   Article 52 (4) EPC has been added as new paragraph (c) to the two
   exceptions to patentability which appear in Article 53 (a) and (b)
   EPC. The former provision was considered "undesirable" since these
   methods are excluded in the interest of public health and not for
   their lack of industrial applicability.
   
   According to current Article 54 (5) EPC (now Article 54 [4]), by way
   of "compensation" for the exclusion from patentability of medical
   methods under Article 53 (c) EPC, known substances or composition of
   matter are deemed to be new, provided they are used for the first time
   in such a medical method. Up till now, the EPO, following the case law
   of the Boards of Appeal (see e.g. T 128/ 82, "Pyrrolidine derivatives/
   Hoffmann-La-Roche", OJ EPO 1984, 164), has granted a broad claim for
   the first medical use, i.e. for a general therapeutic purpose, e.g. as
   a "pharmaceutical substance", even if only a specific use of the
   substance is disclosed in the application.
   
   The Enlarged Board of Appeal was asked to decide whether any further
   medical use could receive patent protection under the EPC in spite of
   the wording of current Article 54 (5) EPC (now Article 54 [4]) which
   seemed to limit patentability to the first medical use. The Enlarged
   Board answered in the affirmative to this question and allowed the
   so-called "Swiss type claim", i.e. a claim "directed to the use of a
   substance or composition for the manufacture of a medicament for a
   specific new and inventive therapeutic application" (see G 5/ 83,
   "Second medical indication/ EISAI", OJ EPO 1985, 64).
   
   The national courts and appeal divisions of the patent offices of the
   contracting states have generally followed this decision. However, the
   French Cour de Cassation, the Dutch Octrooiraad and the UK High Court
   expressed doubts on the validity of "Swiss type claims". Therefore, a
   new Article 54 (5) EPC was introduced which unambiguously permits
   purpose related product protection for each further new medical use of
   a substance or composition already known as a medicine.
   
   On the Diplomatic Conference, all delegations supported the
   introduction of this clarification. However, there has been a long
   discussion on two issues:
   
     * Should the permissible scope of protection provided by a claim on
       the first or second medical use be prescribed by the EPC or should
       this continue to be a matter for case law?
     * Does Article 54 (4) and (5) EPC prescribe or proscribe the scope
       of protection provided by a claim on the first or second medical
       use?
       
   In order to illuminate theses issues, the wording of the new Article
   54 (4) and (5) is cited below:
   
   (4) Paragraph 2 and 3 shall not exclude the patentability of any
   substance or composition, comprised in the state of the art, for use
   in a method referred to in Article 53 (c), provided that its use for
   any such method is not comprised in the state of the art.
   
   (5) Paragraph 2 and 3 shall also not exclude the patentability of any
   substance or composition referred to in paragraph 4 for any specific
   use in any method referred to in Article 53 (c), provided that such
   use is not comprised in the state of the art.
   
   Austria, Greece, Germany, and other delegations took the view that the
   permissible scope of protection provided by a claim on the first or
   second medical use should continue to be a matter for case law. The
   Austrian delegation emphasized that the scope of protection is solely
   determined by the content of the disclosure and that this rule
   prohibits the introduction of any provision prescribing the scope of
   protection with regard to a particular case. On the other hand,
   Sweden, Switzerland, and a number of other delegations supported a
   prescription of the permissible scope of protection according to the
   status quo for the sake of legal certainty.
   
   [5][Officers] 
   
   Consequently, there has been no consensus whether Article 54 (4) and
   (5) EPC prescribes or proscribes the scope of protection provided by a
   claim on the first or second medical use: In the first draft of the
   Basic Proposal it is stated in the explanatory remarks that Article 54
   (4) and (5) does "neither prescribe nor proscribe" the scope of
   protection. However, the final draft does not comment on this issue.
   The Swedish delegation took the view that the revised Article 54 (5)
   is intended "to match as closely as possible to the scope of
   protection ... provided by a `Swiss type claim'" (see document
   MR/18/00). Many other delegations agreed on this point; especially the
   EPO made it clear that paragraph 4 - if interpreted in the light of
   paragraph 5 - prescribes a broad claim for the first medical use. For
   this reason, Portugal proposed to transfer paragraph 4 and 5 to
   Article 53 (c). Greece proposed to replace these provisions by a
   paragraph allegedly "neutral" with regard to the scope of protection.
   In addition, Belgium, Austria, Germany, and other delegations insisted
   that Article 54 governs exclusively the novelty requirement and
   therefore cannot prescribe the scope of protection in any respect.
   
   Finally, the Greek proposal did not find the required majority and the
   Diplomatic Conference adopted Article 54 (4) and (5) in the wording
   cited above.
   
   During the discussion, the AIPPI was in full support of the text
   revised as above indicated.
   
   IV.
   
   Central limitation procedure, Articles 105a to 105c EPC
   
   Under the extended limitation procedure set forth in the new Articles
   105a to 105c EPC, a European patent may be limited or revoked ab
   initio at the request of the patent proprietor. Limitation or
   revocation may be requested at any time, although precedence must
   always be given to opposition proceedings.
   
   The central limitation procedure as such has been supported by all
   delegations; however, for "constitutional reasons" Sweden and Denmark
   proposed to postpone the introduction of this procedure to the "second
   basket": According to these delegations, the Swedish and Danish
   constitutions require a 3/4 and 5/6 majority, respectively, for the
   ratification of these provisions.
   
   The other member states expressed their concerns about the situation
   in Denmark and Sweden. However, they supported the introduction of the
   central limitation procedure in the "first basked". The Dutch
   delegation emphasized that the initiative for the limitation procedure
   can solely be taken by the patent proprietor and not by the EPO;
   therefore, the introduction of the limitation procedure does not
   constitute a substantial transference of sovereign rights. The German
   and Swiss delegation pointed out that the central limitation procedure
   would be an appropriate means to solve cases like the one of the
   so-called "Edinburgh-patent" in a fast and cheap way: If a patent is
   not in line with regulations concerning biotechnological inventions,
   this kind of error can be removed by an action of the patent
   proprietor. Therefore, the limitation procedure was also seen as an
   answer to the criticism voiced by Greenpeace.
   
   The President of the EPO made it clear that there is no real
   "constitutional problem" but rather a political problem in Sweden.
   Germany added that the legal situation in Denmark and Sweden will not
   change in the near future.
   
   [6][Officers] 
   
   The observer delegations supported the introduction of the limitation
   procedure. The UNION pointed out that this procedure will be very
   helpful if a third party finds alleged new prior art and thereby tries
   to extort a free license from the patent proprietor.
   
   The AIPPI also expressed its favorable opinion on the central
   limitation procedure.
   
   Dr. Grossenbacher proposed an amendment allowing each contracting
   state to reserve the right that the limitation procedure shall have no
   effect in respect of that state. Many delegations rejected this
   proposal in order to secure the unity of the convention. Finally 11
   states voted in favor of this amendment. Therefore, this amendment was
   dismissed and the Basic Proposal adopted.
   
   V.
   
   Petition for review by the Enlarged Board of Appeals, Article 112a EPC
   
   In order to make possible a limited judicial review of decisions of
   the boards of appeal, the Enlarged Board of Appeal was given the
   competence to decide on petitions for review.
   
   According to the Basic Proposal, a petition can be filed on two
   grounds: A fundamental procedural defect as defined in the
   Implementing Regulations occurred in the appeal proceedings, or a
   criminal act which may have had an impact on the decision.
   
   In contrast, France, Germany and other delegations preferred a
   non-exclusive list of "fundamental procedural defects" in Article 112a
   EPC in order to allow a flexible definition of that term by case law.
   The Netherlands, Sweden and Switzerland even proposed not to name any
   examples of "fundamental procedural defects" at all. On the other
   hand, Italy and other delegations proposed to introduce an exclusive
   list of "fundamental procedural defects" in the EPC in order to
   prevent a excessive extension of this mean of redress.
   
   Finally, Germany, France, and the EPO proposed as a compromise the
   introduction of an exclusive list of "fundamental procedural defects"
   defined partly in the convention, partly in the Implementing
   Regulations. This proposal was unanimously adopted.
   
   VI.
   
   Doctrine of equivalents, Protocol on Article 69 EPC
   
   The discussion on this protocol was very controversial. The Basic
   Proposal provided to rename the present provision of the Protocol as
   "Article 1" and to add two new Articles: Article 2 had the following
   wording:
   
   (1) For the purpose of determining the extent of protection conferred
   by a European patent, due account shall be taken of means which at the
   time of the alleged infringement are equivalents to the means
   specified in the claims.
   
   (2) A means shall generally be considered as being equivalent if it is
   obvious to a person skilled in the art that using such means achieves
   substantially the same result as that achieved through the means
   specified in the claim.
   
   [7][Officers] 
   
   Article 3 provided that for the purpose of determining the extent of
   protection, due account shall be taken of the so called "file wrapper
   estoppel", i.e. statements limiting the extent of protection made by
   the applicant or the proprietor in the application, the patent, or
   during proceedings concerning the grant or validity of the patent.
   
   The UK strongly opposed the introduction of Articles 2 and 3 because
   this proposal would not attain the aim of harmonizing European patent
   law. A legal definition of "equivalence" cannot prevent different ways
   of interpreting this definition.
   
   Furthermore, the UK criticized that there is no European consensus
   concerning the critical date for determining the scope of protection.
   Additionally, Europe should keep in mind the negative experiences with
   the file wrapper estoppel made by the Americans.
   
   Several observer delegations emphasized that further considerations
   are needed and therefore the discussion should be postponed to the
   "second basket".
   
   The AIPPI disagreed with the definition of equivalence given in the
   basic proposal but stated that the principle of equivalence must be
   kept as indicated in Article 2 (1) without precise reference to the
   date of appreciation of the equivalence.
   
   Switzerland took the position that this issue has been discussed for a
   very long time and further discussions would not improve the results.
   Therefore the Basic Proposal should be adopted.
   
   Germany also supported the Basic Proposal because it provided a
   harmonization of European patent law although the German patent law is
   already in line with the proposed provisions.
   
   Belgium took the view that a legal definition of "equivalence" would
   be very helpful as there is no legal definition of the term
   "invention". The AIPPI replied that there is no definition of
   "equivalence" in the US law as well.
   
   Finally, Germany, the UK, and France proposed as compromise a new
   wording of Article 2 (see MR/PLD 6/00):
   
   "For the purpose of determining the extent of protection conferred by
   a European patent, due account shall be taken of any element which is
   equivalent to an element specified in the claims."
   
   No definition of "equivalence" is given. Germany pointed out that this
   proposal permits national courts to apply the doctrine of equivalence
   in a flexible and "fair" way.
   
   The Swiss delegation asked why the word "means" had been replaced by
   "elements". The UK answered that this wording derives from the Patent
   Law Treaty and fits better to chemical inventions. The CNIPA took the
   view that the word "element" is not as comprehensive as the word
   "means".
   
   Finally, Article 2 (according to MR/ PLD 6/00, see above) was adopted
   by 16 votes.
   
   After this decision, Denmark proposed to remove Article 3 of the
   Protocol (file wrapper estoppel) from the Basic Proposal as well. The
   Danish delegation argued that the interested circles had demanded
   further considerations on this issue and that this need should be
   respected. No objections were raised and this proposal was adopted by
   (just) 14 votes.
   
   [8][Officers] 
   
   VII.
   
   Transitional Provisions
   
   The draft of the Revision Act did not contain precise transitional
   provisions. Article 7a was therefore added and discussed in depth. The
   AIPPI stressed that the revision should not have retroactive effect.
   On the basis of a paper drafted by the EPO, The German, British and
   Dutch delegations suggested an amended wording, which was adopted as
   follows:
   
   (1) The revised version of the Convention shall apply to all European
   parent applications filed after its entry into force, , as well as to
   all patents granted in respect of such applications. It shall not
   apply to European patents already granted at the time of its entry
   into force, or to European patent applications pending at that time,
   unless otherwise decided by the Administartive Council of the European
   Patent Organization.
   
   (2) The Administrative Council of the European Patent Organization
   shall take a decision under paragraph (1) no later than 20 June 2001
   by a majority of three quaters of the Contracting States represented
   and voting. Such decision shall become an integral part part of this
   Revision Act.
   
   VIII.
   
   The Conference Resolution
   
   All governmental delegations agreed to discuss an "conference
   resolution" which would summarize the results of the Diplomatic
   Conference and give a general view for the future.
   
   A lengthy debate took place before the adoption of a text to be
   unanimously approved. Some delegations strongly desired that an agenda
   was fixed. In contrast, a basic proposal stemming from the EPO and
   supported by theAIPPI, did not contain any limit for preparing a new
   diplomatic conference.
   
   The resolution specifically refers to software and biotechnological
   inventions among the topics for further considerations:
   
   Proposals for further revision might in particular relate to software
   and such changes as are required to implement the Community Patent
   without delay, bearing in mind the declaration of the heads of state
   and government of the European Union made in Lisbon in March 2000.
   
   E.
   
   Future Perspectives
   
   The conference has shown that European patent law remains a very
   important issue; however, there has been a shift concerning the
   leading players in this field: As Dr. Grossenbacher pointed out in his
   concluding remarks, the European Community is more and more
   controlling the further development of substantial European patent
   law. The EPO interprets its future role as providing mainly a
   procedural framework concerning patent prosecution.
   
   The schedule for the "second basket" has not been determined, although
   this "second basked" served as a kind of excuse for the postponement
   of very important issues like software and biotech patents and the
   community patent. This cast a shadow on the intention to continue the
   process of reform. However, the steps of this process of reform
   already taken at the Diplomatic Conference are very important and
   should be regarded as an example of impressive willingness to
   compromise.
   
   (1) German group of the AIPPI; Max Planck Institute, Munich.
   R.Nack@intellecprop.mpg.de
   
   (2) Reporter General of AIPPI
   
   [9][Officers] 
   
   [bluepixel.gif]
   [ [10]Home ][ [11]Contact ][ [12]Top ]

References

   1. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/news.html
   2. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/aippi-diplconf00/report-EPO-Dipl.Conf.htm#top
   3. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/aippi-diplconf00/report-EPO-Dipl.Conf.htm#top
   4. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/aippi-diplconf00/report-EPO-Dipl.Conf.htm#top
   5. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/aippi-diplconf00/report-EPO-Dipl.Conf.htm#top
   6. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/aippi-diplconf00/report-EPO-Dipl.Conf.htm#top
   7. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/aippi-diplconf00/report-EPO-Dipl.Conf.htm#top
   8. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/aippi-diplconf00/report-EPO-Dipl.Conf.htm#top
   9. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/aippi-diplconf00/report-EPO-Dipl.Conf.htm#top
  10. file://localhost/
  11. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/contact.html
  12. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/aippi-diplconf00/report-EPO-Dipl.Conf.htm#top
