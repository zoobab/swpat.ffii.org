<!-- Vignette V/5 Thu Sep 04 00:15:49 2003 -->
<!--WEB 3-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>

    <link rel="alternate" type="application/rss+xml" title="PC Magazine: New Product Reviews" href="http://rssnewsapps.ziffdavis.com/pcmag.xml" />


<title>Patent Riots of 2003</title>
<META http-equiv="expires" CONTENT="Thu, 01 Dec 1994 16:00:00 GMT">
<META http-equiv="Pragma" CONTENT="no-cache">
<META name="keywords" content="Public software Copyright Patents " lang="en-us">
<META name="title" content="Patent Riots of 2003" lang="en-us">
<META name="description" content="The title of this column may be something of an exaggeration, but there seems to be a strong protest movement that has begun in Europe regarding software patents." lang="en-us">
  
<link rel="STYLESHEET" type="text/css" href="/util/css/PCMag.css">

<script language="javascript">

function setCookie(name, value) {   
	var expire = new Date();
	var today = new Date();
	expire.setTime(today.getTime() + 1000*60*60*24*7);
	document.cookie = name + "=" + escape(value) + "; path=/"   + ((expire == null) ? "" : ("; expires=" + expire.toGMTString()))
}
	
function getCookie(Name) {   
	var search = Name + "="   
	if (document.cookie.length > 0) {// if there are any cookies      
		offset = document.cookie.indexOf(search)       
		if (offset != -1) { // if cookie exists          
			offset += search.length; 
			// set index of beginning of value         
			end = document.cookie.indexOf(";", offset)
			// set index of end of cookie value         
			if (end == -1)             
				end = document.cookie.length         
			return unescape(document.cookie.substring(offset, end))      
		}    
	}
}

function getMemberName(x) {
	start = (x.indexOf(";") + 1);
	end = x.indexOf(";", start);
	var name = x.substr(start, (end - start));
	return name
}

function displayPopUnder() {
	var cookieExist = getCookie("PC Magazine");
	if (document.referrer&&document.referrer.indexOf('altavista')==-1) {
        if (document.referrer.indexOf('google')==-1) {
            if (cookieExist == null) {
			    setCookie("PC Magazine","PC Magazine");
		    	var promopopup = window.open("http://www.zdmcirc.com/zdmcirc/popups/pcmpop.html","promopopup","width=336,height=280");	
		    	self.focus();
		    }
        }  
    }
}

function cj_window(a_path) {
	article_window=window.open(a_path,'article_window','width=415,height=550,scrollbars=yes,resize=yes,target=_top');
	if (document.images) {article_window.focus();}
}


setCookie("successpage", document.location.pathname)
</script>

</head>					


<body topmargin="0" leftmargin="0" bgcolor="#BEC5CB" marginheight="0" marginwidth="0" onUnload="displayPopUnder();"><center>



<!---------------- Begin Header Table ---------------->


<table border="0" width="780" cellpadding="0" cellspacing="0" background="/images/pcm_nav_bg.gif">
	<tr>
		<td><img src="/images/pcm_spacer.gif" width="6" height="1"></td>
		<td valign="top">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr><td><img src="/images/pcm_spacer.gif" width="1" height="8"></td></tr>
				<tr>
					<td valign="top" align="left"><a href="http://www.pcmag.com/default/0,4148,,00.asp" target="_top"><img src="/images/pcm_header.gif" border="0" width="74" height="78"></a></td>
				</tr>
				<tr><td><img src="/images/pcm_spacer.gif" width="1" height="3"></td></tr>
				<tr>
					<td valign="top" align="left"><a href="https://www.zdmcirc.com/zdmcirc/default.asp?LK=TXTLKLOGO&I=ibmp" target="_top"><img src="/images/pcm_subscribe.gif" border="0" width="65" height="15"></a></td>
				</tr>
			</table>
		</td>
		<td><img src="/images/pcm_spacer.gif" width="4" height="120"></td>
		<td width="100%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="top" align="left"><img src="/images/pcm_spacer.gif" width="1" height="5"></td>
					<td rowspan="2" width="100%"><img src="/images/pcm_spacer.gif" width="1" height="37"></td>
					<td rowspan="2" valign="top" align="right"><a href="http://www.ziffdavis.com" target="_top"><img src="/images/pcm_zifflogo.gif" border="0" width="103" height="20"></a></td>
				</tr>
				<tr>
					<td align="left" valign="top"><img src="/images/pcm_tagline.gif" width="260" height="22"></td>
				</tr>
			</table>
			

	<table border="0" cellpadding="0" cellspacing="0" class="">
		<tr>
			
			<td valign="top">
				<a href="http://www.pcmag.com/category2/0,4148,94798,00.asp" target="_top"><img src="/images/pcm_NAV_home_off.gif" width="63" height="19" alt="Home" border="0"></a><br>
			<td>
								


	
				
					<td valign="top">
					
							<img src="/images/pcm_NAV_break_off.gif" width="1" height="19"><br>
					
					</td>
					<td valign="top">
						<a href="http://www.pcmag.com/category2/0,4148,22,00.asp" target="_top"><img src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=23535,00.jpg" width="86" height="19" alt="reviews" border="0"></a><br>
					<td>
				

	
				
					<td valign="top">
					
							<img src="/images/pcm_NAV_break_off.gif" width="1" height="19"><br>
					
					</td>
					<td valign="top">
						<a href="http://www.pcmag.com/category2/0,4148,23,00.asp" target="_top"><img src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=23527,00.jpg" width="106" height="19" alt="downloads" border="0"></a><br>
					<td>
				

	
				
					<td valign="top">
					
							<img src="/images/pcm_NAV_break_off.gif" width="1" height="19"><br>
					
					</td>
					<td valign="top">
						<a href="http://www.pcmag.com/category2/0,4148,28,00.asp" target="_top"><img src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=23529,00.jpg" width="98" height="19" alt="solutions" border="0"></a><br>
					<td>
				

	
				
					<td valign="top">
					
							<img src="/images/pcm_NAV_break_off.gif" width="1" height="19"><br>
					
					</td>
					<td valign="top">
						<a href="http://www.pcmag.com/category2/0,4148,24,00.asp" target="_top"><img src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=23531,00.jpg" width="70" height="19" alt="news" border="0"></a><br>
					<td>
				

	
				
					<td valign="top">
					
							<img src="/images/pcm_NAV_break_on.gif" width="1" height="19"><br>
					
					</td>
					<td valign="top">
						<a href="http://www.pcmag.com/category2/0,4148,30,00.asp" target="_top"><img src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=23533,00.jpg" width="90" height="19" alt="opinions" border="0"></a><br>
					<td>
				

	
				
					<td valign="top">
					
							<img src="/images/pcm_NAV_break_on.gif" width="1" height="19"><br>
					
					</td>
					<td valign="top">
						<a href="http://www.pcmag.com/category2/0,4148,37,00.asp" target="_top"><img src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=23525,00.jpg" width="69" height="19" alt="shop now" border="0"></a><br>
					<td>
				

	
				
					<td valign="top">
					
							<img src="/images/pcm_NAV_break_off.gif" width="1" height="19"><br>
					
					</td>
					<td valign="top">
						<a href="http://www.pcmag.com/category2/0,4148,36,00.asp" target="_top"><img src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=23523,00.jpg" width="102" height="19" alt="discussions" border="0"></a><br>
					<td>
				

		</tr>
	</table>

		<table border="0" cellpadding="0" cellspacing="0" width="100%" height="32" class="SubNav_BG">
			<tr>

					<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>
					<td width="20%" align="left" valign="middle">
						<img src="/images/pcm_SubNav_arrow.gif" width="5" height="7">&nbsp;<a href="http://www.pcmag.com/category2/0,4148,5378,00.asp" class="SubNav"><nowrap>Michael J. Miller</nowrap></a>
					</td>

					<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>
					<td width="20%" align="left" valign="middle">
						<img src="/images/pcm_SubNav_arrow.gif" width="5" height="7">&nbsp;<a href="http://www.pcmag.com/category2/0,4148,2653,00.asp" class="SubNav"><nowrap>Bill Machrone</nowrap></a>
					</td>

					<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>
					<td width="20%" align="left" valign="middle">
						<img src="/images/pcm_SubNav_arrow_on.gif" width="5" height="7">&nbsp;<span class="SubNav"><nowrap>John C. Dvorak</nowrap></span>
					</td>

					<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>
					<td width="20%" align="left" valign="middle">
						<img src="/images/pcm_SubNav_arrow.gif" width="5" height="7">&nbsp;<a href="http://www.pcmag.com/category2/0,4148,1103,00.asp" class="SubNav"><nowrap>Inside Track</nowrap></a>
					</td>

					<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>
					<td width="20%" align="left" valign="middle">
						<img src="/images/pcm_SubNav_arrow.gif" width="5" height="7">&nbsp;<a href="http://www.pcmag.com/category2/0,4148,6362,00.asp" class="SubNav"><nowrap>Bill Howard</nowrap></a>
					</td>
</tr><tr>
					<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>
					<td width="20%" align="left" valign="middle">
						<img src="/images/pcm_SubNav_arrow.gif" width="5" height="7">&nbsp;<a href="http://www.pcmag.com/category2/0,4148,6363,00.asp" class="SubNav"><nowrap>Lance Ulanoff</nowrap></a>
					</td>

						<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>				
						<td width="20%"><img src="/images/pcm_spacer.gif" width="1" height="1"></td>

						<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>				
						<td width="20%"><img src="/images/pcm_spacer.gif" width="1" height="1"></td>

						<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>				
						<td width="20%"><img src="/images/pcm_spacer.gif" width="1" height="1"></td>

						<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>				
						<td width="20%"><img src="/images/pcm_spacer.gif" width="1" height="1"></td>

			</tr>
		</table>
			<table cellpadding="0" cellspacing="0"  border="0" width="100%">
				<tr>
					<td colspan="4"><img src="/images/pcm_spacer.gif" width="1" height="2"></td>
				</tr>
				<tr>
					<td><img src="/images/pcm_spacer.gif" width="50" height="1"></td>
					<td align="left" class="nav_text">
						<nowrap>
						<!-- Vignette V/5 Thu Sep 04 00:15:39 2003 -->
<!--WEB 3-->


		<a href="http://www.pcmag.com/view_profile/0,2993,,00.asp" target="_top" class="nav_text">My Account</a>&nbsp;|&nbsp;
		<script language="javascript">
			var isMemberCookie = getCookie("zm%5Fuser");
			var signInUrl = "http://www.pcmag.com/sign_up/0,3017,,00.asp" + "?success_page=" + getCookie("successpage")  
			var signOutUrl = "http://www.pcmag.com/data_process_log_out/0,3281,,00.asp" + "?success_page=" + getCookie("successpage")  
			var registerUrl = "http://www.pcmag.com/join/0,3004,,00.asp" + "?success_page=" + getCookie("successpage")  
			
			if (isMemberCookie != null) {
				document.write("<a href=\"" + signOutUrl + "\" target=\"_top\" class=\"nav_text\">Sign Out</a>&nbsp;&nbsp;");
			} else {
				document.write("<a href=\"" + signInUrl + "\" target=\"_top\" class=\"nav_text\">Sign In</a>&nbsp;&nbsp;");
			}
		</script>		
		
		
		<script language="javascript">
		
			var isMemberCookie = getCookie("zm%5Fuser"); 

			if (isMemberCookie != null) {
				var memberName = getMemberName(isMemberCookie);
				document.write("Welcome " + memberName);
			} else {
				document.write("Not a member? <a href=\"" + registerUrl + "\" target=\"_top\" class=\"nav_text\">Join now</a>");
			}
		
		</script>

						</nowrap></td>					
					<td><img src="/images/pcm_spacer.gif" width="1" height="1"></td>
					
					<form name="SmpleSearchForm" action="/search_redirect/" method="GET" target="_top">
					<td align="right">						
						<img src="/images/pcm_search.gif">&nbsp;
						<input name=qry type="text" size="10" maxlength="70" STYLE="font-size:12px;">
						<select name=site size="1" STYLE="font-size:12px;">	
							<option value='0|all| '> All Sites</option><option selected value="3|PC+Magazine|http://www.pcmag.com/">PC Magazine</option>
						</select>
						<input type=image src="/images/pcm_searcharrow.gif"  border="0" alt="">
					</td>
					</form>
				</tr>
			</table>
		</td>
		<td><img src="/images/pcm_spacer.gif" width="5" height="1"></td>
	</tr>
</table>


<!---------------- End Header Tables ----------------->


<!-- Call the module data file before the main content area to check whether there are modules to display -->


<!-- ************* BEGIN MAIN TABLE ***********-->
<table border="0" cellpadding="0" cellspacing="0" width="780" bgcolor="#FFFFFF">	
 
	<tr>
		<td colspan="4"><img src="/images/pcm_spacer.gif" width="1" height="5"></td>
	</tr>
	
	<tr>
		<td width="5"><img src="/images/pcm_spacer.gif" width="5" height="1"></td>
		<td valign="top" align="left" colspan="3">
		<!-- Vignette V/5 Thu Sep 04 16:08:34 2003 -->
<!--WEB 3--><table border="0" cellpadding="0" cellspacing="0" width="100%">

	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
	
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3"><img src="/images/pcm_spacer.gif" height="3" width="5"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td width="100%">

	<div style="margin-top=5;margin-bottom=5;">
	<span class="Breadcrumb_Content">

<a class="Breadcrumb_Content" href=http://www.pcmag.com/default/0,4148,,00.asp>Home</a>&nbsp;>&nbsp;<a class="Breadcrumb_Content" href=http://www.pcmag.com/category2/0,4148,30,00.asp>Opinions</a>&nbsp;>&nbsp;<a class="Breadcrumb_Content" href=http://www.pcmag.com/category2/0,4148,3574,00.asp>John C. Dvorak</a>&nbsp;>&nbsp;<b>Patent Riots of 2003</b>
		</span>
		</div>

		</td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3" width="100%"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
</table>

		</td>
				
		<td width="5" rowspan="2">
			<img src="/images/pcm_spacer.gif" width="5" height="1">
		</td>
	</tr>


	<tr>
		<td width="5"><img src="/images/pcm_spacer.gif" width="5" height="1"></td>
		<td width="615" valign="top">
	



<SCRIPT LANGUAGE="JavaScript">
<!--
  function AlterActionAndSubmit()
  {
	if (document.toc_form.toc_select.value != "")
		{
		toc_select_index = document.toc_form.toc_select.selectedIndex;
		toc_select_value = document.toc_form.toc_select.options[toc_select_index].value;

		document.toc_form.action = toc_select_value;
		document.toc_form.submit();
		}
  }

function OpenSlideshowWindow(slideshow_name, winWidth, winHeight,isScrollbar)
{
LeftPosition = (winWidth) ? (winWidth) : 0;
TopPosition = (winHeight) ? (winHeight): 0;
settings = 'menubar=no,toolbar=no,scrollbars=yes,height=' + winHeight + ',width=' + winWidth
hWin = window.open(slideshow_name, "SlideshowPopup", settings, false);
hWin.focus();
if (hWin.opener == null) hWin.opener = self;
}

// -->
</SCRIPT>

<span class="dot"><br></span> 

<table border="0" cellpadding="0" cellspacing="0" width="100%">


		

    	<tr valign="top">
			<td align="left" valign="top">
			
				<table border="0" cellspacing="0" cellpadding="0" align="left" width="75">
				
					<tr>
	   					<td>
								
						
									
								<img src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=23485,00.jpg" width="70" height="70" border="0" alt="Dvorak">
								
						</td>
						<td>&nbsp;</td>
					</tr>
				</table>

    	<span class="Article_Title">Patent Riots of 2003</span>
	
		<br><span class="Small_Content"><span class="authorsource">By&nbsp;<a href="http://www.pcmag.com/author_bio/0,3055,a=123,00.asp" class="authorsource">John C. Dvorak</a></span></span>

		<br><span class="Article_Date">September 2, 2003</span><!-- Vignette V/5 Thu Sep 04 00:15:50 2003 -->
<!--WEB 3--><span class=dot><br><br></span><span class="Article_Posts"	align="left"><a	href="http://discuss.pcmag.com/pcmag/start/?msg=28039"	class="Article_Posts">Total posts:	30</a></span><br>


  </td>
  <td align="right" valign="top">
		
	  
  </td>


  </tr>
 <tr>
  <td align="left" class="Article_Posts" colspan="2">


	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td width="100%" class="content10">

	</td>


	</tr>
	</table>

 </td></tr>




<tr valign="top">
	<td align="left" class="Article_Content" colspan="2"><p><br>
		
<script language="JavaScript">
function OpenSaveArticleWindow(querystring)
{
LeftPosition = (screen.width) ? (screen.width)/10 : 0;
TopPosition = (screen.height) ? (screen.height)/10 : 0;
settings = 'menubar=no,height=400,width=500'
hWin = window.open("/save_article/" + querystring, "SaveArticle", settings, false);
hWin.focus();
if (hWin.opener == null) hWin.opener = self;
}

function OpenEmailArticleWindow(querystring)
{
	LeftPosition = (screen.width) ? (screen.width)/10 : 0;
	TopPosition = (screen.height) ? (screen.height)/10 : 0;
	settings = 'menubar=no,height=575,width=365,resizable=yes,scrollbars=yes'
	hWin = window.open("/email_article/" + querystring, "SaveArticle", settings, true);
	hWin.focus();
	if (hWin.opener == null) hWin.opener = self;
}



</script>

 <table cellpadding="0" cellspacing="0" border="0">

	<tr>
     <td align="left"><a href="http://www.pcmag.com/print_article/0,3048,a=58679,00.asp" class="link6"><img src="/images/pcm_print_icon.gif" alt="Print" border="0" align="absmiddle"></a></td>
     <td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>
	 <td align="left"><a href="JavaScript:OpenEmailArticleWindow('?a=58679&s=1500&c=/');" class="link6"><img src="/images/pcm_email_icon.gif" alt="email" border="0" align="absmiddle"></a></td>
   
			<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>
     		<td align="left"><a href="http://www.pcmag.com/sign_up/0,3017,,00.asp?success_page=http%3A%2F%2Fwww%2Epcmag%2Ecom%2Farticle2%2F0,4149,1236390,00%2Easp&success_title=Article&failure_page=" class="link6"><img src="/images/pcm_save_icon.gif" alt=""  border="0" align="absmiddle"></a></td>
			<td><img src="/images/pcm_spacer.gif" width="10" height="1"></td>
			<td align="left">	
	  		<a href="http://discuss.pcmag.com/pcmag/start/?msg=28039"><img src="/images/pcm_discuss_icon.gif"  alt="" border="0" align="absmiddle"></a>
	   		
			</td>
 
		</tr>
 </table>
<p>Well, the title of this column may be something of an exaggeration, but there seems to be a strong protest movement that has begun in Europe regarding software patents. It could easily become a juggernaut that will make legislative bodies reconsider the tendency to approve dubious copyright and patent laws that benefit nobody but large corporations. In the copyright arena, there are a lot of people complaining about how Disney made its fortune using public domain stories and characters such as Sleeping Beauty and Bambi (to name a very few), then spent most of its efforts preventing any of its own creations from ever becoming public domain. That battle is looming next. For now, the issue is how oppressive the laws are surrounding software patents. And remember that before 1980 a software patent was incredibly rare.</p>

<p><TABLE cellSpacing=2 cellPadding=2 align=left bgColor=#cccccc border=0><TR><TD><font color="#ffffff" style="FONT-SIZE:10px;">ADVERTISEMENT</font><table border="0" cellpadding="0" cellspacing="0" class="bgcolor2" vspace="3" hspace="3"><tr><td class="content5"><IFRAME SRC="http://ad.doubleclick.net/adi/pcm.dart/opinions;sz=336x280;ord=5557018519?" width="336" height="280" frameborder="no" border="0" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" align="right"><A HREF="http://ad.doubleclick.net/jump/pcm.dart/opinions;abr=!ie;sz=336x280;ord=5557018519?"><IMG SRC="http://ad.doubleclick.net/ad/pcm.dart/opinions;abr=!ie;sz=336x280;ord=5557018519?" border=0 height="280" width="336"></A></IFRAME> </td></tr></table></td></tr></table><!--storyboxend7--><p>The protest fomenter is the Foundation for Free Information Infrastructure&#151;FFII (<a href="http://www.ffii.org" target="_blank">www.ffii.org</a>)&#151;based in Germany. It has organized a lot of physical protests and now is promoting online protests to shut down Web sites. Much traffic from shuttered sites gets redirected to the protest page <a href="http://swpat.ffii.org/group/demo/" target="_blank">http://swpat.ffii.org/group/demo/</a>. A laundry list of examples, the "list of horrors" at <a href="http://swpat.ffii.org/patents/effects/index.en.html" target="_blank">http://swpat.ffii.org/patents/effects/index.en.html</a> details many of the complaints.</p>

<p>None of this is a surprise. The tendency to allow patents for just about any concept has been encouraged by governments worldwide. That lawyers are behind this is no coincidence. It's is a pure bonanza for them. Unfortunately, it has the potential to kill innovation and progress. And clearly this is not what the original concept of patents was all about.</p>

<p><!-- Vignette V/5 Thu Sep 04 00:49:40 2003 -->
<!--WEB 3-->


<!-- RELATED LINKS -->
<p>I was the moderator for a Commonwealth Club debate between fabled Law Professor Larry Lessig and Todd Dickinson a couple of years ago. Dickinson was the director of the US Patent and Trademark Office under Clinton and a huge promoter of the idea that business models should be patented. From what I could tell, he thought everything should be patented. So I asked him if a football play could be patented. He said probably not, since there had to be something technological about it to qualify. "But what if this was a timing play?" I asked. My jaw dropped when he said it could probably get a patent! I was stunned at such an outrageous and stupefying notion. What can you say to a guy like this?</p>

<p>For a moment I thought about some plays that might get patented. Since then, I've heard tales about patents for how kids can swing on trees and thousands of other idiotic patent ideas. Dumb patents have been around from the first days, but software patents are a recent affliction, and they're going to kill the computer business. Here are two examples from the FFII horror-story bank.</p>

<p>The first is the Ugly TrueType and OpenType Font Display Patent. According to the FFII site: "Rendering of Fonts is ugly and slow on Free Software Systems. This is because when the TrueType standard was promoted by Apple and Microsoft, they held a few patents which they never asserted. The FreeType project has asked Apple to clarify the situation, but did not get an answer. Instead, fearful customers of Linux distributors such as SuSE and Redhat (sic) have demanded that any possibly infringing FreeType features be disabled on these distributions. TrueType is the dominating font standard and it is also a part of new standards such as OpenType, in which Adobe participates. Adobe also holds a few patents on which OpenType infringes. These formats must be supported if GNU/Linux/XFree users are to be able to use existing fonts on their platform of choice."</p>
<P><table align="left" width="143" border="0" cellpadding="0" cellspacing="0"><tr><td width="10"><img src="/images/pcm_spacer.gif" borer="0" width="10" height="1"></td><td width="123" align="left" class="DiscussBG"><img src="/images/pcm_spacer.gif" borer="0" width="123" height="1"></td><td width="10"><img src="/images/pcm_spacer.gif" borer="0" width="10" height="1"></td></tr><tr><td width="143" colspan="3"><img src="/images/pcm_spacer.gif" borer="0" width="1" height="3"></td></tr><tr><td width="10"><img src="/images/pcm_spacer.gif" borer="0" width="10" height="1"></td><td width="123" align="left"><img src="/images/pcm_discuss_header.gif" borer="0" width="123" height="16"></td><td width="10"><img src="/images/pcm_spacer.gif" borer="0" width="10" height="1"></td></tr><tr><td width="143" colspan="3"><img src="/images/pcm_spacer.gif" borer="0" width="1" height="10"></td></tr><tr><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><td align="left" class="DiscussText"><a class="DiscussMember" href="http://www.pcmag.com/view_profile/0,2993,m=2037790,00.asp"><b>b_nadolson: </b></a>Intellectual property law is getting out of control.</td><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td></tr><tr><td colspan="3"><img src="/images/pcm_spacer.gif" width="1" height="10"></tr><tr><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><td align="left" class="DiscussText"><a href="http://discuss.pcmag.com/pcmag/messages?msg=28039.2" class="DiscussText"><b>view full post &gt;</b></a></td><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td></tr><tr><td colspan="3" align="center"><img src="/images/pcm_spacer.gif" width="143" height="4"></td><tr><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><td align="center" class="DiscussBreakBG"><img src="/images/pcm_spacer.gif" width="106" height="1"></td><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><tr><td colspan="3" align="center"><img src="/images/pcm_spacer.gif" width="143" height="3"></td><tr><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><td align="left" class="DiscussText"><a class="DiscussMember" href="http://www.pcmag.com/view_profile/0,2993,m=291874,00.asp"><b>NickNielsen: </b></a>I think it's wonderful.</td><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td></tr><tr><td colspan="3"><img src="/images/pcm_spacer.gif" width="1" height="10"></tr><tr><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><td align="left" class="DiscussText"><a href="http://discuss.pcmag.com/pcmag/messages?msg=28039.3" class="DiscussText"><b>view full post &gt;</b></a></td><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td></tr><tr><td colspan="3" align="center"><img src="/images/pcm_spacer.gif" width="143" height="4"></td><tr><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><td align="center" class="DiscussBreakBG"><img src="/images/pcm_spacer.gif" width="106" height="1"></td><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><tr><td colspan="3" align="center"><img src="/images/pcm_spacer.gif" width="143" height="3"></td><tr><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><td align="left" class="DiscussText"><a class="DiscussMember" href="http://www.pcmag.com/view_profile/0,2993,m=2375438,00.asp"><b>Yorzhik: </b></a>Ideas are not property. They cannot be.</td><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td></tr><tr><td colspan="3"><img src="/images/pcm_spacer.gif" width="1" height="10"></tr><tr><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><td align="left" class="DiscussText"><a href="http://discuss.pcmag.com/pcmag/messages?msg=28039.19" class="DiscussText"><b>view full post &gt;</b></a></td><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td></tr><tr><td colspan="3"><img src="/images/pcm_spacer.gif" width="1" height="10"></td></tr><tr><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td><td class="DiscussBG"><img src="/images/pcm_spacer.gif" width="123" height="1"></td><td><img src="/images/pcm_spacer.gif" width="10" height="1"></td></tr></table><p />
<p>And then there is one of many Adobe actions. This one is good since it impinges on GIMP (GNU Image Manipulation Program): "In summer 2001 Adobe attacked Macromedia for infringing on its US patent no. 5546528 which lays claim to the idea of adding a third dimension to menus by grouping them as tabbed palettes one behind the other. The European Patent Office (EPO) has illegally granted EP0689133 with exactly the same set of claims and priority date of 1994-06-23 on 2001-08-06, after five years of examination period. Many programs, including free software such as The GIMP, infringe on this patent and will therefore be at the mercy of Adobe, if the EPO's practice of granting software patents is made enforceable by a Directive from the European Commission."</p>

<p>I have been relentlessly looking at these and other issues and must conclude that the way things are currently rigged, the big companies can easily lord it over the small fry unless the small fry are also loaded. This is neither a level playing field nor a productive environment.</p>

<p>What do I tell people who see this as a problem? I just tell them to patent every idea they can and never produce any sort of product. It looks impossible to develop anything in this environment except patents and lawsuits. Yeah, like this is going to fix the economy. My conclusion: support the FFII.</p>

<p><i><a href="http://discuss.pcmag.com/pcmag/messages/?msg=28039">Discuss this article in the forums</a>.</i></p>

<span class=highlights_content>
<p class=highlights_content><i>More articles from John Dvorak:</i>

<!-- start ziffauthorarchive //-->
<b></b><br>
&#149 <a href="http://www.pcmag.com/article2/0,4149,1236389,00.asp" >Patent Riots of 2003</a><br>
&#149 <a href="http://www.pcmag.com/article2/0,4149,1229754,00.asp" >The Comic Book Connection</a><br>
&#149 <a href="http://www.pcmag.com/article2/0,4149,1230115,00.asp" >I Can't Get No Satisfaction</a><br>
&#149 <a href="http://www.pcmag.com/article2/0,4149,1229756,00.asp" >DivX Reloaded</a><br>
&#149 <a href="http://www.pcmag.com/article2/0,4149,1224343,00.asp" >License Computer Users</a><br>

	</td>
</tr>
</table>

<table width="100%">
<tr>
  <td align="left" valign="top" nowrap width="50%">
  
  	&nbsp;
  
  </td>
  <td align="right" valign="top" nowrap width="50%">&nbsp;
  
  &nbsp;
  </td>
</tr>
</table>



<br>

<table border="0" cellpadding="0" cellspacing="0" width="615">
	<tr><td colspan="3"><img src="/images/pcm_spacer.gif" height="10" width="1"></td></tr>
	<tr>
		<td><img src="/images/pcm_spacer.gif" height="1" width="5"></td>
		<td class="BorderBG" width="100%"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="5"></td>
	</tr>
	<tr><td colspan="3"><img src="/images/pcm_spacer.gif" height="10" width="1"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">

	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
	
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3"><img src="/images/pcm_spacer.gif" height="3" width="5"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td width="100%">

			
			<!----------------- Begin Subscription Form ----------------->
<table width=100% border=0 cellspacing=0 cellpadding=0><tr height="5" bgcolor=#CCOOOO><td><!-- start ziffimage //--><img width="106" height="21" border="0" src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=26873,00.gif" alt="In Print"><!-- end ziffimage //--></td></tr></table><div align=center>
<table class="Newsletter_Content" width=432 border=0 cellspacing=0 cellpadding=0>
<form action=http://www.neodata.com/ITPS2.cgi method=post>
<tr>
<td rowspan="4" width="200" valign="top"><a href="https://www.zdmcirc.com/zdmcirc/default.asp?LK=COVEMB&I=ibmp"><!-- start ziffimage //--><img width="178" height="145" border="0" src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=27508,00.gif" alt="Current Issue: vol 22 no 16"><!-- end ziffimage //--></a><br clear=all><!-- start ziffimage //--><img width="200" height="20" border="0" src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=26874,00.gif" alt="In This Issue"><!-- end ziffimage //--><br clear=all><span class=Content_Med_Gray>
&#149; <a href="/current_issue/" class=Content_Med_Gray>Table of Contents</a><br>
&#149; <!-- start ziffarticle //--><a href="http://www.pcmag.com/article2/0,4149,1230439,00.asp" class="Content_Med_Gray">Build Your Own PC</a><!-- end ziffarticle //--><br>
&#149; <!-- start ziffarticle //--><a href="http://www.pcmag.com/article2/0,4149,1230638,00.asp" class="Content_Med_Gray">Shoot, Scan, Print: Digital Imaging Tips</a><!-- end ziffarticle //--><br>
&#149; <!-- start ziffarticle //--><a href="http://www.pcmag.com/article2/0,4149,1230612,00.asp" class="Content_Med_Gray">Traveling Music</a><!-- end ziffarticle //--><br></span>
<!-- start ziffimage //--><img width="200" height="20" border="0" src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=26876,00.gif" alt="Subscription Services"><!-- end ziffimage //--><br clear=all><span class="Content_Med_Red">
+ <a href="/previous_issues/" class="Content_Med_Red">Previous Issues</a><br>
+ <a href="http://service.pcmag.com/" class="Content_Med_Red">Subscription & Customer Services</a></span></td>
<td colspan="3" width="232"><input type=hidden value=ibmp name=ItemCode>
<!-- start ziffimage //--><img width="232" height="44" border="0" src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=26875,00.gif" alt="Subscribe Now"><!-- end ziffimage //--></td>
</tr>
<tr><script>
<!--
monthly_codes = new Array("PMF","PMG","PMH","PMJ","PMK","PML","A88","PMA","PMB","PMC","PMD","PME");
now = new Date();
document.write("<input type=\"hidden\" name=\"KeyCode\" value=\"" + monthly_codes[now.getMonth()] + "\">\n");
//--->
</script>
<input type=hidden value=5 name=SourceCode>
<input type=hidden value="New Order" name=OrderType>
<input type=hidden value="Bill Me" name=CcType>
<input type=hidden value=On name=XDB>
<input type=hidden value="IBMP.embed" name=iResponse>
<input type=hidden value="IBMP.full" name=iResponseMoreInfo>
<input type=hidden value=On name=Confirm>
<input type=hidden value=USA name=Country>
<input type=hidden value=22@34.97 name=Term>
<td width="20" valign=top></td>
<td colspan="2" width="212" valign=top>Name<br>
<input type=text maxlength=24 name=FullName size="35" style="font-size:10px;"><br>
Email (e.g. user@jsp.com)<br>
<input type=text maxlength=50 name=EmailAdr size="35" style="font-size:10px;"><br>
Address<br>
<input type=text maxlength=24 name=StreetAdr size="35" style="font-size:10px;"><br>
City<br>
<input type=text maxlength=18 name=City size="35" style="font-size:10px;"></td>
</tr>
<tr>
<td width="20" valign=top></td>
<td width="106" valign=top>State<br>
<input type=text maxlength=2 name=State size="13" style="font-size:10px;"></td>
<td width="106" valign=top>Postal Code<br><input type=text maxlength=10 name=PostalCode size="13" style="font-size:10px;"></td>
</tr>
<tr>
<td width="20" valign=top></td>
<td width="106" class=Content_Med_Gray>
Click "Continue"<br>to proceed to our secure server.</td>
<td width="106">
<input type="image" name="I2" value="Submit" src="http://common.ziffdavisinternet.com/util_get_image/2/0,3363,i=26871,00.gif" border="0" WIDTH="94" HEIGHT="25"></td>
</tr>
<tr height="5"><td colspan=4></td></tr>
</form>
</table></div>	
			<!----------------- End Subscription Form ----------------->	


		</td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3" width="100%"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3"><img src="/images/pcm_spacer.gif" height="3" width="5"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td width="100%">

<table border="0" cellpadding="0" cellspacing="0" width="100%" class="Newsletter_BG">
	<tr>	
		<td colspan="6"><img src="spacer.gif" height="5" width="1"></td>
	</tr>
	<tr>
		<td width="2%"><img src="/images/pcm_spacer.gif" width="5" height"1"></td>
		<td colspan="5" align="left" width="98%"><img src="/images/pcm_newsletters_header.gif"></td>
	</tr>
	<tr>	
		<td colspan="6"><img src="spacer.gif" height="5" width="1"></td>
	</tr>
	<form action="/data_process_newsletter_manage/" name="newslettersForm" method="get">
	<tr>
		<td width="2%"><img src="/images/pcm_spacer.gif" width="5" height"1"></td>
		<td align="left" valign="top" class="Newsletter_Content" width="31%">
			Get PCMag.com's <b>FREE</b> email newsletters delivered to your inbox.
			<br><br>
			Its easy, just follow the steps.
			<br><br>
			Want more? Check out our other newsletters <a href="http://www.pcmag.com/newsletter_manage/0,3020,,00.asp" class="Newsletter_Content"><span class="Newsletter_Content">here</span></a>.
			<br><br>
			Manage your newsletter subscriptions <a href="http://www.pcmag.com/newsletter_manage/0,3020,,00.asp" class="Newsletter_Content"><span class="Newsletter_Content">here</span></a>.
		</td>
		<td width="2%"><img src="/images/pcm_spacer.gif" width="5" height"1"></td>
		<td align="left" valign="top" width="32%" class="Newsletter_Content">
			<b>1. Make your selections:</b><br>
			<table border="0" cellpadding="0" cellspacing="0">
			
						<tr>	
							<td width="10" valign="top"><input type="checkbox" name="1" value="1" checked></td>
							<td width="129" class="Newsletter_Content">Inside PCMag.com<br></td>
						</tr>
						<tr>	
							<td colspan="2"><img src="spacer.gif" height="3" width="1"></td>
						</tr>
		
						<tr>	
							<td width="10" valign="top"><input type="checkbox" name="3" value="1" checked></td>
							<td width="129" class="Newsletter_Content">Internet Business Technology<br></td>
						</tr>
						<tr>	
							<td colspan="2"><img src="spacer.gif" height="3" width="1"></td>
						</tr>
		
						<tr>	
							<td width="10" valign="top"><input type="checkbox" name="2" value="1" checked></td>
							<td width="129" class="Newsletter_Content">Productwire: First Looks Update<br></td>
						</tr>
						<tr>	
							<td colspan="2"><img src="spacer.gif" height="3" width="1"></td>
						</tr>
		
						<tr>	
							<td width="10" valign="top"><input type="checkbox" name="39" value="1" checked></td>
							<td width="129" class="Newsletter_Content">Tip of the Day<br></td>
						</tr>
						<tr>	
							<td colspan="2"><img src="spacer.gif" height="3" width="1"></td>
						</tr>
		
						<tr>	
							<td width="10" valign="top"><input type="checkbox" name="31" value="1" checked></td>
							<td width="129" class="Newsletter_Content">PC Magazine's TrendWatch<br></td>
						</tr>
						<tr>	
							<td colspan="2"><img src="spacer.gif" height="3" width="1"></td>
						</tr>
		
			</table>
		</td>
		<td width="2%"><img src="/images/pcm_spacer.gif" width="5" height"1"></td>
		<td align="left" valign="top" width="31%" class="Newsletter_Content">
			<b>2. Select email format:</b><br>
			&nbsp;&nbsp;&nbsp;&nbsp;<SELECT name="SelFormat"><OPTION value="1" selected> HTML </OPTION><OPTION value="2">Text only</OPTION></SELECT>
			<br><br>
			<b>3. Enter email address:</b><br>
			&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" size="10" name="email" maxlength="64">
			&nbsp;&nbsp;<a href="JavaScript: document.newslettersForm.submit();"><img src="/images/pcm_newsletter_arrow.gif" alt="" border="0"></a>
		</td>
	</tr>
	</form>
</table>

		</td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3" width="100%"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="615">
	<tr><td colspan="3"><img src="/images/pcm_spacer.gif" height="10" width="1"></td></tr>
	<tr>
		<td><img src="/images/pcm_spacer.gif" height="1" width="5"></td>
		<td class="BorderBG" width="100%"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="5"></td>
	</tr>
	<tr><td colspan="3"><img src="/images/pcm_spacer.gif" height="10" width="1"></td></tr>
</table>
<!-- Vignette V/5 Thu Sep 04 00:15:42 2003 -->
<!--WEB 3--><!-- start eSeminars -->

<table width="100%" border="0" cellpadding="3" cellspacing="0" style="border: 1px solid">
<tr class="AdModule_BG"><td class="AdModule_Hdr">
<b>FREE ONLINE SEMINARS FOR EXECUTIVES AND IT PROFESSIONALS</b>
</td></tr>

<tr><td valign="top"><ul class="AdModule_Content">

<li><a href="http://webevents.broadcast.com/ziffdavis/082003/index.asp?loc=bot">08/20 - NAS: Making the Right Choice for Your Business</a></b> with Michael Krieger. <i>Sponsored by Iomega & Microsoft</i>.</li>

<li><a href="http://webevents.broadcast.com/ziffdavis/082103/index.asp?loc=bot">08/21 - Complete Service Management: Beyond Service Help Desk</a></b> with Frank Derfler. <i>Sponsored by FrontRange Solutions</i>.</li>

<li><a href="http://webevents.broadcast.com/ziffdavis/082603/index.asp?loc=bot">08/26 - Patch Management Best Practices</a></b> with Jim Louderback. <i>Sponsored by PatchLink</i>.</li>

</ul></td></tr>

<tr><td align="right" valign="bottom" class="AdModule_Content"><a href="http://www.webseminarslive.com?kc=modmore" class="AdModule_Content">view more eSeminars &gt;&gt;</a>
</td></tr></table>

<!-- end eSeminars --></ZIFFHTML><P>

<!-- start side by side ad module -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="50%" height="100%" style="border-top: 1px solid;border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid">

<!-- left -->

<table width="100%" height="100%" border="0" cellpadding="3" cellspacing="0" style="border: 0px solid">
<tr class="AdModule_BG"><td class="AdModule_Hdr">
			<b>WHITE PAPERS & RESEARCH</b>
</td></tr>
<tr><td>
<span class="AdModule_ContentLarge">
<b>Find the latest research and information on <a href="http://pcmag.bitpipe.com/data/rlist?t=pd_10_20_60&src=integb_pcm_lhmod082503">Server Hardware</a> in PCMag's <a href="http://pcmag.bitpipe.com&src=integb_pcm_lhmod082503">Research Library</a>.</b>
</span>
<ul class="AdModule_Content">

<li><a href="http://pcmag.bitpipe.com/data/detail?id=1057321455_172&type=RES&src=integb_pcm_lhmod082503">UNIX Power. Apple Ease of Use. The Server You Need</a>
<li><a href="http://pcmag.bitpipe.com/data/detail?id=1057165617_631&type=RES&src=integb_pcm_lhmod082503">Strategic Directions of HP NonStop & Partner Technologies</a>
<li><a href="http://pcmag.bitpipe.com/data/detail?id=1058536272_268&type=RES&src=integb_pcm_lhmod082503">Enabling Business Agility Through Server Blade Technology</a>
<li><a href="http://pcmag.bitpipe.com/data/detail?id=1053528658_282&type=RES&src=integb_pcm_lhmod080803">Microsoft Windows Server 2003</a>
<li><a href="http://pcmag.bitpipe.com/data/detail?id=1056553289_944&type=RES&src=integb_pcm_lhmod082503">The Power of Invention - A History of Industry Standard Servers from HP</a>
<li><a href="http://pcmag.bitpipe.com/data/detail?id=1037031028_242&type=RES&src=integb_pcm_lhmod082503">Benchmarking Mainframe Linux vs. Distributed Systems</a>

</ul>
</td></tr>
<tr><td valign="bottom">
<P align="right"><span class="AdModule_Content">
More <a href="http://pcmag.bitpipe.com/data/rlist?t=pd_10_20_60&src=integb_pcm_lhmod082503">Server Hardware &gt;&gt;</a>
</span>
</td></tr>
</table>
</td>

<td width="10"><img src="/images/spacer.gif" width="10" height="1"></td>

<td width="50%" height="100%" style="border-top: 1px solid;border-left: 1px solid; border-right: 1px solid; border-bottom: 1px solid">

<!-- right -->

<table width="100%" height="100%" border="0" cellpadding="3" cellspacing="0" style="border: 0px solid">
<tr class="AdModule_BG"><td class="AdModule_Hdr">
			<b>TECH SHOP @ PCMAG.COM</b>
			</td></tr>
<tr>
<td valign="">
<span class="AdModule_ContentLarge">
<b>Find the BEST PRICES on the most popular tech products in PCMag's <a href="http://pcmag.pricegrabber.com/index.php?mode=pcm_rhmod061303&mode=pcm_rhmod082503">Tech Shop</a>.</b>
</span><br><br>
<span class="AdModule_Content"> 
    
<b>Digital Camera</b>: <a href="http://pcmag.pricegrabber.com/search_getprod.php?masterid=706030&mode=pcm_rhmod082503">Canon PowerShot S400</a><br><br>

<b>Handheld</b>: <a href="http://pcmag.pricegrabber.com/search_getprod.php?masterid=847312&mode=pcm_rhmod082503">HP iPAQ H2210</a><br><br>

<b>Printer</b>: <a href="http://pcmag.pricegrabber.com/search_getprod.php/masterid=600661?mode=pcm_rhmod082503">Epson Stylus Photo 2200</a><br><br>

<b>Notebook</b>: <a href="http://pcmag.pricegrabber.com/search_getprod.php?masterid=669308&mode=pcm_rhmod082503">Satellite 5205-S505</a><br><br>

<b>Monitor</b>: <a href="http://pcmag.pricegrabber.com/search_getprod.php/masterid=675321?mode=pcm_rhmod082503">Samsung SyncMaster 171N</a><br><br>

<b>Software</b>: <a href="http://pcmag.pricegrabber.com/search_getprod.php?masterid=607193&mode=pcm_rhmod082503">Norton Antivirus 2003</a><br><br>

</span>
</td></tr>
<tr><td valign="bottom"><P align="right"><span class="AdModule_Content">
More <a href="http://pcmag.pricegrabber.com/index.php?mode=pcm_rhmod061303&mode=pcm_rhmod082503">Tech Shop &gt;&gt;</a>
</span></td></tr>
</table>

</td></tr></table>

<!-- end side by side ad module -->
<table border="0" cellpadding="0" cellspacing="0" width="615">
	<tr><td colspan="3"><img src="/images/pcm_spacer.gif" height="10" width="1"></td></tr>
	<tr>
		<td><img src="/images/pcm_spacer.gif" height="1" width="5"></td>
		<td class="BorderBG" width="100%"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="5"></td>
	</tr>
	<tr><td colspan="3"><img src="/images/pcm_spacer.gif" height="10" width="1"></td></tr>
</table>
<!-- Vignette V/5 Thu Sep 04 15:01:30 2003 -->
<!--WEB 3-->

<!------------- Start MarketPlace  -------------------->
<table border="0" cellpadding="0" cellspacing="0" width="100%">

	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
	
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3"><img src="/images/pcm_spacer.gif" height="3" width="5"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td width="100%">

		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="Sponsor_BG">
			<tr>
				<td colspan="3"><img src="/images/pcm_spacer.gif" width="1" height="3"></td>
			</tr>
			<tr align="left" valign="top">
				<td><img src="/images/pcm_spacer.gif" width="3" height="1"></td>	
				<td align="left" valign="top">
					<img src="/images/pcm_ad_gr.gif" width="75" height="9"><br>	
					<img src="/images/pcm_marketplace_header.gif" width="85" height="13">
				</td>
				<td><img src="/images/pcm_spacer.gif" width="3" height="1"></td>	
			</tr>
			<tr>
				<td colspan="3"><img src="/images/pcm_spacer.gif" width="1" height="3"></td>
			</tr>
			<tr>
				<td><img src="/images/pcm_spacer.gif" width="3" height="1"></td>
				<td align="left" valign="top">	
					<table border="0" cellpadding="0" cellspacing="0" class="Sponsor_BG">
						<?xml version="1.0" encoding="UTF-16"?><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td align="left" valign="middle"><img src="/images/pcm_arrow_blugray.gif" width="5" height="8" /></td><td><img src="/images/spacer.gif" width="7" height="1" /></td><td align="left" valign="middle"><a href="http://links.industrybrains.com/?QFF1BxH3F3NZA1D3YF1m1j1L2AGDK2k1F1jDH2FDPE1E3WXL2L3E2k1O1K2G3k1RoXA1n1AcIL5I4A3A4O5pL1VF13K5pN5H3pqB4iO5rwO5kK5ppG13L3M5tL5N5H3F13H3N5K5rB4L3rwvC4B3N1" class="Sponsor_Content" >Intuit Track-It! Help Desk Software </a></td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td colspan="2"><img src="/images/spacer.gif" width="1" height="1" /></td><td valign="top" align="left" class="Sponsor_Content">Intuit IT Solutions provides Track-It! - the leading help desk software solution for call tracking, problem resolution, employee &amp; customer self-help, remote control, asset management, LAN/PC auditing, and electronic software distribution. Free demo</td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td colspan="3"><img src="/images/spacer.gif" width="1" height="5" /></td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td align="left" valign="middle"><img src="/images/pcm_arrow_blugray.gif" width="5" height="8" /></td><td><img src="/images/spacer.gif" width="7" height="1" /></td><td align="left" valign="middle"><a href="http://links.industrybrains.com/?QFF1BxH3F3NZA1D3TM3DGE2FZHO2I3E2I2O1G1G4N5K5zK5HLB4rA4F13H3B4pN5J3rK5C4sK5wO5szK5utC4vK5F13tpC13oqB4vquvJ4G1" class="Sponsor_Content" >Enterprise Content Security from FutureSoft </a></td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td colspan="2"><img src="/images/spacer.gif" width="1" height="1" /></td><td valign="top" align="left" class="Sponsor_Content">Internet filtering, e-mail filtering and file surveillance with the DynaComm i:series product family from FutureSoft provides your organization with a complete enterprise content security solution.  Learn more and download free trials today.</td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td colspan="3"><img src="/images/spacer.gif" width="1" height="5" /></td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td align="left" valign="middle"><img src="/images/pcm_arrow_blugray.gif" width="5" height="8" /></td><td><img src="/images/spacer.gif" width="7" height="1" /></td><td align="left" valign="middle"><a href="http://links.industrybrains.com/?QFF1BxH3F3ZAJ2Zj1mLO1D2YGN5N3l1G2kG13G4F13K3m1K2F3ZSN2DO2OBJ2A4A4O5qK5vO5L3D1Zm1D3Il1O1N3O3YC3rC4iQA4O5L5uK4pJM1N5vC4vK4vB4O5oC4B4xlhI4L5K5oF3N5rpG3owrM5A3B4wN5C13wtK5K4M1" class="Sponsor_Content" >Vonage DigitalVoice...The BROADBAND Phone Company </a></td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td colspan="2"><img src="/images/spacer.gif" width="1" height="1" /></td><td valign="top" align="left" class="Sponsor_Content">Vonage is a digital phone service that replaces your current phone company, offering unlimited local and long distance calling for $39.99 per month</td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td colspan="3"><img src="/images/spacer.gif" width="1" height="5" /></td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td align="left" valign="middle"><img src="/images/pcm_arrow_blugray.gif" width="5" height="8" /></td><td><img src="/images/spacer.gif" width="7" height="1" /></td><td align="left" valign="middle"><a href="http://links.industrybrains.com/?QFF1BxH3F3NZA1D3m1D2YGF1DCBN2FiK2CE2I3RVM2F3HB4sO5uK5I3M1NprC4upvhuE3N5D4luL3ppnG13owK5nN5F3urqG13tK5trtqI4F4M1" class="Sponsor_Content" >NetSupport Manager PC Remote Control </a></td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td colspan="2"><img src="/images/spacer.gif" width="1" height="1" /></td><td valign="top" align="left" class="Sponsor_Content">Perform remote support and management on multiple systems simultaneously over a LAN, WAN and the Internet with this PC remote control software. Provides speedy, secure remote PC access, dynamic inventory, automated scripting and more.</td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td colspan="3"><img src="/images/spacer.gif" width="1" height="5" /></td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td align="left" valign="middle"><img src="/images/pcm_arrow_blugray.gif" width="5" height="8" /></td><td><img src="/images/spacer.gif" width="7" height="1" /></td><td align="left" valign="middle"><a href="http://links.industrybrains.com/?QFF1BxH3F3NZA1D3GI2LXE2Wk1O1M2GH2K2NC2FXVJ2EG3TJ2l1lJDE1PG3gG2QH2WM5K5ZBHBFSN2B2M5nH2WH2HG3UCC1D4A1n1xtN5yB3l1l1M3mI2PG4K5F3rI4C3N2n1WC13l1QG4G2EyC1NG2SVN4N2YGKrA4vL5E3zINuN5vN5O5iB4N5J3G4J3usiuI3zK5H3C4kwN5J3L5L5O5K4C4N5B4C4B4G13B4G13O" class="Sponsor_Content" >Free SSL VPN Security White Paper from Whale </a></td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td colspan="2"><img src="/images/spacer.gif" width="1" height="1" /></td><td valign="top" align="left" class="Sponsor_Content">Accessing email and applications from Internet kiosks can leave behind ?footprints,? compromising confidential information. Learn about the many risks and how enterprises prevent them, so users can securely access email, files and apps from anywhere.</td></tr><tr xmlns:ibn="urn:industrybrains.com:linkserver"><td colspan="3"><img src="/images/spacer.gif" width="1" height="5" /></td></tr>
					
						<tr>
							<td colspan="2"><img src="/images/pcm_spacer.gif" width="3" height="1"></td>
							<td align="left" valign="top">
								<a href="http://www.industrybrains.com/ziffdavis" class="Sponsor_Content">Get your product or service listed here.</a>
							</td>
						</tr>
					</table>
					
				</td>
				<td><img src="/images/pcm_spacer.gif" width="3" height="1"></td>	
			</tr>
			<tr>
				<td colspan="3"><img src="/images/pcm_spacer.gif" width="1" height="5"></td>
			</tr>
		</table>
		
		</td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3" width="100%"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
</table>

<!------------------- END MarketPlace ------------------------>

			<font style="FONT-SIZE:5px;"><br></font>
			
<!-- Microsites -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">

	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
	
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3"><img src="/images/pcm_spacer.gif" height="3" width="5"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td width="100%">

	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="Sponsor_BG">
		<tr>
			<td colspan="2"><img src="/images/pcm_spacer.gif" width="1" height="3"></td>
		</tr>
		<tr align="left" valign="top">
			<td><img src="/images/pcm_spacer.gif" width="3" height="1"></td>	
			<td align="left" valign="top">	
				<img src="/images/pcm_ad_gr.gif" width="75" height="9"><br>
				<img src="/images/pcm_intel_header.gif" width="101" height="11">
			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/images/pcm_spacer.gif" width="1" height="3"></td>
		</tr>
		<tr>
			<td><img src="/images/pcm_spacer.gif" width="3" height="1"></td>
			<td align="left" valign="top">	
				<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;5945595;5913751;y?http://www.ziffdavisinternet.com/sponsors/dell4me" class="Sponsor_Content">
Shop Now! - Dell Home Solutions Center</a>
<br>

<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;4611625;7008725;f?http://www.buympc.com/specialized/intel/910i/" class="Sponsor_Content">
Build your custom desktop at MPC (MicronPC)!</a>
<br>

			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/images/pcm_spacer.gif" width="1" height="5"></td>
		</tr>
	</table>
	
		</td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3" width="100%"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
</table>

<!-- Microsites End-->
			
			<font style="FONT-SIZE:5px;"><br></font>
<!-- Vignette V/5 Thu Sep 04 00:15:50 2003 -->
<!--WEB 3--><table border="0" cellpadding="0" cellspacing="0" width="100%">

	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
	
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3"><img src="/images/pcm_spacer.gif" height="3" width="5"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td width="100%">

	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="Sponsor_BG">
		<tr>
			<td colspan="2"><img src="/images/pcm_spacer.gif" width="1" height="3"></td>
		</tr>
		<tr align="left" valign="top">
			<td><img src="/images/pcm_spacer.gif" width="3" height="1"></td>	
			<td align="left" valign="top">	
				<img src="/images/pcm_sponsors_header.gif" width="62" height="11">
			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/images/pcm_spacer.gif" width="1" height="3"></td>
		</tr>
		<tr>
			<td><img src="/images/pcm_spacer.gif" width="3" height="1"></td>
			<td align="left" valign="top">	
				<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;5945593;5913751;w?http://landingstrip.dell.com/landingstrip/ls.asp?CID=3514&LID=72106&DGC=DC&DGStor=DHS&DGSite=Lowe-ZIF&Conum=19&K=6VP03&DURL=http://www.dell.com/us/en/dhs/topics/segtopic_jmp_special50.htm?order%5Fcode%3DT1D32%26keycode%3D6VP03%26DGVCode%3DDC



" class="Sponsor_Content">Free CD-RW or DVD Upgrade. Click for details. 
  </a>
<br>


<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;5930539;5913751;q?http://landingstrip.dell.com/landingstrip/ls.asp?CID=3603&LID=69027&DGC=BA&DGStor=BSD&DGSite=PCMag&DGU=%M&DURL=http://www.dell.com/us/en/bsd/default.htm?mc%3D%25M%26DGVCode%3DBA

" class="Sponsor_Content">Find savings on small business solutions at Dell.
  </a>
<br>






<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;6089854;5913751;w?http://www.fujitsupc.com/" class="Sponsor_Content">
Get Free Shipping on all products at Fujitsupc.com</a>
<br>







<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;6089701;5913751;n?http://www.acer.com/us" class="Sponsor_Content">
Rebates on award-winning Acer&#153; 800 notebooks. 
 </a>
<br>






<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;5780529;5913751;s?http://www.shop.kingston.com/07promo/default.asp?bannersource=ettech
" class="Sponsor_Content">
$29.99 for Kingston 128MB USB Flash Memory
After Rebate, Plus Free Shipping!

 </a><br>





<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/click;h=v2|329E|0|0|%2a|z;5705326;0-0;0;8148396;31-1|1;2923466|2926113|1;;%3fhttp://www.microsoft.com/windowsserver2003/evaluation/trial/default.mspx" class="Sponsor_Content">
Try new Microsoft&#174; Windows&#174; Server 2003 free. 

 </a>
<br>

<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;5725117;5913751;k?http://click.atdmt.com/AVE/go/zffdvbre01100027ave/direct/01/%n" class="Sponsor_Content">
BestBuy.com, free shipping on everything.

 </a>
<br>



<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;5882443;5913751;q?http://www.usa.canon.com/html/conCprCatProdIndex.jsp?minisite=10000&section=10199" class="Sponsor_Content">
Canon's full line of photo printers now available.


 </a>
<br>




<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;5961482;5913751;r?http://itzone.ziffdavis.com" class="Sponsor_Content">
Click here for the Ziff Davis IT Zone sponsored by HP
 </a>
<br>




<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;6055472;8331464;j?http://xeroxphaser.com/18166" class="Sponsor_Content">
WIN a 50" HDTV or XEROX COLOR PRINTER!
 </a>
<br>



<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;6118973;5913751;r?http://www.executive.com/diskeeper/diskeeper.asp?ad=pmoldk9" class="Sponsor_Content">
FREE performance boost. Find out what you're MISSING.
 </a>
<br>




<img src="/images/pcm_arrow_blugray.gif" width="5" height="7" >&nbsp;
<A HREF="http://ad.doubleclick.net/clk;6124366;5913751;k?https://home.ironspeed.com/10088/Custom/HtmlDialogs/GetStarted.asp?id=6130075" class="Sponsor_Content">
FREE Eval: New Code Generator: No hand-coding ASPX/ SQL
 </a>
<br>

			</td>
		</tr>
		<tr>
			<td colspan="2"><img src="/images/pcm_spacer.gif" width="1" height="5"></td>
		</tr>
	</table>
	
		</td>
		<td><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
	</tr>
	<tr>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td colspan="3" width="100%"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
		<td class="BorderBG"><img src="/images/pcm_spacer.gif" height="3" width="1"></td>
	</tr>
	<tr><td colspan="5" class="BorderBG"><img src="/images/pcm_spacer.gif" height="1" width="1"></td></tr>
</table>
	
			<br>
		</td>

	
			<td width="5">
				<img src="/images/pcm_spacer.gif" width="5" height="1">
			</td>
			<td valign="top" align="right">
				<span class="bgcolor12"><img src="/images/pcm_spacer.gif" width="10" height="5"></span><br>
				<!-- Vignette V/5 Thu Sep 04 00:15:51 2003 -->
<!--WEB 3-->
 
<!-- END TABLE OF CONTENTS -->

	<table width="145" cellpadding="0" cellspacing="0" border="0" class="Module_BG">
		<form name="form1" action="/prospero_search_results" method="GET">
		<tr>
			<td colspan="2" class="Module_Border" width="4"><img src="/images/pcm_spacer.gif" height="18" width="4"></td>
			<td align="left" valign="center" class="Module_Border" width="137">
				discussions
			</td>
			<td colspan="2" class="Module_Border" width="4"><img src="/images/pcm_spacer.gif" height="18" width="4"></td>
		</tr>
		<tr>
			<td class="Module_Border" width="1"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
			<td width="3"><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
			<td align="left" valign="top" class="Module_Content">
				<span class="dot"><br></span>
			<!-- Vignette V/5 Thu Sep 04 00:15:51 2003 -->
<!--WEB 3--><span class="Module_Content"><b>Recent Discussions</b></span><br>
				  <span class=dot><br></span>&#149;&nbsp;<a href="http://login.discuss.ziffdavisinternet.com/dir-login/index.asp?webtag=pcmag&lgnF=y&lgnDST=http%3A%2F%2Fdiscuss%2Epcmag%2Ecom%2Fpcmag%2Fmessages%2F%3Fmsg%3D28039" class="Module_Content">Patent Riots of 2003</a><br><span class=dot><br></span>&#149;&nbsp;<a href="http://login.discuss.ziffdavisinternet.com/dir-login/index.asp?webtag=pcmag&lgnF=y&lgnDST=http%3A%2F%2Fdiscuss%2Epcmag%2Ecom%2Fpcmag%2Fmessages%2F%3Fmsg%3D27814" class="Module_Content">DivX Reloaded</a><br><span class=dot><br></span>&#149;&nbsp;<a href="http://login.discuss.ziffdavisinternet.com/dir-login/index.asp?webtag=pcmag&lgnF=y&lgnDST=http%3A%2F%2Fdiscuss%2Epcmag%2Ecom%2Fpcmag%2Fmessages%2F%3Fmsg%3D23792" class="Module_Content">In your opinion, what is the best Win...</a><br><span class=dot><br></span>&#149;&nbsp;<a href="http://login.discuss.ziffdavisinternet.com/dir-login/index.asp?webtag=pcmag&lgnF=y&lgnDST=http%3A%2F%2Fdiscuss%2Epcmag%2Ecom%2Fpcmag%2Fmessages%2F%3Fmsg%3D28071" class="Module_Content">Bringing Old Negatives into the Digit...</a><br><span class=dot><br></span>&#149;&nbsp;<a href="http://login.discuss.ziffdavisinternet.com/dir-login/index.asp?webtag=pcmag&lgnF=y&lgnDST=http%3A%2F%2Fdiscuss%2Epcmag%2Ecom%2Fpcmag%2Fmessages%2F%3Fmsg%3D27813" class="Module_Content">The Comic Book Connection</a><br>
				  <br><a href="http://discuss.pcmag.com/n/mb/compose.asp?webtag=pcmag" class="Module_Content">Start a Discussion &gt;</a><br><br>
				  				
				
				<b>Find a Topic:</b><br>
				<input type="text" size="11" name="qu" style="font-family:monospace">
				<input type="image" src="/images/pcm_arrow_go.gif" width="13" height="13" border="0" align="absmiddle">
				<input size="0" type=hidden value="pcmag" name=webtag>				
			</td>
			<td width="3"><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
			<td class="Module_Border" width="1"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		</tr>	
		<tr>
			<td class="Module_Border" width="1"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
			<td width="3"><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
			<td align="center" valign="bottom" class="Module_Content">
				<a href="http://discuss.pcmag.com/pcmag" class="Module_Content">View All &gt;</a>
				<span class="dot"><br><br></span>
			</td>
			<td width="3"><img src="/images/pcm_spacer.gif" height="1" width="3"></td>
			<td class="Module_Border" width="1"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		</tr>		
		<tr>
			<td colspan="5" class="Module_Border"><img src="/images/pcm_spacer.gif" height="1" width="1"></td>
		</tr>		
		</form>
	</table>

					<br>
    
					<br><table width="145" cellpadding="1" cellspacing="0" border="0" class="AD_BG">
  <tr>
     <td align="center"><img src="/images/pcm_advertisement.gif" border="0"></td>
  </tr>
  <tr>
     <td align="left" class="Premium_Partners">&nbsp;<b>Partner Services:</b></td>
  </tr>
  <tr>
<td align="left">




			&nbsp;<img src="/images/pcm_arrow_ltblue.gif" border="0">
			<A HREF="http://ad.doubleclick.net/clk;5946943;5913751;w?http://landingstrip.dell.com/landingstrip/ls.asp?CID=3603&LID=68480&DGC=BA&DGStor=BSD&DGSite=PCMag&DGU=%M&DURL=http://www.dell.com/us/en/bsd/promos/allpromos.htm?mc%3D%25M%26DGVCode%3DBA
" class="Premium_Partners">Dell Business Systems</A><br>







			&nbsp;<img src="/images/pcm_arrow_ltblue.gif" border="0">
			<A HREF="http://ad.doubleclick.net/clk;5945594;5913751;x?http://www.ziffdavisinternet.com/sponsors/dell4me
" class="Premium_Partners">Dell Home Systems</A><br>





			&nbsp;<img src="/images/pcm_arrow_ltblue.gif" border="0">
			<A HREF="http://ad.doubleclick.net/clk;4611744;7008725;h?http://www.buympc.com" class="Premium_Partners">MPC (MicronPC)</A><br>

			
</td>  
</tr>
</table>
<br><br>
			</td>			


	</tr>
</table>
<!-- begin footer -->
<!--  FOOTER -->
<table border="0" cellpadding="0" cellspacing="0" width="780">
	<tr bgcolor="#4080BF" align="left" valign="middle">
		<td width="125">
			<img src="/images/pcm_footer_pcmag.gif" width="86" height="26"></td>
		<td width="655">
			<p class="Footer_Pub">
				<a href="https://secure.cnchost.com/www.zmcirc.com/cgi/custsvc/customer.cgi?id=login&icode=ibmp" class="Footer_Pub">Customer Service</a> | 
				<a href="http://www.pcmag.com/article2/0,4149,17,00.asp" class="Footer_Pub">Contact Us</a> | 
				<a href="http://www.pcmag.com/article2/0,4149,18,00.asp" class="Footer_Pub">About</a> | 
				<a href="http://www.pcmagmedia.com/index.html" class="Footer_Pub">Advertise</a> | 
				
<!-- start ziffsection //-->
<a href="http://www.pcmag.com/category2/0,4148,7120,00.asp" class="Footer_Pub">Ad Index</a>
<!-- end ziffsection //-->

			</p>
		</td>
	</tr>
	<tr align="left" valign="top" bgcolor="#ffffff">
		<td>
			<img src="/images/pcm_footer_ziffdavis.gif" width="108" height="21"></td>
		<td>
			<p class="Footer_Text" style="margin-top:5">
			<a href="http://www.ziffdavis.com/" class="Footer_ZD">Home</a> |
			<a href="http://www.ziffdavis.com/about/index.asp?page=contactus" class="Footer_ZD">Contact Us</a> |
			<a href="http://mediakits.ziffdavis.com/" class="Footer_ZD">Advertise</a> |
			<a href="http://www.circulation.com/" class="Footer_ZD">Magazine Subscriptions</a> |
			<a href="http://www.pcmag.com/newsletter_manage/0,3020,,00.asp?" class="Footer_ZD">Newsletters</a> |
			<a href="http://www.pcmag.com/article2/0,4149,816548,00.asp?kc=PCZD10303TTX1B0000570" class="Footer_ZD">RSS Feeds</a> |
			<a href="http://pcmag.bitpipe.com/?src=integb_footer" class="Footer_ZD">White Papers</a> |
			<a href="http://pcmag.pricegrabber.com/index.php?mode=pcm_footer030303" class="Footer_ZD">Tech Shop</a>
			</p>
			
			<p class="Footer_Text">
			<a href="http://www.baselinemag.com/default/0,3660,,00.asp?kc=BAZD01101TTX1B0000450" class="Footer_ZD">Baseline</a> |
			<a href="http://www.cioinsight.com/default/0,3660,,00.asp?kc=CTZD10107TTX1B0000271" class="Footer_ZD">CIO Insight</a> |
			<a href="http://www.computergaming.com/?kc=zdf" class="Footer_ZD">Computer Gaming World</a> |
			<a href="http://www.egmmag.com/?kc=zdf" class="Footer_ZD">Electronic Gaming Monthly</a> |
			<a href="http://www.webseminarslive.com/?kc=ZDGNAVFTR" class="Footer_ZD">eSeminars</a> |
			<a href="http://www.eweek.com/default/0,3660,,00.asp?kc=EWZD10107TTX1B0000273" class="Footer_ZD">eWEEK</a> | 
			<a href="http://www.extremetech.com/default/0,3398,,00.asp?kc=ETZD10107TTX1B0000272" class="Footer_ZD">ExtremeTech</a> |
			<a href="http://www.gamenowmag.com/?kc=zdf" class="Footer_ZD">GameNOW</a> |
			<a href="http://www.microsoft-watch.com/?kc=MWZD10111TTX1B0000538" class="Footer_ZD">Microsoft Watch</a> |
			<a href="http://www.playstationmagazine.com/?kc=zdf" class="Footer_ZD">Official US PlayStation Magazine</a> | 
			PC Magazine
			</p>
			
			<p class="Footer_Text">Supersites: 
			<a href="http://security.ziffdavis.com/?kc=SCZD10303TTX1B0000566" class="Footer_ZD">Security</a> | 

<a href="http://www.pcmag.com/category2/0,4148,13806,00.asp?kc=PCZD10107TTX1B000058" class="Footer_ZD">Small Business</a> | 


			<a href="http://storage.ziffdavis.com/?kc=SSZD10111TTX1B0000539" class="Footer_ZD">Storage</a> | 
			<a href="http://wireless.ziffdavis.com/?kc=WLZD10303TTX1B0000567" class="Footer_ZD">Wireless</a>
			</p>
			
			<p class="Footer_Text">
			<a href="http://www.ziffdavis.com/terms/index.asp?page=privacypolicy" class="Footer_ZD">Privacy Policy</a> | 
			<a href="http://www.ziffdavis.com/terms/index.asp?page=termsofservice" class="Footer_ZD">Terms of Use</a><br>
			Copyright &copy; 1996-2003 Ziff Davis Media Inc. All Rights Reserved.
			PC Magazine is a registered trademark of Ziff Davis Publishing Holdings Inc.
			Reproduction in whole or in part in any form or medium without express written permission of Ziff Davis Media Inc. is prohibited.
			For reprint information: <a href="http://www.ziffdavis.com/about/index.asp?page=contactus" class="Footer_ZD">click here</a>. 
			</p>
		</td>
	</tr>
</table>
<!-- end footer -->

<!-- ************* END MAIN TABLE ***********-->

<!-- START RedMeasure V4 - CGI v1.1 Revision: 1.6 -->
<!-- COPYRIGHT 2000 Red Sheriff Limited --> 
<div class="redsheriff" style="display:none">

<script language="JavaScript"><!--
var pCid="us_us-ziffdavis_0-pcm-1-41-Opinions_>_John_C._Dvorak-O";
var pUrl="http://www.pcmag.com/article2/0,4149,1236389,00.asp?rsDis=Patent_Riots_of_2003-Page001-58679";
var w0=1;
var refR=escape(document.referrer);
if (refR.length>=252) refR=refR.substring(0,252)+"...";
//--></script>
<script language="JavaScript1.1"><!--
var w0=0;
//--></script>
<script language="JavaScript1.1" src="http://server-us.imrworldwide.com/c1.js">
</script>
<script language="JavaScript"><!--
if(w0){
	document.write('<img src="http://server-us.imrworldwide.com/cgi-bin/count?ref='+
		refR+'&cid='+pCid+'" width=1 height=1>');
}
document.write("<COMMENT>");
//-->
</script>
<noscript>
<img src="http://server-us.imrworldwide.com/cgi-bin/count?cid=us_us-ziffdavis_0-pcm-1-41-Opinions_>_John_C._Dvorak-O&url=http://www.pcmag.com/article2/0,4149,1236389,00.asp?rsDis=Patent_Riots_of_2003-Page001-58679" width=1 height=1>
</noscript>
</COMMENT>


<!-- START RedSheriff Customer Intelligence -->
<!-- COPYRIGHT 2001 Red Sheriff Limited -->
<img src=http://server-us.imrworldwide.com/cgi-bin/count?cid=us_us-zdmkeycode_0 width=1 height=1>
<!-- END RedSheriff Customer Intelligence -->

</div>
<!-- END RedMeasure V4 -->


</center>

</body>


</html>
<!-- CDA R4.707 -->
<!--OID info a=58679 s=1500 -->