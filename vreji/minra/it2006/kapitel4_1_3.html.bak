<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>IT2006</title>
</head>
<body>
<img src="bmbf.gif">
<font face=arial size=2>
<BR>


<A NAME="top">&nbsp</a>
<H3 align="center">IT-Forschung 2006</H3>
<H4 align="center">F�rderprogramm Informations-
und Kommunikationstechnik</H4>


<B>4.1.3. Chipsysteme und Entwurfsmethodik</B>
<P>
<U><B>Ausgangslage</B></U><BR>
Die Chipsysteme sind die eigentlichen Produkte der Halbleiterindustrie. Mit ihnen erzielt die Halbleiterindustrie ihre Ums�tze und Verkaufserl�se. An ihnen werden im globalen Wettbewerb einerseits die Produktivit�t und Effektivit�t eines Halbleiterstandortes gemessen, andererseits k�nnen sich die Anwender mit ihren elektronischen Systemen von der Konkurrenz abheben. Chipsysteme wirken somit sowohl auf die System- als auch auf die Halbleiterindustrie.
<P>
Bereits heute geh�rt der Chipentwurf zu einem der am meisten die Entwicklung hemmenden Flaschenh�lse und Roadblocker. Deshalb sind hier einerseits gro�e Anstrengungen notwendig, andererseits auch gro�e Chancen vorhanden. Ein bedeutendes Risiko liegt  in der Erkennung der richtigen Entwicklungstrends: man darf praktisch keinen Trend unbeachtet lassen. Hier kann staatliche F�rderung f�r FuE in hohem Ma�e den Standort  unterst�tzen. 
<P>
Europa hat f�r diese anwendungsgetriebene Entwicklung eine sehr gute Ausgangsposition. Die deutsche Industrie ist f�hrend im Bereich der Systemarchitekturen (System on Chip), besonders in den Bereichen Telekommunikation, Chipcard und Automobil. Schw�chen zeigen sich noch in der schnellen Umsetzung von Ideen in Produkte. Ursache sind oft  fehlende Werkzeuge, insbesondere f�r den automatisierten Chipentwurf. Um die f�hrende Position auf den genannten Gebieten zu halten, neue Gebiete als Erster zu erschlie�en und in weiteren Bereichen die Spitze zu erobern, sind zwei F�rderans�tze notwendig:
<UL>
<LI>Steigerung der Entwurfsproduktivit�t und deutliche Verbesserung der Designf�higkeiten durch Automatisierung,
<LI>Entwicklung neuer Ans�tze in der Gestaltung von Chipsystemen.
<</UL>
Insgesamt sind die Voraussetzungen zu schaffen, ganze Systeme einschlie�lich ihrer Sensoren, Aktoren und Anzeigen mit ihren unterschiedlichsten technischen Facetten auf einem Chip integrieren zu k�nnen. Diese neuen und preisg�nstigen "Superchips" schaffen letztlich die Voraussetzung f�r eine ubiquit�re Verbreitung der Elektronik.
<P>
<U><B>Handlungsbedarf und Ziele</B></U><BR>
Mit der Entwicklung der technologischen M�glichkeiten ergeben sich neue Herausforderungen an die Entwicklung von Schaltkreisen, die sowohl technologisch als auch systemtechnisch bedingt sind: 
<UL>
<LI>Mit der Strukturverkleinerung und der Gr��enzunahme der Chips �berschreitet die Transistorzahl pro Chip die Milliardengrenze,
<LI>die Frequenz steigt in Bereiche, die eine Ber�cksichtigung der Wellenausbreitung der Signale verlangen,
<LI>das Geh�use wird zum wesentlichen Bestandteil und beeinflusst Funktion und Parameter,
<LI>die Produktionszyklen im Systembereich verk�rzen sich,
<LI>die Vielfalt der zu integrierenden Funktionen w�chst rapide,
<LI>immer neue Industriebereiche erkennen den Nutzen der Mikroelektronik f�r ihre Produkte und bringen neue Anforderungen in die Chipsysteme ein.
</UL>
Die F�rderung zu Chipsystemen und Entwurfsmethodik verfolgt auch hier nicht allein das Teilziel der technischen Qualifizierung. Folgende Teilziele treten hinzu:
<UL>
<LI>Unterst�tzung des Ausbaus der f�hrenden Stellung Europas bei Systems on Chip,
<LI>die Erschlie�ung des Innovationspotenzials neuer Systeme am Standort,
<LI>eine stabile Kooperation von Industrie und Wissenschaft bei der schnellen Umsetzung von Ideen und Ergebnissen aus der Grundlagenforschung,
<LI>eine B�ndelung und St�rkung der EDA- (Electronic Design Automation)-Aktivit�ten,
<LI>Beherrschung der Designf�higkeit bis hin zum Entwurf von Superchips.
</UL>
Mit den heutigen Entwurfsmethoden einen Superchip z.B. des Jahres 2008 entwerfen zu wollen, ist von der Komplexit�t und von den Kosten her vergleichbar mit der Entwicklung eines Gro�raumflugzeuges. Erschwerend wirken zus�tzlich die extrem kurzen Zykluszeiten in der Mikroelektronik. Das  Forschungspotenzial dieser Aufgabe ist enorm: die j�hrliche Steigerung der Produktivit�t im Entwurf  muss �ber Jahre hin bis zu 100 % betragen.
<P>
<U><B>Forschungsthemen</B></U><BR>
An die neuen Chipsysteme werden die vielf�ltigsten und teilweise widersprechende Anforderungen gestellt:<BR>
- Integration nichtelektronischer Komponenten,<BR>
- niedriger Energieverbrauch (low power),<BR>
- hohe Spannungen und Str�me f�r die Leistungselektronik,<BR>
- niedrige Spannungen und Str�me f�r die Informationselektronik (low voltage),<BR>
- Geh�use und Chip als Gesamtsystem betrachten,<BR>
- Echtzeit-Software beeinflusst die Chiparchitektur,<BR>
- hohe Frequenzen (Giga-Technik) und Bitratenverarbeitungsraten bis in den Terabereich (TeraBit-Systeme),<BR>
- drastische Erh�hung der Zuverl�ssigkeit.<BR>
Mit den bisherigen Entwurfsmethoden und der bisherigen internen Gestaltung der Chips sind die Herausforderungen des neuen Jahrzehnts nicht mehr beherrschbar. Der Schaltkreisentwurf muss sich zum  zuverl�ssigen, anwendungsorientierten, vom System zum Layout durchg�ngigen und hochgradig automatisierten Entwurf entwickeln. Es werden vollkommen neue, innovative Ans�tze gesucht, woraus sich folgende Aufgaben ergeben:
<UL>
<LI><B>Chiparchitektur und Schaltungstechnik</B> 
<UL>
<LI>neue digitale und analoge Chiparchitekturen zur effektiven Umsetzung innovativer Algorithmen der Signal- und Datenanalyse,
<LI>neue Schaltungstechniken zur Senkung des Energieverbrauchs und Erh�hung der Verarbeitungsgeschwindigkeit (u.a. high speed CMOS),
<LI>Sensoren, Aktoren und Schnittstellen zwischen optischer Signal�bertragung und elektronischer Signalverarbeitung zur Integration in geschlossene Systeme.
</UL>
<LI><B>Neue Chipfunktionen</B> 
<UL>
<LI>Systemplattformen zur Bereitstellung flexibel anpassbarer Macrobl�cke f�r verschieden Anwendungsszenarien sowie zur Standardisierung komplexer Systeme (u.a. Intellectual Properties),
<LI>Integration bisher getrennter Subsysteme f�r in die Zukunft weisende Anwendungen (z.B. Chipsysteme f�r die personengebundene I-centric-Netze),
<LI>Anzeigetechniken zur �berwindung des Widerspruchs zwischen kleinem Volumen und gro�em Bild (z.B. flache Bildschirme, Mikrolinsen).
</UL>
<LI><B>Erfassung und Handling der Chipkomplexit�t im Entwurf</B> 
<UL>
<LI>Entwicklung neuer Entwurfsmethoden und -schritte zur Handhabung der Giga-Komplexit�t mit ihren Milliarden von Transistoren, u.a. gilt es die hohe Zuverl�ssigkeit im Chipentwurf durch die Entwicklung  neuer und besserer  Formen der Analyse, Synthese, Verifikation und Test zu bewahren,
<LI>Applikationsspezifische Entwurfsprozesse, um unterschiedlichsten Anforderungsprofilen gerecht zu werden,
<LI>Vereinigung von elektronischer, optischer, mechanischer und fluider Spezifikation einschlie�lich der dazugeh�rigen Echtzeit-Software  zur gesamtheitlichen Betrachtung �bergeordneter Systeme unter der Einbeziehung der lokalen Umgebungseinfl�sse.
</UL>
<LI><B>Erschlie�ung neuer Technologien</B> 
<UL>
<LI>Modellbildung und neue Simulationsverfahren, wobei zunehmend neue physikalische Wirkmechanismen einbezogen werden m�ssen,
<LI>Einbeziehung und eventuell Nutzung parasit�rer Effekte in den Entwurfsfluss bzw. f�r den Chipentwurf.
</UL>
<LI><B>Abgestimmte firmen�bergreifende und internationale Zusammenarbeit</B> 
<UL>
<LI>bei Tools und Methodiken ist notwendig, um den Entwurf zu automatisieren und die Designf�higkeit erh�hen zu k�nnen.
</UL></UL>
<P>
<a href="#top">zum Seitenanfang</a>
</font>

</body>
</html>
