<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>IT2006</title>
</head>
<body>
<img src="bmbf.gif">
<font face=arial size=2>
<BR>


<A NAME="top">&nbsp</a>
<H3 align="center">IT-Forschung 2006</H3>
<H4 align="center">Förderprogramm Informations-
und Kommunikationstechnik</H4>


<B>4.2.1. Software Engineering</B>
<P>
<U><B>Ausgangslage</B></U><BR>
Software realisiert Funktionen in Produkten und unterstützt Dienstleistungen und betriebliche Prozesse aller Art. In nahezu allen Branchen ist die Kompetenz zur ingenieurmäßigen Entwicklung von zuverlässiger, adaptierbarer und dabei kostengünstiger Software zur entscheidenden Kernkompetenz geworden. In vielen technischen Bereichen ist das Software Engineering aufgrund der Gewichtsverschiebung von der Hardware hin zur Software auf dem besten Wege zur "Produktionstechnik des 21. Jahrhunderts" zu werden.
<P>
Die Chancen Deutschlands als führender‚ Produktionsstandort für Software in Primärbranchen der IT-Industrie (z.B. Softwarehäuser) und Sekundärbranchen (z.B. Maschinenbau-, Elektrotechnik-, Kraftfahrzeug-, Telekommunikationsindustrie, Banken und Versicherungen) sind sehr gut. Hier kann auf der Basis einer langen Ingenieurtradition eine hohe Anwendungskompetenz in den Sekundärbranchen mit einer führenden Produktionskompetenz zur Erstellung qualitativ hochwertiger, kundenangepasster Software-Speziallösungen und der besonderen Fähigkeit zum Systemdenken kombiniert werden. 
<P>
Mit Förderung durch das Bundesministerium für Bildung und Forschung wurden in der Vergangenheit beachtliche Erfolge erzielt, z.B. in der Entwicklung von CASE-Systemen. Die Kernaufgaben liegen zukünftig primär im Bereich "Embedded Systems". Hier wird es darauf ankommen, von Einzelsystemen zu vernetzten Systemen zu kommen und danach zu einem komplexen Netz aller IT-Module in einem System.
<P>
<U><B>Handlungsbedarf und Ziele</B></U><BR>
Zur nachhaltigen Positionierung Deutschlands als führender Anwendungssoftware-Standort bedarf es gezielter Anstrengung im Bereich der Forschung, aber auch einer Intensivierung forschungsnaher Qualifikation von Personal. Diese Anstrengungen müssen sowohl auf die Spitze wie auch auf die Breite gerichtet sein.
<P>
In der Spitze muss - aufbauend auf existierenden Forschungszentren - ein erheblicher Ausbau der wissenschaftlichen Infrastruktur im Bereich des Software Engineering an Hochschulen und Forschungseinrichtungen vorgenommen werden.
<P>
In der Breite muss die Umsetzung neuester Softwaretechniken in die Masse der deutschen Firmen (bisher setzen nur 30 % der Unternehmen systematische Prozesse bei der Softwareentwicklung ein) sowie die dauerhafte Bereitstellung einer genügenden Anzahl von Softwareentwicklern unterschiedlichster Qualifikation gesichert werden.
<P>
Es muss eine leistungsfähige Forschungsinfrastruktur geschaffen werden, auf welche die Industrie, insbesondere auch die große Anzahl von KMU, zugreifen kann.
<P>
Die Produktivität bei der Softwareentwicklung muss kurz- bis mittelfristig um eine Größenordnung gesteigert werden, bei gleichzeitiger Steigerung der Qualität der entwickelten Software. Dazu ist neben dem Paradigma der ingenieurmäßigen Softwareentwicklung ("Software Engineering"), das vorwiegend den Prozess der Leistungsgestaltung betrifft, das Paradigma der "Industriellen Softwareproduktion", welches auf die Leistungserbringung abzielt, zu forcieren.
<P>
Für die Sicherheit in der Softwareentwicklung und auch für die Sicherung von IT-Systemen müssen neue integrierte Lösungen erforscht und entwickelt werden.
<P>
Schon heute haben Probleme der Softwareentwicklung (Kostensteigerungen, ein nicht zu befriedigender Personalmangel, kostspielige Fehlentwicklungen) eine auf nationaler und internationaler Ebene besorgniserregende Dimension erreicht. Die in den nächsten Jahren erwartete Hardwareentwicklung (etwa eine um mehr als drei Größenordnungen gesteigerte Prozessorkomplexität) wird die Situation weiter verschärfen. Um das Ziel der kostengünstigen Erzeugung effizienter, robuster und flexibler Softwaresysteme zu erreichen, müssen auch grundlegende neue, z.B. bioanaloge Lösungswege erforscht werden.
<P>
<U><B>Forschungsthemen</B></U><BR>
Am Beispiel konkreter Fragestellungen aus der Anwendung/Technik sollen folgende FuE-Themen gefördert werden: 
<UL>
<LI><B>Korrektheit, Sicherheit und Zuverlässigkeit von Softwaresystemen (Safety)</B><BR>
Prozesse, Methoden und Werkzeuge zur Qualitätsverbesserung sicherheitskritischer Software und eingebetteter Systeme und Techniken der:
<UL>
<LI>Integration von Methoden der formalen Programmentwicklung (formale Spezifikation, Transformation und Verifikation, durchgängiges Referenzmodell),
<LI>Entwicklung verifizierbarer Anwendungssoftwarekomponenten,
<LI>System-/Softwareanforderungsanalyse (Requirements Engineering),
<LI>Entwicklung von echtzeitfähigen Softwaresystemen.
</UL>
<LI><B>IT-Sicherheit, Schutz (Security)</B>
<UL>
<LI>Innovative integrierte IT-Sicherheitssysteme (sichere Erstellung, sichere Installation, sichere Konfiguration sowie sicherer Betrieb von IT-Systemen; Persönlichkeitsschutz und Vertrauenswürdigkeit),
<LI>Sicherheit bei neuen IT-Methoden/Techniken wie Ubiquitous Computing.
</UL>
<LI><B>Produktivitätserhöhung mittels Komponentenorientierung und Wiederverwendung</B><BR>
Prozesse, Methoden und Werkzeuge zur Produktivitätserhöhung durch:
<UL>
<LI>Prozessgestaltung und Prozessmanagement zur Erbringung von Qualität und Produktivität,
<LI>Generative Programmierung,
<LI>Komponenten-basierte Softwareentwicklung auf der Basis langlebiger Architekturen,
<LI>Systematische Erstellung rationeller Software-Varianten (Produktlinienansätze),
<LI>Reengineering und Wartung von Altsoftware (Legacy Software),
<LI>Konfigurierbarkeit und Skalierbarkeit von Software in heterogenen Anwendungssystemen.
</UL>
<LI><B>Entwicklung von Softwaresystemen in (räumlich) verteilten Umgebungen</B><BR>
Entwicklung und Spezialisierung der Softwaretechnik für die räumlich verteilte Produktion von Software durch:
<UL>
<LI>Prozessgestaltung und Prozessmanagement; adaptive Modellierungs- und Spezifikationsverfahren,
<LI>Arbeitsteilige Entwicklungsmethoden (z.B.: Inspektionen), deren Kommunikationserfordernisse den Einsatz in verteilten Teams zulassen,
<LI>Methoden und Werkzeuge für die Unterstützung früher kreativer Phasen kooperativer Systementwicklung.
</UL>
<LI><B>Wissensmanagement in der Softwareentwicklung</B><BR>
Weitere Verbesserung der Kommunikation und des Wissensaustausches zwischen den Entwicklern durch:
<UL>
<LI>Methoden zur Wissensakquisition, -speicherung und zum -transfer in der Softwareentwicklung,
<LI>Methoden und Werkzeuge zum Knowledgemanagement in der Softwareentwicklung,
<LI>Aufbereitung und Repräsentation von Informationsinhalten (Contentware Engineering),
<LI>Lernende Systeme, Selbstadaptivität von Softwaresystemen.
</UL>
<LI><B>Empirische Erprobung von Prozessen, Techniken, Methoden und Werkzeugen auf ihre Eignung für unterschiedliche Anwendungsgebiete
<LI>Erprobung neuer Verfahren in der Softwareentwicklung</B>
<UL>
<LI>Adaptive Softwaresysteme, proaktive Software,
<LI>Aspektorientierte Softwareentwicklung, selbstorganisierende Softwaresysteme, Interoperable Software.
</UL>
<LI><B>Auf- und Ausbau von Kompetenznetzwerken zur Softwaretechnologie</B> 
</UL></UL>
<U><B>Wichtige Anwendungsgebiete</B></U>
<UL>
<LI>Mensch-Maschine-Schnittstellen, die auf die Bedürfnisse der Anwender zugeschnitten sind (Human Centered Engineering),
<LI>Heterogene Netzinfrastrukturen mit skalierbaren, adaptierbaren Leistungen/Interoperabilität (Network Engineering),
<LI>Computer-gestützte Modellierung, Simulation und Experimentieren als Ersatz für ‚Real-World'-Experimente (Virtuelle Softwareengineering-Labors),
<LI>Software in Produkten (Embedded Software, z.B. Automobil, Mobilfunk, Avionik, Haushaltstechnik, Maschinenbau, Robotik),
<LI>Software zur Unterstützung von betrieblichen Prozessen aller Art (z.B. Produktion, Workflow, Beschaffung),
<LI>Software zur Unterstützung von Dienstleistungen (z.B. Versicherungen, öffentliche Verwaltung, Gesundheitswesen, Medien, Beschaffung, Planung und Logistik),
<LI>Hochleistungsdatenverarbeitung und -übertragung großer Datenmengen (Data Engineering).
</UL>
<P>
<a href="#top">zum Seitenanfang</a>
</font>

</body>
</html>
