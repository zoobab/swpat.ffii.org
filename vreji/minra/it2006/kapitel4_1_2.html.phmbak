<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>IT2006</title>
</head>
<body>
<img src="bmbf.gif">
<font face=arial size=2>
<BR>


<A NAME="top">&nbsp</a>
<H3 align="center">IT-Forschung 2006</H3>
<H4 align="center">F�rderprogramm Informations-
und Kommunikationstechnik</H4>


<B>4.1.2. Neuartige Schaltungen und Bauelemente</B>
<P>
<U><B>Ausgangslage</B></U><BR> 
In der aktuellen ITRS-Roadmaps wird bez�glich der Verkleinerung der funktionsbestimmenden Strukturabmessungen eine wesentliche Beschleunigung gegen�ber Meilensteinen fr�herer Roadmap-Aktualisierungen festgestellt, w�hrend Speicherdichten eher langsamer wachsen. Diese Strukturverkleinerungen unter 50 nm werden bald zu ersten Bauelementen f�hren, welche �ber die klassische CMOS-Elektronik hinausgehen. Diese wird durch alternative Technologien f�r neue Funktionalit�t erg�nzt und erh�lt die F�higkeit quantenelektronische Effekte zu nutzen oder zu unterdr�cken. 
<P>
Aus den Sachverhalten der Roadmaps ist ebenfalls ableitbar, dass neben den wachsenden Anforderungen an Speichergr��e und Rechengeschwindigkeit der exponentiell steigende Bedarf an Frequenzen und Bandbreite zu einem weiteren Treiber kostenintensiver Forschungs- und Entwicklungsaufgaben wird, um den weitgefassten Anspr�chen - von h�chsten Frequenzen und Speicherdichten, unterschiedlichsten Speicheranforderungen usw. bis zu vergleichsweise hohen Leistungen, verbunden mit Forderungen an Energieeinsparung, Verlustminimierung und niedrigsten Kosten -  gerecht zu werden.
<P>
<U><B>Handlungsbedarf und Ziele</B></U><BR>
Die Forschungsf�rderung wendet sich hier vor allem folgenden Schwerpunkten zu:
<UL>
<LI>Superintegrierte Schaltkreise und Systeme der Silizium-Nanoelektronik,
<LI>Hochkomplexe Silizium-Schaltkreisstrukturen und -systeme f�r neue Anwendungsgebiete,
<LI>Magneto- und Spinelektronik,
<LI>Komponenten und Systeminnovation der Silizium-Leistungselektronik.
</UL>
Besondere Herausforderungen der Forschung sind 
<UL>
<LI>Basis- und Schaltungsstrukturen f�r neue Speichergenerationen bis in den 64 Gbit- Bereich mit hoher Informationserhaltungszeit und System-on-Chip- F�higkeit,
<LI>Si- H�chstfrequenz- Schaltkreise mit Arbeitsfrequenzen �ber 100 GHz,
<LI>Innovative Basis- und Schaltungsstrukturen f�r Informationsverarbeitungs- (Logik-) Schaltkreise h�chster Integrationsdichte und niedrigster Verlustleistung,
<LI>Realisierung h�chstintegrierter nichtfl�chtiger Speicher und Logikbauelemente,
<LI>Neuartige Konfigurationen f�r das Zusammenwachsen von Sensor-Sub- sowie leistungselektronischen Aktorsystemen mit der Nanoelektronik.
</UL>
Weitere Stichworte sind: Erh�hung der Transitfrequenz, Verringerung parasit�rer Effekte, Integration passiver Bauelemente, hohe Sperr-, geringe Durchlasswiderst�nde, Erh�hung der Spannungsfestigkeit, Verringerung der Leistungsaufnahme, Einbeziehung (besser Ber�cksichtigung) quantenelektronischer Effekte, Senkung des Leitbahneinflusses.
<P>
In j�ngster Zeit verst�rken sich z. B. Tendenzen, �ber die klassischen Anwendungsfelder der Siliziumelektronik - wie Computertechnik, Telekommunikation, Automatisierungstechnik - hinaus neue Felder zu erschlie�en, die bereits mittelfristig den Charakter von Massenm�rkten erlangen k�nnen. Hierzu z�hlen in nanoelektronische Schaltungen integrierte mikromechanische Si-Schalter sowie die direkte Anwendung von Si-Mikrochipkonfigurationen in der chemischen Reaktionstechnik, in der Biotechnologie oder in der Umweltanalytik. 
<P>
Auch hier ist die technologische Qualifizierung nur ein Teilziel der Forschungsf�rderung. Schon im Stadium gemeinsamen Forschens werden - vor allem auf europ�ischer Ebene - Allianzen entlang der Wertsch�pfungskette gef�rdert, damit
<UL>
<LI>Kooperationen von Bauelementeherstellern und Zulieferern etabliert werden, um einen raschen Wissenstransfer an m�glichst viele Umsetzer zu erm�glichen. Unter den Zulieferern sind viele KMU, welche durch die Verbundforschung einen effizienten Zugang zu neuen Technologien bekommen,
<LI>Kooperationen mit Anwendern aus anderen Branchen (Mikrotechnologie, Biotechnologie, Optische Technologien) disziplin�bergreifende Verflechtungen erm�glichen und rasch von der Nanoelektronik profitieren. 
</UL>
Forschungsthemen
<UL>
<LI><B>Silizium-Nanoelektronik</B><BR>
<UL>
<LI>Forschungen zu innovativen Speicherzellen, -konfigurationen und -schaltkreisen, die einen entscheidenden Einfluss (z.B. betr�chtliche Verringerung von Zugriffszeiten,, Nichtfl�chtigkeit (Flash-/EEPROM, ferroelektrische Speicher, SoC-F�higkeit u.a.) auf zuk�nftige Entwicklungen in der Nanoelektronik haben, einschlie�lich zugeh�riger, nanoelektronik-gebundener Materialforschungen (z.B. neue Materialien f�r Trench-/Stack-Kapazit�ten, f�r Wort-/Bitleitungen und Kontaktsysteme),<BR>
<LI>Forschungen zu neuartigen Interconnections (z.B. verlust�rmere Verbindungsleitungen), und Packaging (bestimmend f�r die Systemperformance; geeignet f�r sehr hohe Taktraten),<BR>
<LI>Forschungen zu neuen, integrationsf�higen Si-H�chstfrequenz-Strukturen mit neuen siliziumkompatiblen Materialien (z.B. Si/SiGe/SiGe:C)-HBT, SiGe-FET, -Resonanzphasentransistor,<BR>
<LI>Si-H�chstfrequenzschaltkreissysteme unter Einbeziehung neuartiger passiver Schaltungskomponenten (nichtplanare passive Komponenten (MEMS)) sowie ad�quatem Packaging (h�chstfrequenztauglich) f�r die Mobil- und Telekommunikation,<BR>
<LI>H�chstintegrationsf�hige Basisstrukturen (aktive Strukturen, neue Interconnections) f�r Logiksysteme mit dem Potenzial f�r eine System-on-Chip-Integration (Informationsspeicherung und  -verarbeitung mit verbesserter Integrationsf�higkeit, z.B. Mixanordnungen von Lateral- und Vertikalstrukturen),<BR>
<LI>angepasste Schaltungssysteme mit der besonderen Herausforderung eines geringen Energieverbrauches (z.B. neue SOI-Techniken).
</UL>
<LI><B>Nanoelektronische Bauelemente f�r niedrigsten Energiebedarf</B><BR>
<UL>
<LI>Forschungen zu integrationsf�higen Basisstrukturen und -konfigurationen im niedrigen Nanometerbereich mit null- und eindimensionalem Ladungstr�ger-Confinement: z. B. Single-(Few-)Elektron-Transistor, Quantendraht-Transistor, RTDs, Nanoleitungen und Nanor�hren sowie bistabile Molekularschalter,<BR>
<LI>Schaltungs- und Verkn�pfungssysteme dieser Grundstrukturen, insbesondere mit dem Ziel, eine h�chstparallele Informationsverarbeitung zu erm�glichen; Interfacegestaltung und -arten zwischen diesen Nanoschaltungen und der �u�eren Si-Mikroelektronik,<BR>
<LI>Forschungen zu siliziumkompatiblen, nanometrisch strukturierbaren Materialien (z.B. Nanocluster-FET-Konfigurationen) und Grenzfl�chen f�r die Realisierung neuartiger Basisstrukturen mit neuen elektronischen Funktionen.<BR>
</UL>
<LI><B>Magnetoelektronik</B><BR>
Die Magnetoelektronik ist eine neue Basistechnologie, welche ein gegen�ber anderen Nicht-Silizium-Technologien besonders vielversprechendes Potenzial hat, die Halbleiterelektronik mit neuer Funktionalit�t zu erg�nzen. <BR>
<UL>
<LI>Forschungen zu MRAM (Magnetic-Random-Access-Memory): Die Information d.h. das Bit der Speicherzelle wird durch die relative Magnetisierung der magnetischen Schichten dargestellt und nicht wie beim DRAM durch den Ladungszustand eines Kondensators. Der MRAM hat eine so hohe Speicherdichte wie der DRAM, er ist im Gegensatz zum DRAM nichtfl�chtig wie Flash-Speicher und schnell wie der SRAM. Erste MRAM Produkte werden im Jahr 2004 erwartet.<BR>
<LI>Forschungen zu Embedded Systems unter Einbeziehung magnetoelektronischer Bauelemente zum Aufbau einer nichtfl�chtigen Logik. Au�erdem k�nnen so Sensoren und Biochips sowie im Vergleich zu herk�mmlichen Optokopplern schnellere Magnetokoppler aufgebaut werden. Aufgrund der Nichtfl�chtigkeit sind diese Systeme von sehr geringem Leistungsverbrauch und aufgrund der magnetischen Materialien sehr robust. Es ergeben sich Chancen f�r eine integrierte Multifunktionselektronik besonders in tragbaren Systemen sowie in Robustheit erfordernden Umgebungen.<BR>
</UL>
<LI><B>Spintronik</B><BR>
Die bisherige Halbleiterelektronik nutzt nur die elektrische Ladung der Ladungstr�ger. Aktuelles Forschungsthema ist eine neuartige Elektronik die "Spintronik", welche neben der Ladung auch noch die Eigenrotation der Ladungstr�ger, den sogenannten Spin, zur Informationsdarstellung und -auswertung nutzt und damit neue M�glichkeiten bietet. Die Spintronik beruht auf der M�glichkeit der "Spininjektion" in Halbleitermaterialien, die erst 1999 entdeckt wurde. Durch die Spininjektion wird es m�glich, spinpolarisierte Str�me in Halbleitern zu erzeugen, die analog zu polarisiertem Licht in Form der Spinrichtung einen zus�tzlichen Freiheitsgrad aufweisen, der als zus�tzliche Eigenschaft genutzt werden kann. Obwohl aufgrund der Neuheit des Effekts noch keine konkreten Devices vorliegen, die auf diese neue Eigenschaft zur�ckgreifen, sind vielf�ltige Anwendungen denkbar:<BR> 
<UL>
<LI>mehrwertiges Logikkonzept auf Halbleiterbasis,<BR>
<LI>spintronische Optoelektronik-Bauelemente zur direkten Erzeugung bzw. Wandlung polarisierten Lichts,<BR>
<LI>Integration magnetoelektronischer Effekte in Halbleitersysteme,<BR>
<LI>Verwendung der vergleichsweise langzeitstabilen, koh�renten Quantennatur des spinpolarisierten Stromes f�r quantenlogische Operationen anzusehen.<BR>
</UL>
Die gegenw�rtig exploratorischen Forschungen zur Spintronik sollen bis 2005 die Grundlage schaffen, oben genannte theoretische Anwendungsoptionen in Ihrer tats�chlichen Tragf�higkeit abzusch�tzen.
<LI><B>Dreidimensionale Schaltkreiskonfigurationen und Basisstrukturen</B><BR>
<UL>
<LI>Forschungen zu neuartigen, integrationsf�higen vertikalen Transistorstrukturen mit geringsten Parasitics,
<LI>Vertikalintegration von Schaltkreis-Subsystemen (z.B. vertikale Systemintegration mit ged�nnten Chips) mit vertikalen Interconnections, um Volumendichte, Parallelisierung von Signalprozessen und dgl. wesentlich zu steigern und konventionelle Ans�tze zu �bertreffen.
</UL>
<LI><B>Bauelemente und Strukturen mit "embedded nonelectronic systems" und "nonelectronic interfaces"</B><BR>
<UL>
<LI>Forschungen zu nanoelektronischen Basisstrukturen mit integrierten Wandlern  f�r die Umwandlung nichtelektrischer in elektrische Gr��en (z. B. Direktwandlung elektrochemisches Potenzial in Steuerpotenzial f�r FET's),
<LI>vertikale Systemintegration von nanoelektronischen mit mikrooptischen und -opto-elektronischen, mikroakustischen, mikromagnetischen, mikromechanischen bzw. mikro-fluidischen Komponenten, z.B. durch "Silicon direct bonding" (SDB),
<LI>Forschungen zu systemangepassten, silizium- und wandlerkompatiblen Materialien.
</UL>
<LI><B>Innovative Aufbau- und Verbindungstechniken (z. B. f�r smart cards und smart labels)</B><BR>
<UL>
<LI>Forschungen zu innovativen L�sungen f�r in Laminate/Kunststoffe eingebettete Chips und Chipsysteme f�r Massenanwendungen (z.B. "intelligente", funkabfragbare Produkt-Labels, "Body Area Networks"),
<LI>neue L�sungen von kosteng�nstigen Aufbau- und Verbindungstechniken f�r SoP-(MCM) Systeme f�r Allgemein-, Hochtemperatur- und H�chstfrequenzanwendungen,
<LI>M�gliche neuartige optische Inter- und Intra-Chip-Verbindungen werden gezielt untersucht.
</UL>
<LI><B>Innovative Si-Halbleiter-Leistungselektronik</B><BR> 
<UL>
<LI>mit neuen Topologien, verbesserten Leistungsparametern (z.B. hochtemperaturgeeignet), Kopplungen mit Niedrigstleistungs-Signalverarbeitungs- und -Speicherelektronik, minimierten Verlusten (z.B. Nutzung von nanometrischen Substrukturen) und hoher Eigensicherheit,
<LI>Forschungen zu ad�quaten Packaging-/Bondmaterialien (z.B. hochtemperaturfeste Packagingmaterialien, Kupfer-Bondsysteme).
</UL>
<LI><B>Mikroelektronische Bauelemente, die auf neuen Basismaterialien beruhen</B><BR> 
<UL>
<LI>z.B. Polymere, amorphe/poly-/mikrokristalline Si- oder Si-Materialkomposite insofern wie f�r Massenmarktanwendungen geeignet sind.
</UL></UL>
<P>
<a href="#top">zum Seitenanfang</a>
</font>

</body>
</html>
