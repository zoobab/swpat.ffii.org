<html>
<head>
<meta name="keywords" content="�dito, �ditorial, langue de bois, informaticiens, professionnels de l'informatique, informatique, Techref, Tech Net, TechNet, IT Executives, IT Professionals, Strat�gie, Planning , Evaluation , D�ploiement, Help Desk &amp;, Maintenance , Total Cost of Ownership , TCO, commerce, Intranet, Internet , An 2000, An2000, S�minaires, articles, informaticiens, Microsoft">
<meta name="description" content="Technet - l'information cl� pour les sp�cialistes de l�informatique en entreprise.">
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<meta name="ms.locale" content="FR">
<title>Dans Innovation il y a Ovation, mais bon� Edito TechNet - Microsoft France</title>
<!--ST_START-->
<LINK REL="stylesheet" TYPE="text/css" HREF="/library/shared/webparts/netscape.css"/>
<LINK REL="stylesheet" TYPE="text/css" HREF="/library/shared/webparts/down.css"/>
<LINK REL="stylesheet" TYPE="text/css" HREF="/library/shared/webparts/default.css" />
<LINK REL="stylesheet" type="text/css" href="/library/flyoutmenu/default.css" />
<LINK REL="stylesheet" TYPE="text/css" HREF="/library/shared/webparts/netscape.css" />
<LINK REL="stylesheet" type="text/css" HREF="/france/ms_ns.css"/>
<LINK REL="stylesheet" type="text/css" HREF="/france/msdn/css/dev_ns.css"/>

<!--ST_STOP-->
</head>

<BODY TOPMARGIN=0 LEFTMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 BGCOLOR=#FFFFFF>

<!-- Start: ToolBar V2.0-->
	<script language="JavaScript" src="/library/toolbar/toolbar.js"></script>
<DIV ID="TBDownLevelDiv">
<TABLE WIDTH="100%" CELLPADDING=0 CELLSPACING=0 BORDER=0 BGCOLOR="#003399">
<TR>
	<TD VALIGN='top' HEIGHT=60 ROWSPAN=2><A href="/france/technet/default.asp" target=_top><IMG alt="Microsoft TechNet" border=0 height=60 src="/france/technet/images/banner.gif" width=250 ></A></TD>
	<TD VALIGN='top' HEIGHT=20 ALIGN='right' width=100%><IMG alt="" border=0 height=20 src="/library/toolbar/images/curve.gif" width=18></TD>
	<TD BGCOLOR='#000000' HEIGHT=20 VALIGN='center' ALIGN='right' NOWRAP COLSPAN=2>
	<FONT COLOR='#ffffff' FACE="Verdana, Arial" SIZE=1>
		<B>
			&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/redir/m/produits.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>Produits</FONT></A>&nbsp;&nbsp;<FONT COLOR='#ffffff'>|</FONT>
			&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/redir/m/support.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>Support</FONT></A>&nbsp;&nbsp;<FONT COLOR='#ffffff'>|</FONT>
			&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/redir/m/search.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>Recherche</FONT></A>&nbsp;&nbsp;<FONT COLOR='#ffffff'>|</FONT>
			&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/redir/m/gaccueil.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>Microsoft.com France</FONT></A>&nbsp;&nbsp;
		</B>
	</FONT></TD>
</TR>
<TR>
	<TD VALIGN='TOP' HEIGHT=40 WIDTH=19><IMG SRC='/library/images/gifs/homepage/1ptrans.gif' WIDTH=19 HEIGHT=40 ALT='' BORDER=0></TD>
	<TD VALIGN='TOP' HEIGHT=40 ALIGN='RIGHT' NOWRAP COLSPAN=2><A HREF='/france/' TARGET='_top'><IMG SRC='/library/toolbar/images/mslogo_003399.gif' WIDTH=112 HEIGHT=40 ALT='Microsoft France' BORDER=0></A></TD>
</TR>
<TR>
	<TD BGCOLOR='#003399' HEIGHT=20 VALIGN='center' NOWRAP COLSPAN=4>
		<TABLE WIDTH="100%" CELLPADDING=0 CELLSPACING=0 BORDER=0>
			<tr><td>
					<FONT COLOR='#ffffff' FACE="Verdana, Arial" SIZE=1><B>
					&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/France/redir/m/accueil.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>Accueil</FONT></A>&nbsp;&nbsp;<FONT COLOR='#ffffff'>|</FONT>
					&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/redir/m/pourvous.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>Pour vous</FONT></A>&nbsp;&nbsp;<FONT COLOR='#ffffff'>|</FONT>
					&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/redir/m/formation.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>Formations</FONT></A>&nbsp;&nbsp;<FONT COLOR='#ffffff'>|</FONT>
					&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/redir/m/forums.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>Echanger avec nous</FONT></A>&nbsp;&nbsp;<FONT COLOR='#ffffff'>|</FONT>
					&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/redir/m/download.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>T�l�chargements</FONT></A>&nbsp;&nbsp;<FONT COLOR='#ffffff'>|</FONT>
					&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/redir/m/msn.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>MSN</FONT></A>&nbsp;&nbsp;<FONT COLOR='#ffffff'>|</FONT>
					&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/redir/m/MsFrance.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>Microsoft France</FONT></A>&nbsp;&nbsp;<FONT COLOR='#ffffff'>|</FONT>
					&nbsp;&nbsp;<A href="/isapi/gomscom.asp?target=/france/aide/default.asp" style="COLOR: #ffffff; TEXT-DECORATION: none" target=_top ><FONT COLOR='#ffffff'>Aide</FONT></A>
					</B></FONT>
				</td>

		</tr>
	</TABLE>
</TD></TR></TABLE>
</DIV>
<!--TOOLBAR_EXEMPT-->

<!-- End: ToolBar V2.0-->

<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 width=100%>
<tr>

<!--LINK_START-->

<td width=180 valign=top bgcolor="#f1f1f1">
<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr bgcolor="White"><td><img src="/france/1.gif" border="0" width="1" height="1"></td></tr></table>
<table cellpadding="0" cellspacing="0" border=0>
<tr><td class="sea">
<table width=180 border="0" cellpadding="1">
<tr><td class="sea"><img src="/france/1.gif" border="0" alt="" width="2" height="1"></td>
<td class="sea" align="left"><p class=sea><form class=sea method=get action=/france/search/QuickSearchAny.asp>Recherche sur l'espace
<table cellpadding="0" cellspacing="0">
<tr><td><input type=text name=SearchString size=10 style='font-family: verdana, Arial; font-size: 8pt; border:1px solid black; width=130px;'><input type=hidden name=sname value="technet"><input type=hidden name=intCat value=1></td>
<td><img src="/france/1.gif" border="0" width="8" height="1"></td><td><INPUT class="go" type=submit value="Go"></td></tr>
</table>
<br>

</td></tr>
</form></table>
</td>
</tr>
<tr><td bgcolor="#f1f1f1" width=100%>



<TABLE class="flyoutMenu" menudata="/france/technet/inc/menu.xml" width="180" cellpadding="2" cellspacing="0" border="0" style="border-right-width: 0px"><tr><td><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl21"><a href="/france/technet/default.asp">Accueil TechNet</a></td></tr></table><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl22"><a href="/france/technet/edito/def_edito.asp">Editos</a></td></tr></table><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl23"><a href="/france/technet/presentation/default.asp">Qu'est ce que TechNet?</a></td></tr></table><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl27"><a href="/france/technet/produits/default.asp">Produits & Technologies</a></td></tr></table><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl230"><a href="/france/technet/themes/default.asp">Dossiers th�matiques</a></td></tr></table><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl240"><a href="/france/technet/telechargements/default.asp">T�l�chargements</a></td></tr></table><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl243"><a href="/france/technet/seminaires/default.asp">Formation et S�minaires</a></td></tr></table><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl249"><a href="/france/technet/support/default.asp">Support</a></td></tr></table><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl259"><a href="/france/technet/communaute/default.asp">Communaut�s</a></td></tr></table></td></tr></table><table class="flyoutMenu" menudata="/france/technet/inc/menu.xml" width="180" cellpadding="2" cellspacing="0" border="0" style="border-right-width: 0px"><tr><td><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl263"><a href="/france/technet/plansite.asp">Plan du site</a></td></tr></table></td></tr></table><table class="flyoutMenu" menudata="/france/technet/inc/menu.xml" width="180" cellpadding="2" cellspacing="0" border="0" style="border-right-width: 0px"><tr><td><table width="176" cellpadding="0" cellspacing="0" border="0"><tr><td class="flyoutLink"handle="cl265"><a href="/france/msdn/default.asp">MSDN</a></td></tr></table></td></tr></TABLE>

</tr>
</table>
<br>

<div align="center"><a href="/france/technet/presentation/flash/default.asp"><img src="/france/technet/images/technet_nl_140x100.gif" width="140" height="100" border="0" alt="Recevez gratuitement le flash TechNet"></a>

<a href="/france/technet/info/info.asp?mar=/france/technet/info/technet_ppc.html"><img src="/france/technet/images/technet_ppc_140x100.gif" width="140" height="100" border="0" alt="TechNet sur votre Pocket PC"></a>
</div>


</td>
<td width=1 bgcolor="#999999" valign="top"><table cellpadding="0" cellspacing="0" border="0" width="100%"><tr bgcolor="White"><td bgcolor="White" valign="top"><img src="/france/1.gif" border="0" width="1" height="1"></td></tr></table><img src="/france/1.gif" width=1 height="1"></td>



 <script>
function clikker(a,b,IO,IC) {
if (a.style.display =='') {a.style.display = 'none'; b.src=IC;}
else {a.style.display=''; b.src=IO; }}
</script>

<!--LINK_STOP-->
<TD valign="top">
<!--BANNER_START-->
<table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#6699cc">
<tr>
<td height="1" bgcolor="White"><img src="/france/images/1ptrans.gif" width="1" height="1" alt=" " border="0"></td>
</tr>
<tr>
<td height="22" align="right"><font color="White">Microsoft TechNet&nbsp;</font></td>
</tr>
</table>
<br clear="all">
<!--BANNER_STOP-->
<!--NEWS_START-->
<!--NEWS_STOP-->
<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0 width=100%>
<tr>
<td><img src="/france/images/1ptrans.gif" width="20" height="1" alt=" " border="0"></td>
<TD valign="top">
<!--TEXT_START-->
<a name="top"></a>
<p><img src="/france/technet/edito/images/edito_def.gif" width=200 height=40 border=0 align="top" alt="Edito TechNet">
<p>Imaginons que vous soyez comme la plupart d'entre nous des observateurs attentifs du march� informatique et que de fait, vous tentiez de vous tenir inform�s de la vie des soci�t�s, des nouveaux produits, des analyses strat�giques. Eh bien honn�tement, les raisons de se r�volter ne manquent pas.<br>
Traiter ces points avec un �clairage un peu moins 'politiquement correct' que ce qui nous est habituellement propos� est un exercice qui nous para�t rafra�chissant, et c'est ce point de vue tr�s personnel sur les sujets sensibles du moment que nous essayerons de r�guli�rement vous apporter...
<br><br>
<TABLE border=0 cellPadding=0 cellSpacing=0 align=right>
<TR bgcolor="White">
<TD class=tablebg>

<TABLE border=0 cellSpacing=1 width="100%" cellpadding="3">
<TR bgcolor="White">
<td>
<a href="/france/technet/edito/archives_edito.asp" Class="promo"><b>Les archives de l'�dito</b></a></td></tr></table></td></tr></table><p>

<h1>Dans Innovation il y a Ovation, mais bon� </h1>
<img src="/france/technet/edito/images/edito1.gif" alt="Pierre Bugnon" border="0" align="bottom"> 
<b>de <a href="mailto:pierrebu@microsoft.com">Pierre Bugnon</a> <br>
Architecte, D�partement Marketing Technique de Microsoft France<br><br></b>
<p>

La lecture des r�actions aux �ditoriaux est int�ressante � plus d'un titre. Tout d'abord parce qu'elle permet d'apprendre quelques expressions assez utiles si vous �tes amen� un jour � faire preuve de cr�ativit� dans les insultes que vous prof�rez, mais aussi et surtout, parce qu'elle met en �vidence un certain nombre d'id�es re�ues vis-�-vis de Microsoft et constitue donc un bon r�servoir de sujets pol�miques donc potentiellement int�ressants.
<p>
Parmi ces r�actions, on peut d�celer une certaine inqui�tude de la part des internautes sur la capacit� de Microsoft � innover dans des domaines autres que les campagnes marketing ou la tarification des produits. 
<p>
Quelques extraits pour illustrer ce point : 
<ul>
<li>"Alors, redescendez sur Terre s'il vous pla�t. Et surtout, ne parlez plus d'innovation, Microsoft n'innove pas".
<li>"Vraiment, je dois avouer que je vous trouve une singuli�re audace sur cette affirmation, d'autant plus dans la mesure o� vous �tes employ� d'une entreprise experte en communication, mais qui n'a produit qu'un nombre tr�s r�duit d'innovations techniques".
<li>"MS n'a plus l'image du d�part d'une bo�te jeune et innovante (imaginative) repr�sent�e par le logo du papillon".
<li>"Je tiens � pr�ciser aussi que Windows XP n'est pas tr�s innovateur lui non plus : pas de bluetooth, 
pas de version 64 bits � l'heure actuelle (bien que cela semble pr�vu), Linux tourne sur les Itaniums depuis belle lurette, pas de version P.P.C, etc. bref une incompatibilit� totale avec les derni�res nouveaut�s".
</ul>
<p>
J'aime bien cette derni�re citation car elle est la seule � tenter de fournir des arguments pr�cis pour d�montrer l'incapacit� de Microsoft � innover. Evidemment les arguments sont tr�s contestables, mais l'effort est louable.
<p>
Donc... Accoler les mots <i>Microsoft</i> et <i>Innovation</i> est manifestement consid�r� comme une provocation et d�clenche des r�actions assez enflamm�es qui souvent finissent par : "Monsieur, avec tout mon m�pris", ce qui est assez frustrant vous en conviendrez. 
<p>
Autant dire que <b>le sujet est prometteur</b> !!!
<p>
<h3>De quoi parle-t-on au juste ?</h3>
<p>
Que signifie "innover" ? La question peut para�tre stupide, mais il y a souvent une confusion entre l'innovation et l'invention. 

<p>Innover c'est "<i>introduire quelque chose de nouveau dans un domaine particulier</i>". Cette d�finition peut, bien entendu, s'appliquer aux inventions � proprement parler, mais aussi au fait d'imaginer un nouveau concept, ou d'utiliser des inventions pour les int�grer dans des produits satisfaisant de nouveaux besoins (produits qu'on qualifiera alors... d'innovants).
<p>
Normalement quelques dents devraient grincer � la lecture de ce paragraphe, et un esprit retors pourrait imaginer une tentative de red�finition de la notion d'innovation. <br>
Essayons donc d'illustrer par quelques exemples concrets les diff�rents aspects que peut recouvrir le concept d'innovation. 
<p>
Premi�rement, si l'on d�finit l'innovation comme le fait d'�tre le premier � inventer quelque chose, alors tr�s peu de soci�t�s, particuli�rement dans le domaine de la haute technologie, peuvent �tre qualifi�es d'innovantes. Par exemple, la souris d'ordinateur a �t� invent�e par Douglas Engelbart (d�monstration publique en 1968) du Stanford Research Institute, mais il a fallu de nombreuses ann�es et l'arriv�e des interfaces graphiques pour que cette invention soit consid�r�e comme un bouleversement technologique. 
<p>
De la m�me fa�on, le premier syst�me d'�dition hypertexte fut en 1967 l'�uvre de Andries Van Dam mais cette technologie n'a r�ellement influenc� le march� informatique qu'avec l'arriv�e du browser Internet.
Est-ce donc uniquement l'invention qui est importante, ou peut-on consid�rer que son utilisation dans un produit r�el est aussi un facteur d'innovation ? 
<p>
Si l'on consid�re ensuite qu'innover c'est seulement imaginer un nouveau concept, on est alors dans une situation amusante : les auteurs de romans d'anticipation seraient quasiment les seuls � pouvoir pr�tendre �tre des innovateurs dans le domaine des hautes technologies. <br>
La NASA a permis au premier homme de marcher sur la Lune... et alors ?? O� est l'innovation puisque Jules Vernes avait imagin� cette situation ? <br>
La vid�oconf�rence et les �crans plats... Orwell les a d�crit dans <i>1984</i>. 
<br>L'utilisation d'assistants personnels command�s par la voix et interconnect�s � un r�seau global par des technologies sans fil... Tout ceci est d�crit depuis 1960 dans les romans de science-fiction de Philip K. Dick.
<p>
Que Charles Babbage imagine en 1870 les ordinateurs programmables (capables de stocker l'information et de calculer plus vite que l'�tre humain) ou que Vannevar Bush anticipe Internet avec son "Memex" en d�crivant en 1945 la possibilit� de stocker et publier intelligemment la connaissance humaine pourrait s�rieusement faire r�fl�chir sur les r�les jou�s par Von Neumann (ordinateur), le PARC de Xerox (r�seaux locaux), l'ARPA, le CERN ou Vinton Cerf et Robert Kahn (qui r�alis�rent le premier r�seau num�rique) sans lesquels le concept de Memex imagin� par Vannevar Bush serait tr�s certainement rest� un exercice intellectuel brillant mais vain.
<p>
On voit bien ici que le simple fait d'imaginer un concept ou une technologie est certes une d�marche innovante, mais n'en est malheureusement qu'une facette.
<p>
Enfin, lorsque l'on parle d'innovation, on peut �tre tent� d'associer ce terme � des inventions majeures telles que le t�l�phone, l'�lectricit�, le four micro-ondes, les avions ou les fraises Tagada.<br>
Ne pas �tre � l'origine de tels bouleversements techniques ne permettrait pas d'�tre qualifi� d'innovant. Soit... Mais si l'on prend par exemple l'a�ronautique, peut-on consid�rer que toute �volution entre l'avion de Orville et Wilburg Wright et par exemple le concorde ou l'avion � d�collage vertical Harrier n'est pas le fruit d'un grand nombre d'innovations ? Certainement pas. En fait, et c'est particuli�rement vrai dans l'industrie informatique, on constate que l'innovation est un ph�nom�ne continu, et que c'est souvent par touches successives que se construisent les r�alisations les plus significatives. 
<p>
Donc on peut raisonnablement consid�rer que parler d'innovation recoupe des aspects li�s � l'invention ou l'imagination de nouveaux concepts, mais aussi la d�marche consistant � pressentir qu'une d�couverte peut �tre, par son int�gration dans un produit, un facteur d'�volution important.
<p> 
C'est cette derni�re approche que reprend Le ROBERT, dans sa d�finition de l'innovation : "il s'agit d'une technique nouvelle qui s'impose sur le march�". <br>
Cette d�finition est accompagn�e de cette citation du MONDE du 13 juin 1973 : "Une invention ne se hisserait � la qualit� d'innovation que dans la mesure o� elle rencontrerait un march�".
<p>
Il est, dans ces conditions, difficile de nier l'aspect �conomique de l'innovation dans notre soci�t�,  ce que de brillants sp�cialistes (Shumpeter ou plus r�cemment Kizner) ont formalis� en �tudiant par exemple l'influence de la "capacit� � innover" sur la sant� de l'�conomie. 
<p>

<h3>Microsoft est-elle une entreprise innovante ?</h3>
<p>
J'ai bien une petite id�e sur la question, mais il me semble qu'un simple "oui" serait assez d�cevant et pas forc�ment cr�dible. Je vous propose donc de d�velopper. Et pour ne frustrer personne, nous allons aborder les trois aspects du concept d'innovation (imagination, int�gration, invention) et peut-�tre essayer d'expliquer le d�calage entre les faits et cette image de bon gros monopole �touffant l'innovation qui circule dans certains milieux. Cette derni�re partie ne plaira pas � tout le monde, d'o� le "peut-�tre".
<p>
Mais avant de commencer, il y a un certain nombre de faits qu'il peut �tre int�ressant de consid�rer si l'on veut r�ellement se faire une opinion sur la contribution de Microsoft � l'industrie informatique, voire � certaines �volutions de notre soci�t� durant les 25 derni�res ann�es (en vrac) : 
<ul>
<li>Si l'on parle d'innovation, pourquoi ne pas rappeler que l'IEEE (Institute of Electrical and Electronics Engineers) qui n'est certes pas un organisme suspect de complaisance a d�cern� en 1998 � Microsoft son <i>Corporate Innovation Award</i>, ce qui n'est pas rien vous en conviendrez. 

<li>Mettre sur le march� un produit innovant (et de bonne qualit�) peut amener certaines formes de reconnaissance. Voici <a href="/france/technet/edito/edito13-2.asp" class=n>la liste</a> de plus de 550 distinctions d�cern�es (entre 1990 et 1996 pour les US, entre 1991 et 1998 en France) aux produits Microsoft lors de salons, par la presse informatique, ou par des organisations professionnelles. <br>
Cette liste est malheureusement incompl�te, mais si les produits qui y figurent �taient mauvais, inadapt�s ou inutiles, nous serions en droit de nous poser des questions tr�s d�sagr�ables sur l'utilit� de ces distinctions. Mais bon, passons... 
<li>Restons dans les aspects sensibles en �voquant le fait qu'habituellement lorsqu'une entreprise ou un particulier pense avoir d�couvert un proc�d� innovant, un outil est � sa disposition pour prot�ger son travail de cr�ation : le brevet. Et ce depuis trois si�cles. Donc on peut lorsqu'ils ne sont pas manifestement abusifs, consid�rer les brevets d�pos�s comme un bon indicateur de la cr�ativit� d'une entreprise. Il se trouve que Microsoft a d�pos� aux Etats-Unis 2110 brevets (d�compte au 31 Octobre 2001). <br>
Pour couper court aux remarques du genre "Ouais, tr�s bien, mais vos brevets c'est n'importe quoi, vous avez certainement prot�g� des concepts fumeux, parce que j'ai lu dans la rubrique potins de mon magazine t�l� que Microsoft �tait une entreprise monopolistique qui n'a jamais rien invent�", voici la <a href="/france/technet/edito/edito13-3.asp" class=n>liste des brevets d�pos�s</a>. � vous de vous faire une opinion mais cette fois-ci sur des faits concrets. Bonne lecture :)
<li>Ces innovations sont, bien entendu, int�gr�es dans les produits, mais aussi pour certaines fournies � la communaut� informatique par le biais de processus de standardisation. Microsoft est en effet � l'origine de technologies/protocoles/formats largement utilis�s tels que ODBC, ASF, RTF, AVI, WAV, Unimodem, SMB/CIFS, Winsock, et a largement contribu� � TrueType, ACPI, DeviceBay, USB, PXE, SOAP, WSDL, UDDI pour ne citer que quelques exemples. <br>
De plus cette soci�t�, cofondatrice du W3C est membre d'une douzaine d'organismes de standardisation (ANSI, ATSC, DVB, ETSI, HAVi, IEC, IETF, IMTC, ISO, ITU, JTC1, NCITS, SMPTE) et de nombreux consortiums (I2O, DMTF, DEN, ATM Forum, ADSL Forum, CommerceNet, ATVEF...)
<li>Enfin, on peut signaler que Microsoft fut l'un des premiers �diteurs de logiciels � se doter d'un centre de recherche qui regroupe maintenant plus de 600 chercheurs dans 40 domaines scientifiques.
</ul>
<p>
Je ne sais pas pour vous, mais il me semble que les simples �l�ments ci-dessus devraient fortement inciter � la circonspection si, comme � moi, on vous ass�ne un d�finitif "Et surtout, ne parlez plus d'innovation, Microsoft n'innove pas". 
<p>
Cette affirmation ne serait donc pas fond�e ? Difficile � imaginer compte tenu de la tribune dont disposent les personnes qui la v�hiculent. Il est donc n�cessaire de d�velopper un peu l'argumentaire. 
<p>
<h3>Des concepts innovants</h3><p>

M�me ses d�tracteurs les plus hyst�riques admettent que Microsoft est une soci�t� qui par la mise en �uvre de concepts innovants a eu une influence importante sur l'�volution du march� informatique de ces 25 derni�res ann�es. Parmi ces concepts, on peut citer :  
<ul>
<li>"Un ordinateur dans chaque foyer et sur chaque bureau" qui a �t� l'anticipation du succ�s de la  micro-informatique.  
<li>"L'information au bout des doigts" qui formalise la n�cessaire simplification de l'acc�s � l'information par l'importance des aspects d'ergonomie et d'int�gration. 
<li>L'utilisation massive d'un mod�le composants (OLE / COM / COM+) pour la mise en �uvre des applications, pr�figurant les pr�occupations de r�utilisation du code produit. 
<li>"Windows Open System Architecture (WOSA)" qui permet une compl�te isolation des applicatifs et des technologies d'acc�s aux donn�es et p�riph�riques simplifiant les op�rations de d�veloppement sur la plate-forme Windows.
<li>La "strat�gie .NET" qui annonce les �volutions � venir de l'utilisation d'Internet avec la mise � disposition plus simple de services applicatifs et une plus facile interaction entre p�riph�riques de toutes sortes.
<li>Enfin derni�rement le concept de "Trustworthy Computing" d�finissant de nouvelles priorit�s quant � l'investissement technologique, afin de faire �voluer l'outil informatique vers plus de stabilit� et de s�curit�.
</ul>
<p>
Dans le domaine du concept ou de l'imagination, il me semble donc n�cessaire d'�tre dot� d'un solide aplomb pour nier l'aspect innovant de Microsoft sans �clater de rire. Mais qu'en est-il des aspects techniques ?
<p>

<h3>Le point de vue technique</h3>
<p>
Sur ce point, il est de bon ton de relayer les deux points suivants pour �tre totalement en phase avec une certaine forme de pr�t � penser ab�tissant :
<ul>
<li>Comment une soci�t� qui n'a m�me pas �t� capable de cr�er MS-DOS ou BASIC et dont le succ�s n'est d� qu'� une p�le copie de Macintosh peut-elle pr�tendre � une quelconque qualification technique (d'autres "arguments" peuvent circuler, mais y r�pondre impliquerait d'�tre d�sagr�able avec quelques concurrents, ce qui est un exercice dangereux... <a href="/france/technet/edito/edito13-4.asp" class=n>Voici</a> n�anmoins quelques �l�ments relativisant cette affirmation).
<li>C'est en rachetant des myriades de soci�t�s que cette entreprise uniquement compos�e de sp�cialistes en marketing et communication fait progresser la qualit� technique de ses produits. 
</ul> 
<p>
On peut noter dans la d�marche une certaine coh�rence. D�montrer une erreur en s'appuyant sur un fantasme est une d�marche coh�rente. Absurde mais coh�rente. En plus, cette approche a l'avantage d'occulter compl�tement toute tentative de r�flexion (le truc qu'on fait avec sa t�te) sur l'ensemble des avanc�es techniques n�cessaires � la conception d'un produit innovant propos�es par Microsoft. Int�ressons nous donc aux faits.
<p>

<h3>Les innovations techniques</h3>
<p>
Essayons donc de faire une synth�se des innovations techniques int�gr�es par Microsoft dans ses produits  depuis 25 ann�es. On retrouvera dans cette liste un certain nombre d'apports directs de Microsoft Research mais aussi quelques petites r�alisations des �quipes de d�veloppement qui, au risque de surprendre certains, sont compos�es d'une assez importante proportion de d�veloppeurs comp�tents. 
<p>
A tout seigneur tout honneur, commen�ons par les <b>syst�mes d'exploitation</b>. 
<p>
Si l'on remonte � Windows 1.0, en 1983, il est d�j� int�ressant de constater que le fait de proposer aux entreprises un syst�me d'exploitation avec une interface graphique, consid�r�e � l'�poque comme un gadget, n'�tait pas forc�ment une d�marche conservatrice.
<p> 
D�cider en 1989 de d�velopper un nouveau syst�me d'exploitation (Windows NT, mis sur le march� en 1992) n'a pas d� non plus �tre une d�cision facile. 
<p>
Si l'on prend en compte que ce syst�me d'exploitation avait pour ambition d'�tre g�n�raliste, ou pour faire simple de pouvoir �tre utilis� sur les postes de travail, sur les serveurs de ressources et d'infrastructure et sur les serveurs applicatifs, qu'il int�grait le support du multi-processing sym�trique, qu'il apportait une isolation entre l'architecture syst�me et les architectures mat�rielles (HAL pour hardware abstraction layer) et qu'il proposait une infrastructure composants (COM/DCOM) puis un moniteur transactionnel objet (MTS en 1996) et un middleware orient� message (Microsoft Message Queue), je serais assez tent� de dire que ne pas d�celer de quelconque originalit� serait faire preuve d'une �tourdissante mauvaise foi. 
<p>

Cet environnement applicatif est d'ailleurs l'objet d'un certain nombre d'attentions puisque, mine de rien, nous avons pu en moins de deux ans proposer avec le "Framework .NET" des concepts assez originaux tels que les services Web, la communication entre composants h�t�rog�nes (SOAP), la localisation (UDDI) et la description (WSDL) des services applicatifs tout en faisant standardiser les technologies et en semant la panique chez nos concurrents. Il est o� le suivisme ?
<p>
Si l'on ajoute d�s les premi�res versions de Windows la possibilit� de compl�tement s�parer les interfaces de programmations applicatives (API) des "fournisseurs de services" (SPI) et donc de permettre l'acc�s � des sources de donn�es ou des p�riph�riques sans int�grer ce support dans les applications avec ce que l'on appelait � l'�poque (1990) WOSA pour Windows Open System Architecture, approche qui a abouti � des technologies telles que Winsock (communications socket), ODBC (acc�s bases de donn�es), ADSI (acc�s annuaires), CDO (acc�s messageries), TAPI (fonctions t�l�phonie) on comprend pourquoi la plate-forme Windows fut pl�biscit�e par les d�veloppeurs d'applications.
<p> 
On continue ? Allez, on continue, parlons un peu de <b>multim�dia</b>.
<p> 
Penser qu'un PC puisse permettre la manipulation du son, de l'image, de la vid�o �tait � l'�poque une d�marche assez novatrice. Elle n�cessitait le support de ces fonctions dans l'OS mais aussi la promotion d'un support adapt� tel que le CD-ROM, la disquette 1,4 pouce �tant un peu l�g�re compte tenu de la volum�trie des informations � stocker. 
<p>
Microsoft a accueilli la premi�re conf�rence internationale sur le CD-Rom en 1986 et ce n'est pas forc�ment un hasard si elle fut la premi�re soci�t� � rendre disponible ses produits exclusivement sur ce support. 
<p>
Quant aux supports multim�dia dans le syst�me d'exploitation, il est int�ressant de constater le succ�s qu'ont eu les interfaces de programmation DirectX (3D, Sound, Animation, 2D, etc.) fournissant un environnement complet pour le d�veloppement d'applications jusqu'alors cantonn�es dans l'univers de la station de travail graphique et de OpenGL. <br>
Citer ici les d�veloppements de nouveaux codec (WMA et WMV), d'un protocole de streaming (Active Streaming Protocol), des technologies de traitement du signal (annulation d'�cho, transmission multicast sans erreur, etc.) n'apporterait rien, mais bon, �a fait du bien.
<p>

Voyons maintenant l'aspect le plus important pour l'utilisateur non averti : la <b>simplicit� d'utilisation au moindre co�t</b>. 
<p>
Le principe para�t �vident, mais il est effroyablement complexe d'un point de vue technique. On peut citer ici l'ergonomie de l'interface graphique, le support du Plug and Play (dont Microsoft a fourni les sp�cifications), les notions de barre de t�che ou d'assistants pour la mise en �uvre de fonctions syst�mes, le support des balises actives, la possibilit� d'ins�rer des composants Web sur le bureau de travail et l'int�gration de technologies permettant le contr�le du PC pour des personnes handicap�es avec, entre autres, les "Speech API" permettant la reconnaissance vocale et la transformation d'une information audio en texte. 
<p>
Dans ce m�me domaine, un produit comme Windows XP apporte l'association de m�ta-donn�es aux informations saisies (clavier, voix, �criture) pour permettre des traitements complexes (traduction, mise en forme, recherche, d�placement, etc.). Cette technologie, utilis�e dans sa premi�re version par les balises actives, prendra toute sa mesure avec le "Tablet PC" et va tr�s certainement avoir un impact  consid�rable sur l'�volution des mat�riels.
<p>
Le d�veloppement des technologies Cleartype (confort de lecture des caract�res) de l'alpha blending (niveaux de transparences et superpositions des objets affich�s) ou les nombreux algorithmes de compression et d'affichage progressifs des images sont certainement des �l�ments participant aussi au confort d'utilisation d'un syst�me Windows. 
<p>
Bon... On pourrait continuer � disserter sur les diff�rents apports de Microsoft en terme d'innovations technologiques mais j'ai un peu peur que le r�sultat ne soit aussi p�nible � lire qu'il ne l'est � �crire. Donc je vous propose de faire court en citant en vrac quelques �l�ments int�ressants : 
<ul>
<li>Participation au d�veloppement de VIA (Virtual Interface Architecture) permettant des taux de transferts �lev�s entre applications sur diff�rentes machines physiques
<li>Technologie "Single Instance Storage" permettant de r�duire la redondance des donn�es �crites sur disque. 
<li>Syst�me de gestion de fichier clusteris� avec le serveur multim�dia "Netshow Pro"
<li>Int�gration de Netmeeting en 1996, mettant � disposition les fonctions de conf�rencing et collaboration multi-points. 
<li>Support avanc� des langages utilisant des id�ogrammes tels que le Chinois traditionnel (mod�lisation, reconnaissance automatique) et d'un point de vue g�n�ral un fort investissement pour fournir des versions des OS et des applications tenant compte des sp�cificit�s des diff�rentes langues.
<li>D�tection d'emails ind�sirables (Exchange, Hotmail)
<li>Int�gration d'outils d'administration graphiques dans une base de donn�es relationnelle d'entreprise (SQL Server). 
<li>Moteur d'inf�rence pour les assistants de r�solutions d'incidents en ligne (support technique)
<li>Int�gration dans SQL Server du pilotage du moteur par DCOM
<li>Automatisation de l'administration et de l'optimisation d'une base de donn�es relationnelle (SQL Server 7.0).
<li>M�canismes d'optimisation de requ�tes avec SQL Server
<li>Un nouveau langage avec C#
<li>Encarta, qui fut l'une des premi�res encyclop�dies multi-m�dia.
<li>Dans le domaine du mat�riel, le Gamepad FreeStyle qui permet de piloter un jeu avec le mouvement libre, produit qui n'a aucun �quivalent sur le march�.
<li>Le joystick Microsoft � retour de force et � positionnement num�rique 
<li>La souris optique
<li>L'autoPC, int�grant les fonctions d'un PC et d'un autoradio
<li>Word 1.0 (1983) : premier traitement de texte � supporter la souris
<li>Word 3.0 pour r�seaux (1985) : premier traitement de texte apportant le support des imprimantes laser
<li>Word 4.0 pour Mac (1989) : premier traitement de texte int�grant la gestion des tableaux 
<li>Office (1989) : premi�re suite bureautique
<li>Premi�re suite unifi�e (m�me structure de menu, de barre d'outils etc... installation unique) avec Office 4
<li>Works (1991) : Int�gration de fonctions multim�dia 
<li>Word 2.0 pour Windows (1991) : Premier traitement de texte � supporter le glisser-d�placer
<li>Fourniture du premier tableur en environnement graphique (Excel Mac)
<li>Premi�re suite bureautique mettant place le HTML comme format de fichier compagnon natif (Office 2000)
<li>Premier traitement de texte � pouvoir enregistrer en HTML (Word 6 avec Internet Assistant 1.0)
<li>Premi�re suite bureautique rendant disponible l'aide par la r�ponse � des questions en langue naturelle (Office 95)
<li>Menu et barre d'outils auto-adaptatifs (Office 2000)
<li>Premier traitement de texte multilingue (Microsoft Word pour Windows 2.0, puis Microsoft Word pour Windows 2000 avec la d�tection automatique de la langue)
<li>Premi�re suite multilingue (interface) Office 2000
<li>V�rification orthographique contextuelle (soulign� rouge et suggestion contextuelle) (Word 95)
<li>Filtre automatique (Excel 5)
<li>Requ�te sur le Web (Excel 97)
<li>Menus contextuels (Word 6)
<li>Premier gestionnaire d'agenda "de groupe" (permettant les "meeting requests", le partage d'agenda...) (Schedule + 1.0)
<li>Premier " Personal Information Manager ", regroupant et int�grant dans une m�me interface messagerie, calendrier, contacts... (Outlook 97)
<li>Bo�tes de dialogue � onglet (Excel 4) pour simplifier les dialogues
<li>Mode plan dans un traitement de texte (Word 3.0 pour MS-DOS)
<li>Fourniture avec Office 97 d'un correcteur grammatical 
<li>Premier traitement de texte pour PC avec aper�u avant impression (Word 4.0 pour MS-DOS)
<li>Int�gration des langages de programmation aux applications. Au d�part, il s'agissait de macro-commandes, avec l'apparition de Windows, ils sont devenus de v�ritables langages de programmation structur�s (Visual Basic) permettant de r� exploiter les fonctions des logiciels applicatifs pour des d�veloppements sp�cifiques. Office fut la premi�re suite bureautique � proposer un langage de programmation structur� enti�rement unifi� (Visual Basic pour Applications).
<li>Des aides en ligne, assistants et syst�mes d'autoformation int�gr�s aux logiciels (initialis�s dans Works pendant les ann�es 1980). Ce sont des outils qui ont rendu les logiciels plus accessibles, et qui ont r�duit leurs co�ts de formation surtout pour leur mise � jour.
<li>Des tableaux dynamiques crois�s dans Excel pour analyser les donn�es.
<li>La technologie IntelliSense que l'on trouve dans Office et qui simplifie la saisie de textes sous Word ou de tableaux sous Excel en proposant de compl�ter les mots saisis.
<li>Technologie d'agents intelligents pour le support des utilisateurs avec interrogation en langage naturel ou avec la voix. 
</ul>
<p>
Un esprit perspicace notera une proportion tr�s importante des apports technologiques portant sur les domaines des OS, de la suite Office et des bases de donn�es. Peut-on trouver un lien entre les parts de march� de ces produits et la cr�ativit� des d�veloppeurs ? <br>
C'est une question int�ressante mais qui remet en cause l'id�e r�pandue qu'il serait possible d'obliger les gens � acheter des produits ne leur apportant rien par la seule force d'un marketing efficace. Et c'est un point sur lequel il est prudent de ne pas trop insister.
<p>
<h3>Microsoft et la recherche</h3>
<p>
Nous avons illustr� quelques exemples d'innovations dans le domaine des concepts et de la mise sur le march� de technologies cr�atives, voyons maintenant le troisi�me aspect, qui est celui de l'invention.
<p> 
Sur ce point Microsoft f�t l'une des premi�res soci�t�s �ditrices de logiciels � se doter en 1991 d'un centre de recherche fondamentale et appliqu�e dans le domaine de l'informatique. Il y a maintenant chez Microsoft Research environ 620 chercheurs travaillant dans plus de quarante domaines tels que la reconnaissance vocale, les interfaces graphiques, les outils de d�veloppement, les r�seaux et syst�mes d'exploitation, la t�l�-pr�sence, la sociologie ou la s�curit� pour n'en citer que quelques uns.
<p>
Ces �quipes maintenant dirig�es par Rick Rachid (inventeur de l'OS � micro-noyau MACH et concepteur du jeu en ligne "All�geance" en 1999) sont actuellement r�parties entre Redmond, San Francisco, Cambridge et Beijing permettant des �changes plus simples avec les milieux acad�miques europ�ens, am�ricains et asiatiques. 
<p>
<b>Et ces gens l�, ils font quoi ?</b> <br>
Pour essayer de r�pondre � cette question voici quelques �l�ments plus ou moins anecdotiques qui permettront de se faire une petite id�e de leur activit� et de la valeur des personnes travaillant pour Microsoft Research :  
<ul>
<li>Certains chercheurs dans le domaine des syst�mes m�caniques micro-�lectroniques ont d'ores et d�j� d�velopp� des machines plus petites que la section d'un cheveu humain.
<li>On y trouve les concepteurs de l'architecture VAX, de l'imprimante laser, d'Ethernet, de CICS, mais aussi des sociologues et sp�cialistes en psychologie cognitive. Il doit, j'imagine, y avoir aussi des machines � caf� et quelques photocopieuses.
<li>Trois chercheurs ont re�u le prix A.M. Turing, �quivalent du Nobel pour l'informatique : Jim Gray (syst�mes transactionnels), Butler Lampson (interconnexion de syst�mes), et Charles Antony Hoare (design des langages de programmation), 
<li>Des mod�les probabilistiques furent produits permettant de pr�voir et donc d'anticiper le comportement, les pr�f�rences et les besoins d'un utilisateur. Ces techniques sont utilis�es pour la personnalisation automatique des logiciels et ont les retrouve dans les assistants, les outils de support ou pour filtrer les messages ind�sirables.
<li>C'est � Microsoft Research que l'on doit entre autres <br>
<ul>
o	Les technologies optiques utilis�es dans la souris de m�me nature<br>
o	Les technologies Cleartype<br>
o	Le fait que le correcteur grammatical soit � peu pr�s efficace pour un grand nombre de langues<br>
o	Les bases de donn�es se configurant automatiquement<br>
o	Les balises actives <br>
o	Les m�canismes de notification intelligents<br>
o	Les outils d'analyse du code source permettant aux �quipes de d�veloppement de fournir des produits de meilleure qualit� d�s les premi�res versions<br>
o	Les m�canismes de typage de code int�gr�s dans le Framework .NET<br>
o	Et plein d'autres contributions dont la liste est disponible ainsi que beaucoup d'autres informations sur le site Web <a href="http://research.microsoft.com/" class="n" target="_blank">http://research.microsoft.com</a>
</ul></ul>
<p>
Je ne sais pas pour vous, mais moi ce qui me vient assez naturellement � l'esprit comme question c'est : <b>A quoi va ressembler l'informatique dans quelques ann�es ?</b><p>

Il est certainement instructif pour r�pondre � cette question de d�couvrir les projets sur lesquels travaillent actuellement les chercheurs de Microsoft Research. <br>
Pour faire simple, les principales directions de recherche portent sur les aspects de mont�e en charge, sur la qualit� du code (technologies permettant de d�tecter les bogues avant compilation), sur les interactions entre l'homme et la machine qui doivent n�cessairement �voluer pour permettre de dialoguer naturellement avec son ordinateur, sur des environnements de d�veloppement permettant de cr�er plus facilement des programmes, et sur les nouvelles formes de machines intelligentes avec des choses telles que les ordinateurs portables, mais au sens de porter un v�tement (pas de porter sa croix... quoique).
<p>

On peut, pour illustrer ces diff�rents points, imaginer que demain notre ordinateur sera capable de nous reconna�tre, d'anticiper certains de nos besoins (pas tous heureusement ;)), que les interactions avec la machine se feront en utilisant le langage naturel ou en �crivant comme un cochon sur une tablette et que nous aurons en permanence � notre disposition l'ensemble de l'information et des services disponibles sur les sujets les plus vari�s (ce dernier point est une extension du concept de "Memex" imagin� par Vanevar Bush en 1940). <p>

Si l'on ajoute que tout ceci fonctionnera sans erreur et de fa�on totalement s�curis�e, on voit bien qu'il y a dans le domaine de l'informatique quelques gisements inexploit�s d'innovation que les chercheurs de Microsoft Research explorent assez activement.
<p>
<h3>Pourquoi cette impression (Microsoft n'innove pas) ?</h3>
<p>
Voil�, vous avez maintenant � votre disposition quelques �l�ments qui devraient vous permettre de vous faire une petite id�e sur la question de savoir si dans les domaines de l'invention, de la prise de risque associ�e � la commercialisation de technologies un peu en avance de phase ou dans le domaine des concepts Microsoft est ou n'est pas une soci�t� innovante. 
<p>
J'ai personnellement une petite id�e sur la question, mais ladite soci�t� me versant un salaire il n'est pas �vident que mon avis soit consid�r� comme totalement impartial. 
<p>
Donc partons sur une hypoth�se de travail qui serait assez modeste : Consid�rons que Microsoft soit une soci�t� suffisamment innovante pour que les entreprises ou les particuliers pensent qu'acheter ses produits leur apporte une r�ponse � certains besoins nouveaux. C'est modeste, je ne parle pas de r�volution, ni de vision. On reste dans le concret.
<p>
Et bien, il se trouve que la simple phrase ci-dessus va certainement me valoir des flots d'invectives ou dans le meilleur des cas quelques messages sympathiques m'expliquant les bienfaits d'une n�cessaire reconversion dans une activit� moins stressante compte tenu d'un �tat psychique manifestement inqui�tant. 
<p>
En gros, dire que Microsoft puisse �tre une soci�t� innovante c'est dans un d�ner en ville aussi bien per�u que de faire un �norme rot pendant une discussion sur l'importance g�ostrat�gique de la mission de Bernard Henri Levy en Afghanistan. On ne rote pas lors d'une discussion sur le prestige de la France. Encore moins lorsqu'il s'exprime par l'envoi vers des populations l�g�rement d�stabilis�es d'une personne capable de r�soudre leurs probl�mes en moins de trois semaines. C'est mal. C'est compl�tement iconoclaste.
<p>
La question est de savoir pourquoi le fait d'associer innovation et Microsoft est une d�marche iconoclaste. 
<p>
Tout d'abord, il faut nuancer le propos. Pour les personnes ayant un peu de recul sur l'�volution des technologies informatiques, ce genre d'affirmation n'a rien de choquant. Mais pourquoi cette id�e est-elle par ailleurs aussi r�pandue ? 
<p>
Si, pour expliquer les cons�quences, on essaie de s'int�resser aux causes, ce qui pour un paysan savoyard est une d�marche assez logique, on se pose assez vite quelques questions auxquelles il est assez simple, mais pas forc�ment "politiquement correct", d'apporter des r�ponses. 
<p>
Il est int�ressant de noter que cette approche peut �tre utilis�e dans d'autres contextes, permettant d'exercer ce que les anciens appelaient le libre arbitre, notion un peu d�su�te qui �vite cependant de prendre pour argent comptant des informations dont la seule l�gitimit� est de servir les desseins de l'informateur ou de ses mandants.
<p>
Donc voici une explication personnelle (c'est important) de la d�marche :  
<ul>
1. Les logiciels Microsoft sont, pour la plupart, assez largement utilis�s. En particulier les syst�mes d'exploitation. <br>
2. Cette situation peut poser un certain nombre de probl�mes, surtout si vous voulez promouvoir un "nouveau" syst�me d'exploitation. <br>
3. D'un point de vue technique, il n'est pas vraiment envisageable de d�stabiliser Windows parce que les arguments avanc�s ont peu de chance d'�mouvoir <p>
<ul>
o	Ni l'utilisateur de Windows ME : Le d�bat sur l'importance de l'utilisation des threads noyau dans le noyau Linux, il s'en bat l'oreille avec une patte de crocodile� c'est en fait le cas de mon p�re, et de nombreux amis. <br>
o	Ni les personnes un tant soit peu averties qui, de toute fa�on, utilisent Windows 2000 ou Windows XP et ont donc un OS tout � fait acceptable en terme de performances, stabilit� ou s�curit�, qui sont les reproches les plus souvent formul�s.
</ul>
<p>
4. Il est, dans ces conditions, n�cessaire de d�placer le probl�me en essayant de modifier l'image positive de Microsoft assez g�n�ralement associ�e � l'innovation et la r�ussite. <br>
5. Pour cela, il est intelligent de communiquer sur le fait que Microsoft n'innove pas. 

<p>
Ce point m�rite d'�tre d�velopp�. Comment communiquer sur le fait que Microsoft n'innove pas ? A priori l'exercice est d�licat, compte tenu des faits. Il va donc falloir compenser l'extr�me fragilit� des arguments par un important matraquage m�diatique. <p>

Il se trouve que c'est certainement le livre de Dominique Nora interviewant Roberto Di Cosmo (<i>Le hold-up plan�taire, la face cach�e de Microsoft</i>, Edition Calman Levy, 1998) qui fut l'outil utilis�. Mr Di Cosmo (professeur d'informatique � Normal Sup) d�veloppe dans cet ouvrage un discours consternant assez bien r�sum� par la citation suivante : "J'aime profond�ment la technologie, et c'est pr�cis�ment pour �a que je ne peux supporter de la voir pervertie par une entreprise qui con�oit de mauvais produits, qu'elle fait payer cher � ses consommateurs qu'elle asservit, une soci�t� qui m�prise ses clients, pi�ge ses concurrents et �touffe l'innovation". 
<p>
Le reste est � l'encan. On peut, � la lecture de ce livre �tre atterr� par l'absurdit� des arguments avanc�s, mais apr�s tout il n'est pas ill�gal d'�crire n'importe quoi. Ce qui est plus �tonnant, c'est la tribune offerte par les m�dias pour diffuser ce message plus que discutable : la radio, la t�l�vision et la presse �crite ont assez g�n�reusement relay� ces propos en conviant Mr Di Cosmo � d�velopper ses th�ories aux heures de grande �coute (sauf pour la presse �crite parce que la notion d'heure de grande �coute leur est assez �trang�re). 
<p>
6. Reprenons... Communiquer sur le fait que Microsoft n'innove pas est une d�marche compl�tement absurde si l'on s'en tient aux faits, mais elle a un double avantage : 
<p>
<ul>
o	Diminuer sensiblement le capital sympathie de cette soci�t�<br>
o	Permettre de fa�on un peu tordue de faire passer l'id�e que si les gens ach�tent ses produits, ce n'est pas parce qu'ils satisfont voire m�me anticipent leurs besoins, mais tout simplement parce qu'ils sont oblig�s.
</ul><p>
7.	On peut ensuite sur cette base assez facilement communiquer sur le fait qu'une soci�t� qui d�tient de telles parts de march�, sans m�me prendre la peine d'innover, repr�sente un r�el danger pour la libert� individuelle, la d�mocratie, la paix dans le monde, l'�quilibre �cologique de la plan�te et que sais-je encore. <br>
8.	Le d�bat n'est plus technique, mais id�ologique, la boucle est boucl�e. Bravo l'artiste.
</ul>
<p>
Je ne sais pas pour vous mais ce sc�nario �vacuant le r�le des concurrents me ferait hurler de rire si son efficacit� n'�tait pas confirm�e par de nombreuses prises de positions m�diatiques et politiques.
 <p>
Il est enfin � noter que cette "strat�gie" concerne plut�t les promoteurs du logiciel libre, qui ne repr�sentent qu'une petite partie des concurrents de Microsoft. On peut au moins leur conc�der une certaine subtilit� dans l'approche alors que d'autres soci�t�s plus classiques pr�f�rent utiliser la justice pour pallier les manques de leurs offres, mais cet aspect du probl�me �tant... d�licat et hors sujet, je ne d�velopperai pas.
<p>

<b>Pour finir, un petit jeu... </b><br>
Compte tenu de la nature m�me du th�me d�velopp� dans cet �ditorial, et de l'aspect un peu indigeste de certaines parties, je vais me permettre de remercier les quelques personnes qui liront cette conclusion. Braver l'�num�ration r�barbative de technologies, supporter ma mauvaise foi et s'ab�mer les yeux pour essayer de comprendre ce que quelqu'un de Microsoft peut avoir � dire sur l'innovation est tout � fait m�ritoire. Surtout qu'avec l'arriv�e des beaux jours, il y a plein d'autres choses � faire que de lire des d�lires parano�aques sur le site Technet. Dont acte, merci de votre patience, et � bient�t sur le mail.
<p>
Certains d'entre vous ont peut-�tre �t� int�ress�s par la d�marche consistant � confronter les faits et les fantasmes, mais j'imagine qu'un certain nombre, compl�tement atterr�s qu'on puisse essayer d'argumenter sur les aspects innovants de Microsoft, ont poursuivi la lecture, se demandant jusqu'o� il �tait possible d'aller dans l'infamie. Et c'est pour vous, lecteurs exasp�r�s � l'id�e que l'on puisse de temps en temps ne pas rester dans les rails du politiquement correct que je propose ce petit jeu :
<p>
Si cet �dito vous a vraiment r�volt�, avant de m'envoyer la jolie bord�e d'insultes que vous avez pr�par�, je vous propose d'essayer de r�fl�chir un petit peu et d'�tayer votre indignation par des �l�ments concrets. <br>
C'est effectivement assez p�nible d'�tre oblig� de r�fl�chir un peu, mais� celui ou celle d'entre vous qui arrivera � me convaincre par la pertinence de ses arguments gagne une invitation � l'anniversaire de ma fille qui f�tera en Novembre ses 7 ans. Vous pourrez jouer � la XBox, manger des g�teaux et des bonbons en forme d'ours ou de bouteilles de Coca-cola en rigolant avec plein de gosses hyst�riques.
C'est cool non ?
<p>
En plus �a va me permettre de faire le tri entre les personnes qui r�agissent � ce qui est �crit, en essayant d'argumenter, et les d�c�r�br�s qui encombrent ma bo�te aux lettres parce qu'ils ont lu sur un forum qu'il y a un h�r�tique � convertir en recopiant l'argumentaire pr�par� qui lui fera comprendre combien il est absurde d'essayer de d�fendre des id�es qui sont manifestement fausses car pas dans l'air du temps.
<p>
Voil�... En travaillant sur des �l�ments concrets (et publics), il me semble que finir ce long texte en affirmant que Microsoft est une soci�t� innovante n'exprime pas forc�ment une tendance lourde � la provocation. Mais c'est � vous de juger...
<p>
<b>Pierre Bugnon, sain de corps et d'esprit </b>
<p>
Message personnel<br>
Salut les Suisses. Pas de fondue cette fois, mais vos mails sont les bienvenus ;) !



<p><p><img src="/france/technet/images/fl_bleu.gif" width=11 height=9 border="0">&nbsp;<b>Si vous d�sirez r�agir � cet article vous pouvez directement contacter l'auteur : </b><a href="mailto:pierrebu@microsoft.com">pierrebu@microsoft.com </a>
<br><br>
<!--TEXT_STOP-->
</TD>
<td width=10><IMG SRC="/france/images/1.gif" WIDTH=10 HEIGHT=1 BORDER=0></TD>
<td valign=top>
<!--PROMO_START-->

<!--PROMO_STOP-->
</td></tr></table>
</TD>
</TR>
</TABLE><!--CP_START-->

<TABLE ID='idFooter' STYLE='background-color:white;width:100%' cellSpacing='0' cellPadding='0' border='0' width="100%">
<TR VALIGN='MIDDLE'><TD ID='idFooterRow1' STYLE='background-color:#003399;height:20;width:100%' NOWRAP>
		&nbsp;<A TARGET='_top' STYLE='text-decoration:none;cursor:hand;font:bold xx-small Verdana;color:#FFFFFF;' HREF='/france/aide/default.asp' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Besoin d'aide ?</A>
		&nbsp;<SPAN STYLE='font:bold xx-small Verdana;color:#FFFFFF'>&nbsp;|</SPAN>		
		&nbsp;<A TARGET='_top' STYLE='text-decoration:none;cursor:hand;font:bold xx-small Verdana;color:#FFFFFF;' HREF='mailto:?subject=Un article TechNet&body=Voici un article int�ressant de TechNet:  http://www.microsoft.com/france/technet/edito/def_edito.asp' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Envoyer cette page � un(e) ami(e) </A>
		&nbsp;<SPAN STYLE='font:bold xx-small Verdana;color:#FFFFFF'>&nbsp;|</SPAN>				
		&nbsp;<A TARGET='_top' STYLE='text-decoration:none;cursor:hand;font:bold xx-small Verdana;color:#FFFFFF;' HREF='/france/redir/tb_news.asp' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Newsletter : TechNet Flash</A>
		</TD>
	</TR>
</TABLE>
<!--CP_START-->
<TABLE CELLPADDING=0 CELLSPACING=0 BORDER=0 class=copy width=100%>
<td class=copy align=left width=100%>
<SPAN class=copy>&#169; 2002 Microsoft Corporation. Tous droits r�serv�s. <A TARGET="_top" class="copy" HREF="/france/misc/cpyright.asp">Conditions d'utilisation</A>&nbsp;<FONT color="#FFFFFF">|</FONT>&nbsp;<A TARGET='_top' class="copy" HREF='/info/fr/privacy.htm'>Confidentialit� </A>&nbsp;<FONT color="#FFFFFF">|</FONT>&nbsp;<A TARGET='_top' class="copy" HREF='/france/accessibilite/'>Accessibilit�</A>&nbsp;&nbsp;
</SPAN>
</td>
<td class=copy align="right"><a href="#top"><img src="/france/images/top.gif" alt="top" width=24 height=13 border=0></a></td>
</TR>
</TABLE>
<!--CP_STOP-->
</BODY>
</HTML>
<!--CP_STOP-->
</BODY>
</HTML>


