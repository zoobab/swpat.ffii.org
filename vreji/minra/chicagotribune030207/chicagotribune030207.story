















<html>
<head><script language="javascript" type="text/javascript" src="/javascript/Tacoda_AMS_DDC_Header.js"></script>
<title>
	Chicago Tribune | Note: This headline is patented
</title>
<meta name="Keywords" content="patent and trademark office  u s, 000008797, government reform, series, patents, non dup, ph the nation" />
<meta name="Description" content="For decades, finicky children have been eating peanut butter and jelly sandwiches with the crust removed. From a legal point of view, however, the lunchbox staple was invented on a patio in Fargo, N.D., in 1995." />
<script language="javascript" src="/javascript/pulldown.js"></script>
<script language="javascript" src="/javascript/windoid.js"></script>
<script language="javascript" type="text/javascript" src="/javascript/Tacoda_AMS_DDC_Header.js"></script>
<link rel="stylesheet" href="/stylesheets/chinews.css" type="text/css">

</head>
<body bgcolor="#FFFFFF" text="#000000" link="#003366" vlink="#336699" alink="#990000" leftmargin="0" topmargin="2" marginwidth="0" marginheight="2">





<map name="tm"><area shape="rect" alt="News" coords="0,0,64,13" href="/?track=ctnewstab" target="_top"><area shape="rect" alt="Sports" coords="65,0,139,13" href="/go/chicagosports" target="_top"><area shape="rect" alt="Entertainment" coords="140,0,264,13" href="/go/metromix" target="_top"><area shape="rect" alt="Business" coords="265,0,351,13" href="/business/?track=ctbusinesstab" target="_top"><area shape="rect" alt="Homes" coords="352,0,420,13" href="/classified/realestate/?track=cthomestab" target="_top"><area shape="rect" alt="Jobs" coords="421,0,480,13" href="/classified/jobs/?track=ctjobstab" target="_top"><area shape="rect" alt="Cars" coords="481,0,538,13" href="/go/cars" target="_top"><area shape="rect" alt="Place ads" coords="539,0,620,13" href="/classified/placead/chi-classified-placead.htmlstory?track=ctplaceadstab" target="_top"><area shape="rect" alt="Subscribe" coords="621,0,759,13" href="/go/subscribe" target="_top"></map>
<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="4" valign="bottom"><img border="0" src="/images/chinews/biztab.gif" width="759" height="14" usemap="#tm"></td>
</tr>
<tr>
<td align="center" bgcolor="#003366" width="215"><a href="/" target="_top"><img src="/images/chinews/logotopcurve.gif" border="0" width="215" height="54" alt="chicagotribune.com"></a><br><font face="verdana, sans-serif" color="#ffffff" size="1"><b>September 17, 2003</b></font><br><img src="/images/clear.gif" width="215" height="4" alt=""><br></td>
<td bgcolor="#003366" width="70" align="center"><a href="/go/weather"><font id="weather-header" color="#ffffff">








<!-- Weather snippet for kord -- 3:35 PM CDT on September 17, 2003 -->
<img width="33" height="27" hspace=3 vspace=2 border="0" src="/images/chinews/weather/smallicons/clear.gif"><br>81&deg; F
<!-- 0.947:0 -->



</font></a><br><img src="/images/clear.gif" width="70" height="1" alt=""></td>
<td bgcolor="#003366" align="right" valign="middle" width="472"><table cellpadding="0" cellspacing="0" border="0" width="472"><tr><td>




<!-- Ad Space: html.ng/site=chicagotribune&color=none&edition=newspaper&content=technology&channel=local&adtype=banner&adsize=468x60&adplacement=top&tag=std -->

<!-- Blank placeholder ad -->

<!-- /Ad Space: html.ng/site=chicagotribune&color=none&edition=newspaper&content=technology&channel=local&adtype=banner&adsize=468x60&adplacement=top&tag=std -->
</td></tr></table></td><td width="3" bgcolor="#003366"><img src="/images/clear.gif" width="3" height="1" alt=""></td>
</tr></table><table width="760" border="0" cellpadding="0" cellspacing="0">



<tr>
<td bgcolor="#cccccc">
<table width="759" border="0" cellpadding="0" cellspacing="0" bgcolor="#cccccc">
<tr>



	

	
<td valign="middle" align="left"><font id="navbar" color="#990000"><b>&nbsp;Please <a href="/services/site/registration/show-createprofile.register">register</a> or <a href="/services/site/registration/show-login.register">log in</a></b></font><font id="navbar"><b> | </b></font><font id="navbar"><b><a href="/services/site/registration/show-memberservices.register">Member services</a></b></font></td>
	

<form method="get" action="/search/chi_all.jsp"><td align="right" valign="middle"><font id="navbar"><font color="#990000"><b>Story search:</b></font> <b>Last 7 days</b></font></td><td align="left" valign="middle"><font id="navbar"> <IMG SRC="/images/chinews/redarrow.gif" height="20" width="10" border="0"></font></td><td valign="middle"><font id="navbar"><input type="text" name="Query" size="8"> <input type="submit" value="Go"></font></td></form><form method="GET" action="http://pqasb.pqarchiver.com/chicagotribune/search">
<td align="right" valign="middle"><font id="navbar"><b>Older than 7 days</b></font></td><td align="left" valign="middle"><font id="navbar"> <IMG SRC="/images/chinews/redarrow.gif" height="20" width="10" border="0"></font></td><td valign="middle"><font id="navbar"><input type="text" name="QryTxt" size="8"> <input type="submit" value="Go"></font></td></form>
</tr>
</table>
</td>
</tr>
<tr><td bgcolor="#cccccc"><img src="/images/clear.gif" height="2" width="1" alt=""></td></tr>
</table>


<table width="760" cellspacing="0" cellpadding="0" border="0">
<tr>
<td width="140" bgcolor="#cccccc" valign="top">
<!-- Ad Space: html.ng/site=chicagotribune&color=none&edition=newspaper&content=technology&channel=local&adtype=sponsor&adsize=88x31&adplacement=left1&tag=std -->

<!-- Blank placeholder ad -->

<!-- /Ad Space: html.ng/site=chicagotribune&color=none&edition=newspaper&content=technology&channel=local&adtype=sponsor&adsize=88x31&adplacement=left1&tag=std -->





<table width="140" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" height="1" width="1" alt=""></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#666666"><a href="/classified?track=ctclassifedtopinsidenavrail"><font id="navlinks-light" color="#eeeeee">Classified</font></a><font id="navlinks-light" color="#eeeeee"><b>&nbsp;&nbsp;|&nbsp;&nbsp;</b></font><a href="/shopping?track=ctshoppingtopinsidenavrail/"><font id="navlinks-light" color="#eeeeee">Ads</font></a></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#f5ecdc">
<font id="navlinks-classified">
<a href="/classified/jobs/?track=ctjobsinsidenavrail"><font id="navlinks-classified">Find a job</font></a><br>

<a href="http://www.cars.com/carsapp/chitrib/?szc=60005&srv=adlocator&act=populate&ft=1&tf=quick_usedforsale-default.tmpl&page=used&rn=7&zc=&rd=30"><font id="navlinks-classified">Find a car</font></a><br>

<a href="/classified/realestate/?track=cthomesinsidenavrail"><font id="navlinks-classified">Find real estate</font></a><br>

<a href="/classified/realestate/renting/?track=ctapartmentsinsidenavrail"><font id="navlinks-classified">Rent an apartment</font></a><br>

<a href="/classified/realestate/?src=frontpage#mortgages"><font id="navlinks-classified">Find a mortgage</font></a><br>

<a href="/shopping/?track=ctshoppinginsidenavrail"><font id="navlinks-classified">See newspaper ads</font></a><br>

<a href="http://www.switchboard.com/bin/cgidir.dll?MEM=1318"><font id="navlinks-classified">White/yellow pages</font></a><br>

<a href="http://metromix.chicagotribune.com/dating/?track=ctpersonalsinsidenavrail"><font id="navlinks-classified">Personals</font></a><br>

<a href="/classified/placead/chi-classified-placead.htmlstory?track=ctplaceanadinsidenavrail"><font id="navlinks-dark">Place an ad</font></a>
</td>
</tr>
</table>


<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#003366"><a href="/go/weather"><font id="navlinks-light" color="#eeeeee">Weather</FONT></A><font id="navlinks-light" color="#eeeeee"><b>&nbsp;&nbsp;|&nbsp;&nbsp;</b></font><a href="/news/traffic/cltv/chi-traffic.framedurl"><font id="navlinks-light" color="#eeeeee">Traffic</font></a></td>
</tr>
</table>

<table width="140" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" height="1" width="1" alt=""></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#003366"><a href="/"><font id="navlinks-light" color="#eeeeee">News/Home page</font></a></td>
</tr>
</table>

<table width="140" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" height="1" width="1" alt=""></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#003366"><a href="/news/printedition/"><font id="navlinks-light" color="#eeeeee">Today's paper</font></a></td>
</tr>
</table>

<table width="140" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" height="1" width="1" alt=""></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#003366"><a href="/services/site/specialsections/chi-sections.special"><font id="navlinks-light" color="#eeeeee">Special sections</font></a></td>
</tr>
</table>

<table width="140" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" height="1" width="1" alt=""></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#003366"><a href="/business/"><font id="navlinks-light" color="#eeeeee">Business</font></a></td>
</tr>
</table>

<table width="140" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" height="1" width="1" alt=""></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#003366">
<a href="/technology/"><font id="navlinks-light" color="#eeeeee">Technology</font></a><img width="14" height="12" src="/images/chinews/navpointer.gif" alt="You are here"></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#cccccc">

<font id="navlinks-dark"><a href="/technology/local/">Business technology</a></font><br>

<font id="navlinks-dark"><a href="/technology/reviews/">The Digital Page</a></font><br>

<font id="navlinks-dark"><a href="/classified/genmerch/search.jsp?class=7301-7313">Tech training</a></font><br>

<font id="navlinks-dark"><a href="/technology/local/profiles/">Trends</a></font><br>

<font id="navlinks-dark"><a href="/technology/columnists/">Columnists</a></font><br>

<font id="navlinks-dark-sub">&#149; <a href="/technology/columnists/chi-jamescoates.columnist">James Coates</a></font><br>

</td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#003366"><a href="http://chicagosports.com/"><font id="navlinks-light" Color="#eeeeee">Sports</font></a></td>
</tr>
</table>

<table width="140" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" height="1" width="1" alt=""></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#003366"><a href="/features/"><font id="navlinks-light" color="#eeeeee">Leisure</font></a><font id="navlinks-light" color="#eeeeee"><b>&nbsp;&nbsp;|&nbsp;&nbsp;</b></font><a href="/travel/"><font id="navlinks-light" color="#eeeeee">Travel</font></a></td>
</tr>
</table>

<table width="140" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" height="1" width="1" alt=""></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#003366"><a href="/register"><font id="navlinks-light" color="#eeeeee">Registration</font></a></td>
</tr>
</table>

<table width="140" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" height="1" width="1" alt=""></td>
</tr>
</table>

<table border="0" width="140" cellspacing="0" cellpadding="3">
<tr>
<td bgcolor="#990000"><a href="/services/site/chi-customerservice.htmlstory"><font id="navlinks-light" Color="#eeeeee">Customer service</font></a></td>
</tr>
</table>

<table width="140" border="0" cellpadding="0" cellspacing="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" height="1" width="1" alt=""></td>
</tr>
</table>





<table bgcolor="#cccccc" border="0" width="140" cellspacing="0" cellpadding="0">
<tr>
<td><br></td>
</tr>
<tr>
<td align="left"><img border="0" src="/images/chinews/lrcurvetop.gif" height="14" width="140" alt=""></td>
</tr>
<tr>
<td>
<table bgcolor="#ffffff" width="140" cellpadding="4" cellspacing="0" border="0">
<tr>
<td>
<font id="leftrail-header">Special reports</font>
<hr size="1" color="#666666">


	

<a href="/news/specials/united/chi-unitedfront,0,7479584.htmlstory?coll=chi-site-nav"><img src="http://images.chicagotribune.com/media/thumbnails/htmlstory/2003-07/8096051.jpg" alt="United's rhapsody of blues" border="0" width="132" height="94" /></a>

	
<font id="leftrail-link"><a href="/news/specials/united/chi-unitedfront,0,7479584.htmlstory?coll=chi-site-nav">United's rhapsody of blues</font></a><font id="line-spacer"><br><br></font>



	
<font id="leftrail-link"><a href="/news/specials/dp/chi-justicederailed-htmlstory,0,5471082.htmlstory?coll=chi-site-nav">Justice derailed</font></a><font id="line-spacer"><br><br></font>



	
<font id="leftrail-link"><a href="/news/specials/chi-columbiadisaster-specialpackage,0,6611641.special?coll=chi-site-nav">The Columbia disaster</font></a><font id="line-spacer"><br><br></font>



	
<font id="leftrail-link"><a href="/business/chi-021215glut,0,4809163.storygallery?coll=chi-site-nav">The economics of glut</font></a><font id="line-spacer"><br><br></font>


<font id="leftrail-link"><a href="/news/specials/"><I>All special reports</I></a><br><br></font>
<img src="/images/chinews/hrdouble.gif" height="14" width="132" alt=""><br>
<font id="leftrail-header"><font size="1"><br></font>Top technology stories</font><hr size="1" color="#666666">


<a href="/technology/la-fi-smarttag16sep16,0,867024.story?coll=chi-technology-hed"><font id="leftrail-link">Radio-tagged product codes on fast track</font></a><font size="1"><br><br></font>



<a href="/technology/chi-0309160392sep17,0,7383437.story?coll=chi-technology-hed"><font id="leftrail-link">Tips for decoding computer speak</font></a><font size="1"><br><br></font>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td><img border="0" src="/images/chinews/lrcurvebottom.gif" height="14" width="140" alt=""></td>
</tr>
<tr>
<td><br></td>
</tr>
</table>

</td>
<td width="1" bgcolor="#ffffff"><img src="/images/clear.gif" width="1" height="1" alt=""></td>
<td width="619" bgcolor="#ffffff" valign="top">

<table width="619" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="9"><img border="0" src="/images/clear.gif" width="9" height="1" alt=""></td>
<td valign="top" width="610">
<font id="line-spacer"><br></font>
<font id="text">




<font id="source-credit">From the Los Angeles Times<br></font>





<font id="headline">Note: This headline is patented</font><br>


<br>








<!-- BEGIN RELATED CONTENT RAIL -->




<table border="0" width="160" align="right" cellpadding="0" cellspacing="0">

<tr>
<td width="10"><img src="/images/clear.gif" width="10" height="1" alt=""></td>

<td width="5" bgcolor="#eeeeee"><img src="/images/clear.gif" width="5" height="1" alt=""></td>

<td width="140" bgcolor="#eeeeee">
<img src="/images/chinews/hrdouble.gif" height="14" width="140" alt=""><br>









<img src="/images/icons/email.gif" width="15" height="15"> <font id="rail-link"><a href="/technology/local/chi-0302,0,7320487,email.story" target="win_6537988" onclick="if (window.windoid) windoid('','win_6537988',310,540,'resizable=1,scrollbars=1')">E-mail this story</a></font><br>

<img src="/images/icons/printer.gif" width="15" height="15">
<font id="rail-link"><a href="/technology/local/chi-0302,0,6988243,print.story">Printer-friendly format</a></font><br>

<img src="/images/icons/archives.gif" width="15" height="15">
<font id="rail-link"><a href="http://pqasb.pqarchiver.com/chicagotribune/">Search archives</a></font><br>
<font id="line-spacer"><br></font>
	<font id="utility-title">Protecting intellectual property</font><br><img src="/images/clear.gif" width="140" height="4" alt=""><br>

<font id="rail-text">

<A HREF="/technology/local/chi-iproperty-gallery,1,6971446.storygallery">Find out</A>:<BR>
&#149; How copyright affects your right to record digital content<BR>
&#149; The problems with piracy<BR> 
&#149; How small companies should approach negotiations with large companies.<BR>


<font id="line-spacer"><br><br></font>
</font>




</td>

<td width="5" bgcolor="#eeeeee"><img src="/images/clear.gif" width="5" height="1" alt=""></td>
</tr>
</table>

<!-- END RELATED CONTENT RAIL -->



	<font id="byline">By David Streitfeld<br></font>


	<font id="titleline">Times Staff Writer<br></font>



	<font id="date">Published February 7, 2003</font><br>

<br>

<font id="text">






For decades, finicky children have been eating peanut butter and jelly sandwiches with the crust removed. From a legal point of view, however, the lunchbox staple was invented on a patio in Fargo, N.D., in 1995.<br>
<br>
David Geske, who ran a packaged ice business, was entertaining his friend Len Kretchman, a consultant. For lunch, their kids wanted peanut butter and jelly with the bread trimmed and folded over. As they were preparing the meal, Kristen Geske and Emily Kretchman told their husbands: "You guys should make a sandwich with no crust."<br>
<br><table align="left" cellspacing="0" cellpadding="0" border="0">
<tr>
<td><table cellpadding="4" bgcolor="#666666" border="0" cellspacing="0">
<tr>
<td><!-- Blank placeholder ad --></td>
</tr>
</table>
</td>
<td width="5"><img src="/images/clear.gif" width="5" height="5" alt=""></td>
</tr>
<tr>
<td colspan="2"><img src="/images/clear.gif" width="5" height="5" alt=""></td>
</tr>
</table>
That offhand comment spawned Incredible Uncrustables, a sandwich the two entrepreneurs mass-produced for Midwestern schools. It also began a long-running dispute over whether the U.S. Patent and Trademark Office went too far when it gave Geske and Kretchman the first patent on a mundane household sandwich.<br>
<br>
 "This doesn't mean your grandmother can't make you a peanut butter and jelly sandwich," said Ann Harlan, a lawyer for J.M. Smucker Co., which now owns Geske and Kretchman's company.<br>
<br>
But it does mean that Smucker will try to prevent other companies from making them. For more than two years, Smucker has been arguing in court and the patent office that a crustless peanut butter and jelly sandwich made by Albie's Foods Inc. is violating its patent and must be taken off the market.<br>
<br>
"They're misusing the patent system," said Albie's lawyer Kevin Heinl. "It's outrageous."<br>
<br>
A generation ago, Smucker's sandwich, which looks like a flying saucer, and Albie's, which is a fat square, would have fought it out in the marketplace. The best sandwich would win.<br>
<br>
Now the corporate urge is to get a patent to stifle competition. It's a process being helped along by the courts and Congress, which keep broadening the nature of what is patentable while limiting the patent office's ability to reject an application on the grounds of common sense.<br>
<br>
Meanwhile, the system as a whole is breaking down. Patent applications are increasing in complexity and length, but the 3,500 examiners still are evaluated by how many they approve. The inevitable consequence, says one former examiner: "The path of least resistance is saying yes." Three-quarters of applications get approved.<br>
<br>
Two former heads of the patent office have described the agency as sitting "on the edge of an abyss."<br>
<br>
"Crisis is a strong word," the American Intellectual Property Law Assn. has noted in correspondence, "but we believe that it aptly describes the situation."<br>
<br>
James E. Rogan, the former Republican congressman from Glendale who became director of the patent office in December 2001, agrees with all but the most strident critics.<br>
<br>
"This is an agency in crisis, and it's going to get worse if we don't change our dynamic," Rogan said. "It doesn't do me any good to pretend there's not a problem when there is."<br>
<br>
Beyond the plight of an antiquated government bureaucracy overseeing a field that is undergoing explosive growth, there are deeper questions about the fundamental role of patents.<br>
<br>
They played a key role in the technology boom of the last 25 years. Companies licensed their innovations to others, who in turn used them as springboards for new inventions.<br>
<br>
Yet there's a point where patents impede innovation. It can cost more to check whether a software program infringes on previously patented programs than it cost to write the program in the first place.<br>
<br>
Since patents tend to be complex, infringement can be determined only by a professional. That's one reason the number of intellectual property lawyers has quadrupled since 1985. During the same time, the number of court cases involving intellectual property has doubled.<br>
<br>
Technology companies, in particular, spend massive amounts of time and money either suing over patents or being sued. Research in Motion Ltd., maker of the popular BlackBerry hand-held e-mail device, sued competitors for alleged patent violations, gaining licensing fees. Then the company itself was sued for infringement. A private holding company called NTP Inc. said Research in Motion was violating its patents on wireless e-mail.<br>
<br>
Research in Motion lost the case, recording a $32-million charge for litigation and related expenses. NTP is seeking an injunction to prevent the company from selling BlackBerrys. Meanwhile, the patent office is reviewing whether it should have granted the NTP patents in the first place.<br>
<br>
"Developing software is like crossing a minefield: With each design decision, you might step on a patent that will blow up your project," said Richard Stallman, an advocate of free software. "A modern program combines hundreds of ideas, so be prepared for a long stroll among the mines."<br>
<br>
The system was never supposed to be so combative. Patents, which last for 20 years, are enshrined in the Constitution as a means of promoting creativity and encouraging progress by rewarding inventors.<br>
<br>
For a long time, the scope of patents was sharply limited and easily understood. Ideas and natural phenomena were not patentable. Machines and industrial processes were -- provided they were both new and useful.<br>
<br>
In 1880, Supreme Court Justice Noah Swayne added a third requirement: A patentable invention, he wrote, should be inspired by "a flash of genius."<br>
<br>
This put a high bar on patentability, and through the decades the courts raised it. In 1950, Justice William O. Douglas wrote, "The Constitution never sanctioned the patenting of gadgets. Patents serve a higher end -- the advancement of science."<br>
<br>
Inventors and patent-seeking corporations didn't like that. Two years later, Congress removed the "flash of genius" standard and replaced it with a vaguer requirement of "non- obviousness."<br>
<br>
That began to loosen the patent floodgates. In 1980, the Supreme Court said life, in the form of genetically engineered bacteria, was patentable. The decision gave birth to the modern biotech industry.<br>
<br>
A Case for Licensing<br>
<br>
Five years ago, the patent court, the U.S. Court of Appeals for the Federal Circuit, took the increasingly blurry line between what was patentable and what wasn't and erased it.<br>
<br>
At issue was a patent held by the Signature Financial Group Inc. for a system that channeled money from mutual funds into a central investment pool.<br>
<br>
Under existing law, two things should have invalidated this patent.<br>
<br>
First, it was a method of doing business. Previous courts had always held that business methods, like ideas or laws of nature, were not something one could patent. After all, companies already had plenty of incentive to improve their business techniques. If they didn't, they'd lose out to the competition.<br>
<br>
But Signature's system wasn't only a method of doing business. It also was a mathematical process using algorithms.<br>
<br>
An algorithm is a set of instructions for doing things in a certain order. And if a business plan, like "sell quick, cheap food close to major highways," seems like an idea that can't be patented, then an algorithm had seemed doubly so. Like all forms of math, it had been considered part of the realm of ideas -- as unpatentable as E = mc2.<br>
<br>
This time, however, the court said that because Signature's algorithms produced a useful, concrete and tangible result, it could be patented. As for the long-standing exception for business methods, the court found it "ill-conceived."<br>
<br>
The ruling amazed intellectual property experts.<br>
<br>
"What the Signature system was doing was accounting. It was dividing numbers by other numbers," said Duke University law professor James Boyle.<br>
<br>
The number of business-method applications, many of which involved algorithms, rose sevenfold between 1998 and 2001. One patent that quickly became notorious was given to IBM Corp. for a "system and method for providing reservations for restroom use" on airplanes. The method: first come, first served.<br>
<br>
For Boyle, we've reached a point where we're "tremblingly close" to patenting ideas.<br>
<br>
"You're no longer patenting the corkscrew," he said. "You're patenting the idea of taking the cork out of the bottle so you can drink the wine."<br>
<br>
Or, in the case of Patent No. 6,004,596, the idea of the peanut butter and jelly sandwich.<br>
<br>
Products Come Together<br>
<br>
Jelly has been around for centuries, commercial peanut butter from 1890 and machine-sliced bread since the late 1920s. A decade later, some unknown genius combined all three ingredients to make the quintessential American sandwich.<br>
<br>
PB&amp;Js hit it big in the post-war years. The sandwiches weren't too messy, they didn't spoil after a couple of hours in a lunchbox, and they encouraged the consumption of milk.<br>
<br>
A PB&amp;J is pretty simple, which didn't stop food companies from trying to make it simpler. One such innovation was premixed peanut butter and jelly, reducing the number of ingredients from three to two. Then Geske and Kretchman came up with the notion of prefabricating the whole thing.<br>
<br>
They made a good pair. Geske had been looking for something to do in the winter, when demand for his packaged ice dropped, and Kretchman had some background in selling food to schools. They developed the sandwich at home and then did taste tests at schools.<br>
<br>
Incredible Uncrustables was an immediate hit. Not only was the product nutritional and appealing, but it also eliminated the need for the schools to spend time making sandwiches themselves.<br>
<br>
An intellectual property attorney helped secure a trademark on the name. The patent came about more casually.<br>
<br>
"One attorney said, 'There's nothing here,' and we said OK," Geske recalled. "But a new attorney came in, and he said, 'We can get this through, no problem.' We gave them their fees. It took about a year and a half. "<br>
<br>
By the end of 1998, about 50 employees in Fargo were making 35,000 Incredible Uncrustables a day for schoolchildren in eight Midwestern states.<br>
<br>
Smucker, the Orrville, Ohio, maker of jams and jellies, realized the sandwich could be a valuable addition to its product line. Smucker bought the company and shortened the name to Uncrustables.<br>
<br>
The company also got the patent, which was granted Dec. 21, 1999, for a "sealed crustless sandwich."<br>
<br>
"On what basis they granted it, I have no idea," said Geske, who made enough from the sale to Smucker to "take a couple of years off to enjoy the family."<br>
<br>
The defendant in the sandwich lawsuit, Albie's, was founded in 1987 by two childhood buddies to sell pasties, which are meat or vegetables baked in dough. In the summer of 2000, the Gaylord, Mich.-based company began selling a peanut butter and jelly sandwich called E.Z. Jammers. It weighed 2.8 ounces, bigger than Smucker's 2-ounce product.<br>
<br>
By December, Smucker noticed the E.Z. Jammers and demanded that Albie's stop. Albie's sued to have the patent declared invalid. Smucker then sued Albie's for infringement.<br>
<br>
To avoid the expense of a full-blown suit, Albie's asked the patent office for a reexamination, a relatively rare procedure. The best way to get a patent thrown out is by finding examples of so-called prior art -- proof that the patent really didn't offer anything new.<br>
<br>
One such piece of evidence suggested by Albie's was a kitchen tool called the Cut-N-Seal. This plunger-type device allowed an individual to seal and crimp a filling between two slices of bread.<br>
<br>
To knock down the Cut-N-Seal, a Smucker lawyer filed an affidavit describing how he had tried to use it to make a sandwich that looked as sleek and tidy as an Uncrustable.<br>
<br>
The Cut-N-Seal sandwiches, the lawyer said, all had "rupturing problems," particularly in the "upper bread layer." The accompanying photographs showed jelly bursting out all over.<br>
<br>
"So what?" said Heinl, the Albie's lawyer. "Anyone can make a defective sandwich."<br>
<br>
Although the arguments were narrow, the business implications were large.<br>
<br>
"The Uncrustables brand sandwich defines its own market," Smucker said in an affidavit arguing that anything so immediately popular had to be non-obvious and therefore patentable. With sales of 50 million sandwiches a year, it was the firm's fastest-growing product.<br>
<br>
The point of the lawsuit was to keep it that way, Heinl said, noting: "They were filing suit to keep Albie's out of the market."<br>
<br>
Smucker, which recently solidified its hold on the peanut butter market by buying the Jif brand, is confident the reexamination will help Uncrustables.<br>
<br>
"The claims will be narrowed," said attorney Robert Vickers, "but the patent will be a lot stronger."<br>
<br>
Patent Office Challenges<br>
<br>
Patent officials decline to talk about specific patents, although they note that the most controversial come under review or, like the IBM bathroom patent, are quietly dropped by their owners.<br>
<br>
"We grant 170,000 patents a year," said Esther Kepplinger, deputy commissioner of patent operations. "To focus on five, and extrapolate that the overall quality is poor, is unfair."<br>
<br>
Yet the agency's director, Rogan, acknowledges that the problems have run deeper.<br>
<br>
"Some of the early business-method patents were fairly broad," he said. These led to conflicting claims and lawsuits. "We're much narrower now." And tougher: "We've gone from a 75% acceptance rate to a 75% rejection rate" on those patents. Of course, those early, broad patents are still out there, wreaking havoc.<br>
<br>
Rogan hopes to hire 2,500 additional examiners, but his strategic plan to solve the patent crisis also includes outsourcing some of the basic patent search work, saving time and labor. He wants to encourage electronic applications, rescuing a patent office he says is "drowning in paper," and charge applicants higher fees for bigger applications, a move intended to reduce unnecessary patent claims.<br>
<br>
Although no patent director has been so ambitious, smaller reforms have failed before. The patent office first promised a paperless office in 1983.<br>
<br>
Outside critics believe the problems are more intractable.<br>
<br>
In a recent speech, Judge Richard Posner of the U.S. 7th Circuit Court of Appeals in Chicago said a large part of the recent jump in applications "is defensive or strategic patenting."<br>
<br>
"You get a patent because &#91;otherwise&#93; someone else will patent it," he said. "Or you get a patent because you would like to block a competitor."<br>
<br>
The walls protecting this ever-expanding pool of intellectual property are getting stronger. One reason is the 1982 creation of the Court of Appeals for the Federal Circuit. It handles only patent cases -- and usually rules in favor of the patent holder.<br>
<br>
"A specialized court tends to see itself, I think, as a booster of the specialty industry," Posner said.<br>
<br>
Last year, the appeals court said the patent office had incorrectly rejected two applications for "obviousness." If an examiner rejects an application using "general knowledge," the court said, that knowledge "must be articulated and placed on the record."<br>
<br>
In other words, said deputy commissioner Kepplinger, "we can't reject something just because it's stupid."<br>
<br>
The absence of sense seems to figure strongly in the most famous patent of the last few years. Last spring, the patent office issued a patent to Steven Olsen for "a new and improved method of swinging": pulling "first on one chain and then on the other."<br>
<br>
Even if you didn't know Steven was 7 years old, the brief and elegantly written application is obviously tongue in cheek, complete with a grand pronouncement that this "invention" will enable "even young users to swing independently and joyously, which is of great benefit to all."<br>
<br>
Steven's father, Peter Olson, a patent attorney with 3M Co. who wrote the application, was merely trying to show his son what he did for a living. But the examiner didn't get the joke.<br>
<br>
The patent office is searching for documented proof that children have indeed always powered their swinging by pulling on the chains. Then, and only then, will it kill the patent as quietly as possible.<br>
<br>
*<br>
<br>
Next: Patent holders battle for control of the Internet.

</font>


	<p>
	<font id="copyright">Copyright &copy; 2003, <a href="http://www.latimes.com" target="_blank">The Los Angeles Times</a></font>
	</p>

<font size="1"><br></font>





</td>
</tr>
</table>
</td>
</tr>
</table>





<!-- START FOOTER -->

<table width="760" border="0" cellspacing="0" cellpadding="0">
<tr>
<td bgcolor="#ffffff"><img src="/images/clear.gif" width="1" height="1" alt=""></td>
</tr>


<tr>
<td bgcolor="#cccccc">
<table width="760" cellpadding="4" cellspacing="0" border="0">
<tr>
<td align="center"><font id="footer-links"><a href="/"><b>Home</b></a><b>&nbsp;|&nbsp;</b><a href="/services/site/chi-copyright.htmlstory"><b>Copyright and terms of service</b></a><b>&nbsp;|&nbsp;</b><a href="/services/site/chi-privacy.story"><b>Privacy policy</b></a><b>&nbsp;|&nbsp;</b><a href="/subscribe"><b>Subscribe</b></a><b>&nbsp;|&nbsp;</b><a href="/services/site/chi-customerservice.htmlstory"><b>Customer service</b></a><b>&nbsp;|&nbsp;</b><a href="/archives"><b>Archives</b></a><b>&nbsp;|&nbsp;</b>
<a href="http://www.chicagotribune.com/extras/advertise/"><b>Advertise</b></a></td>
</tr>


<script language="JavaScript">
 var st_v=1.0; var st_pg=""; var st_ci="703";
 var st_di="d004"; var st_dd="st.sageanalyst.net";
 var st_tai="v:1.2.1";
 var st_ai="";
</script>
<script language="JavaScript1.1"><!--
 st_v=1.1;
//--></script>
<script language="JavaScript1.2"><!--
 st_v=1.2;
//--></script>
<script language="JavaScript1.1" src="//st.sageanalyst.net/tag-703.js">
</script>
<script language="JavaScript">
 if (st_v==1.0) {
  var st_uj;
  var st_dn = (new Date()).getTime();
  var st_rf = escape(document.referrer);
  st_uj = "//"+st_dd+"/"+st_dn+"/JS?ci="+st_ci+"&di="+st_di+
  "&pg="+st_pg+"&rf="+st_rf+"&jv="+st_v+"&tai="+st_tai+"&ai="+st_ai;
  var iXz = new Image();
  iXz.src = st_uj;
 }
</script>
<noscript><img src="//st.sageanalyst.net/NS?ci=703&di=d004&pg=&ai="></noscript>
</table>
</td>
</tr>
<tr>
<td bgcolor="#cccccc"><table width="760" cellpadding="0" cellspacing="0" border="0"><tr><td></td></tr></table></td>
</tr>
<tr>
<td>
<table width="760" cellpadding="6" cellspacing="0" border="0">
<tr><td bgcolor="#003366" align="center"><!-- Ad Space: html.ng/site=chicagotribune&color=none&edition=newspaper&content=technology&channel=local&adtype=banner&adsize=468x60&adplacement=bottom&tag=std -->

<!-- Blank placeholder ad -->

<!-- /Ad Space: html.ng/site=chicagotribune&color=none&edition=newspaper&content=technology&channel=local&adtype=banner&adsize=468x60&adplacement=bottom&tag=std -->
</td>
</tr>
</table>

</td>
</tr>
</table>

<!-- END FOOTER -->

<script language="JavaScript">Tacoda_AMS_DDC_js=1.0</script>
<script language="JavaScript1.1">Tacoda_AMS_DDC_js=1.1</script>
<script language="JavaScript1.2">Tacoda_AMS_DDC_js=1.2</script>
<script language="JavaScript1.3">Tacoda_AMS_DDC_js=1.3</script>
<script language="JavaScript">
Tacoda_AMS_DDC_addPair('rcid','');
Tacoda_AMS_DDC("http://te.chicagotribune.com/blank.gif",Tacoda_AMS_DDC_js);
</script>
<noscript><img src="http://te.chicagotribune.com/blank.gif" border="0" width="1" height="1"></noscript>
</body>
</html>





