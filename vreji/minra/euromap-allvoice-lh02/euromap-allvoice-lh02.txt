
   [1]Euromap - your path to market for speech and language technologies


     [2]The gateway to speech and language technology opportunities 
                           [3]European network of excellence in HLT 
   [4]EC Programme fostering digital content on the global networks 
   [5]Euromap[6] Home [7]News [8]Members [9]Sitemap [10]Search 
   [11]Euromap

    [12]Articles
    [13]Mission
    [14]Team 
    [15]Events 
    [16]Success Stories 
    [17]Newsletters 
    [18]eBusiness 
    [19]Tourism 
    [20]Tech Transfer 
    [21]Reports 
    [22]HLT Policy Pages
    [23]CEEC Info Centre
    [24]Who's Who
    [25]HLT Projects

Euromap News

   Speech technology: [26]AllVoice wins �400,000 on patent claim
                                                     11.01.2002
   A tiny Devon-based software company has received settlement of a
   two-year-old patent claim against a former speech technology giant.
   AllVoice said it finally won its claim against Lernout & Hauspie, once
   the world's leading speech technology company and now bankrupt, for
   about �400,000 ($575,959). The British company alleged that the larger
   rival had stolen some of its technology, which helps computers to
   recognise human speech, and used it in several products.

   [27]Print version   Full text search:   ____________________   Search
     _________________________________________________________________

   Please report problems to [28]hltteam (.at.) HLTCentral.org

Verweise

   Sichtbare Links
   1. LYNXIMGMAP:file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/euromap-allvoice02/euromap-allvoice02.html#Map1
   2. http://www.hltcentral.org/
   3. http://www.elsnet.org/
   4. http://www.hltcentral.org/page-604.shtml
   5. LYNXIMGMAP:file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/euromap-allvoice02/euromap-allvoice02.html#Map1a
   6. http://www.hltcentral.org/euromap/
   7. http://www.hltcentral.org/euromap/news/
   8. http://www.hltcentral.org/page-61.shtml
   9. http://www.hltcentral.org/euromap/sitemap.html
  10. http://www.hltcentral.org/page-774.shtml
  11. LYNXIMGMAP:file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/euromap-allvoice02/euromap-allvoice02.html#Map1b
  12. file://localhost/page-960.shtml
  13. file://localhost/page-176.shtml
  14. file://localhost/page-59.shtml
  15. file://localhost/page-639.shtml
  16. file://localhost/page-333.shtml
  17. file://localhost/page-900.shtml
  18. file://localhost/page-658.shtml
  19. file://localhost/page-566.shtml
  20. file://localhost/page-337.shtml
  21. file://localhost/page-243.shtml
  22. file://localhost/page-1078.shtml
  23. file://localhost/page-1072.shtml
  24. http://www.hltcentral.org/cdb/
  25. http://www.hltcentral.org/projects/
  26. http://news.ft.com/ft/gx.cgi/ftc?pagename=View&c=Article&cid=FT3VSQWZAWC&live=true&tagid=ZZZC00L1B0C&Collid=ZZZ96PECC0C
  27. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/euromap-allvoice02/index.php?id=2372&print=1
  28. mailto:hltteam (.at.) HLTCentral.org

   Versteckte Links:
  29. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/euromap-allvoice02/index.php?id=2372&print=1
