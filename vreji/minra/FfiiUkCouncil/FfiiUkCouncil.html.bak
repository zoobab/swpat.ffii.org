<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
  <HEAD>
    <TITLE>Foundation for a Free Information Infrastructure in the UK</TITLE>
    <META content=FFII name=author>
    <META content=2003/09/24 name=review>
    <META content="Foundation for a Free Information Infrastructure in the UK" name=title>
    <META
      content="The Foundation for a Free Information Infrastructure (FFII) is a non-profit association registered in Munich, which is dedicated to the spread of data processing literacy.  FFII supports the development of public information goods based on copyright, free competition, open standards.  FFII has currently more than 200 individual members, 60 corporate members and 4200 supporters who have entrusted the FFII to act as their voice in public policy questions in the area of software property law."
      name=description>

  <link rel="stylesheet" type="text/css" href="mainstyle.css" />

  </HEAD>

  <STYLE>
// <!--
     SPAN.MEP {
       font-weight:bold
     }
//   -->
  </STYLE>


  <BODY text=#004010 vLink=#660000 aLink=#006600 link=#0000cc bgColor=#f4fef8>
<!--    <A HREF="http://kwiki.ffii.org/ffii-uk_index"><IMG SRC="images/groupedit.png" ALT="Wiki" BORDER=0 ALIGN=LEFT></A> -->
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
	<input type="hidden" name="cmd" value="_xclick">
	<input type="hidden" name="business" value="paypal@ffii.org">
	<input type="hidden" name="item_name" value="FFII UK">
	<input type="hidden" name="no_note" value="1">
	<input type="hidden" name="currency_code" value="GBP">
	<input type="hidden" name="tax" value="0">
	<input type="image" src="https://www.paypal.com/images/x-click-but21.gif" border="0" name="submit" alt="Make a donation with PayPal - it's fast, free and secure!">
    </form>

    <H1>
      <A HREF="http://www.ffii.org/"><IMG SRC="images/ffii.png" BORDER=0 ALIGN=Left></A>
      Foundation for a Free Information Infrastructure UK
      <IMG SRC="images/uk_flag.png" ALIGN=Right>
    </H1>

    <H2>Lobbying the Council of Europe</H2>

    <em>The European Parliament has voted to restrict software patenting.  But unless UK government ministers can be convinced otherwise by November 10th, they are expected to try to set the democratic parliament decision aside</em>

    <h2>In detail</h2>
    <P>On September 23rd, in a marathon of 78 different votes, the EU parliament voted for real limits on the patentability of computer software. <A HREF="http://lwn.net/Articles/50722/">The resulting directive</A></P>

    <P>MEP Sir Neil MacCormick (SNP), <A HREF="http://www3.europarl.eu.int/omk/omnsapir.so/cre?FILE=0923ma&LANGUE=EN&LEVEL=DOC&NUMINT=2-038&LEG=L5">in this speech in the European Parliament</A>, paid tribute to the input he had had from constituents:
    <BLOCKQUOTE>"We have been lobbied heavily about this, but we have been lobbied heavily because we have very many able, hardworking constituents who see the leakage [into allowing software patents] as a threat to their livelihood. Let us be absolutely sure that what we send back from this debate and from tomorrow's vote really does provide a completely watertight seal... against that danger, which so many of us have been taught to apprehend by our constituents".</BLOCKQUOTE>

    <P>This week, the parliamentary pressure was re-inforced when <A HREF="http://swpat.ffii.org/news/03/peti1001/index.en.html">the petitions committee agreed to accept the Eurolinux petition from over 250,000 EU Citizens concerned about software patents</A>.</P>

    <P>HOWEVER, there is a problem, and the campaign against software patents needs your help <STRONG>now</STRONG>.</P>

    <P>The European Parliament is only one leg of the EU decision making process.</P>

    <P>On November 10th the Council of Ministers from the EU member states will meet in Brussels to consider the issue.</P>

    <P>The UK ministers at the DTI are in thrall to the UK patent office view that software patents == innovation == growth, as can be seen from <A HREF="hewitt.html">this letter from Patricia Hewitt</A>.</P>

    <P>The patent industry is already campaigning that the issue is "too technical" for MEPs, and should be left to national patent offices to co-ordinate a revision of the EPC, as explained in     <A HREF="http://news.zdnet.co.uk/business/legal/0,39020651,39116709,00.htm">this article</A>.</P>

    <P>The UK patent office even has such an amendment ready to roll, which would rubber-stamp the existing disastrous practice.</P>

    <P>Unless Government ministers can be convinced by November 10th to overrule the advice they are getting from the patent office, it is therefore likely that on that date the UK will vote to annul all the Parliament's good amendments, and may even veto the directive altogether.</P>

    <P>We therefore need to write to your MP, and to ask them to bring to Government ministers' attention three things:</P>

    <OL>
      <LI>The breadth of software patents now being granted.</LI>
      <DD>Software patents are currently being granted not only to improved computerised devices (following the decision in "Koch and Sterzel", which the EU Parliament would uphold), but also to generic pure software (following the decision in "Vicom"). For further details on what is being patented see <A HREF="http://swpat.ffii.org/patents/samples/index.en.html">http://swpat.ffii.org/patents/samples/index.en.html</A>.</DD>

      <LI>The damage that these are causing.</LI>
      <BLOCKQUOTE>"Software patents are like landmines for programmers. At each design decision, there is a chance you will step on a patent and it will destroy your project. Considering the large number of ideas that must be combined in a modern program, the danger becomes very large." -- Richard Stallman</BLOCKQUOTE>
      <DD>See <A HREF="http://swpat.ffii.org/patents/effects/">http://swpat.ffii.org/patents/effects/</A> for examples of projects which have been killed or hindered by software patents. Many economists as well as programmers are concerned about the likely bad effects of software patents: see <A HREF="http://www.researchineurope.org">Research in Europe</A>.</DD>

      <LI>The unacceptability of leaving the patent industry to be judge and jury over what is patentable.</LI>
      <DD>Expecting the patent office to campaign for a reduction in what can be patented is like asking turkeys to vote for Christmas.
    </OL>

    <h2>What you can do</h2>

    <UL>
      <LI>You are an SME</LI>
      <DD>tell your MP how a rush to software patents would affect your business.  Be sure to mention your turnover and number of employees -- real money and real jobs are at stake.

      <LI>You are a political party member or a trades unionist</LI>
      <DD>try to have your local branch consider the issue.  Encourage branch members also to write to MPs.

      <LI>You are a user of software.</LI>
      <DD>tell your MP that you do not want to see monopolisation and loss of choice.

      <LI>You want to keep in touch with the campaign daily</LI>
      <DD>join the ffii-uk list at ffii.org.uk
    </UL>

    <h2>Addresses to write to</h2>
    <H3>Ministers</H3>
    <P>The minister responsible for the IT industry in the UK is believed to be <A HREF="http://www.dti.gov.uk/ministers/ministers/timms.html" TARGET=_new>Stephen Timms MP</A> at the <A HREF="http://www.dti.gov.uk/">Department of Trade and Industry (DTI)</A>.</P>

    <P>His boss, the secretary of state at the DTI, is <A HREF="http://www.dti.gov.uk/ministers/ministers/hewitt.html" TARGET=_new>Patricia Hewitt</A></P>

    <P>They <STRONG>can</STRONG> be contacted at the DTI, 1 Victoria Street, London SW1H OET...</P>


    <P><STRONG>BUT</STRONG> the convention is that people who write to the minister directly will receive a standard civil service reply, without the minister ever seeing it.</P>

    <P>Instead it is much more effective if the letter is sent to the local constituency MP, who is then asked to "forward this concern to the appropriate minister at the DTI".  By convention the constituent should then get a reply personally signed by Mr Timms, with a covering letter from the MP.</P>

    <P>Addresses for local MPs can be found by typing your postcode into <A HREF="http://www.faxyourmp.com/" TARGET=_new>www.faxyourmp.com</A> which also provides a free email-to-fax gateway. However, it would probably be more effective to write by snail-mail. You can also write to your MP at <ADDRESS>Name of MP<BR>
    House Of Commons<BR>
    London<BR>
    SW1A 0AA</ADDRESS>
    You should only write to your own local MP.</P>

    <P>MPs' email addresses are of the form <TT>jdoe@parliament.uk</TT> for (fictitious) MP John Doe. However, not all MPs read their emails.</P>

    <H3>Civil Servants</H3>
    <P>It is unusual for civil servants to be lobbied directly; however, the UK Patent Office is <A HREF="http://www.patent.gov.uk/about/ippd/issues/softpat.htm" TARGET=_new>seeking consulatations</A>.</P>

    <P>The relevant individual to contact is:


    <ADDRESS>
    Richard Mulcahy<BR>
    Intellectual Property & Innovation Directorate<BR>
    The Patent Office<BR>
    Concept House<BR>
    Cardiff Road<BR>
    Newport<BR>
    NP10 8QQ<BR><BR>


    Tel: +44 (0)1633 814767<BR>
    Fax: +44 (0)1633 814922<BR>
    E-Mail: <A HREF="richard.mulcahy@patent.gov.uk">richard.mulcahy@patent.gov.uk</A>
    </ADDRESS>

    The actual individuals from the patent office making the case in Brussels will probably be either Richard Mulcahy's boss Graham Jenkins, the head of the Intellectual Property Policy directorate; or Peter Hayward, head of the Patent Office Electrical Division.</P>

    <P>It may be worth asking for any submission you send to the Patent Office to be added to any online pages they are running on the issue.</P>

    <H3>Timetable.</H3>
    <P>We believe that the issue may be due to be discussed in Brussels on the 10th November.</P>

    <P>However, it would be good if letters were already arriving when the UK Parliament resumes sitting on October 10th, so that the minister is already aware of concern about the issue if he is asked to answer any Parliamentary Questions on this subject.</P>

    Last updated by Alex Macfie, 3 October 2003.
  </BODY>
</HTML>
