


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<!-- --------------------------------------------------------------------------------
/ww/de/7_pub/aktuelles.cfm
Aktuelles
Last Update: 24.11.2003 15:12 (#161) - Date created: 14.09.2003
(c) CONTENS Software GmbH
CONTENS 2.5
© 1999-2003 CONTENS Software GmbH
--------------------------------------------------------------------------------- -->
<title>Aktuelles</title>
<meta name="description" content="Aktuelles">
<meta name="keywords" content="Aktuelles">
<meta name="copyright" content="(c) CONTENS Software GmbH">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


<!-- Browsercheck and dynamic style -->

	<link rel="STYLESHEET" type="text/css" href="http://www.dmmv.de/shared/stylesheets/dmmv_ie.css">
	<link rel="STYLESHEET" type="text/css" href="http://www.dmmv.de/shared/stylesheets/appstyle_ie.css">

<!-- /Browsercheck and dynamic style -->
</HEAD>
<BODY bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<img src=http://sitestat2.nedstat.nl/cgi-bin/dmmv/sitestat.gif?name= width=1 height=1 alt="" align=right>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="176" height="131" valign="top">
<p><a href="http://www.dmmv.de" target="_self"><img src="../../../shared/siteimg/redesign/logo_dmmv.jpg" width="176" height="131" border="0"></a></p>
</td>
<td>
<table width="587" height="131" border="0" cellpadding="0" cellspacing="0">
<tr>
<td height="102" valign="top">
<table width="587" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="../../../shared/siteimg/redesign/header_up.jpg" width="587" height="76"></td>
</tr>
<tr>
<td>
<table width="587" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="267" height="26" valign="top" nowrap><img src="../../../shared/siteimg/redesign/header_left.jpg" width="267" height="26"></td>
<td height="26" bgcolor="#FEED00">
<table width="320" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="46" height="26" nowrap>
<p><a href="../../../ww/de/7_pub/suche.cfm"><img src="../../../shared/siteimg/redesign/button_suche.jpg" width="46" height="26" border="0"></a></p>
</td>
<td class="titel" width="121" height="26" nowrap>
<!-- searchform -->
<form style="display:inline" name="searchform" method="get" action="../../../ww/de/7_pub/suche.cfm">
<input type="hidden" name="deutsch" value="2">
<input type="hidden" name="bool" value="or">
<input type="hidden" name="near" value="">
<input type="hidden" name="intern" value="0">
<input type="hidden" name="itemsperpage" value="10">
<input type="hidden" name="fuseaction_sea" value="results">
<input type="hidden" name="newsearch" value="1">
<input name="criteria" type="text" class="cmformsmall" id="suche" size="17">
</form>
</td>
<td width="23" height="26" nowrap><img src="../../../shared/siteimg/redesign/button_orange_go.jpg" width="23" height="26" onclick="if(document.searchform.criteria.value!='')document.searchform.submit()"></td>
<td width="130" height="26" nowrap>
<p><img src="../../../shared/siteimg/redesign/trenner_orange.jpg" width="22" height="26"><a href="../../../ww/de/7_pub/kontakt.cfm"><img src="../../../shared/siteimg/redesign/button_kontakt.jpg" width="37" height="26" border="0"></a><img src="../../../shared/siteimg/redesign/trenner_orange_2.jpg" width="20" height="26"><a href="../../../ww/de/7_pub/sitemap.cfm"><img src="../../../shared/siteimg/redesign/button_sitemap.jpg" width="51" height="26" border="0"></a></p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="587" height="19">
<table width="587" height="19" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="16"><img src="../../../shared/siteimg/redesign/leer.gif" width="16" height="11"></td>
<td>
<a href="http://www.dmmv.de/ww/de/7_pub/aktuelles.cfm"
>Startseite</a>
<img src="../../../shared/siteimg/redesign/text_pfeil.jpg" width="7" height="7">
<a href="http://www.dmmv.de/ww/de/7_pub/aktuelles.cfm"
>Aktuelles</a>
</td>
<td class=txtheadergrey>
<div align="right"><script language="Javascript">
<!--
today = new Date();
day = today.getDay();
date = today.getDate();
month = today.getMonth()+1;
year = today.getYear();
if (day == 0) dayName = "Sonntag"
else if (day == 1) dayName = "Montag"
else if (day == 2) dayName = "Dienstag"
else if (day == 3) dayName = "Mittwoch"
else if (day == 4) dayName = "Donnerstag"
else if (day == 5) dayName = "Freitag"
else dayName = "Samstag"
if (year < 2001) year = year + 1900;
if (month < 10) month = "0" + month;
if (date < 10) date = "0" + date;
document.write(dayName + ", " + date + "." + month + "." + year);
//-->
</script>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table width="763" height="2" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="176"><img src="../../../shared/siteimg/redesign/leer.gif" width="176" height="2"></td>
<td width="16"><img src="../../../shared/siteimg/redesign/leer.gif" width="16" height="2"></td>
<td width="355"><img src="../../../shared/siteimg/redesign/line_long.gif" width="355" height="1"></td>
<td width="20"><img src="../../../shared/siteimg/redesign/leer.gif" width="20" height="1"></td>
<td><img src="../../../shared/siteimg/redesign/line_small.gif" width="195" height="1"></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="176" valign="top">
<table class="bg08" width="176" height="70" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="20"><img src="../../../shared/siteimg/redesign/leer.gif" width="10" height="5"></td>
<td>
<table width="156" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>




	
	<form name="loginform" style="display:inline" action="http://www.dmmv.de/apps/memberlogin/index.cfm" method="post">
	  <input type="Hidden" name="editorgroupname" value="siteusers">
	  <input type="Hidden" name="loginfailedlink" value="/ww/de/7_pub/login.cfm">
	
	  
	  
	  
	  
	  
	  <input type="Hidden" name="ref" value="/ww/de/7_pub/content6741.cfm">

	  <table width="156" border="0" cellspacing="0" cellpadding="0">
	    <tr> 
	      <td width="54" class="newtxt9"><img src="http://www.dmmv.de/shared/siteimg/redesign/button_login.jpg" width="54" height="11"></td>
	      <td align="left" valign="bottom"> 
	        <input name="login" type="text" class="cmformsmall" size="6" border=0>
	      </td>
	    </tr>
	    <tr class="newtxt9"> 
	      <td width="54"><img src="http://www.dmmv.de/shared/siteimg/redesign/button_pass.jpg" width="54" height="11"></td>
	      <td align="left" valign="bottom"> 
	        <input name="password" type="password" class="cmformsmall" size="6" border=0>
	        <input name="imageField" type="image" src="http://www.dmmv.de/shared/siteimg/redesign/button_grau_go.jpg" width="24" height="15" border="0">
	      </td>
	    </tr>
	    <tr class="newtxt9"> 
	      <td height="4" colspan="2"><img src="http://www.dmmv.de/shared/siteimg/redesign/leer.gif" width="54" height="4"></td>
	    </tr>
	  </table>
	
	<img src="http://www.dmmv.de/shared/siteimg/redesign/login_pfeil.jpg" width="7" height="7" border="0">&nbsp;<a href="http://www.dmmv.de/ww/de/7_pub/mitgliedschaft/mitglied_werden.cfm">Mitglied werden</a>
	</form>
	

</td>
</tr>
</table>
</td>
</tr>
</table>
<!-- / Login -->
</td>
<td valign="top">
<table width="587" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="11"><img src="../../../shared/siteimg/redesign/leer.gif" width="587" height="11"></td>
</tr>
</table>
<table width="587" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="16"><img src="../../../shared/siteimg/redesign/leer.gif" width="16" height="11"></td>
<!-- header -->
<td class="titel" width="355" valign="bottom">
</td>
<!-- / header -->
<td width="20"><img src="../../../shared/siteimg/redesign/leer.gif" width="20" height="5"></td>
<td width="195" valign="top">
<table width="191" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="13"><img src="../../../shared/siteimg/redesign/anzeige.gif" width="13" height="47"></td>
<td width="178"><p>
<!-- ADTECH JavaScript TAG 2.0 for network: DMMV (NW 318) ++ Website: dmmv-Websites ++ ContentUnit: dmmv_all_196x49 (CU ID 89507) ++ Date: Mon Aug 25 16:40:06 CEST 2003 -->
<script language="JavaScript"><!--
var myDate = new Date();
AT_MISC = myDate.getTime();
document.write('<scr' + 'ipt src="http://adserver.adtech.de/?addyn|2.0|318|89507|1|46|target=_blank;loc=100;misc=' + AT_MISC + ';">');
if (navigator.userAgent.indexOf("Mozilla/2.") >= 0 || navigator.userAgent.indexOf("MSIE") >= 0) {
document.write('<a href="http://adserver.adtech.de/?adlink|2.0|318|89507|1|46|ADTECH;loc=200;" target="_blank"><img src="http://adserver.adtech.de/?adserv|2.0|318|89507|1|46|ADTECH;loc=200;" border="0" width="196" height="49"></a>');
}
document.write('</scr' + 'ipt>');// -->
</script>
<noscript><a href="http://adserver.adtech.de/?adlink|2.0|318|89507|1|46|ADTECH;loc=300;" target="_blank"><img src="http://adserver.adtech.de/?adserv|2.0|318|89507|1|46|ADTECH;" border="0"></a></noscript>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table width="587" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="10"><img src="../../../shared/siteimg/redesign/leer.gif" width="587" height="10"></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="763" height="2" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="176"><img src="../../../shared/siteimg/redesign/leer.gif" width="176" height="2"></td>
<td width="16"><img src="../../../shared/siteimg/redesign/leer.gif" width="16" height="2"></td>
<td width="355"><img src="../../../shared/siteimg/redesign/line_long.gif" width="355" height="1"></td>
<td width="20"><img src="../../../shared/siteimg/redesign/leer.gif" width="20" height="1"></td>
<td><img src="../../../shared/siteimg/redesign/line_small.gif" width="195" height="1"></td>
</tr>
</table>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="11"><img src="../../../shared/siteimg/redesign/leer.gif" width="763" height="11"></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="176" valign="top">
<table width="175" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0">
<tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../ww/de/7_pub/aktuelles.cfm"
class="cmnavi2active">&nbsp;Aktuelles</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../ww/de/7_pub/aktuelles/pressemitteilungen.cfm"
class="cmnavi3">&nbsp;Pressemitteilungen</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../ww/de/7_pub/aktuelles/tagesthemen.cfm"
class="cmnavi3">&nbsp;Tagesthemen</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../ww/de/7_pub/aktuelles/mitgliedernews.cfm"
class="cmnavi3">&nbsp;Aus den Unternehmen</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../ww/de/7_pub/aktuelles/neue_mitglieder.cfm"
class="cmnavi3">&nbsp;Neue Mitglieder</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../ww/de/7_pub/aktuelles/pressespiegel__newsradar_.cfm"
class="cmnavi3">&nbsp;Pressespiegel</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../ww/de/7_pub/aktuelles/newsletter.cfm"
class="cmnavi3">&nbsp;Newsletter</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../ww/de/7_pub/aktuelles/pressefotos.cfm"
class="cmnavi3">&nbsp;Pressefotos</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../ww/de/7_pub/aktuelles/informationsbroschueren.cfm"
class="cmnavi3">&nbsp;Informationsbroschüren</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../ww/de/7_pub/aktuelles/foren.cfm"
class="cmnavi3">&nbsp;Foren</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../ww/de/7_pub/fachgruppen_neu.cfm"
class="cmnavi2">&nbsp;Fachgruppen</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../ww/de/7_pub/themen_neu.cfm"
class="cmnavi2">&nbsp;Themen</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../ww/de/7_pub/termine.cfm"
class="cmnavi2">&nbsp;Termine</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../ww/de/7_pub/medien_ordnungspolitik.cfm"
class="cmnavi2">&nbsp;Medien & Ordnungspolitik</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../ww/de/7_pub/recht.cfm"
class="cmnavi2">&nbsp;Recht</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../ww/de/7_pub/marktforschung.cfm"
class="cmnavi2">&nbsp;Marktzahlen</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../ww/de/7_pub/serviceangebot.cfm"
class="cmnavi2">&nbsp;Serviceangebot</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../ww/de/7_pub/mitgliedschaft/mitglied_werden.cfm"
class="cmnavi2">&nbsp;Mitgliedschaft</a></td>
</tr><tr><td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../ww/de/7_pub/ueber_uns_neu.cfm"
class="cmnavi2">&nbsp;Über uns</a></td>
</tr>
<tr bgcolor="#FFFFFF"><td width="10"></td>
<td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td width="10"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td width="115"><img src="../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
</tr>
</table>
<table width="176" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- Navifooter, stets unter der auch ausgeklappten Navigation. Nicht überdecken -->
<td height="279" background="../../../shared/siteimg/redesign/back_left.gif">
<div align="center"><img src="../../../shared/siteimg/redesign/leer.gif" width="176" height="118"><br>
<!-- ADTECH JavaScript TAG 2.0 for network: DMMV (NW 318) ++ Website: dmmv-Websites ++ ContentUnit: dmmv_all_156x60 (CU ID 89506) ++ Date: Mon Aug 25 16:40:06 CEST 2003 -->
<script language="JavaScript"><!--
var myDate = new Date();
AT_MISC = myDate.getTime();
document.write('<scr' + 'ipt src="http://adserver.adtech.de/?addyn|2.0|318|89506|1|70|target=_blank;loc=100;misc=' + AT_MISC + ';">');
if (navigator.userAgent.indexOf("Mozilla/2.") >= 0 || navigator.userAgent.indexOf("MSIE") >= 0) {
document.write('<a href="http://adserver.adtech.de/?adlink|2.0|318|89506|1|70|ADTECH;loc=200;" target="_blank"><img src="http://adserver.adtech.de/?adserv|2.0|318|89506|1|70|ADTECH;loc=200;" border="0"</a>');
}
document.write('</scr' + 'ipt>');// -->
</script>
<noscript><a href="http://adserver.adtech.de/?adlink|2.0|318|89506|1|70|ADTECH;loc=300;" target="_blank"><img src="http://adserver.adtech.de/?adserv|2.0|318|89506|1|70|ADTECH;" border="0" width="156" height="60"></a></noscript><br>
<img src="../../../shared/siteimg/redesign/leer.gif" width="176" height="93"></div>
</td>
</tr>
</table>
<table width="176" height="100%" border="0" cellpadding="0" cellspacing="0" background="../../../shared/siteimg/redesign/back.gif">
<tr>
<td><img src="../../../shared/siteimg/redesign/leer.gif" width="176" height="5" align="bottom"></td>
</tr>
</table>
</td>
<td width="16"><img src="../../../shared/siteimg/redesign/leer.gif" width="16" height="11"></td>
<td width="571">
<img src="../../../shared/siteimg/buttons/zurueck.gif" width="18" height="9" border="0">&nbsp;<span class="txtsmall"><a href="Javascript:history.back();">zur&uuml;ck</a></small><br><br>
<span class="newheadline">dmmv empfiehlt EU-Ministerrat Parlamentsentscheidung zu Softwarepatenten zu folgen</span><br><br>
<table width="47" cellspacing=0 cellpadding=0 border=0>
<tr>
<td width=""><img src="../../../shared/img/picture/jpg/rg_verbandspraesentation_farbe_100x132.jpg" width=100 height=132 border=0 alt=""></td>
<td width="2"><img src="../../../shared/siteimg/util/space.gif" width="2" height="1" border="0"></td>
<td width="45" class="bg07"><img src="../../../shared/siteimg/util/space.gif" width="45" height="1" border="0"></td>
</tr>
</table><br><br><span class="txt"><P>PRESSEMITTEILUNG </P>
<P>dmmv&nbsp;empfiehlt EU-Ministerrat Parlamentsentscheidung zu Softwarepatenten zu folgen <BR>Innovationskraft von kleinen und mittelst&auml;ndischen Softwareunternehmen muss erhalten bleiben </P>
<P>D&uuml;sseldorf/M&uuml;nchen, 23. Oktober 2003 <BR>Derzeit ist eine EU-Richtlinie in Vorbereitung, mit der die Patentierung von softwarebezogenen Erfindungen einheitlich geregelt werden soll. Hierzu tagt heute der EU-Ministerrat, um den aktuellen Entwurf der Richtlinie zu beraten. Der Deutsche Multimedia Verband (dmmv) e.V. empfiehlt dem EU-Ministerrat der Entscheidung der EU-Parlamentarier zu Softwarepatenten folgen. Im Rahmen ihres Votums hatten die Parlamentarier Ende September den Vorschlag der EU-Kommission zwar in erster Lesung verabschiedet, aber mit rund 80 &Auml;nderungen zum Vorschlag der Kommission, auch sichergestellt, dass Software in der Regel allein dem Urheberrechtsschutz unterf&auml;llt und damit amerikanischen Verh&auml;ltnissen hinsichtlich des Patentschutzes einen Riegel vorgeschoben wird.</P>
<P>Bei den Mitgliedern der Mitte des Jahres gegr&uuml;ndeten Fachgruppe Softwareindustrie im dmmv, die auf die jahrelange Kompetenz und Erfahrung des Verbands der Softwareindustrie (VSI) e.V. zur&uuml;ckgreifen kann, war diese Entscheidung auf breite Zustimmung gesto&szlig;en. Gerade die mittelst&auml;ndische Softwareindustrie, die das Heraufziehen amerikanischer Verh&auml;ltnisse und damit eine grenzenlose Patentierbarkeit bef&uuml;rchtet hatte, sieht in dem jetzt vorliegenden Ergebnis einen guten Kompromiss zwischen der internationalen Wettbewerbsf&auml;higkeit Europas, zu der grunds&auml;tzlich auch der Patentschutz geh&ouml;rt, und dem Interesse gerade kleinerer Unternehmen. Die Kreativit&auml;t und Innovationsf&auml;higkeit dieser Unternehmen sind damit nicht durch das hohe Risiko einer Patentrechtsverletzung gehemmt.</P>
<P>"Grunds&auml;tzlich wird die Entscheidung den Gegebenheiten der Softwarebranche gerecht. Mit den neuen Formulierungen in Artikel 2 ist eine computerimplementierte Erfindung klar definiert und es kann nach unserer Ansicht auch nicht &uuml;ber Umwege ein ‚Datenverarbeitungsprogramm&rsquo;, also alleine die Software, in den Genuss des Patentschutzes gelangen. Im Gegenzug ist nach unserer Ansicht die M&ouml;glichkeit einer Patentierung f&uuml;r die Bereiche, in denen dies auch aus wettbewerbspolitischen Gr&uuml;nden notwendig ist, weiter m&ouml;glich. Dass sich die Mehrheiten f&uuml;r die angenommenen &Auml;nderungsantr&auml;ge aus unterschiedlichen politischen Lagern zusammengesetzt haben, spiegelt die politische Debatte wieder, die schon vorher einen zweifachen Aufschub der Abstimmung erzwungen hatte" kommentiert Rudi Gallist (R+T GmbH), Vorsitzender der Fachgruppe Softwareindustrie und dmmv-Vizepr&auml;sident, das Parlamentsvotum.</P>
<P>Gallist weist in diesem Zusammenhang unter anderem auch auf den wachsenden Markt von Multimedia-Software f&uuml;r mobile Endger&auml;te hin: "Sollte etwa ein Patentgericht der Meinung sein, ein Handyspiel sei als technischer Beitrag patentierbar, so kann das negative Signalwirkung f&uuml;r die aufstrebende europ&auml;ische Entertainmentsoftware-Industrie haben. Wir brauchen den Wettbewerb der Ideen in Europa, nicht den Wettbewerb der Patentanw&auml;lte."</P>
<P>Gallist fordert den EU-Ministerrat daher auf, auf die Notwendigkeiten und Gegebenheiten vor allem der kleinen und mittelst&auml;ndischen Software-Unternehmen in Europa R&uuml;cksicht zu nehmen: " Es muss sicher gestellt werden, dass beispielsweise Algorithmen, also grunds&auml;tzliche Methoden, mit denen Software funktioniert, auch in Zukunft nicht patentierbar&nbsp; sind.&ldquo;</P>
<P>Allerdings m&uuml;ssen nach Ansicht des dmmv auch die Kosten f&uuml;r die Registrierung und die Recherche von Patenten auf ein Ma&szlig; reduziert werden, dass es auch kleinen und mittelst&auml;ndischen Unternehmen erlaubt, Patente anzumelden bzw. eine Recherche mit einem angemessenen Zeit- und Kostenaufwand durchzuf&uuml;hren. &bdquo;Wir m&uuml;ssen auf alle F&auml;lle f&uuml;r eine St&auml;rkung unserer mittelst&auml;ndisch gepr&auml;gten Softwareindustrie sorgen. Darauf wird auch in Zukunft der Fokus unserer Aktivit&auml;ten liegen&ldquo; schlie&szlig;t Gallist seine Stellungnahme. </P>
<P>dmmv-Vizepr&auml;sident Rudi Gallist wird morgen im Rahmen einer Expertenrunde auf der Systems erneut Stellung zum Thema Softwarepatentierung beziehen. Die Diskussionsrunde findet&nbsp; ab 13 Uhr im Systems TV Studio (Halle B3, 350) statt und wird unter &gt; <A href="http://www.systems-world.de/?id=19466" target=_new>http://www.systems-world.de/?id=19466</A> live im Internet &uuml;bertragen.</P>
<P>Kontakt: <BR>Deutscher Multimedia Verband (dmmv) e.V. <BR>Christoph Salzig, Pressesprecher <BR>Tel. 0211 600 456 -26, Fax: -33 <BR><A href="mailto:salzig@dmmv.de">mailto:salzig@dmmv.de</A> <BR><A href="http://www.dmmv.de" target=_new>www.dmmv.de</A></P>
<P>Wir &uuml;ber uns: <BR>Der Deutsche Multimedia Verband (dmmv) e.V. ist Europas mitgliederst&auml;rkste Interessen- und Berufsvertretung der Digitalen Wirtschaft. Hierzu geh&ouml;ren alle Marktteilnehmer, deren wesentlicher Gesch&auml;ftszweck die Schaffung, Entwicklung, Verarbeitung, Veredelung,&nbsp; Speicherung oder Distribution interaktiver digitaler Inhalte, Produkte und Services ist, unabh&auml;ngig von der technischen Plattform. </P>
<P>Die mehr als 1.000 Mitglieder des dmmv sind Dienstleister interaktiver Medien, Interactivagenturen, Medienproduzenten, Entwickler individueller Software, Systemh&auml;user, Softwareh&auml;ndler, Mobile-Services-Anbieter, E-Kiosk- Anbieter, Vermarkter interaktiver Medien, E-Commerce-Anbieter, E-Services-Anbieter, E-Content-Anbieter, Anbieter im Bereich interaktives Fernsehen, Berater und Aus-&amp; Weiterbildungsinstitutionen. Er vertritt bundesweit mehr als 1.400 Unternehmen* der Digitalen Wirtschaft in medien- und ordnungspolitischen Belangen.</P>
<P>Als der ma&szlig;gebliche Berufsverband der digitalen Wirtschaft entwickelt der dmmv Aus- und Weiterbildungsmodelle (mit Zertifizierung zur Qualit&auml;tssicherung), Kalkulationsgrundlagen, Mustervertr&auml;ge und Handlungsempfehlungen f&uuml;r die neuen T&auml;tigkeitsfelder. Seine Kernfunktion liegt neben der politischen Arbeit in seiner Leistung als Know-how-Pool, Austauschplattform und Anbieter von Serviceleistungen f&uuml;r seine Mitglieder.</P>
<P>Der dmmv bildet das Dach f&uuml;r eigenst&auml;ndige Fachgruppen, in denen die einzelnen Branchensegmente der Digitalen Wirtschaft organisiert sind. Die inhaltliche Arbeit sowie der Austausch der Fachgruppen untereinander ist durch &uuml;bergreifende Arbeitskreise gew&auml;hrleistet. Unter <A href="http://www.dmmv.de" target=_new>www.dmmv.de</A> steht den Mitgliedern eine effektive Kommunikations- und Arbeitsplattform zur Verf&uuml;gung mit umfassendem Inhalteangebot, Foren, Mailinglisten, Voting und Downloads. </P>
<P>Als Ansprechpartner f&uuml;r Beh&ouml;rden, Presse und andere Branchenvertretungen ist es dem dmmv gelungen, ein starke Interessenvertretung zu schaffen, um dem Bereich der interaktiven digitalen Medien, Dienste &amp; Anwendungen ein f&uuml;r alle Marktteilnehmer ertragreiches T&auml;tigkeitsfeld zu gew&auml;hrleisten.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </P>
<P>* dmmv-Mitglieder und die von dmmv politisch vertreten Unternehmen des dmmv-Network <BR></span><br>
<br><img src='../../../shared/siteimg/buttons/zurueck.gif' width='18' height='9' border='0'>&nbsp;<a href='Javascript:history.back();'><span class='txtsmall'>zur&uuml;ck</span></a><br><br>
<br><img src="../../../shared/siteimg/util/space.gif" width=380 height=1>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="763" height="30" border="0" cellpadding="0" cellspacing="0">
<tr valign="bottom">
<td width="192" height="15"><img src="../../../shared/siteimg/redesign/leer.gif" width="192" height="15"></td>
<td width="355" height="15" valign="top"><img src="../../../shared/siteimg/redesign/line_long.gif" width="355" height="1"></td>
<td width="215" height="15"><img src="../../../shared/siteimg/redesign/leer.gif" width="215" height="15"></td>
</tr>
<tr>
<td><img src="../../../shared/siteimg/redesign/leer.gif" width="1" height="1"><img src="../../../shared/siteimg/redesign/leer.gif" width="192" height="1"></td>
<td>
<p>&copy; 2003 Deutscher Multimedia Verband (dmmv) e.V. | <a href="../../../ww/de/7_pub/impressum.cfm">Impressum</a>
|<br>
sponsored, powered and supported by:</p>
<p><img src="../../../shared/siteimg/redesign/logo_sponsors.gif" width="315" height="23" border="0" usemap="#Map"></p>
<map name="Map">
<area shape="rect" coords="1,1,43,22" href="http://www.psineteurope.de" target="_blank" alt="PSINet Europe">
<area shape="rect" coords="68,-1,143,26" href="http://www.contens.de" target="_blank" alt="Contens">
<area shape="rect" coords="153,0,228,25" href="http://www.denkwerk.com" target="_blank" alt="denkwerk">
<area shape="rect" coords="247,1,315,24" href="http://www.microsoft.de" target="_blank" alt="Microsoft">
</map>
</td>
<td><img src="../../../shared/siteimg/redesign/leer.gif" width="215" height="1"></td>
</tr>
</table>
</td>
</tr>
</table>
<!-- IVW -->
<SCRIPT LANGUAGE="JavaScript">
<!--
var IVW="http://dmmv.ivwbox.de/cgi-bin/ivw/CP/sk;var";
document.write("<IMG SRC=\""+IVW+"?r="+escape(document.referrer)+"\" WIDTH=\"1\" HEIGHT=\"1\">");
// -->
</SCRIPT>
<NOSCRIPT>
<IMG SRC="http://dmmv.ivwbox.de/cgi-bin/ivw/CP/sk;var" WIDTH="1" HEIGHT="1">
</NOSCRIPT>
<!-- /IVW -->
</body>
</html>


