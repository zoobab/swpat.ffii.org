<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

AcS: Analytical Articles regarding Software Patents
ArW: A fairly raw and unordered collection of pointers to articles.
Gnf: How patents are endangering the Web
Ssm: Telepolis-Gespräch mit Richard Stallman
Hme: Hauptthema des Telepolis-Artikels ist Stallmans Engagement gegen Softwarepatente, wobei auch FFII zitiert wird.
Bnn: Bei unseren Politikern fehlt es an Verständnis für die Ökonomie der Wissensgesellschaft.  Im blinden Vertrauen auf den Markt einerseits und das Patentwesen andererseits droht die Allmende Internet in die Brüche zu gehen. Der Autor führt zahlreiche ärgerliche Patente an und analysiert insbesondere die Patentpraxis von Microsoft.
NWb: Richard M. Stallman ruft am Datum 1999-05-02 europäische Softwareautoren zu politischem Handeln auf.
DeW: Die Französischsprachige Vereinigung der Anwender von Linux und Freier Software argumentiert gegen eine Ausweitung des Patentschutzes auf Software und warnt Politiker davor, europäische Standortvorteile aufzugeben.
PnW: A lifelong developper of cutting-edge free software is now retiring from business.  He explains that his great productivity and success would not have been possible if software patenting had started earlier.
Heh: Harvard-Rechtsprofessor Lessig sieht staatliche Monopolgewährungswut auf dem Wege, einen sich von selbst organisierenden Markt zu ersticken
Tie: Techweb über die Bedeutung des Falles Amazon
Vow: Verbraucheranwalt Nader pflichtet Richard Stallman bei, der vor Stärkung der Monopolposition von M$ durch Softwarepatente warnt und eine Verpflichtung auf defensiven Gebrauch fordert.
Dtd: Legal strife about to shake the New Market
Dms: Der breite Wirkungskreis von E-Kommerz-Patenten führt zu einer Prozessflut, vor der niemand sicher ist.
Usd: US-Senator Schumer erklärt, das US-Patentamt fordere mit seinen selbstsüchtig-expansionstischen Praktiken die US-Bundesregierung womöglich zu kurzfristigen Notverordnungen und sonstigen ungewollten Übergriffen in den ansonsten autonomen Bereich des Patentwesens.
Eit: EPA-Patentanmeldung für triviale Methode zur Zweckentfremdung von HTTP-Adressen
Dtv: Diskussion über Patentierung von trivialer J2000-Lösung
Dnl: Der drittgrößte Softwarehersteller der Welt hält nichts von Softwarepatenten, hat aber dennoch viele Softwarepatente zu defensiven Zwecken erworben.
AWA: Artikel in TheAtlantic
CaW: Computerwoche berichtet über eine Tagung des Max-Planck-Institus über IT-Patentierung und kommt zu dem Schluss, Softwarepatente seien schädlicher Ballast.
Oag: Ärger mit einem Patent auf grundlegende WWW-Techniken
Utt: US-Patentamt mit Softwarepatenten überfordert
Ere: Eine weitere E-Kommerz-Firma versucht, sich grundlegende WWW-Techniken %(q:schützen) zu lassen
EnW: Ein Kenner beschreibt das amerikanische %(h:Software:Patentsystem) von seiner tragikomischen Seite.
Wcu: Warum Patentschutz für Kathedralen-Technologien wie die Audiokompression durchaus angemessen und fair sein kann
Pee: Probleme hierbei sind
Deo: Das Patentsystem bietet derzeit keine Möglichkeit, die Kathedralen-Technologien von anderen zu unterscheiden
Wkr: Was heute eine Kathedralen-Technik ist, funktioniert vielleicht morgen bereits nach dem Basar-Modell
NeW: Nichts verpflichtet den Patentinhaber, so umsichtig mit seinen Rechten umzugehen, wie im hier beschriebenen MP3-Fall geschehen.  Räuberische Monopoltaktiken sind oft besser für den Aktienkurs.
EWp: Einer softwareverträglicheren Ausgestaltung des Patentrechts stehen internationale Verpflichtungen wie der TRIPS-Vertrag entgegen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatvreji.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatlanle ;
# txtlang: en ;
# End: ;

