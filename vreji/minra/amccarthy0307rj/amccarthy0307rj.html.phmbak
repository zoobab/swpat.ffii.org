<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
 <HEAD>
   <TITLE> [Gllug] [Fwd: Response to your correspondence regarding the draft
	EU directive on patentability of computer-implemented inventions.]
   </TITLE>
   <LINK REL="Index" HREF="index.html" >
   <LINK REL="made" HREF="mailto:gllug%40linux.co.uk?Subject=%5BGllug%5D%20%5BFwd%3A%20Response%20to%20your%20correspondence%20regarding%20the%20draft%0A%09EU%20directive%20on%20patentability%20of%20computer-implemented%20inventions.%5D&In-Reply-To=">
   <META NAME="robots" CONTENT="index,nofollow">
   <META http-equiv="Content-Type" content="text/html; charset=us-ascii">
   <LINK REL="Previous"  HREF="036823.html">
   <LINK REL="Next"  HREF="036833.html">
 </HEAD>
 <BODY BGCOLOR="#ffffff">
   <H1>[Gllug] [Fwd: Response to your correspondence regarding the draft
	EU directive on patentability of computer-implemented inventions.]</H1>
    <B>Richard Jones</B> 
    <A HREF="mailto:gllug%40linux.co.uk?Subject=%5BGllug%5D%20%5BFwd%3A%20Response%20to%20your%20correspondence%20regarding%20the%20draft%0A%09EU%20directive%20on%20patentability%20of%20computer-implemented%20inventions.%5D&In-Reply-To="
       TITLE="[Gllug] [Fwd: Response to your correspondence regarding the draft
	EU directive on patentability of computer-implemented inventions.]">rich at annexia.org
       </A><BR>
    <I>Wed Jul 16 17:12:55 BST 2003</I>
    <P><UL>
        <LI>Previous message: <A HREF="036823.html">[Gllug] Interpocket - USB for iPAQ/Zaurus
</A></li>
        <LI>Next message: <A HREF="036833.html">[Gllug] [Fwd: Response to your correspondence regarding the draft
</A></li>
         <LI> <B>Messages sorted by:</B> 
              <a href="date.html#36825">[ date ]</a>
              <a href="thread.html#36825">[ thread ]</a>
              <a href="subject.html#36825">[ subject ]</a>
              <a href="author.html#36825">[ author ]</a>
         </LI>
       </UL>
    <HR>  
<!--beginarticle-->
<PRE>----- Forwarded message from Arlene McCarthy &lt;<A HREF="http://list.ftech.net/mailman/listinfo/gllug">arlenemccarthymep at yahoo.co.uk</A>&gt; -----

From: Arlene McCarthy &lt;<A HREF="http://list.ftech.net/mailman/listinfo/gllug">arlenemccarthymep at yahoo.co.uk</A>&gt;
Subject: Response to your correspondence regarding the draft EU directive on patentability of computer-implemented inventions.
To: <A HREF="http://list.ftech.net/mailman/listinfo/gllug">rich at annexia.org</A>


Dear Richard Jones,



Thank you for your correspondence concerning the draft directive on the patentability of computer-implemented inventions.



The European Parliament's Legal Affairs Committee has voted on my report on the directive and there will be continuing debate and further democratic scrutiny before the directive becomes law.



At this early stage of legislative process, it is nonetheless important to establish the facts about what the draft EU directive and what I, as the Parliament's rapporteur, are aiming to achieve in the amendments tabled to the Commission proposal.



It has been suggested that the Parliament's report will for the first time allow the patentability of computer-implemented inventions. This is simply not true. The patenting of computer-implemented inventions is not a new phenomenon. Patents involving the use of software have been applied for and granted since the earliest days of the European Patent Office (EPO). Out of over 110,000 applications received at the EPO in 2001, 16,000 will have dealt with inventions in computer-implemented technologies. Indeed, even without an EU directive, these patents will continue to be filed, not only to the EPO but also to national patent offices.



As you will be aware, in the US and increasingly in Japan, patents have been granted for what is essentially pure software. Some EPO and national court rulings indicate that Europe may be drifting towards extending the scope of patentability to inventions which would traditionally have not been patentable, as well as pure business methods. It is clear that Europe needs a uniform legal approach which draws a line between what can and cannot be patented, and prevents the drift towards the patentability of software per se.



My intention is clear in the amendments tabled and in a new Article 4 in the text, to preclude; the patentability of software as such; the patentability of business methods; algorithms; and mathematical methods. Article 4 clearly states that in order to be patentable, a computer-implemented invention must be susceptible to industrial applications, be new, and involve an inventive step. Moreover I have added a requirement for a technical contribution in order to ensure that the mere use of a computer does not lead to a patent being granted.



Furthermore, the amended directive contains new provisions on decompilation that will assist software developers. While it is not possible to comment on whether any patent application would be excluded from the directive, the directive, as amended, would not permit the patentability of Amazon's 'one-click' method. As far as software itself is concerned, it will not be possible to patent a software product. Software itself will continue to be able to be protected by copyright.



With an EU directive, legislators will have scrutiny over the EPO and national court's decisions. With, in addition, the possibility of having a definitive ruling from the European Court in Luxembourg, thus ensuring a restrictive interpretation of the EU directive and a greater degree of legal certainty in the field of patentability of computer-implemented inventions.



Some concerns have been raised that the directive may have an adverse effect on the development of open source software and small software developers. I support the development of open source software and welcome the fact that the major open-source companies are recording a 50% growth in world-wide shipment of its products. 



In the amended proposal, I have imposed a requirement on the Commission to monitor the impact of the directive, in particular its effect on small and medium sized enterprises, and to look at any potential difficulties in respect of the relationship between patent protection of computer-implemented inventions and copyright protection.



Many small companies have given their support to this directive, which will give them more legal certainty as it offers the possibility of protection for their R&amp;D investment, and so assists in spin-off creation and technology transfer and generating new funds for new investments. 



Indeed recently, a small ten-person company in an economic black-spot in the UK granted a licence to a US multinational for its voice recognition software patents. Without European patent protection in this field, the small company could have found itself in the perverse situation whereby its R&amp;D efforts and investment would simply have been taken by a large multinational company, who, with its team of patent lawyers, would have filed a patent on this invention. The EU company could have been faced subsequently with patent infringement proceedings.



Some lobbyists would like us to believe that having no patents is an option - it is not. No patents would put EU software developers at a severe disadvantage in the global market place, and would hand over the monopoly on patents to multinational companies.



The work I have done is an honest attempt to approach this matter objectively, and to produce balanced legislation, taking into account the needs and interests of all sectors of the software development industry and small businesses in Europe. No doubt there will be more debate and refinements to the legislation before a final text is agreed under the EU legislation process.



At a time when many of our traditional industries are migrating to Asia and when Europe needs increasingly to rely on its inventiveness to reap rewards, it is important to have the option of the revenue secured by patents and the licensing out of computer-implemented technologies.



Software development is a major European industry. In 1998 alone the value of the EU software market was ?39 billion. Most of this will be protected by copyright, but genuine computer-implemented inventions must have the possibility, for the future of competitiveness of our industry, to have patent protection.



 

Yours sincerely



Arlene McCarthy MEP


















---------------------------------
Yahoo! Plus - For a better Internet experience

----- End forwarded message -----

-- 
Richard Jones. <A HREF="http://www.annexia.org/">http://www.annexia.org/</A> <A HREF="http://freshmeat.net/users/rwmj">http://freshmeat.net/users/rwmj</A>
Merjis Ltd. <A HREF="http://www.merjis.com/">http://www.merjis.com/</A> - all your business data are belong to you.
C2LIB is a library of basic Perl/STL-like types for C. Vectors, hashes,
trees, string funcs, pool allocator: <A HREF="http://www.annexia.org/freeware/c2lib/">http://www.annexia.org/freeware/c2lib/</A>

</PRE>
<!--endarticle-->
    <HR>
    <P><UL>
        <!--threads-->
	<LI>Previous message: <A HREF="036823.html">[Gllug] Interpocket - USB for iPAQ/Zaurus
</A></li>
	<LI>Next message: <A HREF="036833.html">[Gllug] [Fwd: Response to your correspondence regarding the draft
</A></li>
         <LI> <B>Messages sorted by:</B> 
              <a href="date.html#36825">[ date ]</a>
              <a href="thread.html#36825">[ thread ]</a>
              <a href="subject.html#36825">[ subject ]</a>
              <a href="author.html#36825">[ author ]</a>
         </LI>
       </UL>

<hr>
<a href="http://list.ftech.net/mailman/listinfo/gllug">More information about the Gllug
mailing list</a><br>
</body></html>
