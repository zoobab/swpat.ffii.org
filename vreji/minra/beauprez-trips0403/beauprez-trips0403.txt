
   [1]Login

   Pref Your subscriptions [2]Home [3]Help

   patents@aful.org
   software patents mailing list



   [4]List info 


   Subscribers: 516
   Owners
   [5]jp@nexedi.com
   [6]jp@smets.com
   [7]Hartmut Pilch
   Moderators


   [8]Subscribe 


   [9]Unsubscribe 


   Archive


   Post

   _________________ Search
   [10]Advanced search
   1999 01 02 03 04 [11]05 [12]06 [13]07 [14]08 [15]09 [16]10 [17]11
   [18]12
   2000 [19]01 [20]02 [21]03 [22]04 [23]05 [24]06 [25]07 [26]08 [27]09
   [28]10 [29]11 [30]12
   2001 [31]01 [32]02 [33]03 [34]04 [35]05 [36]06 [37]07 [38]08 [39]09
   [40]10 [41]11 [42]12
   2002 [43]01 [44]02 [45]03 [46]04 [47]05 [48]06 [49]07 [50]08 [51]09
   [52]10 [53]11 [54]12
   2003 [55]01 [56]02 [57]03 [58]04 [59]05 [60]06 [61]07 [62]08 [63]09
   [64]10 [65]11 [66]12
   2004 [67]01 [68]02 [69]03 04 05 06 07 08 09 10 11 12

                [70] previous    [71]Chronological            [72] previous 
   [73]Thread       
     _________________________________________________________________

   [Fwd: "Computer Implemented inventions" and TRIPS] ehj
     * From: <ehj@xxxxxxxxxxx>
     * To: <patents@xxxxxxxx>
     * Cc: <beauprez@xxxxxxxxxxxxxxxxxxxx>
     * Subject: [Fwd: "Computer Implemented inventions" and TRIPS]
     * Date: Sat, 20 Mar 2004 15:23:16 +0100 (CET)
     * Importance: Normal

   A mail from Christian Beauprez was forwarded to me with an interesting
   analysis of c.i.i. and TRIPs relations.
   I forward some excerpts.
   ------ message ------
   As someone who studied Law at the University of Kent and programming
   for 4 years (self taught) I have observed the debate on the status of
   computer implemented inventions with great interest. Of particular
   note
   to me was the implication of the relevance of the EU's obligations
   under
   the TRIPS treaty (Article 5) often quoted by "computer implemented
   inventions" enthusiasts,
   "1. Subject to the provisions of paragraphs 2 and 3, patents shall be
   available for any inventions,whether products or processes, in all
   fields of technology, provided that they are new, involve an inventive
   step and are capable of industrial application. "
   And this is apparently why the EU has to consider "computer
   implemented
   inventions" patents.
   The European Patent office have stated in their case law under the
   "computer implemented inventions" chapter ,In T 1173/97 (OJ 1999, 609)
   and T 935/97
   "Programs for computers could be considered as patentable inventions
   if
   they have a technical character."
   At the same time however both views ignore very specific provisions
   for
   "computer programs" under the WTO (TRIPS) treaty that bound all signed
   "European Communities" members although apparently not the EPC (thus
   not
   the EPO directly) according to their claims, this is possibly how it
   was
   overlooked. Article 10 of said treaty clearly states:
   a.. "Computer programs, whether in source or object code, shall be
   protected as literary works under the Berne Convention (1971)."
   This is the strange thing you see, the statement doesn't seem to mean
   that much on first glance. It is only when reading it closely that one
   realises that it does not simply say that "computer programs are
   automatically copyrighted under the Berne Convention",
   it specifies they "shall be protected as literary works".
   Literary works cannot be patented because they are not inventions.
   Indeed if literary works could be patented one would have to concede
   that books, screenplays, and music could be patented as well although
   according to my research there is no provision for this in law. We
   would
   also have to apply patent laws to these areas since we are not
   allowed,
   apparently under article 5 to restrict on the basis of the field of
   technology .
   Because publishing takes place on a non-material medium does not mean
   that we can apply concepts to software publishing or web publishing
   that
   we would not apply to any other form of "literary works". If we did,
   we
   could not then say that we were protecting them correctly under the
   criteria laid out explicitly in the TRIPS treaty.
   I wanted to make sure that my interpretation was the correct one, so I
   looked in the overview notes of the TRIPS website. What I found
   further
   confirmed my interpretation,
   "This provision confirms that computer programs must be protected
   under
   copyright and that those provisions of the Berne Convention that apply
   to literary works shall be applied also to them. It confirms further,
   that the form in which a program is, whether in source or object code,
   does not affect the protection. The obligation to protect computer
   programs as literary works means e.g. that only those limitations that
   are applicable to literary works may be applied to computer programs."
   Therefore not only would it be untrue to say that the WTO (TRIPS)
   treaty
   requires the EU to treat software as a patentable invention as opposed
   to a literary work . Quite the opposite, under this treaty, any
   signatory who does not protect programs "as literary works" could be
   said to be in breach under WTO rules. Patenting of computer programs
   of
   any description could therefore be said to be in violation of this
   treaty, because it would mean no longer protecting computer programs
   as
   "literary works" but as "inventions" and they cannot be both.
   This is a fundamental point that to my knowledge has been overlooked
   by
   various patent offices, and just about everyone in the debate. I do
   hope
   that none of the proposed laws for software, computer programs or
   "computer implemented inventions" will make us liable for trade
   sanctions under explicit WTO obligations. Please pass my discovery
   around as you see fit.
   ------end message-----
   Dear Christian Beauprez, as you have invited to a discussion on this
   topic, I hope you find it appropriate I do so on this list.
   Kind regards.
   Erik Josefsson

                                   [english.]
                                              Powered by [74]Sympa 3.3.3 

References

   1. http://www.aful.org/wws/nomenu/loginrequest/arc/patents
   2. http://www.aful.org/wws/
   3. http://www.aful.org/wws/help
   4. http://www.aful.org/wws/info/patents
   5. mailto:jp@nexedi.com
   6. mailto:jp@smets.com
   7. mailto:phm@a2e.de
   8. http://www.aful.org/wws/subrequest/patents
   9. http://www.aful.org/wws/sigrequest/patents
  10. http://www.aful.org/wws/arcsearch_form/patents/2004-03
  11. http://www.aful.org/wws/arc/patents/1999-05/
  12. http://www.aful.org/wws/arc/patents/1999-06/
  13. http://www.aful.org/wws/arc/patents/1999-07/
  14. http://www.aful.org/wws/arc/patents/1999-08/
  15. http://www.aful.org/wws/arc/patents/1999-09/
  16. http://www.aful.org/wws/arc/patents/1999-10/
  17. http://www.aful.org/wws/arc/patents/1999-11/
  18. http://www.aful.org/wws/arc/patents/1999-12/
  19. http://www.aful.org/wws/arc/patents/2000-01/
  20. http://www.aful.org/wws/arc/patents/2000-02/
  21. http://www.aful.org/wws/arc/patents/2000-03/
  22. http://www.aful.org/wws/arc/patents/2000-04/
  23. http://www.aful.org/wws/arc/patents/2000-05/
  24. http://www.aful.org/wws/arc/patents/2000-06/
  25. http://www.aful.org/wws/arc/patents/2000-07/
  26. http://www.aful.org/wws/arc/patents/2000-08/
  27. http://www.aful.org/wws/arc/patents/2000-09/
  28. http://www.aful.org/wws/arc/patents/2000-10/
  29. http://www.aful.org/wws/arc/patents/2000-11/
  30. http://www.aful.org/wws/arc/patents/2000-12/
  31. http://www.aful.org/wws/arc/patents/2001-01/
  32. http://www.aful.org/wws/arc/patents/2001-02/
  33. http://www.aful.org/wws/arc/patents/2001-03/
  34. http://www.aful.org/wws/arc/patents/2001-04/
  35. http://www.aful.org/wws/arc/patents/2001-05/
  36. http://www.aful.org/wws/arc/patents/2001-06/
  37. http://www.aful.org/wws/arc/patents/2001-07/
  38. http://www.aful.org/wws/arc/patents/2001-08/
  39. http://www.aful.org/wws/arc/patents/2001-09/
  40. http://www.aful.org/wws/arc/patents/2001-10/
  41. http://www.aful.org/wws/arc/patents/2001-11/
  42. http://www.aful.org/wws/arc/patents/2001-12/
  43. http://www.aful.org/wws/arc/patents/2002-01/
  44. http://www.aful.org/wws/arc/patents/2002-02/
  45. http://www.aful.org/wws/arc/patents/2002-03/
  46. http://www.aful.org/wws/arc/patents/2002-04/
  47. http://www.aful.org/wws/arc/patents/2002-05/
  48. http://www.aful.org/wws/arc/patents/2002-06/
  49. http://www.aful.org/wws/arc/patents/2002-07/
  50. http://www.aful.org/wws/arc/patents/2002-08/
  51. http://www.aful.org/wws/arc/patents/2002-09/
  52. http://www.aful.org/wws/arc/patents/2002-10/
  53. http://www.aful.org/wws/arc/patents/2002-11/
  54. http://www.aful.org/wws/arc/patents/2002-12/
  55. http://www.aful.org/wws/arc/patents/2003-01/
  56. http://www.aful.org/wws/arc/patents/2003-02/
  57. http://www.aful.org/wws/arc/patents/2003-03/
  58. http://www.aful.org/wws/arc/patents/2003-04/
  59. http://www.aful.org/wws/arc/patents/2003-05/
  60. http://www.aful.org/wws/arc/patents/2003-06/
  61. http://www.aful.org/wws/arc/patents/2003-07/
  62. http://www.aful.org/wws/arc/patents/2003-08/
  63. http://www.aful.org/wws/arc/patents/2003-09/
  64. http://www.aful.org/wws/arc/patents/2003-10/
  65. http://www.aful.org/wws/arc/patents/2003-11/
  66. http://www.aful.org/wws/arc/patents/2003-12/
  67. http://www.aful.org/wws/arc/patents/2004-01/
  68. http://www.aful.org/wws/arc/patents/2004-02/
  69. http://www.aful.org/wws/arc/patents/2004-03/
  70. http://www.aful.org/wws/arc/patents/2004-03/msg00064.html
  71. http://www.aful.org/wws/arc/patents/2004-03/mail3.html#00065
  72. http://www.aful.org/wws/arc/patents/2004-03/msg00064.html
  73. http://www.aful.org/wws/arc/patents/2004-03/thrd3.html#00065
  74. http://listes.cru.fr/sympa/
