


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<!-- --------------------------------------------------------------------------------
/ww/de/7_pub/fachgruppen_neu/softwareindustrie.cfm
Softwareindustrie
Last Update: 13.11.2003 11:02 (#16) - Date created: 27.09.2003
(c) CONTENS Software GmbH
CONTENS 2.5
© 1999-2003 CONTENS Software GmbH
--------------------------------------------------------------------------------- -->
<title>Softwareindustrie</title>
<meta name="description" content="Fachgruppen, Online-Vermarkterkreis">
<meta name="keywords" content="Fachgruppen, Online-Vermarkterkreis">
<meta name="copyright" content="(c) CONTENS Software GmbH">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


<!-- Browsercheck and dynamic style -->

	<link rel="STYLESHEET" type="text/css" href="http://www.dmmv.de/shared/stylesheets/dmmv_ie.css">
	<link rel="STYLESHEET" type="text/css" href="http://www.dmmv.de/shared/stylesheets/appstyle_ie.css">

<!-- /Browsercheck and dynamic style -->
</HEAD>
<BODY bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<img src=http://sitestat2.nedstat.nl/cgi-bin/dmmv/sitestat.gif?name= width=1 height=1 alt="" align=right>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="176" height="131" valign="top">
<p><a href="http://www.dmmv.de" target="_self"><img src="../../../../shared/siteimg/redesign/logo_dmmv.jpg" width="176" height="131" border="0"></a></p>
</td>
<td>
<table width="587" height="131" border="0" cellpadding="0" cellspacing="0">
<tr>
<td height="102" valign="top">
<table width="587" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><img src="../../../../shared/siteimg/redesign/header_up.jpg" width="587" height="76"></td>
</tr>
<tr>
<td>
<table width="587" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="267" height="26" valign="top" nowrap><img src="../../../../shared/siteimg/redesign/header_left.jpg" width="267" height="26"></td>
<td height="26" bgcolor="#FEED00">
<table width="320" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="46" height="26" nowrap>
<p><a href="../../../../ww/de/7_pub/suche.cfm"><img src="../../../../shared/siteimg/redesign/button_suche.jpg" width="46" height="26" border="0"></a></p>
</td>
<td class="titel" width="121" height="26" nowrap>
<!-- searchform -->
<form style="display:inline" name="searchform" method="get" action="../../../../ww/de/7_pub/suche.cfm">
<input type="hidden" name="deutsch" value="2">
<input type="hidden" name="bool" value="or">
<input type="hidden" name="near" value="">
<input type="hidden" name="intern" value="0">
<input type="hidden" name="itemsperpage" value="10">
<input type="hidden" name="fuseaction_sea" value="results">
<input type="hidden" name="newsearch" value="1">
<input name="criteria" type="text" class="cmformsmall" id="suche" size="17">
</form>
</td>
<td width="23" height="26" nowrap><img src="../../../../shared/siteimg/redesign/button_orange_go.jpg" width="23" height="26" onclick="if(document.searchform.criteria.value!='')document.searchform.submit()"></td>
<td width="130" height="26" nowrap>
<p><img src="../../../../shared/siteimg/redesign/trenner_orange.jpg" width="22" height="26"><a href="../../../../ww/de/7_pub/kontakt.cfm"><img src="../../../../shared/siteimg/redesign/button_kontakt.jpg" width="37" height="26" border="0"></a><img src="../../../../shared/siteimg/redesign/trenner_orange_2.jpg" width="20" height="26"><a href="../../../../ww/de/7_pub/sitemap.cfm"><img src="../../../../shared/siteimg/redesign/button_sitemap.jpg" width="51" height="26" border="0"></a></p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td width="587" height="19">
<table width="587" height="19" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="16"><img src="../../../../shared/siteimg/redesign/leer.gif" width="16" height="11"></td>
<td>
<a href="http://www.dmmv.de/ww/de/7_pub/aktuelles.cfm"
>Startseite</a>
<img src="../../../../shared/siteimg/redesign/text_pfeil.jpg" width="7" height="7">
<a href="http://www.dmmv.de/ww/de/7_pub/fachgruppen_neu.cfm"
>Fachgruppen</a>
<img src="../../../../shared/siteimg/redesign/text_pfeil.jpg" width="7" height="7">
<a href="http://www.dmmv.de/ww/de/7_pub/fachgruppen_neu/softwareindustrie.cfm"
>Softwareindustrie</a>
</td>
<td class=txtheadergrey>
<div align="right"><script language="Javascript">
<!--
today = new Date();
day = today.getDay();
date = today.getDate();
month = today.getMonth()+1;
year = today.getYear();
if (day == 0) dayName = "Sonntag"
else if (day == 1) dayName = "Montag"
else if (day == 2) dayName = "Dienstag"
else if (day == 3) dayName = "Mittwoch"
else if (day == 4) dayName = "Donnerstag"
else if (day == 5) dayName = "Freitag"
else dayName = "Samstag"
if (year < 2001) year = year + 1900;
if (month < 10) month = "0" + month;
if (date < 10) date = "0" + date;
document.write(dayName + ", " + date + "." + month + "." + year);
//-->
</script>
</div>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table width="763" height="2" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="176"><img src="../../../../shared/siteimg/redesign/leer.gif" width="176" height="2"></td>
<td width="16"><img src="../../../../shared/siteimg/redesign/leer.gif" width="16" height="2"></td>
<td width="355"><img src="../../../../shared/siteimg/redesign/line_long.gif" width="355" height="1"></td>
<td width="20"><img src="../../../../shared/siteimg/redesign/leer.gif" width="20" height="1"></td>
<td><img src="../../../../shared/siteimg/redesign/line_small.gif" width="195" height="1"></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="176" valign="top">
<table class="bg08" width="176" height="70" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="20"><img src="../../../../shared/siteimg/redesign/leer.gif" width="10" height="5"></td>
<td>
<table width="156" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>




	
	<form name="loginform" style="display:inline" action="http://www.dmmv.de/apps/memberlogin/index.cfm" method="post">
	  <input type="Hidden" name="editorgroupname" value="siteusers">
	  <input type="Hidden" name="loginfailedlink" value="/ww/de/7_pub/login.cfm">
	
	  
	  
	  
	  
	  
	  <input type="Hidden" name="ref" value="/ww/de/7_pub/fachgruppen_neu/softwareindustrie.cfm">

	  <table width="156" border="0" cellspacing="0" cellpadding="0">
	    <tr> 
	      <td width="54" class="newtxt9"><img src="http://www.dmmv.de/shared/siteimg/redesign/button_login.jpg" width="54" height="11"></td>
	      <td align="left" valign="bottom"> 
	        <input name="login" type="text" class="cmformsmall" size="6" border=0>
	      </td>
	    </tr>
	    <tr class="newtxt9"> 
	      <td width="54"><img src="http://www.dmmv.de/shared/siteimg/redesign/button_pass.jpg" width="54" height="11"></td>
	      <td align="left" valign="bottom"> 
	        <input name="password" type="password" class="cmformsmall" size="6" border=0>
	        <input name="imageField" type="image" src="http://www.dmmv.de/shared/siteimg/redesign/button_grau_go.jpg" width="24" height="15" border="0">
	      </td>
	    </tr>
	    <tr class="newtxt9"> 
	      <td height="4" colspan="2"><img src="http://www.dmmv.de/shared/siteimg/redesign/leer.gif" width="54" height="4"></td>
	    </tr>
	  </table>
	
	<img src="http://www.dmmv.de/shared/siteimg/redesign/login_pfeil.jpg" width="7" height="7" border="0">&nbsp;<a href="http://www.dmmv.de/ww/de/7_pub/mitgliedschaft/mitglied_werden.cfm">Mitglied werden</a>
	</form>
	

</td>
</tr>
</table>
</td>
</tr>
</table>
<!-- / Login -->
</td>
<td valign="top">
<table width="587" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="11"><img src="../../../../shared/siteimg/redesign/leer.gif" width="587" height="11"></td>
</tr>
</table>
<table width="587" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="16"><img src="../../../../shared/siteimg/redesign/leer.gif" width="16" height="11"></td>
<!-- header -->
<td class="titel" width="355" valign="bottom">
<span class="titel">Softwareindustrie</span></td>
<!-- / header -->
<td width="20"><img src="../../../../shared/siteimg/redesign/leer.gif" width="20" height="5"></td>
<td width="195" valign="top">
<table width="191" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="13"><img src="../../../../shared/siteimg/redesign/anzeige.gif" width="13" height="47"></td>
<td width="178"><p>
<!-- ADTECH JavaScript TAG 2.0 for network: DMMV (NW 318) ++ Website: dmmv-Websites ++ ContentUnit: dmmv_all_196x49 (CU ID 89507) ++ Date: Mon Aug 25 16:40:06 CEST 2003 -->
<script language="JavaScript"><!--
var myDate = new Date();
AT_MISC = myDate.getTime();
document.write('<scr' + 'ipt src="http://adserver.adtech.de/?addyn|2.0|318|89507|1|46|target=_blank;loc=100;misc=' + AT_MISC + ';">');
if (navigator.userAgent.indexOf("Mozilla/2.") >= 0 || navigator.userAgent.indexOf("MSIE") >= 0) {
document.write('<a href="http://adserver.adtech.de/?adlink|2.0|318|89507|1|46|ADTECH;loc=200;" target="_blank"><img src="http://adserver.adtech.de/?adserv|2.0|318|89507|1|46|ADTECH;loc=200;" border="0" width="196" height="49"></a>');
}
document.write('</scr' + 'ipt>');// -->
</script>
<noscript><a href="http://adserver.adtech.de/?adlink|2.0|318|89507|1|46|ADTECH;loc=300;" target="_blank"><img src="http://adserver.adtech.de/?adserv|2.0|318|89507|1|46|ADTECH;" border="0"></a></noscript>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table width="587" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="10"><img src="../../../../shared/siteimg/redesign/leer.gif" width="587" height="10"></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="763" height="2" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="176"><img src="../../../../shared/siteimg/redesign/leer.gif" width="176" height="2"></td>
<td width="16"><img src="../../../../shared/siteimg/redesign/leer.gif" width="16" height="2"></td>
<td width="355"><img src="../../../../shared/siteimg/redesign/line_long.gif" width="355" height="1"></td>
<td width="20"><img src="../../../../shared/siteimg/redesign/leer.gif" width="20" height="1"></td>
<td><img src="../../../../shared/siteimg/redesign/line_small.gif" width="195" height="1"></td>
</tr>
</table>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="11"><img src="../../../../shared/siteimg/redesign/leer.gif" width="763" height="11"></td>
</tr>
</table>
</td>
</tr>
</table>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td width="176" valign="top">
<table width="175" bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0">
<tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../../ww/de/7_pub/aktuelles.cfm"
class="cmnavi2">&nbsp;Aktuelles</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../../ww/de/7_pub/fachgruppen_neu.cfm"
class="cmnavi2active">&nbsp;Fachgruppen</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../../ww/de/7_pub/fachgruppen_neu/agenturen.cfm"
class="cmnavi3">&nbsp;Agenturen</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../../ww/de/7_pub/fachgruppen_neu/aus__und_weiterbildung.cfm"
class="cmnavi3">&nbsp;Aus- und Weiterbildung</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../../ww/de/7_pub/fachgruppen_neu/dienstleister.cfm"
class="cmnavi3">&nbsp;Dienstleister</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../../ww/de/7_pub/fachgruppen_neu/e_commerce.cfm"
class="cmnavi3">&nbsp;E-Commerce</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../../ww/de/7_pub/fachgruppen_neu/e_content.cfm"
class="cmnavi3">&nbsp;E-Content/E-Services</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../../ww/de/7_pub/fachgruppen_neu/mobile_services.cfm"
class="cmnavi3">&nbsp;Mobile Services</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../../ww/de/7_pub/fachgruppen_neu/online_vermarkterkreis.cfm"
class="cmnavi3">&nbsp;Online-Vermarkterkreis</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="2"><img src="../../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="3"><a href="../../../../ww/de/7_pub/fachgruppen_neu/softwareindustrie.cfm"
class="cmnavi3active">&nbsp;Softwareindustrie</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="3"><img src="../../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="2"><a href="../../../../ww/de/7_pub/fachgruppen_neu/softwareindustrie/mitglieder.cfm"
class="cmnavi4">&nbsp;Mitglieder</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right"
colspan="3"><img src="../../../../ssf.gif" width="7" height="7" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="2"><a href="../../../../ww/de/7_pub/fachgruppen_neu/softwareindustrie/geschaeftsordnung.cfm"
class="cmnavi4">&nbsp;Geschäftsordnung</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../../ww/de/7_pub/themen_neu.cfm"
class="cmnavi2">&nbsp;Themen</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../../ww/de/7_pub/termine.cfm"
class="cmnavi2">&nbsp;Termine</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../../ww/de/7_pub/medien_ordnungspolitik.cfm"
class="cmnavi2">&nbsp;Medien & Ordnungspolitik</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../../ww/de/7_pub/recht.cfm"
class="cmnavi2">&nbsp;Recht</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../../ww/de/7_pub/marktforschung.cfm"
class="cmnavi2">&nbsp;Marktzahlen</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../../ww/de/7_pub/serviceangebot.cfm"
class="cmnavi2">&nbsp;Serviceangebot</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../../ww/de/7_pub/mitgliedschaft/mitglied_werden.cfm"
class="cmnavi2">&nbsp;Mitgliedschaft</a></td>
</tr><tr><td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td align="right" valign="top"><img src="../../../../sf.gif" width="11" height="11" border="0"></td>
<td style="border-top:1px solid;color:#E5E5E5;}"
colspan="4"><a href="../../../../ww/de/7_pub/ueber_uns_neu.cfm"
class="cmnavi2">&nbsp;Über uns</a></td>
</tr>
<tr bgcolor="#FFFFFF"><td width="10"></td>
<td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td width="10"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
<td width="115"><img src="../../../../shared/siteimg/util/pix_blanco.gif" width="1" height="17" border="0"></td>
</tr>
</table>
<table width="176" border="0" cellspacing="0" cellpadding="0">
<tr>
<!-- Navifooter, stets unter der auch ausgeklappten Navigation. Nicht überdecken -->
<td height="279" background="../../../../shared/siteimg/redesign/back_left.gif">
<div align="center"><img src="../../../../shared/siteimg/redesign/leer.gif" width="176" height="118"><br>
<!-- ADTECH JavaScript TAG 2.0 for network: DMMV (NW 318) ++ Website: dmmv-Websites ++ ContentUnit: dmmv_all_156x60 (CU ID 89506) ++ Date: Mon Aug 25 16:40:06 CEST 2003 -->
<script language="JavaScript"><!--
var myDate = new Date();
AT_MISC = myDate.getTime();
document.write('<scr' + 'ipt src="http://adserver.adtech.de/?addyn|2.0|318|89506|1|70|target=_blank;loc=100;misc=' + AT_MISC + ';">');
if (navigator.userAgent.indexOf("Mozilla/2.") >= 0 || navigator.userAgent.indexOf("MSIE") >= 0) {
document.write('<a href="http://adserver.adtech.de/?adlink|2.0|318|89506|1|70|ADTECH;loc=200;" target="_blank"><img src="http://adserver.adtech.de/?adserv|2.0|318|89506|1|70|ADTECH;loc=200;" border="0"</a>');
}
document.write('</scr' + 'ipt>');// -->
</script>
<noscript><a href="http://adserver.adtech.de/?adlink|2.0|318|89506|1|70|ADTECH;loc=300;" target="_blank"><img src="http://adserver.adtech.de/?adserv|2.0|318|89506|1|70|ADTECH;" border="0" width="156" height="60"></a></noscript><br>
<img src="../../../../shared/siteimg/redesign/leer.gif" width="176" height="93"></div>
</td>
</tr>
</table>
<table width="176" height="100%" border="0" cellpadding="0" cellspacing="0" background="../../../../shared/siteimg/redesign/back.gif">
<tr>
<td><img src="../../../../shared/siteimg/redesign/leer.gif" width="176" height="5" align="bottom"></td>
</tr>
</table>
</td>
<td width="16"><img src="../../../../shared/siteimg/redesign/leer.gif" width="16" height="11"></td>
<td width="355">
<span class="txt"><P>In der Fachgruppe findet sich ein Abbild der Softwareindustrie. Unternehmen aller Gr&ouml;&szlig;enordnungen und Branchensegmente spiegeln auch in der Fachgruppe die Wirtschaft wieder. Neben Firmen, die ihren Ursprung in der Herstellung von Software haben, sind auch Unternehmen, die Software an die Bed&uuml;rfnisse der Anwender anpassen oder mit weiteren Funktionalit&auml;ten versehen, genauso vertreten wie der Distributor oder der Fachh&auml;ndler. Aber auch Firmen, die sich als Partner der Softwareindustrie verstehen, finden in der Fachgruppe "Softwareindustrie" ihre Heimat.</P>
<P>Die Fachgruppe "Softwareindustrie" steht allen</P>
<UL>
<LI>Herstellern von Software</LI>
<LI>Systemintegratoren</LI>
<LI>Softwareentwickler</LI>
<LI>VARs</LI>
<LI>Softwareverlagen</LI>
<LI>Vertriebsniederlassungen ausl&auml;ndischer Softwareunternehmen</LI>
<LI>Distributoren und Resellern f&uuml;r Soft- und Hardware</LI>
<LI>Zulieferfirmen und Partnern der Softwareindustrie</LI></UL>
<P>offen.</P>
<P><SPAN class=txtsmall><A href="http://www.vsi.de/" target=_blank><IMG height=45 alt="Der Kooperationspartner des dmmv" src="http://www.dmmv.de/shared/img/organisations/vsi_logo.gif" width=107 border=0></A></SPAN></P>Die Fachgruppe "Softwareindustrie" im dmmv wurde Mitte des Jahres ins Leben gerufen. Sie kann jedoch durch die Einbringung des Know How des VSI (Verband der Softwareindustrie Deutschlands) auf dessen inhaltliche Kompetenz und langj&auml;hrige Erfahrung im Bereich der Interessenvertretung der Softwareindustrie zur&uuml;ckgreifen. Auch die Besetzung des Gesamtvorstandes und in der Gesch&auml;ftsleitung im dmmv machen an der Kompetenz des VSI fest und stellen die Bedeutung der Branche "Software" innerhalb des dmmv sicher.</span>
<br clear=all><hr>
<!-- outputtype: file_default --------------------------------------------------->
<a href="../../../../de/data/pdf/fg_7_software_final.pdf">
<img src="../../../../shared/img/fileicons/pdf_small.gif" width=10 height=10 border=0 alt="MultiMedia-File" align="texttop">
</a>
<a href="../../../../de/data/pdf/fg_7_software_final.pdf">
Download der Basisinformation zur Fachgruppe Softwareindustrie
</a>
<span class="txtsmall">(
476.5 KB)</span>
<br><!-- /outputtype: file_default ---------------------------------------------------><hr><br><img src="../../../../shared/siteimg/util/space.gif" width=355 height=1>
</td>
<td width="20"><img src="../../../../shared/siteimg/redesign/leer.gif" width="20" height="5"></td>
<td "line-height:16px" width="195">
<!-- Objekt: "Kontakt", mit gelb unterlegtem Header, Text, positionierbarem Bild -->
<table width="195" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="newheadline" background="../../../../shared/siteimg/redesign/title_back.jpg"><img src="../../../../shared/siteimg/redesign/leer.gif" width="4" height="4"><img src="../../../../shared/siteimg/redesign/title_pfeil.jpg" width="7" height="7">
Kontakt</td>
</tr>
<tr>
<td class="bg08">
<table style="line-height:16px" width="195" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4"><img src="../../../../shared/siteimg/redesign/leer.gif" width="4" height="4"></td>
<td width="191">
<p>
<img src="../../../../shared/img/picture/jpg/jk_kontakt_72dpi.jpg" width=50 height=71 border=0 hspace="0" vspace="0" align="right">
Johannes M. Kr&uuml;ger<br>
dmmv e.V.
Gesch&auml;ftsstelle M&uuml;nchen<br>
Tel: 089 2 90 42 89<br>
</p></td>
</tr>
</table>
</td>
</tr>
</table>
<img src="../../../../shared/siteimg/redesign/line_small_plus11.gif" width="195" height="23">
<table style="line-height:16px" width="195" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4"><img src="../../../../shared/siteimg/redesign/leer.gif" width="4" height="4"></td>
<td width="191">
<a href="mailto:eflash@dmmv.de?subject=Newsletter-Abonnement"><img src="../../../../shared/siteimg/redesign/site_pfeil.gif" width="7" height="7" border="0">&nbsp;Newsletter abonnieren
</a><hr>
<a href="mailto:huther@dmmv.de?subject=Gasteinladung zur naechsten Sitzung"><img src="../../../../shared/siteimg/redesign/site_pfeil.gif" width="7" height="7" border="0">&nbsp;Gasteinladung zur n&auml;chsten Sitzung
</a><hr></td>
</tr>
</table>
<img src="../../../../shared/siteimg/redesign/line_small_plus11.gif" width="195" height="23">
<p>&nbsp;</p>
<br><br><span class="txtsmall"><TABLE cellSpacing=0 cellPadding=0 width=191 border=0>
<TBODY>
<TR vAlign=top>
<TD width=95><A href="../fachgruppen_neu/softwareindustrie/aufgaben_und_ziele.cfm">Aufgaben &amp; Ziele</A><BR>Links<BR>Downloads<BR><A href="../marktforschung/marktzahlenmafojump.cfm">MaFo</A></TD>
<TD width=96>
<P><A href="../themen_neu/software.cfm">Ergebnisse</A><BR><A href="../aktuelles/pressemitteilungen.cfm">Presse</A><BR><A href="../fachgruppen_neu/softwareindustrie/mitglieder.cfm">Mitglieder</A><BR><A href="../fachgruppen_neu.cfm">Fachgruppen-&uuml;bersicht</A></P></TD></TR></TBODY></TABLE></span><hr><br>
</td>
</tr>
</table>
</td>
</tr>
</table>
<table width="763" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="763" height="30" border="0" cellpadding="0" cellspacing="0">
<tr valign="bottom">
<td width="192" height="15"><img src="../../../../shared/siteimg/redesign/leer.gif" width="192" height="15"></td>
<td width="355" height="15" valign="top"><img src="../../../../shared/siteimg/redesign/line_long.gif" width="355" height="1"></td>
<td width="215" height="15"><img src="../../../../shared/siteimg/redesign/leer.gif" width="215" height="15"></td>
</tr>
<tr>
<td><img src="../../../../shared/siteimg/redesign/leer.gif" width="1" height="1"><img src="../../../../shared/siteimg/redesign/leer.gif" width="192" height="1"></td>
<td>
<p>&copy; 2003 Deutscher Multimedia Verband (dmmv) e.V. | <a href="../../../../ww/de/7_pub/impressum.cfm">Impressum</a>
|<br>
sponsored, powered and supported by:</p>
<p><img src="../../../../shared/siteimg/redesign/logo_sponsors.gif" width="315" height="23" border="0" usemap="#Map"></p>
<map name="Map">
<area shape="rect" coords="1,1,43,22" href="http://www.psineteurope.de" target="_blank" alt="PSINet Europe">
<area shape="rect" coords="68,-1,143,26" href="http://www.contens.de" target="_blank" alt="Contens">
<area shape="rect" coords="153,0,228,25" href="http://www.denkwerk.com" target="_blank" alt="denkwerk">
<area shape="rect" coords="247,1,315,24" href="http://www.microsoft.de" target="_blank" alt="Microsoft">
</map>
</td>
<td><img src="../../../../shared/siteimg/redesign/leer.gif" width="215" height="1"></td>
</tr>
</table>
</td>
</tr>
</table>
<!-- IVW -->
<SCRIPT LANGUAGE="JavaScript">
<!--
var IVW="http://dmmv.ivwbox.de/cgi-bin/ivw/CP/sk;var";
document.write("<IMG SRC=\""+IVW+"?r="+escape(document.referrer)+"\" WIDTH=\"1\" HEIGHT=\"1\">");
// -->
</SCRIPT>
<NOSCRIPT>
<IMG SRC="http://dmmv.ivwbox.de/cgi-bin/ivw/CP/sk;var" WIDTH="1" HEIGHT="1">
</NOSCRIPT>
<!-- /IVW -->
</body>
</html>


