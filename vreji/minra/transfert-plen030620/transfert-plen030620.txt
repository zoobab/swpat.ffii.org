
   Spacer
   26 06 2003
   [1]Retour a la home 
   [2]Rubrique �conomie
   �conomie [3]Rubrique Soci�t�
   Soci�t� [4]Rubrique Technologies
   Technologies [5]Rubrique Culture
   Culture
   Spacer
   [6]MOTS CL�S
   [7]Recherche
   [8]Droit
   [9]Libert�s
   [10]Multinationales
   [11]M�dias
    
   [12]Informatique
   [13]Vie priv�e
   [14]Environnement
   [15]Propri�t� intellectuelle
   [16]Tous les mots

   [17] Le projet |  [18]L'�quipe |  [19]L'association |  [20]S'abonner
   _________________________ [21]Actualit�s |  [22]Sources |
   [23]Alertes |  [24]Webmail

   Abonn�s : connectez-vous

                             ________ ________

                                                    Se souvenir  [_]   OK

    Transfert est une agence de presse associative, sans publicit�. Nous
       �ditons un site et un fil d'info quotidiens sur l'avanc�e des
     technologies dans la soci�t�. Enrichi d'outils de recherche et de
            veille, notre service est disponible par abonnement.
                            [25]En savoir plus.

   Gratuit ! D�couvrez notre fil d'info et nos outils. Pendant 15 jours,
    recevez notre newsletter et testez nos services _______________   OK

   [26]CHIFFRE DU JOUR
   [27]1er ou 4�me ordinateur 
   le plus rapide du monde, le nouveau G5 d'Apple ? ([28]...)
   [29]PHRASE DU JOUR
   [30]"La CIA recherche des serruriers d�sirant travailler avec les
   meilleurs esprits du pays lors de missions vitales pour notre nation" 
   Offre d'emploi sur le site officiel de la Central Intelligence Agency
   ([31]...)
   [32]REVUE DE WEB
   [33]Sexisme sur "EverQuest" - Formats domestiqu�s - Yahoo ! m�lomane -
   Agents d�masqu�s 
   ([34]...)
   Spacer
   [35]Dossier Economie
   L'impasse �nerg�tique
   Spacer
   La semaine
   mercredi 25/06 SOCI�T�
   [36]Le S�nat refuse de cr�er un "comit� d'�thique sur internet"
   mardi 24/06 SOCI�T�
   [37]Attaqu� par Lorie, le moteur Voila est jug� non responsable des
   sites qu'il r�f�rence
   lundi 23/06 �CONOMIE
   [38]Brevetabilit� logicielle : le vote du Parlement europ�en avanc� au
   30 juin
   vendredi 20/06 SOCI�T�
   [39]Un rapport approuve la diffusion de donn�es m�dicales aux
   assureurs
   jeudi 19/06 �CONOMIE
   [40]"L'ANPE est le plus gros employeur actuel du jeu vid�o" [Nicolas
   Perret]
   Spacer
   [pixelblanc.gif]
   �conomie
   Europe / Logiciel Libre / Propri�t� intellectuelle
   23/06/2003  o  17h38
   [pixelblanc.gif]
   Brevetabilit� logicielle : le vote du Parlement europ�en avanc� au 30
   juin
   [pixelblanc.gif]
   Pris de cours, les opposants � la directive d�noncent cette
   pr�cipitation

   Le vote du Parlement europ�en sur la directive europ�enne visant �
   �tablir une brevetabilit� du logiciel, initialement pr�vu en
   septembre, a �t� avanc� au 30 juin prochain. Les opposants � la
   directive regrettent que cette question, qui fait d�bat depuis plus de
   trois ans, soit examin�e de fa�on pr�cipit�e. Ils redoutent un passage
   en force devant les eurod�put�s.

   Le 20 juin 2003, le [41]programme de la s�ance du 30 juin 2003 du
   Parlement europ�en a �t� modifi�. D�sormais y figure la question :
   "Droit des brevets : brevetabilit� des inventions mises en oeuvre par
   ordinateur."

   R�unis en s�ance pl�ni�re, les eurod�put�s devront se prononcer sur la
   proposition de directive visant � �tablir la brevetabilit� des
   logiciels en Europe, selon le mod�le d�j� en vigueur aux Etats-Unis
   (Lire [42]Brevetabilit� du logiciel : derni�re ligne droite).

   Ce vote interviendra seulement 13 jours apr�s [43]la finalisation du
   rapport de la Commission juridique et du march� int�rieur du Parlement
   europ�en, charg�e de l'examen du [44]projet de directive. Arl�ne
   McCarthy, une eurod�put�e britannique issue du New Labour, est
   rapporteur aupr�s de la Commission juridique du parlement europ�en.
   Elle est une fervente opposante � toute limite au principe de
   brevetabilit�.

   Processus "opaque"
   Hartmut Pilch est porte-parole de la [45]FFII (Foundation for a free
   information infrastructure), une association bas�e en Allemagne qui
   combat la directive aux c�t�s des partisans du logiciel libre et des
   Verts europ�ens. Il explique : "Nous sommes surpris de la vitesse et
   de la facilit� avec laquelle Arl�ne McCarthy a pu faire changer le
   calendrier parlementaire." "Le processus est tr�s opaque",
   ajoute-t-il.

   "Comment les d�put�s peuvent-ils lire le texte, r�diger et traduire
   des amendements, les soumettre et rechercher des soutiens politiques
   en un temps aussi court ?" demande Hartmut Pilch, qui rappelle que la
   [46]p�tition Eurolinux contre la directive a recueilli plus de 150 000
   signatures, dont celles de 2 000 chefs d'entreprise et de 25 000
   d�veloppeurs du secteur logiciel europ�en.

   Les opposants � la directive soulignent que la pr�cipitation dans
   laquelle le texte sur la brevetabilit� va �tre examin� ne sied pas �
   la gravit� d'une question qui fait d�bat depuis longtemps au sein du
   Parlement europ�en. Ainsi, deux autres commissions, celle de la
   Culture et celle de l'Industrie, avaient rendu un avis d�favorable �
   la proposition de directive, oppos� � celui de la Commission
   juridique, qui n'en a pas tenu compte dans le rapport qu'elle a d�pos�
   sur le bureau du Parlement.

   Le site d'information [47]Linuxfr redoute que le vote de la directive,
   pr�vu en quatri�me position sur l'ordre du jour, n'ait lieu tr�s tard
   dans la soir�e du 30 juin. En cette p�riode estivale de fin de session
   parlementaire, les opposants � la directive craignent un passage en
   force devant un faible nombre de d�put�s.

   "La situation est vraiment mauvaise. Nous pensons que, dans ces
   conditions, les d�put�s voteront uniquement sur le fond du texte, car
   peu d'amendements seront possibles, estime Harmut Pilch de la FFII. En
   effet, Arlene McCarthy et ses alli�s ont 'emball�' leur propos pour
   faire croire aux d�put�s qu'ils introduisaient des limitations � la
   brevetabilit�. Et ils b�n�ficient aussi des canaux officiels de
   communication. En pr�cipitant les choses, ils peuvent obtenir un large
   soutien pour un emballage s�duisant qui dit l'exact contraire de ce
   qui est �crit dans le texte de la directive, longue et difficile �
   lire."

   Les adversaires de la directive ont bien du mal � mobiliser sur une
   question � laquelle peu de politiques fran�ais et europ�ens sont
   sensibilis�s.

                                                    [48]Alexandre Piquard

   [49]Contacter la r�daction |  [50]Imprimer l'article |  [51]Article au
                    format .pdf |  [52]Envoyer l'article

   Brevetabilit� du logiciel: derni�re ligne droite (Transfert.net):
   [53]http://www.transfert.net/a8917

   "Le brevet logiciel est une insulte au principe m�me du libre �change"
   (Transfert.net):
   [54]http://www.transfert.net/a8989

   L'ordre du jour de la s�ance du 30 juin (Parlement europ�en):
   [55]http://www3.europarl.eu.int/ap-cgi/ch...

   Le site de la p�tition Eurolinux:
   [56]http://petition.eurolinux.org/index_html

   La page des "moyens d'action" avant le vote sur
   Brevets-Logiciels.info:
   [57]http://brevets-logiciels.info/wiki/wakka.php?wiki=MoyensDaction
   Dans la m�me rubrique
   25/06/2003  o  19h10
   [pixelgris.gif]
   [58]Copie priv�e : la France plus r�pressive que l'Europe
   24/06/2003  o  19h09
   [pixelgris.gif]
   [59]Une ONG am�ricaine propose une loi pour encadrer les codes-barres
   du futur
   23/06/2003  o  18h48
   [pixelgris.gif]
   [60]Les industriels europ�ens contre la surveillance des
   t�l�communications
   19/06/2003  o  17h18
   [pixelgris.gif]
   [61]"L'ANPE est le plus gros employeur actuel du jeu vid�o" [Nicolas
   Perret]
   18/06/2003  o  16h36
   [pixelgris.gif]
   [62]"Pour �tudier la commercialisation des OGM, nous avons construit
   une Europe artificielle" [Juliette Rouchier]
   [pixelblanc.gif]
   [63]Dossier Culture
   L'art biotech :
   des labos aux expos
   [pixelblanc.gif]
   Derni�res infos
   25/06/2003  o  19h29
   [pixelgris.gif]
   [64]"Le monde actuel est digne de l'imagination d'Orwell" [Simon
   Davies]
   25/06/2003  o  19h20
   [pixelgris.gif]
   [65]Le S�nat refuse de cr�er un "comit� d'�thique sur internet"
   25/06/2003  o  19h05
   [pixelgris.gif]
   [66]L'imagerie c�r�brale prouve notre in�galit� devant la douleur
   25/06/2003  o  16h39
   [pixelgris.gif]
   [67]PrintBrush, une mini-imprimante tout terrain qui communique sans
   fil
   24/06/2003  o  19h24
   [pixelgris.gif]
   [68]Tribune : "J'ai fauch� des OGM" [Fr�d�ric Prat]
   24/06/2003  o  18h56
   [pixelgris.gif]
   [69]Attaqu� par Lorie, le moteur Voila est jug� non responsable des
   sites qu'il r�f�rence
   24/06/2003  o  18h22
   [pixelgris.gif]
   [70]La plus petite sonde du monde d�tectera maladies g�n�tiques et
   polluants
   23/06/2003  o  18h30
   [pixelgris.gif]
   [71]Bient�t des empreintes biom�triques dans les passeports europ�ens
   23/06/2003  o  16h43
   [pixelgris.gif]
   [72]L'aquarium de Lyon invente la "carte � pouce" � empreinte digitale
   23/06/2003  o  16h33
   [pixelgris.gif]
   [73]Les Japonais sortent un robot en carton pour y ranger son PC
   20/06/2003  o  17h43
   [pixelgris.gif]
   [74]Un rapport approuve la diffusion de donn�es m�dicales aux
   assureurs
   20/06/2003  o  15h26
   [pixelgris.gif]
   [75]Une fonctionnalit� d'Internet Explorer transform�e en porte
   d�rob�e
   20/06/2003  o  13h55
   [pixelgris.gif]
   [76]Les firewalls personnels sont loin d'�tre infaillibles
   20/06/2003  o  13h26
   [pixelgris.gif]
   [77]"Le droit de r�ponse doit �tre transpos� � l'internet" [Emmanuel
   Derieux]
   20/06/2003  o  12h41
   [pixelgris.gif]
   [78]Panasonic Japon lance une bicyclette �lectrique rep�rable par
   satellite

              [79]Contacts |  [80]Haut de page |  [81]Archives
     [82]Logiciels libres |  [83]Confidentialit� |  [84]Revue de presse
                   Tous droits r�serv�s | � Transfert.net
                               [85][xml.gif] 

Verweise

   Sichtbare Links
   1. file://localhost/
   2. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/r86
   3. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/r87
   4. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/r89
   5. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/r110
   6. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/themas.php3
   7. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/t46
   8. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/t20
   9. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/t41
  10. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/t18
  11. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/t30
  12. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/t45
  13. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/t7
  14. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/t11
  15. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/t21
  16. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/themas.php3
  17. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8570
  18. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8531
  19. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8571
  20. https://www.transfert.net/a8566
  21. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/news.php
  22. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/tous_sites.php
  23. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/watch.php
  24. https://www.transfert.net/webmail/src/login2.php
  25. http://www.transfert.net/a8566
  26. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/r92
  27. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9031
  28. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9031
  29. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/r116
  30. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9026
  31. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9026
  32. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/r94
  33. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9024
  34. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9024
  35. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/d51
  36. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9028
  37. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9023
  38. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9011
  39. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9007
  40. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8999
  41. http://www3.europarl.eu.int/ap-cgi/chargeur.pl?APP=IRIS+PRG=FRAMERIEF+FILE=REPRIEF+SESSION=JUL%7C03+DAY=1+SES=ALL+LG=FR+BACK=//wwwdb.europarl.eu.int/dors/oeil/fr/activ.htm
  42. http://www.transfert.net/a8917
  43. http://www.transfert.net/a8989
  44. http://europa.eu.int/smartapi/cgi/sga_doc?smartapi!celexapi!prod!CELEXnumdoc&lg=fr&numdoc=52002PC0092
  45. http://www.ffii.org/index.en.html
  46. http://petition.eurolinux.org/index_html
  47. http://www.linuxfr.org/
  48. mailto:VEUILLEZ_RETIRER_CETTE_MENTION_ANTI-SPAMapiquard@transfert.net
  49. mailto:reagir@transfert.net?subject=Brevetabilit%E9 logicielle : le vote du Parlement europ%E9en avanc%E9 au 30 juin[9011]
  50. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/i9011
  51. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/ap9011.pdf
  52. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/formulaire.php3?id_article=9011
  53. http://www.transfert.net/a8917
  54. http://www.transfert.net/a8989
  55. http://www3.europarl.eu.int/ap-cgi/chargeur.pl?APP=IRIS+PRG=FRAMERIEF+FILE=REPRIEF+SESSION=JUL%7C03+DAY=1+SES=ALL+LG=FR+BACK=//wwwdb.europarl.eu.int/dors/oeil/fr/activ.htm
  56. http://petition.eurolinux.org/index_html
  57. http://brevets-logiciels.info/wiki/wakka.php?wiki=MoyensDaction
  58. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9030
  59. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9022
  60. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9012
  61. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8999
  62. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8984
  63. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/d52
  64. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9029
  65. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9028
  66. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9025
  67. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9027
  68. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9021
  69. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9023
  70. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9020
  71. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9013
  72. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9009
  73. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9008
  74. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9007
  75. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8998
  76. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8997
  77. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9002
  78. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a9003
  79. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8531
  80. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/transfert-plen030620.html#top
  81. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/archives.php3
  82. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8589
  83. file://localhost/a8616
  84. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/a8618
  85. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/backend.php3

   Versteckte Links:
  86. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/transfert-plen030620/themas.php3
