

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html lang="en">
<head>

<title>EU Presidency 2004 Website &gt; Sponsorship</title>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"> 
<!--Dublin Core Standards-->
<meta name="Keywords" content="">
<meta name="Title" content="">
<meta name="Creator" content="">
<meta name="Subject" content="">
<meta name="Description" content="">
<meta name="Publisher" content="">
<meta name="Contributor" content="">
<meta name="Date" content="2004-01-01">
<meta name="Type" content="">
<meta name="Format" content="">
<meta name="Identifier" content="">
<meta name="Source" content="">
<meta name="Language" content="en">
<meta name="Relation" content="">
<meta name="Coverage" content="">
<meta name="Rights" content="">

<link href="../includes/style.css" type="text/css" rel="stylesheet">
<LINK title="Text-only version" rel="alternate" href="../includes/textonly.asp?version=text" media="aural, tty">



</head>
<body>
<!-- Start Header -->
<table class="nav_top_background"  border="0" cellspacing="0" cellpadding="0" width="100%" summary="structure table">
	<tr>
		<td rowspan="3" width="168" align="left" valign="bottom"><a title="Ireland 2004 Presidency of the European Union" href="../templates/homepage.asp?sNavlocator=1"><img src="../images/header/logo_top.gif" width="168" height="80" alt="Ireland 2004 Presidency of the European Union" title="Ireland 2004 Presidency of the European Union" border="0"></a></td>
		<td colspan="2" width="100%" valign="bottom" align="left">
			<table class="nav_top_background"  border="0" cellspacing="0" cellpadding="0" summary="structure table">
				<tr>
					<td width="10"><img alt=" " src="../images/spacer.gif" width="10" height="1"></td>		
										
								<td valign="bottom"><a title="The Presidency" href="../content/index.asp?sNavlocator=5" class="nav_top_off" ><strong>The Presidency</strong></a></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
								
								<td valign="bottom"><img src="../images/header/nav_element_divider.gif" width="1" height="15" alt=" "></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
														
								<td valign="bottom"><a title="Policy Areas" href="../content/index.asp?sNavlocator=4" class="nav_top_off" ><strong>Policy Areas</strong></a></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
								
								<td valign="bottom"><img src="../images/header/nav_element_divider.gif" width="1" height="15" alt=" "></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
														
								<td valign="bottom"><a title="The EU" href="../content/index.asp?sNavlocator=6" class="nav_top_off" ><strong>The EU</strong></a></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
								
								<td valign="bottom"><img src="../images/header/nav_element_divider.gif" width="1" height="15" alt=" "></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
														
								<td valign="bottom"><a title="Latest News" href="../content/index.asp?sNavlocator=66" class="nav_top_off" ><strong>Latest News</strong></a></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
								
								<td valign="bottom"><img src="../images/header/nav_element_divider.gif" width="1" height="15" alt=" "></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
														
								<td valign="bottom"><a title="Media Services" href="../content/index.asp?sNavlocator=9" class="nav_top_off" ><strong>Media Services</strong></a></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
								
								<td valign="bottom"><img src="../images/header/nav_element_divider.gif" width="1" height="15" alt=" "></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
														
								<td valign="bottom"><a title="Ireland" href="../content/index.asp?sNavlocator=7" class="nav_top_off" ><strong>Ireland</strong></a></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
								
								<td valign="bottom"><img src="../images/header/nav_element_divider.gif" width="1" height="15" alt=" "></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
														
								<td valign="bottom"><a title="IGC" href="../content/index.asp?sNavlocator=88" class="nav_top_off" ><strong>IGC</strong></a></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
								
								<td valign="bottom"><img src="../images/header/nav_element_divider.gif" width="1" height="15" alt=" "></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
														
								<td valign="bottom"><a title="Youth" href="../content/index.asp?sNavlocator=3" class="nav_top_off" ><strong>Youth</strong></a></td>
								<td><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
								
				</tr>
				<tr>
					<td colspan="5"><img SRC="../images/spacer.gif" width="1" height="5" alt=" "></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="bg1" >
		<td width="10" align="left"><img src="../images/spacer.gif" width="10" height="1" alt=" "></td>
		<td align="left" width="100%">
			<table border="0" cellspacing="0" cellpadding="0" summary="structure table">
				<tr>
					<td align="left" valign="middle" class="nav">You are here:</td>
					<td width="9" align="left"><img src="../images/spacer.gif" width="9" height="1" alt=" "></td>
					<td align="left" valign="middle" class="nav_you_are_here"><a title="Home" href="../index.asp" class="nav_you_are_here"><strong>Home</strong></a> > <span class='nav_you_are_here'><strong>Sponsorship</strong></span></td>
				</tr>
			</table>
		</td>		
	</tr>
	<tr>
		<td bgcolor="white" width="10" align="left"><img src="../images/spacer.gif" width="10" height="1" alt=" "></td>
		<td bgcolor="white">
		
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr>	
					<td width="46"><img src="../images/spacer.gif" width="46" height="1" alt=" "></td>
					<td align="left" class="countdown_msg">&nbsp;</td>
					<td align="right">			
						<table border="0" cellspacing="0" cellpadding="0" summary="links to other languages">
							<tr>					
								
								<td><img src="../images/spacer.gif" width="5" height="1" alt=" "></td>
								<td><img src="../images/common/arrow_right_gold.gif" width="4" height="7" alt=" "></td>
								<td><img src="../images/spacer.gif" width="5" height="1" alt=" "></td>
								<td align="left" valign="middle"><a lang="ga" title="Gaeilge" href="../includes/language.asp?language_id=3&amp;node_id=" class="nav_medium">Gaeilge</a></td>
								
								<td><img src="../images/spacer.gif" width="5" height="1" alt=" "></td>
								<td><img src="../images/common/arrow_right_gold.gif" width="4" height="7" alt=" "></td>
								<td><img src="../images/spacer.gif" width="5" height="1" alt=" "></td>
								<td align="left" valign="middle"><a lang="fr" title="Fran�ais" href="../includes/language.asp?language_id=2&amp;node_id=" class="nav_medium">Fran�ais</a></td>
								
								<td><img src="../images/spacer.gif" width="10" height="1" alt=" "></td>
							</tr>
						</table>
					</td>	
				</tr>
			</table>
		</td>	
	</tr>
</table>
<!-- End Header -->
<!-- Begin Generic Left Navigation -->
<table border="0" cellspacing="0" cellpadding="0" width="100%" summary="structure table">
	<tr>		
		<td width="225" align="left" valign="top">
			<table cellpadding="0" cellspacing="0" border="0" width="225" summary="structure table with search">
				<tr>	
					<td colspan="3"><a title="Ireland 2004 Presidency of the European Union" href="../templates/homepage.asp?sNavlocator=1"><img src="../images/header/bottom_logo.gif" alt="www.eu2004.ie" width="225" height="51" border="0"></a></td>
				</tr>
				<tr><td class="bg1"  colspan="3"><img src="../images/spacer.gif" width="1" height="6" alt=" "></td></tr>
				<tr>	
					<td class="bg1"  width="20"><img src="../images/spacer.gif" alt=" " width="20" height="1"></td>
					<td class="bg1" ><span class="nav"><strong><label for="searchquery">Search Site</label></strong></span></td>
					<td class="bg1"  width="23">&nbsp;</td>
				</tr>
				
				<tr valign="bottom">
					<td class="bg1"  width="20"><img src="../images/spacer.gif" alt=" " width="20" height="1"></td>				
					<td valign="bottom" width="182" class="bg1" >
						<form id="search" method="post" action="../sitetools/search.asp">
						<table cellpadding="0" cellspacing="0" border="0" width="100%" summary="structure table with search">
							<tr>
								<td valign="middle" height="20"><input type="hidden" name="searchaction" value="searching"><input type="text" name="searchquery" id="searchquery" size="12" maxlength="4000" style="width:140px;"></td>
								<td valign="middle" height="20" align="right"><input src="../images/common/go_arrow.gif" alt="Search Site" title="Search Site" id="image1" name="image1" type="image" WIDTH="20" HEIGHT="20">&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td height="23" colspan="2"><img src="../images/common/arrow_right_navy.gif" alt="Advanced Search" WIDTH="4" HEIGHT="7">&nbsp;&nbsp;<a class="nav" href="../sitetools/search.asp?searchtype=advanced" title="Advanced Search">Advanced Search</a></td>
							</tr>
						</table></form></td>
					<td valign="bottom" class="bg1" ><img src="../images/header/noslant.gif" alt=" " WIDTH="23" HEIGHT="27"></td>
				</tr>
				
				<tr>
					<td colspan="2" class="bg_quicknav" ><img src="../images/spacer.gif" width="1" height="3" alt=" "></td>
					
						<td><img src="../images/spacer.gif" width="1" height="2" alt=" "></td>
												
				</tr>
				<tr>
					<td class="bg_quicknav"  width="20"><img src="../images/spacer.gif" alt=" " width="20" height="1"></td>
					<td class="bg_quicknav"  width="182"><strong><label for="quick_nav"><span class="inner_title">Quick Navigation</span></label></strong><br><img SRC="../images/spacer.gif" width="1" height="3" alt=" "></td>
					
						<td>&nbsp;</td>
							
				</tr>
				<tr>
					<td class="bg_quicknav"  width="20"><img src="../images/spacer.gif" alt=" " width="20" height="1"></td>
					<td class="bg_quicknav"  width="182">
						<form id="frmQuickNav" method="post" action="../content/index.asp"><table cellpadding="0" cellspacing="0" border="0" width="100%" summary="structure table with quick navigation">
							<tr>
								<td class="bg_quicknav"  width="182">
												
								<select name="sNavlocator" id="quick_nav" class="quick_nav_select">
									<option value="1">Please select...</option>
								
									<option value="1">Homepage</option>
									
									<option value="5">The Presidency</option>
									
									<option value="5,10">&nbsp;&nbsp;-&nbsp;What is the Presidency?</option>
									
									<option value="5,11">&nbsp;&nbsp;-&nbsp;Ireland's Presidency</option>
									
									<option value="5,14">&nbsp;&nbsp;-&nbsp;Who's Who for the Irish Presidency?</option>
									
									<option value="5,273">&nbsp;&nbsp;-&nbsp;Guide to Ireland's Presidency of the EU (Vade Mecum)</option>
									
									<option value="5,236">&nbsp;&nbsp;-&nbsp;Presidency Documents</option>
									
									<option value="5,418">&nbsp;&nbsp;-&nbsp;Meetings and Events</option>
									
									<option value="5,420">&nbsp;&nbsp;-&nbsp;Cultural Programme</option>
									
									<option value="5,433">&nbsp;&nbsp;-&nbsp;Day of Welcomes</option>
									
									<option value="4">Policy Areas</option>
									
									<option value="4,18">&nbsp;&nbsp;-&nbsp;General Affairs and External Relations</option>
									
									<option value="4,19">&nbsp;&nbsp;-&nbsp;Economic and Financial Affairs</option>
									
									<option value="4,20">&nbsp;&nbsp;-&nbsp;Justice and Home Affairs</option>
									
									<option value="4,21">&nbsp;&nbsp;-&nbsp;Employment, Social Policy, Health and Consumer Affairs</option>
									
									<option value="4,22">&nbsp;&nbsp;-&nbsp;Competitiveness (Internal Market, Industry and Research)</option>
									
									<option value="4,23">&nbsp;&nbsp;-&nbsp;Transport, Telecommunications and Energy</option>
									
									<option value="4,24">&nbsp;&nbsp;-&nbsp;Agriculture and Fisheries</option>
									
									<option value="4,26">&nbsp;&nbsp;-&nbsp;Environment</option>
									
									<option value="4,25">&nbsp;&nbsp;-&nbsp;Education, Youth and Culture</option>
									
									<option value="4,355">&nbsp;&nbsp;-&nbsp;Policy Documents</option>
									
									<option value="6">The EU</option>
									
									<option value="6,27">&nbsp;&nbsp;-&nbsp;About the EU</option>
									
									<option value="6,59">&nbsp;&nbsp;-&nbsp;Enlargement</option>
									
									<option value="6,28">&nbsp;&nbsp;-&nbsp;Member States</option>
									
									<option value="6,29">&nbsp;&nbsp;-&nbsp;New Member States</option>
									
									<option value="6,30">&nbsp;&nbsp;-&nbsp;Candidate Countries</option>
									
									<option value="6,387">&nbsp;&nbsp;-&nbsp;Communicating Europe</option>
									
									<option value="66">Latest News</option>
									
									<option value="66,402">&nbsp;&nbsp;-&nbsp;Statements in International Organisations</option>
									
									<option value="66,80">&nbsp;&nbsp;-&nbsp;Webcasts</option>
									
									<option value="66,469">&nbsp;&nbsp;-&nbsp;Latest News Photos</option>
									
									<option value="9">Media Services</option>
									
									<option value="9,194">&nbsp;&nbsp;-&nbsp;Photo Archive</option>
									
									<option value="9,68">&nbsp;&nbsp;-&nbsp;Press Accreditation</option>
									
									<option value="9,274">&nbsp;&nbsp;-&nbsp;Travelling to Ireland</option>
									
									<option value="9,69">&nbsp;&nbsp;-&nbsp;Media Information</option>
									
									<option value="9,89">&nbsp;&nbsp;-&nbsp;Venues</option>
									
									<option value="7">Ireland</option>
									
									<option value="7,98">&nbsp;&nbsp;-&nbsp;Land & People</option>
									
									<option value="7,90">&nbsp;&nbsp;-&nbsp;History</option>
									
									<option value="7,91">&nbsp;&nbsp;-&nbsp;The State</option>
									
									<option value="7,92">&nbsp;&nbsp;-&nbsp;International Relations</option>
									
									<option value="7,93">&nbsp;&nbsp;-&nbsp;Northern Ireland</option>
									
									<option value="7,94">&nbsp;&nbsp;-&nbsp;Economy</option>
									
									<option value="7,95">&nbsp;&nbsp;-&nbsp;Services</option>
									
									<option value="7,96">&nbsp;&nbsp;-&nbsp;Arts & Culture</option>
									
									<option value="7,97">&nbsp;&nbsp;-&nbsp;Links</option>
									
									<option value="88">IGC</option>
									
									<option value="3">Youth</option>
									
									<option value="3,243">&nbsp;&nbsp;-&nbsp;EU4U</option>
									
									<option value="3,241">&nbsp;&nbsp;-&nbsp;EU+10</option>
									
									<option value="3,148">&nbsp;&nbsp;-&nbsp;Ireland - the Highlights</option>
									
									<option value="3,242">&nbsp;&nbsp;-&nbsp;Ireland - the Lowdown</option>
									
									<option value="3,285">&nbsp;&nbsp;-&nbsp;Learning Zone</option>
									
									<option value="3,385">&nbsp;&nbsp;-&nbsp;Have your say</option>
									
								</select></td>
								<td class="bg_quicknav"  width="182" align="right"><input src="../images/common/go_arrow.gif" alt="Go" title="Go" id="go_arrow" type="image" WIDTH="20" HEIGHT="20">&nbsp;&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2"><img SRC="../images/spacer.gif" width="1" height="3" alt=" "></td>
							</tr>
						</table></form>
					</td>
					
					
					
						<td>&nbsp;</td>
								
				</tr>		
				<tr>
					<td colspan="2" class="bg_quicknav" ><img src="../images/spacer.gif" width="1" height="4" alt=" "></td>
					
						<td><img src="../images/spacer.gif" width="1" height="4" alt=" "></td>
												
				</tr>
				<tr>
					
						<td  colspan="2"  class="horiz_dot"><img src="../images/spacer.gif" width="203" height="1" alt=" "></td>
												
				</tr>
				<tr>
					<td colspan="3"><img src="../images/spacer.gif" width="1" height="7" alt=" "></td>
				</tr>
			</table>
			<!-- End Generic Left Navigation -->
			
				<!-- Begin Left Navigation -->
				<table border="0" cellspacing="0" cellpadding="0" width="225" summary="structure table">
					<tr>
						<td><img src="../images/spacer.gif" width="11" height="1" alt=" "></td>
						<td valign="top">	
							<table border="0" width="192" cellspacing="0" cellpadding="0" summary="structure table">
								<tr>
									<td colspan="2"><img src="../images/spacer.gif" width="9" height="1" alt=" "><a href="../content/index.asp?sNavlocator=" title="" class="nav_primary"><strong></strong></a></td>
								</tr>	
								<tr>
									<td colspan="2"><img src="../images/spacer.gif" width="1" height="4" alt=" "></td>
								</tr>	
								
								<tr>
									<td colspan="2"><img src="../images/spacer.gif" width="1" height="5" alt=" "></td>
								</tr>
								
								<tr>
									<td colspan="2" class="horiz_dot"><img src="../images/spacer.gif" width="100%" height="1" alt=" "></td>
									<!--<td><img src="../images/spacer.gif" width="35" height="1" alt=""></td>-->
								</tr>
								<tr>
									<td colspan="2"><img src="../images/spacer.gif" width="1" height="5" alt=" "></td>
								</tr>
							</table>
						</td>
						<td><img src="../images/spacer.gif" width="22" height="1" alt=" "></td>
					</tr>
				</table>
				<!-- End Left Navigation -->
				
		</td>
		<td align="left" valign="top" width="100%">
		<!-- Start Content -->
<table cellspacing="0" cellpadding="0" border="0" width="100%" summary="Content">
	<tr>
		<td colspan="6" class="horiz_dot"></td>
	</tr>
	<tr>
		<td valign="top" colspan="6">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
				
					<td valign="top"><img SRC="../files/index.asp?id=1067" alt="Irish Presidency logo" border="0" width="124" height="122"></td>
					<td><img src="../images/spacer.gif" height="1" width="12" alt=" "></td>
					<td valign="top"><br><h1>Sponsorship</h1><br><p>The Irish Presidency wishes to acknowledge the sponsorship of the Presidency by the following companies:</p><br></td>
					<td width="3"><img src="../images/spacer.gif" height="1" width="3" alt=" "></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="6" class="horiz_dot"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td width="1" class="vertical_dot"><img src="../images/spacer.gif" height="1" width="1" alt=" "></td>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" width="48%" align="center"><a href="http://www.audi.ie"><img border="0" SRC="../images/sponsorship/audi.gif" alt="Audi logi" WIDTH="140" HEIGHT="90"></a><br><a href="http://www.audi.ie" title="Audi Ireland" class="inner_title"><strong>Audi Ireland</strong></a><br><br></td>
		<td width="10"><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		<td width="1" class="vertical_dot"><img src="../images/spacer.gif" height="1" width="1" alt=" "></td>
		<td width="10"><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		<td valign="top" width="48%" align="center"><a href="http://www.eircom.ie"><img border="0" SRC="../images/sponsorship/eircom.gif" alt="Eircom logo" WIDTH="120" HEIGHT="90"></a><br><a href="http://www.eircom.ie" title="Eircom" class="inner_title"><strong>Eircom</strong></a><br></td>
		<td width="3"><img src="../images/spacer.gif" height="1" width="3" alt=" "></td>
	</tr>
	<tr>
		<td colspan="6" class="horiz_dot"></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
		<td width="1" class="vertical_dot"><img src="../images/spacer.gif" height="1" width="1" alt=" "></td>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" align="center"><a href="http://www.kerrygold.com"><img border="0" SRC="../images/sponsorship/kerrygold.gif" alt="Kerrygold logo" WIDTH="150" HEIGHT="90"></a><br><a href="http://www.kerrygold.com" title="Kerrygold" class="inner_title"><strong>Kerrygold</strong></a><br><br></td>
		<td width="6"><img src="../images/spacer.gif" height="1" width="6" alt=" "></td>
		<td width="1" class="vertical_dot"><img src="../images/spacer.gif" height="1" width="1" alt=" "></td>
		<td width="5"><img src="../images/spacer.gif" height="1" width="5" alt=" "></td>
		<td valign="top" align="center"><a href="http://www.tipperary-water.ie"><img border="0" SRC="../images/sponsorship/tipperary.gif" alt="Tipperary Water logo" WIDTH="170" HEIGHT="90"></a><br><a href="http://www.tipperary-water.ie" title="Tipperary Water" class="inner_title"><strong>Tipperary Water</strong></a><br></td>
		<td width="3"><img src="../images/spacer.gif" height="1" width="3" alt=" "></td>
	</tr>
	<tr>
		<td colspan="6" class="horiz_dot"></td>
	</tr>
	<tr>
		<td colspan="6"><br><p>The Irish Presidency also wishes to acknowledge the contribution of the following companies to specific elements of the Irish Presidency:</p><p></p></td>
	</tr>
	<tr>
		<td colspan="6" class="horiz_dot"></td>
	</tr>
</table>
<table cellspacing="0" cellpadding="0" border="0" width="100%" summary="Content">
	<tr>
		<td valign="top" width="24%" align="center"><a href="http://www.cooleywhiskey.com"><img SRC="../images/sponsorship/cooley.gif" alt="Cooley logo" border="0" WIDTH="82" HEIGHT="75"></a><br><strong><a href="http://www.cooleywhiskey.com/" title="Cooley" class="inner_title">Cooley</a></strong><br><br></td>
		<td width="10"><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		
		<td width="1" class="vertical_dot"><img src="../images/spacer.gif" height="1" width="1" alt=" "></td>
		
		<td width="10"><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		<td valign="top" width="24%" align="center"><a href="http://www.jameson.ie"><img SRC="../images/sponsorship/jameson.gif" alt="Jameson logo" border="0" WIDTH="82" HEIGHT="75"></a><br><strong><a href="http://www.jameson.ie" title="Jameson" class="inner_title">Jameson</a></strong></td>
		<td width="10"><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		
		<td width="1" class="vertical_dot"><img src="../images/spacer.gif" height="1" width="1" alt=" "></td>
		
		<td width="10"><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		<td valign="top" width="24%" align="center"><a href="http://www.jjkavanagh.ie"><img SRC="../images/sponsorship/kavanagh.gif" alt="Kavanagh logo" border="0" WIDTH="82" HEIGHT="75"></a><br><strong><a href="http://www.jjkavanagh.ie" title="Kavanagh" class="inner_title">Kavanagh</a></strong></td>
		<td width="10"><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		
		<td width="1" class="vertical_dot"><img src="../images/spacer.gif" height="1" width="1" alt=" "></td>
		
		<td width="10"><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		<td valign="top" width="24%" align="center"><a href="http://www.microsoft.com/ireland"><img SRC="../images/sponsorship/microsoft.gif" alt="Microsoft logo" border="0" WIDTH="82" HEIGHT="75"></a><br><strong><a href="http://www.microsoft.com/ireland" title="Microsoft" class="inner_title">Microsoft</a></strong></td>	
	</tr>
	<tr>
		<td colspan="13" class="horiz_dot"></td>
	</tr>
	<tr>
		<td valign="top" align="center"><a href="http://www.dell.ie"><img SRC="../images/sponsorship/dell.gif" alt="Dell" border="0" WIDTH="82" HEIGHT="75"></a><br><strong><a href="http://www.dell.ie" title="Dell" class="inner_title">Dell</a></strong><br><br></td>
		<td><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		
		<td class="vertical_dot"><img src="../images/spacer.gif" height="1" width="1" alt=" "></td>
		
		<td><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		<td valign="top" align="center">&nbsp;</td>
		<td><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		
		<td class="vertical_dot"><img src="../images/spacer.gif" height="1" width="1" alt=" "></td>
		
		<td><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		<td valign="top" align="center">&nbsp;</td>
		<td><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		
		<td class="vertical_dot"><img src="../images/spacer.gif" height="1" width="1" alt=" "></td>
		
		<td><img src="../images/spacer.gif" height="1" width="10" alt=" "></td>
		<td valign="top" align="center">&nbsp;</td>	
	</tr>
	<tr>
		<td colspan="13" class="horiz_dot"></td>
	</tr>
</table>

	<!-- end content -->
	
	<br></td>
			<td class="vertical_dot" width="1"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
			<td width="168" align="left" valign="top" height="100%" class="bg1">			
				<table cellpadding="0" cellspacing="0" border="0" width="168" summary="structure">
					<tr>					
						<td valign="top">
							<table cellpadding="0" cellspacing="0" border="0" summary="right navigation">
								<tr>
									<td class="bg1" height="27">
										<table cellpadding="0" cellspacing="0" BORDER="0" class="bg1">
												<tr>
													<td width="10"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
													<td width="100%" class="nav"><STRONG>Sitetools</STRONG></td>
												</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
								</tr>
								<tr>
									<td>
										<table cellpadding="0" cellspacing="0" class="bg_sitetoolsmenu" border="0">
											<tr>
												<td height="22" width="10" class="bg_sitetoolsmenu"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td width="4" class="bg_sitetoolsmenu"><img src="../images/right_side_nav/blue_arrow.gif" width="4" height="7" alt=" "></td>
												<td width="11" class="bg_sitetoolsmenu"><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
												<td width="143" class="bg_sitetoolsmenu"><a title="Personalise / My&nbsp;eu2004.ie" href="../home/myeu2004.asp" class="nav_secondary"><STRONG>Personalise / My&nbsp;eu2004.ie</STRONG></a></td>
											</tr>
											
											<tr>
												<td colspan="4" class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
											</tr>
											<tr>
												<td height="22" width="10" class="bg_sitetoolsmenu"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td width="4" class="bg_sitetoolsmenu"><img src="../images/right_side_nav/blue_arrow.gif" width="4" height="7" alt=" "></td>
												<td width="11" class="bg_sitetoolsmenu"><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
											
												<td class="bg_sitetoolsmenu"><a accesskey="i" title="Text Only" href="../includes/textonly.asp?version=text" class="nav_secondary"><STRONG>Text Only</STRONG></a></td>
											
											</tr>					
											<tr>
												<td colspan="4" class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
											</tr>							
											<tr>
												<td height="22" width="10" class="bg_sitetoolsmenu"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td width="4" class="bg_sitetoolsmenu"><img src="../images/right_side_nav/blue_arrow.gif" width="4" height="7" alt=" "></td>
												<td width="11" class="bg_sitetoolsmenu"><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
												<td class="bg_sitetoolsmenu"><a title="Sitemap" href="../sitetools/sitemap.asp" class="nav_secondary"><STRONG>Sitemap</STRONG></a></td>
											</tr>
											<tr>
												<td colspan="4" class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
											</tr>
											<tr>
												<td height="22" class="bg_sitetoolsmenu" width="10"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td class="bg_sitetoolsmenu" width="4"><img src="../images/right_side_nav/blue_arrow.gif" alt=" " width="4" height="7"></td>
												<td class="bg_sitetoolsmenu" width="11"><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
												<td class="bg_sitetoolsmenu"><a title="Help" href="../sitehelp/index.asp?sNavlocator=-1" class="nav_secondary"><STRONG>Help</STRONG></a></td>
											</tr>
											<tr>
												<td colspan="4" class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
											</tr>							
											<tr>
												<td height="22" class="bg_sitetoolsmenu" width="10"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td class="bg_sitetoolsmenu" width="4"><img src="../images/right_side_nav/blue_arrow.gif" alt=" " width="4" height="7"></td>
												<td class="bg_sitetoolsmenu" width="11"><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
												<td class="bg_sitetoolsmenu"><a title="Site A-Z" href="../sitetools/atoz.asp" class="nav_secondary"><STRONG>Site A-Z</STRONG></a></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
								</tr>
								<tr>
									<td class="bg_nav1" height="22">
										<table cellpadding="0" cellspacing="0" BORDER="0">
												<tr>
													<td width="10"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
													<td width="100%" class="nav_gold"><STRONG>Meeting Calendar</STRONG></td>
												</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
								</tr>
								<tr>
									<td class="bg_nav2">
										<table cellpadding="0" cellspacing="0" BORDER="0">
											<tr>
												<td width="10"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td width="4"><img src="../images/right_side_nav/gold_arrow.gif" alt=" " width="4" height="7"></td>
												<td width="11"><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
												<td width="143"><a title="Today" href="../sitetools/meeting_search.asp?search=today" class="nav_small">Today</a></td>
											</tr>
											<tr>
												<td><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td><img src="../images/right_side_nav/gold_arrow.gif" alt=" " width="4" height="7"></td>
												<td><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
												<td><a title="This Week" href="../sitetools/meeting_search.asp?search=thisweek" class="nav_small">This Week</a></td>
											</tr>
											<tr>
												<td><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td><img src="../images/right_side_nav/gold_arrow.gif" alt=" " width="4" height="7"></td>
												<td><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
												<td><a title="This Month" href="../sitetools/meeting_search.asp?search=thismonth" class="nav_small">This Month</a></td>
											</tr>
											<tr>
												<td><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td><img src="../images/right_side_nav/gold_arrow.gif" alt=" " width="4" height="7"></td>
												<td><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
												<td><a title="Search" href="../sitetools/meeting_search.asp?action=search" class="nav_small">Search</a></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
								</tr>
								<tr>
									<td class="bg_nav2">
										<table cellpadding="0" cellspacing="0" BORDER="0">
												<tr>
													<td width="10"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
													<td width="4"><img src="../images/right_side_nav/gold_arrow.gif" alt=" " width="4" height="7"></td>
													<td width="11"><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
													<td width="100%"><a title="Venues" href="../templates/standard.asp?sNavlocator=9,89" class="nav_secondary"><STRONG>Venues</STRONG></a></td>
												</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
								</tr>							
								<tr>
									<td class="bg_nav1">
									
										<table cellpadding="0" cellspacing="0" BORDER="0" width="100%">
											<tr>
												<td width="10"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td width="4"><img src="../images/right_side_nav/blue_arrow.gif" alt=" " width="4" height="7"></td>
												<td width="11"><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
												<td><a title="Most Accessed " href="../sitetools/most_used.asp?mostusedtype=mostaccessed" class="nav_secondary"><STRONG>Most Accessed </STRONG></a></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
								</tr>							
								<tr>
									<td class="bg_nav1">
									
										<table cellpadding="0" cellspacing="0" BORDER="0" width="100%">
											<tr>
												<td width="10"><img src="../images/spacer.gif" alt=" " width="10" height="20"></td>
												<td width="4"><img src="../images/right_side_nav/blue_arrow.gif" alt=" " width="4" height="7"></td>
												<td width="11"><img src="../images/spacer.gif" alt=" " width="11" height="1"></td>
												<td><a title="Most Sent " href="../sitetools/most_used.asp?mostusedtype=mostsent" class="nav_secondary"><STRONG>Most Sent </STRONG></a></td>
											</tr>
										</table>
									</td>
								</tr>														
								<tr>
									<td class="horiz_dot"><img src="../images/spacer.gif" alt=" " width="1" height="1"></td>
								</tr>
								<tr>
									<td>
								
										<script language="javascript" type="text/javascript">
										<!--//
										
										/* opens popup window of specified link */
										function openWindow(link) {
											window.open(link,"popup","height=500,width=760,menubar=no,status=no,scrollbars=no,toolbar=no,location=no,directories=no,resizeable=no"); 
										}
										//-->
										</script>
										<table cellpadding="0" cellspacing="0" BORDER="0" class="bg1">
												<tr>
													<td colspan="2"><img src="../images/spacer.gif" alt=" " width="1" height="12"></td>
												</tr>
												<tr>
													<td width="12"><img src="../images/spacer.gif" alt=" " width="12" height="1"></td>
													<td width="100%">
													
														<a href="javascript:openWindow('http%3A%2F%2Fwww%2Eservecast%2Ecom%2Feu%2Fpresidency%2Fenglish%2Fmain%2Flogin%2Easp')" title="Please note that this link opens a new browser window"><img src="../files/index.asp?id=1028" alt="External Link: Servecast Webcasts" title="External Link: Servecast Webcasts" border="0" width="147" height="62"></a>
													
													</td>
												</tr>
												<tr>
													<td colspan="2"><img src="../images/spacer.gif" alt=" " width="1" height="16"></td>
												</tr>
												<tr>
													<td width="12"><img src="../images/spacer.gif" alt=" " width="12" height="1"></td>
													<td width="100%">
													
														<a href="http://www.eu2004.ie/templates/standard.asp?sNavlocator=6,387"  title="Link to Ireland Communicating Europe"><img src="../files/index.asp?id=1872" alt="Link to Ireland Communicating Europe" title="Link to Ireland Communicating Europe" border="0" width="147" height="62"></a>
													
													
													</td>
												</tr>
												<tr>
													<td colspan="2"><img src="../images/spacer.gif" alt=" " width="1" height="12"></td>
												</tr>
												<tr>
													<td width="12"><img src="../images/spacer.gif" alt=" " width="12" height="1"></td>
													<td width="100%">
													
														<a href="http://www.eu2004.ie/templates/subhome2.asp?sNavlocator=5,420"  title="Link to Presidency Cultural Programme"><img src="../files/index.asp?id=1792" alt="Link to Presidency Cultural Programme" title="Link to Presidency Cultural Programme" border="0" width="147" height="62"></a>
														
													
													</td>
												</tr>
												<tr>
													<td colspan="2"><img src="../images/spacer.gif" alt=" " width="1" height="12"></td>
												</tr>
												<tr>
													<td width="12"><img src="../images/spacer.gif" alt=" " width="12" height="1"></td>
													<td width="100%">
													
														<a href="http://www.eu2004.ie/templates/standard.asp?sNavlocator=5,433,481"  title="Link to Day of Welcomes page"><img src="../files/index.asp?id=3838" alt="Link to Day of Welcomes page" title="Link to Day of Welcomes page" border="0" width="147" height="62"></a>
														
													
													</td>
												</tr>
												<tr>
													<td colspan="2"><img src="../images/spacer.gif" alt=" " width="1" height="12"></td>
												</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4" align="left" valign="top">
				<table border="0" cellspacing="0" cellpadding="0" summary=" " width="100%">								
					<tr>
						<td colspan="2" class="bg2"><img src="../images/spacer.gif" width="1" height="1" alt=" "></td>
					</tr>
					<tr>
						<td colspan="2"><img src="../images/spacer.gif" width="1" height="5" alt=" "></td>
					</tr>
					<tr>
						<td width="1%">&nbsp;</td>
						<td align="left" valign="top">
						<a href="../sitetools/about.asp" class="nav" title="About eu2004.ie">About eu2004.ie</a>			
						<img src="../images/common/divider_bar_gold.gif" width="1" height="11" alt=" ">	
						<a href="../sitetools/legal.asp" class="nav" title="Legal">Legal</a>	
						<img src="../images/common/divider_bar_gold.gif" width="1" height="11" alt=" ">	
						<a href="../sitetools/sponsorship.asp" class="nav" title="Sponsorship">Sponsorship</a>
						<img src="../images/common/divider_bar_gold.gif" width="1" height="11" alt=" ">					
						<a href="../sitetools/contact.asp" class="nav" title="Contact Us">Contact Us</a>					
						<img src="../images/common/divider_bar_gold.gif" width="1" height="11" alt=" ">					
						<a href="../sitetools/government.asp" class="nav" title="&copy; Government of Ireland 2004">&copy; Government of Ireland 2004</a>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><a href="http://www.w3.org/WAI/WCAG1A-Conformance" target="new" title="Level A conformance icon, W3C-WAI Web Content Accessibility Guidelines 1.0 "><img border="0" height="32" width="88" src="../images/header/wai-a.gif" alt="Level A conformance icon, W3C-WAI Web Content Accessibility Guidelines 1.0 "></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://jigsaw.w3.org/css-validator/" target="new" title="Valid CSS"><img border="0" height="31" alt="Valid CSS" width="88" src="../images/header/w3c-css.gif" title="Valid CSS"></a></td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
<SCRIPT language="JavaScript" type="text/javascript" src="../includes/ProphetInsert36.js"></SCRIPT>
</body>
</html>

