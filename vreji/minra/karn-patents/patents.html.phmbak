<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   <meta name="GENERATOR" content="Mozilla/4.7 [en] (X11; U; Linux 2.2.13 i686) [Netscape]">
   <title>Patents</title>
</head>
<body>

<h2>
The US Patent System Is Out of Control!</h2>

<blockquote>If nature has made any one thing less susceptible than all
others of exclusive property, it is the action of the thinking power called
an idea, which an individual may exclusively possess as long as he keeps
it to himself; but the moment it is divulged, it forces itself into the
possession of everyone, and the receiver cannot dispossess himself of it...He
who receives an idea from me, receives instructions himself without lessening
mine; as he who lights his taper at mine, receives light without darkening
me. That ideas should be spread from one to another over the globe, for
the moral and mutual instruction of man, and improvement of his condition,
seems to have been peculiarly and benevolently designed by nature...Inventions
then cannot, in nature, be a subject of property. --Thomas Jefferson</blockquote>

<blockquote>It was never the object of those laws to grant a monopoly for
every trifling device, every shadow of a shade of an idea, which would
naturally and spontaneously occur to any skilled mechanic or operator in
the ordinary progress of manufactures. Such an indiscriminate creation
of exclusive privileges tends rather to obstruct than to stimulate invention.
It creates a class of speculative schemers who make it their business to
watch the advancing wave of improvement, and gather its foam in the form
of patented monopolies, which enable them to lay a heavy tax upon the industry
of the country, without contributing anything to the real advancement of
the arts. It embarrasses the honest pursuit of business with fears and
apprehensions of concealed liens and unknown liabilities lawsuits and vexatious
accountings for profits made in good faith. (Atlantic Works v. Brady, 1017
U.S. 192, 200 (1882)).</blockquote>
It certainly appears from this Supreme Court opinion, written over a century
ago, that the <a href="http://www.uspto.gov/">US patent office</a> was
already out of control. Sad to say, things have only gotten worse. Thanks
largely to the
<a href="http://lpf.ai.mit.edu">League for Programming Freedom</a>
(yes, I'm a member), software patents have gotten at least some of the
notoriety they deserve. But the more patents I read, the more I come to
the conclusion that things are just as bad in the more traditional hardware
areas.
<p>It seems that every day somebody finds a patent that just makes everyone's
jaws drop open in utter astonishment. Here's one I just discovered: US&nbsp;patent&nbsp;
<a href="http://www.patents.ibm.com/cgi-bin/viewpat.cmd/US05443036__">5,443,036</a>&nbsp;
covers the use of a laser pointer in playing with a cat. Check it out;
this is not a joke, unless you consider (as I do) the entire US patent
system to be one <i>very</i> sick joke.
<p>To their credit, in 1994 the Patent Office put out a call for comments
on "obviousness" standards for patents, asking if perhaps they have been
inappropriately lowered. (Is the Pope Polish? Are your taxes too high?
Does a bear...well, you know.) Here are the
<a href="http://people.qualcomm.com/karn/patents/patent-comments.html">comments</a>
I filed in response. Naturally, they were ignored.
<p>Recently a&nbsp; <a href="http://people.qualcomm.com/karn/patents/www.slashdot.org">Slashdot</a>
reader hit on a brilliant analogy that ties it all together for me: patents,
he said, are merely a form of industrial pollution. After all, both pollution
and patents are economic externalities that can enrich individuals or companies
at the expense of society as a whole.&nbsp; And both are often defended
as economic necessities.&nbsp; At one time, society celebrated the belching
black clouds of smoke and soot from steam locomotives, power plants and
steel mills as signs of progress and economic prosperity, but this changed.
I fervently hope that I live to see a similar sea change in public attitudes
toward the patent system.
<p>Some people have said to me, "How can you attack patents?&nbsp;As a
Qualcomm stockholder, they've made you a lot of money". Yes, but this has
come at an unacceptable personal cost. Last summer I&nbsp;was subpoenaed
to a grueling and pointless deposition in a patent suit by Bellcore, my
former employer, against Fore Systems, a maker of ATM switches. When I
was asked my view of the patent system, I responded, "I find it totally
loathesome, and it has totally drained the joy out of engineering for me".
And I was exaggerating only slightly. Patent angst has<i> significantly</i>
reduced my job satisfaction, enough to make me seriously consider giving
up the engineering profession. And as someone who knew by age 5 that engineering
was his life's calling, quite frankly that's just not something that money
can replace.
<h2>More good stuff on bad patents</h2>

The controversy over the <a href="http://www.noamazon.com/">Amazon
"1-click" patent</a> has unleashed a tidal wave of pent-up frustration
and anger over the Patent Office juggernaut. It's heartening to
see excellent articles on the situation appearing in the mainstream
media almost every day.
One example is
<a href="http://newsweek.com/nw-srv/issue/11_00a/focus/ty/foty0211_1.htm">
The Great Amazon Patent Debate</a> by Steven Levy that appeared in
<a href="http://www.newsweek.com/">Newsweek.</a>

<p><a href="http://cyber.law.harvard.edu/lessigbio.html">
Lawrence Lessig</a>, a Harvard Law professor, has been writing insightful
articles on intellectual property for some time. I highly recommend both
his articles and his book,
<a href="http://code-is-law.org/">The Code of Cyberspace</a>.
Though I certainly don't agree with everything he says (he has a little
too much faith in government as a force for good),
he's becoming somewhat
of a hero of mine -- to the extent that an engineer could ever consider a
<em>lawyer</em> to be a hero, that is.

<p>And there are the columns by
<a href="mailto:dgillmor@sjmercury.com">Dan Gillmor</a>
for the San Jose Mercury News. He has been
<a href="http://weblog.mercurycenter.com/ejournal/">
writing extensively</a> about the Amazon situation.

Dan's columns, which have also included excellent commentaries on the
parallel absurdities happening in copyright law, has posted links to
many other articles on the situation.

One truly <em>outstanding</em> example is <a
href="http://www.nytimes.com/library/magazine/home/20000312mag-patents.html">Patently
Absurd</a> by James Gleick that appears in the March 12, 2000 edition
of the New York Times Magazine. Even the article's illustrations drive
the point home; it takes more than a quick glance to tell the
satirical "patents" (Procedure for Simultaneously Walking and Chewing
Gum) from the real ones
(<a href="http://www.patents.ibm.com/cgi-bin/viewpat.cmd/US05443036__">Method of Exercising a Cat</a> and
<a href="http://www.patents.ibm.com/cgi-bin/viewpat.cmd/US05965809__">Method of Bra Size Determination By Direct Measurement of the Breast</a>).


<h2>

Some more thoughts on patents</h2>
<a href="http://people.qualcomm.com/karn/patents/rohrabacher.txt">Letter
to Rep Dana Rohrabacher</a>
<p><a href="http://people.qualcomm.com/karn/patents/bilbray.html">Letter
to Rep Brian Bilbray</a>
<p><a href="http://people.qualcomm.com/karn/patents/mnews.html">Letter
to Editor of San Jose Mercury News</a>
<p>I'm under no illusions that a few letters from one citizen can posssibly
have an effect in Washington; votes, dollars and appeals to emotion gain
much more attention than reasoned argument. But if you're also upset by
the sorry state of the US patent system, let your own congressman know
in your own words. Sure, you may not get a response either but at least
you'll feel a little bit better about it.
<p><i>Last updated: 11 Mar 2000</i>
<p><a href="http://people.qualcomm.com/karn/patents/index.html">Back to
Phil Karn's Home Page</a>
</body>
</html>
