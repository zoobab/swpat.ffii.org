
                   Developments in patent administration

                  Report on Comparative Study Carried Out
                       under Trilateral Project 24.2

   1. Introduction

   1.1 Current situation of the three Offices

   Computer software is one of the emerging technology, and the software
   industry is highly developing.

   The three Offices have each examination guidelines for computer
   software related inventions.

   The USPTO started applying its new examination guidelines for
   computer-related inventions on March 29, 1996.

   The EPO uses examination guidelines published in 1985.

   The JPO revised its examination guidelines for computer software
   related invention published in 1993, and published new guidelines on
   February 27, 1997. It started applying the new examination guidelines
   on April 1, 1997.

   1.2 Abstract of the project 24.2

   Under this current situation, the JPO worked as Leading Office
   according to the agreement of the Trilateral meeting in November 1995
   and Trilateral comparative study of examination practices in the field
   of computer-related technology was conducted, employing two
   hypothetical applications.

   It was conducted in view of Claim interpretation, Statutory subject
   matter, and, Novelty / Inventive step (Non-obviousness).

   2. Hypothetical application 1

   The hypothetical application 1 relates to "CURVE GENERATING METHOD
   USING TANGENT VECTORS".

   There are 6 claims in the hypothetical application 1, and two kind of
   descriptions are employed (See Appendix 1.1).

   The first description includes disclosures of a method of machining a
   three-dimensional curved surface (marked with underlines), but the
   second does not.

   No Office fundamentally evaluated the [1]two descriptions differently.
   For example, the EPO commented as follows:

   "The difference of the two descriptions appears to be irrelevant for
   the claim assessment, since the original claims (especially claim 3)
   already include the machining context so that a person skilled in
   machining dose not need the underlined passages of the first
   description."

   The USPTO also answered that the inclusion or exclusion of machining a
   three-dimensional curved surface in the description would not impact
   the stated rejections. The USPTO, however, noted as follows:

   "[P]rudent attorneys would be well advised to include the underlined
   material in their disclosure when presenting claims directed to
   machining a three-dimensional curved surface."

   Therefore, the comparative study based on the second description was
   not conducted.

   2.1 Claim interpretation
   claim 1 2 3 4 5 6
   USPTO 1 1 1 3 1 3
    EPO  1 1 1 4 1 1
    JPO  1 1 1 1 4 1

   1: The claim was literally interpreted; i.e. take all claim
   limitations into account.
   2: "means or steps plus function" limitation was not literally
   interpreted (point out the corresponding part of written description.)
   3: Statement of "intended use" in the preamble was neglected (point
   out the neglected part of the claim.)
   4: The claim was not literally interpreted for the lack of clarity
   (explain) .
   5: Others (explain).

   Almost all claims were literally interpreted and ambiguous claims were
   interpreted appropriately and similarly among the three Offices.

   The three Offices showed no difference in interpreting claim 1 through
   claim 3.

   The USPTO neglected or did not give weight to statements of "intended
   use" in interpreting claims 4 and 6, which lead the USPTO to the
   different statutory subject matter evaluation from the other Offices.

   As to claim 4, the USPTO neglected the statement of intended use in
   the preamble ("for machining a three dimensional curved surface") and
   thereby arrived at a claim having the same wording as claim 1. The EPO
   interpreted the claim as if it had included "machining of the three
   dimensional curved surface" as the final step.

   As to claim 5, there was little difference in the claim
   interpretation. The JPO pointed out that the claim was ambiguous
   whether generated curve was displayed or three-dimensional curved
   surface was displayed. The JPO interpreted the claim 5 as displaying
   three-dimensional curved surface.

   As to claim 6, the USPTO did not give weight to the statement of
   intended use in the preamble concerning a system comprising "a
   keyboard for data input, a processor, a ROM for storing a control
   program, a RAM, a working memory, an output unit for outputting
   generated curved surface data to an external storage medium, an
   address bus and a data bus".

   2.2 Statutory subject matter
   claim      1          2          3          4          5          6
   USPTO [batu.gif] [batu.gif] [maru.gif] [batu.gif] [batu.gif]
   [batu.gif]
    EPO  [batu.gif] [batu.gif] [maru.gif] [maru.gif] [maru.gif] [batu.gif]
    JPO  [batu.gif] [batu.gif] [maru.gif] [maru.gif] [batu.gif] [batu.gif]

   In the hypothetical application 1, Two hypothetical prior arts were
   given. The prior arts have no effect on the evaluation of statutory
   subject matter at the USPTO and the JPO.

   The EPO identifies closest prior art in evaluating statutory subject
   matter and it may be probable that the evaluation as to statutory
   subject matter would be dependent on the closest prior art.1

   The EPO employed a general purpose computer or conventional method
   described in the hypothetical application when it evaluated as to
   statutory subject matter based on hypothetical prior art No.1. When it
   comes to hypothetical prior art Nos.1+2, the EPO employed hypothetical
   prior art No.2.

   In this case different prior art was identified depending on the
   choice of hypothetical prior arts (prior art No.1 or prior art
   Nos.1+2) and different logic was employed, but it has no effect on the
   result of statutory subject matter evaluation.

   Although it is probable that the closest prior art has effect on the
   evaluation of statutory subject matter in the EPO, there was no
   difference in this case owing to the closest prior art.

   Each three Offices evaluated claim1 and 2 as being non-statutory, and
   claim 3 as being statutory.

   As to claim 4, the EPO and the JPO evaluated it as being statutory as
   well as claim 3.23 The USPTO

   neglected the intended use in the preamble (for machining ...),
   according to the Examination Guidelines for Computer-Related
   Inventions, IV.B.2.(d).(i) Intended Use or Field of Use Statements (
   Claim language that simply specifies an intended use or field of use
   for the invention generally will not limit the scope of a claim,
   particularly when only presented in the claim preamble.), and thereby
   arrived at a claim having the same wording as claim 1. Since claim 1
   was considered non-statutory, the USPTO considered claim 4 as being
   non-statutory as well.

   The EPO and the JPO made a similar evaluation as to claim 3 and 4,
   however, the USPTO made a different evaluation between claim 3 and 4.
   This is because the USPTO neglected Intended use in the preamble.

   As to claim 5, the USPTO and the JPO evaluated the claim giving weight
   to the word "displaying" in the claim. The USPTO analyzed in view of
   post computer process activity, and the JPO in view of information
   processing in which hardware resources are used. Both Offices
   concluded claim 5 as being non-statutory.

   The EPO alone condidered the claimed method of improved smooth curve
   calculation for display by a computer as being statutory, locating the
   "conventional method page 5 of description second paragraph" as the
   closest prior art.4

   Each three Offices evaluated claim 6 as non-statutory.
   As to claim 6, the USPTO evaluated that it is computer program per se,
   the EPO evaluated that the objective problem lay exclusively in fields
   excluded from patentability, and the JPO evaluated that the claim has
   no matters which suggests, direclty or indirectly, how the hardware
   resources of the computer is utilized in the processing, then the
   processing falls under "mere processing of informarion by using of a
   computer".

   2.3 Novelty / Inventive step (Non-obviousness)
   Two hypothetical prior arts were prepared for evaluating Novelty /
   Inventive step (non-obviousness), and each Office evaluated the
   hypothetical application based on both prior arts. Firstly, Novelty /
   Inventive step (non-obviousness) was evaluated based on the prior art
   No.1, then prior art Nos.1+2.

   2.3.1 As to prior art No.1

                 claim      1          2          3          4          5     6
  USPTO    novelty     [maru.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
   [maru.gif]
        inventive step [maru.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
   [maru.gif]
   EPO     novelty     [maru.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
   [maru.gif]
        inventive step [maru.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
   [maru.gif]
   JPO     novelty     [maru.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
   [maru.gif]
        inventive step [maru.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
   [maru.gif]

   having novelty [maru.gif] lacking novelty [batu.gif]
   having inventive step
   (non-obvious) [maru.gif] lacking inventive step
   (obvious) [batu.gif]

    Since the prior art No.1 discloses at most spline interpolation, the
    three Offices concluded all claims have novelty and inventive step.

                       2.3.2 As to prior art Nos.1+2

                 claim      1          2          3          4          5     6
  USPTO    novelty     [maru.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
   [maru.gif]
        inventive step [batu.gif] [batu.gif] [batu.gif] [batu.gif] [batu.gif]
   [batu.gif]
   EPO     novelty     [maru.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
   [maru.gif]
        inventive step [batu.gif] [batu.gif] [batu.gif] [batu.gif] [batu.gif]
   [batu.gif]
   JPO     novelty     [maru.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
   [maru.gif]
        inventive step [batu.gif] [batu.gif] [maru.gif] [maru.gif] [batu.gif]
   [batu.gif]

   having novelty [maru.gif] lacking novelty [batu.gif]
   having inventive step
   (non-obvious) [maru.gif] lacking inventive step
   (obvious) [batu.gif]

   The JPO stated that claim 3 and 4 fulfilled inventive step in view of
       machining curved surface, which made JPO's evaluation a little
                     different from the other Offices.

   3. Hypothetical application 2

   The hypothetical application 2 relates to an automatic ordering
   system. There are five claims in the application (See Appendix 1.2).

   3.1 Claim interpretation
   claim 1 2 3 4 5
   USPTO 2 2 1 2 2
   EPO5 1
   (4) 1
   (4) 1
   (4) 1
   (4) 1
   (4)
   JPO 1 1 1 1 1

   1: The claim was literally interpreted; i.e. take all claim
   limitations into account.
   2: "means or steps plus function" limitation was not literally
   interpreted (point out the corresponding part of written description.)
   3: Statement of "intended use" in the preamble was neglected (point
   out the neglected part of the claim.)
   4: The claim was not literally interpreted for the lack of clarity
   (explain) .
   5: Others (explain).

   The USPTO interpreted claims 1, 2, 4 and 5 based on means or steps
   plus function limitation.6

   The USPTO and the EPO at first pointed out ambiguity of the claims.7
   The claims were modified, taking the above indication into account.
   The both Offices consequently answered that most ambiguity was
   resolved by the modification.
   ambiguity was resolved by the modification.8

   The USPTO also stated that the claim was not in accordance with 35
   U.S.C. 112, first paragraph (written description and enablement
   requirements) since the software programming technology to enable one
   of ordinary skill in the art to make and use the invention was not
   adequately set forth. The USPTO explains as follows.

   "The kinds of descriptions that are typically required in order to
   meet the written description and enablement requirements of 35 U.S.C.
   112, first paragraph are highly fact dependent and must be assessed on
   a case by case basis. According to MPEP 2106.02 (at page 2100-9):

   [w]hile no specific universally applicable rule exists for recognizing
   an insufficiently disclosed application involving computer programs,
   and examining guideline to generally follow is to challenge the
   sufficiency of such disclosures which fail to include either the
   computer program itself or a reasonably detailed flowchart which
   delineates the sequence of operations the program must perform. In
   programming applications [when the] software disclosure only includes
   a flowchart, as the complexity of functions and the generality of the
   individual components of the flowchart increase, the basis for
   challenging the sufficiency of such a flowchart becomes more
   reasonable because the likelihood of more than routine experimentation
   being required to generate a working program from such a flowchart
   also increases."
   In this case, more detailed description of the software programming
   technology might be required in the US.

   The three Offices may have another opportunity to study written
   description and enablement requirements of the software related
   invention in the future.

   3.2 Statutory subject matter
   claim      1          2          3          4          5
   USPTO [maru.gif] [maru.gif] [batu.gif] [maru.gif] [maru.gif]
    EPO  [batu.gif] [maru.gif] [batu.gif] [batu.gif] [batu.gif]
    JPO  [maru.gif] [maru.gif] [batu.gif] [maru.gif] [maru.gif]

   As far as system caim 2 is concerned, all three Offices come to the
   same conclusion that it is statutory.

   The EPO evaluated only claim 2 as being statutory, and stated as
   follows:
   "- The objective problem lies, at least partly, within the field of
   statistics and mathematics. It is debatable whether the objective
   problem lies also within an engineering field owing to the specific
   type of data and field of implementation. The skills needed to
   understand the objective problem and to conceive of the solution could
   be regarded as the skills of an electric power engineer and a
   mathematician. Giving the benefit of the doubt to the applicant, it is
   judged that claim 2 fulfils the requirements of A. 52(2) and (3) EPC."

   As far as method caim 3 is concerned, all three Offices come to the
   same conclusion that it is non-statutory.

   The EPO judges the contribution to the art, which for claims 1, 4 and
   5 lie in the fields of business and/or mathematics that are excluded
   from patentability under A. 52(2) EPC, and therefore concludes that
   the claims are non-statutory.

   The USPTO states claims except for claim 3 are statutory because they
   fall within "specific machines or manufactures."

   The JPO comes to similar conclusions as to the USPTO, since the
   solution to the problem to be solved by the claimed inventions, with
   the exception of that for claim 3, are considered as "information
   processing in which hardware resources are used".

   3.3 Novelty / Inventive step (Non-obviousness)
                  claim      1          2          3          4          5
   USPTO    novelty     [batu.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
         inventive step [batu.gif] [batu.gif] [batu.gif] [batu.gif] [batu.gif]
    EPO     novelty     [maru.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
         inventive step [batu.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
    JPO     novelty     [batu.gif] [maru.gif] [maru.gif] [maru.gif] [maru.gif]
         inventive step [batu.gif] [batu.gif] [maru.gif] [maru.gif] [maru.gif]

   having novelty [maru.gif] lacking novelty [batu.gif]
   having inventive step
   (non-obvious) [maru.gif] lacking inventive step
   (obvious) [batu.gif]

   While the EPO and the JPO came to almost the same conclusions in
   judging the inventive step of the claims9, the USPTO considered all
   claims as being obvious.

   Evaluation of Novelty / Inventive step (non-obviousness) among the
   three Offices were slightly different concerning sales condition
   limitation i.e. weather, entertainment, events at competitive shops or
   bargain sales.

   The USPTO considered these limitations obvious. The other Offices
   considered most of the claims had inventive step. Paying attention to
   claim 2, however, it seems that the two Offices' evaluate claim
   limitation differently.

   4. Conclusion

   Concerning claim interpretation, the USPTO ignores statement of
   "intended use" (2.1 claim 4, 6), and does not literally interpret
   "means or steps plus function" limitation. The USPTO construe means or
   steps plus function limitations in accordance with 35 U.S.C. 112, six
   paragraph, as limited to the corresponding structure, material or acts
   described in the specification and equivalents. Accodingly, reference
   is made to the specification to determine what is encompassed by the
   claim language.

   In the EPO and the JPO, there is no "means or steps plus function"
   limitation determined by the description.

   As to statutory subject matter, there are cases in which the USPTO
   alone comes to a different conclusion (2.2 claim 4), and in which the
   EPO alone comes to a different conclusion (2.2 claim 5, 3.2 claims 1,
   4, 5). Judging from above cases, there exist clear differences in the
   methods of interpretation and evaluation of claims for statutory
   subject matter among the three Offices.

   There also exist clear differences in the evaluation of Novelty /
   Inventive step (non-obviousness) among the three Offices.

   1In the EPO the evaluation as to statutory subject matter is always
   dependent on the closest prior art, which is why the closest prior art
   must be identified. The only exception is when the clim 1 of
   hypothetical application 1, in which case no prior art need to be
   taken into account to decide that the claim is not allowable.

   2The EPO comments that it could have come to the same conclusion as
   the USPTO. However the EPO, considering the claim to be unclear,
   adopted an applicant-friendly approach, clarified it with the
   description, and thereby arrived at a claim having basically the same
   wording as claim3. Since claim3 was considered statutory, the EPO
   considered claim4 as being statutory as well. If this had been a real
   application, the EPO would have insisted that the applicant amend the
   claim to overcome the lack of clarity.

   3The JPO considered the intended use as providing enough limitation to
   consider the claim as it stands as it stands as being statutory.

   4As stated in page4, lines 5 to 7, the EPO used different closest
   prior art in the evaluation of statuory subject matter in the two
   hypothetical caes, including for claim5.

   5The claim modifications did overcome the EPO's clarity objectons, and
   the claim interpretation category for all claims changed "1".

   6In the EPO and the JPO, there is no "means or steps plus function"
   limitation determined by the description.

   7Answers of the EPO concerning claim interpretation as listed in
   Appendix2.1 relate to the orignal claim versions

   8Since the modified version of the claims has all modifications
   suggested by the EPO, the EPO has literal interpretation of modified
   claim versions.

   9Exception was claim 2, where the EPO saw a borderline case and
   decided for the applicant.

   10Statutory subject matter concerning computer program encoded on a
   computer-readable medium, the practices of which among the three
   Offices are apparent, was not studied this time.
     _________________________________________________________________

             [2]Home [3]Developments in patent administration 

Verweise

   1. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/saikine-repo242/app21.htm#descri
   2. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/homee.htm
   3. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/saikine-repo242/utp242_m.htm
