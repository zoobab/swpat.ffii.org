LtL: Gesetzestexte, Urteile und sonstige Verlautbarungen der Rechtspflegeorgane
Nia: Ces dernières années, l'Office Européen des Brevets a accordé environ 30000 brevets sur des algorithmes et des méthodes d'affaires - programmes d'ordinateurs, en contradiction avec la lettre comme avec l'esprit de la loi en vigueur. A présent, le mouvement européen pro-brevet voudrait d'un même geste légaliser ces attributions rétroactivement, et supprimer toute limitation effective à la brevetabilité. Les programmeurs ne pourront plus s'exprimer librement ni disposer librement de leurs propres oeuvres et quant aux citoyens, ils n'auront plus le droit de modeler eux-mêmes les formes de leur communication. Et quelle compensation nous promet-on à une telle %(e:dépossession intellectuelle) ? Moins d'innovation, moins de compatibilité, moins de bons logiciels.  
EWC: Europäische Patentkonvention
Aie: Article 52
Gmr: Régles pour l'examination dans l'Office Européen de Brevets
Pjt: IIème Partie, Chapitre IV
Pni: Brevetabilité
AuU: %(q:Grünbuch), in dem die EU-Kommission (Generaldirektion 15) und das Europäische Patentamt europaweit einheitlichen %(q:Patentschutz für Computerprogramme) fordert.
Dei: Darin wird gefordert, auch nicht-technische Computerprogramme müssten patentierbar sein, und bereits Kopieren eines Programmes müsse einen Verletzungstatbestand darstellen, damit %(q:die Rechte in der Gemeinschaft überall effektiv durchgesetzt werden können).  Die EU-Kommission wird mit der %(q:Ausarbeitung eines Richtlinienvorschlags auf der Grundlage von Artikel 100 EG-Vertrag) beauftragt.
Mto: Mario Monti kündigt die Ausarbeitung des Rili-Vorschlags %(q:vor Sommer 2000) an und behauptet, nach %(q:breiter Konsultation mit den betroffenen Kreisen) sei festgestellt worden, dass Software patentierbar werden müsse.  In Wirklichkeit wurden nur 44 Patentjuristen konsultiert.
Dio: Darin nehmen informationelle Güter eine prominente Stellung ein.
Snu: Schreibt recht strenge Prüfkriterien vor, u.a. dass eine Erfindung Naturgesetze auf Materie anwenden muss.
Ume: Umfassende und wohlgeordnete Sammlung deutschsprachige Gesetzestexte zum gewerblichen Rechtsschutz
VfW: Vollständige Sammlung französischer Patente, aktiv seit 1999-10-04
ppW3: patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application.
Fqy3: For the purposes of this Article, the terms %(q:inventive step) and %(q:capable of industrial application) may be deemed by a Member to be synonymous with the terms %(q:non-obvious) and %(q:useful) respectively
DfW: Die Welthandelsorgansation empfiehlt hiermit den europäischen Ansatz, Patente auf den Industriebereich zu beschränken, erlaubt es aber auch den Amerikanern, ihr System beizubehalten, indem sie den Begriff %(q:industriell) sehr weit auslegen.
Wnk: WARNUNG: technisch überzüchtet, man findet vor lauter Java, Javascript, ActiveX, Shockwave und sonstigem Firlefanz kaum etwas
fWd: Hier findet sich im linken Frame eine %(q:Collapsible List). Man klickt nicht den Text %(q:Collection of Laws) an, sondern die %(q:Rosette) links davon. Damit expandiert man die Liste.
dank: merci à %1 pour cette contribution et pour les suivantes
Gkc: Gesetzesinitiative französischer Parlamentarier, deren Artikel 4 vorsieht, dass Träger öffentlicher Funktionen sich nicht mittels patentierter Technik an die Bürger wenden dürfen.
Rfi: Recht auf Kompatibilität
WWe: Widersprüche zur Softwarepatentierung
WWe2: Widersprüche zur Softwarepatentierung
DmW: Die US-Regierung meint, E-Geschäftsverkehr erfordere Patente und will dieser Sichtweise im Rahmen von %{IITF} weltweite Geltung verschaffen:
DWi: Die EU kam offenbar dieser Forderung nach, als sie in ihr Grünbuch über den Elektronischen Geschäftsverkehr ähnliches schrieb.
WWh: WIPO/OMPI plant neue Schutzrechte für %(h:Internet:Autoren).
FrW: Französische Presseerklärung der WIPO/OMPI

### Local Variables: ***
### coding: utf-8 ***
### mailto: mlhtimport@a2e.de ***
### login: swpatgirzu ***
### passwd: XXXX ***
### srcfile: /ul/prg/src/mlht/app/swpat/swpatvreji.el ***
### feature: swpatdir ***
### doc: swpatflalu ***
### txtlang: fr ***
### End: ***
