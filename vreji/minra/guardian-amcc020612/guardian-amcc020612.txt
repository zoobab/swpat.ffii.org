   Small fry patently need protection
   Labour MEP Arlene McCarthy argues that harmonising laws across the EU
   will provide legal certainty for inventors
   Thursday June 12, 2003
   [42]The Guardian
   As the European Parliament's draftsperson on the EU directive on the
   patentability of computer implemented inventions, I would like to
   correct some of the myths, misinformation and inaccuracies in last
   week's article by Richard Stallman and Nick Hill.

   First, the EU directive is not proposing to patent all software, it is
   limited to genuine inventions. Software as such cannot be patented.

   Second, my "cosmetic" amendments to the European Commission text are
   proposing a more restrictive interpretation of the law in Europe, to
   try to stop the drift towards the US practice of patenting business
   methods.

   Third, I have included a new article to allow decompiling and reverse
   engineering as requested by computer programmers.

   Fourth, this directive will not have any adverse effects on open
   source software development. Even in the US, where the patenting
   system is more liberal, the Linux operating system proudly declares a
   50% growth in world wide shipment of its operating systems, with the
   Apache system dominating the internet web server market with 66%
   market share. With a more restrictive EU law, the open source
   community has nothing to fear.

   From medical inventions to household appliances, mobile phones and
   machine tools, inventions involving software are increasingly a fact
   of life. With many of our traditional industries migrating to the Far
   East leaving behind job losses, we Europeans are having to rely on
   licensing out inventiveness to generate income and create jobs.

   Numerous people from small to medium-sized enterprises have written to
   me in support of my proposal .

   It is time some of the "computer rights campaigners" got real. Patents
   for software inventions will not go away. It is infinitely better for
   the EU to harmonise laws across the EU with a view to limiting
   patentability, than to continue with the mess of national courts and
   European Patent Office (EPO) systems, and the drift towards US patent
   models.

   This directive will provide legal certainty for European software
   inventors. We have an obligation to legislate not just for one section
   of the software industry who seeks to impose its business model on the
   rest of industry, which moreover is not "free", but is actually a
   different form of monopoly by imposing a copyright licence system on
   users.

   We must legislate to ensure our inventors are not put at a
   disadvantage in the global market place. If we fail to offer European
   industry the possibility of patent protection, we will hand over our
   inventiveness and creativity to big business, who can cherrypick ideas
   and patent them. The perverse outcome would be that European
   originators of those inventions face infringements proceedings from
   the big players. It is this that will lead to job losses, less choice
   and higher prices!

   If the EU does not take the step to develop its competence with regard
   to computer-implemented inventions, then the EPO and its board of
   appeal will continue to be the main arbitrators of the law. This will
   continue to create confusion and uncertainty and will sidestep the
   democratic scrutiny of the EU.
     _________________________________________________________________

   [43]Printable version | [44]Send it to a friend | [45]Save story
   [46]UP 

   IFRAME:
   [47]http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=mi
   ddle&spacedesc=50&site=Online&navsection=4421&section=110837&country=(
   none)&rand=1004167
   [48][Params.richmedia=yes&amp;location=middle&amp;spacedesc=50&amp;sit
   e=Online&amp;navsection=4421&amp;section=110837&amp;country=(none)&amp
   ;rand=1004167] 

   IFRAME:
   [49]http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=mi
   ddle&spacedesc=05&site=Online&navsection=4421&section=110837&country=(
   none)&rand=1004167
   [50][Params.richmedia=yes&amp;location=middle&amp;spacedesc=05&amp;sit
   e=Online&amp;navsection=4421&amp;section=110837&amp;country=(none)&amp
   ;rand=1004167] 

   IFRAME:
   [51]http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=mi
   ddle&spacedesc=06&site=Online&navsection=4421&section=110837&country=(
   none)&rand=1004167
   [52][Params.richmedia=yes&amp;location=middle&amp;spacedesc=06&amp;sit
   e=Online&amp;navsection=4421&amp;section=110837&amp;country=(none)&amp
   ;rand=1004167] 

   IFRAME:
   [53]http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=mi
   ddle&spacedesc=07&site=Online&navsection=4421&section=110837&country=(
   none)&rand=1004167
   [54][Params.richmedia=yes&amp;location=middle&amp;spacedesc=07&amp;sit
   e=Online&amp;navsection=4421&amp;section=110837&amp;country=(none)&amp
   ;rand=1004167] 

   IFRAME:
   [55]http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=mi
   ddle&spacedesc=08&site=Online&navsection=4421&section=110837&country=(
   none)&rand=1004167
   [56][Params.richmedia=yes&amp;location=middle&amp;spacedesc=08&amp;sit
   e=Online&amp;navsection=4421&amp;section=110837&amp;country=(none)&amp
   ;rand=1004167] 

   IFRAME:
   [57]http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=mi
   ddle&spacedesc=09&site=Online&navsection=4421&section=110837&country=(
   none)&rand=1004167
   [58][Params.richmedia=yes&amp;location=middle&amp;spacedesc=09&amp;sit
   e=Online&amp;navsection=4421&amp;section=110837&amp;country=(none)&amp
   ;rand=1004167] 

   IFRAME:
   [59]http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=mi
   ddle&spacedesc=10&site=Online&navsection=4421&section=110837&country=(
   none)&rand=1004167
   [60][Params.richmedia=yes&amp;location=middle&amp;spacedesc=10&amp;sit
   e=Online&amp;navsection=4421&amp;section=110837&amp;country=(none)&amp
   ;rand=1004167] 

   IFRAME:
   [61]http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=mi
   ddle&spacedesc=11&site=Online&navsection=4421&section=110837&country=(
   none)&rand=1004167
   [62][Params.richmedia=yes&amp;location=middle&amp;spacedesc=11&amp;sit
   e=Online&amp;navsection=4421&amp;section=110837&amp;country=(none)&amp
   ;rand=1004167] 

   IFRAME:
   [63]http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=mi
   ddle&spacedesc=12&site=Online&navsection=4421&section=110837&country=(
   none)&rand=1004167
   [64][Params.richmedia=yes&amp;location=middle&amp;spacedesc=12&amp;sit
   e=Online&amp;navsection=4421&amp;section=110837&amp;country=(none)&amp
   ;rand=1004167] 

           Guardian Unlimited � Guardian Newspapers Limited 2003

Verweise

   1. file://localhost/0,6961,,00.html
   2. http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&site=Online&section=110837&country=(none)&rand=1004167&location=top
   3. http://ads.guardian.co.uk/click.ng/Params.richmedia=yes&site=Online&section=110837&country=(none)&rand=1004167&location=top
   4. file://localhost/Users/signin/0,12930,-1,00.html?url=http%3A%2F%2Fwww%2Eguardian%2Eco%2Euk/
   5. file://localhost/Users/register/1,12904,-1,00.html?url=http%3A%2F%2Fwww%2Eguardian%2Eco%2Euk
   6. file://localhost/
   7. file://localhost/online/
   8. file://localhost/online/
   9. file://localhost/online/whatsnew/
  10. file://localhost/online/webwatch/
  11. file://localhost/online/askjack/
  12. file://localhost/online/feedback/
  13. file://localhost/online/gameswatch/
  14. http://onlineblog.com/
  15. file://localhost/online/comment/
  16. file://localhost/online/insideit/
  17. file://localhost/online/businesssolutions/
  18. file://localhost/online/talktime/
  19. file://localhost/life/
  20. file://localhost/online/0,3147,782450,00.html
  21. file://localhost/online/story/0,3605,975117,00.html
  22. file://localhost/online/story/0,3605,975118,00.html
  23. file://localhost/online/story/0,3605,975119,00.html
  24. file://localhost/online/story/0,3605,975120,00.html
  25. file://localhost/online/story/0,3605,975122,00.html
  26. file://localhost/online/story/0,3605,975123,00.html
  27. file://localhost/online/story/0,3605,975124,00.html
  28. file://localhost/online/story/0,3605,975125,00.html
  29. file://localhost/online/story/0,3605,975126,00.html
  30. file://localhost/online/story/0,3605,975127,00.html
  31. file://localhost/online/story/0,3605,975128,00.html
  32. file://localhost/online/story/0,3605,975129,00.html
  33. file://localhost/online/story/0,3605,975130,00.html
  34. file://localhost/online/story/0,3605,975131,00.html
  35. file://localhost/online/story/0,3605,975132,00.html
  36. file://localhost/online/story/0,3605,975133,00.html
  37. file://localhost/online/story/0,3605,975134,00.html
  38. file://localhost/online/story/0,3605,975136,00.html
  39. file://localhost/online/story/0,3605,975139,00.html
  40. file://localhost/online/story/0,3605,975140,00.html
  41. file://localhost/online/story/0,3605,975750,00.html
  42. http://www.guardian.co.uk/
  43. file://localhost/Print/0,3858,4688611,00.html
  44. file://localhost/Widgets/SendIt_Form/1,3637,4688611,00.html
  45. file://localhost/Users/savedstories/save/tr/1,13196,4688611,00.html
  46. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/guardian-amcc020612/guardian-amcc020612.html#top
  47. http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=middle&spacedesc=50&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  48. http://ads.guardian.co.uk/click.ng/Params.richmedia=yes&location=middle&spacedesc=50&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  49. http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=middle&spacedesc=05&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  50. http://ads.guardian.co.uk/click.ng/Params.richmedia=yes&location=middle&spacedesc=05&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  51. http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=middle&spacedesc=06&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  52. http://ads.guardian.co.uk/click.ng/Params.richmedia=yes&location=middle&spacedesc=06&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  53. http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=middle&spacedesc=07&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  54. http://ads.guardian.co.uk/click.ng/Params.richmedia=yes&location=middle&spacedesc=07&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  55. http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=middle&spacedesc=08&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  56. http://ads.guardian.co.uk/click.ng/Params.richmedia=yes&location=middle&spacedesc=08&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  57. http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=middle&spacedesc=09&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  58. http://ads.guardian.co.uk/click.ng/Params.richmedia=yes&location=middle&spacedesc=09&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  59. http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=middle&spacedesc=10&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  60. http://ads.guardian.co.uk/click.ng/Params.richmedia=yes&location=middle&spacedesc=10&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  61. http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=middle&spacedesc=11&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  62. http://ads.guardian.co.uk/click.ng/Params.richmedia=yes&location=middle&spacedesc=11&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  63. http://ads.guardian.co.uk/html.ng/Params.richmedia=yes&location=middle&spacedesc=12&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
  64. http://ads.guardian.co.uk/click.ng/Params.richmedia=yes&location=middle&spacedesc=12&site=Online&navsection=4421&section=110837&country=(none)&rand=1004167
