


     _________________________________________________________________

                       Pressemitteilung 27. M�rz 2002

         CeBIT 2002 - Ein Trauerspiel f�r die deutsche IT-Industrie
              Deutschland verkommt zu einem reinen Absatzmarkt

    Nachdem die CeBIT 2002 Geschichte ist und nach der Konjunkturflaute
    langsam wieder eine positive Stimmung in die IT-Wirtschaft einzieht,
       bietet die deutsche IT-Wirtschaft ein fades Erscheinungsbild.

    Wer auf der diesj�hrigen CeBIT genau hinschaute, konnte feststellen,
   dass 600 Hardware-Hersteller aus Taiwan in Hannover ausstellten. Einer
   der letzten Hersteller aus Deutschland, die ELSA AG, war nur mit einem
    Notstand vertreten und hat vor wenigen Wochen Insolvenz angemeldet.
    Auf der anderen Seite stellten Hunderte von Softwareentwicklern mit
   eigenst�ndigen Produkten aus Amerika auf der CeBIT aus. Lediglich SAP
   konnte als deutscher Softwareproduzent nachhaltig wahrgenommen werden.

       Deutschland mit der weltweit gr��ten Dichte an Systemh�usern,
       Fachh�ndlern und sonstigen Dienstleistern ist zu einem reinen
   Absatzmarkt verkommen. Das Jammern und Klagen der letzten Monate wurde
     in Amerika und Taiwan dazu genutzt, neue Produkte zu kreieren. In
    Deutschland dagegen wurde abgewartet und gezaudert. Das war deutlich
       zu sp�ren. Wer sich an dem Stand eines deutschen Unternehmens
     informieren wollte, h�rte bereits nach 30 Sekunden, dass der arme
    Aussteller alle Probleme der Menschheit auf seinen Schultern tragen
   muss. Schuld sind nat�rlich die Anderen. Vom Bundeskanzler bis hin zu
          den Kunden, die partout nicht kaufen wollen, reichte die
    Argumentationskette. Ging man 5 Meter weiter an einen amerikanischen
        Stand, konnte man innerhalb einer halben Stunde ein Gesch�ft
         abwickeln, ohne Schuldgef�hle mit auf den Weg zu bekommen.

      Es stellt sich die Frage, wieso eine solche Situation eintreten
         konnte. Null Innovation war auf der CeBIT zu sp�ren, null
       Risikobereitschaft und null Gesp�r f�r die wahren Probleme der
       IT-Anwender. Das Grund�bel unserer IT-Wirtschaft ist, dass die
   Endkunden f�r dumm gehalten werden. Sie haben sowieso keine Ahnung und
   wollen alles umsonst haben. Und wenn der Anwender nicht der Schuldige
     ist, dann ist es Microsoft, die schlechte Software produzieren und
               gegen deren Monopol sowieso kein Ankommen ist.

     Man hat in Deutschland noch nicht begriffen, wie die IT-Wirtschaft
      funktioniert. Allen m�sste es langsam aufgefallen sein, dass nur
   Monopole in der IT-Industrie �berleben. Wer als Deutscher nur das Wort
    "Monopol" in den Mund nimmt, ist ein halber Verbrecher. Diese dumpfe
   Polemik gegen Softwarepatente und die Behauptung, Software l�sst sich
      sowieso nicht patentieren, haben jeden Keim an Erfindergeist in
                           Deutschland zerst�rt.

    Patente und Schutzrechte sind die Voraussetzung, um in Ruhe und ohne
   Druck neue Technologien und Anwendungen im Markt platzieren zu k�nnen.
       Es gibt kein Produkt auf dem Weltmarkt, das nachhaltig Gewinne
       produziert und nicht patentiert ist. Sind wir Deutschen davon
                                �berrascht?

     Nat�rlich gibt es kein Patent ohne Erfindung. Keine Erfindung ohne
   Erfinder. Keine Erfinder ohne Motivation f�r die Erfinder. Der Tod des
   deutschen Erfinders von Fax und Kopierer vor mehreren Wochen wurde mit
   einer Randnotiz begleitet. Wenn Verona Feldbusch irgendetwas von sich
      gibt, steht es am n�chsten Tag in jeder Zeitung. Warum? Sind die
    Zeitungen daran schuld? Nein. Wir Deutschen finden Verona Feldbusch
     eben spannender als die Wirtschaft. Bill Gates und Konsorten sind
    Helden in ihren L�ndern und haben ganze Industrien geschaffen. Diese
   bringen dauerhaft Arbeitspl�tze und Wohlstand. Wie viele Arbeitspl�tze
             hat Verona Feldbusch eigentlich schon geschaffen?

   Kein Schatten ohne Licht. Noch ist nichts verloren. Immer noch gibt es
    weltweit gen�gend Probleme unter den IT-Anwendern, die wir Deutschen
    l�sen k�nnen. Wenn wir dazu noch begreifen, dass ein Patent oder ein
     Monopol nichts Schlechtes ist, k�nnen wir die n�chste IT-Industrie
     schaffen. Diese wird davon gepr�gt sein, dass niemand auf der Welt
       mehr eine Software installieren muss, nur um sich vor Viren zu
      sch�tzen und dass niemand auf der Welt mehr einen ADSL-Anschluss
      ben�tigt, um schnell im Internet zu surfen. Die Kette lie�e sich
    beliebig fortsetzen. Wenn wir jedoch unsere Einstellung nicht �ndern
   und unsere Ziele nicht hoch genug stecken, werden wir immer weiter den
    Anschluss verlieren. Und wenn ein Siemens Vorstand das Ziel ausgibt:
   "Wir haben jetzt 8 Prozent Marktanteil im Handygesch�ft und peilen 10
     Prozent an", soll sich niemand wundern, dass der gleiche Vorstand
     diese Sparte fr�her oder sp�ter schlie�en oder verkaufen muss. Er
    h�tte statt der angepeilten 10 Prozent verk�nden sollen: "Wir wollen
     Weltmarktf�hrer werden", denn so werden Mitarbeiter und Investoren
                                 motiviert.



                                [spacer.gif]
                                [paper.gif]


                           [1]Pressemitteilungen
          ________________________________________________________
                 __________________________________________

                        Rene Holzer, Gesch�ftsf�hrer
    Patente und Schutzrechte sind die Voraussetzung, um in Ruhe und ohne
   Druck neue Technologien und Anwendungen im Markt platzieren zu k�nnen.
          ________________________________________________________
                 __________________________________________

                                 SaferSurf
                             Benutzeroberfl�che
                       Verpackung + CD-ROM + Handbuch
          ________________________________________________________
                 __________________________________________




                           Weitere Informationen
   [2]CeBIT 2002: Trend zu Komplettl�sungen best�tigt (Pressemitteilung)
                 [3]Nutzwerk gewinnt beim Innovationspreis
                          [4]Patente von Nutzwerk

   [px_grey.gif] [5]Kontakt
   [6]zur�ck

   Unsere Produktlinie:
   [7]SaferSurf - Anonym surfen - Testsieger ComputerBild 
   [8]www.SaferSurf.com 
      [9]Virenschutz
      [10]Schneller Surfen
      [11] Anonym Tauschen
      [12]Spamschutz

References

   1. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/releases.html
   2. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/releases/02_03_cebit.html
   3. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/message/innopreis.html
   4. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/company/patent_allgemein.html
   5. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/contact/contact.html
   6. javascript:history.back()
   7. http://www.nutzwerk.de/safersurf/anonym-surfen/index.html
   8. http://www.safersurf.com/index.html
   9. http://www.safersurf.com/index.html
  10. http://www.safersurf.com/speed/index.html
  11. http://www.safersurf.com/anonym-tauschen/index.html
  12. http://www.safersurf.com/spam-schutz/index.html
