
	
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

  <head>

        
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15" />
    <meta name="Keywords" content="Fotoreportage, Software-Patente, Europa-Parlament, e-patents, Br�ssel, Demo, Protestkultur, Demonstration, No E-Patents in Europe" />
    <meta name="Description" content="No E-Patents in Europe - Fotoreportage von Ralph Segert" />
    <meta name="copyright" content="Ralph Segert" />
    <meta name="author" content="Ralph Segert" />
    <meta name="publisher" content="Ralph Segert" />
    <meta http-equiv="pragma" content="private" />
    <meta name="ROBOTS" content="index, follow" />
    <meta name="page-type" content="Artikel" />
    <meta name="page-topic" content="Internet/WWW" />
    <meta http-equiv="pragma" content="private" />
    <title>
No E-Patents in Europe! - Fotoreportage von Ralph Segert 
        </title>
	
	
	 
	     <link rel="stylesheet" href="sifotosns4.css" type="text/css" />
    <style type="text/css" media="screen">
    /*<![CDATA[*/
<!--
@import url("sifotos.css");
-->
    /*]]>*/
            </style>

			

				
  </head>
	 

  <body>
      <div id="top"></div>
		
      
		    <div id="oben">
	 	 <p class="oben"> segert sites &nbsp;:&nbsp; <a href="http://krit.de" title="Interviews, Bloggerbuch, Kritletter">krit.de</a> &nbsp;|&nbsp; <a href="http://lomobar.de" title="Lomographien von Verena Segert">lomobar.de</a> &nbsp;|&nbsp; <a href="http://segert-images.net/photolust" title="Protestkultur, Portrait, Reisefotografie von Ralph Segert">photolust</a> &nbsp;|&nbsp; <a href="http://ralph-segert.de" title="Webdesign, Fotografie, Texte">ralph-segert.de</a> &nbsp;|&nbsp; <a href="http://segert.net" title="Webdesign und Fotografie von Segert Webpublishing">segert.net</a> &nbsp;|&nbsp; <a href="http://segert-images.net/silog" title="Ein Fotoblog von Ralph und Verena Segert">si.log</a> &nbsp;|&nbsp; <a href="http://screenario.de" title="Favourites und Typografie im Web von Verena Segert">screenario</a> &nbsp;|&nbsp; <a href="http://segert.net/sw/formular.php" title="Kontakt">@ + @</a></p>
	 	 </div><!-- /oben -->

		
    <div id="gesamt">

<div id="kopf">	 
      	 <h1><strong>Fotoreportage: No E-Patents in Europe!</strong></h1>


	 	<p class="txt"> 
		Am 27.09.2003 trafen sich ca. 300 Teilnehmer vor dem Europa-Parlament in Br�ssel, um gegen die <a href="http://krit.de/ralphs/entry.php?id=00672">EU-Softwarepatente</a> zu demonstrieren. Vortr�ge,  Performance und eine Pressekonferenz wiesen auf die weitreichenden Folgen von Patenten f�r die freie und kreative Software-Entwicklung in Europa hin. <br />
Die Fotoreportage zeigt <a href="02.php">28 Fotos</a> von der Veranstaltung und w�rzt diese mit informativen Links �ber die Problematik von EU-Softwarepatenten. Mein Ziel: Mithilfe zur Aufkl�rung. - 02.09.2003, <a href="http://ralph-segert.de">Ralph Segert</a>
</p>
	
<p class="foto"><a href="02.php" title="next"><img src="gfx/001.jpg" height="325" width="430" alt="next" /></a></p>

 	 <p> <a href="28.php">&laquo;&laquo;&laquo;</a> &nbsp; 01 von 28 &nbsp; <a href="02.php">&raquo;&raquo;&raquo;</a></p>


      <p class="footer">
&copy; 2003 <a href="http://segert.net">segert.net</a> &nbsp;&nbsp;:::&nbsp;&nbsp; <a href="http://segert.net/sw/formular.php">kontakt</a> &nbsp;&nbsp;:::&nbsp;&nbsp; <a href="http://segert-images.net/silog">si.log</a>
</p>

<p class="gfx"><a
        href="http://ralph-segert.de"
         title="ralph-segert.de"><img src="gfx/1b_rs.gif"
        height="16" width="80" alt="ralph-segert.de" /></a></p>


		  
	 </div>
		<!-- kopf -->
		
	 
	</div>
	<!-- gesamt -->
		

  </body>
</html>

