
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
	<head>
		<title>Microsoft Communications Protocol Program Licensing Home Page</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="/library/shared/common/css/ie4.css">
		<link rel="stylesheet" type="text/css" href="/library/shared/comments/css/ie5.css">
		<link href="Members.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="/library/flyoutmenu/default.css">
		<!-- Start: ToolBar V2.5-->
		<script language="JavaScript" src='/library/toolbar/toolbar.js'></script>
		<script language="JavaScript" src='/library/toolbar/global.js'></script>
		<!-- End: ToolBar V2.5-->
	</head>
	<body>
		<form name="Default" method="post" action="Default.aspx" id="Default">
<input type="hidden" name="__VIEWSTATE" value="dDwtNjYwNjk5ODM4O3Q8O2w8aTwxPjs+O2w8dDw7bDxpPDE+Oz47bDx0PDtsPGk8MT47aTwzPjs+O2w8dDxwPGw8XyFJdGVtQ291bnQ7PjtsPGk8MTc+Oz4+O2w8aTwwPjtpPDE+O2k8Mj47aTwzPjtpPDQ+O2k8NT47aTw2PjtpPDc+O2k8OD47aTw5PjtpPDEwPjtpPDExPjtpPDEyPjtpPDEzPjtpPDE0PjtpPDE1PjtpPDE2Pjs+O2w8dDw7bDxpPDA+Oz47bDx0PEA8OTEzOzkxMztBdXRoZW50aWNhdGlvbiBTZXJ2aWNlcyBTZXJ2ZXIgKGluY2x1ZGluZyByZXN0cmljdGVkIHByb3RvY29scykgKjs5MTM7OTEzOzkxMzs+Ozs+Oz4+O3Q8O2w8aTwwPjs+O2w8dDxAPDExMDA7MTEwMDtDZXJ0aWZpY2F0ZSBTZXJ2aWNlcyBTZXJ2ZXIgKjsxMTAwOzExMDA7MTEwMDs+Ozs+Oz4+O3Q8O2w8aTwwPjs+O2w8dDxAPDkwODs5MDg7Q29sbGFib3JhdGlvbiBTZXJ2ZXI7OTA4OzkwODs5MDg7Pjs7Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8QDw5MTE7OTExO0RpZ2l0YWwgUmlnaHRzIE1hbmFnZW1lbnQgU2VydmVyIChpbmNsdWRpbmcgcmVzdHJpY3RlZCBwcm90b2NvbHMpICo7OTExOzkxMTs5MTE7Pjs7Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8QDw5MDc7OTA3O0ZpbGUgU2VydmVyOzkwNzs5MDc7OTA3Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTIwOzkyMDtHZW5lcmFsIFNlcnZlciAoaW5jbHVkaW5nIHJlc3RyaWN0ZWQgcHJvdG9jb2xzKSAqOzkyMDs5MjA7OTIwOz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTE5OzkxOTtHZW5lcmFsIFNlcnZlciB3aXRob3V0IHJlc3RyaWN0ZWQgcHJvdG9jb2xzOzkxOTs5MTk7OTE5Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTE0OzkxNDtNZWRpYSBTdHJlYW1pbmcgU2VydmVyOzkxNDs5MTQ7OTE0Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTE1OzkxNTtNdWx0aXBsYXllciBHYW1lcyBTZXJ2ZXI7OTE1OzkxNTs5MTU7Pjs7Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8QDw5MTc7OTE3O1ByaW50L0ZheCBTZXJ2ZXI7OTE3OzkxNzs5MTc7Pjs7Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8QDw5MjE7OTIxO1Byb3h5L0ZpcmV3YWxsL05BVCBTZXJ2ZXIgKGluY2x1ZGluZyByZXN0cmljdGVkIHByb3RvY29scykgKjs5MjE7OTIxOzkyMTs+Ozs+Oz4+O3Q8O2w8aTwwPjs+O2w8dDxAPDkwOTs5MDk7UHJveHkvRmlyZXdhbGwvTkFUIFNlcnZlciB3aXRob3V0IHJlc3RyaWN0ZWQgcHJvdG9jb2xzOzkwOTs5MDk7OTA5Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8MTExNTsxMTE1O1JpZ2h0cyBNYW5hZ2VtZW50IFNlcnZlciAoaW5jbHVkaW5nIHJlc3RyaWN0ZWQgcHJvdG9jb2xzKSAqOzExMTU7MTExNTsxMTE1Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTE2OzkxNjtTeXN0ZW1zIE1hbmFnZW1lbnQgU2VydmVyOzkxNjs5MTY7OTE2Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTEwOzkxMDtUZXJtaW5hbCBTZXJ2ZXI7OTEwOzkxMDs5MTA7Pjs7Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8QDw5MTI7OTEyO1ZpcnR1YWwgUHJpdmF0ZSBOZXR3b3JrIFNlcnZlcjs5MTI7OTEyOzkxMjs+Ozs+Oz4+O3Q8O2w8aTwwPjs+O2w8dDxAPDkxODs5MTg7V2ViIFNlcnZlcjs5MTg7OTE4OzkxODs+Ozs+Oz4+Oz4+O3Q8cDxsPFRleHQ7PjtsPCo7Pj47Oz47Pj47Pj47Pj47Pg==" />

			<!--Draw Toolbar-->
			<script language="JavaScript" src='/public/consent/toolbar/local_toolbar.js'></script>
			<!--Draw Toolbar-->
			<table border="0" height="100%" cellpadding="0" cellspacing="0" width="100%">
				<!-- DEFINE TABLE LAYOUT -->
				<tr>
					<td width="1" rowspan="999" bgcolor="#f1f1f1" nowrap><img src="/library/images/gifs/homepage/1ptrans.gif" width="8" height="1" alt="" border="0"></td>
					<td width="176" bgcolor="#f1f1f1" rowspan="999" valign="top" nowrap>
						
<!--START LEFT NAVIGATION-->
<script language="javascript">
function ExpandCollapse(img)
{
	var id = img.UniqueID;
	var row = document.all("tr"+id);
	if (row.style.display == "")
	{
		document.all("img"+id).src = "UI_Plus.gif";
		row.style.display = "none";
	}
	else
	{
		document.all("img"+id).src = "UI_Minus.gif";
		row.style.display = "";
	}
	
}
</script>
<table>
	<tr>
		<td width="8" bgcolor="#f0f0eb" style="width:8px;height:1px;" nowrap></td>
		<td width="100%" bgcolor="#f0f0eb" style="height:1px;"></td>
	</tr>
	<tr>
		<td colspan="2"><a href="Default.aspx">MCPP Home</a></td>
	</tr>
	<tr>
		<td colspan="2"><a href="faq.aspx">FAQ</a></td>
	</tr>
	<tr>
		<td colspan="2"><a href="EntryRequirements.aspx">Program Entry Requirements</a></td>
	</tr>
	<tr>
		<td colspan="2"><a href="LicenseOverview.aspx">License Overview</a></td>
	</tr>
	<tr>
		<td colspan="2"><a href="PricingOverview.aspx">Pricing Overview</a></td>
	</tr>
	<tr>
		<td colspan="2"><a href="LitSamples.aspx">Sample Technical Documentation</a></td>
	</tr>
	
	<!--
	<tr>
		<td colspan="2"><a href="TaskSelection.aspx">Task Selection</a></td>
	</tr>
	-->
	
	<tr>
		<td colspan="2"><a href="OptionalDocuments.aspx">Optional Documents</a></td>
	</tr>
	
	<tr>
		<td colspan="2" class="navheader"><br><br>Server Tasks</td>
	</tr>
	<tr>
		<td colspan="2">
			<table>
				
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="913" 
								style="cursor:hand">
								<img id="img913"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Authentication Services Server (including restricted protocols) *
								</span>
							</td>
						</tr>
						<tr id="tr913" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=913'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=913'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="1100" 
								style="cursor:hand">
								<img id="img1100"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Certificate Services Server *
								</span>
							</td>
						</tr>
						<tr id="tr1100" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=1100'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=1100'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="908" 
								style="cursor:hand">
								<img id="img908"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Collaboration Server
								</span>
							</td>
						</tr>
						<tr id="tr908" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=908'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=908'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="911" 
								style="cursor:hand">
								<img id="img911"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Digital Rights Management Server (including restricted protocols) *
								</span>
							</td>
						</tr>
						<tr id="tr911" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=911'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=911'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="907" 
								style="cursor:hand">
								<img id="img907"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									File Server
								</span>
							</td>
						</tr>
						<tr id="tr907" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=907'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=907'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="920" 
								style="cursor:hand">
								<img id="img920"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									General Server (including restricted protocols) *
								</span>
							</td>
						</tr>
						<tr id="tr920" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=920'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=920'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="919" 
								style="cursor:hand">
								<img id="img919"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									General Server without restricted protocols
								</span>
							</td>
						</tr>
						<tr id="tr919" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=919'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=919'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="914" 
								style="cursor:hand">
								<img id="img914"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Media Streaming Server
								</span>
							</td>
						</tr>
						<tr id="tr914" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=914'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=914'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="915" 
								style="cursor:hand">
								<img id="img915"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Multiplayer Games Server
								</span>
							</td>
						</tr>
						<tr id="tr915" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=915'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=915'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="917" 
								style="cursor:hand">
								<img id="img917"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Print/Fax Server
								</span>
							</td>
						</tr>
						<tr id="tr917" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=917'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=917'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="921" 
								style="cursor:hand">
								<img id="img921"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Proxy/Firewall/NAT Server (including restricted protocols) *
								</span>
							</td>
						</tr>
						<tr id="tr921" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=921'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=921'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="909" 
								style="cursor:hand">
								<img id="img909"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Proxy/Firewall/NAT Server without restricted protocols
								</span>
							</td>
						</tr>
						<tr id="tr909" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=909'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=909'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="1115" 
								style="cursor:hand">
								<img id="img1115"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Rights Management Server (including restricted protocols) *
								</span>
							</td>
						</tr>
						<tr id="tr1115" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=1115'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=1115'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="916" 
								style="cursor:hand">
								<img id="img916"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Systems Management Server
								</span>
							</td>
						</tr>
						<tr id="tr916" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=916'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=916'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="910" 
								style="cursor:hand">
								<img id="img910"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Terminal Server
								</span>
							</td>
						</tr>
						<tr id="tr910" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=910'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=910'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="912" 
								style="cursor:hand">
								<img id="img912"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Virtual Private Network Server
								</span>
							</td>
						</tr>
						<tr id="tr912" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=912'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=912'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="918" 
								style="cursor:hand">
								<img id="img918"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Web Server
								</span>
							</td>
						</tr>
						<tr id="tr918" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=918'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=918'>Licensing Information</a>
							</td>
						</tr>
					
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding-top:20px;">
			*&nbsp;<span style="font-size:8pt">These 
				tasks involve restricted protocols related to authentication and digital rights 
				management, and require an additional qualification process. See <a href="EntryRequirements.aspx" style="font-size:8pt;">
					Program Entry Requirements</a> for more information</span></td>
	</tr>
</table>
<!--END LEFT NAVIGATION-->

					</td>
					<td width="1" rowspan="999" bgcolor="#999999" nowrap><img src="/library/images/gifs/homepage/1ptrans.gif" width="1" height="1" alt="" border="0"></td>
					<td width="5" rowspan="999"><img src="/library/images/gifs/homepage/1ptrans.gif" width="8" height="1" alt="" border="0"></td>
					<td><img src="/library/images/gifs/homepage/1ptrans.gif" width="600" height="1" alt="" border="0"></td>
					<td width="5" rowspan="999"><img src="/library/images/gifs/homepage/1ptrans.gif" width="8" height="1" alt="" border="0"></td>
				</tr>
				<tr>
					<td valign="top" align="left">
						<table height="100%" width="470">
							<tr>
								<td>
									<h1><div style="color:gray">Microsoft Communications Protocol Program</div>
										Protocol Licensing Home Page</h1>
									<br>
								</td>
							</tr>
							<tr>
								<td>
									
									<!-- content -->
									
									Welcome to the Microsoft Communications Protocol Program. This site includes program information and specifics of the protocols available for license under the Program. 
									<br><br>
									This program makes available, on reasonable and non-discriminatory terms, the communications protocols implemented in a Microsoft Windows 2000 Professional, Windows XP or successor desktop operating system that is used to interoperate or communicate natively with a Microsoft server operating system product. More than 100 proprietary protocols that were not previously available are now available for license by third parties.
                                    <br><br>
                                    In accordance with the Program’s <a href="LicenseOverview.aspx">license terms</a>, these protocols can be used in development of a broad range of server software products that may use the protocols to interoperate or communicate natively with the Windows XP and successor or predecessor desktop client operating systems.  Microsoft is also generally willing to grant even broader usage rights for the company’s protocol technology than are reflected in standard MCPP license agreements. For example, Microsoft has already agreed to allow the use of certain protocols for specific server to server communications. Microsoft accordingly encourages developers interested in licensing the company’s protocol technology for purposes other than as provided in the standard MCPP license agreement (for example initializing the Rights Management Services environment) to discuss their technical requirements with Microsoft’s protocol licensing team so that a possible solution can be worked out.
								    <br><br>
									The MCPP launched in August 2002 and several companies have already signed protocol license agreements. Recently, Microsoft has made a number of changes to the program, including the adoption of low per-unit royalties for certain server tasks, reduction of initial fees, creation of a streamlined and simplified development and distribution license and publication of a subset of the MCPP protocols on MSDN (see <a href="http://www.microsoft.com/presspass/press/2004/jan04/01-23mcppPR.asp">press release</a>).
									<br><br>
									Many of the protocols used by Windows 2000 and Windows XP client operating systems to interoperate or communicate with Microsoft server operating systems are available through standards organizations or are published and available from third party sources, and are therefore not included in this Program. For more information on these standards and other published protocols, see the <a target="_blank" href="http://www.microsoft.com/legal/protocols/protocols.asp">Published Protocols List</a>. 
									<br><br>
									Communications protocols provide the rules for information exchange to accomplish specific predefined tasks across a network connection. Protocols available under this Program have been grouped according to the server tasks for which they can be used. In some cases, specific protocols may be applicable to more than one task, as reflected in the protocol lists on this site.  A licensee’s choice of server task(s) for their products that implement the communications protocols determines to a large degree the royalty rates that apply.  If a product’s use is not limited to one of the specified tasks, the product will be considered a “General Server,” for which pricing terms are also specified. 
									<br><br>	
									The defined server tasks are shown in the navigation menu along the left edge of this page. To see a description of each task and the protocols associated with the task, click on the task names in the navigation menu, then click on “Protocol Details.” 
									<br><br>
									The documentation for each protocol that is licensed under the Program includes the set of technical rules for information exchange to accomplish predefined tasks between the Windows 2000 Professional, Windows XP and successor client desktop operating systems and current Windows server operating system products. These rules govern the format, semantics, timing, sequencing and error control of messages exchanged over a network. The documentation is equivalent in approach and scope to protocol documentation published by the Internet Engineering Task Force (IETF). However, since the documentation is available only to licensees under the Program, it is protected by digital rights management technology and can only be read using the Microsoft Reader application. 
									<br><br>
									Sample technical documentation, representative of the quality, depth and style of the technical documentation provided for protocols licensed under the Program, is available for review purposes only at the <a href="LitSamples.aspx">Sample Technical Documentation</a> link on the menu. You can review the <a href="evalagr.aspx">Evaluation Agreement</a> and contact the <a href="mailto:protocol@microsoft.com?subject=MCPP licensed task information">MCPP licensing team</a> for more information about sample documentation. 
									<br><br>

									All licenses available under the Program require completion of the <a href="entryrequirements.aspx">Program Entry Requirements</a> and payment of royalties. Because of the range of different possible uses of the protocols that the Program needs to accommodate in a reasonable and non-discriminatory manner, the specific royalty amount depends on several different factors. See the <a href="pricingoverview.aspx">Pricing Overview</a> for additional details. 
									<br><br>
									To read answers to other frequently asked questions about the Program, see the <a href="faq.aspx">FAQ</a> page. 
									
									<!-- end of content -->
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="100%">&nbsp;</td>
				</tr>
				<tr>
					<td valign="bottom">
						<table>
 <tr>
  <td width="600" class="footer">
	
  </td>
 </tr>
</table>

					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<!-- BEGIN FOOTER -->
			<table id='idFooter' style='WIDTH:100%;BACKGROUND-COLOR:white' cellspacing='0' cellpadding='0' border='0'>
				<tr valign='middle'>
					<td id='idFooterRow1' style='WIDTH:100%;HEIGHT:20px;BACKGROUND-COLOR:#003399' nowrap>
						&nbsp;<a target='_top' style='FONT:bold xx-small Verdana; CURSOR:hand; COLOR:#ffffff; TEXT-DECORATION:none' href='/isapi/goregwiz.asp?target=/regwiz/forms/contactus.asp' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Contact 
							Us</a> &nbsp;<span style='FONT:bold xx-small Verdana; COLOR:#ffffff'>&nbsp;|</span>
						&nbsp;<a target='_top' style='FONT:bold xx-small Verdana; CURSOR:hand; COLOR:#ffffff; TEXT-DECORATION:none' href='/isapi/gomscom.asp?target=/legal/' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Legal</a>
					</td>
				</tr>
				<tr valign='middle'>
					<td id='idFooterRow2' style='WIDTH:100%;HEIGHT:30px;BACKGROUND-COLOR:#003399' nowrap>
						<span style='FONT:xx-small Verdana; COLOR:#ffffff'>&nbsp;© 2002 Microsoft 
							Corporation. All rights reserved.&nbsp;</span> &nbsp;<a target='_top' style='FONT:xx-small Verdana; CURSOR:hand; COLOR:#ffffff; TEXT-DECORATION:none' href=' /isapi/gomscom.asp?target=/info/cpyright.htm' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Terms 
							of Use</a>&nbsp;&nbsp;<a target='_top' style='FONT:xx-small Verdana; CURSOR:hand; COLOR:#ffffff; TEXT-DECORATION:none' href='/isapi/gomscom.asp?target=/info/privacy.htm' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Privacy 
							Statement </a>&nbsp;&nbsp;<a target='_top' style='FONT:xx-small Verdana; CURSOR:hand; COLOR:#ffffff; TEXT-DECORATION:none' href='/isapi/gomscom.asp?target=/enable/' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Accessibility
						</a>
					</td>
				</tr>
			</table>
			<!--END FOOTER -->
		</form>
	</body>
</html>
