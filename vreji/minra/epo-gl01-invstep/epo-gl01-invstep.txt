
   ______________________________________________________________________
   ______________________________________________________________________

   [epo-logo_small.gif] [trans.gif] Guidelines for examination in the EPO
   [1]Homepage => Toolbox for applicants => [2]Guidelines for examination
   => [3]HTML-Version
   ______________________________________________________________________
   ______________________________________________________________________

                                                  [4]D / [5]F [trans.gif]

    [6]Guidelines for examination
   [trans.gif] [trans.gif] [trans.gif]
   [7]Part C, Chapter IV [8]8. Non-prejudicial disclosures 
   [9]Part C, Chapter IV [10]Previous Page: 8. Non-prejudicial
   disclosures 
   [trans.gif]
   9. Inventive step
   [11]Art. 56 


   9.1 "An invention shall be considered as involving an inventive step
   if, having regard to the state of the art, it is not obvious to a
   person skilled in the art." Novelty and inventive step are different
   criteria. Novelty exists if there is any difference between the
   invention and the known art. The question - Is there inventive step? -
   only arises if there is novelty.

   9.2 The "state of the art" for the purposes of considering inventive
   step is as defined in Art. 54(2) (see IV, 5); it does not include
   later published European applications referred to in Art. 54(3). As
   mentioned in IV, 5.3, "date of filing" in Art. 54(2), means date of
   priority where appropriate (see Chapter V).

   9.3 Thus the question to consider, in relation to any claim defining
   the invention, is whether at the priority date of that claim, having
   regard to the art known at the time, it would have been obvious to the
   person skilled in the art to arrive at something falling within the
   terms of the claim. If so, the claim is bad for lack of inventive
   step. The term "obvious" means that which does not go beyond the
   normal progress of technology but merely follows plainly or logically
   from the prior art, i.e. something which does not involve the exercise
   of any skill or ability beyond that to be expected of the person
   skilled in the art. In considering inventive step, as distinct from
   novelty (see IV, 7.3), it is fair to construe any published document
   in the light of subsequent knowledge and to have regard to all the
   knowledge generally available to the person skilled in the art at the
   priority date of the claim.

   9.3a The invention claimed must normally be considered as a whole.
   Thus it is not correct as a general rule, in the case of a combination
   claim, to argue that the separate features of the combination taken by
   themselves are known or obvious and that "therefore" the whole
   subject-matter claimed is obvious. The only exception to this rule is
   where there is no functional relationship between the features of the
   combination i.e. where the claim is merely for a juxtaposition of
   features and not a true combination (see the example at 2.1 of the
   Annex to this Chapter).

   9.4 While the claim should in each case be directed to technical
   features (and not, for example, merely to an idea), in order to assess
   whether an inventive step is present it is important for the examiner
   to bear in mind that there are various ways in which the skilled
   person may arrive at an invention. An invention may, for example, be
   based on the following:

   (i) The formulation of an idea or of a problem to be solved (the
   solution being obvious once the problem is clearly stated).

   Example: the problem of indicating to the driver of a motor vehicle at
   night the line of the road ahead by using the light from the vehicle
   itself. As soon as the problem is stated in this form the technical
   solution, viz. the provision of reflective markings along the road
   surface, appears simple and obvious. For another example see T 2/83,
   OJ 6/1984, 265.

   (ii) The devising of a solution to a known problem.

   Example: the problem of permanently marking farm animals such as cows
   without causing pain to the animals or damage to the hide has existed
   since farming began. The solution ("freeze-branding") consists in
   applying the discovery that the hide can be permanently depigmented by
   freezing.

   (iii) The arrival at an insight into the cause of an observed
   phenomenon (the practical use of this phenomenon then being obvious).

   Example: the agreeable flavour of butter is found to be caused by
   minute quantities of a particular compound. As soon as this insight
   has been arrived at, the technical application comprising adding this
   compound to margarine is immediately obvious.

   Many inventions are of course based on a combination of the above
   possibilities - e.g. the arrival at an insight and the technical
   application of that insight may both involve the use of the inventive
   faculty.

   9.5 In identifying the contribution any particular invention makes to
   the art in order to determine whether there is an inventive step,
   account should be taken first of what the applicant himself
   acknowledges in his description and claims to be known. Any such
   acknowledgement of known art should be regarded by the examiner as
   being correct unless the applicant states he has made a mistake (see
   VI, 8.5). However, the further prior art contained in the search
   report may put the invention in an entirely different perspective from
   that apparent from reading the applicant'sspecification by itself (and
   indeed this cited prior art may cause the applicant voluntarily to
   amend his claims to redefine his invention before his application
   comes up for examination). In order to reach a final conclusion as to
   whether the subject-matter of any claim includes an inventive step it
   is necessary to determine the difference between the subject-matter of
   that claim and the prior art and, in considering this matter, the
   examiner should not proceed solely from the point of view suggested by
   the form of claim (prior art plus characterising portion - see III,
   2).

   When assessing inventive step the examiner normally applies the
   problem and solution approach.

   In the problem and solution approach there are three main stages:
    1. determining the closest prior art,
    2. establishing the technical problem to be solved, and
    3. considering whether or not the claimed invention, starting from
       the closest prior art and the technical problem, would have been
       obvious to the skilled person.

   The closest prior art is that combination of features derivable from
   one single reference that provides the best basis for considering the
   question of obviousness. The closest prior art may be, for example:

   (i) a known combination in the technical field concerned that
   discloses technical effects, purpose or intended use, most similar to
   the claimed invention or

   (ii) that combination which has the greatest number of technical
   features in common with the invention and capable of performing the
   function of the invention.

   In the second stage one establishes in an objective way the technical
   problem to be solved. To do this, one studies the application (or the
   patent), the closest prior art, and the difference in terms of
   features (either structural or functional) between the invention and
   the closest prior art, and then formulates the technical problem.

   In this context the technical problem means the aim and task of
   modifying or adapting the closest prior art to provide the technical
   effects that the invention provides over the closest prior art.

   The technical problem derived in this way may not be what the
   application presents as "the problem". The latter may require to be
   reformulated, since the objective technical problem is based on
   objectively established facts, in particular appearing in the prior
   art revealed in the course of the proceedings, which may be different
   from the priorart of which the applicant was actually aware at the
   time the application was filed.

   The extent to which such reformulation of the technical problem is
   possible has to be assessed on the merits of each particular case. As
   a matter of principle any effect provided by the invention may be used
   as a basis for the reformulation of the technical problem, as long as
   said effect is derivable from the application as filed (see T 386/89,
   not published in OJ). It is also possible to rely on new effects
   submitted subsequently during the proceedings by the applicant,
   provided that the skilled person would recognise these effects as
   implied by or related to the technical problem initially suggested
   (see 9.10 below and T 184/82, OJ 6/1984, 261).

   The expression technical problem should be interpreted broadly; it
   does not necessarily imply that the solution is a technical
   improvement over the prior art. Thus the problem could be simply to
   seek an alternative to a known device or process providing the same or
   similar effects or which is more cost-effective.

   Sometimes the features of a claim provide more than one technical
   effect, so one can speak of the technical problem as having more than
   one part or aspect, each corresponding to one of the technical
   effects. In such cases, each part or aspect generally has to be
   considered in turn.

   In the third stage the question to be answered is whether there is any
   teaching in the prior art as a whole that would (not simply could, but
   would) prompt the skilled person, faced with the technical problem, to
   modify or adapt the closest prior art while taking account of that
   teaching, thus arriving at something falling within the terms of the
   claims, and thus achieving what the invention achieves (see IV, 9.3).

   9.5a If an independent claim is new and non-obvious, there is no need
   to investigate the obviousness or non-obviousness of any claims
   dependent thereon, except in situations where the priority claim for
   the subject-matter of the dependent claim has to be checked because of
   intermediate documents (see V, 2.6.3). Similarly, if a claim to a
   product is new and non-obvious there is no need to investigate the
   obviousness of any claims for a process which inevitably results in
   the manufacture of that product or any claims for a use of that
   product. In particular, analogy processes are patentable insofar as
   they provide a novel and inventive product (see T 119/82, OJ 5/1984,
   217).

   9.6 The person skilled in the art should be presumed to be an ordinary
   practitioner aware of what was common general knowledge in the art at
   the relevant date. Heshould also be presumed to have had access to
   everything in the "state of the art", in particular the documents
   cited in the search report, and to have had at his disposal the normal
   means and capacity for routine work and experimentation. If the
   problem prompts the person skilled in the art to seek its solution in
   another technical field, the specialist in that field is the person
   qualified to solve the problem. The assessment of whether the solution
   involves an inventive step must therefore be based on that
   specialist's knowledge and ability (see T 32/81, OJ 6/1982, 225).
   There may be instances where it is more appropriate to think in terms
   of a group of persons, e.g. a research or production team, than a
   single person. This may apply e.g. in certain advanced technologies
   such as computers or telephone systems and in highly specialised
   processes such as the commercial production of integrated circuits or
   of complex chemical substances.

   9.7 In considering whether there is inventive step (as distinct from
   novelty (see IV, 7)), it is permissible to combine together the
   disclosures of two or more documents or parts of documents, different
   parts of the same document or other pieces of prior art, but only
   where such combination would have been obvious to the person skilled
   in the art at the effective priority date of the claim under
   examination (see T 2/83, OJ 6/1984, 265). In determining whether it
   would be obvious to combine two or more distinct disclosures, the
   examiner should have regard to the following:

   (i) Whether the content of the documents is such as to make it likely
   or unlikely that the person skilled in the art, when concerned with
   the problem solved by the invention, would combine them - for example,
   if two disclosures considered as a whole could not in practice be
   readily combined because of inherent incompatibility in disclosed
   features essential to the invention, the combining of these
   disclosures should not normally be regarded as obvious.

   (ii) Whether the documents come from similar, neighbouring or remote
   technical fields.

   (iii) The combining of two or more parts of the same document would be
   obvious if there is a reasonable basis for the skilled person to
   associate these parts with one another. It would normally be obvious
   to combine with a prior art document a well-known textbook or standard
   dictionary; this is only a special case of the general proposition
   that it is obvious to combine the teaching of one or more documents
   with the common general knowledge in the art. It would, generally
   speaking, also be obvious to combine two documents one of which
   contains a clear and unmistakable reference to the other (for
   references which are considered an integral part of the disclosure,
   see 6.1 and 7.1). In determining whether it ispermissible to combine a
   document with an item of prior art made public in some other way, e.g.
   by use, similar considerations apply.

   9.8 The annex to this chapter "Guidance for the assessment of
   inventive step" gives examples for guid ance of circumstances where an
   invention should be regarded as obvious or where it involves an
   inventive step. It is to be stressed that these examples are only
   guides and that the applicable principle in each case is "was it
   obvious to a person skilled in the art?" Examiners should avoid
   attempts to fit a particular case into one of these examples where the
   latter is not clearly applicable. Also, the list is not exhaustive .

   9.9 It should be remembered that an invention which at first sight
   appears obvious might in fact involve an inventive step. Once a new
   idea has been formulated it can often be shown theoretically how it
   might be arrived at, starting from something known, by a series of
   apparently easy steps. The examiner should be wary of ex post facto
   analysis of this kind. He should always bear in mind that the
   documents produced in the search have, of necessity, been obtained
   with foreknowledge of what matter constitutes the alleged invention.
   In all cases he should attempt to visualise the overall state of the
   art confronting the skilled man before the applicant's contribution
   and he should seek to make a "real life" assessment of this and other
   relevant factors. He should take into account all that is known
   concerning the background of the invention and give fair weight to
   relevant arguments or evidence submitted by the applicant. If, for
   example, an invention is shown to be of considerable technical value,
   and particularly if it provides a technical advantage which is new and
   surprising, and this can convincingly be related to one or more of the
   features included in the claim defining the invention, the examiner
   should be hesitant in pursuing an objection that such a claim lacks
   inventive step. The same applies where the invention solves a
   technical problem which workers in the art have been attempting to
   solve for a long time, or otherwise fulfils a long-felt need.
   Commercial success alone is not to be regarded as indicative of
   inventive step, but evidence of immediate commercial success when
   coupled with evidence of a long-felt want is of relevance provided the
   examiner is satisfied that the success derives from the technical
   features of the invention and not from other influences (e.g. selling
   techniques or advertising).

   9.10 The relevant arguments and evidence to be considered by the
   examiner for assessing inventive step may be taken either from the
   originally filed patent application, or be submitted by the applicant
   during the subsequent proceedings (see 9.5 above and VI, 5.7, 5.7a,
   5.7c and 5.7d).

   Care must be taken, however, whenever new effects in support of
   inventive step are referred to. Such new effects can only be taken
   into account if they are implied by or at least related to the
   technical problem initially suggested in the originally filed
   application (see also 9.5 above, T 386/89, not published in OJ and T
   184/82, OJ 6/1984, 261).

   Example of such a new effect:

   The invention as filed relates to a pharmaceutical composition having
   a specific activity. At first sight, having regard to the relevant
   prior art, it would appear that there is a lack of inventive step.
   Subsequently the applicant submits new evidence which shows that the
   claimed composition exhibits an unexpected advantage in terms of low
   toxicity. In this case it is allowable to reformulate the technical
   problem by including the aspect of toxicity, since pharmaceutical
   activity and toxicity are related in the sense that the skilled person
   would always contemplate the two aspects together.

   The reformulation of the technical problem may or may not give rise to
   an amendment, and subsequent insertion, of the statement of the
   technical problem in the description. Any such amendment is only
   allowable if it satisfies the conditions listed in VI, 5.7c. In the
   above example of a pharmaceutical composition, neither the
   reformulated problem nor the information on toxicity could be
   introduced into the description without infringing Art. 123(2).




   [12][up_button.gif] [13]Guidelines for examination
     _________________________________________________________________

   [14]EPO Home Page | [15]Recent updates| [16]Request info | [17]Send
   comments | [18]Index
   [19]Patent information on the Internet
   Last updated on Fri, 5 Oct 2001 13:28:05 GMT +01:00
   Copyright � 1996-2001 European Patent Office . All Rights Reserved.
   e-mail: [20]EPO Mail Distribution

Verweise

   1. http://www.european-patent-office.org/index.htm
   2. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/index.htm
   3. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/Templates/index.htm
   4. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/d/c_iv_9.htm
   5. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/f/c_iv_9.htm
   6. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/epo-gl01-invstep/index.htm
   7. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/epo-gl01-invstep/c_iv.htm
   8. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/epo-gl01-invstep/c_iv_8.htm
   9. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/epo-gl01-invstep/c_iv.htm
  10. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/epo-gl01-invstep/c_iv_8.htm
  11. javascript:show('../../epc/e/ar56.html');
  12. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/epo-gl01-invstep/epo-gl01-invstep.htm#top
  13. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/epo-gl01-invstep/index.htm
  14. http://www.european-patent-office.org/index.htm
  15. http://www.european-patent-office.org/updates.htm
  16. http://www.european-patent-office.org/forms/index.htm
  17. http://www.european-patent-office.org/forms/comments.htm
  18. http://www.european-patent-office.org/search.htm
  19. http://www.european-patent-office.org/online/index.htm
  20. http://www.european-patent-office.org/mail/index.htm
