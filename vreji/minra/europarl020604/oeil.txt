
   [1]| Home | [2]| Menu | | Dansk | | Deutsch | | Ellinika | | English |
   | Espa�ol | [3]| Fran�ais | | Italiano | | Nederlands | | Portugu�s |
   | Suomi | | Svenska |
   
   The Legislative Observatory
   
    Procedure file - [4]Consult any historical information 
   
   You may find the live data here 
   
   Identification of procedure
   
   Reference CNS/2000/0177
   Title Community patent
   Legal basis CE308
   Number of TEC dossier T06946
   Subject(s) 3.50.04- industrial property, European patent, Community
   patent, design and pattern
   Stage reached Awaiting final decision/signature
   
   Stages in the procedure
   
   Events Documents: references Dates of publication
     Source reference Equivalent references of document in Official
   Journal
   Initial proposal CE COM(2000)0412
     C5-0461/2000 01/08/2000 C337 28-NOV-00 278(E)
   Opinion of ESC CES CES0411/2001
     29/03/2001 C155 29-MAY-01 080
   Report tabled PE [5]A5-0059/2002
   PE294.978 19/02/2002
   EP opinion 1st reading PE T5-0163/2002
     10/04/2002
   
   Forecasts
   
   Agents in procedure - European Parliament
   
   Committee Rapporteur Political group
   Legal affairs, internal market (responsible) PALACIO VALLELERSUNDI Ana
    
   Budgets (opinion)
   Industry, external trade, research... ** (opinion) THORS Astrid  ELDR
   
   Agents in procedure - European Commission and Council of the Union
   
   European Commission DG DG Internal market
   Council of the Union Internal market
   ______________________________________________________________________
   
   21/05/2002 - COUNCIL ACTIVITIES
 Le Conseil a eu une discussion approfondie, sur la base d'une proposition de
compromis de la Pr�sidence espagnole visant � �tablir une orientation
politique commune sur les principaux aspects de la proposition. Partant du
principe o� il n'y a accord sur rien tant qu'il n'y a pas accord sur tout, le
Conseil consid�re que l'approche politique commune constitue la base de la
poursuite des travaux.
Compte tenu de l'importance consid�rable que les �tats membres accordent � la
question du syst�me juridictionnel, le Conseil poursuivra les discussions
notamment sur cette question, sur la base d'une nouvelle contribution de la
Commission. Le Conseil examinera l'�quilibre du paquet d'ensemble qui en
d�coulera afin de veiller � ce que l'accord final et global satisfasse aux
crit�res �tablis par le Conseil europ�en et corresponde aux besoins des
entreprises.

   10/04/2002 - EP VOTE 1st READING
 The European Parliament voted 256 to 187 with 95 abstentions in favour of a
resolution approving the Commission's proposal for a regulation to create a
Community patent, together with a number of non-binding amendments. Ahead of
the Council meeting in May, which is expected to reach the unanimous agreement
required to adopt the draft regulation, the amendments express Ana Isabel
PALACIO DEL VALLE LERSUNDI's (EPP-ED, E) views on a series of issues which are
essential for the adoption and proper functioning of this regulation. The key
points are the language arrangements, the role of national patent offices
vis-�-vis the European Patent Office (an existing non-EU body to which the
Community would accede in its own right) and the judicial system. (Please
refer to the summary dated 19/02/02).

   19/02/2002 - DECISION OF COMMITTEE RESPONSIBLE
 The committee adopted by a large majority the report by Ana PALACIO
VALLELERSUNDI (EPP-ED, E) tabling a number of amendments to the proposal
(under the consultation procedure) on the key issues, namely, the language
arrangements, the judicial system and the role of national patent offices
vis-�-vis the European Patent Office.
On the language question, the committee recommended that the language regime
should follow the example of the Community trademark regulation, under which
applications may be submitted in any of the Community's official languages.
However, the applicant would also have to indicate a second language (the
�procedural language�) from a choice of five (English, French, German, Italian
and Spanish), the use of which he would accept for the proceedings (notably
proceedings for opposition, revocation or invalidity). If the application were
filed in a language other than the above five, the Office would arrange for it
to be translated into the procedural language indicated by the applicant.
Concerning the judicial system, whereas the Commission was proposing that
jurisdiction at both first and the second instance should lie with a new
Community intellectual property court, the committee would prefer first
instance jurisdiction to be established at national level, using national
courts with experience in patent litigation as Community Patent Courts, with
rulings at second instance being given by the European Chamber for
Intellectual Property.
Lastly, the committee called on the Commission and the Council to ensure that
national patent offices would continue to play an important role in the
processing of the Community patent. In particular, the European Patent Office
should be able to instruct the national offices to produce research reports on
a limited number of patent applications provided they met the quality criteria
previously agreed. Moreover, MEPs said that a system of quality control should
be put in place under the authority of the European Commission in
collaboration with the European Patent Office.

   20/12/2001 - COUNCIL ACTIVITIES
 In accordance with mandate given by the Laeken European Council, the Council
continued its examination of the main problems still to be resolved concerning
the introduction of the Community patent, focusing in particular on the
language arrangements for the future system, the role to be played by the
national patent offices vis-�-vis the European Patent Office (EPO) and the
judicial system.
Even though the Council did not succeed in reaching unanimous agreement on the
key questions, the great majority of delegations agreed to continue
discussions on the basis of a compromise proposal presented by the Belgian
Presidency. The Spanish delegation indicated that it intended to continue
working actively on this basis during its Presidency.

   27/09/2001 - COUNCIL ACTIVITIES
 The Council noted a progress report by the Presidency and held a wide-ranging
exchange of views on the outstanding problems concerning the creation of a
Community patent. Having noted the principal difficulties of the various
delegations with the common approach which it was proposing, the Presidency
said that it would do everything possible to draw up a compromise text that
all delegations could accept, in order to reach agreement by the end of the
year. The Permanent Representatives Committee was invited to press ahead with
work on all aspects of the creation of the Community patent. The President of
the Council pointed out that all delegations had insisted on the need for the
Community patent to be credible.
As regards developments to date, the Lisbon European Council's conclusions,
subsequently confirmed by the Feira and Stockholm European Councils, requested
that a Community patent including the utility model be available by the end of
2001 at the latest.

   30/05/2001 - COUNCIL ACTIVITIES
 The Council agreed on the following common guidelines for the continued work
on the Community patent:
- a central role is to be played by the European Patent Office in the grant
and administration of Community patents. This would require amendment of the
European Patent Convention.
- National patent offices should have an important role, bearing in mind
contacts with local languages. This would include advising potential
applicants for Community patents and transmitting them to the European Patent
Office and disseminating information. The nature of other activities should be
further considered, taking account of the need to guarantee the quality and
uniformity of the Community patent. Applicants should be free to have their
applications fully processed by the European Patent Office.
- costs need to be kept competitive.
- as regards languages, the Community patent system should be based on general
principles including that of non-discrimination. Possibilities for limiting
translation costs should be explored.
- a certain percentage of income from annual renewal fees for the Community
patent should be distributed among Member States/national patent office.
- jurisdictional arrangements for the Community patent should be set up in
accordance with the framework of Articles 225a and 229a of the EC Treaty as
adopted at Nice.

   12/03/2001 - COUNCIL ACTIVITIES
 The Council held a debate on the proposal for a Regulation on the Community
Patent, including the relationship between this proposal and the European
Patent Convention (EPC), and on the appropriateness of taking steps in order
to keep the door open for possible Community accession to the EPC. Several
delegations stressed the need to examine thoroughly all aspects of the
proposal, giving due consideration to all options, before taking any decision
in this respect.
The Council will revert to this issue, considered to be a high priority by the
Swedish Presidency, at its next meeting on 5 June 2001.

   01/08/2000 - INITIAL PROPOSAL
 PURPOSE: to propose a Council Regulation on the Community Patent
CONTENT: The idea of the Community patent dates back to the 1960s. Most
recently, the Commission adopted, on 5 February 1999, a Communication on the
follow-up to the Green Paper on the Community patent and the patent system in
Europe (COM (1999)42 final). The aim of this Communication was to announce the
various measures and new initiatives which the Commission was planning to take
or propose in order to make the patent system attractive for promoting
innovation in Europe. The initiative concerning the Community patent was
announced and sketched out in broad outline in the Commission Communication
dated 5 February 1999. This proposal incorporates most of that broad outline.
This proposal for a Regulation is aimed at creating a new unitary industrial
property right, the Community patent. It is essential for eliminating the
distortion of competition which may result from the territorial nature of
national protection rights. It is also one of the most suitable means of
ensuring the free movement of goods protected by patents. Furthermore, the
creation of the Community patent will enable undertakings to adapt their
production  and distribution activities to the European dimension. It is
considered to be an essential tool in succeeding in transforming research
results and the new technological and scientific know-how into industrial and
commercial success stories - and thereby put an end to the 'European paradox'
in innovation - while at the same time stimulating private R&D investment,
which is currently at a very low level in the European Union compared with the
United States and Japan.
The main thrust of the proposal is the creation of a 'symbiosis' between two
systems: that of the Regulation on the Community patent, a European Community
instrument, and that of the Munich Convention, a classic international
instrument. The Community patent system will coexist with the national and
European patent systems. Inventors will remain free to choose the type of
patent production best suited to their needs. As already announced in the
Communication of 5 February 1999, the legal basis of the proposal for a
Regulation is Article 308 of the EC Treaty. Use of this legal base is in
accordance with what has been done in relation to the Community trade mark and
Community designs. The form chosen for the instrment- the Regulation- is
warranted by a number of considerations. The Member States cannot be left with
any discretion either to determine the Community law applicable to the
Community patent or to decide on the effects and administration of the patent
once it has been granted. The unity of the patent could not be guaranteed by
less 'binding' measures.
The main features of the proposed Community patent are the following:
- Unitary and autonomous nature of the Community patent;
- Law applicable to the Community patent. Here, the Regulation does not set
out to depart substantially from the principles embodied in national patent
law already in force in the Member States;
- Affordable cost of the Community patent. This proposal is aimed at making
the Community patent more affordable and moreattractive than the present
European patent;
- Legal certainty of the Community patent in terms of the judicial system.
This relates to litigation between private parties and only a centralised
Community court can guarantee without fail unity of law and consistent case
law;
- Division of responsibilities within the centralised Community court. A new
court would have jurisdiction in certain situations where jurisdiction would
normally have been vested in the Court of First Instance.

   Historical data
   
   Procedure reference 
   Legal basis 
   Rapporteurs 
   
   [6]TOP 

References

   1. http://www.europarl.eu.int/sg/tree/en/default.htm
   2. http://wwwdb.europarl.eu.int/dors/oeil/en/search.shtm
   3. http://wwwdb.europarl.eu.int/oeil/oeil_ViewDNL.ProcedureView?lang=1&procid=4446
   4. http://wwwdb.europarl.eu.int/oeil/oeil_ViewDNL.ProcedureView?lang=2&procid=4446#historique
   5. javascript:Start('OEIL_HTTP.enviewrealdoc?DocRef=A5-0059/2002&Lang=2&DocAcr=A&NoLangInstAcr=PE')
   6. http://wwwdb.europarl.eu.int/oeil/oeil_ViewDNL.ProcedureView?lang=2&procid=4446#top
