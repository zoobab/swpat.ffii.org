
<!doctype html public "-//w3c//dtd html 4.0 transitional//en" >
<html>
	<head>
		<title>MCPP License Overview</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="/library/shared/common/css/ie4.css">
		<link rel="stylesheet" type="text/css" href="/library/shared/comments/css/ie5.css">
		<link href="Members.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="/library/flyoutmenu/default.css">
		<!-- Start: ToolBar V2.5-->
		<script language="JavaScript" src='/library/toolbar/toolbar.js'></script>
		<script language="JavaScript" src='/library/toolbar/global.js'></script>
		<!-- End: ToolBar V2.5-->
	</head>
	<body>
		<form name="LicenseOverview" method="post" action="LicenseOverview.aspx" id="LicenseOverview">
<input type="hidden" name="__VIEWSTATE" value="dDwtNjYwNjk5ODM4O3Q8O2w8aTwxPjs+O2w8dDw7bDxpPDE+Oz47bDx0PDtsPGk8MT47aTwzPjs+O2w8dDxwPGw8XyFJdGVtQ291bnQ7PjtsPGk8MTc+Oz4+O2w8aTwwPjtpPDE+O2k8Mj47aTwzPjtpPDQ+O2k8NT47aTw2PjtpPDc+O2k8OD47aTw5PjtpPDEwPjtpPDExPjtpPDEyPjtpPDEzPjtpPDE0PjtpPDE1PjtpPDE2Pjs+O2w8dDw7bDxpPDA+Oz47bDx0PEA8OTEzOzkxMztBdXRoZW50aWNhdGlvbiBTZXJ2aWNlcyBTZXJ2ZXIgKGluY2x1ZGluZyByZXN0cmljdGVkIHByb3RvY29scykgKjs5MTM7OTEzOzkxMzs+Ozs+Oz4+O3Q8O2w8aTwwPjs+O2w8dDxAPDExMDA7MTEwMDtDZXJ0aWZpY2F0ZSBTZXJ2aWNlcyBTZXJ2ZXIgKjsxMTAwOzExMDA7MTEwMDs+Ozs+Oz4+O3Q8O2w8aTwwPjs+O2w8dDxAPDkwODs5MDg7Q29sbGFib3JhdGlvbiBTZXJ2ZXI7OTA4OzkwODs5MDg7Pjs7Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8QDw5MTE7OTExO0RpZ2l0YWwgUmlnaHRzIE1hbmFnZW1lbnQgU2VydmVyIChpbmNsdWRpbmcgcmVzdHJpY3RlZCBwcm90b2NvbHMpICo7OTExOzkxMTs5MTE7Pjs7Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8QDw5MDc7OTA3O0ZpbGUgU2VydmVyOzkwNzs5MDc7OTA3Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTIwOzkyMDtHZW5lcmFsIFNlcnZlciAoaW5jbHVkaW5nIHJlc3RyaWN0ZWQgcHJvdG9jb2xzKSAqOzkyMDs5MjA7OTIwOz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTE5OzkxOTtHZW5lcmFsIFNlcnZlciB3aXRob3V0IHJlc3RyaWN0ZWQgcHJvdG9jb2xzOzkxOTs5MTk7OTE5Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTE0OzkxNDtNZWRpYSBTdHJlYW1pbmcgU2VydmVyOzkxNDs5MTQ7OTE0Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTE1OzkxNTtNdWx0aXBsYXllciBHYW1lcyBTZXJ2ZXI7OTE1OzkxNTs5MTU7Pjs7Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8QDw5MTc7OTE3O1ByaW50L0ZheCBTZXJ2ZXI7OTE3OzkxNzs5MTc7Pjs7Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8QDw5MjE7OTIxO1Byb3h5L0ZpcmV3YWxsL05BVCBTZXJ2ZXIgKGluY2x1ZGluZyByZXN0cmljdGVkIHByb3RvY29scykgKjs5MjE7OTIxOzkyMTs+Ozs+Oz4+O3Q8O2w8aTwwPjs+O2w8dDxAPDkwOTs5MDk7UHJveHkvRmlyZXdhbGwvTkFUIFNlcnZlciB3aXRob3V0IHJlc3RyaWN0ZWQgcHJvdG9jb2xzOzkwOTs5MDk7OTA5Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8MTExNTsxMTE1O1JpZ2h0cyBNYW5hZ2VtZW50IFNlcnZlciAoaW5jbHVkaW5nIHJlc3RyaWN0ZWQgcHJvdG9jb2xzKSAqOzExMTU7MTExNTsxMTE1Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTE2OzkxNjtTeXN0ZW1zIE1hbmFnZW1lbnQgU2VydmVyOzkxNjs5MTY7OTE2Oz47Oz47Pj47dDw7bDxpPDA+Oz47bDx0PEA8OTEwOzkxMDtUZXJtaW5hbCBTZXJ2ZXI7OTEwOzkxMDs5MTA7Pjs7Pjs+Pjt0PDtsPGk8MD47PjtsPHQ8QDw5MTI7OTEyO1ZpcnR1YWwgUHJpdmF0ZSBOZXR3b3JrIFNlcnZlcjs5MTI7OTEyOzkxMjs+Ozs+Oz4+O3Q8O2w8aTwwPjs+O2w8dDxAPDkxODs5MTg7V2ViIFNlcnZlcjs5MTg7OTE4OzkxODs+Ozs+Oz4+Oz4+O3Q8cDxsPFRleHQ7PjtsPCo7Pj47Oz47Pj47Pj47Pj47Pg==" />

			<!--Draw Toolbar-->
			<script language="JavaScript" src='/public/consent/toolbar/local_toolbar.js'></script>
			<!--Draw Toolbar-->
			<table border="0" height="100%" width="100%" cellpadding="0" cellspacing="0">
				<!-- DEFINE TABLE LAYOUT -->
				<tr>
					<td width="1" rowspan="999" bgcolor="#f1f1f1" nowrap><img src="/library/images/gifs/homepage/1ptrans.gif" width="8" height="1" alt="" border="0"></td>
					<td width="176" bgcolor="#f1f1f1" rowspan="999" valign="top" nowrap>
						
<!--START LEFT NAVIGATION-->
<script language="javascript">
function ExpandCollapse(img)
{
	var id = img.UniqueID;
	var row = document.all("tr"+id);
	if (row.style.display == "")
	{
		document.all("img"+id).src = "UI_Plus.gif";
		row.style.display = "none";
	}
	else
	{
		document.all("img"+id).src = "UI_Minus.gif";
		row.style.display = "";
	}
	
}
</script>
<table>
	<tr>
		<td width="8" bgcolor="#f0f0eb" style="width:8px;height:1px;" nowrap></td>
		<td width="100%" bgcolor="#f0f0eb" style="height:1px;"></td>
	</tr>
	<tr>
		<td colspan="2"><a href="Default.aspx">MCPP Home</a></td>
	</tr>
	<tr>
		<td colspan="2"><a href="faq.aspx">FAQ</a></td>
	</tr>
	<tr>
		<td colspan="2"><a href="EntryRequirements.aspx">Program Entry Requirements</a></td>
	</tr>
	<tr>
		<td colspan="2"><a href="LicenseOverview.aspx">License Overview</a></td>
	</tr>
	<tr>
		<td colspan="2"><a href="PricingOverview.aspx">Pricing Overview</a></td>
	</tr>
	<tr>
		<td colspan="2"><a href="LitSamples.aspx">Sample Technical Documentation</a></td>
	</tr>
	
	<!--
	<tr>
		<td colspan="2"><a href="TaskSelection.aspx">Task Selection</a></td>
	</tr>
	-->
	
	<tr>
		<td colspan="2"><a href="OptionalDocuments.aspx">Optional Documents</a></td>
	</tr>
	
	<tr>
		<td colspan="2" class="navheader"><br><br>Server Tasks</td>
	</tr>
	<tr>
		<td colspan="2">
			<table>
				
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="913" 
								style="cursor:hand">
								<img id="img913"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Authentication Services Server (including restricted protocols) *
								</span>
							</td>
						</tr>
						<tr id="tr913" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=913'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=913'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="1100" 
								style="cursor:hand">
								<img id="img1100"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Certificate Services Server *
								</span>
							</td>
						</tr>
						<tr id="tr1100" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=1100'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=1100'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="908" 
								style="cursor:hand">
								<img id="img908"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Collaboration Server
								</span>
							</td>
						</tr>
						<tr id="tr908" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=908'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=908'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="911" 
								style="cursor:hand">
								<img id="img911"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Digital Rights Management Server (including restricted protocols) *
								</span>
							</td>
						</tr>
						<tr id="tr911" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=911'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=911'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="907" 
								style="cursor:hand">
								<img id="img907"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									File Server
								</span>
							</td>
						</tr>
						<tr id="tr907" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=907'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=907'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="920" 
								style="cursor:hand">
								<img id="img920"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									General Server (including restricted protocols) *
								</span>
							</td>
						</tr>
						<tr id="tr920" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=920'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=920'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="919" 
								style="cursor:hand">
								<img id="img919"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									General Server without restricted protocols
								</span>
							</td>
						</tr>
						<tr id="tr919" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=919'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=919'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="914" 
								style="cursor:hand">
								<img id="img914"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Media Streaming Server
								</span>
							</td>
						</tr>
						<tr id="tr914" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=914'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=914'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="915" 
								style="cursor:hand">
								<img id="img915"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Multiplayer Games Server
								</span>
							</td>
						</tr>
						<tr id="tr915" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=915'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=915'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="917" 
								style="cursor:hand">
								<img id="img917"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Print/Fax Server
								</span>
							</td>
						</tr>
						<tr id="tr917" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=917'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=917'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="921" 
								style="cursor:hand">
								<img id="img921"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Proxy/Firewall/NAT Server (including restricted protocols) *
								</span>
							</td>
						</tr>
						<tr id="tr921" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=921'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=921'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="909" 
								style="cursor:hand">
								<img id="img909"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Proxy/Firewall/NAT Server without restricted protocols
								</span>
							</td>
						</tr>
						<tr id="tr909" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=909'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=909'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="1115" 
								style="cursor:hand">
								<img id="img1115"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Rights Management Server (including restricted protocols) *
								</span>
							</td>
						</tr>
						<tr id="tr1115" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=1115'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=1115'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="916" 
								style="cursor:hand">
								<img id="img916"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Systems Management Server
								</span>
							</td>
						</tr>
						<tr id="tr916" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=916'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=916'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="910" 
								style="cursor:hand">
								<img id="img910"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Terminal Server
								</span>
							</td>
						</tr>
						<tr id="tr910" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=910'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=910'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="912" 
								style="cursor:hand">
								<img id="img912"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Virtual Private Network Server
								</span>
							</td>
						</tr>
						<tr id="tr912" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=912'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=912'>Licensing Information</a>
							</td>
						</tr>
					
						<tr>
							<td class="navlinks" 
								style="color:#003399"
								onclick="ExpandCollapse(this)" 
								UniqueID="918" 
								style="cursor:hand">
								<img id="img918"
									src="UI_Plus.gif">
								</img>
								<span
									onmouseover="this.style.color='red'"
									onmouseout="this.style.color='#003399'">
									Web Server
								</span>
							</td>
						</tr>
						<tr id="tr918" style="display:none">
							<td>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskDetails.aspx?pkid=918'>Protocol Details</a>
							<br>
							&nbsp;&nbsp;<a style="color:blue" onmouseover="this.style.color='red'"
									onmouseout="this.style.color='blue'"
									href='TaskInfo.aspx?pkid=918'>Licensing Information</a>
							</td>
						</tr>
					
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding-top:20px;">
			*&nbsp;<span style="font-size:8pt">These 
				tasks involve restricted protocols related to authentication and digital rights 
				management, and require an additional qualification process. See <a href="EntryRequirements.aspx" style="font-size:8pt;">
					Program Entry Requirements</a> for more information</span></td>
	</tr>
</table>
<!--END LEFT NAVIGATION-->

					</td>
					<td width="1" rowspan="999" bgcolor="#999999" nowrap><img src="/library/images/gifs/homepage/1ptrans.gif" width="1" height="1" alt="" border="0"></td>
					<td width="5" rowspan="999"><img src="/library/images/gifs/homepage/1ptrans.gif" width="8" height="1" alt="" border="0"></td>
					<td><img src="/library/images/gifs/homepage/1ptrans.gif" width="600" height="1" alt="" border="0"></td>
					<td width="5" rowspan="999"><img src="/library/images/gifs/homepage/1ptrans.gif" width="8" height="1" alt="" border="0"></td>
				</tr>
				<tr>
					<td valign="top" width="470">
						<h1><div style="color:gray">Microsoft Communications Protocol Program</div>
							License Overview
						</h1>
						<BR>
						<!-- content -->
The purpose of this overview of the Microsoft Communications Protocol Program ("MCPP" or "Program") is to generally describe the features of the Program and is not intended to modify the Program in any way or to serve as an interpretation of any Program agreements. The applicable signed Program agreements define the terms and conditions of the Program for each licensee. Please check this page periodically for updates to this overview. 
<BR>
<BR>
<H2>Program Agreements</H2>
A licensee under the Program will need to sign one of the license agreements offered under the Program.
The Microsoft Protocol License Agreement for Development and Product Distribution
(“<a href="LicenseAgreement.aspx">MCPP Development Agreement</a>”) grants to licensees,
for a fee, certain rights under applicable Microsoft intellectual property to use the Microsoft technical
documentation to develop implementations of the communications protocols for all server tasks enabled by
the MCPP protocols that the licensee selects. An <a href="evalagr.aspx">Evaluation Agreement</a> is also
available for companies that wish to review the technical documentation for potential license under an
MCPP Development Agreement. Licensees may also sign optional addenda or ancillary agreements as appropriate
for certain choices they may make (<a href="OptionalDocuments.aspx">Optional Documents</a>). For example,
certain <a href="RPA.aspx">Restricted Protocols</a> (as described on this Web site) are also available for
license subject to additional requirements.   
<BR>
<BR>
<H2>MCPP Development Agreement Structure</H2>
The MCPP Development Agreement addresses development, distribution and sublicensing activities.
MCPP Development Agreements authorize implementations of the communications protocols in server
software that use the protocols to interoperate or communicate natively with Windows desktop client
operating systems. The MCPP Development Agreement also grants rights to distribute object code
versions of licensee implementations of the protocols under the licensee’s product brands directly
or through channel distribution, subject to license fees
and certain requirements. The license fees vary according to the server tasks performed by the licensed
implementation software, i.e., a licensee’s choice of server task(s) for their products that implement
the communications protocols determines the license fee or royalty structure (e.g., flat fee, flat
fee per unit, or percentage of revenue basis). This Web site identifies and describes briefly the
server tasks that a licensee may choose to implement under an MCPP Development Agreement.
(<I>For details on license fees, see the </I><a href="PricingOverview.aspx">Pricing Overview</a>.) 
<BR>
<BR>
While the MCPP Development Agreement does not include a general right to sublicense Microsoft’s intellectual property, a licensee may provide its implementation source code to a third party who is also a qualified MCPP Development License licensee for further development and distribution. A licensee may also disclose its implementation source code to a potential customer for on-site review on licensee’s premises, subject to certain confidentiality conditions. 
<BR>
<BR>
Under the MCPP Development Agreement, licensees are entitled to receive updates and correction assistance relating to the technical documentation.  Updates for new and modified communications protocols relating to the server tasks the licensee has previously obtained technical documentation will be made available to licensees based on the delivery schedule described in the MCPP Development Agreement.  Microsoft will also offer correction assistance regarding any inaccuracies or omissions in the technical documentation.  Requests for assistance relating to implementation or software development issues are not covered under the MCPP Development Agreement.  (<I>For answers to frequently asked questions, see the </I><a href="#LICFAQ">FAQ</a>)
<BR>
<BR>
The term of the MCPP Development Agreement is five years, at which time development rights expire but
certain distribution rights continue indefinitely, subject in either case to royalty oblications and
applicable termination provisions.  Licensees may also renew their rights by entering into a new MCPP
development agreement under then current terms.  The MCPP Development Agreement also contains provisions
addressing other issues typical of technology licensing arrangements, such as protection of intellectual
property, rights and remedies of the parties, etc. 
<BR>
<BR>
<H2>Restricted Protocols</H2>
The License Agreement does not, by itself, provide access to or authorize use of specific restricted
protocols related to certain security-related areas such as authentication and digital rights management.
(Restricted protocols are identified on this site with an asterisk in the lists of protocols for each task.)
If a licensee would like to implement those protocols, it must sign a <a href="RPA.aspx">Restricted Protocol Addendum</a>
for use with its License Agreement, and meet additional <a href="EntryRequirements.aspx">Program Entry Requirements</a>
before receiving access to the restricted protocols. Licensees who desire to implement restricted protocols must
complete an assessment performed by an independent third party designated by Microsoft that reviews the ability
and intent of the licensee to protect the security and integrity of the security-related technologies
described above as well as the customer systems and data that depend on those technologies. For more information
on the process for licensing restricted protocols see the <a href="QualificationAssessment.aspx">Qualification Assessment Process</a>.
The Restricted Protocols Addendum also includes terms that address the concerns described above for
licensee products that use restricted protocols. 
<BR>
<BR>
<a id="#LICFAQ">
<H2><U>FAQ Regarding Microsoft Protocol License Agreement for Development and Product Distribution</U></H2>
</a>
<BR>
<BR>
<B>Q: I noticed the license grant in the agreement doesn’t mention trademarks.  What are the rights and restrictions regarding Microsoft trademarks under the license agreement?</B>
<BR>
<B>A:</B> The license agreement doesn’t include any rights to use any Microsoft trademarks, but this doesn’t affect licensees’ ability to seek qualification under any separate and existing Microsoft trademark or logo licensing programs.  Licensees can also publicly disclose the fact that they have entered into the agreement and have implemented MCPP protocol(s) in their products, as long as they don’t use any Microsoft logo in so doing.  However, the license agreement doesn’t require Microsoft to license any special logo or brand indicating that licensees have implemented MCPP protocols in their products, or impose any requirements on licensees to qualify their products under such a licensing program. 
<BR>
<BR>
<B>Q: Why can’t I distribute my implementation in source code form?  And why does Microsoft care about “other licenses?”</B>
<BR>
<B>A:</B> The specifications used to create your protocol implementations are confidential and, along with the source code of those implementations, include Microsoft trade secrets.  However, because other MCPP licensees have agreed to MCPP license terms (including distribution and confidentiality provisions), you can distribute the source code of your implementation to them.  The license agreement also permits you to allow others to view the source code of your implementations on-site at your place of business for evaluation purposes, under suitable non-disclosure agreements.  
<BR><BR>
In addition to not disclosing your source code directly (other than as just described), you also need to make sure not to subject your implementation to any other licenses that would require such source code disclosure.  For example, under certain circumstances, other licenses may require your implementation to be disclosed in source code form when you distribute your implementation with other technology that is already subject to that other license.  In short, you can’t subject your authorized implementations to any license that requires you do things that are contrary to the scope of your license and your obligations under the license agreement.
<BR>
<BR>
<B>Q: If I receive a protocol implementation from another MCPP licensee, can I receive documentation updates and correction assistance regarding the MCPP protocols contained in that implementation?</B>
<BR>
<B>A:</B> Yes. If the protocol implementation you receive from another MCPP licensee (“received implementation”) performs a task for which you’ve previously chosen to receive protocol documentation under your license agreement, you will automatically receive documentation updates and be eligible for correction assistance regarding the protocol documentation.  However, if the received implementation performs a task for which you have not previously chosen to receive protocol documentation, in order to receive documentation updates and correction assistance you will need to let Microsoft know that you have selected that additional task to add to your license agreement.    
<BR>
<BR>
<B>Q: I’ve already signed a license agreement, but have only chosen to receive protocol documentation for certain tasks.  Can I still evaluate additional protocol documentation for other tasks in order to help me decide whether to receive that additional documentation under my license agreement?</B>
<BR>
<B>A:</B> Yes. Microsoft has a program for evaluating the general quality of the protocol documentation for some or all (at the evaluator’s choice) MCPP tasks to aid in the decision of whether to enter into a license agreement.  The form of Evaluation Agreement<link to form> used for such evaluations can be adapted for your use as a current MCPP licensee – for instance, eliminating any fees for your “supplemental” evaluation.  If you wish to undertake such a “supplemental” evaluation, please contact the MCPP licensing team<add email link>.
<BR>
<BR>
<B>Q: In order to receive documentation for all the MCPP protocols, do I have to specifically identify all the tasks for which I might want to use the documentation at the time I sign the license agreement?  And do I have to pay royalties on all tasks for which I’ve received protocol documentation?</B>
<BR>
<B>A:</B> No, to both questions.  
<BR><BR>
Regarding task selection, if you would like to receive documentation for all the MCPP protocols, and don’t know or don’t want to specify the tasks for which you might want to use the documentation when you sign the license agreement, you can choose the “General Server” task, which includes documentation of all the MCPP protocols, for use for all the specific tasks that are currently defined (and for tasks other than those as well).  The license agreement also gives you the ability, if you wish, to receive only the protocol documentation for certain tasks, which you will need to identify in order to receive only that selected documentation.  You can also elect to add protocol documentation for other tasks at other times during the term of your agreement.  
<BR><BR>
Regarding royalties, you will only pay royalties based on the tasks performed by the protocol implementations you actually develop and distribute, even if you receive the protocol documentation for other tasks.  In other words, your royalties aren’t calculated on the basis of what protocol documentation you receive, but on what task(s) your developed and distributed protocol implementations actually perform.    
<BR>
<BR>
<B>Q: For the tasks I select under my license agreement, will I receive updated documentation for modified or new protocols that Microsoft might implement in Windows clients in future to perform those same tasks, and if so, when?</B>
<BR>
<B>A:</B> Yes.  If there is a last major beta of future Windows client service packs or new releases, Microsoft will provide you with preliminary documentation for modified and new protocols contained in those service packs or new releases within 30 days after that last major beta.  Of course, as is customary regarding early/beta technology, the preliminary documentation will be provided without any obligation to correct documentation errors or commitment to a particular standard of documentation quality.  You will always receive the final documentation for the corresponding protocols contained in the commercially released version of the relevant Windows client service pack or new release within 30 days of commercial release.  Additionally, if the new or modified protocol is contained in a Windows client update that Microsoft posts to the Windows Update (or successor) site, then you’ll receive the updated documentation on a slightly different schedule, namely, within 30 days after the end of the month that the update was posted.
<BR>
<BR>
<B>Q: The license agreement says that Microsoft will provide correction assistance regarding errors and omissions in the protocol documentation licensed under the agreement.  How will this work?</B>
<BR>
<B>A:</B> Correction assistance will be provided only to the entity licensing MCPP protocol documentation under the license agreement.  Requests for correction assistance must be submitted through the http://www.microsoft.com/support website.  The request will be analyzed by a support professional and you will receive a direct acknowledgement (i.e., in addition to or in place of an auto-generated email) from Microsoft (or a third party support service provider on Microsoft’s behalf) within 24 hours of your request for correction assistance.  Support professionals providing correction assistance are available Monday-Friday, other than on U.S. holidays, from 9 a.m. (Pacific time) until 6 p.m. (Pacific time).  
<BR><BR>
All corrections of MCPP protocol documentation provided under correction assistance will be provided in the form of a web-based, remotely delivered response in the English language.  Microsoft will need to determine whether each request identifies an inaccuracy or omission in the MCPP protocol documentation and therefore is eligible for correction assistance.  In connection with this, Microsoft may ask appropriate diagnostic questions, make troubleshooting suggestions, provide preliminary informal technical advice and/or request additional information to further troubleshoot the issue, and you may be asked to help by cooperating in such diagnostic/problem determination activities (which may include performing network monitoring traces using Microsoft Network Monitor).  Microsoft will notify you if it is determined that an MCPP protocol documentation inaccuracy or omission does not exist and, therefore, that the request does not qualify for correction assistance.
<BR><BR>
Any support that may be available to the licensee from Microsoft under a separate support agreement (i.e., unrelated to the Microsoft Communications Protocol Program) is not transferable to or creditable against correction assistance.  
<BR>
<BR>
<B>Q: I noticed that the license under the license agreement includes intellectual property that Microsoft owns or has the right to sublicense without a fee.  What about intellectual property that Microsoft may have rights to sublicense with a fee?</B>
<BR>
<B>A:</B> If you are concerned about this, please let Microsoft know<add email link>, and a provision addressing it will be added to your license agreement.
<BR>
<BR>
<B>Q: My company has several subsidiary companies that may also have an interest in the MCPP protocol documentation.  How can those subsidiary companies obtain rights to use the MCPP protocol documentation?</B>
<BR>
<B>A:</B> If your interested subsidiaries are wholly-owned, they will be permitted to exercise rights under your license agreement if (a) you provide Microsoft with a Participating Subsidiary Agreement<add link> executed by both you and your subsidiary, and (b) the proposed participating subsidiary satisfies applicable Program Entry Requirements <add link> within 90 days of the effective date of that agreement.  (Under the Participating Subsidiary Agreement, a wholly-owned subsidiary’s rights under a Participating Subsidiary Agreement will end if it ceases to be a wholly-owned subsidiary, and your company and each of its wholly-owned participating subsidiaries will be jointly and severally liable for any violation by the participating subsidiary of its obligations under your license agreement.)
<BR><BR>
If your subsidiaries are not wholly-owned, they will likely need to sign their own license agreement.
<BR>
<BR>
						
						<!-- content -->
						
					</td>
				</tr>
				<tr>
					<td height="100%">&nbsp;</td>
				</tr>
				<tr>
					<td valign="bottom">
						<table>
 <tr>
  <td width="600" class="footer">
	
  </td>
 </tr>
</table>

					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			<!-- BEGIN FOOTER -->
			<table id='idFooter' style='WIDTH:100%;BACKGROUND-COLOR:white' cellspacing='0' cellpadding='0' border='0'>
				<tr valign='middle'>
					<td id='idFooterRow1' style='WIDTH:100%;HEIGHT:20px;BACKGROUND-COLOR:#003399' nowrap>
						&nbsp;<a target='_top' style='FONT:bold xx-small Verdana; CURSOR:hand; COLOR:#ffffff; TEXT-DECORATION:none' href='/isapi/goregwiz.asp?target=/regwiz/forms/contactus.asp' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Contact 
							Us</a> &nbsp;<span style='FONT:bold xx-small Verdana; COLOR:#ffffff'>&nbsp;|</span>
						&nbsp;<a target='_top' style='FONT:bold xx-small Verdana; CURSOR:hand; COLOR:#ffffff; TEXT-DECORATION:none' href='/isapi/gomscom.asp?target=/legal/' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Legal</a>
					</td>
				</tr>
				<tr valign='middle'>
					<td id='idFooterRow2' style='WIDTH:100%;HEIGHT:30px;BACKGROUND-COLOR:#003399' nowrap>
						<span style='FONT:xx-small Verdana; COLOR:#ffffff'>&nbsp;© 2002 Microsoft 
							Corporation. All rights reserved.&nbsp;</span> &nbsp;<a target='_top' style='FONT:xx-small Verdana; CURSOR:hand; COLOR:#ffffff; TEXT-DECORATION:none' href=' /isapi/gomscom.asp?target=/info/cpyright.htm' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Terms 
							of Use</a>&nbsp;&nbsp;<a target='_top' style='FONT:xx-small Verdana; CURSOR:hand; COLOR:#ffffff; TEXT-DECORATION:none' href='/isapi/gomscom.asp?target=/info/privacy.htm' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Privacy 
							Statement </a>&nbsp;&nbsp;<a target='_top' style='FONT:xx-small Verdana; CURSOR:hand; COLOR:#ffffff; TEXT-DECORATION:none' href='/isapi/gomscom.asp?target=/enable/' onmouseout="this.style.color = '#FFFFFF'" onmouseover="this.style.color = '#FF3300'">Accessibility
						</a>
					</td>
				</tr>
			</table>
			<!--END FOOTER -->
		</form>
	</body>
</html>
