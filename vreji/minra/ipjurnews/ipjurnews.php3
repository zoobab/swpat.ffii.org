<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN"
"http://www.w3.org/TR/REC-html40/strict.dtd">
<html>
<head>
<title>Patentanwalt [Patent Attorney] Axel H. Horns</title>
<style type="text/css">
<!--
body  {font-family:zurich,arial,helvetica,geneva,sans-serif; font-size:12px; color:black; margin-left:2%; margin-right:2%; background-color:white;}
P {font-family:zurich,arial,helvetica,geneva,sans-serif; font-size:12px; text-align:justify; margin-top:1em; margin-bottom:0; color:black; background-color:transparent;}
P.grey {font-family:zurich,arial,helvetica,geneva,sans-serif; font-size:12px; text-align:justify; background-color:#cccccc; color:black; margin-top:1em; margin-bottom:0;}
P.right {font-family:zurich,arial,helvetica,geneva,sans-serif; font-size:12px; text-align:right; margin-top:1em; margin-bottom:0; color:black; background-color:transparent;}
A {font-family:zurich,arial,helvetica,geneva,sans-serif; font-size:11px; text-decoration: none;}
A:link {Color:black; background-color:transparent; font-weight:bold;}
A:active {Color:green; background-color:transparent;  font-weight:bold;}
A:visited {Color:black; background-color:transparent; font-weight:bold;}
A:hover {color:black; background-color:red; font-size:11px;}
H1 {font-family:zurich,arial,helvetica,geneva,sans-serif; font-size:20px; text-align:left; font-weight:bold; margin-top:0em; margin-bottom:0;}
H2 {font-family:zurich,arial,helvetica,geneva,sans-serif; font-size:16px; text-align:left; font-weight:bold; margin-top:0em; margin-bottom:0;}
H3 {font-family:zurich,arial,helvetica,geneva,sans-serif; font-size:14px; text-align:left; margin-top:1em; margin-bottom:0;}
LI {font-family:zurich,arial,helvetica,geneva,sans-serif; font-size:12px; text-align:justify; margin-top:1em; margin-bottom:0; color:black; background-color:transparent;}
center.text {font-family:zurich,arial,helvetica,geneva,sans-serif; font-size:12px; text-align:center; margin-top:1em; margin-bottom:0; color:black; background-color:transparent;}
-->
</style><META NAME="AUTHOR" CONTENT="PA Axel H Horns">
<META NAME="KEYWORDS" CONTENT="Patentanwalt Axel H Horns, European Patent Attorney, European Trade Mark Attorney, patents, trademarks, marks, software, intellectual property, patent attorney, patent office, law, SWPAT, prosecution, litigation, Germany, Europe, United Kingdom, England, Scotland, news, newsticker, latest developments, Intellectual Property Notes, mailing list">
<META NAME="DESCRIPTION" CONTENT="IPJUR provides information on Intellectual Property law, in particular with regard to Europe">
<META NAME="GENERATOR" content="P1">
<META NAME="ROBOTS" CONTENT="INDEX,FOLLOW">
</head>
<body text="#000000" bgcolor="#FFFFFF" link="#000000" alink="#FF0000" vlink="#FF0000">

<TABLE WIDTH="100%" border="0" cellpadding="0" cellspacing="0" BGCOLOR="#A0D7FF">
<TR BGCOLOR="#A0D7FF">
    <TD ALIGN="LEFT" VALIGN="CENTER" WIDTH="150" BGCOLOR="#A0D7FF">
    <TABLE BORDER="0" WIDTH="150" cellpadding="0" cellspacing="0">
    <TR BGCOLOR="#000000"><TD>
    <A HREF="http://www.ipjur.com/"><IMG SRC="./ipjur.gif" border="0" hspace="0" vspace="0" HEIGHT="40" WIDTH="150" align="middle"></a>
    </TD></TR>
    </TABLE>
    </TD>
    <TD ALIGN="CENTER" VALIGN="CENTER" colspan="4">
    <IMG SRC="./iplaw.gif" border="0" hspace="0" vspace="0" HEIGHT="40" align="middle">
        </TD>
</TR>
<TR BGCOLOR="#000000">

    <TD HEIGHT="18" WIDTH="20%" ALIGN="LEFT" VALIGN="BOTTOM">
        &nbsp;
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./02.php3"><IMG SRC="./arrow2.gif" ALT="I2P INFO" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./i2pa.gif" ALT="I2P" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./01.php3"><IMG SRC="./arrow2.gif" ALT="SOFTWARE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./software.gif" ALT="SOFTWARE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./03.php3"><IMG SRC="./arrow2.gif" ALT="MOVING SPOTS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./moving.gif" ALT="MOVING SPOTS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" VALIGN="BOTTOM" ALIGN="RIGHT">
        <A HREF="./04.php3"><IMG SRC="./arrow2.gif" ALT="PRACTICE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./practice.gif" ALT="PRACTICE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>
</TR>
</TABLE>

<TABLE WIDTH="100%" CELLPADDING="0" cellspacing="0" BORDER="0">
<TR>
<TD WIDTH="150" ALIGN="LEFT" VALIGN="TOP" BGCOLOR="#A0D7FF">

<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=3 BGCOLOR="#A0D7FF">
<TR><TD><p>&nbsp;</p></td></tr>
<TR>
    <TH COLSPAN="2" BGCOLOR="#A0D7FF">
        <B>LEGAL ASPECTS</B>
    </TH>
</TR>

<TR><TD>&nbsp;</td><td>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.wto.org/wto/eol/e/pdf/27-trips.pdf"><IMG SRC="./arrow.gif"
        ALT="WORLD TRADE ORGANISATION" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.wto.org/">WTO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.wto.org/english/docs_e/legal_e/27-trips.pdf"><IMG SRC="./arrow.gif"
        ALT="AGREEMENT ON TRADE-RELATED APSECTS OF INTELLECTUAL PROPERTY RIGHTS" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.wto.org/english/docs_e/legal_e/27-trips.pdf">TRIPS</A>
        <BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.wipo.int/"><IMG SRC="./arrow.gif"
        ALT="World Intellectual Property Organisation"
        HEIGHT=12 WIDTH=12 BORDER=0></A></TD>
    <TD>
        <A HREF="http://www.wipo.int/">WIPO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/members/convention/index.html"><IMG SRC="./arrow.gif"
        ALT="WIPO Convention" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.wipo.int/members/convention/index.html">WIPO Convention</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/treaties/ip/paris/index.html"><IMG SRC="./arrow.gif"
        ALT="Paris Convention" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.wipo.int/treaties/ip/paris/index.html">WIPO Paris Convention</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/treaties/ip/berne/index.html"><IMG SRC="./arrow.gif"
        ALT="Berne Convention" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.wipo.int/treaties/ip/berne/index.html">WIPO Berne Convention</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/pct/en/index.html"><IMG SRC="./arrow.gif"
        ALT="Patent Cooperation Treaty (PCT)" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.wipo.int/pct/en/index.html">WIPO PCT System</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/madrid/en/index.html"><IMG SRC="./arrow.gif"
        ALT="Madrid Agreement on International Registration of Trade Marks" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.wipo.int/madrid/en/index.html">WIPO Madrid System</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/hague/en/index.html"><IMG SRC="./arrow.gif"
        ALT="Hague Agreement on Protection of Models" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.wipo.int/hague/en/index.html">WIPO Hague System</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/clea/en/index.html"><IMG SRC="./arrow.gif"
        ALT="WIPO COLLECTION OF LAWS FOR ELECTRONIC ACCESS (CLEA)" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.wipo.int/clea/en/index.html">WIPO CELA</A>
        <BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.epo.co.at/"><IMG SRC="./arrow.gif"
        ALT="European Patent Office" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.epo.co.at">EPO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.european-patent-office.org/legal/epc/e/index.html"><IMG SRC="./arrow.gif"
        ALT="European Patent Convention" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.european-patent-office.org/legal/epc/e/index.html">EPO EPC</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.epo.co.at/espacenet/info/access.htm"><IMG SRC="./arrow.gif"
        ALT="esp@acenet" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.epo.co.at/espacenet/info/access.htm">esp@acenet DATABASE</A>
        <BR></TD>
</TR>


<TR><TD><A HREF="http://www.european-patent-office.org/dg3/search_dg3.htm"><IMG SRC="./arrow.gif"
        ALT="EPO Case Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.european-patent-office.org/dg3/search_dg3.htm">EPO Case Law</A>
        <BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://oami.eu.int/en/"><IMG SRC="./arrow.gif"
        ALT="OHIM" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://oami.eu.int/en/">OHIM</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://oami.eu.int/en/aspects/reg.htm"><IMG SRC="./arrow.gif"
        ALT="EU Community Trade Mark Regulations" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://oami.eu.int/en/aspects/reg.htm">OHIM CTM LAW</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://oami.eu.int/search/trademark/la/en_tm_search.cfm"><IMG SRC="./arrow.gif"
        ALT="EU Community Trade Mark Database" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://oami.eu.int/search/trademark/la/en_tm_search.cfm">OHIM CTM DATA</A>
        <BR></TD>
</TR>


<TR><TD><A HREF="http://oami.eu.int/search/LegalDocs/la/EN_Refused_index.cfm"><IMG SRC="./arrow.gif"
        ALT="OHIM Refused Marks" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://oami.eu.int/search/LegalDocs/la/EN_Refused_index.cfm">OHIM Refused Marks</A>
        <BR></TD>
</TR>


<TR><TD><A HREF="http://oami.eu.int/search/LegalDocs/la/EN_Opposition_index.cfm"><IMG SRC="./arrow.gif"
        ALT="OHIM Oppositions" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://oami.eu.int/search/LegalDocs/la/EN_Opposition_index.cfm">OHIM Oppositions</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://oami.eu.int/search/LegalDocs/la/EN_Cancellation_index.cfm"><IMG SRC="./arrow.gif"
        ALT="OHIM Cancellations" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://oami.eu.int/search/LegalDocs/la/EN_Cancellation_index.cfm">OHIM Cancellations</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://oami.eu.int/search/LegalDocs/la/EN_BoA_index.cfm"><IMG SRC="./arrow.gif"
        ALT="OHIM BoA Case Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://oami.eu.int/search/LegalDocs/la/EN_BoA_index.cfm">OHIM Case Law</A>
        <BR></TD>
</TR>


<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.dpma.de/"><IMG SRC="./arrow.gif"
        ALT="DPMA" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.dpma.de/">DPMA (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://bundesrecht.juris.de/bundesrecht/patg/index.html"><IMG SRC="./arrow.gif"
        ALT="DE Patent Act (GER)" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://bundesrecht.juris.de/bundesrecht/patg/index.html">DE Patent ACT (GER)</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://bundesrecht.juris.de/bundesrecht/gebrmg/index.html"><IMG SRC="./arrow.gif"
        ALT="DE Utility Model Act (GER)" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://bundesrecht.juris.de/bundesrecht/gebrmg/index.html">DE Utility Model Act (GER)</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://transpatent.com/gesetze/gintpue.html"><IMG SRC="./arrow.gif"
        ALT="DE Act on International Patent Agreements (GER)" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://transpatent.com/gesetze/gintpue.html">DE-IntPat&Uuml;G (GER)</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://bundesrecht.juris.de/bundesrecht/markeng/index.html"><IMG SRC="./arrow.gif"
        ALT="DE Trade Marks Act (GER)" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://bundesrecht.juris.de/bundesrecht/markeng/index.html">DE Trade Marks Act (GER)</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="https://dpinfo.dpma.de/"><IMG SRC="./arrow.gif"
        ALT="DPINFO DATABASE (GER)" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="https://dpinfo.dpma.de/">DPINFO DATABASE (GER)</A>
        <BR></TD>
</TR>


<TR><TD><A HREF="http://www.depatisnet.de/"><IMG SRC="./arrow.gif"
        ALT="DEPATISnet DATABASE" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.depatisnet.de/">DEPATISnet DATABASE</A>
        <BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.wipo.int/news/en/links/addresses/ip/index.htm"><IMG SRC="./arrow.gif"
        ALT="List of Patent and Trade Mark Offices with WWW site" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.wipo.int/news/en/links/addresses/ip/index.htm">List of Office Sites</A>
        <BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.uspto.gov/"><IMG SRC="./arrow.gif"
        ALT="United States Patent and Trade Mark Office" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://www.uspto.gov/">US-PTO</A>
        <BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://europa.eu.int/eur-lex/en/"><IMG SRC="./arrow.gif"
        ALT="EU Commission" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://europa.eu.int/eur-lex/en/">EU Commission</A>
        <BR></TD>
 </TR>

<TR><TD><A HREF="http://europa.eu.int/eur-lex/en/oj/index.html"><IMG SRC="./arrow.gif"
        ALT="EU Official Journal" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD>
        <A HREF="http://europa.eu.int/eur-lex/en/oj/index.html">EU Official Journal</A>
        <BR></TD>
</TR>

<TR>
<TD>&nbsp;</TD>


<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR>
    <TH COLSPAN="2" BGCOLOR="#A0D7FF">
        <P>PATENT OFFICES<BR>CENTRAL EUROPE</P>
    </TH>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.patent.bmwa.gv.at/"><IMG SRC="./arrow.gif"
        ALT="AT-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.patent.bmwa.gv.at/">AT-PTO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.ige.ch"><IMG SRC="./arrow.gif"
        ALT="CH-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.ige.ch">CH-PTO</A>
        <BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.patent.gov.uk/"><IMG SRC="./arrow.gif"
        ALT="UK-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.patent.gov.uk/">UK-PTO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.bie.minez.nl/engels/index.htm"><IMG SRC="./arrow.gif"
        ALT="NL-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.bie.minez.nl/engels/index.htm">NL-PTO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.european-patent-office.org/patlib/country/belgium/"><IMG SRC="./arrow.gif"
        ALT="BE-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.european-patent-office.org/patlib/country/belgium/">BE-PTO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.inpi.fr/"><IMG SRC="./arrow.gif"
        ALT="FR-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.inpi.fr/">FR-PTO</A><BR>
    </TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>


<TR><TD><A HREF="http://www.upv.cz/english/index.html"><IMG SRC="./arrow.gif"
        ALT="CZ-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.upv.cz/english/index.html">CZ-PTO</A>
        <BR></TD>
</TR>


<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR>
    <TH COLSPAN="2" BGCOLOR="#A0D7FF">
    <P>PATENT OFFICES<BR>SCANDINAVIA AND<br>THE BALTICS</P>
    </TH>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.dkpto.dk/english/start.htm"><IMG SRC="./arrow.gif"
        ALT="DK-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.dkpto.dk/english/start.htm">DK-PTO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.epa.ee/eng/index.htm"><IMG SRC="./arrow.gif"
        ALT="EE-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.epa.ee/eng/index.htm">EE-PTO</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.prh.fi/en.html"><IMG SRC="./arrow.gif"
        ALT="FI-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.prh.fi/en.html">FI-PTO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.vpb.lt/engl/"><IMG SRC="./arrow.gif"
        ALT="LT-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.vpb.lt/engl/">LT-PTO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.patentstyret.no/english/"><IMG SRC="./arrow.gif"
        ALT="NO-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.patentstyret.no/english/">NO-PTO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.uprp.pl/a_index.htm"><IMG SRC="./arrow.gif"
        ALT="PL-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.uprp.pl/a_index.htm">PL-PTO</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.prv.se/eng/index.html"><IMG SRC="./arrow.gif"
        ALT="SE-PTO" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.prv.se/eng/index.html">SE-PTO</A>
        <BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD>&nbsp;</td><TD><HR WIDTH=50%></td></tr>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR>
    <TH COLSPAN="2" BGCOLOR="#A0D7FF">
        <a href="http://www.llrx.com/">LLRX.COM Links:</a>
    </TH>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR>
    <TH COLSPAN="2" BGCOLOR="#A0D7FF">
        <P>LAW RESOURCES<BR>CENTRAL<br>EUROPE</P>
    </TH>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.llrx.com/features/uk2.htm"><IMG SRC="./arrow.gif"
        ALT="UK-Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/uk2.htm">UK-LAW</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.llrx.com/features/irish.htm"><IMG SRC="./arrow.gif"
        ALT="IE-Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/irish.htm">IE-LAW</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.llrx.com/features/dutch.htm"><IMG SRC="./arrow.gif"
        ALT="NL-Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/dutch.htm">NL-LAW</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.llrx.com/features/frenchlaw.htm"><IMG SRC="./arrow.gif"
        ALT="FR-Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/frenchlaw.htm">FR-LAW</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.llrx.com/features/belgian.htm"><IMG SRC="./arrow.gif"
        ALT="BE-Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/belgian.htm">BE-LAW</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.llrx.com/features/germanlaw.htm"><IMG SRC="./arrow.gif"
        ALT="DE-Law I" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/germanlaw.htm">DE-LAW I</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.llrx.com/features/german.htm"><IMG SRC="./arrow.gif"
        ALT="DE-Law II" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/german.htm">DE-LAW II</A>
        <BR></TD>
</TR>


<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR>
    <TH COLSPAN="2" BGCOLOR="#A0D7FF">
        <P>LAW RESOURCES<BR>SCANDINAVIA AND<br>THE BALTICS</P>
    </TH>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.llrx.com/features/scanda.htm"><IMG SRC="./arrow.gif"
        ALT="Scandinavia in General" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/scanda.htm">Scandinavia in General</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.llrx.com/features/estonian1.htm"><IMG SRC="./arrow.gif"
        ALT="EE-Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/estonian1.htm">EE-LAW</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.llrx.com/features/swedish2.htm"><IMG SRC="./arrow.gif"
        ALT="SE-Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/swedish2.htm">SE-LAW</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.llrx.com/features/finnish.htm"><IMG SRC="./arrow.gif"
        ALT="FI-Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/finnish.htm">FI-LAW</A>
        <BR></TD>
</TR>

<TR><TD><A HREF="http://www.llrx.com/features/polish.htm"><IMG SRC="./arrow.gif"
        ALT="PL-Law" BORDER=0
        HEIGHT=12 WIDTH=12></A></TD>
    <TD><A HREF="http://www.llrx.com/features/polish.htm">PL-LAW</A>
        <BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

</TR>
</TABLE>

<!-- common-footer -->
<!-- Search Google -->
<center>
<H3>Search IPJUR.COM</H3>
<FORM method=GET action="http://www.google.de/search">
<TABLE bgcolor="#A0D7FF"><tr><td>

<INPUT TYPE=text name=q size=17 maxlength=255 value="">
<INPUT TYPE=hidden name=hl value=en>
<input name=q size=25 type=hidden value="site:www.ipjur.com">
<br>
<INPUT type=submit name=btnG VALUE="Search">
<p>Powered by Google</p>
</td></tr></TABLE>
</FORM>
</center>
<!-- Search Google -->

</TD>

<TD ALIGN="LEFT" VALIGN="TOP">

<TABLE WIDTH="100%" CELLPADDING="0" cellspacing="0" BORDER="0">
<tr>
<td>&nbsp;&nbsp;</td>
<td>
<P>&nbsp;</p>

<H1><center>NEWS</center></H1>

<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td width=12% valign="top"><P CLASS=grey>DATE</P></td>
<td width=7% valign="top"><P CLASS=grey>FILE</P></td>
<td valign="top" align="center"><P CLASS=grey>TITLE</P></td>
</tr>


<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Editorial remark concerning this page.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-10-26</p></td>
<td width=7% valign="top"><P>N/A</P></td>
<td valign="top"><p><strong>[*]</strong>
Some months ago, a <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://e-observer.com/about/weblog.html">Weblog ("Blog")</a> page <IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://www.ipjur.com/03.php3">BLOG@IP::JUR</a> has been started in the section
"MOVING SPOTS" of this website providing fresh information together with some personal comment. As a consequence, this page will be updated less frequently and only on the occasion of very significant events. Visitors of this website seeking a more continous reoorting an commenting of IP matters might wish to visit the above-mentioned Blog page.</p>
</td>
</tr>

<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Directive on patentability of computer-implemented inventions passes European Parliament.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-09-24</p></td>
<td width=7% valign="top"><P>N/A</P></td>
<td valign="top"><p><strong>[EU]</strong>
There are  <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://yro.slashdot.org/yro/03/09/24/1253227.shtml?tid=155&tid=185&tid=99">reports</a> on  <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://www.slashdot.org/">Slashdot</a> according to which the Directive has passed the European Parliament but, however, apparently with an awful lot of amendments inspired by  <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://www.eurolinux.org/">Eurolinux</a>, in particular by  <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://www.ffii.de/">FFII e.V.</a>. The full picture of all details will become available probably by tomorrow.
For more information, see <IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://www.ipjur.com/03.php3">here</a>.</p>
</td>
</tr>


<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>German Ratification Bill concerning London Agreement on Patent Translations.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-09-09</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[DE]</strong>
The German Government has submitted two Draft Bills concerning the ratification and implementation of the <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://www.ige.ch/E/jurinfo/pdf/epc65_e.pdf">Agreement on the application of Article 65 of the Convention on the Grant of European Patents ("London Agreement")</a> with the Upper House ("Bundesrat") of the German Parliament. Read the texts <IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://www.ipjur.com/data/030815BR566-03.pdf">here</a> and <IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://www.ipjur.com/data/030815BR567-03.pdf">here</a>. [In German]</p>
</td>
</tr>



<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>German Bill on national implementation of EC biotechnology directive.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-09-09</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[DE]</strong>
The <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://europa.eu.int/eur-lex/pri/en/oj/dat/1998/l_213/l_21319980730en00130021.pdf">Directive 98/44/EC of the European Parliament and of the Council of 06 July 1998 on the legal protection of biotechnological inventions</a> has up to now not been implemented in German national law. Only recently the European Commission had decided to <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://europa.eu.int/rapid/start/cgi/guesten.ksh?p_action.gettxt=gt&doc=IP/03/991|0|RAPID&lg=EN">refer</a>, amongst other Member States, Germany to the European Court of Justice as she has still not implemented said Directive 98/44/EC. The Directive should have been written into national law by 30 July 2000. Now, the German Government has submitted a
draft bill concerning the implementation to the Upper House ("Bundesrat") of the German Parliament. Read the text of the draft bill <IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://www.ipjur.com/data/030815BR546-03.pdf">here.</a> [In German]</p>
</td>
</tr>


<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>European Parliament defers voting.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-08-29</p></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><p><strong>[EU]</strong>
The formal voting of the plenary session of the European Parliament on the final <IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://www.ipjur.com/data/030618JURI.pdf">report</a> of the Committee on Legal Affairs and the Internal Market
of the European Parliament on the proposal for a directive of the European Parliament and of the Council on the patentability of computer-implemented inventions (COM(2002) 92 - C5-0082/2002 - 2002/0047(COD)) has once more been delayed. Originally the ballot was scheduled to take place on June 30, 2003. Instead, it had been docketed for September 01, 2003. Now, perhaps in view of the
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://dbs.cordis.lu/cgi-bin/srchidadb?CALLER=NHP_EN_NEWS&ACTION=D&SESSION=&RCN=EN_RCN_ID:20801">
Brussels demonstration</a> staged by Eurolinux, the voting has again been shifted, maybe up to September 22, 2003 or even further.</p>
</td>
</tr>

<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EPO commitment to XML technologies in e-filing.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-07-18</p></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><p><strong>[EPC]</strong>
Paul Brewin, Manager of ePublication epoline Information Systems,
European Patent Office, gives a survey of EPO XML usage today and in future.
Read   Mr. Brewin's article
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.epo.co.at/news/info/2003_07_14_e.htm">here</a>.
</p>
</td>
</tr>


<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Council Regulation concerning customs action against goods
suspected of infringing certain intellectual property rights published.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-07-18</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[EU]</strong>
The EU Council administration has published a draft Regulation concerning customs action against goods
suspected of infringing certain intellectual property rights and the
measures to be taken against goods found to have infringed such
rights which looks rather final.
Read the regulation text under
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://register.consilium.eu.int/pdf/en/03/st11/st11033en03.pdf">here</a>.
</p>
</td>
</tr>


<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EC Commission taking eight countries to court in view of non-implementation of Directive 98/44/EC on the legal protection of biotechnological inventions.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-07-10</p></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><p><strong>[EU]</strong>
The European Commission has decided to refer Germany, Austria, Belgium, France, Italy, Luxembourg, the Netherlands and Sweden to the European Court of Justice, as they have still not implemented Directive 98/44/EC on the legal protection of biotechnological inventions. The Directive should have been written into national law by 30 July 2000. It aims to clarify certain principles of patent law applied to biotechnological inventions whilst ensuring that strict ethical rules are respected.
Read a press release text of the EU Commission
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/rapid/start/cgi/guesten.ksh?p_action.gettxt=gt&doc=IP/03/991|0|RAPID&lg=EN">here</a>.
</p>
</td>
</tr>

<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>German Design Protection Bill reaches Bundestag.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-07-09</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[DE]</strong>
The Draft Bill ("Geschmacksmusterreformgesetz") concerning design protection has now reached the first chamber of the German Parliament ("Bundestag").
Read the printed matter of the Bundestag with comments resulting from of the previous dabate of the second chamber ("Bundesrat")
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ipjur.com/data/1501075.pdf">here</a> [In German].
</p>
</td>
</tr>



<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>New Isue of JILT published.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-07-09</p></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><p><strong>[-]</strong>
The on-line Journal of Information Law & Technology JILT has come
with its issue 2003/1 comprising a number of interesting refereed articles on
problems of patent law and software-related inventions.
Read the table of contents <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://elj.warwick.ac.uk/jilt/03-1/">here</a>.</p>
</td>
</tr>

<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Another proposal for a revision of the European Patent Convention published.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-07-08</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[EU]</strong>
A proposal for a revision of the European Patent Convention drafted by the EU Presidency in view of the upcoming Community Patent system has been published. Read the text <IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ipjur.com/data/030627EPC-EU.pdf">here</a>.</p>
</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Final report of the Committee on Legal Affairs and the Internal Market</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-06-18</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[EU]</strong>
A final report of the Committee on Legal Affairs and the Internal Market
of the European Parliament on the proposal for a directive of the European Parliament and of the Council on the patentability of computer-implemented inventions (COM(2002) 92 - C5-0082/2002 - 2002/0047(COD)) has been published.
Read the final text <IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ipjur.com/data/030618JURI.pdf">here</a>.</p>
</td>
</tr>
<tr><td>&nbsp;</td></tr>

<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Revised text for a proposal for a Council Regulation on the Community patent.</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-06-11</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[EU]</strong>
A revised text for a proposal for a Council Regulation on the Community patent has been published.
Read the revised text <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://register.consilium.eu.int/pdf/en/03/st10/st10404en03.pdf">here</a>.</p>
</td>
</tr>

<tr><td>&nbsp;</td></tr>

<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>New W3C Patent Policy published</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-05-20</p></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><p><strong>[W3C]</strong>
The
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.w3c.org/"> World Wide Web Consortium</a> has published a new patent policy
based on review by the W3C Advisory Committee and the
public. Written by the Patent Policy Working Group, the policy governs
the handling of patents in the process of producing and implementing
W3C Recommendations.
The default policy for W3C standards is "Royalty Free" ("RF") licenses but under very
exceptional circumstances also other models, in particular "Reasonable and non-discriminatory" ("RAND") non-free licenses, are allowable.
In simple terms, the Patent Policy provides that all who participate in the development of a W3C Recommendation must agree to license essential claims (that is, patents that block interoperability) on a royalty-free (RF) basis. However, under certain circumstances, Working Group participants may exclude specifically identified patent claims from the Royalty-Free commitment. These exclusions are required shortly after publication of the first public Working Draft, reducing the likelihood that surprise patents will jeopardize collective Working Group efforts. Patent disclosures are required from W3C Members and requested of anyone else who sees the technical drafts and has actual knowledge of patents that may be essential.
Patent claims not available with terms consistent with the W3C Patent Policy will be addressed by a exception handling process.
Read the <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.w3.org/Consortium/Patent-Policy-20030520.html">W3C Patent Policy</a>.</p>
</td>
</tr>

<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>ICC Roadmap flags latest developments in intellectual property</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-05-18</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[ICC]</strong>
The
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.iccwbo.org/">International Chamber of Commerce (ICC)</a> sees itself as the voice of world business championing the global economy as a force for economic growth, job creation and prosperity.
Now in its fourth year, the ICC Roadmap has been published as a vital tool
for any professional wanting a snapshot of the intellectual property landscape.
Compiled by the more than 240 IP experts who make up the
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.iccwbo.org/home/intellectual_property/commission.asp">
ICC's Commission on
Intellectual Property</a>, this year's roadmap analyses such emerging IP issues as
information products, indigenous rights and biotechnology and genetic rights.
Read the <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.iccwbo.org/IP_Roadmap/FullVersion/roadmap2003-PartOne.pdf">ICC Roadmap Part 1/2</a> and the <IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.iccwbo.org/IP_Roadmap/FullVersion/roadmap2003-PartTwo.pdf">ICC Roadmap Part 2/2</a></p>
</td>
</tr>



<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>New EPLA Draft Version published</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-05-03</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[EPC]</strong>
Another version of a Draft Agreement on the establishment of a European Patent Litigation System and Draft Statute of the European Patent Court drawn up by the European Patent Office acting on behalf of the Working Party on Litigation have recently been published.
The subgroup of the EPLA working party will concentrate of the revised EPLA and the Statute of the European Patent Court at the coming meeting of the group this year on May 19-21.
In view of the recent progress concerning the European Community Patent with its own jurisdictional system the future of EPLA might be somewhat questionable.
Read the <IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"> <a href="./data/030503EPLA.pdf">EPLA Draft Document</a> and the <IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"> <a href="./data/030503SEPC.pdf">Draft Statute of the European Patent Court</a></p>
</td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>New German Draft Bill concerning Design Protection</H3></td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr>
<td width=12% valign="top"><p>2003-04-27</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[DE]</strong>
The Federal Government of Germany has issued a Draft Bill ("Geschmacksmusterreformgesetz") concerning design protection. The main purpose is the implementation of the EU
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<A HREF="./data/981013DIR9871EC.pdf">Directive 98/71/EC</a> of the European Parliament and of the Council of 13 October 1998 on the legal protection of designs. However, somewhat confusingly, the German Government has packed some other stuff completely unrelated to the field of design protection into that bill. In particular, the German Trade Mark Act is to be amended in order to enable the German Patent and Trade Mark Office to refuse trade mark applications if there are serious concerns that the applicantion was made in bad faith.
Read the
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="./data/030411GeschmMRefG.pdf">Geschmacksmusterreformgesetz</a> here. [In German]</p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>British Royal Society publishes IP report</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-04-25</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[UK]</strong>
The British Royal Society has produced a report: "Keeping
science open: the effects of intellectual property policy on the
conduct of science". The report is addressed to universities and
colleges, funding bodies, learned societies, scientists, industry,
the courts as well as to governments from around the world and their
patent offices.
The Royal Society recognises that patents are morally neutral. Like
any other tool they can be used for good or bad. When used properly
they can promote innovation, but if abused can cause some economic
harm.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.royalsoc.ac.uk/files/statfiles/document-221.pdf">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Germany to ratify London Agreement on Patent Translations</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-04-25</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[DE]</strong>
The German Government has published a draft bill concerning the ratification and transposition into national law of the London Agreement.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.bmj.bund.de/ger/themen/urheberrecht_und_patente/10000705/">More ...</a> [In German]</p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Council of European Patent Organisation Fails to Agree New President </H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-03-31</p></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><p><strong>[EPO]</strong>
The Administrative Council of the European Patent Organisation could
not agree a nomination for a President to succeed Ingo Kober at a
meeting on 20 March 2003. The Council has therefore agreed to rerun
the process in June with Member States having until 9 May to nominate
candidates.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.patent.gov.uk/about/notices/president.htm">More ...</a></p>
</td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EPC 2000 Revised European Patent Convention and Implementing Regulations</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-03-28</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[EPC]</strong>
The EPO has published today a nice synoptical edition of the new EPC
2000 Revised European Patent Convention and Implementing Regulations
(which is not yet in force!)
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.epo.co.at/epo/pubs/oj003/03_03/03_ins3.pdf">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>European Community Patent ante portas</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-03-04</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[EU]</strong>
The EU Council has reached agreement on a common political approach
regarding the Community Patent. Discussions went along the
suggestions of the Presidency compromise proposal taking into account
elements from previous debates. The compromise text sets out the
main features for the jurisdictional system for the Community Patent,
the language regimes, costs, the role of national patent offices and
the distribution of fees.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://register.consilium.eu.int/pdf/en/03/st08/st08539en03.pdf">More ...</a> and
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://register.consilium.eu.int/pdf/en/03/st08/st08539-co01en03.pdf">More ...</a>[Corrigendum]</p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>OHIM accepting applications for Community Designs</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-01-09</p></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><p><strong>[OHIM]</strong>
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://oami.eu.int/en/design/faq.htm">More ...</a> [FAQ] and
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://oami.eu.int/en/design/pdf/reg2002_6.pdf">More ...</a> (CDRR] and
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://oami.eu.int/en/design/pdf/Official Regulation21102002.pdf">More ...</a> [CDR-IR] and
<a href="http://oami.eu.int/en/design/pdf/Fees regulation.pdf">More ...</a> [Fees] and
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://oami.eu.int/en/design/pdf/examguidelines en.pdf">More ...</a> [Examination Guidelines]</p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Proposal for a Council Regulation amending Regulation (EC) No 40/94
on the Community trade mark - COM(2002) 767 final of 27.12.2002 </H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2003-01-09</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[EU]</strong>
The EU Commission plans a first major overhaul of the CTMR, considering to abolish the search fror elder registrations.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/eur-lex/en/com/pdf/2002/com2002_0767en01.pdf">More ...</a> and
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/eur-lex/en/com/cnc/2002/com2002_0766en01.pdf">More ...</a> and
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/eur-lex/en/com/cnc/2002/com2002_0754en01.pdf">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EU Commission on planned Community Patent Litigation</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2002-09-12</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[EU]</strong>
The EU Commission has issued a Commission Working Document
COM(2002) 480 final on the planned Community Patent Litigation: A semi-centralised court system.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/eur-lex/en/com/wdc/2002/com2002_0480en01.pdf">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>German Government on "Software Patents"</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2002-06-27</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[DE]</strong>
The German government has issued an official answer to a parlamentarian question on "Software Patents, Competition, Innovation and SMEs"
filed by the CDU/CSU opposition group.
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="./data/1409174.pdf">More ...</a> [In German]</p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Four countries joining EPC</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2002-06-10</p></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><p><strong>[EPC]</strong>
Having ratified the European Patent Convention (EPC), Bulgaria, the Czech Republic, Estonia and Slovakia will become members of the European Patent Organisation (EPO) on 1 July 2002.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.epo.co.at/epo/pubs/oj002/05_02/05_2492.pdf">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Draft for amended EPC Implementation Regulations published</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2002-06-10</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[EPC]</strong>
The European Patent Convention (EPC 1973) was revised at a Diplomatic Conference in Munich by the Act revising the Convention on the Grant of European Patents of 29 November 2000.
As a consequence, it is necessary to overhaul the entire corpus of the Implementing Regulations to the EPC. A <a HREF="./data/EPC-IR-001.pdf">draft</a> has now been published by EPO.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.epo.co.at/news/info/pdf/explanatory_remarks.pdf">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>USA: Decision in "FESTO" case published</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2002-05-28</p></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[USA]</strong>
Supreme Court of the United States in re Festo Corp. vs. Shoketsu Kinzoku Kogyo Kabushiki Co.,Ltd.,et al.:
"[...] Prosecution history estoppel may apply to any claim amendment made to satisfy the Patent Act �s requirements, not just to amendments made to avoid the prior art, but estoppel need not bar suit against every equivalent to the amended claim element. [...]
Prosecution history estoppel does not bar the inventor from asserting infringement against every equivalent to the narrowed element. Though estoppel can bar challenges to a wide range of equivalents, its reach requires an examination of the subject matter surrendered by the narrowing amendment. The Federal Circuit�s complete bar rule is inconsistent with the purpose of applying the estoppel in the first place to hold the inventor to the representations made during the application process and the inferences that may be reasonably drawn from the amendment. [...]"
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ipjur.com/data/festo.pdf">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>MIPEX II completed its work</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2002-05-07</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[OTHER]</strong>
The MIPEX II project completed its work in September 2001 and the final report of the project has now been published on the Internet.
The project delivered a number of useful products including generic software (PaTrAS) to enable electronic filing of patent and
trade mark applications.
The software is now in use at the UK Patent Office for electronic filing of trade mark applications.
Plans are in hand to introduce the software at the Danish Patent and Trademark Office for electronic filing of both patent and
trade mark applications, at the German Patent and Trademark Office for patent applications and at the Swedish Patent and
Registration Office for trade mark electronic filing. A change management board has been set up to oversee future development
of the PaTrAS software, supported by a technical board to manage future software changes.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.patent.gov.uk/about/projects/mipex/final.pdf">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Intellectual Property statistics for 1999</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P>2002-05-03</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><p><strong>[WIPO]</strong>
Intellectual Property statistics for 1999 have recently been published
by WIPO.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="
     http://www.wipo.int/ipstats/en/publications/b/1999/pubb-i.htm">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EU Green Paper on alternative dispute resolution</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2002-04-29</P></td>
<td width=7% valign="top"><p>PDF</P></td>
<td valign="top"><p><strong>[EU]</strong>
The Commission of the EU has recently published a "Green Paper on alternative dispute resolution in civil and commercial law" (COM(2002) 196 final). The purpose of this Green Paper is to initiate a broad-based consultation of those involved in a certain number of legal issues which have been raised as regards alternative dispute resolution in civil and commercial law. Answers to specific questions and general comments can be sent, preferably by 15 October 2002. The Commission intends to organise a public hearing on the subject early in 2003.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/eur-lex/en/com/gpr/2002/com2002_0196en01.pdf">More ...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EU on Consultations on the impact of the Community utility model law</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2002-03-25</P></td>
<td width=7% valign="top"><p>PDF</P></td>
<td valign="top"><p><strong>[EU]</strong>
On 26 July 2001, the Commission published a staff working paper entitled "Consultations on the impact of the Community utility model in order to update the Green Paper on the Protection of Utility  Models in the Single Market" (SEC(2001)1307). Now results have been summarized and published.
Three-quarters of the contributors state their opposition to a Community utility model. The reasons are many and varied, including the risk of restricting competition and adversely affecting the competitiveness of European companies, less legal certainty, unsatisfactory criteria (level of inventiveness, etc.). Moreover, it is felt that the utility model would respond to a need for local, or even national protection, but would not be justified at Community level.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/comm/internal_market/en/indprop/utilreport_en.pdf">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>SPLT</H3></td>
</tr>
<tr>
<td width=12% valign="top"><p>2002-03-20</p></td>
<td width=7% valign="top"><p>PDF</P></td>
<td valign="top"><p><strong>[WO]</strong>
The WIPO Standing Committee on the Law of Patents will have its next meeting (Seventh Session) for discussing the SPLT draft in Geneva on May 06 to May 10, 2002.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.wipo.org/eng/document/scp_ce/index_7.htm">More ...</a></p>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>French Government on "Software Patents"</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2002-03-12</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[FR]</strong>
Appearently, some of the top policymakers in the French political scene take positions sceptical to the current development of the patent system.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.liberation.fr/quotidien/semaine/020312-040031013ECON.html">More ...</a> [In French]</P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>German Patent and Trade Mark Office statistics 2001</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2002-03-12</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[DE]</strong>
The German Patent and Trade Mark Office has reported that in 2001 127,428 patent applications having effect for Germany have been filed - a 15 percent plus compared with 2000. 58,967 patent applications have been filed through the national route directly with the DE-PTO.
However, only 67,361 trade mark applications have been filed in 2001 - this is a minus of 20,000 compared with 2000. According to the DE-PTO, one reason for this decline might have been the collapse of the .com hype.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.dpma.de/infos/pressedienst/pm020312a.html">More ...</a> [In German]</P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EU proposal on the protection by patents of computer-implemented inventions</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P>2002-02-20</P></td>
<td width=7% valign="top"><P>PDF<BR>HTML</P></td>
<td valign="top"><P><strong>[EU]</strong>
The European Commission has presented a <a href="http://europa.eu.int/comm/internal_market/en/indprop/com02-92en.pdf">proposal for a Directive on the protection by patents of computer-implemented inventions
</a>. The proposed Directive would harmonise the way in which national patent laws deal with inventions using software.
Such inventions can already be patented by applying to either the European Patent Office (EPO) or the national patent offices of the Member States, but the detailed conditions for patentability may vary. A significant barrier to trade in patented products within the Internal Market exists as long as certain inventions can be protected by patent in some Member States but not others.
The proposed Directive will be submitted to the EU�s Council of Ministers and the European Parliament for adoption under the so-called �co-decision� procedure.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/comm/internal_market/en/indprop/02-277.htm">More ...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EU Commission Regulation Implementing the Council
Regulation on the Community Design</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2002-02-13</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><P><strong>[EU]</strong>
Following the adoption of the Community Design Regulation in December 2001, the Commission has presented to the Member States the draft proposal for a "Commission Regulation Implementing the Council
Regulation on the Community Design" for discussion under the "Comitology Procedure". The purpose of this Regulation is to provide the Office for the Harmonisation in the Internal Market with the legal and administrative tools necessary for the registration of Community designs.
The Commission would expect to have this Regulation adopted by the Summer 2002. This should permit the Office to make the necessary preparations to accept applications for registered Community designs from next year.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/comm/internal_market/en/indprop/draft_des_en.pdf">More ...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>German Law on Employee Inventions amended</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2002-01-28</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[DE]</strong>
Some plans of the German government to amend the German Law on Employee Inventions have now been implemented. As of February 07, 2002, inventions made by professors, lecturers and scientific assistants, in their capacity as such, at universities and higher schools of science will be governed by the new law. They will no longer be free inventions. A general reform on the
German Law on Employee Inventions is still under discussion. Probably the reform proposal will not be enacted before next General Elections for the German Parliament due in September 2002.
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="./data/bgbl102004s414.pdf">Publication of the Act in the Official Gazette ("Bundesgesetzblatt") [In German]</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>WIPO Madrid Database now  extended to include all international registrations</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2002-01-28</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[WIPO]</strong>
According to a notice given by WIPO, the coverage of Madrid Database has now been extended to include all international registrations that are currently in force or have expired within the past six months.
Moreover, the Hague Database includes bibliographical data and, as far as international deposits governed exclusively or partly by the 1960 Act of the Hague Agreement are concerned, reproductions of industrial designs relating to international deposits that have been recorded in the International Register and published in the International Designs Bulletin as of issue No. 1/1999.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://ipdl.wipo.int?wipo_content_frame=/en/search/search.shtml">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>A single Community system for the protection of designs</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-12-12</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[EU]</strong>
The European Commission has welcomed the adoption by the EU's Council
of Ministers on 12th December of a Regulation introducing a single
Community system for the protection of designs. The
<a href="http://europa.eu.int/eur-lex/en/dat/2002/l_003/l_00320020105en00010024.pdf">Regulation</a> sets
up a simple and inexpensive procedure for registering designs with
the European Union's Office for Harmonisation in the Internal Market
in Alicante. Unregistered designs will also be protected. Companies
will still be able to choose to register designs under national law,
as national design protection, as harmonised by the design protection
Directive (98/71/EC),will continue to exist in parallel with
Community design protection.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/rapid/start/cgi/guesten.ksh?p_action.gettxt=gt&doc=IP/01/1803|0|RAPID&lg=EN&display=">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>German BGH Case Law X ZB 16/00 - "Suche fehlerhafter Zeichenketten"</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-12-03</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><P><strong>[DE]</strong>
On October 17, 2001 the 10th Senate of the Federal High Court (Bundesgerichtshof, BGH) has taken an important landmark decision (X ZB 16/00 - "Suche fehlerhafter Zeichenketten") concerning to patents on computer-implemented inventions. In particular, the court had to decide whether or not to allow claims directed to a "digital storage medium, in particular floppy disc, exhibiting electronically readable control signals which can be read out, the control signals being arranged for interaction with a programmable computer system" and to a "computer program product" as well as to a "computer program".
The BGH argues that the proposed digital storage medium is a subject-matter means ("gegenstaendliches Mittel") for performing the method proposed within the patent application; the intentional usage ("bestimmungsgemaesser Einsatz") thereof leads to the desired result. Moreover, the BGH says that if a teaching expressed as a computer program is deemed to be eligible as subject-matter of a patent claim, the "shaping instructions" ("praegenden Anweisungen") of the teaching as claimed must serve to solve a particular technical problem.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.topica.com/lists/intprop-l/read/message.html?mid=801230481&sort=d&start=995">More...</a> and
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ipjur.com/data/xzb1600.pdf">More...</a>
[In German]</P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>German Government proposes changes to the German Act on Employees Inventions</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-11-14</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><P><strong>[DE]</strong>
The German Government is about to make drastic changes to the German Act on Employees Inventions. In particular, the complex rules requesting the employee to report an invention and subsequently setting a term for the
employer to claim the invention are to be abandoned in favour of a more
streamlined process transferring the rights to the invention to the
employer per default. Moreover, the right of the employee to receive additional remuneration depending of the economic effects of the invention is to be substantially simplified.
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ipjur.com/data/arbnerfg.pdf">More...</a>
[In German]</P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EPO to rationalise its procedures for international preliminary examination</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-11-14</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[EPO]</strong>
The unremitting increase in its PCT search and examination workload has prompted the EPO to rationalise its procedures for international preliminary examination under Chapter II PCT with effect from 3 January 2002.
Its objective is to avoid using examining capacity unnecessarily on cases where applicants are more interested in prolonging the international phase than in the outcome of preliminary examination, and to save it for core work in search and European substantive examination.
To that end, procedures will be streamlined and international preliminary examination focused on its core elements.
A central element in the streamlined procedure is that the result of the international search will serve as the basis for international preliminary examination, without involving the substantive examiner again (procedure without detailed substantive examination).
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.epo.co.at/epo/president/e/2001_11_13_e.htm">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Eighth edition of the Nice classification</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-11-02</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[WIPO]</strong>
The eighth edition of the Nice classification is available. In particular, very imporant changes affecting Intl. Class 42 are worth to be known.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://classifications.wipo.int/fulltext/nice8/enmain.htm">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>German Bill concerning
clarification of cost regulations in the field of intellectual
property</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-11-02</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><P><strong>[DE]</strong>
On October 16, 2001 the Committee on Legal Affairs
("Rechtsausschuss") of the German parliament ("Bundestag") has
finalized a report on a "Bill concerning
clarification of cost regulations in the field of intellectual
property" ("Entwurf eines Gesetzes zur Bereinigung von
Kostenregelungen auf dem Gebiet des Geistigen Eigentums") which has
been published as a printed matter of the German Bundestag under No.
14/7140. The main issue was to convert Official fees of the German Patent and
Trade Mark Office as well as of the Federal Patent Court from
Deutsche Mark to Euro. Moreover, a lot of changes are to be made with regard
to terms and conditions of payment of Official fees.
However, deeply buried within this formal stuff there are a few
surprising amendments. For example, if the bill finally has passed
the parliament, the patent opposition proceedings usually held before
the Opposition Divisions of the German Patent and Trade Mark Office
will be suspended for three years due to overload and backlog
problems; instead, the Federal Patent Court will do this work. There
will be no longer any option to lodge an appeal against such decisions of first instance in view of insufficient factual
findings; only "revision" to the Federal High Court in Karlsruhe
(Bundesgerichtshof) will be allowable under certain circumstances.
Moreover, prior art searches not bound to a patent application will
be suspended for five years.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ipjur.com/data/1407140.pdf">More...</a> [In German]</P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>ECJ has dismissed action seeking annulment of the Community directive on the legal protection of biotechnological inventions</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-10-09</P></td>
<td width=7% valign="top"><P>HTML<BR>PDF</P></td>
<td valign="top"><P><strong>[ECJ]</strong>
The Court of Justice has dismissed the action brought by the
Netherlands seeking annulment of the Community directive on the legal
protection of biotechnological inventions. The Court of Justice takes the view that the Community directive
frames patent law in stringent enough terms to ensure that the human
body is unavailable for patenting and inalienable and to safeguard
human dignity.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/rapid/start/cgi/guesten.ksh?p_action.getfile=gf&doc=CJE/01/48|0|RAPID&lg=EN&type=PDF">More...</a>. The full text can be
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/jurisp/cgi-bin/form.pl?lang=en">accessed</a> using the case number C-377/98</P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EU Innovation Scoreboard</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-10-01</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[EU]</strong>
The Innovation Scoreboard is an assessment of innovation performance in the individual Member States of the European Union.
To measure innovation performance a set of 17 qualitative indicators are used, based on available statistics covering human resources, knowledge creation, the application of knowledge and innovation finance. The scoreboard is a "benchmarking" tool highlighting both strengths and weaknesses.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.cordis.lu/innovation-smes/scoreboard/scoreboard_2001.htm">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Four new FICPI resolutions</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-10-01</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[OTHER]</strong>
The Executive Committee of FICPI met at Goodwood, England, from 2 to 7 September. The Committee adopted the following 4 Resolutions:
(a) Implementation of TRIPS in Relation to Pharmaceuticals, (b) Prior Art Effect of Prior Applications, (c) General Principles of PCT Reform, and (d) European Patent Translations.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ficpi.org/ficpi/newsletters/49/resolGWengl.html">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>WIPO recommendations dealing with the misuse of certain names and
identifiers in the Internet domain name system</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-09-03</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[WIPO]</strong>
At the close of an international consultation process spanning the
past year, the World Intellectual Property Organization (WIPO) has
released its Final Report containing
recommendations dealing with the misuse of certain names and
identifiers in the Internet domain name system (DNS). WIPO finds that
the international legal framework for the protection in the domain
name system (DNS) of the naming systems examined is not yet fully
developed. The Report calls upon the international community to
decide whether to address these insufficiencies and establish a
complete legal basis for dealing with offensive online practices in
connection with the naming systems concerned.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://wipo2.wipo.int/process2/report/index.html">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Problems with epoline.org</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-08-31</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[epi]</strong>
Mr. Dieter K Speiser, Chairman of the Online Communications Committee of the <a href="http://www.patentepi.com/">epi</a>, has issued a notice discouraging from use of the <a href="http://www.epoline.org/">epoline</a> on-line filing faclilities of the EPO.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.patentepi.com/english/200/220/">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Draft modifications of the Administrative Instructions under the Patent Cooperation Treaty (PCT)</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-08-31</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[WIPO]</strong>
Draft modifications of the Administrative Instructions under the Patent Cooperation Treaty (PCT) designed to implement procedures for the
electronic filing and processing of international applications under the PCT, including the storage and records management of such
applications, are close to finalization. The necessary legal framework is proposed to be included in the Administrative Instructions as new
Part 7, and the necessary technical standard as new Annex F.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://pcteasy.wipo.int/efiling_standards/EFPage.htm">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EPO has announced a new research programme</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-08-11</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><P><strong>[EPO]</strong>
The EPO has announced a new research programme in order to help the process of making capacity planning decisions. Researchers or teams of researchers, from Europe or elsewhere, are invited to submit proposals under five modules, namely (a) Methods to survey the intentions of applicants for future numbers of patent filings, (b) Methods to forecast numbers of patent filings at the firm level, (c) Methods to forecast numbers of patent filings at the industrial and national levels, (d) Patent transfer models (to forecast numbers of filings claiming priority of earlier filings), and  (e) Methods to forecast aggregated filing data by time series modelling. The time horizon for the programme is currently set to support projects of up to three years in duration and a total sum of up to EUR 150 000 is available for the programme.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.epo.co.at/news/pressrel/pdf/resblurb2.pdf">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EU on consultations on the impact of the Community utility model </H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-08-07</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><P><strong>[EU]</strong>
A CEC Staff Working Paper No. SEC(2001) 1307 "Consultations on the impact of the Community utility model in order to update the Green Paper on the Protection of Utility Models in the Single Market (COM(95)370 final)" has been published on the CEC website. In order to give appropriate effect to the conclusions of the European Council, the Commission has suggested updating the information it obtained from the interested parties on the possible creation of a Community utility model. On 31 March 2001 the Internal Market Council welcomed the Commission�s intention of quickly organising consultations with a view to drawing up a basic document taking a closer look at the possible impact of a Community utility model in legal, practical and economic terms. Replies to the questions raised in the document must be sent by November 30, 2001 to the European Commission�s Directorate-General for the Internal Market, either by writing to the following address: European Commission, DG Internal Market (MARKT/E/2), rue de la Loi, 200 (C100 5/109), B-1049 Bruxelles, or by e-mail.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/comm/internal_market/en/indprop/consultation_en.pdf">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>CEC online forum on on patenting of software-related inventions</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-08-03</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[EU]</strong>
The Commission of the European Comminities (CEC) has created
an
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.forum.europa.eu.int/Public/irc/markt/softpat/newsgroups?n=forum">ONLINE-FORUM</a> on patenting of software-related inventions.

<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://europa.eu.int/comm/internal_market/en/indprop/softforum.htm">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>CEC awarded contract for reading and summarizing the results of SoftPat consultation</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-08-03</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><P><strong>[EU]</strong>
The Commission of the European Communities (CEC) had awarded to PbT Consultants, UK, a contract for reading and summarizing the
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/comm/internal_market/en/indprop/softreplies.htm">results</a> of the
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12"><a href="http://europa.eu.int/comm/internal_market/en/indprop/softpaten.htm">EU consultation on the patentability of computer implemented inventions</a>. The final report has now been published
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/comm/internal_market/en/indprop/softpatanalyse.htm">on-line</a>.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://europa.eu.int/comm/internal_market/en/indprop/softanalyse.pdf">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>UK-PTO on "Meeting the Future: Consultation on Proposed Changes in Patent Practice and Procedure"</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-08-02</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><P><strong>[UKPTO]</strong>
The United Kingdom Patent Office has published a new consultation paper "Meeting the Future: Consultation on Proposed Changes in Patent Practice and Procedure". Responses are sought until November 01, 2001. Topics are, inter alia, to restrict applicants to a limited number of opportunities to amend the papers of application, to allow examiners to issue an abbreviated examination report, putting a duty on applicants to file search and examination reports on corresponding cases, to cease amendment of the description, and to establish a Code of Practice between Examiners and Agents.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.patent.gov.uk/about/consultations/future/future.pdf">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Confidentiality of e-mails</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-08-02</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[OTHER]</strong>
The United Kingdom Patent Office's current view, pending and subject to clarification by the courts,
is that the mere act of sending an email to an intended recipient or recipients does not amount to public disclosure of the content of that email. This is the essential result of an enquiry recently directed to the UL-PTO by <a href="http://www.mayallj.freeserve.co.uk/">Mr. John Mayall</a>.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.mayallj.freeserve.co.uk/novelty.htm">More...</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>German government plans to amend the German Law on Employee Inventions</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-07-26</P></td>
<td width=7% valign="top"><P>HTML<BR>PDF</P></td>
<td valign="top"><P><strong>[BT]</strong>
The German government plans to amend the German Law on Employee Inventions. Up to now, in accordance with the provisions provided in Sect. 42 of the law and
in derogation from Sections 40 and 41 thereof, inventions made by professors, lecturers and scientific assistants, in their capacity as such, at
 universities and higher schools of science are free inventions.
 The German Government wants to revoke this "Hochschullehrerprivileg", effectively forcing professors and related staff to report their inventions
 to the university which can decide to claim them. The overall intention is to provide additional financial resources for universities coming in from patent licenses. However, the German Government replaces the pending Parliamentary Bill initiated by German Bundesrat (the chamber of the German "L&auml;nder" ("States") by some <a href="./data/1405975.pdf">different draft [In German]</a> attempting to regulate more carefully a balance between a duty to report inventions before publication and constitutional freedoms of university research and teaching.</a>
 <a href="./data/1407565.pdf">Parliamentary Bill [In German]...</a> and <a href="./data/BR74000.pdf">Bundesratsdrucksache ... [In German]</a></P>
</td>
</tr
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>Berlin: expert workshop on patents on computer-implemented inventions</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-07-10</P></td>
<td width=7% valign="top"><P>HTML</P></td>
<td valign="top"><P><strong>[BMWi]</strong>
On July 10, 2001 an expert workshop was held by the German Federal
Ministry for Economics and Technology in Berlin. About approximately
some 60+ attendants listened the presentation of preliminary results
of a study jointly conducted by Fraunhofer Institute for Research on
Systems and Innovation (ISI), Karlsruhe, and by Max Planck Institute
for Foreign and International Patent, Copyright and Competition Law,
Munich.
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="./BMWI001.php3">More ...</a> and <a href="http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2&tsid=212&tdid=1003&page=0">More ... [In German]</a></P>
</td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>France joining "London Protocol"</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-06-29</P></td>
<td width=7% valign="top"><P>HTML<BR>PDF</P></td>
<td valign="top"><P><strong>[EPO]</strong>
France has joined Germany, the United Kingdom and seven other countries in approving the <a href="http://www.ige.ch/E/jurinfo/pdf/epc65_e.pdf">London Protocol</a>, thereby dropping the requirement for European patent applications filed in France to be translated into French.
The London Protocol is an <a href="http://www.ige.ch/E/jurinfo/j14104.htm">optional agreement</a> by which the Member States can totally or partly waive the translation of European patents. A translation can be waived, if one of the 3 official languages of the EPO is also the official language of the state in which the patent is to be  granted. In all other cases a translation of the description can be waived, if the patent is available in an official EPO language selected by the granting state. The agreement will now come into effect on the first day of the fourth month after it has been ratified by 8 states. It is not known when this will happen. Read
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href= "http://www.latribune.fr/Archives/ArchivesProxy.nsf/SearchSimple/400AF8E42B257C51C1256A7E001325DA?OpenDocument">more</a>, <a href="http://www.cncpi.fr/htdocs/comm_presse.html">more</a>, <a href="http://www.industrie.gouv.fr/cgi-bin/industrie/frame423.pl?bandeau=/biblioth/bb_bibl.htm&gauche=/biblioth/docu/rapports/missions/lb_miss.htm&droite=/biblioth/docu/rapports/missions/sb_miss.htm">more, </a>and <a href= "http://www.industrie.gouv.fr/cgi-bin/industrie/sommaire/comm/com_contenu.pl?COM_ID=460">more ...</a></p></td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>New study: "Patent protection of computer programmes"</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-06-15</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><P><strong>[EU]</strong>
A new study on
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="ftp://ftp.ipr-helpdesk.org/softstudy.pdf">Patent protection of computer programmes</a> created under Contract no. INNO-99-04 and submitted to European Commission, Directorate-General Enterprise, by <a href="http://www.cops.ac.uk/iss/people.html#puay">Dr. Puay Tang</a>, SPRU, University of Sussex, <A href="http://www.shef.ac.uk/uni/academic/I-M/law/adams.html">Prof. John Adams</a>, University of Sheffield, <a href="http://www.internetstudies.org/members/danipar.html">Dr. Daniel Par�</a>, SPRU, University of Sussex, has been published as document ECSC-EC-EAEC, Brussels-Luxembourg, 2001, on the Internet by the EU IPR Help Desk. <a href= "http://www.ipr-helpdesk.org/t_en/home.asp">More ...</a></P></td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>European Patents Litigation Protocol [EPLP]</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-06-12</P></td>
<td width=7% valign="top"><P>HTML<BR>PDF</P></td>
<td valign="top"><P><strong>[EU] </strong>
European Patents Litigation Protocol [EPLP]: The summary of the first proposal for an EPLP was discussed at the sub-group of the Working Party on Litigation at its third meeting
in The Hague on April 4-6, 2001. Based on the results of this conference, the first proposal has been revised and is now available as
paper intitulated
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ige.ch/E/jurinfo/pdf/j14105_prop.pdf">"Second proposal for an EPLP"</a> for discussion among the interested circles. <a href= "http://www.ige.ch/E/jurinfo/j14105.htm">More ...</a></P></td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td>&nbsp;</td></tr>
<tr>
<td>&nbsp;</td><td>&nbsp;</td>
<td><H3>EPC sub-group of the Working Party on Litigation</H3></td>
</tr>
<tr>
<td width=12% valign="top"><P> 2001-05-27</P></td>
<td width=7% valign="top"><P>PDF</P></td>
<td valign="top"><P><strong>[EPO] </strong>Currently negotiations are underway to create a Protocol governing centralized litigation on the basis of European patents. The Swiss
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ige.ch/">IGE</a>
has published a
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href="http://www.ige.ch/E/jurinfo/j14103.htm">Status Report</a>
of the sub-group of the Working Party on Litigation convened for a third time at The Hague on April 4-6, 2001: "The summary of the first proposal for an EPLP was discussed. The Member States interested in a unifed patent court reached the conclusion that a European patent court of first and second instance should be created, and the well tried exisiting courts should be integrated into the system as regional chambers. The pending proceedings before the European patent court shall be distributed to the various regional chambers according to procedural rules. The delegations were in agreement that a decision by the European patent court should take effect erga omnes in all countries acceding to the EPLP. The question of language and the composition of the court is still open. However, the group agreed that an appropriate procedure with a simplified language rule should apply for minor local litigation. The question of whether patent attorneys can appear as representatives before the European patent court must be further explored. There will be an expert meeting in mid-May to discuss the procedural law of the future court. The next meeting of the subgroup has been set for Juli 2001."
<IMG SRC="./arrow5.gif" ALT="EXTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<a href= "http://www.ige.ch/E/jurinfo/pdf/j14103_prop.pdf">More ...</a></P></td>
</tr>
<!-- ITEM_SEPARATOR -->
<tr><td colspan=3><hr size="1"></td></tr>
</P>
</table>


<br>&nbsp;<br>

<H3>INTELLECTUAL PROPERTY NOTES</H3>

<table><P>
<tr>
<td width=10% valign="top"><P>No.</P></td>
<td width=15% valign="top"><P> DATE</P></td>
<td valign="top" align="center"><P> TITLE</P></td>
</tr>

<tr><td colspan=3><hr size="1"></td></tr>

<tr>
<td width=10% valign="top"><P>
<A HREF="./ipn/2001-001.pdf">01/2001</A></P></td>
<td width=15% valign="top"><P> 2001-03-10</P></td>
<td valign="top"><P> Observations on Replies to the EU Consultation Paper on the Patentability of Computer-Implemented Inventions</P></td>
</tr>

<tr><td colspan=3><hr size="1"></td></tr>
</table>

<H3>HOT SPOTS ON THE NET [OFFSITE]</h3>
<P>

<table>
<tr width="100%">
 <td valign="TOP">
<P>
<UL>
<LI><A HREF="http://europa.eu.int/geninfo/whatsnew.htm">What's new on the EU Website?</a></li>
<LI><A HREF="http://oami.eu.int/en/news.htm">What's new on the OHIM Website?</a></li>
<li><A HREF="http://www.wipo.org/news/en/2003/index.htm">What's new on the WIPO Website?</a></li>
<li><A HREF="http://www.dkpto.dk/nyheder/">What's new on the DK-PTO Website? [In Danish]</a></li>
</UL>
</P>
</td>
 <td width="50%" valign="TOP"><P>
 <UL>
 <li><A HREF="http://www.epo.co.at/updates.htm">What's new on the EPO Website?</a></li>
<li><A HREF="http://www.dpma.de/neu/neu.html">What's new on the DE-PTO Website? [In German]</a></li>
<li><A HREF="http://www.jpo.go.jp/rireki_e/index.htm">What's new on the JP-PTO Website? </a></li>
<li><A HREF="http://www.ige.ch/E/news/n1.htm">What's new on the CH-IGE Website? </a></li>
</ul>
 </P></td>
</tr>
</table>

<br>&nbsp;<br>

<H3>INTPROP-L MAILING LIST</h3>

<P>A European Intellectual Property Law discussion mailing list INTPROP-L is available. INTPROP-L is an unmoderated list addressing IP professionals. ...</p>

<p>For details, please click <A href="./intprop-l.php3">here.</a></p>


<HR SIZE=1>

<table WIDTH="100%" border="0" cellspacing="0" cellpadding="0" BGCOLOR="#FFFFFF">
<tr><td colspan=2><p>Please read the
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<A HREF="./disclaimer.php3">Disclaimer &amp; About This Website (Pflichtangaben gem&auml;ss TDG)</a> section.</p>
<P>Feel free to contact PA Axel H Horns via e-mail
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<A href="mailto:horns@ipjur.com">horns@ipjur.com</a>. BEWARE: DO NOT SEND CONFIDENTIAL INFORMATION UNENCRYPTED VIA E-MAIL. USE OF ENCRYPTION SOFTWARE IS HIGHLY RECOMMENDED. PA AXEL H HORNS IS PROVIDING SUPPORT FOR ENCRYPTED E-MAIL MESSAGES USING PGP OR PGP COMPATIBLE FORMATS. THE PGP PUBLIC KEY FOR PA AXEL H HORNS IS AVAILABLE
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<A HREF="./pubkey.php3">HERE</a>. THE GnuPG PUBLIC KEY FOR PA AXEL H HORNS IS AVAILABLE
<IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<A HREF="./gnupg_pubkey.php3">HERE</a>.</p>

<P><IMG SRC="./arrow6.gif" ALT="INTERNAL LINK" BORDER=0 hspace="3" vspace="0" HEIGHT="12" WIDTH="12">
<A HREF="./ahh.php3">Dipl.-Phys. Axel H Horns</a> is Patentanwalt (German Patent Attorney),
European Patent Attorney as well as European Trade Mark Attorney. In particular, he is Member of:</P>

</td>
</tr>
<tr>
 <td><a href="http://www.scl.org"><img src="./logo-new-296x37.gif" width="296" height="37" border="0"
 alt="Click here to visit the SCL Online web site" align="center"></a>
 </td>
 <td><a href="http://www.cla.org"><img src="./claclr2.jpg" width="75" height="75" border="0"
 alt="Click here to visit the CLA Online web site" align="center"></a>
 </td>
</tr>
<tr><td><p>&nbsp;</p></td><td><p>&nbsp;</p></td></tr>
<tr>
 <td><a href="http://www.ficpi.org"><img src="./ficpi_logo.gif" width="50" height="50" border="0"
 alt="Click here to visit the FICPI web site"></a>
 </td>
 <td halign="center"><a href="http://www.vpp-patent.de/"><H1>VPP</H1></a></td>
 </tr>

</td>
</tr>
</table>

</table>
</font>
</td>
</tr>
</TABLE>
</BODY>
</HTML>