<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>IT2006</title>
</head>
<body>
<img src="bmbf.gif">

<font face=arial size=2>

<A NAME="top">&nbsp</a>
<H3 align="center">IT-Forschung 2006</H3>
<H4 align="center">F�rderprogramm Informations-
und Kommunikationstechnik</H4>


<B>1. Einleitung</B>
<P>
Investitionen in Bildung und Forschung sind Grundlage f�r Wachstum, Besch�ftigung und gesellschaftlichen 
Fortschritt. Besonders in zentralen Innovationsfeldern des 21. Jahrhunderts, wie der Informations- und 
Kommunikationstechnik, die hohe Markt- und Besch�ftigungspotenziale aufweisen, zunehmend alle 
gesellschaftlichen Bereiche durchdringen und den wirtschaftlichen und gesellschaftlichen Strukturwandel 
vorantreiben, ist Forschung Voraussetzung f�r die internationale Wettbewerbsf�higkeit Deutschlands.
<P>
Die Bundesregierung hat mit dem Aktionsprogramm "Innovation und Arbeitspl�tze in der Informationsgesellschaft 
des 21. Jahrhunderts" den Grundstein f�r Deutschlands Weg in die Wissensgesellschaft gelegt. Dabei wurde der 
Informations- und Kommunikationstechnik in Bildung und Forschung Priorit�t einger�umt.
<P>
Mit dem Handlungskonzept "Anschluss statt Ausschluss - IT in der Bildung" hat das Bundesministerium f�r 
Bildung und Forschung im Jahr 2000 die notwendigen bildungspolitischen Schwerpunkte gesetzt. Mit dem 
vorliegenden F�rderprogramm "IT-Forschung 2006" stellt das Bundesministerium f�r Bildung und Forschung 
die programmatischen Weichen f�r die Forschungf�rderung im Bereich der Informations- und 
Kommunikationstechnik f�r den Zeitraum 2002-2006.
<P>
<B>Ziele von IT-Forschung 2006 sind:</B>
<UL>
<LI>Die Qualit�t von Wissenschaft, Forschung und technologischer Entwicklung zu st�rken und die Rolle der deutschen IT-Forschung als internationaler Partner und Wettbewerber auszubauen.
<LI>Grundlagen f�r den Erhalt und den Ausbau von Arbeitspl�tzen zu schaffen.
</UL>
Dabei kommt es besonders darauf an
<UL>
<LI>die bedarfsorientierte Grundlagenforschung in Erg�nzung zur F�rderung durch die Deutsche Forschungsgemeinschaft auszubauen,
<LI>Akteure aus Unternehmen und �ffentlichen Forschungseinrichtungen zusammenzuf�hren, um Ressourcen und Kompetenzen zu b�ndeln,
<LI>Forschungsergebnisse schnellstm�glich einer wirtschaftlichen Verwertung zuzuf�hren,
<LI>vorrangig in Zukunftsfelder und Zukunftsm�rkte zu investieren und dabei solchen innovativen Forschungsthemen Priorit�t einzur�umen, die gute Chancen haben, neue hochwertige Arbeitspl�tze zu schaffen sowie zu Firmengr�ndungen zu f�hren,
<LI>ein Innovationsklima zu schaffen, das Forscherinnen und Forscher in Deutschland h�lt bzw. nach Deutschland zieht.
</UL>
Die Wirkungen von Forschung im IT-Bereich gehen jedoch weit �ber die technischen und �konomischen Aspekte 
hinaus. Informations- und Kommunikationstechnologien ver�ndern unsere Gesellschaft, die Art und Weise, wie 
wir leben, wie wir lernen, arbeiten und wie wir unsere Freizeit gestalten. Sie schaffen neue 
gesellschaftliche Gestaltungsspielr�ume, k�nnen z.B. Motor sein f�r die Verbesserung der Qualit�t 
unseres Bildungs- und Gesundheitssystems, f�r eine leistungsf�higere Wissenschaft, aber auch f�r eine 
effizientere und transparentere �ffentliche Verwaltung. Beispiele sind:
<UL>
<LI>Hochaufl�sende Bewegtbild-�bertragungen werden schon heute f�r die Mediziner-Ausbildung, aber auch f�r die medizinische Praxis entwickelt und erprobt.
<LI>Wissenschaft lebt von Information und Kommunikation. Schneller und gezielter Zugang zu Fachinformationen oder Hochgeschwindigkeitsdatennetzen zum verteilten Rechnen (Grid) und Arbeiten in virtuellen Laboratorien er�ffnen Wissenschaft und Forschung, aber auch der Umsetzung von Forschungsergebnissen neue Perspektiven.
<LI>Das Internet erm�glicht die zeitnahe Bereitstellung �ffentlich verf�gbarer Information und die Vereinfachung der Kommunikation von B�rgerinnen und B�rgern und Unternehmen z.B. mit der Verwaltung.
</UL>
Wir wissen wenig �ber die Informationsgesellschaft des 21. Jahrhunderts, aber feststeht, dass ohne die 
Entwicklung innovativer neuer Produkte und Dienstleistungen durch qualifizierte Menschen Deutschland nicht 
nur seine Wettbewerbsf�higkeit, sondern auch seine gesellschaftliche Gestaltungsf�higkeit verliert. 
Forschung und Bildung sind die Grundvoraussetzung f�r diese Gestaltungsf�higkeit. Notwendig sind 
intelligente, bedarfsgerechte Verwertungsstrategien der Forschungsergebnisse. Gleichzeitig stellt sich 
heute vor allem die Frage der Qualifizierung der Menschen, die in Beruf und Alltag mit den neuen 
Technologien und ihren Anwendungen umgehen. Dabei geht es um die Qualifizierung von jungen Studentinnen 
und Studenten sowie von jungen Forscherinnen und Forschern durch die fr�hzeitige Einbindung in 
Forschungsprojekte, aber auch um die Vermittlung von Grundkenntnissen �ber die neuen Technologien und 
ihre multimedialen Anwendungen in Schule und Weiterbildung, sowie um die durchgehende Modernisierung der 
dualen Ausbildung.
<P>
"IT-Forschung 2006" und das Handlungskonzept "Anschluss statt Ausschluss - IT in der Bildung" erg�nzen 
sich daher nicht nur, sie bedingen einander. Besonders deutlich wurde diese starke Wechselwirkung 
angesichts des seit Mitte der 90er Jahre bestehenden Mangels an IT-Fachkr�ften.
<P>
Der Mangel an IT-Fachkr�ften und insbesondere an Informatikerinnen und Informatikern mit Hochschulabschluss 
hatte sich zu einer der wesentlichen Wachstumsbremsen bei der Entwicklung, aber vor allem auch bei der 
Nutzung von neuen Informations- und Kommunikationstechniken in Deutschland erwiesen. Nur durch gemeinsames 
Vorgehen von Bundesregierung, Wirtschaft und Gewerkschaften im B�ndnis f�r Arbeit und im Rahmen des 
"Sofortprogramms zur Deckung des IT-Fachkr�ftebedarfs in Deutschland" konnte dies schrittweise �berwunden 
werden.
<P>
Fortschritte in der Informations- und Kommunikationstechnik sind ihrerseits wiederum abh�ngig von 
wissenschaftlichen, technischen und wirtschaftlichen Entwicklungen sowie gesellschaftlichen Prozessen. 
Deutlich wird dies z.B. bei der Mobilkommunikation:
<UL>
<LI>Hochleistungsf�hige mobile IT-Systeme verlangen auch leistungsstarke zuverl�ssige portable Energieversorgung. Solche kompakten, portablen Energiequellen zu entwickeln ist selbst nicht Gegenstand des Programms "IT-Forschung 2006", obgleich die Mobilkommunikation ganz erheblich von dieser Schl�sseltechnologie abh�ngen wird.
<LI>Regulierung bzw. Deregulierung bestimmen ganz wesentlich das Marktgeschehen, k�nnen Innovationen hemmen, aber auch beschleunigen. So war der Boom der Mobilkommunikation in den 90er Jahren mit der Deregulierung der Telekommunikationsm�rkte in Europa aufs Engste verkn�pft.
</UL>
Die digitale Revolution ist bei weitem nicht abgeschlossen. Dies bietet f�r Deutschland erhebliche Chancen, 
seine Kompetenzen auszubauen und M�rkte zu gewinnen. Innovationen aus den Informations- und 
Kommunikationstechniken schaffen neue M�rkte, stimulieren aber auch die "Old Economy" nachhaltig.
<P>
Im Rahmen der Initiative "eEurope" hat sich die Europ�ische Union das Ziel gesetzt, dass Europa die 
wettbewerbsst�rkste und dynamischste Informationswirtschaft der Welt wird und die USA im IT-Bereich 
�berholt. Dazu will dieses Programm einen Beitrag leisten.

<P>
<a href="#top">zum Seitenanfang</a>
</font>

</body>
</html>
