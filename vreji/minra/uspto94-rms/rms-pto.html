<Title>
Richard Stallman's Testimony to the PTO Hearings
</Title>

<Body>

<STRONG>For submission to the Patent & Trademark Office</STRONG><br>
<EM>by Richard Stallman</EM>

<p>
Each year the government creates new bureaucratic programs.  Each is
created for a purpose.  That doesn't mean it serves that purpose or
that it is worth the cost.
</p>

<p>
It's hard to close down unnecessary bureaucracies, because people
presume that they must do <EM>some</EM> good.  It's easy to admit that a
government program has drawbacks, but many won't seriously consider
whether it does more harm than good.
</p>

<p>
Thus we see, in the announcement of these hearings, the supposition
that software patents are helpful.  We are asked whether they
"protect" software developers "enough".  Patent lawyers chose the word
"protection" to imply that patents are good.
</p>

<p>
Now commentators feel pressure to use the same prejudiced word, lest
they appear unschooled.  This does its part to impede consideration of
the possibility that patents are actually "too much".
</p>


<p>
I'm not a lawyer.  I am a programmer, and considered a good one.  I'm
here to explain why software patents impede software development and
retard software progress.
</p>


<p>
Software is like other fields of engineering in many ways.  But there
is a fundamental difference: computer programs are built out of ideal
mathematical objects.  A program always does exactly what it says.
You can build a castle in the air supported by a line of zero
thickness, and it will stay up.
</p>

<p>
Physical machinery isn't so predictable, because physical objects are
quirky.  If a program says to count the numbers from one to a
thousand, it will do exactly that.  If you build the counter out of
machinery, a belt might slip and count the number 58 twice, or a truck
might go by outside and you'll skip 572.  These problems make
designing reliable physical machinery very hard.
</p>

<p>
When we programmers put a <b>while</b> statement inside an <b>if</b> statement,
we don't have to worry about whether the <b>while</b> statement will run
such a long time that it will burn out the <b>if</b> statement, or that it
will rub against the <b>if</b> statement and wear it out.  We don't have to
worry that it will vibrate at the wrong speed and the <b>if</b> statement
will resonate and crack.  We don't have to worry about physical
replacement of the broken <b>if</b> statement.  We don't have to worry
about whether the <b>if</b> statement can deliver enough current to the
<b>while</b> statement without a voltage drop.  There are many problems
of hardware design that we don't have to worry about.
</p>

<p>
The result is that software is far easier to design, per component,
than hardware.  This is why designers today use software rather than
hardware wherever they can.  This is also why teams of a few people
often develop computer programs of tremendous complexity.
</p>

<p>
People naively say to me, "If your program is innovative, then won't
<em>you</em> get <em>the</em> patent?"  This question assumes that one product goes
with one patent.
</p>

<p>
In some fields, such as pharmaceuticals, patents often work that way.
Software is at the opposite extreme: a typical patent covers many
dissimilar programs and even an innovative program is likely to
infringe many patents.
</p>

<p>
That's because a substantial program must combine a large number of
different techniques, and implement many features.  Even if a few are
new inventions, that still leaves plenty that are not.  Each technique
or feature less than two decades old is likely to be patented already
by someone else.  Whether it is actually patented is a matter of luck.
</p>

<p>
The only way a programmer can avoid this mess is by sticking to
things that are obsolete.  You may not recall the state of the
computer field seventeen years ago, since most people didn't pay
attention back then.  There were no personal computers.  If you were a
hobbyist, you might get a computer with a few thousand bytes of
memory.  With luck, it might run Basic.
</p>

<p>
This shows another way software is different: it progresses very
quickly.  A program three years old is becoming obsolete, and one six
years old looks stone-age.  A twenty-year monopoly for anything in
computers is absurd.
</p>

<p>
When I spoke to the American Bar Association
Science and Technology section, the moderator asked the audience
whether they saw a need for any software monopoly lasting more than
ten years.  Most said no.  Some of them said, "No, aside from copyright."
Some said, "No, not even copyright."
</p>

<p>
In other fields, a new technique may require development--building one
device after another until you understand how to make the technique
work.  If a steel part works badly, and you think copper might be
better, you can't type "replace steel with copper" and try it ten
minutes later.  The need to recoup this cost of this development
is part of the usual argument for patents.
</p>

<p>
In software, an individual technique usually doesn't need much
development.  What we develop are products that combine a new
technique with dozens of other techniques.
</p>

<p>
When a second programmer decides to use the same technique, he will
have to do just as much development work as the first
programmer--firstly, because he's probably using a different
combination of techniques, and secondly, because the first programmer
probably kept the results of development a trade secret.
</p>

<p>
The patent system is supposed to help by discouraging trade secrecy.
In software, patents don't do this.  Today, just as in 1980, most
developers publish general ideas and keep the source code secret.
</p>

<p>
The Kodak Photo CD shows how this can work.  The general ideas used in
the Photo CD are patented; the details of the algorithm and data
format are secret.  Even twenty years from now, we will not know how
to read a Photo CD unless someone does reverse engineering--and he will
probably be sued for copyright infringement.
</p>

<p>
To manufacture a physical device, you need to set up a factory--a big
investment.  The need for this investment is one of the arguments for
patents in other fields.  But mastering a CD-ROM costs just $1000.
</p>

<p>
Here is a copy of the compiler that I wrote with a few friends.  This
program is mainly the work of four people.  Another dozen helped
substantially and others occasionally.  The two principal developers
were not working on this full-time.
</p>

<p>
This compiler and its output are probably being used on more than a
million computers today.  Major companies such as Intel and Motorola
have adopted it and now add to it.  The US Air Force is funding
extensions to it.  Many widely used systems are compiled with it.
</p>

<p>
Just a few lines of code can be enough to infringe a patent.  This
compiler has ten thousand pages.  How many patents does it infringe?
</p>

<p>
I don't know.  Nobody does.  Perhaps you can read the code and tell me.
</p>

<p>
I know of one patent it infringes.  I found it--along with some near
misses--in a list I saw by luck.  I believe I have prior art for that
patent, but I can't be sure what a court would say.  I don't dare tell
you the patent number, because if I were sued, I couldn't pay for the
defense.  I would lose by default.
</p>

<p>
An invalid patent is a dangerous weapon.  Defending a patent suit
typically costs a million dollars, and the outcome depends mostly on
legal technicalities.
</p>

<p>
I've had at least one patentable idea in my career.  I know this
because someone else patented it years later.  It is a feature for
using abbreviations in a word processor.
</p>

<p>
A couple of years ago, the users of the word processor XyWrite
received a downgrade in the mail.  XyWrite had an abbreviation
feature; the developer removed it when threatened by the patent
holder.
</p>

<p>
They knew about my earlier published work; why didn't they fight the
patent?  Sometimes a business can't afford to have a lawsuit that will
drag on for years.  At those times, even if the patent is invalid, you
lose.
</p>

<p>
These patents are invalid because of luck.  It was pure luck that
these ideas were published by one person before they were patented by
another.  And it was luck that the ones who published didn't patent
instead.
</p>

<p>
This is an important point: what is patented, and what is not, is
mainly a matter of luck.  When you develop a large system, and you
need to combine a large number of techniques and features, whether or
not you can use each one is a matter of luck.
</p>

<p>
The carelessness of the Patent Office in dealing with software is well
known.  So some people assume that, if the PTO does a better job,
everything will be OK.  They say that we should wait while the invalid
patents spew out, and eventually the PTO will "understand software"
and do the job right.
</p>

<p>
There are two flaws in that suggestion: the PTO will not do a better
job, and it would not solve the problem if they did.
</p>

<p>
Some years ago, a professor I know patented Kirchhoff's current law,
which says that the electric currents flowing into a junction equal
the currents flowing out.  He did this to confirm, privately, his
suspicion that the PTO could not handle the field of electronics.  He
never tried to enforce the patent, which has since expired.  (I will
disclose his name if you give assurances that he and his lawyer will
not get in trouble.)
</p>

<p>
Kirchhoff's laws were formulated in 1845.  If the PTO couldn't
understand electricity after a century, how can we expect it to
understand software in another decade or two?
</p>

<p>
Computer scientists look at many software patents and say, "This is
absurdly obvious."  Defenders of the patent system reject our opinion.
"You're using hindsight," they say.  "You're more skilled than the
typical practitioner."  What we consider obvious patents are not
errors; they reflect a different definition of "obvious".  It won't
change.
</p>

<p>
The most outrageously absurd software patent of all covers using
"exclusive or" (an operation of Boolean arithmetic) to modify the
image on a bit-map display screen.
</p>

<p>
This technique is so basic that it was done with <em>just one
instruction</em> on the PDP-10 computer I used in the 1970s.  I'm told
this patent was upheld twice in court.
</p>

<p>
When the PTO issues two patents for the same technique, as it did for
the LZW data compression algorithm, that is surely a mistake.  These
mistakes can't be avoided because one technique can be described in
different ways.
</p>

<p>
It can take careful thought to see that two verbal descriptions
describe the same computation.  Mathematicians Atiyah and Singer once
noticed that the same computation appeared, with different
descriptions, in two branches of mathematics.  This led them to the
Atiyah-Singer Index Theorem, for which one received a presidential
medal and the other a knighthood.  The PTO expects an examiner to
compare an application with not just one other thing, but everything
known to man, in just 17 hours (average).  Even if examiners were
geniuses like Atiyah and Singer, they couldn't do this.
</p>

<p>
What if the PTO stopped making mistakes and issued no more invalid
patents?  That would not solve the problem, because all <em>new</em>
techniques and features--those not known today--would be patented just
the same.
</p>

<p>
Suppose the PTO were perfect.  Suppose that it is the year 2010 and
you are a software developer.  You want to write a program combining
200 patentable techniques.  Suppose 15 of them are new--you might
patent those.  Suppose 120 of them were known before 1990--those would
not be patented any longer.  That leaves 65 techniques probably
patented by others--more than enough to make the project infeasible.
This is the grid lock we are heading for.
</p>

<p>
Today's PTO mistakes are bringing us to grid lock sooner, but the
ultimate result is grid lock even with a perfect PTO.
</p>

<p>
I've explained how patents impede progress.  Do they also encourage it?
</p>

<p>
Patents may encourage a few people to look for new ideas to patent.
This isn't a big help because we had plenty of innovation without
patents.  (Look at the journals and advertisements of 1980 and you'll
see.)  New ideas are not the limiting factor for progress in our
field.  The hard job in software is developing large systems.
</p>

<p>
People developing systems have new ideas from time to time.  Naturally
they use these ideas.  Before patents, they published the ideas too,
for kudos.  As long as we have a lot of software development, we will
have a steady flow of new published ideas.
</p>

<p>
The patent system impedes development.  It makes us ask, for each
design decision, "Will we get sued?"  And the answer is a matter of
luck.  This leads to more expensive development and less of it.
</p>

<p>
With less development, programmers will have fewer ideas along the
way.  Patents can actually reduce the number of patentable ideas that
are published.
</p>

<p>
You will probably hear someone today raise the issue of whether
software "inventors" are "entitled" to patents just like other
inventors.  The answer is simple: like other inventors, they are not
entitled to patents at all.
</p>

<p>
Our Constitution rejects the idea of patents as an entitlement.
Patents and copyrights are justified as a restriction on the public
only for the sake of the public--only for promoting progress.
</p>

<p>
Every two years, we elect our Congress.  We don't ask who deserves to
win, because it's impossible to deserve this, and that's the wrong
question anyway.  Instead, we ask which Congress the nation deserves.
</p>

<p>
The same applies to patent policy.  We shouldn't ask who deserves a
patent, because it's impossible to deserve this, and that's the wrong
question anyway.  The right question is how the policy affects the
public.  Do software patents "promote the progress of science and
useful arts" as the Constitution requires?  Do they promote progress
enough to justify their cost--the imposition on the freedom of
individuals to use what they know?
</p>

<p>
A decade ago, the field of software functioned without patents.
Without patents, it produced innovations such as windows, virtual
reality, spreadsheets, and networks.  And because of the absence of
patents, programmers could develop software using these innovations.
</p>

<p>
We did not ask for the change that was imposed on us.  There is no
doubt that software patents tie us in knots.  If there's no clear and
vital public need to tie us up in bureaucracy, untie us and let us get
back to work!
</p>

<p>
<EM>Responses to the specific questions posed by the PTO:</EM>
</p>

<p>
1. The best definition for a "software patent" is, any patent that 
restricts the development of new software.  Regardless of the form
in which this restriction is expressed, the practical effect of such
a patent is to create an obstacle to software development.
</p>

<p>
For the most part, the harmfulness of a patent depends on the
substance, not the form, of the patent.  More precisely, the extent to
which a particular software patent impedes software development
depends on the class of programs it prohibits.  A patent that covers a
broad class of programs, or that covers a vital function of programs,
is very harmful.  A patent that covers fewer programs, or only obscure
programs, is less harmful.
</p>

<p>
A patent can be written in various ways while still prohibiting the
same class of programs.  The most extreme example is that a patent
written to describe hardware can apply to software implementations,
under the doctrine of equivalents.
</p>

<p>
As a result, new criteria for patentability, based on the sort of
distinctions that question 1 asks about, would have little practical
effect.  Forbidding patents of one form would encourage applicants to
use another form, with no change in substance.
</p>

<p>
Solving the problem of software patents probably requires changing the
scope of patents rather than the criteria for patentability.  More
precisely, the best solution is to exempt software implementations
running on general purpose computers from patent law--so that a
developer of software for general purpose computers would ipso facto
never need to worry about patents.
</p>

<p>
Nonetheless, here are responses to question 1 as it was asked.
</p>

<p>
1-A. A patent covering the implementation of a particular algorithm
on a general purpose computer obstructs software development and therefore
is harmful.
</p>

<p>
In computer science there is no meaningful distinction between
"mathematical" and "non-mathematical" algorithms.  Algorithms in
general are a branch of mathematics.
</p>

<p>
1-B. A patent that covers only special purpose hardware
implementations is not really a software patent, in that it cannot
cause trouble for software development.
</p>

<p>
Reportedly patents written in this form actually do cover pure
software implementations, under the doctrine of equivalents.  If this
is true, a patent of form B is equivalent in its effect to a patent of
form A, and is harmful to software development.
</p>

<p>
1-C-1. If the program covered by the patent is substantial, the patent
would be narrower in breadth than the copyright on the program it
covers.  This means the patent would be harmless to people writing new
software.  Hardly anyone would apply for such a patent, since the
copyright lasts longer and exists automatically.
</p>

<p>
If the program patented is very short--perhaps just a few lines--then
a patent of this sort might cause trouble for software development.
The practical effects of such a patent would depend crucially on the
precise interpretation of the patent's scope.
</p>

<p>
1-C-2. The typical software patent of today has this form.  These
patents interfere with software development.
</p>

<p>
A patent of this form covers a class of algorithms--the class of
algorithms that have in common the specific steps or elements listed
in the patent.  It is ironic that patenting many algorithms at once in
this fashion is regarded by many as less offensive than patenting a
single algorithm.
</p>

<p>
1-D. I do not see a difference in practical effect between a patent of
form D-1 and a patent of form C-1; likewise for D-2 and C-2.  While a
program can be thought of as an abstract mathematical entity, in
practice it cannot be distributed or used unless stored on a physical
medium.
</p>

<p>
1-E. A patent on a data structure is harmful to software development.
It also directly harms the users of software; they would have data
bases that they cannot read without infringing the patent.
</p>

<p>
In practice, a patent of form A or C or D can have the same effect as
a patent of form E.  For example, the patents covering LZW compression
cover a computational process (that is to say, a class of similar
algorithms); however, no one has found a way to produce the output of LZW
except by using the LZW algorithm.  The data format that LZW
produces is thus, in effect, patented.
</p>

<p>
1-F. This sort of patent interferes with software development.
</p>

<p>
1-G. These patents interfere with software development.
</p>

<p>
2. One of the programs distributed by my project is an interpreter for
the programming language PostScript.  The revised "level 2" version
of PostScript specifies the use of a particular data compression 
algorithm known as LZW.  This algorithm is patented, as a result of
which we cannot implement the full PostScript language.  
</p>

<p>
It should be noted that the LZW algorithm has been patented twice.  It
was developed independently by two groups of people, and there is good
reason to believe both groups would have published in academic
journals in the absence of software patents.
</p>

<p>
We know of a superior compression algorithm that we believe is unpatented,
and for some purposes we use the superior algorithm.  However, no other
algorithm can replace LZW for PostScript purposes, because other algorithms
produce different data formats and thus do not interoperate with other
implementations of PostScript.  
</p>

<p>
The code to implement LZW is approximately 0.2 percent of the
PostScript interpreter.  The rest of the interpreter is full of other
algorithms, data manipulations, and features, any one of which might
be patented (and perhaps some of them are).
</p>

<p>
It was difficult to find any data compression algorithm of reasonable
quality that was not covered by a patent.  There are dozens of patents
in data compression alone.  Our first planned replacement for LZW was
killed off by a newly issued patent, a week before the program's
planned release.  After another year and a half, we finally found 
an algorithm that seems not to be covered by the patents that we know
of.
</p>

<p>
3. The current system is leading to an explosion of patents.
Professor Merges of Boston University reports that, based on the
amount of software business patent law firms seem to be doing and
their interest in hiring lawyers with knowledge of software, the rate
of filing of software patents must be accelerating rapidly.  Each
patent is a land mine for software developers.
</p>

<p>
Eliminating patents from the field of software would make it easy to
develop software again.
</p>

<p>
4. The present patent system obstructs innovation in the software
field.  It appears that the appropriate level of monopoly for
software-related inventions is none whatever; that level of monopoly
produced, through the early 80s, a high rate of that sort of
innovation without hampering the development of software.
</p>

<p>
I have never been involved in a patent lawsuit.  I have had to
withdraw programs from use because I knew of applicable patents for
which I had no prior art--this has happened twice in data compression,
as described above.
</p>

<p>
But main effect that I see is that there are steadily fewer areas of
software where I will be able to develop in the future, due to the
spread of patents.  I forecast this effect when I hear about newly
issued patents; I don't need to wait till I myself am threatened with
them.
</p>

<p>
5. No additional form of intellectual property for software is needed.
The forms in use in 1980--trade secret and narrow copyright--provided
plenty of incentive for commercial software development; there is no
public interest in having more.
</p>

<p>
However, a new form of intellectual property for software could be
an improvement if it <em>replaces</em> all the forms that currently exist.
Here is a proposal for such a system:
</p>

<LI> When a program is released to the public, the developer is
required to place the complete source code in escrow at the Library of
Congress, where it is kept temporarily secret.

<LI> The software is then trade secret and copyrighted for four years.
The power of this copyright could be as strict as anyone wishes.


<LI> Four years later, the program enters the public domain, and the
escrowed source code is released by the Library of Congress, also in
the public domain.


<LI> No other form of intellectual property monopoly covers software.

<p>
This form of intellectual property is sufficient to enable most
software companies to make a profit, because typically a four-year-old
version of a program is obsolete.  When version 1 of a program enters
the public domain, its developer will be selling version 3 or version
4.  Version 1 as public domain software will not be much competition
for them.
</p>

<p>
A software company that has no new version in four years is doing a
bad job.  With this proposal, that company might fail; but it is in
danger of failing under the present system also.  Besides, a badly run
business ought to fail; that is a necessary part of free enterprise.
</p>

<p>
By contrast, successful software companies come out with substantial
improvements every year, and they would be profitable under this
proposal.
</p>


<hr>
Back to <A HREF="patents.html">LPF Patents Page</A>.
<hr>
<Address>HTML by rh@lpf.org</Address>
</Body>