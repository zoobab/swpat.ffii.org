


     _________________________________________________________________

   I-Prom - Innovationsmagazin des Bundesministeriums f�r Wirtschaft und
                    Arbeit, Ausgabe 02, 07/2005, S. 4-5

               Leipzig sch�tzt den Rest der ([1]Computer)Welt
               70 L�nder im Interntet- und E-Mail-Waschsalon

   Seiteneinsteiger sind nicht erst seit Blitzableitererfinder Benjamin
   Franklin, dem sp�teren US-Pr�sidenten, ein Gl�cksfall. Input von au�en
   hat so mancher Branche gut getan. Derzeit revolutioniert ein
   studierter Heizungs- und Sanit�ringenieur aus Mitteldeutschland mit
   einer ganz besonderen "Waschmaschine" das [2]Internet. Ren� Holzer ist
   Erfinder des Echtzeit-Datenfilters gegen [3]Viren, W�rmer, Dialer und
   Spams. "[4]SaferSurf" l�scht Computersch�dlinge bereits in der
   Datenleitung; ein Angriff auf PC und Netzwerke wird somit unm�glich.
   Ramona Wonneberger Ren� [5]Holzer

   Kurz nach Markteintritt 2001 hatte Unternehmensberater Roland Berger
   dem Virusj�ger eine gro�e Zukunft prognostiziert. Daf�r erhielt
   Holzers [6]Nutzwerk GmbH, die er gemeinsam mit Ramona Wonneberger
   leitet, aus M�nchen einen Sonderpreis f�r das beste
   Wertsch�pfungsmodell. Inzwischen hat die ostdeutsche Top-Innovation
   55.000 Kunden in 70 (!) L�ndern. Das sind vorrangig Privatpersonen,
   also Frau M�ller, Monsieur Fontaine, Mrs. Smith und Gospodin Iwanow.
   Aber auch Schulen sowie Unternehmen z�hlen zu den Abo-Kunden der
   Sicherheitsserver, die mehrmals in der Stunde aktualisiert werden.
   Wenn weltweit jeder [7]Provider diese L�sung einsetzen w�rde, w�ren
   wahrscheinlich Computerviren & Co. kein Thema mehr.
   Was macht [8]Holzer anders als die anderen? Er geht der Virenplage
   nicht �ber eine Software f�r den PC-Endnutzer an den Kragen.
   Stattdessen werden die St�renfriede bereits bei der �bertragung
   ausgeschaltet; nur ungef�hrliche Bits und Bytes werden in beide
   Richtungen durchgelassen. "Wir gehen die Sicherheit vor dem Rechner
   an." Diese Idee konnte fast nur von einem Geb�udetechniker stammen,
   str�men doch Wasser und Gas auch durch Rohrleitungen und m�ssen in
   entsprechendem Abstand zum Anwender gefiltert werden.
   Das Prinzip kommt an, nicht erst, seitdem der unkomplizierte
   [9]Virenschutz f�r monatlich wenige Euro auch mit einer ebenso
   cleveren Datenkomprimierung zur schnelleren Kommunikation und damit
   geringeren Netzbetriebskosten verbunden wurde. Derzeit arbeitet das
   [10]Nutzwerk-Team an einer wegweisenden Neuerung, die mit Fug und
   Recht ebenfalls als "unglaublich" attributiert werden kann. Es geht um
   den v�llig automatischen Schutz vor so genannten [11]Spam-Mails -
   einem Problem, vor dem solche Global-Player wie Microsoft und Yahoo
   kapituliert haben.

   [05_07_i-prom_1.jpg] Der neue E-Mail-Filter "made in Leipzig" erkennt
   zweifelsfrei erw�nschte und unerw�nschte E-Mails. Um das zu beweisen,
   wurde die Innovation im hauseigenen Support-System installiert. Das
   ist nicht nur bei [12]Nutzwerk die empfindlichste Schaltstelle, da
   hier s�mtliche umsatzrelevanten Abonnement-Aktivit�ten zusammenlaufen.
   Das Tool l�scht einerseits unerw�nschte E-Mails aus dem Datenstrom und
   f�hrt andererseits [13]automatisch sogar die Abbestellungen aus.

   [14]Nutzwerk hat nicht nur auf der Dienstleistungsseite Gro�es vor.
   Der B�rsengang scheint nur eine Frage der Zeit. Wenn Holzer �ber das
   Woher und Wohin redet, dann oft mit unternehmensphilosophischem
   Impetus. Eine Kostprobe gef�llig? "Im Gegensatz zu Ideen ist
   finanzieller Wert schnell verg�nglich, weil Geld allein bekanntlich
   keine Tore schie�en kann."
   Auch eine andere seiner Thesen ist bei SaferSurf l�ngst Realit�t
   geworden: "Besonders erfolgreich sind die, die sich mit ihrer
   Erfindung auf komplettes Neuland begeben und damit ein Monopol ohne
   Konkurrenzdruck aufbauen k�nnen." Das hat der Enddrei�iger, der nach
   seinem Studium 1992 zun�chst erfolgreiche Software-Programme f�r die
   Baubranche geschrieben und mit "baunet" das erste umfassende
   Branchenportal auf die Beine gestellt hatte, in nur wenigen Jahren
   geschafft. Flankiert wurde der Firmenaufbau von einem privaten
   Investor, der in das Start-up von damals aus innerer �berzeugung eine
   sechsstellige DM-Summe investierte.

   Virenbek�mpfung schon vor dem PC - dieses Verfahren hat sich die
   Nutzwerk GmbH patentieren lassen

   Mitentscheidend f�r den Erfolg gerade auch in der Anfangszeit war eine
   konsequente Schutzrechtsarbeit. Nutzwerk h�lt zwei eigene Patente und
   hat Patentschutz f�r zwei weitere Erfindungen angemeldet. Das
   Basispatent mit der Nummer DE 199 58 638 hat mit entsprechenden
   Auslandsanmeldungen weltweit Furore gemacht. Allein im ersten Halbjahr
   2003 wurde Patentschutz f�r 25 L�nder beantragt. Lizenzen werden nicht
   vergeben. Weil sich alles vom Firmensitz aus steuern l�sst, ist
   Leipzig in diesem Fall wirklich der Nabel der SaferSurf-Welt. Da
   Patenturkunden ein Wertzertifikat f�r jedes Unternehmen sind, dauerte
   es auch bei Nutzwerk nicht lange, bis eine Beteiligungsgesellschaft
   ihren Einstieg signalisierte.
   Die Schutzrechtsstrategie der kleinen Firma mit Dachgarten samt Grill
   auf einem B�rohaus am Rande der Innenstadt bringt die IT-Branche
   geh�rig in Bewegung. Entsprechende Einspr�che haben bereits das
   Bundespatentgericht in M�nchen besch�ftigt. Die beiden Gesch�ftsf�hrer
   sch�tzen indes nicht auf "Teufel komm raus". Inzwischen sieht Holzer
   seine Schutzrechte "langsam verblassen". Man habe via DPMA, das im
   �brigen zwei Jahre f�r die Patentpr�fung gebraucht habe, dennoch einen
   wichtigen Zeitvorsprung erreichen k�nnen, so der geb�rtige
   Merseburger. Wer jetzt [15]kopieren wolle, brauche neben Zeit und Geld
   eigentlich noch mehr Know-how als er selbst."

   Nutzwerk GmbH, Leipzig
   Gr�ndung:
   Mitarbeiter:
   Umsatz 2004:
   1997
   12
   1,9 Mio. Euro (2005: 3 Mio.)



                                [spacer.gif]
                                [paper.gif]


                             [16]Pressespiegel

   I-Prom -- Innovationsmagazin vom Bundesministerium f�r Wirtschaft und
   Arbeit
          ________________________________________________________
                 __________________________________________

                                 SaferSurf
          ________________________________________________________
                 __________________________________________



                           Weitere Informationen
      [17]SaferSurf.com jetzt auch mit T�V-Plakette (Pressemitteilung)
      [18]Echtzeit-Schutz mit Viren-Scanner im WWW (Yahoo! Deutschland
                                Nachrichten)
                   [19]SaferSurf unter www.safersurf.com

   [px_grey.gif] [20]Kontakt
   [21]zur�ck

   Unsere Produktlinie:
   [22]SaferSurf - Anonym surfen - Testsieger ComputerBild 
   [23]www.SaferSurf.com 
      [24]Virenschutz
      [25]Schneller Surfen
      [26] Anonym Tauschen
      [27]Spamschutz

                       � 2002-2005 [28]Nutzwerk GmbH

Verweise

   1. http://www.news4press.net/presse/10995552004706.php3
   2. http://www.mp3-world.net/news/67666_2-serie-anonymes-filesharing-moeglich.html
   3. http://portale.web.de/Computer/Sicherheit/?msg_id=5504820&show=img
   4. http://www.press1.de/ibot/db/press1.nutzw_1110483503.html
   5. http://www.complex-berlin.de/modules.php?name=News&file=article&sid=94
   6. http://www.mp3-world.net/news/68082-anonymitaet-von-p2pnutzern-gefaehrdet.html
   7. http://www.pressebox.de/meldungen/presse-32290
   8. http://www.itseccity.de/content/produkte/dienstleistungsangebote/050402_pro_die_nutzwerk.html
   9. http://www.bvmw-dresden.de/system/article/printarticle.php?sid=89
  10. http://www.digitale-chancen.de/content/buchrezis/indexdeep.cfm/key.67/secid.12/aus.26
  11. http://www.wallstreet-online.de/ws/news/news/main.php?action=viewnews&newsid=1442820&m=3.1.1.3.0&
  12. http://stop1984.org/print.php?lang=de&text=entry.txt&entry=20041105-16
  13. http://www.nutzwerk.de/media/releases.html
  14. http://pressemitteilung.ws/node/view/2175
  15. http://www.geizkragen.de/forum/das-geizforum-archiv-c6/rund-um-internetzugaenge-und-flatrates-f26/next-generation-bei-0190-dailern-p103003/
  16. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/echo.html
  17. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/releases/05_03_safersurf_tuev.html
  18. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/echo/05_04_21_yahoo.html
  19. http://www.safersurf.com/index.html
  20. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/contact/contact.html
  21. javascript:history.back()
  22. http://www.nutzwerk.de/safersurf/anonym-surfen/index.html
  23. http://www.safersurf.com/index.html
  24. http://www.safersurf.com/index.html
  25. http://www.safersurf.com/speed/index.html
  26. http://www.safersurf.com/anonym-tauschen/index.html
  27. http://www.safersurf.com/spam-schutz/index.html
  28. http://www.nutzwerk.de/
