<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
        <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=iso-8859-1">
        <TITLE>Some Observations on the Controversy on "Software Patents"</TITLE>
        <META NAME="GENERATOR" CONTENT="StarOffice/5.2 (Win32)">
        <META NAME="AUTHOR" CONTENT="Axel H Horns">
        <META NAME="CREATED" CONTENT="20010116;21280071">
        <META NAME="CHANGEDBY" CONTENT="Axel H Horns">
        <META NAME="CHANGED" CONTENT="20010614;11305056">
        <META name="DESCRIPTION" content="Relevant information about intellectual property law">
<META name="KEYWORDS" content="patents, trademarks, marks, software, intellectual property, patent attorney, patent office, law, SWPAT, prosecution, litigation, Germany, Europe, programming languages, syntax, grammar, text">
        <STYLE>
        <!--
                @page { size: 21.01cm 29.69cm; margin-left: 4.83cm; margin-right: 3.56cm; margin-top: 2.4cm; margin-bottom: 3.5cm }
                P.sdfootnote { margin-right: 0.2cm; margin-top: 0.1cm; margin-bottom: 0cm; font-family: "Helvetica", sans-serif; font-size: 8pt; line-height: 100%; text-align: justify; widows: 2; orphans: 2; page-break-before: auto }
                A.sdfootnoteanc { font-size: 57% }
                A.sdfootnotesym { font-family: "Helvetica", sans-serif; font-size: 11pt }
        -->
        </STYLE>
</HEAD>
<BODY>
<DIV TYPE=HEADER>
        <P ALIGN=CENTER STYLE="margin-top: 0.2cm; margin-bottom: 0.51cm"><BR>
        </P>
</DIV>

<TABLE WIDTH="100%" border="0" cellpadding="0" cellspacing="0" BGCOLOR="#A0D7FF">
<TR BGCOLOR="#A0D7FF">
    <TD ALIGN="LEFT" VALIGN="CENTER" WIDTH="150" BGCOLOR="#A0D7FF">
    <TABLE BORDER="0" WIDTH="150" cellpadding="0" cellspacing="0">
    <TR BGCOLOR="#000000"><TD>
    <A HREF="http://www.ipjur.com/"><IMG SRC="./ipjur.gif" border="0" hspace="0" vspace="0" HEIGHT="40" WIDTH="150" align="middle"></a>
    </TD></TR>
    </TABLE>
    </TD>
    <TD ALIGN="CENTER" VALIGN="CENTER" colspan="4">
    <IMG SRC="./iplaw.gif" border="0" hspace="0" vspace="0" HEIGHT="40" align="middle">
        </TD>
</TR>
<TR BGCOLOR="#000000">

    <TD HEIGHT="18" WIDTH="20%" ALIGN="LEFT" VALIGN="BOTTOM">
        <A HREF="./00.php3"><IMG SRC="./arrow2.gif" ALT="NEWS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./news1.gif" ALT="NEWS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./02.php3"><IMG SRC="./arrow2.gif" ALT="I2P INFO" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./i2pa.gif" ALT="I2P" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./01.php3"><IMG SRC="./arrow2.gif" ALT="SOFTWARE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./software.gif" ALT="SOFTWARE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./03.php3"><IMG SRC="./arrow2.gif" ALT="MOVING SPOTS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./moving.gif" ALT="MOVING SPOTS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" VALIGN="BOTTOM" ALIGN="RIGHT">
        <A HREF="./04.php3"><IMG SRC="./arrow2.gif" ALT="PRACTICE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./practice.gif" ALT="PRACTICE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>
</TR>
</TABLE>


<TABLE WIDTH="100%" CELLPADDING="0" CELLSPACING="0" BORDER="0">
<TR>
<TD WIDTH="150" ALIGN="LEFT" VALIGN="TOP" BGCOLOR="#A0D7FF">



<!-- /news-left -->
<FONT SIZE="-2" FACE="Arial">
Updated: <!-- last-modified--> 2001-04-10
<!-- /last-modified--><BR>
</FONT>

<!-- common-footer -->
<FONT FACE="ARIAL" SIZE="-2">

<P><A href="mailto:horns@ipjur.com">&copy; 2001 PA Axel H Horns</A></p>
</FONT>

</TD>

<TD ALIGN="LEFT" VALIGN="TOP"> <FONT FACE="Arial" SIZE="-2">

<br>
<P ALIGN=CENTER STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=3><B>Some
Observations on the Controversy</B></FONT></P>
<P ALIGN=CENTER STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=3><B>on
<FONT FACE="Arial, sans-serif">&raquo;</FONT>Software Patents<FONT FACE="Arial, sans-serif">&laquo;</FONT></B></FONT></P>
<P ALIGN=CENTER STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2><SPAN STYLE="font-weight: medium">PA
Axel H Horns<A CLASS="sdfootnoteanc" NAME="sdfootnote1anc" HREF="#sdfootnote1sym"><SUP>1</SUP></A></SPAN></FONT></P>
<P ALIGN=CENTER STYLE="margin-top: 0.2cm; margin-bottom: 0cm; font-weight: medium">
<FONT SIZE=2>(Originally published in epi information 1/2001 p.
31-35)</FONT></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm; font-weight: medium">
<BR>
</P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm; font-weight: medium">
<I>1.        New players introducing themselves</I></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">The Munich
Diplomatic Conference on the Revision of the EPC had closed on
November 29, 2000 after having decided that programs for computers
are <I>not</I> to be deleted from the list of non-patentable
subject-matters listed in Article 52 (2) (c) EPC; a respective
provision of the <FONT FACE="Arial, sans-serif">&raquo;</FONT>Basic
Proposal<FONT FACE="Arial, sans-serif">&laquo;</FONT> had been
overruled by 16 of 20 votes. <B>Ralph Nack</B> and <B>Bruno Ph&eacute;lip</B>
wrote in their report for AIPPI:
</P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>&quot;At
the Diplomatic Conference, France, Denmark, and Germany proposed to
postpone the deletion of computer programs and all other EC member
states except for Austria supported this proposal. The background of
this initiative were the massive protests against software patents by
a number of software developers.&quot;<A CLASS="sdfootnoteanc" NAME="sdfootnote2anc" HREF="#sdfootnote2sym"><SUP>2</SUP></A>
</I></FONT>
</P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">What had happened?
First drafts of the <FONT FACE="Arial, sans-serif">&raquo;</FONT>Basic
Proposal<FONT FACE="Arial, sans-serif">&laquo;</FONT> did exhibit
strong confidence that at least the computer software clause should
be removed from Article 52. But eventually this attempt failed due to
a vast majority of the EPC member states votings, and it will depend
on the view to be taken by the EU whether or not such amendment will
pass a <FONT FACE="Arial, sans-serif">&raquo;</FONT>second basket<FONT FACE="Arial, sans-serif">&laquo;</FONT>
conference to be held later in 2002. Allegedly this move was caused
by some public uproar of activists, namely of the <FONT FACE="Arial, sans-serif">&raquo;</FONT>Open
Source Software<FONT FACE="Arial, sans-serif">&laquo;</FONT> (OSS)
scene,<A CLASS="sdfootnoteanc" NAME="sdfootnote3anc" HREF="#sdfootnote3sym"><SUP>3</SUP></A>
fearing that removal of said clause would open the gates for a flood
of unwanted software patents.<A CLASS="sdfootnoteanc" NAME="sdfootnote4anc" HREF="#sdfootnote4sym"><SUP>4</SUP></A></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">Obviously, political
matters in the field of IP have created more and more public
awareness compared with years before. Groups have introduced
themselves into the arena which did not even exist before. Their main
medium is the Internet. Hence, now we have a somewhat divided public
with regard to IP matters:</P>
<UL>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">On the one
        hand, there are well-known established groups like governments,
        professional organisations and academic research institutes. These
        groups traditionally have their own traditional paper-based
        communication channels and do not act primarily in the public spaces
        newly created by the Internet.</P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">On the other
        hand, a bunch of Non-Governmental Organisations (NGOs) have little
        or no connection to the said traditional paper-based communication
        channels but make creative use of the Internet.</P>
</UL>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">As a matter of fact,
journalists and even politicians like MPs (or, at least, their
secretaries) more and more get savvy with Internet usage and can be
reached by the particular part of the public which prefers to express
their ideas and opinions in the <FONT FACE="Arial, sans-serif">&raquo;</FONT>Cyberspace<FONT FACE="Arial, sans-serif">&laquo;</FONT>.
Moreover, NGOs can gain a lot of organisatorical power at very low
cost on an international scale by utilising the Internet.<A CLASS="sdfootnoteanc" NAME="sdfootnote5anc" HREF="#sdfootnote5sym"><SUP>5</SUP></A>
Therefore, I conclude that it is of urgent importance that in
particular members of the IP professions including professional
representatives before the EPO get more aware of the developments
perceivable only in the on-line world of the Internet. The current
situation can roughly be sketched as follows:</P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm; font-weight: medium">
<I>2.        The EuroLinux coalition</I></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">In 1999, when the
preparatory work for the Diplomatic Conference was underway, a number
of activists mainly organised via the Internet under the umbrella of
the Eurolinux Alliance<A CLASS="sdfootnoteanc" NAME="sdfootnote6anc" HREF="#sdfootnote6sym"><SUP>6</SUP></A>
started activities against patentability of software-related
inventions. Members of the Eurolinux Alliance are:</P>
<UL>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">FFII e.V.;
        &quot;F&ouml;rderverein f&uuml;r eine Freie Informationelle
        Infrastruktur&quot;, Germany<A CLASS="sdfootnoteanc" NAME="sdfootnote7anc" HREF="#sdfootnote7sym"><SUP>7</SUP></A></P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">AFUL; &quot;French
        Speaking Association of Users of Linux and Free Software&quot;,
        France<A CLASS="sdfootnoteanc" NAME="sdfootnote8anc" HREF="#sdfootnote8sym"><SUP>8</SUP></A>
                </P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">APRIL;
        &quot;Association Pour la Recherche en Informatique Libre&quot;,
        France<A CLASS="sdfootnoteanc" NAME="sdfootnote9anc" HREF="#sdfootnote9sym"><SUP>9</SUP></A>
                </P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">Italian
        Initiative Against Software Patents, Italy<A CLASS="sdfootnoteanc" NAME="sdfootnote10anc" HREF="#sdfootnote10sym"><SUP>10</SUP></A></P>
</UL>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">There are a number
of sponsors from the commercial sector, among them some wide known
software companies:<A CLASS="sdfootnoteanc" NAME="sdfootnote11anc" HREF="#sdfootnote11sym"><SUP>11</SUP></A></P>
<UL>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">SuSE AG,
        Nuremberg (Major LINUX Distributor)</P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">Innominate AG,
        Berlin (LINUX Support Services)</P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">Phaidros
        Software AG, Ilmenau (UML-based Software Engineering)</P>
</UL>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">and numerous others.</P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">The most prominent
action undertaken by this coalition was the so-called <FONT FACE="Arial, sans-serif">&raquo;</FONT>Eurolinux
Petition for a Software Patent Free Europe<FONT FACE="Arial, sans-serif">&laquo;</FONT>:<A CLASS="sdfootnoteanc" NAME="sdfootnote12anc" HREF="#sdfootnote12sym"><SUP>12</SUP></A></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>&quot;This
petition is directed to the European Parliament. Its goal is to warn
European Authorities against the dangers of software patents. This
petition is supported by the EuroLinux Alliance together with
European companies and non-profit associations. Please make this
petition well known to everybody concerned. </I></FONT>
</P>
<UL>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>I
        am concerned by current plans to legalise software patents in
        Europe, considering their damaging effect on innovation and
        competition. </I></FONT>
        </P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>I
        am concerned by the possible use of software patents to patent
        business methods, education methods, health methods, etc. </I></FONT>
        </P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>I
        am concerned by the current track record of abuses from the European
        Patent Office, especially by their tendency to abuse their judicial
        power to extend the scope of patentability. </I></FONT>
        </P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>I
        am surprised that no economic report has ever been published by
        European Authorities to study the impact of software patents on
        innovation and competition. </I></FONT>
        </P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>I
        urge decision makers at all levels in Europe to enforce the Law,
        which clearly prohibits patenting pure computer programs, instead of
        changing it. </I></FONT>
        </P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><I><FONT SIZE=2 STYLE="font-size: 9pt">I
        urge decision makers at all levels in Europe to reconsider their
        current plans and to make sure patents are not abused to prohibit or
        restrict the dissemination of computer programs and intellectual
        methods.&quot;</FONT></I></P>
</UL>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">The petition is open
for endorsement by everybody. Provided by Eurolinux is a HTML web
form where full name, address and job position are to be entered by
all those who want to support this motion. Eurolinux claims to have
gathered more than 60,000 entries at the end of the year 2000 and the
petition list has not been closed up to now.</P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">In view of the
allegations set out in the petition, Eurolinux was in particular
rather successful to deliver to the public a message saying that the
EPC presently does not allow software patents and, hence, the current
practice of the EPC is blatantly illegal. Consequently, the proposed
amendment to Article 52 was said to be a statutory change allowing
such software patents so that in future the current daily practice
can be made legal. Although this position is completely wrong as can
be seen from the precise wording of the EPC as well as from the
arguing set out in various decisions of the Office, many computer
experts, journalists and even politicians shared this view, at least
as far as can be seen in the in the German press. One reason for the
quantity of resonance as well as for the broad and willing public
reception of the Eurolinux position was that patent law is indeed
very complicated and very few efforts have been done to explain its
implications to a broader public in the past.</P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">Moreover, several
e-mail discussion lists on software patents have been opened, one of
them e.g. by FFII.<A CLASS="sdfootnoteanc" NAME="sdfootnote13anc" HREF="#sdfootnote13sym"><SUP>13</SUP></A><SUP>,</SUP><A CLASS="sdfootnoteanc" NAME="sdfootnote14anc" HREF="#sdfootnote14sym"><SUP>14</SUP></A>
Regardless on whether it was deliberately made due to tactical
reasons or inadvertently due to true cluelessness, the major and
striking impression of the dialogues happening there is that the
anti-patent activists do not have any substantial knowledge of the
basic concepts of patent law, and some of those behave as if they
might even be proud thereof. Few German patent attorneys had joined
the list and started to attempt to explain the law but nevertheless
all efforts to reach a mere formal common understanding of the
problems related to patenting software-related inventions seem to be
in vain. For example, a number of activists did not even know that
there is a difference between European &quot;A&quot; Documents
exhibiting patent applications as filed by the applicant, on the one
hand, and &quot;B&quot; documents exhibiting patents as granted by
the EPO, on the other hand. Still after having been told of this
difference, they started advertising an on-line &quot;Gallery of
Horror&quot; with examples of alleged bad patent practise.<A CLASS="sdfootnoteanc" NAME="sdfootnote15anc" HREF="#sdfootnote15sym"><SUP>15</SUP></A>
However, a closer inspection uncovered that the ultra-broad patent
claims which were presented as if granted by EPO and quoted there had
actually been taken from &quot;A&quot; documents.<A CLASS="sdfootnoteanc" NAME="sdfootnote16anc" HREF="#sdfootnote16sym"><SUP>16</SUP></A>
It took additional weeks until said activists eventually decided to
take down the non-granted claims from their web site and replace them
by properly granted wordings. I tell this here for not to speak of
the insurmountable hurdles to gain a common ground of understanding
of concepts like <FONT FACE="Arial, sans-serif">&raquo;</FONT>technical
invention<FONT FACE="Arial, sans-serif">&laquo;</FONT> or the like
during the e-mail dialogue. The lack of any common language was
striking.</P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">In particular, FFII
e.V. was very busy in 2000 to promote a draft concept for amendments
to patent law:<A CLASS="sdfootnoteanc" NAME="sdfootnote17anc" HREF="#sdfootnote17sym"><SUP>17</SUP></A></P>
<UL>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>&quot;Freedom
        to Publish original Information Works: Rights derived from patents
        may be used against the industrial application of computer programs
        but not against their publication or distribution. This can be
        achieved by a simple modification of national law.</I></FONT></P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>Freedom
        of Access to Communication Standards: When someone &quot;sets
        standards&quot; using software market power, these standards must be
        free of private claims. Representants of public functions have to
        base their communication with citizens on open standards.  </I></FONT>
        </P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>Precisation
        of Patentability Criteria: The Lawmaker passes a resolution stating
        a consistent and concise interpretation of Art 52 EPC and
        corresponding articles of national law. As far as necessary,
        lawcourts are brought back on the path of this interpretation, which
        represents the original spirit of the law, by explanatory amendments
        to patent laws.</I></FONT></P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>Adequate
        systems for promotion of information innovation: Adequate systems
        for stimulating and rewarding information innovations and other
        mental labour are to be devised. This may mean a combination of soft
        exclusion rights, voting-based rewarding systems and a stronger
        public commitment to the funding of research and education.  </I></FONT>
        </P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><I><FONT SIZE=2 STYLE="font-size: 9pt">Debureaucratisation
        and Internationalisation of the Patent System: It is made possible
        to register patents instantly and free of charge by publishing them
        according to standardised requirements on the Net. On the other
        hand, a market for patentbusters is created. The
        polluter-pays-principle is established: the costs of the patent
        system are born by the owners of unjustified patents.&quot;<A CLASS="sdfootnoteanc" NAME="sdfootnote18anc" HREF="#sdfootnote18sym"><SUP>18</SUP></A></FONT></I></P>
</UL>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">These views were not
only distributed via Internet channels but also covered by lobbying
letters sent by mail to dozens of politicians. The FFII proposals in
their details would mean that statutory criteria for patentable
subject-matters would be set extremely narrow, blocking not only
patenting of software-related inventions but also other inventions
which theoretically could have been obtained by exercising brute
computing force:<A CLASS="sdfootnoteanc" NAME="sdfootnote19anc" HREF="#sdfootnote19sym"><SUP>19</SUP></A></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><FONT SIZE=2 STYLE="font-size: 9pt"><I>&quot;A
'technical' process is one that uses natural forces to directly cause
a transformation of matter. Objects that contain both technical and
non-technical features are inventions only if the part that is
claimed to be new and inventive, i.e. the core of the invention, lies
in the technical realm. A technical process controlled by a computer
program on known hardware is an invention if and only if it uses
natural forces in a new way to directly cause a success in production
of material goods that could not have been predicted by mere
computation based on prior knowledge.&quot;</I></FONT></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">The public sector
would be discouraged from providing substantial patent examination
services to the public:<A CLASS="sdfootnoteanc" NAME="sdfootnote20anc" HREF="#sdfootnote20sym"><SUP>20</SUP></A></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><I><FONT SIZE=2 STYLE="font-size: 9pt">&quot;It
is made possible to register patents free of charge without
examination and without annual fees by a standard-conformant
publication in the Internet. Such patents become valid immediately
upon registration. In return, additional incentives for patent
invalidation proceedings are created. If someone succeeds in
invalidating or narrowing a patent, he receives, in addition to the
reimbursement of litigation costs, an attractive incentive payment
from the owner of the bad patent. Instead of the long and ineffective
official examination, a private professional group of patent busters
shall protect the public from invalid patents. The official
examination continues to exist, but is conducted only at the request
of the applicant. It can be conducted either by the patent office or
by certified examination institutes at home and in foreign
countries.&quot;</FONT></I></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">Obviously FFII seems
to be unaware of the existence of utility models. Moreover, in the
proposed FFII system, the general public would not be able to get
authoritative advice on enforceable patents because of they are
scattered all around the Internet. Furthermore, the incentive payment
would impose a huge financial risk in particular on small and medium
entities (SMEs). In total, the FFII proposal is practically
unworkable in its details and seems to be designed to restrict the
patent system into a limited niche area of the information society.
Supporters for such initiatives are not only recruited from the scene
of OSS activists but also from commercial entities which, for
whatever reasons, have decided not to participate in the patent
system but to fight against it.</P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><I>3.        The role of
OSS</I></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">However, it is
important to obtain a proper understanding of the role of <FONT FACE="Arial, sans-serif">&raquo;</FONT>Open
Source Software<FONT FACE="Arial, sans-serif">&laquo;</FONT> (OSS) in
the patent debate. Open Source does not just mean that merely the
source code of a piece of software is open to the public. The concept
of Open Source also comprises that everyone is entitled to make free
use of the software including creation of derivative works.<A CLASS="sdfootnoteanc" NAME="sdfootnote21anc" HREF="#sdfootnote21sym"><SUP>21</SUP></A>
As a matter of fact, OSS is an impressive success story.<A CLASS="sdfootnoteanc" NAME="sdfootnote22anc" HREF="#sdfootnote22sym"><SUP>22</SUP></A>
But why should OSS activists be privileged in view of patent law? I
do not give legal arguing here.<A CLASS="sdfootnoteanc" NAME="sdfootnote23anc" HREF="#sdfootnote23sym"><SUP>23</SUP></A>
There is a certain <I>political</I> implication which should be taken
well into account. The European Union in general and the German
government in particular are eager to promote e-commerce over the
Internet in order to boost their economical situations. However,
consumers and other trade circles show a somewhat reluctant attitude
towards this new way of running an economy. From polls it is well
known that consumers as well as business professionals lack
sufficient confidence in the software basis of e-commerce. They fear
malicious functions deliberately implemented e.g. by the software
vendor. And, experts say that fostering OSS might be a suitable way
to overcome this problem.<A CLASS="sdfootnoteanc" NAME="sdfootnote24anc" HREF="#sdfootnote24sym"><SUP>24</SUP></A>
There is no theory saying that each and every end user of OSS should
inspect the source code thereof in order to uncover bugs and
malicious code before using it. This would be completely unrealistic
simply because of few people have sufficient knowledge. But
nevertheless and as a matter of fact there is a sufficient number of
experts in the field not bound by loyalty to a certain government
agency or a certain company forming some kind of informed and
competent public exercising all scrutiny when working on the source
code. Hence, it is believed that it would be very difficult to
introduce malicious code e.g. into the LINUX kernel sources.<A CLASS="sdfootnoteanc" NAME="sdfootnote25anc" HREF="#sdfootnote25sym"><SUP>25</SUP></A><SUP>,</SUP><A CLASS="sdfootnoteanc" NAME="sdfootnote26anc" HREF="#sdfootnote26sym"><SUP>26</SUP></A>
This message has been recognised on the political stage. For example,
the European Union<A CLASS="sdfootnoteanc" NAME="sdfootnote27anc" HREF="#sdfootnote27sym"><SUP>27</SUP></A>
as well as the German government<A CLASS="sdfootnoteanc" NAME="sdfootnote28anc" HREF="#sdfootnote28sym"><SUP>28</SUP></A>
are now supporting and funding OSS projects.<A CLASS="sdfootnoteanc" NAME="sdfootnote29anc" HREF="#sdfootnote29sym"><SUP>29</SUP></A>
In particular, the German Secretary for Economical Affairs had made
significant efforts to foster the debate on the implications of OSS,
also in particular with regard to the patent system.<A CLASS="sdfootnoteanc" NAME="sdfootnote30anc" HREF="#sdfootnote30sym"><SUP>30</SUP></A><SUP>,</SUP><A CLASS="sdfootnoteanc" NAME="sdfootnote31anc" HREF="#sdfootnote31sym"><SUP>31</SUP></A></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">It should be
understood that it would not be a good starting position for IP
professionals to shrug the shoulders when the political debate comes
to the topic of the potential threat to OSS in view of possible legal
action by patent holders against OSS activists e.g. based on alleged
contributory infringement by means of sending OSS around the globe
using the Internet. If politicians tend to love OSS they do so due to
reasons which are well beyond the scope of particular political goals
in the IP field. Moreover, as a matter of fact, the political issues
around OSS are not severely affected by any observations regarding
the question whether or not OSS tends to be more innovative than
proprietary software. The role of OSS in the current debate on the
shape if the information society is not that of an outstanding source
of technical inventions. <I>The innovative role of OSS is simply due
to the effects of its license model.</I><A CLASS="sdfootnoteanc" NAME="sdfootnote32anc" HREF="#sdfootnote32sym"><SUP>32</SUP></A></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><I>4.        The big threat
to come</I></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">The public dispute
on patenting of software-related inventions together with the
controversy on biotechnology patenting spark and fuel other
discussions concerning the role of the current patent system in its
entirety:</P>
<UL>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">On the one
        hand, the patent system is seen as an instrument to restrict free
        competition on the market by a system of monopolies. Hence, some
        critics argue that patents should be allowed only to the extent that
        the overall economical benefits outweigh its costs. This is the
        <I>neo-liberal</I> critics of the patent system.<A CLASS="sdfootnoteanc" NAME="sdfootnote33anc" HREF="#sdfootnote33sym"><SUP>33</SUP></A><SUP>,</SUP><A CLASS="sdfootnoteanc" NAME="sdfootnote34anc" HREF="#sdfootnote34sym"><SUP>34</SUP></A><SUP>,</SUP><A CLASS="sdfootnoteanc" NAME="sdfootnote35anc" HREF="#sdfootnote35sym"><SUP>35</SUP></A><SUP>,</SUP><A CLASS="sdfootnoteanc" NAME="sdfootnote36anc" HREF="#sdfootnote36sym"><SUP>36</SUP></A><SUP>,</SUP><A CLASS="sdfootnoteanc" NAME="sdfootnote37anc" HREF="#sdfootnote37sym"><SUP>37</SUP></A><SUP>,</SUP><A CLASS="sdfootnoteanc" NAME="sdfootnote38anc" HREF="#sdfootnote38sym"><SUP>38</SUP></A></P>
</UL>
<UL>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">On the other
        hand, the current patent system is seen as an instrument of
        globalised hypercapitalism in the hands of big business. This is the
        <I>anti-capitalist</I> critics of the patent system.<A CLASS="sdfootnoteanc" NAME="sdfootnote39anc" HREF="#sdfootnote39sym"><SUP>39</SUP></A></P>
</UL>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">Obviously both kinds
of criticism taken together are quite contradictory. Nevertheless,
some activists seem to argue on both tracks. Politicians might,
depending on the stakes of their respective supporters, be readily
prepared to adopt such criticism if they can improve their own
position. Hence, it would not be very surprising if we would see an
emerging general patent debate in this decennium, not unlike the
other general patent controversy<A CLASS="sdfootnoteanc" NAME="sdfootnote40anc" HREF="#sdfootnote40sym"><SUP>40</SUP></A><SUP>,</SUP><A CLASS="sdfootnoteanc" NAME="sdfootnote41anc" HREF="#sdfootnote41sym"><SUP>41</SUP></A>
of the nineteenth century.</P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">My wish is that more
patent professionals will be well aware of the ongoing debate, in
particular on but not limited to the Internet, on the question of the
macroscopic economical benefits of the patent system. And probably it
would be a good idea to make suitable contributions to said
controversy. Otherwise, other groups will rule the field. It should
not be too much difficult to demonstrate positive overall economic
effects of granting patents on proper innovations deserving this
label. With regard to, for example, the pharmaceutical sector this
task seems to be rather trivial but perhaps the IT sector is
different. To this end, the effect of patents on the software market
should be analysed carefully.<A CLASS="sdfootnoteanc" NAME="sdfootnote42anc" HREF="#sdfootnote42sym"><SUP>42</SUP></A>
Maybe different segments of the software markets have to be treated
separately:</P>
<UL>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">Software which
        is visible <I>per se</I> on the end user market, e.g. office
        software etc., and</P>
        <LI><P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">Software which
        runs on embedded systems and which, hence, is not visible <I>per se</I>
        on any end user market but is sold only as part of other goods, e.g.
        mobile phones etc.</P>
</UL>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm">Under any
circumstances ill-considered and unworkable amendments of the
substantial conditions for patenting in Article 52 EPC further
restricting patentable subject-matters as desired by Eurolinux
Alliance should be avoided because of they would severely harm the
patent system in its entirety. Any problems caused in the context of
patents in the field of IT, in particular in conjunction with OSS,
might better be solved by carefully redesigning the effects of
granted patents.</P>
<P ALIGN=CENTER STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><B>* *
* * * * *</B></P>
<P STYLE="margin-top: 0.2cm; margin-bottom: 0cm"><BR>
</P>
<DIV ID="sdfootnote1">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote1sym" HREF="#sdfootnote1anc">1</A>The
        author's e-mail address is <A HREF="mailto:horns@ipjur.com">horns@ipjur.com</A></P>
</DIV>
<DIV ID="sdfootnote2">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote2sym" HREF="#sdfootnote2anc">2</A><B>Ralph
        Nack</B> and <B>Bruno Ph&eacute;lip</B> in &quot;AIPPI Report on the
        Diplomatic Conference for the Revision of the European Patent
        Convention&quot;; available on-line under
        <A HREF="http://www.aippi.org/reports/report-EPO-Dipl.Conf.htm">http://www.aippi.org/reports/report-EPO-Dipl.Conf.htm</A></P>
</DIV>
<DIV ID="sdfootnote3">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote3sym" HREF="#sdfootnote3anc">3</A>See
        <B>Ilkka Tuomi</B>: &quot;Internet, Innovation, and Open Source:
        Actors in the Network&quot;; on-line under
        <BR><A HREF="http://www.firstmonday.org/issues/issue6_1/tuomi/index.html">http://www.firstmonday.org/issues/issue6_1/tuomi/index.html</A></P>
</DIV>
<DIV ID="sdfootnote4">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote4sym" HREF="#sdfootnote4anc">4</A>Software
        patents stay banned in Europe - for now. By: <B>Graham Lea</B>, The
        Register, Posted: 22/11/2000 at 16:15 GMT; on-line via
        <A HREF="http://www.theregister.co.uk/content/archive/14933.html">http://www.theregister.co.uk/content/archive/14933.html</A></P>
</DIV>
<DIV ID="sdfootnote5">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote5sym" HREF="#sdfootnote5anc">5</A>See
        <B>Giorgio Di Pietro</B>, IPTS, &quot;NGOs and the Internet: Use and
        Repercussions&quot;<BR><A HREF="http://www.jrc.es/pages/iptsreport/vol48/english/TRA3E486.htm">http://www.jrc.es/pages/iptsreport/vol48/english/TRA3E486.htm</A></P>
</DIV>
<DIV ID="sdfootnote6">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote6sym" HREF="#sdfootnote6anc">6</A>EUROLINUX
        Alliance self-defining as &quot;The EuroLinux Alliance for a Free
        Information Infrastructure is an open coalition of commercial
        companies and non-profit associations united to promote and protect
        a vigourous European Software Culture based on Open Standards, Open
        Competition and Open Source Software such as Linux. Corporate
        members or sponsors of EuroLinux develop or sell software under
        free, semi-free and non-free licenses for operating systems such as
        GNU/Linux, MacOS or Windows.&quot;; available on-line under
        <A HREF="http://www.eurolinux.org/">http://www.eurolinux.org/</A>
        since June 17, 1999.</P>
</DIV>
<DIV ID="sdfootnote7">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote7sym" HREF="#sdfootnote7anc">7</A>FFII
        e.V. self-defining as &quot;Ein gemeinn&uuml;tziger Verein, in dem
        Projektgruppen f&uuml;r GNU/Linux, FreeBSD, Java,
        Schnittstellenspezifikationen, Normen, Lexika, Enzyklop&auml;dien,
        Fonts und sonstige gemeinn&uuml;tzige Informationswerke arbeiten
        k&ouml;nnen. Gemeinn&uuml;tzigkeit macht unsere Satzung an Merkmalen
        wie Schnittstellenoffenheit, Quellenoffenheit und freie
        Verf&uuml;gbarkeit fest.&quot; See on-line under
        <A HREF="http://www.ffii.org/">http://www.ffii.org/</A></P>
</DIV>
<DIV ID="sdfootnote8">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote8sym" HREF="#sdfootnote8anc">8</A>AFUL
        self-defining as &quot;L'AFUL est l'Association Francophone des
        Utilisateurs de Linux et des logiciels libres. C'est une association
        loi de 1901 dont l'objectif principal est de promouvoir, directement
        ou indirectement, les logiciels libres et en particulier les
        syst&egrave;mes d'exploitation libres, principalement ceux bas&eacute;s
        sur les normes POSIX ou d&eacute;riv&eacute;es, dont le plus connu
        est le syst&egrave;me Linux muni de l'environnement GNU (article 2
        des status).&quot; See on-line under <A HREF="http://www.aful.org/">http://www.aful.org/</A></P>
</DIV>
<DIV ID="sdfootnote9">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote9sym" HREF="#sdfootnote9anc">9</A>APRIL
        self-defining as &quot;L'Association pour la Promotion et la
        Recherche en Informatique Libre (APRIL) est une association &agrave;
        but non lucratif [...]. L'association a pour objet d'engager toute
        action susceptible d'assurer la promotion, le d&eacute;veloppement,
        la recherche et la d&eacute;mocratisation de l'informatique libre.
        [...]&quot; See on-line under <A HREF="http://www.april.org/">http://www.april.org/</A></P>
</DIV>
<DIV ID="sdfootnote10">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote10sym" HREF="#sdfootnote10anc">10</A>&quot;Contro
        i brevetti del software - L'uso dei brevetti &egrave; in crescita
        nel mondo. Gli uffici dei brevetti sono sempre pi&ugrave; pronti ad
        accettare nuove forme di monopolio. Adesso si sta aggiungendo la
        pratica di rilasciare brevetti sul software, che render&agrave; la
        vita molto difficile agli sviluppatori di software libero.&quot;
        <A HREF="http://no-patents.prosa.it/index.en.html">http://no-patents.prosa.it/index.en.html</A></P>
</DIV>
<DIV ID="sdfootnote11">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote11sym" HREF="#sdfootnote11anc">11</A>See
        the list of sponsors under
        <A HREF="http://petition.eurolinux.org/signatures.html">http://petition.eurolinux.org/signatures.html</A></P>
</DIV>
<DIV ID="sdfootnote12">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote12sym" HREF="#sdfootnote12anc">12</A><A HREF="http://petition.eurolinux.org/index_html?LANG=en">http://petition.eurolinux.org/index_html?LANG=en</A></P>
</DIV>
<DIV ID="sdfootnote13">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote13sym" HREF="#sdfootnote13anc">13</A>An
        archive of this e-mail discussion list is available under
        <A HREF="http://swpat.ffii.org/archive/mails/swpat/">http://swpat.ffii.org/archive/mails/swpat/</A></P>
</DIV>
<DIV ID="sdfootnote14">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote14sym" HREF="#sdfootnote14anc">14</A>A
        corresponding e-mail discussion list maintained by AFUL is available
        under<BR><A HREF="http://www.aful.org/pipermail/patents/">http://www.aful.org/pipermail/patents/</A></P>
</DIV>
<DIV ID="sdfootnote15">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote15sym" HREF="#sdfootnote15anc">15</A><A HREF="http://swpat.ffii.org/vreji/pikta/index.en.html">http://swpat.ffii.org/vreji/pikta/index.en.html</A></P>
</DIV>
<DIV ID="sdfootnote16">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote16sym" HREF="#sdfootnote16anc">16</A><A HREF="http://www.fitug.de/debate/0012/msg00035.html">http://www.fitug.de/debate/0012/msg00035.html</A></P>
</DIV>
<DIV ID="sdfootnote17">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote17sym" HREF="#sdfootnote17anc">17</A>Literally
        quoted text portions; English language grammar problems also in the
        original website.</P>
</DIV>
<DIV ID="sdfootnote18">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote18sym" HREF="#sdfootnote18anc">18</A><A HREF="http://swpat.ffii.org/news/patg2C/indexen.html">http://swpat.ffii.org/news/patg2C/indexen.html</A></P>
</DIV>
<DIV ID="sdfootnote19">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote19sym" HREF="#sdfootnote19anc">19</A><A HREF="http://swpat.ffii.org/stidi/eurili/indexen.html">http://swpat.ffii.org/stidi/eurili/indexen.html</A></P>
</DIV>
<DIV ID="sdfootnote20">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote20sym" HREF="#sdfootnote20anc">20</A><A HREF="http://swpat.ffii.org/xatra/patg2C/indexen.html">http://swpat.ffii.org/xatra/patg2C/indexen.html</A></P>
</DIV>
<DIV ID="sdfootnote21">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote21sym" HREF="#sdfootnote21anc">21</A>See
        e.g. the classical definition under
        <A HREF="http://www.gnu.org/philosophy/free-sw.html">http://www.gnu.org/philosophy/free-sw.html</A></P>
</DIV>
<DIV ID="sdfootnote22">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote22sym" HREF="#sdfootnote22anc">22</A><B>Steve
        Lohr</B>: &quot;Code Name: Mainstream &#150; Can 'Open Source'
        Bridge the Software Gap?&quot;; New York Times 2000-08-28; on-line
        via<BR><A HREF="http://www.nytimes.com/library/tech/00/08/biztech/articles/28code.html">http://www.nytimes.com/library/tech/00/08/biztech/articles/28code.html</A></P>
</DIV>
<DIV ID="sdfootnote23">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote23sym" HREF="#sdfootnote23anc">23</A>A
        brief discussion of the relationship of free speech and patent law
        with regard to OSS is presented in my paper &quot;Der Patentschutz
        f&uuml;r softwarebezogene Erfindungen im Verh&auml;ltnis zur &quot;Open
        Source&quot;-Software&quot;, on-line under
        <A HREF="http://www.jurpc.de/aufsatz/20000223.htm">http://www.jurpc.de/aufsatz/20000223.htm</A></P>
</DIV>
<DIV ID="sdfootnote24">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote24sym" HREF="#sdfootnote24anc">24</A>See
        e.g. <B>Bernd Lutterbeck et al.</B>: &quot;Sicherheit in der
        Informationstechnologie und Patentschutz f&uuml;r Software-Produkte
        - Ein Widerspruch?&quot;; on-line available under
        <BR><A HREF="http://www.sicherheit-im-internet.de/download/Kurzgutachten-Software-patente.pdf">http://www.sicherheit-im-internet.de/download/Kurzgutachten-Software-patente.pdf</A></P>
</DIV>
<DIV ID="sdfootnote25">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote25sym" HREF="#sdfootnote25anc">25</A><B>Kristian
        K&ouml;hntopp et al.</B>: &quot;Sicherheit durch Open Source?
        Chancen und Grenzen&quot;; Datenschutz und Datensicherheit (DuD)
        9/2000 pp. 508-513</P>
</DIV>
<DIV ID="sdfootnote26">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote26sym" HREF="#sdfootnote26anc">26</A><B>Brendan
        I. Koerner</B>: &quot;The World's Most Secure Operating System&quot;;
        on-line
        via<BR><A HREF="http://www.thestandard.com/article/display/0,1151,17541,00.html">http://www.thestandard.com/article/display/0,1151,17541,00.html</A></P>
</DIV>
<DIV ID="sdfootnote27">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote27sym" HREF="#sdfootnote27anc">27</A>See
        e.g. &quot;European working group on libre software&quot;; on-line
        under <A HREF="http://eu.conecta.it/">http://eu.conecta.it/</A></P>
</DIV>
<DIV ID="sdfootnote28">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote28sym" HREF="#sdfootnote28anc">28</A>For
        example, the Berlin Open Source Software Competence Center
        (http://www.berlios.de) is co-funded by the German Secretary for
        Economical Affairs; cf.
        on-line<BR><A HREF="http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2&amp;tsid=162&amp;tdid=553&amp;page=0">http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2&amp;tsid=162&amp;tdid=553&amp;page=0</A></P>
</DIV>
<DIV ID="sdfootnote29">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote29sym" HREF="#sdfootnote29anc">29</A>There
        is a further implication which is utmost sensitive and discussed
        behind closed doors, if so at all. The European economy as well as
        the public sector strongly depend on PC operating systems and office
        software from a single quasi monopolist vendor seated in the U.S.
        What if this vendor would voluntarily or under pressure co-operate
        with the competent U.S. Government Agencies to implement hidden
        functions into the closed-source software? It is clear that no
        official statements exist in this regard. But, not very surprising,
        the public is willing to make its own thoughts on every suspicious
        fact which has come to the surface; c.f. e.g. <B>Thomas Greene</B>:
        &quot;Microsoft collaborating with US spymasters&quot;, posted:
        05/09/1999 at 09:26 GMT, on-line
        under<BR><A HREF="http://www.theregister.co.uk/content/archive/6598.html">http://www.theregister.co.uk/content/archive/6598.html</A><BR>Taken
        these reported facts for granted, European politicians wold have no
        other choice than to establish a system for ensuring production of
        inspectable software which cannot be controlled by foreign powers.
        From this point of view, OSS policy has some contact points with
        national and regional security and economical well-being.</P>
</DIV>
<DIV ID="sdfootnote30">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote30sym" HREF="#sdfootnote30anc">30</A><A HREF="http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2&amp;tsid=212&amp;tdid=88&amp;page=0">http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2&amp;tsid=212&amp;tdid=88&amp;page=0</A></P>
</DIV>
<DIV ID="sdfootnote31">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote31sym" HREF="#sdfootnote31anc">31</A><A HREF="http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2&amp;tsid=212&amp;tdid=588&amp;page=0">http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2&amp;tsid=212&amp;tdid=588&amp;page=0</A></P>
</DIV>
<DIV ID="sdfootnote32">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote32sym" HREF="#sdfootnote32anc">32</A>Nevertheless,
        caution should be exercised when analysing statements like that of
        <B>Linus Thorwalds</B> downplaying technical innovations in the
        context of the LINUX operating system based e.g. on an interview
        &quot;Linus Thorwalds: 'Der Markt ist ein strenger Lehrer'&quot; in:
        Computerwoche 41/2000, pp. 9-10: &quot;Technisch ist in Linux kaum
        etwas radikal Neues. Was Betriebssysteme heute machen, ist im
        Wesentlichen alles schon in den 60er Jahren entworfen worden&quot;.
        It is most probably common sense among members of the LINUX
        developer community that patent offices exhibit bad practise by
        allowing patenting of obvious subject-matters. Their view might be
        that an <FONT FACE="Arial, sans-serif">&raquo;</FONT>Innovation<FONT FACE="Arial, sans-serif">&laquo;</FONT>
        in a more rigid and preferred sense is a much more dramatic and rare
        occurrence than the small <FONT FACE="Arial, sans-serif">&raquo;</FONT>sequential<FONT FACE="Arial, sans-serif">&laquo;</FONT>
        inventions which are everyday business in the IP world. Hence, maybe
        that Thorwalds' statement does <I>not</I> mean that e.g. the LINUX
        kernel doesn't comprise patentable inventions in the ordinary
        meaning of the daily IP routine work.</P>
</DIV>
<DIV ID="sdfootnote33">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote33sym" HREF="#sdfootnote33anc">33</A>See,
        for example, <B>Brian Kahin</B>: &quot;The Expansion of the Patent
        System: Politics and Political Economy&quot;; on-line via
        <A HREF="http://www.firstmonday.org/issues/issue6_1/kahin/index.html">http://www.firstmonday.org/issues/issue6_1/kahin/index.html</A></P>
</DIV>
<DIV ID="sdfootnote34">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote34sym" HREF="#sdfootnote34anc">34</A>Also
        <B>Pierre Desrochers</B>: &quot;Le March&eacute; Libre: The Case
        against the Patent System&quot;; on-line
        via<BR><A HREF="http://www.quebecoislibre.org/000902-3.htm">http://www.quebecoislibre.org/000902-3.htm</A></P>
</DIV>
<DIV ID="sdfootnote35">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote35sym" HREF="#sdfootnote35anc">35</A>Also
        <B>Markus Krummenacker</B>: &quot;Are 'Intellectual Property Rights'
        Justified?&quot;; on-line
        via<BR><A HREF="http://www.n-a-n-o.com/ipr/extro2/extro2mk.html">http://www.n-a-n-o.com/ipr/extro2/extro2mk.html</A></P>
</DIV>
<DIV ID="sdfootnote36">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote36sym" HREF="#sdfootnote36anc">36</A>Also
        <B>Francois Rene Rideaux</B>: &quot;Patents Are An Economic
        Absurdity&quot;; on-line
        via<BR><A HREF="http://fare.tunes.org/articles/patents.html">http://fare.tunes.org/articles/patents.html</A></P>
</DIV>
<DIV ID="sdfootnote37">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote37sym" HREF="#sdfootnote37anc">37</A>Also
        a report in the daily newspaper <B>Frankfurter Allgemeine Zeitung</B>
        on 2000-11-20, page 19: &quot;Mehr Markt soll die Politik in ihre
        Schranken verweisen&quot;. This article says that on the annual
        general meeting of the Mont P&egrave;lerin Society, a global
        organisation of liberal movement, the patent system in general was
        heavily under attack, in particular by Mr. <B>J&eacute;sus Huerta de
        Soto</B> of the Universidad Complutense, Madrid.</P>
</DIV>
<DIV ID="sdfootnote38">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote38sym" HREF="#sdfootnote38anc">38</A>Also
        <B>James Bessen</B>, <B>Eric Maskin</B>: &quot;Sequential
        Innovation, Patents, and Imitation.&quot; Working Paper. On-line
        available via<BR><A HREF="http://www.researchoninnovation.org/patent.pdf">http://www.researchoninnovation.org/patent.pdf</A></P>
</DIV>
<DIV ID="sdfootnote39">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote39sym" HREF="#sdfootnote39anc">39</A>This
        position plays a significant role within the context of the anti-WTO
        activities held in Seattle in 1999 and later on elsewhere. See for
        example <B>&quot;Peoples' Global Action against 'Free' Trade and the
        WTO&quot;</B>, Issue number 4, October 1999, on-line
        under<BR><A HREF="http://www.users.globalnet.co.uk/~firstcut/pgab4.html">http://www.users.globalnet.co.uk/~firstcut/pgab4.html</A><BR>&quot;Activists
        from diverse groups and movements around the world are discussing,
        networking and organising for an International Day of Action on
        November 30th. On this day, ministers of 134 governments will be in
        Seattle for the 3rd conference of the World Trade Organisation
        (WTO), at which they will decide on new policies that will further
        escalate the exploitation of our planet and its people by the global
        capitalist system. The 'key players' (the Northern governments,
        especially those of the USA and the European Union) want to [...]
        strengthen intellectual property rights and patents on life, and
        further capitalist globalisation through a new round of free trade
        talks.&quot;</P>
</DIV>
<DIV ID="sdfootnote40">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote40sym" HREF="#sdfootnote40anc">40</A>See
        <B>F. Machlup</B> and <B>E. Penrose</B>: &quot;The Patent
        Controversy in the Nineteenth Century.&quot; J. Econ. Hist. 10
        (1950), p.1, 16
        </P>
</DIV>
<DIV ID="sdfootnote41">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote41sym" HREF="#sdfootnote41anc">41</A>See
        also <B>Wolfgang Bernhardt</B>, <B>Rudolf Krasser</B>: &quot;Lehrbuch
        des Patentrechts&quot;; M&uuml;nchen: C.H. Beck, 4<SUP>th</SUP>
        edition, 1986, pp. 52 &#150; 54.</P>
</DIV>
<DIV ID="sdfootnote42">
        <P CLASS="sdfootnote"><A CLASS="sdfootnotesym" NAME="sdfootnote42sym" HREF="#sdfootnote42anc">42</A>The
        German Secretary for Economic Affairs has launched an empirical
        investigation in December 2000 to be carried out by the &quot;Institut
        f&uuml;r Systemtechnik und Innovationsforschung&quot; (ISI) of the
        Fraunhofer Gesellschaft (FhG). The results are to be expected during
        summer 2001.</P>
</DIV>
<DIV TYPE=FOOTER>
        <P ALIGN=RIGHT STYLE="margin-top: 0.51cm; margin-bottom: 0cm"><BR>
        </P>
</DIV>

<HR SIZE=1>
<br>
</FONT> <br>
<table WIDTH="100%" border="0" cellspacing="0" cellpadding="0" BGCOLOR="#FFFFFF">
<tr><td colspan=2><FONT FACE="Arial" SIZE="-2">
Please read the <A HREF="./disclaimer.php3">Disclaimer &amp; About This Website (Pflichtangaben gem&auml;ss TDG)</a> section.<br>&nbsp;<br>
Feel free to contact PA Axel H Horns via e-mail <A href="mailto:horns@ipjur.com">horns@ipjur.com</a>. BEWARE: DO NOT SEND CONFIDENTIAL INFORMATION UNENCRYPTED VIA E-MAIL. USE OF ENCRYPTION SOFTWARE IS HIGHLY RECOMMENDED. PA AXEL H HORNS IS PROVIDING SUPPORT FOR ENCRYPTED E-MAIL MESSAGES USING PGP OR PGP COMPATIBLE FORMATS. THE PGP PUBLIC KEY FOR PA AXEL H HORNS IS AVAILABLE <A HREF="./pubkey.php3">HERE</a>. THE GnuPG PUBLIC KEY FOR PA AXEL H HORNS IS AVAILABLE <A HREF="./gnupg_pubkey.php3">HERE</a>.<br>&nbsp;<br>

<A HREF="./ahh.php3">Dipl.-Phys. Axel H Horns</a> is Patentanwalt (German Patent Attorney),
European Patent Attorney as well as European Trade Mark Attorney. In particular, he is Member of:</FONT><br><br>
</td>
</tr>
<tr>
 <td><a href="http://www.scl.org"><img src="./logo-new-296x37.gif" width="296" height="37" border="0"
 alt="Click here to visit the SCL Online web site" align="center"></a>
 </td>
 <td><a href="http://www.cla.org"><img src="./claclr2.jpg" width="75" height="75" border="0"
 alt="Click here to visit the CLA Online web site" align="center"></a>
</td>
</tr>
</table>
</font>
</td>
</tr>
</TABLE></BODY>
</HTML>