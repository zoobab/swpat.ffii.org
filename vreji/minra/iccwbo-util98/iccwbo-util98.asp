
<HTML>
<HEAD>
<TITLE>Proposal for a European Parliament and council directive</TITLE>

<script language="javascript">
<!--
// Navigateur < 4 
function fixmenu()
{
return true;
} 
//-->
</script>
 
</HEAD><body  bgcolor="#FFFFFF" BGPROPERTIES="fixed" link="#003F80" vlink="#003F80" alink="#003F80">
<p>
<P>
<IMG SRC="/library/banner720.gif" ALT="ICC" BORDER=0 ALIGN=bottom VSPACE=5 usemap="#TopICC"><BR> 
<TABLE BORDER=0 CELLSPACING=0 WIDTH=720>
  <TR> 
    <TD WIDTH=720> <CENTER>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="720" height="80">
          <param name="movie" value="/library/TopIcon_Menu/TopIcon_Menu.swf?uncacher=5/17/03 10:01:48 PM">
          <param name="quality" value="high">
          <embed src="/library/TopIcon_Menu/TopIcon_Menu.swf?uncacher=5/17/03 10:01:48 PM" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="720" height="80"></embed></object>
      </CENTER></TD>
  </TR>
 
</TABLE>
<map name="TopICC"> <area shape="rect" coords="574,48,613,62" href="/index.asp"  alt="ICC Home" TITLE="ICC Home"> 
<area shape="rect" coords="616,48,671,62" href="/home/menu_news_archives.asp"  alt="News Archive" TITLE="News Archive"> 
<area shape="rect" coords="672,48,721,62" href="/search/query.asp"  alt="Search" TITLE="Search"> 
</map>
 <table border="0" cellPadding="0" cellSpacing="0" width="720"> 
<tr> <td colspan="5" valign="top" align="center"></td></tr> 
<tr> <td height="1" colspan="5" valign="top" align="center" ><img src="/home/images/spot.gif" width="1" height="1"></td></tr> 
<tr> <td height="5" colspan="5" valign="top" align="center"></td></tr> 
<tr> <td width="180" vAlign="top"> <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="180" height="650" VISIBILITY:VISIBLE; ZINDEX:1>
  <param name="movie" value="/library/LeftMenu.swf?v=6">
  <param name="quality" value="high">
  <param name="menu" value="false">
  
  <embed src="/library/LeftMenu.swf?v=6" width="180" height="650" menu=false quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed></object>
 </td><td width = "4">&nbsp;</td><td width = "1" bgcolor="#EE9430" valign = "top"> 
<img height="5" width="1" src="/home/images/spot.gif"></td><td width = "4">&nbsp;</td><td width = "531" vAlign="top" align="left"> 
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
  <script language=JavaScript>
<!--
//-->
		
</script>
<p> <b><font face="Arial, Helvetica, sans-serif" size="2">Policy statement</font> 
  </b> </p>
<p><font size="2" face="Arial, Helvetica, sans-serif"><font size="4">Proposal 
  for a European Parliament and Council directive approximating the legal arrangements 
  for the protection of inventions by utility model</font><b><br>
  </b><font color="#EE9430">Commission on Intellectual and Industrial Property, 
  21 October 1998</font></font></p>
<p> <font face="Arial, Helvetica, sans-serif" size="2">The International Chamber 
  of Commerce (ICC) is the world business organization. It is the only representative 
  body that speaks with authority on behalf of both large and small enterprises 
  from all sectors in every parts of the world. Founded in 1919, it represents 
  today thousands of member companies and associations from over 130 countries. 
  ICC&#146;s purpose is to promote international trade, investment and the market 
  economy system. It makes rules that govern the conduct of business (eg. Incoterms) 
  across borders and provides essential services, foremost among them being the 
  ICC International Court of Arbitration.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">Business leaders and experts 
  drawn from ICC membership establish the business stance on global issues of 
  importance for business such as intellectual property. Since 1922, ICC policy 
  in this field has been elaborated by its Commission on Intellectual and Industrial 
  Property, which brings together leading intellectual property experts from business 
  and private practice from all over the world. As the world business organization, 
  ICC firmly believes that the protection of intellectual property stimulates 
  international trade and investment, and encourages transfer of technology, which 
  are both essential for economic growth.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">ICC has always campaigned 
  for the provision of strong world-wide cost-effective non-discriminatory intellectual 
  property systems as being an essential requirement for the world business community. 
  As part of this campaign, ICC has supported all efforts to harmonize national 
  intellectual property laws because such harmonization is beneficial to world 
  business by, for example, reducing the cost of obtaining intellectual property 
  rights and then enforcing them against infringers. </font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">The ICC campaign has been 
  directed in particular towards the protection of innovative technology by patents 
  and similar rights. This is because ICC firmly believes that companies that 
  do innovate technology should be able to obtain and enforce quickly, cheaply 
  and without aggravation the intellectual property rights protecting such technology. 
  Most importantly, the potency of the rights so provided should, at the same 
  time, always be commensurate with the contribution made by the innovation. Further, 
  a third party wishing to commercialize its own technology must also be able 
  to determine quickly, cheaply and without aggravation whether it is free to 
  work that technology as far as intellectual property rights belonging to competitors 
  are concerned. </font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">In consequence, any intellectual 
  property system must, if it is to be acceptable to world business, maintain 
  a fair balance between these two factors. Any proposals for harmonizing national 
  intellectual property laws in Europe or anywhere else will therefore be carefully 
  scrutinised by ICC. If such harmonization would result in a country being required 
  to introduce a completely new intellectual property right, ICC will consider 
  most carefully whether such a right would be of overall benefit to the world 
  business community.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">In view of the above, ICC 
  is naturally most interested in the European Commission&#146;s proposed Directive 
  on utility models published late last year. Many of the submissions the Commission 
  has already received, and will receive in the future, from non-governmental 
  organizations will no doubt address the question whether the proposals are in 
  the interest of European industry. Clearly, ICC as the world business organization 
  cannot approach the subject from that angle; instead ICC will in the present 
  policy statement consider whether the proposals are, on balance, in the interest 
  of the world business community. ICC has particularly studied the proposed Directive 
  to see if the utility model system provided by the Directive, if adopted, satisfies 
  the above-mentioned fair balance test. This test should aim to achieve a balance 
  between the interests of the proprietor of a utility model, so that his or her 
  innovation has a right commensurate with the contribution made by the innovation, 
  against those of a competitor, so that it is free to work its own technology 
  when this is sufficiently distinguished from the technology protected by the 
  utility model. In addition, ICC has considered whether the introduction of a 
  new intellectual property right in three EU countries (i.e. Luxembourg, Sweden 
  and the UK) would be of overall benefit to the world business community.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">In the draft Directive, 
  the Commission is proposing a simple registration system for obtaining a intellectual 
  property right for an innovation which, to be protectable, need not have any 
  inventive step in the patent sense. Thus the right will be obtainable for any 
  innovation which &quot;exhibits either (a) particular effectiveness in terms 
  of, for example, ease of application or use; or (b) a practical or industrial 
  advantage&quot; (Article 6 of the draft Directive). This means that the right 
  will be obtainable for any innovation which is novel over the prior art and 
  has some advantage over it even though the innovation may be completely obvious 
  to the skilled person in the art. The application for the right will not be 
  searched nor examined by the patent office receiving the application except 
  for certain specific formal issues, and in particular it will not be examined 
  to establish that the innovation is capable of protection. The draft Directive 
  is silent on the potency of the right, once obtained, in each EU country, so 
  it will presumably have, in the absence of express enforcement provisions in 
  the Directive, the same effect as a patent in that country. For example, a right 
  holder facing a competitor commercializing the same or similar technology may 
  be able to stop the competitor from doing this through injunction (which could 
  even be of a preliminary nature) and to extract damages from the competitor. 
  </font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">ICC believes that the harmonized 
  utility model system proposed in the draft Directive does not provide the necessary 
  fair balance between the innovator, on the one hand, and third parties on the 
  other. The system as at present proposed is not therefore in the interests of 
  the world business community. However, if the system were modified to increase 
  the entry threshold for obtaining the right and/or reducing the potency of the 
  right once obtained, ICC might be able to support a system so modified.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">ICC will not be commenting 
  at this stage on the detail of the system proposed in the draft Directive but 
  will wish to do this later in the process of gaining adoption of the Directive.</font></p>
<p> <font face="Arial, Helvetica, sans-serif" size="2">&nbsp;<b>Document n� 450/878</b></font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="2"><a href="/home/menu_statements_rules.asp">Back 
  to statements and rules</a><br>
  <a href="/home/statements_rules/menu_statements.asp">Back to statements</a></font><br>
  </b> </font></p>
</td></tr></table>
<map name="MapNavBar">
<area shape="rect" coords="41,0,104,15" href="/home/intro_icc/introducing_icc.asp" alt="About ICC"> 
<area shape="rect" coords="544,0,596,15" href="/home/menu_news_archives.asp" alt="News Archives"> 
<area shape="rect" coords="468,0,534,15" href="http://www.iccbooks.com" target="_blank" alt="Bookstore"> 
<area shape="rect" coords="293,0,464,15" href="/index_ccs.asp" alt="CCS"> <area shape="rect" coords="110,0,151,15" href="/search/query.asp" alt="Search"> 
<area shape="rect" coords="2,0,35,15" href="/index.asp" alt="Home site"> </map> 
</body>
<script language=JavaScript>
<!--
var model = 2 ;
var menug =  0;
var menub =  0;
//-->
</script>
</HTML>
