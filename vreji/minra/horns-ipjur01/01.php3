<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN"
"http://www.w3.org/TR/REC-html40/strict.dtd">
<html>
<head>
<title>Patentanwalt [Patent Attorney] Axel H. Horns on Patents on Computer-Implemented Inventions</title>
<META name="AUTHOR" content="Axel H Horns">
<META name="DESCRIPTION" content="Relevant information about intellectual property law">
<META name="KEYWORDS" content="patents, software, intellectual property, patent attorney, patent office, law, SWPAT, prosecution, litigation, Germany, Europe, programming languages, syntax, grammar, text, algorithms">
</head>
<body text="#000000" bgcolor="#FFFFFF" link="#000000" alink="#FF0000" vlink="#FF0000">

<TABLE WIDTH="100%" border="0" cellpadding="0" cellspacing="0" BGCOLOR="#A0D7FF">
<TR BGCOLOR="#A0D7FF">
    <TD ALIGN="LEFT" VALIGN="CENTER" WIDTH="150" BGCOLOR="#A0D7FF">
    <TABLE BORDER="0" WIDTH="150" cellpadding="0" cellspacing="0">
    <TR BGCOLOR="#000000"><TD>
    <A HREF="http://www.ipjur.com/"><IMG SRC="./ipjur.gif" border="0" hspace="0" vspace="0" HEIGHT="40" WIDTH="150" align="middle"></a>
    </TD></TR>
    </TABLE>
    </TD>
    <TD ALIGN="CENTER" VALIGN="CENTER" colspan="4">
    <IMG SRC="./iplaw.gif" border="0" hspace="0" vspace="0" HEIGHT="40" align="middle">
        </TD>
</TR>
<TR BGCOLOR="#000000">

    <TD HEIGHT="18" WIDTH="20%" ALIGN="LEFT" VALIGN="BOTTOM">
        &nbsp;
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="LEFT" VALIGN="BOTTOM">
        <A HREF="./00.php3"><IMG SRC="./arrow2.gif" ALT="NEWS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./news1.gif" ALT="NEWS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./02.php3"><IMG SRC="./arrow2.gif" ALT="I2P INFO" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./i2pa.gif" ALT="I2P" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" ALIGN="CENTER" VALIGN="BOTTOM">
        <A HREF="./03.php3"><IMG SRC="./arrow2.gif" ALT="MOVING SPOTS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./moving.gif" ALT="MOVING SPOTS" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>

    <TD HEIGHT="18" WIDTH="20%" VALIGN="BOTTOM" ALIGN="RIGHT">
        <A HREF="./04.php3"><IMG SRC="./arrow2.gif" ALT="PRACTICE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="16"><IMG SRC="./practice.gif" ALT="PRACTICE" BORDER=0 hspace="0" vspace="0" HEIGHT="16" WIDTH="100"></A>
    </TD>
</TR>
</TABLE>

<TABLE WIDTH="100%" CELLPADDING="0" cellspacing="0" BORDER="0">
<TR>
<TD WIDTH="150" ALIGN="LEFT" VALIGN="TOP" BGCOLOR="#A0D7FF">

<TABLE BORDER=0 CELLSPACING=0 CELLPADDING="3" BGCOLOR="#A0D7FF">
<TR>
    <TH COLSPAN="2" BGCOLOR="#A0D7FF">
        <B><FONT FACE="ARIAL" COLOR="#000000" SIZE="-1">
        <BR>EXTERNAL LINKS<BR>[OFFSITE]</FONT></B>
    </TH>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.wto.org/wto/eol/e/pdf/27-trips.pdf"><IMG SRC="./arrow.gif"
        ALT="WORLD TRADE ORGANISATION" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wto.org/">WTO</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wto.org/english/docs_e/legal_e/27-trips.pdf"><IMG SRC="./arrow.gif"
        ALT="AGREEMENT ON TRADE-RELATED APSECTS OF INTELLECTUAL PROPERTY RIGHTS" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wto.org/english/docs_e/legal_e/27-trips.pdf">TRIPS</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.wipo.int/"><IMG SRC="./arrow.gif"
        ALT="World Intellectual Property Organisation" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/">WIPO</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/members/convention/index.html"><IMG SRC="./arrow.gif"
        ALT="WIPO Convention" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/members/convention/index.html">WIPO Convention</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/treaties/ip/paris/index.html"><IMG SRC="./arrow.gif"
        ALT="Paris Convention" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/treaties/ip/paris/index.html">WIPO Paris Convention</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/treaties/ip/berne/index.html"><IMG SRC="./arrow.gif"
        ALT="Berne Convention" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/treaties/ip/berne/index.html">WIPO Berne Convention</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/pct/en/index.html"><IMG SRC="./arrow.gif"
        ALT="Patent Cooperation Treaty (PCT)" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/pct/en/index.html">WIPO PCT System</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/madrid/en/index.html"><IMG SRC="./arrow.gif"
        ALT="Madrid Agreement on International Registration of Trade Marks" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/madrid/en/index.html">WIPO Madrid System</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/hague/en/index.html"><IMG SRC="./arrow.gif"
        ALT="Hague Agreement on Protection of Models" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/hague/en/index.html">WIPO Hague System</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.wipo.int/clea/en/index.html"><IMG SRC="./arrow.gif"
        ALT="WIPO COLLECTION OF LAWS FOR ELECTRONIC ACCESS (CLEA)" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/clea/en/index.html">WIPO CELA</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.epo.co.at/"><IMG SRC="./arrow.gif"
        ALT="European Patent Office" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.epo.co.at">EPO</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.european-patent-office.org/legal/epc/e/index.html"><IMG SRC="./arrow.gif"
        ALT="European Patent Convention" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.european-patent-office.org/legal/epc/e/index.html">EPO EPC</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.epo.co.at/espacenet/info/access.htm"><IMG SRC="./arrow.gif"
        ALT="esp@acenet" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.epo.co.at/espacenet/info/access.htm">esp@acenet DATABASE</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://oami.eu.int/en/"><IMG SRC="./arrow.gif"
        ALT="OHIM" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://oami.eu.int/en/">OHIM</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://oami.eu.int/en/aspects/reg.htm"><IMG SRC="./arrow.gif"
        ALT="EU Community Trade Mark Regulations" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://oami.eu.int/en/aspects/reg.htm">OHIM CTM LAW</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://oami.eu.int/search/trademark/la/en_tm_search.cfm"><IMG SRC="./arrow.gif"
        ALT="EU Community Trade Mark Database" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://oami.eu.int/search/trademark/la/en_tm_search.cfm">OHIM CTM DATA</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.dpma.de/"><IMG SRC="./arrow.gif"
        ALT="DPMA" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.dpma.de/">DPMA (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://jurcom5.juris.de/bundesrecht/patg/index.html"><IMG SRC="./arrow.gif"
        ALT="DE Patent Act (GER)" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://jurcom5.juris.de/bundesrecht/patg/index.html">DE Patent ACT (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://jurcom5.juris.de/bundesrecht/gebrmg/index.html"><IMG SRC="./arrow.gif"
        ALT="DE Utility Model Act (GER)" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://jurcom5.juris.de/bundesrecht/gebrmg/index.html">DE Utility Model Act (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://transpatent.com/gesetze/gintpue.html"><IMG SRC="./arrow.gif"
        ALT="DE Act on International Patent Agreements (GER)" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://transpatent.com/gesetze/gintpue.html">DE-IntPat&Uuml;G (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://jurcom5.juris.de/bundesrecht/markeng/index.html"><IMG SRC="./arrow.gif"
        ALT="DE Trade Marks Act (GER)" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://jurcom5.juris.de/bundesrecht/markeng/index.html">DE Trade Marks Act (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="https://dpinfo.dpma.de/"><IMG SRC="./arrow.gif"
        ALT="DPINFO DATABASE (GER)" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="https://dpinfo.dpma.de/">DPINFO DATABASE (GER)</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.depatisnet.de/"><IMG SRC="./arrow.gif"
        ALT="DEPATISnet DATABASE" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.depatisnet.de/">DEPATISnet DATABASE</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.wipo.int/eng/general/links/ipo_web.htm"><IMG SRC="./arrow.gif"
        ALT="List of Patent and Trade Mark Offices with WWW site" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.wipo.int/eng/general/links/ipo_web.htm">List of Office Sites</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://www.uspto.gov/"><IMG SRC="./arrow.gif"
        ALT="United States Patent and Trade Mark Office" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.uspto.gov/">US-PTO</A>
        </FONT><BR></TD>
</TR>

<TR><TD>&nbsp;</td><TD>&nbsp;</td></tr>

<TR><TD><A HREF="http://europa.eu.int/eur-lex/en/"><IMG SRC="./arrow.gif"
        ALT="EU Commission" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://europa.eu.int/eur-lex/en/">EU Commission</A>
        </FONT><BR></TD>
 </TR>

<TR><TD><A HREF="http://europa.eu.int/eur-lex/en/oj/index.html"><IMG SRC="./arrow.gif"
        ALT="EU Official Journal" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://europa.eu.int/eur-lex/en/oj/index.html">EU Official Journal</A>
        </FONT><BR></TD>
</TR>

<TR><TD><A HREF="http://www.ipr-helpdesk.org/EN/default.htm"><IMG SRC="./arrow.gif"
        ALT="EU IPR Helpdesk" BORDER=0
        HEIGHT=13 WIDTH=15></A></TD>
    <TD><FONT FACE="Arial" SIZE="-2">
        <A HREF="http://www.ipr-helpdesk.org/EN/default.htm">EU IPR Helpdesk</A>
        </FONT><BR></TD>
</TR>

<TR>
<TD>&nbsp;</TD>
</TR>
</TABLE>

<!-- /news-left -->
<FONT SIZE="-2" FACE="Arial">
Updated: <!-- last-modified--> 2001-07-23
<!-- /last-modified--><BR>
</FONT>

<!-- common-footer -->
<FONT FACE="ARIAL" SIZE="-2">

<P><A href="mailto:horns@ipjur.com">&copy; 2001 PA Axel H Horns</A></p>
</FONT>

</TD>

<TD ALIGN="LEFT" VALIGN="TOP"> <FONT FACE="Arial" SIZE="-2">

<br>
GENERAL > SOFTWARE

<br>&nbsp;<br>

<CENTER><H3>"SOFTWARE PATENTS"</H3></CENTER>
<P>

Can software be patented?

<br>&nbsp;<br>

In short, the answer is twofold:

<ul>
<li>NO, software seen as text in a linguistical sense cannot be subject-matter of a patent claim.</li>
<li>YES, the functional ideas behind the text can be patented if they are of a technical nature, novel, inventive and industrially applicable.</li>
</ul>

In particular, software is a "legal hybrid" and has a somewhat ambiguous and janus-like nature:

<ul>
<li>On the one hand, a computer program is simply a certain sort of text written in a
<a href="http://dmoz.org/Computers/Programming/Languages/">"programming language"</a> in accordance with a particular formal grammar.</li>
<li>On the other hand, the primary source of value of a computer program is its behaviour or functionality, not its text.</li>
</ul>

There are two fields of law which are mapped to these aspects, respectively:

<ul>
<li>From a copyright-centred point of view, a computer program is seen as a text.</li>
<li>From a patent-centred point of view, a computer program is seen as an expression of functionality or of dynamic semantics described by operational and/or denotational semantics, i.e. it is viewed as an expression of run-time behaviour.</li>
</ul>

However, the run-time behaviour of a certain computer program cannot be deduced from its text alone. The dynamic semantics of the text largely depends on the processor which is assigned to execute the program. In fact, text and behaviour are largely independent from each other [01]:

<ul>
<li>One and the same functionality can be expressed by different texts. For example, the texts of computer programs implementing the RSA asymmetric cryptography algorithm can look quite different when written in C++ or Java. Even when only a single programming language is used, many different texts representing RSA are conceivable.</li>
<li>One and the same text can cause many different behaviours if run on different processors. For example, a program carefully written in C++ and implementing said RSA algorithm will perform RSA if run on a C++ processor, e.g. a general purpose computer equipped with a C++ compiler. If the same C++ text is run on another processor, say, on a general purpose computer equipped with a JAVA interpreter or with a PERL interpreter, other things will happen (usually an error message will be generated).</li>
</ul>

So, because the behaviour of a computer program is largely independent from its text, we recognise that the code text of a computer program taken alone is always insufficient for discussions of a computer program behaviour in the context of patent law:

<ul>
<li>It would be a rather silly idea to include the text of the code of some computer software into a patent claim. Doing so would not unambiguously determine a functional behaviour - unless the matching processor is also specified in the claim language. However, this would be a strange practice. If a piece of software comprises an invention, the normal way of patenting the same would be to express the inventive functionality thereof in words and expressions of natural language. Of course, this natural language might be - or should be - an expert's language, not that of laymen. </li>
<li>When it comes to patent litigation after a patent has been granted, evidence might have to be taken that a certain piece of computer program code text sold by an alleged infringer in fact reproduces the functional behaviour as defined by the claim. This can be done only by watching the behaviour after having matched the captured computer program code text with the appropriate processor. In terms of proper application of technical concepts, no text taken as such can ever directliy, i.e. more than in a contributary manner, infringe a patent claim. Hence, dealing with the pure text of the code of a computer program (be it source code or binary code) should - in terms of proper understanding of the underlying concepts - never be considered to be be more than an act of contributory infringement because of for proper determination of the infringing functional behaviour the assignment of a matching processor is always necessary. However, no case law has come to be known up to now clarifying any positions with regard to these matters.</li>
</ul>

The above conclusion is the very reason why the term "software patent" is imprecise. It should be replaced by "patent on a computer-implemented or computer-implementable invention".  And, because hardware and software are widely interchangeable, there are many inventions which can be implemented by means of a computer plus software as well as by hardware alone. For example, the IDEA symmetric cipher algorithm can be implemented by software on a general purpose PC as well as by a dedicated integrated circuit. Hence, the invention of performing data scrambling in accordance with the IDEA algorithm resides in an area of ambivalence in view of the answer to the question whether this is an ordinary brick-and-mortar invention or a software invention. In fact, it is none of them or both.

<br>&nbsp;<br>

Why all that <A href="http://petition.eurolinux.org/index.en.html">public controversy</a> about "Software Patents"?

<ul>
<li>One aspect is the question of the technical character of an invention. Generally, patents are granted solely for <b>technical</b> inventions. But are computer programs technical?</li>
<li><A href="http://www.european-patent-office.org/legal/epc/e/ar52.html">Article 52</a> of the
<A href="http://www.european-patent-office.org/legal/epc/e/ma1.html">
European Patent Convention</a> says that, inter alia, programs for computers "as such" shall not be regarded as patentable inventions - probably because of they have been deemed to be a statuary example of non-technical subject-matter. But what are "programs for computers as such" in real life? No significant case law has clarified this concept up to now.</li>
<li>The overall impressions is that policymakers in the 60ies an 70ies of the XX. century who drafted Article 52 of the EPC and other similar clauses of national patent law in various countries were ill-advised or did otherwise not completely understand the nature of computer software. The term "programs for computers as such" is practically unworkable and does not provide help to anybody when undertaking interpretation of the law.</li>
<li>Taking into consideration the fact that computer program code text only in conjunction with some kind of a processor - which must be or at least reside on a piece of hardware - can be relevant in the context of patent law, it seems to be clear that the energy dissipation of the hardware processor in any case causes the respective invention to be of technical nature. However, the technical nature of software-related computer-implemented or at least computer-implementable inventions is seen differently by the case law in various countries or the EPO. At the time being the EPO seems not to be prepared to accept the power dissipation of the processor as a basis for a technical nature of claimed subject-matter. The German Federal Supreme Court (Bundesgerichtshof, BGH) seems to be more generous in this respect.</li>
<li>Currently in particular activists from the field of Open Source Software (OSS) are extremely worried about increasing grants of patents on computer-implemented or computer-implementable inventions. They are not prepared to participate in the patent business due to lack of money or due to other reasons. Some of them argue that grant of patents on computer-implementable inventions e.g. by the EPO is contrary to Article 52 EPC and, hence, blatantly illegal. This is, however, a pure ideological position. There is no provision in the EPC prohibiting grant of such patents. Nevertheless and irrespective of these ideological brawls, OSS is an important building block for safe e-commerce in particular and for a workable information society in general. Hence, all due care should be exercised to avoid destruction of the OSS scene by means of patent law. Under any circumstances any ill-considered and unworkable amendments of the substantial conditions for patenting in Article 52 EPC further restricting patentable subject-matters as desired by certain OSS lobby groups should be avoided because of they would severely harm the patent system in its entirety. Any problems caused in the context of patents in the field of IT, in particular in conjunction with  OSS, might better be solved by carefully redesigning the effects of granted patents, preserving freedom of expression when software code is exchanged over the internet as long as the computer program code isn't matched with the desired processor for production operation. To this end, the effect of patents on the software market should be analysed carefully. Maybe different segments of the software markets have to be treated separately, namely
Software which is visible per se on the end user market, e.g. office software etc., and
Software which runs in embedded systems and which, hence, is not visible per se on any end user market but is sold only as part of other goods, e.g. mobile phones etc.</li>
<li>With regard to the Member States of the European Union, final clarification of the borderline between patentable and non-patentable subject-matters in the context of computer-implemented or computer-implementable inventions is to be expected by a EU Directive forcing a harmonisation of national patent law throughout the entire EU and affecting also the EPC. Up to now, no draft has been published.</li>
</ul>

Conclusion: Because of there is no general provision in patent law prohibiting patenting of computer-implemented or computer-implementable inventions, every potential patent applicant should thoroughly consider an appropriate patent policy, be it active (applying for own patents) or passive (defending against patents of third parties). It depends on the particulars of each case whether or not preparing a patent application or undertaking a patent search makes sense.     <br><br>

Literature:<br><br>

[01] Pamela Samuelson, Randall Davis, Mitchell D. Kapor, and J.H. Reichmann: <a href="http://wwwsecure.law.cornell.edu/commentary/intelpro/manifint.htm">"A Manifesto Concerning The Legal Protection of Computer Programs"</a>; Columbia Law Review, Vol. 94 (1994), pages 2308 to 2431.

<br>&nbsp;<br>

<br>&nbsp;<br>

<CENTER><H3>THE DISPUTE ON "SOFTWARE PATENTS"</H3></CENTER>
<P>

EXTERNAL LINKS (OFFSITE):<br>
<table>
<tr>
 <td width="50%" valign="TOP">
 <FONT FACE="Arial" SIZE="-2">
<UL>
<LI><A HREF="http://europa.eu.int/comm/internal_market/en/intprop/indprop/soften.pdf">Consultation Paper of the EU Commission</A>
<LI><A HREF="http://europa.eu.int/comm/internal_market/en/intprop/indprop/softreplies.htm">Various Responses to the EU Consultation Paper</A>
<LI><A HREF="http://www.patent.gov.uk/about/ippd/consultation/closed/index.htm">
Consultation Process of the UK-PTO</A>
<LI><A HREF="http://www.patent.gov.uk/about/consultations/responses/index.htm">Various Responses to the UK-PTO Consultation Process</A>
<li><A HREF="http://europa.eu.int/comm/internal_market/en/intprop/indprop/planck.pdf">An engaged pleading in support of patents on software-related inventions from the Max-Planck-Institut f�r ausl&auml;ndisches und internationales Patent-, Urheber- und Wettbewerbsrecht, M&uuml;nchen. [In German]</A></li>
<li>The most determined lobby against the grant of patents on software-related inventions: <A HREF="http://www.eurolinux.org">The Eurolinux Coalition</a></li>
<li><A HREF="ftp://ftp.ipr-helpdesk.org/software.pdf">Your Software and how to protect it</a>. This guide is based on research by experts on
Intellectual Property Rights at the University of Sussex
and the University of Sheffield. The research was funded by Directorate General Enterprise of the European Commission.</li>
<li>A new study on <a href="ftp://ftp.ipr-helpdesk.org/softstudy.pdf">Patent protection of computer programmes</a> created under Contract no. INNO-99-04 and submitted to European Commission, Directorate-General Enterprise, by <a href="http://www.cops.ac.uk/iss/people.html#puay">Dr. Puay Tang</a>, SPRU, University of Sussex, <A href="http://www.shef.ac.uk/uni/academic/I-M/law/adams.html">Prof. John Adams</a>, University of Sheffield, <a href="http://www.internetstudies.org/members/danipar.html">Dr. Daniel Par�</a>, SPRU, University of Sussex, has been published as document ECSC-EC-EAEC, Brussels-Luxembourg, 2001, on the Internet by the EU IPR Help Desk.</li>
<li>The Commission of the European Comminities (CEC) has created
an <a href="http://www.forum.europa.eu.int/Public/irc/markt/softpat/newsgroups?n=forum">ONLINE-FORUM</a> on patenting of software-related inventions.</li>
<li>Robert A. Gehring, TU Berlin: <a href="http://ig.cs.tu-berlin.de/ap/rg/2001-10/Gehring2001full-SWPatITSec.pdf">
"'Software Patents' - IT-Security at Stake?"</a>. Has received the Best Paper Award on the conference "Innovations for an e-Society. Challenges for Technology Assessment" (October 17-19, 2001 in Berlin, Germany). This paper presents the thesis that insecurity of software is due to interaction of technological and legal shortcomings, fostered by economic rationality. Patent protection encour-ages the use of proprietary instead of standard technology. Open source software development is proposed as a starting point for a risk management strategy to im-prove the situation. Existing patent laws should therefore be modified to include a "source code privilege". </li>
<li>Full text of the study <a href="http://www.sicherheit-im-internet.de/download/softwarepatentstudie.pdf">
"Mikro- und makro�konomische Implikationen der Patentierbarkeit von Softwareinnovationen: Geistige Eigentumsrechte in der Informationstechnologie im Spannungsfeld von Wettbewerb und Innovation"</a> [In German, approx. 1.2M] of the Fraunhofer Institut f�r Systemtechnik und Innovationsforschung (FhG ISI) and the Max-Planck-Institut f&uuml;r ausl&auml;ndisches und internationales Patent-, Urheber- und  Wettbewerbsrecht (MPI);
<a href="http://www.sicherheit-im-internet.de/download/softwarepatentstudie_e.pdf">English Abstract</a></li>
</UL>
<P>
</FONT>
</td>

 <td width="50%" valign="TOP"><FONT FACE="Arial" SIZE="-2">
 <UL>
 <li>Axel H Horns, <a href="./episwpat.php3">Some Observations on the Controversy on &raquo; Software Patents &laquo;</a></li>
<LI><A HREF="http://www.jurpc.de/aufsatz/20000223.htm">Axel H. Horns, Der Patentschutz f&uuml;r softwarebezogene Erfindungen im Verh&auml;ltnis zur "Open Source"-Software (GER)</A>
<LI><A HREF="http://www.sicherheit-im-internet.de/download/Kurzgutachten-Software-patente.pdf">Lutterbeck et al.: Sicherheit in der Informationstechnologie und Patentschutz f&uuml;r Software Produkte - Ein Widerspruch? Kutzgutachten f&uuml;r das BMWi (GER)</A>
<LI><A HREF="http://europa.eu.int/comm/internal_market/en/intprop/indprop/study.pdf">
Robert Hart (Independent Consultant), Peter Holmes (School of European Studies, University of Sussex) and John Reid (IP Institute) on behalf of Intellectual Property Institute, London: The Economic Impact of Patentability of Computer Programs </A>
<LI><A HREF="http://www.researchoninnovation.org/patent.pdf">James Bessen, Eric Maskin: Sequential Innovation, Patents, and Imitation</A>
<li>On July 10, 2001 an expert workshop was held by the German Federal
Ministry for Economics and Technology in Berlin. About approximately
some 60+ attendants listened the presentation of preliminary results
of a study jointly conducted by Fraunhofer Institute for Research on
Systems and Innovation (ISI), Karlsruhe, and by Max Planck Institute
for Foreign and International Patent, Copyright and Competition Law,
Munich.  <a href="./BMWI001.php3">More ...</a> and <a href="http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2&tsid=212&tdid=1003&page=0">More ... [In German]</a></li>
<li>The Commission of the European Comminities (CEC) had awarded to PbT Consultants, London, UK, a contract for reading and summarizing the
<a href="http://europa.eu.int/comm/internal_market/en/indprop/softreplies.htm">results</a> of the <a href="http://europa.eu.int/comm/internal_market/en/indprop/softpaten.htm">EU consultation on the patentability of computer implemented inventions</a>. The
<a href="http://europa.eu.int/comm/internal_market/en/indprop/softanalyse.pdf">Final Report</a> is available.</li>
<li>Mark Aaron Paley: <a href="http://hometown.aol.com/paleymark/ModelAct.htm">A Model Software Petite Patent Act</a>. The author's comment on his paper: "This is a significantly expanded and slightly updated version of an article by the same name published at 12 Santa Clara
 Computer & High Tech Law Journal 301 (Aug. 1996). This article directly responds to Professor Pamela Samuelson's and
 Professor Jerome Reichman's groundbreaking article, <a href="http://wwwsecure.law.cornell.edu/commentary/intelpro/manifint.htm">
A Manifesto Concerning the Legal Protection of Computer
 Programs</a>, calling upon others to draft new legislation providing a 'well-lit playing field' 'market-oriented' system regulating
 the value embodied in the behavior of commercial software."</li>
<li>On February 20, 2002 the European Commission has presented a <a href="http://europa.eu.int/comm/internal_market/en/indprop/com02-92en.pdf">proposal for a Directive on the protection by patents of computer-implemented inventions
</a>. The proposed Directive would harmonise the way in which national patent laws deal with inventions using software.
Such inventions can already be patented by applying to either the European Patent Office (EPO) or the national patent offices of the Member States, but the detailed conditions for patentability may vary. A significant barrier to trade in patented products within the Internal Market exists as long as certain inventions can be protected by patent in some Member States but not others.
The proposed Directive will be submitted to the EU�s Council of Ministers and the European Parliament for adoption under the so-called �co-decision� procedure.</li>
</UL>
 </FONT></td>
</tr>
</table>

<br><br><br>


<HR SIZE=1>
<br>
</FONT> <br>
<table WIDTH="100%" border="0" cellspacing="0" cellpadding="0" BGCOLOR="#FFFFFF">
<tr><td colspan=2><FONT FACE="Arial" SIZE="-2">
Please read the <A HREF="./disclaimer.php3">Disclaimer &amp; About This Website (Pflichtangaben gem&auml;ss TDG)</a> section.<br>&nbsp;<br>
Feel free to contact PA Axel H Horns via e-mail <A href="mailto:horns@ipjur.com">horns@ipjur.com</a>. BEWARE: DO NOT SEND CONFIDENTIAL INFORMATION UNENCRYPTED VIA E-MAIL. USE OF ENCRYPTION SOFTWARE IS HIGHLY RECOMMENDED. PA AXEL H HORNS IS PROVIDING SUPPORT FOR ENCRYPTED E-MAIL MESSAGES USING PGP OR PGP COMPATIBLE FORMATS. THE PGP PUBLIC KEY FOR PA AXEL H HORNS IS AVAILABLE <A HREF="./pubkey.php3">HERE</a>. THE GnuPG PUBLIC KEY FOR PA AXEL H HORNS IS AVAILABLE <A HREF="./gnupg_pubkey.php3">HERE</a>.<br>&nbsp;<br>

<A HREF="./ahh.php3">Dipl.-Phys. Axel H Horns</a> is Patentanwalt (German Patent Attorney),
European Patent Attorney as well as European Trade Mark Attorney. In particular, he is Member of:</FONT><br><br>
</td>
</tr>
<tr>
 <td><a href="http://www.scl.org"><img src="./logo-new-296x37.gif" width="296" height="37" border="0"
 alt="Click here to visit the SCL Online web site" align="center"></a>
 </td>
 <td><a href="http://www.cla.org"><img src="./claclr2.jpg" width="75" height="75" border="0"
 alt="Click here to visit the CLA Online web site" align="center"></a>
</td>
</tr>
</table>
</font>
</td>
</tr>
</TABLE>
</BODY>
</HTML>