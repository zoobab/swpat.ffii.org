<html>


<head>
<title>Reason: Creation Myths: Does innovation require intellectual property rights? </title>
<link rel="stylesheet" href="/reason/shared/htmls/default_temp.css" type="text/css">
</head>



<body bgcolor="ffffff" topmargin="0" marginheight="0">



<map name="topnav">
 <area shape="rect" coords="0,2,22,18" href="/" alt="" alt="[home]" target="_top">
 <area shape="rect" coords="35,2,59,18" href="/aboutreason.shtml" alt="" alt="[about]" target="_top">
 <area shape="rect" coords="72,2,98,18" href="http://www.picosearch.com/cgi-bin/ts0.pl?index=87281" alt="" alt="[search]" target="_top">
 <area shape="rect" coords="112,2,152,18" href="https://www.kable.com/pub/anxx/newsubs.asp" alt="" alt="[subscribe]" target="_top">
 <area shape="rect" coords="166,2,204,18" href="/advertise.shtml" alt="" alt="[advertise]" target="_top">
</map>



<table border="0" cellpadding="0" cellspacing="0" width="100%" height="44">
  <tr>
    <td width="100%" align="center" height="36"><p align="center"><!-- Begin Banner Code --> </p>
    <p align="center"><!--webbot bot="HTMLMarkup" startspan --><IFRAME SRC="http://reason.com/cgi-bin/ads.pl?iframe" MARGINWIDTH="0" MARGINHEIGHT="0" HSPACE="1" VSPACE="0" FRAMEBORDER="0" SCROLLING="NO" WIDTH="468" HEIGHT="60"><!--webbot bot="HTMLMarkup"
    endspan --> <script LANGUAGE="JavaScript" SRC="http://reason.com/cgi-bin/ads.pl?jscript">
  </script>
<NOSCRIPT>    <a HREF="http://reason.com/cgi-bin/ads.pl?banner=NonSSI;page=687" TARGET="_blank"><img
    SRC="http://reason.com/cgi-bin/ads.pl?page=687" WIDTH="468" HEIGHT="60"
    ALT="Support our Advertisers! Click Here!" BORDER="0"></a> </NOSCRIPT> <!--webbot bot="HTMLMarkup"
    startspan --></IFRAME><!--webbot bot="HTMLMarkup" endspan --> <!-- End Banner Code --> <a
    href="http://www.reason.com/subscribe.html"><img
    src="http://www.reason.com/ads/rsubadx.gif" alt="Subscribe to
Reason" border="0"></a></td>
  </tr>
</table>


<p>

<img src="/reason/shared/graphics/dotclear.gif" border="0" width="1" height="50"><br>
<table align="center" cellspacing="0" cellpadding="0" border="0" width="75%">
	<tr>
		<td align="left" width="345" background="/reason/shared/graphics/dotsback.gif"><a href="http://www.reason.com"><img src="/reason/shared/graphics/reasononline.gif" border="0" alt="" alt="Reason Online"></a></td>
		<td align="right" valign="bottom" background="/reason/shared/graphics/dotsback.gif"><img src="/reason/shared/graphics/topnav.gif" border="0" hspace="7" usemap="#topnav" alt="" alt="[site navigation ...]"></td>
	</tr>
	<tr>
		<td><img src="/reason/shared/graphics/dotclear.gif" border="0" width="1" height="15"></td>
	</tr>
</table>

</p>

<table align="center" cellpadding="25" WIDTH="85%" BORDER="0">
  <tr>

    <td WIDTH="5%">
<!--------------- Sidebar -------------------
      <br>
      <br>
      <br>
      <br>
      <br>
      <font class="sidebar">

      SIDEBAR CONTENT HERE<br>

      <br>
 --------------- Sidebar ------------------->
      <br></td>

    <td WIDTH="80%">
<div class="bodytext"><P ALIGN="RIGHT">March 2003<p></div>
         <div class="hed">
   Creation Myths<br></div>

         <div class="dek">
    Does innovation require intellectual property rights?<br></div>

         <div class="byline">
         By <a href="mailto:Doug.Clement@mpls.frb.org">Douglas Clement</a><br>
</div>




          <img src="/reason/shared/graphics/divider.gif" border="0" hspace="1" alt="" alt="-------------------------------------"><br>
          <img src="/reason/shared/graphics/dotclear.gif" border="0" vspace="5" alt="" alt="-------------------------------------"><br>

         <div class="bodytext">

 

<P>The most forceful performance at last year�s Grammy ceremony was a speech by Michael Greene, then president of the National Academy of Recording Arts and Sciences. Speaking not long after the 9/11 attacks, Greene gravely warned of a worldwide threat -- &quot;pervasive, out of control, and oh so criminal&quot; -- and implored his audience to &quot;em-brace this life-and-death issue.&quot;</P>

<P>Greene was not referring to international terrorism. &quot;The most insidious virus in our midst,&quot; he said sternly, &quot;is the illegal downloading of music on the Net.&quot;</P>

<P>Greene�s sermon may have been a bit overwrought, but he�s not alone in his fears. During the last decade, the captains of many industries -- music, movies, publishing, software, pharmaceuticals -- have railed against the &quot;piracy&quot; of their profits. Copyright and patent protections have been breached by new technologies that quickly copy and distribute their products to mass markets. And as quickly as a producer figures a way to encrypt a DVD or software program to prevent duplication, some hacker in Seattle, Reykjavik, or Manila figures a way around it.</P>

<P>The music industry has tried to squelch the threat, most conspicuously by suing Napster, the wildly popular Internet service that matched patrons with the songs they wanted, allowing them to download digital music files without charge. Napster lost the lawsuit and was liquidated, while similar services survive.</P>

<P>But the struggle over Napster-like services has accented a much broader issue: How does an economy best promote innovation? Do patents and copyrights nurture or stifle it? Have we gone too far in protecting intellectual property?</P>

<P>In a paper that has gained wide attention (and caught serious flak) for challenging the conventional wisdom, economists Michele Boldrin and David K. Levine answer the final question with a resounding yes. Copyrights, patents, and similar government-granted rights serve only to reinforce monopoly control, with its attendant damages of inefficiently high prices, low quantities, and stifled future innovation, they write in &quot;Perfectly Competitive Innovation,&quot; a report published by the Federal Reserve Bank of Minneapolis. More to the point, they argue, economic theory shows that perfectly competitive markets are entirely capable of rewarding (and thereby stimulating) innovation, making copyrights and patents superfluous and wasteful.</P>

<P>Reactions to the paper have been mixed. Robert Solow, the MIT economist who won a Nobel Prize in 1987 for his work on growth theory, wrote Boldrin and Levine a letter calling the paper &quot;an eye-opener&quot; and making suggestions for further refinements. Danny Quah of the London School of Economics calls their analysis &quot;an important and profound development&quot; that &quot;seeks to overturn nearly half a century of formal economic thinking on intellectual property.&quot; But UCLA economist Benjamin Klein finds their work &quot;unrealistic,&quot; and Paul Romer, a Stanford economist whose path-breaking development of new growth theory is the focus of much of Boldrin and Levine�s critique, considers their logic flawed and their assumptions implausible.</P>

<P>&quot;We�re not claiming to have invented anything new, really,&quot; says Boldrin. &quot;We�re recognizing something that we think has been around ever since there has been innovation. In fact, patents and copyrights are a very recent distortion.&quot; Even so, they�re working against a well-established conventional wisdom that has sanctioned if not embraced intellectual property rights, and theirs is a decidedly uphill battle.</P>
<B>
<P>The Conventional Wisdom</P>
</B>
<P>In the 1950s Solow showed that technological change was a primary source of economic growth, but his models treated that change as a given determined by elements beyond pure economic forces. In the 1960s Kenneth Arrow, Karl Shell, and William Nordhaus analyzed the relationship between markets and technological change. They concluded that free markets might fail to bring about optimal levels of innovation.</P>

<P>In a landmark 1962 article, Arrow gave three reasons why perfect competition might fail to allocate resources optimally in the case of invention. &quot;We expect a free enterprise economy to underinvest in invention and research (as compared with an ideal),&quot; he wrote, &quot;because it is risky, because the product can be appropriated only to a limited extent, and because of increasing returns in use.&quot;</P>

<P>Risk does seem a clear roadblock to investment in technological change. Will all the hours and dollars spent on research and development result in a profitable product? Is the payoff worth the risk? The uncertainty of success diminishes the desire to try. Much of Arrow�s article examines economic means of dealing with uncertainty, none of them completely successful.</P>

<P>The second problem, what economists call inappropriability, is the divergence between social and private benefit -- in this case, the difference between the benefit society would reap from an invention and the benefit reaped by the inventor. Will I try to invent the wheel if all humanity would benefit immeasurably from my invention but I�d get only $1,000? Maybe not. Property rights, well-defined, help address the issue.</P>

<P>The third obstacle is indivisibility. The problem here is that the act of invention involves a substantial upfront expenditure (of time or money) before a single unit of the song, formula, or book exists. But thereafter, copies can be made at a fraction of the cost. Such indivisibilities result in dramatically increasing returns to scale: If a $1 million investment in research and development results in just one unit of an invention, the prototype, a $2 million expenditure could result in the prototype plus thousands or millions of duplicates.</P>

<P>This is a great problem to have, but perfect competition doesn�t deal well with increasing returns to scale. With free markets and no barriers to entry, products are priced at their marginal cost (that is, the cost of the latest copy), and that price simply won�t cover the huge initial outlay -- that is, the large indivisibility that is necessary to create the prototype. Inventors will have no financial incentive for bringing their inventions to reality, and society will be denied the benefits.</P>

<P>Increasing returns therefore seem to argue for some form of monopoly, and in the late 1970s Joseph Stiglitz and Avinash Dixit developed a growth model of monopolistic competition -- that is, limited competition with increasing returns to scale. It�s a model in which many firms compete in a given market but none is strictly a price taker. (In other words, each has some ability to restrict output and raise prices, like a monopolist.) It�s a growth model, in other words, without perfect competition. The Dixit-Stiglitz model is widely used today, with the underlying assumption that economic growth requires technological change, which implies increasing returns, which means imperfect competition.</P>

<P>Stanford�s Paul Romer formalized much of this work in the 1980s and 1990s, in what he called a theory of endogenous growth. The idea was that technological change -- innovation  -- should be modeled as part of an economy, not outside it as Solow had done. The policy implication was that economic variables, such as interest and tax rates, as well as subsidies for research and technical education, could influence the rate of innovation. (See &quot;Post-Scarcity Prophet,&quot; December 2001.)</P>

<P>Romer refined the ideas of Arrow and others, developing new terms, integrating the economics of innovation and extending the Dixit-Stiglitz growth model into what he called &quot;new growth theory.&quot; In a parallel track, Robert Lucas, a Nobel laureate at the University of Chicago, elucidated the importance of human capital to economic growth. And just prior to all this growth theory work, Paul Krugman, Elhanan Helpman, and others integrated increasing returns theory with international trade economics, creating &quot;new trade theory.&quot; Similar theories became the bedrock of industrial organization economics.</P>

<P>Central to Romer�s theory is the idea of nonrivalry, a property he considers inherent to invention, designs, and other forms of intellectual creation. &quot;A purely nonrival good,&quot; he wrote, &quot;has the property that its use by one firm or person in no way limits its use by another.&quot; A formula, for example, can be used simultaneously and equally by 100 people, whereas a wrench cannot.</P>

<P>Nonrivalrous goods are inherently subject to increasing returns to scale, says Romer. &quot;Developing new and better instructions is equivalent to incurring a fixed cost,&quot; he wrote. &quot;Once the cost of creating a new set of instructions has been incurred, the instructions can be used over and over again at no additional cost.&quot; But if this is true, then &quot;it follows directly that an equilibrium with price taking cannot be supported.&quot; In other words, economic growth -- and the technological innovation it requires -- aren�t possible under perfect competition; they require some degree of monopoly power.</P>

<B><P>Undermining Convention</P>
</B>
<P>Economists prize economic growth but distrust monopoly, so accepting the latter to obtain the former is a Faustian bargain at best. With &quot;Perfectly Competitive Innovation,&quot; Boldrin and Levine vigorously reject the contract.</P>

<P>Innovation, they argue, has occurred in the past without substantial protection of intellectual property. &quot;Historically, people have been inventing and writing books and music when copyright did not exist,&quot; notes Boldrin. &quot;Mozart wrote a lot of very beautiful things without any copyright protection.&quot; (The publishers of music and books, on the other hand, sometimes did have copyrights in the materials they bought from their creators.) </P>

<P>Contemporary examples are also plentiful. The fashion world -- highly competitive, with designs largely unprotected -- innovates constantly and profitably. A Gucci is a Gucci; knock-offs are mere imitations and worth less than the original, so Gucci -- for better or worse -- still has an incentive to create. The financial securities industry makes millions by developing and selling complex securities and options without benefit of intellectual property protection. Competitors are free to copy a firm�s security package, but doing so takes time. The initial developer�s first-mover advantage secures enough profit to justify &quot;inventing&quot; the security.</P>

<P>As for software, Boldrin refers to an MIT working paper by economists Eric Maskin and James Bessen. Maskin and Bessen write that &quot;some of the most innovative industries today -- software, computers and semiconductors -- have historically had weak patent protection and have experienced rapid imitation of their products.&quot;</P>

<P>Moreover, U.S. court decisions in the 1980s that strengthened patent protection for software led to less innovation. &quot;Far from unleashing a flurry of new innovative activity,&quot; Maskin and Bessen write, &quot;these stronger property rights ushered in a period of stagnant, if not declining, R&amp;D among those industries and firms that patented most.&quot; Industries that depend on sequential product development -- the initial version is followed by an improved second version, etc. -- are, they argue, likely to be stifled by stronger intellectual property regimes.</P>

<P>&quot;So examples abound,&quot; says Boldrin. &quot;That�s the empirical point: Evidence shows that innovators have enough of an incentive to innovate.&quot; But he and Levine are not, by nature or training, empiricists. They build mathematical models to describe economic theory. In the case of intellectual property, they contend, current theory says innovation won�t happen unless innovators receive monopoly rights, but the evidence says otherwise. &quot;So what we do is to develop the theoretical point to explain the evidence,&quot; says Boldrin.</P>

<B><P>Rivalry Over Nonrivalry</P>
</B>
<P>A fundamental tenet of current conventional wisdom is that knowledge-based innovations are subject to increasing returns because ideas are nonrivalrous. Boldrin and Levine argue that in an economy this has no relevance. While pure ideas can be shared without rivalry in theory, the economic application of ideas is inherently rivalrous, because ideas &quot;have economic value only to the extent that they are embodied into either something or someone.&quot; What is relevant in the economic realm is not an abstract concept or formula -- no matter how beautiful -- but its physical embodiment. Calculus is economically valuable only insofar as engineers and economists know and apply it. &quot;Only ideas embodied in people, machines or goods have economic value,&quot; they write. And because of their physical embodiment, &quot;valuable ideas...are as rivalrous as commodities containing no ideas at all, if such exist.&quot;</P>

<P>A novel is valuable only to the extent that it is written down (if then). A song can be sold only if it is sung, played, or printed by its creator. A software program -- once written -- might seem costless, Boldrin and Levine write, but &quot;the prototype does not sit on thin air. To be used by others it needs to be copied, which requires resources of various kinds, including time. To be usable it needs to reside on some portion of the memory of your computer....When you are using that specific copy of the software, other people cannot simultaneously do the same.&quot;</P>

<P>In each instance, the development of the initial prototype is far more costly than the production of all subsequent copies. But because copying takes time -- a limited commodity -- and materials (paper, ink, disk space), it is not entirely costless. &quot;Consider the paradigmatic example of the wheel,&quot; they write. &quot;Once the first wheel was produced, imitation could take place at a cost orders of magnitude smaller. But even imitation cannot generate free goods: to make a new wheel, one needs to spend some time looking at the first one and learning how to carve it.&quot;</P>

<P>The first wheel is far more valuable than all others, of course, but that &quot;does not imply that the wheel, first or last that it be, is a nonrivalrous good. It only implies that, for some goods, replication costs are very small.&quot;</P>

<P>Economic theorists generally have assumed that the dramatic difference between development and replication costs can be modeled as a single process with increasing returns to scale: a huge fixed cost (the initial investment) followed by costless duplication. Boldrin and Levine say this misrepresents reality: There are two distinct processes with very different technologies. Development is one production process involving long hours, gallons of coffee, sweaty genius, and black, tempestuous moods. At the end of this initial process, the prototype (with any luck) exists and the effort and money that produced it are a sunk cost, an expense in the past.</P>

<P>Thereafter, a very different production process governs: Replicators study the original, gather flat stones, round off corners, bore center holes, and prune tree limbs into axles. Stone wheels roll off the antediluvian assembly line. In this second process, the economics of production are the same as for any other commodity, usually with constant returns to scale.</P>

<P>As Boldrin and Levine develop their mathematical model, they assume only that, &quot;as in reality,&quot; copying takes time and there is a limit (less than infinity) on the number of copies that can be produced per unit of time. These &quot;twin assumptions&quot; introduce a slim element of rivalry. After it�s created, the prototype can be either consumed or used for copying in the initial time period. (Technically, it could be used for both, but not as easily as if it were used for just one or the other.)</P>

<P>While others simply have assumed, with Romer, that the prototype of an intellectual product is nonrivalrous, Boldrin and Levine argue that the tiny cost of replicating it undermines the conventional model. Production is not subject to increasing returns, they argue, and competitive markets can work. &quot;Even a minuscule amount of rivalry,&quot; they write, &quot;can turn standard results upside down.&quot;</P>

<B><P>Britney Gets Her Due</P>

</B><P>Still, the central question is whether innovators will have enough incentive to go through the arduous, expensive invention process. Since the 1400s, when the first patent systems emerged in Venice, governments have tried to provide incentive by granting inventors sole rights to their creations for limited periods. The U.S. Constitution gives Congress the power &quot;to promote the Progress of Science and useful Arts, by securing for limited Times to Authors and Inventors the exclusive Right to their respective Writings and Discoveries.&quot;</P>

<P>Economists long have recognized that such exclusive rights give creators monopolies, allowing them to set prices and quantities that may not be socially optimal. But conventional thinking says these costs are the necessary tradeoff for bringing forth creative genius. Today, the legal realities and economic conventions have assumed the air of incontrovertible fact: If inventors can be &quot;ripped off&quot; -- copied as soon as they create -- why would they bother?</P>

<P>In arguing for competitive innovation rather than the monopolistic variety, Boldrin and Levine emphasize that they are not saying creators don�t have rights. On the contrary, they stress that innovators should be given &quot;a well defined right of first sale.&quot; (Or, more technically, &quot;we assume full appropriability of privately produced commodities.&quot;) And creators should be paid the full market value of their invention, the first unit of the new product. That value is &quot;the net discounted value of the future stream of consumption services&quot; generated by that first unit, which is an economist�s way of saying it�s worth the current value of everything it�s going to earn in the future. </P>

<P>So if Britney Spears records a new song, she should be able to sell the initial recording for the sum total of whatever music distributors think her fans will pay for copies of the music during the next century or so. Distributors know her songs are in demand, and she knows she can command a high price. As in any other market, the buyer and seller negotiate a deal. The same rules would hold for a novelist who writes a book, a software programmer who generates code, or a physicist who develops a useful formula. They get to sell the invention in a competitive market. They�re paid whatever the market will bear, and if the market values copies of their song, book, code, or formula, the initial prototype will be precious and they�ll be well paid.</P>

<P>In fact, says Boldrin, &quot;in a competitive market, the very first few copies are very valuable because those are the instruments which the imitators -- the other people who will publish your stuff -- will use to make copies. They�re more capital goods than consumption goods. So the initial copies will be sold at a very high price, but then very rapidly they will go down in price.&quot;</P>

<P>What creators won�t get, in Boldrin and Levine�s world, is the right to impose downstream licensing agreements that prevent customers from reproducing the product, modifying it, or using it as a stepping stone to the next innovation. They can�t prevent their customers from competing with them.</P>

<P>But will the market pay the creator enough? That depends on the innovator�s opportunity costs. If the price likely to be paid for an invention�s first sale exceeds the opportunity costs of the inventor, then yes, the inventor will create. If a writer spends a year on a book, and could have earned $30,000 during that year doing something else, then her opportunity cost is $30,000. Only if she guesses she can sell her book for at least that much is she likely to sit down and write.</P>

<P>&quot;What we show in the technical paper is that the amount [a book publisher] gives me is positive, and in fact, it can be large,&quot; says Boldrin. &quot;Then it�s up to me to figure out if what society is paying me is enough to compensate for my year of work.&quot;</P>

<P>But what happens as reproduction technologies im-prove: as printing presses get quicker, or as the Internet lets teenagers share music files faster and farther? Won�t that drive authors and musicians into utter poverty?</P>
<P>In fact, Boldrin and Levine argue, the opposite should occur. Increasing rates of reproduction will drop marginal production costs and, therefore, prices. If demand for the good is elastic -- that is, if demand rises disproportionately when prices drop -- then total revenue will increase. </P>

<P>And since creators with strong rights of first sale are paid the current value of future revenue, their pay will climb. &quot;The point we�re making is the invention of things like Napster or electronic publishing and so on are actually creating more opportunities for writers, musicians, for people in general to produce intellectual value, to sell their stuff and actually make money,&quot; says Boldrin. &quot;The costs I suffer to write down one of my books or songs have not changed, so overall we actually have a bigger incentive, not smaller incentive.&quot;</P>

<P>Conventional wisdom admits that monopoly rights impose short-term costs on an economy. They give an undue share of the economic pie to those who own copyrights and patents; they misallocate resources by allowing innovators to command too high a price; they allow innovators to produce less than the socially optimal level of the new invention. But these costs are all considered reasonable because innovation creates economic growth: The static costs are eclipsed by dynamic development.</P>

<P>Boldrin and Levine say this is a false dilemma. Monopoly rights are not only unnecessary for innovation but may stifle it, particularly when an innovation reduces the cost of expanding production. &quot;Monopolists as a rule do not like to produce much output,&quot; they write. &quot;Insofar as the benefit of an innovation is that it reduces the cost of producing additional units of output but not the cost of producing at the current level, it is not of great use to a monopolist.&quot; Monopolists, after all, can set prices and quantities to maximize their profits; they may have no incentive to find faster reproduction technologies.</P>

<P>More broadly, producers are likely to engage in what economists call &quot;rent-seeking behavior&quot; -- efforts to protect or expand turf (and profits) by fighting for government-granted monopoly protection -- and that behavior is likely to stifle innovation. Expensive patent races, defensive patenting (in which firms create a wall of patents to prevent competitors from coming up with anything remotely resembling their product), and costly infringement battles are common functions of corporate law departments. Such activity chokes off creative efforts by others, particularly the small and middle-sized firms that are typically more innovative.</P>

<B><P>The Critics</P>
</B>
<P>Like any radical innovation, Boldrin and Levine�s argument has its critics. &quot;We�ve been presenting it in quite a few key places, and I have to admit that every time there was a riot,&quot; says Boldrin. &quot;There was a riot at Stanford last Thursday. It was a huge riot at Chicago two weeks ago. I know it was a riot at Toulouse when David presented it.&quot;</P>

<P>A &quot;riot&quot; among economists might not call for crowd control, but the paper does evoke strong reactions. UCLA�s Klein says the paper is &quot;unrealistic modeling with little to do with the real world.&quot; In a paper with Kevin Murphy of the University of Chicago and Andres Lerner of Economic Analysis LLC, Klein writes that Boldrin and Levine�s model works only under the &quot;arbitrary demand assumption&quot; that demand for copies is elastic, so that as price falls over time output increases more than proportionately and profit rises. In the case of Napster and the music industry, this &quot;clearly conflicts with record company pricing. That is, if Boldrin and Levine were correct, why are record companies not pricing CDs as low as possible?&quot;</P>

<P>Romer has a broader set of objections. As a co-author and graduate school classmate of Levine�s and a former teacher of Boldrin�s at the University of Rochester, Romer has no desire to brawl with his respected colleagues. Moreover, he agrees that property rights for intellectual goods are sometimes too strong; in some cases, society might benefit from weaker restrictions. Music file sharing, for example, might increase social welfare even if it hurts the current music industry. And he stresses that alternative mechanisms for bringing forth innovation -- government support for technology education, for example -- might well be superior to copyrights and patents. Nonetheless, Romer does have serious problems with the new theory.</P>

<P>First of all, the first-sale rights Boldrin and Levine would assign to innovators &quot;would truly be an empty promise.&quot; In their model, if a pharmaceutical firm discovers a new compound, it can sell the first pills but not restrict their downstream use. A generic drug manufacturer could then buy one pill, analyze it, and start stamping out copies.</P>

<P>&quot;So what Boldrin and Levine call �no downstream licensing� is instant generic status for drugs,&quot; Romer complains. And while they argue that the inventor &quot;can sell a few pills for millions of dollars,&quot; this is unrealistic if everyone who buys a pill can copy it. &quot;You can make a set of mathematical assumptions so that this is all logically consistent,&quot; says Romer, &quot;but those assumptions are wildly at odds with the underlying facts in the pharmaceutical industry.&quot;</P>

<P>If Boldrin and Levine are unrealistic about appropriability, they are even more at sea re-garding rivalry, Romer adds. While it�s true that ideas must be embodied to be economically useful, it�s false to say that there is no distinction between the idea and its physical instantiation. A formula must be written down, but the formula is far more valuable than the piece of paper on which it�s written. In a large market, the formula could be so valuable that &quot;the cost of the extra paper is trivial -- so small that it is a reasonable approximation to neglect it entirely.&quot; If Romer�s approximation is right -- if it truly is reasonable to neglect that &quot;trivial&quot; cost -- then out goes the slim element of rivalry on which the Boldrin/Levine argument rests.</P>

<P>Romer also objects to the contention that competition can deal well with sunk costs. And he suggests that Boldrin and Levine are wrong to object to copyright restriction of downstream use, since perfect competition allows sellers and buyers to enter contracts that impose such restrictions. &quot;What justification is there,&quot; says Romer, &quot;for preventing consenting adults from writing contracts that limit subsequent or downstream uses of a good?&quot;</P>

<P>Boldrin�s quick e-mail re-sponse: &quot;We never say anything like that!! Patents and copyrights are NOT private contracts; they are monopoly rights given by governments.&quot;</P>

<P>Romer counters: &quot;The legal system creates an opportunity for an owner to write contracts that limit how a valuable good can be used....The proposal from Boldrin and Levine would deprive a pharmaceutical company or the owner of a song of the chance to write this kind of contract with a buyer.&quot;</P>

<P>According to University of Chicago�s Lucas, &quot;There is no question that Boldrin and Levine have their theory worked out correctly. The issue is where it applies and where it doesn�t.&quot; Their strongest examples, Lucas argues, are Napster and the music industry. &quot;If we do not enforce copyrights to music, will people stop writing and recording songs?&quot; he asks rhetorically. &quot;Not likely, I agree. If so, then protection against musical �piracy� just comes down to protecting monopoly positions: something economists usually oppose, and with reason.&quot;</P>

<P>But Lucas cautions that their theory may not apply everywhere. &quot;What about pharmaceuticals?&quot; he asks, echoing Romer. &quot;Here millions are spent on developing new drugs. Why do this if the good ideas can be quickly copied?&quot;</P>

<B><P>Refining the Theory</P>
</B>
<P>Solow suggests that Boldrin and Levine should enrich their &quot;very nice paper&quot; by testing its robustness. What happens, for example, if the time interval between invention and copying is shrunk? And -- echoing Arrow -- &quot;does anything special happen if you introduce some uncertainty about the outcome of an investment in innovation?&quot;</P>

<P>Boldrin and Levine recognize that work remains to be done to strengthen their theory. They have begun to examine the effect of uncertainty on their model, as Solow suggests, and they say the results still broadly obtain. The difference is that a large monopolist may be able to insure himself against risk, whereas competitors will need to create securities that allow them to sell away some of the risk and buy some insurance.</P>

<P>As for pharmaceutical research and development, Boldrin and Levine contend that their critics are misrepresenting the industry�s economics. Much of the high cost of pharmaceutical R&amp;D, Boldrin argues, is due to the inflated values placed on drug researchers� time because they are employed by monopolists. Researchers are paid far less in the more competitive European drug industry.</P>

<P>In addition, Levine says, pharmaceuticals aren�t sold into a competitive market: &quot;They are generally purchased by large organizations such as governments and HMOs.&quot; If inflated drug prices are viewed more realistically, these economists argue, the development costs of new drugs would not be nearly as insurmountable as commonly believed.</P>

<P>Moreover, copying a drug takes time and money, providing the innovative drug company with a substantial first-mover advantage. &quot;It�s not obvious that the other guys can imitate me overnight,&quot; says Boldrin. &quot;The fact that you are the first and know how to do it better than the other people -- it may be a huge protection.&quot;</P>

<P>Still, they admit, there are cases of indivisibility where the initial investment may simply be too large for a perfectly competitive market. &quot;We have argued that the competitive mechanism is a viable one, capable of producing sustained innovation,&quot; they write. &quot;This is not to argue that competition is the best mechanism in all circumstances.&quot; Indivisibility constraints may keep some socially desirable innovations from being produced; the situation is similar to a public goods problem. The authors suggest that contingent contracts and lotteries could be used in such cases, but &quot;a theory of general equilibrium with production indivisibility remains to be fully worked out.&quot;</P>

<P>Some economists have already begun work on the next stages. Quah at the London School of Economics has pushed Boldrin and Levine�s model in a number of directions to test its robustness and applicability. In one paper, he finds it works well if he tweaks assumptions about the consumption and production of the intellectual assets, but it falters if he changes time constraints.</P>

<P>In another paper, Quah contends that Boldrin and Levine�s potential solutions to indivisibility constraints may not actually resolve the problem. &quot;What is needed,&quot; he writes, &quot;is the capability to continuously adjust the level of an intellectual asset�s instantiation quantity.&quot; Roughly translated: We need the ability to come up with half an idea. That might be a problem.</P>

<P>More studies like Quah�s will be needed to poke, prod, refine, refute, and extend Boldrin and Levine�s theory. And empirical work will be needed to see whether it is indeed a more apt description of innovation. The theory is part of an intellectual thicket, and economists who work that thicket tend to render it impenetrable by adopting different terms or defining identical terms differently.</P>

<P>What is clear, though, is that Boldrin and Levine have mounted a formidable assault on the conventional wisdom about innovation and the need to protect intellectual property. That it has met with opposition or incredulity is to be expected. What matters are the next steps.</P>

<P>&quot;The reaction for now is surprise and disbelief,&quot; Boldrin says. &quot;We�ll see. In these kinds of things, the relevance is always if people find the suggestion interesting enough that it�s worth pushing farther the research. All we have made is a simple theoretical point.&quot; </P>
<P>&#9;</P>
<img src="/reason/shared/graphics/dotclear.gif" border="0" width="1" height="10" alt="" alt="-------------------------------------"><br>

          <font class="tagline">
<I><P>Douglas Clement is a senior writer for The Region, a magazine published by the Federal Reserve Bank of Minneapolis. A version of this article appeared in The Region�s September 2002 issue.</P></I>
</font><br>

         </div>

</td>
    <td width="7%">&nbsp;</td>
  </tr>
</table>

</td>
    <td width="7%">&nbsp;</td>
  </tr>
</table>

<br>
<br>
<br>
</body>


</html>

