
   [bar75k.gif] [bar75l.gif]
   EPO logo
   EPO boards of appeal decisions
   [1]homepage => search & index => [2]boards of appeal decisions
   [bar75k.gif] [bar75l.gif]
   [3]Search mask [4]List of updates
   Date of decision 08 September 2000 
   Case number T 0931/95 - 3.5.1 
   Application number 88302239.4
   IPC G06F15/30
   Procedure Language EN
   Title of the application Improved pension benefits system
   Applicant name Pension Benefit Systems Partnership
   Opponent name -
   Headnote

     -

   Articles and Rules EPC [5]. Art 52(1)
   [6]. Art 52(2)
   [7]. Art 52(2)(c)
   [8]. Art 52(3)
   [9]. Art 56
   [10]. Art 84
   Keywords

     Exclusion from patentability: of schemes, rules and methods for
     doing business (yes) - of apparatus constituting a physical entity
     for carrying out such method (no)

   Cited Decisions [11]T 0208/84
   [12]T 0769/92
   [13]T 1002/92
   [14]T 1173/97
   [15]T 0935/97
   Catchwords:
   1. Having technical character is an implicit requirement of the EPC to
   be met by an invention in order to be an invention within the meaning
   of Article 52(1)EPC. (following decisions T 1173/97 and T 935/97)

   2. Methods only involving economic concepts and practices of doing
   business are not inventions within the meaning of Article 52(1) EPC. A
   feature of a method which concerns the use of technical means for a
   purely non-technical purpose and/or for processing purely
   non-technical information does not necessarily confer a technical
   character to such a method.

   3. An apparatus constituting a physical entity or concrete product,
   suitable for performing or supporting an economic activity, is an
   invention within the meaning of Article 52(1) EPC.

   4. There is no basis in the EPC for distinguishing between "new
   features" of an invention and features of that invention which are
   known from the prior art when examining whether the invention
   concerned may be considered to be an invention within the meaning of
   Article 52(1) EPC. Thus there is no basis in the EPC for applying this
   so-called contribution approach for this purpose. (following decisions
   T 1173/97 and T 935/97)

     * View the [16]full document as PDF file
     * [17]Voll-Dokument als PDF Dokument betrachten
     * Visualiser le [18]document complet au format PDF

                        [19]get the full pdf 

     _________________________________________________________________

   [20]EPO Home Page | [21]Recent updates | [22]Request info | [23]Send
   comments | [24]Index
   [25]Patent information on the internet
   Copyright � 1997-2000 European Patent Office . All Rights Reserved.
   E-mail: [26]EPO Mail Distribution

Verweise

   1. http://www.european-patent-office.org/index.htm
   2. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/dg3/search_dg3.htm
   3. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/dg3/search_dg3.htm
   4. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/dg3/updates/index.htm
   5. javascript:openWinClick('http://www.european-patent-office.org/legal/epc/e/ar52.html',630,400);
   6. javascript:openWinClick('http://www.european-patent-office.org/legal/epc/e/ar52.html',630,400);
   7. javascript:openWinClick('http://www.european-patent-office.org/legal/epc/e/ar52.html',630,400);
   8. javascript:openWinClick('http://www.european-patent-office.org/legal/epc/e/ar52.html',630,400);
   9. javascript:openWinClick('http://www.european-patent-office.org/legal/epc/e/ar56.html',630,400);
  10. javascript:openWinClick('http://www.european-patent-office.org/legal/epc/e/ar84.html',630,400);
  11. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/bib/t840208.htm
  12. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/bib/t920769.htm
  13. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/bib/t921002.htm
  14. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/bib/t971173.htm
  15. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/bib/t970935.htm
  16. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/pdf/t950931eu1.pdf
  17. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/pdf/t950931eu1.pdf
  18. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/pdf/t950931eu1.pdf
  19. file://localhost/ul/sig/srv/res/www/ffii/swpat/vreji/minra/pdf/t950931eu1.pdf
  20. http://www.european-patent-office.org/index.htm
  21. http://www.european-patent-office.org/updates.htm
  22. http://www.european-patent-office.org/forms/index.htm
  23. http://www.european-patent-office.org/forms/comments.htm
  24. http://www.european-patent-office.org/search.htm
  25. http://www.european-patent-office.org/online/index.htm
  26. http://www.european-patent-office.org/mail/index.htm
