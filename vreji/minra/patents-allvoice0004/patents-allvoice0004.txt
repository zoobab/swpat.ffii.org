
   [1]Login

   Pref Your subscriptions [2]Home [3]Help

   patents@aful.org
   software patents mailing list



   [4]List info 


   Subscribers: 462
   Owners
   [5]jp@nexedi.com
   [6]jp@smets.com
   [7]Hartmut Pilch
   Moderators


   [8]Subscribe 


   [9]Unsubscribe 


   Archive


   Post

   _________________ Search
   [10]Advanced search
   1999 01 02 03 04 [11]05 [12]06 [13]07 [14]08 [15]09 [16]10 [17]11
   [18]12
   2000 [19]01 [20]02 [21]03 [22]04 [23]05 [24]06 [25]07 [26]08 [27]09
   [28]10 [29]11 [30]12
   2001 [31]01 [32]02 [33]03 [34]04 [35]05 [36]06 [37]07 [38]08 [39]09
   [40]10 [41]11 [42]12
   2002 [43]01 [44]02 [45]03 [46]04 [47]05 [48]06 [49]07 [50]08 [51]09
   [52]10 [53]11 [54]12
   2003 [55]01 [56]02 [57]03 [58]04 [59]05 [60]06 [61]07 [62]08 09 10 11
   12

                [63] previous    [64]Chronological            [65] previous 
   [66]Thread       
     _________________________________________________________________

   [srctran@world.std.com: PATNEWS: IBM upsets UK over patent; Unisys
   back with GIF; IBM pat cites] Bernard Lang Bernard.Lang
     * From: Bernard Lang Bernard.Lang@xxxxxxxx
     * Subject: [srctran@world.std.com: PATNEWS: IBM upsets UK over
       patent; Unisys back with GIF; IBM pat cites]
     * Date: Wed, 26 Apr 2000 09:50:13 +0200

   1- patents are no protection against big bullies
   2- users switch from GIF to PNG, because of Unisys patents
   ----- Forwarded message from Gregory Aharonian <srctran@world.std.com>
   -----
   Date: Sun, 23 Apr 2000 21:15:14 -0400 (EDT)
   From: Gregory Aharonian <srctran@world.std.com>
   To: patent-news@world.std.com
   Subject: PATNEWS: IBM upsets UK over patent; Unisys back with GIF; IBM
   pat
   cites
   Precedence: list
   Reply-To: patent-news@europe.std.com
   ====
   IBM case sabotages small Devon firm
   By Charles Arthur
   Technology Editor
   The Independent (London)
   20 April 2000
   A small British computer firm with patents on an invention potentially
   worth millions is being ripped off by one of the world's biggest
   companies
   and stymied by American courts, an MP claimed yesterday.
   Allvoice, a Devon-based company, could even go out of business because
   it
   alleges that IBM has infringed its patents for using voice recognition
   technology with computers, while in the US a similar infringement
   claim
   against Dragon Systems has taken more than a year to conclude its
   preliminary hearing - which usually takes only a few days.
   Allvoice's system means people can talk to their computers, rather
   than
   typing into keyboards, and enter words directly into documents,
   correct
   them, and play back what they actually said.
   In Westminster Hall, Patrick Nicholls, Conservative MP for
   Teignbridge, said
   during a short debate that rather than being a multi-million-pound
   firm,
   Allvoice was struggling for its very existence. He blamed this on the
   anti-competitive behaviour of certain US companies and the
   procrastination
   of the US judiciary. He told MPs that US firms IBM and Dragon Systems
   had
   been developing their own voice recognition system but it had many
   faults
   and problems which the Allvoice system had managed to iron out.
   The US firms invited Allvoice representatives to meet them on the
   premise
   that they were interested in buying the system. But IBM later
   announced that
   they were developing a new voice recognition system based on many of
   the
   Allvoice applications.
   "The way US business has rallied round to destroy Allvoice would make
   a pack
   of sharks look like a group of nuns," Mr Nicholls said.
   John Mitchell, managing director of Allvoice, said afterwards: "We
   started
   our patent infringement case against Dragon in February last year. But
   the
   preliminary injunction has been delayed and delayed."
   ==========
   Some excerpts from the Financial Times article by Fiona Harvey.
   "AllVoice has filed a complaint against IBM with the European
   Commission,
   the European Union's Brussels-based executive."
   "Patrick Nicholls, MP for Teignbridge in Devon, said the case showed
   the difficulty small companies faced in enforcing patents against
   multinationals. Patricia Hewitt, small business minister, said the
   trade department would 'determine whether the US was fully compliant
   with its obligations to enforce patents' under intellectual property
   law."
   ====================
   -- UNISYS EFFORTS TO FURTHER LICENSE LZW/GIF PATENT CAUSING A STIR
   Patent demands may spur Unisys rivals in graphics market
   By Evan Hansen
   Staff Writer, CNET News.com
   April 18, 2000, 12:40 p.m. PT
   Unisys is expanding its efforts to license the technology behind the
   Web's
   most popular graphics format, as it continues talks with major
   Internet
   portals to pay for the right to use so-called GIF files.
   The company has successfully licensed its technology for years, but
   Web
   developers say it recently has become more aggressive in asserting its
   GIF
   patent, called LZW, targeting Web content companies and charging
   higher
   licensing fees.
   Unisys' head patent counsel, Mark Starr, refused to discuss specifics
   of
   his company's licensing deals but confirmed that the company is in
   negotiations with Yahoo, Disney's Go.com and other Web companies
   regarding
   potential licensing for the technology.
   "This isn't new," he said, noting that the company began looking at
   Web
   content companies in early 1999 and has been in talks with Yahoo for
   some
   time. "We have more than 2,000 licensees for this technology. There's
   been
   no recent shift in who we're enforcing the patent on."
   Neither Yahoo nor Disney responded to requests for comment.
   Whether there is anything new to Unisys' strategy, content companies
   in
   negotiations with Unisys for the first time may be close to embracing
   GIF
   substitutes, partly because of Unisys' licensing demands.
   At least one Unisys licensee already has indicated that it plans to
   limit
   its use of GIFs, adopting a free alternative known as PNG (pronounced
   "ping") for distributing graphics files to customers. Accuweather,
   which
   sells meteorological data to news outlets and other organizations,
   said in
   a memo to its customers on Friday that the switch to PNG will take
   full
   effect May 12, although Accuweather will continue to hold the rights
   to
   use GIFs on its own Web site.
   "We decided to change because it looks like things are going that
   way,"
   said Brandi Say, the Accuweather customer service representative who
   authored the memo.
   According to one person familiar with the companies' deal, Unisys had
   requested as much as $3.8 million under one licensing scenario that
   Accuweather rejected....
   See
   [67]http://news.cnet.com/news/0-1005-200-1713278.html
   ====================
   ----- End forwarded message -----
   --
   Bernard.Lang@inria.fr ,_ /\o \o/ Tel +33 1 3963 5644
   [68]http://pauillac.inria.fr/~lang/ ^^^^^^^^^^^^^^^^^ Fax +33 1 3963
   5469
   INRIA / B.P. 105 / 78153 Le Chesnay CEDEX / France
   Je n'exprime que mon opinion - I express only my opinion
   CAGED BEHIND WINDOWS or FREE WITH LINUX

                                   [english.]
                                              Powered by [69]Sympa 3.3.3 

Verweise

   1. http://www.aful.org/wws/nomenu/loginrequest/arc/patents
   2. http://www.aful.org/wws/
   3. http://www.aful.org/wws/help
   4. http://www.aful.org/wws/info/patents
   5. mailto:jp@nexedi.com
   6. mailto:jp@smets.com
   7. mailto:phm@a2e.de
   8. http://www.aful.org/wws/subrequest/patents
   9. http://www.aful.org/wws/sigrequest/patents
  10. http://www.aful.org/wws/arcsearch_form/patents/2000-04
  11. http://www.aful.org/wws/arc/patents/1999-05/
  12. http://www.aful.org/wws/arc/patents/1999-06/
  13. http://www.aful.org/wws/arc/patents/1999-07/
  14. http://www.aful.org/wws/arc/patents/1999-08/
  15. http://www.aful.org/wws/arc/patents/1999-09/
  16. http://www.aful.org/wws/arc/patents/1999-10/
  17. http://www.aful.org/wws/arc/patents/1999-11/
  18. http://www.aful.org/wws/arc/patents/1999-12/
  19. http://www.aful.org/wws/arc/patents/2000-01/
  20. http://www.aful.org/wws/arc/patents/2000-02/
  21. http://www.aful.org/wws/arc/patents/2000-03/
  22. http://www.aful.org/wws/arc/patents/2000-04/
  23. http://www.aful.org/wws/arc/patents/2000-05/
  24. http://www.aful.org/wws/arc/patents/2000-06/
  25. http://www.aful.org/wws/arc/patents/2000-07/
  26. http://www.aful.org/wws/arc/patents/2000-08/
  27. http://www.aful.org/wws/arc/patents/2000-09/
  28. http://www.aful.org/wws/arc/patents/2000-10/
  29. http://www.aful.org/wws/arc/patents/2000-11/
  30. http://www.aful.org/wws/arc/patents/2000-12/
  31. http://www.aful.org/wws/arc/patents/2001-01/
  32. http://www.aful.org/wws/arc/patents/2001-02/
  33. http://www.aful.org/wws/arc/patents/2001-03/
  34. http://www.aful.org/wws/arc/patents/2001-04/
  35. http://www.aful.org/wws/arc/patents/2001-05/
  36. http://www.aful.org/wws/arc/patents/2001-06/
  37. http://www.aful.org/wws/arc/patents/2001-07/
  38. http://www.aful.org/wws/arc/patents/2001-08/
  39. http://www.aful.org/wws/arc/patents/2001-09/
  40. http://www.aful.org/wws/arc/patents/2001-10/
  41. http://www.aful.org/wws/arc/patents/2001-11/
  42. http://www.aful.org/wws/arc/patents/2001-12/
  43. http://www.aful.org/wws/arc/patents/2002-01/
  44. http://www.aful.org/wws/arc/patents/2002-02/
  45. http://www.aful.org/wws/arc/patents/2002-03/
  46. http://www.aful.org/wws/arc/patents/2002-04/
  47. http://www.aful.org/wws/arc/patents/2002-05/
  48. http://www.aful.org/wws/arc/patents/2002-06/
  49. http://www.aful.org/wws/arc/patents/2002-07/
  50. http://www.aful.org/wws/arc/patents/2002-08/
  51. http://www.aful.org/wws/arc/patents/2002-09/
  52. http://www.aful.org/wws/arc/patents/2002-10/
  53. http://www.aful.org/wws/arc/patents/2002-11/
  54. http://www.aful.org/wws/arc/patents/2002-12/
  55. http://www.aful.org/wws/arc/patents/2003-01/
  56. http://www.aful.org/wws/arc/patents/2003-02/
  57. http://www.aful.org/wws/arc/patents/2003-03/
  58. http://www.aful.org/wws/arc/patents/2003-04/
  59. http://www.aful.org/wws/arc/patents/2003-05/
  60. http://www.aful.org/wws/arc/patents/2003-06/
  61. http://www.aful.org/wws/arc/patents/2003-07/
  62. http://www.aful.org/wws/arc/patents/2003-08/
  63. http://www.aful.org/wws/arc/patents/2000-04/msg00005.html
  64. http://www.aful.org/wws/arc/patents/2000-04/mail1.html#00006
  65. http://www.aful.org/wws/arc/patents/2000-04/msg00005.html
  66. http://www.aful.org/wws/arc/patents/2000-04/thrd1.html#00006
  67. http://news.cnet.com/news/0-1005-200-1713278.html
  68. http://pauillac.inria.fr/~lang/
  69. http://listes.cru.fr/sympa/
