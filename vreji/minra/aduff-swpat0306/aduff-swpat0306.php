<html>
<head>
	<title>Timeline of Tim Jackson's correspondence on the UK implementation of the European Copyright Directive [timj.co.uk]</title>
	<link rel="stylesheet" type="text/css" href="/main.css">
</head>

<body>
<div id="header">
	<img src="/images/logo.jpg" alt="timj.co.uk - some semblance of sanity in a digital world gone mad">
</div>

<div id="maincontent">
	<h1><a href="/digiculture/">Digital culture</a> : <a href="/digiculture/patents/">Software patents</a> : <a href="timeline.php">Timeline</a> : Andrew Duff's e-mail of 3 June 2003</h1>
	
	<pre class='email'>
From: Andrew Duff MEP &lt;mep/at\andrewduffmep.org&gt;
To: &lt;tim@timj.co.uk&gt;
Subject: Software Patents
Date: Tue, 03 Jun 2003 12:49:18 +0100

Dear Mr Jackson

Thank you for your letter concerning the Software Patentability Directive.
The European Parliamentary Committee on Legal Affairs and the Internal
Market (JURI) has shown a willingness to go along with the legislation as it
stands but the legislation will come before the parliament in June and can
still be revised or even dropped altogether.

Like most Liberal Democrats I do not believe that we are well served by a
system of regulation that seriously damages the financial incentives implied
by copyright protection. Citizens and consumers are also producers of
intellectual and artistic property and their rights deserve respect.
However, I also believe that there are circumstances in which protection
regimes can stifle creativity and subvert the public interest. Software
patenting is potentially one of these areas. There is no evidence that the
allowances provided by the 1972 EPC are suppressing creativity or dissuading
investment - indeed in my Cambridge constituency the very opposite seems the
case. If they close off certain avenues of profitability they do so, in my
opinion, for profoundly important reasons. In an industry like
pharmaceuticals a single idea is often convertible into a single product.
Software is written in code and these routines are implicated in multiple
functions and the narrow bridges between hardware and software implied by
interoperability. Allowing commercial control of these algorithmic building
blocks and gateways creates the kind of monopolies that make a mockery of
free and creative enterprise.

While I see the value of harmonising European standards, in its current form
the Software Patentability Directive proposed by the European Commission
unquestionably seeks to widen the patentability criteria laid down in 1972.
As you are probably aware, both the Economic and Social Council of the
European Union and the German Monopoly Commission have strongly criticised
the Directive, as have Liberal Democrats in this country. The Research
Directorate of the European Parliament also produced a study that is
critical of the attempt to construct a regime that does not explicitly limit
the extent of patentability. It seems that we are on the verge of adopting a
patenting regime modelled on that of the United States, at the very moment
when critics in that country have begun to be forceful and articulate in
condemning it. Regardless of the conclusions of the JURI Committee, along
with my Liberal Democrat peers I will be working to amend the Directive as
it stands. 

Yours sincerely




Andrew Duff MEP
Liberal Democrat MEP for the East of England
	</pre>

</div>


<div id="sidenav">
	<h1>Site Navigation</h1>
	<ul>
				<li><a href='/'>Home</a>&nbsp;&middot;</li>
		<li><a href='/about/'>About Tim</a>&nbsp;&middot;</li>
		<li class='active'><a href='/digiculture/'>Digital culture</a>&nbsp;&middot;</li>
		<ul>
			<li><a href='/digiculture/patents/'>Software patents</a></li>
			<li><a href='/digiculture/eucd/'>EU Copyright Directive</a></li>
			<li><a href='/digiculture/drm.php'>The fallacy of DRM</a></li>
			<li><a href='/digiculture/ip.php'>On intellectual protection</a></li>
			<li><a href='/digiculture/idcards.php'>ID cards</a></li>
			<li><a href='/digiculture/oss.php'>OSS in Government</a></li>
		</ul>
		<li><a href='/linux/'>Linux/OSS</a>&nbsp;&middot;</li>
		<li><a href='/web/'>Web development</a>&nbsp;&middot;</li>
		<li><a href='/dance/'>Dance music</a>&nbsp;&middot;</li>
		<li><a href='/contact/'>Contact</a>&nbsp;&middot;</li>
	</ul>
	<br />
	<ul>
				<li><a href='/amiga/'>Amiga stuff (old)</a>&nbsp;&middot;</li>
	</ul>
	<div id="footer">
		This page last updated: <br />
	8 Jun 2003<br />
	&copy;2003 Tim Jackson

</div>
</div>


</body>
</html>
