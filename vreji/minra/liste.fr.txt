descr: Ces dernières années, l'Office Européen des Brevets a accordé environ 30000 brevets sur des algorithmes et des méthodes d'affaires - programmes d'ordinateurs, en contradiction avec la lettre comme avec l'esprit de la loi en vigueur. A présent, le mouvement européen pro-brevet voudrait d'un même geste légaliser ces attributions rétroactivement, et supprimer toute limitation effective à la brevetabilité. Les programmeurs ne pourront plus s'exprimer librement ni disposer librement de leurs propres oeuvres et quant aux citoyens, ils n'auront plus le droit de modeler eux-mêmes les formes de leur communication. Et quelle compensation nous promet-on à une telle %(e:dépossession intellectuelle) ? Moins d'innovation, moins de compatibilité, moins de bons logiciels.  
title: Textes archivées non-classifiés
Unc: Textes archivées non-classifiés

### Local Variables: ***
### coding: utf-8 ***
### mailto: mlhtimport@a2e.de ***
### login: swpatgirzu ***
### passwd: XXXX ***
### srcfile: /ul/prg/src/mlht/app/swpat/swpatvreji.el ***
### feature: swpatdir ***
### doc: swpatliste ***
### txtlang: fr ***
### End: ***
