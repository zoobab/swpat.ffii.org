
<html>
<head>
<title>Fischer und D&auml;ubler-Gmelin streiten um die Umsetzung der EU-Biopatentrichtlinie</title>
<meta http-equiv="content-language" content="de">
<meta http-equiv="content-type"	content="text/html; charset=iso-8859-1">
<meta http-equiv="expires" content="0">
<meta http-equiv="pragma" content="no-cache">
<meta name="robots" content="index,nofollow">
<meta name="revisit-after" content="3 weeks">
<meta name="keywords" content="Biotechnik,Biotechnologie,Chromosom,Diagnose,DNA,Erbgut,Forschung,Gen,Genforschung,Genmanipulation,Genom,Gensequenz,Gentechnik,Gesundheit,HUGO,Humangenom-Projekt,Krankheit,Medizin,Nanotechnik,Praxis,Therapie,Zellen">
<meta name="description" content="Bundesjustizministerin Herta D&auml;ubler-Gmelin (SPD) ist auf ihre Kabinettskollegin Andrea Fischer (B&uuml;ndnis 90/Die Gr&uuml;nen) nicht gut zu sprechen. Die Gesundheitsministerin hat sich an die Spitze der Ablehnungsfront gegen den vom Justiz-Ressort vorgelegten Gesetzentwurf zur Umsetzung der Biopatentrichtlinie der Europ&auml;ischen Union gesetzt.  ">
<meta name="copyright" content="&Auml;rzte Zeitung, Neu-Isenburg, Germany">
<meta name="date" content="Mon, 24 Jul 2000 08:00:00 GMT">
<meta name="strdate" content="24.7.2000">
<meta name="subject" content="Gentechnik">
<meta name="category" content="\medizin\gentechnik">
<meta name="productid" content="1209">
<meta name="articleid" content="114709">
<meta name="sdid" content="">
<meta name="sorder" content="">

</head>
<body leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 vlink="#004a94" link="#004a94" alink="#004a94">
<a name="top"></a>
<!-- begin vframe -->
<!-- begin adst -->

<table align="center" border="0" cellpadding="0" cellspacing="0">
  <tr valign="bottom"> 
    <td>
		<table align="center" border="0" cellpadding="0" cellspacing="0">
		<tr valign="bottom"> 
			<td>
			<!-- t1 --><img src="/img/global/tr.gif" width=5 height=1><!--Inserted HTML code from MSASv1.0-->
<A HREF="/include/adredir.asp?ciid=243&url=http://www.schwabe.de" TARGET="_new"><IMG SRC="/ads/tebonin.gif" WIDTH=156 HEIGHT=60 BORDER="0" Alt="Aus der Natur f�r den Menschen"></A><!-- /t1 -->
			</td><td>
			<!-- t2 --><img src="/img/global/tr.gif" width=5 height=1><!--Inserted HTML code from MSASv1.0-->
<A HREF="/include/adredir.asp?ciid=195&url=http://www.gelbe-liste.de" TARGET="_new"><IMG SRC="/ads/medimedia.gif" WIDTH=156 HEIGHT=60 BORDER="0" Alt="Gelbe Liste"></A><!-- /t2 -->    
			</td><td>
			<!-- t3 --><img src="/img/global/tr.gif" width=5 height=1><!--Inserted HTML code from MSASv1.0-->
<A HREF="/include/adredir.asp?ciid=167&url=http://www.diabetes-world.net" TARGET="_new"><IMG SRC="/ads/diabetesworld.gif" WIDTH=156 HEIGHT=60 BORDER="0" Alt="www.diabetes-world.net"></A><!-- /t3 -->
			</td><td>
			<!-- t4 --><img src="/img/global/tr.gif" width=5 height=1><!--Inserted HTML code from MSASv1.0-->
<A HREF="/include/adredir.asp?ciid=285&url=http://www.medworld.de" TARGET="_new"><IMG SRC="/ads/mw_fl_156_60.gif" WIDTH=156 HEIGHT=60 BORDER="0" Alt="www.medworld.de"></A><!-- /t4 -->
			</td>
		</tr>
		</table>
    </td>
  </tr>
</table>
<!-- end adst -->
<!-- begin top -->
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td bgcolor="#F0F0EB"><img src="/img/global/tr.gif" width="164" height=1></td>
    <td bgcolor="#F0F0EB"><a href="/docs/index.htm"><img src="/img/global/tr.gif" width="472" height=1 border="0"></a></td>
    <td bgcolor="#F0F0EB"><img src="/img/global/tr.gif" width="164" height=1></td>
  </tr>
  <tr> 
    <td colspan="3" bgcolor="#F0F0EB"> 
      <table width="794" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="164"><img src="/img/global/tr.gif" width=164 height=1></td>
          <td width="90"><img src="/img/global/tr.gif" width=90 height=1></td>
          <td width="90"><img src="/img/global/tr.gif" width=90 height=1></td>
          <td width="90"><img src="/img/global/tr.gif" width=90 height=1></td>
          <td width="90"><img src="/img/global/tr.gif" width=90 height=1></td>
          <td><img src="/img/global/tr.gif" width=90 height=1></td>
          <td><img src="/img/global/tr.gif" width=90 height=1></td>
          <td><img src="/img/global/tr.gif" width=90 height=1></td>
        </tr>
        <tr> 
          <td colspan="2"><a href="/"><img height=57 src="/img/global/kopf.gif" width="254" height="57" border="0" alt="home"></a></td>
          <td>&nbsp;</td>
          <td colspan="4"> 
				  <table border="0" cellspacing="0" cellpadding="0">
				    <tr>
				  <form method="get" action="/suchen/default.asp" name="searchfrm"> 
				      <td align="right"><br>
				        <div class="vorspann">Schnellsuche:&nbsp;</div>
				      </td>
				      <td> 
				        <input type="hidden" name="boolean" value="und" size=8>
				        <br>
				        <input type="text" name="qs" size="18" maxlength="60" value="">
				      </td>
				      <td><br>
				        <input type="image" src="/img/home/go.gif" border="0" width="30" height="32" align="left" alt="suchen in www.aerztezeitung.de">
				      </td></form>
				    </tr>
				  </table>
				</td>
			<td bgcolor="#F0F0EB"><a href="http://www2.aerztezeitung.de/"><img src="/img/global/aerzte2.gif" width="90" height="57" border="0"></a></td>
        </tr>
      <tr> 
        <td bgcolor="#F0F0EB"> 
          <div class="ausgabe"><img src="/img/global/tr.gif" width="164" height="1"><br>
            &nbsp;Update: 09.08.2002</div></td>
        <td bgcolor="#336699" align="center"><font face="Verdana, Arial, sans-serif" color="#FFFFFF" size="1" class="topnav"><b><a href="/">HOME</a></b></font></td>
        <td bgcolor="#003366" align="center"><font color="#FFFFFF" face="Verdana, Arial, sans-serif" size="1" class="topnav"><b><a href="/medizin/">MEDIZIN</a></b></font></td>
        <td bgcolor="#C74F04" align="center"><font face="Verdana, Arial, sans-serif" size="1" color="#FFFFFF" class="topnav"><b><a href="/politik/">POLITIK 
            &amp;<br>
            GESUNDHEIT</a></b></font></td>
        <td bgcolor="#336666" align="center"><font color="#FFFFFF" size="1" face="Verdana, Arial, sans-serif" class="topnav"><b><a href="/computer/">COMPUTER</a></b></font></td>
        <td bgcolor="#CC3333" align="center"><font color="#FFFFFF" size="1" face="Verdana, Arial, sans-serif" class="topnav"><b><a href="/geldundrecht/">RECHT</a></b></font></td>
        <td bgcolor="#669966" align="center"><font color="#FFFFFF" face="Verdana, Arial, sans-serif" size="1" class="topnav"><b><a href="/magazin/">MAGAZIN</a></b></font></td>
        <td bgcolor="#869193" align="center"><font color="#FFFFFF" face="Verdana, Arial, sans-serif" size="1" class="topnav"><b><a href="http://www2.aerztezeitung.de/">F&Uuml;R &Auml;RZTE</a></b></font></td>
      </tr>
      </table>
    </td>  
  </tr>
<!-- end top --><tr><td width=164 valign="top" bgcolor="#F0F0EB"><!-- begin navll local -->
<table width="164" border=0 cellspacing=0 cellpadding=0><tr><td bgcolor="#003366" width=12>&nbsp;</td><td bgcolor="#003366"><font color="FFFFFF" face="Verdana, Arial" size=2 class="mentop">MEDIZIN</font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=151 height=12><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/2pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/medizin/gentechnik/">Gentechnik</a></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/medizin/gentechnik/abc_gentechnik/">ABC der Gentechnik</a><br>&nbsp;<a href="/medizin/gentechnik/gendiagnostik/">Gendiagnostik</a><br>&nbsp;<a href="/medizin/gentechnik/gentherapie/">Gentherapie</a><br>&nbsp;<a href="/medizin/gentechnik/klonen/">Klonen</a><br><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><b>&nbsp;Siehe auch&#58; </b></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/medizin/stammzellen/">Stammzellen</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=18></font></td></tr></table><table width="100%" border=0 cellspacing=0 cellpadding=0><tr><td bgcolor="#003366" width=12>&nbsp;</td><td bgcolor="#003366"><font color="FFFFFF" face="Verdana, Arial" size=2 class="mentop">SUCHE IM THEMA</font></td></tr><tr><form name="searchcat" action="/medizin/gentechnik/default.asp" method="get"><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=12><input type="hidden" name="mode" value="query"><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><b>&nbsp;Gentechnik</b></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1></font><input type="text" name="qc" size=11 style="WIDTH: 100px" value=""><input type="image" src="/img/medizin/go.gif" border=0 width=30 height=32 align="top" alt="suchen" name="go"></td></form></tr></table><!-- begin navgl global -->
<script language="javascript" type="text/javascript"> 
<!-- 
function validateIt(radiogrp) 
{ 
    for (var i=0;i<radiogrp.length;i++) { 
        if (radiogrp[i].checked && radiogrp[i].value != '') 
        {   window.open("/umfrage/summary.asp?nOPId=12&os=" + radiogrp[i].value + "&strCat=home","Ergebnisse","scrollbars=yes,directories=no,menubar=no,resizable=yes,width=600,height=300"); 
            return false; 
        } 
    } 
    window.open("/umfrage/summary.asp?nOPId=12&strCat=home","Ergebnisse","scrollbars=yes,directories= no,menubar=no,resizable=yes,width=600,height=300"); 
    return false; 
} 
//--> </script> 
<table width="100%" border=0 cellspacing=0 cellpadding=0><tr><form name="opform" action="" method="post" onSubmit="return validateIt(document.opform.os)">
<input type="hidden" name="nOPId" value="12">
<input type="hidden" name="strCat" value="home">
<input type="hidden" name="strColor" value="#336699">
<td bgcolor="#003366" width=12>&nbsp;</td><td bgcolor="#003366"><font color="FFFFFF" face="Verdana, Arial" size=2 class="mentop">Aktuelle Umfrage</font></td></tr><tr><td bgcolor="#A6B3C1" width=12><br><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><p><font class="mensub" face="Verdana, Arial" size="1"><br><b>Soll in Deutschland an Embryonalen Stammzellen geforscht werden?
</b></font></p>
</td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><br><input type="radio" name="os" value="1" >Ja, weil man damit vielleicht einmal schwer kranken Menschen helfen kann.<br>
 
<br><input type="radio" name="os" value="2" >Nein, an werdendem Leben darf nicht geforscht werden.<br>
 
<br><input type="radio" name="os" value="3" >Wei� nicht, dazu habe ich (noch) keine Meinung.
<br>
 
<br></td></tr>
<tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=12 height=1></td><td bgcolor="#F0F0EB"><input type=image src="/img/medizin/go.gif" width=30 height=32 border=0 alt="Stimme abgeben" align="right"><font class="smalltext" face="Verdana, Arial" size=-1><i><b>Abstimmen und<br>Auswertung<br>ansehen!</i></b></font></td>
</form></tr>
<tr>
<td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=12 height=1></td>
<td>&nbsp;</td></tr>
<tr>
<td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=12 height=1></td>
<td><font face="Verdana, Arial" size=2 class="smalltext">
Mehr Informationen zur Stammzellforschung finden Sie <a href="/medizin/stammzellen/">hier
<img src="/img/home/3_pfeil.gif" border="0" alt="hier"></a></font></td></tr>
<tr>
<td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=12 height=1></td><td>&nbsp;</td></tr>
</table>

<!-- <TABLE cellSpacing="0" cellPadding="0" width="164" border="0">
  <TR>
    <TD bgcolor="#003366" width="12">&nbsp;</TD>
    <TD bgColor="#003366" width="152"><FONT class="mentop" face="Verdana, Arial" color="#ffffff"
      size="2">Messe &amp; Kongre&szlig;</FONT></TD>
  </TR>
<tr>
<td colspan="2">
      <table width="164" border=0 cellspacing=0 cellpadding=0>
        <tr> 
          <td bgcolor="#A6B3C1" width="12"><img src="/img/global/tr.gif" width="12" height="1"></td>
          <td bgcolor="#F0F0EB" width="6"><img src="/img/global/tr.gif" width="6" height="1"></td>
          <td bgcolor="#F0F0EB" width="146"><img src="/img/global/tr.gif" width="146" height="1"></td>
        </tr>
        <tr> 
          <td bgcolor="#A6B3C1" width="12">&nbsp;</td>
          <td bgcolor="#F0F0EB" width="6">&nbsp;</td>
          <td width="146" bgcolor="#F0F0EB" align="left"><a href="/medizin/medica_aktuell/mittwoch/"><img src="/medizin/Medica_aktuell/local/logo145x60.gif" width="145" height="60" border="0" alt="Zur Medica"></a></td>
        </tr>
<tr> 
          <td bgcolor="#A6B3C1" width="12"><img src="/img/global/tr.gif" width="12" height="10"></td>
          <td bgcolor="#F0F0EB" width="6"><img src="/img/global/tr.gif" width="6" height="1"></td>
          <td bgcolor="#F0F0EB" width="146"><img src="/img/global/tr.gif" width="146" height="1"></td>
        </tr>
        <tr> 
          <td bgcolor="#A6B3C1" width="12">&nbsp;</td>
          <td bgcolor="#F0F0EB" width="6">&nbsp;</td>
          <td bgcolor="#F0F0EB"><font face="Verdana, Arial, Helvetica, sans-serif" size="1" class="smalltext">Das 
            Wichtigste und Interessanteste von der gr��ten Medizinmesse der Welt <a href="/medizin/medica_aktuell/mittwoch/"><img src="/img/home/3_pfeil.gif" border=0 height=9 width=27 alt="zur Medica"></a></font></td>
        </tr>
<tr> 
          <td bgcolor="#A6B3C1" width="12"><img src="/img/global/tr.gif" width="12" height="5"></td>
          <td bgcolor="#F0F0EB" width="6"><img src="/img/global/tr.gif" width="6" height="1"></td>
          <td bgcolor="#F0F0EB" width="146"><img src="/img/global/tr.gif" width="146" height="1"></td>
        </tr>
      </table></td>
</tr>
</table> -->

<table width="164" border=0 cellspacing=0 cellpadding=0><tr><td bgcolor="#003366" width=12>&nbsp;</td><td bgcolor="#003366"><font color="FFFFFF" face="Verdana, Arial" size=2 class="mentop">SERIE</font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=151 height=12><br><a href="/series/default.asp?sdid=6"><img src="/img/series/6.gif" border="0" vspace="0"></a><br><img src="/img/global/tr.gif" width=1 height=18></font></td></tr></table><table width="164" border=0 cellspacing=0 cellpadding=0><tr><td bgcolor="#003366" width=12>&nbsp;</td><td bgcolor="#003366"><font color="FFFFFF" face="Verdana, Arial" size=2 class="mentop">SERIE</font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=151 height=12><br><a href="/series/default.asp?sdid=10"><img src="/img/series/10.gif" border="0" vspace="0" alt="Strategien gegen Krebs"></a><br><img src="/img/global/tr.gif" width=1 height=18></font></td></tr></table><table width="164" border=0 cellspacing=0 cellpadding=0><tr><td bgcolor="#003366" width=12>&nbsp;</td><td bgcolor="#003366"><font color="FFFFFF" face="Verdana, Arial" size=2 class="mentop">SERIE</font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=151 height=12><br><a href="/series/default.asp?sdid=9"><img src="/img/series/9.gif" border="0" vspace="0"></a><br><img src="/img/global/tr.gif" width=1 height=18></font></td></tr></table><table width="164" border=0 cellspacing=0 cellpadding=0><tr><td bgcolor="#003366" width=12>&nbsp;</td><td bgcolor="#003366"><font color="FFFFFF" face="Verdana, Arial" size=2 class="mentop">TEST</font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=151 height=12><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><b>&nbsp;<div class="homebody">SODBRENNEN<br>ist ein Alarmzeichen!</div><font face="Verdana, Arial, Helvetica, sans-serif" size="1" class="smalltext">Wollen Sie wissen, ob Sie gef&auml;hrdet sind?</font></b></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/medizin/magen_darm/sodbrennen/aktion/einleitung.asp"><div class="smalltext">Der Test dauert nur Sekunden.</div></a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=18></font></td></tr></table>


<!-- end navgl global --><!-- begin navgl -->
<table width="164" border=0 cellspacing=0 cellpadding=0><tr><td bgcolor="#CC9933" width=12>&nbsp;</td><td bgcolor="#CC9933"><font color="FFFFFF" face="Verdana, Arial" size=2 class="mentop">SERVICE</font></td></tr><tr><td bgcolor="#DBC598" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=151 height=12><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img height=13 src="/img/service/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/service/abo/">Abo-Service</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img height=13 src="/img/service/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/service/impressum/">Impressum</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img height=13 src="/img/service/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/service/jobs/">Jobs bei uns</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img height=13 src="/img/service/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/service/kontakt/">Kontakt zu uns</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img height=13 src="/img/service/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/service/mediadaten/">Mediadaten Online</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img height=13 src="/img/service/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/service/mediadaten_print/">Mediadaten Print</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img height=13 src="/img/service/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/service/newsletter/">Newsletter</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img height=13 src="/img/service/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/service/links/">N&uuml;tzliche Links</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img height=13 src="/img/service/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/service/sitemap/">Sitemap</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#DBC598" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=18></font></td></tr></table>
<!-- end navgl -->
<!-- end navll local -->
<img src="/cgi-bin/ivw/CP/20000724/medizin/gentechnik/Z/Z/73892230.ivw" border=0 height=1 width=1>
<!-- IVW VERSION="1.2" -->
<SCRIPT LANGUAGE="JavaScript"> <!--
var IVW="http://arztzeit.ivwbox.de/cgi-bin/ivw/CP";
document.write("<IMG SRC=\""+IVW+"?r="+escape(document.referrer)+"\" WIDTH=\"1\" HEIGHT=\"1\">");
// -->
</SCRIPT>
<NOSCRIPT>
<IMG SRC="http://arztzeit.ivwbox.de/cgi-bin/ivw/CP" WIDTH="1" HEIGHT="1">
</NOSCRIPT>
<!-- /IVW -->
</td><td valign="top" width="100%"><table width="100%" border=0 cellspacing=0 cellpadding=0><tr><td bgcolor="#003366" width=12>&nbsp;</td></tr><tr><td><img src="/img/global/tr.gif" height=12 width=8></td></tr><tr><td><font face="Verdana, Arial" size=2 class="mentop"><img src="/img/global/tr.gif" height=1 width=8><a href="/">Home</a>&nbsp;&gt;&nbsp;<a href="/medizin/">Medizin</a>&nbsp;&gt;&nbsp;<a href="/medizin/gentechnik/">Gentechnik</a></font></td></tr></table>
<table width="100%" border="0" cellspacing="10" cellpadding="0"><tr>
<td>
<table border=0 cellPadding=0 cellSpacing=0 width="100%">
<tr>
<td><hr noshade size=1><img src="/img/global/tr.gif" height=1 width=1></td>
</tr>
<tr>
<td valign="top"><font face="Verdana, Arial" size=2 class="ausgabe">&Auml;rzte Zeitung, 24.07.2000</font></td></tr><tr><td valign="top"><hr noshade size=1></td></tr>
</table>
<div class="medizinbody">
<!-- begin story-->

<H4><U> Hintergrund </U></H4>
<H2> Fischer und D&auml;ubler-Gmelin streiten um die Umsetzung der EU-Biopatentrichtlinie </H2>
<P><B><I>Von Karl H. Br&uuml;ckner </I></B></P>
<P> Bundesjustizministerin Herta D&auml;ubler-Gmelin (SPD) ist auf ihre Kabinettskollegin Andrea Fischer (B&uuml;ndnis 90/Die Gr&uuml;nen) nicht gut zu sprechen. Die Gesundheitsministerin hat sich an die Spitze der Ablehnungsfront gegen den vom Justiz-Ressort vorgelegten Gesetzentwurf zur Umsetzung der Biopatentrichtlinie der Europ&auml;ischen Union gesetzt.</P>
<H4>Was eine Erfindung ist, ist genau definiert </H4>
<P>Eigentlich mu&szlig; diese EU-Richtlinie bis Ende Juli europaweit in nationales Recht umgesetzt werden. Bisher hat dies nur D&auml;nemark geschafft. Auch die Bundesrepublik wird den Termin nicht einhalten k&ouml;nnen, obwohl D&auml;ubler-Gmelin den Entwurf vor drei Monaten vorgelegt hat. Die Gesundheitsministerkonferenz der L&auml;nder, Bundes&auml;rztekammer, Wissenschaftler, Greenpeace und Bundestagsabgeordnete machen gegen den &quot;Gesetzentwurf zur Umsetzung der Richtlinie &uuml;ber den rechtlichen Schutz biotechnologischer Erfindungen&quot; mobil. Auch im Forschungsministerium gibt es Bedenken. Ressortchefin Edelgard Bulmahn (SPD) und D&auml;ubler-Gmelin wollen zun&auml;chst eine Experten-Gruppe einzusetzen. Sie soll die Einw&auml;nde pr&uuml;fen.</P>
<P>Worum geht die Auseinandersetzung? Die am 6. Juli 1998 nach zehnj&auml;hriger Kontroverse vom Europaparlament verabschiedete EU-Richtlinie legt fest, da&szlig; unter bestimmten Bedingungen auch Gene patentiert werden k&ouml;nnen. Juristen, die forschende Industrie und viele Wissenschaftler halten diese Regelung f&uuml;r unverzichtbar. &quot;Ohne Patente auf DNA-Sequenzen gibt es keine neuen Arzneimittel&quot;, sagt etwa Joseph Straus vom Max-Planck-Institut f&uuml;r ausl&auml;ndisches Patent- Urheber- und Wettbewerbsrecht in M&uuml;nchen. Ebenso sieht es Christian Stein vom deutschen Humangenomprojekt. Nur ein Patent, das die DNA-Sequenz einschlie&szlig;e, ermutige Firmen, in medizinische Gen-Anwendungen zu investieren. &quot;Wenn Sie nur das Produkt sch&uuml;tzen, nicht aber die Idee daf&uuml;r&quot;, erl&auml;utert Straus, &quot;dann w&uuml;rde jeder versuchen, aus der verwendeten Gensequenz ein &auml;hnliches Produkt zu entwickeln&quot;.</P>
<P>Fischer und andere Kritiker bef&uuml;rchtet, da&szlig; weitgehende Bio-Patentrechte Forschung und Entwicklung gentechnisch hergestellter Arzneimittel behindern k&ouml;nnten. Will n&auml;mlich ein weiteres Unternehmen aus dem bereits patentierten Gen ein anderes Pr&auml;parat entwickeln, mu&szlig; es an den Patentinhaber Lizenzgeb&uuml;hren zahlen. Allerdings kann der Staat eine Zwangslizenz durchsetzen, wenn ein &ouml;ffentliches Interesse vorliegt. Der Zweite ist also dem Patentinhaber nicht ausgeliefert.</P>
<P>Fischer sieht die Gefahr von &quot;Monopolen auf Basisinformationen &uuml;ber den Menschen&quot;. Und der SPD-Bundestagsabgeordnete und Arzt Dr. Wolfgang Wodarg (SPD) l&auml;&szlig;t an der EU-Richtlinie kein gutes Haar. Er behauptet, die bisher f&uuml;r das Patentrecht konstitutive Unterscheidung zwischen der (nicht patentierbaren) &quot;Entdeckung&quot; und der (patentierbaren) &quot;Erfindung&quot; werde verwischt.</P>
<P>Genau dies sei nicht der Fall, so das Justizministerium. Nach wie vor seien f&uuml;r die Erteilung eines Patents vier Bedingungen zu erf&uuml;llen: Neuheit, erfinderische T&auml;tigkeit, gewerbliche Anwendbarkeit, ferner mu&szlig; die Erfindung auf einer Technik basieren. Anders gesagt: Die Entschl&uuml;sselung der Zusammensetzung eines Gens bleibt eine unpatentierbare Entdeckung. Um ein Patent zu erhalten - das sich dann allerdings auch auf das Gen erstreckt - mu&szlig; der Antragsteller angeben, mit welchen technischen Mitteln er welche Ziele erreichen will, und er mu&szlig; wissen, welche Funktion das einzusetzende Gen hat.</P>
<P>Die Bef&uuml;rchtung, Biopatente w&uuml;rden den freien Zugang zu Rohdaten des menschlichen Erbguts behindern, halten praktisch alle Experten f&uuml;r unbegr&uuml;ndet. Auch beim deutschen Humangenomprojekt (HGP) glaubt das niemand. Das HGP betreibt sogar eine eigene Patent- und Lizenzagentur in M&uuml;nchen, &uuml;ber die bereits mehrere hundert Gene zum Patent angemeldet worden sind. &quot;Das erfolgreiche Beispiel der T&auml;tigkeit&quot; dieser Agentur im HGP &quot;sollte Schule machen&quot;, lobte k&uuml;rzlich Wolf-Michael Catenhusen, Biotechnologie-Experte der SPD-Bundestagsfraktion und Parlamentarischer Staatssekret&auml;r im Forschungsministerium. Nach Angaben von Christian Stein, dem Leiter der Agentur, gibt es heute in Deutschland bereits etwa 1000 Patente und Patentantr&auml;ge auf Gene. Der Wettbewerb sei nicht behindert worden.</P>
<P>Auch deshalb macht der Widerstand gegen die EU-Richtlinie und den Entwurf f&uuml;r ihre Umsetzung in Deutschland kaum Sinn:</P>
<H4>Die Richtlinie sagt auch, was unerlaubt ist </H4>
<P>Weltweit sind Erfindungen auf dem Gebiet der belebten Natur patentierbar - entweder gesetzlich geregelt oder durch die Rechtsprechung. Auch in Deutschland gilt dies bereits seit drei Jahrzehnten. Die EU-Richtlinie kodifiziert insofern nur, was l&auml;ngst Praxis ist. Und sie verbietet explizit unter anderem die Patentierbarkeit des Klonens von Menschen und Embryonen, Patente auf Keimbahntherapien beim Menschen sowie die Verwendung menschlicher Embryonen zu kommerziellen Zwecken.</P>
<P><CENTER>
<TABLE BGCOLOR="#F0F0EB" BORDER=0 CELLSPACING=3 CELLPADDING=10 WIDTH=90%>
<TR><TD>
<FONT SIZE=+1 FACE="Verdana, Arial, Helvetica, sans-serif"><U>Stichwort</U></FONT></P>
<H2><font face="Verdana, Arial, Helvetica, sans-serif">Biopatente</font></H2>
<P><FONT SIZE=-1 FACE="Verdana, Arial, Helvetica, sans-serif">
 In Deutschland sind 60 gentechnisch hergestellte Arzneimittel auf dem Markt. Etwa 350 Pr&auml;parate sind weltweit in der Entwicklung. Der Anteil gentechnischer Diagnostika am Weltmarkt liegt bei etwa einem Drittel. Experten erwarten, da&szlig; in knapp 20 Jahren die H&auml;lfte aller Medikamente gentechnisch produziert werden. Darum hat die Industrie die Umsetzung der EU-Biopatentrichtlinie als positives Signal f&uuml;r Patienten und den Forschungsstandort Deutschland begr&uuml;&szlig;t. Der Entwurf schaffe Rechtssicherheit f&uuml;r die Entwicklung gentechnischer Pr&auml;parate und stelle Entwicklung und Produktion innovativer Medikamente sicher.  (rv/br)</FONT></P>
<P></TD></TR>
</TABLE>
</CENTER></P>

<!-- end story-->
</div></td></tr>
<tr><td valign="top"><!-- begin footer -->
		<hr noshade size="1pt">
		<table border="0" cellPadding="0" cellSpacing="0" width="100%">
              <tr align="left">
                <td vAlign="top" width="13"><a href="#top" title="Seitenanfang"><img align="left" height="12" border="0" src="/img/global/zumkopf.gif" width="13" alt="zum Seitenanfang"><font color="#336699" face="Verdana, Arial" size="1" class="smalltext">zum&nbsp;Seitenanfang</font></a></td>
                <td align="right" vAlign="top"><font face="Verdana, Arial" size="1" class="smalltext">Copyright
                  &#169; 1997-2002 by �rzte Zeitung</font></td></tr>
        </table>
        <hr noshade size="1pt">
<!-- end footer --></td></tr></table>
</td><td valign="top" width=164>
<table border=0 cellpadding=0 cellspacing=0 width="100%"><tr><td bgcolor="#003366">&nbsp;</td></tr></table>
<br><!-- begin adsr -->

<table border=0 cellpadding=0 cellspacing=0 width=164>
<tr>
	<td valign="top">
	<!-- r1 --><!-- /r1 -->
	</td>
</tr>
<tr>
	<td valign="top">
	<!-- r2 --><!-- /r2 -->    
	</td>
</tr>
</table>
<!-- end adsr --><table width="164" border="0" cellspacing="0" cellpadding="0"><tr><td valign="top"><img src="/img/global/tr.gif" height=8 width=10"><br>&nbsp;<a href="/docs/2000/07/24/135a0203.asp?nproductid=1209&#38;narticleid=114709"><img src="/img/print.gif" width="22" height="15" border="0" alt="Druckversion"></a> <a href="/docs/2000/07/24/135a0203.asp?nproductid=1209&#38;narticleid=114709"><font class="mensub" face="Verdana, Arial" size=1>Druckversion</font></a> <br>&nbsp;<a href="/mail/default.asp?nproductid=1209&#38;narticleid=114709"><img src="/img/mail.gif" width="22" height="15" border="0" alt="Versenden"></a> <a href="/mail/default.asp?nproductid=1209&#38;narticleid=114709"><font class="mensub" face="Verdana, Arial" size=1>Versenden</font></a> </td></tr></table><br><!-- begin navlr local -->
<!-- include virtual ="/local/navgr.asp" -->

<script language="JavaScript1.2">
<!-- hide it
var crWin;

function open_window(url) {
		if (!crWin || crWin.closed) {
			crWin = window.open(url,"eMail",'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,width=300,height=330');
		}
		else {
			crWin.location.href = url;
			crWin.focus();
		}
}
// -->
</script>

<table width="164" border=0 cellspacing=0 cellpadding=0><tr><td bgcolor="#003366" width=12>&nbsp;</td><td bgcolor="#003366"><font color="FFFFFF" face="Verdana, Arial" size=2 class="mentop">Chromosomen und assoziierte Krankheiten</font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=151 height=12><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom1.htm')">Chromosom 1</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom2.htm')">Chromosom 2</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom3.htm')">Chromosom 3</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom4.htm')">Chromosom 4</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom5.htm')">Chromosom 5</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom6.htm')">Chromosom 6</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom7.htm')">Chromosom 7</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom8.htm')">Chromosom 8</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom9.htm')">Chromosom 9</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom10.htm')">Chromosom 10</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom11.htm')">Chromosom 11</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom12.htm')">Chromosom 12</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom13.htm')">Chromosom 13</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom14.htm')">Chromosom 14</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom15.htm')">Chromosom 15</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom16.htm')">Chromosom 16</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom17.htm')">Chromosom 17</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom18.htm')">Chromosom 18</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom19.htm')">Chromosom 19</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom20.htm')">Chromosom 20</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom21.htm')">Chromosom 21</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chrom22.htm')">Chromosom 22</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chromx.htm')">Chromosom X</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="javascript:open_window('/docs/2000/07/28/chromy.htm')">Chromosom Y</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><b>&nbsp;Lesen Sie dazu...</b></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img height=13 src="/img/medizin/pfeil.gif" vspace=2 width=12></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1>&nbsp;<a href="/docs/2000/07/28/139a1401.asp">Nur dicht gepackte<br>&nbsp;DNA ist als<br>&nbsp;Chromosom sichtbar</a><img src="/img/global/tr.gif" width=1 height=1></font></td></tr><tr><td bgcolor="#A6B3C1" width=12><img src="/img/global/tr.gif" width=1 height=1></td><td bgcolor="#F0F0EB"><font class="mensub" face="Verdana, Arial" size=1><img src="/img/global/tr.gif" width=1 height=18></font></td></tr></table>
<!-- end navlr local -->
</td></tr><tr><td bgcolor="#F0F0EB" valign="bottom" class="mentop">&nbsp;<a href="mailto:info@aerztezeitung.de"><img src="/img/mail.gif" width="22" height="15" border="0" alt="info@aerztezeitung.de"></a>&nbsp; <a href="mailto:info@aerztezeitung.de">Redaktion </a></td><td><!-- begin btnav -->
      <table width="100%" border="0" cellspacing="1" cellpadding="3" align="center" valign="bottom">
        <tr> 
          <td align="center">
            <font size="1" face="Verdana, Aria">
            <a href="/" class="home"><b>HOME</b></a>&nbsp;&nbsp; 
            </font>
            <font size="1" face="Verdana, Aria">
            <a href="/medizin/" class="medizin"><b>MEDIZIN</b></a>&nbsp;&nbsp;
            </font>
            <font size="1" face="Verdana, Aria">
            <a href="/politik/" class="politik"><b>POLITIK</b></a>&nbsp;&nbsp; 
            </font>
            <font size="1" face="Verdana, Aria">
            <a href="/computer/" class="computer"><b>COMPUTER</b></a>&nbsp;&nbsp;
            </font>
            <font size="1" face="Verdana, Aria">
            <a href="/geldundrecht/" class="geldundrecht"><b>RECHT</b></a>&nbsp;&nbsp;
            </font>
            <font size="1" face="Verdana, Aria">
            <a href="/magazin/" class="magazin"><b>MAGAZIN</b></a>&nbsp;&nbsp;
            </font>
            <font size="1" face="Verdana, Aria">
            <a href="/aerzte/" class="aerzte"><b>F&Uuml;R &Auml;RZTE</b></a>
            </font>
            </td>
        </tr>
      </table>
<!-- end btnav --></td><td>&nbsp;</td></tr></table>
<!-- end vframe -->
</body>
</html>
