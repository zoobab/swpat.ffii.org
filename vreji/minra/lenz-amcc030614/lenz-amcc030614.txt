
   #[1]RSS [2]Home [3]Amazon, AOL, Harry Potter [4]Japanese Copyright
   Reform

[5]Lenz Blog

   [6]� Amazon, AOL, Harry Potter | [7]Main | [8]Japanese Copyright
   Reform �

June 15, 2003

McCarthy Guardian Article

   Arlene McCarthy answers in an [9]article in the Guardian to the recent
   attack on her project to sell out the European software industry to
   the American software patent lobby by [10]Richard Stallman and Nick
   Hill.

   There is not much of a new idea in that article, so most of its
   arguments are already dealt with by what what [11]I have said before
   about her position.

   Therefore, I will focus only on the question of international
   competition.

   McCarthy writes:

     "We must legislate to ensure our inventors are not put at a
     disadvantage in the global market place. If we fail to offer
     European industry the possibility of patent protection, we will
     hand over our inventiveness and creativity to big business, who can
     cherrypick ideas and patent them. The perverse outcome would be
     that European originators of those inventions face infringements
     proceedings from the big players. It is this that will lead to job
     losses, less choice and higher prices!

   If we fail to provide the possibility of patent protection, no one can
   get software patents in Europe. No Europeans, no Americans, not IBM or
   Microsoft, not a college student with an idea. Nobody.

   The idea that software patents in Europe will only be applied for by
   Europeans and software patents in the U.S. only by Americans is
   obviously not correct. Actually, most of the illegally granted
   software patents in Europe are secured by American software firms.

   So how exactly is "big business" supposed to "cherry-pick and patent"?

   McCarthy's text could be read to make some sense if you understand
   "cherry-pick and patent" as "patent in the U.S." (or some other
   country that has legalized software patents).

   But if big business can do so, the original European developer can
   apply for a patent in the U.S. as well. At less cost as for a European
   patent.

   So let's try to get this right.

   If McCarthy's proposal of legalizing software patents sails through,
   everybody can start enforcing software patents in Europe with
   infringement lawsuits. Americans, Europeans, Asians, big business,
   high school students, everyone.

   But that's only the theoretical side.

   In practice, most of the illegally granted European software patents
   are owned by American companies. The practical effect of McCarthy's
   proposal will be a large net flow of license payments from the
   European to the American software industry.

   I fail to see how that will contribute to the competitiveness of the
   European industry.
   So it's exactly the other way around. McCarthy says she's concerned
   that Europeans should not be at a disadvantage. But her proposal, far
   from removing any disadvantage, will place an additional burden on
   European firms in their race to catch up with the American software
   industry.
   Posted by Karl-Friedrich Lenz at June 15, 2003 12:28 AM |
   [12]TrackBack
   Comments
   Post a comment
   Name:
   ____________________
   Email Address:
   ____________________
   URL:
   ____________________
   Comments:

   __________________________________________________
   __________________________________________________
   __________________________________________________
   __________________________________________________
   __________________________________________________
   __________________________________________________
   __________________________________________________
   __________________________________________________
   __________________________________________________
   __________________________________________________
   [_] Remember info?
   PREVIEW POST
   [BUTTON]

Verweise

   1. http://k.lenz.name/LB/index.rdf
   2. http://k.lenz.name/LB/
   3. http://k.lenz.name/LB/archives/000413.html
   4. http://k.lenz.name/LB/archives/000417.html
   5. http://k.lenz.name/LB/
   6. http://k.lenz.name/LB/archives/000413.html
   7. http://k.lenz.name/LB/
   8. http://k.lenz.name/LB/archives/000417.html
   9. http://www.guardian.co.uk/online/story/0,3605,975126,00.html
  10. http://k.lenz.name/LB/archives/000391.html#000391
  11. http://k.lenz.name/LB/archives/000333.html#000333
  12. http://k.lenz.name/LB/mt-tb.cgi?__mode=view&entry_id=415
