
<HTML>
<HEAD>
<TITLE>Commission on Intellectual Property</TITLE>

<script language="javascript">
<!--
// Navigateur < 4 
function fixmenu()
{
return true;
} 
//-->
</script>
 
</HEAD><body  bgcolor="#FFFFFF" BGPROPERTIES="fixed" link="#003F80" vlink="#003F80" alink="#003F80">
<p>
<P>
<IMG SRC="/library/banner720.gif" ALT="ICC" BORDER=0 ALIGN=bottom VSPACE=5 usemap="#TopICC"><BR> 
<TABLE BORDER=0 CELLSPACING=0 WIDTH=720>
  <TR> 
    <TD WIDTH=720> <CENTER>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="720" height="80">
          <param name="movie" value="/library/TopIcon_Menu/TopIcon_Menu.swf?uncacher=5/17/03 10:02:47 PM">
          <param name="quality" value="high">
          <embed src="/library/TopIcon_Menu/TopIcon_Menu.swf?uncacher=5/17/03 10:02:47 PM" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="720" height="80"></embed></object>
      </CENTER></TD>
  </TR>
 
</TABLE>
<map name="TopICC"> <area shape="rect" coords="574,48,613,62" href="/index.asp"  alt="ICC Home" TITLE="ICC Home"> 
<area shape="rect" coords="616,48,671,62" href="/home/menu_news_archives.asp"  alt="News Archive" TITLE="News Archive"> 
<area shape="rect" coords="672,48,721,62" href="/search/query.asp"  alt="Search" TITLE="Search"> 
</map>
 <table border="0" cellPadding="0" cellSpacing="0" width="720"> 
<tr> <td colspan="5" valign="top" align="center"></td></tr> 
<tr> <td height="1" colspan="5" valign="top" align="center" ><img src="/home/images/spot.gif" width="1" height="1"></td></tr> 
<tr> <td height="5" colspan="5" valign="top" align="center"></td></tr> 
<tr> <td width="180" vAlign="top"> <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="180" height="650" VISIBILITY:VISIBLE; ZINDEX:1>
  <param name="movie" value="/library/LeftMenu.swf?v=6">
  <param name="quality" value="high">
  <param name="menu" value="false">
  
  <embed src="/library/LeftMenu.swf?v=6" width="180" height="650" menu=false quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed></object>
 </td><td width = "4">&nbsp;</td><td width = "1" bgcolor="#EE9430" valign = "top"> 
<img height="5" width="1" src="/home/images/spot.gif"></td><td width = "4">&nbsp;</td><td width = "531" vAlign="top" align="left"> 
<script language=JavaScript>
<!--
//-->
</script>
<font size="4" face="Arial, Helvetica, sans-serif">Commission on Intellectual 
Property</font> 
<p><font size="2" face="Arial, Helvetica, sans-serif"><b><font size="3">Commission 
  officers</font><br>
  <br>
  </b></font></p>
<table width="130" border="1">
  <tr>
    <td><img src="/home/intellectual_property/ilmonen.jpg" width="130" height="136"></td>
  </tr>
  <tr>
    <td><font size="1" face="Arial, Helvetica, sans-serif">Chair, Urho Ilmonen</font></td>
  </tr>
</table>
<p><font size="2" face="Arial, Helvetica, sans-serif"><b>Chair,</b> Urho Ilmonen, 
  Vice-President Legal, Group Counsel, Nokia Mobile Phones Ltd., Nokia Group, 
  Finland. <a href="/home/intellectual_property/Urho_ilmonen_bio.asp.asp"><font size="1">(Click 
  here for biography)</font></a><br>
  <b>Vice-Chairs,</b> German Cavelier, Alternate Director, Cavelier Abogados, 
  Colombia</font><font size="2" face="Arial, Helvetica, sans-serif"><b><br>
  Rapporteur,</b> Thomas Pletscher, Economiesuisse, Switzerland<b><br>
  Commission Secretary, </b>Daphne Yong-d'Herv&eacute;<b><br>
  </b></font></p>
<p><font size="2" face="Arial, Helvetica, sans-serif"><b><br>
  </b>The Commission on Intellectual and Industrial Property brings together leading 
  experts from all over the world to promote an environment favorable to for the 
  protection of intellectual property on national, regional and international 
  levels. It believes that the protection of intellectual property stimulates 
  international trade, creates a favorable climate for foreign direct investment, 
  and encourages innovation and technology transfer.</font></p>
<p><font size="2" face="Arial, Helvetica, sans-serif">The ICC works closely with 
  intergovernmental and non-governmental organisations involved in intellectual 
  property policy, such as the World Intellectual Property Organisation (WIPO), 
  the World Trade Organisation (WTO), the World Customs Organisation (WCO), the 
  UN Economic Commission for Europe (UNECE), the International Association for 
  the Protection of Industrial Property (AIPPI) and the Licensing Executive Society 
  (LES).</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="3"><b>Priorities</b></font></p>
<ul>
  <li><font face="Arial, Helvetica, sans-serif" size="2" color="#000000">examine 
    the intellectual property issues arising from electronic commerce</font></li>
  <li><font face="Arial, Helvetica, sans-serif" size="2" color="#000000">examine 
    the intellectual property implications of environmental protection and economic 
    development</font></li>
  <li><font face="Arial, Helvetica, sans-serif" size="2" color="#000000">raise 
    the profile of intellectual property rights as a business issue</font></li>
  <li><font color="#000000" face="Arial, Helvetica, sans-serif" size="2">implementation 
    of the Uruguay Round agreement on Trade-related Aspects of Intellectual Property 
    Rights (TRIPs)</font></li>
  <li><font color="#000000" face="Arial, Helvetica, sans-serif" size="2">environment-related 
    intellectual property issues such as the Biodiversity Convention and the protection 
    of biotechnological inventions, in cooperation with the Commission on Environment</font></li>
  <li><font color="#000000" face="Arial, Helvetica, sans-serif" size="2">enforcement 
    of intellectual property rights, and combating counterfeiting and piracy (in 
    liaison with <a href="../../index_ccs.asp">ICC Commercial Crime Services)</a></font></li>
</ul>
<p><font size="3" face="Arial, Helvetica, sans-serif"><font size="2"><b>For more 
  information please contact:</b><br>
  </font></font></p>
<table width="100%" border="0">
  <tr> 
    <td colspan="2"><b><font size="2" face="Arial, Helvetica, sans-serif">Daphne 
      Yong-d'Herv&eacute;, Senior Policy Manager</font></b></td>
  </tr>
  <tr> 
    <td width="20%"><font size="2"><font face="Arial, Helvetica, sans-serif">Tel&nbsp;</font></font></td>
    <td width="91%"><font size="2"><font face="Arial, Helvetica, sans-serif">+33 
      1 49 53 28 24</font></font></td>
  </tr>
  <tr> 
    <td width="20%"><font size="2"><font size="2"><font face="Arial, Helvetica, sans-serif">Fax</font></font></font></td>
    <td width="91%"><font size="2"><font face="Arial, Helvetica, sans-serif">+33 1 49 53 28 59</font></font></td>
  </tr>
  <tr> 
    <td width="20%"><font size="2" face="Arial, Helvetica, sans-serif">E-mail</font></td>
    <td width="91%"><font face="Arial, Helvetica, sans-serif" size="2"><a href="mailto:dye@iccwbo.org"><b>dye@iccwbo.org</b></a></font></td>
  </tr>
</table>
<p><font size="2"><font face="Arial, Helvetica, sans-serif"><b> </b></font></font><font size="2" face="Arial, Helvetica, sans-serif"><b>International 
  Chamber of Commerce<br>
  </b>38, Cours Albert 1er<br>
  75008 Paris<br>
  FRANCE</font></p>
<p>&nbsp;</p>
</td></tr></table>
<map name="MapNavBar">
<area shape="rect" coords="41,0,104,15" href="/home/intro_icc/introducing_icc.asp" alt="About ICC"> 
<area shape="rect" coords="544,0,596,15" href="/home/menu_news_archives.asp" alt="News Archives"> 
<area shape="rect" coords="468,0,534,15" href="http://www.iccbooks.com" target="_blank" alt="Bookstore"> 
<area shape="rect" coords="293,0,464,15" href="/index_ccs.asp" alt="CCS"> <area shape="rect" coords="110,0,151,15" href="/search/query.asp" alt="Search"> 
<area shape="rect" coords="2,0,35,15" href="/index.asp" alt="Home site"> </map> 
</body>
<script language=JavaScript>
<!--
var model = 2 ;
var menug =  14;
var menub =  0;
//-->
</script>
</HTML>
