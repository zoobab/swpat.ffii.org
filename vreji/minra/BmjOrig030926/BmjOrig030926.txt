
Patente f�r computergest�tzte Erfindungen?

   Das Europ�ische Parlament hat in seiner Sitzung am 24. September 2003
   in erster Lesung �ber �nderungsvorschl�ge zum Richtlinienvorschlag der
   Europ�ischen Kommission �ber die Patentierbarkeit
   computerimplementierter Erfindungen abgestimmt und den Text in
   ge�nderter Fassung mit deutlicher Mehrheit angenommen.

   Bei der Patentierbarkeit computerimplementierter Erfindungen handelt
   es sich um ein bereits seit l�ngerem kontrovers diskutiertes Thema,
   das besonders seit Vorlage des Richtlinienvorschlags der Europ�ischen
   Kommission im Februar 2002 gro�e Aufmerksamkeit erregt. Der h�ufig
   benutzte Begriff der Softwarepatente" ist dabei irref�hrend, da
   bereits nach heute geltendem Recht keine Patente auf reine Software,
   sondern nur auf mit Hilfe von Software realisierte technische
   Erfindungen erteilt werden d�rfen(beispielsweise die Programmsteuerung
   eines ABS-Bremssystems). Reine Quellcodes k�nnen nur
   urheberrechtlichem Schutz unterfallen.

   Eine Konkretisierung der Patentierungsvoraussetzungen und damit eine
   Harmonisierung des Patentrechts und der Patentpraxis in den
   EU-Mitgliedstaaten und der Europ�ischen Patentorganisation ist Ziel
   des Richtlinienentwurf. Dabei soll kein Sonderrecht f�r
   computerimplementierte Erfindungen geschaffen werden, sondern die
   allgemeinen Patentierungsvoraussetzungen sollen f�r die speziellen
   Bed�rfnisse dieses Anwendungsfeldes definiert werden. Die
   Patentierbarkeit reiner Software soll weiterhin ausgeschlossen sein,
   ebenso soll durch klare Regeln die Erteilung von Trivialpatenten, z.B.
   Patente auf reine Gesch�ftsmethoden, vermieden werden. Erw�nscht ist
   also keineswegs eine Ausweitung der derzeitigen Patentierungspraxis,
   eher eine Einschr�nkung derselben.

   Durch die genannte EU-Richtlinie soll ein Ausgleich geschaffen werden
   zwischen den Zielen, einerseits klassische" Erfindungen mit
   Computerbezug, die einen technischen Beitrag leisten, weiterhin durch
   Patente zu sch�tzen und andererseits die Entstehung einer
   Software-Monokultur durch eine zu umfassende Patentierungspraxis zu
   verhindern.

   In einem n�chsten Verfahrensschritt wird der Rat der Europ�ischen
   Union sich mit den durch das Parlament vorgenommenen �nderungen
   befassen. Der abge�nderte Text wird zun�chst auf Fachebene in der
   zust�ndigen Ratsarbeitsgruppe gepr�ft, bevor sich der EU-Ministerrat
   mit den �nderungen besch�ftigt.

   Den durch das Parlament angenommenen Text und den urspr�nglichen
   Richtlinienvorschlag der Kommission finden Sie hier.

   [1][10393.jpg] 

   Sie ben�tigen zum Betrachten bzw. Drucken den [2]"Adobe Acrobat
   Reader" ab Version 5!

   � 2001-2003, Bundesministerium der Justiz, Letzte �nderung: 26.09.2003

Verweise

   1. http://www.bmj.bund.de/images/11661.pdf
   2. http://www.adobe.de/products/acrobat/readstep.html
