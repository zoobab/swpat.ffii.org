
<HTML>
<HEAD>
<TITLE>EC directive on the patentability of computer related inventions</TITLE>

<script language="javascript">
<!--
// Navigateur < 4 
function fixmenu()
{
return true;
} 
//-->
</script>
<script type="text/javascript" src="/library/javascript/print_email_page.js"></script>  
</HEAD><body  bgcolor="#FFFFFF" BGPROPERTIES="fixed" link="#003F80" vlink="#003F80" alink="#003F80">
<p>
<P>
<IMG SRC="/library/banner720.gif" ALT="ICC" BORDER=0 ALIGN=bottom VSPACE=5 usemap="#TopICC"><BR> 
<TABLE BORDER=0 CELLSPACING=0 WIDTH=720>
  <TR> 
    <TD WIDTH=720> <CENTER>
        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="720" height="80">
          <param name="movie" value="/library/TopIcon_Menu/TopIcon_Menu.swf?uncacher=5/17/03 10:06:36 PM">
          <param name="quality" value="high">
          <embed src="/library/TopIcon_Menu/TopIcon_Menu.swf?uncacher=5/17/03 10:06:36 PM" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="720" height="80"></embed></object>
      </CENTER></TD>
  </TR>
 
</TABLE>
<map name="TopICC"> <area shape="rect" coords="574,48,613,62" href="/index.asp"  alt="ICC Home" TITLE="ICC Home"> 
<area shape="rect" coords="616,48,671,62" href="/home/menu_news_archives.asp"  alt="News Archive" TITLE="News Archive"> 
<area shape="rect" coords="672,48,721,62" href="/search/query.asp"  alt="Search" TITLE="Search"> 
</map>
 <table border="0" cellPadding="0" cellSpacing="0" width="720"> 
<tr> <td colspan="5" valign="top" align="center"></td></tr> 
<tr> <td height="1" colspan="5" valign="top" align="center" ><img src="/home/images/spot.gif" width="1" height="1"></td></tr> 
<tr> <td height="5" colspan="5" valign="top" align="center"></td></tr> 
<tr> <td width="180" vAlign="top"> <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="180" height="650" VISIBILITY:VISIBLE; ZINDEX:1>
  <param name="movie" value="/library/LeftMenu.swf?v=6">
  <param name="quality" value="high">
  <param name="menu" value="false">
  
  <embed src="/library/LeftMenu.swf?v=6" width="180" height="650" menu=false quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash"></embed></object>
 </td><span id = "ForPrinter"><td width = "4">&nbsp;</td><td width = "1" bgcolor="#EE9430" valign = "top"> 
<img height="5" width="1" src="/home/images/spot.gif"></td><td width = "4">&nbsp;</td><td width = "531" vAlign="top" align="left"> 
<p>
  <script language=JavaScript>
<!--
//-->
		
</script>
  <font face="Arial, Helvetica, sans-serif" size="2"><b>Department of Policy and 
  Business Practices</b></font> </p>
<p><font face="Arial, Helvetica, sans-serif" size="4">Comments on the draft EC 
  Directive on the Patentability of Computer Related Inventions<br>
  </font><font face="Arial, Helvetica, sans-serif" size="2" color="#EE9430">Prepared 
  by the Commission on Intellectual Property</font></p>
<p>&nbsp;</p>
<p></p>
<p><font face="Arial, Helvetica, sans-serif" size="3"><b><font color="#003F80">Introduction</font></b></font><font face="Arial, Helvetica, sans-serif" size="2"><br>
  Computer related inventions are essential tools for businesses and the backbone 
  of several industries. The question of intellectual property protection for 
  computer-implemented inventions (CIIs) is therefore of singular importance to 
  the business community worldwide.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">The discrepancies in current 
  patenting practices on computer- implemented inventions have been of great concern 
  to many governments and to businesses globally. The patent systems most concerned 
  by these practices are those in Europe, Japan and the US.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">The European Patent Office 
  and the Japan Patent Office both require that technical aspects be expressed 
  in a patent claim. While the European Patent Office (EPO) requires the claim 
  to specify a technical feature over and above that represented by the computer 
  alone, the Japan Patent Office is satisfied with a software invention provided 
  the patent claim specifies a computer. </font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">This approach can be contrasted 
  with the practice in the US. To obtain a patent in the United States, an invention 
  must be implicitly within the technological arts. Although a &quot;tangible 
  result&quot; is required, the invention does not have to provide a &quot;technical 
  contribution&quot; as such.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><br>
  <font color="#EE9430"><b><font size="3" color="#003F80">Situation in Europe 
  </font></b></font><br>
  Two factors have resulted in a certain ambiguity and lack of transparency in 
  the patenting of CIIs in the European Union. While Article 52 (2)(c) of the 
  European Patent Convention excludes &quot;computer programs as such&quot; from 
  patentable subject matter, the EPO has in practice granted many patents on computer 
  implemented inventions, by narrowly interpreting this exclusion. </font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"> Secondly, as national courts 
  of EC member states are not bound to follow the decision of EPO appellate bodies, 
  this has led in practice to divergences in the interpretation of the EPC and 
  consequently in the scope of protection accorded in different EU member states 
  to certain classes of invention, including CIIs.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">Following several years 
  of thoughtful discussion, the European Commission has now issued a draft Directive 
  on the Patentability of Computer Implemented Inventions as an attempt to codify 
  EPO practice and jurisprudence, and to harmonise EU member states' treatment 
  of CII patent applications. </font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><br>
  <font color="#003F80"><b><font size="3">General comments on EC draft directive</font></b></font><br>
  ICC supports the European Commission's objective to improve the transparency 
  and coherence of the EU system and to harmonize the approaches of EU national 
  courts. </font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">As stated in its July 2001 
  policy statement concerning software and business methods, ICC firmly believes 
  that patents provide an incentive for innovation by encouraging investment in 
  R&amp;D and promoting the dissemination of technology through the publication 
  of patents. Technologically innovative companies should be able to obtain patents 
  to protect their inventions without discrimination as to the technological field. 
  On the other hand, it has long been established that creations of a purely abstract 
  nature cannot be patented.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">ICC particularly supports 
  the Commission's view that a special set of rules for CIIs should not be created. 
  ICC's position is that computer-implemented inventions should not be treated 
  differently from any other inventions, and should be patentable as long as they 
  meet all of the usual requirements of patentability applied to other fields 
  of technology, including technical content. At the same time, however, it is 
  critical to ensure that the standard rules and requirements for patentability 
  are appropriately applied in examining applications in these fields.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">In parallel to the work 
  on the directive, ICC urges all interested parties to continue their efforts 
  to delete the exclusion of computer programmes in Article 52(2), to remove remaining 
  ambiguities in the system.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><br>
  <font color="#003F80"><b><font size="3">Specific comments on draft directive</font></b></font><br>
  ICC appreciates the fact that the draft directive addresses a difficult policy 
  area where there are divisions of opinion, and therefore reflects an attempt 
  to take into account the different interests involved. It is however essential 
  that the resulting text is clear, and accurately reflects current law and practice, 
  if it is to achieve its objective.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"> ICC would therefore like 
  to make the following suggestions to improve the wording in the current draft 
  directive and to bring it into line with the ICC opinion as expressed in its 
  July 2001 policy statement concerning software and business methods.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><b><font color="#EE9430">Article 
  2 - Definitions</font></b><br>
  This article provides the basis for the rest of the text and its clarity is 
  therefore essential.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><b>(a) &quot;Computer-implemented 
  invention&quot;</b><br>
  Although this definition appears to be derived from the EPO Guidelines for Examination, 
  it is, in ICC's view, too complex for the purposes of a directive which is intended 
  to increase transparency. ICC supports the alternative wording suggested by 
  the EPO for this article which it believes would improve its clarity. This reads 
  :</font></p>
<blockquote>
  <p><font face="Arial, Helvetica, sans-serif" size="2"> <font face="Times New Roman, Times, serif"><i>&quot;computer-implemented 
    invention&quot; means any invention the performance of which involves the 
    use of a computer, computer network or other programmable apparatus [and having 
    one or more prima facie novel features] which [are] is realised wholly or 
    partly by means of a computer program or computer programs.&quot;</i></font></font></p>
</blockquote>
<p><font face="Arial, Helvetica, sans-serif" size="2"><b>(b) &quot;Technical contribution&quot;</b><br>
  The requirement in this definition that the technical contribution should be 
  &quot;not obvious&quot; is confusing. Article 4(2) states that an &quot;inventive 
  step&quot; (equivalent to non-obviousness) must involve a &quot;technical contribution&quot; 
  and to then state in Article 2 that this technical contribution must itself 
  be non-obvious makes the combination of the two articles a circular statement. 
  ICC therefore proposes that the reference to non-obviousness in the definition 
  should be deleted and again supports the wording proposed by the EPO, as follows:</font></p>
<blockquote>
  <p><font face="Arial, Helvetica, sans-serif" size="2"> <font face="Times New Roman, Times, serif"><i>&quot;technical 
    contribution&quot; means a contribution to the state of the art in a [technical] 
    field [which is not obvious to a person skilled in the art] of technology.&quot;</i></font></font></p>
</blockquote>
<p><font face="Arial, Helvetica, sans-serif" size="2"><b><font color="#EE9430">Article 
  4 - Conditions for patentability</font></b><br>
  <font color="#EE9430"><br>
  Article 4(2)</font></font></p>
<blockquote>
  <p><font face="Times New Roman, Times, serif" size="2"><i>&quot;Member States 
    shall ensure that it is a condition of involving an inventive step that a 
    computer-implemented invention must make a technical contribution.&quot;</i></font></p>
</blockquote>
<p><font face="Arial, Helvetica, sans-serif" size="2" color="#EE9430">Article 
  4(3)</font></p>
<blockquote>
  <p><font face="Arial, Helvetica, sans-serif" size="2"> <font face="Times New Roman, Times, serif"><i>&quot;The 
    [technical contribution] inventive step shall be assessed by consideration 
    of the difference between the scope of the patent claim considered as a whole, 
    elements of which may comprise both technical and non-technical features, 
    and the state of the art.&quot;</i></font></font></p>
</blockquote>
<p><font face="Arial, Helvetica, sans-serif" size="2"> The technical implementation 
  must go beyond merely using a known computer in a straightforward manner to 
  implement the method. In such a case the technical contribution results from 
  the technical considerations that are at the root of the claimed invention. 
  This is widely accepted where mathematical concepts are involved and it appears 
  appropriate to apply the same reasoning to other kinds of methods, including 
  business methods.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><b><font color="#EE9430">Article 
  5 - Form of claims</font></b><br>
  ICC believes that the directive should follow current practice in the EPO and 
  a number of EU member states and make it clear that computer program products 
  can be claimed. To disallow such claims in the directive would create great 
  legal uncertainty for holders of such patents already granted. Prohibiting product 
  claims would also render enforcement of patents difficult and raise questions 
  with respect to TRIPS compliance. TRIPS requires patents not only to be available, 
  but also to be &quot;enjoyable&quot; in all areas of technology.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">If program claims are prohibited, 
  patent holders will in most cases be forced to defend their rights under the 
  patent by relying on &quot;contributory infringement&quot; arguments which are 
  often difficult to establish and, in any case, would only apply when the distributor 
  of the computer program and user of such program are located in the same member 
  state. It is unlikely that the patent holder would be able to assert his or 
  her rights in the very common cases where the distributor and user of the program 
  are located in different EU member states.</font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">The proposed wording of 
  Article 5 would allow the production and distribution in Europe of computer 
  programs which, when run in a computer, infringe valid patents. Consumers will 
  be put in the undesirable situation of purchasing computer programs in good 
  faith, but then finding themselves infringing a patent when computer programs 
  are executed by the consumer at home or in the workplace. </font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2">ICC therefore proposes the 
  following reformulation of Article 5:</font></p>
<blockquote>
  <p><font face="Arial, Helvetica, sans-serif" size="2"> <font face="Times New Roman, Times, serif"><i>&quot;Member 
    States shall ensure that a computer-implemented invention may be claimed using 
    any form of claim considered appropriate by the applicant . Suitable forms 
    of claims include, for example, claims to [as] a product, that is [as] a programmed 
    computer, a programmed computer network, [or] other programmed apparatus [or] 
    and a computer program, and claims to [as] a process carried out by [such] 
    a computer, computer network or apparatus through the execution of software.&quot;</i></font></font></p>
</blockquote>
<p><font face="Arial, Helvetica, sans-serif" size="2"><b><font color="#EE9430">Article 
  6 - Relationship with Directive 91/250 EC</font></b><br>
  Current wording of Article 6:</font></p>
<blockquote>
  <p><font face="Arial, Helvetica, sans-serif" size="2"> <font face="Times New Roman, Times, serif"><i>&quot;Acts 
    permitted under Directive 91/250/EEC on the legal protection of computer programs 
    by copyright, in particular provisions thereof relating to decompilation and 
    interoperability, or the provisions concerning semiconductor topographies 
    or trade marks, shall not be affected through the protection granted by patents 
    for inventions within the scope of this Directive.&quot;</i></font></font></p>
</blockquote>
<p><font face="Arial, Helvetica, sans-serif" size="2">ICC believes that the proposed 
  Article 6 should be reconsidered. One reason is that the scope of the provision 
  is not limited to just decompilation or interoperability; the draft article 
  generally provides that whatever is permitted under the copyright directive 
  shall also be permitted under the draft software patent directive. </font></p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><br>
  <font color="#003F80"><b><font size="3">Conclusion </font></b></font><br>
  ICC fully welcomes the current endeavours to provide certainty for the patentability 
  of computer-implemented inventions in Europe. It recognises that, for the directive 
  to realise its objective, it is of the utmost importance that the wording of 
  the proposal provides complete clarity for conditions of patentability. Ambiguous 
  wording could run the risk of introducing different requirements for the patentabilty 
  of CIIs than are applied to other fields of technology and may produce an undesired 
  retrospective effect on existing software patents granted by individual member 
  states and the EPO itself. ICC believes that the adoption of its suggestions 
  for alternative wording would reduce these risks, assisting the draft proposal 
  to attain its goal.</font></p>
<p></p>
<p>&nbsp;</p>
<p><font face="Arial, Helvetica, sans-serif" size="2"><b>Document n&deg; 450/953 
  Rev.</b><br>
  12 September 2002</font></p>
<p></p>
</td></span></tr></table>
<map name="MapNavBar">
<area shape="rect" coords="41,0,104,15" href="/home/intro_icc/introducing_icc.asp" alt="About ICC"> 
<area shape="rect" coords="544,0,596,15" href="/home/menu_news_archives.asp" alt="News Archives"> 
<area shape="rect" coords="468,0,534,15" href="http://www.iccbooks.com" target="_blank" alt="Bookstore"> 
<area shape="rect" coords="293,0,464,15" href="/index_ccs.asp" alt="CCS"> <area shape="rect" coords="110,0,151,15" href="/search/query.asp" alt="Search"> 
<area shape="rect" coords="2,0,35,15" href="/index.asp" alt="Home site"> </map> 
</body>
<script language=JavaScript>
<!--
var model = 2 ;
var menug =  0;
var menub =  0;
//-->
</script>
</HTML>
