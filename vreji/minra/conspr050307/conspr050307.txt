COUNCIL OF THE EUROPEAN UNION

EN
Brussels, 7 March 2005 7060/05 (Presse 57)

ADOPTION OF COUNCIL'S COMMON POSITION ON A DIRECTIVE ON THE PATENTABILITY OF COMPUTERIMPLEMENTED INVENTIONS

The Council adopted by qualified majority today its common position on a draft Directive laying down rules for the patentability of computer-implemented inventions, with Spain voting against and the Austrian, Italian and Belgian delegations abstaining1. The Council's common position will now be submitted to the European Parliament for second reading.

1

The Commission and the following delegations have entered a declaration to the Council's minutes: Denmark, Cyprus, Latvia, Hungary, the Netherlands, and Poland.

PRESS
Rue de la Loi 175 B � 1048 BRUSSELS Tel.: +32 (0)2 285 6219 / 6319 Fax: +32 (0)2 285 8026

press.office@consilium.eu.int http://ue.eu.int/Newsroom

7060/05 (Presse 57)

1

EN


The proposed Directive aims at ensuring an effective, transparent and harmonised protection of computer-implemented inventions throughout the Community so as to enable innovative enterprises to derive the maximum advantage from their inventive activity and provide an incentive for investment and innovation. Existing differences in the administrative practices and the case law of the different Member States regarding the patentability of computer-implemented inventions could create barriers to trade and hence impede the proper functioning of the internal market. The Council's common position lays down certain principles to apply to the patentability of computer-implemented inventions, intended in particular to ensure that such invention, which belong to the field of technology and provided they make a technical contribution, are susceptible to patent protection, and conversely to ensure that those inventions which do not make a technical contribution are not susceptible to protection. The key features of the common position are as follows: � Member States will be obliged to ensure in their national law that computerimplemented inventions are considered to belong to the field of technology. In order to be patentable, a computer-implemented invention must be new, susceptible to industrial application and must involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution to the state of the art. If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in claims. � In accordance with the European Patent Convention, a computer program as such cannot constitute a patentable invention. Inventions involving computer programs, whether expressed as source code, as object code or in any other form, which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run will not be patentable. � The Council has introduced a new provision in order to clarify that in certain circumstances and under strict conditions a patent can cover a claim to a computer program, be it on its own or on a carrier. The Council considers that this would align the Directive on standard current practice both of the European Patent Office and in the Member States.

7060/05 (Presse 57)

2

EN


� The Directive should be without prejudice to the application of Articles 81 and 82 of the Treaty, in particular where a dominant supplier refuses to allow the use of a patented technique which is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them. � The Commission will monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, on Community businesses, especially small and medium-sized enterprises, on the open-source community and on electronic commerce.

7060/05 (Presse 57)

3

EN


