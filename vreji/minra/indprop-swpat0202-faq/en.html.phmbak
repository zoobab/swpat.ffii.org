<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>

<!-- STEP 1: INSERT META DATA -->

<meta name="Creator" content="COMM/DGMARKT/A4">
<META NAME="DatePublication" CONTENT="1998-09-15">
<META NAME="Publisher" CONTENT="DGMARKT/A4/M.Heller">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html;CHARSET=iso-8859-1">
<META NAME="Language" CONTENT="en">

<!-- END OF META DATA SECTION -->

<!-- STEP 2: INSERT TITLE FOR BROWSER-->

<TITLE>EUROPA - Internal Market - Industrial Property - Patentability of computer-implemented inventions - Proposal for a Directive on the patentability of computer-implemented inventions - frequently asked questions</TITLE>


</HEAD>
<BODY BACKGROUND="/comm/images/navigation/backgr.gif" BGCOLOR="#FFFFFF"
 LINK="#0000FF" VLINK="#999999" TEXT="#000000">
<P><font face="Arial, Helvetica, sans-serif" size="-2"><A NAME="top"></A><STRONG>IMPORTANT 
  LEGAL NOTICE</STRONG> - The information on this site is subject to a <A HREF="/geninfo/disclaimer_en.htm">disclaimer</A> 
  and a <A HREF="/geninfo/copyright_en.htm">copyright notice</A>.</font></P>


<A NAME="top"></A>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR>
<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="118"><IMG SRC="/comm/images/navigation/euroflag.gif" WIDTH="55" HEIGHT="36" ALIGN="BOTTOM" BORDER="0" ALT="European Flag"><A HREF="/index-en.htm"><IMG SRC="/comm/images/navigation/europa.gif" WIDTH="63" HEIGHT="36" ALIGN="BOTTOM" ALT="Europa" BORDER="0"></A><BR>
<A HREF="/comm/index_en.htm"><IMG SRC="/comm/images/navigation/eucom_en.gif" WIDTH="118" HEIGHT="30" ALIGN="BOTTOM" ALT="The European Commission" BORDER="0"></A><BR>
<IMG SRC="/comm/images/navigation/ar_bott3.gif" WIDTH="118" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"><BR>

<!-- STEP 3: INSERT LINK, LOGO AND ALTERNATIVE TEXT OF DG -->

<A HREF="/comm/internal_market/en/index.htm"><IMG SRC="/comm/internal_market/images/intmar_en.gif" ALT="Internal Market" WIDTH="118" HEIGHT="40" ALIGN="BOTTOM"  BORDER="0"></A>
</TD>
<TD WIDTH="10" ROWSPAN="2" VALIGN="BOTTOM"><IMG SRC="/comm/images/navigation/15x10.gif" WIDTH="10" HEIGHT="10" ALIGN="BOTTOM" BORDER="0"></TD>
    <TD VALIGN="TOP" ROWSPAN="2" WIDTH="100%"> <!-- STEP 4: LINK TO AVAILABLE LANGUAGE VERSIONS --> 
      <IMG SRC="/comm/images/language/lang_1.gif" WIDTH="12" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"><A HREF="/comm/internal_market/de/indprop/comp/02-32.htm"><IMG SRC="/comm/images/language/lang_de1.gif" WIDTH="26" HEIGHT="19" ALIGN="BOTTOM" BORDER="0" ALT="DE"></A><IMG SRC="/comm/images/language/lang_en2.gif" WIDTH="26" HEIGHT="19" ALIGN="BOTTOM" BORDER="0" ALT="EN"><A HREF="/comm/internal_market/fr/indprop/comp/02-32.htm"><IMG SRC="/comm/images/language/lang_fr1.gif" WIDTH="26" HEIGHT="19" ALIGN="BOTTOM" BORDER="0" ALT="FR"></A> 
      <!-- END OF LANGUAGE SECTION --> <!-- STEP 5: REMOVE UNUSED LANGUAGE ICONS AND LINKS --> 
      <P>

<!-- STEP 6: INSERT BANNER AND ALTERNATIVE TEXT -->

<IMG SRC="/comm/internal_market/images/indpr_en.gif" ALT="Industrial Property" WIDTH="360" HEIGHT="70" ALIGN="BOTTOM">
</P>
      <P> <!-- STEP 7: INSERT DOCUMENT TITLE --> <font face="Arial, Helvetica, sans-serif" size="-1"><A NAME="top"></A></font>
      <table width="94%" border="0">
        <tr> 
          <td width="40%" height="15"><font face="Arial, Helvetica, sans-serif" size="-1"><img src="/comm/internal_market/images/bluearr.gif"
width="12" height="12" align="MIDDLE"><b> <a href="/comm/internal_market/en/indprop/index.htm">Industrial 
            Property</a></b></font></td>
          <td width="60%" height="15"><font face="Arial, Helvetica, sans-serif" size="-1"> 
            <img src="/comm/internal_market/images/bluearr.gif"
width="12" height="12" align="MIDDLE"> <a href="/comm/internal_market/en/indprop/comp/index.htm"><b>Patentability 
            of computer-implemented inventions</b></a> </font></td>
        </tr>
      </table>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><BR>
        </font></P>
      <H2><font color="#030E78" face="Arial, Helvetica, sans-serif" size="+2">Proposal 
        for a Directive on the patentability of computer-implemented inventions 
        - frequently asked questions</font></H2>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>What are the main 
        features of the proposal?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The central requirement 
        of the proposal is that, in order to be patentable, an invention that 
        is implemented through the execution of software on a computer or similar 
        apparatus has to make a contribution in a technical field that is not 
        obvious to a person of normal skill in that field. This is essentially 
        a legal question of a kind which is answered all the time by patent offices 
        and practitioners.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>What is the current 
        situation in the EU concerning patents on computer-implemented inventions?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">Inventors currently 
        have two possible routes for obtaining protection for their inventions. 
        Patents may be applied for, processed and granted either at the European 
        Patent Office (EPO) under the centralised system of the European Patent 
        Convention (EPC), or via national patent offices in the Member States 
        purely according to national law. However whichever route is chosen, national 
        law applies in all cases after grant. Thus, granted European Patents become 
        a "bundle" of national patents which have to be validated, maintained 
        and litigated separately in each Member State.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">Legally speaking, 
        the patent laws of the Member States are in principle supposed to be consistent 
        with the EPC and uniform among themselves. However, in practice there 
        is no unifying structure with binding effect on national courts and there 
        is therefore the potential for differences to appear over the interpretation 
        of particular aspects of patent law. The EPC is entirely separate from 
        the Community and the EPO is not subject to Community law. The EPC currently 
        includes among its membership all Community Member States plus five other 
        countries (Switzerland, Turkey, Cyprus, Monaco, and Liechtenstein). Several 
        more are expected to join later in 2002.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The patentability 
        of software and related inventions is currently determined principally 
        by Article 52 paragraphs (2)(c) and (3) of the EPC, according to which 
        computer programs "as such" (as well as business methods and certain other 
        entities) are excluded from patentability. However, since the EPC came 
        into force in 1978, more than 30,000 software-related patents have been 
        granted, and a considerable body of case law on the subject has been built 
        up by the appellate bodies of the European Patent Office (EPO) and the 
        Member States� courts. Many of these patents have been granted for devices 
        and processes in technical areas, but the majority now relate to digital 
        data processing, data recognition, representation and information handling. 
        This has fuelled debate on whether the limits of what is patentable are 
        still sufficiently clear and applied properly.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">This is moreover 
        an area in which some differences have appeared in practice under national 
        patent laws. This has led to uncertainty over the legal position especially 
        in Member States where less extensive litigation has taken place.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">This is therefore 
        an opportune time to propose EU legislation. In its absence, Member States 
        may feel obliged to act outside the Community framework. In addition, 
        if no action is taken at the Community level, the scope of what is patentable 
        may be defined on the basis of individual decisions on particular cases 
        by the European Patent Office�s (EPO) judicial bodies, without the opportunity 
        for coherent political reflection based on wide consultation and the overall 
        picture. The proposed Directive therefore sets clear borders to what would 
        be patentable in the EU and what would not.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>Does this proposal 
        follow the principles established by the European Patent Convention? </b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">Broadly, yes. The 
        European Patent Convention says that patents should not be granted for 
        computer programs as such. However computers are themselves machines like 
        other technological devices. Over the years, national courts have decided 
        that there is no reason why a patent should not be granted for a machine 
        programmed to carry out some technical function, or a technical process 
        carried out using a computer or similar machine. But in common with all 
        other inventions, it still needs to be new and not obvious. The proposed 
        Directive has followed this approach.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>Won�t extending 
        patents to cover computer programs reduce competition?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The Directive will 
        not make it possible to patent computer programs "as such". In broad terms, 
        nothing will be made patentable which is not already patentable. The objective 
        is simply to clarify the law and to resolve some inconsistencies in approach 
        in national laws.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>So why do we need 
        a Directive?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The present legal 
        situation has essentially been built up by national courts and the EPO. 
        They have had to develop interpretations to cope with a technology whose 
        subsequent development could not have been imagined when the European 
        Patent Convention was first drafted at the beginning of the 1970s. They 
        have done a very good job, but the lack of harmonisation and the existence 
        of different legal traditions has meant that some differences have arisen 
        with the potential for more serious divergences in future if action at 
        the Community level were not taken. Consultations undertaken by the Commission 
        on this issue have indicated very clearly that the lack of legal certainty 
        in this field is widely regarded as very damaging to European interests.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>What consultations 
        has the Commission undertaken on this issue?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">There has been a 
        series of consultations on this subject beginning with the 1997 Green 
        Paper on the Community Patent. Most recently, a discussion document was 
        published on the Europa website on 19 October 2000 (see <a href="/comm/internal_market/en/indprop/comp/softpaten.htm">http://europa.eu.int/comm/internal_market/en/indprop/comp/softpaten.htm</a>, 
        which invited comments by 15 December 2000 on the basis of a number of 
        proposed "Key Elements" for a harmonised approach to the patentability 
        of computer-implemented inventions in the European Community. These "Key 
        Elements" broadly reflected the present state of development of European 
        case law under which it is estimated that at least 30,000 patents for 
        computer-implemented inventions have already been issued.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The consultation 
        produced 1447 responses, the overwhelming majority by e-mail. An analysis 
        of these responses is available at:</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><a href="/comm/internal_market/en/indprop/comp/softpatanalyse.htm">http://europa.eu.int/comm/internal_market/en/indprop/comp/softpatanalyse.htm</a></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">Many of the responses 
        supporting a more restrictive approach than at present, with fewer patents 
        being granted, were transmitted through an open forum set up by the "Eurolinux 
        Alliance", a group of companies and other entities supporting the development 
        of open source software such as Linux. Although this group numerically 
        dominated (90%) the response, the major sectoral bodies representing the 
        information and communication technology industries, as well as many of 
        the Member States, all supported the approach put forward by the discussion 
        paper. Some responses argued for eligibility for patents to be widened 
        in line with the practice in the US.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>How does the proposal 
        reflect responses to consultation?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The proposal takes 
        account of all responses, which covered a huge spectrum of opinion and 
        interests. The objective is to achieve the right balance between making 
        patents available where appropriate in order to reward and encourage innovation, 
        while avoiding stifling competition and open source development. The Commission 
        received some submissions arguing that patents tend to restrict innovation 
        in fields like software development. We also received submissions from 
        organisations representing many thousands of companies arguing that computer-implemented 
        inventions should remain patentable or even that patentability should 
        be extended. The Commission�s proposal reflects the balanced interests 
        of the EU�s economy and society as a whole.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>Why are patents 
        good for innovation?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">From the perspective 
        of the owner of a patent, the limited exclusivity provides an opportunity 
        to generate income. This in turn provides an incentive to invest in research 
        and development. And a successful business will use income from patented 
        inventions to fund further innovation. Patents can also be used as security 
        to obtain loans and as a means of negotiating for licenses on technology 
        owned by others.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">It is a condition 
        of obtaining a patent that there should be disclosure of how the invention 
        works. If the disclosure is not sufficient to enable the invention to 
        be reproduced, the patent can be revoked. Patents are therefore an important 
        source of technical information for others to use.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">EU industry has built 
        itself up in the very legal environment which we are now seeking to harmonise. 
        Other countries which are successful in the information technology sector 
        (such as the US and Japan) also grant patents for computer-implemented 
        inventions.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>Isn�t software 
        different to other technologies in that patents can be used to block legitimate 
        independent innovation?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The Commission has 
        seen little evidence that this has been a problem in practice in the present 
        environment. This would be the case only if the scope of protection granted 
        by patents were extended to software as such and, for instance, blocked 
        the use of an algorithmic idea in other technical fields from the one 
        in which a patent is granted. Since the proposed Directive would not extend 
        the scope of what can be patented, nor the scope of the protection granted 
        by a patent, there should be nothing to fear on this front.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>Would the proposed 
        Directive restrict the interoperability of computer programs?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">No. In fact, Member 
        States� patent laws, while not fully harmonised, do not in general extend 
        to acts performed privately and for non-commercial purposes, or to acts 
        carried out for experimental purposes related to the subject-matter of 
        the invention. Nor is it likely that the making of a back-up copy in the 
        context of the authorised exploitation of a patent covering a programmed 
        computer or the execution of a program could be construed as an infringement. 
        The proposed Directive will not change this situation. Thus, because of 
        the differences between the subject-matter of protection under patent 
        and copyright law, and the nature of the permitted exceptions, the exercise 
        of a patent covering a computer-implemented invention should not interfere 
        with the freedoms granted under copyright law to software developers by 
        the provisions of the existing Directive on the legal protection of computer 
        programs (91/250/EEC).</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">Indeed, the proposed 
        Directive makes specific reference, inter alia, to the provisions on decompilation 
        and interoperability in the Directive 91/250/EEC, in terms that ensure 
        that the different scope of protection granted by patents would not undermine 
        the possibility to carry out the acts that are permitted under that existing 
        Directive. Directive 91/250/EEC includes specific provisions (Articles 
        5 and 6) to the effect that copyright in a computer program is not infringed 
        by acts which under certain circumstances would otherwise constitute infringement. 
        These exceptions include acts performed for the purposes of studying the 
        ideas and principles underlying a program and the reproduction or translation 
        of code if necessary for the achievement of the interoperability of an 
        independently-created computer program. It is also specified that the 
        making of a back-up copy by a lawful user cannot be prevented. Such provisions 
        are necessary in the context of copyright law because copyright confers 
        the absolute right to prevent the making of copies of a protected work. 
        All the acts mentioned involve making copies and would therefore infringe 
        in the absence of any exception.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>Does the proposal 
        include rules about business methods?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The proposal does 
        not deal directly with the patentability of business methods. This is 
        because the consultations have indicated clearly that there is general 
        satisfaction with the current situation, whereby "pure" business methods 
        (that is, methods which have no technical character) are not currently 
        patentable.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">However, in practice 
        some inventions involving business methods could fall within the definition 
        of "computer-implemented inventions". These inventions would be dealt 
        with in accordance with the proposed Directive, and in particular patents 
        would only be granted for inventions that made a "technical contribution".</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>What is meant 
        by "technical contribution"?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The proposed Directive 
        defines a "technical contribution" as "a contribution to the state of 
        the art in a technical field which is not obvious to a person skilled 
        in the art". If an invention implemented through the execution of software 
        did not make a technical contribution it would be considered to lack an 
        inventive step and thus would not be patentable.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">It would not be possible 
        for a legal text such as a Directive to attempt to spell out in fine detail 
        what is meant by "technical", because the very nature of the patent system 
        is to protect what is novel, and therefore not previously known. In practice 
        the courts will determine in individual cases what is or is not encompassed 
        within the definition. However, earlier court decisions have indicated 
        that a technical contribution may arise if there has been some improvement 
        in the way that processes are carried out or resources used in a computer 
        (for example an increase in the efficiency of a physical process), or 
        if the exercise of technical skills beyond "mere" programming has been 
        necessary to arrive at the invention.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>aWhat are the main 
        differences between the approach of the proposed Directive and the situations 
        in the US and Japan?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The main difference 
        lies in the requirement for "technical contribution". Japanese law does 
        not have this as such, but there is in Japan a doctrine which has traditionally 
        been interpreted in a similar way: the invention has to be a "highly advanced 
        creation of technical ideas by which a law of nature is utilised".</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">In the US on the 
        other hand, a patentable invention must simply be within the technological 
        arts. No specific technological contribution is needed. The mere fact 
        that an invention uses a computer or software makes it become part of 
        the technological arts if it also provides a "useful, concrete and tangible 
        result". Among other things, this has meant that in practice in the US, 
        restrictions on patenting of business methods (apart from the requirements 
        of novelty and inventive step) are negligible.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">Practice in both 
        the U.S. and Japan is to allow patent claims to software that implements 
        patentable inventions.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The need to maintain 
        and improve standards of patent examination for computer-implemented inventions 
        is one of the most important challenges facing patent offices world-wide. 
        The EPO, which is widely recognised as a world leader in patent search 
        and examination, is, together with its colleagues in the US and Japanese 
        patent offices, studying this question as one of its highest priorities.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>Why would claims 
        to computer programs on their own not be permitted under the proposed 
        Directive?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">In recent decisions, 
        patents have under certain conditions been allowed which contain claims 
        for computer programs on their own, for example on a disk or even as a 
        signal transmitted over the internet. In the course of consultations, 
        fears were expressed that if enforced, patents including such claims may 
        be used to prevent "reverse engineering" and other activities considered 
        legitimate in respect of computer programs already protected under copyright 
        law. Moreover, such claims could be said to be contrary to the EPC, which 
        does not allow patents for computer programs "as such". In response to 
        these concerns, the Commission has decided not to follow the direction 
        taken by case law in this important respect. Accordingly, the proposal 
        would not allow claims of this type to be considered valid.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>What is the difference 
        between a patent and copyright? Can an invention be protected by both?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">Patent and copyright 
        protection are complementary. In very general terms, patents protect new 
        technical ideas and principles, while copyright protects the form of expression 
        used. For example, a new sort of paper might be protected by a patent, 
        while the printed content of a newspaper would be protected by copyright. 
        In computer terms, the actual code (whether machine-readable or in a form 
        which is intelligible to human readers) would be subject to copyright 
        protection, while underlying technological ideas may be eligible for patent 
        protection.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">Patent law gives 
        the holder of a patent for a computer-implemented invention the right 
        to prevent third parties from using software incorporating any new technology 
        he has invented (as defined by the patent claims).</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">On the other hand, 
        a computer program will be accorded copyright protection where the form 
        of expression is original in the sense of being the author�s own intellectual 
        creation. Third parties would not be able to produce substantially the 
        same content material as the original author has produced, even if they 
        used different technical principles to do so.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">Accordingly, the 
        same program may be protected by both patent and by copyright law. That 
        protection may be cumulative only in the sense that an act involving exploitation 
        of a particular program may infringe both the copyright in the code and 
        a patent whose claims cover the underlying ideas and principles of the 
        invention using the program.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>What about the 
        BT "hyper-text" patent case - can this patent really be valid when hyperlinks 
        are common-place?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">This is a very old 
        patent. It has expired everywhere else in the world, but is still in force 
        in the US by virtue of the old US patent law which counted the patent 
        term from the date of grant (10 October 1989) rather than from the date 
        of application. This happens from time to time and is nothing to do with 
        being in the field of computer programs. It of course remains to be seen 
        whether the US courts will find in favour of the patentees.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">A related patent 
        was granted in the UK and remained in force until it expired in 1997. 
        Patents were also granted in many other countries including Japan and 
        the main European countries. The test for whether a patent is allowable 
        is whether it was inventive at the time of application. Although hyperlinks 
        are commonplace today, it is clear that several different patent offices 
        all took the view that the patent described a valid invention at the time 
        of the application.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>Would the Amazon 
        "one-click" shopping cart ordering model be patentable under the Commission 
        proposal?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">The European Patent 
        Office has yet to come to a decision on the related European application, 
        so it would not be appropriate to comment on whether there is any patentable 
        subject-matter in the application as a whole. However, a patent with the 
        breadth of claims which has been granted in the United States would be 
        highly unlikely to be considered to make a "technical contribution" in 
        the EU under the terms of the proposed Directive.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>When would the 
        proposal become law?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">Once the proposal 
        has been adopted by the EU�s Council of Ministers and the European Parliament 
        under the so-called co-decision procedure, the Directive would have to 
        be implemented in national law by the Member States.</font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1"><b>Will there be 
        further consultations?</b></font></P>
      <P><font face="Arial, Helvetica, sans-serif" size="-1">There have already 
        been extensive consultations since 1997 and no more are planned now that 
        the proposal, reflecting the results of these consultations, has been 
        presented. However, there is plenty of opportunity for further input and 
        comment on the proposal during the process of its adoption by the EU�s 
        Council of Ministers and the European Parliament.<br>
        </font></P>
      <P>
      <HR ALIGN=LEFT>
      <font face="Arial, Helvetica, sans-serif" size="-2"><B><FONT COLOR="#220d6d">Date</FONT></B>:</font><font color="#1d005c" size="-2" face="Arial, Helvetica, sans-serif"> 
      20 February 2002</font><font color="#003800" size="-2" face="Arial, Helvetica, sans-serif"><BR>
      For further details:</font><font size="-2" face="Arial, Helvetica, sans-serif"> 
      <A HREF="mailto:MARKT-E2@cec.eu.int">MARKT-E2@cec.eu.int</A><BR>
      </font> 
      <HR ALIGN=LEFT>
      <P> <font face="Arial, Helvetica, sans-serif" size="-2"><A HREF="#top"><IMG SRC="/comm/images/navigation/ar_pag_t.gif" WIDTH="14" HEIGHT="16" ALIGN="BOTTOM" BORDER="0" ALT="Top"></A> 
        <BR>
        <IMG SRC="/comm/images/navigation/ligne.gif" WIDTH="267" HEIGHT="8" ALIGN="BOTTOM" BORDER="0"> 
        <BR>
        <!-- STEP 8: INSERT HORIZONTAL NAVIGATION --> [&nbsp;<a href="/comm/internal_market/en/update/index.htm">Update 
        on the Single Market</a>&nbsp;] - [ <a href="/comm/internal_market/en/update/economicreform/index.htm">Economic 
        Reform</a> ] - [ <a href="/comm/internal_market/en/finances/index.htm">Financial 
        Services</a> ] - [ <a href="/comm/internal_market/en/company/">Financial 
        Reporting &amp; Company Law</a> ] - [ <a href="/comm/internal_market/en/ecommerce/index.htm">E-Commerce</a> 
        ] - [&nbsp;<a href="/comm/internal_market/en/media/index.htm">Media</a> 
        ] - [&nbsp;<a href="/comm/internal_market/comcom/">Commercial Communications 
        &amp; Unfair Competition</a> ] - [ <a href="/comm/internal_market/en/dataprot/index.htm">Data 
        Protection</a> ] - [ <a href="/comm/internal_market/en/intprop/index.htm">Intellectual 
        Property</a> ] - [ <a href="/comm/internal_market/en/indprop/index.htm">Industrial 
        Property</a> ] - [ <a href="/comm/internal_market/en/goods/index.htm">Free 
        Movement of Goods</a> ] - [&nbsp;<a
href="/comm/internal_market/en/services/index.htm">Services &amp; Establishment</a>&nbsp;] 
        - [ <a href="/comm/internal_market/en/qualifications/index.htm">Professional 
        Qualifications</a> ] - [ <a href="/comm/internal_market/en/postal/index.htm">Postal 
        Services</a> ] - [&nbsp;<a href="/comm/internal_market/en/publproc/index.htm">Public 
        Procurement</a>&nbsp;] <!-- END OF HORIZONTAL NAVIGATION SECTION --> </font></P> 
    </TD>
<TD WIDTH="10" ROWSPAN="2" VALIGN="BOTTOM"><IMG SRC="/comm/images/navigation/15x10.gif" WIDTH="10" HEIGHT="10" ALIGN="BOTTOM" BORDER="0"></TD>
</TR>
<TR>
<TD VALIGN="BOTTOM" WIDTH="77">

<!-- STEP 9: CUSTOMIZE SERVICE TOOLS SECTION -->

<A HREF="/comm/internal_market/en/whatsnew.htm"><IMG SRC="/comm/images/tools/too02_en.gif" ALT="What's New ?" WIDTH="77" HEIGHT="32" BORDER="0"></A><BR>
<IMG SRC="/comm/images/tools/too00_00.gif" WIDTH="77" HEIGHT="16"><BR>
<A HREF="mailto:markt-info@cec.eu.int"><IMG SRC="/comm/images/tools/too03_en.gif" ALT="Mail-Box" WIDTH="77" HEIGHT="32" BORDER="0"></A><BR>
<IMG SRC="/comm/images/tools/too00_00.gif" WIDTH="77" HEIGHT="16"><BR>
<A HREF="/comm/internal_market/en/sitemap.htm"><IMG SRC="/comm/images/tools/too04_en.gif" ALT="Site Map" WIDTH="77" HEIGHT="32" BORDER="0"></A><BR>
<IMG SRC="/comm/images/tools/too00_00.gif" WIDTH="77" HEIGHT="16"><BR>
<A HREF="/comm/internal_market/en/overview.htm"><IMG SRC="/comm/images/tools/too05_en.gif" ALT="Index" WIDTH="77" HEIGHT="32" BORDER="0"></A><BR>
<IMG SRC="/comm/images/tools/too00_00.gif" WIDTH="77" HEIGHT="16"><BR>
<A HREF="/geninfo/query_en.htm"><IMG SRC="/comm/images/tools/too06_en.gif" ALT="Search" WIDTH="77" HEIGHT="32" BORDER="0"></A><BR>
<IMG SRC="/comm/images/tools/too00_00.gif" WIDTH="77" HEIGHT="16"><BR>
<A HREF="/geninfo/info-en.htm"><IMG SRC="/comm/images/tools/too07_en.gif" ALT="Information" WIDTH="77" HEIGHT="32" BORDER="0"></A><BR>
<IMG SRC="/comm/images/tools/too00_00.gif" WIDTH="77" HEIGHT="16"><BR>
<A HREF="/comm/internal_market/en/faq.htm"><IMG SRC="/comm/images/tools/too09_en.gif" ALT="FAQ" WIDTH="77" HEIGHT="32" BORDER="0"></A><BR>
<IMG SRC="/comm/images/tools/too00_00.gif" WIDTH="77" HEIGHT="16"><BR>
<A HREF="/comm/internal_market/en/foren.htm"><IMG SRC="/comm/images/tools/too10_en.gif" ALT="Forum" WIDTH="77" HEIGHT="32" BORDER="0"></A><BR>
<IMG SRC="/comm/images/tools/too00_00.gif" WIDTH="77" HEIGHT="16"><BR>
<A HREF="/comm/internal_market/en/linkswww.htm"><IMG SRC="/comm/images/tools/too11_en.gif" ALT="Links" WIDTH="77" HEIGHT="32" BORDER="0"></A><BR>

<!-- END OF SERVICE TOOLS SECTION -->

<!-- STEP 10: CHECK YOUR WORK IN THE BROWSER -->

</TD>
</TR>
</TABLE>
</BODY>
</HTML>






