<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="de">
<head>
  <title>Softwarepatente - Erich Schubert's Homepage</title>
  <link rel="stylesheet" type="text/css" href="/style.css">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
  <meta name="robots" content="index,follow">
  <meta name="revisit-after" content="180 days">
  <meta name="autor" content="Erich Schubert erich@vitavonni.de">
  <meta name="modified" content="$Id: softwarepatente.wml,v 1.3 2002/07/22 18:59:28 erich Exp $">
  <meta name="keywords" content="" lang="de">
  <meta name="description" content="" lang="de">
</head>
<body margintop="0" marginheight="0" leftmargin="0" marginwidth="0" bgcolor="white" text="black" link="#000099" vlink="#0000AA" alink="#000044" background="/_gfx/bggesicht.jpg">

<table cellspacing="0" cellpadding="0" border="0" width="100%" summary="">
<tr><td><a href="/"><img src="/_gfx/leer.gif" width="150" height="100" border="0" alt=""></a></td>
<td align="center" valign="middle">
<a href="/"><img src="/_gfx/vitavonni.gif" alt="Vitavonni.de" border="0" vspace="5"></a></td>
<td align="center" valign="middle" class="vitavonni">Erich Schubert's Homepage</td></tr>
<tr><td></td>
<td colspan="2" align="right"><small class="standort">Standort: <a href="/">vitavonni.de</a> &gt; <a href="./" class="standort">Artikel</a> &gt; <a href="softwarepatente.html" class="standort">Softwarepatente</a></small>&nbsp;</td></tr>
<tr><td></td><td rowspan="3" colspan="2" valign="top">
<h2>Software-Patente</h2>
<h3>Kommerzialisierung der Informatik-Forschung</h3>
<h4>Patent-Theorie</h4>
<p>Das Patentwesen entstand, um Erfindungen vor Nachahmungen zu sch�tzen und so dem urspr�nglichen Erfinder eine angemessene Bezahlung zu sichern. Um die Forschung voranzutreiben zwingt das Patentwesen aber gleichzeitig den Erfinder, seine Erfindung offenzulegen (um den Schutz zu erhalten) - dadurch wird seine Erfindung f�r andere Erfinder zug�nglich und erweiterbar.</p>
<h4>Patent-Realit�t</h4>
<p>Leider erf�llt das Patentwesen diese Erwartungen nur sehr eingeschr�nkt, es ist viel mehr ein juristisches Werkzeug (Spielzeug?) geworden um andere Firmen zu blockieren und Monopole zu sichern, und solange die Patentierung einer Erfindung l�uft (Dauer derzeit 2 Jahre [f�r das Eintragen]) ist die Erfindung nicht zug�nglich und daher nicht f�r die weitere Forschung verf�gbar, was die Forschung stark bremst.</p>
<p>Arbeitet man mit einem sehr stark vereinfachten Modell, so erf�llt der Patentschutz durchaus seine Erwartungen, erweitert man das Modell aber, so stellt man fest das der Patentschutz die Forschung hemmt - was mitunter sogar zu niedrigeren Einnahmen f�r den Patentinhaber f�hrt. (siehe Verweise)</p>
<h4>Patente in der High-Tech-Industrie</h4>
<p>Die Industrie selbst hat bereits erkannt, dass das Patentsystem nicht optimal ist:</p>
<p>Die meisten Unternehmen geben bei Umfragen an, ihre Patente nur �defensiv� zu nutzen, dass heisst um sich damit vor Patentklagen anderer Firmen zu sch�tzen. Des weiteren ist ein sogenanntes �Cross-Licensing� �blich. Hierbei werden gegen eine geringe Lizenzgeb�hr s�mtliche Patente des Unternehmens, meistens inklusive zuk�nftiger Patente, lizensiert; dieses erfolgt gegenseitig - zwischen den Vertragspartnern ist der Patenschutz also praktisch aufgehoben.</p>
<p>In der Halbleiterindustrie wurde auch bisher kein Markteintritt einer neuen Firma durch Patente verhindert. [Ham Hall 1999]</p>
<h4>Vorteile einer �patentfreien� Situation</h4>
<p>Welche Vorteile ergeben sich also aus einer Aufhebung des Patentschutzes (gegebenenfalls zwischen Vertragspartnern)?</p>
<ul>
<li>Gemeinsame Verbesserung und Weiterentwicklung: Entwickelt das �nachahmende� Unternehmen die Erfindung weiter und verbessert sie, so profitiert davon automatisch auch das Unternehmen das die Erfindung urspr�nglich gemacht hat; durch das bereits vorhandene Know-How ist es f�r das erste Unternehmen viel einfacher die Verbesserungen zu �bernehmen, als f�r das zweite Unternehmen, die urspr�ngliche Erfindung zu kopieren</li>
<li>Bessere Verbreitung: Dadurch, dass die Erfindung von mehreren Unternehmen eingesetzt wird, wird sie zum Standard, die Chancen dass sich die Entwicklung gegen andere Standards durchsetzen kann ist h�her, das Risiko einen �Flop� zu entwickeln und in diesen zu investieren ist dadurch geringer</li>
<li>Bessere Qualit�t: dadurch dass die Erfindungen von allen Unternehmen eingesetzt werden k�nnen, haben die Produkte aller Unternehmen eine h�here Qualit�t</li>
<li>Bessere Forschungs- und Entwicklungsm�glichkeiten: Da auf eine gro�e Menge von Entwicklungen zur�ckgegriffen werden kann, auf die neue Entwicklungen aufsetzen k�nnen, besteht kein Interesse daran, die Erfingungen anderer Unternehmen zu umgehen</li>
</ul>
<h4>Warum also IT-Patente?</h4>
<p>Warum fordern also viele Informatiker den bereits bestehenden Schutz durch das Urheberrecht um einen Schutz durch Patente zu erweitern? Will die Informatik damit �professioneller� wirken? Wollen hier einzelne Personen maximalen Profit aus ihrer Arbeit �herausschlagen�?</p>
<p>Die einzigen die von IT-Patenten wirklich profitieren werden, sind die Patentanw�lte.</p>
<p>Nat�rlich haben junge Softwarefirmen die oft auf einzelnen Software-Entwicklungen basieren Angst ohne Patentschutz keine Unternehmensgrundlage zu haben. Diese d�rfen aber auch nicht vergessen, dass, solange sie nicht die Quelltexte ver�ffentlichen, es f�r die Konkurrenz �hnlich teuer sein d�rfte, die Algorithmen �nachzuahmen�, wie es f�r das urspr�ngliche Unternehmen war, die Algorithmen zu erfinden. Desweiteren haben die derzeitigen �Marktf�hrer� wie Microsoft, Oracle, SAP etc. ihre Position ohne Patente erreicht.</p>
<p>Sie d�rfen aber vor allem nicht vergessen, dass auch die anderen Unternehmen dann �ber Softwarepatente verf�gen werden, auf die sie ihrerseits angewiesen sind - und die sie vielleicht nur �ber �Cross-Licensing� lizensieren k�nnen. Und was f�r Wettbewerbsverzerrungen, unter denen typischerweise die kleinen Unternehmen leiden, auftreten, zeigt das �Vorbild� USA: hier sorgen �Trivialpatente� wie das viel zitierte �1-Click�-Verfahren von amazon.com f�r Probleme.</p>
<h4>Nachteile von Patenten</h4>
<ul>
<li>Verz�gerung bei der Forschung: Bis ein Patent eingetragen ist (derzeit 2 Jahre) werden die zu Grunde legenden Ideen nicht ver�ffentlicht - sind also f�r weitere Forschungen nicht verf�gbar</li>
<li>Veraltete Patente: In der Informatik k�nnen Patente wesentlich schneller �berholt und veraltet sein; hier sind 2 Jahre [bis das Patent eingetragen ist] eine lange Zeitspanne</li>
<li>Kosten: Die Kosten f�r Patentrecherche, -anw�lte, -lizenzen, -anmeldungen schlagen zus�tzlich zu Buche - und werden an den Endverbraucher weitergegeben</li>
<li>Schlechtere Software: Statt die lizenzpflichtigen und daher teureren L�sungen zu w�hlen, werden minderwertige L�sungen verwendet die keine oder geringerere Kosten erzeugen</li>
<li>Monopole: Patente erm�glichen Monopole, insbesondere f�r gros�e Firmen in der Branche, die nicht von anderen Unternehmen gezwungen werden k�nnen, akzeptable Lizenzen zu vergeben</li>
</ul>
<h4>Weiterf�hrende Informationen</h4>
<p>Weiterf�hrende Informationen gibt es gesammelt unter
<a href="http://swpat.ffii.org/">http://swpat.ffii.org/</a>.</p>
<p>Empfehlenswert ist die von zwei MIT-Professoren durchgef�hrte Wirkungsstudie �<a href="http://www.researchoninnovation.org/patent.pdf">Sequentielle Innovation, Patente und Nachahmung</a>� (<a href="http://swpat.ffii.org/vreji/prina/indexde.html">Deutsche �bersetzung und weiterf�hrende Informationen</a>)</p>
<p>Und unter <a href="http://swpat.ffii.org/vreji/minra/siskude.html">
http://swpat.ffii.org/vreji/minra/siskude.html</a>
finden sich zahlreiche Wirkungsstudien �ber das Patentsystem.</p>
<author><a href="mailto:presse@vitavonni.de">Erich Schubert</a></author>
</td></tr>
<tr><td valign="top"><img src="/_gfx/leer.gif" width="10" height="250" alt=""><br>
<table border="0" cellpadding="4" cellspacing="0" class="menu" summary="">
<tr><td><a href="/index.html" class="navbar">Startseite</a></td></tr><tr><td><a href="/facharbeit/" class="navbar">Facharbeit</a></td></tr><tr><td><a href="/gedichte/" class="navbar">Lyrik</a></td></tr><tr><td><a href="/presse/" class="navbar">Artikel</a><table border="0" cellpadding="4" cellspacing="0" class="menu" summary="">
<tr><td><span class="navbar_selected">Softwarepatente</span></td></tr><tr><td><a href="/presse/pisa.html" class="navbar">PISA-Studie</a></td></tr></table>
</td></tr><tr><td><a href="/projekte.html" class="navbar">Projekte</a></td></tr><tr><td><a href="/spiel.phtml" class="navbar">Spiel</a></td></tr><tr><td><a href="/links.html" class="navbar">Links</a></td></tr><tr><td><a href="http://gaestebuch.drinsama.de/erich/gedichte" class="navbar">G�stebuch</a></td></tr><tr><td><a href="/email.html" class="navbar">E-Mail</a></td></tr></table>
</td></tr>
<tr><td><img src="http://counter.drinsama.de/?erich1" width="160" height="36" alt="Counter"><br>
<span class="counter">seit dem 14. Juli 1999<br>[ <a href="softwarepatente.print.html">Druckbare Version</a> ]</span></td></tr></table>
<div align="right" class="copyright">&copy; Copyright 2002 by <a href="mailto:homepage@vitavonni.de">Erich Schubert</a>, Alle Rechte vorbehalten. Letzte �nderung: 22.07.2002 18:59, Version 1.3</div>
</body></html>
