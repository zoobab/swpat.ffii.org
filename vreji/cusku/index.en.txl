<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

##? INSERTION phm 2003-11-12: please translate this
#title: Quotations on Software Patents

##? MODIFICATION phm 2003-11-12: Pay attention to the wording
#descr: Salient quotations from law texts, economic analyses, political
documents as well as statements by programmers, politicians and other
parties interested in the debate about software patents.

#aii: The party's IT policy which includes a paragraph opposing swpat, was
ratified by party conference on Sunday 2003/03/16. During the debate
several speakers either spoke specifically against software patents or
mentioned the issue in passing, as did the proposer of the ratifying
motion (who was the chair of the policy working group).  The paper was
published together with a motion that calls for %(q:supporting
continued widespread innovation by resisting the wider application of
patents in this area.)

#ExW: Europarechtler Lenz exzerpiert und kommentiert Texte aus aktuellen
Studien und Anhörungen, die für die Gegner von Softwarepatenten von
Interesse sind

#Con: Chapter 6 argues that developping countries should be careful not to
follow the US or the European Commission in framing their patent
policy with regard to software, genetics et al.  They should adopt an
approach similar to Art 52 EPC: explicitely exclude software, business
methods and the like from patentability.  The authors are british
scholars, their work was commissioned by the british government.

#Pos: Phil Karn, an software developper working for Qualcomm, a company that
lives largely on patent licenses, finds that the patent system is,
apart from bringing some revenues to companies like Qualcomm, doing
little good for the software industry as a whole.

#AiW: A collection of statements from numerous European politicians, IT
company managers and intellectuals in support of the Eurolinux
position of not allowing software patents.

#SCS: The founder of Lotus explains at a hearing at the USPTO in 1994 why
patents are bad for the software industry.

#Dyh: Douglas Brotz, Principal Scientist at Adobe Systems, explains at the
US Patent Office hearings in 1994 why patents are bad for the software
industry.

#Ich: Autodesk is a world-leader, some say a monopolist, in CAD software. 
Warren became known as a software and business pioneer, founding
editor of the legendary %(q:Dr. Dobb's Journal) and board member of
Autodesk.  In a insightful and passionate testimony at the USPTO
hearing in 1994, Warren explains first why algorithms are different
from material phenomena and why the attempt to monopolise them breaks
fundamental constitutional values.

#Ria: Robert Kohn, chief developper and board member of Borland Corporation
explains at the USPTO hearing in 1994 that what his company really
needs is not broader IP rights but copyright enforcement.

#SWl: Stephan Kinsella on IP

#AWi2: A patent lawyer criticises the patent system and the pretentious
attitudes of patent lawyers

#Ihe: In his address before a packed house at the Open Source Convention,
Lawrence Lessig challenges the audience to get more involved in the
political process.  In his speech he quotes some interesting comments
by Bill Gates and other people on software patents.

#BWn: Microsoft-Broschüre %(q:Überzeugen Sie durch Argumente: Windows vs
Linux)

#ohW: Microsoft schreibt auf Seite 5: %(bc:Ein Problem von Open Source
stellen so genannte Software-Patente dar. Ein prominentes Beispiel
sind die Rechte an dem Dateiformat JPEG. Die entwickelnde Firma hatte
einst die Lizenzen frei vergeben, um eine weite Verbreitung des
Formats zu erreichen.  Der Käufer dieser Firma fordert nun Gebühren
von kommerziellen Anwendern. Dies stellt, wenn man so will, eine
Zeitbombe dar.)

#LWw: LPF Irlam: Quotes on Software Patents

#Cih: Excerpts from studies et al about the effects of the patent system on
software and the economy in general.  Shows that the system has been
badly broken in many areas for a long time.  Collected by Gordon Irlam
and published by the League for Programming Freedom (LPF).

#CWn: Irlam 1994: Quotes on Software Patents

#VrF: A variant of the LPF collection, published by Gordon Irlam in a
mailing list thread.

#Lne: Lang: Citations sur les Brevets Logiciels

#Coe: Quotations in English and French about %(q:Intellectual Property) and
the Informational Commons, collected by Bernard Lang.

#Sei: Software & Technology Practitioners

#Rej: Law Scholars, Judges, Patent Practitioners

#Wfk: Economists

#Msr: Mathematicians, Philosophers, Generalists

#Pik: Politicians

#Ptt: Patent strategists

#Mte: My Introduction To Patent Realities

#AWf: Gary Reback, a famous american software lawyer, narrates from his
memories

#Mne: My own introduction to the realities of the patent system came in the
1980s, when my client, Sun Microsystems--then a small company--was
accused by IBM of patent infringement. Threatening a massive lawsuit,
IBM demanded a meeting to present its claims. Fourteen IBM lawyers and
their assistants, all clad in the requisite dark blue suits, crowded
into the largest conference room Sun had.

#Tri: The chief blue suit orchestrated the presentation of the seven patents
IBM claimed were infringed, the most prominent of which was IBM's
notorious %(q:fat lines) patent: To turn a thin line on a computer
screen into a broad line, you go up and down an equal distance from
the ends of the thin line and then connect the four points. You
probably learned this technique for turning a line into a rectangle in
seventh-grade geometry, and, doubtless, you believe it was devised by
Euclid or some such 3,000-year-old thinker. Not according to the
examiners of the USPTO, who awarded IBM a patent on the process.

#AsW: After IBM's presentation, our turn came. As the Big Blue crew looked
on (without a flicker of emotion), my colleagues--all of whom had both
engineering and law degrees--took to the whiteboard with markers,
methodically illustrating, dissecting, and demolishing IBM's claims.
We used phrases like: %(q:You must be kidding,) and %(q:You ought to
be ashamed.) But the IBM team showed no emotion, save outright
indifference. Confidently, we proclaimed our conclusion: Only one of
the seven IBM patents would be deemed valid by a court, and no
rational court would find that Sun's technology infringed even that
one.

#AnO: An awkward silence ensued. The blue suits did not even confer among
themselves. They just sat there, stonelike. Finally, the chief suit
responded. %(q:OK,) he said, %(q:maybe you don't infringe these seven
patents. But we have 10,000 U.S. patents. Do you really want us to go
back to Armonk [IBM headquarters in New York] and find seven patents
you do infringe? Or do you want to make this easy and just pay us $20
million?) After a modest bit of negotiation, Sun cut IBM a check, and
the blue suits went to the next company on their hit list.

#IWt: In corporate America, this type of shakedown is repeated weekly. The
patent as stimulant to invention has long since given way to the
patent as blunt instrument for establishing an innovation
stranglehold.

#HSi: Quick Introduction

#AWa: To help readers grasp why the European Commission's softpat directive
proposal is so controversial, the analysis starts with a simple
scenario.

#ItW: Imagine you own a small software company.  You have written a powerful
piece of software.  This software is a creative combination of 1000
abstract rules (algorithms) and a lot of data.  The rules take a few
minutes or hours each to [re]invent, whereas developping and debugging
the whole work took you 20 man-years.  900 of the rules were already
known 20 years ago.  50 of the rules are now covered by patents.  You
own 3 of these patents.  In order to obtain these patents, you had to
rush to the patent office, disclose your business strategy and pay
lawyer fees.  IBM and Microsoft are meanwhile already turning your
patented ideas into profit.  You want them to stop?  Their lawyer
teams say you are infringing on 20-30 of their 50000 patents.  So you
reach a gentlemen's agreement:  3% of your annual sales revenues go to
IBM, 2% to Microsoft, 2% %(dots).  Nonetheless, one day you enter the
profit zone.  You are now an attractive company.  A patent agency
approaches you.  You are infringing on 2-3 of their patents, they say.
 Their claims are very broad.  They want 100,000 EUR.  Litigation
could take 10 years and cost 1 million EUR.  You pay.  A month later,
the next patent agent knocks on the door %(dots). Before long you are
broke.  You seek protection from a big company.  Microsoft offers to
buy you for a symbolic fee.  You accept.  Under a copyright-only
system, you would now be independent and rich.  But by means of
patents, Microsoft and others were able to steal your intellectual
property.

#SoW: Software is like other fields of engineering in many ways. But there
is a fundamental difference: computer programs are built out of ideal
mathematical objects. A program always does exactly what it says. You
can build a castle in the air supported by a line of zero thickness,
and it will stay up.

#Ptw: Physical machinery isn't so predictable, because physical objects are
quirky. If a program says to count the numbers from one to a thousand,
it will do exactly that. If you build the counter out of machinery, a
belt might slip and count the number 58 twice, or a truck might go by
outside and you'll skip 572. These problems make designing reliable
physical machinery very hard.

#Wst: When we programmers put a while statement inside an if statement, we
don't have to worry about whether the while statement will run such a
long time that it will burn out the if statement, or that it will rub
against the if statement and wear it out. We don't have to worry that
it will vibrate at the wrong speed and the if statement will resonate
and crack. We don't have to worry about physical replacement of the
broken if statement. We don't have to worry about whether the if
statement can deliver enough current to the while statement without a
voltage drop. There are many problems of hardware design that we don't
have to worry about.

#Tsa: The result is that software is far easier to design, per component,
than hardware. This is why designers today use software rather than
hardware wherever they can. This is also why teams of a few people
often develop computer programs of tremendous complexity.

#Pnt: People naively say to me, %(q:If your program is innovative, then
won't you get the patent?) This question assumes that one product goes
with one patent.

#Iwr: In some fields, such as pharmaceuticals, patents often work that way.
Software is at the opposite extreme: a typical patent covers many
dissimilar programs and even an innovative program is likely to
infringe many patents.

#Teo: That's because a substantial program must combine a large number of
different techniques, and implement many features. Even if a few are
new inventions, that still leaves plenty that are not. Each technique
or feature less than two decades old is likely to be patented already
by someone else. Whether it is actually patented is a matter of luck.

#InW2: I've explained how patents impede progress. Do they also encourage it?

#PtW: Patents may encourage a few people to look for new ideas to patent.
This isn't a big help because we had plenty of innovation without
patents. (Look at the journals and advertisements of 1980 and you'll
see.) New ideas are not the limiting factor for progress in our field.
The hard job in software is developing large systems.

#Pee: People developing systems have new ideas from time to time. Naturally
they use these ideas. Before patents, they published the ideas too,
for kudos. As long as we have a lot of software development, we will
have a steady flow of new published ideas.

#Tdr: The patent system impedes development. It makes us ask, for each
design decision, %(q:Will we get sued?) And the answer is a matter of
luck. This leads to more expensive development and less of it.

#WWt: With less development, programmers will have fewer ideas along the
way. Patents can actually reduce the number of patentable ideas that
are published.

#Adu: A decade ago, the field of software functioned without patents. 
Without patents, it produced innovations such as windows, virtual
reality, spreadsheets, and networks. And because of the absence of
patents, programmers could develop software using these innovations.

#Whc: We did not ask for the change that was imposed on us. There is no
doubt that software patents tie us in knots. If there's no clear and
vital public need to tie us up in bureaucracy, untie us and let us get
back to work!

#crr: %(PH), board director of %(ILOG)

#MoW: http://www.ilog.com/corporate/members/executive.cfm?Printout=Yes

#Lao: Software is closer to math (non-patentable) than to chemistry (often
cited as a success story of the patent system)

#L1e: The american experience of software patents is a disaster.  Before
imitating them we should rather try to see if they won't agree to
change their system.

#Pae: In order to do that, it will be necessary to lobby the big american
corporations.

#LWt: The european software companies prefer to live with the pressure of
having to improve constantly to the pressure of having to apply for
patents, attack other companies and live with a constant risk of
infringing on other companies' patents

#Lsc: Free software is an orthogonal problem.  One could imagine applying
for patents before publishing free software on the Net and thus
creating inextricable legal situations.

#L0j: The argument that the software startups are not able to access capital
without patents is a lie.  I have never encountered this kind of case.

#Mti: My observation is that patents have not been a positive force in
stimulating innovation at Cisco. Competition has been the motivator;
bringing new products to market in a timely manner is critical. 
Everything we have done to create new products would have been done
even if we could not obtain patents on the innovations and inventions
contained in these products. I know this because no one has ever asked
me %(q:can we patent this?) before deciding whether to invest time and
resources into product development.

#Tne: The time and money we spend on patent filings, prosecution, and
maintenance, litigation and licensing could be better spent on product
development and research leading to more innovation.  But we are
filing hundreds of patents each year for reasons unrelated to
promoting or protecting innovation.

#Mea: Moreover, stockpiling patents does not really solve the problem of
unintentional patent infringement through independent development.  If
we are accused of infringement by a patent holder who does not make
and sell products, or who sells in much smaller volume than we do, our
patents do not have sufficient value to the other party to deter a
lawsuit or reduce the amount of money demanded by the other company. 
Thus, rather than rewarding innovation, the patent system penalizes
innovative companies who successfully bring new products to the
marketplace and it subsidizes or  rewards those who fail to do so.

#alcatel02: Alcatel 2002: Frequent Patent Litigation Diverting R&D Efforts

#erW: In a report from 2002 the french telecom giant complains about legal
insecurity caused by patents:

#eWa: Like other companies operating in the telecommunications industry, we
experience frequent litigation regarding patent and other intellectual
property rights. Third parties have asserted, and in the future may
assert, claims against us alleging that we infringe their intellectual
property rights. Defending these claims may be expensive and divert
the efforts of our management and technical personnel. If we do not
succeed in defending these claims, we could be required to expend
significant resources to develop non-infringing technology or to
obtain licenses to the technology that is the subject of the
litigation. In addition, third parties may attempt to appropriate the
confidential information and proprietary technologies and processes
used in our business, which we may be unable to prevent.

#uao: Our business and results of operations will be harmed if we are unable
to acquire licenses for third party technologies on reasonable terms.

#pde: We remain dependent in part on third party license agreements which
enable us to use third party technology to develop or produce our
products. However, we cannot be certain that any such licenses will be
available to us on commercially reasonably terms, if at all.

#B0f: Bradford Friedman (Cadence) 2002:  general animosity to software
patents in the industry

#BoW: Bradford L. Friedman, Director of Intellectual Property, Cadence
Design Systems, Inc.

#Aod: As I'm sure this committee is aware, there is a general animosity to
pure software patents within and outside of the industry due to, one,
the perceived allowance of what I'll diplomatically call overbroad
patent claims, and two, the historically non-proprietary culture of
the software engineering industry.

#IWW3: In sum, largely because the current patent system is poorly fashioned
for the software design tool industry, the industry has evolved to
minimize the impact that patents have on competition and has relied on
other more market-oriented drivers of innovation. I believe this is a
missed opportunity for accelerating technological and economic growth
in the industry.

#GWr: Greenhall (Divx) 2022: 35% of R&D funds diverted to sabre rattling

#Ret: R. Jordan Greenhall, Chief Executive Officer, Divx Networks, explained
at the FTC hearings how wasteful the patent process has become in the
software field.

#AWe: As a small company, one of the biggest risks I face is uncertainty in
the marketplace. I can minimize my risk by understanding my
competitor's products very well, by understanding my products very
well, by understanding what the consumers and customers want. But I've
found in the past year that I really can't understand the patent
landscape and that I'm sitting with a nuclear bomb on top of my
products that could go off at any point and cause me to simply not
have a business anymore.

#InW: I recently took one of my lead developers, a gentleman who's widely
considered a leader in his field -- he sits on both the MPEG and the
ITU committees, is deeply involved with the entire intellectual
property landscape around digital video --  and asked him to evaluate
a particular patent that we've  been hearing about in the marketplace.

#Wun: We did a quick search on the USPTO website, which by the way is very
useful, and uncovered no less than 120 patents that claim to be within
the general scope of this particular patent, which was widely cited.

#TnW2: The poor guy spent the better part of five days examining all these
different patents and came back to me saying, %(q:I haven't the
slightest idea whether or not we infringe on these patents, and
frankly, they all seem to infringe on one another.)

#Trl: The end result being that I have no idea whether my product infringes
on upwards of 120 different patents, all of which are held by large
companies who could sue me without thinking about it.

#Tse: The end result, much like Borland, I have now issued a directive that
we reallocate roughly 20 to 35 percent of our developer's resources
and sign on two separate law firms to increase our patent portfolio to
be able to engage in the patent spew conflict. I think the concept
here would be called saber rattling. I need to be able to say,
%(q:Yeah, I've got that patented too, so go away and leave me alone.)

#DnA: Douglas Brotz (Adobe) 1994

#S9n: At the USPTO hearings of 1994, Adobe's representative said:

#LWo: Let me make my position on the patentability of software clear. I
believe that software per se should not be allowed patent protection. 
I take this position as the creator of software and as the beneficiary
of the rewards that innovative software can bring in the marketplace. 
I do not take this position because I or my company are eager to steal
the ideas of others in our industry.  Adobe has built its business by
creating new markets with new software.  We take this position because
it is the best policy for maintaining a healthy software industry,
where innovation can prosper.

#Fhn: For example, when we at Adobe founded a company on the concept of
software to revolutionize the world of printing, we believed that
there was no possibility of patenting our work. That belief did not
stop us from creating that software, nor did it deter the savvy
venture capitalists who helped us with the early investment. We have
done very well despite our having no patents on our original work.

#Oed: On the other hand, the emergence in recent years of patents on
software has hurt Adobe and the industry. A %(q:patent litigation tax)
is one impediment to our financial health that our industry can
ill-afford.  Resources that could have been used to further innovation
have been diverted to the patent problem. Engineers and scientists
such as myself who could have been creating new software instead are
working on analyzing patents, applying for patents and preparing
defenses.  Revenues are being sunk into legal costs instead of into
research and development. It is clear to me that the Constitutional
mandate to promote progress in the useful arts is not served by the
issuance of patents on software.

#Jpo: Joshua Kaplan (Intouch) 2002

#Wfe: At the FTC hearings of 2002, Kaplan explained:

#Ikn: Intouch is an e-business company that owns many patents and has been
attacked with patents, including by Amazon.  Joshua Kaplan is their
president and CEO.  These excerpts are from his statement at the FTC
hearings of 2002:

#Tls: There are patents that come out today with hundreds of claims,
unintelligible to almost anyone except the people who drew them. And
yet, people who violate them jeopardize sometimes a lifetime of
investment or their division or their product. That system doesn't
work well to spur innovation or carry out the constitutional mandate.

#Iid: Indeed, for those of you who were here this morning and listened to
the people in the software industry talk about how threatening this is
to their businesses, as I see it, patents today are often entrenching
the established at the expense of allowing the newcomer to come in. I
question today whether a Steve Jobs could start an Apple or a Bill
Gates could start a Microsoft in view of the web and thicket of
patents that is out there.

#OWr: Oracle 1994

#toW: From Oracle's statement submitted to the hearings on software
patentability at the US Patent Office in 1994

#OWW: Oracle Corporation opposes the patentability of software.  The Company
believes that existing copyright law and available trade secret
protections, as opposed to patent law, are better suited to protecting
computer software developments.

#Plm: Patent law provides to inventors an exclusive right to new technology
in return for publication of the technology. This is not appropriate
for industries such as software development in which innovations occur
rapidly, can be made without a substantial capital investment, and
tend to be creative combinations of previously-known techniques.

#UWs: Unfortunately, as a defensive strategy, Oracle has been forced to
protect itself by selectively applying for patents which will present
the best opportunities for cross-licensing between Oracle and other
companies who may allege patent infringement.

#Auu: Although all that I %(q:invented) were innovative, all utilized
complex procedures and all were valued by those who paid millions to
use what my innovative entrepreneurial risk created, it never occurred
to me to patent them, and I could not have patented those %(q:useful
arts) if I had wanted to.

#TWW: The fundamental question is: Do we want to permit the monopoly
possession of everything that works like logical intellectual
processes. I hope not.

#Tam: The mind has always been sacrosanct. The claim that intellectual
processes and logical procedures (that do not primarily manipulate
devices) can be possessed and monopolized extends greed and avarice
much too far. Algorithmic intellectual processes must remain
unpatentable -- even when represented by binary coding in a computer;
even when executed by the successor to the calculator.

#Wnw: What frightens and infuriates so many of us about software patents is
that they seek to monopolize our intellectual processes when their
representation and performance is aided by a machine.

#EWl: Everything that is represented or performed by software is first a
completely-detailed algorithmic intellectual process. There are no
exceptions, other than by error.

#TWp: Thus, I respectfully object to the title for these hearings --
%(q:Software-Related Inventions) -- since you are not primarily
concerned with gadgets that are controlled by software. The title
illustrates an inappropriate and seriously-misleading bias. In fact,
in more than a quarter-century as a computer professional and observer
and writer in this industry, I don't recall ever hearing or reading
such a phrase -- except in the context of legalistic claims for
monopoly, where the claimants were trying to twist the tradition of
patenting devices in order to monopolize the execution of intellectual
processes.

#TWn: There is absolutely no evidence, whatsoever -- not a single iota --
that software patents have promoted or will promote progress.

#TkW: The company for which I am speaking, Autodesk, holds some number of
software patents and has applied for others -- which, of course,
remain secret under current U.S. law. However, all are defensive -- an
infuriating waste of our technical talent and financial resources,
made necessary only by the lawyer's invention of software patents.

#Ara: Autodesk has faced at least 17 baseless patent claims made against it
and has spent over a million dollars defending itself, with millions
more certain to pour down the bottomless patent pit unless we halt
this debacle. Fortunately -- unlike smaller software producers -- we
have the financial and technical resources to rebuff such claims.  We
rebutted all but one of the claims, even before the patent-holders
could file frivolous law-suits, and will litigate the remaining claim
to conclusion. Note that your Office has issued at least 16 patents
that we have successfully rebutted, and we never paid a penny in these
attempted extortions that your Office assisted.

#Beb: But it was an enormous waste of resources that could have better been
invested in useful innovation. These unending baseless claims benefit
patent lawyers, but they certainly do not promote progress.

#Wso: We offer two recommendations, the second having twelve parts -- so to
speak, the 12 Apostles of Redress:

#Foe: FIRST: Issue a finding that software, as I have defined it, implements
intellectual processes that have no physical incarnation; processes
that are exclusively analytical, intellectual, logical and algorithmic
in nature. Use this finding plus the clearly-stated Constitutional
intent, to declare that the Patent Office acted in error when it
granted software patents. Declare that software patents monopolize
intellectual and algorithmic processes, and also fail to fulfill the
Constitutional mandate to promote progress -- that in fact, they
clearly threaten it.

#Sdt: SECOND: Until -- and only until -- software patents are definitively
prohibited, reject or at least freeze all such applications that have
not yet been granted, pending conclusive action on all of the
following twelve recommendations:

#RtW: REDRESS SERIOUS ERRORS OF PREVIOUS ADMINISTRATIONS: Issue a finding
that there have been extensive and serious errors of judgment in a
large percentage of software patents granted in the past, and
immediately recall all software patents for re-review and possible
revocation.

#Leh: Let us stand on each others' shoulders, rather than on each others'
toes.

#Mft: Mitch Kapor 1994

#Bhb: Because it is impossible to know what patent applications are in the
application pipeline, it is entirely possible, even likely, to develop
software which incorporates features that are the subject of another
firm's patent application.  Thus, there is no avoiding the risk of
inadvertently finding oneself being accused of a patent infringement
simply because no information was publicly available at the time which
could have offered guidance of what to avoid.

#Tor: The period of patent protection, 17 years, no longer makes sense in an
era when an entire generation of technology passes within a few years.

#IiW: If some future litigant is successful in upholding rights to one of
these %(q:bad) patents, it will require expensive and time-consuming
litigation, whose outcome is frankly uncertain, to defend the rights
of creators which should never have been challenged in the first
place.

#BEf: Bill Gates 1991:  Patents exclude competitors, lead industry to
standstill

#T9o: This was quoted by Fred Warshofsky in %(q:The Patent Wars) of 1994. 
The text is from an internal memo written by Bill Gates to his staff. 
Part of has appeared in another %(cc:Gates memos).

#IWe: If people had understood how patents would be granted when most of
today's ideas were invented and had taken out patents, the industry
would be at a complete standstill today.  ... The solution is
patenting as much as we can. A future startup with no patents of its
own will be forced to pay whatever price the giants choose to impose. 
That price might be high. Established companies have an interest in
excluding future competitors.

#rGW: Microsoft Germany 2003

#aea: linux_partner_brosch.pdf

#HmO: Håkon Wium Lie, CTO of Opera

#Tlu: The norwegian software comany Opera Inc develops a web browser which
is well known for its stability, compactness and speed.  Due to its
position as a quality leader, Opera also develops the multimedia
software that is used in Nokia's mobile phones.  Opera Software
supports the Eurolinux campaign for a software patent free Europe. 
Their CTO Håkon Wium Lie published the following statement at the W3C
at the occasion of whether fee-based (RAND) or only royalty-free (RF)
standards should be accepted by W3C in early 2002:

#Ofr: Opera Software's position in the RF/RAND debate is that the
fundamental standards for the Web must continue to be royalty free
(RF). Therefore, we do not think W3C should describe procedures for
RAND licensing.  Doing so would help legitimize software patents which
we think are harmful to the development of the Web. Also, software
patents is largely an American concept not recognized in other parts
of the world.

#LoW: Linus Torvalds 2002/08

#Lhm: Linux VM hackers are engaged in ongoing discussions on both large page
support (covered last week) and improving the performance of the new
reverse mapping mechanism. That conversation slowed down, however,
when Alan Cox pointed out that a number of the techniques being
discussed are covered by patents. In fact, a closer look by Daniel
Phillips shows that a number of existing Linux methods, including
reverse mapping in general and the buddy allocator, are covered by
these patents. This is a problem, he said, that we can't ignore.  That
was Linus's cue to jump in with his policy on software patents and
kernel code.  He later conceded that this was not %(q:legally tenable
advice) but the only way to keep developping the kernel without going
nuts.

#Ins: I do not look up any patents on %(e:principle), because (a) it's a
horrible waste of time and (b) I don't want to know.

#Thy: The fact is, technical people are better off not looking at patents.
If you don't know what they cover and where they are, you won't be
knowingly infringing on them. If somebody sues you, you change the
algorithm or you just hire a hit-man to whack the stupid git.

#TFa: %(AL), author of %(VD), a free software tool for converting multimedia
file formats, reports sad news:

#TFa2: Today I received a polite phone call from a fellow at Microsoft who
works in the Windows Media group. He informed me that Microsoft has
intellectual property rights on the ASF format and told me that,
although the implementation was still illegal since it infringed on
Microsoft patents. I have asked for the specific patent numbers, since
I find patenting a file format a bit strange. At his request, and much
to my own sadness, I have removed support for ASF in VirtualDub 1.3d,
since I cannot risk a legal confrontation.)

#Woy: Tord Jansson (SE): Corporations controlling my work thanks to legal
uncertainty introduced by EPO

#dat: In early 2003, Tord Jansson, developper of a streaming software called
BladeEnc, wrote to a member of the European Parliament:

#eml: I'm a professional software developer who early summer 1998 wrote a
computer program that I decided to put on my homepage. The program
turned out to be a tremendous success and was quickly distributed in
millions of copies, obviously filling a need among many computer
users. I quickly started to improve my program and release new
versions. That same autumn I was contacted by a large company with a
competing product, who claimed that my program infringed on certain
patents they had been granted. Consulting SEPTO gave no reason to take
infringement claims seriously since computer programs are not
patentable as such, but in early 1999 my legal advisor explained that
the legal uncertainty lately introduced by EPO would perhaps make the
claims valid. That eventually forced me to stop making my program
available.

#atc: Do you believe a corporation should have the right to control what
computer programs I can write and publish?

#raf: Marcel Martin (FR): I had to stop ...

#oWt: Oberthur Card System applied in 1999 for a patent on a method of
geometry (point-halving in elliptic curves).  In Oct 2001, the
Oberthur's legal department sent a cease-and-desist letter to Marcel
Martin, French informatics student and author of the shareware library
HIT, in which it asked him to %(q:immediately stop marketing your
product).  Which he did, although the legal status of Oberthur's
patent claims particularly in Europe is very unclear. Martin explains:

#edw: I had to stop this project, because I cannot afford to pay an army of
lawyers every time someone wants to impose conditions on my work.
Software developpers react very sensitively to this kind of terrorism.
 If European politicians legalise software patents in Europe, that
will work as a disinscentive to software production in Europe.

#Mnt: %(MV) in %(LDI) of 1998

#Lkm: Is software now finally patentable?

#SuW: Without doubt not yet.

#Eee: In reality, the national and conventional rules are clear:  they
stipulate without ambiguity a principle of non-patentability of
software.  The game which is being played today consists in twisting
these rules one way or another, e.g. by imagining to consider, as we
have seen, the totality of software and hardware as a virtual machine
which is potentially patentable (tomorrow ...).  From that point on
one can speak about software in patent language.  The patents which
may be obtained this way, by this channel or by another, however still
do not have any value beyond what we lend to them - but of course it
is possible that they will finally acquire a value simply through an
informal consensus to stop discussing the question.  In fact, the
efficiency of this twisting of rules of law is largely dependent on
whether this consensus evolves to take for granted -- against the
rules of written law -- that we will play this game or not.  This
question is no longer a legal question in the strict sense of the
term.

#Soi: In this domain, we must base our judgement on the evolution of the
written rules, i.e. whether the exclusions of non-patentable subject
matter are deleted or not.

#Bro: Bundegerichtshof 1976

#Sai: However in all cases the plan-conformant utilisation of controllable
natural forces has been named as an essential precondition for
asserting the technical character of an invention.  As shown above,
the inclusion of human mental forces as such into the realm of the
natural forces, on whose utilisation in creating an innovation the
technical character of that innovation is founded, would lead to the
consequence that virtually all results of human mental activity, as
far as they constitute an instruction for plan-conformant action and
are causally overseeable, would have to be attributed a technical
meaning.  In doing so, we would however de facto give up the concept
of the technical invention and extend the patent system to a vast
field of achievements of the human mind whose essence and limits can
neither be recognized nor overseen.

#Eii: It can furthermore be argued with good reasons that, given the
unanimity with which the jurisdiction and the legal literature have
always insisted on limiting the patent system to technical inventions,
the above reasoning constitutes a theorem of customary patent law.

#Dnu: Whether we want to postulate such a theorem is however not essential
for this discussion, because also from a purely objective point of
view the concept of technical character seems to be the only usable
criterion for delimiting inventions against other human mental
achievements, for which patent protection is neither intended nor
appropriate.  If we gave up this delimitation, there would for example
no longer be a secure possibility of distinguishing patentable
achievements from achievements, for which the legislator has provided
other means of protection, especially copyright protection.  The
system of German industrial property and copyright protection is
however founded upon the basic assumption that for specific kinds of
mental achievements different specially adapted protection regulations
are in force, and that overlappings between these different protection
rights need to be excluded as far as possible.  The patent system is
also not conceived as a reception basin, in which all otherwise not
legally privileged mental achievements should find protection.  It was
on the contrary conceived as a special law for the protection of a
delimited sphere of mental achievements, namely the technical ones,
and it has always been understood and applied in this way.

#Ene: Any attempt to attain the protection of mental achievements by means
of extending the limits of the technical invention -- and thereby in
fact giving up this concept -- leads onto a forbidden path.  We must
therefore insist that a pure rule of organisation and calculation,
whose sole relation to the realm of technology consists in its
usability for the normal operation of a known computer, does not
deserve patent protection.  Whether it can be awarded protection under
some other regime, e.g. copyright or competition law, is outside the
scope of our discussion.

#Beh: Bundesgerichtshof 1980

#Dzg: The German Federal Court of Justice explains why a new rule for
optimising the use of steel in a steam rolling factory is not a
technical invention.

#Wdg: /swpat/papers/bgh-dispo76/index.en.html

#GKW: Gert Kolle 1977

#GdW: Gert Kolle, today a chief diplomat of the EPO, was in the 1970s the
leading theoretician on the limits of patentability with regard to
computer programs.  His most-quoted article explains why algorithms
are not technical and cannot be patented under the present law and why
changing the law would be dangerous.  Kolle's article is even today
still one of the most profound and lucid treatises on its subject,
worth reading from beginning to end.  We quote here only one paragraph
which shows why Kolle was able to know everything that we know today
on this subject:  because the role of software in society was already
well established in the 70s and, contrary to what proponents of
software patents like to argue, has not changed much since then:

#Wmr: Automatic Data Processing (ADP) has today become an indispensable
auxiliary tool in all domains of human society and will remain so in
the future.  It is ubiquitous.  ... Its instrumental meaning, its
auxiliary and ancillary function distinguish ADP from the ...
individual fields of technology and liken it to such areas as
enterprise administration, whose work results and methods ... are
needed by all enterprises and for which therefore prima facie a
need-to-keep-free is indicated.

#EPf: European Patent Office 1978

#Ilt: /swpat/analysis/epc52/index.en.html

#Aso: A computer program may take various forms, e.g. an algorithm, a
flow-chart or a series of coded instructions which can be recorded on
a tape or other machine-readable record-medium, and can be regarded as
a particular case of either a %(e:mathematical method) or a
%(e:presentation or information).  If the contribution to the known
art resides solely in a computer program then the subject matter is
not patentable in whatever manner it may be presented in the claims. 
For example, a claim to a computer characterised by having the
particular program stored in its memory or to a process for operating
a computer under control of the program would be as objectionable as a
claim to the program %(e:per se) or the program when recorded on
magnetic tape.

#IWn: In considering whether an invention is present, [the examiner] should
disregard the form or kind of claim and concentrate on the content in
order to identify the novel contribution which the alleged
%(qc:invention) claimed makes to the known art.  If this contribution
does not constitute an invention, there is not patentable subject
matter.  This point is illustrated by the examples %(dots) of
different ways of claiming a computer program.

#RLt: Prof. Dr. iur. Rudolf Kraßer 1986

#Tmr: This standard manual explains the underlying legal systematics of Art
52 EPC and of the German jurisprudence.  In the concluding paragraphs
it discusses pressures to remove the borderlines of patentability by
means of caselaw and explains why such a development would be illegal.
 Unfortunately the EPO didn't care:  it embarked on this illegal
development in 1986.

#1en: 1. The limitation of patentability to the field of technology has as a
concequence that important achievements with economic value remain
ineligible.

#SpW: Thus the finding of the solution principle underlying a computer
program, its implementation in a preparatory stage such as flow chart
and finally the elaboration of the machine-readable program all
require considerable efforts.  The mental achievements accomplished
therein are certainly not in general inferior to those in many
patentworthy technical inventions.  The economic value of data
processing software is often considerable; ..

#Das: The Federal Court of Justice (FCJ/BGH) however refuses to drop the
requirement of technical character.

#Dnn: The FCJ also deems it impermissible to extend the scope of
patentability by stretching the concept of technology.

#ArW: Also in legal writings the delimiting function of the requirement of
technical character is mentioned.

#Ien: If loosened, all teachings for mental activity are opened to patent
protection; such a step would be objectionable due to the freedom
needs regarding the working results and methods of microeconomics (for
managment, organisation, accounting, financing, advertising, marketing
etc), which are needed by all enterprises, and algorithms, which
underly computer programs.

#Aeo: On the other hand there have been calls for review of the exclusion of
non-technical activities, because this exclusion is said to have
resulted from historical developments and meanwhile become obsolete.

#2nt: 2. As far as the %(s:law in force) is concerned, the limitation of
patentability to technical inventions, as reinforced by the FCJ, will
have to remain in vigor.  Apart from the explicit regulation in  Art
1.2 of the German Patent Act and EPC Art 52.2, also the existing
institutional and organisational framework of the patent system, which
is designed for the technical field, does not allow to deviate from
these constraints by means of a change at the level of caselaw.

#Eht: If a %(s:change at the level of written law) would have to see to it
that exclusions of unoverseeable scope are avoided; thus at least
discoveries, scientific theories and mathematical methods must remain
excluded from patentability.

#3iz: 3. Contrastingly, in the area of %(s:non-technical instructions for
action) and of %(s:information processing) the application of changes
in practise would not necessarily be so sweeping that the scope of the
exclusion right would become incalculable.  Whether patentability in
this field is recommendable depends on how the protection and
rewarding interests are to be weighted in comparison to the freedom
interests.

#Tkt: In general the freedom interests in this area will probably have
greater weight than in the area of technical inventions, because
exclusion rights for innovations which humans can use without
harnessing forces of nature are to a lesser degree dependent on
extra-human objects and therefore intervene in his freedom of action
more directly and strongly.  In particular, exclusion rights on
commercial innovations could lead to considerable anti-competitive
effects.

#Kee: No sufficient argument for granting of patent protection can be found
in the fact that the mental achievement to which it is to be accorded
is otherwise not or not fully protected.  On the contrary, the limits
of the protection that is attainable under current law can be seen as
indications that the freedom interests are of greater weight.

#Pio: Karl Friedrich Lenz subjects the argumentation of the EPO in its
decisions of 1998 to a criticism based on the conventional methods  of
interpretation of law: the gramatical-lexical, systematic, historic,
teleological and constitution-based method.  Lenz concludes that on
all five accounts the caselaw of the EPO appears to be irreconcilable
with  the written law.

#DWx: Dies ist so weit vom Wortlaut entfernt, dass eine Bestrafung aufgrund
eines infolge dieser gesetzwidrigen Auslegung erteilten Patentes mit
dem Gesetzlichkeitsprinzip (Art. 103 Absatz 2 des Grundgesetzes) in
Widerspruch steht. Es ist eine völlige Neuformulierung der Schranke in
Absatz 3, die mit dem Wortlaut des Gesetzes nichts mehr gemein hat.
Die technische Beschwerdekammer überschreitet damit klar die Grenzen
richterlicher Tätigkeit. Wer die Formulierung %(q:als solche) durch
die Formulierung %(q:ohne technischen Charakter) ersetzen möchte, muss
dies durch eine entsprechende Änderung des Vertragstextes nach den
dafür erforderlichen Verfahren bewirken. Die Rechtsprechung kann dies
nicht.

#soa: Die Behauptung, die Beschränkung des Ausschlusses von Software von der
Patentierbarkeit auf Software als solche in Absatz 3 habe den Zweck,
im Lichte der Entwicklung der Informationstechnik durch Anerkennung
der Patentierbarkeit den technischen Fortschritt zu fördern, überzeugt
nicht. Falls der Gesetzgeber einen solchen Zweck verfolgt haben
sollte, hätte er den Ausschluss in Absatz 2 von vornherein nicht
vorgesehen. Die Unterstellung eines mit dem gewünschten Ergebnis
übereinstimmenden Gesetzeszweckes ist zwar keine korrekte Anwendung
der teleologischen Auslegungsmethode, zeigt aber deutlich die
Bereitschaft der technischen Beschwerdekammer, die eigenen Wertungen
an die Stelle der Wertung des Gesetzgebers zu setzen.

#biw: Preambule to Nordic Patent Law of 1963

#oae: Finland, Sweden, Norway and Denmark had a joint patent system before
they joined the European Patent Convention.  Their patent law included
a statement about the technical invention which closely resembles the
Dispositionsprogramm doctrine, thereby showing that it is not an
invention of german lawcourts:

#oiu: The definition of the concept of invention, which is
constitutionalized in the Nordic countries, contains the requirement
that the invention must have a %(it:technical character).  An exact
definition of what this means can hardly be given, but within the
concept lies definitely a requirement that an invention must be a
solution of a problem by means of natural forces, i.e. by means of a
causally determined use of natural matter and energy.

#Tgi: This sigh shows that the patent system has tended to run out of
control for a long time already.

#SbW2: Stuber 1907

#AeW: Arguing against the using economics to determine the scope of the
patent system.

#Has: How far legal protection should reach different fields of industry is
primarily a field for the Jurists.

#JKl: J. Kohler

#Aig: After jurisprudence has taken hold of any area treated by the law, it
is up to science to develop it and all the other disciplines must
resign; from now on it is the method of judicial thinking which must
rule.

#PaW: Patents and Industrial Progress, Law and Contemporary Problems

#Qne: Questioning the qualification of Walter Hamilton to write on the
subject of patents.

#WiW: What are those qualifications?  Is he a lawyer?  Has he ever practiced
law?  Has he any law degree? ... Professor Hamilton ... prior to his
Professorship in the Yale Law School was a Professor of Economics ...
It does not appear that an affirmative answer could be given ... to
any of the foregoing pertinent questions as to his qualifications to
speak as an expert on the subject of patents or the patent system.

#Kee2: PA Stephan Kinsella 2002

#Shf: Stephan Kinsella is a registered patent attorney and earns his living
by helping people obtain patents.  Yet he is highly critical of the
system and of fellow patent lawyers.  When one of these colleagues
attacked Stanford law professor Lawrence Lessig as a %(q:pompous
pedagogue pronouncing patent policies), Kinsella commented:

#IVl: In fact, in my view, most patent lawyers -- most lawyers in general --
fit into the category %(q:Pompous Pedagogues Pronouncing Patent
Policies), to the extent they themselves unthinkingly spout pro-patent
slogans. That is because most patent and IP and even other attorneys
with an opinion on this issue mindlessly parrot the simpleminded
economics with which they were propagandized in law school. Virtually
every patent lawyer will reiterate the mantra that %(q:we need patents
to stimulate innovation,) as if they have given deep and careful
thought to this. Of course, virtually none of them have. They repeat
what they have read in Supreme Court and CAFC (Court of Appeals for
the Federal Circuit, the primary federal appellate court dealing with
patent law issues) opinions as if the positive law enunciated by
government functionaries is some Holy Writ. It does not take a genius
to figure out why most patent lawyers are in favor of the patent
system; and it is not because they have really studied the matter and
dispassionately concluded that society is better off with a patent
system -- it is because they don't want to see the system that pays
the mortgage for them eroded or abolished.

#hrP: Dr. Matthew Lee 1994: Forces behind Changes of EPO Practise after 1985

#ahj: A UK-educated barrister in Honkong specialising in computing law at
Hongkong University analyses the history of patent examination
guideline revisions at the EPO.  Lee starts by exaggerating the
restrictive character of the EPO's first examination guidelines of
1978: %(bc:The implication of this approach would be to severely
narrow the scope of patentability for software-related inventions.
Inventive process control %(tp|mechanisms|e.g. those used in a
conventional chemical plant to produce new polymers) that would
otherwise be standard patent material would fall outside the scope of
patentable subject-matter simply because a program was used in
implementing the inventive process control scheme.)  This, according
to Lee, apparently did not disturb the chemical industry as much as
certain other customers of the EPO:

#tei: However, in response to pressure from the computer industry and trends
emerging in the US, the European Patent Office reviewed its guidelines
in 1985.

#rsm: Federal Patent Court 2002: Technical Contribution Doctrine Makes All
Business Methods Patentable

#rfa: The 17th senate of Germany's Federal Patent Court explains how the EPO
doctrine of %(q:technical problem) or %(q:technical contribution)
works and how it leads to unlimited patentability and is therefore
incompatible with Art 52 EPC:

#bpatg17: If computer implementation of non-technical processes were attributed
a technical character merely because they display different specific
characteristics, such as needing less computing time or storage space,
the consequence of this would be that any computer implementation
would have to be deemed to be of technical character.  This is because
any distinct process will have distinct implementation
characteristics, that allow it to either save computing time or save
storage space.  These properties are, at least in the present case,
not based on a technical achievement but are pre-determined by the
chosen non-technical method.  If the completion of such a task could
be a sufficient reason to attribute technical character to a computer
implementation, then every implementation of a non-technical method
would have to be patentable; this however would run against the
conclusion of the Federal Court of Justice that the legal exclusion of
computer programs from patentability does not allow us to adopt an
approach which would make any teaching that is framed in
computer-oriented instructions patentable.

#eei: /swpat/papers/eubsa-swpat0202/index.en.html

#2nW: Schäfers 2003: Patent Examination in Bioinformatics no longer feasible

#EWW: In a letter to MEP Wuermeling, a german IT lawyer writes:

#edp: We and our clients, mainly small software companies, are concerned
about the proposed patent directive.

#Wer: Recently we had to examin a patent conflict with a US patent and
employed a highly specialised german patent law firm for this purpose.

#bes: The diagnosis of this firm (costing 4000,00 eur) was: for examining
whether the US patent in question is being violated, the US patent
attorney demanded pre-payment of 25,000.00 USD.  Moreover, we were
told that, as a result of this work, we would still not receive any
definite information as to whether we are infringing or not.  The
answer would rather be something like %(q:possibly ...), %(q:it can
not be excluded that ...).  This was not to say that the US colleague
was not competent.  Rather, the german patent law took account of the
common experience that in the field of bioinformatics reliable patent
examinations are simply no longer feasible.

#0pW: PS.BE 2002: Software Copyrightable, not Patentable

#toe: The Belgian Socialist Party's Election Program of 2003 stipulates on
page 110:

#0Ea: Henkel 2002/12/29: Why the EU should ban software patents

#Wmh: In the central organ of the German Association of Engineers (VDI), Dr.
Joachim Henkel, an innovation researcher from Munich University,
explains why software patents are harmful and demands that the
European Commission should clearly ban them.

#goi: Highly tentative conclusions that can be made based on the literature
suggest that stronger patent rights may create substantial problems in
the telecommunication sector. First, strong patent rights may cause
%(q:patent portfolio race). In other words, companies may use patents
primarily not to protect their technological invention itself but as
instruments with which to trade in order to be able to negotiate
access to external technologies. Given the observed entry deterrence
strategies of the incumbents, stronger patent rights might provide
with them new powerful weapons to defend monopolistic market
positions.

#uno: Thus, stronger patent rights may hinder the development of effective
competition in the telecommunication markets. Patentability of
principles or ideas might further result in strategic patenting
against compatibility. This could be particularly lethal to the
content industry and further to the markets of the future generations
of cellular mobile telephones and services. Currently avail able
empirical evidence does not allow us to make definite conclusions.
However, it suggests that strengthening of patent rights in the
communication sector or extending patent protection to cover
intellectual property currently protected by copyright involves great
potential risks.

#TWe: The patent system fits best a model of progress where the patented
product, which can be developed for sale to consumers, is the discrete
outcome of a linear research process. The safety razor and the
ballpoint pen are examples, and new drugs also share some of these
characteristics.  By contrast in many industries, and in particular
those that are knowledge-based, the process of innovation may be
cumulative, and iterative, drawing on a range of prior inventions
invented independently, and feeding into further independent research
processes by others. ... The development of software is very much a
case of building incrementally on what exists already. Indeed, the
Open Source Software Movement depends precisely on this characteristic
to involve a network of independent programmers in iterative software
development on the basis of returning the improved product to the
common pool.

#DWr: Developed and developing countries have historically provided that
certain things do not constitute inventions for the purpose of patent
protection. Included in these are those set out, for example, in
Article 52 of the European Patent Convention (EPC):

#Edo: Even though subsequent EPO practice and jurisprudence have to some
extent diluted the scope of these Articles,13 it would seem entirely
reasonable for most developing countries to adopt this list of
exclusions as a minimum.

#Snt: Summary of Recommendations Relating to the Patent System

#Eim: Exclude from patentability computer programs and business methods

#Rra: Rechtlicher Ausgangspunkt für die Betrachtungen ist das Europäische
Patentübereinkommen (EPÜ). Es wurde 1973 in München unterzeichnet und
zählt 20 Vertragsstaaten. Europäische Patente werden generell für
Erfindungen erteilt, die neu sind, auf erfinderischer Tätigkeit
beruhen und gewerblich anwendbar sind. Expressis verbis ausgeschlossen
sind gemäß Art. 52 Abs. 2 lit. c EPÜ jedoch %(q:Programme für
Datenverarbeitungsanlagen). Dennoch hat das Europäische Patentamt
(EPA) vielfach Patente für technische Erfindungen erteilt, bei denen
ein Computerprogramm verwendet wird. Das EPA geht nämlich davon aus,
dass durch das EPÜ nicht alle Computerprogramme von der
Patentierbarkeit ausgeschlossen sind und verweist auf den
%(fn:3:technischen Charakter) einer Erfindung als wesentliche
Voraussetzung für ihre Patentierbarkeit. Es unterscheidet folglich
zwischen patentfähigen und nicht patentfähigen Computerprogrammen nach
dem Kriterium der Technizität. Demnach ist ein rein abstraktes Werk
ohne technischen Charakter nicht patentfähig; es muss ein zusätzlicher
technischer Effekt (Mehreffekt) vorliegen. Das EPA schafft damit einen
zusätzlichen Ausschlusstatbestand, der sich aus Wortlaut und
Systematik des Art. 52 EPÜ nicht ergibt und nur mit einem
Zirkelschluss begründet werden kann. Da der technische Charakter einer
Erfindung Voraussetzung für ihre Patentierbarkeit ist und das EPÜ ein
Patentierungsverbot für Computerprogramme enthält, muss es nach dem
Verständnis des EPA Computerprogrammen ohne zusätzlichen technischen
Effekt an der Technizität mangeln. Diese Auslegung zäumt das Pferd von
hinten auf und ist mit dem Wortlaut des Art. 52 EPÜ nicht vereinbar.

#AWh: Aus ökonomischer Sicht ist die Sinnhaftigkeit eines Patentschutzes
danach zu beurteilen, ob dieser effiziente Anreize für die Investition
in Forschung und Entwicklung setzt. Immaterielle Güter zeichnen sich
dadurch aus, dass sie beliebig und kostenlos reproduzierbar sind und
der Konsum von Wissen durch eine Person eine andere Person nicht daran
hindert, dieses Wissen ebenfalls zu konsumieren. Der Einzelne wird
deshalb nur so viel in die Produktion von Wissen investieren, wie er
durch seinen eigenen Konsum rechtfertigen kann. Dies führt insgesamt
zu einer ineffizient niedrigen Produktion von Wissen. Deshalb müssen
Anreize, z.B. durch Patente, geschaffen werden, die die
kostenintensive Produktion von Wissen in einem darüber hinausgehenden
Maß bewirken.  Ökonomisch gesehen wird bei einem Patent ein
(ineffizientes) Monopol vorübergehend gewährt, um Produktionsanreize
zu setzen. Das soeben dargestellte Modell ist weitgehend statisch.
Kompliziertere Szenarien haben demgegenüber gezeigt, dass eine
Verstärkung des Patentschutzes keineswegs zwingend zu einem vermehrten
Forschungsaufwand führt. Im Gegensatz zur allgemeinen Annahme, dass
weitgehender immaterialgüterrechtlicher Schutz zu höherer
Investitionstätigkeit führt, vermochten %(fn:6:Untersuchungen, die
unter ähnlichen Bedingungen wie die der Softwareindustrie operiert),
eine generelle Zunahme der Ausgaben für Forschung und Entwicklung
nicht %(fn:7:nachzuweisen). Empirische Studien über das Verhalten von
kleinen und mittleren Unternehmen im Softwarebereich haben gezeigt,
dass Patente für diese zu den am wenigsten effizienten Methoden des
Investitionsschutzes zählen.

#Vnz: Vor diesem Hintergrund ist eine Ausdehnung des Patentschutzes auf
Computerprogramme kritisch zu bewerten. Die Entwicklung von Software
ist generell nicht kapitalintensiv, außerdem bestehen auf diesen
Märkten bereits Netzeffekte, die wiederum Konzentrationstendenzen
begünstigen. Durch Softwarepatente werden insbesondere für kleine und
mittlere Unternehmen erhebliche Marktbarrieren entstehen. 
Open-Source-Softwareprodukte stellen ex definitione den Programmcode
allen Interessierten zur Verfügung und könnten Patentschutz deshalb
generell nicht in Anspruch nehmen. Die mit dem Patentschutz verbundene
vorübergehende Monopolstellung eines Unternehmens ist geeignet, die
Konzentrationstendenzen auf dem Markt für Softwareprodukte weiter zu
verstärken und den Wettbewerb zu behindern.

#WaW: Innovations in the software field are, more than innovations in other
fields, dependent upon previous innovations.  THerefore the negative
aspect of patents, that they make further development more difficult,
weighs particularly heavily in the case of software.  Licensing hardly
solves this problem, given the transaction costs involved.

#SSa: Seth Shulman 2000

#Jee: Just because software hasn't experienced a cyber-Bhopal doesn't mean
it won't ever happen. Indeed, the noxious clouds of litigation now
gathering around e-commerce are renewing industry fears. ... There's
ample historical evidence that overly broad patents have stifled
innovation in emerging industries.

#Tel: The effects of this substantial de facto broadening of patent subject
matter to cover information inventions are as yet unclear. Because
this expansion has occurred without any oversight from the legislative
branch and takes patent law into uncharted territories, it would be
worthwhile to study this phenomenon to ensure that the patent
expansion is promoting the progress of science and the useful arts, as
Congress intended.

#Tno: There are many reasons to be concerned. ...

#Ene2: Economic Effects of the Australian Patent System:  A Commissioned
Report to the Industrial Property Advisory Committee

#Tsn: This report, submitted in 1982, contains statistics about the use of
the patent system as a source of information and as a source of
revenues.  It finds the patent system as a whole to be of questionable
value.  The original draft recommended abolishing the system.  A later
draft instead recommended to raise the standards of patentability and
restrict certain abusive practises.

#SbW: Since the benefits of the patent system are so tenuous and subtle and
the overall benefit/cost ratio is considered to be negative, there is
no economic justification for extending patent monopolies by
lengthening the term, or by widening the grounds for either
infringement, or patentability (for example, Plant Variety Rights or
computer programs).  However, in the light of our findings, there is
considerable economic justification for policy action to reduce the
negative effects of the patent system by stricter examination, by
reducing the length of term and the scope of patent monopolies, and by
action to deal with undesirable restrictive practices in patent
licensing.

#AlW: An historical awareness of the political economy of patent reform
suggests that this task is not easy at the domestic policy level. This
is basically because those who perceive they would lose by such reform
are concentrated, powerful and active defenders of their interests. 
In contrast, those who would gain by patent reform are diffuse and
hardly aware of their interest in the matter.

#CWr: Canadian Department of Consumer and Corporate Affairs 1976

#Ieh: In a working Paper on Patent Law Revision the department finds that
the patent system is doing more harm than good to the economy and
recommends abolishing it.

#Ois: On the basis of the review and analysis contained in this first part
of the working paper it is evident that Canada should give serious
consideration to the possibility of abandoning the continued
maintenance of a patent system in any form.

#FMp: Fritz Machlup 1958

#Toa: The patent system was introduced in Germany in 1873 through a lobbying
effort of lawyers and protectionists who used the %(q:me too)
argument: other countries have it so we must too.  Most economists of
the time were opposed to the patent system.  Machlup's report to the
US congress contains a long account of the activities and writings of
this period.  This statement appears near its end.

#BRw: Bis zum Jahre 1873 war die Patentfrage heißumstrittenes Thema. Die
Volkswirte hatten ihren Standpunkt mit Nachdruck vertreten, eifrig
bemüht, die Öffentlichkeit und die Regierung zu überzeugen. Die
Niederlage der Patentgegner - die in Regierungskreisen von vielen als
ein Sieg der Juristen und anderer »Protektionisten« über die Mehrheit
der Nationalökonomen angesehen wurde - veränderte den Charakter der
volkswirtschaftlichen Erörterungen und Stellungnahmen zum Patentwesen.
Die Flut von Büchern, Flugschriften und Artikeln über die
wirtschaftlichen Grundlagen des Patentschutzes nahm ein Ende, die
Nationalökonomen hatten das Interesse an der Patentfrage verloren und
wandten sich anderen Problemen zu.

#EnW2: E. Penrose 1951

#Fsl: From %(q:The Economics of the International Patent System), 1951.

#Upy: Up to the present, the regime for the international protection of
patent rights has been developed primarily in the interests of
patentees.  The gains to be derived from an extension of the patent
system have been stressed, but the concomitant increase in social
costs has been seriously neglected.

#Gdr: Granstrand UNCTAD report 1990

#Fvp: From %(q:The use of patents for the protection of technological
innovation: A case study of selected Swedish firms) -- A commissioned
report for the UN Conference on Trade and Development Secretariat,
1990.

#PWr: Patents as an instrument to stimulate innovative activities appear to
be of little relevance for small firms.  It was found that no
significant changes in R&D behavior would take place if the patent
protection time were reduced or extended.  Also, for large firms, the
R&D behavior seems to be rather independent of the availability of
patent protection.  The survey showed that increased patent protection
time is likely to provide, at most, a modest stimulus for R&D
activities.  Chemical, and particularly pharmaceutical, firms appear
to be more sensitive to such changes.

#Soe: S. Scotchmer 1991

#SuW2: Standing on the Shoulders of Giants: Cumulative Research and the
Patent Law, Journal of Economic Perspectives, 1991.

#Ifp: It appears that patent policy is a very blunt instrument trying to
solve a very delicate problem.  Its bluntness derives largely from the
narrowness of what patent breadth can depend on, namely the realized
values of the technologies.  As a consequence, the prospects for
fine-tuning the patent system seem limited, which may be an argument
for more public sponsorship of basic research.

#MiW: M. Wirth 1866

#VWu: Vierteljahrschrift fur Volkswirtschaft und Kulturgeschichte, 1863.

#IWp: Inventions do not belong in the category of intellectual property,
because inventions are emanations of the current state of civilization
and, thus, are common property. ...  What the artist or poet create is
always something quite individual and cannot simultaneously be created
by anyone else in exact likeness.  In the case of inventions, however,
this is easily possible, and experience has taught us that one and the
same invention can be made at the same time by two different persons;
inventions are merely blossoms on the tree of civilization.

#MlW2: M. Polanyi 1944

#PRm: Patent Reform, Review of Economic Studies, 1944

#Iyd: I believe the law is essentially deficient because it aims at a
purpose which cannot be rationally achieved.  It tries to parcel up a
stream of creative thought into a serious of distinct claims, each of
which constitutes the basis of a separately owned monopoly.  But the
growth of human knowledge cannot be divided up into such sharply
circumscribed phases.  Ideas usually develop shades of emphasis, and
even when, from time to time, sparks of discovery flare up and
suddenly reveal a new understanding, it usually appears on closer
scrutiny that the new idea had at least been partly foreshadowed in
previous speculations.  Moreover, discovery and invention do not
progress only along one sequence of thought, which perhaps could
somehow be divided up into consecutive segments.  Mental progress
interacts at every stage with the whole network of human knowledge and
draws at every moment on the most varied and diverse stimuli.
Invention, and particularly modern invention which relies more and
more on a systematic process of trial and error, is a drama enacted on
a crowded stage.  It may be possible to analyze its various scenes and
acts, and to ascribe different degrees of merit to the participants;
but it is not possible, in general, to attribute to any of them one
decisive self-contained mental operation which can be formulated in a
definitive claim.

#LWi: L. Mises

#HAE: Human Action: A Treatise of Economics, 1949

#lhh: .... the fairness of the patent laws is contested on the ground that
they reward only those who put the finishing touch leading to
practical utilization of achievements on many predecessors.  These
precursors go empty-handed although their contribution to the final
result was often much more weighty than that of the patentee.

#FrH: Friedrich Hayek

#IWW: Individualism and Economic Order, 1948.

#Iho: In the field of industrial patents in particular we shall have
seriously to examine whether the award of a monopoly privilege is
really the most appropriate and effective form of reward for the kind
of risk bearing which investment in scientific research involves.

#All: Arnold Plant

#Tce: The Economic Theory Concerning Patents for Inventions, Economica,
1934.

#Ect: Expedients such as licenses of right, nevertheless, cannot repair the
lack of theoretical principle behind the whole patent system.  They
can only serve to confine the evils of monopoly within the limits
contemplated by the legislators; and, as I have endeavoured to show,
the science of economics, as it stands today, furnishes no basis of
justification for this enormous experiment in the encouragement of a
particular activity by enabling monopolistic price control.

#LoM: From a letter to Isaac McPherson

#Iss: If nature has made any one thing less susceptible than all others of
exclusive property, it is the action of the thinking power called an
idea, which an individual may exclusively possess as long as he keeps
it to himself; but the moment it is divulged, it forces itself into
the possession of everyone, and the receiver cannot dispossess himself
of it...He who receives an idea from me, receives instructions himself
without lessening mine; as he who lights his taper at mine, receives
light without darkening me. That ideas should be spread from one to
another over the globe, for the moral and mutual instruction of man,
and improvement of his condition, seems to have been peculiarly and
benevolently designed by nature ... Inventions then cannot, in nature,
be a subject of property.

#Jov: Johann Wolfgang von Goethe

#JGb: Johann Peter Eckermann, Gespräche mit Goethe in den letzten Jahren
seines Lebens, Berlin und Weimar, 1982, S.662 ff.

#IjW: Im Grunde aber sind wir alle kollektive Wesen, wir mögen uns stellen
wie wir wollen. Denn wie weniges haben und sind wir, das wir im
reinsten Sinne unser Eigentum nennen! Wir müssen alle empfangen und
lernen, sowohl von denen, die vor uns waren, als von denen, die mit
uns sind. Selbst das größte Genie würde nicht weit kommen, wenn es
alles seinem eigenen Inneren verdanken wollte. Das begreifen aber
viele sehr gute Menschen nicht und tappen mit ihren Träumen von
Originalität ein halbes Leben im Dunkeln. ... Es ist im Grunde auch
alles Torheit, ob einer etwas aus sich habe oder ob er es von anderen
habe; ob einer durch sich wirke oder ob er durch andere wirke: die
Hauptsache ist, daß man ein großes Wollen habe und Geschick und
Beharrlichkeit besitze, es auszuführen; ...

#tWE: Jean-Michel Yolin, president of the %(q:Innovation) section in the
French Ministery of Economics and author of the report %(IE), observes
in an interview about the SCO vs IBM/Linux case

#pat: Patents once served to make research and development efforts pay off. 
Meanwhile, instead of serving innovation, the patent system has been
twisted to become a means of mining the territory and neutralising
unwanted innovators by sending them lawyers to screw them up at a
moment where they are raising funds or looking for customers.

#Eoe: Economic and Social Committee of the European Community 2002/09

#Aot: An expert opinion adopted by vote of a consultative body of the
European Community says about the CEC directive proposal:

#AyE: Although  for  the  time  being  the  scope  of  application  of  the 
Commission's  proposal  for  a directive concerns computer-implemented
inventions, to which  are  attached  the  classic,  cumulative 
criteria limiting the field of application of patentability - which
will not satisfy those in favour of purely and simply abolishing all
limits on the field of application of patent law - the text is,
nonetheless, a de facto acceptance and justification of the a
posteriori drift of EPO jusriprudence. While at first glance the
directive seems to advocate something less extreme than the pure and 
simple  abolition  of  Article  52(2)  of  the  EPC,  which  is what 
the  EPO  executive  and  some  Council  members  want,  it  does 
nonetheless  open  the  way  to  the  future patentability  of  the 
entire  software  field,  in  particular  by  the  admission  that 
the  %(q:technical  effect) can amount to the simple fact of a program
running on a standard computer.

#Ohf: One  may  well  wonder  what  the  real  objective  of  the  Directive
 is,  in  particular  given  the explanatory memorandum, which begins
with considerations about the need to protect the software industry
against piracy, and  in  the  documents  appended  to  the  Directive 
discusses  almost  exclusively  software  and the %(q:software
industry), whose influence on the proposal seems excessive yet
entirely irrelevant, if the scope of application was really as limited
as the Commission maintains.

#IWW2: Is  it  wise  in  today's  world  to  widen  the  scope  of  patents, 
tools  of  the  industrial  age,  to intellectual  works  which  are 
immaterial,  such  as  software,  and  to  the  results  of  running 
software  on  a computer? The reply is quite explicit and partisan in
the presentation of the proposal for a directive and the impact 
assessment  form.  The  narrow  field  of  vision  that  has  been 
adopted,  based  on  the  legal  regime  for patents  as  the  sole 
motivation,  without  sufficient  consideration  of  the  economic 
factors,  the  impact  on research  or  on  European  companies, 
which  therefore  lacks  a  view  of  the  whole,  is  not  consistent
 with  the importance  of  the  implications  for  society,  for 
development  and  indeed  for  democracy  (e-administration, 
education, citizens' information), which in the longterm is what is at
stake.

#ThW: The COR would draw the Commission's attention to the dangers that
might arise from systematically relying on patents in the field of
intellectual property, since patent protection is not universal. This
applies mainly to the new technologies, and especially to information
technologies and the life sciences, which are the subject of a
detailed and heated debate.

#Iro: In the case of software, the debates that have taken place since the
1970s in the main countries concerned have all led to a copyright
system, although such a legal framework is not entirely suited to the
sector's specific requirements. The European Directive of 1 January
1993 has shown some wisdom in encouraging interoperability among
programmes so as to counteract the anti-competitive strategies of
seeking a dominant position. But for several years now, US case law
has been led into allowing the issuing of patents for software
%(q:components), a practice to which it had previously been hostile.
And the US is putting increasing pressure on Europe to allow software
patenting.

#TnW: The stakes here are extremely high. Such a practice would threaten the
progress of innovation in this industry, since it would lead to a
compartmentalisation of knowledge and procedures, thereby preventing
any interaction. The multitude of patents registered and granted in
the USA include a very large number of procedures, or even algorithms.
Many of them seem a long way from satisfying the criteria of novelty
and originality which, theoretically, are the basis for issuing a
patent.

#Icr: If the issuing of patents for software became institutionalised, it
would strengthen the dominant position of the biggest US market
leaders in the sector. It would be a direct threat to the huge number
of innovating smaller firms in Europe, the USA and in other countries.
Finally, it would be a very severe handicap for the European software
industry, which has a hard time remaining commercially competitive
despite its high level of competence.

#Pan: French Socialist Party

#PrW: Among the questions of the future of the european software industry,
the question of software patentability is of particular urgency, given
the level of progression of the european work in this area and the
policy of accomplished facts continuously pursued by the European
Patent Office.  Before installing a patent protection for software (as
the USA demand from us), the Socialist Party believes that it is
necessary to ascertain that it absense would really put the european
software publishers at a disadvantage.  The economic studies so far
rather lead to believe that software patents are an arm in the hands
of large enterprises which use them in order to block all inconvenient
innovation or to restrict the development of free software.  The
american experience with software patents is particularly negative and
merits reflexion.

#LWa: The patent system doesn't seem to match the rhythm of innovation in
the software sector:  the time required for obtaining a patent is too
long, as is the patent's validity term (20 years).  The innovations in
the software area are by their nature very close to mathematical
discoveries:  a new method of calculation, new algorithm or
application of a known mathematical technique to a specific problem. 
How and where can we draw the line of separation between software
processes and intellectual methods?  We should also not forget that in
our intellectual property system software already enjoys a significant
legal protection.

#POI: Propositions

#Loa: Therefore the negotiations at the European level require firm
positions: thus, as long as no favorable effects on innovation can be
seen, the socialists believe that:

#l1o: la brevetabilité du logiciel doit être refusée.

#loe: les dérives constatées dans le fonctionnement de l'Office européen des
brevets (OEB) combattues.

#Eeo: PS.BE 2003 Election Programme

#ltt: Software ... should continue to be protected by copyright rather than
by patents.

#fmn: former french minister of economy, research etc and presidential
candidate in 2002

#TWs: The extension of the scope of patentability to software, an economic
absurdity which would handicap our enterprises and stifle development
in this sector, must be denied.

#dih: député socialiste de Meurthe-et-Moselle

#Lio: Le système de brevet s'est étendu depuis quelques années bien au-delà
de son domaine de légitimité historique, économique et éthique. Cette
extension est le résultat de décisions de jurisprudence de l'Office
Européen des Brevets (OEB) qui sont parfois prises en contradiction
avec l'esprit de la loi, telle qu'elle a été ratifiée par le
législateur, et le plus souvent sans que les Etats signataires de la
convention de Munich ne disposent des moyens de contrôler la portée
économique et sociale de ces décisions. En particulier, je considère
qu'en affirmant qu'un « programme d'ordinateur présentant des effets
techniques » n'est pas « un programme d'ordinateur en tant que tel »
et peut donc faire l'objet d'un brevet, l'Office des Européen des
Brevets a clairement abusé de son pouvoir. L'OEB a en effet développé
une jurisprudence manifestement contraire à la convention
internationale qu'il est sensé appliquer, puisque tous les programmes
d'ordinateurs ont un effet technique, comme l'ont très justement
rappelé dès 1997 les experts européens en propriété industrielle
réunis lors de la table ronde sur la « brevetabilité des logiciels »
qui s'est tenue à Munich.

#Cts: Cette extension incontrôlée du système de brevet dans le domaine du
logiciel contribue à mettre en péril de façon croissante les
entreprises informatiques européennes, les auteurs de logiciels libres
et les principes fondamentaux qui ont permis l'essor de la société de
l'information. Plus de 10,000 brevets logiciels ont été déposés depuis
10 ans à l'Office Européen des Brevets par des astuces de procédure
cautionnées par l'OEB alors même que les guides distribués depuis 10
ans par les offices nationaux de brevets rappellent clairement que les
programmes d'ordinateur ne peuvent être brevetés. Plus de 75% de ces
brevets ont été déposés par des entreprises non-européennes. Nombre de
ces brevets logiciels portent sur des méthodes de commerce
électronique, voire des méthodes d'organisation des entreprises ou des
méthodes éducatives.

#Mev: Mais, comme il est rappelé dans les manuels de référence juridique
tels que le %{LI}, ces brevets n'ont de valeur que celle que l'on veut
bien leur accorder en raison de la contradiction manifeste qui existe
aujourd'hui entre le droit positif et le système jurisprudentiel de
l'OEB. En cas de contentieux, il n'est pas certain qu'un juge national
accepterait la validité de ces brevets en raison de leur objet,
manifestement contraires à l'esprit de la loi.  Les détenteurs de
brevets logiciels, de brevets sur le commerce électronique et de
brevet Internet n'attendent donc qu'une chose pour attaquer les
acteurs français et européens de la nouvelle économie: une révision de
la convention de Munich qui supprimerait l'exception sur les
programmes d'ordinateurs.

#AWi: Aussi, je vous serais reconnaissant de bien vouloir user dans les
consultations nationales, européennes ou mondiales à venir, de tous
les moyens qui sont en votre pouvoir pour exiger:

#dan: de ne pas modifier en novembre 2000 l'article 52 de la convention de
Munich, afin de ne pas activer le %(q:cheval de troie) qui sommeille
actuellement à l'OEB où de nombreux brevets Internet accordés
abusivement à des entreprises non-européennes peuvent menacer du jour
au lendemain la nouvelle économie française et européenne.

#qlW: http://www.osslaw.org/

#qnW: que les termes « technique », « application industrielle » et «
programme en tant que tel » soient clarifiés de façon à ce que toute
oeuvre, tout produit informationnel immatériel (y compris un logiciel
sur un support d'information) ne soit ni admis dans le champ de la
brevetabilité ni dans celui de la fourniture de moyen de contrefaçon
de brevet.

#qWo: que tout produit matériel, extension d'un produit informationnel
immatériel (ex. un lecteur MP3) puisse être breveté à condition que
soient satisfaits les critères de nouveauté, de technicité et
d'application industrielle de ce produit matériel, considéré
indépendamment des éléments logiciels qu'il exploite.

#qdi: que soit lancé dans les plus brefs délais un débat ouvert et
démocratique fondé sur des études scientifiques détaillées des effets
économiques et sociaux induits par une extension du système des
brevets à la société de l'information.

#qey: que soit mise en place une base de données de brevets complète,
librement accessible sous forme de contenu libre et de logiciels
libres, afin de donner aux PME les moyens de faire face aux risques de
contentieux de brevets en Europe et dans le monde.

#Aen: Aucune étude n'ayant été publiée par l'Office Européen des Brevets
pour justifier l'intérêt économique de l'extension au logiciel de la
brevetabilité, alors même que des économistes ont démontré que le
système de brevet pouvait aboutir à une diminution de l'innovation
dans l'économie du logiciel, il me semblerait également opportun de
commanditer un audit de l'Office Européen des Brevets afin de
déterminer les moyens de mieux contrôler les décisions de cet
organisme et de s'assurer qu'elles sont bien conformes à l'intérêt
général et au principe fondamental d'impartialité de la justice.

#Wse: Wirtschaftspolitische Sprecherin der Grünen Fraktion

#Vir: Gründer werden durch die Debatte über die mögliche Patentierbarkeit
von Software und Geschäftsideen auch in Europa verunsichert.  Hier
muss schnell Klarheit geschaffen werden.

#CWa: Computerprogramme %(q:als solche) sind nach dem Europäischen
Patent-Übereinkommen (EPÜ) nicht patentierbar. In den USA dagegen ist
grundsätzlich alles menschengemachte patentierbar.

#Bwd: Bei Software-Entwicklern und in der Open-Source-Szene besteht
erhebliche Verunsicherung, weil befürchtet wird, dass über die Novelle
des EPÜ und eine angekündigte Richtlinie der Europäischen Kommission
in Europa amerikanische Verhältnisse eingeführt werden sollen.

#IgA: In den USA wird der Wettbewerb bereits erheblich auch durch die
Patentierung von Geschäftsideen behindert. Viel zitiertes Beispiel ist
das Patent von Amazon auf das one-click-Verfahren bei der Bestellung
von Gütern im Internet.

#Asm: Allerdings ist bereits in den letzten Jahren in Europa Software
zunehmend als Bestandteil technischer Verfahren patentiert worden,
denn: Technische Verfahren, die Computerprogramme beinhalten, sind
patentierbar. Beispiel dafür ist eine computergesteuerte
Werkzeugmaschine, die insgesamt patentierbar ist. Es mangelt derzeit
an eingehenden ökonomischen Analysen, die die Wirkungen einer
möglichen Patentierung von Software beschreiben. Wir sehen allerdings
die Gefahr einer weiteren Verstärkung von Bürokratie mit dem Effekt,
dass dringend notwendige Innovationen behindert werden.

#DDf: Die Ermöglichung der Patentierung von Software würde darüber hinaus
erhebliche technische und administrative Probleme schaffen: in der
Zeit, die Anmeldung eines Patentes derzeit benötigt (derzeit 2 Jahre),
ist das Patent längst veraltet. Die Dokumentation der Patente wäre
extrem aufwendig. Kleine und mittlere Unternehmen würden durch die
Patentierung benachteiligt: Große Firmen, die über die Ressourcen
verfügen, die Patententwicklung zu verfolgen und Patente anzumelden
könnten auf diese Weise zusätzliche Erträge erwirtschaften. Der
Wettbewerb würde sich von der schnellen Umsetzung von Innovationen auf
juristische Streitereien verlagern und der technische Fortschritt
würde behindert werden. Das muss verhindert werden!

#MsW: MdB

#Vns: Vorsitzender des Parlamentarischen Unterausschusses für die Neuen
Medien

#Itm: In technologiepolitischen Fachkreisen hört man immer wieder die
Behauptung, das Patentsystem müsse auf gewisse Bereiche der
Informationstechnik ausgeweitet werden, weil sonst deren Investitionen
nicht genügend geschützt würden.  Diese Behauptung wurde bisher
allerdings immer nur als abstrakte Grundwahrheit weitergegeben und
niemals anhand von Tatsachen der deutschen oder europäischen
IT-Wirtschaft belegt.

#ScW: Selbst wenn es gelänge, Bereiche der Informationstechnik zu finden, in
denen Patente nachweislich vorteilhaft wirken oder gewirkt haben,
müsste man noch immer untersuchen, ob eventuelle schädliche
Nebenwirkungen der Patentierung diese Vorteile nicht überwiegen.

#Aiz: Aber während bei der Legislative noch vollkommene Unklarheit herrscht,
schreitet die Judikative bereits zur Tat, gewährt Tausende von
Softwarepatenten und drängt auf Änderung der Gesetzesregeln.  Es ist
daher höchste Zeit für uns als Gesetzgeber, uns um diese Fragen zu
kümmern.

#ReK: Rainder Brüderle

#WhP: Wirtschaftspolitischer Sprecher der FDP-Bundestagsfraktion

#DWs: Die Vorentscheidung zugunsten von Softwarepatenten des
Verwaltungsrates des Europäischen Patentamtes lässt aufhorchen. Hier
wurde auf Verwaltungsebene eine Schlüsselentscheidung für die
Schlüsseltechnologien des 21. Jahrhunderts vorbereitet. Das Thema
Softwarepatente ist allerdings zu wichtig für unsere wirtschaftliche
Zukunft, als dass es länger in den Hinterzimmern multinationaler
Gremien und den juristischen Spezialzirkeln geführt werden darf.

#Wmi: Wir sollten sehr kritisch prüfen, ob wir auf dem schnellebigen Gebiet
der Software einen exklusiven Patentschutz wollen.  Denn die
Patentierung von Software ist ein höchst zweischneidiges Unterfangen.
Dem Schutzinteresse Einzelner steht die Innovationsfähigkeit der
gesamten Branche gegenüber.  Es sollte aufhorchen lassen, dass der
zweitgrößte Softwarehersteller der Welt ORACLE sich aus diesem Grund
massiv gegen Softwarepatente ausspricht. Insbesondere die in Europa
starke und zukunftsträchtige Bewegung der freien Software (OpenSource
wie LINUX) wäre durch Softwarepatente in ihren Grundfesten gefährdet.

#Stn: Softwarepatente bergen die Gefahr in sich, daß die Großen der Branche
dank Finanz und Personalkraft kleine und mittelständische
Softwareschmieden mittels der Patentierung existenziell gefährden
werden.  Die Verfahren gegen Microsoft belegen aber deutlich, wie
wichtig und schwierig Wettbewerb schon heute im Softwarebusiness ist.
Die heftigen Debatten um Softwarepatente in den USA, aufgekommen durch
die Patente für amazon.com, sollten Europa eine Mahnung sein. Wir
sollten nicht um jeden Preis alles von Amerika übernehmen.

#EeM: Europa wäre gut beraten, bei den Softwarepatenten an die meist klein-
und mittelständische Struktur seiner Softwareindustrie zu denken. Es
wirft kein gutes Licht auf das Gewicht die Bundesregierung, dass sie
sich nicht im Verwaltungsrat gegen die Softwarepatentierung
durchsetzen konnte. Die Märkte von morgen sind die Märkte von Ideen.
Die Gedanken, auch die zu Software geronnenen, sollten aus liberaler
Sicht weitestgehend frei bleiben.

#Ftu: FDP Resolution 2001/05

#UdW: At their party convention in 2001/05 the German Liberal Democrats
passed a resolution on various questions of the Digital Economy,
deciding inter alia that only copyright and not patents should be
applicable to software.

#UWi: UK E-Minister Patricia Hewitt 2001-03-12

#ShU: Software genießt heute weltweit umfassenden Schutz durch das
Urheberrecht. Damit ist den Herstellern und Programmierern ein starkes
absolutes Recht verliehen, um ihre Interessen umfassend gegenüber
Dritten wahrnehmen zu können. Indes kennt die US-amerikanische
Rechtsordnung auch die Patentierbarkeit von Software. Im Gegensatz zum
Urheberrecht schützt das Patent jedoch nicht das fertige Produkt,
sondern dehnt den Schutz auf die Methode oder gar ein
softwarebasierendes Geschäftsmodell aus. Die Entwicklung in den USA
zeigt schon heute deutlich, dass die Patentierung von Software sich
negativ auf die Entwicklung neuer Produkte und Geschäftsmodelle
auswirken kann. Denn einzelne Softwarepatente können im Bereich der
sogenannten Individualsoftware ganze Märkte blockieren.

#SeW: Sowohl nach deutschem Patentrecht als auch nach dem Europäischen
Patentübereinkommen sind Computerprogramme als solche derzeit nicht
patentierbar. Die FDP spricht sich dafür aus, an dieser Rechtslage im
Grundsatz festzuhalten.

#bay: UK Liberal Democrats IT Policy Motion

#gWr: The party's IT policy was ratified by party conference on Sunday
2003/03/16. During the debate several speakers either spoke
specifically against software patents or mentioned the issue in
passing, as did the proposer of the ratifying motion (who was the
chair of the policy working group).  The paper was published together
with a motion that calls for:

#tsa: supporting continued widespread innovation by resisting the wider
application of patents in this area.

#Mia: Dr. Martin Mayer

#meW: Media politics speaker of the Christian Democrats

#Sde: Statt der beabsichtigten generellen Ausdehnung des Patentschutzes für
Software in Europa muss ein zweijähriges Moratorium beschlossen
werden.

#Amt: Auf der 'Diplomatischen Konferenz 2000' der Europäischen Patentämter
ist vorgesehen, 'Programme für Datenverarbeitungsanlagen' aus der
Ausnahmevorschrift Art. 52(2), europäisches Patentübereinkommen, zu
streichen, und somit generell die Patentierung von Software zu
ermöglichen.

#IWg: In der Fachwelt gibt es gegen diese Absicht die berechtigte
Befürchtung, dass durch den Revisionsvorschlag

#Mqs: Monopolstellungen großer Softwarehäuser gestärkt und erweitert,

#kdr: kleine Softwareunternehmen und selbstständige Programmierer in ihrer
Existenz bedroht und

#iiW: insgesamt der Fortschritt in der Softwareentwicklung deutlich gebremst
würden.

#Esj: Eine derart verheerende Entwicklung, die sich in den USA schon jetzt
abzeichnet, darf in Europa nicht stattfinden. Deshalb muss vor einer
weiteren Rechtssetzung für den Schutz von Software eine gründliche,
öffentliche Diskussion von Fachwelt und Politik auf der Basis der
folgenden Grundsätze geführt werden.

#ZcW: Ziele des Rechtsschutzes für Software:

#Dbs: Der Rechtsschutz muss Programmierer und Unternehmen in die Lage
versetzen, die Früchte ihrer Arbeit zu ernten. Die finanzielle
Entlohnung, die sich nur über den Rechtsschutz verwirklichen lässt,
ist der wichtigste Anreiz für den Fortschritt in der
Softwareprogrammierung und Anwendung.

#Dsr: Der Rechtsschutz darf aber nicht zur Stärkung von weltbeherrschenden
Monopolen führen. Er muss den Wettbewerb fördern statt ihn zu
behindern.  Vor allem darf er keinesfalls kleine Softwareunternehmen
und selbstständige Programmierer benachteiligen und in ihrer Existenz
bedrohen.

#Mtt: Maßgeschneiderter Rechtsschutz für Software

#DWS: Das Urheberrecht wurde zum Schutz von künstlerischen und
schriftstellerischen Werken geschaffen. Es schützt auch
Computerprogramme in ihrer Eigenschaft als Sprachwerke. Allerdings
schützt das Urheberrecht die Software nur unzulänglich.

#Dse: Die Patente wurden im beginnenden Industriezeitalter zum Schutz
technischer Erfindungen eingeführt. In Ausgestaltung und Zeitdauer
tragen sie den Erfordernissen der Wissensgesellschaft nur unzureichend
Rechnung.

#Sln: Software ist im Vergleich zu schriftstellerischen und künstlerischen
Werken und zu technischen Erfindungen etwas völlig Neues und
Andersartiges. Sie ist das elementare Hilfsmittel in der
Informationsgesellschaft und dringt in immer neue Bereiche vor. Daher
muss für sie ein eigenes, maßgeschneidertes Instrument des
Rechtsschutzes geschaffen werden.

#EsJ: YEPP/DEMYC 2000-10-28

#Rae: http://www.demyc.org

#Nct: Noting  that European Patent Office is planning to remove computer
programs from the list of the things which can not be patented of  in
its next conference in end of November

#Fnb: Further noting the still existing great amount of unsolved problems in
software patents like:

#Asr: Abstract-logical nature of software, which is in conflict with the
patent system's requirement for concreteness and physical substance

#Ige: Inefficient  long period of protection (20 years)

#Ttb: The lack of IT-expertise among the patent inspectors which would lead
too wide patent claims to be accepted as has happened in the United
States

#Top: The problem of anti-competitiveness; US software patents, if easily
transferable to EU, could substantially narrow competition in the
European Union, give dominant position to US corporations and have
negative influence among european IT start-ups

#Ttl: The negative effect of patents to the interoperability of computer
programs

#Toa2: Taking into consideration the strong opposition among small and medium
size companies towards software patents in Europe

#HcW: Having in mind the enormous economic importance of computer software
industry and its need for effective protection for its investments

#Yit: YEPP calls upon European political parties and the European Commission
(DG XV) to take immediate actions to delay all development on software
patents until the problems are correctly addressed

#Yee: YEPP further calls for taking actions to strengthen the European
software industry by improving the regulatory framework on combating
software piracy

#Bgo: MdB Wolfgang Wodarg

#Wna: Wodarg hat die %(ep:Eurolinux-Petition) wohl nicht zuletzt deshalb
unterzeichnet, weil er auf seinem Gebiet mit den Entgrenzungs-Taktiken
der Patentgesetzgeber aufs genaueste vertraut ist.  Im %(q:Deutschen
Ärzteblatt) vom Juli 2000 seziert er unter der Überschrift
``Schwammige Definitionen, moralische Lyrik'' die EU-Richtlinie
``Rechtlicher Schutz biotechnologischer Erfindungen''.  Darin schreibt
er u.a.:

#DWn: Der Text der EU-Richtlinie enthält vor allem im Vorspann und in den
Erwägungsgründen viel moralische Lyrik, mit der Kritiker
abgeschmettert und Parlamentariergewissen beschwichtigt werden können.
 Klare Grenzen und Definitionen fehlen.  Schwammige oder in sich
widersprüchliche Bestimmungen sorgen dafür, dass Rechtsnormen, wie sie
das 1977 in Kraft getretene Europäische Patentübereinkommen vorgibt,
systematisch ausgehöhlt werden können -- zum Beispiel das zentrale
Prinzip der ärztlichen Therapiefreiheit.  Nach Artikel 52,4 des
Übereinkommens dürfen diagnostische, therapeutische und chirurgische
Verfahren am menschlichen und tierischen Koerper nicht patentiert
werden.  Diesen Grundsatz beizubehalten, verspricht die Richtlinie in
Erwägungsgrund 35, jedenfalls im Hinblick auf die Verfahren als
ganzes.  Teilschritte von Verfahren können jedoch sehr wohl patentiert
werden.  Nach dem Motto: Keine Tür darf dem Arzt zum Wohle der
Patienten verschlossen sein, über die Nutzung der Türgriffe
entscheidet aber der Patentinhaber.

#Ele: Eine ähnliche juristische Spitzfindigkeit findet sich in dem für die
EU-Richtlinie zentralen Artikel 5.  Im ersten Abschnitt ist
festgelegt, dass der menschliche Körper in allen Entwicklungsstadien
nicht patentierbar ist, ebenso wenig wie die bloße Entdeckung eines
seiner Bestandteile (Beispiel Gene).  Im zweiten Abschnitt heisst es
jedoch, dass Teile, die mit Hilfe eines technischen Verfahrens
isoliert wurden, sehr wohl patentierbar sind.  Wer sich mit der
Regelung näher befasst, wie der Nationale Ethikrat der Dänen oder
Kritiker in Frankreich, kommt nicht umhin festzustellen, dass bei
Genen und Teilsequenzen von Genen die vermeintliche Ausnahme die Regel
ist.  Gene liegen immer isoliert vor, wenn sie entschlüsselt werden.

#Hes: Herta Däubler-Gmelin, Bundesministerin der Justiz

#Mkg: Mit der Vergabe der 20-jährigen Monopole auf die Nutzung von
Software-Ideen, so Däubler-Gmelin, seien erhebliche Probleme für die
Ökonomie und die Sicherheit der Informationsgesellschaft verbunden,
die ``erst einmal gründlich und breit diskutiert werden müssen''.

#MuW: Man könne schließlich eine solch zentrale Frage der Wirtschaftspolitik
%(q:nicht von Malta und Liechtenstein bestimmen lassen), erklärt ein
Ministerialer. Notfalls könne man auch das Abkommen insgesamt platzen
lassen und innerhalb der EU eine eigene Patentpolitik entwickeln.

#Jfk: Justizministerin Prof. Dr. Herta Däubler-Gmelin im März 2001

#DWW: /swpat/analysis/epc52/index.en.html

#Vtu: Vehement hat sich Bundesjustizministerin Herta Däubler-Gmelin auf der
Hannover Messe gegen Vorschläge der EU ausgesprochen, die
Patentierbarkeit von Software generell zu erlauben. Mitte dieses
Monats will EU-Kommissar Frits Bolkenstein der europäischen Kommission
eine neue Richtlinie zur Patentierung von Computerprogrammen vorlegen.
In dieser Richtlinie werden sämtliche Computerprogramme als
patentierbar erklärt. Justizministerin Herta Däubler-Gmelin spricht
sich gegenüber der Computer Zeitung deutlich gegen diesen Vorschlag
aus und fordert die Beibehaltung der jetzigen Regelung, die
Softwarepatente nur als Bestandteil eines technischen Verfahrens
erlaubt.

#lWh: Ms Hewitt may not have thought through the idea of patenting machine
tool software and left a window for confusion open here, but her basic
policy goal is clearly outlined:  If anything about software can be
patented, that must be something related to advanced machinery and
physical phenomena.  There should be something new in the hardware. 
Software ideas for known general-purpose computers, especially those
related to social phenomena such as language and business, must in any
case be unpatentable.

#OWn: Our key principle is that patents should be for technological
innovations. So a program for a new machine tool should be patentable
but a non-technological innovation, such as grammar-checking software
for a word-processor, should not be.

#SWa: V. Strategic Considerations

#WWl2: Recently my firm had to explore the patent situation world-wide for
one the major banks. All existing U.S. patents and all published
European patent documents have been searched regarding any potential
risk for the banking business. The search was performed on the basis
of a profile combining both the leading banks, service providers in
that field and also key-terms relevant in the field of banking. The
search revealed more than 5000 patent documents which could possibly
have an impact on the activities of the bank.  The next step will be
to evaluate those patents regarding scope of protection and the
possible relevancy. The task being a list of patents which might be
infringed by the bank's business. Possible counter-measures are
envisaged in a next step (oppositions, negotiations etc.).

#tle: It is assumed that the evaluation of all patents will need two or
three patent experts, combined with two or three IT-experts from the
bank itself and a time period of three to six months. This is the
%(q:defensive) measure. As an %(q:offensive) measure it is advisable
that banks start filing patent applications wherever possible and
promising. Of course, the liberal practice in the U.S. requires to
file almost any business method for patent. These business methods are
always implemented by a computer (I have not seen any other example).
This computer implementation in most cases gives the opportunity to
look for some sort of a %(q:technical effect) or %(q:technical
consideration) or %(q:technical problem) etc. which might help to
obtain a European patent, in the long run. When studying a business
method computer software program it turns out that in most cases that
program includes at least one aspect (feature) which might qualify
under the European standards as %(q:technical). It is an advisable
measure for any bank these days to try to develop a patent portfolio
in order to have at least some arguments in hand when approached by
others for patent infringement.

#Aon: A regular report on patent strategies of various corporations quotes
him explaining why AT&T has been filing 7 times as many patents in
1999 as in 1997.

#WuW: We want to build picket fences around the technologies that we think
are most important for the future.

#Toe: Think Magazine 1990 #5 contains an article which explains IBM's patent
strategy by quoting Smith.

#Yde: You get value from patents in two ways: through fees, and through
licensing negotiations that give IBM access to other patents. The IBM
patent portfolio gains us the freedom to do what we need to do through
cross-licensing--it gives us access to the inventions of others that
are the key to rapid innovation. Access is far more valuable to IBM
than the fees it receives from its 9,000 active patents. There's no
direct calculation of this value, but it's many times larger than the
fee income, perhaps an order of magnitude larger.

#JnW: Josef Straus: Patents as a Weapon of Developped Nations

#Pei: Prof. Straus, member on advisory committees of WIPO, AIPPI, EPO and
patent counsellor of the German government, says that (1) without
patents there is no incentive to innovate (2) the main purpose of
patents today is to keep developping countries at distance.  Straus
probably somewhat overestimates the effect of patents.

#Dgn: Da im System des freien Welthandels Produkte vielerorts billiger
hergestellt werden können als in den hochentwickelten Ländern, hängt
der wirtschaftliche Erfolg europäischer Unternehmen in zunehmendem
Maße davon ab, ob es ihnen gelingt, durch Innovationen
Wettbewerbsvorteile zu erlangen. Dieser Anreiz zu Innovationen kann
jedoch nur bestehen, wenn ein rechtlicher Schutz der Erfindungen
sichergestellt ist. Sind die Erfindungen rechtlich geschützt, können
sie als %(q:intellectual property) einen Hauptteil des
Unternehmensvermögens darstellen. Der Bedarf an Juristen,
Naturwissenschaftlern und Ingenieuren, die Kenntnisse im Patentrecht
haben, steigt dieser Entwicklung entsprechend.

#Mee: Microsoft to opensourcers 2001: Get your money and let's go to court

#Aoe: News report about a public discussion at the %(q:Open Source
Convention) between Microsoft executive Craig Mundie and well known
representatives of the free software community in July 2001. 
%(bc:Asked by CollabNet CTO Brian Behlendorf whether Microsoft will
enforce its patents against open source projects, Mundie replied,
%(q:Yes, absolutely.) An audience member pointed out that many open
source projects aren't funded and so can't afford legal representation
to rival Microsoft's. %(q:Oh well,) said Mundie. %(q:Get your money,
and let's go to court.))  Similar statements have been made by other
Microsoft officials, such as Steve Ballmer at the CeBit trade fair in
2002.

#tai: swpatcusku.en.txl

#sfq: send us further quotations

#Pae2: Please show this list of quotations to people who should be on it with
their own quote!

#BWg: Bitte bitten Sie SAP oder das Büro Tauss, die verschwundene Erklärung
von Prof. Plattner nachzuliefern!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatcusku.el ;
# mailto: phm@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatcusku ;
# txtlang: xx ;
# End: ;

