<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Citações sobre Patentes de Software

#descr: Citações importantes de textos legais, análises económicas, documentos
políticos e também citações de programadores, de políticos e de outras
entidades e empresas interessadas no debate sobre as patentes de
software.

#aii: A política de Tecnologias de Informação do partido, que inclui um
parágrafo de oposição às patentes de software, foi ratificada na
conferência de Domingo, 16 de Março de 2003. Durante o debate vários
oradores falaram específicamente contra as patentes de software ou
mencionaram o assunto de passagem, como fez o proponente da moção
ratificada (que era o presidente do grupo de trabalho sobre a
política). A declaração foi publicada em conjunto com uma moção que
pede %(q:o apoio à continuação da inovação generalizada através da
oposição ao alargamento da patenteabilidade nesta área.)

#ExW

#Con: O capítulo 6 argumenta que os países em desenvolvimento devem ter o
cuidado de não seguir os EUA ou a Comissão Europeia no enquadramento
da política de patentes respeitante ao software, à genética, et al.
Devem adoptar uma aproximação semelhante ao Artigo 52 da Convenção
Europeia de

#Pos: Phil Karn, um programador informático que trabalha para a Qualcomm,
uma empresa que vive largamente dos rendimentos das licenças de
patentes, diz que o sistema de patentes, além de trazer bastantes
proveitos às empresas como a Qualcomm, está a fazer muito pouco bem à
indústria de software como um todo.

#AiW: Uma recolha de declarações de numerosos políticos Europeus, gerentes
de empresas da área das Tecnologias de Informação e de intelectuais em
apoio à posição da Eurolinux de não permitir as Patentes de Sofware.

#SCS: O fundador da Lotus explica, numa audiência com o USPTO em 1994,
porquê as Patentes de Software são más para a indústria de software.

#Dyh: Douglas Brotz, principal investigador da Adobe Systems, explica nas
audiências do USPTO em 1994, porquê as patentes de software são más
para a indústria de software.

#Ich: A Autodesk (empresa autora do software AUTOCAD) é um líder mundial,
alguns dirão monopolista, em software de CAD. Warren é reconhecido
como um pioneiro de negócios e de software, editor fundador do
legendário %(q:Dr. Dobb's Journal) e membro da direcção da Autodesk.
Num testemunho profundo e apaixonado nas audiências do USPTO em 1994,
Warren explica, em primeiro lugar como os algorítmos são distintos dos
fenómenos naturais e como a tentativa de os monopolizar viola direitos
constitucionais fundamentais.

#Ria: Robert Kohn, chefe de desenvolvimento e membro da direcção da Borland
Corporation explica na audiência do USPTO em 1994 que o que sua
empresa realmente necessita não é de direitos de Propriedade
Intelectual mais vastos mas sim que os Direitos de Autor sejam
garantidos.

#SWl: Stephan Kinsella sobre o tema da Propriedade Intelectual

#AWi2: Um advogado de patentes critica o sistema de patentes e as atitudes
pretenciosas dos advogados de patentes

#Ihe: Na sua intervenção perante uma vasta plateia na Convenção do Software
Livre, Lawrence Lessig desafia a audiência a envolver-se mais no
processo político. No seu discurso cita comentários interessantes de
Bill Gates e de outras pessoas sobre patentes de software.

#BWn

#ohW

#LWw: LPF Irlam: Citações sobre Patentes de Software

#Cih: Excertos de estudos sobre os efeitos do sistema de patentes no
software e na economia em geral. Mostra que o sistema está há muito
tempo gravemente danificado em várias áreas. Recolhido por Gordon
Irlam e publicado pela League for Programming Freedom (LPF).

#CWn: Irlam 1994: Citações sobre Patentes de Software

#VrF

#Lne

#Coe

#Sei

#Rej

#Wfk

#Msr

#Pik

#Ptt

#Mte

#AWf: Gary Reback, um famoso advogado de software americano, narra as suas
memórias

#Mne: A minha própria introdução às realidades das patentes de software
aconteceu nos anos 80 quando a minha cliente Sun Microsystems -- na
altura uma pequena empresa -- foi acusada pela IBM de infringir
patentes. Ameaçando com um processo legal massivo, a IBM pediu uma
reunião para apresentar as suas exigências. Quatorze advogados da IBM
e respectivos assistentes, todos vestidos no obrigatório fato azul
escuro, amontoaram-se na sala de conferências maior de que a Sun
dispunha.

#Tri: O fato-azul chefe orquestrou a apresentação de sete patentes que a IBM
reclamava terem sido infringidas, a mais notória das quais era patente
das %(q:linhas gordas) da IBM: Num ecran de computador, para
transformar uma linha fina numa linha grossa sobe-se e desce-se uma
distância igual a partir das extremidades da linha fina e então
liga-se os quatro pontos. Provávelmente todos aprendemos esta técnica
para transformar uma linha num rectângulo em geometria do sétimo ano e
acreditamos que a mesma foi proposta por Euclides ou outro pensador há
qualquer coisa como 3000 anos. Segundo os examinadores do USPTO, que
concederam a patente do processo a IBM, não foi assim.

#AsW: Após a apresentação da IBM chegou a nossa vez. Sob o olhar da equipa
do Gigante Azul (que não mostrava uma pinga de emoção), os meus
colegas -- todos os quais eram formados simultaneamente em Engenharia
e em Direito -- aproximaram-se do quadro branco de marcadores em punho
e metódicamente dissecaram e demoliram as reivindicações da IBM.
Usamos frases como %(q:Devem estar a brincar,) e %(q:Deveriam ter
vergonha.) A equipa da IBM não mostrou qualquer emoção mas uma pura
indiferença. Entre nós chegámos a uma conclusão: Apenas uma das sete
patentes da IBM poderia ser declarada válida por um tribunal e nenhum
tribunal racional chegaria à conclusão de que a tecnologia da Sun
infringia sequer essa patente.

#AnO: Seguiu-se um silêncio incómodo. Os fatos-azuis nem sequer falaram
entre si. Permaneceram sentados como se fossem de pedra. Finalmente o
fato chefe replicou. Disse: %(q:OK, talvez vocês não infrinjam nenhuma
destas sete patentes mas nós temos 10.000. Querem realmente que
voltemos para Armonk [a sede da IBM em Nova Iorque] e encontremos sete
patentes que vocês infrinjam? Ou querem tornar as coisas mais fáceis e
pagarem-nos somente 20 milhões de dólares?) Após alguma negociação a
Sun passou um cheque à IBM e os fatos-azuis passaram à empresa
seguinte da sua lista de alvos.

#IWt: Na América corporativa este tipo de ataque repete-se semanalmente. A
patente como estímulo à invenção há muito tempo que deu lugar à
patente como puro instrumento de estrangulamento da inovação.

#HSi: Introdução Rápida

#AWa: Para ajudar os leitores a entender a razão porque a directiva de
patentes de software da Comissão Europeia é tão controversa comecemos
por analisar um cenário simples.

#ItW: Imagine que é proprietário de uma pequena empresa de software. Imagine
que escreveu um software potente. Esse software é uma combinação
criativa de 1000 regras abstractas (algoritmos) e de uma grande
quantidade de dados. Cada regra levou entre alguns minutos a algumas
horas a [re]inventar, enquanto que o desenvolvimento e depuração de
todo este trabalho levou 20 anos-homem. Das 1000 regras, 900 eram já
conhecidas há mais de 20 anos. 50 das regras estão agora cobertas por
patentes. A sua empresa é dona de 3 dessas patentes, para as obter
teve que correr até ao gabinete de patentes, revelar as suas
estratégias de negócio e pagar os custos dos advogados. A IBM e a
Microsoft estão já entretanto a converter as suas ideias patenteadas
em lucro. Vai querer pará-las? As equipas de advogados delas dizem que
você infringe 20 a 30 das suas 50000 patentes. Então chegam a um
acordo de cavalheiros: 3% da sua facturação anual vai para a IBM, 2%
para a Microsoft, 2% %(dots). Mesmo assim, um dia a empresa torna-se
lucrativa. É agora dono de uma empresa atractiva. Uma agência de
patentes aborda-o. Está a infringir duas ou três das suas patentes,
dizem eles. Querem 100.000 Euros. Um processo em tribunal pode levar
anos e custar 1 milhão de Euros. Você paga. Um mês mais tarde, a
agência de patentes seguinte bate à porta %(dots). Em pouco tempo
ficaria falido. Procura protecção de uma grande empresa. A Microsoft
oferece-se para lhe comprar a empresa por um valor simbólico. Você
aceita. Sob um sistema de direitos de autor você teria ficado
independente e rico. Por meio das  patentes a Microsoft e outros
conseguiram roubar a sua propriedade intelectual.

#SoW: O software é sob vários aspectos semelhante às outras áreas da
engenharia. Mas há uma diferença fundamental: um programa de
computador é construido a partir de objectos matemáticos ideais. Um
programa faz sempre exactamente o que diz fazer. Podemos construir um
castelo no ar seguro por uma linha de espessura zero, e o castelo
manter-se-á no ar.

#Ptw: A maquinaria física não é tão predizível porque os objectos físicos
são temperamentais. Se num programa está escrito que deve contar entre
um e mil, o programa fará exactamente isso. Se construirmos o contador
com maquinaria, uma correia pode derrapar e contar duas vezes o número
58, ou uma engrenagem pode falhar e o número 572 pode ser passado à
frente. Estes problemas tornam muito difícil o projecto de maquinaria
fiável.

#Wst

#Tsa

#Pnt: As pessoas perguntam-me ingénuamente: %(q:Se o teu programa é inovador
então não terás obviamente a patente?) Esta pergunta parte do
princípio de que a um produto corresponderá uma patente.

#Iwr: Em algumas áreas, tal como a indústria farmaceutica, as patentes
realmente funcionam dessa forma. O software está no extremo oposto>
uma patente típica cobre inúmeros programas distintos e mesmo um
programa completamente inovador infringirá provávelmente inúmeras
patentes.

#Teo: Isto é assim porque um programa substancial tem que combinar um grande
número de técnicas diferentes e implementar muitas características.
mesmo se algumas sejam invenções novas, ainda restarão muitas que não
o são. Qualquer técnica ou característica com menos de duas décadas é
provável que esteja já patenteada por outro. Se está mesmo ou não é
uma questão de sorte.

#InW2

#PtW

#Pee

#Tdr

#WWt

#Adu: Uma década atrás a área do software funcionava sem patentes. Sem
patentes foram produzidas inovações tais como os sistemas de janelas,
a realidade virtual, as folhas de cálculo e as redes. E por causa da
ausência das patentes, os programadores poderão desenvolver software
que usasse estas inovações.

#Whc: Não pedimos que esta situação mudasse, a mudança foi-nos imposta. Não
há qualquer dúvida de que as patentes de software nos deixam
amarrados. Se não há nenhum interesse público claro e vital em nos
deixar amarrados em burocracia, libertem-nos e deixem-nos voltar ao
trabalho.

#crr

#MoW

#Lao

#L1e

#Pae

#LWt

#Lsc

#L0j

#Mti

#Tne

#Mea

#alcatel02

#erW

#eWa

#uao

#pde

#B0f

#BoW

#Aod

#IWW3

#GWr

#Ret

#AWe

#InW

#Wun

#TnW2

#Trl

#Tse

#DnA

#S9n

#LWo

#Fhn

#Oed

#Jpo

#Wfe

#Ikn

#Tls

#Iid

#OWr

#toW

#OWW

#Plm

#UWs

#Auu

#TWW

#Tam

#Wnw

#EWl

#TWp

#TWn

#TkW

#Ara

#Beb

#Wso

#Foe

#Sdt

#RtW

#Leh

#Mft

#Bhb

#Tor

#IiW

#BEf

#T9o

#IWe

#rGW

#aea

#HmO

#Tlu

#Ofr

#LoW

#Lhm

#Ins

#Thy

#TFa

#TFa2

#Woy

#dat

#eml

#atc

#raf

#oWt

#edw

#Mnt

#Lkm

#SuW

#Eee

#Soi

#Bro

#Sai

#Eii

#Dnu

#Ene

#Beh

#Dzg

#Wdg

#GKW

#GdW

#Wmr

#EPf

#Ilt

#Aso

#IWn

#RLt

#Tmr

#1en

#SpW

#Das

#Dnn

#ArW

#Ien

#Aeo

#2nt

#Eht

#3iz

#Tkt

#Kee

#Pio

#DWx

#soa

#biw

#oae

#oiu

#Tgi

#SbW2

#AeW

#Has

#JKl

#Aig

#PaW

#Qne

#WiW

#Kee2

#Shf

#IVl

#hrP

#ahj

#tei

#rsm

#rfa

#bpatg17

#eei

#2nW

#EWW

#edp

#Wer

#bes

#0pW

#toe

#0Ea

#Wmh

#goi

#uno

#TWe

#DWr

#Edo

#Snt

#Eim

#Rra

#AWh

#Vnz

#WaW

#SSa

#Jee

#Tel

#Tno

#Ene2

#Tsn

#SbW

#AlW

#CWr

#Ieh

#Ois

#FMp

#Toa

#BRw

#EnW2

#Fsl

#Upy

#Gdr

#Fvp

#PWr

#Soe

#SuW2

#Ifp

#MiW

#VWu

#IWp

#MlW2

#PRm

#Iyd

#LWi

#HAE

#lhh

#FrH

#IWW

#Iho

#All

#Tce

#Ect

#LoM

#Iss

#Jov

#JGb

#IjW

#tWE

#pat

#Eoe

#Aot

#AyE

#Ohf

#IWW2

#ThW

#Iro

#TnW

#Icr

#Pan

#PrW

#LWa

#POI

#Loa

#l1o

#loe

#Eeo

#ltt

#fmn

#TWs

#dih

#Lio

#Cts

#Mev

#AWi

#dan

#qlW

#qnW

#qWo

#qdi

#qey

#Aen

#Wse

#Vir

#CWa

#Bwd

#IgA

#Asm

#DDf

#MsW

#Vns

#Itm

#ScW

#Aiz

#ReK

#WhP

#DWs

#Wmi

#Stn

#EeM

#Ftu

#UdW

#UWi

#ShU

#SeW

#bay

#gWr

#tsa

#Mia

#meW

#Sde

#Amt

#IWg

#Mqs

#kdr

#iiW

#Esj

#ZcW

#Dbs

#Dsr

#Mtt

#DWS

#Dse

#Sln

#EsJ

#Rae

#Nct

#Fnb

#Asr

#Ige

#Ttb

#Top

#Ttl

#Toa2

#HcW

#Yit

#Yee

#Bgo

#Wna

#DWn

#Ele

#Hes: Herta Däubler-Gmelin, Bundesministerin der Justiz

#Mkg: Mit der Vergabe der 20-jährigen Monopole auf die Nutzung von
Software-Ideen, so Däubler-Gmelin, seien erhebliche Probleme für die
Ökonomie und die Sicherheit der Informationsgesellschaft verbunden,
die ``erst einmal gründlich und breit diskutiert werden müssen''.

#MuW: Man könne schließlich eine solch zentrale Frage der Wirtschaftspolitik
%(q:nicht von Malta und Liechtenstein bestimmen lassen), erklärt ein
Ministerialer. Notfalls könne man auch das Abkommen insgesamt platzen
lassen und innerhalb der EU eine eigene Patentpolitik entwickeln.

#Jfk

#DWW

#Vtu

#lWh

#OWn

#SWa

#WWl2

#tle

#Aon

#WuW

#Toe

#Yde

#JnW

#Pei

#Dgn

#Mee

#Aoe

#tai

#sfq

#Pae2

#BWg

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatcusku.el ;
# mailto: ml1jdc@x64.com ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatcusku ;
# txtlang: xx ;
# End: ;

