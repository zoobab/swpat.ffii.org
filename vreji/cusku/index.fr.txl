<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

##? INSERTION phm 2003-11-12: please translate this
#title: Citations sur Brevets Logiciels

##? MODIFICATION phm 2003-11-12: Pay attention to the wording
#descr: Citations de textes juridiques, analyses économiques, documents
politiques ainsi qu'énoncés de programmeurs, politiciens et autres
parties qui s'intéressent au débat sur les limites de la brevetabilité
vis-a-vis du logiciel.

#aii: La politique du groupe sur les Technologies de l'Information (IT) qui
inclut un paragraphe s'opposant au brevetage logiciel, a été approuvée
lors d'un congrès du groupe le dimanche 16/03/2003. Pendant le débat,
plusieurs orateurs ont parlé spécifiquement contre les brevets
logiciels ou ont mentionné le problème de leurs adoptions, comme l'a
fait l'auteur de la motion approuvée (qui était le président du groupe
de travail).  LE papier a été publié ainsi qu'une motion qui appelle à
%(q:soutenir l'innovation répandue et continue en résistant à une plus
large application des brevets dans ce domaine.)

#ExW: Europarechtler Lenz exzerpiert und kommentiert Texte aus aktuellen
Studien und Anhörungen, die für die Gegner von Softwarepatenten von
Interesse sind

#Con: Le chapitre 6 soutient que les pays en voie de développement seraient
prudents de ne pas suivre les États-Unis ou la Commission Européenne
en construisant leur politique de brevets en considérant le logiciel,
la génétique et autres.  Ils devraient adopter une approche similaire
à celle de l'article 52 EPC: en excluant explicitement le logiciel,
les méthodes d'entreprise et tout ce qui y ressemble de la
brevetabilité.  Ceci, comme le soulignent les auteurs, est
parfaitement compatible avec l'article 27 TRIPs. Les auteurs sont des
universitaires britanniques, leur travail a été commandé par le
gouvernement britannique.

#Pos: Phil Karn, un développeur travaillant pour Qualcomm, une compagnie qui
vit largement des licences de brevets, trouve que le système de
brevets, en dehors d'apporter des revenus aux compagnies telles que
Qualcomm, n'apporte que peu de bien pour l'industrie logicielle dans
son ensemble.

#AiW: Un recueil de déclarations de nombreux politiques européens, de
dirigeants de compagnies de technologies de l'information (IT) et
d'intellectuels soutiennent la position de Eurolinux de ne pas
autoriser les brevets logiciels.

#SCS: Un représentant de Oracle Corporation explique dans une audition en
1994 pourquoi les brevets sont néfastes pour le développement du
logiciel et ce qu'il faudrait changer dans le système des brevets
s'ils étaient conservés malgré tout.

#Dyh: Douglas Brotz, scientifique principal chez Adobe Systems, explique
lors d'auditions à l'Office des Brevets Américain en 1994 pourquoi les
brevets sont néfastes pour l'industrie du logiciel.

#Ich: Jim Warren, membre du conseil chez Autodesk, est connu en tant que
pionnier dans le logiciel et dans les affaires et en fondant l'éditeur
du Dobb's Journal.  Dans un témoignage lors d'auditions à l'Office des
Brevets Américain pour le compte de Autodesk Inc en 1994, Warren
explique d'abord pourquoi les règles de calcul sont différentes des
phénomènes matériels et pourquoi la tentative de les monopoliser
violent des valeurs constitutionnelles fondamentales.

#Ria: Robert Kohn, développeur en chef et membre du conseil chez Borland
Corporation explique lors d'une audition de l'Office des Brevets
Américain en 1994 que ce dont sa compagnie à vraiment besoin ne sont
pas des droits de propriété intellectuelle plus étendus mais d'un
renforcement du copyright.

#SWl: Stephan Kinsella sur la %(q:Propriété Intellectuelle)

#AWi2: Un avocat en brevet critique le système de brevets et les attitudes
prétentieuses de ses confrères conseils en propriété intellectuelle.

#Ihe: Dans son discours devant une salle comble à la Convention Open Source,
Lawrence Lessig exhorte l'auditoire à être plus impliqué dans le
processus politique.  Dans son discours, il cite des commentaires
intéressants de Bill Gates et d'autres personnes sur les brevets
logiciels.

#BWn

#ohW

#LWw: Ligue pour la Liberté de Programmation, Irlam: Déclarations sur les 
Brevets Logiciels

#Cih: Extraits d'études et autres documents concernant les effets du 
système de brevets sur le logiciel et l'économie en général.  Montre
le  dysfonctionnement du système dans beaucoup de domaines sur une
longue  durée.  Propos recueillis par Gordon Irlam et publiés par la
Ligue pour  la Liberté de Programmation.

#CWn: Irlam 1994: Déclarations sur les Brevets Logiciels

#VrF: Une variante du recueil de documents de la ligue pour la Liberté de 
Programmation, envoyé par Gordon Irlam à une liste de diffusion.

#Lne: Lang: Déclarations sur les Brevets Logiciels

#Coe: Déclarations sur la %(q:Propriété Intellectuelle) et l'espace  public
informationel, recueilli par Bernard Lang.

#Sei: Informaticiens et Techniciens

#Rej: Experts en Droit, Juges

#Wfk: Économistes

#Msr: Mathématiciens, Savants, Intellectuels

#Pik: Politiciens

#Ptt: Stratèges en Brevet

#Mte: Mon introduction aux Réalités du Brevet

#AWf: Gary Reback, un avocat en brevets américain bien connu raconte
quelques uns de ses mémoires

#Mne: Mon premier contact avec les réalités du système des brevets fut dans
les années 1980, quand mon client, Sun Microsystems--qui était alors
une petite compagnie--fut accusée par IBM de violation de brevet.
Menaçant d'un procès important, IBM a demandé une réunion pour
présenter ses revendications. Quatorze avocats d'IBM et leurs
assistants, tous revêtus du costume bleu marine réglementaire,
s'entassèrent dans la plus grande salle de conférences dont disposait
Sun.

#Tri: Le chef des costumes bleus orchestra la présentation des sept brevets
IBM revendiqués qui étaient enfreints, dont le plus important était le
notoire brevet %(q:lignes épaisses) d'IBM: Pour transformer une ligne
fine sur un écran d'ordinateur en une ligne large, vous allez à une
distance égale au-dessus et au-dessous des extrémités de la ligne fine
et connectez alors les quatre points. Vous avez sûrement appris cette
technique de transformation d'une ligne en rectangle en cours de
géométrie en classe de 5ème, et, sans doute, vous croyez que cela a
été imaginé par Euclide ou quelque penseur vieux de 3000 ans. Pas
selon les examinateurs de l'Office des Brevets Américain, qui a
accordé à IBM un brevet sur ce procédé.

#AsW: Après la présentation d'IBM, notre tour vint. Alors que l'équipe de
Big Blue regardait (sans une once d'émotion) mes collègues--tous
licenciés à la fois en droit et en ingénierie--occupés sur le tableau
blanc avec des marqueurs, illustrant méthodiquement, disséquant, et
démolissant les revendications d'IBM. Nous avons utilisé des phrases
comme: %(q:Vous devez plaisanter,) et %(q:Vous devriez avoir honte.)
Mais l'équipe d'IBM ne montra aucune émotion, en gardant une
indifférence totale. En confiance, nous assénions notre conclusion:
Seul un des sept brevets d'IBM serait recevable par un tribunal, et
aucun tribunal rationnel ne trouverait que la technologie de Sun
enfreignait même celui-là.

#AnO: Un silence embarrassé s'ensuivit. Les costumes bleus ne se
concertèrent même pas. Ils restèrent juste assis là, comme des
statues. Finalement, le chef des costumes bleus répondit. %(q:OK,)
dit-il, %(q:peut-être que vous n'enfreignez pas ces sept brevets. Mais
nous avons dix mille brevets américains. Voulez-vous vraiment que nous
retournions à Armonk [le quartier général de IBM à New York] et que
nous trouvions sept brevets que vous enfreignez? Ou voulez-vous
faciliter ceci et nous payer vingt millions de dollars?) Après une
très courte négociation, Sun donna à IBM un chèque, et les costumes
bleus s'en allèrent vers la compagnie suivante sur leur liste de
compagnies à assassiner.

#IWt: Dans la société Amérique, ce genre d'extorsion se répète chaque
semaine. Le brevet comme stimulant de l'invention a depuis longtemps
ouvert la voie au brevet comme instrument contondant pour établir la
mainmise sur l'innovation.

#HSi: Introduction Rapide

#AWa: Pour aider les lecteurs à saisir pourquoi la proposition de directive
sur les brevets logiciels de la Commission Européenne est si
controversée, l'analyse démarre avec un scénario simple.

#ItW: Imaginez que vous êtes propriétaire d'une petite société de logiciels.
 Vous avez écrit un beau logiciel.  Ce logiciel est une combinaison
créative de mille règles abstraites (algorithmes) et beaucoup de
données.  Les règles ont pris quelques minutes ou heures chacune à
[ré]inventer, tandis que le développement et le débogage du logiciel
complet vous ont pris vingt années-hommes.  Neuf cents de ces règles
étaient déca connues il y a vingt ans.  Cinquante de ces règles sont
maintenant couvertes par des brevets.  Vous êtes propriétaire de trois
de ces brevets.  Pour obtenir ces trois brevets, vous étiez obligé de
courir à l'office de brevets, à exposer votre stratégie commerciale,
et à payer des frais d'avocat.   Entre-temps, IBM et Microsoft ont
déca transformé vos idées brevetées en profit.   Vous voulez qu'ils
s'arrêtent?  Leurs équipes d'avocats disent que vous êtes coupables de
contrefaçons sur vingt ou trente de leurs cinquante mille brevets. 
Aussi vous obtenez un accord amiable: vous reversez 3% du revenu de
vos ventes annuelles à IBM, 2% à Microsoft, 2% à %(dots).  Néanmoins,
vous entrez un jour dans la zone de profit.  Vous êtes maintenant une
société attractive.  Une agence de brevets vous approche. Il disent
que vous enfreignez deux ou trois de leurs brevets.  Le domaine
revendiqué est vraiment large.  Ils veulent cent mille euro.  Une
bataille juridique pourrait durer dix ans et coûter un million
d'euros.  Vous payez.  Un mois plus tard un agent en brevet frappe à
votre porte %(dots).  Bientôt vous êtes à bout de ressources
financières.  Vous cherchez la protection d'une grande société. 
Microsoft offre de vous racheter pour un prix symbolique.  Vous
acceptez.  Sous un système de droits d'auteur, vous seriez maintenant
indépendant et riche.  Mais grâce à l'aide des avocats de brevets,
Microsoft et autres sont capables de voler votre propriété
intellectuelle.

#SoW: Le logiciel est semblable aux autres domaines d'ingénierie de bien des
façons. Mais il y a une différence fondamentale: les programmes
d'ordinateurs sont issus d'objets mathématiques idéaux. Un programme
fait toujours exactement ce qu'il dit. Vous pouvez construire un
château dans les airs soutenu par une ligne d'épaisseur nulle, et il
restera debout.

#Ptw: Les mécanismes physiques ne sont pas si prévisibles, car les objets
physiques ont des comportements étranges. Si un programme est supposé
compter de un à mille, il fait exactement cela. Si vous construisez un
compteur à partir de machinerie, une courroie peut glisser et compter
le nombre cinquante-huit deux fois, ou un rouage peut dérailler et
vous pouvez sauter 572. Ces problèmes rendent la conception
d'appareillage mécanique fiable très difficile.

#Wst: Quand nous, programmeurs, mettons une déclaration while à l'intérieur
d'une déclaration if, nous n'avons pas à nous préoccuper de savoir si
la déclaration while fonctionnera tellement longtemps que cela brûlera
la déclaration if, ou si cela contre la déclaration if et si cela
l'usera. Nous n'avons pas à nous inquiéter de savoir si cela vibrera à
la mauvaise vitesse et si la déclaration if entrera en résonance et se
cassera. Nous n'avons pas besoin de nous inquiéter du remplacement
d'une déclaration if cassée. Nous n'avons pas besoin de savoir si la
déclaration if peut délivrer suffisamment de courant à la déclaration
while sans chute de courant. Il y a beaucoup de problèmes de
conception matérielle dont nous n'avons pas à nous préoccuper.

#Tsa: Le résultat, c'est que le logiciel est beaucoup plus facile à
concevoir, par composant, que le matériel. C'est pourquoi aujourd'hui,
les concepteurs utilisent le logiciel plutôt que le matériel chaque
fois qu'ils le peuvent. C'est aussi pourquoi, des petites équipes
développent souvent des programmes d'ordinateurs d'une formidable
complexité.

#Pnt: Les gens me disent naïvement, %(q:Si votre programme est innovant,
alors pourquoi ne pas le breveter?) Cette question présume qu'un
produit ne nécessite qu'un brevet.

#Iwr: Dans certains domaines, comme celui des produits pharmaceutiques, les
brevets fonctionnent souvent de cette manière. Le logiciel est à
l'extrême opposé: un brevet typique couvre beaucoup de programmes
dissemblables et même un programme innovant enfreindrait probablement
plusieurs brevets.

#Teo: C'est pourquoi un programme considérable doit combiner un grand nombre
de techniques différentes, et impliquer beaucoup de caractéristiques.
Même si quelques uns sont de nouvelles inventions, cela en laisse
quand même beaucoup qui ne le sont pas. Chaque technique ou
caractéristique vieille de moins de vingt ans est susceptible d'être
déjà brevetée par quelqu'un d'autre. Que ce soit effectivement breveté
est juste une affaire de chance.

#InW2: J'ai expliqué comment les brevets entravent le progrès.
L'encouragent-il aussi?

#PtW: Les brevets ne peuvent encourager que peu de gens à chercher de
nouvelles idées à breveter. Ils ne sont pas d'une grande aide car il y
a eu beaucoup d'innovations sans brevets. (Regardez les journaux et
les publicités des années 1980 et vous verrez.) Les nouvelles idées ne
sont pas le facteur limitant dans notre domaine. Le travail le plus
dur est le développement de grands systèmes.

#Pee: Les gens qui développent des systèmes ont de nouvelles idées de temps
en temps. Naturellement, ils utilisent ces idées. Avant les brevets,
ils publiaient leurs idées aussi, pour le prestige. Tant que nous
aurons beaucoup de développement logiciel, nous aurons un flux continu
de nouvelles idées publiées.

#Tdr: Le système de brevet entrave le développement. Cela nous pousse à nous
demander, pour chaque décision de conception, %(q:Serons-nous
poursuivis?). Et la réponse est une affaire de chance. Cela conduit à
des développements plus onéreux et à moins de développement.

#WWt: Avec moins de développement, les programmeurs auront encore moins
d'idées au fur et à mesure. Les brevets peuvent effectivement réduire
le nombre d'idées brevetables qui sont publiées.

#Adu: Il y a dix ans, le domaine du logiciel fonctionnait sans brevet.  Sans
brevet, ce domaine a produit des innovations telles que Windows, la
réalité virtuelle, les tableurs, et les réseaux. Et grâce à l'absence
des brevets, les programmeurs pouvaient développer des logiciels
utilisant ces innovations.

#Whc: Nous n'avons pas demandé le changement qui s'est imposé à nous. Il n'y
a aucun doute sur le fait que les brevets logiciels nous mettent la
corde au cou. S'il n'y a pas de besoin public clair et vital de nous
ligoter dans la bureaucratie, détachez-nous et laissez-nous retourner
au travail!

#crr

#MoW: Beaucoup de grands groupes industriels utilisent les fameuses
bibliothèques d'optimisation et autres outils logiciels écrits par
ILOG.  Ces outils représentent l'état de l'art dans l'innovation
logicielle et les mathématiques appliquées à la production
industrielle.  ILOG est une plus grandes et des plus respectées des
sociétés de logiciels en Europe.  %(PH) est le PDG fondateur de ILOG
et propriétaire de deux brevets logiciels.

#Lao: Le logiciel se rapproche plus des mathématiques (non brevetables) que
de la chimie (souvent citée comme une réussite du système de brevets);

#L1e: L'expérience américaine des brevets logiciels est désastreuse. Avant
de les imiter, nous devrions essayer de voir s'ils ne seraient pas
d'accord pour changer leur système.

#Pae: Pour ce faire, il sera nécessaire de faire du lobbying auprès des
grands éditeurs américains;

#LWt: Les éditeurs de logiciel européens préfèrent vivre avec la pression de
devoir faire progresser leurs logiciels en permanence plutôt que de
déposer des brevets, d'attaquer d'autres éditeurs, et de courir le
risque permanent d'enfreindre le brevet d'autres sociétés;

#Lsc: Le logiciel libre est un problème orthogonal. On peut imaginer déposer
des brevets avant de publier un logiciel libre sur le Net, et donc
créer des situations inextricables sur le plan juridique;

#L0j: L'argument que les start-ups du logiciel ne lèvent pas d'argent sans
brevet est fallacieux, je n'ai jamais rencontré ce cas de figure. 

#Mti: Ce que j'observe, c'est que les brevets n'ont pas été un bon moyen
pour stimuler l'innovation chez Cisco. La compétition en a été le
moteur; apporter de nouveaux produits sur le marché de façon opportune
est décisif.  Tout ce que nous avons fait pour créer de nouveaux
produits aurait été fait même si nous n'avions pas obtenu de brevets
sur les innovations et les inventions contenues dans ces produits. Je
le sais car personne ne m'a jamais demandé %(q:pouvons-nous breveter
cela?) avant de décider s'il fallait investir du temps et des
ressources dans le développement d'un produit.

#Tne: Le temps et l'argent que nous dépensons en montant des dossiers de
brevets, en poursuite judiciaire, en maintenance, en litige et en
droits d'usage serait mieux dépensé en recherche et développement de
produits conduisant à plus d'innovation.  Mais nous remplissons des
dossiers de centaines de brevets chaque année pour des raisons sans
rapport avec la promotion ou la protection de l'innovation.

#Mea: De plus, entasser les brevets ne résout pas vraiment le problème de
violation non intentionnelle de brevet dans le développement
indépendant.  Si nous sommes accusés de violation de brevet par son
détenteur, qui ne fabrique ou ne vend pas les produits, ou qui vend
dans de très moindres volumes que nous, nos brevets n'ont pas une
valeur suffisante aux yeux de l'autre partie pour le décourager d'une
poursuite judiciaire ou pour réduire la somme d'argent demandée par
l'autre société.  Donc, plutôt que de récompenser l'innovation, le
système de brevets pénalise les sociétés innovantes qui réussissent a
apporter de nouveaux produits sur le marché et il subventionne ou
récompense ceux qui n'ont pas réussi à le faire.

#alcatel02: Alcatel 2002: Les Litiges Fréquents sur les Brevets Détournent les
Efforts en Recherche et Développement

#erW: Dans un rapport de 2002, le géant des télécommunications français se
plaint de l'insécurité juridique causée par les brevets:

#eWa: Comme les autres sociétés opérant dans l'industrie des
télécommunications, nous avons de fréquents litiges concernant les
brevets et autres droits de propriété intellectuelle. Les tierces
parties ont revendiqué, et pourront revendiquer à l'avenir, que les
revendications de brevets à notre encontre allèguent que nous
enfreignons leurs droits de propriété intellectuelle. Se défendre de
ces revendications peut coûter cher et détourner les efforts de nos
personnels techniques et de gestion. Si nous ne réussissons pas à nous
défendre de ces revendications, nous pourrions être obligés de
dépenser des sommes considérables pour développer une technologie qui
n'enfreindrait pas de brevets ou pour obtenir des licences de la
technologie objet du litige. De plus, les tierces parties pourraient
essayer de s'approprier des informations confidentielles, et des
technologies et procédés commerciaux propriétaires utilisés dans nos
sociétés, ce dont nous ne pourrions nous prémunir.

#uao: Cela nuira à nos sociétés et nos résultats d'opérations si nous ne
sommes pas capables d'obtenir des licences pour des technologies
tierces dans des termes raisonnables.

#pde: Nous demeurons dépendant en partie des autorisations de licence de la
tierce partie qui nous permettent d'utiliser la technologie tierce
pour développer et produire nos produits. Cependant, nous ne pouvons
pas être certains que de telles licences seront disponibles pour nous
dans des termes commerciaux raisonnables ou disponibles tout court.

#B0f: Bradford Friedman (Cadence) 2002:  animosité générale à l'encontre des
brevets logiciels dans l'industrie

#BoW: Bradford L. Friedman, Directeur de la Propriété Intellectuelle,
Cadence Design Systems, Inc.

#Aod: Je suis sûr que ce comité en est conscient, il y a une animosité
générale à l'encontre des brevets purement logiciels à l'intérieur et
à l'extérieur de l'industrie due a, premièrement, la visible rente que
représentent, ce que j'appellerais diplomatiquement, les
revendications de brevets sur-étendues, et deuxièmement, la culture
historique non propriétaire de l'industrie de l'ingénierie logicielle.

#IWW3: En somme, en grande partie parce que le système de brevet actuel est
mal adapté à l'industrie d'outil de conception logicielle, l'industrie
a évolué pour minimiser l'impact des brevets sur la compétitivité et a
compté sur d'autres d'autres pilotes de l'innovation plus orientés
marché. Je pense que c'est une occasion manquée pour l'accélération de
la croissance économique et technologique dans l'industrie.

#GWr: Greenhall (Divx) 2022: 35% des fonds de Recherche et Développement
détournés en propos belliqueux

#Ret: R. Jordan Greenhall, Président Directeur Général, Divx Networks, a
expliqué lors d'auditions à la Federal Trade Commission combien le
processus de brevet était devenu une source de gaspillage dans le
domaine du logiciel.

#AWe: En tant que petite société, un des plus gros risques auxquels je dois
faire face est l'incertitude du marché. Je peux minimiser le risque en
comprenant très bien les produits de mes concurrents, en comprenant
très bien mes produits, en comprenant ce que les consommateurs et les
clients veulent. Mais j'ai découvert l'an dernier que je ne pouvais
vraiment pas comprendre le paysage des brevets et que je suis assis à
côté d'une bombe nucléaire, en haut de mes produits, qui peut exploser
à tout moment et faire simplement que je n'ai plus de société.

#InW: J'ai pris récemment un de mes chefs développeurs, un homme qui est
considéré comme une référence dans son domaine -- il siège aux comités
MPEG et ITU, et il est très impliqué dans le paysage de la propriété
intellectuelle sur tout ce qui touche à la vidéo numérique --  et je
lui ai demandé d'évaluer un brevet particulier dont nous avions
entendu parler sur le marché.

#Wun: Nous avons fait une recherche rapide sur le site web de l'Office des
Brevets Américain, qui à propos est très pratique, et nous avons
découvert pas moins de cent-vingt brevets qui revendiquaient être dans
le champ d'application général de ce brevet particulier, qui était
abondamment cité.

#TnW2: Le pauvre homme a perdu la plus grande partie de cinq jours à examiner
tous ces différents brevets et revint me voir, %(q:Je n'ai pas la
moindre petite idée sur le fait que nous enfreignons ou pas ces
brevets, et franchement, ils ont tous l'air de s'enfreindre
mutuellement.)

#Trl: Le résultat final, c'est que je ne sais absolument pas si mon produit
enfreint plus de cent-vingt brevets, lesquels sont tous détenus par de
grandes sociétés qui pourraient me poursuivre sans même y penser.

#Tse: En conclusion, un peu comme Borland, j'ai fait une directive qui
réalloue en gros vingt à trente pour cents des ressources de nos
développeurs et j'ai signé avec deux agences de brevets afin
d'augmenter notre portefeuille de brevets et d'être capable de
s'engager dans le conflit vomitif des brevets. Je pense que le concept
ici serait nommé propos belliqueux. Je dois être capable de dire,
%(q:Ouais, moi aussi j'ai breveté ça, alors allez-vous en et
laissez-moi seul.)

#DnA

#S9n: Lors des auditions de l'Office des Brevets Américains de 1994, un
représentant de Adobe dit:

#LWo: Laissez-moi clarifier ma position sur la brevetabilité du logiciel. Je
crois que le logiciel en tant que tel ne devrait pas avoir droit à la
protection par brevet.  Je prend cette position en tant que créateur
de logiciel et comme bénéficiaire des récompenses que le logiciel
innovant peut apporter sur le marché.  Je ne prend pas cette position
parce que moi ou ma société est avide de voler les idées des autres
dans notre industrie.  Adobe a bâti sa société en créant de nouveaux
marchés avec de nouveaux logiciels.  Nous prenons cette position parce
que c'est la meilleure politique pour maintenir une industrie
logicielle en bonne santé, où l'innovation peut prospérer.

#Fhn: Par exemple, quand nous, chez Adobe, avons fondé une société sur le
concept d'un logiciel pour révolutionner le monde de l'impression,
nous pensions qu'il n'y avait aucune possibilité de breveter notre
travail. Cette croyance ne nous a pas arrêtés pour créer ce logiciel,
ni découragés les capital-risqueurs futés qui nous ont aidés avec les
premiers investissements. Nous avons fait de très bonnes affaires bien
que nous n'eussions pas de brevets couvrant notre travail original.

#Oed: D'un autre côté, l'émergence ces dernières années de brevets logiciels
a causé du tort à Adobe et à l'industrie. Une %(q:taxe sur les litiges
de brevets) est une entrave à notre santé financière, que notre
industrie ne peut se permettre.  Des ressources qui auraient pu être
utilisées pour davantage d'innovation ont été détournées pour le
problème des brevets. Des ingénieurs et scientifiques comme moi-même
qui auraient pu créer de nouveaux logiciels travaillent à la place à
l'analyse de brevets, au dépôt de dossier de brevets et à la
préparation des défenses.  Les revenus sont engloutis dans les
dépenses juridiques au lieu de la recherche et du développement. Il
est clair pour moi que le mandat constitutionnel pour promouvoir le
progrès dans les métiers utiles n'est pas rempli par la question des
brevets logiciels.

#Jpo

#Wfe: Aux Auditions de la Federal Trade Commission de 2002, Kaplan
expliquait:

#Ikn: Intouch est une société de e-commerce qui détient beaucoup de brevets
et qui a été attaqué avec des brevets, y compris par Amazon.  Joshua
Kaplan est leur Président Directeur Général.  Ces extraits sont tirés
de ses déclarations lors d'auditions de la Federal Trade Commission de
2002:

#Tls: Il y a des brevets qui sortent aujourd'hui avec des centaines de
revendications, inintelligible pour la plupart des gens exceptés ceux
qui les ont construits. Et cependant, les gens qui les enfreignent
mettent quelquefois en péril toute une vie d'investissement ou leur
division ou leur produit. Ce système ne fonctionne pas bien pour être
l'aiguillon de l'innovation ou pour remplir le mandat constitutionnel.

#Iid: En effet, pour ceux d'entre vous qui étaient là ce matin et qui ont
écouté les personnes de l'industrie logicielle parler de la menace que
cela représentait pour leur sociétés, comme je le vois, les brevets
aujourd'hui permettent aux sociétés établies de se retrancher derrière
eux aux dépens du nouveau venu. Je demande aujourd'hui si un Steve
Jobs pourrait créer un Apple ou un Bill Gates créer un Microsoft en
vue sur le web et dans la forêt de brevets qui est là.

#OWr

#toW: Extrait d'une déclaration de Oracle lors des auditions sur la
brevetabilité logicielle à l'Office des Brevets Américain en 1994

#OWW: Oracle Corporation s'oppose à la brevetabilité logicielle.  La 
société pense que la loi existante sur les droits d'auteurs et les 
protections disponibles sur le secret commercial, par rapport à la loi
 sur les brevets, sont mieux adaptés à la protection des
développements  de logiciels pour ordinateur.

#Plm: La loi sur les brevets fournit aux inventeurs un droit exclusif sur 
une nouvelle technologie en contrepartie de la publication de la 
technologie. Ceci n'est pas approprié pour les industries telles que 
celles du développement logiciel, pour lesquelles les innovations 
arrivent rapidement, peuvent être réalisées sans apport de capital 
substantiel, et qui tendent à être des combinaisons créatives de 
techniques déjà connues.

#UWs: Malheureusement, comme stratégie de défense, Oracle a été forcée  pour
se protéger de déposer sélectivement des brevets, ce qui donnera  les
meilleures opportunités de licences croisées entre Oracle et  d'autres
sociétés qui pourraient prétendre qu'un de leurs brevets a été  violé.

#Auu: Bien que tout ce j'aie %(q:inventé) fut innovant, ces inventions 
utilisaient toutes des procédures complexes et ont toutes été
appréciées  par ceux qui ont payé des millions pour utiliser ce que le
risque  d'entrepreneur innovant que j'ai pris, a créé. Il ne m'est
jamais venu à  l'esprit de les breveter, et je n'aurais pas pu
breveter ces %(q:arts  utiles) si je l'avais voulu.

#TWW: La question fondamentale est: Voulons-nous permettre la détention 
d'un monopole sur tout ce qui fonctionne comme des processus 
intellectuels logiques? J'espère que non.

#Tam: L'esprit a toujours été sacro-saint. La revendication pour que les 
processus intellectuels et les procédures logiques (qui ne manipulent 
pas principalement des équipements) puissent être détenues ou 
monopolisées développe bien trop l'avidité et l'avarice. Les processus
 intellectuel algorithmiques doivent demeurer non brevetables -- même 
s'ils sont représentés par du codage binaire dans un ordinateur; même 
s'ils sont exécutés par le successeur de la calculatrice.

#Wnw: Ce qui effraie et rend furieux tant d'entre nous au sujet des  brevets
logiciels, c'est qu'ils cherchent à monopoliser nos processus 
intellectuels quand leur représentation ou leur exécution est assistée
 par une machine.

#EWl: Tout ce qui est représenté ou exécuté par logiciel est d'abord un 
processus intellectuel algorithmique complètement détaillé. Il n'y a
pas  d'exception, autre que par erreur.

#TWp: C'est pourquoi, je proteste respectueusement contre le titre donné  à
ces auditions -- %(q:Inventions Relatives aux Logiciels) -- puisque 
vous n'êtes pas essentiellement concernés par des gadgets contrôlés
par  logiciel. Le titre illustre un parti pris inapproprié et prêtant 
sérieusement à confusion. En fait, en plus d'un quart de siècle en
tant  que professionnel des ordinateurs et observateur et écrivain
dans cette  industrie, je ne me souviens pas avoir jamais entendu ou
lu une telle  phrase -- excepté dans le contexte des revendications
juridiques pour le  monopole, où les demandeurs essayaient de
dénaturer la tradition de  brevetage des équipements afin de
monopoliser l'exécution des processus  intellectuels.

#TWn: Il n'y a absolument aucune preuve, quoi qu'il en soit -- pas une  once
-- que les brevets logiciels ont facilité ou faciliteront le progrès.

#TkW: La société pour laquelle je parle, Autodesk, détient un certain 
nombre de brevets logiciels et en a déposé d'autres -- qui, bien sûr, 
demeurent secrets selon la loi américaine actuelle. Cependant, tous
sont  défensifs -- dans un phénoménal gaspillage de nos talents
techniques et  de nos ressources financières, rendu nécessaire
seulement par  l'invention de brevets logiciels des avocats.

#Ara: Autodesk a affronté au moins dix-sept revendications de brevets  sans
fondement faites à son encontre et a dépensé plus d'un million de 
dollars pour se défendre, et certainement des millions à venir pour 
tarir le puits sans fonds des brevets à moins que nous n'arrêtions
cette  débâcle. Heureusement -- contrairement aux producteurs de
logiciels plus  modestes -- nous avons les ressources techniques et
financières pour  repousser de telles revendications.  Nous avons
réfuté toutes les  revendication sauf une, avant même que les
détenteurs de brevets  puissent engager une action vaine judiciaire,
et nous avons contesté la  revendication restante en conclusion.
Remarquez que votre Office a  délivré au moins seize brevets que nous
avons réfuté avec succès, et  nous n'avons jamais payé un penny pour
ces tentatives d'extorsions  auxquelles votre Office a prêté son
concours.

#Beb: Mais ce fut un énorme gaspillage de ressources qui auraient pu être 
mieux investies en innovation utile. Ces revendications infondées et 
interminables bénéficient aux avocats en brevets, mais elles ne 
facilitent certainement pas le progrès.

#Wso: Nous proposons deux recommandations, la seconde ayant douze parties 
-- pour ainsi dire, les Douze Apôtres de la Réparation:

#Foe: PREMIÈREMENT: Publier le résultat que le logiciel, comme je l'ai 
défini, met en oeuvre des processus intellectuels qui n'ont pas 
d'incarnation physique; processus qui sont exclusivement de nature 
analytique, intellectuelle, logique et algorithmique. Utilisez ce 
résultat ainsi que l'intention Constitutionnelle clairement énoncée, 
pour déclarer que l'Office des Brevets s'est trompé quand il a accordé
 des brevets logiciels. Déclarez que les brevets logiciels
monopolisent  les processus intellectuels et algorithmiques, et aussi,
qu'ils ne  réussissent pas à remplir le mandat Constitutionnel de
facilitation du  progrès -- qu'en fait, ils menacent clairement cet
objectif.

#Sdt: DEUXIÈMEMENT: Tant que -- et seulement tant que -- les brevets 
logiciels soient définitivement interdits, rejetez ou au moins gelez 
toute application de ce genre qui n'a pas encore été accordée, en 
attendant qu'une action concluante soit entreprise sur les douze 
recommandations suivantes:

#RtW: RÉPARER LES ERREURS GRAVES DES ADMINISTRATIONS PRÉCÉDENTES: Publiez 
le résultat qu'il y a eu beaucoup d'erreurs, et d'erreurs graves de 
jugement sur un grand pourcentage de brevets logiciels délivrés par le
 passé, et demandez immédiatement le retour de tous les brevets
logiciels  pour correction et annulation possible.

#Leh: Laissez-nous nous épauler plutôt que de nous laisser nous marcher  sur
les pieds.

#Mft

#Bhb: Comme il est impossible de savoir quelles demandes de brevets sont 
dans le circuit de demandes, il est très possible, même probable, de 
développer un logiciel qui incorpore des caractéristiques qui sont le 
sujet de la demande de brevet d'une autre société.  Donc, il n'y a pas
 de moyen d'éviter le risque de se retrouver par inadvertance accusé
de  violation de brevet parce qu'aucune information n'est disponible 
publiquement au moment qui aurait pu offrir une direction de ce qu'il 
fallait éviter.

#Tor: La période de protection par brevet, dix-sept ans, n'a plus de sens 
dans une ère où une génération entière de technologies disparaît en 
quelques années.

#IiW: Si quelque futur plaignant réussit à confirmer les droits d'un de  ces
%(q:mauvais) brevets, cela demandera un litige long et coûteux, dont 
l'issue est franchement incertaine, pour défendre les droits des 
créateurs qui n'auraient jamais du être récusés en premier lieu.

#BEf: Bill Gates 1991:  Les brevets empêchent la concurrence et mènent 
l'industrie à la stagnation

#T9o: Ceci a été déclaré par Fred Warshofsky dans %(q:La Guerre des 
Brevets) en 1994.  Ce texte est tiré d'un mémo interne écrit par Bill 
Gates à son personnel.  Apparaît en partie dans d'autres %(cc:mémos de
 Gates).

#IWe: Si les gens avaient compris comment les brevets seraient accordés, 
quand la plupart des idées inventées aujourd'hui ont obtenu des
brevets,  l'industrie serait aujourd'hui en complète stagnation.  ...
La solution  est de breveter autant que nous le pouvons. Une future
jeune pousse sans  brevets lui appartenant sera forcée de payer le
prix que les géants  choisiront d'imposer, quel que soit.  Ce prix
pourrait être élevé. Les  sociétés établies ont un intérêt à exclure
les futurs concurrents.

#rGW: Microsoft Allemagne 2003

#aea: Dans une brochure destinée au personnel de vente de Microsoft  appelée
%(q:Windows contre Linux), Microsoft soutient que les brevets 
logiciels créent un grand risque pour le logiciel open source et le 
logiciel indépendant en général.  Le PDG de Microsoft Allemagne a 
insisté là-dessus à différentes occasions, quelquefois en combinaison 
avec la suggestion que Microsoft lui-même utilisera    son 
porte-feuilles de brevets plus agressivement à l'avenir.  La brochure
a  été rapidement retirée du Net, mais nous en avons gradé une
%(cp:copie)  (en allemand).  En page 5 Microsoft écrit:

#HmO: Håkon Wium Lie, Directeur technique de Opera

#Tlu: La société norvégienne de logiciels Opera Inc développe un  navigateur
web qui est réputé pour sa stabilité, sa compacité et sa  rapidité. 
Grâce à sa position en tant que leader qualité, Opera  développe aussi
le logiciel multimédia qui est utilisé dans les  téléphones mobiles
Nokia.  Les Logiciels Opera soutiennent la campagne  de Eurolinux pour
une Europe sans brevets logiciels.  Leur directeur  technique Håkon
Wium Lie a publié la déclaration suivante au World Wide  Web
Consortium (W3C) à l'occasion de la question: le W3C devrait-il 
accepter des standards fondés sur une redevance (RAND) ou des
standards  libre de droits (RF), au début 2002:

#Ofr: La position des Logiciels Opera dans le débat RF/RAND est que les 
standards fondamentaux pour le Web doivent continuer à être libres de 
droits (RF). En conséquence, nous pensons que le W3C ne devrait pas 
définir des procédures d'accord de licence RAND.  Faire ceci aiderait
à  légitimer les brevets logiciels dont nous pensons qu'ils sont
nuisibles  au développement du Web. Également, les brevets logiciels
sont un  concept essentiellement américain non reconnu dans les autres
parties du  monde.

#LoW: Linus Torvalds 08/2002

#Lhm: Les bidouilleurs de Linux VM sont actuellement engagés dans des 
discussions à la fois sur la prise en charge des grandes pages
(couvert  la semaine dernière) et l'amélioration de performance du
nouveau  mécanisme de cartographie inverse. Cette discussion ralentit
cependant,  quand Alan Cox montra du doigt que nombre des techniques
débattues  étaient couvertes par des brevets. En fait,un examen plus
poussé de  Daniel Phillips montre que nombre des méthodes Linux
existantes, y  compris la cartographie inverse en général et
l'algorithme d'allocation  mémoire buddy, sont couverts par ces
brevets. C'est un problème, dit-il,  que nous ne pouvons ignorer. 
C'était en réponse à Linus, monté au  créneau avec sa politique de
brevets logiciels et de code noyau.  Il  concéda plus tard que ce
n'était pas un %(q:argument juridiquement  soutenable) mais le seul
moyen de continuer à développer le noyau sans  devenir dingues.

#Ins: Je ne vérifie pas les brevets par %(e:principe), parce que (a)  c'est
une énorme perte de temps et (b) je ne veux pas savoir.

#Thy: Le fait est que les techniciens se portent mieux à ne pas regarder 
les brevets. Si vous ne savez pas ce qu'ils couvrent et où ils sont, 
vous ne violerez pas sciemment l'un d'entre eux. Si quelqu'un vous 
poursuit en justice, vous changez l'algorithme ou vous engagez 
simplement un homme de main pour rosser le stupide imbécile.

#TFa: %(AL), auteur de %(VD), un outil pour conversion de formats audio  et
vidéo, a de tristes nouvelles à raconter:

#TFa2

#Woy

#dat

#eml

#atc

#raf

#oWt

#edw

#Mnt

#Lkm

#SuW

#Eee

#Soi

#Bro

#Sai: Cependant, dans tous ces textes, l'utilisation planifiée de forces de
la nature contrôlables est considérée comme une condition nécessaire
pour que l'agrément soit donné au caractère technique d'une invention.
Ainsi que nous l'avons exposé plus haut, l'inclusion des forces de la
raison humaine en tant que telles dans le domaine des forces de la
nature dont l'utilisation pour la création d'une innovation fonde son
caractère technique, aurait pour conséquence directe l'attribution
d'une signification technique à toutes les activités de la pensée qui
en tant que série d'instructions sont susceptibles de causer un
résultat d'une manière prévisible. A partir de là, le concept de
technicité perdrait son rôle de critère, l'ensemble des réalisations
de l'intelligence humaine - dont les l'envergure et les limites sont
inconnues et imprévisibles - se verraient ouvrir les portes du droit
des brevets.

#Eii: De plus, on peut avoir de bonnes raisons de conclure que, étant donné
l'unanimité avec laquelle la jurisprudence et la littérature ont
constamment insisté sur les limitations de la brevetabilité aux
inventions de nature technique, on peut parler d'un droit coutumier à
ce sujet.

#Dnu: Mais peu importe finalement. En effet le concept de technicité
apparaît comme le seul qui puisse servir à distinguer clairement ce
qui est brevetable de ce qui relève plutôt d'une autre forme de
production intellectuelle de l'homme pour laquelle le brevet n'est ni
approprié ni envisagé. Si l'on devait renoncer à cette ligne de
partage, il n'y aurait plus, par exemple, de possibilité certaine de
faire la différence entre les les prestations brevetables et celles
pour lesquelles le législateur a attribué d'autres formes de
protection, en particulier le droit d'auteur. Le système allemand de
propriété industrielle et droit d'auteur repose essentiellement sur le
fait que certaines formes de protection sont valables pour certaines
catégories précises de prestations intellectuelles et qu'il faut
éviter autant que possible les recoupements entre ces différents
droits de protection. La loi sur les brevets n'a tout de même pas été
conçue comme une loi à tout faire au sein de laquelle les catégories
de prestations intellectuelles pour lesquelles rien n'a été prévu par
la loi pourraient trouver une protection, mais comme une loi
spécialisée, visante à protéger un ensemble clairement délimité de
prestations intellectuelles : les prestations techniques, et elle a
toujour été comprise et appliquée comme telle.

#Ene: Par conséquent, il faut empêcher le système de protection des biens
intellectuels d'obtenir une extension des limites de la technicité,
concept qui serait alors détourné de son rôle. Il doit rester clair,
bien au contraire, qu'une règle d'organisation ou de calcul en
elle-même ne mérite pas de se voir protégée par un brevet si sa
relation au domaine technique ne repose que sur l'emploi d'un
ordinateur dans l'usage commercial qui en est fait. Il ne nous
appartient pas de débattre ici de l'éventuelle protection qui peut lui
être accordée soit par le droit d'auteur, soit par le droit de la
concurrence.

#Beh

#Dzg: La Cour Fédérale de Justice explique pour quoi une règle de calcul
pour l'optimisation de la production de acier n'est pas une invention
technique.

#Wdg

#GKW

#GdW

#Wmr

#EPf

#Ilt

#Aso

#IWn

#RLt

#Tmr

#1en

#SpW

#Das

#Dnn

#ArW: National Research Council of the USA 2000

#Ien

#Aeo

#2nt

#Eht

#3iz

#Tkt

#Kee

#Pio

#DWx

#soa

#biw

#oae

#oiu

#Tgi: Cette citation montre depuis combien de temps le système de brevets a
déja été hors de contrôle.

#SbW2

#AeW

#Has

#JKl

#Aig

#PaW

#Qne

#WiW

#Kee2

#Shf

#IVl

#hrP

#ahj

#tei

#rsm

#rfa

#bpatg17

#eei

#2nW

#EWW

#edp

#Wer

#bes

#0pW

#toe: Le programme d'elections du Parti Socialiste Belge dit sur page 110

#0Ea: Henkel 2002/12/29: Pourquo l'UE devrait interdire les brevets sur le
logiciel

#Wmh: Dans l'organ central de l'Association des Ingénieurs Allemands, Dr.
Joachim Henkel de l'Institut de Recherches sur l'Innovation explique
pour quoi les brevets logiciels nuirent a l'innovation et demande que
la Commission Européenne doit les interdire.

#goi

#uno

#TWe

#DWr

#Edo

#Snt

#Eim

#Rra

#AWh

#Vnz

#WaW: Les innovations dans le domaine du logiciel sont, plus que les
innovations dans les autres domaines, basé sur des innovations
précédentes.  C'est pour quoi l'aspect négatif du brevet, le fait
qu'il rend les developpement complémentaire plus difficile, est plus
onéreux dans le cas du logiciel que autre part.  Le licensement n'est,
a cause des couts de transaction impliqués, pas une vraie solution.

#SSa

#Jee

#Tel

#Tno

#Ene2

#Tsn

#SbW

#AlW

#CWr

#Ieh

#Ois

#FMp

#Toa

#BRw

#EnW2

#Fsl

#Upy

#Gdr

#Fvp

#PWr

#Soe

#SuW2

#Ifp

#MiW

#VWu

#IWp

#MlW2

#PRm

#Iyd

#LWi

#HAE

#lhh

#FrH

#IWW

#Iho

#All

#Tce

#Ect

#LoM

#Iss

#Jov

#JGb

#IjW

#tWE: Jean-Michel Yolin, président de la section %(q:Innovation) au
Ministère de l'économie, des finances et de l'industrie, auteur du
rapport %(IE), observe dans un entretien sur les menaces de brevet de
SCO contre IBM/Linux

#pat

#Eoe: Committé Économique et Social de la Communauté Européenne 2002/09

#Aot

#AyE: Même si le champ d'application de la directive proposée par la
Commission concerne pour l'instant  les  inventions  mises  en  oeuvre
 par  ordinateur,  auxquelles  sont  attachés  les  critères 
cumulatifs classiques  délimitant  le  domaine  d'application  de  la 
brevetabilité,  ce  qui  ne  satisfera  pas  les  partisans  de 
l'abolition pure et simple de l'abolition de toute limite au domaine
d'application du droit des brevets, ce texte n'en  constitue  pas 
moins,  de  fait,  une  acceptation  et  une  justification  a 
posteriori  de  la  dérive jurisprudentielle  de  l'OEB.  Tout  en 
présentant  à  première  vue  une  position  moins  extrême  que 
l'abolition pure et simple de l'article 52.2 CBE que souhaitent  la 
direction  de  l'OEB  et  certains  membres  du  Conseil, cette
directive n'en serait pas moins une porte ouverte à la brevetabilité
future de la totalité du domaine des logiciels,  notamment  par 
l'admission  que  %(q:l'effet  technique)  peut  être  le  fait  du 
logiciel  seul  sur  un ordinateur standard.

#Ohf: Il est permis de s'interroger sur la finalité réelle d'une telle
directive, en particulier au regard de l'exposé des motifs, qui
s'ouvre  sur  des  considérations  relatives  à  la  nécessité  de 
protéger  l'industrie  du logiciel contre le piratage, et évoque
presque exclusivement, dans les documents annexes à la directive, les
logiciels  et  

#IWW2: Convient-il  aujourd'hui  d'étendre  les  brevets,  outils  de  l'ère 
industrielle,  à  des  créations  de  l'esprit, immatérielles, comme
les logiciels et au résultat de  leur  exécution  par  ordinateur  ? 
La  réponse  est tout à fait explicite et partisane dans la
présentation de la proposition de directive et  la  fiche  d'impact. 
Le champ  de  vision  étroit  adopté,  partant  du  régime  juridique 
des  brevets  comme  motivation  unique,  sans considération 
suffisante  des  facteurs  économiques,  de  l'impact  sur  la 
recherche,  sur  les  entreprises européennes, donc sans vision
d'ensemble, n'est pas en cohérence avec l'importance des enjeux de
société, de développement  et  même  de  démocratie 
(e-administration,  éducation,  information  des  citoyens)  qui  sont
 en cause à terme.

#ThW: Le Comité des régions tient à attirer l'attention de la Commission sur
le caractère non universel de la protection par les brevets et sur les
dangers qui peuvent résulter d'une systématisation du recours aux
brevets en matière de propriété intellectuelle. Ces questions
concernent principalement les nouvelles technologies et avant tout les
technologies de l'information et les sciences du vivant, lesquelles
sont l'objet d'un débat riche et passionné.

#Iro: En ce qui concerne le cas du logiciel, les débats qui ont eu lieu dès
les années soixante-dix dans les grands pays concernés ont tous conclu
au recours au système du droit d'auteur, bien que ce système ne
constitue qu'un cadre juridique imparfait quant aux spécificités de ce
secteur. La Directive européenne du 1er janvier 1993 a notamment
montré une voie de sagesse en vue de favoriser l'interopérabilité des
programmes afin de contrecarrer les stratégies anticoncurrentielles de
recherche de position dominante. Mais depuis plusieurs années, la
jurisprudence américaine a été conduite à accorder la délivrance de
brevets pour des %(q:composants) logiciels, à laquelle elle avait été
jusque là hostile. Et la pression américaine sur l'Europe se fait de
plus en plus vive pour que la brevetabilité soit acceptée au niveau
européen.

#TnW: Or l'enjeu est ici des plus importants. Une telle pratique menace la
dynamique de l'innovation dans cette industrie, dans la mesure où elle
conduit à un cloisonnement des savoirs et des procédures, qui interdit
toute pratique combinatoire. Ainsi parmi la multitude de brevets
déposés et accordés aux Etats-Unis, on trouve un très grand nombre de
procédures, voire d'algorithmes. Beaucoup d'entre eux apparaissent
éloignés des critères de nouveauté et d'originalité, qui conditionnent
en principe la délivrance d'un brevet.

#Icr: L'institutionnalisation de la délivrance de brevets dans le domaine du
logiciel constituerait une arme pour le renforcement de la position
dominante des plus gros leaders américains du secteur. Elle
constituerait une menace directe pour l'immense population des PME
innovantes dans cette activité, tant en Europe qu'aux USA et dans les
pays tiers. Elle serait enfin un très lourd handicap pour l'industrie
européenne du logiciel dont la compétitivité a beaucoup de mal à
s'affirmer commercialement, malgré le haut niveau des compétences qui
s'y déploient.

#Pan

#PrW

#LWa

#POI

#Loa

#l1o

#loe

#Eeo: Programme Électoral du Parti Socialiste Belge 2003

#ltt

#fmn

#TWs

#dih: Député de la Moselle

#Lio

#Cts

#Mev: Mais, comme il est rappelé dans les manuels de référence juridique
tels que le %{LI}, ces brevets n'ont de valeur que celle que l'on veut
bien leur accorder en raison de la contradiction manifeste qui existe
aujourd'hui entre le droit positif et le système jurisprudentiel de
l'OEB. En cas de contentieux, il n'est pas certain qu'un juge national
accepterait la validité de ces brevets en raison de leur objet,
manifestement contraires à l'esprit de la loi.  Les détenteurs de
brevets logiciels, de brevets sur le commerce électronique et de
brevet Internet n'attendent donc qu'une chose pour attaquer les
acteurs français et européens de la nouvelle économie: une révision de
la convention de Munich qui supprimerait l'exception sur les
programmes d'ordinateurs.

#AWi: Aussi, je vous serais reconnaissant de bien vouloir user dans les
consultations nationales, européennes ou mondiales à venir, de tous
les moyens qui sont en votre pouvoir pour exiger:

#dan

#qlW: que soient garantis par la loi un %(q:droit à diffuser ses propres
oeuvres originales) (logiciels y compris) ainsi qu'un « droit à la
compatibilité » tel qu'il est défini dans la %(os:proposition de loi
déposée avec MM. Paul, Cohen et Bloche).

#qnW: que les termes « technique », « application industrielle » et «
programme en tant que tel » soient clarifiés de façon à ce que toute
oeuvre, tout produit informationnel immatériel (y compris un logiciel
sur un support d'information) ne soit ni admis dans le champ de la
brevetabilité ni dans celui de la fourniture de moyen de contrefaçon
de brevet.

#qWo: que tout produit matériel, extension d'un produit informationnel
immatériel (ex. un lecteur MP3) puisse être breveté à condition que
soient satisfaits les critères de nouveauté, de technicité et
d'application industrielle de ce produit matériel, considéré
indépendamment des éléments logiciels qu'il exploite.

#qdi: que soit lancé dans les plus brefs délais un débat ouvert et
démocratique fondé sur des études scientifiques détaillées des effets
économiques et sociaux induits par une extension du système des
brevets à la société de l'information.

#qey: que soit mise en place une base de données de brevets complète,
librement accessible sous forme de contenu libre et de logiciels
libres, afin de donner aux PME les moyens de faire face aux risques de
contentieux de brevets en Europe et dans le monde.

#Aen: Aucune étude n'ayant été publiée par l'Office Européen des Brevets
pour justifier l'intérêt économique de l'extension au logiciel de la
brevetabilité, alors même que des économistes ont démontré que le
système de brevet pouvait aboutir à une diminution de l'innovation
dans l'économie du logiciel, il me semblerait également opportun de
commanditer un audit de l'Office Européen des Brevets afin de
déterminer les moyens de mieux contrôler les décisions de cet
organisme et de s'assurer qu'elles sont bien conformes à l'intérêt
général et au principe fondamental d'impartialité de la justice.

#Wse: porte-parole des Verts pour les questions économiques

#Vir: Les créateurs sont confrontés à une situation d'insécurité du fait de
la possible brevetabilité des logiciels et des concepts commerciaux.
Il faut faire la lumière au plus vite sur ce sujet.

#CWa: D'après la Convention Européenne des Brevets (CBE), les programmes
d'ordinateurs ne sont pas brevetables %(q:en tant que tels). Aux
Etats-Unis au contraire, tout ce qui peut être créé par l'homme est
par principe brevetable.

#Bwd: Chez les développeurs de logiciels et sur la scène Open Source, règne
une grande insécurité. On craint en effet que l'amendement de la CBE
et la doctrine juridique de la Commission Européenne qui s'annonce
n'aboutisse à l'introduction en Europe des habitudes américaines. 

#IgA: Aux Etats-Unis, l'exercice de la concurrence est déjà considérablement
entravé par la brevetabilité des concepts commerciaux. Un exemple
souvent cité est le brevet déposé par Amazon sur le procédé öne-click
lors de la commande de produits sur l'Internet.

#Asm: De toutes façons, ce n'est qu'au cours des dernières années en Europe
que le logiciel est devenu brevetable comme composant de procédés
techniques. En effet, les procédés techniques qui contiennent les
programmes d'ordinateurs sont brevetables. Par exemple, une machine
outil pilotée par ordinateur constitue un objet entièrement
brevetable. Il nous manque pour l'instant des analyses économiques
détaillées des effets d'une possible brevetabilité des logiciels. Nous
voyons tout de même clairement le danger d'un renforcement
supplémentaire de la bureaucratie qui aurait pour effet de freiner les
innovations les plus nécessaires.

#DDf: De plus, le fait de rendre les logiciels brevetables soulèverait de
considérables problèmes techniques autant qu'administratifs: avec les
deux ans de délai actuel, quand le brevet serait enfin déposé il
serait depuis longtemps obsolète. Le travail de documentation des
brevets coûterait très cher. Les petites et moyennes entreprises
seraient désavantagées par la brevetabilité: seules les grandes
entreprises disposent des ressources nécessaires pour rester en phase
avec le développement des brevets. Seules les grandes entreprises
pourraient déposer des brevets et en tirer des revenus
supplémentaires. La concurrence se jouerait sur des disputes
juridiques et non plus sur la mise en oeuvre rapide des innovations.
Le progrès technique s'en trouverait entravé. Il faut empêcher cela !

#MsW: député fédéral

#Vns: Président de la Commission Parlementaire sur les nouveaux media

#Itm: Dans les milieux de spécialistes des questions de politique des
technologies, on entend constamment affirmer que le système des
brevets doit être étendu à certains domaines des technologies de
l'information parce que sans cela les investissements ne seraient pas
suffisamment protégés. Jusqu'ici, cette affirmation a toujours été
présentée comme une vérité abstraite et n'a été étayée par aucun fait
prouvé de l'économie allemande ni européenne. 

#ScW: Et même s'il était possible de trouver des domaines des techniques de
l'information pour lesquels les brevets auraient, ou auraient eu par
le passé, un effet favorable manifeste, encore faudrait-il se demander
si d'éventuels effets secondaires néfastes de la brevetabilité n'en
annuleraient pas les avantages.

#Aiz: Mais, alors que le pouvoir législatif est encore dans l'incapacité de
faire la lumière sur ce sujet, le pouvoir judiciaire passe déjà à
l'action, accordant des milliers de brevets sur des logiciels et
essayant d'imposer une modification des règles légales. Il est donc
grand temps pour nous les législateurs de nous occuper de ces
questions. 

#ReK: Rainder Brüderle

#WhP: Wirtschaftspolitischer Sprecher der FDP-Bundestagsfraktion

#DWs: Die Vorentscheidung zugunsten von Softwarepatenten des
Verwaltungsrates des Europäischen Patentamtes lässt aufhorchen. Hier
wurde auf Verwaltungsebene eine Schlüsselentscheidung für die
Schlüsseltechnologien des 21. Jahrhunderts vorbereitet. Das Thema
Softwarepatente ist allerdings zu wichtig für unsere wirtschaftliche
Zukunft, als dass es länger in den Hinterzimmern multinationaler
Gremien und den juristischen Spezialzirkeln geführt werden darf.

#Wmi: Wir sollten sehr kritisch prüfen, ob wir auf dem schnellebigen Gebiet
der Software einen exklusiven Patentschutz wollen.  Denn die
Patentierung von Software ist ein höchst zweischneidiges Unterfangen.
Dem Schutzinteresse Einzelner steht die Innovationsfähigkeit der
gesamten Branche gegenüber.  Es sollte aufhorchen lassen, dass der
zweitgrößte Softwarehersteller der Welt ORACLE sich aus diesem Grund
massiv gegen Softwarepatente ausspricht. Insbesondere die in Europa
starke und zukunftsträchtige Bewegung der freien Software (OpenSource
wie LINUX) wäre durch Softwarepatente in ihren Grundfesten gefährdet.

#Stn: Softwarepatente bergen die Gefahr in sich, daß die Großen der Branche
dank Finanz und Personalkraft kleine und mittelständische
Softwareschmieden mittels der Patentierung existenziell gefährden
werden.  Die Verfahren gegen Microsoft belegen aber deutlich, wie
wichtig und schwierig Wettbewerb schon heute im Softwarebusiness ist.
Die heftigen Debatten um Softwarepatente in den USA, aufgekommen durch
die Patente für amazon.com, sollten Europa eine Mahnung sein. Wir
sollten nicht um jeden Preis alles von Amerika übernehmen.

#EeM: Europa wäre gut beraten, bei den Softwarepatenten an die meist klein-
und mittelständische Struktur seiner Softwareindustrie zu denken. Es
wirft kein gutes Licht auf das Gewicht die Bundesregierung, dass sie
sich nicht im Verwaltungsrat gegen die Softwarepatentierung
durchsetzen konnte. Die Märkte von morgen sind die Märkte von Ideen.
Die Gedanken, auch die zu Software geronnenen, sollten aus liberaler
Sicht weitestgehend frei bleiben.

#Ftu: Décision du Parti Libéraldémocrate Allemand 2001/05

#UdW: Sur leur convention de 2001/05 les Démocrates Libérales ont décidé une
resolution sur diverses questions de l'économie numérique.  Selon eux,
le logiciel doit continuer a faire objét du droit d'auteur seulement
et non du brevèt.

#UWi: UK E-Minister Patricia Hewitt 2001-03-12

#ShU: Software genießt heute weltweit umfassenden Schutz durch das
Urheberrecht. Damit ist den Herstellern und Programmierern ein starkes
absolutes Recht verliehen, um ihre Interessen umfassend gegenüber
Dritten wahrnehmen zu können. Indes kennt die US-amerikanische
Rechtsordnung auch die Patentierbarkeit von Software. Im Gegensatz zum
Urheberrecht schützt das Patent jedoch nicht das fertige Produkt,
sondern dehnt den Schutz auf die Methode oder gar ein
softwarebasierendes Geschäftsmodell aus. Die Entwicklung in den USA
zeigt schon heute deutlich, dass die Patentierung von Software sich
negativ auf die Entwicklung neuer Produkte und Geschäftsmodelle
auswirken kann. Denn einzelne Softwarepatente können im Bereich der
sogenannten Individualsoftware ganze Märkte blockieren.

#SeW: Sowohl nach deutschem Patentrecht als auch nach dem Europäischen
Patentübereinkommen sind Computerprogramme als solche derzeit nicht
patentierbar. Die FDP spricht sich dafür aus, an dieser Rechtslage im
Grundsatz festzuhalten.

#bay: UK Liberal Democrats IT Policy Motion

#gWr: The party's IT policy was ratified by party conference on Sunday
2003/03/16. During the debate several speakers either spoke
specifically against software patents or mentioned the issue in
passing, as did the proposer of the ratifying motion (who was the
chair of the policy working group).  The paper was published together
with a motion that calls for:

#tsa: supporting continued widespread innovation by resisting the wider
application of patents in this area.

#Mia: Dr. Martin Mayer

#meW: expert média du parti chétien-démocrate

#Sde: Statt der beabsichtigten generellen Ausdehnung des Patentschutzes für
Software in Europa muss ein zweijähriges Moratorium beschlossen
werden.

#Amt: Auf der 'Diplomatischen Konferenz 2000' der Europäischen Patentämter
ist vorgesehen, 'Programme für Datenverarbeitungsanlagen' aus der
Ausnahmevorschrift Art. 52(2), europäisches Patentübereinkommen, zu
streichen, und somit generell die Patentierung von Software zu
ermöglichen.

#IWg: In der Fachwelt gibt es gegen diese Absicht die berechtigte
Befürchtung, dass durch den Revisionsvorschlag

#Mqs: Monopolstellungen großer Softwarehäuser gestärkt und erweitert,

#kdr: kleine Softwareunternehmen und selbstständige Programmierer in ihrer
Existenz bedroht und

#iiW: insgesamt der Fortschritt in der Softwareentwicklung deutlich gebremst
würden.

#Esj: Eine derart verheerende Entwicklung, die sich in den USA schon jetzt
abzeichnet, darf in Europa nicht stattfinden. Deshalb muss vor einer
weiteren Rechtssetzung für den Schutz von Software eine gründliche,
öffentliche Diskussion von Fachwelt und Politik auf der Basis der
folgenden Grundsätze geführt werden.

#ZcW: Ziele des Rechtsschutzes für Software:

#Dbs: Der Rechtsschutz muss Programmierer und Unternehmen in die Lage
versetzen, die Früchte ihrer Arbeit zu ernten. Die finanzielle
Entlohnung, die sich nur über den Rechtsschutz verwirklichen lässt,
ist der wichtigste Anreiz für den Fortschritt in der
Softwareprogrammierung und Anwendung.

#Dsr: Der Rechtsschutz darf aber nicht zur Stärkung von weltbeherrschenden
Monopolen führen. Er muss den Wettbewerb fördern statt ihn zu
behindern.  Vor allem darf er keinesfalls kleine Softwareunternehmen
und selbstständige Programmierer benachteiligen und in ihrer Existenz
bedrohen.

#Mtt: Maßgeschneiderter Rechtsschutz für Software

#DWS: Das Urheberrecht wurde zum Schutz von künstlerischen und
schriftstellerischen Werken geschaffen. Es schützt auch
Computerprogramme in ihrer Eigenschaft als Sprachwerke. Allerdings
schützt das Urheberrecht die Software nur unzulänglich.

#Dse: Die Patente wurden im beginnenden Industriezeitalter zum Schutz
technischer Erfindungen eingeführt. In Ausgestaltung und Zeitdauer
tragen sie den Erfordernissen der Wissensgesellschaft nur unzureichend
Rechnung. 

#Sln: Software ist im Vergleich zu schriftstellerischen und künstlerischen
Werken und zu technischen Erfindungen etwas völlig Neues und
Andersartiges. Sie ist das elementare Hilfsmittel in der
Informationsgesellschaft und dringt in immer neue Bereiche vor. Daher
muss für sie ein eigenes, maßgeschneidertes Instrument des
Rechtsschutzes geschaffen werden.

#EsJ: YEPP/DEMYC 2000-10-28

#Rae: Resolution on Computer Software Patents Adopted by both %(YEPP) and
%(DEMYC) the YEPP Council Meeting 2000-10-28 in Helsinki under the
chairmanship of Michael Hahn (from Junge Union of Germany).

#Nct: Noting  that European Patent Office is planning to remove computer
programs from the list of the things which can not be patented of  in
its next conference in end of November

#Fnb: Further noting the still existing great amount of unsolved problems in
software patents like:

#Asr: Abstract-logical nature of software, which is in conflict with the
patent system's requirement for concreteness and physical substance

#Ige: Inefficient  long period of protection (20 years)

#Ttb: The lack of IT-expertise among the patent inspectors which would lead
too wide patent claims to be accepted as has happened in the United
States

#Top: The problem of anti-competitiveness; US software patents, if easily
transferable to EU, could substantially narrow competition in the
European Union, give dominant position to US corporations and have
negative influence among european IT start-ups

#Ttl: The negative effect of patents to the interoperability of computer
programs

#Toa2: Taking into consideration the strong opposition among small and medium
size companies towards software patents in Europe

#HcW: Having in mind the enormous economic importance of computer software
industry and its need for effective protection for its investments

#Yit: YEPP calls upon European political parties and the European Commission
(DG XV) to take immediate actions to delay all development on software
patents until the problems are correctly addressed

#Yee: YEPP further calls for taking actions to strengthen the European
software industry by improving the regulatory framework on combating
software piracy

#Bgo: MdB Wolfgang Wodarg

#Wna: Wodarg hat die %(ep:Eurolinux-Petition) wohl nicht zuletzt deshalb
unterzeichnet, weil er auf seinem Gebiet mit den Entgrenzungs-Taktiken
der Patentgesetzgeber aufs genaueste vertraut ist.  Im %(q:Deutschen
Ärzteblatt) vom Juli 2000 seziert er unter der Überschrift
``Schwammige Definitionen, moralische Lyrik'' die EU-Richtlinie
``Rechtlicher Schutz biotechnologischer Erfindungen''.  Darin schreibt
er u.a.:

#DWn: Der Text der EU-Richtlinie enthält vor allem im Vorspann und in den
Erwägungsgründen viel moralische Lyrik, mit der Kritiker
abgeschmettert und Parlamentariergewissen beschwichtigt werden können.
 Klare Grenzen und Definitionen fehlen.  Schwammige oder in sich
widersprüchliche Bestimmungen sorgen dafür, dass Rechtsnormen, wie sie
das 1977 in Kraft getretene Europäische Patentübereinkommen vorgibt,
systematisch ausgehöhlt werden können -- zum Beispiel das zentrale
Prinzip der ärztlichen Therapiefreiheit.  Nach Artikel 52,4 des
Übereinkommens dürfen diagnostische, therapeutische und chirurgische
Verfahren am menschlichen und tierischen Koerper nicht patentiert
werden.  Diesen Grundsatz beizubehalten, verspricht die Richtlinie in
Erwägungsgrund 35, jedenfalls im Hinblick auf die Verfahren als
ganzes.  Teilschritte von Verfahren können jedoch sehr wohl patentiert
werden.  Nach dem Motto: Keine Tür darf dem Arzt zum Wohle der
Patienten verschlossen sein, über die Nutzung der Türgriffe
entscheidet aber der Patentinhaber.

#Ele: Eine ähnliche juristische Spitzfindigkeit findet sich in dem für die
EU-Richtlinie zentralen Artikel 5.  Im ersten Abschnitt ist
festgelegt, dass der menschliche Körper in allen Entwicklungsstadien
nicht patentierbar ist, ebenso wenig wie die bloße Entdeckung eines
seiner Bestandteile (Beispiel Gene).  Im zweiten Abschnitt heisst es
jedoch, dass Teile, die mit Hilfe eines technischen Verfahrens
isoliert wurden, sehr wohl patentierbar sind.  Wer sich mit der
Regelung näher befasst, wie der Nationale Ethikrat der Dänen oder
Kritiker in Frankreich, kommt nicht umhin festzustellen, dass bei
Genen und Teilsequenzen von Genen die vermeintliche Ausnahme die Regel
ist.  Gene liegen immer isoliert vor, wenn sie entschlüsselt werden.

#Hes: Herta Däubler-Gmelin, Bundesministerin der Justiz

#Mkg: Mit der Vergabe der 20-jährigen Monopole auf die Nutzung von
Software-Ideen, so Däubler-Gmelin, seien erhebliche Probleme für die
Ökonomie und die Sicherheit der Informationsgesellschaft verbunden,
die ``erst einmal gründlich und breit diskutiert werden müssen''.

#MuW: Man könne schließlich eine solch zentrale Frage der Wirtschaftspolitik
%(q:nicht von Malta und Liechtenstein bestimmen lassen), erklärt ein
Ministerialer. Notfalls könne man auch das Abkommen insgesamt platzen
lassen und innerhalb der EU eine eigene Patentpolitik entwickeln.

#Jfk: Justizministerin Prof. Dr. Herta Däubler-Gmelin im März 2001

#DWW: Dies berichtete die %(CZ) von einem Interview, welches sie auf der
CeBit mit Frau D.G. führte.  Leider wird die Ministerin nicht wörtlich
zitiert, und die Aussage, Software sei %(q:als Teil eines technischen
Verfahrens patentierbar), ist mehrdeutig und steht auf den ersten
Blick im Widerspruch zum %(ep:Gesetz).  Worin die %(q:vehemente)
Kritik bestehen soll, bleibt schleierhaft.  Diese Aussage ist
möglicherweise als ein politisches Zugeständnis zustande gekommen: die
Bundesregierung hatte sich von den EU-Patentjuristen von der
Dienststelle für den Gewerblichen Rechtschutz in Bolkesteins
%(gb:Generaldirektion Binnenmarkt) vereinnahmen lassen, indem sie
ihren Vertreter bei einer entscheidenden Sitzung fehlen ließ und
dennoch nicht protestierte, als die EU-Patentjuristen eine Zustimmung
der Bundesregierung in ihrem Protokoll festhielten.  Mit diesem
Interview wollte DG offenbar Ausgleich schaffen, nachdem MdB Tauss
protestiert hatte.  Das Signal war aber wohl zu schwach und
missverständlich, um Bolkesteins Patentjuristen zu bremsen, wie wir
heute wissen.

#Vtu

#lWh

#OWn

#SWa

#WWl2

#tle

#Aon

#WuW

#Toe: Le numéro 5 de la revue Think Magazine 1990 contient un article 

#Yde

#JnW: Josef Straus: Les brevets : une arme des pays développés

#Pei: Le Professeur Straus, membre des comités consultatifs de l'OMPI 

#Dgn: Comme dans le système de libéralisation du commerce mondial la 

#Mee

#Aoe

#tai

#sfq

#Pae2

#BWg

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatcusku.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatcusku ;
# txtlang: xx ;
# End: ;

