<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

##? EINFÜGUNG phm 2003-11-12: please translate this
#title: Zitate zu Softwarepatenten

##? ÄNDERUNG phm 2003-11-12: Pay attention to the wording
#descr: Einschlägige Zitate aus Rechtstexten, wirtschaftwissenschaftlichen
Analysen, politischen Beschlüssen und Aussagen von Programmierern,
Informatikern, Wissenschaftlern, Politikern und anderen
Persönlichkeiten des öffentlichen Lebens.

#aii: The party's IT policy which includes a paragraph opposing swpat, was
ratified by party conference on Sunday 2003/03/16. During the debate
several speakers either spoke specifically against software patents or
mentioned the issue in passing, as did the proposer of the ratifying
motion (who was the chair of the policy working group).  The paper was
published together with a motion that calls for %(q:supporting
continued widespread innovation by resisting the wider application of
patents in this area.)

#ExW: Europarechtler Lenz exzerpiert und kommentiert Texte aus aktuellen
Studien und Anhörungen, die für die Gegner von Softwarepatenten von
Interesse sind

#Con: Kapitel 6 warnt, dass Entwicklungsländer sich bei der Festlegung ihrer
Patentpolitik insbesondere im Hinblick auf Gene oder Software nicht an
US oder EU orientieren sollen.  Stattdessen empfehlen die
Wissenschaftler einen Ansatz wie den des Art 52 EPÜ, der Software und
Geschäftsmethoden explizit ausschließt.  Die Studie wurde von der
britischen Regierung bestellt

#Pos: Phil Karn, an software developper working for Qualcomm, a company that
lives largely on patent licenses, finds that the patent system is,
apart from bringing some revenues to companies like Qualcomm, doing
little good for the software industry as a whole.

#AiW: Eine Sammlung von Aussagen zahlreicher europäischer Politiker,
IT-Manager und Intellektueller zur Unterstützung der
Eurolinux-Position.

#SCS: Der Gründer von Lotus erklärt auf der Anhörung im US-Patentamt 1994,
warum Patente für die Softwarebranche schlecht sind.

#Dyh: Douglas Brotz, Leiter der Forschung bei Adobe Systems, erklärt auf der
Anhörung des US-Patentamtes 1994, warum Patente schlecht für die
Softwarebranche sind.

#Ich: Autodesk ist Weltführer, manche sagen Monopolist, im Bereich der
CAD-Software.  Vorstandsmitglied Jim Warren wurde als Pionier der
Softwareentwicklung bekannt, gründete das legendäre Dr. Dobb's
Journal.  In seiner Zeugenaussage auf der Anhörung des US-Patentamtes
1994 erklärt Warren leidenschaftlich und wortgewaltig, warum die
Patentanwälte mit ihren Versuchen, Algorithmen unter die
patentierbaren Erfindunge einzuordnen, die Wahrheit, die Interessen
der Softwarebranche und die Verfassung der Vereinigten Staaten mit
Füßen treten.

#Ria: Robert Kohn, Chefentwickler bei Borland, erklärt auf der Anhörung 1994
im US-Patentamt, dass seine Firma nicht breitere Eigentumsrechte
sondern wirksame Durchsetzung des bereits vorhandenen Urheberrecht
braucht.

#SWl: PA Stephan Kinsella über Geistiges Eigentum

#AWi2: Ein Patentanwalt kritisiert das Patentsystem und die Vorhaltungen der
Patentanwälte

#Ihe: In einer Ansprache erklärt Lessig die Geschichte der informationellen
Ausschlussrechte in den USA und zeigt auf, wie Ansprüche aus der
Vergangenheit zunehmend die Zukunft einschnüren und die Gesellschaft
dadurch unfreier und unproduktiver wird.  Er geht auch auf
Logikpatente ein und zitiert dabei u.a. Bill Gates als Kronzeugen.

#BWn: Microsoft-Broschüre %(q:Überzeugen Sie durch Argumente: Windows vs
Linux)

#ohW: Microsoft schreibt auf Seite 5: %(bc:Ein Problem von Open Source
stellen so genannte Software-Patente dar. Ein prominentes Beispiel
sind die Rechte an dem Dateiformat JPEG. Die entwickelnde Firma hatte
einst die Lizenzen frei vergeben, um eine weite Verbreitung des
Formats zu erreichen.  Der Käufer dieser Firma fordert nun Gebühren
von kommerziellen Anwendern. Dies stellt, wenn man so will, eine
Zeitbombe dar.)

#LWw: LPF Irlam: Quotes on Software Patents

#Cih: Auszüge aus Sudien über die Wirkungen des Patentwesens auf den
Software-Bereich und die Volkswirtschaft im allgemeinen.  Zeigt, dass
das Patentsystem in vielen Bereichen seit langem kaputt ist. 
Gesammelt von Gordom Irlam und veröffentlicht von der Liga für die
ProgrammierFreiheit.

#CWn: Irlam 1994: Quotes on Software Patents

#VrF: Eine Variante der LPF-Sammlung, die Gordon Irlam an ein
e-post-basiertes Forum schickte.

#Lne: Lang: Citations sur les Brevets Logiciels

#Coe: Englische und französische Zitate über %(q:Geistiges Eigentum) und die
Informationsallmende, gesammelt von Bernard Lang.

#Sei: Informatiker, Programmierer und Ingenieure

#Rej: Rechtsgelehrte, Richter

#Wfk: Wirtschaftswissenschaftler

#Msr: Mathematiker, Geisteswissenschaftler, Generalisten

#Pik: Politiker

#Ptt: Patentstrategen

#Mte: Meine Einführung in die Realitäten des Patentwesens

#AWf: Ein berühmter amerikanischer Anwalt erinnert sich.

#Mne: Meine eigene Einführung in die Realitäten des Patentsystems kam in den
1980er Jahren, als mein Mandant, Sun Microsystems - damals eine kleine
Firma - von IBM einer Patentverletzung beschuldigt wurde. Unter
Androhung eines massiven Gerichtsverfahrens verlangte IBM ein Meeting,
um seine Ansprüche darzulegen. Vierzehn IBM-Anwälte und ihre
Assistenten, alle in die obligaten blauen Anzüge gekleidet, drängten
sich in den größten Konferenzraum, den Sun hatte.

#Tri: Der oberste blaue Anzug dirigierte die Präsentation von sieben
Patenten, die nach IBM's Behauptung verletzt wurden. Das prominenteste
darunter war IBM's berüchtigtes %(q:Fette-Linien-Patent): Um eine
dünne Linie auf einem Computerbildschirm zu einer breiten Linie zu
machen, geht man von den Enden der dünnen Linie jeweils die gleiche
Strecke aufwärts und abwärts und verbindet dann die vier Punkte. Sie
haben diese Technik zur Umwandlung einer Linie in ein Rechteck
vermutlich im Geometrieunterricht der siebten Klasse gelernt, und
zweifellos glauben sie, sie sei von Euklid oder irgendeinem 3000 Jahre
alten Denker erfunden worden. Die Prüfer der US-Patentbehörde waren
anderer Ansicht und erteilten IBM ein Patent für das Verfahren.

#AsW: Nach IBMs Präsentation waren wir dran. Unter den unbewegten Augen der
Big-Blue-Truppe begaben sich meine Kollegen -- von denen jeder
einzelne sowohl einen Ingenieurs- wie einen juristischen Abschluß
hatte -- an die Tafel und illustrierten, sezierten und zerstörten
methodisch IBM's Behauptungen. Wir sagten %(q:Sie machen wohl Witze!)
und %(q:Sie sollten sich schämen.) Aber das IBM Team zeigte keine
Emotion - bis auf unverhohlene Indifferenz. Voller Zuversicht kamen
wir zu unserer Schlußfolgerung: Nur eins der sieben IBM-Patente würde
vor Gericht als gültig anerkannt werden und selbst bei diesem würde
kein rational denkendes Gericht eine Verletzung durch Suns Technologie
feststellen.

#AnO: Ein peinliches Schweigen folgte. Die blauen Anzüge berieten sich nicht
einmal untereinander. Sie saßen nur da, versteinert. Schließlich
antwortete der oberste blaue Anzug. %(q:OK,) sagte er, %(q:vielleicht
verletzen sie diese sieben Patente nicht. Aber wir haben 10.000
US-Patente. Wollen Sie wirklich, dass wir nach
Armonk[IBM-Hauptquartier in New York] zurückgehen und sieben Patente
finden, die Sie tatsächlich verletzen? Oder wollen Sie es sich leicht
machen und uns einfach 20 Millionen Dollar zahlen?) Sun verhandelte
ein kleines bißchen, dann wurde IBM ein Scheck ausgestellt, und die
blauen Anzüge zogen weiter zur nächsten Firma auf Ihrer Abschußliste.

#IWt: In Amerikas Wirtschaft wiederholen sich Beutezüge dieser Art jede
Woche. Das Patent als Anreiz für Erfindungen hat schon lange Platz
gemacht für das Patent als Instrument zum unverblümten Abwürgen von
Innovation.

#HSi: Schnelle Einführung

#AWa: Um den Lesern verstehen zu helfen, weshalb die SoftPat Direktive der
Europäischen Kommission so umstritten ist, beginnen wir die Analyse
hier mit einem einfachen Szenario.

#ItW: Stellen Sie sich vor Sie besitzen eine kleine Softwarefirma.  Sie
haben eine starkes Softwareprodukt entwickelt.  Dieses Werk ist eine
schöpferische Kombination von 1000 Rechenregeln (Algorithmen) und
einer Menge Daten.  Die Rechenregeln fanden Ihre Programmierer nach
jeweils ein paar Minuten oder Stunden Nachdenken.  Die Entwicklung des
gesamten Werkes erforderte 20 Mannjahre.  900 der Rechenregeln waren
vor 20 Jahren schon bekannt.  50 sind heute patentiert.  Sie besitzen
3 dieser Patente.  Um sich die 3 Rechenregeln zu schützen, sind Sie
zum Patentamt geeilt, haben ihre Geschäftsstrategie offenbart und
Anwaltskosten bezahlt.  IBM und Microsoft setzen derweil Ihre Ideen in
klingende Münze um und weisen Sie darauf hin, dass Sie ca 20-30 von
50000 Patenten aus deren Portfolio verletzen.  Sie einigen sich
gütlich:  3% Ihrer jährlichen Einnahmen gehen an IBM, 2% an Microsoft,
2% %(dots).  Dennoch erreichen Sie bald die Gewinnzone.  Jetzt sind
sie eine attraktive Firma.  Eine Patentagentur wendet sich an Sie. 
Sie verletzen 2-3 von deren Patenten, heißt es.  Die Ansprüche sind
sehr breit.  Die Agentur will 100.000 EUR.  Eine gerichtliche Klärung
könnte 10 Jahre dauern und 1 Million EUR kosten.  Sie zahlen.  Einen
Monat später steht die nächste Patentagentur auf der Matte %(dots)
Bald sind Sie pleite.  Sie suchen Schutz.  Microsoft bietet an, Sie
für einen symbolischen Preis zu kaufen.  Sie akzeptieren.  Unter einem
reinen Urheberrechtssystem wären Sie jetzt unabhängig und reich. 
Mithilfe von Patenten ist es Microsoft und anderen gelungen, Ihr
geistiges Eigentum zu stehlen.

#SoW: Das Software-Feld hat viel mit anderen technischen Wissenschaften
gemein. Aber es gibt einen fundamentalen Unterschied:
Computerprogramme werden aus idealen mathematischen Objekten
aufgebaut. Ein Programm tut immer genau das, was es sagt. Man kann ein
Schloß in den Wolken bauen, das von einer Linie der Breite Null
gestützt wird, und es wird oben bleiben.

#Ptw: Reale Geräte sind nicht so vorhersagbar, weil physikalische
Gegenstände ihre Eigenheiten haben. Wenn ein Programm sagt, es zähle
die Zahlen von eins bis tausend, dann wird es genau das tun. Wenn man
so einen Zähler als Maschine zusammenbaut, kann es sein, daß ein
Riemen verrutscht und die Zahl 58 zweimal gezählt wird oder daß
draußen ein LKW vorbeifährt und 572 übersprungen wird. Diese Probleme
machen es sehr schwierig, zuverlässige Maschinen zu entwerfen.

#Wst: Wenn wir Programmierer eine while-Schleife innerhalb einer
if-Bedingung verwenden, müssen wir uns keine Gedanken darüber machen,
ob die while-Schleife solange durchlaufen wird, daß die if-Bedingung
durchbrennt oder daß sie gegen die if-Bedingung stößt und sie abnutzt.
Wir müssen uns keine Sorgen machen, daß sie bei der falschen
Geschwindigkeit vibriert und das if in Resonanz gerät und zerbricht.
Wir müssen nicht daran denken, wie man einen kaputten Befehl ersetzen
könnte. Oder ob das if genügend Strom an das while liefern kann ohne
einen Spannungsabfall zu erzeugen. Es gibt viele Schwierigkeiten beim
Hardware-Design, mit denen wir uns nicht herumschlagen müssen.

#Tsa: Die Folge ist, daß Software pro Bestandteil viel einfacher zu
entwerfen ist als Hardware. Das ist der Grund, weshalb Designer heute
wo immer möglich Software statt Hardware benutzen. Das ist auch der
Grund, weshalb selbst kleine Teams von nur wenigen Leuten oft
Programme von gewaltiger Komplexität entwickeln.

#Pnt: Gelegentlich fragen mich naive Menschen: %(q:Wenn Dein Programm
innovativ ist, wirst Du dann nicht ein Patent beantragen?) Diese Frage
geht davon aus, daß ein Produkt mit einem Patent einhergeht.

#Iwr: In manchen Branchen, wie z.B. der pharmazeutischen Industrie,
funktionieren Patente oft auf diese Weise. Software stellt das andere
Extrem dar: ein typisches Patent deckt viele unterschiedliche
Programme ab und selbst ein innovatives Programm verletzt mit großer
Wahrscheinlichkeit viele Patente.

#Teo: Das liegt daran, daß umfangreiche Programme eine große Zahl
verschiedener Techniken kombinieren und viele Features implementieren
müssen. Selbst wenn einige davon neue Erfindungen sind, bleiben viele
übrig, die es nicht sind. Alle Techniken und Features, die noch keine
zwanzig Jahre alt sind, sind mit großer Wahrscheinlichkeit schon von
jemand anderem patentiert worden. Ob sie es wirklich sind, ist reine
Glückssache.

#InW2: Ich habe erläutert, inwiefern Patente der Fortschritt behindern.
Befördern sie ihn auch?

#PtW: Patente ermuntern möglicherweise einige Leute, nach neuen,
patentierbaren Ideen zu suchen. Das ist keine große Hilfe, da wir auch
ohne Patente viel Innovation hatten. %(pe:Man braucht nur die
Zeitschriften und Anzeigen von 1980 anzuschauen, um das zu sehen.) 
Neue Ideen sind nicht der begrenzende Faktor in unserem Feld. Im
Software-Bereich besteht die schwierige Aufgabe im Entwickeln großer
Systeme.

#Pee: Leute, die Systeme entwickeln haben ab und zu neue Ideen. Natürlich
verwenden sie diese Ideen. Bevor es Software-Patente gab,
veröffentlichten sie die Ideen auch -- der Anerkennung wegen. So lange
viel Software entwickelt wird, werden wir einen steten Strom neuer
veröffentlichter Ideen haben.

#Tdr: Das Patent-System behindert die Entwicklung. Es führt dazu, daß wir
bei jeder Design-Entscheidung fragen: %(q:Werden wir verklagt werden?)
 Und die Antwort ist Glückssache. Das führt zu kostspieligerer und zu
weniger Entwicklung.

#WWt: Wenn weniger Software entwickelt wird, werden Programmierer dabei
weniger Ideen haben. Patente können also die Zahl der patentierbaren
Ideen, die veröffentlicht werden, reduzieren.

#Adu: Vor zehn Jahren hat die Software-Branche ohne Patente funktioniert.
Ohne Patente sind solche Innovationen wie Fenster, virtuelle Realität,
Tabellenkalkulation oder Netzwerke produziert worden. Und eben weil es
keine Patente gab, konnten Programmierer Software entwickeln, die
diese Innovationen benutzte.

#Whc: Wir haben nicht um die Veränderung gebeten, die uns oktroyiert wurde. 
Es gibt keinen Zweifel, daß Software-Patente uns die Hände binden.
Wenn es kein klares und lebenswichtiges Bedürfnis der Öffentlichkeit
gibt, uns in Bürokratie zu verstricken, dann bindet uns los und laßt
und zurück an die Arbeit gehen!

#crr: %(PH), Vorstand von %(ILOG)

#MoW: http://www.ilog.com/corporate/members/executive.cfm?Printout=Yes

#Lao: Software ähnelt mehr der Mathematik %(pe:nicht patentierbar) als der
Chemie %(pe:die häufig als Erfolgserfahrung des Patentwesens angeführt
wird).

#L1e: Die amerikanische Erfahrung mit Softwarepatenten ist katastrophal. 
Bevor man sie nachahmt, sollte man daran denken, die Amerikaner von
ihrem System abzubringen.

#Pae: Dazu müsste man eine Lobbygruppe organisieren, die bei den großen
amerikanischen Firmen vorstellig wird.

#LWt: Die europäischen Softwarefirmen leben lieber mit der Notwendigkeit,
ständig ihre Produkte verbessern zu müssen, als mit der Notwendigkeit,
ständig Patente anmelden, Wettbewerber verklagen und sich selbst wegen
möglicher unbeabsichtigter Patentverletzungen ängstigen zu müssen.

#Lsc: Das Problem der Freien Software ist ein orthogonales Problem (ein
anderer Schauplatz).  Es wäre durchaus denkbar, freie Software zu
schreiben und vor ihrer Veröffentlichung Patente anzumelden, um so ein
undurchdringliches Gestrüpp rechtlicher Probleme zu erzeugen.

#L0j: Das Argument, dass Software-Neugründer (IT-Startups) ohne Patente
nicht an Risikokapital herankämen, ist eine Lüge.  Ich habe noch nie
solche Fälle kennengelernt.

#Mti: My observation is that patents have not been a positive force in
stimulating innovation at Cisco. Competition has been the motivator;
bringing new products to market in a timely manner is critical. 
Everything we have done to create new products would have been done
even if we could not obtain patents on the innovations and inventions
contained in these products. I know this because no one has ever asked
me %(q:can we patent this?) before deciding whether to invest time and
resources into product development.

#Tne: The time and money we spend on patent filings, prosecution, and
maintenance, litigation and licensing could be better spent on product
development and research leading to more innovation.  But we are
filing hundreds of patents each year for reasons unrelated to
promoting or protecting innovation.

#Mea: Moreover, stockpiling patents does not really solve the problem of
unintentional patent infringement through independent development.  If
we are accused of infringement by a patent holder who does not make
and sell products, or who sells in much smaller volume than we do, our
patents do not have sufficient value to the other party to deter a
lawsuit or reduce the amount of money demanded by the other company. 
Thus, rather than rewarding innovation, the patent system penalizes
innovative companies who successfully bring new products to the
marketplace and it subsidizes or  rewards those who fail to do so.

#alcatel02: Alcatel 2002: Patente erschweren Produktentwicklung

#erW: In einem Bericht aus dem Jahre 2002 klagt der französische
Telekomgigant über die von Patenten verursachte Rechtsunsicherheit:

#eWa: Like other companies operating in the telecommunications industry, we
experience frequent litigation regarding patent and other intellectual
property rights. Third parties have asserted, and in the future may
assert, claims against us alleging that we infringe their intellectual
property rights. Defending these claims may be expensive and divert
the efforts of our management and technical personnel. If we do not
succeed in defending these claims, we could be required to expend
significant resources to develop non-infringing technology or to
obtain licenses to the technology that is the subject of the
litigation. In addition, third parties may attempt to appropriate the
confidential information and proprietary technologies and processes
used in our business, which we may be unable to prevent.

#uao: Our business and results of operations will be harmed if we are unable
to acquire licenses for third party technologies on reasonable terms.

#pde: We remain dependent in part on third party license agreements which
enable us to use third party technology to develop or produce our
products. However, we cannot be certain that any such licenses will be
available to us on commercially reasonably terms, if at all.

#B0f: Bradford Friedman (Cadence) 2002:  general animosity to software
patents in the industry

#BoW: Bradford L. Friedman, Director of Intellectual Property, Cadence
Design Systems, Inc.

#Aod: As I'm sure this committee is aware, there is a general animosity to
pure software patents within and outside of the industry due to, one,
the perceived allowance of what I'll diplomatically call overbroad
patent claims, and two, the historically non-proprietary culture of
the software engineering industry.

#IWW3: In sum, largely because the current patent system is poorly fashioned
for the software design tool industry, the industry has evolved to
minimize the impact that patents have on competition and has relied on
other more market-oriented drivers of innovation. I believe this is a
missed opportunity for accelerating technological and economic growth
in the industry.

#GWr: Greenhall (Divx) 2022: 35% of R&D funds diverted to sabre rattling

#Ret: R. Jordan Greenhall, Chief Executive Officer, Divx Networks, explained
at the FTC hearings how wasteful the patent process has become in the
software field.

#AWe: As a small company, one of the biggest risks I face is uncertainty in
the marketplace. I can minimize my risk by understanding my
competitor's products very well, by understanding my products very
well, by understanding what the consumers and customers want. But I've
found in the past year that I really can't understand the patent
landscape and that I'm sitting with a nuclear bomb on top of my
products that could go off at any point and cause me to simply not
have a business anymore.

#InW: I recently took one of my lead developers, a gentleman who's widely
considered a leader in his field -- he sits on both the MPEG and the
ITU committees, is deeply involved with the entire intellectual
property landscape around digital video --  and asked him to evaluate
a particular patent that we've  been hearing about in the marketplace.

#Wun: We did a quick search on the USPTO website, which by the way is very
useful, and uncovered no less than 120 patents that claim to be within
the general scope of this particular patent, which was widely cited.

#TnW2: The poor guy spent the better part of five days examining all these
different patents and came back to me saying, %(q:I haven't the
slightest idea whether or not we infringe on these patents, and
frankly, they all seem to infringe on one another.)

#Trl: The end result being that I have no idea whether my product infringes
on upwards of 120 different patents, all of which are held by large
companies who could sue me without thinking about it.

#Tse: The end result, much like Borland, I have now issued a directive that
we reallocate roughly 20 to 35 percent of our developer's resources
and sign on two separate law firms to increase our patent portfolio to
be able to engage in the patent spew conflict. I think the concept
here would be called saber rattling. I need to be able to say,
%(q:Yeah, I've got that patented too, so go away and leave me alone.)

#DnA: Douglas Brotz (Adobe) 1994

#S9n: At the USPTO hearings of 1994, Adobe's representative said:

#LWo: Let me make my position on the patentability of software clear. I
believe that software per se should not be allowed patent protection. 
I take this position as the creator of software and as the beneficiary
of the rewards that innovative software can bring in the marketplace. 
I do not take this position because I or my company are eager to steal
the ideas of others in our industry.  Adobe has built its business by
creating new markets with new software.  We take this position because
it is the best policy for maintaining a healthy software industry,
where innovation can prosper.

#Fhn: For example, when we at Adobe founded a company on the concept of
software to revolutionize the world of printing, we believed that
there was no possibility of patenting our work. That belief did not
stop us from creating that software, nor did it deter the savvy
venture capitalists who helped us with the early investment. We have
done very well despite our having no patents on our original work.

#Oed: On the other hand, the emergence in recent years of patents on
software has hurt Adobe and the industry. A %(q:patent litigation tax)
is one impediment to our financial health that our industry can
ill-afford.  Resources that could have been used to further innovation
have been diverted to the patent problem. Engineers and scientists
such as myself who could have been creating new software instead are
working on analyzing patents, applying for patents and preparing
defenses.  Revenues are being sunk into legal costs instead of into
research and development. It is clear to me that the Constitutional
mandate to promote progress in the useful arts is not served by the
issuance of patents on software.

#Jpo: Joshua Kaplan (Intouch) 2002

#Wfe: Auf den FTC-Anhörungen von 2002 erklärte Kaplan:

#Ikn: Intouch is an e-business company that owns many patents and has been
attacked with patents, including by Amazon.  Joshua Kaplan is their
president and CEO.  These excerpts are from his statement at the FTC
hearings of 2002:

#Tls: There are patents that come out today with hundreds of claims,
unintelligible to almost anyone except the people who drew them. And
yet, people who violate them jeopardize sometimes a lifetime of
investment or their division or their product. That system doesn't
work well to spur innovation or carry out the constitutional mandate.

#Iid: Indeed, for those of you who were here this morning and listened to
the people in the software industry talk about how threatening this is
to their businesses, as I see it, patents today are often entrenching
the established at the expense of allowing the newcomer to come in. I
question today whether a Steve Jobs could start an Apple or a Bill
Gates could start a Microsoft in view of the web and thicket of
patents that is out there.

#OWr: Oracle 1994

#toW: Aus Oracles Stellungnahme im Rahmen der Anhörung zu Software-Patenten
im US Patentamt 1994

#OWW: Oracle spricht sich gegen die Patentierbarkeit von Software aus. Die
Firma ist der Meinung, daß das bestehende Urheberrecht und und der
vorhandene Schutz von Geschäftsgeheimnissen, im Gegensatz zum
Patentrecht, besser geeignet sind, die Entwicklung von
Computersoftware zu schützen.

#Plm: Das Patentrecht bietet Erfindern ein ausschließliches Recht an einer
neuen Technologie im Austausch für die Veröffentlichung der
Technologie. Das ist in Branchen wie der Software-Entwicklung nicht
angemessen, in der Innovation sehr schnell erfolgt, mit geringem
Kapitaleinsatz möglich ist und oft aus kreativer Kombination bekannter
Techniken besteht.

#UWs: Leider ist Oracle dazu gezwungen, sich im Rahmen einer defensiven
Strategie durch das selektive Beantragen solcher Patente zu schützen,
die die besten Aussichten für gegenseitige Lizenzabkommen mit
Unternehmen, die uns Patentverletzungen vorwerfen, bieten.

#Auu: Although all that I %(q:invented) were innovative, all utilized
complex procedures and all were valued by those who paid millions to
use what my innovative entrepreneurial risk created, it never occurred
to me to patent them, and I could not have patented those %(q:useful
arts) if I had wanted to.

#TWW: The fundamental question is: Do we want to permit the monopoly
possession of everything that works like logical intellectual
processes. I hope not.

#Tam: The mind has always been sacrosanct. The claim that intellectual
processes and logical procedures (that do not primarily manipulate
devices) can be possessed and monopolized extends greed and avarice
much too far. Algorithmic intellectual processes must remain
unpatentable -- even when represented by binary coding in a computer;
even when executed by the successor to the calculator.

#Wnw: What frightens and infuriates so many of us about software patents is
that they seek to monopolize our intellectual processes when their
representation and performance is aided by a machine.

#EWl: Everything that is represented or performed by software is first a
completely-detailed algorithmic intellectual process. There are no
exceptions, other than by error.

#TWp: Thus, I respectfully object to the title for these hearings --
%(q:Software-Related Inventions) -- since you are not primarily
concerned with gadgets that are controlled by software. The title
illustrates an inappropriate and seriously-misleading bias. In fact,
in more than a quarter-century as a computer professional and observer
and writer in this industry, I don't recall ever hearing or reading
such a phrase -- except in the context of legalistic claims for
monopoly, where the claimants were trying to twist the tradition of
patenting devices in order to monopolize the execution of intellectual
processes.

#TWn: There is absolutely no evidence, whatsoever -- not a single iota --
that software patents have promoted or will promote progress.

#TkW: The company for which I am speaking, Autodesk, holds some number of
software patents and has applied for others -- which, of course,
remain secret under current U.S. law. However, all are defensive -- an
infuriating waste of our technical talent and financial resources,
made necessary only by the lawyer's invention of software patents.

#Ara: Autodesk has faced at least 17 baseless patent claims made against it
and has spent over a million dollars defending itself, with millions
more certain to pour down the bottomless patent pit unless we halt
this debacle. Fortunately -- unlike smaller software producers -- we
have the financial and technical resources to rebuff such claims.  We
rebutted all but one of the claims, even before the patent-holders
could file frivolous law-suits, and will litigate the remaining claim
to conclusion. Note that your Office has issued at least 16 patents
that we have successfully rebutted, and we never paid a penny in these
attempted extortions that your Office assisted.

#Beb: But it was an enormous waste of resources that could have better been
invested in useful innovation. These unending baseless claims benefit
patent lawyers, but they certainly do not promote progress.

#Wso: We offer two recommendations, the second having twelve parts -- so to
speak, the 12 Apostles of Redress:

#Foe: FIRST: Issue a finding that software, as I have defined it, implements
intellectual processes that have no physical incarnation; processes
that are exclusively analytical, intellectual, logical and algorithmic
in nature. Use this finding plus the clearly-stated Constitutional
intent, to declare that the Patent Office acted in error when it
granted software patents. Declare that software patents monopolize
intellectual and algorithmic processes, and also fail to fulfill the
Constitutional mandate to promote progress -- that in fact, they
clearly threaten it.

#Sdt: SECOND: Until -- and only until -- software patents are definitively
prohibited, reject or at least freeze all such applications that have
not yet been granted, pending conclusive action on all of the
following twelve recommendations:

#RtW: REDRESS SERIOUS ERRORS OF PREVIOUS ADMINISTRATIONS: Issue a finding
that there have been extensive and serious errors of judgment in a
large percentage of software patents granted in the past, and
immediately recall all software patents for re-review and possible
revocation.

#Leh: Let us stand on each others' shoulders, rather than on each others'
toes.

#Mft: Mitch Kapor 1994

#Bhb: Because it is impossible to know what patent applications are in the
application pipeline, it is entirely possible, even likely, to develop
software which incorporates features that are the subject of another
firm's patent application.  Thus, there is no avoiding the risk of
inadvertently finding oneself being accused of a patent infringement
simply because no information was publicly available at the time which
could have offered guidance of what to avoid.

#Tor: The period of patent protection, 17 years, no longer makes sense in an
era when an entire generation of technology passes within a few years.

#IiW: If some future litigant is successful in upholding rights to one of
these %(q:bad) patents, it will require expensive and time-consuming
litigation, whose outcome is frankly uncertain, to defend the rights
of creators which should never have been challenged in the first
place.

#BEf: Bill Gates 1991:  Patents exclude competitors, lead industry to
standstill

#T9o: This was quoted by Fred Warshofsky in %(q:The Patent Wars) of 1994. 
The text is from an internal memo written by Bill Gates to his staff. 
Part of has appeared in another %(cc:Gates memos).

#IWe: If people had understood how patents would be granted when most of
today's ideas were invented and had taken out patents, the industry
would be at a complete standstill today.  ... The solution is
patenting as much as we can. A future startup with no patents of its
own will be forced to pay whatever price the giants choose to impose. 
That price might be high. Established companies have an interest in
excluding future competitors.

#rGW: Microsoft Germany 2003

#aea: linux_partner_brosch.pdf

#HmO: Håkon Wium Lie, CTO of Opera

#Tlu: The norwegian software comany Opera Inc develops a web browser which
is well known for its stability, compactness and speed.  Due to its
position as a quality leader, Opera also develops the multimedia
software that is used in Nokia's mobile phones.  Opera Software
supports the Eurolinux campaign for a software patent free Europe. 
Their CTO Håkon Wium Lie published the following statement at the W3C
at the occasion of whether fee-based (RAND) or only royalty-free (RF)
standards should be accepted by W3C in early 2002:

#Ofr: Opera Software's position in the RF/RAND debate is that the
fundamental standards for the Web must continue to be royalty free
(RF). Therefore, we do not think W3C should describe procedures for
RAND licensing.  Doing so would help legitimize software patents which
we think are harmful to the development of the Web. Also, software
patents is largely an American concept not recognized in other parts
of the world.

#LoW: Linus Torvalds 2002/08

#Lhm: Linux VM hackers are engaged in ongoing discussions on both large page
support (covered last week) and improving the performance of the new
reverse mapping mechanism. That conversation slowed down, however,
when Alan Cox pointed out that a number of the techniques being
discussed are covered by patents. In fact, a closer look by Daniel
Phillips shows that a number of existing Linux methods, including
reverse mapping in general and the buddy allocator, are covered by
these patents. This is a problem, he said, that we can't ignore.  That
was Linus's cue to jump in with his policy on software patents and
kernel code.  He later conceded that this was not %(q:legally tenable
advice) but the only way to keep developping the kernel without going
nuts.

#Ins: I do not look up any patents on %(e:principle), because (a) it's a
horrible waste of time and (b) I don't want to know.

#Thy: The fact is, technical people are better off not looking at patents.
If you don't know what they cover and where they are, you won't be
knowingly infringing on them. If somebody sues you, you change the
algorithm or you just hire a hit-man to whack the stupid git.

#TFa: %(AL), Autor von %(VD), einem Werkzeug zur Umwandlung von Ton- und
Bild-Datenformaten, hat traurige Nachrichten zu verkünden:

#TFa2: Today I received a polite phone call from a fellow at Microsoft who
works in the Windows Media group. He informed me that Microsoft has
intellectual property rights on the ASF format and told me that,
although the implementation was still illegal since it infringed on
Microsoft patents. I have asked for the specific patent numbers, since
I find patenting a file format a bit strange. At his request, and much
to my own sadness, I have removed support for ASF in VirtualDub 1.3d,
since I cannot risk a legal confrontation.)

#Woy: Tord Jansson (SE): Corporations controlling my work thanks to legal
uncertainty introduced by EPO

#dat: In early 2003, Tord Jansson, developper of a streaming software called
BladeEnc, wrote to a member of the European Parliament:

#eml: I'm a professional software developer who early summer 1998 wrote a
computer program that I decided to put on my homepage. The program
turned out to be a tremendous success and was quickly distributed in
millions of copies, obviously filling a need among many computer
users. I quickly started to improve my program and release new
versions. That same autumn I was contacted by a large company with a
competing product, who claimed that my program infringed on certain
patents they had been granted. Consulting SEPTO gave no reason to take
infringement claims seriously since computer programs are not
patentable as such, but in early 1999 my legal advisor explained that
the legal uncertainty lately introduced by EPO would perhaps make the
claims valid. That eventually forced me to stop making my program
available.

#atc: Do you believe a corporation should have the right to control what
computer programs I can write and publish?

#raf: Marcel Martin (FR): Ich musste dieses Projekt einstellen ...

#oWt: Oberthur Card System applied in 1999 for a patent on a method of
geometry (point-halving in elliptic curves).  In Oct 2001, the
Oberthur's legal department sent a cease-and-desist letter to Marcel
Martin, French informatics student and author of the shareware library
HIT, in which it asked him to %(q:immediately stop marketing your
product).  Which he did, although the legal status of Oberthur's
patent claims particularly in Europe is very unclear. Martin explains:

#edw: Ich musste dieses Projekt einstellen, denn ich kann nicht eine Armee
von Anwälten bezahlen, um mich jedesmal zu verteidigen, wenn Oberthur
oder jemand anders mir mir neue Bedingungen für das Weitermachen
diktieren will.  Entwickler reagieren sehr empfindlich auf
juristischen Terror dieser Art.  Wenn Softwarepatente in Europa
legalisiert werden, wird dies zwangsläufig zu einer weiteren
Drosselung der Softwareproduktion führen.

#Mnt: %(MV) 1998 in %(LDI)

#Lkm: Ist Software nun schließlich patentierbar?

#SuW: Zweifellos noch nicht.

#Eee: In Wirklichkeit sind die Gesetzesregeln des Übereinkommens und der
nationalen Gesetze klar:  Sie fordern unmissverständlich die
Nicht-Patentierbarkeit von Software.  Das Spiel, das heute gespielt
wird, besteht darin, in einer oder der anderen Weise diese Regeln zu
verdrehen, z.B. indem man sich, wie oben beschrieben, die Gesamtheit
aus Hardware und Software als eine virtuelle Maschine denkt, die
(künftig ...) patentierbar sein könnte.  Unter dieser Voraussetzung
kann man dann patentrechtlich argumentieren.  Die auf diese Weise auf
dem einen oder anderen Wege erhältlichen Patente haben allerdings nur
denjenigen Wert, den man ihnen beimisst --- oder der sich durch einen
Konsens ergibt, dieser Frage nicht genauer nachgehen zu wollen. 
Tatsächlich kann die Verdrehung der Gesetzesregeln nur insoweit
Wirkung entfalten, wie sich ein Konsens darüber herstellen lässt, ob
man dieses Spiel gegen die bestehenden Gesetzesregeln spielen soll
oder nicht.  Hierbei handelt es sich nicht mehr um eine juristische
Frage im strengen Sinne.

#Soi: Auf diesem Gebiet müssen wir uns an der Entwicklung der geschriebenen
Regeln orientieren (im Sinne einer etwaigen Streichung von
Patentierungsausschlüssen).

#Bro: Bundegerichtshof 1976

#Sai: Stets ist aber die planmäßige Benutzung beherrschbarer Naturkräfte als
unabdingbare Voraussetzung für die Bejahung des technischen Charakters
einer Erfindung bezeichnet worden.  Wie dargelegt, würde die
Einbeziehung menschlicher Verstandeskräfte als solcher in den Kreis
der Naturkräfte, deren Benutzung zur Schaffung einer Neuerung den
technischen Charakter derselben begründen, zur Folge haben, dass
schlechthin allen Ergebnissen menschlicher Gedankentätigkeit, sofern
sie nur eine Anweisung zum planmäßigen Handeln darstellen und kausal
übersehbar sind, technische Bedeutung zugesprochen werden müsste. 
Damit würde aber der Begriff des Technischen praktisch aufgegeben,
würde Leistungen der menschlichen Verstandestätigkeit der Schutz des
Patentrechts eröffnet, deren Wesen und Begrenzung nicht zu erkennen
und übersehen ist.

#Eii: Es ließe sich ferner mit guten Gründen die Auffassung vertreten, dass
angesichts der Einhelligkeit, mit der Rechtsprechung und Literatur
seit jeher die Beschränkung des Patentschutzes auf technische
Erfindungen vertreten haben, von einem gewohnheitsrechtlichen Satz
dieses Inhalts gesprochen werden kann.

#Dnu: Das mag aber letztlich dahinstehen.  Denn der Begriff der Technik
erscheint auch sachlich als das einzig brauchbare Abgrenzungskriterium
gegenüber andersartigen geistigen Leistungen des Menschen, für die ein
Patentschutz weder vorgesehen noch geeignet ist.  Würde man diese
Grenzziehung aufgeben, dann gäbe es beispielsweise keine sichere
Möglichkeit mehr, patentierbare Leistungen von solchen zu
unterscheiden, denen nach dem Willen des Gesetzgebers andere Arten des
Leistungsschutzes, insbesondere Urheberrechtsschutz, zuteil werden
soll.  Das System des deutschen gewerblichen und Urheberrechtsschutzes
beruht aber wesentlich darauf, dass für bestimmte Arten geistiger
Leistungen je unterschiedliche, ihnen besonders angepasste
Schutzbestimmungen gelten und dass Überschneidungen zwischen diesen
verschiedenen Leistungsschutzrechten nach Möglichkeit ausgeschlossen
sein sollten.  Das Patentgesetz ist auch nicht als ein Auffangbecken
gedacht, in welchem alle etwa sonst nicht gesetzlich begünstigten
geistigen Leistungen Schutz finden sollten.  Es ist vielmehr als ein
Spezialgesetz für den Schutz eines umgrenzten Kreises geistiger
Leistungen, eben der technischen, erlassen und stets auch als solches
verstanden und angewendet worden.

#Ene: Es verbietet sich demnach, den Schutz von geistigen Leistungen auf dem
Weg über eine Erweiterung der Grenzen des Technischen -- die auf deren
Aufgabe hinauslaufen würde -- zu erlangen.  Es muss vielmehr dabei
verbleiben, dass eine reine Organisations- und Rechenregel, deren
einzige Beziehung zum Reich der Technik in ihrer Benutzbarkeit für den
bestimmungsgemäßen Betrieb einer bekannten Datenverarbeitungsanlage
besteht, keinen Patentschutz verdient.  Ob ihr auf andere Weise, etwa
mit Hilfe des Urheber- oder des Wettbewerbsrechts, Schutz zuteil
werden kann, ist hier nicht zu erörtern.

#Beh: Bundesgerichtshof 1980

#Dzg: Der BGH erklärt, warum eine neue Regel zur Optimierung der
Stahlverwendung in einem Walzwerk keine technische Erfindung ist.

#Wdg: /swpat/papiere/bgh-dispo76/index.de.html

#GKW: Gert Kolle 1977

#GdW: Gert Kolle, heute Abteilungsleiter im EPA, war in den 70er Jahren der
führende Rechtsdogmatiker in Fragen der Begrenzung der
Patentierbarkeit im Hinblick auf informatische Geistesleistungen. 
Auch heute gibt es wenige Texte, die die Problematik klarer darlegen
als Kolles meistzitierter Artikel von 1977.  Wir zitieren eine
Passage, die belegt, wie gut bekannt die Thematik bereits damals war.

#Wmr: Die Automatische Datenverarbeitung (ADV) ist heute zu einem
unentbehrlichen Hilfsmittel in allen Bereichen der menschlichen
Gesellschaft geworden und wird dies auch in Zukunft bleiben.  Sie ist
ubiquitär.  ...  Ihre instrumendale Bedeutung, ihre Hilfs- und
Dienstleistungsfunktion unterscheidet die ADV von den ...
Einzelgebieten der Technik und ordnet sie eher solchen Bereichen zu
wie z.B. der Betriebswirtschaft, deren Arbeitsergebnisse und Methoden
... von allen Wirtschaftsunternehmen benötigt werden und für die daher
prima facie ein Freihaltungsbedürfnis indiziert ist.

#EPf: European Patent Office 1978

#Ilt: /swpat/analyse/epue52/index.de.html

#Aso: Programme für Datenverarbeitungsanlagen können verschiedene Formen
haben, beispielsweise Algorithmen, Flussdiagramme oder Serien
codierter Befehle, die auf einem Band oder anderen maschinenlesbaren
Aufzeichnungsträgern gespeichert werden können; sie können als
Sonderfall entweder für eine %(e:mathematische Methode) oder eine
%(e:Wiedergabe von Informationen) betrachtet werden.  Wenn der Beitrag
zum bisherigen Stand der Technik lediglich in einem Programm für
Datenverarbeitungsanlagen besteht, ist der Gegenstand nicht
patentierbar, unabhängig davon, in welcher Form er in den Ansprüchen
dargelegt ist.  So wäre z.B. ein Patentanspruch für eine
Datenverarbeitungsanlage, die dadurch gekennzeichnet ist, dass das
besondere Programm in ihr gespeichert ist, oder für ein Verfahren zum
Betrieb einer durch dieses Programm gesteuerten
Datenverarbeitungsanlage in Steuerabhängigkeit von diesem Programm
ebenso zu beanstanden wie ein Patentanspruch für das Programm als
solches oder für das auf Magnettonband aufgenommene Programm.

#IWn: Bei der Prüfung der Frage, ob eine Erfindung vorliegt, muss der Prüfer
%(dots) die Form oder die Art des Patentanspruchs außer acht lassen
und sich auf den Inhalt konzentrieren, um festzustellen, welchen neuen
Beitrag die beanspruchte angebliche %(qc:Erfindung) zum Stand der
Technik leistet.  Stellt dieser Beitrag keine Erfindung dar, so liegt
kein patentierbarer Gegenstand vor.  Dieser Sachverhalt ist durch
[obige] Beispiele anhand verschiedener Wege zur Beanspruchung eines
Programms für eine Datenverarbeitungsanlage erläutert.

#RLt: Prof. Dr. iur. Rudolf Kraßer 1986

#Tmr: In diesem Standard-Lehrwerk wird die dem Art 52 EPÜ und der deutschen
Rechtsprechung zugrundeliegende Gesetzessystematik erklärt.  Zum
Abschluss wird auf den Druck einiger Patentjuristen und Anmelder
berichtet, die die Grenzen auf dem Wege einer Rechtsfortbildung
aufweichen möchten, und es wird erklärt, warum dies rechtswidrig ist. 
Leider nahm das EPA hierauf keine Rücksicht.  Es begann 1986 mit dem
Rechtsbruch.  Der BGH zog 1992 nach.

#1en: 1. Die Begrenzung des Patentschutzes auf das Gebiet der Technik hat
zur Folge, dass wesentliche geistige Leistungen von hohem
wirtschaftlichem Wert unberücksichtigt bleiben.

#SpW: So pflegen das Erarbeiten des Lösungsprinzips, das einem
Computerprogramm zugrundeliegt, seine Umsetzung in Programmvorstufe
wie Ablaufplan oder Datenflussplan und schließlich das
maschinenlesbare Programm selbst beträchtlichen Aufwand zu erfordern. 
Die geistigen Leistungen, die dabei erbracht werden, sind gewiss nicht
generell geringer als bei vielen patentwürdigen technischen
Erfindungen.  Wirtschaftlich ist Datenverarbeitungs-Software oft von
bedeutendem Wert; ...

#Das: Der BGH lehnt es freilich ab, auf das Erfordernis des technischen
Charakters zu verzichten.

#Dnn: Durch Ausdehnung des Technikbegriffs den Anwendungsbereich des
Patentschutzes zu erweitern, hält der BGH ebenfalls nicht für richtig.

#ArW: Nationaler Forschungsrat der USA 2000

#Ien: Im Falle seiner Lockerung werde Schritt für Schritt allen Lehren für
verstandesmäßige Tätigkeit der Patentschutz eröffnet; hiergegen
bestünden Bedenken insbesondere wegen der Freihaltebedürfnisse in
Bezug auf Arbeitsergebnisse und Methoden der Betriebswirtschaft (für
Management, Organisation, Rechnungswesen, Finanzierung, Werbung,
Marketing usw.), die von allen Wirtschaftsunternehmen benötigt würden,
und auf Algorithmen, wie sie Computerprogrammen zugrundeliegen.

#Aeo: Auf der anderen Seite wird eine Überprüfung des Ausschlusses
nichttechnischer Handlungsanweisungen verlangt, da er vorwiegend
historisch bedingt und nicht mehr zeitgemäß sei.

#2nt: 2. Für das %(s:geltende Recht) wird es bei der vom BGH bekräftigten
Begrenzung des Patentschutzes auf technische Erfindungen sein Bewenden
haben müssen.  Neben der ausdrücklichen Regelung in §1 Abs 2 PatG und
Art 52 Abs 2 EPÜ erlaubt es auch die bestehende institutionelle und
organisatorische Ausgestaltung des Patentwesens, die durchweg auf das
Gebiet der Technik zugeschnitten ist, nicht, hierüber durch
richterliche Rechtsfortbildung hinauszugehen.

#Eht: Eine %(s:gesetzgeberische Fortentwicklung) hätte jedenfalls daran
festzuhalten, dass Ausschlussrechte von nicht übersehbarer Tragweite
vermieden werden müssen; mindestens Entdeckungen, wissenschaftlichen
Theorien und mathematischen Methoden müsste deshalb der Patentschutz
verschlossen bleiben.

#3iz: 3. Dagegen ist im Bereich der %(s:nichttechnischen
Handlungsanweisungen) und der %(s:Informationsvermittlung) nicht
notwendigerweise der mögliche Anwendungsbereich von Neuerungen so
breit, dass die Reichweite eines Ausschlussrechts unkalkulierbar
werden muss.  Ob sich die Zulassung zum Patentschutz empfiehlt, hängt
davon ab, wie die Schutz- und Belohnungsinteressen im Vergleich zu den
Freihaltebedürfnissen zu werten sind.

#Tkt: Tendenziell dürfte hierbei das Freihaltebedürfnis schwerer wiegen als
bei technischen Erfindungen, weil Ausschlussrechte an Neuerungen, die
der Mensch ohne Einsatz von Naturkräften benutzen kann, weniger als
solche an technischen Erfindungen durch außerhalb seiner selbst
liegende Objekte definiert sind und deshalb regelmäßig unmittelbarer
und stärker in seine Handlungsfreiheit eingreifen.  Insbesondere wären
von Ausschlussrechten an kommerziellen Neuerungen erhebliche
wettbewerbsbeschränkende Effekte zu befürchten.

#Kee: Kein hinreichendes Argument für die Gewährung von Patentschutz ist der
Umstand, dass die geistige Leistung, der sie zugutekommen soll,
anderweitig nicht oder nicht umfassend geschützt ist.  Vielmehr können
die Grenzen des nach geltendem Recht erreichbaren Schutzes auch
Anzeichen dafür sein, dass den Freihaltungsinteressen der Vorrang
gebührt.

#Pio: Karl Friedrich Lenz unterzieht die Argumentation des EPA in seinen
Entscheidungen von 1998 einer Kritik nach den üblichen Methoden der
Gesetzesauslegung: grammatisch-lexikalische, systematische,
historische, teleologische und verfassungskonforme Auslegung.  In
allen Punkten urteilt das EPA gesetzeswidrig.

#DWx: Dies ist so weit vom Wortlaut entfernt, dass eine Bestrafung aufgrund
eines infolge dieser gesetzwidrigen Auslegung erteilten Patentes mit
dem Gesetzlichkeitsprinzip (Art. 103 Absatz 2 des Grundgesetzes) in
Widerspruch steht. Es ist eine völlige Neuformulierung der Schranke in
Absatz 3, die mit dem Wortlaut des Gesetzes nichts mehr gemein hat.
Die technische Beschwerdekammer überschreitet damit klar die Grenzen
richterlicher Tätigkeit. Wer die Formulierung %(q:als solche) durch
die Formulierung %(q:ohne technischen Charakter) ersetzen möchte, muss
dies durch eine entsprechende Änderung des Vertragstextes nach den
dafür erforderlichen Verfahren bewirken. Die Rechtsprechung kann dies
nicht.

#soa: Die Behauptung, die Beschränkung des Ausschlusses von Software von der
Patentierbarkeit auf Software als solche in Absatz 3 habe den Zweck,
im Lichte der Entwicklung der Informationstechnik durch Anerkennung
der Patentierbarkeit den technischen Fortschritt zu fördern, überzeugt
nicht. Falls der Gesetzgeber einen solchen Zweck verfolgt haben
sollte, hätte er den Ausschluss in Absatz 2 von vornherein nicht
vorgesehen. Die Unterstellung eines mit dem gewünschten Ergebnis
übereinstimmenden Gesetzeszweckes ist zwar keine korrekte Anwendung
der teleologischen Auslegungsmethode, zeigt aber deutlich die
Bereitschaft der technischen Beschwerdekammer, die eigenen Wertungen
an die Stelle der Wertung des Gesetzgebers zu setzen.

#biw: Preambule to Nordic Patent Law of 1963

#oae: Finland, Sweden, Norway and Denmark had a joint patent system before
they joined the European Patent Convention.  Their patent law included
a statement about the technical invention which closely resembles the
Dispositionsprogramm doctrine, thereby showing that it is not an
invention of german lawcourts:

#oiu: The definition of the concept of invention, which is
constitutionalized in the Nordic countries, contains the requirement
that the invention must have a %(it:technical character).  An exact
definition of what this means can hardly be given, but within the
concept lies definitely a requirement that an invention must be a
solution of a problem by means of natural forces, i.e. by means of a
causally determined use of natural matter and energy.

#Tgi: Dieser Seufzer zeigt, wie lange das Patentsystem schon lange außer
Kontrolle ist.

#SbW2: Stuber 1907

#AeW: Argumentation gegen die Heranziehung ökonomischer Regeln zur
Bestimmung des Geltungsbereichs des Patentsystems.

#Has: Wie weit gesetzlicher Schutz in verschiedenen Bereichen der Industrie
gehen sollte, ist primär eine Frage für Juristen.

#JKl: J. Kohler

#Aig: After jurisprudence has taken hold of any area treated by the law, it
is up to science to develop it and all the other disciplines must
resign; from now on it is the method of judicial thinking which must
rule.

#PaW: Patente und industrieller Fortschritt, Gesetze und damit verbundene
Probleme

#Qne: Überprüfung der Qualifikation von Walter Hamilton, über das Ziel von
Patenten zu schreiben.

#WiW: Was sind das für Qualifikationen?  Ist er Anwalt?  Hat er jemals als
Jurist praktiziert?  Besitzt er einen juristischen Titel? ...
Professor Hamilton ... war vor seiner Professur an der Yale Law School
Professor der Wirtschaftswissenschaften ... Scheinbar ist eine
befriedigende Antwort nicht möglich ... soweit es die vorangestellten
Fragen zu seiner Qualifikation betrifft, als Experte zum Zweck von
Patenten und des Patentsystems zu sprechen.

#Kee2: PA Stephan Kinsella 2002

#Shf: Stephan Kinsella ist eingetragener Patentanwalt und bestreitet seinen
Lebensunterhalt mit der Hilfe für andere Personen beim Erwerb von
Patenten.  Er ist ein scharfer Kritiker des Patentsystems und seiner
Kollegen.  Als einer dieser Kollegen den Jura-Professor der
Universität Stanford, Lawrence Lessig als einen %(q:Patentpolitik
betreibenden, prunkvollen Pädagogen) bezeichnete, kommentierte
Kinsella:

#IVl: Meiner Ansicht nach fallen tatsächlich die meisten Patentanwälte
--grundsätzlich alle Anwälte-- in die Kategorie des %(q:Patentpolitik
betreibenden, prunkvollen Pädagogen), mit der Folge, dass sie
gedankenlos Thesen 'pro' Patente von sich geben. Dies kommt daher,
dass die meisten Patent-, aber auch andere Anwälte mit Interessen in
diesem Bereich, ohne Verstand das Papageienartig wiederholen, was
ihnen im Jurastudium an drastisch vereinfachten wirtschaftlichen
Grundregeln eingetrichtert wurde. Nahezu jeder Patentanwalt wiederholt
gebetsmühlenartig das Mantra, dass %(q:wir Patente bräuchten, um den
Fortschritt zu stimulieren,) so als ob sie sorgfältig darüber
nachgedacht hätten. Natürlich hat das fast keiner getan. Sie
wiederholen das, was sie in den Werken des Supreme Court und CAFC
(Court of Appeals for the Federal Circuit, dem obersten Bundesgericht
für Patentrechtsfragen) lasen, als ob die Meinung der
Regierungsfunktionäre die Heilige Schrift schlechthin wäre. Man muss
kein Genie sein um herauszufinden, warum die meisten Patentanwälte das
Patentsystem gutheissen; und es ist nicht, weil sie sich mit der
Materie beschäftigt hätten und zu dem unparteiischen Schluss gelangt
wären, diese Gesellschaft wäre mit einem Patentsystem besser dran -- 
sie wollen nur nicht an dem System kratzen, das ihnen ihr Einkommen
sichert.

#hrP: Dr. Matthew Lee 1994: Kräfte hinter den Änderungen der EPA-Praxis nach
1985

#ahj: A UK-educated barrister in Honkong specialising in computing law at
Hongkong University analyses the history of patent examination
guideline revisions at the EPO.  Lee starts by exaggerating the
restrictive character of the EPO's first examination guidelines of
1978: %(bc:The implication of this approach would be to severely
narrow the scope of patentability for software-related inventions.
Inventive process control %(tp|mechanisms|e.g. those used in a
conventional chemical plant to produce new polymers) that would
otherwise be standard patent material would fall outside the scope of
patentable subject-matter simply because a program was used in
implementing the inventive process control scheme.)  This, according
to Lee, apparently did not disturb the chemical industry as much as
certain other customers of the EPO:

#tei: However, in response to pressure from the computer industry and trends
emerging in the US, the European Patent Office reviewed its guidelines
in 1985.

#rsm: Bundespatentgericht 2002: Die Doktrin vom Technischen Beitrag macht
alle Geschäftsmethoden Patentierbar

#rfa: The 17th senate of Germany's Federal Patent Court explains how the EPO
doctrine of %(q:technical problem) or %(q:technical contribution)
works and how it leads to unlimited patentability and is therefore
incompatible with Art 52 EPC:

#bpatg17: Würde Computerimplementierungen von nichttechnischen Verfahren schon
deshalb technischer Charakter zugestanden, weil sie jeweils
unterschiedliche spezifische Eigenschaften zeigen, etwa weniger
Rechenzeit oder weniger Speicherplatz benötigen, so hätte dies zur
Konsequenz dass jeglicher Computerimplementierung technischer
Charakter zuzubilligen wäre. Denn jedes andersartige Verfahren zeigt
bei seiner Implementierung andersartige Eigenschaften, erweist sich
entweder als besonders rechenzeitsparend oder als
speicherplatzsparend. Diese Eigenschaften beruhen - jedenfalls im
vorliegenden Fall - nicht auf einer technischen Leistung, sondern sind
durch das gewählte nichttechnische Verfahren vorgegeben. Würde schon
das Erfüllen einer solchen Aufgabenstellung den technischen Charakter
einer Computerimplementierung begründen, so wäre jeder Implementierung
eines nichttechnischen Verfahrens Patentierbarkeit zuzubilligen; dies
aber liefe der Folgerung des Bundesgerichtshofs zuwider, dass das
gesetzliche Patentierungsverbot für Computerprogramme verbiete,
jedwede in computergerechte Anweisungen gekleidete Lehre als
patentierbar zu erachten.

#eei: /swpat/papiere/eubsa-swpat0202/index.de.html

#2nW: Schäfers 2003: Patentprüfung in Bioinformatik nicht mehr leistbar

#EWW: In einem Brief an den Europa-Abgeordneten Dr. Joachim Wuermeling
schreibt Harald Schäfers, Leiter einer Rechtsanwaltskanzlei für IT-
und Wirtschaftsrecht:

#edp: Wir machen uns - insbesondere auch im Namen unserer Mandanten
(IT-Unternehmen) - Sorgen wegen der zur Entscheidung anstehenden
Patentrichtlinie. Wir beraten hauptsächlich kleinere
Softwareunternehmen.

#Wer: Erst kürzlich hatten wir eine Patentkonkurrenz prüfen (einen
eventuellen Konflikt mit einem konkreten US-Patent) lassen und wir
schalteten dafür eine hochspezialisierte, deutsche
Patentanwaltskanzlei ein.

#bes: Die Diagnose dieser Kanzlei (für ca. 4000,00 Euro netto) war: Für die
Prüfung, ob eine Verletzung des gezielt benannten US-Patents vorliegt
verlangt ein US-amerikanischer Patentanwalt 25.000,00 USD Vorschuss. 
Schlimmer aber ist, dass nach deren Prognose die Aussage des
amerikanischen Kollegen nicht lauten wird: %(q:ja - Patentverletzung)
oder %(q:nein - keine Patentverletzung).   Die Antwort wird - nach der
Prognose der deutschen Spezialisten sein -: %(q:Möglicherweise),
%(q:Es ist nicht auszuschließen, dass ...). Damit soll nicht
mangelhafte Ausbildung der Kollegen gerügt werden, denn schon die
deutschen Spezialisten erkannten: Recherchen, geschweige denn:
Prüfungen, sind zumindest im Bereich der Bioinformatiksoftwarepatente
oft nicht mehr wirklich leistbar.

#0pW: PS.BE 2002: Software Copyrightable, not Patentable

#toe: Auf S. 110 steht zu lesen:

#0Ea: Henkel 2002/12/29: Warum die EU Patent auf Software verbieten sollte

#Wmh: In den VDI-nachrichten schreibt Dr. Joachim Henkel vom Institut für
Innovationsforschung der Ludwig Maximilians Universität München unter
dem Titel %(bc:Zu viel Schutz schadet nur -- Wirtschaft: Warum die EU
Patente auf Software verbieten sollte).  Die Redaktion fasst zusammen
%(bc:Ende dieses Jahres entscheidet die EU-Kommission über die
Patentierbarkeit von Software. Dr. Joachim Henkel vom Institut für
Innovationsforschung der Uni München plädiert im folgenden Beitrag
gegen Patente. Sein Urteil: Mehr Schutz ist innovationsfeindlich und
schadet dem Mittelstand.)

#goi: Highly tentative conclusions that can be made based on the literature
suggest that stronger patent rights may create substantial problems in
the telecommunication sector. First, strong patent rights may cause
%(q:patent portfolio race). In other words, companies may use patents
primarily not to protect their technological invention itself but as
instruments with which to trade in order to be able to negotiate
access to external technologies. Given the observed entry deterrence
strategies of the incumbents, stronger patent rights might provide
with them new powerful weapons to defend monopolistic market
positions.

#uno: Thus, stronger patent rights may hinder the development of effective
competition in the telecommunication markets. Patentability of
principles or ideas might further result in strategic patenting
against compatibility. This could be particularly lethal to the
content industry and further to the markets of the future generations
of cellular mobile telephones and services. Currently avail able
empirical evidence does not allow us to make definite conclusions.
However, it suggests that strengthening of patent rights in the
communication sector or extending patent protection to cover
intellectual property currently protected by copyright involves great
potential risks.

#TWe: The patent system fits best a model of progress where the patented
product, which can be developed for sale to consumers, is the discrete
outcome of a linear research process. The safety razor and the
ballpoint pen are examples, and new drugs also share some of these
characteristics.  By contrast in many industries, and in particular
those that are knowledge-based, the process of innovation may be
cumulative, and iterative, drawing on a range of prior inventions
invented independently, and feeding into further independent research
processes by others. ... The development of software is very much a
case of building incrementally on what exists already. Indeed, the
Open Source Software Movement depends precisely on this characteristic
to involve a network of independent programmers in iterative software
development on the basis of returning the improved product to the
common pool.

#DWr: Developed and developing countries have historically provided that
certain things do not constitute inventions for the purpose of patent
protection. Included in these are those set out, for example, in
Article 52 of the European Patent Convention (EPC):

#Edo: Even though subsequent EPO practice and jurisprudence have to some
extent diluted the scope of these Articles,13 it would seem entirely
reasonable for most developing countries to adopt this list of
exclusions as a minimum.

#Snt: Summary of Recommendations Relating to the Patent System

#Eim: Exclude from patentability computer programs and business methods

#Rra: Rechtlicher Ausgangspunkt für die Betrachtungen ist das Europäische
Patentübereinkommen (EPÜ). Es wurde 1973 in München unterzeichnet und
zählt 20 Vertragsstaaten. Europäische Patente werden generell für
Erfindungen erteilt, die neu sind, auf erfinderischer Tätigkeit
beruhen und gewerblich anwendbar sind. Expressis verbis ausgeschlossen
sind gemäß Art. 52 Abs. 2 lit. c EPÜ jedoch %(q:Programme für
Datenverarbeitungsanlagen). Dennoch hat das Europäische Patentamt
(EPA) vielfach Patente für technische Erfindungen erteilt, bei denen
ein Computerprogramm verwendet wird. Das EPA geht nämlich davon aus,
dass durch das EPÜ nicht alle Computerprogramme von der
Patentierbarkeit ausgeschlossen sind und verweist auf den
%(fn:3:technischen Charakter) einer Erfindung als wesentliche
Voraussetzung für ihre Patentierbarkeit. Es unterscheidet folglich
zwischen patentfähigen und nicht patentfähigen Computerprogrammen nach
dem Kriterium der Technizität. Demnach ist ein rein abstraktes Werk
ohne technischen Charakter nicht patentfähig; es muss ein zusätzlicher
technischer Effekt (Mehreffekt) vorliegen. Das EPA schafft damit einen
zusätzlichen Ausschlusstatbestand, der sich aus Wortlaut und
Systematik des Art. 52 EPÜ nicht ergibt und nur mit einem
Zirkelschluss begründet werden kann. Da der technische Charakter einer
Erfindung Voraussetzung für ihre Patentierbarkeit ist und das EPÜ ein
Patentierungsverbot für Computerprogramme enthält, muss es nach dem
Verständnis des EPA Computerprogrammen ohne zusätzlichen technischen
Effekt an der Technizität mangeln. Diese Auslegung zäumt das Pferd von
hinten auf und ist mit dem Wortlaut des Art. 52 EPÜ nicht vereinbar.

#AWh: Aus ökonomischer Sicht ist die Sinnhaftigkeit eines Patentschutzes
danach zu beurteilen, ob dieser effiziente Anreize für die Investition
in Forschung und Entwicklung setzt. Immaterielle Güter zeichnen sich
dadurch aus, dass sie beliebig und kostenlos reproduzierbar sind und
der Konsum von Wissen durch eine Person eine andere Person nicht daran
hindert, dieses Wissen ebenfalls zu konsumieren. Der Einzelne wird
deshalb nur so viel in die Produktion von Wissen investieren, wie er
durch seinen eigenen Konsum rechtfertigen kann. Dies führt insgesamt
zu einer ineffizient niedrigen Produktion von Wissen. Deshalb müssen
Anreize, z.B. durch Patente, geschaffen werden, die die
kostenintensive Produktion von Wissen in einem darüber hinausgehenden
Maß bewirken.  Ökonomisch gesehen wird bei einem Patent ein
(ineffizientes) Monopol vorübergehend gewährt, um Produktionsanreize
zu setzen. Das soeben dargestellte Modell ist weitgehend statisch.
Kompliziertere Szenarien haben demgegenüber gezeigt, dass eine
Verstärkung des Patentschutzes keineswegs zwingend zu einem vermehrten
Forschungsaufwand führt. Im Gegensatz zur allgemeinen Annahme, dass
weitgehender immaterialgüterrechtlicher Schutz zu höherer
Investitionstätigkeit führt, vermochten %(fn:6:Untersuchungen, die
unter ähnlichen Bedingungen wie die der Softwareindustrie operiert),
eine generelle Zunahme der Ausgaben für Forschung und Entwicklung
nicht %(fn:7:nachzuweisen). Empirische Studien über das Verhalten von
kleinen und mittleren Unternehmen im Softwarebereich haben gezeigt,
dass Patente für diese zu den am wenigsten effizienten Methoden des
Investitionsschutzes zählen.

#Vnz: Vor diesem Hintergrund ist eine Ausdehnung des Patentschutzes auf
Computerprogramme kritisch zu bewerten. Die Entwicklung von Software
ist generell nicht kapitalintensiv, außerdem bestehen auf diesen
Märkten bereits Netzeffekte, die wiederum Konzentrationstendenzen
begünstigen. Durch Softwarepatente werden insbesondere für kleine und
mittlere Unternehmen erhebliche Marktbarrieren entstehen. 
Open-Source-Softwareprodukte stellen ex definitione den Programmcode
allen Interessierten zur Verfügung und könnten Patentschutz deshalb
generell nicht in Anspruch nehmen. Die mit dem Patentschutz verbundene
vorübergehende Monopolstellung eines Unternehmens ist geeignet, die
Konzentrationstendenzen auf dem Markt für Softwareprodukte weiter zu
verstärken und den Wettbewerb zu behindern.

#WaW: Innovationen im Software-Bereich bauen, mehr als in anderen Branchen,
auf vorhergehenden Innovationen auf. Aus diesem Grunde wiegt der
negative Aspekt von Patenten, weiterführende Entwicklungen zu
erschweren, bei Software besonders schwer. Lizenzierungen lösen dieses
Problem aufgrund der mit ihnen verbundenen Transaktionskosten nur sehr
bedingt.

#SSa: Seth Shulman 2000

#Jee: Nur weil Software noch kein virtuelles Bhopal erlebt hat, heisst das
nicht, es könne nicht so kommen. Tatsächlich erneuern die giftigen
Wolken der Restriktion, die durch die Hallen des e-Commerce ziehen,
alte Änste vor der Industrie. ... Es gibt reichlich historische Belege
dafür, wie ein Übermass an Patenten den Entwicklungsprozess neuer,
aufstrebender Industrien enorm gebremst hat.

#Tel: Die Folgen dieser grundlegenden de facto-Ausweitung des Patentrechts
auf Erfindungen der Informationstechnik sind zum jetzigen Zeitpunkt
völlig unklar. Da diese Ausweitung ohne jeden Weitblick des
Gesetzgebers stattfand und da das Patentrecht hier völlig neue Ufer
betritt, wäre es angebracht dieses Phänomen zu untersuchen um
sicherzustellen, dass diese Ausweitung der Patentierbarkeit der
Wissenschaft und Kultur positive Impulse vermittelt, wie vom Kongress
beabsichtigt.

#Tno: Hierfür müssen viele Ursachen in Betracht gezogen werden. ...

#Ene2: Economic Effects of the Australian Patent System:  A Commissioned
Report to the Industrial Property Advisory Committee

#Tsn: In diesem 1982 herausgegebenen Bericht finden sich Statistiken über
die Nutzung das Patentsystems als Informations- und Ertragsquelle. Er
stellt das gesamte Patentsystem an sich in Frage. Der Originalentwurf
empfahl die komplette Abschaffung des Systems. Ein späterer Entwurf
dagegen empfahl stattdessen die Erhöhung der Standards zur
Patentierbarkeit und die Beschränkung verschiedener
Missbrauchsmöglichkeiten.

#SbW: Da die Vorteile des Patentsystems so geringfügig sind und das
Gesamtergebnis einer Kosten/Nutzenrechnung negativ sein dürfte, gibt
es keine ökonomische Rechtfertigung für die Ausweitung der
Patentmonopole durch eine Erhöhung der Laufzeit oder Erweiterung der
Basis für Patentierbarkeit oder Patentverletzung (zB: Patente auf
Pflanzen oder Software).  Wie auch immer, nach unserer Ansicht gibt es
hinreichend ökonomische Gründe nach bestehender Rechtsordnung, die
negativen Folgen des Patentsystems durch striktere Prüfung, die
Verkürzung der Laufzeit und Wirkungsbreite des Patentschutzes sowie
verschärfte Handhabung von unerwünschten Praktiken bei der
Patentlizenzierung einzudämmen.

#AlW: An historical awareness of the political economy of patent reform
suggests that this task is not easy at the domestic policy level. This
is basically because those who perceive they would lose by such reform
are concentrated, powerful and active defenders of their interests. 
In contrast, those who would gain by patent reform are diffuse and
hardly aware of their interest in the matter.

#CWr: Canadian Department of Consumer and Corporate Affairs 1976

#Ieh: In a working Paper on Patent Law Revision the department finds that
the patent system is doing more harm than good to the economy and
recommends abolishing it.

#Ois: On the basis of the review and analysis contained in this first part
of the working paper it is evident that Canada should give serious
consideration to the possibility of abandoning the continued
maintenance of a patent system in any form.

#FMp: Fritz Machlup 1958

#Toa: The patent system was introduced in Germany in 1873 through a lobbying
effort of lawyers and protectionists who used the %(q:me too)
argument: other countries have it so we must too.  Most economists of
the time were opposed to the patent system.  Machlup's report to the
US congress contains a long account of the activities and writings of
this period.  This statement appears near its end.

#BRw: Bis zum Jahre 1873 war die Patentfrage heißumstrittenes Thema. Die
Volkswirte hatten ihren Standpunkt mit Nachdruck vertreten, eifrig
bemüht, die Öffentlichkeit und die Regierung zu überzeugen. Die
Niederlage der Patentgegner - die in Regierungskreisen von vielen als
ein Sieg der Juristen und anderer »Protektionisten« über die Mehrheit
der Nationalökonomen angesehen wurde - veränderte den Charakter der
volkswirtschaftlichen Erörterungen und Stellungnahmen zum Patentwesen.
Die Flut von Büchern, Flugschriften und Artikeln über die
wirtschaftlichen Grundlagen des Patentschutzes nahm ein Ende, die
Nationalökonomen hatten das Interesse an der Patentfrage verloren und
wandten sich anderen Problemen zu.

#EnW2: E. Penrose 1951

#Fsl: From %(q:The Economics of the International Patent System), 1951.

#Upy: Up to the present, the regime for the international protection of
patent rights has been developed primarily in the interests of
patentees.  The gains to be derived from an extension of the patent
system have been stressed, but the concomitant increase in social
costs has been seriously neglected.

#Gdr: Granstrand UNCTAD report 1990

#Fvp: From %(q:The use of patents for the protection of technological
innovation: A case study of selected Swedish firms) -- A commissioned
report for the UN Conference on Trade and Development Secretariat,
1990.

#PWr: Patents as an instrument to stimulate innovative activities appear to
be of little relevance for small firms.  It was found that no
significant changes in R&D behavior would take place if the patent
protection time were reduced or extended.  Also, for large firms, the
R&D behavior seems to be rather independent of the availability of
patent protection.  The survey showed that increased patent protection
time is likely to provide, at most, a modest stimulus for R&D
activities.  Chemical, and particularly pharmaceutical, firms appear
to be more sensitive to such changes.

#Soe: S. Scotchmer 1991

#SuW2: Standing on the Shoulders of Giants: Cumulative Research and the
Patent Law, Journal of Economic Perspectives, 1991.

#Ifp: It appears that patent policy is a very blunt instrument trying to
solve a very delicate problem.  Its bluntness derives largely from the
narrowness of what patent breadth can depend on, namely the realized
values of the technologies.  As a consequence, the prospects for
fine-tuning the patent system seem limited, which may be an argument
for more public sponsorship of basic research.

#MiW: M. Wirth 1866

#VWu: Vierteljahrschrift fur Volkswirtschaft und Kulturgeschichte, 1863.

#IWp: Inventions do not belong in the category of intellectual property,
because inventions are emanations of the current state of civilization
and, thus, are common property. ...  What the artist or poet create is
always something quite individual and cannot simultaneously be created
by anyone else in exact likeness.  In the case of inventions, however,
this is easily possible, and experience has taught us that one and the
same invention can be made at the same time by two different persons;
inventions are merely blossoms on the tree of civilization.

#MlW2: M. Polanyi 1944

#PRm: Patent Reform, Review of Economic Studies, 1944

#Iyd: I believe the law is essentially deficient because it aims at a
purpose which cannot be rationally achieved.  It tries to parcel up a
stream of creative thought into a serious of distinct claims, each of
which constitutes the basis of a separately owned monopoly.  But the
growth of human knowledge cannot be divided up into such sharply
circumscribed phases.  Ideas usually develop shades of emphasis, and
even when, from time to time, sparks of discovery flare up and
suddenly reveal a new understanding, it usually appears on closer
scrutiny that the new idea had at least been partly foreshadowed in
previous speculations.  Moreover, discovery and invention do not
progress only along one sequence of thought, which perhaps could
somehow be divided up into consecutive segments.  Mental progress
interacts at every stage with the whole network of human knowledge and
draws at every moment on the most varied and diverse stimuli.
Invention, and particularly modern invention which relies more and
more on a systematic process of trial and error, is a drama enacted on
a crowded stage.  It may be possible to analyze its various scenes and
acts, and to ascribe different degrees of merit to the participants;
but it is not possible, in general, to attribute to any of them one
decisive self-contained mental operation which can be formulated in a
definitive claim.

#LWi: L. Mises

#HAE: Human Action: A Treatise of Economics, 1949

#lhh: .... the fairness of the patent laws is contested on the ground that
they reward only those who put the finishing touch leading to
practical utilization of achievements on many predecessors.  These
precursors go empty-handed although their contribution to the final
result was often much more weighty than that of the patentee.

#FrH: Friedrich Hayek

#IWW: Individualism and Economic Order, 1948.

#Iho: In the field of industrial patents in particular we shall have
seriously to examine whether the award of a monopoly privilege is
really the most appropriate and effective form of reward for the kind
of risk bearing which investment in scientific research involves.

#All: Arnold Plant

#Tce: The Economic Theory Concerning Patents for Inventions, Economica,
1934.

#Ect: Expedients such as licenses of right, nevertheless, cannot repair the
lack of theoretical principle behind the whole patent system.  They
can only serve to confine the evils of monopoly within the limits
contemplated by the legislators; and, as I have endeavoured to show,
the science of economics, as it stands today, furnishes no basis of
justification for this enormous experiment in the encouragement of a
particular activity by enabling monopolistic price control.

#LoM: Aus einem Brief an Isaac McPherson

#Iss: Wenn die Natur ein Ding am wenigsten tauglich für alleiniges Eigentum
gemacht hat, so ist das die denkende Kraft, die man %(q:Idee) nennt,
die ein Individuum nur solange alleinig besitzt, solange es sie für
sich behält; aber in dem Moment, in dem sie ausgeplaudert wird, bringt
sie sich selbst in den Besitz eines jeden, und der Empfänger kann sich
ihrer nicht entledigen. Ebenso ist es ihr eigentümlich, dass niemand
weniger besitzt, denn jeder besitzt sie als ganzes. Der, der eine Idee
von mir erhält, erhält damit Unterweisungen für sich selbst, ohne mich
um meine zu bringen, so wie der, der seine Fackel an meiner entzündet,
das Licht empfängt, ohne mich ins Dunkel zu stürzen. Dass sich Ideen
frei vom einem zum anderen über den Erdball verbreiten sollen, zur
moralischen und gegenseitigen Anleitung des Menschen, und der
Verbesserung seiner Lebensumstände, scheint einzigartig und gütig von
der Natur eingerichtet worden zu sein.  ... Erfindungen können also,
ihrer Natur nach, kein besitzbares Gut sein.

#Jov: Johann Wolfgang von Goethe

#JGb: Johann Peter Eckermann, Gespräche mit Goethe in den letzten Jahren
seines Lebens, Berlin und Weimar, 1982, S.662 ff.

#IjW: Im Grunde aber sind wir alle kollektive Wesen, wir mögen uns stellen
wie wir wollen. Denn wie weniges haben und sind wir, das wir im
reinsten Sinne unser Eigentum nennen! Wir müssen alle empfangen und
lernen, sowohl von denen, die vor uns waren, als von denen, die mit
uns sind. Selbst das größte Genie würde nicht weit kommen, wenn es
alles seinem eigenen Inneren verdanken wollte. Das begreifen aber
viele sehr gute Menschen nicht und tappen mit ihren Träumen von
Originalität ein halbes Leben im Dunkeln. ... Es ist im Grunde auch
alles Torheit, ob einer etwas aus sich habe oder ob er es von anderen
habe; ob einer durch sich wirke oder ob er durch andere wirke: die
Hauptsache ist, daß man ein großes Wollen habe und Geschick und
Beharrlichkeit besitze, es auszuführen; ...

#tWE: Jean-Michel Yolin, Leiter der Abteilung %(q:Innovation) im
französischen Wirtschaftsministerium und Autor des Berichtes %(IE),
merkt in einem Gespräch über den Fall SCO ./ IBM/Linux an:

#pat: Patente dienten einmal dazu, Forschung und Entwicklung rentabel zu
machen.  Inzwischen stehen sie aber nicht mehr im Dienste der
Innovation sondern sind zu einem Mittel zu verkommen, das Gelände zu
verminen und unerwünschte Innovatoren auszustechen.  Man jagt ihnen
einfach im entscheidenden Moment Anwälte auf den Hals, wenn sie gerade
dabei sind, Kapital oder Kunden zu akquirieren.

#Eoe: Wirtschafts- und Sozialausschuss der Europäischen Gemeinschaft 2002/09

#Aot: An expert opinion adopted by vote of a consultative body of the
European Community says about the CEC directive proposal:

#AyE: Selbst  wenn  der  Anwendungsbereich  des  Richtlinienvorschlags  der 
Kommission  sich  zum gegenwärtigen  Zeitpunkt  auf 
computerimplementierte  Erfindungen  beschränkt,  denen  die 
traditionellen kumulativen  Kriterien  des  Anwendungsbereichs  der 
Patentierbarkeit  zugeschrieben  werden  (was  die Anhänger einer
vollständigen Abschaffung jedweder Begrenzung des Anwendungsbereichs
des Patentrechts nicht  zufrieden  stellen  dürfte),  so  stellt 
dieses  Dokument  dennoch  eine  De  facto  Akzeptanz  und  eine
nachträgliche  Rechtfertigung  des  rechtsprechungsmäßigen  Irrwegs 
des  EPA  dar.  Obwohl  der Richtlinienvorschlag auf den ersten Blick
ein etwas abgeschwächteres Vorgehen als die einfache Streichung von
Artikel 52 Absatz 2 des EPÜ - was die Direktion des EPA und
verschieden Mitglieder des Rats gerne sähen  -  beinhaltet  wird,  so 
ebnet  er  doch  den  Weg  für  die  künftige  Patentierbarkeit  des 
gesamten Softwarebereichs,  insbesondere  aufgrund  der  Annahme, 
dass  unter  %(q:technischer  Wirkung)  auch  die Auswirkung von
Software lediglich auf einem Standard-Computer verstanden werden kann.

#Ohf: Man darf sich nach dem wirklichen Zweck einer solchen Richtlinie
fragen, insbesondere mit Blick auf die Begründung, die mit
Überlegungen zum Schutz der Softwarebranche vor Raubkopien beginnt und
 die  sich  in  den  Dokumenten  in  der  Anlage  zum 
Richtlinienvorschlag  fast  ausschließlich  auf  Software und die
%(q:Softwareindustrie) bezieht, deren Stellenwert für den Vorschlag
außerdem überzogen um nicht zu sagen  völlig  irrelevant  erscheint, 
wenn  der  Geltungsbereich  wirklich  so  beschränkt  wäre,  wie  es 
die Kommission darstellt.

#IWW2: Ist  es  aus  heutiger  Sicht  vernünftig,  die  Erfassungsgrenze  von
 Patenten,  die  doch  ein Werkzeug  des  Industriezeitalters  sind, 
auf  geistige  und  immaterielle  Schöpfungen  wie  Software  sowie 
auf deren Ausführung auf Computern auszudehnen? Die Antwort der
Kommission ist ein unmissverständliches Ja  und  ist  aus  der 
Aufmachung  des  Richtlinienvorschlags  und  des 
Folgenabschätzungsbogens  abgelesen.  Die  gewählte  enge  Sichtweise,
 die  einzig  und  allein  vom  rechtlichen  Aspekt  von  Patenten 
ausgeht,  ohne gebührende  Berücksichtigung  der  wirtschaftliche 
Aspekte,  der  Auswirkungen  für  die  Forschung  und  die
europäischen  Unternehmen,  d.h.  also  ohne  Gesamtsicht,  verträgt 
sich  nicht  mit  der  Größenordnung  der Gefahr für die Gesellschaft,
die Entwicklung und sogar der Demokratie (elektronische Verwaltung,
Bildung, Information der Bürger), die auf lange Sicht aufs Spiel
gesetzt werden.

#ThW: Der Ausschuß der Regionen möchte die Aufmerksamkeit der Kommission auf
die Tatsache lenken, daß der Patentschutz nicht universal ist, sowie
auf die Gefahren der systematischen Patentierung des geistigen
Eigentums hinweisen. Diese Fragen betreffen hauptsächlich die neuen
Technologien und vor allem die Informationstechnologien und die
Biowissenschaften, die Gegenstand einer inhaltsreichen und
leidenschaftlichen Diskussion sind.

#Iro: Im Fall der Software haben die seit den siebziger Jahren in den
großen, von dem Thema betroffenen Staaten geführten Diskussionen alle
zu dem Ergebnis geführt, daß das System der Urheberrechte gelten soll,
obwohl es den Besonderheiten des Bereichs nicht umfassend gerecht
wird. Die europäische Richtlinie vom 1. Januar 1993 zeigte
insbesondere den goldenen Mittelweg zur Förderung der
%(q:Interoperabilität) von Programmen, um wettbewerbsschädliche
Strategien mit dem Ziel einer marktbeherrschenden Stellung zu
konterkarieren. Seit mehreren Jahren ist die amerikanische
Rechtsprechung dazu übergegangen, die Ausstellung von Patenten für
%(q:Softwareelemente) zu gewähren, was sie bis dahin abgelehnt hatte.
Und der Druck Amerikas auf Europa zur Gewährung der Patentfähigkeit
auf europäischer Ebene nimmt immer mehr zu.

#TnW: Dabei sind die Risiken hier sehr hoch. Ein derartiges Vorgehen bedroht
die Innovationsdynamik in dieser Industrie, da sie zu einer Isolierung
der Kenntnisse und Verfahren führt, die jegliche Kombination
unterbindet. So finden sich unter der Vielzahl in den Vereinigten
Staaten angemeldeter und eingetragener Patenten zahlreiche Verfahren
oder sogar Algorithmen. Viele scheinen von den Merkmalen Neuheit und
Originalität, die eigentlich Voraussetzung für die Ausstellung eines
Patents sind, weit entfernt zu sein.

#Icr: Sollte die Ausstellung von Patenten für Software zur festen
Einrichtung werden, so wäre das eine Waffe zur Stärkung der
Vormachtstellung der größten amerikanischen Marktführer in diesem
Bereich. Sie wäre eine direkte Bedrohung für die Masse der
innovatorischen KMU, sowohl in Europa als auch in den Vereinigten
Staaten und anderswo.  Schließlich würde sie ein sehr großes Handicap
für die europäische Softwareindustrie darstellen, die trotz des hier
eingesetzten hochwertigen Fachwissens große Schwierigkeiten hat, im
Handel wettbewerbsfähig zu sein.

#Pan: Sozialistische Partei Frankreichs

#PrW: Unter den Zukunftsfragen der europäischen Softwarebranche kommt der
Einrichtung einer eventuellen Patentierbarkeit von Software ein
besonders dringlicher Charakter zu, da die europäischen Entwicklungen
in diesem Bereich schon weit fortgeschritten sind und da das
Europäische Patentamt ständig vollendete Tatsachen schafft.  Vor der
Einrichtung eines Patentschutzes für Software (wie ihn die Vereinigten
Staaten von uns fordern) hält die Sozialistische Partei es für
unerlässlich, sich zu vergewissern, dass das Fehlen von
Softwarepatenten für die europäischen Softwarehäuser wirklich
nachteilhaft ist.  Die wirtschaftlichen Studien geben eher Anlass zu
der Annahme, dass Softwarepatente heute eine Waffe in den Händen der
Großunternehmen sind, die davon Gebrauch machen, um lästige
Innovationen zu verhindern und die Entwicklung freier Software zu
beschränken.  Die besonders schlechte amerikanische Erfahrung mit
Softwarepatenten sollte zu Denken geben.

#LWa: Das Patentwesen passt möglicherweise nicht zum Rhythmus der Innovation
im Bereich der Software : die zum Erwerb eines Patentes erforderliche
Zeit ist ebenso wie die Patentlaufzeit (20 Jahre) zu lang.  Die
Innovation im Softwaresektor ähnelt ihrem Wesen nach mathematischen
Entdeckungen: neues Rechenverfahren, neuer Algorithmus, Anwendung
eines bekannten mathematischen Verfahrens auf ein spezielles Problem. 
Wie und wo soll die Trennlinie zwischen Softwareprozess und einem
gedanklichen Verfahren gezogen werden?  Man sollte sich auch
vergegenwärtigen, dass Software bereits jetzt in unserem System des
geistigen Eigentums einen bedeutenden Rechtschutz genießt.

#POI: Thesen

#Loa: Die Verhandlungen auf europäischer Ebene erfordern daher klare
Positionen: solange keine begünstigenden Auswirkungen auf die
Innovation zu erkennen sind, meinen die Sozialisten, dass

#l1o: die Patentierbarkeit von Software abzulehnen ist.

#loe: die beim Europäischen Patentamtes festgestellten Fehlentwicklungen zu
bekämpfen sind.

#Eeo: Wahlprogramm der Belgischen Sozialistischen Partei 2003

#ltt: Software ... should continue to be protected by copyright rather than
by patents.

#fmn: ehemaliger französischer Minister in diversen Ressorts und
Präsidenschaftskandidat 2002

#TWs: Die Ausweitung des Bereichs des Patentierbaren auf Software, ein
wirtschaftlicher Unsinn der unsere Unternehmen behindern und diesen
Sektor bremsen wuerde, muss verweigert werden.

#dih: Abgeordneter der Moselregion in der Französischen Nationalversammlung

#Lio: Das Patentsystem hat sich in den letzten Jahren weit über den Bereich
seiner historischen, ökonomischen und moralischen Existenzberechtigung
hinaus ausgedehnt.  Diese Ausdehnung ist das Ergebnis von
Gerichtsentscheidungen des Europäischen Patentamts (EPA), die
bisweilen im Gegensatz zum Geist der Gesetze stehen, wie sie von den
Gesetzgebern verabschiedet wurden, wobei die Unterzeichnerstaaten des
Münchener Übereinkommens meist nicht über die nötigen Mittel verfügen,
um die wirtschaftlichen und gesellschaftlichen Auswirkungen dieser
Entscheidungen zu kontrollieren.  Insbesondere glaube ich, dass das
EPA, indem es allen Ernstes behauptete, ein %(q:Computerprogramm mit
technischen Wirkungen) sei kein %(q:Computerprogramm als solches) und
könne daher zum Gegenstand von Patentansprüchen werden, eindeutig
seine Macht missbraucht hat.  Das EPA hat eine Rechtsprechung
entwickelt, die in offensichtlichem Gegensatz zu dem internationalen
Vertrag steht, mit dessen Einhaltung es betraut wurde.   Denn alle
Computerprogramme haben einen technischen Effekt, wie die Europäischen
Patentberater zu Recht anmahnten, die sich 1997 im EPA zu einem
%(q:Runden Tisch) versammelten.

#Cts: Diese unkontrollierte Ausweitung des Patentsystems in den Bereich der
Software hinein gefährdet in zunehmendem Maße die europäischen
IT-Unternehmen, die Autoren freier Software und die Grundarchitektur
der Informationsgesellschaft.  Mehr als 10000 Softwarepatente wurden
in den letzten 10 Jahren bei den nationalen Patentämtern durch vom EPA
inspirierte juristische Winkelzüge angemeldet, während selbst die
Anmeldungsanleitungen der Patentämter noch immer klar sagen, dass
Computerprogramme nicht patentierbar sind.  Mehr als 75% dieser
Patente wurden von außereuropäischen Unternehmen angemeldet.  Nicht
wenige dieser Softwarepatente beziehen sich auf Verfahren des
elektronischen Geschäftsverkehrs sowie Organisations- und
Lehrmethoden.

#Mev: Juristische Standardwerke wie %{LI} weisen allerdings darauf hin, dass
diese Patente angesichts der offensichtlichen Widersprüche zwischen
dem geltenden Gesetz und der Rechtsprechung des EPA keinerlei Wert
haben außer demjenigen, den die jeweiligen Gerichte ihnen zuzuerkennen
bereit sind.  Im Falle eines Rechtsstreits besteht kein Verlass
darauf, dass ein nationaler Richter die Ansprüche dieser Patente
anerkennen würde, da sie sich offensichtlich auf Gegenstände beziehen,
deren Patentierung dem Geist des Gesetzes widerspricht.  Daher warten
die Inhaber der Softwarepatente, der E-Geschäftsmethoden-Patente und
der Internet-Patente nur noch auf eines, bevor sie sich entschließen,
die französischen und europäischen Protagonisten der Neuen Wirtschaft
anzugreifen:  die Revision des Münchener Übereinkommens, die sich
anschickt, den Ausschluss der Computerprogramme von der
Patentierbarkeit zu beseitigen.

#AWi: Ich würde mich Ihnen verbunden fühlen, wenn Sie in den kommenden
Konsultationen auf nationaler, europäischer und globaler Ebene alle
Ihnen zu Gebote stehenden Mittel einsetzen würden, um zu verlangen,

#dan: dass im November 2000 Art 52 des Münchener Übereinkommens nicht
geändert wird und somit das Trojanische Pferd nicht aktiviert wird,
das derzeit beim EPA schläft, wo zahlreiche missbräuchlich an
außereuropäische Unternehmen vergebene Internet-Patente von einem Tag
auf den anderen die Neue Wirtschaft Frankreichs und Europas bedrohen
können.

#qlW: http://www.osslaw.org/

#qnW: dass die Begriffe %(q:technisch), %(q:gewerbliche Anwendung) und
%(q:Programm als solches) derart geklärt werden, dass kein
Informationswerk und kein immaterielles Produkt (einschließlich of
Informationssystemen laufende Software) als unmittelbar oder mittelbar
patentverletzender Gegenstand in Betracht kommen kann.

#qWo: dass materielle Produkte oder materielle Erweiterungen von
immateriellen Produkten (z.B. MP3-Abspielgeräte) patentiert werden
könnten, sofern die Kriterien der Neuheit, Technizität und
industriellen Anwendbarkeit dieses materiellen Produktes erfüllt
werden, und zwar unabhängig von den darin verwendeten
Softwareelementen betrachtet.

#qdi: dass unverzüglich eine offene und demokratische Diskussion auf
Grundlage ausführlicher wissenschaftlicher Studien über die
wirtschaftlichen und gesellschaftlichen Wirkungen des Patentsystems
auf die Informationsgesellschaft in Angriff genommen wird.

#qey: dass eine frei verfügbare und über freie Software zugängliche
umfassende Patentdatenbank aufgebaut wird, um den KMU die Mittel an
die Hand zu geben, mit Patentgefahren in Europa und der Welt besser
fertig zu werden.

#Aen: Da das Europäische Patentamt keinerlei Studie publiziert hat, um die
Ausweitung der Patentierbarkeit auf Computerprogramme zu
rechtfertigen, obwohl Wirtschaftswissenschaftler bewiesen haben, dass
das Patentsystem zu einer Minderung der Innovation in der
Softwareökonomie führen könnte, scheint es mir ferner an der Zeit,
eine Betriebsprüfung des Europäischen Patentamtes in Auftrag zu geben,
um nach Mitteln zu suchen, wie man die Entscheidungen dieser
Institution besser kontrollieren und in Einklang mit dem
Allgemeininteresse und den Grundprinzipien einer unparteiischen
Rechtsprechung bringen kann.

#Wse: Wirtschaftspolitische Sprecherin der Grünen Fraktion

#Vir: Gründer werden durch die Debatte über die mögliche Patentierbarkeit
von Software und Geschäftsideen auch in Europa verunsichert.  Hier
muss schnell Klarheit geschaffen werden.

#CWa: Computerprogramme %(q:als solche) sind nach dem Europäischen
Patent-Übereinkommen (EPÜ) nicht patentierbar. In den USA dagegen ist
grundsätzlich alles menschengemachte patentierbar.

#Bwd: Bei Software-Entwicklern und in der Open-Source-Szene besteht
erhebliche Verunsicherung, weil befürchtet wird, dass über die Novelle
des EPÜ und eine angekündigte Richtlinie der Europäischen Kommission
in Europa amerikanische Verhältnisse eingeführt werden sollen.

#IgA: In den USA wird der Wettbewerb bereits erheblich auch durch die
Patentierung von Geschäftsideen behindert. Viel zitiertes Beispiel ist
das Patent von Amazon auf das one-click-Verfahren bei der Bestellung
von Gütern im Internet.

#Asm: Allerdings ist bereits in den letzten Jahren in Europa Software
zunehmend als Bestandteil technischer Verfahren patentiert worden,
denn: Technische Verfahren, die Computerprogramme beinhalten, sind
patentierbar. Beispiel dafür ist eine computergesteuerte
Werkzeugmaschine, die insgesamt patentierbar ist. Es mangelt derzeit
an eingehenden ökonomischen Analysen, die die Wirkungen einer
möglichen Patentierung von Software beschreiben. Wir sehen allerdings
die Gefahr einer weiteren Verstärkung von Bürokratie mit dem Effekt,
dass dringend notwendige Innovationen behindert werden.

#DDf: Die Ermöglichung der Patentierung von Software würde darüber hinaus
erhebliche technische und administrative Probleme schaffen: in der
Zeit, die Anmeldung eines Patentes derzeit benötigt (derzeit 2 Jahre),
ist das Patent längst veraltet. Die Dokumentation der Patente wäre
extrem aufwendig. Kleine und mittlere Unternehmen würden durch die
Patentierung benachteiligt: Große Firmen, die über die Ressourcen
verfügen, die Patententwicklung zu verfolgen und Patente anzumelden
könnten auf diese Weise zusätzliche Erträge erwirtschaften. Der
Wettbewerb würde sich von der schnellen Umsetzung von Innovationen auf
juristische Streitereien verlagern und der technische Fortschritt
würde behindert werden. Das muss verhindert werden!

#MsW: MdB

#Vns: Vorsitzender des Parlamentarischen Unterausschusses für die Neuen
Medien

#Itm: In technologiepolitischen Fachkreisen hört man immer wieder die
Behauptung, das Patentsystem müsse auf gewisse Bereiche der
Informationstechnik ausgeweitet werden, weil sonst deren Investitionen
nicht genügend geschützt würden.  Diese Behauptung wurde bisher
allerdings immer nur als abstrakte Grundwahrheit weitergegeben und
niemals anhand von Tatsachen der deutschen oder europäischen
IT-Wirtschaft belegt.

#ScW: Selbst wenn es gelänge, Bereiche der Informationstechnik zu finden, in
denen Patente nachweislich vorteilhaft wirken oder gewirkt haben,
müsste man noch immer untersuchen, ob eventuelle schädliche
Nebenwirkungen der Patentierung diese Vorteile nicht überwiegen.

#Aiz: Aber während bei der Legislative noch vollkommene Unklarheit herrscht,
schreitet die Judikative bereits zur Tat, gewährt Tausende von
Softwarepatenten und drängt auf Änderung der Gesetzesregeln.  Es ist
daher höchste Zeit für uns als Gesetzgeber, uns um diese Fragen zu
kümmern.

#ReK: Rainder Brüderle

#WhP: Wirtschaftspolitischer Sprecher der FDP-Bundestagsfraktion

#DWs: Die Vorentscheidung zugunsten von Softwarepatenten des
Verwaltungsrates des Europäischen Patentamtes lässt aufhorchen. Hier
wurde auf Verwaltungsebene eine Schlüsselentscheidung für die
Schlüsseltechnologien des 21. Jahrhunderts vorbereitet. Das Thema
Softwarepatente ist allerdings zu wichtig für unsere wirtschaftliche
Zukunft, als dass es länger in den Hinterzimmern multinationaler
Gremien und den juristischen Spezialzirkeln geführt werden darf.

#Wmi: Wir sollten sehr kritisch prüfen, ob wir auf dem schnellebigen Gebiet
der Software einen exklusiven Patentschutz wollen.  Denn die
Patentierung von Software ist ein höchst zweischneidiges Unterfangen.
Dem Schutzinteresse Einzelner steht die Innovationsfähigkeit der
gesamten Branche gegenüber.  Es sollte aufhorchen lassen, dass der
zweitgrößte Softwarehersteller der Welt ORACLE sich aus diesem Grund
massiv gegen Softwarepatente ausspricht. Insbesondere die in Europa
starke und zukunftsträchtige Bewegung der freien Software (OpenSource
wie LINUX) wäre durch Softwarepatente in ihren Grundfesten gefährdet.

#Stn: Softwarepatente bergen die Gefahr in sich, daß die Großen der Branche
dank Finanz und Personalkraft kleine und mittelständische
Softwareschmieden mittels der Patentierung existenziell gefährden
werden.  Die Verfahren gegen Microsoft belegen aber deutlich, wie
wichtig und schwierig Wettbewerb schon heute im Softwarebusiness ist.
Die heftigen Debatten um Softwarepatente in den USA, aufgekommen durch
die Patente für amazon.com, sollten Europa eine Mahnung sein. Wir
sollten nicht um jeden Preis alles von Amerika übernehmen.

#EeM: Europa wäre gut beraten, bei den Softwarepatenten an die meist klein-
und mittelständische Struktur seiner Softwareindustrie zu denken. Es
wirft kein gutes Licht auf das Gewicht die Bundesregierung, dass sie
sich nicht im Verwaltungsrat gegen die Softwarepatentierung
durchsetzen konnte. Die Märkte von morgen sind die Märkte von Ideen.
Die Gedanken, auch die zu Software geronnenen, sollten aus liberaler
Sicht weitestgehend frei bleiben.

#Ftu: FDP Parteitagsbeschluss 2001/05

#UdW: Unter dem Titel %(q:Standort zukunft.de) treffen die Liberalen auf
ihrem Düsseldorfer Parteitag im Mai 2001 allerlei Aussagen zur
digitalen Ökonomie.

#UWi: UK E-Minister Patricia Hewitt 2001-03-12

#ShU: Software genießt heute weltweit umfassenden Schutz durch das
Urheberrecht. Damit ist den Herstellern und Programmierern ein starkes
absolutes Recht verliehen, um ihre Interessen umfassend gegenüber
Dritten wahrnehmen zu können. Indes kennt die US-amerikanische
Rechtsordnung auch die Patentierbarkeit von Software. Im Gegensatz zum
Urheberrecht schützt das Patent jedoch nicht das fertige Produkt,
sondern dehnt den Schutz auf die Methode oder gar ein
softwarebasierendes Geschäftsmodell aus. Die Entwicklung in den USA
zeigt schon heute deutlich, dass die Patentierung von Software sich
negativ auf die Entwicklung neuer Produkte und Geschäftsmodelle
auswirken kann. Denn einzelne Softwarepatente können im Bereich der
sogenannten Individualsoftware ganze Märkte blockieren.

#SeW: Sowohl nach deutschem Patentrecht als auch nach dem Europäischen
Patentübereinkommen sind Computerprogramme als solche derzeit nicht
patentierbar. Die FDP spricht sich dafür aus, an dieser Rechtslage im
Grundsatz festzuhalten.

#bay: UK Liberal Democrats IT Policy Motion

#gWr: The party's IT policy was ratified by party conference on Sunday
2003/03/16. During the debate several speakers either spoke
specifically against software patents or mentioned the issue in
passing, as did the proposer of the ratifying motion (who was the
chair of the policy working group).  The paper was published together
with a motion that calls for:

#tsa: supporting continued widespread innovation by resisting the wider
application of patents in this area.

#Mia: Dr. Martin Mayer

#meW: medienpolitischer Sprecher der CDU/CSU-Fraktion

#Sde: Statt der beabsichtigten generellen Ausdehnung des Patentschutzes für
Software in Europa muss ein zweijähriges Moratorium beschlossen
werden.

#Amt: Auf der 'Diplomatischen Konferenz 2000' der Europäischen Patentämter
ist vorgesehen, 'Programme für Datenverarbeitungsanlagen' aus der
Ausnahmevorschrift Art. 52(2), europäisches Patentübereinkommen, zu
streichen, und somit generell die Patentierung von Software zu
ermöglichen.

#IWg: In der Fachwelt gibt es gegen diese Absicht die berechtigte
Befürchtung, dass durch den Revisionsvorschlag

#Mqs: Monopolstellungen großer Softwarehäuser gestärkt und erweitert,

#kdr: kleine Softwareunternehmen und selbstständige Programmierer in ihrer
Existenz bedroht und

#iiW: insgesamt der Fortschritt in der Softwareentwicklung deutlich gebremst
würden.

#Esj: Eine derart verheerende Entwicklung, die sich in den USA schon jetzt
abzeichnet, darf in Europa nicht stattfinden. Deshalb muss vor einer
weiteren Rechtssetzung für den Schutz von Software eine gründliche,
öffentliche Diskussion von Fachwelt und Politik auf der Basis der
folgenden Grundsätze geführt werden.

#ZcW: Ziele des Rechtsschutzes für Software:

#Dbs: Der Rechtsschutz muss Programmierer und Unternehmen in die Lage
versetzen, die Früchte ihrer Arbeit zu ernten. Die finanzielle
Entlohnung, die sich nur über den Rechtsschutz verwirklichen lässt,
ist der wichtigste Anreiz für den Fortschritt in der
Softwareprogrammierung und Anwendung.

#Dsr: Der Rechtsschutz darf aber nicht zur Stärkung von weltbeherrschenden
Monopolen führen. Er muss den Wettbewerb fördern statt ihn zu
behindern.  Vor allem darf er keinesfalls kleine Softwareunternehmen
und selbstständige Programmierer benachteiligen und in ihrer Existenz
bedrohen.

#Mtt: Maßgeschneiderter Rechtsschutz für Software

#DWS: Das Urheberrecht wurde zum Schutz von künstlerischen und
schriftstellerischen Werken geschaffen. Es schützt auch
Computerprogramme in ihrer Eigenschaft als Sprachwerke. Allerdings
schützt das Urheberrecht die Software nur unzulänglich.

#Dse: Die Patente wurden im beginnenden Industriezeitalter zum Schutz
technischer Erfindungen eingeführt. In Ausgestaltung und Zeitdauer
tragen sie den Erfordernissen der Wissensgesellschaft nur unzureichend
Rechnung.

#Sln: Software ist im Vergleich zu schriftstellerischen und künstlerischen
Werken und zu technischen Erfindungen etwas völlig Neues und
Andersartiges. Sie ist das elementare Hilfsmittel in der
Informationsgesellschaft und dringt in immer neue Bereiche vor. Daher
muss für sie ein eigenes, maßgeschneidertes Instrument des
Rechtsschutzes geschaffen werden.

#EsJ: Erklärung der Nachwuchsorganisationen der Christdemokratischen und
Konservativen Parteien Europas 2000-10-28

#Rae: http://www.demyc.org

#Nct: Noting  that European Patent Office is planning to remove computer
programs from the list of the things which can not be patented of  in
its next conference in end of November

#Fnb: Further noting the still existing great amount of unsolved problems in
software patents like:

#Asr: Abstract-logical nature of software, which is in conflict with the
patent system's requirement for concreteness and physical substance

#Ige: Inefficient  long period of protection (20 years)

#Ttb: The lack of IT-expertise among the patent inspectors which would lead
too wide patent claims to be accepted as has happened in the United
States

#Top: The problem of anti-competitiveness; US software patents, if easily
transferable to EU, could substantially narrow competition in the
European Union, give dominant position to US corporations and have
negative influence among european IT start-ups

#Ttl: The negative effect of patents to the interoperability of computer
programs

#Toa2: Taking into consideration the strong opposition among small and medium
size companies towards software patents in Europe

#HcW: Having in mind the enormous economic importance of computer software
industry and its need for effective protection for its investments

#Yit: YEPP calls upon European political parties and the European Commission
(DG XV) to take immediate actions to delay all development on software
patents until the problems are correctly addressed

#Yee: YEPP further calls for taking actions to strengthen the European
software industry by improving the regulatory framework on combating
software piracy

#MlW: Ms Hewitt may not have thought through the idea of patenting machine
tool software and left a window for confusion open here, but her basic
policy goal is clearly outlined:  If anything about software can be
patented, that must be something related to advanced machinery and
physical phenomena.  There should be something new in the hardware. 
Software ideas for known general-purpose computers, especially those
related to social phenomena such as language and business, must in any
case be unpatentable.

#Wna: Wodarg hat die %(ep:Eurolinux-Petition) wohl nicht zuletzt deshalb
unterzeichnet, weil er auf seinem Gebiet mit den Entgrenzungs-Taktiken
der Patentgesetzgeber aufs genaueste vertraut ist.  Im %(q:Deutschen
Ärzteblatt) vom Juli 2000 seziert er unter der Überschrift
``Schwammige Definitionen, moralische Lyrik'' die EU-Richtlinie
``Rechtlicher Schutz biotechnologischer Erfindungen''.  Darin schreibt
er u.a.:

#DWn: Der Text der EU-Richtlinie enthält vor allem im Vorspann und in den
Erwägungsgründen viel moralische Lyrik, mit der Kritiker
abgeschmettert und Parlamentariergewissen beschwichtigt werden können.
 Klare Grenzen und Definitionen fehlen.  Schwammige oder in sich
widersprüchliche Bestimmungen sorgen dafür, dass Rechtsnormen, wie sie
das 1977 in Kraft getretene Europäische Patentübereinkommen vorgibt,
systematisch ausgehöhlt werden können -- zum Beispiel das zentrale
Prinzip der ärztlichen Therapiefreiheit.  Nach Artikel 52,4 des
Übereinkommens dürfen diagnostische, therapeutische und chirurgische
Verfahren am menschlichen und tierischen Koerper nicht patentiert
werden.  Diesen Grundsatz beizubehalten, verspricht die Richtlinie in
Erwägungsgrund 35, jedenfalls im Hinblick auf die Verfahren als
ganzes.  Teilschritte von Verfahren können jedoch sehr wohl patentiert
werden.  Nach dem Motto: Keine Tür darf dem Arzt zum Wohle der
Patienten verschlossen sein, über die Nutzung der Türgriffe
entscheidet aber der Patentinhaber.

#Ele: Eine ähnliche juristische Spitzfindigkeit findet sich in dem für die
EU-Richtlinie zentralen Artikel 5.  Im ersten Abschnitt ist
festgelegt, dass der menschliche Körper in allen Entwicklungsstadien
nicht patentierbar ist, ebenso wenig wie die bloße Entdeckung eines
seiner Bestandteile (Beispiel Gene).  Im zweiten Abschnitt heisst es
jedoch, dass Teile, die mit Hilfe eines technischen Verfahrens
isoliert wurden, sehr wohl patentierbar sind.  Wer sich mit der
Regelung näher befasst, wie der Nationale Ethikrat der Dänen oder
Kritiker in Frankreich, kommt nicht umhin festzustellen, dass bei
Genen und Teilsequenzen von Genen die vermeintliche Ausnahme die Regel
ist.  Gene liegen immer isoliert vor, wenn sie entschlüsselt werden.

#Hes: Herta Däubler-Gmelin, Bundesministerin der Justiz

#Mkg: Mit der Vergabe der 20-jährigen Monopole auf die Nutzung von
Software-Ideen, so Däubler-Gmelin, seien erhebliche Probleme für die
Ökonomie und die Sicherheit der Informationsgesellschaft verbunden,
die ``erst einmal gründlich und breit diskutiert werden müssen''.

#MuW: Man könne schließlich eine solch zentrale Frage der Wirtschaftspolitik
%(q:nicht von Malta und Liechtenstein bestimmen lassen), erklärt ein
Ministerialer. Notfalls könne man auch das Abkommen insgesamt platzen
lassen und innerhalb der EU eine eigene Patentpolitik entwickeln.

#Jfk: Justizministerin Prof. Dr. Herta Däubler-Gmelin im März 2001

#DWW: /swpat/analyse/epue52/index.de.html

#Vtu: Vehement hat sich Bundesjustizministerin Herta Däubler-Gmelin auf der
Hannover Messe gegen Vorschläge der EU ausgesprochen, die
Patentierbarkeit von Software generell zu erlauben. Mitte dieses
Monats will EU-Kommissar Frits Bolkenstein der europäischen Kommission
eine neue Richtlinie zur Patentierung von Computerprogrammen vorlegen.
In dieser Richtlinie werden sämtliche Computerprogramme als
patentierbar erklärt. Justizministerin Herta Däubler-Gmelin spricht
sich gegenüber der Computer Zeitung deutlich gegen diesen Vorschlag
aus und fordert die Beibehaltung der jetzigen Regelung, die
Softwarepatente nur als Bestandteil eines technischen Verfahrens
erlaubt.

#OWn: Our key principle is that patents should be for technological
innovations. So a program for a new machine tool should be patentable
but a non-technological innovation, such as grammar-checking software
for a word-processor, should not be.

#SWa: V. Strategic Considerations

#WWl2: Recently my firm had to explore the patent situation world-wide for
one the major banks. All existing U.S. patents and all published
European patent documents have been searched regarding any potential
risk for the banking business. The search was performed on the basis
of a profile combining both the leading banks, service providers in
that field and also key-terms relevant in the field of banking. The
search revealed more than 5000 patent documents which could possibly
have an impact on the activities of the bank.  The next step will be
to evaluate those patents regarding scope of protection and the
possible relevancy. The task being a list of patents which might be
infringed by the bank's business. Possible counter-measures are
envisaged in a next step (oppositions, negotiations etc.).

#tle: It is assumed that the evaluation of all patents will need two or
three patent experts, combined with two or three IT-experts from the
bank itself and a time period of three to six months. This is the
%(q:defensive) measure. As an %(q:offensive) measure it is advisable
that banks start filing patent applications wherever possible and
promising. Of course, the liberal practice in the U.S. requires to
file almost any business method for patent. These business methods are
always implemented by a computer (I have not seen any other example).
This computer implementation in most cases gives the opportunity to
look for some sort of a %(q:technical effect) or %(q:technical
consideration) or %(q:technical problem) etc. which might help to
obtain a European patent, in the long run. When studying a business
method computer software program it turns out that in most cases that
program includes at least one aspect (feature) which might qualify
under the European standards as %(q:technical). It is an advisable
measure for any bank these days to try to develop a patent portfolio
in order to have at least some arguments in hand when approached by
others for patent infringement.

#Aon: A regular report on patent strategies of various corporations quotes
him explaining why AT&T has been filing 7 times as many patents in
1999 as in 1997.

#WuW: We want to build picket fences around the technologies that we think
are most important for the future.

#Toe: Think Magazine 1990 #5 contains an article which explains IBM's patent
strategy by quoting Smith.

#Yde: You get value from patents in two ways: through fees, and through
licensing negotiations that give IBM access to other patents. The IBM
patent portfolio gains us the freedom to do what we need to do through
cross-licensing--it gives us access to the inventions of others that
are the key to rapid innovation. Access is far more valuable to IBM
than the fees it receives from its 9,000 active patents. There's no
direct calculation of this value, but it's many times larger than the
fee income, perhaps an order of magnitude larger.

#JnW: Josef Straus: Patente als Waffe hochentwickelter Länder

#Pei: Prof. Josef Straus, der in allen Kommittees der Patentorganisationen
(WIPO, AIPPI, EPA etc) vertreten ist und die Bundesregierung in
Patentfragen berät, betont immer wieder, dass (1) ohne Patente kein
Anreiz zu Neuerungen bestehe (2) Patente dazu dienen, die Dritte Welt
auf Distanz zu halten.  Vermutlich überschätzt Straus die Wirkung von
Patenten.

#Dgn: Da im System des freien Welthandels Produkte vielerorts billiger
hergestellt werden können als in den hochentwickelten Ländern, hängt
der wirtschaftliche Erfolg europäischer Unternehmen in zunehmendem
Maße davon ab, ob es ihnen gelingt, durch Innovationen
Wettbewerbsvorteile zu erlangen. Dieser Anreiz zu Innovationen kann
jedoch nur bestehen, wenn ein rechtlicher Schutz der Erfindungen
sichergestellt ist. Sind die Erfindungen rechtlich geschützt, können
sie als %(q:intellectual property) einen Hauptteil des
Unternehmensvermögens darstellen. Der Bedarf an Juristen,
Naturwissenschaftlern und Ingenieuren, die Kenntnisse im Patentrecht
haben, steigt dieser Entwicklung entsprechend.

#Mee: Microsoft to opensourcers 2001: Get your money and let's go to court

#Aoe: News report about a public discussion at the %(q:Open Source
Convention) between Microsoft executive Craig Mundie and well known
representatives of the free software community in July 2001. 
%(bc:Asked by CollabNet CTO Brian Behlendorf whether Microsoft will
enforce its patents against open source projects, Mundie replied,
%(q:Yes, absolutely.) An audience member pointed out that many open
source projects aren't funded and so can't afford legal representation
to rival Microsoft's. %(q:Oh well,) said Mundie. %(q:Get your money,
and let's go to court.))  Similar statements have been made by other
Microsoft officials, such as Steve Ballmer at the CeBit trade fair in
2002.

#tai: swpatcusku.de.txl

#sfq: send us further quotations

#Pae2: Please show this list of quotations to people who should be on it with
their own quote!

#BWg: Bitte bitten Sie SAP oder das Büro Tauss, die verschwundene Erklärung
von Prof. Plattner nachzuliefern!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatcusku ;
# txtlang: de ;
# multlin: t ;
# End: ;

