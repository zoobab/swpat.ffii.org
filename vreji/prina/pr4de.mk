pr4de.lstex:
	lstex pr4de | sort -u > pr4de.lstex
pr4de.mk:	pr4de.lstex
	vcat /ul/prg/RC/texmake > pr4de.mk


pr4de.dvi:	pr4de.mk pr4de.tex
	latex pr4de && while tail -n 20 pr4de.log | grep references && latex pr4de;do eval;done
	if test -r pr4de.idx;then makeindex pr4de && latex pr4de;fi
pr4de.pdf:	pr4de.mk pr4de.tex
	pdflatex pr4de && while tail -n 20 pr4de.log | grep references && pdflatex pr4de;do eval;done
	if test -r pr4de.idx;then makeindex pr4de && pdflatex pr4de;fi
pr4de.tty:	pr4de.dvi
	dvi2tty -q pr4de > pr4de.tty
pr4de.ps:	pr4de.dvi	
	dvips  pr4de
pr4de.001:	pr4de.dvi
	rm -f pr4de.[0-9][0-9][0-9]
	dvips -i -S 1  pr4de
pr4de.pbm:	pr4de.ps
	pstopbm pr4de.ps
pr4de.gif:	pr4de.ps
	pstogif pr4de.ps
pr4de.eps:	pr4de.dvi
	dvips -E -f pr4de > pr4de.eps
pr4de-01.g3n:	pr4de.ps
	ps2fax n pr4de.ps
pr4de-01.g3f:	pr4de.ps
	ps2fax f pr4de.ps
pr4de.ps.gz:	pr4de.ps
	gzip < pr4de.ps > pr4de.ps.gz
pr4de.ps.gz.uue:	pr4de.ps.gz
	uuencode pr4de.ps.gz pr4de.ps.gz > pr4de.ps.gz.uue
pr4de.faxsnd:	pr4de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo pr4de-??.g3n`;source faxsnd main
pr4de_tex.ps:	
	cat pr4de.tex | splitlong | coco | m2ps > pr4de_tex.ps
pr4de_tex.ps.gz:	pr4de_tex.ps
	gzip < pr4de_tex.ps > pr4de_tex.ps.gz
pr4de-01.pgm:
	cat pr4de.ps | gs -q -sDEVICE=pgm -sOutputFile=pr4de-%02d.pgm -
pr4de/pr4de.html:	pr4de.dvi
	rm -fR pr4de;latex2html pr4de.tex
xview:	pr4de.dvi
	xdvi -s 8 pr4de &
tview:	pr4de.tty
	browse pr4de.tty 
gview:	pr4de.ps
	ghostview  pr4de.ps &
aview:	pr4de.pdf
	acroread pr4de.pdf
print:	pr4de.ps
	lpr -s -h pr4de.ps 
sprint:	pr4de.001
	for F in pr4de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	pr4de.faxsnd
	faxsndjob view pr4de &
fsend:	pr4de.faxsnd
	faxsndjob jobs pr4de
viewgif:	pr4de.gif
	xv pr4de.gif &
viewpbm:	pr4de.pbm
	xv pr4de-??.pbm &
vieweps:	pr4de.eps
	ghostview pr4de.eps &	
clean:	pr4de.ps
	rm -f  pr4de-*.tex pr4de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} pr4de-??.* pr4de_tex.* pr4de*~
tgz:	clean
	set +f;LSFILES=`cat pr4de.ls???`;FILES=`ls pr4de.* $$LSFILES | sort -u`;tar czvf pr4de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
