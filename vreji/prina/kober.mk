kober.lstex:
	lstex kober | sort -u > kober.lstex
kober.mk:	kober.lstex
	vcat /ul/prg/RC/texmake > kober.mk


kober.dvi:	kober.mk kober.tex
	latex kober && while tail -n 20 kober.log | grep references && latex kober;do eval;done
	if test -r kober.idx;then makeindex kober && latex kober;fi
kober.tty:	kober.dvi
	dvi2tty -q kober > kober.tty
kober.ps:	kober.dvi	
	dvips  kober
kober.001:	kober.dvi
	rm -f kober.[0-9][0-9][0-9]
	dvips -i -S 1  kober
kober.pbm:	kober.ps
	pstopbm kober.ps
kober.gif:	kober.ps
	pstogif kober.ps
kober.eps:	kober.dvi
	dvips -E -f kober > kober.eps
kober-01.g3n:	kober.ps
	ps2fax n kober.ps
kober-01.g3f:	kober.ps
	ps2fax f kober.ps
kober.ps.gz:	kober.ps
	gzip < kober.ps > kober.ps.gz
kober.ps.gz.uue:	kober.ps.gz
	uuencode kober.ps.gz kober.ps.gz > kober.ps.gz.uue
kober.faxsnd:	kober-01.g3n
	set -a;FAXRES=n;FILELIST=`echo kober-??.g3n`;source faxsnd main
kober_tex.ps:	
	cat kober.tex | splitlong | coco | m2ps > kober_tex.ps
kober_tex.ps.gz:	kober_tex.ps
	gzip < kober_tex.ps > kober_tex.ps.gz
kober-01.pgm:
	cat kober.ps | gs -q -sDEVICE=pgm -sOutputFile=kober-%02d.pgm -
kober/kober.html:	kober.dvi
	rm -fR kober;latex2html kober.tex
xview:	kober.dvi
	xdvi -s 8 kober &
tview:	kober.tty
	browse kober.tty 
gview:	kober.ps
	ghostview  kober.ps &
print:	kober.ps
	lpr -s -h kober.ps 
sprint:	kober.001
	for F in kober.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	kober.faxsnd
	faxsndjob view kober &
fsend:	kober.faxsnd
	faxsndjob jobs kober
viewgif:	kober.gif
	xv kober.gif &
viewpbm:	kober.pbm
	xv kober-??.pbm &
vieweps:	kober.eps
	ghostview kober.eps &	
clean:	kober.ps
	rm -f  kober-*.tex kober.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} kober-??.* kober_tex.* kober*~
tgz:	clean
	set +f;LSFILES=`cat kober.ls???`;FILES=`ls kober.* $$LSFILES | sort -u`;tar czvf kober.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
