euipCAde.lstex:
	lstex euipCAde | sort -u > euipCAde.lstex
euipCAde.mk:	euipCAde.lstex
	vcat /ul/prg/RC/texmake > euipCAde.mk


euipCAde.dvi:	euipCAde.mk euipCAde.tex
	latex euipCAde && while tail -n 20 euipCAde.log | grep references && latex euipCAde;do eval;done
	if test -r euipCAde.idx;then makeindex euipCAde && latex euipCAde;fi
euipCAde.pdf:	euipCAde.mk euipCAde.tex
	pdflatex euipCAde && while tail -n 20 euipCAde.log | grep references && pdflatex euipCAde;do eval;done
	if test -r euipCAde.idx;then makeindex euipCAde && pdflatex euipCAde;fi
euipCAde.tty:	euipCAde.dvi
	dvi2tty -q euipCAde > euipCAde.tty
euipCAde.ps:	euipCAde.dvi	
	dvips  euipCAde
euipCAde.001:	euipCAde.dvi
	rm -f euipCAde.[0-9][0-9][0-9]
	dvips -i -S 1  euipCAde
euipCAde.pbm:	euipCAde.ps
	pstopbm euipCAde.ps
euipCAde.gif:	euipCAde.ps
	pstogif euipCAde.ps
euipCAde.eps:	euipCAde.dvi
	dvips -E -f euipCAde > euipCAde.eps
euipCAde-01.g3n:	euipCAde.ps
	ps2fax n euipCAde.ps
euipCAde-01.g3f:	euipCAde.ps
	ps2fax f euipCAde.ps
euipCAde.ps.gz:	euipCAde.ps
	gzip < euipCAde.ps > euipCAde.ps.gz
euipCAde.ps.gz.uue:	euipCAde.ps.gz
	uuencode euipCAde.ps.gz euipCAde.ps.gz > euipCAde.ps.gz.uue
euipCAde.faxsnd:	euipCAde-01.g3n
	set -a;FAXRES=n;FILELIST=`echo euipCAde-??.g3n`;source faxsnd main
euipCAde_tex.ps:	
	cat euipCAde.tex | splitlong | coco | m2ps > euipCAde_tex.ps
euipCAde_tex.ps.gz:	euipCAde_tex.ps
	gzip < euipCAde_tex.ps > euipCAde_tex.ps.gz
euipCAde-01.pgm:
	cat euipCAde.ps | gs -q -sDEVICE=pgm -sOutputFile=euipCAde-%02d.pgm -
euipCAde/euipCAde.html:	euipCAde.dvi
	rm -fR euipCAde;latex2html euipCAde.tex
xview:	euipCAde.dvi
	xdvi -s 8 euipCAde &
tview:	euipCAde.tty
	browse euipCAde.tty 
gview:	euipCAde.ps
	ghostview  euipCAde.ps &
aview:	euipCAde.pdf
	acroread euipCAde.pdf
print:	euipCAde.ps
	lpr -s -h euipCAde.ps 
sprint:	euipCAde.001
	for F in euipCAde.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	euipCAde.faxsnd
	faxsndjob view euipCAde &
fsend:	euipCAde.faxsnd
	faxsndjob jobs euipCAde
viewgif:	euipCAde.gif
	xv euipCAde.gif &
viewpbm:	euipCAde.pbm
	xv euipCAde-??.pbm &
vieweps:	euipCAde.eps
	ghostview euipCAde.eps &	
clean:	euipCAde.ps
	rm -f  euipCAde-*.tex euipCAde.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} euipCAde-??.* euipCAde_tex.* euipCAde*~
tgz:	clean
	set +f;LSFILES=`cat euipCAde.ls???`;FILES=`ls euipCAde.* $$LSFILES | sort -u`;tar czvf euipCAde.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
