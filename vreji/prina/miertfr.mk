miertfr.lstex:
	lstex miertfr | sort -u > miertfr.lstex
miertfr.mk:	miertfr.lstex
	vcat /ul/prg/RC/texmake > miertfr.mk


miertfr.dvi:	miertfr.mk miertfr.tex
	latex miertfr && while tail -n 20 miertfr.log | grep references && latex miertfr;do eval;done
	if test -r miertfr.idx;then makeindex miertfr && latex miertfr;fi
miertfr.tty:	miertfr.dvi
	dvi2tty -q miertfr > miertfr.tty
miertfr.ps:	miertfr.dvi	
	dvips  miertfr
miertfr.001:	miertfr.dvi
	rm -f miertfr.[0-9][0-9][0-9]
	dvips -i -S 1  miertfr
miertfr.pbm:	miertfr.ps
	pstopbm miertfr.ps
miertfr.gif:	miertfr.ps
	pstogif miertfr.ps
miertfr.eps:	miertfr.dvi
	dvips -E -f miertfr > miertfr.eps
miertfr-01.g3n:	miertfr.ps
	ps2fax n miertfr.ps
miertfr-01.g3f:	miertfr.ps
	ps2fax f miertfr.ps
miertfr.ps.gz:	miertfr.ps
	gzip < miertfr.ps > miertfr.ps.gz
miertfr.ps.gz.uue:	miertfr.ps.gz
	uuencode miertfr.ps.gz miertfr.ps.gz > miertfr.ps.gz.uue
miertfr.faxsnd:	miertfr-01.g3n
	set -a;FAXRES=n;FILELIST=`echo miertfr-??.g3n`;source faxsnd main
miertfr_tex.ps:	
	cat miertfr.tex | splitlong | coco | m2ps > miertfr_tex.ps
miertfr_tex.ps.gz:	miertfr_tex.ps
	gzip < miertfr_tex.ps > miertfr_tex.ps.gz
miertfr-01.pgm:
	cat miertfr.ps | gs -q -sDEVICE=pgm -sOutputFile=miertfr-%02d.pgm -
miertfr/miertfr.html:	miertfr.dvi
	rm -fR miertfr;latex2html miertfr.tex
xview:	miertfr.dvi
	xdvi -s 8 miertfr &
tview:	miertfr.tty
	browse miertfr.tty 
gview:	miertfr.ps
	ghostview  miertfr.ps &
print:	miertfr.ps
	lpr -s -h miertfr.ps 
sprint:	miertfr.001
	for F in miertfr.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	miertfr.faxsnd
	faxsndjob view miertfr &
fsend:	miertfr.faxsnd
	faxsndjob jobs miertfr
viewgif:	miertfr.gif
	xv miertfr.gif &
viewpbm:	miertfr.pbm
	xv miertfr-??.pbm &
vieweps:	miertfr.eps
	ghostview miertfr.eps &	
clean:	miertfr.ps
	rm -f  miertfr-*.tex miertfr.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} miertfr-??.* miertfr_tex.* miertfr*~
tgz:	clean
	set +f;LSFILES=`cat miertfr.ls???`;FILES=`ls miertfr.* $$LSFILES | sort -u`;tar czvf miertfr.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
