miertde.lstex:
	lstex miertde | sort -u > miertde.lstex
miertde.mk:	miertde.lstex
	vcat /ul/prg/RC/texmake > miertde.mk


miertde.dvi:	miertde.mk miertde.tex
	latex miertde && while tail -n 20 miertde.log | grep references && latex miertde;do eval;done
	if test -r miertde.idx;then makeindex miertde && latex miertde;fi
miertde.tty:	miertde.dvi
	dvi2tty -q miertde > miertde.tty
miertde.ps:	miertde.dvi	
	dvips  miertde
miertde.001:	miertde.dvi
	rm -f miertde.[0-9][0-9][0-9]
	dvips -i -S 1  miertde
miertde.pbm:	miertde.ps
	pstopbm miertde.ps
miertde.gif:	miertde.ps
	pstogif miertde.ps
miertde.eps:	miertde.dvi
	dvips -E -f miertde > miertde.eps
miertde-01.g3n:	miertde.ps
	ps2fax n miertde.ps
miertde-01.g3f:	miertde.ps
	ps2fax f miertde.ps
miertde.ps.gz:	miertde.ps
	gzip < miertde.ps > miertde.ps.gz
miertde.ps.gz.uue:	miertde.ps.gz
	uuencode miertde.ps.gz miertde.ps.gz > miertde.ps.gz.uue
miertde.faxsnd:	miertde-01.g3n
	set -a;FAXRES=n;FILELIST=`echo miertde-??.g3n`;source faxsnd main
miertde_tex.ps:	
	cat miertde.tex | splitlong | coco | m2ps > miertde_tex.ps
miertde_tex.ps.gz:	miertde_tex.ps
	gzip < miertde_tex.ps > miertde_tex.ps.gz
miertde-01.pgm:
	cat miertde.ps | gs -q -sDEVICE=pgm -sOutputFile=miertde-%02d.pgm -
miertde/miertde.html:	miertde.dvi
	rm -fR miertde;latex2html miertde.tex
xview:	miertde.dvi
	xdvi -s 8 miertde &
tview:	miertde.tty
	browse miertde.tty 
gview:	miertde.ps
	ghostview  miertde.ps &
print:	miertde.ps
	lpr -s -h miertde.ps 
sprint:	miertde.001
	for F in miertde.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	miertde.faxsnd
	faxsndjob view miertde &
fsend:	miertde.faxsnd
	faxsndjob jobs miertde
viewgif:	miertde.gif
	xv miertde.gif &
viewpbm:	miertde.pbm
	xv miertde-??.pbm &
vieweps:	miertde.eps
	ghostview miertde.eps &	
clean:	miertde.ps
	rm -f  miertde-*.tex miertde.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} miertde-??.* miertde_tex.* miertde*~
tgz:	clean
	set +f;LSFILES=`cat miertde.ls???`;FILES=`ls miertde.* $$LSFILES | sort -u`;tar czvf miertde.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
