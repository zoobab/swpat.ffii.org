gorton.lstex:
	lstex gorton | sort -u > gorton.lstex
gorton.mk:	gorton.lstex
	vcat /ul/prg/RC/texmake > gorton.mk


gorton.dvi:	gorton.mk gorton.tex
	latex gorton && while tail -n 20 gorton.log | grep references && latex gorton;do eval;done
	if test -r gorton.idx;then makeindex gorton && latex gorton;fi
gorton.pdf:	gorton.mk gorton.tex
	pdflatex gorton && while tail -n 20 gorton.log | grep references && pdflatex gorton;do eval;done
	if test -r gorton.idx;then makeindex gorton && pdflatex gorton;fi
gorton.tty:	gorton.dvi
	dvi2tty -q gorton > gorton.tty
gorton.ps:	gorton.dvi	
	dvips  gorton
gorton.001:	gorton.dvi
	rm -f gorton.[0-9][0-9][0-9]
	dvips -i -S 1  gorton
gorton.pbm:	gorton.ps
	pstopbm gorton.ps
gorton.gif:	gorton.ps
	pstogif gorton.ps
gorton.eps:	gorton.dvi
	dvips -E -f gorton > gorton.eps
gorton-01.g3n:	gorton.ps
	ps2fax n gorton.ps
gorton-01.g3f:	gorton.ps
	ps2fax f gorton.ps
gorton.ps.gz:	gorton.ps
	gzip < gorton.ps > gorton.ps.gz
gorton.ps.gz.uue:	gorton.ps.gz
	uuencode gorton.ps.gz gorton.ps.gz > gorton.ps.gz.uue
gorton.faxsnd:	gorton-01.g3n
	set -a;FAXRES=n;FILELIST=`echo gorton-??.g3n`;source faxsnd main
gorton_tex.ps:	
	cat gorton.tex | splitlong | coco | m2ps > gorton_tex.ps
gorton_tex.ps.gz:	gorton_tex.ps
	gzip < gorton_tex.ps > gorton_tex.ps.gz
gorton-01.pgm:
	cat gorton.ps | gs -q -sDEVICE=pgm -sOutputFile=gorton-%02d.pgm -
gorton/gorton.html:	gorton.dvi
	rm -fR gorton;latex2html gorton.tex
xview:	gorton.dvi
	xdvi -s 8 gorton &
tview:	gorton.tty
	browse gorton.tty 
gview:	gorton.ps
	ghostview  gorton.ps &
print:	gorton.ps
	lpr -s -h gorton.ps 
sprint:	gorton.001
	for F in gorton.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	gorton.faxsnd
	faxsndjob view gorton &
fsend:	gorton.faxsnd
	faxsndjob jobs gorton
viewgif:	gorton.gif
	xv gorton.gif &
viewpbm:	gorton.pbm
	xv gorton-??.pbm &
vieweps:	gorton.eps
	ghostview gorton.eps &	
clean:	gorton.ps
	rm -f  gorton-*.tex gorton.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} gorton-??.* gorton_tex.* gorton*~
tgz:	clean
	set +f;LSFILES=`cat gorton.ls???`;FILES=`ls gorton.* $$LSFILES | sort -u`;tar czvf gorton.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
