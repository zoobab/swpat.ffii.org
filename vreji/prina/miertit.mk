miertit.lstex:
	lstex miertit | sort -u > miertit.lstex
miertit.mk:	miertit.lstex
	vcat /ul/prg/RC/texmake > miertit.mk


miertit.dvi:	miertit.mk miertit.tex
	latex miertit && while tail -n 20 miertit.log | grep references && latex miertit;do eval;done
	if test -r miertit.idx;then makeindex miertit && latex miertit;fi
miertit.tty:	miertit.dvi
	dvi2tty -q miertit > miertit.tty
miertit.ps:	miertit.dvi	
	dvips  miertit
miertit.001:	miertit.dvi
	rm -f miertit.[0-9][0-9][0-9]
	dvips -i -S 1  miertit
miertit.pbm:	miertit.ps
	pstopbm miertit.ps
miertit.gif:	miertit.ps
	pstogif miertit.ps
miertit.eps:	miertit.dvi
	dvips -E -f miertit > miertit.eps
miertit-01.g3n:	miertit.ps
	ps2fax n miertit.ps
miertit-01.g3f:	miertit.ps
	ps2fax f miertit.ps
miertit.ps.gz:	miertit.ps
	gzip < miertit.ps > miertit.ps.gz
miertit.ps.gz.uue:	miertit.ps.gz
	uuencode miertit.ps.gz miertit.ps.gz > miertit.ps.gz.uue
miertit.faxsnd:	miertit-01.g3n
	set -a;FAXRES=n;FILELIST=`echo miertit-??.g3n`;source faxsnd main
miertit_tex.ps:	
	cat miertit.tex | splitlong | coco | m2ps > miertit_tex.ps
miertit_tex.ps.gz:	miertit_tex.ps
	gzip < miertit_tex.ps > miertit_tex.ps.gz
miertit-01.pgm:
	cat miertit.ps | gs -q -sDEVICE=pgm -sOutputFile=miertit-%02d.pgm -
miertit/miertit.html:	miertit.dvi
	rm -fR miertit;latex2html miertit.tex
xview:	miertit.dvi
	xdvi -s 8 miertit &
tview:	miertit.tty
	browse miertit.tty 
gview:	miertit.ps
	ghostview  miertit.ps &
print:	miertit.ps
	lpr -s -h miertit.ps 
sprint:	miertit.001
	for F in miertit.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	miertit.faxsnd
	faxsndjob view miertit &
fsend:	miertit.faxsnd
	faxsndjob jobs miertit
viewgif:	miertit.gif
	xv miertit.gif &
viewpbm:	miertit.pbm
	xv miertit-??.pbm &
vieweps:	miertit.eps
	ghostview miertit.eps &	
clean:	miertit.ps
	rm -f  miertit-*.tex miertit.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} miertit-??.* miertit_tex.* miertit*~
tgz:	clean
	set +f;LSFILES=`cat miertit.ls???`;FILES=`ls miertit.* $$LSFILES | sort -u`;tar czvf miertit.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
