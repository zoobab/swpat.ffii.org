itgrpatde.lstex:
	lstex itgrpatde | sort -u > itgrpatde.lstex
itgrpatde.mk:	itgrpatde.lstex
	vcat /ul/prg/RC/texmake > itgrpatde.mk


itgrpatde.dvi:	itgrpatde.mk itgrpatde.tex
	latex itgrpatde && while tail -n 20 itgrpatde.log | grep references && latex itgrpatde;do eval;done
	if test -r itgrpatde.idx;then makeindex itgrpatde && latex itgrpatde;fi
itgrpatde.tty:	itgrpatde.dvi
	dvi2tty -q itgrpatde > itgrpatde.tty
itgrpatde.ps:	itgrpatde.dvi	
	dvips  itgrpatde
itgrpatde.001:	itgrpatde.dvi
	rm -f itgrpatde.[0-9][0-9][0-9]
	dvips -i -S 1  itgrpatde
itgrpatde.pbm:	itgrpatde.ps
	pstopbm itgrpatde.ps
itgrpatde.gif:	itgrpatde.ps
	pstogif itgrpatde.ps
itgrpatde.eps:	itgrpatde.dvi
	dvips -E -f itgrpatde > itgrpatde.eps
itgrpatde-01.g3n:	itgrpatde.ps
	ps2fax n itgrpatde.ps
itgrpatde-01.g3f:	itgrpatde.ps
	ps2fax f itgrpatde.ps
itgrpatde.ps.gz:	itgrpatde.ps
	gzip < itgrpatde.ps > itgrpatde.ps.gz
itgrpatde.ps.gz.uue:	itgrpatde.ps.gz
	uuencode itgrpatde.ps.gz itgrpatde.ps.gz > itgrpatde.ps.gz.uue
itgrpatde.faxsnd:	itgrpatde-01.g3n
	set -a;FAXRES=n;FILELIST=`echo itgrpatde-??.g3n`;source faxsnd main
itgrpatde_tex.ps:	
	cat itgrpatde.tex | splitlong | coco | m2ps > itgrpatde_tex.ps
itgrpatde_tex.ps.gz:	itgrpatde_tex.ps
	gzip < itgrpatde_tex.ps > itgrpatde_tex.ps.gz
itgrpatde-01.pgm:
	cat itgrpatde.ps | gs -q -sDEVICE=pgm -sOutputFile=itgrpatde-%02d.pgm -
itgrpatde/itgrpatde.html:	itgrpatde.dvi
	rm -fR itgrpatde;latex2html itgrpatde.tex
xview:	itgrpatde.dvi
	xdvi -s 8 itgrpatde &
tview:	itgrpatde.tty
	browse itgrpatde.tty 
gview:	itgrpatde.ps
	ghostview  itgrpatde.ps &
print:	itgrpatde.ps
	lpr -s -h itgrpatde.ps 
sprint:	itgrpatde.001
	for F in itgrpatde.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	itgrpatde.faxsnd
	faxsndjob view itgrpatde &
fsend:	itgrpatde.faxsnd
	faxsndjob jobs itgrpatde
viewgif:	itgrpatde.gif
	xv itgrpatde.gif &
viewpbm:	itgrpatde.pbm
	xv itgrpatde-??.pbm &
vieweps:	itgrpatde.eps
	ghostview itgrpatde.eps &	
clean:	itgrpatde.ps
	rm -f  itgrpatde-*.tex itgrpatde.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} itgrpatde-??.* itgrpatde_tex.* itgrpatde*~
tgz:	clean
	set +f;LSFILES=`cat itgrpatde.ls???`;FILES=`ls itgrpatde.* $$LSFILES | sort -u`;tar czvf itgrpatde.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
