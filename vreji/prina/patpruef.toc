\contentsline {section}{\numberline {1}Ein Programm als solches}{2}
\contentsline {subsection}{\numberline {1.1}Der objektive Erfindungsgegenstand}{3}
\contentsline {subsection}{\numberline {1.2}Erfindungen m�ssen L�sungen sein}{3}
\contentsline {section}{\numberline {2}Der technische Charakter}{5}
\contentsline {subsection}{\numberline {2.1}Die Grundannahme}{6}
\contentsline {subsection}{\numberline {2.2}Der Umkehrschluss}{6}
\contentsline {subsection}{\numberline {2.3}Ausschlu\active@dq \dq@prtct {s} auch technischer Lehren}{7}
\contentsline {subsection}{\numberline {2.4}Technizit�t von Computerprogrammen}{7}
\contentsline {section}{\numberline {3}Der Technikbegriff}{8}
\contentsline {subsection}{\numberline {3.1}Sprachanalyseeinrichtung und Logikverifikation}{9}
\contentsline {subsection}{\numberline {3.2}Die Glocke}{10}
\contentsline {subsection}{\numberline {3.3}Zus�tzlicher technischer Effekt}{10}
\contentsline {subsection}{\numberline {3.4}Programme zur Hardwarebeschreibung}{11}
\contentsline {subsection}{\numberline {3.5}Grenzen des Technikbegriffs}{11}
\contentsline {section}{\numberline {4}Der objektive Schutzgegenstand}{12}
\contentsline {subsection}{\numberline {4.1}Offenbarung des Programmcodes}{13}
\contentsline {subsection}{\numberline {4.2}Patentierung von L�sungen}{13}
\contentsline {subsection}{\numberline {4.3}Programme als L�sungen}{14}
\contentsline {subsubsection}{\numberline {4.3.1}Idee und L�sung}{15}
\contentsline {section}{\numberline {5}Der Ausschlu\active@dq \dq@prtct {s}katalog als gesetzlicher Disclaimer}{16}
\contentsline {section}{\numberline {6}Quellcode im Patentanspruch}{18}
\contentsline {subsection}{\numberline {6.1}Klarheit von Quellcode}{18}
\contentsline {subsection}{\numberline {6.2}Die wesentlichen Merkmale}{19}
\contentsline {subsection}{\numberline {6.3}Schutzbereich von Patentanspr�chen mit Quellcode}{19}
\contentsline {section}{\numberline {7}Schlussbemerkung}{20}
