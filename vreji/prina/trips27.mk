trips27.lstex:
	lstex trips27 | sort -u > trips27.lstex
trips27.mk:	trips27.lstex
	vcat /ul/prg/RC/texmake > trips27.mk


trips27.dvi:	trips27.mk trips27.tex
	latex trips27 && while tail -n 20 trips27.log | grep references && latex trips27;do eval;done
	if test -r trips27.idx;then makeindex trips27 && latex trips27;fi
trips27.pdf:	trips27.mk trips27.tex
	pdflatex trips27 && while tail -n 20 trips27.log | grep references && pdflatex trips27;do eval;done
	if test -r trips27.idx;then makeindex trips27 && pdflatex trips27;fi
trips27.tty:	trips27.dvi
	dvi2tty -q trips27 > trips27.tty
trips27.ps:	trips27.dvi	
	dvips  trips27
trips27.001:	trips27.dvi
	rm -f trips27.[0-9][0-9][0-9]
	dvips -i -S 1  trips27
trips27.pbm:	trips27.ps
	pstopbm trips27.ps
trips27.gif:	trips27.ps
	pstogif trips27.ps
trips27.eps:	trips27.dvi
	dvips -E -f trips27 > trips27.eps
trips27-01.g3n:	trips27.ps
	ps2fax n trips27.ps
trips27-01.g3f:	trips27.ps
	ps2fax f trips27.ps
trips27.ps.gz:	trips27.ps
	gzip < trips27.ps > trips27.ps.gz
trips27.ps.gz.uue:	trips27.ps.gz
	uuencode trips27.ps.gz trips27.ps.gz > trips27.ps.gz.uue
trips27.faxsnd:	trips27-01.g3n
	set -a;FAXRES=n;FILELIST=`echo trips27-??.g3n`;source faxsnd main
trips27_tex.ps:	
	cat trips27.tex | splitlong | coco | m2ps > trips27_tex.ps
trips27_tex.ps.gz:	trips27_tex.ps
	gzip < trips27_tex.ps > trips27_tex.ps.gz
trips27-01.pgm:
	cat trips27.ps | gs -q -sDEVICE=pgm -sOutputFile=trips27-%02d.pgm -
trips27/trips27.html:	trips27.dvi
	rm -fR trips27;latex2html trips27.tex
xview:	trips27.dvi
	xdvi -s 8 trips27 &
tview:	trips27.tty
	browse trips27.tty 
gview:	trips27.ps
	ghostview  trips27.ps &
aview:	trips27.pdf
	acroread trips27.pdf
print:	trips27.ps
	lpr -s -h trips27.ps 
sprint:	trips27.001
	for F in trips27.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	trips27.faxsnd
	faxsndjob view trips27 &
fsend:	trips27.faxsnd
	faxsndjob jobs trips27
viewgif:	trips27.gif
	xv trips27.gif &
viewpbm:	trips27.pbm
	xv trips27-??.pbm &
vieweps:	trips27.eps
	ghostview trips27.eps &	
clean:	trips27.ps
	rm -f  trips27-*.tex trips27.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} trips27-??.* trips27_tex.* trips27*~
tgz:	clean
	set +f;LSFILES=`cat trips27.ls???`;FILES=`ls trips27.* $$LSFILES | sort -u`;tar czvf trips27.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
