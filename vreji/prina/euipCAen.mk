euipCAen.lstex:
	lstex euipCAen | sort -u > euipCAen.lstex
euipCAen.mk:	euipCAen.lstex
	vcat /ul/prg/RC/texmake > euipCAen.mk


euipCAen.dvi:	euipCAen.mk euipCAen.tex
	latex euipCAen && while tail -n 20 euipCAen.log | grep references && latex euipCAen;do eval;done
	if test -r euipCAen.idx;then makeindex euipCAen && latex euipCAen;fi
euipCAen.tty:	euipCAen.dvi
	dvi2tty -q euipCAen > euipCAen.tty
euipCAen.ps:	euipCAen.dvi	
	dvips  euipCAen
euipCAen.001:	euipCAen.dvi
	rm -f euipCAen.[0-9][0-9][0-9]
	dvips -i -S 1  euipCAen
euipCAen.pbm:	euipCAen.ps
	pstopbm euipCAen.ps
euipCAen.gif:	euipCAen.ps
	pstogif euipCAen.ps
euipCAen.eps:	euipCAen.dvi
	dvips -E -f euipCAen > euipCAen.eps
euipCAen-01.g3n:	euipCAen.ps
	ps2fax n euipCAen.ps
euipCAen-01.g3f:	euipCAen.ps
	ps2fax f euipCAen.ps
euipCAen.ps.gz:	euipCAen.ps
	gzip < euipCAen.ps > euipCAen.ps.gz
euipCAen.ps.gz.uue:	euipCAen.ps.gz
	uuencode euipCAen.ps.gz euipCAen.ps.gz > euipCAen.ps.gz.uue
euipCAen.faxsnd:	euipCAen-01.g3n
	set -a;FAXRES=n;FILELIST=`echo euipCAen-??.g3n`;source faxsnd main
euipCAen_tex.ps:	
	cat euipCAen.tex | splitlong | coco | m2ps > euipCAen_tex.ps
euipCAen_tex.ps.gz:	euipCAen_tex.ps
	gzip < euipCAen_tex.ps > euipCAen_tex.ps.gz
euipCAen-01.pgm:
	cat euipCAen.ps | gs -q -sDEVICE=pgm -sOutputFile=euipCAen-%02d.pgm -
euipCAen/euipCAen.html:	euipCAen.dvi
	rm -fR euipCAen;latex2html euipCAen.tex
xview:	euipCAen.dvi
	xdvi -s 8 euipCAen &
tview:	euipCAen.tty
	browse euipCAen.tty 
gview:	euipCAen.ps
	ghostview  euipCAen.ps &
print:	euipCAen.ps
	lpr -s -h euipCAen.ps 
sprint:	euipCAen.001
	for F in euipCAen.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	euipCAen.faxsnd
	faxsndjob view euipCAen &
fsend:	euipCAen.faxsnd
	faxsndjob jobs euipCAen
viewgif:	euipCAen.gif
	xv euipCAen.gif &
viewpbm:	euipCAen.pbm
	xv euipCAen-??.pbm &
vieweps:	euipCAen.eps
	ghostview euipCAen.eps &	
clean:	euipCAen.ps
	rm -f  euipCAen-*.tex euipCAen.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} euipCAen-??.* euipCAen_tex.* euipCAen*~
tgz:	clean
	set +f;LSFILES=`cat euipCAen.ls???`;FILES=`ls euipCAen.* $$LSFILES | sort -u`;tar czvf euipCAen.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
