\contentsline {section}{\numberline {1}DG XV\ (Noteboom, Mueller, Ravillard)}{2}
\contentsline {section}{\numberline {2}EuroLinux\ (Smets)}{2}
\contentsline {section}{\numberline {3}DG XV\ (Noteboom)}{3}
\contentsline {section}{\numberline {4}SuSE\ (Pilch)}{3}
\contentsline {section}{\numberline {5}DG XV\ (Noteboom)}{4}
\contentsline {section}{\numberline {6}Infomatec\ (Perlzweig)}{4}
\contentsline {section}{\numberline {7}Prosa\ (Didone)}{5}
\contentsline {section}{\numberline {8}Linux-Verband\ (Siepman)}{5}
\contentsline {section}{\numberline {9}Net Presenter\ (Hoen)}{6}
\contentsline {section}{\numberline {10}EuroLinux\ (Smets)}{6}
\contentsline {section}{\numberline {11}DG XV\ (Noteboom)}{7}
\contentsline {section}{\numberline {12}Side talks}{7}
\contentsline {subsection}{\numberline {12.1}Inventive step not an obstacle}{7}
\contentsline {subsection}{\numberline {12.2}Patent organisations want to make programs patentable}{7}
\contentsline {subsection}{\numberline {12.3}Utility directive may be changed}{7}
\contentsline {subsection}{\numberline {12.4}``Equality'' with Hardware or with Books?}{8}
\contentsline {subsection}{\numberline {12.5}No more conferences, unless hosted by Eurolinux}{8}
\contentsline {subsection}{\numberline {12.6}EU lawmakers reluctant to innovate}{8}
