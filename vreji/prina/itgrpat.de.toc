\contentsline {section}{\numberline {1}Wann sind Patente legitim und n\newumlaut utzlich?}{2}
\contentsline {section}{\numberline {2}Wege zur Legitimit\newumlaut at von Softwarepatenten}{2}
\contentsline {subsection}{\numberline {2.1}Mi\ss {}st\newumlaut ande}{2}
\contentsline {subsubsection}{\numberline {2.1.1}Gummibegriff ``Erfindungsh\newumlaut ohe''}{2}
\contentsline {subsubsection}{\numberline {2.1.2}Neuheit in der Praxis schwer nachpr\newumlaut ufbar}{3}
\contentsline {subsection}{\numberline {2.2}L\newumlaut osungsvorschl\newumlaut age}{3}
\contentsline {subsubsection}{\numberline {2.2.1}Offener Wettbewerb der L\newumlaut osungsanbieter}{3}
\contentsline {subsubsection}{\numberline {2.2.2}Stand der Technik mit digitalem Zeitstempel archivieren}{4}
\contentsline {section}{\numberline {3}Wege zur volkswirtschaftlichen Ausgewogenheit von Softwarepatenten}{4}
\contentsline {subsection}{\numberline {3.1}Mi\ss {}st\newumlaut ande}{4}
\contentsline {subsubsection}{\numberline {3.1.1}Softwarepatente f\newumlaut ordern Rechtsunsicherheit}{5}
\contentsline {subsubsection}{\numberline {3.1.2}Softwarepatente f\newumlaut ordern Geheimhaltung}{5}
\contentsline {subsubsection}{\numberline {3.1.3}Softwarepatente machen den Anwender erpressbar}{5}
\contentsline {subsubsection}{\numberline {3.1.4}Softwarepatente f\newumlaut ordern Monopole und zerst\newumlaut oren ihren eigenen Markt}{5}
\contentsline {subsubsection}{\numberline {3.1.5}Softwarepatente behindern den Informationsfluss}{6}
\contentsline {subsubsection}{\numberline {3.1.6}Softwarepatente etablieren die Herrschaft von Nicht-Fachleuten}{6}
\contentsline {subsubsection}{\numberline {3.1.7}Patent auf ``Automobil'' bis zum Jahr 2199?}{6}
\contentsline {subsection}{\numberline {3.2}L\newumlaut osungsvorschl\newumlaut age}{6}
\contentsline {subsubsection}{\numberline {3.2.1}Transparente Lizenzbedingungen, Verwertungsgesellschaft}{6}
\contentsline {subsubsection}{\numberline {3.2.2}Freiheit der Implementierung}{7}
\contentsline {subsubsection}{\numberline {3.2.3}Freiheit der Weiterverteilung}{7}
\contentsline {subsubsection}{\numberline {3.2.4}Geb\newumlaut uhreneinzug durch Schl\newumlaut usselsystem}{7}
\contentsline {subsubsection}{\numberline {3.2.5}Strafe nur nach Warnung}{7}
\contentsline {subsubsection}{\numberline {3.2.6}Dekompilierungsverbote aufheben}{7}
\contentsline {subsubsection}{\numberline {3.2.7}Entscheiden, ob mit offenen oder mit geschlossenen Karten gespielt werden soll}{8}
\contentsline {subsubsection}{\numberline {3.2.8}Eigentumsrechte nur auf propriet\newumlaut are Gegenst\newumlaut ande anwenden}{8}
\contentsline {subsubsection}{\numberline {3.2.9}Geltungszeitraum reduzieren}{8}
\contentsline {subsubsection}{\numberline {3.2.10}Die Fach\newumlaut offentlichkeit als Richter einbeziehen}{8}
\contentsline {subsubsection}{\numberline {3.2.11}Verantwortung f\newumlaut ur Kosten propriet\newumlaut arer Standards regeln}{8}
\contentsline {section}{\numberline {4}Vertragsdickicht blockiert die Wege}{8}
