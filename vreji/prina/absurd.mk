absurd.lstex:
	lstex absurd | sort -u > absurd.lstex
absurd.mk:	absurd.lstex
	vcat /ul/prg/RC/texmake > absurd.mk


absurd.dvi:	absurd.mk absurd.tex
	latex absurd && while tail -n 20 absurd.log | grep references && latex absurd;do eval;done
	if test -r absurd.idx;then makeindex absurd && latex absurd;fi
absurd.tty:	absurd.dvi
	dvi2tty -q absurd > absurd.tty
absurd.ps:	absurd.dvi	
	dvips  absurd
absurd.001:	absurd.dvi
	rm -f absurd.[0-9][0-9][0-9]
	dvips -i -S 1  absurd
absurd.pbm:	absurd.ps
	pstopbm absurd.ps
absurd.gif:	absurd.ps
	pstogif absurd.ps
absurd.eps:	absurd.dvi
	dvips -E -f absurd > absurd.eps
absurd-01.g3n:	absurd.ps
	ps2fax n absurd.ps
absurd-01.g3f:	absurd.ps
	ps2fax f absurd.ps
absurd.ps.gz:	absurd.ps
	gzip < absurd.ps > absurd.ps.gz
absurd.ps.gz.uue:	absurd.ps.gz
	uuencode absurd.ps.gz absurd.ps.gz > absurd.ps.gz.uue
absurd.faxsnd:	absurd-01.g3n
	set -a;FAXRES=n;FILELIST=`echo absurd-??.g3n`;source faxsnd main
absurd_tex.ps:	
	cat absurd.tex | splitlong | coco | m2ps > absurd_tex.ps
absurd_tex.ps.gz:	absurd_tex.ps
	gzip < absurd_tex.ps > absurd_tex.ps.gz
absurd-01.pgm:
	cat absurd.ps | gs -q -sDEVICE=pgm -sOutputFile=absurd-%02d.pgm -
absurd/absurd.html:	absurd.dvi
	rm -fR absurd;latex2html absurd.tex
xview:	absurd.dvi
	xdvi -s 8 absurd &
tview:	absurd.tty
	browse absurd.tty 
gview:	absurd.ps
	ghostview  absurd.ps &
print:	absurd.ps
	lpr -s -h absurd.ps 
sprint:	absurd.001
	for F in absurd.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	absurd.faxsnd
	faxsndjob view absurd &
fsend:	absurd.faxsnd
	faxsndjob jobs absurd
viewgif:	absurd.gif
	xv absurd.gif &
viewpbm:	absurd.pbm
	xv absurd-??.pbm &
vieweps:	absurd.eps
	ghostview absurd.eps &	
clean:	absurd.ps
	rm -f  absurd-*.tex absurd.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} absurd-??.* absurd_tex.* absurd*~
tgz:	clean
	set +f;LSFILES=`cat absurd.ls???`;FILES=`ls absurd.* $$LSFILES | sort -u`;tar czvf absurd.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
