mierten.lstex:
	lstex mierten | sort -u > mierten.lstex
mierten.mk:	mierten.lstex
	vcat /ul/prg/RC/texmake > mierten.mk


mierten.dvi:	mierten.mk mierten.tex
	latex mierten && while tail -n 20 mierten.log | grep references && latex mierten;do eval;done
	if test -r mierten.idx;then makeindex mierten && latex mierten;fi
mierten.tty:	mierten.dvi
	dvi2tty -q mierten > mierten.tty
mierten.ps:	mierten.dvi	
	dvips  mierten
mierten.001:	mierten.dvi
	rm -f mierten.[0-9][0-9][0-9]
	dvips -i -S 1  mierten
mierten.pbm:	mierten.ps
	pstopbm mierten.ps
mierten.gif:	mierten.ps
	pstogif mierten.ps
mierten.eps:	mierten.dvi
	dvips -E -f mierten > mierten.eps
mierten-01.g3n:	mierten.ps
	ps2fax n mierten.ps
mierten-01.g3f:	mierten.ps
	ps2fax f mierten.ps
mierten.ps.gz:	mierten.ps
	gzip < mierten.ps > mierten.ps.gz
mierten.ps.gz.uue:	mierten.ps.gz
	uuencode mierten.ps.gz mierten.ps.gz > mierten.ps.gz.uue
mierten.faxsnd:	mierten-01.g3n
	set -a;FAXRES=n;FILELIST=`echo mierten-??.g3n`;source faxsnd main
mierten_tex.ps:	
	cat mierten.tex | splitlong | coco | m2ps > mierten_tex.ps
mierten_tex.ps.gz:	mierten_tex.ps
	gzip < mierten_tex.ps > mierten_tex.ps.gz
mierten-01.pgm:
	cat mierten.ps | gs -q -sDEVICE=pgm -sOutputFile=mierten-%02d.pgm -
mierten/mierten.html:	mierten.dvi
	rm -fR mierten;latex2html mierten.tex
xview:	mierten.dvi
	xdvi -s 8 mierten &
tview:	mierten.tty
	browse mierten.tty 
gview:	mierten.ps
	ghostview  mierten.ps &
print:	mierten.ps
	lpr -s -h mierten.ps 
sprint:	mierten.001
	for F in mierten.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	mierten.faxsnd
	faxsndjob view mierten &
fsend:	mierten.faxsnd
	faxsndjob jobs mierten
viewgif:	mierten.gif
	xv mierten.gif &
viewpbm:	mierten.pbm
	xv mierten-??.pbm &
vieweps:	mierten.eps
	ghostview mierten.eps &	
clean:	mierten.ps
	rm -f  mierten-*.tex mierten.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} mierten-??.* mierten_tex.* mierten*~
tgz:	clean
	set +f;LSFILES=`cat mierten.ls???`;FILES=`ls mierten.* $$LSFILES | sort -u`;tar czvf mierten.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
