pr4en.lstex:
	lstex pr4en | sort -u > pr4en.lstex
pr4en.mk:	pr4en.lstex
	vcat /ul/prg/RC/texmake > pr4en.mk


pr4en.dvi:	pr4en.mk pr4en.tex
	latex pr4en && while tail -n 20 pr4en.log | grep references && latex pr4en;do eval;done
	if test -r pr4en.idx;then makeindex pr4en && latex pr4en;fi
pr4en.pdf:	pr4en.mk pr4en.tex
	pdflatex pr4en && while tail -n 20 pr4en.log | grep references && pdflatex pr4en;do eval;done
	if test -r pr4en.idx;then makeindex pr4en && pdflatex pr4en;fi
pr4en.tty:	pr4en.dvi
	dvi2tty -q pr4en > pr4en.tty
pr4en.ps:	pr4en.dvi	
	dvips  pr4en
pr4en.001:	pr4en.dvi
	rm -f pr4en.[0-9][0-9][0-9]
	dvips -i -S 1  pr4en
pr4en.pbm:	pr4en.ps
	pstopbm pr4en.ps
pr4en.gif:	pr4en.ps
	pstogif pr4en.ps
pr4en.eps:	pr4en.dvi
	dvips -E -f pr4en > pr4en.eps
pr4en-01.g3n:	pr4en.ps
	ps2fax n pr4en.ps
pr4en-01.g3f:	pr4en.ps
	ps2fax f pr4en.ps
pr4en.ps.gz:	pr4en.ps
	gzip < pr4en.ps > pr4en.ps.gz
pr4en.ps.gz.uue:	pr4en.ps.gz
	uuencode pr4en.ps.gz pr4en.ps.gz > pr4en.ps.gz.uue
pr4en.faxsnd:	pr4en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo pr4en-??.g3n`;source faxsnd main
pr4en_tex.ps:	
	cat pr4en.tex | splitlong | coco | m2ps > pr4en_tex.ps
pr4en_tex.ps.gz:	pr4en_tex.ps
	gzip < pr4en_tex.ps > pr4en_tex.ps.gz
pr4en-01.pgm:
	cat pr4en.ps | gs -q -sDEVICE=pgm -sOutputFile=pr4en-%02d.pgm -
pr4en/pr4en.html:	pr4en.dvi
	rm -fR pr4en;latex2html pr4en.tex
xview:	pr4en.dvi
	xdvi -s 8 pr4en &
tview:	pr4en.tty
	browse pr4en.tty 
gview:	pr4en.ps
	ghostview  pr4en.ps &
aview:	pr4en.pdf
	acroread pr4en.pdf
print:	pr4en.ps
	lpr -s -h pr4en.ps 
sprint:	pr4en.001
	for F in pr4en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	pr4en.faxsnd
	faxsndjob view pr4en &
fsend:	pr4en.faxsnd
	faxsndjob jobs pr4en
viewgif:	pr4en.gif
	xv pr4en.gif &
viewpbm:	pr4en.pbm
	xv pr4en-??.pbm &
vieweps:	pr4en.eps
	ghostview pr4en.eps &	
clean:	pr4en.ps
	rm -f  pr4en-*.tex pr4en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} pr4en-??.* pr4en_tex.* pr4en*~
tgz:	clean
	set +f;LSFILES=`cat pr4en.ls???`;FILES=`ls pr4en.* $$LSFILES | sort -u`;tar czvf pr4en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
