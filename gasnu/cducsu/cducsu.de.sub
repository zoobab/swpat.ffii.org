\begin{subdocument}{swpatcducsu}{CDU/CSU und Softwarepatente}{http://swpat.ffii.org/akteure/cducsu/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Die mit Internet und Informatik befassten Bundestagsabgeordneten der CDU/CSU, allen voran Dr. Martin Mayer (CSU), haben gegen\"{u}ber Softwarepatenten eine kritische Haltung eingenommen.  Von Mayer stammt der einzige Antrag im Bundestag zu diesem Thema.  Stoibers Unterschrift steht unter einem Papier des Ausschusses der Regionen der Europ\"{a}ischen Gemeinschaft, welches Softwarepatente ablehnt.  Unter dem Vorsitz Michael Hahns von der Jungen Union verabschiedete die Jugendorganisation der Europ\"{a}ischen Volkspartei im Oktober 2000 eine wohl durchdachte Resolution, welche die Patentierbarkeit von Software ablehnt.  Allerdings ver\"{o}ffentlichte sp\"{a}ter die CDU einige (ohne Mayers oder Hahns Zutun) schludrig geschriebene Thesenpapiere, darunter ein ``Strategiepapier'' vom August 2001, welches die Legalisierung von Softwarepatenten mit f\"{u}nfj\"{a}hriger Laufzeit vorschl\"{a}gt und dabei allerlei Grundbegriffe durcheinander bringt.  Von der Bayerischen Staatskanzlei kam etwa zur gleichen Zeit der Vorschlag, zum Zwecke des Jugendschutzes im Internet ``Sendezeiten'' einzuf\"{u}hren.  Nach dem 11. September 2001 erntete die CDU/CSU f\"{u}r allerlei hastige Vorst\"{o}{\ss}e ``gegen Cyber-Terrorismus'' viel Spott von der Fachwelt.  Beim EU/BSA-Richtlinienvorschlag lie{\ss} sich die CDU/CSU mit der Erarbeitung einer Stellungnahme noch Zeit.  Mitte Mai stellte Dr. Mayer ``Kleine Anfrage'', die im wesentlichen richtige Fragen stellt.  Einige patentlastige Pr\"{a}missen wurden durch ein sp\"{a}teres Papier von Krogmann korrigiert, so dass kurz vor der Bundestagswahl die CDU/CSU als die neben den Gr\"{u}nen patentkritischste Partei erschien.}
\begin{sect}{links}{Kommentierte Verweise}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Bundestag 2002-05-14: Kleine Anfrage der CDU/CSU zu Swpat\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/cducsu020514/index.de.html}}}

\begin{quote}
Nachdem die Bundesregierung lange Zeit geschwiegen und auch die \"{o}ffentliche Anfrage ihres Medienexperten MdB J\"{o}rg Tauss nicht beantwortet hat, fragt die unionschristliche Opposition unter Federf\"{u}hrung ihres Medienexperten MdB Dr. Martin Mayer die Bundesregierung, ob der Br\"{u}sseler Richtlinienvorschlag ihrer Meinung nach der F\"{o}rderung von Innovation und Forschung dienlich und hinreichend klar gefasst ist.  Wie schon anl\"{a}sslich seiner Anfrage vom Herbst 2000 gelingt es Mayer, im wesentlichen klare Gedanken zu fassen.  Allerdings weist der Text auch Unzul\"{a}nglichkeiten auf, die wir in dieser Rezension aufzeigen.
\end{quote}
\filbreak

\item
{\bf {\bf Martina Krogmann MdB 2002-07-12: Arbeitsbericht zur Internetpolitik der CDU/CSU-Fraktion\footnote{http://www.martina-krogmann.de/presse/pdf\_Arbeitsbericht\_Fraktion\_.pdf}}}

\begin{quote}
Das 2 Seiten lange (400 KB schwere inkonvertible) PDF-Flugblatt z\"{a}hlt allerlei bisherige Initiativen und Standpunkte der CDU/CSU-Fraktion im Bereich der digitalen Medien auf.  Zum Schluss wird kurz die Anfrage von MdB Martin Mayer in sibyllinischer Manier erw\"{a}hnt: \begin{quote}
{\it Zudem hat die Unionsfraktion eine kleine Anfrage -- ``Softwarepatente, Wettbewerb, Innovation und KMU'' -- gestellt.  In dieser werden von der Regierung u.a. Stellungnahmen zum aktuellen Entwurf der EU-Kommission f\"{u}r eine ``Richtlinie \"{u}ber die Patentierbarkeit computerimplementierter Erfindungen'' gefordert.  Ziel der Union ist, dass auch zuk\"{u}nftig Innovation und Forschung gef\"{o}rdert wird, freie Softwareentwickler und kleine Softwareunternehmen in ihrer Arbeit nicht gef\"{a}hrdet und Open-Source-Software-Entwicklungen nicht behindert werden.}
\end{quote}  Krogmann wird in n\"{a}chster Zeit Martin Mayer in seiner Funktion als Medienexperte abl\"{o}sen.  Krogmann hat sich Anfang 2002 f\"{u}r die Einf\"{u}hrung von GNU/Linux im Bundestag eingesetzt.   Eigene Stellungnahme Krogmanns zu dem Thema sind nicht bekannt.
\end{quote}
\filbreak

\item
{\bf {\bf Zitate zur Frage der Patentierbarkeit von computer-implementierten Organisations- und Rechenregeln\footnote{http://swpat.ffii.org/archiv/zitate/index.de.html}}}

\begin{quote}
S. u.a. Papier des Ausschusses der Regionen, Eckwertepapier von Martin Mayer und Resolution der Europ\"{a}ischen Volkspartei-Jugend
\end{quote}
\filbreak

\item
{\bf {\bf CDU 2001-08: Internet-Strategie-Papier\footnote{http://ffii.org/archive/mails/neues/2001/Aug/0013.html}}}

\begin{quote}
Eine Arbeitsgruppe der CDU unter der Leitung von Prof. Heilmann hat ein ``Strategiepapier'' f\"{u}r die Gestaltung und Nutzung des Internet in Deutschland ver\"{o}ffentlicht.  Dieses Papier enth\"{a}lt eine kurze und widerspr\"{u}chliche Passage zur Patent-Thematik.  Die Arbeitsgruppe will die Laufzeit von Logikpatenten auf 5 Jahre reduzieren und weitere abfedernde Sonderregelungen einf\"{u}hren, um die angeblich aufgrund ``internationaler Verpflichtungen'' erforderliche Wahrung der ``Rechte der Urheber'' zu ``erm\"{o}glichen''.  Offenbar wurde hier einiges missverstanden.  Zudem scheint das Papier einem fr\"{u}heren Antrag der CDU/CSU-Fraktion zu widersprechen und auch in vielen anderen Punkten Ungereimtheiten aufzuweisen.  Aus CDU-Kreisen war zun\"{a}chst zu h\"{o}ren, dass dieses Papier noch diskutiert werde und keine offizielle Meinung der Partei oder der Fraktion darstellen solle.  Im Fr\"{u}hjahr 2002 schien niemand mehr dieses Papier ernst zu nehmen und man h\"{o}rte nichts mehr von Prof. Heilmann.
\end{quote}
\filbreak

\item
{\bf {\bf CDU/CSU will sicheres Regierungsnetz schaffen\footnote{http://www.heise.de/tp/deutsch/special/info/12202/1.html}}}

\begin{quote}
Einer von vielen unausgegorenen und weithin l\"{a}cherlichen Antr\"{a}gen der CDU/CSU-Fraktion zum Thema IT.  S. auch die anschlie{\ss}ende Diskussion im Telepolis-Forum
\end{quote}
\filbreak

\item
{\bf {\bf MdB Norbert Hauser (CDU) gegen Gen-Patente\footnote{http://www.norberthauser.de/Presse\percent{}20aktuell/Presse\percent{}202000/pm\percent{}20000420.htm}}}

\begin{quote}
Bei dieser Kritik spielen Zweifel am volkswirtschaftlichen Nutzen der Genpatente oder des Patentwesens keine Rolle. In einer anderen Stellungnahme pl\"{a}diert Hauser durchaus f\"{u}r den Ausbau des Patentwesens an den Hochschulen\footnote{http://swpat.ffii.org/akteure/bmbf/index.de.html} und m\"{o}chte daf\"{u}r sogar noch mehr Geld zur Verf\"{u}gung stellen als von Bulmahn vorgesehen. Hauser ist in der CDU f\"{u}r Bildung und Forschung zust\"{a}ndig.  Hauser zog sich im Fr\"{u}hjahr 2002 aus der Politik zur\"{u}ck und nahm zugleich seine Webseiten vom Netz.
\end{quote}
\filbreak

\item
{\bf {\bf Kampagne ``Linux in den Bundestag!''\footnote{http://www.bundestux.de}}}

\begin{quote}
Diese Kampagne wurde von einigen Abgeordneten der CDU/CSU mit initiiert, u.a. Martina Krogmann, Mitunterzeichnerin der Kleinen Anfrage von 2005/05/14.
\end{quote}
\filbreak

\item
{\bf {\bf Stoiber will umfassende Regulierung des Internet\footnote{http://www.heute.t-online.de/ZDFheute/artikel/0,1251,POL-0-183355,00.html}}}

\begin{quote}
Dem ZDF-Nachrichtenmagazin Heute gegen\"{u}ber fordert Stoiber umfassende Zensurma{\ss}nahmen gegen unanst\"{a}ndige Computerspiele und \"{u}berhaupt anr\"{u}chige Netzinhalte aller Art.  Hierdurch will Stoiber als k\"{u}nftiger Kanzler Gewalttaten wie dem Amoklauf von Erfurt den gesellschaftlichen N\"{a}hrboden entziehen.  Als Kommunikationsmedium des m\"{u}ndigen B\"{u}rgers taugt das Internet laut Stoiber nur begrenzt:  die Diskussionen seien zu schnelllebig und es lie{\ss}en sich Mehrheiten f\"{u}r alles im Nu erzeugen.  Stoiber gibt zu, dass er das Internet kaum nutzt, da seine Zuarbeiter f\"{u}r ihn die ben\"{o}tigten Informationen zusammenstellen.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
\ifmlhttasks
\begin{itemize}
\item
{\bf {\bf Wie Sie uns helfen k\"{o}nnen, dem Swpat-Albtraum ein Ende zu machen\footnote{http://swpat.ffii.org/gruppe/aufgaben/index.de.html}}}
\filbreak

\item
{\bf {\bf Mit Ihrem CDU/CSU-Bundestagskandidaten und Europaabgeordneten sprechen}}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: latex ;
% End: ;

