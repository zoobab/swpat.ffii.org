<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: CDU/CSU und Softwarepatente

#descr: Im Europa-Parlament nahmen die CSU-Abgeordneten Dr. Joachim Wuermeling
und Angelika Niebler ab November 2002 sichtbar eine Vorreiterrolle bei
der Verfechtung der grenzenlosen Patentierbarkeit und maximalen
Blockierwirkung von Patenten im Bereich der Software ein.  Sie
gefielen sich in der Pose vermeintlicher Patentexperten, die alle
Anfrager darüber belehrten, dass ihre Sorgen unbegründet seien und die
Richtlinie eigentlich nur der Begrenzung der Patentierbarkeit diene. 
Gegenströmungen innerhalb der Bundes-CDU konnten keinen Einfluss
gewinnen.  Bei der Abstimmung im Europa-Parlament am 24. September
votierten die meisten CDU/CSU-Abgeordneten für Wuermelings
Extremposition und gegen die Empfehlungen der CDU-nahen Verbände
kleiner und mittlerer Unternehmen (SME Union) und der Kollegen
konservativer Schwesterparteien aus anderen europäischen Ländern. 
Auch bei anderen Themen des Immaterialgüterrechts agieren
%(q:Experten) aus dem Europarl-Rechtsausschuss wie Wuermeling und
Niebler erfolgreich als Relais der Großkonzernlobbies innerhalb der
CDU/CSU.

#ngM: Dr. Edmund Stoiber, der im Jahre 2000 einmal ein
softwarepatentkritisches Gutachten des Rates der Regionen
unterzeichnete, erklärte auf einem Microsoft-Jubiläum 2002-12
Microsoft zum %(q:Anziehungsmagneten) des IT-Standortes Bayern und
gelobte, dies durch transatlantische Treue zu honorieren.  Im
Münchener Stadtrat votierte kurz darauf die CSU im Mai 2003 als
einzige Fraktion gegen die Einführung von Linux in der
Stadtverwaltung.

#Inf: Diverse CDU-Papiere zu verschiedenen IT-Themen lehnen sich eng an die
Vorgaben von Großindustrieverbänden an.  Es gibt vereinzelt
Abgeordnete in der CDU/CSU-Fraktion, die Distanz zu solchen Positionen
und ein offenes Ohr für wissenschaftliche Diskussionen wahren.

#nZW: Die mit Internet und Informatik befassten Bundestagsabgeordneten der
CDU/CSU, allen voran Dr. Martin Mayer (CSU), haben indes gegenüber
Softwarepatenten eine kritische Haltung eingenommen.  Von Mayer stammt
der einzige Antrag im Bundestag zu diesem Thema.  Die Unterschrift von
Dr. Edmund Stoiber steht unter einem Papier des Ausschusses der
Regionen der Europäischen Gemeinschaft, welches Softwarepatente
ablehnt.  Unter dem Vorsitz Michael Hahns von der Jungen Union
verabschiedete die Jugendorganisation der Europäischen Volkspartei im
Oktober 2000 eine wohl durchdachte Resolution, welche die
Patentierbarkeit von Software ablehnt.  Allerdings veröffentlichte
später die CDU einige (ohne Mayers oder Hahns Zutun) schludrig
geschriebene Thesenpapiere, darunter ein %(q:Strategiepapier) vom
August 2001, welches die Legalisierung von Softwarepatenten mit
fünfjähriger Laufzeit vorschlägt und dabei allerlei Grundbegriffe
durcheinander bringt.  Von der Bayerischen Staatskanzlei kam etwa zur
gleichen Zeit der Vorschlag, zum Zwecke des Jugendschutzes im Internet
%(q:Sendezeiten) einzuführen.  Beim EU/BSA-Richtlinienvorschlag ließ
sich die CDU/CSU mit der Erarbeitung einer Stellungnahme noch Zeit. 
Mitte Mai stellte Dr. Mayer %(q:Kleine Anfrage), die im wesentlichen
richtige Fragen stellt.  Einige patentlastige Prämissen wurden durch
ein späteres Papier von Krogmann korrigiert, so dass kurz vor der
Bundestagswahl die CDU/CSU zeitweilig als die neben den Grünen
patentkritischste Partei erschien.

#ape: Hans-Peter Mayer MdEP: EP-Änderungen stärken Innovationsfähigkeit des
Mittelstands

#rrr: Mit dieser weitgehend von seinem Kollegen Joachim Wuermeling
übernommenen PE stellt sich der CDU-Rechtsexperte sowohl hinter die
Parlamentsentscheidung als auch hinter den dieser diametral
entgegengesetzten Vorschlag der Kommission und des Rates.  Dem letzten
Satz der Erklärung können die von Mayer kritisierten Gegner der
ursprünglichen CDU-Position nur zustimmen:  %(q:Mit den vom Parlament
durchgesetzten Änderungen wird Rechtssicherheit für Unternehmen
jeglicher Art geschaffen und gerade auch die Innovationsfähigkeit des
Mittelstands gestärkt, so der CDU-Rechtsexperte Hans-Peter Mayer
abschließend.)

#VUh: Eine reiche Sammlung von Zitaten und Verweisen über Softwarepatente
aus der Perspektive Kleiner und Mittlerer Unternehmen (KMU) von der
KMU-Union (SME Union) der Europäischen Volkspartei (Bündnis
christdemokratischer und konservative Fraktionen im Europaparlament)

#Kci: Martina Krogmann MdB 2002-07-12: Arbeitsbericht zur Internetpolitik
der CDU/CSU-Fraktion

#Dal: Das 2 Seiten lange (400 KB schwere inkonvertible) PDF-Flugblatt zählt
allerlei bisherige Initiativen und Standpunkte der CDU/CSU-Fraktion im
Bereich der digitalen Medien auf.  Zum Schluss wird kurz die Anfrage
von MdB Martin Mayer in sibyllinischer Manier erwähnt: %(bc:Zudem hat
die Unionsfraktion eine kleine Anfrage -- %(q:Softwarepatente,
Wettbewerb, Innovation und KMU) -- gestellt.  In dieser werden von der
Regierung u.a. Stellungnahmen zum aktuellen Entwurf der EU-Kommission
für eine %(q:Richtlinie über die Patentierbarkeit
computerimplementierter Erfindungen) gefordert.  Ziel der Union ist,
dass auch zukünftig Innovation und Forschung gefördert wird, freie
Softwareentwickler und kleine Softwareunternehmen in ihrer Arbeit
nicht gefährdet und Open-Source-Software-Entwicklungen nicht behindert
werden.)  Das Papier ist vom Wahlkampf geprägt.  Auf
fraktionsübergreifende Initiativen des Ausschusses Neue Medien, wie
z.B. dessen Anhörung zum Thema Softwarepatente, wird nicht
eingegangen.  Gesonderte Stellungnahmen von Krogmann zu dem Thema sind
nicht bekannt.  Krogmann wird in nächster Zeit Martin Mayer ableiten. 
Krogmann hat sich Anfang 2002 für die Einführung von GNU/Linux im
Bundestag eingesetzt.  Derzeit sind ihre Standpunkte vielfach nur
umrisshaft entwickelt und es ist noch nicht klar, in wie weit sie bei
einer eventuellen Durchsetzung Rückhalt genießt.

#Sau: Stoiber will umfassende Regulierung des Internet

#Drn: Dem ZDF-Nachrichtenmagazin Heute gegenüber fordert Stoiber umfassende
Zensurmaßnahmen gegen unanständige Computerspiele und überhaupt
anrüchige Netzinhalte aller Art.  Hierdurch will Stoiber als künftiger
Kanzler Gewalttaten wie dem Amoklauf von Erfurt den gesellschaftlichen
Nährboden entziehen.  Als Kommunikationsmedium des mündigen Bürgers
taugt das Internet laut Stoiber nur begrenzt:  die Diskussionen seien
zu schnelllebig und es ließen sich Mehrheiten für alles im Nu
erzeugen.  Stoiber gibt zu, dass er das Internet kaum nutzt, da seine
Zuarbeiter für ihn die benötigten Informationen zusammenstellen.

#CWr: CDU 2001-08: Internet-Strategie-Papier

#Etd: Eine Arbeitsgruppe der CDU unter der Leitung von Prof. Heilmann hat
ein %(q:Strategiepapier) für die Gestaltung und Nutzung des Internet
in Deutschland veröffentlicht.  Dieses Papier enthält eine kurze und
widersprüchliche Passage zur Patent-Thematik.  Die Arbeitsgruppe will
die Laufzeit von Logikpatenten auf 5 Jahre reduzieren und weitere
%(kr:abfedernde Sonderregelungen) einführen, um die angeblich aufgrund
%(q:internationaler Verpflichtungen) erforderliche Wahrung der
%(q:Rechte der Urheber) zu %(q:ermöglichen).  Offenbar wurde hier
einiges missverstanden.  Zudem scheint das Papier einem früheren
Antrag der CDU/CSU-Fraktion zu widersprechen und auch in vielen
anderen Punkten Ungereimtheiten aufzuweisen.  Aus CDU-Kreisen war
zunächst zu hören, dass dieses Papier noch diskutiert werde und keine
offizielle Meinung der Partei oder der Fraktion darstellen solle.  Im
Frühjahr 2002 schien niemand mehr dieses Papier ernst zu nehmen und
man hörte nichts mehr von Prof. Heilmann.  Doch zum Wahlkampfauftakt
im Juni 2002 tauchte das Heilmann-Papier dann plötzlich wieder auf und
wurde von A. Merkel u.a. als zukunftsweisendes Dokument gelobt.  Es
steht zu befürchten, dass dank der darin enthaltenen widersprüchlichen
Aussagen es einer CDU/CSU-Regierung nicht möglich sein wird, eine
eigenständige IT-Politik zu entwickeln und gegen die Wünsche
einflussreicher Lobbygruppen durchzuhalten.

#Ccg: CDU/CSU will sicheres Regierungsnetz schaffen

#EcW: Einer von vielen unausgegorenen und weithin lächerlichen Anträgen der
CDU/CSU-Fraktion zum Thema IT.  S. auch die anschließende Diskussion
im Telepolis-Forum

#0tr: Heise 2003/03/17: CDU fordert Verschärfung des Urheberrechts

#ttt: Bericht über ein Positionspapier des Wirtschaftsrates der CDU,
welches, im Sinne der Bitkom-Positionen, von der Bundesregierung eine
zügige Umsetzung der EU-Kopierschutzrichtlinie im Sinne möglichst
uneingeschränkter Vorrechte der Verwerter und ferner ein Gesetz zur
Förderung von Systemen zur Digitalen Verwertungsrechteverwaltung (DRM)
einzubringen.

#Wrr: Papier des CDU-Wirtschaftsrats zur Durchsetzung digitaler
Urheberrechte

#anf: Original der Position des %(q:Wirtschaftsrat der CDU e.V.)

#WcD: Bei einem Mindestbeitrag von 800,- EUR p. a. ein eher exklusiver
Verein.  Es kann aber jeder Mitglied werden, unabhängig von einer
CDU-Mitgliedschaft.  Die Meinung des Wirtschaftsrats der CDU ist also
nicht unbedingt die Meinung er CDU.  Die %(q:Bundesfachkommission
Innovation & Information) wird auf der website nicht näher
beschrieben. Der Vorsitzende Dr. Joachim Dreyer war von 1991 bis 2002
in verschiedenen Positionen bei der Debitel AG (zuletzt
Aufsichtsratmitglied) und ist jetzt anscheinend Aufsichtsratmitglied
bei Mobilcom.

#JyM: Dr. Joachim Dreyer in Who Magazine

#foD: Der Vorsitzende der %(q:Bundesfachkommission Innovation & Information)
des CDU-Wirtschaftsrates, Dr. Joachim Dreyer war von 1991 bis 2002 in
verschiedenen Positionen bei der Debitel AG (zuletzt
Aufsichtsratmitglied).

#Jee: Dr. Joachim Dreyer bei Debitel

#kri: Der Vorsitzende der %(q:Bundesfachkommission Innovation & Information)
des CDU-Wirtschaftsrates, Dr. Joachim Dreyer war laut dieser
Pressemeldung von 1991 bis 2002 in verschiedenen Positionen bei der
Debitel AG (zuletzt Aufsichtsratmitglied).

#Jeo: Dr. Joachim Dreyer bei Mobilcom

#BrA: Der Vorsitzende der %(q:Bundesfachkommission Innovation & Information)
des CDU-Wirtschaftsrates, Dr. Joachim Dreyer ist jetzt anscheinend
Aufsichtsratmitglied bei Mobilcom.

#0ga: FFII 2003/02/04: Stoiber, Wuermeling und der %(q:Anziehungsmagnet
Microsoft)

#2ue: Der bayerische Ministerpräsident Dr. Edmund Stoiber (CSU) gelobt
Microsoft anlässlich von deren 20-jährigen Jubiläum in Bayern
besondere Treue, grenzt sich gegen die Linux-Politik des ebenfalls
anwesenden Bundesinnenministers Otto Schily ab.  Der Bericht enthält
die Details uns spekuliert über Zusammenhänge zu anderen besonders
microsoft-freundlichen Aktivitäten von CSU-Abgeordneten.

#Sse: S. u.a. Papier des Ausschusses der Regionen, Eckwertepapier von Martin
Mayer und Resolution der Europäischen Volkspartei-Jugend

#aWp: Europarl-CSU macht sich für Softwarepatente stark

#Wnd: Bericht über die Anträge der CSU-Abgeordneten Angelika Niebler im
Industrieausschuss des Europa-Parlaments

#NuW: MdB Norbert Hauser (CDU) gegen Gen-Patente

#Est: Bei dieser Kritik spielen Zweifel am volkswirtschaftlichen Nutzen der
Genpatente oder des Patentwesens keine Rolle. In einer anderen
Stellungnahme plädiert Hauser durchaus für den %(bm:Ausbau des
Patentwesens an den Hochschulen) und möchte dafür sogar noch mehr Geld
zur Verfügung stellen als von Bulmahn vorgesehen. Hauser ist in der
CDU für Bildung und Forschung zuständig.  Hauser zog sich im Frühjahr
2002 aus der Politik zurück und nahm zugleich seine Webseiten vom
Netz.

#Dni: Diese Kampagne wurde von einigen Abgeordneten der CDU/CSU mit
initiiert, u.a. Martina Krogmann, Mitunterzeichnerin der Kleinen
Anfrage von 2005/05/14.

#atg: EICTAs Kampagne gegen die Parlamentsentscheidung von 2004-09-24 wird
von Leo Baumann geleitet, der bis kurz vor der Parlamentsentscheidung
im Büro der CSU-Abgeordneten Angelika Niebler arbeitete.

#MPe: Mit Ihrem CDU/CSU-Bundestagskandidaten und Europaabgeordneten sprechen

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatcducsu ;
# txtlang: de ;
# multlin: t ;
# End: ;

