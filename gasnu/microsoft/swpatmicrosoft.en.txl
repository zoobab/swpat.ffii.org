<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Microsoft and Patents

#descr: == News and Chronology

#fWO: Microsoft Patents at the European Patent Office (EPO)

#nW3: Microsoft Corporation grew large and successful without patents,
relying instead on copyright.  In 1991, Microsoft CEO Bill Gates
warned that patents could bring the software market to a complete
standstill and drive out small players.  In 1994, Microsoft was the
only software company at the USPTO hearings which spoke in favor of
software patentability.   Meanwhile, Microsoft had been stepping up
efforts to build a patent portfolio to counter the much larger
portfolios of traditional IT hardware companies such as IBM, HP, Canon
etc.  When the patent lawyers at the European Commission pressed for
legalising software patents in Europe in 1997, they cited Microsoft as
a success model, pointing out that Microsft already owned 400 software
patents.  In late 1998, an internal Microsoft stratgegy document about
the %(q:opensource threat) leaked out which suggested using software
patents alongside with proprietary standards in order to crush
competition from free software such as Apache and Linux.  In 2000,
Microsoft forced a free sofware project to abandon support for its
patented video streaming format ASF.  In 2001/07, in the midst of an
ongoing campaign against free software, a leading MS executive
challenged opensource companies to keep clear of Microsoft patents or
else %(q:Get your money and let's go to court!).  In 2002/03 Steve
Ballmer, CEO of Microsoft, declared that Microsoft's new standard
DotNet was protected by patents and free implementations would not be
allowed.  In 2003/04 Microsoft published patent license terms for CIFS
which disallow the use ore reimplementation of this communication
architecture by GNU software.  In late 2002, Microsoft began dissuade
corproporate customers from introducing GNU/Linux by pointing out that
if they use free software nobody would protect them from being sued
for patent infringement.

#tx0: Until June 2003, Microsoft largely abstained from public comments on
European patent policies.  In small-circle discussions at %(BITKOM),
where IBM patent lawyers pushed vocally for far-reaching
patentability, the Microsoft representative remained almost silent but
tacitly supported the IBM patent department's hard line.  At another
association, %(VSI), Microsoft's pro patent involvement was more
overt.  In France, Microsoft representatives have exerted pressure on
associations such as %(si:Syntec Informatique) not to oppose software
patents.  The %(bs:Business Software Alliance) had Microsoft's full
support when it worked on the %(eb:software patentability directive)
for the European Commission (CEC), which CEC adopted almost without
modification.  German Microsoft representatives have, as a part of
their anti-Linux campaign of 2002 at the Federal Parliament, been
asking politicians in Berlin to support the CEC/BSA directive
proposal.  One Microsoft paper circulating in Berlin based its
arguments mainly on the %(tf:TRIPs fallacy).

#itl: %(al|Microsoft's new chief patent lawyer spells out his vision of
maximising patent rents and lobbying the European Union to legalise
software patents.|%(orig|Hired in June, Phelps has been brought to
Redmond from his home in Connecticut to spark the kind of licensing
programme he inspired at his previous company, IBM, which is now
reaping almost $2 billion a year from licensing its patents. But
Phelps is in charge of more than just the company's licensing. In his
new role he oversees all aspects of Microsoft's IP department, from
the number of paralegals on its staff, to its relationship with
external law firms and national governments. Phelps's appointment has
seen the company move into a more aggressive mode of viewing the IP
world, what Phelps calls a new %(q:outward-facing) approach. This
involves not just making money from research and development (R&D),
but also engaging with the EU on the shape of its proposed software
patent directive, and lobbying the US Congress to end the diversion of
USPTO funds.|...|In Europe, for instance, the past few months have
been dominated by the controversy surrounding the future of a European
software patent directive. Following September's vote in the European
Parliament, the EU's proposals for the patent are falling some way
short of what many corporations would like to see, allowing for
patents on inventions that affect the way hardware works, but not for
software itself. %(q:We'll focus on Europe that bit more,) says
Phelps. %(q:Again, I think we've been way too US-centric, and I don't
think we joined the debate [on software patents in Europe] in the
right way.)|...|%(q:My concern with Europe is that what they're going
to do, writ large, is develop an industrial policy for this kind of
thing in Europe. The US isn't following that. The US is off to the
races on software patents and that's not going to be turned around
anytime soon, if ever, and so is Japan. And why Europe would want to
be uncompetitive with those two areas of the world, I really don't
know.)|...|%(q:I think there's a fundamental lack of understanding [in
Europe] that there isn't a real difference between software and
hardware. I also think there are large political forces driving this
issue. For some reason, it has caught the attention of the Greens, and
I'm not clear why. But that's true of this country too, where
[consumer activist and Green Party presidential candidate in 2000]
Ralph Nader is involved with the Open Source movement. Some on either
side approach these issues with an almost religious fervour.)))

#yoe: narrates how Microsoft hired IBM's patent lawyer Marshall Phelps to
embark on a more aggressive patent strategy which appears to be aimed
at Linux.  Also quotes Simon Phipps from Sun with comments about how
%(q:the evil of software patents) makes is imperative for small
entities to rely on a big company to protect them.

#sla: says that patent licenses needed for communication with Microsoft
applications will be given on a %(q:reasonable and non-discriminatory)
basis, i.e. not for free software.

#fre: details of Microsoft license terms for use of its patented protocols

#sie: List of protocols for communication with Microsoft applications for
whose use %(pe:patent) license fees must bee paid.

#NtW: In a magazine article, the CEO of Microsoft Israel, Arie Scope, warns
customers that by introducing Linux they risk running afoul of patents
and advises them to seek indemnification from suppliers such as Redhat
in the event of patent infringement.  This article provides a response
to open source initiatives in Israel.  In the article, Scope says:
%(bc:IBM is not developing its own version of the Linux OS. Rather
than that it distributes Red Hat's version and clears itself from any
liability in case the customer changes the code.  I advise
organizations to review the licensing agreement of Red Hat distributed
by IBM, and ask the company for legal protection if it turns out that
the OS infringes patents.)

#pWh: In early 2003 press release drafts from SCO leaked out in which SCO
was to announce that it owned %(q:Unix IP) and that any Unix user, in
particular any Linux user, would either have to buy SCO's distribution
or pay 96 USD for each processor on which a Unix/Linux system is
running.  SCO partially denied the rumors with a press release, in
which it said it never intended to charge money from %(q:fellow
vendors) of the United Linux distribution.  SCO is partially owned by
Microsoft and some people reacted to the news by suspecting SCO of
wanting to raise its value for a complete Microsoft buyout.  No patent
numbers have been named throughout this discussion and it is not clear
to us, whether SCO has acquired any Unix-related patents from Bell
Laboratories.

#Msn: Microsoft 2001-07 to opensourcers: %(q:Get your money and let's go to
court)!

#Aoe: News report about a public discussion at the %(q:Open Source
Convention) between Microsoft executive Craig Mundie and well known
representatives of the free software community in July 2001. 
%(bc:Asked by CollabNet CTO Brian Behlendorf whether Microsoft will
enforce its patents against open source projects, Mundie replied,
%(q:Yes, absolutely.) An audience member pointed out that many open
source projects aren't funded and so can't afford legal representation
to rival Microsoft's. %(q:Oh well,) said Mundie. %(q:Get your money,
and let's go to court.))  The article goes on to speculate that the
Mono and Samba projects could be among the first victims of this
Microsoft strategy.  E.g. US Patent 5,719,941 - the %(q:method for
changing passwords on a remote computer) applies to a new
password-changing feature in SMB which is necessary in order to
maintain full interoperability between Microsoft clients and Linux
fileservers in typical company networks.  The article is hidden behind
passwords, accessible only to subscribers.  It is marked as written by
Rachel Chalmers on 2001-08-08.

#MeW: Microsoft France speaker explains that Microsoft is an innovative
company and the proof of this is the number of 2110 US patents which
Microsoft already owned by 2001-10-31.

#rWr: German news report about Microsoft patents on principles of Digital
Restrictions Managment

#loW: Halloween Documents of 1998

#inr: Internal strategy papers from Microsoft, suggest that opensource
software could be combatted by creating proprietary extensions to
Internet protocols and by patents: %(q:Linux's homebase is currently
commodity network and server infrastructure. By folding extended
functionality (e.g. Storage+ in file systems, DAV/POD for networking)
into today's commodity services, we raise the bar & change the rules
of the game.) ... %(q:The Linux community is very willing to copy
features from other OS's if it will serve their needs. Consequently,
there is the very real long term threat that as MS expends the
development dollars to create a bevy of new features in NT, Linux will
simply cherry pick the best features an incorporate them into their
codebase.  The effect of patents and copyright in combatting Linux
remains to be investigated.)

#DMc: Discussion thread on this event, where the MS representative stated
the intent of his company to use patents against free software.

#ooc: Microsoft Germany Windows vs Linux Brochure 2003

#Wth: Uses the argument that patents put GNU/Linux at risk and that
companies are on the safe side if they use MS Windows instead.

#ufy: Arlene McCarthy, MEP of the UK Labour group, who championed the cause
of software patenting, was usually surrounded by a swarm of lobbyists
from the European Patent Office, the European Commission's industrial
Property Unit as well as patent lawyers and lobbyists of large
corporations.  In some of her public statements McCarthy mixed up the
case for software patents with the case against the GNU GPL, calling
the latter %(q:just another form of monopoly), a kind of statement
which only Microsoft would make.  In summer 2003 McCarthy and other
MEPs who meet at the %(q:European Internet Foundation), a lobbying
group of large companies aiming to provide %(q:political leadership
for network society), travelled to Seattle with a group of MEPs to
visit Microsoft.  After the hearing on software patents of 2002-11-07,
McCarthy sat at a table with the Microsoft representative in Brussels
and put the other witnesses and experts to marginal tables.

#Aoo: A fairly well written article that gives a lot of space to Microsoft
FUD about the %(q:viral) nature of free software.  Microsoft has been
advocating this in more aggressive ways since 2000.  They forget to
mention that free software makes it easy to develop proprietary
complements and the GPL doesn't forbid anything that proprietary
software would allow.

#Teh: This patent caused trouble for the %(W3C) in 1999

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatmicrosoft ;
# txtlang: en ;
# multlin: t ;
# End: ;

