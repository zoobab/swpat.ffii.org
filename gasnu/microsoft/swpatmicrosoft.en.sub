\begin{subdocument}{swpatmicrosoft}{Microsoft and Patents}{http://swpat.ffii.org/players/microsoft/index.en.html}{Workgroup\\swpatag@ffii.org}{Microsoft Corporation grew large and successful without patents, relying instead on copyright.  Initially, Bill Gates was very critical of software patentability.  However, as Microsoft began to play the game successfully, it became one of the staunchest supporters of software patentability.   It has also been involved in promoting software patentability in Europe.  Simultaneously Microsoft's has invested ample ressources into a campaign to dissuade governments and corporations from using free operating system.  Pointing out the insecurity caused to Free Software by patents and contributing to this insecurity by occasional threats has become an important part of the campaign.  At the same time, Microsoft itself appears to be a favorite victim of patent attacks.}
\begin{sect}{intro}{introduction}
Microsoft Corporation grew large and successful without patents, relying instead on copyright.  In 1991, Microsoft CEO Bill Gates warned that patents could bring the software market to a complete standstill and drive out small players.  In 1994, Microsoft was the only software company at the USPTO hearings which spoke in favor of software patentability.   Meanwhile, Microsoft had been stepping up efforts to build a patent portfolio to counter the much larger portfolios of traditional IT hardware companies such as IBM, HP, Canon etc.  When the patent lawyers at the European Commission pressed for legalising software patents in Europe in 1997, they cited Microsoft as a success model, pointing out that Microsft already owned 400 software patents.  In late 1998, an internal Microsoft stratgegy document about the ``opensource threat'' leaked out which suggested using software patents alongside with proprietary standards in order to crush competition from free software such as Apache and Linux.  In 2000, Microsoft forced a free sofware project to abandon support for its patented video streaming format ASF.  In 2001/07, in the midst of an ongoing campaign against free software, a leading MS executive challenged opensource companies to keep clear of Microsoft patents or else ``Get your money and let's go to court!''.  In 2002/03 Steve Ballmer, CEO of Microsoft, declared that Microsoft's new standard DotNet was protected by patents and free implementations would not be allowed.  In 2003/04 Microsoft published patent license terms for CIFS which disallow the use ore reimplementation of this communication architecture by GNU software.  In late 2002, Microsoft began dissuade corproporate customers from introducing GNU/Linux by pointing out that if they use free software nobody would protect them from being sued for patent infringement.

Meanwhile, Microsoft has largely abstained from public comments on European patent policies.  In small-circle discussions at Bitkom: Voice of IT corporate patent lawyers in Germany\footnote{http://swpat.ffii.org/players/bitkom/index.en.html}, where IBM patent lawyers pushed vocally for far-reaching patentability, the Microsoft representative remained almost silent but tacitly supported the IBM patent department's hard line.  At another association, Verband der Software-Industrie e.V. (VSI) und Logikpatente\footnote{http://swpat.ffii.org/players/vsi/index.de.html}, Microsoft's pro patent involvement was more overt.  In France, Microsoft representatives have exerted pressure on associations such as Syntec Informatique\footnote{http://swpat.ffii.org/papers/eukonsult00/syntec/index.en.html} not to oppose software patents.  The Business Software Alliance\footnote{http://swpat.ffii.org/players/bsa/index.en.html} (BSA) had Microsoft's full support when it worked on the software patentability directive for the European Commission (CEC), which the CEC adopted almost without modification.  German Microsoft representatives have, as a part of their anti-Linux campaign of 2002 at the Federal Parliament, been asking politicians in Berlin to support the CEC/BSA directive proposal.  One Microsoft paper circulating in Berlin based its arguments mainly on the TRIPs fallacy\footnote{http://swpat.ffii.org/analysis/trips/index.en.html}.
\end{sect}

\begin{sect}{epat}{Microsoft Patents at the European Patent Office (EPO)}
\input{microsoft-epatents}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Microsoft Germany Windows vs Linux Brochure 2003\footnote{http://swpat.ffii.org/archive/quotes/index.en.html\#microsoft-de-linuxbrosch03} (Uses the argument that patents put GNU/Linux at risk and that companies are on the safe side if they use MS Windows instead.)}}
\filbreak

\item
{\bf {\bf The Register 2002/11/18: MS suggests Linux could infringe patents\footnote{http://www.theregister.co.uk/content/4/28155.html}}}

\begin{quote}
In a magazine article, the CEO of Microsoft Israel, Arie Scope, points out that free software such as GNU/Linux may be infringing on patents and advises potential adopters to seek indemnification from suppliers such as Redhat in the event of patent infringement.  This article provides a response to open source initiatives in Israel.  In the article, Scope says: \begin{quote}
{\it IBM is not developing its own version of the Linux OS. Rather than that it distributes Red Hat's version and clears itself from any liability in case the customer changes the code.  I advise organizations to review the licensing agreement of Red Hat distributed by IBM, and ask the company for legal protection if it turns out that the OS infringes patents.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Steve Ballmer 2002-03-12: Kein T\"{a}nzchen an der Leine\footnote{http://www.heise.de/newsticker/data/jk-12.03.02-000/}}}

\begin{quote}
Heise report about Steve Ballmer's talk at CeBit.  At a speech event together with chancellor Schroeder, Ballmer says that Microsoft owns lots of patents which cover its new DotNet standard and that it aims to use them to prevent opensource implementations of DotNet. The key phrases read, in translation: \begin{quote}
{\it Responding to questions about the opening-up of the .NET framework, Ballmer announced that there would certainly be a ``Common Language Runtime Implementation'' for Unix, but then explained that this development would be limited to a subset, which was ``intended only for academic use''.  Ballmer rejected speculations about support for free .NET implementationens such as Mono: ``We have invested so many millions in .NET, we have so many patents on .NET, which we want to cultivate.''}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Microsoft 2001-07 to opensourcers: ``Get your money and let's go to court''!\footnote{http://www.the451.com/login/index.php?entity\_id=11185\&source=\&sm=UGxlYXNlIGxvZyBpbiB0byB2aWV3IHRoaXMgc3Rvcnku}}}

\begin{quote}
News report about a public discussion at the ``Open Source Convention'' between Microsoft executive Craig Mundie and well known representatives of the free software community in July 2001.  \begin{quote}
{\it Asked by CollabNet CTO Brian Behlendorf whether Microsoft will enforce its patents against open source projects, Mundie replied, ``Yes, absolutely.'' An audience member pointed out that many open source projects aren't funded and so can't afford legal representation to rival Microsoft's. ``Oh well,'' said Mundie. ``Get your money, and let's go to court.''}
\end{quote}  The article goes on to speculate that the Mono and Samba projects could be among the first victims of this Microsoft strategy.  E.g. US Patent 5,719,941 - the ``method for changing passwords on a remote computer'' applies to a new password-changing feature in SMB which is necessary in order to maintain full interoperability between Microsoft clients and Linux fileservers in typical company networks.  The article is hidden behind passwords, accessible only to subscribers.  It is marked as written by Rachel Chalmers on 2001-08-08.
\end{quote}
\filbreak

\item
{\bf {\bf 2001-07-24 Opensource Summit and Microsoft\footnote{http://aful.org/wws/arc/patents/2001-08/msg02284.html}}}

\begin{quote}
Discussion thread on this event, where the MS representative stated the intent of his company to use patents against free software.
\end{quote}
\filbreak

\item
{\bf {\bf Pierre Bugnon: Petite Digression\footnote{http://www.microsoft.com/france/technet/edito/def\_edito.asp}}}

\begin{quote}
Microsoft France speaker explains that Microsoft is an innovative company and the proof of this is the number of 2110 US patents which Microsoft already owned by 2001-10-31.
\end{quote}
\filbreak

\item
{\bf {\bf Microsoft bars GNU software from interoperating with CIFS\footnote{http://swpat.ffii.org/patents/effects/cifs/index.en.html}}}

\begin{quote}
During the 1st week of April 2002, Microsoft published a license for its new specification CIFS which it is trying to establish as a de facto communication standard.  This license says that free software under GNU GPL, LGPL and similar licenses may not use CIFS.  It bases this ban on two broad and trivial US patents with priority dates of 1989 and 1993.  Preliminary search results suggst that these patents to not have EP (European Patent) counterparts.  But there is nevertheless an EP patent which could possibly be used by MS for the same purpose.  Critical network infrastructure such as Samba as well as new projects such as Mono seem to be affected.
\end{quote}
\filbreak

\item
{\bf {\bf ASF: changing copyright rules by means of patents\footnote{http://swpat.ffii.org/patents/effects/asf/index.fr.html}}}

\begin{quote}
Microsoft has prohibited a Free Software programmer from writing import/export filters for its Advanced Streaming Format (ASF).  The programmer wanted interoperability with a format that Microsoft is promoting.  But for Microsoft, interoperability is in this case doubly disadvantageous:  besides reducing the lock-in effect, on which Microsoft's platform strategy relies, it also can circumvent the locks on unauthorized copying, by which Microsoft wants to attract content providers to its ASF platform.  Whereas in the DeCSS case a court ruling was necessary to enforce new draconian copyright provisions of the highly disputed Digital Millenium Act, in the ASF case a simple patent suffices to achieve the same legislative goal.
\end{quote}
\filbreak

\item
{\bf {\bf Noam Chomsky on Microsoft and the rise of private tyrannies since Madison\footnote{http://www.corpwatch.org/trac/feature/microsoft/chomsky.html}}}
\filbreak

\item
{\bf {\bf Microsoft patent on distributed file systems 1999\footnote{http://l2.espacenet.com/dips/viewer?PN=WO0057315\&CY=ep\&LG=en\&DB=EPD}}}
\filbreak

\item
{\bf {\bf Microsoft: GPL destroys intellectual property\footnote{http://globalarchive.ft.com/globalarchive/article.html?id=020320002253\&query=poynder}}}

\begin{quote}
A fairly well written article that gives a lot of space to Microsoft FUD about the ``viral'' nature of free software.  Microsoft has been advocating this in more aggressive ways since 2000.  They forget to mention that free software makes it easy to develop proprietary complements and the GPL doesn't forbid anything that proprietary software would allow.
\end{quote}
\filbreak

\item
{\bf {\bf Microsoft patent on distributed file systems 1999\footnote{http://l2.espacenet.com/dips/viewer?PN=WO0057315\&CY=ep\&LG=en\&DB=EPD}}}
\filbreak

\item
{\bf {\bf The Halloween Documents\footnote{http://www.opensource.org/halloween/}}}

\begin{quote}
internal strategy papers from Microsoft, suggest that opensource software could be combatted by creating proprietary extensions to Internet protocols and by patent lawsuits.
\end{quote}
\filbreak

\item
{\bf {\bf Heise 2002-12-14: Microsoft DRM Patent\footnote{http://www.heise.de/newsticker/data/cgl-14.12.01-000/}}}
\filbreak

\item
{\bf {\bf Microsoft-Betriebssysteme mit eingebauter Hintert\"{u}r\footnote{http://www.ccc.de/CRD/CRD19990903.html}}}
\filbreak

\item
{\bf {\bf }}
\filbreak

\item
{\bf {\bf Microsoft Electronic Asset System Patent\footnote{http://www.patents.ibm.com/patlist?icnt=US\&patent\_number=5872844}}}
\filbreak

\item
{\bf {\bf Microsoft P3P Patent\footnote{http://www.patents.ibm.com/patlist?icnt=US\&patent\_number=5860073}}}

\begin{quote}
This patent caused trouble for the W3C in 1999
\end{quote}
\filbreak

\item
{\bf {\bf Gates 2002-04: GPL will eat your economy\footnote{http://www.theregister.co.uk/content/4/24970.html}}}
\filbreak

\item
{\bf {\bf GPL Pacman will eat your business, warns Gates\footnote{http://www.theregister.co.uk/content/4/19836.html}}}
\filbreak

\item
{\bf {\bf Open source terror stalks Microsoft's lawyers\footnote{http://www.theregister.co.uk/content/archive/19953.html}}}
\filbreak

\item
{\bf {\bf Perens, Stallman, Raymond et al: Free Software leaders responding to Microsoft's FUD campaign\footnote{http://perens.com/Articles/StandTogether.html}}}
\filbreak

\item
{\bf {\bf The Microsoft Antitrust Trial and Free Software\footnote{http://www.gnu.org/philosophy/microsoft-antitrust.html}}}
\filbreak

\item
{\bf {\bf Microsoft honous Linux programmer with patent gong\footnote{http://www.theregister.co.uk/content/4/14909.html}}}
\filbreak

\item
{\bf {\bf The Microsoft DRM Patent and our freedom to speak and think\footnote{http://aful.org/wws/arc/patents/2001-12/msg00060.html}}}
\filbreak

\item
{\bf {\bf Gerhard Schr\"{o}der\footnote{http://swpat.ffii.org/players/schroeder/index.de.html}}}

\begin{quote}
Der ``Genosse der Bosse'' hat bekanntlich ein besonders offenes Ohr f\"{u}r die Einfl\"{u}sterungen hoher Wirtschatsfunktion\"{a}re und insbesondere eine anhaltende Neigung, sich an Seite von Microsoft-F\"{u}hrungskadern \"{o}ffentlich zu zeigen und stolz auf Partnerschaften mit MS zu sein.  Seine Herkunft aus dem Anwaltsberuf, seine Orientierung an anglo-amerikanischen Vorbildern (bei relativer Distanz zu Frankreich) k\"{o}nnte f\"{u}r Software-Urheber und -anwender ebenfalls Anlass zu Sorge sein: in Frankreich fielen die Entscheidungen gegen Softwarepatente auf der Ebene des Premierministers.  Bislang hat sich Schr\"{o}der zum Thema Softwarepatente, soweit bekannt, nicht ge\"{a}u{\ss}ert, aber sein Verhalten im Zusammenhang mit der Kopierschutzrichtlinie l\"{a}sst wenig Verst\"{a}ndnis f\"{u}r informationelle Infrastrukturen erwarten.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: latex ;
% End: ;

