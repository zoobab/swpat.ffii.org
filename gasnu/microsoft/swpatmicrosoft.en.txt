<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#descr: Microsoft Corporation grew large and successful without patents, relying instead on copyright.  Initially, Bill Gates was very critical of software patentability.  However, as Microsoft began to play the game successfully, it became one of the staunchest supporters of software patentability.   It has also been involved in promoting software patentability in Europe.  Simultaneously Microsoft's has invested ample ressources into a campaign to dissuade governments and corporations from using free operating system.  Pointing out the insecurity caused to Free Software by patents and contributing to this insecurity by occasional threats has become an important part of the campaign.  At the same time, Microsoft itself appears to be a favorite victim of patent attacks.

#title: Microsoft and Patents

#fWO: Microsoft Patents at the European Patent Office (EPO)

#nW3: Microsoft Corporation grew large and successful without patents, relying instead on copyright.  In 1991, Microsoft CEO Bill Gates warned that patents could bring the software market to a complete standstill and drive out small players.  In 1994, Microsoft was the only software company at the USPTO hearings which spoke in favor of software patentability.   Meanwhile, Microsoft had been stepping up efforts to build a patent portfolio to counter the much larger portfolios of traditional IT hardware companies such as IBM, HP, Canon etc.  When the patent lawyers at the European Commission pressed for legalising software patents in Europe in 1997, they cited Microsoft as a success model, pointing out that Microsft already owned 400 software patents.  In late 1998, an internal Microsoft stratgegy document about the %(q:opensource threat) leaked out which suggested using software patents alongside with proprietary standards in order to crush competition from free software such as Apache and Linux.  In 2000, Microsoft forced a free sofware project to abandon support for its patented video streaming format ASF.  In 2001/07, in the midst of an ongoing campaign against free software, a leading MS executive challenged opensource companies to keep clear of Microsoft patents or else %(q:Get your money and let's go to court!).  In 2002/03 Steve Ballmer, CEO of Microsoft, declared that Microsoft's new standard DotNet was protected by patents and free implementations would not be allowed.  In 2003/04 Microsoft published patent license terms for CIFS which disallow the use ore reimplementation of this communication architecture by GNU software.  In late 2002, Microsoft began dissuade corproporate customers from introducing GNU/Linux by pointing out that if they use free software nobody would protect them from being sued for patent infringement.

#tx0: Meanwhile, Microsoft has largely abstained from public comments on European patent policies.  In small-circle discussions at %(BITKOM), where IBM patent lawyers pushed vocally for far-reaching patentability, the Microsoft representative remained almost silent but tacitly supported the IBM patent department's hard line.  At another association, %(VSI), Microsoft's pro patent involvement was more overt.  In France, Microsoft representatives have exerted pressure on associations such as %(si:Syntec Informatique) not to oppose software patents.  The %(bs:Business Software Alliance) had Microsoft's full support when it worked on the software patentability directive for the European Commission (CEC), which the CEC adopted almost without modification.  German Microsoft representatives have, as a part of their anti-Linux campaign of 2002 at the Federal Parliament, been asking politicians in Berlin to support the CEC/BSA directive proposal.  One Microsoft paper circulating in Berlin based its arguments mainly on the %(tf:TRIPs fallacy).

#NtW: In a magazine article, the CEO of Microsoft Israel, Arie Scope, warns customers that by introducing Linux they risk running afoul of patents and advises them to seek indemnification from suppliers such as Redhat in the event of patent infringement.  This article provides a response to open source initiatives in Israel.  In the article, Scope says: %(bc:IBM is not developing its own version of the Linux OS. Rather than that it distributes Red Hat's version and clears itself from any liability in case the customer changes the code.  I advise organizations to review the licensing agreement of Red Hat distributed by IBM, and ask the company for legal protection if it turns out that the OS infringes patents.)

#pWh: In early 2003 press release drafts from SCO leaked out in which SCO was to announce that it owned %(q:Unix IP) and that any Unix user, in particular any Linux user, would either have to buy SCO's distribution or pay 96 USD for each processor on which a Unix/Linux system is running.  SCO partially denied the rumors with a press release, in which it said it never intended to charge money from %(q:fellow vendors) of the United Linux distribution.  SCO is partially owned by Microsoft and some people reacted to the news by suspecting SCO of wanting to raise its value for a complete Microsoft buyout.  No patent numbers have been named throughout this discussion and it is not clear to us, whether SCO has acquired any Unix-related patents from Bell Laboratories.

#Msn: Microsoft 2001-07 to opensourcers: %(q:Get your money and let's go to court)!

#Aoe: News report about a public discussion at the %(q:Open Source Convention) between Microsoft executive Craig Mundie and well known representatives of the free software community in July 2001.  %(bc:Asked by CollabNet CTO Brian Behlendorf whether Microsoft will enforce its patents against open source projects, Mundie replied, %(q:Yes, absolutely.) An audience member pointed out that many open source projects aren't funded and so can't afford legal representation to rival Microsoft's. %(q:Oh well,) said Mundie. %(q:Get your money, and let's go to court.))  The article goes on to speculate that the Mono and Samba projects could be among the first victims of this Microsoft strategy.  E.g. US Patent 5,719,941 - the %(q:method for changing passwords on a remote computer) applies to a new password-changing feature in SMB which is necessary in order to maintain full interoperability between Microsoft clients and Linux fileservers in typical company networks.  The article is hidden behind passwords, accessible only to subscribers.  It is marked as written by Rachel Chalmers on 2001-08-08.

#MeW: Microsoft France speaker explains that Microsoft is an innovative company and the proof of this is the number of 2110 US patents which Microsoft already owned by 2001-10-31.

#DMc: Discussion thread on this event, where the MS representative stated the intent of his company to use patents against free software.

#ooc: Microsoft Germany Windows vs Linux Brochure 2003

#Wth: Uses the argument that patents put GNU/Linux at risk and that companies are on the safe side if they use MS Windows instead.

#Aoo: A fairly well written article that gives a lot of space to Microsoft FUD about the %(q:viral) nature of free software.  Microsoft has been advocating this in more aggressive ways since 2000.  They forget to mention that free software makes it easy to develop proprietary complements and the GPL doesn't forbid anything that proprietary software would allow.

#inr: internal strategy papers from Microsoft, suggest that opensource software could be combatted by creating proprietary extensions to Internet protocols and by patent lawsuits.

#Teh: This patent caused trouble for the %(W3C) in 1999

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatmicrosoft ;
# txtlang: en ;
# multlin: t ;
# End: ;

