# -*- mode: makefile -*-

swpatmccarthy.en.lstex:
	lstex swpatmccarthy.en | sort -u > swpatmccarthy.en.lstex
swpatmccarthy.en.mk:	swpatmccarthy.en.lstex
	vcat /ul/prg/RC/texmake > swpatmccarthy.en.mk


swpatmccarthy.en.dvi:	swpatmccarthy.en.mk
	rm -f swpatmccarthy.en.lta
	if latex swpatmccarthy.en;then test -f swpatmccarthy.en.lta && latex swpatmccarthy.en;while tail -n 20 swpatmccarthy.en.log | grep -w references && latex swpatmccarthy.en;do eval;done;fi
	if test -r swpatmccarthy.en.idx;then makeindex swpatmccarthy.en && latex swpatmccarthy.en;fi

swpatmccarthy.en.pdf:	swpatmccarthy.en.ps
	if grep -w '\(CJK\|epsfig\)' swpatmccarthy.en.tex;then ps2pdf swpatmccarthy.en.ps;else rm -f swpatmccarthy.en.lta;if pdflatex swpatmccarthy.en;then test -f swpatmccarthy.en.lta && pdflatex swpatmccarthy.en;while tail -n 20 swpatmccarthy.en.log | grep -w references && pdflatex swpatmccarthy.en;do eval;done;fi;fi
	if test -r swpatmccarthy.en.idx;then makeindex swpatmccarthy.en && pdflatex swpatmccarthy.en;fi
swpatmccarthy.en.tty:	swpatmccarthy.en.dvi
	dvi2tty -q swpatmccarthy.en > swpatmccarthy.en.tty
swpatmccarthy.en.ps:	swpatmccarthy.en.dvi	
	dvips  swpatmccarthy.en
swpatmccarthy.en.001:	swpatmccarthy.en.dvi
	rm -f swpatmccarthy.en.[0-9][0-9][0-9]
	dvips -i -S 1  swpatmccarthy.en
swpatmccarthy.en.pbm:	swpatmccarthy.en.ps
	pstopbm swpatmccarthy.en.ps
swpatmccarthy.en.gif:	swpatmccarthy.en.ps
	pstogif swpatmccarthy.en.ps
swpatmccarthy.en.eps:	swpatmccarthy.en.dvi
	dvips -E -f swpatmccarthy.en > swpatmccarthy.en.eps
swpatmccarthy.en-01.g3n:	swpatmccarthy.en.ps
	ps2fax n swpatmccarthy.en.ps
swpatmccarthy.en-01.g3f:	swpatmccarthy.en.ps
	ps2fax f swpatmccarthy.en.ps
swpatmccarthy.en.ps.gz:	swpatmccarthy.en.ps
	gzip < swpatmccarthy.en.ps > swpatmccarthy.en.ps.gz
swpatmccarthy.en.ps.gz.uue:	swpatmccarthy.en.ps.gz
	uuencode swpatmccarthy.en.ps.gz swpatmccarthy.en.ps.gz > swpatmccarthy.en.ps.gz.uue
swpatmccarthy.en.faxsnd:	swpatmccarthy.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo swpatmccarthy.en-??.g3n`;source faxsnd main
swpatmccarthy.en_tex.ps:	
	cat swpatmccarthy.en.tex | splitlong | coco | m2ps > swpatmccarthy.en_tex.ps
swpatmccarthy.en_tex.ps.gz:	swpatmccarthy.en_tex.ps
	gzip < swpatmccarthy.en_tex.ps > swpatmccarthy.en_tex.ps.gz
swpatmccarthy.en-01.pgm:
	cat swpatmccarthy.en.ps | gs -q -sDEVICE=pgm -sOutputFile=swpatmccarthy.en-%02d.pgm -
swpatmccarthy.en/swpatmccarthy.en.html:	swpatmccarthy.en.dvi
	rm -fR swpatmccarthy.en;latex2html swpatmccarthy.en.tex
xview:	swpatmccarthy.en.dvi
	xdvi -s 8 swpatmccarthy.en &
tview:	swpatmccarthy.en.tty
	browse swpatmccarthy.en.tty 
gview:	swpatmccarthy.en.ps
	ghostview  swpatmccarthy.en.ps &
print:	swpatmccarthy.en.ps
	lpr -s -h swpatmccarthy.en.ps 
sprint:	swpatmccarthy.en.001
	for F in swpatmccarthy.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	swpatmccarthy.en.faxsnd
	faxsndjob view swpatmccarthy.en &
fsend:	swpatmccarthy.en.faxsnd
	faxsndjob jobs swpatmccarthy.en
viewgif:	swpatmccarthy.en.gif
	xv swpatmccarthy.en.gif &
viewpbm:	swpatmccarthy.en.pbm
	xv swpatmccarthy.en-??.pbm &
vieweps:	swpatmccarthy.en.eps
	ghostview swpatmccarthy.en.eps &	
clean:	swpatmccarthy.en.ps
	rm -f  swpatmccarthy.en-*.tex swpatmccarthy.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} swpatmccarthy.en-??.* swpatmccarthy.en_tex.* swpatmccarthy.en*~
swpatmccarthy.en.tgz:	clean
	set +f;LSFILES=`cat swpatmccarthy.en.ls???`;FILES=`ls swpatmccarthy.en.* $$LSFILES | sort -u`;tar czvf swpatmccarthy.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
