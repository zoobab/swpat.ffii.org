# -*- mode: makefile -*-

swpatamccarthy.en.lstex:
	lstex swpatamccarthy.en | sort -u > swpatamccarthy.en.lstex
swpatamccarthy.en.mk:	swpatamccarthy.en.lstex
	vcat /ul/prg/RC/texmake > swpatamccarthy.en.mk


swpatamccarthy.en.dvi:	swpatamccarthy.en.mk
	rm -f swpatamccarthy.en.lta
	if latex swpatamccarthy.en;then test -f swpatamccarthy.en.lta && latex swpatamccarthy.en;while tail -n 20 swpatamccarthy.en.log | grep -w references && latex swpatamccarthy.en;do eval;done;fi
	if test -r swpatamccarthy.en.idx;then makeindex swpatamccarthy.en && latex swpatamccarthy.en;fi

swpatamccarthy.en.pdf:	swpatamccarthy.en.ps
	if grep -w '\(CJK\|epsfig\)' swpatamccarthy.en.tex;then ps2pdf swpatamccarthy.en.ps;else rm -f swpatamccarthy.en.lta;if pdflatex swpatamccarthy.en;then test -f swpatamccarthy.en.lta && pdflatex swpatamccarthy.en;while tail -n 20 swpatamccarthy.en.log | grep -w references && pdflatex swpatamccarthy.en;do eval;done;fi;fi
	if test -r swpatamccarthy.en.idx;then makeindex swpatamccarthy.en && pdflatex swpatamccarthy.en;fi
swpatamccarthy.en.tty:	swpatamccarthy.en.dvi
	dvi2tty -q swpatamccarthy.en > swpatamccarthy.en.tty
swpatamccarthy.en.ps:	swpatamccarthy.en.dvi	
	dvips  swpatamccarthy.en
swpatamccarthy.en.001:	swpatamccarthy.en.dvi
	rm -f swpatamccarthy.en.[0-9][0-9][0-9]
	dvips -i -S 1  swpatamccarthy.en
swpatamccarthy.en.pbm:	swpatamccarthy.en.ps
	pstopbm swpatamccarthy.en.ps
swpatamccarthy.en.gif:	swpatamccarthy.en.ps
	pstogif swpatamccarthy.en.ps
swpatamccarthy.en.eps:	swpatamccarthy.en.dvi
	dvips -E -f swpatamccarthy.en > swpatamccarthy.en.eps
swpatamccarthy.en-01.g3n:	swpatamccarthy.en.ps
	ps2fax n swpatamccarthy.en.ps
swpatamccarthy.en-01.g3f:	swpatamccarthy.en.ps
	ps2fax f swpatamccarthy.en.ps
swpatamccarthy.en.ps.gz:	swpatamccarthy.en.ps
	gzip < swpatamccarthy.en.ps > swpatamccarthy.en.ps.gz
swpatamccarthy.en.ps.gz.uue:	swpatamccarthy.en.ps.gz
	uuencode swpatamccarthy.en.ps.gz swpatamccarthy.en.ps.gz > swpatamccarthy.en.ps.gz.uue
swpatamccarthy.en.faxsnd:	swpatamccarthy.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo swpatamccarthy.en-??.g3n`;source faxsnd main
swpatamccarthy.en_tex.ps:	
	cat swpatamccarthy.en.tex | splitlong | coco | m2ps > swpatamccarthy.en_tex.ps
swpatamccarthy.en_tex.ps.gz:	swpatamccarthy.en_tex.ps
	gzip < swpatamccarthy.en_tex.ps > swpatamccarthy.en_tex.ps.gz
swpatamccarthy.en-01.pgm:
	cat swpatamccarthy.en.ps | gs -q -sDEVICE=pgm -sOutputFile=swpatamccarthy.en-%02d.pgm -
swpatamccarthy.en/swpatamccarthy.en.html:	swpatamccarthy.en.dvi
	rm -fR swpatamccarthy.en;latex2html swpatamccarthy.en.tex
xview:	swpatamccarthy.en.dvi
	xdvi -s 8 swpatamccarthy.en &
tview:	swpatamccarthy.en.tty
	browse swpatamccarthy.en.tty 
gview:	swpatamccarthy.en.ps
	ghostview  swpatamccarthy.en.ps &
print:	swpatamccarthy.en.ps
	lpr -s -h swpatamccarthy.en.ps 
sprint:	swpatamccarthy.en.001
	for F in swpatamccarthy.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	swpatamccarthy.en.faxsnd
	faxsndjob view swpatamccarthy.en &
fsend:	swpatamccarthy.en.faxsnd
	faxsndjob jobs swpatamccarthy.en
viewgif:	swpatamccarthy.en.gif
	xv swpatamccarthy.en.gif &
viewpbm:	swpatamccarthy.en.pbm
	xv swpatamccarthy.en-??.pbm &
vieweps:	swpatamccarthy.en.eps
	ghostview swpatamccarthy.en.eps &	
clean:	swpatamccarthy.en.ps
	rm -f  swpatamccarthy.en-*.tex swpatamccarthy.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} swpatamccarthy.en-??.* swpatamccarthy.en_tex.* swpatamccarthy.en*~
swpatamccarthy.en.tgz:	clean
	set +f;LSFILES=`cat swpatamccarthy.en.ls???`;FILES=`ls swpatamccarthy.en.* $$LSFILES | sort -u`;tar czvf swpatamccarthy.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
