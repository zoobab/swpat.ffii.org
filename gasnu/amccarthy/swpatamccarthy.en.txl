<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Arlene McCarthy MEP and Software Patents

#descr: British Member of the European Parliament, Labor/PSE, appointed by the
Europarl Committtee for Legal Affairs and Internal Market (JURI) in
2002/03 to report on the software patentability directive.  In June
2002 Arlene McCarthy published a short report which aggressively
promoted the agenda of the European Patent Office (EPO).   The paper
charged the patent critics of having provided only invalid arguments,
but failed to quote or refute any of these arguments.  Meanwhile
various people from the EPO and patent lobby were in contact with
McCarthy and boasted that their viewpoint would prevail and the
discussion would soon be over.  A hearing arranged by McCarthy and the
europarl webspace dedicated to the hearing both offered minimal room
for critical views.  Arlene McCarthy's draft report of 2003/02/19, her
explanatory note of 2003/05/03 and her refusal to accept any
amendments which limit patentability or patent enforcability in any
way show complete dedication to the interests of patent owners.  While
staying away from all informed discussions and conferences on software
patent questions, McCarthy has actively reaching out to the media in
order to present herself as a victim of a %(q:dishonest and
destructive misinformation campaign) who is sincerely trying to limit
patentability.  McCarthy has been serving the recording industry and
various projects of the Commission's Directorate for the Internal
Market (Bolkestein) with equal fervor. McCarthy's political business
model appears to consist in (1) aggressively and unconditionally
serving of the Commission and big business (2) obtaining favorabl
treatment by Commission and Big Business for her electoral region /
constituency.

#Aso: Already in 2002/02, when rumours about Arlene McCarthy's imminent
nomination as a rapporteur on the software patentability directive
reached us, insiders told us that her report would almost certainly be
pro patent.  They saw her appointment as orchestrated by the british
patent establishment, which is also pushing the directive at the
European Commission.  Some say that McCarthy is a %(q:creature of
%(ah:Anthony Howard)). We have occasionally contacted McCarthy's
secretariat in May 2002, partly due to a %(eh:hearing at the European
Parliament), just enough to make sure they are aware of our
%(cp:demands) and documentation.  Unfortunately, Arlene McCarthy has
indeed hardly departed an inch from her initial positions, regardless
of what arguments were brought forward.

#rie: McCarthy's Political Interests

#iVn: Suppressing Patent-Critical Views in the European Parliament

#eun: Putting everybody at an equal level of Non-Understanding

#nWf: Shifting Focus to her Opponents' Impoliteness

#ubo: McCarthy's friends and counsels from UK/CEC/EPO patent community

#ero: Campaigning the European Parliament for Music Industry rights

#ano: McCarthy and Microsoft

#aWt: McCarthy's Reelection

#Set: Some relevant pages

#ain: possible affiliations and sources of influence

#isp: McCarthy seems mainly interested in regional politics. She is however
also a %(q:governor) of the %(q:European Internet Foundation), a forum
which creates opportunities for its members, mainly large corporations
and business associations with permanent Brussels representatives, to
come into contact with MEPs. Perhaps it is this position as
%(q:governor) which has led McCarthy to specify in one of her
%(cv:vitas) that %(q:e-commerce) is one of her fields of interest. For
McCarthy, E-Commerce, the Internet and even the patent system seem to
be rather remote subjects. Although the Software Patent Directive is
the single most prominent subject which has been assigned to her since
her accession to the Legal Affairs Commission (JURI), McCarthy has
been keeping an extremely low profile. On her websites she never
mentions the subject but instead does occasionally mention her ability
to successfully lobby for important regional interest groups. It seems
that putting one's political skills to the service of potent clients
is one of the basic patterns of McCarthyan politics.

#eli: Through these regional interest group activities, McCarthy may have
come into contact with the venture capital scene.  In JURI discussions
of spring 2003, she supported her pro-patent viewpoints by citing the
positive experience of a couple whose garage enterprise had grown big
thanks to speech recognition patents.  While we do not know who this
enterprise was and whether it survived the dotcom crash, it was indeed
commonplace around 1998 for VC investors in the software field to ask
for patents.

#Wnl: McCarthy did everything to prevent the viewpoint of the critics from
being given any space in the debate.  Her papers either completely
ignore them or attack them without quoting any references.  In the
JURI hearing which she had arranged, initially there was only space
for a 5 minute speech of a representative of the Eurolinux Alliance,
which had become the organisational basin for a numerically very large
group of individuals and companies who oppose software patentability. 
Even this 5 minute speech was to come only after a sequence of 4-5
statements made by patent lobby representatives.  Later, due to the
intervention of other MEPs, the hearing setting was slightly improved,
but on the hearing website further texts from uninvited patent lobby
organisations were published while demands from the Eurolinux side for
publication of more papers from patent-critical SMEs were declined.  A
second hearing, organised by the Greens/EFA 3 weeks later in order to
make up for the bias of McCarthy's hearing, was not attended by
McCarthy.  Several MEPs openly criticised McCarthy for this at the
hearing.

#nin: Around the time of the Nov 7th hearing, McCarthy tried to stress her
%(q:complete open-mindedness).  On 2002/11/06 McCarthy underlined this
open-mindedness by receiving Jean-Paul Smets (Eurolinux).  At a
hearing on the following day, she said that patentability rules should
be clarified in terms of sample patents.  In early december she
moreover gave a speech to the JURI committee, in which she suggested
that the current patent practise of the EPO produces questionable
patents and might indeed need some fixing.  This however ultimately
did not lead to further discussions or a change of mind.

#eWt: During the interval from december to february 2002, McCarthy lived in
seclusion from her peers, including fellow PSE members of parliament. 
When her draft report came out in 2003/02/19 and JURI meetings
started, she stayed away from many meetings for reasons of illness and
regularly complained about lobbying against her at other meetings,
apparently attempting to present herself as a victim and trying to
shift the focus of the discussion onto alleged unfair treatment of her
person.  She intensified this approach in early May, when she
complained to Hartmut Pilch from FFII/Eurolinux with cc to Daniel
Cohn-Bendit MEP and later to various press organs and to JURI that she
had received an invitation to a FFII/Eurolinux-organised public
meeting too late and that FFII/Eurolinux was trying to exclude her
from the discussion.

#Wid: McCarthy's policy seems to be consistent with the general strategy of
the patent lobby on this issue:  avoid discussions as far as possible,
and when you do have to discuss, speak a %(el:dogmatic language from
the EPO which nobody understands), so that you can then safely, from a
position of authority, look down on your audience and point out that
they do not understand %(q:the law).

#fib: The critics of McCarthy's approach have repeatedly tried to ask her to
explain her view in terms of a set of example patents. She has
consistently refused to answer these questions. Instead she has either
stayed quiet or lectured people with a standard FAQ document which
again speaks only in abstract dogmatic terms. She tried to justify
this approach by saying to MEPs on 2003/06/16: %(bq:We have attemted
to set some limits in perhaps a moderately restrictive way, without
entirely reinventing patent law, which I would hasten to add, we are
not in ability to do that, we are legislators to create framework and
laws for interpretation by experts, but we are not experts ourselves.)

#sir: McCarthy is said to have studied and worked as a research fellow in
the political science field at various universities in UK, DE and FR.
Her papers written for the Europarl are however very far from
scientific papers. Espcecially the level of reasoning McCarthy's
software patentability directive reports would hardly be good enough
to pass any exam in first-year university courses. This was %(ek:said)
(in somewhat more polite form) by a group of well-known innovation
economists in August 30. In 2003/02/19 McCarthy published, after a
year of research by her and her staff, a completely unresearched
report of such a poor quality that even those who are backing McCarthy
have little reason to feel satisfied. This paper repeats many of those
doctrines whose evident flaws had already been pointed out when she
presented them in another paper 8 months earlier. McCarthy further
ignored criticism of this paper and presented the same flawed
doctrines once again in a paper of May 2003. Her approach has been to
avoid informed discussions and instead contact journalists or
uninformed SME representatives and tell them that she was doing her
best to exclude algorithms and business methods from patentability and
that a group of %(q:misguided lobbyists), financed by George Soros,
was misrepresenting her views.

#rpo: Meanwhile McCarthy and her allies %(LST) have steadfastly opposed all
amendment proposals which could have effectively limited patentability
or patent enforcability in any way.

#jlW: At discussions within the Legal Affairs Committee (JURI), McCarthy has
claimed that software patents are good for %(q:a family enterprise
that works on speech recognition and protects itself by means of a
patent). Also, she lashed out against %(q:those lobbyists who are
abusing the debate as an opportunity to attack [ those poor honest
hardworking officials from ] the European Patent Office).

#Wcn: McCarthy is fully aware of the British consulations, of the
meaningless of her %(q:technical contribution) talk (pointed out e.g.
by her compatriot Dominic Sweetman in the Europarl hearing), of the
uselessness of isolated %(q:speech recognition patents), of the
interests of SMEs on this subject, of %(mh:Mandy Haberman's letter to
the European Parliament) etc.

#leW: which refutes claims about her patent-based family enterprise made by
UNICE's Nguyen at the 2002/11/07 hearing

#WuW: In a letter to the Guardian of June 2003, McCarthy even attacks the
GNU GPL as %(q:just another form of monopolism).  It seems that
McCarthy may be interested in arousing flamewars and then presenting
herself as a victim of unreasonable invectives.  This can be a fairly
successful strategy, because politeness standards are very high in the
European Parliament and fellow MEPs may feel compelled to come to the
rescue of an MEP who is being subjected to heated criticism.

#tmf: Given that a circle of corporate patent lawyers around the UK Patent
Office had a decisive influence on the formation of the present
directive proposal and that these people are pushing it in the name of
the UK government, one could assume that the same UK patent promoters
are skilled enough to exert influence on who will be the rapporteur in
the JURI (Legal Affairs and Internal Market) commission.  Assuming
that McCarthy was their choice, one could only congratulate them for
this move.  The socialist fraction of the European Parliament is the
single largest force which could be expected to oppose the directive. 
At least the French, Belgian and Spanish socialists have already made
binding programmatic statements against software patents. McCarthy's
potential of splitting the PSE (Europarl socialists) is further
enhanced by the fact that she has in the past often voted together
with PSE, Greens and Liberals against the Conservatives.

#oWh: McCarthy's proposals themselves do not look completely ghostwritten. 
At least the advocacy parts contain unmistakeable personal
characteristics.  Much of the amendment proposals themselves however
is reminiscent of Anthony Howard (from CEC) and of industrial patent
lawyers.  Although some EPO officials have also visited McCarthy and
even boasted in semi-public circles that they, together with McCarthy,
would quickly get their point of view through the parliament, much of
what McCarthy wrote in 2003 is more reminiscent of UK and CEC patent
lawyers than of the EPO.  Some of McCarthy's amendment proposals go
slightly further than most at the EPO would in undermining
patentability standards.

#ssa: This appeal prepared the ground for another directive which the
European Commission had put to the parliament at the request of phono
industry and other lobby organisations and for which %(JF), herself an
IP lobbyist with vested family interests (Vivendi, Aventis), got
herself appointed as rapporteur.  On June 5th 2003, McCarthy, wore a
t-shirt %(q:Piracy is not a victimless crime) to help lobby fellow
MEPs to support her campaign. The campaign organiser IFPI,
organisation of the phonographic industry, celebrated this climax of
their campaign with a press release.

#toi: While IFPI and Vivendi are not likely to profit from software patents,
they may feel some diffuse sympathy to anything that promises control
of downstream economy, and they are of course, through various
industry lobby organisations, in bed with the patent lobby.  This
diffuse and cozy sense of belonging to the same group of people is
probably, more than any rational policy consideration, what binds
McCarthy and the software patent lobby together.

#nWu: As of 2003/07/23, this page contained the photo of Arlene McCarthy
with two friends from the music industry and an explanation %(q:Arlene
with Jay Berman and Sir George Martin, campaigning for Music Industry
rights in the European Parliament)

#Sut: McCarthy and a few other MEPs travel to Seattle to visit Microsoft
together with the European Internet Foundation, an EU lobbying group
financed by large corporations and designed to facilitate
communication between MEPs and the Brussels representatives of thesese
corporations.

#til: MEPs at Boeing Facilities in Seattle 2003/08

#rkW: At the %(ei:EIF dinner after the software patentability hearing of
2003-11-07), McCarthy shared the table with a Brussels representative
of Microsoft.

#ehb: In her letter to the Guardian, McCarthy spreads Microsoft FUD about
the GNU GPL, saying that it is just another form of monopoly and
implying that non-free software needs patents.

#inl: At the OECD Conference on Aug 29 in Paris, Marie-Thérése Huppertz, who
has been lobbying for software patents on behalf of Microsoft, cited
this McCarthy letter to the Guardian as an example of the thinking of
a true European patriot.

#lCe: Rebellion in McCarthy's Electorate

#Wec: Which region/cities elected McCarthy?

#awe: By what mechanism was McCarthy elected?

#cWi: Upcoming elections

#cWd: Arlene McCarthy is a Labour MEP for Chester and Merseyside.

#icO: Her constituency covers the following counties: Cumbria, Lancashire,
Chester, Cheshire.  Cities and major towns include: Manchester, Wigan,
Blackburn. Preston, Carlisle, Bolton, Bury, Rochdale, Oldham,
Stockport, Crewe, Macclesfield, Runcorn.

#eha: In the previous election (2000) Arlene McCarthy came first on the
labour party preference list. This time, she came second. She may have
been promoted to second place as a result of the Labour party rules
which attempt to balance representation of women and men.

#Wpv: The MEP local elections are done on a proportional representation
system. The party selects a list of candidates in preference order via
an internal selection process.

#MoW: The north west region has 9 MEP candidates. If Labour, Conservative
and Libdems each had 33% of the votes, each would have 3 MEPs. The
three at the top of the list for each party will be selected.

#peo: Internal party selection has been done. Public election is in 2004.

#Wen: McCarthy is at the top of the Labour list, competing mainly with
Conservatives and Liberal Democrats.

#aWo: McCarthy Quarrel with green MEPs over failed CEC directive on
corporate takeover

#cWW: Arlene McCarthy supported a proposal by European Commission for a
directive to liberalise the stock market with the effect of
facilitating hostile takeovers.  This met with resistance from a wide
alliance of forces, including the german government.  When the
directive failed in the European Parliament, Arlene McCarthy published
a letter in the Guardian in which she accuses the Greens of naively
playing into the hands of the Germans.  Two green MEPs sent letters
which reject McCarthy's %(q:ludicrous accusations).  The directive
proposal was from Frits Bolkestein, the DG Internal Market
Commissioner whose software patent directive proposal McCarthy
supported with similar fervor a year later.

#MEo: Arlene McCarthy 1998 on EU reform of Regional Policy

#WWa: McCarthy advertises positions of the European Commission and the UK
government.  She explains that there is no need for an %(q:efficiency
reserve fund), because that could mean increasing %(q:red tape), i.e.
bureaucratisation.

#0Ps: LGIB 2000/01/23: European Parliament appoints committees

#fCp: news report about allocation of members of the european parliament to
the individual committees.  Accordingly, the decision to allocate
McCarthy to the JURI committee was taken about 1 month before the
software patentability directive proposal came out and McCarthy was
appointed rapporteur.

#rWo: One sentence of McCarthy's vita reads: %(bc:Before her election as a
Euro MP, Arlene McCarthy was in charge of running European programmes
at Kirklees Metropolitan Borough Council, attracting substantial
amounts of money.) This must have been 1992-94.

#oei: This page extensively and favorably reports on McCarthy's Europarl
activities but, except for a hint to interest in %(q:e-commerce), does
not mention her involvment in questions of software or patents:
%(bc|Labour Spokesperson and Government Link on the Legal Affairs and
Internal Market Committee and a substitute Member on the Regional
Policy, Transport and Tourism Committee. Arlene is actively involved
in the debate on the future of European grants for the UK and works
closely with the Labour Government on the development of regional
policy in the UK. ... Arlene wrote the Parliament's report on the
reform of European regional funds post 2000. ...Arlene has
successfully led a campaign in defence of local textile companies to
stop duties being levied that could lead to major job losses locally.
As draftsperson for the Parliament's Budget Report in 1997 she lobbied
successfully for an additional £40 million for the coalfield areas. 
Special interests: EU regional and industrial policy, urban
regeneration, e-commerce.)

#wnP: Glasgow, 96/04: Arlene McCarthy MEP speech

#bcn: Another speech by McCarthy on her chief themes, such as regional
equilibria, employment in rural areas etc

#ycp: McCarthy biography as specified on the Europarl website: %(bc:...
Worked with the Socialist Group in the European Parliament (1989-1992)
and with Mr Ford, former Leader of the European Parliamentary Labour
Party (1990-1992). Lecturer in politics, Institute for International
Politics and Regional Studies, Free University of Berlin (1991-1992).
Principal European Liaison Officer, Kirklees Metropolitan Council
(until 1994).  The obligatory %(q:Declaration of Financial Interest)
was still missing from this page as of June 2003.

#rWj: The site contains quite a few press releases, including one from
2003/02/18 about protecting young girls from harmful breast implants,
but nothing about patents as of 2003/03/07, if the search engine is
working correctly.

#prd: European Internet Foundation

#eWW: An organisation of which McCarthy is a %(q:vice governor) and many
other MEPs hold similar posts.  EIF is financed by business and
associate members.  Associate members are business associations like
EICTA, BSA etc, except maybe for CECUA and one university.  Fee is 500
euros/year for associations, + cost of participation to each event. 
EIF frequently organises events which are attended by MEPs and
permanent Brussels representatives of member associations and
companies.

#ief: McCarthy is involved in regional industrial policies in northwestern
England and in the venture capital scene and thereby is probably
frequently exposed to the %(q:venture capital requires patents)
fallacy.

#ohi: UK-based lobbying association which operates in a very similar way as
EIF

#roW: An organisation which is closely related to McCarthy's work and which
has written a paper which strongly warns against software patents. The
CEC/BSA software patentability directive mentions this paper because
the COR is a EU organisation whose opinion must be consulted.
McCarthy's own reports do fail to mention the COR paper as well as
other papers written by EU institutions, except for those that argue
in favor of software patents. At least in the patent context the COR
does not seem to be a source of authority for McCarthy.

#aat: The US government has an official policy of pushing for extension and
strengthening of patentability everywhere in the world, and Britain
has a tradition of aligning itself with the US, on patent matters
also.  The Blair government has further strengthened this
constellation, and McCarthy appears to be a close follower of Blair. 
The Conservatives also support this position by default.  Criticism
has come only from the Liberal Democrats.

#Ets: If you are located in the North-East region: Contact local political
parties for ideas on how to make McCarthy's software patent activities
known in the region.  Start a discussion to raise awareness in all
local LUGs.

#WWo: Contact your EuroMPs through %(uk:UK Keele MEP list) or through the
%(ff:FFII support tool).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/21.3/site-lisp/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@ffii.org ;
# login: astohrl ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatamccarthy ;
# txtlang: en ;
# multlin: t ;
# End: ;

