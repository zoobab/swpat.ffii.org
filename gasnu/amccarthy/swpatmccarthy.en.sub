\begin{subdocument}{swpatmccarthy}{Arlene McCarthy and Software Patents}{http://swpat.ffii.org/players/mccarthy/index.en.html}{Workgroup\\swpatag@ffii.org}{British Member of the European Parliament, Labor/PSE, appointed by the Europarl Committtee for Judicial Affairs and Internal Market (JURI) in 2002/03 to report on the software patentability directive, previously reporter for community patent and other directive proposals.  In June 2002 Arlene McCarthy published a paper which aggressively promoted the agenda of the European Patent Office (EPO).   The paper charged the patent critics of having provided only invalid arguments, but failed to quote and refute any of these arguments.  Meanwhile various people from the EPO and patent lobby were in contact with McCarthy and boasted that their viewpoint would prevail and the discussion would soon be over.  At the same time McCarthy continued to stress her ``complete open-mindedness''.  In November McCarthy received Jean-Paul Smets (Eurolinux) to reconfirm this open-mindedness.  At the hearing in November 2002 she indeed sounded somewhat more thoughtful.   In early december she moreover gave a speech to the JURI committee, in which she said that the current patent practise of the EPO is a problem and that something must be done to limit patentability so that software developpers and SMEs do not suffer.  Some people suspected that this apparent change of mind had to do with changes of mind in London.  McCarthy is said to be a close follower of Tony Blair.  In early 2003, McCarthy largely secluded herself from other MEPs and kept everybody waiting for her expected proposal to JURI.  Several fellow socialist MEPs sought access to her and failed.  It was rumored that McCarthy was again only receiving people from the EPO and multinationals.  These fears were confirmed by McCarthy's paper which came out on 2003/02/19.}
\begin{center}
\begin{tabular}{|C{44}|C{44}|}
\hline
\mbox{\includegraphics{amccarthy}} & Already in 2002/02, when rumours about Arlene McCarthy's imminent nomination as a rapporteur on the software patentability directive reached us, insiders told us that her report would almost certainly be pro patent.  They saw her appointment as orchestrated by the british patent establishment, which is also pushing the directive at the European Commission.  We had mail and phone to McCarthy's secretariat in May, partly due to a hearing at the European Parliament\footnote{http://swpat.ffii.org/events/2002/swpparl025/index.en.html}, just enough to make sure they are aware of our demands\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/demands/index.en.html} and documentation.  Since these are backed by a broad consensus of software professionals and economists, we thought we could have given McCarthy and her staff reasons to investigate the issue more in depth.\\\hline
\end{tabular}
\end{center}

\begin{sect}{ammcref}{Some relevant pages}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf McCarthy 2003-02-19 Software Patent Directive Proposal\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy0302/index.en.html}}}

\begin{quote}
Arlene McCarthy, British Labor MEP appointed by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI) to report on the European Commission's Software Patentability Directive Proposal (CEC/BSA Proposal), suggests that the European Parliament should enact the CEC/BSA version with minor amendments.  McCarthy completely disregards the public discussion on the subject, including economic and legal expertises ordered by the European Parliament.  McCarthy reinforces the CEC/BSA proposal's pro-patent bias by a few more unfounded assertions, such as that companies like Ericsson and Alcatel need software patents to finance their R\&D, that SMEs need ``good'' software patents, that patents are ``evidently'' of vital importance at a time where capital is moving to countries with lower production costs.  McCarthy uses the term ``computer-implemented inventions'' as a synonym for ``software innovations'' and stipulates that these ``by their very nature belong to a field of technology''.  McCarthy says she wants to prevent ``bad patents'' and to explain what is patentable and what not.   Unfortunately her amendments do not achieve this aim.  On the contrary, they add more confusion to an already confusing CEC/BSA proposal.  If the McCarthy Software Patent Directive is passed, the European Patent Office (EPO) will have nothing to worry about, as long as it continues to grant patents on whatever the corporate patent lawyers in its Standing Advisory Committee (SACEPO) might find worth monopolising.  McCarthy insists that not the European Parliament but the European Patent Office (EPO) is responsible for legislation on the question of patentability: ``we are proposing nothing revolutionary ... but only harmonising the status quo''.  In reality McCarthy is proposing to make recent revolutionary decisions of a few influential people at the EPO irreversible and binding for all of Europe.
\end{quote}
\filbreak

\item
{\bf {\bf MEP Arlene McCarthy 2002-06-19: Report on the CEC/BSA Directive Proposal\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy0206/index.en.html}}}

\begin{quote}
In a democratic Europe, one would expect the legislative power, such as the European Parliament, to critically monitor the activities of the executive and judicial powers, such as the European Commission (CEC) and the European Patent Office (EPO), and to correct their shortcomings.  The british Labor MEP Arlene McCarthy, as a rapporteur to the European Parliament's Committee for Legal Affairs and the Internal Markt on the proposed software patentability directive COM(02) 92, however merely reaffirms the beliefs of the patent lawyers who have been dominating the dossier at the CEC and the EPO.  McCarthy's report disregards the opinions of virtually all respected programmers and economists and fails to correctly identify the controversial issues.  Yet at the end McCarthy asks a few questions which provide us with a certain hope that eventually a more open debate can be brought about.  Please read our answers below.
\end{quote}
\filbreak

\item
{\bf {\bf IPR Commission 2002-09: Final Report\footnote{http://www.iprcommission.org/graphic/documents/final\_report.htm}}}

\begin{quote}
It had been speculated that McCarthy's apparent change of mind in november might be related to this paper, which again has some british governmental background and could therefore be seen as a souce of authority for McCarthy.
\end{quote}
\filbreak

\item
{\bf {\bf Labour Party - Arlene McCarthy Member of the European Parliament ...\footnote{http://www.arlenemccarthy.labour.co.uk/}}}
\filbreak

\item
{\bf {\bf http://www.eplp.org.uk/amcc.htm}}
\filbreak

\item
{\bf {\bf http://www.politicallinks.co.uk/politics/BIOG/MEP\_BIOGS/30541.stm}}
\filbreak

\item
{\bf {\bf Glasgow, 96/04: Arlene McCarthy MEP speech\footnote{http://www.metrex.dis.strath.ac.uk/en/pastconfs/glasgow/opening/mccarthy.html}}}
\filbreak

\item
{\bf {\bf European Parliament UK Office - UK MEPs for the North West\footnote{http://www.europarl.org.uk/uk\_meps/txukmeps/txnorthwest.html}}}
\filbreak

\item
{\bf {\bf Quarrel with green MEPs over failed CEC directive on corporate takeover\footnote{http://www.jeanlambertmep.org.uk/downloads/letters/0108takeovr.htm}}}
\filbreak

\item
{\bf {\bf commissione giuridica e mercato interno a nominato Arlene McCarthy come relatrice per parere\footnote{http://www.ricercagiuridica.com/ilaw/visual.asp?num=278}}}
\filbreak

\item
{\bf {\bf About the Constituency of Arlene McCarthy in High Peak\footnote{http://www.cel.co.uk/labour/tom/constitu.htm}}}
\filbreak

\item
{\bf {\bf Photos of the SURF launch: include AMcC\footnote{http://www.surf.salford.ac.uk/documents/launchphotos.htm}}}
\filbreak

\item
{\bf {\bf European Information Service --- Article on Regional Policy Reform by AMcC\footnote{http://www.lgib.gov.uk/eis/1998/jun/articles/01.htm}}}
\filbreak

\item
{\bf {\bf European Parliament appoints committees\footnote{http://www.lgib.gov.uk/news/story.html?newsId=127}}}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{ammcinf}{possible affiliations and sources of influence}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf EURIM\footnote{http://www.eurim.org}}}
\filbreak

\item
{\bf {\bf American Chamber of Commerce\footnote{http://swpat.ffii.org/papers/eukonsult00/parker/index.en.html}}}
\filbreak

\item
{\bf {\bf involved in a venture capital fund in Liverpool (regional industrial policies), therefore possibly exposed to the ``venture capital requires software patents'' fallacy.}}
\filbreak

\item
{\bf {\bf Committee of the Regions}}
\filbreak

\item
{\bf {\bf The UK Patent Family and Software Patents\footnote{http://swpat.ffii.org/players/uk/index.en.html}}}

\begin{quote}
Much of the lobbying for software patents in Europe has been done by British patent lawyers and patent officials wearing the hat of the European Commission or the British government.  Most of the law-drafters and alleged ``independent contractors'' for the European Commission (CEC) were members of the British patent family.  The UK Patent Office (UKPO) was the first national patent office to officially follow the European Patent Office (EPO) in allowing direct patent claims to computer programs in 1998.  The UKPO has also succeeded in keeping its hand on the British government's patent policy, in moderating a public consultation which showed strong public opposition to software patentability, and, most admirably, in interpreting this public opinion as a legitimation basis for \begin{enumerate}
\item
establishing software (including business method) patents in Britain and

\item
pushing Brussels to establish them in all of Europe.
\end{enumerate}  The UK Patent Family appears to be the strongest single force behind the European Commission's software patentability directive project.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
% mode: mlatex ;
% End: ;

