<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Gerhard Schröder

#descr: Der %(q:Genosse der Bosse) hat bekanntlich ein besonders offenes Ohr für die Einflüsterungen hoher Wirtschatsfunktionäre und insbesondere eine anhaltende Neigung, sich an Seite von Microsoft-Führungskadern öffentlich zu zeigen und stolz auf Partnerschaften mit MS zu sein.  Seine Herkunft aus dem Anwaltsberuf, seine Orientierung an anglo-amerikanischen Vorbildern (bei relativer Distanz zu Frankreich) könnte für Software-Urheber und -anwender ebenfalls Anlass zu Sorge sein: in Frankreich fielen die Entscheidungen gegen Softwarepatente auf der Ebene des Premierministers.  Bislang hat sich Schröder zum Thema Softwarepatente, soweit bekannt, nicht geäußert, aber sein Verhalten im Zusammenhang mit der Kopierschutzrichtlinie lässt wenig Verständnis für informationelle Infrastrukturen erwarten.

#Ier: Im Frühjahr 1998 zeigt sich Schröder stolz mit einem Symbol wirtschaftlichen Erfolges.  Auch bei dem CeBIT-Auftritt von Microsoft-Chef Ballmer, bei dem dieser den Einsatz von Softwarepatenten zur Verhinderung kompatibler DotNet-Implementationen ankündigte, war Schröder als Mitredner dabei und lud Microsoft zu stärkerem Engagement im öffentlichen Sektor in Deutschland ein.  Entwaffnenderweise gibt Schröder gelegentlich zu erkennen, dass er von Computerprogrammen nichts verstehe.  Nur so erklärt sich wohl, dass er ausgerechnet die Symbolik der monopolistischen Stagnation wählt, um seine Modernität auszudrücken.

#BSK: Bei der Umsetzung der EU-Kopierschutzrichtlinie dato 2002-07-31 wischte Schröder im Kabinett der Minister persönlich Einwände der Verbraucherschützer beiseite und sorgte für eine Übererfüllung der Brüsseler Vorgaben.

#Kle: Kanzler holt zum Gegenschlag aus

#Ido: Im Rahmen des Wahlkampf-Aktivismus um den Erfurter Amoklauf äußert Gerhard Schröder persönliche Enttäuschung darüber, dass das Computerspiel %(q:Counterstrike) nicht auf eine Liste jugendgefährdender Werke gesetzt wurde.

#S0e: Schröder 2002-03-12: Rede auf CeBit

#Dna: Der Vorredner, MS-Chef Ballmer, wird persönlich angesprochen, als Repräsentant Amerikas mit Beteuerungen besonderer Solidarität bedacht und zu stärkerem Engagement im öffentlichen Sektor (Schulen ans Netz, D21) eingeladen.  Von einem Problembewusstseini für die Rolle des Staates als Garant einer offenen Infrastruktur und Verhinderer von Monopolen keine Spur.  Die Rede wurde im Fernsehen (Phoenix) übertragen.

#CWd: Clement gilt als einer der engsten Vertrauten/Verbündeten Schröders in der SPD

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatschroeder ;
# txtlang: de ;
# multlin: t ;
# End: ;

