<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Fea: Fraunhofer TI brüstet sich mit Trivialpatent

#Ene: Bericht und Diskussion über das ISI-Gutachten auf dem Verteiler swpat@ffii.org

#Fkr: Fraunhofer-Studie fordert Swpat

#FDs: FFII-Diskussion

#Dti: Der Linuxtag e.V. bemängelt, dass das ISI-Gutachten kein Wort über die Bedeutung der freien/quelloffenen Softwareentwicklung verliert.

#DsW: Die Heise-Journalisten spötteln über das ISI-Gutachten

#DWu: Die Untersuchung des Fraunhofer-Institutes beklagt, wie unsystematisch und eklektisch die Softwareunternehmen vorgehen und empfiehlt eine Politik des %(q:Technologietransfers) zwischen diesen Unternehmen und fortschrittlichen Fortschungseinrichtungen wie Fraunhofer.  Um die Organisation solcher Kooperationen zu erleichtern und eine ingenieurmäßige Ausrichtung der Softwarebranche zu fördern, bedarf es laut ISI der Klarstellung, dass Computerprogramme patentierbar sein müssen.  Diese Schlussfolgerung scheint eine Art Standard-Topos bei Fraunhofer-Gutachten zu sein, die im Auftrag des BMBF erstellt werden.

#Bnn: Besprechung des obigen Textes im Diskussionsforum swpat@ffii.org

#KSe: Kritik an ISI-Projektbeschreibung

#DoW: Das BMWi erteilte dem Fraunhofer ISI einen Auftrag zur Erstellung einer Studie über die Auswirkungen von Softwarepatenten auf das Kalkül der betroffenen Unternehmen.  In der Projektbeschreibung werden die Vorzüge von Patenten wortreich argumentativ begründet und die Einwände dagegen nur kurz und ohne Argument erwähnt.

#Scb: Ankündigung einer Studie des Fraunhofer Instituts für Innovationsforschung (ISI), in welchen die Mechanismen des %(q:Technologietransfers) zwischen Wissenschaft und Hochschulen beschrieben und Probleme aufgezeigt werden sollen.

#ZWi: Zentrale Patentagentur für das deutsche Hochschulwesen und Patentdienstleister für die Wirtschaft

#Stt: Selbstdarstellung

#descr: Mit ihren MP3-Patenten hat die Fraunhofer-Gesellschaft ein Vorbild für relativ anspruchsvolle und zugleich lukrative Softwarepatente geschaffen, durch die der Staat bei der Finanzierung von Forschungsinstituten ein wenig entlastet wird.  Dieses Modell ist zwar nicht unproblematisch und auch nicht ohne weiteres beliebig ausweit- und wiederholbar, aber es ist zu einem Erfolgssymbol der Patentbewegung im Hochschulbereich (s. BMBF) geworden.  Die Fraunhofer-Gesellschaft betreibt zugleich eine zentrale Patentstelle für die deutschen Hochschulen, die eine ähnliche Pilotfunktion ausübt.  Das Fraunhofer-Institut für Innovationsforschung verfasst regelmäßig auf Bestellung des BMBF Gutachten, in denen die unfortschrittliche Methodik der Softwarebranche beklagt und die patentorientierte Fraunhofer-Forschung als Hoffnungsträger dargestellt wird.  Ihre Pilotfunktion in der Hochschul-Patentbewegung verleiht den Fraunhofer-Leuten ein starkes Sendungsbewusstsein.

#title: Die Fraunhofer-Gesellschaft als Bastion der Patentbewegung

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatfhg ;
# txtlang: de ;
# multlin: t ;
# End: ;

