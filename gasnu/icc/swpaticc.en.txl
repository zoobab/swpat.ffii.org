<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: International Chamber of Commerce (ICC) and Software Patents

#descr: ICC's %(q:Intellectual Property Committee), consisting of 240
corporate %(q:IP professionals) from around the world, headed by Urho
Ilmonen, Vice-President Legal of Nokia Mobile Phones Ltd, has
vigorously defended the interests of the patent community in Europe. 
Their letters and statements are characterised by %(q:strong belief)
in the beneficiality of patents and disregard for the opinions not
only of most ICC member companies but also of national member
organisations such as the German Chamber of Commerce, which has
pronounced itself against software patents and against the directive
proposal.

#tos: explains that ICC was founded in 1919 in order to monitor policy
decisions and that it does this by forming commissions:
%(q:Commissions are the bedrock of ICC, composed of a total of more
than 500 business experts who give freely of their time to formulate
ICC policy and elaborate its rules. Commissions scrutinize proposed
international and national government initiatives affecting their
subject areas and prepare business positions for submission to
international organizations and governments.)  The IP Commission alone
counts %(q:240 IP experts).

#Wcp: The commission consisting of 240 IP experts which takes the decisions
on patent policy matters at ICC, headed by Nokia IP lawyer Urho
Ilmonen.  It is founded on firm beliefs and strong alliances: %(bc:The
Commission on Intellectual and Industrial Property brings together
leading experts from all over the world to promote an environment
favorable to for the protection of intellectual property on national,
regional and international levels. It believes that the protection of
intellectual property stimulates international trade, creates a
favorable climate for foreign direct investment, and encourages
innovation and technology transfer.  The ICC works closely with
intergovernmental and non-governmental organisations involved in
intellectual property policy, such as the %(tp|World Intellectual
Property Organisation|WIPO), the %(tp|World Trade Organisation|WTO),
the %(tp|World Customs Organisation|WCO), the %(tp|UN Economic
Commission for Europe|UNECE), the %(tp|International Association for
the Protection of Industrial Property|AIPPI) and the %(tp|Licensing
Executive Society|LES).  Priorities: %(ul|examine the intellectual
property issues arising from electronic commerce|...))

#otr: ICCs plans to push for more %(q:intellectual property protection)
throughout the world

#WOe: demands program claims, codification of current EPO practise and
further efforts to delete Art 52(2) EPC.

#aeC: The ostentatious reason is that utility models are too easy to obtain
and thereby create legal insecurity.  The untold reason is that
utility models would take a way a large chunk of revenue from patent
lawyers.  Patent lawyers make much of their living on the examination
process.  The opposition from patent lawyer committees such as that of
ICC was enough to kill the European Commission's utility model
directive proposal in 2000.  In its reasoning, ICC wrote: %(bc:The ICC
campaign has been directed in particular towards the protection of
innovative technology by patents and similar rights. This is because
ICC firmly believes that companies that do innovate technology should
be able to obtain and enforce quickly, cheaply and without aggravation
the intellectual property rights protecting such technology.  Most
importantly, the potency of the rights so provided should, at the same
time, always be commensurate with the contribution made by the
innovation. Further, a third party wishing to commercialize its own
technology must also be able to determine quickly, cheaply and without
aggravation whether it is free to work that technology as far as
intellectual property rights belonging to competitors are concerned) 
Unfortunately ICC never seems to care about commensurateness in the
case of software patents, whose examination brings fat revenues to the
%(q:IP professionals) on the ICC IP Committee.

#css: Among the members of the German section of ICC are organisation such
as the German Chamber of Commerce as well as many companies who
expressed themselves against software patents and against the CEC
directive, as well as many whose interest in software patentability
are more than questionable.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpaticc ;
# txtlang: en ;
# multlin: t ;
# End: ;

