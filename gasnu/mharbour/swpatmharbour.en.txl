<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Malcolm Harbour MEP and Software Patents

#descr: Malcolm Harbour, Member of the European Parliament, UK Conservative, has been an active and forceful promoter of software and business method patents in Europe, all while pretending that he was %(q:only closing loopholes in the current law so as to avoid US-style broad patentability) and that claims to the contrary came from %(q:misguided lobbyists in the European Parliament).  Harbour vigorously promoted program claims and opposed all amendment proposals, including those approved in CULT and ITRE, which put any limits on what can be patented.  Patent lobbyists have great confidence in %(q:Malcolm).  Some write letters to MEPs telling them to look out for Harbour's amendment proposals and to support them as soon as they come out.  Harbour, until recently an automobile industry manager at Rover, speaks in a very self-confident manner which gives many of his listeners, including MEPs from other countries and other parties, the impression that he is in power and they can rely on him and follow him.

#raW: Hartmut Pilch quotes a letter from Malcolm Harbour.  Harbour says that his aim in pushing for the software patent directive is to halt the drift toward patents on algorithms and business methods, and that the fears of some %(q:misguided lobbyists within the European Parliament) are groundless.  This sounds reassuring, because it shows that Mr. Harbour knows what his constituents are expecting from him.  In reality however Malcolm Harbour has been consistently pushing for unlimited patentability and maximum blocking effects of software patents, as Hartmut points out in a preface.

#Wrr: This is what Malcolm Harbour MEP, under the heading %(q:Conservative MEPs fight Brussels Bureaucracy), tells his home audience in the West Midlands.  In reality Harbour is working in the EP to launch new attacks on innovation and create thousands of new regulations for SMEs to study and observe every year.  Also, while Harbour tells his audience that he wants the European Parliaments legislative processes to be subject to auditing and impact assessments similar to those in the industry, within the European Parliament he consistently opposes demands that patent legislation be measured against a test suite of example patents.

#Wne: lists members of the European Parliament (MEPs) who have taken particular interest in patent matters. Arlene McCarthy, Malcolm Harbour, Diana Wallis and Neil McCormick are marked with an asterisk.

#tbi: Looking at Harbour's contributions to the discussions, it seems that Harbour has difficulty understanding how any process running on a computer (or, his favorite example, a mobile phone) could be unpatentable subject matter.  He combines this lack of understanding with a deep trust for %(q:IP experts) speaking in the name of large companies and industry associations.  A trust which is has been reciprocated and nurtured for a while already.

#opi: In October, Malcolm Harbour showed dismay at the European Parliament's %(rl:decision for real limits on patentability):

#nWa: Parliament Misses the Chance to Close Software Patent Loophole

#bmb: Parliament's approval of the Directive on Computer Implemented Inventions on 24 September should have given a boost to the EU's quest for a 'knowledge driven' economy. The Directive aims to clarify European Patent Law and give examiners clear grounds for refusing patents on software and business methods. However, MEPs voted to approve Socialist, Green and Liberal amendments which greatly complicate the proposal and render it largely unworkable. This is a disappointing result. We wanted to ensure that the EU did not go down the road of the USA in extending patents to computer software and business methods. This would be damaging to our economy and to the crucial IT sector. We now must wait for the Council and Commission to present a workable compromise proposal.

#ftW: This statement was published in the e-mail newsletter of Geoffrey van Orden, Conservative MEP of the Eastern Region. Given that Mr. van Orden doesn't have any particular interest in software and that the statement closely reflects Malcolm Harbour's style and diction, we can be certain that it comes from Malcolm Harbour, the person in charge of the software patent directive on behalf of the UK conservative group in the Parliament.

#Woh2: Industrial Patent Lawyers from Austria tell their MEPs to watch out for Malcolm Harbour's upcoming amendment proposals and follow them, since they are those of %(q:the industry).

#ooe: Harbour vita on tory website

#vny: Harbour vita on website of an automotive industry association

#ncy: Documentation on Malcolm Harbour by FFII UK

#tse: says that Harbour hosted an EURIM meeting on 03-06-11 in EP in Brussels.  EURIM is a british lobby organisation which represents mainly big business

#dor: Contains correspondeance with Harbour and Tory colleagues who mainly copy&paste standard letters by Harbour on software patents

#rot: standard response by Harbour to questions about software patents

#ooc: Harbour speech on E-Commerce etc

#WtW: Malcolm Harbour as a fighter for EU efforts in research

#sri: contains questions by Harbour on Information Society

#tsg: reports about a speech where Harbour presents himself as a fighter against bureaucracy

#ocW: Harbour as a former Rover employee expresses concern about BMW's policy toward its acquired subsidiary.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatmharbour ;
# txtlang: en ;
# multlin: t ;
# End: ;

