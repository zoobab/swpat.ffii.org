<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: A french informaticist and economist, author of a comprehensive study on the problem of software patentability and of a book on free software.  Argues against software patents from an economic and legal point of view, for the latter relying much on Vivant.  His study, made in the name of the Conseil des Mines, is the only one that proposes and weighs alternative approaches to the codification of EPO practise, including a sui generis software law approach.descr: french informaticist and economist, author of a comprehensive study on the problem of software patentability and of a book on free software.  Argues against software patents from an economic and legal point of view, for the latter relying much on Vivant.  His study, made in the name of the Conseil des Mines, is the only one that proposes and weighs alternative approaches to the codification of EPO practise, including a sui generis software law approach.
title: Jean-Paul Smets and Software Patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatsmets ;
# txtlang: en ;
# End: ;

