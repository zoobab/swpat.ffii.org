<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: The Universitarian Patent Movement

#descr: Hundreds of universitarian %(q:technology transfer agencies) are
putting brakes on the diffusion of technology with help of patents. 
The taxpayer finances the creation of monopolised knowledge goods and
then later pays about 10 times as much for the monopoly costs as the
university can hope to gain from patent royalties.  An atmosphere of
tension between researchers and patent bureaucrats is created.  The
macro-economic reasons for the existence of a public research sector
are neglected and a second-rate competition to industrial research is
instead created and alimented by the taxpayer.  Some day in the future
this may save the state some money, says the lobby that gets the newly
created posts in the patent bureaucracy.  This lobby has its own
governmental agencies on its side, of which the best known one in
Germany is the %(q:Patent Offensive) in the Ministery of Education and
Research.

#Zii: The universitarian patent movement is especially served by the
following institutions and initiatives

#cWg: %(q:Patentoffensive) alias %(q:Verwertungsoffensive) im BMBF

#Fnu: Fraunhofer-Patentstelle der Deutschen Forschung

#Ewa: IPR marketing initiative of the Max Planck Society

#iue

#Ahs: Abschaffung von Auflagen, durch welche Universitäte bislang
verpflichtet wurden, ihre Erfindungen der Öffentlichkeit zugute kommen
zu lassen und eventuelle Patente in einigermaßen fairer Weise zu
verwerten

#Eur: Introduction of a novelty grace period

#tai

#Sec: Deutscher Hochschulverband 2002-06-30: Verwertungszwang verschlechtert
Forschungsklima

#Des: In this resolution, the german association of university professors
warns that the universitarian patent offensive will lead to less
bureaucratisation of universities and demotivation of researchers

#tWd

#nms

#a0i: BMBF Patentpolitik 2004-01: Ein Streitgespräch

#eem: Führende Patentpolitiker im Bundesministerium für Bildung und
Forschung führen ein fiktives Streitgespräch, in dem landläufigen
Argumenten naiver Hochschulangehöriger gegen die Expansion des
Patentwesens in den Hochschulbereich und insbesondere gegen den
Richtlinienvorschlag der Europäischen Kommission die Weisheit der
BMBF-Patentexperten gegenübergestellt wird.  Zunächst wird das
Nachahmen von Ideen mit dem Klauen von Wollpullovern gleichgesetzt und
erklärt, es handele sich bei Erfindungen um Eigentum im
naturrechtlichen Sinne, das anerkannt werden müsse.   Im Verlauf einer
Folge aus vielen kurzen Einwänden und langen Belehrungen wird
schließlich gefordert, informatische Erfindungen müssten mit anderen
gleichbehandelt werden, die Rhetorik um %(q:KMU und
Opensource-Software) sei irrational, beim Richtlinienentwurf der
Europäischen Kommission gehe es nur um Erfindungen mit %(q:technischen
Beiträgen), vergleichbar etwa dem Antiblockiersystem, das vor 15
Jahren noch nicht patentierbar gewesen sei aber heute dank legitimer
Rechtsfortbildung patentierbar sei.  Mit diesem Papier nimmt mit
Günter Reiner, der Gründer und Leiter der %(q:Verwertungsoffensive)
(früher %(q:Patentoffensive genannt) des BMBF erstmals ein Vertreter
des BMBF zur Frage der Softwarepatente Stellung.  Bislang kam auf dem
BMBF-Patentserver diesbezüglich nur die Patentabteilung der Firma
Siemens zu Wort.  Den Dialog mit Kritikern hat die
BMBF-Verwertungsoffensive bislang nicht gepflegt, und das neue Papier
ändert hieran nichts.  Es deutert allerdings darauf hin, dass die
%(q:Offensive) allmählich in die Defensive gerät.

#Bet: German Ministery of Science and Education uses Siemens Brochure to
advocate software patents

#Inn: BMBF recommends brochure from the Siemens patent department which
spreads the word that software has been made de facto patentable by
patent jurisdiction, and endorses this policy.

#NiW: Neue Patentinitiativen von Bulmahn

#Kea: Kritischer Bericht

#Hfi: Hauser 2001-11-30: Abschaffung des Hochschullehrerprivilegs richtig,
Weg falsch

#Dii: Der für Forschung zuständige CDU-Abgeordnete Norbert Hauser bläst auch
in Bulmahns Horn.  Das %(q:Hochschullehrerprivileg) sei ein
%(q:Überbleibsel aus der Kaiserzeit).  Der Aufbau einer zeitgemäßen
Hochschul-Patentverwertung nach amerikanischem Vorbild erfordere aber
mehr als die von Bulmahn vorgesehenen 100 Millionen DEM
Anschubsfinanzierung.

#Mrc: Matthias Berninger 2000: Bedeutung der Patentvermarktung in der
Wissensgesellschaft

#Mhu: Matthias Berninger, MdB Grüne und Staatssekretär im
Landwirtschaftsministerium, stimmt in den Chor %(q:gegen das
Hochschullehrerprivileg) und %(q:für den Aufbau einer Patentkultur an
den Hochschulen) mit ein.   Der Patentbewegungs-Kampfbegriff
%(q:Hochschullehrerprivileg) scheint auch grüne Abgeordnete
anzusprechen.  Berninger meint, in der %(q:Wissensgesellschaft) würde
wegen %(q:immer kürzerer Innovationszyklen) %(q:geistiges Eigentum)
und insbesondere Patente immer wichtiger, und daher müsse die
Patentvermarktung zentral von der Hochschule vorangetrieben werden. 
Hiervon verspricht sich Berninger %(q:weniger Geheimforschung),
%(q:besseren Technologietransfer), %(q:Etablierung einer Patentkultur)
und vieles mehr, was die Patentbewegung an Versprechungen bereithält

#deW: danish counterpart of the Bayh-Dole Act and of the German university
patent laws of 2002/02

#aao: apparently written by people at a universitarian patent administration
agency of the University of Columbia, reports very positively about
the effects, fairly informative

#Eel: Evan Brown, an IT service person, mentioned to his employer that he
had recently thought of new ways to convert old programs to new ones. 
The employer asked him to disclose the ideas so the company could
patent them.  Brown refused and was fired.  The company was later
acquired by Alcatel.  Alcatel is continuing to litigate against Brown
for refusing to hand over his idea.  Brown has lost in court and is
faced with lawyer bills of several 100k USD.  On this site he is
asking the public for help.  Since in Europe, too, politicians have
recently been eager to turn people's ideas into %(q:property) of their
employers, this site should give people something to think about.

#Ise: Im Namen des BMBF werden Hochschulen und Unternehmen angeregt, mehr
Patente anzumelden.

#Bct: Bulmahn: Änderung des Hochschullehrerprivilegs soll jetzt zügig
angegenangen werden

#EWq: Ein erhebliches Innovationspotential liege an den deutschen
Hochschulen Brach, erklärt Bulmahn.  Durch Anschubsfinanzierung solle
die systematische Erschließung der wirtschaftlich verwertbaren
Forschungsergebnisse sichergestellt werden.

#adu: american professor of computer science arguing that the Bayh-Dole Act
did at the time seem to address some problems, such as lack of
practicality and proof of concept in university research, but it
created many more, and there is no longer any valid justification for
maintaining this law.

#ich

#EoW: Some answers on how the Bayh-Dole Act is impeding free software at
universities in the US

#Woi: Effects on Universities, macro-economy?

#Bni: So far this whole policy does not seem to be based on any serious
reasoning but rather on the gut feeling of politicians that american
tides are irresistible, that universities are somehow inefficient,
that professors should no longer be %(q:privileged), and that any kind
of market-oriented reform is good.  How good/bad this reform really
is, is an independent question on which we do not have more than
guesses to offer either.  More studies, data, links etc needed. 
Alternative approaches need to be developped and compared.

#FaF: Forderungen an das BMBF und die Forschungspolitik

#SWt: Schützenhilfe für den Rechtsbruch der Patentämter in bezug auf
Software hat zu unterbleiben.  Die Siemens-Broschüre ist vom Netz zu
nehmen und durch eine klare Grundsatzerklärung zu ersetzen. 
Softwarepatente dürfen nicht im Rahmen von BMBF-geförderten Projekten
angemeldet werden.

#Pre: Primärindikator für den Erfolg geförderter Projekte sollen die dabei
erzeugten öffentlichen Informationsgüter (einschließlich Ideen und
Werke) sein.  Im Bereich der Informatik/Software ist dieser Idikator
sogar der alleine ausschlaggebende.  Messen lässt sich der Erfolg z.B.
an Zitier- und Gebrauchshäufigkeit und der Zahl Personen im Inland,
die darauf Fachwissen aufbauen und verwerten.  Ferner ist wichtig, ob
ein Marktversagen/Gefangenendilemma vorlag, welches durch öffentliche
Finanzierung behoben wurde.

#JWh: Jemand muss Frau Bulmahn aufklären.

#Wdz: Wir brauchen einen Forschungsminister (egal welchen Geschlechts), der
seine Ministerialbeamten kritisieren und zur Verantwortung zu ziehen
in der Lage ist.  Das ist die Hauptaufgabe jedes Politikers.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatbmbf ;
# txtlang: xx ;
# End: ;

