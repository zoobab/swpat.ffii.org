<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Die Hochschul-Patentbewegung

#descr: Hunderte von universitären %(q:Technologietransfer-Stellen)
beschäftigen sich hauptamtlich damit, die Verbreitung von Technologien
mithilfe von Patenten zu bremsen.  Der Steuerzahler finanziert die
Erschließung und sofortige Monopolisierung neuer (und alter)
Wissensgüter.  Das BMBF bemisst den Erfolg seiner Investitionen nicht
an den erzeugten öffentlichen Gütern sondern umgekehrt an den der
Öffentlichkeit entzogenen Gütern.  Für die Patentgebühren zahlt der
Steuerzahler später ggf noch einmal 10 mal so viel drauf, wie die
Universität (bei effizienter Führung vielleicht irgendwann einmal)
daran verdient.  In den so geschaffenen Patentfabriken bestimmt eine
staatlich geförderte Verwertungsbürokratie die Richtung der
Forschungspolitik.  Den guten volkswirtschaftlichen Gründen für die
Existenz eines öffentlichen Bildungs- und Forschungssektors wird
offenbar kaum noch Rechnung getragen.  Stattdessen entsteht eine
zweitklassige Konkurrenz zur industriellen Forschung.  Irgendwann in
der Zukunft wird hierdurch vielleicht einmal die Staatskasse
entlastet.  Das versprechen zumindest die
%(q:Technologietransferbeamten), die in dem neuen System die Posten
bekleiden.  Dieser Gruppe stehen in diversen Regierungen auf
regionaler, nationaler und supranationaler Ebene steuerlich
finanzierte Agenturen zur Seite, von denen insbesonere die
%(q:Patentoffensive) im Bundesministerium für Bildung und Forschung
von sich reden macht.

#Zii: Mit der Hochschulpatentbewegung hängen insbesondere folgende
Institutionen und Initiativen zudammen:

#cWg: %(q:Patentoffensive) alias %(q:Verwertungsoffensive) im BMBF

#Fnu: Fraunhofer-Patentstelle der Deutschen Forschung

#Ewa: Eine Patent- und Verwertungsstelle der Max-Planck-Gesellschaft

#iue: Gesetzgebungswünsche der Hochschulpatentbewegung:

#Ahs: Abschaffung von Auflagen, durch welche Universitäte bislang
verpflichtet wurden, ihre Erfindungen der Öffentlichkeit zugute kommen
zu lassen und eventuelle Patente in einigermaßen fairer Weise zu
verwerten

#Eur: Einführung einer Neuheitsschonfrist für Patentanmeldungen

#tai: Alles was das Patentsystem im allgemeinen und die universitären
Patentinstitutionen im besonderen stärkt

#Sec: Deutscher Hochschulverband 2002-06-30: Verwertungszwang verschlechtert
Forschungsklima

#Des: Der Verband der Professoren hält nichts von der Verwertungsoffensive,
befürchtet Bürokratisierung und Demotivierung der Hochschullehrer. 
Der Verband erwähnt ein Gutachten, welches eine Patentanwaltskanzeli
im Auftrag des BMBF anfertigte.  Dieses Gutachten spricht sich gegen
einen Verwertungszwang und für die Beibehaltung des
%(q:Hochschullehrerprivilegs) aus und nennt dafür gute Gründe.

#tWd: Patentverwertung: Adressen und Links

#nms: Sammlung der Arbeitsgemeinschaft Hessischer IHKs

#a0i: BMBF Patentpolitik 2004-01: Ein Streitgespräch

#eem: Führende Patentpolitiker im Bundesministerium für Bildung und
Forschung führen ein fiktives Streitgespräch, in dem landläufigen
Argumenten naiver Hochschulangehöriger gegen die Expansion des
Patentwesens in den Hochschulbereich und insbesondere gegen den
Richtlinienvorschlag der Europäischen Kommission die Weisheit der
BMBF-Patentexperten gegenübergestellt wird.  Zunächst wird das
Nachahmen von Ideen mit dem Klauen von Wollpullovern gleichgesetzt und
erklärt, es handele sich bei Erfindungen um Eigentum im
naturrechtlichen Sinne, das anerkannt werden müsse.   Im Verlauf einer
Folge aus vielen kurzen Einwänden und langen Belehrungen wird
schließlich gefordert, informatische Erfindungen müssten mit anderen
gleichbehandelt werden, die Rhetorik um %(q:KMU und
Opensource-Software) sei irrational, beim Richtlinienentwurf der
Europäischen Kommission gehe es nur um Erfindungen mit %(q:technischen
Beiträgen), vergleichbar etwa dem Antiblockiersystem, das vor 15
Jahren noch nicht patentierbar gewesen sei aber heute dank legitimer
Rechtsfortbildung patentierbar sei.  Mit diesem Papier nimmt mit
Günter Reiner, der Gründer und Leiter der %(q:Verwertungsoffensive)
(früher %(q:Patentoffensive genannt) des BMBF erstmals ein Vertreter
des BMBF zur Frage der Softwarepatente Stellung.  Bislang kam auf dem
BMBF-Patentserver diesbezüglich nur die Patentabteilung der Firma
Siemens zu Wort.  Den Dialog mit Kritikern hat die
BMBF-Verwertungsoffensive bislang nicht gepflegt, und das neue Papier
ändert hieran nichts.  Es deutert allerdings darauf hin, dass die
%(q:Offensive) allmählich in die Defensive gerät.

#Bet: BMBF bietet Softwarepatent-Broschüre der Patentabteilung von Siemens
an

#Inn: Im Namen des BMBF plädiert die Patentabteilung von Siemens für die
Patentierbarkeit von Software und regt Hochschulen an, Softwarepatente
anzumelden.

#NiW: Neue Patentinitiativen von Bulmahn

#Kea: Kritischer Bericht

#Hfi: Hauser 2001-11-30: Abschaffung des Hochschullehrerprivilegs richtig,
Weg falsch

#Dii: Der für Forschung zuständige CDU-Abgeordnete Norbert Hauser bläst auch
in Bulmahns Horn.  Das %(q:Hochschullehrerprivileg) sei ein
%(q:Überbleibsel aus der Kaiserzeit).  Der Aufbau einer zeitgemäßen
Hochschul-Patentverwertung nach amerikanischem Vorbild erfordere aber
mehr als die von Bulmahn vorgesehenen 100 Millionen DEM
Anschubsfinanzierung.

#Mrc: Matthias Berninger 2000: Bedeutung der Patentvermarktung in der
Wissensgesellschaft

#Mhu: Matthias Berninger, MdB Grüne und Staatssekretär im
Landwirtschaftsministerium, stimmt in den Chor %(q:gegen das
Hochschullehrerprivileg) und %(q:für den Aufbau einer Patentkultur an
den Hochschulen) mit ein.   Der Patentbewegungs-Kampfbegriff
%(q:Hochschullehrerprivileg) scheint auch grüne Abgeordnete
anzusprechen.  Berninger meint, in der %(q:Wissensgesellschaft) würde
wegen %(q:immer kürzerer Innovationszyklen) %(q:geistiges Eigentum)
und insbesondere Patente immer wichtiger, und daher müsse die
Patentvermarktung zentral von der Hochschule vorangetrieben werden. 
Hiervon verspricht sich Berninger %(q:weniger Geheimforschung),
%(q:besseren Technologietransfer), %(q:Etablierung einer Patentkultur)
und vieles mehr, was die Patentbewegung an Verheißungen bereithält.

#deW: dänisches Gegenstück des Bayh-Doleschen Gesetzes und der deutschen
Hochschulpatentgesetze von 2002/2

#aao: apparently written by people at a universitarian patent administration
agency of the University of Columbia, reports very positively about
the effects, fairly informative

#Eel: Evan Brown, an IT service person, mentioned to his employer that he
had recently thought of new ways to convert old programs to new ones. 
The employer asked him to disclose the ideas so the company could
patent them.  Brown refused and was fired.  The company was later
acquired by Alcatel.  Alcatel is continuing to litigate against Brown
for refusing to hand over his idea.  Brown has lost in court and is
faced with lawyer bills of several 100k USD.  On this site he is
asking the public for help.  Since in Europe, too, politicians have
recently been eager to turn people's ideas into %(q:property) of their
employers, this site should give people something to think about.

#Ise: BMBF patent server

#Bct: Bulmahn: Änderung des Hochschullehrerprivilegs soll jetzt zügig
angegenangen werden

#EWq: Ein erhebliches Innovationspotential liege an den deutschen
Hochschulen Brach, erklärt Bulmahn.  Durch Anschubsfinanzierung solle
die systematische Erschließung der wirtschaftlich verwertbaren
Forschungsergebnisse sichergestellt werden.

#adu: american professor of computer science arguing that the Bayh-Dole Act
did at the time seem to address some problems, such as lack of
practicality and proof of concept in university research, but it
created many more, and there is no longer any valid justification for
maintaining this law.

#ich: Openinformatics on Bayh-Dole

#EoW: Einige Infos über Behinderung freier Software durch das
Hochschulpatentwesen gemäß Bayh-Doleschem Gesetz in den USA.

#Woi: Wirkung auf die Hochschulen, auf die Volkswirtschaft?

#Bni: Bislang stellen wir nur fest, dass die Argumente der
Hochschulpatentbewegung nicht überzeugen und dass die Politiker wohl
ohne richtige Überlegungen anzustellen mit dem Strom geschwommen sind,
der von Amerika kam.  Andererseits ist klar, dass die real
existierende Patentinflation auch die Hochschulen vor gewisse
Herausforderungen stellt, auf die eine organisatorische Antwort in der
einen oder anderen Form zu finden ist.  Es käme darauf an, hier
alternative Konzepte zu entwickeln und vergleichende Kostenrechnungen
auf allen Ebenen anzustellen.  Verweise auf vorhandene Überlegungen
wären auch interessant.

#FaF: Forderungen an das BMBF und die Forschungspolitik

#SWt: Schützenhilfe für den Rechtsbruch der Patentämter in bezug auf
Software hat zu unterbleiben.  Die Siemens-Broschüre ist vom Netz zu
nehmen und durch eine klare Grundsatzerklärung zu ersetzen. 
Softwarepatente dürfen nicht im Rahmen von BMBF-geförderten Projekten
angemeldet werden.

#Pre: Primärindikator für den Erfolg geförderter Projekte sollen die dabei
erzeugten öffentlichen Informationsgüter (einschließlich Ideen und
Werke) sein.  Im Bereich der Informatik/Software ist dieser Idikator
sogar der alleine ausschlaggebende.  Messen lässt sich der Erfolg z.B.
an Zitier- und Gebrauchshäufigkeit und der Zahl Personen im Inland,
die darauf Fachwissen aufbauen und verwerten.  Ferner ist wichtig, ob
ein Marktversagen/Gefangenendilemma vorlag, welches durch öffentliche
Finanzierung behoben wurde.

#JWh: Jemand muss Frau Bulmahn aufklären.

#Wdz: Wir brauchen Minister, die ihre Ministerialbeamten zu kritisieren und
zur Verantwortung zu ziehen in der Lage sind.  Leider sind diese sehr
rar.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@ffii.org ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: swpatbmbf ;
# txtlang: xx ;
# End: ;

