\begin{subdocument}{swnbmbf022}{German Universitarian Patent Law Decided}{http://swpat.ffii.org/players/bmbf/n022/index.en.html}{Workgroup\\swpatag@ffii.org}{A new German Law authorises and encourages universities to patent the results of their employees's research and to oblige their employees to cooperate in this endeavor.  The economic considerations behind this law seem questionable not only because it creates dangers for the often very successful cooperative development of software and certain technologies in the university sector.  A public discussion with the media and science expert of the Federal Parliament, J\"{o}rg Tauss, did not help to diminish our concerns.  At least a law to guarantee the freedom of software and cooperatively developped technologies will become necessary if software patents are legalised.}
\begin{sect}{force}{Ministry: Enforcing Valorisation}
In April 2001 we warned\footnote{http://swpat.ffii.org/players/bmbf/n014/index.de.html} against plans for a university patent law.  These warnings did not get through to the decisionmakers in the German Federal Parliament.  In a press release\footnote{http://www.bmbf.de/presse01/561.html} of yesterday, the Ministery of Research announced:

\begin{quote}
This thursday a change in the employee inventor's law will go in force.  Universities thereby acquire the right to patent inventions of their female and male employees and thereby enforce the economic valorisation of these inventions.
\end{quote}

The Ministery has also been propagating\footnote{http://www.patente.bmbf.de/inform/down/software.pdf} software patentability.  Taken together this means that university software is, before any permission of publication on the Net, first to be assessed for its patentability, and then either to be classed as worthless or to be forced onto a path of proprietary commercialisation.
\end{sect}

\begin{sect}{links}{Annotated Links}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Universitarian Patent Movement planning new laws\footnote{http://swpat.ffii.org/players/bmbf/n014/index.de.html}}}

\begin{quote}
By means of a ``Law on the Promotion of the Patent System in Institutions of Higher Learning'' (short: Patent Factory Law PFL) and a ``Law for Changing the Law of Employees' Inventions'' (short: Research Serf Law RSL), the right of university teachers to make their findings freely available to the public according to the ethics of Benjamin Franklin will be completely removed.  The university teacher will be obliged to inform his university's patent administration about anything that could be patentable, before he writes about it.  The patent administration, which is to be built by generous state funding in the next few years, will then dispose of all ``intellectual property''.  The consent of the professors to this loss of freedom and dignity is to be bought by a share of 30\percent{} in the revenues.
\end{quote}
\filbreak

\item
{\bf {\bf phm: Hochschulpatentgesetz verabschiedet\footnote{http://lists.ffii.org/archive/mails/neues/2002/Feb/0006.html}}}
\filbreak

\item
{\bf {\bf Tauss: Re: Hochschulpatentgesetz verabschiedet\footnote{}}}
\filbreak

\item
{\bf {\bf phm: Re: Hochschulpatentgesetz verabschiedet\footnote{http://lists.ffii.org/archive/mails/neues/2002/Feb/0008.html}}}
\filbreak

\item
{\bf {\bf A. Stohr: Re: Hochschulpatentgesetz verabschiedet\footnote{http://lists.ffii.org/archive/mails/offen/2002/Feb/0019.html}}}

\begin{quote}
extract
``\begin{quote}
... Damit wird der ganze wissenschaftliche Bereich in eine Spannungszone verwandelt, die irgendwo zwischen der Aufdringlichkeit eines Versicherungsvertreters, der Hartn\"{a}ckigkeit eines GEZ-Vertragspartners und dem Erfolgsdruck eines Vertragshandy-Verk\"{a}ufers angesiedelt sein k\"{o}nnte. ...  Der Zweck der Anderstartigkeit der staatlichen Forschung wird immer weiter ignoriert und folglich wird auch diese Forschungsvariante selbst immer weiter erodieren, zu unser aller Nachteil in dieser Volkswirtschaft.
\end{quote}''
\end{quote}
\filbreak

\item
{\bf {\bf Edelgard Bulmahn und ihre Patentrezepte f\"{u}r die deutschen Hochschulen\footnote{http://swpat.ffii.org/players/bulmahn/index.de.html}}}

\begin{quote}
W\"{a}hrend ihrer ersten Amtszeit 1998-2002 brachte die junge SPD-Forschungsministerin einen Gesetzesentwurf durch den Bundestag, der die Hochschullehrern zum Patentieren verpflichtet.  In zahlreichen Reden der Ministerin taucht das Wort ``Patente'' in rosaroter F\"{a}rbung auf.  Das Patentsystem ist f\"{u}r Frau Bulmahn offensichtlich ein Hoffnungstr\"{a}ger.  Es k\"{o}nnte erlauben, die kostspielige und sperrige \"{o}ffentliche Forschung abzusch\"{u}tteln (oder wenigstens durchzusch\"{u}tteln) und den Staatshaushalt zu entlasten.  Die Entw\"{u}rfe dazu werden Bulmahn von den wohletablierten Patentfunktion\"{a}ren ihres Ministeriums auf den Tisch gelegt, aber die Ministerin legt bei ihrer Abarbeitung eine auff\"{a}llig euphorische Haltung an den Tag.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: latex ;
% End: ;

