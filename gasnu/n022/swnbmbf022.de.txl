<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: 2002-02-07: Neues deutsches Gesetz forciert patentrechtliche Verwertung der Hochschulforschung

#descr: Ein neues deutsches Gesetz ermächtigt die Universitäten, die Forschungsergebnisse ihres Personals patentrechtlich zu verwerten und dessen Kooperation hierbei zu erzwingen.  Die volkswirtschaftlichen Überlegungen hinter diesem Gesetz sind nicht nur deshalb fraglich, weil es für die universitäre Kultur der Quellenoffenheit Hürden und Gefahren schafft.  Eine öffentlicher Meinungsaustausch mit dem Bundestags-Medienexperten Jörg Tauss, der auch für den Bereich Bildung und Forschung zuständig ist, trug wenig dazu bei, unsere Befürchtungen zu zerstreuen.  Ein schadenbegrenzendes Gesetz für die freie Software and den Hochschulen dürfte notwendig werden, wenn Softwarepatente legalisiert werden.

#BrW: BMBF: Verwertung forciert

#WWi: Wir %(bf:warnten) im April 2001 vor dem geplanten Hochschulpatentgesetzgebung.  Die Warnungen drangen nicht bis zu den zuständigen Bundestagsabgeordneten durch.  In einer %(pe:Presseerklärung) des Bundesforschungsministeriums (BMBF) heißt es nunmehr:

#AlM: Am heutigen Donnerstag tritt eine Änderung des Arbeitnehmererfindungsgesetzes in Kraft. Damit werden Hochschulen das Recht erhalten, Erfindungen ihrer Mitarbeiterinnen und Mitarbeiter zum Patent anzumelden und damit die wirtschaftliche Verwertung zu forcieren.

#DWf: Das BMBF %(pr:propagiert) ebenfalls seit einigen Jahren die Patentierbarkeit von Software.  Zusammen genommen bedeutet dies u.a., dass im Rahmen der Hochschulforschung erstellte Software vor einer eventuellen Veröffentlichung im Internet zunächst von der Patentverwaltung auf patentierbare Ideen hin untersucht wird und dann entweder als wertlos beurteilt oder einer proprietären Zwangsverwertung zugeführt wird.

#Hee: phm: Hochschulpatentgesetz verabschiedet

#Tst: Tauss: Re: Hochschulpatentgesetz verabschiedet

#phe: phm: Re: Hochschulpatentgesetz verabschiedet

#Asu: Auszug

#Egl: ... Damit wird der ganze wissenschaftliche Bereich in eine Spannungszone verwandelt, die irgendwo zwischen der Aufdringlichkeit eines Versicherungsvertreters, der Hartnäckigkeit eines GEZ-Vertragspartners und dem Erfolgsdruck eines Vertragshandy-Verkäufers angesiedelt sein könnte. ...  Der Zweck der Anderstartigkeit der staatlichen Forschung wird immer weiter ignoriert und folglich wird auch diese Forschungsvariante selbst immer weiter erodieren, zu unser aller Nachteil in dieser Volkswirtschaft.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnbmbf022 ;
# txtlang: de ;
# multlin: t ;
# End: ;

