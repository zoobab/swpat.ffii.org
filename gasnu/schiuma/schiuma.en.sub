\begin{subdocument}{swpatschiuma}{Daniele Schiuma and Software Patents}{http://swpat.ffii.org/players/schiuma/index.en.html}{Workgroup\\swpatag@ffii.org}{patent attorney and patent law scholar at the MPI, together with Nack and Straus, argues that ``new technologies'' (usually citing some very impressive ultra-advanced examples) are unprotected without software patents, that patents should be available for anything that is at the forefront of R\&D, that the constitutional right to private property extends to such things, and especially that art 27 TRIPS requires the deletion of the computer programs exclusion from art 52 EPC, no matter how that exclusion is interpreted.  This is also called the ``autonomous interpretation of TRIPS'', a theory which is tied to Schiuma's name.}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Bronwyn H. Hall \& Rose Marie Ham: The Patent Paradox Revisited\footnote{http://swpat.ffii.org/papers/iic-schiuma00/index.en.html}}}
\filbreak

\item
{\bf {\bf Daniele Schiuma zu Softwarepatenten: Beitrag zur Bundestags-Anh\"{o}rung 2001-06-21\footnote{http://swpat.ffii.org/events/2001/bundestag/schiuma/index.de.html}}}

\begin{quote}
Der M\"{u}nchener Patentanwalt Dr. Daniele Schiuma hat als Forscher am M\"{u}nchenr Max-Planck-Institut f\"{u}r Internationales Patent-, Urheber- und Wettbewerbsrecht mehrere in den einflussreichen Zeitschriften ebendieses Institutes zum Thema Softwarepatente ver\"{o}ffentlicht.  Er ist insbesondere als Vertreter der These bekannt geworden, der TRIPS-Vertrag erzwinge die Patentierbarkeit von Software, da er \emph{autonom interpretiert} werden m\"{u}sse.  Damit ist gemeint, dass bei der Interpretation dieses Vertrages kein spezifisch europ\"{a}ischer Technik- und Erfindungsbegriff zugrunde gelegt werden d\"{u}rfe.  Ferner vertritt Schiuma die These, der Eigentumsbegriff des Grundgesetzes mache Softwarepatente erforderlich.  Wir warten noch auf Zusendung von Herrn Schiumas Referatstext zwecks Ver\"{o}ffentlichung auf dieser Seite.
\end{quote}
\filbreak

\item
{\bf {\bf The TRIPS Treaty and Software Patents\footnote{http://swpat.ffii.org/analysis/trips/index.en.html}}}

\begin{quote}
European patent authorities often cite the TRIPS treaty as a reason for making computer programs patentable.  This reasoning is fallacious and easy to refute.  Here you find everything you need to know.
\end{quote}
\filbreak

\item
{\bf {\bf Patent Lobbyism and Scripture Erudition in the name of Max Planck\footnote{http://swpat.ffii.org/players/mpi/index.de.html}}}

\begin{quote}
Das Max-Planck-Institut f\"{u}r Internationales Patent-, Urheber- und Wettbewerbsrecht in M\"{u}nchen bet\"{a}tigt sich seit jahren als rechtspolitischer Wegbereiter des Privatbesitzes an allen Geisteserzeugnissen, die sich irgendwie beanspruchen und verwerten lassen.  Insbesondere der Lehrstuhl f\"{u}r Gewerbliche Schutzrechte ist eng mit EPA, AIPPI, Gro{\ss}unternehmen und allen Schaltstellen der Patentbewegung in der Bundesregierung, der Europ\"{a}ischen Kommission und bei den Vereinten Nationen verflochten.  Ratschl\"{u}sse des Europ\"{a}ischen Patentamtes und seiner Freunde sind f\"{u}r Prof. Straus und Kollegen ein Heiliger Gral, der \"{u}ber dem Gesetz und \"{u}ber der Wirklichkeit steht.  Die unersch\"{o}pflichen Weisheiten des Patentrechtsprechung gilt es, wie Plancks Kollege Rutherford zu sagen pflegte, briefmarkensammlerisch aufzubereiten, aber niemals im Geiste der Wissenschaft an ihren Wirkungen zu messen.  Es gibt jedoch einen mehr oder weniger gut versteckten Ma{\ss}stab, an dem das MPI seine Schriftauslegungsk\"{u}nste orientiert: die W\"{u}nsche der ``Wirtschaft'', d.h. der Kommilitonen und Freunde aus den Gro{\ss}konzern-Patentabteilungen, denen das Institut direkt oder indirekt sein Wohlergehen verdankt.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
% mode: latex ;
% End: ;

