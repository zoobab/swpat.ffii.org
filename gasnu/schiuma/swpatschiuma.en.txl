<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Daniele Schiuma and Software Patents

#descr: patent attorney and patent law scholar at the MPI, together with Nack and Straus, argues that %(q:new technologies) (usually citing some very impressive ultra-advanced examples) are unprotected without software patents, that patents should be available for anything that is at the forefront of R&D, that the constitutional right to private property extends to such things, and especially that art 27 TRIPS requires the deletion of the computer programs exclusion from art 52 EPC, no matter how that exclusion is interpreted.  This is also called the %(q:autonomous interpretation of TRIPS), a theory which is tied to Schiuma's name.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatschiuma ;
# txtlang: en ;
# multlin: t ;
# End: ;

