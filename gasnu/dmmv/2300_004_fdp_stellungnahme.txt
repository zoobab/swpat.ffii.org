Deutscher Multimedia Verband e.V.  
z.H. Herrn Alexander Felsenberg 
Kaistra�e 14 
 40221 D�sseldorf 
       dmmv-Initiative  St�rkung der digitalen Wirtschaft" 
   Sehr geehrter Herr Wiedmann, 
sehr geehrter Herr Felsenberg, 
sehr geehrter Herr Dr. Korff, 
sehr geehrte Board-Mitglieder und Mitglieder des dmmv, 
 
mit gro�em Interesse haben wir den Punkteplan des dmmv zur St�rkung der digitalen 
Wirtschaft gelesen. Themenauswahl, Fundiertheit und Darstellung haben uns in 
unserer Wahrnehmung best�rkt, dass der dmmv einer der zentralen 
Gespr�chspartner ist, wenn es um die Zukunft der digitalen Wirtschaft in Deutschland 
geht.  
Die FDP hat als die liberale Partei einen starken Focus auf eine ordnungspolitisch klar 
strukturierte und wachstumsorientierte Wirtschaftspolitik. Dabei steht die FDP ein f�r 
das intellektuelle Eigentum und seinen Schutz. Die digitale Wirtschaft wird auch in den 
n�chsten Jahren einer der zentralen Wachstumsmotoren bleiben. 
Die im Punkteplan aufgef�hrten Aspekte enthalten viele wertvolle Informationen, die 
wir bei politischen Entscheidungen in der Zukunft im Blick behalten m�chten. Im 
Einzelnen m�chten wir hierzu folgende Statements abgeben: 

1.         Digitale G�ter sind ein wichtiges Wirtschaftsobjekt. Effektiver Schutz ist daher 
           genauso wichtig wie Motivation  der Unternehmer und Absicherung von 
           Investitionen. Ein effektiver Schutz f�r die Produzenten digitaler Werke ist 
           hierf�r nicht nur w�nschenswert, sondern notwendig. Die FDP zieht die 
           individuelle Verg�tung f�r die Nutzung urheberrechtlich gesch�tzter 
           Leistungen der Pauschalabgabe vor. 
           Wir empfehlen dem dmmv, seine Forderung zum Leistungsschutzrecht f�r 
           Produzenten digitaler Werke in der konkreten Auspr�gung eines 
           Gesetzestextes in das aktuell laufende Gesetzgebungsverfahren zum 
           Urheberrecht einzubringen. Besonders wichtig erscheint uns dabei die 
           Definition des  digitalen Werks", um der in einigen Bev�lkerungsteilen 
           vorhandenen Sorge vor zu starker Monopolisierung entgegenzuwirken. Soweit 
           der Schutz f�r die Produzenten digitaler Werke mehr als nur Klarstellung der 



                                         2 



       aktuellen Rechtslage ist, sollte man sich auch Gedanken dar�ber machen, 
       welcher Ausgleich f�r weitergehende Monopolisierung geschaffen werden 
       kann. Digitale Leistungen zeichnen sich auch dadurch aus, dass sie auf einer 
       vorhandenen Entwicklung aufsetzen und leichter fortentwickelt werden 
       k�nnen. Zum gr��tm�glichen Nutzen der Allgemeinheit k�nnte daher z.B. als 
       Ausgleich f�r zus�tzliche Rechte ein Kontrahierungszwang oder eine Aufgabe 
       f�r Verwertungsgesellschaften vorgesehen werden. 

2.     Zwischen dem urheberrechtlichen Schutz und dem Patentschutz f�r digitale 
       G�ter besteht eine Wechselwirkung. Wir sind gegen eine Patentierbarkeit von 
       Gesch�ftsmodellen und Ideen, anders als dies beispielsweise in den USA der 
       Fall ist. Patente sollten nur dort erteilt werden, wo die Patentierbarkeit den 
       technischen Fortschritt mehr f�rdert als ihn blockiert. Deshalb sollten blo�e 
       Gesch�ftsmodelle in Europa nicht patentierbar werden. Davon geht auch der 
       EU-Richtlinienentwurf zu den Softwarepatenten aus. Im �brigen ist noch 
       sorgf�ltig zu pr�fen, inwieweit dieser Richtlinienentwurf geeignet ist, 
       sch�dliche Patente zu verhindern und n�tzliche zu erm�glichen. 
       Der Patentschutz f�r Software wurde im Bundestag in einer Anh�rung schon 
       im Sommer 2001 diskutiert. Nach Vorliegen des Richtlinienvorschlags der 
       Europ�ischen Kommission ist jetzt ein guter Zeitpunkt, dieses Thema wieder 
       verst�rkt aufzugreifen. Wir m�chten daher gerne zusammen mit dem dmmv 
       den zust�ndigen Kommissar Frits Bolkestein ansprechen, um uns den 
       Richtlinienentwurf vorstellen zu lassen und dessen Inhalte kontrovers zu 
       diskutieren. 



                                          3 



3.     Wer den Schutz geistigen Eigentums ernst meint, will Piraterie auch 
       konsequent verfolgen. Die Organisation der Ermittlungsbeh�rden ist aber 
       L�ndersache und muss dezentral angegangen werden. Wir w�rden jedoch 
       Schulungs- und Informationskampagnen bef�rworten, die helfen, Piraterie 
       fl�chendeckend als bedeutsame Wirtschaftsstraftat zu erkennen und zu 
       �chten. 

4.     Wir sind uns im Klaren, dass die Kapitalausstattung junger Unternehmen im 
       digitalen Umfeld angesichts der aktuellen Marktsituation schwierig ist. Die 
       FDP m�chte gerne dabei helfen, hier positive Signale zu setzen. Die 
       M�glichkeit, digitale Produkte auch dann zu aktivieren, wenn sie selbst 
       entwickelt worden sind, scheint bilanzierungstechnisch und  -rechtlich ein 
       bedenkenswerter Ansatz zu sein. Wir w�rden uns freuen, wenn der dmmv uns 
       mit ein oder zwei Beispielen aus der unternehmerischen Praxis unterst�tzen 
       k�nnte, die praktische Relevanz aufzuzeigen. Eine �nderung von 
       Bilanzierungsvorschriften muss aber auch im europ�ischem Umfeld 
       beleuchtet werden. Vor einem nationalen Vorgehen w�ren daher 
       L�sungsans�tze europaweit zu pr�fen. 

5.     Die Flexibilisierung von Besch�ftigung und Weiterbildung ist f�r uns ein 
       wichtiges Thema. Nach dem B�rgerprogramm 2002 der FDP  soll z.B. das 
       K�ndigungsschutzgesetz erst ab der Betriebsgr��e von 20 Mitarbeitern gelten 
       und erst zwei Jahre nach Beginn des Arbeitsverh�ltnisses einsetzen.  
       Bildung und Bildungspolitik sind zentrale Standort- und Wirtschaftsfaktoren. 
       Hierzu geh�rt selbstverst�ndlich eine effektive betriebliche Weiterbildung. 
       Im B�rgerprogramm 2002 fordert daher die FDP lebenslanges Lernen sowohl 
       durch Anreize als auch durch Eigenengagement zu f�rdern. Langfristig sehen 
       wir dies unabh�ngig von der steuerlichen Struktur. Nach unseren Pl�nen soll 
       das Steuersystem m�glichst einfach durch drei Steuerklassen (15 %, 25 % 
       und 35 %) ausgestaltet sein, was auf der Einnahmenseite  voraussetzt, dass 
       Sondertatbest�nde abgeschafft werden.  
       Unser Bekenntnis zu Aus- und Weiterbildung bedeutet auch, dass wir die 
       Schaffung neuer Berufsbilder im Bereich Multimedia begr��en und f�rdern 
       wollen. In einer neuen Bundesregierung unter Beteiligung der FDP wird daher 
       der Gedanke der Schaffung einer Multimedia-Weiterbildungsordnung 
       sicherlich positiv aufgegriffen werden k�nnen. 



                                                   4 



6.       Derzeit bestehen zu viele einzelne F�rdert�pfe, die zu einer 
         Un�bersichtlichkeit f�hren. Wir wollen dieses Problem an der Wurzel packen 
         und die �bersichtlichkeit der F�rderm�glichkeiten st�rken. Wir sind der 
         Auffassung, dass dann keine zus�tzlichen staatlichen Stellen zur Beratung bei 
         Existenzgr�ndung und F�rderung notwendig sind. Vielmehr gibt es Angebote 
         von privatwirtschaftlichen Founding-Beratern, die dieses Feld kompetent 
         abdecken. Der wirtschaftliche Wettbewerb und die kreative Bet�tigung 
         Privater sind die bessere Alternative. Gerne vertiefen wir die Diskussion 
         nochmals anhand der von Ihnen vorgestellten Struktur, Wirkungsweise und 
         Erfolge von Systemen im benachbarten Ausland. 

7.       Der Kampf gegen Korruption ist ein wichtiges Thema f�r die FDP. Mittel zur 
         Verhinderung von Korruption ist Transparenz. Von daher finden wir ihre 
         Forderung zur Schaffung eines umfangreichen Ausschreibungsportals ebenso 
         naheliegend wie �berzeugend. Wir k�nnten uns vorstellen, dass eine neue 
         Regierung �ber einen prominenten Bef�rworter, wie beispielsweise den 
         Wirtschaftsminister, das Zugpferd erh�lt, um viele weitere Beh�rden, L�nder 
         und Kommunen f�r ein umfassendes und einheitliches Ausschreibungsportal 
         zu gewinnen. 
 
Wir freuen uns darauf, an diesen Themen gemeinsam mit dem  dmmv weiter zu 
arbeiten. 
 
Mit freundlichen Gr��en 
 
 
Hans-Joachim Otto, MdB                                        Rainer Br�derle, MdB 
kultur- und medienpolitischer                                 stellvertretender Bundes- und 
Sprecher der FDP- Bundestagsfraktion                          Fraktionsvorsitzender der FDP 
 



