 
Gr�ne und die Herausforderung Informationsgesellschaft 
  
Das Internet ver�ndert die Gesellschaft in allen Bereichen. Neue Besch�ftigungsfelder und 
neue M�glichkeiten demokratischer Beteiligung entstehen.  
Niemand darf jedoch auf dem Weg in die Informationsgesellschaft auf der Strecke bleiben. 
Die Eckpfeiler f�r den gr�nen Weg in das Internetzeitalter sind die  
Informationsgerechtigkeit und die Sicherung der B�rger- und Verbraucherrechte.  
Der Umgang mit den Neuen Medien muss fester Bestandteil der Aus- und Fortbildung 
werden und der Zugang durch preiswertere Geb�hren und �ffentliche Terminals f�r alle 
erleichtert werden.  
Dabei m�ssen Grundrechte wie der Daten- und Verbraucherschutz gew�hrleistet werden. 
Dies ist auch die Voraussetzung f�r eine schnelle Entwicklung des E-Commerce. Die rot-
gr�ne Bundesregierung hat daher z.B. mit der Novellierung des 
Teledienstedatenschutzgesetzes und des Fernabsatzgesetzes f�r Rechtssicherheit bei 
den Unternehmen und f�r mehr Schutz bei den Verbrauchern gesorgt.  
Um mehr Transparenz und Sicherheit in der Infrastruktur von Computersystemen zu 
erreichen, unterst�tzen B�ndnis 90/Die Gr�nen die Open Source Bewegung, die vor allem 
von einzelnen Entwicklern und von kleinen und mittleren Unternehmen getragen wird.  
Wissen wird in der Informationsgesellschaft zur zentralen Produktivkraft. Die schnelle und 
flexible Probleml�sung wird zum entscheidenden Wettbewerbsfaktor. Damit einher gehen 
dramatische Ver�nderungen von Arbeit und Unternehmenskultur. Trotz des Crashes am 
Neue Markt sehen wir auf die Dauer gro�e Perspektiven f�r Unternehmen, die E- 
Commerce-Angebote machen. 
  Vier-Punkte-Plan 
 
PUNKT I: EFFEKTIVER SCHUTZ DIGITALER G�TER  
  In diesem Zusammenhang wird sicherlich die Individuallizenz in Verbindung mit Digital-
Rights-Management (DRM) ein wichtiger Baustein zuk�nftiger Verg�tungsregelungen im 
digitalen Zeitalter sein � aber auch nur  ein Baustein" und keineswegs  der Baustein".  
 Denn die digitale Vielfalt, die wir anstreben, erfordert keine Patent- oder 
Pauschall�sungen, sondern umfassende Konzepte  - digitale Vielfalt bedeutet auch 
vielf�ltige Verg�tung  � neben Verwertungsgesellschaften f�r das Internet wird es auch 
individuelle Angebotsmodelle geben. Au�erdem muss im Sinne des Verbraucherschutzes 
beim legalen Erwerb von Lizenzen das Recht auf Privatkopie gewahrt bleiben. 
 Nat�rlich kann man in der Hoffnung, dass die Internetnutzerinnen und -nutzer f�r mein 
Werk per Einzelabrechnung bezahlen, dieses entsprechend technisch gesch�tzt in das 
Netz stellen. Genauso muss es aber m�glich sein, �ber Verwertungsgesellschaften f�r 
verbreitete Texte entlohnt zu werden oder diese frei ins Netz zu stellen.  
 Das digitale Zeitalter hat bereits zahlreiche K�nstler, Autoren und Wissenschaftler 
hervorgebracht, die ihre Reputation und ihr Einkommen der Tatsache zu verdanken 
                                                                                            1



haben, dass sie von Anfang an Neue Medien wie das Internet offensiv zum Dialog und 
Wissenstransfer genutzt haben, indem Sie Ihr Wissen im Netz zur Verf�gung gestellt 
haben.  
 Wissen l�sst sich nun einmal nicht einsperren und schon gar nicht im Internet. Das Netz 
l�sst sich nicht nach traditionellen Regelungen kontrollieren, und es wird keine technische 
L�sung geben, die 100%ige Sicherheit f�r den Schutz digitaler G�ter garantiert. 
 Geradezu kontraproduktiv f�r die globale Wissensgesellschaft ist es jedoch, wenn digitale 
Vervielf�ltigungen l�ckenlos durch den Einsatz von sogenannten  Digital Rights 
Management Systemen" kontrolliert und lizenziert werden sollen. 
 Wenn wir das Netz nur in diese  technische Einbahnstra�e" pressen w�rden, dann w�rde 
die digitale Spaltung der Gesellschaft erheblich vorangetrieben werden. 
Wissen zu erwerben darf nicht generell mit Kosten verbunden sein. Die Qualit�t des 
Netzes besteht geradezu in der breiten Verf�gbarkeit verschiedenster Inhalte  � eine 
vollkommene Kommerzialisierung des Netzes muss aus Sicht von B�ndnis 90/Die Gr�nen 
unbedingt verhindert werden. 
Wir d�rfen den Internetnutzerinnen und  -nutzern nicht vorschreiben, zu welchen 
technischen L�sungen sie zu greifen haben  � es werden im Netz von selbst neue 
Verkaufs- und Vertriebsmodelle � zur Finanzierung von Urhebern und Content-Inhabern - 
entstehen, ohne dass wir diese vorgeben oder bereits kennen!  
In diesen Mikro�konomien wird Platz f�r verschiedenste Abrechnungsmodelle und Formen 
von Wissenstransfer sein � die wir nicht durch vorgeschriebene technische Mittel b�ndigen 
und kontrollieren d�rfen. 
Und einmal weitergedacht: Da die technische Entwicklung zum heutigen Zeitpunkt 
un�bersehbar ist, muss Politik die Rahmenbedingungen so gestalten, dass das 
Urheberrecht jederzeit flexibel angepasst oder �berarbeitet werden kann. Im vorliegenden 
Gesetzentwurf der Bundesregierung  Zur Regelung des Urheberrechts in der 
Informationsgesellschaft" bem�ngeln wir vor allem eine unklare Regelung des  Rechts auf 
Privatkopie", das wir unbedingt einfordern. Hier muss nachgebessert werden. Ein 
Reformprozess bei den Verwertungsgesellschaften hin zu mehr Transparenz und 
Gerechtigkeit ist ebenfalls unerl�sslich. Zuk�nftig werden sich unserer Meinung nach 
Mischmodelle aus pauschaler und individueller Verg�tung durchsetzen. Die legitimen 
Interessen von K�nstlern, Verbrauchern und Industrie m�ssen in Einklang gebracht 
werden. 
 
Software l�sst sich unserer Meinung nach weder mal so eben in das Urheberrecht, noch in 
die g�ngigen Patent�bereinkommen integrieren. Jedoch ist bereits in den letzten Jahren 
die Patentierbarkeit von Software durch die Praxis der Patent�mter und die 
Rechtsprechung immer weiter ausgedehnt worden. 
 
W�nschenswert ist von unserer Seite eine �nderung der Rechtslage, um eine Ausweitung 
der Patentierbarkeit zu verhindern. Allerdings ist durch den Richtlinienvorschlag der  
Kommission bereits klar, dass die Patentierbarkeit von Software unter bestimmten 
Bedingungen endg�ltig erlaubt und geregelt werden soll. 
 


                                                                                           2



Insbesondere Entwickler von Open Source Software, kleine und mittlere Unternehmen und 
freie Entwickler von Software sehen sich durch die Ausweitung der Patentierbarkeit von 
Software gef�hrdet.  
 
In der Informations�konomie gewinnt die Kooperation im Netzwerk an Bedeutung 
gegen�ber der hierarchischen Kooperation. Bei der Softwareentwicklung ist die 
Zusammenarbeit von kleinen und mittleren Unternehmen und freien Softwareentwicklern 
der Entwicklung in Gro�unternehmen z.T. �berlegen. 
 
Open Source hat eine wichtige Funktion bei der Herstellung von mehr Wettbewerb auf 
dem Softwaremarkt. Es gew�hrleistet durch die Offenheit des Quellcodes die M�glichkeit, 
Interoperabilit�t und Wettbewerb gleichzeitig zu gew�hrleisten.  
 
Open Source erm�glicht es, Wettbewerb und Kommunikationsf�higkeit unterschiedlicher 
Software-L�sungen sicherzustellen. Das Open-Source Betriebssystem Linux setzt sich bei 
Servern mehr und mehr durch - gegen Microsoft-Windows und andere propriet�re 
Betriebssysteme. Es l�uft stabiler, ist billiger und kann den jeweiligen Bed�rfnissen der 
Nutzer dank seines offenen Quellcodes besser angepasst werden. Zudem l�sst sich Linux 
wesentlich besser gegen Angriffe von au�en sichern. 
 
Daher darf Open Source durch Software-Patente nicht behindert werden.  
 
Die Patentierbarkeit von Software nutzt vor allem den Gro�unternehmen: Sie verf�gen 
�ber eigene Patent- und Rechtsabteilungen, die Recherchen und Anmeldungen effizient 
abwickeln k�nnen. Die zunehmende Patentierbakeit von Software f�hrt dazu, dass der 
Wettbewerb um Innovationen hinter juristische Auseinandersetzungen zur�cktritt.  
 
Eine Studie des Massachusetts Institute of Technology hat die volkswirtschaftlich 
negativen Effekte der Patentierbarkeit von Software nachgewiesen. Ein gro�er Teil der 
Energie der Entwicklungsarbeit m�sste dann auf die Recherche bestehender Patente 
verwandt werden.  
 
Die Rechte der Entwickler von Software m�ssen gewahrt werden. Unternehmen und 
Programmierer m�ssen angemessene Ertr�ge f�r ihre Arbeit realisieren k�nnen. 
Softwarepatente sind nicht der geeignete Schutz. Sie behindern Innovation und 
Wettbewerb. Softwareentwickler betonen z.T. den v�llig eigenen Charakter von Software, 
andere sehen sich ausreichend durch das Urheberrecht gesch�tzt.  
 
Notwendig ist eine breite Debatte �ber den geeigneten Schutz der Rechte der Entwickler, 
die Gew�hrleistung von Anreizen zur Investition in Software von Innovationen und 
Sicherstellung von Wettbewerb auf den Softwarem�rkten.  
 
 


                                                                                              3



PUNKT II: FLEXIBILISIERUNG VON WEITERBILDUNG UND BESCH�FTIGUNG  
      
B�ndnis 90/ Die Gr�nen unterst�tzen lebensbegleitendes Lernen, denn das Wissen von 
Mitarbeiterinnen und Mitarbeitern ist das Kapital der Unternehmen in der 
Wissensgesellschaft. Deshalb haben wir mit dem Job-Aqtiv-Gesetz die M�glichkeit 
geschaffen, dass die Weiterbildung von Besch�ftigten und deren Stellvertretung durch 
Arbeitslose miteinander verbunden werden k�nnen Die befristete Einstellung von 
StellvertreterInnen wird durch einen Lohnkostenzuschuss unterst�tzt. Damit wird die 
Qualifizierung der Mitarbeiter f�r die Unternehmen billiger und die Integration Arbeitsloser 
erleichtert.  
 Die Kosten f�r Weiterbildung sind bei den Unternehmen voll absetzbar, bei Privaten als 
Werbungskosten im Zusammenhang mit dem Beruf gegeben. Aufwendungen f�r Fort- und 
Weiterbildung sind bereits beim Unternehmen als Betriebsausgaben voll und beim 
Arbeitnehmer als Werbungskosten im ausge�bten Beruf ebenfalls voll und als 
Aufwendungen in einem (noch) nicht ausge�bten Beruf (also z.B. Ausbildung) als 
vorweggenommene Werbungskosten oder Sonderausgaben in bestimmten Grenzen 
steuerlich absetzbar. Auch sonstige indirekte Kosten wie z.B. Entgeltfortzahlung sind als 
Betriebsausgaben nat�rlich voll absetzbar. Umsatzausfall f�hrt ebenfalls entsprechend zu 
einer Gewinnminderung und damit zu einer geringeren Besteuerung. Die angesprochenen 
"Frustrationskosten", die entstehen, wenn die Fort- und Weiterbildung f�r das 
Unternehmen nicht den erw�nschten Erfolg bringt, schlagen sich in geringerem Umsatz, 
niedrigerem Gewinn nieder und mindern so wiederum die Steuerlast. 
  *  Die Schaffung einer Multimedia-Weiterbildungsverordnung 
            o  Es sind bereits viele neue Berufsbilder in der IT festgelegt worden.  
Die Neudefinition neuer IT- Ausbildungsberufe war ein gro�er Erfolg. Die Wirtschaft hat 
eine Erh�hung der Ausbildungsstellen im IT- Bereich von 40.000 auf 60.000 Stellen bis 
zum Jahr 2003 zugesichert � in Berufen, die es vor wenigen Jahren noch nicht gab. Ein 
beschleunigtes Verfahren zur Definition von Erstausbildungen muss dringend umgesetzt 
werden. 
Die Bundesregierung hat die Entwicklung eines Rahmens f�r die IT- Weiterbildung in 
Angriff  genommen. Geplant ist ein Ordnungsverfahren f�r ausgew�hlte Weiterbildungen 
abzuschlie�en. Wir unterst�tzen das. Einheitliche Zertifikate sind unbedingt erforderlich. 
Allein die Bundesanstalt f�r Arbeit f�hrt jedes Jahr 40.000 Weiterbildungsma�nahmen im 
IT-Bereich durch. Hier brauchen wir Praxisn�he und Transparenz. Daf�r wollen wir 
zusammen mit der Wirtschaft modulare Abschl�sse definieren.  
     *  Die Einf�hrung eines Jungunternehmerprivilegs im Arbeitsrecht 
Kleine und mittlere Unternehmen sind in besonderer Weise auf einen flexiblen 
Arbeitsmarkt angewiesen. Sie k�nnen nicht � wie gro�e Konzerne � Flexibilit�t durch 
einen internen Arbeitsmarkt herstellen, sondern ben�tigen einen flexiblen Rahmen nach 
au�en. F�r B�ndnis 90/ Die Gr�nen stehen kleine und mittlere Unternehmen bei der 
notwendigen Modernisierung des Arbeitsmarktes im Zentrum.  
Wir stehen f�r einen Ansatz, der Flexibilisierung und soziale Sicherheit ins Gleichgewicht 
bringt. Nur so haben wir eine Chance, auch strukturelle Arbeitslosigkeit abzubauen und 
bei den Betroffenen den Mut und die F�higkeit zur Ver�nderung zu erh�hen. Dazu ist die 
Zusammenf�hrung von Arbeitslosen- und Sozialhilfe in eine Grundsicherung n�tig. 
     *  Die eingeschr�nkte Zulassung von Kettenbefristungen im Projektgesch�ft 

                                                                                               4



Mit dem Gesetz zur �ber Teilzeit- und befristete Besch�ftigung haben wir der Regelung, 
nach der Arbeitnehmer ohne besonderen Grund bis zu zwei Jahre befristet besch�ftigt 
werden k�nnen, dauerhaft Geltung verschaffen. Die Union hatte lediglich eine zeitlich 
befristete Regelung geschaffen.  
Bei Projekten kann dar�ber hinaus l�nger zeitlich befristet werden. In der Sprache des 
Gesetzes handelt es sich dann um eine Besch�ftigung mit sachlichem Grund, f�r die 
anderen Kriterien gelten.  
Wir treten daf�r ein, innerhalb der Frist von 2 Jahren ohne jede Begrenzung Befristungen 
zuzulassen � also auch sogenannte Kettenbefristungen.  
 
PUNKT III: EINRICHTUNG VON FOUNDING- AGENTUREN 
 
In den letzten Jahren hat sich eine neue Kultur der Selbst�ndigkeit in Deutschland 
entwickelt - trotz der �berbewertung in Medien und B�rse in der Phase des Hype - und 
Unterbewertung, die nun darauf gefolgt ist. Selbst�ndigkeit ist f�r viele junge Menschen 
wieder zum Berufsziel geworden.  
Nat�rlich hat es aufgrund der schlechten konjunkturellen Entwicklung, bedingt durch die 
weltwirtschaftliche Situation, R�ckschl�ge gegeben. Dennoch hat die Zahl der 
Unternehmen in Deutschland kontinuierlich zugenommen - im Jahr 2001 hatten wir 7.000 
Unternehmen mehr als im Jahr 1998. Auch die Zahl der Besch�ftigten der am Neuen 
Markt gelisteten Unternehmen ist gestiegen - trotz des Kursverfalls.  
B�ndnis 90/ Die Gr�nen verstehen sich auch als Partei f�r Selbst�ndigkeit und 
Selbstorganisation. Deshalb unterst�tzen wir alle Vorhaben, die darauf abzielen, 
Selbst�ndigkeit und Existenzgr�ndungen zu erleichtern.  
Ein wesentlicher Punkt ist dabei die Modernisierung der �ffentlichen Verwaltungen 
zugunsten von mehr Servicequalit�t.  
Wir verfolgen das Ziel, One- Stop- Founding- Agenturen zu schaffen. Sie sollten an die 
Kommunen angegliedert werden und umfassende Beratungen anbieten. Mit allen 
F�rderinstitutionen wie den L�nderwirtschaftsministerien und den F�rderbanken m�ssten 
sie vernetzt sein. Zugleich wollen wir auf Bundesebene die entsprechenden rechtlichen 
Rahmenbedingungen schaffen, damit wirklich alle Meldungen aus einer Hand m�glich 
werden.  
Durch die Verbesserung des Beratungsangebotes wollen wir den Zugang zu den 
F�rderm�glichkeiten verbessern. 
Den Zugang des kleiner und mittelst�ndischer Unternehmen zu Fremd- und 
Beteiligungskapital wollen wir verbessern. F�r einen lebendigen Beteiligungskapitalmarkt 
wollen wir die Transparenz des Kapitalmarktes verbessern. Wirtschaftspr�fer sollen z.B. 
keine Beratungsauftr�ge mehr �bernehmen d�rfen, denn damit werden sie vom Vorstand 
abh�ngig. Wirtschaftspr�fer m�ssen sich aber an den Interessen von Aufsichtsrat und 
Aktion�ren orientieren.  
Zur Entwicklung der Kultur der Selbst�ndigkeit haben die 42 Existenzgr�nderlehrst�hle 
wesentlich beigetragen, die w�hrend unserer Regierungszeit mit F�rderung durch die 
Deutsche Ausgleichsbank an den Hochschulen geschaffen worden sind. Denn es kommt 
darauf an, in der Berufsausbildung auf eine selbst�ndige T�tigkeit vorzubereiten.  
      


                                                                                             5



     *  F�rderm�glichkeiten: umfangreiche F�rderung durch das BMWi (Abt. II), z.B. 
        Gr�ndungsportal sowie Beteiligungskapitalf�rderung, Gr�ndungswettbewerb 
        Multimedia 
 
 
Punkt IV: Schaffung eines einheitlichen Ausschreibungsportals 
 
Wir unterst�tzen die Schaffung eines einheitlichen Ausschreibungsportals von Bund, 
L�ndern und Gemeinden. Ein Projekt f�r Ausschreibungen des Bundes ist in Arbeit, die 
Einbeziehung von L�ndern und Kommunen wird angestrebt. Mit der neuen 
Vergabeverordnung, dem Signatur- und Formvorschriftenanpassungsgesetz im letzten 
Jahr und die Einf�hrung elektronischer Signaturen ist es m�glich, komplette 
Beschaffungsvorhaben von der elektronischen Angebotsabgabe bis zur Auftragsvergabe 
und von der Bekanntmachung bis zur Vertragsschlie�ung, �ber das Internet abzuwickeln.  
 Derzeit wird vom Bundeswirtschaftsministerium das Projekt E-Vergabe durchgef�hrt, das 
die elektronische Beschaffung erprobt. Eine wissenschaftliche Begleitforschung analysiert 
und b�ndelt die verschiedenen Erfahrungen w�hrend der Pilotphase, stellt die 
�bertragbarkeit der Ergebnisse sicher und leistet eine koh�rente Verbreitung der 
Ergebnisse. 
 Im Anschluss an eine erfolgreiche Testphase ist eine breite Anwendung in der 
Bundesverwaltung vorgesehen. Wir treten daf�r ein, L�nder und Kommunen zu beteiligen.  
            
























                                                                                           6



