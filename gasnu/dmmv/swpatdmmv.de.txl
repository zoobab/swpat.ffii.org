<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Deutscher Multi-Media-Verband und Softwarepatente

#descr: Der DMMV veröffentlichte im Mai 2002 ein Programmpapier, in dem er die
Schaffung und Stärkung von %(q:geistigem Eigentum) aller Art zur
zentralen ordnungspolitischen Aufgabe der Informationswirtschaft
erklärt und insbesondere eine weitere Erleichterung des Zugangs zu
Patenten fordert.  Trotz einer starken Stellung von Großunternehmen
und Juristen innerhalb des in München ansässigen Verbandes konnte sich
der DMMV sich im Oktober 2003 dazu durchringen, deutlich gegen
Softwarepatente und für die Entscheidung des Europäischen Parlamentes
zur Änderung der geplanten Softwarepatentrichtlinie Stellung zu
beziehen.  Damit positioniert sich der Verband gegen Bitkom als
Sprecher der mittelständischen Softwarebranche.

#dao: dmmv als solcher

#VWW: Vorstand (board) des dmmv

#Kad: Kooperation dmmv-VSI

#Vot: VSI, Microsoft und Software-Patente

#Faz: Fazit

#Wcr: Wenn man durch das Mitgliederverzeichnis durchstöbert (Buchstabe
%(q:A) s. u.), dann sind das schon den Firmennamen nach im
wesentlichen Firmen, wie Werbeagenturen, Multimediaagenturen, Verlage,
Bildungssinstitute. Für mich also eher Firmen, die %(e:Inhalte)
entwickeln und Software %(e:anwenden), aber Software nicht oder nur
zum kleinen Teil entwickeln.

#Bcc: Beim Buchstaben %(q:A) finden sich beispielsweise

#Anr: Agenturen

#sdd: so so, der dmmv %(q:vertritt mehr als 1700 kleine und mittelständische
Unternehmen dieses neuen Wirtschaftszweiges.) Ist die Rezession schon
so weit fortgeschritten, daß Axel Springer AG, AOL, T-Online, Worldcom
jetzt klein und mittelständisch sind?

#AjW: Ach ja, am Fuß jeder www-Seite des dmmv steht

#Ddn: Der Deutsche Multimedia Verband (dmmv) e.V. und der VSI, der Verband
der Softwareindustrie Deutschlands e.V., haben eine umfassende
Kooperation ab dem Jahr 2002 vereinbart.

#Dee: Der Verband der Softwareindustrie Deutschlands e.V. (VSI), gegründet
1987, ist eine Interessenvereinigung der Softwareindustrie mit
Hauptsitz in München. Die inzwischen über 170 Mitglieder kommen aus
den Bereichen Software-Herstellung, Distribution, Handel und
Dienstleistung.

#dWd: dmmv und VSI führen gemeinsame Geschäftsstellen in Düsseldorf, München
und Berlin. Den Mitglieder beider Verbände stehen alle Benefits offen.

#Ite: Insofern gehen wir davon aus, daß der dmmv was Software betrifft eine
Tendenz haben könnte, Positionen des VSI oder von VSI-Mitgliedern
wiederzugeben.

#VaS: Vorstand VSI 2000

#DSn: Der VSI hat folgenden Vorstand (Stand 2000, auf der Website haben wir
keine neuere Wahl gefunden):

#DWi: Diese Liste ist eher konzernlastig als mittelstandsverdächtig.

#Ddg: Dafür kann er auf Unterstützung im gesamten Verband rechnen. %(bc:Wir
freuen uns, auf die Zusammenarbeit mit Herrn Roy. Sicherlich wird das
aktive Engagement eines Vizepräsidenten der Firma Microsoft die
Interessenvertretung der Softwareindustrie stärken), erklärte Rudolf
Gallist, Vorstandvorsitzender des VSI. Erfahrung bringt Roy für diese
Aufgabe zur Genüge mit. Schließlich leitet er in seiner Funktion bei
Microsoft ein Team aus Experten für politische Themen,
Öffentlichkeitsarbeit und europäische Angelegenheiten.

#Knt: Künstliche Gegensätze zwischen freier und proprietärer Software ---
Wen vertritt Gallist?

#SFm: Kurz nach dem Eintritt von Roy in den Vorstand holt der VSI zu einem
etwas plumpen Angriff gegen die Einführung von GNU/Linux im Bundestag
aus, der bei weitem nicht das intellektuelle Niveau von Microsofts
hauseigener Pressearbeit (Mundie, Allchin) erreicht, aber im
wesentlichen mit den gleichen Scheinargumenten arbeitet.

#WiW: Wir glauben nicht, daß die frühere dmmv-Position zu Software-Patenten
von kleinen und mittleren dmmv-Mitgliedern erarbeitet wurde, die
selbst Software entwickeln.  Es besteht vielmehr eine hohe
Wahrscheinlichkeit, dass entsprechende Positionen in diesem Papier von
Microsoft kamen.  Zugleich entsprachen solche Positionen auf den
ersten Blick der eigentumsverliebten Ideologie dieses Verbandes und
weit verbreiteten positiven Vorurteilen über das Patentwesen.  Es ist
sehr leicht, einem Verband wie dem DMMV eine Position zu einem Thema
unterzujubeln, welches die meisten Verbandsmitglieder nicht
interessiert und zu welchem ohnehin nur Patentjuristen in unbeachteten
Arbeitskreisen zu Wort kommen.

#edK: Umso beachtlicher ist der im Herbst 2003 vollzogene Kurswechsel und
die Rede des ehemaligen Microsoft-Managers Gallist.  Er fällt zusammen
mit Bemühungen des DMMV, durch Zusammenschlüsse u.a. in neue Gebiete
zu expandieren und Anerkennung als Vertreter der mittelständischen
Softwarebranche zu finden.

#WeV: Mitte 2003 gegründet, geht auf VSI zurück

#3dw: DMMV 2003-10-23: DMMV empfiehlt EU-Ministerrat, der
Parlamentsentscheidung zu Softwarepatenten zu folgen

#ota: Der Mitte 2003 neu gegründete Arbeitskreis der Softwareindustrie
innerhalb des Deutschen MultiMediaVerbandes (DMMV) hält
Softwarepatente wie von EU-Kommission und Rat befürwortet für
mittelstandsfeindlich und verweist auf Gefahren, die verschiedenen
mittelständisch geprägten neuen Branchen drohen.  Der DMMV ist daher
mit den Änderungen des EU-Parlamentes zufrieden und verweist auf einen
breiten Konsens unterschiedlicher Strömungen, der hinter diesen
Änderungen stehe.

#bsz: berichtet über DMMV-Forderungen nach einer %(q:Internet-Polizei), die
stichprobenartig den Datenverkehr abhören soll, um
Urheberrechtsverletzungen aufzuspüren.

#Dtg: Der in München ansässige Deutsche Multimedia Verband (dmmv) fordert in
einem %(q:Vier-Punkte-Plan) allerlei Gesetzesänderungen zur
%(q:Stärkung der Digitalen Wirtschaft).  Er feiert diese Initiative
durch eine Übergibt und Diskussion mit den größten Bundesparteien am
15. Mai in Berlin.  %(q:Zentrale Forderung des Papieres ist ein
effektiver Schutz digitaler Güter sowie deren Anerkennung als
kreditfähiges Wirtschaftsgut und die Vereinfachung des
Patentschutzes.)  Ferner fordert der DMMV den Abbau von
Arbeitnehmerrechten und massive staatliche Subventionen für junge
Unternehmen.

#Bnl: Berichtet über Reaktionen der Parteipolitiker auf eine Aktion des DMMV
für stärkeren %(q:Schutz digitaler Güter) einschließlich Erleichterung
der Patentierung von Programmierideen und Subventionen des Staates für
junge Unternehmen.  Angesprochene Parteipolitiker versichern ihre
Unterstützung.  Einige Heise-Diskutanten geloben, nun PDS zu wählen.

#Mee: Leitseite der DMMV-Wahlkampagne.  Der Verein hofft, dass zahlreiche
Unternehmen das Werbebanner der Kampagne ausstellen werden. 
Wesentliche Argumentationsgrundlagen des DMMV konnten wir noch nicht
überprüfen -- sie liegen nur als Präsentationsgrafiken in
MS-Powerpoint-Format vor.  Der DMMV zählt Microsoft und Adobe zu
seinen fünf Haupt-Sponsoren.

#UWg: Unter Punkt 1.2. / S. 7-9 findet sich auch die Forderung nach
Legalisierung von Softwarepatenten.  Punkt %(q:1.2.2 Problemstellung)
ist wirtschaftlicher Unsinn, sieht nach
Patentbewegungs-Glaubensbekenntnis aus.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatdmmv ;
# txtlang: de ;
# multlin: t ;
# End: ;

