<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Patentpapst Prof. Dr. Josef Straus

#descr: Geschäftsführender Direktor einer Patentbewegungs-Kaderschmiede in München, die ihr Ansehen vor allem auf den Namen des inzwischen wehrlosen Max Planck stützt.  Vorkämpfer für die Ausdehnung des Patentwesens auf Ideen aller Art, insbesondere Rechenregeln und Gene, Verfasser zahlreicher Kampfschriften für die Weltorganisation der Patentrechtler und Gutachten für das Europäische Patentamt (EPA), offenbar darin durchweg einen bedingungslosen Glauben an den Wert der Schaffung immer neuer künstlicher Eigentumsrechte durch Ausdehnung überkommener, niemals auf ihre volkswirtschaftliche Wirkung hin überprüfter und -- nicht zuletzt dank Pflege durch Straus & Co -- inzwischen ziemlich verwahrloster Regeln des Patentwesens.  Lehrer von Nack, Schiuma und anderen Schülern im Glauben.  Regelmäßiger Verfasser von Resolutionen und Gutachten für Lobby-Organisationen wie AIPPI sowie Patentämter und Regierungen.   Auftragnehmer des BMWi für die Erstellung einer Studie über Softwarepatente.

#BWr: Born 1938 in Trieste, Italy. Law Studies in Ljubljana and Munich. Private practise from 1968 to 1977; since then with the MPI, later also other universities.  Author or co-author of numerous publications in the field of industrial property, especially in the biotech field. Consultant to OECD, WIPO, UNCTAD, UNIDO, CEC, World Bank, Scientific Services of the German Federal Parliament and the European Patent Organisation. Former President of the International Association for the Advancement of Teaching and Research in Intellectual Property (ATRIP), Chairman of the Program Committee of the International Association for the Protection of Industrial Property (AIPPI), Chairman of the Intellectual Property Rights Committee of the Human Genome Organisation (HUGO), Member of Standing Advisory Committy of the European Patent Organisation (SACEPO), Senate Commission on Basic Issues of Gene Research of the German Research Assocation (DFG) etc.

#OnU: Ärztezeitung 2000-07-24: Fischer und Däubler-Gmelin streiten um die Umsetzung der EU-Biopatentrichtlinie

#Edn: Der Artikel zitiert diverse Patentjuristen mit Forderungen zugunsten von Genpatenten. %(bc:Ohne Patente auf DNA-Sequenzen gibt es keine neuen Arzneimittel), sagt etwa Joseph Straus vom Max-Planck-Institut für ausländisches Patent- Urheber- und Wettbewerbsrecht in München. Ebenso sieht es Christian Stein vom deutschen Humangenomprojekt. Nur ein Patent, das die DNA-Sequenz einschließe, ermutige Firmen, in medizinische Gen-Anwendungen zu investieren. %(q:Wenn Sie nur das Produkt schützen, nicht aber die Idee dafür), erläutert Straus, %(q:dann würde jeder versuchen, aus der verwendeten Gensequenz ein ähnliches Produkt zu entwickeln).  Während Justizministerin Herta Däubler-Gmelin sich die Position von Straus zu eigen macht, stellt sich die grüne Gesundheitsministerin Andrea Fischer an die Spitze einer Gruppe diverser Verbände, die eine Behinderung der Entwicklung durch zu breite Patentansprüche befürchten und fordert Zurückhaltung bei der Umsetzung der EU-Richtlinie zur Genpatentierung.  Die Ärztezeitung höhnt zusammen mit den zitierten Patentjuristen, es gehe hier nur um eine rechtliche Absicherung von seit Jahrzehnten etablierter Patentamtspraxis und es sei nicht erwiesen, dass diese Praxis irgendwelchen Schaden angerichtet habe.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatstraus ;
# txtlang: de ;
# multlin: t ;
# End: ;

