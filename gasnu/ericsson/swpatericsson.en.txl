<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Ericsson and Software Patents

#descr: A report by the Swedish telecommunication giant Ericsson estimated in
2001 that %(q:Like other companies operating in the telecommunications
industry, because our products comprise complex technology, we
experience litigation regarding patent and other intellectual property
rights.  Third parties have asserted, and in the future may assert,
claims against us alleging that we infringe their intellectual
property rights.  If we do not succeed in any such litigation, we
could be required to expend significant resources to pay damages,
develop non-infringing technology or to obtain licenses to the
technology which is the subject of such litigation. However, we cannot
be certain that any such licenses, if available at all, will be
available to us on commercially reasonable terms.)  Ericsson is
currently (2003) facing software patent infringment charges in a US
court where the plaintiff is demanding 1 billion USD in damages.  The
patent activities of the swedish telecommunication giant Ericsson are
cited in MEP Arlene McCarthy's software patentability directive
proposal as a reason for legalising software patents in Europe.  Here
we take a close look at the software patent applications of Ericsson
at the European Patent Office.  Although Ericsson's patent portfolio
is gigantic, we cannot find the %(q:1000 patents per year) which
McCarthy talks about, nor does it seem likely that Ericsson's R&D is
dependent on the availability of software patents.  The Ericsson paper
itself writes:  %(q:We rely upon a combination of trade secrets,
confidentiality policies, nondisclosure and other contractual
arrangements, and patent, copyright and trademark laws to protect our
intellectual property rights...).  In order to defent itself against
litigation threats and earn itself the freedom to develop products,
Ericsson it is patenting masses of ideas and concepts which any normal
engineer or software developper would hardly consider as
%(q:inventions).

#epat: Ericsson Software Patents at the European Patent Office (EPO)

#snt: Ericsson 2001: Turning Ideas into Profit

#sbs: An advertising pamphlet which portrays Ericsson as a great inventor
and its patents as reflections of great R&D efforts by showing a
laboratory worker experimenting with computer hardware. The pamphlet
acknowledges that there is a conflict between patents a open standards
and praises Ericsson for its ability to reconcile both.  Ericsson's
patents around the Bluetooth standards are mentioned as the greatest
success of Ericsson's patent policy.

#oIt: Ericsson 2001: Legal Insecurity due to Patents

#WaW: Page 12 of the SEC document, in which Ericsson assesses various risks,
reads: %(q:Like other companies operating in the telecommunications
industry, because our products comprise complex technology, we
experience litigation regarding patent and other intellectual property
rights.  Third parties have asserted, and in the future may assert,
claims against us alleging that we infringe their intellectual
property rights.  If we do not succeed in any such litigation, we
could be required to expend significant resources to pay damages,
develop non-infringing technology or to obtain licenses to the
technology which is the subject of such litigation. However, we cannot
be certain that any such licenses, if available at all, will be
available to us on commercially reasonable terms.)  As of 2003,
Ericsson is facing litigation for infringing tdma patents of
Interdigital. Interdigital wants 1 billion, so Ericssons risk
assessment in 2001 was probably correct.  Ericsson also writes that
patents are only one of many means of securing a return on
investments: %(q:We rely upon a combination of trade secrets,
confidentiality policies, nondisclosure and other contractual
arrangements, and patent, copyright and trademark laws to protect our
intellectual property rights...)

#ins: Ericsson Website

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatericsson ;
# txtlang: en ;
# multlin: t ;
# End: ;

