<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EICTA and Software Patents

#descr: EICTA is an IT lobbying group founded in 1999.

#ata: Recently, EICTA lobbyists in Brussels have said to several MEPs that
they have made a poll which shows that 97% of the software companies
want patents.  The basis for this is a poll made by Bitkom which in
fact says that 52% of the respondents said that they wanted to
%(q:preserve the status quo or reduce patentability).  We have not yet
found out how the enquete was made, but it is clear that at Bitkom the
patent question is in the hands of IBM's patent lawyer Fritz Teufel,
and it is likely that these 52% were from a population of corporate
lawyers.  Hardly a basis for EICTA's lobbying.

#aaW: This AelWiki page argues that EICTA and Agoria mainly represent large
US companies.

#Wrn: CALIU 2003-02: Sobre la posición de EICTA

#cah: Refutal of EICTA's patent credo by Xavier Drudis Ferran from CALIU.org
at the demand of a Spanish Senator after a hearing in the Spanish
Senate, for which EICTA had submitted this paper.

#hWs: Here, patent attorney Harald Hagedorn from SAP, a big loser of the
patent game, spoke for EICTA, saying that all mature industries need
patents and so does the software industry, and of course it also needs
program claims.  Hagedorn also misrepresented studies of the German
government as supporting this assertion.  EICTA is probably sending
Hagedorn to such occasions, because SAP, although a loser, is one of
the few European companies in the game.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpateicta ;
# txtlang: en ;
# multlin: t ;
# End: ;

