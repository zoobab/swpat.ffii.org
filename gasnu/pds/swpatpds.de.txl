<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: PDS und Softwarepatente

#descr: Angelika Marquard hat sich im Namen des PDS-Fraktion gegenüber der
Presse mehrfach deutlich ablehnend gegenüber allen Plänen zur
Legalisierung von Softwarepatenten geäußert.  Insgesamt interessiert
sich die PDS aber nicht stärker als andere Parteien für die Belange
der informationellen Infrastruktur oder für Medienpolitik.  Es hat
einzelne mehr oder weniger erfolgreiche Versuche gegeben, das Thema in
die PDS zu tragen.  Dabei weckt das Faszinosum %(q:OpenSource)
offenbar mehr Interesse als die Auseinandersetzung um die Grenzen der
Patentierbarkeit.  Die PDS-nahe Rosa-Luxemburg-Stiftung unterstützt
das Oekonux-Projekt, welches freie Software als %(q:freie Kooperation)
begreift und nach Wegen zur weitergehenden Überwindung der Markt- und
Warenwirtschaft sucht.  In wie weit solche %(q:Plattformen) Einfluss
auf das politische Handeln der PDS gewinnen können, bleibt unklar.

#Hae: Hartmut Pilch verweist auf zwei Texte aus PDS-Arbeitsgruppen, in denen
auf die Diskussion um den wirtschaftspolitischen Status von
Informationsgütern zwischen Allmende und Markt Bezug genommen wird. 
Dabei wird auf Texte des Oekonux-Projektes verwiesen.   Allerdings
sind die Autoren in den allgemeinen positiven Vorurteilen über das
Patentwesen als Innovationsmotor befangen und wissen nicht so recht
was Patente und insbesondereSoftwarepatente sind.  Der Schreiber
versucht, die Ökonuxler anzuregen, daran etwas zu ändern.  Daran
entspinnt sich eine Diskussion, die zeigt, dass niemand so recht an
diese Arbeit rangehen will.  Mögliche Ansprechpartner innerhalb der
PDS werden allerdings genannt.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatpds ;
# txtlang: de ;
# multlin: t ;
# End: ;

