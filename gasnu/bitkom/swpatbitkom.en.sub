\begin{subdocument}{swpatbitkom}{Bitkom: Voice of IT corporate patent lawyers in Germany}{http://swpat.ffii.org/gasnu/bitkom/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{The German Information and Telecomunication Industry Association Bitkom, one of the influential members of the European EICTA.org, began in 2001 to get involved in questions of patent policy.  The activity was conducted in a closed circle of patent lawyers, dominated by IBM's european patent deparment head Fritz Teufel.  This circle published a first statement in support of the CEC/BSA directive proposal in spring 2002 after adoption by the meeting between 7 patent lawyers of large corporations, held by the IP workgroup under the presidence of Teufel.  This workgroup later published a survey which tries to demonstrate that the software industry wants patents.  This survey was used for supporting talks with politicians for many month before the survey was finally published, after we had reported about it.  The survey does not show what its authors claim it shows.}
heise-bitkom020512

\begin{sect}{prog}{Baseline: More Power for Corporate Boards (?)}
Bitkom has recently been fighting for
\begin{description}
\item[Gegen Urheberrechtspauschalgeb\"{u}hren, f\"{u}r Verwertungsutopien:]\ Besonders der Vorsitzende Rohleder ist von DRM und anderen Hoffnungen auf Verkr\"{u}ppelung digitaler Information zwecks Kassierens pro Lesevorgang begeistert.  Auch der Dachverband EICTA setzt hier einen Schwerpunkt.  Rohleder und EICTA-Kollegen fordern strafrechlich bewehrten Kopierschutz und Einschr\"{a}nkung der Programmierfreiheit gem\"{a}{\ss} neuer EU-Kopierschutzrichtlinie (EuroDMCA).  Dort, wo die IT-Honoratioren neue Wertsch\"{o}pfungspotentiale wittern, muss der Staat hart durchgreifen, um deren Verwirklichung zu erzwingen.  Ob diese Art der Wertsch\"{o}pfung funktioniert oder produktiv ist, interessiert vorerst nicht.  Der Staat hat so zu tun, als ob sie funktionieren w\"{u}rde und schon heute -- ja sogar r\"{u}ckwirkend f\"{u}r die Vergangenheit -- ordnungspolitische Entscheidungen, z.B. hinsichtlich Pauschalgeb\"{u}hren, an dieser Fiktion auszurichten.
\item[F\"{u}r den Import billiger IT-Fachkr\"{a}fte:]\ die Schr\"{o}der-Ank\"{u}ndigung auf CeBit 2001 st\"{u}tzte sich vor allem auf eine Bitkom-Studie.  Seit dem IT-Konjunktureinbruch im zweiten Halbjahr h\"{o}rt man weniger davon.
\item[Gegen B\"{u}rokratisierung und staatliche Regulierung aller Art:]\ nicht nur hinsichtlich Inderimport:  m\"{o}glichst viele Rechte und wenige Pflichten f\"{u}r Unternehmensvorst\"{a}nde
\item[F\"{u}r B\"{u}rokratisierung und staatliche Regulierung durch Patente:]\ Dieser Standpunkt wir allerdings nicht besonders publik gemacht, er findet sich nur seit kurzem in unauff\"{a}lligen PDF-Dateien hinter kryptischen \"{U}berschriften (``SWP-Gutachten'').
\end{description}

Ein Bitkom-Sprecher kann offenbar fachlich noch so ahnungslos daherreden -- Hauptsache die Gesinnung stimmt.  Solange er seine Forderungen an der Devise ``Mehr Macht f\"{u}r die Vorst\"{a}nde'' ausrichtet, wird er schon nicht zur Rechenschaft gezogen werden.
\end{sect}

\begin{sect}{orgnatur}{Organisational structure of Bitkom}
Im Vorstand von Bitkom sitzen Gesch\"{a}ftsleute von Microsoft (Sibold), IBM, Siemens und anderen Gro{\ss}unternehmen.  Auch in den Arbeitsaussch\"{u}ssen haben vor allem diese Leute die n\"{o}tige Zeit, um die Verbandspolitik zu bestimmen.  Im Vorstand ist keine einzige Person vertreten, die Ansehen als Programmierer oder Informatiker genie{\ss}t.  F\"{u}r die einfachen Mitglieder ist Bitkom weniger eine Interessenvertretung als eine Gelegenheit, gewisse Leistungen in Anspruch zu nehmen, vgl ADAC.
\end{sect}

\begin{sect}{umfrage}{Bitkom Survey 2002}
In 2002 Teufel's industrial property committee prepared an unpublished survey\footnote{\url{http://swpat.ffii.org/players/bitkom/bitkom-swpat-umfrage030120.pdf}} which was used by EICTA lobbyists as a basis for their assertions that ``surveys show that most software companies are in favor of patents'', which they have been making at the European Parliament and also in Berlin during this time.

The original questionnaire is not included in the study, but at least some of the questions are indirectly quoted in the report.

The survey asks one basic appetizing question in several variations:

\begin{itemize}
\item
Do you believe that patents can play an important role in strengthening a company's bargaining position?

\item
Do you believe that patents can help your company gain easier access to risk capital?

\item
\dots
\end{itemize}

and interprets the 82\percent{} ``yes'' to any such question as an ``82\percent{} in favor of software patents''.  FFII would also have answered ``yes''.

One question comes close to asking for a political opinion. Bitkom asks approximately:

\begin{quote}
{\it Should the current possibilities for obtaining patent protection for computer-implemented inventions be maintained, extended or reduced?}
\end{quote}

Here, among 90 member companies interviewed, the answers are as follows:

\begin{center}
\begin{tabular}{L{44}R{44}}
extend: & 39\\
maintain: & 37\\
reduce: & 10\\
\end{tabular}
\end{center}

Here FFII would have said ``maintain'', and again Bitkom would have reported to MEPs that ``FFII is in favor of software patents''.

Indeed nobody opposes patents on ``computer-implemented inventions''.  It is just that many of us still believe that, while a chemical reaction or an anti-blocking system can be patented regardless of whether a computer is used for controlling it, computer programs as such are not inventions in the sense of patent law.

It should be noted that quite a few surveys about industry opinion on software patents have been conducted in the United States (e.g. Effy Oz, Samulea Pamuelson, ACM Titus, OTA, USPTO hearings, ...) and also here (Fraunhofer 2001, CEC Consultation), and -- surprise -- most of them were much more strongly against software patents than that of Bitkom.  To be documented here.
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf \url{http://www.bitkom.org/}}}
\filbreak

\item
{\bf {\bf Bitkom-PE 2003-03-22:\footnote{\url{http://www.bitkom.org/index.cfm?gbAction=gbcontentfulldisplay&ObjectID=D321C079-46F6-4D51-9B08AA448674390A&MenuNodeID=4C872DB6-8470-4B01-A36FD8C1EBA2E22D}}}}

\begin{quote}
Susanne Schopf und Bernd Rohlehder erkl\"{a}ren, ohne Softwarepatente gebe es keine Innovation, m\"{u}sse die Branche ins Ausland wandern, fordern vom Rat, mit dem Europ\"{a}ischen Parlament auf Konfrontationskurs zu gehen oder dessen Entscheidung zu kippen: \begin{quote}
{\it Bitkom setzt sich daf\"{u}r ein, dass der Ministerrat in seiner Stellungnahme nicht den Irrwegen des Parlaments folgt, sondern seinen bisherigen vern\"{u}nftigen Kurs weiterverfolgt und sich im Interesse der europ\"{a}ischen Wirtschaft f\"{u}r einen angemessenen Patentschutz f\"{u}r computerimplementierte Erfindungen einsetzt.}
\end{quote}.

see also Treffen der Patent-Arbeitsgruppe des EU-Ministerrates\footnote{\url{http://localhost/swpat/lisri/03/cons1023/index.en.html}} and Heise 03-10-22: \percent{}(q:IT-Verband ruft EU auf den rechten Weg zurück)\footnote{\url{http://www.heise.de/newsticker/data/anw-22.10.03-001/}}
\end{quote}
\filbreak

\item
{\bf {\bf Bitkom in favor of software patents\footnote{\url{http://www.bitkom.org/index.cfm?gbAction=gbcontentfulldisplay&ObjectID=14AA029B-478C-4E0A-874AEA2AAA0EB94C}}}}

\begin{quote}
The German IT Association Bitkom welcomes the CEC/BSA Software Patentability directive proposal.  Bitkom is especially happy to see that software as such is recognised as inventive subject matter by the CEC proposal and demands that correspondingly program claims should also be admitted so that it is assured that software patents can actually be enforced.  In apparent contradiction to the contents of the directive proposal, Bitkom claims that this proposal ensures a high standard of inventiveness.  Because both the software industry and the patent licensing business are gaining importance, it is important that the two be brought together.  This opens a lot of opportunities for SMEs. The Bitkom declaration was decreed by a session of 8 members of the Bitkom Workgroup on industrial property \& copyright.  7 of these 8 members came were patent lawyers representing large companies.  1 was a marketing person from an SME.  The draft was written by the committee speaker, Dr. iur Kathrin Bremer and approved due to the leadership of Fritz Teufel, the IBM patent lawyer who dominated the session.  The paper does not care to substantiate its claims by means of any examples from the experience of Bitkom members nor does it spell out any examples of patents which Bitkom thinks are good and others which they deem bad.  Also, the declaration disregards all informed discussions and studies which have been conducted on the subject.  The initial draft even claimed without providing any trace of evidence that software patents create employment.  Maybe they meant ``for patent lawyers'' like those dominating the session.
\end{quote}
\filbreak

\item
{\bf {\bf FFII an Bitkom: Gesetzentwürfe an Testsuite messen!\footnote{\url{http://localhost/swpat/xatra/bitk025/index.de.html}}}}

\begin{quote}
On 2002-05-07 the German IT association Bitkom published a press release supporting the EU Directive Proposal for the patentability of ``comuter-implemented inventions''.  After a phone conversation with the authors of the press release, Hartmut Pilch wrote a letter to Bitkom which we publish here.  The letter explains a methodological consensus position which at first sight seemed convincing to the Bitkom people.
\end{quote}
\filbreak

\item
{\bf {\bf Heise: IT-Verband sieht bei Software-Patenten Nachbesserungsbedarf\footnote{\url{http://www.heise.de/newsticker/data/anw-07.05.02-005/}}}}

\begin{quote}
Heise-Bericht \texmath{\backslash}``{u}ber die Bitkom-Erkl\texmath{\backslash}``{a}rung
\end{quote}
\filbreak

\item
{\bf {\bf Bitkom zu Softwarepatenten: Beiträge zur Bundestags-Anhörung 2001-06-21\footnote{\url{http://localhost/swpat/penmi/2001/bundestag/bitkom/index.en.html}}}}

\begin{quote}
Das Referat hielt die Vorsitzende des Arbeitskreises Gewerblicher Rechtschutz, Frau Dr. Katrin Bremer.  Anwesend war auch PA Fritz Teufel von IBM, der diesen Arbeitskreis bis vor kurzem geleitet hatte.  In ihrem Referat fordert Frau Bremer eine z\"{u}gige Legalisierung von Softwarepatenten durch Anpassung von Art 52 EP\"{U} an die Rechtsprechung des Europ\"{a}ischen Patentamtes und meint, die ``Opensource-Bewegung'' werde es \"{u}berleben, da sie innovativ sei.  In der sp\"{a}ter eingereichten offenbar von PA Teufel geschriebenen schriftlichen Eingabe hei{\ss}t es, freie Software sei nicht innovativ und es seien immer die Nachahmer, die das Patentwesen f\"{u}rchteten.
\end{quote}
\filbreak

\item
{\bf {\bf Die Stimme ihres Herren -- Dr. iur. Kathrin Bremer\footnote{\url{}}}}

\begin{quote}
Legal delegate of Bitkom e.V., German software trade association.  Female, born 1969, studied international law and came to Bitkom directly from there in 1999.  Apparently insecure on the issue, unfamiliar with software and even with patent law, lets her predecessor in office, IBM patent lawyer Fritz Teufel ghostwrite most of her contributions to the debate.  These include a participation in a hearing of the German Parliament in 2001-06-21.
\end{quote}
\filbreak

\item
{\bf {\bf PA Fritz Teufel\footnote{\url{http://localhost/swpat/gasnu/teufel/index.en.html}}}}

\begin{quote}
Patent lawyer, software patentability guru, patent department head of IBM in Germany and Europe, working in Stuttgart, active promoter of software patents, responsible for pushing many landmark cases through the EPO and the German courts.  Ghostwriter of various patent papers of German and European trade associations.  Positions and style well known from public discussions. Hardline advocate of software patentability and very much in love with certain dogmatic fallacies which he successfully used to win over the (already very inclined) EPO and BGH in a series of decisive battles.  Do not expect Teufel to understand the viewpoint of opensource programmers or to come up with solutions to non-juridical problems.  Expect him to stick to EPO fallacies as steadfastly as anyone.  These fallacies constitute his success experience before the lawcourts.
\end{quote}
\filbreak

\item
{\bf {\bf Bitkom zu Softwarepatenten: Beiträge zur Bundestags-Anhörung 2001-06-21\footnote{\url{http://localhost/swpat/penmi/2001/bundestag/bitkom/index.en.html}}}}

\begin{quote}
Das Referat hielt die Vorsitzende des Arbeitskreises Gewerblicher Rechtschutz, Frau Dr. Katrin Bremer.  Anwesend war auch PA Fritz Teufel von IBM, der diesen Arbeitskreis bis vor kurzem geleitet hatte.  In ihrem Referat fordert Frau Bremer eine z\"{u}gige Legalisierung von Softwarepatenten durch Anpassung von Art 52 EP\"{U} an die Rechtsprechung des Europ\"{a}ischen Patentamtes und meint, die ``Opensource-Bewegung'' werde es \"{u}berleben, da sie innovativ sei.  In der sp\"{a}ter eingereichten offenbar von PA Teufel geschriebenen schriftlichen Eingabe hei{\ss}t es, freie Software sei nicht innovativ und es seien immer die Nachahmer, die das Patentwesen f\"{u}rchteten.
\end{quote}
\filbreak

\item
{\bf {\bf EICTA\footnote{\url{}}}}
\filbreak

\item
{\bf {\bf BITKOM: Gesundheitsdebatte beenden, UMTS zügig ausbauen!\footnote{\url{http://www.heise.de/newsticker/data/ad-12.05.02-000/}}}}

\begin{quote}
Ein Bitkom-Sprecher versichert, 20000 Gutachten h\"{a}tten bewiesen, dass Bedenken gegen die UMTS-Strahlen unbelegt seien und warnt vor wirtschaftlichen Sch\"{a}den, welche die anhaltende Gesundheitsdebatte erzeuge.
\end{quote}
\filbreak

\item
{\bf {\bf BITKOM ... eine Lachnummer\footnote{\url{http://www.heise.de/mobil/newsticker/foren/go.shtml?read=1&msg_id=1743513&forum_id=29250}}}}
\filbreak

\item
{\bf {\bf Bitkom Enquete:  84\percent{} see patents as useful means of protection\footnote{\url{bitkom-swpat-umfrage030120.pdf}}}}

\begin{quote}
Bitkom report about an enquete conducted among member companies.  Unfortunately the catalogue of questions was not published.  The report was worked out by Teufel's patent lawyer workgroup.  Questions about the usefulness of patents for a company strategy are reinterpret as endorsements of a certain patent policy.  Ambiguous terms such as ``status quo'' or ``current legal situation'' are interpreted so as to make a lack of enthusiasm for patents appear like support of patentability.  60\percent{} of the respondents pronounced themselves against ``extension of patentability''.
\end{quote}
\filbreak

\item
{\bf {\bf GEMA droht mit Millionenklage wegen Abgabe auf CD-Brenner\footnote{\url{http://www.heise.de/newsticker/data/anw-01.03.02-005/}}}}

\begin{quote}
Manches mag gegen eine Urheberrechtssteuer auf Drucker und Festplatten sprechen.  Diese Steuer ist jedoch gesetzlich geregelt, und die Argumente mit denen HP und, als deren Sprachrohr, Bitkom sich ihr zu entziehen versuchen, sind weitaus bedenklicher als die Urheberrechtssteuer selber.  Denn Individuelle Verg\"{u}tungsverfahren nach Bitkom-Wunsch \begin{itemize}
\item
sind bislang nirgends in nennenswertem Umfang im Einsatz

\item
f\"{u}hren zu einer Wertminderung dessen, was verg\"{u}tet werden soll

\item
f\"{u}hren ebenso wenig wie andere Verfahren zu ``gerechter Entlohnung von Leistungen''

\item
f\"{u}hren geradeaus zu den Hollywood/Hollings-Gesetzentw\"{u}rfen, gegen die die sich die gesamte amerikanische IT-Welt derzeit auflehnt

\item
f\"{u}hren im Erfolgsfall zu un\"{u}bersehbaren Konfliktszenarien.
\end{itemize}  Wer aufgrund einer Utopie dieser Beschaffenheit im Ton der Entr\"{u}stung gegen die GEMA Sturm laufen zu m\"{u}ssen glaubt, ist wohl einfach nur von allen guten Geistern verlassen.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Questions, Things To Do, How you can Help}
If you have any questions concerning the bitkom\footnote{\url{http://localhost/ffii/index.en.html\#bitkom}} project, please do not hesitate to contact bitkom-help@ffii.org.

see also Bitkom Wiki Page\footnote{\url{http://kwiki.ffii.org/SwpatbitkomEn}}

\begin{itemize}
\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Contact Bitkom member companies}}

\begin{quote}
Contact bitkom-help@ffii.org to obtain some addresses and phone numbers and instructions on what to ask them.  You will receive a response within 1 day.
\end{quote}
\filbreak

\item
{\bf {\bf Supply contact data}}

\begin{quote}
Find out contact persons and phone numbers and report on results.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

