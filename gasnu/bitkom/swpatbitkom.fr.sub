\begin{subdocument}{swpatbitkom}{Vom Teufel geritten --- BITKOM e.V.}{http://swpat.ffii.org/acteurs/bitkom/index.fr.html}{Groupes de travail\\swpatag@ffii.org}{Der deutsche Branchenverband Bitkom hat erst Mitte 2001 begonnen, sich mit Fragen der Patentpolitik zu befassen.  Die Meinungsbildung fand offenbar in einem sehr kleinen Kreis von Juristen und Patentjuristen statt, wobei IBM-Patentanwalt Fritz Teufel alles dominierte.  An der EU-Konsultation 2000 zu Swpat nahm Bitkom wegen unabgeschlossener Meinungbildung in der Sache nicht teil, daf\"{u}r aber sprang der europ\"{a}ische Dachverband EICTA ein, f\"{u}r den offenbar ebenfalls Teufel die Stellungnahme schrieb.}
\begin{sect}{prog}{Roter Faden: Mehr Macht den Vorst\"{a}nden (?)}
Bitkom engagierte sich in letzter Zeit
\begin{description}
\item[Gegen Urheberrechtspauschalgeb\"{u}hren, f\"{u}r Verwertungsutopien:]\ Besonders der Vorsitzende Rohleder ist von DRM und anderen Hoffnungen auf Verkr\"{u}ppelung digitaler Information zwecks Kassierens pro Lesevorgang begeistert.  Auch der Dachverband EICTA setzt hier einen Schwerpunkt.  Rohleder und EICTA-Kollegen fordern strafrechlich bewehrten Kopierschutz und Einschr\"{a}nkung der Programmierfreiheit gem\"{a}{\ss} neuer EU-Kopierschutzrichtlinie (EuroDMCA).  Dort, wo die IT-Honoratioren neue Wertsch\"{o}pfungspotentiale wittern, muss der Staat hart durchgreifen, um deren Verwirklichung zu erzwingen.  Ob diese Art der Wertsch\"{o}pfung funktioniert oder produktiv ist, interessiert vorerst nicht.  Der Staat hat so zu tun, als ob sie funktionieren w\"{u}rde und schon heute -- ja sogar r\"{u}ckwirkend f\"{u}r die Vergangenheit -- ordnungspolitische Entscheidungen, z.B. hinsichtlich Pauschalgeb\"{u}hren, an dieser Fiktion auszurichten.
\item[F\"{u}r den Import billiger IT-Fachkr\"{a}fte:]\ die Schr\"{o}der-Ank\"{u}ndigung auf CeBit 2001 st\"{u}tzte sich vor allem auf eine Bitkom-Studie.  Seit dem IT-Konjunktureinbruch im zweiten Halbjahr h\"{o}rt man weniger davon.
\item[Gegen B\"{u}rokratisierung und staatliche Regulierung aller Art:]\ nicht nur hinsichtlich Inderimport:  m\"{o}glichst viele Rechte und wenige Pflichten f\"{u}r Unternehmensvorst\"{a}nde
\item[F\"{u}r B\"{u}rokratisierung und staatliche Regulierung durch Patente:]\ Dieser Standpunkt wir allerdings nicht besonders publik gemacht, er findet sich nur seit kurzem in unauff\"{a}lligen PDF-Dateien hinter kryptischen \"{U}berschriften (``SWP-Gutachten'').
\end{description}

Ein Bitkom-Sprecher kann offenbar fachlich noch so ahnungslos daherreden -- Hauptsache die Gesinnung stimmt.  Solange er seine Forderungen an der Devise ``Mehr Macht f\"{u}r die Vorst\"{a}nde'' ausrichtet, wird er schon nicht zur Rechenschaft gezogen werden.
\end{sect}\begin{sect}{cuibono}{Schattenhafte Funktion\"{a}rsgestalten}
Im Vorstand von Bitkom sitzen Chefs und Fachleute von Microsoft (Sibold), IBM, Siemens und anderen Gro{\ss}unternehmen.  Auch in den Arbeitsaussch\"{u}ssen haben vor allem diese Leute die n\"{o}tige Zeit, um die Verbandspolitik zu bestimmen.  F\"{u}r die einfachen Mitglieder ist Bitkom weniger eine Interessenvertretung als eine Gelegenheit, gewisse Leistungen in Anspruch zu nehmen, vgl ADAC.
\end{sect}\begin{sect}{links}{Liens Annot\'{e}s}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Bitkom f\"{u}r Softwarepatente\footnote{http://www.bitkom.org/index.cfm?gbAction=gbcontentfulldisplay\&ObjectID=14AA029B-478C-4E0A-874AEA2AAA0EB94C}}}

\begin{quote}
Der deutsche IT-Branchenverband Bitkom begr\"{u}{\ss}t in einer Presseerkl\"{a}rung, dass nunmehr laut Br\"{u}sseler RiLi-Vorschlag die Leistungen von Programmierern unmittelbar dem Patentschutz zug\"{a}nglich sein sollen und mahnt an, dass konsequenterweise auch unmittelbare Programm-Anspr\"{u}che zugelassen werden m\"{u}ssen, damit diese Software-Patente auch durchsetzbar sind.  In offensichtlicher Verkennung des RiLi-Entwurfes behauptet Bitkom, dieser stelle hohe Anforderungen an patentierbare Erfindungen.  Da die Softwarebranche immer bedeutender w\"{u}rde und das Patentwesen im Trend der Zeit liege (laut zitierter EPA-Statistik Verzehnfachung der Lizenzgeb\"{u}hren allein in den letzten 10 Jahren), m\"{u}sse nun dringend auch der Softwarebranche dieses neue Wertsch\"{o}pfungspotential erschlossen werden.  Insbesondere kleinen und mittleren Unternehmen biete dies Investitionssicherheit.  Dem FFII vorliegenden Informationen zufolge wurde die auf einer Sitzung des ``Bitkom-Ausschusses f\"{u}r Gewerblichen Rechtschutz und Urheberrecht'' von 7 Gro{\ss}unternehmens-Patentanw\"{a}lten gegen einen KMU-Vertreter beschlossen.  Sie ist von unbelegten Behauptungen gepr\"{a}gt, die im Widerspruch zu allen wirtschaftlichswissenschaftlichen Studien stehen, auch den von der Patentlobby bezahlten.  In ihrem ersten Entwurf behauptete die Ausschussvorsitzenden Frau Dr. Kathrin Bremer sogar, dass Softwarepatente Arbeitspl\"{a}tze schaffen.  M\"{o}glicherweise fiel den versammelten Patentanw\"{a}lten dann doch auf, dass dies allenfalls f\"{u}r ihre eigene Branche gilt und daher besser nicht erw\"{a}hnt werden sollte.
\end{quote}
\filbreak

\item
{\bf {\bf FFII \`{a} Bitkom: Mesurons les Propos de loi par une Suite de Testes!\footnote{http://swpat.ffii.org/lettres/bitk025/index.de.html}}}

\begin{quote}
Am 7. Mai 2002 ver\"{o}ffentlichte der IT-Branchenverband Bitkom eine Presseerkl\"{a}rung, in der er den EU-Richtlinienentwurf f\"{u}r die Patentierbarkeit von ``computer-implementierbaren Erfindungen'' unterst\"{u}tzt. Nach telefonischer Unterhaltung mit Urhebern dieser Presseerkl\"{a}rung schrieb Hartmut Pilch einen Brief an Bitkom, den wir hier ver\"{o}ffentlichen.  Darin erl\"{a}uterte er einen methodologische Konsensposition, die auch den Bitkom-Leuten einzuleuchten schien.
\end{quote}
\filbreak

\item
{\bf {\bf Heise: IT-Verband sieht bei Software-Patenten Nachbesserungsbedarf\footnote{http://www.heise.de/newsticker/data/anw-07.05.02-005/}}}

\begin{quote}
Heise-Bericht \"{u}ber die Bitkom-Erkl\"{a}rung
\end{quote}
\filbreak

\item
{\bf {\bf http://www.bitkom.org/}}
\filbreak

\item
{\bf {\bf Bitkom zu Softwarepatenten: Beitr\"{a}ge zur Bundestags-Anh\"{o}rung 2001-06-21\footnote{http://swpat.ffii.org/dates/2001/bundestag/bitkom/index.de.html}}}

\begin{quote}
Das Referat hielt die Vorsitzende des Arbeitskreises Gewerblicher Rechtschutz, Frau Dr. Katrin Bremer.  Anwesend war auch PA Fritz Teufel von IBM, der diesen Arbeitskreis bis vor kurzem geleitet hatte.  In ihrem Referat fordert Frau Bremer eine z\"{u}gige Legalisierung von Softwarepatenten durch Anpassung von Art 52 EP\"{U} an die Rechtsprechung des Europ\"{a}ischen Patentamtes und meint, die ``Opensource-Bewegung'' werde es \"{u}berleben, da sie innovativ sei.  In der sp\"{a}ter eingereichten offenbar von PA Teufel geschriebenen schriftlichen Eingabe hei{\ss}t es, freie Software sei nicht innovativ und es seien immer die Nachahmer, die das Patentwesen f\"{u}rchteten.
\end{quote}
\filbreak

\item
{\bf {\bf Her Master's Voice --- Dr. iur. Kathrin Bremer\footnote{http://swpat.ffii.org/acteurs/bremer/index.de.html}}}

\begin{quote}
Legal delegate of Bitkom e.V., German software trade association.  Female, born 1969, studied international law and came to Bitkom directly from there in 1999.  Apparently insecure on the issue, unfamiliar with software and even with patent law, lets her predecessor in office, IBM patent lawyer Fritz Teufel ghostwrite most of her contributions to the debate.  These include a participation in a hearing of the German Parliament in 2001-06-21.
\end{quote}
\filbreak

\item
{\bf {\bf PA Fritz Teufel\footnote{http://swpat.ffii.org/acteurs/teufel/index.en.html}}}

\begin{quote}
Patent lawyer, software patentability guru, patent department head of IBM in Germany and Europe, working in Stuttgart, active promoter of software patents, responsible for pushing many landmark cases through the EPO and the German courts.  Ghostwriter of various patent papers of German and European trade associations.  Positions and style well known from public discussions. Hardline advocate of software patentability and very much in love with certain dogmatic fallacies which he successfully used to win over the (already very inclined) EPO and BGH in a series of decisive battles.  Do not expect Teufel to understand the viewpoint of opensource programmers or to come up with solutions to non-juridical problems.  Expect him to stick to EPO fallacies as steadfastly as anyone.  These fallacies constitute his success experience before the lawcourts.
\end{quote}
\filbreak

\item
{\bf {\bf Bitkom zu Softwarepatenten: Beitr\"{a}ge zur Bundestags-Anh\"{o}rung 2001-06-21\footnote{http://swpat.ffii.org/dates/2001/bundestag/bitkom/index.de.html}}}

\begin{quote}
Das Referat hielt die Vorsitzende des Arbeitskreises Gewerblicher Rechtschutz, Frau Dr. Katrin Bremer.  Anwesend war auch PA Fritz Teufel von IBM, der diesen Arbeitskreis bis vor kurzem geleitet hatte.  In ihrem Referat fordert Frau Bremer eine z\"{u}gige Legalisierung von Softwarepatenten durch Anpassung von Art 52 EP\"{U} an die Rechtsprechung des Europ\"{a}ischen Patentamtes und meint, die ``Opensource-Bewegung'' werde es \"{u}berleben, da sie innovativ sei.  In der sp\"{a}ter eingereichten offenbar von PA Teufel geschriebenen schriftlichen Eingabe hei{\ss}t es, freie Software sei nicht innovativ und es seien immer die Nachahmer, die das Patentwesen f\"{u}rchteten.
\end{quote}
\filbreak

\item
{\bf {\bf EICTA statement on patentability of ``computer-implemented inventions''\footnote{http://swpat.ffii.org/papiers/eukonsult00/eicta/index.en.html}}}

\begin{quote}
In the name of the European Information and Communications Technology Industry Association, an anonymous patent lawyer explains that software patents are good for innovation and competition and even helpful and fair for opensource software, because opensource companies can use patents to impose their business model on others.  Moreover the study of Bessen \& Maskin is based on old data and wrong models, the patent expert says.  The style and content of these statements attributed to EICTA indicate that the anonymous patent lawyer is Fritz Teufel, chief patent politician of IBM in Germany and Europe.  A look at the EICTA website, as of 2002-02-24, shows that EICTA has not published this paper nor any other paper on software patents.  The association has evidently not engaged in any discussions on this subject, which so far was not of great concern for the European software industry.
\end{quote}
\filbreak

\item
{\bf {\bf BITKOM: Gesundheitsdebatte beenden, UMTS z\"{u}gig ausbauen! (http://www.heise.de/newsticker/data/ad-12.05.02-000/)}}

\begin{quote}
Ein Bitkom-Sprecher versichert, 20000 Gutachten h\"{a}tten bewiesen, dass Bedenken gegen die UMTS-Strahlen unbelegt seien und warnt vor wirtschaftlichen Sch\"{a}den, welche die anhaltende Gesundheitsdebatte erzeuge.

see also BITKOM ... eine Lachnummer (http://www.heise.de/mobil/newsticker/foren/go.shtml?read=1\&msg\_id=1743513\&forum\_id=29250)
\end{quote}
\filbreak

\item
{\bf {\bf GEMA droht mit Millionenklage wegen Abgabe auf CD-Brenner (http://www.heise.de/newsticker/data/anw-01.03.02-005/)}}

\begin{quote}
Manches mag gegen eine Urheberrechtssteuer auf Drucker und Festplatten sprechen.  Diese Steuer ist jedoch gesetzlich geregelt, und die Argumente mit denen HP und, als deren Sprachrohr, Bitkom sich ihr zu entziehen versuchen, sind weitaus bedenklicher als die Urheberrechtssteuer selber.  Denn Individuelle Verg\"{u}tungsverfahren nach Bitkom-Wunsch \begin{itemize}
\item
sind bislang nirgends in nennenswertem Umfang im Einsatz

\item
f\"{u}hren zu einer Wertminderung dessen, was verg\"{u}tet werden soll

\item
f\"{u}hren ebenso wenig wie andere Verfahren zu ``gerechter Entlohnung von Leistungen''

\item
f\"{u}hren geradeaus zu den Hollywood/Hollings-Gesetzentw\"{u}rfen, gegen die die sich die gesamte amerikanische IT-Welt derzeit auflehnt

\item
f\"{u}hren im Erfolgsfall zu un\"{u}bersehbaren Konfliktszenarien.
\end{itemize}  Wer aufgrund einer Utopie dieser Beschaffenheit im Ton der Entr\"{u}stung gegen die GEMA Sturm laufen zu m\"{u}ssen glaubt, ist wohl einfach nur von allen guten Geistern verlassen.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}\begin{sect}{tasks}{Questions, Choses a faires, Comment vous pouvez aider}
\ifmlhttasks
\begin{itemize}
\item
{\bf {\bf Comment vous pouvez nous aider a terminer le cauchemare des brevets logiciel (http://swpat.ffii.org/groupe/gunka/index.de.html)}}
\filbreak

\item
{\bf {\bf Mit Bitkom-Firmen \"{u}ber Swpat reden}}

\begin{quote}
Die meisten wissen sicherlich nichts von der Stellungnahme ihres Verbandes.  Falls Sie in dieser Hinsicht etwas unternehmen m\"{o}chten, planen Sie dies am besten mit uns.
\end{quote}
\filbreak

\item
{\bf {\bf Mit GEMA etc reden}}

\begin{quote}
Pauschalgeb\"{u}hren haben vielleicht Zukunft, wenn man sich sie im Lichte digitaler Wirklichkeiten neu interpretiert.  Sie sind allemal interessanter als die hausbackene Projektion industrieller Gesch\"{a}ftsmodelle auf die Informations\"{o}konomie.  Vielleicht gibt es bei GEMA, VWG Wort etc Leute, die sich dar\"{u}ber Gedanken machen.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: mlatex ;
% End: ;

