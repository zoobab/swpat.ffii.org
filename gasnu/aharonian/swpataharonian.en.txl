<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Gregory Aharonian and Software Patents: Propagating a Disease, Selling a Medicine

#descr: Editor of a regular newsletter called PatNews, dubbed GregNews, with detailed information especially about software patents which should have failed the novelty test.  Greg offers services of searching novelty destroying documentation.  GregNews strongly opposes any attempt to exclude either algorithms or business methods from patentability.  This is because %(q:information is physical), %(q:the universe is made of algorithms), all distinctions between mind and matter are %(q:unscientific) etc, %(q:as witnessed by Einstein, Turing, ...) and demonstrated by the existence of co-design tools.  Any process which can be described as %(q:technology) or %(q:applied science) must be patentable and not copyrightable, GregNews insists, and the sorrows with software patents would of course go away if only patent offices had more ressources, so as to become as proficient as Greg in patent busting. Some people have however reported that the %(q:tons of prior art) supplied by G.A. regularly fail to achieve their aim of invalidating stupid patents.

#Glw: Greg must be spending most of his time on his gratis news service, which is rich in information and has over the years accumulated a large reader base, especially among patent professionals.  G.A. has been leveraging this reader base as an instrument for promoting the US patent establishment's beliefs in order to influence the European policy debate.  This includes unsubstantiated bashing of famous patent critics such as Lessig, Bessen & Maskin and others.  While the big names may get at least a chance for an answer, the choice of information made accessible by GregNews has over the years become very narrowly subservient to his policy goals, and critics are even denied read-only access.  Public discussions with Greg show that two-way communication is difficult: Greg's argumentation seems to iterate over a specific set of axioms.  Nevertheless GregNews is often witty, and Co-Design Cosmology can be appealing at first sight.  Readers are also unlikely to suspect political manipulation, because they remember Gregory Aharonian as the man who first made people aware of just how bad the USPTO's software patents are.

#DPb: DE-PTO 2002-03-16: Information is Physical, Business Methods Patentable, Program Text not a Disclosure

#Goo: Greg Aharonian publishes in his newsletter a long article on the concept of technical invention which, Greg says, represents the viewpoint of the German Patent and Tradmark Office (DE-PTO).   In a triumphant comment, Greg points out that according to position expressed in this article, all business methods are technical and therefore patentable in Germany and there is no longer a major difference to Greg's doctrines, which are basically those of the US courts.  The DE-PTO article evidently carries the handwriting of DE-PTO's chief software patentability theoretician, Wolfgang Tauchert, but it is not clear where it was published and to what extent it is an official office position.  At least it can be said that at the time of publication, the DE-PTO also quoted Tauchert with similar statements in an official press release.   Some patent laweyrs have been eager to treat this statement as an official DE-PTO position (and implicitely as a source of authority on philosophical questions).  Tauchert's position has also created concern, because it suggests that program texts are not enabling disclosures and therefore apparently not novelty-destroying.  This would mean that people can harvest the ideas from other people's open source programs and obtain patents thereon.

#Sen: Stephan Kinsella in defense of Lessig

#Ioe: In 2002/03 Gregory Aharonian criticised Prof Lawrence Lessig's new book and a few patent lawyers echoed this criticism, adding that Lessig has no patent litigation experience and is therefore not qualified to speak about patent law.  Kinsella, himself a patent lawyer, explains that on the contrary, most of his colleagues have given very little thought to the effects of the patent system but usually pretend that they are experts.

#iat: James Heald takes the time to refute in detail one of Greg Aharonian's attempts to enlist %(q:modern physics) in support of unlimited patentability.

#Ans: Aharonian's site

#trr: try to search for words like Aharonian, co-design, Greg, GregNews

#AeW: Aharonian names some european business method patents and says that it is pointless to try to exclude them patentability and that people should instead rely on prior art to get rid of them.  Defends himself against allegations of Hartmut Pilch that Aharonian's prior art approach has regularly failed to invalidate even very trivial EPO software patents.  see the preceding and ensuing mails in this thread.

#Syr: Some of Greg Aharaonian's typical arguments and counter-arguments by Hartmut Pilch.

#Sap: Sylvain Perchaud et al debate against Greg Aharonian's assertions that any applied science must be patentable.

#con: create an Aharonian Co-Design Cosmology FAQ

#slW: so that we needn't always go through the same old stuff again

#eui: evaluate the aful patents mailing list archives

#cWf: create a table in the form of %(na:this one)

#Fgg: For example, the line %(LINE) will define a symbol %(SYM) as a reference to %(LINK).

#EWr: Extract some nice dialogues in a somewhat structured plain-text, using the above designed document reference symbols, further elaborate the dialogue into a FAQ and submit it to us.

#AVt: Greg's CV, photo etc

#sil: submit more info to complete this page

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpataharonian ;
# txtlang: en ;
# multlin: t ;
# End: ;

