<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bernhard Müller and Software Patents

#descr: Bernhard Müller worked in the European Commission's Industrial Property Unit and was in charge of the software patent directive proposal drafting and consultation activities for several years until March 2001, then moved to the European Trademark Office in Alicante.  Müller conducted %(q:representative surveys) among several dozen patent lawyers in order to conclude that patents on software and business methods as granted by the European Patent Office (EPO) are needed.  Until summer 2000, Müller advocated a %(q:harmonisation) of European patentability rules with those of US and Japan.  Later, during the Consultation of late 2000 which he conducted, Müller presented the same contents with a more cautious packaging, claiming to be aiming at a %(q:restrictive harmonisation of the status quo).  In early 2001 Müller obtained support from governmental officials for a position paper which calls for codification of EPO practise and dismisses the 91% opposition to software patents expressed in the consultation as irrelevant.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbmueller ;
# txtlang: en ;
# multlin: t ;
# End: ;

