\begin{subdocument}{swpatfhg}{Fraunhofer Society as a Bastion of the Patent Movement in Germany}{http://swpat.ffii.org/players/fhg/index.en.html}{Workgroup\\swpatag@ffii.org}{Mit ihren MP3-Patenten hat die Fraunhofer-Gesellschaft ein Vorbild f\"{u}r relativ anspruchsvolle und zugleich lukrative Softwarepatente geschaffen, durch die der Staat bei der Finanzierung von Forschungsinstituten ein wenig entlastet wird.  Dieses Modell ist zwar nicht unproblematisch und auch nicht ohne weiteres beliebig ausweit- und wiederholbar, aber es ist zu einem Erfolgssymbol der Patentbewegung im Hochschulbereich (s. BMBF) geworden.  Die Fraunhofer-Gesellschaft betreibt zugleich eine zentrale Patentstelle f\"{u}r die deutschen Hochschulen, die eine \"{a}hnliche Pilotfunktion aus\"{u}bt.  Das Fraunhofer-Institut f\"{u}r Innovationsforschung verfasst regelm\"{a}{\ss}ig auf Bestellung des BMBF Gutachten, in denen die unfortschrittliche Methodik der Softwarebranche beklagt und die patentorientierte Fraunhofer-Forschung als Hoffnungstr\"{a}ger dargestellt wird.  Ihre Pilotfunktion in der Hochschul-Patentbewegung verleiht den Fraunhofer-Leuten ein starkes Sendungsbewusstsein.}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Fraunhofer Gesellschaft--Selbstdarstellung (http://www.fhg.de/)}}

\begin{quote}
\begin{quote}
...Im Rahmen der Technologieprogramme der Europ\"{a}ischen Union wirkt die Fraunhofer-Gesellschaft in Industriekonsortien an der L\"{o}sung technischer Fragen zur Verbesserung der Wettbewerbsf\"{a}higkeit der europ\"{a}ischen Wirtschaft mit. Im Auftrag und mit F\"{o}rderung durch Ministerien und Beh\"{o}rden des Bundes und der L\"{a}nder werden strategische Forschungsprojekte durchgef\"{u}hrt, die zu Innovationen im Bereich von Schl\"{u}sseltechnologien und im \"{o}ffentlichen Nachfragebereich (Energie, Verkehr, Umwelt) beitragen...
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Fraunhofer Patentstelle (http://www.pst.fhg.de/)}}

\begin{quote}
Zentrale Patentagentur f\"{u}r das deutsche Hochschulwesen und Patentdienstleister f\"{u}r die Wirtschaft
\end{quote}
\filbreak

\item
{\bf {\bf Fraunhofer ISI: Wissens- und Technologietransfer in Deutschland (http://www.isi.fhg.de/ti/Projektbeschreibungen/us-wtt-2000.htm)}}

\begin{quote}
Ank\"{u}ndigung einer Studie des Fraunhofer Instituts f\"{u}r Innovationsforschung (ISI), in welchen die Mechanismen des ``Technologietransfers'' zwischen Wissenschaft und Hochschulen beschrieben und Probleme aufgezeigt werden sollen.
\end{quote}
\filbreak

\item
{\bf {\bf ISI-Projektbeschreibung Softwarepatente ()}}

\begin{quote}
Das BMWi erteilte dem Fraunhofer ISI einen Auftrag zur Erstellung einer Studie \"{u}ber die Auswirkungen von Softwarepatenten auf das Kalk\"{u}l der betroffenen Unternehmen.  In der Projektbeschreibung werden die Vorz\"{u}ge von Patenten wortreich argumentativ begr\"{u}ndet und die Einw\"{a}nde dagegen nur kurz und ohne Argument erw\"{a}hnt.
\end{quote}
\filbreak

\item
{\bf {\bf Kritik an ISI-Projektbeschreibung (http://lists.ffii.org/archive/mails/swpat/2001/May/0094.html)}}

\begin{quote}
Besprechung des obigen Textes im Diskussionsforum swpat@ffii.org
\end{quote}
\filbreak

\item
{\bf {\bf ISI-Untersuchung \"{u}ber den Zustand der Softwarebranche (http://www.dlr.de/IT/IV/Studien/evasoft\_abschlussbericht.pdf)}}

\begin{quote}
Die Untersuchung des Fraunhofer-Institutes beklagt, wie unsystematisch und eklektisch die Softwareunternehmen vorgehen und empfiehlt eine Politik des ``Technologietransfers'' zwischen diesen Unternehmen und fortschrittlichen Fortschungseinrichtungen wie Fraunhofer.  Um die Organisation solcher Kooperationen zu erleichtern und eine ingenieurm\"{a}{\ss}ige Ausrichtung der Softwarebranche zu f\"{o}rdern, bedarf es laut ISI der Klarstellung, dass Computerprogramme patentierbar sein m\"{u}ssen.  Diese Schlussfolgerung scheint eine Art Standard-Topos bei Fraunhofer-Gutachten zu sein, die im Auftrag des BMBF erstellt werden.
\end{quote}
\filbreak

\item
{\bf {\bf Heise News-Ticker: Software-Entwicklung: Kampf den Altlasten (http://www.heise.de/newsticker/data/prak-03.01.01-001/)}}

\begin{quote}
Die Heise-Journalisten sp\"{o}tteln \"{u}ber das ISI-Gutachten
\end{quote}
\filbreak

\item
{\bf {\bf Heise News-Ticker: Blinder Fleck Open Source: Software-Studie unter Beschuss (http://www.heise.de/newsticker/data/prak-05.01.01-000/)}}

\begin{quote}
Der Linuxtag e.V. bem\"{a}ngelt, dass das ISI-Gutachten kein Wort \"{u}ber die Bedeutung der freien/quelloffenen Softwareentwicklung verliert.
\end{quote}
\filbreak

\item
{\bf {\bf FFII-Diskussion: Fraunhofer-Studie fordert Swpat (http://lists.ffii.org/archive/mails/swpat/2001/Jan/0076.html)}}

\begin{quote}
Bericht und Diskussion \"{u}ber das ISI-Gutachten auf dem Verteiler swpat@ffii.org
\end{quote}
\filbreak

\item
{\bf {\bf Fraunhofer TI br\"{u}stet sich mit Trivialpatent (http://swpat.ffii.org/patents/samples/de19838253/index.en.html)}}

\begin{quote}
In order to better equip an intranet against cracker attacks, the firewall is always physically cut off on one of its two sides, and two-way communication is maintained by rapid alternation.  The patentee asserts that this principle can make a network absolutely secure, whereas current firewalls allow only relative security.  While this assertion seems questionable, the fact is that anybody who does the hard work of putting this obvious functionality to work will have to beg for a license from Fraunhofer.
\end{quote}
\filbreak

\item
{\bf {\bf MPEG-related patents on compression of acoustic data\footnote{http://swpat.ffii.org/patents/effects/mpeg/index.en.html}}}

\begin{quote}
Acoustic compression requires knowledge of auditive perception, which had to be acquired through experimentation.  Thus this field is close to the borderline of technical inventions which could be patentable.  Yet most of the research results were published in the 60s and 70s, and the patented processes based thereon are pure informational processes, some of them quite basic and trivial, when viewed against the background of available theoretical knowledge.  The whole field of audio compression is cluttered with dozens of basic patents, thus making it very difficult to develop alternatives.  Ogg Vorbis seems to have succeeded in developping patent-free audio compression, but is being threatened by the patent holders, who have formed various consortia such as MP3 and MPEG2.  In order to develop free software for MP3, one must pay an upfront payment of 1 million USD.  Otherwise money must be charged per copy, thus barring the possibility of opensource development.  Moreover, recently MPEG-LA, a consortium of MPEG patent holders, also proposed charging fees from content producers.
\end{quote}
\filbreak

\item
{\bf {\bf The Universitarian Patent Movement\footnote{http://swpat.ffii.org/players/bmbf/index.en.html}}}

\begin{quote}
Hundreds of universitarian ``technology transfer agencies'' are putting brakes on the diffusion of technology with help of patents.  The taxpayer finances the creation of monopolised knowledge goods and then later pays about 10 times as much for the monopoly costs as the university can hope to gain from patent royalties.  An atmosphere of tension between researchers and patent bureaucrats is created.  The macro-economic reasons for the existence of a public research sector are neglected and a second-rate competition to industrial research is instead created and alimented by the taxpayer.  Some day in the future this may save the state some money, says the lobby that gets the newly created posts in the patent bureaucracy.  This lobby has its own governmental agencies on its side, of which the best known one in Germany is the ``Patent Offensive'' in the Ministery of Education and Research.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: mlatex ;
% End: ;

