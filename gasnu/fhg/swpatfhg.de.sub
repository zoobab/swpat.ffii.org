\begin{subdocument}{swpatfhg}{Die Fraunhofer-Gesellschaft als Bastion der Patentbewegung}{http://swpat.ffii.org/gasnu/fhg/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2004/12/03 von FFII\footnote{\url{http://lists.ffii.org/mailman/listinfo/traduk}}}{Mit ihren MP3-Patenten hat die Fraunhofer-Gesellschaft ein Vorbild f\"{u}r relativ anspruchsvolle und zugleich lukrative Softwarepatente geschaffen, durch die der Staat bei der Finanzierung von Forschungsinstituten ein wenig entlastet wird.  Dieses Modell ist zwar nicht unproblematisch und auch nicht ohne weiteres beliebig ausweit- und wiederholbar, aber es ist zu einem Erfolgssymbol der Patentbewegung im Hochschulbereich (s. BMBF) geworden.  Die Fraunhofer-Gesellschaft betreibt zugleich eine zentrale Patentstelle f\"{u}r die deutschen Hochschulen, die eine \"{a}hnliche Pilotfunktion aus\"{u}bt.  Das Fraunhofer-Institut f\"{u}r Innovationsforschung verfasst regelm\"{a}{\ss}ig auf Bestellung des BMBF Gutachten, in denen die unfortschrittliche Methodik der Softwarebranche beklagt und die patentorientierte Fraunhofer-Forschung als Hoffnungstr\"{a}ger dargestellt wird.  Ihre Pilotfunktion in der Hochschul-Patentbewegung verleiht den Fraunhofer-Leuten ein starkes Sendungsbewusstsein.}
\begin{itemize}
\item
{\bf {\bf Fraunhofer Gesellschaft--Selbstdarstellung\footnote{\url{http://www.fhg.de/}}}}

\begin{quote}
\begin{quote}
...Im Rahmen der Technologieprogramme der Europ\"{a}ischen Union wirkt die Fraunhofer-Gesellschaft in Industriekonsortien an der L\"{o}sung technischer Fragen zur Verbesserung der Wettbewerbsf\"{a}higkeit der europ\"{a}ischen Wirtschaft mit. Im Auftrag und mit F\"{o}rderung durch Ministerien und Beh\"{o}rden des Bundes und der L\"{a}nder werden strategische Forschungsprojekte durchgef\"{u}hrt, die zu Innovationen im Bereich von Schl\"{u}sseltechnologien und im \"{o}ffentlichen Nachfragebereich (Energie, Verkehr, Umwelt) beitragen...
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Fraunhofer Patentstelle\footnote{\url{http://www.pst.fhg.de/}}}}

\begin{quote}
Zentrale Patentagentur f\"{u}r das deutsche Hochschulwesen und Patentdienstleister f\"{u}r die Wirtschaft
\end{quote}
\filbreak

\item
{\bf {\bf Fraunhofer ISI: Wissens- und Technologietransfer in Deutschland\footnote{\url{http://www.isi.fhg.de/ti/Projektbeschreibungen/us-wtt-2000.htm}}}}

\begin{quote}
Ank\"{u}ndigung einer Studie des Fraunhofer Instituts f\"{u}r Innovationsforschung (ISI), in welchen die Mechanismen des ``Technologietransfers'' zwischen Wissenschaft und Hochschulen beschrieben und Probleme aufgezeigt werden sollen.
\end{quote}
\filbreak

\item
{\bf {\bf ISI-Projektbeschreibung Softwarepatente\footnote{\url{}}}}

\begin{quote}
Das BMWi erteilte dem Fraunhofer ISI einen Auftrag zur Erstellung einer Studie \"{u}ber die Auswirkungen von Softwarepatenten auf das Kalk\"{u}l der betroffenen Unternehmen.  In der Projektbeschreibung werden die Vorz\"{u}ge von Patenten wortreich argumentativ begr\"{u}ndet und die Einw\"{a}nde dagegen nur kurz und ohne Argument erw\"{a}hnt.
\end{quote}
\filbreak

\item
{\bf {\bf Kritik an ISI-Projektbeschreibung\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/May/0094.html}}}}

\begin{quote}
Besprechung des obigen Textes im Diskussionsforum swpat@ffii.org
\end{quote}
\filbreak

\item
{\bf {\bf \url{http://www.dlr.de/IT/IV/Studien/evasoft_abschlussbericht.pdf}}}

\begin{quote}
Die Untersuchung des Fraunhofer-Institutes beklagt, wie unsystematisch und eklektisch die Softwareunternehmen vorgehen und empfiehlt eine Politik des ``Technologietransfers'' zwischen diesen Unternehmen und fortschrittlichen Fortschungseinrichtungen wie Fraunhofer.  Um die Organisation solcher Kooperationen zu erleichtern und eine ingenieurm\"{a}{\ss}ige Ausrichtung der Softwarebranche zu f\"{o}rdern, bedarf es laut ISI der Klarstellung, dass Computerprogramme patentierbar sein m\"{u}ssen.  Diese Schlussfolgerung scheint eine Art Standard-Topos bei Fraunhofer-Gutachten zu sein, die im Auftrag des BMBF erstellt werden.
\end{quote}
\filbreak

\item
{\bf {\bf \url{http://www.heise.de/newsticker/data/prak-03.01.01-001/}}}

\begin{quote}
Die Heise-Journalisten sp\"{o}tteln \"{u}ber das ISI-Gutachten
\end{quote}
\filbreak

\item
{\bf {\bf \url{http://www.heise.de/newsticker/data/prak-05.01.01-000/}}}

\begin{quote}
Der Linuxtag e.V. bem\"{a}ngelt, dass das ISI-Gutachten kein Wort \"{u}ber die Bedeutung der freien/quelloffenen Softwareentwicklung verliert.
\end{quote}
\filbreak

\item
{\bf {\bf FFII-Diskussion: Fraunhofer-Studie fordert Swpat\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/Jan/0076.html}}}}

\begin{quote}
Bericht und Diskussion \"{u}ber das ISI-Gutachten auf dem Verteiler swpat@ffii.org
\end{quote}
\filbreak

\item
{\bf {\bf Fraunhofer TI br\"{u}stet sich mit Trivialpatent\footnote{\url{http://localhost/swpat/pikta/mupli/de19838253/index.de.html}}}}

\begin{quote}
Um ein Intranet besser gegen Eindringversuche zu wappnen, wird der Schutzwallrechner (firewall) abwechselnd mal von der einen und dann von der anderen Seite physisch abgetrennt.  Die Patentinhaberin behauptet, hierdurch werde ein wesentlicher Fortschritt in der Netzwerksicherheit erzielt.  Tatsache ist, dass jeder, der diese naheliegende Funktionalit\"{a}t wirklich zum Funktionieren bringen m\"{o}chte, bei Fraunhofer um Erlaubnis bitten muss.
\end{quote}
\filbreak

\item
{\bf {\bf MPEG-related patents on compression of acoustic data\footnote{\url{http://localhost/swpat/pikta/xrani/mpeg/index.de.html}}}}

\begin{quote}
Akustische Kompression erfordert Kenntnisse der Geh\"{o}rpsychologie, die auf Experimenten beruhen.  Diese Verfahren liegen insofern nahe an dem Gebiet der patentierbaren Erfindungen nach klassischem Technikverst\"{a}ndnis.  Allerdings wurden die meisten Forschungsergebnisse bereits l\"{a}ngst vor der Patentierung ver\"{o}ffentlicht, so dass wir es doch mit reinen Softwarepatenten zu tun haben, die trivial erscheinen, wenn man sie vor dem Hintergrund des ver\"{o}ffentlichten theoretischen Wissens betrachtet.  Das gesamte Gebiet der Audiokompression ist von Dutzenden von grundlegenden Patenten zugemauert.  Der Entwicklergruppe um Ogg Vorbis ist es offenbar gelungen, eine patentfreie Alternative zu entwickeln, aber sie wird dennoch von den Konsortien der Patentinhaber bedroht.  Um eine Lizenz zur Ver\"{o}ffentlichung von MP3- oder MPEG2-Software zu erhalten, muss man eine Pauschalsumme von 1 Million USD anzahlen.  Andernfalls kommt nur die Ver\"{o}ffentlichung als propriet\"{a}re Software mit genauer Distributionskontrolle und Geldeinzug pro Kopie in Frage.
\end{quote}
\filbreak

\item
{\bf {\bf Die Hochschul-Patentbewegung\footnote{\url{http://localhost/swpat/gasnu/bmbf/index.de.html}}}}

\begin{quote}
Hunderte von universit\"{a}ren ``Technologietransfer-Stellen'' besch\"{a}ftigen sich hauptamtlich damit, die Verbreitung von Technologien mithilfe von Patenten zu bremsen.  Der Steuerzahler finanziert die Erschlie{\ss}ung und sofortige Monopolisierung neuer (und alter) Wissensg\"{u}ter.  Das BMBF bemisst den Erfolg seiner Investitionen nicht an den erzeugten \"{o}ffentlichen G\"{u}tern sondern umgekehrt an den der \"{O}ffentlichkeit entzogenen G\"{u}tern.  F\"{u}r die Patentgeb\"{u}hren zahlt der Steuerzahler sp\"{a}ter ggf noch einmal 10 mal so viel drauf, wie die Universit\"{a}t (bei effizienter F\"{u}hrung vielleicht irgendwann einmal) daran verdient.  In den so geschaffenen Patentfabriken bestimmt eine staatlich gef\"{o}rderte Verwertungsb\"{u}rokratie die Richtung der Forschungspolitik.  Den guten volkswirtschaftlichen Gr\"{u}nden f\"{u}r die Existenz eines \"{o}ffentlichen Bildungs- und Forschungssektors wird offenbar kaum noch Rechnung getragen.  Stattdessen entsteht eine zweitklassige Konkurrenz zur industriellen Forschung.  Irgendwann in der Zukunft wird hierdurch vielleicht einmal die Staatskasse entlastet.  Das versprechen zumindest die ``Technologietransferbeamten'', die in dem neuen System die Posten bekleiden.  Dieser Gruppe stehen in diversen Regierungen auf regionaler, nationaler und supranationaler Ebene steuerlich finanzierte Agenturen zur Seite, von denen insbesonere die ``Patentoffensive'' im Bundesministerium f\"{u}r Bildung und Forschung von sich reden macht.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

