<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Portugal and Software Patents

#descr: Portugal, a nation whose citizens own none of the 30,000 software
patents granted by the EPO as of summer 2003, has formed a
%(q:national consensus) in favor of software patents and of the EU
directive proposal.  This consensus was reached by a consultation
exercise conducted by the Portuguese Patent Office (INPI) in 2001. 
Back then the INPI sent a pro-software-patent letter to 33 senior
corporate executives, and received 6 responses, all of which applaud
the INPI viewpoint.  In summer 2003 MEP Ilda Figueiredo conducted
another hearing which showed a large consensus of portuguese software
companies against software patents.  Impressed by this, INPI launched
a second round of consultations.

#Wia: Source of Portuguese Patent Policy: INPI.pt -- the Patent Office

#lpW: INPI pro-patent consultations of 2001 / 2002

#kon: %(q:Re-Opening) of Consultations in 2003-07

#or1: Extensive document, contains among others %(ol|replies of 3 companies
who responded to a consultation call on software patents by the
Portuguese Patent Office in early 2001 and a new call for
consultation.|%(q:Portugues position), according to which, in addition
to the European Commission's proposal, direct claims to %(q:computer
program characterised by that ....) must be legalised, so that
programmers can be sued as soon as they publish a program on the
Internet.)

#rWa: INPI presents a series pro-software-patent viewpoints from the
European Commission and the European Parliament's Legal Affairs
Commission and calls on companies to express their views.  The call is
rather hidden inside the INPI web site.

#ffW: Portuguese Patent Office.  As in most other countries, the patent
office has been determining patent policies and speaking for the
government on these matters in Portugal.

#eiW: A letter sent by INPI.pt to 19 senior corporate executives. This
letter says the European Comission is conducting a %(ek:consulation)
regarding software and business methods patents and that the INPI has
sketched a %(i6:first reply to this consultation) and asks these
people for their comments on this non final document.  This letter was
written on 2001-01-11, 4 weeks after expiry of deadline, signed by
Jaime Andrez on behalf of INPI and of Portugal as a whole.

#Gda: Tentative response of INPI.pt to consultation call of
CEC/DGIM/Indprop, written on 2001-01-16, 1 month after expiry of
deadline.  Approvingly recites European Patent Office (EPO) caselaw
and positions agreed upon at the Trilateral Conference of June 2000 by
EPO, USPTO and JPO.  In a letter to 19 senior corporate executives,
INPI.pt asked for endorsement of this position.  Says that SMEs and
individuals particularly need patent protection and that patents
provide just recompense for innovation, without which people wouldn't
invest in technological development in any field.

#tii: In a letter of 2002-10-04 on p. 15, the Portuguese Patent Office
explains how the Council Working Party arrived at the decision to ask
for direct patentability of software.

#eom: In view of the need to protect inventions in this field by patents,
Portugal drew attention to the enhanced value of protection that could
be achieved if this protection was extended to %(q:programs on
computer media).  This is because protection of a program on computer
media would make it possible for the patent owner to directly sue for
patent infringement anyone who sells or uses without permission of the
patent owner a program covered by the program claim.   Most recently,
the German delegation presented an amendment to article 5 so that
patent protection would be extended to %(q:computer programs on
computer media), and as a first reaction the Portuguese delegation
supported this amendment.

#Nne: The second letter which INPI sent to 33 companies asks for their
opinions on this specific subject. 2 companies replied saying it is a
good idea.

#ibt: INPI represented Portugal in the %(q:Patent Working Party) of the
European Council and basically supported what the other governments,
also represented by their patent offices, decided.

#bWp: The EU Consultation, based on a pro-software-patent paper and
suggestive questions, yet resulting in 91% of participants opposing
software patentability.

#Wso: INPI says that in 2001 they contacted the following 19 persons of
their choice (mostly senior corporate executives):

#ait: Name + Title

#hWb: Phone number

#ona: Company Name

#oii: Position

#mWe: Company address

#cnW: The original contacting letter reads (translated from Portuguese):

#ian: Excluding computer programs from being patentable as in article 52 of
the Munich Convention and consequentially in every member state
legislation gives the wrong impression that it is not possible to
obtain patent protection for inventions in the domain of computer
programs which demotivates many independent programmers and SMEs who
are not familiarised with patent law

#Ior: INPI, 16th of January 2001

#ogu: no signature

#fee: Of the 19 people contacted, only 3 responded.  These %(ic:3 replies)
approved of INPI's position.  The 3 respondents were:

#oem: Rogério Carapuça, manager of Novabase

#sno: Novabase owns 51% of %(OT), the company who implemented the first set
top boxes for %(s:Microsoft)

#Wdl: Someone (probably Valério Marques) from %(QD). Quadriga (a small
company doing WAP applications) It is associated with TMN (mobile
phone company) which belongs to %(PT). PT is a Portuguese old phone
company, used to be owned by the government and had a complete
monopoly of communications for decades. Upon privatisation, it became
a close ally of Microsoft, with plenty of strategic agreements (from
browser wars to interactive television).

#wyi: Shows plenty of alliances.

#toE: This announcement of a major Strategic Alliance with a large
investment of MS in PT, written from the viewpoint of MS, was widely
distributed in English and Portuguese by PT.

#Wen: Someone from Timesharing, an old company from the timesharing computer
age. This company has been bought by Portugal Telecom and its name has
been changed to %(PC). Its main activity is now telemarketing.

#lTd: On 2002/10/04 INPI sent out a new letter asking for support for their
position regarding the proposed directive. This letter was sent to
senior executives of 33 companies. Two companies replied, supporting
INPI's position: SOL-S and Sonae.

#nti: SOL-S reply is signed by António Ramos Costa, CEO and Chairman of the
Board. The SOL-S group recently integrated Solsuni. Solsuni was
founded in 1992 as the only distributor for Sun Microsystems in
Portugal.

#rju: Sonae's reply is signed by Jorge Marques dos Santos. He was an
assistant of the board. At the time, Jorge Marques dos Santos was also
the president of the APED (Portuguese Distribution Companies
Association).

#rah: This hearing was attended by numerous people from SMEs and produced a
100% consensus against software patents and against the proposed
directive.  Isabel Alfonso from INPI was impressed and promised to
%(q:re-open consultations).

#hno: In late July the consultation was re-opened by an inconspicuous
consultation call on the INPI website.

#tli: Below we cite the INPI consultation call with some comments and links
added by us.

#NfP: PATENTABILITY of Computer Programs

#pns: Computer programs as such are excluded from patentability by the
patent law of Member States and by the %(ec:European Patent
Convention), but the %(ie:European Patent Office) (EPO) and national
patent offices have granted thousands of patents for inventions
implying computer programs. Just the EPO by itself has granted
%(ep:over 20'000 such patents).

#ose: It is this problem regarding the patentability of inventions implying
computer programs that the %(sp:Directive) of the European Parliament
and of the %(dk:Council) wants to solve. The directive aims to promote
an increased legal certainty in the domain of inventions implying
computer programs, harmonizing national legislations regarding this
subject and making patentability requirements more transparent.

#Wso2: The process for constructing this directive has integrated consulting
initiatives to the interested parties by the %(ek:Commission) and also
by the Member States. INPI, the Portuguese organisation for industrial
property was also part of this consultation effort. On the INPI site a
discussion fórum has taken place and two letters have been written to
software companies based in Portugal, asking them for comments that
were to be used on the technical discussions for the directive that
were done at the time. INPI has received replies from five companies
from a total of 33 to whom the letters have been sent. The replies
indicated an agreement with the text %(q:Comentários à carta da
Comissão Europeia sobre a patenteabilidade dos programas de
computador) (commentaries to the letter from the European Commission
about the patentability of computer programs) as well as an agreement
with the %(p5:Portuguese position on article 5 of the directive
proposal).

#glC: The %(q:portuguese position on article 5) seems to be that direct
patent claims of the form %(q:a program, characterised by that upon
loading it into memory [some computing process] is executed) should be
allowed.  The European Commission had proposed that they should not be
allowed.

#ltW: The European Parliament voting on a %(tm:modified directive text) will
most likely take place on the 1st of September 2003.

#eoo: There are two radically opposed positions in confrontation:

#won: from one side established software companies and also those who see
the patent system more as a set of opportunities than as a set of
threats, who want to continue counting on it for their positioning
strategies.

#nWl: None of the more than 30000 software patents granted by the European
Patent Office against the letter and spirit of the written law are
owned by portuguese companies or individuals.  Even in countries where
more people own software patents, the software industry at large is
not using the patent system to a significant extent yet.  It is
factually wrong to speak about %(q:continuation) of patenting
strategies, unless by %(q:established software companies) INPI means
only a few very large players such as %(LST).

#cty: on the other side the defenders of free software who consider the
patent system inadequate to protect inventions related to computer
programs and who fear this Directive because they consider it will
increase the use of software patents; they also fear that their
programming activity will be permanently inhibited by the Damocles
sword of the patents on software algorithms.

#lat: At the %(pt:Porto hearing of 2003-07-15) several representatives of
software companies who write proprietary software were present and
opposed software patentability.  INPI als has been consistently
failing to mention copyright.  They are creating the wrong impression
that the discussion is about whether all software should be free.

#die: To gather more data for this discussion and to allow the adoption of
fairer and more balanced legislation that promotes Europe's
development INPI has a great pleasure in reopening the Forum on this
matter.

#Wli: ALL COMMENTARIES MAY BE SENT TO THE E-MAIL ablanch@inpi.pt and they
will be published on the site as soon as possible.

#min: To help with the commentaries to be sent to this Forum the most
important documents related to this discussion have been made
available.

#WNh: Documents related to the the INPI consultation to the companies

#thn: Summary of the consultation made by the European Union to the
interested parties

#rPW: Initial version of the Directive Proposal on the 20th of February 2002

#fen: Final version of the Directive Proposal that will be submitted to the
European Parliament on September the 1st

#idgecho: Uncritical Portuguese Echo of IDG News Service

#publico030623d: An article which parrots misinformation from Paul Meller of IDG,
claiming that the JURI directive proposal would prevent business
method patents such as Amazon One Click.

#rha: One of several well-written articles which appeared in this journal in
summer 2003.

#iom: This includes a Portuguese pamphlet, distributable especially to
Portuguese MEPs.

#oqg: Multicert, a portuguese company, says it has acquired a patent on a
method of digital timestamping.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatpt ;
# txtlang: en ;
# multlin: t ;
# End: ;

