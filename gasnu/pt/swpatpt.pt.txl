<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Portugal e as Patentes de Software

#descr: Portugal, uma nação em que nenhum dos cidadãos tem nenhuma das 30.000
patentes de software atribuídas pelo EPO até ao Verão de 2003, formou
um %(q:consenso nacional) a favor das patentes de software e da
proposta de directiva. Este consenso foi atingido através de uma
consulta conduzida pelo Instituto Nacional da Propriedade Industrial
(INPI) em 2001. Nessa altura o INPI enviou uma carta a favor das
patentes de software para 19 directores de empresas e recebeu 3
respostas. Todas aplaudem o ponto de vista do INPI. No Verão de 2003 a
eurodeputada Ilda Figueiredo organizou um encontro que mostrou um
largo consenso de empresas portuguesas de software contra as patentes
de software. Impressionado por tal facto, a representante do INPI
prometeu %(q:reabrir a consulta pública).

#Wia: Fonte da Política Portuguesa de Patentes: INPI.pt -- Instituto
Nacional da Propriedade Industrial

#lpW: Consulta pro-patentes do INPI de 2001 / 2002

#kon: %(q:Reabertura) da Consulta em 2003-07

#or1: Extensive document, contains among others %(ol|replies of 3 companies
who responded to a consultation call on software patents by the
Portuguese Patent Office in early 2001 and a new call for
consultation.|%(q:Portugues position), according to which, in addition
to the European Commission's proposal, direct claims to %(q:computer
program characterised by that ....) must be legalised, so that
programmers can be sued as soon as they publish a program on the
Internet.)

#rWa: INPI presents a series pro-software-patent viewpoints from the
European Commission and the European Parliament's Legal Affairs
Commission and calls on companies to express their views.  The call is
rather hidden inside the INPI web site.

#ffW: Instituto Nacional de Propriedade Industrial, gabinete de patentes. 
Como na maioria dos outros países, o gabinete de patentes tem
determinado a política sobre patentes e falado em nome do governo
sobre estes assuntos em Portugal.

#eiW: Uma carta enviada pelo INPI.pt a 19 executivos séniors. Esta carta diz
que a Comissão Europeia conduz uma %(ek:consulta) a respeito de
patentes de software e patentes de modelos de negócio e em que o INPI
rascunhou uma %(i6:primeira resposta à consulta) e pede a estas
pessoas os seus comentários sobre este documento não final. Esta carta
foi escrita em 2001-01-11, 4 semanas após ter expirado o prazo,
assinada por Jaime Andrez em nome do INPI e de Portugal.

#Gda: Tentativa de resposta do INPI.pt à consulta do CEC/DGIM/Indprop,
escrita em 2001-01-16, um mês após ter expirado o prazo. Aparentemente
recita a jurisprudência do Gabinete Europeu de Patentes (EPO) e as
posições acordadas na Conferência Trilateral de Junho de 2000 pelo
EPO, USPTO (Gabinete Estado-unidense de patentes) e JPO (Gabinete
Japonês de Patentes). Numa carta a 19 executivos séniors, o INPI.pt
pede o seu apoio a esta posição.

#tii: In a letter of 2002-10-04 on p. 15, the Portuguese Patent Office
explains how the Council Working Party arrived at the decision to ask
for direct patentability of software.

#eom: Portugal, pensando na necessidade de proteger por patente as invenções
neste domínio, chamou a atenção para o aumento do valor da protecção
se esta fosse estendida a %(q:programas em suportes informáticos). 
Isto porque uma protecção outorgada a um programa num suporte
permitirá reagir, invocando violação de direito de patente
directamente contra qualquer entitade que venda ou utilize, sem
autorização do titular, programas patenteados em suportes.  Mais
recentemente, a delegação almemã apresentou uma proposta de alteração
do referido artigo 5° que vai no sentido de também incluir, na
Directiva, a protecção dos programas de computador em suportes,
proposta esta que a delegação portuguesa, como primeira reacção,
apoiou.

#Nne: The second letter which INPI sent to 33 companies asks for their
opinions on this specific subject. 2 companies replied saying it is a
good idea.

#ibt: O INPI representou Portugal no %(q:Grupo de Trabalho sobre Patentes)
do Concelho Europeu e basicamente suportou o que os outros governos,
também representados pelos seus gabinetes de patentes, decidiram.

#bWp: The EU Consultation, based on a pro-software-patent paper and
suggestive questions, yet resulting in 91% of participants opposing
software patentability.

#Wso: O INPI diz que em 2001 eles contactaram as seguintes 19 pessoas da sua
escolha (essencialmente executivos séniors):

#ait: Nome + Título

#hWb: Número de telefone

#ona: Nome da companhia

#oii: Posição

#mWe: Morada da companhia

#cnW: A carta de contacto original diz:

#ian: Com efeito a exclusão da patenteabilidade dos programas de computador
enunciada no artigo 52º da Convenção de Munique e em consequência nas
diferentes legislações dos Estados-membros, dá uma impressão errada de
que não é possível obter protecção por patente para invenções no
domínio dos programas de computador, o que desmotiva muitos
programadores independentes e PME's, pouco familiarizados com o
direito de patentes.

#Ior: INPI, 16 de Janeiro de 2001

#ogu: sem assinatura

#fee: Das 19 pessoas que responderam, apenas 3 respondera. Estas 3 respostas
foram favoráveis a patentes de software. Os 3 que responderam foram:

#oem: Rogério Carapuça, gerente da Novabase

#sno: A Novabase detêm 51% da %(OT), a companhia que implementou as
primeiras set top boxes para a %(s:Microsoft)

#Wdl: Alguém (Provavelmente Valério Marques) da %(QD). Quadriga (uma pequena
empresa que faz aplicações WAP) é associada da TMN, que pertence à
%(PT). A PT é uma companhia de telefones velha, habituada à gestão
governamental e teve um monopólio completo das comunicações durante
décadas. Após a privatização, tornou-se um aliado próximo da
Microsoft, com muitos acordos estratégicos (desde as guerras dos
browsers até à televisão interactiva).

#wyi: Mostra muitas alianças.

#toE: Este anúncio de uma enorme Aliança Estratégica com grande investimento
da MS na PT, escrito do ponto de vista da MS, foi vastamente
distribuido em inglês e português pela PT.

#Wen: Alguém da Timesharing, uma companhia velha dos tempos dos computadores
em timesharing. Esta companhia foi comprada pela Portugal Telecom e o
seu nome mudou para %(PC). A sua principal actividade é o
telemarketing.

#lTd: A 2002/10/04 o INPI enviou uma nova carta a pedir apoio para a sua
posição em relação à directiva. Esta carta foi enviada para a direcção
de 33 empresas. Duas responderam, suportando a posição do INPI: SOL-S
e Sonae.

#nti: A resposta da SOL-S foi assinada por António Ramos Costa, presidente
do conselho de administração. O Grupo SOL-S integrou recentemente a
Solsuni. A Solsuni foi fundada em 1992 como o único distribuidor da
Sun Microsystems em Portugal.

#rju: A resposta da Sonae SGPS foi assinada por Jorge Marques dos Santos.
Ele era um assistente da administração. Nessa altura, Jorge Marques
dos Santos era, também, presidente da APED (Associação Portuguesa das
Empresas de Distribuição).

#rah: Esta audição teve a participação de várias pessoas de PMEs e produziu
um consenso de 100% contra patentes de software e contra a proposta de
directiva. Isabel Alfonso do INPI ficou impressionada e prometeu
%(q:reabrir a consulta).

#hno: In late July the consultation was re-opened by an inconspicuous
consultation call on the INPI website.

#tli: Below we cite the INPI consultation call with some comments and links
added by us.

#NfP: PATENTEABILIDADE DE PROGRAMAS DE COMPUTADOR

#pns: Os programas de computador enquanto tais estão excluídos da
patenteabilidade pelas leis em matéria de patentes dos Estados-Membros
e pela %(ec:Convenção sobre a Patente Europeia), mas o %(ie:Instituto
Europeu de Patentes) e os organismos nacionais de patentes concederam
milhares de patentes a inventos que implicam programas de computador.
Só o Instituto Europeu de Patentes concedeu %(ep:mais de 20.000).

#ose: É nestes termos que vem definido o pano de fundo do problema que a
%(sp:Directiva) do Parlamento Europeu e do %(dk:Conselho) relativa à
patenteabilidade dos inventos que implicam programas de computador)
pretende resolver. Ela pretende promover uma maior certeza jurídica no
domínio dos inventos que implicam programas de computador,
harmonizando as legislações nacionais sobre patentes no que diz
respeito à patenteabilidade dos inventos que implicam programas de
computador e tornando mais transparentes as condições de
patenteabilidade.

#Wso2: O processo de construção da Directiva tem integrado iniciativas de
consulta dos meios interessados, protagonizadas quer pela
%(ek:Comissão) quer pelos Estados-Membros. O INPI, organismo nacional
de propriedade industrial em Portugal fez também parte deste esforço.
Teve lugar no site do INPI um fórum de discussão e foram escritas duas
cartas dirigidas a empresas de software domiciliadas em Portugal,
pedindo-lhes comentários para que pudessem ser usados nas discussões
técnicas da Directiva que entretanto tiveram lugar. Foram recebidas
respostas por parte de cinco empresas, das 33 a quem as cartas foram
dirigidas. As respostas indicavam a concordância quer com o texto
Comentários à carta da Comissão Europeia sobre a patenteabilidade dos
programas de computador, de 16 de Janeiro de 2001, produzido pelo
INPI, quer com a posição assumida por Portugal no âmbito das
discussões %(p5:sobre o teor do artigo 5º) da proposta de Directiva.

#glC: The %(q:portuguese position on article 5) seems to be that direct
patent claims of the form %(q:a program, characterised by that upon
loading it into memory [some computing process] is executed) should be
allowed.  The European Commission had proposed that they should not be
allowed.

#ltW: Prevê-se para 1 de Setembro de 2003, a votação no Parlamento Europeu
de um %(tm:texto modificado) de Directiva.

#eoo: Em confronto estão duas posições radicalmente opostas:

#won: de um lado empresas de software com posições de mercado definidas e
aqueles que vêem no sistema de patentes mais um conjunto de
oportunidades que um conjunto de ameaças, e que pretendem poder
continuar a contar com ele para as suas estratégias de posicionamento;

#nWl: None of the more than 30000 software patents granted by the European
Patent Office against the letter and spirit of the written law are
owned by portuguese companies or individuals.  Even in countries where
more people own software patents, the software industry at large is
not using the patent system to a significant extent yet.  It is
factually wrong to speak about %(q:continuation) of patenting
strategies, unless by %(q:established software companies) INPI means
only a few very large players such as %(LST).

#cty: do outro lado os defensores do software livre que consideram o sistema
de patentes inadequado para proteger invenções relacionadas com
programas de computador os quais receiam esta Directiva porque
consideram que ela vem fomentar ainda mais o recurso à patentes de
software; receiam ainda que a sua actividade de programação se veja
inibida pela permanente espada de Damocles das patentes que incidem
sobre algoritmos de programação.

#lat: At the %(pt:Porto hearing of 2003-07-15) several representatives of
software companies who write proprietary software were present and
opposed software patentability.  INPI als has been consistently
failing to mention copyright.  They are creating the wrong impression
that the discussion is about whether all software should be free.

#die: É no sentido de obter mais elementos que possam enriquecer a discussão
e permitir a adopção de medidas legislativas justas, equilibradas e
promotoras do desenvolvimento da Europa, que o INPI reabre,
gostosamente, o Fórum sobre esta matéria.

#Wli: TODOS OS COMENTÁRIOS PODEM SER ENVIADOS PARA O E-MAIL ablanch@inpi.pt
sendo publicados no site o mais brevemente possível.

#min: Como apoio aos comentários a enviar para este Fórum, estão disponíveis
os documentos mais importantes relacionados com a discussão sobre a
patenteabilidade dos programas de computador na União Europeia.

#WNh: Documentos relacionados com as consultas do INPI às empresas.

#thn: Resumo das consultas realizadas pela União Europeia aos meios
interessados.

#rPW: Versão inicial da Proposta de Directiva, de 20 Fevereiro de 2002.

#fen: Versão final da Proposta de Directiva que vai ser submetida ao
Parlamento Europeu no próximo dia 1 de Setembro.

#idgecho: Uncritical Portuguese Echo of IDG News Service

#publico030623d: Um artigo que replica a desinformação de Paul Meller do IDG, alegando
que a proposta de directiva da JURI previne contra patentes de modelos
de negócio tais como a %(q:One Click Shopping) da Amazon.

#rha: Um dos vários artigos bem escritos que apareceram neste jornal no
verão de 2003.

#iom: Este inclui um panfleto portugês, distribuido especialmente aos
Eurodeputados portugueses.

#oqg: Multicert, uma companhia portuguesa, diz ter adquirido patente sobre
um método de notariado digital (timestamping).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatpt ;
# txtlang: pt ;
# multlin: t ;
# End: ;

