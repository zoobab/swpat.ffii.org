<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Patent lawyer, software patentability guru, patent department head of IBM in Germany and Europe, working in Stuttgart, active promoter of software patents, responsible for pushing many landmark cases through the EPO and the German courts.  Ghostwriter of various patent papers of German and European trade associations.  Positions and style well known from public discussions. Hardline advocate of software patentability and very much in love with certain dogmatic fallacies which he successfully used to win over the (already very inclined) EPO and BGH in a series of decisive battles.  Do not expect Teufel to understand the viewpoint of opensource programmers or to come up with solutions to non-juridical problems.  Expect him to stick to EPO fallacies as steadfastly as anyone.  These fallacies constitute his success experience before the lawcourts.
title: PA Fritz Teufel

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatteufel ;
# txtlang: fr ;
# End: ;

