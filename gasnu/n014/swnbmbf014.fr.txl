<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: plans pour système de brevets universitaire en Allemagne

#descr: Deux décisions prises hâtivement, l'une étant la %(q:loi de soutien de la prise de brevets dans les universités) - que nous abbrévions ici en %(q:Loi des usines-à-brevets) -, l'autre, liée à la première, une %(q:modification de loi sur les inventions dans le cadre du travail) - ou plus court, %(q:loi des chercheurs-esclaves), instituent la suppression totale du droit des professeurs d'université à mettre à disposition du public leurs résultats de recherche conformément à l'éthique de Benjamin Franklin. Tout professeur d'université est tenu de signaler au bureau des brevets de son université tout ce qui est potentiellement brevetable avant d'avoir le droit d'écrire un quelconque papier sur ses résultats. L'administration des brevets, qui doit être mise sur pied grâce à une injection massive d'argent public d'ici à 2003, disposera alors de l'ensemble de la %(q:propriété intellectuelle). Le consentement des professeurs à la perte de leur liberté et de leur dignité sera compensé par quelques gâteries : ils toucheront 30 pourcent des revenus ainsi générés. Les universités d'Etat assument le risque entrepreneurial de la valorisation des brevets, tandis que les professeurs un peu malins peuvent continuer à valoriser dans leur coin les inventions vraiment intéressantes, dans le secteur privé. Là où auparavant la valorisation par le brevet représentait une opportunité pour le secteur privé, mais aussi un écart par rapport à la norme culturelle du monde savant - une publication rapide et le moins possible d'obstacles à la communication -, l'avenir de la culture savante elle-même menace de se retrouver soumise aux contraintes de valorisation dictées par l'administration des brevets. Nous verrons la séparation entre recherche fondamentale subventionnée par l'Etat, et secteur des applications soutenu, ô combien, par les brevets, et cette séparation se fera au détriment de chacun des deux domaines.

#Aen: Le péril représenté par la %(q:loi de soutien au dépôt de brevets dans les universités)

#Eet: Premier rapport sur le projet de loi, et pourquoi il est inopérant et mêm nuisible.

#Inh: Je suis vraiment ébranlé.

#Ano: L'entrepreneur leipzigien en communication voit dans cette loi une attaque contre le droit commun et un non-sens économique calqué sans réflexion sur les idées à la mode. Il ne voit pas pourquoi ses impôts devraient servir à financer des interdictions qui risquent de le gêner dans ses activités d'innovation.

#Wil: Naïf et dommageable pour l'Allemagne en tant que site universitaire.

#Dhe: Pour cet informaticien de Rostock, la loi ne fontionnerait pas, au contraire elle contribuerait à une plus mauvaise gestion et à une bureaucratisation encore plus grande des universités.

#Asc: Arguments en faveur du projet de loi sur les brevets universitaires

#EWe: Un collaborateur expérimenté d'une société de transfert technologique appartenant à une université expose les raisons pour lesquelles il pense que cette loi devient nécessaire. Mais ses arguments ne réussissent guère à amoidrir les préventions des participants du forum. Ils auraient même plutôt tendance, pour certains en tous cas, à les renforcer, ainsi qu'il apparaît clairement à la lecture des contributions suivantes.

#Vlt: Texte intégral

#SWc: Article du magazine Spiegel sur la loi sur les brevets dans les universités

#SWr: Dit le plus grand bien de cette initiative de la ministre de la recherche et compare les universités à des Belles au Bois Dormant qu'il faudrait réveiller en les adaptant aux coutumes américaines. Remarque tout de même qu'au moins dans le cas des les petites universités le compte n'y est pas, que le seul effet risque d'être la création d'une coûteuse bureaucratie.

#Kae: KONNO Hiroshi: Le brevet et le logiciel Karmarkar

#Eua: Un mathématicien japonais de premier plan, bon connaisseur des questions des brevets, démontre comment un brevet sur une innovation brillante en mathématiques laisse de tous côtés des dégâts sur son passage, comment l'orientation américaine en faveur des brevets a conduit des sites de recherche américains autrefois réputés sur le chemin du déclin économique, créant à la place des milliers d'emplois pour des juristes en brevets.

#Rrs: Ralf Schwöbel, président d'%(id:Intradat AG), argumente à partir des faits suivants :

#Dtg: Le but de la politique économique d'une nation ne doit pas être de subventionner des instituts dont les brevets logiciels menacent la croissance de petites et de moyennes entreprises, pour ensuite faire passer ces même brevets pour des retombées directes des orientations économiques. A notre avis, il n'est pas souhaitable pour les instituts de recherche allemands de se comporter selon le modèle du MIT. Les brevets sont sensés être des moyens de garantir la propriété intellectuelle. Si des brevets logiciels devaient être accordés, alors par pitié, seulement pour des entreprises qui les transforment en succès économique, pas pour les institus de recherche qui doivent soutenir l'économie nationale, et non jouer un rôle actif dans le système du marché.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: obenassy ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnbmbf014 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

