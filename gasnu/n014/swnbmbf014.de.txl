<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Pläne für Hochschulpatentgesetze

#descr: Durch ein im Eilverfahren zu verabschiedenes %(q:Gesetz zur Förderung des Patentwesens an den Hochschulen) (kurz: Patentfabrikengesetz) und ein dazugehöriges %(q:Gesetz zur Änderung des Gesetzes über Arbeitnehmererfindungen) (kurz: Denkknechtegesetz) soll das Recht des Hochschullehrers, seine Forschungsergebnisse im Geiste der Ethik eines Benjamin Franklin der Öffentlichkeit zur Verfügung zu stellen, restlos abgeschafft werden. Der Hochschullehrer wird verpflichtet, alles, was möglicherweise patentrechtlich verwertbar sein könnte, der Patentverwaltung seiner Hochschule zu melden, bevor er darüber schreibt. Die Patentverwaltung, die mit staatlicher Anschubfinanzierung bis ca 2003 aufgebaut werden soll, verfügt dann über alles %(q:geistige Eigentum). Die Einwilligung der Professoren in ihren Verlust an Freiheit und Würde wird ihnen durch Vorteilsgewährung versüßt: sie erhalten 30% der erwirtschafteten Einnahmen. Staatlich verfasste Hochschulen tragen das unternehmerische Risiko der Patentverwertung, während Professoren mit etwas Geschick noch immer ohne weiteres die wirklich interessanten Erfindungen an der Patentbürokratie vorbei privat verwerten können. Wo früher die patentrechtliche Verwertung eine Privatangelegenheit und eine Abweichung von den Normen Wissenschaftskultur (schnelles Veröffentlichen, unbehinderte Kommunikation) war, droht in Zukunft die Wissenschaftskultur selber den Verwertungszwängen des Patentwesens untergeordnet zu werden. Die Trennung zwischen öffentlich geförderter Grundlagenforschung und patentgeförderter Anwendungsentwicklung wird zum Schaden beider Sphären verwischt.

#Aen: Akute Gefahr durch %(q:Gesetz zur Förderung des Patentwesens an den Hochschulen)

#Eet: Erster Bericht über das geplante Gesetz und Argumente, warum es nicht funktioniert und gefährlich ist.

#Inh: Ich bin einfach nur erschüttert.

#Ano: Der Leipziger Medienunternehmer sieht in dem Gesetz einen Angriff auf Grundrechte und von einem gedankenlosen Zeitgeist getriebenen volkswirtschaftlichen Unsinn.  Er sieht nicht ein, warum er mit seinen Steuergeldern Verbote finanzieren soll, die ihn bei seiner Innovationstätigkeit behindern.

#Wil: Weltfremd und schädlich für den Hochschulstandort Deutschland

#Dhe: Der Rostocker Informatiker meint, das Gesetz werde nicht funktionieren und zugleich zu einer weiteren Fehlsteuerung und Bürokratisierung der Hochschulen beitragen.

#Asc: Argumente für das geplante Hochschulpatentgesetz

#EWe: Ein erfahrener Mitarbeiter einer hochschuleigenen Technologietransferstelle erläutert, warum das Gesetz aus seiner Sicht benötigt wird.  Seine Argumente können die Bedenken der Forumsteilnehmer allerdings kaum ausräumen, tragen z.T. vielmehr zu deren Stärkung bei, wie man an den nachfolgenden Diskussionsbeiträgen unschwer erkennt.

#Vlt: Volltext

#SWc: Spiegel-Kolumne über das Hochschulpatentgesetz

#SWr: Lobt die Initiative der Forschungsministerin, verträumte Hochschulen aus Dornröschenschlaf wecken und an amerikanische Gepflogenheiten anpassen zu wollen, warnt aber, dass insbesondere bei kleineren Hochschulen die Rechnung oftmals nicht aufgehen und lediglich eine teure Bürokratisierung bewirkt werden dürfte.

#Kae: KONNO Hiroshi: Das Karmarkar-Patent und Software

#Eua: Ein führender japanischer Mathematiker und Kenner des Patentwesens erklärt, wie ein Patent auf eine brilliante mathematische Neuerung auf allen Seiten Flurschaden hinterließ und wie die Patentorientierung ehemals führende amerikanische Forschungsstätten auf einen Weg des wissenschaftlichen Abstieges führte und stattdessen dort Tausende von Arbeitsplätzen für Patentjuristen schuf.

#Rrs: Ralf Schwöbel, Vorstand der %(id:Intradat AG), erklärt aus gegebenen Anlass:

#Dtg: Die staatliche Subvention von Instituten die mit Software-Patenten das Wachstum kleiner und mittelstaendiger Unternehmen gefaehrden, indem diese Patente spaeter auf sogenannte 'Spin-offs' mit wirtschaftlicher Ausrichtung uebertragen, darf nicht Ziel der Wirtschaftspolitik sein.  Ein Verhalten deutscher Forschungsinstitute nach Vorbild des MIT ist aus unserer Sicht nicht wuenscheswert.  Patente sollen Mittel zur Sicherung des geistigen Eigentums sein und wenn Software-Patente schon gebilligt werden sollen, dann bitte doch auch nur fuer Unternehmen, die diese in einen wirtschaftlichen Erfolg umsetzen und nicht fuer Forschungsinstitute, die volkswirtschaftlich foerdern und nicht eine aktive Rolle im Markt uebernehmen sollen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnbmbf014 ;
# txtlang: de ;
# multlin: t ;
# End: ;

