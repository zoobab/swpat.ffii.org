\begin{subdocument}{swnbmbf014}{plans pour syst\`{e}me de brevets universitaire en Allemagne}{http://swpat.ffii.org/acteurs/n014/swnbmbf014.fr.html}{Groupe de travail\\swpatag@ffii.org\\version fran\c{c}aise 2002/02/17 par Odile BÉNASSY\footnote{http://www.entrouvert.com}}{Deux d\'{e}cisions prises h\^{a}tivement, l'une \'{e}tant la ``loi de soutien de la prise de brevets dans les universit\'{e}s'' - que nous abbr\'{e}vions ici en ``Loi des usines-\`{a}-brevets'' -, l'autre, li\'{e}e \`{a} la premi\`{e}re, une ``modification de loi sur les inventions dans le cadre du travail'' - ou plus court, ``loi des chercheurs-esclaves'', instituent la suppression totale du droit des professeurs d'universit\'{e} \`{a} mettre \`{a} disposition du public leurs r\'{e}sultats de recherche conform\'{e}ment \`{a} l'\'{e}thique de Benjamin Franklin. Tout professeur d'universit\'{e} est tenu de signaler au bureau des brevets de son universit\'{e} tout ce qui est potentiellement brevetable avant d'avoir le droit d'\'{e}crire un quelconque papier sur ses r\'{e}sultats. L'administration des brevets, qui doit \^{e}tre mise sur pied gr\^{a}ce \`{a} une injection massive d'argent public d'ici \`{a} 2003, disposera alors de l'ensemble de la ``propri\'{e}t\'{e} intellectuelle''. Le consentement des professeurs \`{a} la perte de leur libert\'{e} et de leur dignit\'{e} sera compens\'{e} par quelques g\^{a}teries : ils toucheront 30 pourcent des revenus ainsi g\'{e}n\'{e}r\'{e}s. Les universit\'{e}s d'Etat assument le risque entrepreneurial de la valorisation des brevets, tandis que les professeurs un peu malins peuvent continuer \`{a} valoriser dans leur coin les inventions vraiment int\'{e}ressantes, dans le secteur priv\'{e}. L\`{a} o\`{u} auparavant la valorisation par le brevet repr\'{e}sentait une opportunit\'{e} pour le secteur priv\'{e}, mais aussi un \'{e}cart par rapport \`{a} la norme culturelle du monde savant - une publication rapide et le moins possible d'obstacles \`{a} la communication -, l'avenir de la culture savante elle-m\^{e}me menace de se retrouver soumise aux contraintes de valorisation dict\'{e}es par l'administration des brevets. Nous verrons la s\'{e}paration entre recherche fondamentale subventionn\'{e}e par l'Etat, et secteur des applications soutenu, \^{o} combien, par les brevets, et cette s\'{e}paration se fera au d\'{e}triment de chacun des deux domaines.}
\begin{itemize}
\item
{\bf {\bf Hartmut Pilch: Le p\'{e}ril repr\'{e}sent\'{e} par la ``loi de soutien au d\'{e}p\^{o}t de brevets dans les universit\'{e}s''\footnote{http://lists.ffii.org/archive/mails/swpat/2001/Apr/0185.html}}}

\begin{quote}
Premier rapport sur le projet de loi, et pourquoi il est inop\'{e}rant et m\^{e}m nuisible.
\end{quote}
\filbreak

\item
{\bf {\bf Xu\^{a}n Baldauf: Je suis vraiment \'{e}branl\'{e}.\footnote{http://lists.ffii.org/archive/mails/swpat/2001/Apr/0194.html}}}

\begin{quote}
L'entrepreneur leipzigien en communication voit dans cette loi une attaque contre le droit commun et un non-sens \'{e}conomique calqu\'{e} sans r\'{e}flexion sur les id\'{e}es \`{a} la mode. Il ne voit pas pourquoi ses imp\^{o}ts devraient servir \`{a} financer des interdictions qui risquent de le g\^{e}ner dans ses activit\'{e}s d'innovation.
\end{quote}
\filbreak

\item
{\bf {\bf Prof. Dr. Clemens Cap: Na\"{\i}f et dommageable pour l'Allemagne en tant que site universitaire.\footnote{http://lists.ffii.org/archive/mails/swpat/2001/May/0104.html}}}

\begin{quote}
Pour cet informaticien de Rostock, la loi ne fontionnerait pas, au contraire elle contribuerait \`{a} une plus mauvaise gestion et \`{a} une bureaucratisation encore plus grande des universit\'{e}s.
\end{quote}
\filbreak

\item
{\bf {\bf Dr. Erich Siebel: Arguments en faveur du projet de loi sur les brevets universitaires\footnote{http://lists.ffii.org/archive/mails/swpat/2001/Jul/0070.html}}}

\begin{quote}
Un collaborateur exp\'{e}riment\'{e} d'une soci\'{e}t\'{e} de transfert technologique appartenant \`{a} une universit\'{e} expose les raisons pour lesquelles il pense que cette loi devient n\'{e}cessaire. Mais ses arguments ne r\'{e}ussissent gu\`{e}re \`{a} amoidrir les pr\'{e}ventions des participants du forum. Ils auraient m\^{e}me plut\^{o}t tendance, pour certains en tous cas, \`{a} les renforcer, ainsi qu'il appara\^{\i}t clairement \`{a} la lecture des contributions suivantes.
\end{quote}
\filbreak

\item
{\bf {\bf Gesetz zur \"{A}nderung des Arbeitnehmererfindergesetzes (Referentenentwurf)\footnote{http://swpat.ffii.org/papiers/bmj-hochpatarbeg01/bmj-hochpatarbeg01.de.html}}}

\begin{quote}
Texte int\'{e}gral
\end{quote}
\filbreak

\item
{\bf {\bf Gesetz zur F\"{o}rderung des Patentwesens an den Hochschulen\footnote{http://swpat.ffii.org/papiers/bmj-hochpatg01/bmj-hochpatg01.de.html}}}

\begin{quote}
Texte intégral
\end{quote}
\filbreak

\item
{\bf {\bf Offensive de Brevet\footnote{http://swpat.ffii.org/acteurs/bmbf/swpatbmbf.de.html}}}

\begin{quote}
Hunderte von universit\"{a}ren ``Technologietransfer-Stellen'' besch\"{a}ftigen sich hauptamtlich damit, die Verbreitung von Technologien mithilfe von Patenten zu bremsen.  Der Steuerzahler finanziert die Erschlie{\ss}ung und sofortige Monopolisierung neuer (und alter) Wissensg\"{u}ter.  Das BMBF bemisst den Erfolg seiner Investitionen nicht an den erzeugten \"{o}ffentlichen G\"{u}tern sondern umgekehrt an den der \"{O}ffentlichkeit entzogenen G\"{u}tern.  F\"{u}r die Patentgeb\"{u}hren zahlt der Steuerzahler sp\"{a}ter ggf noch einmal 10 mal so viel drauf, wie die Universit\"{a}t (bei effizienter F\"{u}hrung vielleicht irgendwann einmal) daran verdient.  In den so geschaffenen Patentfabriken bestimmt eine staatlich gef\"{o}rderte Verwertungsb\"{u}rokratie die Richtung der Forschungspolitik.  Den guten volkswirtschaftlichen Gr\"{u}nden f\"{u}r die Existenz eines \"{o}ffentlichen Bildungs- und Forschungssektors wird offenbar kaum noch Rechnung getragen.  Stattdessen entsteht eine zweitklassige Konkurrenz zur industriellen Forschung.  Irgendwann in der Zukunft wird hierdurch vielleicht einmal die Staatskasse entlastet.  Das versprechen zumindest die ``Technologietransferbeamten'', die in dem neuen System die Posten bekleiden.  Dieser Gruppe stehen in diversen Regierungen auf regionaler, nationaler und supranationaler Ebene steuerlich finanzierte Agenturen zur Seite, von denen insbesonere die ``Patentoffensive'' im Bundesministerium f\"{u}r Bildung und Forschung von sich reden macht.
\end{quote}
\filbreak

\item
{\bf {\bf Article du magazine Spiegel sur la loi sur les brevets dans les universit\'{e}s\footnote{http://www.spiegel.de/wissenschaft/0,1518,147988,00.html}}}

\begin{quote}
Dit le plus grand bien de cette initiative de la ministre de la recherche et compare les universit\'{e}s \`{a} des Belles au Bois Dormant qu'il faudrait r\'{e}veiller en les adaptant aux coutumes am\'{e}ricaines. Remarque tout de m\^{e}me qu'au moins dans le cas des les petites universit\'{e}s le compte n'y est pas, que le seul effet risque d'\^{e}tre la cr\'{e}ation d'une co\^{u}teuse bureaucratie.
\end{quote}
\filbreak

\item
{\bf {\bf Fritz Machlup 1962: The Production and Distribution of Knowledge in the United States\footnote{http://swpat.ffii.org/papiers/machlup62/machlup62.en.html}}}
\filbreak

\item
{\bf {\bf KONNO Hiroshi: Le brevet et le logiciel Karmarkar\footnote{http://swpat.ffii.org/papiers/konno95/konno95.ja.html}}}

\begin{quote}
Un math\'{e}maticien japonais de premier plan, bon connaisseur des questions des brevets, d\'{e}montre comment un brevet sur une innovation brillante en math\'{e}matiques laisse de tous c\^{o}t\'{e}s des d\'{e}g\^{a}ts sur son passage, comment l'orientation am\'{e}ricaine en faveur des brevets a conduit des sites de recherche am\'{e}ricains autrefois r\'{e}put\'{e}s sur le chemin du d\'{e}clin \'{e}conomique, cr\'{e}ant \`{a} la place des milliers d'emplois pour des juristes en brevets.
\end{quote}
\filbreak
\end{itemize}

Ralf Schw\"{o}bel, pr\'{e}sident d'Intradat AG, argumente \`{a} partir des faits suivants :
\begin{quote}
Le but de la politique \'{e}conomique d'une nation ne doit pas \^{e}tre de subventionner des instituts dont les brevets logiciels menacent la croissance de petites et de moyennes entreprises, pour ensuite faire passer ces m\^{e}me brevets pour des retomb\'{e}es directes des orientations \'{e}conomiques. A notre avis, il n'est pas souhaitable pour les instituts de recherche allemands de se comporter selon le mod\`{e}le du MIT. Les brevets sont sens\'{e}s \^{e}tre des moyens de garantir la propri\'{e}t\'{e} intellectuelle. Si des brevets logiciels devaient \^{e}tre accord\'{e}s, alors par piti\'{e}, seulement pour des entreprises qui les transforment en succ\`{e}s \'{e}conomique, pas pour les institus de recherche qui doivent soutenir l'\'{e}conomie nationale, et non jouer un r\^{o}le actif dans le syst\`{e}me du march\'{e}.
\end{quote}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: latex ;
% End: ;

