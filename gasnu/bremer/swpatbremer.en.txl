<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Her Master's Voice --- Dr. iur. Kathrin Bremer

#descr: Legal delegate of Bitkom e.V., German software trade association.  Female, born 1969, studied international law and came to Bitkom directly from there in 1999.  Apparently insecure on the issue, unfamiliar with software and even with patent law, lets her predecessor in office, IBM patent lawyer Fritz Teufel ghostwrite most of her contributions to the debate.  These include a participation in a hearing of the German Parliament in 2001-06-21.

#Dev: Die Sitzung vom Frühjahr 2002, auf der 7 Großkonzern-Patentjuristen gegen einen KMU-Vertreter die Verbandsposition gegenüber dem Patentrichtlinienentwurf festlegten, wurde von Bremer einberufen und von Teufel inhaltlich bestimmt, wobei Teufel einige übereifrige Pro-Swpat-Töne von Bremer abmilderte.  So hatte Bremer in ihrer Variante des Resolutionsentwurfs behauptet, dass die Stärkung des Patentwesens zur Schaffung von Arbeitsplätzen beitragen werde.

#Fli: Frau Bremer war seit Anfang 2001 mehrfach von Patentkritikern innerhalb und außerhalb von Bitkom im Vorfeld darauf hingewiesen worden, dass die meisten Softwareunternehmen die Ausweitung des Patentwesens in ihren Bereich hinein mit Unbehagen sehen.  Sie berichtete uns sogar mündlich von kleineren Umfragen innerhalb von Bitkom, die dieses Bild bestätigten.  Ihr lagen umfangreiche Dokumentationen über die Problematik der Logikpatente vor.  Doch diese Erkenntnisse ließen sie ähnlich unberührt wie auch ihren BSA-Kollegen Francisco Mingorance.  Vermutlich bemüht sich Frau Bremer ebenso wie Herr Mingorance lediglich um gute Beziehungen zu den Kreisen, die sie für maßgeblich hält, und interessiert sich ansonsten nicht für das Thema.

#D0n: Derzeit, im Herbst 2002, ist Frau Bremer in Mutterschaftsurlaub.

#LsW: Bremer Vita

#Eai: A source for the above data: Bremer's appearance at a DRM conference in 2002.

#KWi: Kathrin Bremer wurde 1969 in Frankfurt am Main geboren.  Sie studierte von 1988 bis 1994 Rechtswissenschaften an der Universität Frankfurt am Main und an der Université Paris X Nanterre in Paris, wo sie die maitrise en droit public erwarb.  Die Schwerpunkte ihres Studiums lagen auf den Gebieten des Europarechts und des Völkerrechts.  Nach der ersten juristischen Staatsprüfung arbeitete Kathrin Bremer in den Jahren 1995 und 1996 neben ihrem Promotionsstudium in der international ausgerichteten Anwaltssozietät Döser Amereller Noack Baker & McKenzie. Von 1997 bis 1999 absolvierte sie ihr Referendariat in Frankfurt am Main und in Genf, wo sie für die Ständige Vertretung der Bundesrepublik Deutschland bei den Vereinten Nationen tätig war. Ihre Promotion auf dem Gebiet des Völkerrechts hat sie 1998 abgeschlossen. Im Jahr 1999 begann Kathrin Bremer ihre Tätigkeit für den Fachverband Informationstechnik im Verband deutscher Maschinen- und Anlagenbau e.V., der einer der Gründungsverbände des im November 1999 gegründeten Bundesverbandes Informationswirtschaft, Telekommunikation und neue Medien e.V. (BITKOM) ist.  Im BITKOM ist Kathrin Bremer verantwortlich für Rechtsfragen zum E-Commerce und zum gewerblichen Rechtsschutz sowie insbesondere zum Urheberrecht.

#Ain: Auf dem DRM-Seminar in Berlin Anfang 2002 vertritt sie ohne jedes Problembewusstsein Verwertungsutopien nach dem Geschmack des Bitkom-Vorsitzenden Rohleder.

#Bni: Bild von Katrhin Bremer

#Wwh: Similar Career Pattern

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbremer ;
# txtlang: en ;
# multlin: t ;
# End: ;

