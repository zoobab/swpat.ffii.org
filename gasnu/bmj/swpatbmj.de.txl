<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: BMJ: Ängstlicher Vorreiter der Patentinflation in DE und EU

#descr: Für alle Gesetzgebung im Bereich des Patentwesens ist innerhalb der
Bundesregierung das Bundesministerium der Justiz (BMJ) zuständig.  Das
BMJ unterhält in seiner Abteilung für Industrie und Handel ein eigenes
Patentreferat.  Zum Geschäftsbereich des BMJ gehören ferner das
Deutsche Patent- und Markenamt (DPMA), das Bundespatentgericht (BPatG)
und der Bundesgerichtshof (BGH).  Zu all diesen Organisationen und
auch zum Europäischen Patentamt (EPA) hin bestehen innige personelle
Verflechtungen.  Das BMJ-Patentreferat ist personell schwach
ausgestattet und verlässt sich daher auf EPA, DPMA und andere
Patentbewegungs-Institutionen.  Die Karierre der BMJ-Beamten verläuft
häufig innerhalb des Patentwesens.  Sie folgen im allgemeinen (aus
Gewohnheit) wortgetreu deren (von wenigen Instanzen beschlossener)
herrschender Meinung und beschränken die argumentative
Auseinandersetzung meist von vorneherein auf grammatische Fragen. 
Innerhalb der Bundesregierung und des Europäischen Rates vertreten sie
energisch die Interessen der Patentanwälte führender Großkonzerne und
in zweiter Linie der Patentinstitutionen.  Sie verbitten sich
Einmischungen %(q:fachfremder) Personen (einschließlich BMWi,
Abgeordnete) in ihren Kreis und begegnen diesen durch Ignorieren oder
sonstige Diskussionsverhinderungsstrategien.  Auch von tausenden von
Personen unterzeichnete Briefe an das BMJ-Patentreferat (Dr Welp)
blieben bisher unbeantwortet.

#B3e: BMJ Brüssel 2002-06-13: Speerspitze der Grenzenlosen Patentierbarkeit

#Bke: BMJ Brüssel 2002-09-13: Text-Patente durchgesetzt

#Bee: Beamtenkarrieren innerhalb der Patentfamilie

#ARa: In der %(q:Arbeitsgruppe Geistiges Eigentum und Patente) im
Europäischen Rat sind vor allem Vertreter von nationalen Patentämtern
und Patentverwaltungen versammelt.  Auch die Europäische Kommission
lässt sich dort ausschließlich von Patentjuristen, ja sogar von
Beamten des Europäischen Patentamtes, vertreten.  Es ist keineswegs
eine Runde, die das Patentwesen mit besonderem Argwohn betrachtet. 
Dennoch ist es dem BMJ gelungen, auch in dieser Runde eine Position in
der vordersten Front der Vorreiter der grenzenlosen Patentierbarkeit
einzunehmen, und das obwohl führende Politiker innerhalb der Berliner
Regierungskoalition sich kritisch bis ablehnend über die Politik des
Patentämter und der Europäischen Kommission geäußert haben

#Bce: Bei der ersten Lesung der Richtlinie am 13. Juni 2002 erklärte
%(ah:Anthony Howard) (Eur. Kommission) auf bohrende Fragen der
Delegationen von FR, BE, IT, ES, GR und NL hin nach und nach, was die
vorgeschlagene Richtlinie bewirken soll: nämlich die Anerkennung von
jeder auf einem Rechner umgesetzten Idee als patentierbare Erfindung
und die internationale vertragliche Verpflichtung mittels TRIPs auf
diese Doktrin.  Insbesondere die Vertreter von Spanien, welches die
Ratspräsidentschaft führte, zeigten sich demgegenüber
unmissverständlich ablehnend.

#Dlc: Derweil applaudierten die BMJ-Beamten, welche Deutschland vertraten,
durchweg dem Ansatz der Europäischen Kommission und schlugen einzelne
Nachbesserungen vor, allerdings nur im Sinne einer noch großzügigeren
Patentvergabepraxis.  Die deutsche Delegation ergriff die Gelegenheit
zu folgenden Wortmeldungen, die wir aus uns übermittelten
Gesprächsprotokollen zurückübersetzt haben:

#WWi: Wir begrüßen generell diesen Richtlinienvorschlag.  Drei Punkte sind
in unseren Augen wesentlich:  wir können das derzeitige Patentrecht
unverändert weiterverwenden, ohne im Bereich der Software besondere
Anpassungen (wie z.B. Zwangslizenzierung) vorzunehmen.  Es kommt nur
darauf an, das derzeitige Recht zu kodifizieren und zu präzisieren. 
Die rein geschäftlichen Verfahren sind nicht patentierbar.  Es
bestehen gewisse Zweifel daran, ob sich die positive Wirkung der
Patente in den USA auf Europa übertragen lässt.  Deshalb hat die
Bundesregierung eine Studie in Auftrag gegeben, die zeigt, dass der
Markt für die KMU und die kleinen Entwickler im wesentlichen auf der
Grundlage des Urheberrechts funktioniert, und dass letztere sich vor
einer Monopolisierung durch die Großen fürchten.  Die empirischen
Daten deuten aber darauf hin, dass diese Befürchtung grundlos ist. 
Daher arbeiten wir an einer zweiten Studie, die untersucht, ob derzeit
vor Gerichten anhängige Streitfälle weitere Hinweise in diesem Sinne
liefern können.  Wir haben Treffen mit alle interessierten Parteien
durchgeführt. Hinsichtlich der Artikel 2a und 5 ist vielleicht noch
manches nachzubessern. Wir zollen der Europäischen Kommission großen
Respekt für ihre Bemühungen.

#Wrt: Wenn wir harmonisieren, dann muss das resultierende Recht nicht nur
von den nationalen Patentämtern sondern auch vom EPA übernommen
werden.

#Iei: In Art 2a sollte jeder Bezug auf Neuheit gestrichen werden

#Dda: Dies geht offensichtlich auf eine Forderung in der
%(bd:BDI-Stellungnahme) zurück, wobei allerdings die BDI/BMJ-Juristen
offensichtlich nicht verstanden hatten, was Howard im folgenden
erklärte:  dass in Art 2a der Bezug auf Neuheit die Patentierbarkeit
ausweitet und keineswegs einschränkt.

#Dnl: Die deutsche Übersetzung des Richtlinienvorschlages ist teilweise
missverständlich, die englische Fassung ist besser.

#Wwv: Wir teilen die Auffassung von Englands und Österreichs, wonach
Ansprüche auf Programmprodukte, Programme etc zulässig sein sollten. 
Darüber muss noch nachgedacht werden.

#Aci: Alle Delegationen außer UK, AT und DE zeigten sich mit Art 5 des
Richtlinienvorschlages einverstanden.  GR und BE hatten ihn sogar als
zentrale Errungenschaft der Richtlinie hervorgehoben und gefordert,
dass Programmansprüche noch unmissverständlicher ausgeschlossen werden
müssten.

#Wtk: Wir halten ebenso wie IT und NL Art 3 für notwendig.

#Gte: Howard (Eur. Kommission) hatte erläutert, dass es bei Art 3 darum
gehe, %(q:computer-implementierte Erfindungen) generell für technisch
zu erklären und ihre Patentierbarkeit als eine internationale
Verpflichtung aus Art 27 TRIPs herzuleiten.  GR hatte heftig
protestiert, der Bezug auf TRIPs sei hier fehl am platze und bedeute
eine zu starre Festlegung.  NL und IT hatte den griechischen Bedenken
entgegnet, die Auslegung von Art 27 TRIPs müsse unmissverständlich
festgelegt werden, denn es gebe zu viele Leute, die meinten, Software
sei untechnisch, und eine solche Streitfrage dürfe man nicht offen
lassen.

#Zmn: In Vorüberlegung 16 (der Präambel) ist nur von den Beziehungen zu
%(q:unseren wichtigsten Handelspartnern) die Rede.  Darin kommt nicht
klar genug zum Ausdruck, dass es im wesentlichen um die
transatlantischen Beziehungen geht.

#DaG: Diese Positionen stimmen auch mit den Positionen überein, die das BMJ
in einigen Papieren formulierte, welche einem Kreis von
Gesprächspartnern (dem der FFII nicht angehört) vorgelegt wurden.  Auf
einer weiteren Sitzung im Juli 2002 argumentierte das BMJ ähnlich. 
Bei alledem zeigen sich immerhin erste Spuren schlechten Gewissens. 
Man hat z.B. Kenntnis davon genommen, dass die Softwarebranche mit dem
Urheberrecht gut zurecht kommt und sich nicht unbedingt Swpat wünscht,
und gibt diese Kenntnis immerhin weiter, lässt sich aber in der
generellen Linie der Folgsamkeit gegenüber Brüssel und München nicht
beirren.

#AsW: Am Freitag, den 13. September tagte die Rats-Arbeitsgruppe erneut. 
Herr Welp vom BMJ trug erneut den Standpunkt der Bundesregierung (?)
vor, wonach die Europäische Kommission bei der Ermöglichung von
Softwarepatenten in einem wesentlichen Punkt nicht weit genug geht: 
es müsse möglich werden die Existenz eines Textes auf einem
Datenträger oder im Internet als unmittelbare Patentverletzung
anzusehen. Art 5 des EU/BSA-Vorschlages müsse dahingehend geändert
werden, dass Computerprogramme nicht nur verschämt als %(q:Sytem und
Verfahren, dadurch gekennzeichnet dass ...) sondern auch direkt als
%(q:Information auf einem Datenträger, dadurch gekennzeichnet dass
...) beansprucht werden können.

#Ded: Die Patentverletzung darf laut BMJ-Standpunkt nicht erst dann
beginnen, wenn ein Programm auf einem Rechner ausgeführt wird, sondern
wie beim Urheberrecht schon dann, wenn ein verletzendes Werk in Umlauf
gebracht wird.  Damit könnte die vom Patentamt geforderte Offenlegung
einer Software-Innovation bereits den Tatbestand der Patentverletzung
erfüllen.  Das BMJ erfüllt mit dieser juristisch unlogischen
Forderungen einen via Bitkom und BDI veröffentlichten Wunsch des
%(ft:IBM-Patentanwalts Teufel) und des Münchener Patentpapstes
%(js:Prof. Dr. Josef Straus).  Beide sind Gewährsleute, an deren
Meinung das BMJ regelmäßig seine Politik ausrichtet.

#Dei: Das BMJ konnte am 13. September in der Rats-Arbeitsgruppe weitere
nationale Delegationen für seinen Standpunkt gewinnen. Dank der
Bemühungen der Bundesregierung soll nun das einzige Zugeständnis,
welches die Europäische Kommission den Softwarepatentgegnern zu machen
bereit war, auch noch beseitigt werden.

#DKd: Das BMJ setzte sich zugleich im Vorfeld der Arbeitssitzung mit
Kollegen in der franzöisischen Regierung ins Benehmen und trug dazu
bei, dass deren patentkritische Position abgeschwächt wurde.

#Pre: Proteste aus den Reihen der regierenden Koalitionsparteien wurden vom
BMJ ignoriert.

#IPn: In fast allen anwesenden Delegationen dominierten Funktionäre der
nationalen Patentämter.  Aufgrund der engen Verflechtung des
BMJ-Patentreferates mit dem DPMA und den Patentgerichten lässt sich
dies auch von der deutschen Delegation sagen.  Die EUropäische
Kommission wurde ebenfalls von 2 %(uk:britischen)
Patentamtsfunktionären vertreten, darunter Dai Rees, der am
Europäischen Patentamt seit Jahren als Wegbereiter und Propagandist
der Softwarepatentierung fungiert.

#Dri: Da unter den Patentamtskollegen weitgehende Einigkeit herrscht, wird
erwartet, dass der Ministerrat unter der dänischen Präsidentschaft in
ca 2 Wochen ein %(q:Kompromisspapier) veröffentlicht, das uns
hinsichtlich Logikpatente vom Regen in die Traufe bringt.  Es wird den
Politikern der Regierungskoalition schwer fallen, dann gegen eine
Politik zu opponieren, die die von ihnen gestellte Regierung
wesentlich mit initiiert hat.

#2re: 2002-09-13 EU-Ministerrat: Bundesregierung setzt Textpatente durch

#Drw: Der derzeitige Präsident des Europäischen Patentamtes, %{IK}, war
früher Staatssekretär im BMJ.

#DVa: Die beiden Vertreter Deutschlands, die im Juli 1999 zusammen mit
anderen europäischen Fachkollegen dem Vorstand des Europäischen
Patentamtes das Mandat zur Ausarbeitung eines
Gesetzesänderungsvorschlags zur Legalisierung von Logikpatenten gaben,
machten kurz darauf Karriere in München: %(PM) wurde Leiter einer
Direktion des EPA, %(GL) Präsident des Deutschen Patentamtes und seit
2001 des Bundespatentgerichts sowie der Kommission zum Aufbau der
Europäischen Patentgerichtsbarkeit.

#DeW: Däubler-Gmelin preist Fähigkeiten von Landfermann, der 1972-2000 im
BMJ diente und am Aufbau des Europäischen Patentwesens mitwirkte und
nun mit Schwerpunkt auf die Europäisierung der noch verbleibenden an
nationale Rechtstaatlichkeit gebundenen Patentrechtsprechung hinwirken
soll.  Dazu wurde eine Kommission gegründet, der Landfermann nun
vorsteht.

#Ete: Eine Journalistin sprach mit BMJ-Vertretern über Softwarepatente und
produzierte einen etwas wirren Artikel, weil sie ohne Mut zu
kritischen Fragen wirre BMJ-Standpunkte wiedergab. Grundtenor des BMJ
ist demnach %(q:keine Verweigerungshaltung gegenüber den Brüsseler
Plänen) und %(q:der Diskussion nicht vorgreifen) sowie %(q:den Erwerb
von Softwarepatenten leichter machen).

#BWa: Reaktion des BMJ auf den FTD-Pressebericht. BMJ-Pressesprecherin Marit
Strasser erklärt, die FTD habe die Standpunkte des BMJ falsch
wiedergegeben und Pilch habe sich durch Verbreiten von
Verschwörungstheorien als Dialogpartner des BMJ disqualifiziert. 
Heiko Recktenwald und Bernhard Reiter bitten um Aufnahme eines Dialogs
und um Klarstellung der Standpunkte des BMJ.  Hierauf antwortet
Strasser nicht.  Es kommt auch später weder zu einem Dialog noch zu
einer Klarstellung.

#tii: In einem Brief von 2004-10-04 auf S. 15 erklärt das Portugiesische
Patentamt, wie die Rats-Arbeitsgruppe zu ihrer Position für
unmittelbare Patentierbarkeit von Computerprogrammen gelangte.  Die
deutsche Delegation spielte demnach eine führende Rolle:

#eom: Angesichts der Notwendigkeit, Erfindungen in diesem Bereich durch
Patente zu schützen, lenkte Portugal die Aufmerksamkeit auf die
Steigerung der Schutzwirkung, die erreicht werden könnte, wenn dieser
Schutz auf %(q:Programme auf Datenträger) ausgedehnt werden könnte. 
Denn ein auf ein Programm auf Datenträger gerichteter Schutz würde es
erlauben, unmittelbar gegen jeden vorzugehen, der ein verletzendes
Programm vertreibt oder verwendet.  In jüngster Zeit hat die deutsche
Delegation einen Vorschlag zur Änderung von Artikel 5 eingebracht, der
darauf abzielt, den Schutz für Programme auf Datenträgern in die
Richtlinie aufzunehmen.  Die portugiesische Delegation unterstützte in
einer ersten Reaktion diesen Vorschlag.

#PS0: Protokoll der Sitzung von 2002-06-05 im BMJ

#AsK: Am 5. Juni 2002 rief das Patentreferat des BMJ eine Reihe von
Patentjuristen zu sich, die seit Jahren an vorderster Front für die
Aufweichung des Technikbegriffes und die Patentierbarkeit von Software
(computer-implementierten Organisations- und Rechenregeln) kämpfen. 
Als einziger patentkritischer Gesprächspartner wurde RA Siepmann vom
Linux-Verband hinzugezogen, zu dem sich noch Daniel Riek gesellte. 
Die Moderatoren bemühten sich, keine Diskussion darüber aufkommen zu
lassen, was durch die Regelung erreicht werden soll.  Stattdessen
sammelten sie unterstützende Stimmen für die eine oder andere
Formulierung, offenbar mit dem Ziel, die Regierung von Verantwortung
zu entlasten.  Nach der Sitzung versandten sie ein Protokoll.  Darin
steht u.a. zu lesen:

#Eao: Einhellig wurde gefordert, dass der Schutz  nach Patenterteilung
umfassend sein solle, dass also auch Computerprogrammprodukte
eingeschlossen sein sollten. Der Ausschluss wurde als systemwidrig
angesehen.

#Elc: Eine solche Forderung wurde sicher nicht von allen Teilnehmern
aufgestellt.  Zumindest entspricht sie nicht der Position des
Linux-Verbandes.  In einer späteren Version des BMJ-Papiers wurde
deshalb auch %(q:einhellig) zu %(q:fast einhellig) korrigiert.  Wobei
diese %(q:Einhelligkeit) natürlich nur im Kreise der beim BMJ
versammelten Patentjuristen gilt.  Die Gegenposition kann sich
hingegen auf eine ebenfalls %(q:einhellige) oder %(q:fast einhellige)
Meinung aller Software-Entwickler und Wirtschaftswissenschaftler
stützen.  Das juristische Argument, der Ausschluss sei systemwidrig,
ist auch sachlich nicht haltbar.  Wenn ein Programmtext beansprucht
werden kann, wird damit jede bessere Patentschrift (die eine
Referenzimplementation in ihrer Beschreibung enthält) zu einem
patentverletzenden Gegenstand.  Die Monopolisierung von Informatio ist
systemwidrig und verstößt gegen den Geist wenn nicht gegen den
Buchstaben von Art 5 GG (Meinungs- und Informationsfreiheit) .

#Ede: Eine Definition des Begriffs der Technik ... wurde nahezu einstimmig
gefordert; eine geeignete Definition konnte aber noch nicht gefunden
werden.

#Dvi: Der BMJ-Textschreiber versäumt es, die BGH-Definition %(q:Einsatz
beherrschbarer Naturkräfte zur Herbeiführung eines kausal übersehbaren
Erfolges ...) zu erwähnen.  Herr Siepmann schlug dies in Berlin vor
und führt näher aus, dass die Notwendigkeit experimenteller
Überprüfung das entscheidende Kriterium sei, m.a.W. müssten die
Naturkräfte Teil der Problemlösung und ihr Einsatz zum Auffinden der
Lösung erforderlich sein, wie dies in der BGH-Rechtsprechung ab
%(dp:Dispositionsprogramm) lange Zeit galt.  Gegen Herrn Siepmanns
Vorschlag wandte BGH-Richter Scharen ein, dadurch würden zu viele
Patente wegfallen.  Als Herr Siepmann fragte, was denn so schlimm
daran sei, wenn ein paar Patente weniger erteilt werden, schwieg die
Runde.  Weitere Gegenargumente oder gar Gegenvorschläge gab es nicht.

#EeW: Es wird andererseits immerhin von der Forderung eines Teiles der
Anwesenden berichtet, den von EUK/BSA abgeschafften Erfindungsbegriff
wieder einzuführen, nämlich derart dass %(e:Neuheit und erfinderische
Tätigkeit sich auf den technischen Beitrag gründen sollen).  Zwar war
dies nicht konsensfähig, aber immerhin wird es erwähnt.  Vielleicht
auch deshalb, weil auch Herr Tauchert vom DPMA und manche Richter hier
ein Problem sehen.

#Rnt: Referentenentwürfe

#Aer: Aktuelle Gesetzgebungsvorhaben des BMJ

#Eop: Eines von zahlreichen Schreiben gewählter Volksvertreter an das BMJ,
die dessen Patentpolitik kritisieren, nur um vom BMJ keines Blickes
und keiner Antwort gewürdigt zu werden.

#Ake: Auch dieser von zahlreichen namhaften Akteuren der Softwarebranche
unterzeichnete Brief prallte unbeantwortet am BMJ ab.

#Ile: Im trauten Kreise der Patentjuristen sind Raimund Lutz und Kollegen in
ihrem Element. Normalerweise ist es nicht leicht, die Patentpolitiker
des BMJ für eine Teilnahme an irgendwelchen Veranstaltungen zu
gewinnen, auf denen das Patentwesen kritisiert wird.  Die Rednerliste
dieser Veranstaltung liest sich jedoch wie das Who-is-Who der
Patentbewegung und speziell derjenigen Leute, die sich das BMJ in
Swpat-Fragen zwecks Erzeugung einer %(q:einhelligen Meinung) als
Berater nach Berlin holt.

#ArK: An dieser Anhörung nahm ein Vertreter des BMJ-Patentreferates teil,
der den Patentprüfer Dr. Swen Kiesewetter-Köbinger vom DPMA mit
hintergründig drohenden Mine musterte und von ihm in unfreundlichem
Ton verlangte, klarzustellen, was er ohnehin schon klargestellt hatte,
nämlich dass kleine widerspenstige Patentprüfer keineswegs im Namen
des DPMA sprechen dürfen.  Der Beitrag des BMJ zu diesem
Bundestags-Expertengespräch erschöpfte sich unseres Wissens in
disziplinierenden Blicken obiger Art.

#Dnr: Dieses Gericht liegt im Zuständigkeitsbereich des BMJ und zwischen
seinem Patentsenat und dem BMJ-Patentreferat gibt es intensive
Beziehungen.  Zu Sitzungen über die Frage der Softwarepatente lädt man
von seiten des BMJ vorzugsweise patentierfreudige Richter ein.

#Hhe: Hier gilt ähnliches wie für den BGH

#EeW2: Eine regelmäßiges Karriereziel für BMJ-Patentreferenten.  Der letzte
DPMA-Präsident Landfermann kam vom BMJ und wechselte dann zum BPatG.

#Ers: Ein FDP-Politiker, dessen Karriere über das BMJ an die Spitze des
Europäischen Patentamtes führte.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbmj ;
# txtlang: de ;
# multlin: t ;
# End: ;

