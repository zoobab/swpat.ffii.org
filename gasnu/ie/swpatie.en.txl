<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Ireland and Software Patents

#descr: Irish members of the European Parliament have to a large part voted
against software patents (i.e. for amending the software patent
directive) in September 2003.  Irish ICT associations have been more
sceptical of this directive than some of their counter-parts of other
countries.  Ireland took the EU presidency in the first half year of
2004.  The Irish presidency supported efforts of the national patent
administrators to scrap the European Parliament's amendments without
discussion at the ministerial level.  The Irish presidency said on its
website that it was %(q:sponsored by Microsoft).  The Presidency's
effort was supported by FUD from a group of patent-centered companies
which were heard by the Irish Parliament and parrotted by the Irish
press in early May.  Ireland provides a tax haven for patent royalty
fees and harbours (partially for this and similar reasons) much of the
finance flow of large US companies, so that Ireland is often called
%(q:Europe's largest software exporting country).  Trinity College in
Dublin is a leading European facility for training patent lawyers.

#lWy: Publications of the Irish Presidency as of January suggested that the
legislative process in the Irish ministeries, as elsewhere, is
dominated by faithful followers of the patent movement.  On the other
hand the IE government may be seen as avoiding the inevitable
political conflicts which are associated with the software patent
directive, putting it behind the Community Patent and IP Enforcement
directives, so as not to create tensions between Council and
Parliament in a year in which the adoption of the EU Constitution and
other contentious issues are already creating enough problems.  It
could be expected that the Irish presidency will try to keep a low
profile and then try to align the Council on a pro-software-patent
position in mid May, if that is seen as feasible by then.

#CIl: ICT Ireland

#Cye: Irish ICT industry lobbying group, member of EICTA

#see: Sponsors of the Irish Presidency

#siW: List of companies who sponsor the Irish Presidency.  Apparently it has
become customary in the EU that presidencies are sponsored by large
companies.  This time one of them is Microsoft.

#Wao: %(al|This 16 page PDF brochure contains two almost identical,
instances of the word 'software'. Page 6:%(orig|A knowledge-based
economy must have effective instruments and laws to protect and
promote investment in research and innovation. The enforcement of
intellectual property rights, a common regime for the community
patent, community trade mark regulations, and the protection of
software inventions form an important underpinning to the research and
knowledge-based economy. We will ensure that progress is made in these
areas, which play an essential part in supporting our adjustment to
new and higher value economic activities.)|and page 12:%(orig|A
knowledge-based economy must have effective instruments and laws to
protect and promote investment in research and innovation. A
harmonised approach to enforcement of intellectual property rights, EU
wide protection of patents afforded by a community patent, and the
protection of software inventions, form an important underpinning of
the research and knowledge-based economy. We will ensure that progress
is made on these areas which play an essential part in supporting our
adjustment to new and higher value economic activities. The Community
Patent is important for European industry and must be available to
firms at reasonable cost. The Irish Presidency will make every effort
to ensure its adoption. With a view to completing the internal market
in the area of intellectual property rights, an important initiative
is the Directive on enforcement which aims to harmonise enforcement
measures in respect of those IP rights which have already been
substantively harmonised at Community level. It is the intention of
the Irish Presidency to make every effort to achieve a common position
within the Council on this initiative.))

#ore: %(q:Political agreement) on the software patents directive is
scheduled as item 8 on the provisional agenda for the Competitiveness
Council on 17-18 May (top of page 11), but does not appear on the
schedule for the 11 March meeting.  What is on the schedule for March
is political agreement on the IP enforcement directive.  There is also
activity planned for both meetings on the Community Patent, with an EU
regulation on the Community Patent scheduled for agreement in March,
and the establishment of the Community Patent Court apparatus
scheduled for May.

#hWh: Mary Harney (Irish deputy PM) has given a speech launching a brochure
on Ireland's priorities for the Competitiveness council

#taP: Pro-Patent companies talking to Irish Parliament

#Wri: An affiliate organisation of FSF Europe, coordinated by a 4-person
committee chaired by Ciaran O'Riordan

#hhc: Since June 2003 this archived list has been collaborating on issues
such as software patents, and the European Copyright Directive, the
Intellectual Property Rights Enforcement Directive as well as
education and adoption of Free Software in Ireland.

#FoE: IFSO on MEPs

#uku: Report about Europarl lobbying work done by the circle around
IFSO/FSFE-IE

#Wse: Introduction to IFSO's work on software patents.

#Waw: News report of an Irish newspaper

#flm: Comprehensive information on the Irish presidency collected by Hill &
Knowlton, a law firm residing in Belgium.

#viW: As the last item on the competitiveness agenda, it is stated that the
IE presidency is %(q:committed to achieving progres on IP issues to
create an environment that encourages business to invest further in
research and development)

#oci: Comments of Irish Business association IBEC on the Irish presidency

#poW: Complete position paper

#Wc2: IE Presicency 2004

#fls: Official Website

#iaw: Information about the %(q:Lisbon Agenda), to which the Irish
presidency is said to be committed.   This agenda is mainly about
meeting statistical targets, including amount of public and private
R&D spending, numbers of patents etc.  It can also serve as an action
program for EU institutions which are risking to run out of work as
there is little left to be %(q:harmonised) in the %(q:internal
market).  Moreover, the %(q:Lisbon Agenda) serves business
associations as a means of pushing for policies which strengthen the
position of the managing boards of companies in the name of
competitivity and which would be to unpopular to push for at a
national level.   E.g. %(q:competitivity) is often used to argue in
favor of lax environmental requirements, abolition of welfare
regulation as well as commercialisation of culture, research and
education.

#yas: Corporate patent lawyers are regularly claiming that software
patentability is needed in order to fulfill the targets of the
%(q:Lisbon Agenda).

#aee: European Commission says acceding countries do not meet the targets of
the Lisbon Agenda with respect to innovativity.  In particular, they
file too few patents at USPTO and EPO.

#val: Reserve a room in a Dublin Hotel in March

#eIa: Fix a date for the planned FFII tour through European capitals.

#oel: List contact addresses of Irish politicians

#pww: Concerned programmers need to know to whom to write and how to write.

#obW: Politics on this point has to become as transparent as possible.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatie ;
# txtlang: en ;
# multlin: t ;
# End: ;

