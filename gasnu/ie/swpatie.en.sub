\begin{subdocument}{swpatie}{Ireland and Software Patents}{http://swpat.ffii.org/gasnu/ie/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/05/16 by FFII\footnote{\url{http://lists.ffii.org/mailman/listinfo/traduk}}}{Irish members of the European Parliament have to a large part voted against software patents (i.e. for amending the software patent directive) in September 2003.  Irish ICT associations have been more sceptical of this directive than some of their counter-parts of other countries.  Ireland took the EU presidency in the first half year of 2004.  The Irish presidency supported efforts of the national patent administrators to scrap the European Parliament's amendments without discussion at the ministerial level.  The Irish presidency said on its website that it was ``sponsored by Microsoft''.  The Presidency's effort was supported by FUD from a group of patent-centered companies which were heard by the Irish Parliament and parrotted by the Irish press in early May.  Ireland provides a tax haven for patent royalty fees and harbours (partially for this and similar reasons) much of the finance flow of large US companies, so that Ireland is often called ``Europe's largest software exporting country''.  Trinity College in Dublin is a leading European facility for training patent lawyers.}
\begin{sect}{intro}{introduction}
Publications of the Irish Presidency as of January suggested that the legislative process in the Irish ministeries, as elsewhere, is dominated by faithful followers of the patent movement.  On the other hand the IE government may be seen as avoiding the inevitable political conflicts which are associated with the software patent directive, putting it behind the Community Patent and IP Enforcement directives, so as not to create tensions between Council and Parliament in a year in which the adoption of the EU Constitution and other contentious issues are already creating enough problems.  It could be expected that the Irish presidency will try to keep a low profile and then try to align the Council on a pro-software-patent position in mid May, if that is seen as feasible by then.
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf EU Council 2004 Proposal on Software Patents\footnote{\url{}}}}

\begin{quote}
The Council of Ministers has reached political agreement on a paper which contains alternative suggestions to the amendments on the directive ``on the patentability of computer-implemented inventions'' passed by the European Parliament (EP). In contrast to the EP version, the council version permits unlimited patentability and patent enforceability. Following the current version, ``computer-implemented'' algorithms and business methods would be inventions in the sense of patent law, and the publication of a functional description of a patented idea would constitute a patent infringement. Protocols and data formats could be patented and would then not be freely usable even for interoperability purposes. These implications might not be apparent to the casual reader.  Here we try to decipher the misleading language of the proposal and explain its implications.
\end{quote}
\filbreak

\item
{\bf {\bf \url{http://kwiki.ffii.org/Dail0405En}}}

\begin{quote}
Pro-Patent companies talking to Irish Parliament
\end{quote}
\filbreak

\item
{\bf {\bf ICT Ireland\footnote{\url{http://www.ictireland.ie/}}}}

\begin{quote}
Irish ICT industry lobbying group, member of EICTA

see also EICTA and Software Patents\footnote{\url{http://localhost/swpat/gasnu/eicta/index.en.html}}
\end{quote}
\filbreak

\item
{\bf {\bf Sponsors of the Irish Presidency\footnote{\url{http://www.eu2004.ie/sitetools/sponsorship.asp}}}}

\begin{quote}
List of companies who sponsor the Irish Presidency.  Apparently it has become customary in the EU that presidencies are sponsored by large companies.  This time one of them is Microsoft.
\end{quote}
\filbreak

\item
{\bf {\bf Brochure on Irish Priorities for Competitiveness Council\footnote{\url{http://www.eu2004.ie/templates/document_file.asp?id=2054}}}}

\begin{quote}
This 16 page PDF brochure contains two almost identical, instances of the word 'software'. Page 6:\begin{quote}
{\it A knowledge-based economy must have effective instruments and laws to protect and promote investment in research and innovation. The enforcement of intellectual property rights, a common regime for the community patent, community trade mark regulations, and the protection of software inventions form an important underpinning to the research and knowledge-based economy. We will ensure that progress is made in these areas, which play an essential part in supporting our adjustment to new and higher value economic activities.}
\end{quote}

and page 12:\begin{quote}
{\it A knowledge-based economy must have effective instruments and laws to protect and promote investment in research and innovation. A harmonised approach to enforcement of intellectual property rights, EU wide protection of patents afforded by a community patent, and the protection of software inventions, form an important underpinning of the research and knowledge-based economy. We will ensure that progress is made on these areas which play an essential part in supporting our adjustment to new and higher value economic activities. The Community Patent is important for European industry and must be available to firms at reasonable cost. The Irish Presidency will make every effort to ensure its adoption. With a view to completing the internal market in the area of intellectual property rights, an important initiative is the Directive on enforcement which aims to harmonise enforcement measures in respect of those IP rights which have already been substantively harmonised at Community level. It is the intention of the Irish Presidency to make every effort to achieve a common position within the Council on this initiative.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Harney 2004-01: Ireland's priorities for the Competitiveness Council\footnote{\url{http://dbs.cordis.lu/cgi-bin/srchidadb?CALLER=NHP_EN_NEWS&ACTION=D&SESSION=&RCN=EN_RCN_ID:21416}}}}

\begin{quote}
Mary Harney (Irish deputy PM) has given a speech launching a brochure on Ireland's priorities for the Competitiveness council
\end{quote}
\filbreak

\item
{\bf {\bf Irish presidency: Provisional agendas for ministerial meetings\footnote{\url{http://register.consilium.eu.int/pdf/en/03/st16/st16175.en03.pdf}}}}

\begin{quote}
``Political agreement'' on the software patents directive is scheduled as item 8 on the provisional agenda for the Competitiveness Council on 17-18 May (top of page 11), but does not appear on the schedule for the 11 March meeting.  What is on the schedule for March is political agreement on the IP enforcement directive.  There is also activity planned for both meetings on the Community Patent, with an EU regulation on the Community Patent scheduled for agreement in March, and the establishment of the Community Patent Court apparatus scheduled for May.
\end{quote}
\filbreak

\item
{\bf {\bf Irish Free Software Organisation\footnote{\url{http://ifso.info}}}}

\begin{quote}
An affiliate organisation of FSF Europe, coordinated by a 4-person committee chaired by Ciaran O'Riordan
\end{quote}
\filbreak

\item
{\bf {\bf FSFE Ireland\footnote{\url{http://mail.fsfeurope.org/mailman/listinfo/fsfe-ie/}}}}

\begin{quote}
Since June 2003 this archived list has been collaborating on issues such as software patents, and the European Copyright Directive, the Intellectual Property Rights Enforcement Directive as well as education and adoption of Free Software in Ireland.
\end{quote}
\filbreak

\item
{\bf {\bf IFSO on MEPs\footnote{\url{http://ifso.info/documents/swpats-meps-00.html}}}}

\begin{quote}
Report about Europarl lobbying work done by the circle around IFSO/FSFE-IE
\end{quote}
\filbreak

\item
{\bf {\bf IFSO Swpats Project\footnote{\url{http://ifso.info/projects/swpats.html}}}}

\begin{quote}
Introduction to IFSO's work on software patents.
\end{quote}
\filbreak

\item
{\bf {\bf MEP letter from Ciaran O'Riordan\footnote{\url{http://www.compsoc.com/\~coriordan/docs/dear_mep/}}}}
\filbreak

\item
{\bf {\bf ENN 2003-11-27 news on Council decision\footnote{\url{http://www.enn.ie/frontpage/news-9383014.html}}}}

\begin{quote}
News report of an Irish newspaper
\end{quote}
\filbreak

\item
{\bf {\bf Irish Presidency: Glossary and links\footnote{\url{http://www.hillandknowlton.be/irish_presidency/glosslinks.html}}}}

\begin{quote}
Comprehensive information on the Irish presidency collected by Hill \& Knowlton, a law firm residing in Belgium.
\end{quote}
\filbreak

\item
{\bf {\bf Priorities: Competitiveness\footnote{\url{http://www.hillandknowlton.be/irish_presidency/competitiveness.html}}}}

\begin{quote}
As the last item on the competitiveness agenda, it is stated that the IE presidency is ``committed to achieving progres on IP issues to create an environment that encourages business to invest further in research and development''
\end{quote}
\filbreak

\item
{\bf {\bf IBB - Irish EU Presidency 2004\footnote{\url{http://www.ibec.ie/Sectors/IBB/IBBDoclib3.nsf/vLookupHTML/EU_Presidency?OpenDocument}}}}

\begin{quote}
Comments of Irish Business association IBEC on the Irish presidency
\end{quote}
\filbreak

\item
{\bf {\bf Complete position paper\footnote{\url{http://www.ibec.ie/Sectors/IBB/IBBDoclib3.nsf/6017c7ddb4ce19fb80256d8f003c8abb/f2d95876b4d455f980256d900030f43f/$FILE/A\%20Growth\%20Agenda\%20for\%20Europe\%20.pdf}}}}

\begin{quote}
PDF
\end{quote}
\filbreak

\item
{\bf {\bf IE Presicency 2004\footnote{\url{http://www.eu2004.ie/}}}}

\begin{quote}
Official Website
\end{quote}
\filbreak

\item
{\bf {\bf EurActiv.com Portal - LinksDossier - Lisbon Agenda\footnote{\url{http://www.euractiv.com/cgi-bin/cgint.exe/616173-208?714&1015=9&1014=ld_innolisbon}}}}

\begin{quote}
Information about the ``Lisbon Agenda'', to which the Irish presidency is said to be committed.   This agenda is mainly about meeting statistical targets, including amount of public and private R\&D spending, numbers of patents etc.  It can also serve as an action program for EU institutions which are risking to run out of work as there is little left to be ``harmonised'' in the ``internal market''.  Moreover, the ``Lisbon Agenda'' serves business associations as a means of pushing for policies which strengthen the position of the managing boards of companies in the name of competitivity and which would be to unpopular to push for at a national level.   E.g. ``competitivity'' is often used to argue in favor of lax environmental requirements, abolition of welfare regulation as well as commercialisation of culture, research and education.

Corporate patent lawyers are regularly claiming that software patentability is needed in order to fulfill the targets of the ``Lisbon Agenda''.

see CEOs of big telcos sign letter against Europarl Amendments\footnote{\url{http://localhost/swpat/lisri/03/telcos1107/index.en.html}}
\end{quote}
\filbreak

\item
{\bf {\bf EurActiv 2003-09-03: CEC: Big innovation effort needed in acceding countries\footnote{\url{http://www.euractiv.com/cgi-bin/cgint.exe/616173-208?204&OIDN=1506099&-tt=}}}}

\begin{quote}
European Commission says acceding countries do not meet the targets of the Lisbon Agenda with respect to innovativity.  In particular, they file too few patents at USPTO and EPO.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Questions, Things To Do, How you can Help}
\begin{itemize}
\item
{\bf {\bf \url{}}}
\filbreak

\item
{\bf {\bf Reserve a room in a Dublin Hotel in March}}

\begin{quote}
Fix a date for the planned FFII tour through European capitals.

see also FFII: Software Patent Events 2004\footnote{\url{http://localhost/swpat/penmi/2004/index.en.html}}
\end{quote}
\filbreak

\item
{\bf {\bf List contact addresses of Irish politicians}}

\begin{quote}
Concerned programmers need to know to whom to write and how to write.

Politics on this point has to become as transparent as possible.

see also Analysis of the European Parliament's Vote of 2003/09/24\footnote{\url{}}
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

