<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Thales and Software Patents

#descr: The french weapon maker Thales has only recently applied for at best a
handful (3 until 2003) of software patents at the European Patent
Office.  It is clear that Thales is not among the winners of the
software patents arms race and has more to lose from it than most
players.  Yet the chief patent lawyer of Thales, Thierry Nguyen, has
been one of the most active promoters of software patentability in
Europe.  He has been present at many conferences and spoken in the
name of his company as well as the whole industry of Europe, demanding
that programs should be directly patentable.  Nguyen's argumentation
was limited to the usual belief statements of the patent law
community, such as that patents are good and SMEs in particular need
them.  Some people at the R&D department of Thales are unhappy about
Mr. Nguyen's activities, but they do not have a say in the company's
policy on patent matters.

#Saa: Thales Software Patents at the European Patent Office

#ary: At this occasion Nguyen stated his beliefs in the name of Europe's
industry.  Nguyen cited an example of an SME which relies on software
patents.  The CEO of this SME later wrote a letter to the Europan
Parliament in which it protested against these assertions and made
clear that its company policy is not dependent on the availability of
patents for software.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatthales ;
# txtlang: en ;
# multlin: t ;
# End: ;

