# -*- mode: makefile -*-

swpataniebler.de.lstex:
	lstex swpataniebler.de | sort -u > swpataniebler.de.lstex
swpataniebler.de.mk:	swpataniebler.de.lstex
	vcat /ul/prg/RC/texmake > swpataniebler.de.mk


swpataniebler.de.dvi:	swpataniebler.de.mk
	rm -f swpataniebler.de.lta
	if latex swpataniebler.de;then test -f swpataniebler.de.lta && latex swpataniebler.de;while tail -n 20 swpataniebler.de.log | grep -w references && latex swpataniebler.de;do eval;done;fi
	if test -r swpataniebler.de.idx;then makeindex swpataniebler.de && latex swpataniebler.de;fi

swpataniebler.de.pdf:	swpataniebler.de.ps
	if grep -w '\(CJK\|epsfig\)' swpataniebler.de.tex;then ps2pdf swpataniebler.de.ps;else rm -f swpataniebler.de.lta;if pdflatex swpataniebler.de;then test -f swpataniebler.de.lta && pdflatex swpataniebler.de;while tail -n 20 swpataniebler.de.log | grep -w references && pdflatex swpataniebler.de;do eval;done;fi;fi
	if test -r swpataniebler.de.idx;then makeindex swpataniebler.de && pdflatex swpataniebler.de;fi
swpataniebler.de.tty:	swpataniebler.de.dvi
	dvi2tty -q swpataniebler.de > swpataniebler.de.tty
swpataniebler.de.ps:	swpataniebler.de.dvi	
	dvips  swpataniebler.de
swpataniebler.de.001:	swpataniebler.de.dvi
	rm -f swpataniebler.de.[0-9][0-9][0-9]
	dvips -i -S 1  swpataniebler.de
swpataniebler.de.pbm:	swpataniebler.de.ps
	pstopbm swpataniebler.de.ps
swpataniebler.de.gif:	swpataniebler.de.ps
	pstogif swpataniebler.de.ps
swpataniebler.de.eps:	swpataniebler.de.dvi
	dvips -E -f swpataniebler.de > swpataniebler.de.eps
swpataniebler.de-01.g3n:	swpataniebler.de.ps
	ps2fax n swpataniebler.de.ps
swpataniebler.de-01.g3f:	swpataniebler.de.ps
	ps2fax f swpataniebler.de.ps
swpataniebler.de.ps.gz:	swpataniebler.de.ps
	gzip < swpataniebler.de.ps > swpataniebler.de.ps.gz
swpataniebler.de.ps.gz.uue:	swpataniebler.de.ps.gz
	uuencode swpataniebler.de.ps.gz swpataniebler.de.ps.gz > swpataniebler.de.ps.gz.uue
swpataniebler.de.faxsnd:	swpataniebler.de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo swpataniebler.de-??.g3n`;source faxsnd main
swpataniebler.de_tex.ps:	
	cat swpataniebler.de.tex | splitlong | coco | m2ps > swpataniebler.de_tex.ps
swpataniebler.de_tex.ps.gz:	swpataniebler.de_tex.ps
	gzip < swpataniebler.de_tex.ps > swpataniebler.de_tex.ps.gz
swpataniebler.de-01.pgm:
	cat swpataniebler.de.ps | gs -q -sDEVICE=pgm -sOutputFile=swpataniebler.de-%02d.pgm -
swpataniebler.de/swpataniebler.de.html:	swpataniebler.de.dvi
	rm -fR swpataniebler.de;latex2html swpataniebler.de.tex
xview:	swpataniebler.de.dvi
	xdvi -s 8 swpataniebler.de &
tview:	swpataniebler.de.tty
	browse swpataniebler.de.tty 
gview:	swpataniebler.de.ps
	ghostview  swpataniebler.de.ps &
print:	swpataniebler.de.ps
	lpr -s -h swpataniebler.de.ps 
sprint:	swpataniebler.de.001
	for F in swpataniebler.de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	swpataniebler.de.faxsnd
	faxsndjob view swpataniebler.de &
fsend:	swpataniebler.de.faxsnd
	faxsndjob jobs swpataniebler.de
viewgif:	swpataniebler.de.gif
	xv swpataniebler.de.gif &
viewpbm:	swpataniebler.de.pbm
	xv swpataniebler.de-??.pbm &
vieweps:	swpataniebler.de.eps
	ghostview swpataniebler.de.eps &	
clean:	swpataniebler.de.ps
	rm -f  swpataniebler.de-*.tex swpataniebler.de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} swpataniebler.de-??.* swpataniebler.de_tex.* swpataniebler.de*~
swpataniebler.de.tgz:	clean
	set +f;LSFILES=`cat swpataniebler.de.ls???`;FILES=`ls swpataniebler.de.* $$LSFILES | sort -u`;tar czvf swpataniebler.de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
