<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Robert J. Hart and Software Patents

#descr: Some data and links about one of the leading figures of the British software patentability lobbying effort at the European Commission.

#vita: Robert J. Hart is a british Chartered Patent Agent, European Patent Attorney and Fellow of the British Computer Society.  Formerly the Intellectual Property Development Executive for Plessey Telecommunications.  Around 2000 he was an independent consultant in the field of intellectual property.  Consultant of the European Commission on boththe Software Directive and the Database Directive.  Consultant to WIPO and the World Bank.   Secretariat to the CAI Founders Group handling licensing of the intellectual property in the CT2 Air Interface an Senior Associate with EAG Ltd who performed a study contract for the Commission on the Member States implementation of the Software Directive.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatrhart ;
# txtlang: en ;
# multlin: t ;
# End: ;

