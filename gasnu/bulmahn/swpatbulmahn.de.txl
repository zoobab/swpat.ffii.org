<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Edelgard Bulmahn und ihre Patentrezepte für die deutschen Hochschulen

#descr: Während ihrer ersten Amtszeit 1998-2002 brachte die junge SPD-Forschungsministerin einen Gesetzesentwurf durch den Bundestag, der die Hochschullehrern zum Patentieren verpflichtet.  In zahlreichen Reden der Ministerin taucht das Wort %(q:Patente) in rosaroter Färbung auf.  Das Patentsystem ist für Frau Bulmahn offensichtlich ein Hoffnungsträger.  Es könnte erlauben, die kostspielige und sperrige öffentliche Forschung abzuschütteln (oder wenigstens durchzuschütteln) und den Staatshaushalt zu entlasten.  Die Entwürfe dazu werden Bulmahn von den wohletablierten Patentfunktionären ihres Ministeriums auf den Tisch gelegt, aber die Ministerin legt bei ihrer Abarbeitung eine auffällig euphorische Haltung an den Tag.

#g1s: geb 1951 in München, aufgewachsen in Döhren, Studium Politologie und Anglistik, Lehrerin; seit 1987 Mitglied des Deutschen Bundestages, 1991-98  Mitglied des Fraktionsvorstandes der SPD-Bundstagsfraktion; seit 1993 Mitglied des Parteivorstandes der SPD; seit 1998/10 Landesvorsitzende der SPD in Niedersachsen; 1990-94  stv. Sprecherin für Forschungs- und Technologiepolitik der SPD-Bundestagsfraktion; 1995-96 Vorsitzende des Ausschusses für Bildung, Wissenschaft, Forschung, Technologie und Technikfolgenabschätzung; 1996-98  Sprecherin für Bildung und Forschung der SPD-Bundestagsfraktion; seit 1998/10/27 Bundesministerin für Bildung und Forschung

#Bgu: BMBF-Angaben zu Bulmahn

#HzW: Heinz Maier-Leibnitz-Preis 2000 - Rede Ministerin Bulmahn

#Ent: Eine von vielen Reden der Ministerin, in denen das Leitmotiv %(q:Patente) eine zentrale Stellung einnimmt.

#BzP: BMBF-Dokument zur Hochschul-Patentverwertung

#Ece: Ein patentbegeisterter Biotech-Institutsleiter spottet über die deutschen Hochschulen und preist die amerikanische Praxis des %(q:Technologietransfers), d.h. hochschuleigener Patentverwertung.   Allerdings warnt er, dass die deutschen Hochschulen kaum in der Lage sein werden, Bulmahns Rezepte dem amerikanischen Vorbild entsprechend zum Funktionieren zu bringen.

#NuW: Norbert Hauser gegen Gen-Patente

#Est: Ein für Bulmahns Ressort zuständiger CDU-Abgeordneter, der in zahlreichen Presseerklärungen Bulmahn als unfähig darzustellen sucht und hier sich zu einem Patent-Thema äußert.  Allerdings hängt beides soweit erkennbar nicht eng miteinander zusammen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbulmahn ;
# txtlang: de ;
# multlin: t ;
# End: ;

