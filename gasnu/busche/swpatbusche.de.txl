<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Prof. Dr. Jan Busche und Softwarepatente: Gute Noten für großzügige Patentrichter

#descr: Prof. Dr. Jan Busche, Inhaber des Lehrstuhls für Bürgerliches Recht und Gewerblichen Rechtsschutz der Heinrich-Heine-Universität Düsseldorf, Autor zahlreicher Artikel in den Fachzeitschriften der Patentjuristenzunft und Redner auf Patentanwaltskongressen.  Busches Artikel und Reden schildern und die Entwicklung der Rechtsprechung bezüglich Softwarepatenten und versuchen sie zu legitimieren, obwohl die Analyse manchmal durchaus Widersprüche offenlegt.  Über solche Widersprüche hilft Busche seinen Lesern dann meist durch schlichte wertende Adjektive hinweg.

#Vim: Dort hält auch Dr. Meier-Beck vom BGH-Patentsenat Vorlesungen, u.a. über Themen wie die neueste Swpat-Rechtsprechung (z.B. Fall %(q:Logikverifikation)).  Vorlesungsskripte und Referenzmaterialien scheinen grundsätzlich nur im MSWord-Format angeboten zu werden.

#Bna: Bericht des Gastgebers Prof. Dr. Jan Busche über die Düsseldorfer Patentrechtstage 2002.  Dort sprachen neben zahlreichen Patentanwälten auch Gert Kolle (BMJ), Wilfried Anders (EPA) und Raimund Lutz (BMJ).  Der Richtlinienvorschlag der EU für die Patentierbarkeit von Organisations- und Rechenregeln vom 2002-02-20 war ebenfalls ein Thema.

#Bwi: Busche Swpat-Seminar 2001

#Esd: Ein Referat von Felix Klopmeier, einem Studenten Rechtwissenschaft im 6. Semester in einem Seminar unter Leitung von Prof. Jan Busche, zum Herunterladen angeboten vom Internetsicherheitsreferat im BMWi.  Klopmeier benotet ganz nach der Manier seines Lehrmeisters die deutsche Rechtsprechung nach ihrer Folgsamkeit gegenüber dem %(q:europäischen Recht) (d.h. dem Europäischen Patentamt).  Wer dem EPA folgt, erhält gute Noten, wer abweicht erhält schlechte.  Begründungen der Noten sind von ähnlicher Qualität wie beim Lehrmeister.  Zunächst wird die gesetzeskonforme Praxis der 70er und 80er Jahre durch allerlei recht oberflächliche Einwände angegriffen, die darauf hin deuten, dass Klopmeier den Begriff der technischen Erfindung nicht versteht.  So schreibt er z.B. %(bc:Kolle lehnt .. die Patentierbarkeit von Software ab, da jede Programmierung eines Computers sich immer im Rahmen des bestimmungsgemäßen Gebrauchs einer Datenverarbeitungsanlage bewegt. Auffällig ist, daß die Schutzmöglichkeit für hardwaremäßig realisierte Programme hiervon grundsätzlich nicht berührt sein soll. Die Meinung Kolles ist jedoch insoweit nicht nachvollziehbar.  %(tp|Ein und derselbe informatische Hintergrund|das Programm) soll bei Einbettung in ein %(tp|technisches Umfeld|Hardware) patentwürdig sein und bei Einbettung in ein Anwendungs- oder Betriebsumfeldumfeld nicht mehr. Diese Einschätzung ist unlogisch und kann heute als überholt angesehen werden.)  Hiermit offenbart Klopmeier, ähnlich wie Busche in einem etwas früheren Artikel, eine besondere Virtuosität im Missverstehen von Kolles Artikel von 1977.  Kolle erklärt darin eigentlich deutlich genug, dass ein eventuell in Frage kommender Hardware-Anspruch nicht den Algorithmus sondern ein neue Materialisierung betreffen muss, und Koles Ausführungen sind für einen Informatiker heute ebenso wie damals besser verständlich als die Begrifflichkeiten von Klopmeier und Busche.  %(q:Überholt) ist Kolle allenfalls inswoweit, wie man das EPA zum Maßstab der Wahrheit macht.   Dieser Methodik folgende wendet Klopmeier EPA-Worthülsen wie %(q:technischer Effekt) und %(q:als solches) so lange hin und her, bis nur noch die %(q:grundsätzliche Technizität aller Software) als unumstößlicher Lehrsatz übrig bleibt, wobei jedoch auch Klopmeier selbst nicht mehr so recht weiß, was mit diesem Lehrsatz gemeint sein soll.  Eine Messung der eigenen Empfehlungen anhand der erteilten Softwarepatente findet ebenso wenig statt wie eine Betrachtung der Auswirkungen dieser Patente.  Allerdings weist Klopmeier gegen Ende auf die in den USA zum Politikum gewordenen %(q:Geschäftsmethoden) hin und versichert, am EPA werde man derartige Peinlichkeiten zu vermeiden wissen.  Auch hier zeigt Klopmeier, dass er sich nicht die Mühe gemacht hat, die vom EPA erteilten Patente anzuschauen.  Immerhin enthält der Artikel ein interessantes Literaturverzeichnis.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbusche ;
# txtlang: de ;
# multlin: t ;
# End: ;

