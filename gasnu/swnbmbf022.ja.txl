<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Egl: ... Damit wird der ganze wissenschaftliche Bereich in eine Spannungszone verwandelt, die irgendwo zwischen der Aufdringlichkeit eines Versicherungsvertreters, der Hartnäckigkeit eines GEZ-Vertragspartners und dem Erfolgsdruck eines Vertragshandy-Verkäufers angesiedelt sein könnte. ...  Der Zweck der Anderstartigkeit der staatlichen Forschung wird immer weiter ignoriert und folglich wird auch diese Forschungsvariante selbst immer weiter erodieren, zu unser aller Nachteil in dieser Volkswirtschaft.

#Asu: 摘要

#phe: phm: Re: Hochschulpatentgesetz verabschiedet

#Tst: Tauss: Re: Hochschulpatentgesetz verabschiedet

#Hee: phm: Hochschulpatentgesetz verabschiedet

#DWf: 同省も計算法の特許性を%(pr:主張)している。合わせて見れば，大学のソフト等の情報作品は今度発表前に知財部に提出され，「価値あり」と判定されたら専有的な商用ソフトとしての道を歩まざるを得なくなるでしょう。

#AlM: Am heutigen Donnerstag tritt eine Änderung des Arbeitnehmererfindungsgesetzes in Kraft. Damit werden Hochschulen das Recht erhalten, Erfindungen ihrer Mitarbeiterinnen und Mitarbeiter zum Patent anzumelden und damit die wirtschaftliche Verwertung zu forcieren.

#WWi: 2001年四月私達は大学特許促進法の危険性に対する%(bf:警告)を発表しましたが，連邦議会はそれを考慮に入れず特許運動の願いをかないました。新しい%(pe:新事聲明)の中で連邦科学省は次の様に説明している:

#BrW: BMBF: Verwertung forciert

#descr: A new German Law authorises and encourages universities to patent the results of their employees's research and to oblige their employees to cooperate in this endeavor.  The economic considerations behind this law seem questionable not only because it creates dangers for the often very successful cooperative development of software and certain technologies in the university sector.  A public discussion with the media and science expert of the Federal Parliament, Jörg Tauss, did not help to diminish our concerns.  At least a law to guarantee the freedom of software and cooperatively developped technologies will become necessary if software patents are legalised.

#title: ドイツ新法律で大学研究の特許化が義務付けられている

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swnbmbf022 ;
# txtlang: ja ;
# multlin: t ;
# End: ;

