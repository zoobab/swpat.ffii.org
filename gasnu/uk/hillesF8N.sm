**LU22 cover


The UK Patent Office: A law unto itself


**pull quote 
Software patents are a tax on our common knowledge, stifle innovation and competition, and offend common sense

**pull quote 

"The patent system is at best an irrelevancy for most small firms" - The Intellectual Property Research Programme


In January 2001, the UK Patent Office (UKPO), called for participation by software professionals in a consultation on UK policy towards software and business method patents. An overwhelming majority of the respondents came out strongly against the patenting of software and of business processes. In a press release dated 12 March 2001, Patricia Hewitt, then E-Minister, made an unequivocal statement of the government's policy "The majority of those who responded agree with the Government and oppose patents for ways of doing business on the internet." The press release summarised the key conclusions of the consultation as follows: "there should be no significant change to the patentability of software; the law is not clear enough. Urgent European action to clarify is needed; and business methods should remain unpatentable."


The conclusions themselves are couched in moderate tones and would suggest a policy that concurred with the views of both the E-Minister and the vast majority of respondents to the consultation, broadly against the extension of patents to software and business methods. Conveniently, however, the wording of the press release passes UKPO responsibility for future policy decisions to Europe. In the European consultations the UKPO's position has been to argue for the "status quo" as it is in Britain, where patents on software "inventions" have been granted for the last twenty years, and where many would contend that patents have been granted on "inventions" that are suspiciously akin to "business methods". Those who believed that the Patent Office were convinced of the arguments against software patents were the victims of a softly worded deception. The UK Patent Office has argued in Europe for the widest possible definition of what is allowable as a software patent. In other words, the consultation has had no influence whatsoever on the policy of the Patent Office.


Very few software professionals who have studied the implications of software patents can find any virtue in the concept. Even Bill Gates has said, "If people had understood how patents would be granted when most of today's ideas were invented and had taken out patents, the industry would be at a complete standstill today." (quoted by Larry Lessig).

**subhead
Action to clarify


The UK government, as represented by the UKPO in Brussels, has pushed more than any other for the adoption of the European directive, "On the patentability of computer implemented inventions", and for the clauses therein (as expressed at the European Council Intellectual Property and Patents Working Group meeting of 13 June 2002). Far from representing the position broadly understood by the announcement of 12 March 2001, or the views of the software professionals who responded to the consultation, the UKPO has taken a positive stance in favour of software patents. The only article of the EU directive which was taken exception to by the UK representatives was Article 5, which was included as a mild compromise for opponents of software patents, and forbids literal patent claims to computer programs, thus making it difficult to force internet providers to remove programs from servers.

The European directive itself is a lawyers' delight, containing as it does a maze of broad and fuzzy definitions of what can and cannot be patented, most of them open to reinterpretation. A computer program can be patented if it is firstly, 'technical', and secondly, new, inventive and industrially applicable. What is meant by technical is not clearly stated, and appears to cover everything and anything . Another difficult concept has been the "as such" criteria presented in EPC Article 52(3). The Article states that the items listed in Article 52(2) are not patentable 'as such' - a qualification that embraces any number of re-interpretations. The definitions are incomprehensible to the layman or software professional, but just as worryingly, the technical implications of any particular patent applied through these rules, will be equally incomprehensible to the lawyers and patent officers who grant the patent. Clarifiaction is required. The arguments have already begun, and the directive is not yet implemented.


Interestingly, the European directive also dismisses the results of its consultations with the following explanation. "Many of the responses supporting a more restrictive approach than at present, with fewer patents being granted, were transmitted through an open forum set up by the "Eurolinux Alliance", a group of companies and other entities supporting the development of open source software such as Linux. Although this group numerically dominated (90%) the response, the major sectoral bodies representing the information and communication technology industries, as well as many of the Member States, all supported the approach put forward by the discussion paper. Some responses argued for eligibility for patents to be widened in line with the practice in the US." The major sectoral bodies, of course, refers to the patent arms of the large software comapnies. One of the many ironies of the situation is that one of the greatest proponents of software patents is IBM, a company that is reaping huge rewards from Linux and other open source software.


The key sections of the press release of 12 March, 2001, are the statements that "there should be no significant change to the patentability of software"; that "the law is not clear enough", and that "urgent European action to clarify is needed". "Harmonisation" and the "urgent European action to clarify" is provided by the UKPO's allies on the EU Commission's General Directorate for the Internal Market. Substantively, patent law was harmonised across Europe in the period 1973/1977, by the European Patent Convention (EPC) and its translation into the UK Patents Act. But the UK patent establishment has another agenda, and has found the European route to be a particularly useful means of sidestepping opposition to software patents, in this instance to legitimise its current practise, which conforms to UKPO precedent, if not the current European or UK law, and to sanction the patentability of software, making it impossible to rid industry of the crippling effects of software and business method patents for years, perhaps decades, to come.


**subhead
Yes, Minister


Hartmut Pilch, of the Foundation for Free Information Infrastructure, (FFII), a leading resource for information on European software patents, argues that "there is not a single economic study, field survey or theoretical analysis, whether done in the UK, by the EC or elsewhere, that concludes in favour of software patentability", and believes that. although the UKPO frequently holds consultations, "when the "consultation" fails to produce the desired support, the UKPO simply reinterprets the results and does what it had planned to do anyway."


The E-Minister's basic policy goal was clear, says Pilch: "If anything about software can be patented, that must be something related to advanced machinery and physical phenomena. There should be something new in the hardware. Software ideas for known general-purpose computers, especially those related to social phenomena such as language and business, must in any case be unpatentable." Dr. Kim Howells, then Consumer Affairs Minister stated that, "The patent system is there to stimulate innovation and benefit the consumer. This is the test we have applied to determine what should, and should not, be patentable in the fields of computer software and ways of doing business."


Meanwhile, the UKPO, under the guise of consultation with industry experts, patent lawyers and other professionals with a vested interest in promoting patent applications, is, according to Pilch, "pushing for a regime in which business methods, spell-checking methods and in fact anything that is described in computing jargon is considered technical and patentable." In Pilch's view the UKPO "is not involved in promoting innovation ... but in imposing transaction costs for the benefit of patent lawyers and patent officials. They are thus transferring wealth from the productive sector of the economy to the nonproductive sector; away from job creation, towards making bloated sheltered professions even richer."


The UKPO makes no bones about its attitude to software patents. EEtimes carried an interview in September 2001, with Peter Hayward, head of the Patent Office's electrical division in which he threatened that "a decision is urgently needed [from the European Patent Covention (EPC)] because the US has already recognised software patents and even business method patents", and the EPC "provides no guidance on the validity of these concepts ... While this gulf persists, there is some confusion over what software patents can be sought and enforced." Hayward said: "If it does not happen, we will attempt to clarify European law by other routes." 

When it was suggested that some companies have called for an alternative protection mechanism for software, other than patents. Hayward said: "Most are against it. People say software has a short lifetime but a lot of the basic principles go on for a long time." The sensible alternative is copyright, which is unambiguous and does not confer arbitrary monopolies on mathematical techniques and vague business methodologies. All the research suggests that the argument that "most are against it" is at best contentious. Copyright is the conventional means to license software, protects the rights of the creator, and allows the proper exchange of ideas. Patents for software are as inappropriate as they would be for artistic endeavour, and benefit nobody but the lawyers, the patent officers and the very few companies that can afford to fund and enforce them.


A common contention of those with an interest in promoting software patents is that the opponents are restricted to free software advocates. On the contrary, software patents are opposed by SMEs, software houses, professionals and academics from every corner of the computing industry, and for very good reasons. Software patents are a tax on our common knowledge, stifle innovation and competition, and offend common sense. The advocates of software patents are seldom those who understand and write the software, who know the intricacies of the code, and how it gets that way. Far from Howell's, and therefore the government's, statement of intent, that "the patent system is there to stimulate innovation and benefit the consumer", software patents are a tax on innovation, and provide no benefit for the consumer.

**subhead
Contrary to our laws


A patent confers a monopoly on an invention, idea or product for a given period of time, (usually 20 years), with the notion that this protects the inventor from appropriation of his or her invention, and allows the invention to be released into the public realm to increase the potential for the dissemination of ideas. Patents have been part of British law since the 15th century when 'Letters Patent' were granted as favours from the Crown, and have always been subject to controversy and charges of abuse. In 1610, James I was forced to revoke all existing patents and declare that "monopolies are things contrary to our laws." The theoretical justification for patents, like copyright, is that they protect the 'little man', the inventor or creator, from the predatory instincts of larger commercial entities, and such protection is of ultimate benefit to the community. A wider justification is that patents encourage innovation, and the propagation of new ideas and inventions, and spread knowledge of new inventions for the greater good of all.


Such a view could be described as the common wisdom, so it is revealing to discover that research does not substantiate this assumption. In particular, one can refer to the wide ranging UK-based Intellectual Property Research Programme (1996-1999), sponsored by the Economics and Social Research Council, the Department of Trade and Industry and the Intellectual Property Institute, which concluded that "patents are only one method of appropriating valuable knowledge. Know-how and confidentially are more important in many commercial sectors." The project was undoubtedly initiated to project the concept of patents and Intellectual Property (IP) but, after conducting surveys and interviews involving 2600 firms, the report concluded that "the patent system gives SMEs no help in innovating. It neither fosters nor protects their innovation. The patent system is at best an irrelevancy for most small firms", including those that operate in the sectors of industry that the patent industry considers especially relevant, (software, mechanical engineering, electronics and design).


Most companies prefer informal methods of protecting their IP because "they are successful, cheaper and within the control of the company." The principal method of maintaining confidentiality is through "working with customers, suppliers and employees who can be trusted." The greatest perceived threat to proprietary know-how is "loss of key people"; rather than "loss through copying by the competition", and such threats are not seen as particularly damaging. The patent industry is largely irrelevant to the objectives of the vast majority of companies, and provides little practical service. The research "finds no evidence that SMEs use the patent system to access new technology, even though making information public is a basic tenet of the system", and observes that the patent industry is a field "dominated by lawyers."


The prime beneficiaries of the current patent system are the large companies, the corporates that can afford to prosecute licenses on their monopoly patents, and the lawyers who are employed to enforce them. The common view that patents are probably a good thing, and protect the rights of the individual against the greater forces of industry, is less than complete. More profoundly, the patent office is either blissfully unaware of its lack of effectiveness, or is actively working against its public remit, which is to promote innovation through the interchange of information and ideas. Most companies ignore the patent industry as an unnecessary and expensive diversion - technology moves too quickly, and it is more important to be quick on your feet - and a small minority in a limited range of industries use patent monopolies as a revenue stream, guaranteed by an elite phalanx of lawyers who know their way around the.system, and as a means to inhibit competition. Software is best protected by copyright, which prohibits the appropriation of somebody else's work. Patents prohibit the proper use of the common language of software.


Applying for a patent is a lengthy and costly business. The patent application is drafted and enforced by patent lawyers, and patent databases have to be searched for conflicting patents, an act that even patent officials acknowledge, requires the experrtise of a patent lawyer. Even then, a patent is useless if prior art exists - proving prior art, or for that matter, defending a patent, is another expensive exercise, so those threatened with transgressing a patent will often pay up because it is the easiest option, not because it is right. Contesting a patent, even when justified, is seldom cost effective, so patents have become the preserve of those that can afford the indulgence, and an effective instrument for impeding innovation and effective competition in many areas of industry. Which is not what patents were meant to be. The last person to benefit is "the little man", and the concept of Intellectual Property is little more than a connivance to ensure that invention remains in the ownership of the big players. The UKPO is a publicly funded body, and has the unique ability to confer monopoly status on a vast range of products. In the case of software patents the UKPO is not operating in the public interest. Perhaps it is time that the patent industry and its black arts were properly exposed to public scrutiny.



**subhead
Let's go to court


The patents industry, the lawyers, the Patent Offices and the revenue earners, have a vested interest in expanding the patent market to cover areas way beyond the original remit of protecting and promoting innovation and invention.


Traditionally, patents applied to physical innovations, the machines that changed our lives. Scientific knowledge and research was open to free exchange, but the patent industry views knowledge as Intellectual Property, and Intellectual Property as a marketable commodity. While everybody understands a patent applied to a distinctive mechanical invention, it is more difficult to understand how generic knowledge, the human gene for instance, can be privatised, patented and licensed. The discovery and identification of a particular human gene is the end result of cumulative research by thousands of individuals who have contributed to the bank of knowledge that has made the final discovery possible. Should each stage of this research be made patentable, and if so, would the same discovery have been made in the first place? As for the human gene, so for software.


Software patents should be of concern to all software developers, because it is impossible to say that in writing any piece of software one has not stumbled upon a technique, a building block, or "technical invention" that has been used, and patented before. A business or an individual faces the prospect of being taxed for infringement of a patent at any point in the future, for any number of techniques used in the software. As often as not contesting a patent will cost more than paying the tax, so there is no escape. All innovation is vulnerable, but of its nature free software is especially susceptible, as was perfectly illustrated by Microsoft executive Craig Mundie at the "Open Source Convention" in July 2001. Asked by CollabNet CTO Brian Behlendorf whether Microsoft will enforce its patents against open source projects, Mundie replied, "Yes, absolutely." An audience member pointed out that many open source projects aren't funded and so can't afford legal representation to rival Microsoft's. "Oh well," said Mundie. "Get your money, and let's go to court."


The most common mistake is to believe that patents are granted for particular fruits of development, e.g. particular programs. In reality, to obtain a patent, you needn't have developed anything at all. It is enough to have had the idea that a certain thing could be done and then claim this functionality as yours. Software patents are generally characterised by this kind of "function claim". The problem is well expressed on the FFII website: "Large software projects are like monumental symphonic works. The difficulty does not consist in conceiving a melody or a chord sequence or an orchestration technique, but rather in creating a consistent and well-formed whole. If Haydn had patented creative elements of his music, Mozart and Beethoven could no longer have written any symphonies. Largescale projects are particularly adversely affected when the patenting of building blocks is allowed. The transaction costs involved in negotiating with hundreds of patent owners tend to become prohibitive, so that innovation is actually blocked and less ideas are produced than without patents." Software patents are a means of killing off the competition, and are a barrier to entering the market for small or independent developers. They are also a serious potential impediment to the development of the UK's indigenous software industry. So why is the UK Patent Office promoting them?

**Begin boxout
**header
Sole and exclusive right

Software patents are a growth industry, especially in the United States. In 1986 Compression Labs filed a US patent on JPEG (Joint Photographic Experts Group) image compression technology, but never pursued royalties. Compression Labs was purchased by Forgent Networks, a video conferencing company, in 1997.

Earlier this year, Forgent rediscovered the patent, and is actively seeking sole and exclusive right to royalties "wherever JPEGs are transmitted", including "browsers, PDAs, digital cameras, phones and scanners", the only exception being satellite broadcasting. Since 1986, JPEG has become a standard means for transmitting images across the web.

H�kon Lie, the chief technical officer of Opera Software, has advised potential licensees to ignore the patent, because "What it tries to do is patent Huffman coding in combination with runlength coding and we believe there's plenty of prior art for that before 1986".

Nonetheless, Forgent have already "reached agreement with Sony", for an undisclosed sum, and another "unnamed consumer electronic company" for the sum of $15 million, which is a considerable portion of the $22 million revenue Forgent declared last quarter. These sums were probably offset by the potential cost of fighting the patent through the courts. The licensee is always at a disadvantage in patent law.

**end boxout


**boxout
**subhead
Reverse mapping the patent tree

**body

	In August the Linux kernel developers were engaged in discussions about improving the performance of the new reverse mapping mechanism, when Alan Cox pointed out that some of the techniques being discussed were covered by patents held by SGI. A closer look by Daniel Phillips revealed that several existing Linux technologies, including the concept of reverse mapping in general, were included in the patents, issued in 1997. "Yes, it's stupid", he said, "but we can't just ignore it". At which point Linus Torvalds responded,

**the next 3 paras are an excerpt from an email 
**can you indent them and put them in a different font 
**begin diff font

Actually, we can, and I will.

I do not look up any patents on _principle_, because (a) it's a horrible waste of time and (b) I don't want to know.

The fact is, technical people are better off not looking at patents. If you don't know what they cover and where they are, you won't be knowingly infringing on them. If somebody sues you, you change the algorithm or you just hire a hit-man to whack the stupid git. 
**end diff font

	Torvalds later noted that this "may not be legally tenable advice." But affirmed his point that it's impossible to write an interesting program without running into somebody's patent. Rather than worry about it, it's better to just proceed and deal with any problems as they emerge.

	Linux Weekly News (lwn.net) made the observation that "this is probably the only rational approach; otherwise kernel hackers would go nuts trying to find and avoid all of the applicable patents. It's probably only a matter of time, though, until one of these patents bites the kernel in a big way - at least in the US. Those are the times we live in, though."

**end boxout

**boxout
**header
Key links

Foundation for a Free Information Infrastructure 
www.ffii.org/index.en.html

The UK Patent Office
www.patent.gov.uk

The EuroLinux Alliance
www.eurolinux.org

Intellectual Property Research programme
//info.sm.umist.ac.uk/esrcip/background.htm

UKPO press release
www.patent.gov.uk/about/press/releases/2001/software.htm

UKPO Conclusions
www.patent.gov.uk/about/consultations/conclusions.htm

Analysis of respondents' views
www.patent.gov.uk/about/consultations/annexc.htm

European directive on the patentability of computer implemented inventions 
www.europa.eu.int/comm/internal_market/en/indprop/com02-92en.pdf.

**end boxout









