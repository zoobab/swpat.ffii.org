<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: The UK Patent Family and Software Patents

#descr: The United Kingdom's patent matters are run nearly exclusively by the
UK Patent Office which has been a relentless promoter of software
patentability in Europe since the 1970s.

#xrW: Webb reinterprets the overwhelming rejection of software patents by
the previous consultation as a support for the UKPO's practise of
granting masses of trivial software and business method patents.  He
refers to this as %(q:the conclusion) that was reached by %(q:the
government).  He does this by creating confusing using the term
%(q:technical contribution) and by an unclear distinction between
software and business methods as well as by denigrating the law which
the UKPO has been violating as unclear.  He then procedes to ask some
patent mumbojumbo questions about the %(eb:EC Directive Proposal),
based on the assumption that we want software patents and only certain
implementational details remain to be talked about.  These questions
are likely to attract only patent lawyers, who will know how to play
the game.  They will protest that the European Commission's proposal
does not go far enough in legalising software patents and that this is
very bad for the european industry.  This will then be touted as the
%(q:voice of the european industry).  Just as the UKPO wears the
government's hat, these lawyers will wear the hats of sectoral bodies
or industry groups that don't understand patent mumbo jumbo and
therefore allowed their patent lawyers to write something in their
name.  Thus this consultation is again designed as a mobilisation
exercise for the UKPO's supporter base.  Interestingly, Robin Webb
admits what we all know:  that %(q:the government) (i.e. the UKPO and
its friends) was most active in pressing Brussels for the directive
proposal.

#ehL: 1st in a series of introductory articles to the Swpat problem in the
british Linux User magazine.

#rtL: Second in a series of introductory articles to the Swpat problem in
the british Linux User magazine.

#tif: Paul Hartnack, Comptroller General of the UK Patent Office, explains
that TRIPS does not require an extension of patentability to software

#Wis: Summary and Commentary in Digital Technology Law Journal about the
London Conference by Philipp Leith, reader in law at Queen's
University in Belfast.  Worried that patent protection may extend to
general principles and balance may be lost.  Finds that the
conference, in spite of its general pro-swpat tenor, failed to answer
various questions and thus did not manage to move the law-change
forward as intended by most of the speakers.

#lan: Speech of Ron McQuaker, Director of Exxel Consultants Ltd, Immediat
Past Chairman of the British Computer Society, explains why software
patents are a bad idea.  Quaker is the only software developper ever
heard at the only hearing which the European Commission ever conducted
before decidng to move ahead and make software patentable.

#nWn: Bolkestein & Inglewood 2002-04: Responses to a concerned programmer

#ogr: Frits Bolkestein, CEC Commissioner for the Internal Market, and Lord
Inglewood, british tory legal affairs spokesman MEP and member of
legal affairs commission, respond to a letter from Jonathan Riddell, a
software developper, by telling him that he does not understand the
law, which is based on %(q:legal decisions) taken by the European
Patent Office (EPO) during the last 25 years and which must now be
transcribed into written law in order to avoid further
misunderstandings.  Both letters claim that %(q:business methods
remain unpatentable) and %(q:programs as such remain unpatentable)
while at the same time asserting that anything that can be implemented
on hardware, i.e. all computing and business logic, must be patentable
for reasons of legal systematics.

#e0i: Bolkestein & Inglewood 2002-04: Mailing List Comment

#Wld: Criticism of the letters by Bolkestein and Eaglewood of 2002-04

#teW: Consultation Fraud for the Benefit of the Patent Law Community

#Pup: UKPO Doublespeak

#oWs: The UKPO Consultation of 2000/2001 and the %(q:Government's
Conclusions)

#eWr: UKPO Pressing the EU to Legalise Software Patents

#PmW: The UK Patent Family's arms in the European Parliament

#fcP: Lack of Invention Concept in the UK Patents Act

#uoW: The Business Method debate in the UK

#woo: The UKPO, surrounded by a circle of industrial patent lawyers, is the
powerhouse of UK and even much of EU patent policy.   THe UKPO is
currently not involved in promoting innovation by having an efficient
patent/copyright system but in imposing transaction costs for the
benefit of patent lawyers and patent officials. They are thus
transferring wealth from the productive sector of the economy to the
nonproductive sector; away from job creation, towards making bloated
sheltered professions even richer.

#onW: The Blair government is closely following and implementing the
decisions of the UK patent family.  In the European Parliament, Labour
MEP Arlene McCarthy is using UKPO doublespeak, learned from UKPO
employees and their circle of industrial patent lawyer friends, to
impose unlimited patentability of algorithms and business methods such
as Amazon One Click Shopping on Europe, while publicly proclaiming
that she is aiming for the contrary.  Similar doublespeak is also used
by other Labour (and Tory) politicians.

#Wnt: There is not a single %(es:economic study), field survey or
theoretical analysis, whether done in the UK, by the EC or elsewhere,
that concludes in favor of software patentability.  The current push
is the work of the patent lobby (patent offices, patent lawyers,
patent departments in companies) against the will of most SMEs, of
technicians, and of economists.  The French government has
%(cp:rejected) the %(dp:CEC/BSA directive proposal) as not founded on
a serious analysis on the current situation.  The United Kingdom, led
by its Patent Office, has come forward in support of the proposal and
seems, judging from its various activities, more determined than ever
in its drive to obtain from Brussels a rubber-stamp for its current
illegal practise and to impose this practise on the rest of Europe.

#Wwp: The UKPO frequently holds %(q:consultations) in order to show that it
is not acting on its own responsibility but only responding to demands
from %(q:the industry) (= patent lawyers wearing hats of companies). 
When the %(q:consultation) fails to produce the desired support, the
UKPO simply reinterprets the results and does nonetheless what it had
planned to do anyway.

#ihn: While continuing to act as the spearhead of unlimited software
patentability in Europe (see annotated links below), the UKPO has, in
response to public pressure, changed its wording to a very moderate
tone.  It achieves this by playing with differences between the normal
meaning and the patent jargon meaning of certain words and
expressions.

#Kwi: UKPO wording

#trn: outsider meaning

#noW: for consumption by politicians and citizens

#sWi: insider meaning

#dps: as understood by patent professionals

#sue: Business methods should not be patentable.

#Wlh: One-Click shopping or a solution to the problem of the travelling
salesman by linear programming etc should not be patentable.

#isy: One-Click shopping and linear programming methods should be patentable
as long as the claims refer to the operation of a machine (e.g. by
running software on a computer).

#the: The status quo should be preserved.

#ult: The present law, under which rules or organisation and calculation are
not considered to be inventions, should be applied.

#Wco: The present law should be amended or superseded, so that the patent
office can continue to grant patents for non-inventions without having
to fear a challenge.

#upr: We must not adopt the US practise.

#Stn: Unlike the US, we must refuse to grant patents on rules of
organisation and calculation.

#sti: We must blame all the embarassing problems with software patents on
certain irrelevant peculiarities of the US system (such as
reexamination, first-to-invent, the undiplomatically clear language of
the State Street decision etc) which we would in any case be unable or
unlikely to adopt.

#oln: An invention must have a technical contribution in its inventive step.

#the2: Only technical contributions to prior art (new teachings of how to
harness the forces of nature) can be patentable inventions.

#pWs: There should be no separate test of whether a technical contribution
(= an invention) is present.  Testing non-obviousness (= inventive
step) is enough.

#ena: technical

#dgf: related to harnessing the forces of nature

#oam: related to any kind of skill whose monopolisation could be
commercially rewarding and for which the patent office might want to
hire examiners

#ncW: The current legal situation is confusing and needs to be clarified.

#eWW: The incoherence in the current practise of the patent courts is
unsatisfactory and must go away.  

#rWe: The law in its present form is painfully clear.  It is standing in our
way.  We must replace it by an unclear formula, so that we can end
this embarassing discussion once for all.

#aWr: National patent laws need to be harmonised.

#aWW: National patent laws need to be made compatible with each other by
means of some common reference framework or meta-law.

#rnW: Legislative power needs to be removed from democratic sovereigns and
handed over to the international patent community.  The already
painfully clear and totally uniform national patent laws need to be
muddled, so that national lawcourts can't rely on the laws but must
look to the consensus of the international patent community for
guidance.

#aoh: In 2000/1 the UKPO called for submission of opinions on software
patents.  Most of the respondents explained why software patents are a
bad idea and the european law that forbids them should be maintained. 
The UKPO here summarises these results and interprets them as favoring
their %(q:status quo), i.e. their recent practice of granting patents
on software methods, including computerised business methods. 
Moreover it stresses that the pro-swpat opinions came from the more
important players and builds an imaginary main counter-argument,
according to which software patents unduly hurt opensource software,
only in order to dismiss that argument as unsubstantiated.  It is
difficult to imagine what kind of consultation replies the UKPO would
need to collect in order to be willing to desist from its course of
granting patents on software and business methods.

#uWk: unfortunately not all that very well-informed

#tro: the text body reads as follows

#anW: In March last year the Government published its conclusions on whether
patents should be granted for computer software or ways of doing
business, following a consultation exercise.  The central conclusion
was %(q:to reaffirm the principle that patents are for technological
innovations.  Software should not be patentable where there is no
technological innovation, and technological innovations should not
cease to be patentable merely because the innovation lies in
software.)  But an urgent need to clarify the law was identified. 
Ways of doing business should remain unpatentable.  The Government's
conclusions are available at %(URL).

#aWa: Since then the Government has been pressing the case for action at
European level, and last month the European Commission published its
long-awaited proposal for a directive, available at %(URL)

#Wjc: The Patent Office invites views on how far the proposal for a
directive meets the objectives set out in the Government's
conclusions.  In particular, we would welcome comments on:

#hoc: whether the proposal is clear;

#enW: whether it deals clearly and satisfactorily with computer-implemented
business methods where the inventive step is in the business method;

#tei: the treatment of the form of claim, in relation in particular to
claims for programs.

#lJl: We would welcome comments by Friday 7 June.  These should be emailed
to %(URL) ...

#Wno: The UKPO is cheating its superiors.  In this press release, published
through the UKPO, E-Minister Patricia Hewitt and Consumer Affairs
Minister Dr. Kim Howells say in plain language (without PTO
doublespeak) what they really want.  Hewitt says:

#dpt: Some people who responded to our consultation favour making it easier
to patent software and others see patents as a threat to development
of new software.

#soW: Our key principle is that patents should be for technological
innovations. So a program for a new machine tool should be patentable
but a non-technological innovation, such as grammar-checking software
for a word-processor, should not be.

#heb: The majority of those who responded agree with the Government and
oppose patents for ways of doing business on the internet.

#lWh: Ms Hewitt may not have thought through the idea of patenting machine
tool software and left a window for confusion open here, but her basic
policy goal is clearly outlined:  If anything about software can be
patented, that must be something related to advanced machinery and
physical phenomena.  There should be something new in the hardware. 
Software ideas for known general-purpose computers, especially those
related to social phenomena such as language and business, must in any
case be unpatentable.

#WeE: The UKPO, meanwhile, is, while trying to sound similar in rhetoric to
Ms Hewitt, actually pushing for a regime in which business methods,
spell-checking methods and in fact anything that is described in
computing jargon is considered technical and patentable.  And whereas
Mr. Howell formulated %(q:The patent system is there to stimulate
innovation and benefit the consumer. This is the test we have applied
to determine what should, and should not, be patentable in the fields
of computer software and ways of doing business,) the UKPO has never
applied this test anywhere in writing its consultation conclusions. 
It did not even try to demonstrate that software patents are in the
interest of innovation and the consumer.  It only advocated them under
the name of %(q:not changing the status quo), playing on a double
meaning of this word.  The UK government apparently, like most people,
understands software patents as not being part of the status quo in
Europe.  The Editor's Note says:  %(q:Following judicial decisions US
practice has moved towards granting patents for software and
non-technical business methods. Such divergence of practice has
prompted reconsideration of the European regime.)

#Wts: One of our friends in the UK has tried to reach Ms Hewitt in order to
tell her how the UKPO is misinterpreting her words.  This is what he
reported:

#gmn: I got a reply from the UKPO saying %(q:we have been delegated to by
the minister to handle these issues).  I emailed back explaining that
I had intended to contact the minister directly, not the patent
office, but of course no luck.

#rss: Apparently you need some special credentials in order to get access to
the E-Commerce minister.  This is in part what associations are there
for.  If you can't access the minister, access someone who has access
to someone who has access to ...  It should't be all that difficult,
as long as you remember that talking to the UKPO is a waste of time.

#tfo: The current directive project is to a large extent a result of UKPO
pressure in Brussels.  The UKPO has worked for it for 10 years.  They
will not give up easily, and they seem determined to press ahead
against the European Parliament.

#ige: This position paper of the Council was shaped mainly by the UKPO and
by the german ministerial delegation, both of which pressed to have
%(pc:program claims) legalised in addition to the European
Commission's provisions for unlimited patentability.  The UKPO now
seems determined to press ahead in the Council on the basis of this
paper.

#sae: Newspaper report quotes %(ph:Peter Hayward) as saying that the UK has
been pressing for the European Commission (CEC) to %(q:remove the
ambiguity) about software patents (%(q:the EPC doesn't even contain a
definition of %(q:programs for computers))) and that the CEC needs to
lobby the other EPC member states to accept its position.

#eeW: A fairly comprehensive account of the BSA/CEC proposal scandal.  It is
interesting to see the UKPO speaker defend his compatriot %(AH)
against well-proven allegations of BSA's [co-]authorship in drafting
the %(dp:directive proposal).  When Bolkestein published this
directive proposal at the Internal Market Council (IMC) on 2002-03-01,
the British delegation applauded, while the French expressed dismay
and most of the other delegations said very little.  The pattern was
similar at previous IMC meetings starting from 2000-12-19.  The UKPO
representatives were always the spearhead of software patentability.

#ada: When the directive proposal came into the European Parliament under
pressure from the UKPO and under direct supervision of a former UKPO
employee, Anthony Howard, the Parliament again appointed a rapporteur
who from UK Labour, Arlene McCarthy, who went far out of her way to
fight for UKPO's agenda.  McCarthy was reported to be a %(q:creature
of Anthony Howard).  During the final vote, not only all UK Labour
MEPs stood behind McCarthy, but so did most of the Conservatives and
Liberal Democrats.  The conservatives were led by Malcolm Harbour onto
this path, the Liberal Democrats by Diana Wallis.  The UK stood with
fervor against most of the rest of Europe as the software patent
country.  McCarthy was isolated in the European Socialist Group, and
even Malcolm Harbour stood on the pro software patent extreme in the
conservative block, whereas Diana Wallis (and with her most of the
Liberal Democrat group, including MEPs who had spoken against software
patents in public) acted against the written programmatic statements
of her own party base.  Apparently UK MEPs took it as a sign of
loyalty to their country to betray their constituents in favor of the
UKPO.  The Greens were the only group in UK which stood with the
majority of the European Parliament and the UK constituents.

#Wtw: The UKPO seemed to have a firm grip on the british MEPs.  This was
also expressed in two briefings, one by the UK government (i.e. the
patent office) and one by the US government (same), which were
directed only to british MEPs and which were circulated as
governmental briefings of these MEPs.

#aai: This EU %(q:industry) initiative seems to have been dominated by
players from UK, among them %(q:Open Forum Europe), a group backed
mainly by large UK companies and UK governmental institutions, asking
for software patents and %(pc:program claims) in the name of the
European %(q:opensource community).

#art: A %(fm:formalisation) and erosion of patentability criteria was
already under way in the 1970s, particularly in countries which had
inherited the English legal structure.  The UK was the only European
country to oppose explict limits on patentability at the Munich
Conference of 1973. The UK has a case-law-oriented legal culture which
attaches more importance to pragmatism and precedent than to
abstraction and theoretical grounding.  The %(ep:European Patent
Convention) (EPC) of 1973 provided only an implicit theoretical
guidance and no examples at all.  Against this background, UK patent
officials find it fairly easy to look moderate and reasonable whilst
in effect bending the law and pushing for unlimited patentability.

#tco: The %(ep:European Patent Convention) of 1973 and the %(pa:Patents Act)
of 1977 were apparently difficult to digest for the UK patent law
estabilishment.  The British delegation at the EPC conference of 1973
opposed explicit exclusions and was pacified only by the inclusion of
the %(q:as such) formula.  In the Patents Act concept of %(q:technical
invention), which is inherent in the original EPC wording, was removed
by a british rewording.  This has led to communication failures and to
proposals from the UKPO to reword Art 52 EPC in a way which, if
understood in proper EPC terminology, would read as %(bc:The following
are not inventions, unless they are inventions).  See analysis in the
%(al:Annotated Links) below.

#Wce: The push for software patents at the European Patent Office (EPO) in
the 1980s is reported by insiders to have been largely due to
difficulties in explaining concepts such as that of %(q:technical
invention) to the influential British group.  This was all the more
difficult, as the EPO had yet to create its own precedents. 
Meanwhile, the practices of the EPO and UKPO have converged, and the
UKPO is eager to change the EPC so that this new status quo -- very
close to what the UKPO asked for in the 1970s -- can be legalised and
made obligatory throughout Europe.

#W8a: An early example of UKPO patent extremism is the Nymeyer case (GB
patent number 1352742).  This was a (simple) calculation rule for
dynamically determining prices, upheld by the UKPO against the
opposition of IBM in 1980.   Back then nobody at the EPO would have
dreamt of making this kind of thing patentable and even IBM felt
compelled to uphold the rules of the European Patent Convention (EPC)
against the UKPO.  The UKPO refused and responded as follows:

#Oma: This extreme type of business method claim has later been rejected in
Britain and may even be rejected today at the UKPO, at least as long
as the basic idea is not carefully reformulated in some kind of
computing jargon, which would make the idea look %(q:technical). 
However algorithms, whether applied to business or more abstract
computing or optimisation of oil drilling, still remain the same, and
the difference between algorithms and teachings about the world of
controllable forces of nature, as worked out by continental
jurisprudence seems to have never been a major subject of discussion
in british patent jurisprudence.  Thus the decision whether a
%(q:technical) contribution is found or not seems to have been treated
by the UKPO at a level similar to where the EPO is treating it today:
claim wording and gut feeling.

#noa: Look especially at the %(pa:Patents Act of 1977, section 1 ff) or our
%(lc:local copy).  Note that the wording differs from the
corresponding passages of the %(ep:European Patent Convention) (EPC). 
According to the EPC and its first examination guidelines, the
%(e:invention) must be construed to be a synonym of %(e:technical
contribution), and the list of exclusions is a list of non-inventions.
 According to §1(1) of the UK patent act, however, any idea, whether
technical or not, is an %(q:invention), but certain %(q:inventions)
are unsystematically excluded from patentability by various adhoc
reasons, including those listed in articles 52.2-3 and 53 of the EPC. 
This means that the UK Patents Act, unlike most other national patent
laws and the EPC, does not have an implicit concept of %(e:technical
invention).  It seems thus that the reluctance to appreciate a
systematic invention concept is not new in Britain.  It is also to a
certain degree understandable that the UK patent family considers the
current law to be unclear.  The Patents Act 1977 already looks like an
ill-guided attempt to put a legal construction into clearer terms
which was not fully understood at the time.

#tun: Recent rejection of an extreme business method application by the
UKPO.  For this purpose, the %(q:technical contribution) doctrine is
used quite reasonably, but without reference to any definition.  In
other cases, courts may choose to identify a %(q:technical
contribution) based on gut feeling.

#nre: The British delegation actively opposed the inclusion of explicit
limitations on patentability at the Conference of 1973.  It was
overruled by the others, mainly thanks to French pressure.  But,
according to %(KB), it was the british pressure that finally led to
the insertion of the %(q:as such) clause into Art 52 EPC.

#aar: The UKPO was the first national patent office in Europe that followed
the European Patent Office (EPO) in its 1998 decisions to allow direct
claims to computer programs.

#mhh: A lot of the impetus for patenting software more widely in Europe
comes from the UK patent office. It had a public consultation, and
most responses were opposed to software patents, but it then wrote a
report saying people seemed to be content with them. The UK patent
office uses a term called technical effect -- this is a term that can
stretch tremendously. The result is that the UK patent office is
promoting something that looks like it helps solves the problem but
that really gives carte blanche for patenting anything.

#nTl: The European Commission did a less elegant job than the UKPO in
organising a consultation and interpreting unfavorable results in its
favor.  The summary report again was written by a British contractor,
PbT Consultants, in the spirit of the UKPO patent family.  They
invented the concept of %(q:economic majority), meaning that the
submissions to a consultation should be weighed neither by their
argumentative quality nor by the quantity of people involved, but
rather by the financial power of the institutions whose hats the
submittants are wearing.

#ofW: The UKPO is also in charge of trademarks and copyright.  It operates
almost at the level of a government ministry.  This high-profile
political position of he UKPO is also reflected in a %(pr:UKPO Press
Release of 2001-04-26), in which Anthony Murphy, Director of Copyright
at the UKPO, rants about %(q:copyright crime).  Murphy asks for school
education on intellectual property: %(bc:By bringing awareness of the
importance of copyright into our schools, tomorrow's consumers can
take their place in a community which understands, values and respects
intellectual property.)  This initiative coincides with simultaneous
PR efforts of various large corporations to portray unauthorised
copying as a heinous crime connected to all kinds of evil practices
such as drug-trafficking, and to inculcate this thinking to school
children from the earliest possible age.  See the ensuing discussion
thread.

#Kem: Software patentability is not the only bad thing which the UKPO (also
called %(q:Intellectual Property Policy Directorate)) is currently up
to. They are also involved in drafting UK regulations to implement the
EU copylock directive (EuroDMCA), and they seem determined to make
them harsh. For example, there will be none of the voluntary
exemptions that the directive permits - only the mandatory ones will
be implemented.

#WnW: A must read for all those who believe that the current law is unclear
and that terms such as %(q:technical) are necessarily ambiguous.

#Wkt: The rules which the UKPO is pushing for (see references below) have
already been put to a practise test at the EPO (and UKPO).  Here you
get a quick look at the big pile of junk which resulted from illegally
applying the very rules which the UK patent family wants to make legal
and binding for all of Europe.

#tnn: allows searching in the UKPO's patent database.  You might use some of
the keywords found in EPO applications and have a try.  Closely look
at the main claims.  You will find that they are not noticeably
different from those of the EPO.

#etn: points out deceptive language in gene patenting.  See also
%(gp:Greenpeace Criticism).

#hWd: A very ambivalent article by a scottish lawyer which, although at an
abstract level arguing for software patentability, pokes deeper than
most into the problems but fails to offer any shade of a solution. 
Thus the final pro-swpat recommendation remain hypothetical, dependent
on solutions, which are not visible anywhere on the horizon:

#nie: This wish for %(q:clarification) based on (1) avoiding patents that
would be too broad or useless (2) keeping the patent office in
business, seems to be exactly what is driving the UKPO.  But as wish
(1) cannot be fulfilled, it is clinging to (2), hoping to keep
muddling through with %(mb:broken models) and doublespeak, no matter
what that may cost the industry.

#ota: The site contains current info on the software patent situation in the
UK

#gWH: Pages by Alex Hudson

#ias: a wealth of information about correspondance with UK politicians on
software patents.

#pfW: Support %(fu:FFII UK)

#WeW: Write to british MPs, ask them to bring the UKPO under control

#tpr: Contact British MEPs and consult europarl-uk at ffii org to share
experiences.

#Wad: List some samples of software patents recently granted by the UKPO!

#lUh: As long as we don't list them, some people will still believe that
UKPO software patents could somehow be better than %(mu:those of the
EPO)!

#myr: Show the PTO and the government some simplified sample patents, with
claims and prior art, and ask them to clarify whether the invention
found in there (= contribution) is a technical one according to the
new standard which they advocate!

#sWu: Analyse the UKPO swpat consultation

#Wsu: compare our %(ec:analysis of the CEC consultation))

#owg: Show with easy-to-follow quotations how the UKPO asked wrong/ambiguous
questions and drew wrong/ambiguous conclusions

#eas: Put together a list of participants for us to talk to

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatuk ;
# txtlang: en ;
# multlin: t ;
# End: ;

