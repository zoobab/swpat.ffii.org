<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bundespatentgericht

#descr: In den 60er Jahren aus dem Deutschen Patentamt herausgeklagt, seitdem
institutionell einigermaßen unabhängig (zumindest formelle Reste der
Abhängigkeit sind noch vorhanden).  Zwei Senate beschäftigen sich mit
Softwarepatenten.  Bis um 1973 tendierte das BPatG zu einer laxen
Haltung, danach kam eine Wende.  Der 17. Senat bemüht sich bis heute,
das Gesetz anzuwenden und Softwarepatente zu vermeiden, verweigert
dabei regelmäßig EPA und BGH die Gefolgschaft.  Der 21. Senat, der
mehr für Elektronik zuständig ist, neigt unter seinem Vorsitzenden
Wilfried Anders zu einer grenzenlosen Ausweitung der Patentierbarkeit
und greift dabei sogar EPA und BGH gelegentlich vor.  Patentanwälte
und Richter haben Wege gefunden, möglichst auch Fälle, die nichts mit
Elektronik zu tun haben und eigentlich in die Zuständigkeit des 17.
Senates fallen würden, dem 21. Senat zu übergeben.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbpatg ;
# txtlang: de ;
# multlin: t ;
# End: ;

