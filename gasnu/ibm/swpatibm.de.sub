\begin{subdocument}{swpatibm}{IBM und Logikpatente}{http://swpat.ffii.org/akteure/ibm/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Die Patentabteilung von IBM f\"{o}rdert besonders aktiv die Patentierbarkeit von Software in Europa.  Sie hat zahlreiche Trivialpatente bis zu den h\"{o}chsten Gerichten durchgefochten um Grundsatzentscheidungen f\"{u}r Swpat zu erwirken.  Mit tausenden solcher Patente gelingt es IBM, eine Steuer von Softwareunternehmen zu erheben, die gewissen vagen Zeitungsberichten zufolge 1.6 Milliarden pro Jahr einbringen soll.   Zugleich wiegt aber die Last der Patentb\"{u}rokratie schwer auf dem Unternehmen, und IBM ist h\"{a}ufige Zielscheibe von professionellen Prozessierfirmen, die selber nichts programmieren aber ein Patentportfolio erwerben, um gro{\ss}e Unternehmen zu erpressen.  Es kann nicht als gesicherte Erkenntnis gelten, dass IBM unter dem Strich von Swpat profitiert.  Die IBM-Patentabteilung hat Druck auf Regierungen ausge\"{u}bt, um Studien \"{u}ber die Wirkungen von Softwarepatenten zu verhindern und Widerstand gegen Gesetzes\"{a}nderungspl\"{a}ne auszubremsen.}
\begin{sect}{bild}{Creating Pro-Swpat Caselaw and Lobbying in Europe}
IBM's patent lawyers have fought most of the landmark cases through the European Patent Office (EPO) and the German courts in order to make software patentable in Europe.  Most recently, IBM's chief lawyer, Fritz Teufel (http://swpat.ffii.org/akteure/teufel/index.en.html), has pressed a spellcheck patent (http://swpat.ffii.org/papiere/bpatg17-suche00/index.de.html) all the way to the highest court and obtained a (somewhat sleazy and half-hearted) legalisation of computer program claims.  In 1998 IBM obtained the same from the EPO (http://swpat.ffii.org/papiere/epo-t971173/index.de.html).

Moreover IBM representatives were active in various organisations and lobbied governments to permit software patents.  In Sep 2000 The Gesellschaft f\"{u}r Informatik (GI) (http://www.gi-ev.de/) published pro swpat press releases that were immediately echoed by the association's vice president, Andrea Grimm, who is an IBM manager.  When questioned by some critical members of GI, Ms Grimm stated that IBM is against trivial software patents and that software patents can harmoniously coexist with opensource software.

IBM patent department representatives gave strong statements in favor of software patentability at several meetings at EU and national levels.  In private meetings with government officials in 1997/98 they have pressure governments to push the European patent legislation toward official recognition of software patetents and announce that IBM will make its investments in a specific country dependent on that country's government's favorable behavior.
\end{sect}\begin{sect}{spi}{Preventing Investigations in the US}
IBM has for many years sponsored the Software Patents Institute, which failed to solve the problem of software prior art while consuming a lot of state and industry money.  In 1998, IBM exerted pressure on the Clinton administration to prevent one of the president's advisers from ordering a thourough survey of patent quality and of the effects of the recent expansion of patentability.

American patent law organisations like the AIPLA never invite the famous patent quality critic Gregory Aharonian to give lectures at their meetings.  This, according to some rumors, is due to pressures from IBM.
\end{sect}\begin{sect}{gajn}{License income from IBM patent portfolio}
Around 1990, IBM was not aggressively asserting its software patents but rather using them to gain access to key innovations of competitors, as IBM manager Roger Smith explained (http://lpf.ai.mit.edu/Links/prep.ai.mit.edu/ibm.think.article) at the time.

Business Week had an article about how IBM uses its patents to press money out of the American software industry.  This amount is enough to build many IBM business centers throughout Europe.  The question is only, whether Europe should accept the IBM tax or not rather do something on its own to foster the development of software.

\begin{quote}
{\it BIG BLUE IS OUT TO COLLAR SOFTWARE SCOFFLAWS}

{\it Business Week: March 17, 1997}

{\it ...}

{\it Big Blue holds more software patents than any other company in the world. That's great for bragging rights, but it does little for the bottom line. Now, however, IBM sees money in that trove of intellectual property--and its efforts to collect are making other software companies hopping mad. Lawyers for Big Blue are searching for software companies that it says should be paying royalties but aren't. Over the past several months, IBM has been quietly pursuing patent claims against such well-known software companies as Oracle, Computer Associates, Adobe Systems, Autodesk, Intuit, and Informix. IBM is also pressing a software claim against computer maker Sequent Computer Systems Inc.}

{\it ...}

{\it Now, Reback is hurling charges against IBM similar to those he leveled at Microsoft. ``IBM shows up the same way someone might demand protection money,'' he says. Officials at the companies confirm that IBM has contacted them, but most refuse to talk publicly. Collecting the patent royalties could add millions to IBM's net profits. In 1995--the last year IBM released figures--the company took in \$650 million from royalties on all patents, software and hardware alike. Insiders say that senior managers believe IBM could collect \$1 billion a year from its patents.}

{\it When IBM strikes a royalty agreement, it collects 1\percent{} to 5\percent{} of the retail price of a product using the covered technology. That's a sliding scale depending on the number of patents involved. If IBM can collect royalties from those companies using its approximately 2,500 U.S. software patents, it could reap almost as much from software as the \$200 million in royalties it gets from PC makers. Reback says he was told IBM asked one software maker to pay \$30 million to \$40 million a year.}

{\it Indeed, some folks in the computer and software businesses fear that the whole industry could wind up paying a 1\percent{} to 5\percent{} tax to IBM. ``It's hard to be in the computing business--hardware or software--and not infringe on a couple of dozen IBM patents, if not more,'' says Greg Aharonian, a patent consultant. Meanwhile, other technology companies are following IBM's lead. Says Richard A. McGinn, president of Lucent Technologies Inc.: ``We've seen IBM become much more aggressive, and we are, too.''}

{\it IBM has patented everything from the software to automatically return the cursor to the start of the next line on a computer screen to a state-of-the-art virus-detection program. So far, it has made claims against companies by invoking patents including a spell-check function and techniques for how a database program handles queries and runs on so-called parallel-processing computers.}

{\it Software companies aren't eager to settle. Intuit Inc., for one, rejected a patent license deal that IBM offered. Some companies are afraid that paying now will set a precedent, making it harder to say no later. ``If we sign up with IBM today, then what happens in three or five years, when the patent agreement expires?'' asks Oracle Corp. patent attorney Allen Wagner. With all the skirmishing that lies ahead, this dispute is still in Version 1.0.}
\end{quote}

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf IBM 2003-01: 3288 US patents in 2002, 10 bn IP royalties in 10 years\footnote{http://www.ibm.com/news/us/2003/01/131.html}}}

\begin{quote}
An IBM press release about the patent figures of 2002, which celebrates the continued champion position of IBM in patent numbers among the IT companies, with Canon and HP lagging far behind.  The article does not disclose how much of the ``10 bn IP royalties in 10 years'' is due to patents and how much to copyright and other legal titles.
\end{quote}
\filbreak

\item
{\bf {\bf WIRED: IBM Patent Revenues in 2000\footnote{http://www.wired.com/news/technology/0,1282,43186,00.html}}}

\begin{quote}
Reports license revenue figures but fails to distinguish between patents and copyright: \begin{quote}
{\it IBM touted 2,886 patents last year, of which a third -- 962 concepts -- shipped in the form of products. IBM raked in \$1.6 billion in intellectual property license fees last year, according to company spokesman Tim Blair.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Inc.com 01/09: Patent Licensing Strategies\footnote{http://www.inc.com/articles/legal/ip/patents/23293.html}}}

\begin{quote}
Advises patent departments on when to embark on an aggressive patent enforcement and royalty-collecting strategy and how to get corporate support behind it.  In order to profit from patents it is necessary to transform the company and orient it around a patent strategy unit.  Names IBM royalty income figures as a success example.  Again the figures relate to ``IP'' and it is unclear how much of this is based on patents.
\end{quote}
\filbreak

\item
{\bf {\bf Reback \"{u}ber Patent-Beutez\"{u}ge von IBM\footnote{http://www.forbes.com/asap/2002/0624/044.html}}}

\begin{quote}
Der amerikanische Anwalt Gary Reback ist wegenseiner Rolle bei den Anti-MS-Kartellprozessen bekannt.  In den 80er Jahren arbeitete er f\"{u}r Sun Microsystems.  Dort lernte er IBM und das Patentwesen kennen.  Eine Gruppe von 14 IBM-Anw\"{a}lten + Helfern kam vorbei und forderte 20 Millionen aufgrund eines Portfolios 10000 Patenten, ohne auch nur in einem einzigen Fall Verletzung durch Sun nachzuweisen.  Sie bekamen die 20 Mio und schritten dann zum n\"{a}chsten Opfer weiter.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}\begin{sect}{eur}{Recently a somewhat Cautious Policy in Europe?}
IBM is one of the few companies in the software area that speak independently and with a certain degree of coherence between the departments about their patent policy.

The speeches by IBM patent law experts such as David Kappos can be astonishing.  On 2001-10-20 at a hearing in London, Kappos incurred the wrath of european patent lawyers by criticising the effects of business method patents and trivial patents on the economy and asking the people from the European Commission not to follow the USA but rather to apply stricter standards of ``technical contribution'' and non-obviousness.  When vocal patent lawyers such as J\"{u}rgen Betten (http://swpat.ffii.org/akteure/betten/index.en.html) protested, saying that IBM was profiting most of all from this system and now apparently ``once again, as in the 70s'' turning its back on their allies, Kappos responded along the lines of:  \begin{quote}
For us as a company adjusting to whatever system there is is a question of survival.  If the mayor hands out guns to everybody in town, you can bet IBM is going to get some of the best guns.  That doesn't mean that we are necessarily in favor of a liberal gun policy.
\end{quote}

This did not prevent IBM from factually doing everything to push software patentability in Europe.  They merely took a more cautious rhetoric, and it seems that they are, unlike GE and some other american companies, not doing this merely because the patent department follows a patent movement agenda, but also because they do not want their server market to be blocked by business method patents, of which IBM does not have so many yet.  This is indeed similar to their motivation to oppose software patents in the 1970s.

Perhaps IBM is not quite united on this issue.  In its upcoming UK Patent Orifice (http://swpat.ffii.org/termine/2002/ukpo06/index.de.html) rally in Brussels, the British patent movement invited Fritz Teufel (http://swpat.ffii.org/akteure/teufel/index.en.html) instead of David Kappos.  As a pure lawyer, Teufel is unlikely to change the hardline views which were the keys of his successes in european courts until now.
\end{sect}\begin{sect}{patmov}{IBM Patent Movement vs IBM}
Although IBM may be earning substantial revenues from its large collection of trivial software patents, it is not sure that unlimited patentability is in the best interest even of IBM.  Patent managment generates costs that were not considered in the above calculations.  Specialised litigation companies acquire patents in order to go after giants.  IBM could perhaps be even more profitable if it didn't dedicate so much of its ressources to this type of warfare.  Building the business strategy on patents is increasingly appearing incoherent with IBM's strong support for opensource software and the significant business it has generated therefrom.
\end{sect}\begin{sect}{sslug0202}{IBM afraid of working with GNU/Linux because of the risk of infringing a patent}
In an interview (http://www.sslug.dk/patent/strassemeyer/) from 2002/02, a leading developper working for IBM explained some aspects of IBM's patent policy and its conflict with the policy of supporting GNU/Linux.

Dr. Karl-Heinz Strassemeyer basically said:

\begin{quote}
\begin{itemize}
\item
IBM does not ship any free software that infringes on patents.\footnote{Indeed in 2001 IBM requested SuSE to use ugly TrueType rendering schemes (http://swpat.ffii.org/patente/wirkungen/ttf/index.de.html) on a SuSE distribution used for IBM servers, precisely for this reason.}

\item
IBM only submits patches to the Linux kernel after a formal procedure of patent clearance

\item
IBM does not do distributions because the risk of infringing a patent that way is too high.

\item
IBM does not deploy Linux in embedded systems of devices which it sells because someone might find that the kernel infringes on a patent and then sue IBM.
\end{itemize}
\end{quote}

As explained above, IBM has become a favorite target of specialised patent litigation companies.  IBM attracts such companies more than any small Linux distributor at present.
\end{sect}\begin{sect}{epat}{E-Patente von IBM am Europ\"{a}ischen Patentamt (EPA)}
\input{ibm-epatents}
\end{sect}\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
\ifmlhttasks
\begin{itemize}
\item
{\bf {\bf Wie Sie uns helfen k\"{o}nnen, dem Swpat-Albtraum ein Ende zu machen (http://swpat.ffii.org/gruppe/aufgaben/index.de.html)}}
\filbreak

\item
{\bf {\bf How are the IBM ``IPR licenses'' revenues distributed between copyright, software patents, hardware patents and contracts based on some other kind or combination of proerty titles ?}}
\filbreak

\item
{\bf {\bf To what degree is IBM's patent lobbying a part of the general actvities of the patent movement to which the people at the IBM patent department belongs (and therefore simply determined by the ideology of that movement), and to what extent is it a well-deliberated corporate policy of IBM?}}
\filbreak

\item
{\bf {\bf Can some common grounds be found that would enable IBM to develop a position of its own that is more rational and more friendly to free software?}}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: mlatex ;
% End: ;

