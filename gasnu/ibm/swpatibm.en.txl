<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: IBM and Software Patents

#descr: IBM's patent department is actively lobbying Europe to legalise
software patents.  They have invested millions in fighting example
cases to leading European lawcourts such as the EPO's Technical Boards
of Appeal and the German Federal Court in order to soften and
eventually remove European restrictions on patenting software.  They
have also threatened European politicians that IBM might close down
local facilities if software patents are not legalised in Europe.  IBM
has also prevented the US government from conducting studies on the
value of software patents for the national economy.  In the wake of
the Opensource hype, IBM's rhetoric has become relatively moderate,
but nonetheless it is supported by real pressure.  IBM has acquired
approximately 1000 European software patents whose legal status is
currently unclear.  Given the great number of software patents in
IBM's hands, IBM is one of the few software companies who may have a
genuine interest in software patentability.  Once software patents
become assertable in Europe, an IBM tax of several billion EUR per
year may be levied on European software companies.

#Cab: Creating Pro-Swpat Caselaw and Lobbying in Europe

#PIo: Preventing Investigations in the US

#Let: License income from IBM patent portfolio

#PyE: Recently a somewhat Cautious Policy in Europe?

#Ine: IBM Patent Movement vs IBM

#IWr: IBM afraid of working with GNU/Linux because of the risk of infringing
a patent

#arf: IBM E-Patents at the European Patent Office (EPO)

#Iom: IBM's patent lawyers have fought most of the landmark cases through
the European Patent Office (EPO) and the German courts in order to
make software patentable in Europe.  Most recently, IBM's chief
lawyer, %(FT), has pressed a %(bp:spellcheck patent) all the way to
the highest court and obtained a (somewhat sleazy and half-hearted)
%(bg:legalisation of computer program claims).  In 1998 IBM obtained
%(et:the same from the EPO).

#Gan: Gesellschaft für Informatik

#Msm: Moreover IBM representatives were active in various organisations and
lobbied governments to permit software patents.  In Sep 2000 The %{GI}
published pro swpat press releases that were immediately echoed by the
association's vice president, Andrea Grimm, who is an IBM manager. 
When questioned by some critical members of GI, Ms Grimm stated that
IBM is against trivial software patents and that software patents can
harmoniously coexist with opensource software.

#Iei: IBM patent department representatives gave strong statements in favor
of software patentability at several meetings at EU and national
levels.  In private meetings with government officials in 1997/98 they
have pressure governments to push the European patent legislation
toward official recognition of software patetents and announce that
IBM will make its investments in a specific country dependent on that
country's government's favorable behavior.

#Iop: IBM has for many years sponsored the Software Patents Institute, which
failed to solve the problem of software prior art while consuming a
lot of state and industry money.  In 1998, IBM exerted pressure on the
Clinton administration to prevent one of the president's advisers from
ordering a thourough survey of patent quality and of the effects of
the recent expansion of patentability.

#Anm: Around 1990, IBM was not aggressively asserting its software patents
but rather using them to gain access to key innovations of
competitors, as IBM manager Roger Smith %(ti:explained) at the time.

#Brr: Business Week had an article about how IBM uses its patents to press
money out of the American software industry.  This amount is enough to
build many IBM business centers throughout Europe.  The question is
only, whether Europe should accept the IBM tax or not rather do
something on its own to foster the development of software.

#Nsk: Now, Reback is hurling charges against IBM similar to those he leveled
at Microsoft. %(q:IBM shows up the same way someone might demand
protection money,) he says. Officials at the companies confirm that
IBM has contacted them, but most refuse to talk publicly. Collecting
the patent royalties could add millions to IBM's net profits. In
1995--the last year IBM released figures--the company took in $650
million from royalties on all patents, software and hardware alike.
Insiders say that senior managers believe IBM could collect $1 billion
a year from its patents.

#Wno: When IBM strikes a royalty agreement, it collects 1% to 5% of the
retail price of a product using the covered technology. That's a
sliding scale depending on the number of patents involved. If IBM can
collect royalties from those companies using its approximately 2,500
U.S. software patents, it could reap almost as much from software as
the $200 million in royalties it gets from PC makers. Reback says he
was told IBM asked one software maker to pay $30 million to $40
million a year.

#Ino: Indeed, some folks in the computer and software businesses fear that
the whole industry could wind up paying a 1% to 5% tax to IBM.
%(q:It's hard to be in the computing business--hardware or
software--and not infringe on a couple of dozen IBM patents, if not
more,) says Greg Aharonian, a patent consultant. Meanwhile, other
technology companies are following IBM's lead. Says Richard A. McGinn,
president of Lucent Technologies Inc.: %(q:We've seen IBM become much
more aggressive, and we are, too.)

#Iee: IBM has patented everything from the software to automatically return
the cursor to the start of the next line on a computer screen to a
state-of-the-art virus-detection program. So far, it has made claims
against companies by invoking patents including a spell-check function
and techniques for how a database program handles queries and runs on
so-called parallel-processing computers.

#Sge: Software companies aren't eager to settle. Intuit Inc., for one,
rejected a patent license deal that IBM offered. Some companies are
afraid that paying now will set a precedent, making it harder to say
no later. %(q:If we sign up with IBM today, then what happens in three
or five years, when the patent agreement expires?) asks Oracle Corp.
patent attorney Allen Wagner. With all the skirmishing that lies
ahead, this dispute is still in Version 1.0.

#Rst: Reback on IBM's predatory patent practises

#T8s: The american attorney Gary Reback is known for his role in the
procedings against anti-competitive practises by Microsoft.  In the
80s, working for Sun Microsystems, he got to know IBM's patent
portfolio.  14 IBM lawyers + assistants visited the young firm and
asked for 20 million USD without even showing any infringement by Sun.
 They got what they asked for and proceded to the next victim.

#d6e: Reports license revenue figures but fails to distinguish between
patents and copyright: %(bc:IBM touted 2,886 patents last year, of
which a third -- 962 concepts -- shipped in the form of products. IBM
raked in $1.6 billion in intellectual property license fees last year,
according to company spokesman Tim Blair.)

#mtr: Inc.com 01/09: Patent Licensing Strategies

#iig: Advises patent departments on when to embark on an aggressive patent
enforcement and royalty-collecting strategy and how to get corporate
support behind it.  In order to profit from patents it is necessary to
transform the company and orient it around a patent strategy unit. 
Names IBM royalty income figures as a success example.  Again the
figures relate to %(q:IP) and it is unclear how much of this is based
on patents.

#tWa: Patenting: An Idea Whose Time has Gone?

#ehg: A banker writes that as patent application numbers explode it is
becoming increasingly unattractive for small companies to file for
patents.  They will usually spend a lot of money and in any case have
to share their monopoly with large companies who own related patents. 
Small companies are usually more successful if they instead rely on
informal protection shields (product complexity, business secret) and
concentrate their energies on quickly moving in the market.  Instead
of patenting themselves, they should look for a corporate big brother
from the start.

#0Wi: IBM 2003-01: 3288 US patents in 2002, 10 bn IP royalties in 10 years

#Woi: An IBM press release about the patent figures of 2002, which
celebrates the continued champion position of IBM in patent numbers
among the IT companies, with Canon and HP lagging far behind.  The
article does not disclose how much of the %(q:10 bn IP royalties in 10
years) is due to patents and how much to copyright and other legal
titles.

#e5B: According to statistics in this article, IBM spent $4.75 billion on
research and development, and this article suggests, but does not say
clearly, that this is turned into profit mainly by means of patent
licensing rather than by application development. %(orig:But over the
last several years IBM has brought research out of the shadows and in
front of customers. Much of the focus has been on software, which
accounts for 15% of IBM's revenue and one-third of its profits.
Software now contributes almost half of its patents, compared with
less than 10% five years ago. With more than 22,000 patents in total,
IBM has been granted more patents than any company in the world for
the past decade.  According to Paul Horn, IBM's senior vice president
of research, IBM has generated $1 billion in profit--that's profit,
not sales--by licensing intellectual property developed by its
researchers.)

#Iae: IBM is one of the few companies in the software area that speak
independently and with a certain degree of coherence between the
departments about their patent policy.

#Ten: The speeches by IBM patent law experts such as David Kappos can be
astonishing.  On 2001-10-20 at a hearing in London, Kappos incurred
the wrath of european patent lawyers by criticising the effects of
business method patents and trivial patents on the economy and asking
the people from the European Commission not to follow the USA but
rather to apply stricter standards of %(q:technical contribution) and
non-obviousness.  When vocal patent lawyers such as %(jb:Jürgen
Betten) protested, saying that IBM was profiting most of all from this
system and now apparently %(q:once again, as in the 70s) turning its
back on their allies, Kappos responded along the lines of:  %(bq:For
us as a company adjusting to whatever system there is is a question of
survival.  If the mayor hands out guns to everybody in town, you can
bet IBM is going to get some of the best guns.  That doesn't mean that
we are necessarily in favor of a liberal gun policy.)

#Tir: This did not prevent IBM from factually doing everything to push
software patentability in Europe.  They merely took a more cautious
rhetoric, and it seems that they are, unlike GE and some other
american companies, not doing this merely because the patent
department follows a %(pa:patent movement agenda), but also because
they do not want their server market to be blocked by business method
patents, of which IBM does not have so many yet.  This is indeed
similar to their motivation to oppose software patents in the 1970s.

#Pan: Perhaps IBM is not quite united on this issue.  In its upcoming
%(up:UK Patent Orifice) rally in Brussels, the British patent movement
invited %(FT) instead of David Kappos.  As a pure lawyer, Teufel is
unlikely to change the hardline views which were the keys of his
successes in european courts until now.

#AWe: Although IBM may be earning substantial revenues from its large
collection of trivial software patents, it is not sure that unlimited
patentability is in the best interest even of IBM.  Patent managment
generates costs that were not considered in the above calculations. 
Specialised litigation companies acquire patents in order to go after
giants.  IBM could perhaps be even more profitable if it didn't
dedicate so much of its ressources to this type of warfare.  Building
the business strategy on patents is increasingly appearing incoherent
with IBM's strong support for opensource software and the significant
business it has generated therefrom.

#IWw: In an %(iv:interview) from 2002/02, a leading developper working for
IBM explained some aspects of IBM's patent policy and its conflict
with the policy of supporting GNU/Linux.

#Dim: Dr. Karl-Heinz Strassemeyer basically said:

#IyW: IBM does not ship any free software that infringes on patents.

#IWn: Indeed in 2001 IBM requested SuSE to use %(tt:ugly TrueType rendering
schemes) on a SuSE distribution used for IBM servers, precisely for
this reason.

#Isf: IBM only submits patches to the Linux kernel after a formal procedure
of patent clearance

#IbW: IBM does not do distributions because the risk of infringing a patent
that way is too high.

#Idl: IBM does not deploy Linux in embedded systems of devices which it
sells because someone might find that the kernel infringes on a patent
and then sue IBM.

#Fes: As explained above, IBM has become a favorite target of specialised
patent litigation companies.  IBM attracts such companies more than
any small Linux distributor at present.

#Het: How are the IBM %(q:IPR licenses) revenues distributed between
copyright, software patents, hardware patents and contracts based on
some other kind or combination of proerty titles ?

#Toe: To what degree is IBM's patent lobbying a part of the general
actvities of the patent movement to which the people at the IBM patent
department belongs (and therefore simply determined by the ideology of
that movement), and to what extent is it a well-deliberated corporate
policy of IBM?

#Clm: Can some common grounds be found that would enable IBM to develop a
position of its own that is more rational and more friendly to free
software?

#sem: ein Verband wichtiger mittelständischer Softwareunternehmen der
Region, bei dem %(it:PA Teufel) von IBM im September 2003 einen
Vortrag hielt.   Ein PDF zum Vortrag vom Software-Tag 2003 ist beim
SIBB wahrscheinlich auf Anforderung verfügbar.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatibm ;
# txtlang: en ;
# multlin: t ;
# End: ;

