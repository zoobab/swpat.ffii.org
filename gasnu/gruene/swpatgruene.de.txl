<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Die Grünen und Softwarepatente

#descr: Die deutschen Grünen haben sich relativ früh gegen Softwarepatente
positioniert.  Die Partei neigt stärker als andere dazu, sich als
Vertreterin der "Zivilgesellschaft" gegen Konzernlobbies zu
positionieren.  Angesichts vielfältiger Verbindungen zu den
Genpatent-Kritikern von Greenpeace und der Welt der Freien
Software/Wissens usw. fand auch der Widerstand gegen die Ausweitung
des Patentwesens auf Organisationsabläufe und Rechenregeln schneller
Anklang als bei anderen Parteien.  Allerdings ist dieser Widerstand
nicht von weitergehendem fachlichem Verständnis als bei anderen
Parteien getragen, was "Patentrechtsexperten" in der Berliner
Führungetage die Gelegenheit bietet, ihn auszubremsen.

#Dna: Die Ablehnung von Softwarepatenten war bei den Grünen von anfang an
klar und eindeutig.  Die parteinahe Böll-Stiftung hat hier auch
einiges unternommen.  Sie hat auch ein %(q:Netzwerk Neue Medien) ins
Leben gerufen.

#Dim: Die Internet-Referentin Grietje Bettin unterzeichnete als erste
Bundestagsabgeordnete den %(dm:Aufruf zum Handeln).  Weitere Beiträge
kamen von Margareta Wolf (Wirtschaftsreferentin, BMWi) und Götz von
Stumpfeld.

#BeD: Beim Thema %(bm:Hochschulpatentwesen) ließen sich die Grünen offenbar
von irreführender Sozialneid-Rhetorik gegen das
%(q:Hochschullehrer-Privileg) einlullen.  Der Abgeordnete Berninger,
der sich später als Staatssekretär für die %(pk:Privatkopie) stark
machte, %(mb:plädierte) unter diesem Vorzeichen sogar für Einführung
des obligatorischen Patentierens an den Hochschulen.

#OWa: Open Source: Freie Software für alle

#Bun: Beschluss der Grünen für freie Software und gegen die Ausuferung des
Patentsystems.  Der Beschluss wurde offenbar im Vorfeld der geplanten
(und schließlich u.a. dank Einsatzes der Grünen verhinderten)
Streichung der %(q:Programme für Datenverarbeitunganlagen) von der
Liste der Nicht-Erfindungen aus %(ep:Art 52 EPÜ) mit heißer Nadel
gestrickt.

#EWe: Eine den Grünen nahestehende Organisation, die sich für eine Reihe von
Belangen einsetzt, die denen des FFII ähneln.  U.a. ruft die
Titelseite zur Unterzeichnung der Petition gegen Softwarepatente auf.

#Adt: Auftritt der für Netz- und Medienpolitik zuständigen
Bundestagsabgeordneten der Grünen, enthält auch die Stellungnahme
gegen Softwarepatente.

#Lrg: Grundsatzprogramm der Grünen (Antrag)

#Dst: Das Demokratieverständnis der Grünen scheint sich stark von dem der
Europäischen Kommission zu unterscheiden.  Während die EUK für eine
blinde Gefolgsamkeit gegenüber Verbandsmeinungen, die sie mitunter als
%(q:wirtschaftlichen Mehrheiten) bezeichnet, plädiert, steht im
Grundsatzprogramm der Grünen zu lesen: %(bc:Demokratie lebt vom
Wettstreit der politischen Positionen und Konzepte. Deshalb halten wir
es für falsch, wenn Positionen nicht mehr offen eingeführt und
erstritten, sondern allein mit den großen Interessenverbänden
ausgehandelt werden. Wir wollen die Rolle der Parlamente und der
Abgeordneten im politischen Willensbildungs- und   
Entscheidungsprozess aufwerten.)

#eut: enthält ältere Aussagen von Politikern der Grünen

#Ewg: Ein grüner Abgeordneter sichert einem %(dm:durchgeknallten
Verbandsjuristen auf Lobby-Tournee) Unterstützung zu.  Die verkürzt
wiedergegebenen Aussagen des MdB Kuhn lösten im Heise-Leserkreis
Unruhe aus.

#Eed: Ein Buch über die schleichende Plünderung der Allmende der natürlichen
und informationellen Gemeingüter, welches den Zusammenhang zwischen
Umweltbewegung und Schutz der informationellen Infrastruktur leicht
verständlich macht.

#etT: Taten einfordern

#DWd: Die unsichtbare Enteignung der Allmende, die Plünderung öffentlicher
Ressourcen durch private Interessen ist den Grünen recht bekannt.  Die
Grünen können sich aus gegebenem Anlass auch mal als Partei der
Leistungsträger profilieren und sollten die Tatsache nutzen, dass sie
in der Swpat-Frage einig und handlungsfähig sind.  Der Wille scheint
vorhanden zu sein, nur geht das Thema oft im politischen Tagesgeschäft
unter.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatgruene ;
# txtlang: de ;
# multlin: t ;
# End: ;

