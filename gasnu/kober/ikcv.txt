Lebenslauf von Ingo Kober
Pr�sident des Europ�ischen Patentamts

Curriculum vitae of Ingo Kober
President of The European Patent Office

Curriculum vitae de Monsieur Ingo Kober
Pr�sident de l'Office europ�en des brevets

[Ingo Kober]     
* Geboren: 22. Juli 1942 in Liegnitz (heute Legnica/Polen)
* Jurastudium: 1964 bis 1969 an der Universit�t Heidelberg
* Bundesministerium der Justiz: 1975 bis 1995
* Pr�sident des Europ�ischen Patentamts: seit 1. Januar 1996
* Sprachen: Deutsch, Englisch, Franz�sisch, Spanisch,
 Schwedisch
Beruflicher Werdegang:

Nach Abschlu� des Studiums der Rechtswissenschaften begann Ingo Kober seine
berufliche Laufbahn 1972 als Richter und Staatsanwalt in Mannheim und
Tauberbischofsheim.1975 trat er in das Bundesministerium der Justiz (BMJ) ein,
wo er bis 1982 als Referent t�tig war. Nach kurzer T�tigkeit als Referent f�r
Rechtspolitik �bernahm Ingo Kober im November 1982 im BMJ die Leitung des
Referats f�r Kabinett- und Parlamentsangelegenheiten. In der Folge leitete er
die Unterabteilung Personal und Organisation (1985) und danach die Abteilung
Justizverwaltung (1986).
Im Januar 1991 wurde Ingo Kober zum Staatssekret�r im Bundesministerium der
Justiz ernannt. Diese Position bekleidete er bis zur Aufnahme seiner
Amtsgesch�fte als Pr�sident des Europ�ischen Patentamts im Januar 1996. Die Wahl
in dieses Amt durch die Mitgliedsstaaten der Europ�ischen Patentorganisation
erfolgte Dezember 1994. Als Leiter der deutschen Delegation hatte Ingo Kober
bereits zwischen 1987 und 1991 die Bundesrepublik Deutschland im Verwaltungsrat
dieser Organisation vertreten.

------------------------------------------------------------------------

[Ingo Kober]     * Date of birth: 22 July 1942 in Liegnitz (now Legnica in
 Poland)
* Law studies: 1964 to 1969 (Heidelberg University)
* Federal German Ministry of Justice: 1975 to 1995
* President of the European Patent Office: since 1 January 1996
* Languages: German, English, French, Spanish, Swedish
Professional career

After completing his legal studies, Ingo Kober began his professional career in
1972 as judge and public prosecutor in Mannheim and Tauberbischofsheim. In 1975
he moved to the Federal German Ministry of Justice (MoJ), where he served until
1982. He then worked for a short time as chief legal policy adviser before
returning to the MoJ in November 1982 as head of its "Cabinet and Parliamentary
Affairs" department. Subsequently he took charge of personnel and organisation
(1985), then of overall administration at the MoJ (1986).
In January 1991 Ingo Kober was appointed state secretary at the MoJ, a position
he held until taking over as President of the European Patent Office in January
1996. He was elected to the EPO presidency in December 1994 by the member states
which make up the European Patent Organisation's Administrative Council - on
which he served as head of the German delegation from 1987 to 1991.

------------------------------------------------------------------------

[Ingo Kober]     * N� le 22 juillet 1942 � Liegnitz (aujourd'hui
 Legnica/Pologne)
* Etudes de droit de 1964 � 1969 � l'universit� de Heidelberg
* Minist�re de la justice de la R�publique f�d�rale d'Allemagne
 de 1975 � 1995
* Pr�sident de l'Office europ�en des brevets depuis le 1er
 janvier 1996
* Langues: allemand, anglais, fran�ais, espagnol, su�dois
Carri�re

Apr�s avoir termin� ses �tudes de droit, Monsieur Ingo Kober a commenc� sa
carri�re professionnelle en 1972 comme juge et procureur � Mannheim et �
Tauberbischofsheim. Il entre en 1975 au minist�re f�d�ral de la justice, o� il
est chef de bureau jusqu'en 1982. Apr�s avoir �t� bri�vement rapporteur charg�
des questions de politique juridique, Ingo Kober a pris en novembre 1982 la
direction du service charg� des questions relevant du conseil des ministres et
du parlement, avant de diriger la sous-direction "Personnel et organisation"
(1985), puis la direction "Administration" au minist�re de la justice (1986).
Ingo Kober a �t� nomm� secr�taire d'Etat du minist�re de la justice en janvier
1991, position qu'il a occup�e jusqu'� ce qu'il prenne ses fonctions de
Pr�sident de l'Office europ�en des brevets en janvier 1996. Il a �t� nomm� � ce
poste par les Etats membres de l'Organisation europ�enne des brevets en d�cembre
1994. Ingo Kober avait repr�sent� la R�publique f�d�rale d'Allemagne au Conseil
d'administration de l'Organisation en qualit� de chef de la d�l�gation allemande
de 1987 � 1991.




