<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: president of the European Patent Office (EPO) since the mid-nineties, former FDP (liberal party) secretary of state in the BMJ (ministery of justice), active promoter of patentability expansion and of international unification of the patent system.
title: Ingo Kober and Software Patents
bWK: born 1942 in Liegniz (Legnica), Silesia. %(bm:Federal German Ministry of Justice) (BMJ): 1975 to 1995, President of the %(ep:European Patent Office) (EPO) since 1996-01-01, After completing his legal studies, Ingo Kober began his professional career in 1972 as judge and public prosecutor in Mannheim and Tauberbischofsheim. In 1975 he moved to the BMJ, where he served until 1982. He then worked for a short time as chief legal policy adviser before returning to the BMJ in November 1982 as head of its %(q:Cabinet and Parliamentary Affairs) department. Subsequently he took charge of personnel and organisation (1985), then of overall administration at the BMJ (1986).  In January 1991 Kober was appointed state secretary at the BMJ, a position he held until taking over as President of the EPO in January 1996. He was elected to the EPO presidency in December 1994 by the member states which make up the European Patent Organisation's Administrative Council - on which he served as head of the German delegation from 1987 to 1991.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatkober ;
# txtlang: en ;
# End: ;

