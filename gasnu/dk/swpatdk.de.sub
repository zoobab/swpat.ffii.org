\begin{subdocument}{swpatdk}{Denmark, DKPTO and Software Patents}{http://swpat.ffii.org/akteure/dk/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{The Danish government has left patent matters almost completely in the hands of the Danish Patent and Trademark Office (DKPTO), which has been one of the most vigorous promoter of the patent faith and of unlimited patentability in Europe.  Their activity includes efforts to rush the CEC/BSA software patentability directive through the Council of the European Union (CEU) and publishing ``expert opinions'' which claim that no innovation would happen without patents and that patents are beneficial or at least not harmful even to free/opensource software, placing the Danish EU Presidency of 2002 under the slogan ``Growth, Prosperity and Patents'' and lobbying for EU subsidies in favor of patenting activities in small and medium enterprises.}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf CEU/DKPTO 2002/09/23: Amended Software Patentability Directive Proposal\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/dkpto0209/index.de.html}}}

\begin{quote}
Der Rat der Europ\"{a}ischen Union (REU) schl\"{a}gt vor, den Softwarepatent-Richtlinienvorschlages der Europ\"{a}ischen Kommission in einigen Punkten umzuschreiben, um diverse Kritikpunkte aufzunehmen, die im Kreis der Patentrechts-Arbeitsgruppe des Rates ge\"{a}u{\ss}ert wurden.  Diese Arbeitsgruppe besteht aus Fachreferenten nationaler Regierungen, die wiederum weitgehend aus den nationalen Patent\"{a}mtern stammen oder mit diesen in enger Tuchf\"{u}hlung stehen.  Der Gegenvorschlag des REU stammt aus der Feder der d\"{a}nischen Delegierten (d.h. des d\"{a}nischen Patent- und Markenamtes DKPMA), die im zweiten Halbjahr 2002 die Pr\"{a}sidentschaft des REU inne haben.  Der DKPMA-Vorschlag steht auf der Arbeitssitzung vom 3. Oktober 2002 zur Entscheidung an.  Wir pr\"{a}sentieren ihn in tabellarischer Gegen\"{u}berstellung mit dem Entwurfder Europ\"{a}ischen Kommission (BSA/EUK-Entwurf) vom 20. Februar 2002.  Es zeigt sich, dass das DKPMA -- bei aller Beschw\"{o}rung der Technizit\"{a}t -- zus\"{a}tzliche Unklarheiten schafft und die Grenzen des Patentierbaren nur noch weiter ausdehnt.
\end{quote}
\filbreak

\item
{\bf {\bf Danish Presidency 2002: Growth, Prosperity \& Patents\footnote{http://www.eu2002.dk/calendar/meetinginfo.asp?iCalendarID=3811}}}

\begin{quote}
Die D\"{a}nische Ratspr\"{a}sidentschaft hat dieses Motto f\"{u}r ihre Pr\"{a}sidentschaft im zweiten Halbjahr 2002 gew\"{a}hlt und sich dann auch in der Ratsarbeitsgruppe \"{u}ber Patente dementsprechend verhalten.  Das euphorischste Motto der EU-Patentbewegung seit Ingo Kobers ``Ein Europa, eine W\"{a}hrung, ein Patent'' von 1997.

siehe auch Kober 1997 zum Gr\"{u}nbuch: ``Ein Europa, Eine W\"{a}hrung, Ein Patent''\footnote{http://swpat.ffii.org/papiere/eporep97/index.de.html}
\end{quote}
\filbreak

\item
{\bf {\bf DKPTO Opensource Report Summary (http://www.dkpto.dk/en/publications/reports/open\_source/open\_source.htm)}}

\begin{quote}
The DKPTO officials, who are de facto framing the Danish government's patent policy, argue that patents don't harm free software
\end{quote}
\filbreak

\item
{\bf {\bf http://www.dkpto.dk/eu/conf/index.htm}}

\begin{quote}
states that ``At the same time, innovation and new ideas should be rewarded appropriately in the new knowledge-based economy, especially through patent protection. ... The winners of the future are those who are capable of getting new ideas. But the ideas are worth nothing if they cannot be protected against copying.''  When asked by a danish EuroLinux representative if they had knowledge of any macro-economic reports that supported these claims, the DKPTO responded that they did not and that these claims were meant to apply to all patents and therefore did not need to be specially justified in view of software.
\end{quote}
\filbreak

\item
{\bf {\bf DKPTO Economics (http://www.dkpto.dk/publikationer/rapporter/oekonomiske\_konsekvenser\_patent\_forsikring.pdf)}}

\begin{quote}
A report by the DKPTO about the macro-economic beneficiality of patents in general which however ends up arguing mainly for the creation of an insurance system for patent owners.
\end{quote}
\filbreak

\item
{\bf {\bf Patent Insurance (http://www.dkpto.dk/en/publications/reports/insurance/eco\_con\_patent\_insurance.pdf)}}

\begin{quote}
A report by the DKPTO about its plans for a patent insurance system which would make it easier for patent owners to sue people for patent infringement.
\end{quote}
\filbreak

\item
{\bf {\bf General Info (http://www.dkpto.dk/en/general/information\_pdf.htm)}}

\begin{quote}
DKPTO General Information
\end{quote}
\filbreak

\item
{\bf {\bf DKPTO: How to patent software, and how to avoid infringing others' patents (http://www.dkpto.dk/indhold/serviceydelser/kurser/softwareopfindelser.htm)}}

\begin{quote}
A oneday course on softwarepatents by the DKPTO.  Abstract: \begin{quote}
{\it Software inventions are mostly protected by copyright, amongst other because patents are not granted for software ``as such'' but requires the presence of an apparatus in some form. This should be used, if possible, since patents offers a more efficient protection compared to copyright. At the same time this change means that  there are many patents on software, which can block a company's development of new products.}
\end{quote}  Not a very clear translation, but it's not a very well written original.  At least it shows that the patent office is aware of the problems regarding infringement of software patents. Their statement that software inventions are protected by copyright is even more rarely heard from the mouth of patent lobbyists, but it is quite correct if indirect protection of innovation-related investments is taken into consideration.
\end{quote}
\filbreak

\item
{\bf {\bf softwarepatenter.dk (http://www.softwparepatenter.dk/)}}

\begin{quote}
A patent-critical website run by a coalition of danish software professionals
\end{quote}
\filbreak

\item
{\bf {\bf Skandinavien: auch ohne ``als solches'' Klausel kann Diebstahl einen weiteren rechtlichen Effekt haben\footnote{http://swpat.ffii.org/analyse/epue52/udgor/index.de.html}}}

\begin{quote}
The danish and swedish version of the EPC does not contain the ``as such'' clause, which has served the EPO as a pretext for violating the law.  Patent lawyers in Sweden and Denmark had to rely entirely on the argument that ``european caselaw'' needs to be followed, but apparently that did not cause much trouble for them.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: mlatex ;
% End: ;

