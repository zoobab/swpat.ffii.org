<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Denmark, DKPTO and Software Patents

#descr: The Danish government has left patent matters almost completely in the
hands of the Danish Patent and Trademark Office (DKPTO), which has
been one of the most vigorous promoters of the patent faith and of
software patentability in Europe.  Their activity includes efforts to
rush the CEC/BSA software patentability directive through the Council
of the European Union (CEU) and to worsen it by introducing program
claims, and publishing %(q:expert opinions) which claim that no
innovation would happen without patents and that patents are
beneficial even for the development of free/opensource software,
placing the Danish EU Presidency of 2002 under the slogan %(q:Growth,
Prosperity and Patents) and lobbying for EU subsidies to a proposed
insurance for patent owners.

#Dso: DKPTO Opensource Report Summary

#TiW: The DKPTO officials, who are de facto framing the Danish government's
patent policy, argue that patents don't harm free software

#swi: states that %(q:At the same time, innovation and new ideas should be
rewarded appropriately in the new knowledge-based economy, especially
through patent protection. ... The winners of the future are those who
are capable of getting new ideas. But the ideas are worth nothing if
they cannot be protected against copying.)  When asked by a danish
EuroLinux representative if they had knowledge of any macro-economic
reports that supported these claims, the DKPTO responded that they did
not and that these claims were meant to apply to all patents and
therefore did not need to be specially justified in view of software.

#DWo: DKPTO Economics

#Ayy: A report by the DKPTO about the macro-economic beneficiality of
patents in general which however ends up arguing mainly for the
creation of an insurance system for patent owners.

#Ptu: Patent Insurance

#AWa: A report by the DKPTO about its plans for a patent insurance system
which would make it easier for patent owners to sue people for patent
infringement.

#GrI: General Info

#Dnf: DKPTO General Information

#Dti: DKPTO: How to patent software, and how to avoid infringing others'
patents

#AWa2: A oneday course on softwarepatents by the DKPTO.  Abstract:
%(bc:Software inventions are mostly protected by copyright, amongst
other because patents are not granted for software %(q:as such) but
requires the presence of an apparatus in some form. This should be
used, if possible, since patents offers a more efficient protection
compared to copyright. At the same time this change means that  there
are many patents on software, which can block a company's development
of new products.)  Not a very clear translation, but it's not a very
well written original.  At least it shows that the patent office is
aware of the problems regarding infringement of software patents.
Their statement that software inventions are protected by copyright is
even more rarely heard from the mouth of patent lobbyists, but it is
quite correct if indirect protection of innovation-related investments
is taken into consideration.

#AWn: A patent-critical website run by a coalition of danish software
professionals

#Tcv: The danish and swedish version of the EPC does not contain the %(q:as
such) clause, which has served the EPO as a pretext for violating the
law.  Patent lawyers in Sweden and Denmark had to rely entirely on the
argument that %(q:european caselaw) needs to be followed, but
apparently that did not cause much trouble for them.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatdk ;
# txtlang: en ;
# multlin: t ;
# End: ;

