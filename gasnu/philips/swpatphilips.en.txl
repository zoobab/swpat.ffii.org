<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Philips and Software Patents

#descr: The dutch electronics giant grew big in the early 20th century when
Holland had abolished patent law.  Historians say that Philips owes
its growth to the absence of patents in Holland at that time. 
Nowadays Philips is one of the largest European software patent
applicants at the European Patent Office (EPO), and the patent
department of Philips is pressing heavily to have software patents
legalised.  They say that Philps is earning more than 1 bn eur from
%(q:IPR licenses).  This figure probably consists mostly of hardware
patents and even copyright licenses.  Statements of Philips patent
lawyers at the European Commission's consultation show that their
thinking is shaped by the consensus of the patent lawyer community
rather than by specific interests of Philips, and it is difficult to
know to what degree the managment of Philips is interested in
questions of patent law at all.  Yet some smaller companies that have
Philips as a customer are afraid of publicly voicing concern about
software patents.

#epat: Philips Software Patents at the European Patent Office (EPO)

#ins: Philips Website

#uMt: Ius Mentis

#isl: A fairly informative website of a Philips patent lawyer.  Presents
recent EPO caselaw to be the authoritative interpretation of the
written law.

#nal: Philips contribution to CEC Software Patentability Consultation of
2000

#rWs: A patent lawyer applauds the European Commission's proposal for
unlimited patentability, states common patent movement beliefs.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatphilips ;
# txtlang: en ;
# multlin: t ;
# End: ;

