<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: PA Axel Pfeiffer and Software Patents

#descr: A German/European patent lawyer who often writes articles and participates in mailing list discussions, trying to convince software professionals that patents are good for them and that Art 52 EPC was never meant to exclude software concepts from patentability.  Pfeiffer believes that software is, like rubber or iron, just another means of embodying an %(q:invention), and most software ideas are inventions according to Art 52 EPC.  Which is why he likes to speak about %(q:software-implemented inventions).

#Kki: Extracts from Discussions with PA AP

#ndr: Editor's Remark

#nla: The dialogs were extracted from mailing list discussions.  Some contributions are not literally quoted but somewhat simplified to fit into this context.  Some of our final replies (to which no further reply of Pfeiffer is listed here) were edited.  All originals can be found through the %(ls:links section) below.

#rWe: Dr. Lenz

#vWe: Es lässt sich unterscheiden:

#fnW: Die Konzeption (z.B. ein Programm zur Fehlersuche in Texten zu schreiben, das ohne ein Woerterbuch auskommt).

#usu: 2. Die Ausführung in einer Computersprache, etwa C, die zu Quelltext führt.

#sci: 3. Das Programm nach einer Kompilierung.

#uke: Sehe ich genauso. Man kan noch %(q:0. Problem- bzw. Defiziterkenntnis) davorsetzen, wenn man mag.

#lQe: Soweit ersichtlich reicht niemand Programme als Quelltext oder als lauffähigen Code beim Patentamt ein.

#baW: Vielmehr betreffen die  Patentansprueche  immer die Ebene 1 oben.

#ihi: Richtig.

#seW: Wer nun (insoweit genau spiegelbildlich zur Ansicht von %(km:Melullis) nur die Ebenen 2 und 3 von der  Patentierbarkeit ausschliessen will (so auch etwa %(st:Tauchert)) ...,

#de9: .. und auch der Gesetzgeber 1973 ...

#cEh: ... schliesst im Ergebnis nichts aus.

#Sil: Was der Gesetzgeber 1973 intendierte, ist klar dokumentiert.

#ztW: Die RSA-Erfindung *ist* nicht Software, ganz einfach aus folgendem Grund: Es ist absolut denkbar, daß derjenige, der sich RSA ausgedacht hat, von Computern nur als Anwender und vom Programmieren und von Software überhaupt keine Ahnung hat. Wie soll er dann Software erfunden haben?

#iWe: RSA kann jedoch mit Software %(q:gebaut) (implementiert) werden (von jemandem, dem bspw. der softwareahnungslose Erfinder sagt: %(q:Schau, ich hab' hier RSA, schreib' mir bitte den Code dazu)), ähnlich wie ein erfundenes Zahnrad mit Metall gebaut werden kann und man nicht auf die Idee käme zu sagen, die Erfindung (das Zahnrad) sei %(q:Metall als solches).

#irh: Wenn der Patentanspruch einen logischen Zusammenhang zwischen Eingabe, Ausabe, Speicher, Rechner und sonstigen Elementen des Universalrechners beschreibt, dann beschreibt er Software.

#riW: Abgesehen davon ist jede korrekt formulierte Rechenregel bereits Software.  Es lässt sich immer ein Umsetzungsprogram (Compiler, Interpreter, ..) bauen, welches die Formulierung versteht und ausführt. Die korrekt formulierte RSA-Regel ist eine mathematische Methode [ als solche ] und zugleich auch schon ein Programm [ als solches ].

#adi: Nicht die sprachliche Einkleidung zählt, sondern die Substanz - und die *ist* nicht Software.

#hhd: Typischerweise wird so etwas wie RSA als %(e:Programm für Datenverarbeitungsanlagen) beansprucht: %(bc:System und Methode, dadurch gekennzeichnet dass ... [ mit den üblichen Mitteln einer Datenverarbeitungsanlage nach einer originellen Rechenregel Daten verarbeitet werden ] ..).  Die %(q:Erfindung) besteht bei RSA sogar in einer %(q:mathematischen Methode) im engsten Sinne.

#tWe: Zuletzt ein noch etwas extremeres Beispiel: Ein hirndübeliger und von Software unbeleckter Zahlen- und Sonstwiemystiker habe   nachts um 12:13 bei Vollmond auf dem Münchner Südfriedhof %(q:herausgefunden), daß es der menschlichen Leistungskraft extrem zuträglich ist (nämlich sie versiebenfacht), wenn immer dann, wenn im Rechenwerk der CPU die Ganzzahl 13 erscheint, auf dem zugeordneten   Bildschirm eine Abbildung von Stonehenge erscheint. Lachen Sie nicht,   solchen Unsinn gibt es tatsächlich! Er hat eine subjektiv extrem wichtige Erfindung gemacht, die - objektiv gesehen - ein Sch... ist. Man nehme   weiter an, daß der Hirnverdübelte sich eine Patentanmeldung stricken läßt und sie einreicht, deren Patentanspruch 1 wie folgt aussehen möge:

#see: Verfahren zur Steigerung der menschlichen Leistungskraft, mit den Schritten Überwachen des Rechenwerks (ALU) einer CPU dahingehend, ob in ihr eine Primzahl vorzugsweise zwischen 5 und 20, insbesondere 13, ansteht, und dann, wenn eine solche Primzahl gefunden wird, Anzeigen eines vorbestimmten Bildes, vorzugsweise von Stonehenge, auf einem Bildschirm.

#Wfs: Dazu kann man folgendes sagen:

#aeg: Im ffii-Jargon wäre obiges eine %(q:swpat-Anmeldung), obwohl ...

#Woh: .. der Erfinder von Software keine Ahnung hat,

#WWr: .. der Patentanspruch 1 nicht Software ist, und

#mrh: .. eine implementierende Software vermutlich nie geschrieben wird.

#fea: Bei Ihrem Beispiel-Patentanspruch geht es nicht um Software. Die verwendete Rechenregel ist altbekannt.  Das Patentbegehren stützt sich nicht auf die Rechenregel sondern auf eine vermeintlich neue und erfinderische Lehre über parapsychologische Zusammenhänge.

#sPW: Es ist irreführend, im Kontext des Patentwesens sich zu obiger Erfindung überhaupt ernsthaft Gedanken zum Thema %(q:Software) zu machen, solange Patentanspruch 1 nicht Software *ist*. Man mag sich fragen, ob das Ding technisch ist (m. E. %(q:ja)), gewerblich anwendbar (sicherlich %(q:ja)) und erfinderisch.

#sah: Wegen der über die Jahre progressiv steigenden Jahresgebühren und des vermutlich ausbleibenden wirtschaftlichen Erfolgs wird irgendwann die n-te Jahresgebühr nicht mehr gezahlt, und das Ding geht geräuschlos unter.

#odx: Mit Software hat das alles nix zu tun.

#Wno: Nur in der SciFi-Welt des Herrn Pfeiffer, in der Software eine Art Werkstoff ist (vgl %(q:Metall als solches) s.o.).

#bKr: Lange Dokumentation von PA Mattias von Kanzlei Beetz, welches neben der üblichen Patent-Evangelium durchaus einiges an interessanten Informationen enthält, so z.B. einen Anhang mit Beispielpatenten, denen ein Platz in unserem Gruselkabinett gebührt. Auch die Darstellung des Patent-Evangeliums ist differenzierter, als wir es von Kollege Pfeiffer gewöhnt sind. Z.B. wird Art 52 EPÜ als eine ernsthafte Hürde, mit der zu rechnen ist (und die beseitigt werden muss, weil sie Art 27 TRIPs widerspricht), dargestellt.

#ine: PA Pfeiffer erwidert %(pl:Prof. Lenz) mit seinen üblichen Argumenten und bekommt danach eine %(ph:Antwort von H. Pilch)

#bln: PA Pfeiffer beklagt sich über die Darstellung seiner Standpunkte auf unseren Webseiten.

#W01: AW: Trivialitaet[Scanned]

#nrb: PA AP beklagt sich abermals.  S. auch den vorigen Gesprächsfaden, in dem AP erklärt, wie er mit dem Kriterium der Erfindungshöhe filtern will:  es dürfe nicht so trivial werden, dass jede genervte Hausfrau auf die Idee kommt.  Dies sei bei Amazons Ein-Klick-Patent der Fall.  Was phm wiederum bestreitet.

#srs: Pfeiffer liest einem Software-Unternehmer die Leviten, der sich über die ausufernden Programmierverbote und den geringen Informationswert von Patentschriften beklagt:  es sei auch beim Umwelt- und Arbeitsrecht selbstverständlich, dass man Vorschriften zur Kenntnis nimmt, und ab- und an sei die Lektüre von Patentschriften ja auch interessant.   Wer partout nicht wolle, dem stehe es ja frei, das Rad neu zu erfinden.

#zea: Kanzlei Beetz & Partner

#ree: Hier arbeitet PA Pfeiffer.

#kll: Swpat-Beispielsammlung von Kanzlei Beetz

#eon: Verweis auf einen recht informative Dokumentation, mit dem ein Kollege der Kanzlei Beetz unter Software-Unternehmern auf Kundenfang geht.

#eej: Pfeiffer: Inhaltliche Beliebigkeit, Taktik

#iWn: Pfeiffer wirft FFII vor, einerseits immer Technizität zu verlangen, andererseits aber es nicht zu würdigen, wenn die Europäische Kommission dieses Wort verwende.  Neu-Abonnenten des Forums reiben sich mit Pfeiffer, und es entsteht eine geräuschvolle Diskussion, in die sich auch Kollege Horns einschaltet.

#ekS: Here Pfeiffer occasionally writes articles. The editors favor software patentability and often quote Pfeiffer as an authority in patent law.

#Wnn: Here too Pfeiffer has, together with other patent attorneys and elderly professors, helped position the association in the pro-swpat camp

#sze: This colleague has repeatedly supported Pfeiffer's attempts to %(q:educate) software professionals about the %(q:real meaning) of Art 52 EPC and has reported with sympathy about what a difficult time Pfeiffer is having due to the intransigent insistence of his discussion partners on their %(q:myths).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatpfeiffer ;
# txtlang: en ;
# multlin: t ;
# End: ;

