<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Österreich und Softwarepatente
#descr: The Austrian government has for a long time been hijacked by patent alwyers.  It always took the most radical pro patent positions in the international patent policy fora.  Austria introduced %(q:program logic utility certificates) in the early 90s in an attempt to prevent patent expansion into this area while providing a more light-weight system for experimenting with exclusivity of algorithms.  However, due to the intrusion of patents into the same field, the utility certificate with its shorter duration wasnot much used.  The Austrian Chamber of Commerce has published an anti-swpat statement in 2000.
#aWW: Of the austrian MEPs, Maria Berger from PSE (SPÖ) initially took a hardline pro-patent position, tabling an amendment in favor of program claims in JURI.  Christa Prets from the same party was more critical.  Mercedes Echerer (Greens) was one of the most vocal critics.   Othmar Karas led the conservative (ÖVP) group into a critical position fairly early, in contrast to the pro-patent hardliners %(LST) who were leading the EPP.  His colleague Paul Rübig of SME Union also supported this approach.  Both signed the amendments of Piia-Noora-Kauppi in the final vote.  FPÖ also took a patent-critical position. Thus, in sharp contrast with its position in the Council, Austria appeared as an almost completely patent-critical country in the Parliament.
#tsW: Wirtschaftskammer 2000
#Wen: Erklärt warum Softwarepatente nicht wünschenswert sind.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatat ;
# txtlang: de ;
# End: ;

