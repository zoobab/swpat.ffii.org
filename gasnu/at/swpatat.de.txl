<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Österreich und Softwarepatente

#descr: Österreichs Parteien trugen im Europäischen Parlament im wesentlichen
die Änderungsanträge vom September 2003 mit.  Insbesondere die ÖVP,
die FPÖ und die Grünen haben deutlich Stellung gegen die
Patentierbarkeit von Software bezogen.

#aWW: Of the austrian MEPs, Maria Berger from PSE (SPÖ) initially took a
hardline pro-patent position, tabling an amendment in favor of program
claims in JURI.  Christa Prets from the same party was more critical. 
Mercedes Echerer (Greens) was one of the most vocal critics.   Othmar
Karas led the conservative (ÖVP) group into a critical position fairly
early, in contrast to the pro-patent hardliners %(LST) who were
leading the EPP.  His colleague Paul Rübig of SME Union also supported
this approach.  Both signed the amendments of Piia-Noora-Kauppi in the
final vote.  FPÖ also took a patent-critical position. Thus, in sharp
contrast with its position in the Council, Austria appeared as an
almost completely patent-critical country in the Parliament.

#tsW: Wirtschaftskammer 2000

#Wen: Erklärt warum Softwarepatente nicht wünschenswert sind.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatat ;
# txtlang: de ;
# multlin: t ;
# End: ;

