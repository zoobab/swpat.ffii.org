<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Herta Däubler-Gmelin und Logikpatente: vehement dagegen, beharrlich dafür

#descr: Allen öffentlichen Äußerungen von 1999 bis 2002 zufolge scheint die Bundesministerin der Justiz nicht nur fest an die %(q:Belohnungstheorie) und die a priori segensreichen Wirkungen des Patentwesens zu glauben, sondern sich auch durch eine besonders patentfreundliche Politik profilieren zu wollen.  Bei Gesprächen in kleinerem Kreise zeigte sich, dass die Ministerin Schwierigkeiten hat, sich eine Welt vorzustellen, in der nicht jede kreative Idee einzeln vom Staat mit einem Monopol belohnt wird.  Auch in der Frage der Genpatente macht sich die Ministerin, trotz gelegentlicher Skrupel hinsichtlich der Fragen der ethischen Vertretbarkeit von Genmanipulationen, vehement für eine weitgehende Patentierbarkeit stark und scheut dabei auch nicht Dissonanzen innerhalb der Koalition.  Sie scheint generell fast blind dem Rat des Münchener Patentpapstes Prof. Straus und der Führung des Deutschen Patentamtes zu vertrauen.  Auch hinsichtlich der Organisations- und Rechenregeln (Software) verfolgte Däubler-Gmelin langezeit eine patentfreudige Linie, aber nach einigen harten Diskussionen konnte sie sich im November 2000 dazu durchringen, energisch für eine Vertagung der geplanten Änderung des EPÜ einzutreten.  Im Frühjahr 2001 äußerte Däubler-Gmelin für die Presse %(q:vehemente Kritik) daran, dass die EU-Kommission %(q:Programme als solche) patentierbar machen wolle.  Unter %(q:Programm als solches) versteht D.G. jedoch nur den urheberrechtlich geschützten Ausdruck, dessen Patentierung ohnehin von niemandem angestrebt wird.  Hinter den Kulissen arbeiteten Däubler-Gmelins Ministerialbeamte weiter auf eine grenzenlose Patentierbarkeit hin.  In geheimen Stellungnahmen (z.B. der Antwort der Bundesregierung auf die Sondierung von 2000/10 sowie Redebeiträgen auf Sitzungen) ermutigten die BMJ-Patentreferenten alle Brüsseler Initiativen in diese Richtung.  In internen Papieren der EU-Kommission wird die Bundesregierung zusammen mit der Britischen Regierung zu den Proponenten einer möglichst unbegrenzten Patentierbarkeit gezählt.  Auf Sitzungen der Arbeitsgruppe für Geistiges Eigentum und Patente des Europäischen Rates setzten sie sich mehrfach dafür ein, über den Vorschlag der Europäischen Kommission hinaus auch unmittelbare Patentansprüche auf %(q:Computerprogrammprodukte) und andere Informationsgegenstände zuzulassen.  Ein offener Brief von MdB Tauss an Frau Däubler-Gmelin, der hier Gegensteuerung verlangte, blieb unbeantwortet.  Überredungsversuche des Virtuellen Ortsvereins der SPD prallten auch hart an Frau Däubler-Gmelin ab.

#Jfk: Däubler-Gmelin 2001-03: vehement gegen Softwarepatente

#DWW: Dies Computer-Zeitung berichtet von einem Interview, welches sie auf der CeBit mit Frau D.G. führte.  Leider wird uns das Interview vorenthalten.  Indirekt zitierte Worthäppchen bleiben mehrdeutig: %(bc:Vehement hat sich Bundesjustizministerin Herta Däubler-Gmelin auf der Hannover Messe gegen Vorschläge der EU ausgesprochen, die Patentierbarkeit von Software generell zu erlauben. Mitte dieses Monats will EU-Kommissar Frits Bolkenstein der europäischen Kommission eine neue Richtlinie zur Patentierung von Computerprogrammen vorlegen. In dieser Richtlinie werden sämtliche Computerprogramme als patentierbar erklärt. Justizministerin Herta Däubler-Gmelin spricht sich gegenüber der Computer Zeitung deutlich gegen diesen Vorschlag aus und fordert die Beibehaltung der jetzigen Regelung, die Softwarepatente nur als Bestandteil eines technischen Verfahrens erlaubt.)  Hiermit spricht sich Däubler-Gmelin offenbar für Bolkesteins Linie aus.  Worin die %(q:vehemente) Kritik bestehen soll, bleibt schleierhaft.  Die gequälte Pseudo-Kritik von Frau Däubler-Gmelin ist möglicherweise als ein politisches Zugeständnis zustande gekommen: die Bundesregierung hatte sich von den EU-Patentjuristen von der Dienststelle für den Gewerblichen Rechtschutz in Bolkesteins %(gb:Generaldirektion Binnenmarkt) vereinnahmen lassen.  Ihr Vertreter fehlte bei einer entscheidenden Sitzung, aber das BMJ ließ sich dennoch als Unterstützer der Softwarepatentierung auflisten.  Hiergegen hatte MdB Tauss bei Däubler-Gmelin protestiert.  Nun musste Däubler-Gmelin Ausgleich schaffen.  Das Signal war natürlich zu schwach und keineswegs geeignet, Bolkesteins Patentjuristen zu bremsen.

#Pan: Pro-Patent-Wahlkampferklärung von Däubler-Gmelin

#Ist: In einer Presseerklärung feiert DG die von ihr für das Patentamt neu geschaffenen %(q:106 neue Stellen für Erfindungsreichtum).  O-Ton: %(q:Deutschland ist ein Hoch-Technologieland, wir leben von unseren Erfindungen. Den Kreativen so schnell, so fachkundig und so unbürokratisch wie möglich zu helfen, ist daher echte Investition in unsere Zukunft. Gerade die Wirtschaft interessiert sich erheblich mehr für Spitzenpersonal beim Patentamt als für die Personalfrage der Union. ... Das Patentwesen war ein ungeliebtes Stiefkind der Kohl-Regierung. Darüber konnten große Sprüche und wilde Fantasienamen wie ,Zukunftsministerium? nicht hinweg täuschen. Für rot-grün ist die Förderung von Innovation hingegen Schwerpunkt der Politik.)

#cle: c't 99/16, S. 72: Patente und Politik

#Ecm: Ein patentfreundlicher Artikel der C'T von 1999.  Geht von der Prämisse aus, dass Patente den kleinen Erfinder schützen und fragt, warum nicht auch im Bereich der Informatik.  Stellt fest, dass zwischen dem Gesetz und der neuesten Praxis der Patentämter eine Lücke klafft.  Software-Patentanwalt Schöninger von Kanzlei Betten fordert baldige Änderung des Gesetzes.  Justizministerin Däubler-Gmelin erklärt im Interview, eine solche Änderung sei nötig und werde kommen.  Das Gesetz verbiete nur die Patentierung von Quelltext, nicht die von Algorithmen.  Die kleinen Firmen machten noch zu wenig Gebrauch vom Patentwesen und seien von der %(q:jahrelang unter Anwälten und Ämtern diskutierten) neuen Patenterteilungspraxis, derzufolge %(q:praktisch alle Computerprogramme patentierbar) seien, zu schlecht unterrichtet.  Hier bestehe Aufklärungsbedarf.  Die Rechtsänderung sei legitim, es handele sich um einen normalen Vorgang der Gesetzesauslegung.  Man merkt, dass Frau Däubler-Gmelin ihre Informationen hauptsächlich von Wolfgang Tauchert, Softwarepatent-Cheftheoretiker im Deutschen Patentamt, bezieht.  Sie versteht möglicherweise nicht, wie Software funktioniert, gibt aber Taucherts Doktrin recht wortgetreu wieder.  Der Unterschied zwischen einem (fast immer patentierbaren) %(q:Computerprogramm) und einem %(q:Computerprogramm als solchem) bleibt jedenfalls unverständlich.

#DiW: Während das BMJ seinen Standpunkt zum Softwarepatent-Richtlinienvorschlag bildet, feiert Däubler-Gmelin euphorisch die Patentinflation und kündet weiteren Ausbau des Patentwesens an.

#Ere: Ein Telepolis-Bericht legt nahe, dass Däubler-Gmelin auch im Bereich der Umsetzung von EU-Kopierschutzrichtlinien mit Beschwichtigungen arbeitet, die einer Täuschung nahe kommen.  S. auch anschließende Diskussion.

#IPb: Im Forum des MdB Mayer ist auch ein Brief des Patentprüfers Günter Schölch an Frau Däubler-Gmelin abgedruckt, der vielleicht dazu beitrug, dass Frau D.G. sich im November 2000 für eine Vertagung der Swpat-Frage entschied.

#Huv: Hier finden sich auch einige Zitate von Däubler-Gmelin

#Enr: Eine möglicherweise noch patentverliebtere Kabinettskollegin

#DeG: Däubler-Gmelins Mentor in Sachen Patentpolitik.  Fliegt unentwegt als Berater nach Berlin und genießt bei D.G. weit mehr Ansehen als irgendwelche Programmierer.

#ZsF: Zu diesem Brief liegt bislang keine Antwort von Frau Däubler-Gmelin vor.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatdgmelin ;
# txtlang: de ;
# multlin: t ;
# End: ;

