<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: UNICE -- The Voice of European [Patent] Business

#descr: UNICE is an association of national industry associations for lobbying
at the EU level.  Its position concerning patents have, like those of
most national industry associations, habitually been dominated by
patent lawyers.  UNICE papers on patent policy are rarely publicized
outside of the circles of patent lawyers.  Some papers are even
published through patent law organs before they appear anywhere else. 
There is virtually no participation of engineers of software
developpers, as can be seen from the lawyer-style in which they are
written.  UNICE treats the enunciations of patent offices and patent
judges as sources of ultimate wisdom, at least as far as they are in
favor of extensive patentability.  UNICE has been instrumental in
lobbying for the EU software patentability directive and has closely
collaborated with the European Commission's patent lawyers in
overcoming the resistance of other departments of the European
Commission.  Protest letters from (indirect) member companies have
regularly been left without reply from UNICE.  Patent-critical
statements of member associations have not found any reflection in the
UNICE position papers.

#U6w: Unice 2002-06-25 position on the patentability of software

#TWn: Unice asks the European Commission to remove all ambiguities from its
directive proposal and to fully endorse the patentability of all
innovative software the enforcability of patent claims against program
texts on disk or on the internet.  Although no author is named, the
paper is characterised by patent lawyer beliefs and reasoning habits. 
It was not widely advertised in public and subsequently published
without commentary in a german patent lawyer journal.  Slightly
earlier protest letters about Unice's position on software patents by
Unice members were left unreplied and did not have any impact on the
extremely pro-patent viewpoint taken by this paper.

#Upp: Unice's german branch took its swpat position paper from Bitkom, where
again it was drafted and pushed through by IBM patent lawyer Fritz
Teufel.

#Mtk: MdP veröffentlicht Unice-Swpat-Papier

#BPl: At

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatunice ;
# txtlang: en ;
# multlin: t ;
# End: ;

