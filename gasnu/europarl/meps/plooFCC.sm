> Please find for your information the draft opinion on software
> patentability by Elly Plooij- van Gorsel.

Thank you very much, that's good news.

I also read Mr. Rocard's draft and am commenting it publicly, see

	http://swpat.ffii.org/papers/eubsa-swpat0202/cult0212/

I find Rocard's draft quite smart, and believe that it will at least
not expose us to more dangers than the current Art 52 EPC, although
it may be insufficient for stuffing all the loopholes.

How public is your draft so far?

On the whole it is very good news, because it sets the targets right.
As to the details, let me make a few quick remarks:

  Patent and copyright protection are complementary.  In very
  general terms, patents protect new technical ideas and principles, while
  copyright protects the form of expression used. For example, a new sort
  of paper might be protected by a patent, while the printed content of a
  newspaper would be protected by copyright. 

The german patent law tradition, as expressed in the

	Dispositionsprogramm Decision (see quotation in Appendix F)
	http://swpat.ffii.org/papers/bgh-dispo76/

sees it differently: "ideas and principles" should in general not be
"protected", and copyright is not necessarily limited to "expression".
Rather, patents are a specialised form of intellectual property for
the field of technical invention (i.e. problem solution involving
forces of nature as indispensable constituent).  In this field, we
have a special kind of economics (simple products depending on one or
a few ideas which are difficult to find and, once known, easy to
implement).  Yet the aim of the patent system is *not* to protect
"ideas and principles" but rather, similar to copyright, reasonably
concrete structures.  Moreover, as Dispositionsprogramm says, patents
and copyright are in different areas and it is a basic design
principle of the IP system to assign one area to one system and avoid
overlapping.  The basic aim of all areas/systems is the same: balance
of interests.  As one US judge put it: "protect the innovator against
the plagiator, protect the public against monopolies."

  In computer terms, the actual code (whether machine-readable or in a
  form which is intelligible to human readers) would be subject to
  copyright protection, while underlying technological ideas may be
  eligible for patent protection.

A very questionable view, see above.
Software is a complex structure of structures which cannot be adequately
described by a division into "expression" and "idea".  Software copyright
does not allow "protection" of ideas for good reasons.  Not in order
to invite patent law to come in and fill the gap.  

  Patent law gives the holder of a patent for a computer-implemented
  invention the right to prevent third parties from using software
  incorporating any new technology he has invented (as defined by the
  patent claims).

No: patent law (in contrast to EPO caselaw) gives the holder of a
patent for a c-i i the right to prevent third parties from using *the*
*invention*.  This does not mean that they may not use or distribute
computer programs which describe the invention.  Otherwise software as
such would have been patented.  A computer-implemented invention can
only be an teaching about use of forces of nature, in which the
software is an unimportant side-aspect of implementation which is not
the object of property.  Software itself (as such) does not belong to
the realm of patents but to the realm of copyright.

  In the EC legal framework as well as in the national laws, the legal
  protection of software is ensured as a intellectual property matter
  (droit d'auteur, Autorenrecht) similar to a litterary work, and not

more directly similar to a scientific article or an instruction manual.
Software in fact is a kind of very well written instruction manual.

  through a patent.

  The main text applicable is the Directive 91/250/EEC on the legal
  protection of computer programs. The European patent law does not ignore
  software, however. The European Patent Convention excludes computer
  programs "as such" (as well as business methods and certain other
  entities) from patentability. 

I would write:

  The European Patent Convention excludes computer
  programs as well as business methods, mathematical methods,
  methods of presentating information and other entities from
  patentability and adds that this exclusion applies only to the
  extent that patent application is directed to the named entities as such. 

In fact there is really nothing mysterious or phoney about the word "as such",
and it doesn't only apply to the computer programs but to all excluded
entities.  See also a german law professor's text on this

	http://swpat.ffii.org/analyse/epue52/exeg/

  However, many patents relating to software and related inventions have
  been granted for devices and processes in technical areas, which cannot
  operate independently of the software components that they implement.
  The majority of these now relate to digital data processing, data
  recognition, representation and information handling. 

Quite good.  I would write:

  However, in recent years, many patents relating to software innovations have
  been granted.  The claims in these patents weve initially framed in terms of
  technical devices and processes and most recently (since 1998) also in terms
  of "computer program products" and "programs on a carrier". 
  Most of these patents cover certain functionalities that may be carried out 
  by a computer program, such as data compression, memory allocation,
  optimisation of ressources and methods of business organisation.


> This has fuelled debate on whether the limits of what is patentable are
> still sufficiently clear and applied properly, especially as the
> different national laws and the EPO do not always take into account the
> same criteria.

> Some advocate that the fact that the European industry is deprived from
> the legal protection of patent as is the case in the USA is detrimental
> to its expansion and competitiveness. But many observers and industry
> leaders in the USA have been underlining the drawbacks of software
> patents in their home market.

good until here

> On the other hand, the opponents to any mention of software in patent
> law fear that software patents become the general rule, thus creating a
> permanent legal uncertainty over the use of algorithms and technical
> solutions that currently circulate freely or create bottlenecks limiting
> innovation.

Problems:

  - nobody is against mentioning software in patent law
  - the problem is not limited to already-existing ideas (which lack
    novelty and thus do not lead to valid patents)

Solution:

  On the other hand, those who demand stricter limits on patentability
  fear that as software patents become more and more widespread, they
  create a permanent legal uncertainty over the use of basic ideas that
  would otherwise circulate freely, or create bottlenecks limiting
  innovation.

> The existence and not the absence of patents could become the real
> threat to the competitiveness of the European industry - software and
> beyond. It should be noted that the European software industry is not
> unanimous in supporting demands for European patents. Innovative SMEs in
> particular see them as a menace as it would a contrario leave them
> totally exposed whenever, for time or financial reasons, they could not
> afford protecting their inventions through this costly and lengthy
> procedure.

Problems:
  - even if patenting was cheap and you could patent everything you wrote,
    your software would still (and a fortiori) infringe on lots of patents 
    of other people.  The cost problem is secondary.

Solution:

  The existence and not the absence of patents could become the real
  threat to the competitiveness of the European industry - software and
  beyond. It should be noted that the European software industry is not
  unanimous in supporting demands for European patents. Innovative SMEs in
  particular see them as a menace because every major software project may
  contain hundreds of ideas which could have been already patented by others,
  who may not even be interested in exchanging licenses.  Studies show that
  most innovative software SMEs do not apply for patents, because building
  a patent portfolio is for them not worth the costs, and because copyright
  already offers reasonably effective protection.

> The proposed Directive will not make it possible to patent computer
> programs "as such". In broad terms, nothing will be made patentable
> which is not already patentable. The objective is simply to clarify the
> law and to resolve some inconsistencies in approach in national laws.

Not true.
  - the proposal does promote the patenting of computer programs as such.
    See Article 1:  c.i.i. is a claimed object whose prima facie novel aspect
    lies in the computer program.
  - They claimed objective is to clarify the law.  What is achieved is different,
    and I dare say: thou shalt recognize the tree by its fruits:  the achievement
    is the real objective. 

Suggestion:

   The proposed Directive is, according to its Explanatory Memorandum,
   not intended make it possible to patent computer programs as such. In
   broad terms, nothing is intended to become patentable which is not
   already patentable. The objective is simply to clarify the law and to
   resolve some inconsistencies in approach in national laws.

> However, it is clear that, despite the Commission's claims, it paves
> the way to a broader use of patents as a model for protecting
> computer software. Two orders of questions are open: the political
> opportunity of such a move, and, if yes, the criteria for defining
> the borders of patentability in such a way that abuses and perverse
> effects are avoided.

Problems:
  -- "it is clear that" may be seen as insufficiently reasoned
  -- no need to criticise the European Commission
  -- Instead, the authority of the EPO model should be questioned
  -- the downsides may lie not only in "abuses and perverse effects" 

Suggestion:

  However, the Commission also points out correctly that the current
  judicial practise is ambiguous, and it is clear that among the various
  possible interpretations of Art 52 EPC the European Commission is
  proposing to codify an interpretation that confirms the disputed recent
  practise of the European Patent Office and makes it difficult if not
  impossible to return to a more restrictive patenting practise, such
  as the one which the EPO abandoned in the late 80s and which some of
  the member countries and member candidate countries have not yet
  abandoned.  This leads to the question of why the current practise
  of the European Patent Office should be the model on which the directive
  is built.  Is such a move politically opportune?  And, more importantly,
  does it allow us to define the borders of patentability in a clear
  and adequate way?

> In our view, the scope of the Directive - if there is one - should
> therefore be strictly limited to unequivocal cases where adverse effects
> would not jeopardise the usefulness of the protection.

Not quite clear to me.  What about:

In our view, the scope of patentability should therefore be strictly
limited to an area in which it is clear that the restriction of
freedom, which is necessarily imposed by patents, will be justifiable
in view to a macro-economic gain in terms of increased production of 
innovations.  

> Finally, it should be noted that patents and copyright are not the only
> instruments for protection: designs, models, trademarks enjoy specific
> protection schemes, and even in the field of technical inventions
> patents are flanked by the more flexible system of utility models. There
> is therefore no conceptual hinderance to developing ad hoc protection
> schemes suited to the specificities of computer software: patents are
> dispensable.

Very good.

> AMENDMENTS

> <TitreAm>Recital 5</TitreAm>

> Therefore, the legal rules as interpreted by Member States' courts
> should be harmonised and the law governing the patentability of
> computer-implemented inventions should be made transparent. The
> resulting legal certainty should enable enterprises to derive the
> maximum advantage from patents for computer-implemented inventions and
> provide an incentive for investment and innovation.	Therefore, the legal
> rules governing the patentability of computer-implemented inventions
> should be harmonised so as to ensure that the resulting legal certainty
> and the level of requirements demanded for patentability enable
> innovative enterprises to derive the maximum advantage from their
> inventive process and provide an incentive for investment and
> innovation.
> <Crossref></Crossref>

> <TitreJust>Justification</TitreJust>

> <AmJust>The objective of any patenting law  is not to ensure that
> patent-owners enjoy an advantage:  the advantage granted to the
> patent-owner is only a means to encourage the inventive process, for the
> benefit of the society as whole. The advantages granted to the
> patent-owner must not work against this ultimate objective of the patent
> principle</AmJust>

Very good.
Unfortunately the Preamble contains quite a lot more points that need
similar debugging.

> <TitreAm>Article 2, letter (a)</TitreAm>

> (a) ``computer-implemented invention'' means any invention the
> performance of which involves the use of a computer, computer network or
> other programmable apparatus and having one or more prima facie novel
> features which are realised wholly or partly by means of a computer
> program or computer programs;	(a) ``computer-implemented invention''
> means any invention susceptible of industrial application the
> performance of which involves the use of a computer, computer network or
> other programmable apparatus and having one or more prima facie novel
> features which constitute a technical contribution and require to be
> realised wholly or partly by means of a computer program or computer
> programs;
> <Crossref></Crossref>

> <TitreJust>Justification</TitreJust>

> <AmJust>The wording of the Commssion proposal is too vague and leaves
> aside the technical aspect which characterise an invention as opposed to
> an idea. This distinction is of utmost importance, not only for a
> theoretical legal point of view, but above all to guarantee that the
> competition in an economic sector is not hindered by the monopolisation
> of a given business method or management recipe by one operator only on
> a given market.</AmJust>

Why not also insert something like 'calculation rule' or 'mathematical method'?
Theses are usually even broader than business methods and tend to encompass
business methods (e.g. linear optimisation as a solution to the problem of
the travelling salesman:  the EPO has patented the Karmarkar algorithm for
linear optimisation which is used for allocating passengers to airplanes just
as much as for optimising oil drilling).

> (b) ``technical contribution'' means a contribution to the state of the
> art in a technical field which is not obvious to a person skilled in the
> art.	(b) ``technical contribution'' means a contribution involving an
> inventive activity to a technical field which solves an existing
> technical problem or extends the state of the art in a significant way
> to a person skilled in the art.
> <Crossref></Crossref>

> <TitreJust>Justification</TitreJust>

> <AmJust>In a rapidly moving field like the software and software-related
> industries, where most inventions come from SMEs, sometimes very small
> and young, which rely more on cross-fertilisation than on law firms'
> advices, a grace period is necessary to avoid that an inventor is
> spoiled of his/her invention when s/he has made it public a few weeks
> before applying for a patent, usually as a testbed of the market
> attractivity of its creation.</AmJust>

Here the patent lobby will say that it agrees with your view but the swpat directive
is not the right place to discuss it.  This might weaken your credibility.
If you want to introduce the novelty grace period issue here, it might be better
to propose it in a specially dedicated article.

In this article, it is important not to leave its more central aspects
uncriticised.  Suggestion:

  For delimiting the field of patentability, more than a
  circular definition of ``technical contribution'' is needed.
  Possibilities are
    (1) a definition of "technical invention"  along the lines of
        traditional patent doctrines, such as
        - "teaching about a plan-conformant use of controllable forces
           of nature for achieving a causally overseeable effect,
           which is the immediate result of the forces of nature"            
           (German Federal Court of Justice Anti Blocking System Decision of 1980)
        - "advanced utilisation of laws of nature" (Japanese Patent Act)
        - "problem solution involving controllable forces of nature",
          "teaching about cause-effect relations in use of controllable forces of nature"
           (FFII/Eurolinux proposals)          
    (2) an catalogue of entities which are not invention, such as
        in Art 52 EPC
    (3) a catalogue of model decisions on example patents, in which the use of 
        the terminology is clarified and which may be periodically revised by 
        the Parliament.  The CEC Directive does this indirectly by citing
        recent EPO decisions such as "Pension Benefit System" as models.

> <TitreAm>Article 4, paragraph 1</TitreAm>

> 1. Member States shall ensure that a computer-implemented invention is
> patentable on the condition that it is susceptible of industrial
> application, is new, and involves an inventive step.	1. Member States
> shall ensure that a computer-implemented invention is only patentable on
> the condition that it makes a technical contribution as defined in
> article 2(b).

Problem: "technical contribution" and "invention" are synonyms in correct
patent language, and the above rule is tautological, because all inventions
are and must be patentable according to Art 52 EPC and Art 27 TRIPs.  The
question is whether an invention (= a technical contribution) is present at
all.  Whether it is "computer-implemented" or not is irrelevant (merely a
confusion tactic of the patent lobby).   This makes the above provision
difficult to debug.  I would write something like:

	Member States shall ensure that technical inventions are patentable
	and that rules of organisation and calculation are not patentable,
        regardless of whether during their implementation a computer is used
        and of how they are presented in the claims.

> <AmJust>This wording makes the article consistent with the previous
> amendments. /AmJust>

> </Amend><Amend>Amendment <NumAm>6</NumAm>

> <TitreAm>Article 4, paragraph 2</TitreAm>

> 2. Member States shall ensure that it is a condition of involving an
> inventive step that a computer-implemented invention must make a
> technical contribution.	Deleted
> <Crossref></Crossref>

> <TitreJust>Justification</TitreJust>

> <AmJust>This wording becomes redundant with the previous amendments.
> /AmJust>

> </Amend><Amend>Amendment <NumAm>7</NumAm>

> <TitreAm>Article 4, paragraph 3</TitreAm>

> 3. The technical contribution shall be assessed by consideration of the
> difference between the scope of the patent claim considered as a whole,
> elements of which may comprise both technical and non-technical
> features, and the state of the art.	3. The significant extent of the
> technical contribution shall be assessed by consideration of the
> difference between the technical elements included in the scope of the
> patent claim considered as a whole and the state of the art.  Elements
> disclosed by the person applying for a patent over a period of six
> months before the date of the application are not considered as being
> part of the state of the art when assessing that particular claim.
> <Crossref></Crossref>

Problem: 
  -- as above: the novelty grace period is a separate problem.
  -- "as a whole" is very dangerous.  It means that we are not
     comparing the real invention but the claim wording with the state
     of the art, i.e. we are handing control over from the examiner
     to the patent lawyer who drafts the claims.  In principle, it is
     a bad idea to regulate the formalities of examination by law.
     This provision only encourages formalistic approaches and
     consequently loopholes.

Suggestion: Delete or replace by a wording from the EPO Guidelines of 1978,
which you find in two versions in my evaluation of Rocard's proposal (see above).

> <AmJust>This wording makes the article consistent with the previous
> amendments. The reference to a so-called "grace period" ovelaps with an
> on-going debate in general patenting law, as a similar concept exists in
> some legal systems (in particular US), but not in the European Union
> legislation nor in the rules of the European Patent Office. Introducing
> a European patentability of software inventions, while depriving the
> inventors of the flexibility of early communication would create an
> unnecessary bottleneck at the expense of innovative SMEs and of the
> university-enterprise co-operation.< /AmJust>

>	  Exclusions from patentability
>	  A computer-implemented invention shall not be regarded as making a
> technical contribution merely because it involves the use of a computer,
> or other apparatus. Accordingly, inventions involving computer programs
> which implement business, mathematical or other methods, which
> inventions do not produce any technical effects beyond the manipulation
> and representation of information within computer-system or network
> shall not be patentable.

> <TitreJust>Justification</TitreJust>

> <AmJust>The rule that an invention, whatever its scope, is only
> considered as such when it does have real effects on the real world, is
> a fundamental principle of patent law, constantly confirmed both in the
> legislations and Court decisions.</AmJust>

Problem: 
  -- not clear enough.
  -- calculation rules (often called software algorithms) 
     are broader than business methods and encompass them.
     Excluding business methods without excluding calculation
     rules doesn't work.  It is nothing but a political trick which
     singles out software developpers for special victimisation and
     in the end still hits the big mass.

> Member States shall ensure that a computer-implemented invention may be
> claimed as a product, that is as a programmed computer, a programmed
> computer network or other programmed apparatus, or as a process carried
> out by such a computer, computer network or apparatus through the
> execution of software.	(a) Member States shall ensure that a
> computer-implemented invention may be claimed as a product or a
> production process.
	  (b) Member states shall ensure that the publication or distribution of
> information in whatever form can never constitute a direct or indirect
> patent infringement. 
	  (c) Member states shall ensure that the execution of a computer program
> for purposes that do not belong to the scope of the patent can not
> constitute a direct or indirect patent infringement.
	  (d) Member states shall ensure that whenever a patent claim names
> features that imply the use of a computer program, a well-functioning
> and well documented reference implementation of such a program shall be
> published as a part of description wihtout any restricting licensing
> terms. 

Great. Thank You! 

> <AmJust>The effect of patents is to ensure an economic monopoly. It
> should not deter development and pursuit of innovation by competitors.
> </AmJust>

> </Amend><Amend>Amendment <NumAm>10</NumAm>

> <TitreAm>Article 6 a (new)</TitreAm>

	  Member states shall ensure that wherever the use of a patented
> technique is needed for the sole purpose of ensuring conversion of the
> conventions used in two different computer systems or network so as to
> allow communication and exchange of data content between them, such use
> is not considered to be a patent infringement.
> <Crossref></Crossref>

> <TitreJust>Justification</TitreJust>

> <AmJust>The possibility of connecting equipments so as to make them
> interoperable is a way of ensuring open networks and avoiding abuse of
> dominant positions. This has been specifically ruled in thecase law of
> i.a. the Court of Justice of the European Communities. The patent law
> should not give a possibility to overrule this principle at the expense
> of free competition and users.</AmJust>

> </Amend>

Great. Thank you!

I hope my comments are useful.

May I put you on the softprop-en@eurolinux.org list, where a group of
software property experts (interested programmers and lawyers) are at
your disposition to answer questions concerning amendment proposals?

-- 
Hartmut Pilch, FFII & Eurolinux Alliance              tel. +49-89-12789608   
Protecting Innovation against Patent Inflation	     http://swpat.ffii.org/
130,000 signatures against software patents      http://www.noepatents.org/
