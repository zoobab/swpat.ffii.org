#!/usr/bin/env perl

#This script must work receiving the HTML source of the MEP page
#through a pipe. Once it's finished, having all the HTML files 
#a directory and performing a
#'for a in *html; do cat "$a" | perl europarser.pl >> info; done'
#should do the job.

my ($name, $euro_party, $national_party, $country, @comm, @deleg);
my (@euro_address, @fax, @email, $webpage, @national_data, $id);

@data = <>;

$i = 0; #index var


while (!($data[$i] =~ /\<STRONG\>/i)) {
	$i++;
}

#Now we are in the line containing the name.

chop($name = $data[$i]);
$name =~ s/\<(|\/)STRONG\>//g;

#Debug
#print "NAME: $name\n";

#We are also inside the first cell, which contains all the committees and
#delegations the MEP belongs to. So, until we reach the end of the cell,
#we get all the lines that start with "Committee" or "Delegation". It also
#contains the political party (which is in the next line). So we continue
#reading the file.

$i=$i+2; #The next line is two HTML lines after the current one (that's because
#of the <BR> tag.

$euro_party = $data[$i];

#Debug
#print "PARTY: $euro_party";

while (!($data[$i] =~ /\<BR\>/ && $data[$i+1] =~ /\<BR\>/)) {
	$i++;
	if ($data[$i] =~ /^Committe.*/) {
		push(@comm, $data[$i]);
	}
	if ($data[$i] =~ /^Delegation.*/) {
		push(@deleg, $data[$i]);
	}
}

#Debug
#print "COMMITTEES: @comm" if (@comm);
#print "DELEGATIONS: @deleg" if (@deleg);

#Get his/her country. 2 lines below here.

$i = $i+2;

chop($country = $data[$i]);

#Now we can get his/her national party's name. It's 2 lines below our 
#current position.

$i = $i+2;

chop($national_party = $data[$i]);

#Debug
#print "NATIONAL PARTY: $national_party\n";

#From this point, there are two horizontal lines (<HR> tag) before we get
#to the next cell containing some data of interest. So let's advance.

$aux_i = 0;

while ($aux_i != 2) {
	if ($data[$i] =~ /.*\<HR\>.*/) {
		$aux_i++;
	}
	$i++;
}


#Now there are two cells that begin with the tag:
#<TD nowrap width="50%" valign="top">
#Inside them, there are the European addresses for the MEP, along
#with a fax number. 

while (!($data[$i] =~ /\<TD nowrap width="50%" valign="top"\>/)) {
	last if ($data[$i] =~ /.*\<bgcolor\>.*/); #Some MEPs have only one European address
	$i++;
}
$i++;
#The index is pointing to the first half of the table now.


$euro_add1 = $data[$i];
$euro_add1 =~ s/\<BR\>/; /g;
$euro_add1 =~ s/\<(|\/)B\>//g;


#Continue to the next address.

while (!($data[$i] =~ /\<TD nowrap width="50%" valign="top"\>/)) {
	last if ($data[$i] =~ /.*bgcolor.*/); #Some MEPs have only one European address
	$i++;
}
$i++;


$euro_add2 = $data[$i];
$euro_add2 =~ s/\<BR\>/; /g;
$euro_add2 =~ s/\<(|\/)B\>//g;

@euro_address = ($euro_add1, $euro_add2);

#Debug
#print "EURO ADDRESS: @euro_address";

#Go on. Get the fax numbers.

while (!($data[$i] =~ /\<TABLE id="FAX".*/)) {
	last if ($data[$i] =~ /.*\<HR\>.*/); #Some MEPs don't have fax number.
	$i++;
}

$fax_1 = $data[$i];
$fax_1 =~ s/.*\<TD width="80%"\>(.*)\<\/TD.*/$1/;

$i++;

#The same for the second fax numer.

while (!($data[$i] =~ /\<TABLE id="FAX".*/)) {
	last if ($data[$i] =~ /.*\<HR\>.*/); #Some MEPs don't have fax number.
	$i++;
}

$fax_2 = $data[$i];
$fax_2 =~ s/.*\<TD width="80%"\>(.*)\<\/TD.*/$1/;

@fax = ($fax_1, $fax_2);

#Debug
#print "FAX: @fax";


#Now it dependes on the MEP. The structure has been the same for everyone
#until now. But at this point, two things can happen:
#a) The MEP has e-mail, web page or both. 
#b) The MEP hasn't got any of them.
#
#If there are 2 <HR> tags remaining, there is some e-mail or web page. If 
#there is only 1, only the Postal address is waiting ahead.
#There are some that don't have any other data. So, $remaining_HR = 0;

$aux_index = $i;
$remaining_HR = 0;

while ($aux_index < $#data) {
	if ($data[$aux_index] =~ /.*\<HR\>.*/) {
		$remaining_HR++;
	}
	$aux_index++;
}


#So now it depends. Time to use some functions.


if ($remaining_HR == 1) {
	#There may also happen that they have e-mail but not national data. So let's see
	#if there's any mailto: tag.
	$aux_index2 = $i;
	$just_mail = 0;
	while ($aux_index2 < $#data) {
		if ($data[$aux_index2] =~ /.*mailto.*/) {
			$just_mail = 1;
		}
		$aux_index2++;
	}
	if ($just_mail) {
		&mailnweb;
#		print "E-MAIL: @email\n";
#		print "WEB PAGE: $webpage\n";
	} else {
		&address;
#		print "NATIONAL DATA:\n @national_data\n";
	}
} elsif ($remaining_HR == 2) {
	&mailnweb;
	#Debug
#	print "EMAIL: @email\n";
#	print "WEB PAGE: $webpage\n";
	&address;
	#Debug
#	print "NATIONAL DATA:\n @national_data";
}

sub mailnweb {
	while (!($data[$i] =~ /.*\<HR\>.*/)) {
		$i++;
	}
	$i++;

	#Now we are between two HRs. In the middle, all the "mailto:"
	#<a> tags are e-mail addresses. And all the "href" are web pages.
	#Note that there may be more than one e-mail address, but only
	#one web page.
	
	while (!($data[$i] =~ /.*\<HR\>.*/)) {
		last if ($i == $#data); #Some MEP pages don't have another HR at the end.
		@garbage = split(/href/,$data[$i]); 
		for $line (@garbage) {
			if ($line =~ /.*mailto/) {
				$line =~ s/="mailto:.*\">(.*)<\/A.*/$1/;
				push @email, $line;
			}

			if ($line =~ /.*http.*/) {
				$line =~ s/=(.*) target.*/$1/;
				$webpage = $line;
			}
		}
		$i++;
	}
}

			
sub address {

	while (!($data[$i] =~ /.*Postal address.*/)) {
		$i++;
	}
	$i++;
	
	#Now we are in the last cell. Let's get the postal address, and we're
	#done.

	while (!($data[$i] =~ /\<\/TD\>/)) {
		if (!($data[$i] =~ /\<BR\>/ || $data[$i] =~ /\<\/TD\>/)) {
			push @national_data, $data[$i];
		}
		$i++;
	}
}

#Finally, the ID. I'll calculate it only if there's an europarl
#e-mail address.

if (@email) {
	foreach $address (@email) {
		if ($address =~ /.*europarl.*/) {
			$id = $address;
			$id =~ s/(.*)\@.*/$1/;
		}
	}
}

#Debug
#print "ID: $id\n";
#print "----------------------------------------------\n\n\n";
#

####################################################################
#                   SECOND PART: FILE WRITING                      #
####################################################################

chomp($euro_party);

chomp($identification = $id);

$name2 = $name;

#There are problems with some chars.
$name2 =~ s/Ã§/Ç/;
$name2 =~ s/Ã©/e/;
$name2 =~ s/-/ /;

$name2 =~ /(\w)\w* (\w*)$/;
$identification = lc($1.$2) if (!$id);

#It's not completely sure that we have the id now. But it's better than nothing.

print "name : $identification : $name\n";
print "euparty : $identification : $euro_party\n";
print "natparty : $identification : $national_party\n";
print "land : $identification : $country\n"; #Hartmut, if you want "it" instead of 
#Italy (for example), just tell me, I'll try to do it.
foreach (@comm) {
    $comm_name = $_; 
    $comm_name =~ s/Committee on//;
    $comm_name =~ s/Foreign Affairs, Human Rights, Common Security and Defence Policy/AFET/;
    $comm_name =~ s/Budgets/BUDG/;
    $comm_name =~ s/Budgetary Control/CONT/;
    $comm_name =~ s/Citizens' Freedoms and Rights, Justice and Home Affairs/LIBE/;
    $comm_name =~ s/Economic and Monetary Affairs/ECON/;
    $comm_name =~ s/Legal Affairs and the Internal Market/JURI/;
    $comm_name =~ s/Industry, External Trade, Research and Energy/ITRE/;
    $comm_name =~ s/Employment and Social Affairs/EMPL/;
    $comm_name =~ s/the Environment, Public Health and Consumer Policy/ENVI/;
    $comm_name =~ s/Agriculture and Rural Development/AGRI/;
    $comm_name =~ s/Fisheries/PECH/;
    $comm_name =~ s/Regional Policy, Transport and Tourism/RETT/;
    $comm_name =~ s/Culture, Youth, Education, the Media and Sport/CULT/;
    $comm_name =~ s/Development and Cooperation/DEVE/;
    $comm_name =~ s/Constitutional Affairs/AFCO/;
    $comm_name =~ s/Women's Rights and Equal Opportunities/FEMM/;
    $comm_name =~ s/Petitions/PETI/;
    $comm_name =~ s/,/ :/;
    print "committee : $identification :$comm_name";
}
#I'm not sure the above is totally right. Please, send me any patch you may do.

#--->IMPORTANT<---- I NEED THE DELEGATIONS ACRONIMS TO DO THE SAME THAT I DID ON COMMITTEES.

foreach(@deleg) {
    $_ =~ s/,/ :/;
    print "delegation : $identification : $_";
}

foreach (@euro_address) {
    print "adreu : $identification : $_";
}

foreach (@fax) {
    print "fax : $identification : $_";
}

foreach (@email) {
    $_ =~ s/\n//; #I'd do chomp($_), but not all them have \n
    print "mail : $identification : $_\n";
}

if ($webpage) {
    print "wpage : $identification : $webpage";
}

$natdat = "";
foreach (@national_data) {
    $_ =~ s/\n/ /;
    $natdat .= $_;
}
print "adrnat : $identification : $natdat\n\n";



