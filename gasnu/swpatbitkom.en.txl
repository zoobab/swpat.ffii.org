<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#chr: Find out contact persons and phone numbers and report on results.

#poW: Supply contact data

#SeF: Contact %(MT) to obtain some addresses and phone numbers and instructions on what to ask them.  You will receive a response within 1 day.

#mfa: Contact Bitkom member companies

#tWP: Bitkom Wiki Page

#Mlt: Manches mag gegen eine Urheberrechtssteuer auf Drucker und Festplatten sprechen.  Diese Steuer ist jedoch gesetzlich geregelt, und die Argumente mit denen HP und, als deren Sprachrohr, Bitkom sich ihr zu entziehen versuchen, sind weitaus bedenklicher als die Urheberrechtssteuer selber.  Denn Individuelle Vergütungsverfahren nach Bitkom-Wunsch %(ul|sind bislang nirgends in nennenswertem Umfang im Einsatz|führen zu einer Wertminderung dessen, was vergütet werden soll|führen ebenso wenig wie andere Verfahren zu %(q:gerechter Entlohnung von Leistungen)|führen geradeaus zu den Hollywood/Hollings-Gesetzentwürfen, gegen die die sich die gesamte amerikanische IT-Welt derzeit auflehnt|führen im Erfolgsfall zu %(rr:unübersehbaren Konfliktszenarien).)  Wer aufgrund einer Utopie dieser Beschaffenheit im Ton der Entrüstung gegen die GEMA Sturm laufen zu müssen glaubt, ist wohl einfach nur von allen guten Geistern verlassen.

#Gig: GEMA droht mit Millionenklage wegen Abgabe auf CD-Brenner

#bca: Bitkom report about an enquete conducted among member companies.  Unfortunately the catalogue of questions was not published.  The report was worked out by Teufel's patent lawyer workgroup.  Questions about the usefulness of patents for a company strategy are reinterpret as endorsements of a certain patent policy.  Ambiguous terms such as %(q:status quo) or %(q:current legal situation) are interpreted so as to make a lack of enthusiasm for patents appear like support of patentability.  60% of the respondents pronounced themselves against %(q:extension of patentability).

#Wsn: Bitkom Enquete:  84% see patents as useful means of protection

#WeU: It should be noted that quite a few surveys about industry opinion on software patents have been conducted in the United States (e.g. Effy Oz, Samulea Pamuelson, ACM Titus, OTA, USPTO hearings, ...) and also here (Fraunhofer 2001, CEC Consultation), and -- surprise -- most of them were much more strongly against software patents than that of Bitkom.  To be documented here.

#knr: Indeed nobody opposes patents on %(q:computer-implemented inventions).  It is just that many of us still believe that, while a chemical reaction or an anti-blocking system can be patented regardless of whether a computer is used for controlling it, computer programs as such are not inventions in the sense of patent law.

#eaf: Here FFII would have said %(q:maintain), and again Bitkom would have reported to MEPs that %(q:FFII is in favor of software patents).

#eue: reduce:

#atn: maintain:

#xed: extend:

#gtr: Here, among 90 member companies interviewed, the answers are as follows:

#pWa: Should the current possibilities for obtaining patent protection for computer-implemented inventions be maintained, extended or reduced?

#WWs: One question comes close to asking for a political opinion. Bitkom asks approximately:

#8cl: and interprets the 82% %(q:yes) to any such question as an %(q:82% in favor of software patents).  FFII would also have answered %(q:yes).

#vrW: Do you believe that patents can help your company gain easier access to risk capital?

#Wts: Do you believe that patents can play an important role in strengthening a company's bargaining position?

#euv: The survey asks one basic appetizing question in several variations:

#iWc: The original questionnaire is not included in the study, but at least some of the questions are indirectly quoted in the report.

#ehW: In 2002 Teufel's industrial property committee prepared an %(um:unpublished survey) which was used by EICTA lobbyists as a basis for their assertions that %(q:surveys show that most software companies are in favor of patents), which they have been making at the European Parliament and also in Berlin during this time.

#IBe: Im Vorstand von Bitkom sitzen Geschäftsleute von Microsoft (Sibold), IBM, Siemens und anderen Großunternehmen.  Auch in den Arbeitsausschüssen haben vor allem diese Leute die nötige Zeit, um die Verbandspolitik zu bestimmen.  Im Vorstand ist keine einzige Person vertreten, die Ansehen als Programmierer oder Informatiker genießt.  Für die einfachen Mitglieder ist Bitkom weniger eine Interessenvertretung als eine Gelegenheit, gewisse Leistungen in Anspruch zu nehmen, vgl ADAC.

#Ect: Ein Bitkom-Sprecher kann offenbar fachlich noch so ahnungslos daherreden -- Hauptsache die Gesinnung stimmt.  Solange er seine Forderungen an der Devise %(q:Mehr Macht für die Vorstände) ausrichtet, wird er schon nicht zur Rechenschaft gezogen werden.

#Dsr: Dieser Standpunkt wir allerdings nicht besonders publik gemacht, er findet sich nur seit kurzem in unauffälligen PDF-Dateien hinter kryptischen Überschriften (%(q:SWP-Gutachten)).

#FeW: Für Bürokratisierung und staatliche Regulierung durch Patente

#nWW: nicht nur hinsichtlich Inderimport:  möglichst viele Rechte und wenige Pflichten für Unternehmensvorstände

#Gee: Gegen Bürokratisierung und staatliche Regulierung aller Art

#dha: die Schröder-Ankündigung auf CeBit 2001 stützte sich vor allem auf eine Bitkom-Studie.  Seit dem IT-Konjunktureinbruch im zweiten Halbjahr hört man weniger davon.

#Foe: Für den Import billiger IT-Fachkräfte

#HnE: Besonders der Vorsitzende Rohleder ist von DRM und anderen Hoffnungen auf Verkrüppelung digitaler Information zwecks Kassierens pro Lesevorgang begeistert.  Auch der Dachverband EICTA setzt hier einen Schwerpunkt.  Rohleder und EICTA-Kollegen fordern strafrechlich bewehrten Kopierschutz und Einschränkung der Programmierfreiheit gemäß neuer EU-Kopierschutzrichtlinie (EuroDMCA).  Dort, wo die IT-Honoratioren neue Wertschöpfungspotentiale wittern, muss der Staat hart durchgreifen, um deren Verwirklichung zu erzwingen.  Ob diese Art der Wertschöpfung funktioniert oder produktiv ist, interessiert vorerst nicht.  Der Staat hat so zu tun, als ob sie funktionieren würde und schon heute -- ja sogar rückwirkend für die Vergangenheit -- ordnungspolitische Entscheidungen, z.B. hinsichtlich Pauschalgebühren, an dieser Fiktion auszurichten.

#Gei: Gegen Urheberrechtspauschalgebühren, für Verwertungsutopien

#Beh: Bitkom has recently been fighting for

#tmW: Bitkom Survey 2002

#Wee: Organisational structure of Bitkom

#RAn: Baseline: More Power for Corporate Boards (?)

#EWh: Ein Bitkom-Sprecher versichert, 20000 Gutachten hätten bewiesen, dass Bedenken gegen die UMTS-Strahlen unbelegt seien und warnt vor wirtschaftlichen Schäden, welche die anhaltende Gesundheitsdebatte erzeuge.

#Btr: BITKOM: Gesundheitsdebatte beenden, UMTS zügig ausbauen!

#elr: Susanne Schopf und Bernd Rohlehder erklären, ohne Softwarepatente gebe es keine Innovation, müsse die Branche ins Ausland wandern, fordern vom Rat, mit dem Europäischen Parlament auf Konfrontationskurs zu gehen oder dessen Entscheidung zu kippen: %(orig|Bitkom setzt sich dafür ein, dass der Ministerrat in seiner Stellungnahme nicht den Irrwegen des Parlaments folgt, sondern seinen bisherigen vernünftigen Kurs weiterverfolgt und sich im Interesse der europäischen Wirtschaft für einen angemessenen Patentschutz für computerimplementierte Erfindungen einsetzt.).

#2nr: Bitkom-PE 2003-03-22:

#HWk: Heise-Bericht über die Bitkom-Erklärung

#Htn: Heise: IT-Verband sieht bei Software-Patenten Nachbesserungsbedarf

#descr: The German Information and Telecomunication Industry Association Bitkom, one of the influential members of the European EICTA.org, began in 2001 to get involved in questions of patent policy.  The activity was conducted in a closed circle of patent lawyers, dominated by IBM's european patent deparment head Fritz Teufel.  This circle published a first statement in support of the CEC/BSA directive proposal in spring 2002 after adoption by the meeting between 7 patent lawyers of large corporations, held by the IP workgroup under the presidence of Teufel.  This workgroup later published a survey which tries to demonstrate that the software industry wants patents.  This survey was used for supporting talks with politicians for many month before the survey was finally published, after we had reported about it.  The survey does not show what its authors claim it shows.

#title: Bitkom: Voice of IT corporate patent lawyers in Germany

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbitkom ;
# txtlang: en ;
# multlin: t ;
# End: ;

