<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#lhc: MEP McCarthy mentions Alcatel's success story and appears very preoccupied with the function of patents in an environment where production costs are lower elsewhere.

#ctm: The place of higher formation where Heitto and many others, forming a large influential network of colleagues spread over all kinds of institutions, were inculcated with the patent movement's philosophy and corporatist spirit.

#bse: Alcatel's website expresses Alcatel's desire to control upcoming Internet standards.

#ins: Alcatel Builds Next Generation Networks

#rWr: A patent expert from the legal department of Alcatel applauds the European Commission's proposal for unlimited patentability, explains that all patents stimulate innovation and the fact that the US has the most advanced software industry sufficiently proves that software patents, just like patents in any other field of technology, stimulate software innovation.  Moreover, the most important reason to support patents is that small and medium enterprises (SMEs) badly need them.

#WCC: Alcatel 2000: Reply to CEC Software Patent Consultation

#emW: Alcatel has an exceptional patent portfolio. In 1997, the company applied for over 750 patents for its innovative processes, ...

#1tW: Alcatel 1997: Report for British MEP: 750 patent applications

#WfW: Report to shareholders.  Says inter alia %(q:Alcatel and Thomson-CSF created a joint research laboratory devoted to software architecture.)  ... %(q:Alcatel considers patent protection to be important to its businesses, particularly)

#eam: Alcatel 1998: Alliance with Thomson-CSF

#loW: Report to shareholders: %(q:In 2000, Alcatel devoted Euro 2.8 billion to its R&D budget and filed for 870 patents.)

#eWi: Alcatel 2000: 870 patent applications

#ehi2: A regular report to shareholders with a section on patent policy and disputes

#lWd2: Alcatel 1999: Policy on Patents and Licenses

#ehi: A regular report to shareholders with a section on patent policy and disputes

#lWd: Alcatel 1999: Policy on Patents and Licenses

#erW: A report by Alcatel which mentions patent troubles encountered by Alcatel: %(bc|Like other companies operating in the telecommunications industry, we experience frequent litigation regarding patent and other intellectual property rights. Third parties have asserted, and in the future may assert, claims against us alleging that we infringe their intellectual property rights. Defending these claims may be expensive and divert the efforts of our management and technical personnel. If we do not succeed in defending these claims, we could be required to expend significant resources to develop non-infringing technology or to obtain licenses to the technology that is the subject of the litigation. In addition, third parties may attempt to appropriate the confidential information and proprietary technologies and processes used in our business, which we may be unable to prevent.|Our business and results of operations will be harmed if we are unable to acquire licenses for third party technologies on reasonable terms.|We remain dependent in part on third party license agreements which enable us to use third party technology to develop or produce our products. However, we cannot be certain that any such licenses will be available to us on commercially reasonably terms, if at all.)

#eui: Alcatel 2002: Frequent Patent Litigation Diverting Efforts of Technical Personnel

#epat: Alcatel Software Patents at the European Patent Office (EPO)

#descr: In 2002, the french telecom giant published a paper which complains in harsh words about negative effects of patents such as legal insecurity and diversion of funds from R&D to litigation.  On the other hand, Alcatel, as a global telecom player, owns a large software patent portfolio, and Alcatel's patent department is actively lobbying politicians to assure that Alcatel's patenting efforts will pay off in Europe in the same way as in the US.  Jonas Heitto, Alcatel patent lawyer trained at MPI in Munich, seems to be present at every related meeting in Brussels and some national capitals.  He is not the only one.  Alcatel participated in the European Commission's %(q:Consultation Exercise) of 2000 in its own name as well as in the name of IT associations such as ANIEL from Spain.  Alcatel has recently been pursuing a strategy %(q:fabless company), i.e. outsource as much production as possible to partners in countries where production is cheap (e.g. China) while keeping a lean headquarter (financial and legal services and some R&D) in France and/or the Bahamas/Bermudas, depending on where taxes are lower.  Patents are said to be helpful in implementing this strategy and in transferring money to tax havens.  In the rationale for her software patentability directive proposal, MEP Arlene McCarthy cites Alcatel as an example and stresses that patents are needed to protect European businesses at a time where production costs are high here.  Here we try to take a closer look at Alcatel's patent-related strategies and in particular their software patent applications at the European Patent Office.  Most of Alcatel's developments are in the area of software, and recently Alcatel is more than ever focussing on %(q:next generation Internet) communication standards and applications.

#title: Alcatel and Software Patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatalcatel ;
# txtlang: en ;
# multlin: t ;
# End: ;

