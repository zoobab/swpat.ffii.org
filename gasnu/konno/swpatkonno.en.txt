<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: A professor of mathematics, specialising in operations research, at Tokyo University.  Wrote a very readable book about his experience with the Karmarkar patent in which he points out the inconsistencies and harmful effects of patenting math and software.  Has tried to resist software patentability in Japan by filing a lawsuit against the Karmarkar patent.  Although, after spending an equivalent of 1 mn usd, he finally won the lawsuit, he did not achieve his aim of making japanese courts rethink their drive to patent software and mathematics against the letter and spirit of the Japanese law.  In recent years, Konno has himself been in the role of struggling to patent many business methods in order to preempt US companies.  This made part of the japanese press erroneously hype him as a promoter of business method patents in Japan.  In 2002, Konno published another japanese book, titled %(q:Patents for just anything?  Where is the patent business heading?)
title: KONNO Hiroshi and Software Patents
Jmr: Japanese account of his writings: mostly mathematics and operations research, some on patent problems.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatkonno ;
# txtlang: en ;
# End: ;

