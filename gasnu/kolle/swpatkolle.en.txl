<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Gert Kolle and Software Patents

#descr: As a young scholar in the 70s, Gert Kolle quickly became the leading german theoretician on the limits of patentability with respect to computer programs.  Kolle wrote his doctoral thesis at the Max-Planck Institute and Patent Law on this subject and published several deep-searching articles in GRUR from the early 70s to the early 80s.  These articles confirmed and refined the view of the courts that there is no room for patenting computer programs if the notion of technical invention is taken seriously.  Kolle's works wor often cited by German courts as a foundation for their refusal to grant software patents.  Later Kolle became an official at the European Patent Office.  Currently he is their head of diplomacy, and he occasionally gives talks where he explains and justifies the current software patenting policy of the EPO.

#GWt: On 2001-01-17 Kolle declared at a conference about %(q:Protection of Software) at INRIA (institute for research in applied informatics) in Paris that there were no longer any significant differences between the practise of the EPO and that of the USPTO concerning computer programs.  The EPO, Kolle explained, views practially all programs/algorithms as %(q:computer-implemented inventions), which are not considered to be programs/algorithms as such.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatkolle ;
# txtlang: en ;
# multlin: t ;
# End: ;

