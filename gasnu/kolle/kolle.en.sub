\begin{subdocument}{swpatkolle}{Gert Kolle and Software Patents}{http://swpat.ffii.org/players/kolle/index.en.html}{Workgroup\\swpatag@ffii.org}{As a young scholar in the 70s, Gert Kolle quickly became the leading german theoretician on the limits of patentability with respect to computer programs.  Kolle wrote his doctoral thesis at the Max-Planck Institute and Patent Law on this subject and published several deep-searching articles in GRUR from the early 70s to the early 80s.  These articles confirmed and refined the view of the courts that there is no room for patenting computer programs if the notion of technical invention is taken seriously.  Kolle's works wor often cited by German courts as a foundation for their refusal to grant software patents.  Later Kolle became an official at the European Patent Office.  Currently he is their head of diplomacy, and he occasionally gives talks where he explains and justifies the current software patenting policy of the EPO.}
On 2001-01-17 Kolle declared at a conference about ``Protection of Software'' at INRIA (institute for research in applied informatics) in Paris that there were no longer any significant differences between the practise of the EPO and that of the USPTO concerning computer programs.  The EPO, Kolle explained, views practially all programs/algorithms as ``computer-implemented inventions'', which are not considered to be programs/algorithms as such.

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs\footnote{http://swpat.ffii.org/papers/grur-kolle77/index.de.html}}}

\begin{quote}
Gert Kolle, heute im Europ\"{a}ischen Patentamt f\"{u}r Internationales Patentrecht zust\"{a}ndig, war bis Mitte der 80er Jahre der meistzitierte Rechtstheoretiker in Fragen der Technizit\"{a}t und der Patentierbarkeit von Computerprogrammen.  Er agierte als wissenschaftlicher Referent und Berichterstatter der deutschen Delegation bei verschiedenen Patentgesetzgebungskonferenzen der 70er Jahre, bem\"{u}hte sich stets um einen unparteiischen wissenschaftlichen Standpunkt fernab jeglicher ``ideologischer Versteinerung'', in der die beiden Fronten schon damals aufeinanderprallten.  Im vorliegenden GRUR-Artikel von 1977 erkl\"{a}rt er, warum Computerprogramme nicht als ``technisch'' im Sinne des Patentrechts gelten k\"{o}nnen und warum eine ``naiv oder bewusst'' herbeigef\"{u}hrte ``Lockerung des Technikbegriffs'' zu unverantwortbaren Sperrwirkungen f\"{u}hren w\"{u}rde.  Es m\"{u}sse daher ein ``Niemandsland des Geistigen Eigentums'' geben, und Algorithmen sollten ``vergesellschaftet'' werden.  Ein wegen seiner Tiefe und Klarheit sehr empfehlenswerter Artikel, der nach \"{u}ber 20 Jahren kaum etwas von seiner Aktualit\"{a}t verloren hat.
\end{quote}
\filbreak

\item
{\bf {\bf Gert Kolle: Der Rechtsschutz von Computerprogrammen aus nationaler und internationaler Sicht\footnote{http://swpat.ffii.org/papers/grur-kolle73/index.de.html}}}

\begin{quote}
Dieser zweiteilige Artikel konstatiert eine ideologische Versteinerung in der Frage der Patentierbarkeit von Computerprogrammen und erl\"{a}utert die unterschiedlichen Positionen der verschiedenen L\"{a}nder.  Kontinentaleuropa und Australien lehnen die Patentierbarkeit ab, w\"{a}hrend England und die USA ihr zuneigen.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
% mode: latex ;
% End: ;

