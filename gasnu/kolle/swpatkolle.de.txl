<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Gert Kolle und Softwarepatente

#descr: Als junger Assessor in den 70er Jahren wurde Gert Kolle schnell zum führenden deutschen Theoretiker der Grenzen der Patentierbarkeit im Hinblick auf Datenverarbeitungsprogramme.  Kolle schrieb seine Doktorarbeit am Münchener MPI für Patentrecht über dieses Thema und veröffentlichte eine Reihe grundlegender Artikel in GRUR zwischen 1973 und 1982.  Diese Artikel bestätigten und verfeinerten die damalige BGH-Rechtsprechung, der zufolge Rechenprogramme dem Patentschutz nicht zugänglich sein können, sofern man den Begriff der Technischen Erfindung ernst nimmt und überhaupt noch irgendwelche Grenzen zu setzen bereit ist.  Kolles Werke wurden wiederum oft von Gerichten als Grundlage ihrer strengen Rechtsprechung zitiert.  Anfang der 80er Jahre wurde Kolle ein Beamter des Europäischen Patentamtes.  Dort schätzte man seinen Standpunkt weniger, und sein Interesse wechselte zu anderen Themen.  Inzwischen ist Kolle Leiter der Abteilung für Internationale Angelegenheiten, m.a.W. Chefdiplomat des EPA.  In dieser Rolle hält er gelegentlich Vorträge, in denen er die gegenwärtige Praxis des EPA auch hinsichtlich Softwarepatenten rechtfertigt.

#GWt: Kolle erklärte am 2002-01-17 auf einer Konferenz über den %(q:Schutz von Computerprogrammen) bei INRIA (Institut für Angwandte Informatik) in Paris, dass es keine nennenswerten Unterschiede zwischen der Praxis des EPA und derjenigen des US-Patentamtes mehr gebe, da das EPA heute praktisch alle Computerprogramme als %(q:computer-implementierte Erfindungen) ansehe, die wiederum als solche nicht von der Patentierbarkeit ausgeschlossen seien.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatkolle ;
# txtlang: de ;
# multlin: t ;
# End: ;

