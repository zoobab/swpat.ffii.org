\begin{subdocument}{swpatbmwa}{Bundesministerium f\"{u}r Wirtschaft und Arbeit (BMWA, BMWi) und Softwarepatente}{http://swpat.ffii.org/akteure/bmwa/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2004/09/07 von PILCH Hartmut\footnote{\url{http://www.ffii.org/\~phm}}}{Die Arbeit der Berliner Bundesregierung zu Fragen der Patentgesetzgebung wird vom Justizministerium geleiget.  Das Wirtschaftsministerium unterh\"{a}lt jedoch ein eigenes Patentreferat, das sich gelegentlich einmischt.  Ferner zeigen die mit Fragen der Kommunikations-Infrastruktur und Netzwerksicherheit befassten Referate gelegentlich Interesse an dem Thema.  Patentbeamte wie Nils Baggehufwudt und Vorgesetzte wie Alfred Tacke, Werner M\"{u}ller u.a. haben ihre Politik in der Vergangenheit an den Interessen der Patent-Institutionen ausgerichtet oder sich zumindest bem\"{u}ht, deren Interessen nicht in Frage zu stellen.  Zugleich gab das BMWA jedoch zur Unterstreichung der eigenen Kompetenz in der Frage mehrmals Studien in Auftrag, deren Erkenntnisse Zweifel an diesen Interessen unvermeidlich machten.}
\begin{sect}{news}{Neues und Chronik}
\begin{center}
\begin{tabular}{|C{44}|C{44}|}
\hline
2004/08/25 & Staatssekret\"{a}r entschuldigt sich bei Bitkom f\"{u}r BMWA-Umfrage\footnote{\url{http://swpat.ffii.org/log/04/tacke0825/index.de.html}}\\\hline
2004/07/15 & BMWA befragt Unternehmen zu Softwarepatenten\footnote{\url{http://swpat.ffii.org/log/04/bmwa0715/index.de.html}}\\\hline
2004/07/10 & Patente gro{\ss}es Thema bei BMWA-Gepr\"{a}chen zu Freier Software auf R\"{u}gen\footnote{\url{http://swpat.ffii.org/log/04/ruegen0710/index.de.html}}\\\hline
2004/06/16 & Weber-Cludius rechtfertigt BMJ-Position im Rat\footnote{\url{http://swpat.ffii.org/log/04/bmwa06/index.de.html}}\\\hline
2004/04 & 2004-04: BMWA-Auftragsstudie fordert Erm\"{o}glichung von Softwarepatenten\footnote{\url{http://swpat.ffii.org/papiere/bmwa0404/index.de.html}}\\\hline
2002/12/09 & Baggehufwudt-Schreiben\footnote{\url{http://swpat.ffii.org/analyse/trips/index.de.html}}\\\hline
2002/09 & Zusammenlegung von BMWi mit BMA zu BMWA, neuer Superminister Clement\\\hline
2002/04/00 & BMJ setzt im EU-Rat Programmanspr\"{u}che durch, BMWi schweigt\footnote{\url{http://swpat.ffii.org/papiere/eubsa-swpat0202/dkpto0209/index.de.html}}\\\hline
2004/08/24 & Monopolkommission warnt vor Softwarepatenten, kritisiert gesetzeswidriges Richterrecht\footnote{\url{http://swpat.ffii.org/papiere/eubsa-swpat0202/mopoko0207/index.de.html}}\\\hline
2004/08/24 & Eur. Kommission ver\"{o}ffentlicht Richtlinien-Vorschlag\footnote{\url{http://swpat.ffii.org/papiere/eubsa-swpat0202/index.de.html}}\\\hline
2001 & In Beratungen der nationalen Ministerien mit der Kommission f\"{u}hrt Staatssekret\"{a}r Gerlach vom BMWA gelegentlich die deutsche Delegation an, schw\"{a}nzt Sitzungen, l\"{a}sst Deutschland von den Patentbeamten der Kommission f\"{u}r deren Ansinnen grenzenloser Patentierbarkeit in Abwesenheit vereinnahmen.\\\hline
2004/08/24 & In Konsultation der Europ\"{a}ischen Kommission \"{a}u{\ss}ert BMWA sich unverbindlich, erw\"{a}hnt Auftragsstudien.\footnote{\url{http://swpat.ffii.org/papiere/eukonsult00/index.de.html}}\\\hline
2004/08/24 & Auftragsstudie an Fraunhofer/MPI bescheinigt Sch\"{a}dlichkeit von Softwarepatenten, empfiehlt ihre Legalisierung\footnote{\url{http://swpat.ffii.org/papiere/bmwi-fhgmpi01/index.de.html}}\\\hline
2004/08/24 & Kurzgutachten Lutterbeck, Horns, Gering: bescheinigt Sch\"{a}dlichkeit von Softwarepatenten, empfiehlt ihre Legalisierung\footnote{\url{http://swpat.ffii.org/papiere/bmwi-luhoge00/index.de.html}}\\\hline
0518 & Anh\"{o}rung zu Swpat im BMWi: Konsens der KMU gegen Softwarepatente\footnote{\url{http://swpat.ffii.org/termine/2000/bmwi0518/index.de.html}}\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{intro}{Einf\"{u}hrung}
\begin{itemize}
\item
{\bf {\bf \url{http://www.bmwa.bund.de/}}}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{patref}{Patentreferat}
\begin{itemize}
\item
Leitung von Nils Baggehufwudt\footnote{\url{}}

\item
Federf\"{u}hrung von Swantje Weber-Cludius\footnote{\url{}}

\item
wirtschaftswissenschaftliches Denken schafft eine gewisse Distanz zur Patentbewegung

\item
enge Verflechtung mit der Patentbewegung verhindert Mut zu klarer Kritik

\item
Studien bieten einerseits M\"{o}glichkeit zur Profilierung der Wirtschaftswissenschaftler innerhalb der Patentbewegung, andererseits Risiko des Familienzwists, daher nur enger Spielraum f\"{u}r die Auftragnehmer
\end{itemize}
\end{sect}

\begin{sect}{etc}{Sonstige Abteilungen}
\begin{itemize}
\item
Bereiche f\"{u}r Netzwerksicherheit, KMU etc sehen Softwarepatente mit mehr Distanz/Unbehagen.

\item
Bereich ``Sicherheit im Internet'' ist ins BMI \"{u}bergewechselt. Hubertus Soquat, der das Kurzgutachten an Lutterbeck \& Co in Auftrag geben lie{\ss} und eine Webseite zum Thema unterhielt, ging 2003.

\item
Monopolkommission\footnote{\url{}} (unabh\"{a}ngig und doch mit BMWA personell verbunden) hat sich sehr klar gegen Softwarepatente ge\"{a}u{\ss}ert\footnote{\url{http://swpat.ffii.org/papiere/eubsa-swpat0202/mopoko0207/index.de.html}}
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatbmwa.el ;
% mode: latex ;
% End: ;

