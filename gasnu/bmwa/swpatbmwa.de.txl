<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Bundesministerium für Wirtschaft und Arbeit (BMWA, BMWi) und
Softwarepatente

#descr: Die Arbeit der Berliner Bundesregierung zu Fragen der
Patentgesetzgebung wird vom Justizministerium geleiget.  Das
Wirtschaftsministerium unterhält jedoch ein eigenes Patentreferat, das
sich gelegentlich einmischt.  Ferner zeigen die mit Fragen der
Kommunikations-Infrastruktur und Netzwerksicherheit befassten Referate
gelegentlich Interesse an dem Thema.  Patentbeamte wie Nils
Baggehufwudt und Vorgesetzte wie Alfred Tacke, Werner Müller u.a.
haben ihre Politik in der Vergangenheit an den Interessen der
Patent-Institutionen ausgerichtet oder sich zumindest bemüht, deren
Interessen nicht in Frage zu stellen.  Zugleich gab das BMWA jedoch
zur Unterstreichung der eigenen Kompetenz in der Frage mehrmals
Studien in Auftrag, deren Erkenntnisse Zweifel an diesen Interessen
unvermeidlich machten.

#unr: Neues und Chronik

#ate: Patentreferat

#nWl: Sonstige Abteilungen

#eWi: Zusammenlegung von BMWi mit BMA zu BMWA, neuer Superminister Clement

#Dkpto0209Ent: BMJ setzt im EU-Rat Programmansprüche durch, BMWi schweigt

#Mopoko0207T: Monopolkommission warnt vor Softwarepatenten, kritisiert
gesetzeswidriges Richterrecht

#mtn: Eur. Kommission veröffentlicht Richtlinien-Vorschlag

#mWe: In Beratungen der nationalen Ministerien mit der Kommission führt
Staatssekretär Gerlach vom BMWA gelegentlich die deutsche Delegation
an, schwänzt Sitzungen, lässt Deutschland von den Patentbeamten der
Kommission für deren Ansinnen grenzenloser Patentierbarkeit in
Abwesenheit vereinnahmen.

#neh: In Konsultation der Europäischen Kommission äußert BMWA sich
unverbindlich, erwähnt Auftragsstudien.

#BmwiFhgmpi01T: Auftragsstudie an Fraunhofer/MPI bescheinigt Schädlichkeit von
Softwarepatenten, empfiehlt ihre Legalisierung

#BmwiLuhoge00Det: Kurzgutachten Lutterbeck, Horns, Gering: bescheinigt Schädlichkeit von
Softwarepatenten, empfiehlt ihre Legalisierung

#zsf: Anhörung zu Swpat im BMWi: Konsens der KMU gegen Softwarepatente

#bmwa040715t: BMWA befragt Unternehmen zu Softwarepatenten

#bmwa040715d: Das Bundesministerium für Wirtschaft und Arbeit beginnt eine Umfrage,
die später für viel Wirbel sorgt.

#ruegen040710t: Patente großes Thema bei BMWA-Geprächen zu Freier Software auf Rügen

#ruegen040710d: Bei einem Treffen von Beamten des BMWA, Softwareunternehme und
Wirtscahftswssenschaftlern auf der Insel Rügen kommen Sorgen über
Softwarepatente zur Sprache.  Wenig später startet das BMWA eine
Umfrage zum Thema.

#Bmwa0406T: Weber-Cludius rechtfertigt BMJ-Position im Rat

#Bmwa0406D: Swantje Weber-Cludius antwortet einem Briefschreiber, rechtfertigt
darin die vom BMJ im EU-Rat eingenommene Position

#Bmwa021209T: Baggehufwudt-Schreiben

#Bmwa021209D: für Linie von Kommission und Rat, d.h. grenzenlose Patentierbarkeit,
verwirrt und beschwichtigt mit patentjuristischen Wortspielen,
verwendet TRIPs-Lüge.

#iWc: Leitung von %(NB)

#enc: Federführung von %(WC)

#iar: wirtschaftswissenschaftliches Denken schafft eine gewisse Distanz zur
Patentbewegung

#ewW: enge Verflechtung mit der Patentbewegung verhindert Mut zu klarer
Kritik

#ldu: Studien bieten einerseits Möglichkeit zur Profilierung der
Wirtschaftswissenschaftler innerhalb der Patentbewegung, andererseits
Risiko des Familienzwists, daher nur enger Spielraum für die
Auftragnehmer

#Wsr: Bereiche für Netzwerksicherheit, KMU etc sehen Softwarepatente mit
mehr Distanz/Unbehagen.

#ttz: Bereich %(q:Sicherheit im Internet) ist ins BMI übergewechselt.
Hubertus Soquat, der das Kurzgutachten an Lutterbeck & Co in Auftrag
geben ließ und eine Webseite zum Thema unterhielt, ging 2003.

#slw: %(mk:Monopolkommission) (unabhängig und doch mit BMWA personell
verbunden) hat sich sehr klar gegen Softwarepatente %(m2:geäußert)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/mlht/app/swpat/swpatbmwa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbmwa ;
# txtlang: de ;
# multlin: t ;
# End: ;

