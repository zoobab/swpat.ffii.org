\begin{subdocument}{swpatdepak}{Deutsche Patentanwaltskammer}{http://swpat.ffii.org/gasnu/depak/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2004/12/03 von FFII (\url{http://lists.ffii.org/mailman/listinfo/traduk})}{Verband des Berufsstandes der deutschen Patentanw\"{a}lte.  Gibt die Zeitschrift ``Mitteilungen der Deutschen Patentanw\"{a}lte'' (MDP) heraus und hat einen Softwarepatente-Arbeitskreis unter Leitung des auf Software spezialisierten Patentanwaltes Markus H\"{o}ssle.  Sowohl in der Verbandszeitschrift als auch in diversen Resolutionen hat sich die Patentanwaltskammer f\"{u}r eine grenzenlose Ausweitung der Patentierbarkeit und insbesondere f\"{u}r die Streichung des Ausschlusses von Computerprogrammen im EP\"{U}/PatG eingesetzt.  Dabei wurde bisweilen auch behauptet, die Gegner der Patentierbarkeit k\"{a}men alle aus dem Lager der Opensource-Software, die nicht innovativ und volkswirtschaftlich unbedeutend sei.  Allerdings vertritt der Chefredakteur von MDP, PA Reimar K\"{o}nig, einen weitaus kundigeren Standpunkt und l\"{a}sst auch ketzerische Meinungen zu Wort kommen.}
\begin{itemize}
\item
{\bf {\bf \url{http://www.patentanwalt.de}}}
\filbreak

\item
{\bf {\bf König 2001: Patentfähige Datenverarbeitungsprogramme - ein Widerspruch in sich\footnote{\url{http://localhost/swpat/papri/grur-koenig01/index.de.html}}}}

\begin{quote}
Der D\"{u}sseldorfer Patentanwalt Dr. K\"{o}nig zeigt allerlei Ungereimtheiten in der Softwarepatent-Rechtsprechung des BGH und EPA auf, kritisiert ``Zirkelschl\"{u}sse'' und argumentiert, das EPA habe Art 52 EP\"{U} ``Gewalt angetan''.  Durch eine ``grammatische Auslegung des Begriffs ``Datenverarbeitungsprogramme als solche'''' gelangt er zu der Erkenntnis, dass damit nur alle Datenverarbeitungsprogramme ohne Ausnahme gemeint sein k\"{o}nnen, allerdings nur insoweit sie alleine Gegenstand des Patentbegehrens seien.  Seit der Umsetzung des EP\"{U} in die PatG-Novelle von 1978 gibt es keine Grundlage mehr f\"{u}r eine Unterscheidung zwischen technischen und untechnischen Programmen.  Beim Verst\"{a}ndnis des Begriffes ``DV-Programm'' habe sich die Rechtsprechung nach allgemeinen au{\ss}erjuristischen Definitionen zu richten, wie sie etwa von DIN-Fachleuten geleistet wurden.  Danach ist ein Programm die Gesamtheit aus Sprachdefinitionen und Text und nicht etwa nur der urheberrechtlich sch\"{u}tzbare Text.  Das EP\"{U}/PatG erlaube es aber, ``Kombinationserfindungen'' zu patentieren, die als ganzes auf Technizit\"{a}t, Neuheit, Nichtnaheliegen und gewerbliche Anwendbarkeit zu pr\"{u}fen seien.  Gerichte h\"{a}tten schon immer Kreativit\"{a}t entfaltet, wenn es darum ging, ``sich \"{u}ber Patentierbarkeitsausschl\"{u}sse hinwegzuhelfen''.  Datenverarbeitungsprogrammen (als solchen) k\"{o}nne auf dem Wege \"{u}ber ein Kombinationspatent mittelbar der volle Schutz eines Sachpatents zukommen.  Datenverarbeitungsprogramme seien ebenso wie etwa naturwissenschaftliche Entdeckungen (als solche) dem Verwendungsschutz zug\"{a}nglich.
\end{quote}
\filbreak

\item
{\bf {\bf EUK/BSA Richtlinienvorschlag Logikpatente: Anmerkung von Roman Sedlmaier 2002/03\footnote{\url{}}}}

\begin{quote}
ein in MDP erschienener Artikel, der sich kritisch mit dem EU-Richtlinienvorschlag auseinandersetzt, auch wenn er, \"{a}hnlich wie viele Artikel in MDP und \"{a}hnlichen Zeitschriften, mit juristischer (Schein)Logik politische Entscheidungen vorweg zu nehmen bestrebt ist.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

