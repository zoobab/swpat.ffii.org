<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Deutsche Patentanwaltskammer

#descr: Verband des Berufsstandes der deutschen Patentanwälte.  Gibt die
Zeitschrift %(q:Mitteilungen der Deutschen Patentanwälte) (MDP) heraus
und hat einen Softwarepatente-Arbeitskreis unter Leitung des auf
Software spezialisierten Patentanwaltes Markus Hössle.  Sowohl in der
Verbandszeitschrift als auch in diversen Resolutionen hat sich die
Patentanwaltskammer für eine grenzenlose Ausweitung der
Patentierbarkeit und insbesondere für die Streichung des Ausschlusses
von Computerprogrammen im EPÜ/PatG eingesetzt.  Dabei wurde bisweilen
auch behauptet, die Gegner der Patentierbarkeit kämen alle aus dem
Lager der Opensource-Software, die nicht innovativ und
volkswirtschaftlich unbedeutend sei.  Allerdings vertritt der
Chefredakteur von MDP, PA Reimar König, einen weitaus kundigeren
Standpunkt und lässt auch ketzerische Meinungen zu Wort kommen.

#eau: ein in MDP erschienener Artikel, der sich kritisch mit dem
EU-Richtlinienvorschlag auseinandersetzt, auch wenn er, ähnlich wie
viele Artikel in MDP und ähnlichen Zeitschriften, mit juristischer
(Schein)Logik politische Entscheidungen vorweg zu nehmen bestrebt ist.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatdepak ;
# txtlang: de ;
# multlin: t ;
# End: ;

