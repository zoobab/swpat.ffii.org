\begin{subdocument}{swpattauchert}{Wolfgang Tauchert}{http://swpat.ffii.org/akteure/tauchert/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Leiter der Swpat-Abteilung im Deutschen Patent- und Markenamt (DPMA).  Der Patent-Durchsatz seiner Abteilung wuchs in wenigen Jahren von nahe Null auf einige Tausend, als das DPMA begann, mit Billigung des BGH dem EPA in der Erteilung von Softwarepatenten nachzueifern.  Tauchert feuerte diesen Trend mit allerlei Artikeln an, in denen er auch einige pers\"{o}nliche Akzente setzt.  Laut Tauchert ist nur im Quelltext eines Programms das Programm als solches zu sehen.  Programmbezogene Bin\"{a}rdateien, Algorithmen, Funktionalit\"{a}ten usw sind hingegen keine Programme als solche.  Diese f\"{u}r Informatiker schwer nachvollziehbare Sichtweise erfreut sich unter Patentanmeldern und Patentjuristen gro{\ss}er Beachtung, zumal Tauchert sie mit beeindruckender Quellengelehrsamkeit vortr\"{a}gt.}
\begin{center}
\begin{tabular}{|C{44}|C{44}|}
\hline
\mbox{\includegraphics{tauchert.jpg}} & Leiter der Abteilung fuer Datenverarbeitung und Informationsspeicherung am Deutschen Patent- und Markenamt

Tauchert hat eine klare Meinung zum Thema Swpat: ``Gerade Startups brauchen den Schutz von Patenten'', sagt er. ``Die Gegner der Patente verfolgen eigene Interessen, n\"{a}mlich den unbeschr\"{a}nkten Zugriff auf das geistige Eigentum anderer.'' ``Softwarepatente bed\"{u}rfen keiner weiteren \"{o}konomischen Begr\"{u}ndung.  Der Markt hat bereits entschieden.  Mehr als 1000 Firmen aller Art beantragen bei uns jedes Jahr Softwarepatente, und das DPMA tr\"{a}gt sich finanziell selbst.'' ``Softwarepatente zeigen gerade dann ihren \"{o}konomischn Sinn, wenn sie weh tun.''\\\hline
\end{tabular}
\end{center}

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf FTD - 101 K\"{o}pfe: Wolfgang Tauchert\footnote{http://www.ftd.de/pw/de/1074024.html}}}
\filbreak

\item
{\bf {\bf DPMA 2002-03-16: Information ist eine Naturkraft, Gesch\"{a}ftsverfahren Patentierbar, Programmtexte keine Offenbarung\footnote{http://lists.essential.org/pipermail/random-bits/2002-March/000791.html}}}

\begin{quote}
Greg Aharonian (GA) ver\"{o}ffentlicht in seinem Rundschreiben einen Artikel \"{u}ber den Begriff der Technischen Erfindung, der, so GA, den offiziellen Standpunkt des Deutschen Patent- und Markenamtes (DPMA) darstellt.  In einem triumphierenden Einleitungskommentar findet GA darin seine eigene Meinung wieder, wonach alle Gesch\"{a}ftsverfahren technisch sind und patentierbar sein m\"{u}ssen.  Der DPMA-Artikel tr\"{a}gt die Handschrift des DPMA-Software-Chefideologen Wolfgang Tauchert.  Es ist aber bislang unklar, in wieweit es sich hierbei um einen offiziellen Amtsstandpunkt handelt.  Auff\"{a}llig ist aber, dass Tauchert zeitgleich mit der Ver\"{o}ffentlichung dieses Artikels in einer DPMA-Presseerkl\"{a}rung als Autorit\"{a}t zitiert wird und eine neue Runde der Patentinflation einl\"{a}uten darf.  Das Positionspapier beunruhigt auch dadurch, dass es Programm-Quelltexte als f\"{u}r eine Offenbarung einer Lehre ungeeignet abqualifiziert.  Hieraus scheint zu folgen, dass ein Patentgoldgr\"{a}ber nur quelloffene Programme anderer Leute abgrasen und in Patentsprech umformulieren muss, um darauf selber Patente zu erhalten und die eigentlichen Autoren zu blockieren.
\end{quote}
\filbreak

\item
{\bf {\bf Tauchert 1999: Zur Patentierbarkeit von Programmen f\"{u}r Datenverarbeitungsanlagen\footnote{http://swpat.ffii.org/papiere/mdp-tauch99/index.de.html}}}

\begin{quote}
Taucherts erkl\"{a}rt einem Publikum von Patentanw\"{a}lten seine Lehre \"{u}ber das Wesen des Programms als solchen.  Hierunter ist laut Tauchert der Quelltext zu verstehen.  Der Gesetzgeber von 1973 wollte lediglich Anmelder davon abhalten, ihre Anspr\"{u}che auf einen bestimmten urheberrechtlich gesch\"{u}tzten Text einzuschr\"{a}nken.  Ferner interpretiert Tauchert die BGH-Lehre \"{u}ber die Naturkr\"{a}ftegebundenheit von Erfindungen um: Technisch ist alles, was mithilfe physischer Mittel (wie z.B. eines Prozessors) in kausal \"{u}bersehbarer Weise beherrschbar wird.  Dies kann auch Verfahren f\"{u}r gesch\"{a}ftliche T\"{a}tigkeit mit einschlie{\ss}en.  Sofern das Verfahren erst mithilfe eines Rechners m\"{o}glich wird, handelt es sich nicht um eine Gesch\"{a}ftsmethode als solche.  F\"{u}r die fr\"{u}here BGH-Methodik, aus dem Anspruch eine Lehre herauszusch\"{a}len, welche zugleich neu und technisch sein muss, gibt es laut Tauchert heute keine rechtliche Grundlage mehr.
\end{quote}
\filbreak

\item
{\bf {\bf Zur Patentierbarkeit von Programmen f\"{u}r DV-Anlagen : Anmerkungen zum Aufsatz von Mellulis\footnote{http://swpat.ffii.org/papiere/grur-tauch99/index.de.html}}}

\begin{quote}
In einem vieldiskutierten Artikel von 1998 hatte BGH-Richter Melullis erkl\"{a}rt, der Ausschluss von Programmen f\"{u}r Datenverarbeitungsanlagen von der Patentierbarkeit sei dahingehend zu verstehen, dass breite Anspr\"{u}che auf abstrakte Konzepte zu vermeiden seien.  Hingegen k\"{o}nne es m\"{o}glich sein, hinreichend spezifisch abgefasste Lehren zum Gebrauch von Datenverarbeitungsanlagen zu patentieren.  Dem widerspricht Tauchert, der am Deutschen Patent- und Markenamt die f\"{u}r Informatikpatente zust\"{a}ndige Abteilung leitet.  Tauchert meint umgekehrt, der Gesetzgeber habe nur besonders enge Anspr\"{u}che wie etwa die auf Programmtexte ausschlie{\ss}en aber die Gew\"{a}hrung breiter Anspr\"{u}che auf ``technische'' Ideen aller Art ermutigen wollen.  Tauchert erl\"{a}utert in diesem GRUR-Artikel seine Sichtweise und sendet damit ein Signal an die Software-Unternehmen.
\end{quote}
\filbreak

\item
{\bf {\bf Tauchert-Artikel in JurPC 2001\footnote{http://www.jurpc.de/aufsatz/20010040.htm}}}
\filbreak

\item
{\bf {\bf Tauchert-Artikel in JurPC 2002\footnote{http://www.jurpc.de/aufsatz/20020028.htm}}}

\begin{quote}
meint, bei Art 52 EP\"{U} gehe es nicht um technische Lehren sondern um Anspruchsgegenst\"{a}nde, da das alte deutsche PatG, in dem ausf\"{u}hrlich die Doktrin vom ``allgemeinen Erfindungsgedanken'' dargelegt war, seit 1978 nicht mehr gilt.  Folglich habe sich der BGH fr\"{u}her geirrt und sei erst Anfang der 90er Jahre auf den richtigen Kurs gekommen.
\end{quote}
\filbreak

\item
{\bf {\bf phm: Tauchert zum Erfindungsbegriff\footnote{http://lists.ffii.org/archive/mails/swpat/2002/Feb/0002.html}}}

\begin{quote}
phm 2002-02: Kritik am neuen Tauchert-Artikel
\end{quote}
\filbreak

\item
{\bf {\bf Dr. Swen Kiesewetter-K\"{o}binger: Stellungnahme zur Patentierbarkeit von Softwarekonzepten\footnote{http://swpat.ffii.org/termine/2001/bundestag/kiesew/index.de.html}}}

\begin{quote}
Die vorgesehene Einf\"{u}hrung der Patentierbarkeit von Softwarekonzepten l\"{a}{\ss}t unter anderem Zweifel an der Verfassungsgsm\"{a}{\ss}igkeit der daraus resultierenden Folgen aufkommen. Gro{\ss}e Teile der Antworten zu dem gestellten Fragenkatalog sind von dieser Sorge gepr\"{a}gt. Da einige dieser \"{U}berlegungen noch ziemlich neu sind und bisherigen, zu oberfl\"{a}chlichen Betrachtungen widersprechen, hat deren Ausf\"{u}hrung einen breiten Raum eingenommen. Hoffentlich gelang es trotzdem, die Problematik der Patentierung von Software mit typischem Werkcharakter bei gleichzeitigem Schutz durch das Urheberrecht allgemeinverst\"{a}ndlich darzustellen.
\end{quote}
\filbreak

\item
{\bf {\bf DPMA 2002-03-12: Patentrekord - Positives Signal\footnote{http://www.dpma.de/infos/pressedienst/pm020312a.html}}}

\begin{quote}
Wachstum der Anmeldezahlen am Deutschen Patent- und Markenamt (DPMA) um 15\percent{}, Einbruch am neuen Markt, nat\"{u}rlich alles ein Spiegel der lebendigen Innovation und nicht etwa der Patentinflation.  Nebenbei wird Swpat-Chefideologe Tauchert mit einer Aussage zur neuen Politik des DPMA hinsichtlich Software und Gesch\"{a}ftsmethoden zitiert.  Tauchert k\"{u}ndigt an, dass das Patentamt die neueste BGH-Entscheidung (Zeichensuche 2001) umsetzen und somit unmittelbare Anspr\"{u}che auf Programme zulassen werde.  Damit prescht das DPMA in einem Punkt vor, in dem sogar der neue Br\"{u}sseler Richtlinienvorschlag Zur\"{u}ckhaltung empfiehlt.
\end{quote}
\filbreak

\item
{\bf {\bf Tauchert 2000/11: Vortrag bei B\"{o}ll-Stiftung\footnote{http://www.boell.de/downloads/medien/vortragtauchert.pdf}}}

\begin{quote}
Tauchert setzt sich in Berlin f\"{u}r die geplante und sp\"{a}ter zur\"{u}ckgenommene Legalisierung von Softwarepatenten ein, meint diese folge aus dem Auftrag des Grundgesetzes zum Schutz des Privateigentums, Verzicht darauf sei ehrenvoll, k\"{o}nne aber niemandem ``aufgezwungen'' werden, wie die ``Opensource-Bewegung'' dies versuche.
\end{quote}
\filbreak

\item
{\bf {\bf FFII-Programm auf dem LinuxTag 2000\footnote{http://swpat.ffii.org/termine/2000/linuxtag/index.de.html}}}

\begin{quote}
Tauchert war einer der Redner
\end{quote}
\filbreak

\item
{\bf {\bf Tauchert 2000-08: CSU-Offensive\footnote{http://lists.ffii.org/archive/mails/swpat/2000/Aug/0126.html}}}

\begin{quote}
Tauchert nimmt auf FFII-Verteiler zu Fragen Stellung, meint die Entwickler freier Software m\"{u}ssten eine eigene Verwertungsorganisation gr\"{u}nden, um ihr ``Gesch\"{a}ftsmodell'' zum Patentsystem kompatibel zu machen.
\end{quote}
\filbreak

\item
{\bf {\bf Tauchert 2000-07-24: ius sui generis\footnote{http://lists.ffii.org/archive/mails/swpat/2000/Jul/0132.html}}}

\begin{quote}
Tauchert nimmt auf FFII-Verteiler zu Fragen Stellung, meint die Entwickler freier Software m\"{u}ssten eine eigene Verwertungsorganisation gr\"{u}nden, um ihr ``Gesch\"{a}ftsmodell'' zum Patentsystem kompatibel zu machen.
\end{quote}
\filbreak

\item
{\bf {\bf Tauchert 2000-05-18: Positionspapier Softwarepatente\footnote{http://www.sicherheit-im-internet.de/themes/themes.phtml?ttid=2\&tdid=86}}}

\begin{quote}
Positionspapier von Tauchert bei der BMWi-Anh\"{o}rung\footnote{http://swpat.ffii.org/termine/2000/bmwi0518/index.de.html}
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
% mode: latex ;
% End: ;

