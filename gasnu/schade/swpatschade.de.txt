<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Geboren am 3. Dezember 1942 in Berlin, verheiratet, zwei Söhne. 1963 Abitur in München, Studium der Theologie und Rechtswissenschaften von 1963 bis 1973 in München.  1973 bis 1977 Wissenschaftlicher Mitarbeiter des %(mp:Max-Planck-Instituts für ausländisches und internationales Patent-, Urheber- und Wettbewerbsrecht).  1978 Promotion zum Dr. jur. 1977 bis 1986 Mitglied des Deutschen Patent- und Markenamts. Zunächst Erinnerungsprüfer in der Markenabteilung, dann in der Rechtsabteilung tätig; zuletzt als Referatsleiter für internationales Patentrecht. Zwischen 1981-1982 abgeordnet zu der Weltorganisation für geistiges Eigentum (WIPO) in Genf.  1986 Wechsel als Richter zum %(bp:Bundespatentgericht). Dort bis 1994 zunächst Mitglied eines Marken-Beschwerdesenats, dann Mitglied eines Technischen Beschwerdesenats, schließlich Mitglied des Rechtssenats.  1994 bis 1998 Mitglied des Bayerischen Landtags, lange als SPD-Politiker auf Starnberger Kreistagsebene tätig.  Äußerte gelegentlich Bedenken gegen europäische Zentralisierung des Patentrechtsprechung.  1999 Wiederaufnahme der Tätigkeit beim Bundespatentgericht als Richter, zuletzt als Vorsitzender Richter. Seit 2001-08-01 Präsident des %(dp:Deutschen Patent- und Markenamts).
title: Jürgen Schade, Präsident des DPMA seit 2001

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatschade ;
# txtlang: de ;
# End: ;

