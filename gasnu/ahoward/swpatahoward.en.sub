\begin{subdocument}{swpatahoward}{Anthony Howard}{http://swpat.ffii.org/players/ahoward/index.en.html}{Workgroup\texmath{\backslash}\texmath{\backslash}swpatag@ffii.org}{Chief drafter of the European Community software patentability directive proposal, born 194x, previously an examiner and administration official at the UKPO, involved in TRIPS negotiation for the UKPO, came to the European Commission on secondment from the UKPO in 2000/09, succeded Berhard M\"{u}ller in his post as a chief drafter in 2001/03.}
\begin{center}
\parbox[c]{.4\textwidth}{\centerline{\mbox{\includegraphics{ahoward}}}}
\hfill
\parbox[c]{.5\textwidth}{According to his own biography\footnote{http://www.european-patent-office.org/epidos/conf/pat\_eac01/pdf/abstracts/4\_howard.pdf}, Anthony Howard has since September 2000 been with the European Commission's Directorate-General for the Internal Market, on secondment from the UK Patent Office.  He took over the post of the chief directive drafter from Bernhard M\"{u}ller, who left in March 2001 for the European Trademark Office in Alicante. Howard's main areas of work lie in the fields of computer-implemented inventions, the Community Patent and aspects of IP law in relation to pharmaceuticals.  He joined the examining staff of the UK Patent Office in 1978 and has worked in a range of functions, including a previous posting to Brussels when he was closely involved in the TRIPS\footnote{http://swpat.ffii.org/analysis/trips/index.en.html} Uruguay Round negotiations. His most recent postition was as Deputy Director in charge of a technical examining group. Responsibilities included acting as a hearing officer for ex parte actions and as assistant for inter partes actions in proceedings conducted before the Patent Office. He has also led projects concerned with management and development of computer systems for use by technical examiners and their support staff.}
\end{center}

\begin{itemize}
\item
{\bf {\bf CEC \& BSA 2002-02-20: proposal to make all useful ideas patentable\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/index.en.html}}}

\begin{quote}
The European Commission (CEC) proposes to legalise the granting of patents on computer programs as such in Europe and ensure that there is no longer any legal foundation for refusing american-style software and business method patents in Europe.   ``But wait a minute, the CEC doesn't say that in its press release!'' you may think.  Quite right!  To find out what they are really saying, you need to read the proposal itself.  But be careful, it is written in an esoteric Newspeak from the European Patent Office (EPO), in which normal words often mean quite the opposite of what you would expect.  Also you may get stuck in a long and confusing advocacy preface, which mixes EPO slang with belief statements about the importance of patents and proprietary software, implicitely suggesting some kind of connection between the two.  This text disregards the opinions of virtually all respected software developpers and economists, citing as its only source of information about the software reality two unpublished studies from BSA \& friends (alliance for copyright enforcement dominated by Microsoft and other large US companies) about the importance of proprietary software.  These studies do not even deal with patents!  The advocacy text and the proposal itself were apparently drafted on behalf of the CEC by an employee of BSA.  Below we cite the complete proposal, adding proofs for BSA's role as well as an analysis of the content, based on a tabular comparison of the BSA and CEC versions with a debugged version based on the European Patent Convention (EPC) and related doctrines as found in the EPO examination guidelines of 1978 and the caselaw of the time.  This EPC version help you to appreciate the clarity and wisdom of the patentability rules in the currently valid law, which the CEC's patent lawyer friends have worked hard to deform during the last few years.
\end{quote}
\filbreak

\item
{\bf {\bf}}
\filbreak

\item
{\bf {\bf The UK Patent Family and Software Patents\footnote{http://swpat.ffii.org/players/uk/index.en.html}}}

\begin{quote}
Much of the lobbying for software patents in Europe has been done by British patent lawyers and patent officials wearing the hat of the European Commission or the British government.  Most of the law-drafters and alleged ``independent contractors'' for the European Commission (CEC) were members of the British patent family.  The UK Patent Office (UKPO) was the first national patent office to officially follow the European Patent Office (EPO) in allowing direct patent claims to computer programs in 1998.  The UKPO has also succeeded in keeping its hand on the British government's patent policy, in moderating a public consultation which showed strong public opposition to software patentability, and, most admirably, in interpreting this public opinion as a legitimation basis for (1) establishing software (including business method) patents in Britain and (2) pushing Brussels to establish them in all of Europe.  The UK Patent Family appears to be the strongest single force behind the European Commission's software patentability directive project.
\end{quote}
\filbreak

\item
{\bf {\bf Bernhard M\"{u}ller and Software Patents\footnote{http://swpat.ffii.org/players/bmueller/index.en.html}}}

\begin{quote}
Bernhard M\"{u}ller worked in the European Commission's Industrial Property Unit and was in charge of the software patent directive proposal drafting and consultation activities for several years until March 2001, then moved to the European Trademark Office in Alicante.  M\"{u}ller conducted ``representative surveys'' among several dozen patent lawyers in order to conclude that patents on software and business methods as granted by the European Patent Office (EPO) are needed.  Until summer 2000, M\"{u}ller advocated a ``harmonisation'' of European patentability rules with those of US and Japan.  Later, during the Consultation of late 2000 which he conducted, M\"{u}ller presented the same contents with a more cautious packaging, claiming to be aiming at a ``restrictive harmonisation of the status quo''.  In early 2001 M\"{u}ller obtained support from governmental officials for a position paper which calls for codification of EPO practise and dismisses the 91\percent{} opposition to software patents expressed in the consultation as irrelevant.
\end{quote}
\filbreak

\item
{\bf {\bf 2nd meeting of the European Commission's Industrial Property Unit with a Eurolinux delegation\footnote{http://www.eurolinux.org/news/euip017/index.en.html}}}

\begin{quote}
For the second time, representatives of the Eurolinux Alliance are meeting officials who are in charge of drafting a European Directive on the Patentability of Computer-Implementable Rules of Organisation and Calculation (Programs for Computers).  A preliminary outline of a possible directive draft, published on 2000-10-19, proposed to legalise such patents and asked a series of questions concerning details of this approach. The consultation yielded written statements, often with well known faith-based arguments, and also with a well known distribution of forces:  a large number of individual software developpers, mostly speaking for themselves, against a small number of patent lawyers, mostly speaking in the name of big organisations.  So far no further questioning and dialogue has been organised.  The Eurolinux Alliance is therefore happy to have the chance to meet the consultation organisers and help pave the way for an in-depth discussion.
\end{quote}
\filbreak

\item
{\bf {\bf The Community legal state of play in software patentability\footnote{http://www.european-patent-office.org/epidos/conf/pat\_eac01/pdf/abstracts/4\_howard.pdf}}}

\begin{quote}
speech given by Howard at an EPO conference in 2001
\end{quote}
\filbreak

\item
{\bf {\bf ITT: May 2001 - IPR for software : Patent Pending ?\footnote{http://www.cordis.lu/itt/itt-en/01-3/policy05.htm}}}

\begin{quote}
refers to a statement from Howard
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
% mode: latex ;
% End: ;

