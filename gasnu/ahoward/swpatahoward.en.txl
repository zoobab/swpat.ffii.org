<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#AoH: Anthony Howard

#CpU: Chief drafter of the European Community software patentability directive proposal, born 194x, previously an examiner and administration official at the UKPO, involved in TRIPS negotiation for the UKPO, came to the European Commission on secondment from the UKPO in 2000/09, succeded Berhard Müller in his post as a chief drafter in 2001/03.

#AWW: According to his own %(eb:biography), Anthony Howard has since September 2000 been with the European Commission's Directorate-General for the Internal Market, on secondment from the %(uk:UK Patent Office).  He took over the post of the chief directive drafter from %(bm:Bernhard Müller), who left in March 2001 for the European Trademark Office in Alicante. Howard's main areas of work lie in the fields of computer-implemented inventions, the Community Patent and aspects of IP law in relation to pharmaceuticals.  He joined the examining staff of the UK Patent Office in 1978 and has worked in a range of functions, including a previous posting to Brussels when he was closely involved in the %(tr:TRIPS) Uruguay Round negotiations. His most recent postition was as Deputy Director in charge of a technical examining group. Responsibilities included acting as a hearing officer for ex parte actions and as assistant for inter partes actions in proceedings conducted before the Patent Office. He has also led projects concerned with management and development of computer systems for use by technical examiners and their support staff.

#soo: speech given by Howard at an EPO conference in 2001

#rWf: refers to a statement from Howard

#title: Anthony Howard

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatahoward ;
# txtlang: en ;
# multlin: t ;
# End: ;

