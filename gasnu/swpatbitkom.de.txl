<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#chr: Finden Sie Kontaktdaten, Telefonnummern etc heraus und berichten Sie über Ergebnisse.

#poW: Kontaktdaten liefern

#SeF: Kontaktieren Sie %(MT), um Adressen und Hinweise zu kontaktierender Bitkom-Firmen zu bekommen.  Sie bekommen innerhalb eines Tages eine Antwort.

#mfa: Bitkom-Mitgliedsfirmen kontaktieren

#tWP: Wiki-Seite über Bitkom

#Mlt: Manches mag gegen eine Urheberrechtssteuer auf Drucker und Festplatten sprechen.  Diese Steuer ist jedoch gesetzlich geregelt, und die Argumente mit denen HP und, als deren Sprachrohr, Bitkom sich ihr zu entziehen versuchen, sind weitaus bedenklicher als die Urheberrechtssteuer selber.  Denn Individuelle Vergütungsverfahren nach Bitkom-Wunsch %(ul|sind bislang nirgends in nennenswertem Umfang im Einsatz|führen zu einer Wertminderung dessen, was vergütet werden soll|führen ebenso wenig wie andere Verfahren zu %(q:gerechter Entlohnung von Leistungen)|führen geradeaus zu den Hollywood/Hollings-Gesetzentwürfen, gegen die die sich die gesamte amerikanische IT-Welt derzeit auflehnt|führen im Erfolgsfall zu %(rr:unübersehbaren Konfliktszenarien).)  Wer aufgrund einer Utopie dieser Beschaffenheit im Ton der Entrüstung gegen die GEMA Sturm laufen zu müssen glaubt, ist wohl einfach nur von allen guten Geistern verlassen.

#Gig: GEMA droht mit Millionenklage wegen Abgabe auf CD-Brenner

#bca: Bitkom-Bericht über eine unter Mitgliesunternehmen durchgeführte Umfrage.  Leider fehlt bislang der Fragenkatalog.

#Wsn: Bitkom-Umfrage: 84% sehen in Patenten ein geeignetes Mittel zum Schutz computer-implementierbarer Erfindungen

#WeU: Es sollte daran erinnert werden, dass es bereits einige Meinungsumfragen über die Haltung der Softwareunternehmen zu Patenten gibt so etwa die von Effy Oz, Pamela Samuelson, ACM Titus, OTA, USPTO etc in den USA und die von Fraunhofer sowie die Konsultation der Europäischen Kommission hier.  Bei Oz zeigten sich ca 90% der führenden Entwickler in großen Softwarehäusern gegen Softwarepatente.  Beim US-Patentamt war Microsoft das einzige befürwortende Software-Unternehmen.  Auch Fraunhofer stellte stärkere Abneigung gegen das Patentwesen fest als Bitkom.  Wen wundert's?

#knr: In der Tat wendet sich niemand gegen Patente auf %(q:computer-implementierte Erfindungen).  Allerdings glauben viele von uns noch immer, dass zwar ein chemischer Vorgang oder ein Antiblockiersystem unabhängig davon patentiert werden kann, ob zu seiner Steuerung ein Rechner eingesetzt wird, dass aber die steuernde Software als solche keine Erfindung im Sinne des Patentrechts darstellt.  Diese Regelung wollen wir beibehalten.

#eaf: Auch der FFII hätte %(q:beibehalten) geantwortet, und Bitkom hätte berichtet %(q:FFII befürwortet Softwarepatente).

#eue: eingeschränkt

#atn: beibehalten

#xed: ausgeweitet

#gtr: Unter 90 befragten Mitgliedsfirmen fallen die Antworten wie folgt aus:

#pWa: Sollten die derzeitigen Möglichkeiten zum Schutz computer-implementierter Erfindungen erhalten, ausgeweitet oder eingeschränkt werden?

#WWs: Eine der Fragen scheint auf eine politische Meinungsäußerung zu zielen.  Bitkom fragt in etwa:

#8cl: Die 82 %(q:Ja) interpretiert Bitkom dann als %(q:82% für Softwarepatente).  Auch der FFII hätte %(q:ja) geantwortet.

#vrW: Glauben Sie, dass Patente Ihnen dabei helfen könnten, Risikokapital zu erhalten?

#Wts: Glauben Sie, dass Patente dazu beitragen können, die Verhandlungsposition eines Unternehmens zu stärken?

#euv: Die Studie stellt zunächst eine appetitanregende Grundfrage in vielen Variationen

#iWc: Leider enthält der Umfrage-Bericht nicht den Fragebogen, aber immerhin werden einige Fragen indirekt zitiert.

#ehW: Im zweiten Halbjahr 2002 erstellte der Teuflesche Arbeitskreis eine %(um:unveröffentliche Umfrage), die von EICTA-Lobbyisten gegenüber Euro-Parlamentariern und auch Bundestagsabgeordneten als Grundlage ihrer Behauptung verwendet wurde, die meisten Softwarefirmen wollten Softwarepatente.

#IBe: Im Vorstand von Bitkom sitzen Geschäftsleute von Microsoft (Sibold), IBM, Siemens und anderen Großunternehmen.  Auch in den Arbeitsausschüssen haben vor allem diese Leute die nötige Zeit, um die Verbandspolitik zu bestimmen.  Im Vorstand ist keine einzige Person vertreten, die Ansehen als Programmierer oder Informatiker genießt.  Für die einfachen Mitglieder ist Bitkom weniger eine Interessenvertretung als eine Gelegenheit, gewisse Leistungen in Anspruch zu nehmen, vgl ADAC.

#Ect: Ein Bitkom-Sprecher kann offenbar fachlich noch so ahnungslos daherreden -- Hauptsache die Gesinnung stimmt.  Solange er seine Forderungen an der Devise %(q:Mehr Macht für die Vorstände) ausrichtet, wird er schon nicht zur Rechenschaft gezogen werden.

#Dsr: Dieser Standpunkt wir allerdings nicht besonders publik gemacht, er findet sich nur seit kurzem in unauffälligen PDF-Dateien hinter kryptischen Überschriften (%(q:SWP-Gutachten)).

#FeW: Für Bürokratisierung und staatliche Regulierung durch Patente

#nWW: nicht nur hinsichtlich Inderimport:  möglichst viele Rechte und wenige Pflichten für Unternehmensvorstände

#Gee: Gegen Bürokratisierung und staatliche Regulierung aller Art

#dha: die Schröder-Ankündigung auf CeBit 2001 stützte sich vor allem auf eine Bitkom-Studie.  Seit dem IT-Konjunktureinbruch im zweiten Halbjahr hört man weniger davon.

#Foe: Für den Import billiger IT-Fachkräfte

#HnE: Besonders der Vorsitzende Rohleder ist von DRM und anderen Hoffnungen auf Verkrüppelung digitaler Information zwecks Kassierens pro Lesevorgang begeistert.  Auch der Dachverband EICTA setzt hier einen Schwerpunkt.  Rohleder und EICTA-Kollegen fordern strafrechlich bewehrten Kopierschutz und Einschränkung der Programmierfreiheit gemäß neuer EU-Kopierschutzrichtlinie (EuroDMCA).  Dort, wo die IT-Honoratioren neue Wertschöpfungspotentiale wittern, muss der Staat hart durchgreifen, um deren Verwirklichung zu erzwingen.  Ob diese Art der Wertschöpfung funktioniert oder produktiv ist, interessiert vorerst nicht.  Der Staat hat so zu tun, als ob sie funktionieren würde und schon heute -- ja sogar rückwirkend für die Vergangenheit -- ordnungspolitische Entscheidungen, z.B. hinsichtlich Pauschalgebühren, an dieser Fiktion auszurichten.

#Gei: Gegen Urheberrechtspauschalgebühren, für Verwertungsutopien

#Beh: Bitkom engagierte sich in letzter Zeit

#tmW: Bitkom-Umfrage 2002

#Wee: Organisationsstruktur

#RAn: Roter Faden: Mehr Macht den Vorständen (?)

#EWh: Ein Bitkom-Sprecher versichert, 20000 Gutachten hätten bewiesen, dass Bedenken gegen die UMTS-Strahlen unbelegt seien und warnt vor wirtschaftlichen Schäden, welche die anhaltende Gesundheitsdebatte erzeuge.

#Btr: BITKOM: Gesundheitsdebatte beenden, UMTS zügig ausbauen!

#elr: Susanne Schopf und Bernd Rohlehder erklären, ohne Softwarepatente gebe es keine Innovation, müsse die Branche ins Ausland wandern, fordern vom Rat, mit dem Europäischen Parlament auf Konfrontationskurs zu gehen oder dessen Entscheidung zu kippen: %(orig|Bitkom setzt sich dafür ein, dass der Ministerrat in seiner Stellungnahme nicht den Irrwegen des Parlaments folgt, sondern seinen bisherigen vernünftigen Kurs weiterverfolgt und sich im Interesse der europäischen Wirtschaft für einen angemessenen Patentschutz für computerimplementierte Erfindungen einsetzt.).

#2nr: Bitkom-PE 2003-03-22:

#HWk: Heise-Bericht über die Bitkom-Erklärung

#Htn: Heise: IT-Verband sieht bei Software-Patenten Nachbesserungsbedarf

#descr: Der deutsche Branchenverband Bitkom hat erst Mitte 2001 begonnen, sich mit Fragen der Patentpolitik zu befassen.  Die Meinungsbildung fand offenbar in einem sehr kleinen Kreis von Juristen und Patentjuristen statt, wobei IBM-Patentanwalt Fritz Teufel alles dominierte.  An der EU-Konsultation 2000 zu Swpat nahm Bitkom wegen unabgeschlossener Meinungbildung in der Sache nicht teil, dafür aber sprang der europäische Dachverband EICTA ein, für den offenbar ebenfalls Teufel die Stellungnahme schrieb.

#title: Vom Teufel geritten --- BITKOM e.V.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbitkom ;
# txtlang: de ;
# multlin: t ;
# End: ;

