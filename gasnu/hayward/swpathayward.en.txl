<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Peter Hayward and Software Patents

#descr: A key player in the drive for software patentability at the UKPO and the European Commission, entrusted by the British government in late 2000 to conduct a %(q:consultation) and formulate policies in the name of the government.

#dlo: divisional director in the Patents and Designs Directorate of the UKPO.  His division, which includes 100 professional staff, is responsible for the search and examination of most of the electrical, electronic and computing patent applications received by the Office.  He has particular responsibility for government policy on the patentability of software and business method related innovations.  He initiated the %(e:forum on legal protection for software-related innovation) held at the UKPO in 1994 and was also a key player in the Office's %(lc:follow-up conferene in London in 1998).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpathayward ;
# txtlang: en ;
# multlin: t ;
# End: ;

