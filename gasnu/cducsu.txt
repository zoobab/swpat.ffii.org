CDU/CSU-Europaabgeordnete in den relevanten Ausschüssen des
Europäischen Parlaments 
------------------------------------------------------------

  JURI Ausschuss für Recht und Binnenmarkt 

    http://www.cdu-csu-ep.de/ueber-uns/ausschuss-06.htm

    Ordentliche Mitglieder CDU/CSU

      Klaus-Heiner Lehne                     Tel. -5047 Fax -9047
      klehne@europarl.eu.int       Nordrhein-Westfalen
      http://www.LehneMdEP.de/
      geb. 28. Oktober 1957, Düsseldorf
    * Koordinator der EVP in JURI
      Rechtsanwalt

      Kurt Lechner                           Tel. -5826 Fax -9826
      klechner@europarl.eu.int     Rheinland-Pfalz
      http://www.kurt-lechner.de/
      geb. 26. Oktober 1942, Kaiserslautern
      Rechtsanwalt und Notar

      Rainer Wieland                         Tel. -5545 Fax -9545
      rwieland@europarl.eu.int     Baden-Württemberg
      http://www.mdep.de/
      geb. 19. Februar 1957, Stuttgart 
      Rechtsanwalt

      Dr. Joachim Wuermeling                 Tel. -5711 Fax -9711
      jwuermeling@europarl.eu.int  Bayern   
      http://www.wuermeling.net/
      geb. 19. Juli 1960, Münster 
    * Schattenberichterstatter in JURI zum EU-Richtlinienvorschlag
      Rechtsanwalt, Lehrbeauftragter an der Universität Bayreuth
      95-99 Bayerische Staatskanzlei: Referatsleiter in der
      Europaabteilung 

    Stellvertretende Mitglieder CDU/CSU

      Hartmut Nassauer                       Tel. -5361 Fax -9361
      hnassauer@europarl.eu.int    Hessen
      http://www.hartmut-nassauer.de/
      geb. 17. Oktober 1942, Marburg/Lahn 
    * Vorsitzender der CDU/CSU-Gruppe im Europäischen Parlament
      Rechtsanwalt, 90-91 Hessischer Minister des Inneren 

      Dr. Angelika Niebler                   Tel. -5390 Fax -9390
      aniebler@europarl.eu.int     Bayern
      http://www.angelika-niebler.de/
      geb. 18. Februar 1963, München
    * Schattenberichterstatterin in ITRE zum EU-Richtlinienvorschlag
      Rechtsanwältin (Patentanwältin ?)

      Prof. Dr. Hans-Peter Mayer             Tel. -5994 Fax -9994
      Niedersachsen 
      http://www.europa-mayer.de/
      geb. 5. Mai 1944, Riedlingen
      Professor für Recht- und Verwaltungslehre, Rechtsanwalt
      1991 Staatssekretär im Ministerium für Wirtschaft und Technologie 
      des Landes Sachsen-Anhalt (mit Europa-Referat) 


  ITRE Ausschuss für Industrie, Außenhandel, Forschung und Energie 

    http://www.cdu-csu-ep.de/ueber-uns/ausschuss-07.htm

    Ordentliche Mitglieder CDU/CSU

      Dr. jur. Peter-Michael Mombaur         Tel. -5978 Fax -9978
      pmombaur@europarl.eu.int     Nordrhein-Westfalen     
      -
      geb. 12. Dezember 1938, Solingen 
    * Stellv. Vorsitzender ITRE
      Rechtsanwalt, 98-00 stellv. Mitglied des Verfassungsgerichtshofs
      Nordrhein-Westfalen 

      Dr. Konrad Schwaiger                   Tel.       Fax -9384
      kschwaiger@europarl.eu.int   Baden-Württemberg
      -
      geb. 25. April 1935, Bruchsal
    * stellv. Koordinator EVP-Fraktion in ITRE 
        (EVP-Sprecher für Außenwirtschaft) 
      Wirtschaftsjurist, 1990-92 Staatssekretär im Ministerium für 
      Wirtschaft, Technologie und Verkehr von Sachsen-Anhalt 

      Dr. Werner Langen                      Tel. -5385 Fax -9385 
      Wlangen@europarl.eu.int      Rheinland-Pfalz
      http://www.EUINFO.de/
      geb. 27. November 1949, Müden/Mosel
      Dipl.-Volkswirt / Dr. rer. pol., 
      1990-91 Minister für Landwirtschaft, 

      Dr. Angelika Niebler                   Tel. -5390 Fax -9390
      aniebler@europarl.eu.int     Bayern
      http://www.angelika-niebler.de/ 
      geb. 18. Februar 1963, München
    * Schattenberichterstatterin in ITRE zum EU-Richtlinienvorschlag
      Rechtsanwältin (Patentanwältin ?)

      Dr. Godelieve Quisthoudt-Rowohl        Tel. -5338 Fax -9338
      gquisthoudt@europarl.eu.int  Niedersachsen
      http://www.europabrief.de/
      geb. 18. Juni 1947, Etterbeek (Belgien)
      Dr. rer. nat. (Physikalische Chemie)

    Stellvertretende Mitglieder CDU/CSU

      Prof. Dr. Alfred Gomolka               Tel. -7307 Fax -9307
      agomolka@europarl.eu.int     Mecklenburg-Vorpommern 
      -
      geb. 21. Juli 1942, Breslau 
      Geographie, Germanistik, 1990-92 Ministerpräsident

      Dr. Hans-Peter Liese                   Tel. -7981 Fax -9981
      pliese@europarl.eu.int       Nordrhein-Westfalen
      http://www.peter-liese.de/
      geb. 20. Mai 1965, Olsberg 
      Arzt

      Alexander Radwan                       Tel. -5538 Fax -9538
      aradwan@europarl.eu.int       Bayern
      http://www.alexander-radwan.de/
      geb. 30. August 1964, München
    * Mitglied SME Circle (Europaabgeordnete aus der SME Union =
        Small & Medium Entrepreneurs Union of the EPP)
      Rechtsanwalt, Diplom-Ingenieur (FH) Fachrichtung Luftfahrzeuge
      Abteilungsleiter in Unternehmen der Telekommunikations-Branche

      Dr. Horst Schnellhardt                 Tel. -7618 Fax -9618 
      hschnellhardt@europarl.eu.int Sachsen-Anhalt
      http://www.schnellhardt-europa.de/
      geb. 12. Mai 1946, Rüdigershagen
      Fachtierarzt für Lebensmittelhygiene
 
      Prof. Dr. Jürgen Zimmerling            Tel. -7868 Fax -9868
      jzimmerling@europarl.eu.int   Nordrhein-Westfalen
      http://www.juergen-zimmerling.de/
      geb. 17. Februar 1952, Oberhausen
      Professor für Wirtschaftsrecht und Wirtschaftsverwaltungsrecht


  CULT Ausschuss für Kultur, Jugend, Bildung, Medien und Sport 

    http://www.cdu-csu-ep.de/ueber-uns/ausschuss-13.htm

    Ordentliche Mitglieder CDU/CSU

      Doris Pack                             Tel. -7310 Fax -9310
      dpack@europarl.eu.int         Saarland
      http://www.dorispack.de/
      geb. 18. März 1942, Schiffweiler/Saar 
    * Koordinatorin der EVP-Fraktion in CULT
      Lehrerin, Rektorin im saarländischen Ministerium für Kultus, 
      Bildung und Sport 

      Ruth Hieronymi                         Tel. -5859 Fax -9859
      rhieronymi@europarl.eu.int    Nordrhein-Westfalen
      http://www.hieronymi.de/
      geb. 8. November 1947, Bonn
      Magister Examen (M.A.) Universität Köln

      Sabine Zissener                        Tel. -5708 Fax -9708
      szissener@europarl.eu.int     Rheinland-Pfalz
      http://www.sabine-zissener.de/
      geb. 1. November 1970, Gebhardshain 
      Arzthelferin

    Stellvertretende Mitglieder CDU/CSU

      Rolf Berend                            Tel. -5413 Fax -9413
      rberend@europarl.eu.int  Thüringen 
      http://home.brx.epri.org/berend/
      geb. 1. Oktober 1943, Gernrode/Eichsfeld 
      Lehrer (Studium Musikerziehung/Germanistik)
