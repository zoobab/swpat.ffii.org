<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#oWi: Thomson website

#tWs: contains a statement from Tord Jansson, a prominent victim of Thomson's patent policies.

#asc: Note that Thomson probably owns many more than these, because its business model is (partially) based on acquiring and enforcing other companies's patents.

#nen: Thomson Patents at the European Patent Office

#descr: Various subsidiaries of the US-based Thomson group have been active in lobbying for software patentability in Europe.  Thomson's strategy is largely based on acquiring patents and enforcing them, but Thomson also has an R&D department of its own.  One of Thomson's first victims in Europe was Tord Jansson, a developper of opensource audio streaming software.  Thomson has shifted its MP3 licensing conditions from a liberal to a more aggressive regime, so that some shareware developpers were forced to take support for MP3 encoding and decoding out of their software after having spent years on developping it.

#title: Thomson and European Software Patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatthomson ;
# txtlang: en ;
# multlin: t ;
# End: ;

