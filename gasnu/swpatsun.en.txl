<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#epat: Sun Microsystems Software Patents at the EPO

#descr: Sun was the number 1 software patent applicant at the European Patent Office in the late 90s for several years.  Sun's rise as a patenter marked the end of the dominance of IBM in this field.  During that time, its patent lawyers also appeared at various public conferences and advocated patentability of software.  They were fairly effective in that role, because Sun enjoyed a reputation of supporting open architectures and cooperating well with free software.  Not everybody in the audience was aware of the cultural gaps between the IP department and other departments.  Sun's patenting activities gradually declined and left its position to companies like Microsoft, Matsushita and Hewlett Packard.

#title: Sun Microsystems and European Software Patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsun ;
# txtlang: en ;
# multlin: t ;
# End: ;

