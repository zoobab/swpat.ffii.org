<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: SPD und Softwarepatente

#descr: Mit Internet und Informatik befassten Abgeordneten der
Sozialdemokratischen Partei Deutschlands (SPD) wie MdB Jörg Tauss,
MdEP Evelyn Gebhardt u.a. haben mehr oder weniger entschiedenen
Widerstand gegen die Legalisierung von Softwarepatenten geleistet,
während andere wie MdEP Willi Rothley (stellvertretender Vorsitzender
des Europarl-Rechtsausschusses) sich für möglichst unbegrenzte
Patentierbarkeit im Sinne der Praxis des Europäischen Patentamtes
stark machten.  Bei der Abstimmung vom 24. September 2003 war die
SPD-Fraktion im EU-Parlament gespalten.  An der Spitze der Befürworter
grenzenloser Patentierbarkeit stand Erika Mann, die noch wenige Monate
zuvor als Meinungsführerin der Patent-Kritiker hervorgetreten war. 
Tauss präsidierte als Bildungsexperte über die Einführung des
Hochschulpatentgesetzes.  Innerhalb der SPD-Bundestags-Fraktion sind
neben der Arbeitsgruppe Neue Medien auch der Rechtsausschuss und der
Wirtschaftsausschuss mit der Softwarepatente-Frage befasst.  Der
Abgeordnete Dr. med Wolfgang Wodarg hat sich kenntnisreich gegen
Genpatente engagiert.  Die Justizministerinnen Herta Däubler-Gmelin
und Zypries schienen bis 2003 im wesentlichen die Interessen ihres
Apparates nach außen zu vertreten, d.h. sie betrachten das Patentwesen
als ihre eigene Politikdomäne, in die sie sich durch keinen offenen
Dialog hineinreden lassen wollen, und folgen im wesentlichen den
Vorgaben der Patentämter und ihrer Beiräte aus
Industrie-Patentanwälten.  SPD-Parteisekretär Müntefering äußerte
gegenüber dem DMMV, man sei gemeinsam mit diesem für Softwarepatente. 
Der Virtuelle Ortsverein (VOV) der SPD hat besonders klare Standpunkte
gegen die Patentierbarkeit von Rechenregeln bezogen, und die
SPD-Bundestagsfraktion hat sich organisiert mit dem Thema befasst. 
Zwar überwiegt dabei eine kritische Haltung, aber im Ergebnis konnten
die Patentjuristen durch wenige rhetorische Verstellungen ihre
Positionen behaupten und die SPD-geführten Bundesregierung auf einen
Kurs der grenzenlosen Patentierbarkeit bei maximaler Blockierwirkung
bringen und dort halten.

#vovt: Virtueller Ortsverein der Sozialdemokratischen Partei Deutschlands

#IuW: In den 80er Jahren gegründeter eingetragener Verein, der die SPD
mithilfe einer netzbasierten Öffentlichkeit von unten her erneuern und
in Fragen der Netzpolitik kompetent machen möchte.  Setzt sich gegen
Softwarepatente ein.  Enge Verbindungen zu netzpolitisch aktiven
SPD-Abgeordneten wie Jörg Tauss, der den Verein einige Jahre leitete.

#FWc: Forum des MdB Ulrich Kelber

#A2l: Ein javaskript-verseuchtes Forum, welches den Besucher durch
Verweisschleifen gefangen hält und dessen Diskussionsbeiträge nicht
per URL zitierbar sind.  Inhaltlich ist das Forum z.T. recht
interessant.  MdB Kelber beantwortet viele Fragen und verrät
gelegentlich etwas über den Stand der Debatte innerhalb der SPD zu
Softwarepatenten.

#Sft: SPD.de - Klartext - Informations-, Kommunikations- und Medienpolitik

#EDe: Ein zuständiges Diskussionsforum der SPD, in dem oft allerlei Unmut
über die Medienpolitik der Regierung geäußert wird.

#Lrg: Leipziger Grundsatzprogramm der SPD

#Dst: Das Demokratieverständnis der SPD scheint sich stark von dem der
Europäischen Kommission zu unterscheiden.  Während die EUK für eine
blinde Gefolgsamkeit gegenüber Verbandsmeinungen, die sie mitunter als
%(q:wirtschaftlichen Mehrheiten) bezeichnet, plädiert, steht im
Leipziger Grundsatzprogramm der SPD zu lesen: %(bq:Demokratie bezieht
ihre Lebenskraft aus der Gesellschaft und ihrer politischen Kultur.
Sie wird durch die Ballung von wirtschaftlicher oder Medienmacht und
durch die Anhäufung von Herrschaftswissen in privater oder
öffentlicher Hand bedroht.)

#Ahi: Auf eine Anfrage von Torsten Wöllert von Ende Februar 2002 über die
Haltung der SPD bezüglich des %(es:Richtlinienvorschlages der
Europäischen Kommission für die Patentierbarkeit von
Programmierkonzepten) antwortet Kelber:%(bc|Leider habe ich noch
keinen Überblick zur Meinungsbildung in den deutschen Parteien. Bei
einer ensprechenden %(ba:Anhörung), die auf Anregung der SPD
stattfand, haben sich mein Kollege Jörg Tauss und ich gegen eine
Amerikanisierung ausgesprochen, ebenso die grüne Vertreterin. CDU und
FDP haben sich bedeckt gehalten, die PDS war nicht
vertreten.|Meinungsbildung hat dann bisher nur in einem der damit
befassten SPD-Arbeitsgruppen stattgefunden. Dort schlossen sich die
Kollegen Tauss und mir an.|Zumindest in Frankreich ist die Skepsis
gegen eine amerikanische Form der Patentierung groß, aus anderen
Ländern kenne ich vereinzelte Stimmen. Gerade die kleinen Länder
sollten da eigentlich ein Eigeninteresse haben.)  Am 2002-02-05
erklärte Kelber zu diesem Thema:%(bc|Letzte Woche gab es aber weder in
SPD-Fraktion noch in der Regierung eine abgeschlossene Meinung. Auch
die Generaldebatte hat noch nicht stattgefunden. ... Beschäftigt in
der Fraktion sind Rechtsausschuss, Wirtschaftsausschuss und
Arbeitsgruppe Neue Medien.) Kelber bemüht sich, als %(q:gläserner MdB)
seine privaten Einkünfte offenzulegen.  Während der Diskussion um die
Verwendung von GNU/Linux im Bundestag im Januar 2002 stellte sich
Kelber gegen den politischen Diskurs der GNU-Befürworter. 
Anschließend lasen Journalisten in Kelbers freiwilligen Angaben, dass
er erhebliche nebenberufliche Einkünfte aus Beratungsdiensten für eine
von %(ms:Microsoft) abhängige Firma bezog.  Beim anschließenden
Aufruhr fand Kelber einige Fürsprecher in den Reihen der
GNU-Befürworter.

#DcD: Der Artikel erzählt, wie das Arbeitnehmer-Erfinderrecht und damit
teilweise die Patentbewegung in der SPD der Weimarer Zeit
einflussreiche Fürsprecher fand.

#MPe: Mit Ihrem SPD-Abgeordneten/Kandidaten sprechen

#Btd: Bitte berichten Sie uns dann darüber!

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatspd ;
# txtlang: de ;
# multlin: t ;
# End: ;

