Stefan Pollmeier
----------------

  Dipl.-Ing. Stefan Pollmeier, age 43, 
  General manager, head of development department ESR Pollmeier GmbH
  active member of several industry associations, 21 publications
  (e. g. "Standards for networked drives in open systems",
  speech to be held at VDE conference Dresden 22. October 2002)

  Former software developer, now general manager and head of 
  development department, deeply involved in software and the issues 
  of software patents. From many discussions regarding software patents 
  Stefan Pollmeier had with entrepreneurs, industry associations, patent 
  attorneys and others he knows the position of enterprises, especially 
  small and medium, very well. Through his own work in industry 
  associations and in developing standards he can asses the influence 
  of software patents on standards and technology. Author of the
  website http://www.esr-pollmeier.de/swpat/index_en.html on software
  patents. Member of IEEE, VDE, VDI, FFII.

ESR Pollmeier 
-------------

  ESR Pollmeier GmbH, 45 employees, http://www.esr-pollmeier.de
  industrial electronics, software accounts for around 80 % of 
  ESR's development costs

    Remark: the software ESR develops is for so-called "embedded 
    systems" where computers are hidden in devices. Around 70 % 
    (percentage growing) of all software developers work on embedded 
    systems, only the rest develops software for classic computers. 

ESR and open ...
----------------

ESR Pollmeier GmbH

   is supporting open international or industry standards for 
   networking drives and integrating drives in open automation systems 
   (fieldbusses) http://www.esr-pollmeier.de/en/Feldbusse.html


Stefan Pollmeier and open ...
-----------------------------

Board member of Drivecom User Group e. V.

  An association of international drive manufacturers, universities,
  institutes. Goal of Drivecom is to develop industry standards 
  for simple integration of drives in open automation systems.
  http://www.drivecom.org/

  As board member responsible for standards, e. g.
 
http://www.drivecom.org/download/Drivecom_Device_Description_en_11.pdf      

Member of standardisation committees for open communication standards
for drives of Profibus Nutzerorganisation e. V., Sercos interface e. V.
and (in the past) CAN in Automation e. V.

Vice chairman of the ZVEI committee "communication in automation"

  ZVEI is the German electrical and electronic manufacturers' 
  association. ZVEI's committee "communication in automation" defines
  itself as an independent platform for information interchange,
  especially in the field of global innovation trends regarding
  communication in automation (e. g. fieldbus, Ethernet in automation).
http://www.zvei.org/automation/organisationsplan/ta/ind_kommunikation.html
  Member of the sub-group "Drive Interface" preparing a standard in
  cooperation with IEC
  


-- 
Dipl.-Ing. Stefan Pollmeier      mailto:gl@esr-pollmeier.de

ESR Pollmeier GmbH               Tel. +49 6167 9306-0
Lindenstr. 20                    Fax  +49 6167 9306-77

D-64372 Ober-Ramstadt, Germany   http://www.esr-pollmeier.de


