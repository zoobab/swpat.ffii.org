\begin{subdocument}{swpatbetten}{J\"{u}rgen Betten's lifetime fight for Software Patents}{http://swpat.ffii.org/players/betten/index.en.html}{Workgroup\\swpatag@ffii.org}{PA J\"{u}rgen Betten, a Munich-based patent attorney, chairman of several software patent work groups of patent lawyer associations, rapporteur to AIPPI, close collaborator of Prof. Straus and friends from MPI, AIPPI lobbyist to the European Commission, passionate software patentability advocate, author of expert opinions ordered by the EPO, attorney in EPO procedings that brought about landmark decisions, has particularly many Japanese clients.}
In 1997 Betten was a main organiser of the UNION conference in the EPO, which demanded general patentability of software.

It seems that Mr Betten has advocated sofware patentability very vocally and sometimes even rudely.  He jumped out as a leader of software patentability when most of his colleagues found this subject to be rather embarassing and preferred to muddle through silently.  Thus it seemed as if introducing software patents had been Betten's big lifetime project.  Once the subject becomes unpopular and there is a serious backlash, Betten is likely to be one of the first to be given the blame, as he has never been popular among colleagues.  Yet, his outspoken style was appreciated in the given situation, and even in autumn 2000, Betten was sent on a lobbying mission to Brussels by AIPPI in order to prevent the postponement of the deletion of Art 52 EPC.

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Betten \& Resch 1999: News on Computer Law in Europe and Germany\footnote{http://swpat.ffii.org/papers/bettenresch9901/index.en.html}}}

\begin{quote}
In a circular to their clients, leading software patent lawyers and patentability activists in Munich explain: the European Patent Office (EPO) has decided that media claims and Internet claims admissible.  In the long run, the EPO will grant data structure claims.   In view of the practice of the last two to five years it can be said that, in principle, a patent will be granted for all computer programs (including business methods) which are new and inventive.
\end{quote}
\filbreak

\item
{\bf {\bf }}
\filbreak

\item
{\bf {\bf }}
\filbreak

\item
{\bf {\bf PA J\"{u}rgen Betten answers questions about software patents\footnote{http://swpat.ffii.org/papers/betten-faq00/index.de.html}}}

\begin{quote}
Der langj\"{a}hrige Softwarepatentaktivist listet allerlei aus seiner Sicht falsche Argumente der Swpat-Gegner auf und antwortet darauf.  Er erw\"{a}hnt einige F\"{a}lle von Swpat-Einspruchsverfahren am EPA.  Offenbar wurde eines der bekanntesten Gruselpatente widerrufen und bei anderen haben bis zu 10 Firmen Einspruch erhoben.  Ferner erw\"{a}hnt Betten, dass es bislang keine Swpat-Verletzungsklagen gegeben hat. Erw\"{a}hnt eine ``japanische Studie'', die extensive Patentpolitik der USA als Grundlage f\"{u}r deren Aufstieg und Japans Niedergang sieht.
\end{quote}
\filbreak

\item
{\bf {\bf European Union of Industrial Property Professionals\footnote{http://swpat.ffii.org/players/union/index.en.html}}}

\begin{quote}
Die UNION tagte im Dezember 1997 im Europ\"{a}ischen Patentamt zum Thema Patentierbarkeit von Computerprogrammen. Dabei forderten die meisten Redner eine generelle Patentierbarkeit von Computerprogrammen. Hierzu wurde auch ein Tabungsband und eine Resolution verabschiedet.
\end{quote}
\filbreak

\item
{\bf {\bf Association Internationale Pour la Protection de la Propri\'{e}t\'{e} Industrielle (AIPPI)\footnote{http://swpat.ffii.org/players/aippi/index.en.html}}}

\begin{quote}
AIPPI defines itself as an ``association of patent lawyers, patent owners and other users of the institutions of industrial property''.  Indeed most actors of the patent world are members of AIPPI.  In Vienna in 1997 AIPPI passed a resolution demanding the legalisation of software patents in Europe.  In 2001 in Melbourne there was an AIPPI resolution proposal for worldwide patentability of business methods (unclear whether accepted or not).  In 2002 an AIPPI paper submitted to WIPO demands worldwide patentability of software and business methods.
\end{quote}
\filbreak

\item
{\bf {\bf Patent Lobbyism and Scripture Erudition in the name of Max Planck\footnote{http://swpat.ffii.org/players/mpi/index.de.html}}}

\begin{quote}
Das Max-Planck-Institut f\"{u}r Internationales Patent-, Urheber- und Wettbewerbsrecht in M\"{u}nchen bet\"{a}tigt sich seit jahren als rechtspolitischer Wegbereiter des Privatbesitzes an allen Geisteserzeugnissen, die sich irgendwie beanspruchen und verwerten lassen.  Insbesondere der Lehrstuhl f\"{u}r Gewerbliche Schutzrechte ist eng mit EPA, AIPPI, Gro{\ss}unternehmen und allen Schaltstellen der Patentbewegung in der Bundesregierung, der Europ\"{a}ischen Kommission und bei den Vereinten Nationen verflochten.  Ratschl\"{u}sse des Europ\"{a}ischen Patentamtes und seiner Freunde sind f\"{u}r Prof. Straus und Kollegen ein Heiliger Gral, der \"{u}ber dem Gesetz und \"{u}ber der Wirklichkeit steht.  Die unersch\"{o}pflichen Weisheiten des Patentrechtsprechung gilt es, wie Plancks Kollege Rutherford zu sagen pflegte, briefmarkensammlerisch aufzubereiten, aber niemals im Geiste der Wissenschaft an ihren Wirkungen zu messen.  Es gibt jedoch einen mehr oder weniger gut versteckten Ma{\ss}stab, an dem das MPI seine Schriftauslegungsk\"{u}nste orientiert: die W\"{u}nsche der ``Wirtschaft'', d.h. der Kommilitonen und Freunde aus den Gro{\ss}konzern-Patentabteilungen, denen das Institut direkt oder indirekt sein Wohlergehen verdankt.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
% mode: latex ;
% End: ;

