<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Jürgen Betten's lifetime fight for Software Patents

#descr: PA Jürgen Betten, a Munich-based patent attorney, chairman of several software patent work groups of patent lawyer associations, rapporteur to AIPPI, close collaborator of Prof. Straus and friends from MPI, AIPPI lobbyist to the European Commission, passionate software patentability advocate, author of expert opinions ordered by the EPO, attorney in EPO procedings that brought about landmark decisions, has particularly many Japanese clients.

#Ihm: In 1997 Betten was a main organiser of the UNION conference in the EPO, which demanded general patentability of software.

#Ico: It seems that Mr Betten has advocated sofware patentability very vocally and sometimes even rudely.  He jumped out as a leader of software patentability when most of his colleagues found this subject to be rather embarassing and preferred to muddle through silently.  Thus it seemed as if introducing software patents had been Betten's big lifetime project.  Once the subject becomes unpopular and there is a serious backlash, Betten is likely to be one of the first to be given the blame, as he has never been popular among colleagues.  Yet, his outspoken style was appreciated in the given situation, and even in autumn 2000, Betten was sent on a lobbying mission to Brussels by AIPPI in order to prevent the postponement of the deletion of Art 52 EPC.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbetten ;
# txtlang: en ;
# multlin: t ;
# End: ;

