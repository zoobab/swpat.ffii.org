<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Jerzy Montag und Softwarepatente

#descr: MdB Jerzy Montag ist rechtspolitischer Sprecher der Grünen, Vertreter
im Rechtsausschuss des Bundestages, stellv. in Angelegenheiten der
Europäischen Union.  Er meldete sich im Herbst 2004 in der Diskussion
um die Softwarepatent-Resolution des Bundestages zu Wort und bezog
dabei in wesentlichen Punkten überraschend patentlobby-freundliche
Positionen.

#KKa: Jerzy Montag wurde am 13. Februar 1947 in Kattowitz, Polen, geboren. 
Er ist seit 1975 %(ra:Rechtsanwalt) und Fachanwalt für Strafrecht in
München.  Seit 2002 ist Montag rechtspolitischer Sprecher der Grünen
im Bundestag sowie Vertreter im Rechtsausschuss und stellvertretend im
Ausschuss für Angelegenheiten der Europäischen Union.

#lhW: Jerzy Montag machte bei diversen öffentlichen und halb-öffentlichen
Anlässen im Herbst 2004 klar, dass er wenig von den
%(q:undifferenzierter) und %(q:populistischer) Generalablehnung von
Softwarepatenten hält. Er setzte als Unterhändler im Bundestag
Positionen durch, die Kernanliegen des %(BMJ) entsprechen.  So wollte
Montag z.B. im Gegensatz zu den Unterhändlern von SPD, FDP und CDU
abhängige Programmansprüche nicht ausschließen.

#eia: Montag verteidigt Ratsvereinbarung und Programmansprüche

#trW: deutlich gegen Softwarepatente und Ratsvereinbarung, aber Spuren von
Spannung innerhalb der Bundesgrünen sind auch hier zu erkennen

#Wec: TAZ zum Bundestagsbeschluss

#WAa: zitiert Montag und %(KRINGS).  Während Krings den Bundestags-Antrag
als Kritik am BMJ wertet, tritt Montag dieser Wertung entgegen.

#Wib: Bei den Verhandlungen über den interfraktionellen Antrag setzt sich
Montag für Positionen des %(BMJ) bezüglich Softwarepatenten ein. 
Während die Unterhändler der anderen Fraktionen zu einem klaren
Ausschluss von %(pa:Programmansprüchen) (ohne einschränkende
Attribute) neigen, will Montag den Ausschluss auf %(q:selbstständige
Programmansprüche) begrenzen, d.h. de facto nichts ausschließen.

#Wra: Juristen unter sich, kein Vertreter einer FFII-nahen Position

#nnW: Büro Montag plant Softwarepatenttreffen grüner Entscheider mit
Vertretern diverser Standpunkte aber ohne FFII, legt in Schreiben an
Brüsseler Grüne diesen auch nahe, nicht mit FFII zusammenzuarbeiten,
da dessen Vertreter %(q:ideologisch) und %(q:falsch) argumentierten,
so dass %(q:eine sinnvolle Diskussion mit ihnen nicht möglich) sei.

#nWw: %(q:Montagsgespräche) zum Thema Softwarepatente

#Wve: Auf der Veranstaltung nimmt Montag als Moderator breiten Raum ein,
spielt die Rolle eines Avocatus Diaboli für das BMJ, wobei die Grenzen
zwischen dieser Rolle und seiner Meinung z.T. verschwimmen.

#dfW: Dabei bezeichnet Montag Forderungen nach Nichtpatentierbarkeit
genialer %(q:technischer) Software wie MP3 als unverständlich oder
zumindest undurchsetzbar, s. auch %(es:Blog von Erich Schubert).

#Wrn: Auf der grünen Bundesdelegiertenkonferenz kommt es zu einem
%(bg:Beschluss gegen Softwarepatente), der die Berliner Fraktion zu
Widerstand gegen die Ratsvereinbarung verpflichten soll.  Die geplante
ausführliche Diskussion entfällt von der Tagesordnung, so dass es
nicht gelingt, brennende Fragen zwischen Fraktion und
Parteiöffentlichkeit zu diskutieren.

#sWb: Peter Mühlbauer (Journalist) %(tp:schreibt in Telepolis):
%(q:Problematisch ist, dass auch Parteien, die sich in der
Öffentlichkeit als klare Softwarepatentgegner darstellen,
%(s:keineswegs immun gegen eine %(q:Betreuung) durch die Patentlobby)
sind. So sah man in München beispielsweise den %(s:grünen Abgeordneten
Jerzy Montag), der bereits mit seinen Vorschlägen zur Verschlüsselung
auffiel (vgl. %(ga:Grüner Abgeordneter will Hinterlegung von
Kryptoschlüsseln prüfen)), %(s:von Industrievertretern umringt).)

#Wtn: empörtes Schreibens eines Mitglieds der Grünen an Montag u.a.

#ift: %(q:Wir begrüßen die derzeit stattfindenden europaweiten Aktivitäten
zur Verhinderung von Software-Patenten. Nachdem sich das Europäische
Parlament im vergangenen September - unter maßgeblicher Beteiligung
unserer Europafraktion - dafür ausgesprochen hat, Software-Patente nur
in Ausnahmefällen zu erlauben, beabsichtigt der EU-Rat nun, diesen
Beschluss wieder vollständig rückgängig zu machen.)

#njh: Erstmals erscheint in der grünen Rhetorik die Andeutung,
Softwarepatenten sollten %(q:in Ausnahmefällen) erlaubt sein.

#ctW: Montag nimmt sich als rechtspolitischer Sprecher des Dossiers
Softwarepatent-Richtlinie an -- früher war dies eine Domäne der
Medien- und Wirtschaftspolitiker bei den Grünen

#Wdi: Montag kommt auf der Liste der Grünen in den Bundestag, wird
rechtspolitischer Sprecher

#suM: Webseiten zu Jerzy Montag

#neW: Montags eigene Seite

#rin: Informationsseite des Bundestags

#FWk: FFII Wiki

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatremna.el ;
# mailto: mlhtimport@ffii.de ;
# login: rainbow ;
# passwd: YYYYY ;
# feature: ffiirc ;
# dok: JerzyMontag ;
# txtlang: xx ;
# End: ;

