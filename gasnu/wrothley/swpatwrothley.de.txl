<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Willi Rothley und Softwarepatente

#descr: Willi Rothley, Patentexperte der deutschen sozialdemokratischen Gruppe im Europäischen Parlament, Vizepräsident des Ausschusses für Rechtsangelegenheiten (JURI), hält recht wenig von der Qualität der Gesetzgebungsverfahren in der EU und vertraut lieber den Selbstregulierungsfähigkeiten der Patentgerichtsbarkeit, mit deren Urteilen er recht vertraut ist.  In diesem Sinne hielt Rothley am 16. Juni 2003 eine Rede in JURI, in der er diesen Ausschuss als eine %(q:Wurstmaschine) bezeichnete, die immer-neue überflüssige Gesetze produziert, um sich am Laufen zu halten.  Man kann von Rothley erwarten, dass er seine Einstellung gegenüber parlamentarischen oder sonstigen gesetzgeberischen %(q:Pfuschern) recht unverblümt zum Ausdruck bringt.  Dieser Einstellung treu stimmte er am 24. September 2003 gegen die Richtlinie.  Ob Software patentierbar sein soll oder nicht, ist für Rothley eine zweitrangige Frage.  Im Zweifelsfalle wird sich Rothley an der aktuellen Rechtsprechung, auch der des Europäischen Patentamtes, orientieren und mit Argwohn alle Versuche von Politikern oder Interessenverbänden betrachten, die in irgend einer Weise Regeln setzen wollen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatwrothley ;
# txtlang: de ;
# multlin: t ;
# End: ;

