<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Willi Rothley and Software Patents

#descr: Willi Rothley, patent expert of the german social democratic group in the European Parliament, vice president of the Legal Affairs Commission, is critical of EU lawmaking and instead believes very much in the self-regulatory capability of the patent judiciary.  In this sense, he gave a speech in the Legal Affairs Committee (JURI) on 2003-06-16 where he called JURI a %(q:sausage machine), which produces ever-new superfluous laws just to keep turning.  Rothley can be expected to show an attitude of disdain for parliamentary patent lawmaking in quite blunt ways.  Faithful to this attitude, Rothley voted against the proposed directive.  Whether or not software should be patentable is a secondary consideration for Rothley, but in case of doubt he will usually follow the current patent jurisdiction, including that of the European Patent Office, and look with suspicion at any attempts by politicians or pressure groups to set rules in one way or other.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatwrothley ;
# txtlang: en ;
# multlin: t ;
# End: ;

