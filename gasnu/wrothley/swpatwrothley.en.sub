\begin{subdocument}{swpatwrothley}{Willi Rothley and Software Patents}{http://swpat.ffii.org/players/wrothley/index.en.html}{Workgroup\\swpatag@ffii.org}{Willi Rothley, patent expert of the german social democratic group in the European Parliament, vice president of the Legal Affairs Commission, is critical of EU lawmaking and instead believes very much in the self-regulatory capability of the patent judiciary.  In this sense, he gave a speech in the Legal Affairs Committee (JURI) on 2003-06-16 where he called JURI a ``sausage machine'', which produces ever-new superfluous laws just to keep turning.  Rothley can be expected to show an attitude of disdain for parliamentary patent lawmaking in quite blunt ways.  Faithful to this attitude, Rothley voted against the proposed directive.  Whether or not software should be patentable is a secondary consideration for Rothley, but in case of doubt he will usually go with what the current patent jurisdiction, including that of the European Patent Office, and look with suspicion at any attempts by politicians or pressure groups to set rules in one way or other.}
\begin{itemize}
\item
{\bf {\bf Parliament Accepts Petitions Against Software Patents\footnote{http://swpat.ffii.org/news/03/peti1001/index.en.html}}}

\begin{quote}
At the Hearing of the Petition Committee in the Parliament yesterday evening at 18:00, Anthony Howard, a former employee of the UK Patent Office who is handling the Software Patent Directive Project for DG Internal Market under Frits Bolkestein at the European Commission, asked the Parliament to discard the two petitions against software patents, signed by a quarter million european citizens, which were presented to it.  Very few MEPs attended the evening session, and among those who attended were prominent software patentability advocates from the Legal Affairs Commission (JURI) who vocally supported Howard's call.  However the Petition Committee did not follow their calls.  Acceptance of the petition will mean that the Legal Affairs Commission, which voted against meaningful limits on patentability in June 2003 and was overruled by the plenary vote on 24th of September, will receive another opportunity to explain its position.
\end{quote}
\filbreak

\item
{\bf {\bf Willi Rothley 2003-06-16: JURI is a Sausage Machine\footnote{http://wiki.ael.be/index.php/JuriEnPartFive}}}

\begin{quote}
Willi Rothley MEP (DE, PSE) says that the directive as proposed by the European Commission is poorly crafted and unnecessary and that it should really be sent back to the European Commission.  But JURI keeps rotatint like a sausage machine, turning out one law after another, and we have to make the best out of it.

see also JURI 2003-06-16 Software Patent Speeches\footnote{http://wiki.ael.be/index.php/JuriEnPartNine}, Willi Rothley 2003-06-16: JURI is a Sausage Machine\footnote{http://wiki.ael.be/index.php/JuriEnPartFive}
\end{quote}
\filbreak

\item
{\bf {\bf JURI votes for Fake Limits on Patentability\footnote{http://swpat.ffii.org/news/03/juri0617/index.en.html}}}

\begin{quote}
The European Parliament's Committee for Legal Affairs and the Internal Market (JURI) voted on tuesday morning about a list of proposed amendments to the planned software patent directive.  It was the third and last in a series of committee votes, whose results will be presented to the plenary in early september.  The other two commissions (CULT, ITRE) had opted to more or less clearly exclude software patents.  The JURI rapporteur Arlene McCarthy MEP (UK socialist) also claimed to be aiming for a ``restrictive harmonisation of the status quo'' and ``exclusion of software as such, algorithms and business methods from patentability''.  Yet McCarthy presented a voting list to fellow MEPs which, upon closer look, turns ideas like ``Amazon One-Click Shopping'' into patentable inventions.  McCarthy and her followers rejected all amendment proposals that try to define central terms such as ``technical'' or ``invention'', while supporting some proposals which reinforce the patentability of software, e.g. by making publication of software a direct patent infringment, by stating that ``computer-implemented inventions by their very nature belong to a field of technology'', or by inserting new economic rationales (``self-evident'' need for Europeans to rely on ``patent protection'' in view of ``the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union'') into the recitals.  Most of McCarthy's proposals found a conservative-socialist 2/3 majority (20 of 30 MEPs), whereas most of the proposals from the other committees (CULT = Culture, ITRE = Industry) were rejected.  Study reports commissioned by the Parliament and other EU institutions were disregarded or misquoted, as some of their authors point out (see below).  A few socialists and conservatives voted together with Greens and Left in favor of real limits on patentability (such as the CULT opinion, based on traditional definitions, that ``data processing is not a field of technology'' and that technical invention is about ``use of controllable forces of nature''), but they were overruled by the two largest blocks.  Most MEPs simply followed the voting lists of their ``patent experts'', such as Arlene McCarthy (UK) for the Socialists (PSE) and shadow rapporteur Dr. Joachim Wuermeling (DE) for the Conservatives (EPP).  Both McCarthy and Wuermeling have closely followed the advice of the directive proponents from the European Patent Office (EPO) and the European Commission's Industrial Property Unit (CEC-Indprop, represented by former UK Patent Office employee Anthony Howard) and declined all offers of dialog with software professionals and academia ever since they were nominated rapporteurs in May 2002.
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
% mode: latex ;
% End: ;

