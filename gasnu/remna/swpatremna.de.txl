<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Personen und Softwarepatente

#descr: Wer bei der Auseinandersetzung um Softwarepatente in Europa welche
Rolle spielte.

#aWa: Dr. Joachim Wuermeling MdEP und Softwarepatente

#auh: Joachim Wuermeling, Doktor des Rechts, Mitglied des Europäischen
Parlaments, CSU, Schattenberichterstatter der Europäischen Volkspartei
(EVP) über die Softwarepatentrichtlinie und gelegentlich über andere
Patentangelegenheiten, setzte sich gegen alles ein, was die
Patentierbarkeit irgendwie beschränken könnte (z.B.
Interoperabilitätsprivileg) und für alles, was sie erweitert (z.B. die
von der Eur. Kommission nicht vorgesehenen Programmansprüche).  Die
meisten EVP-Abgeordneten folgen Wuermeling in diesen Fragen blind. 
Viele Kritiker haben mit Wuermeling vergeblich den Dialog gesucht. 
Während Wuermeling für Gespräche bislang unzugänglich geblieben ist,
sucht er aktiv den Kontakt zur Presse, um sich als Gegner von Patenten
auf %(q:reine Software und Geschäftsmethoden) auszugeben.   Den in der
Öffentlichkeit entstandenen gegenteiligen Eindruck führt er durchweg
auf %(q:unbegründete Ängste der Opensource-Bewegung) zurück. 
Wuermeling brachte im Rechtsausschuss einen wirkungslosen
Änderungsantrag (Erwägungsgrund 13d) ein, demzufolge ein
Patentanspruch sich nur auf ein bestimmtes Produkt beziehen und
Algorithmen nicht erfassen kann.   Wuermeling scheint tatsächlich die
verharmlosenden Erklärungen zu glauben, die ihm einige
Verbandslobbyisten liefern.  Die selben Verbandslobbyisten machen sich
in internen Schriftstücken über Wuermelings fehlendes Verständnis für
elementare Zusammenhänge des Patentwesens lustig.  Wuermelings
Unwissen und Unzugänglichkeit hängen möglicherweise damit zusammen,
dass er mit den Arbeiten für den Verfassungskonvent überlastet ist.

#iup: Angelika Niebler und Softwarepatente

#iWn: Als Mitglied des Industrie- und Rechtsausschusses hat Angelika Niebler
sich für die Einführung von Programmansprüchen stark gemacht und
konsequent gegen alle Änderngsanträge argumentiert und gestimmt, die
zu einer Begrenzung der Patentierbarkeit hätten führen können.  Sie
erwies sich als rechte Hand ihres Kollegen Joachim Wuermeling bei der
durchsetzung grenzenloser Patentierbarkeit im Sinne der Forderungen
der Patentanwälte von BDI und Bitkom.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatremna ;
# txtlang: de ;
# multlin: t ;
# End: ;

