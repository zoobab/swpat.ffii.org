<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Individuals and Software Patents

#descr: Who played which role in the debate about software patents in Europe
and elsewhere

#aWa: Dr. Joachim Wuermeling MEP and Software Patents

#auh: Joachim Wuermeling, doctor of law, member of the European Parliament
(MEP) for Bavaria's Christian Social Union (CSU), shadow rapporteur of
the European People's Party (EPP) on the software patent directive,
opposed everyting that could somehow limit patentability (e.g.
interoperability privilege) and advocated everything that extends it
beyond the European Commission's wishes (e.g. program claims).  Most
EPP colleagues tend to blindly follow their shadow rapporteur.  Many
critics have in vain sought dialogue with Wuermeling.  Wuermeling has
however actively contacted the press in order to present himself as an
opponent of %(q:patents on pure software and business methods) and to
ascribe contrary impressions of the public to %(q:misperceptions of
the opensource lobby).  In the JURI discussions of May 2003,
Wuermeling tabled a pseudo-restrictive amendment (recital 13d),
according to which a patent claim is limited to a specific product an
may not encompass underlying algorithms.   Some of the corporate
patent lawyers on whom Wuermeling is relying have been laughing about
Wuermeling's lack of basic knowledge about the subject matter on which
he is legislating.  One explanation for this lack of knowledge and
unwillingness to meet critics is that Wuermeling is extremely busy
with other subjects, such as the EU Constitution Convention.

#iup: Angelika Niebler und Softwarepatente

#iWn: Als Mitglied des Industrie- und Rechtsausschusses hat Angelika Niebler
sich für die Einführung von Programmansprüchen stark gemacht und
konsequent gegen alle Änderngsanträge argumentiert und gestimmt, die
zu einer Begrenzung der Patentierbarkeit hätten führen können.  Sie
erwies sich als rechte Hand ihres Kollegen Joachim Wuermeling bei der
durchsetzung grenzenloser Patentierbarkeit im Sinne der Forderungen
der Patentanwälte von BDI und Bitkom.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatremna ;
# txtlang: en ;
# multlin: t ;
# End: ;

