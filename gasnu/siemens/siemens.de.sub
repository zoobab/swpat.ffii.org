\begin{subdocument}{swpatsiemens}{Siemens und Logikpatente}{http://swpat.ffii.org/akteure/siemens/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Siemens war insbesondere in Deutschland schon immer an der Spitze der Patentbewegung.  Dennoch behaupten die Vertreter der Siemens-Patentabteilung, die in Verb\"{a}nden und regierungsnahen Organisationen f\"{u}r die Ausweitung der Patentierbarkeit eintreten, nicht ernsthaft, dass die Patentflut dem technischen Fortschritt diene oder f\"{u}r das Gesch\"{a}ftsmodell von Siemens notwendig sei.  Sie betonen lediglich, dass Europa den von den USA geschaffenen globalen Regeln zu folgen habe.  Nicht wenige Verantwortungstr\"{a}ger im technischen und gesch\"{a}ftlichen Bereich von Siemens st\"{o}hnen derweil \"{u}ber die bremsende Wirkung, welche von der Patentflut ausgeht und die Branche ebenso wie die gesch\"{a}ftlichen Aktivit\"{a}ten von Siemens oft schmerzhaft beeintr\"{a}chtigt.  Aber diese Leute sind nicht berechtigt, im Namen von Siemens \"{u}ber ``Patentfragen'' sprechen.}
\begin{sect}{intro}{Einf\"{u}hrung}
In the late 80s and early 90s, Arno K\"{o}rber, head of the patent department of Siemens, successfully installed a patent incentive system at Siemens which made the patenting numbers soar to the sky and helped leveraging the economy-of-scale advantage of Siemens as a large company.

Many at Siemens are used to the patent system and associate success experiences with it.  They are afraid that the continued informatisation of many problems will erode the advantages which they have drawn from the patent game.  Basically their approach is conservative: they want to repeat past success experiences.  In a similar spirit, they are afraid of the profound change in the patent system which this implies and therefore keen to reassure everybody that they or course ``do not want business method patents''.  Yet this does not prevent the Siemens patent department from applying for lots of business method patents and trivial functionality patents (see tabular listing below), nor does it prevent the patent policy representatives of Siemens from strongly endorsing and supporting all legislative initiatives which make such patents obligatory.  The fear of losing the patentopoly success experience is greater than the fear of patent inflation.

A newer current of opinion among engineers and managers at Siemens thinks more sceptically about the patent process.  Younger managers have told us that ``patents are a dragging our foot and not needed for protecting investments in the telecommunication sector''.  This may be related to their experience of negotiating with QualComm and other small companies who do not develop products but only patents and who assert these patents aggressively against big players such as Siemens.  Many Siemens employees have signed the Petition for a Free Europe without Software Patents\footnote{http://petition.eurolinux.org/index.de.html}.  Yet this current has so far not been able to influence the presentation of Siemens on the political stage.

Zu den hinsichtlich ihrer Patentpolitik von der Siemens-Patentabteilung stark beeinflussten Verb\"{a}nden geh\"{o}ren u.a. ZVEI, Bitkom und BDI/Unice.  Arno K\"{o}rber war der Autor einer ZVEI-Stellungnahme f\"{u}r Softwarepatente im Sommer 1999.  K\"{o}rber und andere Siemens-Patentstrategen sind auch in Beir\"{a}ten und Gremien der Patentwelt (EPA, AIPPI etc) aktiv.  Siemens verf\"{u}gt auch in Br\"{u}ssel \"{u}ber st\"{a}ndige Vertreter, welche u.a. Positionen der Siemens-Patentabteilung transportieren.
\end{sect}\begin{sect}{epat}{E-Patente von Siemens beim EPA}
\includefile{siemens-epatents.html}
\end{sect}\begin{sect}{links}{Kommentierte Verweise}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Siemens Patentabteilung 2002:  Patente sind sexy, strategischer Einsatz vor allem in IT entscheidend\footnote{http://www.vdi-nachrichten.com/aus\_der\_redaktion/akt\_ausg\_detail.asp?id=8868}}}

\begin{quote}
Mitglieder des Vorstandes und der Patentabteilung berichten stolz \"{u}ber die Fr\"{u}chte der systematischen Patentpolitik von Siemens: 16 Patente pro Tag, USA-Swpat zukunftsentscheidend, Qualit\"{a}t wird nach Blockiereffekt bemessen, Mitarbeiter nehmen an einem Pr\"{a}mien- und Optionssystem teil, Siemens-Abteilungen verlangen voneinander Lizenzgeb\"{u}hren, Prozesskosten doppelt so bedeutend wie vor 10 Jahren, Image des friedlichen Riesen abgelegt, inzwischen aggressives Vorgehen der nx100 Mann starken Patentabteilung.  \texmath{>}6000 Patente pro Jahr in Europa, 60\percent{} Software.  Patentierungsschwerpunkt in USA, da Europa weniger strategisch bedeutend und weniger swpat-freudig.
\end{quote}
\filbreak

\item
{\bf {\bf Siemens 2001-3/4: Potente Waffen, US-System wegweisend\footnote{}}}

\begin{quote}
Der neue Patentchef von Siemens erkl\"{a}rt in einem Interview mit einem Webmagazin von Siemens, dass Patente ``m\"{a}chtige Waffen'' sind, und dass Siemens diese Waffen k\"{u}nftig verst\"{a}rkt im Bereich der Software sammeln und einsetzen will: \begin{quote}
{\it Software-Patente haben einen steigenden Anteil in unserem Portfolio. Einige Bereiche wie Automation \& Drives und Medical Solutions haben f\"{u}r derartige Patente besondere Initiativen gestartet. Die Erstanmeldung dieser Schutzrechte erfolgt dabei zunehmend in USA, da hier der Rechtsrahmen f\"{u}r Software-Patente wegweisend ist. Dieser Trend gilt in noch st\"{a}rkerem Ma{\ss} bei Patenten f\"{u}r so genannte ``Gesch\"{a}ftsmodelle'' oder vereinfacht}

{\it  Patente f\"{u}r Electronic-Business-L\"{o}sungen. Hierf\"{u}r haben wir eine weltweite Koordinierungs- und Beratungsstelle eingerichtet.}
\end{quote}.
\end{quote}
\filbreak

\item
{\bf {\bf Ulrich Eberl 1998: Kalter Krieg um neue Erfindungen\footnote{http://w3.siemens.de/newworld/PND/PNDG/PNDGA/PNDGAD/pndgad5.htm}}}

\begin{quote}
Ein Siemens-Mitarbeiter berichtet \"{u}ber die strategische Bedeutung von Patenten f\"{u}r Siemens und nennt Zahlen:  150 Patentexperten, 100K Patente, 1G Lizenzeinnahmen/Jahr, Strategien, Schlachten, Hinterhalte, ...
\end{quote}
\filbreak

\item
{\bf {\bf Arno Koerber: What do Software Patents mean to European Industry\footnote{http://www.patent.gov.uk/about/ippd/softpat//1050.htm}}}

\begin{quote}
Rede des Patent-Chefstrategen von Siemens auf einer von der Europ\"{a}ischen Kommission gef\"{o}rderten Kundgebung f\"{u}r Softwarepatente in London 1998.  Erkl\"{a}rt, dass Swpat notwendig seien weil (a) Softwareideen von strategischer Bedeutung sind (b) alles strategische besitzbar sein muss (c) das Urheberrecht keinen Ideenbesitz erlaubt.  Abgesehen von diesen falschen Voraussetzungen enth\"{a}lt die Rede allerlei interessante Angaben und \"{U}berlegungen.
\end{quote}
\filbreak

\item
{\bf {\bf Pollmeier GmbH: Bedenken eines Elektronik-Unternehmens gegen Logikpatente\footnote{http://www.esr-pollmeier.de/swpat/}}}

\begin{quote}
enth\"{a}lt allerlei Info \"{u}ber die Pateniterungsaktivit\"{a}ten von Siemens im Bereich der Automatisierung
\end{quote}
\filbreak

\item
{\bf {\bf BMBF bietet Softwarepatent-Brosch\"{u}re der Patentabteilung von Siemens an\footnote{http://www.patente.bmbf.de/inform/down/software.pdf}}}

\begin{quote}
Im Namen des BMBF pl\"{a}diert die Patentabteilung von Siemens f\"{u}r die Patentierbarkeit von Software und regt Hochschulen an, Softwarepatente anzumelden.
\end{quote}
\filbreak

\item
{\bf {\bf Fraunhofer/ISI 2001: \"{O}konomisch-Rechtliche Studie \"{u}ber Softwarepatente\footnote{http://swpat.ffii.org/papiere/boch97-koerber/index.de.html}}}

\begin{quote}
Der Autor, Arno K\"{o}rber, geb 1934, war bis Oktober 1997 Leiter der Patentabteilung der Siemens AG und ist seitdem Repr\"{a}sentant des Hauses Siemens im gewerblichen Rechtschutz gegen\"{u}ber staatlichen und nichtstaatlichen Einrichtungen und Organisationen.  Er nimmt Vorstandsfunktionen in zahlreichen Verbandsgremien wahr.  K\"{o}rber blickt auf die letzten Jahrzehnte zur\"{u}ck. Bis vor kurzem entfalteten Patente ihre Wirkung vor allem in forschungsintensiven Gebieten wie der Chemie und Pharmazie.  In diesen Gebieten ist der Ausschluss von Wettbewerbern sowohl wichtig als auch, dank Stoffpatenten, leicht durchf\"{u}hrbar.  In Elektronik und Informationstechnik herrschte Patentfrieden.  U.a. im Interesse der Interoperabilit\"{a}t wurden Patente nur defensiv eingesetzt.  Mit der Umorientierung des amerikanischen Patentsystems nach Einrichtung des CAFC \"{a}nderte sich dies jedoch zusehends.  Heute ist es unerl\"{a}sslich f\"{u}r Unternehmen, die beider Verabschiedung von informatischen Standards ein W\"{o}rtchen mitreden wollen, \"{u}ber ein geeignetes Patentportfolio zu verf\"{u}gen.  Um 1990 stagnierte die Zahl der Patentanmeldungen der Siemens-Mitarbeiter bei ca 2000 pro Jahr.  Das lag u.a. daran, dass die Entwickler glaubten, Softwarel\"{o}sungen seien in Europa nicht patentierbar und in diesem Glauben von konservativen Patentgerichten und Patent\"{a}mtern best\"{a}rkt wurden.  Daher leistete Siemens einen aktiven Beitrag zur Rechtsfortbildung.  Ferner wurden Pr\"{a}miensysteme f\"{u}r Patentanmelder eingef\"{u}hrt.  In wenigen Jahren verdoppelten sich die Anmeldezahlen.
\end{quote}
\filbreak

\item
{\bf {\bf ZVEI und Softwarepatente\footnote{http://swpat.ffii.org/akteure/zvei/index.de.html}}}

\begin{quote}
Der Zentralverband der Elektronischen Industrie (ZVEI) fungierte lange Zeit als Sprachrohr der Patentabteilung von Siemens.  Seine Stellungnahmen wurden z.T. direkt von deren Chef Arno K\"{o}rber formuliert und allenfalls mit einer Arbeitsgruppe der Patentjuristen, niemals aber mit Ingenieuren oder Software-Entwicklern besprochen.  Der ZVEI forderte dabei eine Neuregulierung der Patentierbarkeit nach US-Vorbild oder gem\"{a}{\ss} der dem weitgehend entsprechenden neuesten Praxis des Europ\"{a}ischen Patentamtes (EPA).  Auch nach der Ver\"{o}ffentlichung des Swpat-Richtlinienentwurfs der Europ\"{a}ischen Kommission forderte der ZVEI eine noch vorbehaltlosere Bejahung der grenzenlosen Patentierbarkeit als in diesem Entwurf vorgesehen war.  Nachdem im Fr\"{u}hjahr 2002 einige Mitglieder in nicht-juristischen Arbeitskreisen Kritik an der patentlastigen Verbandsposition ge\"{a}u{\ss}ert hatten, begann der ZVEI, seine Position neu zu \"{u}berpr\"{u}fen.  Die Fachpresse sprach von ``Zur\"{u}ckrudern''.
\end{quote}
\filbreak

\item
{\bf {\bf Politische \"{O}konomie des Patentwesens: die Mechanismen der Patentinflation\footnote{http://swpat.ffii.org/analyse/tisna/index.de.html}}}

\begin{quote}
In den letzten 200 Jahren hat sich das Patentsystem kontinuierlich ausgeweitet.  Diese Expansion ist nicht das Ergebnis einer planm\"{a}{\ss}igen Wirtschaftspolitik sondern vielmehr eines selbstverst\"{a}rkenden Mechanimsusses, den man etwa der Geldwert-Inflation oder dem R\"{u}stungswettlauf vergleichen kann.  Dieser Artikel analysiert die Mechanismen der Patentinflation und verfolgt ihre Entwicklung.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: latex ;
% End: ;

