<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Siemens und Logikpatente

#descr: Siemens hält etwa 50% der am Europäischen Patentamt angemeldeten
deutschen Softwarepatente.  Die Patentabteilung von Siemens
beschäftigt in Deutschland 200 Mitarbeiter.  Sie bestimmt maßgeblich
die Verlautbarungen des ZVEI und anderer Verbände mit und haben in den
90er Jahren beim BGH Grundsatzurteile zugunsten von Softwarepatenten
erwirkt.  Siemens-Vorstand Heinrich von Pierer hat sich öffentlich für
die Aufweichung des Richtlinienvorschlages des Europäischen Parlaments
stark gemacht und durch Briefe an den Bundeskanzler u.a. dafür
gesorgt, dass im Gegenvorschlag des Rates auch Rechenregeln und
Geschäftsmethoden wieder patentierbar werden.

#eeh: E-Patente von Siemens beim EPA

#bto: In the late 80s and early 90s, Arno Körber, head of the patent
department of Siemens, successfully installed a patent incentive
system at Siemens which made the patenting numbers soar to the sky and
helped leveraging the economy-of-scale advantage of Siemens as a large
company.

#Was: Many at Siemens are used to the patent system and associate success
experiences with it.  They are afraid that the continued
informatisation of many problems will erode the advantages which they
have drawn from the patent game.  Basically their approach is
conservative: they want to repeat past success experiences.  In a
similar spirit, they are afraid of the profound change in the patent
system which this implies and therefore keen to reassure everybody
that they or course %(q:do not want business method patents).  Yet
this does not prevent the Siemens patent department from applying for
lots of business method patents and trivial functionality patents (see
tabular listing below), nor does it prevent the patent policy
representatives of Siemens from strongly endorsing and supporting all
legislative initiatives which make such patents obligatory.  The fear
of losing the patentopoly success experience is greater than the fear
of patent inflation.

#ttW: A newer current of opinion among engineers and managers at Siemens
thinks more sceptically about the patent process.  Younger managers
have told us that %(q:patents are a dragging our foot and not needed
for protecting investments in the telecommunication sector).  This may
be related to their experience of negotiating with QualComm and other
small companies who do not develop products but only patents and who
assert these patents aggressively against big players such as Siemens.
 Many Siemens employees have signed the %(ep:Petition for a Free
Europe without Software Patents).  Yet this current has so far not
been able to influence the presentation of Siemens on the political
stage.

#ZWr: Zu den hinsichtlich ihrer Patentpolitik von der
Siemens-Patentabteilung stark beeinflussten Verbänden gehören u.a.
ZVEI, Bitkom und BDI/Unice.  Arno Körber war der Autor einer
ZVEI-Stellungnahme für Softwarepatente im Sommer 1999.  Körber und
andere Siemens-Patentstrategen sind auch in Beiräten und Gremien der
Patentwelt (EPA, AIPPI etc) aktiv.  Siemens verfügt auch in Brüssel
über ständige Vertreter, welche u.a. Positionen der
Siemens-Patentabteilung transportieren.

#yte: Siemens has for many years cultivated very close strategic alliance
with %(MS), another firm with a keen interested in establishing
software patents.

#stt: Siemens 2001-3/4: Potente Waffen, US-System wegweisend

#Wrl: Der neue Patentchef von Siemens erklärt in einem Interview mit einem
Webmagazin von Siemens, dass Patente %(q:mächtige Waffen) sind, und
dass Siemens diese Waffen künftig verstärkt im Bereich der Software
sammeln und einsetzen will: %(bc:Software-Patente haben einen
steigenden Anteil in unserem Portfolio. Einige Bereiche wie Automation
& Drives und Medical Solutions haben für derartige Patente besondere
Initiativen gestartet. Die Erstanmeldung dieser Schutzrechte erfolgt
dabei zunehmend in USA, da hier der Rechtsrahmen für Software-Patente
wegweisend ist. Dieser Trend gilt in noch stärkerem Maß bei Patenten
für so genannte %(q:Geschäftsmodelle) oder vereinfacht: Patente für
Electronic-Business-Lösungen. Hierfür haben wir eine weltweite
Koordinierungs- und Beratungsstelle eingerichtet.).

#UxW: Ulrich Eberl 1998: Kalter Krieg um neue Erfindungen

#EKn: Ein Siemens-Mitarbeiter berichtet über die strategische Bedeutung von
Patenten für Siemens und nennt Zahlen:  150 Patentexperten, 100K
Patente, 1G Lizenzeinnahmen/Jahr, Strategien, Schlachten, Hinterhalte,
...

#Stn: Siemens Patentabteilung 2002:  Patente sind sexy, strategischer
Einsatz vor allem in IT entscheidend

#Mth: Mitglieder des Vorstandes und der Patentabteilung berichten stolz über
die Früchte der systematischen Patentpolitik von Siemens: 16 Patente
pro Tag, USA-Swpat zukunftsentscheidend, Qualität wird nach
Blockiereffekt bemessen, Mitarbeiter nehmen an einem Prämien- und
Optionssystem teil, Siemens-Abteilungen verlangen voneinander
Lizenzgebühren, Prozesskosten doppelt so bedeutend wie vor 10 Jahren,
Image des friedlichen Riesen abgelegt, inzwischen aggressives Vorgehen
der nx100 Mann starken Patentabteilung.  >6000 Patente pro Jahr in
Europa, 60% Software.  Patentierungsschwerpunkt in USA, da Europa
weniger strategisch bedeutend und weniger swpat-freudig.

#Zfa: ZVEI fordert umfassenden Software-Patentschutz

#Glw: Pressebericht über eine ZVEI-Stellungnahme für Softwarepatente.  Diese
Stellungnahme wurde weitgehend ohne Kenntnis der Vereinsmitglieder vom
Siemens-Patentchef Arno Körber verfasst, wie wir später durch
Telefonate herausfanden.

#ZWe: ZVEI für Softwarepatente

#ZWw: Pressebericht über eine ZVEI-Stellungnahme für Softwarepatente.  Diese
Stellungnahme wurde weitgehend ohne Kenntnis der Vereinsmitglieder vom
Siemens-Patentchef Arno Körber verfasst, wie wir später durch
Telefonate herausfanden.

#AWn: Arno Koerber: What do Software Patents mean to European Industry

#sal: Rede des Patent-Chefstrategen von Siemens auf einer von der
Europäischen Kommission geförderten Kundgebung für Softwarepatente in
London 1998.  Erklärt, dass Swpat notwendig seien weil (a)
Softwareideen von strategischer Bedeutung sind (b) alles strategische
besitzbar sein muss (c) das Urheberrecht keinen Ideenbesitz erlaubt. 
Abgesehen von diesen falschen Voraussetzungen enthält die Rede
allerlei interessante Angaben und Überlegungen.

#Lmg: LiVe argumentiert gegen ZVEI

#CnW: Kritische Antwort an ZVEI durch Linux-Verband

#leu: enthält allerlei Info über die Pateniterungsaktivitäten von Siemens im
Bereich der Automatisierung

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsiemens ;
# txtlang: de ;
# multlin: t ;
# End: ;

