<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: PA Axel Horns und Softwarepatente

#descr: Ein in München lebender Patentanwalt mit Spezialgebiet, Software, Prozesssteuerung, Telekommunikation u.ä., beredter Apologet der grenzenlosen Patentierbarkeit.  Horns ist der tonangebende Mitverfasser einer vom BMWi in Auftrag gegebenen Studie über Softwarepatente.  Er meint, der Ausschluss der %(q:Pläne und Regeln für gedankliche und geschäftliche Tätigkeit sowie Programme für Datenverarbeitungsanlagen) gemäß Art 52 EPÜ sei %(q:Begriffsschrott), ein %(q:Missgriff des Gesetzgebers), keineswegs als %(q:Bollwerk gegen Softwarepatente) geeignet: die Erfindung sei immer das, was im Anspruch geschrieben steht, und solange dort von einem energieverbrauchenden Vorgang die Rede sei, handle es sich um eine technische Erfindung und niemals um eine Rechenregel als solche.  Horns meint allerdings, für die freie/quelloffene Software müsse etwas getan werden: Quelltext solle wie eine wissenschaftliche Publikation behandelt werden: nicht Autoren sondern Anwender sollten das Patentverletzungsrisiko tragen.  Horns argumentiert in diesem Sinne in zahlreichen Zeitschriftenartikeln ebenso wie auf seiner Webestätte www.ipjur.com, wo er umfangreiche Dokumentation aus dem Bereich des Immaterialgüterrechts bereit stellt.

#Aus: Axel Horns verbringt etwa die Hälfte seiner Zeit mit Netzaktivismus, öffentlichen Veranstaltungen und Publikationen in juristischen Zeitschriften und den damit, für seine Mandanten in der Kanzlei KSNH Patente, hauptsächlich im Bereich der Software, am EPA und DPMA anzumelden.  Er wurde vermutlich um 1960 geboren und studierte in den 80er Jahren Patentrecht, um als Vertreter vor dem Deutschen Patentamt und später vor dem Europäischen Patentamt zugelassen zu werden.

#IRW: PA Horns on Software Patents

#Eee: Eine schöne und informative Darstellung vieler Fragen des gegenwärtigen Immaterialgüterrechts.  Leider ist PA Horns zwar einerseits methodologisch streng und auf Korrektheit bedacht, andererseits aber in gewisse Ideen wie z.B. die der unbegrenzten Patentierbarkeit unheilbar verliebt.  Er verbreitet beständig Falschinformationen über Art 52 EPÜ und seine Geschichte.  Er weigert sich, auf %(lh:unsere Kritik) oder andere relevante Gegenpositionen zu verweisen und hält an Irrtümern fest, die er in offenen Diskussionen nie in schlüssiger Weise verteidigen konnte.

#KAl: KSNH: Axel H. Horns

#Iic: Info der Patentanwaltskanzlei übere ihren Mitarbeiter, der Fachkenntnisse in Physik, Elektronik und Software einbringt.

#Pin: PA Horns 2002: RA Klein kann offenbar Patentansprüche nicht lesen

#HWW: Formaljuristische Scheinargumente von Horns, die seiner Softwarepatente-Apologetik im Aufbau und Stil sehr ähneln, mit Widerlegung von Hartmut Pilch.

#Hgc: Horns 2002-07-22: Energieverbrauch im Anspruchswortlaut begründet Technizität, Computer-Implemented Invention (CII) ist nicht Programm als solches

#Amd: Auf den Vorwurf hin, er führe juristische Laien in die Irre, indem er höchst zweifelhafte persönliche Rechtsauffassungen mit Recht und Gesetz gleichsetze und als eine Art Grundwissen ausgebe, räumt Horns ein, dass er sich in der im immer Einklang mit der Rechtsprechung befinde: %(bc:Beim EPÜ teile ich in der Tat die Auffassung der Beschwerdekammern nicht, dass der Energieumsatz einer CPU bei einer CII noch nicht für %(q:Technizität) ausreichen soll. Das EPA begründet das damit, dass bekannte Geschäftsmethoden nicht durch %(q:Portierung) auf einen Rechner patentfähig werden dürfen. Im Ergebnis ist das richtig, aber ich vertrete doch sehr stark die Meinung, dass dies eine Frage der %(q:Erfindungshöhe) und nicht der %(q:Technizität) sein sollte.)  Hiermit sagt Horns, dass neuartige Geschäftsmethoden patentierbar sein müssen, sofern bei ihrer Durchführung Energie verbraucht wird.

#M5i: Uecker 2002-07-23: Musikinstrument-Implementierte Erfindungen (MII) sind nicht ästhetische Formschöpfungen als solche (1)

#Efz: Eine parodierende Antwort auf den Hornsschen Begriff der Technischen Erfindung.  Nach Hornsscher Logik könnte man auch musikalische Ideen patentieren. Es sind nämlich ganz eindeutig musikinstrument-implementierte Erfindungen.  Technischer Effekt ist die Schallerzeugung.

#Wxr: Wenning 2002-07-23: Spezialregeln für Software möglich

#TuW: Technische Problemlösungen z.B. bei Rolltreppen lassen sich sehr wohl von reiner Informationsverarbeitung (Programmen als solchen) unterscheiden, und der Unterschied ist auch volkswirtschaftlich von Bedeutung. Während Patente auf Rolltreppentechnik in dem kleinen Kreis, den sie betreffen, akzeptiert sind, schaffen Patente auf Rechenregeln einer großen Öffentlichkeit Probleme, ohne welche zu lösen.  Rigo Wenning, der beim W3C als Jurist arbeitet, meint, Software hinreichend leicht von technischen Vorgängen abgrenzbar.  Da die generelle Ablehnung von Algorithmen-Eigentum nicht konsensfähig sei, solle man Spezialregeln für Algorithmen anstreben.  Den von PA Horns über ein Regierungsgutachten in die Diskussion geworfenen %(q:Vorschlag, bei OpenSource die Verwender zu belasten), hält Wenning hingegen für ein %(q:Herumdoktern an den Symptomen).

#L5s: Donnerhacke 2002-07-25: unakzeptable formalistische Argumentation von Horns

#Hpb: Axel Horns hatte das Europäische Patentamt gegen den Vorwurf der Rechtsbeugung in Schutz genommen.  Nach tagelanger Beobachtung der darauf folgenden Diskussion kritisiert der Kryptograph und Fitug-Mitinitiator die formalistische Argumentationstaktik von Horns:  %(q:Bei der Diskussion um Abstrakta dem Gegenüber abzuverlangen, auf dem eigenen Gebiet so konkret zu werden, daß man anhand von Spezialdetails weitermachen kann, zeigt, daß der abstrakte Grundvorwurf zutrifft.)  Er beanstandet damit insbesondere, dass Horns Ücker aufgefordert hatte, die fraglichen %(q:musikinstrument-implementierten Erfindungen) in Form von Patentansprüchen darzustellen, damit man darüber reden könne.

#mka: Horns 2003/09: Softwarepatentkritiker sind verkappte Kapitalismuskritiker

#Wnm: Seit Sommer 2002 ist PA Horns bemüht, die Patentierbarkeit von Programmlogik als unausweichliche Konsequenz des Kapitalismus im Informationszeitalter und Patentkritiker als Kapiatalismuskritiker darzustellen.  In dieser Forums-Antwort meint Horns, einen solchen Wunschgegner gefunden zu haben.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpathorns ;
# txtlang: de ;
# multlin: t ;
# End: ;

