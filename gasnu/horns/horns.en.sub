\begin{subdocument}{swpathorns}{PA Axel H. Horns and Software Patents}{http://swpat.ffii.org/players/horns/index.en.html}{Workgroup\\swpatag@ffii.org}{A Munich-based patent attorney specialising on software, process control, telecommunications and the like, vociferous supporter of unlimited patentability.  Horns is the main author of a German government-ordered study on the subject.  He argues that Art 52 EPC is ill-defined and practically meaningless: not meant to limit patentability but only to limit claim wording: the invention is what is written in the claims: as long as the claims describe an engergy-consuming process, the invention is technical.  Horns however feels that something should be done to accomodate open source software: source code should be treated like a scientific publication: not authors but users should bear the risk of patent infringement.  Horns proposes this in various publications, including his ipjur.com website, an ambitious collection of immaterial property related documentation.}
\begin{center}
\begin{tabular}{|C{44}|C{44}|}
\hline
\mbox{\pdfimage {/ul/sig/srv/res/www/ffii/swpat/players/horns/horns.png}} & Axel Horns spends about half of his time on Net activism, public lectures and journal publications and the rest on obtaining patents, mainly in the field of software, at the EPO and DE-PTO for his customers at KSNH.  He must have been born around 1960 and studied patent law in the 80s to become a registered patent representative before the DE-PTO and later before the EPO.\\\hline
\end{tabular}
\end{center}

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf KSNH: Axel H. Horns\footnote{http://www.ksnh.de/beruns/bio/horns.html}}}

\begin{quote}
Info by patent lawyer company about their member, a specialist in physics, electronics and software.  Contains a photo.
\end{quote}
\filbreak

\item
{\bf {\bf Security in Information Technology and Patent Protection for Software Products -- Expert Opinion by Lutterbeck et al written at the order of the German Ministery of Economics and Technology\footnote{http://swpat.ffii.org/papers/bmwi-luhoge00/index.en.html}}}

\begin{quote}
Prof. Lutterbeck of Berlin Technical University, his assistant Robert Gehring and Axel Horns, patent lawyer in Munich, figuring under the name of ``Internet Governance Research Group'', received an order from the German Ministery of Economics and Technology in late summer of 2000 to work out this ``short expert opinion'' which was published in December 2000.  A large part of the 166 pages is dedicated to reiterating the well-known legal opinion of Horns.  Horns states that Art 52 EPC was a misconception from the beginning, and that patent law will be seriously impaired unless any innovation that is implemented through a computer is patentable.  However he warns that software patents can have a very negative impact on open source software and proposes that patent law at least in Germany should be amended in such a way that the publication and transmission of source code does not violate the law, even if the excecution of object code on a computer does.
\end{quote}
\filbreak

\item
{\bf {\bf PA Horns on Software Patents\footnote{http://www.ipjur.com/01.php3}}}

\begin{quote}
A nice and [des]informative presenation of many current law issues. Unfortunately PA Horns, although on the whole quite methodologically strict and correct, is in love with the idea of unlimited patentability, and continues to prominently propagate fallacies about the European Patent Convention and its history.  In general he likes to use formalistic, sometimes quite idiosyncratic interpretation of law in order to refute non-lawyer criticism of the patent system.  Using this lawyer language he often succedes from deterring the critics.  When the fallacy is pointed out, he withdraws from the debate without however changing his propaganda on the Web.
\end{quote}
\filbreak

\item
{\bf {\bf Horns 2000: Some Observations on the Controversy on ``Software Patents''\footnote{http://swpat.ffii.org/papers/horns00/index.en.html}}}

\begin{quote}
A German software patent attorney calls on his colleagues to lobby for the survival of their profession which he sees under attack by a coalition of neoliberals and leftists riding on a wave of growing popularity of open source software among politicians in Europe.   In particular the people from FFII and Eurolinux have successfully spread myths about the meaning of Art 52 EPC and proposed restrictions on patentability which would be disastrous for the patent system, because they would relegate it to a marginal role in information society, Horns warns.  If patent lawyers continue to be complacent about this development and to rely solely on their traditional means of paper based communication, they risk losing control of the legislative process.  Horns warns against underestimating the political potential of the free software movement.  It strikes a resonating chord with politicians and patent attorneys should therefore try to make peace with this movement and instead establish the patent system in other parts of the software industry.   In particular  they should try to build an economic rationale for the patent system in the area of embedded software.  Much of the argumentation of Horns is built on treating law texts in a formalistic way (e.g. putting ``software patents'' and ``as such'' in double quotes, arguing that Art 52 is about claim language rather than about inventions, quoting an overwhelming mass of more or less irrelevant law sources while suppressing relevant ones), as well as on historical myths about the EPC and on false assertions about FFII/Eurolinux.
\end{quote}
\filbreak

\item
{\bf {\bf PA Horns 2002: RA Klein kann offenbar Patentanspr\"{u}che nicht lesen\footnote{http://lists.ffii.org/archive/mails/swpat/2002/Feb/0048.html}}}

\begin{quote}
Horns trying to debunk patent criticism from a speaker of the German farmers' association as founded on a misunderstanding of legal concepts.  Very similar in style and structure to Horns' software patent discourse.  With a detailed refutation from Hartmut Pilch.
\end{quote}
\filbreak

\item
{\bf {\bf Horns 2002-07-22: Energieverbrauch im Anspruchswortlaut begr\"{u}ndet Technizit\"{a}t, Computer-Implemented Invention (CII) ist nicht Programm als solches\footnote{http://www.fitug.de/debate/0207/msg00363.html}}}

\begin{quote}
Auf den Vorwurf hin, er f\"{u}hre juristische Laien in die Irre, indem er h\"{o}chst zweifelhafte pers\"{o}nliche Rechtsauffassungen mit Recht und Gesetz gleichsetze und als eine Art Grundwissen ausgebe, r\"{a}umt Horns ein, dass er sich in der im immer Einklang mit der Rechtsprechung befinde: \begin{quote}
{\it Beim EP\"{U} teile ich in der Tat die Auffassung der Beschwerdekammern nicht, dass der Energieumsatz einer CPU bei einer CII noch nicht f\"{u}r ``Technizit\"{a}t'' ausreichen soll. Das EPA begr\"{u}ndet das damit, dass bekannte Gesch\"{a}ftsmethoden nicht durch ``Portierung'' auf einen Rechner patentf\"{a}hig werden d\"{u}rfen. Im Ergebnis ist das richtig, aber ich vertrete doch sehr stark die Meinung, dass dies eine Frage der ``Erfindungsh\"{o}he'' und nicht der ``Technizit\"{a}t'' sein sollte.}
\end{quote}  Hiermit sagt Horns, dass neuartige Gesch\"{a}ftsmethoden patentierbar sein m\"{u}ssen, sofern bei ihrer Durchf\"{u}hrung Energie verbraucht wird.
\end{quote}
\filbreak

\item
{\bf {\bf Donnerhacke 2002-07-25: unakzeptable formalistische Argumentation von Horns\footnote{http://www.fitug.de/debate/0207/msg00481.html}}}

\begin{quote}
Axel Horns hatte das Europ\"{a}ische Patentamt gegen Vorw\"{u}rfe von FFII/Eurolinux in Schutz genommen.  Nach tagelanger Beobachtung der darauf folgenden Diskussion kritisiert der Kryptograph und Fitug-Mitinitiator die formalistische Argumentationstaktik von Horns:  ``Bei der Diskussion um Abstrakta dem Gegen\"{u}ber abzuverlangen, auf dem eigenen Gebiet so konkret zu werden, da{\ss} man anhand von Spezialdetails weitermachen kann, zeigt, da{\ss} der abstrakte Grundvorwurf zutrifft.''  Er beanstandet damit insbesondere, dass Horns \"{U}cker aufgefordert hatte, die fraglichen ``musikinstrument-implementierten Erfindungen'' in Form von Patentanspr\"{u}chen darzustellen, damit man dar\"{u}ber reden k\"{o}nne.
\end{quote}
\filbreak

\item
{\bf {\bf Uecker 2002-07-25: Musikinstrument-Implementierte Erfindungen (MII) sind nicht \"{a}sthetische Formsch\"{o}pfungen als solche (2)\footnote{http://www.fitug.de/debate/0207/msg00485.html}}}

\begin{quote}
Ende einer ausf\"{u}hrlichen Diskussionsfadens, den Horns dadurch ausgel\"{o}st hatte, dass der das Europ\"{a}ische Patentamt gegen Vorw\"{u}rfe von FFII/Eurolinux in Schutz nahm.  Horns erkl\"{a}rte u.a. im Zusammenhang mit patentierter Gesch\"{a}ftsprozess-Software: \begin{quote}
{\it Die Erfindung ist in allen F\"{a}llen die Lehre, die sich in den Patentanspr\"{u}chen ausdr\"{u}ckt. Da hier physikalische ``Signale'' transferiert und prozessiert werden, habe ich nicht den geringsten Zweifel, dass es sich um eine ``technische Lehre zum Handeln'' handelt.}
\end{quote}  Dem hielt Martin Uecker entgegen, dass somit nach Horns alle Ideen, so z.B. auch musikalische Ideen, zu patentierbaren Erfindungen werden.  Nachdem Horns zun\"{a}chst mit seinem Versuch, die Diskussion auf patentrechtliche Formalien umzulenken, bei Donnerhacke abgeprallt war, blieb er nun \"{U}cker eine Antwort schuldig und lie{\ss} die Diskussion hiermit enden.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
% mode: latex ;
% End: ;

