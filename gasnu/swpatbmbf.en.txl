<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Wdz: Wir brauchen einen Forschungsminister (egal welchen Geschlechts), der seine Ministerialbeamten kritisieren und zur Verantwortung zu ziehen in der Lage ist.  Das ist die Hauptaufgabe jedes Politikers.

#JWh: Jemand muss Frau Bulmahn aufklären.

#Pre: Primärindikator für den Erfolg geförderter Projekte sollen die dabei erzeugten öffentlichen Informationsgüter (einschließlich Ideen und Werke) sein.  Im Bereich der Informatik/Software ist dieser Idikator sogar der alleine ausschlaggebende.  Messen lässt sich der Erfolg z.B. an Zitier- und Gebrauchshäufigkeit und der Zahl Personen im Inland, die darauf Fachwissen aufbauen und verwerten.  Ferner ist wichtig, ob ein Marktversagen/Gefangenendilemma vorlag, welches durch öffentliche Finanzierung behoben wurde.

#SWt: Schützenhilfe für den Rechtsbruch der Patentämter in bezug auf Software hat zu unterbleiben.  Die Siemens-Broschüre ist vom Netz zu nehmen und durch eine klare Grundsatzerklärung zu ersetzen.  Softwarepatente dürfen nicht im Rahmen von BMBF-geförderten Projekten angemeldet werden.

#FaF: Forderungen an das BMBF und die Forschungspolitik

#Bni: So far this whole policy does not seem to be based on any serious reasoning but rather on the gut feeling of politicians that american tides are irresistible, that universities are somehow inefficient, that professors should no longer be %(q:privileged), and that any kind of market-oriented reform is good.  How good/bad this reform really is, is an independent question on which we do not have more than guesses to offer either.  More studies, data, links etc needed.  Alternative approaches need to be developped and compared.

#Woi: Effects on Universities, macro-economy?

#EoW: Some answers on how the Bayh-Dole Act is impeding free software at universities in the US

#adu: american professor of computer science arguing that the Bayh-Dole Act did at the time seem to address some problems, such as lack of practicality and proof of concept in university research, but it created many more, and there is no longer any valid justification for maintaining this law.

#EWq: Ein erhebliches Innovationspotential liege an den deutschen Hochschulen Brach, erklärt Bulmahn.  Durch Anschubsfinanzierung solle die systematische Erschließung der wirtschaftlich verwertbaren Forschungsergebnisse sichergestellt werden.

#Bct: Bulmahn: Änderung des Hochschullehrerprivilegs soll jetzt zügig angegenangen werden

#Des: In this resolution, the german association of university professors warns that the universitarian patent offensive will lead to less bureaucratisation of universities and demotivation of researchers

#Sec: Deutscher Hochschulverband 2002-06-30: Verwertungszwang verschlechtert Forschungsklima

#Ise: Im Namen des BMBF werden Hochschulen und Unternehmen angeregt, mehr Patente anzumelden.

#Eel: Evan Brown, an IT service person, mentioned to his employer that he had recently thought of new ways to convert old programs to new ones.  The employer asked him to disclose the ideas so the company could patent them.  Brown refused and was fired.  The company was later acquired by Alcatel.  Alcatel is continuing to litigate against Brown for refusing to hand over his idea.  Brown has lost in court and is faced with lawyer bills of several 100k USD.  On this site he is asking the public for help.  Since in Europe, too, politicians have recently been eager to turn people's ideas into %(q:property) of their employers, this site should give people something to think about.

#aao: apparently written by people at a universitarian patent administration agency of the University of Columbia, reports very positively about the effects, fairly informative

#deW: danish counterpart of the Bayh-Dole Act and of the German university patent laws of 2002/02

#Mhu: Matthias Berninger, MdB Grüne und Staatssekretär im Landwirtschaftsministerium, stimmt in den Chor %(q:gegen das Hochschullehrerprivileg) und %(q:für den Aufbau einer Patentkultur an den Hochschulen) mit ein.   Der Patentbewegungs-Kampfbegriff %(q:Hochschullehrerprivileg) scheint auch grüne Abgeordnete anzusprechen.  Berninger meint, in der %(q:Wissensgesellschaft) würde wegen %(q:immer kürzerer Innovationszyklen) %(q:geistiges Eigentum) und insbesondere Patente immer wichtiger, und daher müsse die Patentvermarktung zentral von der Hochschule vorangetrieben werden.  Hiervon verspricht sich Berninger %(q:weniger Geheimforschung), %(q:besseren Technologietransfer), %(q:Etablierung einer Patentkultur) und vieles mehr, was die Patentbewegung an Versprechungen bereithält

#Mrc: Matthias Berninger 2000: Bedeutung der Patentvermarktung in der Wissensgesellschaft

#Dii: Der für Forschung zuständige CDU-Abgeordnete Norbert Hauser bläst auch in Bulmahns Horn.  Das %(q:Hochschullehrerprivileg) sei ein %(q:Überbleibsel aus der Kaiserzeit).  Der Aufbau einer zeitgemäßen Hochschul-Patentverwertung nach amerikanischem Vorbild erfordere aber mehr als die von Bulmahn vorgesehenen 100 Millionen DEM Anschubsfinanzierung.

#Hfi: Hauser 2001-11-30: Abschaffung des Hochschullehrerprivilegs richtig, Weg falsch

#Kea: Kritischer Bericht

#NiW: Neue Patentinitiativen von Bulmahn

#Inn: BMBF recommends brochure from the Siemens patent department which spreads the word that software has been made de facto patentable by patent jurisdiction, and endorses this policy.

#Bet: German Ministery of Science and Education uses Siemens Brochure to advocate software patents

#Eur: Introduction of a novelty grace period

#Ahs: Abschaffung von Auflagen, durch welche Universitäte bislang verpflichtet wurden, ihre Erfindungen der Öffentlichkeit zugute kommen zu lassen und eventuelle Patente in einigermaßen fairer Weise zu verwerten

#Ewa: IPR marketing initiative of the Max Planck Society

#Fnu: Fraunhofer-Patentstelle der Deutschen Forschung

#cWg: %(q:Patentoffensive) alias %(q:Verwertungsoffensive) im BMBF

#Zii: The universitarian patent movement is especially served by the following institutions and initiatives

#descr: Hundreds of universitarian %(q:technology transfer agencies) are putting brakes on the diffusion of technology with help of patents.  The taxpayer finances the creation of monopolised knowledge goods and then later pays about 10 times as much for the monopoly costs as the university can hope to gain from patent royalties.  An atmosphere of tension between researchers and patent bureaucrats is created.  The macro-economic reasons for the existence of a public research sector are neglected and a second-rate competition to industrial research is instead created and alimented by the taxpayer.  Some day in the future this may save the state some money, says the lobby that gets the newly created posts in the patent bureaucracy.  This lobby has its own governmental agencies on its side, of which the best known one in Germany is the %(q:Patent Offensive) in the Ministery of Education and Research.

#title: The Universitarian Patent Movement

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbmbf ;
# txtlang: en ;
# multlin: t ;
# End: ;

