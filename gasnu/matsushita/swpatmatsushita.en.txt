<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">


#descr: Matsushita Electrics alias National Panasonic is one of the most prolific software patenters and has regularly obtained more software patents than Siemens at the European Patent Office.  Politically, they have kept a low profile in the European debate on the limits of patentability.  Unlike IBM, General Electrics or the American Chamber of Commerce etc, they sent no reply to the European Commission's consultation call.  Here we try to give an overview of Matsushita's patents and patent applications at the European Patent Office (EPO).

#title: Matsushita Electrics and European Software Patents

#epat: Matsushita Software Patents at the EPO

#tts: Matsushita website

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: XXXXX ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatmatsushita ;
# txtlang: en ;
# multlin: t ;
# End: ;

