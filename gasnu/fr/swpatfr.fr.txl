<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Les Brevets Logiciels en France

#descr: Le gouvernement français a bien compris que la brevetabilité 
logicielles est dommageable pour les intérêts de la France et a montré
 sa position par des interventions politiques de temps en temps.  
Cependant, en général, la politique de brevets de la France est entre 
les mains de l'Institut National de la Protection Industrielle (INPI),
 dont les représentants traitent régulièrement avec dédain les
politiques  du gouvernement et suivent seulement le consensus au sein
de l'Office  des Brevets Européen.  Récemment, le gouvernement
Raffarin a adouci son  attitude d'opposition à la brevetabilité
logicielle.  Tous les  conseillers qui avaient acquis une connaissance
du dossier ont été  congédiés et à la place, une personne de confiance
du mouvement des  brevets a été chargé de la politique des brevets. 
Dans la  circonscription de M. Raffarin, on dit que Microsoft
financerait un  grand projet d'infrastructure publique appelé
Lunapark.

#csr: Le Programme du Parti Socialiste Français

#nge: Contient un passage contre les brevets logiciels

#Wpd: Le premier site web majeur européen contre les brevets logiciels,  la
plupart des pages sont en anglais, a été lancé en France en 1999.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: ccorazza ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatfr ;
# txtlang: fr ;
# multlin: t ;
# End: ;

