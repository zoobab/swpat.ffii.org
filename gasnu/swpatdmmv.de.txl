<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#UWg: Unter Punkt 1.2. / S. 7-9 findet sich auch die Forderung nach Legalisierung von Softwarepatenten.  Punkt %(q:1.2.2 Problemstellung) ist wirtschaftlicher Unsinn, sieht nach Patentbewegungs-Glaubensbekenntnis aus.

#Mee: Leitseite der DMMV-Wahlkampagne.  Der Verein hofft, dass zahlreiche Unternehmen das Werbebanner der Kampagne ausstellen werden.  Wesentliche Argumentationsgrundlagen des DMMV konnten wir noch nicht überprüfen -- sie liegen nur als Präsentationsgrafiken in MS-Powerpoint-Format vor.  Der DMMV zählt Microsoft und Adobe zu seinen fünf Haupt-Sponsoren.

#Bnl: Berichtet über Reaktionen der Parteipolitiker auf eine Aktion des DMMV für stärkeren %(q:Schutz digitaler Güter) einschließlich Erleichterung der Patentierung von Programmierideen und Subventionen des Staates für junge Unternehmen.  Angesprochene Parteipolitiker versichern ihre Unterstützung.  Einige Heise-Diskutanten geloben, nun PDS zu wählen.

#Dtg: Der in München ansässige Deutsche Multimedia Verband (dmmv) fordert in einem %(q:Vier-Punkte-Plan) allerlei Gesetzesänderungen zur %(q:Stärkung der Digitalen Wirtschaft).  Er feiert diese Initiative durch eine Übergibt und Diskussion mit den größten Bundesparteien am 15. Mai in Berlin.  %(q:Zentrale Forderung des Papieres ist ein effektiver Schutz digitaler Güter sowie deren Anerkennung als kreditfähiges Wirtschaftsgut und die Vereinfachung des Patentschutzes.)  Ferner fordert der DMMV den Abbau von Arbeitnehmerrechten und massive staatliche Subventionen für junge Unternehmen.

#bsz: berichtet über DMMV-Forderungen nach einer %(q:Internet-Polizei), die stichprobenartig den Datenverkehr abhören soll, um Urheberrechtsverletzungen aufzuspüren.

#WiW: Wir glauben nicht, daß die dmmv-Position zu Software-Patenten von kleinen und mittleren dmmv-Mitgliedern erarbeitet wurde, die selbst Software entwickeln.  Es besteht vielmehr eine hohe Wahrscheinlichkeit, dass entsprechende Positionen in diesem Papier von Microsoft kommen.  Zugleich entsprechen solche Positionen auf den ersten Blick der eigentumsverliebten Ideologie dieses Verbandes und weit verbreiteten positiven Vorurteilen über das Patentwesen.  Es ist sehr leicht, einem Verband wie dem DMMV eine Position zu einem Thema unterzujubeln, welches die meisten Verbandsmitglieder nicht interessiert und zu welchem ohnehin nur Patentjuristen in unbeachteten Arbeitskreisen zu Wort kommen.

#SFm: Kurz nach dem Eintritt von Roy in den Vorstand holt der VSI zu einem etwas plumpen Angriff gegen die Einführung von GNU/Linux im Bundestag aus, der bei weitem nicht das intellektuelle Niveau von Microsofts hauseigener Pressearbeit (Mundie, Allchin) erreicht, aber im wesentlichen mit den gleichen Scheinargumenten arbeitet.

#Knt: Künstliche Gegensätze zwischen freier und proprietärer Software --- Wen vertritt Gallist?

#Ddg: Dafür kann er auf Unterstützung im gesamten Verband rechnen. %(bc:Wir freuen uns, auf die Zusammenarbeit mit Herrn Roy. Sicherlich wird das aktive Engagement eines Vizepräsidenten der Firma Microsoft die Interessenvertretung der Softwareindustrie stärken), erklärte Rudolf Gallist, Vorstandvorsitzender des VSI. Erfahrung bringt Roy für diese Aufgabe zur Genüge mit. Schließlich leitet er in seiner Funktion bei Microsoft ein Team aus Experten für politische Themen, Öffentlichkeitsarbeit und europäische Angelegenheiten.

#DWi: Diese Liste ist eher konzernlastig als mittelstandsverdächtig.

#DSn: Der VSI hat folgenden Vorstand (Stand 2000, auf der Website haben wir keine neuere Wahl gefunden):

#VaS: Vorstand VSI 2000

#Ite: Insofern gehen wir davon aus, daß der dmmv was Software betrifft eine Tendenz haben könnte, Positionen des VSI oder von VSI-Mitgliedern wiederzugeben.

#dWd: dmmv und VSI führen gemeinsame Geschäftsstellen in Düsseldorf, München und Berlin. Den Mitglieder beider Verbände stehen alle Benefits offen.

#Dee: Der Verband der Softwareindustrie Deutschlands e.V. (VSI), gegründet 1987, ist eine Interessenvereinigung der Softwareindustrie mit Hauptsitz in München. Die inzwischen über 170 Mitglieder kommen aus den Bereichen Software-Herstellung, Distribution, Handel und Dienstleistung.

#Ddn: Der Deutsche Multimedia Verband (dmmv) e.V. und der VSI, der Verband der Softwareindustrie Deutschlands e.V., haben eine umfassende Kooperation ab dem Jahr 2002 vereinbart.

#AjW: Ach ja, am Fuß jeder www-Seite des dmmv steht

#sdd: so so, der dmmv %(q:vertritt mehr als 1700 kleine und mittelständische Unternehmen dieses neuen Wirtschaftszweiges.) Ist die Rezession schon so weit fortgeschritten, daß Axel Springer AG, AOL, T-Online, Worldcom jetzt klein und mittelständisch sind?

#Anr: Agenturen

#Bcc: Beim Buchstaben %(q:A) finden sich beispielsweise

#Wcr: Wenn man durch das Mitgliederverzeichnis durchstöbert (Buchstabe %(q:A) s. u.), dann sind das schon den Firmennamen nach im wesentlichen Firmen, wie Werbeagenturen, Multimediaagenturen, Verlage, Bildungssinstitute. Für mich also eher Firmen, die %(e:Inhalte) entwickeln und Software %(e:anwenden), aber Software nicht oder nur zum kleinen Teil entwickeln.

#Faz: Fazit

#Vot: VSI, Microsoft und Software-Patente

#Kad: Kooperation dmmv-VSI

#VWW: Vorstand (board) des dmmv

#dao: dmmv als solcher

#descr: Der DMMV veröffentlichte im Mai 2002 ein Programmpapier, in dem er staatliche Eingriffe zum Zwecke der Quadratur des Kreises (Übertragung materieller Gesetzmäßigkeiten auf die Ökonomie immaterieller Güter) und insbesondere eine weitere Erleichterung des Zugangs zu Patenten fordert.  Mit Glaubensbekenntissen dieser Art liegt man in einem bestimmten Milieu nach wie vor nie falsch.  Die öffentlichen Diskussionen der letzten Jahre sind aber an diesen Verbandsjuristen, die 1700 Unternehmen zu vertreten behaupten, offenbar völlig vorbeigegangen.  Bei den Bundespolitikern, die der DMMV-Vorstand am 15. Mai 2002 besuchen ging, ernte er höfliche Kommentare.  Weitere angekündigte Termine der Kampagne wurden nicht eingehalten.

#title: Deutscher Multi-Media-Verband

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatdmmv ;
# txtlang: de ;
# multlin: t ;
# End: ;

