<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: PA Springorum and Software Patents

#descr: Patent Attorney Dr. Dipl-Inform. Harald Springorum is one of the most vigorous promoters of software patentability in Germany.  Born in the 50s, he studied computer science and patent law.  At the time, computing and patents were two entirely divorced subjects, andSpringorum seems to be one of those patent attorneys who devoted much of their life to bringing the two together.  His endeavor may even be fuelled by some idealism or remnants thereof.  Springorum has been following the FFII on its footsteps since 1999, often warning people who have contacted us about how unworthy of cooperation we are.  He managed to be appointed by the Gesellschaft für Informatik (GI, German Computing Society) as a ghostwriter of their opinion on patent policy which they submitted to the EU and to advocate unlimited patentability in this paper in a way which goes even beyond what the EPO is willing to accept.  Moreover he has managed to speak on behalf of the German Chamber of Patent Attorneys at internal consultations of the Ministery of Justice, although his position is not particularly representative of patent attorneys.  Springorum's positions carry a certain individual hallmark that makes them easy to recognise even when he acts as a ghostwriter.

#Ede: Eine informative und über weite Strecken hin gut durchdachte Darstellung der Situation der Softwarepatentierung in Deutschland und Europa.  Der Artikel wird schon seit einigen Jahren immer wieder überarbeitet.  Seine Schwächen sind zugleich die Schwächen der meisten juristischen Fachartikel: er verehrt an entscheidenden Stellen die Meinung der Patentjuristen als eine Art mysteriöse höhere Wahrheit.  So heißt es z.B. %(q:In wieweit Information selbst eine beherrschbare Naturkraft i.S.d. Patentrechts ist, bleibt abzuwarten.  Die Tendenz in Erteilungspraxis wie in Rechtsprechung und h.M. geht jedenfalls in diese Richtung).

#HeW: Hier trat Springorum aus, um den Linux-Anwendern die Opposition gegen Softwarepatente auszureden und ihre Gedanken auf %(q:praktische) Alternativen wie z.B. die Gründung eines Schutzbündnisses zu lenken.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatspringorum ;
# txtlang: en ;
# multlin: t ;
# End: ;

