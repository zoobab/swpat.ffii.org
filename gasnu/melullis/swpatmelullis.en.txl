<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Dr. Klaus-J. Melullis and Software Patents

#descr: Presiding Judge at the patent senate of the Bundesgerichtshof (BGH, Federal Court), lawyer by education, later became known as software patentability theoretician, seeking to bring BGH closer to the EPO's line of making software patentable, but showing a certain uneasiness about the consequences, warning that only the technical (implementational) aspects should be patented and not the concepts and functionalities.  In november 2000, Melullis expressed himself publicly against the plans to remove the patentability exception.  In 2001 Melullis became the presiding judge of the BGH patent senate.  In 2002, after public exchanges of views with Swen Kiesewetter-Köbinger, Reimar König and other critical patent law scholars, Melullis published a series of articles and memorandums which criticise the EPO's approach and the European Commission's directive proposal for deviating from what is permissible under Art 52 EPC and sticking to inconclusive legalistic reasoning instead of attempting a fair assessment of the interests at stake.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatmelullis ;
# txtlang: en ;
# multlin: t ;
# End: ;

