\contentsline {section}{\numberline {1}Das EPA: eine besondere Rechtskonstruktion}{3}{section.1}
\contentsline {section}{\numberline {2}Aufl\"{o}sung des Erfindungsbegriff von Art 52 EP\"{U}}{3}{section.2}
\contentsline {section}{\numberline {3}Missachtung der Patentierbarkeitsausnahmen von Art 53 EP\"{U}}{3}{section.3}
\contentsline {section}{\numberline {4}Aufweichung des Neuheitsbegriffs von Art 54 EP\"{U}}{3}{section.4}
\contentsline {section}{\numberline {5}Formelle Sicht von Art 52 EP\"{U}: Niedrigste Erfindungsh\"{o}he der Welt}{4}{section.5}
\contentsline {section}{\numberline {6}St\"{a}ndig sinkende Pr\"{u}fungsqualit\"{a}t}{6}{section.6}
\contentsline {section}{\numberline {7}BEST: schlechtestm\"{o}gliche Pr\"{u}fungsqualit\"{a}t, gesetzeswidrig eingef\"{u}hrt, 2000 legalisiert}{7}{section.7}
\contentsline {section}{\numberline {8}Probleme der Neuheitsrecherche zunehmend komplex, EPA weit abgeschlagen}{8}{section.8}
\contentsline {section}{\numberline {9}Rechtsfreier Raum, schlechtes Arbeitsklima}{9}{section.9}
\contentsline {section}{\numberline {10}Karrieren zwischen \"{U}berwachenden Organen und EPA}{10}{section.10}
\contentsline {section}{\numberline {11}Unabh\"{a}ngigkeit der Beschwerdekammern nicht gew\"{a}hrleistet}{10}{section.11}
\contentsline {section}{\numberline {12}Vorenthaltung von Patentinformationen.}{12}{section.12}
\contentsline {section}{\numberline {13}Annotated Links}{12}{section.13}
\contentsline {section}{\numberline {14}Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}{17}{section.14}
