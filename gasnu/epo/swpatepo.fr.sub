\begin{subdocument}{swpatepo}{Office Europ\'{e}en de Brevets: Au Dessus de la L\'{e}galit\'{e}}{http://swpat.ffii.org/acteurs/epo/index.fr.html}{Groupes de travail\\swpatag@ffii.org}{The European Patent Office finances itself by fees from the patents which it grants.  It is free to use a certain percentage of these fees.  Since the 1980s the EPO has illegally lowered the standards of technicity, novelty, non-obviousness and industrial applicability and abolished examination quality safeguards so as to increase the number of granted patents by more than 10\percent{} and the license tax on the industry by 26\percent{} per year.  As an international organisation, the EPO is not subject to criminal law or taxation.  The local police's power ends at the gates of the EPO.  High EPO officials have inflicted corporal injury on their employees and then escaped legal consequences by their right to immunity.  The work climate within the EPO is very bad, leading to several suicides per year.  The quality of examination reached a relative high in the 80s but has after that been deteriorating, partly because the EPO had to hire too many people too quickly for too low wages.  Examiners who reject patents load more work on themselves without getting more pay.  Examiners are treated by the EPO management as a kind of obstacle to the corporate goal of earning even more patent revenues.  The high-level employees of the EPO owe their jobs to political pressures from within national patent administrations and do not understand the daily work of the office.  The EPO has its own jurisdictional arm, consisting of people whose career is controlled by the EPO's managment, which again is strongly influenced by industry patent lawyers (on the Standing Advisory Committee (SACEPO)) and by the Office's internal climate.  The national organs that are supposed to supervise the EPO are all part of the same closed circle, thus guaranteeing the EPO managment enjoys feudal powers beyond constitutional legality, and that whatever they decide is propagated to the national administrations and lawcourts.}
\begin{center}
\begin{tabular}{|C{44}|C{44}|}
\hline
\mbox{\includegraphics{epologo}} & The fingerprint is a symbol of ``intellectual property'', as known from the concept of copyright: the individual creation which other people are unlikely to recreate on their own.  The EPO was founded in 1973/1978 with the mission to extend the principle of property in individual creation into the physical world, where forces of nature are harnessed in a peculiar and difficult-to find and easy-to-imitate way.  Meanwhile, the EPO is doing just the opposite today: it is granting patents for broad and general principles and extending this practise into the sphere of copyright.  Thus the EPO logo symbolises one of several astonishing developments which we are trying to document here.\\\hline
\end{tabular}
\end{center}

\begin{sect}{intro}{The special legal construction of the EPO}
\dots
\end{sect}\begin{sect}{epc52}{Dissolving the invention concept of Art 52 EPC}
The EPO was the leader, others willingly followed.

cf Jurisprudence de Brevet sur Terrain Glissant: Le Prix a Payer pour le D\'{e}montage de l'Invention Technique (http://swpat.ffii.org/analyse/invention/index.fr.html)
\end{sect}\begin{sect}{epc53}{Disrespecting the Exceptions of Art 53 EPC}
cf Greenpeace: OEB et amis violent la Loi (http://swpat.ffii.org/papiers/greenpeace-epo/index.en.html)

\dots
\end{sect}\begin{sect}{epc54}{Eroding the novelty concept of Art 54 EPC}
Art 54 EPC says

\begin{quote}
{\it \begin{enumerate}
\item
An invention is deemed new, if it does not belong to the state of the art.

\item
The state of the art is formed by everything that was made accessible to the public before the day of application for the European Patent by written or oral description, by use or in any other way.
\end{enumerate}}
\end{quote}

From this we can not infer that anything that is not described within one self-contained document is new.

Only knowledge that was previously not accessible is new.  Knowledge that a person skilled in the art can obtain by reading several documents together is not new.  There is not much objective reason for the assumption that a known process must have been disclosed in a single document somewhere.  Many informations are not transmitted in written form, and whether something is new or not has little to do with how it has been codified.  The German Patent Office has also, at least until recently, not construed novelty in this way.  By requiring that the teaching be found in a single document, the EPO has severely weakened the requirement of novelty.  At the same time, the non-obvious requirement has been reduced to a means of filling the holes in the novelty requirement which the EPO has opened up.  The EPO considers any trivial idea non-obvious, ifonly the previous knowledge about this idea is dispersed in several documents and the person skilled in the art would not automatically have read these documents in context.

By weakening requirements such as ``novelty'', ``non-obviousness'', ``industriality'' and ``technicity'', the EPO has succeeded in greatly increasing the number of patents granted year by year.a

One of the reasons for the continued weakening lies in the fact that patent examiners do not have a practical possibility of rejecting applications: the applicant only needs to write more letters and insist on his legal right of being heard. An examiner cannot afford to bear the blame of violating this right.  If an examiner continuously rejects the applicant's demand, he can only agree to an oral proceding. This constitutes a huge burden which the examiner can only afford to shoulder a few times every year.  Thus the examiner has only one strategy survival:  lower the requirements as far as possible.  Thus, in order to justify the granting of a patents, he will usually resort to justifications which, in the eyes of an unprejudiced outsider, may read like enunciations of a mentally ill person.
\end{sect}\begin{sect}{epc56}{Formal view of Art 56 EPC: Lowest Non-Obviousness Standards of the World}
\dots

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Comparative Study of Software Examination Practise of EPO, JPO and USPTO\footnote{http://www.jpo-miti.go.jp/saikine/repo242.htm}}}
\filbreak

\item
{\bf {\bf TAMAI Tetsuo: Abstraction orientated property of software and its relation to patentability\footnote{http://swpat.ffii.org/papiers/ist-tamai98/index.en.html}}}

\begin{quote}
Prof Tamai of Tokyo University shows how patenting of software clashes with some of the underlying assumptions of the patent system.  While concreteness and physical substance are at the basis of patent system, the value of software innovation lies in its abstractness.  This contradiction can be solved in one of two ways: (1) blanket acceptance of claims to abstract-logical innovations without reference to concrete applications (2) clear delimitation of patentability to concrete applications where the innovative idea itself has a physical substance.  It seems that only the more conservative second solution leads to results that are in the public interest.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}\begin{sect}{exam}{Continuously deteriorating examination quality}
Before the EPO existed, there was an organisation called IIB (Institut International des Brevets), based in The Hague, with France and BeNeLux as member states.  There was no substantial examination, only search.

When the EPO was founded in 1978, the IIB was reorganised to become the EPO's Research Directorate (GD 1). At the same time the status of the employees, who held a diplomat's passport, was reduced to that of a kind of pseudo diplomat, and the salary was lowered.  This provoked dissatisfaction, and research quality sank. 

Moreover, due to the newly introduced obligation to grant a ``right of being heard'', examiners were forced to reply to whatever moronic arguments came from the patent attorney, and was not allowed to reject as long as new arguments kept appearing, unless an oral hearing was carried out.  This fact determined that in EPO practise granting (sometimes with slight narrowing of claims) would be the rule and rejection a rare exception.

Das Problem mit dem ``rechtlichen Geh\"{o}r'' besteht auch in dem Fall, dass der Pr\"{u}fer die Anspr\"{u}che einzuschr\"{a}nken versucht.  Da kann sich der Anwalt auf alle m\"{o}glichen Arten querlegen: sehr beliebt ist blo{\ss} mikrometerweise nachgeben oder neue S\"{a}tze von Anspr\"{u}chen mit etwas anderer Formulierungen und etwas anders gelagertem Umfang. Es muss nur genug in der Gesamtheit offenbart sein, dass sich immer wieder etwas anderes herausgreifen l\"{a}sst, man also nicht mit Art. 123 (urspr\"{u}ngliche Offenbarung) nicht in Konflikt kommt. Sehr n\"{u}tzlich hierf\"{u}r ist eine Beschreibung riesiger L\"{a}nge (z.B. n x 100 Seiten). Da der Pr\"{u}fer nur f\"{u}r den Erstbescheid und fuer den Abschluss eines Verfahrens je einen halben Punkt bekommt und eine bestimmte Mindestpunktzahl im Jahr zu erreichen hat (und nur dann Aussicht auf Bef\"{o}rderung hat, wenn er erheblich mehr erreicht), kann er sich nicht allzuviele Bescheide pro Vorgang leisten.  Konsequenz: runter mit den Anforderungen und ab durch die Mitte....

Those who were very productive, i.e. very permissive, have been promoted to become today's directors, i.e. heads of directorates, which is what the examination divisions are called.

Low examination quality has right from the start been an invitable part of this system.

20 years ago the examiners still had to accumulate far fewer credits, because the number of applications was lower.  This changed quickly, as more and more questionable patents passed through and it became a profitable strategy for the industry to patent any shade of a shade of an innovation.  Thus the backlog grew and the examiners had to cope with more applications in less time.  This again led to a drop in examination quality, which again spurred a rise in the number of patent applications.  And so forth.

Moreover in 1991 a clueless statistics commission predicted a drop in the number of applications, whereupon EPO president Br\"{a}ndli decreed to stop hiring examiners.  This hiring ban was in force until Br\"{a}ndli's end of term in 1995.

In 1995 president Kober came and started to hire in masses.  This again was problematic.  The EPO's reputation had already been damaged, so that it was difficult to find good people.  Also the salary of examiners had not risen in 10 years.  Kober aggravated this problem by introducing new so-called negative salary levels, i.e. hiring new examiners at a lower salary than what had been the lowest before.  Thus the EPO had to put up with people who would not have found a job in the industry, and these people again had to be trained by the EPO itself.  This meant that better examiners had to be freed for education jobs and the ratio of applications to be handled by each examiner grow even more adverse.

Meanwhile examination quality has sunk to a point where it can hardly sink any further, and the huge pile of crappy patents accumulated by the EPO has become hard to overlook.
\end{sect}\begin{sect}{best}{BEST: assured worst examination quality, illegally introduced, legalised in 2000}
Yet in the mid 90s there was still an untouched potential for further lowering patent examination quality:  the separation of search and examination.  This separation constituted one of the few remaining incentives that worked in favor of serious examination, and it was based on legal requirements laid down in the EPC.  However, starting in the mid 90s, there was a movement in the EPO managment to circumvent this requirement.  Under the euphemism ``Bringing Search and Examination Together'' (BEST), the EPO found ways to break the law, which it then successfully asked the Diplomatic Conference of 2000 to rewrite according to the ``BEST practise''.  

Art 17 EPC:

\begin{quote}
{\it The research departments belong to the branch in The Hague.  They are responsible for creating european research reports.}
\end{quote}

This means: when the Munich-based GD2, which is not subordinate to the Hague branch, began to research on its own in place of GD 1, it was violating Art 17 EPC. 

GD 2 did this under the pretext that they ``wanted to test the electronic research equipment''.  This equipment, as a sidenote, operated at a level of technology and sophistication which corresponded to that of the 1960s at best and was evidently unusable and by no means worthy of testing. 

Even some of the applicants had little trust in the BEST practise.  A swiss company exlicitely demanded that the EPO should not use it for examining its patents.

In november 2000 the Diplomatic Conference agreed to the BEST practise.  Although the member states have not yet ratified the amendments of the EPC, the BEST practise has already been generally introduced.  Now every examiner conducts both research and substantive examination at the same time.  Under the given conditions this means that it is now possible to simply ignore any documents that would make examination work too difficult.  In any case it is unlikely that much prior art will be found.  The EPO's search tools are much less useful than general Internet search machines (which of course are also by far not good enough), and the IBM VM/CMS style user interface makes them inconvenient to use.

Between 1991 and 1993 the examination quality at the EPO was still respectable.  At least in some areas (not software), it was considered to be better than that of the USPTO and JPO.  After the inauguration of the new examiner's building at Pschorrh\"{o}fe (west of Munich central station), deterioration was clearly felt, and it accelerated exponentially starting in 1996.  Nowadays the patent examination system at the  EPO is no longer anything more than a joke.

At the moment (2002) the location agreement of the EPO with the dutch government is ending, and the latter is showing little interest in prolonging it.  Meanwhile the EPO is planning new facilities in Munich and hiring en masse, while The Hague (GD 1) has been observing a hiring freeze since 1999.

This is apparently an effect of BEST: given that Examination and Search are Brought Together, it is clear that either GD 1 or GD 2 has become superfluous.  The fight for survival between the two has been going on for a while already.  It looks now as if GD 2 has made it and The Hague will be closed with expiry of the agreement with the Netherlands.

The morale in The Hague has thus reached new lows, and with it also the search quality.
\end{sect}\begin{sect}{obje}{Novelty search problems increasingly complex, EPO lagging far behind}
Um recherchieren zu k\"{o}nnen, braucht man eine entsprechend aufbereitete Dokumentation, und zwar auch und vor allem von Nichtpatentliteratur.  Aber dank der galoppierenden Patentinflation nimmt auch die Patentliteratur in einem Ma{\ss}e zu, dass man kaum noch mithalten kann, und angesichts nicht vorhandener Erfindungsh\"{o}hestandards muss jede Kleinigkeit, die vielleicht kaum dokumentiert ist, weil niemand das f\"{u}r erw\"{a}hnenswert h\"{a}lt, wie eine Nadel in einem exponentiell wachsenden Heuhaufen recherchiert werden.  Der Arbeitsaufwand f\"{u}r eine Pflege und Klassifikation der Dokumentation ist erheblich und steigt st\"{a}ndig, wobei die Sucheffizienz f\"{u}r den Pr\"{u}fer eher sinkt als steigt. Da die EPA-Leitung von alledem keine Ahnung hat, wurde in dieser Hinsicht nicht viel getan. Als der Chefbibliothekar des EPA, Gerhard Kruse, das aendern und N\"{a}gel mit K\"{o}pfen machen wollte, wurde er brutal ausgebremst.  Logisch - mit guter Dokumentation w\"{u}rde weniger erteilt, was dem politischen Ziel, m\"{o}glichst viel zu erteilen, nat\"{u}rlich zuwiderl\"{a}uft.

Eine gro{\ss}e Organisation mit genug Geld und vor allem eigener Dokumentation und Recherchemitteln k\"{o}nnte wohl locker jedes zweite EPA-Patent umwerfen, und zwar auch auf herk\"{o}mmlichen gut dokumentierten Gebieten.  Zur not tut es auch nur viel Geld und die Recherche einer darauf spezialisierten Firma zu \"{u}berlassen. Was die meisten nicht wissen: das ist ein ganzer Gewerbezweig mit eigenen Grossfirmen, deren Kunden die Patentaemter der ganzen Welt sind.  Unter diesen Gro{\ss}firmen die gr\"{o}{\ss}te und bekannteste (aber nicht unbedingt beste) ist die englische Firma Derwent. Das EPA ist Gro{\ss}kunde bei Derwent.
\end{sect}\begin{sect}{pers}{Outlaw zone, poor working climate}
Zur systematischen Gesetzesverletzung hinzu kommt, dass das EPA in einem weitestgehend rechtsfreien Raum lebt.  Der Hoheitsbereich der deutschen Polizei endet vor den T\"{u}ren des EPA.  Gewerkschaften sind innerhalb des EPA weitgehend\footnote{Es gibt ein paar formale Gesetze, die zumindest die Existenz einer Personalvertretung gewaehrleisten. Viel ist es aber nicht.} rechtlos.

Folgendes kam uns aus dem Kreise der EPA-Mitarbeiter zu Ohren und erscheint glaubw\"{u}rdig, ist aber noch n\"{a}her zu pr\"{u}fen:

\begin{quote}
Eine Gewerkschaftsvertreterin wurde kurz vor Kobers Amtsantritt von dessen Amtsvorg\"{a}nger Paul Br\"{a}ndli in den Bauch getreten und verletzt ins Krankenhaus eingeliefert.   Der EPA-Pr\"{a}sident kam unbelangt davon, weil er sich auf seine Immunit\"{a}t berief.

Ingo Kober gilt weithin als der schlechteste EPA-Pr\"{a}sident, den es je gab: wegen seiner Unf\"{a}higkeit, einen einvernehmlichen Dialog mit dem Personal zu fuehren sind gewerkschaftliche Kampfma{\ss}nahmen im EPA ein Dauerzustand. Das aber bedeutet, dass erheblich weniger Anmeldungen durchgezogen werden, als das normal ueblich ist, und da die Signatarstaaten 50 \percent{} der EPA-Einnahmen abgreifen (der jeweilige Anteil bemisst sich nach einem Schl\"{u}ssel, der auf den Anmeldezahlen der jeweiligen nationalen Patent\"{a}mter beruht), bedeutet das Mindereinnahmen fuer Selbige. Und das ist begreiflicherweise dem Verwaltungsrat gar nicht recht, weshalb man Kober nicht wohlgesinnt ist (die offizielle Begruendung dafuer lautet, dass er ``nicht in der Lage sei, den sozialen Frieden im EPA zu erhalten''). Dementsprechend wurde er auf ein Ma{\ss} demontiert, dass es dem Verwaltungsrat erlaubt, im EPA nach eigenem Gutd\"{u}nken zu schalten und zu walten, wie es ihm beliebt.

Das Schlimme am EPA-Verwaltungsrat sind die in letzter Zeit beliebten ``ausserordentlichen Sitzungen'' -- Geheimsitzungen inoffizieller Natur, in denen hinter verschlossenen Tueren alles ausgekasperlt und dann auf den offiziellen Sitzungen einfach beschlossen wird. Da lassen sich Dinge wie weitreichende EP\"{U}-\"{A}nderungen bequem vorbereiten, so dass man die \"{O}ffentlichkeit damit ratzfatz \"{u}berfahren kann. Man stelle sich vor, das Gemeinschaftpatent w\"{a}re schon da....lustig, was?
\end{quote}

Manche Leute l\"{a}stern \"{u}ber den Vatikan, in dem keine demokratischen Verh\"{a}ltnisse herrschten und der deshalb nicht in die EU und vielleicht auch nicht in die UNO aufgenommen werden sollte. Man fragt sich, ob nicht das EPA einen Antrag auf Aufnahme in die UNO stellen sollte.  Es erf\"{u}llt, soweit ich sehen kann alle Bedingungen der Staatlichkeit.  Ob der Antrag vielleicht wegen mittelalterlicher Zust\"{a}nde im EPA-Land zur\"{u}ckgewiesen werden k\"{o}nnte, ist eine andere Frage.  Man sollte da die Latte nicht zu hoch h\"{a}ngen, denn in vielen Staaten der Welt geht es Gewerkschaften schlimmer als beim EPA.  Indem man das EPA in die UNO aufn\"{a}hme, k\"{o}nnte man es zumindest an diverse Menschenrechtsvertr\"{a}ge binden.

\"{A}hnliches gilt \"{u}brigens auch f\"{u}r die Europ\"{a}ische Kommission, die von niemandem, auch nicht vom Europaparlament, so recht kontrolliert wird.  G\"{a}be es nicht das Br\"{u}sseler Einfallstor, so h\"{a}tten Idiotien wie EuroDCMA, SSSCA, und Software- und Genpatente wohl in kaum einem europ\"{a}ischen Land eine Chance, ernsthaft ins Gespr\"{a}ch zu kommen.

Zusammenfassend laesst sich sagen, dass im EPA der Verwaltungsrat ungehindert regiert: und der besteht im Wesentlichen aus oberen Chargen nationaler Patentaemter.  Irgendeine Art wirksamer Kontrolle aus den Herkunftsl\"{a}ndern besteht allerdings nicht. Es kursieren recht ernstzunehemnde Ger\"{u}chte, dass Patentabteilungen der Gro{\ss}industrie und Patentanwaltsverb\"{a}nde als graue Eminenzen im Hintergrund federf\"{u}hrend mitwirken.
\end{sect}\begin{sect}{karr}{National controllers of the EPO aspiring for careers in EPO}
Diejenigen, die eigentlich das EPA \"{u}berwachen sollten, sind Anw\"{a}rter auf Posten im EPA.

Kober kommt vom DE-Justizministerium, Br\"{a}ndli war Patentamtspraesident in der Schweiz, Frau Leutheuser-Schnarrenberger von der FDP ist heute ebenso wie der ehemalige BMJ-Patentreferent Peter M\"{u}hlens Direktor im EPA....eine endlose Geschichte.
\end{sect}\begin{sect}{kamm}{EPO Boards of Appeal lack Independence}
Artikel 23(3) EP\"{U} beteuert zwar die Unabh\"{a}ngigkeit der Beschwerdekammern, leider aber wird er bereits von Artikel 23(1) ausgehebelt:

\begin{quote}
{\it Die Mitglieder der Grossen Beschwerdekammer und der Beschwerdekammern werden f\"{u}r einen Zeitraum von 5 Jahren ernannt und k\"{o}nnen w\"{a}hrend dieses Zeitraums ihrer Funktion nicht enthoben werden, es sei denn, dass schwerwiegende Gr\"{u}nde vorliegen und der Verwaltungsrat auf Vorschlag der Gro{\ss}en Beschwerdekammer einen entsprechenden Beschluss fasst.}
\end{quote}

Sollte die Bedenklichkeit des Obigen f\"{u}r die Unabh\"{a}ngigkeit noch unklar sein, dann wird er mit Artikel 11(3) vollst\"{a}ndig klar:

\begin{quote}
{\it Die Mitglieder der Beschwerdekammern und der Gro{\ss}en Beschwerdekammer einschliesslich des Vorsitzenden werden auf Vorschlag des Pr\"{a}sidenten des Europ\"{a}ischen Patentamtes vom Verwaltungsrat ernannt. Sie k\"{o}nnen vom Verwaltungsrat nach Anh\"{o}rung des Pr\"{a}sidenten des Europ\"{a}ischen Patentamtes wiederernannt werden.}
\end{quote}

Im Klartext: wer nicht spurt, wird nicht wiederernannt.

Sollten noch irgendwelche Zweifel an der rein politischen Natur der Auswahl hoher Beamter im EPA bestehen, dann beseitigt Artikel 11 auch diese:

\begin{quote}
{\it \begin{enumerate}
\item
Der Pr\"{a}sident des Europ\"{a}ischen Patentamts wird vom Verwaltungsrat ernannt.

\item
Die Vizepr\"{a}sidenten\footnote{Leiter der Generaldirektionen} werden nach Anh\"{o}rung des Pr\"{a}sidenten vom Verwaltungsrat ernannt.
\end{enumerate}}
\end{quote}

Und so lernt das auch jeder Pr\"{u}ferneuling: ``In dem Laden kannst du es maximal bis zum Hauptdirektor bringen.''

Was auch nicht immer stimmt: der jetzige Vizepr\"{a}sident der GD2, Herr Kyriakides, seines Zeichens ein Zypriot mit britischem Pass, war fr\"{u}her sogar Pr\"{u}fer (selten!).  Das war aber nicht der Grund, weshalb er den Job bekam: der Grund war, dass die englische Delegation des Verwaltungsrates dahingehend dr\"{a}ngte (was nat\"{u}rlich bedeutet, dass er in seiner Delegation die richtigen Leute kennt). Sein Vorg\"{a}nger, ein Herr Vivian, war hingegen nie Pr\"{u}fer gewesen.  Ein Gl\"{u}cksfall: Kyriakides steht in dem Ruf, weit besser als sein Vorg\"{a}nger zu sein.

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Linux-Magazin: Keiner will den schwarzen Peter\footnote{}}}
\filbreak

\item
{\bf {\bf PA Pfeiffer: Schwachsinn, EPA-TBK sind unabh\"{a}ngig. weist auf Art 22 EP\"{U} hin, in dem dies steht, s. auch folgende Diskussion\footnote{}}}
\filbreak

\item
{\bf {\bf PA Springorum: BVerfG hat entschieden: EPA-TBK sind echte Gerichte im Sinne des GG\footnote{}}}
\filbreak

\item
{\bf {\bf Prof. Lenz: Pilchs Irrtum\footnote{}}}
\filbreak

\item
{\bf {\bf PA Pfeiffer: Lenz muss von Pilch hypnotisiert worden sein\footnote{}}}
\filbreak

\item
{\bf {\bf BVerfG 2001-04-04: EPA-Beschwerdekammern f\"{u}r Entscheidung \"{u}ber Zulassung von Vertretern am EPA hinreichend befugt\footnote{}}}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}\begin{sect}{espa}{Withholding Patent Information from the Public}
The patents which the EPO grants are still available only in the form of graphic files, which are wrapped into one PDF file per page, using a LZW compression (which moreover is patented and therefore not accessible to free software.)

Thus the EPO has managed until this day to keep its patent database, at least the valid (B1) writings, outside of the scope of any Internet search engine.

This is a deliberate policy, designed to create a market for proprietary information providers.

Things have already improved quite a bit in this respect.  In the mid 90s, the EPO hat a clear and explicity policy of withholding the very information whose public disclosure and diffusion is said to constitute the meaning of the patent system.  They were spread out as graphic files on a proprietary difficult-to-copy system dependent on Microsoft Windows and sold for the equivalent of several 1000 EUR per year.  The EPO's copyright notice moreover labeled the content of the CDs as ``proprietary information'', although according to existing laws patents are public and the copyright on them cannot be enforced.

This policy should be considered symptomatic of the EPO management's attitude toward the public interest and toward the purpose of system which it is supposed to represent.
\end{sect}\begin{sect}{links}{Liens Annot\'{e}s}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Office Europ\'{e}en de Brevets\footnote{http://www.european-patent-office.org}}}
\filbreak

\item
{\bf {\bf EPO Staff Union 2002: A Public Service Organisation Out of Control?\footnote{http://aful.org/wws/arc/patents/2002-08/msg00014.html}}}

\begin{quote}
The Union of Employees at the European Patent Office (EPO) points out a systematic pattern of uncontrolled and illegal behavior by the Administrative Council, the President and other organs of the EPO.  This text was displayed in spring of 2002 at the Union's website and later removed.
\end{quote}
\filbreak

\item
{\bf {\bf OEB 2002-06-21: Rapport concernant le propos de directive CCE/BSA\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/epo020621/index.fr.html}}}

\begin{quote}
L'Office Europ\'{e}en de Brevets (OEB) pr\'{e}sente une prise de position sur le propos de directive europ\'{e}enne pour la brevetabilit\'{e} des logiciels et demande un avis du comit\'{e} consultatif SACEPO, qui est compos\'{e} surtout par experts en brevet de l'industrie, notamment des grandes entreprises.  L'OEB mentionne la campagne eurolinux comme un position ``int\'{e}griste'' d'une ``groupe de pression forte''.  L'OEB explique que le propos de directive se fonde sur des papiers de l'OEB et mentionne quelques points ou il y a des diff\'{e}rences ou des manques de clart\'{e}.  L'OEB explique l'\'{e}tat des consultations dans le Conseil et le Parlament Europ\'{e}en. L'OEB est pr\'{e}sent dans le conseil comme repr\'{e}santant expert de la commission europ\'{e}enne.  L'Alliance Eurolinux se demande pour quoi elle n'a pas de l'acces a ces sessions et pour quoi elle n'est pas encore membre du SACEPO.
\end{quote}
\filbreak

\item
{\bf {\bf Slashdot 2002-08-09: Peek Into European Patent Examining Cancelled\footnote{http://interviews.slashdot.org/article.pl?sid=02/08/09/0012208\&mode=thread\&tid=155}}}

\begin{quote}
John Savage, an examiner at the European Patent Office (EPO), had agreed to answer a list of questions from users of Slashdot.com.  However, after the questions had been passed to him, he withdrew.  Apparently the PR department of the EPO did not want him to engage in this kind of discussion.  Other patent examiners have already undergone disciplinary measures because they had engaged in public discussions about their employer.
\end{quote}
\filbreak

\item
{\bf {\bf Convention Europ\'{e}ennes de Brevets\footnote{http://www.european-patent-office.org/legal/epc97/f/}}}

\begin{quote}
texte de la version OEB de 1997
\end{quote}
\filbreak

\item
{\bf {\bf Art 52 CEB: Interpr\'{e}tation and R\'{e}vision\footnote{http://swpat.ffii.org/analyse/epc52/index.de.html}}}

\begin{quote}
Les limites de la brevetabilit\'{e} fix\'{e}s dans la Convention Europ\'{e}enne des Brevets (CEB) en 1973 ont \'{e}t\'{e} contourn\'{e} dans les derni\`{e}res ann\'{e}es.  Quelques courts de brevets ont utilis\'{e} chaque trou dans l'article 52 pour \'{e}largir l'influence de leur syst\`{e}me.  Ces nouvelles formulations pourraient aider \`{a} reetablir une d\'{e}limitation claire.
\end{quote}
\filbreak

\item
{\bf {\bf Logiciels et Droit de Brevet: R\'{e}censions d'Articles\footnote{http://swpat.ffii.org/papiers/epo-t971173/index.fr.html}}}

\begin{quote}
En 1998-07-01 la Chambre de Recours Technique 3.5.1 de l'OEB d\'{e}cide: ``Un produit programme d'ordinateur n'est pas exclu de la brevetabilit\'{e} en application de l'article 52(2'' et (3) CBE si sa mise en oeuvre sur un ordinateur produit un effet technique suppl\'{e}mentaire, allant au-del\`{a} des interactions physiques normales entre programme (logiciel) et ordinateur (mat\'{e}riel).) Le document finisse par une ``Note de l'\'{e}diteur'': ``L'OEB a l'intention d'adapter sa pratique \`{a} la lumi\`{e}re de cette d\'{e}cision.  Les Directive relative \`{a} l'examen pratiqu\'{e} \`{a} l'OEB seront remani\'{e}es en cons\'{e}quence.  Des information \`{a} ce sujet seront publi\'{e}es ent temps utile.''
\end{quote}
\filbreak

\item
{\bf {\bf Brevets Logiciels de l'Office Europ\'{e}en de Brevets\footnote{http://swpat.ffii.org/brevets/txt/ep/index.en.html}}}

\begin{quote}
Pendant les derni\`{e}res ann\'{e}es l'Office Europ\'{e}en de Brevets (OEB) a accord\'{e} quelques 10000 de brevets sur des r\`{e}gles d'organisation et de calcul mises en oevre par ordinateur, i.e. des program d'ordinateurs [ en tant que tels ].  Nous essayons de collectioner ces brevets et les rendre facilements accessible.
\end{quote}
\filbreak

\item
{\bf {\bf Jurisprudence de Brevet sur Terrain Glissant: Le Prix a Payer pour le D\'{e}montage de l'Invention Technique\footnote{http://swpat.ffii.org/analyse/invention/index.fr.html}}}

\begin{quote}
Jusqu'\`{a} pr\'{e}sent les programmes d'ordinateur et en g\'{e}n\'{e}ral les \emph{r\`{e}gles d'organisation et de calcul} ne sont pas des \emph{inventions brevetables} selon la loi Europ\'{e}enne, ce qui n'exclut pas qu'un proc\'{e}d\'{e} industriel brevetable puisse \^{e}tre control\'{e} par un logiciel.  L'Office Europ\'{e}en des Brevets et quelques Cours nationales ont cependant contourn\'{e} cette r\`{e}gle, en rempla\c{c}ant un concept de d\'{e}limitation claire par une ins\'{e}curit\'{e} l\'{e}gale sans limites reconnaissables.  Cet article offre une introduction aux probl\`{e}mes et \`{a} la litt\'{e}rature juridique sur ce sujet.
\end{quote}
\filbreak

\item
{\bf {\bf }}
\filbreak

\item
{\bf {\bf Ingo Kober and Software Patents\footnote{http://swpat.ffii.org/acteurs/kober/index.en.html}}}

\begin{quote}
president of the European Patent Office (EPO) since the mid-nineties, former FDP (liberal party) secretary of state in the BMJ (ministery of justice), active promoter of patentability expansion and of international unification of the patent system.
\end{quote}
\filbreak

\item
{\bf {\bf Gert Kolle et Brevets Logiciels\footnote{http://swpat.ffii.org/acteurs/kolle/index.en.html}}}

\begin{quote}
As a young scholar in the 70s, Gert Kolle quickly became the leading german theoretician on the limits of patentability with respect to computer programs.  Kolle wrote his doctoral thesis at the Max-Planck Institute and Patent Law on this subject and published several deep-searching articles in GRUR from the early 70s to the early 80s.  These articles confirmed and refined the view of the courts that there is no room for patenting computer programs if the notion of technical invention is taken seriously.  Kolle's works wor often cited by German courts as a foundation for their refusal to grant software patents.  Later Kolle became an official at the European Patent Office.  Currently he is their head of diplomacy, and he occasionally gives talks where he explains and justifies the current software patenting policy of the EPO.
\end{quote}
\filbreak

\item
{\bf {\bf Greenpeace: OEB et amis violent la Loi\footnote{http://swpat.ffii.org/papiers/greenpeace-epo/index.en.html}}}

\begin{quote}
Greenpeace has since the beginning of the 1990s been fighting against the extension of the realm of patentability to elements of life and animals.  Much of this extension was carried out by decisions of the president of the EPO which were in blatant contradiction to the written law.  Later many of these decisions were ex posteriori legalised by EU directives which again were results of abusive legislative procedures.  Greenpeace has never shyed to raise these charges in unambiguous language.  Their patent specialist Christoph Then from Hamburg explains in detail that the EPO is not subject to any effective control mechanisms and why this has happened.
\end{quote}
\filbreak

\item
{\bf {\bf Repr\'{e}santation des Employee de l'OEB (http://www.usoeb.org/)}}

\begin{quote}
The Workers Union of EPO, has occasionally featured articles by staff complaining about lack of democratic control of the EPO, it not being bound to human rights or anything else, difficulty to do a good job with the many pressures they get from above, etc. An article titled ``A Public Service Organisation out of control?'' contained much of what is written in our documentation, including the story of former EPO president Paul Br\"{a}ndli's coroporal injury to a union leader.  However the page was removed from the Net in summer 2002.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}\begin{sect}{tasks}{Questions, Choses a faires, Comment vous pouvez aider}
\ifmlhttasks
\begin{itemize}
\item
{\bf {\bf Comment vous pouvez nous aider a terminer le cauchemare des brevets logiciel (http://swpat.ffii.org/groupe/gunka/index.de.html)}}
\filbreak

\item
{\bf {\bf Comment vous pouvez nous aider a terminer le cauchemare des brevets logiciel (http://swpat.ffii.org/groupe/gunka/index.de.html)}}
\filbreak

\item
{\bf {\bf translate this page}}

\begin{quote}
Start by inserting the passages from the EPC
\end{quote}
\filbreak

\item
{\bf {\bf Document some more basic facts and figures, including organisation chart, salaries, patent numbers, history etc}}
\filbreak

\item
{\bf {\bf Collect quotations about the EPO that could serve as an introductory section to this page}}
\filbreak

\item
{\bf {\bf Document some of the suicide cases and some more facts and examples that illustrate the working climate at the EPO.}}
\filbreak

\item
{\bf {\bf Document the backgrounds of the patentability extension decisions.  Who are Van den Berg and the other TBoA members?  What do policy notes under T1173/97 etc mean?}}
\filbreak

\item
{\bf {\bf Some people have asserted that the breakdown of patentability standards in the mid 80s was to a large extent due to the British group (http://swpat.ffii.org/acteurs/uk/index.en.html)'s reluctance to appreciate abstract doctrines such as that of the technical invention. Try to find out more.}}
\filbreak

\item
{\bf {\bf Find more evidence about the organisational climate and pressures within the EPO, more examples of career patterns of EPO officials, the position of the EPO in the patent family, the position of the patent family in political and economic institutions etc}}
\filbreak

\item
{\bf {\bf Find out more about the EPO's connections to the European Commission, the CEC representation in Munich, the EPO liaison office in Brussels, the EPO as a subcontractor of many CEC projects (such as EU-China IP exchange, CEC-financed export of european patent system to countries such as Malaysia etc)}}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: mlatex ;
% End: ;

