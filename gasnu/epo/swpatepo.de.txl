<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Europäisches Patentamt: Hoch über dem Gesetz

#descr: Das Europäische Patentamt finanziert sich durch Einnahmen aus
Patentgebühren.  Es kann über diese Einnahmen frei verfügen.  Je mehr
Patente es erteilt, desto besser geht es seinen Bediensteten.  Es
bestimmt seit den späten 80er Jahren de facto selber, nach welchen
Kriterien es die Patent erteilt.  Seit Mitte der 90er Jahre ist das
EPA bestrebt, für diese Tatsache gesetzliche Rückendeckung zu
erhalten.  Das EPA versteht sich als Dienstleister an seine Kunden,
den Patentinhabern.  Es kostet einen Prüfer wesentlich mehr Aufwand,
ein Patent zurückzuweisen, als es zu erteilen.  Das EPA ist personell
und finanziell viel besser ausgestattet als die zuständigen Stellen in
Berlin oder Brüssel.  Daher ist das EPA trotz theoretischer
politischer Enthaltsamkeit das eigentliche politische Zenrum der
Patentbewegung in Europa.  Eine wichtige Rolle hierbei spielt auch der
Ständige Beirat aus Industrie-Patentanwälten, SACEPO.

#Eni: Fingerabdruckslogo des EPA

#Tit: Das Fingerabruck ist ein Symbol des %(q:Geistigen Eigentums), wie man
es vom Urheberrechtsdenken her kennt:  die unverwechselbar
individuelle Schöpfung, die kaum jemand unabhängig vom Urheber neu
schaffen würde.  Das EPA wurde 1973/78 mit dem Auftrag gegründet, das
Prinzip des Eigentums in individuellen Schöpfungen auf die Welt der
Naturkräfte anzupassen, wo es viele schwer zu findende und leicht
nachzuahmende Anwendungslösungen gibt.  Inzwischen tut das EPA
allerdings genau das Gegenteil: es erteilt Patente auf breite
allgemeine Prinzipien und ist bestrebt, diese Praxis in die Sphäre des
Urheberrechts zurückzuexportieren und damit das Prinzip der
individuellen Schöpfung auszuhebeln.  Somit symblisiert das EPA-Logo
eine von mehereren erstaunlichen Entwicklungen, die wir hier
nachzuzeichnen versuchen.

#Tet: Das EPA: eine besondere Rechtskonstruktion

#Dip: Auflösung des Erfindungsbegriff von Art 52 EPÜ

#DtW: Missachtung der Patentierbarkeitsausnahmen von Art 53 EPÜ

#Eio: Aufweichung des Neuheitsbegriffs von Art 54 EPÜ

#Lod: Formelle Sicht von Art 52 EPÜ: Niedrigste Erfindungshöhe der Welt

#Ctm: Ständig sinkende Prüfungsqualität

#SKe: BEST: schlechtestmögliche Prüfungsqualität, gesetzeswidrig eingeführt,
2000 legalisiert

#Pce: Probleme der Neuheitsrecherche zunehmend komplex, EPA weit
abgeschlagen

#Ras: Rechtsfreier Raum, schlechtes Arbeitsklima

#KeW: Karrieren zwischen Überwachenden Organen und EPA

#BAW: Unabhängigkeit der Beschwerdekammern nicht gewährleistet

#Vgi: Vorenthaltung von Patentinformationen.

#Tli: Hier konnte das EPA aufgrund seiner außergesetzlichen Stellung als
Leithammel agieren, die Herde der europäischen Patentbewegung folgte
willig, unter Berufung auf die Autorität des EPA natürlich.

#A4W: Art 54 EPÜ sagt

#EnS: Eine Erfindung gilt als neu, wenn sie nicht zum Stand der Technik
gehört.

#DPi: Den Stand der Technik bildet alles, was vor dem Anmeldetag der
europäischen Patentanmeldung der Öffentlichkeit durch schriftliche
oder mündliche Beschreibung, durch Benutzung oder in sonstiger Weise
zugänglich gemacht worden ist.

#Dlu: Daraus lässt sich nicht entnehmen, dass alles neu ist, was nicht in
einem einzigen zusammenhängenden Dokument beschrieben ist.

#Ntc: Neu ist, was nicht vorher zugänglich war.  Auch Informationen, die ein
Fachmann durch Lesen mehrere Dokumente in Erfahrung bringen kann, sind
nicht neu.  Es besteht sachlich wenig Grund zu der Annahme, dass ein
bekanntes Verfahren unbedingt irgendwo in einem Dokument
zusammenhängend beschrieben worden ist.  Viele Informationen werden
nicht in Schriftform weitergegeben, und ob etwas als ein
zusammenhängendes Dokument vorliegt oder nicht, hat mit der Frage der
Neuheit nichts zu tun.  Auch das DP(M)A sah dort zumindest bis vor
kurzem keinen zwingenden Zusammenhang.  Das EPA hat den Begriff der
Neuheit durch die Festlegung auf ein einziges Dokument offenbar
ausgehebelt.  Zugleich ist %(q:erfinderische Tätigkeit) ebenfalls zu
einem stumpfen Kriterium gemacht worden, welches vor allem dazu zu
dienen scheint, die Löcher teilweise zu füllen, die das EPA in den
Neuheitsbegriff gerissen hat.  Nicht naheliegend ist etwas laut EPA
dann, wenn der Fachmann die Information beschaffen musste, indem er
mehrere Dokumente konsultierte, und wenn es keine zwingenden Gründe
gab, die Informationen aus diesen Dokumenten miteinander zu
kombinieren.

#UWi: U.a. durch diese Aushöhlung der Begriffe %(q:Neuheit) und
%(q:Nichtnaheliegen) ist es dem Patentwesen im Laufe der Zeit offenbar
gelungen, die Zahl der Patente und kassierbaren Lizenzgebühren zu
vervielfachen.  Letztere hat sich laut EPA-Angaben seit 1990
verzehnfacht.

#Dha: Diese Aushöhlung war u.a. die Folge davon, dass ein Patentprüfer keine
praktikable Moeglichkeit hat, Anmeldungen zurueckzuweisen: der
Anmelder kann durch immer neue Briefe und Draengen auf %(q:rechtliches
Gehör) (dessen Nichgewährung einen schweren Formfehler mit sehr
unangenehmen Folgen fuer den betroffenen Prüfer) eine endgültige
Entscheidung hinauszögern.  Letztendlich bleibt dann nur die
muendliche Verhandlung: die ist aber ein Riesenaufwand und ist
bestenfalls einigemal im Jahr drin. Da hilft dann nur noch eins:
Anforderungen soweit runterschrauben, dass es durchgeht. Die
entsprechenden Voten lesen sich dementsprechend wie die Ergüsse von
Schwachsinnigen....

#Bre: Bevor es das EPA gab, gab es eine Organisation namens IIB (Institut
International des Brevets) mit Sitz in Den Haag. Mitgliedsstaaten
waren damals Frankreich, Belgien, die

#Ais: Als um 1978 das EPA kam, wurde das IIB in Den Haag kurzerhand zur
Rechercheabteilung (GD1) umfunktioniert. Gleichzeitig wurde der Status
der dort Beschäftigten, die damals alle einen Diplomatenpass hatten,
auf einen Pseudodiplomatenpass reduziert und die Bezahlung wurde auch
gesenkt. Das sorgte für viel böses Blut und Frust und schlechtere
Recherchen.

#WWo: Wegen der Krux des %(q:rechtlichen Gehörs) und der Tatsache, dass man
jeden noch so dümmlichen Brief des Anwalts beantworten muss und ihn
erst zurueckweisen kann, wenn keine Argumente mehr auftauchen (was
natürlich nie der Fall ist) und zur Zurückweisung praktisch immer eine
aufwendige mündliche Verhandlung erforderlich ist, war eines bereits
festgeschrieben: es würde nur in Ausnahmefaellen zurückgewiesen werden
und allenfalls eingeschränkt und dann erteilt werden.

#Dfd: Das Problem mit dem %(q:rechtlichen Gehör) besteht auch in dem Fall,
dass der Prüfer die Ansprüche einzuschränken versucht.  Da kann sich
der Anwalt auf alle möglichen Arten querlegen: sehr beliebt ist bloß
mikrometerweise nachgeben oder neue Sätze von Ansprüchen mit etwas
anderer Formulierungen und etwas anders gelagertem Umfang. Es muss nur
genug in der Gesamtheit offenbart sein, dass sich immer wieder etwas
anderes herausgreifen lässt, man also nicht mit Art. 123
(ursprüngliche Offenbarung) nicht in Konflikt kommt. Sehr nützlich
hierfür ist eine Beschreibung riesiger Länge (z.B. n x 100 Seiten). Da
der Prüfer nur für den Erstbescheid und fuer den Abschluss eines
Verfahrens je einen halben Punkt bekommt und eine bestimmte
Mindestpunktzahl im Jahr zu erreichen hat (und nur dann Aussicht auf
Beförderung hat, wenn er erheblich mehr erreicht), kann er sich nicht
allzuviele Bescheide pro Vorgang leisten.  Konsequenz: runter mit den
Anforderungen und ab durch die Mitte....

#Jss: Jene, die viel produziert haben, sprich besonders lasch waren, sind
heute Direktoren, d. h. Chefs von Direktionen (so heißen
Prüfungsabteilungen).

#Dve: Die miese Prüfungsqualität ist damit von vornherein ins System
eingebaut.

#Vja: Vor 20 Jahren hatten die Prüfer noch erheblich weniger Punkte zu
erreichen, da die Anmeldezahlen weit geringer waren.  Das änderte sich
allerdings rasch, nachdem sich zeigte, dass auch damals schon viel
Dubioses durchkam und Patentieren fuer die Industrie einfach lohnend
wurde. Daraufhin mussten die Prüfer einfach mehr Anmeldungen
wegbringen, damit der Rückstau (backlog) nicht zu groß wurde. Damit
wurde die Prüfungsqualität noch schlechter, was eine weitere Erhöhung
der Anmeldezahlen nach sich zog. Und so weiter....

#Vmv: Verschärft wurde die Situation noch dadurch, dass Ende 1991 eine
unfähige Statistikkommission ein Absinken der Anmeldezahlen
vorraussagte und der damalige Präsident Paul Brändli einen
Einstellungsstopp verfügte, der bis zum Ende seiner Amtszeit in Kraft
war.

#AWe: Als dann Kober auf Teufel komm raus einzustellen anfing, war das mit
einer ganzen Reihe von Problemen behaftet: der Ruf des EPA war bereits
durch eine Reihe Skandale schwer geschädigt, so dass schwer an gute
Leute heranzukommen war. Ferner war es auch so, dass die EPA
Entlohnung, die damals seit 10 Jahren nicht mehr wesentlich erhoeht
worden war, auch nicht mehr recht mithalten konnte. Damit das EPA
lohnmäßig noch weniger mithalten konnte, führte Kober für die
Neueinstellungen sogar die berühmten Negativgehaltsstufen ein (und
teilweise nach Protesten wieder zurückgenommen), so dass diese weniger
bekamen als die früheren Neulinge.  Die Folge: es wurden Leute
eingestellt, die zu anderem nicht zu gebrauchen waren, auf die
früheren hohen Standards musste verzichtet werden. Und all die Leute
mussten auch noch ausgebildet werden - das bedeutete aber, dass
etliche Prüfer zumindest zum Teil vom Prüfen abgezogen und zur
Ausbildung der Neulinge herangezogen werden musste, so dass auch noch
weniger Prüferstunden für immer mehr Arbeit zur Verfügung standen. Das
ging natürlich wieder mal auf Kosten der Qualität (sofern man von
einer solchen zu dem Zeitpunkt überhaupt noch sprechen konnte).

#Ueu: Und jetzt ist dieselbe halt soweit abgesunken, dass ihre Ergebnisse
(ein Riesenhaufen Schrottpatente) überall unübersehbare Probleme
bereiten und Schäden verursachen.

#Dre: Der Gipfelpunkt war dann seit Mitte der 90er Jahre die Bemühung, die
Trennung zwischen Recherche und Sachprüfung aufzuheben, und zwar unter
dem beschönigenden Kürzel BEST (Bringing Examining and Search
Together).  Das war selbst bei den Anmeldern hochumstritten und laut
EPÜ bis 2000 natürlich illegal.

#AeW2: Artikel 17 EPÜ:

#Dtu: Die Recherchenabteilungen gehören zur Zweigstelle in Den Haag.  Sie
sind für die Erstellung europäischer Recherchenberichte zuständig.

#DWn: Das besagt: Als die GD2 in München, welche NICHT der GD1 in Den Haag
untersteht, anfing, auf eigene Faust an Stelle von GD1 zu
recherchieren, verstieß sie damit gegen Art. 17 EPÜ.

#Gea: Rezensionen

#AeW: Aus dieser Zeit stammt auch die berühmte Bitte einer Schweizer Firma,
%(q:ihre Anmeldung nicht dem BEST - Verfahren zu unterziehen).

#Mtn: Im November 2000 wurde BEST per EPÜ-Änderung abgesegnet und allgemein
eingeführt.  Somit betreibt nun jeder Prüfer sowohl Recherche als auch
Sachprüfung. Man bedenke wohl, was das unter den oben geschilderten
Bedingungen heißt: es ist jetzt möglich, Dokumente, die einer
Direkterteilung im Weg stehen, einfach unter den Tisch zu kehren.
Allerdings ist es ohnehin nicht wahrscheinlich, dass man viel findet:
die Suchwerkzeuge sind noch wesentlich schlechter als
durchschnittliche Internetsuchmaschinen (die den zu stellenden
Anforderungen natürlich auch nicht genügen), und die Bedienung ist
vorsintflutlich (so wie eben IBM VM/CMS auf Großrechnern ist).

#ZWr: Zwischen 1991 und 1993 war die Prüfungsqualität am EPA noch passabel
-- zumindest in einigen Bereichen (nicht Software) besser als die des
USPA und JPA. Nach der Inbetriebnahme der %(q:Pschorrhöfe) in der
Baierstrasse allerdings ging es sehr merkbar bergab, so etwa bis 1996.
Ab 1996 war dann die Talfahrt exponentiell.  Heute ist das
Patentprüfungswesen am EPA nur noch ein Witz.

#Dhu: Derzeit (2002) läuft das Sitzabkommen des EPA mit der niederländischen
Regierung aus und letztere hat beliebig wenig Interesse an dessen
Verlängerung. Gleichzeitig plant das EPA in München neue
Räumlichkeiten und stellt wie verrückt ein, während Den Haag (GD1)
seit 2 Jahren Einstellungsstop hat.

#OnW: Offensichtlich zeigt BEST bereits Wirkung: Es ist natürlich klar, dass
mit der Zusammenlegung von Prüfung und Recherche entweder die GD1 oder
die GD2 überflüssig würden. Vernichtungskämpfe beider gegeneinander
sind schon länger im Gange. Wenn das Sitzabkommen mit den
Niederlandenden Bach runtergeht hat dementsprechend die GD2 gewonnen
und die GD1 wird eliminiert.

#Dne: Das Betriebsklima in Den Haag hat dementsprechend neue Tiefen
erreicht, was zu der Verschlechterung der Recherchequalität natürlich
beiträgt.

#UgA: Um recherchieren zu können, braucht man eine entsprechend aufbereitete
Dokumentation, und zwar auch und vor allem von Nichtpatentliteratur. 
Aber dank der galoppierenden Patentinflation nimmt auch die
Patentliteratur in einem Maße zu, dass man kaum noch mithalten kann,
und angesichts nicht vorhandener Erfindungshöhestandards muss jede
Kleinigkeit, die vielleicht kaum dokumentiert ist, weil niemand das
für erwähnenswert hält, wie eine Nadel in einem exponentiell
wachsenden Heuhaufen recherchiert werden.  Der Arbeitsaufwand für eine
Pflege und Klassifikation der Dokumentation ist erheblich und steigt
ständig, wobei die Sucheffizienz für den Prüfer eher sinkt als steigt.
Da die EPA-Leitung von alledem keine Ahnung hat, wurde in dieser
Hinsicht nicht viel getan. Als der Chefbibliothekar des EPA, Gerhard
Kruse, das aendern und Nägel mit Köpfen machen wollte, wurde er brutal
ausgebremst.  Logisch - mit guter Dokumentation würde weniger erteilt,
was dem politischen Ziel, möglichst viel zu erteilen, natürlich
zuwiderläuft.

#EGd: Eine große Organisation mit genug Geld und vor allem eigener
Dokumentation und Recherchemitteln könnte wohl locker jedes zweite
EPA-Patent umwerfen, und zwar auch auf herkömmlichen gut
dokumentierten Gebieten.  Zur not tut es auch nur viel Geld und die
Recherche einer darauf spezialisierten Firma zu überlassen. Was die
meisten nicht wissen: das ist ein ganzer Gewerbezweig mit eigenen
Grossfirmen, deren Kunden die Patentaemter der ganzen Welt sind. 
Unter diesen Großfirmen die größte und bekannteste (aber nicht
unbedingt beste) ist die englische Firma Derwent. Das EPA ist
Großkunde bei Derwent.

#Zhe: Zur systematischen Gesetzesverletzung hinzu kommt, dass das EPA in
einem weitestgehend rechtsfreien Raum lebt.  Der Hoheitsbereich der
deutschen Polizei endet vor den Türen des EPA.  Gewerkschaften sind
innerhalb des EPA %(wg:weitgehend) rechtlos.

#Eiu: Es gibt ein paar formale Gesetze, die zumindest die Existenz einer
Personalvertretung gewaehrleisten. Viel ist es aber nicht.

#Egt: Folgendes kam uns aus dem Kreise der EPA-Mitarbeiter zu Ohren und
erscheint glaubwürdig, ist aber noch näher zu prüfen:

#Egt2: Eine Gewerkschaftsvertreterin wurde kurz vor Kobers Amtsantritt von
dessen Amtsvorgänger Paul Brändli in den Bauch getreten und verletzt
ins Krankenhaus eingeliefert.   Der EPA-Präsident kam unbelangt davon,
weil er sich auf seine Immunität berief.

#Ill: Ingo Kober gilt weithin als der schlechteste EPA-Präsident, den es je
gab: wegen seiner Unfähigkeit, einen einvernehmlichen Dialog mit dem
Personal zu fuehren sind gewerkschaftliche Kampfmaßnahmen im EPA ein
Dauerzustand. Das aber bedeutet, dass erheblich weniger Anmeldungen
durchgezogen werden, als das normal ueblich ist, und da die
Signatarstaaten 50 % der EPA-Einnahmen abgreifen (der jeweilige Anteil
bemisst sich nach einem Schlüssel, der auf den Anmeldezahlen der
jeweiligen nationalen Patentämter beruht), bedeutet das
Mindereinnahmen fuer Selbige. Und das ist begreiflicherweise dem
Verwaltungsrat gar nicht recht, weshalb man Kober nicht wohlgesinnt
ist (die offizielle Begruendung dafuer lautet, dass er %(q:nicht in
der Lage sei, den sozialen Frieden im EPA zu erhalten)).
Dementsprechend wurde er auf ein Maß demontiert, dass es dem
Verwaltungsrat erlaubt, im EPA nach eigenem Gutdünken zu schalten und
zu walten, wie es ihm beliebt.

#DWe: Das Schlimme am EPA-Verwaltungsrat sind die in letzter Zeit beliebten
%(q:ausserordentlichen Sitzungen) -- Geheimsitzungen inoffizieller
Natur, in denen hinter verschlossenen Tueren alles ausgekasperlt und
dann auf den offiziellen Sitzungen einfach beschlossen wird. Da lassen
sich Dinge wie weitreichende EPÜ-Änderungen bequem vorbereiten, so
dass man die Öffentlichkeit damit ratzfatz überfahren kann. Man stelle
sich vor, das Gemeinschaftpatent wäre schon da....lustig, was?

#Mal: Manche Leute lästern über den Vatikan, in dem keine demokratischen
Verhältnisse herrschten und der deshalb nicht in die EU und vielleicht
auch nicht in die UNO aufgenommen werden sollte. Man fragt sich, ob
nicht das EPA einen Antrag auf Aufnahme in die UNO stellen sollte.  Es
erfüllt, soweit ich sehen kann alle Bedingungen der Staatlichkeit.  Ob
der Antrag vielleicht wegen mittelalterlicher Zustände im EPA-Land
zurückgewiesen werden könnte, ist eine andere Frage.  Man sollte da
die Latte nicht zu hoch hängen, denn in vielen Staaten der Welt geht
es Gewerkschaften schlimmer als beim EPA.  Indem man das EPA in die
UNO aufnähme, könnte man es zumindest an diverse
Menschenrechtsverträge binden.

#OWu: Ähnliches gilt übrigens auch für die Europäische Kommission, die von
niemandem, auch nicht vom Europaparlament, so recht kontrolliert wird.
 Gäbe es nicht das Brüsseler Einfallstor, so hätten Idiotien wie
EuroDCMA, SSSCA, und Software- und Genpatente wohl in kaum einem
europäischen Land eine Chance, ernsthaft ins Gespräch zu kommen.

#Ztm: Zusammenfassend laesst sich sagen, dass im EPA der Verwaltungsrat
ungehindert regiert: und der besteht im Wesentlichen aus oberen
Chargen nationaler Patentaemter.  Irgendeine Art wirksamer Kontrolle
aus den Herkunftsländern besteht allerdings nicht. Es kursieren recht
ernstzunehemnde Gerüchte, dass Patentabteilungen der Großindustrie und
Patentanwaltsverbände als graue Eminenzen im Hintergrund federführend
mitwirken.

#Dsd: Diejenigen, die eigentlich das EPA überwachen sollten, sind Anwärter
auf Posten im EPA.

#Kha: Kober kommt vom DE-Justizministerium, Brändli war Patentamtspraesident
in der Schweiz, Frau Leutheuser-Schnarrenberger von der FDP ist heute
ebenso wie der ehemalige BMJ-Patentreferent Peter Mühlens Direktor im
EPA....eine endlose Geschichte.

#Air: Arbeitsgruppe

#Dnd: Die Mitglieder der Grossen Beschwerdekammer und der Beschwerdekammern
werden für einen Zeitraum von 5 Jahren ernannt und können während
dieses Zeitraums ihrer Funktion nicht enthoben werden, es sei denn,
dass schwerwiegende Gründe vorliegen und der Verwaltungsrat auf
Vorschlag der Großen Beschwerdekammer einen entsprechenden Beschluss
fasst.

#Sii: Sollte die Bedenklichkeit des Obigen für die Unabhängigkeit noch
unklar sein, dann wird er mit Artikel 11(3) vollständig klar:

#Dwe: Die Mitglieder der Beschwerdekammern und der Großen Beschwerdekammer
einschliesslich des Vorsitzenden werden auf Vorschlag des Präsidenten
des Europäischen Patentamtes vom Verwaltungsrat ernannt. Sie können
vom Verwaltungsrat nach Anhörung des Präsidenten des Europäischen
Patentamtes wiederernannt werden.

#Iii: Im Klartext: wer nicht spurt, wird nicht wiederernannt.

#SlW: Sollten noch irgendwelche Zweifel an der rein politischen Natur der
Auswahl hoher Beamter im EPA bestehen, dann beseitigt Artikel 11 auch
diese:

#Dio: Der Präsident des Europäischen Patentamts wird vom Verwaltungsrat
ernannt.

#Des: Die %(tan|Vizepräsidenten|Leiter der Generaldirektionen) werden nach
Anhörung des Präsidenten vom Verwaltungsrat ernannt.

#Ula: Und so lernt das auch jeder Prüferneuling: %(q:In dem Laden kannst du
es maximal bis zum Hauptdirektor bringen.)

#Win: Was auch nicht immer stimmt: der jetzige Vizepräsident der GD2, Herr
Kyriakides, seines Zeichens ein Zypriot mit britischem Pass, war
früher sogar Prüfer (selten!).  Das war aber nicht der Grund, weshalb
er den Job bekam: der Grund war, dass die englische Delegation des
Verwaltungsrates dahingehend drängte (was natürlich bedeutet, dass er
in seiner Delegation die richtigen Leute kennt). Sein Vorgänger, ein
Herr Vivian, war hingegen nie Prüfer gewesen.  Ein Glücksfall:
Kyriakides steht in dem Ruf, weit besser als sein Vorgänger zu sein.

#Thi: The patents which the EPO grants are still available only in the form
of graphic files, which are wrapped into one PDF file per page, using
a LZW compression (which moreover is patented and therefore not
accessible to free software.)

#TWW: Thus the EPO has managed until this day to keep its patent database,
at least the valid (B1) writings, outside of the scope of any Internet
search engine.

#Tsr: This is a deliberate policy, designed to create a market for
proprietary information providers.

#Tln: Things have already improved quite a bit in this respect.  In the mid
90s, the EPO hat a clear and explicity policy of withholding the very
information whose public disclosure and diffusion is said to
constitute the meaning of the patent system.  They were spread out as
graphic files on a proprietary difficult-to-copy system dependent on
Microsoft Windows and sold for the equivalent of several 1000 EUR per
year.  The EPO's copyright notice moreover labeled the content of the
CDs as %(q:proprietary information), although according to existing
laws patents are public and the copyright on them cannot be enforced.

#Tcn: This policy should be considered symptomatic of the EPO management's
attitude toward the public interest and toward the purpose of system
which it is supposed to represent.

#EAi: EPO Staff Union 2002: A Public Service Organisation Out of Control?

#TuW: Die Gewerkschaft der Angestellten des EPA beschreibt, wie Funktionäre
des Europäischen Patentamtes (EPA) und insbesondere sein
Verwaltungsrat sich über Gesetze hinwegsetzen und sich dank faktischer
Gesetzesgebungsmacht zunehmend selbst unbeschränkbare Vollmachten
genehmigen und diese in einer für Europas Bürger schädlichen Weise
ausnutzen.  Diese Webseite war im Frühjahr 2002 kurzzeitig zugänglich
und wurde dann vom Netz genommen.  Eine Kopie bleibt in diesem
E-Verteiler-Archiv erhalten.

#Jte: John Savage, Prüfer am Europäischen Patentamt (EPA), hatte zugesagt,
den Lesern von Slashdot.com einen Fragenkatalog zu beantworten.  Nach
Erhalt des Fragenkatalogs sagte er dann unerwartet ab.  Offenbar
wollte die Presseabteilung des EPA keine unautorisierte Kommunikation
dieser Art.  Andere Patentprüfer sind bereits wegen solcher
unerlaubter öffentlicher Reden über das EPA diszipliniert worden.

#Eyr: Gewerkschaftliche Vertretung der Angestellten des EPA

#TtW: The Workers Union of EPO, has occasionally featured articles by staff
complaining about lack of democratic control of the EPO, it not being
bound to human rights or anything else, difficulty to do a good job
with the many pressures they get from above, etc. An article titled
%(q:A Public Service Organisation out of control?) contained much of
what is written in our documentation, including the story of former
EPO president Paul Brändli's coroporal injury to a union leader. 
However the page was removed from the Net in summer 2002.

#tai: Diese Seite übersetzen

#Sis: Zunächst %(EPC)-Zitaten einfügen.

#DWh: Document some more basic facts and figures, including organisation
chart, salaries, patent numbers, history etc

#CEt: Collect quotations about the EPO that could serve as an introductory
section to this page

#Dma: Document some of the suicide cases and some more facts and examples
that illustrate the working climate at the EPO.

#DWr: Document the backgrounds of the patentability extension decisions. 
Who are Van den Berg and the other TBoA members?  What do policy notes
under T1173/97 etc mean?

#SiW: Some people have asserted that the breakdown of patentability
standards in the mid 80s was to a large extent due to the %(uk:British
group)'s reluctance to appreciate abstract doctrines such as that of
the technical invention. Try to find out more.

#FeW: Find more evidence about the organisational climate and pressures
within the EPO, more examples of career patterns of EPO officials, the
position of the EPO in the patent family, the position of the patent
family in political and economic institutions etc

#FiU: Find out more about the EPO's connections to the European Commission,
the CEC representation in Munich, the EPO liaison office in Brussels,
the EPO as a subcontractor of many CEC projects (such as EU-China IP
exchange, CEC-financed export of european patent system to countries
such as Malaysia etc)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatepo ;
# txtlang: de ;
# multlin: t ;
# End: ;

