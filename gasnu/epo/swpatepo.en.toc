\contentsline {section}{\numberline {1}The special legal construction of the EPO}{3}{section.1}
\contentsline {section}{\numberline {2}Dissolving the invention concept of Art 52 EPC}{3}{section.2}
\contentsline {section}{\numberline {3}Disrespecting the Exceptions of Art 53 EPC}{3}{section.3}
\contentsline {section}{\numberline {4}Eroding the novelty concept of Art 54 EPC}{3}{section.4}
\contentsline {section}{\numberline {5}Formal view of Art 56 EPC: Lowest Non-Obviousness Standards of the World}{4}{section.5}
\contentsline {section}{\numberline {6}Continuously deteriorating examination quality}{6}{section.6}
\contentsline {section}{\numberline {7}BEST: assured worst examination quality, illegally introduced, legalised in 2000}{7}{section.7}
\contentsline {section}{\numberline {8}Novelty search problems increasingly complex, EPO lagging far behind}{8}{section.8}
\contentsline {section}{\numberline {9}Outlaw zone, poor working climate}{9}{section.9}
\contentsline {section}{\numberline {10}National controllers of the EPO aspiring for careers in EPO}{10}{section.10}
\contentsline {section}{\numberline {11}EPO Boards of Appeal lack Independence}{10}{section.11}
\contentsline {section}{\numberline {12}Withholding Patent Information from the Public}{12}{section.12}
\contentsline {section}{\numberline {13}Annotated Links}{12}{section.13}
\contentsline {section}{\numberline {14}Questions, Things To Do, How you can Help}{17}{section.14}
