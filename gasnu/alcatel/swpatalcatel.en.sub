\begin{subdocument}{swpatalcatel}{Alcatel and Software Patents}{http://swpat.ffii.org/players/alcatel/index.en.html}{Workgroup\\swpatag@ffii.org}{In 2002, the french telecom giant published a paper which complains in harsh words about negative effects of patents such as legal insecurity and diversion of funds from R\&D to litigation.  On the other hand, Alcatel, as a global telecom player, owns a large software patent portfolio, and Alcatel's patent department is actively lobbying politicians to assure that Alcatel's patenting efforts will pay off in Europe in the same way as in the US.  Jonas Heitto, Alcatel patent lawyer trained at MPI in Munich, seems to be present at every related meeting in Brussels and some national capitals.  He is not the only one.  Alcatel participated in the European Commission's ``Consultation Exercise'' of 2000 in its own name as well as in the name of IT associations such as ANIEL from Spain.  Alcatel has recently been pursuing a strategy ``fabless company'', i.e. outsource as much production as possible to partners in countries where production is cheap (e.g. China) while keeping a lean headquarter (financial and legal services and some R\&D) in France and/or the Bahamas/Bermudas, depending on where taxes are lower.  Patents are said to be helpful in implementing this strategy and in transferring money to tax havens.  In the rationale for her software patentability directive proposal, MEP Arlene McCarthy cites Alcatel as an example and stresses that patents are needed to protect European businesses at a time where production costs are high here.  Here we try to take a closer look at Alcatel's patent-related strategies and in particular their software patent applications at the European Patent Office.  Most of Alcatel's developments are in the area of software, and recently Alcatel is more than ever focussing on ``next generation Internet'' communication standards and applications.}
\begin{sect}{epat}{Alcatel Software Patents at the European Patent Office (EPO)}
\input{alcatel-epatents}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf Alcatel 2002: Frequent Patent Litigation Diverting Efforts of Technical Personnel\footnote{http://www.alcatel.com/finance/reports/2001/20-f/20f.pdf}}}

\begin{quote}
A report by Alcatel which mentions patent troubles encountered by Alcatel: \begin{quote}
{\it Like other companies operating in the telecommunications industry, we experience frequent litigation regarding patent and other intellectual property rights. Third parties have asserted, and in the future may assert, claims against us alleging that we infringe their intellectual property rights. Defending these claims may be expensive and divert the efforts of our management and technical personnel. If we do not succeed in defending these claims, we could be required to expend significant resources to develop non-infringing technology or to obtain licenses to the technology that is the subject of the litigation. In addition, third parties may attempt to appropriate the confidential information and proprietary technologies and processes used in our business, which we may be unable to prevent.}

{\it Our business and results of operations will be harmed if we are unable to acquire licenses for third party technologies on reasonable terms.}

{\it We remain dependent in part on third party license agreements which enable us to use third party technology to develop or produce our products. However, we cannot be certain that any such licenses will be available to us on commercially reasonably terms, if at all.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Alcatel 2000: Reply to CEC Software Patent Consultation\footnote{http://swpat.ffii.org/papers/eukonsult00/alcatel/index.fr.html}}}

\begin{quote}
A patent expert from the legal department of Alcatel applauds the European Commission's proposal for unlimited patentability, explains that all patents stimulate innovation and the fact that the US has the most advanced software industry sufficiently proves that software patents, just like patents in any other field of technology, stimulate software innovation.  Moreover, the most important reason to support patents is that small and medium enterprises (SMEs) badly need them.
\end{quote}
\filbreak

\item
{\bf {\bf Alcatel 2000: 870 patent applications\footnote{http://www.alcatel.com/finance/reports/2000/pdf\_gb/report.pdf}}}

\begin{quote}
Report to shareholders: ``In 2000, Alcatel devoted Euro 2.8 billion to its R\&D budget and filed for 870 patents.''
\end{quote}
\filbreak

\item
{\bf {\bf Alcatel 1999: Policy on Patents and Licenses\footnote{http://www.alcatel.com/finance/reports/1999/pdf/07\_otherinfo.pdf}}}

\begin{quote}
A regular report to shareholders with a section on patent policy and disputes
\end{quote}
\filbreak

\item
{\bf {\bf Alcatel 1999: Policy on Patents and Licenses\footnote{http://www.alcatel.com/finance/reports/1999/pdf/00\_fullreport.pdf}}}

\begin{quote}
A regular report to shareholders with a section on patent policy and disputes
\end{quote}
\filbreak

\item
{\bf {\bf Alcatel 1998: Alliance with Thomson-CSF\footnote{http://www.alcatel.com/finance/reports/1998/20F/pdf/2-busine.pdf}}}

\begin{quote}
Report to shareholders.  Says inter alia ``Alcatel and Thomson-CSF created a joint research laboratory devoted to software architecture.''  ... ``Alcatel considers patent protection to be important to its businesses, particularly''
\end{quote}
\filbreak

\item
{\bf {\bf Alcatel 1997: Report for British MEP: 750 patent applications\footnote{http://www.alcatel.com/finance/reports/1997/uk/PDF/mepuk.pdf}}}

\begin{quote}
Alcatel has an exceptional patent portfolio. In 1997, the company applied for over 750 patents for its innovative processes, ...
\end{quote}
\filbreak

\item
{\bf {\bf Alcatel Builds Next Generation Networks\footnote{http://www.alcatel.com/}}}

\begin{quote}
Alcatel's website expresses Alcatel's desire to control upcoming Internet standards.
\end{quote}
\filbreak

\item
{\bf {\bf CEC/ETLA 2002: Technology policy in the telecommunication sector -- Market responses and economic impacts\footnote{http://swpat.ffii.org/papers/cec-telecom02/index.en.html}}}

\begin{quote}
A report issued by the European Commission, prepared by Heli Koski, ETLA, the Research Institute of the Finnish Economy, and the London School of Economics.  It studies the current situation of the European telecommunication enterprises and concludes with a warning against harmful effects of recent extensions of patentability on this sector.
\end{quote}
\filbreak

\item
{\bf {\bf German Universitarian Patent Law Decided\footnote{http://swpat.ffii.org/players/mpi/index.de.html}}}

\begin{quote}
The place of higher formation where Heitto and many others, forming a large influential network of colleagues spread over all kinds of institutions, were inculcated with the patent movement's philosophy and corporatist spirit.
\end{quote}
\filbreak

\item
{\bf {\bf McCarthy 2003-02-19: Amended Software Patent Directive Proposal\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy0302/index.en.html}}}

\begin{quote}
MEP McCarthy mentions Alcatel's success story and appears very preoccupied with the function of patents in an environment where production costs are lower elsewhere.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: latex ;
% End: ;

