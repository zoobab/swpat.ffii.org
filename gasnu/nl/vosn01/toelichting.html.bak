<HTML>

<HEAD>
  <TITLE>Vereniging Open Source Nederland en software patenten</TITLE>
</HEAD>

<BODY>

<H1>De problemen van patenten op software</H1>

<p>Een patent (ook octrooi genoemd) is een zware maatregel: het
verleent een monopolie.  Dat monopolies schadelijk zijn is algemeen
aanvaard.  Tegenover deze mogelijke schade staan bij een patent dan
ook twee zaken die de maatschappij ten goede komen: ten eerste
stimuleert zij het doen van een uitvinding, ten tweede stimuleert zij
de publicatie ervan.</p>

<p>In veel vakgebieden zijn patenten erkende hulpmiddelen die de markt
ten goede komen, maar het is ondertussen duidelijk dat in het geval van
software deze laatste twee winstpunten niet aanwezig zijn, en dat
zelfs het tegenovergestelde effect wordt bereikt: het wordt zeer
moeilijk om software op de markt te brengen.</p>

<p>Verreweg de meeste software wordt gepubliceerd, hetzij in voor mensen
leesbare vorm, maar veelal in ieder geval in machine-leesbare vorm,
zodat computers deze software direct kunnen uitvoeren.  Het is in
beide gevallen altijd mogelijk om 'in' de software te kijken, om te
zien welke ideeen de ingredienten ervoor waren.</p>

<p>De licencies waaronder gepubliceerde software op dit moment
uitgebracht wordt verwijzen ter bescherming naar het auteursrecht, wat
door alle marktpartijen als voldoende wordt ervaren, en wat door het
TRIPS verdrag, het internationale verdrag waar de europese
patentwetgeving aan moet voldoen, ook als voldoende wordt gedefinieerd.</p>

<p>Daarnaast biedt een verbod op 'reverse engineering', dat wil zeggen
het 'in' de software kijken, in sommige landen nog extra bescherming.</p>

<p>Voor de meeste software is er in ieder geval geen keus dan haar te
publiceren ter vermarkting, patent of geen patent.</p>

<p>Nu blijkt dat in het geval van software, het extreem moeilijk is om de
triviale patenten buiten de deur te houden.  Dit zijn patenten op
kleine maar fundamentele ideeen, die onderdeel uit maken van ieder groter stuk
software.  Er zijn verschillende mechanismes die eraan bijdragen dat
deze moeilijk te weren zijn.  Zo blijkt het extreem moeilijk om een
drempel van moeilijkheidsgraad aan te brengen in de wet, en heeft het
EPO, in navolging van haar amerikaanse tegenhanger het USPTO, de
afgelopen decennia haar maatstaven van 'innoverend' verregaand
opgerekt.  Hier draagt toe bij dat het EPO zelf belang heeft bij een
zo breed mogelijk patentbegrip.</p>

<p>Daar komt bij dat patenten op software vaak over deelapplicaties gaan
zoals optellen, of wachtwoordsauthenticatie, of het snel tekenen van
vierkantjes.  Dit zijn geen 'industrial applications' in de zin van de
patentwetgeving, maar losse onderdelen ervan.  Deze triviale patenten
zijn in de USA inmiddels aangewassen tot werkelijk ongelooflijke
aantallen.  In het oog springend voorbeeld is het 'one-click shopping'
patent van Amazon.com, waarin een standaardmethode voor websites is
geclaimd.   Ook is British Telecom op dit moment met een US patent op
hyperlinks bezig om middelgrote ISP's in de US op licensie kosten te jagen.</p>

<p>Door de wet 'ruim te interpreteren' (te overtreden) is het EPO er,
ondanks het expliciete verbod, in geslaagd om reeds 30.000
patentaanvragen op software te honoreren.  De ook de trivialiteit van
veel van deze patenten doen het ergste vrezen.
(<a href="http://swpat.ffii.org/vreji/indexen.html">http://swpat.ffii.org/vreji/indexen.html</a>)</p>

<p>Het grote probleem dat vele mensen in het midden en klein bedrijf in
de IT vrezen, en wat in de VS reeds bewaarheid is, is dat bij het op
de markt brengen van onze software, deze openligt voor alle
triviale-patenthouders om te inspecteren op inbreuk.  Een typisch
voorbeeld uit mijn eigen praktijk: Mijn eigen bedrijf Mind over Matter
is de R&amp;D afdeling van zijn klanten. Met drie werknemers van een van
mijn klant heb ik recent 100.000 regels programmacode gemaakt in 5
maanden.  Daarin zitten minstens 100 ideeen waar een (amerikaans)
patent op bestaat.  Ik heb alleen geen idee welke, want ik heb die 100
ideeen zelf bedacht als antwoord op mijn eigen problemen.  Maar er is
geen beginnen aan om de amerikaanse patentliteratuur na te zoeken.<br>
Nog niemand is er ook in geslaagd om de database van patenten en van
niet gepatenteerde 'prior art' zo toegankelijk te maken dat dit wel
doenlijk zou zijn.</p>

<p>Dit betekent dus dat de misstanden in de patenttoekenning voor de hele
IT industrie hun core business proces verschrikkelijk riskant maken.
Als publicatie van software al zo riskant wordt, dan is de stimulans
tot innovatie er ook helemaal vanaf.  De werkgelegenheid in de recent
zo opbloeiende E-commerce zal hier ernstig onder leiden.</p>

<p>Nog een voorbeeld: Branchegenoot en mede VOSN lid M. van der Boom
van HS-development (<a href="http://www.hsdev.com">http://www.hsdev.com</a>), zou zich uit
oogpunt van de risico analyse bijvoorbeeld min of meer gedwongen zien
om zijn software ter ondersteuning van de arbowetgeving op het gebied
van gevaarlijke stoffen van de markt te halen.  Dit betekent weer dat
zijn klanten, minstens 300 Nederlandse bedrijven acuut problemen
krijgen met het naleven van de arbowetgeving.</p>

<p>Ook het verbod op reverse engineering leidt in dit geval tot
inconsistente wetgeving: men moet de wet overtreden om inbreuk op
patenten aan te kunnen tonen.</p>

<p>Deze voorbeelden tonen al aan dat het zonder meer afschaffen van het
verbod op software patenten niet kan zonder nauwgezet de juridische en
economisce consequenties na te gaan en in elk geval maatregelen te
treffen die de ongewenste neveneffecten beperken.  In de huidige
voorstellen is hiervan geen enkele sprake.</p>

<H2>De situatie in de VS en Japan</H2>

<p>Het expliciete verbod op software patenten heeft de EU is tot dusver
min of meer gevrijwaard van de wantoestanden die de USA plagen op dit
gebied. Patenten dienen daar als wisselgeld tussen de grootste
marktpartijen, en vormen een sterk concurrentievervalsend middel, dat
mono- en oligopolies in de hand werkt en hoge drempels opwerpt voor
kleinere marktpartijen.</p>

<p>Er zijn legio voorbeelden van gevallen waar het US patentsysteem
tekortschoot in het beschermen van intellectueel eigendom, en zelfs
bijdroeg aan het onrechvaardig, maar helaas rechtmatig toeeigenen
ervan. (<a href="http://eurolinux.ffii.org/news/euipCAen.html#dg154">http://eurolinux.ffii.org/news/euipCAen.html#dg154</a>)</p>

<p>Het oorspronkelijke doel van patenten -- bevorderen van vooruitgang
door gepubliceerde vindingen te beschermen -- is in de VS allang in zijn
tegendeel verkeerd.  Op dit moment klinkt in de USA luider dan ooit de
roep om hervorming van de US Patent Office.
(<a href="http://www.bustpatents.com/corrupt.htm">http://www.bustpatents.com/corrupt.htm</a>)
Dus het zou extra wrang zijn als Europa zo'n historische misser beging.</p>

<p>In japan is recentelijk een wijziging in dezelfde richting gedaan,
onder verwijzing naar het TRIPS verdrag, dat echter geenszins dwingend
voorschrijft dat de EU deze stap ook neemt, integendeel, zelfs
expliciet vermeldt dat computerprogramma's onder het auteursrecht
mogen te vallen. (<a href="http://eurolinux.ffii.org/news/agenda/indexen.html">http://eurolinux.ffii.org/news/agenda/indexen.html</a>)</p>

<p>Als de EU de patenten op software (en op andere gebieden) liberaler
regelt dan Japan en de VS, levert ons dat een niet te onderschatten
concurrentievoordeel op.  Als wij daarentegen opgescheept raken met
het monstrum dat het EPO nu heeft voorgesteld, zullen wij tot lang
nadat de VS het hare al heeft afgeschaft met de hindernissen voor 
innovatie blijven zitten.
(een voorstel daarvoor was onlangs te lezen op
<a href="http://www.wired.com/news/politics/0,1283,39238,00.html">http://www.wired.com/news/politics/0,1283,39238,00.html</a>)</p>

<p>Zelfs de grote europese marktpartijen zullen in het nadeel zijn
tegenover hun amerikaanse tegenhangers, omdat die met hun amerikaanse
portofolio van triviale patenten onmiddelijk europese patenten kunnen
claimen, terwijl het tegenovergestelde niet kan.</p>

<H2>De voorstanders</H2>

<p>Aan de kant van de voorstanders van herziening, i.e. van het
patenteerbaar maken, vinden we bedrijven die de afgelopen jaren meer
geld hebben gestoken in rechtzaken om de concurrentie te dwarsbomen,
dan in innovatie, en belangrijker nog, de Intellectual Property
professionals, met name die van het European Patent Office, die hun in
het verleden onrechtmatig genomen beslissingen nu kunnen gaan
rechtvaardigen met een aangepaste wet in de hand. (<a href="http://www.greenpeace.de/GP_DOK_3P/STU_LANG/C05ST02.PDF">http://www.greenpeace.de/GP_DOK_3P/STU_LANG/C05ST02.PDF</a>)</p>

<p>Dhr. J.Mogg, voormalig directeur van het EPO, tegenwoordig DG onder
F.Bolkestein, is nu zelf de auteur van het bovengenoemde voorstel ter
'vereenvoudiging' van het verdrag, dat de invloed van het EPO, de uitvoering
waarover toch al geen democratische verantwoording afgelegd wordt,
vergroot tot desastreuze proporties.  Zo zal het EPO afgerekend worden op
het aantal verleende patenten, en niet op het aantal onderzochte, wat
ergoe zal leiden dat europa binnen geen tijd het bespottelijke aantal
van de amerikaanse patenten zal overtreffen.  Dat dit voorstel door de
board van het EPO met 10 tegen 9 is aangenomen illustreert al dat
zelfs binnen het EPO niet iedereen met de voorgenomen plannen kan
leven.</p>

<p>Voorts is duidelijk te merken het dat de Intellectual Property
gemeenschap in Europa alles aan gelegen is om deze wijziging snel en
zonder al te veel publiciteit af te handelen.  Vragen van journalisten
worden niet beantwoord, en de discussie wordt uit de weg gegaan.  Het
is dan ook niet verwonderlijk dat slechts enkelen op de hoogte zijn
van de issues.</p>

<p>Dat de patentwetgeving vereenvoudigd wordt door er delen van af te
schaffen is natuurlijk een bijzonder vreemd argument: om maar een
zijweg in te slaan: v&oacute;&oacute;r en tegenstanders van de gecompliceerde
Nederlandse euthanasiewetgeving zouden vreemd opkijken als deze werd
'vereenvoudigd' door alle wetsartikelen te schrappen die moord en
doodslag verbieden.</p>

<p>Het beleid van de EPO is de afgelopen tien jaar gericht geweest op
kwantiteit, en niet de kwaliteit van patenten, tot grote schade aan de
europese maatschappij en haar
wetten. (<a href="http://www.freepatents.org/law/agenda.pdf">http://www.freepatents.org/law/agenda.pdf</a>)<br>
Het is hoog tijd dat hierover democratische verantwoording wordt afgelegd.</p>

<p>In de motivatie voor de wijziging van artikel 52.c op bladzijde 37 van
het genoemde voorstel staat een apert onjuistheid: dat er een brede
concensus zou bestaan, dat software patenten gewenst zouden zijn.  Een
korte rondvraag onder IT bedrijven in Europa heeft een lijst met meer
dan 20.000 handtekeningen *tegen* de patenteerbaarheid van software
opgeleverd. (<a href="http://www.freepatents.org">http://www.freepatents.org</a>)</p>

<p>Onder deze tegenstanders van patenteerbaarheid van software vind u de
-- veelal kleinere -- bedrijven waar de innovatie echt plaatsvind.
Deze bedrijven vrezen dat hun orginele vindingen kunnen worden
afgepakt door horden goedbetaalde advocaten, in dienst van de grotere
marktpartijen.  Voorbeelden uit de USA de afgelopen tien jaar, en de
laatste paar maanden in het bijzonder, geven hen gelijk.</p>

<H2>Wat nu?</H2>

<p>Wij pleiten ervoor dat in Europa het verbod op patenten op software
gehandhaafd blijft.  Mochten er alternatieven voor de huidige
bescherming van software via het auteursrecht gewenst zijn, dan
dringen wij er op aan dat de juridische en economische consequenties
onderzocht worden, en dat de negatieve neveneffecten zorgvuldig worden
ingeperkt.</p>

<p>Het is van groot belang dat deze discussie interdisciplinair wordt
gevoerd en niet wordt overgelaten aan de Intellectual Property
juristen, die, om het voorzichtig uit te drukken, niet direct het
maatschappelijk belang hoeven te dienen.</p>

<p>Tot slot willen wij ervoor pleiten dat beslissingen over de
monopoliseerbaarheid van kennis onderhevig zijn aan een democratische
controle.</p>


</BODY>
</HTML>
