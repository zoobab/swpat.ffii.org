# LaTeX2HTML 99.2beta8 (1.42)
# Associate labels original text with physical files.


$key = q/sec:source/;
$external_labels{$key} = "$URL/" . q|kriteria_fenit_vosn.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:beperk/;
$external_labels{$key} = "$URL/" . q|kriteria_fenit_vosn.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:experiment/;
$external_labels{$key} = "$URL/" . q|kriteria_fenit_vosn.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:toets/;
$external_labels{$key} = "$URL/" . q|kriteria_fenit_vosn.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:natuur/;
$external_labels{$key} = "$URL/" . q|kriteria_fenit_vosn.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 99.2beta8 (1.42)
# labels from external_latex_labels array.


$key = q/sec:source/;
$external_latex_labels{$key} = q|6.1|; 
$noresave{$key} = "$nosave";

$key = q/sec:beperk/;
$external_latex_labels{$key} = q|6.2|; 
$noresave{$key} = "$nosave";

$key = q/sec:experiment/;
$external_latex_labels{$key} = q|6.3|; 
$noresave{$key} = "$nosave";

$key = q/sec:toets/;
$external_latex_labels{$key} = q|6.4|; 
$noresave{$key} = "$nosave";

$key = q/sec:natuur/;
$external_latex_labels{$key} = q|4|; 
$noresave{$key} = "$nosave";

1;

