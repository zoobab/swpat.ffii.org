<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Wilfried Anders und Logikpatente

#descr: Vorsitzender Richter des 20ten Senates des Bundespatentgerichtes, ehemaliger langjähriger Siemens-Mitarbeiter, Autor zahlreicher patentjuristischer Fachartikel, in denen er für die Patentierbarkeit von Geschäfts- und Programmlogik eintritt.  Von Patentanwälten und Patentverwaltungsbeamten als Meinungsführer gefeiert.  Anders nahm im November 2000 das Lob an, indem er sich in einer Rede vor der Patentanwaltschaft als %(q:progressiv) und seine Kollegen vom 17. Senat als %(q:konservativ) bezeichnete.  Anders hatte den Höhepunkt seiner Progressivität vielleicht 1999 erreicht, als er mit der Entscheidung %(q:Automatische Absatzsteuerung) Geschäftsmethoden für patentfähig erklärte und damit auch die Patentjuristen des EPA und der Europäischen Kommission verschreckte, die das lieber nicht so klar gesagt hätten.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatanders ;
# txtlang: de ;
# multlin: t ;
# End: ;

