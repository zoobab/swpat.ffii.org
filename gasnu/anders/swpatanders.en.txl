<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Wilfried Anders and Software Patents

#descr: Presiding judge at the 20th senate of the German Federal Patent Court (Bundespatentgericht), former Siemens employee,  author of many articles in law journals advocate patentability of software and business methods.  Praised by patent lawyers and patent administrators as an opinion leader.  Anders gladly accepted the praise by labelling himself as %(q:progressive) and his colleagues from the 17th senate as %(q:conservative) in a speech at a public patent policy discussion meeting in november 2000.  Anders may have he reached his preliminary climax of progressiveness when his senate officially declared business methods to be patentable in Germany through the Automatical Sales System (Automatische Absatzsteuerung) decision of 1999, a decision that scared even the patent lawyers from the EPO and the European Commission, who prefer not to put things so clearly.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatanders ;
# txtlang: en ;
# multlin: t ;
# End: ;

