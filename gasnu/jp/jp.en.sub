\begin{subdocument}{swpatjp}{Patent Inflation in Japan}{http://swpat.ffii.org/players/jp/index.en.html}{Workgroup\\swpatag@ffii.org}{Japan, although a world champion in the number of patents, has been rather passive in the patent inflation movement.  The patent officials in the government bent their patent law in reaction to pressures from the US government and the local patent lawyers.  The subject was hardly discussed anywhere.  Prof. Konno of Tokyo University has tried to prevent the patent inflation movement by publications and even by appealing to the highest courts.  In early 2002 his last appeal was rejected by the supreme court on the grounds that he as an individual was not entitled to sue the Japanese Patent Office (JPO).}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf JPO\footnote{http://www.jpo.go.jpg}}}
\filbreak

\item
{\bf {\bf KONNO Hiroshi: The Karmarkar Patent and Software -- Is Math Patentable?\footnote{http://swpat.ffii.org/papers/konno95/index.ja.html}}}

\begin{quote}
In this book which received great public attention in Japan, Prof. Konno, a famous specialist of operations research (applied mathematics) from Tokyo University, explains what is behind the recent drive to extend the scope of patentability to software and mathematics.  The book starts from a few fascinating examples of real advances in mathematical research, namely Narendra Karmarkar's interior point method of linear programming.  This method was a truly rare achievement of ingenuity with great value for large-scale industrial planning.  If any mathematical achievement could be suitable for patenting, then it was this method.  The AT\&T laboratories did receive a patent for this method in 1988.  However the Karmarkar patent did not create very significant revenues for AT\&T while causing significant inconvenience to the industry and destroying much of the positive interaction cycles that had previously characterised the cooperation between linear programming research and industry in the US.  The AT\&T Laboratories no longer produced any Nobel Prize winners after they became patent-oriented, and Karmarkar himself was isolated from the network of mathematic researchers, who soon left him lagging far behind.   Konno captures the reader with a fascinating introduction to linear programming, interwoven with a dramatic account of the battles that accompanied the Karmarkar patent within the mathematics community and the patent system.  Konno was on the podium or in the first row during the conferences where the drama around Karmarkar, linear programming and mathematical patents evolved.  Moreover it was Konno who filed an opposition to the Karmarkar patent in Japan.  He shows that the Japanese Ministery of Trade and Industry (MITI) pushed the Japanese patent office to bend Japanese patent law in an inconsistent direction.  Lawyers at the time explained that it was not necessary to be consistent but to go with the tide, which was inevitably pro software patents.  The MITI acted under the pressure from these lawyers and from the US government.   Konno's vivid examples suggest however that software patents are not here to stay.  They do not correspond to any need of the software industry.  They have stifled innovation and resulted in a massive blood transfer from the software industry to the litigation business. They created a new market of about 2 billion USD per year for the patent law professionals.  'You needn't be named Hercule Poirot to see who is the driving force behind all this', Konno remarks.  But there is hope, because the young generation has not lost sight of the problems.
\end{quote}
\filbreak

\item
{\bf {\bf The USPTO and the US Patent Lobby\footnote{http://swpat.ffii.org/players/us/index.en.html}}}

\begin{quote}
The patent lobby and lawyers in general wield great influence in the United States.  One Japanese book is titled ``Litigating a Country to Death -- The United States of America''.  Like in Britain, the patent system ran out of control rather early in the US.  In the 80s, this was partially reinterpreted as an american national policy.  Under the influence of the patent law lobby, the US government pushed other countries to allow patentability of everything under the sun according to US standards.  Jordan signed a bilateral treaty with the US in this sense in 2000.  Japan was heavily lobbied.  US lobbying has made itself felt in Europe as well, so that many, including the French president, have spoken about a strategic need to resist the US pressure.  Whether this US pressure is really based on a national policy in the interest of the USA is to be doubted.  It looks more like a paper tiger built up by US patent lawyers afraid to otherwise lose the position which they have acquired in the US.  But without doubt the USA is in the position of the early adopter of patent inflation.  While others were still unaware of illegal inflatory activities of their patent office, the USPTO moved ahead unhindered, so that most of the questionable patents fell into the hand of US companies.  In Europe, more than 2/3 of the software patents are in US hands, and it is somewhat natural for the US to press for their legalisation.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: latex ;
% End: ;

