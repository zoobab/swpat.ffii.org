<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Brevetti Software in Italia

#descr: The patent politicians in the Italian government have been among very
few who opposed program claims in the European Council of Ministers. 
Italian representatives in the European Parliament of both right and
left parties also voted against software patents, and some Italian
representatives, such as Marco Cappato, actively helped the campaign
against software patents.  The patent movement seems to be somewhat
weaker in Italy.  Some opposition against software patents has been
organised since 2000 or earlier.  Yet the knowledge about these
problems is as superficial and unreliable in Italian politics as
elsewhere.

#pit: Swpat in Italy

#mhm: Informations gathered by Simo Sorce

#sio: IT-Parl Discussion Forum

#wiW: IT-News Mailing List

#eet: Codice di Legge de Brevetti Italiano

#Wig: This text is part of a current project to reform the Italian
industrial property system, including patents and trademarks.

#Pco: Art 48 corresponds to Art 52 EPC but has slightly different wordings
for par 3 and omits %(q:aesthetic creations) from par 2.  This text is
part of a current project to reform the Italian industrial property
system.

#lWe: lo sapevate che c'é in progetto una riforma della proprietà
industriale. Il software non é neppure nominato.  La tipizzazione
della registrazione dei domain names... affidata a una %(q:Commissione
Nazionale per l'accesso ad Internet) ... %(q:coadiuvata da un collegio
consultivo formato da un massimo di quindici componenti da designare
tra docenti nelle università  di materie informatiche, giuridiche ed
economiche e tra gli operatori e gli utenti della INTERNET)
(selezionati da chi... non si sa).

#csm: Italian branch of %(AIPPI), on whose website the reform project
documents are found.

#aeW: Translate this page.

#hxi: Edit the %(wp:Wiki extension to this page).

#tWa: Edit the source of this page via %(CVS).

#ngo: Set up plans and budget for meetings with politicians and other
players.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatit ;
# txtlang: it ;
# multlin: t ;
# End: ;

