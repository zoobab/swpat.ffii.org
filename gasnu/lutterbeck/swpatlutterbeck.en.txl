<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Prof. Dr. iur. Bernd Lutterbeck and Software Patents

#descr: a professor at Berlin Technical University specialising in informatics related law.  Co-authored a government-ordered study on software patentability.  Supports the policy of the European Patent Office (EPO) but advocates that publication of software source code should not be considered an infringement.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatlutterbeck ;
# txtlang: en ;
# multlin: t ;
# End: ;

