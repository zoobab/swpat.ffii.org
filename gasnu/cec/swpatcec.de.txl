<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Aktivitäten der Patentbewegung im Namen der Europäischen Kommission

#descr: Die Generaldirektion Binnenmarkt wird, was Fragen des Patentwesens u.a. betrifft, völlig von der Patentbewegung beherrscht.  Sie stimmt immer mit der im Rahmen AIPPI, WIPO u.a. vereinbarten Linie überein und treibt diese Linie im Rahmen der EU-Kommission voran.  Die Mitarbeiter der GDBM sind normalerweise schon von ihrer Vorbildung her nicht in der Lage, etwas anderes zu tun, als den Konsens der internationalen Patentbewegung in europäische Richtlinien zu gießen.  Sie wollen mehr Patente um jeden Preis und lügen regelmäßig, wenn sie um eine Rechtfertigung verlegen sind.  Ihre Karrieren verlaufen typischerweise innerhalb der Institutionen der Patentbewegung: sie kommen z.B. von einem Patentinstitut und avancieren nach ihrem Dienst in Brüssel zu einem hohen Posten an einem Patentamt.  Die meisten von ihnen entstammen einem britischen Klüngel, der unter sich die Aufträge vergibt und dafür sorgt, dass keine unabhängige Kontrolle stattfinden kann.  Ein volkswirtschaftlich orientiertes Gegengewicht zur GDBM in Fragen der Patentgesetzgebung gibt es in der Europäischen Kommission bislang nicht.  In einzelnen Fällen regt sich leiser Widerstand von seiten der Generaldirektion Informationsgesellschaft und der Generaldirektion Wettbewerb.

#Tfn

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatcec ;
# txtlang: de ;
# multlin: t ;
# End: ;

