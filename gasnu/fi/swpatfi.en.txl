<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Software Patents in Finnland

#descr: Between 1998 and 2003 the Finnish Patent Office (FiPO/FiPRH) did not
follow the European Patent Office's (EPO) decisions to grant literal
claims to information objects such as %(q:computer program product,
characterised by ...).  In 2003 the FiPO suddenly rushed to grant such
claims, although both the European Commission and the European
Parliament had proposed not to allow them and the existing laws
clearly forbid them.  The FiPO based its decision merely on the fact
that the FiPO itself had participated in pushing the %(q:European
Council's Patent Working Party), a group of national patent
administrators, to propose that such claims be accepted.  Thanks to
Nokia, Finnland is one of the tallest software patenting dwarfs at the
EPO.  Nokia owns about 70-80% of the finnish software patents at the
EPO and is said to wield overwhelming influence on Finnland's
politics.  Nokia's patent department has been intensively lobbying for
software patentability in Helsinki, Brussels and Strasburg.

#sWW: Finnish software patents at the EPO

#xde: According to the %(fs:FFII statistics), currently finnish companies
hold %(NFI) patent applications at the EPO, and of these %(NNO) are
held by Nokia.

#ntf: Finnish Patent Office

#aco: Finnish Patent Office 2003: decision to legalise program claims

#ote: Pekka Launis and Eero Bomanson fron the Finnish Patent Office announce
that they will now, contrary to the written law and to the European
Commission's Directive Proposal and the European Parliament's
amendment proposals, allow literal claims to computer programs and
other information objects.  They say that they must do so now, because
the EPO has been doing so since 1998 and because the European Council
has endorsed a proposal by the Patent Working Group, in which the
Finnish Patent Office speaks for the Finnish government, to change the
proposed directive in such a way that program claims are legalised. 
This text is available in Finnish and Swedish.

#hWr: Finnish Patent Office legalises program claims

#ohn: rough translation of an announcement of the Finnish Patent Office by
Erik Josefsson and discussion thread.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatfi ;
# txtlang: en ;
# multlin: t ;
# End: ;

