<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: The patent lobby and lawyers in general wield great influence in the United States.  One Japanese book is titled %(q:Litigating a Country to Death -- The United States of America).  Like in Britain, the patent system ran out of control rather early in the US.  In the 80s, this was partially reinterpreted as an american national policy.  Under the influence of the patent law lobby, the US government pushed other countries to allow patentability of everything under the sun according to US standards.  Jordan signed a bilateral treaty with the US in this sense in 2000.  Japan was heavily lobbied.  US lobbying has made itself felt in Europe as well, so that many, including the French president, have spoken about a strategic need to resist the US pressure.  Whether this US pressure is really based on a national policy in the interest of the USA is to be doubted.  It looks more like a paper tiger built up by US patent lawyers afraid to otherwise lose the position which they have acquired in the US.  But without doubt the USA is in the position of the early adopter of patent inflation.  While others were still unaware of illegal inflatory activities of their patent office, the USPTO moved ahead unhindered, so that most of the questionable patents fell into the hand of US companies.  In Europe, more than 2/3 of the software patents are in US hands, and it is somewhat natural for the US to press for their legalisation.
title: The USPTO and the US Patent Lobby
LiW: Like that of other patent offices, thes USPTO website is full of self-serving propaganda, displays very little spirit of objectivity as far as the effects of patents are concerned.
Cos: CPTech: US obliges Japan and Jordan to legalise business method patents
2eW: 2001-03-02: a meeting between %(q:United States and Japanese government officials) focused on %(q:U.S. proposals to promote a more information-technology (IT) friendly regulatory environment in Japan).  According to a US-side %(q:fact sheet), %(bq:The United States urged the Japanese Government to take a number of measures in this area, including ... clarifying its laws to ensure that the personal use exception for copying is not abused in the digital environment; and protecting business method patents.)
2nW: 2000-10-24:  Memorandum of Understanding on Issues Related to the Protection of Intellectual Property Rights Under the %(ft:Agreement Between the United States and Jordan on the Establishment of a Free Trade Area). The MoU contains the following provision: %(bq:Jordan shall take all steps necessary to clarify that the exclusion from patent protection of 'mathematical methods' in Article 4(B) of Jordan's Patent Law does not include such methods as business methods or computer-related inventions.
AWn: A latecomer to the %(ec:EU Commission's consultation exercise), in which many patent lawyers wearing the hat of large US companies participated and spoke in the same sense as this document.
AWn2: Al Gore says E-Commerce needs Patents
TpW: The %(VL) news service frequently takes up the subject of software patents partly because it is concerned with asserting latin/romanic culture against the perceived dominance of anglo-american culture.
AwW: A well known american patent law office wrote the following on their web pages as of 2002/03/13
Ouo: Our practice has allowed us to build up close contacts with key officials in the European Commission's Competition Directorate-General, including the case-handlers who administer the files and the policy makers at all levels of the administration. This enables us to ensure that policy decisions in individual cases are not taken without our clients' concerns having been expressed and considered at the appropriate level.
WWt: Which is to be put in relation to %(ip:another page of theirs):
Tte: The attorneys in our Brussels office provide clients with the most up-to-date information on the policies and regulations of the European Community with respect to intellectual property matters, including standardization of practices among the EU nations.
Wtt: Well, no lie, if they're so close to EC executives...
IW1: In %(q:Industrialisation without National Patents), published in 1971, the economic historian Eric Schiff tells the story of the emergence of some of Europe's biggest corporations. They came into being in Switzerland and the Netherlands during the period (1850-1907 in Switzerland; 1869-1912 in the Netherlands) in which neither country recognised patents. Some of them appear to owe their very existence to this exemption.
Ssu: Switzerland and the Netherlands eventually adopted patent laws in response to threats from other industrialised nations. This, Schiff argues, was a political decision, not an economic one. It is, he notes, %(q:difficult to avoid the impression) that the absence of patent laws %(q:furthered, rather than hampered development).
CWi: CIA und Patentinflation
CtW: Cites the Church Commission's report on CIA activities to demonstrate that the CIA is active in covert opinion making on various issues believed to be %(q:strategic).
Ats: All French Parties/Candidates against Swpat
Cbt: Chirac is not the only French politician to worry about software patents being used as a strategic instrument against European technological independence
foo: further detailed explanations about the US Jordan FTA from James Love, project director of CPTech.  Mainly concerned with the effects on pharma patents.
AWW: An FAQ-like explanation of the Free Trade Agreement of the Americas (FTAA) policy, including its impact on patent policies.
aWl: a call for action against the US government's policy of pushing IP related policies into free trade agreements

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatus ;
# txtlang: fr ;
# End: ;

