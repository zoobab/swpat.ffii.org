<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Softwarepatente in den USA

#descr: Die Patentwelt und Rechtsanwälte insgesamt üben in den USA großen
Einfluss aus.  Ein japanisches Buch titelt gar %(q:Ein Land
prozessiert sich zu Tode -- die Vereinigten Staaten von Amerika).  
Ähnlich wie in Großbritannien brachen die Schleusen im Hinblick auf
Rechenregeln und Software in den USA bereits teilweise in den 70er und
gänzlich in den 80er Jahren.  In den 80er Jahren wurde dieser
Dammbruch dann zu einer Regierungspolitik verbrämt.  Einige Politiker
sahen in einem verstärkten Patentsystem Möglichkeiten, der
bedrohlichen asiatischen Konkurrenz Herr zu werden.  Länder wie Japan,
Jordanien, aber auch Europa, wurden von der US-Regierung so intensiv
bedrängt, dass zeitweilig der französische Präsident von einer
strategischen Notwendigkeit sprach, diesem Druck zu widerstehen.  Wie
viele US-Studien belegen, schadet die Patentinflation auch den USA. 
Ihr Export dürfte weniger Ausdruck eines nationalen Interesses als
eines natürlichen Expansionismus des Patentwesens sein, den man auch
in Europa beobachten kann.  Allerdings ist kaum zu bezweifeln, dass
US-Firmen sich bereits wesentlich gründlicher als europäische auf die
Spielregeln der Softwarepatentierung eingestellt haben.  Ca 2/3 der
vom Europäischen Patentamt (gesetzeswidrig) erteilten Softwarepatente
liegen in amerikanischen Händen.   Es ist verständlich, wenn
amerikanische Unternehmen auf eine baldige Anerkennung dieser Patente
in Europa drängen.

#cun: A Washington think tank whose leading members occupy high posts in the
GW Bush administration and which were advocating many of this
administration's policies, including the war on Iraq, already in 1998
and earlier.  The website provides interesting reading about a new
american sense of mission and and imperial agenda.  The patent
movement has been able to take advantage of this agenda for its
purposes, which are quite unrelated to the US interests as described
on this website.

#tna: Brian Kahin, a researcher on information policy and former advisor of
the Clinton administration on this subject, points that neither Europe
nor the US have a policy on where the limits of patentability should
be and that yet there are divergences, based on phase differences, at
least in legal theory, in the progressive glide toward unlimited
patentability, which is why  %(orig:the U.S. Government is urging the
rest of the world to do away with limits on patentable subject matter
in a proposed %(tp|World Intellectual Property Organization|WIPO)
treaty on substantive patent law. In May, the U.S. threatened to walk
out of negotiations if the treat does not mandate patents for all
fields of activity, whether or not they fall within common notions of
%(q:technology).)

#1fi: WiPO 2003/05/10: US threatens to walk out of WIPO if unlimited
patentability is not accepted

#qee: Report, 7th Session, WIPO Standing Committee on Patent Law, Geneva,
6-10 May 2002, adopted 25 November 2002 as document SCP/7/8; see
paragraphs 159-173, especially the exchange between the European and
U.S. delegations at 170-171:  %(bc|167. The Delegation of the United
States of America also welcomed the reintroduction of the industrial
applicability/utility requirement into the Treaty, since utility was
an important requirement for the United States of America. The
Delegation, however, was of the view that the industrial applicability
standards in certain systems might require a claimed invention to have
a technical character or technical effect. The Delegation was also
concerned that such a provision might also be used to refuse the
patenting of inventions that were considered to be private in nature.
The Delegation saw no reason to limit patentability in such a manner;
the criteria should be that the invention has utility, is novel and
involves an inventive step. The Delegation expressed further concern
that an %(q:industrial applicability) standard could stifle the
development of new areas of innovation, such as software,
biotechnology, or other newly developing areas that could not be
foreseen now and that might defy definition according to the current
understanding of what is meant by %(q:industrial).|171. The Delegation
of the United States of America stated that it could support neither a
%(q:technical) requirement in the SPLT nor the importation of the very
minimal standards of protection that were found in the TRIPS
Agreement, nor an %(q:industry) or %(q:industrial-based) standard on
the issue of industrial applicability or utility. The Delegation
expressed the view that the inclusion of a %(q:technical) or
%(q:industrial) requirement would result in the standards for
protection for inventions throughout the world to slip backwards,
eroding the level of protection for inventors and inventions
everywhere. The Delegation was of the opinion that the end result of
the discussions, if it were based in part on any of those elements,
would not be acceptable to the United States of America, and
accordingly, the Delegation might well have to reconsider its
participation in those discussions. The Delegation stated that it had
come to the negotiations in good faith in that many provisions in the
draft SPLT would require fundamental changes to the United States
patent system. However, the Delegation stated that its continued
participation was contingent on similar good faith from all members of
the Committee.)

#prt: Naders Kritik an US-Patentexpansionismus

#dot: USTR und US-Aktivitäten zu Software-Patenten in Europa

#det: USTR und US-Aktivitäten zu Software-Patenten ausserhalb Europas

#tWe: IPR Watchlist of the US Trade Representative

#Tth: Der U. S. Trade Representative (USTR) und sein Office

#oli: Fazit

#ksg: Wired 2003-03-11: Nader criticises US policy of exporting patent
system

#pns: Hier wird berichtet, dass sich der bekannte US-Verbraucherschützer
Ralph Nader neuerdings mit Software- und Geschäftsmethoden-Patenten
beschäftigt. Er kritisiert speziell, dass die USA über den %(q:US
Trade Representative) (USTR) ihr System in Entwicklungsländer
exportieren will.

#ogW: Letter to USTR Zoellick regarding WTO Patent discussions

#Wsa: This letter by Jamie Love of CPTech (Nader's organisation) criticises
the activities of the US Trade Representative which are making
medicines unaffordable in poor countries and thereby harming overall
US foreign policy objectives and lowering the standing of the US in
the international community.  The letter also mentions that, while the
US is making compulsory licensing difficult to implement for poor
countries, it has itself not been hesitant to impose compulsory
zero-fee licensing of Mexican and Canadian Software Patents, as seen
from the %(q:2001-09-26 Federal Register notice regarding United
States v. 3D Systems Corporation and DTM Corporation).

#iWi: In its annual report on its european activities, the US Trade
Representative writes:  %(bc:Most U.S. businesses also support
European Commission efforts to launch a proposal for an EU software
patent. However, internal Commission disagreement has blocked progress
on this project.)

#heW: Europa und Japan gehören aber auch zu den Zielen des USTR, das belegt
die USTR-Website.  Sie zeigt u. a., dass ein EU-Richtlinienvorschlag
laut USTR bereits %(e:vor) seiner Veröffentlichung von vielen
US-Unternehmen unterstützt wurde.

#ecW: Da wird sich der USTR gefreut haben, als im Februar 2002 der
%(er:EU-Richtlinienvorschlag) schließlich doch erschienen ist. Aber
der USTR hat sich nicht erst 2001 mit dem Thema beschäftigt. In seinen
Berichten 2000 und 1999 schreibt er:

#etl2: Under the heading %(q:Foreign Trade Barriers) the USTR states: %(bc:In
addition, the [European] Commission believes ... that EU law on
patentability of computer programs and software related inventions
must be brought into line with the United States and Japan. ... A
series of concrete measures to improve the framework for obtaining
patent protection in the EU ... include ... a proposal for a Directive
on patent protection of inventions related to computer programs.)

#dvr2: US Trade Representative Report on Europe 1999

#etl: Under %(q:Foreign Trade Barrier) the report reports encouragingly
about extensions of patentability, taking for granted that such
extensions are good for U.S. firms: %(bc:In addition, the [European]
Commission believes ... that EU law on patentability of computer
programs and software related inventions must be brought into line
with the United States and Japan. ... On June 16, 1998, after years of
debate, the European Council adopted a directive on legal protection
of biotechnological inventions. ... The directive excludes plant and
animal varieties from patentability and, although a positive
development for U.S. firms, will not provide the same level of patent
protection that is provided in the United States to biotechnological
inventions.)

#cse: Insofern ist Skepsis angebracht, wenn die EU-Kommission behauptet,
dass der EU-Richtlinienvorschlag %(q:amerikanische Verhältnisse)
verhindern solle und %(q:nur) bereits vorhandenes Recht harmonisiere.

#rcW: Es ist auf der USTR-Website gut dokumentiert, dass der USTR über all
die Jahre gut informiert war und US-Unternehmen den
EU-Richtlinien-Vorschlag bereits vor seiner Veröffentlichung
unterstützt haben. US-Unternehmen wie Microsoft begrüßen den
Richtlinienvorschlag der EU-Kommission ausdrücklich.

#rrw: In summer 2003, when it became apparent that the European Parliament
was no longer under complete control of the software patent lobby, the
US Representation at the EU forwarded a document, apparently written
by someone in the US Patent Office, to put pressure on the European
Parliament.  From this document it became apparent that the USTR
supported the Commission's approach and considered it basically
equivalent to the US rules for software and business method patents,
with differences in wording accepted as a means of overcoming
political resistance, as long as these differences remained only
rhetorical.

#Usi: Die Aktivitäten der USA in Europa stehen nicht alleine. Das Büro des
USTR kümmert sich in diesem Zusammenhang um viele große und kleine
Länder.

#Wgd: In einem %(q:Fact Sheet) zu einem %(q:Information Technology Expert
Level Meeting) zwischen den USA und Japan vom 2. März 2001 wird auf
der Website des USTR festgehalten:

#ntW: US-JP %(q:Information-Technology Expert-Level Meeting) 2001-03-02
%(q:Fact Sheet)

#nss: This memorandum of a meeting of US Trade Representatives with Japanese
Colleagues states %(bc:Intellectual Property: Robust intellectual
property protection is essential to the growth of e-commerce. The
United States urged the Japanese Government to take a number of
measures in this area, including: ... protecting business method
patents.)

#nxl: Auch kleine Länder werden von den USA softwarepatentmäßig umsorgt, so
findet sich in einem %(q:Memorandum of Understanding) zwischen den USA
und Jordanien vom 24. Oktober 2000 auf der Website des USTR:

#nge: US-Jordan Memorandum of Understanding 2000-10-24 obliges Jordan to
make software and business methods patentable

#rdb: The Memorandum stipulates %(bc:5. Jordan shall take all steps
necessary to clarify that the exclusion from patent protection of
%(q:mathematical methods) in Article 4B of Jordan's Patent Law does
not include such %(q:methods) as business methods or computer-related
inventions.)

#Tre: USTR - Press Release

#hnr: Statement of the US Trade Representative accompanying the publication
of the %(q:Priority Watch List).

#eWo: USTR - Intellectual Property - 2003 Special 301 Report - Priority
Watch List

#nmn: List of countries which are causing special grief to the right-holders
in the USA and require special monitoring and pressure by the US Trade
Repressentative.  This list contains the EU.  The EU has a conflict
with the US on the regime for geographical indications (such as
%(q:Budweiser), %(q:Champagne)) and its relation to trademarks. 
Moreover, according to the USTR, some EU member states are failing to
implement the EU biotech patent directive, i.e. providing insufficient
%(q:protection) for gene patents.  It is to be expected that the USTR
will, in response to complaints from patent lawyer organisations such
as AIPLA, list the European Parliament's vote against software
patentability of 2003-09-24 on next year's list of griefs for which
the EU deserves to be %(q:watched) with priority.

#4lW: USTR 2004-01: South Korea elevated to Priority Watch List

#Wtf: The USTR reports failures of South Korean authorities to comply with
their commitments to enforce digital copyright.

#0sR: IIPA 2003/05: Copyright Industries Support the US Trade Representative

#nWW: An association of 5 associations of information producing companies
welcome the USTR efforts to monitor enforcement of copyright in
foreign countries and request the addition of further countries to the
%(q:Priority Watch List).  Associations such as IIPA seem to be major
sources for the Watch Lists drawn up by the USTR.

#cWn: website of the US Trade Representative

#fWe: Role of the US Trade Representative (USTR)

#TPc: USTR IP Sector

#row: The US Trade Representative has special staff to work on
%(q:Intellectual Property) issues, which includes patents.

#cWe: USTR 1997: Building American Prosperity in the 21st Century - U.S.
Trade and Investment in the Asia Pacific Region

#eiW: Under the heading %(q:Impact of the Information Age ...) the following
policy is formulated: %(bc|The United States must adopt policies and
practices based on the following imperatives:|...|5. As vigorously as
the United States would protect its most precious natural resources,
America must have strong, reliable intellectual property protection
and make this issue a high priority in all international trade fora.)

#rWn: Der U.S. Trade Representative USTR ist Berater, Unterhändler und
Sprecher des US-Präsidenten in Handelsfragen:

#sSg: Ein spezielles Arbeitsgebiet (Sector) des USTR ist %(q:geistiges
Eigentum) und dort Patente:

#WWa: In einem Bericht von 1997 auf der Website des USTR wird als Leitlinie
der Politik der US-Regierung formuliert, dass der %(q:Schutz des
Geistigen Eigentums) zu einem Anliegen höchster Priorität in allen
internationalen Foren gemacht werden soll:

#rsW: Man überinterpretiert die US-Politik wohl kaum, wenn man festhält,
dass beim USTR Software-Patente und Geschäftsmethoden-Patente nach
US-amerikanischem Recht mit Bodenschätzen gleichgesetzt werden und
dass die weltweite Verbreitung des entsprechenden US-Rechtssystems ein
vornehmes Ziel der US-Handelspolitik ist.

#rOW: Wichtiges USPTO Paper von 94/95

#AWn: A latecomer to the %(ec:EU Commission's consultation exercise), in
which many patent lawyers wearing the hat of large US companies
participated and spoke in the same sense as this document.

#rmW: Vice President Al Gore says E-Commerce needs Patents

#rvd: FICPI, world association of patent lawyers, calls for removal of all
means of restricting patentability by interpreting words such as
%(q:technical), %(q:industrial), %(q:inventions) from TRIPs and
upcoming treaties such as SPLT.  FICPI is allied with the US
government on this point.  One could also say that the US government
has been hijacked by patent lawyers.

#ACf: The American Chamber of Commerce participated in the European
Commission's consultation by submitting this paper which supports the
EU directive proposal and hails it for bringing Europe closer to the
US model which it finds just fine.

#LiW: Like that of other patent offices, thes USPTO website is full of
self-serving propaganda, displays very little spirit of objectivity as
far as the effects of patents are concerned.

#Cos: CPTech: US obliges Japan and Jordan to legalise business method
patents

#2eW: 2001-03-02: a meeting between %(q:United States and Japanese
government officials) focused on %(q:U.S. proposals to promote a more
information-technology (IT) friendly regulatory environment in Japan).
 According to a US-side %(q:fact sheet), %(bc:The United States urged
the Japanese Government to take a number of measures in this area,
including ... clarifying its laws to ensure that the personal use
exception for copying is not abused in the digital environment; and
protecting business method patents.)

#2nW: 2000-10-24:  Memorandum of Understanding on Issues Related to the
Protection of Intellectual Property Rights Under the %(ft:Agreement
Between the United States and Jordan on the Establishment of a Free
Trade Area). The MoU contains the following provision: %(bc:Jordan
shall take all steps necessary to clarify that the exclusion from
patent protection of 'mathematical methods' in Article 4B of Jordan's
Patent Law does not include such methods as business methods or
computer-related inventions.)

#psi: The explanatory memorandum of the directive proposal cites BSA studies
which are unrelated to software patents but also appear in the
reasoning of software patent advocacy papers transmitted by the US
Representation in Brussels.  The directive draft itself was at least
partially written by an employee of BSA, a US-based organisation
dominated by Microsoft.

#TpW: The %(VL) news service frequently takes up the subject of software
patents partly because it is concerned with asserting latin/romanic
culture against the perceived dominance of anglo-american culture.

#AwW: A well known american (patent) law office had the following statement
on their web pages as of 2002/03/13:

#Ouo: Our practice has allowed us to build up close contacts with key
officials in the European Commission's Competition
Directorate-General, including the case-handlers who administer the
files and the policy makers at all levels of the administration. This
enables us to ensure that policy decisions in individual cases are not
taken without our clients' concerns having been expressed and
considered at the appropriate level.

#WWt: Which is to be put in relation to %(ip:another page of theirs):

#Tte: The attorneys in our Brussels office provide clients with the most
up-to-date information on the policies and regulations of the European
Community with respect to intellectual property matters, including
standardization of practices among the EU nations.

#Wtt: Well, no lie, if they're so close to EC executives...

#ego: These pages were later removed or moved to areas which are accessible
only to clients.  But you can find a lot of other pages on the current
site which show that Oppenheimer's lawyers are still in regular close
contact with the political scene in Brussels and elsewhere in Europe.

#grn: It is so far unclear to us whether Oppenheimer LLP, %(OF), A.M.
Oppenheimer and others share more than the name.  Some of these are
highly active in telecommunications and i.a. largest foreign
shareholder of Ericsson, owning 215 million B-shares.  Oppenheimer is
a famous jewish business dynasty originating in Germany.  Robert S.
Oppenheimer was a legendary jewish businessman and politician who,
after a change in political power, fell victim to judicial murder in
Wuerttemberg 300 years ago.

#CWi: CIA und Patentinflation

#CtW: Cites the Church Commission's report on CIA activities to demonstrate
that the CIA is active in covert opinion making on various issues
believed to be %(q:strategic).

#Ats: All French Parties/Candidates against Software Patents

#Cbt: Chirac is not the only French politician to worry about software
patents being used as a strategic instrument against European
technological independence

#foo: further detailed explanations about the US Jordan FTA from James Love,
project director of CPTech.  Mainly concerned with the effects on
pharma patents.

#AWW: An FAQ-like explanation of the Free Trade Agreement of the Americas
(FTAA) policy, including its impact on patent policies.

#aWl: a call for action against the US government's policy of pushing IP
related policies into free trade agreements

#Wae: At WIPO the US government, represented by its patent office, is
obstructing all discussions about the efficiency of the patent system
in promoting innovation.  In 2003/08 the US representative asked that
a WIPO movement on implications of opensource software for the IPR
system should be cancelled, because %(q:it is WIPO's job to uphold and
extend property rights, not to restrict or waive them).  See also the
ensuing discussion.

#mrh: The Intellectual Property Committee's primary objective is to focus on
intellectual property matters as they relate to the IEEE-USA
membership (employed engineers, faculty, scientists, inventors,
entrepreneurs, etc.), fast moving technology, technology transfer, and
U.S. competitiveness.

#neS: IPC prepares testimony and position statements, drafts legislation,
and delivers expert testimony before the U.S. Congress and the United
States Patent and Trademark Office.

#let: At OECD in Paris 2003/08/28, the US government, representat by patent
officials, asked the scientists and politicians assembled at the
conference to refrain from discussing %(q:so-called problems of the
patent system).  The US representative said: patents are basically
good, the system has worked for 200 years, and any attempt to discuss
its so-called problems will only be taken as a pretext by some
third-world-ideologues to stir up artificial problems.  In contrast,
the EPO representatives at the conference seemed very intelligent and
enlightened, although (or because) they were not speaking in the name
of any government.

#dse: KONNO Hiroshi, an authority on industry-applied mathematics in Japan,
reports about his decade-long fight against the introduction of math
patents in Japan and about the role of the US in generating political
pressure for this change.

#ifh: One of the leading software patent evangelists at the European
Commission cites the US example and misrepresents the decisions that
led to software patentability in the US.  This review gives an
overview of the development in the US and illustrates how this has
been (ab)used for patent advocacy in Brussels.

#nyw: major US opposition group against software patents

#ena: Research on Innovation

#ler: A group of social scientists who have been studying the evolution of
the patent system and writing critcical papers

#chW: Subscribe to the %(c:us) project

#dow: Provide addresses of people to write to

#hxh: Edit the %(we:Wiki Extension) of this page.

#oWW: Write to your political representatives in the US

#oWf: Ask them to question the IPR evangelism activities of the US Trade
Representative and the US Patent Office and in particular to

#mna: stop promotion of software and business method patentability

#otT: stop promotion of extensive interpretations of the TRIPs treaty

#tco: stop promotion of US patentability criteria through the proposed SPLT
treaty

#hoe: publish a statement which makes it clear that the US government has no
objections against the European Parliament's amended software patent
directive.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatus ;
# txtlang: de ;
# multlin: t ;
# End: ;

