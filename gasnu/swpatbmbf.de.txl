<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Wdz: Wir brauchen einen Forschungsminister (egal welchen Geschlechts), der seine Ministerialbeamten kritisieren und zur Verantwortung zu ziehen in der Lage ist.  Das ist die Hauptaufgabe jedes Politikers.

#JWh: Jemand muss Frau Bulmahn aufklären.

#Pre: Primärindikator für den Erfolg geförderter Projekte sollen die dabei erzeugten öffentlichen Informationsgüter (einschließlich Ideen und Werke) sein.  Im Bereich der Informatik/Software ist dieser Idikator sogar der alleine ausschlaggebende.  Messen lässt sich der Erfolg z.B. an Zitier- und Gebrauchshäufigkeit und der Zahl Personen im Inland, die darauf Fachwissen aufbauen und verwerten.  Ferner ist wichtig, ob ein Marktversagen/Gefangenendilemma vorlag, welches durch öffentliche Finanzierung behoben wurde.

#SWt: Schützenhilfe für den Rechtsbruch der Patentämter in bezug auf Software hat zu unterbleiben.  Die Siemens-Broschüre ist vom Netz zu nehmen und durch eine klare Grundsatzerklärung zu ersetzen.  Softwarepatente dürfen nicht im Rahmen von BMBF-geförderten Projekten angemeldet werden.

#FaF: Forderungen an das BMBF und die Forschungspolitik

#Bni: Bislang stellen wir nur fest, dass die Argumente der Hochschulpatentbewegung nicht überzeugen und dass die Politiker wohl ohne richtige Überlegungen anzustellen mit dem Strom geschwommen sind, der von Amerika kam.  Andererseits ist klar, dass die real existierende Patentinflation auch die Hochschulen vor gewisse Herausforderungen stellt, auf die eine organisatorische Antwort in der einen oder anderen Form zu finden ist.  Es käme darauf an, hier alternative Konzepte zu entwickeln und vergleichende Kostenrechnungen auf allen Ebenen anzustellen.  Verweise auf vorhandene Überlegungen wären auch interessant.

#Woi: Wirkung auf die Hochschulen, auf die Volkswirtschaft?

#EoW: Einige Infos über Behinderung freier Software durch das Hochschulpatentwesen gemäß Bayh-Doleschem Gesetz in den USA.

#adu: american professor of computer science arguing that the Bayh-Dole Act did at the time seem to address some problems, such as lack of practicality and proof of concept in university research, but it created many more, and there is no longer any valid justification for maintaining this law.

#EWq: Ein erhebliches Innovationspotential liege an den deutschen Hochschulen Brach, erklärt Bulmahn.  Durch Anschubsfinanzierung solle die systematische Erschließung der wirtschaftlich verwertbaren Forschungsergebnisse sichergestellt werden.

#Bct: Bulmahn: Änderung des Hochschullehrerprivilegs soll jetzt zügig angegenangen werden

#Des: Der Verband der Professoren hält nichts von der Verwertungsoffensive, befürchtet Bürokratisierung und Demotivierung der Hochschullehrer.  Der Verband erwähnt ein Gutachten, welches eine Patentanwaltskanzeli im Auftrag des BMBF anfertigte.  Dieses Gutachten spricht sich gegen einen Verwertungszwang und für die Beibehaltung des %(q:Hochschullehrerprivilegs) aus und nennt dafür gute Gründe.

#Sec: Deutscher Hochschulverband 2002-06-30: Verwertungszwang verschlechtert Forschungsklima

#Ise: BMBF patent server

#Eel: Evan Brown, an IT service person, mentioned to his employer that he had recently thought of new ways to convert old programs to new ones.  The employer asked him to disclose the ideas so the company could patent them.  Brown refused and was fired.  The company was later acquired by Alcatel.  Alcatel is continuing to litigate against Brown for refusing to hand over his idea.  Brown has lost in court and is faced with lawyer bills of several 100k USD.  On this site he is asking the public for help.  Since in Europe, too, politicians have recently been eager to turn people's ideas into %(q:property) of their employers, this site should give people something to think about.

#aao: apparently written by people at a universitarian patent administration agency of the University of Columbia, reports very positively about the effects, fairly informative

#deW: dänisches Gegenstück des Bayh-Doleschen Gesetzes und der deutschen Hochschulpatentgesetze von 2002/2

#Mhu: Matthias Berninger, MdB Grüne und Staatssekretär im Landwirtschaftsministerium, stimmt in den Chor %(q:gegen das Hochschullehrerprivileg) und %(q:für den Aufbau einer Patentkultur an den Hochschulen) mit ein.   Der Patentbewegungs-Kampfbegriff %(q:Hochschullehrerprivileg) scheint auch grüne Abgeordnete anzusprechen.  Berninger meint, in der %(q:Wissensgesellschaft) würde wegen %(q:immer kürzerer Innovationszyklen) %(q:geistiges Eigentum) und insbesondere Patente immer wichtiger, und daher müsse die Patentvermarktung zentral von der Hochschule vorangetrieben werden.  Hiervon verspricht sich Berninger %(q:weniger Geheimforschung), %(q:besseren Technologietransfer), %(q:Etablierung einer Patentkultur) und vieles mehr, was die Patentbewegung an Versprechungen bereithält

#Mrc: Matthias Berninger 2000: Bedeutung der Patentvermarktung in der Wissensgesellschaft

#Dii: Der für Forschung zuständige CDU-Abgeordnete Norbert Hauser bläst auch in Bulmahns Horn.  Das %(q:Hochschullehrerprivileg) sei ein %(q:Überbleibsel aus der Kaiserzeit).  Der Aufbau einer zeitgemäßen Hochschul-Patentverwertung nach amerikanischem Vorbild erfordere aber mehr als die von Bulmahn vorgesehenen 100 Millionen DEM Anschubsfinanzierung.

#Hfi: Hauser 2001-11-30: Abschaffung des Hochschullehrerprivilegs richtig, Weg falsch

#Kea: Kritischer Bericht

#NiW: Neue Patentinitiativen von Bulmahn

#Inn: Im Namen des BMBF plädiert die Patentabteilung von Siemens für die Patentierbarkeit von Software und regt Hochschulen an, Softwarepatente anzumelden.

#Bet: BMBF bietet Softwarepatent-Broschüre der Patentabteilung von Siemens an

#Eur: Einführung einer Neuheitsschonfrist für Patentanmeldungen

#Ahs: Removal of traditional obligations to fair use of patents by universities

#Ewa: Eine Patent- und Verwertungsstelle der Max-Planck-Gesellschaft

#Fnu: Fraunhofer-Patentstelle der Deutschen Forschung

#cWg: %(q:Patent Offensive) alias %(q:Valorisation Offensive) in the German Ministery for Education and Research

#Zii: Mit der Hochschulpatentbewegung hängen insbesondere folgende Institutionen und Initiativen zudammen:

#descr: Hunderte von universitären %(q:Technologietransfer-Stellen) beschäftigen sich hauptamtlich damit, die Verbreitung von Technologien mithilfe von Patenten zu bremsen.  Der Steuerzahler finanziert die Erschließung und sofortige Monopolisierung neuer (und alter) Wissensgüter.  Das BMBF bemisst den Erfolg seiner Investitionen nicht an den erzeugten öffentlichen Gütern sondern umgekehrt an den der Öffentlichkeit entzogenen Gütern.  Für die Patentgebühren zahlt der Steuerzahler später ggf noch einmal 10 mal so viel drauf, wie die Universität (bei effizienter Führung vielleicht irgendwann einmal) daran verdient.  In den so geschaffenen Patentfabriken bestimmt eine staatlich geförderte Verwertungsbürokratie die Richtung der Forschungspolitik.  Den guten volkswirtschaftlichen Gründen für die Existenz eines öffentlichen Bildungs- und Forschungssektors wird offenbar kaum noch Rechnung getragen.  Stattdessen entsteht eine zweitklassige Konkurrenz zur industriellen Forschung.  Irgendwann in der Zukunft wird hierdurch vielleicht einmal die Staatskasse entlastet.  Das versprechen zumindest die %(q:Technologietransferbeamten), die in dem neuen System die Posten bekleiden.  Dieser Gruppe stehen in diversen Regierungen auf regionaler, nationaler und supranationaler Ebene steuerlich finanzierte Agenturen zur Seite, von denen insbesonere die %(q:Patentoffensive) im Bundesministerium für Bildung und Forschung von sich reden macht.

#title: Die Hochschul-Patentbewegung

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbmbf ;
# txtlang: de ;
# multlin: t ;
# End: ;

