<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Frits Bolkestein and Software Patents

#descr: Internal Market Commissioner of the European Commission since 2000, leader of dutch right-wing liberal party VVD, known mainly by interventions in favor of the world's second largest pharmaceutical company, of whose supervisory board he is a member, and by various policies in favor of big business.  Ever since he took office, Bolkestein firmly committed himself to the agenda for legalisation of software patents in Europe and pushed this agenda through an unwilling European Commission.  Bolkestein saw himself forced to pay lipservice to the goals of his opponents and sell his directive as a means to achieve those goals.  When it became apparent that this strategy did not work, Bolkestein threatened the Parliament with removal of its competence of decision.  The Parliament was neither deceived nor intimidated.  A month later, Bolkestein's directorate lobbied the national ministers to ignore the Parliament's will and drive the directive project against the wall.  When asked for justification, officials of the directorate often point to %(q:the Commissioner) who allegedly is insisting on this.

#Wms: Bolkestein's page on the European Commission's web server, with contact coordinates, links speeches, press interviews and curriculum vitae.

#BolkIschT: Quo Vadis? (Where do we go from here?) Striking the balance on industrial property rights

#BolkIschD: Bolkestein acknowledges failure of communication strategy

#sWo: Bolkestein speech at Ischia Conference

#wmf: Acknowledges communication failures

#Wft: Bemoaning the Ineffectiveness of Fraudulent Communication Strategies

#gWa: Promoting TRIPs Extremism in favor of Microsoft and Pharma Patent Lobby

#soW: Politics at the Service of Friends from Big Business

#BolkD: Internal Market Commissioner of the European Commission since 2000, leader of dutch right-wing liberal party VVD, known mainly by interventions in favor of the world's second largest pharmaceutical company, of whose supervisory board he is a member, and by various policies in favor of big business.  Ever since he took office, Bolkestein firmly committed himself to the agenda for legalisation of software patents in Europe.  A hardline pro-patent speech delivered by Bolkestein to a patent lawyer audience in Spain in June 2000 sparked off the largest petition on IT-related subject matter in history.  From March to October 2000, Bolkestein's directorate kept a study secret which had warned of negative effects of software patents.  In 2001, Bolkestein's directorate prevented follow-up to a consultation which had shown overwhelming public opinion against software patents.  In February 2002, after a year of silence and almost no dialogue within the European Commission, Bolkestein's directorate published a draft directive %(q:on the patentability of computer-implemented inventions), drafted in close collaboration with EPO and BSA, which was grudgingly accepted by other directorates due to pressure from Bolkestein and the UK Patent Office.  This draft was condemned by many leading scientists and amended into its opposite by the European Parliament in September 2003.  During this time, Bolkestein pretended that his directive was designed to prevent patents on %(q:pure software) and business methods and was the only viable way of achieving this aim.  Shortly before the Parliament's vote, Bolkestein threatened that the Commission would withdraw the directive if the Parliament voted as it later did.  Later Bokestein moaned in front of his patent lawyer audiences that %(q:we) (including the audience) had done the real work of careful and balanced analysis but had failed to convey %(q:our) message against a tide of ignorance and misinformation.  In these same patent lawyer rallies where Bolkestein feels at home, the %(q:misinformers) (also called %(q:detractors) or %(q:open source and anti-globalisation movement) by Bolkestein) are always carefully excluded from participation.  Many panels are about economics, but the economic expertise always comes from patent lawyers, never from economists.

#gWo: On 2003-10-15 a congregation of patent lawyers was invited by Bolkestein's directorate to discuss about economic policy in Ischia near Naples.  Bolkestein, as the host of the conference, said in his inaugural speech:

#etW: As far as communication is concerned, my recent experiences in the European Parliament, where I debated our proposal on the protection of computer implemented inventions, made it clear to me that our message is not coming through to everyone. I am afraid the impression is widespread that industrial property rights are there to serve the interests of industry, at the expense of citizens and consumers.

#ear: To a large extent we have ourselves to blame for this unfortunate state of affairs. Too many policy discussions in this field are conducted in the jargon of specialists and lawyers. These technical discussions have led to sound plans and proposals. But we have been less successful in making clear to the non-specialists what industrial property rights are really about. And we have had even greater difficulty in explaining that industrial property rights also benefit citizens and consumers.

#daa: For example, in the past weeks, I literally spent hours and hours trying to clear up one simple misunderstanding: our proposal on the protection of computer implemented inventions is not at all about software. I spent evenings explaining to MEPs and journalists that this proposal will not seal off the software market to initiatives and new inventions, but that it merely wants to reward those who invest in developing genuinely new products that depend on computer implemented technology. I spent days trying to persuade people, that our approach to industrial property rights in general always aims to strike a balance, between encouraging innovation via property rights on the one hand, and respecting that the prerequisite of competition on the other.

#eWf: The key issue in the European Parliament and in the press was the %(q:interoperability) of computer software. Many MEPs and even more lobbyists were very concerned that the protection we allegedly granted to software would prevent computer systems from %(q:interoperating), %(q:speaking to) or indeed %(q:competing) with each other. But I am just as convinced as the European Parliament that the proposal on the protection of computer implemented inventions should respect the principle of interoperability. There should and there will be market access for all software developers. Consumers should have a choice. I am fully committed to making sure that they will not get a raw deal.

#aje: But it continues to be a difficult debate. Many people still need persuading. And that means we have to explain more clearly what our principle is, namely that we all stand to gain from well-designed legislation in this area. And what is more: we need to do this in %(q:plain speak). The acronyms don't help.

#ete: To conclude, this conference is named 'quo vadis?', which means: %(q:Where do we go from here?) Let me give you an answer. We need to persuade our critics, such as the open source and anti-globalisation movements, that our ideas are right. We need to persuade them that what we want is in the end not that different from what they want. Industrial property rights are not about corporate interest. They are about an open and competitive economy that also provides incentives for research and innovation. They are about an environment in which citizens, all over the world, can benefit from new techniques and inventions, against a reasonable price.  That is our basic philosophy! And I can think of no reason why we should be so modest about it.

#eWn: Pro software patent rally organised by Bolkestein's directorate, conducted under strict exclusion of economists and software patent critics, inaugurated by a speech of Mr. Bolkestein, in which he bemoans his community's communication failures.  Among the participants is also a patent department head from Merck, Sharp & Dohme.

#olt: This consultation, conducted by Bolkestein's directorate in 2000, led to a high quantity and quality of responses that asked for clear exclusion of software from patentability.  Bolkestein's directorate discarded the responses as coming from a vocal %(q:economic minority) and put the project on hold for nearly a year, avoiding all communication on the matter.

#WWg: Bolkestein pretending that %(q:computer-implemented inventions) are not general purpose computing instructions but washing machines and mobile phones.

#WWW: Exchange of Articles in Volkskrant, where Bolkestein attacked a liberal party colleague who had proposed far-reaching amendments to his software patent directive.

#pWV: Further paper article copies are available from Vrijschrift.

#ehi: Bolkestein threatens withdrawal of directive proposal

#tnt: In his speech 1 day before the EP vote against sofwate patentablity on 2003/09/24, Commissioner Frits Bolkestein threatened to withdraw the directive in case the EP votes far-reaching amendments.  In this case the EP would no longer have a say in patent matters.  Therefore it should watch out well how far it will go.  The speech apparently did not impress the MEPs.

#isb: In March 2004 Bolkestein intervened in the antitrust procedings of the Commission's competition directorate against Microsoft, pushing for extreme interpretations of TRIPs according to which Microsoft must be allowed to charge fees for patented standards.

#fnr: Bolkestein is known for his affiliations with big pharma, one of the main force behind the Commission drive for extreme interpretations of Art 30 TRIPs, as can be seen from a WTO complaint filed by the Commission against Canada.  This complaint was filed before Bolkestein became Internal Market Commissioner.  However Bolkestein's later moves in favor of Microsoft may be related to these pharma patent interests as much as to Microsoft itself.

#WWo: In 1996 Frits Bolkestein, member of dutch parliament and of the supervisory board of the dutch branch of %(MSD), wrote a letter to health minister %(EB), %(q:Beste Els) (dear Els), asking her to promote admission of a specific MSD drug.  That caused a stir the time.

#eso: As an Internal Market commissioner, Bolkestein pushed for %(q:harmonisation) of food supplement regulations in Europe which were heavily criticised as favoring big pharma corporations and disadvantaging natural health foods.

#ari: Another Bolkestein project aimed at removing regulatory safeguards from stock markets.  Here, as in the software patent directive project, Arlene McCarthy was one of Bolkestein's main allies in the Parliament, but ultimately failed.

#tWL: Bolkestein's statement of interests.  Lists MSD.

#WWD: Dutch news article about the MSD scandal

#WeS: German account of the Bolkestein-MSD story

#hWa: McCarthy's account of the Parliamentary debate

#ass: A very critical account of Bolkestein's health policies in favor of big pharma under the guise of %(q:harmonisation)

#ass2: A very critical account of Bolkestein's health policies in favor of big pharma under the guise of %(q:harmonisation)

#sty: List of speakers of the Patent Movement Rally of 2003-10

#eWt: Bolkestein biography on Dutch Parliament web page

#asW: The responsible officials from DG Internal Market now often point to Commissioner Bolkestein when asked why they are defending such unreasonable positions.

#duu: Around 2000 Bolkestein has also pushed finance liberalisation legislation which was rejected by the European Parliament because it would have excessively loosened accountability of banks.  This was at the time supported aggressively by %(AM).  To be researched.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbolkestein ;
# txtlang: en ;
# multlin: t ;
# End: ;

