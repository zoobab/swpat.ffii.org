\begin{subdocument}{swpataippi}{Association Internationale Pour la Protection de la Propriété Industrielle (AIPPI)}{http://swpat.ffii.org/gasnu/aippi/index.en.html}{Workgroup\\\url{swpatag@ffii.org}\\english version 2004/12/12 by Alexander STOHR\footnote{\url{http://private.freepage.de/alexs}}}{AIPPI defines itself as an ``association of patent lawyers, patent owners and other users of the institutions of industrial property''.  Indeed most actors of the patent world are members of AIPPI.  In Vienna in 1997 AIPPI passed a resolution demanding the legalisation of software patents in Europe.  In 2001 in Melbourne there was an AIPPI resolution proposal for worldwide patentability of business methods (unclear whether accepted or not).  In 2002 an AIPPI paper submitted to WIPO demands worldwide patentability of software and business methods.}
\begin{sect}{intro}{introduction}
In 2002 Bernard Lang submitted questions to AIPPI, asking them about the factual basis for their repeated assertions that software patents are needed to promote innovation.  In a written response, AIPPI evaded and indirectly admitted that they have no basis for these claims, e.g. no economic studies that support them.

Similarly, AIPPI has made false claims about the European Parliament's amendments to the software patent directive.

AIPPI resolutions are regularly composed of false and half-true statements.  For AIPPI, the patent system is good by default, problems needn't be addressed, and truth is whatever serves the monetary interests of AIPPI's constituency.

Nevertheless, many scholars of patent law still are proud of being involved in AIPPI.
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf AIPPI 2003 Resolution Q132 zur Europarl-Abstimmung\footnote{\url{http://www.aippi.org/reports/resolutions/Q132_E.pdf}}}}

\begin{quote}
A short text which tells readers very little about what the Parliament actually voted for but claims that the TRIPs treaty was violated, without substantiating this claim.

see also Europarl 2003-09-24: Amended Software Patent Directive\footnote{\url{http://localhost/swpat/papri/europarl0309/index.en.html}}, Reactions to the EU Parliament's Vote of 2003/09/24\footnote{\url{}} and L’Accord sur les ADPIC et les brevets logiciels\footnote{\url{http://localhost/swpat/stidi/trips/index.en.html}}
\end{quote}
\filbreak

\item
{\bf {\bf AIPPI 2001 Swedish delegation: Business Methods should be Patentable\footnote{\url{http://www.aippi.org/reports/q158/gr-q158-Sweden-e.htm}}}}

\begin{quote}
At the Melbourne congress of AIPPI, various national delegations answer questions concerning business methods.  The Swedish delegation responds that business methods should be directly patentable like any other methods.

see also Software Patents in Sweden\footnote{\url{http://localhost/swpat/gasnu/se/index.en.html}}
\end{quote}
\filbreak

\item
{\bf {\bf AIPPI-Resolutionsantrag f\"{u}r uneingeschr\"{a}nkte Patentierbarkeit von Gesch\"{a}ftsverfahren\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/Apr/0099.html}}}}
\filbreak

\item
{\bf {\bf German delegates 2001: time is not yet ripe for officially advocating patentability of all business methods\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/May/0004.html}}}}
\filbreak

\item
{\bf {\bf German delegates 2001: Computing not yet digested in Europe, talk about business methods should wait\footnote{\url{http://lists.ffii.org/archive/mails/swpat/2001/May/0008.html}}}}
\filbreak

\item
{\bf {\bf AIPPI report on the Diplomatic Conference of 2000/11\footnote{\url{http://www.aippi.org/reports/report-EPO-Dipl.Conf.htm}}}}

\begin{quote}
Removal of the computer program exclusion from Art 52 EPC ``postoponed due to massive protests of some software developpers''.  AIPPI protested against the postponement.   The report gives some interesting details about the [non-]discussion of this issue at the Conference.
\end{quote}
\filbreak

\item
{\bf {\bf AIPPI Resolutions\footnote{\url{http://www.aippi.org/reports/resolutions/resolutionsindex.htm}}}}

\begin{quote}
includes such goodies as

\begin{itemize}
\item
{\bf {\bf AIPPI Melbourne 2001-03: Patentability of Business Methods\footnote{\url{http://www.aippi.org/reports/resolutions/res-q158-nil-Congress-2001.htm}}}}

\begin{quote}
demands that any idea should be patentable and that business method patents should be examined and enforced like any other kinds of patents.
\end{quote}
\filbreak

\item
{\bf {\bf AIPPI Melbourne 2001-03: Relationship between Patents and Standards\footnote{\url{http://www.aippi.org/reports/resolutions/res-q157-nil-Congress-2001.htm}}}}

\begin{quote}
says there is a conflict between standardisation and patenting and gives standardisation organisation advice on how to deal with this conflict, implying that patent law should be left untouched.  If only the price tag on ``RAND'' (reasonable and non-discriminatory) standard use fees are high enough, it will usually be possible to bring together a consortium of patentees.  Creating proprietary de facto standards by means of patents is a common way of doing business and in the rare event that it becomes too abusive, competition law can be used.  The patent system for sure is not to blame for any problems.
\end{quote}
\filbreak
\end{itemize}
\end{quote}
\filbreak

\item
{\bf {\bf AIPPI position concerning WIPO patent agenda 2002\footnote{\url{http://www.aippi.org/reports/report-wipo-patent-agenda.pdf}}}}

\begin{quote}
12 pages of usual patent propaganda, demands unlimited patentability for software and business methods worldwide.  Claims, without trying to offer any evidence, that patents are good for innovation and growth in ``all fields of technology''.  We asked at the AIPPI office about evidence for claims as to the beneficiality of software patents and received only the the usual dogmas, as they are found in some below-average patent lawschool textbooks, as a response.
\end{quote}
\filbreak

\item
{\bf {\bf Patent Lobbyism and Scripture Erudition in the name of Max Planck\footnote{\url{http://localhost/swpat/gasnu/mpi/index.de.html}}}}

\begin{quote}
Prof Straus and his subordinates / colleagues seem to be particularly active and influential in AIPPI
\end{quote}
\filbreak

\item
{\bf {\bf AIPPI-Tagung Melbourne 1974/02/24-03/02: Bericht der Deutschen Landesgruppe: Schutz der Computerprogramme (57B): Bericht erstattet von Gert Kolle\footnote{\url{http://localhost/swpat/papri/grur-kolle74/index.de.html}}}}

\begin{quote}
Back then, AIPPI conferences were more colorful.  There was serious thinking about sui generis law for software, based on copyright.  An AIPPI proposal to this effect was made in the late 70s.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

