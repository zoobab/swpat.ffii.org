<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Association Internationale Pour la Protection de la Propriété
Industrielle (AIPPI)

#descr: AIPPI defines itself as an %(q:association of patent lawyers, patent
owners and other users of the institutions of industrial property). 
Indeed most actors of the patent world are members of AIPPI.  In
Vienna in 1997 AIPPI passed a resolution demanding the legalisation of
software patents in Europe.  In 2001 in Melbourne there was an AIPPI
resolution proposal for worldwide patentability of business methods
(unclear whether accepted or not).  In 2002 an AIPPI paper submitted
to WIPO demands worldwide patentability of software and business
methods.

#Ahf: AIPPI report on the Diplomatic Conference of 2000/11

#Pdi: Removal of the computer program exclusion from Art 52 EPC
%(q:postoponed due to massive protests of some software developpers). 
AIPPI protested against the postponement.   The report gives some
interesting details about the [non-]discussion of this issue at the
Conference.

#2np: AIPPI 2003 resolution Q132 on Europarl Vote

#Woi: A short text which tells readers very little about what the Parliament
actually voted for but claims that the TRIPs treaty was violated,
without substantiating this claim.

#1Bd: AIPPI 2001 Swedish delegation: Business Methods should be Patentable

#Pti: At the Melbourne congress of AIPPI, various national delegations
answer questions concerning business methods.  The Swedish delegation
responds that business methods should be directly patentable like any
other methods.

#IoW: In 2002 Bernard Lang submitted questions to AIPPI, asking them about
the factual basis for their repeated assertions that software patents
are needed to promote innovation.  In a written response, AIPPI evaded
and indirectly admitted that they have no basis for these claims, e.g.
no economic studies that support them.

#Wet: Similarly, AIPPI has made false claims about the European Parliament's
amendments to the software patent directive.

#ooh: AIPPI resolutions are regularly composed of false and half-true
statements.  For AIPPI, the patent system is good by default, problems
needn't be addressed, and truth is whatever serves the monetary
interests of AIPPI's constituency.

#jwn: Nevertheless, many scholars of patent law still are proud of being
involved in AIPPI.

#Aur: AIPPI-Resolutionsantrag für uneingeschränkte Patentierbarkeit von
Geschäftsverfahren

#Ahv: German delegates 2001: time is not yet ripe for officially advocating
patentability of all business methods

#DiW: German delegates 2001: Computing not yet digested in Europe, talk
about business methods should wait

#iWo: includes such goodies as

#Pev: Patentability of Business Methods

#vue: demands that any idea should be patentable and that business method
patents should be examined and enforced like any other kinds of
patents.

#Bsn: Relationship between Patents and Standards

#wjW: says there is a conflict between standardisation and patenting and
gives standardisation organisation advice on how to deal with this
conflict, implying that patent law should be left untouched.  If only
the price tag on %(q:RAND) (reasonable and non-discriminatory)
standard use fees are high enough, it will usually be possible to
bring together a consortium of patentees.  Creating proprietary de
facto standards by means of patents is a common way of doing business
and in the rare event that it becomes too abusive, competition law can
be used.  The patent system for sure is not to blame for any problems.

#Aoa: AIPPI position concerning WIPO patent agenda 2002

#1do: 12 pages of usual patent propaganda, demands unlimited patentability
for software and business methods worldwide.  Claims, without trying
to offer any evidence, that patents are good for innovation and growth
in %(q:all fields of technology).  We asked at the AIPPI office about
evidence for claims as to the beneficiality of software patents and
received only the the usual dogmas, as they are found in some
below-average patent lawschool textbooks, as a response.

#Pmr: Prof Straus and his subordinates / colleagues seem to be particularly
active and influential in AIPPI

#Brp: Back then, AIPPI conferences were more colorful.  There was serious
thinking about sui generis law for software, based on copyright.  An
AIPPI proposal to this effect was made in the late 70s.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: pleonard ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpataippi ;
# txtlang: en ;
# multlin: t ;
# End: ;

