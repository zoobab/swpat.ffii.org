<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Association Internationale Pour la Protection de la Propriété
Industrielle (AIPPI)

#descr: AIPPI versteht sich als ein Verband der Patentjuristen, Patentinhaber
und sonstigen Nutznießer der Institutionen des Gewerblichen
Rechtschutzes.  AIPPI forderte 1997 in Wien in einer Resolution, dass
Softwarepatente auch in Europa eingeführt werden müssen.  2001
forderte eine AIPPI-Resolution in Melbourne die weltweite Durchsetzung
von Patenten auf Geschäftsverfahren.  2002 forderte AIPPI in einem
offiziellen Papier an WIPO die Einführung eines Weltpatentes, welches
Geschäftsverfahren und Rechenprogramme überall patentieraber macht.

#Ahf: AIPPI report on the Diplomatic Conference of 2000/11

#Pdi: Removal of the computer program exclusion from Art 52 EPC
%(q:postoponed due to massive protests of some software developpers). 
AIPPI protested against the postponement.   The report gives some
interesting details about the [non-]discussion of this issue at the
Conference.

#2np: AIPPI 2003 Resolution Q132 zur Europarl-Abstimmung

#Woi: A short text which tells readers very little about what the Parliament
actually voted for but claims that the TRIPs treaty was violated,
without substantiating this claim.

#1Bd: AIPPI 2001 Swedish delegation: Business Methods should be Patentable

#Pti: At the Melbourne congress of AIPPI, various national delegations
answer questions concerning business methods.  The Swedish delegation
responds that business methods should be directly patentable like any
other methods.

#IoW: In 2002 Bernard Lang submitted questions to AIPPI, asking them about
the factual basis for their repeated assertions that software patents
are needed to promote innovation.  In a written response, AIPPI evaded
and indirectly admitted that they have no basis for these claims, e.g.
no economic studies that support them.

#Wet: Similarly, AIPPI has made false claims about the European Parliament's
amendments to the software patent directive.

#ooh: AIPPI resolutions are regularly composed of false and half-true
statements.  For AIPPI, the patent system is good by default, problems
needn't be addressed, and truth is whatever serves the monetary
interests of AIPPI's constituency.

#jwn: Nevertheless, many scholars of patent law still are proud of being
involved in AIPPI.

#Aur: AIPPI-Resolutionsantrag für uneingeschränkte Patentierbarkeit von
Geschäftsverfahren

#Ahv: AIPPI 2001: Für einen Vorstoß für uneingeschränkte Patentierbarkeit
von Geschäftsverfahren ist die Zeit noch nicht reif

#DiW: Deutsche Patentjuristen: Informatik noch nicht voll ins Patentwesen
integriert, noch kein Konsens für Geschäftsverfahrenspatente

#iWo: includes such goodies as

#Pev: Patentierbarkeit von Geschäftsverfahren

#vue: verlangt dass alle Ideen patentierbar sein sollen und dass
Geschäftsverfahrenspatente wie alle anderen Patenten geprüft und
durchgesetzt werden sollen.

#Bsn: Beziehung zwischen Patenten und Standards

#wjW: weist auf einen Konflikt zwischen Standardisierung und Patenten hin
und gibt Standardisierungsorganisationen guten Rate, wie sie damit
fertig werden sollen.  Es gibt immer ein paar schwarze Schafe, die mit
effektiven Rechtsmitteln wie z.B. dem Wettbewerbsrecht in die
Schranken gewiesen werden können.  Die Aneignung von
De-Fakto-Standards durch Patente ist, bis auf wenige
wettbewerbswidrige Fälle, ein legitimes Mittel der Geschäftspolitik. 
Falls es Probleme geben sollte, liegen sie bestimmt nicht am
Patentwesen oder dessen Ausweitung auf Rechenregeln, so der Tenor
dieser Resolution.

#Aoa: AIPPI position concerning WIPO patent agenda 2002

#1do: 12 pages of usual patent propaganda, demands unlimited patentability
for software and business methods worldwide.  Claims, without trying
to offer any evidence, that patents are good for innovation and growth
in %(q:all fields of technology).  We asked at the AIPPI office about
evidence for claims as to the beneficiality of software patents and
received only the the usual dogmas, as they are found in some
below-average patent lawschool textbooks, as a response.

#Pmr: Prof Straus and his subordinates / colleagues seem to be particularly
active and influential in AIPPI

#Brp: Back then, AIPPI conferences were more colorful.  There was serious
thinking about sui generis law for software, based on copyright.  An
AIPPI proposal to this effect was made in the late 70s.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpataippi ;
# txtlang: de ;
# multlin: t ;
# End: ;

