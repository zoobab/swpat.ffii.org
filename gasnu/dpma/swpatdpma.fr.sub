\begin{subdocument}{swpatdpma}{Deutsches Patent- und Markenamt (DPMA): Verl\"{a}ssliche St\"{u}tze der Patentbewegung in Deutschland}{http://swpat.ffii.org/acteurs/dpma/index.fr.html}{Groupes de travail\\swpatag@ffii.org}{Das Deutsche Patentamt griff selten sichtbar Initiativen f\"{u}r die Absenkung der Patentierbarkeitsstandards.  Es \"{u}berlie{\ss} dies meist dem EPA oder dem BGH.  Doch seine Vertreter feuerten in Fachzeitschriften die inflation\"{a}re Entwicklung an.  In seinen Presseerkl\"{a}rungen feiert das Patentamt wiederum die Ergebnisse dieser Inflation unkritisch als ``positives Signal f\"{u}r den Standort Deutschland''.  Das DPMA steht im Wettbewerb zum Europ\"{a}ischen Patentamt (EPA) und muss um des eigenen \"{U}berlebens willen bei jeder Absenkung der Patentierbarkeitsstandards mitziehen.  Es ist offensichtlich diesem eigenen \"{U}berlebensinteresse viel st\"{a}rker verpflichtet als dem Interesse der Innovation in Deutschland.  Organisatorische Inzucht f\"{o}rdert diese Tendenz:  praktisch alle hohen Funktion\"{a}re des DPMA stammen aus der Patentbewegung, so z.B. aus dem BMJ-Patentreferat oder aus dem DPMA selber.}
\begin{sect}{intro}{Allgemeines}
Anders als das EPA ist das DPMA liefert das DPMA seine Patenteinnahmen beim Staat ab.  Es besteht also ein weniger direktes Interesse an der Patentinflation.  Bez\"{u}glich der Patentierbarkeit von Software wurde der Standpunkt des DPMA in den 90er Jahren weitgehend von dem Leiter der f\"{u}r Datenverarbeitung zust\"{a}ndigen Abteilung, Wolfgang Tauchert, gepr\"{a}gt.  Taucherts pers\"{o}nliches Interesse an einer Ausweitung der Patentierbarkeit und damit einem Bedeutungszuwachs seiner Abteilung ist schwer zu verneinen.  Ein zeitweiliger Pr\"{a}sident des Deutschen Patentamtes, Dr. Georg Landfermann, wechselte im Jahre 2000 vom BMJ auf diesen Posten her\"{u}ber.  Er hatte sich zuvor im Rahmen seiner T\"{a}tigkeit als Regierungsdelegierter auf diversen europ\"{a}ischen Konferenzen f\"{u}r die Ausweitung der Patentierbarkeit auf informatische Innovationen hin eingesetzt.  Gegen Ende 2001 wurde er von dem SPD-Politiker J\"{u}rgen Schade abgel\"{o}st, von dem man sich m\"{o}glicherweise eine gr\"{o}{\ss}er Unabh\"{a}ngigkeit vom Patentmilieu erhoffen kann.
\end{sect}\begin{sect}{pres}{bisherige DPMA-Pr\"{a}sidenten}
\begin{description}
\item[Erich H\"{a}usser:]\ 
\item[Norbert Haugg:]\ 
\item[Dr. Georg Landfermann:]\ 1999-2001, fr\"{u}her BMJ-Patentreferat, dort f\"{u}r die Patentierbarkeit von Software aktiv, seit 2001 Pr\"{a}sident des Bundespatentgerichtes (http://swpat.ffii.org/acteurs/bpatg/index.de.html) und Sonderbeauftragter f\"{u}r die Errichtung einer Europ\"{a}ischen Patentgerichtsbarkeit
\item[J\"{u}rgen Schade, Pr\"{a}sident des DPMA seit 2001 (http://swpat.ffii.org/acteurs/schade/index.de.html):]\ 2001-
\end{description}
\end{sect}\begin{sect}{ref}{Liens Annot\'{e}s}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf DE-PTO 2002-03-12: Patent Record - Positive Signal\footnote{http://www.dpma.de/infos/pressedienst/pm020312a.html}}}

\begin{quote}
Growth of application numbers at the German Patent and Trademark Office (DE-PTO = DPMA) by 15\percent{}, recession in applications at the ``New Market''.  All this of course reflects Germany's buoyant innovation rather than a trend of patent inflation.  As a side note, the DPMA's chief ideologue for technicity questions, Wolfgang Tauchert, is quoted announcing that the DPMA will follow the Federal Court (BGH) in its recent decision (Character Search 2001) to legalise patent claims to computer programs.  The DPMA apparently chooses to ignore the latest directive proposal from Brussels, according to which program claims should not be allowed.
\end{quote}
\filbreak

\item
{\bf {\bf DE-PTO 2002-03-16: Information is Physical, Business Methods Patentable, Program Text not a Disclosure\footnote{http://lists.essential.org/pipermail/random-bits/2002-March/000791.html}}}

\begin{quote}
Greg Aharonian publishes in his newsletter a long article on the concept of technical invention which, Greg says, represents the viewpoint of the German Patent and Tradmark Office (DE-PTO).   In a triumphant comment, Greg points out that according to position expressed in this article, all business methods are technical and therefore patentable in Germany and there is no longer a major difference to Greg's doctrines, which are basically those of the US courts.  The DE-PTO article evidently carries the handwriting of DE-PTO's chief software patentability theoretician, Wolfgang Tauchert, but it is not clear where it was published and to what extent it is an official office position.  At least it can be said that at the time of publication, the DE-PTO also quoted Tauchert with similar statements in an official press release.   Tauchert's position has also created concern, because it suggests that program texts are not enabling disclosures and therefore apparently not novelty-destroying.  This would mean that people can harvest the ideas from other people's open source programs and obtain patents thereon.
\end{quote}
\filbreak

\item
{\bf {\bf Heise-Artikel zum DPMA-Rekord\footnote{http://www.heise.de/newsticker/data/pmz-12.03.02-001/}}}
\filbreak

\item
{\bf {\bf Lebenslauf von Pr\"{a}sident Schade und Vizepr\"{a}sident Hammer\footnote{http://www.dpma.de/infos/amtsleitung/amtsleitung.html}}}
\filbreak

\item
{\bf {\bf Wolfgang Tauchert\footnote{http://swpat.ffii.org/acteurs/tauchert/index.de.html}}}

\begin{quote}
Head of the department at the German Patent Office that started to grow from near zero to several thousand applications per year when the German courts began emulating the EPO in allowing software patents.  Tauchert supported this trend by articles in various journals which argue in the same direction.  In his interpretation, only source code is 'as such' while object code and algorithms are 'not as such'.  His view has received great attention due to his position, and since Mr. Tauchert usually does not shun the efforts needed to found it on learned reasoning, he can be characterised as the ``chief ideologue for technicity questions at the German Patent Office''.
\end{quote}
\filbreak

\item
{\bf {\bf J\"{u}rgen Schade, Pr\"{a}sident des DPMA seit 2001\footnote{http://swpat.ffii.org/acteurs/schade/index.de.html}}}

\begin{quote}
Der neue Pr\"{a}sident des DPMA stammt zwar auch aus der Patentwelt, k\"{o}nnte aber ein wenig unabh\"{a}ngiger sein als manche seiner Vorg\"{a}nger.
\end{quote}
\filbreak

\item
{\bf {\bf Office Europ\'{e}en de Brevets: Au Dessus de la L\'{e}galit\'{e}\footnote{http://swpat.ffii.org/acteurs/epo/index.en.html}}}

\begin{quote}
The European Patent Office finances itself by fees from the patents which it grants.  It is free to use a certain percentage of these fees.  Since the 1980s the EPO has illegally lowered the standards of technicity, novelty, non-obviousness and industrial applicability and abolished examination quality safeguards so as to increase the number of granted patents by more than 10\percent{} and the license tax on the industry by 26\percent{} per year.  As an international organisation, the EPO is not subject to criminal law or taxation.  The local police's power ends at the gates of the EPO.  High EPO officials have inflicted corporal injury on their employees and then escaped legal consequences by their right to immunity.  The work climate within the EPO is very bad, leading to several suicides per year.  The quality of examination reached a relative high in the 80s but has after that been deteriorating, partly because the EPO had to hire too many people too quickly for too low wages.  Examiners who reject patents load more work on themselves without getting more pay.  Examiners are treated by the EPO management as a kind of obstacle to the corporate goal of earning even more patent revenues.  The high-level employees of the EPO owe their jobs to political pressures from within national patent administrations and do not understand the daily work of the office.  The EPO has its own jurisdictional arm, consisting of people whose career is controlled by the EPO's managment, which again is strongly influenced by industry patent lawyers (on the Standing Advisory Committee (SACEPO)) and by the Office's internal climate.  The national organs that are supposed to supervise the EPO are all part of the same closed circle, thus guaranteeing the EPO managment enjoys feudal powers beyond constitutional legality, and that whatever they decide is propagated to the national administrations and lawcourts.
\end{quote}
\filbreak

\item
{\bf {\bf Gesetzeswidrige Wirtschaftspolitik des BGH-Patentsenates\footnote{http://swpat.ffii.org/acteurs/bgh/index.de.html}}}

\begin{quote}
Patent Senate of the German Federal Court
\end{quote}
\filbreak

\item
{\bf {\bf Bundespatentgericht\footnote{http://swpat.ffii.org/acteurs/bpatg/index.de.html}}}

\begin{quote}
In den 60er Jahren aus dem Deutschen Patentamt herausgeklagt, seitdem institutionell einigerma{\ss}en unabh\"{a}ngig (zumindest formelle Reste der Abh\"{a}ngigkeit sind noch vorhanden).  Zwei Senate besch\"{a}ftigen sich mit Softwarepatenten.  Bis um 1973 tendierte das BPatG zu einer laxen Haltung, danach kam eine Wende.  Der 17. Senat bem\"{u}ht sich bis heute, das Gesetz anzuwenden und Softwarepatente zu vermeiden, verweigert dabei regelm\"{a}{\ss}ig EPA und BGH die Gefolgschaft.  Der 21. Senat, der mehr f\"{u}r Elektronik zust\"{a}ndig ist, neigt unter seinem Vorsitzenden Wilfried Anders zu einer grenzenlosen Ausweitung der Patentierbarkeit und greift dabei sogar EPA und BGH gelegentlich vor.  Patentanw\"{a}lte und Richter haben Wege gefunden, m\"{o}glichst auch F\"{a}lle, die nichts mit Elektronik zu tun haben und eigentlich in die Zust\"{a}ndigkeit des 17. Senates fallen w\"{u}rden, dem 21. Senat zu \"{u}bergeben.
\end{quote}
\filbreak

\item
{\bf {\bf Minist\`{e}re F\'{e}d\'{e}ral de Justice et Brevets Logiciels\footnote{http://swpat.ffii.org/acteurs/bmj/index.de.html}}}

\begin{quote}
F\"{u}r alle Gesetzgebung im Bereich des Patentwesens ist innerhalb der Bundesregierung das Bundesministerium der Justiz (BMJ) zust\"{a}ndig.  Das BMJ unterh\"{a}lt in seiner Abteilung f\"{u}r Industrie und Handel ein eigenes Patentreferat.  Zum Gesch\"{a}ftsbereich des BMJ geh\"{o}ren ferner das Deutsche Patent- und Markenamt (DPMA), das Bundespatentgericht (BPatG) und der Bundesgerichtshof (BGH).  Zu all diesen Organisationen und auch zum Europ\"{a}ischen Patentamt (EPA) hin bestehen innige personelle Verflechtungen.  Das BMJ-Patentreferat ist personell schwach ausgestattet und verl\"{a}sst sich daher auf EPA, DPMA und andere Patentbewegungs-Institutionen.  Die Karierre der BMJ-Beamten verl\"{a}uft h\"{a}ufig innerhalb des Patentwesens.  Sie folgen im allgemeinen (aus Gewohnheit) wortgetreu deren (von wenigen Instanzen beschlossener) herrschender Meinung und beschr\"{a}nken die argumentative Auseinandersetzung meist von vorneherein auf grammatische Fragen.  Innerhalb der Bundesregierung und des Europ\"{a}ischen Rates vertreten sie de facto die Interessen der Patentinstitutionen, und sie tun dies unter Einsatz ihrer gesetzgeberischen Macht.
\end{quote}
\filbreak

\item
{\bf {\bf \"{U}ber die Patentpr\"{u}fung von Programmen f\"{u}r Datenverarbeitungsanlagen\footnote{http://swpat.ffii.org/papiers/grur-skk01/index.de.html}}}

\begin{quote}
Dr. Swen Kiesewetter-K\"{o}binger, examinateur \`{a} l'Office allemand des brevets, critique violemment les incons\'{e}quences de la pratique actuelle en mati\`{e}re de brevetabilit\'{e} de logiciels telles qu'il les observe quotidiennement, et les met en perspective par une critique des nouvelles doctrines de la BGH expos\'{e}es par Richter Melullis -- une analyse brillante et profonde de novembre 2000. Kiesewetter-K\"{o}binger trouve que les pr\'{e}tentions habituelles de fonctionnalit\'{e}s des brevets logiciels ne sont pas diff\'{e}rentes de celles des programmes de pr\'{e}sentation de l'information en tant que tels. Il \'{e}crit aussi qu'en plus d'une \'{e}vidente ill\'{e}galit\'{e}, cette pratique pose des probl\`{e}mes pr\'{e}occupants \`{a} plus d'un titre, non seulement pour les d\'{e}veloppeurs de logiciels mais aussi pour le syst\`{e}me juridique.
\end{quote}
\filbreak

\item
{\bf {\bf Sch\"{o}lch 2001: Softwarepatente ohne Grenzen?\footnote{http://swpat.ffii.org/papiers/grur-schoelch01/index.de.html}}}

\begin{quote}
In 2000 the 10th Senate of the German Federal Court of Justice (BGH/10) published the verdicts ``Sprachanalyse'' (Language Analysis) and ``Logikverifikation'' (logic verification) and with them a new doctrine that makes anything patentable that can be described as a ``program-technical device''.  The BGH/10 overruled decisions of another court that had rejected the same patent applications due to lack of technicity (technical character).  The 17th Senate of the Federal Patent Court (BPatG/17) had applied the ``core theory'', i.e. differentiated between new and old technology and demanded that the new and inventive part (i.e. the core of the invention) be in the ``technical'' realm, i.e. that it contribute a ``teaching in the area of applied natural science'', outside the scope of the list of exclusions on \S1(2) PatG aka Art 52(2) EPC.  The new verdict will on the contrary admit any claims even if only a non-inventive periphery is within the ``technical sphere''.  Applied to organ building this would mean that not only a new way to build organ pipes would be considered a technical invention, but also a new piece of music played on the organ, as the author of this article observes.  G\"{u}nter Sch\"{o}lch, who is confronted with dubious software patents every day in his work at the German Patent Office, finds the BGH/10 decision unconvincing and warns that they will lead to a flood of harmful patents.  Sch\"{o}lch also reviews the process of of patent inflation (gradual expansion of the scope of patentability) during the last decade and warns about dangerous social consequences.
\end{quote}
\filbreak

\item
{\bf {\bf G\"{u}nter Sch\"{o}lch: Stellungnahme zum Sondierungspapier\footnote{http://swpat.ffii.org/papiers/eukonsult00/angumema/index.en.html}}}

\begin{quote}
An examiner of the German Patent Office points out that the EU patent department's consultation paper uses a meaningless concept of technical character and sticks to a shoddy reasoning that dates back to the EPO caselaw of the Sohei decision (1986).  It is this sophistry which has created the legal insecurity in Europe, and perpetuating this sophistry just perpetuates legal insecurity.  If we want to tackle the legal insecurity, we need to break with this sophistry, delete the much-abused 'as such' clause (Art 52(3) EPC) and reestablish clear and consistent definitions of what is a technical invention.   It is not enough that the claims contain technical features.  Not the claim wording but the invention must be technical.  The EPO's approach is to compare even non-technical problem solutions with with non-technical closest prior art and then deciding whether any technical feature is contained in the difference.  This is illogical and circular.  Such approaches, as proposed also in the consultation paper, are apparently using the ``technical character'' doctrine only as a cover-up for their real purpose, which is to make anything man-made under the sun patentable, like in the US.  However this could have even graver consequences in Europe than in the US, given that the EPO and European courts tends to apply formalistic rules that favor the patentee and make claims have a broader effect than the same claims would have in the US.  On the whole it can be said that the adventure of expanding patentability to all ideas constitutes a rupture of occidental civilisation and a course into a brave new world whose outline is just gradually appearing on the horizon in the US.  This adventure is undertaken in spite of strong scientific evidence in its disfavor, supported only by the irrational belief of a well entrenched lobby in the universally beneficial effect of property rights.
\end{quote}
\filbreak

\item
{\bf {\bf Dr. Swen Kiesewetter-K\"{o}binger: Stellungnahme zur Patentierbarkeit von Softwarekonzepten\footnote{http://swpat.ffii.org/dates/2001/bundestag/kiesew/index.de.html}}}

\begin{quote}
Die vorgesehene Einf\"{u}hrung der Patentierbarkeit von Softwarekonzepten l\"{a}{\ss}t unter anderem Zweifel an der Verfassungsgsm\"{a}{\ss}igkeit der daraus resultierenden Folgen aufkommen. Gro{\ss}e Teile der Antworten zu dem gestellten Fragenkatalog sind von dieser Sorge gepr\"{a}gt. Da einige dieser \"{U}berlegungen noch ziemlich neu sind und bisherigen, zu oberfl\"{a}chlichen Betrachtungen widersprechen, hat deren Ausf\"{u}hrung einen breiten Raum eingenommen. Hoffentlich gelang es trotzdem, die Problematik der Patentierung von Software mit typischem Werkcharakter bei gleichzeitigem Schutz durch das Urheberrecht allgemeinverst\"{a}ndlich darzustellen.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: mlatex ;
% End: ;

