<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Das Deutsche Patentamt griff selten sichtbar Initiativen für die Absenkung der Patentierbarkeitsstandards.  Es überließ dies meist dem EPA oder dem BGH.  Doch seine Vertreter feuerten in Fachzeitschriften die inflationäre Entwicklung an.  In seinen Presseerklärungen feiert das Patentamt wiederum die Ergebnisse dieser Inflation unkritisch als %(q:positives Signal für den Standort Deutschland).  Das DPMA steht im Wettbewerb zum Europäischen Patentamt (EPA) und muss um des eigenen Überlebens willen bei jeder Absenkung der Patentierbarkeitsstandards mitziehen.  Es ist offensichtlich diesem eigenen Überlebensinteresse viel stärker verpflichtet als dem Interesse der Innovation in Deutschland.  Organisatorische Inzucht fördert diese Tendenz:  praktisch alle hohen Funktionäre des DPMA stammen aus der Patentbewegung, so z.B. aus dem BMJ-Patentreferat oder aus dem DPMA selber.
title: Deutsches Patent- und Markenamt (DPMA): Verlässliche Stütze der Patentbewegung in Deutschland
Age: Allgemeines
ber: bisherige DPMA-Präsidenten
AeW2: Anders als das EPA ist das DPMA liefert das DPMA seine Patenteinnahmen beim Staat ab.  Es besteht also ein weniger direktes Interesse an der Patentinflation.  Bezüglich der Patentierbarkeit von Software wurde der Standpunkt des DPMA in den 90er Jahren weitgehend von dem Leiter der für Datenverarbeitung zuständigen Abteilung, Wolfgang Tauchert, geprägt.  Taucherts persönliches Interesse an einer Ausweitung der Patentierbarkeit und damit einem Bedeutungszuwachs seiner Abteilung ist schwer zu verneinen.  Ein zeitweiliger Präsident des Deutschen Patentamtes, Dr. Georg Landfermann, wechselte im Jahre 2000 vom BMJ auf diesen Posten herüber.  Er hatte sich zuvor im Rahmen seiner Tätigkeit als Regierungsdelegierter auf diversen europäischen Konferenzen für die Ausweitung der Patentierbarkeit auf informatische Innovationen hin eingesetzt.  Gegen Ende 2001 wurde er von dem SPD-Politiker Jürgen Schade abgelöst, von dem man sich möglicherweise eine größer Unabhängigkeit vom Patentmilieu erhoffen kann.
1ri: 1999-2001, früher BMJ-Patentreferat, dort für die Patentierbarkeit von Software aktiv, seit 2001 Präsident des %(bp:Bundespatentgerichtes) und Sonderbeauftragter für die Errichtung einer Europäischen Patentgerichtsbarkeit

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatdpma ;
# txtlang: en ;
# End: ;

