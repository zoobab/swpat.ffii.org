\begin{subdocument}{swpatdpma}{Deutsches Patent- und Markenamt und Softwarepatente}{http://swpat.ffii.org/gasnu/dpma/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2004/12/03 von FFII\footnote{\url{http://lists.ffii.org/mailman/listinfo/traduk}}}{Das Deutsche Patentamt griff selten sichtbar Initiativen f\"{u}r die Absenkung der Patentierbarkeitsstandards.  Es \"{u}berlie{\ss} dies meist dem EPA oder dem BGH.  Doch seine Vertreter feuerten in Fachzeitschriften die inflation\"{a}re Entwicklung an.  In seinen Presseerkl\"{a}rungen feiert das Patentamt wiederum die Ergebnisse dieser Inflation unkritisch als ``positives Signal f\"{u}r den Standort Deutschland''.  Das DPMA steht im Wettbewerb zum Europ\"{a}ischen Patentamt (EPA) und muss um des eigenen \"{U}berlebens willen bei jeder Absenkung der Patentierbarkeitsstandards mitziehen.  Es ist offensichtlich diesem eigenen \"{U}berlebensinteresse viel st\"{a}rker verpflichtet als dem Interesse der Innovation in Deutschland.  Organisatorische Inzucht f\"{o}rdert diese Tendenz:  praktisch alle hohen Funktion\"{a}re des DPMA stammen aus der Patentbewegung, so z.B. aus dem BMJ-Patentreferat oder aus dem DPMA selber.}
\begin{sect}{intro}{Allgemeines}
Anders als das EPA ist das DPMA liefert das DPMA seine Patenteinnahmen beim Staat ab.  Es besteht also ein weniger direktes Interesse an der Patentinflation.  Bez\"{u}glich der Patentierbarkeit von Software wurde der Standpunkt des DPMA in den 90er Jahren weitgehend von dem Leiter der f\"{u}r Datenverarbeitung zust\"{a}ndigen Abteilung, Wolfgang Tauchert, gepr\"{a}gt.  Taucherts pers\"{o}nliches Interesse an einer Ausweitung der Patentierbarkeit und damit einem Bedeutungszuwachs seiner Abteilung ist schwer zu verneinen.  Ein zeitweiliger Pr\"{a}sident des Deutschen Patentamtes, Dr. Georg Landfermann, wechselte im Jahre 2000 vom BMJ auf diesen Posten her\"{u}ber.  Er hatte sich zuvor im Rahmen seiner T\"{a}tigkeit als Regierungsdelegierter auf diversen europ\"{a}ischen Konferenzen f\"{u}r die Ausweitung der Patentierbarkeit auf informatische Innovationen hin eingesetzt.  Gegen Ende 2001 wurde er von dem SPD-Politiker J\"{u}rgen Schade abgel\"{o}st, der auch von Haus aus Patentjurist ist.
\end{sect}

\begin{sect}{pres}{Bisherige DPMA-Pr\"{a}sidenten}
\begin{description}
\item[Erich H\"{a}usser:]\ 
\item[Norbert Haugg:]\ 
\item[Dr. Georg Landfermann:]\ 1999-2001, fr\"{u}her BMJ-Patentreferat, dort f\"{u}r die Patentierbarkeit von Software aktiv, seit 2001 Pr\"{a}sident des Bundespatentgerichtes\footnote{\url{http://localhost/swpat/gasnu/bpatg/index.de.html}} und Sonderbeauftragter f\"{u}r die Errichtung einer Europ\"{a}ischen Patentgerichtsbarkeit
\item[Jürgen Schade, Präsident des DPMA seit 2001\footnote{\url{http://localhost/swpat/gasnu/schade/index.de.html}}:]\ 2001-
\end{description}
\end{sect}

\begin{sect}{ref}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf (DPMA 2002-03-12: Patentrekord - Positives Signal)\footnote{\url{http://www.dpma.de/infos/pressedienst/pm020312a.html}}}}

\begin{quote}
Wachstum der Anmeldezahlen am Deutschen Patent- und Markenamt (DPMA) um 15\percent{}, Einbruch am neuen Markt, nat\"{u}rlich alles ein Spiegel der lebendigen Innovation und nicht etwa der Patentinflation.  Nebenbei wird Swpat-Chefideologe Tauchert mit einer Aussage zur neuen Politik des DPMA hinsichtlich Software und Gesch\"{a}ftsmethoden zitiert.  Tauchert k\"{u}ndigt an, dass das Patentamt die neueste BGH-Entscheidung (Zeichensuche 2001) umsetzen und somit unmittelbare Anspr\"{u}che auf Programme zulassen werde.  Damit prescht das DPMA in einem Punkt vor, in dem sogar der neue Br\"{u}sseler Richtlinienvorschlag Zur\"{u}ckhaltung empfiehlt.
\end{quote}
\filbreak

\item
{\bf {\bf DPMA 2002-03-16: Information ist eine Naturkraft, Geschäftsverfahren Patentierbar, Programmtexte keine Offenbarung\footnote{\url{http://lists.essential.org/pipermail/random-bits/2002-March/000791.html}}}}

\begin{quote}
Greg Aharonian (GA) ver\"{o}ffentlicht in seinem Rundschreiben einen Artikel \"{u}ber den Begriff der Technischen Erfindung, der, so GA, den offiziellen Standpunkt des Deutschen Patent- und Markenamtes (DPMA) darstellt.  In einem triumphierenden Einleitungskommentar findet GA darin seine eigene Meinung wieder, wonach alle Gesch\"{a}ftsverfahren technisch sind und patentierbar sein m\"{u}ssen.  Der DPMA-Artikel tr\"{a}gt die Handschrift des DPMA-Software-Chefideologen Wolfgang Tauchert.  Es ist aber bislang unklar, in wieweit es sich hierbei um einen offiziellen Amtsstandpunkt handelt.  Auff\"{a}llig ist aber, dass Tauchert zeitgleich mit der Ver\"{o}ffentlichung dieses Artikels in einer DPMA-Presseerkl\"{a}rung als Autorit\"{a}t zitiert wird und eine neue Runde der Patentinflation einl\"{a}uten darf.  Das Positionspapier beunruhigt auch dadurch, dass es Programm-Quelltexte als f\"{u}r eine Offenbarung einer Lehre ungeeignet abqualifiziert.  Hieraus scheint zu folgen, dass ein Patentgoldgr\"{a}ber nur quelloffene Programme anderer Leute abgrasen und in Patentsprech umformulieren muss, um darauf selber Patente zu erhalten und die eigentlichen Autoren zu blockieren.
\end{quote}
\filbreak

\item
{\bf {\bf (Heise-Artikel zum DPMA-Rekord)\footnote{\url{http://www.heise.de/newsticker/data/pmz-12.03.02-001/}}}}
\filbreak

\item
{\bf {\bf (Lebenslauf von Präsident Schade und Vizepräsident Hammer)\footnote{\url{http://www.dpma.de/infos/amtsleitung/amtsleitung.html}}}}
\filbreak

\item
{\bf {\bf Wolfgang Tauchert\footnote{\url{http://localhost/swpat/gasnu/tauchert/index.de.html}}}}

\begin{quote}
Leiter der Swpat-Abteilung im Deutschen Patent- und Markenamt (DPMA).  Der Patent-Durchsatz seiner Abteilung wuchs in wenigen Jahren von nahe Null auf einige Tausend, als das DPMA begann, mit Billigung des BGH dem EPA in der Erteilung von Softwarepatenten nachzueifern.  Tauchert feuerte diesen Trend mit allerlei Artikeln an, in denen er auch einige pers\"{o}nliche Akzente setzt.  Laut Tauchert ist nur im Quelltext eines Programms das Programm als solches zu sehen.  Programmbezogene Bin\"{a}rdateien, Algorithmen, Funktionalit\"{a}ten usw sind hingegen keine Programme als solche.  Diese f\"{u}r Informatiker schwer nachvollziehbare Sichtweise erfreut sich unter Patentanmeldern und Patentjuristen gro{\ss}er Beachtung, zumal Tauchert sie mit beeindruckender Quellengelehrsamkeit vortr\"{a}gt.
\end{quote}
\filbreak

\item
{\bf {\bf Jürgen Schade, Präsident des DPMA seit 2001\footnote{\url{http://localhost/swpat/gasnu/schade/index.de.html}}}}

\begin{quote}
Der neue Pr\"{a}sident des DPMA stammt zwar auch aus der Patentwelt, k\"{o}nnte aber ein wenig unabh\"{a}ngiger sein als manche seiner Vorg\"{a}nger.
\end{quote}
\filbreak

\item
{\bf {\bf Europäisches Patentamt: Hoch über dem Gesetz\footnote{\url{http://localhost/swpat/gasnu/epo/index.de.html}}}}

\begin{quote}
Das Europ\"{a}ische Patentamt finanziert sich durch Einnahmen aus Patentgeb\"{u}hren.  Es kann \"{u}ber diese Einnahmen frei verf\"{u}gen.  Je mehr Patente es erteilt, desto besser geht es seinen Bediensteten.  Es bestimmt seit den sp\"{a}ten 80er Jahren de facto selber, nach welchen Kriterien es die Patent erteilt.  Seit Mitte der 90er Jahre ist das EPA bestrebt, f\"{u}r diese Tatsache gesetzliche R\"{u}ckendeckung zu erhalten.  Das EPA versteht sich als Dienstleister an seine Kunden, den Patentinhabern.  Es kostet einen Pr\"{u}fer wesentlich mehr Aufwand, ein Patent zur\"{u}ckzuweisen, als es zu erteilen.  Das EPA ist personell und finanziell viel besser ausgestattet als die zust\"{a}ndigen Stellen in Berlin oder Br\"{u}ssel.  Daher ist das EPA trotz theoretischer politischer Enthaltsamkeit das eigentliche politische Zenrum der Patentbewegung in Europa.  Eine wichtige Rolle hierbei spielt auch der St\"{a}ndige Beirat aus Industrie-Patentanw\"{a}lten, SACEPO.
\end{quote}
\filbreak

\item
{\bf {\bf Gesetzeswidrige Wirtschaftspolitik des BGH-Patentsenates\footnote{\url{http://localhost/swpat/gasnu/bgh/index.de.html}}}}

\begin{quote}
Patent Senate of the German Federal Court
\end{quote}
\filbreak

\item
{\bf {\bf Bundespatentgericht\footnote{\url{http://localhost/swpat/gasnu/bpatg/index.de.html}}}}

\begin{quote}
In den 60er Jahren aus dem Deutschen Patentamt herausgeklagt, seitdem institutionell einigerma{\ss}en unabh\"{a}ngig (zumindest formelle Reste der Abh\"{a}ngigkeit sind noch vorhanden).  Zwei Senate besch\"{a}ftigen sich mit Softwarepatenten.  Bis um 1973 tendierte das BPatG zu einer laxen Haltung, danach kam eine Wende.  Der 17. Senat bem\"{u}ht sich bis heute, das Gesetz anzuwenden und Softwarepatente zu vermeiden, verweigert dabei regelm\"{a}{\ss}ig EPA und BGH die Gefolgschaft.  Der 21. Senat, der mehr f\"{u}r Elektronik zust\"{a}ndig ist, neigt unter seinem Vorsitzenden Wilfried Anders zu einer grenzenlosen Ausweitung der Patentierbarkeit und greift dabei sogar EPA und BGH gelegentlich vor.  Patentanw\"{a}lte und Richter haben Wege gefunden, m\"{o}glichst auch F\"{a}lle, die nichts mit Elektronik zu tun haben und eigentlich in die Zust\"{a}ndigkeit des 17. Senates fallen w\"{u}rden, dem 21. Senat zu \"{u}bergeben.
\end{quote}
\filbreak

\item
{\bf {\bf BMJ: Ängstlicher Vorreiter der Patentinflation in DE und EU\footnote{\url{http://localhost/swpat/gasnu/bmj/index.de.html}}}}

\begin{quote}
F\"{u}r alle Gesetzgebung im Bereich des Patentwesens ist innerhalb der Bundesregierung das Bundesministerium der Justiz (BMJ) zust\"{a}ndig.  Das BMJ unterh\"{a}lt in seiner Abteilung f\"{u}r Industrie und Handel ein eigenes Patentreferat.  Zum Gesch\"{a}ftsbereich des BMJ geh\"{o}ren ferner das Deutsche Patent- und Markenamt (DPMA), das Bundespatentgericht (BPatG) und der Bundesgerichtshof (BGH).  Zu all diesen Organisationen und auch zum Europ\"{a}ischen Patentamt (EPA) hin bestehen innige personelle Verflechtungen.  Das BMJ-Patentreferat ist personell schwach ausgestattet und verl\"{a}sst sich daher auf EPA, DPMA und andere Patentbewegungs-Institutionen.  Die Karierre der BMJ-Beamten verl\"{a}uft h\"{a}ufig innerhalb des Patentwesens.  Sie folgen im allgemeinen (aus Gewohnheit) wortgetreu deren (von wenigen Instanzen beschlossener) herrschender Meinung und beschr\"{a}nken die argumentative Auseinandersetzung meist von vorneherein auf grammatische Fragen.  Innerhalb der Bundesregierung und des Europ\"{a}ischen Rates vertreten sie energisch die Interessen der Patentanw\"{a}lte f\"{u}hrender Gro{\ss}konzerne und in zweiter Linie der Patentinstitutionen.  Sie verbitten sich Einmischungen ``fachfremder'' Personen (einschlie{\ss}lich BMWi, Abgeordnete) in ihren Kreis und begegnen diesen durch Ignorieren oder sonstige Diskussionsverhinderungsstrategien.  Auch von tausenden von Personen unterzeichnete Briefe an das BMJ-Patentreferat (Dr Welp) blieben bisher unbeantwortet.
\end{quote}
\filbreak

\item
{\bf {\bf Kiesewetter-Köbinger 2000: Über die Patentprüfung von Programmen für Datenverarbeitungsanlagen\footnote{\url{http://localhost/swpat/papri/grur-skk01/index.de.html}}}}

\begin{quote}
Ein Patentpr\"{u}fer zeigt die Ungereimtheiten der Pr\"{u}fung von Software-Anmeldungen auf.  In ihrem Bem\"{u}hen, ein Gesetz umzuinterpretieren, welches unmissverst\"{a}ndlich die Patentierung von Datenverarbeitungsprogrammen verbietet, hat die Rechtsprechung im Laufe der Jahre Funktionsanspr\"{u}che zugelassen, die es dem Anmelder erlauben, ein Programm zu verkleiden.  Aber diese Funktionsanspr\"{u}che stellen eher Probleme als L\"{o}sungen dar, und die L\"{o}sung zu diesen Problemen besteht in einem (nicht patentierbaren) Datenverarbeitungsprogramm (als solchem).  Probleme zu patentieren ist aber noch weniger zul\"{a}ssig und in seinen Auswirkungen noch bedenklicher als Programme zu patentieren.
\end{quote}
\filbreak

\item
{\bf {\bf Schölch 2001: Softwarepatente ohne Grenzen?\footnote{\url{http://localhost/swpat/papri/grur-schoelch01/index.de.html}}}}

\begin{quote}
Im Jahre 2000 ver\"{o}ffentlichte der 10. Senat des Bundesgerichtshofes (BGH/10) mit den Urteilen ``Sprachanalyse'' und ``Logikverifikation'' eine neue Doktrin \"{u}ber die grunds\"{a}tzliche Patentierbarkeit von allem, was als ``programmtechnischen Vorrichtung'' beschrieben werden kann.  Der BGH/10 verwarf dabei Urteile einer unteren Instanz, welche die selben Patentantr\"{a}ge mangels Technizit\"{a}t zur\"{u}ckgewiesen hatte.  Der 17. Senat des Bundespatentgerichtes (BPatG/17) hatte eine Differenzbetrachtung (Kerntheorie) angewandt und konnte daher in den fraglichen Patentantr\"{a}gen keinen ``Beitrag zum Stand der Technik'', m.a.W. keine ``neue Lehre auf dem Gebiet der angewandten Naturwissenschaften'' erkennen.  G\"{u}nter Sch\"{o}lch, der als Pr\"{u}fer am Deutschen Patentamt t\"{a}glich mit zweifelhaften Programmlogik-Patentantr\"{a}gen konfrontiert ist, findet die neuen Vorgaben des BGH/10 weniger einleuchtend als die Position des BPatG/17 und sorgt sich um die gesellschaftlichen Folgen einer grenzenlosen Patentierbarkeit.
\end{quote}
\filbreak

\item
{\bf {\bf Günther Schölch 2000: Stellungnahme zur EU-Konsultation zu Softwarepatenten\footnote{\url{http://localhost/swpat/papri/eukonsult00/angumema/index.de.html}}}}

\begin{quote}
G\"{u}nter Sch\"{o}lch, Pr\"{u}fer am Deutschen Patentamt, zeigt auf, dass das Konsultationspapier der EU-Patentabteilung eine sinnentleerte Version des Technikbegriffs verwendet und auf holprigen patentjuristischen Sophistereien aus dem EPA-Beschluss Sohei aufbaut.  Diesen Sophistereien verdanken wir die heutige Rechtsunsicherheit, und mit ihr gilt es zu brechen, wenn wir Rechtssicherheit wiederherstellen wollen, was ja das Ziel der geplanten Richtlinie sein soll.  Dazu sollte die allzu sehr mi{\ss}brauchte ``Als-Solches''-Klausel aus Art 52 EP\"{U} gestrichen und ein von momentanen Rechtsprechungen unabh\"{a}ngig klar definierter Begriff der ``Technischen Erfindung'' wieder eingef\"{u}hrt werden.  Es gen\"{u}gt nicht, dass die Patentanspr\"{u}che technische Merkmale enthalten.  Die Erfindung muss technisch sein.   Die Methode des EPA, auch nicht-technische Probleml\"{o}sungen mit dem letzten Stand der (Nicht-)Technik zu vergleichen und dann zu fragen, ob die Differenz zwischen beiden technische Merkmale enth\"{a}lt, ist zirkul\"{a}r und unlogisch.  Solche Methoden, wie auch das Konsultationspapier sie empfiehlt, sind nur ein Feigenblatt.  Sinn der \"{U}bung ist die Einf\"{u}hrung einer unbegrenzten Patentierbarkeit amerikanischen Musters.  Ein solcher Zustand h\"{a}tte in Europa schlimmere Folgen als in den USA, da die Gerichte bei uns dazu neigen, zugunsten des Patentinhabers Formalismen anzuwenden, so dass ein und derselbe Patentanspruch in Europa breiter ausgelegt und durchgesetzt wird als in den USA.  Die Entgrenzung des Patentwesens kommt einem Bruch in der abendl\"{a}ndischen Zivilisation gleich.  Alle bisherigen wissenschaftlichen Erkenntisse sprechen dagegen, dass hierdurch irgend etwas zu gewinnen w\"{a}re.
\end{quote}
\filbreak

\item
{\bf {\bf Dr. Swen Kiesewetter-Köbinger: Stellungnahme zur Patentierbarkeit von Softwarekonzepten\footnote{\url{http://localhost/swpat/penmi/2001/bundestag/kiesew/index.de.html}}}}

\begin{quote}
Die vorgesehene Einf\"{u}hrung der Patentierbarkeit von Softwarekonzepten l\"{a}{\ss}t unter anderem Zweifel an der Verfassungsgsm\"{a}{\ss}igkeit der daraus resultierenden Folgen aufkommen. Gro{\ss}e Teile der Antworten zu dem gestellten Fragenkatalog sind von dieser Sorge gepr\"{a}gt. Da einige dieser \"{U}berlegungen noch ziemlich neu sind und bisherigen, zu oberfl\"{a}chlichen Betrachtungen widersprechen, hat deren Ausf\"{u}hrung einen breiten Raum eingenommen. Hoffentlich gelang es trotzdem, die Problematik der Patentierung von Software mit typischem Werkcharakter bei gleichzeitigem Schutz durch das Urheberrecht allgemeinverst\"{a}ndlich darzustellen.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

