<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#leu: contains a lot of info on Siemens patenting activities in the automation field

#CnW: Critical response to ZVEI by Linux-Verband

#Lmg: LiVe argumentiert gegen ZVEI

#sal: speech by the Siemens patent chief at an EU sponsored software patentability rally in London 1998.  Explains that patents are necessary because (a) software ideas are %(q:strategic) (b) anything strategic must be ownable (c) copyright doesn't allow idea ownership.  Apart from these faulty premises, the speech contains a lot of interesting facts and thoughts.

#AWn: Arno Koerber: What do Software Patents mean to European Industry

#ZWw: ZdNet report about a ZVEI statement in favour of software patentability which was, as we found later, written by Arno Körber from Siemens.  The people at the ZVEI office didn't seem to know anything about the debate about software patents nor that ZVEI had intervened in this debate.

#ZWe: ZVEI für Softwarepatente

#Glw: GNN report about a ZVEI statement in favour of software patentability which was, as we found later, written by Arno Körber from Siemens.  The people at the ZVEI office didn't seem to know anything about the debate about software patents nor that ZVEI had intervened in this debate.

#Zfa: ZVEI fordert umfassenden Software-Patentschutz

#Mth: Members of the board and leaders of the patent department speak proudly about the fruits of their work of building an efficient patent incentive and management system: >6000 patents per year, 60% in software, aggressive use especially in the USA, charging of licensing fees even within the Siemens group, so as to improve the incentive effect.  Siemens no longer has the image of the %(q:peaceful giant), litigation fees have soared by 100% in the last 10 years, patent department with 200 employees is one of the most profit-oriented %(q:patent law companies), especially in the US, which Siemens views as the key market where the future is decided, also because Europe still seems to be a bit reluctant in granting swpat.

#Stn: Siemens patent department 2002: patents are sexy, aggressive strategic use in IT area decisive for succes of Siemens

#EKn: A Siemens patent expert reports about the new strategic importance of patents for Siemens and the market in general, and lists figures: 150 patent experts, 100K patents, 1G license revenues, strategies, battles, ambushes, ...

#UxW: Ulrich Eberl 1998: Kalter Krieg um neue Erfindungen

#Wrl: Dr. Horst Fischer, patent department head of Siemens since 1994, declares in an interview with Siemens Webzine that patents are %(q:potent weapons) and that software patents will play an increasingly great role for the strategy of Siemens: %(bc:Software patents even account for an increasingly large share of our portfolio. Some groups, such as Automation & Drives and Medical Solutions, have launched special initiatives for such patents. Initial registration of these property rights is increasingly occurring in the U.S., because the legal framework for software patents is very advanced there. This trend is even more pronounced in the case of patents for %(q:business models), in other words, patents for electronic business solutions. Here we've set up a worldwide coordination and counseling center.)

#stt: Siemens 2001-3/4: Potent Weapons, US system most advanced

#yte: Siemens has for many years cultivated very close strategic alliance with %(MS), another firm with a keen interested in establishing software patents.

#ZWr: Among the organisations which have been under heavy influence of the Siemens patent department are ZVEI, Bitkom, BDI/Unice.  Chief patent strategist Arno Körber was the author of a ZVEI paper calling for software patentability in 1999/07.

#ttW: A newer current of opinion among engineers and managers at Siemens thinks more sceptically about the patent process.  Younger managers have told us that %(q:patents are a dragging our foot and not needed for protecting investments in the telecommunication sector).  This may be related to their experience of negotiating with QualComm and other small companies who do not develop products but only patents and who assert these patents aggressively against big players such as Siemens.  Many Siemens employees have signed the %(ep:Petition for a Free Europe without Software Patents).  Yet this current has so far not been able to influence the presentation of Siemens on the political stage.

#Was: Many at Siemens are used to the patent system and associate success experiences with it.  They are afraid that the continued informatisation of many problems will erode the advantages which they have drawn from the patent game.  Basically their approach is conservative: they want to repeat past success experiences.  In a similar spirit, they are afraid of the profound change in the patent system which this implies and therefore keen to reassure everybody that they or course %(q:do not want business method patents).  Yet this does not prevent the Siemens patent department from applying for lots of business method patents and trivial functionality patents (see tabular listing below), nor does it prevent the patent policy representatives of Siemens from strongly endorsing and supporting all legislative initiatives which make such patents obligatory.  The fear of losing the patentopoly success experience is greater than the fear of patent inflation.

#bto: In the late 80s and early 90s, Arno Körber, head of the patent department of Siemens, successfully installed a patent incentive system at Siemens which made the patenting numbers soar to the sky and helped leveraging the economy-of-scale advantage of Siemens as a large company.

#eeh: Siemens E-Patents at the EPO

#descr: Siemens has always been at the forefront of the patent movement.  Yet, even those Siemens patent cadres who most actively promote software patentability within industrial and governmental organisations do not try to argue that this is beneficial, either for Siemens or for the world.  They merely say that Europe has to follow the lead of the US, which sets the standards in the global market.  Meanwhile, many Siemens managers in IT and telecommunications believe that patents are putting brakes on their activities and doing substantial harm to the industry.

#title: Siemens and Software Patents

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsiemens ;
# txtlang: en ;
# multlin: t ;
# End: ;

