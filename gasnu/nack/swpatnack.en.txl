<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Ralph Nack and Software Patents

#descr: Ralph Nack, a young scholar at the MPI, student of Straus.  Ardent supporter of unlimited patentability, in particular of patents for anything that runs on a computer and can be said to %(q:stand in a tradition of engineering).  This, according to Nack, includes algorithms that allow a computing task to be solved more efficiently and that are also sometimes said to stand in a tradition of purely mental activity.  Nack has written a doctorate thesis, numerous GRUR articles and some AIPPI reports and participated in a government-ordered study on this subject.  Nack insists that text must also be directly patentable because the intellectual achievement and not the form is important.  He also insists that patentability can neither be limited by explicit exclusions as those in Art 52 EPC nor by a theory of %(q:technical invention) as has been built by german courts in the 60/70s.  Instead anything that stands in an %(q:engineering tradition) should be patentable, and it is up to the courts to identify and follow the path of this tradition and shape it according to a legal intuition which is not open to questioning by outsiders.  Some have remarked that Nack sees law as a kind of Disk World.  On the other hand Nack does not agree with the widespread patent lawyer view that the invention is what is claimed and that Art 52 EPC can be dodged by writing appropriate claims.   Instead, Nack agrees with us that %(q:invention) = %(q:technical contribution) = %(q:the novel core of the teaching, which must be technical).

#2ep: Nack 2002: Die Patentierbare Erfindung

#WWg: Doctoral thesis on expanding scope of patentability, sold as a book for 95 eur by Amazon

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatnack ;
# txtlang: en ;
# multlin: t ;
# End: ;

