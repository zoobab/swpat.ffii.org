\begin{subdocument}{swpatnack}{Ralph Nack and Software Patents}{http://swpat.ffii.org/players/nack/index.en.html}{Workgroup\\swpatag@ffii.org}{Ralph Nack, a young scholar at the MPI, student of Straus.  Ardent supporter of unlimited patentability, in particular of patents for anything that runs on a computer and can be said to ``stand in a tradition of engineering''.  This, according to Nack, includes algorithms that allow a computing task to be solved more efficiently and that are also sometimes said to stand in a tradition of purely mental activity.  Nack has written a doctorate thesis, numerous GRUR articles and some AIPPI reports and participated in a government-ordered study on this subject.  Nack insists that text must also be directly patentable because the intellectual achievement and not the form is important.  He also insists that patentability can neither be limited by explicit exclusions as those in Art 52 EPC nor by a theory of ``technical invention'' as has been built by german courts in the 60/70s.  Instead anything that stands in an ``engineering tradition'' should be patentable, and it is up to the courts to identify and follow the path of this tradition and shape it according to a legal intuition which is not open to questioning by outsiders.  Some have remarked that Nack sees law as a kind of Disk World.  On the other hand Nack does not agree with the widespread patent lawyer view that the invention is what is claimed and that Art 52 EPC can be dodged by writing appropriate claims.   Instead, Nack agrees with us that ``invention'' = ``technical contribution'' = ``the novel core of the teaching, which must be technical''.}
nack-patinv02

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Nack 2000: Sind jetzt computer-implementierte Gesch\"{a}ftsmethoden patentierbar?\footnote{http://swpat.ffii.org/papers/grur-nack00/index.de.html}}}

\begin{quote}
In spring 2000, the German Federal Supreme Court (BGH) declared in a spectacular dogmatic u-turn that the technical character of a ``program-technical device'' should be considered indendently of whether that device contributes anything to the state of the art.  Thus it seemed the way to an unlimited patentability of all business methods seemed to be paved.  Nack asks whether this is really true.  He introduces some basic questions, shows inconsistencies of jurisdiction of both the Federal Supreme Court and the EPO (``Dogmatically seen the chaos could not be greater''), makes the written law appear meaningless and demands that software should be patentable, points out the possibility of further surprising dogmatic u-turns and finally demands that the subject should not be only treated at a juridical but also at a political level.  Like in 1877, when Germany decreed its patent law, we are facing a decision of whether to introduce the patent system to a large range of industries such as banking, commerce and services.  ``In 1877 the legislator was courageous and in the end very successful; why shouldn't we show the same courage today?''
\end{quote}
\filbreak

\item
{\bf {\bf US-Urteil CAFC 1998-07-23: Algorithmen und Gesch\"{a}ftsmethoden patentierbar\footnote{http://swpat.ffii.org/papers/grur-nack99/index.de.html}}}

\begin{quote}
Kommentator Nack zur Doktrin des State-Street-Beschlusses, wonach Verfahren jedweder Art grunds\"{a}tzlich strukturell gleich und somit alle gleicherma{\ss}en patentierbar sind: Diese bemerkenswerte Erkenntnis hat leider in Deutschland noch kaum Beachtung gefunden ... Die Auffassung des Court of Appeal steht im Widerspruch zur deutschen Kerntheorie und zur Lehre vom technischen Effekt. ... Der durch diese Kriterien gebotene Spielraum ist im europ\"{a}ischen und deutschen Recht noch lange nicht ausgesch\"{o}pft.
\end{quote}
\filbreak

\item
{\bf {\bf Fraunhofer/MPI 2001: Economic/Legal Study about Software Patents\footnote{http://swpat.ffii.org/papers/bmwi-fhgmpi01/index.en.html}}}

\begin{quote}
In 2001-01, the German Federal Ministery of Economy and Technology (BMWi) ordered a study on the economic effects of software patentability from well known think tanks with close affinity to the German patent establishment:  the Fraunhofer Institute for Innovation Research (ISI.fhg.de), the Fraunhofer Patent Agency (PST.fhg.de) and the Max Planck Institute for Foreign and International Patent, Copyright and Competition Law (MPI = intellecprop.mpg.de).  The study was largely concluded in 2001-06 and preliminary results were presented to a selected audience.  The final report was published by the BMWi on 2001-11-15.  The study is based on an opinion poll answered by several hundred software company representatives and independent software developpers, conducted by Fraunhofer ISI.  Most respondents have had little experience with software patents and don't want software patents to become a daily reality like in the US.  The poll also investigated the significance of open source software for these companies and found it to be of substantial importance as a common infrastructure.  Based on these findings, the Fraunhofer authors predict that an increase in the use of software patents will put many software companies out of business and slow down innovation in the software field.  The study then jumps to conclude that software patents must be legalised and SMEs must be better informed about them.  This surprising conclusion is drawn by the patent law scholars from MPI.  The MPI's legal study does not explore any ways to redraw the borders between patents and copyright but just takes the EPO and USPTO practise as an inevitable reality.  They find that the EPO's caselaw is contradictory and chaotic and blame this on Art 52.2c EPC, which they say has failed to provide clear guidance and should therefore be deleted.  Business related algorithms are, they say, less likely to be patented at the EPO than algorithms that ``stand in a tradition of engineering''.  The MPI writers however do not try to provide a clear rule for distinguishing the two, and they oppose the idea of drawing a line between the physical and the logical (``technical inventions'' vs ``rules of organisation and calculation'') as done by lawcourts in the 70s and 80s, asserting that information is also a physical phenomenon.  They propose that all legislative power concerning the limits of patentability be handed over to the EPO, which should then, at its discretion and as far as Art 27 TRIPS allows, consult experts of interested parties for regular rewriting of its Examination Guidelines.  Art 27 TRIPS demands that patents be ``available in all fields of technology'', and the MPI understands ``technology'' as ``the useful arts'' and is careful not to mention Kolle and other European theoreticians of the concept of technical invention.  Summarily the study can be summarised as ``Fraunhofer: software patents are unpopular in the software industry and dangerous to innovation and competition.  MPI:  Fine, so let's legalise them quickly.''
\end{quote}
\filbreak

\item
{\bf {\bf Patent Lobbyism and Scripture Erudition in the name of Max Planck\footnote{http://swpat.ffii.org/players/mpi/index.de.html}}}

\begin{quote}
Das Max-Planck-Institut f\"{u}r Internationales Patent-, Urheber- und Wettbewerbsrecht in M\"{u}nchen bet\"{a}tigt sich seit jahren als rechtspolitischer Wegbereiter des Privatbesitzes an allen Geisteserzeugnissen, die sich irgendwie beanspruchen und verwerten lassen.  Insbesondere der Lehrstuhl f\"{u}r Gewerbliche Schutzrechte ist eng mit EPA, AIPPI, Gro{\ss}unternehmen und allen Schaltstellen der Patentbewegung in der Bundesregierung, der Europ\"{a}ischen Kommission und bei den Vereinten Nationen verflochten.  Ratschl\"{u}sse des Europ\"{a}ischen Patentamtes und seiner Freunde sind f\"{u}r Prof. Straus und Kollegen ein Heiliger Gral, der \"{u}ber dem Gesetz und \"{u}ber der Wirklichkeit steht.  Die unersch\"{o}pflichen Weisheiten des Patentrechtsprechung gilt es, wie Plancks Kollege Rutherford zu sagen pflegte, briefmarkensammlerisch aufzubereiten, aber niemals im Geiste der Wissenschaft an ihren Wirkungen zu messen.  Es gibt jedoch einen mehr oder weniger gut versteckten Ma{\ss}stab, an dem das MPI seine Schriftauslegungsk\"{u}nste orientiert: die W\"{u}nsche der ``Wirtschaft'', d.h. der Kommilitonen und Freunde aus den Gro{\ss}konzern-Patentabteilungen, denen das Institut direkt oder indirekt sein Wohlergehen verdankt.
\end{quote}
\filbreak

\item
{\bf {\bf Nack 2002: Dissertation on expanding scope of patentability\footnote{http://www.amazon.de/exec/obidos/ASIN/3452252663/qid=1037972127/sr=2-1/ref=sr\_aps\_prod\_1\_1/028-2118006-9361368}}}

\begin{quote}
published as a german paperback available for ordering from Amazon
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
% mode: latex ;
% End: ;

