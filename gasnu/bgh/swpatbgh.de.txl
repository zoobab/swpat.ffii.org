<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Gesetzeswidrige Wirtschaftspolitik des BGH-Patentsenates

#descr: Der BGH-Patentsenat weichte ab 1991 mit wirtschaftspolitischen
Begründungen schrittweise die Ausschlüsse von Softwarepatenten
auf.Regelmäßig verwarf er Entscheidungen des 17. Senats des BPatG, der
die Patente anhand einer einfacheren Auslegungen von §1 PatG (Art 52
EPÜ) abgelehnt hatte.  Der Höhepunkt der Aufweichung und
argumentatorischen Komplexität wurde 2000 mit den Entscheidungen
"Logikverifikation" und "Sprachanalyse" erreicht.  2002 deutete der
Senat an, dass Programmansprüche akzeptabel sein könnten.  Zugleich er
aber nach wegen zurück zu einer strengeren Rechtsprechung, bei der der
die "prägenden Anweisungen" im Bereich der "Technik" liegen müssen,
worunter tendenziell die angewandten Naturwissenschaftenverstanden
werden.  Nach wie vor sind die Urteile schwer zu lesen.

#BWr: Bis etwa 1990 hielt der BGH unter dem vorsitzenden Richter Bruchhausen
am strengen Erfindungsbegriff des PatG/EPÜ fest und gab allenfalls
gelegentlich mit ungewöhnlichen Urteilen wie %(q:Antiblockiersystem)
Denkanstöße zur Vermeidung einer formalistischen Gesetzesauslegung. 
Anfang der 90er Jahre schwenkte der BGH unter dem neuen vorsitzenden
Richter Rogge auf einen gesetzeswidrigen Kurs der zunehmend
grenzenlosen Patentierbarkeit von Organisations- und Rechenregeln ein,
wie ihn mit dem Europäischen Patentamt fast das gesamte deutsche
patentjuristische Schrifttum dieser Zeit forderte.  Rogges Kurs wurde
auch von anderen BGH-Richtern wie Keukenschrijver, Scharen und
Melullis mitgetragen.   Ende der 90er Jahre zeichnete sich jedoch ein
erneutes Umdenken ab.  In wie weit der neue vorsitzende Richter
Melullis, der 2001 sein Amt antrat, versuchen wird, Fehlentwicklungen
der 90er Jahre zu korrigieren, wird sich zeigen.  Die Fraktion der
Gesetzesaufweicher versucht, einer solchen Kurskorrektur durch eine
europäische Richtlinie zuvorzukommen.

#Dod: Der Wirtschaftspolitische Wille des Gesetzgebers

#DtW: Der Wirtschaftspolitische Wille der Patentbewegung

#Wrk: Weitere Lektüre

#DDi: Die geltenden Gesetze (PatG, EPÜ) wurden in den 70er Jahren
geschaffen.  Damals fand in den europäischen Flächenstaaten eine rege
Diskussion über den patentrechtlichen Status von Computerprogrammen
statt.  Es gab zwei Parteien.  Die einen wollten das Patentwesen für
alle wirtschaftslich interessanten automatisierbaren Lösungen bereit
halten.  Die anderen wollten es auf Lösungen beschränkt wissen, die
einen unmittelbaren Einsatz von Naturkräften zur Herbeiführung eines
kausal übersehbaren Erfolges lehren.  Letztere setzten sich in dem
Moment durch, als die Gesetze beschlossen wurden.  In den 80er Jahren
verschob sich jedoch allmählich das Gewicht zugunsten der ersteren.

#Lle: Lange Zeit blieb diese Entwicklung außerhalb patentjuristischer Kreise
unbeachtet.  Im Jahr 2000 drang sie wieder auf die Ebene der Politik
vor.  Führende Politiker alle Fraktionen des Bundestages kritisierten
die expansive Patentierungspraxis.

#Due: Die Patentfachwelt führte immer wirtschaftspolitische Argumente an, um
für eine Änderung der Rechtslage zu plädieren.  Vor allem handelt es
sich dabei um folgende Argumente, die auch der vorsitzende Richter des
BGH-Patentsenates, Dr. Klaus Melullis als Begründung für die
Regeländerungen %(bm:anführt):

#Bwf: Bedeutungszuwachs der Softwarebranche

#DPG: Die Softwarebranche sei so bedeutend geworden, dass ihren
Investitionen der Patentschutz nicht länger versagt werden dürften. 
Die Gesetze müssen an diese Gegebenheiten angepasst werden.  Die
Entscheidung des Gesetzgebers sei falsch gewesen.

#Dln: Diese Argumentation beruht auf der Voraussetzung, dass alles
patentierbar sein müsse, was wirtschaftlich bedeutend ist.  Sie
entspricht zwar dem Interesse und der Ideologie der Patentbewegung,
ist aber ansonsten völlig unbegründet und wurde bereits in den 70er
Jahren %(gk:vom BGH ausdrücklich zurückgewiesen).

#Vod: Urheberrecht schützt unzureichend gegen Nachahmung

#Dga: Diese Argumente wurden nie geprüft.  Eigentlich sind gerade
Computerprogramme im Vergleich zu anderen Informationsgütern relativ
schwer nachahmbar.  Dekompilierung bringt wenig und ist auch in der
Regel verboten.  Das gleiche gilt für Kopieren und Plagiieren.  Einem
geringen Erfindungsaufwand bei den Einzelideen steht ein hoher
Nachahmungsaufwand für das Gesamtwerk gegenüber.  Im BGH sitzen nur
Juristen, die dies offenbar nicht wissen.

#PWe: Patente schützen den kleinen Innovator vor dem großen Monopolisten

#BTe: Bei Zehntausenden von jährlichen Swpat-Anmeldungen ist das graue
Theorie.  Auf diesem Spielfeld profitieren allenfalls diejenigen
Kleinfirmen, die selber keine Programme schreiben und daher nicht auf
Tauschgeschäfte mit Großfirmen angewiesen sind.  D.h.
Patentverwertungsfirmen, nicht Softwarefirmen.

#Eir: Endlich (erst seit 2002) hat auch der BGH seinen Webauftritt.  Bis
dato (2002/03) waren BGH-Urteile dort aber noch nicht abrufbar.  Damit
hinkt der BGH dem BPatG, EPA, DPMA etc weit hinterher.

#AEu: Allgemeine Einführung und Verweise

#BiW: BGH-Softwareexperte Melullis erklärt seine Wirtschaftspolitik und
erläutert, warum diese eine Änderung der Regeln erforderlich macht.

#DAe: Das Bundespatentgericht widerlegt einige der Melullis-Argumente in
einem Urteil vom August 2000, gegen welches IBM wiederum Berufung
einlegte -- beim BGH natürlich.

#Ven: Ein Patentprüfer des DPMA nimmt die Melullis-Argumentation
schonungslos und genüsslich auseinander.

#SnP: Sprachanalyse-Pingpong

#DWh: Der BGH %(gh:erklärt) %(q:programmtechnische Vorrichtungen) für
patentierbar.  Jede geistige Idee muss künftig nur noch als
%(q:Vorrichtung) eingekleidet werden, und schon gibt es ein Patent. 
Im vorliegenden Falle ging es um die %(q:Erfindung), bei der
automatischen Analyse eines Satzes dem Leser die Möglichkeit zu geben,
zwischen mehreren möglichen Treffern auszuwählen und dadurch die
Analyse zu verfeinern.  Das Bundespatentgericht hatte die
%(pg:besseren Argumente), der BGH jedoch die Macht.

#Dha: Der Leiter der Patentabteilung von Siemens erklärt, dass Siemens einen
aktiven Beitrag zur %(q:Rechtsfortbildung) in Sachen
Softwarepatentierung geleistet habe.  Um %(q:konservative) Gerichte
dazu zu bewegen, Softwarepatente anzuerkennen, wurden einige
Musterprozesse vor dem BGH geführt.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatbgh ;
# txtlang: de ;
# multlin: t ;
# End: ;

