\begin{subdocument}{swpatbgh}{Gesetzeswidrige Wirtschaftspolitik des BGH-Patentsenates}{http://swpat.ffii.org/gasnu/bgh/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2004/12/12 von FFII\footnote{\url{http://lists.ffii.org/mailman/listinfo/traduk}}}{Der BGH-Patentsenat weichte ab 1991 mit wirtschaftspolitischen Begr\"{u}ndungen schrittweise die Ausschl\"{u}sse von Softwarepatenten auf.Regelm\"{a}{\ss}ig verwarf er Entscheidungen des 17. Senats des BPatG, der die Patente anhand einer einfacheren Auslegungen von \S{}1 PatG (Art 52 EP\"{U}) abgelehnt hatte.  Der H\"{o}hepunkt der Aufweichung und argumentatorischen Komplexit\"{a}t wurde 2000 mit den Entscheidungen ``Logikverifikation`` und ``Sprachanalyse`` erreicht.  2002 deutete der Senat an, dass Programmanspr\"{u}che akzeptabel sein k\"{o}nnten.  Zugleich er aber nach wegen zur\"{u}ck zu einer strengeren Rechtsprechung, bei der der die ``pr\"{a}genden Anweisungen`` im Bereich der ``Technik`` liegen m\"{u}ssen, worunter tendenziell die angewandten Naturwissenschaftenverstanden werden.  Nach wie vor sind die Urteile schwer zu lesen.}
\begin{center}
\parbox[c]{.4\textwidth}{\centerline{\mbox{\includegraphics{bgh}}}}
\hfill
\parbox[c]{.5\textwidth}{Bis etwa 1990 hielt der BGH unter dem vorsitzenden Richter Bruchhausen am strengen Erfindungsbegriff des PatG/EP\"{U} fest und gab allenfalls gelegentlich mit ungew\"{o}hnlichen Urteilen wie ``Antiblockiersystem'' Denkanst\"{o}{\ss}e zur Vermeidung einer formalistischen Gesetzesauslegung.  Anfang der 90er Jahre schwenkte der BGH unter dem neuen vorsitzenden Richter Rogge auf einen gesetzeswidrigen Kurs der zunehmend grenzenlosen Patentierbarkeit von Organisations- und Rechenregeln ein, wie ihn mit dem Europ\"{a}ischen Patentamt fast das gesamte deutsche patentjuristische Schrifttum dieser Zeit forderte.  Rogges Kurs wurde auch von anderen BGH-Richtern wie Keukenschrijver, Scharen und Melullis mitgetragen.   Ende der 90er Jahre zeichnete sich jedoch ein erneutes Umdenken ab.  In wie weit der neue vorsitzende Richter Melullis, der 2001 sein Amt antrat, versuchen wird, Fehlentwicklungen der 90er Jahre zu korrigieren, wird sich zeigen.  Die Fraktion der Gesetzesaufweicher versucht, einer solchen Kurskorrektur durch eine europ\"{a}ische Richtlinie zuvorzukommen.}
\end{center}

\begin{sect}{bundpol}{Der Wirtschaftspolitische Wille des Gesetzgebers}
Die geltenden Gesetze (PatG, EP\"{U}) wurden in den 70er Jahren geschaffen.  Damals fand in den europ\"{a}ischen Fl\"{a}chenstaaten eine rege Diskussion \"{u}ber den patentrechtlichen Status von Computerprogrammen statt.  Es gab zwei Parteien.  Die einen wollten das Patentwesen f\"{u}r alle wirtschaftslich interessanten automatisierbaren L\"{o}sungen bereit halten.  Die anderen wollten es auf L\"{o}sungen beschr\"{a}nkt wissen, die einen unmittelbaren Einsatz von Naturkr\"{a}ften zur Herbeif\"{u}hrung eines kausal \"{u}bersehbaren Erfolges lehren.  Letztere setzten sich in dem Moment durch, als die Gesetze beschlossen wurden.  In den 80er Jahren verschob sich jedoch allm\"{a}hlich das Gewicht zugunsten der ersteren.

Lange Zeit blieb diese Entwicklung au{\ss}erhalb patentjuristischer Kreise unbeachtet.  Im Jahr 2000 drang sie wieder auf die Ebene der Politik vor.  F\"{u}hrende Politiker alle Fraktionen des Bundestages kritisierten die expansive Patentierungspraxis.
\end{sect}

\begin{sect}{bghwpol}{Der Wirtschaftspolitische Wille der Patentbewegung}
Die Patentfachwelt f\"{u}hrte immer wirtschaftspolitische Argumente an, um f\"{u}r eine \"{A}nderung der Rechtslage zu pl\"{a}dieren.  Vor allem handelt es sich dabei um folgende Argumente, die auch der vorsitzende Richter des BGH-Patentsenates, Dr. Klaus Melullis als Begr\"{u}ndung f\"{u}r die Regel\"{a}nderungen anf\"{u}hrt\footnote{\url{http://localhost/swpat/papri/grur-mellu98/index.de.html}}:

\begin{description}
\item[Bedeutungszuwachs der Softwarebranche:]\ Die Softwarebranche sei so bedeutend geworden, dass ihren Investitionen der Patentschutz nicht l\"{a}nger versagt werden d\"{u}rften.  Die Gesetze m\"{u}ssen an diese Gegebenheiten angepasst werden.  Die Entscheidung des Gesetzgebers sei falsch gewesen.
Diese Argumentation beruht auf der Voraussetzung, dass alles patentierbar sein m\"{u}sse, was wirtschaftlich bedeutend ist.  Sie entspricht zwar dem Interesse und der Ideologie der Patentbewegung, ist aber ansonsten v\"{o}llig unbegr\"{u}ndet und wurde bereits in den 70er Jahren vom BGH ausdr\"{u}cklich zur\"{u}ckgewiesen\footnote{\url{http://localhost/swpat/papri/bgh-dispo76/index.de.html}}.
\item[Urheberrecht sch\"{u}tzt unzureichend gegen Nachahmung:]\ Diese Argumente wurden nie gepr\"{u}ft.  Eigentlich sind gerade Computerprogramme im Vergleich zu anderen Informationsg\"{u}tern relativ schwer nachahmbar.  Dekompilierung bringt wenig und ist auch in der Regel verboten.  Das gleiche gilt f\"{u}r Kopieren und Plagiieren.  Einem geringen Erfindungsaufwand bei den Einzelideen steht ein hoher Nachahmungsaufwand f\"{u}r das Gesamtwerk gegen\"{u}ber.  Im BGH sitzen nur Juristen, die dies offenbar nicht wissen.
\item[Patente sch\"{u}tzen den kleinen Innovator vor dem gro{\ss}en Monopolisten:]\ Bei Zehntausenden von j\"{a}hrlichen Swpat-Anmeldungen ist das graue Theorie.  Auf diesem Spielfeld profitieren allenfalls diejenigen Kleinfirmen, die selber keine Programme schreiben und daher nicht auf Tauschgesch\"{a}fte mit Gro{\ss}firmen angewiesen sind.  D.h. Patentverwertungsfirmen, nicht Softwarefirmen.
\end{description}
\end{sect}

\begin{sect}{ref}{Weitere Lekt\"{u}re}
\begin{itemize}
\item
{\bf {\bf \url{http://www.bundesgerichthof.de/}}}

\begin{quote}
Endlich (erst seit 2002) hat auch der BGH seinen Webauftritt.  Bis dato (2002/03) waren BGH-Urteile dort aber noch nicht abrufbar.  Damit hinkt der BGH dem BPatG, EPA, DPMA etc weit hinterher.
\end{quote}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis für die Demontage des Technikbegriffs\footnote{\url{http://localhost/swpat/stidi/korcu/index.de.html}}}}

\begin{quote}
Allgemeine Einf\"{u}hrung und Verweise
\end{quote}
\filbreak

\item
{\bf {\bf Mellulis (BGH) 1998: Zur Patentierbarkeit von Programmen fuer DV-Anlagen\footnote{\url{http://localhost/swpat/papri/grur-mellu98/index.de.html}}}}

\begin{quote}
BGH-Softwareexperte Melullis erkl\"{a}rt seine Wirtschaftspolitik und erl\"{a}utert, warum diese eine \"{A}nderung der Regeln erforderlich macht.
\end{quote}
\filbreak

\item
{\bf {\bf \url{http://localhost/swpat/papri/bpatg17-suche00/index.de.html}}}

\begin{quote}
Das Bundespatentgericht widerlegt einige der Melullis-Argumente in einem Urteil vom August 2000, gegen welches IBM wiederum Berufung einlegte -- beim BGH nat\"{u}rlich.
\end{quote}
\filbreak

\item
{\bf {\bf \url{}}}

\begin{quote}
Ein Patentpr\"{u}fer des DPMA nimmt die Melullis-Argumentation schonungslos und gen\"{u}sslich auseinander.
\end{quote}
\filbreak

\item
{\bf {\bf Sprachanalyse-Pingpong\footnote{\url{http://www.ffii.org/archive/mails/swpat/2000/Dec/0209.html}}}}

\begin{quote}
Der BGH erkl\"{a}rt ``programmtechnische Vorrichtungen'' f\"{u}r patentierbar.  Jede geistige Idee muss k\"{u}nftig nur noch als ``Vorrichtung'' eingekleidet werden, und schon gibt es ein Patent.  Im vorliegenden Falle ging es um die ``Erfindung'', bei der automatischen Analyse eines Satzes dem Leser die M\"{o}glichkeit zu geben, zwischen mehreren m\"{o}glichen Treffern auszuw\"{a}hlen und dadurch die Analyse zu verfeinern.  Das Bundespatentgericht hatte die besseren Argumente, der BGH jedoch die Macht.
\end{quote}
\filbreak

\item
{\bf {\bf Arno Körber: Patentschutz aus der Sicht eines Großunternehmens\footnote{\url{http://localhost/swpat/papri/boch97-koerber/index.de.html}}}}

\begin{quote}
Der Leiter der Patentabteilung von Siemens erkl\"{a}rt, dass Siemens einen aktiven Beitrag zur ``Rechtsfortbildung'' in Sachen Softwarepatentierung geleistet habe.  Um ``konservative'' Gerichte dazu zu bewegen, Softwarepatente anzuerkennen, wurden einige Musterprozesse vor dem BGH gef\"{u}hrt.
\end{quote}
\filbreak

\item
{\bf {\bf (DPMA 2002-03-12: Patentrekord - Positives Signal)\footnote{\url{http://www.dpma.de/infos/pressedienst/pm020312a.html}}}}

\begin{quote}
Wachstum der Anmeldezahlen am Deutschen Patent- und Markenamt (DPMA) um 15\percent{}, Einbruch am neuen Markt, nat\"{u}rlich alles ein Spiegel der lebendigen Innovation und nicht etwa der Patentinflation.  Nebenbei wird Swpat-Chefideologe Tauchert mit einer Aussage zur neuen Politik des DPMA hinsichtlich Software und Gesch\"{a}ftsmethoden zitiert.  Tauchert k\"{u}ndigt an, dass das Patentamt die neueste BGH-Entscheidung (Zeichensuche 2001) umsetzen und somit unmittelbare Anspr\"{u}che auf Programme zulassen werde.  Damit prescht das DPMA in einem Punkt vor, in dem sogar der neue Br\"{u}sseler Richtlinienvorschlag Zur\"{u}ckhaltung empfiehlt.
\end{quote}
\filbreak

\item
{\bf {\bf BGH Urheberrechtssenat 1990-10-04: Entscheidung %(q:Betriebssystem)\footnote{\url{http://localhost/swpat/papri/bgh1-bs90/index.de.html}}}}

\begin{quote}
Entscheidung des 1. Zivilsenats des BGH zum Urheberrechtsschutz von Datenverarbeitungsprogrammen und zur Abgrenzung von Urheber- und Patentrecht: ein Betriebssystem ist kein technisches Datenverarbeitungsprogramm im Sinne des Patentrechts, da es sich in Rechenanweisungen zum bestimmungsgemd\_en Gebrauch eines Rechners samt seiner Peripherie erschvpft.  Die Programmierleistung verdient jedoch urheberrechtlichen Schutz.  Selbst neuere EPA-Entscheidungen seit 1986 sind, zumindest nach Ansicht des EPA, nicht auf Programme als solche sondern auf darin verkvrperte technische Verfahren gerichtet.  Ein Betriebssystem ist urheberrechtlich gesch|tzt, weil es eigent|mliche Leistungen aufweist, die nicht technisch bedingt sind sondern auf gestalterischen Elementen beruhen.  Gegenstand des Schutzes ist weder eine (nicht vorhandene) technische Erfindung noch eine (nicht sch|tzbare) Idee oder Rechenregel, sondern das ``Gewebe'', d.h. die von gestalterischer Fdhigkeit und Vorstellungskraft geprdgte Art der Implementierung und Zuordnung.  Dieses Gewebe weist bei komplexen Programmen regelmd\_ig eine ausreichende Gestaltungshvhe auf.  Die Darlegungslast fdllt auf denjenigen, welcher die Gestaltungshvhe abstreiten mvchte.
\end{quote}
\filbreak

\item
{\bf {\bf BGH 1976-06-22: Dispositionsprogramm\footnote{\url{http://localhost/swpat/papri/bgh-dispo76/index.de.html}}}}

\begin{quote}
Eine Grundsatzentscheidung des BGH von 1976, die Ergebnisse einer 20j\"{a}hrige Diskussion um die Patentierbarkeit von Computerprogrammen zusammenfasst.  Der Leitsatz lautet: {\bf Organisations- und Rechenprogramme f\"{u}r elektronische Datenverarbeitungsanlagen zur L\"{o}sung von betrieblichen Dispositionsaufgaben, bei deren Anwendung lediglich von einer in Aufbau und Konstruktion bekannten Datenverarbeitungsanlage der bestimmungsgem\"{a}{\ss}e Gebrauch gemacht wird, sind nicht patentf\"{a}hig}.   Der Dispositionsprogramm-Beschluss ist der erste und wegen seiner allgemeinen Betrachtungen meistzitierte einer Reihe von Beschl\"{u}ssen des BGH-Patentsenats, in denen die Nichtpatentierbarkeit von Organisations- und Rechenregeln, Programmen f\"{u}r Datenverarbeitungsanlagen sowie immateriellen Verfahren (Algorithmen) im allgemeinen erl\"{a}utert und eine Methodologie f\"{u}r die Unterscheidung zwischen technischen Erfindungen und anderen nicht-patentierbaren Geistesleistungen herausgearbeitet wird.  In den Schlussbetrachtungen wird prophetisch warnend erkl\"{a}rt, dass das Patentrecht ein spezielles Werkzeug zur Belohnung von Neuerungen in einem \"{u}berschaubar begrenzten Bereich ist, n\"{a}mlich des Probleml\"{o}sens durch unmittelbaren Einsatz von Naturkr\"{a}ften, und dass eine Ausweitung dieses Bereiches in mehrfacher Hinsicht unverantwortbare Folgen nach sich ziehen w\"{u}rde.
\end{quote}
\filbreak

\item
{\bf {\bf BGH 1980-09-16: Beschluss Walzstabteilung\footnote{\url{http://localhost/swpat/papri/bgh-walzst80/index.de.html}}}}

\begin{quote}
Leitsatz: {\bf Rechenprogramme f\"{u}r elektronische Datenverarbeitungsanlagen, bei deren Anwendung lediglich von einer in Aufbau und Konstruktion bekannten Datenverarbeitungsanlage der bestimmungsgem\"{a}{\ss}e Gebrauch gemacht wird, sind auch dann nicht patentf\"{a}hig, wenn mit Hilfe der Datenverarbeitungsanlage ein Herstellungs- oder Bearbeitungsvorgang mit bekannten Steuerungsmitteln unmittelbar beeinflusst wird}.  Beschluss des 10. Zivilsenats des BGH in einer Rechtsbeschwerdesache.
\end{quote}
\filbreak

\item
{\bf {\bf BGH-Urteil %(q:Antiblockiersystem) von 1980 bremst %(q:Hexenjagd gegen alles Untechnische)\footnote{\url{http://localhost/swpat/papri/bgh-abs80/index.de.html}}}}

\begin{quote}
Der BGH nimmt in dieser Entscheidung ein techniknahes Steuerungslogikpatent, welches das BPatG zuvor wegen mangelnder Technizit\"{a}t zur\"{u}ckgewiesen hatte, zum Anlass, den W\"{u}nschen der Patentjuristengemeinde entgegenzukommen und die Absage des Dispositionsprogramm-Urteils von 1976 an jegliche Softwarepatentierung ein wenig aufzulockern.  Dabei beschlie{\ss}t der BGH, dass neuartige Verfahren zum unmittelbaren Erreichung eines Erfolgs durch Einsatz beherrschbarer Naturkr\"{a}fte, in diesem Falle eine Bremsregel, auch dann patentierbar sein k\"{o}nnen, wenn ihre Ausf\"{u}hrung nicht mehr als den Einsatz eines Computerprogramms auf einer bekannten Anlage erfordert und wenn keine konkreten technischen Mittel sondern nur die erreichten Wirkungen beansprucht werden.  Entscheidend ist laut BGH, dass der beanspruchte Vorgang von Naturkr\"{a}ften abh\"{a}ngt und nicht durch Logik determiniert ist.  Der GRUR-Kommentator, PA Eisenf\"{u}hr, applaudiert und meint, ein Aufatmen sei durch die Reihen der Patentjuristen gegangen.  Er kritisiert die von BPatG betriebene 'Hexenjagd gegen alles Untechnische' und prophezeit richtig, die offizielle Anerkennung von Wirkungsanspr\"{u}chen durch den BGH berge ein gro{\ss}es Potential zur k\"{u}nftigen Ausweitung der Patentierbarkeit und werde daher immer wieder zitiert werden.
\end{quote}
\filbreak

\item
{\bf {\bf BGH Logikverifikation 1999-12-13\footnote{\url{http://localhost/swpat/papri/bgh-logik99/index.de.html}}}}

\begin{quote}
Eine von zwei revolutiondren Entscheidungen, mit denen der BGH-Patentsenat den Begriff der Technischen Erfindung lockert und Weichen f|r eine grenzenlose Patentierbarkeit aller Ideen, die als ``programmtechnische Vorrichtungen'' beschrieben werden kvnnen, stellt.  U.a. hei\_t es: \begin{quote}
{\it Betrifft der Lvsungsvorschlag einen Zwischenschritt im Proze\_, der mit der Herstellung von (Silicium-}
\end{quote}Chips endet, so kann er vom Patentschutz nicht deshalb ausgenommen sein, weil er auf den unmittelbaren Einsatz von beherrschbaren Naturkrdften verzichtet und die Mvglichkeit der Fertigung tauglicher Erzeugnisse anderweitig durch auf technischen erlegungen beruhende Erkenntnisse voranzubringen sucht.
\end{quote}
\filbreak

\item
{\bf {\bf BGH 2000-05-11: Sprachanalyseeinrichtung\footnote{\url{http://localhost/swpat/papri/bgh-sprach00/index.de.html}}}}

\begin{quote}
Der BGH beschlie\_t, dass alles, was auf einem Rechner lduft, patentierbar ist:  ``Einer Vorrichtung (Datenverarbeitungsanlage'', die in bestimmter Weise programmtechnisch eingerichtet ist, kommt technischer Charakter zu.  ... F|r die Beurteilung des technischen Charakters einer solchen Vorrichtung kommt es nicht darauf an, ob mit ihr ein (weiterer) technischer Effekt erzielt wird, ob die Technik durch sie bereichert wird oder ob sie einen Beitrag zum Stand der Technik leistet.
\end{quote}
\filbreak

\item
{\bf {\bf BGH 2001: Computerisierung darf Untechnische Lehren nicht Patentierbar machen\footnote{\url{http://localhost/swpat/papri/bgh-suche01/index.de.html}}}}

\begin{quote}
Beschluss des 10. Senates des Bundesgerichtshofes (BGH/10) in der Rechtsbeschwerdesache betr die Patentanmeldung P 4323241.8-53 (IBM: Suche fehlerhafter Zeichenketten in einem Text) vom Oktober 2000.  Der 17. Senat des Bundespatentgerichtes (BPatG/17) hatte im August 2000 erkldrt, dass ein Computerprogramm nicht beansprucht werden kann, und dass ein ``Computerprogramm als solches'' nichts anderes als ein Computerprogramm in beliebiger Entwurfsstufe ist, und dabei diverse Lehrsdtze des BGH und EPA analysiert und zur|ckgewiesen.  BGH/10 hebt den BPatG/17-Beschluss auf und deutet an, dass Programme mvglicherweise beansprucht werden kvnnten, wenn der Gegenstand des Hauptanspruchs sich als eine Erfindung gemd\_ g1 PatG erweist, deutet aber an, dass dies im vorliegenden Falle nicht gegeben sein d|rfte und verweist den Fall zum BPatG zur erneuten Pr|fung zur|ck.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

