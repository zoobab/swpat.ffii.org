<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Freie Demokratische Partei (FDP.de) und Logikpatente

#descr: Am 25. Mai 2004 stellte die FDP einen Entschließungsantrag im
Bundestag, der sehr klar gegen die Patentierbarkeit von Software und
für die Position des Euorpäischen Parlamentes in dieser Frage Stellung
nimmt.  Die FDP ist neben den Grünen die einzige Partei, die zum Thema
Softwarepatente eine Position beschlossen hat.  Während sich der
Düsseldorfer Beschluss von 2000 noch als Formelkompromiss deuten
lässt, fand die FDP im September 2000 und im Frühjahr 2004
unmissverständliche Worte gegen die Patentierung von Software. 
Seitdem haben einige Regionalverbände ähnliche Resolutionen
verabschiedet.  Dennoch ist auch für die meisten FDP-Politiker das
Patentwesen ein Buch mit sieben Siegeln, und die Bewusstseinsbildung
steht erst ganz am Anfang.

#Dre: Der Düsseldorfer Beschluss lehnt sich an die vorigen Erklärungen der
Abgeordneten Brüderle und Otto an, die sich im Herbst 2000 gegen die
Streichung der EPÜ-Bestimmung, wonach %(q:Programme für
Datenverarbeitungsanlagen) nicht zu den patentfähigen Erfindungen
gehören, ausgesprochen hatten.  Im letzten Absatz werden jedoch
doppelbödige Begriffe aus der Sprache des Europäischen Patentamts
übernommen, welche es erlauben dürften, den Beschluss bei Bedarf zu
verwässern.

#Wnt: Der medienpolitische Sprecher MdB Hans-Joachim Otto zeigt Mitte 2001
zeitweilig Tendenzen, den Wünschen der Patentlobby entgegenzukommen
und nur noch die %(q:Patentierung von Geschäftsmodellen) abzulehnen. 
Mit den Bestrebungen einiger Bundestagsabgeordneter, im Bundestag
GNU/Linux einzuführen, konnte sich Otto nicht so recht anfreunden. 
Auf einigen von Microsofts PR-Berater Hunzinger organisierten
parlamentarischen Abenden trat Otto als Ehrengast auf.  Während des
Wahlkampfes 2002 nahm er in der Frage der Logikpatente eine
unentschiedene Haltung ein.  Dies änderte sich nach dem Beschluss des
Europäischen Parlaments.  Im März 2004 informierte sich das Büro Otto
intensiv über die Fragen und erarbeitete zusammen mit
FDP-Arbeitsgruppen Positionen, die denen des FFII sehr nahe stehen. 
Dies hatte u.a. mit fortgeschrittener Meinungsbildung in FDP-nahen
Freiberufler- und Mittelstandsvereinigungen und entsprechenden
FDP-Arbeitskreisen zu tun.

#ZrW: Zu den Mitgliedern der FDP gehören der derzeitige Präsident des
Europäischen Patentamtes (EPA) Ingo Kober und weitere EPA-Funktionäre.
 Auch Sabine Leutheuser-Schnarrenberger bekleidete zeitweilig führende
Rollen im EPA. Auch das Patentanwaltsmilieu steht der FDP nicht fern. 
Andererseits arbeiten hundertausende von Programmierern in kleinen
Wirtschaftseinheiten, die ebenfalls zur Zielkundschaft der FDP
gehören.  Mit dem Wort %(q:Individualsoftware) in der Erklärung soll
dies vielleicht angedeutet werden.  Sonst wäre die Erwähnung von
Individualsoftware kaum zu erklären, denn die Patentgefahr beschränkt
sich weder auf diesen Sektor noch trifft sie ihn am stärksten.  Man
kann ahnen, dass es auf dem Parteitag einen Richtungsstreit gegeben
haben muss.

#Fsm: FDP Parteitagsbeschlüsse 2001/05/04-6 Düsseldorf

#UWi: Unter dem Titel %(q:Standort zukunft.de) trifft diese Resolution
allerlei Aussagen zur digitalen Ökonomie.  Zu Softwarepatenten findet
sich folgendes: %(bc|Software gehört zu den Schlüsseltechnologien von
Gegenwart und Zukunft. Die Innovationsfähigkeit der Softwareindustrie
ist künftig mitentscheidend für das Schicksal der Volkswirtschaften.
Wir brauchen daher rechtliche und gesamtwirtschaftliche
Rahmenbedingungen, die die hohen Investitionen in Software sichern und
eine dynamische Weiterentwicklung nicht behindern.|Software genießt
heute weltweit umfassenden Schutz durch das Urheberrecht. Damit ist
den Herstellern und Programmierern ein starkes absolutes Recht
verliehen, um ihre Interessen umfassend gegenüber Dritten wahrnehmen
zu können. Indes kennt die US-amerikanische Rechtsordnung auch die
Patentierbarkeit von Software. Im Gegensatz zum Urheberrecht schützt
das Patent jedoch nicht das fertige Produkt, sondern dehnt den Schutz
auf die Methode oder gar ein softwarebasierendes Geschäftsmodell aus.
Die Entwicklung in den USA zeigt schon heute deutlich, dass die
Patentierung von Software sich negativ auf die Entwicklung neuer
Produkte und Geschäftsmodelle auswirken kann. Denn einzelne
Softwarepatente können im Bereich der sogenannten Individualsoftware
ganze Märkte blockieren.|Sowohl nach deutschem Patentrecht als auch
nach dem Europäischen Patentübereinkommen sind Computerprogramme als
solche derzeit nicht patentierbar. Die FDP spricht sich dafür aus, an
dieser Rechtslage im Grundsatz festzuhalten. Angesichts der
unterschiedlichen Praxis in den EU-Mitgliedsstaaten begrüßt es die
FDP, dass die Europäische Kommission jetzt die Initiative ergriffen
hat, um einen einheitlichen europäischen Ansatz zur Frage der
Softwarepatente zu entwickeln. Ein solcher Ansatz im Rahmen der EU ist
einer Novellierung des Europäischen Patentübereinkommens eindeutig
vorzuziehen, denn er schafft supranationales Recht und damit
Rechtssicherheit und Rechtsklarheit für Unternehmen im gesamten
Europäischen Binnenmarkt. Inhaltlich sollte eine EU-Richtlinie zur
Patentierung von Software die europäische Softwareindustrie durch
einen liberalen, wettbewerbsorientierten Ansatz in ihrer
Innovationsfähigkeit bestärken.)

#Rgc: Rainer Brüderle (MdB FDP) gegen Swpat: Softwareschmieden nicht
gefährden

#Eir: Erklärung des Wirtschaftssprechers der Freien Demokraten vor der
Diplomatischen Konferenz im Herbst 2000

#Ttr: Textversion

#Ifr: Im Sitzungsprotokoll finden sich Zwischenfragen u.a. von MdB Otto

#Dln: Der EPA-Präsident, der sich seit 1995 (und wohl schon davor im BMJ)
besonders um die Patentinflation verdient gemacht hat, ist
FDP-Mitglied.

#Iie: In diesem Amt sitzen noch weitere FDP-Politiker

#UdK: Erarbeitung von Positionen zum EU/BSA-Richtlinienentwurf fordern

#Ddz: Die größte Gefahr ist, dass alle das Thema schlorren lassen und
warten, bis die EU-RiLi nur noch in nationales Recht umgesetzt werden
kann maW das Kind schon in den Brunnen gefallen ist.

#VFn: Vorhandene FDP-Positionen sammeln

#Gne: Grundsatzprogramm und ähnliche Dokumente lesen, einschlägiges
heraussuchen

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatfdp ;
# txtlang: de ;
# multlin: t ;
# End: ;

