<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: SAP has been spending billions of DEM every year for research and development.  They have applied for no patents and even published the sourcecode of most of their software.  Like Microsoft, they have become one of the largest most profitable software companies of the world without software patents.  Only in 1999 did SAP start with its own patenting activities.  This was in reaction to litigation threats in the USA.  Since then the head of their new patent department, Dr. Hagedorn, has managed to install a patent committee and patent information system and an incentive system for motivating programmers to assist the patent committee.  As a result, SAP obtained four software patents by April 2000.  Although this lets them appear as losers of the software patent race, their patent department is a loyal member of the patent movement and through Mr. Hagedorn's speeches it has occasionally joined the choir of those calling for the legalisation of software patents in Europe.  However SAP board members have declared in the German Parliament that SAP needs software patents not as an innovation incentive but only as a weapon against other patents which have created problems for SAP in the US.
title: Software Patents and SAP
Wth: List of SAP Patents at the EPO
rhc: Compare this to the lists of %(LST).
nal: At the Europarl Hearing on Software Patentability Mr. Hagedorn gave a speech in which he claimed that no industry cannot thrive without patents and that the European Commission does not go far enough in legalising software patents.  In particular, claims to program texts are essential for the survival of Mr. Hagedorn's industry.  Which industry?  %(q:Mr. Hagedorn is not a software salesman but a patent salesman!), the following speaker exclaimed.  At this hearing, several SAP people were present, including permanent Brussels representatives and board consultants.  So it must be assumed that Mr. Hagedorn's position has at least some support at SAP.  Possibly SAP is scared to death of growing SME competition to its monopoly position and is willing to engage in any devil's pact, even if SAP is the biggest loser.
oaq: Hagedorn was one of the 7 megacorp patent lawyers who dictated the position of Bitkom on the swpat question in summer 2002.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatsap ;
# txtlang: ca ;
# End: ;

