<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Softwarepatente und SAP

#descr: SAP gibt seit Jahrzehnten jährlich Milliarden für Forschung und
Entwicklung aus.  Erst 1999 begann SAP mit dem Aufbau eines
Patentportfolios.  Bis März 2001 besaß SAP 4 Softwarepatente und hatte
von dieser Entwicklung nur schlimmstes zu befürchten.  Dennoch setzt
sich die Patentabteilung von SAP im Namen der Firma für die
Patentierbarkeit von Software in Europa ein.  Allerdings haben
SAP-Vorstandsmitglieder im Deutschen Bundestag erklärt, dass SAP
Patente nicht als Innovationsanreiz oder Investitionsschutz braucht
sondern vor allem als Waffe gegen andere Patente in einer Umgebung wie
den USA, wo Softwarepatente etabliert sind und zum Tagesgeschäft
gehören.

#Wth: List of SAP Patents at the EPO

#rhc: Compare this to the lists of %(LST).

#nal: At the Europarl Hearing on Software Patentability Mr. Hagedorn gave a
speech in which he claimed that no industry cannot thrive without
patents and that the European Commission does not go far enough in
legalising software patents.  In particular, claims to program texts
are essential for the survival of Mr. Hagedorn's industry.  Which
industry?  %(q:Mr. Hagedorn is not a software salesman but a patent
salesman!), the following speaker exclaimed.  At this hearing, several
SAP people were present, including permanent Brussels representatives
and board consultants.  So it must be assumed that Mr. Hagedorn's
position has at least some support at SAP.  Possibly SAP is scared to
death of growing SME competition to its monopoly position and is
willing to engage in any devil's pact, even if SAP is the biggest
loser.

#oaq: Hagedorn was one of the 7 megacorp patent lawyers who dictated the
position of Bitkom on the swpat question in summer 2002.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatsap ;
# txtlang: de ;
# multlin: t ;
# End: ;

