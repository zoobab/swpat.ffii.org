<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Der Zentralverband der Elektronischen Industrie (ZVEI) fungierte lange Zeit als Sprachrohr der Patentabteilung von Siemens.  Seine Stellungnahmen wurden z.T. direkt von deren Chef Arno Körber formuliert und allenfalls mit einer Arbeitsgruppe der Patentjuristen, niemals aber mit Ingenieuren oder Software-Entwicklern besprochen.  Der ZVEI forderte dabei eine Neuregulierung der Patentierbarkeit nach US-Vorbild oder gemäß der dem weitgehend entsprechenden neuesten Praxis des Europäischen Patentamtes (EPA).  Auch nach der Veröffentlichung des Swpat-Richtlinienentwurfs der Europäischen Kommission forderte der ZVEI eine noch vorbehaltlosere Bejahung der grenzenlosen Patentierbarkeit als in diesem Entwurf vorgesehen war.  Nachdem im Frühjahr 2002 einige Mitglieder in nicht-juristischen Arbeitskreisen Kritik an der patentlastigen Verbandsposition geäußert hatten, begann der ZVEI, seine Position neu zu überprüfen.  Die Fachpresse sprach von %(q:Zurückrudern).
title: ZVEI und Softwarepatente
Zuz: ZVEI rudert zurück
scD: s. auch anschließende Diskussion.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatzvei ;
# txtlang: de ;
# End: ;

