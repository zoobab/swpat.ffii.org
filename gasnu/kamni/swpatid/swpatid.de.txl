<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: India and Software Patents

#descr: India has for a long time resisted the calls of patent inflation. 
Programs for computers are excluded from patentability in Indian law. 
The current examination guidelines of the Indian Patent Office on
software sound similar to the approach which the European Parliament
adopted on 2003-09-24.  However due to international pressure and a
growing domestic patent movement, the limits of patentability have
been eroding.

#iWr: %(orig|2. Since the claims may be couched in terms which tend to
obscure the fact that the invention relates to a computer program, it
is always essential to analyse them, in the light of what is described
and of the prior art, in order to identify the contribution to the art
and hence determine whether this advance resides in, or necessarily
includes, technological features, or is solely intellectual in its
content.|For example, if the new feature comprises a set of
%(tp|instructions|program), which may be formulated and presented in
any one of a variety of ways, designed to control a known computer to
cause it to perform desired operations, the computer being suitable
for the purpose without special adoption or modification of its
hardware or organization then, no matter whether claimed as a computer
arranged to operate etc or as a method of operating a computer etc 
.Such a subject matter is not patentable and hence excluded from
patentability.|3. If the implementation of a new program requires
internal modification to a computer of such a nature that it may
reasonably be regarded as a new computer then clearly a claim to this
computer is not excluded, even though at first sight the invention may
seem to relate merely to a program and the purpose of modifying the
computer is subsidiary to this. The modification must however be
inventive itself; if a computer is modified in a manner which is the
obvious way of implementing the program, then the inventive
contribution will still reside solely in the program itself.)

#tho: According to this article, some Indian patent lawyers are playing the
%(q:technical features vs software per se) trick and successfully
obtaining software patents.

#OWw: NASCOMM - IPR: Time to own it!

#nWf: An Indian version of BSA advertising patent inflation, using the term
%(q:IPR) to create confusion with copyright.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: ffii ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatid ;
# txtlang: de ;
# multlin: t ;
# End: ;

