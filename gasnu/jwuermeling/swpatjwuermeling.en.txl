<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Dr. Joachim Wuermeling MEP and Software Patents

#descr: Joachim Wuermeling, doctor of law, member of the European Parliament (MEP) for Bavaria's Christian Social Union (CSU), shadow rapporteur of the European People's Party (EPP) on the software patent directive, opposed everyting that could somehow limit patentability (e.g. interoperability privilege) and advocated everything that extends it beyond the European Commission's wishes (e.g. program claims).  Most EPP colleagues tend to blindly follow their shadow rapporteur.  Many critics have in vain sought dialogue with Wuermeling.  Wuermeling has however actively contacted the press in order to present himself as an opponent of %(q:patents on pure software and business methods) and to ascribe contrary impressions of the public to %(q:irrational fears of the opensource lobby).  Wuermeling may believe what he says.  In the JURI discussions of May 2003, Wuermeling tabled a pseudo-restrictive amendment (recital 13d), according to which a patent claim is limited to a specific product an may not encompass underlying algorithms.  Some corporate patent lawyers from Wuermeling's clientele have been laughing about Wuermeling's lack of basic knowledge about patent law.  Wuermeling accuses the European Parliament of having bowed to these %(q:irrational fears) in September 2003 and hails the Council decision of May 2004 as a %(q:positive signal for innovation in Europe).

#aWa: Dr. Joachim Wuermeling MEP and Software Patents

#auh: Joachim Wuermeling, doctor of law, member of the European Parliament (MEP) for Bavaria's Christian Social Union (CSU), shadow rapporteur of the European People's Party (EPP) on the software patent directive, opposed everyting that could somehow limit patentability (e.g. interoperability privilege) and advocated everything that extends it beyond the European Commission's wishes (e.g. program claims).  Most EPP colleagues tend to blindly follow their shadow rapporteur.  Many critics have in vain sought dialogue with Wuermeling.  Wuermeling has however actively contacted the press in order to present himself as an opponent of %(q:patents on pure software and business methods) and to ascribe contrary impressions of the public to %(q:misperceptions of the opensource lobby).  In the JURI discussions of May 2003, Wuermeling tabled a pseudo-restrictive amendment (recital 13d), according to which a patent claim is limited to a specific product an may not encompass underlying algorithms.   Some of the corporate patent lawyers on whom Wuermeling is relying have been laughing about Wuermeling's lack of basic knowledge about the subject matter on which he is legislating.  One explanation for this lack of knowledge and unwillingness to meet critics is that Wuermeling is extremely busy with other subjects, such as the EU Constitution Convention.

#Wni: Der Wuermelingsche Standard-Diskurs

#Whe: Wuermeling's Activities in in connexion with the Software Patent Directive Proposal

#Slt: Accounts of failed attempts to get in touch Wuermeling

#Knr: Folgende Versatzstücke finden sich typischerweise in Antworten von Wuermelings Fraktionskollegen an besorgte Software-Entwickler.  Sie zeugen von mangelnder Kenntnis sowohol der ökonomischen wie der juristischen Aspekte der Software-Innovation und von grob-fahrlässigem Umgang mit zentralen ordnungspolitischen Fragen.

#Gti: As a matter of fact, the legal framework for patenting computer-implemented inventions needs to be harmonised on European level. Modern inventions are increasingly based on computer programmes. These innovations cannot be exempted from the protection available. Intellectual property can, in these cases, be protected either by granting a patent on the invention or through copyright protection.

#hdW: Urheberrecht ist nicht Kopierschutz.  Schutzgegenstand ist die %(e:Schöpfung), ähnlich wie beim Patent die %(q:Erfindung).  Wuermeling neigt dazu, das Urheberrecht klein zu reden und hat in diesem Zusammenhang auch schon juristisch unhaltbare Falschaussagen unter seinen Fraktionskollegen verbreitet.

#eas: However, it is very difficult to draw the line. This has, due to non-harmonised rules and practices, lead to differing decisions throughout the EU. While the patent office of one Member State grants a patent on a certain invention, the competent authorities of another Member State refuse to do so. This is not acceptable in an common market.

#vci: Das ist unvermeidbar und von keinem Richtlinienvorschlag änderbar.  Es gibt beim Patentrecht eine gewisse systematische Unsicherheit hinsichtlich der Gültigkeit von Patenten.  Diese Unsicherheit wird durch Wuermelings Richtlinie eher vergrößert als verkleinert, wie eine für seinen Rechtsausschuss angefertigte Studie belegt.

#ife: The Commission has proposed to harmonise the patent law in this area. Its proposal is mainly based on the existing practice in the Member States and of the European Patent Office. The aim is a harmonised application of the law by patent offices and courts in the common market.

#rtu: Die Europäische Kommission hat sich streng an der Praxis des Europäischen, Japanischen und Amerikanischen Patentamtes orientiert, wie sie von der %(ts:Trilateralen Konferenz) der drei Ämter im Jahre 2000 formuliert wurde.  Nationale Praxis fand keine Berücksichtigung.

#ttW: Contrary to your understanding, this does not enable a general patenting of software.

#Wei: The condition for patentability is, apart from other criteria that have to be fulfilled, the existence of a %(q:technical contribution). This is not the case for software alone.

#tSW: Es ist bei Software in der Regel der Fall.

#tae: On the other hand it would not be justified to deny an invention patentability just because it contains data processing elements.  Thus we are consciously not following the practise of the US, where patents are even granted for computer-based business methods. These are patents that we do not want.

#iee: The EPO has recently also been granting patents for %(q:computer-based business methods) (called %(q:computer-implemented business methods) by the EPO), based on the %(ts:Trilateral Standard of US, EU and JP), and this directive confirms the practise and makes it irrevertible.

#rd4: Thus we want to assure legally in the European Parliament, that software alone is not patentable (Article 4, § 1: %(q:In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step.  In order to involve an inventive step, a computer-implemented invention must make a technical contribution.))

#bie: Alle neuen Programmierideen erfüllen diese Voraussetzungen.  Art 4(1) ist redundant und schränkt die Patentierbarkeit von Software nicht ein.

#crs: The European Parliament complies with your justified request to enable the development of Open Source Software also in the future. It clearly states an exception of patentability: %(q:Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable) (Article 4a of the draft report). Consequently, and contrary to your fears, logical sequences that follow from the law of nature, would not be patentable.

#tBj: Auch Artikel 4a bestätigt lediglich die Praxis des Europäischen Patentamtes, die zur Patentierung von Algorithmen und Geschäftsmethoden wie in den USA führt.  Worte wie %(q:normale physische Interaktionen) sind leer, der Begriff %(q:technischer Beitrag) oder %(q:technisch) ist undefiniert.  Es waren Wuermeling und seine Parteigänger im Rechtsausschuss, die konsequent und erfolgreich jeden Versuch einer Präzisierung, wie von anderen Ausschüssen unternommen, bekämpft haben.

#dce: Mit dem Unterschied zwischen freier und proprietärer Software hat all das nichts zu tun.  Wuermeling verschickt dieses Standardschreiben an alle Kritiker, unabhängig davon, ob die Benachteiligung freier Software in deren Anfrage überhaupt als Problem genannt worden war.

#Wed: Also, there is no danger that elements of up to now free software could be protected by subsequent patenting. As opposed to the copyright, the patent does not protect an element of the invention but the invention as such as a whole. In addition, such kind of software would not be

#lun: Jedes einzelne Element laut JURI-Entwurf eine potentiell patentierbare Erfindung.  Patente beziehen sich immer auf einzelne Ideen, nicht auf komplexe Ideengewebe.

#suW: Wuermeling fehlen offenbar Grundkenntnisse darüber, was ein Patent ist.  Dieses mangelnde Verständnis zeigt sich insbesondere in Wuermelings Änderungsantrag zu Erwägungsgrund 13(d), über den Patentjuristen aus Wuermelings Klientel sich lustig machen.  So etwa Patentanwalt Erwin Basinski in einem Memorandum von Ende August 2003:

#fne: In addition, such kind of software would not be %(q:new) in the sense of patent law.

#rai: In der Tat geht es vor allem um künftige Rechenregeln.  Aber auch um diejenigen Logikpatente, die seit 1986 vom Europäischen Patentamt gegen den Buchstaben und Geist des Gesetzes erteilt worden sind, und deren Rechtsbeständigkeit den Konzern-Patentanwälten (Herrn Wuermelings Klientel) besonders am Herzen liegt.

#idb: The patent protects especially small and medium size developers. Without patenting, big competitors would be in a position to use the ideas of the small developers and make their profits. The %(me:recent sentencing of Microsoft to 520 million US$ for patent violation) is a good example for this.

#eri: Microsoft wurde hier für Verletzung eines %(q:reinen Softwarepatentes) verurteilt, wie Wuermeling sie angeblich verhindern will.  Das Eolas-Patent ist imstande, weit über Microsoft hinaus grossen Flurschaden anzurichten.  Inhaber ist nicht ein %(q:mittelständischer Entwickler) sondern eine Patent-Glücksritter-Firma, die bei einem Universitätsprojekt absprang.

#trd: For the above-mentioned reasons, we support the directive as amended by the draft report. It helps us to avoid a drift into the US-practice of a too far reaching and thus damaging grant of patents.

#ccc: Am EPA herrscht ohnehin bereits der %(te:trilaterale) Erteilungs-Standard.  Ein weiteres Abdriften ist kaum noch möglich und auch nicht in Sicht.  Keineswegs unwahrscheinlich erscheint hingegen eine Rückkehr des EPA auf den Pfad der Tugend.  Wenn sie nicht in letzter Minute durch die Richtlinie von Wuermeling und Freunden blockiert wird.

#tir: Veröffentlichung des Richtlinienvorschlages der Kommission

#une: Ernennung zum Schattenberichterstatter der EVP

#ulv: Veröffentlichung des %(ec:Aufrufs zum Handeln) und %(ep:Gegenvorschlages von FFII/Eurolinux)

#UWn: Wuermeling gehört nicht zu den Unterzeichnern, aber es zeigt sich im Vergleich zu der früheren klareren Sprache des Kollegen Dr. Martin Mayer eine Verschiebung hin zu nebeligen Begriffen aus dem Umfeld der Patentämter, die mit Wuermelings Zuständigkeit zu tun haben könnte.

#ctW: Telefonat von Wuermeling mit %(jm:Patentrichter Melullis), welches Wuermeling gelegentlich gegenüber Gesprächspartnern erwähnt, so auch öffentlich bei der kurz darauf folgenden JURI-Anhörung.  Wuermeling scheint von der Nachdenklichkeit des obersten deutschen Patentrichters wenig gelernt zu haben.

#rnn: Störmanöver während des Vortrags von Hartmut Pilch bei der Anhörung in JURI

#thi: Wuermeling macht bereits vor Beginn der Rede Geräusche, verlässt während ihr den Saal, protestiert nach ihr gegen die %(q:massiven Vorwürfe des Herrn Pilch).

#Etk: Gegenüber Journalisten einer EU-Zeitung erklärt Wuermeling, nur die Opensource-Szene sei gegen Softwarepatente, und sie verstehe nichts vom Patentrecht und habe keine konstruktiven Gegenvorschläge geliefert.

#ept: Auf einer JURI-Sitzung äußert Arlene McCarthy vorsichtige Kritik an der Praxis des Europäischen Patentamtes.  Daraufhin steht Wuermeling auf und hält eine Rede zur Verteidigung des EPA.

#jnu: Briefe der BDI-Patentjuristen an Wuermeling, Forderung nach Durchsetzung von %(pa:Programmansprüchen), ansonsten Billigung des Kommissions-Ansatzes.

#Wee: CSU-Mitstreiterin Angelika Niebler scheitert im Industrieausschuss mit ihren Versuchen, Beschränkungen der Patentierbarkeit zu verhindern und %(pg:Programmansprüche) durchzusetzen.  Niebler handelt immer in enger Tuchfühlung mit Wuermeling.

#hin: Bei einer CDU-Tagung zum Thema Patentrichtlinie in Berlin lässt sich Wuermeling von seinem Assistenten vertreten.  Allgemein herrscht auf der Tagung Unbehagen über Wuermelings Linie.

#Iet: Wuermeling gibt sich auf JURI-Sitzungen ein wenig nachdenklich, unterstützt aber McCarthy-Kurs ebenso wie den Kurs seiner Partei-Kollegen, die Anträge zur Durchsetzung von Programmansprüchen einreichen.

#Wkm: Wuermelings Mitstreiter in der EVP-Fraktion setzen in JURI Programmansprüche durch.

#trs: Wuermeling reicht %(q:Kompromiss-Änderungsantrag 4) betreffend Erwägungsgrund 13(d) ein und unterstreicht damit ein altes grundlegendes Missverständnis, wonach der Patentanspruch nur ein bestimmtes Produkt in seiner Gesamtheit betreffe.  Obwohl niemand mit dem Antrag etwas anfangen kann, stimmt der Rechtsausschuss mit seiner sozialistisch-konservativen Kompromiss-Mehrheit dafür.

#elg: Wuermeling ist unter den Empfängern des Offenen Briefes, der vorher immer wieder in privater E-Post und telefonisch von verschiedenen Seiten gestellte Fragen wiederholt.  Diese Fragen wurden von Wuermeling bislang ignoriert.

#nxw: Gegenüber der Süddeutschen Zeitung wiederholt Wuermeling seine Darstellung, die %(q:Opensource-Szene) habe die Richtlinie missverstanden, er wolle gar keine Softwarpatente etc.

#reW: Wuermeling wiederholt seinen Standard-Diskurs gegenüber dem Linux-Verband, der spezifische Fragen gestellt hatte.

#dIf: Die Patentlobby läuft gegen den %(io:Interoperabilitäts-Änderungsantrag 6a) Sturm, der in JURI versehentlich durchrutschte.  Wuermelings Fraktion drängt darauf, ihn aus der Richtlinie zu streichen.

#Wam: Wuermeling wiederholt seinen Standard-Diskurs gegenüber dem %(hb:Handelsblatt) und der Financial Times.

#lri: Dem Handelsblatt-Artikel ist eine Leserbefragung beigefügt, deren Ergebnis darauf hin deutet, dass über 70% der Leser Wuermelings Diskurs nicht überzeugend finden.

#tin: Armin Laschet MdEP versendet das Wuermelingsche Standard-Schreiben an einen Anfrager.

#Wit: In einigen Punkten unterscheidet sich %(lv:Laschets Variante) von anderen:

#Fwe: Der Fall %(EM) wird nicht erwähnt

#rxf: Es findet sich ein Abschnitt, der das Patentevangelium in abstrakter Form predigt und Wuermelings Patentgesetzgebung vorab von Verantwortung für die zu erwartenden Folgen freispricht: %(q:Sollte die Gefahr der Ausnutzung einer marktbeherrschenden Stellung bestehen, die zu Wettbewerbsverzerrungen und Nachteilen für die Konkurrenz führen könnte, ist dies keine Frage der Patenterteilung, sondern des Kartellrechts.)

#WWi: Wuermeling empfängt eine Gruppe von Softwarepatentkritikern in seinem Büro.

#eWl: Diese waren kurzfristig (ca 1-2 Tage im voraus) eingeladen worden.

#oWe: Am selben Tag beantwortet Wuermeling (oder sein Assistent) Anfragen, die sich angehäuft haben, mit dem Wuermelingschen %(jw:Standard-Schreiben).

#isl: Während Wuermeling in diesem Schreiben %(q:im Namen meiner Kollegen die Position der CDU/CSU-Gruppe) erläutert, wird in zeitgleichen Schreiben von CDU/CSU-Kollegen in den letzten Wochen zunehmend diese Position explizit als die Position des Kollegen Wuermeling bezeichnet.  Wuermeling scheint unter einem gewissen Druck zu stehen, hat seine Positionen aber nicht im geringsten verändert.  Selbst für kleinere Retuschen am Standard-Schreiben hat Wuermeling offenbar keine Zeit gehabt.

#anm: Various representatives of major associations and companies which are opposed to software patents tried in vain to meet Wuermeling or have him on the phone.  We know of nobody who met him before the final %(jd:JURI decision).  However, like McCarthy, Wuermeling always has plenty of time for talking to journalists, such as e.g. Tanja Schwarzenbacher from Süddetusche Zeitung on 2003/06/25, whom he told his usual story that the %(q:fears of the opensource lobby are groundless) and he was %(q:not changing anything) and %(q:not making software as such patentable, but only computer-implemented inventions).

#nrs: Attorney Jürgen Siepmann, legal delegate of Linux-Verband, documents his numerous futile attempts to contact Wuermeling:

#ilW: Ein erster Kontaktaufnahmeversuch mit der Bitte um ein Gespräch erfolgte am 11.03.2003 per Email, da ich mich am 12.03.2003 in Strasbourg aufhielt. Am 12.03.2003 habe ich dann vom Parlament aus im Büro nochmals angerufen und meine Bitte wierderholt.

#ase: Mir war klar, dass dies viel zu kurzfristig war.  Deshalb gab es noch folgende Telephonate von meinem Büro aus:

#EWe: und eine Email vom 20.03.2003, die ein Gesprächsangebot enthält.

#far: Es wurde ein Treffen in Luxemburg in Aussicht gestellt, was aber nicht stattfand, da Herr Dr. Wuermeling keine Zeit hatte.

#m6n: In weiteren Emails vom 03.04.2003 und vom 16.05.2003 wurde das Gesprächsangebot wiederholt.

#TWr: Mit seinem Assistenten Herrn Huber hatte ich mehrere kurze Telephongespräche, in denen er ein Treffen am 31.03.2003 in Berlin vorschlug, was aber dann nicht stattfand, weil Herr Huber terminlich zu sehr eingeengt war. Es wurden in Berlin nur einige Worte zwischen Tür und Angel auf einer Veranstaltung der CDU über Softwarepatente gewechselt, in denen er pauschal versprach, dass unsere Bedenken berücksichtigt werden, was man aber nicht erkennen kann. Auch die telephonischen Gespräche mit Herrn Huber enthielten nur das pauschale Versprechen, unsere Bedenken zu berücksichtigen.

#dir: Frau Dr. Krogmann äußerte sich auf dieser Veranstaltung am 31.03.2003 in der Bundesgeschäftsstelle der CDU in Berlin nicht besonders glücklich über die Entwicklungen in JURI, sie schien jedoch den Eindruck zu haben, dass man daran nichts ändern könne.

#uni: Interessant finde ich, dass Herr Huber einerseits geäußert hat, die EVP-Mitglieder in JURI würden sich dem Votum von Herrn Dr. Wuermeling anschließen, während er in Berlin damit argumentiert hat, die Mehrheitsverhältnisse in JURI seien halt so.

#oWn: Marco Schulze, managing director of Nightlabs.com, spent 10 days in the European Parliament from Aug 27 to Sep 5 and obtained an appointment with Wuermeling through his assistant.  This appointment was however cancelled without a reason.

#lsa: Zahlreiche Briefe an Wuermeling blieben unbeantwortet, darunter ein Offener Brief des Linux-Verbandes, der Klarstellung anhand genannter Beispielpatente fordert und auf einen ebenfalls unbeantworteten Brief von Hartmut Pilch verweist.

#tgg: Der Vortragsinhalt ist dort nicht zu finden.  Er wurde vor dem Wirtschaftsrat der CDU/CSU gehalten, einem CDU-nahem Gremium welches sehr stark von protektionistischen Ideologien der Vorstände großer Unternehmen geprägt ist.  Bei der gleichen Veranstaltung hielt Wuermelings Kollegin Angelika Niebler, die sich auch im Europaparlament für maximale Patentierbarkeit von Software stark machte, zusammen mit einem Vertreter des Gennahrungs- und Genpatent-Konzerns Monsanto zum Thema %(q:Vollkasko fuer die Umwelt auf kosten der Wirtschaft?)

#6mi: Geboren: 1960 Familienstand ... NB: Bamberg ist ein bedeutender Siemens-Standort.

#KsW: This report of Wuermeling about his achievements in Brussels mentions that he is studying the software patent issue.

#lem: Dort finden sich gelegentlich Fragmente zur Patentpolitik, z.B. %(bq:Eine Vereinfachung des Patentsystems ist lange überfällig, da Europa im globalen Wettbewerb der Innovationen gerade gegenüber den USA und Japan an Konkurrenzfähigkeit gewinnen muss,) so Wuermeling. %(bq:Mit dem Gemeinschaftspatent schließt die EU im globalen Wettlauf um den Patentschutz zu den USA auf. Der Binnenmarkt braucht ein Weltklassepatent.)

#2uW: Dirk Schmidt 2003-08: Pantentierbarkeit computerimplementierter Erfindungen - Softwarepatente?

#def: Ein CDU-Stadtrat stellt den Europa-Abgeordneten seiner Fraktion öffentlich Fragen.

#Sri: On the 20th anniversary of Microsoft's European headquarter in Munich, Dr. Edmund Stoiber, head of Bavarian government and of its governing conservative party, the Christian Social Union (CSU), says Bavaria owes Microsoft a lot and vows loyalty, pledging among others not to follow the Berlin government's interior ministry (whose minister Otto Schily was also present and gave a more distanced speech) in introducing Linux-based computing solution.  Various influential people within CSU have during this time taken the Microsoft side in debates about the use of free software in public administrations.

#Wcs: Wuermeling's own home page.  Some political speeches, nothing about software patents.

#eni: Wuermeling & Siemens

#mPa: 2004-05-19 Wuermeling hails Council decision, says Parliament was misguided by irrational opensource lobbyists

#toc: These blatant misinformations about software copyright were distributed in the EPP fraction by their shadow rapporteur on software patentability, Dr. Joachim Wuermeling, who should know copyright law, since he is a lawyer himself.

#enj: Der Vorsitzende des Linux-Verbandes weist Wuermeling darauf hin, dass seine Vorschläge das Gegenteil der von ihm verkündeten Zielsetzungen erreichen und fordert ihn auf, anhand von Beispielpatenten zu erklären, was er bezweckt und wie er seine Ziele erreicht.  Dieser Brief ist bislang (2003/08/06) unbeantwortet geblieben.

#pul: Wuermeling gehörte zu den Haupt-Adressaten dieser Bitte um Klarstellung der mehrdeutigen JURI-Formulierungen anhand im Brief beschriebener Beispielpatente und Fragestellungen.  Die Klarstellung erfolgte nicht.

#eoh: Wuermeling's standard reply, sent to Hartmut Pilch in English on 2003/09/16.

#itt: EU Council of Regions 1999 statement against software patents

#iWi: This statement carries the signature of Dr. Edmund Stoiber, Bavaria's head of government and Wuermeling's boss.  Arlene McCarthy is also often active in the political scene of the Council of Regions.

#oja: McCarthy's text is connected to a letter to the Financial Times from McCarthy, which was preceded by one co-signed by Harbour and Wuermeling, in which both complain about %(q:malicious open source lobbyists) who are judging them by their actual deeds rather than by their claimed intentions.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatremna.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatjwuermeling ;
# txtlang: en ;
# multlin: t ;
# End: ;

