Sehr geehrter Herr Dirnstorfer,

vielen Dank f�r Ihre E-Mail, in welcher Sie sich zur Frage der Patentierung von computerimplementierten Erfindungen �u�ern. Gerne m�chte ich Ihnen auch im Namen meiner Kollegen die Position der CDU/CSU-Gruppe im Europ�ischen Parlament zum Kommissionsvorschlag erl�utern.

Grunds�tzlich bedarf die patentrechtliche Behandlung computerimplementierter Erfindungen einer gemeinschaftsweiten Regelung. Moderne Erfindungen st�tzen sich zunehmend auf Computerprogramme. Diese Innovationen k�nnen nicht von jeglichem Schutz ausgenommen werden. Der Schutz des geistigen Eigentums kann in solchen F�llen einerseits durch die Erteilung eines Patents auf die Erfindung, andererseits durch den Kopierschutz des Urheberrechts erreicht werden. Jedoch ist die Abgrenzung schwierig. Das hat zu einer uneinheitlichen Rechtspraxis in der Europ�ischen Union gef�hrt. W�hrend das Patentamt eines Mitgliedstaats auf eine bestimmte Erfindung ein Patent erteilt hat, wurde das von den zust�ndigen Stellen eines anderen Mitgliedstaates verweigert. Das ist nicht hinnehmbar in einem Binnenmarkt.

Die Kommission hat vorgeschlagen, das Patentrecht in diesem Feld zu harmonisieren. Sie st�tzt sich dabei im wesentlichen auf die in den Mitgliedstaaten und beim Europ�ischen Patentamt existierende Praxis. Ziel ist eine m�glichst einheitliche Rechtsanwendung durch Patent�mter und -gerichte innerhalb des Binnenmarktes. Wir unterst�tzen diesen Ansatz.



Dabei wird - entgegen Ihrer Einsch�tzung - nicht etwa eine generelle Patentierung von Software erm�glicht. Voraussetzung f�r die Patentierbarkeit soll hingegen das Vorliegen eines *technischen Beitrags" sein. Das ist bei reiner Software nicht der Fall. Es w�re andererseits auch nicht gerechtfertigt, einer Erfindung nur deshalb die Patentierung zu versagen, weil sie EDV-Elemente beinhaltet. Damit folgen wir bewusst nicht der US-Praxis. Dort haben die Patent�mter selbst f�r computergest�tzte Gesch�ftsmethoden Patente erteilt. Das wollen wir nicht.

Deshalb wollen wir im Europ�ischen Parlament juristisch sicherstellen, dass reine Software nicht patentiert werden kann. (Art. 4, Ziff. 1: "Um patentierbar zu sein, m�ssen computerimplementierte Erfindungen neu sein, auf einer erfinderischen T�tigkeit beruhen und gewerblich anwendbar sein. Um das Kriterium der erfinderischen T�tigkeit zu erf�llen, m�ssen computerimplementierte Erfindungen einen technischen Beitrag leisten").

Ihrem berechtigten Anliegen, auch weiterhin die Entwicklung Freier Software zu erm�glichen, entspricht das Europ�ische Parlament, wenn es in einem neu eingef�gten Artikel klare Ausnahmen von der Patentierbarkeit statuiert: "Erfindungen, zu deren Ausf�hrung ein Computerprogramm eingesetzt wird und durch die Gesch�ftsmethoden, mathematische oder andere Methoden angewendet werden, [sind] nicht patentf�hig, wenn sie �ber die normalen physikalischen Interaktionen zwischen einem Programm und dem Computer, Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in der es abgespielt wird, keine technischen Wirkungen erzeugen" (siehe Artikel 4a im Berichtsentwurf des Rechtsausschusses). Damit w�ren auch, entgegen Ihrer Bef�rchtung, sich aus den Naturgesetzen ergebende logische Abfolgen nicht patentierbar.

Die Bef�rchtung, einzelne bisher freie Softwareelemente k�nnten durch eine sp�tere Patentierung gesch�tzt werden, ist nicht berechtigt. Denn das Patent sch�tzt - anders als das Urheberrecht - nicht einzelne Elemente der Erfindung, sondern nur die Erfindung als solche. Au�erdem w�re eine solche Software nicht "neu" im Sinne des Patentrechts.

Das Patent sch�tzt gerade mittelst�ndische Entwickler. Denn ohne die Patentierung k�nnten gro�e Vermarkter ohne Sanktion Ideen von kleinen H�usern nutzen und den finanziellen Gewinn daraus ziehen. Die j�ngste Verurteilung von Microsoft zum Schadenersatz von 520 Mio. US$ wegen Verletzung des Patents eines Softwarehauses zeugt davon.

Wir unterst�tzen aus den oben genannten Erw�gungen die Richtlinie mit den im Berichtsentwurf vorgesehenen �nderungen. Nur so kann ein Abdriften in amerikanische Verh�ltnisse mit einer zu weitgehenden und daher sch�dlichen Patentierung vermieden werden.

Mit freundlichen Gr��en


Dr. Joachim Wuermeling, MdEP


________________________________
Dr. Joachim Wuermeling, MdEP
Europ�isches Parlament
Rue Wiertz  ASP 15 E 210
B-1047 Br�ssel
Tel.: +32-2-284-5711
Fax: +32-2-284-9711
www.wuermeling.net
www.eu-konvent.de


>>> Stefan Dirnstorfer <Stefan.Dirnstorfer@nagler-company.com> 07/15/03 10:32 >>>
Sehr geehrter Herr W�rmeling

Am 1. Semptember entscheiden Sie �ber ein neues europ�isches 
Patentrecht. Die vorgeschlagene Neuregelung birgt viele Chancen und 
viele Risiken f�r die europ�ische und die deutsche Wirtschaft.

Ich gehe davon aus, dass Sie mit vielen positiven Effekten bereits 
vertraut sind. Jedoch gibt es, wie bei jedem Gesetz, Gewinner und 
Verlierer. Bei einem sinnvollen Gesetz sollte der Gewinn gr��er sein als 
der Verlust.

Ich m�chte ihnen drei Argumente pr�sentieren, die zeigen das Europa mit 
einem Patentrecht nach US Vorbild mehr verlieren wird als es jemals 
gewinnen kann:


1) Auf dem Gebiet der Softwareentwicklung sind uns die USA einige Jahre 
voraus und werden daher auch weiterhin den L�wenanteil der 
Softwarepatente halten. F�r die deutsche Wirtschaft bedeutet dass 
zwangsl�ufig einen Nettoabfluss von Kapital.
Ein m�glicher Anreiz f�r zus�tzliche Forschung kann das nicht 
ausgleichen. Studien zeigen sogar, das nach der Einf�hrung von 
Softwarepatente in den USA die Ausgaben f�r R&D r�ckl�ufig waren. [1]

2) US Firmen hatten bereits 20 Jahre Zeit auf Patent-orientierte
Softwareentwicklung unzusteigen. Die Neuregelung des Patentrechts 
erlaubt es extrem allgemeine Patente anzumelden. Patentiert ist 
beispielsweise jeder automatisierte Verkauf �ber das Internet 
(EP0803105) oder jede automatisierte Preisverhandlung (EP792493). Es 
wird kaum m�glich sein ein Online-Gesch�ft zu betreiben ohne eines der 
US-Patente zu verletzen. Bei diesen Patenten ging es offensichtlich 
nicht darum m�glichst innovativ zu sein, vielmehr war es entscheidend 
als erster am Patentamt zu sein. Europa ist hierbei bereits ins 
Hintertreffen gelangt und m�sste mit dem neuen Patentrecht bitter daf�r 
bezahlen.

3) Europa verliert seine Vorreiterrolle in der Einf�hrung von OpenSource
Software. Ein ganz neues Gesch�ftmodell erlaubt bereits vielen Firmen 
und zunehmend �ffentlichen Einrichtungen ihre Kosten zu senken und die 
Produktivit�t zu steigern. Europa besizt einen bedeutenden Vorsprung bei 
der inzwischen weltweit vollzogenen Umstellung der Standardsoftware auf 
frei Pendants wie Linux und OpenOffice. Nachdem bei diesem erfolgreichen 
Konzept Code statt Geld getauscht werden, k�nnen auch kleinste 
Lizenzforderungen nicht bedient werden. Viele europ�ische Entwicklungen 
w�rden mit der Einf�hrung von Softwarepatenten ihren Wert verlieren.


Obwohl die Argumente der Patentbef�rworter einleuchtend und �berzeugend 
sind, f�hren sie nicht in jedem Fall zum Erfolg. Das Beispiel Australien 
zeigt, wie ein Land seiner eigenen Wirtschaft ein Bein stellte in dem es 
jedes noch so triviale Patent aus dem Ausland f�r g�ltig erkl�rte.[2]

Ich bitte Sie nun, handeln Sie im Interesse der gesamten deutschen und 
europ�ischen Wirtschaft. �berlegen Sie sich gr�ndlich was Europa als 
Kontinent gewinnt und verliert. Stimmen Sie am 1. September f�r ein 
starkes Europa und eine starke Softwareindustrie in Deutschland.

Vielen Dank,
Stefan Dirnstorfer


REFERENZEN:
[1] http://swpat.ffii.org/papers/bessenhunt03/index.en.html
[2] http://swpat.ffii.org/papiere/mandeville82/index.en.html
