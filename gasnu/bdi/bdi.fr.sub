\begin{subdocument}{swpatbdi}{BDI: Bund der Deutschen Industrie-Patentjuristen}{http://swpat.ffii.org/acteurs/bdi/index.fr.html}{Groupes de travail\\swpatag@ffii.org}{Der Bund der Deutschen Industrie (BDI) ist ein Dachverband der deutschen Industrieverb\"{a}nde, der wiederum auf EU-Ebene dem Dachverband UNICE angeh\"{o}rt.  Bitkom und ZVEI haben ihre Unterst\"{u}tzung des Europ\"{a}ischen Patentamtes und des EU-Richtlinienvorschlages f\"{u}r die Patentierbarkeit von computer-implementierter Organisations- und Rechenregeln (Algorithmen, Datenverarbeitungsprogrammen) bekundet.  VDMA u.a. haben aufgrund innerer Uneinigkeit auf eine Stellungnahme verzichtet.  In Wirklichkeit herrscht auch bei Bitkom und ZVEI Uneinigkeit.  BDI und EUNICE werden aber ihrerseits \"{a}hnlich wie die Mitgliesverb\"{a}nde in dieser Frage von Patentjuristen dominiert, die es gewohnt sind, ihre Standesinteressen als die der Industrie auszugeben.  In den Stellungnahmen von BDI und EUNICE finden sich keinerlei Hinweise auf die unterschiedlichen Interessenlagen der Mitgliedsunternehmen, und volkswirtschaftliche Studien werden ebenso ignoriert wie die Sicht der Techniker und Programmierer.}
Es ist zwar nicht bekannt, wie genau die Meinungsbildung beim BDI und UNICE verl\"{a}uft, aber aus den Ergebnissen ist klar zu entnehmen, dass keine Unternehmensinteressen zur Sprache kommen.  In BDI-Stellungnahmen findet sich lediglich inmitten von allerlei unverst\"{a}ndlichen rechtsdogmatischen Glaubensaussagen eine Feststellung wie ``Das Interesse der Industrie liegt darin, keine M\"{o}glichkeiten der Patentierung zu verlieren'', aus denen man eine einseitige Fixierung auf den Standpunkt des Patent-Nutznie{\ss}ers herauslesen k\"{o}nnte, sofern der BDI nicht den fiskalischen Nutzen von Patenten (als Mittel zur Bilanzgestaltung und Steuereinsparung) im Auge hat.

Versuche, mit dem BDI ins Gespr\"{a}ch zu kommen, um ermitteln zu lassen, welche Patentierungsm\"{o}glichkeiten denn wirklich stimulierend und welche eher bremsend wirken k\"{o}nnten, scheiterten an der v\"{o}llig unkritischen Haltung der Vereinsfunktion\"{a}re, die sich offensichtlich nur als Transmissionsriemen f\"{u}r Beschl\"{u}sse verstehen, \"{u}ber deren Zustandekommen sie keine Rechenschaft abgeben wollen.   Hierauf angesprochen meinte die beim BDI zust\"{a}ndige Juristin Frau Dr. Vieregge, so schlimm k\"{o}nne es ja nicht sein, denn dem BDI seien die Mitglieder noch nicht davon gelaufen.  Ebenso wie andere Verbandsmitarbeiter verfolgen die BDI-Leute nicht unbedingt bewusst finstere Partikularinteressen, aber sie verstehen die Patentdiskussion nicht und geben sich auch keine M\"{u}he, sie zu verstehen.

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf BDI 2002-04-15 zum EUK/BSA-Entwurf: die Industrie will keine Patentierungsm\"{o}glichkeiten einb\"{u}{\ss}en\footnote{http://swpat.ffii.org/papiers/eubsa-swpat0202/bdi020415/index.de.html}}}

\begin{quote}
Hiermit nimmt der Bundesverband der Deutschen Industrie (BDI) zu dem knapp 2 Monate zuvor von der Europ\"{a}ischen Kommission vorgelegten Entwurf einer Richtlinie \"{u}ber die Patentierbarkeit computer-implementierter Organisations- und Rechenregeln Stellung.  Der BDI sollte satzungsgem\"{a}{\ss} die Meinung seiner Mitgliedsverb\"{a}nde b\"{u}ndeln.  In der Stellungnahme ist von deren durchaus uneinheitlicher Meinungsbildung jedoch \"{u}berhaupt nicht die Rede.  Es werden auch nicht Interessen von Unternehmen artikuliert.  Stattdessen finden sich nur Maximalforderungen der Patentbranche wieder, die mit abstrakten Begriffen \"{u}ber abstrakte Begriffe in einem esoterischen Jargon des Europ\"{a}ischen Patentamtes sprechen und dabei h\"{a}ufig noch \"{u}ber die eigenen Abstraktionen stolpern.  So werden z.B. Formulierungen der Kommission, die auf eine m\"{o}glichst weite Patentierbarkeit zielen, als Einschr\"{a}nkungen der Patentierbarkeit verstanden und bek\"{a}mpft.  Das einzige Interesse der Industrie besteht laut BDI-Papier darin, keinerlei M\"{o}glichkeiten der Patenterlangung und -durchsetzung zu verlieren.  Die Sicht der von Patentdickichten belasteten und bedrohten Unternehmen kommt ebenso wenig zur Sprache wie die (unter Patentjuristen allgemein unbekannte) volkswirtschaftliche Kritik am Patentwesen und seiner Ausdehnung.
\end{quote}
\filbreak

\item
{\bf {\bf Pollmeier GmbH gegen Logikpatente: Industrieverb\"{a}nde vertreten nicht Industrieinteressen\footnote{http://www.esr-pollmeier.de/swpat/}}}

\begin{quote}
Stefan Pollmeier leitet ein Unternehmen mit 45 Angestellten im Bereich der Elektronik.  Er hat sich mit einigen seiner Kollegen zusammen die Patentierungspraxis des Europ\"{a}ischen Patentamtes angesehen und sieht hierin eine sinnlose Bedrohung seiner unternehmerischen Interessen.  Pollmeier stellt die Probleme dar und weist darauf hin, dass sehr viele andere Unternehmen in gleicher Weise von Logikpatenten bedroht sind und dass deren Sorgen in Verb\"{a}nden wie ZVEI und BDI bislang \"{u}bergangen worden sind.  Zur L\"{o}sung des Logikpatentproblems empfiehlt Pollmeier eine R\"{u}ckkehr zum strengen Begriff der technischen Erfindung als einer Lehre \"{u}ber Kausalzusammenh\"{a}nge von Naturkr\"{a}ften.
\end{quote}
\filbreak

\item
{\bf {\bf Vom Teufel geritten --- BITKOM e.V.\footnote{http://swpat.ffii.org/acteurs/bitkom/index.de.html}}}

\begin{quote}
Der deutsche Branchenverband Bitkom hat erst Mitte 2001 begonnen, sich mit Fragen der Patentpolitik zu befassen.  Die Meinungsbildung fand offenbar in einem sehr kleinen Kreis von Juristen und Patentjuristen statt, wobei IBM-Patentanwalt Fritz Teufel alles dominierte.  An der EU-Konsultation 2000 zu Swpat nahm Bitkom wegen unabgeschlossener Meinungbildung in der Sache nicht teil, daf\"{u}r aber sprang der europ\"{a}ische Dachverband EICTA ein, f\"{u}r den offenbar ebenfalls Teufel die Stellungnahme schrieb.
\end{quote}
\filbreak

\item
{\bf {\bf Evasion Fiscale par Brevets\footnote{http://swpat.ffii.org/analyse/cteki/index.en.html}}}

\begin{quote}
As patents are increasingly being granted for methods of organisation and calculation (which may be described in terms of computer machinery but are nonetheless nothing but abstract principles), it becomes very difficult for tax inspectors to assess whether a patent is actually being used or not.  Thus patents can be used to almost arbitrarily increase or decrease company assets and to create revenue streams from one corporate entity to another, preferably between affiliated companies or (overt or hidden) partner companies.  In some countries, such as Ireland, patent license revenues are even exempt from taxation.   In others, such as France, reduced tax rates apply.  These possibilities are increasingly being used.  They may also be coming into the reach of ordinary people who, by networking in a smart way, can shelter their money in safe distance from tax inspectors.  Maybe we can put together a consulting work group that will help people realise this potential.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: latex ;
% End: ;

