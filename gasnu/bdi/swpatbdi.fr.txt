<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Der Bund der Deutschen Industrie (BDI) ist ein Dachverband der deutschen Industrieverbände, der wiederum auf EU-Ebene dem Dachverband UNICE angehört.  Bitkom und ZVEI haben ihre Unterstützung des Europäischen Patentamtes und des EU-Richtlinienvorschlages für die Patentierbarkeit von computer-implementierter Organisations- und Rechenregeln (Algorithmen, Datenverarbeitungsprogrammen) bekundet.  VDMA u.a. haben aufgrund innerer Uneinigkeit auf eine Stellungnahme verzichtet.  In Wirklichkeit herrscht auch bei Bitkom und ZVEI Uneinigkeit.  BDI und EUNICE werden aber ihrerseits ähnlich wie die Mitgliesverbände in dieser Frage von Patentjuristen dominiert, die es gewohnt sind, ihre Standesinteressen als die der Industrie auszugeben.  In den Stellungnahmen von BDI und EUNICE finden sich keinerlei Hinweise auf die unterschiedlichen Interessenlagen der Mitgliedsunternehmen, und volkswirtschaftliche Studien werden ebenso ignoriert wie die Sicht der Techniker und Programmierer.
title: BDI: Bund der Deutschen Industrie(-Patentjuristen)
Epe: Es ist zwar nicht bekannt, wie genau die Meinungsbildung beim BDI und UNICE verläuft, aber aus den Ergebnissen ist klar zu entnehmen, dass keine Unternehmensinteressen zur Sprache kommen.  In BDI-Stellungnahmen findet sich lediglich inmitten von allerlei unverständlichen rechtsdogmatischen Glaubensaussagen eine Feststellung wie %(q:Das Interesse der Industrie liegt darin, keine Möglichkeiten der Patentierung zu verlieren), aus denen man eine einseitige Fixierung auf den Standpunkt des Patent-Nutznießers herauslesen könnte, sofern der BDI nicht den fiskalischen Nutzen von Patenten (als Mittel zur Bilanzgestaltung und Steuereinsparung) im Auge hat.
Vdb: Versuche, mit dem BDI ins Gespräch zu kommen, um ermitteln zu lassen, welche Patentierungsmöglichkeiten denn wirklich stimulierend und welche eher bremsend wirken könnten, scheiterten an der völlig unkritischen Haltung der Vereinsfunktionäre, die sich offensichtlich nur als Transmissionsriemen für Beschlüsse verstehen, über deren Zustandekommen sie keine Rechenschaft abgeben wollen.   Hierauf angesprochen meinte die beim BDI zuständige Juristin Frau Dr. Vieregge, so schlimm könne es ja nicht sein, denn dem BDI seien die Mitglieder noch nicht davon gelaufen.  Ebenso wie andere Verbandsmitarbeiter verfolgen die BDI-Leute nicht unbedingt bewusst finstere Partikularinteressen, aber sie verstehen die Patentdiskussion nicht und geben sich auch keine Mühe, sie zu verstehen.
Pee: Pollmeier GmbH gegen Logikpatente: Industrieverbände vertreten nicht Industrieinteressen
Sln: Stefan Pollmeier leitet ein Unternehmen mit 45 Angestellten im Bereich der Elektronik.  Er hat sich mit einigen seiner Kollegen zusammen die Patentierungspraxis des Europäischen Patentamtes angesehen und sieht hierin eine sinnlose Bedrohung seiner unternehmerischen Interessen.  Pollmeier stellt die Probleme dar und weist darauf hin, dass sehr viele andere Unternehmen in gleicher Weise von Logikpatenten bedroht sind und dass deren Sorgen in Verbänden wie ZVEI und BDI bislang übergangen worden sind.  Zur Lösung des Logikpatentproblems empfiehlt Pollmeier eine Rückkehr zum strengen Begriff der technischen Erfindung als einer Lehre über Kausalzusammenhänge von Naturkräften.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatbdi ;
# txtlang: fr ;
# End: ;

