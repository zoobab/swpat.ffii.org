<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Gesellschaft für Informatik, %(q:Software-Engineering) und Patente

#descr: Die GI wird von Hochschulprofessoren geleitet und hat 21000
Mitglieder.  Viele Informatikstudenten und auch Softwareunternehmen
sehen eine Mitgliedschaft in der GI als obligatorisch an.  Die GI
steht weder unter direktem Einfluss der Patentbewegung noch sind ihre
Mitglieder besonders an Softwarepatenten interessiert.  Dennoch
schaltete sich das Präsidium der GI im Herbst 2000 in die Debatte ein
und plädierte -- unter heftigem Protest vieler Mitglieder -- für
Softwarepatente.  Dabei verwendete es die bekannte Ideologie der
Patentbewegung mit einigen GI-spezifischen Zusätzen, nämlich dem Traum
von perfekter generalstabsmäßiger Planung der Softwareentwicklung
(%(q:Software Engineering)).  Diese Wunschvorstellung hängt mit dem
Selbstwertgefühl einiger Universitätsinformatiker zusammen und ist in
er GI als eine Art Berufsideologie seit Jahren weit verbreitet.  Diese
%(q:ingenieurmäßige) Denken steht nach Sicht mancher älterer
GI-Professoren (wie z.B. Endres) in unversöhnlichem Gegensatz zu einer
anarchischen Vorgehensweise der %(q:Opensource-Bewegung).  Die
Ausdehnung des Patentwesens bietet manchen Informatikern auch deshalb
eine unwiderstehliche Chance, weil patentierte Rechenregeln zu Zwecken
der Forschung und Lehre frei verwendet werden dürfen.  Während
theoretisch ausgerichtete Informatiker mit Programmierverboten und
Erpresserbriefen kaum in Berührung kommen können, eröffnen Patente
ihnen die Hoffnung auf unmittelbare Verwertbarkeit ihrer
Forschungsergebnisse.  Bei Verhandlungen mit
privatisierungsbeflissenen Hochschulpolitikern kann dies einen
erheblichen Vorteil bedeuten.  Glaubt man jedenfalls.  Die Frage der
Technizität von Computerprogrammen ist für das GI-Präsidium vor allem
eine Prestigefrage.

#Dvn: Die GI gibt die Zeitschrift %(q:Informatik-Spektrum) heraus und ist
eng mit der %(CZ) verbunden.  Die Computer-Zeitung hat auf einigen
Messen Podiumsdiskussionen organisiert, bei denen wir die
Softwarepatentebefürworter aus dem GI-Präsidium und seinem Umkreis
kennenlernen konnten.

#H0W: Heise 2002-03-30: 30 Jahre Informatik: Eliten oder Nieten

#Bin: Bei einer Jubiläumsfeier der deutschen Informatik provozierten
Vertreter von SAP u.a., indem sie die Verdienste dieses Faches
grundsätzlich in Frage stellten.  Hierdurch erklärt sich wohl z.T. das
Streben der GI nach Anerkennung als %(q:Ingenieurwissenschaft).  Es
gibt offenbar Wunden, an denen man die Vertreter dieses Faches packen
kann.

#GlD: Gräbe 2001-08-01: Gesellschaft für Desinformatik?

#Edh: Ein Informatiker nennt einige Beispiele aus der Vergangenheit, die
zeigen, dass die GI seit Jahren weit häufiger als andere große
Wissenschaftsverbände anti-aufklärerisch im Dienste pekuniärer
Interessen arbeitet.

#Hei: Hier findet sich im wesentlichen die Position des GI-Präsidiums,
allerdings im Namen weiterer Fachverbände, die einen Arbeitskreis
gebildet haben wollen.  Ernsthafte Arbeit wurde freilich nicht
geleistet, und uns vorliegenden Informationen zufolge hat der Kreis
nie getagt.

#Nds: Nur ein kurzes Konzept, in dem sich die Grundthesen des Vorsitzenden
wiederfinden.

#VMf: Vortrag von Mayr über Softwarepatente

#DeK: Den gleichen Vortrag hielt Mayr 2001-04-15 in Frankfurt.  Die Folien
zeigen sehr schön Mayrs Argumentationskette:  (1) Die
Informatikprofessoren sind immens wichtig, aber genießen weniger
Autorität als die Ingenieurskollegen, denn jeder Hinz und Kunz
programmiert.  Unqualifizierten Programmier arbeiten nicht
ingenieurmäßig.  Ergo ist ingenieurmäßiges Programmieren
erstrebenswert. (2) Wenn Programmierer ebenso wie Ingenieure
regelmäßig patentieren würden, würden sie wahrscheinlich eher einen
ingenieurmäßigen Arbeitsstil annehmen.  (3) Also soll die
Softwarebranche am Patentwesen genesen.  Daran kann sich dann die
Wichtigkeit der Hochschulinformatik bestätigen, und
Hochschulprofessoren können davon profitieren. (4)Eigentum an Ideen
ist von der Verfassung geboten.  Viel von diesem Vortrag beruht auf
Einflüsterungen von PA Harald Springorum.

#Ano: Auf einer %(sf:Veranstaltung in der Universität Frankfurt) trug
GI-Präsident Mayr vor, warum das GI-Präsidium für Softwarepatente ist.
 Er meinte, Programmierung sollte %(q:ingenieurmäßig) betrieben werden
und Patente hätten sich im Bereich des Ingenieurwesens bewährt. 
Hieraus folgerte er, dass die Einführung von Patenten auf die
Informatik einer ingenieurmäßigen Herangehensweise in der Informatik
förderlich sei.  Aus dem Fehlen einer solchen Herangehensweise
entstehe der Volkswirtschaft jährlich viel Schaden.  Der Vortrag
entsprach weitgehend dem Inhalt einschlägiger GI-Presseerklärungen. 
Auf Anfragen aus dem Publikum hin konnte Mayr nicht erklären, wie
seine Schlussfolgerungen zustande kommen.  Bärmann berichtet
erstaunt-ironisch über die sprunghaft assoziative Logik des
GI-Präsidenten.

#lVt: GI-Mitglied 2003-08-28: Vorstand ignoriert Mitglieder

#huo: Ein GI-Mitglied berichtet: Trotz heftiger Bedenken vieler Mitglieder
wurde die Pro-Softwarepatent-Entscheidung vom GI-Vorstand einfach so
verkündet.

#Wdz: Bericht über Einspruch der GI gegen Amazon-Patent.

#Ske: Diskussion mit PA Springorum im Anschluss an die GI-Presseerklärung
zum Amazon-Patent.  Viele Mitglieder bemängeln %(q:Unlogik) der PE und
%(q:Haarspalterei) der Patentanwälte Springorum und Gravenreuth,
welche die Stellung des GI-Vorstands halten.

#tWh: Übersicht der GI-Geschäftsführung zum Thema Patente.  Es wird auch auf
kritische Stimmen, einschließlich FFII, verwiesen.

#Wze: GI 2003/09: FAQs zu Patenten

#uri: PA Springorum beantwortet im Namen der GI diverse fingierte Fragen,
z.B. %(q:Ich habe keinen Bock, mir den Patentkram reinzuziehen).  Die
FAQ spiegelt nicht im geringsten die Diskussionen im GI-Forum wieder.

#uGm: Bericht und Diskussion über GI-Ankündigung zum Amazon-Patent

#WWg: Bericht und Diskussion über Kritik des Linux-Verbands an dem
%(q:PR-Manöver) der GI.  Verbandssprecher Daniel Riek kritisiert
insbesondere die Parteinahme des GI-Präsidiums für die geplante
Richtlinie, welche gerade Patente wie das von Amazon ermögliche und
nicht, wie irreführenderweise behauptet, verhindere.

#Wkf: Hartmut Pilch setzt sich mit einer neuen GI-PR zur Patentfähigkeit von
Software auseinander.

#kh3: VDI-Nachrichten 2003/09/05

#ede: Mayr fordert in der Wochenzeitung des Verbands Deutscher Ingenieure,
die geplante Patentrichtlinie müsse zügig in der vom Rechtsausschuss
vorgeschlagenen Fassung verabschiedet werden.  Mayer erklärt, die GI
habe Einspruch gegen den europäischen Ableger des Amazon-Patentes
eingereicht, da dessen technischer Beitrag %(q:nicht für einen
Patentanspruch ausreiche). %(q:Um die Schutzwürdigkeit von
Softwareerfindungen kompetent beurteilen zu können, müssen die
Patentämter nun aber auch Informatikerinnen und Informatiker zur
Begutachtung eingereichter Softwaregesuche einsetzen).  Im gleichen
Artikel erklärt Patentanwallt %(JB), Trivialpatente seien seit drei
Jahren in den USA kein Problem mehr und seien es in Euroa noch nie
gewesen, da die Ämter qualifizierte Informatiker als Prüfer
eingestellt hätten.

#Ise: In dieser Ausschreibung ist sehr viel von %(q:Software-Engineering)
die Rede und Patente werden als %(q:Primärindikatoren) zur Beurteilung
informatischer Leistungen gepriesen.  Als besonders förderungswürdig
erachtet werden einige Systeme mit denen ein Fraunhofer-Institut
behauptet, mit dem Wildwuchs der %(q:Kunst des Programmierens)
aufräumen und ein %(q:ingenieurmäßiges Vorgehen) einführen zu wollen. 
Von den Ergebnissen solcher Projekte hört man nicht viel, und meist
kommen z.B. CASE-Tools o.ä. heraus, die geeigent sind, schöne
Flussdiagramme für die Führungsetage zu erzeugen aber ansonsten das
Programmieren in ein oft unproduktives Korsett schnüren und daher
inzwischen schon aus der Mode gekommen sind.  Aber als Methode, um
Informatikprofessoren wichtig erscheinen zu lassen und BMBF-Gelder
abzuräumen hat die %(q:Engineering)-Rhetorik wohl noch lange nicht
ausgedient.  Die assoziativen Patentreden von Mayr kann man als einen
untergeordneten Teil dieser Rhetorik verstehen.

#HzS: Hier verteidigte der Vize-Vorsitzende Andreas Stöckigt die
GI-Position.  Im wesentlichen war es das Argument %(q:Wenn die
Ingenieure patentieren dürfen, warum dann nicht auch wir
Informatiker?)  Diese Frage beantwortete Xuân Baldauf für den FFII
sehr gründlich.  Allerdings erreichte er wohl nicht alle Zuhörer und
auch nicht Stöckigt.  Es blieb bei einer eimaligen Vorführung.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatgiev ;
# txtlang: de ;
# multlin: t ;
# End: ;

