<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Free Software Alliance

#descr: La %(q:Free Software Alliance) (FSA, alliance du logiciel libre) est une organisation imaginaire, créée en août 2003 par Arlene McCarthy (membre du Parlement européen, Parti travailliste anglais )pour les communiqués de presse.  La FSA est un pendant de la %(q:BSA) (Business Software Alliance) : elle se bat de manière aggressive et intrusive pour le droit de ses membres à exploiter gratuitement la propriété intellectuelle d'autrui.  La FSA défend une caricature des positions de la FFII (Foundation for a Free Information Infrastructure) et de %(el:l'alliance Eurolinux).  Contrairement à la FFII, Eurolinux et la BSA, la FSA n'est composée que de %(cc:partisans du droit informatique) et ne bénéficie d'aucun soutien de la part d'éditeurs de logiciels commerciaux.

#tss: Arlene McCarthy s'est référée à la FSA dans plusieurs articles qu'elle a envoyés à des journalistes depuis fin août 2003 en persistant au moins jusqu'à la mi-septembre de la même année, en dépit d'articles largement diffusés soulignant le caractère imaginaire de cette organisation.

#Whn: Dans une interview avec le journaliste Stefano Porro, McCarthy attaque violemment la %(q:Free Software Alliance) et recommande de consulter le site web de cette organisation.  La fait le jeu de la mafia italienne en s'opposant à une législation nécessaire pour sévir contre son trafic de contrefaçons.  On doit distinguer les %(q:radicaux) de la FSA des %(q:modérés du mouvement Open Source), dont les inquiétudes légitimes sont prises en compte de manière adéquate par les propositions d'amendements de McCarthy.  Ces propositions d'amendements s'assurent qu'aucun logiciel en tant que tel ne soit brevetable, mais que seules le soient les inventions géniales comme l'idée d'Eolas d'étendre les navigateurs web avec des plug-ins are patentable.  En s'opposant à des brevets comme celui d'Eolas, la FSA fait le jeu de Microsoft et nuit aux intérêts des PME innovantes.  Porro finit son article avec des copier/coller du communiqué de presse de McCarthy du 24 septembr  e 2003, qui présente le vote du Parlement européen comme une victoire de McCarthy.

#eau: L'article du Financial Times cite largement les porte-parole du lobby des brevets et attribue le revers du Parlement européen à un lobbying agressif de la part d'un petit groupe, %(q:basé en Europe), suggérant que ce groupe est l'%(q:Association for Free Software) (Association pour les logiciels libres).  L'article semble ainsi très inspiré des communiqués de presse de McCarthy sur la FSA.

#aqW: Page de promotion des intérêts du logiciel libre, cite comme avantage de l'utilisation des logiciels libres %(q:Le fait qu'aucune 'Free Software Alliance' ne menacera votre entreprise avec des audits, des poursuites ou des amendes).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatfsa ;
# txtlang: fr ;
# multlin: t ;
# End: ;

