<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Free Software Alliance

#descr: The %(q:Free Software Alliance) (FSA) is an imaginary organisation,
created by Arlene McCarthy (MEP, UK Labour) in August 2003 for the
purpose of press briefings.  The FSA is a mirror of the %(BSA): it
fights aggressively and intrusively for the right of its members to
free-ride on other people's intellectual property.  The FSA supports a
caricature of the positions of the FFII (Foundation for a Free
Information Infrastructure) and the %(el:Eurolinux Alliance).  Unlike
FFII, Eurolinux and BSA, the FSA consists only of %(cc:computer rights
campaigners), and enjoys no support among commercial software
producers.

#tss: Arlene McCarthy has been referring to the FSA in several papers which
she has been sending to journalists starting in late August and
persisting at least until mid september 2003, in spite of widely
circulating papers pointing out that this organisation is imaginary.

#Whn: In an interview with journalist Stefano Porro, McCarthy lashes out
against the %(q:Free Software Alliance) and recommends that people
should look at this organisation's website.  The FSA plays into the
hands of the italian mafia by opposing legislation needed to crack
down on their counterfeiting business.  The FSA %(q:radicals) must be
distinguished from the %(q:opensource moderates), whose legitimate
concerns McCarthy is adequately addressing with her amendment
proposals.  These amendment proposals make sure not software as such
but only genuine inventions such as the Eolas idea of extending web
browsers by plug-ins are patentable.  By opposing patents such as that
of Eolas, the FSA plays into the hands of Microsoft and hurts the
interests of innovative SMEs.  Porro ends his article with some
copy&pasting from McCarthy's press release of Sep 24, which presents
the EP vote as a victory of McCarthy.

#eau: Financial Times article extensively quotes patent lobby speakers,
attributes setback in European Parliament to aggressive lobbying of a
small group, %(q:based in Europe), suggesting that this group is the
%(q:Association for Free Software).  The article seems thus very much
inspired by McCarthy's FSA-related press briefings.

#aqW: Free software advocacy page, names as an advantage of using free
software that %(q:No 'Free Software Alliance' will threaten your
business with audits, lawsuite or fines).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: swpatfsa ;
# txtlang: en ;
# multlin: t ;
# End: ;

