<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Eine ganz durch Abkommen mit Unternehmen und Verbänden finanzierte Zeitung aus einer Verlagsgruppe, die vorauseilenden Gehorsam gegenüber den (vermeintlichen) Interessen der potentiellen Geldgeber pflegt und bislang durch Patentpropaganda der plumpesten Art auffiel.  Mit Softwarepatenten ihr Brot verdienende Patentanwälte wie Axel Pfeiffer werden hier in jedem Artikel als Autorität zitiert.  Wenn etwa die Bundesjustizministerin zur CZ etwas gegen Softwarepatente sagt, wird das Zitat von Frau Däubler-Gmelin unterschlagen und durch einen abwertenden Kommentar von PA Pfeiffer ersetzt.  Nicht selten kommen auch FFII/Eurolinux kurz zu Wort, aber mehr in der Rolle exotischer Dissidenten, die auf verlorenem Posten stehen.  Dies spiegelte sich auch in einigen von der CZ veranstalteten Podiumsdiskussionen wieder, etwa nach dem Muster: %(q:Opensource-Sozialromantiker Hartmut Pilch gegen drei Praktiker aus dem Patent-Establishment).  Da in Wirklichkeit aber das Geld nicht unbedingt nur bei den Großfirmen und ihren Patentrechtlern liegt, besteht Hoffnung auf Besserung.  Wie strengen Weisungen die Redakteure unterliegen, ist uns nicht klar.  Der Chef Eduard Heilmeier hat immerhin auch ein Unix/Open-Magazin betrieben.  Im Prinzip dürfte es ihm egal sein, woher die Kohle kommt.
title: Computer-Zeitung
0Wt: CZ 020521: %(q:KMU wollen Patentschutz)
esl: CZ-Redakteur Thomas Hengl berichtet wohlwollend über eine %(bk:Bitkom-Stellungnahme für Softwarepatente).  In dieser Stellungnahme, die in einer geschlossenen Sitzung von 7 Großkonzern-Patentanwälten gegen 1 KMU-Vertreter durchgesetzt wurde, wird behauptet, Softwarepatente seien im Interesse Kleiner und Mittlerer Unternehmen (KMU).  Hengl baut aus dieser wilden Behauptung seine Zeitungs-Schlagzeile und zitiert den Universitätsausgründer Peter Baumann (Active Knowledge GmbH) als Kronzeugen.  Allerdings äußert Baumann auch Kritik an den Patentämtern und dem von Bitkom vorbehaltlos begrüßten Richtlinienvorschlag.  Im Gegensatz zu Bitkom sieht Baumann die zahlreichen Trivialpatente als ein ernsthaftes Problem an und meint, der RiLi-Vorschlag gehe ihm zu weit.  Baumann gehört auch zu dem überschauberen Kreis derjenigen Universitäts-Informatiker im GI-Umfeld, die gerne eine Firma auf einen oder wenige %(q:geniale Algorithmen) gründen, aber nicht gerne mit der etwas tristeren (und unserer Meinung nach unvermeidbaren) Realität von Logikpatenten in Verbindung gebracht werden wollen.  Die CZ hat gelegentlich Podiumsdiskussionen mit Beteiligung von Baumann und FFII veranstaltet.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: swpatcomputerzeitung ;
# txtlang: en ;
# End: ;

