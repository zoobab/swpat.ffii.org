\begin{subdocument}{swpatbitkom}{Vom Teufel geritten --- BITKOM e.V.}{http://swpat.ffii.org/akteure/bitkom.de.html}{Arbeitsgruppe\texmath{\backslash}\texmath{\backslash}swpatag@ffii.org}{Der deutsche Branchenverband Bitkom hat erst Mitte 2001 begonnen, sich mit Fragen der Patentpolitik zu befassen.  Die Meinungsbildung fand offenbar in einem sehr kleinen Kreis von Juristen und Patentjuristen statt, wobei IBM-Patentanwalt Fritz Teufel alles dominierte.  An der EU-Konsultation 2000 zu Swpat nahm Bitkom wegen unabgeschlossener Meinungbildung in der Sache nicht teil, daf\"{u}r aber sprang der europ\"{a}ische Dachverband EICTA ein, f\"{u}r den offenbar ebenfalls Teufel die Stellungnahme schrieb.}
heise-bitkom020512

\begin{sect}{prog}{Roter Faden: Mehr Macht den Vorst\"{a}nden (?)}
Bitkom engagierte sich in letzter Zeit
\begin{description}
\item[Gegen Urheberrechtspauschalgeb\"{u}hren, f\"{u}r Verwertungsutopien:]\ Besonders der Vorsitzende Rohleder ist von DRM und anderen Hoffnungen auf Verkr\"{u}ppelung digitaler Information zwecks Kassierens pro Lesevorgang begeistert.  Auch der Dachverband EICTA setzt hier einen Schwerpunkt.  Rohleder und EICTA-Kollegen fordern strafrechlich bewehrten Kopierschutz und Einschr\"{a}nkung der Programmierfreiheit gem\"{a}{\ss} neuer EU-Kopierschutzrichtlinie (EuroDMCA).  Dort, wo die IT-Honoratioren neue Wertsch\"{o}pfungspotentiale wittern, muss der Staat hart durchgreifen, um deren Verwirklichung zu erzwingen.  Ob diese Art der Wertsch\"{o}pfung funktioniert oder produktiv ist, interessiert vorerst nicht.  Der Staat hat so zu tun, als ob sie funktionieren w\"{u}rde und schon heute -- ja sogar r\"{u}ckwirkend f\"{u}r die Vergangenheit -- ordnungspolitische Entscheidungen, z.B. hinsichtlich Pauschalgeb\"{u}hren, an dieser Fiktion auszurichten.
\item[F\"{u}r den Import billiger IT-Fachkr\"{a}fte:]\ die Schr\"{o}der-Ank\"{u}ndigung auf CeBit 2001 st\"{u}tzte sich vor allem auf eine Bitkom-Studie.  Seit dem IT-Konjunktureinbruch im zweiten Halbjahr h\"{o}rt man weniger davon.
\item[Gegen B\"{u}rokratisierung und staatliche Regulierung aller Art:]\ nicht nur hinsichtlich Inderimport:  m\"{o}glichst viele Rechte und wenige Pflichten f\"{u}r Unternehmensvorst\"{a}nde
\item[F\"{u}r B\"{u}rokratisierung und staatliche Regulierung durch Patente:]\ Dieser Standpunkt wir allerdings nicht besonders publik gemacht, er findet sich nur seit kurzem in unauff\"{a}lligen PDF-Dateien hinter kryptischen \"{U}berschriften (``SWP-Gutachten'').
\end{description}

Ein Bitkom-Sprecher kann offenbar fachlich noch so ahnungslos daherreden -- Hauptsache die Gesinnung stimmt.  Solange er seine Forderungen an der Devise ``Mehr Macht f\"{u}r die Vorst\"{a}nde'' ausrichtet, wird er schon nicht zur Rechenschaft gezogen werden.
\end{sect}

\begin{sect}{orgnatur}{Organisationsstruktur}
Im Vorstand von Bitkom sitzen Gesch\"{a}ftsleute von Microsoft (Sibold), IBM, Siemens und anderen Gro{\ss}unternehmen.  Auch in den Arbeitsaussch\"{u}ssen haben vor allem diese Leute die n\"{o}tige Zeit, um die Verbandspolitik zu bestimmen.  Im Vorstand ist keine einzige Person vertreten, die Ansehen als Programmierer oder Informatiker genie{\ss}t.  F\"{u}r die einfachen Mitglieder ist Bitkom weniger eine Interessenvertretung als eine Gelegenheit, gewisse Leistungen in Anspruch zu nehmen, vgl ADAC.
\end{sect}

\begin{sect}{umfrage}{Bitkom-Umfrage 2002}
Im zweiten Halbjahr 2002 erstellte der Teuflesche Arbeitskreis eine unver\"{o}ffentliche Umfrage\footnote{http://swpat.ffii.org/players/bitkom/bitkom-swpat-umfrage030120.pdf}, die von EICTA-Lobbyisten gegen\"{u}ber Euro-Parlamentariern und auch Bundestagsabgeordneten als Grundlage ihrer Behauptung verwendet wurde, die meisten Softwarefirmen wollten Softwarepatente.

Leider enth\"{a}lt der Umfrage-Bericht nicht den Fragebogen, aber immerhin werden einige Fragen indirekt zitiert.

Die Studie stellt zun\"{a}chst eine appetitanregende Grundfrage in vielen Variationen

\begin{itemize}
\item
Glauben Sie, dass Patente dazu beitragen k\"{o}nnen, die Verhandlungsposition eines Unternehmens zu st\"{a}rken?

\item
Glauben Sie, dass Patente Ihnen dabei helfen k\"{o}nnten, Risikokapital zu erhalten?

\item
\dots
\end{itemize}

Die 82 ``Ja'' interpretiert Bitkom dann als ``82\percent{} f\"{u}r Softwarepatente''.  Auch der FFII h\"{a}tte ``ja'' geantwortet.

Eine der Fragen scheint auf eine politische Meinungs\"{a}u{\ss}erung zu zielen.  Bitkom fragt in etwa:

\begin{quote}
{\it Sollten die derzeitigen M\"{o}glichkeiten zum Schutz computer-implementierter Erfindungen erhalten, ausgeweitet oder eingeschr\"{a}nkt werden?}
\end{quote}

Unter 90 befragten Mitgliedsfirmen fallen die Antworten wie folgt aus:

\begin{center}
\begin{tabular}{L{44}R{44}}
ausgeweitet & 39\\
beibehalten & 37\\
eingeschr\"{a}nkt & 10\\
\end{tabular}
\end{center}

Auch der FFII h\"{a}tte ``beibehalten'' geantwortet, und Bitkom h\"{a}tte berichtet ``FFII bef\"{u}rwortet Softwarepatente''.

In der Tat wendet sich niemand gegen Patente auf ``computer-implementierte Erfindungen''.  Allerdings glauben viele von uns noch immer, dass zwar ein chemischer Vorgang oder ein Antiblockiersystem unabh\"{a}ngig davon patentiert werden kann, ob zu seiner Steuerung ein Rechner eingesetzt wird, dass aber die steuernde Software als solche keine Erfindung im Sinne des Patentrechts darstellt.  Diese Regelung wollen wir beibehalten.

Es sollte daran erinnert werden, dass es bereits einige Meinungsumfragen \"{u}ber die Haltung der Softwareunternehmen zu Patenten gibt so etwa die von Effy Oz, Pamela Samuelson, ACM Titus, OTA, USPTO etc in den USA und die von Fraunhofer sowie die Konsultation der Europ\"{a}ischen Kommission hier.  Bei Oz zeigten sich ca 90\percent{} der f\"{u}hrenden Entwickler in gro{\ss}en Softwareh\"{a}usern gegen Softwarepatente.  Beim US-Patentamt war Microsoft das einzige bef\"{u}rwortende Software-Unternehmen.  Auch Fraunhofer stellte st\"{a}rkere Abneigung gegen das Patentwesen fest als Bitkom.  Wen wundert's?
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf http://www.bitkom.org/}}
\filbreak

\item
{\bf {\bf Bitkom-PE 2003-03-22:\footnote{http://www.bitkom.org/index.cfm?gbAction=gbcontentfulldisplay\&ObjectID=D321C079-46F6-4D51-9B08AA448674390A\&MenuNodeID=4C872DB6-8470-4B01-A36FD8C1EBA2E22D}}}

\begin{quote}
Susanne Schopf und Bernd Rohlehder erkl\"{a}ren, ohne Softwarepatente gebe es keine Innovation, m\"{u}sse die Branche ins Ausland wandern, fordern vom Rat, mit dem Europ\"{a}ischen Parlament auf Konfrontationskurs zu gehen oder dessen Entscheidung zu kippen: \begin{quote}
{\it Bitkom setzt sich daf\"{u}r ein, dass der Ministerrat in seiner Stellungnahme nicht den Irrwegen des Parlaments folgt, sondern seinen bisherigen vern\"{u}nftigen Kurs weiterverfolgt und sich im Interesse der europ\"{a}ischen Wirtschaft f\"{u}r einen angemessenen Patentschutz f\"{u}r computerimplementierte Erfindungen einsetzt.}
\end{quote}.

siehe auch Treffen der Patent-Arbeitsgruppe des EU-Ministerrates\footnote{http://swpat.ffii.org/log/03/cons1023/index.en.html}
\end{quote}
\filbreak

\item
{\bf {\bf}}
\filbreak

\item
{\bf {\bf 2002/05 an Bitkom\footnote{http://swpat.ffii.org/briefe/bitk025.de.html}}}
\filbreak

\item
{\bf {\bf Heise: IT-Verband sieht bei Software-Patenten Nachbesserungsbedarf\footnote{http://www.heise.de/newsticker/data/anw-07.05.02-005/}}}

\begin{quote}
Heise-Bericht \texmath{\backslash}``{u}ber die Bitkom-Erkl\texmath{\backslash}``{a}rung
\end{quote}
\filbreak

\item
{\bf {\bf Bitkom zu Softwarepatenten: Beitr\"{a}ge zur Bundestags-Anh\"{o}rung 2001-06-21\footnote{http://swpat.ffii.org/termine/2001/bundestag/bitkom/index.de.html}}}

\begin{quote}
Das Referat hielt die Vorsitzende des Arbeitskreises Gewerblicher Rechtschutz, Frau Dr. Katrin Bremer.  Anwesend war auch PA Fritz Teufel von IBM, der diesen Arbeitskreis bis vor kurzem geleitet hatte.  In ihrem Referat fordert Frau Bremer eine z\"{u}gige Legalisierung von Softwarepatenten durch Anpassung von Art 52 EP\"{U} an die Rechtsprechung des Europ\"{a}ischen Patentamtes und meint, die ``Opensource-Bewegung'' werde es \"{u}berleben, da sie innovativ sei.  In der sp\"{a}ter eingereichten offenbar von PA Teufel geschriebenen schriftlichen Eingabe hei{\ss}t es, freie Software sei nicht innovativ und es seien immer die Nachahmer, die das Patentwesen f\"{u}rchteten.
\end{quote}
\filbreak

\item
{\bf {\bf Kathrin BREMER\footnote{http://swpat.ffii.org/akteure/bremer.de.html}}}
\filbreak

\item
{\bf {\bf PA Fritz Teufel\footnote{http://swpat.ffii.org/akteure/teufel.en.html}}}

\begin{quote}
patent guru of IBM in Germany and Europe, active promoter of software patents, responsible for pushing many landmark cases through the EPO and the German courts.  Ghostwriter of various patent papers of German and European trade associations.  Positions and style well known from public discussions.
\end{quote}
\filbreak

\item
{\bf {\bf Bitkom zu Softwarepatenten: Beitr\"{a}ge zur Bundestags-Anh\"{o}rung 2001-06-21\footnote{http://swpat.ffii.org/termine/2001/bundestag/bitkom/index.de.html}}}

\begin{quote}
Das Referat hielt die Vorsitzende des Arbeitskreises Gewerblicher Rechtschutz, Frau Dr. Katrin Bremer.  Anwesend war auch PA Fritz Teufel von IBM, der diesen Arbeitskreis bis vor kurzem geleitet hatte.  In ihrem Referat fordert Frau Bremer eine z\"{u}gige Legalisierung von Softwarepatenten durch Anpassung von Art 52 EP\"{U} an die Rechtsprechung des Europ\"{a}ischen Patentamtes und meint, die ``Opensource-Bewegung'' werde es \"{u}berleben, da sie innovativ sei.  In der sp\"{a}ter eingereichten offenbar von PA Teufel geschriebenen schriftlichen Eingabe hei{\ss}t es, freie Software sei nicht innovativ und es seien immer die Nachahmer, die das Patentwesen f\"{u}rchteten.
\end{quote}
\filbreak

\item
{\bf {\bf}}
\filbreak

\item
{\bf {\bf BITKOM: Gesundheitsdebatte beenden, UMTS z\"{u}gig ausbauen!\footnote{http://www.heise.de/newsticker/data/ad-12.05.02-000/}}}

\begin{quote}
Ein Bitkom-Sprecher versichert, 20000 Gutachten h\"{a}tten bewiesen, dass Bedenken gegen die UMTS-Strahlen unbelegt seien und warnt vor wirtschaftlichen Sch\"{a}den, welche die anhaltende Gesundheitsdebatte erzeuge.
\end{quote}
\filbreak

\item
{\bf {\bf BITKOM ... eine Lachnummer\footnote{http://www.heise.de/mobil/newsticker/foren/go.shtml?read=1\&msg\_id=1743513\&forum\_id=29250}}}
\filbreak

\item
{\bf {\bf Bitkom-Umfrage: 84\percent{} sehen in Patenten ein geeignetes Mittel zum Schutz computer-implementierbarer Erfindungen\footnote{bitkom-swpat-umfrage030120.pdf}}}

\begin{quote}
Bitkom-Bericht \"{u}ber eine unter Mitgliesunternehmen durchgef\"{u}hrte Umfrage.  Leider fehlt bislang der Fragenkatalog.
\end{quote}
\filbreak

\item
{\bf {\bf GEMA droht mit Millionenklage wegen Abgabe auf CD-Brenner\footnote{http://www.heise.de/newsticker/data/anw-01.03.02-005/}}}

\begin{quote}
Manches mag gegen eine Urheberrechtssteuer auf Drucker und Festplatten sprechen.  Diese Steuer ist jedoch gesetzlich geregelt, und die Argumente mit denen HP und, als deren Sprachrohr, Bitkom sich ihr zu entziehen versuchen, sind weitaus bedenklicher als die Urheberrechtssteuer selber.  Denn Individuelle Verg\"{u}tungsverfahren nach Bitkom-Wunsch \begin{itemize}
\item
sind bislang nirgends in nennenswertem Umfang im Einsatz

\item
f\"{u}hren zu einer Wertminderung dessen, was verg\"{u}tet werden soll

\item
f\"{u}hren ebenso wenig wie andere Verfahren zu ``gerechter Entlohnung von Leistungen''

\item
f\"{u}hren geradeaus zu den Hollywood/Hollings-Gesetzentw\"{u}rfen, gegen die die sich die gesamte amerikanische IT-Welt derzeit auflehnt

\item
f\"{u}hren im Erfolgsfall zu un\"{u}bersehbaren Konfliktszenarien.
\end{itemize}  Wer aufgrund einer Utopie dieser Beschaffenheit im Ton der Entr\"{u}stung gegen die GEMA Sturm laufen zu m\"{u}ssen glaubt, ist wohl einfach nur von allen guten Geistern verlassen.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
Wenn Sie Fragen zum Projekt bitkom\footnote{http://lists.ffii.org/mailman/listinfo/proj/index.html\#bitkom} haben, z\"{o}gern Sie bitte nicht, sich mit bitkom-help@ffii.org in Verbindung zu setzen.

siehe auch Wiki-Seite \"{u}ber Bitkom\footnote{http://kwiki.ffii.org/SwpatbitkomDe}

\begin{itemize}
\item
{\bf {\bf Wie Sie uns helfen k\"{o}nnen, dem Logikpatent-Schildb\"{u}rgerstreich ein Ende zu bereiten\footnote{http://swpat.ffii.org/gruppe/aufgaben.de.html}}}
\filbreak

\item
{\bf {\bf Bitkom-Mitgliedsfirmen kontaktieren}}

\begin{quote}
Kontaktieren Sie bitkom-help@ffii.org, um Adressen und Hinweise zu kontaktierender Bitkom-Firmen zu bekommen.  Sie bekommen innerhalb eines Tages eine Antwort.
\end{quote}
\filbreak

\item
{\bf {\bf Kontaktdaten liefern}}

\begin{quote}
Finden Sie Kontaktdaten, Telefonnummern etc heraus und berichten Sie \"{u}ber Ergebnisse.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/swpatgasnu.el ;
% mode: latex ;
% End: ;

