<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Hoc: Historisches

#Dnr: Die technische Erfindung

#Iia: Informatik und Patentrecht

#DnW: Die Bedeutung der Entscheidung für das künftige Patentrecht

#Esu: Ergebnis und Ausblick

#Ieh: Im Sommer 1974 kam dann die Wende.  Nach einer grundlegend veränderten
Besetzung nahm der 17. Senat des Bundespatentgerichts drei weitere
Sachen zum Anlass, seine bisherige Rechtsprechung zu überprüfen.  Dies
führte zu einer nahezu vollständigen Abkehr von der früheren
Rechtsansicht.  Fassen wir die wesentlichen Ergebnisse dieser drei
Entscheidungen leitsatzartig zusammen, so stellt sich die Meinung des
Senats über die Patentfähigkeit von Vorschriften zur automatischen
Datenverarbeitung nunmehr wie folgt dar:

#Lrh: Lösungsmaßnahmen zur maschinellen Bearbeitung organisatorischer oder
ähnlicher Problemesowie die Angabe von Algorithmen bzw daraus
abgeleiteter Rechenprogramme zur maschinellen Problemlösung durch eine
Datenverarbeitungsanlage bilden jedenfalls dann keine Lehre zu
technischem Handeln, wenn das Gewicht der vorgeschlagenen Maßnahmen
nicht in der die Zweckerreichung erst ermöglichenden konstruktiven
Weiterbildung der zu verwendenden Datenverarbeitungsanlage und/oder
der besonderen Zusammenschaltung, Ausnutzung oder Anpassung ihrer
Baugruppen liegt, sondern die vorgeschlagegenen Maßnahmen sich in der
Angabe des mathematischen Lösungswegs, des Algorithmus oder der daraus
ableitbaren Folge von Teiloperationen erschöpfen und lediglich die
Bearbeitungsmöglichkeiten einer zum Stand der Technik gehörenden
Datenverarbeitungsanlage genutzt werden und die Ausführung aufgrund
der Anlagekonstruktion keine Schwierigkeiten bereitet.

#Vnh: Verfahren, die außer technischen Verfahrensschritten auch solche
Schritte aufweisen, die eine abwägende Verstandestätigkeit des
Menschen erfordern, also nicht nur eine rein mechanische
Geistestätigkeit, die auch ein Automat abnehmen könnte, sind als
Ganzes nicht patentfähig.

#Weh: Wegen der grundsätzlichen Bedeutung der Entscheidungen hat der Senat
in allen drei Fällen die Rechtsbeschwerde zugelassen, die auch
eingelegt worden ist. ...

#Mho: Mit dem Dispositionsprogramm-Beschluss des Bundesgerichtshofs liegt
nun die erste Entscheidung über die drei vom 17. Senat des
Bundespatentgerichts zugelassenen Rechtsbeschwerden und die erste
höchstrichterliche Entscheidung zu den patentrechtlichen Problemen der
ADV überhaupt vor.  Wie die vorgesehene Aufnahme des Beschlusses in
die amtliche Sammlung erkennen lässt, misst ihm der Patentsenat selbst
grundsätzliche Bedeutung zu.  Diese hat der sorgfältig begründete
Beschluss in der Tag.  Weit über die Entscheidung des konkreten Falls
hinaus bildet der Beschluss einen gewichtigen Beitrag zur Fortbildung
des materiellen Patentrechts, der mit seiner im Leitsatz nicht
annähernd reflektierten Fülle von Aspekten und Gedanken auch für die
Anwendung und Auslegung der %(q:europäischen) Regeln über die
Patentfähigkeit des künftigen deutschen Patentrechts uneingeschränkt
Geltung beanspruchen kann.

#Ddt: Den Kern der Entscheidung bilden die Üeberlegungen zum Begriff der
%(q:technischen) Erfindung, zu seiner Abgrenzungsfunktion und
Anwendung auf Lösungsmaßnahmen im Bereich der ADV.  Sodann präsentiert
der Bundesgerichtshof in solcher Klarheit erstmals ausgesprochene
Regeln für die Sachprüfung von Anmeldungen, deren technischer
Charakter zweifelhaft ist.  Von eminent praktischer Bedeutung ist
schließlich der in der Entscheidung umrissene Orientierungsrahmen, wo
für Lehren auf dem Gebiet der Informatik positiv die Zone des
Patentierbaren beginnt.

#Mna: Mit dem Dispositionsprogramm-Beschluss legalisiert der
Bundesgerichtshof gewissermaßen eine seit seiner
Rote-Taube-Entscheidung ständige Praxis: Die Übernahme der dort
gegebenen Definition der technischen Erfindung als gleichsam
universaler Formel für die Prüfung, ob eine Erfindung im Rechtssinn
vorliegt, welchem spezifischen Bereich menschlichen Wirkens sie auch
immer zugehören mag.  Denn damals hatte der Bundesgerichtshof seine
inzwischen berühmte Definition der technischen Erfindung als %(q:Lehre
zum planmäßigen Handeln unter Einsatz beherrschbarer Naturkräfte zur
Erreichung eines kausal übersehbaren Erfolgs) aus der Frage heraus
entwickelt, ob Erscheinungen und Vorgänge in der belebten Natur unter
einen zeitgemäß verstandenen Begriff der Technik zu subsumieren sind,
und ihre Geltung grundsätzlich auf diesen Bereich beschränkt.  Obwohl
der Bundesgerichtshof diese Formel, wie aus den Gründen klar
hervorgeht, nicht ohne weiteres auch für die Abgrenzung der Technik
von der Welt des rein Geistigen verwendet wissen wollte, hat z.B. der
17. Senat des Bundespatentgerichts sie in der Benson-Entscheidung als
auch für dieses Abgrenzungsproblem maßgeblich zugrunde gelegt und die
Patentfähigkeit des streitigen Verfahrens gerade daraus hergeleitet. 
Dass der Bundesgerichtshof die Rote-Taube-Formel der technischen
Erfindung nunmehr für %(q:allgemeinverbindlich) erklärt hat, ist zu
begrüßen, jedenfalls was ihren Kernbestandteil angeht, d.h. die
Umschreibung des Technischen als eines Gebiets, auf dem beherrschbare
Naturkräfte eingesetzt werden.

#Dhu: Die Lehre zum technischen Handeln und ihre Elemente

#Drv: Die Regeln für verstandesmäßige Tätigkeit

#cra: %(q:Fertige) Lehre und Unmittelbarkeitsgrundsatz

#Pnt: Prüfungsmethode

#UWR: Unter allen bekannten Ansätzen erscheint die Rote-Taube-Formel nach
dem derzeitigen Stand der Erkenntnisse als am besten geeignet und
flexibel genug, eine der technischen, wissenschaftlichen und
gesellschaftlichen Entwicklung Rechnung tragende, sachgerechte
Abgrenzung patentfähiger Lehren von nicht schutzfähigen Vorschlägen zu
ermöglichen.

#DAe: Die Versuche des Reichsgerichts und des Reichspatentamts, der Technik
definitorisch Herr zu werden, stellten entweder allein auf Physik und
Chemie ab oder sahen das Wesen des Technischen allzu eng nur in seinem
Gegensatz zur Welt des Geistigen.  Die häufig zitierte Definition der
technischen Erfindung aus der Wettschein-Entscheidung des
Bundesgerichtshofs, wonach unter Erfindung %(q:eine angewandte
Erkenntnis auf technischem Gebiet) zu verstehen ist, also eine
%(q:Anweisung, mit bestimmten technischen Mitteln zur Lösung einer
technischen Aufgabe ein technisches Ergebnis zu erzielen), nennt zwar
die notwendigen Elemente der Regel zu technischem Handeln, bleibt aber
weitgehend eine Antwort darauf schuldig, was denn nun Technik im
patentrechtlichen Sinn sei.  Immerhin findet sich in der Begründung
mit der beiläufigen Erwähnung der %(lf:Lindenmaier'schen Formel), dass
die Technik sich als %(q:Benutzung von Kräften oder Stoffen der
belebten oder unbelebten Natur) darstelle, ein richtungsweisender
Ansatz.  Auch die kürzlich zum Gebrauchsmusterrecht ergangene
Buchungsblatt-Entscheidung vermittelt über das spezielle Problem der
Beurteilung von Flächenmustern hinaus keine tieferen Einsichten in das
Wesen der Technik.  Die vom 17. Senat des Bundespatentgerichts im
Typensatz-Beschluss vorgeschlagege Umschreibung des Technischen als
%(q:Welt der Dinge) ist blass und für Abgrenzungszwecke kaum
brauchbar.

#SWh: Solche Schwächen haften der Rote-Taube-Formel nicht an, soweit sie das
Gebiet der Technik im patentrechtlichen Sinn als durch den %(e:Einsatz
beherrschbarer Naturkräfte) gekennzeichnet begreift und diesen zur
unabdingbaren Voraussetzung der patentfähigen Erfindung erhebt.  Sie
erlaubt einerseits sowohl die Einbeziehung anderer Naturkräfte als
physikalischer und chemischer wie auch bisher unbekannter Naturkräfte,
schließt andererseits aber die menschlichen Verstandeskräfte als
solche Naturkräfte aus.  Wollte man die menschliche
Verstandestätigkeit mit Rücksicht auf die primär biologisch-chemischen
Vorgänge, die die gesamte Tätigkeit des Gehirns regeln und
beeinflussen, den beherrschbaren Naturkräften zurechnen, so würde dies
nichts anderes bedeuten als eine schrankenlose Öffnung des
Erfindungsbegriffs, mithin eine Eröffnung des Patentschutzes für
schlechthin alle Ergebnisse menschlichen Wirkens.  Denn die beiden
anderen Elemente der Rote-Taube-Formel, die %(q:Planmäßigkeit) und
%(q:kausale Übersehbarkeit), könnten dieses Ergebnis in aller Regel
nicht korrigieren, weil auch Regeln für geistige Tätigkeiten sich
nahezu immer als Anweisungen für planmäßiges Handeln darstellen und
kausal übersehbar sind.  Für die der streitigen Anmeldung
zugrundeliegende Organisations- und Rechenregel räumt der
Bundesgerichtshof dies ausdrücklich ein.  Daher drängen sich Zweifel
auf, ob diese beiden Komponenten des Beggriffs der technischen
Erfindung noch eine eigenständige, d.h. abgrenzungsrelevante Funktion
haben.

#Dei: Damit ist die entscheidende Aussage für eine sachgerechte und
zuverlässige Abgrenzung der nicht-technischen Schöpfungen gewonnen. 
Die beherrschbaren Naturkräfte sind ein Oberbegriff für alle
Erscheinungen und Vorgänge, die durch eine nach unseren Erfahrungen
und Erkenntnissen zwingende --- wenn auch nicht notwendigerweise stets
erkannte --- Gesetzmäßigkeit zwischen Ursache und Wirkung
charkterisiert sind.  Andere Formeln zur näheren Kennzeichnung der
technischen Lehre im patentrechtliche Sinn wie etwa %(q:Nutzung von
Materie und/oder Energie) oder %(q:Beeinflussung des Wechselspiels von
Stoffen, Kräften und Energiearten) sind inhaltlich nichts anderes als
definitorische Synonyme für eben diese beherrschbaren Naturkräfte. 
Den Einsatz beherrschbarer Naturkräfte zur unabdingbaren Voraussetzung
der patentfähigen Erfindung zu erheben, trifft daher den Nagel auf den
Kopf.

#Dea: Die menschliche Verstandestätigkeit gehört jedenfalls nach den
Anschauungen unserer Zeit nicht zu den %(e:beherrschbaren)
Naturkräften.  Auch wenn die bei geistiger Tätigkeit ablaufenden
physiologischen, kybernetischen und informatorischen Prozesse von
Naturkräften, z.B. biologischen oder chemischen Kräften, beeinflusst
werden, so ist -- unbeschadet der weitgehend noch fehlenden Erkenntnis
der Kausalzusammenhänge -- die menschliche Verstandestätigkeit, das
Denken in seiner Gesamtheit, eine andere Dimension, deren
%(q:Gesetzlichkeit) zwar gegeben sein mag, die aber nicht als
%(q:beherrschbar) im naturgesetzlichen Sinn angesehen und %(e:bewusst)
den in der Natur waltenden Kräften gegenübergestellt wird.  Wenn der
Bundesgerichtshof das menschliche Denken nicht dem Begriff der Technik
zuordnen will, weil dieser damit seiner %(q:spezifischen und
unterscheidenden Bedeutung) beraubt würde, so ist das keine
willkürliche inhaltliche Begrenzung dieses Begriffs für den Bereich
des Patentrechts, sondern eine konsequente Übernahme der Anschauungen,
die sich in Naturwissenschaften und Technik selbst entwickelt haben. 
Naturwissenschaftliches und technisches Denken wie überhaupt
außerrechtliche Betrachtungsweisen präjudizieren zwar nicht die
Auslegung von Rechtsbegriffen, sollten aber ohne Not nicht beiseite
geschoben werden, zumal wenn es sich wie hier beim Patentrecht um ein
Teilsystem des Privatrechts handelt, das für ein klar umrissenes
Gebiet meschlichen Wirkens, nämlich die industrielle Güterproduktion
im weitesten Sinne, konzipiert worden ist und seine Ausformung bis
heute durch ständige Interaktion mit der technischen Wirklichkeit und 
Gedankenwelt erfahren hat.  Dies besagt freilich nicht --- um
Missverständnissen vorzubeugen ---, dass der Richter dann auch
blindlings zu akzeptieren hätte, was nach den in der Technik selber
herrschenden Anschauungen als dem Gebiet der Technik zugehörend
qualifiziert wird.

#EiW: Eine undifferenzierte Einbeziehung aller Lehren, die eine geistige
Tätigkeit des Menschen zum Gegenstand haben, in den Kreis der dem
Patentschutz zugänglichen Neuerungen wäre daher nur dann möglich, wenn
auf das Erfordernis, dass die patentfähige Erfindung eine Lehre zu
technischem Handeln darstellen muss, gänzlich verzichtet würde. ...

#lei: ... Überzeugender wäre in diesem Zusammenhang ein Rückgriff auf den
historischen Gesetzgeber gewesen, der zwar nie von %(q:Technik) oder
gar %(q:technischer Erfindung) spricht, der aber, wie dies auch aus
der in der %(mp:Gesetzesbegründung) -- freilich mit Rücksicht auf das
heute als nicht mehr einschlägig erachtete Merkmal der gewerblichen
Verwertbarkeit -- gezogenen Grenzlinie zwischen  patentfähigen und
nicht patentfähigen Lehren hervorgeht, den Patentschutz schöpferischen
Leistungen im Bereich der Technik, der gewerblichen Gütererzeugung,
vorbehalten wollte.  Jedenfalls hege ich keinen Zweifel daran, dass
sich die ständige Praxis der Patentbehörden und Gerichte, als
patentfähig nur eine technische Erfindung anzuerkennen, aufgrund einer
nahezu einmütigen Überzeugung und Billigung aller beteiligten Kreise
über einen Zeitraum von nunmehr fast 100 Jahren zu einem
gewohnheitsrechtlichen Grundsatz verfestigt hat.  Insoweit wäre allein
der Gesetzgeber befugt, diese Patentierbarkeitsvoraussetzung zu
beseitigen.  Solchen Willen hat aber der Bundestag auch bei der
jüngsten Revision des Patentgesetzes durch das Gesetz über
internationale Patentübereinkommen vom Juni 1976 nicht zu erkennen
gegeben.

#Mrr: Motive zum Patentgesetz, Verhandlungen des Deutschen Reichstags, 1877,
Drucksache Nr. 8 S. 17 und Nr. 144 S. 5.  Auf den historischen
Gesetzgeber beruft sich der BGH aber in der Wettschein-Entscheidung,
GRUR 1968, 602

#Deu: Der Bundesgerichtshof stützt sein Festhalten am Erfordernis des
technischen Charakters der patentfähigen Erfindung schließlich darauf,
dass der Begriff der Technik das %(q:einzig brauchbare
Abgrenzungskriterium) gegenüber anderen Leistungen des Menschen sei
und ein völliger Verzicht darauf zu einer Überschneidung mit dem
sachlichen Geltungsbereich anderer Schutzsysteme führen müsse.  Dieses
Argument ist in jeder Hinsicht überzeugend, selbst wenn man
berücksichtigt, dass auch das Urheberrecht der ineinem Werk
enthaltenen geistigen Leistung als solcher, z.B. einer neuen
mathematischen Formel oder einem wissenschaftlichen Lehrsatz, keinen
Schutz bietet.  Denn obwohl wir über ein recht umfassendes System von
Ausschließlichkeitsrechten verschiedenster Art verfügen, bedeutet dies
keineswegs, dass jeder schöpferischen menschlichen Leistung in
irgendeinem dieser Schutzsysteme ein Platz eingeräumt werden müsste. 
Würden nun aber die Lehren für verstandesmäßige Tätigkeiten
schlechthin der Technik zugerechnet, wäre eben dies der Fall.  Das
Patentgesetz würde, wie der Bundesgerichtshof es plastisch formuliert,
zum %(q:Auffangbecken) für den Schutz jedweder geistigen Leistung,
sofern sie nur neu und erfinderisch wäre.  Eine solche, auch nur
mögliche Monopolisierung des Denkens ist uns fremd.  Schließlich ist
ein lückenloses System des Ideenschutzes weder aus
Gerechtigkeitsgründen geboten noch wäre es segensreich.  Denn selbst
wenn viele Anmeldungen an den Hürden der Neuheit oder Erfindungshöhe 
zu Fall kommen sollten, könnte kaum verhindert werden, dass ein
qualitativ und quantitativ beträchtlicher Anteil von
Geistesschöpfungen den Patentschutz erhalten würde, dessen
Auswirkungen aber kaum zuverlässig abschätzbar sind und der mit der
Sperrwirkung technischer Patente nicht vergleichbare
Behinderungseffekte nach sich ziehen könnte.  Dass das im Ergebnis
demnach berechtigte Festhalten am Erfordernis der %(q:technischen
Erfindung) nicht verbietet, den Bereich des Technischen selber im
Gefolge veränderter Anschauungen und  Verhältnisse behutsam zu
erweitern und damit den Patentschutz für bisher nicht als technisch
angesehene Neuerungen zu eröffnen, versteht sich von selbst.  Des
Bundesgerichtshofs %(q:Rote Taube) ist ein Paradebeispiel dafür.

#UWL: Um nun herausfiltern zu können, ob eine Lehre, die irgendwo auch den
Einsatz technischer Mittel vorschlägt, tatsächlich technischer Natur
ist oder in Wahrheit nur eine Anweisung für eine rein geistige
Tätigkeit darstellt, führt der Bundesgerichtshof den interessanten
Aspekt der %(q:fertigen Regel) ein.  Dieser Gedanke hat nichts mit der
Frage nach dem Fertigsein der Erfindung zu tun, also ob etwa die
Notwendigkeit weiterer Versuche der Patentierbarkeit einer Erfindung
entgegensteht.  Er betrifft vielmehr allein das Problem der Kausalität
des Mitteleinsatzes für den nach der Aufgabenstellung der Erfindung
bezwecken Erfolg.  Hinter der Feststellung, dass die Anwendung einer
als Erfindung beanspruchten Regel bzw Problemlösung, die bereits
%(q:fertig) oder besser bereits vollzogen ist, %(e:bevor) es zu einem
technischen Handeln kommt, nicht die Benutzung beherrschbarer
Naturkräfte erfordert und daher der nachfolgende Einsatz technischer
Mittel die beanspruchte Lehre grundsätzlich nicht in den Rang einer
technischen Regel zu erheben vermag, verbirgt sich daher nichts
anderes als die richtige Erkenntnis, dass eine Lehre eben nur dann
technisch ist, wenn sie einen %(e:unmittelbaren) Einsatz
beherrschbarer Naturkräfte erfordert, d.h. der Kausalzusammenhang
zwischen Mitteleinsatz und Erfolg nicht unterbrochen ist.  Später
präzisiert der Bundesgerichtshof denn auch seine Überlegung
dahingehend, dass die Verwendung technischer Mittel %(q:Bestandteil
der Problemlösung selbst) sein müsse und nicht entfallen dürfe, ohne
dass zugleich der angestrebte Erfolg entfiele, und führt damit das
strenge Äquivalenzprinzip der strafrechtlichen Kausalitätstheorie in
das Patentrecht ein.

#Bfw: Begreift man das Wesen der technischen Erfindung als Lehre zum Einsatz
beherrschbarer Naturkräfte, dann kann auf die weitere Bedingung, dass
die erfindungsgemäße Regel unmittelbar die Benutzung solcher
Naturkräfte zum Gegenstand haben und unmittelbar zum bezweckten Erfolg
führen muss, nicht verzichtet werden.  Würde es nämlich für die
Patentfähigkeit ausreichen, wenn im Zusammenhang mit der beanspruchten
Lehre irgendwo auch technische Mittel benutzt werden, so würde der
Begriff der Technik wieder %(q:seiner spezifischen und
unterscheidenden Bedeutung) entkleidet und wäre damit als
Abgrenzungsinstrument praktisch wertlos. ...

#EWn: Einer der Hauptstreitpunkte bei der patentrechtlichen Diskussion war
und ist die Frage, welche Bedeutung dem Umstand zukommt, dass
datenverarbeitungsgerecht konzipierte, d.h. algorithmisierte
Rechenvorschriften ohne weiteres automatisierbar sind, also mit Hilfe
eines Universalrechners nach entsprechender Programmierung ausgeführt
werden können.  Jene, die solche Regeln als dem Gebiet der Technik
zugehörig erachten, leiten dies aus dem Umstand her, dass die
algorithmisierte Problemlösung ohne weiteres automatisierbar ist, nach
entsprechender Umsetzung in ein Programm %(q:vollautomatisch und ohne
weitere Einwirkung oder Mithilfe geistiger menschlicher Kräfte)
abläuft.  Auch in anderem Zusammenhang neigt die Praxis dazu, dem
Kriterium der Automatisierbarkeit %(q:patentbegründende) Bedeutung
zuzuerkennen.  Jene hingegen, die die Patentfähigkeit von primär
verstandesmäßig vorzunehmenden, gleichwohl automatisierbaren
Operationen verneinen, argumentieren nicht zuletzt damit, dass der
durch die Benutzung des Computers ermöglichte automatische
Prozessablauf durch eine eigenständige Kausalkette in Gang gesetzt
wird, also nicht unmittelbare Folge der algorithmisierten
Problemlösung ist.

#WdW: Wird der Unmittelbarkeitsgrundsatz ernst genommen, so kann nicht
zweifelhaft sein, welche Betrachtungsweise die richtige ist.  Zwischen
der Rechenregel und der sich anschließenden automatischen Lösung mit
Hilfe des Computers besteht kein die Unmittelbarkeit begründender
Kausalzusammenhang.  Daran ändert sich auch dann nichts, wenn die
Verwendung einer Datenverarbeitungsanlage zweckmäßig ist oder gar --
wie dies bei den algorithmisierten Rechenvorschriften nahezu immer der
Fall ist -- ihre Benutzung die einzige Möglichkeit darstellt, die
jewilige Aufgabe überhaupt oder wenigstens in wirtschaftlich
sinnvoller Weise zu lösen. ...

#Git: Grundsätzlich besteht jedoch kein Anlass, das Unmittelbarkeitsprinzip
aufzugeben.  Eine bereinigte, von Ballast befreite Definition der
technischen Erfindung würde daher lauten:

#Dre: Die technische Erfindung ist eine Lehre, wie beherrschbare Naturkräfte
unmittelbar zur Erzielung eines bestimmten Ergebnisses eingesetzt
werden sollen.

#Dtu: Die so herausgearbeitete Definition der technischen Erfindung kann
ihre Abgrenzungsfunktion in der Praxis nur dann voll entfalten, wenn
die Prüfung von Anmeldungen, deren technischer Charakter zweifelhaft
ist, nicht rein schematisch-formal erfolgt, sondern der Eigenart
solcher Anmeldungen, deren Ansprüche stets auch eine Reihe technischer
Merkmale enthalten, Rechnung trägt.   ... In seinen wenigen zum
Problemkreis der technischen Erfindung ergangenen Entscheidungen hat
der Bundesgerichtshof zur Methode der Prüfung bisher nie Stellung
genommen.  Im Dispositionsprogramm-Beschluss postuliert er nun
erstmals für die Prüfung von Anmeldungen, deren Zugehörigkeit zum
Gebiet der Technik nicht auf der hand liegt, zwei Regeln, die von
großer praktischer Bedeutung sind und ungeteilte Zustimmung verdienen.

#Dde: Die %(e:erste Regel) besagt, dass solche Anmeldungen ungeachtet der
Formulierung der Ansprüche und -- auch wenn dies nicht explicite
ausgesprochen ist -- der gewählten Anspruchsart auf ihren wirklichen,
materialen Gehalt zu untersuchen sind.  ... Formulierungskünste wie
%(q:Datenverarbeitungsanlage ... dadurch gekennzeichnet, dass sie nach
folgender Formel programmiert ist), %(q:Schaltungsanordnung ...),
%(q:Verfahren zur Steuerung einer Datenverarbeitungsanlage ...),
%(q:Verwendung einer Datenverarbeitungsanlage mit den Merkmalen X, Y,
Z zu ...) usw werden also in Zukunft einer Anmeldung nicht mehr zum
Patentschutz verhelfen können, wenn sich dahinter nur eine
nicht-technische Rechenvorschrift verbirgt.  ...

#AAe: Aus dieser grundsätzlich materialen Betrachtung erbibt sich beinahe
zwanglos die %(e:zweite), noch wichtigere %(e:Prüfungsregel), dass
Anmeldungen, die sowohl technische als auch nicht-technische Merkmale
enthalten, nur dann patentfähig sind, wenn das als neu und
erfinderisch Beanspruchte, also der Kern der Erfindung, im Technischen
liegt.  In der Formulierung des Bundesgerichtshofs liest sich dies so:

#led: ... technische Merkmale in dem Patentanspruch können nur dann eine
Patentierung rechtfertigen ..., wenn sich die erfinderische Neuheit in
ihnen niederschlägt, nicht jedoch dann, wenn die Erfindung auf den
nicht-technischen Teil der Lehre ... beschränkt bleibt.

#Snf: So verwundert es nicht, wenn in der jüngeren Diskussion um die
Schutzfähigkeit der Software ein Begriff wie %(q:Software-Engineering)
auftaucht, der die patentrechtliche Wertung gewissermaßen schon
vorwegnimmt.  Solche Neuschöpfungen ändern freilich nichts daran, dass
die Informatik mit ihren Arbeitsergebnissen unter
immaterialgüterrechtlichem Blickwinkel im Grenzgebiet von Urheberrecht
(Geisteswissenschaften), Patentrecht (Technik) und Niemandsland liegt
und es daher einer sorgfältigen tatsächlichen und rechtlichen Analyse
bedarf, ob eine konkrete Software-Schöpfung in den einen oder anderen
Bereich fällt.

#Dka: Die Dispositionsprogramm-Entscheidung und ihre patentrechtliche
Würdigung

#Pri: Patentschutz für Software: Bleibt noch ein Weg?

#HWd: Hierfür zieht der Bundsgerichtshof zwei Gesichtspunkte heran, die auch
in der bisherigen Diskussion um den Patentschutz für
Software-Schöpfungeneine hervorragende Stellung eingenommen haben: Zum
einen die Überlegung, dass solche algorithmisierten, auf
ADV-Unterstützung zugeschnittenen Organisations- und Rechenregeln im
Zusammenhang mit den sog. %(q:Gebrauchsanweisungen) zu sehen sind, zum
anderen den sich im Hinblick auf die Interdependenz von Hardware und
Software aufdrängenden Aspekt, dass sich eine Korrektur deshalb als
notwendig erweisen könnte, weil zwischen dem jeweiligen Algorithmus
bzw Programm und den dadurch ausgelösten Schaltvorgängen in der
Datenverarbeitungsanlage eine enge Korrelation besteht, die eine
Äquivalenz von dauerhafter Festschaltung in Form eines Spezialrechners
und programmeirtem Betrieb eines Universalrechners nahelegt.

#Dae: Die Gebrauchsanweisungen

#DWh: Das Verhältnis von Programm und maschineller Schaltung

#DWz: Die Überlegung, dass jedem Computerprogramm und weitergehend sogar
einer algorithmisierten Rechenvorschrift ein bestimmter Schaltzustand
bzw eine bestimmte Schaltfolge im Computer entspreche, die ihrerseits
nicht anders behandelt werden dürften als eine Festschaltung in Form
eines Spezialrechners für eben dieses Programm oder diesen
Algorithmus, beruht in ihrem Kern auf der wohlbekannten
Äquivalenzlehre und ist schon deshalb anfechtbar.  Die
patentrechtliche Äquivalenztheorie ist als Hilfsmittel für die
Feststellung der Erfindungshöhe und des Schutzumfangs einer
patentierten technischen Lehre entwickelt wordne.  Ihre Anwendung bei
der Prüfung, ob ein konkreter Vorschlag dem Patentschutz überhaupt
zugänglich, also technischer Natur ist, verbietet sich schon deshalb,
weil Prüfungsgegenstand hier allein das ist, was %(e:tatsächlich
beansprucht und offenbart) ist, nicht aber das, was sein könnte.  Aber
selbst wenn die Lehre von den patentrechtlichen Gleichwerten auch in
diesem Zusammenhang brauchbar wäre, so könnte sie deie ihr zugedachte
Stützungsfunktion nur dann erfüllen, wenn ein Durchschnittsfachmann
aus der bloßen Offenbarung des erfindungsgemäßen Algorithmus bzw eine
speziellen Programms ohne weiteres und unmittelbar entnehmen könnte,
wie die entsprechende schaltungstechnische Lösung z.B. durch den
Aufbau eines Spezialrechners zu bewerkstelligen wäre.  Das ist aber,
wie der 17. Senat selber nunmehr ausdrücklich und völlig zu Recht
einräumt -- abgesehen vielleicht von ganz trivialen Fällen --, ohne
erfinderisches Zutun nicht möglich.

#TfW: Trotz der -- aus Sicht der Informatik -- grundsätzlich bestehenden
Austauschbarkeit von Hardware und Software, die zur Annahme auch einer
patentrechtlichen Gleichwertigkeit verführt, sind
Computerprogrammierung und Computer-Engineering nach
Ausgangssituation, Arbeitsweise und verwendeten Mitteln zwei Paar
Stiefel.  Wie der Informatiker aus der Offenbarung eines komplexen
Spezialschaltwerks nicht ersehen kann, wie er einen Universalrechner
zu programmieren hätte, kann der Computeringenieur aus der bloßen
Offenbarung eines Programms oder gar nur eines Algorithmus nicht
ableiten, wie er einen Spezialrechner konstruieren müsste.  Da der
Algorithmus als solcher niemals geschützter Gegenstand der Erfindung
wäre, sondern nur die spezielle Vorrichtung mit ihren
baulich-funktionellen Merkmalen, wäre im übrigen für die Patentprüfung
wahre Aktrobatik vonnöten:  aus der Angabe des Algorithmus bzw der
darauf aufbauenden spezifischen Steueranweisung in Form eines
Programms projiziert der Prüfer als Durchschnittsfachmann gedanklich
die ihm ja ohne weiteres mögliche Spezialschaltung, die er
anschließend mit den im Stand der Technik vorgefundenen Schaltwerken,
Computern etc. vergleicht!  Dass die Annahme einer generellen
Äquivalenz ein Irrweg ist, lässt sich schließlich auch am umgekehrten
Fall demonstrieren:  sie hätte nämlich zur Folge, dass jede
Spezialschaltung mangels Erfindungshöhe nicht patentierbar wäre, wenn
ein Universalrechner entsprechend programmiert werden könnte.  Das ist
aber in aller in aller Regel der Fall.

#DhW: Der Bundesgerichtshof gelangt zu der Schlussfolgerung:

#DWg: Die Lehre, eine Datenverarbeitungsanlage nach einem bestimmten
Rechenprogramm zu betreiben, kann .. nur patentfähig sein, wenn das
Programm einen neuen, erfinderischen Aufbau einer solchen Anlage
erfordert und lehrt oder wenn ihm die Anweisung zu entnehmen ist, die
Anlage auf eine neue, bisher nicht übliche und auch nicht naheliegende
Art und Weise zu benutzen.

#Wlg: Wie aus dieser Formulierung und aus anderen Stellen der
Entscheidungsbegründung hervorgeht, wird also die Grenze zur
Patentierbarkeit erst in der Stufe der eigentlichen Programmierung
überschritten, nicht schon im Bereich der dem jeweiligen Programm
zugrundeliegenden algorithmischen Problemlösung.  Nach Auffassung des
Bundesgerichtshofs wird das %(q:Reich des Technischen) erst mit der
%(q:auf dem Algorithmus beruhenden Anweisung für den Betrieb) einer
Datenverarbeitungsanlage betreten.  Die Unterscheidung zwischen der
algorthmischen Problemlösung einerseits und ihrer Programmierung
andererseits ist berechtigt.  Wie bereits früher dargelegt, ist ein
Computerprogramm als fertige maschinengerechte Betriebsvorschrift
%(e:nicht) gleichbedeutend mit dem Algorithmus, auch wenn dieser das
Gewerbe des konkreten Programms bildet.  Selbst wenn ein
durchschnittlicher Programmierer befähigt ist, einen vorgegebenen
Algorithmus in eine Steueranweisung für eine bestimte
Datenverarbeitungsanlage umzusetzen, so enthält die Rechenregel nicht
auch und zugleich jede denkbare und mögliche Programmiervorschrift. 
Vielmehr besteht in der Stufe der Programmierung ein wesentlicher
Spielraum für mehr oder minder optimale, einfache und elegante
Lösungen.  Algorithmus und Programm wären selbst dann nicht
gleichzusetzen, wenn der Algorithmus unmittelbar in die Maschine
eingegeben werden könnte, da in diesem Fall die
Programmierungsfunktion von der Hardwarelogik, dem Betriebssystem und
der Dienstsoftware übernommen werden, ohne dass dies am Charakter des
Algorithmus etwas ändert.

#Aot: Algorithmen

#Cto: Computerprogramme

#Fsi: Für die Informatiker ist dieses Ergebnis allerdings mehr als
betrüblich.  Tatsächlich stehen sie vor einem Trümmerhaufen, da sie
für ihr schönstes Kind, die Algorithmen, praktisch keinerlei
Ausschließlichkeitsschutz erhalten können.

#Des: Das klingt zunächst paradox, sind doch die Algorithmen der eigentlich
schöpferische Beitrag der Informatik oder, wie dies in der
patentrechtlichen Diskussion betont worden ist, der %(q:allgemeine
Erfindungsgedanke). ...  Wenn nun die Informatiker insbesondere auf
eine schlaltungstechnische Realisierung neuer oder verbesserter
Algorithmen verwiesen werden, so ist das wenig tröstlich für sie. 
Denn abgesehen davon , dass die Hardware-Lösung allenfalls einen
mittelbaren Algorithmenschutz bewirken kann, wird sie oft überaus
aufwendig, umständlich, teuer und vor allem im Hinblick auf die durch
den Algorithmus ermöglichte Programmierbarkeit eines beliebigen
Universalrechners in vielen Fällen geradezu unsinnig sein.  Da der
Urheberrechtsschutz für Algorithmen per se ebenso versagt und
wettbwerbsrechtliche Ansprüche nur ausnahmsweise durchgreifen werden,
sind die Informatiker letztlich ins Niemandsland geworfen.

#Smf: Sucht man nach Möglichkeiten, wie den Algorithmen dennoch der
Patentschutz auf breiter Basis offengehalten werden könnte, so wird
nur ein einziger -- wie die Anmeldepraxis zeigt, stets erhoffter --
Ausweg aus diesem Dilemma sichtbar: Auf der einen Seite eine Lockerung
des Unmittelbarkeitsgrundsatzes, wonach die technische Lehre
unmittelbar zum Einsatz beherrschbarer Naturkräfte führen muss, auf
der anderen Seite, gewissermaßen als Tribut für diese Wohltat, eine
Beschränkung des sachlichen Schutzbereichs patentierter Algorithmen
auf eine maschinelle Anwendung bzw Ausführung oder, weitergehend, auf
die Anwendung mit einer spezifischen Maschinenkonfiguration.

#Feu: Für eine solche Liberalisierung mag insbesondere sprechen, dass die
meisten -- namentlich anwendungsorientierte -- Algorithmen
ausschließlich im Zusammenhang mit einer Computeranwendung brauchbar
und nützlich sind.  ...

#Nlb: Nun, der Bundesgerichtshof hat sich, wie aus der eigentlichen
Entscheidungsbegründung sowie den Überlegungen zu den Möglichkeiten
einer Erweiterung des Begriffs der Technik oder eines völligen
Verzichts hierauf klar hervorgeht, dieser Betrachtungsweise
verschlossen und wird sich wohl auch in Zukunft kaum für sie erwärmen.
 Auch dafür gibt es gute Gründe.  Zunächst würde ein weniger streng
gehandhabter Unmittelbarkeitsgrundsatz, selbst wenn man ihn nur den
datenverarbeitungsgerechten Rechenvorschriften zugutekommen lassen
wollte, auf längere Sicht mit hoher Wahrscheinlichkeit dazu führen,
dass die Patentierbarkeitsvoraussetzung der technischen Lehre ihre
Abgrenzungsfunktion einbüßte und allen Lehren für verstandesmäßige
Tätigkeiten Schritt für Schritt der Patentschutz eröffnet würde.  Vor
allem aber sind erhebliche Zweifel angebracht, ob eine solche Ausnahme
für das Gebiet der Informatik überhaupt vertretbar ist.  Die ADV ist
heute zu einem unentbehrlichen Hilfsmittel in allen Bereichen der
menschlichen Gesellschaft geworden und wird dies auch in Zukunft
bleiben.  Sie ist ubiquitär.  Ist die Hardware-Industrie noch relativ
leicht eingrenzbar, so gilt dies nicht mehr für die
Software-Industrie, wo die Software-Hersteller ebenso sehr Anwender
sind wie die überall zu findenden Software-Nutznießer auch Hersteller.
 Ihre instrumendale Bedeutung, ihre Hilfs- und Dienstleistungsfunktion
unterscheidet die ADV von den enger oder weiter begrenzten
Einzelgebieten der Technik und ordnet sie eher solchen Bereichen zu
wie z.B. der Betriebswirtschaft, deren Arbeitsergebnisse und Methoden
-- beispielsweise auf den Gebieten des Managements, der Organisation,
des Rechnungswesens, der Werbung und des Marketings -- von allen
Wirtschaftsunternehmen benötigt werden und für die daher prima facie
ein Freihaltungsbedürfnis indiziert ist.  Nach welchen Seiten hin auch
immer aber ein patentrechtlicher Algorithmenschutz begrenzt würde --
durch die Bindung an eine bestimmte Maschinenkonfiguration oder sogar
an eine ganz bestimmte Datenverarbeitungsanlage, durch den
Anwendungszweck und das Anwendungsgebiet --, so ist doch die Gefahr
offensichtlich, dass dieser Schutz eine weit über das mit
herkömmlichen technischen Schutzrechten verbundene Maß hinausgehende
Sperrwirkung entfalten und die Benutzung von Datenverarbeitungsanlagen
nachhaltig blockieren könnte.  Das zugunsten der Patentfähigkeit von
Algorithmen oft gehörte Argument, dass diese nur in Verbindung mit
digitalen Rechenautomaten nützlich sind, hat eine Kehrseite, weil sich
eben gerade aus dieser Tatsache ergeben kann, dass der patentierte
Algorithmus dann die Benutzung von Computern überhaupt verwehrt, weil
eine Substitution durch andere Mittel oder einen anderen Algorithmus
nicht möglich oder nicht zumutbar ist.  So besehen erscheint der
Denkansatz einer notwendigen %(q:Vergesellschaftung) der Informatik,
zumindest der Algorithmen, durchaus plausibel, will man nicht den im
Gefolge eines Algorithmenschutzes wahrscheinlichen ungeheueren
privaten Machtzuwachs -- naiv oder bewusst -- leugnen.

#DiW: Diese wenigen Gesichtspunkte zeigen, dass die Beantwortung der Frage,
ob und in welchem Umfang datenverarbeitungsgerecht konzipierte Rechen-
und Organisationsregeln und andere Lehren für verstandesmäßige
Tätigkeiten einen Ausschließlichkeitsschutz genießen sollen, Aufgabe
der Rechtspolitik ist.  Jedenfalls kann und darf es nicht Aufgabe der
Gerichte sein, im Rahmen eines Einzelfalls eine rechtstechnisch und
rechtspolitisch zwar vertretbare, in ihren Konsequenzen aber nicht
überschaubare Entscheidung zu treffen.  Das hat der Bundesgerichtshof
wohl erkannt und in der Begründung auch klar zum Ausdruck gebracht.
Auch wenn nicht zu erwarten ist, dass der Gesetzgeber sich in
absehbarer Zeit oder überhaupt mit dem Problem eines
Ausschließlichkeitsschutzes für Algorithmen und ähnliche geistige
Leistungen befassen wird, mag sich der Bundesgerichtshof den Vorwurf,
konservativ, noch in der Gedankenwelt des 19. Jahrhunderts verhaftet
zu sein, daher ruhig gefallen lassen.  Denn ein konservatives,
Rechtssicherheit erzeugendes Urteil ist für alle Betroffenen immer
noch besser als ein vermeintlich fortschrittliches, das sich später
als Danaergeschenk erweist.

#Nps: Nach Auffassung des Bundesgerichtshofs kommt eine Patentierung von
Lösungsmaßnahmen im Zusammenhang mit der ADV überhaupt erst in der
Stufe der eigentlichen Programmierung in Betracht, d.h. dort, wo es um
die auf einem Algorithmus beruhenden Anweisungen für den Betrieb einer
Datenverarbeitungsanlage geht.  Diese bereits auf die patentrechtliche
Wertung zugeschnittene Definition des Computerprogramms entspricht
durchaus der in der Informatik gebräuchlichen und in DIN 44300
normierten Begriffsbestimmung, wonach ein Programm %(q:eine zur Lösung
einer Aufgabe vollständige %(tpe:Anweisung:Arbeitsvorschrift) zusammen
mit allen erforderlichen Vereinbarungen) ist. Wiederum wird es der
Informatiker zunächst als paradox ansehen, dass die
Patentierbarkeitszone erst im weniger schöpferischen, sich im
Konstruktiven und Handwerksmäßigen bewegenden Bereich der
Programmierung beginnen soll.  Vielleicht wird er sich mit der
Überlegung trösten, %(q:Ist mein Algorithmus patentrechtlich nichts
wert, so bekomme ich doch wenigstens Schutz für ein darauf aufbauendes
Programm).  Wie der Informatiker jedoch alsbald feststellen wird,
kommt er dabei vom Regen in die Traufe.

#Dte: Der Bundesgerichtshof beurteilt ein Computerprogramm als eine
konkretisierte technische Gebrauchsanweisung für den Betrieb einer
Datenverarbeitungsanlage, um mit ihrer Hilfe eine bestimmte Aufgabe zu
lösen.  Dem ist zuzustimmen.  Jedes Programm hat als Ergebnis einer
nach bestimmten, insbesondere durch die verwendete Programmiersprache
vorgegebenen Regeln vorzunehmenden geistigen Ordnungstãigkeit zwar
zunächst und unmittelbar keine technische Wirkung.  Seine
Zweckbestimmung ist indessen mit der Vollendung des rein geistigen
Schöpfungsakts und der schriftlichen Niederlegung nicht erreicht, da
sein Adressat nicht der Mensch sondern eine Maschine ist. 
Zweckbestimmung eines Computerprogramms ist es ausschließlich, nach
Umsetzung in eine maschinenlesbare Darstellung, die eine Erkennung der
Befehle durch elektromagnetische Signale und Impulse ermöglicht, die
entsprechenden Schaltoperationen im Computer auszulösen und so zu
steuern, bis das gewünschte Ergebnis erreicht ist.  Jedes Programm
verkörpert daher die für den Betrieb einer Datenverarbeitungsanlage
unabdingbare Steueranweisung, ohne die der Computer leere Hülle, tote
Materie wäre, und ist somit in Übereinstimmung mit den oben
dargelegten Grundsätzen %(e:echte Gebrauchsanweisung) für eine
Vorrichtung, also Lehre zum technischen Handeln.

#MWe: Mit der Qualifizierung des Programms als Lehre zum technischen Handeln
ist freilich nichts gewonnen, weil die durch das Programm
repräsentierte Gebrauchsanweisung nicht unabhängig von der
Datenverarbeitungsanlage beurteilt werden kann und nur dann
patentierbar wäre, wenn sie nicht mehr im Bereich des
bestimmungsgemäßen Gebrauchs der nach ihr betriebenen
Datenverarbeitungsanlage liegt.  Machen wir uns nun bewusst, dass die
Universalrechner zur Lösung beliebiger Aufgaben konzipiert und ihre
Einsatzmöglichkeiten nahezu unbegrenzt sind, dann erscheint diese
Hürde unüberwindbar.  Schon hier wird sich jeder fragen, ob für die
Patentierung von Computerprogrammen wirklich noch Raum ist.

#Der: Der Bundesgerichtshof weist zwei Wege für die Patentierung von
Programmen:  Entweder den einer konstruktiven, schaltungstechnischen
Lösung durch eine spezielle Schaltungseinheit oder Vorrichtung oder
den der verfahrensmäßigen Lösung durch Angabe einer neuen, bisher
nicht üblichen und nicht naheliegenden Betriebsvorschrift.  Beide
Möglichkeiten schränkt der Bundesgerichtshof weiterhin dadurch ein,
dass eine in der Problemanalyse und/oder in der Auffindung des
Algorithmus liegende schöpferische Leistung patentrechtlich irrelevant
ist, d.h. der darauf beruhenden speziellen Schaltung bzw
Programmierungsmaßnahme weder Neuheit noch Erfindungshöhe vermitteln
kann.  Diese Einschränkung ist mit Rücksicht auf die Tatsache, dass
die Algorithmen nicht technischer Natur sind und deshalb auch als
solche nicht Gegenstand der Erfindung sein können, ebenso konsequent
wie die Feststellung, dass der fehlende technische Charakter der
programmierten Rechenvorschrift der Patentfähigkeit eines bestimmten
Programmierungsvorschlags nicht entgegensteht.  Die weitere Aussage
schließlich, dass das Fehlen einer schöpferischen Leistung bei der
Erstellung des Programms die %(q:Patentierung der auf das Programm
bezogenen Betriebsanweisung) nicht zwingend ausschließt, ist nur für
die zweite vom Bundesgerichtshof ins Auge gefasste Alternative der
verfahrensmäßigen Lösung bedeutsam.  Sie ergibt nur dann einen Sinn,
wenn man sie so versteht, dass eine an sich naheliegende, weil
fachmännische Programmierungsmaßnahme jedenfalls dann nicht
patenthindernd ist, wenn sie einen gewissermaßen %(q:überraschenden)
Effektim Operationsablauf des Computers zur Folge hätte, d.h. eine
irgendwie vorteilhafte Wirkungsweise, die aufgrund der Konzeption und
Konstruktion der benutzten Datenverarbeitungsanlage nicht zu erwarten
war.  Im HInblick auf den Zusammenhang zwischen Programm und
bestimmungsgemäßem Gebrauch der Datenverarbeitungsanlage ist dieser
Ansatz durchaus zutreffend, da eben Bezugspunkt allein der Computer
und seine Arbeitsweise ist, nicht das Programm als solches, auf dessen
Abweichung von vorbekannten Programmen es daher nicht ankommt.

#DcW: Damit sind wir aber schon bei der Kernfrage, die die vom
Bundesgerichtshof entwickelte Konzeption eines möglichen
%(q:Programm)-schutzes aufwirft: Ob nämliche die Verweisung auf die
konstruktive, schaltungstechnische Lösung bzw die Angabe einer
erfinderischen Gebrauchsanweisung für einen zum Stand der Technik
gehörenden Computer für die Praxis eine realistische, brauchbare
Alternative darstellt, um sich %(q:Programm)-Patente sichern zu
können.  Ich glaube nein.

#Mse: Mehr noch als dieser Weg scheint mir der zweite vom Bundesgerichtshof
für möglich gehaltene Weg, für Programme über die %(e:Angabe einer
neuen Gebrauchsanweisung) für den Computer Patentschutz zu erhalten,
illusionär zu sein.  Die Patentierbarkeit einer solchen
Gebrauchsanweisung setzt voraus, dass der Erfinder erkennt und angibt,
auf welche neue, nicht naheliegende und fortschrittliche Art und Weise
ein Computer zu steuern oder zu betreiben sei, um eine verbesserte
Arbeits- oder Wirkungsweise des %(e:Computers selber) herbeizuführen. 
Dass dies auch nur gelegentlich gelingen könnte, erscheint kaum
denkbar.  Die Universalrechner der heutigen Generatiuon und auch schon
früherer Generationen sind von der Logik und vom Design so konzipiert
und strukturiert, dass sie zur Bewältigung der unterschiedlichsten
Probleme eingesetzt und programmiert werden können.  Ihre
schaltungstechnisch-funktionelle Flexibilität ist daher grundsätzlich
unbegrenzt.  Eine geschickte Programmierung kann nun allerlei
bewirken, z.B. eine Zeitersparnis, eine günstigere Speicherausnutzung
und vieles andere mehr.  Eines aber kann sie nicht bewirken:  Dass der
Computer selber auf neue Art und Weise arbeitet, also etwa Schaltungen
aufbaut, die vom Hardware-Design her nicht vorgesehen oder möglich
wären.  Die Vielfalt der möglichen Maschinenoperationen und
Schaltkombinationen kann nicht zu einem anderen Ergebnis führen. 
Hieraus folgt, dass ein Programm stets nur im Vergleich zu anderen
Programmen anders oder besser sein kann, auf die Arbeitsweise und das
Leistungsvermögen der benutzten Datenverarbeitungsanlage aber nicht
verändernd einwirkt.

#Dic: Die patentrechtliche Konsequenz ist lapidar: %(s:Jede Programmierung
eines Computers bewegt sich immer im Rahmen des bestimmungsgemäßen
Gebrauchs) der Anlage, wie er durch deren Systemstruktur und -funktion
festgelegt und möglich ist.  Dies gilt für alle Typen von Programmen,
also auch für die der Maschine näherstehenden Programme des
Betriebssystems.  Die Vorstellung, dass ein spezielles Programm
%(q:schlummernde) Fähigkeiten des Computers wecken könnte, findet in
der technischen Wirklichkeit keine Stütze.  Selbst wenn ein solcher
Ausnahmefall, d.h. eine die Arbeits- oder Wirkungsweise des Computers
verändernde Gebrauchsanweisung via Programm denkbar wäre und auch
erkannt (!) werden sollte, dann erscheint die Inanspruchnahme des
Urheberrechtsschutzes für das maschinenlesbare Programmpaket erheblich
zweckmäßiger als der kostspielige und langwierige Versuch, für das
Programm ein Patent zu erhalten, das wegen des bloßen
Verfahrensschutzes überdies geringen wirtschaftlichen Wert hätte.  Da
nach der in Deutschland vorherrschenden Rechtsauffassung die
maschinenlesbaren Computerprogramme mit zugehöriger Dokumentation
urheberrechtlichen Schutz genießen, erübrigt sich insoweit auch eine
nähere Untersuchung der Frage, ob eine Korrektur dahingehend geboten
wäre, dass Programmpatente erteilt werden müssten, wenn sie zwar vom
Computer selber nur bestimmungsgemäßen Gebrauch machen, sich aber von
vorbekannten Programmen patentbegründend unterscheiden.

#Drh: Die Bilanz der Dispositionsprogramm-Entscheidung des
Bundesgerichtshofs ist klar und ernüchternd.  Auf der Aktivseite ist
praktisch nichts verblieben.  Versuche, Patentschutz für
Software-Schöpfungen zu erhalten, haben keine Zukunft mehr.

#Ddr: Die vom Bundesgerichtshof vertretene Rechtsauffassung fügt sich
nahtlos in die Rechtsentwicklung in den kontinentaleuropäischen
Ländern ein, die für einen Patentschutz von Algorithmen und
Computerprogrammen kaum mehr Raum lässt.  Der den algorithmisierten
Rechen- und Organisationsvorschriften verweigerte
Ausschließlichkeitsschutz durch Patente verschafft insbesondere den
Herstellern von Datenverarbeitungsanlagen den aus ihrer Sicht
notwendigen Freiraum, da die dem Hardware-Absatz sicher förderliche
umfassende Nutzungsmöglichkeit der Computer durch Drittrechte nicht
eingeschränkt werden kann, und wird von ihnen sicher begrüßt werden. 
Anders sieht die Sache für die Informatiker und die
Software-Hersteller, insbesondere die zahlreichen unabhängigen
Service-Unternehmen aus, die an den von ihnen geschaffenen
algorithmischen Problemlösungen keinen Ausschließlichkeitsschutz
begründen können.  Weder das Urheberrecht noch das von der
Weltorganisation für geistiges Eigentum vorbereitete besondere
Schutzsystem für Computer-Software kann oder soll hier Abhilfe
bringen.  Sie sind daher darauf angewiesen, für ihre verschiedenen
Software-Produkte, namentlich maschinenlesbare Programme nebst
zugehöriger Dokumentation, auf den Urheberschutz und den in Zukunft
vielleicht verfügbaren Sonderschutz für Software zu vertrauen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: grur-kolle77 ;
# txtlang: de ;
# multlin: t ;
# End: ;

