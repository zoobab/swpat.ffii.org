\contentsline {section}{\numberline {1}Historisches}{2}{section.1}
\contentsline {section}{\numberline {2}Die technische Erfindung}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Die Lehre zum technischen Handeln und ihre Elemente}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Die Regeln f\"{u}r verstandesm\"{a}{\ss }ige T\"{a}tigkeit}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}``Fertige'' Lehre und Unmittelbarkeitsgrundsatz}{7}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Pr\"{u}fungsmethode}{8}{subsection.2.4}
\contentsline {section}{\numberline {3}Informatik und Patentrecht}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}Die Dispositionsprogramm-Entscheidung und ihre patentrechtliche W\"{u}rdigung}{9}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Das Verh\"{a}ltnis von Programm und maschineller Schaltung}{10}{subsubsection.3.1.1}
\contentsline {subsection}{\numberline {3.2}Patentschutz f\"{u}r Software: Bleibt noch ein Weg?}{11}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Algorithmen}{12}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Computerprogramme}{13}{subsubsection.3.2.2}
\contentsline {section}{\numberline {4}Ergebnis und Ausblick}{16}{section.4}
