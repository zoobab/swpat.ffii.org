<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Der Artikel über Patente analysiert die Geschichte der französischen und europäischen Patentrechtsprechung.  Er erklärt, warum die europäischen Parlamente und Gerichte sich in den 60-70er Jahren gegen die Patentierbarkeit von Computerprogrammen und Programmierideen entschieden und wie das Europäische Patentamt diese Entscheidungen seit 1986 schrittweise rückgängig gemacht hat.  Er warnt aber, dass auf diese Weise vom EPA gewährte Patente in der Praxis von ungewissem Wert sind.
title: Lamy Droit Informatique 1998
LKy: %{LAMY} ist ein französisches Standard-Nachschlagewerk des Computerrechts.
Ve1: Am Schluss wird resümiert:
Lkm: Ist Software nun endlich patentierbar?
SuW: Zweifellos noch nicht.
Eee: In Wirklichkeit sind die Gesetzesregeln des Übereinkommens und der nationalen Gesetze klar:  Sie fordern unmissverständlich die Nicht-Patentierbarkeit von Software.  Das Spiel, das heute gespielt wird, besteht darin, in einer oder der anderen Weise diese Regeln zu verdrehen, z.B. indem man sich, wie oben beschrieben, die Gesamtheit aus Hardware und Software als eine virtuelle Maschine denkt, die (künftig ...) patentierbar sein könnte.  Unter dieser Voraussetzung kann man dann patentrechtlich argumentieren.  Die auf diese Weise auf dem einen oder anderen Wege erhältlichen Patente haben allerdings nur denjenigen Wert, den man ihnen beimisst --- oder der sich durch einen Konsens ergibt, dieser Frage nicht genauer nachgehen zu wollen.  Tatsächlich kann die Verdrehung der Gesetzesregeln nur insoweit Wirkung entfalten, wie sich ein Konsens darüber herstellen lässt, ob man dieses Spiel gegen die bestehenden Gesetzesregeln spielen soll oder nicht.  Hierbei handelt es sich nicht mehr um eine juristische Frage im strengen Sinne.
Soi: Juristisch gesehen müsste die Entwicklung sich auf der Ebene der geschriebenen Regeln bewegen (etwa durch Aufhebung von Patentierungsausschlüssen).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: lamy98 ;
# txtlang: de ;
# End: ;

