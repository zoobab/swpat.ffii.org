<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Lamy Droit Informatique 1998

#descr: Der Artikel |ber Patente analysiert die Geschichte der franzvsischen
und europdischen Patentrechtsprechung.  Er erkldrt, warum die
europdischen Parlamente und Gerichte sich in den 60-70er Jahren gegen
die Patentierbarkeit von Computerprogrammen und Programmierideen
entschieden und wie das Europdische Patentamt diese Entscheidungen
seit 1986 schrittweise r|ckgdngig gemacht hat.  Er warnt aber, dass
auf diese Weise vom EPA gewdhrte Patente in der Praxis von ungewissem
Wert sind.

#LKy: %{LAMY} ist ein französisches Standard-Nachschlagewerk des
Computerrechts.

#Ve1: Am Schluss wird resümiert:

#Lkm: Ist Software nun endlich patentierbar?

#SuW: Zweifellos noch nicht.

#Eee: In Wirklichkeit sind die Gesetzesregeln des Übereinkommens und der
nationalen Gesetze klar:  Sie fordern unmissverständlich die
Nicht-Patentierbarkeit von Software.  Das Spiel, das heute gespielt
wird, besteht darin, in einer oder der anderen Weise diese Regeln zu
verdrehen, z.B. indem man sich, wie oben beschrieben, die Gesamtheit
aus Hardware und Software als eine virtuelle Maschine denkt, die
(künftig ...) patentierbar sein könnte.  Unter dieser Voraussetzung
kann man dann patentrechtlich argumentieren.  Die auf diese Weise auf
dem einen oder anderen Wege erhältlichen Patente haben allerdings nur
denjenigen Wert, den man ihnen beimisst --- oder der sich durch einen
Konsens ergibt, dieser Frage nicht genauer nachgehen zu wollen. 
Tatsächlich kann die Verdrehung der Gesetzesregeln nur insoweit
Wirkung entfalten, wie sich ein Konsens darüber herstellen lässt, ob
man dieses Spiel gegen die bestehenden Gesetzesregeln spielen soll
oder nicht.  Hierbei handelt es sich nicht mehr um eine juristische
Frage im strengen Sinne.

#Soi: Juristisch gesehen müsste die Entwicklung sich auf der Ebene der
geschriebenen Regeln bewegen (etwa durch Aufhebung von
Patentierungsausschlüssen).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: lamy98 ;
# txtlang: de ;
# multlin: t ;
# End: ;

