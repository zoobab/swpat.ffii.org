<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Lamy Droit Informatique 1998

#descr: L'article sur le brevet analyse l'historique de la jurisdiction
frangaise et europienne en matihre de brevets.  Il explique pour quoi
les ligislateurs europiens dans les annies 60-70 dicidhrent contre la
brevetabiliti du logiciel et comment l'Office Europiens de Brevets a
contourni ces dicisions peu a peu depuis 1986, et que les brevets
accordis par cette nouvelles jurisdiction ont une valeur incertaine.

#LKy: %{LAMY} est un oevre de référence prestigieux.

#Ve1: Voici le Résumé final:

#Lkm: Le logiciel est-il donc finalement brevetable?

#SuW: Sans doute pas encore.

#Eee: En réalité, les règles nationales et conventionnelles sont claires: 
elles posent sans équivoque un principe de non-brevetabilité du
logiciel.  Le jeu qui se joue aujourd'hui consiste à contourner d'une
manière ou d'une autre celles-ci, par exemple en imaginant de
considérer, comme on l'a vu, l'ensemble constitué par le matériel et
le logiciel comme une machine virtuelle susceptible (demain ...)
d'être breveteée.  À ce compte-là, on peut parler brevets.  Les
brevets susceptibles d'être ainsi obtenus, par ce canal ou un autre,
n'ont, toutefois, que la valeur qu'on leur prête - mais il ne faut pas
écarter l'hypothèse selon laquelle on finirait par une sorte de
consensus à ne pas vraiment la discuter.  De fait, l'efficacité de ce
countournement des règles légales sera largement fonction du fait
qu'un tel consensus se dégagera pour accepter --- contre les règles
positives --- que ce nouveau jeu se joue ou non.  La question ne se
situe plus sur le terrain juridique %(e:stricto sensu).

#Soi: Sur ce terrain, c'est en termes d'évolution des règles écrites qu'il
faudrait se situer (en termes de levée des interdits).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: lamy98 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

