<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Beier 1972: Zukunftsprobleme des Patentrechts

#descr: Prof. Dr. Karl Beier, ein bis in die 90er Jahre aktiver und viel
beachteter Patentrechtsgelehrter, ist besorgt, dass der Gesetzgeber an
einem engen Technikbegriff festhalte, weil er nur rechtssystematisch
denke und sich nicht um die Zukunft des Patentsystems sorge. So
könnten große zukunftsträchtige Bereiche wie Informatik und Gentechnik
zunehmend in ein schutzrechtliches Niemandsland fallen.  Man müsse
sich von den traditionellen Vorbehalten gegen die Monopolwirkung des
Patentes verabschieden und zu einem klaren Ja zum Patentwesen finden. 
Auch die Grundlagenforschung der Universitäten müsse in dessen Genuss
kommen.  Dieser Artikel zeigt sehr deutlich, wo die Linien der
Auseinandersetzung um das Patentrecht in den 70er Jahren lagen und
gibt Aufschluss darüber, in wieweit die Entscheidung von 1973,
Programme für Datenverarbeitungsanlagen u.a. explizit von der
Patentierbarkeit auszuschließen, eine bewusst gewählte
gesetzgeberische Weichenstellung war.

#Slb: Straus erwähnt Beier als seinen wichtigsten Mitstreiter in den 70/80er
Jahren.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: grur-beier72 ;
# txtlang: de ;
# multlin: t ;
# End: ;

