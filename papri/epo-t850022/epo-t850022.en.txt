<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: The EPO's Technical Board of Appeal rejects a patent application which is directed to a program for computers.  In 1984, the EPO's examiners had rejected the patent based on the original Examination Guidelines of 1978, saying that the claims, by referring to classical computing equipment and rules for using it, described nothing but a program for computers [ as such ].  Later, due to the EPO's policy changes 1985/86, the Board of Appeal no longer bases its rejection on the fact that the claim is directed to a program for computers, but on the fact that the innovation falls into the category of %(q:schemes, rules and methods for performing mental acts) and does not become patentable merely because it is applied to a computer.
title: EPO T 22/85: Document Processing System Patent Rejected, 1984 as Program, 1988 as Algorithm
eha: The EPO rejects a patent application which is directed to a program for computers.
snr: In 1984, the EPO's examiners had rejected the patents based on the original %(eg:Examination Guidelines of 1978), saying that the claims referred to a program for computers in verbal clothing.
she: The appellant argued on the basis of newer Guidelines and caselaw that his claims are directed to further technical effects and not a program as such.  This time, in 1988, the Board of Appeal evades the question of what %(q:programs for computer) are in the EPC context and instead rejects the appeal by arguing indirectly that the use of general-purpose computer hardware does not confer technicity on an abstract method:
jps: The EPO's judges, still in 1990, pronounce remarkable insights such as:
iws: The Examining Division had in 1984 taken an even more straightforward view of Art 52(2)(c), based on the then used Examination Guidelines:

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: epo-t850022 ;
# txtlang: en ;
# End: ;

