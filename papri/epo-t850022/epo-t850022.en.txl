<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EPO 1990: T 0022/85

#descr: A Technical Board of Appeal of the European Patent Office (EPO)
rejects a patent application which is directed to a program for
computers.  In 1984, the EPO's examiners had rejected the patents
based on the original Examination Guidelines of 1978, saying that the
claims referred to a %(q:program for computers).  The appellant argued
on the basis of newer Guidelines and caselaw that his claims are
directed to technical effects and not a program as such.  The Board of
Appeal rejects the appeal by arguing indirectly that the use of
general-purpose computer hardware does not confer technicity on an
abstract method:  %(q:Abstracting a document, storing the abstract,
and retrieving it in response to a query falls as such within the
category of schemes, rules and methods for performing mental acts and
constitutes therefore non-patentable subject-matter under Article 52
EPC) and %(q:The mere setting out of the sequence of steps necessary
to perform an activity, excluded as such from patentability under
Article 52 EPC, in terms of functions or functional means to be
realised with the aid of conventional computer hardware elements does
not import any technical considerations and cannot, therefore, lend a
technical character to that activity and thereby overcome the
exclusion from patentability.)

#eha: The EPO rejects a patent application which is directed to a program
for computers.

#snr: In 1984, the EPO's examiners had rejected the patents based on the
original %(eg:Examination Guidelines of 1978), saying that the claims
referred to a program for computers in verbal clothing.

#she: The appellant argued on the basis of newer Guidelines and caselaw that
his claims are directed to further technical effects and not a program
as such.  This time, in 1988, the Board of Appeal evades the question
of what %(q:programs for computer) are in the EPC context and instead
rejects the appeal by arguing indirectly that the use of
general-purpose computer hardware does not confer technicity on an
abstract method:

#jps: The EPO's judges, still in 1990, pronounce remarkable insights such
as:

#iws: The Examining Division had in 1984 taken an even more straightforward
view of Art 52(2)(c), based on the then used Examination Guidelines:

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: epo-t850022 ;
# txtlang: en ;
# multlin: t ;
# End: ;

