<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: Prof Tamai of Tokyo University shows how patenting of software clashes with some of the underlying assumptions of the patent system.  The patent system relies on requirements such as concreteness and physical substance in order to keep the breadth of claims within reasonable limits.  Software innovation however is the art of making processes as general as possible, i.e. the art of abstraction.  Tamai quotes a set of patent claims from the SOFTIC symposium of 1993, where patent officials from JP, US and EU judged the patentability of an example algorithm at different levels of concretisation.  The European representative was more willing than his colleagues from US and JP to grant patents on abstract claims, but even he shyed back from granting them at the level that really represents the innovative achievement.  Tamai shows how this inconsistency leads to a series of other inconsistencies.  Tamai sees only two ways out of the inconsistency: (1) acceptance of abstract claims (2) exclusion of software patents.
title: Tamai 1998: Abstraction orientated property of software and its relation to patentability
med: Tamai refers to the exemplary debate about software patentability between %(dc:Chisum) and %(pn:Newell) in 1986 and then further elaborates on one of Newell's arguments: the orientation of software towards abstraction and generalisation.
hes: Tamai then explains several types of abstraction:
tWe: Abstraction of procedures
siW: Abstraction of data
tnj: Abstraction of objects
tWr: Abstraction of algorithms
rmf: Finally, Tamai describes cases where the same algorithm was discovered independently by different people and it was not trivial to discover that in fact these algorithms were the same.  The greatest achievement in these discoveries was in fact in discovering commonalities between different algorithms and thereby increasing the level of abstractness.
cla: claim
stt: Suppose the quick sort algorithm is newly invented and this claim describes its algorithm as operations on an array in memory.  The operations are writtenin at a relatively abstract level such as %(q:a sequence of numerical data are transferred so that they are partitioned into two data sequences, one having values equal to or less than the boundary value and the other having values greater than the boundary value).  Iterative application by recursion and the termination condition are also described in a similar way.
Wal: The same quick sort algorithm is described at the detailed level of programs.  As assumed data structure is an array just as Claim 1 but the numerical data sequence is represented concretely by a pair of pointers for the first and the last positions in the array and operations of data exchange are represented also using pointers.  To implement recursion, a data structure of stack is explicitly introduced.
Wtf: Claims 3 to 6 also deal with sorting but a specific sorting method, e.g. the quick sort, is not specifically designated.  Claim 3 describes an application of sorting that sorts addresses written on commodities to deliver in the order of delivery numbers and prints out the result.  The correspondence between the addresses and the delivery numbers is given by a table.  The method first determines the delivery numbers from the input addresses and sorts them accordingly.
shr: The same processing as Claim 3 but an image reader is used for input.  Character codes of addresses are obtained by the character recognition processing.
adl: In addition to Claim 4, three devices of stackers are added to the system and they are used physically to sort commodities.
rsW: The contents are the same as Claim 4 but are described as an apparatus claim not as a method claim.
WWk: The European Patent Officer surprised many in the audience by making more %(q:liberal) judgments than those from US and Japan.  The Japanese officer left some answers open (expressed by question marks above), suggesting that he needed to know more details about what the claims were to look like.
aat: In his further analysis, Tamai highlights the fact that the real achievement lies in the abstraction, but patents tend to be granted more readily for concretisations.  He sees the following problems:
mui: Tamai summarises
Wna: and discusses in detail how this inconsistency is at the origine of several debates, such as
ohE: Function Claims and the Doctrine of Equivalence
tyg: Contributory Infringement
ioa: Finally Tamai concludes that, in order to achieve consistency, two paths are possible
ehs: remove the requirements of concreteness, specificity and physical substance and grant patents to abstract and logical claims
whW: decide that software and algorithms do not fit into the framework of the patent system and must be unpatentable.
ueo: The first option would achieve logical consistency within the patent system but undesirable results toward the outside.  Tamai summarises:
giW: It would be desirable to have a way of rewarding the inventor who invented a highly general algorithm or software.  However the harm of admitting too broad rights for abstract and logical claims will surpass its merits.  On the other hand, the current practise of issuing software-related patents if the claims are combined with hardware and written concretely and specifically is essentially against the nature of software.  Thus, the conclusion is we have to resort to other means besides patents, e.g. copyright, for the intellectual property protection.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: ist-tamai98 ;
# txtlang: en ;
# End: ;

