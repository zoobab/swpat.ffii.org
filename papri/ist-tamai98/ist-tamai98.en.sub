\begin{subdocument}{ist-tamai98}{Tamai 1998: Abstraction orientated property of software and its relation to patentability}{http://swpat.ffii.org/papers/ist-tamai98/index.en.html}{Hartmut PILCH\\\url{http://www.a2e.de}\\\url{phm@a2e.de}\\FFII\\\url{}\\\url{info@ffii.org}\\english version 2004/08/16 by Hartmut PILCH\footnote{\url{http://www.a2e.de}}}{Prof Tamai of Tokyo University shows how patenting of software clashes with some of the underlying assumptions of the patent system.  The patent system relies on requirements such as concreteness and physical substance in order to keep the breadth of claims within reasonable limits.  Software innovation however is the art of making processes as general as possible, i.e. the art of abstraction.  Tamai quotes a set of patent claims from the SOFTIC symposium of 1993, where patent officials from JP, US and EU judged the patentability of an example algorithm at different levels of concretisation.  The European representative was more willing than his colleagues from US and JP to grant patents on abstract claims, but even he shyed back from granting them at the level that really represents the innovative achievement.  Tamai shows how this inconsistency leads to a series of other inconsistencies.  Tamai sees only two ways out of the inconsistency: (1) acceptance of abstract claims (2) exclusion of software patents.}
\begin{description}
\item[title:]\ Tamai 1998: Abstraction orientated property of software and its relation to patentability
\item[source:]\ IST\footnote{\url{http://swpat.ffii.org/papers/index.en.html\#IST}} elsevier Information and Software Technology 40 (1998) 253-257
\end{description}

Tamai refers to the exemplary debate about software patentability between Chisum\footnote{\url{http://swpat.ffii.org/papers/uplj-chisum86/index.en.html}} and Newell\footnote{\url{http://swpat.ffii.org/papers/uplr-newell86/index.en.html}} in 1986 and then further elaborates on one of Newell's arguments: the orientation of software towards abstraction and generalisation.

\begin{quote}
{\it The history of software engineering shows that it has been exploring technologies applicable to a wider range and usable in the more general context.  The most fundamental principle for attaining that goal is abstraction.  In general, software is more valuable if its components are constructed at a more abstract level, be they procedures, data, objects, substystems and eventually the total system itself.}
\end{quote}

Tamai then explains several types of abstraction:
\begin{itemize}
\item
Abstraction of procedures

\item
Abstraction of data

\item
Abstraction of objects

\item
Abstraction of algorithms
\end{itemize}

Finally, Tamai describes cases where the same algorithm was discovered independently by different people and it was not trivial to discover that in fact these algorithms were the same.  The greatest achievement in these discoveries was in fact in discovering commonalities between different algorithms and thereby increasing the level of abstractness.

Tamai gives an example set of patent claims, used at the Fourth International Symposium on Legal Protection of Computer Software sponsored by SOFTIC, and reports about how patent officials from US, Japan and Europe judged their patentability.  The example claims are described as follows:

\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
claim & JP & US & EU\\\hline
Suppose the quick sort algorithm is newly invented and this claim describes its algorithm as operations on an array in memory.  The operations are writtenin at a relatively abstract level such as ``a sequence of numerical data are transferred so that they are partitioned into two data sequences, one having values equal to or less than the boundary value and the other having values greater than the boundary value''.  Iterative application by recursion and the termination condition are also described in a similar way. & - & - & -\\\hline
The same quick sort algorithm is described at the detailed level of programs.  As assumed data structure is an array just as Claim 1 but the numerical data sequence is represented concretely by a pair of pointers for the first and the last positions in the array and operations of data exchange are represented also using pointers.  To implement recursion, a data structure of stack is explicitly introduced. & ? & - & +\\\hline
Claims 3 to 6 also deal with sorting but a specific sorting method, e.g. the quick sort, is not specifically designated.  Claim 3 describes an application of sorting that sorts addresses written on commodities to deliver in the order of delivery numbers and prints out the result.  The correspondence between the addresses and the delivery numbers is given by a table.  The method first determines the delivery numbers from the input addresses and sorts them accordingly. & - & - & -\\\hline
The same processing as Claim 3 but an image reader is used for input.  Character codes of addresses are obtained by the character recognition processing. & - & - & +\\\hline
In addition to Claim 4, three devices of stackers are added to the system and they are used physically to sort commodities. & + & + & +\\\hline
The contents are the same as Claim 4 but are described as an apparatus claim not as a method claim. & ? & + & +\\\hline
\end{tabular}
\end{center}

The European Patent Officer surprised many in the audience by making more ``liberal'' judgments than those from US and Japan.  The Japanese officer left some answers open (expressed by question marks above), suggesting that he needed to know more details about what the claims were to look like.

In his further analysis, Tamai highlights the fact that the real achievement lies in the abstraction, but patents tend to be granted more readily for concretisations.  He sees the following problems:

\begin{enumerate}
\item
If a generally usable algorithm or software is found, it cannot be claimed directly but rather must be claimed in terms of a list of concretisations.

\item
If a patent is issued to concretisation and later a more powerful algorithm is found by abstraction, the later algorithm will tend to be unpatentable.

\item
Concretisations of known algorithms in new areas of application will tend to be patentable.  It will at least require great skill and effort on the part of examiners to reject such applications.
\end{enumerate}

Tamai summarises

\begin{quote}
{\it Many issues debated in relation to software patents have their origin in the inconsistency between the innate properties of software, i.e. abstractness, generality and logical nature, and the required properties for patent claims, i.e. concreteness, specificity and physical substance.}
\end{quote}

and discusses in detail how this inconsistency is at the origine of several debates, such as

\begin{itemize}
\item
Function Claims and the Doctrine of Equivalence

\item
Contributory Infringement
\end{itemize}

Finally Tamai concludes that, in order to achieve consistency, two paths are possible

\begin{enumerate}
\item
remove the requirements of concreteness, specificity and physical substance and grant patents to abstract and logical claims

\item
decide that software and algorithms do not fit into the framework of the patent system and must be unpatentable.
\end{enumerate}

The first option would achieve logical consistency within the patent system but undesirable results toward the outside.  Tamai summarises:

\begin{quote}
{\it It would be desirable to have a way of rewarding the inventor who invented a highly general algorithm or software.  However the harm of admitting too broad rights for abstract and logical claims will surpass its merits.  On the other hand, the current practise of issuing software-related patents if the claims are combined with hardware and written concretely and specifically is essentially against the nature of software.  Thus, the conclusion is we have to resort to other means besides patents, e.g. copyright, for the intellectual property protection.}
\end{quote}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
% mode: latex ;
% End: ;

