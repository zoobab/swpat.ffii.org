<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: NRC 2000: The Dilemme Numirique

#descr: Selon ce rapport du Conseil National de la Recherche des Etats-Unis,
les brevets logiciels ont iti introduits par les cours de justice sans
le soutien du corps ligislatif, et il semble douteux que l'expansion
des brevets promeuve le progrhs des sciences et des arts appliquis,
comme l'entendait le Congrhs.  La Cour d'Appel du Circonscription
Fidiral (CAFC) a porti  le systhme de brevets dans %(q:des eaux
inexplories), et l'expirience de l'industrie du logiciel sugghre que
cette dicision attends de fagon urgente qu'elle soit revue par la voie
ligislative.

#ArW: Comme vous pouvez le voir sur la table des matières, il s'agit d'un
rapport du Conseil National de la Recherche des Etats-Unis, avec la
participation de plusieurs savants de renommée dans tous les domaines,
y compris des chercheurs d'IBM et de Microsoft.

#Tei: Le but de ce rapport est d'explorer les voies possibles de la
propriété intellectuelle dans le monde numérique.

#Tti: Le rapport propose des voies d'extension du copyright, mais en même
temps est très critique sur l'application des brevets à propos %(q:des
innovations sur l'information) --- un bon terme qui maintiens la
distinction entre %(q:invention) brevetable et %(q:innovations)
immatérielles, qui auraient du rester en dehors du domaine du système
des brevets.

#TtW: Le Chapitre aux pages 192 à 197

#Ttf: L'Impact de la Délivrance de Brevets sur l'Innovation dans
l'Information

#rWW: rapporte comment le système de brevets des Etats-Unis a été élargi par
les cours de justice sur le brevet (sans le soutien législatif) pour y
inclure le logiciel et les méthodes d'entreprises, et analyse quelques
uns des cas célèbres.  Le rapport conclut:

#Tel: Les effets de cet élargissement substantiel de facto au sujet des
brevets pour y englober les inventions sur l'information sont encore
incertains. Parce que cette expansion est survenue sans la supervision
de la branche législative et a amené la loi sur les brevets dans des
territoires inexplorés, cela vaudrait la peine d'étudier ce phénomène
pour s'assurer que l'expansion des brevets promeuve le progrès de la
science et des arts appliqués, comme l'entendait le Congrès.

#Tno: Il y a beaucoup de raisons d'être inquiet. La première est que l'USPTO
manque d'information suffisante sur l'état de l'art dans les domaines
de la technologie de l'information, de la conception de l'information,
et des méthodes d'entreprises plus généralement pour rendre des
décisions adaptées sur la nouveauté ou la non pertinence de
revendications dans ces domaines. L'inquiétude liée à l'article 47 A
est le nombre insuffisant d'examinateurs de brevets adéquatement
formés et le schéma de classification des brevets inadéquate pour
traiter de nouveaux sujets. Le succès du système de brevets dans la
promotion de l'innovation dans un domaine dépend de l'intégrité du
processus de délivrance des brevets, qui en retour, dépend de
l'information appropriée sur le domaine. Des questions sérieuses
demeurent au sein du domaine de la technologie de l'information à
propos des décisions du PTO relatives au brevet logiciel. Nombre de
commentateurs juridiques ont souligné que l'autorisation de ce genre
de brevets rendraient potentiellement conceptualisable, et non
technologique, la propriété à protéger du détenteur de brevet,
%(q:rendant virtuellement brevetable n'importe quoi).

#Sic: Deuxièment, la tradition d'indépendance de création dans le domaine de
la programmation d'ordinateur peut aller à l'encontre des suppositions
et des pratiques associées aux brevets telles qu'elles sont appliquées
dans leurs domaines traditionnels. Quand quelqu'un brevète un
composant d'un système manufacturé par exemple, il sera généralement
possible pour l'inventeur de fabriquer son composant ou de donner
licence à une autre entreprise pour son produit et récolter le fruit
de son invention en vendant son composant. Les droits d'utilisation de
l'invention disparaissent avec l'achat du composant pour son
installation dans un matériel plus grand.

#BWW: Mais il y a un petit marché, si ce n'est aucun, pour les composants
logiciels. Les programmeurs, généralement, conçoivent de grands et
complexes systèmes à partir de rien. Ils le font en grande majorité
sans se référer à la littérature sur les brevets (en partie parce
qu'ils considèrent qu'elle est déficiente), bien qu'ils respectent
généralement les contraintes du copyright et du secret professionnel
dans leur travail. Avec des dizaines de milliers de programmeurs
écrivant du code qui pourrait bien enfreindre des centaines de brevets
sans avoir connaissance de leur existence, il y a un risque accru
d'infraction par inadvertance.  Un autre facteur n'encourageant pas la
consultation de la littérature sur les brevets est que d'apprendre
l'existence d'un brevet augmenterait le risque d'être pris pour avoir
sciemment violer le brevet. C'est pourquoi la littérature sur les
brevets ne peut pas fournir au monde du logiciel un de ses centre
d'intérêt traditionnel --fournir une information sur l'évolution de
l'état de l'art. On pourrait en dire autant sur l'inadéquation entre
les brevets et les inventions sur l'information en général.

#Tts: Troisièmement, bien que les brevets semblent avoir été couronnés de
succès dans la promotion des investissements en faveur du
développement des entreprises innovatrices et des autres technologies
et processus industriels, il est possible qu'ils ne soient pas aussi
heureux dans la promotion de l'innovation dans l'économie de
l'information. Un des problème est que le rythme de l'innovation dans
les industries de l'informationest si rapide, et que les rouages du
système de brevets sont si lents, que les brevets ne peuvent pas
promouvoir l'innovation dans les industries de l'information aussi
bien qu'ils l'on fait dans l'économie des industries classiques. Le
cycle du marché pour un produit d'information est souvent très court
-- un cycle de 18 mois est courant; par contre, un brevet peut très
bien ne pas être sorti jusqu'à ce que le produit soit devenu obsolète.
Si les inventions sur l'information continuent de rester dans le
périmètre des brevets, alors, au minimum, la durée du cycle des
brevets a besoin de s'améliorer significativement. Les systèmes de
classification des brevets pour les innovations sur l'information
peuvent aussi être plus difficile à développer et maintenir dans une
voie qui informera et contribuera au succès des domaines qu'elles
servent.

#Oir: Une dernière raison de s'inquiéter est que le développement et le
déploiement des logiciels et systèmes peut cesser d'être un travail à
domicile en raison du besoin d'accéder aux agréments de licences
croisées et à la protection juridique de grandes entreprises. Ceci
peut avoir en retour des effets délétères sur la créativité des
industries de logiciels et Internet des Etats-Unis.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: gibuskro ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: digidilem00 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

