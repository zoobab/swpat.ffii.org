<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta name="author" content="Workgroup">
<link href="/group/index.en.html" rev="made">
<link href="http://www.ffii.org/assoc/polis/webdist/webstyl/ffii.css" rel="stylesheet" type="text/css">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2005/01/04">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="en">
<meta http-equiv="Reply-To" content="swpatag@ffii.org">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="Foundation for a Free Information Infrastructure, Software Patents, intellectual property, industrial property, IP, I2P, immaterial assets, law of immaterial goods, mathematical method, business methods, Rules of Organisation and Calculation, invention, non-inventions, computer-implemented inventions, computer-implementable inventions, software-related inventions, software-implemented inventions, software patent, computer patents, information patents, technical invention, technical character, technicity, technology, software engineering, industrial character, Patentierbarkeit, substantive patent law, Nutzungsrecht, patent inflation, quelloffen, standardisation, innovation, competition, European Patent Office, General Directorate for the Internal Market, patent movement, patent family, patent establishment, patent law, patent lawyers, lobby">
<meta name="title" content="Robert Hart 1997: The Case for Patent Protection for Computer Program-Related Inventions">
<meta name="description" content="Patent lawyers try to create the impression that the United States introduced software patents after a conscious, public-opinion based debate. One of the most outspoken pro software patent activists at the European Commission, Robert Hart, argued the case in 1997 by misrepresenting the positions of some major patent-critical voices as being pro software patent. Hart later co-authored an &quot;independent study&quot; at the order of the European Commission.  Both use approximately the same methodology.">
<title>Hart 1997</title>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><div id="doktop">
<!--#include virtual="links.en.html"-->
<div id="doktit">
<h1>Robert Hart 1997: The Case for Patent Protection for Computer Program-Related Inventions<br>
<!--#include virtual="../../../../../../banner0.en.html"--></h1>
</div>
<div id="dokdes">
<!--#include virtual="deskr.en.html"-->
</div>
</div>
<div id="dokmid">
<div id="papri">
<dl><dt>title:</dt>
<dd>Robert Hart 1997: The Case for Patent Protection for Computer Program-Related Inventions</dd>
<dt>source:</dt>
<dd><a href="/papers/index.en.html#CLSR">CLSR</a> CLSR 13(4) /p:247-252</dd></dl>
</div>
<div class="sectmenu">
<ol type="1"><li><a href="#intro">Introduction</a></li>
<li><a href="#suigen">Reinterpreting a manifesto for a sui generis law as justification for patents</a></li>
<li><a href="#ota">Taking what is useful from the OTA report</a></li>
<li><a href="#klud">Conclusion</a></li>
<li><a href="#ref">Further Reading</a></li>
<li><a href="#auth">Acknowledgements</a></li></ol>
</div>

<div class="secttext">
<div class="secthead">
<h2>1. <a name="intro">introduction</a></h2>
</div>

<div class="sectbody">
The british patent lawyer Robert Hart is a well-known <a href="http://www.softprot.demon.co.uk/indpatent.htm">long term advocate</a> in favor of software patents in Europe.  In 2000 he wrote a much-quoted <a href="/papers/digidilem00/bessenhunt03/cec-telecom02/indprop-ipi00/index.en.html">study</a> on the subject at the order of the European Commission.  In 2002, the European Parliament invited him as an expert to a <a href="/events/2002/europarl11/index.en.html">hearing</a> on the subject.<p>The Hart paper of 1997 was published in a context where the European Commission was formulating its policy on software patents as expressed in the <a href="http://kwiki.ffii.org/Eupatgp97En">Greenpaper</a> and its <a href="http://kwiki.ffii.org/Indprop8682En">follow-up papers</a>.  Like many of his colleagues, Hart argues mainly by extension of precedents:  computer programs are comparable to some currently patentable objects and should therefore also be patentable:
<div class=cit>
Patent systems have been created to stimulate the evolution of existing and the creation of new industry by protecting the industrial application of innovations. The computer software industry needs that stimulation as much as any other industry.<p>...To deny patent protection for such inventions would I believe remove from the patent system a significant industry thereby severely disadvantaging the innovators in that industry encouraging second comers rather than original creators upon whom any industry relies heavily for its evolution. (p. 252)
</div><p>In the paper, the argumentation goes:
<ol><li>Introduction</li>
<li>limitations of copyright
<ol><li>the need for protection beyond copyright</li>
<li>what inventions could be patented</li>
<li>position of the EPO</li>
<li>history</li>
<li>policy</li>
<li>trade secrets</li></ol></li>
<li>conclusion</li></ol><p>From this design  it is obvious section 2b is the actual main argument whether patentability is desirable, so let us check it for scholarly consistency.)<p>The main arguments are given as quotes from
<ol><li>Manifesto for sui generis protection and</li>
<li>OTA report.</li></ol>
</div>

<div class="secthead">
<h2>2. <a name="suigen">Reinterpreting a manifesto for a sui generis law as justification for patents</a></h2>
</div>

<div class="sectbody">
Hart summarizes the Columbia Law Review paper <a href="http://wwwsecure.law.cornell.edu/commentary/intelpro/manifint.htm#intro">A Manifesto Concerning the Legal Protection of Computer Programs</a> as<p><div class=cit>
Effectively the Manifesto considers that there should be (i) copyright protection for the literal expression of the program code, (ii) patent protection for those functional features of the computer program and their application that meet the level of protection required by the patent system and (iii) an anti-cloning system to protect the program behavior not protected by the patent system.
</div><p>However, in the manifesto document, &quot;patents&quot; does not occur in the table of contents, nor in the conclusion.  The only mention we have (passim) so far found is <div class=cit>
the issue of a large number of questionable patents for software related ideas may impede competitive development and follow-on innovation in the software industry
</div> (p. 2422). The emphasis seems to call for a sui generis property law (like the semiconductor act, cf pp. 2413-2420). Again, from the manifesto's conclusion:<p><div class=cit>
We propose to remedy market-destructive appropriatins of program behavior and the industrial designs aimed at producing efficient program behavior through a period of automatic anti-cloning protection of these innovations. The period should be long enough enough to give efficient incentives to invest in the development of innovative software, yet short enough to avert the market failure that would result if second competitors and follow-on inventors were blocked from entering the market long after the first firm had recouped its initial investment. (p. 2430)
</div><p>It is clear one does not even have to join the call for sui generis semiconducor-like protection desired by that manifesto (so may be of limited usefulness for those opposed to software protection beyond copyright), but to state they advocated patents for programs seems abusive.
</div>

<div class="secthead">
<h2>3. <a name="ota">Taking what is useful from the OTA report</a></h2>
</div>

<div class="sectbody">
The conclusions of the <a href="http://www.wws.princeton.edu/~ota/disk1/1992/9215.html">OTA report</a> (Section 1, pp. 32ff) are neutral:<p><div class=cit>
Option 2.1: Refine the statutory definition of patentable subject matter to provide guidance for the courts and PTO. Legislation might address the extent to which processes implemented in software or mathematical algorithms are or are not statutory subject matter. Legislation might also address the issue of special exemptions, such as for research and education).<p>Option 2.2: Exclude software-related inventions and/or algorithms from the patent law and create a special, sui generis protection within a patent framework for some inventions. The latter might have a shorter term, lower criteria for inventiveness, and/or special exemptions from infringement.
</div><p>However, the section that Hart quotes at length (on p. 132 of the report in its Section 4) in a way suggesting this also would be the OTA's direct recommendation, is just a summary of the USPTO arguments and of no recommendative meaning, in the same manner one could quote nearly the entire p. 136 of said Section 4 which summarizes objections to software patentability.
</div>

<div class="secthead">
<h2>4. <a name="klud">Conclusion</a></h2>
</div>

<div class="sectbody">
To defend Hart, the OTA report is presented in a more balanced way in his section 2f (p. 251). But the fact that both pillars of the core &quot;why we need patents&quot; section are hollow leaves a somewhat bitter taste in the argumentation's overtones &quot;the US know what they are doing&quot;.<p>Hart also does not tell us that at the 1994 USPTO hearings, only a fraction of software companies (among them Microsoft) argued clearly in favor of software patents, whereas a larger fraction considered software patents harmful to business interests (among them Oracle, Adobe). Of course, there also was a broad field of undecided statements and statements from law firms in favor of software patents.<p>Even in 2000 the USPTO conducted a similar hearing.  Yet, however critical the opinions raised in these hearings, it was always unlikely that they would have any impact, since the decisions in the US were not taken by Congress, and not even by the USPTO, but by a few courts, namely the CAFC.   Proponents of software patents in Europe have repeatedly asserted that the US and Japan have decided in favor of software patentability after some careful and conscious public deliberation process.  The facts don't support that assertion.
</div>

<div class="secthead">
<h2>5. <a name="ref">Further Reading</a></h2>
</div>

<div class="sectbody">
<div class="links">
<dl><dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;">Robert Hart 1997, The Case for Patent Protection for Computer Program-Related Inventions, in: The computer law and security report, 13(4), 247-252.</b></dt>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://www.wws.princeton.edu/~ota/disk1/1992/9215.html">OTA 1992, OTA-TCT-527, Finding a Balance: Computer Software, Intellectual Property and the Challenge of Technological Change</a></b></dt>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://wwwsecure.law.cornell.edu/commentary/intelpro/manifint.htm#intro">Pamela Samuelson et al. 1994, A Manifesto Concerning the Legal Protection of Computer, Columbia Law Review, 94(8), 2308-2431.</a></b></dt>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://www.eff.org/pub/Intellectual_property/ca_softpatent.testimony">United States Patent and Trademark Office, Public Hearing on Use of the Patent System to Protect Software-Related Inventions, Transcript of Proceedings, 1994</a></b></dt>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/papers/digidilem00/bessenhunt03/cec-telecom02/indprop-ipi00/index.en.html">The Economic Impact of Patentability of Computer Programs</a></b></dt>
<dd>Advocacy treatise written by Hart at the order of the European Commission's Industrial Property Unit and guised as an economic study.  It contains only one chapter on economics which provides only evidence against the usefulness of software patents.  The conclusions of the treatise are of course in favor of software patentability.  They have regularly been cited by proponents in the justification parts of pro software patent directive proposals from the European Commission and from MEP Arlene McCarthy.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/papers/eubsa-swpat0202/index.en.html">CEC & BSA 2002-02-20: proposal to make all useful ideas patentable</a></b></dt>
<dd>relies on Hart's &quot;Economic Study&quot;</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/papers/eubsa-swpat0202/opin/juri0304/amccarthy0302/index.en.html">McCarthy 2003-02-19: Amended Software Patent Directive Proposal</a></b></dt>
<dd>relies on Hart's &quot;Economic Study&quot;</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/events/2002/europarl11/index.en.html">Europarl Hearings 2002-11-07 and 26</a></b></dt>
<dd>Robert Hart was one of the invited experts</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="/papers/digidilem00/index.en.html">US National Research Council 2000: The Digital Dilemma</a></b></dt>
<dd>This report by the US National Research Council from July 2000 criticizes that the decision in favor of software patentability was taken by lawcourts without any backing from Congress.</dd>
<dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://www.isoc.asso.fr/PRESSE/ce-brvt.htm">(Réponse de l'Isoc-France à la Consultation sur la Brevetabilité du Logiciel de la Direction Générale du Marché Intérieur de la Commission Européenne)</a></b></dt>
<dd>This consultation paper from the French Section of the Internet Society criticizes the IPI study and specially mentions bad argumentation by Robert Hart within this study.</dd></dl>
</div>
</div>

<div class="secthead">
<h2>6. <a name="auth">Acknowledgements</a></h2>
</div>

<div class="sectbody">
The body of this review was researched and written by Holger Blasum, thanks to hints from Thomas Ebinger.  Editorial responsibility is taken by Hartmut Pilch
</div>
</div>
</div>
<div id="dokped">
<div id="pedarb">
<!--#include virtual="doksrow.en.html"-->
</div>
<!--#include virtual="../../../../../../valid.en.html"-->
<div id="dokadr">
http://swpat.ffii.org/papers/digidilem00/bessenhunt03/cec-telecom02/indprop-ipi00/clsr-rhart97/index.en.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2005/01/04 (2000/12/21)
<a href="/group/index.en.html">Workgroup</a><br>
english version 2004/08/16 by <a href="http://www.ffii.org/~phm">PILCH Hartmut</a>
</div>
</div></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el -->
<!-- mode: html -->
<!-- End: -->

