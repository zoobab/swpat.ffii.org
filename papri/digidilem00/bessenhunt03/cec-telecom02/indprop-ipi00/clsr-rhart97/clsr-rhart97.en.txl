<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Robert Hart 1997: The Case for Patent Protection for Computer
Program-Related Inventions

#descr: Patent lawyers try to create the impression that the United States
introduced software patents after a conscious, public-opinion based
debate. One of the most outspoken pro software patent activists at the
European Commission, Robert Hart, argued the case in 1997 by
misrepresenting the positions of some major patent-critical voices as
being pro software patent. Hart later co-authored an %(q:independent
study) at the order of the European Commission.  Both use
approximately the same methodology.

#Roa: Reinterpreting a manifesto for a sui generis law as justification for
patents

#TsW: Taking what is useful from the OTA report

#Ccs: Conclusion

#Qde: Further Reading

#Awe: Acknowledgements

#AWd: The british patent lawyer Robert Hart is a well-known %(la:long term
advocate) in favor of software patents in Europe.  In 2000 he wrote a
much-quoted %(ip:study) on the subject at the order of the European
Commission.  In 2002, the European Parliament invited him as an expert
to a %(ep:hearing) on the subject.

#TnW: The Hart paper of 1997 was published in a context where the European
Commission was formulating its policy on software patents as expressed
in the %(gp:Greenpaper) and its %(fd:follow-up papers).  Like many of
his colleagues, Hart argues mainly by extension of precedents: 
computer programs are comparable to some currently patentable objects
and should therefore also be patentable:

#eWo: In the paper, the argumentation goes:

#ndi: Introduction

#iWr: limitations of copyright

#ecc: the need for protection beyond copyright

#WWa: what inventions could be patented

#sWe: position of the EPO

#itr: history

#plc: policy

#rWr: trade secrets

#oli: conclusion

#ihi: From this design  it is obvious section 2b is the actual main argument
whether patentability is desirable, so let us check it for scholarly
consistency.)

#Ter: The main arguments are given as quotes from

#egc: Manifesto for sui generis protection and

#Tro: OTA report.

#HWL: Hart summarizes the Columbia Law Review paper %(sm:A Manifesto
Concerning the Legal Protection of Computer Programs) as

#EiW: Effectively the Manifesto considers that there should be (i) copyright
protection for the literal expression of the program code, (ii) patent
protection for those functional features of the computer program and
their application that meet the level of protection required by the
patent system and (iii) an anti-cloning system to protect the program
behavior not protected by the patent system.

#Hsy: However, in the manifesto document, %(q:patents) does not occur in the
table of contents, nor in the conclusion.  The only mention we have
(passim) so far found is %(bc:the issue of a large number of
questionable patents for software related ideas may impede competitive
development and follow-on innovation in the software industry) (p.
2422). The emphasis seems to call for a sui generis property law (like
the semiconductor act, cf pp. 2413-2420). Again, from the manifesto's
conclusion:

#ItW: It is clear one does not even have to join the call for sui generis
semiconducor-like protection desired by that manifesto (so may be of
limited usefulness for those opposed to software protection beyond
copyright), but to state they advocated patents for programs seems
abusive.

#TfW: The conclusions of the %(ot:OTA report) (Section 1, pp. 32ff) are
neutral:

#Hua: However, the section that Hart quotes at length (on p. 132 of the
report in its Section 4) in a way suggesting this also would be the
OTA's direct recommendation, is just a summary of the USPTO arguments
and of no recommendative meaning, in the same manner one could quote
nearly the entire p. 136 of said Section 4 which summarizes objections
to software patentability.

#TlW: To defend Hart, the OTA report is presented in a more balanced way in
his section 2f (p. 251). But the fact that both pillars of the core
%(q:why we need patents) section are hollow leaves a somewhat bitter
taste in the argumentation's overtones %(q:the US know what they are
doing).

#Hll: Hart also does not tell us that at the 1994 USPTO hearings, only a
fraction of software companies (among them Microsoft) argued clearly
in favor of software patents, whereas a larger fraction considered
software patents harmful to business interests (among them Oracle,
Adobe). Of course, there also was a broad field of undecided
statements and statements from law firms in favor of software patents.

#Tmt: Even in 2000 the USPTO conducted a similar hearing.  Yet, however
critical the opinions raised in these hearings, it was always unlikely
that they would have any impact, since the decisions in the US were
not taken by Congress, and not even by the USPTO, but by a few courts,
namely the CAFC.   Proponents of software patents in Europe have
repeatedly asserted that the US and Japan have decided in favor of
software patentability after some careful and conscious public
deliberation process.  The facts don't support that assertion.

#eoW: Advocacy treatise written by Hart at the order of the European
Commission's Industrial Property Unit and guised as an economic study.
 It contains only one chapter on economics which provides only
evidence against the usefulness of software patents.  The conclusions
of the treatise are of course in favor of software patentability. 
They have regularly been cited by proponents in the justification
parts of pro software patent directive proposals from the European
Commission and from MEP Arlene McCarthy.

#sfc: relies on Hart's %(q:Economic Study)

#sfc2: relies on Hart's %(q:Economic Study)

#WoW: Robert Hart was one of the invited experts

#T0l: This report by the US National Research Council from July 2000
criticizes that the decision in favor of software patentability was
taken by lawcourts without any backing from Congress.

#Ttt: This consultation paper from the French Section of the Internet
Society criticizes the IPI study and specially mentions bad
argumentation by Robert Hart within this study.

#Ban: The body of this review was researched and written by Holger Blasum,
thanks to hints from Thomas Ebinger.  Editorial responsibility is
taken by Hartmut Pilch

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: clsr-rhart97 ;
# txtlang: en ;
# multlin: t ;
# End: ;

