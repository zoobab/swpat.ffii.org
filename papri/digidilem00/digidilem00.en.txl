<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: NRC 2000: The Digital Dilemma

#descr: According to this report by the US National Research Council, software
patents were introduced by lawcourt decisions without support from the
legislature, and it seems doubtful whether the patent expansion is
promoting the progress of science and the useful arts, as Congress
intended.  The Court of Appeal of the Federal Circuit (CAFC) has taken
the patent system into %(q:unchartered waters), and the experience of
the software industry suggests that this decision is urgently awaiting
legislative review.

#ArW: As you can see from the table of contents, it is a report by the
United States National Research Council, with participation from the
leading scholars in all fields, including reserachers from IBM and
Microsoft.

#Tei: The aim of the report is to explore possible ways of protecting
intellectual property in a digital world.

#Tti: The report proposes ways of extending copyright, but at the same time
is very critical about applying patents to %(q:information
innovations) --- a good term that maintains the distinction between a
patentable %(q:invention) and immaterial %(q:innovations), which
should have stayed outside the scope of the patent system.

#TtW: The Chapter on p 192-197

#Ttf: The Impact of Granting Patents on Information Innovation

#rWW: reports how the US patent system was gradually broadened by the patent
courts (without legislative support) to include software and business
methods, and analyses a few of the famous cases.  The report
concludes:

#Tel: The effects of this substantial de facto broadening of patent subject
matter to cover information inventions are as yet unclear. Because
this expansion has occurred without any oversight from the legislative
branch and takes patent law into uncharted territories, it would be
worthwhile to study this phenomenon to ensure that the patent
expansion is promoting the progress of science and the useful arts, as
Congress intended.

#Tno: There are many reasons to be concerned. There is first the concern
that the U.S. Patent and Trademark Office lacks sufficient information
about prior art in the fields of information technology, information
design, and business methods more generally to be able to make sound
decisions about the novelty or nonobviousness of claims in these
fields.47 A related concern is the insufficient number of adequately
trained patent examiners and inadequate patent classification schemata
to deal with this new subject matter. The success of the patent system
in promoting innovation in a field depends on the integrity of the
process for granting patents, which in turn depends on adequate
information about the field. Serious questions continue to exist
within the information technology field about the PTO's
software-related patent decisions. A number of legal commentators have
pointed out that allowing these kinds of patents potentially makes
concepts, not technology, the protectable property of the patent
holder, %(q:allow[ing] virtually anything under the sun to win patent
protection).

#Sic: Second, the tradition of independent creation in the field of computer
programming may run counter to assumptions and practices associated
with patents as they are applied to its traditional domains. When
someone patents a component of a manufactured system, for example, it
will generally be possible for the inventor to manufacture that
component or license its manufacture to another firm and reap rewards
from the invention by sale of that component. Rights to use the
invention are cleared by buying the component for installation into a
larger device.

#BWW: But there is little or no market in software components. Programmers
routinely design large and complex systems from scratch. They do so
largely without reference to the patent literature (partly because
they consider it deficient), although they generally respect copyright
and trade secrecy constraints on their work. With tens of thousands of
programmers writing code that could well infringe on hundreds of
patents without their knowing it, there is an increased risk of
inadvertent infringement.  An added disincentive to searching the
patent literature is the danger that learning about an existing patent
would increase the risk of being found to be a willful infringer. The
patent literature may thus not be providing to the software world one
of its traditional purposes--providing information about the evolving
state of the art. Much the same could be said about the mismatch
between patents and information inventions in general.

#Tts: Third, although patents seem to have been quite successful in
promoting investments in the development of innovative manufacturing
and other industrial technologies and processes, it is possible that
they will not be as successful in promoting innovation in the
information economy. One concern is that the pace of innovation in
information industries is so rapid, and the gears of the patent system
are so slow, that patents may not promote innovation in information
industries as well as they have done in the manufacturing economy. The
market cycle for an information product is often quite short--18
months is not unusual; thus, a patent may well not issue until the
product has become obsolete. If information inventions continue to
fall within the scope of patents, then, at a minimum, the patent
cycle-time needs to be improved significantly. Patent classification
systems for information innovations may also be more difficult to
develop and maintain in a way that will inform and contribute to the
success of the fields they serve.

#Oir: One final reason for concern is that developing and deploying software
and systems may cease to be a cottage industry because of the need for
access to cross-licensing agreements and the legal protection of large
corporations. This in turn may have deleterious effects on the
creativity of U.S. software and Internet industries.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: digidilem00 ;
# txtlang: en ;
# multlin: t ;
# End: ;

