<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: William Kingston from the School of Business Studies at Trinity College in Dublin: %(q:[Patents] have never been indispensable for [protecting information], and indeed there are indications that - except for chemicals - they are now becoming even less important than they were.) %(q:By protecting invention directly, the modern system protects innovation only indirectly. What protection an innovation gets, therefore, depends upon how strong the link is between it and its related invention. This goes far to explain the differential performance of the patent system according to the subject matter of inventions. In chemicals this link is strong, so patents work much better for them than  they do for mechnical and electrical inventions, where the link is weak) (p.355). Kingston argues for a system of more broad but more short-term and investment-dependent monopolies.
title: Patent Protection for Modern Technologies

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/swpatpapri.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: ipq-kingston97 ;
# txtlang: en ;
# End: ;

