<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: Die Autorin ist Sinologin mit Nebenfach Informatikrecht am Institut
für Informatik der Universität Zürich.  Sie hat rechtliche Quellen und
Doktrinen systematisch aufgearbeitet und übersichtlich dargestellt. 
Insbesondere hat sid den Technikbegriff des BGH verständlich
dargestellt.  Um emotionslose Äquidistanz bemüht, lässt sie dabei
manchmal Argumente aus dem EPA-Freundeskreis recht unkritisch
durchgehen (übernimmt z.B. deren Behauptung, die Änderung der
EPA-Rechtsprechung sei durch Änderung der technischen Inhalte
motiviert, nimmt Patentabteilungs-Stellungnahmen im Namen grosser
Organisationen für bare Münze etc), bietet dennoch eine sehr
empfehlenswerte Einführung in die Thematik und auch einen erhellenden
Überblick über die Swpat-Konsultation auf EU-Ebene.

#Tsi: Schlichttextversion

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: unizh-guetlin01 ;
# txtlang: de ;
# multlin: t ;
# End: ;

