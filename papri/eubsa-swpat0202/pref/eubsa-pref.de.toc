\contentsline {chapter}{\numberline {1}``BEGR\"{U}NDUNG''}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}VORGESCHICHTE: SONDIERUNGEN DURCH DIE KOMMISSION}{7}{chapter.2}
\contentsline {chapter}{\numberline {3}``INTERNATIONALER WETTBEWERB: DIE RECHTSLAGE IN DEN USA UND IN JAPAN''}{13}{chapter.3}
\contentsline {chapter}{\numberline {4}``DERZEITIGE RECHTSLAGE IM ZUSAMMENHANG MIT ARTIKEL 52 ABSATZ 1 UND 2 EP\"{U}''}{17}{chapter.4}
\contentsline {chapter}{\numberline {5}``Funktion von Algorithmen''}{20}{chapter.5}
\contentsline {chapter}{\numberline {6}``Patent- und Urheberrechtschutz erg\"{a}nzen sich''}{22}{chapter.6}
\contentsline {chapter}{\numberline {7}``NOTWENDIGKEIT EINER GEMEINSCHAFTSMA{\ss }NAHME ZUR HARMONISIERUNG DER NATIONALEN RECHTSVORSCHRIFTEN UND RECHTSGRUNDLAGE DIESER MA{\ss }NAHME''}{25}{chapter.7}
\contentsline {chapter}{\numberline {8}``GEW\"{A}HLTER ANSATZ''}{29}{chapter.8}
\contentsline {chapter}{\numberline {9}``RECHTSGRUNDLAGE F\"{U}R DIE HARMONISIERUNG''}{33}{chapter.9}
\contentsline {chapter}{\numberline {10}``Erl\"{a}uterung der einzelnen Artikeln (sic!) der Richtlinie''}{34}{chapter.10}
\contentsline {section}{\numberline {10.1}Artikel 1}{34}{section.10.1}
\contentsline {section}{\numberline {10.2}Artikel 2}{34}{section.10.2}
\contentsline {section}{\numberline {10.3}Artikel 3}{35}{section.10.3}
\contentsline {section}{\numberline {10.4}Artikel 4}{35}{section.10.4}
\contentsline {section}{\numberline {10.5}Artikel 5}{38}{section.10.5}
\contentsline {section}{\numberline {10.6}Artikel 6}{39}{section.10.6}
\contentsline {section}{\numberline {10.7}Artikel 7}{39}{section.10.7}
\contentsline {section}{\numberline {10.8}Artikel 8}{40}{section.10.8}
\contentsline {section}{\numberline {10.9}Artikel 9, 10 und 11}{40}{section.10.9}
