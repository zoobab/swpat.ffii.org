\select@language {french}
\select@language {french}
\contentsline {chapter}{\numberline {1}``EXPOS\'{E} DES MOTIFS''}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}FONDEMENT DE L'INITIATIVE\@DP LA CONSULTATION DE LA COMMISSION}{7}{chapter.2}
\contentsline {chapter}{\numberline {3}``CONCURRENCE INTERNATIONALE\@DP LA SITUATION JURIDIQUE AUX \'{E}TATS-UNIS ET AU JAPON''}{13}{chapter.3}
\contentsline {chapter}{\numberline {4}``LA SITUATION JURIDIQUE ACTUELLE CONCERNANT L'ARTICLE 52, PARAGRAPHES 1 ET 2 DE LA CONVENTION SUR LE BREVET EUROP\'{E}EN''}{17}{chapter.4}
\contentsline {chapter}{\numberline {5}``Le r\^{o}le de l'algorithme''}{20}{chapter.5}
\contentsline {chapter}{\numberline {6}``La protection par brevet et la protection par droit d'auteur sont compl\'{e}mentaires''}{22}{chapter.6}
\contentsline {chapter}{\numberline {7}``N\'{E}CESSIT\'{E} D'UNE MESURE COMMUNAUTAIRE HARMONISANT LES DROITS INTERNES ET FONDEMENT JURIDIQUE D'UNE TELLE MESURE''}{25}{chapter.7}
\contentsline {chapter}{\numberline {8}``LA D\'{E}MARCHE ADOPT\'{E}E''}{29}{chapter.8}
\contentsline {chapter}{\numberline {9}``BASE JURIDIQUE DE L'HARMONISATION''}{33}{chapter.9}
\contentsline {chapter}{\numberline {10}``EXPLICATION DE LA DIRECTIVE ARTICLE PAR ARTICLE''}{34}{chapter.10}
\contentsline {section}{\numberline {10.1}Article 1}{34}{section.10.1}
\contentsline {section}{\numberline {10.2}Article 2}{34}{section.10.2}
\contentsline {section}{\numberline {10.3}Article 3}{35}{section.10.3}
\contentsline {section}{\numberline {10.4}Article 4}{35}{section.10.4}
\contentsline {section}{\numberline {10.5}Article 5}{37}{section.10.5}
\contentsline {section}{\numberline {10.6}Article 6}{38}{section.10.6}
\contentsline {section}{\numberline {10.7}Article 7}{39}{section.10.7}
\contentsline {section}{\numberline {10.8}Article 8}{39}{section.10.8}
\contentsline {section}{\numberline {10.9}Articles 9, 10 et 11}{39}{section.10.9}
