\contentsline {chapter}{\numberline {1}``EXPLANATORY MEMORANDUM''}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}The background to the initiative: Commission's consultations}{7}{chapter.2}
\contentsline {chapter}{\numberline {3}``International competition: The legal situation in the U.S. and Japan''}{13}{chapter.3}
\contentsline {chapter}{\numberline {4}``The current legal situation regarding art. 52(1) and (2) of the epc.''}{17}{chapter.4}
\contentsline {chapter}{\numberline {5}``The role of algorithms''}{20}{chapter.5}
\contentsline {chapter}{\numberline {6}``Patent and copyright protection are complementary''}{22}{chapter.6}
\contentsline {chapter}{\numberline {7}``The necessity of a Community action harmonising national laws and its legal basis''}{25}{chapter.7}
\contentsline {chapter}{\numberline {8}``The approach adopted''}{28}{chapter.8}
\contentsline {chapter}{\numberline {9}``The legal basis for harmonisation''}{32}{chapter.9}
\contentsline {chapter}{\numberline {10}``Explanation of the Directive article by article''}{33}{chapter.10}
\contentsline {section}{\numberline {10.1}Article 1}{33}{section.10.1}
\contentsline {section}{\numberline {10.2}Article 2}{33}{section.10.2}
\contentsline {section}{\numberline {10.3}Article 3}{34}{section.10.3}
\contentsline {section}{\numberline {10.4}Article 4}{34}{section.10.4}
\contentsline {section}{\numberline {10.5}Article 5}{37}{section.10.5}
\contentsline {section}{\numberline {10.6}Article 6}{38}{section.10.6}
\contentsline {section}{\numberline {10.7}Article 7}{38}{section.10.7}
\contentsline {section}{\numberline {10.8}Article 8}{38}{section.10.8}
\contentsline {section}{\numberline {10.9}Articles 9, 10 and 11}{39}{section.10.9}
