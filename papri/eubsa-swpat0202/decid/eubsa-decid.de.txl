<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Entscheidungsfindung in den EG-Institutionen

#descr: The directive will be decided jointly by the European Parliament and the European Council.  The legal details are found in various EC treaties and.   We are trying to find out and document the intricacies of this Process.

#Egc: EuroParl meeting on Swpat Directive 2002-06-04

#Poa: Preparatory text for a meeting of MEPs with representatives from the European Commission.  Tries to create the impression that the Commission's proposal intends to restrict patentability.

#Crk: Comittee of Juridical Affairs and interior market 2002-02-19: parlamentary report

#TWi: This committee will also work on the eubsa directive proposal.  It seems they amended the Community Patent to support more languages, give some more play to national PTOs and ...(bc:Moreover, MEPs said that a system of quality control should be put in place under the authority of the European Commission in collaboration with the European Patent Office.) QC is good, but those two institutions do have a terrible record on helping it. Subject matter could be part of quality control. Unfortunately, in PTO doublespeak, %(q:quality control) may also be limited to the meaning of %(q:assuring that prior art search is good enough, so that most granted patents can be upheld in EPO courts and other courts that operate under the same low-quality rules as the EPO).  If anybody has more details please let us know. It sounds very vague.

#Wtc: Das Entscheidungsverfahren

#Shd: Schedule

#ceo: concerned europarl commissions

#Pri: Parties

#Iyi: Setting up Inquiry Commissions

#uti: Einfluss nationaler Parlamente

#Wip: Richtlininenentwurfs der Kommission

#eaW: Zur Rechtssetzung kommt das %(q:Mitentscheidung gem. Art. 251 EG-Vertrag) zur Anwendung, weil es eine Materie ist, die dem Binnenmarkt (Art. 95 EG-Vertrag) unterliegt.

#bie: Hintergrundinfos zu den Arten der Rechtssetzungsverfahren

#teo: Zum konkreten Ablauf eines Mitentscheidungsverfahrens

#eMW: hier muss man bei der einfachen Suche eingeben: COM 2002 92 (das sind die Kennziffern des KOM-Vorschlages

#aiW: Under codecision, Parliament can only accept, reject or amend the Commission's proposal.  If it is rejected, Parliament could still call on the Commission to produce a new proposal.  It is always open to the Commission itself to withdraw its proposal and submit a new one.

#WeW: Once the draft report has been submitted to the Legal Affairs Committee, a deadline will be open for members to submit amendments.  Those amendments, together with the ones contained in the draft report and those contained in the opinions of the Industry and Cultural Committees will be put to the vote.

#bWi: If a large number of amendments is tabled, it is possible that the committee will ask the rapporteur to try to reach a compromise on the amendments.  The report as adopted will then go to the plenary session, where there will be a further opportunity to submit amendments (although there are rules which mean in practice that only political groups, the lead committee or groups of members may submit amendments).  That will conclude the first reading.

#mis: Parliament will then await the Council's common position and any amended proposal from the Commission (the Commission usually limits itself to saying which amendments submitted by Parliament it is prepared to endorse)  Once the common position has been announced in the plenary session, Parliament will have three (possibly four) months.  In that period Ms McCarthy will produce a draft recommendation for second reading for adoption by the Legal Affairs Committee.  There will be no further opinions from the other committees.  At second reading, Paliament's Rules of Procedure limit the scope of amendments broadly to reinstating what it sought at first reading and/or amending parts of the common position which are new as compared with the original Commission proposal.

#lWW: Council Working Party position paper published

#UHi: JURI Hearing

#vne: plenary vote in the europarl

#gdc: possibly leading to further negotiations with CEU and CEC, return to CEC (in case of rejection) or ratification

#Ojy: On the software patentability directive, COM(2002)92  2002/0047, at %(URL) they used to say %(OLD). Now they say %(NEW).

#frn: few clear opinions so far

#Gvi: Green Party and Conservative youth organisations seem to be against

#Pcl: Patent lobby within large parties seem to be well organised and particularly influential in the two concerned committees.  They may try to keep silence and then sudenly come up with a conclusion that looks like a compromise but is pro swpat and push this through the europarl very quickly in september.

#Tfj: Treaty of the EU, art. 193:

#Iil: In the course of its duties, the European Parliament may, at the request of a quarter of its Members, set up a temporary Committee of Inquiry to investigate, without prejudice to the powers conferred by this Treaty on other institutions or bodies, alleged contraventions or maladministration in the implementation of Community law, except where the alleged facts are being examined before a court and while the case is still subject to legal proceedings.

#TqW: The temporary Committee of Inquiry shall cease to exist on the submission of its report.

#TiW: The detailed provisions governing the exercise of the right of inquiry shall be determined by common accord of the European Parliament, the Council and the Commission.

#Iil2: It could inquire the Comission game or investigate certain EPO abuses but hardly monitor the patent system permanently.

#pWg: Man muss zwei Wege unterscheiden:

#Wtg: der Bundestag behandelt ein Thema im Rahmen eines Gesetzgebungsverfahrens.

#naW: er befasst sich allgemein politisch mit einem Thema.

#iWo: Gesetzgebung kommt nur in Frage, nachdem die RiLi in der EU beschlossen ist. Dann haben die Mitgliedstaaten eine Umsetzungsfrist für die Anpassung nationaler Gesetze zu beachten. Dies erfolgt in der Regel durch einen Gesetzentwurf der Bundesregierung. Dann läuft die ganze Maschinerie an, man kann aber inhaltlich an der RiLi nichts ändern, die Umsetzung im nationalen Recht hat zwar etwas Spielraum, kann aber nicht in ganz andere Richtung gehen.  Sonst würde die Kommission Deutschland vor den EuGH zerren.

#iae: Allgemeinpolitische Befassung ist jederzeit möglich. Ausgangspunkt könnte eine Befassung eines Ausschusses sein. Es besteht ein etabliertes Verfahren zur Unterrichtung des Bundestages über Vorhaben in der EU, eben damit man nicht erst mit den Auswirkungen befasst wird.

#unb: Wenn sich (vielleicht auch verschiedene Ausschüsse) mit dem Thema befasst haben, könnte eine Fraktion einen Entschließungsantrag einbringen und dadurch seinen Willen im hinblick auf die RiLi äußern. Aber: auch wenn der Bundestag diese Entschließung annimmt, ist er für die Bundesregierung als Verhandlungsführerin in Brüssel nicht verbindlich und hat keinerlei aufschiebende Wirkung für das Verfahren in Brüssel.  Es läge jedoch im Sinne der Gewaltenteilung, wenn die Bundesregierung in einer Frage der europäischen Gesetzgebung dem Bundestag folgen würde.

#Csr: Codecision Procedure

#IeD: Infos in Catalonian, collected by Xavi Drudis Ferran

#ife: includes a guide to the codecision procedure et al in graphical form.  %(EURIM) is a UK-based lobby group that has MEPs and large companies as members and considers consensus-finding about e-commerce related EU legislation as its mission.  It could have a major impact on the processes within the Europarl.  The EURIM newsletters have strange characters in their filenames which makes them inaccessible at least from some browsing platforms.

#Ifg: It seems the first vote is in the European Parliament by %(q:qualified majority). But even if they reject it, the Council (ministers of member states) has a say and may have it voted again in Parliament, this time with simple majority (but absolute majority for ammending it).

#TWs2: The opinions of the EuroParliament in the co-decision procedure are adopted by simple majority voting, I think.  A qualified majority is about voting in the Council (heads of governments).  Open debates in the EuroParl and amendments can change everything.

#EuCl: European Council

#EuCn: European Commission

#EuPa: European Parliament

#TeW: This algorithm may infringe on existing EPO patents.  Please tell us if you know any such patent.  An improved codification of the algorithm in can be found at %(URL).

#Moa: Mail Thread about decision making procedure

#HWo: How is the decision procedure affected by the fact that basic freedoms and other constitutional questions are involved here?

#WmW: What other Europarl Committees are involved?  Who is reporting to them?

#PWs: Study the procedure and improve the co-decision algorithm shown above!

#Woe: What else should we ask in order to make this procedure as transparent as possible?

#Wei: Where do we find a list of MEPs?

#Ptl: Prepare for our supporters (e.g. 100k petition signatories) a set of ressources that will enable them to talk to their representatives in a meaningful way

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-decid ;
# txtlang: de ;
# multlin: t ;
# End: ;

