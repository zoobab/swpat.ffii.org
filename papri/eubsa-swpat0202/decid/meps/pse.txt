1.Sr. BAR�N CRESPO, Enrique , Presidente
Espa�a
1: ebaroncrespo@europarl.eu.int


2.Herr SCHULZ, Martin , Vicepresidente
Rep�blica Federal de Alemania


3.Mr MURPHY, Simon Francis , Vicepresidente
Reino Unido de Gran Breta�a e Irlanda del Norte


4.Mme BERES, Pervenche , Vicepresidente
Francia
4: pberes@europarl.eu.int


5.Sig.ra NAPOLETANO, Pasqualina , Vicepresidente
Italia


6.Sr. LAGE, Carlos , Vicepresidente
Portugal


7.?. KATIFORIS, Giorgos , Vicepresidente
Grecia
7: recife@attglobal.net


8.Herr SWOBODA, Johannes (Hannes) , Vicepresidente
Austria


9.De Heer van den BERG, Margrietus J. , Vicepresidente
Pa�ses Bajos
9: mvandenberg@europarl.eu.int


10.Hr. ANDERSSON, Jan , Vicepresidente
Suecia


11.M. DEHOUSSE, Jean-Maurice , Vicepresidente
B�lgica


12. MYLLER, Riitta , Vicepresidente
Finlandia


13.Hr. LUND, Torben , Vicepresidente
Dinamarca


14.M. GOEBBELS, Robert , Vicepresidente
Luxemburgo
14: rgoebbels@europarl.eu.int


15.Mr DE ROSSA, Proinsias , Vicepresidente
Irlanda


16.Sr. OBIOLS i GERM�, Raimon , Vicepresidente
Espa�a


17.Frau ROTHE, Mechtild , Miembro de la mesa
Rep�blica Federal de Alemania


18.Mrs McAVAN, Linda , Miembro de la mesa
Reino Unido de Gran Breta�a e Irlanda del Norte


19.On. BOSELLI, Enrico , Miembro de la mesa
Italia


20.Sra. D�EZ GONZ�LEZ, Rosa M. , Miembro de la mesa
Espa�a


21.M. DUHAMEL, Olivier , Miembro de la mesa
Francia
21: oldu@club-internet.fr
21: oduhamel@europarl.eu.int


22.Sra. DAMI�O, Elisa Maria , Miembro de la mesa
Portugal


23.Sig.ra GHILARDOTTI, Fiorella , Miembro de la mesa
Italia
23: fiorella.ghilardotti@stcom.com


24.Frau BERGER, Maria , Miembro de la mesa
Austria
24: maberger@europarl.eu.int


25.?? KARAMANOU, Anna , Miembro de la mesa
Grecia


26.Mevr. VAN LANCKER, Anne E.M. , Miembro de la mesa
B�lgica
26: avanlancker@europarl.eu.int


27.Mme ROURE, Martine , Miembro de la mesa
Francia
27: martine.roure.deputee@wanadoo.fr


28.Mr HUME, John , Miembro de la mesa
Reino Unido de Gran Breta�a e Irlanda del Norte


29.Mr MARTIN, David W. , Miembro de la mesa
Reino Unido de Gran Breta�a e Irlanda del Norte
29: martin@martinmep.com


30.On. IMBENI, Renzo , Miembro de la mesa
Italia
30: rimbeni@europarl.eu.int
30: imbeni@bologna.pds.it


31.Mme LALUMIERE, Catherine , Miembro de la mesa
Francia


32.Sr. COLOM i NAVAL, Joan , Miembro de la mesa
Espa�a
32: jcolom@europarl.eu.int
32: opepsc@readysoft.es


33.Herr SCHMID, Gerhard , Miembro de la mesa
Rep�blica Federal de Alemania


34.M. POOS, Jacques F. , Miembro de la mesa
Luxemburgo
34: jpoos@europarl.eu.int


35. IIVARI, Ulpu , Tesorero
Finlandia
35: uiivari@europarl.eu.int


36.Mr ADAM, Gordon J. , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
36: gadam@europarl.eu.int
36: gadammep@aol.com


37.Sr. APARICIO S�NCHEZ, Pedro , Miembro
Espa�a


38.?. BALTAS, Alexandros , Miembro
Grecia


39.Sr. BERENGUER FUSTER, Luis , Miembro
Espa�a
39: ana@hpr2.es


40.Herr B�SCH, Herbert , Miembro
Austria
40: hboesch@europarl.eu.int


41.Mr BOWE, David Robert , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte


42.Herr BULLMANN, Hans Udo , Miembro
Rep�blica Federal de Alemania
42: ubullmann@europarl.eu.int


43.Mevr. van den BURG, Ieke , Miembro
Pa�ses Bajos


44.Sr. CAMPOS, Ant�nio , Miembro
Portugal


45.Sr. CANDAL, Carlos , Miembro
Portugal


46.Mme CARLOTTI, Marie-Arlette , Miembro
Francia


47.Sr. CARNERO GONZ�LEZ, Carlos , Miembro
Espa�a


48.On. CARRARO, Massimo , Miembro
Italia


49.Sra. CARRILHO, Maria , Miembro
Portugal


50.Sr. CASACA, Paulo , Miembro
Portugal
50: pcasaca@europarl.eu.int


51.Mr CASHMAN, Michael , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte


52.M. CAUDRON, G�rard , Miembro
Francia
52: gcaudron@nordnet.fr


53.Sr. CERCAS, Alejandro , Miembro
Espa�a


54.Sra. CERDEIRA MORTERERO, Carmen , Miembro
Espa�a


55.Herr CEYHUN, Ozan , Miembro
Rep�blica Federal de Alemania
55: Ceyhun.mdep@t-online.de


56.Mr CORBETT, Richard , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
56: rcorbett@europarl.eu.int


57.Mevr. CORBEY, Dorette , Miembro
Pa�ses Bajos


58.Mme DARRAS, Danielle , Miembro
Francia


59.M. DARY, Michel J.M. , Miembro
Francia


60.Mme DE KEYSER, V�ronique , Miembro
B�lgica
60: vdekeyser@ulg.ac.be
60: vdekeyser@europarl.eu.int


61.M. D�SIR, Harlem , Miembro
Francia


62.Sra. D�HRKOP D�HRKOP, B�rbara , Miembro
Espa�a
62: bduhrkop@europarl.eu.int


63.Herr DUIN, Garrelt , Miembro
Rep�blica Federal de Alemania
63: gduin@garreltduin.de
63: gduin@europarl.eu.int


64.Herr ETTL, Harald , Miembro
Austria


65.Mr EVANS, Robert J.E. , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte


66.Hr F�RM, G�ran , Miembro
Suecia
66: gfarm@europarl.eu.int
66: goran.farm@norrkoping.se


67.On. FAVA, Giovanni Claudio , Miembro
Italia


68.Mme FERREIRA, Anne , Miembro
Francia
68: ferreira.anne@wanadoo.fr
68: anferreira@europarl.eu.int


69.Mr FORD, Glyn , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
69: penny_richardson@new.labour.org.uk


70.M. FRUTEAU, Jean-Claude , Miembro
Francia


71.M. GAROT, Georges , Miembro
Francia


72.Frau GEBHARDT, Evelyne , Miembro
Rep�blica Federal de Alemania
72: egebhardt@europarl.eu.int
72: egebhardt.mdep@t-online.de


73.Mrs GILL, Neena , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte


74.Mme GILLIG, Marie-H�l�ne , Miembro
Francia
74: mhgillig@europarl.eu.int


75.Herr GLANTE, Norbert , Miembro
Rep�blica Federal de Alemania
75: nglante@europarl.eu.int
75: norbert@glante.de


76.Herr G�RLACH, Willi , Miembro
Rep�blica Federal de Alemania
76: wgoerlach@europarl.eu.int
76: europabuero.hessen-sued@spd.de


77.Frau GR�NER, Lissy , Miembro
Rep�blica Federal de Alemania
77: lgroener@europarl.eu.int


78.Mme GUY-QUINT, Catherine , Miembro
Francia
78: c.guyquint@wanadoo.fr


79.Herr H�NSCH, Klaus , Miembro
Rep�blica Federal de Alemania
79: klaus.haensch@spd.de
79: khaensch@europarl.eu.int


80.Frau HAUG, Jutta D. , Miembro
Rep�blica Federal de Alemania


81.Mme HAZAN, Adeline , Miembro
Francia
81: ahazan@wanadoo.fr
81: ahazan@europarl.eu.int


82.Fru HEDKVIST PETERSEN, Ewa , Miembro
Suecia
82: ehedkvist@europarl.eu.int


83.Frau HOFF, Magdalene , Miembro
Rep�blica Federal de Alemania


84.Mrs HONEYBALL, Mary , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte


85.Mr HOWITT, Richard , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
85: richard.howitt@geo2.poptel.org.uk


86.Mr HUGHES, Stephen , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte


87.De Heer van HULTEN, Michiel , Miembro
Pa�ses Bajos
87: mvanhulten@europarl.eu.int


88.Fru HULTH�N, Anneli , Miembro
Suecia


89.Sr. IZQUIERDO COLLADO, Juan de Dios , Miembro
Espa�a


90.Sra. IZQUIERDO ROJO, Mar�a , Miembro
Espa�a
90: mizquierdo@europarl.eu.int


91.Frau J�NS, Karin , Miembro
Rep�blica Federal de Alemania
91: kjoens@europarl.eu.int


92.Frau JUNKER, Karin , Miembro
Rep�blica Federal de Alemania
92: kjunker@europarl.eu.int


93.Hr. KARLSSON, Hans , Miembro
Suecia
93: hans.Karlsson@Sverige.nu
93: hKarlsson@europarl.eu.int


94.Frau KESSLER, Margot , Miembro
Rep�blica Federal de Alemania


95.Herr KINDERMANN, Heinz , Miembro
Rep�blica Federal de Alemania
95: Dr.Heinz.Kindermann@t-online.de
95: hkindermann@europarl.eu.int


96.Mrs KINNOCK, Glenys , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
96: gkinnock@europe-wales.new.labour.org.uk


97.?. KOUKIADIS, Ioannis , Miembro
Grecia


98.Frau KREHL, Constanze Angela , Miembro
Rep�blica Federal de Alemania
98: ckrehl@europarl.eu.int.


99.Herr KREISSL-D�RFLER, Wolfgang , Miembro
Rep�blica Federal de Alemania


100.Herr KUCKELKORN, Wilfried , Miembro
Rep�blica Federal de Alemania
100: Christa.Wirtgen@t-online.de
100: wkuckelkorn@europarl.eu.int


101.Herr KUHNE, Helmut , Miembro
Rep�blica Federal de Alemania
101: hkuhne@europarl.eu.int
101: kuhne.mdep@t-online.de


102.Herr LANGE, Bernd , Miembro
Rep�blica Federal de Alemania
102: bernd.lange@spd.de
102: blange@europarl.eu.int


103.On. LAVARRA, Vincenzo , Miembro
Italia


104.Herr LEINEN, Jo , Miembro
Rep�blica Federal de Alemania


105.Herr LINKOHR, Rolf , Miembro
Rep�blica Federal de Alemania
105: rlinkohr@europarl.eu.int


106.Mrs McCARTHY, Arlene , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte


107.Mrs McNALLY, Eryl Margaret , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
107: emcnally@europarl.eu.int


108.?? MALLIORI, Minerva Melpomeni , Miembro
Grecia


109.Frau MANN, Erika , Miembro
Rep�blica Federal de Alemania
109: emann@europarl.eu.int


110.Sr. MARINHO, Lu�s , Miembro
Portugal
110: lmarinho@europarl.eu.int
110: np62uj@mail.telepac.pt


111.Herr MARTIN, Hans-Peter , Miembro
Austria
111: hpmartin@europarl.eu.int


112.Sr. MART�NEZ MART�NEZ, Miguel Angel , Miembro
Espa�a


113.?. MASTORAKIS, Emmanouil , Miembro
Grecia


114.Sr. MEDINA ORTEGA, Manuel , Miembro
Espa�a


115.Sr. MENDILUCE PEREIRO, Jos� Mar�a , Miembro
Espa�a
115: jmendiluce@europarl.eu.int


116.Sr. MEN�NDEZ del VALLE, Emilio , Miembro
Espa�a


117.Sra. MIGU�LEZ RAMOS, Rosa , Miembro
Espa�a


118.Mr MILLER, Bill , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte


119.Mr MORAES, Claude , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte


120.Mrs MORGAN, Eluned , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
120: emorgan@europe-wales.new.labour.org.uk
120: emorgan@europarl.eu.int


121.Frau M�LLER, Rosemarie , Miembro
Rep�blica Federal de Alemania


122.M. NAIR, Sami , Miembro
Francia


123.On. NAPOLITANO, Giorgio , Miembro
Italia


124.Mrs O'TOOLE, Barbara , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
124: botoolemep2@aol.com
124: botoole@europarl.eu.int


125. PAASILINNA, Reino , Miembro
Finlandia
125: rpaasilinna@europarl.eu.int


126.Sig.ra PACIOTTI, Elena Ornella , Miembro
Italia
126: epaciotti@europarl.eu.int


127.Mme PATRIE, B�atrice , Miembro
Francia


128.Sr. P�REZ ROYO, Fernando , Miembro
Espa�a


129.Herr PIECYK, Wilhelm Ernst , Miembro
Rep�blica Federal de Alemania
129: willi.piecyk-lv-schleswig-holstein@spd.de


130.On. PITTELLA, Giovanni , Miembro
Italia
130: pit@income.it


131.M. POIGNANT, Bernard , Miembro
Francia
131: bernard.poignant@wanadoo.fr


132.Frau PRETS, Christa , Miembro
Austria
132: eu-buero.prets@members.at


133.Frau RANDZIO-PLATH, Christa , Miembro
Rep�blica Federal de Alemania
133: crandzio@europarl.eu.int
133: c.randzio-plath.mdep@t-online.de


134.Herr RAPKAY, Bernhard , Miembro
Rep�blica Federal de Alemania
134: brapkay@europarl.eu.int


135.Mrs READ, Imelda Mary , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
135: readm@labmeps-emids.fsnet.co.uk


136.M. ROCARD, Michel , Miembro
Francia


137.Sra. RODR�GUEZ RAMOS, Mar�a , Miembro
Espa�a


138.Frau ROTH-BEHRENDT, Dagmar , Miembro
Rep�blica Federal de Alemania


139.Herr ROTHLEY, Willi , Miembro
Rep�blica Federal de Alemania


140.On. RUFFOLO, Giorgio , Miembro
Italia


141.On. SACCONI, Guido , Miembro
Italia
141: gsacconi@europarl.eu.int
141: sacconi@toscanaeuropa.it


142.Herr SAKELLARIOU, Jannis , Miembro
Rep�blica Federal de Alemania
142: jsakellariou@europarl.eu.int


143.Sr. dos SANTOS, Manuel Ant�nio , Miembro
Portugal
143: msantos@ps.parlamento.pt
143: manuel.dos.santos@oninet.pt


144.Sra. SAUQUILLO P�REZ DEL ARCO, Francisca , Miembro
Espa�a


145.M. SAVARY, Gilles , Miembro
Francia
145: gsavary@europarl.eu.int
145: gilles-savary@wanadoo.fr


146.M. SCARBONCHI, Michel-Ange , Miembro
Francia
146: contact@scarbonchi.org
146: mscarbonchi@europarl.eu.int


147.Frau SCHEELE, Karin , Miembro
Austria
147: kscheele@europarl.eu.int


148.Mr SIMPSON, Brian , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
148: briansimpson@lab.u-net.com
148: bsimpson@europarl.eu.int


149.Mr SKINNER, Peter William , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
149: pskinner@europarl.eu.int


150.Sr. SOARES, M�rio , Miembro
Portugal


151.Sra. SORNOSA MART�NEZ, Mar�a , Miembro
Espa�a


152.?. SOULADAKIS, Ioannis , Miembro
Grecia
152: souladakis@ath.forthnet.gr


153.Sr. SOUSA PINTO, S�rgio , Miembro
Portugal


154.Mrs STIHLER, Catherine , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
154: cstihler@europarl.eu.int


155.Herr STOCKMANN, Ulrich , Miembro
Rep�blica Federal de Alemania


156.Mevr. SWIEBEL, Joke , Miembro
Pa�ses Bajos
156: jswiebel@europarl.eu.int


157.Sra. TERR�N i CUS�, Anna , Miembro
Espa�a
157: aterron@europarl.eu.int


158.Fru THEORIN, Maj Britt , Miembro
Suecia


159.Fru THORNING-SCHMIDT, Helle , Miembro
Dinamarca
159: hthorning@europarl.eu.int


160.Mr TITLEY, Gary , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
160: gtitley@europarl.eu.int


161.Sra. TORRES MARQUES, Helena , Miembro
Portugal
161: htorresmarques@europarl.eu.int


162.On. TRENTIN, Bruno , Miembro
Italia
162: btrentineu@yahoo.it
162: btrentin@europarl.eu.int


163.?. TSATSOS, Dimitris , Miembro
Grecia


164.Sr. VAIRINHOS, Joaquim , Miembro
Portugal


165.Sra. VALENCIANO MART�NEZ-OROZCO, Mar�a Elena , Miembro
Espa�a
165: evalenciano@europarl.eu.int
165: fund_mujeres@infornet.es


166.Mevr. VAN BREMPT, Kathleen , Miembro
B�lgica
166: kvanbrempt@europarl.eu.int


167.On. VATTIMO, Gianni , Miembro
Italia


168.On. VELTRONI, Walter , Miembro
Italia
168: wveltroni@europarl.eu.int
168: w.veltroni@democraticidisinistra.it


169.On. VOLCIC, Demetrio , Miembro
Italia
169: volcic@tmedia.it


170.Herr WALTER, Ralf , Miembro
Rep�blica Federal de Alemania
170: rwalter@europarl.eu.int


171.Mr WATTS, Mark Francis , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte


172.Frau WEILER, Barbara , Miembro
Rep�blica Federal de Alemania
172: bweiler@europarl.eu.int


173.Sr. WESTENDORP Y CABEZA, Carlos , Miembro
Espa�a


174.Mr WHITEHEAD, Phillip , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
174: pwhitehead@europarl.eu.int
174: whiteheadp@labmeps-emids.fsnet.co.uk


175.De Heer WIERSMA, Jan Marinus , Miembro
Pa�ses Bajos


176.Mr WYNN, Terence , Miembro
Reino Unido de Gran Breta�a e Irlanda del Norte
176: twynn@europarl.eu.int


177.M. ZIMERAY, Fran�ois , Miembro
Francia


178.?? ZORBA, Myrsini , Miembro
Grecia
178: mzorba@europarl.eu.int


179.Mme ZRIHEN, Olga , Miembro
B�lgica
179: ozrihen@europarl.eu.int


