\begin{subdocument}{eubsa-decid}{Entscheidungsfindung in den EG-Institutionen}{http://swpat.ffii.org/papiere/eubsa-swpat0202/decid/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{The directive will be decided jointly by the European Parliament and the European Council.  The legal details are found in various EC treaties and.   We are trying to find out and document the intricacies of this Process.}
\begin{sect}{kode}{Das Entscheidungsverfahren}
Under codecision, Parliament can only accept, reject or amend the Commission's proposal.  If it is rejected, Parliament could still call on the Commission to produce a new proposal.  It is always open to the Commission itself to withdraw its proposal and submit a new one.

Ms McCarthy's report is expected for January.  She has not made up her mind yet, it seems.  Many MEPs say that amendment is preferable to rejection.  It is likely that Ms McCarthy will involve the shadow rapporteurs and perhaps rapporteurs of the other commissions in order to avoid surprises later.

Once the draft report has been submitted to the Legal Affairs Committee, a deadline will be open for members to submit amendments.  Those amendments, together with the ones contained in the draft report and those contained in the opinions of the Industry and Cultural Committees will be put to the vote.

If there are a very large number of amendments tabled, it is possible that the committee will ask the rapporteur to try to reach a compromise on the amendments.  The report as adopted will then go to the plenary session, where there will be a further opportunity to submit amendments (although there are rules which mean in practice that only political groups, the lead committee or groups of members may submit amendments).  That will conclude the first reading.

 

Parliament will then await the Council's common position and any amended proposal from the Commission (the Commission usually limits itself to saying which amendments submitted by Parliament it is prepared to endorse)  Once the common position has been announced in the plenary session, Parliament will have three (possibly four) months.  In that period Ms McCarthy will produce a draft recommendation for second reading for adoption by the Legal Affairs Committee.  There will be no further opinions from the other committees.  At second reading, Paliament's Rules of Procedure limit the scope of amendments broadly to reinstating what it sought at first reading and/or amending parts of the common position which are new as compared with the original Commission proposal.
\end{sect}

\begin{sect}{sched}{Schedule}
\begin{center}
\begin{tabular}{|C{44}|C{44}|}
\hline
2002-05-15 & EuroParl 2002-05-15\footnote{http://swpat.ffii.org/treffen/2002/europarl05/index.de.html}\\\hline
2002-05-21 & Internal Market Council session\\\hline
2002-06-04 & EuroParl meeting on Swpat Directive 2002-06-04\footnote{http://wwwdb.europarl.eu.int/oeil/oeil\_ViewDNL.ProcViewCTX?lang=2\&procid=5974\&HighlighType=2\&Highlight\_Text=patentability}\\\hline
2002-06-19 & MdEP Arlene McCarthy 2002-06-19: Bericht \"{u}ber den EUK/BSA-Richtlinienvorschlag\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/amccarthy0206/index.en.html}\\\hline
2002-11-08 & Council Working Party position paper published\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/dkpto0209/index.de.html}\\\hline
2002-11-07 & JURI Hearing\footnote{http://swpat.ffii.org/treffen/2002/europarl11/index.en.html}\\\hline
2002-11-26 & Green Party Hearing\footnote{http://swpat.ffii.org/treffen/2002/europarl11/index.en.html}\\\hline
2003-01-?? & position papers of parliamentary commissions (JURI, ITRE, CULT)\\\hline
2003-02-?? & plenary vote in the europarl, possibly leading to further negotiations with CEU and CEC, return to CEC (in case of rejection) or ratification\\\hline
\end{tabular}
\end{center}

On the software patentability directive, COM(2002) 92  2002/0047 at http://wwwdb.europarl.eu.int/oeil/oeil_ViewDNL.ProcViewCTX?lang=2&procid=5974&HighlighType=2&Highlight_Text=patentability they used to say \begin{quote}
{\it Forthcoming adoption by parliamentary commmittee 10/9/2002 Probable part-session by the DGI(I) 23/9/2002}
\end{quote} Now they say \begin{quote}
{\it Forthcoming adoption by parliamentary commmittee 3/12/2002}

{\it Probable part-session by the DGI(I) 4/12/2002}
\end{quote}
\end{sect}

\begin{sect}{comm}{concerned europarl commissions}
\begin{center}
\begin{tabular}{|C{21}|C{21}|C{21}|C{21}|}
\hline
Legal affairs & responsible & McCARTHY Arlene & PSE\\\hline
Industry & opinion & PLOOIJ-VAN GORSEL Elly & ELDR\\\hline
Culture & opinion & ROCARD Michel & PSE\\\hline
\end{tabular}
\end{center}
\end{sect}

\begin{sect}{part}{Parties}
few clear opinions so far

Green Party and Conservative youth organisations seem to be against

Patent lobby within large parties seem to be well organised and particularly influential in the two concerned committees.  They may try to keep silence and then sudenly come up with a conclusion that looks like a compromise but is pro swpat and push this through the europarl very quickly in september.
\end{sect}

\begin{sect}{enquet}{Setting up Inquiry Commissions}
Treaty of the EU, art. 193:

\begin{quote}
{\it In the course of its duties, the European Parliament may, at the request of a quarter of its Members, set up a temporary Committee of Inquiry to investigate, without prejudice to the powers conferred by this Treaty on other institutions or bodies, alleged contraventions or maladministration in the implementation of Community law, except where the alleged facts are being examined before a court and while the case is still subject to legal proceedings.}

{\it The temporary Committee of Inquiry shall cease to exist on the submission of its report.}

{\it The detailed provisions governing the exercise of the right of inquiry shall be determined by common accord of the European Parliament, the Council and the Commission.}
\end{quote}

It could inquire the Comission game or investigate certain EPO abuses but hardly monitor the patent system permanently.
\end{sect}

\begin{sect}{ceunat}{Einfluss nationaler Parlamente}
Man muss zwei Wege unterscheiden:

\begin{enumerate}
\item
der Bundestag behandelt ein Thema im Rahmen eines Gesetzgebungsverfahrens.

\item
er befasst sich allgemein politisch mit einem Thema.
\end{enumerate}

Gesetzgebung kommt nur in Frage, nachdem die RiLi in der EU beschlossen ist. Dann haben die Mitgliedstaaten eine Umsetzungsfrist f\"{u}r die Anpassung nationaler Gesetze zu beachten. Dies erfolgt in der Regel durch einen Gesetzentwurf der Bundesregierung. Dann l\"{a}uft die ganze Maschinerie an, man kann aber inhaltlich an der RiLi nichts \"{a}ndern, die Umsetzung im nationalen Recht hat zwar etwas Spielraum, kann aber nicht in ganz andere Richtung gehen.  Sonst w\"{u}rde die Kommission Deutschland vor den EuGH zerren.

Allgemeinpolitische Befassung ist jederzeit m\"{o}glich. Ausgangspunkt k\"{o}nnte eine Befassung eines Ausschusses sein. Es besteht ein etabliertes Verfahren zur Unterrichtung des Bundestages \"{u}ber Vorhaben in der EU, eben damit man nicht erst mit den Auswirkungen befasst wird.

Wenn sich (vielleicht auch verschiedene Aussch\"{u}sse) mit dem Thema befasst haben, k\"{o}nnte eine Fraktion einen Entschlie{\ss}ungsantrag einbringen und dadurch seinen Willen im hinblick auf die RiLi \"{a}u{\ss}ern. Aber: auch wenn der Bundestag diese Entschlie{\ss}ung annimmt, ist er f\"{u}r die Bundesregierung als Verhandlungsf\"{u}hrerin in Br\"{u}ssel nicht verbindlich und hat keinerlei aufschiebende Wirkung f\"{u}r das Verfahren in Br\"{u}ssel.  Es l\"{a}ge jedoch im Sinne der Gewaltenteilung, wenn die Bundesregierung in einer Frage der europ\"{a}ischen Gesetzgebung dem Bundestag folgen w\"{u}rde.
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Codecision Procedure\footnote{http://www.tinet.org/~xdrudis/codecisio.html}}}

\begin{quote}
Infos in Catalonian, collected by Xavi Drudis Ferran
\end{quote}
\filbreak

\item
{\bf {\bf EURIM guide to EU institutions\footnote{http://www.eurim.org/EURGUIDE.html}}}

\begin{quote}
includes a guide to the codecision procedure et al in graphical form.  EURIM\footnote{http://www.eurim.org/} is a UK-based lobby group that has MEPs and large companies as members and considers consensus-finding about e-commerce related EU legislation as its mission.  It could have a major impact on the processes within the Europarl.  The EURIM newsletters have strange characters in their filenames which makes them inaccessible at least from some browsing platforms.
\end{quote}
\filbreak

\item
{\bf {\bf codecision procedure\footnote{http://europa.eu.int/prelex/aide.cfm?CL=en\&page=procdec\#450}}}

\begin{quote}
It seems the first vote is in the European Parliament by ``qualified majority''. But even if they reject it, the Council (ministers of member states) has a say and may have it voted again in Parliament, this time with simple majority (but absolute majority for ammending it).
\end{quote}
\filbreak

\item
{\bf {\bf http://europa.eu.int/scadplus/leg/en/cig/g4000q.htm}}

\begin{quote}
The opinions of the EuroParliament in the co-decision procedure are adopted by simple majority voting, I think.  A qualified majority is about voting in the Council (heads of governments).  Open debates in the EuroParl and amendments can change everything.
\end{quote}
\filbreak

\item
{\bf {\bf European Council\footnote{http://europa.eu.int/eur-lex/en/treaties/livre248.html\#anArt4}}}

\begin{quote}
qualified majority
\end{quote}
\filbreak

\item
{\bf {\bf Comittee of Juridical Affairs and interior market 2002-02-19: parlamentary report\footnote{http://wwwdb.europarl.eu.int/oeil/oeil\_ViewDNL.ProcedureView?lang=2\&procid=4446}}}

\begin{quote}
This committee will also work on the eubsa directive proposal.  It seems they amended the Community Patent to support more languages, give some more play to national PTOs and ...(bc:Moreover, MEPs said that a system of quality control should be put in place under the authority of the European Commission in collaboration with the European Patent Office.) QC is good, but those two institutions do have a terrible record on helping it. Subject matter could be part of quality control. Unfortunately, in PTO doublespeak, \&quot;quality control\&quot; may also be limited to the meaning of \&quot;assuring that prior art search is good enough, so that most granted patents can be upheld in EPO courts and other courts that operate under the same low-quality rules as the EPO\&quot;.  If anybody has more details please let us know. It sounds very vague.
\end{quote}
\filbreak

\item
{\bf {\bf Codecision process\footnote{http://europa.eu.int/eur-lex/en/treaties/livre252.html\#anArt3} (European Commission + European Parliament)}}

\begin{quote}
\begin{quote}
\begin{verbatim}

Directive codecision_procedure(EuropeanParliament ep, Comission cec, Council council)
{
    //  proposal=cec.writeProposal();
    Directive proposal=cec.call(bsa);
    cec.send(proposal,ep) &
    cec.send(proposal,council);

    View opinion = ep.reading(proposal);
    if ( opinion.ammendments.count == 0 ) {

         // agreed
         if (council.qualified_majority(proposal) {
                 proposal.state=PASSED;
           	 return proposal;
         }
         state = PASSED;
	 foreach amendment in (opinion.amendments) {
	     if (! council.qualified_majority(amendment)) {
		state=null;
             }
	 };
         if ( state == PASSED ) {
                 proposal.state = PASSED;
           	 return proposal;
         };

         // disagreeing
         View common_position = council.discuss();
         while   (! council.qualified_majority(common_position)) {
            common_position = council.discuss();
         };
         council.send(common_position,ep);
	 cec.send(cec.position(common_position), ep);

         try {
             setTimeout(3*MONTH);
             opinion = ep.reading(common_position);
         } catch (Timeout e) {
              common_position.state = PASSED;
              return common_position;
         }

    }
}

\end{verbatim}
\end{quote}

This algorithm may infringe on existing EPO patents.  Please tell us if you know any such patent.  Moreover, we may not have fully understood the procedure yet.  Please help us improve the code.
\end{quote}
\filbreak

\item
{\bf {\bf Mail Thread about decision making procedure\footnote{http://aful.org/wws/arc/patents/2002-03/msg00006.html}}}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
\ifmlhttasks
\begin{itemize}
\item
{\bf {\bf Wie Sie uns helfen k\"{o}nnen, dem Swpat-Albtraum ein Ende zu machen\footnote{http://swpat.ffii.org/gruppe/aufgaben/index.de.html}}}
\filbreak

\item
{\bf {\bf How is the decision procedure affected by the fact that basic freedoms and other constitutional questions are involved here?}}
\filbreak

\item
{\bf {\bf What other Europarl Committees are involved?  Who is reporting to them?}}
\filbreak

\item
{\bf {\bf Study the procedure and improve the co-decision algorithm shown above!}}
\filbreak

\item
{\bf {\bf Point us to some useful basic ressources on EC law}}
\filbreak

\item
{\bf {\bf What else should we ask in order to make this procedure as transparent as possible?}}
\filbreak

\item
{\bf {\bf Where do we find a list of MEPs?}}

\begin{quote}
Prepare for our supporters (e.g. 100k petition signatories) a set of ressources that will enable them to talk to their representatives in a meaningful way
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: latex ;
% End: ;

