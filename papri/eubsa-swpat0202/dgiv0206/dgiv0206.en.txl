<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: DG IV Bakels 2002-06-19: The Patentability of Computer Programs

#descr: A study on software patentability which was commissioned by the European Parliament's Research Directorate as a reference for deliberations about the European Commission's proposal to make all useful ideas patentable.  Unlike most other studies commissioned by EU institutions, this one does not seek to hide the problems with software patents and takes a refreshingly undogmatic look.  Bakels finds that the proposed directive fails on all its proposed goals (clarification, harmonisation etc) and contains numerous inconsistencies.  Concerning the impact of software patents, Bakels finds that %(q:it is crucial to change the long-standing tradition of patents being granted for relatively simple inventions).  Yet Bakels offers no hints as to how this long-standing problem can be solved and does not properly assess how some other possible filters such as concreteness and physical substance (technical character) or %(q:invention character) (laid out in Art 52 EPC) relate to it.

#Ced: Bakels & Hugenholtz 2002-06-19: Study on Software Patentability for European Parliament

#Pit: PDF original of the study commissioned by the Research Directorate of the European Parliament, written by Reinier Bakels and Prof. Bernt Hugenholtz from Amsterdam University.

#unu: Xavier Drudis Ferran comments on the DG IV Bakels & Hugenholtz Study

#ofs: Source of much of the ffii page on the same study.

#NWe: Nice to see that finally official reports recognize problems with software patents, and there are a few good points in the study, although it isn't 100% right. At least, and this is refreshing news, it is readable and plays no word games and sophistry, as was the norm with DG Internal Market sponsored studies.

#IoW: In general one can imagine that the authors see more than they say, but shy away from criticising institutions too much.

#AdE: Although we'll see some weak points later, the study is a better basis to build on than most other EU documents, and therefore it is very much welcome.

#Ftq: For the more important points see %(q:Key points) below

#Sno: Strong points

#WWn: Weak points

#KWi: Key points

#Qoe: Quotes

#WiW: We like the perspective that patent law is economic law, and there is no moral justification for it, nor are patents a natural right of inventors. This is very basic, but is often ignored by the Comission reports, or simply by ordinary people. It is very appropiate to state it as a reminder.

#TWe: The study clearly explains (3.6) that the %(tr:TRIPs agreement does not impose software patents), and that is good too, since some pretend TRIPs to have already decided our policy for us.

#IaW: It also recognizes (4.1) that prosperity of the software business is not a consequence but a cause of extension of patentaility to software. That is true indeed. Microsoft has been cited in other CEC-sponsored papers as having hundreds of patents and having done well, but they never say MS grew without patents and has only started using then strategically when it was large and wanted to keep its monopoly.

#Ine: In fact in 4.5, when it says:

#Tls: This is not the place to speculate about possible strategies of commercial software vendors, but if they would be able to compete using legal means such as the patent system, there is no guarantee they would not do that.

#Ian: It misses the recent (barely before the date of the document) news from Microsoft according to which they %(ms:plan) to assert patents against free software compatible with their CIFS specification for sharing files and printers in a network. This is a clear example of their strategic use to keep their monopoly.

#Iis: It further states (4.3.1) that software innovation exists without the incentive of patents, and therefore software patents are unnecessary and counterproductive.

#Iir: It shows some of the weak points of the COM(2002) 92 directive proposal, such as e.g. the fact that the technical contribution requirement in the directive is meaningless (5.2).

#Ifp: It denounces patent inflation (6.2), but unfortunately restricts its outlook to the problem of %(q:trivial patents).  Trivial patents are indeed a serious problem, and should be dealt with before any extension of the patent system (and before the EPO is granted new powers with the Community Patent).  But triviality is not the only problem, and the study ignores the evidence that patent inflation is connected not only to a lowering/formalisation of non-obviousness requirements but also to an erosion of the invention/technicity concept and to a series of other factors.

#Tmn: It proposas a %(q:patentability observatory) (6.3), i.e. an independent institution to monitor the patent system.  This is very similar to the supervisory committee in the %(ed:Eurolinux demands)). This idea should be worked on.

#W6o: We also generally agree on section 6.4. And most of the other points not explicitly commented are useful.

#Ose: On some issues, the study seems not to be very well informed:

#Ttn: The study rightly emphasizes the problems of software patents for SMEs, but fails to note that most software jobs are in SMEs. We do not have the data handy for Europe, but statistics for Catalonia, %(sa:say) that 2/3 of employees in firms with more than 10 workers in information and communication technology work in firms with less than 200 employees. These companies could be seriously damaged if the directive is approved. If the same proportion holds for Europe (as is likely) then saying simply (4.2.2) %(q:The software industry is very diverse.  Again, generalisations should be critically examined) is correct, but fails to note that certain generalisations are nonetheless possible and do not speake for the directive proposal.

#Tae: The study downplays the value of economic research work which has so far been done on the subject.  Most economics analyses demonstrate that software patents are a bad idea.  Of course they can't be absolutely certain of the outcome of all the possible scenarios, but in a field where direct experimentation is not possible, all we have as a basis for our decisions are these informed and rational speculations. We should assess them critically, but not simply discard them as unconclusive, just because they do not meet unrealistically rigid expectations of exact science.

#Sul: Section 4.1 downplays the meaning of popular opinion and consultations replies. %(bc:Apparent majorities may or may not be meaningful) it says. In the Comission's consultation, for example, by reading some %(cr:hundreds of replies), and then the study of replies to the consultation, it is easy to see that it was %(ek:not fair). Although the study does not reflect the replies, it shows more than 90% of respondents were against software patents. This is not an %(q:apparent majority), it is not even simply %(q:widely varying opinions) it is an overwhelming near-consensus. The study might not be a statistical significant sample, but neither was it a sample of only software patents opponents. In fact the organizers were pro software patents, and the consultation was equally open to everyone. It is hardly appropriate in a democracy to downplay the significance of majorities in this way. After all we don't leave empty seats in Parliament just because abstention rate is higher that we'd like.

#StE: Section 3.1 fails to note the traditional distiction between copyright and patents. They were %(dp:not meant to overlap). Certainly you can devise a system like in the US where software can get both patents and copyright, but then software would be the only work covered by both forms of property, and patents being a stronger property would erode the protection offered by copyright. The study recognizes implicitly the potential for those damages (SMEs prefering copyright and being threatened by patents that they could inadvertedly infringe and then being unable to exploit their copyrighted original work), but then assumes that both can coexist for the same subject matter.

#Sto: Section 4.3.1 is generally shy, in that it shows compelling reasons to believe software patents are more harmful than helpful in promoting innovation, but concludes only that %(q:[the stifling effect on innovation] may be true as well).  Here it also misses one clue: the fact that free software is an increasing and powerful phenomenon shows that not only the software market has enough incentives to innovate without patents, but that it has also enough incentives to disclose inventions in a vastly more useful way for this particular field than patents in inconvenient repositories: with publically available documentation and source code in the internet, that not only explains the invention, but allows anyone to directly test and use it, and any person skilled in the art to modify to adapt it to their needs (not merely reproduce it from scratch like patents do).

#Ses: Section 4.5 shows terminologically that the author is not very familiar with free software, when it opposes %(q:open source) to %(q:commercial software).  You can have any combination of commercial or non-commercial, gratis or priced, %(q:open source) or %(q:closed source) software. Clearly much %(q:open source) software is commercial, and many companies live on it.

#WrW: When section 5.2 comments in the directive, it is a little too soft on the important problems it finds. The directive explanatory memorandum is full of pro software patent propaganda and dogma, with litle rational discourse. For a better critique of the directive see %(URL).  The authors seem to have had looked at this critique, but they don't cite it, only speak of %(q:early reactions to the proposal [...] from the Open Source Software movement and its supporters.)  In fact Eurolinux is one of the most active opponents of the directive, and it is an alliance by both %(q:open source supporters) and hundreds of organisations and companies that write and use proprietary software.

#I5l: In this section 5.2, and specially in section 7:

#Gac: Given these uncertainties, it would be more appropriate to concentrate on legal security and harmonisation of law than to contemplate major substantive changes in patent law. This is indeed the route chosen by the European Commission in the proposed Software patent directive, even though the explanatory memorandum accompanying the proposal suggests that software patents are good for the European economy.

#Wps: We get the impression the directive proposal has not been fully understood. The directive carefully sets several provisions to avoid any software from not being patentable and forces courts to uphold substantive changes in patent law that have been slipping through currently unenforceable EPO case law. The %(q:technical contribution) requirement is only a dodge, because the directive defines software as being a technical field. Therefore any software contribution is technical and any software is patentable. In fact, when we tried to imagine amendments, we saw that by ammending only few articles, the directive would still apparently allow for any software patents.

#AsW: Any one of the following provisions alone would probablly suffice to allow for unlimited patentability:

#Aee: Article 2 defines computer programs to be inventions, that would make them patentable subject matter, although art 52 EPC excludes them.

#Adt: Article 3 says software belongs to technology. That would make it patentable and would void any technical contribution requirement, since software contributions would be technical contributions and any piece of software represents a software contribution.

#Anc: Article 4 completes the empty technical contribution requirement outlined in article 2 and anyway takes care to include non-technical features in the inventive step, so that the only sane delimiting criteria for subject matter %(es:cannot be applied). Even if some ammendment would leave software as non-technical, innovations in software would still cause inventive step and make programmed conventional computers inventions (with inventive non-technical features).

#AWl: Article 5 allows software claims as long as they are dressed up in language involving conventional computers or networks.

#Alw: Article 7 and 8 set up minimal control measures,which are not allocated any budget, to let the patent system continue inflation without proper assessment of its effects on economy and society.

#TWa: The study embraces the CEC/BSA view according to which the understanding of %(q:what is technical) is likely to evolve with the rise of %(q:new technologies).  This is an excuse for leaving free way to the EPO to decide on its own and forces courts to uphold any patents, rather than setting clear limits by law.  If the aim is to let the EPO do what it wants, then we should not need a directive.

#Trh: The fact is that precise definitions of %(q:technical invention) %(ti:exist), have been used in the past, and are still useful and applicable if one cares to try to tune the patent system for the benefit of society.  Occasional technological revolutions will hardly affect philosopical categories such as matter and mind, information and physics, and those can be used to build a persistent framework that is relevant today as it was in the seventies.

#Afe: Also, there's somewhere a reference to patents being good for SMEs to raise venture capital. This is (%(js:if at all)) a temporary effect that will vanish as soon as investors realise that software patents are no good for SMEs and that they are not a stamp of technical excellence, as the EPO itself %(ee:admits).

#Tjh: The study fails to see a delimiting criteria for patentable subject matter, and surrenders to its own inability, ignoring traditional distinctions that are well rooted in philosophy, stand the pass of time and simply work.

#Idi: It focuses the problem on trivial patents. This is a huge problem, but not the only one, and we must not let this one hide other problems.  Specially because %(sf:the triviality problem does not have an easy solution).  Or how high should the %(q:inventive height) of a software patent be?  3 Lemelson?  10 Lemelson?  How do we define the measuring unit %(q:Lemelson)?

#Oae: One obvious solution to a large part of the problems lies in combining technicity and novelty in a way, as it used to be the case in the %(eg:EPO examination guidelines of 1978).  Clearly an innovation should not be patentable or unpatentable depending of the drafting of the claim wording. We must look beyond words into meaning and ask whether the application teaches new insights into the use of controllable natural forces for a predictable result. It is not enough that the invention is new and it has physical effects. The physical effects must be new, we should not allow patents on new uses of already existing machines such as computers, whatever electrical currents or effects their peripheral devices have on the physical world. If the innovation lies in logics and calculation to achieve effects through a know machine and known peripherals, using natural forces in known ways, the fact that the effects are new is not sufficient, because we get no new insight in technical matters, just a new application of already available knowledge.

#Tie: This %(q:technical invention) criterion has its rationale in the rule that any new discovery that %(q:only) needs rational thought would appear sonner or later and does not need monopolies on ideas as an incentive, and risks putting any rational being (specially computer users) under infringement, while those inventions that require empirical observation of the physical world are likely to require experimentation, laboratories and infrastructure, maybe would not be possible without important incentives as patents, and can only be infringed by those manufacturing (or commercially using) patented inventions.

#Rct: Remember that the scarce human ressource in software (and any information, logic or business innovation) does not consist in the ability to conceive ideas that can be patented, but in writing the code (or building the business, etc.) that puts such ideas to work. This is already protected by copyright (at least for software) and other mechanisms and does not benefit from patents. On the contrary, creativity would suffer from the artificial scarcity of ideas that patents create.

#Poc: Please see the references for better explanations of the vital concept of the technicity criteria to apply. A proper application of this criterion would not completely solve the triviality problem, but it would eliminate the most objectionable patents, whichconstitue about 3% of the number of patents which the EPO grants every year, and it would do so in a reliable and predictable way.

#TWt: The study is wrong in identifying triviality as the real cause of concern especially for free/opensource software.  Even non-trivial software patents, as rare as they may be, are harmful.  In fact algorithms can be classified into an abstract and all-encompassing type and a more concrete and trivial type.  The alternative to triviality is abstractness, whose patentability is just as undesirable.  The study fails to take into account work that has been done by computer scientists on this %(q:problem of abstraction), see

#Aph: A more basic concern about software patents is in their epistemological nature.  This is well addressed by the technicity criterion but not by the non-obviousness criterion.

#PWa: Patents are monopolies in the use of some machine in exchange for publication of information on that machine. When the machine becomes software, and therefore machine=information patents collapse into %(q:monopolies in the use of some information in exchange for publication of information).  So we are allowing people to monopolise information in exchange for publishing information that cannot be used because is monopolised.  The fact that publication equals manufacturing in software (and other logical creations) makes patenting software (and all other logical achievements) absurd.

#TnW: The basic difference that distingueshes software from other fields is that it is information. The study fails to see that, because this line does not only separate software from mechanical engineering, but also excludes other logical achievements from patentability (like VHDL hardware (that is designed by feeding logic programs to a machine that builds the hardware), business methods, social interactions, etc.).

#Tot: The study keeps reiterating that no criteria can distinguish desirable from undesirable software patents, although such criteria exist and have been successfully used for decades.  A %(e:technical invention) is a %(e:teaching of cause-effect relations in the use of controllable physical forces). This allows for patents on real computer-implemented inventions, such as for example a chemical engineering process that needs certain changes in pressure and temperature controlled by a computer, when there was not previously know that those changes obtained the desired effects. The fact that a program controls the process does not mean that the process is not patentable.  We would not be patenting software as such, but a technical invention that happens to use a program. Everybody would be free to use the same program consepts (respecting copyright if any) for other purposes (like a virtual simulation of the process), but they would infringe when performing the chemical process with real machinery and substances. This is the useful meaning of the clause %(q:as such) in EPC art 52, you cannot patent software as such, but can patent inventions that include software when the innovation is not in information but in the material world. Thus, a program alone, on a carrier, in transmission, working on a conventional computer, or otherwise would not infringe. It is not enough to take a conventional computer, argue that it uses electrical currents or known devices in known ways and pretend that that allows patenting any program when the application is properly drafted.

#Taa: The study has apparently not understood this delimiting criterion for patentability and therefore infers unlimited patentability from a difficulty in defining software.

#Tnr: The basic underlying problem of this study may be that it stops short of asking the responsible governmental institutions to confess that they have committed serious errors.  In the USA in 1994, the most well-respected software business pioneers %(jw:called upon the government to recognize its errors and declare previous decisions of various administrative and judicial organs as illegal).  In Europe, the errors are even more evident than in the US.  The European Patent Organisation and the european governments who are sitting on its board of directors have acted against the letter and spirit of the law and against the interests of the innovators and the public at large.  Face-saving and problem-solving are in conflict.  In this context it may be difficult for studies in general and specially those commissioned by state institutions not to opt for some degree of face-saving.

#oof: In theory non-obviousness is not enough; inventions must meet a certain standard of inventiveness. ...

#tiq: In practice however, a novel invention is rarely denied a patent, unless it is  completely obvious to %(q:a person skilled in the art). To be sure, this is by no means unique to software or business method patents. Unfortunately, courts may not be able to raise this standard, as it is firmly rooted in the tradition of the patent system. A major departure from current practice in this respect would be beyond the authority of the courts.  The %(q:patent inflation) caused by granting %(q:trivial) patents on a routine basis in our view calls for a fundamental debate, and eventually legislative action.

#dts: SMEs apparently do not like patents at all, despite extensive promotion efforts by proponents of the patent system.

#War: Dutch article by Bakels one year later.  We have an %(en:english) and %(de:german) version also.

#InW: Staff home page at Instutut voor Informatierecht (Institute for Information Law).  A less complete %(ev:english version) is also available.  The dutch page shows pointers to a few more texts which Reinier Bakels wrote on the subject.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: dgiv0206 ;
# txtlang: en ;
# multlin: t ;
# End: ;

