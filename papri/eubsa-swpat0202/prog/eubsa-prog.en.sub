\begin{subdocument}{eubsa-prog}{Program Claims: Bans on Publication of Patent Descriptions}{http://swpat.ffii.org//papers/eubsa-swpat0202/prog/index.en.html}{Workgroup\texmath{\backslash}\texmath{\backslash}swpatag@ffii.org}{Patent Claims to ``computer program, characterised by that upon loading it into memory [ some process ] is executed'', are called ``program claims'', ``Beauregard claims'', ``In-re-Lowry-Claims'', ``program product claims'', ``text claims'' or ``information claims''.  Patents which contain these claims are sometimes called ``text patents'' or ``information patents''.  Such patents no longer monopolise a physical object but a description of such an object.  Whether this should be allowed is one of the controversial questions in the struggle about the proposed EU Software Patent Directive.  We try to explain how this debate emerged and what is really at stake.}
\begin{sect}{intro}{introduction}
Patent Claims directed to a \begin{quote}
``computer program product'', ``computer program'', ``data structure'' or other information object, ``characterised by that upon reading out the information ... happens'',
\end{quote} were introduced in the USA in 1994, shortly after a USPTO hearing in which all major software companies except for Microsoft pronounced themselves against software patentability.  In Europe, program claims were introduced in defiance of the written law by the European Patent Office in 1998, followed immediately by the UK Patent Office and later by some more national patent offices.  In 2002 the European Commission proposed to reverse this decision.

The European Commission's Software Patent Directive Proposal's Article 5 does not allow program claims, i.e. claims of the form ``computer program, characterised by that upon loading it into memory [ some process ] is executed''.  In justification, the European Commission said that it did not want ``software as such'' to be patentable.  Yet the Commission's proposal speaks of ``infringing programs'' and ensures that at least the execution of a computer program on a general-purpose computer can be a patent infringement, whereas its publication or distribution may or may not be seen as an infringement, depending on how the courts apply the doctrine of ``contributory infringement''.  Critics from all sides have found this inconsistent.  The European Parliament's committees for cultural and industrial affairs (CULT and ITRE) voted for amendments which safeguard freedom of publication.  Amendment ITRE-13 states that the publication of a program can never constitute a patent infringement.   The Legal Affairs Commission (JURI), on the other hand, explicitely rejected ITRE-13 and instead voted for a ``compromise amendment 1'', which bashfully legalises program claims (with a pseudo-limitation: ``only if ... [ the program claims are derived from process claims ]'').  According to this ``compromise'' between the patent lobby and the patent lobby, software authors and Internet service providers (ISPs) can be sued for direct patent infringement everywhere in the European Union, as soon as they make a program text with the claimed features available somewhere on the Internet.

The European Parliament's Committe for Legal Affairs and the Internal Market (JURI), in its resolution of 2003/06/18\footnote{http://swpat.ffii.org//news/03/juri0617/index.en.html}, proposes to legalise \emph{program claims}, i.e. claims to

\begin{center}
\begin{quote}
{\it a computer program, characterised by that upon loading it into computer memory [ some process ] is executed.}
\end{quote}
\end{center}

This would make publication of many programs a direct patent infringement, thus creating additional risks of litigation for programmers, university researchers and Internet service providers.

Program claims also lead the patent system ad absurdum.  Taken to the extreme of consistency, they mean that patent descriptions are patented: by publishing the full disclosure (which in case of software should contain some usable source code), the patent office would infringe on the program claim.  This illustrates nicely why software cannot be patentable: patents are supposed to be deals between the patentee and the public, where information is disclosed and matter monopolised.
\end{sect}

\begin{sect}{just}{Standard Justifications for Program Claims}
see JURI Art 5\footnote{http://swpat.ffii.org//papers/eubsa-swpat0202/juri0304/index.en.html\#art5}

The ``consumer-protection'' argument in favor of program claims is groundless and cynical.  It is always possible for the end user of commercial software to hold the distributor liable by means of standard contract clauses, and this is what would usually happen, if software is patentable without program claims.

The real reason why industrial patent lawyers are pushing for program claims seems to be a symbolic one:  the existence of program claims brutally and indecently documents that the border to patenting software as such has been crossed.  Conversely, by refusing to grant program claims, the European Commission can appear as moderate.

However, in order to protect the freedom of publication, as guaranteed by Art 10 ECHR, the mere refusal of an indecent claim form is not enough.  A more explicit affirmation of the freedom of publication, such as ITRE-13, is needed.

Ultimately, in another context, it must be made clear that information objects in whatever form are not patentable.
\end{sect}

\begin{sect}{amcc}{McCarthy: Program Claims render Freedom of Publication ``Caduc''}
Interestingly, JURI did not vote on ITRE-13 at all, because Arlene McCarthy judged it incompatible with her program claim amendment COMP-1 and therefore ``caduc'' (obsolete) if COMP-1 was voted for, which it was.  In other words: Arlene McCarthy admits that program claims are a direct negation of the freedom of publication.   One could conceive of a scenario where program claims are really considered to be ``only of declaratory nature'' (as said in the justification of COMP-1) and freedom of publication is nevertheless maintained.  Such scenarios are not rare in law.  Yet for JURI the matter is clear:  on the one hand side they downplay the impact of program claims by calling them ``merely of declaratory nature'', on the other their decision is built on the implicit assumption that program claims and freedom of publication are so completely mutually exclusive that ITRE-13 needn't even be put to vote.
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf JURI proposals and justifications on program claims\footnote{http://swpat.ffii.org//papers/eubsa-swpat0202/juri0304/index.en.html\#art5}}}

\begin{quote}
COMP-1 was approved.  It extends patentability compared to the European Commission's proposal by introducing program claims, but misleadingly presents this extension as a restriction, by saying that program claims are ``only'' admitted in as far as they are equivalent to process claims, which of course they always are.
\end{quote}
\filbreak

\item
{\bf {\bf Phil Salin: Freedom of Speech in Software\footnote{http://philsalin.com/patents.html}}}

\begin{quote}
A text which explains in clear and easy-to-understand terms why software patents violate freedom of speech.

see also Seth Johnson 2003/08: Phil Salin on Software Patents\footnote{http://aful.org/wws/arc/patents/2003-08/msg00091.html}
\end{quote}
\filbreak

\item
{\bf {\bf CEC \& BSA 2002-02-20: proposal to make all useful ideas patentable\footnote{http://swpat.ffii.org//papers/eubsa-swpat0202/index.en.html}}}

\begin{quote}
The Commission Proposal does not allow program claims, but suggests between the lines that they should be allowed.
\end{quote}
\filbreak

\item
{\bf {\bf Interop\'{e}rabilit\'{e} et Brevet: Controverse au Parlement europ\'{e}en\footnote{http://swpat.ffii.org//papers/eubsa-swpat0202/itop/index.en.html}}}

\begin{quote}
An article from AIPLA (Erwin Basinski) published here suggests that the European Commission won't mind if the European Parliament reintroduces program claims.  AIPLA is usually very well informed about the thinking of their patent lawyer colleagues at the European Commission's Directorate for the Internal Market, who are in charge of directive drafting.
\end{quote}
\filbreak

\item
{\bf {\bf EPO T 1173/97: IBM Computer Program Product\footnote{http://swpat.ffii.org//papers/epo-t971173/index.en.html}}}

\begin{quote}
EPO decision of 1997 to introduce program claims
\end{quote}
\filbreak

\item
{\bf {\bf The UK Patent Family and Software Patents\footnote{http://swpat.ffii.org//players/uk/index.en.html}}}

\begin{quote}
UK Patent Office was the first to follow the EPO in introducing program claims in 1998
\end{quote}
\filbreak

\item
{\bf {\bf BPatG Fehlersuche 2000-07-28: Patentanspr\"{u}che auf ``Computerprogrammprodukt'' etc unzul\"{a}ssig\footnote{http://swpat.ffii.org//papers/bpatg17-suche00/index.en.html}}}

\begin{quote}
German court decision of 2000 against program claims
\end{quote}
\filbreak

\item
{\bf {\bf CEU/DKPTO 2002/09/23..: Software Patentability Directive Amendment Proposal\footnote{http://swpat.ffii.org//papers/eubsa-swpat0202/dkpto0209/index.en.html}}}

\begin{quote}
Under leadership of UK and DE governmental patent administration officials, the European Council's Industrial Property Working Party recommends in favor of program claims.
\end{quote}
\filbreak

\item
{\bf {\bf Software Patents in Finnland\footnote{http://swpat.ffii.org//players/fi/index.en.html}}}

\begin{quote}
Finnish patent office introduced program claims in 2003, shortly after CULT and ITRE had recommended against them
\end{quote}
\filbreak

\item
{\bf {\bf Portugal and Software Patents\footnote{http://swpat.ffii.org//players/pt/index.en.html}}}

\begin{quote}
Portugues patent office, ostentatively following lead of German governmental patent lawyer, pronounces itself in favor of program claims
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: latex ;
% End: ;

