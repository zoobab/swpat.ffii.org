<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Programmansprüche: Verbote der Veröffentlichung Nützlicher Patentbeschreibungen

#descr: Patentansprüche auf ein %(q:computer program, characterised by that upon loading it into memory [ some process ] is executed), werden genannt %(q:program claims), %(q:Beauregard claims), %(q:In-re-Lowry-Claims), %(q:program product claims), %(q:text claims) oder %(q:information claims). Patente die diesen Anspruch enthalten werden manchmal auch %(q:text patents) oder %(q:information patents) genannt. Solche Patente monopolisieren nicht länger ein physikalisches Objekt sondern eine Beschreibung eines solchen Objekts.  Ob dies erlaubt werden sollte ist eine der kontroversen Fragen in der Auseinandersetzung um die vorgeschlagene EU Software Patent Richtlinie. Wir versuchten zu erklären wie diese Debatte aufkam und was wirklich auf dem Spiel steht.

#rom: Übliche Rechtfertigung von Programm-Ansprüchen

#Wri: McCarthy: Program Claims render Freedom of Publication %(q:Caduc)

#sim: Patentansprüche auf ein %(bq|%(q:Computerprogramm Produkt), %(q:Computerprogramm), eine %(q:Datenstruktur) oder anderen Informationsgegenstand, die dadurch gekennzeichnet sind, dass beim Auslesen der Information ... geschieht),) wurden in den USA 1994 eingeführt, kurz nach einer Anhörung beim USPTO, in der alle großen Softwarefirmen mit der Ausnahme von Microsoft sich gegen die Patentierbarkeit von Software ausgesprochen hatten.  In Europa wurden Programmansprüche entgegen den geltenden Gesetzen durch das Europäische Patentamt 1998 eingeführt, gefolgt vom UK Patentamt und später von einigen anderen nationalen Patentämtern.  Die Europäische Kommission schlug 2002 vor, diese Entwicklung umzukehren.

#gWn: Der Artikel 5 des Richtlinenvorschlags zu Softwarepatenten der Europäische Kommission erlaubt keine Programmansprüche, d.h. Ansprüche in der Form eines %(q:Computerprogramms, gekennzeichnet dadurch, dass es beim Laden in den Speicher [eines Prozeßes] ausgeführt wird).  Zur Rechtfertigung sagte die Europäische Kommission, dass sie nicht möchte, dass %(q:Software als solche) patentierbar ist.  Dennoch spricht der Vorschlag der Kommission von %(q:verletzenden Programmen) und stellt sicher, dass zumindest die Ausführung eines Computerprogramms auf einen universellen Computer eine Patentverletzung sein kann, wohingegen die Veröffentlichung oder Verbreitung des Programms als Verletzung betrachtet werden kann oder auch nicht, abhängig davon, wie die Gericht die Doktrin der %(q:mitursächlichen Verletzung) anwenden.  Kritiker von allen Seiten haben dies als inkonsisten erachtet.  Die Kommitteen des Europäischen Parlaments für Kulturelle und Industrielle Angelegenheiten (CULT und ITRE) stimmten für Änderungsanträge, welche die Freiheit zur Veröffentlichung sicherstellen.  Änderungsantrag ITRE-13 sagt aus, dass die Veröffentlichung eines Programms niemals eine Patentverletzung darstellen kann.  Auf der anderen Seit hat die Komission f̈r rechtliche Angelegenheiten (JURI) ITRE-13 ausdrücklich abgelehnt und stattdessen für einen %q(Kompromißantrag 1) gestimmt, welcher Programmansprüche schamhaft legalisiert (mit einer Pseudo-Einschränkung: %(q: nur wenn ... [ die Programmansprüche vom Prozeßanspruch abgeleitet sind ])).  Nach diesem %(q:Kompromiß) zwischen der Patentlobby und der Patentlobby, können Softwareautoren und Internetdienstleister (ISPs) für unmittelbare Patentverletzungen verklagt überall in der Europäischen Union verklagt werden, sobald sie einen Programmtext mit den benaspruchten Merkmalen irgendwo im Internet verfügbar machen.

#2es: Das Kommittee des Europäischen Parlaments für rechtliche Angelegenheiten und den Binnenmarkt (JURI) hat in seiner %(jr:Resolution vom 18.06.2003) vorgeschlagen, %(q:Programmansprüche) zu legalisieren, d.h. Ansprüche auf

#aiW: ein Computerprogramm, gekennzeichnet dadurch, dass es beim Laden in den Speicher [eines Prozeßes] ausgeführt wird.

#pis: Dies würde die Veröffentlichung vieler Programme zu einer unmittelbaren Patentverletzung machen, und so ein zusätzliches Prozeßrisiko für Programmierer, Universitätsforscher und Internet Dienstleister bedeuten.

#tPt: Programmansprúche können auch das Patentsystem ad absurdum führen.  In letzter Konsequenz bedeuten sie, dass Patentbeschreibungen selbst patentiert sind: bei vollständiger Veröffentlichung (welche im Fall von Software brauchbaren Quelltext enthalten sollte) würde das Patentamt die Patentansprüche verletzen.  Dies demonstriert warum Sofware nicht patentierbar sein kann: Patente sind gedacht als ein Abkommen zwischen dem Patentierer und der Öffentlichkeit, bei dem Informationen bekanntgegeben und Gegenstände monopolisiert werden.

#UWt: JURI Art 5

#tse: Das %(q:Verbraucherschutz) Argument für Patentansprüche ist gegenstandslos und zynisch.  Es ist für den Endverbraucher immer über die Standardverträge möglich, sich am Verbreiter schadlos zu halten, und dies ist auch was normalerweise geschehen würde, wenn Software patentierbar wäre ohne Programmansprüche.

#pWt: Der eigentlich Grund, warum industrielle Patentanwälte sich für Programmansprüche einsetzen, scheint ein symbolischer zu sein: Die Existenz von Programmansprüchen dokumentiert auf brutale und unanständige Weise, dass die Grenze hin zur Patentierung von Software als solche überschritten worden ist.  Umgekehrt kann, indem Programmansprüche abgelehnt werden, die Europäische Kommission moderat auftreten.

#sit: Um allerdings die Freiheit zur Veröffentlichung zu schützen, wie sie in Art 10 ECHR garantiert ist, reicht es nicht aus, einfach nur unanständige Anspruchsformen abzulehnen.  Eine explizitere Bestätigung der Freiheit zur Publikation, wie zum Beispiel ITRE-13, ist notwendig.

#nto: Abschliesßend muss, in einem anderen Kontext, klargemacht werden, dass Informationsgegenstände in welcher Form auch immer nicht patentierbar sind.

#tnt: Interessanterweise stimmte JURI gar nicht über ITRE-13 ab, weil Arlene McCarthy diesen Antrag als inkompatiebl zu ihrem Programmanspruchsantrag COMP-1 eingeschätzt hat, und demnach %(q:caduc) (obsolet), wenn COMP-1 angenommen würde, was geschehen ist.  In anderen Worten: Arlene McCarthy gibt zu, dass Programmansprüche eine direkte Verneinung der Freiheit zur VEröffentlichung sind.  Man könnte sich ein Szenario vorstellen, in dem Programmansprüche wirklich nur von %(q: beschreibender Natur) sind (wie es in der Rechtfertigung von COMP-1 gesagt wird), und die Freiheit zur Veröffentlichung nichtsdestotrotz beibehalten wird.  Solche Szenarios treten im Recht aber selten auf.  Für JURI aber ist die Sache klar:  Auf der einen Seite spielen sie die Auswirkungen von Programmansprüchen herunter, indem sie sie als (q: nur von beschreibender Natur) bezeichen.  Auf der anderen Seite beruht ihre Entscheidung auf der Annahme, dass Programmansprüche udn Freiheit zur Veröffentlichung sich gegenseitig so vollständig ausschliessen, dass ITRE-13 nicht einmal zur Abstimmung gestellt werden muß.

#klW: Einführung, Hintergrund und Diskussion auf der Mailingliste zu einem klassischen Text über Redefreiheit und ihre Bedeutung in Software.

#ofr: JURI Vorschläge und Rechtfertigungen zu Programmansprüchen.

#iiv: COMP-1 was approved.  It extends patentability compared to the European Commission's proposal by introducing program claims, but misleadingly presents this extension as a restriction, by saying that program claims are %(q:only) admitted in as far as they are equivalent to process claims, which of course they always are.

#bWn: Der Kommissionsvorschlag erlaubt Programmansprüche nicht, aber sagt zwischen den Zeilen, dass sie erlaubt sein sollten.

#avW: Ein Artikel von %(AI), hier Veröffentlicht, schlägt vor, dass die Europäische Kommission nichts dagegen hätte, wenn das Europäische Parlament Programmansprüche einführen würde.  AIPLA ist normalerweise sehr gut über das Denken der Patentanwaltskollegen im Direktorat der Europäischen Kommission für den Binnenmarkt informiert, welche für den Richtlinienvorschlag verantwortlich sind.

#cor: EPA Entscheidung von 1997, Programmansprüche einzuführen

#fWr: Das Britische Patentamt war das erste, welches dem EPA in der Einführung von Patentansprüchen 1998 gefolgt ist

#cWg: Deutsches Gerichtsurteil von 2000 gegen Programmansprüche

#num: Under leadership of UK and DE governmental patent administration officials, the European Council's Industrial Property Working Party recommends in favor of program claims.

#f0e: Das finnische Patentamt führt Programmansprüche 2003 ein, kurz nachdem CULT und ITRE sich gegen sie ausgesprochen hatten

#fgW: Das portugiesische Patentamt, demonstrativ der Vorreiterschaft der deutschen Regierungspatentjuristen folgend, spricht sich für Programmansprüche aus

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: rainbow ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-prog ;
# txtlang: de ;
# multlin: t ;
# End: ;

