\begin{subdocument}{eubsa-prog}{Revendications Programmes: Interdiction de Publication de D\texmath{\backslash}'{e}scriptions de Brevet}{http://swpat.ffii.org//papiers/eubsa-swpat0202/prog/index.fr.html}{Groupe de travail\texmath{\backslash}\texmath{\backslash}swpatag@ffii.org\texmath{\backslash}\texmath{\backslash}version fran\texmath{\backslash}c{c}aise 2003/11/10 par Cedric Corazza\texmath{\backslash}footnote{http://wiki.ael.be/index.php/FightingSWPatentsTranslation}}{Patentenconclusies op ``een computerprogramma, gekenmerkt door het feit dat wanneer het geladen wordt in het geheugen [ een bepaald proces ] wordt uitgevoerd'', worden als volgt genoemd: ``programmaconclusies'', ``Beauregardconclusies'', ``In-re-Lowry-Conclusies'', ``programmaproductconclusies'', ``tekstconclusies'' or ``informatieconclusies''. Patenten die dergelijke conclusies bevatten worden soms ``tekstpatenten'' of ``informatiepatenten'' genoemd. Zulke patenten monopoliseren niet langer een fysisch object, maar een beschrijving van een dergelijk object.  Of dit moet toegelaten worden, is een vand e controversi\texmath{\backslash}``{e}le vragen in de strijd over de voorgestelde EU Richtlijn over Softwarepatenten.  We proberen uit te leggen hoe dit debat ontstaan is en wat er werkelijk op het spel staat.}
\begin{sect}{intro}{introduction}
Les Revendications de Brevet concernant les \begin{quote}
``produit programme'', ``programme pour ordinateur'', ``structure de donn\'{e}es'' ou autre objet d'information , ``charact\'{e}ris\'{e} en ce que lorsqu' il se charge en m\'{e}moire d'ordinateur [ un processus ] est ex\'{e}cut\'{e}'',
\end{quote} ont \'{e}t\'{e} introduites aux USA en 1994, peu de temps apr\`{e}s l'audition de l'Office de Brevets des \'{E}tats Unis (USPTO) dans laquelle toutes les grandes soci\'{e}t\'{e}s informatiques \`{a} l'exception de Microsoft se sont prononc\'{e}es contre la brevetabilit\'{e} logicielle. En Europe, Les revendications de programme ont \'{e}t\'{e} introduites au m\'{e}pris de la loi \'{e}crite par l'Office Europ\'{e}en des Brevets (OEB) en 1998, imm\'{e}diatement suivi par le Office de Brevets du Royaume-Uni (UKPO) et plus tard par quelques autres bureaux des brevets nationaux.  En 2002 la Commission Europ\'{e}enne proposait d'annuler cette d\'{e}cision.

L'Article 5 de la Proposition de Directive sur les Brevets Logiciels de la Commission Europ\'{e}enne n'autorise pas les revendications de programme, c'est-\`{a}-dire de la forme ``programme d'ordinateur, caract\'{e}ris\'{e} par le fait que lorsqu'il se charge en m\'{e}moire [ un processus ] est ex\'{e}cut\'{e}''.  Comme justification, la Commission Europ\'{e}enne dit qu'elle ne veut pas que ``le logiciel en tant que tel'' soit brevetable.  Cependant la proposition de la Commission parle de ``programmes contrefacteurs de brevets'' et affirme que l'ex\'{e}cution d'un programme sur un ordinateur d'usage g\'{e}n\'{e}ral peut \^{e}tre une infraction de brevet, attendu que sa publication ou sa distribution peut ou peut n'\^{e}tre pas vue comme une infraction, en fonction de la mani\`{e}re dont les tribunaux appliquent la doctrine de l'``infraction indirecte''.  Des critiques de toutes part ont trouv\'{e} ceci contradictoire.  Les comit\'{e}s de la culture et des affaires industrielles du Parlement Europ\'{e}en (CULT et ITRE) ont vot\'{e} des amendements qui pr\'{e}serve la libert\'{e} de publication.  L'amendement ITRE-13 stipule que la publication d'un programme ne peut jamais constituer une infraction de brevet.   La Commission des Affaires Juridiques (JURI), d'autre part, a explicitement rejet\'{e} le ITRE-13 et a, \`{a} la place vot\'{e} pour un  ``amendement compromis 1'', qui l\'{e}galise timidement les revendications de programme (avec une pseudo-limitation: ``seulement si ... [ les revendications de programme sont d\'{e}riv\'{e}es des revendications de processus ]'').  Selon ce ``compromis'' entre le lobby des brevets et le lobby des brevets, les auteurs de logiciels et les fournisseurs d'acc\`{e}s \`{a} Internet (FAIs) peuvent \^{e}tre poursuivis pour infraction de brevet directe n'importe o\`{u} dans l'Union Europ\'{e}enne, aussit\^{o}t qu'ils auront fait un programme texte avec les fonctions revendiqu\'{e}es, disponible quelque part sur Internet.

La JURI l\'{e}galise \emph{les revendications de programme}, c'est-\`{a}-dire pour

\begin{center}
\begin{quote}
{\it un programme d'ordinateur, caract\'{e}ris\'{e} par le fait que lorsqu'il se charge en m\'{e}moire [ un processus ] est ex\'{e}cut\'{e}.}
\end{quote}
\end{center}

Ceci mettrait la publication de beaucoup de programmes en infraction directe de brevet, donc cr\'{e}erait des risques de litiges suppl\'{e}mentaires pour les programmeurs, les chercheurs universitaires et les fournisseurs d'acc\`{e}s \`{a} Internet.

Les revendications de programme poussent le syst\`{e}me de brevet jusqu'\`{a} l'absurde.  Pouss\'{e} \`{a} l'extr\^{e}me de la logique, cela signifie que les descriptions de brevets sont brevet\'{e}es: en publiant la d\'{e}claration compl\`{e}te (qui dans le cas de logiciel devrait contenir du code source utilisable), l'office des brevets serait en infraction sur les revendications du m\^{e}me brevet.  Ceci illustre joliment pourquoi le logiciel ne peut pas \^{e}tre brevetable: les brevets sont suppos\'{e}s \^{e}tre des contrats entre le d\'{e}tenteur du brevet et le public, o\`{u} l'information est r\'{e}v\'{e}l\'{e}e et des obj\`{e}ts mat\'{e}riels sont monopolis\'{e}s.
\end{sect}

\begin{sect}{just}{Les Justifications Courantes des Revendications de Programme}
voir JURI Art 5\footnote{http://swpat.ffii.org//papiers/eubsa-swpat0202/juri0304/index.fr.html\#art5}

L'argument de ``protection du consommateur'' en faveur des revendications de programme est sans fondement et cynique.  Il est toujours possible pour l'utilisateur final d'un logiciel commercial de tenir le distributeur responsable au moyen de clauses contractuelles standards, et c'est ce qui arrive habituellement, si le logiciel est brevetable sans revendications de programme.

La vraie raison pour laquelle les proposants du brevet logiciel insistent sur les revendications de programme semble \^{e}tre symbolique:  l'existence de revendications de programme documente de fa\c{c}on brutale et ind\'{e}cente que la fronti\`{e}re du brevetage logiciel en tant que tel a \'{e}t\'{e} franchie.  Inversement, en refusant d'autoriser les revendications de programme, la Commission Europ\'{e}enne peut appara\^{\i}tre comme mod\'{e}r\'{e}e.

Cependant, afin de prot\'{e}ger la libert\'{e} de publication, comme garanti par l'Article 10 Convention Europ\'{e}enne sur les Droits Humains (CEDR), le simple refus d'une forme de revendication ind\'{e}cente n'est pas suffisant.  Une affirmation plus explicite sur la libert\'{e} de publication, comme le ITRE-13, est n\'{e}cessaire.

Finalement, dans un autre contexte, il doit \^{e}tre clarifi\'{e} que les objets d'information quelle que soit leur forme ne peuvent \^{e}tre brevetable.
\end{sect}

\begin{sect}{amcc}{McCarthy: Les Revendications de Programme rendent la Libert\'{e} de Publication ``Caduque''}
De fa\c{c}on int\'{e}ressante, la JURI n'a pas vot\'{e} du tout le ITRE-13, car Arlene McCarthy l'a jug\'{e} incompatible avec son amendement sur les revendications de programme COMP-1 et aussi ``caduc'' si le COMP-1 \'{e}tait vot\'{e} favorablement, ce qui a \'{e}t\'{e} le cas.  En d'autres termes: Arlene McCarthy admet que les revendications de programme sont une n\'{e}gation directe de la libert\'{e} de publication.   On pourrait concevoir un scenario o\`{u} les revendications de programme seraient vraiment consid\'{e}r\'{e}s comme \'{e}tant seulement ``de nature d\'{e}clarative'' (comme il est dit dans la justification du COMP-1) et la libert\'{e} de publication serait tout de m\^{e}me maintenue.  De tels sc\'{e}narios ne sont pas rares dans la loi.  Cependant pour la JURI le sujet est clair:  d'un c\^{o}t\'{e} pr\'{e}tendent de n'avoir rien fait en pr\'{e}sentant les revendications de programme comme ``simplement de nature d\'{e}clarative'', d'un autre c\^{o}t\'{e} leur d\'{e}cision est fond\'{e}e sur le postulat que les revendications de programme et la libert\'{e} de publication sont si compl\`{e}tement et mutuellement exclusives que le ITRE-13 n'a m\^{e}me pas besoin d'\^{e}tre mis au vote.
\end{sect}

\begin{sect}{links}{Liens annot\'{e}s}
\begin{itemize}
\item
{\bf {\bf Les propositions et justifications de la JURI sur les revendications de programme\footnote{http://swpat.ffii.org//papiers/eubsa-swpat0202/juri0304/index.fr.html\#art5}}}

\begin{quote}
Le COMP-1 a \'{e}t\'{e} approuv\'{e}.  Il \'{e}tend la brevetabilit\'{e} compar\'{e} \`{a} la proposition de la Commission en introduisant les revendications de programme, mais pr\'{e}sente de fa\c{c}on trompeuse cette extension comme une restriction, en disant que les revendications de programme sont ``seulement'' admises pour autant qu'elles soient \'{e}quivalentes aux revendications de processus, ce qui bien s\^{u}r est toujours le cas.
\end{quote}
\filbreak

\item
{\bf {\bf Phil Salin: Freedom of Speech in Software\footnote{http://philsalin.com/patents.html}}}

\begin{quote}
A text which explains in clear and easy-to-understand terms why software patents violate freedom of speech.

voir aussi Seth Johnson 2003/08: Phil Salin on Software Patents\footnote{http://aful.org/wws/arc/patents/2003-08/msg00091.html}
\end{quote}
\filbreak

\item
{\bf {\bf CCE \& BSA 2002-02-20: Proposition pour rendre toutes les id\'{e}es utiles brevetables\footnote{http://swpat.ffii.org//papiers/eubsa-swpat0202/index.fr.html}}}

\begin{quote}
La Commission n'autorise pas les revendications de programme, mais sugg\`{e}re entre les lignes qu'elles devraient \^{e}tre autoris\'{e}es.
\end{quote}
\filbreak

\item
{\bf {\bf Interop\'{e}rabilit\'{e} et Brevet: Controverse au Parlement europ\'{e}en\footnote{http://swpat.ffii.org//papiers/eubsa-swpat0202/itop/index.fr.html}}}

\begin{quote}
An article from AIPLA (Erwin Basinski) published here suggests that the European Commission won't mind if the European Parliament reintroduces program claims.  AIPLA is usually very well informed about the thinking of their patent lawyer colleagues at the European Commission's Directorate for the Internal Market, who are in charge of directive drafting.
\end{quote}
\filbreak

\item
{\bf {\bf EPO T 1173/97: IBM Computer Program Product\footnote{http://swpat.ffii.org//papiers/epo-t971173/index.fr.html}}}

\begin{quote}
La d\'{e}cision de 1997 de EPO pour introduire les revendications de programme
\end{quote}
\filbreak

\item
{\bf {\bf The UK Patent Family and Software Patents\footnote{http://swpat.ffii.org//acteurs/uk/index.fr.html}}}

\begin{quote}
L'Office des Brevets du Royaume-Uni a \'{e}t\'{e} le premier \`{a} suivre le OEB en introduisant les revendications de programme en 1998
\end{quote}
\filbreak

\item
{\bf {\bf BPatG Fehlersuche 2000-07-28: Patentanspr\"{u}che auf ``Computerprogrammprodukt'' etc unzul\"{a}ssig\footnote{http://swpat.ffii.org//papiers/bpatg17-suche00/index.fr.html}}}

\begin{quote}
D\'{e}cision de la court F\'{e}d\'{e}rale de Brevet Allemande de 2000 contre les revendications de programme, stipulant explicitement que la pratique de l'OEB est incompatible avec la loi \'{e}crite et ne doit pas \^{e}tre suivie.
\end{quote}
\filbreak

\item
{\bf {\bf CEU/DKPTO 2002/09/23..: Software Patentability Directive Amendment Proposal\footnote{http://swpat.ffii.org//papiers/eubsa-swpat0202/dkpto0209/index.fr.html}}}

\begin{quote}
Sous l'impulsion des officiels britanniques et allemands des administrations de brevet gouvernementales, le Groupe de Travail sur la Propri\'{e}t\'{e} Industrielle du Conseil Europ\'{e}en se prononce en faveur des revendications de programme.
\end{quote}
\filbreak

\item
{\bf {\bf Software Patents in Finnland\footnote{http://swpat.ffii.org//acteurs/fi/index.fr.html}}}

\begin{quote}
L'Office des Brevets finlandais a introduit les revendications de programmes en 2003, peu de temps apr\`{e}s que le CULT et le ITRE aient recommand\'{e} ne ne pas le faire
\end{quote}
\filbreak

\item
{\bf {\bf Portugal and Software Patents\footnote{http://swpat.ffii.org//acteurs/pt/index.fr.html}}}

\begin{quote}
L'Office des Brevets Portugais, suivant ostensiblement la voie de l'expert en brevets du gouvernement allemand, se prononce en faveur des revendications de programme
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: latex ;
% End: ;

