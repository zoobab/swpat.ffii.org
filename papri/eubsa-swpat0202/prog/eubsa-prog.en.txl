<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Program Claims: Bans on Publication of Patent Descriptions

#descr: Patent Claims to %(q:computer program, characterised by that upon loading it into memory [ some process ] is executed), are called %(q:program claims), %(q:Beauregard claims), %(q:In-re-Lowry-Claims), %(q:program product claims), %(q:text claims) or %(q:information claims).  Patents which contain these claims are sometimes called %(q:text patents) or %(q:information patents).  Such patents no longer monopolise a physical object but a description of such an object.  Whether this should be allowed is one of the controversial questions in the struggle about the proposed EU Software Patent Directive.  We try to explain how this debate emerged and what is really at stake.

#rom: Standard Justifications for Program Claims

#Wri: McCarthy: Program Claims render Freedom of Publication %(q:Caduc)

#sim: Patent Claims directed to a %(bq|%(q:computer program product), %(q:computer program), %(q:data structure) or other information object, %(q:characterised by that upon reading out the information ... happens),) were introduced in the USA in 1994, shortly after a USPTO hearing in which all major software companies except for Microsoft pronounced themselves against software patentability.  In Europe, program claims were introduced in defiance of the written law by the European Patent Office in 1998, followed immediately by the UK Patent Office and later by some more national patent offices.  In 2002 the European Commission proposed to reverse this decision.

#gWn: The European Commission's Software Patent Directive Proposal's Article 5 does not allow program claims, i.e. claims of the form %(q:computer program, characterised by that upon loading it into memory [ some process ] is executed).  In justification, the European Commission said that it did not want %(q:software as such) to be patentable.  Yet the Commission's proposal speaks of %(q:infringing programs) and ensures that at least the execution of a computer program on a general-purpose computer can be a patent infringement, whereas its publication or distribution may or may not be seen as an infringement, depending on how the courts apply the doctrine of %(q:contributory infringement).  Critics from all sides have found this inconsistent.  The European Parliament's committees for cultural and industrial affairs (CULT and ITRE) voted for amendments which safeguard freedom of publication.  Amendment ITRE-13 states that the publication of a program can never constitute a patent infringement.   The Legal Affairs Commission (JURI), on the other hand, explicitely rejected ITRE-13 and instead voted for a %(q:compromise amendment 1), which bashfully legalises program claims (with a pseudo-limitation: %(q:only if ... [ the program claims are derived from process claims ])).  According to this %(q:compromise) between the patent lobby and the patent lobby, software authors and Internet service providers (ISPs) can be sued for direct patent infringement everywhere in the European Union, as soon as they make a program text with the claimed features available somewhere on the Internet.

#2es: The European Parliament's Committe for Legal Affairs and the Internal Market (JURI), in its %(jr:resolution of 2003/06/18), proposes to legalise %(e:program claims), i.e. claims to

#aiW: a computer program, characterised by that upon loading it into computer memory [ some process ] is executed.

#pis: This would make publication of many programs a direct patent infringement, thus creating additional risks of litigation for programmers, university researchers and Internet service providers.

#tPt: Program claims also lead the patent system ad absurdum.  Taken to the extreme of consistency, they mean that patent descriptions are patented: by publishing the full disclosure (which in case of software should contain some usable source code), the patent office would infringe on the program claim.  This illustrates nicely why software cannot be patentable: patents are supposed to be deals between the patentee and the public, where information is disclosed and matter monopolised.

#UWt: JURI Art 5

#tse: The %(q:consumer-protection) argument in favor of program claims is groundless and cynical.  It is always possible for the end user of commercial software to hold the distributor liable by means of standard contract clauses, and this is what would usually happen, if software is patentable without program claims.

#pWt: The real reason why industrial patent lawyers are pushing for program claims seems to be a symbolic one:  the existence of program claims brutally and indecently documents that the border to patenting software as such has been crossed.  Conversely, by refusing to grant program claims, the European Commission can appear as moderate.

#sit: However, in order to protect the freedom of publication, as guaranteed by Art 10 ECHR, the mere refusal of an indecent claim form is not enough.  A more explicit affirmation of the freedom of publication, such as ITRE-13, is needed.

#nto: Ultimately, in another context, it must be made clear that information objects in whatever form are not patentable.

#tnt: Interestingly, JURI did not vote on ITRE-13 at all, because Arlene McCarthy judged it incompatible with her program claim amendment COMP-1 and therefore %(q:caduc) (obsolete) if COMP-1 was voted for, which it was.  In other words: Arlene McCarthy admits that program claims are a direct negation of the freedom of publication.   One could conceive of a scenario where program claims are really considered to be %(q:only of declaratory nature) (as said in the justification of COMP-1) and freedom of publication is nevertheless maintained.  Such scenarios are not rare in law.  Yet for JURI the matter is clear:  on the one hand side they downplay the impact of program claims by calling them %(q:merely of declaratory nature), on the other their decision is built on the implicit assumption that program claims and freedom of publication are so completely mutually exclusive that ITRE-13 needn't even be put to vote.

#klW: Introduction, background and mailin list discussion on a classical text about freedom of speech and its meaning in software.

#ofr: JURI proposals and justifications on program claims

#iiv: COMP-1 was approved.  It extends patentability compared to the European Commission's proposal by introducing program claims, but misleadingly presents this extension as a restriction, by saying that program claims are %(q:only) admitted in as far as they are equivalent to process claims, which of course they always are.

#bWn: The Commission Proposal does not allow program claims, but suggests between the lines that they should be allowed.

#avW: An article from %(AI) published here suggests that the European Commission won't mind if the European Parliament reintroduces program claims.  AIPLA is usually very well informed about the thinking of their patent lawyer colleagues at the European Commission's Directorate for the Internal Market, who are in charge of directive drafting.

#cor: EPO decision of 1997 to introduce program claims

#fWr: UK Patent Office was the first to follow the EPO in introducing program claims in 1998

#cWg: German court decision of 2000 against program claims

#num: Under leadership of UK and DE governmental patent administration officials, the European Council's Industrial Property Working Party recommends in favor of program claims.

#f0e: Finnish patent office introduced program claims in 2003, shortly after CULT and ITRE had recommended against them

#fgW: Portugues patent office, ostentatively following lead of German governmental patent lawyer, pronounces itself in favor of program claims

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-prog ;
# txtlang: en ;
# multlin: t ;
# End: ;

