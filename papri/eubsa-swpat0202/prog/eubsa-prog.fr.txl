<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Revendications de programmes: Interdiction de Publication des  descriptions de brevets utiles

#descr: Les revendications de brevet portant sur %(q:un programme  d'ordinateur, caractérisé en ce qu'étant chargé dans une mémoire [un  process] est exécuté), s'appellent %(q:revendications de programme),  %(q:revendications Beauregard), %(q:revendications In-re-Lowry),  %(q:revendications de produit-programme). Les brevets contenant ces  revendications sont parfois appelés %(q:brevets de texte) ou %(q:brevets  d'information).  De tels brevets monopolisent non seulement un objet  physique mais aussi une description de cet objet. La question de savoir  si cela peut être permis est l'une des questions controversée dans la  bataille concernant la proposition de directive sur les brevets  logiciels de l'Union Européenne. Nous essayons d'expliquer comment ce  débat est apparu et ce qui est réellement en jeu.

#rom: Les Justifications Courantes des Revendications de Programme

#Wri: McCarthy: Les Revendications de Programme rendent la Liberté de Publication %(q:Caduque)

#sim: Les Revendications de Brevet concernant les %(bq|%(q:produit programme), %(q:programme pour ordinateur), %(q:structure de données) ou autre objet d'information , %(q:charactérisé en ce que lorsqu' il se charge en mémoire d'ordinateur [ un processus ] est exécuté),) ont été introduites aux USA en 1994, peu de temps après l'audition de l'Office de Brevets des États Unis (USPTO) dans laquelle toutes les grandes sociétés informatiques à l'exception de Microsoft se sont prononcées contre la brevetabilité logicielle. En Europe, Les revendications de programme ont été introduites au mépris de la loi écrite par l'Office Européen des Brevets (OEB) en 1998, immédiatement suivi par le Office de Brevets du Royaume-Uni (UKPO) et plus tard par quelques autres bureaux des brevets nationaux.  En 2002 la Commission Européenne proposait d'annuler cette décision.

#gWn: L'Article 5 de la Proposition de Directive sur les Brevets Logiciels de la Commission Européenne n'autorise pas les revendications de programme, c'est-à-dire de la forme %(q:programme d'ordinateur, caractérisé par le fait que lorsqu'il se charge en mémoire [ un processus ] est exécuté).  Comme justification, la Commission Européenne dit qu'elle ne veut pas que %(q:le logiciel en tant que tel) soit brevetable.  Cependant la proposition de la Commission parle de %(q:programmes contrefacteurs de brevets) et affirme que l'exécution d'un programme sur un ordinateur d'usage général peut être une infraction de brevet, attendu que sa publication ou sa distribution peut ou peut n'être pas vue comme une infraction, en fonction de la manière dont les tribunaux appliquent la doctrine de l'%(q:infraction indirecte).  Des critiques de toutes part ont trouvé ceci contradictoire.  Les comités de la culture et des affaires industrielles du Parlement Européen (CULT et ITRE) ont voté des amendements qui préserve la liberté de publication.  L'amendement ITRE-13 stipule que la publication d'un programme ne peut jamais constituer une infraction de brevet.   La Commission des Affaires Juridiques (JURI), d'autre part, a explicitement rejeté le ITRE-13 et a, à la place voté pour un  %(q:amendement compromis 1), qui légalise timidement les revendications de programme (avec une pseudo-limitation: %(q:seulement si ... [ les revendications de programme sont dérivées des revendications de processus ])).  Selon ce %(q:compromis) entre le lobby des brevets et le lobby des brevets, les auteurs de logiciels et les fournisseurs d'accès à Internet (FAIs) peuvent être poursuivis pour infraction de brevet directe n'importe où dans l'Union Européenne, aussitôt qu'ils auront fait un programme texte avec les fonctions revendiquées, disponible quelque part sur Internet.

#2es: La JURI légalise %(e:les revendications de programme), c'est-à-dire pour

#aiW: un programme d'ordinateur, caractérisé par le fait que lorsqu'il se charge en mémoire [ un processus ] est exécuté.

#pis: Ceci mettrait la publication de beaucoup de programmes en infraction directe de brevet, donc créerait des risques de litiges supplémentaires pour les programmeurs, les chercheurs universitaires et les fournisseurs d'accès à Internet.

#tPt: Les revendications de programme poussent le système de brevet jusqu'à l'absurde.  Poussé à l'extrême de la logique, cela signifie que les descriptions de brevets sont brevetées: en publiant la déclaration complète (qui dans le cas de logiciel devrait contenir du code source utilisable), l'office des brevets serait en infraction sur les revendications du même brevet.  Ceci illustre joliment pourquoi le logiciel ne peut pas être brevetable: les brevets sont supposés être des contrats entre le détenteur du brevet et le public, où l'information est révélée et des objèts matériels sont monopolisés.

#UWt: JURI Art 5

#tse: L'argument de %(q:protection du consommateur) en faveur des revendications de programme est sans fondement et cynique.  Il est toujours possible pour l'utilisateur final d'un logiciel commercial de tenir le distributeur responsable au moyen de clauses contractuelles standards, et c'est ce qui arrive habituellement, si le logiciel est brevetable sans revendications de programme.

#pWt: La vraie raison pour laquelle les proposants du brevet logiciel insistent sur les revendications de programme semble être symbolique:  l'existence de revendications de programme documente de façon brutale et indécente que la frontière du brevetage logiciel en tant que tel a été franchie.  Inversement, en refusant d'autoriser les revendications de programme, la Commission Européenne peut apparaître comme modérée.

#sit: Cependant, afin de protéger la liberté de publication, comme garanti par l'Article 10 Convention Européenne sur les Droits Humains (CEDR), le simple refus d'une forme de revendication indécente n'est pas suffisant.  Une affirmation plus explicite sur la liberté de publication, comme le ITRE-13, est nécessaire.

#nto: Finalement, dans un autre contexte, il doit être clarifié que les objets d'information quelle que soit leur forme ne peuvent être brevetable.

#tnt: De façon intéressante, la JURI n'a pas voté du tout le ITRE-13, car Arlene McCarthy l'a jugé incompatible avec son amendement sur les revendications de programme COMP-1 et aussi %(q:caduc) si le COMP-1 était voté favorablement, ce qui a été le cas.  En d'autres termes: Arlene McCarthy admet que les revendications de programme sont une négation directe de la liberté de publication.   On pourrait concevoir un scenario où les revendications de programme seraient vraiment considérés comme étant seulement %(q:de nature déclarative) (comme il est dit dans la justification du COMP-1) et la liberté de publication serait tout de même maintenue.  De tels scénarios ne sont pas rares dans la loi.  Cependant pour la JURI le sujet est clair:  d'un côté prétendent de n'avoir rien fait en présentant les revendications de programme comme %(q:simplement de nature déclarative), d'un autre côté leur décision est fondée sur le postulat que les revendications de programme et la liberté de publication sont si complètement et mutuellement exclusives que le ITRE-13 n'a même pas besoin d'être mis au vote.

#klW: Introduction, background and mailin list discussion on a classical text about freedom of speech and its meaning in software.

#ofr: Les propositions et justifications de la JURI sur les revendications de programme

#iiv: Le COMP-1 a été approuvé.  Il étend la brevetabilité comparé à la proposition de la Commission en introduisant les revendications de programme, mais présente de façon trompeuse cette extension comme une restriction, en disant que les revendications de programme sont %(q:seulement) admises pour autant qu'elles soient équivalentes aux revendications de processus, ce qui bien sûr est toujours le cas.

#bWn: La Commission n'autorise pas les revendications de programme, mais suggère entre les lignes qu'elles devraient être autorisées.

#avW: An article from %(AI) published here suggests that the European Commission won't mind if the European Parliament reintroduces program claims.  AIPLA is usually very well informed about the thinking of their patent lawyer colleagues at the European Commission's Directorate for the Internal Market, who are in charge of directive drafting.

#cor: La décision de 1997 de EPO pour introduire les revendications de programme

#fWr: L'Office des Brevets du Royaume-Uni a été le premier à suivre le OEB en introduisant les revendications de programme en 1998

#cWg: Décision de la court Fédérale de Brevet Allemande de 2000 contre les revendications de programme, stipulant explicitement que la pratique de l'OEB est incompatible avec la loi écrite et ne doit pas être suivie.

#num: Sous l'impulsion des officiels britanniques et allemands des administrations de brevet gouvernementales, le Groupe de Travail sur la Propriété Industrielle du Conseil Européen se prononce en faveur des revendications de programme.

#f0e: L'Office des Brevets finlandais a introduit les revendications de programmes en 2003, peu de temps après que le CULT et le ITRE aient recommandé ne ne pas le faire

#fgW: L'Office des Brevets Portugais, suivant ostensiblement la voie de l'expert en brevets du gouvernement allemand, se prononce en faveur des revendications de programme

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: ccorazza ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-prog ;
# txtlang: fr ;
# multlin: t ;
# End: ;

