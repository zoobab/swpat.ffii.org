<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: CEC & BSA ./ Sverige

#descr: In a statement submitted to the Swedish Ministery of Justice on behalf of SSLUG, a group of 5500 programmers and users of free software in the area around Copenhagen and Malmö, Erik Joseffson shows how an influential group at the European Commission and the European Patent Office has eroded the standards of patentability and is trying to impose a regime of unlimited patentability on Sweden.  This influential group has also, by overstretching the competence of the European Patent Office (EPO), illegally overruled the Swedish courts, thereby inflicting damage on the swedish constitutional order and on software developpers in Sweden.  Even in their most recent decisions in the mid-nineties, the Swedish courts did not agree with the EPO's illegal practise, but now the European Commission is set to force this practise on Sweden by means of %(q:european harmonisation).  It was the duty of the EPO to abide by a role of %(q:cold harmonisation) in the first place: act as a conservative follower and summariser of national caselaw rather than as an innovative trendsetter pursuing its own agenda.  Josefsson cites ample examples of patents granted by the EPO and rejected by Swedish courts, and of damage done by such patents in Sweden.

#Sth: Skriftliga synpunkter på Europaparlamentets och rådets direktiv om patenterbarhet för %(q:datorrelaterade uppfinningar)

#SAT: SAMMANFATTNING

#SPK: SYNPUNKTER

#CET: CASE STUDY

#HRT: HÄMMAS ELLER FRÄMJAS UTVECKLINGEN?

#OEO: OBESVARADE FRÅGESTÄLLNINGAR

#VxA: VAD ÄR EN %(q:DATORRELATERAD UPPFINNING)?

#UTO: UPPKOMST AV EPOs PRAXIS

#KIP: KODIFIERING AV EPOs PRAXIS

#DoW: Direktivförslaget legaliserar 30.000 patent på datorprogram och affärsmetoder som idag inte är giltiga under EPC. Majoriteten av dessa ägs av utomeuropeiska bolag.

#Dlh: Direktivförslaget överför nyttjanderätt av intellektuell egendom av ett omfattande värde från upphovsrättsinnehavare till patentägare i strid med europakonventionen.

#Dml: Direktivförslaget minskar rättssäkerheten genom att implementera en oöverskådlig konflikt mellan industriellt och intellektuellt rättsskydd.

#Drr: Direktivförslaget legaliserar patentering av alla datorprogram som körs på en dator.

#Dgs: Direktivförslaget legaliserar patentering av alla affärsmetoder som implementeras i ett datorprogram som körs på en dator.

#EiE: Enligt svensk lag är datorprogram och affärsmetoder inte uppfinningar och kan inte patenteras. Direktivförslaget kräver därför omfattande svensk lagändring.

#PEW: Patent motverkar utvecklingen av fria och öppna standarder, och hämmar därmed en fri och öppen utveckling av nätverkssamhället.

#Ato: Av de många studier som gjorts visar ingen att patent på datorprogram leder till ökad produktivitet, innovation och kunskapsspridning.

#Iro: Introduktionen av patent i programvarubranschen hämmar utvecklingen, minskar konkurrensen och ökar monopolbildningen.

#IeW: Introduktionen hämmar särskilt små och medelstora företag som inte har resurser att bygga upp juridisk kompetens att hantera frågor om intrång, giltighet, licensiering och ansökningar av patent eller att bygga upp en patentportfölj.

#SWg: SSLUG anser att ett starkt upphovsrättsligt skydd för datorprogram skapar förutsättningar för en dynamisk och kreativ utveckling.

#Srl: SSLUG vill betona att upphovsrättsligt skydd av datorprogram är förutsättningen för den fortsatta expansiva utvecklingen av fri programvara och ett fritt och öppet Internet.

#Sk5: SSLUG %(cp:föreslår) att rättsläget för skydd av datorprogram klargörs genom att direktivförslaget dras tillbaka, EPC 52.3 stryks och nya patentgranskningsregler utarbetas.

#Vi2: Vi är mycket glada för möjligheten att delta i diskussionen om kommissionens direktivförslag om patenterbarhet av %(q:datorrelaterade uppfinningar). SSLUG har sedan föreningen bildades 1995 förespråkat öppna standarder, öppen källkod och friheten att välja, och har aktivt deltagit i debatten om patent på datorprogram sedan kommissionens hearing hösten 2000. Frågan diskuteras livligt av SSLUGs ca 6000 medlemmar på mailinglistan sslug-it-politik@sslug.dk och har varit en punkt på dagordningen på flera års- och %(ms:styrelsemöten).

#TjW: Tyvärr har vi upplevt att den offentliga debatten inte alltid varit ärlig, öppen och demokratisk. Representanter för ekonomiskt starka intressegrupper, och för patentsystemet, har givits tolkningsföreträde framför användare, programmerare och andra professionella i mjukvarubranschen. Som exempel kan nämnas att författaren till en förlaga till kommissionens direktivförslag, Francisco Mingorance, %(bs:arbetar för %(BSA)) samt att en anställd vid danska patentverket offentligt uttalat att det finns vetenskapliga studier som visar att Open Source Software (OSS) inte drabbas av %(mv:mjukvarupatent) (vilket i alla delar är ett oriktigt påstående).

#Dif: Dessa representanters tolkningsföreträde avspeglas i kommissionens direktivförslag. Som exempel kan nämnas att Eurolinux upprop mot %(mv:mjukvarupatent), som idag har nästan 120.000 underskrifter och är en av de största europeiska petitionerna på nätet, inte nämns i direktivtexten och att de avvikande åsikter som kom in till kommissionens hearing hösten 2000, varav 1400 kopior administrerade genom %(ew:Eurolinux), i texten lämnas utan analys och avfärdas med argumentet att de framförts av en ekonomisk minoritet (not i). Bristen på övergripande analys och referenser till forskning kring direktivets konsekvenser för det europeiska informationssamhället är iögonenfallande.

#Edb: En av SSLUGs medlemmar, Tord Jansson, började i slutet på 90-talet skriva på ett datorprogram som konverterar ett filformat till ett annat. Utifrån filformatens dokumenterade standarder och ISOs %(rk:referenskod) började han hitta programmeringslösningar som skulle göra konverteringen så snabb och effektiv som möjligt. Första versionen av datorprogrammet BladeEnc blev klar 1997-98. Tord valde en modern öppen licensform för att få så många användare (och bugfixare) som möjligt. Inom kort hade BladeEnc flera miljoner användare runt om i världen.

#Iee: I september 1998 fick Tord ett brev från ett företag som hävdade att det har flera patent utställda av EPO som gäller i Sverige. De påstod att Tords program gör intrång på dessa %(on:patent).

#DdW: Direktivförslaget ger företaget rätt. EPO-patenten avser varje tekniskt förverkligande av %(q:de underliggande idéerna och principerna).   Varje enskild implementation av konverteringsproblemet i ett datorprogram som körs på vilken som helst dator är ett sådant tekniskt förverkligande och gör därför intrång på EPO-patenten.

#Sen: Svensk lagstiftning ger Tord Jansson rätt att skriva och använda och distribuera sin intellektuella egendom på det sätt han önskar och garanterar att patentanspråken inte är giltiga eftersom hans datorprogram inte är en uppfinning.

#Paa: På grund av det oklara rättsläget har Tord efter juridisk rådgivning valt att sluta distribuera BladeEnc.

#Dsl: Det visar sig att flera andra programvaruutvecklare som utvecklat andra konverteringsprogram också fått liknande brev. Många av dem lägger ner sina projekt.

#Ngm: Nuvarande svensk lagstiftning skyddar varje enskild implementation mot kopiering och plagiering och lämnar utrymme för innovation och konkurrens. Direktivförslaget inför en lagstiftning som ger den som först patenterar en %(q:grundläggande ide) möjlighet att helt dominera marknaden.

#Kri: Konsekvenserna av att %(q:datorrelaterade uppfinningar) kan förverkligas på vilken dator som helst är oöverskådliga. Det drabbar inte bara innovativa programvaruutvecklare som Tord Jansson utan också varje företag som i sin verksamhet yrkesmässigt redan använder datorer som kör datorprogram vars %(q:grundläggande idéer och principer) kan bli patenterade (not vii).

#FWt: Forskning visar att patent i branscher som karakteriseras av sekventiell innovation hämmar %(ri:utvecklingen). Amerikanska erfarenheter visar att särskilt små och innovativa programvaruföretag måste ta sig igenom en %(q:matta) av patentanspråk innan de kan nå marknaden och börja tjäna %(st:pengar). Svenska IT-företag är oroliga för att ökade kostnader för patentadministration inte ska kompenseras av ökade %(sw:vinster).

#KsE: Kunskapsspridning är en förutsättning för utveckling och innovation. Patentsystemets bidrag till kunskapsspridning på mjukvaruområdet är marginell. Dels av skälet att programmerare inte läser patenthandlingar (de läser källkod och manualer) och dels för att en stor del av programvaruutvecklingen idag sker helt öppet på %(in:Internet). Samhället får idag ta del av denna kunskap, utan att behöva erbjuda patent i utbyte. Det finns inga skäl att störa den bytesbalansen.

#Dkc: Den mest innovativa delen av den svenska it-branschen är små företag med upp till 20 anställda. En enkel beräkning visar att endast mottagandet av ett enda patenthot per år och företag kostar den delen av mjukvarubranschen över 60 miljoner kronor (not ix). Risken för att investeringsviljan i detta segment sjunker är uppenbar.

#Hax: Hur påverkas följande områden av införandet av patent på %(q:datorrelaterade uppfinningar)?

#icu: innovation och utveckling inom mjukvaruområdet i allmänhet

#ikc: innovation och utveckling inom Open Source och Free Software

#uWW: utveckling av och tillgång till Internet

#udW: utgivare och distributörer av programvara

#pve: programvaruutvecklare

#kca: konsumenter och användare av programvara

#fea: forskning och utveckling inom den akademiska världen

#SWm: SSLUG anser inte att kommissionen tillräckligt väl har undersökt frågeställningarna och rekommenderar därför att direktivet dras tillbaka.

#DFn: Datorprogram har idag ett starkt rättsligt skydd under upphovsrätten. Datorprogram är komplexa logiska verk som består av tiotusentals enskilda logiska och matematiska problemlösningar. Flera av dessa problemlösningar är flera sekler gamla, andra är helt nya. Upphovsrätten skyddar på ett adekvat sätt varje datorprograms unika sammansättning av nya och gamla problemlösningar på samma sätt som den skyddar enskilda litterära och musikaliska verks unicitet.  Datorprogram räknas därför tillsammans med litteratur och musik idag till %(q:Intellectual Property).

#Dns: Datorprogram är idag inte patenterbara. De är tillsammans med bl a matematiska metoder undantagna från patentering av skälet att dessa problemlösningar inte räknas som uppfinningar. En uppfinning är en lösning på ett tekniskt problem. Tekniska problem handlar i grund och    botten om att kontrollera fysiska objekt eller de naturlagar som verkar på dessa objekt (se förarbeten till patentlagen). Om en sådan teknisk problemlösning kan tillgodogöras industriellt kan den under vissa ytterligare villkor skyddas av ett patent. Uppfinningar räknas därför tillsammans med bl a mönsterskydd idag till %(q:Industrial Property).

#Vee: Vad är då en %(q:datorrelaterad uppfinning)? Är det en lösning på ett tekniskt problem eller en lösning på ett logiskt problem? Är det en uppfinning eller inte? Skall en %(q:datorrelaterad uppfinning) räknas till %(q:Intellectual Property) eller %(q:Industrial Property)?

#Doa: Definitionen i kommissionens direktivförslag (artikel 2, sid 18) lyder:

#Etg: En %(q:datorrelaterad uppfinning) är en uppfinning

#vun: vars verkan förutsätter användning av en dator

#sca: som uppvisar kännetecken som verkställs av ett datorprogram.

#Dtr: Definitionen kan inte härledas ur svensk rättspraxis utan måste förstås som en kodifiering av den praxis som utvecklats inom EPO.

#FvW: För att värdera direktivförslagets ambition att %(q:skapa tydligare regler för patenterbarhet) (sid 3) är det nödvändigt att analysera orsakerna till att %(q:det saknas rättslig förutsägbarhet) (sid 2) för skydd av datorprogram, dvs orsakerna till att praxis i EPO:s besvärsinstanser avviker från den som tillämpas i medlemsstaternas domstolar.

#Gro: Generellt vid ansökningar om patent, har i Sverige frågan om ansökningens problemlösning tillhört logikens eller teknikens område varit vägledande och avgörande för patentlagens tolkning och tillämpning. I %(sd:nedanstående exempel) gäller bedömningen en ansökan, som avsett en problemlösning för att hålla reda på köordningen till olika kassor. I ett domslut från 1994 (T1002/92) framgår att EPO och det svenska patentväsendet har diametralt motsatt uppfattning om denna problemlösnings karaktär.

#Prg: PRV, Patentbesvärsrätten och Regeringsrätten fann att:

#Ets: En metod för att bestämma i vilken ordning kunder skall betjänas... utgör i sig inte lösningen av något tekniskt problem och saknar i sig överhuvudtaget sådan teknisk karaktär som enligt grunderna för l § patentlagen erfordras för patenterbarhet.

#mEo: medan EPO bedömde samma problemlösning:

#lia: ...the %(sc:subject-matter of Claim 1) might be regarded as a technical system with cooperating technical components, in which the operation of the computing means solves a technical problem, and thus is patentable under Article 52(1) EPC...

#Elu: Enligt Lennarth Törnroth vid Patentbesvärsrätten skall EPO enligt de ursprungliga avtalen mellan EPO och EPCs medlemsstater reglera sin praxis i förhållande till nationella domstolar genom kall harmonisering. I det här domslutet tar EPO inte några sådana hänsyn och ändrar inte sin tolkning till förmån för svensk rättsuppfattning. Att EPO underlåtit att lyssna till nationella domslut är en bidragande orsak till att praxis i EPO:s besvärsinstanser avviker från den som tillämpas i medlemsstaternas domstolar.

#Fom: Förutom att EPO utvecklat en helt motsatt uppfattning än svenskt rättsväsende om vad som är ett tekniskt problem, används också i T1002/92 en tolkning av EPC 52.3 som tillämpad på alla patentlagens icke-uppfinningar gör dem patenterbara om de har fysisk form:

#Thn: The subject-matter of Claim 1 represents a hardware-like selfconsistent physical structure, which has a concrete technical construction... Such a technical realisation of the claimed system forming subject-matter of the claim is by no means a mental rule or method which is only %(q:as such) excluded from patentability by Articles 52(2) and (3) EPC.

#EtW: Enligt denna tolkning är således varje tekniskt förverkligande %(q:technical realisation) av en regel (%(q:mental rule)) inte en %(q:regel 'som sådan') (%(q:mental rule 'as such')) och därför inte undantaget från patentering.

#DgW: Detta strider mot svensk praxis som kräver att patentansökan skall avse en uppfinning, d v s att problemlösningen inte enbart skall utgöra t ex en regel. I det här fallet har PRV och Patentbesvärsrätten och Regeringsrätten gjort precis den bedömningen: det är en regel (eller med ett annat uttryck, en algoritm) som löser problemet att organisera en kö på ett visst sätt. En regel är inte en uppfinning.

#Srn: Skillnaden är subtil men fundamental. EPO menar att enbart det abstrakta objektet %(q:regel) är undantaget från patentering och ställer aldrig frågan om problemlösningens grundläggande karaktär. Eftersom EPO inte ställer frågan om ansökans problemlösning är en uppfinning leder denna tolkning till den absurda konsekvensen att alla tekniska förverkliganden av patentlagens exempel på saker som inte är uppfinningar blir patenterbara:

#esr: en upptäckt, vetenskaplig teori eller matematisk metod,

#enk: en konstnärlig skapelse,

#eka: en plan, regel eller metod för intellektuell verksamhet, för spel eller för affärsverksamhet eller ett datorprogram,

#egi: ett framläggande av information.

#Ies: I klartext: Ett tekniskt förverkligande av en matematisk metod är inte en matematisk metod %(q:as such) och därför inte undantaget från patentering.

#Eeo: Ett tekniskt förverkligande av en plan eller regel är inte en plan eller regel %(q:as such) och är därför inte undantaget från patentering.

#Ego: Ett tekniskt förverkligande av ett datorprogram är inte ett datorprogram %(q:as such) och därför inte undantaget från patentering.

#Eip: Ett tekniskt förverkligande av ett datorprogram är naturligtvis ett datorprogram som körs på en dator, alltså är ett datorprogram som körs på en dator inte undantaget från patentering.

#Egn: Ett teknisk förverkligande av ett affärsmetod är inte en affärsmetod %(q:as such) och därför inte undantaget från patentering. Om affärsmetoden implementeras i ett datorprogram är således även affärsmetoden tekniskt förverkligad genom att datorprogrammet körs på en dator och därmed inte undantaget från patentering.

#Efx: Ett tekniskt förverkligande av ett framläggande av information är inte ett framläggande av information %(q:as such) och därför inte undantagen från patentering.

#TaW: Tillämpningen av denna absurda tolkning av EPC 52.3 bidrar till att praxis i EPO:s besvärsinstanser avviker från den som tillämpas i medlemsstaternas domstolar (not ii).

#Igr: I sin egen tolkningstradition av EPC 52.3 har EPO 1998 i beslutet T1173/97 (denna patentansökan avslogs av PRV och i Patentbesvärsrätten, se not iii) introducerat en esoterisk skillnad mellan %(q:datorprogram som enbart utgörs av datorprogram) och %(q:datorprogram som inte enbart utgörs av datorprogram):

#Toe: The combination of the two provisions (Article 52(2) and (3) EPC) demonstrates that the legislators did not want to exclude from patentability all programs for computers. In other words the fact that only patent applications relating to programs for computers as such are excluded from patentability means that patentability may be allowed for patent applications relating to programs for computers where the latter are not considered to be programs for computers as such.

#EWp: En liknande tolkning av motsvarande text i patentlagen: %(q:Såsom uppfinning anses aldrig vad som utgör enbart... ett datorprogram) är inte möjlig. Lagstiftarens avsikt har varit att göra skillnad på om problemlösningen i patentansökan %(q:utgör enbart ett datorprogram) eller om problemlösningen består av mer än ett datorprogram, eller med lagens ord: %(q:inte utgör enbart ett datorprogram).

#Oar: Om problemlösningen i patentansökan är ett datorprogram är det helt enkelt ingen uppfinning. Om problemlösningen i patentansökan däremot använder sig av ett datorprogram kan det vara en uppfinning om övriga krav är uppfyllda. Uppfinningen kan t ex avse framställning av en kemikalie som styrs av ett datorprogram, eller en röntgenapparat vars strålningsintensitet beräknas av ett datorprogram. Detta synsätt ligger till grund för EPOs %(tp|patentgranskningsregler|Guidelines for examination) från 1978.

#Eos: EPOs tolkning av EPC 52.3 leder till slutsatser som inte är logiskt möjliga att överföra till svensk patentlag och strider mot lagstiftarens avsikt.

#DWo: Det är också värt att notera att hela EPC 52.3 kan strykas utan att överensstämmelsen mellan EPC 52 och patentlagens kapitel 1 § 1 påverkas.

#TlE: Tvärtom ökar symmetrin mellan lagtexterna om EPC 52.3 tas bort. Vid ratificeringen av EPC tillskrevs följdenligt heller inte 52.3 någon större betydelse.

#Naa: När en stor del av EPOs praxis vad gäller datorprogram är baserad på EPC 52.3 och denna lagtext saknas i svensk lag är denna asymmetri i sig också en bidragande orsak till att praxis i EPO:s besvärsinstanser avviker från den som tillämpas i medlemsstaternas domstolar.

#EaW: Eftersom beslut (som t ex T1002/92 och T1173/97) av en teknisk besvärskammare inom EPO inte kan överklagas vare sig på nationell eller internationell väg utan är definitivt bindande, skall besvärskammaren hänskjuta grundläggande tolkningskonflikter till den stora besvärskammaren, vars uppgift är %(tf:att besluta om hur EPC skall tolkas och tillämpas i oklara rättsfrågor). Den stora besvärskammaren inom EPO har aldrig behandlat frågan om patent på datorprogram.

#DsW: Det kaotiska rättsläget har alltså uppkommit för att:

#Eta: EPO underlåtit att lyssna till nationella domslut

#Een: EPO tillämpat en absurd tolkning av EPC 52.3

#EkW: EPOs tolkning av EPC 52.3 inte är kongruent med svensk patentlag och strider mot lagstiftarens avsikt.

#Eks: EPC 52.3 saknas i svensk patentlag

#Dts: De tekniska besvärskamrarna underlåtit att hänskjuta tolkningen av EPC 52.2 och 52.3 till den stora besvärskammaren inom EPO och därmed brutit mot EPC 112.

#Den: Det är mot denna bakgrund kommissionens ambition att %(q:harmonisera de nationella patentlagarna) måste ses.

#OoW: Orsaken till att %(q:det saknas rättslig förutsägbarhet) är inte att Europas patentlagar avviker från varandra (de harmoniserades vid införandet av EPC) utan att EPOs praxis utvecklats utan korrektiv (not iv, not v).

#DWW: Därför är det mycket olyckligt att kommissionen vill legalisera resultatet av denna utveckling och implementera den i nationell lagstiftning.

#Ecr: En betydligt enklare och billigare lösning är att stryka EPC 52.3.

#DpW: Direktivförslaget kodifierar, i specialfallet %(q:datorrelaterade uppfinningar), EPOs generella tolkning av EPC 52.3 att alla tekniska förverkliganden av patentlagens icke-uppfinningar är patenterbara.

#Wnr: Det %(q:tekniska förverkligandet) motsvaras av den %(q:tekniska karaktär) varje matematisk metod, spel, etc, eller metod för affärsverksamhet erhåller då den uttrycks i programkod (programmeras) och körs på en dator. Detta synsätt beskrivs i direktivtexten på sidan 7: %(q:Liknande överväganden har EPO:s besvärskammare tillämpat på andra företeelser som enligt artikel 52.2 är undantagna %(q:i sig), t.ex. %(q:metoder för affärsverksamhet), %(q:framläggande av information) eller %(q:konstnärliga skapelser). Uppfinningar med koppling till något av detta har således också bedömts vara patenterbara om de har teknisk karaktär.)

#Djw: Detta ses också tydligt i följande exempel på EPO-patent på icke-uppfinningar (not viii).

#Mil: Metoder för intellektuell verksamhet:

#Fnf: Framläggande av information:

#Art: Affärsmetoder:

#Fti: För att motivera denna radikala upplösning av patentsystemets gräns skriver kommissionen %(q:att alla program som körs i en dator definitionsmässigt är tekniska (eftersom datorn är en maskin) och därmed tar sig förbi den knepiga frågan om huruvida de är %(q:uppfinningar) eller inte.)

#Dgn: Den %(q:knepiga frågan) utgör kärnan inte bara i svensk patentpraxis (not vi).

#Iik: I det tyska domslutet Dispositionsprogramm beskrivs vad som krävs av en teknisk uppfinning:

#arW: a %(q:plan-conformant utilisation of controllable natural forces has been named as an essential precondition for asserting the technical character of an invention)

#sfv: samt en definition av begreppet:

#ano: a patentable invention is a teaching for plan-conformant action utilising controllable natural forces for achieving a causally overseeable result

#Dne: Direktivet använder alltså inte begreppet %(q:teknisk) i den traditionella juridiska betydelse som förutsattes när EPC konstruerades, nämligen %(q:teknisk) i betydelsen av empirisk naturvetenskaplig kunskap som används för att lösa ett praktiskt problem genom kontrollerad användning av naturlagarna; och en %(q:teknisk uppfinning) som en lära om användandet av naturlagar som kan utnyttjas industriellt.

#TrW: Tvärtom öppnar direktivförslaget för en tolkning som gör begreppet %(q:teknisk) användbart också för att beskriva affärsteknik, programmeringsteknik, undervisningsteknik, databehandlingsteknik, informationsteknik, etc, vilket tydligt framgår av de patent som EPO beviljat just på affärsmetoder, datorprogram och undervisning etc.

#DWp: Denna utveckling är enligt tysk rättsuppfattning förbjuden:

#AsW: Any attempt to attain the protection of mental achievements by means of extending the limits of the technical invention -- and thereby in fact giving up this concept -- leads onto a forbidden path.

#Msg: Med kunskap om EPOs uppfattning att alla tekniska förverkliganden av logiska eller matematiska problem är uppfinningar, visar en enkel omredigering av direktivets artiklar att t ex affärsmetoder mycket väl kan patenteras:

#OWt: Om verkan av en affärsmetod förutsätter användning av en dator och uppvisar nya kännetecken som verkställs av ett datorprogram är det en %(q:datorrelaterad uppfinning).

#Eki: En affärsmetod är patenterbar på villkor att den kan tillgodogöras industriellt, är ny och har uppfinningshöjd.

#EpW: En affärsmetod får tillerkännas uppfinningshöjd, endast på villkor att den utgör ett tekniskt bidrag.

#Ean: Ett tekniskt bidrag är ett för fackmannen icke uppenbart bidrag till teknikens ståndpunkt på ett teknikområde.

#Ead: En affärsmetod uppfattas som tillhörande ett teknikområde.

#Ddl: Direktivförslaget om patentering av %(q:datorrelaterade uppfinningar) innebär alltså i princip ett gränslöst patentsystem.

#Sa0: Svaren från medlemsländerna på kommissionens hearing var mycket trevande och återhållsamma, särskilt med avseende på förslaget att stryka datorprogram från EPC 52.2.c vilket EPO lagt fram på den föregående diplomatkonferensen i München hösten 2000. Eurolinux Alliance startade i samband med hearingen en webbsida via vilken ca 1400 svar förmedlades till kommissionen.

#Ekd: En enkel statistik visar på ett bedövande motstånd:

#pWp: För mjukvarupatent

#ans: Mot mjukvarupatent

#asW: Motståndet i olika grupper

#asW2: Privatpersoner

#asW3: Små och medelstora företag

#asW4: Stora företag

#asW5: Föreningar

#use: Användare

#sue: Studerande

#sue2: Akademiker

#sue3: Programvaruutvecklare

#sue4: Jurister

#sue5: Regeringsrepresentanter

#Div: Dessa ungefärliga tal överensstämmer med andra undersökningar som gjorts bla i USA. När man ser på argumentationens kvalitet framkommer ett tydligt mönster: generella trosbekännelser från patentadvokater om patentens välsignelse på den ena sidan, indignerad och saklig argumentation från berörda mjukvaruutvecklare på den andra.

#Fgg: Flera av de insända svaren innehåller grundläggande och genomgripande analyser som aldrig tagits upp till diskussion av kommissionen, t ex

#Sij: SSLUG lämnade sitt svar på danska. Trots att föreningen är relativt stor blev svaret inte översatt till engelska och inte heller, trots SSLUGs önskemål, offentliggjort på kommissionens hemsida.

#Fav: För en grundläggande genomgång av förfarandet se:

#Nit: Notera att uttrycket %(q:undantagen från patentering) (%(q:excluded from patentability)) inte återfinns i patentlagen. Det är naturligtvis ett tautologiskt påstående att säga att icke-uppfinningar är %(q:undantagna från patentering). Den svenska lagtexten är inte tautologisk men förutsätter att uppfinningar förstås som en delmängd av mängden problemlösningar, nämligen tekniska problemlösningar.

#FWt2: För att motivera sitt beslut refererar EPO till sin egen praxis i dubbel bemärkelse. Först explicit, och sedan implicit genom att hänvisa till ett svenskt domslut som i sin tur refererar till EPOs praxis som normativ.

#Thu: The Board notes that both the Swedish Patent Office and the Court of Patent Appeal regarded the subject-matter of the Swedish application as constituting a solution to a problem which was not of a technical nature, and therefore unpatentable. Although the Supreme Administrative Court confirmed the rejection of the application, its judgment included a dissenting opinion.

#Sba: Since the issue of these decisions in Sweden between 1983 and 1987, the case law of the Boards of Appeal concerning the interpretation of Article 52 EPC has been developed, and has been matched by corresponding development of the law in Sweden, as shown in the judgment of the Supreme Administrative Court issued on 13 June 1990 in a case concerning an application by N. V. Philips Gloeilampenfabrieken. This judgment indicates that earlier Swedish case law, at the time of the above-identified decisions on the corresponding Swedish application, had deviated from the EPO case law.

#Har: Having regard to what is now established case law within the Boards of Appeal, and for the reasons set out in detail above, the Board does not agree with the reasoning which led to the rejection of the corresponding Swedish application.[7]

#Eea: EPO nämner inte att applikationen Philips sökte patent på fick avslag både hos PRV och i Patentbesvärsrätten. Inte heller att Regeringsrättens enda motiv för sitt bifall är EPOs egen praxis och EPOs Guidelines per 1985:

#Nen: N.V. Philips' Gloeilampenfabrieken (%(q:Philips)) ingav till patentverket en patentansökan om sätt och anordning för bestämning av tonhöjden i mänskligt tal. Patentverket anförde i föreläggande bland annat att den av [sökande] angivna lösningen för att erhålla de nämnda resultatet utgörs av ett antal matematiska algoritmer vilka realiseras med hjälp av konventionell hårdvara och kan utföras av i vanlig dator lagrade subrutiner. Patentverket anförde vidare att ansökan saknade teknisk karaktär och ej var patentbart enligt 1 § 2 st patentlagen.  Patentverket avslog ansökan.

#Plo: Philips anförde besvär över beslutet. I besvären vidhöll sökande patentansökan med ny patentkrav. Patentbesvärsrätten lämnade besvären utan bifall. Patentbesvärsrätten anförde att det de med patentkraven åsyftade förfarandena väsentligen bestod av organisatoriska och matematiska åtgärder som kan genomföras medelst programmering av en konventionell dator och som inte heller annars krävde användning av något nytt tekniskt hjälpmedel.

#Ppf: Philips anförde besvär över patentbesvärsrättens beslut. Regeringsrätten hänvisar i sina skäl till förarbetena till patentlagen som anger att det med hänsyn till den nära överensstämmande utformningen av 1 § 1 st Patentlagen och motsvarande bestämmelse i den europeiska patentkonventionen finns anledning att tillmäta utvecklingen av praxis vid det europeiska patentverket (EPO) stor betydelse.9 Regeringsrätten anför vidare att för ett sådant hänsynstagande talar också att patent meddelade av EPO har samma rättsverkan i Sverige som patent meddelande av det svenska patentverket. Regeringsrätten redogör för aktuell praxis från EPO samt noterar särskilt att det i den 1985 utgivna reviderade riktlinjer för prövning (%(q:Guidelines for the Examination in the EPO)) tydligt klargörs att bedömningen skall avse uppfinningen som helhet och vad som är det reella bidraget till teknikens ståndpunkt. Om det sökta patentet ger ett tekniskt bidrag till känd teknik får inte patenterbarhet förnekas enbart av det skälet att ett datorprogram är inbegripet i uppfinningens förverkligande.

#cdP: %(q:Något om patenterbarhet av datorprogram i svensk rätt) av Patrik Wallström och Mikael Pawlo

#DOt: Det ligger tyvärr också i EPOs ekonomiska intresse att utöka patentsystemets omfång:

#VWn: Verksamheten vid EPO finansieras fullt ut med de avgifter som tas ut i samband med behandlingen av patentansökningarna. EPO är således inte i någon mån beroende av ekonomiska bidrag från medlemsstaterna[19]

#VPt: Vidare har det framförts klagomål både från industrin och patenträttsråd om att kvalitén på EPO-patent är låg. Det finns förutom ett antal officiella %(ra:rapporter) även uppgifter om att en intern utredning kommit fram till att vart femte patent egentligen inte borde beviljats:

#DiP: Därtill har EPO som institution en immunitet som gör genomlysning av EPOs verksamhet ytterst ofullständig:

#Ejs: överlåtande av juridiskt bindande beslutanderätt till en internationell inrättning som EPO, är underkastad villkoren i regeringsformens kapitel 10 § 5. Där ställs krav om överlåtelsens paritet med grundläggande rättsskydd och överensstämmelse med europakonventionen.

#Mah: Mellanstatliga frågor är oerhört komplexa, men det finns anledning att fundera över huruvida regleringen av EPOs verksamhet och myndighetsutövande överensstämmer med grundläggande demokratiska principer.

#Rte: Regeringsformen uttrycker explicit att EPO inte har rätt att fatta beslut som innebär en %(q:begränsning av någon av de fri- och rättigheter som avses i 2 kap) i regeringsformen.

#Vgi: Vad gäller upphovsrättsligt skyddad intellektuell egendom, som datorprogram, kan man ställa frågor om inte patentanspråk på samma datorprogram begränsar:

#Rsk: Regeringsformen kapitel 2

#med: medborgares yttrandefriheten

#mea: medborgares meddelandefrihet

#yio: yttrandefrihet och informationsfrihet i vetenskapliga och kulturella angelägenheter

#seW: skydd av medborgares egendom mot expropriation eller annat förfogande

#mWg: medborgares rätt att driva näring eller utöva yrke

#mgn: medborgares grundläggande friheter garanterade genom europakonventionen och av den anledningen är ogiltiga.

#Ogt: Om en sådan patenttvist skulle gå till Europadomstolen finns det goda skäl att tro att patentets begränsande effekt på upphovsrättsinnehavarens grundläggande fri- och rättigheter skulle räcka för att förklara denna kontradiktoriska egendomsrätt för olaglig och ogiltigförklara EPOs tolkning av EPC.

#Esa: Exakt motsatt ståndpunkt är kanske klarast uttryckt i det tyska domslutet %(dp:Dispositionsprogramm):

#Wht: We must therefore insist that a pure rule of organisation and calculation, whose sole relation to the realm of technology consists in its usability for the normal operation of a known computer, does not deserve patent protection.

#ThW: The system of German industrial property and copyright protection is however founded upon the basic assumption that for specific kinds of mental achievements different specially adapted protection regulations are in force, and that overlappings between these different protection rights need to be excluded as far as possible. The patent system is also not conceived as a reception basin, in which all otherwise not   legally privileged mental achievements should find protection. It was on the contrary conceived as a special law for the protection of a delimited sphere of mental achievements, namely the technical ones, and it has always been understood and applied in this way.

#StW: Storleken av detta potentiella hot avspeglas i kostnaden för att teckna en företagsförsäkring mot ofrivilligt patentintrång. Premien för närvarande startar på ca 300.000 Skr per år.

#Fem: För fler exempel se:

#Aan: Antag att företag med 1-19 anställda, totalt 6246 stycken, inte har egen juridisk kompetens utan måste köpa juridisk rådgivning. Enmansföretag och större företag räknas inte med i detta överslag.

#Aae: Antag att varje företag per år mottar en uppmaning från en innehavare av ett patent på en %(q:programvarurelaterad uppfinning) att betala en licensavgift eller att lägga ner programvaruutvecklingen.

#Ary: Antag att varje företag anlitar en patentjurist för att analysera validiteten i uppmaningen.

#EWp2: En första bedömning av validiteten och en ytterst enkel åtgärd baserad på denna bedömning kostar 10.000-20.000 Skr i konsultarvode. överslaget visar att endast mottagandet, och en professionell hanteringen, av ett enda patentanspråk per företag kostar den mest kreativa och innovativa delen av mjukvarubranschen som lägst 62.460.000 Skr per år.

#Frt: Företag

#aWv: antal anställda

#Ttl: Totalt

#Dku: Datakonsulter

#Pvd: Programvaruproducenter

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: sslug0205 ;
# txtlang: sv ;
# multlin: t ;
# End: ;

