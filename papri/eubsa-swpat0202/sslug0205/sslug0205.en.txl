<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: CEC & BSA trying to impose unlimited patentability on Sweden

#descr: In a statement submitted to the Swedish Ministry of Justice on behalf of SSLUG, a group of 6100 programmers and users of free software in the area around Copenhagen and Malmö, Erik Josefsson shows how an influential group at the European Commission and the European Patent Office has eroded the standards of patentability and is trying to impose a regime of patentability on all achievements of the human mind that can help to solve some practical problem.  This influential group has also, by overstretching the competence of the EPO's Technical Boards of Appeal, illegally overruled the Swedish courts and damaged the Swedish constitutional order.  Even in their most recent decisions in the mid-nineties, the Swedish courts did not agree with the EPO's illegal practice, but now the European Commission is set to force this practice on Sweden by means of %(q:european harmonisation).  It was the duty of the EPO to abide by a role of %(q:cold harmonisation) in the first place: act as a conservative follower and summarizer of national caselaw rather than as an innovative trendsetter pursuing its own agenda.  Josefsson cites ample examples of patents granted by the EPO and rejected by Swedish courts.

#Sth: Viewpoints on the directive proposal on the patentability of %(q:computer-implemented inventions).

#SAT: ABSTRACT

#SPK: VIEWPOINTS

#CET: CASE STUDY

#HRT: DISCOURAGE OR ENCOURAGE?

#OEO: UNANSWERED QUESTIONS

#VxA: WHAT IS A %(q:COMPUTER-IMPLEMENTED INVENTION)?

#UTO: ORIGIN OF EPO PRACTISE

#KIP: CODIFYING OF EPO PRACTISE

#DoW: The directive proposal legalizes 30.000 patents on computer programs and business methods that are not valid under the European Patent Convention (EPC). The majority is owned by non-European companies.

#Dlh: The directive proposal transfers intellectual property rights of a substantial value from copyright owners to patent holders in violation of the European Convention on Human Rights.

#Dml: The directive proposal reduces legal security by implementing an intransparant conflict between industrial and intellectual property protection.

#Drr: The directive proposal legalizes patents on all computer programs that run on a computer.

#Dgs: The directive proposal legalizes patents on all business methods that are implemented in a computer program that run on a computer.

#EiE: According to Swedish patent law, computer programs and business methods are not inventions and can not be patented as such. Therefore, the directive proposal requires extensive change in Swedish law.

#PEW: Software Patenting inhibits the development of free and open standards, and impedes the evolution of a free and open network society.

#Ato: Of all research studies that has been done, no one concludes that patents on computer programs lead to increased productivity, innovation and knowledge diffusion.

#Iro: Introduction of patents in the software industry inhibits development, reduces competition and increases monopolization.

#IeW: The legalization of software patents creates an adverse environment in particular for SMEs that do not have resources to build their own juridical competence to handle infringement, licensing and patent applications or build a patent portfolio.

#SWg: SSLUG believes that strong copyright protection for computer programs provides the basic condition for a dynamical and creative software development.

#Srl: SSLUG emphasizes that copyright protection for computer programs is a prerequisite for the continuous rapid development of free software and a free and open Internet.

#Sk5: SSLUG %(cp:suggests) that the legal situation regarding protection of computer programs can be made transparent by rewriting the directive proposal, deleting EPC 52.3 and drafting new guidelines for patent examination.

#Vi2: We are very happy to be invited to the discussion about the directive proposal on patentability of %(q:computer-implemented inventions). SSLUG has since the start in 1995 always advocated open standards, open source and the freedom to choose, and has actively participated in the debate on patents on computer programs since the Commission hearing in the fall 2000. The question is vividly discussed by SSLUG members on the mailing list sslug-itpolitik@sslug.dk and has been an item in many annual and board meeting %(ms:agendas).

#TjW: Unfortunately, we don't think the public debate has been honest, open and democratic. Representatives from strong economic groups, and from the patent system, has been given preferential right of interpretation before users, programmers and other software professionals. One example to mention is the directive proposal draft written by Francisco Mingorance, %(bs:BSA), another is public statements from representatives from the Danish patent office saying there are scientific studies proving Open Source Software (OSS) is not harmed by %(mv:software patents) (which is not true).

#Dif: The referential right of interpretation of these representatives is reflected in the directive proposal. As an example of suppression of deviant opinions, the Eurolinux %(mv:petition), with 120.000 signatures one of the largest European on-line petitions ever, is not mentioned in the directive text. Another example is the summary examination of 1400 papers, administered through %(ew:Eurolinux), and sent to the Commission hearing in 2000 as being put forward by an economical minority (note 1). The directive proposal obviously suffers from lack of holistic analysis and references to scientific research on effects upon the European information society.

#Edb: A SSLUG member, Tord Jansson, started to write a computer program in the late nineties that converted one file format to another. From official and published standards of the file formats and some ISO %(rk:referential code) he tried to find programmable problem solutions that would make the conversion as fast and efficient as possible. The first version of the computer program BladeEnc was finished in 1997-1998. Tord chose a modern open license to get as many users (and bugfixers) as possible. In a short time BladeEnc was used by millions of people all over the world.

#Iee: In September 1998 Tord received a letter from a company claiming it has many patents granted by the European Patent Office (EPO) and valid in Sweden, and that Tord's computer program infringed on these %(on:patents).

#DdW: The directive proposal legitimizes the claims of the company. The EPO-patents covers technical realisations of the %(q:underlying ideas and principles) of the patent. Any singular implementation of the conversion problem in a computer program that runs on any general purpose computer is such a technical realisation and is therefore infringing on the EPO-patents.

#Sen: Swedish legislation upholds the rights of Tord Jansson to write, use and distribute his intellectual property in any way he wishes, and guarantees the patent claims to be invalid because his computer program is not an invention.

#Paa: After juridical consultation Tord Jansson has decided to stop distributing BladeEnc due to the uncertain legal situation.

#Dsl: It becomes known that many program developers that made similar conversion programs received similar letters. Almost all of them abandon their projects.

#Ngm: Present Swedish legislation protects every single implementation against copying and plagiarism and leaves scope for innovation and competition. The directive proposal introduces a regime which gives the one that first patents a %(q:underlying idea) complete market domination.

#Kri: The consequences of %(q:computer-implemented inventions) being realised on any general purpose computer are incalculable. It will not only affect innovative program developers like Tord Jansson, but also any company that uses computer programs whose %(q:underlying ideas and principles) can be patented (note 7).

#FWt: Scientific research concludes that branches characterized by sequential innovation are %(ri:impeded) by patents. American experience shows that innovative software SMEs in particular has to penetrate a patent %(q:thicket) before they reach the market and starts making %(st:money). Swedish IT-companies are worried increased patent administration costs will not be balanced by increased %(sw:revenue).

#KsE: Knowledge diffusion is a prerequisite for development and innovation. The contribution of the patent system to knowledge diffusion in the software area is marginal. Partly because programmers do not read patent applications (they read source code and manuals), partly because a lot of software today is developed on-line on the %(in:Internet). Today, the community does not have to offer patent protection to find out about this knowledge. There is no reason to disturb that accounting balance.

#Dkc: The most innovative part of the Swedish software industry is SMEs with 1-20 employees. A simple calculation shows that the reception alone of a patent threat per year and company costs that part of the industry more than 60 million Swedish crowns (note 9). The risk for a decrease in investments in that segment is obvious.

#Hax: How are the following areas influenced by allowing patents on %(q:computer-implemented inventions)?

#icu: innovation and development in the software area in general

#ikc: innovation and development of Open Source and Free Software

#uWW: evolution of and access to Internet

#udW: publishers and distributors of software

#pve: software developers

#kca: consumers and users of software

#fea: research and development in academia

#SWm: SSLUG does not consider these questions properly answered by the Commission and therefore recommends a withdrawal of the directive proposal.

#DFn: Currently, computer programs are strongly protected by copyright. Computer programs are complex logical works made up by tens of thousands of singular logical and mathematical problem solutions. Many of these problem solutions are many centuries old, others are quite new. Copyright protects, in an adequate way, the unique composition of old and new problem solutions in a computer program in the same way as it protects the unicity of literary and musical works. Computer programs are therefore, together with literature and music, properly classified as %(q:Intellectual Property).

#Dns: Computer programs are currently not patentable. They are together with mathematical methods excluded from patentability for the reason they are not regarded as inventions. An invention is a solution to a technical problem. Technical problems are basically problems of controlling physical objects or the natural forces acting upon these objects (see the preparatory works to patent law). If such a technical problem solution is industrially applicable it can under some additional conditions be protected by a patent. Patents are therefore together with e.g. design patterns classified as %(q:Industrial Property).

#Vee: What is then a %(q:computer-implemented invention)? Is it a solution to a technical problem or a solution to a logical problem? Is it an invention or not? Should a %(q:computer-implemented invention) be classified as %(q:Intellectual Property) or %(q:Industrial Property)?

#Doa: The definition from the directive proposal (article 2, page 20) says:

#Etg: A %(q:computer-implemented invention) is an invention

#vun: which performance involves the use of a computer

#sca: that has features which are realised by means of a computer program.

#Dtr: The definition can not be deduced from Swedish practise but has to be understood as a codification of the practise that has evolved at the EPO.

#FvW: To be able to evaluate the ambition to make %(q:the conditions of patentability more transparent) (page 3) it is necessary to analyze what caused the present lack of %(q:legal certainty) (page 2) for legal protection for computer programs, that is, what caused the practise of EPO Board of Appeals to diverge from the practise of national courts.

#Gro: Generally, when applying for a patent in Sweden, the question whether the problem solution described in the patent application belongs to the logical or the technical field has been guiding and conclusive for interpretation and application of patent law. In the %(sd:following example) the appraisal concerns an application that refers to a solution how to organize one queue to different service desks. In a verdict from 1994 (T1002/92) it becomes obvious that the EPO and the Swedish patent institutions has diametrically opposite opinions about the character of this problem solution.

#Prg: The Swedish Patent Office, the Swedish Court of Patent Appeals and the Swedish Supreme Administrative Court found that:

#Ets: A method for deciding in which order customers shall be served... does not constitute a solution to a technical problem and is as such void of any technical character that according to the fundamentals of 1 § patent law is required for patentability.

#mEo: while the EPO judged the same problem solution:

#lia: ...the %(sc:subject-matter of Claim 1) might be regarded as a technical system with cooperating technical components, in which the operation of the computing means solves a technical problem, and thus is patentable under Article 52(1) EPC...

#Elu: According to Lennarth Törnroth at the Swedish Court of Patent Appeals, the EPO is obliged, according to the original agreements between the EPO and the member states of EPC, to adjust its practise to national court verdicts through cold harmonization. In this case the EPO does not take the Swedish verdicts into consideration and does not change its own interpretation to make way for the Swedish conception of justice. The EPO has neglected to listen to national verdicts. This contributes to the evolving divergence between the practise at the EPO Board of Appeals and the practise in member country courts.

#Fom: In addition to the evolution of a opposite opinions between the EPO and the Swedish courts on what constitutes a technical problem, T1002/92 also shows that the EPO has used an interpretation of EPC 52.3 which makes all the non-inventions of the patent law patentable if they have a physical structure:

#Thn: The subject-matter of Claim 1 represents a hardware-like selfconsistent physical structure, which has a concrete technical construction... Such a technical realisation of the claimed system forming subject-matter of the claim is by no means a mental rule or method which is only %(q:as such) excluded from patentability by Articles 52(2) and (3) EPC.

#EtW: According to this interpretation, every technical realisation of a mental rule is not a mental rule as such and therefore not excluded from patentability.

#DgW: This contradicts Swedish practise that requires the patent application to describe an invention, i.e., that the problem solution shall not only constitute e.g. a mental rule. In this case, the Swedish Patent Office, the Swedish Court of Patent Appeals and the Swedish Supreme Administrative Court judgment is: it is a mental rule (or with another expression, an algorithm) that solves the problem to organize a queue in a certain way. A mental rule is not an invention.

#Srn: The difference is subtle but fundamental. The EPO conjecture that it is only the abstract object %(q:mental rule) that is excluded from patentability does not raise the question of the fundamental character of the problem solution. Since the EPO does not ask whether the problem solution described in the application is an invention, the absurd consequence is that all technical realisations of the patent law examples of non-inventions becomes patentable:

#esr: discoveries, scientific theories and mathematical methods

#enk: aesthetic creations

#eka: schemes, rules and methods for performing mental acts, playing games or doing business, and programs for computers

#egi: presentations of information

#Ies: In plain language: A technical realisation of a mathematical method is not a mathematical method as such and is not excluded from patentability.

#Eeo: A technical realisation of a scheme or rule is not a scheme or rule as such and not excluded from patentability.

#Ego: A technical realisation of a computer program is not a computer program as as such and not excluded from patentability.

#Eip: A technical realisation of a computer program is a computer program that runs on a computer. A computer program that runs on a computer is not excluded from patentability.

#Egn: A technical realisation of a business method is not a business method as such and not excluded from patentability. If the business method is implemented in a computer program, the business method is technically realised if the computer program runs on a computer, and not excluded from patentability.

#Efx: A technical realisation of presentation of information is not a presentation of information as such and not excluded from patentability.

#TaW: Application of this absurd interpretation of EPC 52.3 adds to the divergence between practise in the EPO and the member countries (note 2).

#Igr: The EPO has in the decision T1173/97 (this application was rejected by the Swedish Patent Office and the Swedish Court of Patent Appeals, see note 3), in accordance with their own interpretation tradition of EPC 52.3, introduced an esoteric difference between %(q:computer programs as such) and %(q:computer programs not as such):

#Toe: The combination of the two provisions (Article 52(2) and (3) EPC) demonstrates that the legislators did not want to exclude from patentability all programs for computers. In other words the fact that only patent applications relating to programs for computers as such are excluded from patentability means that patentability may be allowed for patent applications relating to programs for computers where the latter are not considered to be programs for computers as such.

#EWp: A similar interpretation of the Swedish patent law: %(q:As an invention is never regarded what alone constitutes... a computer program) is not possible. The intention of the legislator is to separate problem solutions that %(q:alone constitutes a computer program) from problem solutions that constitutes more than a computer program. Again with the words of the law: %(q:what alone constitutes... a computer program) is not regarded as an invention.

#Oar: If the patent application problem solution is a computer program it is simply not an invention. If the problem solution uses a computer program and the other requirements are fulfilled it may be an invention. The invention can e.g. be a computer controlled production of a chemical substance, or a x-ray apparatus that uses a computer program to calculate radiation intensities. This approach is found in %(tp:Guidelines for examination) from 1978.

#Eos: The EPO interpretation of EPC 52.3 leads to conclusions that are not possible to logically transfer to Swedish patent law and violates the intention of the legislator.

#DWo: It is also noteworthy that the complete article of EPC 52.3 can be deleted without reducing congruence between EPC 52 and Swedish patent law 1 §.

#TlE: On the contrary, symmetry increases if EPC 52.3 is deleted. Accordingly, EPC 52.3 was not considered important when EPC was ratified.

#Naa: When a major part of the EPO practise is based on EPC 52.3 and this paragraph is missing in Swedish law, this asymmetry is in itself a contribution to the divergent practices of the EPO Board of Appeals and national patent courts.

#EaW: While decisions like T1002/92 and T1173/97 of an EPO Board of Appeal are final and binding and can not be appealed nationally or internationally, the Boards of Appeal are obliged to put forward important questions of interpretation to the Enlarged Board of Appeal, whose main task is to %(tf:decide how EPC shall be interpreted in ambiguous legal questions). The Enlarged Board of Appeal has never considered the question of computer program patents.

#DsW: The chaotic legal situation has arised due to:

#Eta: The EPO has neglected to consider national verdicts

#Een: The EPO has applied an absurd interpretation of EPC 52.3

#EkW: The EPO interpretation of EPC 52.3 is not congruent with Swedish patent law and violates the intention of the legislator

#Eks: EPC 52.3 is missing in Swedish patent law

#Dts: The Technical Board of Appeal has failed to forward the interpretation of EPC 52.2 and 52.3 to the Enlarged Board of Appeal in violation of EPC 112.

#Den: The ambition of the Commission to %(q:harmonize the national patent laws) should be viewed in the light of the causes above.

#OoW: The main reason for lack of %(q:legal certainty) is not that European patent laws are particularly divergent (they were harmonized when EPC was drafted), but rather that the EPO practise has evolved without correctives (note 4, note 5).

#DWW: It is very unfortunate that the Commission wants to legalize the result of this evolution and implement it into national legislation.

#Ecr: A cheaper solution would be to delete EPC 52.3.

#DpW: The directive proposal on the patentability of %(q:computer-implemented inventions) codifies in the special case of computer programs, the general rule that all technical realisations of the non-inventions mentioned in EPC 52.2 are patentable.

#Wnr: The %(q:technical realisation) is equivalent to the %(q:technical character) that every mathematical method, game, etc, or business method acquires when implemented in a computer program and run on a computer. This outlook is described in the directive text on page 7: Similar considerations have been applied by the EPO Boards of Appeal to the other items of Art. 52(2) which are excluded  %(q:as such), for instance, to  %(q:methods for doing business), %(q:presentation of information), or %(q:aesthetic creations). This means that inventions relating to one of these items have equally been held to be patentable when they have a technical character.

#Djw: This is also obviously seen by the following examples of EPO patents on non-inventions (note 8).

#Mil: Methods for performing mental acts:

#Fnf: Presentations of information:

#Art: Business methods:

#Fti: To justify this radical deconstruction of the limit of the patent system the Commission writes: all programs when run in a computer are by definition technical (because a computer is a machine), and so are able pass this basic hurdle of being an %(q:invention).

#Dgn: This %(q:basic hurdle) is the core of not only the Swedish patent practise (note 6).

#Iik: The German verdict Dispositionsprogramm is describing what is the prerequisite of a technical invention:

#arW: a plan-conformant utilisation of controllable natural forces has been named as an essential precondition for asserting the technical character of an invention

#sfv: and also a definition of the concept:

#ano: a patentable invention is a teaching for plan-conformant action utilising controllable natural forces for achieving a causally overseeable result

#Dne: The conclusion is that the directive proposal does not use the word %(q:technical) in the traditional juridical sense that was presupposed when EPC was drafted, namely %(q:technical) in the meaning of empirical natural science knowledge that is used to solve a practical problem of controlling natural forces; and a %(q:technical invention) as a teaching about industrial usage of natural forces.

#TrW: On the contrary, the directive proposal makes possible other interpretations of the concept %(q:technique) usable for describing business techniques, programming techniques, educational techniques, computing techniques, information techniques, etc, which is obvious from the patents the EPO has granted on business methods, computer programs education etc.

#DWp: This evolution is forbidden according to German conception of justice:

#AsW: Any attempt to attain the protection of mental achievements by means of extending the limits of the technical invention -- and thereby in fact giving up this concept -- leads onto a forbidden path.

#Msg: With the knowledge of the EPO conception that all technical realisations of logical or mathematical problem solutions are inventions, it is easy to deduce that business methods are patentable from a simple substitution of words in the articles of the directive proposal:

#OWt: If the performance of the business method involves the use of a computer and has novel features which are realised by means of a computer program, it is a %(q:computer-implemented invention).

#Eki: A business method is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#EpW: A business method must make a technical contribution to involve an inventive step.

#Ean: A technical contribution is a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#Ead: A computer-implemented business method is considered to belong to field of technology.

#Ddl: The directive proposal on the patentability of %(q:computer-implemented inventions) leads to unlimited patentability.

#Sa0: The answers from the member states to the Commission hearing were rambling and restrained, particularly regarding the proposition to delete computer programs from EPC 52.2.c, which was put forward by the EPO at the diplomatic conference in Munich the fall 2000. Eurolinux Alliance initiated an administrative web page through which approx. 1400 answers were forwarded to the Commission.

#Ekd: A simple statistical evaluation reveals a stunning resistance:

#pWp: pro swpat

#ans: against swpat

#asW: against by groups

#asW2: private persons

#asW3: SME

#asW4: large comany

#asW5: Föreningar

#use: users

#sue: students

#sue2: Academics

#sue3: software developpers

#sue4: lawyers

#sue5: governmental

#Div: These numbers resemble similar results from surveys made in the USA. When the quality of of the argumentation is considered, a pattern appears: general confessions to the benevolence of the patent systems from patent lawyers, concerned and impartial argumentation from software developers.

#Fgg: Many submitted answers with thorough and detailed presentations of the problems have never been analyzed and discussed by the Commission, e.g.

#Sij: SSLUG submitted an answer in Danish, but it was never published nor translated by the Commission.

#Fav: The complete story can be found at:

#Nit: Please observe that the notion %(q:excluded from patentability) does not exist in Swedish patent law. To say that a non-invention is %(q:excluded from patentability) is a tautological statement. The Swedish patent law is not tautological but presupposes that inventions are regarded as a subset of all problem solutions, namely technical problem solutions.

#FWt2: To justify the verdict, the EPO refers to its own practise in two ways. First explicitly, and then implicitly by refering to a Swedish verdict that in turn refers to EPO practise as normative.

#Thu: The Board notes that both the Swedish Patent Office and the Court of Patent Appeal regarded the subject-matter of the Swedish application as constituting a solution to a problem which was not of a technical nature, and therefore unpatentable. Although the Supreme Administrative Court confirmed the rejection of the application, its judgment included a dissenting opinion.

#Sba: Since the issue of these decisions in Sweden between 1983 and 1987, the case law of the Boards of Appeal concerning the interpretation of Article 52 EPC has been developed, and has been matched by corresponding development of the law in Sweden, as shown in the judgment of the Supreme Administrative Court issued on 13 June 1990 in a case concerning an application by N. V. Philips Gloeilampenfabrieken. This judgment indicates that earlier Swedish case law, at the time of the above-identified decisions on the corresponding Swedish application, had deviated from the EPO case law.

#Har: Having regard to what is now established case law within the Boards of Appeal, and for the reasons set out in detail above, the Board does not agree with the reasoning which led to the rejection of the corresponding Swedish application (T1002/92, page 17).

#Eea: The EPO does not mention that the application Philips tried to patent was rejected by the Swedish Patent Office and the Swedish Court of Patent Appeals. Additionally, it is not mentioned that the only motivation the Swedish Supreme Administrative Court had for granting the patent, was EPO practise itself and the EPO 1985 Guidelines:

#Nen: N.V. Philips' Gloeilampenfabrieken (%(q:Philips)) submitted a patent application to the Swedish Patent Office on method and process to determine the pitch of human speech. The Swedish Patent Office concluded in procedures that among other things, the problem solution to achieve the mentioned result, constitutes a number of mathematical algorithms, which are realised with the help of conventional hardware, and can be executed by stored subroutines on a normal computer. The Swedish Patent Office further stated that the application was void of technical character and was not patentable according to 1 § section 2 patent law. The Swedish Patent Office rejected the application.

#Plo: Philips appealed against the decision. In the appeals, further patent claims was added by the patentee. The Swedish Court of Patent Appeals rejected the application. The Swedish Court of Patent Appeals brought forward that the claims of the patent refered to procedures that essentially constituted organizational and mathematical measures that can be carried out by programming of a conventional computer, and that they did not in any other way require use of any new technical means.

#Ppf: Philips appealed against the decision of the Swedish Court of Patent Appeal. The Swedish Supreme Administrative Court refers in their reasoning, to the preparatory works of the patent law, that says, considering the close resemblance between 1 § section 1 patent law and corresponding provision in the EPC, there are reasons to assign the evolution of EPO practise great importance. The Swedish Supreme Administrative Court further adduce that such consideration is supported by the fact that patents granted by the EPO has the same legal status in Sweden as a patent granted by the Swedish Patent Office. The Swedish Supreme Administrative Court describes the present practise at the EPO and observes that the Examination Guidelines from 1985 clearly explain that the assessment of a patent application shall concern the invention as a whole and where the real contribution to the state of the art lies. If the patent gives a technical contribution to known technique, patentability can not be denied for the reason that a computer program is involved in the realisation of the invention.

#cdP: %(q:Något om patenterbarhet av datorprogram i svensk rätt) av Patrik Wallström och Mikael Pawlo

#DOt: Unfortunately, the EPO has an economic interest in increasing the scope of the patent system:

#VWn: The EPO is financed by patentee patent fees and is not dependent on subsidies from member countries.

#VPt: The industry and members of the Swedish Court of Patent Appeal has complained about EPO-patent quality. Apart from a number of official %(ra:reports), there is also an internal inquiry coming to the conclusion that one fifth of all EPO patents should not have been granted:

#DiP: Additionally, the EPO has an institutional immunity that makes transparency of EPO activities incomplete:

#Ejs: Transfer of juridically binding decision making power, to a international institution like the EPO, must be subordinated the conditions described in the Swedish Constitution in Chapter 10 § 5. The Constitution requires the transfer to be compatible and in accordance with basic protection of the law and the European Convention.

#Mah: International questions are very complex, but there are reasons to ask if the regulations controlling the activity and authority of the EPO are compatible with general democratic principles.

#Rte: The Swedish Constitution Chapter 10 § 5 explicitly says that the EPO does not have the right to make decisions that leads to a %(q:limitation of any of the freedoms and rights described in Chapter 2) of the Constitution.

#Vgi: When it comes to copyright protection of intellectual property like computer programs, questions can be asked if patent claims on the very same computer programs does not limit:

#Rsk: The Swedish Constitution Chapter 2

#med: freedom of expression

#mea: freedom of information

#yio: freedom of speech and information in scientific and cultural affairs

#seW: protection of property against expropriation or other obstruction of disposal

#mWg: freedom of trade and profession practice

#mgn: basic freedom rights guaranteed through the European Convention

#Ogt: Such patent claims could on those grounds be invalid. If a software patent controversy would reach the European Court, there are good reasons to believe that the restricting effects of the patent on the the basic freedoms of the copyright holder, would be enough to deem such contradictory property rights illegal and and thereby refute the EPO interpretation of the EPC.

#Esa: Exactly the opposite view is maybe most clearly expressed in the German verdict %(dp:Dispositionsprogramm):

#Wht: We must therefore insist that a pure rule of organization and calculation, whose sole relation to the realm of technology consists in its usability for the normal operation of a known computer, does not deserve patent protection.

#ThW: The system of German industrial property and copyright protection is however founded upon the basic assumption that for specific kinds of mental achievements different specially adapted protection regulations are in force, and that overlappings between these different protection rights need to be excluded as far as possible. The patent system is also not conceived as a reception basin, in which all otherwise not   legally privileged mental achievements should find protection. It was on the contrary conceived as a special law for the protection of a delimited sphere of mental achievements, namely the technical ones, and it has always been understood and applied in this way.

#StW: The size of this potential threat is reflected in the cost of a company insurance against inadvertent patent infringement. The present premium starts at 300.000 Swedish crowns per year.

#Fem: More examples at:

#Aan: Suppose companies with 1-19 employees, in all 6246, does not have their own judicial competence and must buy judicial expertise. Smaller and larger companies are non included in this estimation.

#Aae: Suppose every company each year receives a patent infringement claim from a patent holder of a %(q:computer-implemented invention) and is requested to either pay a license fee or to shut down software development.

#Ary: Suppose each company hires one patent lawyer to evaluate the validity of the request.

#EWp2: A first evaluation of the validity and a most simple measure based on this judgment costs 10.000-20.000 Swedish crown in consultant fees. This estimation shows that the reception alone (and professional handling) of just one patent infringement per company costs the most creative and innovative part of the software industry at least 62.460.000 Swedish crowns per year.

#Frt: Companies

#aWv: employees

#Ttl: Total

#Dku: Computer Consultants

#Pvd: Software producers

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: sslug0205 ;
# txtlang: en ;
# multlin: t ;
# End: ;

