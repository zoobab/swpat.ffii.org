\begin{subdocument}{cducsu020514}{Bundestag 2002-05-14: Kleine Anfrage der CDU/CSU zu Swpat}{http://swpat.ffii.org/papri/eubsa-swpat0202/cducsu020514/index.es.html}{Workgroup\\swpatag@ffii.org}{Nachdem die Bundesregierung lange Zeit geschwiegen und auch die \"{o}ffentliche Anfrage ihres Medienexperten MdB J\"{o}rg Tauss nicht beantwortet hat, fragt die unionschristliche Opposition unter Federf\"{u}hrung ihres Medienexperten MdB Dr. Martin Mayer die Bundesregierung, ob der Br\"{u}sseler Richtlinienvorschlag ihrer Meinung nach der F\"{o}rderung von Innovation und Forschung dienlich und hinreichend klar gefasst ist.  Wie schon anl\"{a}sslich seiner Anfrage vom Herbst 2000 gelingt es Mayer, im wesentlichen klare Gedanken zu fassen.  Allerdings weist der Text auch Unzul\"{a}nglichkeiten auf, die wir in dieser Rezension aufzeigen.}
\begin{sect}{anfr}{Die Anfrage}
\begin{quote}
{\it Deutscher Bundestag}

{\it Drucksache 14/9039}

{\it 14. Wahlperiode}

{\it 2002-05-14}

{\it Kleine Anfrage

der Abgeordneten Dr. Martin Mayer (Siegertsbrunn), Dr. Martina Krogmann, Bernd Neumann (Bremen), Dr. Heinz Riesenhuber, Dr. Bernd Protzner, Sylvia Bonitz, Renate Diemers, Elmar M\"{u}ller (Kirchheim), Norbert R\"{o}ttgen und der Fraktion der CDU/CSU\footnote{http://swpat.ffii.org/gasnu/cducsu/index.de.html}}

{\it Softwarepatente, Wettbewerb, Innovation und KMU}

{\it Die Frage der Patentierbarkeit von softwarebasierten Erfindungen ist in Europa seit l\"{a}ngerem umstritten. Das wurde auch im Vorfeld der Vorlage einer neuen EU-Richtlinie deutlich.}

{\it Zwischenzeitlich hat die Kommission der europ\"{a}ischen Gemeinschaft (EU-Kommission) einen Entwurf f\"{u}r eine ''Richtlinie des Europ\"{a}ischen Parlaments und des Rates \"{u}ber die Patentierbarkeit computerimplementierter Erfindungen`` vorgelegt. Die Meinungen dazu gehen weit auseinander und reichen von der umfassenden Zustimmung zur Harmonisierung der Patentierbarkeit auf der Basis des status quo bis zur absoluten Ablehnung der Patentierung von softwarebasierten Erfindungen.}
\end{quote}

In der Wortwahl stellt sich die CDU/CSU hier -- vielleicht unwillk\"{u}rlich -- auf die Seite der Softwarepatentbef\"{u}rworter:
\begin{itemize}
\item
Die derzeitige Praxis des Europ\"{a}ischen Patentamtes wird als ``status quo'' bezeichnet.  Dass diese Praxis vom Gesetz ebenso wie von der Rechtsprechung zahlreicher europ\"{a}ischer (einschlie{\ss}lich deutscher) Gerichte stark abweicht, findet keine Erw\"{a}hnung.  Die politischen Interessen m\"{a}chtiger europ\"{a}ischer Beh\"{o}rden haben offenbar auch bei der CDU/CSU im Zweifelsfall Vorrang vor dem Rechtsstaat und dem Grundrechtsschutz des europ\"{a}ischen B\"{u}rgers.

\item
Kein Diskussionsteilnehmer (insbesondere nicht FFII oder Eurolinux) wendet sich gegen die Patentierung von Erfindungen.  Erfindungen m\"{u}ssen gem\"{a}{\ss} patentrechtlicher Dogmatik patentierbar sein, egal ob sie als ``softwarebasiert'' bezeichnet werden k\"{o}nnen oder nicht.  Andererseits spricht kein Softwareentwickler von ``Erfindungen''.  Wir lehnen die Patentierung von Programmierideen (Organisations- und Rechenregeln, Algorithmen, Abstraktionen) ab, nicht die von \emph{Erfindungen} (d.h. Lehren \"{u}ber anwendbare Wirkungszusammenh\"{a}nge von Naturkr\"{a}ften).  Mayer optiert hier nicht f\"{u}r die Sprache des Softwareentwicklers sondern f\"{u}r die des unaufgekl\"{a}rten Patentrechtlers, der alle m\"{o}glichen Ideen f\"{u}r den Zugriff des Patentwesens erschlie{\ss}en will.
\end{itemize}

\begin{quote}
{\it Auch bei der Anh\"{o}rung\footnote{http://swpat.ffii.org/penmi/2001/bundestag/index.de.html} im Unterausschuss ``Neue Medien'' des Deutschen Bundestages im Juni 2001 wurde das Thema kontrovers diskutiert.}

{\it Die Bef\"{u}rworter der Patentierbarkeit softwarebasierter Erfindungen argumentieren damit, dass in Europa die Patentierung ohnehin bereits jetzt m\"{o}glich sei, sofern Technizit\"{a}t bestehe, also ein technischer Beitrag geleistet werde. Zudem sei es nicht ersichtlich, weshalb Softwarepatente anders als Patente f\"{u}r andere Technologiebereiche behandelt werden sollten. Vor allem zum besseren Schutz der Rechte der Erfinder softwarebasierter Neuerungen sei es unerl\"{a}sslich, auch diese Erfindungen unter den umfassenderen Schutz eines Patents stellen zu k\"{o}nnen. Hingewiesen wird auch auf die Verh\"{a}ltnisse in den USA, wo im Gegensatz zu Europa die Patentierung von Gesch\"{a}ftsideen und Dateiformaten ohne technischen Beitrag m\"{o}glich ist.}
\end{quote}

Die Patentierbarkeitsregeln des Europ\"{a}ischen Patentamtes (EPA) unterscheiden sich nicht mehr wesentlich von denen des US-Patentamtes, wie man an den Ergebnissen\footnote{http://swpat.ffii.org/pikta/index.de.html} unschwer erkennt.  Der letzte Satz ist doppelt unwahr:
\begin{itemize}
\item
Sowohl am EPA als auch am USPA werden Patente erteilt, die man durch Anwendung eines Dateiformats oder einer Gesch\"{a}ftsidee verletzen kann.  Diese Patente werden vom EPA in gro{\ss}er Menge erteilt und durch den neuen Richtlinienvorschlag best\"{a}tigt.

\item
Das ``Erfordernis eines technischen Beitrags'' ist tautologisch: ``technischer Beitrag'' ist seit jeher ein Synonym f\"{u}r ``Erfindung''.  Laut EPA und RiLi-Vorschlag\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/index.en.html} muss die Erfindung nunmehr nicht einmal mehr neu sein, und Rechenregeln gelten per se als ``technisch'', so dass dem Begriff ``Technik'' in diesem Zusammenhang, anders als etwa in der traditionellen deutschen Rechtsprechung, nicht nur keine einschr\"{a}nkende sondern sogar eine aufweichende Bedeutung zukommt.  Andererseits lassen sich auch die Patentierbarkeitserfordernisse des US-Patentamtes mit der Worth\"{u}lse ``technischer Beitrag'' ebenso gut (oder schlecht) erfassen wie die z.T. noch niedrigeren Anforderungen des EPA und des Br\"{u}sseler Richtlinienvorschlages.
\end{itemize}

\begin{quote}
{\it Die Gegner der Patentierbarkeit von Software kommen vor allem aus der Open-Source-Bewegung. Zu ihnen geh\"{o}ren auch viele kleine und mittelst\"{a}ndische Unternehmen (KMU). Diese bef\"{u}rchten, dass durch eine Ausdehnung der Patentierbarkeit von softwarebasierten Erfindungen Monopolstrukturen gest\"{a}rkt und erweitert, Fortschritt und Innovation in der Softwareentwicklung gebremst, kleine Softwareunternehmen und selbst\"{a}ndige Programmierer in ihrer Existenz gef\"{a}hrdet und die Open-Source-Programmierung zum Erliegen kommen k\"{o}nnte. Auch in den USA ist die gegenw\"{a}rtig gehandhabte extensive Patentierbarkeit von softwarebasierten Erfindungen nicht ganz unumstritten.}
\end{quote}

Hier res\"{u}miert Mayer die Argumente einigerma{\ss}en pr\"{a}gnant, redet sie aber klein.
\begin{itemize}
\item
Die meisten politisch interessierten Programmierer sind sowohl gegen Softwarepatente als auch f\"{u}r alles, was eine Bresche in die Monopolisierung der informationellen Infrastruktur zu schlagen geeignet ist, wie z.B. offene Standards und freie Software.  Das macht aber noch lange nicht alle Patentkritiker zu Anh\"{a}ngern eines bestimmten Gesch\"{a}ftsmodells oder gar einer sozialromatischen ``Bewegung''.  Unter den 118000 Unterst\"{u}tzern der Eurolinux-Petition befinden sich ca 1000 Vorst\"{a}nde von Firmen und 300 offizielle Firmen-Sponsoren, von denen die meisten propriet\"{a}re Software schreiben.

\item
Soziologisch korrekt w\"{a}re es hingegen, in dem Umfeld der Patentjuristen eine Patentbewegung\footnote{http://swpat.ffii.org/stidi/lijda/index.de.html} zu erkennen.  Laut der \"{u}berzeugenden Darstellung von Fritz Machlup\footnote{http://swpat.ffii.org/papri/machlup58/index.en.html} handelte es schon vor 150 Jahren beim ``Deutschen Patentschutzverein'' um eine Bewegung der Juristen und Protektionisten gegen die Volkswirte.

\item
In den USA ist nicht nur die ``gegenw\"{a}rtige Handhabung'' der Logikpatente h\"{o}chst umstritten.  Die amerikanischen Wirtschaftswissenschaftler sind sich, soweit an ihren Ver\"{o}ffentlichungen erkennbar, einig, dass diese Praxis der Wirtschaft schadet.  Zahlreiche Umfragen zeigen, dass ca 90\percent{} der Programmierer auch in den gr\"{o}{\ss}ten amerikanischen Softwareunternehmen diese Praxis ablehnen.  Gerade der von Mayer unvorsichtig \"{u}bernommene Patentamts-Neusprech, der aus Algorithmen ``softwarebasierte Erfindungen'' macht, offenbart durch seinen verkrampft euphemistischen Duktus die tyrannische Natur des Regimes, welches Patentjuristen in den USA der Softwarebranche aufgezwungen haben.
\end{itemize}

\begin{quote}
{\it Wir fragen dazu die Bundesregierung:}

{\it Welche Haltung nimmt die Bundesregierung gegen\"{u}ber dem Schutz der Rechte f\"{u}r softwarebasierte Erfindungen und dem Richtlinienvorschlag der EU-Kommission ein?}
\end{quote}

Mit dem Ausdruck ``Schutz der Rechte ... Erfindungen'' \"{u}bernimmt Mayer die Propaganda-Diktion der Patentbewegung.  Monopole mutieren zu Eigentum, Verbote zu Rechten, Privilegien zu Schutz, Algorithmen zu Erfindungen.

\begin{quote}
{\it Welche Ma{\ss}nahmen hat die Bundesregierung bereits ergriffen, um den Diskussionsprozess \"{u}ber die Rechte zum Schutz von softwarebasierten Erfindungen zu gew\"{a}hrleisten, und wie will sie diesen weiter f\"{o}rdern?}
\end{quote}

Die Bundesregierung hat immerhin ein paar Auftr\"{a}ge zu Gutachten vergeben.  An den Einzelheiten erkennt man allerdings, wie verkrampft man sich dort in politisch vermintem Gel\"{a}nde bewegt -- nicht viel anders als bei der Opposition:
\begin{itemize}
\item
Sicherheit in der Informationstechnologie und Patentschutz f\"{u}r Software-Produkte - Kurzgutachten von Lutterbeck et al im Auftrag des BMWi\footnote{http://swpat.ffii.org/papri/bmwi-luhoge00/index.de.html}

\item
Fraunhofer/MPI 2001: Economic/Legal Study about Software Patents\footnote{http://swpat.ffii.org/papri/bmwi-fhgmpi01/index.de.html}
\end{itemize}

\begin{quote}
{\it Ist nach Auffassung der Bundesregierung in dem nun vorliegenden Richtlinienentwurf der Patentschutz f\"{u}r softwarebezogene Erfindungen so gestaltet, dass}

{\it \begin{itemize}
\item
Innovationen und Forschung gef\"{o}rdert werden

\item
eine sinnvolle Abgrenzung zwischen patentierbaren und nicht-patentierbaren Programmen sichergestellt wird

\item
freie Softwareentwickler und kleine Softwareunternehmen in ihrer Arbeit nicht gef\"{a}hrdet werden

\item
die Open-Source-Softwareentwicklung nicht behindert wird und

\item
der europ\"{a}ischen Softwarebranche keine Nachteile gegen\"{u}ber den USA entstehen?
\end{itemize}}
\end{quote}

Diese Fragen sind gut formuliert.   Allerdings macht Mayer es der Bundesregierung leicht, sich in kryptischen Patentamts-Neusprech zu fl\"{u}chten und verbindlichen Aussagen aus dem Wege zu gehen.  So ist es auch bereits bei den Gutachten geschehen, welche die Bundesregierung mit unklarer Fragestellung vergeben hat.  Die CDU/CSU sollte diesmal unserer Bundesregierung eine Testsuite tats\"{a}chlicher Patente\footnote{http://swpat.ffii.org/stidi/manri/index.en.html} vorlegen und eine klare Stellungnahme verlangen.

\begin{quote}
{\it Was gedenkt die Bundesregierung zu unternehmen, um sicherzustellen, dass die unter Punkt drei genannten Anforderungen in der ``Richtlinie \"{u}ber die Patentierbarkeit computerimplementierter Erfindungen'' umgesetzt werden?}

{\it Berlin, den 14. Mai 2002}

{\it \begin{center}
Dr. Martin Mayer (Siegertsbrunn)

Dr. Martina Krogmann

Bernd Neumann (Bremen)

Dr. Heinz Riesenhuber

Dr. Bernd Protzner

Sylvia Bonitz

Renate Diemers

Elmar M\"{u}ller (Kirchheim)

Norbert R\"{o}ttgen

und die Fraktion der CDU/CSU\footnote{http://swpat.ffii.org/gasnu/cducsu/index.de.html}
\end{center}}
\end{quote}
\end{sect}\begin{sect}{antw}{Die Antwort aus dem BMJ}
Antworten der Bundesregierung auf die Kleine Anfrage der Fraktion der CDU/CSU - Softwarepatente, Wettbewerb, Innovation und KMU, [BT-Drs.14/9039]

\begin{sect}{frage1}{1. Frage}
\begin{quote}
{\it Welche Haltung nimmt die Bundesregierung gegen\"{u}ber dem Schutz der Rechte f\"{u}r softwarebasierte Erfindungen und dem Richtlinienvorschlag der EU-Kommission ein?}
\end{quote}

Die Bundesregierung tritt f\"{u}r einen angemessenen Schutz von Erfindungen auf allen Gebieten der Technik ein. Dies entspricht auch einer Vorgabe des WTO-\"{U}bereinkommens \"{u}ber handelsbezogene Aspekte der Rechte geistigen Eigentums (WTO-TRIPS-\"{U}bereinkommen, Artikel 27). Die Bundesregierung sieht darin eine M\"{o}glichkeit, Anreize f\"{u}r Investitionen zu schaffen.

Die Bundesregierung begr\"{u}{\ss}t den von der Kommission in ihrem Richtlinienentwurf verfolgten Ansatz, durch Vereinheitlichung der Patentierungspraxis zu einem Abbau der Rechtsunsicherheit im Bereich von computerimplementierten Erfindungen beizutragen. Der Richtlinienvorschlag  bietet eine geeignete Verhandlungsgrundlage f\"{u}r die weitere Diskussion.
\end{sect}\begin{sect}{frage2}{2. Frage}
\begin{quote}
{\it Welche Ma{\ss}nahmen hat die Bundesregierung bereits ergriffen, um den Diskussionsprozess \"{u}ber die Rechte zum Schutz von softwarebasierten Erfindungen zu gew\"{a}hrleisten, und wie will sie diesen weiter f\"{o}rdern?}
\end{quote}

Zur umfassenden Ermittlung der wirtschaftlichen Auswirkungen der Patentierung computerimplementierter Erfindungen hat die Bundesregierung bereits im Jahre 2001 zwei Gutachten in Auftrag gegeben, u.a. ein Forschungsprojekt, das die relevanten Fragestellungen unter anderem mit Hilfe empirischer Erhebungen unter deutschen und europ\"{a}ischen Unternehmen vertieft behandelt hat (Mikro- und makro\"{o}konomische Implikationen der Patentierbarkeit von Softwareinnovationen. Geistige Eigentumsrechte im Spannungsfeld von Wettbewerb und Innovation\footnote{http://swpat.ffii.org/papri/bmwi-fhgmpi01/index.de.html}).

Nach Vorlage des Vorschlags der Europ\"{a}ischen Kommission f\"{u}r eine Richtlinie \"{u}ber die Patentierbarkeit computerimplementierter Erfindungen wurden die beteiligten Kreise informiert und um Stellungnahme gebeten.  Bereits am 13. Mai 2002 wurde au{\ss}erdem ein Fachgespr\"{a}ch mit Vertretern der Wissenschaft, der Rechtsprechung, der Patentanwaltschaft und verschiedener Verb\"{a}nde, u.a. der Open-Source-Bewegung durchgef\"{u}hrt, das zu fruchtbaren Diskussionen gef\"{u}hrt hat. Die beteiligten Kreise werden \"{u}ber weitere Entwicklungen unterrichtet werden und die Gelegenheit erhalten, ihre Vorstellungen einzubringen.

Parallel soll verst\"{a}rkt Aufkl\"{a}rungsarbeit f\"{u}r kleinere Unternehmen und freie Entwickler \"{u}ber die M\"{o}glichkeit der Patentierung durch die Patent\"{a}mter geleistet werden, da hier noch erhebliche Unsicherheiten bestehen.
\end{sect}\begin{sect}{frage3}{3. Frage}
\begin{quote}
{\it Ist nach Auffassung der Bundesregierung in dem nun vorliegenden Richtlinienentwurf der Patentschutz f\"{u}r softwarebezogenen Erfindungen so gestaltet, dass \begin{itemize}
\item
Innovationen und Forschung gef\"{o}rdert werden,|eine sinnvolle Abgrenzung zwischen patentierbaren und nicht-patentierbaren Programmen sichergestellt wird,|freie Softwareentwickler und kleine Softwareunternehmen in ihrer Arbeit nicht gef\"{a}hrdet werden,| die Open-Source-Softwareentwicklung nicht behindert wird und|der europ\"{a}ischen Softwarebranche keine Nachteile gegen\"{u}ber den USA entstehen?
\end{itemize}}
\end{quote}

Ziel der Richtlinie ist es, die nationalen Patentgesetze zu harmonisieren und Patentierungsvoraussetzungen transparenter zu machen.  In Zusammenarbeit mit den beteiligten Kreisen und Experten aus dem Deutschen Patent- und Markenamt und den Gerichten pr\"{u}ft die Bundesregierung derzeit, ob die vorgeschlagenen Regelungen geeignet sind, die angestrebten Ziele zu erreichen. Der Abstimmungsprozess ist noch nicht abgeschlossen.
\end{sect}\begin{sect}{frage4}{4. Frage}
\begin{quote}
{\it Was gedenkt die Bundesregierung zu unternehmen, um sicherzustellen, dass die genannten Anforderungen in der ``Richtlinie \"{u}ber die Patentierbarkeit computerimplementierter Erfindungen'' umgesetzt werden?}
\end{quote}

Um dies zu erreichen, wird die sorgf\"{a}ltige Pr\"{u}fung des Richtlinienvorschlags unter Beteiligung der interessierten Kreise, der Rechtsprechung, der Wissenschaft und der Praxis fortgesetzt.

Bei den Beratungen auf EU-Ebene wird sich die Bundesregierung f\"{u}r die Umsetzung der genannten Ziele und Anforderungen einsetzen.
\end{sect}
\end{sect}\begin{sect}{links}{Annotated Links}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf Heise: Bundesregierung begr\"{u}{\ss}t grunds\"{a}tzlich Softwarepatente\footnote{http://www.heise.de/newsticker/data/hod-10.06.02-000/}}}

\begin{quote}
Ein Heise-Journalist fasst den Inhalt der BMJ-Antwort zusammen und l\"{o}st damit unter Softwareentwicklern Entsetzen aus, wie man an der Diskussion im Forum sieht.
\end{quote}
\filbreak

\item
{\bf {\bf Federal Ministery of Justice and Logic Patents\footnote{http://swpat.ffii.org/gasnu/bmj/index.de.html}}}

\begin{quote}
Das Bundesministerium der Justiz und die Ministerin m\"{o}chten sich, soweit man ihren Presseerkl\"{a}rugen glauben darf, gerne durch einen gro{\ss}z\"{u}gigen Ausbau des Patentwesens profilieren.  In dieser Dokumentation erl\"{a}utern wir u.a. aktuelle Hintergr\"{u}nde der derzeitigen Auseinandersetzung um Softwarepatente, bei der das BMJ europaweit eine Vorreiterrolle einnimmt, ohne wirklich so recht zu wissen, was es will.  Gegen\"{u}ber diversen nackten Kaisern nehmen BMJ-Beamte eine Haltung des ``Nichts h\"{o}ren, nichts sehen, nicht sagen'' ein.  Viele von ihnen verstehen tats\"{a}chlich nicht, was in den esoterischen Entw\"{u}rfen aus Br\"{u}ssel geschrieben steht.  Bei allen bisherigen Gespr\"{a}chsrunden ist das BMJ immer nur als Bremser aufgetreten.  Dennoch bleibt das BMJ innerhalb der Bundesregierung in diesem Dossier, welches sich gegen die W\"{u}rde des Menschen, die Allgemeine Handlungsfreiheit und andere Grundwerte unserer Verfassung richtet und zur politischen Deckung eines gro{\ss}angelegten Diebstahl geistigen Eigentums durch einflussreiche Kreise dient, weiterhin federf\"{u}hrend.
\end{quote}
\filbreak

\item
{\bf {\bf Gr\"{u}ne gegen EU-Entwurf zur Patentierbarkeit von Software\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/gruene020513/index.en.html}}}

\begin{quote}
Die Stellungnahme der Gr\"{u}nen Fraktion zum ``Expertengespr\"{a}ch'' vom 13. Mai findet ebenso wenig Beachtung bei der Bundesregierung wie der Brief des SPD-Medienexperten Tauss.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: mlatex ;
% End: ;

