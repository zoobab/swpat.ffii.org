             The European Union's Proposed Directive on
           Computer-Implemented Inventions: What Price
                                                     `Interoperability'?

By Erwin J. Basinski, Basinski & Associates, Santa Barbara, California; tel. 805-962-7437; fax:
805-962-0799; e-mail: ejb@basinskilaw.com; Web: www.basinskilaw.com


                                                                     Summary

   The European Parliament's Committee on Legal Affairs and the Internal Market issued its Final
Report on the proposed directive on the patentability of computer-implemented inventions on June 18,
2003.The report in Word format can be found at www2.europarl.eu.int/omk/sipade2?PUBREF=-//EP//NONSGML+REPORT+A5-2003-
0238+0+DOC+WORD+V0//EN&L=EN&LEVEL=2&NAV=S&LSTDOC=Y. In its final session, the unexpected adoption by the
parliamentary committee of a wide-ranging exception for interoperability (Article 6a) raises questions
about how serious the European Community really is about protecting the intellectual property of
European businesses as well as questions of compatibility with the World Trade Organization's
Agreement on Trade-Related Aspects of Intellectual Property Rights (TRIPS).
   The report is on the agenda for consideration by the plenary session of the Parliament in the week
beginning September 1, 2003. Once adopted there, it passes back to the Council (which must accept it
or send it back for a second reading), but there still exists the possibility of making last-minute
amendments.The consolidated text (i.e., the original Commission proposal plus the recent amendments) should be available on the Parliament's Website
in due course as one of the documents to be considered by the Plenary Session in early September 2003.



                                                                  Final Report

   Under the direction of MEP Ms. Arlene McCarthy as the Rapporteur, the Legal Affairs Committee
concluded a series of 10 sessions extending from April 2002 through May 2003, to consider the
Commission proposalCOM(2002)92 � 2002/0047(COD). See my earlier report on the original Commission proposal titled "European Union's
Proposed Directive on Computer-Implemented Inventions", World E-Commerce & IP Report, Vol. 2, Issue 3, March 2002, BNA International Inc. and a
draft Committee report. The Final ReportEuropean Parliament Report A5-0238/2003, dated June 18, 2003. contains a plethora
of amendments to the original proposal. Many of the amendments are cosmetic and clarify the
language of the original proposal. A detailed review of all of these amendments is beyond the scope of
this article, and the reader is encouraged to carefully review the Final Report. However there are at
least a few amendments -- and two in particular -- that should be of interest.
   Before reviewing these amendments, it is necessary to acknowledge the efforts of Ms. McCarthy for
her persistence and courageous efforts in the face of intense pressure from members of the Open
Source and Free Software groups, as well as the lobbying efforts of such groups as the European
Committee on Interoperable Systems (ECIS).I have been unable to obtain a complete list of the members of ECIS, but it is known that
the membership includes Sun Microsystems, Fujitsu, Hitachi, and many of the same groups that are members of the American Committee on Interoperable

Systems (ACIS). ACIS members include Accolade, Inc.; Advanced Micro Devices, Inc.; Amdahl Corporation; America Online, Inc.; Berkeley Software

Design, Inc.; Broderbund Software, Inc.; Bull HN Information Systems, Inc.; Clearpoint Research Corporation; Color Dreams, Inc.; Comdisco, Inc.; Emulex

Corporation; Forecross Corporation; The Fortel Group; Fujitsu Systems Business of America, Inc.; Hitachi Data Systems; ICTV; Insignia Solutions; Johnson-

Laird, Inc.; Landmark Systems Corporation; LCS/Telegraphics; MidCore Software, Inc.; NCR Corporation; New York Systems Exchange, Inc.; Passage

Systems, Inc.; Phoenix Technologies, Ltd.; Plimoth Research Inc.; QAD Inc.; Seagate Technology, Inc.; Software Association of Oregon (consists of over 550

software development firms, firms in associated industries, and individuals professionally involved in software development); Software Forum (consists of over

1,000 software entrepreneurs and developers); Storage Technology Corporation; Sun Microsystems, Inc.; 3 Com Corporation; Tandem Computers; Trilium

Consumer Electronics, Inc.; TriTeal; Western Digital Corporation; and Zenith Data Systems Corporation. In a recent article in the
Guardianwww.guardian.co.uk/online/story/0,3605,975126,00.html., responding to criticisms from the Open Source and
Free Software groups, Ms. McCarthy summarized the Final Report.
   First, she said, the directive "is not proposing to patent all software" but is limited to genuine
inventions. "Software as such cannot be patented," she reiterated. Next, her "cosmetic" amendments
propose "a more restrictive interpretation of the law in Europe" in an effort to stop the drift towards
the patenting of business methods, as under U.S. practice. She added that she had also included a new
article to allow decompiling and reverse engineering, which were sought by computer programmers.
   In addition, she said, the directive "will not have any adverse effects on open source software
development". Even in the United States, "where the patenting system is more liberal", the Linux
operating system has declared a 50 percent growth in worldwide shipment of its operating systems,
she noted, as the Apache system dominates the Web server market with a 66 percent market share.


"With a more restrictive EU law, the open source community has nothing to fear," she declared.
"From medical inventions to household appliances, mobile phones and machine tools, inventions
involving software are increasingly a fact of life. With many of our traditional industries migrating to
the Far East leaving behind job losses, we Europeans are having to rely on licensing out inventiveness
to generate income and create jobs."


                                                      Amendments

   Amendments 6 through 9 present new Recitals 13(a) through 13(d), which attempt to provide
further specifics against the patenting of business methods as such, but in fact, appear to add
ambiguities to the issue. For example, new recital 13(d) (shown in the report without any justification)
states:

      The scope of the exclusive rights conferred by any patent are defined by the claims. Computer-
   implemented inventions must be claimed with reference to either a product such as a programmed
   apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements
   of software are used in contexts which do not involve the realisation of any validly claimed product
   or process, such use will not constitute patent infringement.

   Does this mean that a patent validly granted on a computer program would be unenforceable if used
for an application not specifically mentioned in the patent specification? If the rule is that a patent
grants the owner the right to prevent others from making, using, or selling it, and if the claims define
the invention, then any use which reads on the claims must be infringing. Thus this recital at least
appears unnecessary and only adds ambiguity.
   In the original Commission proposal, specifically omitted by the Commission was an article which
provided that a computer program on a computer-readable medium, or carrier, or as a signal, is
patentable if the method executed by the program was validly patentable.Generally called Beauregard claims after the
IBM cases wherein these type claims were allowed, both in the U.S. and in the European Patent O Amendment 18 of the Final Report
provides a new Article 5.2, which reinstates the patentability of such Beauregard claims. It states:

      A claim to a computer program, on its own, on a carrier or as a signal, shall be allowable only if
   such program would, when loaded or run on a computer, computer network or other programmable
   apparatus, implement a product or carry out a process patentable under Articles 4 and 4a.

   As this amendment provides the addition of a right specifically omitted by the Commission, it is
understood to be a possibility that the Commission could reject this Final Report and propose its
original directive to the Parliament and Council, although it is not believed that the Commission will
do so.


                                                  Controversial Item

   The most controversial amendment in the Final Report is Amendment 20, providing a new Article
6(a), which states:

      "Member States shall ensure that wherever the use of a patented technique is needed for the sole
   purpose of ensuring conversion of the conventions used in two different computer systems or
   network so as to allow communication and exchange of data content between them, such use is not
   considered to be a patent infringement."

   The justification for this new article states:

      "The possibility of connecting equipments so as to make them interoperable is a way of ensuring
   open networks and avoiding abuse of dominant positions. This has been specifically ruled in the
   case law of the Court of Justice of the European Communities in particular. Patent law should not
   make it possible to override this principle at the expense of free competition and users."


                                                     Interoperability

   The term "interoperability" has become similar to the phrase "system" in that it can mean almost
anything one desires. Doing a search for the term "interoperability" on www.google.com produces well
over 100 hits, including various definitions and descriptions of organizations dedicated to
interoperability in various aspects. For example, in the article "Interoperability -- What Is It and Why
Should I want it?"www.ariadne.ac.uk/issue24/interoperability/. Paul Miller presents the following comments with
perhaps the most elementary definitions:


       Interoperability. Together with terms like "metadata" and "joined-up thinking", this word is
    increasingly being used within the information management discourse across all of our memory
    institutions. Its meaning, though, remains somewhat ambiguous, as do many of the benefits of
    "being interoperable".

       He notes the following definitions:

       interoperable, adj., "able to operate in conjunction" (Concise Oxford Dictionary, 9th Edition);

       interoperability, "the ability of a system or a product to work with other systems or products
    without special effort on the part of the customer. Interoperability becomes a quality of increasing
    importance for information technology products as the concept that `The network is the computer'
    becomes a reality. For this reason, the term is widely used in product marketing descriptions."
    (whatis.com)

    A more expansive definition can be found in the article from the UK Office for Library and
Information Networkingwww.ukoln.ac.uk/interop-focus/about/:

       "Interoperability" is a broad term, encompassing many of the issues impinging upon the
    effectiveness with which diverse information resources might fruitfully co-exist. These issues are
    many and varied, but a key set may usefully be identified as; Technical interoperability, Semantic
    interoperability, Political/Human interoperability, Inter-community interoperability and
    International interoperability.

    More specific to this discussion is perhaps the definition of "Technical Interoperability":

       Technical Interoperability: In many ways the most straightforward aspect of maintaining
    interoperability, consideration of technical issues includes ensuring an involvement in the continued
    development of communication, transport, storage and representation standards such as Z39.50,
    ISO-ILL, XML, etc. Work is required both to ensure that these individual standards move forward
    to the benefit of the community, and to facilitate where possible their convergence, such that
    systems may effectively make use of more than one standards-based approach.Id.

    Indeed, among the many new interoperability-based organizations focused on "Technical
interoperability" are WS-1WS-I is an open industry organization chartered to promote Web services interoperability across platforms, operating
systems, and programming languages. The organization works across the industry and standards organizations to respond to customer needs by providing

guidance, best practices, and resources for developing Web services solutions., ECISSee footnote 6 supra., ACISSee footnote 6 supra., and the
CCIASee footnote 19 infra.. All of these groups generally contain the same companies as members, but the
newer ones such as WS-1 also include many users and patent holders such as IBM, EDS, Accenture,
etc. These groups are focused on "standards" for protocols, conventions, interconnects, etc., wherein
these standards may or may not be covered by patents and may or may not be licensed royalty-free.
    Such agreements and standards, while they may touch on patents and royalty/licensing
considerations, nevertheless exist within the framework of the current patent laws, which have no
known infringement exceptions for interoperability reasons. Thus, while there is general agreement
that Interoperability, as a goal, is indeed desirable, it would appear that the approach proposed through
amended Article 6a would render more harm than good.


                                                                  Amendment 6a

    This amendment appears to be an additional attempt to impose the interoperability constraints of the
Copyright Directive and copyright law in general onto patent law. The Court of Justice case
mentioned (but not properly cited) is a copyright case. It is assumed that the "conventions" would be
software or hardware conventions which permit the interoperability of systems. There is a similar
provision in the Directive on copyright in computer software. This excluded from copyright
infringement the creation of interfaces necessary for the interoperability between two systems.It is well
known that the aims and objectives of ECIS are the same as those of ACIS and the Computer & Communications Industry Association (CCIA). In the U.S.

ACIS and the CCIA have authored many amicus curie briefs in copyright cases to promote interoperability in the copyright area. An amicus brief which

describes the aims and objectives of ACIS and CCIA can be found at www.eff.org/IP/Video/DVDCCA_case/20000628_def_ccia_appeal_amicus.html.

    In the U.S., in the early copyright case of Lotus v. BorlandLotus v. Borland (CA 1), 49 F.3d 870, the ultimate
issue was whether an interface was copyrightable. The appeals court said it was not and further added
that if an interface was to be protected the owner must do it via patent law and not copyright.One authority
indicated that "the court of appeals found that the Lotus interface was a method of operation and not copyrightable (the majority, anyway; the concurring

opinion suggested that it might be copyrightable, but that a compatible use was a privileged use akin to fair use). . . . the holding [was] that the user interface

was not copyrightable. The Supreme Court subsequently granted cert, but Justice Stevens had recused himself, and with eight justices hearing the case, it split

4-4 and affirmed with no opinion. A split among the circuits on this issue continues." To my knowledge there is no reservation of
rights in patent law in any country due to notions of interoperability, and while standards for technical


interoperability are desirable, if not necessary, making unenforceable untold existing patents would
appear to be the wrong approach. To allow this new Article 6(a) would appear to eviscerate the
purpose of the Commission directive as identified in Recital 14, to wit:

       ... The rules of national patent law remain the essential basis for the legal protection of
   computer-implemented inventions. This Directive simply clarifies the present legal position having
   regard to the practices of the European Patent Office with a view to securing legal certainty,
   transparency, and clarity in the law and avoiding any drift towards the patentability of unpatentable
   methods, such as business methods.

   This new Article 6(a) goes beyond the reverse engineering concerns regarding the copyright
interoperability issue, which would appear to be adequately covered by the original Article 6. This
Article 6(a) is so broad as to render unenforceable a majority of the patents held by European
companies such as Siemens, STMicroelectronics, Nokia, and Ericsson, as well as IBM, Sun, Fujitsu,
Hitachi, and others. For example, "conventions used in two different computer systems or network so
as to allow communication and exchange of data content between them" would appear to cover any
patent for any protocol or data format specification (JPEG, MPEG, XML, any wireless protocol, etc.),
any convention such as language compatibility (JAVA) or architecture interfaces, many Internet-
related regimes, and any number of technologies that might be needed for the sole purpose of ensuring
computer 1 can talk to computer 2.
   Article 6(a) would appear to render any such patent unenforceable in Europe.This would appear to cover patents
held by the members of ACIS, ECIS, and CCIA. The known CCIA members include Amdahl Corporation; AT&T Corporation; Bell Atlantic Corporation;

Block Financial Corporation; CAI/SISCo; Cerebellum Software, Inc.; Commercial Data Servers, Inc.; Datum, Inc.; E-Stamp Corp.; Entegrity Solutions

Corporation; Fantasma Networks; Fujitsu Limited; Giga Information Group; Government Sales Consultants, Inc.; Hitachi Data Systems, Inc.; Intuit, Inc.;

MERANT; Mercator; NetCom Solutions International, Inc.; NOKIA; Nortel Networks; Novak Biddle Venture Partners; NTT America, Inc.; Okidata; Oracle

Corporation; SABRE Inc.; Sun Microsystems, Inc.; Tantivy Communications, Inc.; Telesciences, Inc.; Time Domain Corporation; US West; Viatel, Inc.; ViON

Corporation; and Yahoo! Inc. It should be noted that many of these protocols are covered by standards and, as
such, are licensed generally to all at reasonable rates, if not royalty-free.
   Perhaps of greater importance, the inclusion of this Article 6(a) throws into question whether the
directive as amended complies with the provisions of TRIPS to provide patent protection for all fields
of technology. In my opinion, this Article 6(a) must be removed in order for the directive to be
TRIPS-compatible, as well as to conform to current European patent law.


                                                                  Conclusion

   As mentioned, the report is on the agenda for consideration by the plenary session of the European
Parliament in the week beginning September 1, 2003. It is not believed possible to have a first-reading
adoption, as it presently stands with the controversial Article 6(a) included. An adoption of the present
amendments would guarantee that the United States will cement its lead in the global information
technology revolution, because its authorities provide "a hospitable environment to information
technology (IT) business development".See the article at zdnet.com.com/http://zdnet.com.com/2100-1104-1027238.html. In any
case, the issues of technical interoperability as well as international interoperability will remain a key
focus as efforts to harmonize patent systems continue.

Notes

1 The report in Word format can be found at www2.europarl.eu.int/omk/sipade2?PUBREF=-
    //EP//NONSGML+REPORT+A5-2003-
    0238+0+DOC+WORD+V0//EN&L=EN&LEVEL=2&NAV=S&LSTDOC=Y.
2 The consolidated text (i.e., the original Commission proposal plus the recent amendments) should be available
    on the Parliament's Website in due course as one of the documents to be considered by the Plenary Session in
    early September 2003.
3 COM(2002)92 � 2002/0047(COD). See my earlier report on the original Commission proposal titled
    "European Union's Proposed Directive on Computer-Implemented Inventions", World E-Commerce & IP
    Report, Vol. 2, Issue 3, March 2002, BNA International Inc.
4 European Parliament Report A5-0238/2003, dated June 18, 2003.
5 I have been unable to obtain a complete list of the members of ECIS, but it is known that the membership
    includes Sun Microsystems, Fujitsu, Hitachi, and many of the same groups that are members of the American
    Committee on Interoperable Systems (ACIS). ACIS members include Accolade, Inc.; Advanced Micro
    Devices, Inc.; Amdahl Corporation; America Online, Inc.; Berkeley Software Design, Inc.; Broderbund
    Software, Inc.; Bull HN Information Systems, Inc.; Clearpoint Research Corporation; Color Dreams, Inc.;
    Comdisco, Inc.; Emulex Corporation; Forecross Corporation; The Fortel Group; Fujitsu Systems Business of
    America, Inc.; Hitachi Data Systems; ICTV; Insignia Solutions; Johnson-Laird, Inc.; Landmark Systems
    Corporation; LCS/Telegraphics; MidCore Software, Inc.; NCR Corporation; New York Systems Exchange,
    Inc.; Passage Systems, Inc.; Phoenix Technologies, Ltd.; Plimoth Research Inc.; QAD Inc.; Seagate
    Technology, Inc.; Software Association of Oregon (consists of over 550 software development firms, firms in


   associated industries, and individuals professionally involved in software development); Software Forum
   (consists of over 1,000 software entrepreneurs and developers); Storage Technology Corporation; Sun
   Microsystems, Inc.; 3 Com Corporation; Tandem Computers; Trilium Consumer Electronics, Inc.; TriTeal;
   Western Digital Corporation; and Zenith Data Systems Corporation.
6 www.guardian.co.uk/online/story/0,3605,975126,00.html.
7 Generally called Beauregard claims after the IBM cases wherein these type claims were allowed, both in the
   U.S. and in the European Patent Office.
8 www.ariadne.ac.uk/issue24/interoperability/.
9 www.ukoln.ac.uk/interop-focus/about/.
10 Id.
11 WS-I is an open industry organization chartered to promote Web services interoperability across platforms,
   operating systems, and programming languages. The organization works across the industry and standards
   organizations to respond to customer needs by providing guidance, best practices, and resources for
   developing Web services solutions.
   WS-I was formed specifically for the creation, promotion, or support of Generic Protocols for Interoperable
   exchange of messages between services. Generic Protocols are protocols that are independent of any specific
   action indicated by the message beyond actions necessary for the secure, reliable, or efficient delivery of
   messages; "Interoperable" means suitable for and capable of being implemented in a neutral manner on
   multiple operating systems and in multiple programming languages. See www.ws-i.org/.
12 See footnote 6 supra.
13 See footnote 6 supra.
14 See footnote 19 infra.
15 It is well known that the aims and objectives of ECIS are the same as those of ACIS and the Computer &
   Communications Industry Association (CCIA). In the U.S. ACIS and the CCIA have authored many amicus
   curie briefs in copyright cases to promote interoperability in the copyright area. An amicus brief which
   describes the aims and objectives of ACIS and CCIA can be found at
   www.eff.org/IP/Video/DVDCCA_case/20000628_def_ccia_appeal_amicus.html.
   In this brief the amicus parties state "ACIS and CCIA members believe that computer programs deserve
   effective intellectual property protection to give developers sufficient incentive to create new programs. At
   the same time, ACIS and CCIA are concerned that improper extension of intellectual property law will
   impede innovation and inhibit fair competition in the computer industry. ACIS and CCIA have long
   supported interpreting the intellectual property laws to permit reverse engineering performed to develop
   interoperable products."
16 Lotus v. Borland (CA 1), 49 F.3d 870, 34 U.S.P.Q.2D 1014 (1ST CIR 1995), aff'd by equally divided U.S.
   Supreme Court, 516 US 233 (1996).
17 One authority indicated that "the court of appeals found that the Lotus interface was a method of operation
   and not copyrightable (the majority, anyway; the concurring opinion suggested that it might be copyrightable,
   but that a compatible use was a privileged use akin to fair use). . . . the holding [was] that the user interface
   was not copyrightable. The Supreme Court subsequently granted cert, but Justice Stevens had recused
   himself, and with eight justices hearing the case, it split 4-4 and affirmed with no opinion. A split among the
   circuits on this issue continues."
18 This would appear to cover patents held by the members of ACIS, ECIS, and CCIA. The known CCIA
   members include Amdahl Corporation; AT&T Corporation; Bell Atlantic Corporation; Block Financial
   Corporation; CAI/SISCo; Cerebellum Software, Inc.; Commercial Data Servers, Inc.; Datum, Inc.; E-Stamp
   Corp.; Entegrity Solutions Corporation; Fantasma Networks; Fujitsu Limited; Giga Information Group;
   Government Sales Consultants, Inc.; Hitachi Data Systems, Inc.; Intuit, Inc.; MERANT; Mercator; NetCom
   Solutions International, Inc.; NOKIA; Nortel Networks; Novak Biddle Venture Partners; NTT America, Inc.;
   Okidata; Oracle Corporation; SABRE Inc.; Sun Microsystems, Inc.; Tantivy Communications, Inc.;
   Telesciences, Inc.; Time Domain Corporation; US West; Viatel, Inc.; ViON Corporation; and Yahoo! Inc.
19 See the article at zdnet.com.com/http://zdnet.com.com/2100-1104-1027238.html.


