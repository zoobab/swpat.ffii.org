<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Interopérabilité et Brevet: Controverse au Parlement européen

#descr: L'article 6 de la proposition de directive sur les brevets logiciels prétend défendre l'interopérabilité en imposant une limite à la mise en application des brevets. L'article 6a, qui fut introduit par le Parlement européen et approuvé par les trois comités concernés, impose réellement une légère mais vraie limite. Il dit que les filtres pour la conversion d'un format à un autre peuvent toujours être utilisés, ceci indépendamment des brevets. Malheureusement, même cette limite a provoqué une résistance furieuse du lobby de la corporation des avocats en brevet, supporté par des associations des technologies de l'information et par les gouvernements (dont les politiques sont formulées par des avocats en brevet). Ils répandent %(q:la peur l'incertitude et le doute) a propos de la signification de l'article 6a et font pression pour le retirer car il pourrait légèrement réduire leur domination sur la standardisation. Pendant la pause de l'été 2003, Arlene McCarthy a rédigé une proposition d'amendement pour l'article 6a qui lui retirerait tout son sens. Nous avons expliqué le sens de l'article 6a et ses différents amendements en discussion.

#tel2: Vrais et faux privilèges d'interopérabilité

#rsl: Souvent en informatique, les mérites techniques d'un programme particulier sont moins importants que le nombre d'autres personnes qui l'utilise, celles qui peuvent interagir -- c'est l'effet réseau.

#tga: Une grande inquiétude est que par la création d'un standard  propriétaire dominant, une société informatique pourrait %(q:contrôler)  l'ensemble du marché, rendant impossible pour d'autres programmes  d'interagir, et donc impossible pour eux de rentrer en concurrence.

#eoa: Comme l'affaire Microsoft l'a montré, les lois anti-cartel peuvent  mettre très longtemps pour fonctionner, et à ce moment-là, le marché a  complètement changé.

#WWW2: La loi européenne sur le droit d'auteur reconnaît ce danger, et la  directive 91/250/EEC, articles 5(2) et (3) et (6), permettent la  décompilation d'un programme pour étudier ses interfaces, même si le  code source décompilé ne peut être rendu public, et la décompilation est  permise uniquement si l'information n'est pas disponible aisément d'une  autre façon.

#lat: L'article 6 de la CCE (et les amendements variés qui le réécrivent)  confirme ce droit de décompilation.

#aWW: Mais tout ceci est d'une aide minime pour obtenir l'interopérabilité lorsque les interfaces sont brevetées: l'interopérabilité est uniquement possible en obtenant une licence valable pour le brevet. La décompilation est un problème uniquement dans le contexte du droit d'auteur. L'autoriser dans le contexte des brevets signifie ne rien autoriser du tout.

#mwi: Donc un amendement d'importance capitale est ITRE-15 (Article  6(a)), qui créerait une protection similaire pour l'interopérabilité  face aux droits des brevets. Il dit:

#interop: Les États membres veillent à ce qu'en tout lieu où une  utilisation d'une technique brevetée est exigée pour la communication  avec un autre système (c'est-à-dire pour la conversion en import et en  export des conventions de ce système), une telle utilisation ne puisse  être considérée comme une contrefaçon.

#uia: Cet amendement rendrait par exemple possible de convertir des  données d'un futur format Microsoft Word breveté vers un nouveau format  non breveté. Ceci n'exempterait pas les processus qui pourraient être  concernés par l'utilisation d'un de ces formats. Donc le brevet utilisé  dans un standard de télécommunication resterait valable, mais il serait  possible de créer un deuxième standard de télécommunication qui  utiliserait une technologie indépendante et convertirait les données  créées par l'une des technologies pour produire les données nécessaires  pour l'autre.

#Wae: Ceci est une proposition d'amendement extrêmement modérée. Mais puisqu'elle diminuerait la mainmise de la corporation des avocats en brevet sur les infrastructures publiques de communication, elle a provoqué une violente réaction du lobby des brevets. Arlene McCarthy, qui avait proposé à la JURI de voter contre l'article 6a, a vite proposé un changement qui le rendrait vide de sens:

#tef: McCarthy a essayé de vendre cette proposition aux critiques sur les brevets logiciels dans son parti comme une %(q:offre de compromis).

#Poo: Le lobby des brevets a jusqu'à présent reçu le soutien de MPE tels  que McCarthy, Wuermeling et Manders sans même avoir à déclarer  publiquement ce qui ne va pas dans l'article 6a.  Ils n'ont pas été  capables de montrer que l'article 6a compromettrait un quelconque de  leurs intérêts légitimes.  Le seul argument a été que l'article 30 de  l'ADPIC n'autorise pas de limitations déraisonnables au renforcement des  brevets :

#ere: Les Membres pourront prévoir des exceptions limitées aux droits  exclusifs conférés par un brevet, à condition que celles-ci ne portent  pas atteinte de manière injustifiée à l'exploitation normale du brevet  ni ne causent un préjudice injustifié aux intérêts légitimes du  titulaire du brevet, compte tenu des intérêts légitimes des tiers.

#erW: Le traité des ADPIC a besoin d'être pris au sérieux.  En fait, la  directive est une bonne opportunité pour concrétiser la signification du  traité.

#aWv: Le traité des ADPIC impose une obligation de limiter la  brevetabilité et le renforcement des brevets de façon systématique, et  qui ne soit pas motivée par du protectionnisme commercial ou par des  considérations politiques adhoc en faveur de l'un ou l'autre des groupes  d'intérêts locaux.

#nre: L'article 6a concerne les limitations systématiques au renforcement  de brevets, similaires par nature aux exemptions pour la recherche  universitaire.  Il ne concerne pas les exceptions aux droits exclusifs  conférés par %(e:un brevet), et ne cause pas de préjudice aux intérêts  légitimes aux titulaires de brevets, puisqu'il n'y a pas d'intérêt  légitime (qui soit soutenable) à contrôler l'utilisation des standards  de communication.  En conséquence, l'article 6a fournit un moyen de  résoudre les problèmes de concurrence potentiels causés par les brevets  relatifs au logiciel, et une concrétisation de l'article 30 des ADPIC.   La Commission européenne a échoué même à s'attaquer à ces problèmes.   Arlene McCarthy, encouragé par le commissaire Bolkestein, s'effarouche  de clarifier ce que l'article 30 signifierait dans le contexte de la  standardisation, et propose au lieu de cela, d'écrire la terminologie  abstraite des ADPIC directement dans la directive,  laissant sa  concrétisation aux tribunaux nationaux.  Ainsi, la Commission montre un  désintéressement pour la %(q:clarification et l'harmonisation), et une  approche passive et même destructrice, de résolution des problèmes  réglementaires qu'elle rencontre.

#rjm: Les propositions d'amendements de la JURI concernant l'article 6.  Contrairement aux recommandations de Arlene McCarthy, la commission à  décidé d'approuver le ITRE-15. Cela déclencha des vagues de fureur dans  les cercles des comités de juristes en brevet industriel des  associations d'industries et des institutions gouvernementales, car  c'était le seul point où la JURI avait effectivement fait quelque chose  qui touchait légèrement les intérêts des avocats en brevet: %(q:Comment  osent-ils!) .. %(q:Ils ont du s'emmêler les pinceaux quelque part) ..  %(q:Je suis sûr que Malcolm nous aidera à régler ça) .. %(q:Nous nous  assurerons que le Conseil ne laisse pas passer ça) ..

#pfs: Quand on en vint aux questions sur les lois sur les brevets,  l'%(EICTA) était toujours aux côtés de l'industrie des brevets contre  l'industrie des logiciels. Alors ils s'insurgèrent avec véhémence contre  le ITRE-15, répandant de fausses informations sur la signification de  cet amendement et sur le sens de l'article 27 ADPIC. Ce document a été  distribué par les lobbyistes de l'EICTA à de nombreux membres du  Parlement européen peu de temps après le vote de la JURI le 17/06/2003.

#nWh: L'Association de avocats en propriété industrielle américaine  (AIPLA) a d'excellents contacts avec la Commission européenne et le  Parlement européen et sait très bien comment ils se comporteront. Un des  experts de l'AIPLA pour les affaires européennes, Erwin Basinski, a fait  l'éloge de Arlene McCarthy pour avoir courageusement résisté au féroce  %(q:lobby opensource). Cependant, Basinski a déploré que McCarthy et ses  amis n'aient pas été assez attentifs et, dans leurs efforts d'apaiser  les peurs, ont permis à des amendements nuisibles de se faufiler.  Basinski analyse d'abord quelques amendements ridicules, de nature  principalement cosmétique afin de préparer la base pour une attaque de  ce qu'il appelle l'%(q:amendement controversé sur l'interopérabilité),  c'est-à-dire le ITRE-15, la seule proposition d'amendement qui puisse  effectivement enlever un peu de beurre des épinards de la communauté des  brevets. Basinski sait que le Parlement européen ne peut pas adopter la  directive si cet amendement exaspérant passe -- bien sûr le Parlement  européen n'oserait pas prendre de décision à l'encontre du lobby des  brevets. Basinski donne alors des affirmations effrayantes et insensées:  (1) que le ITRE-15 tuerait de nombreux brevets (2) que l'article 27  ADPIC interdirait tout ce qui peut limiter l'étendue de l'application de  la loi à quelques classes de brevets plus qu'à d'autres (3) cet  affaiblissement de l'application de la loi sur les brevets  défavoriserait l'investissement des entreprises des technologies de  l'information (IT) européennes.

#iAW: pointe vers un discours de Bolkestein lors d'un débat en plénière  du Parlement européen, dans lequel il exprime ses craintes au sujet de  l'article 6a et soutient l'approche de Arlene McCarthy de jeter le flou  sur cet article en y introduisant directement le langage abstrait de  l'article 30 ADPIC.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: ccorazza ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-itop ;
# txtlang: fr ;
# multlin: t ;
# End: ;

