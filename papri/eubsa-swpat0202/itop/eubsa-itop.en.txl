<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Interoperability and the Software Patents Directive: What Degree of Exemption is Needed

#descr: Art 6 of the proposed software patent directive pretends to impose a limit on patent enforcement to safeguard interoperability.  Art 6a, which was inserted by the European Parliament and approved by all three concerned committees, actually does impose a gentle but real limit.  It says that filters for conversion from one format to another may always be used, regardless of patents.   Unfortunately even this limit has provoked a furious backlash from corporate patent lawyers, seconded by large IT associations and governments (whose patent policy is usually formulated by corporate patent lawyers).  After the summer pause of 2003, Arlene McCarthy MEP proposed an amendment to Art 6a which would render Art 6a meaningless.  The movement against Art 6a was joined by Wuermeling (EPP), Manders (ELDR) as well as the governments of UK and US.  Yet explanations as to what is wrong with the Interoperability Privilege remain very vague.  We explain the meaning of Art 6a and the different amendments under discussion.

#tel2: Real vs Fake Interoperability Privilege

#rsl: Often in computer software, the technical merits of a particular program are less important than how many other users it has, who can all interact -- the network effect.

#tga: A great concern is that by creating a dominant proprietary standard, one software house may %(q:lock in) the whole market, making it impossible for other programs to interoperate, and so impossible for them to compete.

#eoa: As the Microsoft case made clear, anti-trust laws can take a very long time to operate, by which time the marketplace may have utterly changed.

#WWW2: EU copyright laws recognise this danger, and Directive 91/250/EEC, Articles 5(2) and (3) and 6, allow decompilation of a program to investigate its interfaces, although the decompiled source code may not be made public, and decompilation is permitted only if the information is not otherwise readily available.

#lat: The CEC article 6 (and various amendments that rewrite it) uphold this right of decompilation.

#aWW: But this is of little help in achieving interoperability when interfaces are patented: interoperation would be possible only by securing a valid licence for the patent.  Decompilation is a problem only in the context of copyright.  Allowing it in the contexts of patents means allowing nothing.

#mwi: So an amendment of fundamental importance is ITRE-15 (Article 6(a)), which would create a similar protection for interoperability in the face of patent rights.  It reads:

#interop: Member States shall ensure that wherever the use of a patented technique is needed for the sole purpose of ensuring conversion between the conventions used in two different data processing systems so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.

#uia: This amendment would e.g. make it possible to convert data from a future patented MSWord format to an new unpatented format.  It would not exempt processes which may be involved in using any of the formats.  Thus the patents involved in a telecommunication standard would stay valid, but it would be possible to create a second telecommunicaton standard using independent technology and to convert the output of one of the technologies to the input data needed by the other.

#Wae: This is an extremely moderate proposal.  But since it would loosen the stranglehold of corporate patent lawyers on public commmunication infrastructures, it has provoked a severe backlash of the patent lobby.  Arlene McCarthy, who had recommended that JURI vote against Art 6a, soon proposed a change that would render it void:

#tef: McCarthy tried to sell this proposal to the software patent critics in her party as a %(q:compromise offer).

#Poo: The patent lobby has so far received support from MEPs such as McCarthy, Wuermeling and Manders without even havnign to argue in public what is wrong with Art 6a.  They have not been able to show that Art 6a impairs any legitimate interest of theirs.  The only argument has been that Art 30 TRIPs does not allow unreasonable limitations on the enforcability of patents:

#ere: Members may provide limited exceptions to the exclusive rights conferred by a patent, provided that such exceptions do not unreasonably conflict with a normal exploitation of the patent and do not unreasonably prejudice the legitimate interests of the patent owner, taking account of the legitimate interests of third parties.

#erW: The TRIPs treaty needs to be taken seriously.  In fact the directive is a good opportunity to concretise the meaning of the treaty.

#aWv: The TRIPs treaty imposes an obligation to limit patentability and patent enforcability in systematic ways which are not motivated by trade protectionism or adhoc policy considerations in favor of one or the other local interest group.

#nre: Art 6a is about systematic limitations to patent enforcement, similar in nature to the exemptions for university research.  It is not about exceptions to the exclusive rights conferred by %(e:a patent), and it does not prejudice legitimate interests of patent owners, since there is (arguably) no legitimate interest to control the use of communication standards.  Thus Art 6a provides a way of solving potential competition problems caused by softwar-related patents, and a concretisation of Art 30 TRIPs.  The European Commission has failed to even address these problems.  Arlene McCarthy, encouraged by Commissioner Bolkestein, shyes away from clarifying what Art 30 should mean in the context of standardisation, and instead proposes to write the abstract terminology of TRIPs directly into the directive,  leaving its concretisation to national caselaw.  Thereby the Commission is showing disinterestin %(q:clarification and harmonisation) and a passive, even destructive approach to solving regulatory problems placed before it.

#rjm: JURI amendment proposals concerning article 6.  Contrary to the recommendations of Arlene McCarthy, the committee decided to approve ITRE-15.  This sent waves of fury through the circles of industrial patent lawyers committees in industry associations and governmental institutions, because it was the only instance where JURI actually did something that slightly touched on patent lawyer interests:  %(q:How dare they!) .. %(q:They must have confused something here) .. %(q:I am sure that Malcolm will help us fix this) .. %(q:We will make sure that the Council does not let this through) ..

#pfs: /swpat/players/eicta/index.en.html

#nWh: The American Industrial Property Lawyers' Association (AIPLA) has excellent contacts to the European Commission and to the European Parliament and knows very well how they will behave.  One of AIPLA's experts for European matters, Erwin Basinski, commends Arlene McCarthy for courageously resisting the ferocious %(q:opensource lobby).  However, Basinski deplores that McCarthy and friends were not careful enough and, in their efforts to placate fears, allowed deleterious amdendments to slip in.  Basinski first analyses some ridiculous amendments of mainly cosmetic nature in order to prepare the ground for an attack on what he calls the %(q:controversial interoperability amendment), i.e. ITRE-15, the only amendment proposal that actually can take some butter off the bread of the patent community.  Basinski knows that the European Parliament can not pass the directive if the infuriatinig amendment is passed -- of course the EP would not dare to decide against the patent lobby.   Basinski then makes some frightening and unreasoned statements: (1) that ITRE-15 would kill numerous patents (2) that Art 27 TRIPs prohibits anything which might limit the scope of enforcability some classes of patents more than others (3) that weakening of patent enforcability would disfavor investment in European IT.

#iAW: points to a speech made by Bolkestein in the plenary debate of the European Parliament, in which he expresses misgivings about Art 6a and support for Arlene McCarthy's approach of blurring this article by writing the abstract language of Art 30 TRIPs directly into it.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-itop ;
# txtlang: en ;
# multlin: t ;
# End: ;

