\contentsline {chapter}{\numberline {1}The Text}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Why do we need a directive?}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}What will the Directive achieve?}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}What the Directive would NOT do}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}What the Directive would do}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}Why are so many people worried about this directive?}{7}{section.1.5}
\contentsline {section}{\numberline {1.6}UK Gov't Calls for Bans on Publication of Patent Descriptions}{7}{section.1.6}
\contentsline {section}{\numberline {1.7}UK Government Calls for Bans on Conversion betweeen Data Formats}{8}{section.1.7}
\contentsline {section}{\numberline {1.8}Examples of what can currently be patented and what cannot}{8}{section.1.8}
\contentsline {chapter}{\numberline {2}Annotated Links}{10}{chapter.2}
