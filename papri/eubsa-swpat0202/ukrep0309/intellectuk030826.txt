






Proposed Directive on the Patentability of Computer-implemented Inventions

The CBI and Intellect are pleased that the European Commission is proposing a Directive on the patentability of computer-implemented inventions that would confirm the current practice of the European Patent Office in granting patents for such inventions and reduce discrepancies among different Member States.  
With one very important exception, we urge you to support the final report that Mrs Arlene McCarthy has introduced into the Legal Affairs Committee of the European Parliament, which takes the same approach. 
The important exception is Amendment 20, which would create a new Article 6a.   Although Amendment 20 purports to be about interoperability, it would actually go much further and impede innovation by restricting patents for all sorts of new products.   As the attached table illustrates, Amendment 20 affects many kinds of technologies using conversion of data, ranging from digital cameras, to aircraft guidance and control systems, and wireless connections which in future would allow a fridge to talk to a mobile phone.  Such important technologies - which are currently patentable in Europe - - would effectively be denied protection under proposed Article 6a.  Interoperability is already safeguarded by Article 6 and the 1991 computer program Directive. Therefore, we strongly urge you to vote against Amendment 20 (Article 6a).
On a particular point of detail, we believe that the Legal Affairs Committee has significantly improved upon the original Commission proposal by including in its report amendment 18 to Article 5 which allows direct patent protection for a program on a carrier (i.e. so-called “program product claims”) - which is the form in which programs are actually supplied on the market – in line with current European practice.   If the Directive removed this possibility (as in the Commission’s original proposal) it would have the effect of putting unnecessary barriers in the way of enforcing a patent.  For small companies, in particular, it is important that the enforcement of a patent should be as simple and straightforward as possible.  Therefore, we urge you to support amendment 18.
We hope you will find the attached Q&A helpful in explaining more about the content of the Directive proposal and its impact on European industry.  
If you have further questions, please contact Pamela Taylor or Jennifer Carlton.

Jennifer Carlton
Senior Programme Manager
Intellect
Russell Square House 10-12 Russell Square London WCIB 5EE
Tel: 	+44 (0) 20 7331 2000  
Mobile:	07932 636477  			
Email:	jennifer.carlton@intellectuk.org




THE COMMISSION’S PROPOSAL FOR A DIRECTIVE ON THE PATENTABILITY OF COMPUTER-IMPLEMENTED INVENTIONS
Why do we need a Directive?
Today patents in Europe are still governed by national laws1.  Without a Directive it is possible for the courts in Member States either to narrow or even extend the bounds of what is now widely regarded as best practice in Europe concerning patenting software and business methods.  The Directive sets a harmonised standard for computer-implemented inventions across all Member States, which will become even more significant with enlargement.
Who will the Directive affect?
This Directive will certainly have an impact on what we tend to think of as the traditional computer and software industries, but it will also affect on many other sectors of European industry.  This is because more and more industries are using software in their products. For example, such diverse products as mobile phones, cars and public transport, machine tools, medical instruments, consumer electronics, and household appliances – to name but a few – all use software.  In short, the Directive is important for all industries that use software in their products. 
Does the Directive extend the bounds of what is already patentable?
The Directive allays any fears about extending the boundaries of what is currently patentable by maintaining the current status quo in Europe.  The Directive creates no new rights nor any new risks, whether for big companies, SMEs or individuals.
Why patents for software?
Patents are an accepted mechanism for protecting innovative technology solutions that lead to better products for the benefit of society.  Patents provide an opportunity for reaping a return on investment in R&D and, as such, are an important incentive to continued R&D investment.
Until recently, corporate R&D was more about “hard” engineering.   Even in electronics, the building blocks were essentially tangible: electronic components, materials, devices, assemblies, circuitry, and so on.
Today businesses are spending much more of their R&D budgets on software.  For example, telecommunications companies can spend as much as 90% or more of R&D expenditure on software resources.  As a policy matter, Europe must continue to encourage investment in R&D and the development of innovative new products in today’s digital information society. The Directive would be good for innovation in strategic areas like telecommunications where Europe has a world lead.
This is the basic reason why it is so fundamentally important that Europe permits patents for computer-implemented inventions. 
The Directive will benefit both SMEs and big companies involved in software-related innovation.
Won’t this open the floodgates for patenting all software?
There has been a real concern that all software would be patentable, and this would give rise to a proliferation of patents resulting in all kinds of new problems and risks creating new economic burdens, especially for SMEs.  The Directive lays this fear to rest by establishing clear thresholds of patentability.  It is made very clear that simply implementing a solution in software does not automatically make it patentable. There has to be a genuine technical contribution to the state of the art. The Directive makes it clear that just merely doing something on a computer does not impart technical contribution.  Not only that, but  a software-related invention must also pass several other important tests or hurdles.  These same tests already apply in all other fields of technical endeavour.  That is to say, the solution must be new, non-obvious, and industrially applicable.  In short, the Directive excludes all software that does not provide an innovative technical solution.
What about business method patents and what the US is doing in this area?
There has also been a worry that Europe might drift closer to the US practice of allowing patents for business methods, and other non-technical methods and techniques. There are plenty of “bad examples” from the US, like the patent for swinging from side to side on a swing.  It is widely felt that the US has gone too far in this respect.  The Directive tackles this issue head-on by providing a special legal filter, in the form of a “technical contribution” test.  This means that all non-technical inventions are excluded.  As such pure business methods will not be patentable in Europe.
Should Europe permit “Program Product Claims” (Software on a Carrier)?
This issue has received a lot of attention.  What it boils down to is what kinds of product the patent can and cannot cover.  In particular, can you protect a computer program on a carrier, e.g. a CD-ROM or other memory device? 
This is important when it comes to enforcing the patent, because it is easier to license or sue in relation to the patented product itself.  Outlawing program product claims would mean that an unauthorised supplier is not a direct infringer making it much more difficult and in some circumstances legally impossible to enforce the patent.  On the other hand, permitting patents to cover the program as supplied, would enable the patent owner to bring an action against the supplier responsible for infringement.  Small companies, in particular, need a simple and straightforward way to enforce their patents if they are to benefit from the opportunities the Directive gives them.  Following the current practice of the European Patent Office and allowing program product claims will give them that chance.
Copyright and Patents – Double trouble?
It is true that copyright does also apply to software and some have argued that copyright alone is good enough, and ask why there is a need for patents.
But, copyright and patents in fact provide very different types of protection.  Copyright protects expression, and patents protect function or principle.  Take a very simple example, a self-assembly piece of furniture.  Copyright would stop you copying the instruction book, but it would not protect the innovative way the furniture is constructed – only a patent would do that.  In the software field, patents similarly cover the underlying principles or functionality while copyright only covers the computer program itself in the form in which it is recorded.  A patent might be the only way for a small company to prevent a larger one stealing its market for an innovative program, because it is possible to side-step the copyright by writing a new program with the same functionality as the original but which does not copy the expression.  Whereas all computer programs are protected by copyright automatically (provided they are original), the Directive ensures that patents would only be available for software which provides an innovative technical solution.  The high threshold of patentability means that relatively few software solutions will be patentable.  And, unlike copyright, patent protection is not automatic so not even all patentable software solutions will actually be patented.  In short, the Directive ensures that only the minority of technically-deserving software will fall into the patentable domain.
Don’t patents hamper interoperability?
Importantly, Article 6 of the Directive addresses interoperability, and removes any concern that patents could be used to override the provisions in the 1991 computer program Directive dealing with software copyright, which says it is not an infringement to reverse engineer in the interests of interoperability.  
So why is Amendment 20 proposing a new Article 6a so worrying?
Although Amendment 20 seemingly is about interoperability, Article 6a goes far beyond existing provisions in the 1991 computer program Directive to promote interoperability. The attached table shows just some examples of technologies which are currently patentable (assuming that they meet existing standards for patentability) in Europe but for which effective protection would be denied under proposed Article 6a.  It can be seen from the table that Article 6a would affect  holders of patents in very many industries and technology sectors including, for example, televisions & set-top boxes, mobile phones, and e-commerce. This would have damaging consequences, particularly with regard to attracting investment and incentivising innovation in these industry sectors in Europe.
 Would amendment 20 change the current state of the law in Europe?
Amendment 20 would create major new exceptions in European patent law and increase uncertainty by bringing into question the enforceability of existing and future patents. It would create new and unnecessary EU patent law, addressing potential problems of interoperability and abuse of dominant positions which are already addressed through EU competition law.
Moreover, Amendment 20 is contrary to the EU’s and Member States’ obligations under Articles 27(1) and 30 of the WTO TRIPs Agreement. 


TJF/19.08.03



Appendix: Table of technologies affected by Amendment 20 (Article 6a).

Technologies where conversion of data for communication and exchange is common
Enforceability
Current EPO Law
JURI Report
Without
Art. 6a
With Art. 6a
Television:
reception/conversion of broadcast TV signals

Set-top Boxes: 
to view satellite and digital signals on a conventional TV

Radio:
reception/conversion of broadcast radio signals

Audio (sound) Recorders:
Converting speech and music signals from one format to another

Video recorders:
Data compression for image handling

Digital Cameras:
Image processing

Mobile phones:
How a mobile phone connects to the mobile network
How speech is converted into signals so the voice can be transmitted

Mobile phone networks:
How a mobile network communicates with the fixed landline network
How a mobile network communicates with other networks

Computers:
Linking to peripheral devices, e.g. printers, displays, disk drives
Compressing data for more efficient data storage
One program talking to another program

Wireless connectivity:
Bluetooth, e.g. for connecting a mobile phone to a PDA  
Infra-red wireless connections, e.g. for connecting a printer to a laptop computer

Credit Card machines and banking terminals:
Communication between a terminal used by a customer and a central computer in the bank offices.

E- commerce and m-commerce
Security for on-line transactions using encryption 

Remote control handsets: 
Communicating with all kinds of devices (e.g. TV, garage, lights etc) around the home and office 


Yes*


Yes*


Yes*


Yes*



Yes*


Yes*


Yes*
Yes*



Yes*

Yes*


Yes*

Yes*
Yes*


Yes*
Yes*



Yes*



Yes*


Yes*

Yes*


Yes*


Yes*


Yes*



Yes*


Yes*


Yes*
Yes*



Yes*

Yes*


Yes*

Yes*
Yes*


Yes*
Yes*



Yes*



Yes*


Yes*

No


No


No


No



No


No


No
No



No

No


No

No
No


No
No



No



No


No

* but only if they meet the existing EPC criteria of industrial application, novelty, non-obviousness, and make a technical contribution.