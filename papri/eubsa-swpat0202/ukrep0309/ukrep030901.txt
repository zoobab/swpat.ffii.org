Supplementary briefing for UK MEPs – 1 September 2003

DIRECTIVE ON PATENTABILITY OF COMPUTER-IMPLEMENTED INVENTIONS (“SOFTWARE PATENTS” DIRECTIVE)

Background
In February 2002, the European Commission presented a proposal for a Directive on the patentability of computer-implemented inventions (the “software patent” directive). 
The United Kingdom and all other Member States supported the Commission’s proposal, designed to make clear the boundaries of existing patent law on software.  There was strong consensus in the Council on a draft agreed in November 2002, which aimed to improve the impact of the Directive. 

Why do we need a directive?
Patents exist to protect technical innovation. Increasingly, technically innovative manufactured products (ranging from machine tools to automobiles to household appliances) have computer programs embedded in them. The law needs updating to cope with the proliferation of these new inventions so that European industry is able to protect its investment and remain competitive with its US and Japanese rivals. As such, the directive has important implications for many sections of manufacturing industry, as well as the software, telecoms and broadcasting sectors.
In particular a directive is necessary to distinguish between inventions which use computer programs to make machines work better and “pure” software which merely manipulates abstract data within a computer. The former needs patent protection – the latter does not.  
At present the law is hazy, which is a disadvantage to EU companies operating in innovative sectors, especially SMEs.

What will the Directive achieve?
The fundamental aims of the proposed Directive are twofold:
To clarify and confirm the current legal position, and so minimise the confusion as to what can be patented, and what cannot be patented.
To put down a legislative marker to prevent any extension of the patent system towards a more permissive regime that would potentially undermine the principle that patents exist to protect technological innovation. 

What the Directive would NOT do - 
The proposed Directive would not make anything patentable that is not already patentable in the UK and elsewhere in Europe.  It would not extend patent rights to new areas, nor does it provide extra protection for patent holders. It emphatically does not follow the American model of patents for all software.

What the Directive would do -
The draft Directive would set out the current position on software and make clear – as far as is possible in a fast-changing environment – what is and is not acceptable for patenting.  
It makes clear that in order to be patentable a computer-implemented invention must make a technical contribution (in other words must make a machine work better). By providing greater certainty, it will allow software developers, owners and users a clearer legal basis on which to work.
Existing European law, while saying that software ‘as such’ is not an invention for patent purposes, is qualified by exemptions - upheld by UK and European courts – that mean that programs can be patented if they include innovation in a technical field .  The Directive would help to settle this situation in the light of 25 years’ development of software applications since the European Patent Convention came into force.

Difference between copyright and patents
Copyright is automatic intellectual property protection for the expression of a certain idea (such a song or lines of text). The text of computer programs benefits from copyright protection. The directive would not change this. The code for computer programs and other software is covered by copyright law and an earlier Copyright Directive (91/250/EEC): this also allows people to develop programs that will connect with other programs through an ‘interoperability’ article.
Patents are granted for finding technical solutions to particular technical problems – a new machine, drug or engine part. The conditions for granting a patent are much stricter than for copyright, to ensure that only truly innovative ideas are granted patent protection. 

Why are so many people worried about this directive?
Opponents of the patent system, and in particular those who consider that copyright provides sufficient protection for computer program-related inventions, maintain that the law as it stands excludes computer programs from the field of patentable inventions. Consequently they argue that by confirming that computer programs can be patented in some circumstances, the Directive would be extending the boundaries of patentability.  However, this view, which is widespread, is based on a misunderstanding of the law.
Opponents also argue that a lack of definition of the legal test of ‘technical contribution’ weakens the Directive.  However, it allows the Directive to be understood within the framework of current patent law and allows the decisions to move with changing technology: any over-specific definition would be out of date very quickly.

Contentious amendments
Art 5(2) allows programs to be protected when transferred by disk or over a network.  This is important to inventors, who need it to protect against piracy: without this article, a patented invention could be stolen by a third party and sold on to other users – and only the end-users could be subject to legal action with the third party safe from infringement action. Without Art 5(2) the Directive may be in breach of a WTO treaty since it discriminates against inventions in one field of technology.

Art 6(a) aims to extend the ‘interoperability’ provisions of earlier directives, but in effect it strips out patent protection from a swathe of industries (including telecommunications and broadcasting) by referring to “communication and exchange of data content”.  Previous directives do not attempt to say that such uses are “not considered to be a patent infringement” and the article itself appears to be designed to deal with competition and market-dominance issues rather than patent issues.  Art 6a would undermine the whole principle of the Directive and should not be supported. 


Examples of what can currently be patented and what cannot
Here are some examples of software-related innovations that would be patentable:

1.Smart vacuum cleaner that adjusts suction automatically
2.Faster calculations by linking several computer memories
3.Improved control of airbags in cars
4.Better automatic speech recognition
5.Joystick for computer games with more controls

and here are some that would not:

6.Program for managing litigation
7.Faster calculations by using a new mathematical formula
8.Improved automatic translation software
9.Simpler financial management program for SMEs
10.Database allowing improved the targeting of your constituents
11.New characters for a computer game

However, something in the second list may be patentable if, unusually, it involves technical innovation (for example, if the new games character in example 11 were displayed more realistically by a new way of controlling a computer screen).  Conversely, something in the first list would not be patentable if it involves no technical innovation. 


UK Representation the European Union
ukrep@fco.gov.uk

