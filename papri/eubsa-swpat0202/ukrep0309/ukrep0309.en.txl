<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Wec: Why do we need a directive?

#WDa: What will the Directive achieve?

#WtW: What the Directive would NOT do

#teu: What the Directive would do

#WoW: Why are so many people worried about this directive?

#WbW: UK Gov't Calls for Bans on Publication of Patent Descriptions

#eno: UK Government Calls for Bans on Conversion betweeen Data Formats

#WtW2: Examples of what can currently be patented and what cannot

#tno: Patents exist to protect technical innovation. Increasingly, technically innovative manufactured products (ranging from machine tools to automobiles to household appliances) have computer programs embedded in them. The law needs updating to cope with the proliferation of these new inventions so that European industry is able to protect its investment and remain competitive with its US and Japanese rivals. As such, the directive has important implications for many sections of manufacturing industry, as well as the software, telecoms and broadcasting sectors.

#Weo: In particular a directive is necessary to distinguish between inventions which use computer programs to make machines work better and %(q:pure) software which merely manipulates abstract data within a computer. The former needs patent protection, the latter does not.

#aak: All computer programs (1) %(q:merely manipulate abstract data within a computer) and thereby (2) %(q:make a machine work better).

#Wpo: The embedding of computer programs in manufactured goods is not new.  Data processing was already ubiquitous in the 1970s, and the European Patent Convention of 1973 does not exclude computerised manufacturing processes from patentability, if the invention lies in the realm of manufacturing (use of controllable forces of nature) rather than %(q:manipulation of data within the computer).

#ecv: If the UK government's political aim is to exclude pure software from patentability, then the proposed directive does not achieve this aim.

#eWs: It does however bring Europe's patent law in line with US and Japan, and, judging from the UK government's above-cited statement, this seems to be the real aim.  There is however no economic evidence to support the UK government's claim that the current practise of the world's three big patent offices (EPO, USPTO, JPO) is promoting innovation or competitivity in Europe or elsewhere.  Numerous studies, including those written under direct supervision of the UK patent establishment, have only produced evidence to the contrary.

#aEc: At present the law is hazy, which is a disadvantage to EU companies operating in innovative sectors, especially SMEs.

#iWn: The law is clear, but the UKPO and EPO have created a hazy situation by failing to follow the law.  The directive codifies this hazy situation, to the disadvantage of EU companies, as has been pointed out by many law scholars.

#title: UK Gov't Promoting Patent Extremism in the European Parliament

#ptW: The fundamental aims of the proposed Directive are twofold: To clarify and confirm the current legal position, and so minimise the confusion as to what can be patented, and what cannot be patented.

#rjW: If the %(q:current legal position) is unclear, then it can only be clarified, not %(q:confirmed).

#owe: Or do the authors want to %(q:confirm) questionable %(q:legal positions) of the owners of patents which they have granted against the letter and spirit of the existing law?

#eie: To put down a legislative marker to prevent any extension of the patent system towards a more permissive regime that would potentially undermine the principle that patents exist to protect technological innovation.

#jaP: Clearer markers are already in the law, and they have not prevented the EPO and UKPO from granting patents on algorithms and business methods according to the most permissive possible standards.  The word %(q:technological innovation), as used by EPO and UKPO, has no restricting meaning whatsoever.  UKPO and EPO have already reached a peak of permissivity, beyond which further extension is not to be expected.  The proposed directive would sanction this peak of permissivity, so as to prevent the pendulum from swinging back.

#cta: The proposed Directive would not make anything patentable that is not already patentable in the UK and elsewhere in Europe.

#mWt: The directive would make it difficult for courts anywhere in Europe to revoke patents granted by the current laxist standards of the UKPO/EPO.

#Wrn: It would not extend patent rights to new areas, nor does it provide extra protection for patent holders. It emphatically does not follow the American model of patents for all software.

#iwW: It merely codifies the current UKPO/EPO practise, which does not significantly differ from the US practise.

#onu: It makes clear that in order to be patentable a computer-implemented invention must make a technical contribution (in other words must make a machine work better).

#imr: Any working program makes a machine (the computer) run better.

#teW: By providing greater certainty, it will allow software developers, owners and users a clearer legal basis on which to work.

#tWh: It provides greater certainty for the owners of software patents.  This comes at the price of systematic legal insecurity for anyone who develops, publishes, sells or uses a computer program.

#gne: Existing European law, while saying that software %(q:as such) is not an invention for patent purposes, is qualified - by exemptions - upheld by UK and European courts that mean that programs can be patented if they include innovation in a technical field .  The Directive would help to settle this situation in the light of 25 years of development of software applications since the European Patent Convention came into force.

#ctu: Art 52(3) of the European Patent Convention (EPC) says that programs for computer, along with algorithms and business methods, are not inventions in the sense of patent law.  Art 52(3) explains that a non-invention (e.g. program) may well be contained in an invention (e.g. computer-controlled chemical process), as long as patent protection is not sought for the non-invention as such.  This principle has been well understood and applied by the EPO until 1986 and by many national courts until today.  Examples are:

#EWs: The UKPO has however, from its earliest days, bitterly fought the imposition of Art 52 EPC and used the %(q:as such) clause in Art 52(3) as a loophole for allowing software and business method patents, thereby rendering the whole Art 52 meaningless.  The UKPO's practise is not in line with the %(ex:usual rules of law interpretation), and there is nothing in the current law which could motivate, let alone necessitate, such a %(q:creative interpretation).  If the UKPO practise is to be imposed on Europe, such a decision should be based on sound economic policy reasons, rather than merely on UKPO precedent.

#eWl: Copyright is automatic intellectual property protection for the expression of a certain idea (such a song or lines of text). The text of computer programs benefits from copyright protection. The directive would not change this. The code for computer programs and other software is covered by copyright law and an earlier Copyright Directive (91/250 EEC): this also allows people to develop programs that will connect with other programs through an %(q:interoperability) article.

#hof: Just as patents are for %(q:inventions), copyright is %(q:creations).  The notion of %(q:creation) is better applicable to software development than the notion of %(q:invention).  By imposing patents in the field of software, the directive would weaken the property in intellectual creations which programmers have acquired over the years.  It would expose honest prorammers to blackmailing by people who were first to run to the patent office and lay claim to one of many ideas which are used in the program.

#aor: Patents are granted for finding technical solutions to particular technical problems for a new machine, drug or engine part. The conditions for granting a patent are much stricter than for copyright, to ensure that only truly innovative ideas are granted patent protection.

#iSW: If there is anything in patent law which ensures that only %(q:truly innovative ideas) are granted, then this is the requirement that there be a %(q:technical invention), i.e. a teaching based on experimenting with forces of nature, rather than on logical creation.  Software is the art of abstraction.  By sanctioning software patents, the directive in fact systematically opens the patent system for abstract ideas which, while requiring no significant investment in research and development, lay claim to an unoverseeably broad range of possible applications.

#Wlp: Opponents of the patent system, and in particular those who consider that copyright provides sufficient protection for computer program-related inventions, maintain that the law as it stands excludes computer programs from the field of patentable inventions. Consequently they argue that by confirming that computer programs can be patented in some circumstances, the Directive would be extending the boundaries of patentability.  However, this view, which is widespread, is based on a misunderstanding of the law.

#eee: Law should not be confused with caselaw.  The UK Government is doing unjustice to the large majority of %(ce:respondents of its own consultation exercise).  These software professionals are neither %(q:opponents of the patent system) nor patent law experts, but they know well enough what software patents, as granted by the UKPO, mean for innovation and productivity in the software business.  On the other hand side, some patent experts have an vested interest in questionable caselaw which they themselves have created in recent years.  By codifying this caselaw into a new written law, the UKPO and its patent policy working group, consisting mainly of corporate patent lawyers, are trying to secure questionable legal positions which they have created for themselves.

#gaW: Opponents also argue that a lack of definition of the legal test of a %(q:technical contribution) weakens the Directive.

#Wek: Indeed it the %(q:test of technical contribution) is responsible for the %(q:present legal position) at the UKPO and EPO, whereby algorithms and business methods are patentable as in the USA, with only cosmetic differences, as the members of the UKPO patent policy working group know particularly well.  See the quotations from Keith Beresford in

#vwf: However, it allows the Directive to be understood within the framework of current patent law and allows the decisions to move with changing technology: any over-specific definition would be out of date very quickly.

#hmW: The specifics of technology may change but the general principles of what should be considered patentable remain much the same.  The key question should be: %(q:was experimentation with forces of nature needed for arriving at the invention?)  The term %(q:forces of nature) is flexible enough to accomodate new technological developments.  But the Legal Affairs Committee (JURI) has rejected this test, and the UKPO group has also adamantly opposed it at every occasion.  Without this test, there is no limit on patentability.

#rle: Art 5(2) allows programs to be protected when transferred by disk or over a network.  This is important to inventors, who need it to protect against piracy:  without this article, a patented invention could be stolen by a third party and sold on to other users -- and only the end-users could be subject to legal action with the third party safe from infringement action. Without Art 5(2) the Directive may be in breach of a WTO treaty since it discriminates against inventions in one field of technology.

#Wdr: Art 5 as amended by JURI (against the recommendations of the European Commission!) prohibits the publication of descriptions of inventions in the form of program text.  It thereby severely jeopardises the free flow ideas, contradicts fundamental assumptions on which the patent system is built (such as that it should encourage diffusion of information).  In doing so, it does not even serve any practical needs of software patent owners, nor of customers, who would usually be exempted from liabilities by standard contract clauses.

#tfs: The UK Government is implying that the European Commission's proposal violates Art 27 the WTO's Treaty on Trade Related Aspects of Intellectual Property (TRIPs).  This well-known %(tr:TRIPs lie) is quite worn out.  It has been debunked by Paul Hartnack, comptroller of the UKPO, as well as by officials from the European Commission, authors of EU-commissioned legal expertises and many others at many occasions.  This is probably why the UK government no longer mentions the name of the treaty but only say %(q:a WTO treaty).

#pnW: Art 6(a) aims to extend the %(q:interoperability) provisions of earlier directives, but in effect it strips out patent protection from a swathe of industries (including telecommunications and broadcasting) by referring to %(q:communication and exchange of data content).

#toW: Previous directives do not attempt to say that such uses are not considered to be a patent infringement and the article itself appears to be designed to deal with competition and market-dominance issues rather than patent issues.  Art 6a would undermine the whole principle of the Directive and should not be supported.

#tjs: Previous directives have not dealt with the impact of patents on communication between data processing systems, because it was assumed that intellectual property in the area of data processing systems is protected by copyright only.

#teW2: In the EC Copyright Directive, interoperability was also treated as a %(q:competition and market-dominance issue), not as a %(q:copyright issue).  Upon balancing property against freedom of competition, freedom of competition prevailed.

#tor: Art 6a does the same in the case of patents.  Patents present much more serious competition problems in the first place, so that a more sweeping exemption would be justified.

#Wyh: Yet Art 6a is by no means a sweeping exemption.  It does not make a single patent invalid or worthless.  E.g. the MP3 patents could still be enforced against any action of playing music, but not against an action of converting MP3 data to a format such as Ogg or Wav, which does not require the use of patented MP3 techniques for playing.

#lwo: It should be noted that Microsoft has already applied for patents which would cover future versions of its MSWord format in Europe and there is no doubt that these would be grantable under the rules promoted by the UK government.  Moreover Microsoft has used patents to prevent conversion of its Windows Media Player format (ASF) to other formats.  Competition law provides no adequate remedies for addressing these problems.

#mll: Here are some examples of software-related innovations that would be patentable:

#aWu: Smart vacuum cleaner that adjusts suction automatically

#akt: Faster calculations by linking several computer memories

#vWW: Improved control of airbags in cars

#rWo: Better automatic speech recognition

#cWe: Joystick for computer games with more controls

#iWi: Much of this is pure software, where the problem is solved at an abstract level within a perfectly known abstract model, unrelated to controlling forces of nature.  Some of these applications, such as those related to speech data and memory managment, do not even require any equipment beyond the standard logic machine called universal computer.

#hml: and here are some that would not:

#rni: Program for managing litigation

#cii: Faster calculations by using a new mathematical formula

#vWs: Improved automatic translation software

#raW: Simpler financial management program for SMEs

#WhW: Database allowing improved the targeting of your constituents

#cft: New characters for a computer game

#res: EPO and UKPO have granted many patents on the above, and the current directive has nothing in it that could possibly exclude such patents.

#grr: It would have been more appropriate here to give examples of UKPO/EPO patents which have actually been granted and which would be rejected for lack of technical character under the proposed directive.

#ily: However, something in the second list may be patentable if, unusually, it involves technical innovation (for example, if the new games character in example 11 were displayed more realistically by a new way of controlling a computer screen).

#los: This is exactly the kind of %(q:technical innovation) which should not be considered patentable.

#mtt: Conversely, something in the first list would not be patentable if it involves no technical innovation.

#tnW: Indeed a new kind of vacuum cleaner should not be patentable if the innovation lies in programming logic (e.g. a new way of improving calculation efficiency in the micro-processor within the vacuum cleaner).  But nothing in the directive (or in the UKPO/EPO practise whose %(q:present legal position) it purports to codify) would allow any such innovation to be excluded from patentability.

#nsP: PDF file converted from MSWord.  Very similar to the Intellect (IP Lobby) briefing.

#Wti: PDF file converted from MSWord.  Fear, Uncertainty and Distrust (FUD) arguments nicely put into the form of an FAQ by Britain's leading patent lobby organisation.    Especially tries to create unfounded fears about Article 6a, which (1) allegedly kills lots of patents in various fields of technology far beyond software (2) violates TRIPs because it is only related to one field of technology.  Apart from this, the paper repeats well known and long-refuted old positions, which any professional in the field of patents knows to be false, such as necessity of program claims, undefinability of the term %(q:technical).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ukrep0309 ;
# txtlang: en ;
# multlin: t ;
# End: ;

