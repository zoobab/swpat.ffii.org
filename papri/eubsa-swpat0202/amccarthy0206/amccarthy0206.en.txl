<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: MEP Arlene McCarthy 2002-06-19: Report on the CEC/BSA Directive Proposal

#descr: In a democratic Europe, one would expect the legislative power, such as the European Parliament, to critically monitor the activities of the executive and judicial powers, such as the European Commission (CEC) and the European Patent Office (EPO), and to correct their shortcomings.  The british Labor MEP Arlene McCarthy, as a rapporteur to the European Parliament's Committee for Legal Affairs and the Internal Markt on the proposed software patentability directive COM(02) 92, however merely reaffirms the beliefs of the patent lawyers who have been dominating the dossier at the CEC and the EPO.  McCarthy's report disregards the opinions of virtually all respected programmers and economists and fails to correctly identify the controversial issues.  Yet at the end McCarthy asks a few questions which provide us with a certain hope that eventually a more open debate can be brought about.  Please read our answers below.

#Acr: Arlene McCarthy report 1

#glW: They show the rapporteur for the JURI comittee on the European Parliament is deeply ignorant of the issues at stake. In fact she seems to be worst than the Comission itself, and her only doubt is whether to allow for business methods as well.

#Acr2: Arlene McCarthy report part 2

#Lga: Lots of explanations may be needed and we are not sure she's listening. She repeats all the myths, confuses all the facts, takes case law as law, etc. We wonder how anybody can get it so wrong in just 6 pages.

#Cct: Can be used for discussing the Directive proposal.  Requires JavaScript-capable browser and registration.

#Ofe: On the DG IV (Bakels / Hugenholtz) study

#OMy: On the McCarthy Opinion

#Otn: On the McCarthy Questionnaire document

#Sia: Some Criticisms of McCarthy's Paper

#Ros: Responses to McCarthy's Questions

#Uee: Unasked Questions

#byr: based on a draft by Xavier Drudis Ferran from CALIU.org

#OWs: On the Comission's consultation too much credit is given to a %(sr:biased study of the replies).  In saying

#HuW: However, the responses received were dominated by supporters of open-source software, who believe that there should be no patents for software at all or no patents for software running on general purpose computers.

#Mon: McCarthy repeats the mistake of the study in assigning all opposition to software patents to free software advocates. Much opposition comes from non free software companies, and it is reflected in the replies to the consultation.

#ThW: The section titled %(q:current state of the law) might be better titled %(q:current EPO practice). The European Patent Office no longer follows the law (the European Patent Convention) which clearly excludes programs for computers and other immaterial innovation from patentability. It used to follow it, as reflected in its %(eg:1978 guidelines), but, starting around 1986, it gradually changed its practice and its guidelines without a change in law that would justify it. The EPO interprets the %(q:as such) appositive in EPC as a means to grant patents on %(q:technical realisations) of any kind of non-invention (including mathematical methods, business methods and computer programs), and grants or rejects patents based on claim wording instead of substance. There are guides around on how to obtain European patents for any software. The EPC would clearly not have included an exclusion for %(q:software as such) if it meant that %(q:software with a technical effect is not software as such), as the EPO postulates.  Thus, the EPO's interpretation of the EPC is tantamount to ignoring the EPC.

#Tta: The correct interpretation of the as such clause would be that no software is patentable, but using software in an invention that is otherwise patentable does not render that invention unpatentable. The innovation must lie in the realm of applied natural science (i.e. %(q:technology) in the sense of patent law).  It is not enough to use forces of nature (technical means) for a routine implentation of an innovation that lies in the field of logic. For example a chemical engineering process that needs certain changes in pressure and temperature controlled by a computer, when it was not previously known that those changes obtained the desired effects. The fact that a program controls the process does not mean that the process is not patentable.  We would not be patenting software as such, but a technical invention that happens to use a program. Everybody would be free to use the same program concepts (respecting copyright if any) for other purposes (like a virtual simulation of the process), but people would infringe when performing the chemical process with real machinery and substances. This is the useful meaning of the clause %(q:as such) in EPC art 52: you cannot patent software as such, but can patent inventions that include software when the innovation is not in information but in the material world. Thus, a program alone, on a carrier, in transmission, working on a conventional computer, or otherwise, would not infringe. It is not enough to take a conventional computer, argue that it uses electrical currents or known devices in known ways and pretend that that allows patenting any new programming idea when the application is properly drafted.

#Ted: There is a piece that is very difficult to understand for a software developper:

#Tea: To give an example, a program which, when run, makes for more efficient memory usage and so enables a computer to run faster may be claimed in the context of a patent covering the process because of the technical effect it has on the operation of the computer. In contrast, a new computer game would not be patentable at present because it has no technical effect (it would, however, be protected by copyright).

#Mre: More efficient memory usage, or a faster sorting algorithm or any other such achievement are logical rules of organization of elements of the universal computer.  The merit of any computing idea can be expressed in terms of using ressources more efficiently.  If this was an argument for patentability, perhaps even the codecision procedure of the European Parliament should have been patented, because it leads to better, less resource-consuming and faster results than some alternatives (for instance random picking of proposals until unanimity is reached for some, or an assambleary system where all of Europe's population meets together every monday).  Also, new computer games rules have traditionally been designed in view of optimal managment of information, so they could store more information in memory and achieve greater responsiveness, and it has happened often that a computer game succeded just because it used memory in a more efficient way and therbey improved user experience (including more detailed and/or faster graphics or sound). Saying that a memory optimisation is patentable and computer games are not sounds contradictory to a programmer.

#Ita: In this recent decision, the german Federal Patent Court has rejected a software patent and explained that the argument of %(q:improving memory usage) does not hold water because it would apply to all software and therefore is not compatible with the legislator's intention of excluding software from patentability, as expressed in Art 52 EPC.

#TWa: McCarthy's concept of %(q:algorithm) is also confusing:

#HtW: However, a computer program which is designed to obtain the technical effect of the patented invention and which uses the technical features claimed in the patent could indeed fall within the scope of a valid claim.[...]  Furthermore, a patent claim to an invention based on a particular algorithm would not extend to other applications of that algorithm.

#HPm: How do these sentences hold together? If the claims cover just the algorithm (by drafting the wording with references to abstract computation resources in hardware jargon, to pass the EPO's %(q:technical effect) test), then surely any attempt to apply the algortihm to another problem would infringe. The case presented would only happen with deficiently drafted applications, that choose not to cover as much as they could be granted. Maybe the misunderstanding is not seeing that software is exclusively algorithms. There's nothing more. Algorithms applied to a particular class of computer hardware.

#EWr: Even if the EPO pratice is considered compatible with %(tr:TRIPs), that is not a reason to endorse it. The techinicity criterion in old EPO practice and current EPC is TRIPs-compatible too, as the %(bs:Bakels study) (and others) explain.

#Tni: The document fails to note the traditional distiction between copyright and patents. They were not meant to overlap. Certainly you can devise a system like in the US where software can get both patents and copyright, but then software would be the only work covered by both forms of property, and patents being a stronger property would erode the protection offered by copyright.  The Bakels study recognizes implicitly the potential for those damages (SMEs prefering copyright and being threatened by patents that they could inadvertedly infringe and then being unable to exploit their copyrighted original work).

#Acl: A vociferous section of public opinion believes that there should be no patentability of software at all.  In order to be able to give proper consideration to this argument, we will need to have full information about what effects the proposed directive might have on open-source software and on the creativity and vitality of the industry.  It seems to many that the opponents of software patents have not been able to show that software with a technical effect should cease to be patentable.  The arguments against patenting software which satisfies the tests of the EPC could well be levied at patenting in general.  All patents grant a limited monopoly as a reward for invention.

#Iho: It seems that Ms McCarthy has not taken a look at any %(sp:examples) of EPO-granted patents for %(q:software with a technical effect).  The proponents of software patents have not even tried to show that any of these patents, or in fact any of the software patents granted by the EPO, could be helpful in promoting innovation or achieving any other public-policy goal.  In fact they have not pointed to any patents which are both broad enough to be useful to the patentee and narrow enough to be acceptable to the general ethics of programmers.

#chh: There is no %(q:software which satisfies the tests of the EPC), because according to the EPC computer programs are not inventions.

#Tef: There is however %(q:software which satisfies the tests of the EPO).  These tests are a joke.  The EPO's distinction between %(q:software as such) and %(q:software with a technical effect) is semantic and grammatical nonsense which can be understood best by means of satire.

#Ieu: If the EPO ceased to violate the EPC, the number of patents granted would be reduced by about 3%, as we have %(et:shown).

#Tic: These 3% of the EPO's patents show many distinctive features, against which most of the critical arguments are specifically directed.

#Eay: Even if the argument was in fact %(q:levied against patenting in general) and not only against 3% of the patents, that would not make it less valid.  Patenting in general must always be justified anew.  In a liberal polity and economy, the burden of proof is on those who advocate restrictions on freedom.  Restrictions that were legitimate yesterday may be illegitmate today.

#Wxn: What McCarthy calls a %(q:vociferous group) is a large majority of concerned software creators and the public at large.   If McCarthy feels that this group is wrong and the software patentability proponents are right, a certain burden of proof falls on her shoulders.  McCarthy should explain how software patentability will improve overall productivity during the next 20 or 50 years.  Arguments about the current %(q:status quo) of patentability caselaw, however valid they may be, are as much beside the point as would be argumentations about the %(q:status quo) of economic policy.   Unemployment does not become better by the fact that it is resulting from a certain %(q:status quo) of malpractise in state agencies designed to reduce unemployment.  If a change of this malpractise affects the vested interest of a certain group, then that is tough luck.

#MWi: Moreover, open-source software has flourished alongside the existing patenting regime in recent years. Again, under the present patenting regime many firms have invested billions of euros in R&D ­ investments founded on, among other things, the availability of patents.  It is argued that it would be very unwise to jeopardise this investment by reducing patentability of software.

#Meg: Many firms have, until very recently, trusted the law which states that software innovations are not patentable, and based their investments on this trust.  Even big companies such as SAP were until 1997 of the opinion that software is not patentable and did not unfold any patenting activities.  The big hardware companies whose patent lawyers have been pushing for software patentability at the EPO are not necessarily the leaders of innovation in software.  They certainly did invest a lot in illegal EPO patents which they now would like to legalise, but such investments do not legitimate any posession claims.   By acknowledging the %(q:posessions) of these companies, McCarthy is proposing to take away the same posessions from developpers and citizens, to destroy the copyrighted property and their rights to fair access and competition on the market place, which have formed the basis upon which perhaps even more %(q:billions of euros in R&D) were invested.

#TWu: The European Parliament is taking a decision to create an appropriate order framework for the next 20-50 years, not a decision about how to maintain the %(q:status quo) and defend some group's posessions.  What is important is which kind of posession will be more productive for society as a whole.

#Mte: McCarthy's %(q:status quo) logic should ring a warning to the European Parliament:  be careful not to sanction any protectionist claims, because once you have sanctioned them, it will be very difficult to get rid of them.   Even when the protectionist claims have been sanctioned only by blatantly illegal patent office decisions, politicians like McCarthy will be very sensitive to the interests of incumbent protectionists and insensitive to the public's freedom interests.  This is because protectionist interests are concentrated and organised, while freedom interests are dispersed.   While the protectionist lobby will be identified with %(q:the industry), the freedom lobby will tend to be disqualified as a %(q:vociferous group), ideological movement etc, even when it enjoys strong support among the concerned professionals and businesses.

#OWr2: On the other hand, patents impose indirect regulatory and other costs and there is always a trade off between freedom and intellectual property.  These considerations would suggest that the burden of proof is very much with the minority of persons who wish to extend the boundary of what is patentable (e.g. in the direction of patentability of business methods).

#ToW2: The burden of proof is on those who advocate a restriction of freedom.  This group is in fact a minority, and it is proposing to change the law.  But even if this was not so, the burden of proof would still on their shoulders.  Freedom is a basic constitutional principle of European polity and economy.

#TWn: There is no special controversy about %(q:business methods).  Business methods have, just like other software ideas, regularly been granted patent protection by the EPO against the letter and spirit of the written law, and the Directive proposal would legalise that practise.  Within the patent law community there may be a minority of people who propose a whole-hearted commitment to business method patents, while the majority prefers to keep quiet or to engage in a virtual debate about business methods while silently continuing to patent all kinds of logics, including business methods.  This type of debating manoevre is called %(q:shift of focus) in rhetorics.  By conducting exorcist rituals against an undefined concept called %(q:business methods), the software patent lobby is trying to create the impression that it has the adverse effects of software patents under control and is able to confine them to some US-specific quarantene area which european politicians needn't worry about.

#FWp: For your rapporteur, what is important above all is legal certainty.  The European Patent Convention is now over 20 years old and the consultations held by the European Commission indicate that legal certainty needs improving.  Otherwise some will incur the costs of submitting patent applications with little chance of success, while there is evidence that additional complexity is helping to deter others, particularly SMEs, from seeking patents at all, which means they may be missing financially advantageous opportunities.  Uncertainty tends to drive up costs at every part of the patenting system, making infringement harder to predict and disputes more likely.  In addition the complexity of intellectual property rules may act as a barrier to entry to the market.

#Wsl: When you advocate a bad regulation, one last justification is that it creates %(q:legal certainty).  A bad but predictable regulation may in fact sometimes better than none at all, just as dictatorial rule may sometimes be deemed better than anarchy.

#Lua: However the proposed directive, while sanctioning EPO supremacy in patent matters, would add complexity and diminish overall legal certainty.

#Wem: While the limits of patentability would become more uncertain than ever as some lawcourts demand a %(q:technical contribution) while others look at the technicity of the %(q:invention as a whole), there would no longer be any secure legal grounds for revoking the 30000 illegal software and business method patents granted by the EPO.  From the patent owner perspective, which McCarthy seems to prefer, this may look like %(q:increased legal security).

#Ikq: In a way, legal uncertainty has always been an integral part of the patent system.  The patent system is necessarily based on fuzzy concepts such as %(q:novelty) and %(q:inventive step).  When patent inflation runs out of control, legal uncertainty is increased.  This drives up the cost risks associated with prosecuting patents and thus weakens each patent.  Legal security is best served by insisting on high standards of patentability.

#sWe: source of questions

#sea: source of answers

#bbr: based on a submission by Erik Josefsson

#Hrw: How the BladeEnc project was shut down by patent threats

#IeA: Is such a directive necessary or can we just leave matters to the Boards of Appeal and the national patent courts?

#Wih: Will the directive achieve its ends, in particular without unwanted side effects?

#DtW: Does Art. 5 run counter to TRIPs in imposing artificial restrictions on the type of claims permissible in this field?  Is it intended to be retroactive?  If so, with what effects?

#IeW: Is the proposal legally watertight (legal certainty)?

#IWy: Is there any merit in following in the footsteps of the USA (patentability of business methods) or should patentability of business methods be clearly and specifically precluded?

#Sye: Should the system of (compulsory) licences be reviewed to prevent abuse of the patent system?

#Ito: Is the issue of trivial patents a problem?  If so, how should it be addressed?

#Wwo: What risk, if any, is posed to open-source software?  If so, how is it that open-source and proprietary software seem to co-exist at present?

#IWe: Is it possible to argue that patents may stifle innovation, if so how?

#Imi: Is it possible to quantify in economic terms/employment the benefits/disbenefits of software patentability?

#Waa: What impact, if any, will action in this area, one way or another, have on SMEs?

#WhW: What is the impact of TRIPs?

#TWt: The EPC is not limited to EU countries.  What are the implications of this in the event that the directive is adopted?

#Dbu: Do recital 18 and Art. 6 of the proposal for a directive need to be reworded in order to allow decompilation of programs for the purposes of interoperability as is permitted under Directive 91/250?

#TnW: Let's reformulate into several questions.

#CWl: Can we just leave matters to the EPO's boards of a appeal and national patent courts?

#Ioe: Is there a problem of divergent national rules which needs to be addressed by a directive, as CEC/BSA claim?

#Amc: Are the other proclaimed goals of the directive proposal valid?

#TgW: The EPO, in particular its Management and its Technical Boards of Appeal, can not be trusted.

#Tni2: The EPO has, since 1986, gradually dismantled the borders of patentability with regard to software and, against the letter and spirit of the current law, authorised the granting of patents on mathematical methods, intellectual methods, computer programs and presentation of information as such.  It has also violated EPC provisions on novelty, inventive step, separation of search and examination, biological exclusions and various other issues.  It has regularly placed its own will above the will of the legislator.

#Imm: In the case of Art 52, the systematic violation has been perpetrated by the Technical Boards of Appeal in collusion with the EPO managment, which again seems to rely very much on the advice of %(q:industry representatives), i.e. delegates from patent departments of large companies, on the Standing Advisory Committee of the EPO (SACEPO).  The responsible court within the EPO, the Enlarged Board of Appeal, was not consulted during this whole questionable process.

#Ty1: This is remarkable, while there are some well documented cases of completely contradictory interpretations of EPC 52, and the TBAs are obliged by EPC 112 to forward important questions of interpretation to the EBA.

#Eye: Evidently, the organisational framework of the European Patent Convention is not working as it should.  According to the original philsophy, the EPO should have followed the national jurisdictions, which again should have conservatively followed the harmonised law.  Through a constant discussion, decisions of the EBA and occasional revisions of the EPC, it should have been possible to hold the system together.  Unfortunately, in reality, power gravitated to Munich, where the EPO management and its dependent judges on the Technical Boards of Appeal, reassured by support from %(q:industry representatives) in the Standing Advisory Committee (SACEPO), have, as one European Patent Attorney %(ss:put it), %(bc:promoted themselves into %(q:radiant lights of the people of Europe), into %(q:flowers of European patent knowledge), into ultimate sources of wisdom in matters like patenting software.  Nowadays, the buying of any diskette containing SW might constitute patent infringement, but this far-reaching consequence is only based upon decisions of one TBA which had no authority whatsoever to issue binding decisions on this basic matter without first referring the question to the EBA.)

#Trt: The Commission says that the proposed directive is needed because national and EPO court verdicts are divergent. However, all EU member countries already have identical laws with respect to the patentability of computer programs. Further harmonization of national patent laws on this issue is actually not possible.

#Tse: There are other areas of patent law, such as the regulations on what constitutes an infringement, where significant differences between national legal systems exist.  Such differences have not been addressed by any initiative from the European Commission so far.  Nevertheless lawcourts in various European countries have, through consensus formation in the world of law experts, worked toward common standards even in cases where the national legal systems are different.

#Tnt: The problem which the directive really tries to address is not that of national divergence but that of ensuring the validity of 30000 software and business method patents illegally granted by the European Patent Office.  It addresses this problem by introducing various unclear concepts so as to make it difficult for any court to reject any patent on the grounds of not being an invention, without however ensuring that national jurisdictions converge.

#Tti: The alleged problem of a lack of innovation incentives or of too easy imitation in the software industry has not been substantiated and does not seem to exist.

#Toc: The alleged need of the software (or telecommunication or electronics) industry for %(q:protection) by logic patents has not been substantiated and does not seem to exist.

#TWW: The alleged urgency of the issue does not exist: so far software patents have had only a very small impact in the European software economy, and national differences in patent jurisdiction have even less of an impact.  Nevertheless in the long term, the impact of allowing or not allowing software patents may be quite significant.

#Tim: The alleged demands by an %(q:economic majority) of industry representatives do not exist.  What does exist is a community of corporate patent lawyers who have for decades been dominating the work of %(q:industrial property committees) in various industry associtations and, in that role, reiterate unfounded belief statements which they learned in law school, such as that %(q:patents are needed to stimulate innovation), %(q:SMEs badly need patent protection), %(q:patents are needed for technology transfer) etc.  Usually few in the industry, especially among SMEs, cared to refute any of these mantras.  Especially in the software field, there seemed to be no need to worry about patents.  However, in spite of the dominant role of industrial patent lawyers in industrial associations, some major organisations have pronounced themselves against software patents, as have 120,000 supporters and 300 corporate sponsors of the Eurolinux Alliance.

#Yto: Yes, there is a good case for ending the EPO dictatorship and bringing the European patent system under control of an accountable democratic body, possibly under the auspices of the European Parliament.  A directive is one of several possible ways to achieve this.

#UWe: Unfortunately the CEC/BSA directive proposal achieves just the opposite:  it aims to end once for all the embarassing discussions about Art 52 EPC and to make sure that in the future the EPO management is free to de-facto-legislate as it pleases.

#ItW: It will mostly achieve the opposite of what it claims to be aiming for.

#Teo: The proposal says it wants to exclude patents on business methods, but gives no legal structure to achieve this.   A %(sa:simple analysis based on a few example patents) should suffice to show that the directive as proposed would legalise the mass of business method patents already granted by the EPO, but few of the responsible people seem willing to take a close look.  It is difficult to have a debate when the central terms have not been properly defined, or worse, designed to have double meanings.

#Apa: As stated above, the objective of harmonizing the national patent laws cannot be met, since the national patent laws cannot be more harmonized on this issue than they already are.

#TtO: The overall objective of stimulating innovation is very unlikely to be met.  %(si:Economists) have always been skeptical about the patent system's claimed ability to stimulate innovation, and in particular there is no ground for assuming that masses of trivial and broad software and business method patents, which this study would make irrevocably enforcable, will do any good.  All economic studies conducted so far have concluded that software patents have a more or less negative impact on innovation.

#Adi: Art 27 TRIPs says that patent should be available for inventions in all fields of technology.  By stating that %(q:computer-implemented inventions belong to a field of technology) the directive proposal makes this non-discrimination provision of Art 27 TRIPs applicable to software.

#Cti: Claims to information objects are generally disallowed according to the proposal.  This limitation is not a special regulation for one %(q:field of technology) but applies to all fields.  E.g. a claim to an operation instruction for a chemical process, be it in the form of an instruction manual, a computer program or a patent description, would also not be allowable.  Allowing information claims would plunge the patent system into contradictions.  Disallowing them is not motivated by the kind of industry-specific trade protectionism which TRIPs was designed to avoid.

#IWl: If Art 27 TRIPs can be construed to require that any useful implementation of the patented idea must be claimable, then a claim to a %(q:thought process in the human mind, characterised by that ...) would also have to be allowable.  All rules of organisation and calculation are primarily thought processes, devised to run in a person's head and on a mathematical model called the Turing Machine / the Universal Computer.

#IdW: It is inconsistent with the basic underlying assumptions of patent law to patent advances in abstract reasoning in the first place.  But once abstract reasoning is made patentable under the guise of computer processes, an %(q:artificial restriction) on what can be claimed must be drawn somewhere.  It is a normal principle in law that any privilege, including such as conferred by Art 27 TRIPs, will find its limitations in some higher public value, such as e.g. the freedom of thought or the freedoms associated with the written word.

#ToW: The exclusion of program claims in the current directive proposal is based on such necessities, and that should be made clear in the text.  The infringement regulations of the EU member states are strongly divergent and if harmonisation is needed anywhere, then it is here.  Member States should also be obliged to ensure that the distribution of a program on a carrier cannot constitute a patent infringement.  The current proposal is half-hearted on this point and thereby invites patent lawyers to attempt eroding it by means of over-stretched interpretations of Art 27 TRIPs.

#Tcn: The CEC/BSA draft explains the ban on information claims as a consequence of Art 52 EPC.  Patent claims that were granted against Art 52 cannot be considered to be legally valid in any european country.  Information claims in current EPO patents are just as invalid as claims to processes for which prior art exists.

#Tac2: The proposal contains many fuzzy concepts and contradicts Art 52 EPC.

#Ina: It does not make anything clearer, but people will no longer have any solid legal basis for refusing patents on anything for which the EPO grants patents, including computerised business methods of the %(q:Amazon One-Click) type.

#Eaa: Economists in the US and Europe agree that there is no merit in allowing business method patents.

#TWe: The same applies to software patents in general.

#Teo2: The concept of %(q:business method patent) is poorly defined.  One proposed definition is %(q:software patent which has become a political problem because not only programmers complain about it).

#Awl: Any attempt to exclude %(q:business method patents) but allowing software patents is likely to be nothing but a political maneuvre, with little effect on what is patented in the end.

#BtW: By claiming to exclude business method patents, the lawmaker risks to achieve the worst of both worlds: %(ol|actually excluding nothing|deterring european SMEs from obtaining business method patents.)

#TuW: The minimum requirement for any directive is that it should speak an honest, straight-forward language: properly name what it excludes and properly exclude what it names.

#CWi: Yes, certainly, as many other modalities of the patent system.

#Tcl: There are however limits to what can be achieved by compulsory licensing, especially in a fast-moving area such as software.

#Taa: Most software patents are trivial.

#Tct: Trivial software patents can be avoided only by not granting softare patents.

#BWE: A skilled programmer tends to find several (or many) EPO-patentable ideas every day.  Although his programming work leads to highly complex non-trivial works, any ensuing patents will tend to be trivial and broad.

#Ano: A software patent that is neither trivial nor broad is probably just an expensive form of copyright.  Nobody would want to apply for a %(q:good software patent), because such a patent would not benefit its owner.

#TWs3: The software world, including open-source software, is coexisting with the patent world at present, but it is incurring substantial damage thereby, even in Europe.

#BWy: There is an increasing number of free/opensource projects being threatened, forced to retreat and sometimes shut down by patent holders.

#FeW: Free and proprietary software have both co-existed in symbiosis under common copyright rules for many decades.  Patents undermine these copyright rules and are harmful to both free and proprietary software.  There is no antagonism between free and proprietary software, only one between patents and software.

#Ynp: Yes.  Economists have always cautioned that patents in general are likely to stifle innovation, and for patents on information innovation this is self-evident to anyone who cares to read the claims and description.

#Srl: Some attempts have been made.  Of course they are just more or less good guesses.  The available data are not cogent and can perhaps never be.

#OfW: One of the reasons for the difficulty lies in the opacity of the patent litigation business.

#Bda: Bessen & Maskin have asserted that software R&D decreased due to the introduction of software patents in the US and tried explained this finding by a game-theory model.

#Ite: Iain Cockburn has shown that an approved patent is not visible in stock market value (new press release/commercial generates 7% increase).

#Spr: Software patentability is likely to create employment in the field of patent litigation. The Commissions analysis of the replies to the hearing it held on this issue also shows that 44% of replies with a positive attitude towards software patents come from IPR professionals.

#Ino: In Sweden, a professional handling of the reception alone of one patent infringement claim (valid or invalid) per company per year in the SME company sector 1-20 employees, will cost that segment 60 million Swedish crowns per year. Even for larger companies the burden of patent administration is worrisome.

#Tnu: This may be offset by fiscal uses of patents.  Unlike copyright, patents are counted on the books as assets.  In some countries, such assets and dividends as well as incentives payed thereby enjoy preferential tax treatment.  Even without such treatment patents are regularly used to evade taxes and transfer much of the cost of patents to the state.  With logic patents, this tax evasion game becomes accessible to the masses.

#Tnb: The growth of patent thickets will reduce the number of players on the market.  Capital concentration may be viewed a good thing, but in the software industry it is unlikely to improve productivity.  Much of the productivity of this sector is an unplanned side effect of having many players on the market.  Finding new algorithms itself is not expensive and does not require capital concentration.

#TWo: The impact of TRIPs, if the directive proposal is adopted, works toward unlimited patentability under a uniform 20 year term and other unflexible conditions. This is due to articles 27 and 32 in the directive and to the tendency of the patent establishment in Europe to interpret TRIPs extensively.  If Europe wants to retain freedom to experiment with more flexible software property regulations, it must, contrary to the directive proposal, ensure that computer-implemented rules of organisation and calculation (software and business ideas) are not considered to be %(q:inventions in a field of technology) pursuant to Art 27 TRIPs.

#Bco: Because the directive is at odds with the letter and spirit of Art 52 EPC, it would force EU countries to break a treaty or force an incoherent interpretation of this treaty on partner countries.  Some countries could refuse to go along with this interpretation.

#TrW: The coexistence of the EPC system with the EU creates a lot of problems and makes the EPO extremely difficult to control.   By uncritically codifying EPO practise and in effect issuing a carte blanche to the EPO, the EU would make it more difficult than before to establish any control over the EPO.  Power would further gravitate toward this black hole in the European constitutional order.

#IWv: If a completely new original and independent computer program can infringe on a patent on a %(q:computer-implemented invention), we can't see how a version arising from decompilation of a patented computer program would not.

#TWe2: The whole idea of patenting computer programs is incompatible with the decompilation and interoperability provisions of the copyright directive.

#TdW: The basic question of how to limit patentability remains unasked.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amccarthy0206 ;
# txtlang: en ;
# multlin: t ;
# End: ;

