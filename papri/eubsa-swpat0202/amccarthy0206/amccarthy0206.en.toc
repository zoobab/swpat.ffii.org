\contentsline {chapter}{\numberline {1}Some Criticisms of McCarthy's Paper}{5}{chapter.1}
\contentsline {chapter}{\numberline {2}Responses to McCarthy's Questions}{10}{chapter.2}
\contentsline {section}{\numberline {2.1}Is such a directive necessary or can we just leave matters to the Boards of Appeal and the national patent courts?}{10}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Can we just leave matters to the EPO's boards of a appeal and national patent courts?}{10}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Is there a problem of divergent national rules which needs to be addressed by a directive, as CEC/BSA claim?}{11}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Are the other proclaimed goals of the directive proposal valid?}{11}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Are there any valid goals for which a directive might be necessary?}{11}{subsection.2.1.4}
\contentsline {section}{\numberline {2.2}Will the directive achieve its ends, in particular without unwanted side effects?}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}Does Art. 5 run counter to TRIPs in imposing artificial restrictions on the type of claims permissible in this field? Is it intended to be retroactive? If so, with what effects?}{12}{section.2.3}
\contentsline {section}{\numberline {2.4}Is the proposal legally watertight (legal certainty)?}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}Is there any merit in following in the footsteps of the USA (patentability of business methods) or should patentability of business methods be clearly and specifically precluded?}{13}{section.2.5}
\contentsline {section}{\numberline {2.6}Should the system of (compulsory) licences be reviewed to prevent abuse of the patent system?}{13}{section.2.6}
\contentsline {section}{\numberline {2.7}Is the issue of trivial patents a problem? If so, how should it be addressed?}{13}{section.2.7}
\contentsline {section}{\numberline {2.8}What risk, if any, is posed to open-source software? If so, how is it that open-source and proprietary software seem to co-exist at present?}{14}{section.2.8}
\contentsline {section}{\numberline {2.9}Is it possible to argue that patents may stifle innovation, if so how?}{14}{section.2.9}
\contentsline {section}{\numberline {2.10}Is it possible to quantify in economic terms/employment the benefits/disbenefits of software patentability?}{14}{section.2.10}
\contentsline {section}{\numberline {2.11}What impact, if any, will action in this area, one way or another, have on SMEs?}{15}{section.2.11}
\contentsline {section}{\numberline {2.12}What is the impact of TRIPs?}{15}{section.2.12}
\contentsline {section}{\numberline {2.13}The EPC is not limited to EU countries. What are the implications of this in the event that the directive is adopted?}{15}{section.2.13}
\contentsline {section}{\numberline {2.14}Do recital 18 and Art. 6 of the proposal for a directive need to be reworded in order to allow decompilation of programs for the purposes of interoperability as is permitted under Directive 91/250?}{16}{section.2.14}
\contentsline {chapter}{\numberline {3}Unasked Questions}{17}{chapter.3}
