<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: If these conditions are not fulfilled, the directive should be rejected (i.e. sent back to the Commission or to the Parliament for reworking).
title: Conditions for Rejection
dmj: The directive must be rejected
eli: if freedom of publication is impaired
anW: e.g. by allowing %(pc:program claims) or failing to state clearly that publication of text cannot constitute a patent infringement, as done e.g. in %(i3:ITRE amendment 13)
htp: if the right to interoperate (i.e. to convert data between different representation conventions) is impaired, e.g. if %(a6:Art 6a) is weakened.
dWe: if the directive stops short of excluding algorithms, business methods and programs for computers (= %(q:computer-implemented inventions) = patent claim objects consisting of general purpose data processing equipment and rules for operating it) from patentability.
Wci: if the directive makes patentability hinge on abstract terms which are open to interpretation by caselaw.  In particular, the word %(q:technical), if used, must be defined by reference to %(q:controllable forces of nature), and the word %(q:industrial), if used, by %(q:production of material goods).
deW: if the word %(q:computer-implemented invention) is used in the directive.
weW: This term means nothing but %(q:program for computers in the context of a patent claim).  It was introduced by European Patent Office (EPO) in 2000 in attempt to legitimate %(q:computer-implemented business methods) and bring Europe in line with the %(q:Trilateral Standard) of the US, Japanese and European Patent Office.  Please read the definitions of the European Commission (CEC)'s Article 2 and the EPO' %(tw:Trilateral Document Appendix 6). Don't be misled by secondary information!  A computer-controlled washing machine is %(q:not) a %(q:computer-implemented invention) in the strict sense of the term.
iep: If the directive text is contradictory, e.g. if the aims laid out in the recitals do not support the means stipulated in the articles.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/plen0309.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: plenkond0309 ;
# txtlang: en ;
# End: ;

