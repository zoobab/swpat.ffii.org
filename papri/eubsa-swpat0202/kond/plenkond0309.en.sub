\begin{subdocument}{plenkond0309}{Conditions for Rejection}{http://swpat.ffii.org/papers/eubsa-swpat0202/kond/index.en.html}{Workgroup\\swpatag@ffii.org}{If these conditions are not fulfilled, the directive should be rejected (i.e. sent back to the Commission or to the Parliament for reworking).}
The directive must be rejected
\begin{itemize}
\item
if freedom of publication is impaired (e.g. by allowing program claims\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/prog/index.en.html} or failing to state clearly that publication of text cannot constitute a patent infringement, as done e.g. in ITRE amendment 13\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/juri0304/index.en.html\#art5})

\item
if the right to interoperate (i.e. to convert data between different representation conventions) is impaired, e.g. if Art 6a\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/itop/index.en.html} is weakened.

\item
if the directive stops short of excluding algorithms, business methods and programs for computers (= ``computer-implemented inventions'' = patent claim objects consisting of general purpose data processing equipment and rules for operating it) from patentability.

\item
if the directive makes patentability hinge on abstract terms which are open to interpretation by caselaw.  In particular, the word ``technical'', if used, must be defined by reference to ``controllable forces of nature'', and the word ``industrial'', if used, by ``production of material goods''.

\item
if the word ``computer-implemented invention'' is used in the directive. (This term means nothing but ``program for computers in the context of a patent claim''.  It was introduced by European Patent Office (EPO) in 2000 in attempt to legitimate ``computer-implemented business methods'' and bring Europe in line with the ``Trilateral Standard'' of the US, Japanese and European Patent Office.  Please read the definitions of the European Commission (CEC)'s Article 2 and the EPO' Trilateral Document Appendix 6. Don't be misled by secondary information!  A computer-controlled washing machine is ``not'' a ``computer-implemented invention'' in the strict sense of the term.)

\item
If the directive text is contradictory, e.g. if the aims laid out in the recitals do not support the means stipulated in the articles.
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/plen0309.el ;
% mode: latex ;
% End: ;

