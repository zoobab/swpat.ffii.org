Betr.:	Vorschlag der Europ�ischen Kommission �ber die Patentierbarkeit
computerimplementierter Erfindungen vom 20.�Februar 2002

Sehr geehrter Herr Pr�sident,

zu dem Richtlinienvorschlag der Europ�ischen Kommission nehme ich wie
folgt Stellung:

1. Grunds�tzlich zuzustimmen ist dem Ausgangspunkt der Kommission, da�
es im Hinblick auf patentrechtliche Behandlung computerimplementierter
Erfindungen einer gemeinschaftsweiten Regelung bedarf. Auch wenn die
Kommission im einzelnen die Differenzen �ber deren Behandlung in den
Staaten der Gemeinschaft �bersch�tzt und hier insbesondere die
Rechtsprechung des Senats nicht v�llig zutreffend gew�rdigt hat, ist
nicht zu verkennen, da� computerimplementierte Erfindungen in der
Gemeinschaft unterschiedlich behandelt werden mit der Folge, da� in
einzelnen L�ndern ein Patentschutz gew�hrt wird, den andere versagen.
Das mu�, wie die Kommission zutreffend hervorgehoben hat, zu St�rungen
und Irritationen im innergemeinschaftlichen Handel f�hren und damit
zumindest die Grundfreiheiten des Vertrages ber�hren.

2. Bedauerlich ist jedoch, da� die Erw�gungen zur Richtlinie jegliche
Auseinandersetzung mit den unterschiedlichen Standpunkten zur
Patentierbarkeit von Software vermissen lassen. Aus ihnen geht nicht
hervor, da� sich die Kommission mit den gegen eine solche Patentierung
sprechenden Gesichtspunkten �berhaupt auseinandergesetzt hat. Sie
ersch�pfen sich weitgehend in einer kritiklosen �bernahme der Argumente,
die von seiten der an einem solchen Patentschutz interessierten Kreise
in der Wirtschaft, insbesondere den gro�en Unternehmen der klassischen
Software-Branche, erhoben werden.

In welchem Umfang die Wirtschaft auf Patente mit welchem Inhalt wirklich
angewiesen ist, bleibt weitgehend ungekl�rt. Nicht zu verkennen ist
zwar, da� insbesondere f�r kleinere Unternehmen und freie Entwickler der
Patentschutz eine der wenigen Sicherheiten bieten kann, auf deren
Grundlage die Aufnahme von Fremdmitteln m�glich ist. Nicht gesichert
erscheint derart jedoch, da� es sich dabei um eine Frage von Gewicht
handelt, zumal die Patentierung ihrerseits mit einem nicht
unbetr�chtlichen finanziellen Aufwand verbunden ist, der die
Sinnhaftigkeit einer darauf aufbauenden Kreditaufnahme bereits wieder in
Frage stellen kann. Da� die gro�en Unternehmen auf einen Patentschutz
der Kreditgrundlage angewiesen sind, erscheint eher unwahrscheinlich.

Ein Argument von Gewicht kann die Rechtslage in anderen Industriestaaten
sein, die es wegen des dort bestehenden Patentschutzes und der fehlenden
M�glichkeit wechselseitiger Lizenzierung europ�ischen Unternehmen
erschweren kann, auf den dortigen M�rkten Fu� zu fassen. Auch insoweit
ist bislang jedoch nicht gekl�rt, in welchem Umfang hier tats�chlich
Probleme entstehen oder auch nur zu erwarten sind. Im Raum steht nur die
Behauptung international t�tiger Unternehmen aus solchen ausl�ndischen
Wirtschaftsr�umen, da� hier ein Problem bestehe.

Eine Patentierung von Software setzt vor dem Hintergrund der beteiligten
wirtschaftlichen Interessen differenzierte L�sungen voraus, die die
einseitig auf einen Teil dieser Interessen ausgerichtete Richtlinie
nicht erkennen l��t. Die dort vorgeschlagene L�sung ist aus mehreren
Gr�nden gef�hrlich:

a) Nach dem Inhalt der geplanten Richtlinie bleibt unklar, was jeweils
in einem Patent unter Schutz gestellt werden kann. Die zugrundeliegenden
Erw�gungen legen ebenso wie der Text der Richtlinie selbst die Vermutung
nahe, da� insoweit an die Praxis des Europ�ischen Patentamts und seiner
Beschwerdekammer sowie derjenigen Staaten angekn�pft werden soll, in
denen sich ein Patentschutz f�r Software in gro�em Umfang entwickelt
hat.

Die Praxis des Europ�ischen Patentamts und ebenso des Deutschen Patent-
und Markenamts begr�ndet die Gefahr, da� in zunehmendem Ma�e
Entwicklungen patentiert werden, die als Bezug zur Technik lediglich
aufweisen, da� ein aufgabenhaft beschriebenes Anliegen mit Hilfe eines
Computers bew�ltigt werden soll, ohne zugleich eine konkrete technische
L�sung anzugeben. Ein Beispiel hierf�r ist -�aus der deutschen Praxis�-
die der Entscheidung "Automatische Absatzsteuerung" des
Bundespatentgerichts (GRUR 1999, 1078) zugrundeliegende Anmeldung. Diese
gibt verschiedene Schritte an, mit denen ein Kaufmann die Daten seines
Lagers erfa�t und im Sinne einer Beschleunigung des Absatzes auswerten
kann; der Bezug zur Technik besteht aus dem allgemeinen Hinweis, da� die
Erfassung, Speicherung und Auswahl der Daten "elektronisch" erfolgen
soll. In welcher Weise dies zu geschehen hat, ist der Anmeldung
allenfalls bedingt zu entnehmen. Bezeichnenderweise ist die Anmeldung im
wesentlichen daran gescheitert, da� die kaufm�nnischen Schritte zum
�blichen Handwerkszeug eines Kaufmanns geh�rt haben; die fehlende
Offenbarung einer technischen Lehre hat in diesem Zusammenhang keine
Rolle gespielt.

Schreibt man diese Praxis fort, wird in zunehmendem Ma�e damit zu
rechnen sein, da� Patente f�r an sich vom Computer unabh�ngige
gesch�ftliche oder sonstige Ideen allein deshalb erteilt werden, weil
ihre Umsetzung nach der Vorstellung des Anmelders und dem Inhalt seiner
Anmeldung mit Hilfe eines Computers geschehen soll, ohne da� zugleich
eine dem entsprechende konkrete technische L�sung offenbart wird. Das
wird im Ergebnis die Benutzung des Computers im gewerblichen Bereich in
einer mit dem Zweck des Patentschutzes nicht mehr zu vereinbarenden
Weise beschr�nken. Angesichts der Formulierung des Patentanspruchs in
dem oben angef�hrten Beispiel aus der Rechtsprechung des
Bundespatentgerichts w�re es jedenfalls nicht sicher auszuschlie�en, da�
ein Kaufmann, der die ihm gel�ufigen kaufm�nnischen Schritte mit Hilfe
einer �blichen Tabellenkalkulation umsetzt, damit in den Schutzbereich
des Patents eingriffe und mithin patentverletzend handeln w�rde.

b) Hiermit im Zusammenhang steht die weitere �berlegung, da� die
Gew�hrung von Patentschutz teilweise ihren Zweck verfehlt. Nach
herk�mmlichem Verst�ndnis ist sie auch eine Belohnung daf�r, da� der
Erfinder seine Entwicklung bekannt gibt und der �ffentlichkeit zur
Verf�gung stellt; weil diese sie dann aufgrund des gewonnenen
Erkenntnisstandes benutzen k�nnte, wird ihm f�r die Dauer des
Patentschutzes ein Ausschlie�lichkeitsrecht gew�hrt, das andere f�r
diese Zeit von dieser Benutzung ausschlie�t. Im Bereich der
computerbezogenen Erfindungen, soweit sie sich -�wie mittlerweile in
erheblicher Zahl�- auf die oben bezeichnete aufgabenhafte Beschreibung
beschr�nken, findet eine dem entsprechende Offenbarung nicht statt. Der
Quellcode des Programms, das der Umsetzung dieser Idee dient, wird in
der Regel nicht ver�ffentlicht, sondern als Betriebsgeheimnis behandelt,
nicht nur, um Nachahmungen zu erschweren, sondern -�wie insbesondere das
Beispiel Microsoft zeigt�- Dritten nicht alle Informationen zu
vermitteln, die diese in die Lage versetzen, auf dem Programm aufbauende
weitere Software zu entwickeln. Soweit die amerikanische
Software-Industrie von den -�kartellrechtlichen�- Auseinandersetzungen
um Microsoft betroffen ist, ist das gerade auch vor dem Hintergrund zu
sehen, da� Microsoft die Schnittstellen zu Windows Dritten nur in einem
begrenzten Umfang zug�nglich gemacht hat, um sich das Gesch�ft mit auf
dem System aufbauender Software zu sichern. Darin unterscheiden sich
viele -�nicht alle�- computerbezogene Patente, die in der Vergangenheit
erteilt wurden, von den Schutzrechten klassischer Pr�gung. Diese
versetzen den Fachmann ohne weiteres in die Lage, nach Lekt�re der
Patentschrift die vorgestellte Vorrichtung zu bauen, den patentgem��en
Stoff herzustellen oder das Verfahren auszu�ben.

c) Eine Patentierung von Software in gr��erem Umfang begr�ndet die
-�auch kartellrechtlich gewichtige�- Gefahr, da� sich der einschl�gige
Markt auf einige wenige gro�e Unternehmen konzentriert. Auch vor dem
Hintergrund der aktuellen Bem�hungen um eine Verringerung der mit der
Anmeldung und Aufrechterhaltung von Patenten verbundenen Kosten werden
diese auch in Zukunft einen Faktor von erheblichem wirtschaftlichen
Gewicht darstellen. Diese Kosten werden von freien Entwicklern und
kleineren Unternehmen auch in Zukunft nicht oder nur bedingt aufgebracht
werden k�nnen, insbesondere vor dem Hintergrund, da� eine
Software-Patentierung nur Sinn macht, wenn sie sich m�glichst umfassend
�ber alle Industriestaaten erstreckt. Schutzrechte werden sich daher vor
allem in der Hand dieser gro�en Unternehmen wie Microsoft, IBM, SAP und
�hnlicher Firmen sammeln. Damit Hand in Hand geht die Gefahr, da� freie
Entwickler und kleine Unternehmen zuk�nftig auch aus wirtschaftlichen
Gr�nden zur Erstellung von Software nicht mehr in der Lage sind. Einige
der gro�en Unternehmen wie etwa IBM betreiben derzeit noch eine
gro�z�gige Lizenzierungspolitik; auch die von ihnen vergebenen Lizenzen
setzen jedoch eine Verg�tung durch den Lizenznehmer voraus. Nach
Untersuchungen aus den USA verlangt schon heute dort der Vertrieb von
neu entwickelter Software in der Regel 200�Lizenzen und mehr, f�r die
jeweils eine Verg�tung zu entrichten sein m��te. F�r den einzelnen
Entwickler bildet das eine erhebliche Kostenbelastung, die er weder
selbst tragen noch in der Regel uneingeschr�nkt an seine Abnehmer
weitergeben kann. Die Abgabe von Freeware-Programmen und das gesamte
Gesch�ftsmodell des Open-Source-Bereichs wird damit praktisch unm�glich
gemacht, ohne da� davon ausgegangen werden k�nnte, da� ihm eine
geringere Schutzw�rdigkeit als den klassischen Software-Firmen zukommen
w�rde. Die Innovation im Softwarebereich wird derzeit in erheblichem
Ma�e von diesen Entwicklern getragen, wie etwa der Erfolg der auf dem
Betriebssystem Linux aufbauenden auch gesch�ftlichen Bet�tigungen
belegt. Auf diesem Sektor ist heute eine Vielzahl von Probleml�sungen
f�r nahezu alle Bereiche des gesch�ftlichen, technischen und
wissenschaftlichen Bedarfs zu finden, die der klassische Markt nur
bedingt leistet.

d) Vor diesem Hintergrund ist die Patentierung von Software auch nicht
ohne Gefahren f�r den (technischen) Fortschritt, dessen F�rderung die
eigentliche und prim�re Aufgabe des Patentrechts ist. Ein wesentlicher
Teil der Entwicklungsarbeit auf diesem Gebiet wird heute von freien
Entwicklern und kleinen Unternehmen geleistet, deren Arbeitsergebnisse
von den gr��eren Unternehmen vielfach aufgekauft werden, wenn sie sich
im Markt bew�hrt und durchgesetzt haben. Das Betriebssystem DOS, aus dem
ein wesentlicher Teil der Gr��e von Microsoft flie�t, ist von einem
kleinen freien Programmierer entwickelt worden, der Gr�nder von
Microsoft, Bill Gates, hat es von ihm erworben und ohne nachhaltige
�berarbeitung an IBM geliefert. Dieses Unternehmen, das damals den
Universalcomputer entwickelt hatte, war seinerseits nicht der Lage
gewesen, f�r ihn ein funktionierendes Betriebssystem auf den Markt zu
bringen; es mu�te sich daher an andere Unternehmen, wie unter anderen an
Bill Gates, wenden, um diese L�cke zu schlie�en. Das anschlie�end von
IBM entwickelte eigene System hat das Unternehmen trotz unbestreitbarer
Vorz�ge des Programms nicht im Markt durchsetzen k�nnen. Die Software
Windows, die einen wesentlichen weiteren Grund f�r die heutige Gr��e von
Microsoft darstellt, beruht auf der �bernahme von Ideen der Entwickler
der seinerzeitigen Software f�r den "Apple", die heutige Firma
Mackintosh; dar�ber hinaus sind wesentliche Elemente dieser Software von
Dritten, insbesondere kleinen Entwicklern, hinzugekauft worden. Laufen
diese Gefahr, bei ihren Entwicklungen fremde Patente zu verletzen -�und
im Gefolge mit Patentverletzungsprozessen �berzogen zu werden�- oder in
erheblichem Umfang Lizenzgeb�hren auswerfen zu m�ssen, wird deren
Entwicklungsarbeit zumindest betr�chtlich erschwert, wenn nicht,
insbesondere wenn man die Finanzkraft, die typischerweise auf diesem
Sektor vorhanden ist, ber�cksichtigt, unm�glich gemacht.

e) Noch problematischer wird die Entwicklung, wenn man die derzeit noch
nur vereinzelt zu beobachtende Tendenz zur Patentierung von Algorithmen
in die Betrachtung einbezieht. Programmierarbeit ist im wesentlichen
angewandte Mathematik. In der Mathematik steht nur ein eng begrenzter
Kreis m�glicher Algorithmen f�r die Erstellung von Software zur
Verf�gung. Eine Patentierung dieses Formenschatzes macht schon vor dem
Hintergrund der wirtschaftlichen Belastung mit Lizenzgeb�hren die
Entwicklung von Software durch freie Entwickler nahezu unm�glich. Hinzu
kommt, da� nach dem derzeitigen Stand ein Zwang zur Lizenzierung nicht
besteht. Auch der Richtlinienentwurf hat darauf verzichtet, eine
Zwangslizenz vorzuschreiben.

3. Der Text der Richtlinie selbst ist zumindest ungl�cklich gefa�t.

Die Begriffsbestimmung des technischen Beitrags in Art.�2 definiert sich
zum einen durch sich selbst, wenn es hei�t, da� ein technischer Beitrag
ein Beitrag zum Stand der Technik ist. Die weitere Erg�nzung vermengt
die im Patentrecht herk�mmlich aus guten Gr�nden getrennten Begriffe der
Technizit�t und der erfinderischen T�tigkeit, wenn sie als technischen
Beitrag nur solche Beitr�ge gelten l��t, die f�r eine fachkundige Person
nicht naheliegend sind.

In gleicher Weise ungl�cklich ist auch die Definition der
computerimplementierten Erfindung, die zumindest eine teilweise
Vermengung des Erfindungsbegriffs mit dem der Neuheit enth�lt.

Die Regelung in Art.�3 verpflichtet die Mitgliedstaaten sicherzustellen,
da� eine computerimplementierte Erfindung als einem Gebiet der Technik
zugeh�rig gilt. Das bedeutet, da� nach den von den Mitgliedstaaten zu
schaffenden Regelungen jede computerimplementierte Erfindung im Sinne
der Begriffsbestimmung nach Art.�2�a als technischer Gegenstand zu
gelten hat ohne R�cksicht darauf, ob und in welchem Umfang eine
technische Lehre offenbart worden ist. Die gleichen Bedenken bestehen
gegen�ber der Regelung nach Art.�4, nach der wiederum unabh�ngig von
jeder Offenbarung einer technischen Lehre allein der Umstand, da� eine
computerimplementierte Erfindung vorliegt, die gewerblich anwendbar und
neu ist und auf einer erfinderischen T�tigkeit beruht.

Die Regelungen nach Art.�4 Nr.�2 und 3 des Richtlinienentwurfs vermengen
wiederum verschiedene Begriffe des Patentrechts. Art.�4 Nr.�2 macht das
Vorliegen einer erfinderischen T�tigkeit von dem Vorliegen eines
technischen Beitrags abh�ngig. Dem Gedanken, da� nur die Offenbarung
einer technischen Lehre Patentschutz verdient, tr�gt diese Regelung
nicht Rechnung, da ein technischer Beitrag nach der bisherigen Praxis,
von der auch die Richtlinie ausgeht, schon durch die Verwendung des
Computers erf�llt ist. Das entspricht im �brigen auch der Regelung in
Art.�2�a.

Die Regelung in Art.�4 Nr.�3 macht die Ermittlung des technischen
Beitrags von der Neuheit des Vorschlags abh�ngig. Die in Art.�5
bestimmte Form des Patentanspruchs begr�ndet Erzeugnisschutz schon dann,
wenn Gegenstand der Anmeldung ein programmierter Computer, ein
programmiertes Computernetz oder eine sonstige programmierte Vorrichtung
ist; sie kn�pft die Voraussetzungen der Patentierung insoweit also
daran, da� ein Computer und ein Programm miteinander verkn�pft worden
sind. Daran ist richtig, da� sich bei dieser Betrachtung ein technischer
Gegenstand nicht mehr leugnen l��t; �ber die Frage, ob eine solche
Kombination Patentschutz verdient, ist damit jedoch nichts ausgesagt. In
der Sache geht diese Regelung deutlich auch �ber die bisherige Praxis
des EPA hinaus, nach der das �bliche Zusammenwirken vom Computer und
Programm allein zur Begr�ndung von Patentschutz nicht gen�gt. Die f�r
den Patentschutz von verfahrensbezogenen Erfindungen bestimmten
Voraussetzungen lassen diesen auch dann entstehen, wenn lediglich eine
Gesch�ftsidee abstrakt mit der Benutzung des Computers verbunden ist;
auf die Offenbarung konkreter technischer Lehren ist dieser Schutz
ebenfalls nicht beschr�nkt.

Mit freundlichen Gr��en

 -

�

?

Q

R

^

_

b

e

"

?

?

?

?

