<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#rWa: Wuermelings Medienkampagne

#sSt: Wuermelings Änderungsvorschläge: Scheinbegrenzung der Patentierbarkeit

#ern: Zweifel an Wuermelings wahren Absichten ergeben sich indes nicht nur aus seinem bisherigen unermüdlichen Eintreten für weitestmögliche Patentierbarkeit von Software (einschließlich Programmansprüche, von Wuermling in JURI durchgesetzt und auch durch die Änderungsanträge nicht angetastet), sondern auch aus der PR selber.  In dieser wird z.B. als Ziel genannt, Verfahren der Worterkennung patentierbar zu machen und %(q:Ideenklau) zu verhindern.  Dem gegenüber steht laut Wuermelingscher Darstellung lediglich das Interesse der %(q:Opensource-Unternehmen), dem Wuermeling widerwillig nachkommt.  Ferner setzt Wuermeling seine Absichten mit denen der Kommission gleich, deren %(q:Erklärendes Memorandum) deutlich für die Patentierbarkeit reiner Software wirbt und dies zum Ziel erklärt.

#giP: Der Artikel gibt Wuermelings Standpunkt wieder, wie er in Wuermelings PR Ausdruck findet.

#Am84: In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, they must in addition make a new technical contribution to the state of the art, in order to distinguish them from pure software.

#Am86: Um computerimplementierte Erfindungen rechtlich zu schützen, sind keine getrennten Rechtsvorschriften erforderlich, die das nationale Patentrecht ersetzen. Die Vorschriften des nationalen Patentrechts sind auch weiterhin die Hauptgrundlage für den Rechtschutz computerimplementierter Erfindungen. Durch diese Richtlinie wird lediglich die derzeitige Rechtslage klargestellt, um Rechtssicherheit, Transparenz und Rechtsklarheit zu gewährleisten und Tendenzen entgegenzuwirken, nicht patentierbare Methoden, wie Trivialvorgänge und Geschäftsmethoden, als patentfähig zu erachten.

#Am87: %(al|Ausschluss von der Patentierbarkeit|Bei computerimplementierten Erfindungen wird nicht schon deshalb von einem technischen Beitrag ausgegangen, weil zu ihrer Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird. Folglich sind Erfindungen, zu deren Ausführung lediglich ein Computerprogramm eingesetzt wird (Datenverarbeitung) und durch die Geschäftsmethoden, mathematische oder andere Methoden angewendet werden, nicht patentfähig, wenn sie über die normalen physikalischen Interaktionen zwischen einem Programm und dem Computer, Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in der es abgespielt wird, keine technischen Wirkungen erzeugen.)

#Am88: (5a) Die Regeln gemäß Artikel 52 des Europäischen Patentübereinkommens über die Grenzen der Patentierbarkeit sollen bestätigt und präzisiert werden. Die dadurch entstehende Rechtssicherheit sollte zu einem investitions- und innovationsfreudigen Klima im Bereich der Software beitragen.

#shl: Bezeichnend ist, dass Wuermeling in Erwägungsgründen manche der Formulierungen übernommen hat, mit denen der FFII die Formulierungen der Europäischen Kommission zu ersetzen %(fg:vorschlug), aber nur als Zusätze, ohne erstere zu ersetzen.  Und natürlich nur in Erwägungsgründen, wo dies ohnehin wenig bewirkt.

#title: Wuermeling für Scheinbegrenzungen der Patentierbarkeit

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: jwuerm0309 ;
# txtlang: de ;
# multlin: t ;
# End: ;

