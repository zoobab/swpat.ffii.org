<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

rWa: Wuermeling's Media Campaign
ern: Zweifel an Wuermelings wahren Absichten ergeben sich indes nicht nur aus seinem bisherigen unermüdlichen Eintreten für weitestmögliche Patentierbarkeit von Software (einschließlich Programmansprüche, von Wuermling in JURI durchgesetzt und auch durch die Änderungsanträge nicht angetastet), sondern auch aus der PR selber.  In dieser wird z.B. als Ziel genannt, Verfahren der Worterkennung patentierbar zu machen und %(q:Ideenklau) zu verhindern.  Dem gegenüber steht laut Wuermelingscher Darstellung lediglich das Interesse der %(q:Opensource-Unternehmen), dem Wuermeling widerwillig nachkommt.  Ferner setzt Wuermeling seine Absichten mit denen der Kommission gleich, deren %(q:Erklärendes Memorandum) deutlich für die Patentierbarkeit reiner Software wirbt und dies zum Ziel erklärt.
giP: The article reproduces Wuermeling's point of view, as expressed in his press release.
sSt: Wuermeling's Amendments: Fake Limits on Patentability
Am84: In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, they must in addition make a new technical contribution to the state of the art, in order to distinguish them from pure software.
Am86: The legal protection of computerimplemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as trivial procedures and business methods.
Am87: %(al|Exclusions from patentability|A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions merely involving computer programs (data processing) which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.)
Am88: The rules pursuant to Article 52 of the European Patent Convention concerning the limits to patentability should be confirmed and clarified. The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software.
shl: Bezeichnend ist, dass Wuermeling in Erwägungsgründen manche der Formulierungen übernommen hat, mit denen der FFII die Formulierungen der Europäischen Kommission zu ersetzen %(fg:vorschlug), aber nur als Zusätze, ohne erstere zu ersetzen.  Und natürlich nur in Erwägungsgründen, wo dies ohnehin wenig bewirkt.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: jwuerm0309 ;
# txtlang: en ;
# End: ;

