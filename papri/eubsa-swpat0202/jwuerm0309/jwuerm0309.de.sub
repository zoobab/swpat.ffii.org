\begin{subdocument}{jwuerm0309}{Wuermeling f\"{u}r Scheinbegrenzungen der Patentierbarkeit}{http://swpat.ffii.org/papiere/eubsa-swpat0202/jwuerm0309/index.de.html}{Arbeitsgruppe\\swpatag@ffii.org}{Der Schattenberichterstatter der Europ\"{a}ischen Volkspartei, Dr. Joachim Wuermeling aus Bayreuth (CSU), ist unter Druck gekommen.  Eine internationale Gruppe von 32 EVP-Abgeordnete, angef\"{u}hrt von Piia-Noora Kauppi aus Finnland, hat eigene \"{A}nderungsantr\"{a}ge eingebracht, die wirksam Software und Gesch\"{a}ftsmethoden von der Patentierbarkeit ausschlie{\ss}en und grundlegende Freiheiten des Wettbewerbs und des Ver\"{o}ffentlichens sch\"{u}tzen.  Viele EVP-Kollegen haben begonnen, diese \"{A}nderungsantr\"{a}ge eigenst\"{a}ndig zu studieren, ohne sich auf ihren Berichterstatter zu verlassen.  Zugleich fand eine Demonstration zum Protest gegen die Wuermelings Politik in M\"{u}nchen regen Zulauf.  Wuermeling hat inzwischen mit einer eigenen Pressemitteilung und mit einem Satz von \"{A}nderungsantr\"{a}gen reagiert.  In beiden wird den Interessen der ``Opensource-Firmen und freine Entwickler'' Lippendienst gezollt und beteuert, Patentierbarkeit reiner Software sei nicht beabsichtigt und m\"{u}sse verhindet werden.  Es kommt aber auch zwischen den Zeilen zum Ausdruck, dass Wuermeling im Grunde Softwarepatente z.B. auf ``Erfindungen'' im Bereich etwa der ``Worterkennung'' durchsetzen und ``Ideenklau'' verhindern will.  Entsprechend unklar fallen auch Wuermelings \"{A}nderungsantr\"{a}ge aus.  Es bleibt abzuwarten, ob Wuermeling n\"{a}chste Woche beim Erstellen seiner Abstimmungsempfehlungen f\"{u}r die EVP die \"{A}nderungsantr\"{a}ge der Kauppi-Gruppe unterst\"{u}tzt.  Denn diese \"{A}nderungsantr\"{a}ge setzen wirklich das um, was Wuermeling als seine Absicht ausgibt.}
\begin{sect}{cusku}{Wuermelings Medienkampagne}
\begin{itemize}
\item
{\bf {\bf Wuermeling ver\"{o}ffentlicht neue PE Gegen (F\"{u}r) Softwarepatente\footnote{http://www.wuermeling.net/presse/presse192.htm}}}

\begin{quote}
Der Schattenberichterstatter der Europ\"{a}ischen Volkspartei, Dr. Joachim Wuermeling aus Bayreuth (CSU), ist unter Druck gekommen.  Eine internationale Gruppe von 32 EVP-Abgeordnete, angef\"{u}hrt von Piia-Noora Kauppi aus Finnland, hat eigene \"{A}nderungsantr\"{a}ge eingebracht, die wirksam Software und Gesch\"{a}ftsmethoden von der Patentierbarkeit ausschlie{\ss}en und grundlegende Freiheiten des Wettbewerbs und des Ver\"{o}ffentlichens sch\"{u}tzen.  Viele EVP-Kollegen haben begonnen, diese \"{A}nderungsantr\"{a}ge eigenst\"{a}ndig zu studieren, ohne sich auf ihren Berichterstatter zu verlassen.  Zugleich fand eine Demonstration zum Protest gegen die Wuermelings Politik in M\"{u}nchen regen Zulauf.  Wuermeling hat inzwischen mit einer eigenen Pressemitteilung und mit einem Satz von \"{A}nderungsantr\"{a}gen reagiert.  In beiden wird den Interessen der ``Opensource-Firmen und freine Entwickler'' Lippendienst gezollt.  Es kommt aber auch zum Ausdruck, dass Wuermeling im Grunde Softwarepatente z.B. auf ``Erfindungen'' im Bereich der Textverarbeitung, will und der Meinung ist, dass andernfalls ``dem Ideenklau T\"{u}r und Tor ge\"{o}ffnet'' werde.  Dieser Wille kommt auch in Wuermelings \"{A}nderungsantr\"{a}gen zum Ausdruck, bei denen es sich durchweg um wohlklingende Scheinbegrenzungen handelt.  Wuermeling scheint immerhin seinen Widerstand gegen Art 6a aufgegeben zu haben, m\"{o}chte aber wohl noch immer \"{u}ber die Absichten der Europ\"{a}ischen Kommission hinaus die Ver\"{o}ffentlichung von Software zu einer unmittelbaren Verletzungshandlung machen.  Hier\"{u}ber geben jedoch \"{A}nderungsantr\"{a}ge und Pressemeldungen nicht letztg\"{u}ltige Auskunft.  Enscheidend ist vielmehr die Frage, ob Wuermeling n\"{a}chste Woche beim Erstellen seiner Abstimmungsempfehlungen f\"{u}r die EVP die \"{A}nderungsantr\"{a}ge der Kauppi-Gruppe unterst\"{u}tzt.  Denn diese \"{A}nderungsantr\"{a}ge setzen genau das um, was Wuermeling zu beabsichtigen vorgibt.

Zweifel an Wuermelings wahren Absichten ergeben sich indes nicht nur aus seinem bisherigen unerm\"{u}dlichen Eintreten f\"{u}r weitestm\"{o}gliche Patentierbarkeit von Software (einschlie{\ss}lich Programmanspr\"{u}che, von Wuermling in JURI durchgesetzt und auch durch die \"{A}nderungsantr\"{a}ge nicht angetastet), sondern auch aus der PR selber.  In dieser wird z.B. als Ziel genannt, Verfahren der Worterkennung patentierbar zu machen und ``Ideenklau'' zu verhindern.  Dem gegen\"{u}ber steht laut Wuermelingscher Darstellung lediglich das Interesse der ``Opensource-Unternehmen'', dem Wuermeling widerwillig nachkommt.  Ferner setzt Wuermeling seine Absichten mit denen der Kommission gleich, deren ``Erkl\"{a}rendes Memorandum'' deutlich f\"{u}r die Patentierbarkeit reiner Software wirbt und dies zum Ziel erkl\"{a}rt.

siehe auch Demonstration M\"{u}nchen 2003/09/19: Innovation statt Patentinflation!\footnote{http://swpat.ffii.org/termine/2003/europarl/09/muenchen/index.de.html} und Dr. Joachim Wuermeling MdEP und Softwarepatente\footnote{http://swpat.ffii.org/akteure/jwuermeling/index.de.html}
\end{quote}
\filbreak

\item
{\bf {\bf FTD 2003/09/22: EU stimmt über Softwarepatente ab\footnote{http://www.ftd.de/tm/it/1064044984638.html}}}

\begin{quote}
Der Artikel gibt Wuermelings Standpunkt wieder, wie er in Wuermelings PR Ausdruck findet.
\end{quote}
\filbreak

\item
{\bf {\bf Handelsblatt.com: Seite gesperrt\footnote{http://www.handelsblatt.com/hbiwwwangebot?fn=relhbi\&sfn=buildhbi\&CN=GoArt!200104,204016,657988\&bt=0\&SH=0\&depot=0}}}

\begin{quote}
Berichtet \"{u}ber ``Opensource-Protestler'' in Br\"{u}ssel, enth\"{a}lt viel Fehlinformation, teilweise durch ein ausf\"{u}hrliches Interview mit MdEP Joachim Wuermeling beigesteuert.  FFII wurde nicht befragt.

siehe auch Dr. Joachim Wuermeling MdEP und Softwarepatente\footnote{http://swpat.ffii.org/akteure/jwuermeling/index.de.html}, Handelsblatt Umfrage: Brauchen wir eine RiLi fuer EU-Softwarepatente\footnote{http://www.handelsblatt.com/hbiwwwangebot?fn=relhbi\&sfn=buildhbi\&STRUCID=page\_202829\&PAGEID=page\_202769\&BMC=cn\_hnavi\&BMC=cn\_umfrage\_erg\&BMC=cn\_umfrage\_lesermeinungen\&CNCT=0\&BELEGUNG=DOCUMENT\_202776\&BUCHID=657995\&bt=0\&SH=0\&depot=0} und EU Software Patent Plans Shelved Amid Massive Demonstrations\footnote{http://swpat.ffii.org/neues/03/demo0827/index.en.html}
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{amend}{Wuermelings \"{A}nderungsvorschl\"{a}ge: Scheinbegrenzung der Patentierbarkeit}
\begin{sect}{am84}{\"{A}nderungsvorschlag 84}
\begin{quote}
{\it In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, they must in addition make a new technical contribution to the state of the art, in order to distinguish them from pure software.}
\end{quote}
\end{sect}

\begin{sect}{am86}{\"{A}nderungsvorschlag 86}
\begin{quote}
{\it Um computerimplementierte Erfindungen rechtlich zu schützen, sind keine getrennten Rechtsvorschriften erforderlich, die das nationale Patentrecht ersetzen. Die Vorschriften des nationalen Patentrechts sind auch weiterhin die Hauptgrundlage für den Rechtschutz computerimplementierter Erfindungen. Durch diese Richtlinie wird lediglich die derzeitige Rechtslage klargestellt, um Rechtssicherheit, Transparenz und Rechtsklarheit zu gewährleisten und Tendenzen entgegenzuwirken, nicht patentierbare Methoden, wie Trivialvorgänge und Geschäftsmethoden, als patentfähig zu erachten.}
\end{quote}
\end{sect}

\begin{sect}{am87}{\"{A}nderungsvorschlag 87}
{\bf Ausschluss von der Patentierbarkeit

Bei computerimplementierten Erfindungen wird nicht schon deshalb von einem technischen Beitrag ausgegangen, weil zu ihrer Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird. Folglich sind Erfindungen, zu deren Ausführung lediglich ein Computerprogramm eingesetzt wird (Datenverarbeitung und durch die Geschäftsmethoden, mathematische oder andere Methoden angewendet werden, nicht patentfähig, wenn sie über die normalen physikalischen Interaktionen zwischen einem Programm und dem Computer, Computernetzwerk oder einer sonstigen programmierbaren Vorrichtung, in der es abgespielt wird, keine technischen Wirkungen erzeugen.)}
\end{sect}

\begin{sect}{am88}{\"{A}nderungsvorschlag 88}
\begin{quote}
{\it Erw\"{a}gung 5(a) (new)}

{\it (5a) Die Regeln gemäß Artikel 52 des Europäischen Patentübereinkommens über die Grenzen der Patentierbarkeit sollen bestätigt und präzisiert werden. Die dadurch entstehende Rechtssicherheit sollte zu einem investitions- und innovationsfreudigen Klima im Bereich der Software beitragen.}
\end{quote}

Bezeichnend ist, dass Wuermeling in Erw\"{a}gungsgr\"{u}nden manche der Formulierungen \"{u}bernommen hat, mit denen der FFII die Formulierungen der Europ\"{a}ischen Kommission zu ersetzen vorschlug\footnote{http://swpat.ffii.org/papiere/eubsa-swpat0202/prop/index.de.html}, aber nur als Zus\"{a}tze, ohne erstere zu ersetzen.  Und nat\"{u}rlich nur in Erw\"{a}gungsgr\"{u}nden, wo dies ohnehin wenig bewirkt.
\end{sect}
\end{sect}

\begin{sect}{links}{Kommentierte Verweise}
\begin{itemize}
\item
{\bf {\bf Dr. Joachim Wuermeling MdEP und Softwarepatente\footnote{http://swpat.ffii.org/akteure/jwuermeling/index.de.html}}}

\begin{quote}
Joachim Wuermeling, Doktor des Rechts, Mitglied des Europ\"{a}ischen Parlaments, CSU, Schattenberichterstatter der Europ\"{a}ischen Volkspartei (EVP) \"{u}ber die Softwarepatentrichtlinie und gelegentlich \"{u}ber andere Patentangelegenheiten, setzte sich gegen alles ein, was die Patentierbarkeit irgendwie beschr\"{a}nken k\"{o}nnte (z.B. Interoperabilit\"{a}tsprivileg) und f\"{u}r alles, was sie erweitert (z.B. die von der Eur. Kommission nicht vorgesehenen Programmanspr\"{u}che).  Die meisten EVP-Abgeordneten folgen Wuermeling in diesen Fragen blind.  Viele Kritiker haben mit Wuermeling vergeblich den Dialog gesucht.  W\"{a}hrend Wuermeling f\"{u}r Gespr\"{a}che bislang unzug\"{a}nglich geblieben ist, sucht er aktiv den Kontakt zur Presse, um sich als Gegner von Patenten auf ``reine Software und Gesch\"{a}ftsmethoden'' auszugeben.   Den in der \"{O}ffentlichkeit entstandenen gegenteiligen Eindruck f\"{u}hrt er durchweg auf ``unbegr\"{u}ndete \"{A}ngste der Opensource-Bewegung'' zur\"{u}ck.  Wuermeling brachte im Rechtsausschuss einen wirkungslosen \"{A}nderungsantrag (Erw\"{a}gungsgrund 13d) ein, demzufolge ein Patentanspruch sich nur auf ein bestimmtes Produkt beziehen und Algorithmen nicht erfassen kann.   Wuermeling scheint tats\"{a}chlich die verharmlosenden Erkl\"{a}rungen zu glauben, die ihm einige Verbandslobbyisten liefern.  Die selben Verbandslobbyisten machen sich in internen Schriftst\"{u}cken \"{u}ber Wuermelings fehlendes Verst\"{a}ndnis f\"{u}r elementare Zusammenh\"{a}nge des Patentwesens lustig.  Wuermelings Unwissen und Unzug\"{a}nglichkeit h\"{a}ngen m\"{o}glicherweise damit zusammen, dass er mit den Arbeiten f\"{u}r den Verfassungskonvent \"{u}berlastet ist.
\end{quote}
\filbreak

\item
{\bf {\bf Demonstration M\"{u}nchen 2003/09/19: Innovation statt Patentinflation!\footnote{http://swpat.ffii.org/termine/2003/europarl/09/muenchen/index.de.html}}}

\begin{quote}
Der CSU, die sich viel auf das High-Tech-Land Bayern zu gute h\"{a}lt, soll noch einmal deutlich gemacht werden, da{\ss} die von ihr im Europa-Parlament vertretene Patentgesetzgebung Gift f\"{u}r die Innovationskraft der bayerischen Wirtschaft ist.  Dazu treffen sich zahlreiche Betroffene und Spitzenkandidaten des bayerischen Wahlkampfes am Freitag, den 19. September um 15.30 vor dem Europ\"{a}ischen Patentamt und um 17.00 am Odeonsplatz.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: latex ;
% End: ;

