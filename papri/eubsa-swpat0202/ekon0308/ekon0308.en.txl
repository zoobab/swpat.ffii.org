<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#rut: A Critique of the Rapporteur’s Explanatory Statement accompanying the
JURI Report to the European Parliament on the proposed Directive on
the Patentability of Computer-Implemented Inventions

#zSe: The present document, written jointly by scholars specializing in the
economics of innovation and intellectual property, critiques and
corrects misleading impressions conveyed by the Explanatory Statement
that appears in the JURI report to the European Parliament, especially
those pertaining  to the interpretation of the existing economic
research evidence on patents’ role in stimulating software
innovations.

#rne: Inasmuch as the rapporteur’s explanatory statement in the JURI report
is the document that will introduce the directive to many members of
the Parliament who are unfamiliar with the subject, the imbalance and
incompleteness of the analysis it offers is extremely unfortunate.2
Because the proposed directive’s terms are treated as matters of
political compromise, economic issues that deserve careful
consideration are glossed over, and the many problematic aspects of
software patents that are now becoming manifest in the United States
are entirely ignored. Indeed, a number of these problems were raised
in the economics section of the study conducted for the Commission by
the Intellectual Property Institute and, after the publication of the
proposed directive, in the Parliament’s own commissioned study, “The
Patentability of Computer Programs,” prepared by the Institute for
Information Law at the University of Amsterdam.3

#rtq: Critique

#cWc: Conclusions and discussion

#cte: The explanatory statement opens by reciting the large number of
patents granted for computer-related inventions (not limited to
computer programs) as evidence of the demand for patents. This is
dangerously misleading without a proper understanding of the context,
as is the suggestion later in the document that companies with
substantial software-related research may be expected to spend 5-10%
of R&D on patenting. Both higher numbers of patents and high
expenditures on patents may be symptomatic of strategic portfolio
building. As the CEO of an SME testified at the FTC/DOJ hearings:

#0Wo: I have no idea whether my product infringes on upwards of 120
different patents, all of which are held by large companies who could
sue me without thinking about it. The end result, much like Borland, I
have now issued a directive that we reallocate roughly 20 to 35
percent of our developers’ resources and sign on two separate law
firms to increase our patent portfolio to be able to engage in the
patent spew conflict.4

#maf: In the same hearings, Robert Barr, Vice President of Cisco, said

#Wei: Where the patent system enables true innovation, true progress, where
it enables companies to bring new products to consumers in
circumstances where they otherwise would not do it, or where it
disseminates knowledge that others need and want, then it’s working.
There are certainly examples of industries where it serves these
purposes, and these benefits must be preserved. But in my experience
at Cisco and my prior experience representing a variety of companies,
the negative effects of stockpiling patents, the consequences of
innocent infringement through independent development, the cost of
proving non-infringement or invalidity through patent litigation and
the exploitation of the patent system as a revenue generating tool in
its own right have hindered true innovation and outweighed the
benefits.5

#WBd: Both executives are responding to the fact that the patenting increase
in the computing and communications technology area in the United
States has been driven by the strategic motive of piling up patents
for defensive use and use in cross-licensing negotiations and not by
the need to secure returns to innovation.6 It is noteworthy that the
majority of U.S. patents in software technology are not taken out by
package software publishers, but by hardware manufacturers plus IBM
(which is classified as a computing service provider).7 The growth in
patenting in this areas is disproportionately driven by large U.S.
firms involved in the manufacture of semiconductors, computers, and
communications equipment, who are amassing patents specifically for
defensive purposes as described by both the speakers quoted above.8 
While cross-licensing is commonplace among the large companies, many
are now using their portfolios to extract offset fees from companies
that bring smaller portfolios to the table.

#ane: Section 2 of the rapporteur’s statement discusses the need for patent
protection in software. It concludes with the following rationale for
software patents: “It would be wrong to discriminate against software
developers by refusing them the patent protection available to other
inventors when all the conditions for patentability are present.” The
European Parliament should not need to be reminded that the purpose of
the patent system is to encourage investment in technological
innovation and economic growth, and not to establish equitable
treatment for all forms of human enterprise that conceivably could be
granted legal monopoly rights by this device. A similar argument could
have been applied to scientific discoveries such as the first and
second laws of thermodynamics or to theorems derived from the axioms
of Euclidean geometry, but there seems to be universal agreement to
deny patentability to scientific discoveries, presumably because it is
thought that granting patents on such discoveries would greatly retard
scientific progress.

#teW: Section 3 of the rapporteur’s statement cites the familiar argument
that the directive will reduce legal uncertainty by rephrasing the
standard of patentability, and indeed this is an admirable goal.
Nevertheless, the statement contains no explanation of why this
standard will be any more secure against administrative and judicial
erosion than was the U.S. standard. Nor is there a response to the
view expressed by the experts at the Institute for Information Law
that the proposed language of the directive actually would increase
legal uncertainty.9 Even in the U.S., where technicity is not a
limitation, it is evident from testimony at the FTC/Department of
Justice hearings [2002] that in practice very great uncertainties
surround the scope, reach, and effect of information-goods patents.10

#faW: Section 4 of the statement concerns the impact on small and
medium-sized software developers, which is perhaps the area with the
most serious cause for concern, both with respect to the positive and
negative effects of software patentability.11 The statement relies
heavily on a single undocumented sentence from the Intellectual
Property Institute: “the patentability of computer program related
inventions has helped the growth of computer program related
industries in the States, in particular the growth of SMEs and
independent software developers into sizeable indeed major
companies.”12 A close reading of the IPI study reveals that there is
no basis for such a broad conclusion either in the study itself or in
the relevant empirical findings reported by the academic economics
research literature. By omitting the caveats that appear in the IPI
study’s discussion of this issue, the statement conveys a seriously
misleading picture of the likely impact of software patents on SMEs in
the European Union and the new accession States.

#hyr: While it is generally accepted that patents support niche entry in
information technology markets as in other fields, the widespread
development of strategic patent portfolio-building practices within
the ICT sector limits the ability of small companies to grow in
competition with large companies.13 Small companies are disadvantaged
in using the patent system in complex technologies, because many
patent licenses are required to assemble complex products, and also
because considerable legal resources are needed to credibly assert and
defend against patents. As a recent report from the National Academy
of Sciences in the U.S. concluded: “[D]eveloping and deploying
software and systems may cease to be a cottage industry because of the
need for access to cross-licensing agreements and the legal protection
of large corporations.”14

#Weo: The anecdote in this section about the SME issuing a non-exclusive
license to a large multinational enterprise is cited as evidence of
the importance of small firm innovation to large firms. But there is
another interpretation: rather than evidencing the good will of SMEs
toward large companies, such non-exclusive licensing often takes place
when SMEs are found to be inadvertently infringing patents in a large
company’s portfolio. Typically, the large company demands access to
the SME’s technology in return for providing access to patents that
the SME needs. Not only does the SME lose exclusivity in its
intellectual property, but it is commonly required to make a side
payment to the cross-licensing large entity in acknowledgment of its
much larger patent portfolio.

#tgo: The rapporteur’s statement overlooks these issues, and similarly omits
consideration of the liabilities and high costs of using patent
information to guide R&D investment strategy. It fails to acknowledge
properly the disaffection that small companies with experience in the
workings of the system express in regard to patenting in general, and
toward software patents in particular.15 Much of this is due to the
very high costs of pursuing, enforcing, and defending against patents.
As the authors of the Institute for Information Law study observed in
testimony to the Parliament: “in practice, patents are widely used to
fight competitors with legal means rather than with product
superiority.”16

#oco: The SCO actions against Linux should remind policymakers that
exploitation of intellectual property is not limited to the protection
of going businesses.  As a weapon, intellectual property is used most
effectively by companies that have no going business and therefore
little to lose from uninhibited aggression.  They wield the most power
when their property claims have been inadvertently embedded in the
systems of millions of business so as to cause widespread uncertainty
among sectors that are mere users of technology.  This prospect of
economy-wide liability argues strongly for a strict interpretation of
the technicity requirement that limits patents to the scope of the
technical contribution.  This would limit speculation in software
patents by largely confining their application to technical fields
where there is more likely to be awareness of relevant patents and
less temptation to assert patents against unknowing and
unsophisticated users.  Ideally, this would eliminate the risk of
inadvertent infringement in the basic information processing
functionality of operating systems and common applications such as
word processors.

#WWl: More generally, the rapporteur’s statement fails to address the
potential negative economic effects of large portfolios on the
competitive structure of an industry characterized by strong economies
of scale and network effects. Software patents readily may multiply
instances of monopoly control over compatibility interfaces in
computer code, thereby opening the way for the abuse of market power
over “essential facilities” – and expanding the problems with which
the competition authorities would have to contend.  We appreciate the
JURI committee's efforts to address this by amending Article 6 to
ensure that patents (like copyright) cannot be used to block
interoperability by complements, but note that this does not preclude
the use of patents to pre-empt critical functions within a
general-purpose information infrastructure.

#tWp: The most serious error in interpreting the economic evidence is
perhaps that in section 5, where the rapporteur’s statement asserts
that “academic studies have shown a link between R&D spending, patent
applications, and productivity.” No documentation for this claim is
provided. In fact, what is known via academic research is that
although a firm’s R&D spending is clearly related to its productivity,
profitability, or market value, there is little evidence that patents
contribute separately to performance, that is, above and beyond R&D
spending.17 Direct survey evidence for the United States and Europe
has found that patents are only considered important for securing
returns to innovation in the specialty chemicals industry including
pharmaceuticals, medical instruments, and specialized machinery.18

#WWp: In the study already cited that explicitly focused on software
patents, Bessen and  Hunt found that the correlation between R&D and
patenting in the U.S. over time has been significantly negative. In
other words, as software patenting rates have risen, R&D investment in
sectors using information technologies has declined. Whether there is
an underlying causal relationship remains unestablished, but the U.S.
experience warrants scepticism regarding claims that software
patenting would contribute to the goal of raising the software R&D
investment rate in the European Research Area.

#efo: We appreciate the rapporteur’s and the Parliament committees’ concerns
for monitoring the impact of software patents in Europe and the U.S.
This should be done in a scientifically sound manner in which the
methodology and conclusions are publicly debated. It must begin with a
proper empirical baseline before the directive is implemented in order
to understand the effects of the directive. Indeed, it should be done
before the directive is enacted to ensure that the baseline is not
prejudiced by business behaviour that anticipates implementation. As
the Institute for Information Law study notes, there is grossly
inadequate information about the functioning of patents in practice
and a need for disinterested independent oversight, such as the
proposed European Patent Observatory.19

#oiW: This critique has been directed primarily at the rapporteur’s
statement rather than the Commission’s original analysis, because 18
months have passed since the draft directive was issued. In the
interim, there have been substantial additions to the record, such as
the Institute for Information Law study, the FTC/DOJ hearings in the
U.S., and the noteworthy empirical study of U.S. software patents by
Bessen and Hunt. A number of conferences and seminars have been held
on the economics of the patent system with special attention to
software and services. None of this is reflected in the JURI
rapporteur’s statement, which is substantially unchanged from its
original draft.

#Wtr: That said, it should be noted the Commission has been regrettably slow
to provide balanced background analysis. While DG Internal Market is
to be commended for ordering a study on the economic impact of
patentability, this was only after the Commission had already taken a
poorly explained position on the issue in its 1997 Green Paper20 and
its 1999 Communication.21 This made it difficult to embark on a study
that could address the issue afresh with the resources and scope that
it deserved.

#taa: Despite the promise implicit in its title, the small contract for the
study was not awarded to one of Europe’s many research institutes
specializing in the economics of innovation, but to the legally
oriented Intellectual Property Institute in London, which has no
economists on staff and at best a limited record of conducting
economic research. The economist brought in on the study did a
respectable review of the literature in 11 pages, a minor fraction of
the report, which as a whole dwelt extensively on legal issues.22 As
noted, there were discrepancies between the economics section and the
summary, yet the Commission did not provide for peer review, either in
writing or through a public event. Nevertheless, the analysis in the
memo that accompanies the Commission’s draft directive is largely
based on selected statements from the study and basically justifies
the direction indicated in the 1997 and 1999 documents.

#tey: Most regrettably, Internal Market as the lead DG on intellectual
property has failed to address the larger economic and institutional
problems inherent in the patent system, even as it moves Europe toward
the laudable goal of a community patent. Indeed, instead of making an
effort to better understand how the system affects competition and
innovation, it has invested resources in unabashedly promoting greater
use of the system. In effect, this urges Europe into the kind of
strategic portfolio building that operates as a limitation and barrier
to small businesses in the U.S. To this end, the Commission
jeopardized its reputation for objectivity by awarding a large
contract to IBM, a company with a uniquely voluminous portfolio and a
famously aggressive licensing program, to make recommendations on how
national patent offices should promote patenting.23

#Wae: In the same vein, the Commission has pursued planning for insurance to
support patent enforcement through litigation, since it is known to be
costly for SME. This, however, was premised on another
misunderstanding of the U.S. situation. As the contracted study
concluded: “The tacitly assumed successful and wide use of insurance
in the USA proved to be illusory.” The investigation showed that the
principal insurance market in the U.S. was for defensive insurance and
that the insurers themselves were fearful of the risks involved.24
Their concerns were remarkably consistent with the picture of patent
minefields and attendant risk and uncertainty painted in the FTC/DOJ
hearings.

#onr: Unfortunately, many in Europe continue to be enthralled by the
American mythology of patents, and on that basis conclude that the
best course must be to follow U.S. policy. But, instead of
unquestioningly accepting self-interested and often biased
interpretations of the U.S.experience, Europe’s interest will be far
better served by expending the effort to thoroughly understand the
business effects of software patents. Members of the European
Parliament should insist on being informed by independent, critical
assessment of the available evidence (including that from the U.S.
during the 1990’s) and the likely benefits and costs of liberalizing
the issuance of software patents, before committing the region’s
economies to an institutional change that will prove very difficult to
reverse.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: ekon0308 ;
# txtlang: en ;
# multlin: t ;
# End: ;

