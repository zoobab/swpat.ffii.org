<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: ESC 2002-09: Europe should reconfirm Non-Patentability of Software!

#descr: The Economic and Social Council of the European Union, a consultative organ of experts from various fields, criticises the European Patent Office's software caselaw and the European Commission's proposal for a software patentability directive and asks the European Parliament to reject the proposal and instead ask for a reconfirmation of the non-patentability of software.  This study met strong resistance from a group of supporters of the European Patent Office, but was in the end passed with a 2/3 majority.

#TWW: The Directive makes it possible to patent a programmed computer or programmed network or  a  process  implemented  through  the  execution  of  a  programme.  Any  innovation  made  in  this  way  is automatically  considered  %(q:to  belong  to  a  field  of  technology),  even  if  the  result  is  derived  entirely  from software  operations.  The  door  thus  seems  wide  open  to  a  software  patent,  as  no  programmable  electronic hardware  can  operate  without  software  and  as  the  distinction  between  software  %(q:by  itself)  and  %(q:software producing technical results), the product of legal casuistry, is indefinable in practice as all software is made to run on a computer or an electronic component, either as a system or as an application. This extension of the scope of application of patentability could thereafter be extended without limit to software programmes and intellectual methods at successive legal rulings of the technical chambers of the EPO, irrespective of the exclusion provided for in Article 52 of the EPC.

#AyE: Although for the time being the scope of application of the Commission's proposal for a directive concerns computer-implemented inventions, to which are attached the classic, cumulative criteria limiting the field of application of patentability - which will not satisfy those in favour of purely and simply abolishing all limits on the field of application of patent law - the text is, nonetheless, a de facto acceptance and justification of the a posteriori drift of EPO jusriprudence.  While at first glance the directive seems to advocate something less extreme than the pure and simple abolition of Article 52(2) of the EPC, which is what the EPO executive and some Council members want, it does nonetheless open the way to the future patentability of the entire software field, in particular by the admission that the %(q:technical effect) can amount to the simple fact of a program running on a standard computer.

#Ohf: One may well wonder what the real objective of the Directive is, in particular given the explanatory memorandum, which begins with considerations about the need to protect the software industry against piracy, and in the documents appended to the Directive discusses almost exclusively software and the %(q:software industry), whose influence on the proposal seems excessive yet entirely irrelevant, if the scope of application was really as limited as the Commission maintains.

#IWW: Is it wise in today's world to widen the scope of patents, tools of the industrial age, to intellectual works which are immaterial, such as software, and to the results of running software on a computer? The reply is quite explicit and partisan in the presentation of the proposal for a directive and the impact assessment form.  The narrow field of vision that has been adopted, based on the legal regime for patents as the sole motivation, without sufficient consideration of the economic factors, the impact on research or on European companies, which therefore lacks a view of the whole, is not consistent with the importance of the implications for society, for development and indeed for democracy (e-administration, education, citizens' information), which in the longterm is what is at stake.

#IeW: It is hardly plausible to have us believe that the directive would only be a sort of reversible three-year experiment, at the end of which an assessment would be made.  Rights would have been acquired and in any event there would be uncertainty and perhaps even legal chaos.  In fact the process would be irreversible, with largely unknown effects on our economies and societies, although certain trends can already be deduced: brakes on innovation and interoperability, risk of internet segmentation, increase in access costs, pressures on the choice for consumers of open source software and its type of profitability for authors and providers of internet and network services and applications adapted to use this type of software.

#TWW2: The Committee considers that given the lack of independent, in-depth, serious economic and impact studies, in particular on SMEs-SMIs, employment and long-term social impact, it would be dangerous to rush legislation through to extend the arrangements for patents to an indefinite number of software programmes considered to produce a %(q:technical effect), but that it would be more appropriate to harmonise laws and, by a knock-on effect, the jurisprudence of the member countries by confirming, as is already the case in most member countries, the possibility of allowing patents for technical inventions that include specific dedicated code indispensable for them to operate (but not those solely or mainly in the software) or which would use standard software almost exclusively).

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: esc0209 ;
# txtlang: en ;
# multlin: t ;
# End: ;

