\begin{subdocument}{esc0209}{ESC 2002-09: Europe should reconfirm Non-Patentability of Software!}{http://swpat.ffii.org/papiers/eubsa-swpat0202/ces0209/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise  par }{The Economic and Social Council of the European Union, a consultative organ of experts from various fields, criticises the European Patent Office's software caselaw and the European Commission's proposal for a software patentability directive and asks the European Parliament to reject the proposal and instead ask for a reconfirmation of the non-patentability of software.  This study met strong resistance from a group of supporters of the European Patent Office, but was in the end passed with a 2/3 majority.}
\begin{itemize}
\item
{\bf {\bf source\footnote{retureau0209.de\_DE@euro.pdf}}}
\filbreak
\end{itemize}

\begin{quote}
{\it 3.1}

{\it La directive permet de breveter un  ordinateur  ou  un  r\'{e}seau  programm\'{e}s,  ou  un  ``proc\'{e}d\'{e}'' r\'{e}alis\'{e}  par  l'ex\'{e}cution  d'un  programme.  Toute  innovation  r\'{e}alis\'{e}e  de  cette  mani\`{e}re  est  en  effet  consid\'{e}r\'{e}e d'office  comme  }

{\it M\^{e}me si le champ d'application de la directive propos\'{e}e par la Commission concerne pour l'instant les inventions mises en oeuvre par ordinateur, auxquelles sont attach\'{e}s les crit\`{e}res cumulatifs classiques d\'{e}limitant le domaine d'application de la brevetabilit\'{e}, ce qui ne satisfera pas les partisans de l'abolition pure et simple de l'abolition de toute limite au domaine d'application du droit des brevets, ce texte n'en constitue pas moins, de fait, une acceptation et une justification a posteriori de la d\'{e}rive jurisprudentielle de l'OEB.  Tout en pr\'{e}sentant \`{a} premi\`{e}re vue une position moins extr\^{e}me que l'abolition pure et simple de l'article 52.2 CBE que souhaitent la direction de l'OEB et certains membres du Conseil, cette directive n'en serait pas moins une porte ouverte \`{a} la brevetabilit\'{e} future de la totalit\'{e} du domaine des logiciels, notamment par l'admission que ``l'effet technique'' peut \^{e}tre le fait du logiciel seul sur un ordinateur standard.}

{\it Il est permis de s'interroger sur la finalit\'{e} r\'{e}elle d'une telle directive, en particulier au regard de l'expos\'{e} des motifs, qui s'ouvre sur des consid\'{e}rations relatives \`{a} la n\'{e}cessit\'{e} de prot\'{e}ger l'industrie du logiciel contre le piratage, et \'{e}voque presque exclusivement, dans les documents annexes \`{a} la directive, les logiciels et }

{\it Convient-il aujourd'hui d'\'{e}tendre les brevets, outils de l'\`{e}re industrielle, \`{a} des cr\'{e}ations de l'esprit, immat\'{e}rielles, comme les logiciels et au r\'{e}sultat de leur ex\'{e}cution par ordinateur ? La r\'{e}ponse est tout \`{a} fait explicite et partisane dans la pr\'{e}sentation de la proposition de directive et la fiche d'impact.  Le champ de vision \'{e}troit adopt\'{e}, partant du r\'{e}gime juridique des brevets comme motivation unique, sans consid\'{e}ration suffisante des facteurs \'{e}conomiques, de l'impact sur la recherche, sur les entreprises europ\'{e}ennes, donc sans vision d'ensemble, n'est pas en coh\'{e}rence avec l'importance des enjeux de soci\'{e}t\'{e}, de d\'{e}veloppement et m\^{e}me de d\'{e}mocratie (e-administration, \'{e}ducation, information des citoyens) qui sont en cause \`{a} terme.}

{\it Laisser \`{a} penser qu'il ne s'agirait, pour les trois ans \`{a} l'issue desquels une \'{e}valuation serait men\'{e}e, que d'une sorte d'exp\'{e}rimentation r\'{e}versible, alors que des droits seraient acquis, n'est gu\`{e}re plausible et cr\'{e}erait en toute hypoth\`{e}se une ins\'{e}curit\'{e}, et m\^{e}me \'{e}ventuellement un chaos juridiques.  En r\'{e}alit\'{e}, un processus irr\'{e}versible serait engag\'{e}, aux cons\'{e}quences largement inconnues sur nos \'{e}conomies et nos soci\'{e}t\'{e}s, mais dont certaines tendances annoncent quelques-uns des traits : freins \`{a} l'innovation et \`{a} l'interop\'{e}rabilit\'{e}, risque de segmentation de l'internet et d'augmentation des co\^{u}ts d'acc\`{e}s, pressions sur l'option des logiciels ouverts pour les consommateurs et sur son mod\`{e}le de rentabilit\'{e} pour les cr\'{e}ateurs et les fournisseurs de services internet et de services de r\'{e}seaux et d'applications adapt\'{e}es utilisant ces logiciels.}

{\it Le Comit\'{e} consid\`{e}re qu'en l'absence d'\'{e}tudes \'{e}conomiques et d'impact ind\'{e}pendantes, s\'{e}rieuses et approfondies, en particulier sur les PME-PMI, sur l'emploi, sur l'impact social \`{a} long terme, il serait hasardeux de l\'{e}gif\'{e}rer pr\'{e}cipitamment pour \'{e}tendre le champ d'application du r\'{e}gime des brevets \`{a} un nombre ind\'{e}fini de logiciels consid\'{e}r\'{e}s comme produisant un }
\end{quote}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

