<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: ESC 2002-09: Europe should reconfirm Non-Patentability of Software!

#descr: The Economic and Social Council of the European Union, a consultative organ of experts from various fields, criticises the European Patent Office's software caselaw and the European Commission's proposal for a software patentability directive and asks the European Parliament to reject the proposal and instead ask for a reconfirmation of the non-patentability of software.  This study met strong resistance from a group of supporters of the European Patent Office, but was in the end passed with a 2/3 majority.

#TWW: La directive permet de breveter un  ordinateur  ou  un  réseau  programmés,  ou  un  %(q:procédé) réalisé  par  l'exécution  d'un  programme.  Toute  innovation  réalisée  de  cette  manière  est  en  effet  considérée d'office  comme

#AyE: Même si le champ d'application de la directive proposée par la Commission concerne pour l'instant les inventions mises en oeuvre par ordinateur, auxquelles sont attachés les critères cumulatifs classiques délimitant le domaine d'application de la brevetabilité, ce qui ne satisfera pas les partisans de l'abolition pure et simple de l'abolition de toute limite au domaine d'application du droit des brevets, ce texte n'en constitue pas moins, de fait, une acceptation et une justification a posteriori de la dérive jurisprudentielle de l'OEB.  Tout en présentant à première vue une position moins extrême que l'abolition pure et simple de l'article 52.2 CBE que souhaitent la direction de l'OEB et certains membres du Conseil, cette directive n'en serait pas moins une porte ouverte à la brevetabilité future de la totalité du domaine des logiciels, notamment par l'admission que %(q:l'effet technique) peut être le fait du logiciel seul sur un ordinateur standard.

#Ohf: Il est permis de s'interroger sur la finalité réelle d'une telle directive, en particulier au regard de l'exposé des motifs, qui s'ouvre sur des considérations relatives à la nécessité de protéger l'industrie du logiciel contre le piratage, et évoque presque exclusivement, dans les documents annexes à la directive, les logiciels et

#IWW: Convient-il aujourd'hui d'étendre les brevets, outils de l'ère industrielle, à des créations de l'esprit, immatérielles, comme les logiciels et au résultat de leur exécution par ordinateur ? La réponse est tout à fait explicite et partisane dans la présentation de la proposition de directive et la fiche d'impact.  Le champ de vision étroit adopté, partant du régime juridique des brevets comme motivation unique, sans considération suffisante des facteurs économiques, de l'impact sur la recherche, sur les entreprises européennes, donc sans vision d'ensemble, n'est pas en cohérence avec l'importance des enjeux de société, de développement et même de démocratie (e-administration, éducation, information des citoyens) qui sont en cause à terme.

#IeW: Laisser à penser qu'il ne s'agirait, pour les trois ans à l'issue desquels une évaluation serait menée, que d'une sorte d'expérimentation réversible, alors que des droits seraient acquis, n'est guère plausible et créerait en toute hypothèse une insécurité, et même éventuellement un chaos juridiques.  En réalité, un processus irréversible serait engagé, aux conséquences largement inconnues sur nos économies et nos sociétés, mais dont certaines tendances annoncent quelques-uns des traits : freins à l'innovation et à l'interopérabilité, risque de segmentation de l'internet et d'augmentation des coûts d'accès, pressions sur l'option des logiciels ouverts pour les consommateurs et sur son modèle de rentabilité pour les créateurs et les fournisseurs de services internet et de services de réseaux et d'applications adaptées utilisant ces logiciels.

#TWW2: Le Comité considère qu'en l'absence d'études économiques et d'impact indépendantes, sérieuses et approfondies, en particulier sur les PME-PMI, sur l'emploi, sur l'impact social à long terme, il serait hasardeux de légiférer précipitamment pour étendre le champ d'application du régime des brevets à un nombre indéfini de logiciels considérés comme produisant un

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: esc0209 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

