<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Plooij/ITRE Counter-Proposal: Publication and Interoperation do not Infringe

#descr: The European Parliament's industry committee (ITRE) is working on a counter-proposal for the CEC/BSA software directive proposal, based on a draft by its rapporteur Elly Plooij Van Gorsel, a dutch liberal Member of the European Parliament (MEP).  Plooij's first draft states that patents are not needed for protecting the interests of software creators and proposes to assure that programs can at least be freely distributed without any fear of patent liabilities.  Moreover, any use of a patented method for the purpose of interoperability (compliance with de facto standards) is not considered to be an infringement.  The proposal is strong on defininig these limits but stops short of correcting CEC/BSA misconceptions about %(q:computer-implemented invention), %(q:technical contribution) etc, thus leaving it undecided whether Amazon One-Click and the like are patentable inventions or not.  Current law says they are not, but the European Patent Office (EPO) says they are, and the European Commission wants to bring the law in line with the EPO's position.  The ITRE counter-proposal solves only part of the problem, but fellow MEPs in ITRE have tabled amendments.  Some of these are designed to complete the solution, others to dismantle it.

#iaP: Plooij's Initial Draft: Preface

#eWm: Preamble Amendments and Lack Thereof

#iet: Amendments to Articles

#dln: Amendments to Plooij's Amendments

#utl: The ITRE committee has realised of the problems in the directive proposal by the CEC and has reacted to protect the European economy from an unclear and more than potentially harmful text. The rapporteur Elly Pooij van Gorsel has adressed the main problems (by disclaiming software is in a technical field, ruling out monopolisation of information handling, etc), and many of the rest of MEPs in the committee have completed and refined her amendments to provide a precise definition of technical contribution, patentability criteria, and the structural challenges in the EPO, as well as trying to reduce the problems in the preamble. Other amendments, more aligned with the EPO's position, risk worsening the effects of the CEC proposal on the Lisbon goals, e-Europe, the European economy and the rights of citizens. This means stakes are high for the EU in the forthcoming voting in the ITRE committee. It's up to EU citizens and their representatives to correct the fatal development of the patent system on which the EPO and other patent offices have embarked a few years ago, or to live with the consequences of a system efectivelly as dysfunctional as that of the USA.  Plooij's approach would not be as clear, safe and sound as %(ep:ours), but depending on the votes in ITRE may well help to alleviate some of the worst aspects of the economic and constitutional disaster for which the European patent establishment is asking the European Parliament to give its stamp of approval.

#itrat: Plooij Amendment Proposals

#itrad: 8 amendments proposed in 2003-12, version of 2003-02-19

#itret: ITRE MEP Amendments to the Amendments

#itred: some attempts to complete the Plooij amendments by further delimitations of what is technical and what not, some to dismantle the Plooj amendments by introducing program claims.

#Wey: The ITRE report with the amendments as voted by the MEPs.  Much of the vote followed our recommendations, thanks to support by the center-left parties.  On the central points, i.e. definition of the technical invention by forces of nature and by exclusion of data processing, the socialists sided with the conservatives, leaving only greens and liberals supporting a limit of patentability.

#srm: The initial draft's preface says:

#dWW: Patent and copyright protection are complementary and may overlap.

#Woe: Europe's patent law tradition, as expressed in the European Patent Office's fingerprint logo and in court decisions such as %(DP), sees it differently.  Regimes of intellectual property should not overlap.  Patents are a contextual variant of copyright: a specialised form of intellectual property for a special case, the technical invention (i.e. problem solution involving forces of nature as indispensable constituent).  Just like copyright, patents should not cover broad ideas which many people are likely to find and use in their own creations.  Rather, they should cover creative implementations.  In general, one intellectual creation should be covered by only one property right.  Creative schemes for using forces of nature are covered by patents while creations of pure reason are covered by copyright, and the legislator must either provide a clear borderline which avoids overlapping or integrate the two into one system.

#eri: In computer terms, the actual code (whether machine-readable or in a form which is intelligible to human readers) would be subject to copyright protection, while underlying technological ideas may be eligible for patent protection.

#bax: Software is a tissue of calculation rules (algorithms), which is not adequately described by dichotomies such as %(q:expression vs idea) or %(q:code vs technology).  Moreover, patents do not protect software developpers but threaten them.

#osW: Software copyright is designed to keep %(q:underlying ideas) free from property claims.  If ideas within a creation can become property, then the owner of an idea can expropriate the creator of an independent work which happens to use that idea.  I.e. Plooij could be forced to withdraw her amendment proposals, because some other politician had a patent on the idea of %(q:creating legislation by submitting amendments which redefine central terms of a law proposal).

#Wri: Patent law gives the holder of a patent for a computer-implemented invention the right to prevent third parties from using software incorporating any new technology he has invented (as defined by the patent claims).

#ocx: Patent Office Decisions are not Law, and they may run counter to the law.  By directly reading the current patent law %(ep:EPC of 1973) and %(lt:legal textbooks), one will find that patent law gives the patent holder is the right to prevent third parties from using the invention (including computer-implemented inventions, such as a computer-controlled chemical reaction). Software cannot incorporate this invention, because programs for computers are not part patentable %(q:technology).  Patents can only prevent third parties from using the software to perform the invention (e.g. control a chemical reaction), not from simulating it on a computer.  Copyright will of course prevent third parties from executing an author's program without permission.

#mWt: The preface contains some more problematic statements.  We have only singled out a few of them in order to clarify some basic points.

#Dtn: Unlike the Preamble of the Directive, this preface however is not subject to amendments or extensive discussions in the parliament.  It is only a personal expression of opinion, as could come from any other participant of the discussion.

#fsm: Fortunately for us, Plooij's preface does express a will to question and limit patentability:

#Vio: However, it is clear that, despite the Commission's claims, [ the proposed directive ] paves the way to a broader use of patents as a model for protecting computer software. Two types of questions remain open: the political expediency of such a move, and, if patentability is regarded as politically desirable, the criteria for defining the borders of patentability in such a way that abuses and perverse effects are avoided.

#WWs: Important point, but the justification makes it more clearly than the Amendment.  The Amendment still transports some patent ideologemes, such as an overstressing of the importance of %(q:inventors) and %(q:the inventive process) for society.  %(q:Maximum advantage) for %(q:innovative companies) still seems to weigh more than the overall macro-economic productivity or the constitutional principle of %(q:general freedom of action).

#uie: Rewriting the Preamble is of crucial importance, as it specifies (or should specify) the political goals in the light of which patent judges will interpret the articles in the likely event of finding some ambiguity in them.

#pgn: The CEC/BSA preamble is still full of misguided goal statements which need to be amended.  Some have been taken up by other MEPs.

#vbs: A computer-implemented invention is an invention (= a technical contribution) for whose implementation a computer is used.  It should not be necessary to define this term at all, but the Plooij counter-proposal does at least partially succeed in debugging the CEC/BSA's obfuscation attempts.

#lWr: Unfortunately, ideas which are %(q:wholly realised by computer programs) are still considered as admissible candidates for being an %(q:invention) and containing a %(q:technical contribution).  This seems to imply that algorithms (calculation rules) are patentable inventions.

#mof: The rationale also mentions only %(q:business methods) as non-patentable.  It seems to have been overlooked that calculation rules, not business methods, are the real problem.  If calculation rules are patentable, then business methods are also patentable.  Calculation rules are broader and more all-encompassing.  E.g. the linear optimisation algorithms can be applied to oil refinery as well as to the problem of the travelling saleseman.

#iaa: Phrases like %(q:in a significant way) etc will not eliminate a single software patent, however trivial or otherwise undesirable it might be.  See also %(TR).  For delimiting the field of patentability, the following approaches might work, especially when combined:

#ogt: a definition of %(q:technical invention) along the lines of traditional patent doctrines, such as

#nie: teaching about a plan-conformant use of controllable forces of nature for achieving a causally overseeable effect, which is the immediate result of the forces of nature

#dAc: German Federal Court of Justice Anti Blocking System Decision of 1980

#cio: advanced utilisation of laws of nature

#pWt: Japanese Patent Act

#sWe: problem solution involving controllable forces of nature

#osf: teaching about cause-effect relations in use of controllable forces of nature

#lsi: a listing of entities or activities which are not inventions, such as

#atI: Programs for Computers are not Patentable Inventions

#htW: Art 52(2) EPC

#roT: Data Processing is not a Field of Technology

#tpm: constraints on patent claims, e.g.

#tWm: Art 5 of the CEC/BSA proposal, which excludes information claims

#gpt: A %(q:computer program product) is not a result of material production and therefore not a product in the sense of Art 27 TRIPs

#erh: Patent claims shall not be directed to the mere operation of generic data processing equipment (i.e. universal computer with periphery for communication with other computers or humans) but must comprise the operation of hardware which serves to control forces of nature in an inventive way.

#eWl: a catalogue of model decisions on example patents, in which the use of the terminology is clarified and which may be periodically revised by the Parliament.

#sWW: CEC/BSA does this by referring to the EPO's %(q:Pension Benefit System) decision

#WoW: Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.

#utb: Member States shall ensure that a computer-implemented invention is patentable only on the condition that it makes a technical contribution as defined in Article 2(b).

#nno: This wording makes the article consistent with the previous amendments.

#Wia: Problem: %(q:technical contribution) and %(q:invention) are synonyms in patent language.  The criterion of %(q:technical contribution) is implicit in the invention concept.  It can not be formulated as an independent criterion in parallel to novelty, inventivity and industrial applicability.  The question is whether an invention (= a technical contribution) is present at all.  Whether it is %(q:computer-implemented) or not is irrelevant.  The creation of special patentability rules for %(q:computer-implemented inventions) may even be seen as violations of Art 27 TRIPs.   We would write something like:

#hrs: Member States shall ensure that technical inventions are patentable and that rules of organisation and calculation are not patentable, regardless of whether for their implementation a computer is used and of how they are presented in the claims.

#svm: Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.

#eee: Deleted

#dnW: This wording becomes redundant with the previous amendments.

#lcn: 3. The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.

#eec: 3. The significant extent of the technical contribution shall be assessed by consideration of the difference between the technical elements included in the scope of the patent claim considered as a whole and the state of the art.  Elements disclosed by the person applying for a patent over a period of six months before the date of the application are not considered as being part of the state of the art when assessing that particular claim.

#rbe: Problem

#raW: The novelty grace period provides no relief from the pain of software patents.  On the whole it will hardly make a difference, and it may even do more harm than good.  Introducing it solely for %(q:computer-implemented inventions) seems in conflict with Art 27 TRIPs.

#tnW: The wording %(q:as a whole) leads to unlimited patentability.  It means that we are not comparing the real invention but the claim wording with the state of the art, i.e. we are handing control over from the examiner to the patent lawyer who drafts the claims.  More generally speaking, it is a bad idea to regulate the formalities of examination by law.  This provision only encourages formalistic approaches and consequently loopholes.

#oui: Solution

#Wss: Delete or replace by a wording from the %(ep:EPO Guidelines of 1978), which you find in two versions in our counter-proposal.

#atn: This states a good intention, but will probably exclude nothing in practise.  Which patent attorney would ever dream of arguing that his algorithm or business method is patentable %(q:merely because it involves the use of a computer)?  Of course it is patentable because it is a useful new idea, and he will have no difficulty saying that it produces %(q:technical effects beyond ...) whatever that may mean.

#rtr: (c) needs explanation.  The rest is very helpful for protecting the freedom of publication as guaranteed by Art 10 ECHR.

#ygo: Again very helpful in protecting a basic civil and economic right.

#eiW: Quite a few MEPs have tabled %(am:amendments to Plooij's Amendments).  Some of these address the problems described above.  Others, especially those from Angelika Niebler of CSU/EPP, copy&paste the %(dk:Danish Patent Office's %(q:European Council Patent Working Party's Compromise Paper)), which introduces program claims and de facto allows patents on algorithms and business methods while pretending to introduce restrictive wording.

#WiW: Below are our opinions about the tabled amendments.

#n9b: Amendment 19=21 is better

#ioj: Niebler repeats Plooij's proposal, apparently in order to insert the DKPTO rationale for it.

#8Wt: 28 is better

#ljh: deleting art 4.2 was OK with amendment 5, but rejecting art 5 might make it worthwhile to keep art 4.2

#sWa: Amendment 32 is better.  It does not include the grace period nor the %(q:significant extent) qualifier.

#Wma: It would be nice to keep the points c and d of amendment 9, but apply also amendments 37=38 and 40.  Can amendment 9 be voted favorably and then 37=38 and 40 passed too?

#aib: 18 is better.  %(q:in the context of an apparatus such as a computer) could be seen as referring to software, while %(q:with the help of a computer) indicates more clearly that we are talking about material processes which may be controlled by software but where this software is not of interest for the patentee.  The english version of the justification text needs polishing.

#phs: Very good and important definition.  Again the english version of the justification text is not as well written.

#oW1: The English translation of 21 does not include a minor %(q:et) that was in the original French 21 and not in the original French 19, which led us to prefer 19.

#cyt: This patent lobby proposal (modelled on DKPTO text and UNICE/BDI papers) considers all software innovations to be %(q:inventions) and therefore implicitely patentable.

#Wtl: Systematically correct, but dangerous if the other systematic errors are not corrected as well.  It needs to be made clear that %(q:invention) is not the %(q:set of claimed features) but only a subset thereof, a synonym for %(q:technical contribution), %(q:technical teaching) or %(q:problem solution whose disclosure is to be rewarded by a patent).  This problem solution must be both new and technical.  Forces of nature must be a constituent part of it.  If non-obviousness and technicity are separated, as proposed here, it must by other means be assured that they refer to the same object.

#ebs: Could be further improved.  %(q:predictable) is not as clear as %(q:causally overseeable).  %(q:physical effects beyond the representation of information) etc still allows an EPO-like argumentation that a %(q:further technical effect) is achieved by software, even when the problem solution does not involve forces of nature.  Without some examples it will be difficult to communicate the legislator's intention to the patent courts.  Amendment 25 does not mention %(q:distributing).  This however does not make much of a difference in the context of determining what is an invention.

#NtO: part of Niebler's attempts to put the CEU/DKPTO document in

#cie: It would be more correct and perfectly sufficient to simply define %(q:invention) or even %(q:computer-implemented invention) as implicitely belonging to a technical field.

#tsn: Good intentions but not clear enough.  It should rather be made clear that an invention *is* a technical contribution.

#WWd: part of Niebler's attempt to put the DKPTO document in

#WWd2: part of Niebler's attempt to put the DKPTO document in

#aee: Difficult to say which is better.  The %(q:significant extent) qualifier adds ambiguity, as does the qualifier %(q:as a whole).  The %(q:significant extent of the technical contribution) can only be a synonym of %(q:the technical contribution), which again is a synonym of %(q:the invention).  Some of the features described in a patent claim may be %(q:insignificant) for assessing patentability, but then that is because these features are not part of the invention (but only serve to describe the scope of exclusivity, which is derived from but not equal to the invention).  By saying %(q:as a whole), patent lawyers will be induced to consider these non-invention features as part of the invention.  The key to solving this problem is to distinguish clearlyl between %(q:invention) and %(q:claimed object).  Qualifiers show that MEPs are struggling with the problem.

#sgn: Contains flimsy EPO terminology such a %(q:further technical effects beyond normal interactions between program and computer) which has in the past allowed patent offices to grant patents for mathematical methods and business methods (even without much of an effort at verbal clothing) .

#WiW2: Malcolm Harbour attempts to impose information claims (program product etc) and directly violate Art 10 ECHR (freedom of publication).

#pWu: Angelika Niebler attempts to impose information claims and violate Art 10 ECHR, using the DKPTO wording and a Justification which tries to further hide the effect of the proposed nebulous wording.

#gte: In general, there is nothing to be said against claiming products and processes.  This amendment tries to draw a line between patentable material processes and information processes.  It seems doubtful whether the approach of restricting process claims to %(q:technical production processes) is tenable.  If this amendment is adopted, it should later be revised to something more elegant, such as a separate clause %(bc:Patent claims shall not be directed to the mere operation of generic data processing equipment (i.e. universal computer with periphery for communication with other computers or humans) but must comprise the operation of hardware which serves to control forces of nature in an inventive way.

#nds: The original French amendment 40 exempted distribution of information from being considered an infringement and therefore was better. The English translation is the same. A serious translation error, it seems.  Plooij's proposal already exempted distribution.

#Wei: To be examined

#nin: Corrects a clerical error in the original text.  %(q:patent protection) is euphemistic, %(q:patenting) or %(q:patentability) would be better.  Niebler's %(q:justification) is strange: %(q:.. impact ... will not depend on the granting of patents as such, but on how patent-holders enforce their patent protection).  The presence of patents, even if unenforced, has of course an impact.

#nie2: Doesn't make anything worse nor better.  The important problem is: the European Commission has, by this directive proposal, sufficiently demonstrated that it is political ally of the patent establishment.  Should it be trusted to do further monitoring?  Why not entrust an organisation that is less dependent on the patent establishment, such as %(ESC) or the EuroParl?  Why not specify in detail what should investigated and reported?  Why not do some of this reporting first, before new property rights are irreversibly created?

#thg: McCarthy suddenly published this paper simultaneously with the ITRE amendments, when she saw that they didn't go into the direction of CEC/BSA/McCarthy (e.g. patentability of anything useful, with several layers of safeguard to make sure that there can be no limits).  Also, in the last minute, McCarthy talked to Plooij to have her water down the ITRE proposal by oral amendments.  Plooij seemed persuaded by McCarthy but the other ITRE members, including those from EPP, insisted on a vote based on the already tabled amendments.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: itre0212 ;
# txtlang: en ;
# multlin: t ;
# End: ;

