<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: The BSA/CEC Proposal(s) and EPC-based Counter-Proposal

#descr: The European Commission proposed on 2002-02-20 to consider computer programs as patentable inventions and make it very difficult not to grant a patent on an algorithm or a logical functionality, including business method, that is claimed with the typical features of a computer program (e.g. computer, i/o, memory etc).  Below you find a tabular listing of this proposal, compared to the BSA draft and an early version of the FFII counter-proposal.

#Pem: Preamble

#TAc: The Articles

#cRW: %(nl|Proposal for a|DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL|%(s:on the patentability of computer-implemented inventions))

#cRW2: %(nl|Proposal for a|DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL|%(s:on the limits of patentability with respect to computer programs))

#TEO: THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,

#HiW: Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof,

#HWo: Having regard to the %(fn:39:proposal from the Commission),

#HpW: Having regard to the %(fn:40:opinion of the Economic and Social Committee),

#AtW2: Acting in accordance with the %(fn:41:procedure laid down in Article 251 of the Treaty),

#Wee: Whereas:

#Een: Effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#TWE: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market.  Effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Tse: The realisation of the internal market requires the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. A %(s:reliable and equitable protection of investments in software development) is an important element for the success of the internal market.  %(s:Innovative software developpers should not have to fear that others take away the fruits of their labor either by %(tp|copying|plagiarism) or by excluding them from the use of basic ideas and interfaces.  The software market should be a level playing field where many small companies can flourish and grow by developping high quality interoperable software).

#Dao: Differences exist in the legal protection of computer-implemented inventions offered by the administrative practices and the case law of the different Member States. Such differences could create barriers to trade and hence impede the proper functioning of the internal market.

#Ccy: Computer-implemented rules of organisation and calculation (mathematical methods, plans and methods for mental and commercial activity, programs for computers, presentation of information etc) are not considered patentable inventions by the laws of the member states.  The substantive patent law of the member states is already completely harmonised thanks to the enactment of Art 52-57 of the European Patent Convention of 1973 into national laws.  However the rules laid down in these laws have not been uniformly observed.  In particular the European Patent Office (EPO) has, starting in 1986, gradually changed its interpretation of Art 52 in such a way that computer-implemented rules of organisation and calculation are de facto treated as patentable inventions and the written law loses much of its delimiting meaning.  Some national courts have more or less followed the EPO's interpretation while others have not.  Even within national jurisdictions the policies of the courts diverge.

#San: Such differences have developed and could become greater as Member States adopt new and different administrative practices, or where national case law interpreting the current legislation evolves differently.

#Nua: Recently caselaw standards have been evolving on a more and more pan-european level.  Different national courts have adopted similar rules where their national laws allowed this.  Given that Europe's susbstantive patent law is already unified, a uniform development of national caselaws could normally be expected to occur.  However, due to the EPO's decisions of 1986 and later, european courts are now faced with two competing sets of rules.  This situation calls for a legislative decision.

#TWe: The steady increase in the distribution and use of computer programs in all fields of technology and in their world-wide distribution via the Internet is a critical factor in technological innovation. It is therefore necessary to ensure that an optimum environment exists for developers and users of computer programs in Europe.

#Tos: The steady increase in the distribution and use of computer programs in all fields of technology and in their world-wide distribution via the Internet is a critical factor in technological innovation. It is therefore necessary to ensure that an optimum environment exists for developers and users of computer programs in the Community.

#Aui: Already in 1973 computer programs were commonly used to control industrial engineering processes.  Informatisation has since then made further progress.  The Universal Computer has made the development of new ideas easier.  Processes that formerly needed to be verified through experimentation with forces of nature have since the 1950s been broken down to abstract-logical models, within which innovation conveniently evolves as a series of small mental steps of abstraction, calculation, deduction and mathematical proof.  The legislators of 1973 decided that abstract-logical innovation should stay free of patents.  Recent experiences and economic research have only confirmed the wisdom of this policy decision. The emergence of a knowledge-based economy has raised stakes of involved parties and the public at large.  It has become necessary to reassure developers and users of computer programs in the Community that Europe will continue to respect and protect their copyright-based property and their freedom to publish and use their original works.

#TWv: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer-implemented inventions and provide an incentive for investment and innovation.

#TWl: Therefore, the legal rules on the limits of patentability as interpreted by Member States' courts should once again be brought in line with the written law and codified in a less misunderstandable form.  The resulting legal certainty should be conducive to innovation and investment in the area of software.

#Ttn3: The European Community and its Member States are bound by the TRIPS Agreement, Article 27(1) of which provides that, subject to only to certain exceptions as set out in paragraphs (2) and (3) of that Article, patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application.  Moreover, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to inventions which are the subject-matter of this Directive.

#TcW: The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/CEC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the %(fn:42:agreements reached in the Uruguay Round multilateral negotiations (1986-1994)). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.

#Wjr: The Community and its Member States are bound by the %(tr:Agreement on trade-related aspects of intellectual property rights), approved by Council Decision 94/800/CEC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the %(fn:42:agreements reached in the Uruguay Round multilateral negotiations (1986-1994)). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available without discrimination as to the field of technology.  %(s:This means that patentability must be effectively limited in terms of general concepts such as %(q:invention), %(q:technology) and %(q:industry), so as to avoid commercial monopolies or ad hoc policies, both of which would act as barriers to free trade.  Thus technical inventions, including those that use a computer, must be patentable no matter %(tp|to which engineering discipline they belong|which forces of nature they help to harness).  Data processing, no matter whether performed by the human mind or by means of a computer, is not a field of technology in the sense of patent law).

#Urh: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology.

#Uys: Under the European Patent Convention (EPC) and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This %(s:exclusion) applies and is justified only to the extent that a patent application relates to such subject-matter or activities as such.  %(s:Thus it might not apply if the disclosed invention does not reside in the computer program as such but in a %(tp|technical invention|e.g. new chemical process) which is carried out under program control.  In this case the program itself would be freely implementable for simulation purposes and the like, but the chemical process would fall under the patent).

#Pha: Patent protection allows innovators to benefit from their creativity. Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anti-competitive.

#Pri: Patents are temporary monopolies granted by the state to inventors in order to stimulate technical progress.  In order to ensure that the system works as intended, the conditions for granting patents and the modalities for enforcing them must be carefully designed.  In particular, inevitable corrollaries of the patent system such as restriction of creative freedom, legal insecurity and anti-competitive effects must be kept within reasonable limits.

#I3H: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs43, the expression in any form of an original computer program is protected by copyright as a literary work. However, ideas and principles which underlie any element of a computer program are not protected by copyright.

#InW: In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, the expression in any form of an original computer program is protected by copyright as an %(s:individual creation).  General ideas and principles which underlie any element of a computer program are %(s:explicitely exempted from copyright).

#Isc: In order for any invention to be considered as patentable it should have a technical character, and thus belong to a field of technology.

#IWd: In order for any idea to be considered an invention in the sense of the patent system it is necessary that it should have a technical character.  Patents are temporary monopolies granted in return for a disclosure of a technical invention.  %(s:Every technical invention belongs to a field of technology, but not every patent claim to an apparent %(q:product or process in a field of technology) also discloses a technical invention).

#AfW: Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they should make a technical contribution to the state of the art.

#Put: Rules of organisations and calculation do not belong to a field of technology but rather to the field of data processing, informatics, mathematics, logics and to some extent even social sciences.  Computer programs consist of entities within an abstract model called %(q:Turing Machine).  However computer programs are commonly used for describing and implementing technical teachings.  In order to be considered as a technical invention, any teaching must represent a contribution to the art of harnessing controllable forces of nature.

#Ahi: Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#Aho: Accordingly, a teaching that does not make a technical contribution to the state of the art is not a technical invention and thus not patentable.

#Aaw: A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.

#Aao: A defined procedure or sequence of actions when performed with help of an apparatus such as a computer may contribute to our knowledge about the cause-effect relations of controllable forces of nature and thereby constitute a patentable invention.  However, an algorithm, regardless of whether the symbolic entities of which it is composed can be interpreted as referring to a physical environment, is inherently non-technical and cannot therefore constitute a patentable invention.  In particular, references to normal realisations of the Universal Computer, the Turing Machine or other mathematical models, whose physical applicability is beyond any doubt, cannot confer a technical character on an algorithm.  A computer program is no more than an algorithm expressed in the most general terms: those of the universal computer.

#Tet: The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive.

#IrW3: In order to reconfirm that rules of organisation and calculation are not inventions, it is not necessary to create a separate body of law in place of the rules of national patent law.  In some countries where courts have not followed the EPO caselaw, no legislation may be needed at all.   In some countries it may be deemed appropriate to delete the equivalent of Art 52(3) (the %(q:as such) clause, which is redundant and has been a source of confusion) from their national patent law.  Member states may also decide to jointly revise the European Patent Convention (EPC) by deleting Art 52(3) and incorporating a definition of %(q:technical invention) in Art 52(1), so as to clearly oblige all courts at the national and european level to follow the letter and spirit of the written law as explicated by this directive.

#Tel2: The Community's legal framework for the protection of computer-implemented inventions can be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not so susceptible.

#AmW: Applying the above principles to eliminate the current differences in the legal protection of computer-implemented inventions and to provide for transparency of the legal situation should also improve the competitive position of European industry in relation to its major trading partners.

#TsW2: This Directive shall be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty as well as the relevant provisions of the Member States' laws with respect to competition and fair business practices.

#Tmo: This Directive shall be without prejudice to other Community legislation such as Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks. The Directive shall also be without prejudice to Member States' laws relating to trade secrets or the law of contract.

#Tsc: This Directive should be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not so susceptible.

#Ttu: The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent.

#TWn2: This Directive shall be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.

#Aod: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#SjW: Since the objectives of the proposed action, namely to harmonise national rules on computer-implemented inventions, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale or effects of the action, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary to achieve those objectives.

#yWn: If this principle is to be taken seriously, the directive should limit itself to specifying in the exactest possible terms which kinds of patents must be refused and which must be granted, and leave internal details of legal theory and patent examination methodology to the member states.  It should in particular not codify esoteric EPO concepts such as %(q:technical contribution in the inventive step).

#Tho: This Directive should be limited to laying down certain principles %(s:concerning the limits of patentability with regard to computer programs), such principles being intended in particular to ensure that %(s:technical inventions are susceptible of patent protection and that rules of organisation and calculations are not so susceptible).

#End: The legal security enjoyed by %(s:European citizens) and the competitive position of European %(s:software entrepreneurs) in relation to Europe's major trading partners would be improved if it was %(s:made clear that Europe will not extend patentability beyond the field of technical inventions).

#DWd: This Directive shall be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.

#Teh: This directive %(s:upholds the %(dp:principle of separation of property systems)).  It ensures that the patent system stays confined to the realm of technical inventions and that %(s:results of abstract reasoning such as algorithms are safeguarded as a public domain and its expression stays within the area of copyright).  If there is any property system that could be allowed to restrict the use of algorithms, then that is copyright.  The public domain of algorithms, executed either in the human mind or on a universal computer, is protected by the freedom of publication and other basic freedoms which form part of the constitutional law of the Community and its member states and are considered to be of particular importance for the development of the information society.

#Ste: Since the objectives of the proposed action, namely to clarify the rules on the limits of patentability with respect to computer programs, can be more easily achieved at the Community level than by the individual member states, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary to achieve those objectives.

#HTD: HAVE ADOPTED THIS DIRECTIVE:

#Sco: Scope

#Tie2: This Directive relates to computer-implemented inventions. The term %(q:computer-implemented invention) includes any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs.

#Tsm: This Directive lays down rules for the patentability of computer-implemented inventions.

#Dit: Definitions

#FWW: For the purposes of this Directive the following definitions shall apply:

#cWe: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;

#cWi: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#Tap: This directive explains the rules concerning the limits of patentability with respect to computer programs.  For the purpose of this Directive the following definitions shall apply:

#Ain: An %(e:invention), also called %(q:technical invention), %(q:technical teaching), %(q:technical solution) or %(q:technical contribution), is %(ps:teaching about cause-effect relations in the use of controllable forces of nature).

#Ase: Alternative: An invention ... is a problem solution involving controllable forces of nature.

#xad: We prefer not to use the propaganda term %(q:computer-implemented invention).  If you feel you really need a definition for this term, we recommend the following counter-poison, derived from the CEC/BSA definition by slight rewriting:

#rWW: A %(e:computer-implemented invention) is any technical solution whose implementation involves the use of a computer, computer network or other programmable apparatus, and which is claimed in terms of one or more prima facie non-novel implementation features realised wholly or partly by means of a computer program or computer programs, whereas the prima facie novel solution features depend wholly or partly on the use of controllable forces of nature.

#Aoj3: A %(e:calculation rule), also called %(e:algorithm), is a teaching about relations between entities within constructions of the human mind, such as models and axiomatic systems, including any formal model of computers such as the Turing Machine.

#AlB: An %(e:organisation rule) is a teaching about relations between material phenomena that are not determined by controllable forces of nature.  Methods of business and social engineering are organisation rules.  Most organisation rules are direct applications of calculation rules to known models of social relations.  %(e:Rules of organisation and calculation) are teachings about cause-effect relations that are not determined by controllable forces of nature.

#OWd: Only inventions (technical contributions) are patentable.  In order to be patentable, an invention must be new, non-obvious and destined for an industrial application.

#CeW: Computer-implemented inventions as a field of technology

#Peh: Patentable inventions as technical teachings

#Ailx: A computer-implemented invention shall be considered to belong to a field of technology.

#Mue: Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.

#Mte: Member states shall ensure that a technical inventions are patentable regardless of whether they are implemented on a computer, and that, conversely, nobody can obtain a patent for rules of organisation and calculation by presenting them in combination with implementation features which do not constitute patentable achievements.  All rules of organisation and calculation shall be treated as part of the body of public knowledge to which a potentially patentable technical contribution is made.

#Tar2: Technical contribution

#Cse: Conditions for patentability

#ItW2: In order to involve an inventive step, a computer-implemented invention must make a technical contribution. A %(q:technical contribution) is a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#Tnc: The technical contribution must be assessed by consideration of the difference between the scope of the patent claim considered as a whole, which may comprise both technical and non-technical features, and the state of the art.

#Mii: Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#Mfm: Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.

#Tba: The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.

#MaW: Member states shall ensure that patents are granted only for technical inventions, which are new, non-obvious and industrially applicable.

#MWh: Member states shall ensure that technical inventions are understood to be technical contributions, definable as %(q:problem solutions completed under use of controllable forces of nature) or %(q:teachings about cause-effect relations of controllable forces of nature).

#MWq: Member States shall ensure that patentability is examined as outlined in %(EP): In assessing whether a claim discloses a technical invention, one must disregard the form or kind of claim and concentrate on the content in order to identify the novel contribution which the claimed subject matter makes to the known art.  If this contribution does not constitute a technical invention, there is no patentable subject matter.

#FWc: Form of Claims

#Cnl: Claims, Infringement, Disclosure, Novelty, Prior Authorship, Industriality

#AWr: A computer-implemented invention may be claimed as a product, in particular as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.

#Msc: Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.

#MWo: Member States shall ensure that claims are examined in the spirit of the %(EP): If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims.  For example, a claim to a processor in combination with a certain program (logical functionality) or to a process for operating a computer under that program would be as objectionable as a claim to the program per se or to a data carrier containing the program.

#sih: Member States shall ensure that patent claims are not directed to the mere operation of generic data processing equipment (i.e. universal computer with devices for communication with other computers or humans) but must comprise the operation of equipment which serve to control forces of nature in an inventive way.

#Dte: Member states shall ensure that the publication or distribution of information in whatever form can never constitute a direct or indirect patent infringement.  Member states shall guarantee the freedom of verbal expression and information exchange regardless of the language and purpose of the texts in question, and shall ensure that this freedom can be restricted only by copyright and never by patents.

#Man: Member states shall ensure that the execution of a computer program on a data processing machine (i.e. computer with devices for storage, input and output of data) can not constitute a direct or indirect patent infringement, whereas a combined action involving program execution, such as the distribution of chemicals produced under program control, may constitute an infringement.

#scrambled: Member states shall ensure that scrambled information is considered to be public knowledge as soon as it is factually possible for the public to obtain this information, even if obtaining it required tedious or illegal procedures of decompilation, descrambling or decryption.

#prekre: Member states shall ensure that any copyrightable work that was in use before the publication date of a patent may be further developped, distributed and used for the operation of data processing equipment by the copyright owner as well as by any persons who have been or will be authorised by the copyright owner at conditions to be determined by the copyright owner.

#progpub: Member states shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program shall be published as a part of description wihtout any restricting licensing terms.

#indapp: Member states shall ensure that patentable inventions must be susceptible of industrial application, and that %(q:industry) in the sense of patent law is the %(e:production of material goods by means of production machinery which, due to its high consumption of matter and energy, is usually operated by a specific %(tp|enterprise|industrial organisation)).  Words of the legislator which were intended to limit patentability may never be interpreted in a way that removes or substantially weakens their limiting meaning.

#RWi: Relationship with Directive 91/250 CEC

#Aod2: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#MtW: Members states shall keep the spheres of copyright and patents clearly separated.  Property in computer programs is acquired and regulated through copyright.  Property in technical inventions is acquired and regulated through patents.   Property that has been acquired through one system may not be restricted by another.  Aspects of a computer program that can not be appropriated through copyright can also not be appropriated through patents, utility certificates or any other property regime.

#interop: Member states shall ensure that whereever a use of a patented technique is needed for communicating with another system (i.e. for converting to and from the conventions of that system), such use is not considered to be a patent infringement.

#Mir: Monitoring

#Tnt: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#Tia2: The Euroean Parliament shall monitor the impact of patents on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#ReW: Report on the effects of the Directive

#Tnt2: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#Typ: The Commission shall send to the European Parliament and the Council within three years as from the date specified in Article 6(1) a report assessing the impact of patents for computer-implemented inventions on the factors mentioned in paragraph 1.

#Iir: In this context, the report will also address the questions as to whether the rules governing the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and whether difficulties have been experienced in respect of Member States where the requirements of novelty and inventive step are not examined prior to issuance of a patent, and if so, whether any steps are desirable to address such difficulties.

#Tme2: The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on

#teW: the impact of patents for computer-implemented inventions on the factors referred to in Article 7;

#wtn: whether the rules governing the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and

#wra: whether difficulties have been experienced in respect of Member States where the requirements of novelty and inventive step are not examined prior to issuance of a patent, and if so, whether any steps are desirable to address such difficulties.

#Toi2: The European Parliament shall form a permanent committee on the criteria of patentability.  The Investigation Committe shall be empowerered to take appropriate means to obtain any needed information from the %(ep:European Patent Office), the European Commission and important industry players.  The European Community retains the freedom to adopt a stricter interpretation of concepts such as %(q:invention), %(q:techical character), %(q:inventivity) and %(q:industrial application) any time later and to apply this stricter interpretation retroactively to patents which were granted under looser interpretations when this is found to be in the public interest.  The Investigation Committee shall among others investigate the following:

#nno: to what extent and under what terms licenses have become available for the patents granted under this directive and to what extent licensing information is accessible to the public.

#Iet: Implementation

#Mnl: Member States shall bring into force the laws, regulations or administrative provisions necessary to comply with this Directive not later than ... They shall forthwith notify the Commission thereof.

#Mvf: Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than [DATE (last day of a month)]. They shall forthwith inform the Commission thereof.

#Wrp: When Member States adopt these provisions, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

#WDi: When Member States adopt those provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.

#Mia: Member States shall communicate to the Commission the provisions of national law which they adopt in the field governed by this Directive.

#EWW2: Entry into force

#TWi2: This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities.

#Ars: Addressees

#Tsh2: This Directive is addressed to the Member States.

#Dau: Done at Brussels,

#Fua: For the European Parliament

#Fhu: For the Council

#Pres: The President

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-kern ;
# txtlang: en ;
# multlin: t ;
# End: ;

