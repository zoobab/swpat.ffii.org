<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Entwürfe von BSA/EUK und EPÜ-basierter Gegenentwurf

#descr: Am 2002-02-20 schlug die Europäische Kommission vor, Comptuterprogramme als patentierbare Erfindungen zu betrachten und es sehr schwer zu machen, eine Patentanmeldung auf einen Algorithmus oder eine logische Funktionalität, einschließlich Geschäftsmethode, abzulehnen, sofern diese in den typischen Begriffen der Informatik (e.g. Rechner, Ein- und Ausgabe, Speicher, Datenbank etc) beansprucht wird.  Im folgenden finden Sie eine tabellarische Auflistung dieses Vorschlages zusammen mit der Vorlage der BSA und dem Gegenvorschlag des FFII.

#Pem: Präambel

#TAc: Die Artikel

#cRW: %(nl|Vorschlag für eine|RICHTLINIE DES EUROPÄISCHEN PARLAMENTS UND DES RATES|über die Patentierbarkeit computerimplementierter Erfindungen)

#cRW2: %(nl|Vorschlag für eine|RICHTLINIE DES EUROPÄISCHEN PARLAMENTS UND DES RATES|über die Grenzen der Patentierbarkeit|im Hinblick auf Computerprogramme)

#TEO: DAS EUROPÄISCHE PARLAMENT UND DER RAT DER EUROPÄISCHEN UNION -

#HiW: gestützt auf den Vertrag zur Gründung der Europäischen Gemeinschaft, insbesondere auf Artikel 95,

#HWo: auf %(fn:39:Vorschlag der Kommission),

#HpW: nach %(fn:40:Stellungnahme des Wirtschafts- und Sozialausschusses),

#AtW2: gemäß dem %(fn:41:Verfahren des Artikels 251 EG-Vertrag),

#Wee: in Erwägung nachstehender Gründe:

#Een: Effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#TWE: Damit der Binnenmarkt verwirklicht wird, müssen Beschränkungen des freien Warenverkehrs und Wettbewerbsverzerrungen beseitigt werden, und es muss ein Umfeld geschaffen werden, das Innovationen und Investitionen begünstigt. Vor diesem Hintergrund ist der Schutz von Erfindungen durch Patente ein wesentliches Kriterium für den Erfolg des Binnenmarkts. Es ist unerlässlich, dass computerimplementierte Erfindungen in allen Mitgliedstaaten wirksam und einheitlich geschützt sind, wenn Investitionen auf diesem Gebiet gesichert und gefördert werden sollen.

#Tse: Die Verwirklichung des Binnenmarktes erfordert, dass Beschränkungen des freien Warenverkehrs und Wettbewerbsverzerrungen beseitigt werden.  Dabei soll ein Umfeld geschaffen werden, welches Innovationen und Investitionen begünstigt.  Ein %(s:verlässlicher und ausgeglichener Leistungsschutz im Bereich der Software-Entwicklung) ist ein wichtiger Baustein für den Erfolg des Binnenmarktes.  %(s:Innovative Software-Entwickler sollten weder durch %(tp|faule Nachahmung|Plagiarismus) noch durch die Monopolisierung von grundlegenden Ideen und Schnittstellen an der Entwicklung und Verwertung ihrer Werke gehindert werden.  Die Software-Branche sollte ein ebenes Spielfeld sein, auf dem viele Kleinunternehmen florieren und wachsen können, indem sie hochwertige interoperable Software entwickeln.)

#Dao: Die Patentpraxis und die Rechtsprechung in den einzelnen Mitgliedstaaten hat zu Unterschieden beim Schutz computerimplementierter Erfindungen geführt. Solche Unterschiede könnten den Handel stören und somit verhindern, dass der Binnenmarkt reibungslos funktioniert.

#Ccy: Computer-implementierte Organisations- und Rechenregeln (mathematische Methoden, Pläne und Regeln für geistige und geschäftliche Tätigkeiten, Programme für Datenverarbeitungsanlagen, Wiedergabe von Informationen usw) werden gemäß den Gesetzen der Mitgliedsstaaten nicht als patentfähige Erfindungen angesehen.  Das materielle Patentrecht der Mitgliedsstaaten ist dank Umsetzung der Artikel 52-57 des Europäischen Patentübereinkommens (EPÜ) von 1973 in nationale Gesetze bereits vollständig harmonisiert.  Allerdings wurden diese Gesetze nicht immer in einheitlicher Weise befolgt.  Insbesondere das Europäische Patentamt hat seit 1986 allmählich seine Auslegung von Art 52 EPÜ schrittweise derart geändert, dass nunmehr in der Praxis computer-implementierte Organisations- und Rechenregeln zu patentfähigen Erfindungen geworden sind und dem Gesetz keinerlei begrenzende Wirkung mehr zukommt.  Einige nationale Gerichte sind der Auslegung des EPA mehr oder weniger gefolgt, andere nicht.  Selbst innerhalb der nationalen Rechtssysteme verfolgen verschiedene Gerichte verschiedene Ansätze.

#San: Die Ursachen für die Unterschiede liegen darin begründet, dass die Mitgliedstaaten neue, voneinander abweichende Verwaltungspraktiken eingeführt oder die nationalen Gerichte die geltenden Rechtsvorschriften unterschiedlich ausgelegt haben; diese Unterschiede könnten mit der Zeit noch größer werden.

#Nua: Mit zunehmend intensiver Meinungsbildung auf nationaler und europäischer Ebene werden Unterschiede im Richterrecht normalerweise eher abgebaut als vertieft.  In den letzten Jahren hat sich die Patentrechtsprechung in vielen Fragen transnational konvergierend entwickelt.  Gerichte verschiedener Länder haben ihre Regeln aneinander angeglichen, wo dies möglich war.  Da das materielle Patentrecht in Europa bereits vereinheitlicht ist, wäre hier normalerweise kein Harmonisierungsbedarf zu erwarten.  Dank der seit 1986 schrittweise veränderten EPA-Rechtsprechung sind die Gerichte in Europa jedoch zwischen zwei miteinander konkurrierenden Rechtsnormen hin und her gerissen.  Diese Situation erfordert eine gesetzgeberische Entscheidung.

#TWe: The steady increase in the distribution and use of computer programs in all fields of technology and in their world-wide distribution via the Internet is a critical factor in technological innovation. It is therefore necessary to ensure that an optimum environment exists for developers and users of computer programs in Europe.

#Tos: Die zunehmende Verbreitung und Nutzung von Computerprogrammen auf allen Gebieten der Technik und die weltumspannenden Verbreitungswege durch das Internet sind ein kritischer Faktor für die technologische Innovation. Deshalb sollte sichergestellt sein, dass die Entwickler und Nutzer von Computerprogrammen in der Gemeinschaft ein optimales Umfeld vorfinden.

#Aui: Bereits 1973 wurden Computerprogramme weitgehend zur Steuerung von industriellen Herstellungsvorgängen eingesetzt.  Seitdem ist die Informatisierung noch weiter vorangeschritten.  Der Universalrechner erleichterte die Entwicklung neuer Ideen.  Vorgänge die ehedem durch Versuche an Naturkräften erforscht und überprüft werden mussten, wurden zunehmend auf gedanklichen Modelle reduziert, innerhalb derer Raum für eine bequeme Innovation in kleinen Schritten des Abstrahierens, Rechnens und Beweisens entstand.  Der Gesetzgeber entschied 1973, dass abstrakt-logische Innovation von Patenten frei bleiben sollte.  Neuere Erfahrungen und wirtschaftswissenschaftliche Studien haben die Weisheit dieser Entscheidung bestätigt.  Mit zunehmender Herausbildung einer Wissensbasierten Wirtschaftsordnung hängt das Wohl der Wirtschaftsakteure ebenso wie für die Öffentlichkeit in stärkerem Maße als früher von der Verlässlichkeit dieser Entscheidung ab.  Es ist nunmehr notwendig geworden, den Entwicklern und Anwendern von Computerprogrammen in Europa die Gewissheit zurückzugeben, dass sie auch künftig ihre eigenen Werke veröffentlichen und nutzen dürfen.

#TWv: Aus diesen Gründen sollten die Rechtsvorschriften, so wie sie von den Gerichten in den Mitgliedstaaten ausgelegten werden, vereinheitlicht und die Vorschriften über die Patentierbarkeit computerimplementierter Erfindungen transparent gemacht werden. Die dadurch gewährte Rechtssicherheit sollte dazu führen, dass Unternehmen den größtmöglichen Nutzen aus Patenten für computerimplementierte Erfindungen ziehen, und sie sollte Anreize für Investitionen und Innovationen schaffen.

#TWl: Deshalb sollten die Regeln über die Grenzen der Patentierbarkeit, die im Laufe der Zeit von den Gerichten der Mitgliedsstaaten auf dem Wege der Rechtsfortbildung geschaffen wurden, erneut in Einklang mit dem Wortlaut und Geist des geschriebenen Rechts gebracht werden und in unmissverständlicher Form fest gehalten werden.  Die dadurch entstehende Rechtssicherheit sollte %(s:zu einem investitions- und innovationsfreudigen Klima im Softwarebereich beitragen).

#Ttn3: The European Community and its Member States are bound by the TRIPS Agreement, Article 27(1) of which provides that, subject to only to certain exceptions as set out in paragraphs (2) and (3) of that Article, patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application.  Moreover, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to inventions which are the subject-matter of this Directive.

#TcW: Die Gemeinschaft und ihre Mitgliedstaaten sind auf das Übereinkommen über handelsbezogene Aspekte der Rechte des geistigen Eigentums verpflichtet (TRIPS-Übereinkommen), und zwar durch den Beschluss des Rates 94/800/EG vom 22. Dezember 1994 über den Abschluss der Übereinkünfte im Rahmen der multilateralen Verhandlungen der Uruguay-Runde (1986-1994) im Namen der Europäischen Gemeinschaft in Bezug auf die in ihre Zuständigkeiten fallenden %(fn:42:Bereiche). Nach Artikel 27 Absatz 1 des TRIPS-Übereinkommens sollen Patente für Erfindungen auf allen Gebieten der Technik erhältlich sein, sowohl für Erzeugnisse als auch für Verfahren, vorausgesetzt, sie sind neu, beruhen auf einer erfinderischen Tätigkeit und sind gewerblich anwendbar. Gemäß dem TRIPS-Übereinkommen sollten ferner ohne Diskriminierung nach dem Gebiet der Technik Patente erhältlich sein und Patentrechte ausgeübt werden können. Diese Grundsätze sollten demgemäß auch für computerimplementierte Erfindungen gelten.

#Wjr: Die Gemeinschaft und ihre Mitgliedstaaten sind auf das Übereinkommen über handelsbezogene Aspekte der Rechte des geistigen Eigentums verpflichtet (TRIPS-Übereinkommen), und zwar durch den Beschluss des Rates 94/800/EG vom 22. Dezember 1994 über den Abschluss der Übereinkünfte im Rahmen der multilateralen Verhandlungen der Uruguay-Runde (1986-1994) im Namen der Europäischen Gemeinschaft in Bezug auf die in ihre Zuständigkeiten fallenden Bereiche. Nach Artikel 27 Absatz 1 des TRIPS-Übereinkommens sollen Patente für Erfindungen auf allen Gebieten der Technik erhältlich sein, sowohl für Erzeugnisse als auch für Verfahren, vorausgesetzt, sie sind neu, beruhen auf einer erfinderischen Tätigkeit und sind gewerblich anwendbar. Gemäß dem TRIPS-Übereinkommen sollten ferner ohne Diskriminierung nach dem Gebiet der Technik Patente erhältlich sein und Patentrechte ausgeübt werden können. %(s:Dies bedeutet, dass technische Erfindungen, einschließlich derer, bei deren Ausführung ein Rechner zum Einsatz kommt, unabhängig davon patentierbar sein müssen, zu welchem %(tp|technischen Gebiet|z.B. Mechanik, Elektrik, Chemie) sie gehören.  Datenverarbeitung, egal ob vom menschlichen Geist oder mit hilfe elektronischer Datenverarbeitungsanlagen durchgeführt, ist kein Gebiet der Technik im Sinne des Patentrechts).

#Urh: Nach dem Übereinkommen über die Erteilung europäischer Patente (Europäisches Patentübereinkommen) vom 5. Oktober 1973 (EPÜ) und den Patentgesetzen der Mitgliedstaaten gelten Programme für Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien, mathematische Methoden, ästhetische Formschöpfungen, Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele oder für geschäftliche Tätigkeiten sowie die Wiedergabe von Informationen ausdrücklich nicht als Erfindungen, weshalb ihnen die Patentierbarkeit abgesprochen wird. Diese Ausnahme gilt jedoch nur, und hat auch ihre Berechtigung nur, sofern sich die Patentanmeldung oder das Patent auf die genannten Gegenstände oder Tätigkeiten als solche bezieht, da die besagten Gegenstände und Tätigkeiten als solche keinem Gebiet der Technik zugehören.

#Uys: Nach dem Übereinkommen über die Erteilung europäischer Patente (Europäisches Patentübereinkommen) vom 5. Oktober 1973 (EPÜ) und den Patentgesetzen der Mitgliedstaaten gelten Programme für Datenverarbeitungsanlagen, Entdeckungen, wissenschaftliche Theorien, mathematische Methoden, ästhetische Formschöpfungen, Pläne, Regeln und Verfahren für gedankliche Tätigkeiten, für Spiele oder für geschäftliche Tätigkeiten sowie die Wiedergabe von Informationen ausdrücklich nicht als Erfindungen, weshalb ihnen die Patentierbarkeit abgesprochen wird. Dieser %(s:Ausschluss) gilt jedoch nur, und hat auch seine Berechtigung nur, sofern sich die Patentanmeldung oder das Patent auf die genannten Gegenstände oder Tätigkeiten als solche bezieht.  %(s:Sie kommt somit etwa nicht zur Geltung, wenn die offenbarte Erfindung nicht in einem Datenverarbeitungsprogramm als solchem liegt sondern in einer %(tp|technischen Erfindung|z.B. einem neuen chemischen Vorgang), der durch ein Programm gesteuert wird.  In diesem Falle wäre das Programm selber für Simulationszwecke u.ä. frei umsetzbar, aber der chemische Vorgang unterfiele dem Patent).

#Pha: Der Patentschutz versetzt die Innovatoren in die Lage, Nutzen aus ihrer Kreativität zu ziehen. Patentrechte schützen zwar Innovationen im Interesse der Gesellschaft allgemein; sie sollten aber nicht in wettbewerbswidriger Weise genutzt werden.

#Pri: Patente sind zeitweilige Monopole, welche der Staat Erfindern gewährt, um den technischen Fortschritt zu fördern.  Um sicher zu stellen, dass das System wie vorgesehen funktioniert, müssen die Bedingungen der Erteilung und Durchsetzung von Patenten umsichtig festgelegt werden.  Insbesondere müssen unvermeidbare Begleiterscheinungen des Patentwesens wie die Beschränkung der Schaffensfreiheit, Rechtsunsicherheit und kartellfördernde Wirkungen in vernünftigen Grenzen gehalten werden.

#I3H: Nach der Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 über den Rechtsschutz von Computerprogrammen43 sind alle Ausdrucksformen von originalen Computerprogrammen wie literarische Werke durch das Urheberrecht geschützt.  Die Ideen und Grundsätze, die einem Element eines Computerprogramms zugrunde liegen, sind dagegen nicht durch das Urheberrecht geschützt.

#InW: Nach der %(fn:43:Richtlinie 91/250/EWG des Rates vom 14. Mai 1991 über den Rechtsschutz von Computerprogrammen) sind alle Ausdrucksformen von originalen Computerprogrammen wie literarische Werke durch das Urheberrecht als %(s:individuelle Schöpfungen) geschützt.  Die Ideen und Grundsätze, die einem Element eines Computerprogramms zugrunde liegen, sind %(s:durch das Urheberrecht aus gutem Grund ausdrücklich vom Schutz ausgenommen.)

#Isc: Damit eine Erfindung als patentierbar gilt, sollte sie technischen Charakter haben und somit einem Gebiet der Technik zuzuordnen sein.

#IWd: Damit eine Idee als Erfindung im Sinne des Patentrechts gelten kann, muss sie technischen Charakter aufweisen.  Patente sind zeitweilige Monopole die im Gegenzug für die Veröffentlichung einer technischen Erfindung erteilt werden.  %(s:Jede technische Erfindung ist einem Gebiet der Technik zuzuordnen, aber nicht jeder Patentanspruch auf ein vermeintliches %(s:Erzeugnis oder Verfahren auf einem Gebiet der Technik) offenbart auch eine technische Erfindung).

#AfW: Zwar werden computerimplementierte Erfindungen einem Gebiet der Technik zugerechnet, aber um das Kriterium der erfinderischen Tätigkeit zu erfüllen, sollten sie wie alle Erfindungen einen technischen Beitrag zum Stand der Technik leisten.

#Put: %(s:Organisations- und Rechenregeln gehören keinem Gebiet der Technik sondern eher dem Gebiet der Informatik, Mathematik oder Logik an).  Computerprogramme werden jedoch regelmäßig als Mittel zur Umsetzung technischer Erfindungen eingesetzt.  Um als technische Erfindung angesehen zu werden, muss eine computer-implementierbare Lehre einen technischen Beitrag zum Stand der Technik darstellen, m.a.W. sie muss der Öffentlichkeit neue Erkenntnisse über Wirkungszusammenhänge beherrschbarer Naturkräfte vermitteln.

#Ahi: Folglich erfüllt eine Erfindung, die keinen technischen Beitrag zum Stand der Technik leistet, z. B. weil dem besonderen Beitrag die Technizität fehlt, nicht das Kriterium der erfinderischen Tätigkeit und ist somit nicht patentierbar.

#Aho: Folglich ist eine Lehre, die keinen technischen Beitrag zum Stand der Technik leistet, keine technische Erfindung und somit nicht patentierbar.

#Aaw: Wenn eine festgelegte Prozedur oder Handlungsfolge in einer Vorrichtung, z. B. einem Computer, abläuft, kann sie einen technischen Beitrag zum Stand der Technik leisten und somit eine patentierbare Erfindung darstellen. Dagegen besitzt ein Algorithmus, der ohne Bezug zu einer physischen Umgebung definiert ist, keinen technischen Charakter; er stellt somit keine patentierbare Erfindung dar.

#Aao: Bei einer festgelegten Prozedur oder Handlungsfolge, die in einer Vorrichtung, z. B. einem Computer, abläuft, kann es sich insoweit um eine technische Erfindung handeln, wie darin bislang unbekannte Wirkungszusammenhänge beherrschbarer Naturkräfte genutzt werden. Dagegen weist ein Algorithmus, gleichgültig ob die symbolischen Elemente, aus denen er besteht, als Bezüge zu einer physischen Umgebung interpretiert werden können, keinen technischen Charakter auf.  Insbesondere die Bezugnahme auf physische Umsetzungen des Universalrechners, der Turing-Maschine oder sonstiger bekannter mathematischer Modelle, deren physische Umsetzbarkeit außer Frage steht, kann einem Algorithmus keinen technischen Charakter verleihen.  Ein Datenverarbeitungsprogramm ist nichts als ein Algorithmus, der in den allgemeinsten möglichen Begriffen formuliert wurde, nämlich denen des Universalrechners.

#Tet: Um computerimplementierte Erfindungen rechtlich zu schützen, sollten keine getrennten Rechtsvorschriften erforderlich sein, die das nationale Patentrecht ersetzen. Die Vorschriften des nationalen Patentrechts sollten auch weiterhin die Hauptgrundlage für den Rechtschutz computerimplementierter Erfindungen liefern, und lediglich in bestimmten Punkten, die in dieser Richtlinie dargelegt sind, angepasst oder ergänzt werden.

#IrW3: Um zu bekräftigen, dass Organisations- und Rechenregeln keine Erfindungen sind, ist es nicht erforderlich, an stelle des nationalen Patentrechts neue Rechtsvorschriften zu schaffen.  In manchen Ländern kann diese Richtlinie vielleicht ganz ohne gesetzgeberische Schritte umgesetzt werden.  In anderen Ländern mag es ratsam erscheinen, den Gerichten durch Streichung der redundanten Als-Solches-Klausel aus den nationalen Entsprechungen von Art 52 einen Wink zu geben.  Die Mitgliedsstaaten des EPÜ könnten auch gemeinsam beschließen, im Zuge einer Revision des Europäischen Patentübereinkommens (EPÜ) Art 52(3) zu streichen und eine Definition des Begriffs %(q:technische Erfindung) in Art 52(1) aufzunehmen, um die Gerichte der nationalen und europäischen Ebene unmissverständlich auf den Wortlaut und Geist des geschriebenen Gesetzes zu verpflichten, wie er in dieser Richtlinie erläutert wird.

#Tel2: The Community's legal framework for the protection of computer-implemented inventions can be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not so susceptible.

#AmW: Applying the above principles to eliminate the current differences in the legal protection of computer-implemented inventions and to provide for transparency of the legal situation should also improve the competitive position of European industry in relation to its major trading partners.

#TsW2: This Directive shall be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty as well as the relevant provisions of the Member States' laws with respect to competition and fair business practices.

#Tmo: This Directive shall be without prejudice to other Community legislation such as Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks. The Directive shall also be without prejudice to Member States' laws relating to trade secrets or the law of contract.

#Tsc: Diese Richtlinie sollte sich auf die Festlegung bestimmter Patentierbarkeitsgrundsätze beschränken; im Wesentlichen sollen diese Grundsätze einerseits die Schutzfähigkeit von Erfindungen sicherstellen, die einem Gebiet der Technik zugehören und einen technischen Beitrag zum Stand der Technik leisten, andererseits Erfindungen vom Schutz ausschließen, die keinen technischen Beitrag zum Stand der Technik leisten.

#Ttu: Die Wettbewerbsposition der europäischen Wirtschaft im Vergleich zu ihren wichtigsten Handelspartnern würde sich verbessern, wenn die bestehenden Unterschiede beim Rechtschutz computerimplementierter Erfindungen ausgeräumt würden und die Rechtslage transparenter wäre.

#TWn2: Diese Richtlinie berührt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 EG-Vertrag.

#Aod: Urheberrechtlich zulässige Handlungen gemäß der Richtlinie 91/250/EWG über den Rechtsschutz von Computerprogrammen, insbesondere deren Vorschriften über die Dekompilierung und die Interoperabilität, oder die Vorschriften über Marken oder Halbleitertopografien sollen unberührt bleiben von dem Patentschutz für Erfindungen aufgrund diese Richtlinie.

#SjW: Gemäß Artikel 5 EG-Vertrag kann die Gemeinschaft nach dem Subsidiaritätsprinzip tätig werden, da die Ziele der vorgeschlagenen Maßnahme, also die Harmonisierung der nationalen Vorschriften für computerimplementierte Erfindungen, auf Ebene der Mitgliedstaaten nicht ausreichend erreicht werden können und daher wegen ihres Umfangs oder ihrer Wirkung besser auf Gemeinschaftsebene erreicht werden können. Diese Richtlinie steht auch im Einklang mit dem in diesem Artikel festgeschriebenen Grundsatz der Verhältnismäßigkeit, da sie nicht über das für die Erreichung der Ziele erforderliche Maß hinausgeht -

#yWn: If this principle is to be taken seriously, the directive should limit itself to specifying in the exactest possible terms which kinds of patents must be refused and which must be granted, and leave internal details of legal theory and patent examination methodology to the member states.  It should in particular not codify esoteric EPO concepts such as %(q:technical contribution in the inventive step).

#Tho: Diese Richtlinie sollte sich darauf beschränken, bestimmte Grundsätze %(s:betreffend die Grenzen der Patentierbarkeit im Hinblick auf Datenverarbeitungsprogramme) festzulegen, wobei insbesondere sicher gestellt werden soll, dass %(s:technische Erfindungen patentierbar und Organisations- und Rechenregeln nicht patentierbar) sein sollen.

#End: Es dürfte der Rechtssicherheit und den Wettbewerbschancen der europäischen Softwareunternehmer im Vergleich zu denen von Europas wichtigsten Handelspartnern sehr zugute kommen, wenn nunmehr %(s:klar gestellt wird, dass Europa die Patentierbarkeit nicht über den Bereich der technischen Erfindungen hinaus ausdehnt).

#DWd: Diese Richtlinie berührt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 EG-Vertrag.

#Teh: Diese Richtlinie %(s:bekräftigt den %(db:Grundsatz der Trennung von Eigentumssystemen)).  Sie sorgt dafür, dass das Patentsystem auf den Bereich der technischen Erfindungen beschränkt bleibt und dass %(s:Ergebnisse abstrakten Denkens wie etwa Algorithmen von privater Aneignung frei gehalten werden).  Die Organisations- und Rechenregeln, die im menschlichen Geiste oder auf dem Universalrechner ausgeführt werden, unterliegen verfassungsmäßigen Freiheitsgarantien der Gemeinschaft und ihrer Mitgliedsstaaten.  Solche Freiheitsgarantien dürfen nicht auf dem Wege einer Rechtsfortbildung oder Harmonisierung ausgehöhlt werden.  Wenn an ihnen gerüttelt wird, dann nur auf dem Wege einer Reform des Urheberrechts und des Verfassungsrechts.

#Ste: Gemäß Artikel 5 EG-Vertrag kann die Gemeinschaft nach dem Subsidiaritätsprinzip tätig werden, da die Ziele der vorgeschlagenen Maßnahme, also die %(s:Klärung der Grenzen der Patentierbarkeit im Hinblick auf Computerprogramme), wegen der grenzüberschreitenden Natur von Computerprogrammen und anderen Informationsgegenständen besser auf Gemeinschaftsebene erreicht werden können. Diese Richtlinie steht auch im Einklang mit dem in diesem Artikel festgeschriebenen Grundsatz der Verhältnismäßigkeit, da sie nicht über das für die Erreichung der Ziele erforderliche Maß hinausgeht.

#HTD: HABEN FOLGENDE RICHTLINIE ERLASSEN:

#Sco: Anwendungsbereich

#Tie2: This Directive relates to computer-implemented inventions. The term %(q:computer-implemented invention) includes any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs.

#Tsm: Diese Richtlinie legt Vorschriften für die Patentierbarkeit computerimplementierter Erfindungen fest.

#Dit: Begriffsbestimmungen

#FWW: Für die Zwecke dieser Richtlinie gelten folgende Begriffsbestimmungen:

#cWe: %(q:Computerimplementierte Erfindung) ist jede Erfindung, zu deren Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die auf den ersten Blick mindestens ein neuartiges Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.

#cWi: %(q:Technischer Beitrag) ist ein Beitrag zum Stand der Technik auf einem Gebiet der Technik, der für eine fachkundige Person nicht nahe liegend ist.

#Tap: Diese Richtlinie erklärt die Regeln über die Grenzen der Patentierbarkeit im Hinblick auf Programme für Datenverarbeitungsanlagen.  Für die Zwecke dieser Richtlinie sollen die folgenden Definitionen gelten:

#Ain: Eine %(e:Erfindung), auch %(q:technische Erfindung), %(q:technische Lehre), %(q:technische Lösung) oder %(q:technischer Beitrag) genannt, ist eine Lehre über %(ps:Wirkungszusammenhänge beim Einsatz beherrschbarer Naturkräfte).

#Ase: Alternative: An invention ... is a problem solution involving controllable forces of nature.

#xad: Wir kommen gut ohne eine Definition des Propagandabegriffes %(q:computer-implementierte Erfindung) aus.  Wer dennoch eine Definition zu brauchen glaubt, möge es mit dem folgenden Gegengift versuchen, welches sich durch leichte Umformulierung aus der KEG/BSA-Definition ableiten lässt:

#rWW: %(q:Computerimplementierte Erfindung) ist jede technische Lösung, zu deren Umsetzung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die in Form von mindestens einem auf den ersten Blick nicht neuen Implementationsmerkmal beansprucht wird, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird, wohingegen die auf den ersten Blick neuen Lösungsmerkmale ganz oder teilweise vom Einsatz beherrschbarer Naturkräfte abhängen.

#Aoj3: Eine %(e:Rechenregel), auch %(e:Algorithmus) genannt, ist eine Lehre über Beziehungen innerhalb von gedanklichen Gebilden wie z.B. Modellen und Axiomsystemen, einschließlich der Turing-Maschine.

#AlB: Eine %(e:Organisationsregel) ist eine Lehre über Beziehungen zwischen dinglichen Erscheinungen (materiellen Phänomenen), die nicht von beherrschbaren Naturkräften bestimmt sind.  Geschäftsmethoden und soziale Techniken sind Organisationsregeln.  Die meisten Organisationsregeln sind Anwendungen von Rechenregeln auf bekannte Modelle gesellschaftlicher Beziehungen.  %(q:Organisations- und Rechenregeln) sind Lehren über Wirkungszusammenhänge, die nicht von beherrschbaren Naturkräften bestimmt sind.

#OWd: Nur Erfindungen (technische Beiträge) sind patentfähig.  Um patentierbar zu sein, muss eine Erfindung neu, nicht naheliegend und für eine industrielle Anwendung bestimmt sein.

#CeW: Gebiet der Technik

#Peh: Patentfähige Erfindungen als technische Lehren

#Ailx: Eine computer-implementierte Erfindung soll als einem Gebiet der Technik zugehörig betrachtet werden.

#Mue: Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung als einem Gebiet der Technik zugehörig gilt.

#Mte: Die Mitgliedsstaaten sorgen dafür, dass technische Erfindungen unabhängig davon patentierbar sind, ob sie auf einer Datenverarbeitungsanlage ausgeführt werden, und dass umgekehrt niemand Datenverarbeitungsprogramme oder sonstige Organisations- und Rechenregeln dadurch patentfähig machen kann, dass er sie in Kombination mit Umsetzungsmerkmalen präsentiert, in welchen keine patentbegründende Leistung liegt.  Die Organisations- und Rechenregeln sind als Teil des öffentlichen Wissens zu betrachten, von dem ausgehend ein möglicherweise patentwürdiger technischer Beitrag geleistet wurde.

#Tar2: Technical contribution

#Cse: Voraussetzungen der Patentierbarkeit

#ItW2: In order to involve an inventive step, a computer-implemented invention must make a technical contribution. A %(q:technical contribution) is a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#Tnc: The technical contribution must be assessed by consideration of the difference between the scope of the patent claim considered as a whole, which may comprise both technical and non-technical features, and the state of the art.

#Mii: Die Mitgliedstaaten stellen sicher, dass eine computerimplementierte Erfindung patentierbar ist, sofern sie gewerblich anwendbar und neu ist und auf einer erfinderischen Tätigkeit beruht.

#Mfm: Die Mitgliedstaaten stellen sicher, dass die Voraussetzung der erfinderischen Tätigkeit nur erfüllt ist, wenn eine computerimplementierte Erfindung einen technischen Beitrag leistet.

#Tba: Bei der Ermittlung des technischen Beitrags wird beurteilt, inwieweit sich der Gegenstand des Patentanspruchs in seiner Gesamtheit, der sowohl technische als auch nichttechnische Merkmalen umfassen kann, vom Stand der Technik abhebt.

#MaW: Die Mitgliedsstaaten sorgen dafür, dass Patente nur für technische Erfindungen erteilt werden, die neu, nicht naheliegend und industriell anwendbar sind.

#MWh: Die Mitgliedstaaten sorgen dafür, dass %(q:technischen Erfindungen) als %(e:technische Beiträge) verstanden werden, m.a.W. %(q:durch Einsatz beherrschbarer Naturkräfte zustande kommende Problemlösungen) oder %(q:Lehren über Wirkungszusammenhänge beherrschbarer Naturkräfte.)

#MWq: Die Mitgliedsstaaten sorgen dafür, dass die Patentierbarkeit im Geiste der %(EP) geprüft wird: Bei der Prüfung der Frage, ob eine technische Erfindung vorliegt, sollte man die Form oder die Art des Patentanspruchs außer acht lassen und sich auf den Inhalt konzentrieren, um festzustellen, welchen neuen Beitrag der beanspruchte Gegenstand zum Stand der Technik leistet.  Stellt dieser Beitrag keine Erfindung dar, so liegt kein patentierbarer Gegenstand vor.

#FWc: Form des Patentanspruchs

#Cnl: Patentansprüche, Verletzung, Offenbarung, Neuheit, Vorurheberschaft, Industrialität

#AWr: A computer-implemented invention may be claimed as a product, in particular as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.

#Msc: Die Mitgliedstaaten stellen sicher, dass auf eine computerimplementierte Erfindung entweder ein Erzeugnisanspruch erhoben werden kann, wenn es sich um einen programmierten Computer, ein programmiertes Computernetz oder eine sonstige programmierte Vorrichtung handelt, oder aber ein Verfahrensanspruch, wenn es sich um ein Verfahren handelt, das von einem Computer, einem Computernetz oder einer sonstigen Vorrichtung durch Ausführung von Software verwirklicht wird.

#MWo: Die Mitgliedsstaaten sorgen dafür, dass Ansprüche im Geiste der %(EP) geprüft werden: Wenn der Beitrag zum bisherigen Stand der Technik lediglich in einem Programm für Datenverarbeitungsanlagen besteht, ist der Gegenstand nicht patentierbar, unabhängig davon, in welcher Form er in den Ansprüchen dargelegt ist.  So wäre z.B. ein Patentanspruch für eine Datenverarbeitungsanlage, die durch ein in ihr gespeichertes Programm (logische Funktionalität) gekennzeichnet ist, oder für ein Verfahren zum Betrieb einer durch dieses Programm gesteuerten Datenverarbeitungsanlage in Steuerabhängigkeit von diesem Programm ebenso zu beanstanden wie ein Patentanspruch auf das Programm als solches oder auf einen das Programm enthaltenden Datenträger.

#sih: Die Mitgliedsstaaten sorgen dafür, dass Patentansprüche sich nicht auf den bloßen Betrieb von Datenverarbeitungsanlagen (d.h. Universalrechner mit Peripherie zur Kommunikation mit anderen Rechnern oder Personen) beziehen sondern den Betrieb von Geräten umfassen, die der erfinderischen Beherrschung von Naturkräften dienen.

#Dte: Die Mitgliedsstaaten sorgen dafür, dass ein Datenträger nicht dadurch zum patentverletzenden Gegenstand werden kann, dass auf ihm bestimmte Informationen gespeichert werden, und dass die Veröffentlichung und Verbreitung von Informationen auf nicht-patentverletzenden Speichermedien niemals eine direkte oder indirekte Patentverletzung darstellen kann.  Die Mitgliedsstaaten schützen die Freiheit des sprachlichen Ausdrucks und Informationsaustausches unabhängig von Sprache und Verwendungszweck der betroffenen Texte, und sorgen darür, dass diese Freiheit nur durch das Urheberrecht und niemals durch Patente eingeschränkt werden kann.

#Man: Die Mitgliedstaaten sorgen dafür, dass die Ausführung eines Programms auf einer Datenverarbeitungsanlage (d.h. Rechner mit Geräten für Speicherung, Eingabe und Ausgabe von Daten) keine Patentverletzung darstellen kann, wohingegen eine Kombinationshandlung, wie z.B. der Vertrieb von programmgesteuert hergestellten Chemikalien, sehr wohl eine Patentverletzung darstellen kann.

#scrambled: Die Mitgliedsstaaten sorgen dafür, dass digitale Information als öffentliches Wissen gilt, sobald es der Öffentlichkeit faktisch möglich ist, diese Information einem veröffentlichten Text zu entnehmen, auch dann, wenn die Entnahme mühevolle oder illegale Vorgänge der Entschlüsselung voraussetzt.

#prekre: Die Mitgliedsstaaten sorgen dafür, dass alle urheberrechtsfähigen Werke, die vor der Veröffentlichung eines Patentes in Gebrauch waren, auch künftig von dem Urheberrechtsinhaber und von allen Personen, denen der Urheberrechtsinhaber dies gestattet, gemäß vom Urheberrechtsinhaber zu bestimmenden Bedingungen weiterentwickelt, weiterverbreitet und zum Betrieb von Datenverarbeitungsanlagen verwendet werden dürfen.

#progpub: Mitgliedesstaaten sorgen dafür, dass immer dann, wenn  ein Patentanspruch Merkmale nennt, welche die Verwendung eines Datenverarbeitungsprogramms voraussetzen, eine gut funktionierende und gut dokumentierte Referenzimplementation eines solchen Programmes als Teil der Patentbeschreibung ohne jedwede einschränkende Lizenzbedingung veröffentlicht wird.

#indapp: Die Mitgliedsstaaten sorgen dafür, dass patentierbare Erfindungen industriell anwendbar sind, und dass unter %(q:Industrie) oder %(q:Gewerbe) im Sinne des Patentrechts die %(e:Fertigungs materieller Güter mithilfe von Ausrüstungsgütern (%(q:Industrieanlagen)), die typischerweise schon aufgrund ihrer Anschaffungskosten und ihres Material- und Energieverbrauchs das betreibende Unternehmen prägen).  Worte des Gesetzgebers, welche zur Beschränkung der Patentierbarkeit dienen, dürfen niemals so ausgelegt werden, dass die beschränkende Wirkung entfällt oder wesentlich abgeschwächt wird.

#RWi: Konkurrenz zur Richtlinie 91/250/EWG

#Aod2: Zulässige Handlungen im Sinne der Richtlinie 91/250/EWG über den Rechtsschutz von Computerprogrammen durch das Urheberrecht, insbesondere der Vorschriften über die Dekompilierung und die Interoperabilität, oder im Sinne der Vorschriften über Marken oder Halbleitertopografien bleiben vom Patentschutz für Erfindungen aufgrund dieser Richtlinie unberührt.

#MtW: Members states shall keep the spheres of copyright and patents clearly separated.  Property in computer programs is acquired and regulated through copyright.  Property in technical inventions is acquired and regulated through patents.   Property that has been acquired through one system may not be restricted by another.  Aspects of a computer program that can not be appropriated through copyright can also not be appropriated through patents, utility certificates or any other property regime.

#interop: Die Mitgliedsstaaten sorgen dafür, dass überall dort, wo ein Gebrauch einer patentierten Technik zur Kommunikation mit dem anderen System (d.h. zur Umwandlung in die und aus den Konventionen dieses Systems) erforderlich ist, ein solcher Gebrauch nicht als Verletzung angesehen wird.

#Mir: Beobachtung

#Tnt: Die Kommission beobachtet, wie sich computerimplementierte Erfindungen auf die Innovationstätigkeit und den Wettbewerb in Europa und weltweit sowie auf die europäischen Unternehmen und den elektronischen Geschäftsverkehr auswirken.

#Tia2: Das Europaparlament beobachtet, wie sich Patente auf die Innovationstätigkeit und den Wettbewerb in Europa und weltweit sowie auf die europäischen Unternehmen und den elektronischen Geschäftsverkehr auswirken.

#ReW: Bericht über die Auswirkungen der Richtlinie

#Tnt2: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#Typ: The Commission shall send to the European Parliament and the Council within three years as from the date specified in Article 6(1) a report assessing the impact of patents for computer-implemented inventions on the factors mentioned in paragraph 1.

#Iir: In this context, the report will also address the questions as to whether the rules governing the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and whether difficulties have been experienced in respect of Member States where the requirements of novelty and inventive step are not examined prior to issuance of a patent, and if so, whether any steps are desirable to address such difficulties.

#Tme2: Die Kommission legt dem Europäischen Parlament und dem Rat spätestens am [DATUM (drei Jahren nach dem in Artikel 9 Absatz 1 genannten Datum)] einen Bericht vor über:

#teW: die Auswirkungen von Patenten auf computerimplementierte Erfindungen auf die in Artikel 7 genannten Faktoren,

#wtn: die Angemessenheit der Regeln für die Festlegung der Patentierbarkeitsanforderungen, insbesondere im Hinblick auf die Neuheit, die erfinderische Tätigkeit und den eigentlichen Patentanspruch, und

#wra: etwaige Schwierigkeiten, die in Mitgliedstaaten aufgetreten sind, in denen Erfindungen vor Patenterteilung nicht auf Neuheit und Erfindungshöhe geprüft werden, und etwaige Schritte, die unternommen werden sollten, um diese Schwierigkeiten zu beseitigen.

#Toi2: Das Europäische Parlament bildet einen Untersuchungsausschuss über die Grenzen der Patentierbarkeit.  Der Untersuchungsausschuss ist mit den nötigen Vollmachten und Geldmitteln ausgestattet, um vom Europäischen Patentamt, der Europäischen Kommission und von wichtigen Akteuren aus der Industrie benötigte Informationen über die Praxis der Verwertung von Patenten, über die Auswirkungen von Patenten auf Geldflüsse aller Art, über die politischen Willensbildung im Patentwesen usw zu beschaffen.  Der Untersuchungsausschuss soll nachhaltig wirksame Regeln vorschlagen, mit denen mehr Transparenz und Steuerbarkeit in das Patentwesen gebracht wird und mit denen es in vernünftigen Grenzen gehalten werden kann.  Der Untersuchungsausschuss soll in der öffentlichsten möglichen Art die Forschung in diesem Bereich vorantreiben.  Der Untersuchungsausschuss darf zu nicht mehr als 20% aus Personen bestehen, die ihre geschäftliche oder berufliche Existenz dem Patentwesen verdanken.

#nno: to what extent and under what terms licenses have become available for the patents granted under this directive and to what extent licensing information is accessible to the public.

#Iet: Umsetzung

#Mnl: Member States shall bring into force the laws, regulations or administrative provisions necessary to comply with this Directive not later than ... They shall forthwith notify the Commission thereof.

#Mvf: Die Mitgliedstaaten erlassen die erforderlichen Rechts­ und Verwaltungsvorschriften, um dieser Richtlinie spätestens am [DATUM (letzter Tag des betreffenden Monats)] nachzukommen. Sie setzen die Kommission unverzüglich davon in Kenntnis.

#Wrp: When Member States adopt these provisions, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.

#WDi: Bei Erlass dieser Vorschriften nehmen die Mitgliedstaaten in den Vorschriften selbst oder durch einen Hinweis bei der amtlichen Veröffentlichung auf diese Richtlinie Bezug. Die Mitgliedstaaten regeln die Einzelheiten der Bezugnahme.

#Mia: Die Mitgliedstaaten übermitteln der Kommission den Wortlaut der innerstaatlichen Rechtsvorschriften, die sie im Geltungsbereich dieser Richtlinie erlassen.

#EWW2: Inkrafttreten

#TWi2: Diese Richtlinie tritt am zwanzigsten Tag nach ihrer Veröffentlichung im Amtsblatt der Europäischen Gemeinschaften in Kraft.

#Ars: Adressaten

#Tsh2: Diese Richtlinie ist an die Mitgliedstaaten gerichtet.

#Dau: Geschehen zu Brüssel am

#Fua: Im Namen des Europäischen Parlaments

#Fhu: Im Namen des Rates

#Pres: Der Präsident

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-kern ;
# txtlang: de ;
# multlin: t ;
# End: ;

