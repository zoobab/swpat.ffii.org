<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Proposition(s) BSA/CCE et Contre-Proposition Basée sur la CBE

#descr: Le 2002-02-20 la Commission Européenne a publié une proposition de directive pour définir les programmes d'ordinateur comme des inventions brevetables et rendre difficile toute tentative de refuser un brevet sur un algorithme ou une fonctionnalité logique, y compris les méthodes d'affaires, qui a été drapée dans le langage de l'informatique (ex. ordinateur, mémoire, réseaux, entrée/sortie, base de données etc).  Ci-dessus vous trouvez une compaison de cette proposition avec l'original de la BSA et une  contre-proposition de la FFII.

#Pem: Préambule

#TAc: Les Articles

#cRW: %(nl|Proposition de|DIRECTIVE DU PARLEMENT EUROPÉEN ET DU CONSEIL|concernant la brevetabilité des inventions mises en oeuvre par ordinateur)

#cRW2: %(nl|Proposition de|DIRECTIVE DU PARLEMENT EUROPÉEN ET DU CONSEIL|%(s:sur les limites de la brevetabilité vis-à-vis des programmes d'ordinateur))

#TEO: LE PARLEMENT EUROPÉEN ET LE CONSEIL DE L'UNION EUROPÉENNE,

#HiW: vu le traité instituant la Communauté européenne et notamment son article 95,

#HWo: vu la %(fn:39:proposition de la Commission),

#HpW: vu l'%(fn:40:avis du Comité économique et social),

#AtW2: Statuant conformément à la %(fn:41:procédure prévue à l'article 251 du traité),

#Wee: Considérant ce qui suit:

#Een: La protection effective et harmonisée des inventions implémentées par ordinateur dans tous les États membres est essentielle pour maintenir et encourager les investissements dans ce domaine.

#TWE: La réalisation du marché intérieur implique que l'on élimine les restrictions à la libre circulation et les distorsions à la concurrence, tout en créant un environnement favorable à l'innovation et à l'investissement. Dans ce contexte, la protection des inventions par brevet est un élément essentiel du succès du marché intérieur. Une protection effective et harmonisée des inventions mises en oeuvre par ordinateur dans tous les États membres est essentielle pour maintenir et encourager les investissements dans ce domaine.

#Tse: La réalisation du marché intérieur implique l'élimination des restrictions à la libre circulation et aux distortions de la concurrence. Un environnement favorisant l'innovation et l'investissement doit alors être créé. %(s:Une protection sûre et égalitaire dans le domaine du développement de logiciels) est un élément important pour le succès du marché intérieur. %(s:Les créateurs de logiciels innovants ne doivent être gênés ni par %(tp|la copie|le plagiat), ni encore par la monopolisation d'idées et interfaces fondamentales). Le secteur du logiciel doit être un espace de compétition régulier dans lequel de nombreuses jeunes pousses peuvent fleurir et prospérer pour développer des logiciels interopérables de haute valeur.

#Dao: Des différences existent dans la protection des inventions mises en oeuvre par ordinateur, elles sont offertes par les pratiques administratives et la jurisprudence des différents États Membres. De telles différences pourraient créer des barrières au commerce et ainsi entraver le fonctionnement propre du marché intérieur.

#Ccy: Les règles d'organisation et de calcul implémentées par ordinateur (méthodes mathématiques, plans ou méthodes pour activités mentales ou commerciales, programmes pour ordinateur, présentation d'information etc) ne sont pas considérées comme des inventions brevetables par les lois des États Membres.  Le droit positif des brevets des États Membres est déjà complètement harmonisé grâce à la promulgation des Articles 52-57 de la Convention Européenne des Brevets de 1973 dans les lois nationales.  Néanmoins les règles établies dans ces lois n'ont pas été uniformément observées.  En particulier l'Office Européen des Brevets (OEB) a, à partir de 1986, graduellement changé son interprétation de l'Art 52 d'une telle façon que les règles d'organisation et de calcul mises en oeuvre par ordinateur sont de facto traitées comme des inventions brevetables et la loi écrite perd ainsi son effet de délimitation.  Quelques cours nationales ont plus ou moins suivi l'interprétation de l'OEB tandis que d'autres ne l'ont pas fait.  Même à l'intérieur des juridictions nationales les décisions des cours divergent.

#San: De telles différences résultent du fait que les États membres adoptent de nouvelles pratiques administratives qui diffèrent les unes des autres ou que les jurisprudences nationales interprétant la législation actuelle évoluent différemment.

#Nua: Avec l'accroissement intensif de l'opinion sur les plans nationaux et européens les différences de droit positif sont normalement plutôt atténuées qu'amplifiées. Ces dernières années la jurisprudence des brevets a convergé dans de nombreux cas. Différentes cours nationales ont adopté des règles similaires où leurs lois nationales le permettaient.  Étant donné que le droit positif des brevets de l'Europe est déjà harmonisé, il n'y aurait pas à attendre normalement ici le besoin d'une harmonisation.  Or, à cause des décisions prises par l'OEB depuis 1986, les cours européennes font maintenant face à deux normes du droit en concurrence.  Cette situation exige une décision législative.

#TWe: L'accroissement continu dans la distribution et l'usage des programmes d'ordinateur dans tous les domaines de la technologie et dans leur distribution mondiale via l'Internet est un facteur critique dans l'innovation technologique. Il est par conséquent nécessaire d'assurer qu'un environnement optimum existe pour les développeurs et les utilisateurs de programmes d'ordinateur en Europe.

#Tos: L'accroissement continu dans la distribution et l'usage des programmes d'ordinateur dans tous les domaines de la technologie et dans leur distribution mondiale via l'Internet est un facteur critique dans l'innovation technologique. Il est par conséquent nécessaire d'assurer qu'un environnement optimum existe pour les développeurs et les utilisateurs de programmes d'ordinateur en Europe.

#Aui: Déjà en 1973 les programmes d'ordinateur étaient communément utilisés pour piloter des processus d'ingénierie industrielle.  L'informatisation a depuis fait des progrès supplémentaires.  L'ordinateur universel a facilité le développement de nouvelles idées. Des processus qui devaient auparavant être étudiés et vérifiés par des expérimentations avec les forces de la nature ont de plus en plus été réduits à des modèles abstraits, dans lesquels un espace pour une innovation aisée par opérations purement intellectuelles (abstraction, calculation, analyse, déduction etc) s'est developpée.  Les législateurs de 1973 ont décidé que les innovations abstraites et logiques devaient rester libres des brevets.  Les expériences récentes et les études économiques n'ont fait que confirmer la sagesse de cette décision.  Avec l'importance croissante d'une économie basée sur les connaissances, la prospérité des acteurs économiques ainsi que des individus dépend à un degré plus élevé qu'auparavant du caractère sûr de cette limitation. Il est devenu désormais indispensable de donner aux développeurs et aux utilisateurs de programmes d'ordinateur en Europe l'assurance qu'ils auront le droit à l'avenir de publier et d'utiliser leurs oeuvres.

#TWv: Par conséquent, les règles légales comme interprétées par les Cours des États Membres devront être harmonisés et la loi gouvernant la brevetabilité des inventions implémentées par ordinateur devra être rendue transparente. La certitude légale résultante devrait permettre aux entreprises de tirer le maximum d'avantage des brevets pour les inventions implémentées par ordinateur et donner une incitation pour l'investissement et l'innovation.

#TWl: C'est pourquoi les règles sur les limites de la brevetabilité, qui ont été instituées au cours du temps par les Cours des États Membres sur le chemin de la formation continue du droit, devront être alignées à nouveau en accord avec l'esprit et le contenu du droit écrit et arrêté sous une forme qui ne permet pas de malentendu. La certitude légale résultante devrait %(s:contribuer à un climat encourageant l'investissement et l'innovation dans le domaine du logiciel).

#Ttn3: La Communauté Européenne et ses États Membres sont liés aux Accords ADPIC, dont l'Article 27(1) qui dispose que, étant sujet uniquement à certaines exceptions déterminées dans les paragraphes (2) et (3) de cet Article, les brevets doivent être disponibles pour toutes les inventions, qu'elles soient des produits ou des processus, dans tous les domaines de la technologie, pourvu qu'elles soient nouvelles, impliquent une étape inventive et sont capables d'application industrielle.  De plus, les droits du brevet doivent être disponibles et profitables sans discrimination vis-à-vis du domaine de technologie. Ces principes devraient s'appliquer en conséquence aux inventions qui sont le sujet de cette Directive.

#TcW: La Communauté et ses États membres sont liés par l'accord relatif aux aspects des droits de propriété intellectuelle qui touchent au commerce (ADPIC), approuvé par la décision 94/800/CE du Conseil, du 22 décembre 1994, relative à la conclusion au nom de la Communauté européenne, pour ce qui concerne les matières relevant de ses compétences, des %(fn:42:accords des négociations multilatérales du cycle de l'Uruguay (1986-1994)). L'article 27, premier paragraphe, de l'accord sur les ADPIC dispose qu'un brevet pourra être obtenu pour toute invention, de produit ou de procédé, dans tous les domaines techniques, à condition qu'elle soit nouvelle, qu'elle implique une activité inventive et qu'elle soit susceptible d'application industrielle. En outre, selon l'accord sur les ADPIC, des brevets peuvent être obtenus et des droits de brevets exercés sans discrimination quant au domaine technique. Ces principes devraient donc s'appliquer aux inventions mises en oeuvre par ordinateur.

#Wjr: La Communauté et ses États Membres sont liés par l'%(tr:Accord sur les Aspects des Droits de Propriété Intellectuelle qui touchent au Commerce) (ADPIC), approuvé par la Décision du Conseil 94/800/CEC du 22 Décembre 1994 concernant la conclusion au nom de la Communauté Européenne, en regard des matières en sa compétence, des %(fn:42:accords atteints lors des négociations multilatérales de l'Uruguay Round (1986-1994)). Selon l'Article 27(1) de l'ADPIC, les brevets doivent être disponibles pour toute les inventions, de produit ou de procédé, dans tous les domaines techniques, à condition qu'elle soient nouvelles, qu'elle impliquent une activité inventive et qu'elle soient susceptible d'application industrielle. De plus, selon l'ADPIC, les droits des brevets doivent être disponibles sans discrimination pour tous les domaines de la technologie. Conformément à l'ADPIC, des brevets doivent être disponibles en outre sans discrimination quant au domaine de la technique ou les droits sur les brevets peuvent être exercés. %(s:Ceci signifie que les inventions techniques, dont celles dont l'exécution implique l'utilisation d'un ordinateur, doivent être brevetables sans que l'on ait à considérer à quel %(tp|domaine technique|par expl. ingénierie mécanique, électrique, chimique) elles appartiennent.  Le processement de données, soit il conduit par la raison humane ou par un ordinateur, n'est pas un champ de technologie dans le sens du loi de brevets.)

#Urh: Dans la Convention sur l'Octroi des Brevets Européens signée à Munich le 5 octobre 1973 et les lois sur les brevets des États Membres, les programmes d'ordinateur ainsi que les découvertes, les théories scientifiques, les méthodes mathématiques, les création esthétiques, les plans, les principes et méthodes dans l’exercice d’activités intellectuelles, en matière de jeu ou dans le domaine des activités économiques, et les présentations d'information ne sont expressément pas considérés comme des inventions et sont par conséquent exclus de la brevetabilité. Cette exception, néanmoins, s'applique et n'est justifiée que dans la mesure où une application d'un brevet ou un brevet se rapporte à un tel sujet ou de telles activités en tant que tels, car le sujet et les activités nommés en tant que tels n'appartiennent pas à un domaine de la technologie.

#Uys: Sous la Convention sur l'Octroi de Brevets Européens du 5 octrobre 1973 (CEB) et les lois sur les brevets des États Membres, les programmes pour ordinateur ainsi que les découvertes, les théories scientifiques, les méthodes mathématiques, les créations esthétiques, les plans, les principes et méthodes dans l'exercice d'activités intellectuelles, en matière de jeu ou dans le domaine des activités économiques, et les présentations d'information ne sont expressément pas considérés comme des inventions et sont par conséquent exclus de la brevetabilité. Cette %(s:exclusion), bien entendu, s'applique et n'est justifiée que dans la mesure où une application d'un brevet ou un brevet se rapporte à un tel sujet ou de telles activités en tant que tels.  %(s:Ainsi cela peut ne pas s'appliquer si l'invention ne consiste pas en un programme d'ordinateur mais en une %(tp|invention technique|ex. une nouvelle recette chimique) qui peut être mise en oeuvre sous le contrôle d'un programme.  Dans ce cas le programme en tant que tel serait librement implémentable, ex. pour des simulations, mais le procédé chimique tomberait sous le brevet.)

#Pha: La protection par brevet permet aux innovateurs de tirer profit de leur créativité. Les droits de brevet protègent l'innovation dans l'intérêt de la société dans son ensemble mais ils ne doivent pas être utilisés d'une manière anticoncurrentielle.

#Pri: Les brevets sont des monopoles temporaires accordés par l'État afin de promouvoir le progrès technique.  Afin de s'assurer que le système fonctionne comme prévu, les conditions pour accorder des brevets et les modalités pour les faire valoir doivent être conçues avec attention.  En particulier, des corollaires inévitables du système des brevets comme la restriction de la liberté de créer, l'insécurité légale et les comportements de cartel doivent être contenus dans des limites raisonnables.

#I3H: Conformément à la directive du Conseil 91/250/CEE du 14 mai 1991 concernant la protection juridique des programmes d'ordinateurs43, toute expression d'un programme d'ordinateur original est protégée par un droit d'auteur en tant qu'oeuvre littéraire. Toutefois, les idées et principes qui sont à la base de quelques éléments que ce soit d'un programme d'ordinateur ne sont pas protégés par le droit d'auteur.

#InW: Conformément à la la Directive du Conseil 91/250/CEE du 14 Mai 1991 sur la protection légale des programmes d'ordinateur, l'expression sous toute forme d'un programme d'ordinateur original comme oeuvre littéraire est protégée par le droit d'auteur en tant que %(s:création individuelle).  Les idées et principes qui sont à la base de n'importe quel élément d'un programme d'ordinateur sont %(s:pour de bonnes raisons explicitement exemptés de la protection sous le droit d'auteur).

#Isc: Pour être considérée comme brevetable, une invention doit présenter un caractère technique et donc appartenir à un domaine technique.

#IWd: Afin qu'une idée soit considérée comme une invention dans le sens du système des brevets, il est nécessaire qu'elle présente un caractère technique.  Les brevets sont des monopoles temporaires accordés en échange de la publication d'une invention technique.  %(s:Toute invention technnique appartient à un domaine technique, mais cela ne veut pas dire que toute revendication de ce qui a l'air d'être un %(s:produit ou procédé dans un domaine technique) révèle aussi une invention technique).

#AfW: Bien que les inventions mises en oeuvre par ordinateur soient considérées comme appartenant à un domaine technique, elles devraient, comme toutes les inventions, apporter une contribution technique à l'état de la technique pour répondre au critère de l'activité inventive.

#Put: Les règles d'organisation et de calcul n'appartiennent pas à un domaine de la technologie mais plutôt au domaine de l'informatique, des mathématiques et de la logique.  Cependant  les programmes d'ordinateur sont communément utilisés comme moyens d'implémenter des inventions techniques.  Afin d'être considéré comme une invention technique, un enseignement implémentable par ordinateur doit représenter une contribution technique à l'état de l'art dans la technologie, i.e. sa publication doit apporter des nouvelles connaissances sur les rapports de cause et effet des forces de la nature contrôlables.

#Ahi: En conséquence, lorsqu'une invention n'apporte pas de contribution technique à l'état de la technique, parce que, par exemple, sa contribution spécifique ne revêt pas un caractère technique, elle ne répond pas au critère de l'activité inventive et ne peut donc faire l'objet d'un brevet.

#Aho: En conséquence, un enseignement qui n'apporte pas de contribution technique à l'état de l'art n'est pas une invention technique et est ainsi non brevetable.

#Aaw: Une procédure définie ou une séquence d'actions exécutées sur un appareil tel qu'un ordinateur, peut apporter une contribution technique à l'état de la technique et constituer ainsi une invention brevetable. Par contre, un algorithme défini sans référence à un environnement physique ne présente pas un caractère technique et ne peut donc constituer une invention brevetable.

#Aao: Une procédure définie ou une séquence d'actions exécutées dans un dispositif, par expl. un ordinateur, peut constituer une invention technique, %(s:dans la mesure où des effets inconnus en rapport avec les forces de la nature sont dévoilés.  Toutefois, un algorithme, sans se soucier de savoir si les entités symboliques dont il est composé peuvent être interprétées comme se référant à un environnement physique, ne présente aucun caractère technique) et ne peut par conséquent constituer une invention brevetable.  En particulier des références à des réalisations connus d'éléments de l'Ordinateur Universel, de la Machine Turing ou d'autres modèles mathématiques connus ne peut pas donner un caractère technique à un algorithme.  Un programme d'ordinateur n'est qu'un algorithme exprimé dans les termes les plus généraux: ceux de l'Ordinateur Universel.

#Tet: La protection juridique des inventions mises en oeuvre par ordinateur ne devrait pas nécessiter l'établissement d'une législation distincte en lieu et place des dispositions du droit national des brevets. Les règles du droit national des brevets doivent continuer de former la base de référence de la protection juridique des inventions mises en oeuvre par ordinateur, même si elles doivent être adaptées ou ajoutées en fonction de certaines contraintes spécifiques définies dans la directive.

#IrW3: Afin de confirmer que les règles d'organisation et de calcul ne sont pas des inventions, il n'est pas nécessaire de créer de nouvelles prescriptions légales à la place des droits des brevets nationaux. Dans quelques pays cette Directive peut être transposée sans démarches légales. Dans d'autres pays il peut être prudent de supprimer de la loi nationale la clause qui conrrespond a l'Art. 52(3) (traitant d'inventions %(q:en tant que telles)).  Les États membres de la Convention sur le Brevet Européen (CBE) pourraient aussi décider, dans le courant d'une révision de la CBE de supprimer l'Art. 52(3) CBE et de dresser une définition du concept d'%(q:invention technique) dans l'Art. 52(1), afin d'obliger les tribunaux aux plans nationaux et européens à éviter tout malentendu sur l'esprit et le contenu de la loi écrite, comme  éclairci par cette directive.

#Tel2: La structure légale de la Communauté pour la protection des inventions implémentées par ordinateur peut être limitée en posant certains principes comme ils s'appliquent à la brevetabilité de telles inventions, de tels principes étant déterminés en particulier pour assurer que les inventions qui appartiennent à un domaine de la technologie et qui apportent une contribution technique sont susceptibles de protection, et réciproquement pour assurer que ces inventions qui n'apportent pas de contribution technique ne sont pas tant susceptibles de protection.

#AmW: Appliquer les principes ci-dessus pour éliminer les différences actuelles dans la protection légale des inventions implémentées par ordinateur et pour apporter à la transparence de la situation légale devrait aussi améliorer la position compétitive de l'industrie européenne en relation avec ses principaux partenaires commerciaux.

#TsW2: Cette Directive devrait être sans préjudice pour l'application des règles de concurrence, en particulier les Articles 81 et 82 du Traité ainsi que les provisions appropriées des lois des États Membres concernant la concurrence et des pratiques économiques équitables.

#Tmo: La Directive devrait être sans préjudice à d'autres législations communautaires telles que la Directive 91/250/EEC sur la protection légale des programmes d'ordinateur par le droit d'auteur, en particulier les provisions relatant de cela pour la décompilation ou l'intéropérabilité, ou les provisions concernantles topographies de semiconducteur ou les marques déposées. La Directive devrait aussi être sans préjudice aux lois des États Membres concernant les secrets économiques ou les lois sur le contrat.

#Tsc: La présente directive devrait se borner à fixer certains principes s'appliquant à la brevetabilité de ce type d'inventions, ces principes ayant notamment pour but d'assurer que les inventions appartenant à un domaine technique et apportant une contribution technique peuvent faire l'objet d'une protection et inversement d'assurer que les inventions qui n'apportent pas de contribution technique ne peuvent bénéficier d'une protection.

#Ttu: La position concurrentielle de l'industrie européenne vis-à-vis de ses principaux partenaires commerciaux serait améliorée si les différences actuelles dans la protection juridique des inventions mises en oeuvre par ordinateur étaient éliminées et si la situation juridique était transparente.

#TWn2: La présente directive ne préjuge pas de l'application des règles de concurrence, en particulier des articles 81 et 82 du traité.

#Aod: Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment les dispositions particulières relatives à la décompilation et à l'interopérabilité ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affectés par la protection octroyée par les brevets d'invention dans le cadre de la présente directive.

#SjW: Dans la mesure où les objectifs de l'action envisagée ne peuvent pas être réalisés de manière suffisante par les États membres et peuvent donc, en raison des dimensions ou des effets de l'action envisagée, être mieux réalisés au niveau communautaire, la Communauté est en droit d'adopter des mesures conformément au principe de subsidiarité énoncé à l'article 5 du traité. Conformément au principe de proportionnalité, tel qu'énoncé dans cet article, la présente directive ne va pas au-delà de ce. qui est nécessaire pour atteindre les objectifs fixés,

#yWn: If this principle is to be taken seriously, the directive should limit itself to specifying in the exactest possible terms which kinds of patents must be refused and which must be granted, and leave internal details of legal theory and patent examination methodology to the member states.  It should in particular not codify esoteric EPO concepts such as %(q:technical contribution in the inventive step).

#Tho: Cette Directive devrait être limitée à établir certains principes %(s:concernant les limites de la brevetabilité vis-à-vis des programmes d'ordinateur), de tels principes étant déterminés en particulier à assurer que les %(s:inventions techniques sont brevetables et que les règles d'organisation et de calcul ne le sont pas).

#End: La sécurité légale et les chances de position concurrentielle des entreprises européennes du logiciel, en compétition avec des partenaires commerciaux majeurs de l'Europe, seraient améliorées s'il était %(s:posé de façon claire que l'Europe n'étend pas la brevetabilité au-delà du domaine des inventions techniques).

#DWd: La présente directive ne préjuge pas de l'application des règles de concurrence, en particulier des articles 81 et 82 du traité CE.

#Teh: Cette directive %(s:soutient le %(dp:principe de la séparation des systèmes de propriété)).  Elle assure que le système des brevets reste confiné au domaine des inventions techniques et que %(s:les résultats de la pensée abstraite et les algorithmes sont sauvegardés d'une appropriation privée). Les règles d'organisation et de calcul, qui sont exécutées dans l'esprit humain ou sur l'ordinateur universel, sont soumises aux mesures de garantie de liberté de la communauté et des États membres. De telles garanties de liberté ne doivent pas être mises en danger par voie d'une dérive de jurisprudence ou d'une harmonisation. Elles ne peuvent être mises en cause que par la voie d'une réforme du droit positif ou du droit constitutionnel.

#Ste: D'après l'article 5 du traité CE sur le principe de subsidiarité, la communauté peut clarifier les limites de la brevetabilité concernant les programmes d'ordinateur, ce qui est l'objectif de la mesure proposée, contre la nature sans frontière des programmes d'ordinateur ; ce qui peut être plus aisément atteint au niveau communautaire. En accord avec le principe de la proportionnalité, comme exposé dans cet Article, cette Directive ne va pas au-delà de ce qui est nécessaire pour atteindre ces objectifs.

#HTD: ONT ARRÊTÉ LA PRÉSENTE DIRECTIVE:

#Sco: Champ d'application

#Tie2: Cette Directive se rapporte aux inventions implémentées par ordinateur. Le terme %(q:invention implémentée par ordinateur) inclu toute invention dont la mise en oeuvre implique l'usage d'un ordinateur, d'un réseau informatique ou d'autres appareils programmables et ayant une ou plusieurs nouvelles caractéristiques qui sont réalisées entièrement ou partiellement par le moyen d'un ou de plusieurs programmes d'ordinateur.

#Tsm: La présente directive établit des règles concernant la brevetabilité des inventions mises en oeuvre par ordinateur.

#Dit: Définitions

#FWW: Aux fins de la présente directive, les définitions suivantes s'appliquent:

#cWe: %(q:invention mise en oeuvre par ordinateur) désigne toute invention dont l'exécution implique l'utilisation d'un ordinateur, d'un réseau informatique ou d'autre appareil programmable et présentant une ou plusieurs caractéristiques à première vue nouvelles qui sont réalisées totalement ou en partie par un ou plusieurs programmes d'ordinateurs ;

#cWi: %(q:contribution technique) désigne une contribution à l'état de la technique dans un domaine technique, qui n'est pas évidente pour une personne du métier.

#Tap: Cette directive explique les règles sur les limites de la brevetabilité vis-à-vis des programmes pour ordinateurs. Pour les fins de cette directive, les définitions suivantes doivent s'appliquer:

#Ain: Une %(e:invention), appelée aussi %(q:invention technique), %(q:contribution technique a l'état de l'art) ou %(q:contribution technique), est un %(ps:enseignement de relations cause-effet dans l'application des forces de la nature contrôlables).

#Ase: Alternative: An invention ... is a problem solution involving controllable forces of nature.

#xad: We prefer not to use the propaganda term %(q:computer-implemented invention).  If you feel you really need a definition for this term, we recommend the following counter-poison, derived from the CEC/BSA definition by slight rewriting:

#rWW: A %(e:computer-implemented invention) is any technical solution whose implementation involves the use of a computer, computer network or other programmable apparatus, and which is claimed in terms of one or more prima facie non-novel implementation features realised wholly or partly by means of a computer program or computer programs, whereas the prima facie novel solution features depend wholly or partly on the use of controllable forces of nature.

#Aoj3: Une %(e:règle de calcul), dite aussi %(e:algorithme), est un enseignement sur des relations au sein des constructions de la pensée, comme des modèles et systèmes axiomatiques, y compris tout modèle formel d'une machine de processement de données comme ex. la machine de Turing.

#AlB: Une %(e:règle d'organisation) est un enseignement sur les relations entre des phénomènes réels (phénomènes matériels) qui ne sont pas déterminés par des forces de la nature contrôlables.   Les méthodes d'affaires et les techniques sociales sont des règles d'organisation.  La plupart des règles d'organisation sont des  applications de règles de calcul aux modèles connus de relations sociales.  Les %(e:règles d'organisation et calcul) sont des enseignements sur des relations cause-effet, qui ne sont pas déterminés par des forces de la nature contrôlables.

#OWd: Seules les inventions (c-à-d les contributions techniques) sont brevetables.  Pour être brevetable, une invention doit en plus être nouvelle, non-évidente et destinée à une application industrielle.

#CeW: Domaine technique

#Peh: Inventions brevetables comme enseignements techniques

#Ailx: Une invention implémentée par ordinateur doit être considérée comme appartenant à un domaine de la technologie.

#Mue: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur soit considérée comme appartenant à un domaine technique.

#Mte: Les États membres veillent à ce que les inventions techniques soient brevetables indépendamment de la question de savoir si elles sont mises en oeuvre sur un ordinateur ou non, et de l'autre côté, que personne ne peut rendre les programmes d'ordinateur et autres règles d'organisation et de calcul brevetables en les présentant en combinaison avec des traits d'implémentation dans lesquelles il n'y a pas un achèvement qui justifie un brevet.  Les règles d'organisation et de  calcul doivent être considérées comme une partie du savoir public qui ne peut être que le point de départ d'une contribution technique digne d'un brevet.

#Tar2: Contribution technique

#Cse: Conditions de brevetabilité

#ItW2: Afin d'impliquer une étape inventive, une invention implémentée par ordinateur doit apporter une contribution technique. Une %(q:contribution technique) est une contribution à l'état de l'art dans un domaine technique qui n'est pas évidente à une personne compétente dans cette technique.

#Tnc: La contribution technique doit être estimée en considération de la différence entre la portée de la revendication du brevet considérée dans son ensemble, qui peut comprendre des caractéristiques techniques et non-techniques, et l'état de l'art.

#Mii: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur soit brevetable à la condition qu'elle soit susceptible d'application industrielle, qu'elle soit nouvelle et qu'elle implique une activité inventive.

#Mfm: Les États membres veillent à ce que pour impliquer une activité inventive, une invention mise en oeuvre par ordinateur apporte une contribution technique.

#Tba: La contribution technique est évaluée en prenant en considération la différence entre l'objet de la revendication de brevet considéré dans son ensemble, dont les éléments peuvent comprendre des caractéristiques techniques et non techniques, et l'état de la technique.

#MaW: Les États membres veillent à ce que les brevets soient accordés seulement pour des invention techniques nouvelles, non-évidentes et destinées pour une application industrielle.

#MWh: Les États-membres veillent à ce que les %(q:inventions techniques) soient entendues comme étant des enseignements techniques, c-à-d des enseignements sur les relations cause à effet de forces de la nature contrôlables.

#MWq: Les état membres veillent a ce que la brevetabilité est examinée dans l'esprit des %(EP): En estimant si une revendication révèle une invention technique, on ne doit tenir aucun compte de la forme ou du type de la revendication mais se concentrer sur le contenu afin d'identifier la contribution nouvelle que l'objet revendiqué fait à la technique connue.  Si cette contribution ne constitue pas une invention, il n'y a pas d'objet brevetable.

#FWc: Forme de Revendication

#Cnl: Revendications, Contrefaçon, Révèlation, Nouveauté, Création Antérieure, Industrialisation

#AWr: Une invention implémentée par ordinateur peut être revendiquée comme un produit, en particulier en tant qu'ordinateur programmé, que réseau informatique programmé ou autre appareil programmé, ou en tant que processus effectué par un ordinateur, réseau informatique ou appareil via l'exécution d'un logiciel.

#Msc: Les États membres veillent à ce qu'une invention mise en oeuvre par ordinateur puisse être revendiquée en tant que produit, c'est-à-dire en tant qu'ordinateur programmé, réseau informatique programmé ou autre appareil programmé ou en tant que procédé, réalisé par un tel ordinateur, réseau d'ordinateur ou autre appareil à travers l'exécution d'un programme.

#MWo: Les États membres veillent à ce que les revendications dans l'esprit des %(EP) soient examinées: Si la contribution à l'état de l'art se trouve dans un programme pour ordinateur, l'objet n'est pas brevetable, quelque soit la forme dans lequel il est présenté dans les revendications.  Par exemple une revendication sur un ordinateur caractérisé par un programme (fonctionnalité logique) stocké à l'intérieur ou sur un procédé pour faire fonctionner l'ordinateur sous contrôle de ce programme serait aussi inacceptable qu'une revendication sur le programme en tant que tel ou sur un disque contenant ce programme.

#sih: Les états membres veillent à ce que les revendications de brevets ne portent pas seulement sur l'opération des appareils de processement de données (c.a.d. ordinateur universel avec périphérie pour communication avec autres ordinateurs ou humains) mais doivent inclure l'opération d'appareils qui servent pour contrôler des force de la nature d'une façon inventive.

#Dte: Les États membres doivent s'assurer qu'un support de données, sur lequel des données précises ont été sauvegardées, ne puisse faire l'objet d'un brevet, et que la publication et la diffusion d'informations sur des medias de sauvegarde ne violant pas de brevet ne puisse jamais faire l'objet de la violation directe ou indirecte d'un brevet.  Les États membres protègent la liberté de l'expression orale et de l'échange d'information indépendamment de la langue et du but de l'emploi du texte concerné, et s'assurent que cette liberté ne puisse être limitée que par le droit positif et non les brevets.

#Man: Les États membres doivent s'assurer que l'exécution d'un programme d'ordinateur (un ordinateur avec des périphériques de sauvegarde, d'entrée et de sortie de données) ne puisse constituer une violation d'un brevet, alors que son usage, comme par expl. la distribution de produits chimiques piloté par un programme d'ordinateur, peut constituer une violation de brevet.

#scrambled: Les États membres veillent à ce que l'information numérique soit réputée comme étant une connaissance publique, dès que il est possible au public d'en prendre connaissance, même si la prise de connaissance ne résulte que d'un procédé laborieux ou illégal de déchiffrage, décompilation ou décryptage.

#prekre: Les États membres veillent à ce que toutes les oeuvres pouvant être protégées par le droit d'auteur, qui étaient en usage avant la publication d'un brevet, doivent pouvoir à l'avenir être continuées à être développées et employées pour l'exploitation des ordinateurs par le détenteur du droit d'auteur et de toutes les personnes qui ont été ou vont être autorisées par lui, selon des modalités précisées par le détenteur du droit d'auteur.

#progpub: Les États membres veillent à ce que toujours, quand une revendication d'un brevet suppose qu'un programme d'ordinateur est utilisé,  une implémentation fonctionnelle d'un tel programme fera partie de la description du brevet, laquelle sera publié sans conditions de licenses réstrictives.

#indapp: Les États membres veillent à ce qu'une invention brevetable doit être %(q:capable d'application industrielle) et que le terme industrie au sens du droit de brevet soit compris dans son sens stricte de %(q:production en série de biens matériels).  En général, les termes utilisés par le législateur pour limiter la brevetabilité ne doivent jamais être interprétés dans un sens qui enlève ou affaibli sérieusement leur effet limitatif.

#RWi: Rapport avec la directive 91/250/CE

#Aod2: Les actes permis en vertu de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateur par un droit d'auteur, notamment les dispositions particulières relatives à la décompilation et à l'interopérabilité ou les dispositions concernant les topographies des semi-conducteurs ou les marques, ne sont pas affectés par la protection octroyée par les brevets d'invention dans le cadre de la présente directive.,

#MtW: Les États membres doivent garder les sphères du droit d'auteur et des brevets clairement séparées.  La propriété pour les programmes d'ordinateur est acquise et régulée via le droit d'auteur.  La propriété pour les inventions techniques est acquise et régulée via les brevets.   La propriété qui a été acquise via un système ne peut être restreinte par un autre.  Les aspects d'un programme d'ordinateur qui ne peuvent être attribués via le droit d'auteur ne peuvent aussi être attribués via les brevets, les certificats d'utilité ou n'importe quel autre régime de propriété.

#interop: Les États membres veillent à ce qu'en tout lieu où une utilisation d'une technique brevetée est exigée pour la communication avec un autre système (c.-à-d. pour la convertion en import et en export des conventions de ce système), une telle utilisation ne puisse être considérée comme une contrefaçon.

#Mir: Suivi

#Tnt: La Commission surveille l'incidence des inventions mises en oeuvre par ordinateur sur l'innovation et la concurrence en Europe et dans le monde entier ainsi que sur les entreprises européennes y compris le commerce électronique.

#Tia2: Le Parlement Européen doit surveiller l'impact des brevets sur l'innovation et la concurrence, les deux en Europe et internationalement, et sur les compagnies européennes, et sur le commerce électronique.

#ReW: Rapport sur les effets de la directive

#Tnt2: La Commission doit surveiller l'impact des inventions implémentées par ordinateur sur l'innovation et la concurrence, les deux en Europe et internationalement, et sur les compagnies européennes, dont le commerce électronique.

#Typ: La Commission doit envoyer au Parlement Européen et au Conseil dans les trois ans de la date spécifiée à l'Article 6(1) un rapport estimant l'impact des brevets sur les inventions implémentées par ordinateur sur les facteurs mentionnés dans le paragraphe 1.

#Iir: Dans ce contexte, le rapport va aussi aborder les questions de savoir si les règles gouvernant la détermination des exigences de la brevetabilité, et plus spécialement la nouveauté, l'étape inventive et la juste portée des revendications, sont adéquats; et si des difficultés ont été connues à l'égard des États Membres où les exigences de nouveauté et d'étape inventive ne sont pas examinées avant la publication d'un brevet, et si cela est le cas, si quelques mesures sont désirables pour s'attaquer à de telles difficultés.

#Tme2: La Commission soumet au Parlement européen et au Conseil, pour le [DATE (trois ans à compter de la date spécifiée à l'article 9 (1))] au plus tard, un rapport indiquant :

#teW: l'incidence des brevets délivrés pour des inventions mises en oeuvre par ordinateur sur les éléments mentionnés à l'article 7;

#wtn: si les règles régissant la détermination des critères de brevetabilité en ce qui concerne plus précisément la nouveauté, l'activité inventive et la portée des revendication sont adéquates : et

#wra: si des difficultés sont apparues dans les États membres où les aspects de la nouveauté et de l'activité inventive des inventions ne sont pas examinés avant la délivrance d'un brevet et si des mesures doivent être prises, le cas échéant, pour y remédier.

#Toi2: Le Parlement Européen doit former une commission d'enquête sur les limites de la brevetabilité.  La commission d'enquête est pourvue des procurations et des moyens financiers nécessaires, afin de se procurer auprès de l'Office Européen des Brevets, de la Commission Européenne et des acteurs importants du secteur, les informations requises sur la pratique de l'exploitation des brevets, sur les répercussions des brevets sur tous les types de flux financiers, sur les dispositions politiques vis-à-vis du système des brevets etc. La commission d'enquête doit proposer des règles efficaces et durables qui apporteront plus de transparence et plus de contrôle sur le système des brevets et avec lesquelles ce système pourra être contenu dans des limites raisonnables. La commission d'enquête doit mettre en avant les possibilités de recherche ouverte dans ce domaine. La composition de la commission d'enquête ne doit comprendre plus de 20% de personnes dont l'existence économique ou professionelle profite du système des brevets.

#nno: to what extent and under what terms licenses have become available for the patents granted under this directive and to what extent licensing information is accessible to the public.

#Iet: Mise en oeuvre

#Mnl: Les États Membres mettre en vigueur les lois, réglementations ou provisions administratives nécessaires pour se conformer à cette Directive au plus tard le ... Ils doivent en notifier la Commission immédiatement.

#Mvf: Les États membres mettent en vigueur les dispositions législatives, réglementaires et administratives nécessaires pour se conformer à la présente directive, au plus tard le [DATE (dernier jour d'un mois)] et en informent immédiatement la Commission.

#Wrp: Quand les États Membres adoptent ces provisions, ils doivent contenir une référence à cette Directive ou doivent être accompagnés par une telle référence à l'occasion de leur publication officielle. Les méthodes pour faire une telle référence doivent être fixées par les États Membres.

#WDi: Lorsque les États membre adoptent ces dispositions, celles-ci contiennent une référence à la présente directive ou sont accompagnées d'une telle référence lors de leur publication officielle. Les États membres déterminent la manière dont cette référence doit être faite.

#Mia: Les États membres communiquent à la Commission le texte des dispositions de droit interne qu'ils adoptent dans le domaine couvert par la présente directive.

#EWW2: Entrée en vigueur

#TWi2: La présente directive entre en vigueur le vingtième jour suivant celui de sa publication au Journal officiel des Communautés européennes.

#Ars: Destinataires

#Tsh2: Les États membres sont destinataires de la présente directive.

#Dau: Fait à Bruxelles,

#Fua: Par le Parlement européen

#Fhu: Par le Conseil

#Pres: Le Président

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-kern ;
# txtlang: fr ;
# multlin: t ;
# End: ;

