\contentsline {chapter}{\numberline {1}What is the current situation?}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Explaining the ``Current Situation'' by a Historical Myth}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Impressive Statistics of Patent Inflation}{6}{section.1.2}
\contentsline {section}{\numberline {1.3}Preventing the Drift by Imposing It (I)}{6}{section.1.3}
\contentsline {section}{\numberline {1.4}Controlling the EPO by Following it}{8}{section.1.4}
\contentsline {chapter}{\numberline {2}What are the aims of the proposal?}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Harmonising a Uniform System, Clarifying that Confusion Rules}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}Preventing the Drift by Imposing it (II)}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Ensuring EU-Wide Incontestability of Software Patents (I)}{10}{section.2.3}
\contentsline {section}{\numberline {2.4}Authorising an Already-Authorised Court}{10}{section.2.4}
\contentsline {chapter}{\numberline {3}How important is software to the EU economy?}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}A multi billion dollar market}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Europe's Future as Seller of Patented Ideas}{13}{section.3.2}
\contentsline {chapter}{\numberline {4}What sort of programmes will the Directive cover?}{14}{chapter.4}
\contentsline {section}{\numberline {4.1}Patentable Programmes = those which the EPO deems ``technical''}{14}{section.4.1}
\contentsline {section}{\numberline {4.2}Software Industry still Thriving, so No Worry?}{14}{section.4.2}
\contentsline {chapter}{\numberline {5}What sort of programmes will be excluded?}{15}{chapter.5}
\contentsline {section}{\numberline {5.1}Programs and Algorithms unpatentable ... unless patentable}{15}{section.5.1}
\contentsline {section}{\numberline {5.2}Business Methods unpatentable ... unless patentable}{16}{section.5.2}
\contentsline {section}{\numberline {5.3}``Restrictive Statement of the Law'': certain nothings remain unpatentable}{20}{section.5.3}
\contentsline {chapter}{\numberline {6}What would the situation be without the EU directive?}{21}{chapter.6}
\contentsline {section}{\numberline {6.1}EPO sets the rules, either with or without EU participation}{21}{section.6.1}
\contentsline {section}{\numberline {6.2}Preventing the Drift by Imposing It (III)}{21}{section.6.2}
\contentsline {section}{\numberline {6.3}USPTO sets the rules, either with or without EU participation}{22}{section.6.3}
\contentsline {chapter}{\numberline {7}What international agreements and conventions need to be taken into account?}{23}{chapter.7}
\contentsline {section}{\numberline {7.1}Stripping Invention Concept from Art 52 EPC}{23}{section.7.1}
\contentsline {section}{\numberline {7.2}Stripping invention concept from TRIPs}{23}{section.7.2}
\contentsline {chapter}{\numberline {8}How do patent and copyright protection differ?}{25}{chapter.8}
\contentsline {chapter}{\numberline {9}What are the objectives of the report?}{27}{chapter.9}
\contentsline {section}{\numberline {9.1}Objective: EU-wide incontestability of software patents (II)}{27}{section.9.1}
\contentsline {section}{\numberline {9.2}First Kill, then Appoint Killer as Detective}{27}{section.9.2}
\contentsline {section}{\numberline {9.3}Prevent Drift by Imposing it (IV)}{28}{section.9.3}
\contentsline {chapter}{\numberline {10}Annotated Links}{29}{chapter.10}
