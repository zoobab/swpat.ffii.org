<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: McCarthy 2003/05/03: Software Patent Directive Proposal FAQ

#eWp: Arlene McCarthy, member of the European Parliament and rapporteur of the Legal Affairs Commission (JURI) on the Software Patentability Directive Proposal explains her point of view in a FAQ manner.  She asked us to distribute this document to the participants of a conference which we organised in Brussels.  In a nutshell, she says that %(ol|Software patents, as granted by the %(tp|European Patent Office|EPO) at present, are needed for various reasons, e.g. protecting Europe against competition from Asia, allowing European SMEs to compete in the US, etc|The EPO is granting software patents but not patents on non-technical algorithms and business methods.|The current proposal is designed to ensure that the EPO's practise is followed throughout Europe in a uniform manner and that a drift toward patenting of %(q:non-technical algorithms and business methods) is halted.)  We debug McCarthy's questions and answers one by one.  It seems that many of McCarthy's proclaimed objectives could be achieved only by voting in favor of a series of CULT, ITRE and JURI amendment proposals which unfortunately have not enjoyed the support of the rapporteur.

#curr: What is the current situation?

#aims: What are the aims of the proposal?

#swimp: How important is software to the EU economy?

#patprog: What sort of programmes will the Directive cover?

#nepatprog: What sort of programmes will be excluded?

#seneudir: What would the situation be without the EU directive?

#legenv: What international agreements and conventions need to be taken into account?

#patvskr: How do patent and copyright protection differ?

#robjv: What are the objectives of the report?

#IWa: Impressive Statistics of Patent Inflation

#nWg: Preventing the Drift by Imposing It (I)

#oPi: Controlling the EPO by Following it

#nWi2: Until 1986, the European Patent Office strictly refused granting of software patents.

#rda: It may well be that patents involving the use of software have been granted since the earliest days of the European Patent Office. They certainly weren't called %(q:computer-implemented inventions), though. This term was %(tw:introduced in 2000 by the EPO). In EPO's youth it was clear that an invention could not be %(q:implemented) in a computer. Let's be precise about terminology because that will allow us to identify a confusion that may be the source of much of the disagreement.

#fqu: The Trilateral initiative of EPO with US and Japanese Patent Offices introduced the term %(q:computer-implemented invention) for the purpose of promoting business method patents in 2000: %(URL)

#vWi: An invention is a new, inventive (non-obvious),industrial-applicable and statutory (not excluded) teaching. In a patent there is a description of the invention and a collecton of claims. The claims define the monopoly that the patent grants, and must include the invention, but usually will include many other things which are not new, inventive, etc. but are needed to put the invention to work (for instance if you invent a new shape for a plane's wings you may want to claim planes with those wings, or the wings adapted to accomodate engines or landing gear, or the wings made of known materials, or whatever).  You are not patenting all these known things you use to put your invention to work, it is only their use to realise your invention what is monopolised, so even if they appear in the claims, they are not patented as such (you can still use engines, wheels, materials in wings with other shapes, so it is not the wheel as such which is patented). What characterizes the claimed object is the invention, not any accessory needed to make it useful.

#eco: In this sense, the claimed objects since the beginning of the EPO may well have included computers and software. But programs for computers is excluded subject matter (EPC 52), and therefore it can only be an accessory in the claims, it is explicitely disallowed as an invention, so that it can not be patented as such. You can, for instance, patent an industrial chemical process if you find some new and inventive way of mixing reactives under some controlled conditions. The control of this conditions may involve a computer and software, but you cannot claim the software as such. That is, it is not allowed to claim any unspecified industrial process that may be controlled by a computer with some particular new program. The program may be new, in a sense non-obvious, and be usable in industry, but it is not an invention, so it needs to be claimed, if at all, as an accessory of some invention.  This means that the program itself will not be patented, although it may be mentioned in the claims.

#hcn: So it is necessary to discern what is an invention from what is not, then examining the claims. This is clear enough in EPC 52, and was explicitly regulated in the %(eg:EPO's Examination Guidelines) of their %(q:earliest days) (1978).

#oto: A computer program may take various forms, e.g. an algorithm, a flow-chart or a series of coded instructions which can be recorded on a tape or other machine-readable record-medium, and can be regarded as a particular case of either a mathematical method or a presentation or information.If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims.  For example, a claim to a computer characterised by having the particular program stored in its memory or to a process for operating a computer under control of the program would be as objectionable as a claim to the program per se or the program when recorded on magnetic tape.

#orn: Apologetics for Violation of Law

#ioi2: We have pointed out McCarthy's error before but never received a response.

#IWs: Very impressive indeed.  Patent Inflation is a cause of concern, although according to %(fs:FFII statistics), in 2001 the number of real software patents was around 5000.

#aon: %(bh:Bessen & Hunt) show that strategic, anticompetitive and defensive use of patents tends to concentrate in software patents, because they are easier to obtain (they don't require experimentation or prototyping, not even writing a program). They are also broader, because software is not subject to physical constraints and can therefore be composed into more complex systems, potentially infringing in hundreds of patents per program. This causes a patent buildup simlar to a cold war arms race that discourages innovation and competition, and instead of bringing new products to consumers, reduces their choice and their access to infomation society, resulting in significant costs and less productivity for businesses.

#wWo: Given the current imbalance of costs and rewards in the EPO, where granting a patent brings revenues and rejecting it brings complaints and extra work in justifying the rejection, it is too easy for major corporations to build caselaw by simply drowning the EPO in applications for excluded subject matter.

#elo: So it is important to resist the pressure in order to keep a healthy knowledge economy.  The best option would be, as the %(ce:Economic and Social Committee of the EU suggests), to reform or replace the EPO. A more inmediate way, though, is to have the current law (EPC) enforced to ensure that software is not patentable, and any software patent granted by the EPO is consistently invalidated. This may reduce the incentive to apply for excluded subject matter, and will of course bring legal certainity to developers and enterpreneurs.

#leu: %(sk:Moreover, concern has been raised that the EPO has already drifted a long way towards extending the scope of patentability to achievements which traditionally would not have been called inventions, and hence are not patentable). %(sm:Examples of improperly granted patents abound).  As Piia Noora Kauppi MEP said in Brussels 2003.05.07: %(q:The EPO has been running wild).

#b8e: The expansion of subject matter is evident when comparing the %(e1:1978 EPO guidelines) quoted above with the current %(e2:2001 EPO guidelines):

#WtW: Which means that if you want to patent any software concept nowadays you just have to claim it has a %(q:futher technical effect) (which is defined to include either %(q:technical considerations) on how a computer works, %(q:technicality) in the meaning attributed to the data the software handles, or the efficiency and security of the process it describes).

#ofi: Since any program for computers can be presented as having some efficiency or security trade-off, any software innovation can be patented.

#dWt: If the program has any usefulness, so that its data can be said to refer to the real world, it will also be deemed to have %(q:further technical effects).   According to this doctrine, a program that adds 3 litres of fuel in a tank and 3 litres of fuel in another is technical, while a program that adds 3 EUR in an account, and 3 EUR in another account is not. The program is exactly the same, requires the same investment, and the only difference is what you apply it to.

#itl: However, you don't even need to refer to physical objects or allege efficiency or security trade-offs to patent software.  Usually it will be enough if your innovation is explained in terms of %(q:technical) objects on which any computer program operates, such as memory, display, processor or storage means.  Such %(q:technical considerations) will, in the EPO's view turn your innovation into something more than a program for computers as such:

#Wey: In other words, anything you can do by programming a computer is patentable, since you can claim a computer system programmed in some way. Since the computer system needs not be specified, and the characterisation may focus on the software, this means you can monopolise the use of any software by patenting a generic computer system with the software.

#Aeg: This clarifies that the EPO will interpret Art 52 in a manner equivalent to not having any exclusion for %(q:programs for computers) at all. They have decided to apply the tests of novelty, inventive step and industrial applicability and disregard the test for whether there is a patentable invention.

#naw: There is even a manual on %(hp:how to patent any software concept), and abundant %(sp:granted software patents) confirm the lack of practical limits. Any program can be claimed to be more secure than some other, or to use less resources, and of course anything you write a program for can be claimed to be more efficiently done with computers than manually. But if we accept this criteria we are refusing any limits on subject matter and instead turning to usefulness, which is the US situation, maybe spiced with some additional rethorics about (non innovative) technical means.

#tlt: The confusion arises because we desist in trying to assess what new teaching society gets from the patent and concentrate in the final result. When a programmer optimizes a program so that it runs faster, or is more efficient, she is simply looking at what operations are not necessary to solve the problem, what results can be reused, or what organisation of data simplifies its use.  She is not finding a way to etch smaller transistors in semiconductors so that the computer can perform more operations per second, or a better material to dissipate generated heat. There is no techincal device running faster, there is simply less use of the same technical device.

#Wat: There's a story about a famous mathematician who was punished in school when he was a child. The teacher had him add up all the numbers from 1 to 1000 so that he would not disturb for a while.  Very shortly the child came up with the answer 500500, and the teacher was outraged, thinking he made that up, until the child explained it was very easy, look: 1+1000 = 2+999 = 3 + 998 = ... = 500 + 501 = 1001. So he didn't have to calculate 999 additions (1 + 2 + 3 +...), he could simply calculate one addition giving 1001, and then multiply by 500, because he saw there were 500 additions giving the same result. Did he use any brain accelerating drugs?  A faster pen for writing partial results? No. He %(q:simply) found a way to do less operations, at the same speed as usual. If he had used a calculator instead of pen and paper, the EPO would probably deem the calculation speedup as a technical effect beyond normal interactions of arithmethic operations and a calculator, and would have granted a patent on it. But this is what programmers do daily in their jobs, so granting a patent for eficient software would be the information society equivalent of granting a patent for turning bolts in an engine assembly line in the industrial society. It is similar to claiming that driving a known car through a shortcut between two towns is a car boost and should be patentable, since it not only has the technical effect of moving people around, but also it has the further technical effect of reducing trip time.

#Wtg: This should be sufficient to show there is little fear that the EPO may drift towards granting patents for anything more you can do with computers, since there is little more left to include, but the current practice deserves concern. And this is not just a matter of legal tradition but of economic and social policy which should be based on sound understanding of what computers and software really are and how development and research are done.

#rAi: Very possibly so, indeed. %(ds:The EU needs to take the step to democratically scrutinize the EPO). This means the pratice of the EPO must be questioned and whatever is legislated on software patentability must come from an informed inquiry and debate.  Legislation should build on the decision by the democratic legislator of what is good or bad for the European economy and values. Accepting the current scope of patentability in the EPO practice, as the CEC proposal and the JURI rapporteur draft report do, without thorough economical, social and human rights justifications would be evading the responsibility of the democratic scrutiny, or even hinting that the EPO is wellcome to undemocratically set the rules by drifting of its practice and subsequently expect the EU to endorse the fait accompli.

#mTn: As amendment 2 in either ITRE or CULT opinions point out

#ijn: Harmonising a Uniform System, Clarifying that Confusion Rules

#itg: Preventing the Drift by Imposing it (II)

#nle: Ensuring EU-Wide Incontestability of Software Patents (I)

#ree: Authorising an Already-Authorised Court

#wWe: %(q:Harmonisation) might be a noble goal if only the law was different in different member states.  However the substantial patent law of the member states is already completely unified by the European Patent Convention, by which all the EU member states are bound and whose Articles 52-57 have been literally incorporated into the national laws. There may remain differences in related law (like fiscality of patents, etc.) but not in patentable subject matter.  Any difference in subject matter is caused by the particular interpretation the EPO makes of the unified law.  Some national courts follow this interpretation while %(bp:others do not). These differences can be observed even within the same member states.

#lht: Yet clarity is important, since the more clear the law is the less room for differing interpretations (assuming rationality and good will in the interpreters). But one may wonder how much clarity the CEC proposal provides when the documents published by the Commission with the directive included a %(qa:questions and answers document) which, when discussing the controversial %(q:one click shopping) patent granted to Amazon.com, says that patentability of this would be %(q:highly unlikely) under the proposed rules (due to lack of novelty or non-obviousness), but that it did not want to rule absolutely on this matter because the EPO was still studying it. This casts doubt on the legislator's intention to control the EPO or to clearly exclude even business methods from patentability.  Instead, it suggests that the new criteria are so ambiguous that not even those who propose them are capable of applying them to an example which they themselves have chosen, and that the EPO alone is the master of the rules.

#nnW: Law experts say the directive does not clarify anything. See for instance the %(bh:Bakels & Hugenholtz study) (or Bakels' speech in Nov the 8th conference in the European Parliament, saying that the lack of clarity in the directive was bad for both pro software patent people and people against software patents, and good only for lawyers who could profit from the confusion).

#fsr: The wish for clarity is thus justified indeed, since it can only be fulfilled by amendments which define terms that are central to the criteria in the directive, but remain undefined in the CEC proposal or the JURI rapporteur's draft report (such as amendment 45 defining %(q:technical), amendment 51,52 and CULT amendment 14 explaining inventive step/technical contribution), or other %(ja:amendments) that put forward clearer criteria that would set up more meaningful limits.

#snp: Currently the only part left for this drift towards absolute patentability of software is providing the missing legal basis. As explained above, patents are %(sk:already granted routinely by the EPO for all kinds of software innovations). The directive proposal, by borrowing from the EPO practice, would open the floodgates of software patent enforcement.

#5ao: The goal of stemming the drift from the EPC would be better served by %(ja:amendments 32,33,41,43,45,46,48,51,52,59 and some others), which reinstates the exclusion of programs for computers (and hence data processing) from patentability, and impose a true limit (forces of nature). The only sound way to separate patentable from unpatentable subject matter is to look at whether the innovation is of deductive or empirical nature, i.e. whether it is logical or material, so that teachings that require experimentation, laboratories and prototypes are covered by patents and copyright covers the result of composing solutions by logical combination of operations defined in a formal model.

#htm: As someone put it in the European Parliament conference, 2003.05.08, %(q:it is tradition when levelling a playing field, to remove the dirt, not to add more).

#tgb: So in case the paragraph is meant to imply that software patents pass from being of uncertain validity to being uncontestable, as the CEC proposal and the JURI's rapporteur draft report would bring, we should keep in mind that software patents as granted by the EPO have proved %(es:harmful to the economy and society), are %(do:rejected by most of the software business and academics), and are %(ic:inconsistent).

#upc: The distiction between material and immaterial innovations as outlined in our %(cp:Counter-Proposal) and in many of the amendments currently under discussion in JURI would provide a consistent fixpoint where the pratice of all patent courts could converge, instead of a wandering sequence of EPO caselaw eluding any systematic evolution, which the member state courts are asked to follow and the elected representatives to legalize after the fact.

#mlu: This is a legitimate goal, but care is needed to achieve it. For the Court of Justice (which is already going to be competent by virtue of the Community Patent anyway) to be able to ensure a high degree of legal certainty and uniformity the legislator must give it a clear and unambiguous law. Merely endorsing unlimited patentability with a requirement of rethorical application crafting is of little use to any court. Those proposals will only generalise uncertainity in software business and, according to Bakels, a long delay until cases are resolved while courts struggle to make sense of undefined concepts. As long as the criteria allow software patents, the legal uncertainity of the software developers and users will be increased, since they'll have no way of knowing whether they are infringing on someone else's patents.

#iaW: This is all the more so as specialised courts tend to promote their own field of spciality and, in this case, act as promoters of patent inflation, as can be seen from the US experience with the Court of Appeal of the Federal Circuit (CAFC).

#oWt: A multi billion dollar market

#ekt: Europe's Future as Seller of Patented Ideas

#Wne: Indeed, almost anyone depends on computers today, and there are millions of professionals devoted to developing, maintaining and administering software.  Most of these professionals (%(cs:>70% in Catalonia)) work in small or medium enterprises, which have shown %(dc:deep concern about the CEC proposal).

#Cns: %(gc:Statistics on Information and Communications Technology sector) from the Generalitat de Catalunya (catalan government) say that for those companies with more than 10 employees, around 70% of jobs are in companis with less than 200 employees.

#isW: As was evident in the %(be:Brussels events of May 7th and 8th 2003)

#tnW: This is an important reason to take the utmost care in any legislation that may affect such an enourmous market, such a widespread infraestructure and such an strategic EU policy (e-Europe, etc.).

#eWt: For a legislator to alter the information society landscape care should be taken to fully understand all the issues. Neither intuition nor easy analogies nor one sided interests should be used to rush uninformed decisions. Time should be spent on independent inquiry to understand the market and the nature of software and computers if there is honest interest in preserving and improving the EU advantage and opportunities in knowledge economy.

#eon: It would be more accurate to say that %(q:Creations engendered as a result of software development are important to the EU economy).  Software consists of abstract ideas, woven together into complex tissues by creative knowhow.  Productivity in software creations is endangered, if ideas are covered by patents.

#Wsp: %(bh:Bessen and Hunt) show empirical evidence of a 10-15% decrease in R&D intensity in the US since they started to grant software patents. %(bm:Bessen and Maskin) provide a theoretical explanation of this behaviour. The US %(ft:Federal Trade Commission hearings), news clips and other sources provide anecdotal evidence and %(sc:quotations). The %(sp:European Software Patents Horror Gallery) proves that criteria as those in use by the EPO and proposed in the CEC text and the JURI rapporteur's draft have resulted in the same kind of broad, blocking and unnecessary idea patents in Europe as in the US.

#cne: The opposition of around 95 % of SMEs in the %(ek:CEC Consultation) clearly indicates that the innovators in the European software economy are asking %(q:please, potect us from patent protection).

#WpW: This statement is difficult to understand, and its relation with the directive is unclear. Patents do not discriminate by nationality of the applicant, so foreign companies can and do patent at the EPO. In software, most of the already granted software patents that the rapporteur's draft would legalise are in the hands of US and Japanese companies.

#non: If the intended meaning is that in an age of de-industrialisation it is of utmost importance to make Europe's knowledge economy efficient, then this is another reason to keep software out of reach of patent lawyers, and to rely on proper economic research rather than analogy and intuition for deciding economic policy.

#tWe: We have answered to this more in depth in our reply to the McCarthy Draft Report under the heading %(os:He who diggeth a Pit ...).

#eni: Patentable Programmes = those which the EPO deems %(q:technical)

#ehd: Software Industry still Thriving, so No Worry?

#Wgg: This has already been commented above. But it's unclear how must it be interpreted under the title %(q:What sort of programmes will the Directive cover?).  There is no explanation on any criteria defining patentable programs, only a suggestion that lots of software patents have already being granted under the label of %(q:technical inventions), and correctly so.  It can be inferred from this that tens of thousands of US-style broad and trivial software patents granted by the EPO are endorsed by the EU legislator, and that, rather than imposing clear and uniform patentability criteria, the EU will endorse whatever the EPO grants.

#oco: We can agree to the prosperity of the European software business, including free software (or open source software if you like). In spite of disturbances caused by too simplistic spectations and unfounded valuation of inmaterial assets in the .com crash, European software creativity is in good shape. Despite the documented %(ae:anecdotal evidence of harm caused by European software patents granted), the general understanding of their invalidity, backed by some courts, and the small number of court cases may have not been able to slow down very much the pace of progress.

#avo: But current prosperity in a sector is hardly a reason to change its current laws. The anecdotal European evidence and the US experience strongly suggested that a move to validate current and future European software patents would worsen the situation, and bring a new wave of unfounded speculation this time on inflated industrial property titles.

#thn: European software businesses would be better served by a clarification that software patents are invalid and should not be a cause of concern.  This would lead to higher legal security and confidence in copyright over one's own work, higher investment (as %(lc:testified) by venture capitalist Laura Creighton in the European Parliament) and a more competitive environment, unimpeded by any remaining intimidation power of currently granted software patents. This is what this sector is demanding.

#dew: Programs and Algorithms unpatentable ... unless patentable

#aes: Business Methods unpatentable ... unless patentable

#nea: %(q:Restrictive Statement of the Law): certain nothings remain unpatentable

#hem: Programmes which produce an aesthetic outcome, components of programmes (code sequences) which of themselves do not deliver any technical solution, algorithms and underlying programming ideas will not, as now, be patentable.

#acw: This is the same as saying %(q:only nothings are excluded from patentability), as we shall show below.

#iWo: Again we see the term %(q:technical) used without a clear definition.  Based on its use by Arlene McCarthy in this and her previous documents, we can assume that it means %(q:functional) as an antonym to %(q:aesthetic).  Which means that %(ms:anything useful is patentable) or %(ms:all practical problem solutions are technical inventions), as expressed by an EPO official in 1998.

#oye: Programs which produce an aesthetic outcome may not be patented because of lack of interest by the author, but there are a lot of what the EPO would call %(q:technical considerations) involved in such a program (graphic compression, efficiency optimisation, or simply claims which refer to generic computing equipment).  So they could not only be %(ap:patented if desired), but they are likely to infringe many already granted software patents, and there is nothing in McCarthy's proposals which could prevent this.

#roe: Some EPO-granted patents already come quite close to aesthetic programs:

#asm: Graphics elements

#osi: A code sequence is not interesting to patent because it is too narrow.  You can appropiate a code sequence by copyright, using patents for that would only be more expensive and have a shorter lifespan. Software patents claim a broad range of possible code sequences characterized by some new rule of calculation or organisation, not simply a code sequence. It is this broadness that makes software patents useful for anticompetitive strategies (besides the advantage of being cheap since they don't require empirical research).

#otc: There is nothing in McCarthy's proposal which could prevent algorithms or %(q:underlying ideas) from being patented. Indeed later in this text McCarthy writes that patents are needed because copyright does not cover the underlying ideas.  Moroever, it is contradictory to say that %(q:underlying ideas will remain unpatentable) when at presen the EPO, whose %(q:status quo) McCarthy wants to impose, has already granted more than 30000 EPO patents on underlying ideas (programming tasks) and algorithms.

#ewh: Software is the art of abstraction.  There is no way to patent software without patenting abstract ideas.  So what is there to patent in software, if algorithms are unpatentable?

#nid: McCarthy excludes only %(q:non-technical business methods) from patentability, and the European Patent Office (EPO) finds all business methods to be %(q:technical).  Let us quote what the EPO and leading patent practitioners have to say about this:

#pee: A US patent attorney explains how claims to business methods must be crafted if they are to pass at the EPO in the light of the %(q:Pension Benefit System) decision.  Basinski concludes that %(bc:If you have any intent to file a Business Method type application in Europe/Japan/etc. and even for purposes of the US you will have to approach the invention a little differently.  The EPO rules should generally be followed because if you do it their way it should work in the US and Japan.)  Basinski explains that, by following the EPO's rules, the applicant will have to add some redundant specifications to his patent description which do not make the patent less broad but rather more cryptic: %(bc:In the background section, one should describe the %(q:technical problem) addressed by the invention, in terms of how the system/method makes the computer function %(q:more efficiently) %(q:faster) %(q:in a less costly way) %(q:more of something) in doing what the invention is doing %(pe:such that you have a %(q:further technical effect) rather than just the interaction of computer and its instructions). The description of the technical solution then becomes a cryptic statement of what the invention does and should specify the novelty and inventive step.)

#efe: An editor of a much-read Patent Newsletter and operator of a patent busting business explains that the EPO's %(q:Pension Benefit System) decision is based on meaningless concepts which seem to serve mainly as a pretext for politically motivated arbitrary decisions.  Quotes an EPO official as saying in private correspondance about what the EPO means by %(q:technical): %(bc:I'm glad you asked me about that. It's a wonderful word, fuzzy and yet sounds meaningful. We love it. Until 2001, it had no basis whatsoever in the EPC, just a passing mention in a couple of rules. .... I agree that we've never defined 'technical'. It's deliberate and allows us to fine-tune as a consensus develops on what should be patentable. Yes, I do know the correct way to do that is by amending the law, but have you any idea how hard it is to get consensus on amending the EPC?)

#sWW: EPO decides in 2000 that the %(q:computer-implemented business method) in question is statutory subject matter (i.e. an %(q:invention)), but does not %(q:make a technical contribution in its inventive step).  Amidst their efforts at revising Art 52 EPC in summer 2000, EPO representatives immediately touted this as the new model caselaw of the EPO.  It later became, together with Appendix 6 of the Trilateral Conference documentation, the basis of the CEC software patent directive draft.  It is however to be doubted whether this decision provides a clear guidance to anyone except perhaps for claim-drafting patent attorneys.  The judges admit that their basis for rejecting the patent is undefined:  %(bc:It may very well be that, as put forward by the appellant, the meaning of the term %(q:technical) or %(q:technical character) is not particularly clear. However, this also applies to the term %(q:invention). In the Board's view the fact that the exact meaning of a term may be disputed does in itself not necessarily constitute a good reason for not using the term as a criterion, certainly not in the absence of a better term; case law may clarify this issue.)

#imbmepot: Iusmentis: EPO Business Method Patents

#imbmepod: A patent attorney informs about the EPO's practise of granting business method patents, citing numerous examples.

#tlb: It should be emphasised that ... the computer implementation of a ... business method, can involve %(q:technical considerations), and therefore be considered the solution of a technical problem, if implementation features are claimed.

#dob: K. Beresford, World Patent Information 23 (2001) 253-263: European patents for software, E-commerce and business model inventions.

#adt: Abstract: It is commonly held that software, e-commerce and business models related inventions are inhenrently unpatentable in Europe. In reviewing the situation, this article emphasises the large number of inventions in this field which have in fact been patented through the EPO or through national patent offices in Europe. [...] He shows that, if proper attention is paid to both the substance and the form of claims and description, to direct the reader to the technical problem and its solution, effective protection is in fact very often available.

#Woa: In 2002 the German Federal Patent Court (Error Search decision, 26 March 2002, BPatG 17 W (pat) 69/98) rejects an EPO patent on a non-technical innovation and points out that the EPO's doctrine of %(q:technical contribution) makes all business methods patentable and is therefore in violation of the European Patent Convention:

#sea: Examples of EPO patents on financial methods:

#lWk: Realkredit Danmark AS

#euW: Most of the notorious US business method patents have cousins at the EPO.  One of the related news headlines was:

#aWk: Divine Inc. attacked Thompson and Morgan (a company who made business in the USA) with patent US5715314. The EPO database gives an equivalent European Patent, %(EP), on electronic commerce with a bank payment gateway. Thompson and Morgan has not been sued in the UK, as far as we know, but the chances for Divine Inc. to try it in the UK too will increase if the technicity criteria in the EPC is dismantled as per the CEC proposal or the JURI rapporteur's draft report. It may be interesting to ask whether the rapporteur welcomes such news or, if not, what provisions in her directive proposal a judge in Europe could use to invalidate this patent.

#rdf: Indeed the shouldn't.  But undoubtedly will.  McCarthy is stating desirable policy goals, but proposing measures which achieve exactly the opposite of the proclaimed goals.

#WWW: The EPO is regularly granting patents on components, algorithms, non-technical innovations etc.  The McCarthy report endorses the EPO's practise. There is nothing in the McCarthy report that would prevent such patents from being granted and enforced, and this appears to be intended.   According to what McCarthy says later in the text, %(q:copyright is insufficient), because it does not allow patenting of %(q:underlying ideas).

#lro: EPO sets the rules, either with or without EU participation

#iyI: Preventing the Drift by Imposing It (III)

#rlo: USPTO sets the rules, either with or without EU participation

#ucu: Without the EU directive the situation would be the current one, which was reasonably presented as in good shape above.  Since %(ep:the written law is crystal-clear) and the EPO's caselaw is not binding on national caselaw, EPO-granted patents will most likely continue to be revoked by national courts, the EPO itself will be under pressure to reform.

#oer: The directive as proposed by the CEC would support the EPO's trespassing into unpatentable subject matter, and encourage further expansion beyond computers by fait accompli politics. The JURI committee rapporteur's draft report would not make the situation any better.

#yvE: The directive is difficult to amend since it is based in fundamentally wrong assumptions, and includes many redundant provisions to erode the delimiting criteria mandated by the EPC. Yet the three European Parliament committees involved have made a good job together, and there are tabled %(ja:amendments) that could solve most of the problems and improve clarity and juridical certainity for European Citizens, by reinforcing the EPC and helping EU judges invalidate software patents.

#gWs: So in case the %(ja:right amendments) are all voted and the wrong ones are all rejected, the directive could indeed improve the situation to some extent. Concessions or deviations from this baseline, with very few exceptions, could lead to stagnation of the currently healthy European software sector.  So fixing the directive is indeed the best and necessary option, but the only alterantive to a real, sound and clear fix is rejection.

#hmt: Unfortunately, Arlene McCarthy has steadfastly opposed such amendments and proposed only amendments which further loosen the directive proposal and ensure that there can be no limit on patentability.

#yud: Devaluating the debate and democratic scrutiny of the EU by dogma, weak arguments and threats of administrative malpractice, or ignoring the economic evidences, stakeholders view, and basic values would be even worse than keeping the EU uninvolved. The legitimity of the EU cannot be put in doubt by uninformed decision making, disregard for public opinion or interest or blind following of any institution.

#nmo: At the very least, any proposed legislation should be tried with some example patents and the benefits of the outcome should be justified.

#trm: If this argument was valid, it would mean that Europe must grant patents on whatever the US may deem patentable in order to keep up with the US.  This argument is incompatible with the proclaimed intention to avoid a drift.

#Wtt: McCarthy moreover causes confusion by alleging that someone wants to %(q:abolish all patents on computer-implemented inventions).  We know of no call to deny the patentability of any subject matter merely on the grounds that computers are used as part of the implementation.

#tWW: The %(q:global disadvantage) argument is misleading, since it assumes a priori either some positive effect of software patents or some asymetry in access to foreign patents.  Since patents are only valid for the territory where they are granted, in case Europe stays clear of software patents and the USA keeps granting them, European software developers and companies will have a safe home where they'll be able to grow and work without being attacked with software patents, and at their discretion they will be able to file for USA software patents and use them against their competitors there. European companies will only be vulnerable to attacks with software patents if they have businesses in the USA. On the other hand USA companies will be vulnerable at home.  This would be a competitive advantage for the EU, although both markets would use the same rules for all players, either national or foreign.

#otW: See also our previous criticism of the same statement:

#ite: Economics from a US Corporate Patent Department's Perspective

#rtW: Stripping Invention Concept from Art 52 EPC

#na7: Stripping invention concept from TRIPs

#Wdt: And %(q:programs for computers) are not inventions at all. Not being an invention means that software cannot be patented as such, although the presence of software in something else which is an invention does not render the invention unpatentable.

#tWe2: In case this long quotation is meant to suggest that computer programs must, by virtue of the TRIPs agreement, be considered as patentable subject matter, Bakels and Hugenholtz come to mind. They are not the only experts saying so, but respect for public money justifies a quote from the %(bh:study) commisioned by the JURI committee itself (page 14 and 15):

#tae: Copyright and Patents complement one another as long as they are applied to different objects. For instance a patent could apply to some peripheral hardware and copyright to its driver program.  But as soon as you try to apply copyright to hardware or patents to software, you run into contradictions.

#sWa: If we had copyright and patents for software, like in the USA, we would see the same kind of weakening of intellectual property by erosion of copyright. Writing a program should entitle the author to control who and how can copy her program, sell licenses or otherwise profit from her job. Allowing third parties to patent concepts that the program writer may have employed means that it is no longer the program writer, but the patent owner who controls distribution, copying and use of the writer's program.

#oau: Copyright is automatically enjoyed by authors, while patents are cumbersome, expensive and bureaucratic, by the way.

#tga: If a patent covers an %(q:invention), then copyright covers a %(q:creation).

#iWp: The creation includes more than the actual lines of code written by a programmer.  It covers any derived works, including modifications to the program, inclusion of parts of a program in another, linking a module of a program to another, etc.. It therefore covers the set of programs that would be generated by copying all or part of the program and modifying it or building on it.

#Wee: Copyright covers individual creation of the human intellect, such as programs, and patents cover empirical and applied R&D of the industry.  They were not meant to overlap, and the increasing importance of software in our economy does only make it more important that they don't conflict with one another.

#irn: From an earlier section it appeared that the intention was not to cover %(q:underlying programming ideas) or %(q:algorithms), so what's the need for patents? Ideas and thoughts are free.

#fha: The wording %(q:what the software does within a machine) is unclear: software does nothing inside a machine, just like a recipe book cooks nothing inside a kitchen. Software is information on how to solve a problem with a computer, and the computer is a machine that reacts to some appropiated supports of that information according to certain rules that allow the writer of the information to ensure that the computer will solve the problem. But software only defines a process, never performs it.

#iis: The clause %(q:how a machine, under software control, interacts with its environment) is more understandable. Yet since software defines that control, it defines the interaction, so copyright on the software would be enough to prohibit copying the software to the machine and therefore using it to control such interaction with the environment. If the problem is that this prohibition is not as broad as some would want, we may be returning to the debate of whether %(q:underlying programming ideas) should be ownable.

#2tc: McCarthy 2003/02/20: Coprygight protects only the actual coding

#fdo: We have refuted McCarthy's misleading statements about copyright before

#fukpid: RA (attorney) Jürgen Siepmann explains how software copyright works, cites german law and caselaw, forcefully refutes misinformation which has been spread in JURI by some MEPs and by industrial patent lawyers on whom these MEPs seem to be relying.

#vew: Objective: EU-wide incontestability of software patents (II)

#oee: First Kill, then Appoint Killer as Detective

#eyi: Prevent Drift by Imposing it (IV)

#gnn: As already said, %(q:harmonisation) or even a legitimate grab of competence (e.g. bringing patent system under control of EU parliament) is no excuse to generalise bad policy or to cease to consider public interest when passing directives.

#Wan: This desirable goal may be attained by passing the %(ja:amendments) outlined above. Patentability would not be extended beyond what EPC already specifies, patents would still be available for technical inventions (problem solutions involving controllable forces of nature), regardless of whether software is used or not in the invention, and software developers and users should not have to worry about patents as long as the only thing they develop, publish or use is software.

#ref: Arlene McCarthy on the other hand provides courts with no means of preventing a minefield of broad and trivial software patents nor any other means of preventing the plight of the US software industry from being repeated in even worse form in Europe.

#rWa: This is a proposal to first poison the patient and then appoint the the poison-mixer as the dective (or as the obituary writer).

#oar: The term %(q:review clause) is misleading.  McCarthy's directive proposal does not empower the EU to change the directive or to revoke any property rights granted under it.  It only says that European Commission (CEC) shall produce another report.  The %(se:European Commission's Industrial Property Unit), which will undoubtedly be in charge of the proposed reporting, has been acting as a mouthpiece of the European Patent Office ever since it launched the directive proposal in 1997 and earlier.  None of the European Commission's reports have ever questioned the adequacy or legality of the EPO's practise.

#eWe: The monitoring would be better carried out by either an observatory as proposed by Bakels and Hugenholtz in the %(js:study commissioned by the Juri committee) or by a Parliamentary Committee, than by the same institution that proposed the directive. Once the independence of the monitoring is beyond all doubt, it will only be useful with an adequate budget and powers of inquiry, and monitoring should probably start before the directive is enacted, so that changes caused by the directive if any can be compared with the initial situation.

#tnW2: If reporting was meant to have any effect at all, the rapporteur could have %(ol|cared to take some of the existing research literature, including EU reports such as the JURI study, into account|proposed to wait for the very comprehensive %(ft:report of the Federal Trade Commission), based on hearings involving many US companies, which is expected for later this year|proposed a sunset clause which might make the present directive easier to change later.|proposed that property rights granted under this directive can be removed later, subsequent to further review)

#slt: Certainly, it is deplorable that European democratic institutions cannot more closely control such a perturbation of the European market as drifting patentability standards can provoke. To ensure that at least the recourse to the courts exists, we should make sure that the directive reinforces EPC by clarifying the exclusion of programs for computers as per the %(ja:suggested amendments).

#2lo: List of necessary amendments:  4, 5, 9, 17, 18, 20, 22, 23, 24, 25, 26, 27, 28, 31, 32, 33, 34, 35, 39, 40, 41, 43, 45, 46, 48, 51, 52, 54, 59, 63, 65, 68, 70, 71. Cult4, Cult5, Cult6, Cult9, Cult11, Cult18, Itre1, Itre2, Itre3, Itre11, Itre14, Itre15, Itre18.  For more details on why these are needed and what other options are there, and what are the consecuences of each choice, see %(URL).

#bjn: The European Parliament can start to establish tighter control over the EPO with legislation reinforcing a clear limit for patentability, and later see how the EPO could be reformed or replaced so that the current imbalance of incentives biased towards patent inflation can be corrected.

#xrr: CALIU: About McCarthy Considerations

#sAW: Xavier Drudis Ferran debugs the McCarthy FAQ.  This is the source of the present document.

#Ino: CALIU patents info site

#tnF: introductory texts on software patents, mostly in Catalan, by Xavier Drudis Ferran et al

#WrW: McCarthy 2003/05/03: Software Patent Directive Proposal FAQ

#Wlr: The original text as distributed by MEP Arlene McCarthy, converted from MSWord to PDF.

#cas: Dr. Karl-Friedrich Lenz, professor of European Law, cites and refutes some paragraphs of the McCarthy FAQ.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amccarthy0305 ;
# txtlang: en ;
# multlin: t ;
# End: ;

