\contentsline {chapter}{\numberline {1}What is the current situation?}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Patent Inflation a Reason to Legalise Software Patents?}{6}{chapter.2}
\contentsline {chapter}{\numberline {3}Preventing Drift by Imposing It (I)}{7}{chapter.3}
\contentsline {chapter}{\numberline {4}Controlling the EPO by Following it?}{9}{chapter.4}
\contentsline {chapter}{\numberline {5}Harmonising a Uniform System, Clarifying that Confusion Rules?}{10}{chapter.5}
\contentsline {chapter}{\numberline {6}Stemming the Drift by Imposing it (II)}{11}{chapter.6}
\contentsline {chapter}{\numberline {7}Ensuring Incontestibility of Software Patents}{12}{chapter.7}
\contentsline {chapter}{\numberline {8}Authorising an already authorised Court}{13}{chapter.8}
\contentsline {chapter}{\numberline {9}Importance of Software}{14}{chapter.9}
\contentsline {chapter}{\numberline {10}The Zero-effort Myth}{15}{chapter.10}
\contentsline {chapter}{\numberline {11}Patentable Programs: derive Concepts from EPO Granting Statistics}{16}{chapter.11}
\contentsline {chapter}{\numberline {12}Software Industry Still Thriving, so No Need to Worry?}{17}{chapter.12}
\contentsline {chapter}{\numberline {13}Excluded Programs: those which are not worthwhile}{18}{chapter.13}
\contentsline {chapter}{\numberline {14}Patentable vs Non-Patentable Business Methods}{21}{chapter.14}
\contentsline {chapter}{\numberline {15}Only unclaimable objects are not patentable}{22}{chapter.15}
\contentsline {chapter}{\numberline {16}False Alternative I: EPO rules because EU not involved}{23}{chapter.16}
\contentsline {chapter}{\numberline {17}Stemming Drift by Imposing It (III)}{24}{chapter.17}
\contentsline {chapter}{\numberline {18}False Alternative II: Non-Patentability of anything that involves a computer}{25}{chapter.18}
\contentsline {chapter}{\numberline {19}Misrepresenting Art 52 EPC}{26}{chapter.19}
\contentsline {chapter}{\numberline {20}Misinformation about Art 27 TRIPS}{27}{chapter.20}
\contentsline {chapter}{\numberline {21}Belittling Copyright, Calling for Idea Protection}{29}{chapter.21}
\contentsline {chapter}{\numberline {22}Objective: EU-wide Uncontestability of software patents}{31}{chapter.22}
\contentsline {chapter}{\numberline {23}Objective: EU-wide Uncontestability of software patents}{32}{chapter.23}
\contentsline {chapter}{\numberline {24}Prevent Drift by Imposing it (IV)}{33}{chapter.24}
\contentsline {chapter}{\numberline {25}References}{34}{chapter.25}
\contentsline {chapter}{\numberline {26}Kommentierte Verweise}{37}{chapter.26}
