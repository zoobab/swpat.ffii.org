<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: US Gov't Promoting Patent Extremism in the European Parliament

#descr: The %(q:Mission of the United States of America to the European Union) in Brussels has sent a long paper %(q:by the US), titled %(q:U.S. Comments on the Draft European Parliament Amendments to the Proposed European Union Directive on the Patentability of Computer-Implemented Inventions) to numerous members of the European Parliament.  %(q:The US) warns that Europe might fall afoul of the TRIPs treaty if it passes the proposed directive as amended by the Parliament.  In particular, %(q:the US) believes that conversion between patented file formats should generally not be allowed without a license, and therefore demands deletion of Art 6a.  Moreover %(q:the US) cites the same BSA studies and the same reasoning as found in the European Commission's directive proposal, and warns that any failure to wholeheartedly endorse patentablity of software in the directive might %(q:adversely impact certain sectors of the economy), because %(q:copyright does not protect the functionality of the software, which is of significant value to the owner), and that lack of clarity in the concept of %(q:technical contribution) would lead to a continued need for negotiations with the US in WIPO and other fora.  This warning comes shortly after a similar letter to MEPs from the UK Government.  It is part of a US Government %(q:Action Plan) to %(q:promote international harmonisation of substantive patent law) in order to %(q:strengthen the rights of American intellectual property holders by making it easier to obtain international protection for their inventions).  This plan has been promoted aggressively by top officials of the US Patent Office in international fora such as WIPO, WSIS and OECD as well as through bilateral negotiations.

#orh: Cover Sheet

#cne: The %(q:Comments by the U.S.)

#lWa: Original graphical file, with Recipient data erased.

#rhh: The cover letter, signed by %(HO), is written %(q:by the US) and answered by the %(q:IPR Officer) in the %(q:Economic Section) of the %(q:U.S. Mission to the European Union), 27 Bd du Regent, B-1000 Brussels, reachable by phone 0032-2-580-2788 and Fax 0032-2-502-8117.  It says

#tmb: Please find attached a document containing the comments by the U.S. on the draft European Parliament amendments regarding the proposed European Union Directive on the patentability of computer-implemented inventions.

#lWe: We would welcome any comments you may have.   Comments should be addressed to:

#fae: Mr. Kim Gagne, IPR Officer at the U.S. Mission to the EU, Economic Affairs Section, e hyphen mail colon GagneCR at state gov, fax: +32/2/514.33.15.

#sWo: Mr. Oosterloo has the phone number +32-2-508-2788, fax +32-2-502-8117, e hyphen mail colon OosterlooHJ at state gov

#edu: It seems clear to us that Mr. Oosterloo and Mr. Gagne are transmitting papers which are not their own but written by top officials of the US Patent Office.  The US Patent Office has an official %(ap:action plan) to promote unlimited patentability %(q:in the interests of American intellectual property owners) world wide.  The details of this action plan %(q:remain confidential), but have become well known by many case examples over the years, of which this is one.  As an agency of the Department of Commerce, the USPTO is able to use the apparatus of the US Government, including the State Department and the trade representatives, for promoting its agenda.

#Wao: The document first sets the stage by defining two shorthands

#aes: ... and then begins the story:

#nWf: On August 21, 2003, United States officials met with Mr. Wim van Velzen, a member of the European Parliament, to discuss various matters of mutual interest, including the Directive and the amendments proposed by the European Parliament in the Resolution.  During the course of the discussions,  specific provisions of the Directive and specific amendments in the Resoluliton were addressed, and it was agreeed that the United States would submit comments on these provisions for consideration by the European Parliament.  We are pleased to now present those comments, which focus on the following three Articles: Article 4 and the definition of %(q:technical contribution), Article 6 and the proposed %(q:reverse engineering) exception to infringment, and Article 6(a) and the proposed %(q:interoperability) exception to infringement.  Of these, Article 6(a) is of the greatest concern to the United States because of its apparent inconsistency with international obligations and its potential for weakening patent protection in Europe.

#nda: The %(q:international obligations) are indeed at best %(q:apparent), not real, as shown below.

#tsl: Whatever its intent, Article 4 is problematic for a number of reasons.  First, it blurs the distinction between what constitutes patentable subject matter and what constitutes an inventive step. ... It is our understanding that the %(q:technicality) inquiry is already a part of the patentable subject matter test in Europe by virtue of the requirement of %(q:invention) in the European Patent Convention Article 52.  By including a %(q:technicality) inquiry in the inventive step test as well, Article 4 inappropriately combines two requirements that are intended to serve two different purposes within the patent law.

#nrr: This is correct.  It has been pointed out by %(fi:our analyses) as well.  Unfortunately the blurring of subject matter with non-obviousness is found not only in Art 4 but throughout the directive draft.

#ete: The second concern, which is related to the blurring of the patentable subject matter and inventive step requirements, is that the invention may not be considered as a whole when determining issues of patentability.

#eiW: %(q:The U.S.) then procedes to cite a U.S. court decision and explain that the technicity requirement, if applied to the invention itself and not to the scope of forbidden objects as a whole, would in ultimate consequence lead to the abolition of the patent system.  Therefore, for logical reasons, anything that runs on a computer must be considered a patentable invention and only the current U.S. criteria (novelty, non-obviousness) should be applied.  This is indeed also what the CEC/JURI directive says, but its commitment to unlimited patentability is, from the point of view of the U.S. government, not clear and consistent enough.

#ceh: More generally, we have concerns that the %(q:technical contribution) requirement will adversely impact certain sectors of the economy, such as the software industry.  According to a recent study by the Busines Software Alliance, Western Europe's packages software industry is expected to grow 14% to 109 billion euros by 2005.  Because many software companies expend significant resources in research and development, they understandably desire to maximize protection for their developments, recoup investment costs and to prevent others in the market from getting a %(q:free ride).  While copyright protection is available, it essentially protects the lines of code in the software program against copying, not the underlying process of operation.  Thus, copyright does not protect the functonality of the software, which is of significant value to the owner. Patents, however, do protect functionality and would thus allow companies to obtain the necessary protection.

#ceW: %(q:The US) is propagating conventional wisdom such as %(q:the more patents the more property, the more property the more innovation), which is in sharp contrast to consensus of all serious scholars of %(er:software economics), as expressed in numerous studies conducted in the USA and in %(dd:reports by the US Academy of Sciences).  Moreover, %(q:the US) has been ignoring the voice of its own software industry, which is, as shown by last year's %(ft:FTC hearings), characterised by %(q:continued animosity against software patents) and whose major players, including such companies as Adobe, Oracle and Autodesk, all opposed software patentability at the USPTO hearing of 1994.  The same USPTO which is ghostwriting this paper in the name of %(q:the US) today proceded to legalise program claims shortly after the 1994 hearing, thereby completely ignoring the voice of the US software industry.

#fgW: Despite the foregoing comments, we understand that the language of Article 4 is intended to codify current practice at the European Patent Office (EPO).  We believe however, that further discussions between the United States, the member states of the EU and the EPO are essential in order to continue progress in such fora as the Trilateral Patent Offices and the World Intellectual Property Organisation twoard increased cooperation and a harmonized international patent system.  We suggest that, if this provision is maintained in the Directive, the need for continued discussion of these matters be recognized.  Therefore, the monitoring and reporting required by Articles 7 and 8 of the Directive should additionally provide or monitoring and reporting of whether the technical contribution standard, both generally and as particularly defined with regard to inventive step, represents the best standard for innovation policy with respect to computer-implemented inventions particularly and other inventions generally.

#cmo: In other words: %(q:the US) appreciates that %(ol|Article 4 only codifies EPO practise, which is equivalent to the %(tp:Trialateral Standard of US, EU and Japanese Patent Offices), and that|the pseudo-restrictive rhetoric about %(q:technical contribution) was inserted by JURI only in an attempt to defuse political pressure.)  However, in order to ensure that these disturbing wordings truly remain without any effect, it would be desirable to add a monitoring provision which casts doubt on their interpretation and puts reliable comrades from the patent offices, the European Council's patent working group, WIPO and other bastions of the patent lobby in charge, so that they can then take legislative decisions in calmer places, far away from the pressures of public opinion to which a Parliament is exposed.

#rcW: Article 6

#uWl: %(q:The US) argues that the reverse engineering privilege, although factually harmless, is a limitation on patent owners' rights, and that analogy to copyright is not a valid reason for such limitations, because the situation in the copyright context entirely different.  We skip the discussion, because Art 6 is, as %(q:the US) correctly points out, without any real impact, and because %(q:the US)'s pattern of argumentation is the same as for Art 6a.

#rcW2: Article 6a

#tWW: Proposed new Article 6(a) of the Resolution excepts from patent infringement the use of a patented technique %(q:wherever the use of [that] patented technique is needed for the sole purpose of ensuring the conversion of the conventions used in two different computer systems or network [sic] so as to allow communication and exchange of data content between them[.])  The justification for this provision is that it ensures open networks and avoids abuse of dominant positions, consistent with the case law of the European Court of Justice.

#nhW: This provision is much more problematic than Article 6 for several reasons.   The scope of permissible activity is significantly broader than the specific acts permitted under the Software Copyright Directive referred to in Article 6.  It would permit infringement of patented inventions with significant commercial value based solely on some undefined %(q:need) to %(q:exchange data).  Virtually all computers exchange or are capable of exchanging data with other computers, and many require some %(q:conversion of conventions) for communication.  Thus, many data exchange methods would appear to fall under this exception.  The justification given for Article 6(a)in the Resolution indicates that it is intended as a remedy for anticompetitive activity.  Article 6(a), however, permits infringement even in the absence of the determination of anti-competitive activity.

#eeo: %(q:The US) is playing with a double meaning of the word %(q:anti-competitive): anti-competitive effects as a daily reality, as observed by economists, and anti-competitive behavior as an offence of competition rules which already exist and function as a part of a particular legal system.  Like other patent experts, %(q:the US) firmly believes that patentee's rights should never be limited by competition considerations in any systematic way, especially not by new competition rules.  Only case-by-case limitations based on tedious anti-trust procedings, using old and inoperational competition rules, should be allowed as a remote theoretical possibility, which may lead to a %(e:compulsory license) once every few decades.

#aWl: Software companies of all sizes regularly strive to control data exchange standards in order to deny competitors access to the market.  Such behavior is always anti-competitive.  It is however difficult if not entirely pointless to %(q:determine) this by means of a procedure of competition law (anti-trust law) on a case-by-case basis. The US DOJ v. Microsoft case shows how insecure and inefficient competition law is in this area.

#tof: Therefore competition considerations need to be built into a patent directive which deals with software-related problems.

#int: Overall, the scope of Article 6(a) is so broad that it would significantly undermine the rights of affected patent owners.  In addition, Article 6(a) may result in the grant of compulsory licenses without the safeguards required by TRIPs Article 31

#gau: Article 6(a) never results in the grant of compulsory licenses.  It simply says that conversion between data formats is not an infringment.  No special procedure for obtaining a license is required.  Therefore TRIPs 31 does not even apply.

#Weu: To avoid diminution of patent rights through liberization of compulsory licenses, at a minimum, TRIPs Article 31 sets forth serveral safeguards that WTO member states must comply with to ensure that the patent owner's rights are respected.  Article 6(a) provides no such safeguards.  It simply permits infringement whenever an unauthorized entity feels a %(q:need) to %(q:exchange data) between computers.

#ace: As said, TRIPs 31 does not apply here.  And, since data conversion is not an infringement, there is no %(q:unauthorised entity).  Morover, the freedom of data conversion does not only apply when some %(q:entity) %(q:feels a need) but for any data conversion, no matter whether needed or not.  %(q:The US) has been misreading the article.  It does not talk about a perceived %(q:need for data conversion) but an (objectively existing) %(q:need for using a patented technique).  %(q:The US) is engaging in empty rhetoric.

#rtp: The United States does, however, appreciate the concerns underlying this proposal, including the abuse of dominant market positions.  On this point, the United States would like to point out that patents laws are also subject to antitrust laws and to the extent there is any abuse in market power, the appropriate remedy is through the application of relevant antitrust laws.

#aof: Antitrust laws in the US have not prevented the abuse of patent-based market power so far.  As was found at the FTC hearings, there is a regular pattern of anti-competitive patent practise especially in the area of software.  Many standards have died or become off-limits for free software due to patents, and anti-trust laws were of no help.

#nbW: One of the main motivations of the FTC/DOJ exercise was that antitrust is itself too blunt and unwieldy a tool-- which is why the hearings looked more broadly at the patent system.

#fia: When it happens that a patented product is so successful that it creates its own economic market or devours a major portion of an already existing market, there may be anticompetitive undertones.  Like everyone else, the patentee is subject to these antitrust laws and when the patent owner improperly harms competition, that owner may become liable for antitrust violations.

#ofu: Currently in the US, in absence of a clear rule such as Art 6a would provide, it would seem very difficult for any patent owner to become liable for anti-competitive practises.  No such cases seem to have become known so far.

#eto: Thus, it is important to keep in mind that whle the patentee has the right to exclude others, the patentee may not use the patent to extend market power in the market place improperly.

#djf: What provides better legal security, Art 6a or a word like %(q:improperly)?

#orr: For these reasons, it is recommended that the broad exceptionto infringment in Art 6(a) be deleted.

#nWq: Thus ends the position statement of %(q:the US).

#Wni: The firm conviction of %(q:the US) that patent enforcement should never be limited by competition considerations in any systematic way is shared by the European Commission's patent lawyers.  Recital 8 of the CEC/BSA Directive Proposal says:

#ane: Patent protection allows innovators to benefit from their creativity. Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anti-competitive.

#Wel: Here too %(q:patent rights) are a priori beneficial, and any contrary evidence confined to  %(q:improper use), which is to be adressed either by moral exhortations (%(q:should not be used ...)) or by competition law -- certainly not by any new rule to be created within this directive.

#Wlt: This is not the only part of the directive and its explanatory memorandum where it becomes apparent that the European Commission's industrial property unit, the BSA and the patent establishments of UK and US belong to a common transatlantic network of patent legislators.

#eat: Biography of the signator of the letter to the MEPs.  Oosterloo is the %(q:economic specialist), in charge of %(q:European Parliamentary Afairs).  However he may be a transmitter only, whereas the letter is written %(q:by the US) and answered by Mr. Kim Gagne.)

#ofw: USPTO Action Plan 2003: Pursuit of Substantive Patent Law Harmonisation

#pen: Action: Promote harmonization in the framework of the World Intellectual Property Organization and its Standing Committee on the Law of Patents; resolve major issues in a broader context and pursue substantive harmonization goals that will strengthen the rights of American intellectual property holders by making it easier to obtain international protection for their invention and creations.  Details:  The details of this action paper are by their nature sensitive and confidential, and therefore not appropriate for publication.

#eWU: Journalist David Streitfeld reports about abusive extensions of the patent system in the US and quotes USPTO president as saying that the USPTO is an %(q:agency in crisis). %(orig|Two former heads of the patent office have described the agency as sitting %(q:on the edge of an abyss.)|James E. Rogan, the former Republican congressman from Glendale who became director of the patent office in December 2001, agrees with all but the most strident critics.|%(q:This is an agency in crisis, and it's going to get worse if we don't change our dynamic,) Rogan said. %(q:It doesn't do me any good to pretend there's not a problem when there is.)|Beyond the plight of an antiquated government bureaucracy overseeing a field that is undergoing explosive growth, there are deeper questions about the fundame ntal role of patents. ...|There's a point where patents impede innovation. It can cost more to check whether a software program infringes on previously patented programs than it cost to write the program in the first place.)

#ira: The original fax document with recipient data blackened.

#Wsi: Note: Please to not link to this PDF file.  Its may be moved to another name/location soon.  Links should go to the commenting web page only, or to a copy of the PDF file which you are free to place on your site.

#Wae: At WIPO the US government, represented by its patent office, is obstructing all discussions about the efficiency of the patent system in promoting innovation.  In 2003/08 the US representative asked that a WIPO movement on implications of opensource software for the IPR system should be cancelled, because %(q:it is WIPO's job to uphold and extend property rights, not to restrict or waive them).  See also the ensuing discussion.

#let: At OECD in Paris 2003/08/28, the US government, representat by patent officials, asked the scientists and politicians assembled at the conference to refrain from discussing %(q:so-called problems of the patent system).  The US representative said: patents are basically good, the system has worked for 200 years, and any attempt to discuss its so-called problems will only be taken as a pretext by some third-world-ideologues to stir up artificial problems.  In contrast, the EPO representatives at the conference seemed very intelligent and enlightened, although (or because) they were not speaking in the name of any government.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: ccorazza ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: usrep0309 ;
# txtlang: en ;
# multlin: t ;
# End: ;

