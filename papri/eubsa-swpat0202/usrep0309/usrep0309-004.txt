  26-SEP-2003 16. 41  FROM; UIEU/ ECON 0251281 17                  TO. 022849153         P; 4/7

         _. urpo__ e _I wi CII i II IIIe p I I I.tl I-  l. I' w.

         Th,e_ second conc e rn,  whi cII i 'I t-e I III ed r-n Ihe b lurt-ing ol t he
         PII t entabl e I' ubj e ct milt te t- Ind i)IvIIn C -i. ve s t eF regui remen I I,
         j_ I I ha I- IhP i_ IIvinl. i. ol II? n..II L'. I. Ie conl idet-ed.I I_ a vho _.. e I_e_
         d_e t_P_ I_ I _I_.I.I I- I I U.e I o I patentabi l itv.  The ex_er i. en ce ol _he
         Uni ted _S tates in t.he cas e o I Diamond v. D i.eh..r..,  g _ o u. _.  _ _ _
          ( I 9 B I),  mly he Ip to i l lus Ira Ie Ihp. po int.  In Di ehr,  the
         I _'Ve_t I O_ WI.8 I. r.. ubbe.r moldin_ g roce I I that u I_ ed a
         _rogr__Tned di I. it Il comput er t o c I I' cut Ite a ma Ih Imlt i _al
         fOP_Ul _ to i7npro_e curiII_ o I the rubber.  T.. _ _Id bee_
         I Ig I ed 1_--h I L  I. tIVInl I. _n VI _.i IIn_a I.e.n. L II I e I I C Ius e a l l ol the
         plrt I o I the rubber - molding machine vere known,  Ind Ihe
         __ I_ _ " IIeW " __ It Wil5 _ mere Tn_thelilt i C I l I IIIU ], I ol
         I. Igo I-i t.hm,  wl,.i c.)I II n.d I I' U - S.  pa I_. I n I. I I v  ( I nl undp=r European
         pnt ent l IM 3  i I. not pat.eII t. II l e?-: ub j e rt mlt. t,er.  The U. S.
         Sup reIIe Courl I-e ._ ect ed thi s tvpe o I " p. i ecemeal " IpproiIch to
         p. It I-nt abi 1 i t _,  _' I. Iogtl i z i tlg I.)I I. t-  " i f c. a Ir. i el I'.o i I a ext I'eme.
          I, I_uch an Ina IvI_ i s Irl IIl d. I  Itta.II.I I l l. i.n'Ie=n L i on I unpate It ab l e
         bec I u Gc a I I  invent ionl c In be reduc ed to under l_in_.

         .p. r i nc i p l e G cl f _I Itu. L'e vhi ch,  onc e IIown,  mlke the
         i mp lement I t i rlrl r-I.?I v i rl II I. ._  Ar I. i c'. l? q,  IovFvpt-.  I r= FmG I. I.
         cun I. Imp l I I-e Itle __ Ime tvpe o f Ina l vs i I rl.�uli ate d by t he
         CIu I- t  i II I i I.= t_I )_- tle I'- Iu s e i i de I i III I_ p. It ent _Ih i l i t v sn l e l v in
         I Irm__ o I t he  i vEIe rl.I I-..r.II r.. I_ i IIu I i nn IIIde bv.  I piI t- c I I Ihe
         I. I_J17t.I I- on,  i t- I II-_. ec t iv I u.I  L_.h II C II I YI C. Iet._ I f I I-Ie- i nvenl i In

         iII a wh.o I C o I I-IIe Ihel- the di I I e I-enc I �. Ie twle.n tIIe in_Ien C ._L In

         iII' I IIIole anl I It.e � I_ I, 07 itt_t wou Id hlve been Ib'Ii ous t o t he
         p I IIon Iki l l ed i n the a I. 'r..

         More gene.ra. J I. _,  ve have concernl that the " te chnical
         cont r�Iut ion "  .I-'I-r3u i. rement wi l l Idverl e l_ impact cert ain
         p_ec tor6 nf the economy,  such I I Ihe Bo I I_a re i.ndIII Ir. y.                  .
         Iccordi ng Io I  Iecent I_ tud_ bv t he Bu I ine I- s S o I tware
         A.I.'l. i. IIce,  Me s t ern Eu.rIpc ' 6 p Ichaged I_o I twlre induI_t ry i s
         e= Ipe c I cl to grlw l 4 I t. I l O 9 I;. I. I i IIn. euros bI_ � O O 5.
         Icclu I_ e mIII_ Ir.I f I.wlre compani e I e_pend I igni I i c a.n_L
         Ic I_ ource s in I'II_. I' II I. I-c II IIIId deve l _Ipmlnt,  thev unde r Itandah Iy
         d,I, I i re t I mI_imi r. I _I t.'Ir. pr_r i III f n I t h p i r I plrp l n_mpnt -I,
         r. e CoII,p i IIve I tment col t. I I. I.. d  r.-o p reveII olher I i n the marke t
         I rom _e I I- i ng I  " I t-ee ri dp-. "  Whi l I coE>_r.i _h I. p In Ie c I i. on i I
         IviI i Iab le,  i I c__. I. I IIII I- i- Il l-_ prot ec t I Ihe l i ne I o I code i n t he
         Io I Iwa re pt-Igt-am Iglirts I. cnpy � rl3,  tl.o r- Ihe un.Ie t- Iying
         P.I-oC'e I.S E-.I f _FI rl-I i i on.  Thus,  r='rlp _.I_ -i. g II. I. I.I?I I rl c3 I. p I.'cl I I C C. I h e-
         fuIII t i ona I i t, _ c_I 7  I. IIe I I I' I'Ia re,  whi Ih i s n I I_ i g. ni I i c ant
         I__ l ue I I t IIe III'r.) II I_.   Ii3 I- itl I I_,  t-Icveve r,  dn p t-I I er-. t
         _I u nc t,..i otl I l i t V-  IIId IoIIl I t-_ II.u I I I.. I. CIv I'-Imp I I-1;. e I  r..n obl II i n the
         _ece I.I I.I.__ _I _O C e C I 10n.

         Ic Ip it e the fII I-I=31 ing coltlment I_.  ve underl t and t hat t.he
         Iilnguage o I hl'I i. r.' J..e 4 i s intended t I codi I_ current
         p I'Icl ice It t_he _. u,r.'opean I_I I enl II I ice  ( EPO _.  We be l i eve,
         howeve I J  thlt fu _.rl)ter di _I cus I ioIII bet.weel t.he United
