\begin{subdocument}{eubsa-post}{The Proposal}{http://swpat.ffii.org/papiers/eubsa-swpat0202/post/index.fr.html}{Groupes de travail\\swpatag@ffii.org\\version fran\c{c}aise 2002/07/25 par Europe-Shareware.org\footnote{http://www.europe-shareware.org/}}{Apr\`{e}s-Proposition Propagandisante, BSA seulement}
\begin{quote}
{\it 1. Taking account of the principle of subsidiarity, why is Community legislation necessary in this area and what are its main aims?}

{\it Harmonisation of the relevant parts of national patent laws can only be put into effect by Community action. The administrative practices and the case law of the Member States have been differing for many years.  While there has been some convergence between the practices of the EPO Boards of Appeal and the German Federal Court of Justice, there is no indication that practices would converge across the European Community without legislative action being taken.}

{\it The impact on business}

{\it 2. Who will be affected by the proposal?}

{\it Which sectors of business?}

{\it First, it is the software industry which should be able to derive advantage from increased legal certainty in relation to patents for software-implemented inventions and, relying on such patents, be provided with an incentive to increase investment and innovation. The clarification that entities involving no technical contribution (such as ``pure'' business methods) cannot be monopolised should also encourage innovative developments in this sphere. These factors should bring about a positive impact also upstream, that is on suppliers of materials and on manufacturing and marketing services. Further, it should be to the benefit of the downstream side, namely for distribution, training and support services.}

{\it Second, increased software innovation is likely to enhance productivity, capability and competitiveness in virtually all sectors of business. IT, communications technology, and software, are the keys to improved European competitiveness They have helped making European business restructuring in the 1990s possible which was triggered by global competition and have brought about great productivity gains and enhanced employees' possibilities to communicate.}

{\it The above contributions to the economies of Western Europe on which software patents should have a positive impact have been identified in the context of the packaged software industry in a study commissioned by the Business Software Alliance.}

{\it Software innovation must continuously be harnessed in order to keep European enterprises competitive world-wide.}

{\it Which sizes of business (what is the concentration of small and medium-sized firms)?}

{\it All sizes of business can take advantage of the proposal because protection of computer-implemented inventions by patents is open to all of them. However, the proposal should be of particular benefit to small and medium-sized companies who play a major and rapidly increasing role in software innovation. They can strengthen their economic position by protecting the ideas and principles underlying their computer-implemented inventions (which cannot be protected by copyright) against appropriation by others. In the past, the software industry has lived largely without patents. However this will have made it easier for major players to take ideas, especially of SMEs, and market them without recompense to the originators. Further, while larger firms may be in a better position to accumulate patent portfolios and thereby bargain for cross-licensing, small firms may in practice find that they have few weapons other than patents to protect their inventions, and so be relatively more dependent upon them. Patents can also be crucial for start-ups in the software field to obtain venture capital. They can make it easier for SMEs to successfully participate in calls for tenders, facilitate their going public, and increase their value in take-over situations.}

{\it Many SMEs, however, are either unaware of the patentability of computer-implemented inventions or, in the alternative, are concerned about the potential effects of patents for such inventions. Member States will need to evaluate whether the specific situation of patents in this field requires specific educational initiatives to be undertaken, in particular by their patent offices.}

{\it With the above in mind, the Commission engaged a contractor to conduct a study on software patent awareness of SMEs including possible actions to improve this awareness. In the framework of this study, the contractor has produced a brochure for information of SMEs.}

{\it As regards the impact on the open source community, who have raised concerns against the patentability of computer-implemented inventions, many of the negative comments from individuals and small companies are directed against those patents for computer-implemented inventions that would affect the dissemination (``publication'') and use of programs running on a general-purpose computer. The alternative harmonisation proposal made by EuroLinux\footnote{http://swpat.ffii.org/analyse/javni/index.de.html} is not inconsistent with the grant of patents on ``traditional inventions which include a computer program, for example in the case of the chemical or mechanical industry''. However, there are a substantial number of features provided by European patent laws of which the open source community can take advantage including
\begin{itemize}
\item
prior use rights allowing, under certain conditions, an inventor to continue to use his/her invention despite the fact that somebody else subsequently patented it;

\item
publication or public use of an invention preventing subsequent patent protection for that invention by a third party;

\item
the definition of patent infringement: a program infringes only if fulfils a certain patented function in the way defined in the patent claim;

\item
the opposition procedure: detailed procedures vary, but all patent offices (including the EPO) offer the possibility of challenging the validity of a patent in formal proceedings and/or of filing observations relating to patentability before a patent is granted. In addition, granted patents may be challenged before national courts;

\item
cross-licensing by which owners of two or more patents grant each other licenses; in certain circumstances compulsory licenses may be obtainable where a patent cannot be exploited without infringing an earlier one.
\end{itemize}}

{\it Are there particular geographical areas of the Community where the businesses are found?}

{\it Given the limited needs for technological equipment for much software development on one hand and the global communication and networking facilities provided by the Internet on the other, the geographical location, in many cases, is of secondary importance.}

{\it 3. What will business have to do to comply with the proposal?}

{\it Increased legal certainty should provide businesses with an incentive to make wider use of patents for computer-implemented inventions. It is however up to them to judge whether a computer-implemented invention is of sufficient economic relevance to warrant initiating the patenting procedure. As businesses increase their use of patents for computer-implemented inventions, they will also have to monitor their competitors' patents in order to detect and avoid possible infringements. On the other hand, from such monitoring, businesses will be able to derive important information about new inventions and possibly also their competitors' business strategies.}

{\it 4. What economic effects is the proposal likely to have?}

{\it On employment?}

{\it The software industry makes a significant contribution to CEC economies and creates a substantial and constantly growing number of highly skilled jobs in the software industry itself as well as upstream and downstream.}

{\it The study commissioned by the Business Software Alliance quoted above estimates that the packaged software industry generated \$ 37.0 B in sales and created 334,181 jobs in Western Europe in 1996. Assuming an annual rate of market growth of 10 \percent{} and a concurrent rise in employment of only 5 \percent{}, this would allow to expect a further 92,283 jobs by the end of the period from 1996-2001, that is total employment of 426,464 and an overall market of \$ 59.8 B by 2001. Direct employment by packaged software publishers in Western Europe amounted to 45,388 in 1996.  Estimations for upstream employment were 81,016 and for downstream employment 207,777. These estimates were conservative. A study by Datamonitor concluded that the number of packaged software workers in Western European countries will grow by between 24\percent{} and 71\percent{} from 1999 to 2003, with an average of 47\percent{}. A further conclusion is that each packaged software job creates 2-4 jobs in the downstream economy and 1 job in the upstream economy.}

{\it It is not possible to forecast with any certainty the employment growth that could result from this proposal. Nevertheless, the ambiguity of the present legal situation as well as the divergent case law and administrative practices addressed by this proposal have a detrimental effect on innovation. These conditions also tend to have a proportionally greater effect on smaller enterprises which may lack resources to pay for extensive legal advice. Currently, around 75 \percent{} of software patents in Europe are held by very large, often non-European companies. European companies, and in particular SMEs, may not derive full advantage from their computer-implemented inventions due to lack of knowledge about the legal possibilities and advantages of patenting, and, thus, cannot garner the maximum turnover and profits which in turn could create new jobs.}

{\it The present proposal will create an environment of greater legal certainty in which innovation is fostered and will therefore help to create employment.}

{\it On investment and the creation of new businesses?}

{\it Although there still is relatively low use by European independent software developers of patents in raising finance or in licensing, an increasing number of small firms in the European software business, and in particular start-ups, find patents to be a crucial part of their business strategy because they are essential to attracting venture capital to develop and market computer-implemented inventions, and/or to license competitors and/or to sell or license an innovation to a major firm. Many venture capitalists will not normally support new companies based upon new software products unless adequate protection, in particular by patents, is available. A substantial number of companies would not exist had it been impossible to obtain patents protecting their software innovations.}

{\it On the competitive position of businesses?}

{\it Internally (within the CEC), by stimulating competition through facilitating entry in the market by small innovative firms, European small independent software developers will be able to compete more effectively with big players.}

{\it The existence of an effective anti-trust regime provides an important safeguard to deal with abuses which might arise, for example if patented technology should form the basis of a standard (e.g. an interface or file format). In the future, the importance of de facto proprietary standards may decrease while electronic business customers increasingly demand open standards for interoperability across disparate platforms on the Internet. On the other hand, applications built on these platforms might, to a large extent, remain proprietary. To the extent that proprietary standards remain in place, other industries, such as the electronics industry, have shown that voluntary agreements such as patent pools or patent platforms can be adequate tools for managing complex essential patent portfolios owned by many different firms which are necessary for creating complex products and services.}

{\it Internationally, the proposal should improve the competitive position of European software businesses in the competition with our global trading partners, the U.S. and Japan where software patents are widely granted.}

{\it 5. Does the proposal contain measures to take account of the specific situation of small and medium-sized firms (reduced or different requirements etc)?}

{\it Given the nature and scope of the proposal, it is not feasible to include explicit measures involving differential treatment of SME's. Nevertheless, such entities should benefit particularly from the increased legal certainty which will result from the implementation of the Directive (see above, at the end of section 2 and at section 4 (likely economic effects on investment and the creation of new businesses)).}

{\it Consultation}

{\it 6. List the organisations which have been consulted about the proposal and outline their main views}

{\it The proposal itself has not been circulated to interested parties since the Commission still has to adopt it. However, the need for a Commission initiative in this field has been identified in a consultation process which the Commission started in 1997 with the Green Paper on the Community patent and the patent system in Europe. The European Parliament and the Economic and Social Committee have both supported the patentability of inventions involving computer programs. Moreover, the interested circles had strongly urged legislative action in conferences organised by the Luxembourg and U.K. Presidencies in co-operation with the Commission. These conferences were held in Luxembourg on 25-26 November 1997 and in London on 23 March 1998. In a follow-up communication to the Green Paper, the Commission took stock of the consultation process and stated that the patentability of computer programs was one of the priority issues identified during this process on which the Commission should rapidly submit a proposal. Organisations representing European businesses, namely UNICE and EICTA, continued to ask the Commission to take a legislative initiative on the issue. UNICE, for instance, in February 2000, renewed its call for swift action to remove ambiguity and legal uncertainty which surrounds the patentability of computer-implemented inventions. If rapid action were not undertaken, the respective market segment would be dominated by Europe's main trading partners, in particular Japan and the U.S. where there were fewer restrictions on patenting inventions relating to or relying on software.}

{\it The Commission had also distributed a questionnaire on the main points that should be dealt with in the Directive. The answers received in 1999 have been taken into account in the present proposal.}

{\it The services of the Commission organised a meeting with representatives of the open source community, namely a delegation of EuroLinux representatives, on 15 October 1999 in Brussels. On 18 November 1999, the Committee of the Regions gave its opinion on the issue. Both EuroLinux and the Committee have expressed concerns that software patents might impede the progress of innovation in the software field. These concerns have been taken into consideration in this proposal.}

{\it The Commission launched an independent study on the scope of harmonisation in the light of the recent developments in the United States. While the consultation on the Green Paper had clearly shown the need to harmonise and clarify the current legal situation, the purpose of the study on the economic impact of the patentability of computer-implemented inventions was to provide assistance in determining how extensive harmonisation should be. For this purpose, the study assessed the main consequences for innovation and competition, in particular for SMEs, of extending patent protection beyond current levels. The results of the study, as well as of other pertinent economic studies, have been taken into account in this proposal.}

{\it Finally, the Commission conducted a consultation between October and December 2000 on the basis of a paper which was communicated to the Member States and made generally available in the Internet. This paper sought views on whether there was any need at all for action at the Community level, and in the event this question were to be answered in the affirmative, what the appropriate level would be. The paper then proceeded to set out in some detail the current state of the case law on patentability of software-implemented inventions as established within the EPO, and suggested on the basis of this a number of very specific elements which might figure in any harmonisation exercise based on this status quo. 1447 separate responses were received, which were analysed by a contractor and summarised in a report which has also been published. While opposition to patents relating to software was expressed by a large majority of the individual responses to the consultation, the collective responses on behalf of the regional and sectoral bodies, representing companies of all sizes across the whole of European industry, were unanimous in expressing support for rapid action by the Commission more or less along the lines suggested in the discussion paper.}
\end{quote}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

