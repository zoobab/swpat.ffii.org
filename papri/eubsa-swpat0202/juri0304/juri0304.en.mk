# -*- mode: makefile -*-

juri0304.en.lstex:
	lstex juri0304.en | sort -u > juri0304.en.lstex
juri0304.en.mk:	juri0304.en.lstex
	vcat /ul/prg/RC/texmake > juri0304.en.mk


juri0304.en.dvi:	juri0304.en.mk
	rm -f juri0304.en.lta
	if latex juri0304.en;then test -f juri0304.en.lta && latex juri0304.en;while tail -n 20 juri0304.en.log | grep -w references && latex juri0304.en;do eval;done;fi
	if test -r juri0304.en.idx;then makeindex juri0304.en && latex juri0304.en;fi

juri0304.en.pdf:	juri0304.en.ps
	if grep -w '\(CJK\|epsfig\)' juri0304.en.tex;then ps2pdf juri0304.en.ps;else rm -f juri0304.en.lta;if pdflatex juri0304.en;then test -f juri0304.en.lta && pdflatex juri0304.en;while tail -n 20 juri0304.en.log | grep -w references && pdflatex juri0304.en;do eval;done;fi;fi
	if test -r juri0304.en.idx;then makeindex juri0304.en && pdflatex juri0304.en;fi
juri0304.en.tty:	juri0304.en.dvi
	dvi2tty -q juri0304.en > juri0304.en.tty
juri0304.en.ps:	juri0304.en.dvi	
	dvips  juri0304.en
juri0304.en.001:	juri0304.en.dvi
	rm -f juri0304.en.[0-9][0-9][0-9]
	dvips -i -S 1  juri0304.en
juri0304.en.pbm:	juri0304.en.ps
	pstopbm juri0304.en.ps
juri0304.en.gif:	juri0304.en.ps
	pstogif juri0304.en.ps
juri0304.en.eps:	juri0304.en.dvi
	dvips -E -f juri0304.en > juri0304.en.eps
juri0304.en-01.g3n:	juri0304.en.ps
	ps2fax n juri0304.en.ps
juri0304.en-01.g3f:	juri0304.en.ps
	ps2fax f juri0304.en.ps
juri0304.en.ps.gz:	juri0304.en.ps
	gzip < juri0304.en.ps > juri0304.en.ps.gz
juri0304.en.ps.gz.uue:	juri0304.en.ps.gz
	uuencode juri0304.en.ps.gz juri0304.en.ps.gz > juri0304.en.ps.gz.uue
juri0304.en.faxsnd:	juri0304.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo juri0304.en-??.g3n`;source faxsnd main
juri0304.en_tex.ps:	
	cat juri0304.en.tex | splitlong | coco | m2ps > juri0304.en_tex.ps
juri0304.en_tex.ps.gz:	juri0304.en_tex.ps
	gzip < juri0304.en_tex.ps > juri0304.en_tex.ps.gz
juri0304.en-01.pgm:
	cat juri0304.en.ps | gs -q -sDEVICE=pgm -sOutputFile=juri0304.en-%02d.pgm -
juri0304.en/juri0304.en.html:	juri0304.en.dvi
	rm -fR juri0304.en;latex2html juri0304.en.tex
xview:	juri0304.en.dvi
	xdvi -s 8 juri0304.en &
tview:	juri0304.en.tty
	browse juri0304.en.tty 
gview:	juri0304.en.ps
	ghostview  juri0304.en.ps &
print:	juri0304.en.ps
	lpr -s -h juri0304.en.ps 
sprint:	juri0304.en.001
	for F in juri0304.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	juri0304.en.faxsnd
	faxsndjob view juri0304.en &
fsend:	juri0304.en.faxsnd
	faxsndjob jobs juri0304.en
viewgif:	juri0304.en.gif
	xv juri0304.en.gif &
viewpbm:	juri0304.en.pbm
	xv juri0304.en-??.pbm &
vieweps:	juri0304.en.eps
	ghostview juri0304.en.eps &	
clean:	juri0304.en.ps
	rm -f  juri0304.en-*.tex juri0304.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} juri0304.en-??.* juri0304.en_tex.* juri0304.en*~
juri0304.en.tgz:	clean
	set +f;LSFILES=`cat juri0304.en.ls???`;FILES=`ls juri0304.en.* $$LSFILES | sort -u`;tar czvf juri0304.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
