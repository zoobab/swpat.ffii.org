<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: JURI 2003/04-6 Amendments: Real and Fake Limits on Patentability

#descr: Members of the European Parliament's Commission on Legal Affairs and the Internal Market (JURI) submitted amendments to the European Commission's software patent directive proposal. While some MEPs are asking to bring the directive in line with Art 52 EPC so as to clearly restate that programs for computers are not patentable inventions, another group of MEPs is endorsing the EPO's recent practice of unlimited patentability, shrouded in more or less euphemistic wordings. Among the latter, some propose to make programs directly claimable, so as to ensure that software patents are not only granted but achieve maximal blocking effects.  This latter group obtained a 2/3 majority, with some exceptions.  We document in tabular form what was at stake, what various parties recommended, and what JURI finally voted for on 2003/06/17.

#Ces: Tabular Comparison of Amendment Proposals, Voting Recommendations and Results

#WWW: The EU Parliament's legal affairs committee (JURI) is due to meet on June 10 or 17 to vote on amendments to the software patents directive.

#osh: As well as amendments submitted in a draft report by the committee's rapporteur, Arlene McCarthy, the committee will also be considering further amendments submitted by its own members, and must also consider the amendments recommended by two other committees, the Industry committee (ITRE) and the Culture committee (CULT).

#aue: A consolidated analysis of all of the amendments can be found %(rc:here) for the recitals, and %(ar:here) for the articles.

#iet: The JURI decision is important, as its amended report will then be submitted to the plenary for voting.

#hah: What should be patentable and what should not?

#SsW: Four Separate Tests on one Unified Invention: Preserving the Logic of Art 52 EPC

#oma2: Freedom of Publication vs Program Claims

#tel2: Real vs Fake Interoperability Privilege

#tWu: Other Issues

#Wrl: According to Article 52 of the European Patent Convention (1973), %(q:mathematical methods; presentation of information; schemes, rules and methods for performing mental acts, playing games or doing business and programs for computers) etc are not regarded as inventions in the sense of patent law and are excluded from patentability, provided that the patent application is directed to these objects as such (and not a combination invention, e.g. a chemical process whose input parameters are calculated according to a mathematical formula).

#Wso: The EPO's Examination Guidelines of 1978 further elaborate on this:

#wWo: A computer program may take various forms, e.g. an algorithm, a flow-chart or a series of coded instructions which can be recorded on a tape or other machine-readable record-medium, and can be regarded as a particular case of either a mathematical method (see above) or a presentation or information (see below).  If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims.  For example, a claim to a computer characterised by having the particular program stored in its memory or to a process for operating a computer under control of the program would be as objectionable as a claim to the program %(e:per se) or the program when recorded on magnetic tape.

#nif: Until 1986 the EPO's boards of appeal and national courts, except in the UK, firmly rejected any claims to new combinations of generic computing equipment with calculation rules (= computer programs) in whatever form.   After a few years the original hurdles to patentability were eroded to a level where %(kb:about 99% of all applications for pure data processing, including %(q:computer-implemented business methods), were demed %(q:technical) and %(q:not directed to programs for computers as such) by the EPO).  National courts in various countries did not follow the EPO's practise and revoked patents some of the patents granted under, saying that the alleged invention consisted in nothing more than a computer program, intellectual method or business method.

#nra: See K. Beresford in %(q:European patens for software, E-commerce and business model inventions), World Pat. Info 23 (2001) p. 253

#sWW: The Commission of the European Communities (CEC) and MEP Arlene McCarthy are proposing to make the EPO's rules obligatory for all of Europe.

#tfa: Despite statements like %(q:the EPO... facing well-drafted applications and the need to offer applicants the benefit of the doubt, are contributing to a slow but discernible drift towards wider patenting) (Arlene McCarthy 21.05.03), the CEC draft directive and McCarthy's amendments steadfastly refuse to take a stand on the critical question of what is %(q:technical) (i.e. patentable) and what is not: it must all be left to caselaw.

#WlW: The thrust of McCarthy's efforts is to ensure that the software and business method patents, which the European Patent Office (EPO) has granted against the letter and spirit of the written law in recent years, shall neither be revoked nor become unenforceable.  Therefore, effectively, everything is to be patentable.

#edt: %(am46:Amendment 46) to Article 3 attempts a negative definition of %(q:technical), similar to what is found in Art 52 EPC: %(q:... data processing should not be a field of technology for the purposes of patent law ...).

#ssi: This is essential, to reformulate the principles of Art 52(2) in the language of the international treaty Art 27 TRIPs and thereby ward off recent attempts to abuse Art 27 TRIPs as a pretext for extending patentability beyond the limits of Art 52 EPC. It it the single most important principle that needs clarification.

#tec: Further clarification can be achieved by a positive definition of %(q:technical invention), as adopted by CULT and proposed by members of the JURI committee, e.g. %(q:a new teaching about cause-effect-relations in the use of controllable forces of nature) or %(q:problem solution involving controllable forces of nature).

#cdf: Arlene McCarthy's %(dr:draft report) dismisses the %(q:technical invention) concept as an outdated peculiarity of German jurisprudence, but overlooks that the same concept is also more or less present in EPO caselaw and in many national jurisdictions (the UK being a notable exception) . %(q:Modernist) patent law scholars have however ever since the 60s opposed the limitation of patentability to the use of forces of nature, arguing that the mission of the patent system is to %(q:protect) today's %(q:high-tech) innovation, or even that %(q:information has become the fifth force of nature).  It should however be understood that:

#gei: Art 52 EPC does not mention the word %(q:technical).  It is CEC/McCarthy themselves who insist that the limit between patentable and non-patentable %(q:computer-implemented inventions) is to be drawn along the line of what is %(q:technical).  The burden of defining the term %(q:technical) in a meaningful way is on CEC/McCarthy, not on their critics.

#son: The definition by reference to %(q:controllable forces of nature) is based on epistemological concepts which are widely recognized as valid outside the patent law community.  Recent trials on a testbed of EPO patent applications have shown that this concept allows highly predictable decisions on what is patentable and what not.  It moreover is the only concept which allows clear delimitations between the spheres of patents and copyright.   This is so today as much as in 1976, when the concept reached its classical form of expression in the %(dp:Dispositionsprogramm Decision).

#cWW: Modern %(q:high-tech) innovators know very well why they like to solve problems at the level of software wherever that is possible: because software innovation is easy.  The Universal Computer is a mathematical model which allows direct application of logics without having to control quirky forces of nature through tedious experimentation.  It therefore results in very complex products and a relation of one product to many patents.  Some corporate patent departments may wish to transpose past success experience into the new world of %(q:high-tech) by extending patentability.  They want to have the best of both worlds: light-weight innovation and heavy-weight monopolies.  Their desire is understandable, but by no means %(q:modern).  Copyright is sufficient and well-adapted for the world of logical creations.  %(de:Copyright is modern).

#uvW: As the study of the European Commission Enterprise Directorate on software patents wrote:  We are living in an age of intellectual property, but not in an age of patents.

#ntW: This concern is reflected in %(am45:Amendment 45) (Article 2 (ba)), and several others.

#h4a: The debate thus centres on Articles %(ART); and is reflected in recitals %(REC).

#hsi: The CEC proposal suggests that any idea expressed in the terms of generic computing equipment is a %(q:computer-implemented invention) and that thereby %(q:belongs to a field of technology).

#sjg: In contrast, according to Art 52 EPC, the presence of a technical invention cannot be taken for granted but must be assessed.  Certain items, such as algorithms, business methods and computer programs, do not qualify as technical inventions.  Moreover, according to Art 52 EPC four distinct requirements for patentability must be fulfilled by the same invention, not by different parts of a claimed object.  An invention is a teaching (1) about statutory subject matter which must be (2) new, (3) non-obvious and (4) susceptible of industrial application.  Obviously it is not enough to have something new (e.g. a new icon), something else non-obvious (use of the icon in a drawing showing how to use a washing machine), something else with industrial application (washing clothes) and something else within statutory subject matter (a washing machine) to claim the combination (a known washing machine with a clever drawing showing by means of new icons how to use it to wash clothes). The requirements must all apply to the same teaching (invention).

#cog: The technical contribution is the invention, and it must pass the four tests: invention character (statutory subject matter), novelty, non-obviousness (= inventive step) and industrial applicability. The claimed object may (and usually will) include other features which do not pass the tests, along with the invention itself, but this should not be taken as a excuse to claim objects which have nothing in them that passes all the four tests, only different things, each passing different tests. Otherwise, any program for computers could be claimed by claiming any computer (technical but not new) with the program (non-technical but new).

#hoE: The CEC proposal eliminates one of the 4 tests, that of %(q:technical invention), and instead says that this test should be part of the non-obviousness test.  This is not a harmonisation but a change to the system of Art 52 EPC and Art 27 TRIPs.  Many of the amendment proposals commit the same type of error.  It is essential to identify one object in a patent application, and apply four different filters to it.  Any provision which says that %(q:the claim as a whole) needs to be tested or which makes one filter dependent on another weakens the patentability requirements of the EPC.  This is also true of claims such as %(ahi:amITRE-6:ITRE-6), which mixes various tests in an attempt to sound strict, but in doing so actually loosens the patentability criteria.

#vWi: By mixing %(q:technical contribution) and %(q:inventive step), the CEC proposal creates systematic obscurity and legal insecurity.  It opens an infinite ocean of interpretation for EPO and prevents most national patent offices (who do not examine non-obviousness) from rejecting patents for non-inventions.  Due to this proposal, the European Commission itself had to admit in its %(ec:FAQ) about the directive proposal that it is unable to say whether %(q:Amazon One Click Shopping) is patentable subject matter or not.  Everything is made dependent on the EPO's %(q:inventive step) analysis.

#2es: Amendments %(ahi:am62:62), %(ahi:am64:64) and %(ahi:COMP-2:COMP-1) (Article 5.2) call for %(e:program claims), i.e. claims to

#aiW: a computer program, characterised by that upon loading it into computer memory [ some computing process ] is executed.

#pis: This would make publication of many programs a direct patent infringement, thus creating additional risks of litigation for programmers and Internet service providers.

#tPt: Program claims also lead the patent system ad absurdum.  Taken to the extreme of consistency, they mean that patent descriptions are patented: by publishing the full disclosure (which in case of software should contain some sample source code), the patent office would infringe on the program claim.  This illustrates nicely why software cannot be patentable: patents are supposed to be deals between the patentee and the public, where information is disclosed and matter monopolised.

#tse: The %(q:consumer-protection) argument in favor of program claims is groundless and cynical.  It is always possible for the end user of commercial software to hold the distributor liable by means of standard contract clauses, and this is what would usually happen, if software is patentable without program claims.

#pWt: The real reason why industrial patent lawyers are pushing for program claims seems to be a symbolic one:  the existence of program claims brutally and indecently documents that the border to patenting software as such has been crossed.  Conversely, by refusing to grant program claims, the European Commission can appear as moderate.

#sit: However, in order to protect the freedom of publication, as guaranteed by Art 10 ECHR, the mere refusal of an indecent claim form is not enough.  A more explicit affirmation of the freedom of publication, such as ITRE-13, is needed.  And ultimately, in another context, it must be made clear that information objects in whatever form are not patentable.

#rsl: Often in computer software, the technical merits of a particular program are less important than how many other users it has, who can all interact -- the network effect.

#tga: A great concern is that by creating a dominant proprietary standard, one software house may %(q:lock in) the whole market, making it impossible for other programs to interoperate, and so impossible for them to compete.

#eoa: As the Microsoft case made clear, anti-trust laws can take a very long time to operate, by which time the marketplace may have utterly changed.

#WWW2: EU copyright laws recognise this danger, and Directive 91/250/EEC, Articles 5(2) and (3) and 6, allow decompilation of a program to investigate its interfaces, although the decompiled source code may not be made public, and decompilation is permitted only if the information is not otherwise readily available.

#lat: The CEC article 6 (and various amendments that rewrite it) uphold this right of decompilation.

#aWW: But this is of little help in achieving interoperability when interfaces are patented: interoperation would be possible only by securing a valid licence for the patent.  Decompilation is a problem only in the context of copyright.  Allowing it in the contexts of patents means allowing nothing.

#mwi: So an amendment of fundamental importance is %(ahi:amITRE-15:Amendment ITRE-15) (Article 6(a)), which would create a similar protection for interoperability in the face of patent rights.

#rWm: For a general introduction to the pitfalls of the CEC/BSA directive proposal and the art of amendment writing, please read

#Lgn: Legend and Commented Example

#eia: Title and Recitals

#ril: Articles and Motions

#enm: Amendment number

#Wte: Member(s) of European Parliament who submitted this recommendation

#Etn: FFII/Eurolinux voting recommendation

#eoe: Arlene McCarthy MEP voting recommendation

#Mlm: Toine Manders MEP (Liberals) voting recommendation

#Ris: JURI voting result

#jac: pro, contra, undecided

#tuW: 21 voted in favour, 9 voted against

#Tit: Title

#ehr: Proposal for a Directive of the European Parliament and of the Council on the patentability of computer-implemented inventions

#trW: text of the %(op:original proposal of European Commission)

#ung: The criticism of Manders and Doorn is justified. It resonates with the view of the %(dg:study of Bakels and Hugenholtz), commissioned by DG IV, as well as that of many leading law experts, that this directive creates a legal mess. Yet a regulation is not necessarily better.

#mFo: Comment of FFII/Eurolinux

#mmW: Amendment 21

#dTe: This amendment was submitted by Toine Manders MEP and Bernt Doorn MEP

#amd: FFII have no recommendation.

#leg: MEP Arlene McCarthy recommends voting against.

#nWg: MEP Toine Manders (ELDR) recommends voting in favor.

#tva: 3 voted in favour, 25 against

#ute: Proposal for a Regulation of the European Parliament and of the Council on the patentability of computer-implemented inventions

#xae: text of amendment 21

#Wsl: In the following we comment on each amendment under discussion at JURI, grouped by the articles to which they apply.

#ea1: recital 11 a

#ea3: recital 13 a

#ea32: recital 13 aa

#ea33: recital 13 b

#ea34: recital 13 c

#ea35: recital 13 d

#too: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Wnl: CEC boldly postulates, against all %(ss:economic evidence), including %(bh:very detailed empirical research by Bessen & Hunt from 2003), that software patents encourage investments in software. Amendment 22 confirms the propaganda and adds that the %(q:protection of computer-implemented inventions) should be not only %(q:effective and harmonised) but also %(q:transparent). This addition reduplicates text from recital 5 and makes the semantic structure of the sentence less transparent.

#oet: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. Effective, transparent and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#tpm: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer- implemented inventions and provide an incentive for investment and innovation.

#Wrs: While European Commission (CEC) and Amendment 1 (McC) say dogmatically that software patents stimulate innovation, ITRE says that the policy goal is to stimulate innovation and patentability should be measured by that goal. ITRE also suggests that more patents could possibly mean less innovation, while CEC/McC do not admit such a possibility.  Amendment 1 seems to claim that the mere fact of writing EU law, no matter what the contents, already %(q:secures legal certainty), simply because the Luxemburg court can interpret this law.  This new goal statement is not only questionable but also pointless, because %(ol|the Community Patent is under way anyway|there are easier and more systematic ways to put the Luxemburg court in charge than by passing an interpretative directive for one aspect of the European Patent Convention)

#tln: Therefore, the legal rules governing the patentability of computer-implemented inventions should be harmonised so as to ensure that the resulting legal certainty and the level of requirements demanded for patentability enable innovative enterprises to derive the maximum advantage from their inventive process and provide an incentive for investment and innovation.

#anl: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer-implemented inventions and provide an incentive for investment and innovation.

#css: Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may and national courts of last instance must seek a ruling from the Court of Justice.

#nph: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology.

#eug: Art 52 EPC says that calculation rules and computer programs are not %(e:inventions) in the sense of patent law, i.e. that a system consisting of generic computing hardware and some combination of calculation rules operating on it can not form the object of a patent. It does not say that such systems can be patented by declaring them to be %(q:not as such) or %(q:technical). Amendment 23 attempts to fix the bug but succedes only partially.

#oWe: Amendment 24 tries to redress some potential uncertainity in vague wording in the CEC proposal by recognizing the validity of the EPC in case any provision in the directive would contradict it, thus making the coexistence of both legislations less uncertain.

#stg: Amendment 25 clarifies that programs are not inventions (nor %(q:computer-implemented inventions), whatever that may be), and they cannot infringe.

#WWa: ITRE-2 hints another part of the solution to the European patent system, beyond the directive itself: accountability of the EPO. This reminds that the EPC is above EPO caselaw in case of conflict.

#aWi: CULT-1 reminds that software is a form of expression, explicitly recognizing freedom of expression principles for software and therefore justifiying human rights considerations.

#iTT: ITRE-3 is similar to CULT-1, CULT-3 , CULT-5, CULT-7.

#Wih: CULT-2 is similar, slightly weaker than ITRE-2.

#Teg: CULT-3 and CULT-5 introduce the majority of stakeholders, whose mention was missing in the CEC proposal.

#Wfs: CULT-4 introduces the role of software as a form of knowledge and a knowledge enabler, and therefore the need for interoperability and access to software for expression, education, and cultural heritage.

#sfn: CULT-6 completes other amendments in noting the difference in stakes between software and industrial property.

#Utt: CULT-7 and CULT-8 justify restraint in extending patentable subject matter to software.

#tti: CULT-9 warns that the complexity and incrementality of software makes it unsuitable for patents.

#WaW: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception also applies if the matter referred to is implemented in computer programs which are executed in computers and which do not make a technical contribution. Merely specifying technical means in the patent claims does not constitute a technical contribution.

#ste: (7a) The aim of this Directive is not to amend the European Patent Convention, but to prevent different interpretations of its provisions.

#inW: (7a) A computer program, and in particular the expression of a computer program in source code or object code or in any other form, does not, therefore, constitute a patentable invention. The manufacture, offering for sale or placing on the market of such a computer program, or the importation or possession thereof to those ends, cannot constitute an infringement of patent law.

#osf: (7a) Parliament has repeatedly asked the European Patent Office to review its operating rules and for the Office to be publicly accountable in the exercise of its functions. In this connection it would be particularly desirable to reconsider the practice in which the Office sees fit to obtain payment for the patents that it grants, as this practice harms the public nature of the institution. In its resolution 1 on the decision by the European Patent Office with regard to patent No EP 695 351 granted on 8 December 1999, Parliament requested a review of the Office's operating rules to ensure that it was publicly accountable in the exercise of its functions. 1- OJ C 378, 29.12.2000, p. 95.

#WWW3: (7b) In its resolution (published in OJ C 378, 29.12.2000, p. 95) on a decision by the EPO with regard to patent No EP 695 351 granted on 8 December 1999, the European Parliament demanded a review of the EPO to ensure that it becomes publicly accountable in the exercise of its functions.

#osi: While software plays an important role in a number of industries it is also a basic form of creativity and self-expression. Software is, in addition, a field of specialised engineering and a basic human activity, with more than 10 million professional developers throughout the world and tens of millions of people creating software for one purpose or another. Independent developers and small businesses play a fundamental role in innovation in this area. It follows that the means employed to boost investment in largely software-based industries should not lead to jeopardising the capacity of all concerned to become active creators and innovative users of software, and in particular that patents should not permit the monopolisation of tools for self-expression, creativity, and the dissemination and exchange of information and knowledge.

#aWr: (7a) Software plays a key role in many industries and, moreover, is a fundamental means of creation and expression.

#W0W: (7c) At the same time software is a specialised field of engineering and an important human activity, with more than 10 million professional software developers worldwide and tens of millions of people who develop software in one capacity or another.

#nde: (7d) An increasing amount of information and knowledge is intrinsically linked to the software through which it is created, expressed, disseminated and put to use.

#Wso2: (7e) Independent software developers and small businesses make a crucial contribution to innovation in this area.

#WcW: (7f) This situation, in which there is a huge number of innovators and technology influences basic cultural activities, marks a completely new departure in the history of patents and requires specific precautions as to the manner in which patents are applied in this area.

#WuW: (7g) It follows, therefore, that the means used to encourage investment in software- intensive industries ought not to serve to jeopardise the potential of anyone to become an active developer and innovative user of software.

#neg: (7h) In particular patents must not allow monopolies to be established over means of expression, creation, dissemination and exchange of information and knowledge.

#oeW: (7i) The various software components or levels are highly interdependent and, therefore, the greatest possible care should be exercised in respect of the extent of the protection afforded by patents, in order to ensure that markets remain competitive and open.

#ane: Patent protection allows innovators to benefit from their creativity. Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anti- competitive.

#tpe: Dogmatic statements about the beneficiality of patents need to be replaced by a statement which takes the various possible effects of patents into account.

#WiW: Patents are temporary monopolies granted by the State to inventors in order to stimulate global technical progress. In order to ensure that the system works as intended, the conditions for granting patents and the modalities for enforcing them must be carefully designed. In particular, in order that inevitable corollaries of the patent system such as restriction of creative freedom, legal insecurity and anti-competitive effects be kept within reasonable limits.

#eWe: Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they should make a technical contribution to the state of the art.

#nry: According to CEC and amendment 2, the techinicity test only affects whatever is non-obvious, and the test for subject matter, novelty or industrial applicability may be passed by different parts of the claimed object.  Such a logic assures that patentability is simply a question of claim wording, see the section on %(se:Four Separate Tests on One Unified Object).  In order to avoid confusion, the recital needs to be rewritten or deleted.

#delet: deleted

#ews: In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, computer-implemented inventions should make a technical contribution to the state of the art.

#inr: Amendment 28 would be a perfect restatement of Art 52 EPC if it had said %(q:inventions) instead of %(q:computer-implemented inventions). As it is now, it looks as if it was a special rule for inventions involving computers.   Yet it is useful in that reinforces the notion that the same invention must pass the four tests.  See Article 1 for why the term %(q:computer-implemented invention) is misleading.

#nWr: (11a) Computer-implemented inventions are only patentable if they may be considered to belong to a field of technology and, in addition, are new, involve an inventive step and are susceptible of industrial application.

#ola: Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#tjW: As Manders writes correctly in his justification for Amendment 29: %(q:The question of patentable matter is being linked to the question of inventive step. These questions must be considered separately from each other, as is the case with patents in general.)  The EPO has been mixing the two in order to avoid clearly excluding any subject matter from patentability. Amendment 31 avoids this confusion and states what constitutes a technical contribution. Something like this needs to be in the Recitals.  Deletion, as proposed by Manders, is next-best.  McCarthy's Nr. 3 is particularly objectionable, since it reintroduces the idea that computer programs %(q:by their very nature are technical) from Article 3, whose deletion McCarthy had recommended.

#toc: Accordingly, a patentable computer-implemented invention should make a technical contribution to the state of the art, which is only the case if the contribution meets the criterion of having a technical character, i.e. teaches in one or more ways about cause-effect relationships when using controllable forces of nature. The extent of the protection provided by patent claims granted may, irrespective of further non-technical contributions and effects, only include this technical contribution. Patent claims should, in addition, be granted only if sufficient disclosure has been made of the computer-implemented invention.

#Wio: (12) Accordingly, even though a computer-implemented invention belongs by virtue of its very nature to a field of technology, it is important to make it clear that where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable. When assessing whether an inventive step is involved, it is usual to apply the problem and solution approach in order to establish that there is a technical problem to be solved. If no technical problem is present, then the invention cannot be considered to make a technical contribution to the state of the art.

#mnt: A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.

#gin: The CEC version pretends to exclude algorithms but in fact makes algorithms patentable, because an algorithm which is defined with reference to the environment of generic data processing equipment (= %(q:a physical environment)) is equivalent to the algorithm per se.

#feo: A defined procedure or sequence of actions, when performed with the help of an apparatus such as a computer, may contribute to our knowledge about the cause-effect relations of controllable forces of nature and thereby constitute a patentable invention. However, an algorithm or a computer program, regardless of whether the symbolic entities of which it is composed can be interpreted as referring to a physical environment or not, is inherently non-technical, and cannot therefore constitute a patentable invention.

#plD: Amendment 33 introduces the concept of %(q:rules of organisation and calculation) as an antonym to %(q:technical inventions), thereby drawing from the german patent law doctrine (e.g. %(dp:Dispositionsprogramm) and successor decision.)

#aWc: Computer-implemented rules of organisation and calculation (mathematical methods, algorithms, schemes, rules and methods for performing mental acts or doing business and programs for computers, etc) do not, pursuant to this directive, belong to the field of technology and are therefore not patentable. A method involving the use of rules of organisation or calculation may only be patentable subject to the additional condition that it makes a technical contribution to the state of the art.

#dad: Syntactical ambiguity in this amendment leaves it unclear whether %(q:business method) in general are considered to be a %(q:methods in which the only contribution to the state of the art is non-technical) or whether certain business methods could contain %(q:technical contributions).  More importantly, this amendment will not really exclude anything, because patent lawyers will be quick to claim that some computer-related features are characteristic for the %(q:invention), which, as stated elsewhere in this directive proposal, must be %(q:considered as a whole).  For this to be of any use, the syntactical ambiguity around %(q:other method) would have to be removed and the category of %(q:non-technical methods) would have to be explained by definitions and/or examples.

#tta: However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.

#hcp: This states a common sense position which has often been disregarded in order to extend patentability.  This alone does not achieve much and the statement in the law that the law %(q:can not be circumvented) is a pious wish.  Yet even a wish is better than nothing.

#eWe2: If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims.

#toW: Placed in the context of patent examination practise, this statement is not what it seems.  It widens patentability even compared to EPO practise by allowing patentability of non-technical solutions to technical problems.  Moreover, it makes all algorithms patentable in all situations, because a %(q:technical problem) can always be defined in terms of generic computing hardware, which at the same time provides a way of claiming algorithms in their most abstract form.

#nWe: Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.

#Waf: The amendment simply restates the definition of claims: claims define the exclusion right granted by the patent. It allows claims to software by merely refering to generic computer equipment or computer processes, regardless of where the innovation lies. It's similar to saying: you can claim what you please as long as you use vocabulary like storage, processor, or apparatus somewhere in the claims, but be careful to make your claim as broad as you want it, because you won't monopolise what you didn't claim.

#Wns: The amendment may have an indirect benefit: it seems to contradict amendment COMP-1 by saying that only programmed apparatusses and processes can be claimed.

#fsn: The scope of the exclusive rights conferred by any patent are defined by the claims.  Computer-implemented inventions must be claimed with reference to either a product such as a programmed apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such use will not constitute patent infringement.

#nnW: The legal protection of computer-implemented inventions should not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive.

#tdw: The CEC recital is not too bad (although it uses the %(q:computer-implemented inventions) term, see Article 1). It simply says patent law should not be replaced.  Amendment 7 is less clear.  On the one hand it equates %(q:present legal position) (?) with %(q:practices of the European Patent Office). On the other it says that the patentability of business methods is to be avoided. However the EPO itself has been granting many patents for business method and has provided %(a6:legal reasoning to support business method patents).  The only possible effect of the amendment if any would be to surrender legislative responsability to the EPO, whose practices would seem to define patentability instead of converging to laws by the European Parliament and other democraticaly representative bodies.

#tnt: (14) The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position having regard to the practices of the European Patent Office with a view to securing legal certainty, transparency, and clarity in the law and avoiding any drift towards the patentability of unpatentable methods, such as business methods.

#nni: The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent.

#olb: The CEC version aims to give a rationale for %(q:harmonisation) by a EU directive in a situation where patent law is already 100% harmonised. The rationale is unreasoned and highly questionable, as has been pointed out by the %(bh:study by Bakels and Hugenholtz) and many others.

#lsp: Amendment 8 suggests that the directive is motivated by present-day external market policy considerations rather than by mere %(q:harmonisation of the internal market).  It presents software patents as a tool of protectionism.  As far as this could work at all, it would protect companies from USA and Japan (who hold about 75% of the software patents already granted by the EPO) from competition by our otherwise competitive European software sector, including that of low-cost economies such as those of Poland, Portugal, Greece etc.  Also, if programming work is exported, then patents will follow suit sooner rather than later, leaving Europe behind as a high-cost economy, characterised by a flourishing patent litigation sector.

#sed: Amendment 34 is more to the point. It stresses that the directive should take the aim of %(q:harmonisation and clarification) seriously and not pursue other unstated goals (such as those insinuated in amendment 8) under a cover of %(q:harmonisation and clarification).

#jue: The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent. However, the removal of these barriers may not entail the abandonment of basic principles of European patent law, such as the principle that only technical inventions and inventions with industrial application are patentable.

#rst: The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident.

#emW: The role of free/opensource software in today's information economy and specifically in Europe indeed should be addressed in the recitals.

#ael: (16a) At the international level, Europe is ahead in the area of open, alternative development and licensing approaches to computer programs, e.g. open source projects under the %(q:General Public License). Particularly in the light of increasing requirements for stability, interoperability and IT security of computer programs, open source computer programs developed on a common, ongoing and transparent basis are gaining in importance. In order to turn this European lead in terms of development into a real competitive advantage, the legal framework conditions for such alternative development and licensing approaches must continue to be able to be relied upon.

#mdw: Amendment 36 is confusing.  The definition of %(q:industrial) is circular, and excluding %(q:intellectual activity) hardly makes it clearer.  Moreover, the four requirements of %(q:[technical] invention), %(q:novelty), %(q:non-obviousness) and %(q:industrial applicability) should be separate tests.

#Wof: For an invention to have industrial character, it is necessary not only for the means used in its implementation to be predominantly industrial, but also for the result it produces to be industrial in character. The industrial character of an invention also implies that it contains no indication of human intellectual activity. In any case, as far as this Directive is concerned, when mention is made of the technical contribution, it should be understood that this refers to industrial technology.

#lWs: This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.

#aiW2: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#eWa: The CEC proposal only pretends to protect interoperability but in reality assures that patentee rights can not be limited in any way by interoperability considerations. See explanation of Art 6 for this. Amendments 37,38 only further restrict the provisions that can be used to override patent rights to those written in a specific set of laws.   Art 39 adds a principle of non-overlapping between spheres of property rights, which is helpful and dserves further elaboration.

#Eia: (18) Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, should not be affected through the protection granted by patents for inventions within the scope of this Directive.

#t5t: The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for Articles 5 or 6 of Directive 91/250/EEC, would require such authorisation, shall not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program.

#nii: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive. Under no circumstances should patent protection and copyright protection overlap.

#ieu: Legislative Resolution

#nis: Motion for a legislative resolution

#vtm: This Directive lays down rules for the patentability of computer-implemented inventions.

#nWw: Ideas implemented in generic computer hardware are not inventions in the sense of Art 52(2) EPC. The term %(q:computer-implemented invention) is not known to the man skilled in the art. It was introduced by the EPO in 2000 in the infamous %(e6:Appendix 6 of the Trilateral Website document) in order to bring European patent law in line with US and JP, allow patents for programs and business methods and prepare abolishment of Art 52(2) EPC.

#ste2: This Directive lays down rules concerning the limits of patentability and patent enforcability with respect to computer programs.

#enW: A major bug of the CEC proposal is that it uses the term %(q:invention) in a double sense and thereby obliterates the invention test, which in traditional patent doctrine of EPC and TRIPs is an independent test and not ancillary to %(q:inventive step) (= non-obviousness).  Amendment 11 goes into the wrong direction by declaring this bug to be a feature.

#Wmb: (a) %(q:invention) encompasses both patentable inventions and matter whose patentability has not been established or is in question;

#aga: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;

#apt: Amendment 41: Best of breed. Destroys the notion that a standalone computer can be a computer-implemented invention; Amendment 5 itre: Technicity criterion is absolutely critical; useful removal of the words 'prima facie', to confirm the word 'invention' applies only to patentable material; Amendment 10 cult: Technicity criterion is absolutely critical; Amendment 12: Companion to Am 11, reserving until later whether inventions are patentable or not. But this upsets other parts of the directive; Amendment 42: Slight tidying up of Am 12

#Wtr: (a) %(q:computer-implemented invention) means any technical solution the implementation of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel or non-novel implementation features which are realised wholly or partly by means of a computer program or computer programs, whereas the prima facie novel solution features depend wholly or partly on the presence of peripheral hardware which uses forces of nature in an inventive way;

#stW: (a) %(q:computer-implemented invention) means any invention susceptible of industrial application the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more novel features which constitute a technical contribution, and other features whether novel or not, and have to be realised wholly or partly by means of a computer program or computer programs;

#Wtr2: (a) %(q:computer-implemented invention) means any technical solution the implementation of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;

#mrm: (a) %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and which is realised wholly or partly by means of a computer program or computer programs;

#eWl: (a) %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more features which are realised wholly or partly by means of a computer program or computer programs;

#bWo: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#ytW: Amendment 43 and CULT-11 are the only amendments which actually clarify anything. A directive which makes patentability hinge on the word %(q:technical contribution) must explain this term clearly. Unfortunately none of the amendments fixes the notional chaos which is introduced by mixing the %(q:technical contribution) (= invention) requirement with the novelty and non-obviousness requirements. All thereby deviate from Art 52 EPC.

#Wni: ITRE-6 seems to imply that only the problem but not the solution needs to be technical. The word %(q:significant) is merely another indeterminate pious wish and therefore conducive to confusion rather than clarification.

#aWn: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art, that is, a new teaching on cause-effect relations in the use of controllable forces of nature.

#aog: (b) %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.

#eoi: (b) %(q:technical contribution) means a contribution, involving an inventive step to a technical field which solves an existing technical problem or extends the state of the art in a significant way to a person skilled in the art.

#cbn: (b) %(q:technical contribution) means a contribution to the state of the art in a technical field.

#mca: Since the rapporteur's attempt to %(q:make it clear what is patentable and what not) hinges entirely on the word %(q:technical), this term must be defined clearly and restrictively. The only definition which achieves this is the one along the lines of amendment 45, which is found in various national caselaws and in some patent laws (e.g. Nordic Patent Treaty and patent laws of Poland, Japan, Taiwan et al). This definition is based on valid concepts of science and empistemology and has been proven to have a clear meaning in practise. It assures that broad, expensive and insecurity-fraught broad exclusion rights such as patents are used only in areas where there is an economic rationale for them, and that abstract-logical innovation is the domain of copyright and copyright-like sui generis rights only.  The word %(q:numerical) in Amendment 45 is apparently the result of mistranslation from French.  It should be %(q:digital).

#Wct: %(q:technical field) means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. %(q:Technical) means %(q:belonging to a technical field). The use of forces of nature to control physical effects beyond the numerical representation of information belongs to a technical domain. The production, handling, processing, distribution and presentation of information do not belong to a technical field, even when technical devices are employed for such purposes.

#aWa: Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.

#nas: This article is about the interpretation of Art 27 TRIPs. Amendment 46 reformulates the principles of Art 52(2) in the language of Art 27 TRIPs and thereby wards off misinterpretations of TRIPs which have been abused as pretexts for extending patentability beyond the limits of Art 52 EPC. It it the single most important principle that needs clarification.  Article 3 should therefore not be deleted but corrected, if this directive is to clarify anything.  Arlene McCarthy correctly criticises this article and proposes its deletion, only to reinsert the same criticsed content into recital 12 by amendment 3.

#ieW: Member States shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not inventions in the sense of patent law, regardless of whether they are executed in the human mind or by means of technical devices.

#WoW: Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#svm: Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.

#eak: The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.

#irW: Amendment 47 is an improvement in that it avoids mingling %(q:technical contribution) with %(q:inventive step). It leaves other flaws of the CEC proposal unaddressed, such as the word %(q:computer-implemented invention) and the distinction between %(q:invention) and %(q:technical contribution), both of which run counter to Art 52 EPC.  It would however be preferable to have a longer Art 4 with better amdendments such as 48, 51 and 52.  Amendment 14 seems to do little except for making the text more difficult to cite.

#jia: In order to be patentable, a computer-implemented invention must be susceptible of industrial application, be new, involve an inventive step and make a technical contribution.

#tpn: Amendment 14 is analysed as parts 14(1), 14(2) and 14(3) below

#WoW2: Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#aWp: %(q:Computer-implemented inventions) (i.e. ideas framed in terms of general-purpose data processing equipment = programs for computers) are non-inventions according to Art 52 EPC. Amendment 48 fixes the bug and restates Art 52 EPC.  None of the amendments limit patentability, but nr. 48 at least gets things formally correct.

#nib: Member States shall ensure that patents are granted only for technical inventions which are new, non-obvious and susceptible of industrial application.

#nWt: 1. Member States shall ensure that a computer-implemented invention is patentable only on the condition that it makes a technical contribution as defined in Article 2(b).

#tst: 1. Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, non-obvious, involves an inventive step, and belongs to a technical field.

#eii: In order to be patentable, a computer- implemented invention must be susceptible of industrial application and new and involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution. The technical contribution shall be assessed by considering the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective whether or not such features are accompanied by non-technical features.

#aai: Member States shall ensure that a computer-implemented invention is not patentable `per se', but only insofar as it is susceptible of industrial application, is new, and involves an inventive step and a technical contribution.

#svm2: Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.

#u5e: The Commission text deviates from Art 52 EPC by merging the independent requirement of %(q:invention) and %(q:technical character) into a single requirement of %(q:technical contribution) which again is not an independent requirement but somehow subordinate to the requirement of %(q:inventive step) (non-obviousness).  Moreover the Commission text implies that ideas framed in terms of the general purpose computer (programs for computers) are patentable inventions. These deviations from Art 52 EPC are incompletely addressed by Amendments 51 and CULT-14. They do however supply the missing explanation of the term %(q:technical contribution), on which the proposed limitation on patentability hinges.  Deletion, as proposed by ITRE-9, could be helpful, because the only effect of this paragraph is to confuse patentability tests and thereby make it impossible for national patent offices to reject non-statutory patent applications without substantive examination. COMP-6 and 14(2) inflate the CEC wording without changing the meaning.

#Wrc: Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution, that is, provide new problem solutions consisting in teachings about new cause-effect-relations in the use of controllable forces of nature, which are not obvious to a person skilled in the art.

#anW: Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution, that is to say, it must impart a new lesson in the relationships of cause and effect involved in the controlled use of natural forces.

#oie: In order to involve an inventive step, a computer-implemented invention must make a technical contribution.

#WoW3: Les États membres veillent à ce que le fait qu'une invention mise en oeuvre par ordinateur apporte une contribution technique constitue une condition nécessaire à l'existence d'une activité inventive.

#ecn: included in 4.1

#WWr: (Does not affect English version.  Of linguistic nature, not voted)

#eak2: The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.

#nrh: The European Commission's proposal voids its own concept of %(q:technical contribution) by allowing the contribution to consist of non-technical features, as has been widely criticised in the patent law literature. It is clear that a correction such as that in amendment 52 is necessary, if the concept of %(q:technical contribution) is to be used at all. Amendment 53 also removes some of the worst inconsistencies, but at the price of being meaningless and tautological: who would ever doubt that an invention must be assessed %(q:on the basis of) the patent claim as a whole? The question: how does the examiner single out the invention/contribution part from within that basis?

#lWo: The novelty grace period proposed by ITRE is an orthogonal question.  As shown by a %(ng:recent consultation conducted by the UK Patent Office), it is very controversial even among those who are supposed to benefit from it.  It is not clear whether a novelty grace period would in fact, as suggested by the ITRE report, benefit SMEs.  In the case of open source development, it could even cause additional insecurity as to whether published ideas are free from patents.

#ien: The technical contribution shall be assessed by consideration of the difference between all of the the technical features of the patent claim and the state of the art.

#ohW: 3. The technical contribution shall be assessed by consideration of the difference between the scope of the technical features of the patent claim considered as a whole and the state of the art.

#eii2: In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step. In order to involve an inventive step, a computer-implemented invention must make a technical contribution. The technical contribution shall be assessed by considering the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective whether or not such features are accompanied by non-technical features.

#btW: 3. The significant extent of the technical contribution shall be assessed by consideration of the difference between the technical elements included in the scope of the patent claim considered as a whole and the state of the art. Elements disclosed by the applicant for a patent over a period of six months before the date of the application shall not be considered to be part of the state of the art when assessing that particular claim.

#pce: Indeed whenever any reference to a computer program (computing logic) is made in the claims, the description should contain a reference implementation of this logic in a well-standardised computing language. This amendment does not protect anyone from the negative effects of software patents, but it helps to improve the disclosure function of granted patents in various fields which use software.

#lne: Member States shall ensure that patent claims to a computer-implemented invention are granted only if there is full disclosure of the invention. Full disclosure includes the publication of the computer programs used for the purpose of implementation in source code, including comments.

#eec: Amendments 15 and 55 assume that %(q:normal physical interaction between a program and the computer) means something. It doesn't and would thus have to be clarified by caselaw.  If the Luxemburg court interprets this statement as an intention to exclude data processing from patentability, it can be helpful.  It would have been more helpful to specify, as done by the German Federal Patent Court in the %(es:Error Search decision of 2002) and by the EPO in some earlier decisions, that %(q:increase of computation efficiency), %(q:savings in memory usage) etc %(q:do not constitute a technical contribution) or %(q:are within the scope of the normal physical interaction between a program and the computer).  ITRE-11 goes somewhat in this direction by saying that %(q:manipulation of information within the computer system) is unpatentable, but this is still unclear and may be interpreted even more narrowly than %(q:normal physical interaction between the program and the computer): in a strict sense computers do not process information at all (but only data).  55 is preferable to 15 because it establishes a category of %(q:rules of organisation and calculation) for the non-patentable subject matter.  On the other hand it suggests that such rules are patentable (can %(q:make a technical contribution)).  ITRE-11 could be understood to mean that implementations of business and mathematical methods in general do not produce %(q:further technical effects), but it stops short of stating this clearly.

#snd: 3a. Exclusions from patentability

#nar: A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, or other apparatus. Accordingly, inventions involving computer programs which implement business, mathematical or other methods, which inventions do not produce any technical effects beyond the manipulation and representation of information within computer-system or network, shall not be patentable.

#til: McCarthy prevented voting of this amendment by making it dependent on previous refusal of her amendment 14.

#uoa2: Exclusions from patentability

#dch: The fact that a computer or other programmable apparatus is used cannot in itself be regarded as constituting a technical contribution by a computer-implemented invention. Accordingly, inventions which involve computer programs and which, beyond the normal physical interaction between a program and the computer, computer network or other apparatus on which the program is run, do not make any technical contribution, shall not be patentable.

#Wes: Computer-implemented inventions which, in one or more ways, implement rules of organisation and calculation, such as business, mathematical or other methods, and which, beyond the normal physical interaction between a program and the computer, computer network or other apparatus on which the program is run, do not make any technical contribution, shall not be patentable.

#uoa: Exclusions from patentability

#hWW: A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus.  Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.

#6oW: Amendment 56 is not clear and seems to mean that new algorithms can be patented.

#tmn: For the purposes of the previous article, the following conditions must be met in order for computer-implemented inventions to be patentable:

#WWW4: Both the means employed and the result must be industrial,

#yWa: They must involve novelty and an inventive step, to ensure that inventions already in the public domain cannot be appropriated by the mere fact of running a computer program;

#ppe: The description that must be made public should include the source code;

#osi2: Research into the object of the patent must be allowed, and therefore its decompilation;

#edt2: In any event, the patent holder must keep the market adequately supplied with the result of the patented invention.

#ime: Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.

#roW: Amendments 57, 58, 60, 61, 62, 64 and COMP-1 propose to introduce program claims, i.e. claims of the form %(bc:a program, characterised by that upon loading in computer memory [ some computing process ] is executed.) This would make all publication of software a potential patent infringement and thus put programmers and Internet service distributors at risk. Without program claims, distributors could be liable indirectly, e.g. through the guarantees which they give to their customer.  The program claim question seems to be of a symbolic than of material nature: the patent lobby wants to document unmistakeably that software as such is patentable.  CEC has refrained from this last consequence.  Yet the CEC wording still suggests that known general purpose computing hardware and calculation rules executed thereon (= computer programs) are patentable products and processes. Amendment 59 tries to address this problem.  It might be a good idea to replace %(q:technical production processes operated by such a computer) by %(q:the inventive processes running on such a set of equipment) if that is still possible in the form of a %(q:compromise amendment).

#xmp: Amendment 65 forbids program claims and thereby makes explicit what the CEC version only says implicitely. It has formal flaws which would have to be corrected later, e.g. %(ol|it is impossible to include only those features in the patent claim which belong to the contribution|the propaganda term %(q:computer-implemented invention) is incompatible with Art 52 EPC, as explained above.)

#nWt2: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is a set of equipment comprising both programmable apparatus and devices which use forces of nature in an inventive way, or as a technical production process operated by such a computer, computer network or apparatus through the execution of software.

#eea: (a) Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed device, or as a technical production process.

#rWo: Member States shall ensure that the forms of claims in respect of a computer-implemented invention may be made only to the effect that the invention is a product, that is a programmed computer, a programmed computer network or other programmed apparatus, or a technical production process controlled by such a computer, computer network or apparatus through the execution of software.

#EAW: CEC Art 5

#Wce: Member States shall ensure that patent claims granted in respect of computer-implemented inventions include only the technical contribution which justifies the patent claim. A patent claim to a computer program, either on its own or on a carrier, shall not be allowed.

#Web: Without this amendment, software patent owners could still send cease-and-desist letters to programmers or software distributors accusing them of contributory infringement.  This article makes sure that the right of publication as guaranteed by Art 10 ECHR takes precedence over patents.  On the other hand, this amendment does not prevent the granting of software patents or their enforcement against users of software.  Vendors of proprietary software and commercial Linux distributors could still be under pressure from their customers, who would usually impose a contractual obligation of patent clearance on them.

#pte: Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.

#vto: McCarthy prevented voting of this amendment by making it dependent on adoption of ITRE-12.

#Wae: Member States shall ensure that the production, handling, processing, diffusion and presentation of information in whatever form can never constitute a direct or indirect patent infringement, even when technical devices are used for that purpose.

#Wic: Member States shall ensure that the processing, handling, dissemination, and presentation of information in whatever form do not constitute a direct or indirect patent infringement.

#dWw: The suggested provisions are likely to be redundant, but stating them won't do any harm.  They suggest that only programs related to inventive physical processes are affected, and that these may be run for simulation or other purposes in a generic computing environment where the physical process is not executed.

#tta2: Member States shall ensure that the use of a computer program for purposes not requiring the use of the technical contributions claimed in the patent does not constitute a direct or indirect patent infringement.

#sne: (c) Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.

#oWo: Not put to vote, because Arlene McCarthy made it dependent on non-adoption of COMP-1

#reW: These amendments do, unlike some may think, not serve to promote protect free/opensource software, but rather to ensure that the obligation of disclosure which is inherent in the patent system is taken seriously and that software is, like any other information object, on the disclosure side of the patent rather than on its exclusion/monopolisation side.

#eWe3: (d) Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program is published as part of the patent description without any restricting licensing terms.

#oWo2: Not put to vote, because Arlene McCarthy made it dependent on non-adoption of COMP-1.

#eae: Member States shall ensure that whenever a patent claim mentions features entailing the use of a computer program, an operational and well-documented reference run of that program is published as part of the patent description without any restricting licensing terms.

#abn: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trademarks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#ttg: The European Commission's proposal does not protect interoperability but, on the contrary, makes sure that interoperable software can not be published or used when an interface is patented. The only behavior which the European Commission wants to permit in this case is decompilation, which would not infringe on patents anyway. Amendments 66 and 67 moroever specify the laws on which interoperability privileges can be based, thus making it even more certain that the provision can not have any effect. Amendment 68 states the principle of separation of copyright and patents and consequently non-patentability of software, thus protecting interoperability only indirectly.  Only ITRE-15 formulates a real interoperability privilege.  However fears have been %(rm:expressed) that the wording %(q:computer systems) could be interpreted narrowly in the sense of %(q:hardware architectures).  It should be corrected to %(q:data processing systems) if possible.

#ydi: Member States should ensure that the domains of copyright and patent protection do not overlap. Property in computer programs is acquired and regulated through copyright. Property in technical inventions is acquired and regulated through patents. Aspects of a computer program that cannot be appropriated through copyright cannot be appropriated through patents, utility certificates nor any other property regime. Consequently, acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected by this Directive.

#hin: (a) Member States shall ensure that wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or network so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.

#5ie: 6. Acts permitted as exceptions under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#s0e: The rights conferred by patents granted for inventions within the scope of this Directive shall be without prejudice to acts permitted by way of exception under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular the acts specified and described in the closed list set out in Articles 5(2) and (3) and 6 of Directive 91/250/EEC.

#nes: The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.

#sic: The Member States shall ensure that computer-implemented inventions that meet the conditions laid down in this Directive do not benefit from protection by both patent and copyright.

#tei: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#tWa: If SMEs are specially mentioned, then at least free/opensource software and standardisation should also be mentioned, and it should be made sure that the investigations are not performed by the patent lawyers from DG Internal Market but rather by software and economics experts.

#pnd: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises, and electronic commerce.

#Win: The Commission shall monitor the impact of patent protection for computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#aWW2: The continuous expansion of the scope of patentability is largely due to the political difficulties of narrowing the field of patentability once it has been widened, because only patentees have an explicit claim scope granted to them, while the public at large does not. Something must be done to counter-balance this automatism.

#irt: The European Parliament shall form a permanent committee on the criteria of patentability. This investigation committe shall be empowerered to take appropriate means to obtain any needed information from the European Patent Office, the European Commission and important industry players. The European Community retains the freedom to adopt a stricter interpretation of concepts such as %(q:invention), %(q:technical character), %(q:inventivity) and %(q:industrial application) any time later and to apply this stricter interpretation retroactively to patents which were granted under looser interpretations when this is found to be in the public interest.

#eea2: Monitoring by the European Commission means that the European Commission can also propose amendments.  It is not wrong to state this here.  But the European Commission is not an impartial player.  Before proposing the directive it has already failed to take conflicting views and interests into account.  The proper solution would be to have a permanent committee of the European Parliament handle this job, as proposed in amendment 70.

#adi: The EPO has been making, breaking and rewriting laws as it pleased and the EU institutions merely followed its lead.  Expressing at least some concern about this may be little more than a symbolic gesture, but it's better than not doing so.

#tso: Whether the powers delegated to the EPO are compatible with requirements for harmonisation of the EU legislation, together with the principles of transparency and accountability.

#yct: The questions imply the answers.  The patent lobby at the European Commission is inveited to restate its dogma that %(q:copyright and patents are complementary), one applying to %(q:computer programs) and the other to %(q:computer-implemented inventions).  In reality both apply to computer programs, but patents have a broader exclusion scope.  Investigating the %(q:relationship between patentable inventions and copyrightable creations in the field of computer programs) could have been more interesting.

#niy: (d) whether difficulties have been experienced in respect of the relationship between the protection by patents of computer-implemented inventions and the protection by copyright of computer programs as provided for in Directive 91/250/EC and whether any abuse of the patent system has occurred in relation to computer-implemented inventions;

#iph: (ca) any difficulties that have arisen with the relationship between protection by means of patents on computer-implemented inventions and the protection of computer programs by means of copyright law, as laid down in Directive 91/250/EEC.

#bne: This does not address any problems related to computers or software.

#lai: (e) whether it would be desirable and legally possible having regard to the Community's international obligations to introduce a %(q:grace period) in respect of elements of a patent application for any type of invention disclosed prior to the date of the application.

#fet2: This seems to suggest that Art 52 EPC should be changed and that this directive is in fact not about harmonisation but about changing the rules of patentability.  Problems of legal structure, such as integration with the Community Patent, should be investigated and solved before the directive is passed.  E.g. the %(q:harmonisation) rationale of this directive has been rendered completely void by the advent of the Community Patent and it is time to address this now, as MEP Manders initially proposed, rather than after passing the directive, as COMP-2 suggests, when it is too late to correct anything.

#aet: (f) in what respects it may be necessary to prepare for a diplomatic conference to revise the European Patent Convention, also in the light of the advent of the Community patent;

#qms: It is clear that this is one of the questions which, if anything, merit to be monitored.  However, if performed by the same institution that proposed the directive, monitoring itself may be meaningless and serve to cover up rather than to discover problems.

#mtW: (g) on how the requirements of this Directive have been taken into account in the practice of the European Patent Office and in its examination guidelines.

#opa: Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than [DATE (last day of a month)]. They shall forthwith inform the Commission thereof.

#fty: Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than eighteen months after its entry into force. They shall forthwith inform the Commission thereof.

#ydW: MEP Manders initially called for withdrawal of the directive in amendments 72 and 73, but later agreed with McCarthy on Compromise Amendments 2-3.

#iod: 1. Calls on the Commission to withdraw the directive. For reasons of legal certainty, calls on Member States which are contracting states to the European Patent Convention to examine the possibility of including the patentability of computer-implemented inventions in Article 52 of the European Patent Convention.

#eat: For reasons of legal certainty, calls on Member States which are also contracting states to the European Patent Convention to ensure that practice with regard to the granting of patents pursuant to the European Patent Convention is brought into conformity with the provisions of this directive.

#Wel: Coexistence of two dissociated legal systems means calling for trouble.  As the justification in Amendment 74 says:  %(bq:Legislation forming part of Community law, in respect of which the Court of Justice has jurisdiction and over which the democratically elected Parliament has control, is to be preferred. In view of the consequences of patents for the economy and society, it is important that such matters fall within the competence, and are subject to the control, of a democratically elected Parliament.)  However in the context of the current software patentability directive, this motion could be construed as directed against Art 52 EPC.  It would have been more helpful if Amendment 74 had called on the EU and its member states to enforce the existing and perfectly clear and harmonised rules of Art 52ff EPC (substantive patent law), while solving the problems of concurring legal structures (EPC vs EU) by some other means, such as replacing the EPC with a similar EU-based version.

#fCg: 1b. Calls on the Commission to put forward, within two years, concrete proposals for establishing a Community patent. Calls on the Member States to withdraw from the European Patent Convention following the introduction of such Community legislation.

#Ie3: JURI Amendments 2003/04/08

#nWi: The PDF source file.  One group of MEPs demands reaffirmation of principle that programs are not inventions, another wants to insert program claims into the CEC/McCarthy proposal

#0Cn: JURI 2003/05/13 %(q:Compromise Amendments)

#iyr: Only minor amendments, nothing to clarify what is %(q:technical) or to explain how the patenting of algorithms and business methods, which Arlene McCarthy says she does not want, can be prevented.   As a compromise with Toine Manders, Arlene McCarthy proposes that a revision of the European Patent Convention should be envisaged.

#2md: JURI 2003/05/21 Compromise Amendments 4-5

#aun: Almost meaningless amendment proposals.  An explanation of what a patent claim is and a restatement of the European Commission's competence in the context of monitoring.

#0Cn2: JURI 2003/06/16 %(q:Compromise Amendment 6)

#Inh: JURI Änderungsvorschläge

#oWW: contains links to a short intorductory pamphlet to the JURI discussion. This pamphlet exists in various EU languages

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: juri0304 ;
# txtlang: en ;
# multlin: t ;
# End: ;

