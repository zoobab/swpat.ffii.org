\contentsline {chapter}{\numberline {1}introduction}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}What should be patentable and what should not?}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Four Separate Tests on one Unified Invention: Preserving the Logic of Art 52 EPC}{7}{section.1.2}
\contentsline {section}{\numberline {1.3}Freedom of Publication vs Program Claims}{8}{section.1.3}
\contentsline {section}{\numberline {1.4}Real vs Fake Interoperability Privilege}{8}{section.1.4}
\contentsline {section}{\numberline {1.5}Other Issues}{9}{section.1.5}
\contentsline {chapter}{\numberline {2}Tabular Comparison of Amendment Proposals, Voting Recommendations and Results}{10}{chapter.2}
\contentsline {section}{\numberline {2.1}Legend and Commented Example}{10}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Title}{10}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Title and Recitals}{11}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Title}{11}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Recital 1}{12}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Recital 5}{12}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Recital 8}{13}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Recital 11}{14}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}recital 11 a}{15}{subsection.2.2.6}
\contentsline {subsection}{\numberline {2.2.7}Recital 12}{15}{subsection.2.2.7}
\contentsline {subsection}{\numberline {2.2.8}Recital 13}{16}{subsection.2.2.8}
\contentsline {subsection}{\numberline {2.2.9}recital 13 a}{17}{subsection.2.2.9}
\contentsline {subsection}{\numberline {2.2.10}recital 13 aa}{18}{subsection.2.2.10}
\contentsline {subsection}{\numberline {2.2.11}recital 13 b}{18}{subsection.2.2.11}
\contentsline {subsection}{\numberline {2.2.12}recital 13 c}{19}{subsection.2.2.12}
\contentsline {subsection}{\numberline {2.2.13}recital 13 d}{19}{subsection.2.2.13}
\contentsline {subsection}{\numberline {2.2.14}Recital 14}{20}{subsection.2.2.14}
\contentsline {subsection}{\numberline {2.2.15}Recital 16}{20}{subsection.2.2.15}
\contentsline {subsection}{\numberline {2.2.16}Recital 16a}{21}{subsection.2.2.16}
\contentsline {subsection}{\numberline {2.2.17}Recital 16aa}{22}{subsection.2.2.17}
\contentsline {subsection}{\numberline {2.2.18}Recital 17}{23}{subsection.2.2.18}
\contentsline {subsection}{\numberline {2.2.19}Recital 18}{23}{subsection.2.2.19}
\contentsline {section}{\numberline {2.3}Articles and Motions}{24}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Article 1}{24}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Article 2 (-a)}{25}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Article 2 (-a)}{25}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Article 2 (b)}{27}{subsection.2.3.4}
\contentsline {subsection}{\numberline {2.3.5}Article 2 (ba)}{28}{subsection.2.3.5}
\contentsline {subsection}{\numberline {2.3.6}Article 3}{28}{subsection.2.3.6}
\contentsline {subsection}{\numberline {2.3.7}Article 4}{29}{subsection.2.3.7}
\contentsline {subsection}{\numberline {2.3.8}Article 4.1}{30}{subsection.2.3.8}
\contentsline {subsection}{\numberline {2.3.9}Article 4.2}{31}{subsection.2.3.9}
\contentsline {subsection}{\numberline {2.3.10}Article 4.3}{32}{subsection.2.3.10}
\contentsline {subsection}{\numberline {2.3.11}Article 4.3 (a)}{33}{subsection.2.3.11}
\contentsline {subsection}{\numberline {2.3.12}Article 4 (a)}{34}{subsection.2.3.12}
\contentsline {subsection}{\numberline {2.3.13}Article 4 (a')}{36}{subsection.2.3.13}
\contentsline {subsection}{\numberline {2.3.14}Article 5}{36}{subsection.2.3.14}
\contentsline {subsection}{\numberline {2.3.15}Article 5 (b)}{38}{subsection.2.3.15}
\contentsline {subsection}{\numberline {2.3.16}Article 5 (c)}{39}{subsection.2.3.16}
\contentsline {subsection}{\numberline {2.3.17}Article 5 (d)}{39}{subsection.2.3.17}
\contentsline {subsection}{\numberline {2.3.18}Article 6}{40}{subsection.2.3.18}
\contentsline {subsection}{\numberline {2.3.19}Article 7}{42}{subsection.2.3.19}
\contentsline {subsection}{\numberline {2.3.20}Article 7 (a)}{43}{subsection.2.3.20}
\contentsline {subsection}{\numberline {2.3.21}Article 8 (a)}{43}{subsection.2.3.21}
\contentsline {subsection}{\numberline {2.3.22}Article 8 (c)(bis)}{44}{subsection.2.3.22}
\contentsline {subsection}{\numberline {2.3.23}Article 8 (d)}{44}{subsection.2.3.23}
\contentsline {subsection}{\numberline {2.3.24}Article 8 (e)}{45}{subsection.2.3.24}
\contentsline {subsection}{\numberline {2.3.25}Article 8 (f)}{45}{subsection.2.3.25}
\contentsline {subsection}{\numberline {2.3.26}Article 8 (g)}{46}{subsection.2.3.26}
\contentsline {subsection}{\numberline {2.3.27}Article 9.1}{46}{subsection.2.3.27}
\contentsline {subsection}{\numberline {2.3.28}Legislative Resolution}{46}{subsection.2.3.28}
\contentsline {subsection}{\numberline {2.3.29}Motion for a legislative resolution}{47}{subsection.2.3.29}
\contentsline {chapter}{\numberline {3}Annotated Links}{49}{chapter.3}
