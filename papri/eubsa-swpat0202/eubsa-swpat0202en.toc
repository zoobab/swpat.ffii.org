\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Essence of the Proposals}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}What the EC/BSA proposes in terms of legal doctrine}{4}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Computer programs as such are patentable inventions}{4}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}All practical problem solutions are patentable inventions.}{5}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}The EPO's low non-obviousness standards made obligatory for all of Europe.}{6}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}The EPO rulings of 1998 are not accepted}{6}{subsubsection.2.1.4}
\contentsline {subsection}{\numberline {2.2}What the EC/BSA proposes in terms of software economy}{6}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Some apparent lies}{7}{subsection.2.3}
\contentsline {section}{\numberline {3}The text in BSA, CEC and FFII versions}{9}{section.3}
\contentsline {subsection}{\numberline {3.1}BSA/CEC Advocative Preface and FFII criticism}{9}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}``EXPLANATORY MEMORANDUM''}{9}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}The background to the initiative: Commission's consultations}{11}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}``International competition: The legal situation in the U.S. and Japan''}{16}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}``The current legal situation regarding art. 52(1) and (2) of the epc.''}{18}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}``The role of algorithms''}{20}{subsubsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.6}``Patent and copyright protection are complementary''}{22}{subsubsection.3.1.6}
\contentsline {subsubsection}{\numberline {3.1.7}``The necessity of a Community action harmonising national laws and its legal basis''}{24}{subsubsection.3.1.7}
\contentsline {subsubsection}{\numberline {3.1.8}``The approach adopted''}{24}{subsubsection.3.1.8}
\contentsline {subsubsection}{\numberline {3.1.9}``The legal basis for harmonisation''}{27}{subsubsection.3.1.9}
\contentsline {subsection}{\numberline {3.2}The BSA/CEC Proposal(s) and FFII Counter-Proposal}{34}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}``Financial Statement''\ (BSA only)}{49}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}``The Proposal''\ (Advocative Postface, BSA only)}{50}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Footnotes}{55}{subsection.3.5}
\contentsline {section}{\numberline {4}What to do, how you can help}{58}{section.4}
\contentsline {section}{\numberline {5}Further Reading}{60}{section.5}
