<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: supports amendments from many sides, including most of those from McCarthy and ITRE, even where they contradict each other, but none of the amendments that really limit patentability.  By supporting both program claims and freedom of publication, the effect of this voting list is that program claims are supported and freedom of publication related amendments are rendered %(q:caduc), i.e. not put to vote.  Much of the contradictory nature of these amendments may be due to the fact that ITRE report was written by a liberal (Plooij Van-Gorsel), that program claims were supported by a liberal (De Clercq).  Some national liberal parties (e.g. UK Liberal Democrats, represented in JURI by Diana Wallis who also followed the Manders voting list) have published positions against software patents.  Manders himself seems uninterested in limiting patentability but quite keen on cleaning up the institutional framework of the European patent system.   Manders has recommended some measures for ending the parallelity between EPO and EU.  These recommendations were however followed by very few MEPs even within the liberal group.
title: Liberal Group (Manders) 2003/06/17 Voting List

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: eldr030617 ;
# txtlang: de ;
# End: ;

