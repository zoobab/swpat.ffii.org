# -*- mode: makefile -*-

amccarthy0302.en.lstex:
	lstex amccarthy0302.en | sort -u > amccarthy0302.en.lstex
amccarthy0302.en.mk:	amccarthy0302.en.lstex
	vcat /ul/prg/RC/texmake > amccarthy0302.en.mk


amccarthy0302.en.dvi:	amccarthy0302.en.mk
	rm -f amccarthy0302.en.lta
	if latex amccarthy0302.en;then test -f amccarthy0302.en.lta && latex amccarthy0302.en;while tail -n 20 amccarthy0302.en.log | grep -w references && latex amccarthy0302.en;do eval;done;fi
	if test -r amccarthy0302.en.idx;then makeindex amccarthy0302.en && latex amccarthy0302.en;fi

amccarthy0302.en.pdf:	amccarthy0302.en.ps
	if grep -w '\(CJK\|epsfig\)' amccarthy0302.en.tex;then ps2pdf amccarthy0302.en.ps;else rm -f amccarthy0302.en.lta;if pdflatex amccarthy0302.en;then test -f amccarthy0302.en.lta && pdflatex amccarthy0302.en;while tail -n 20 amccarthy0302.en.log | grep -w references && pdflatex amccarthy0302.en;do eval;done;fi;fi
	if test -r amccarthy0302.en.idx;then makeindex amccarthy0302.en && pdflatex amccarthy0302.en;fi
amccarthy0302.en.tty:	amccarthy0302.en.dvi
	dvi2tty -q amccarthy0302.en > amccarthy0302.en.tty
amccarthy0302.en.ps:	amccarthy0302.en.dvi	
	dvips  amccarthy0302.en
amccarthy0302.en.001:	amccarthy0302.en.dvi
	rm -f amccarthy0302.en.[0-9][0-9][0-9]
	dvips -i -S 1  amccarthy0302.en
amccarthy0302.en.pbm:	amccarthy0302.en.ps
	pstopbm amccarthy0302.en.ps
amccarthy0302.en.gif:	amccarthy0302.en.ps
	pstogif amccarthy0302.en.ps
amccarthy0302.en.eps:	amccarthy0302.en.dvi
	dvips -E -f amccarthy0302.en > amccarthy0302.en.eps
amccarthy0302.en-01.g3n:	amccarthy0302.en.ps
	ps2fax n amccarthy0302.en.ps
amccarthy0302.en-01.g3f:	amccarthy0302.en.ps
	ps2fax f amccarthy0302.en.ps
amccarthy0302.en.ps.gz:	amccarthy0302.en.ps
	gzip < amccarthy0302.en.ps > amccarthy0302.en.ps.gz
amccarthy0302.en.ps.gz.uue:	amccarthy0302.en.ps.gz
	uuencode amccarthy0302.en.ps.gz amccarthy0302.en.ps.gz > amccarthy0302.en.ps.gz.uue
amccarthy0302.en.faxsnd:	amccarthy0302.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo amccarthy0302.en-??.g3n`;source faxsnd main
amccarthy0302.en_tex.ps:	
	cat amccarthy0302.en.tex | splitlong | coco | m2ps > amccarthy0302.en_tex.ps
amccarthy0302.en_tex.ps.gz:	amccarthy0302.en_tex.ps
	gzip < amccarthy0302.en_tex.ps > amccarthy0302.en_tex.ps.gz
amccarthy0302.en-01.pgm:
	cat amccarthy0302.en.ps | gs -q -sDEVICE=pgm -sOutputFile=amccarthy0302.en-%02d.pgm -
amccarthy0302.en/amccarthy0302.en.html:	amccarthy0302.en.dvi
	rm -fR amccarthy0302.en;latex2html amccarthy0302.en.tex
xview:	amccarthy0302.en.dvi
	xdvi -s 8 amccarthy0302.en &
tview:	amccarthy0302.en.tty
	browse amccarthy0302.en.tty 
gview:	amccarthy0302.en.ps
	ghostview  amccarthy0302.en.ps &
print:	amccarthy0302.en.ps
	lpr -s -h amccarthy0302.en.ps 
sprint:	amccarthy0302.en.001
	for F in amccarthy0302.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	amccarthy0302.en.faxsnd
	faxsndjob view amccarthy0302.en &
fsend:	amccarthy0302.en.faxsnd
	faxsndjob jobs amccarthy0302.en
viewgif:	amccarthy0302.en.gif
	xv amccarthy0302.en.gif &
viewpbm:	amccarthy0302.en.pbm
	xv amccarthy0302.en-??.pbm &
vieweps:	amccarthy0302.en.eps
	ghostview amccarthy0302.en.eps &	
clean:	amccarthy0302.en.ps
	rm -f  amccarthy0302.en-*.tex amccarthy0302.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} amccarthy0302.en-??.* amccarthy0302.en_tex.* amccarthy0302.en*~
amccarthy0302.en.tgz:	clean
	set +f;LSFILES=`cat amccarthy0302.en.ls???`;FILES=`ls amccarthy0302.en.* $$LSFILES | sort -u`;tar czvf amccarthy0302.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
