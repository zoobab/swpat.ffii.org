<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: McCarthy 2003-02-19: Amended Software Patent Directive Proposal

#descr: Arlene McCarthy, British Labor MEP appointed by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI) to report on the European Commission's Software Patentability Directive Proposal (CEC/BSA Proposal), suggests that the European Parliament should enact the CEC/BSA version with additional safeguards to align Europe on the US practise and make sure that there can be no limit on patentability.  McCarthy reiterates the CEC/BSA software patent advocacy and misrepresents the wide-spread criticism without citing any of it.  Even economic and legal expertises ordered by the European Parliament and other critical opinions of EU institutions are not taken into account.  McCarthy's economic argumentation consists of tautologies and unfounded assertions, such as that companies like Ericsson and Alcatel need software patents to finance their R&D, that SMEs need european software patents in order to compete in the USA, that patents are needed to keep developping countries at bay.  McCarthy uses the term %(q:computer-implemented inventions) as a synonym for %(q:software innovations).  These %(q:by their very nature belong to a field of technology).  McCarthy insists that %(q:irreconcilable conflicts) with the EPO must be avoided.  McCarthy says she wants to %(q:set clear limits as to what is patentable) -- and that she wants to avoid the %(q:sterile discussions) about %(q:technical effects) and %(q:exclusions from patentability).  Yet her proposal stays confined to such discussions.  McCarthy demands that all useful ideas, including algorithms and business methods, must be patentable as %(q:computer-implemented inventions).  McCarthy proposes to recognise the EPO as Europe's supreme patent legislator and to make decisions of a few influential people at the EPO irreversible and binding for all of Europe.

#dWs: Contradictons on technicity of %(q:computer-implemented inventions)

#ite: Economics from a US Corporate Patent Department's Perspective

#iWg: He who diggeth a Pit ...

#aWc2: Disregarding all but one %(q:economic study)

#lrh2: Algorithm Untechnical, Program Technical?

#tto: Patents for non-technical solutions

#iWi: %(q:Rationale behind the Directive) remains obscure

#orn: Apologetics for Violation of Law

#sht: Denying the Parliament Its Right to Set the Rules

#mWt: Shall %(q:computer-implemented inventions) generally %(q:belong to a field of technology) or not?  McCarthy contradicts herself on this question.

#eeo: The CEC/BSA proposal answers the question affirmatively in Art 3, but McCarthy demands that this passage should be removed, because it could lead to unpredictable results or to a widening of the scope of patentability.

#ktf: Computer-implemented inventions as a field of technology

#iWn: This article is unnecessary and unclear in scope.  It would be difficult to put into effect, and might lead to unpredictable results.  It might be construed as extending the scope of patent protection.

#dre: On the other hand McCarthy introduces an amendment to the preamble reintroduces the criticised provision and declares it to be self-evident:

#amendment: Amendment

#rgg: Recital 12

#origvers: original version

#erh: Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#amendvers: amended version

#lcr: Accordingly, even though  a computer-implemented invention belongs by virtue of its very nature to a field of technology, it is important to make it clear that where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable

#tce: Thereby McCarthy reintroduces through the back door the very risks which she criticised before.

#utW: McCarthy suggests that we have no choice but to follow the US model:

#cde: The impact on small and medium-sized software developers

#tnh3: European business does not operate in a vacuum. Computer-implemented inventions are increasingly important, yet many of the 20,000 patents for software-related patents already granted in Europe are in non-European hands. Indeed, we would do small and medium-sized European software developers a disservice if we were either to leave matters as they stand, or if we were to attempt to ban all patents for such inventions, thus potentially putting our software developers at a disadvantage when they seek to compete in the US.

#edk: Indeed >75% of the software patents which the EPO has so far granted %(fs:are in non-European hands).  It is difficult to see why confirming the invalidity of these patents could put European software developpers at a disadvantage when they compete on the US battlefield.  In order to battle in the US, European companies must obtain US patents, and they can do so independently of whether similar patents are available in Europe.  On the other hand, as long as Europe outlaws software patents, US companies will not be able to use such patents in Europe.  Among the big US software patent owners, who might like to extend their supremacy to Europe, are some global players of European origin, such as %(LST).  %(s:These US-centered global players, not European SMEs, appear to be the beneficiaries to whom McCarthy is asking us not to %(q:do a disservice).)

#aWc: For her economic argumentation, McCarthy consistently takes a the perspective of US-centered global players:

#mmr: The economic importance of patentability of computer-implemented inventions for European industry.

#cWc: Although no consolidated data seem to exist concerning royalties for patents paid in Europe, the important thing about patents as far as companies are concerned is the protection of their R&D investments. Ericsson files more than 1,000 patents every year and almost all of them are computer-implemented inventions. Nokia estimates that 60-95% of their patent applications are for such inventions, whilst Alcatel estimates that 60 % of their inventions are for computer-implemented inventions and that the trend is upwards. In order to value how important patent protection is for a company, it is not unusual for companies with major R&D programmes to measure their patenting (internal) costs as a percentage of their R&D spend.  Some companies spend as much as 5-10% of their R&D on patents. This means that companies with substantial software-related R&D can be estimated to be spending as much as perhaps 10% of their overall R&D budgets on patenting.  Moreover, academic studies have shown a link between R&D spending, patent applications and productivity.

#eao: In other words: %(ol|Those who own patents are more productive than those who depend on the goodwill of their patent-owning competitors.|This amazing correlation has been corroborated by unspecified economic studies.)

#WfW: McCarthy does not ask whether software patents promote innovation in the industry as a whole.

#sqa: %(s:This question has been %(ss:answered) sceptically or negatively by those economists who have endeavored to study it).

#skW: %(bm:Bessen and Maskin) from MIT even find a direct negative correlation between software patents and productivity:

#Whe: some of the most innovative industries today -- software, computers and semiconductors -- have historically had weak patent protection and have experienced rapid imitation of their products.

#yrd: %(s:Far from unleashing a flurry of new innovative activity, these stronger property rights ushered in a period of stagnant, if not declining, R&D among those industries and firms that patented most.)

#Wss: Jim Warren, board member of %(s:Autodesk Inc), was one of a majority of representatives of large US software companies who %(jw:testified) against software patents in 1994:

#TWn: There is absolutely no evidence, whatsoever -- not a single iota -- that software patents have promoted or will promote progress.

#TkW: The company for which I am speaking, Autodesk, holds some number of software patents ... However, all are defensive -- an infuriating waste of our technical talent and financial resources, made necessary only by the lawyer's invention of software patents.

#tme: Robert Barr, head of the IPR department of Cisco, a leading process innovator who produces as many software patents each year as Alcatel and Ericsson, %(rb:responded) to hearings of the US Federal Trade Commission in 2002:

#Mti: %(s:My observation is that patents have not been a positive force in stimulating innovation at Cisco). Competition has been the motivator; bringing new products to market in a timely manner is critical.  Everything we have done to create new products would have been done even if we could not obtain patents on the innovations and inventions contained in these products. I know this because no one has ever asked me %(q:can we patent this?) before deciding whether to invest time and resources into product development.

#Tne: The time and money we spend on patent filings, prosecution, and maintenance, litigation and licensing could be better spent on product development and research leading to more innovation.  But we are filing hundreds of patents each year for reasons unrelated to promoting or protecting innovation.

#Mea: Moreover, stockpiling patents does not really solve the problem of unintentional patent infringement through independent development.  If we are accused of infringement by a patent holder who does not make and sell products, or who sells in much smaller volume than we do, our patents do not have sufficient value to the other party to deter a lawsuit or reduce the amount of money demanded by the other company.  %(s:Thus, rather than rewarding innovation, the patent system penalizes innovative companies who successfully bring new products to the marketplace and it subsidizes or  rewards those who fail to do so.)

#ior: Barr is speaking from experience with traditional telecommunication companies who lost the router market to the fast-moving newcomer Cisco Inc and later tried to strike back with their huge patent portfolios.

#sau: A %(ct:%(s:report by the European Commission on recent problems in the telecommunications industry)) strongly warns about the adverse effect of the %(q:patent race) on this sector and explains that the patent race is a result of the extension of patentability to software.  %(s:Even %(LST) themselves have complained that the burden of the patent game diverts ressources from the innovation process):

#eWa: Like other companies operating in the telecommunications industry, we experience frequent litigation regarding patent and other intellectual property rights. Third parties have asserted, and in the future may assert, claims against us alleging that we infringe their intellectual property rights. Defending these claims may be expensive and divert the efforts of our management and technical personnel. ... We remain dependent in part on third party license agreements which enable us to use third party technology to develop or produce our products. However, we cannot be certain that any such licenses will be available to us on commercially reasonably terms, if at all.

#eWW2: Ericsson has just settled a patent licence deal with Interdigital to pay 50.000.000 EUR until 2006 to avoid a threatened billion dollar patent lawsuit.

#tlt: Note also that

#nsk: McCarthy uses the term %(q:computer-implemented inventions) as a synonym for %(q:software ideas): they are the fruit of %(q:software-related R&D efforts).   Although McCarthy says elsewhere that the term %(q:computer-implemented inventions) refers to %(q:such devices as mobile phones, intelligent household appliances, engine control devices, machine tools ...), when she actually uses the term she refers to pure software, i.e. calculation rules which become patentable %(q:because computer-implemented inventions by their very nature belong to a field of technology).

#nWt: McCarthy implicitely requires that software companies should spend about 10% of their R&D budget on patenting.  It will have to be more for SMEs, given that the cited Ericsson and Alcatel can profit from economy of scale.  All telecom giants are active in the US and look at matters from a US patent owner's perspective.  Most european software jobs are created by SMEs which are not active in the USA and which do not patent.  %(s:McCarthy is in fact proposing to impose an extra tax-like burden of more than 10% on each software development job in Europe.)  This does not yet include the famous %(ib:IBM tax) or %(q:Microsoft tax) and the various patent extortion and rent-seeking activities, as we currently observe them in the US.

#nce: McCarthy proposes a new economic rationale for software patentability:

#mty: Let us summarise McCarthy's reasoning:

#WsW: At present, there is a trend in the traditional manufacturing industry to shift operations to low-cost economies outside the European Union.

#rla: This is easier to accomplish when traditional manufacturing processes are patentable.

#Pro: Proof:

#sxi: %(q:Self-evident).

#rrW: A company will be more productive when it owns patents than when it is at the mercy of a patent-owning competitor.  This amazing correlation has been corroborated by unspecified studies.

#Wnu: Needless to say: whatever applies to patents of traditional manufacturing industries also applies to software copyright and to software patents.

#eWW: This %(q:outsourcing needs patents) argumentation is %(fe:frequently heard in the patent law community).   It provokes a number of questions, such as:

#oie: Do patents really play a significant role in facilitating shifts of operations to %(q:low-cost economies)?

#ene: %(s:Far from being %(q:self-evident), this assumption is at odds with evidence from various %(es:economic studies)), see the list below.  According to these studies, patents are only one of many means of harvesting returns from R&D activities and in particular they contribute %(q:at best nothing) to fostering innovation in SMEs.

#Wla: Is it an official policy of the EU to exclude developping countries outside Europe from using new ideas?

#tWs2: It should be noted that countries like Poland and Portugal have a share of 0,00% of the 30000 software patents granted by the EPO so far.

#tcp: Who fill really fall into the pit that we are told to dig?

#urn: Currently not only manufacturing but also a lot of intellectual activity and R&D activity is shifted to countries outside the EU, some of which already own a lot more EPO software patents than Europe's %(q:low-cost economy) regions.  The european telecom giants which McCarthy places at the center of her considerations have many R&D labs in China.  Some have been talking about transferring their headquarters to China.  %(s:If company headquarters can move to low-cost economies, so can patent ownership, especially in the case of software.)  Unlike industrial labor, software development is an intellectual activity at the most abstract possible level.  There is no %(q:manual labor) or %(q:routine coding) to outsource.  %(s:Software development and software patents will tend to be in one hand.)  The only part of the software sector that is fairly immune to the outsourcing trend is that of services, often connected to free/open-source software.  Under a system of European software patents however, even that part of the european software business risks to become subject to foreign appropriation.

#oWs: Why is the pro patent camp trying to play with protectionist reflexes?

#ucs: %(s:Could it be that a true economic rationale for software patents simply cannot be built?)  That there is simply no citable study that could provide one?

#rds: Now where are the mysterious %(q:studies) which McCarthy cites several times?

#WiW: McCarthy seems to offer only only one hint

#rhe: Moreover, a study conducted by the Intellectual Property Institute in London has found that %(q:the patentability of computer-related inventions has helped the growth of computer program-related industries in the US, in particular the growth of small and medium enterprises and independent software developers into sizeable indeed major companies).

#rWh: %(s:The facts and arguments listed by the %(IPI) do not support such a conclusion).  Rather, they could be correctly summarised as follows:

#WWi: Individual (software) comanies can benefit from software patents.  %(s:There is no evidence that this benefit for individual market participants corresponds to a benefit for the economy or society as a whole.)  Many economists have doubts about such a benefit.  These doubts are supported by continuing and growing concern about software patents in the USA.

#sWf: The %(ip:IPI study) does contain a statement which resembles that of McCarthy, see page 5 (pg 7 in the pdf file):

#Wzt: Let us first summarize:

#Wdr: McCarthy quotes only the claimed advantage %(q:1.) of software patentability, whereas the IPI study itself lists %(tp|one advantage|%(q:1.)) and %(tp|three disadvantages|%(q:2.1-2.3)).

#adi: %(s:The IPI study fails to cite any evidence in favor of the claimed advantage) (%(q:1)) of software patentability, although it contains 10 pages of bibliography.

#got: It remains unclear in what way the claimed %(q:growth of some SMEs into large companies) can, even if examples for it are found, be an argument for or against anything.   Companies such as Philips, Ciba-Geigy, SAP and Microsoft grew big in patent-free environments.  Companies grow and die, no matter what the conditions of the market game are.

#eFe: On page 8 (pg 10 of the PDF file) the IPI study then writes

#h3t: This is apparently a reference to page 32 (pg 34 of PDF file) of the study:

#WoW: The economics literature does not show that the balance of positive and negative effects lies with the negative. All it says is that there are grounds for supposing that the negative forces are stronger relative to the positive forces in this area than in some others and that any move to strengthen IP protection in the software industry cannot claim to rest on solid economic evidence.

#aWt: It should be noted that the economic literature does not show that the balance of effects lies within the positive, either for software or for technical inventions in the traditional sense.  %(s:Leading economic studies suggest that the patent system as a whole is harmful.)  Canadian and Australian governmental reports of the 1970/80s have proposed to abolish the patent system or, as a compromise, %(mv:to prevent its expansion into further fields such as software, where the balance was assumed to be particularly negative.)

#nwd: The author of the economic chapter of the study, Peter Holmes, explained at a conference in june 2002 in reply to irritated questions of fellow economists approximately as follows: %(q:In that study I had to find some macro-economic arguments in favor of software patents.  %(s:My two co-authors were patent lawyers.  For them it was a matter of conviction.)  Without arguments in favor of software patents, there would have been no way to finish the study).  The leading author was in fact %(RH), a deeply committed advocate of software patentability with a %(cs:well-documented record of distorting facts to suit convictions).   Hart was also closely connected to the %(uk:British group at the European Commission's Industrial Property Unit) (Indprop) that initiated the whole Software Patent Directive project.  It was also this group that ordered this legal study from Hart and then, in view of rising demands to provide an economic rationale, dubbed it as an %(q:ecnomic impact study).   Yet, %(s:because it failed to provide the desired economic rationale for their directive project, the CEC patent law drafters kept the study unpublished for 6 months.)  Nevertheless, today the proponents of the CEC softpat directive draft frequently quote one sentence from this one %(q:economic study), which is really a legal study.  This is because the numerous real %(es:economic studies), provide even less material to support their pro-software-patent convictions.

#Wph: Here is a list of a few pertinent monographies and expertises which have in common that they (1) judge the economic impact of patents rather unfavorably (2) are not mentioned in McCarthy's draft report:

#hWd: Arlene McCarthy's draft contains the following Amendment Proposal

#cal: Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention.  Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem.  However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.

#uft: Justification

#pep: Article 52(2)(a) and (c) of the European Patent Convention precludes the patentability of %(q:mathematical methods) and %(q:schemes, rules and methods for performing mental acts, playing games and doing business, and programs for computers). Since an algorithm could be a computer program or an element of such a program in isolation from its execution environment or a mathematical formula or method, it is, as such,  precluded from patentablity.  However, the mere use of an algorithm does not preclude patentability.

#lis: According to the justification, the regulation about algorithms [ as such ] made in this recital should also apply to %(q:programs for computers) [ as such ], i.e. algorithms applied to generic data processing equipment.  Generic data processing equipment, also known as %(q:Turing machine), %(q:Von Neumann Machine) or %(q:Universal Computer), is the realisation of an abstract model by which algorithms are expressed in their most general manner.

#WCn: The %(q:computer-implementation) of algorithms is trivial.  If anything in computing could be sufficiently inventive in order to be considered eligible for patenting, it is the algorithm.  Between an algorithm and its representation in terms of generic computing equipment, there is nothing to patent.  Claiming an algorithm in the verbal clothing of generic computing equipment is equivalent to claiming the algorithm itself.  The only way in which an algorithm can be excluded from patentability is by not allowing its execution on generic computing equipment to be claimed.  Which is precisely what Art 52 EPC, as quoted here, does.

#lrn: Unfortunately the proposed regulation does the opposite of Art 52 EPC: %(s:McCarthy says that algorithms are patentable).  The only condition for patentability according to McCarthy is that the algorithm must be claimed by reference to something tangible, such as the universal computer on which all algorithms are normally modelled.  This condition does not even exclude a single algorithm from patentability.  It pretends to have a restricting intention but in fact stipulates a principle of unlimited patentability of algorithms.

#iee: When assessing whether an inventive step is involved, it is usual to apply the problem and solution approach in order to establish that there is a technical problem to be solved. If no technical problem is present, then the invention cannot be considered to make a technical contribution to the state of the art.

#Osa: Even at the EPO, it is always said that there must be a %(q:technical solution to a technical problem).  If applications fail on technicity, then they fail because there is no technical solution.  A %(q:technical problem) (e.g. task description referring to physical entities) is always easy to construct.

#WWo: McCarthy is thus in fact stating that %(s:even untechnical solutions are patentable), thereby inserting another layer of safeguard to make sure that there can be no way to limit patentability.

#iot: McCarthy does this by transferring internal EPO examination methodologies (formalisms) into european law.  Not methodologies but results should be of interest for the legislator.  The EPO's examination methodologies have moreover been regularly criticised for their incomprehensibility from all sides.   Advocates of software patentability have %(rn:pointed out) that, while the EPO's methods produce the desired result (i.e. grant software patents but leave a backdoor for arbitrarily refusing some of them in case of emergency), these methodologies are the cause of %(q:unsurpassable dogmatic chaos), due especially to the EPO's method of mixing the technical invention test into the non-obviousness test.  This is incompatible with Art 52 EPC, with the directive's aim of %(q:clarification) and with the subsidiarity principle.

#kil: McCarthy makes quite presumptuous promises about what her proposal is to achieve:

#WoW2: Copyright protection is considered to have limitations as a means of protecting more than the actual coding of a computer program and there are misgivings lest patent protection should lead to patents being granted for inventions which do not satisfy the traditional criteria.  The proposal for a directive as amended by the rapporteur resolves this dilemma reasonably and subtly.

#eex: The problem of %(q:copyright not protecting enough) has been hyped by patent lawyers.  %(s:Copyright on textbooks is far less effective than software copyright, and yet nobody seems to be demanding that %(q:underlying ideas) of book-writing must be patentable.)  Also, the %(q:misgivings) are not about %(q:software inventions which do not satisfy the traditional criteria) -- in fact no software innovation can satisfy the criterion of %(q:technical invention) as laid out in Art 52 EPC -- but mainly about %(s:the daily experience that software patents, as granted by the patent offices, are broad and trivial, and that, once the claims are narrowed down to a reasonable scope, software patents are no longer anything more than a prohibitively expensive form of software copyright.)

#tec: McCarthy is misrepresenting the criticism and creating an artificial %(q:dilemma).  Let us see how %(q:reasonably and subtly) she %(q:resolves) this %(q:dilemma):

#ahn: 3. The rationale behind the directive and the need for a strict definition of patentability

#rrn: Practice to date at the EPO has evolved over a succession of decided cases in the direction of what some consider to be a liberalisation of the criteria for patentability, as a result of which they will now grant patents for computer-implemented inventions provided they make a %(q:technical contribution).

#tnb: %(q:What some consider to be) a violation (not %(q:liberalisation)) %(q:of the criteria for patentability) is that %(s:patents have regularly been granted for %(q:computer-implemented inventions) which are nothing but %(q:programs for computers) presented in claim language, and whose %(q:technical contribution) is only claimed, not actually %(q:made).)

#WWi2: However, this has resulted in the complaint that too many applications for patents for computer program patents are for trivial inventions or make an insufficient contribution in relation to the state of the art and that examination of these questions tends to take second place to %(q:the rather sterile and philosophical issue of whether or not the alleged invention confers a 'technical effect').

#otC: The %(q:complaints) about %(q:insufficient contributions) to the %(q:state of the art) are not directed to %(q:patent applications) but to patents which the EPO has actually granted.  The triviality (and breadth) of the claims is closely related to the lack of technical character (i.e. abstractness, lack of necessity to conduct experiments with forces of nature).  Why McCarthy considers the technicity question a %(q:sterile issue) remains unclear.  %(s:McCarthy's proposed legislation is completely based on the %(q:sterile issue) of technicity in a dozen flavors, such as %(q:technical character), %(q:technical contribution), %(q:technical effect), %(q:technical problem), %(q:field of technology), ..., without a single definition.)

#emn: Far from being radical, the Commission's proposal - which the rapporteur endorses whilst seeking to tighten it up further - aims to counter any extension of the scope of patent protection for software while resisting the call to exclude patent protection altogether.

#WCt: This statement (and the following ones) fails to offer any hint as to how the Commission's (and the rapporteur's) proposal addresses the %(q:complaints ... of some).

#lne: Indeed, the proposal for a directive sets out to avoid irreconcilable conflict with established practice at the EPO, while %(q:subtly changing the nature of the investigation ... from the sterile one of exceptions into one of obviousness) thus answering %(q:one of the major criticisms of most computer-implemented inventions), while retaining the criterion of %(q:technical contribution). Thus it focuses on whether claims are for bona fide inventions.

#chs: %(s:Does %(q:sets out to avoid irreconcilable conflict with established practice of the EPO) mean that the practise of the EPO is not put in question?)  How can %(q:major criticisms of most computer-implemented inventions) (i.e. criticism of the established practise of the European Patent Office?) be addressed if the practise of the EPO must stay untouched?  By %(q:subtle changes)?  Which?  By avoiding %(q:sterile discussions of exceptions) and at the same time limiting the proposal to such discussions?

#rrl: The rapporteur's amendments would also very clearly exclude the grant for patents for non-inventive business methods. As a result, the directive would not lead to patents being granted for otherwise unpatentable business methods simply because use of a computer is specified in the claims.

#iss: Excluding patents on %(q:non-inventive business methods) is a tautology.  Of course patents can't be granted for anything %(q:non-inventive).  And, needless to say, %(q:non-inventive) business methods do not become patentable %(q:simply because use of a computer is specified in the claims).  In other words, %(s:McCarthy is proposing that [ inventive ] business methods, be they computerised or not, should be patentable.)

#TWr3: More importantly, %(s:business methods only account for a small part of the %(q:complaints .. of some).)  The larger and more essential part is about software.  Both computer programs and business methods are, as such, however %(q:inventive) they may seem, not inventions in the sense of Art 52 EPC.  Both computer programs and business methods can however be part of the implementation of a technical invention.  The disputed question is: what is this %(q:technical invention) or %(q:technical contribution)?  Does the proposal explain it clearly?  McCarthy's %(q:rationale) section heading promises an explanation, but the body fails to deliver on the promise.

#iro: In her final observations, McCarthy hints %(s:why she does not want any clarification) of the concept of %(q:technical invention) in the proposed directive:

#een: As regards some specific amendments put forward in the other committees, the rapporteur considers that she should make two specific observations.  First, the test laid down in the Rote Taube case antedates the European Patent Convention, but it is significant  that the drafters of the Convention chose not to include it as part of the definition of patentable subject-matter.

#WaC: Who in the European Parliament understands terms such as %(q:Rote Taube test)?  Why does McCarthy not directly quote the CULT amendment proposals, which define the %(q:technical invention) by reference to %(q:forces of nature)?  Why is McCarthy trying to hide this central question from the eyes of fellow MEPs?

#sut: The %(s:technical invention test) is pervasive in the written law and caselaw of many countries througout the world, but certain german decisions such as %(q:Rote Taube) (red dove, genetics case of 1968) and %(dp:Dispositionsprogramm) (disposition program, software case of 1976) are most often cited due to the depth of the generalisations made therein.  The %(e:technical invention) is accordingly defined as %(e|problem solution involving forces of nature) or %(e|teaching of plan-conformant action using controllable forces of nature to achieve a causally overseeable result without mediation by human reason.)

#tls: The framers of the European Patent Convention chose not to include a positive definition of %(q:technical invention). They do not use the word %(q:technical) at all. Instead they provide list of items which are not considered to be [ technical ] inventions.  However some people, including our rapporteur, say that this has not been clear enough, which is why they want a clarifying directive.

#oti: The %(cp:CULT proposal) tries to fix the problem by proposing both a negative and a positive definition of technology:

#rWh2: The use of natural forces to control physical effects belongs to a technical field. The processing, handling and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.

#sWW2: Similar statements are found in the %(ia:ITRE amendments).

#WWa: By contrast, McCarthy's draft proposes neither a positive nor a negative definition.

#aiW2: One could summarise the three approaches to defining %(q:technical invention) in the following table

#rps: Proposal

#gWi: negative definition

#sWi: positive definition

#itl: McCarthy justifies the complete lack of definitions in her proposal as follows:

#eja: The imposition of a specific interpretation of that test must be rejected, as it would not be relevant to all inventions or appropriate in all situations.

#Wtr2: We do not understand

#crW2: Which %(q:specific interpretation) of the traditional invention test do CULT/ITRE %(q:impose)?

#xoc: Which %(q:unspecific interpretation) of the test would McCarthy favor?

#ets: To which %(q:inventions) is the traditional invention test %(q:not relevant)?  In which situations is it %(q:not appropriate)?

#lxW: McCarthy's remark makes sense if we decrypt it as follows: %(bc|%(q:specific interpretation) = %(q:definition)|%(q:not appropriate) = %(q:undesirably strict).)  Far from %(q:further tightening up) the European Commission's laxist proposal, McCarthy consistently resists all demands to assign central terms such as %(q:invention), %(q:technical) and %(q:industrial) any meaning, be it by a positive or by a negative definition.

#nlv: McCarthy even proposes to deliberately make the term %(q:invention) ambiguous:

#ipW: Article 2, point -a (new)

#otb: %(q:invention) encompasses both patentable inventions and matter whose patentability has not been established or is in question.

#cvs: It should be clarified that the term %(q:invention) is used in both senses in the Directive.

#trW: Classical patent law textbooks such as %(LST) use the term %(q:invention) only in the sense of %(q:patentable invention) = %(q:technical invention).  The CEC/BSA proposal has created confusion by implicitely assigning the word a double meaning.  McCarthy wants to %(q:clarify) that this confusion is not a bug but a feature.  %(s:Far from keeping her promise of %(q:further tightening up) the CEC/BSA proposal, McCarthy only further loosens a proposal that did not limit patentability in the first place.)

#uRW: The introduced ambiguity closely colludes with %(fp:ongoing efforts of the US government and international patent lawyer lobbies at WIPO) to ensure that Europe can not use the concept of %(q:technical invention) as an independent filter on patentability under Art 27 TRIPs.

#Cyt: McCarthy writes:

#tjs: It should first be pointed out that computer-implemented inventions cover such devices as mobile phones, intelligent household appliances, engine control devices, machine tools and computer program-related inventions.

#pWi: Why doesn't McCarthy say %(q:mobile phone-related inventions, ..., machine tool-related inventions and computer program-related inventions)?  Why not %(q:mobile phones, ..., machine tools and computer programs)?  Why this aymetry?  Why this special care to veil the %(q:computer-programs) in a term that implies their patentability, and to hide them behind an enumeration of items whose patentability is uncontested?  Upon close reading of the whole text we find that %(q:computer programs) are precisely what McCarthy means when she actually uses the term %(q:computer-implemented inventions).  Quite naturally so: the term %(q:computer-implemented inventions) can be transformed to %(q:problem solutions that are implemented by using a general-purpose computer), which again is the same as %(q:programs for computers [ in the context of patent claims ]).

#oct: %(s:By lumping together the non-patentable %(q:programs for computer) of Art 52 with various patentable items into one misleadingly named category, McCarthy is once again deliberately introducing ambiguity) rather than %(q:setting clear limits as to what is patentable).  McCarthy seems to need such a tactic in order be able to argue that the EPO's current practise is perfectly legal and faithful to the original understanding of the EPC:

#lor: The proposal under consideration is not revolutionary. The patenting of computer-implemented inventions is not new. Indeed, patents involving use of software have been applied for and granted since the earliest days of the European patent system and it is now estimated that 15% of all applications for patents received by the EPO relate to computer-implemented inventions.

#lrg: Indeeed, McCarthy's category of %(q:computer-implemented inventions) contains many items that have been patentable since 1978 and before.  Statistics about this category are therefore meaningless.  As can bee seen from the %(fs:FFII statistics), which are more pertinent to the subject under discussion than the EPO statistics quoted by McCarthy, the number of program-related patent applications was very low under the %(ep:EPO's Exmination Guidelines of 1978) and suddenly soared in 1989, 3 years after a first set of revolutionary decisions of the EPO's Technical Boards of Appeal, taken on the basis of new softened Guidelines.  However these patents were still hypothetical and %(bs:prima facie not directed to computer programs).  Another set of %(rd:revolutionary EPO decisions in 1998), connected to the launching of the EU Software Patent Directive project, sent the software applications skyrocketing again in 2001.

#nmw: It is simply not true that patents are not at present applied for and granted for software-related inventions in Europe, as witness the figures set out in section. This fairly widespread misapprehension springs from the express exception for computer programs as such in the European Patent Convention and national statute law.

#uif: Nobody contests that software patents are %(q:at present applied for and granted in Europe).  But caselaw is not law, and state powers may violate the law.  This is why we have a separation of powers.  We need a vigilant legislator to keep an eye on the administrative and judicative powers.  140,000 signatories of a petition share what McCarthy calls a %(q:widespread misapprehension) and have asked the European Parliament to act as a vigilant legislator.   %(s:The %(q:widespread misapprehension) is also %(is:shared by famous law scholars and some national lawcourts).)  The german Federal Patent Court is regularly rejecting EPO patents on the grounds that they are directed to programs as such.  The editor of the german patent lawyer association's journal, PA Reimar König, exclaimed in an article: %(bq:%(s:By its interpetation of the word %(q:as such), the EPO is doing violence to the law.))  Prof. Michel Vivant, who spoke at the European Parliament's expert hearing, writes:

#Eee: %(s|In reality, the national and EPC rules are clear:  they stipulate without ambiguity a principle of non-patentability of software.  The game which is being played today consists in twisting these rules one way or another), e.g. by imagining to consider, as we have seen, the totality of software and hardware as a virtual machine which is potentially patentable (tomorrow ...).  From that point on one can speak about software in patent language.  %(s|The patents which may be obtained this way, by this channel or by another, however still do not have any value) beyond what we lend to them -- but of course it is possible that they will finally acquire a value simply through an informal consensus to stop discussing the question.  In fact, the efficiency of this twisting of rules of law is largely dependent on whether a consensus evolves to take for granted -- against the rules of written law -- that we will play this game or not.  This question is no longer a legal question in the strict sense of the term.

#Wwi: When considering the law textbooks and examination guidelines of the 1970s and 80s, the matter becomes even clearer.  The %(q:game) with Art 52 EPC has been played for quite a while now, and a widespread %(q:informal consensus to stop discussing the question) was about to be established by the Greenbook of 1998.  But this consensus suddenly broke down when more people outside of the patent community started paying attention.

#tea: McCarthy argues the EPO's case as follows

#Wix: In fact, what the EPC says is that computer programs %(q:as such) are not patentable, which is reasonable and justified because a computer program %(q:as such) is protected by copyright.

#vss: The EPC says nothing about copyright nor does it give any justification for constructing an entity called %(q:computer program as such) and assigning it the meaning %(q:copyrightable aspects of a computer program), which is what McCarthy appears to be doing.  Likewise one could try to claim that %(bq:Art 52 EPC excludes aesthetic creations %(q:as such) from patentability.  This applies only to individual musical pieces as copyrighted works, not to %(tp|composition techniques|loudspeaker-implemented inventions).)  %(ee:Such an understanding of Art 52 EPC runs counter to all established methods of law interpretation.)  The reasoning of Art 52 EPC is by no means mysterious.  It is easily understood in a commonly used sentence context, e.g.: %(bq|You can get a patent for a new chemical reaction that is carried out under program control, but not for the program as such.  The invention must consist in something beyond programming logic: a problem-solution involving forces of nature.)

#ytf: McCarthy goes on to belittle the effect of copyright:

#nWn: What copyright does is protect the expression, the actual lines of code written by programmer.  What it offers is the right to prohibit the copying or commercialisation of that code. It is simple to obtain and long lasting and perfect protection against piracy (unauthorised copying and distribution of copies).

#tWs: Copyright includes the right to prohibit derivative works and crude imitation (plagiarism).  Software copyright is usually rather difficult to circumvent.  Competitors have to write and debug from scratch.  The second author has to do approximately the same amount of work as the first author.

#Wff: But copyright does not protect the ideas underlying software, what the software does within a machine, or how a machine under software control interacts with its environment. If such a process were to involve the solution of a technical problem in an inventive way (that is, in a way which is new and not obvious to a skilled person), then a patentable invention would be present. This is what is meant by a computer-implemented invention. The grant of a patent for such an invention is completely consistent with the normal principles of European patent law.

#tar: %(s:No sensible person, not even in the patent system, wants to %(q:protect the ideas).)  Software Copyright clearly stipulates that %(q:underlying ideas) should not be %(q:protected).  As far as software ideas can be framed as patent claims, they are algorithms.  Algorithms cover a very broad range of creative uses and are, even in McCarthy's words, %(q:inherently non-technical).  If the legislator of 1973 had meant the innovative aspects of computer programs to be patentable, he would hardly have written into the law that %(q:mathematical methods, ... , plans and rules for mental acitivity, games and programs for computers, presentation of information) etc are as such not patentable inventions.

#ooe: McCarthy does not limit herself to justifying the EPO's action as %(q:allowable under the current law).  She goes one step further and suggests that they are %(q:required by law):

#int: It would be wrong to discriminate against software developers by refusing them the patent protection available to other inventors when all the conditions for patentability are present.

#ciW: Couldn't it also be %(q:wrong to discriminate against drug developpers by refusing them the freedom of creative imitation which is available to inventors in other fields such as music, financial services and programming)?

#tWp: The answer can only be obtained by a careful deliberation of the overall interests of economic policy.  The patent system is not designed for creating opportunities of ownership for all ideas which can not otherwise be owned, but for stimulating knowledge production in one specific area of human endeavor, that of the %(q:technical invention). As the German Federal Court of Justice writes in its %(dp:Dispositionsprogramm Decision) in 1976:

#Sai: However in all cases the plan-conformant utilisation of controllable forces of nature has been named as an essential precondition for asserting the technical character of an invention.  As shown above, the inclusion of human mental forces as such into the realm of the forces of nature, on whose utilisation in creating an innovation the technical character of that innovation is founded, would lead to the consequence that virtually all results of human mental activity, as far as they constitute an instruction for plan-conformant action and are causally overseeable, would have to be attributed a technical meaning.  In doing so, we would however de facto give up the concept of the technical invention and extend the patent system to a vast field of achievements of the human mind whose essence and limits can neither be recognized nor overseen.

#Dnu: Also from a purely factual point of view the concept of technical character seems to be the only usable criterion for delimiting inventions against other human intellectual achievements, for which patent protection is neither intended nor appropriate.  If we gave up this delimitation, there would for example no longer be a secure possibility of distinguishing patentable achievements from achievements, for which the legislator has provided other means of protection, especially copyright protection.  The system of German industrial property and copyright protection is however founded upon the basic assumption that for specific kinds of intellectual achievements different specially adapted protection regulations are in force, and that overlappings between these different protection rights need to be excluded as far as possible.  The patent system is also not conceived as a reception basin, in which all otherwise not legally privileged intellectual achievements should find protection.  It was on the contrary conceived as a special law for the protection of a delimited sphere of intellectual achievements, namely the technical ones, and it has always been understood and applied in this way.

#Ene: Any attempt to attain the protection of intellectual achievements by means of extending the limits of the technical invention -- and thereby in fact giving up this concept -- leads onto a forbidden path.  We must therefore insist that a pure rule of organisation and calculation, whose sole relation to the realm of technology consists in its usability for the normal operation of a known computer, does not deserve patent protection.  Whether it can be awarded protection under some other regime, e.g. copyright or competition law, is outside the scope of our discussion.

#cyh: One way to make people accept an unattractive proposal is by persuading them that they have no choice.  This is what McCarthy sets out to do in the concluding remarks of her draft:

#eir: In the rapporteur's view, there are only two choices: either to approve the Commission's proposal, possibly with amendments, such as her own, consistent with the European Patent Convention and TRIPs, or to reject it.  If the Commission's proposal is rejected, the European Patent Office and its Boards of Appeal would remain the principal arbitrators of the law and there would be nothing to prevent a gradual drift towards the patentability of business methods and the like, as has been witnessed in the United States.

#gea: Art 52 EPC already excludes %(q:business methods and the like) from patentability, and the EPO is already violating this exclusion.  Interestingly, Arlene McCarthy too assumes that the EPO is likely to %(q:drift) toward violation of Art 52 EPC and must be prevented from doing so by a directive.   McCarthy proposes to %(q:bind) the EPO by a directive which %(ol|advances all possible and impossible arguments to advocate software patentability|stipulates patentability of algorithms and -- literally or by inference -- business methods|refuses to define central concepts such as %(q:technical) or %(q:invention), either in a positive or negative way|treats the EPO as a legislative authority.)

#uel: It is rather difficult to understand, why a rejection of the proposal would leave legislation in the hands of the European Patent Office.  Why couldn't the European Commission come up with a new directive proposal and restart negotiations?  McCarthy seems to be speaking from a position of insider knowledge here, and it is not the only part of her text which reveals a close affinity between McCarthy and the patent law drafters from the European Commission's Industrial Property Unit.  McCarthy's text makes more sense if we read it as a threat uttered on behalf of the European Commission: %(s:Dear Parliamentarians, you are at present enjoying a unique opportunity to put your rubber stamp under the decisions of the European Patent Office.  The EPO is a political ally with whom we must avoid irreconcilable conflicts.  You are allowed to apply cosmetic amendments, but please do not overestimate your role.  If you miss the opportunity, we will simply withdraw the directive project and you will have no part to play in the rule-setting about the limits of patentability in the foreseeable future.)

#seW: This is indeed what leading CEC patent law drafters have been letting people understand in oral conversations at the European Parliament.  The threat, as expressed by McCarthy, is however based on some false assumptions:

#oWy: There would therefore continue to be uncertainty and a lack of transparency and there would be no Community competence in this area.

#tei: Community competence is already arising from the Community Patent, and even the European Commission is gradually becoming less dependent on alliances with the European Patent Office.

#lso: McCarthy seems to imply that, if only the EPO's practise of granting masses of trivial and broad patents is rubber-stamped by the European Parliament, the intransparency of the EPO's patentability rules will be remedied and software creators will be able to know clearly what they are allowed to do and what not.  This strange assertion becomes understandable if we decode as follows: %(bc|%(q:legal uncertainty) = %(q:uncertainty about validity of software patents granted under current EPO practise)|%(q:intransparency) = %(q:complexity and incoherence of the legal fictions that must be cultivated in order to circumvent Art 52 EPC))

#ahr: This uncertainty is however, according to a %(cp:report by the French State Planning Commission (Commissariat du Plan) of 2002), currently the only reason why the software developpers and SMEs enjoy better patent protection (i.e. better protection from patent attacks) in Europe than in the US:

#cWg: Only the armistice which is currently prevailing, precisely due to the legal uncertainty around the notion of software patent, explains in effect that the existing patents are not more frequently used.

#ttl: McCarthy predicts that this will change:

#Wls: Also software developers' only recourse would be to bring proceedings in their national courts and all the indications are that national courts, would tend to follow the case law of the Boards of Appeal in Munich.

#WWu2: Indications are rare, because only a few national courts have dealt with the subject matter so far.  Not all have followed the EPO.  The 17th senate of the German Federal Patent Court has consistently applied Art 52 EPC, and the German Federal Court has also backed down from some earlier more adventurous decisions.  The recent %(bg:Error Search decision) case brought both courts close together and led to the %(bp:rejection of an EPO patent) and the statement that %(bc:An improvement in computing efficiency does not constitute a patentable invention according to Art 52 EPC.)  A Swedish high court has used a similar argumentation to reject EPO software patents.  Even within the European Patent Office there are conflicting lines of reasoning.  The EPO may well find its way back to legality if the patent inflationist positions continue to lose support.  The European Parliament could help this process by%(ol|passing a resolution that explains Art 52 EPC and exhorts European patent courts to apply it correctly|rejecting the European Commission's directive proposal and specifying requirements which the next proposal should meet.  If the European Commission means what is says about the %(q:need of harmonisation and clarification), then it will not fail to meet these requirements.)

#eWW3: Lastly, software developers could not benefit from the interoperability exceptions provided for in Article 6 of the proposed directive, thus risking infringement proceedings.

#pWC: This could perhaps be said of the CULT and ITRE version of Article 6, but not of McCarthy's version.  If McCarthy's Article 6 has any effect at all, then that the rights of patent owners are not limited by interoperability considerations in any meaningful way.

#nsa: The CULT and ITRE versions, too, are patchwork on a directive proposal which is structurally flawed.  There is no reason to rush to accept such a patchwork.

#pne2: Setting %(q:clear limits as to what is patentable and what not) may require more ressources than have so far been allocated for this task.

#eoP: Software developpers can afford some patience, because the current legal situation is protecting them and will continue to do so, especially if the European Parliament and others show a political will to reinforce this protection.

#yav: McCarthy 2003-02-19 Software Patent Directive Proposal

#Cth: Arlene McCarthy, British Labor MEP appointed by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI) to report on the European Commission's Software Patentability Directive Proposal (CEC/BSA Proposal), proposes to pass this proposal with minor amendments.

#0eS: Pollmeier 2003/03: Stellungnahme zu Änderungsanträgen JURI zum EU-Richtlinienvorschlag Software-Patente V 1.0a

#rpt: A paper from a german SME which criticises the McCarthy paper.  We have recycled some parts of this paper in our paper.

#StW: VOSN.nl letter to MEPs

#sr7: This letter from the Dutch Open Source Association was distributed together with the present paper in paper form (20 pages) to 70 members of JURI on 2003/03/25.

#Wtr: McCarthy knows our replique to her previous paper quite well.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amccarthy0302 ;
# txtlang: en ;
# multlin: t ;
# End: ;

