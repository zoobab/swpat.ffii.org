<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: The Presidency Candidate of the French Green Party joins the Socialist Party and his fellow contender Jean-Pierre Chevènement in general rejection of software patents and specifically criticises the %(eb:software patentability directive proposal of the European Commission) of 2002-02-20, demands a return to a strict adherence to the EPC and a removal of the much-abused %(q:as such) clause from the EPC and its French equivalent, asks the French government to act firmly based on the principles agreed upon recently by the Socialist Party, says that asserting the law against an illegal patent court practise is of highest political priority because constitutional freedoms are touched and the prosperity of the software industry in Europe is under threat.
title: Noël Mamère 2002-20-28: Let's just delete the %(q:As Such) clause!
Sei: Communiqué de presse de Noël Mamère sur le projet de directive visant à légaliser les brevets logiciels en Europe.
LWs: La Commission Européenne a dévoilé le 20 février 2002 un projet de directive visant à légaliser les brevets sur les logiciels en Europe, sur le mode américain.
Lri: Les communiqués de la commission européenne laissent penser que cette proposition de directive est un bon compromis et qu'elle tient compte des remarques des opposants à la brevetabilité des %(q:inventions mises en oeuvre par ordinateur) (en fait des règles abstraites enrobées en jargon informatique). Pourtant, le projet de directive introduit une brevetabilité illimitée de toutes les idées, celles de l'informatique, mais aussi les méthodes d'organisation du travail, de l'éducation, de la santé ... .
Cvn: Cette directive s'appuie sur la pratique illégale de l'Office Européen des Brevets tentant de faire passer un viol de la loi pour sa jurisprudence, alors que la conférence diplomatique de novembre 2000 avait conclut que les activités de l'OEB devraient être plus étroitement contrôlées par les gouvernements européens.  De plus on constate dans la fiche d'étude d'impact annexée au projet de directive que le seul document économique servant à étayer la position de la commission européenne est un document commissionné par la Business Software Alliance en 1998 (structure qui ne dispose d'aucun statut juridique en France et pratique lobbying agressif et menaces financières contre les entreprises européennes), qui ne parle même pas de brevetabilité, alors que les différentes études économiques françaises, britanniques ou allemandes qui ont des conclusions négatives ne sont pas utilisées.
LWm: Le droit d'auteur seul permet une protection égalitaire entre multinationales, PME et développeurs indépendants, et a d'ailleurs permis la prospérité de l'industrie européenne du logiciel. Ce projet de directive conduirait inéluctablement à l'affaiblissement de l'innovation logicielle en Europe (comme le montrent les études économiques), la monopolisation des standards, donnerait le champ libre aux multinationales non européennes pour contrôler les joyaux de l'industrie européenne du logiciel, et conduirait à la quasi suppression des droits élémentaires des citoyens de la société de l'information.
Fot: Face à ces dangers, il faut aujourd'hui un acte politique fort. Le Parti Socialiste se dit contre les brevets logiciels, pourtant le gouvernement laisse dériver la situation. S'agissait-il d'une simple promesse de circonstance liée à la proximité des élections présidentielle ?
Laa: Le comportement de la commission européenne montre que ce n'est pas le moment d'harmoniser et donc le gouvernement français doit clairement dire que la proposition de directive publiée par la commission européenne ne peut être considérée comme une base de négociation, et doit se prononcer dés maintenant et très clairement contre la brevetabilité du logiciel et des innovations immatérielles.
Meu: Modifier substantiellement la loi par le biais d'une pseudo-jurisprudence, c'est violer la Constitution de la Vème République qui spécifie précisément que cette latitude n'appartient qu'au Peuple. L'accepter serait une forfaiture.
Lev: La volonté intéressée de l'OEB et de la commission européenne est de modifier l'esprit de la loi en légalisant des pratiques qui la violent. La volonté du gouvernement doit être de reformuler la loi pour que sa lettre en respecte l'esprit.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: mamere020228 ;
# txtlang: ca ;
# End: ;

