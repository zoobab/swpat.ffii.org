\contentsline {chapter}{\numberline {1}Introduction}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Essence of the Proposal}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Tabular text edition in BSA, CEC versions with counter-proposal in the spirit of the EPC}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}BSA/CEC Advocative Preface}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}EU Software Patent Directive Amendment Proposals}{6}{section.3.2}
\contentsline {section}{\numberline {3.3}Financial Statement}{7}{section.3.3}
\contentsline {section}{\numberline {3.4}The Proposal}{7}{section.3.4}
\contentsline {section}{\numberline {3.5}Footnotes}{7}{section.3.5}
\contentsline {chapter}{\numberline {4}Questions, Things To Do, How you can Help}{8}{chapter.4}
\contentsline {chapter}{\numberline {5}Annotated Links}{9}{chapter.5}
