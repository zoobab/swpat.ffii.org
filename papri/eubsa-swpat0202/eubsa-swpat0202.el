; -*- coding: utf-8 -*-

(require 'swpatdir)
;(load-library "make-mlht-elc") 
;(load-library "swpatdir")

(list 'eubsa-swpat0202-dir

(let ((texlink 'footnote) (longtable t)) 
(mlhtdoc 'eubsa-swpat0202
(ML title "CEC & BSA 2002-02-20: proposal to make all useful ideas patentable")
(ML descr "The European Commission (CEC) proposes to legalise the granting of patents on computer programs as such in Europe and wholesale adoption of the american practise of unlimited patentability.   %(q:But wait a minute, the CEC doesn't say that in its press release!) you may think.  Quite right!  To find out what they are really saying, you need to read the proposal itself.  But be careful, it is written in an esoteric Newspeak from the European Patent Office (EPO), in which normal words often mean quite the opposite of what you would expect.  Also beware of the long and confusing advocacy preface, which mixes EPO slang with dogmatic statements about the importance of patents and proprietary software, making naive readers believe that there must be some kind of linkage between the two.  This text disregards the opinions of virtually all respected software developpers and economists, citing as its only source of information about the software reality two unpublished studies from BSA & friends (anti-piracy alliance dominated by Microsoft and other large US companies) about the importance of copyright enforcement.  These studies do not even deal with patents!  The advocacy text and the proposal itself were apparently drafted on behalf of the CEC by an employee of BSA.  Below we cite the complete proposal, adding proofs for BSA's role as well as an analysis of the content, based on a tabular comparison of the BSA and CEC versions with a debugged version from FFII.  This FFII version may help you to appreciate the clarity and wisdom of the patentability rules in the currently valid European Patent Convention (EPC), which the CEC's patent lawyer friends have worked hard to deform during the last few years.")
nil

(sects
(intro (ML Iot "Introduction")

(ML Tee "Imagine you own a small software company.  You have written a powerful piece of software.  This software is a creative combination of 1000 abstract rules (algorithms) and a lot of data.  The rules take a few minutes or hours each to [re]invent, whereas developping and debugging the whole work took you 20 man-years.  900 of the rules were already known 20 years ago.  50 of the rules are now covered by patents.  You own 3 of these patents.  In order to obtain these patents, you had to rush to the patent office, disclose your business strategy and pay lawyer fees.  IBM and Microsoft are meanwhile already turning your patented ideas into profit.  You want them to stop?  Their lawyer teams say you are infringing on 20-30 of their 50000 patents.  So you reach a gentlemen's agreement:  3% of your annual sales revenues go to IBM, 2% to Microsoft, 2% %(dots).  Nonetheless, one day you enter the profit zone.  Now you are an attractive company.  A patent agency approaches you.  You are infringing on 2-3 of their patents, they say.  Their claims are very broad.  They want 100,000 EUR.  Litigation could take 10 years and cost 1 million EUR.  You pay.  A month later, the next patent agent knocks on the door %(dots). Before long you are broke.  You seek protection from a big company.  Microsoft offers to buy you for a symbolic fee.  You accept.  Under a copyright-only system, you would now be independent and rich.  But by means of patents, Microsoft and others were able to steal your intellectual property.")

(ML ChC "Does this help you understand why many software creators feel that BSA and their patent lawyer friends at the European Commission are among the world's biggest software pirates?")

(filters ((ew ahs 'warn01C)) (ML IuP "If you are still new to the European debate, you may wish to carefully read our %(ew:crash course on European Patent Lingo and the Backgrounds of the upcoming European Community Directive Proposal), available in 4 languages.  This was written before we received any directive draft and is meant for general sensibilisation to the questions under discussion.  It will hopefully take you only 20 minutes to read."))

(ML NcB "Now what is this European Community Directive Proposal about and how did BSA get in there?")

(filters ((ip ah 'indprop) (is ah 'indprop-swpat0202) (pr ah 'indprop-swpat0202-pr) (FAQ ah 'indprop-swpat0202-faq "FAQ")) (ML TWr2 "The final version of the document was adopted by the European Commission on the morning of 2002-02-20 and %(is:published) at noon of that day on the %(ip:web page of the Industrial Property Unit) together with an %(pr:press release) and an %(FAQ)."))

(filters ((do pet (commas (ah "proposal.pdf" "PDF") (ah "proposal.doc" "MSWord"))) (fm ah 'swpatmingorance)) (ML Ato "An almost completely identical %(do:directive draft proposal) started circulating among national government officials in mid february.  Although it is dated 2001, knowledgable people said that it was %(q:hot off the presses from Brussels).  Interestingly, the MSWord document contained a hidden author's field with the name %(fm:Francisco Mingorance).  This indicates that he has at least revised the text on his machine, and further evidence suggests that he played a major role in the drafting."))

(center (let ((width 100) (texwidth 30)) (img "mingorance.jpg" "Francisco Mingorance")))

(filters ((mp ah "http://www.01net.com/rdn?oid=177132&rub=2796") (BSA ah 'eukonsult00-bsa "BSA")) (ML Mps "Mingorance is the current director of public policy for Europe at %(BSA) and a longtime subscriber of an eurolinux-related patents mailing list.  Until recently, he used to work as a fundraising manager for an AIDS help organisation in Geneva.  In that function, he tried hard to defend the patent system against what he called %(q:vilification) by the supporters of the South African government's battle for lower drug prices.  The English in Mingorance's draft proposal carries traces of Mingorance's native language: French."))

(ML Wno "When Eurolinux revealed the BSA draft on the morning of the publication of the directive, the CEC's press service denied, telling journalists that this was %(q:not the Commission's draft but a draft from the industry).  Correctly so!")
(ML Sfa "Shortly after the publication of the final version by the CEC, Mingorance was quoted by the Wall Street Journal:")

(bc (ML FaW "Francisco Mingorance, director of the European public policy for the Business Software Alliance, an association representing such U.S. giants as International Business Machines Corp. and Microsoft Corp, said the proposal's authors %(q:have taken a couple of steps backwards.)"))

(ML Bst "Indeed the final version took a few minor steps %(q:backward) compared to Mingorance's draft.  Especially a few interoperability-related provisions were added, see tabular comparison below.")

(ML Mtf "Most BSA member companies are against software patents.  But the WSJ article correctly points out who are the active members of BSA that can afford to pay people to shape the patent policy of BSA: IBM and Microsoft.")

(ML Mfo "Microsoft as a specialised supplyer and near-monopolist of packaged proprietary software, has by far the highest stakes in BSA.  While BSA as a whole has no interest in the patent system, Microsoft is engaging in an active crusade for everything that hurts open source software, including software patents.  Microsoft's PR strategy is simple: it links proprietary software to %(q:intellectual property), %(q:wealth of nations) and %(q:jobs), while opensource software is claimed to be opposed to all these.  Likewise, patents are linked to proprietary software and the patent critics are portrayed as advocates of some unrealistic %(q:open source business model).  Although these assertions are grossly untrue -- proprietary software is, just like free software, based on copyright and threatened by patents -- they seem plausible from a naive reader's point of view.  Mingorance's proposal uses this same strategy.  It cites some obscure BSA studies about the importance of proprietary software to help cultivate this popular misconception while completely ignoring all informed discussions about the problems of software patents.")

(filters ((gp ahs 'eupatgp97)) (ML Ido "It should be noted that the EU Commission's %(gp:Greenpaper) cites Microsoft as a success model and bases its assertion that %(q:software patents have had a very positive impact on the software industry in the US) solely on the argument that %(q:Microsoft already owns 400 software patents)."))

(filters ((ah ah 'swpatahoward) (bm ah 'swpatbmueller)) (ML EEA "Except for the pseudo-economic argumentation and the references to BSA and Microsoft-related studies about the importance of Microsoft's business model, most of this text is apparently neither a creation of BSA nor of the European Commission but a transcript of texts which originiate in the European Patent Office.  Thus, when asked who wrote this proposal, the answer should be:  someone in the European Patent Office, together with Francisco Mingorance from BSA, in addition to a few corrections from Liikanen's subordinates while the final editing work was done by %(ah:Anthony Howard) at the Industrial Property Unit of Bolkestein's General Directorate for the Internal Market.  Possibly BSA wrote its draft on the basis of an earlier draft which %(bm:Bernhard Müller) had already been preparing in October 2000 on the basis of input from the EPO.  It is clear from the draft that, apart from the BSA rhetoric and a few minor compromises with Liikanen's subordinates, nothing substantially new has been added since october 2000."))

)

(summ (ML Sor "Essence of the Proposals")

(sects
(dokt
(ML Wtr "What the CEC/BSA proposes in terms of legal doctrine")

(sects

(prog 
 (ML CWn "Computer programs as such are patentable inventions")
 (ML Aeg "According to this proposal, all %(q:computer-implemented) ideas belong to a %(q:field of technology) and are as such %(e:patentable inventions).") )

(prob 
 (ML Alp "All practical problem solutions are patentable inventions.")
 (ML Ate "Any abstract rule that is or can be applied to the material world belongs to a field of technology and is therefore a patentable invention.  It is no longer necessary to assess of what type the achievement is, for which someone wants a patent.  The european traditional distinction between %(tpe|patentable vs unpatentable innovations|%(e:technical inventions) vs %(e:rules of organisation and calculation)) is abolished.  Instead all practically applicable ideas are treated as %(e:technical inventions).  However, these inventions may not be patentable, because they are obvious (lack an inventive step).  The non-obvious part, according to the CEC/BSA proposal, must be of %(e:technical character).   Here the discarded %(e:technical invention) concept seems to come back in again %(q:for practical reasons) -- a nice way of point out %(e:theoretical inconsistency).  And what is %(q:technical)?  The BSA/CEC answer is circular: %(q:anything that is based on technical considerations).  Moreover, computer programs as such are declared to %(e:belong to a field of technology).  Thus any rule of organisation or calculation (algorithm, business method) is not only a technical invention but also automatically contains a %(q:technical contribution) if it is deemed %(q:new) and %(q:non-obvious).  It must only be presented in the jargon of computing (i.e. using terms such as %(q:processor), %(q:memory), %(q:i/o), %(q:database) etc.")
 (ML CrC "Computing jargon may sound %(q:technical) to the naive mind.  Any computer-literate person however knows that the %(e:universal computer) is our standard %(e:logical device). Computing jargon is merely a metaphorical language for expressing abstract thought.  It is equivalent to mathematics as well as business concepts and many other expression forms of algorithms.  Not only that, it is the standard form in which algorithms come into touch with the material world today.  Thus, under the CEC/BSA proposal, any rule of organisation and calculation, no matter in which field, will receive a patent if it is %(q:novel) and %(q:non-obvious).  Moreover, since algorithms are expressed in pseudo-%(e:technical) clothing rather than in their most naked form, it is ensured that these patents will tend be granted for trivial variations of known algorithms rather than for real mathematical innovations.")
 (filters ((ms ah 'jwip-schar98)) (ML Tfa "This proposal is a 100% transcription of the current doctrine of the European Patent Office (EPO).  An leading EPO judge, in %(ms:explaining) this doctrine, accurately summarised it in the formula %(q:All practical problem solutions are patentable inventions)."))
 (filters ((pr ah 'indprop-swpat0202-pr)) (ML Tnd3 "The CEC's %(pr:press release) in fact names a few fictive examples of what should be patentable, but it does not name a single example of a new and non-obvious algorithm that would not be patentable.  The CEC's assertion, found elsewhere in the PR, that Amazon One Click would not be patentable under these rules, is a plain lie.  The One-Click idea is, according to the CEC/BSA rules, a %(q:computer-implemented invention) in a %(q:field of technology), which, if found to be novel, also contains a %(q:technical contribution), since methods for improving the efficiency of an automatic process by the clever use of generic computer hardware, according to CEC/BSA, lie in a technical field.")) )

(nono (ML Tnd2 "The EPO's low non-obviousness standards made obligatory for all of Europe.") 
  (filters ((PR ah 'indprop-swpat0202-pr)) (ML Aoh "As a side effect, this directive codifies several detail aspects of the EPO's current formalisms for testing non-obviousness.  These formalisms are theoretically incoherent and practically easy to bypass.  The non-obviousness standards of the EPO are notoriously low.  Not only have they put the German Patent Office under pressure to compete by lowering its standards.  They have even been shown by comparative studies to be lower than those of the patent offices of the USA and Japan.  While in their %(pr:press release) Bolkestein and Liikanen claim that the CEC is doing something to prevent US-style trivial software patents, in reality their proposal does the exact opposite: it makes sure that in Europe any functional idea, however abstract, broad and trivial, will be patentable as long as no bullet-proof prior art document is found."))
  (ML IWt "Instead of creating clear rules, the CEC/BSA tries to push the wild-grown esoteric reasoning habits of a few EPO courts into the law, therebey making this law more proprietary, intransparent and unaccountable than ever.  Evidently the CEC/BSA/EPO want to create a legislative jungle to which they and their friends alone hold the key, perpetually denying everybody else access to the discussion.")
)

 

(disk 
 (ML Tge "The EPO rulings of 1998 are not accepted")
 (ML Cxl "Claims to %(q:program on disk), %(q:computer program product), %(q:data structure) etc, which the EPO introduced in 1998, are not allowed, but nevertheless there are %(q:infringing computer programs).")
 (filters ((TAM ah 'ist-tamai98)) (ML Tin "The provision of Art 52 EPC that computer programs as such are not patentable inventions is reinterpreted to mean that computer programs (computer-implented teachings) are as such patentable inventions but they %(tan|may not be claimed as what they are|Indeed all non-trivial software innovations are highly abstract ideas and would, if contradiction was to be consistently avoided, have to be claimed as %(q:a thought process in the human mind, characterised by that ...).  See %(TAM))."))
 (ML Tsk "The CEC/BSA believes that this dirty construction makes it possible to maintain Art 52 EPC unchanged -- but of course deprived of all practical meaning.")
 ) ) 
;dokt
)

(ekon (ML WoW "What the CEC/BSA proposes in terms of software economy")

(ul
(ML Teb "The CEC/BSA ignores all economic studies, even official economic reports published by the governments of France and Germany.  These reports show that the introduction of patents in the software economy tends to create legal risks for innovators, thus stiffling innovation and competition.")
(ML T0s "The CEC/BSA also disregards the 90% opposition of software professionals against software patents.")
(ML Tfh "The CEC/BSA recognizes that 75% software patents filed in Europe belong to non-EU companies and that European SMEs are not ready to file patents.")
(ML Tle "The CEC/BSA recognizes also that open source software developers may have to get a patent license to keep on developing their project and that nothing garantees that they will receive it.")
(ML TiW "The CEC/BSA believes that there is no possible way to create distincions between SMEs and non SMEs with patent law and that nothing can be done to offset the unbalance in disfavor of open source software (and shareware).")
(ML Aao "Apparently, the European Commission has chosen to back the position of certain lobbying groups (patent attorneys and large IT companies) which may benefit from the extension of the patent system, rather than backing a position based on a neutral assessment of the impact of software patents on innovation, competition, safety and consumers. This would make the current proposed directive illegal with respect to the Rome Treaty and to the European Convention on Human Rights which both require that any new CEC Directive must benefit European citizens/consumers in some way and help foster innovation and competition.")
)
)

(mens (ML Spt "Some apparent lies")

(ul
(ML Tou "The CEC/BSA says that %(q:the collective responses on behalf of the regional and sectoral bodies, representing companies of all sizes across the whole of European industry, were unanimous in expressing support for rapid action by the Commission more or less along the lines suggested in the discussion paper).  This is a plain lie, given the large number of organisations, even large ones such as Syntec-Informatique.fr (80000 members), Prosa.dk (15000) members, Bileta, the BBC as well as the 300 organisations in the Eurolinux Alliance epxpress themselves more or less clearly against the discussion paper's approach.")
(ML Tad "The CEC/BSA claims that software ideas are easy to imitate and difficult to develop and that this is %(q:increasingly) so.  The latter statement is unsubstantiated and the former is untrue.  Unlike most ideas in copyrighted works, software ideas are very difficult to imitate, especially when the work is available only in binary form, and fairly easy to develop.  Most software patents represent the intellectual work of a few minutes or hours.")
(filters ((HG ah 'swpatpikta "Horror Gallery of Euorpean Software Patents") (ii ah 'euip017)) (ML Tda "The CEC/BSA claims that the European Patent Office does a good job at examining software patents and at making sure that no trivial patents are granted.  It fails to cite any real EPO patents, most of which are trivial.  It is saying this in bad faith, because it knows our %(HG) quite well and has even been orally %(ii:informed) in detail about the situation.   But then this paper is not by the CEC but by BSA.  Yet Francisco Mingorance, as a reader of our mailing list, is also aware of the reality of the EPO's patents, which are, as far as software is concerned, just as bad as those of the USPTO."))

(ML Tlm "The CEC/BSA claims that patents are particularly helpful for small companies.  This is in contradiction to common knowledge and to evidence from all serious studies.")

(ML TWe3 "The CEC/BSA claims that opposition procedures at the EPO are very helpful for eliminating %(q:bad patents) and that the US has no system of this kind.  They forget to mention that almost no oppositions have been filed in the software field and that the US institution of %(e:reexamination) is actually more useful, because there is no time limit of 9 months after publication.")

(filters ((it ahs 'ist-tamai98)) (ML Trt "The CEC/BSA claims that programming ideas can be distinguished from abstract algorithms and from business methods.  This is not true.  The jargon of %(q:software engineering) is an equivalent of mathematics (and of business methods).  The %(it:problem of abstraction) is well known, and the CEC/BSA tries to fool people into believing that it is addressing it, while it is not, allowing every abstraction to be patented at the most abstract level, which in fact is the level of the universal virtual machine called computer."))

(filters ((sk ahs 'swpatkorcu)) (ML TnJ "The CEC/BSA claims that what the EPO is doing is legal, that the EPC allows the patenting of software and that the proposed regulation changes nothing.  %(sk:This is not true.)  Even the CEC/BSA does mention that only the German Federal Court of Justice (BGH) has followed the EPO on its adventurous course and that the others must be forced to follow by means of this directive."))

(filters ((ba ahs 'basinski-busmeth01)) (ML TuW "The CEC/BSA claims that its proposal brings legal security.  In reality, the proposal, besides subjecting software creators to a regime of systematic legal insecurity, creates an ambigous situation for business methods.  It says on the one hand that business methods shouldn't be patented but on the other hand creates rules which inevitably lead to their being patented.  As a result small players in Europe may be deterred from applying for business method patents while large players from the US will easily transfer their business method patents to Europe.  Getting the EPO to patent business methods is a simple art for which american %(ba:instruction manuals) already exist.  Business methods of the %(am:Amazon One Click) type are even directly legalised by this directive proposal.  No special wording skill is needed to get them patented under the proposed CEC/BSA regulation."))


(filters ((rp ahs 'swpatjavni) (el ahs 'eukonsult00-eurolinux)) (ML Txo "The CEC/BSA says that the %(rp:regulation proposal) in the %(el:Eurolinux Official Response) agrees to patenting industrial software.  They have evidently not cared to read this proposal."))

(al

 (ML Tdm "The CEC/BSA proposal includes a financial statement at the end which which claims that the directive will be without budgetary impact and without impact on the number of posts in the Commission. On the basis of such a statement, DG Budget was not consulted in preparation phase.")

 (linol 
  (ML TWg "This raises interesting questions:")
  (ML Cen "Can one truly envisage a doubling (conservative estimate) of the number of patents granted in Europe without any impact on what it costs to monitor such a situation?")
  (ML Aoa "Are no corrective measures envisaged to cope with possible harmful effects, and would these measures come for free?")
  (ML Swi "Since the directive plans a review 3 years after delay for tranposition, who will manage this review?") )
 
 (filters ((bm ah 'swpatbmueller) (ah ah 'swpatahoward)) (ML Tot "The answer might be: we will go on having employees of the patent family doing it in the Commission offices, just as Paul Schwander, EPO liaison to the Commission managed for the Commission (DG Entreprise) the study on SME and software patents, just as %(bm:Bernhard Müller) drafted the initial versions in DG Internal Market, before being replaced by %(ah:Anthony Howard)."))

 (ML Ntl "Note that unfortunately, this is only one aspect: the cost effectiveness part of the financial statement is even more outrageous: Patent expenditures (litigation and associated activities and risks excluded) have been growing 12-15% per year in Europe for the past years. No other tax can afford to grow at such a rate, and even the big industry supporters of patents have started complaining about it.")
 (bridi 'cf (ahs 'swpatpleji))
)
)
)
)
; summ
) 

(text (ML Tnd "The text in BSA, CEC and FFII versions")

(ML Hes "Here we are presenting a critical edition of the text, with annotation and a tabular comparison between the CEC/BSA initial and final draft and an added FFII version, which shows what is wrong with the CEC/BSA version and how it could be rewritten in a coherent and adequate way.  We highlighted some differences by bold typeface.")

(sects 
(advok (ML Aca "BSA/CEC Advocative Preface and FFII criticism")
(sects
(exme (quot (ML ETM "EXPLANATORY MEMORANDUM"))
(kompar
(l2col (ML OWt "Objective of the Community initiative")) 
(l2col (ML S9b "Software development has shown steady growth in recent years. It has had a major impact on the whole of European industry and provides a substantial contribution to the GDP and to employment. In 1998, the value of the packaged software market in Europe was 39 B euros. A recent study by Datamonitor concluded that the number of packaged software workers in Western European countries will grow by between 24% and 71% from 1999 to 2003, with an average of 47%. A further conclusion is that each packaged software job creates 2-4 jobs in the downstream economy and 1 job in the upstream economy.") (ML Rdy "Reasoning and material typical of BSA.  The Datamonitor study was not available on the Internet as of 2002-02-28, not even at Datamonitor's website.  We have asked Datamonitor for it and received no response."))
(l2col (ML Ien "Its future potential for growth and, thus, its impact on the economy are even stronger because of the accelerating importance of electronic commerce in the Internet-based Information Society. Given the maturity that today's software industry has achieved, many improvements of software are increasingly difficult and expensive to achieve while, at the same time, they can easily be copied.") (ML Utu "Unsubstantiated and untrue, see above."))
(l2col (ML PWi "Patents play an important role in ensuring the protection of technical inventions in general. The basic principle underlying the patent system has proven its efficiency with respect to all kinds of inventions for which patent protection has thus far been afforded in the Member States of the European Community. Patents act as an incentive to invest the necessary time and capital and it stimulates employment. Society at large also reaps benefits from the disclosure of the invention which brings about technological progress upon which other inventors can build.") (filters ((ma ah 'machlup58) (ek ahs 'swpatsisku)) (ML Tnu "Time-proven myths of the patent movement.  Read %(ma:Machlup) and other %(ek:economists).")))
(l2col (ML TWe "The current legal situation regarding patent protection in the field of computer-implemented inventions is ambiguous, and thus lacks legal certainty. In fact, computer programs ``as such'' are excluded from patentability by Member States' patent laws and the European Patent Convention (EPC) but thousands of patents for computer-implemented inventions have been granted by the European Patent Office (EPO) and by national patent offices. The EPO alone accounts for more than 20,000 of them. Many of these patents are in the core areas of information technology, i.e. digital data processing, data recognition, representation and storage. Others are being granted in other technical areas such as automotive and mechanical engineering, e.g. for program-controlled processors.") (ML WWW "When the EPO violates the law, the blame must of course be put on the law.  Note how the word %(q:as such) is used to make the law seem meaningless.  Note also the care taken to make information processing appear as %(q:just another field of technology).")) 
(l2col (ML WSp "While the statutory provisions setting out the conditions for granting such patents are similar, their application in the case law and the administrative practices of Member States is divergent. There are differences, in particular, between the case law of the Boards of Appeal of the European Patent Office the courts of Member States. Thus, a computer-implemented invention may be protected in one Member State but not in another one, which has direct and negative effects on the proper functioning of the internal market."))
(l2col (ML Ttn "This Directive addresses this situation by harmonising national patent laws with respect to the patentability of computer-implemented inventions and by making the conditions of patentability more transparent.") (ML Grr "Given that even the CEC press release and the CEC directive proposal contradict each other about what should be patentable, and that key terms are not defined, this claim of %(q:increased transparency) is a bad joke."))
) ) 

(bakg (ML Tes "The background to the initiative: Commission's consultations")
(kompar

(l2col (ML Fpa "Following consultation centred on the 1997 Green Paper on the Community Patent and the Patent System in Europe, the patentability of computer-implemented inventions was one of the priority issues identified in early 1999 on which the European Commission should rapidly take action. It was envisaged that a Directive harmonising Member States' law on the issue would remove the ambiguity and lack of legal certainty surrounding the issue. Furthermore, it was stated that in parallel with this action at the Community level, the contracting states to the EPC would need to take steps to modify Article 52(2)(c) of the Convention, in particular to abolish computer programs from the list of non-patentable inventions.")) 

(l2col (ML Aef "After 1999, public debate on the issue developed and became more intense. Some sections of European industry repeatedly asked for swift action to remove the current ambiguity and legal uncertainty surrounding the patentability of computer-implemented inventions, while on the other hand, developers and users of open source software and a substantial number of small and medium-sized enterprises backing them have increasingly raised concerns about software patents.") (ML Osn "On the whole, this is correct. %(q:Some Sections) is referring to the patent family's strongholds in associations such as EICTA and ZVEI.  E.g. in ZVEI this was a press release written by the Siemens patent department chief Arno Körber, while nobody else in the office seemed to know or even care about it."))

(l2col (ML Ouc "On 19 October 2000 the European Commission launched a final round of consultations in which the public at large and Member States were invited to comment on the basis of a paper which was made available on the Internet.") (ML Nwt "Nobody said beforehand that this round would have to be %(q:final).  Rather, it was a way to take account of a new situation after firm action by some national governments to prevent the deletion of the programs for computers from the list of non-inventions."))

(l2col (ML Tnf "The consultation adopted a two-pronged approach. In the first place, the basic question was posed as to whether there was any need at all for action at the Community level on harmonisation, and in the case this question were to be answered in the affirmative, what the appropriate level would be in general terms. Following this, there was set out in some detail the current state of the case law as established within the EPO, with the suggestion of a number of very specific elements which might figure in any harmonisation exercise based more or less on this status quo.") (ML Tie2 "The first of these two questions was not asked during the public consultation period but only thereafter in confidential meetings with civil servants from the national patent administrations.  The second question set made it clear that this consultation was indeed only a %(q:final round) of an already predetermined course and, as the previous patent lawyer consultations, was only a virtual debate in which patent lawyers were to hold learned debate on grammatical questions rather than on the content of the regulation to be decided."))

(l2col (ML Tso "The consultation produced around 1450 responses, which have been analysed by a contractor whose report has been published.") (filters ((sr ah 'eukonsult00-softanalyse)) (ML Iow "Indeed only selected responses were published by the CEC, and the contractor wrote a %(sr:summary report) catering to the taste of the CEC patent lawyers and keeping silent about many important patent-critical submissions.")))

(l2col (ML OWi "One conclusion which can be drawn unquestionably from the responses is that there is a clear demand for action. The present situation in which there is lack of clarity as to the limits of what is patentable is seen as an important negative influence on the industry. However as to precisely what action should be taken, opinions were sharply divided between those who wish to see strict limits on software-related patents (or a complete ban) and those who support harmonisation at the level of more or less the status quo as defined by the current practice and jurisprudence of the EPO.") (ML Eid "Eurolinux was of the opinion that no legislative action by the Commission is needed but did agree that it could be useful."))

(l2col (ML Tai "The individual responses were dominated by supporters of open source software, whose views ranged from wanting no patents for software at all to the %(q:official) position of the Eurolinux Alliance which is to oppose patents for software running on general-purpose computers. On the other hand, submissions broadly in support of the approach of the consultation paper tended to come from regional or sectoral organisations representing large numbers of companies of all sizes, such as UNICE, the Union of Industrial and Employer's Confederations of Europe, EICTA, the European Information and Communications Technology Industry Association, and the European IT Services Association. There were also individual large organisations, other industry associations and IP professionals. Thus although the responses in this category were numerically much fewer that those supporting the open source approach, there seems little doubt that the balance of economic weight taking into account total jobs and investment involved is in favour of harmonisation along the lines suggested in the paper.") (filters ((op ah 'eukonsult00-eurolinux) (EK ahs 'eukonsult00) (jv ah 'swpatjavni)) (ML Twb "This calculation is silly and inhumane.  The CEC/BSA values the robot-talk from a few anonymous apparatchik masks higher than the argued reasoning of 1000 software creators.  We all know: companies are authoritarian machines designed to make money rather than to discuss about public policy questions.  Usually companies stay out of politics, and from the Consultation papers it is clear that most of the aplauding views expressed by the named associations and companies were inevitably written by patent lawyers and reflected the climate of the patent world rather than that of the software industry.  The CEC itself mobilised the patent world by asking %(q:very special) questions of patent law doctrine.  Anyone knows what will happen, when such a paper goes to a company or an industry asociation:  it is forwarded to the patent experts, i.e. the members of the patent family.  The responses from corporate patent lawyers were usually not supported by reasoned economic argument and did not reflect the experience of software companies.  Most of them did not even sign with their name, nor were the papers published on the company or association website.  As of 2002-02-28 you cannot find them neither at BSA nor at EICTA nor at UNICE.  You can however find well-reasoned anti-software-patent papers prominently figuring at the sites of such large organisations as Sytnec Informatique (80000 members), Prosa (15000 members) and Eurolinux (100000 signatures, 300 companies, 100 proprietary software), FENIT (dutch IT industry association) and even occasional support statements from very large companies such as ILOG, SAP, Cap Gemini, Frontbase, Oracle etc.  The pro-patent people stayed far away from all informed discussions and were keen to hide their unspeakable agenda from the public, trying instead to rely on an illusionary financial weight called %(q:economic majority).  It has become clear during the last 2 years that the Eurolinux Petition with its 100000 signatures and broad support by individuals and companies throughout the software sector does represent a general feeling of this sector that goes far beyond any questions of %(q:opensource) vs %(q:closed source).  The software sector is characterised by a symbiosis of these two.  The %(op:official eurolinux position) was one of rejection of all software patents and acceptance of patents on technical inventions (i.e. teachings of physical causality).  See the %(jv:Eurolinux Regulation Proposal) and %(EK).")))

(l2col (ML Ttn2 "The Commission's Directorate-General for Enterprise also commissioned a study, specifically in relation to small and medium sized enterprises (SMEs). This study aimed to investigate how SMEs involved in the development of software manage their IP. A central objective was to produce for them a brochure that will enhance the awareness of various methods of IP protection, as well as to inform them of these forms of protection. The research was largely desk-based but was supplemented with a survey questionnaire of European software SMEs that were selected from a number of sources. Of the questionnaires distributed, 12 SMEs responded. A limited number of large European software companies were also surveyed, as was a group of public research organizations.") (filters ((ta ah 'tangadpa00)) (ML TeW3 "This %(ta:study) clearly states that software patents tend to be at best useless for the promotion of innovation among software SMEs.")))

(l2col (ML Aea "Among the SMEs who responded there was generally quite a low level of awareness of patents as a means of protection for their products.  Patents were seen as complex, expensive and difficult to enforce for small entities and therefore less valuable than copyright or informal means of protection. Neither was there much awareness of the possibilities to use patents as a source of technical information. These results highlight the need to increase awareness among SMEs and present a particular challenge to practitioners and those responsible for administering the various systems.") (ML Wxe "When a study finds that software patents are less than useful for innovation in software enterprises, the patent system can't be at fault. It is always right -- %(q:in all fields of technology) as the sacred scripture teaches us.  If the economic data don't conform to this truth in some field, the SMEs in that field must be reeducated so as to produce the correct data next time.  And this reeducation can be obtained for free: no impact on the budget, as said in the later part of the study.")) 

(l2col (ML Tdn "The Commission has assessed the question as to how extensive harmonisation of the national patent laws regarding computer-implemented inventions should be in the light of the likely impact of the proposal on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce. Moreover, it has considered the impact on small and medium-sized enterprises and on the creation and dissemination of free/open source software. For this purpose, in particular, the findings of a study on the economic impact of the patentability of computer programs as well as of other pertinent economic studies have been taken into account. In determining the conditions for patentability, the Commission has paid special attention to the practice of its main trading partners, in particular of the United States and Japan. In this context, consideration has been given to the granting of patents for computer-implemented business methods in the United States, and more specifically to those of these patents which have applications in electronic commerce. Business method patents have become the subject of considerable debate in industrialised countries.") (filters ((RH ahs 'clsr-rhart97) (rs ah 'swpatsisku)) (filters ((ip ahs 'indprop-ipi00)) (ML Tbf "The named %(ip:study) is a voluminous patent movement propaganda pamphlet, as ridiculous in its argumentation as the GreenPaper and the BSA/CEC Directive Proposal, written by proven comrades from the british institutions of higher learning in questions of patent doctrine, see %(RH).  Yet even this propaganda pamphlet admits in the end that %(q:any move to extend patentability could not claim to be based on economic reasoning).  The many %(rs:real economic studies on the subject) are not mentioned by the CEC/BSA")))) 
) )

(kpet (quot (ML Iio "International competition: The legal situation in the U.S. and Japan"))

(kompar
(l2col (ML TWi "To create a level playing field regarding the conditions for protecting computer-implemented inventions between Europe and the U.S., it could have been considered desirable to widen the scope of protection and bring European patent law in this field more in line with the U.S. law.  One could have conceived, in particular, to allow for the patentability of computer-implemented business methods.") (ML Itf2 "Indeed there is no doubt that this directive proposal authorises the patenting of computer-implemented business methods."))

(l2col (ML Tel "The difference between the U.S. and Europe and between the U.S. and Japan is that in Europe there has to be a technical contribution provided by the invention. In Japan there is a doctrine which has traditionally been interpreted in a similar way: the invention has to be a highly advanced creation of technical ideas by which a law of nature is utilised. In the U.S., the invention must simply be within the technological arts and no technological contribution is needed. The mere fact that the invention uses a computer or software makes it become part of the technological arts if it also provides a %(q:useful, concrete and tangible result). That the U.S. does not require the invention to provide a technical contribution means that the restrictions on patenting of business methods (apart from the requirements of novelty and inventive step) are negligible.") (ML Ttd "This is mainly a rhetorical difference.  The patents granted are the same, and the restrictions are negligible at the EPO as well.  Especially with this directive, it is made clear that there is no obstacle to patenting computer-implemented business methods."))

(l2col (ML Tsv "The impact of the patentability of software-related inventions on innovation, competition and on businesses"))

(l2col (ML Tri "The study referred to above (see note 11) relies on the United States as a test case. It finds that %(q:the patentability of computer program related inventions has helped the growth of computer program related industries in the States, in particular the growth of SMEs and independent software developers into sizeable indeed major companies).  In Europe, too, there is increasing, even though still relatively low, use by independent software developers of patents in raising finance or in licensing. The main source of protection that has allowed the software industry to grow has been the law of copyright."))

(l2col (ML HhW "However, the study also clearly identifies concerns about the patentability of computer-implemented inventions in the U.S. They relate, first, to the grant of allegedly %(q:clearly invalid patents) (in particular for e-commerce), that is patents which are granted for inventions that are either not new or where inventive step is on the face of it lacking.  Second, patents for computer-implemented inventions might strengthen big players' market positions.  And, third, patents for incremental innovation which is typical of the software industry entail the economic costs of figuring out the patent holders and negotiating the necessary licences. Yet, the study acknowledges that it has not been shown that these reservations would outweigh the positive effects of the patentability of computer-implemented inventions in the U.S. To outline how Europe might be better placed than the U.S. to avoid adverse effects, the study stresses %(q:our strength in having opposition procedures in addition to the facility ... of being able to submit observations on the patentability of inventions to the EPO without the expense of opposition procedures). These are important legal means to ensure patent quality which are not available in the U.S.") (ML Amt "Again, some patent movement propaganda myths are given credibility by quoting them from a self-fabricated %(q:study)."))

(l2col (ML Mnn "Moreover, the study points out that in Europe we must ensure the application of proper examination standards, in particular of the inventive step, to prevent invalid patents. It should be added that the quality of the examination done in particular by the EPO is widely respected. Finally, the study finds %(q:no evidence that European independent software developers have been unduly affected by the patent positions of large companies or indeed of other software developers).") (ML IWg "Indeed software patents have been to date illegal in europe and, although the EPO has granted many, there have been no infringement procedings, mainly because large companies were waiting for the change of law which the CEC/BSA is proposing now."))

(l2col (ML Tbe "The study identifies as one possible option for the scope of harmonisation to %(q:stay with the status quo (as defined by the case law of the EPO), subject to removal of the exclusion of `computer programs' `as such'. This would, the authors consider, have no consequence save for the important one that SMEs and independent software developers will be less likely to consider computer program related inventions unpatentable.) On the other hand, %(q:any move to strengthen IP protection in the software industry cannot claim to rest on solid economic evidence)."))
) )

(legs (quot (ML Ta2 "The current legal situation regarding art. 52(1) and (2) of the epc.") )

(kompar
(l2col (ML Tqe "The fundamental requirement of %(q:technical character)"))

(l
(al
(ML Tah "The Boards of Appeal of the EPO have held that it is fundamental to all inventions that they have a technical character. Similarly, Article 27(1) of the TRIPS Agreement confirms that patents shall be available for inventions in all fields of technology. Accordingly, the EPO Boards of Appeal and courts of the Member States have held that computer-implemented inventions must be considered as patentable when they have a technical character, i.e. when they belong to a field of technology. Computer-implemented inventions which meet this condition are not considered to relate to programs for computers %(q:as such).")
(ML CWe "Conversely, the exclusion has been interpreted by the Boards of Appeal of the EPO as relating to those computer-implemented inventions which have no technical character. In the recent Controlling pension benefits system case, the Board decided that all programs when run in a computer are by definition technical (because a computer is a machine), and so are able pass this basic hurdle of being an %(q:invention). However, in Computer program product I & II the Board held that because of the potential of a program on a carrier to produce a technical effect when run on a computer, it should be allowable to claim a program as itself or as a record on a carrier (i.e. as a program product or as a signal).")
)

(al
(ML Aoj "According to the general requirements cf. article 52(1)-(3) of the EPC, which are reproduced in essence in Member States' patent laws, all patentable inventions must be new, involve an inventive step and be capable of industrial application cf. Article 52(1).")

(ML Usw "Under Art. 52(2) of the EPC, programs for computers %(q:as such) are defined as not being inventions and are thus excluded from patentability. The Boards of Appeal of the EPO have held that it is fundamental to all inventions that they have a technical character. Similarly, Article 27(1) of the TRIPS Agreement confirms that patents shall be available for inventions in all fields of technology. Accordingly, the EPO Boards of Appeal and courts of the Member States have held that computer-implemented inventions can be considered as patentable when they have a technical character, i.e. when they belong to a field of technology. Computer-implemented inventions which meet this condition are not considered to fall under the exclusion in Article 52(2) as they are considered not to relate to programs for computers %(q:as such). In fact, the exclusion has been interpreted by the Boards of Appeal of the EPO as relating to those computer-implemented inventions which have no technical character.")

(ML Wdn "With regard to what computer-implemented inventions can be said to have %(q:technical character) the conclusion to be drawn from the recent Controlling pension benefits system case is that all programs when run in a computer are by definition technical (because a computer is a machine), and so are able pass this basic hurdle of being an %(q:invention).")
)
(ML Twg "This section makes the appositive %(q:as such) seem unclear in order to allow art 52 EPC to be bended at will.  While the BSA version directly takes EPO decisions to be the law, the CEC's writer (Anthony Howard?) differentiates a bit but still does not ask the question of whether the EPO decisions are law-conformant.")
) 

(l2col
(ML SWe "Similar considerations have been applied by the EPO Boards of Appeal to the other items of Art. 52(2) which are excluded %(q:as such), for instance, to %(q:methods for doing business), %(q:presentation of information), or %(q:aesthetic creations). This means that inventions relating to one of these items have equally been held to be patentable when they have a technical character.") )

(l nil (ML WdW "With regard to the representation of the invention in the patent claims, the Board held, in Computer program product I & II that if a program on a carrier has the potential to produce a technical effect when loaded and run on a computer, such a program claimed by itself should not be excluded from patentability. This has been interpreted as meaning that it should be allowable to claim such a program by itself or as a record on a carrier or in the form of a signal (e.g. stored as a file on a disk or transmitted across the internet)."))
) )

(algo (quot (ML Teg "The role of algorithms"))
(kompar
(l2col (ML Teg "The role of algorithms"))

(l2col (ML Tyc "The term %(q:algorithm) may been understood in its broadest sense to mean any detailed sequence of actions intended to perform a specific task. In this context, it can clearly encompass both technical and non-technical processes.") (ML Wpj "Wrong.  An algorithm is not a process but potentially a description of a processs.  Real technical inventions are teachings about phenomena of nature, e.g. about what happens when matter is moved in a certain way.  Such teachings may be described by algorithms, but a sane patent system, as in the 70s, would never allow the patenting of such algorithms but only of the techncial invention."))

(l2col
 (ML Tre "The mere existence of an algorithm does not constitute a workable criterion for distinguishing patentable from non-patentable subject matter. An algorithm may underlie either a computer-implemented invention or an invention relating to a conventional (mechanical, electrical etc.) machine or the process carried out by that machine. The sole difference is that a computer program is executed by instructions directed to the computer and a conventional machine is operated by its (mechanical, electrical etc.) components.")
 (ML Wxr "Who said that the %(q:existence of an algorithm) constituted a criterion?  The difference between algorithms executed on a computer (or indeed on a conventional machine) and real technical inventions is that algorithms are achievements in the realm of abstract reasoning and are not based on new insights into phenomena of nature."))

(l2col 
 (ML Anc "An abstract algorithm can be defined in terms of pure logic in the absence of any physical reference points. It is possible that such an algorithm may be put to practical use in many different functions in apparently unrelated domains, and may be capable of achieving different effects. Thus, an algorithm which is considered as a theoretical entity in isolation from the context of a physical environment, and in respect of which it is accordingly not possible to infer its effects, will be inherently non-technical and thus not susceptible of being regarded as a patentable invention.")
 (filters ((dk ah '(anch swpatcusku knuth02))) (ML Tcy "This means that while %(q:1 + 1 = 2) is considered %(q:abstract) and %(q:inherently non-technical), %(q:1 apple + 1 apple = 2 apples) may be considered to be a teaching in a field of technology, namely computer-implemented botanics.  Nice reasoning for a supposedly clarifying directive.  Computer science guru Donald Knuth justly %(dk:compares) this reasoning to legislative rulings that pi = 3.0 or medieval Catholic Church decisions that the sun must revolve around the earth.")) )

(l2col (ML Ilu "It is a consequence of the above that an abstract algorithm as such cannot be monopolised. The normal rules for patentability mean that a patent claim to an invention which is founded on a particular algorithm would not extend to other applications of that algorithm.") (ML IWi "It is a consequence of the above that an abstract algorithm as such can be monpolised.  It only needs to be claimed in the %(q:concrete) terminology of %(q:computer-implemented botanics) or even of computing itself.  This terminology is an equivalent and the only practically relevant way of describing algorithms."))
) )
 
(pako (quot (ML Pia "Patent and copyright protection are complementary"))

(kompar
(l2col (ML Pia "Patent and copyright protection are complementary"))

(l2col (ML Ahe "A patent protects an invention as delimited by the patent claims which determine the extent of the protection conferred. Thus, the holder of a patent for a computer-implemented invention has the right to prevent third parties from using any software which implements his invention (as defined by the patent claims). This principle holds even though various ways might be found to achieve this using programs whose source or object code is different from each other and which might be protected in parallel by independent copyrights which would not mutually infringe each other."))

(l2col (ML Oro "On the other hand, for the purposes of Directive 91/250/EEC on the legal protection of computer programs, copyright protection is accorded to the particular expression in any form of a computer program, while ideas and principles which underlie any element of a computer program, including those which underlie its interfaces, are not protected. A computer program will be accorded copyright protection where the form of expression is original in the sense of being the author's own intellectual creation. In practice, this means that copyright would subsist in the expression in any form of the source code or the object code but would not subsist in the underlying ideas and principles of the source code or object code of a program. Copyright prohibits a substantial copy of the source code or object code but does not prevent the many possible alternate ways to express the same ideas and principles in different source or object code. It also does not protect against development of an identical or substantially identical program without the knowledge of an existing copyright."))

(l2col (ML Ayg "Accordingly, legal protection may exist in a complementary manner in respect of the same program both by patent and by copyright law. The protection may be cumulative in the sense that an act involving exploitation of a particular program may infringe both the copyright in the code and a patent whose claims cover the underlying ideas and principles."))
 
(l2col (ML Duf "Directive 91/250/EEC includes specific provisions (Articles 5 and 6) to the effect that copyright in a computer program is not infringed by the doing of acts under certain circumstances which would otherwise constitute infringement. These exceptions include acts done for the purposes of studying the ideas and principles underlying a program and the reproduction or translation of code if necessary for the achievement of the interoperability of an independently-created computer program. It is also specified that the making of a back-up copy by a lawful user cannot be prevented."))

(l
(al
(ML See "Such provisions are justified and necessary in the context of copyright law because copyright confers the absolute right to prevent the making of copies of a protected work. All the acts mentioned involve making copies and would therefore infringe in the absence of any exception. On the other hand, Member States' patent laws, while not fully harmonised, do not in general extend to acts done privately and for non-commercial purposes, or to acts carried out for experimental purposes related to the subject-matter of the invention. Nor is it likely that the making of a back-up copy in the context of the authorised exploitation of a patent covering a programmed computer or the execution of a program could be construed as an infringement. Thus, because of the differences between the subject-matter of protection under patent and copyright law, and the nature of the permitted exceptions, the exercise of a patent covering a computer-implemented invention should not interfere with the freedoms granted under copyright law to software developers by the provisions of the Directive 91/250/EEC. Moreover, as regards developing interoperable programs, the requirement for each patent to include an enabling disclosure should facilitate the task of a person seeking to adapt a program to another, pre-existing one incorporating patented features (the requirement of disclosure has no analogue under copyright law).")
(ML Fci "Finally, it should be said that in the event that patent rights are exercised in abusive way, compulsory licenses may be available as a remedy, as well as possible recourse to competition law. Recital 18 makes specific reference, inter alia, to the provisions on decompilation and interoperability in Directive 91/250/EEC.") 
)

(ML Syn "Such provisions are justified and necessary in the context of copyright law because copyright confers the absolute right to prevent the making of copies of a protected work. All the acts mentioned involve making copies and would therefore infringe in the absence of any exception. On the other hand, Member States' patent laws, while not fully harmonised, do not in general extend to acts done privately and for non-commercial purposes, or to acts carried out for experimental purposes related to the subject-matter of the invention. Nor is it likely that the making of a back-up copy in the context of the authorised exploitation of a patent covering a programmed computer or the execution of a program could be construed as an infringement. Thus, because of the differences between the subject-matter of protection under patent and copyright law, and the nature of the permitted exceptions, the exercise of a patent covering a computer-implemented invention should not interfere with the freedoms granted under copyright law to software developers by the provisions of the Directive 91/250/EEC. Moreover, as regards developing interoperable programs, the requirement for each patent to include an enabling disclosure should facilitate the task of a person seeking to adapt a program to another, pre-existing one incorporating patented features (the requirement of disclosure has no analogue under copyright law). Finally, it should be said that in the event that patent rights are exercised in abusive way, compulsory licenses may be available as a remedy, as well as possible recourse to competition law. Recital 18 and Article 6 make specific reference, inter alia, to the provisions on decompilation and interoperability in Directive 91/250/EEC.")

(ML NWb "Note that this has nothing to do with ensuring interoperability.  Programmers are only allowed to use patented processes in private for the purpose of reverse engineering, but not to publish a program that uses this process for the purpose of interoperability.")
) ) )

(natl (quot (ML Tya "The necessity of a Community action harmonising national laws and its legal basis"))

(kompar
(l2col (ML Tya "The necessity of a Community action harmonising national laws and its legal basis"))
(l 
 (ML Tnh "There are divergences of national case law and administrative practices which can have an impact on the question of whether individual inventions are patentable. The most significant are outlined below. In this context, it should be understood that the majority of litigation so far in this field has been conducted in the courts of only two Member States: Germany and the U.K.")
 (al
  (ML EhW "European Patents are granted by the European Patent Office, thus a uniform set of rules in a centralised procedure is provided for according to which, once granted European patents become subject to the national patent laws of each country for which they enter into force.  Furthermore, the basic national laws on patentability are in principle uniform as between themselves and the provisions of the European Patent Convention, but their detailed interpretation \255 with regard to the effect of a European Patent as well as a national patent - is the preserve of the courts. While the national courts may accord persuasive authority to decisions of the EPO's appellate bodies (and to decisions of other Member States' courts), they are not bound to follow them, and in the event of direct conflict, they may have no choice but to respect binding precedents in accordance with their own legal traditions. This can lead, and has in practice led, to divergences in interpretation of the European Patent Convention and consequently in the scope of protection accorded to certain classes of invention.") 
  (ML TsW2 "The majority of national level jurisprudence so far in the field of computer-implemented inventions has been developed in the courts of only two Member States: Germany and the U.K. Interestingly, even these have decided differently on important questions touching on the requirements for obtaining a patent (definition of patentable matter). This suggests strongly that the courts of other Member States, in the absence of any harmonising measures, could well come to widely diverging positions if and when confronted with cases to decide in this field. Thus, patentees and the public at large who may be users of patentable matter currently lack certainty as to whether in the event of litigation patents which have been granted in this field will be upheld.")
  (ML MeW "Moreover, the existence of such uncertainty and divergences in legal protection can have a real and negative effect on investment decisions and free movement of goods within the internal market. The most obvious example of this can arise where a product is held to be patentable in the jurisdiction of one Member States and not in another. The competitive environment for innovative products in this situation will be radically different depending upon whether or not they are protected, while unlicensed copies will be prevented from passing across the Community's internal frontiers from Member States where protection has been denied to those where it exists. Companies considering the location of development facilities or the entry into new markets are also likely to be influenced in their decisions by the degree of certainty in the extent to which the local courts would give protection to computer-implemented inventions.")
  (ML Itt "It should also be recalled that patents can be obtained by a purely national route without the involvement of the European Patent Office. The above arguments concerning divergences between national laws apply equally in such situations, but there is the additional factor that the applications will be fully processed and granted exclusively according to national laws. Thus even the unifying factor of the EPO as a single granting authority will be absent, with the consequence that members of the same patent %(q:family) in different countries (i.e. patents all relating to the same invention and stemming from a single original application) could be granted from the very outset with very different scopes of protection.") ) 
 (ML ToW3 "To summarise: The laws are the same in all countries, but the question of whether the law-violating EPO caselaw should be followed or not has created confusion.  Normally, a need for %(q:harmonisation) may arise when the countries have different laws.  In this case the laws are already unified and %(q:harmonisation) serves only to preempt a democratic decisionmaking about what the rules should be to replace this with debates on directive formulating grammar, to be conducted in a sphere of international expert circles which is free from the constraints of national constitutional democracy.")
 )

(l (spaced "(a)" (ML Den "Differences between U.K. and EPO case law")))

(l
(ML Scm "Significant differences exist between the case law of the U.K. courts and that of the EPO Board of Appeal as regards computer-implemented inventions in the field of other classes of excluded matter. Under U.K. jurisprudence (in contrast to EPO case law), a computer program related invention that amounts to no more than, for example, a method for doing business or a mental act, is unpatentable even if a technical contribution can be found. This is illustrated by Merrill Lynch, for business methods, and by Raytheon Co's Application, for mental acts.") 
(ML Ata "As to the specific differences which exist between the case law of the U.K. courts and that of the EPO Board of Appeal, these concern the manner in which the law is interpreted in relation to excluded matter in general. Under U.K. jurisprudence (in contrast to that of the EPO), a computer program related invention that amounts to, for example, a method for doing business or a mental act, is considered unpatentable even if a technical contribution (in terms defined in this Directive) can be found. This is illustrated by Merrill Lynch, for business methods, and by Raytheon Co's Application, for mental acts.")
(ML IiW "In other words: this directive proposal forces British courts to accept business method patents as they have been granted by the EPO in recent years.") )

(l (spaced "(b)" (ML BeG "Business methods: Differences between U.K., German and EPO case law")))  

(l
(filters ((aa ahs 'bpagt-autabs99)) (ML FWn "Further differences exist between the case law of U.K. courts and the German Federal Patent Court as regards the patentability of methods for doing business. In particular, German jurisprudence does not appear to exclude the possibility that business methods can be patentable even if the only contribution that the invention makes is non-technical. This is in contradiction to the U.K. approach of Merrill Lynch referred to above. Relevant cases include the %(aa:Automatic Sales Control) case and %(sa:Speech Analysis Apparatus). The EPO Board of Appeals, on the other hand, has clearly stated that an essentially economic improvement cannot contribute to inventive step."))
(ML OWc "On the other hand, it had been thought that German jurisprudence did not exclude the possibility that business methods having a technical aspect could be patentable even if the only contribution that the invention makes is non-technical. Such an interpretation would open the door to significant extension of patentability into this field. Relevant cases include the %(q:Automatic Sales Control) case and Speech Analysis Apparatus. While the Bundesgerichthof recently clarified the position by affirming that the correct approach is the one adopted by the EPO Board of Appeals and this Directive, namely that an inventive technical contribution is an essential prerequisite for inventive step, this example clearly illustrates the potential for judicial interpretation to develop the law in such a manner as to result in major changes to the scope of patentability at the national level.")
(al
 (filters ((as ah 'bpatg-autabs99) (sa ah 'bgh-sprach00) (rn ah 'grur-nack00)) (ML Ttu "The recent %(as:Automatic Sales Control) (BPatG/21 1999) and %(sa:Speech Analysis Apparatus) (BGH/10 2000) cases have been %(rn:criticised) for the chaotic situation which they create in legal theory.  The Speech Analysis reasoning is systematically coherent in that, unlike the EPO/CEC/BSA doctrine it maintains the concept of %(q:invention), but it seems to overtly assert that business methods are inventions, which again is politically undesirable at the moment even for the staunchest patent extremists in Europe."))
 (ML Tsp "The CEC/BSA forgets to note that in Germany there is also the 17th senate of the Federal Patent Court which has consistently refused to follow the adventurous doctrines of the two other courts responsible for the above-mentioned decisions.  Thus there is not a question of disunity between national jurisdictions but rather one of confusion within all jurisdictions.  The first step here is to bring national jurisdictions in order using the means of constitutional democracy which are available only at the national level.  Only the second step can be %(q:harmonisation).") ) )

(spaced "(c)" (ML Dgl "Differences regarding the allowable claims"))

(l
 (filters ((I ah 'epo-t971173 "I") (II ah 'epo-t970935 "II")) (ML IoW "In addition to differences in the assessment of the patentability criteria, the administrative practices of the U.K. Patent Office and the EPO on one hand and those of other patent offices on the other differ with respect to the possible claims. While the U.K. allows the program product claims in the form approved in the two EPO Board of Appeal Computer program product %(I) and %(II), there is no suggestion that other Member States, as of yet, appear prepared to admit such claims.")) 
 (filters ((bs ahs 'bgh-suche01)) (ML Ieo "In addition to differences in the assessment of the patentability criteria, there is uncertainty with respect to the form of possible claims allowable. While the U.K. moved quickly to announce that its patent office would be allowing program product claims in the form approved in the two EPO Board of Appeal decisions Computer program product I and II, and this approach was recently also %(bs:endorsed by the German court), other Member States have not yet clearly followed suit."))
 (filters ((bs ah 'bgh-suche01)) (ML TsW3 "The CEC version hastily adds a reference to the newest %(bs:BGH/10 decision) and spreads optimism about member state courts' willingness to follow the EPO."))
) ) )

(adop (quot (ML TpW "The approach adopted"))

(kompar
(l2col (ML TpW "The approach adopted"))

(l2col
(ML Ipt "In the light of the Commission's findings on the impact of patents for computer-implemented inventions on innovation and competition and European businesses, the Commission believes that harmonisation should rely on the basic principles which have become established in the present case law and administrative practices. The proposal should thus not result in any sudden change in the legal position and in particular avoid any extension of patentability to computer programs %(q:as such).  An important safeguard is provided in Article 5 which mandates the Commission to report to the European Parliament and Council within three years of the coming into force of the Directive on the impact of computer-implemented inventions on innovation. In the light of the experience gained following the implementation of the Directive and the reports of the special panel, the Commission could consider proposing changes to the Directive.")
(ML Iii "In reality what this directive does is declaring programs as such to be patentable inventions.  The %(q:safeguard) in Article 5 only ensures that the CEC patent establishment will have further ressources available for engaging in patent expansionist propaganda activities, as it has been doing since the GreenBook and in the case of Gene Patent experience reporting.") )

(l
(ML Wct "While the patent system has to be adapted where appropriate to meet the need for protection of inventions in new fields of technology, such developments should be based on the general principles of European patent law as they have evolved historically. These are expressed, in particular, in the rule that an invention, to be patentable, must make a technical contribution to the state of the art. Based on this assumption, the case law and the administrative practices have, in recent years, developed patentability requirements for computer-implemented inventions.")
(ML Wyx "While the patent system has to be adapted where appropriate to meet the need for protection of inventions in new fields of technology, such developments should be based on the general principles of European patent law as they have evolved historically. These are expressed, in particular, in the rule that an invention, to be patentable, must make a technical contribution to the state of the art.") )
 
(l2col
(ML Hai "Having reached this stage, the Commission believes it is right that Europe should, for the time being at least, refrain from extending the patent protection available for computer-implemented inventions, for example by dispensing with the technical contribution requirement. Such a course of action would lead to the patenting of computer-implemented business methods. The U.S. experience in this field is still only recent and the impact of business method patents on the economy in general and on electronic commerce in particular cannot yet be fully assessed. Moreover, on this subject there is considerable debate in the U.S. where it has been argued that such patents may stifle e-commerce. An additional consideration is that a harmonisation in this sense would essentially create a set of rules for computer-implemented inventions separate from the more general principles of European patent law which have always required a technical contribution.") 
(ML TWx "This directive explicitely authorises the patenting of computer-implemented business methods.  The language here is deceptive.  Ordinary terms are used but with a meaning that is determined by certain doctrine debates, in which the term %(q:computer-implemented business method) refers to %(q:the obvious computerisation of an already known business method).  Patent applications of the latter type can be rejected anywhere, also in the US.")
)

(l2col
(ML Bfs "By codifying the requirement for a technical contribution in accordance with the current judicial interpretation of the EPC, the Directive should ensure that patents for %(q:pure) business methods or more generally social processes will not be granted because they do not meet the strict criteria, including the need for technical contribution.")
(ML TWb "There are no limiting criteria at all, let alone %(q:strict criteria).  Also the %(q:technical contribution) mumbo-jumbo does not come from the written law (EPC) but from the EPO.")
)

(l2col
(ML TvE "The above should ensure that patents for computer-related inventions in Europe have a positive impact on innovation and European businesses, and do not unfairly stifle competition.")
(filters ((es ah 'swpatsisku)) (ML Asf "All %(es:economic studies) on the subject more or less clearly suggest that software patents (not only business method patents, whatever the difference may be) have a questionable or negative impact on innovation and create unsolvable competition problems.") ) )

(l2col
(ML PrW "Patents for computer-implemented inventions are of importance for all enterprises in the software field, including SMEs. SMEs however often have little or no experience with the patent system. Therefore, they have frequently preferred to rely solely on copyright, which provides protection for the expression of computer programs as literary works. In order for SMEs to be able to make full use of the different possibilities offered by the patent system, they must have easy access to information about the means of obtaining patent protection, the benefits which this protection can provide, and the conditions for obtaining patents for their own inventions, for licensing them and for securing patent licenses from other patent holders. Member States have a role in evaluating whether the specific situation of patents in the field of computer-implemented inventions requires specific educational initiatives to be undertaken, in particular by their patent offices.")
(ML Ste "So the CEC does admit that it is proposing to change the %(q:status quo) in Europe.") )

(l2col
(ML Tii "The proposed Community action meets the subsidiarity criteria since its objectives cannot be achieved at national level. In fact, the case law and administrative practices of the Member States regarding computer-implemented inventions have been divergent for many years and there is no indication that these practices would converge without legislative action being taken. In the light of the cross-border impact of these practices, the objectives can, therefore, only be achieved by Community action.")
(filters ((ahs ah 'eukonsult00-angumema)) (ML Wdr "Wrong again:  the caselaw has been thrown into confusion by the EPO and by national courts which follow the EPO.  The lines of contention are not defined by national borders.  It would be possible for individual member states to put order into this confusion by giving some legislative hint for national courts to return to the law, such as removing the (materially redundant, only explanatory) %(q:as such) clause from their national patent law, as has been proposed by the german patent examiner Günter Schölch in his %(an:consultation submission)."))
)

(l2col
(ML Tte "The means of the Community action are also proportional to its objectives. The Directive is strictly confined to setting forth the basic rules regarding the patentability of computer-implemented inventions. To the largest extent possible, general patent law, as it relates both to procedure and to substance and as it has been interpreted by the national courts, will continue to apply and complement the Directive, provided that it is not contradictory to it.")
)

(l2col
(ML Hfe "Harmonisation and greater transparency should provide an incentive for European companies, and in particular for SMEs, to use such patents in order to fully exploit their computer-implemented inventions.") 
(ML Isi "Indeed.  This is however in sharp contradiction to Commissary Bolkesteins press statement which says that this directive will lead to a decrease in the number of patents.")) ) )

(harm (quot (ML Tar "The legal basis for harmonisation"))

(kompar
(l2col (ML Tar "The legal basis for harmonisation"))
(l
(ML Aer "As the legal basis for harmonisation, the Commission proposes to rely on Article 95 of the CEC Treaty. This was done in the case of other directives aligning national laws on intellectual property. This choice of legal basis has been upheld by the Court of Justice on a number of occasions.")
(ML Aaf "As the measure has as its object the achievement of the internal market by approximation of the provisions laid down by law, regulation or administrative action in Member States related to the patentability of computer-implemented inventions, the Commission proposes to rely on Article 95 of the CEC Treaty as legal basis for the harmonisation. This legal base has been relied upon in the case of other directives aligning national laws on intellectual property and, most importantly, in the recent Directive 98/44/CEC concerning the harmonisation of the patentability of biotechnological inventions. This choice of legal basis has been recognised under the circumstances which are present with regard to patentability by the Court of Justice on a number of occasions and especially with regard to the mentioned Directive 98/44/CEC in a recent ruling of the Court of Justice where the legal basis was examined thoroughly.")
)


(l2col (ML Etr "Explanation of the Directive article by article"))

(l2col (pf (ML ArtX "Article %d") 1))

(l2col
 (ML Tve "This article provides that a %(q:computer-implemented invention) is understood to mean any invention implemented on a computer or similar apparatus which is realised by a computer program. It is a consequence of this definition that the %(q:novelty) of any invention within the scope of the Directive does not necessarily need to reside in a technical feature. The employment of the expression %(q:prima facie) to qualify %(q:novel features) means that it is not necessary to establish actual novelty (for example through the carrying out of a search) in order to determine whether an alleged invention falls within the scope of this definition. As set out in recital 11 and Article 3, the presence of a %(q:technical contribution) is to be assessed not in connection with novelty but under inventive step. Experience has shown that this approach is the more straightforward to apply in practice.")
 (ML StW2 "Summary: no independent assessment of whether a patentable invention is present, declaration that computer programs as such are technical and patentable, codification of the EPO's questionable examining formalisms, regulation of basic concepts at a level of unclear and unstable formalisms.")
)  

(l2col (pf (mlval 'ArtX) 2))

(l
(al
(ML AWe "Article 2, in the context of Recital 6, reflects Article 27(1) of the TRIPS Agreement, according to which patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are susceptible of industrial application. A computer-implemented invention is defined as belonging to a field of technology.") 
(ML Itf "It is clarified by Recital 13 that an abstract algorithm does not fall within a field of technology.") )
(al
(ML Tol "This article defines certain terms used in the Directive. A %(q:computer-implemented invention) is stated to mean any invention implemented on a computer or similar apparatus which is realised by a computer program. It is a consequence of this definition that the %(q:novelty) of any invention within the scope of the Directive does not necessarily need to reside in a technical feature. The employment of the expression %(q:prima facie) to qualify %(q:novel features) means that it is not necessary to establish actual novelty (for example through the carrying out of a search) in order to determine whether an alleged invention falls within the scope of this definition. As set out in recital 11 and Article 4, the presence of a %(q:technical contribution) is to be assessed not in connection with novelty but under inventive step. Experience has shown that this approach is the more straightforward to apply in practice.")
(ML crW "%(q:Technical contribution) is defined to mean a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.") )
(ML Tto "This means that any programming idea which has not been literally documented anywhere and which would require the programmer to do a bit of ordinary thinking can be patented.")
)

(l2col (pf (mlval 'ArtX) 3))

(l
(al
(ML Att "Article 3 paragraph 1 provides that it is a requirement for the presence of inventive step that a computer-implemented invention must make a non-obvious technical contribution to the state of the art. This is to be regarded as a qualification of, and not a substitute for, the definition of inventive step as it appears in Article 56 of the EPC, which provides that an invention shall be regarded as having an inventive step if, having regard to the state of the art, it is not obvious to a person skilled in the art. This is effectively already a general requirement for all patentable inventions, although naturally, in the course of assessing the inventive step of inventions in fields where there is rarely any question of excluded matter (for example mechanical subject-matter), there is normally no need to consider whether a contribution to the state of the art is technical or not.")
(ML Tfl "Thus, a computer-implemented invention in which the contribution to the prior art does not have a technical character will be considered to lack inventive step even if the (non-technical) contribution to the prior art is not obvious. When assessing inventive step, the questions as to what is to be included in the state of the art and the knowledge of the skilled person must be determined according to the criteria applied when assessing inventive step in general (see for example Article 56 EPC, second sentence).")

(ML Ait "Article 3 paragraph 2 provides that in determining the technical contribution, the invention must be assessed as a whole. This is consistent with the decisions of the EPO Technical Boards of Appeal in Controlling Pension Benefits and Koch & Sterzel according to which there must be no assessment of a %(q:weighting) between technical and non-technical features in an attempt to determine which aspect makes the more important contribution to the invention's success.")

(ML Irs "It follows from the above that an invention, aspects of which lie in a field of subject-matter excluded under Article 52(2) (for example a method for doing business), may still be patentable if a non-obvious technical contribution is present. However, if there is no technical contribution, e.g. if the contribution to the state of the art lies wholly in non-technical aspects, as would be the case if the contribution to the state of the art comprised purely a method of doing business, there will be no patentable subject-matter. A further logical consequence of this approach is that although a valid claim may comprise both technical and non-technical features, it is not possible to monopolise the purely non-technical features in isolation from the technical features.")

(ML Tty "The term %(q:technical contribution) has been used in the case law of the EPO Boards of Appeals for many years. Consistent with the jurisprudence of the EPO, a technical contribution may result from%(ul|the problem underlying, and solved by, the claimed invention|the means, that is the technical features, constituting the solution of the underlying problem|the effects achieved in the solution of the underlying problem|the need for technical considerations to arrive at the computer implemented invention as claimed.)") 

(ML TvW "The technical contribution may constitute an alternative solution for an already solved technical problem or for achieving a technical effect that is already known.")
)

(ML AoW "Article 3, in the context of Recital 6, reflects Article 27(1) of the TRIPS Agreement, according to which patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are susceptible of industrial application. A computer-implemented invention is defined as belonging to a field of technology. However, an algorithm which is defined without reference to a physical environment does not meet the definition of %(q:computer-implemented invention) and does not fall within a field of technology.")

(ML Tat "This means that an algorithm needs only to be formulated in the standard language of computing in order to be patentable.  I.o.w. any algorithm is patentable.") )

(l nil (pf (mlval 'ArtX) 4))
(l
 nil
 (al
  (ML Asi "Article 4 paragraph 1 obliges Member States to protect computer-implemented inventions as any other invention, subject to the basic requirements of novelty, inventive step and industrial applicability as laid down in Article 52(1) of the European Patent Convention.") 
  (ML Pae "Paragraph 2 provides that it is a requirement for the presence of inventive step that a computer-implemented invention must make a technical contribution, that is, a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art (Article 2). This is to be regarded as a qualification of, and not a substitute for, the definition of inventive step as it appears in Article 56 of the EPC, which provides that an invention shall be regarded as having an inventive step if, having regard to the state of the art, it is not obvious to a person skilled in the art. This is effectively already a general requirement for all patentable inventions, although naturally, in the course of assessing the inventive step of inventions in fields where there is rarely any question of excluded matter (for example mechanical subject-matter), there is normally no need to consider whether a contribution to the state of the art is technical or not.") 
  (ML Tfl2 "Thus, a computer-implemented invention in which the contribution to the prior art does not have a technical character will be considered to lack inventive step even if the (non-technical) contribution to the prior art is not obvious. When assessing inventive step, the questions as to what is to be included in the state of the art and the knowledge of the skilled person must be determined according to the criteria applied when assessing inventive step in general (see for example Article 56 EPC, second sentence).")
  (ML Ait2 "Article 4 paragraph 3 provides that in determining the technical contribution, the invention must be assessed as a whole. This is consistent with the decisions of the EPO Technical Boards of Appeal in Controlling Pension Benefits and Koch & Sterzel according to which there must be no assessment of a %(q:weighting) between technical and non-technical features in an attempt to determine which aspect makes the more important contribution to the invention's success.")
  (ML Irs2 "It follows from the above that an invention, aspects of which lie in a field of subject-matter excluded under Article 52(2) (for example a method for doing business), may still be patentable if a non-obvious technical contribution is present. However, if there is no technical contribution, e.g. if the contribution to the state of the art lies wholly in non-technical aspects, as would be the case if the contribution to the state of the art comprised purely a method of doing business, there will be no patentable subject-matter. A further logical consequence of this approach is that although a valid claim may comprise both technical and non-technical features, it is not possible to monopolise the purely non-technical features in isolation from the technical features. ")
  (linul
   (ML TWj "The term %(q:technical contribution) has been used in the case law of the EPO Boards of Appeals for many years. Consistent with the jurisprudence of the EPO, a technical contribution may result from")
   (ML tnh "the problem underlying, and solved by, the claimed invention;")
   (ML tll "the means, that is the technical features, constituting the solution of the underlying problem;")
   (ML tie "the effects achieved in the solution of the underlying problem;")
   (ML tti "the need for technical considerations to arrive at the computer implemented invention as claimed.") ) ) )

(l (pf (mlval 'ArtX) 4) (pf (mlval 'ArtX) 5))
(l
 (al
  (ML IrW "In accordance with Article 27(1) of the TRIPS Agreement, patents have to be available for any inventions, whether they be products or processes.  Article 4 provides that a computer-implemented invention may be claimed either as a programmed computer or similar apparatus (i.e. a product) or as a process carried out by such an apparatus.")
  (ML ToW "The proposal has not followed the practice of the EPO in permitting claims to computer program products either on their own or on a carrier, as this could be seen as allowing patents for computer programs %(q:as such). %(s:Moreover this is still a relatively recent development which has not so far been tested in either the EPO's Enlarged Board of Appeal or the Member States' courts.)") )
 (al
  (ML IrW "In accordance with Article 27(1) of the TRIPS Agreement, patents have to be available for any inventions, whether they be products or processes.  Article 4 provides that a computer-implemented invention may be claimed either as a programmed computer or similar apparatus (i.e. a product) or as a process carried out by such an apparatus.")
  (ML ToW "%(s:It should be noted that) the proposal has not followed the practice of the EPO in permitting claims to computer program products either on their own or on a carrier, as this could be seen as allowing patents for computer programs %(q:as such).") )
 (filters ((eg ah 'epo-gl78)) (ML Htx "Here BSA/CEC apparently tries to codify a convenient misinterpretation of the %(q:as such) clause Art 52(3) EPC.  It is of course clear that a claim of the form %(bq:computer program, characterised by that it executes the method according to claim 1) violates Art 52.   From this it does however not follow that a method or apparatus claim to the same object would not violate art 52 EPC.  Indeed the %(eg:EPO Guidelines of 1978) were very clear on this point: %(bq:If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims.  For example, a claim to a computer characterised by having the particular program stored in its memory or to a process for operating a computer under control of the program would be as objectionable as a claim to the program %(e:per se) or the program when recorded on magnetic tape.)  Art 52 says that, while a technical invention may make use of a program, a programming solution as such is not an invention.  When there is no invention, there is nothing to claim, regardless of whether the claim is directed to the non-invention as such or to an apparatus whose only novel feature consists in the non-invention."))
)

(l nil (pf (mlval 'ArtX) 6))
(l 
 nil
 (ML AiW "Article 6 expressly preserves the application of the provisions on decompilation and interoperability in Directive 91/250/EEC.")
 (ML TWa "This is apparently a later insertion requested by GD Information Society, but itdoes not ensure that interoperable software may be published or used.") )

(l (pf (mlval 'ArtX) 5) (pf (mlval 'ArtX) 7))

(l
 (ML Ase "Article 5 requires the Commission to monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce. A report will be prepared and sent to the Parliament and the Council on the operation of the Directive within three years from the date by which Member States have to transpose it into national laws. This framework provides an important safeguard which should ensure that any negative effects of the Directive are detected and reported.")
 (ML Amn "Article 7 requires the Commission to monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.")
 (ML Tpt "This article authorises the CEC patent establishment to use more public money for their patent propaganda activities, but apparently the cost is zero, as stated later in the financial statement.")
)

(l (ML Ae7 "Articles 6, 7 and 8") (ML AsW "Articles 9, 10 and 11"))

(l2col
 (al 
  (ML TeW "These are standard articles governing the coming into force of the Directive and its transposition by the Member States.")
  (ML IWo "In order to implement this Directive, Member States will need to introduce new provisions in their patent laws which, in particular, make it clear that the patentability criteria for computer-implemented inventions are as set out in Articles 1 to 4 of the Directive. The Directive does not require action in respect of any of the other exceptions from patentability in the provisions of Member States' patent laws corresponding to Art. 52(2) of the EPC.") 
  (ML Bet "Beyond what is provided for in this Directive, the procedural and substantive legal rules of national patent laws and binding international agreements remain the essential basis for the legal protection of computer-implemented inventions.")
) ) 
) )
) )

(prop (ML TPo2 "The BSA/CEC Proposal(s) and FFII Counter-Proposal") 

(kompar
(l2col
(ML cNt "%(nl|Proposal for a|DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL|on the patentability of computer-implemented inventions)") 
(ML cNt "%(nl|Proposal for a|DIRECTIVE OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL|%(s:on the limits of patentability with respect to computer programs))") 
)

(l2col
(ML TEO "THE EUROPEAN PARLIAMENT AND THE COUNCIL OF THE EUROPEAN UNION,")
)

(l2col (ML HiW "Having regard to the Treaty establishing the European Community, and in particular Article 95 thereof,"))

(l2col (ML HWo "Having regard to the proposal from the Commission,"))

(l2col (ML HpW "Having regard to the opinion of the Economic and Social Committee,"))

(l2col (ML AtW "Acting in accordance with the procedure laid down in Article 251 of the Treaty,"))

(l2col (ML Wee "Whereas:"))

(l
 (ML Een "Effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.")
 (ML TWf "The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market.  Effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.")
 (ML TWf "The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context %(s:an adequate regulation of property in software creations) is an essential element for the success of the internal market.  %(s:Ensuring a level playing field where innovative software creators can prosper) is essential in order to maintain and encourage investment in this field.")
)

(l2col (ML Dao "Differences exist in the legal protection of computer-implemented inventions offered by the administrative practices and the case law of the different Member States. Such differences could create barriers to trade and hence impede the proper functioning of the internal market."))

(l2col (ML San "Such differences have developed and could become greater as Member States adopt new and different administrative practices, or where national case law interpreting the current legislation evolves differently.") )

(l 
 (ML TWe2 "The steady increase in the distribution and use of computer programs in all fields of technology and in their world-wide distribution via the Internet is a critical factor in technological innovation. It is therefore necessary to ensure that an optimum environment exists for developers and users of computer programs in Europe.")
 (ML Tos "The steady increase in the distribution and use of computer programs in all fields of technology and in their world-wide distribution via the Internet is a critical factor in technological innovation. It is therefore necessary to ensure that an optimum environment exists for developers and users of computer programs in the Community."))

(l2col
(ML TWv "Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer-implemented inventions and provide an incentive for investment and innovation.") 
(ML TWv "Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the %(s:law governing the limits of patentability) should be made transparent. The resulting legal certainty should %(s:stimulate investment and innovation in the area of software).")   
)

(l
 (ML Ttn3 "The European Community and its Member States are bound by the TRIPS Agreement, Article 27(1) of which provides that, subject to only to certain exceptions as set out in paragraphs (2) and (3) of that Article, patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application.  Moreover, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to inventions which are the subject-matter of this Directive.")
 (ML Tcn "The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/CEC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994)42. Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.")
 (filters ((tr pet (ah 'swpattrips "TRIPS"))) (ML Tmp "The Community and its Member States are bound by the %(tr:Agreement on trade-related aspects of intellectual property rights), approved by Council Decision 94/800/CEC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994)42. Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available without discrimination as to the field of technology.  %(s:This means that technical inventions, including those that use a computer, must be patentable no matter to which %(tpe|technical field|e.g. mechanical, electrical, chemical, biological engineering) they belong.)")) )


(l2col
 (ML UtW "Under the European Patent Convention (EPC) and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology.")
 (ML UtW "Under the European Patent Convention (EPC) and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This %(s:exclusion), of course, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such.  %(s:Thus it might not apply if the invention does not consist in a computer program but in a %(tpe|technical invention|e.g. chemical recipe) which may be implemented under program control).")
 )

(l2col
 (ML PWW "Patent protection allows innovators to benefit from their creativity.  Whereas patent rights protect innovation in the interests of society as a whole; they should not be used in a manner which is anti-competitive.")
 (ML PWW "Patent protection %(s:should make it easier) for inventors to benefit from their %(s:achievement).  %(s:In order to ensure that) patent rights also serve the interest of society as a whole, they must be carefully %(s:designed).  %(s:In particular, inevitable corrollaries of the patent system such as restriction of creative freedom, legal insecurity and anti-competitive effects must be kept within reasonable limits).")	
)

(l2col
(ML IWo2 "In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, the expression in any form of an original computer program is protected by copyright as a literary work.  However, ideas and principles which underlie any element of a computer program are not protected by copyright.")
(ML IWo2 "In accordance with Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs, the expression in any form of an original computer program is protected by copyright as an %(s:individual creation).  Ideas and principles which underlie any element of a computer program are %(s:explicitely exempted from protection under copyright).")

)

(l2col
(ML Iet "In order for any invention to be considered as patentable it is necessary that it should have a technical character, that is to say it belongs to a field of technology.")
(ML Iet "In order for any idea to be considered an invention in the sense of the patent system it is necessary that it should have a technical character, that is to say it %(s:is a technical invention).  %(s:Every technical inventions belong to a field of technology, but not every innovative idea that can be associated to a field of technology is also a technical invention).")

)

(l2col
(ML Aon "Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they must make a technical contribution to the state of the art.")
(ML Aon "Programming is not a field of technology.  However computer programs are commonly used in many fields of technology.  In order to be considered as a technical invention, a computer-implementable teaching, just like any other teaching, must represent a technical contribution to the state of the art.")
)

(l2col
 (ML Ahi "Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.") 
 (ML Ahi "Accordingly, a teaching that does not make a technical contribution to the state of the art is not a technical invention and thus not patentable.") 
)

(l2col
 (tan
  (ML AaW "A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention.  However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.")
  (filters ((dk ah '(anch swpatcusku knuth02))) (ML Tcy "This means that while %(q:1 + 1 = 2) is considered %(q:abstract) and %(q:inherently non-technical), %(q:1 apple + 1 apple = 2 apples) may be considered to be a teaching in the field of bio-technology.  Donald Knuth, guru and god of computer science, remarks about this kind of reasoning: %(bq:Therefore the idea of passing laws that say some kinds of algorithms belong to mathematics and some do not strikes me as absurd as the 19th century attempts of the Indiana legislature to pass a law that the ratio of a circle's circumference to its diameter is exactly 3, not approximately 3.1416.  It's like the medieval church ruling that the sun revolves about the earth.  Man-made laws can be significantly helpful but not when they contradict fundamental truths.)  Matters may become even worse when such man-made laws are said to be intended for %(q:harmonisation and clarification).")) )
  (ML AaW "A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may %(s:contribute to our knowledge about physical causalities) and thereby constitute a patentable invention.  However, %(s:an algorithm, regardless of whether the symbolic entities of which it is composed can or should be interpreted as referring to a physical environment, is inherently non-technical) and cannot therefore constitute a patentable invention.") )

(l2col
 (ML Tet "The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive.")
)

(l
(al
(ML Tel2 "The Community's legal framework for the protection of computer-implemented inventions can be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not so susceptible.")

(ML AmW "Applying the above principles to eliminate the current differences in the legal protection of computer-implemented inventions and to provide for transparency of the legal situation should also improve the competitive position of European industry in relation to its major trading partners.")

(ML TsW "This Directive shall be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty as well as the relevant provisions of the Member States' laws with respect to competition and fair business practices.")

(ML Tmo "This Directive shall be without prejudice to other Community legislation such as Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks. The Directive shall also be without prejudice to Member States' laws relating to trade secrets or the law of contract.")
)

(al
(ML Tsc "This Directive should be limited to laying down certain principles as they apply to the patentability of such inventions, such principles being intended in particular to ensure that inventions which belong to a field of technology and make a technical contribution are susceptible of protection, and conversely to ensure that those inventions which do not make a technical contribution are not so susceptible.")

(ML Ttu2 "The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent.")

(ML TWn "This Directive shall be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.")

(ML Aod "Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.")

(ML SjW "Since the objectives of the proposed action, namely to harmonise national rules on computer-implemented inventions, cannot be sufficiently achieved by the Member States and can therefore, by reason of the scale or effects of the action, be better achieved at Community level, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary to achieve those objectives.")
)

(al
(ML Tho "This Directive should be limited to laying down certain principles %(s:concerning the limits of patentability with regard to computer programs), such principles being intended in particular to ensure that %(s:technical inventions are susceptible of patent protection and that rules of organisation and calculations are not so susceptible).")

(ML Trr "The legal security of %(s:European citizens) and the competitive position of European %(s:software creators and) industry in relation to Europe's major trading partners would be improved if it was %(s:made clear that Europe does not follow the USA in extending patentability beyond the field of technical inventions).")

(ML TWn "This Directive shall be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.")

(filters ((dp ah 'bgh-dispo76)) (ML Aod "This directive %(s:upholds the %(dp:principle of separation of property systems)).  It ensures that the patent system stays confined to the realm of technical inventions and that %(s:abstract thought and algorithms are safeguarded as a public domain within the area of copyright).  If there is any property system that could be allowed to set up some kinds of private enclosures on the public domain of algorithms, then that is copyright, and the extension should only happen by means of a legislative process which takes into account the central importance of information-related freedom rights in the constitutional order of the European Community's member states."))

(ML SjW "Since the objectives of the proposed action, namely to clarify the rules on the limits of patentability with respect to computer programs, can be more easily achieved at the Community level than by the individual member states, the Community may adopt measures, in accordance with the principle of subsidiarity as set out in Article 5 of the Treaty. In accordance with the principle of proportionality, as set out in that Article, this Directive does not go beyond what is necessary to achieve those objectives.")
)
)

(l2col (ML HTD "HAVE ADOPTED THIS DIRECTIVE:"))

(l2col (pf (ML ArtX "Article %d") 1))

(l2col (ML Sco "Scope"))

(l
(ML Tie "This Directive relates to computer-implemented inventions. The term %(q:computer-implemented invention) includes any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs.")
(al
 (ML Tsm "This Directive lays down rules for the patentability of computer-implemented inventions.") 
 (ML Aie "Article 2")
 (ML Dit "Definitions")
 (let ((type 'a))
   (linol
    (ML FWW "For the purposes of this Directive the following definitions shall apply:")
    (ML cWe "%(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;")
    (ML cWi "%(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.") ) )
)

(ant
(filters ((ep ah 'epo-gl78)) (ML Wge "The FFII version is based on the wording and spirit of the EPC and the %(ep:EPO's examination guidelines of 1978)."))
(linol
 (ML TwW "This directive relates to the limits of patentability with respect to computer programs.  For the purpose of this Directive the following definitions shall apply:")
 (ML Aso "A computer program is a rule for operating a computer that may take various forms, e.g. an algorithm, a flow-chart or a series of coded instructions which can be recorded on a tape or other machine-readable record-medium.")
 (ML AeW2 "A technical invention is a teaching about cause-effect relations in the use of controllable physical forces.")
) ) )

(l (pf (mlval 'ArtX) 2) (ML Aie2 "Article 3"))

(l nil (ML CeW "Computer-implemented inventions as a field of technology") (ML Peh "Patentable inventions as technical teachings"))

(l 
 (ML Ail "A computer-implemented invention shall be considered to belong to a field of technology.")
 (tan
  (ML Mue "Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.")
  (ML TWn2 "This apparently says that in order to qualify as a technical invention, a teaching only needs to be computer-implementable.  Thus anything is a technical invention.")
 )
 (ML MWw "Member states shall ensure that technical inventions are patentable regardless of whether they are implemented on a computer, and that, conversely, computer programs cannot be made patentable by claim-language formalisms that make them look like technical inventions.")
)

(l (pf (mlval 'ArtX) 3) (pf (mlval 'ArtX) 4))

(l (ML Tar2 "Technical contribution") (ML Cse "Conditions for patentability"))

(l
(ol
(ML ItW "In order to involve an inventive step, a computer-implemented invention must make a technical contribution. A %(q:technical contribution) is a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.")
(ML Tnc "The technical contribution must be assessed by consideration of the difference between the scope of the patent claim considered as a whole, which may comprise both technical and non-technical features, and the state of the art.") )
(tan
 (ol
  (ML Mii "Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.")
  (ML Mfm "Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.")
  (ML Tba "The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.")
  )
 (ML Tti "This paragraph removes the basic test of Art 52, namely whether there is an invention, from European patent law.  The term %(q:computer-implemented invention) just assumes that any idea is an %(q:invention) and that only novelty and non-obviousness need to be assessed.  It then creates further confusion by pretending to build the invention concept into the non-obviousness test.") )
(ant 
 (filters ((ep ah 'epo-gl78) (dp ah 'bgh-dispo76) (wt ah 'bgh-walzst80)) (ML Tes2 "The FFII version follows the wording and spirit of the EPC, the %(eg:EPO examination guidelines of 1978) and german caselaw of the 1970s as expressed in the %(dp:Dispositionsprogramm) and %(wt:Walzstabteilung) decisions."))
 (ol 
  (ML MaW "Member states shall ensure that patents are granted only for technical inventions, which are new, non-obvious and industrially applicable.")
  (ML MWo "Member states shall ensure that technical inventions are understood to be technical teachings, i.e. teachings about controllable cause-effect relations of natural forces.")
  (ML IWn "In assessing the technical invention, one should disregard the form or kind of claim and concentrate on the content in order to identify the novel contribution which the alleged %(qc:invention) claimed makes to the known art.  If this contribution does not constitute an invention, there is not patentable subject matter." (de "Bei der Prüfung der Frage, ob eine technische Erfindung vorliegt, sollte man die Form oder die Art des Patentanspruchs außer acht lassen und sich auf den Inhalt konzentrieren, um festzustellen, welchen neuen Beitrag die beanspruchte angebliche %(qc:Erfindung) zum Stand der Technik leistet.  Stellt dieser Beitrag keine Erfindung dar, so liegt kein patentierbarer Gegenstand vor."))
  ) ) )

(l (pf (mlval 'ArtX) 4) (pf (mlval 'ArtX) 5))

(l2col (ML FWc "Form of claims"))

(l 
(ML AWr "A computer-implemented invention may be claimed as a product, in particular as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.")
(ML Msc "Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.") 
(ant 
 (filters ((ep ah 'epo-gl78)) (ML Am7 "The FFII proposal follows the letter and spirit of the %(ep:EPO's Examination Guidelines of 1978)."))
 (ML Aso "If the contribution to the known art resides solely in a computer program then the subject matter is not patentable in whatever manner it may be presented in the claims.  For example, a claim to a computer characterised by having the particular program stored in its memory or to a process for operating a computer under control of the program would be as objectionable as a claim to the program %(e:per se) or the program when recorded on magnetic tape." (de "Wenn der Beitrag zum bisherigen Stand der Technik lediglich in einem Programm für Datenverarbeitungsanlagen besteht, ist der Gegenstand nicht patentierbar, unabhängig davon, in welcher Form er in den Ansprüchen dargelegt ist.  So wäre z.B. ein Patentanspruch für eine Datenverarbeitungsanlage, die dadurch gekennzeichnet ist, dass das besondere Programm in ihr gespeichert ist, oder für ein Verfahren zum Betrieb einer durch dieses Programm gesteuerten Datenverarbeitungsanlage in Steuerabhängigkeit von diesem Programm ebenso zu beanstanden wie ein Patentanspruch für das Programm als solches oder für das auf Magnettonband aufgenommene Programm.")) ) )

(l nil (pf (mlval 'ArtX) 6))

(l nil (ML RWi "Relationship with Directive 91/250 CEC"))

(l nil 
(tan
(ML Aod2 "Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.")
(ML Trs "The CEC version means that interoperable software may not be written and distributed without license when a standard is patented.  Only the use of patents for reverse engineering is allowed.  This is not seriously contested anyway.")
)

(ML Aod2 "Members states shall keep the spheres of copyright and patents clearly separated.  Property in computer programs is acquired and regulated through copyright.  Property in technical inventions is acquired and regulated through patents.   Property that has been acquired through one system may not be restricted by another.  Aspects of a computer program that can not be appropriated through copyright can also not be appropriated through patents, utility certificates or any other property regime.")
)

(l nil (pf (mlval 'ArtX) 7))

(l nil (ML Mir "Monitoring"))

(l nil
   (ML Tnt2 "The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.")
   (ML Tep "Monitoring is a good idea.  But the European Commission in its current setup cannot be trusted to carry out any meaningful monitoring on the subject of patentability.  The negative effects of software patents are clearly visible and have been studied, but the European Commission has shown no interest in these studies.  This is evidently just another authorisation for the patent family at the European Commission to use public funds for its patent promotion activities in the same way as it has been doing this in its recent interim report on the marvelously beneficial effects gene patents.")

) 

(l (pf (mlval 'ArtX) 5) (pf (mlval 'ArtX) 8))

(l2col (ML ReW "Report on the effects of the Directive"))

(l
 (ol
  (ML Tnt "The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.")
  (ML Typ "The Commission shall send to the European Parliament and the Council within three years as from the date specified in Article 6(1) a report assessing the impact of patents for computer-implemented inventions on the factors mentioned in paragraph 1.")  
  (ML Iir "In this context, the report will also address the questions as to whether the rules governing the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and whether difficulties have been experienced in respect of Member States where the requirements of novelty and inventive step are not examined prior to issuance of a patent, and if so, whether any steps are desirable to address such difficulties.") )
 (let ((type 'a))
   (linol
    (ML Tme "The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on") 
    (ML teW "the impact of patents for computer-implemented inventions on the factors referred to in Article 7;")
    (ML wtn "whether the rules governing the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and")
    (ML wra "whether difficulties have been experienced in respect of Member States where the requirements of novelty and inventive step are not examined prior to issuance of a patent, and if so, whether any steps are desirable to address such difficulties.")))
 (filters ((tp ah 'swpatpikta)) (ML Slt "Such studies already exist.  Since this directive merely enshrines the recent illegal EPO practise, there is already a lot of material to study.  We have shown what the results of this practise are:  %(tp:an ever exploding number of software and business method patents, already more than 30000 by late 2000, mostly on trivial ideas).  The CEC and BSA have been challenged to list some real examples of EPO patents which they think are good for stimulating innovation.  They have not listed a single one.  They know that the EPO produces harmful junk.  They have closed their eyes before this evidence.  The directive proposal avoids all references to the existing research about the nature of the EPO's patents.  There is no reason to believe that this will change in 3 years.  Moreover, it can be expected that some of the worst results will show up only after the 3 years."))
 )

(l (pf (mlval 'ArtX) 6) (pf (mlval 'ArtX) 9))

(l2col (ML Iet2 "Implementation"))

(l
 (spaced "1." (ML Mnl "Member States shall bring into force the laws, regulations or administrative provisions necessary to comply with this Directive not later than ... They shall forthwith notify the Commission thereof."))
 (spaced "1." (ML Mvf "Member States shall bring into force the laws, regulations and administrative provisions necessary to comply with this Directive not later than [DATE (last day of a month)]. They shall forthwith inform the Commission thereof.")) )

(l
(ML Wrp "When Member States adopt these provisions, they shall contain a reference to this Directive or shall be accompanied by such reference on the occasion of their official publication. The methods of making such reference shall be laid down by Member States.")
(ML WDi "When Member States adopt those provisions, they shall contain a reference to this Directive or shall be accompanied by such a reference on the occasion of their official publication. Member States shall determine how such reference is to be made.") )

(l2col (spaced "2." (ML Mia "Member States shall communicate to the Commission the provisions of national law which they adopt in the field governed by this Directive.")) )
 
(l (pf (mlval 'ArtX) 7) (pf (mlval 'ArtX) 10))

(l2col (ML EWW "Entry into force"))

(l2col (ML TWi2 "This Directive shall enter into force on the day of its publication in the Official Journal of the European Communities."))

(l (pf (mlval 'ArtX) 8) (pf (mlval 'ArtX) 11))

(l2col (ML Ars "Addressees"))

(l2col (ML Tsh "This Directive is addressed to the Member States."))

(l2col 
(nl
(ML Dau "Done at Brussels,")
(ML Fua "For the European Parliament")
(ML Fhu "For the Council")
(ML Pres "The President")
(mlval 'Pres)
))
)
;prop
)

(fins (tpe (quot (ML FIA "Financial Statement")) (ML BAo "BSA only"))

(colons (ML EoR "Editors Remark") (ML Tee2 "This statement is only found in the BSA version.  It has been mysteriously truncated from the CEC-adopted Directive Proposal."))

(spaced "1." (ML TOR "TITLE OF OPERATION"))

(ML Pdi "Proposal for a European Parliament and Council Directive on the patentability of computer-implemented inventions.")

(spaced "2." (ML BEI "BUDGET HEADINGS INVOLVED"))

(ML Non "None.")

(spaced "3." (ML LAB "LEGAL BASIS"))

(ML A5E "Article 95 of the CEC Treaty.")

(spaced "4." (ML DOW "DESCRIPTION OF THE OPERATION"))

(spaced "4.1." (ML Gaj "General objective"))

(ML Hea "Harmonisation and clarification of Member States' patent laws and practices concerning the patentability of computer-implemented inventions.")

(spaced "4.2." (ML PWn "Period covered and arrangements for renewal"))

(ML Uei "Unspecified.") 

(spaced "5." (ML CAE "CLASSIFICATION OF EXPENDITURE"))

(spaced "6." (ML TFN "TYPE OF EXPENDITURE"))

(spaced "7." (ML FWo "FINANCIAL IMPACT (on Part B)"))

(ML Non2 "None")

(spaced "8." (ML FEW "FRAUD PREVENTION MEASURES"))

(spaced "9." (ML EON "ELEMENTS OF COST-EFFECTIVENESS ANALYSIS"))

(spaced "9.1." (ML Siy "Specific and quantifiable objectives; target population"))

(ML BWe "By clarifying the legal framework concerning the patentability of computer-implemented inventions, the initiative should make it possible for businesses and in particular for SMEs all over Europe to make increased use of the possibility to obtain patents for such inventions.  Furthermore, by harmonising the patentability conditions, the proposed Directive should facilitate the cross-border exchange of patented software.") 

(ML Eos "European businesses should also benefit from the increased certainty which will be brought about by clarifying that computer-implemented business methods with no technical character (%(q:pure) business methods) cannot be patented. This will engender an environment in which innovative business methods can flourish without fear of damaging legal action.")

(spaced "9.2." (ML Gfo "Grounds for the operation"))

(ML Tra "The interested circles consulted have strongly asked for a harmonisation of law and practices on the subject which should also remove the ambiguity and legal uncertainty surrounding it.")

(spaced "9.3." (ML MWW "Monitoring and evaluation of the operation"))

(ML ArW "Article 5 of the proposed Directive provides for the Commission to report to the Parliament and the Council no later than three years after expiration of the time limit for implementation of the proposed Directive. The Commission will report through service papers made by the staff assigned to the administration of the operation. Any proposals for adjusting the proposed system could be put forward at that time.") 

(spaced "10." (ML ARI "ADMINISTRATIVE EXPENDITURE (PART A OF SECTION III OF THE GENERAL BUDGET)"))

(ML Nee "No effect.")

(spaced "10.1." (ML EWe "Effect on the number of posts"))

(ML Nee2 "No effect.")

(spaced "10.2." (ML Oia "Overall financial impact of additional human resources"))

(ML Nee3 "No effect.")

(spaced "10.3." (ML Iir2 "Increase in other administrative expenditure arising from the operation"))

(ML ntt "negigible effect on administrative expenditure.")

(ML IAE "IMPACT ASSESSMENT FORM")

(ML TWr "Title of proposal")

(ML Pnp "Proposal for a Directive on the patentability of computer-implemented inventions") 
(ML DWc "Document Reference Number")

)

(aims (tpe (quot (ML TPo "The Proposal")) (ML Aee "Advocative Postface, BSA only"))

(spaced "1." (ML Taa "Taking account of the principle of subsidiarity, why is Community legislation necessary in this area and what are its main aims?"))

(ML HWW "Harmonisation of the relevant parts of national patent laws can only be put into effect by Community action. The administrative practices and the case law of the Member States have been differing for many years.  While there has been some convergence between the practices of the EPO Boards of Appeal and the German Federal Court of Justice, there is no indication that practices would converge across the European Community without legislative action being taken.") 

(ML Tab "The impact on business")

(spaced "2." (ML Wat "Who will be affected by the proposal?"))

(ML WcW "Which sectors of business?")

(ML Fir "First, it is the software industry which should be able to derive advantage from increased legal certainty in relation to patents for software-implemented inventions and, relying on such patents, be provided with an incentive to increase investment and innovation. The clarification that entities involving no technical contribution (such as %(q:pure) business methods) cannot be monopolised should also encourage innovative developments in this sphere. These factors should bring about a positive impact also upstream, that is on suppliers of materials and on manufacturing and marketing services. Further, it should be to the benefit of the downstream side, namely for distribution, training and support services.")

(ML Sis "Second, increased software innovation is likely to enhance productivity, capability and competitiveness in virtually all sectors of business. IT, communications technology, and software, are the keys to improved European competitiveness They have helped making European business restructuring in the 1990s possible which was triggered by global competition and have brought about great productivity gains and enhanced employees' possibilities to communicate.")
  
(ML Tna "The above contributions to the economies of Western Europe on which software patents should have a positive impact have been identified in the context of the packaged software industry in a study commissioned by the Business Software Alliance.")

(ML SWa "Software innovation must continuously be harnessed in order to keep European enterprises competitive world-wide.")

(ML Wta "Which sizes of business (what is the concentration of small and medium-sized firms)?")

(ML Ans "All sizes of business can take advantage of the proposal because protection of computer-implemented inventions by patents is open to all of them. However, the proposal should be of particular benefit to small and medium-sized companies who play a major and rapidly increasing role in software innovation. They can strengthen their economic position by protecting the ideas and principles underlying their computer-implemented inventions (which cannot be protected by copyright) against appropriation by others. In the past, the software industry has lived largely without patents. However this will have made it easier for major players to take ideas, especially of SMEs, and market them without recompense to the originators. Further, while larger firms may be in a better position to accumulate patent portfolios and thereby bargain for cross-licensing, small firms may in practice find that they have few weapons other than patents to protect their inventions, and so be relatively more dependent upon them. Patents can also be crucial for start-ups in the software field to obtain venture capital. They can make it easier for SMEs to successfully participate in calls for tenders, facilitate their going public, and increase their value in take-over situations.")

(ML Mea "Many SMEs, however, are either unaware of the patentability of computer-implemented inventions or, in the alternative, are concerned about the potential effects of patents for such inventions. Member States will need to evaluate whether the specific situation of patents in this field requires specific educational initiatives to be undertaken, in particular by their patent offices.")

(ML WrW "With the above in mind, the Commission engaged a contractor to conduct a study on software patent awareness of SMEs including possible actions to improve this awareness. In the framework of this study, the contractor has produced a brochure for information of SMEs.") 

(linul
(ML Aln "As regards the impact on the open source community, who have raised concerns against the patentability of computer-implemented inventions, many of the negative comments from individuals and small companies are directed against those patents for computer-implemented inventions that would affect the dissemination (%(q:publication)) and use of programs running on a general-purpose computer. The alternative harmonisation proposal made by EuroLinux is not inconsistent with the grant of patents on %(q:traditional inventions which include a computer program, for example in the case of the chemical or mechanical industry). However, there are a substantial number of features provided by European patent laws of which the open source community can take advantage including") 
(ML pne "prior use rights allowing, under certain conditions, an inventor to continue to use his/her invention despite the fact that somebody else subsequently patented it;")
(ML poi "publication or public use of an invention preventing subsequent patent protection for that invention by a third party;")
(ML tmf "the definition of patent infringement: a program infringes only if fulfils a certain patented function in the way defined in the patent claim;")
(ML tia "the opposition procedure: detailed procedures vary, but all patent offices (including the EPO) offer the possibility of challenging the validity of a patent in formal proceedings and/or of filing observations relating to patentability before a patent is granted. In addition, granted patents may be challenged before national courts;")
(ML cWl "cross-licensing by which owners of two or more patents grant each other licenses; in certain circumstances compulsory licenses may be obtainable where a patent cannot be exploited without infringing an earlier one.")
)

(ML Aih "Are there particular geographical areas of the Community where the businesses are found?")

(ML Gon "Given the limited needs for technological equipment for much software development on one hand and the global communication and networking facilities provided by the Internet on the other, the geographical location, in many cases, is of secondary importance.")

(spaced "3." (ML Why "What will business have to do to comply with the proposal?"))

(ML Ifr "Increased legal certainty should provide businesses with an incentive to make wider use of patents for computer-implemented inventions. It is however up to them to judge whether a computer-implemented invention is of sufficient economic relevance to warrant initiating the patenting procedure. As businesses increase their use of patents for computer-implemented inventions, they will also have to monitor their competitors' patents in order to detect and avoid possible infringements. On the other hand, from such monitoring, businesses will be able to derive important information about new inventions and possibly also their competitors' business strategies.")

(spaced "4." (ML Wes "What economic effects is the proposal likely to have?"))

(ML Omy "On employment?")

(ML TnW "The software industry makes a significant contribution to CEC economies and creates a substantial and constantly growing number of highly skilled jobs in the software industry itself as well as upstream and downstream.")

(ML Tje "The study commissioned by the Business Software Alliance quoted above estimates that the packaged software industry generated $ 37.0 B in sales and created 334,181 jobs in Western Europe in 1996. Assuming an annual rate of market growth of 10 % and a concurrent rise in employment of only 5 %, this would allow to expect a further 92,283 jobs by the end of the period from 1996-2001, that is total employment of 426,464 and an overall market of $ 59.8 B by 2001. Direct employment by packaged software publishers in Western Europe amounted to 45,388 in 1996.  Estimations for upstream employment were 81,016 and for downstream employment 207,777. These estimates were conservative. A study by Datamonitor concluded that the number of packaged software workers in Western European countries will grow by between 24% and 71% from 1999 to 2003, with an average of 47%. A further conclusion is that each packaged software job creates 2-4 jobs in the downstream economy and 1 job in the upstream economy.")

(ML IWm "It is not possible to forecast with any certainty the employment growth that could result from this proposal. Nevertheless, the ambiguity of the present legal situation as well as the divergent case law and administrative practices addressed by this proposal have a detrimental effect on innovation. These conditions also tend to have a proportionally greater effect on smaller enterprises which may lack resources to pay for extensive legal advice. Currently, around 75 % of software patents in Europe are held by very large, often non-European companies. European companies, and in particular SMEs, may not derive full advantage from their computer-implemented inventions due to lack of knowledge about the legal possibilities and advantages of patenting, and, thus, cannot garner the maximum turnover and profits which in turn could create new jobs.")

(ML TWs "The present proposal will create an environment of greater legal certainty in which innovation is fostered and will therefore help to create employment.")

(ML Odf "On investment and the creation of new businesses?")

(ML Apt "Although there still is relatively low use by European independent software developers of patents in raising finance or in licensing, an increasing number of small firms in the European software business, and in particular start-ups, find patents to be a crucial part of their business strategy because they are essential to attracting venture capital to develop and market computer-implemented inventions, and/or to license competitors and/or to sell or license an innovation to a major firm. Many venture capitalists will not normally support new companies based upon new software products unless adequate protection, in particular by patents, is available. A substantial number of companies would not exist had it been impossible to obtain patents protecting their software innovations.")

(ML Oto "On the competitive position of businesses?")

(ML Ief "Internally (within the CEC), by stimulating competition through facilitating entry in the market by small innovative firms, European small independent software developers will be able to compete more effectively with big players.")

(ML Tec "The existence of an effective anti-trust regime provides an important safeguard to deal with abuses which might arise, for example if patented technology should form the basis of a standard (e.g. an interface or file format). In the future, the importance of de facto proprietary standards may decrease while electronic business customers increasingly demand open standards for interoperability across disparate platforms on the Internet. On the other hand, applications built on these platforms might, to a large extent, remain proprietary. To the extent that proprietary standards remain in place, other industries, such as the electronics industry, have shown that voluntary agreements such as patent pools or patent platforms can be adequate tools for managing complex essential patent portfolios owned by many different firms which are necessary for creating complex products and services.")

(ML IWW "Internationally, the proposal should improve the competitive position of European software businesses in the competition with our global trading partners, the U.S. and Japan where software patents are widely granted.")
  
(spaced "5." (ML Dti "Does the proposal contain measures to take account of the specific situation of small and medium-sized firms (reduced or different requirements etc)?"))

(ML Gei "Given the nature and scope of the proposal, it is not feasible to include explicit measures involving differential treatment of SME's. Nevertheless, such entities should benefit particularly from the increased legal certainty which will result from the implementation of the Directive (see above, at the end of section 2 and at section 4 (likely economic effects on investment and the creation of new businesses)).")

(ML Cut "Consultation")

(spaced "6." (ML Les "List the organisations which have been consulted about the proposal and outline their main views"))

(ML Tvn "The proposal itself has not been circulated to interested parties since the Commission still has to adopt it. However, the need for a Commission initiative in this field has been identified in a consultation process which the Commission started in 1997 with the Green Paper on the Community patent and the patent system in Europe. The European Parliament and the Economic and Social Committee have both supported the patentability of inventions involving computer programs. Moreover, the interested circles had strongly urged legislative action in conferences organised by the Luxembourg and U.K. Presidencies in co-operation with the Commission. These conferences were held in Luxembourg on 25-26 November 1997 and in London on 23 March 1998. In a follow-up communication to the Green Paper, the Commission took stock of the consultation process and stated that the patentability of computer programs was one of the priority issues identified during this process on which the Commission should rapidly submit a proposal. Organisations representing European businesses, namely UNICE and EICTA, continued to ask the Commission to take a legislative initiative on the issue. UNICE, for instance, in February 2000, renewed its call for swift action to remove ambiguity and legal uncertainty which surrounds the patentability of computer-implemented inventions. If rapid action were not undertaken, the respective market segment would be dominated by Europe's main trading partners, in particular Japan and the U.S. where there were fewer restrictions on patenting inventions relating to or relying on software.") 

(ML Toe "The Commission had also distributed a questionnaire on the main points that should be dealt with in the Directive. The answers received in 1999 have been taken into account in the present proposal.")  

(ML TOc "The services of the Commission organised a meeting with representatives of the open source community, namely a delegation of EuroLinux representatives, on 15 October 1999 in Brussels. On 18 November 1999, the Committee of the Regions gave its opinion on the issue. Both EuroLinux and the Committee have expressed concerns that software patents might impede the progress of innovation in the software field. These concerns have been taken into consideration in this proposal.")

(ML Ten "The Commission launched an independent study on the scope of harmonisation in the light of the recent developments in the United States. While the consultation on the Green Paper had clearly shown the need to harmonise and clarify the current legal situation, the purpose of the study on the economic impact of the patentability of computer-implemented inventions was to provide assistance in determining how extensive harmonisation should be. For this purpose, the study assessed the main consequences for innovation and competition, in particular for SMEs, of extending patent protection beyond current levels. The results of the study, as well as of other pertinent economic studies, have been taken into account in this proposal.") 

(ML Fei "Finally, the Commission conducted a consultation between October and December 2000 on the basis of a paper which was communicated to the Member States and made generally available in the Internet. This paper sought views on whether there was any need at all for action at the Community level, and in the event this question were to be answered in the affirmative, what the appropriate level would be. The paper then proceeded to set out in some detail the current state of the case law on patentability of software-implemented inventions as established within the EPO, and suggested on the basis of this a number of very specific elements which might figure in any harmonisation exercise based on this status quo. 1447 separate responses were received, which were analysed by a contractor and summarised in a report which has also been published. While opposition to patents relating to software was expressed by a large majority of the individual responses to the consultation, the collective responses on behalf of the regional and sectoral bodies, representing companies of all sizes across the whole of European industry, were unanimous in expressing support for rapid action by the Commission more or less along the lines suggested in the discussion paper.")
)

(fnot (ML Ftt "Footnotes")

(ML Chn "Cf. study by Booz Allen & Hamilton for the Dutch Ministry of Economic Affairs, The Competitiveness of Europe's ICT Markets, March 2000, at 10.")

(ML Pts "Packaged software in Western Europe: The economic impact of the packaged software industry on the combined economies of sixteen European countries September 2000 Datamonitor, London")

(ML Fir2 "For a definition of the term, see Art. 1.")

(ML ces "%(q:The Munich Convention). It entered into force on 7 October 1977. All 15 CEC Member States as well as Cyprus, Liechtenstein, Monaco, Switzerland and Turkey are contracting states.")

(ML Ocd "On the divergences in greater detail see below.")

(ML Ppt "Promoting innovation through patents: Green Paper on the Community patent and the patent system in Europe COM(1997) 314 final, 24 June 1997")

(ML PWW2 "Promoting innovation through patents: The follow-up to the Green Paper on the Community patent and the patent system in Europe COM (1999) 42 final, 5 February 1999.  The study was conducted by the Intellectual Property Institute, London, on behalf of the Commission and finalised in March 2000.") 

(ML OCt "Other pertinent economic studies which have been taken into account and which relate to the divergent U.S. situation include Cohen, Wesley M., Nelson, Richard R., and Walsh, John P., Protecting their Intellectual Assets: Appropriability Conditions and why U.S. Manufacturing Firms Patent (or not), Working Paper 7552, National Bureau of Economic Research, February 2000; Bessen, James and Maskin, Eric, Sequential Innovation, Patents, and Imitation, Working Paper, Department of Economics, Massachusetts Institute of Technology, January 2000; Jaffe, Adam B., The U.S. Patent System in Transition: Policy Innovation and the Innovation Process, Working Paper 7280, National Bureau of Economic Research, August 1999.")
 
(ML Ifl "In the wake of the decision of the U.S. Court of Appeals for the Federal Circuit, of 23 July 1998, in State Street State Street Bank & Trust Co. v. Signature Financial Group, Inc., 149 F.3d 1368, patent applications for business methods have soared.")

(ML StW "See study, at 5.")

(ML Ilt "Ibid., at 3.")

(ML IWe "Ibid., at 5 et seq.")

(ML Ilt2 "Ibid., at 3.")

(ML Ilt3 "Ibid., at 8.")

(ML Ilt4 "Ibid., at 35.")

(ML CWC "Computer program product I and II, T1173/97 of 1.7.1998, 1999 OJ EPO [609] and T0935/97 of 4.2.1999, [1999] R.P.C. 861. The holdings of the two cases are largely similar.")

(ML Ci5 "Controlling pension benefits system/PBS T-0931/1995 decision dated 8.09.1995")

(ML Srh "Supra. See also case T1002/92 where the EPO Board of Appeal made this criticism for the first time.")

(ML ToW2 "The claims have to be interpreted in the light of the description and the drawings relating to the invention. Cf., e.g., Art. 69(1) of the EPC.")

(ML Sed "Such expression alone cannot serve as disclosure of a respective invention; see, e.g., EPO Guidelines for Substantive Examination, C-II, 4.14a.")

(ML TWW "The law relating to copyright, as it applies to computer programs, was harmonised at Community level with the introduction of this Directive, Council Directive of 14 May 1991 on the legal protection of computer programs (91/250/EEC), [17.5.1991] OJ L 122, at 42. See Commission Report on the implementation and effects of Directive 91/250/EEC, COM(2000) 199 final of 10.4.2000.")

(ML VXW "[1989] RPC 569.")

(ML Vie "[1993] RPC 427, insofar confirming Wang Laboratories Inc's Application [1991] RPC 463.")

(ah 'grur-nack00 (ML CeE "Cf. in this sense Nack, Ralph, Sind jetzt computerimplementierte Gesch\344tsmethoden patentf\344ig? - Analyse der Bundesgerichtshof-Entscheidung %(q:Sprachanalyseeinrichtung), [2000] GRUR Int. 853."))

(ML VXR "[1999] GRUR 1078")

(ML VXR2 "[2000] GRUR 930")

(ML SCn "See e.g. Directive 89/104/EEC approximating the laws of the Member States relating to trade marks (OJ No L 40, 11.2.1989, at 1) Directive 91/250/EEC on the legal protection of computer programs (OJ No L 122, 17.5.1991, at 42) Directive 93/98/EEC harmonising the term of protection of coyright and certain related rights (OJ No L 290, 24.11.1993, at 9) and Directive 96/9/CEC on the legal protection of databases (OJ No L 77, 27.3.1996, at 20).")

(ML Sas "See opinion 1/94, Competence of the Community to conclude international agreements concerning services and the protection of intellectual property [15.11.1994] ECR I-5267, and Case C-350/92 Spain v Council [13.7.1995] ECR I-1985. See also Case C-376/98, Federal Republic of Germany v. European Parliament and Council of the European Union [5.10.2000], where this choice was rejected for reasons which do not apply here (certain prohibitions of that Directive were considered as helping in no way to facilitate trade in the products concerned, see para. 99; moreover the Directive was held not to ensure free movement of products which are in conformity with its provisions (para. 101) since Member States retain the right to lay down stricter requirements without the Directive ensuring the free movement of products which conform to its provisions, see paras. 103 et seq.).") 

(ML SWt "See note 20")

(ML Tl8 "T26/86 (21.5.87) [1988] OJEPO 19")

(ML S26 "See Vicom Case T208/84 (15.7.1986) [1987] OJEPO 14")

(ML OWC "OJ C, , p. OJ C, , p.") 

(ML OCj "OJ C, , p.")

(ML O7l "OJL 122 , 17/05/1991 p. 0042 - 0046")

(filters ((URL ah "http://www.bsa.org/statistics/index.html")) (ML Shy "See study of May 1998 by Price Waterhouse, %(q:The Contribution of the Packaged Software Industry to the European Economies), available at %(URL)."))

(ML SWt2 "See note 10")

(ML SWt3 "See note 40")

(ML SWt4 "See note 2")

(ML CWW "COM(1997) 314 final of 24.6.1997. The issue had already been addressed in the Commission %(q:questionnaire on Industrial Property Rights in the Information Society, supra, note 34.)")

(ML Ra1 "Resolution on the Commission Green Paper, A4-0384/98, Minutes of 19.11. 1998, paragraph 16, [1999] OJ EPO 197.") 

(ML Oe9 "Opinion of the Economic and Social Committee on the Green Paper, [27.4.1998] OJ C 129, at 8, points 1.14., 6.9.1.1. and 6.9.1.2.")

(ML SlW "See point 11 of the conclusions of this hearing, OJ EPO 1-2/1998, at 82.")

(filters ((URL ah "http://www.patent.gov.uk/softpat/en/frmain.html")) (ML TtW "The programme of the conference as well as transcripts of the speeches given there are accessible on the world-wide web at %(URL)."))

(ML C4f "COM(1999) 42 final of 5.2.1999.")

(filters ((URL ahh "www.eicta.org")) (ML SIa "See, e.g., the EICTA position statement at %(URL)."))

(filters ((URL ah "http://eurolinux.ffii.org/news/euipCAen.html")) (ML TWo "The representatives of EuroLinux have published an unofficial, non-authorised report of the meeting on the web site of the EuroLinux Alliance at %(URL)."))

(ah '(anch swpatcusku cor) (ML OEn "Opinion of the Committee of the Regions on `The competitiveness of European enterprises in the face of globalisation - How it can be encouraged', OJ C 57, 29.2.2000, at 36 et seq., points 7.4. and 8.20."))

(ML SnW "See note 11.")

(ML Ibi "Ibid.")

(ML SWt5 "See note 9")
)
)
;text
)

(help (ML Wya "What to do, how you can help")
(ul
(ah 'euluxpet (ML ste "sign the petition"))
(filters ((mr ah 'swpatmrilu) (wg ah 'swpatgirzu)) (ML jlf "join our %(mr:mailing lists) and %(wg:work groups)."))
(ML LsW "Link to this page from your page.")
(ML iru "inform yourself and explain the danger to others, get your software company / your parliamentary deputy / etc to support our campaign, get media interested, write articles, especially in situations such as this one.")
(filters ((dp ah 'swpatkunst)) (ML aft "acquire and distribute %(dp:drawings and pamphlets), create some yourself."))
(filters ((ef ah 'indprop-swpat0202-faq)) (ML Dtm "Draft an de-desinforming FAQ, basically on the same questions as the %(ef:CEC FAQ).  Write other documents which might be published here.  Sumbit them as plain text files."))
(filters ((sm ah 'swpikmupli)) (ML MiW "Make a list of software patents that are pure software, and challenge the EU to say whether the directive would allow them.  If they say %(q:No), the next step would be to say, %(q:You should say this explicitly in the directive--please list these examples and say that the general policy must be designed so that these will not be patentable.)  The list can be based on the %(sm:real examples), but they should be simplified, anonymised, somewhat modified, and the prior art should be explained clearly, so that the only remaining question is whether there is a %(q:technical invention) (or a %(q:technical contribution))."))
(ML Hce "Help us mobilise the press and politicians.  Instructions on how to procede can be obtained after contacting us.")
(ML Ira "If you are a journalist, uphold the ethics of your profession: be critical,  distinguish between other people's opinion and your own, don't decorate yourself with the work of others, point to sources.  Don't worship power or money (nor Robin Hood), don't cultivate clichees.  Don't transscribe anybody's press releases, be critical to Bolkestein & Co and to us, don't believe any apparatchiks from BSA & Co who say that the industry wants patents or applauds Bolkestein's paper.")
(ML Dsa "Demand a written position statement from your government.  Bolkestein will convene a meeting of governmental patent representatives on March 4th.  Your government must give these patent officials clear written instructions.   Since they tend to obey only the patent establishment and disregard written instructions from their government, the government must also publish these instructions.")
(filters ((mo ah 'epue52moses) (do ah 'epue52)) (ML Drh "Demand from your national legislators that the much abused %(q:as such) clause be deleted from your national patent law's equivalent of Art 52 EPC.  The %(q:as such) clause is merely explanatory.  There is nothing wrong with it, but it has become the basis of a %(mo:whole system of illegal caselaw).  This would send a signal to your national judges, and it would be simple to do.  There was never a need for European-level harmonisation.  Explain that the CEC's patent lawyers played foul from the beginning to the end, that they have disqualified themselves, and that the BSA paper must be the end of this whole shameful exercise.  A discussion about the limits of patentability can and should of course be conducted at the European Community level.  It must be conducted by economists and software people.  Patent lawyers should stay outside for a while.  Bolkestein, Kober (EPO president) and Liikanen owe the public some explanations.  A criminal investigation for misconduct, illoyalty and possibly corruption should be envisaged.  The degree of illoyalty exposed in this analysis is difficult to explain without the assumption of corruption.  If you are an investigative journalist, please spend some time on these questions."))
(filters ((em ah 'euluxmembers) (EL ah 'eurolinux "Eurolinux Alliance")) (ML doc "donate money to the Eurolinux campaign by donating it to a %(em:member organisation in your country) with the purpose specification %(e:eurolinux) and notifying the %(EL) of this donation."))
(ML Wii "Are you an organisational talent?  Here is a great challenge.  Look, we are the people, we are the software creators, we are even what the CEC/BSA call the %(q:economic majority).  But we are by far not as well organised as they.  Help us turn our 100000 supporters and 300 corporate sponsors into a power that will no longer be ignored.  It may be a turning point on the way to a better Europe.")
(ah 'swpatgunka)
)
)

(ref (ML Fea "Further Reading")

(docmenu-large)

(linklist
'swpukpo026
(l (ah "http://lenz.als.aoyama.ac.jp/Stellungnahmen/Sink_the_software_patent_propasal.htm" (colons "Prof. Dr. iur. Karl-Friedrich Lenz 2002-03-01" "Sinking the Proposal")) (ML lpa "lists some legal and constitutional arguments to explain why this proposal is a legal and political scandal and cannot form the basis of any negotiations"))
(l (ah "http://www.bsa.org/europe-eng/press/newsreleases/2002-02-21.936.phtml" "BSA Europe - Press Release - BSA Responds to EU Software Patent Directive Adoption") (ML Mil "Mingorance denies our allegations that he wrote the proposal, arguing that he would have written the %(q:form of claims) section differently.  We think that the decision to not follow the EPO on the claim form question was indeed taken by the CEC.  We do not doubt that the CEC played an important role in shaping the proposal.  But so did Mingorance."))
(l (ah 'goltzsch-eubsa0202) (ML AEu "An journalist reports that he just phonecalled the European Commission to find out whether the BSA version that Eurolinux proposed was really the version that the CEC was going to publish that day.  He was told by the Commission's press speaker that the version which Eurolinux published was not theirs but one %(q:from the industry).  Several French journalists who called on the same morning received the same answer.  One even received the answer %(q:no, that draft is not from us but from BSA)."))
(l (ah "http://zdnet.com.com/2100-1104-842159.html" "ZDNet News: Row erupts over European patent plan") (ML AvA "A fairly comprehensive account of the BSA/CEC proposal scandal"))
'eubsa-faq
(l (ah "http://petition.eurolinux.org/pr/pr17.html" (colons "Eurolinux 2002-02-20: Collusion Discovered between BSA and Euorpean Commission")) "Eurolinux Press Release")
(l (ah (case lang (de '(mail neues 2002 2 7)) (t '(mail news 2002 2 7))) (ML EcB "CEC Press Release inconsistent with CEC/BSA directive content" (de "EUK-Presseerklärung steht im Widerspruch zum Inhalt des RiLi-Vorschlages"))) 
   (ML EWs "Eurolinux Warning to journalists issued on 2002-02-20 shortly after publication of CEC/BSA directive proposal")
)
(l (ah "http://www.patent.gov.uk/about/ippd/issues/index.htm#Software Patents" (ML Rsr "Robin Webb (UK PTO & Gov't) 2002-02-20: proposes to remove all limits to patentability and to rewrite Art 52 EPC so as to reflect EPO practise"))

   (filters ((pr ah "http://www.patent.gov.uk/about/ippd/issues/software.htm")) (ML TfW "The UK PTO conducted its own consultation, which showed an overwhelming wish of software professionals to be free of software patents.  But the UK PTO, speaking in the name of the UK government, reinterprets this as a legitimation to remove all limits on patentability by modifying Art 52 EPC at the Diplomatic Conference in June 2002.  The %(pr:proposal) has one argument going for it:  while rendering Art 52 meaningless, it at least brings clarity.  It describes in fairly straightforward (and yet still sufficiently deceptive) terms what the EPO is doing and what the patent movement wants.  This paper was presented by the UKTPO on the same day as the EU-BSA Directive Proposal.  The UKPTO had held a parallel consultation exercise in Great Britain.  Its exercise ignored public opinion using the same double-talk about %(q:technical contribution) and by selective choice of examples made sure that this talk is misunderstood by the public.  Yet the UKPTO has shown the Commission that even massive fraud and %(e:theft of intellectual property with with a further legal effect), jointly committed by the EPO, the CEC and the UKPTO against the european software industry and the european public, can be carried out in a graceful and mannered fashion.")) )
(l (ah "http://solutions.journaldunet.com/0202/020222_eurolinux.shtml" "L'innovation logicielle remise en cause par la Commission européenne") (ML Fti "French news report saying that the CEC proposal fails to bring any clarity to the debate"))
(l (ah "http://www.europeshareware.free.fr/pages/pr_swpat3.html" "Une directive contre le logiciel européen") (ML Jti "Joint statement of the European shareware associations condemning the directive proposal as a hostile act against the creators of software in Europe"))
'warn01C
(l (ah "http://pauillac.inria.fr/~lang/arte/brevet/directive.html" (colons "Bernard Lang" "The CEC/BSA Directive Proposal and the reactions")) (ML Pti "Pointers to more press reports, especially from France, including some clueless ones"))
'swpatcusku
'swpatsisku
'swpatpleji
'swpatkorcu
)
;ref
)
;sects
)
;eubsa-swpat0202 
))

(mlhtdoc 'pvda020220
 (ML title "Partij Van De Arbeid 2002-02-20: Ontevreden over Concept EU Richtlijn Software Octrooien" (en "Dutch Labor Party 2002-02-20: Criticism of EU Software Patentability Proposal") (de "Niederländische Sozialdemokraten 2002-02-20: Kritik des Swpat-Richtrlinienvorschlages") (fr "Parti du Travail Néerlandais 2002-02-20: Critique du Propos de Directive Brevets Logiciels"))
 (ML descr "Dutch Labor Party criticises that the European Commission's draft in no way fulfills the Dutch Parliament's demands for strict patentability standards and on the contrary is cause of concern and necessitates a lot of sharp questions, which the government should ask Bolkestein.  The PVdA position is based on the recommendations of a joint work group of the largest software industry association FENIT.nl and the Opensource Association VOSN.nl" (de "Die Regierungspartei PvdA kritisiert in einer ersten Presseerklärung, dass der Entwurf der europäischen Kommission keineswegs die vom niederländischen Parlament geforderten Verbesserungen der Praxis des EPA bringt und im Gegenteil sehr viele Fragen aufwirft. In Holland hat auch der Branchen-Gesamtverband FENIT (vgl Bitkom und EICTA) die EPA-Praxis kritisiert und unterstützt die Forderung der Sozialdemokraten, dass vor einer überzeugenden Lösung des Problems der fehlenden Erfindungshöhe nicht an die Legalisierung von Softwarepatenten gedacht werden kann."))
 nil

 (ah '(mail swpat 2002 2 85))
 
 (ML PET "PERSBERICHT PVDA")

 (ML PVC "PARTIJ VAN DE ARBEID ONTEVREDEN OVER CONCEPT EU RICHTLIJN SOFTWARE OCTROOIEN")

 (ML Vtk "Vandaag is met een persbericht de concept EU richtlijn over software octrooien verschenen. Deze richtlijn zal een dwingend karakter krijgen binnen de EU en nationale wetgeving zal daaraan moeten worden aangepast. Ondanks grote bezwaren van meerdere Europese landen biedt het concept veel ruimere mogelijkheden o software te patenteren dan tot nu toe. De PvdA heeft zich meermalen uitgesproken tegen de mogelijkheid om triviale software octrooien erkend te krijgen. De PvdA is voorstander van patenten ter bescherming van reele investeringen in innovatie. Maar dan moet het wel om technische vernieuwing en aantoonbaar werkende produkten gaan. Anders ontstaat er ruimte om slechts papieren ideeen te octrooiieren en vervolgens kleine bedrijven te dwingen daarvoor te betalen. De PvdA wenst de open toegang tot kennis en innvatie voor alle ondernemers te beschermen. Op initiatief van de PvdA hebben de Vereniging Open Source Nederland en de FENIT, brancheorganisatie van softwarebedrijven gezamenlijk criteria ontwikkeld waar software aan moet voldoen voor er een patent verstrekt kan worden.")

 (ML ZnW "Zowel kleine zelfstandige softwareontwikkelaars als grotere softwarebedrijven erkennen het belang van harde scherpe criteria voor softwarepatenten om te zorgen dat echte inovatie beloond wordt en machtsvorming wordt tegengegaan.")

 (ML Szk "Staatssecretaris Ybema heeft de Kamer meermalen toegezegd dat hij in Europees verband strenge regelgeving en heldere criteria zou bevorderen. Daarvan is niets terug te vinden in de concept richtlijn. De vraag is hoe dat komt en in een serie schriftelijke vragen nodigt Rik Hindriks namens de PvdA-fractie uit om aan te geven hoe hij alsnog een wijziging denkt te gaan realiseren.")

(colons (ML Vef "Voor meer informatie") (spaced "Rik Hindriks" "06-51 82 59 46"))
(ML VaW "Vragen aan de staatssecretaris van Economische Zaken van het lid Hindriks (PvdA)")

(ML HWa "Heeft u kennis genomen van de press release en faq over de draft directive inzake software patenten die vandaag door de EU is gepubliceerd?")

(ML HWd "Heeft U kennis genomen van het commentaar van software organisaties op deze draft directives? Hoe beoordeelt dit commentaar?")

(ML Bin "Bent U van mening dat de voorstellen van de EU in overeenstemming zijn met de inzet van de Nederlandse Regering? Zo nee, welke acties bent U voornemens te ondernemen om de voorstellen alsnog in overeenstemming te brengen met deze inzet?")

(ML Bil "Bent U van mening dat de voorstellen van de EU in voldoende mate rekening houden met gewenste criteria inzake het erkennen van softwarepatenten? Zo ja, op grond waarvan? Zo nee, hoe denkt U de criteria, zoals die zijn ontwikkeld door VOSN en FENIT alsnog in voldoende mate in de richtlijn verwerkt te krijgen?") 

(ML Oed "Op welke wijze heeft U een actieve rol gespeeld bij de totstandkoming van deze draft directive?")

(ML Otm "Op welke wijze zult U alsnog de door U toegezegde initiatieven om scherpe criteria ingevoerd te krijgen nemen in het Europees overleg over deze draft directive?")

(ML Iok "Is het naar uw oordeel mogelijk om op grond van de nu bekende tekts van de draft directive te komen tot het ongeldig verklaren van triviale software octrooien? Zo ja, op grond waarvan meent U dat en welke softwareoctrooien zal dat betreffen? Zo nee, bent U voornemens daartoe nadere acties te ondernemen?")

(ML IWW2 "Is het juist dat deze draft EU directive is geschreven door Francisco Mingorance? Indien de directe verantwoordlijkheid elders ligt is het dan juist dat Francisco Mingorance een belangrijke rol heeft vervuld bij het schrijven van deze draft directive? Indien dat het geval is, hoe beoordeelt U dan zijn relatie met de Business Software Alliance en de daarin vertegenwoordigde grote Amerikaanse softwareondernemingen?")

(ML Bld "Bent U bereid om ten aanzien van de wijze van totstandkoming, alsmede de invloed daarop van externe adviseurs, van deze draft directive nadere informatie te vragen aan de Europese Commissie?")

(ML BWE "Bent U bereid om nader overleg te voeren met de Vereniging Open Source Nederland en de FENIT over deze draft directive van de EU? Kan de Kamer nadere berichten over dat overleg tegemoet zien?")

(ah "http://europa.eu.int/comm/press_room/index_en.htm")

(ah "http://www.eurolinux.org/news/warn01C/indexen.html")

(ML Znr "Zie algemeen overleg 21670 nr 14 over een Algemeen Overleg gehouden 14 Maart 2001")

(ML UnW "Uit 21670 nr 14 %(q:De staatssecretaris gaat niet akkoord met wijzigingen van het verdrag zonder verbeterde regels. Hij gaat de strijd in Europa aan tegen triviale octrooien. Hij zal initiatieven nemen waar het gaat om scherpe criteria voor het vernauwen van de mazen, in overleg met onder andere het BIE, de FENIT en de VOSN. Hij zal ook kijken naar verkorting van de termijn. Er komt voorlichting aan het MKB en voor het reces of in ieder geval vlak daarna zal er een evaluatie komen.) En %(q:Volgens de staatssecretaris is deze samenvatting correct.)")
)

(mlhtdoc 'mamere020228
 (ML title "Noël Mamère 2002-20-28: Let's just delete the %(q:As Such) clause!" (de "Noël Mamère 2002-20-28: einfach weg mit der %(q:als-solche)-Klausel!") (fr "Noël Mamère 2002-20-28: effaçons simplement la clause %(q:en tant que tel)!"))
 (ML descr "The Presidency Candidate of the French Green Party joins the Socialist Party and his fellow contender Jean-Pierre Chevènement in general rejection of software patents and specifically criticises the %(eb:software patentability directive proposal of the European Commission) of 2002-02-20, demands a return to a strict adherence to the EPC and a removal of the much-abused %(q:as such) clause from the EPC and its French equivalent, asks the French government to act firmly based on the principles agreed upon recently by the Socialist Party, says that asserting the law against an illegal patent court practise is of highest political priority because constitutional freedoms are touched and the prosperity of the software industry in Europe is under threat." (de "Der Präsidentschaftskandidat der Französischen Grünen übt scharfe Kritik am %(eb:Softwarepatente-Richtlinienvorschlag der Europäischen Kommission) vom Februar 2002, fordert strenge Anwendung der Patentierbarkeitsgrenzen wie sie im EPÜ vorgesehen sind und Streichung der viel-missbrauchten %(q:als-solches)-Klausel aus dem EPÜ und seiner entsprechung im französischen Patentgesetz; kritisiert die französische Regierung wegen mangelnder Tatkraft, fordert Befolgung der kürzlich von der Regierungspartei beschlossenen Leitlinien zur Frage der Softwarepatente.  Der Durchsetzung des Rechts gegen die illegale Rechtsfortbildung durch einige Patentgerichte komme höchste Priorität zu, da es um verfassungsrechtliche Grundwerte gehe und da das Wohlergehen und die Innovationskraft der Softwarebranche schwer gefährdet sei.") (fr "Le Candidat aux Présidentielles du Parti Vert Français"))
 nil

 (ah "http://www.noelmamere.eu.org/presse/presse1.php")

 (ML Sei "Communiqué de presse de Noël Mamère sur le projet de directive visant à légaliser les brevets logiciels en Europe.")

 (ML LWs "La Commission Européenne a dévoilé le 20 février 2002 un projet de directive visant à légaliser les brevets sur les logiciels en Europe, sur le mode américain.")

 (ML Lri "Les communiqués de la commission européenne laissent penser que cette proposition de directive est un bon compromis et qu'elle tient compte des remarques des opposants à la brevetabilité des %(q:inventions mises en oeuvre par ordinateur) (en fait des règles abstraites enrobées en jargon informatique). Pourtant, le projet de directive introduit une brevetabilité illimitée de toutes les idées, celles de l'informatique, mais aussi les méthodes d'organisation du travail, de l'éducation, de la santé ... .")
 
 (ML Cvn "Cette directive s'appuie sur la pratique illégale de l'Office Européen des Brevets tentant de faire passer un viol de la loi pour sa jurisprudence, alors que la conférence diplomatique de novembre 2000 avait conclut que les activités de l'OEB devraient être plus étroitement contrôlées par les gouvernements européens.  De plus on constate dans la fiche d'étude d'impact annexée au projet de directive que le seul document économique servant à étayer la position de la commission européenne est un document commissionné par la Business Software Alliance en 1998 (structure qui ne dispose d'aucun statut juridique en France et pratique lobbying agressif et menaces financières contre les entreprises européennes), qui ne parle même pas de brevetabilité, alors que les différentes études économiques françaises, britanniques ou allemandes qui ont des conclusions négatives ne sont pas utilisées.") 
 
 (ML LWm "Le droit d'auteur seul permet une protection égalitaire entre multinationales, PME et développeurs indépendants, et a d'ailleurs permis la prospérité de l'industrie européenne du logiciel. Ce projet de directive conduirait inéluctablement à l'affaiblissement de l'innovation logicielle en Europe (comme le montrent les études économiques), la monopolisation des standards, donnerait le champ libre aux multinationales non européennes pour contrôler les joyaux de l'industrie européenne du logiciel, et conduirait à la quasi suppression des droits élémentaires des citoyens de la société de l'information.")

 (ML Fot "Face à ces dangers, il faut aujourd'hui un acte politique fort. Le Parti Socialiste se dit contre les brevets logiciels, pourtant le gouvernement laisse dériver la situation. S'agissait-il d'une simple promesse de circonstance liée à la proximité des élections présidentielle ?")

 (ML Laa "Le comportement de la commission européenne montre que ce n'est pas le moment d'harmoniser et donc le gouvernement français doit clairement dire que la proposition de directive publiée par la commission européenne ne peut être considérée comme une base de négociation, et doit se prononcer dés maintenant et très clairement contre la brevetabilité du logiciel et des innovations immatérielles.")

 (ML Meu "Modifier substantiellement la loi par le biais d'une pseudo-jurisprudence, c'est violer la Constitution de la Vème République qui spécifie précisément que cette latitude n'appartient qu'au Peuple. L'accepter serait une forfaiture.")

 (ML Lev "La volonté intéressée de l'OEB et de la commission européenne est de modifier l'esprit de la loi en légalisant des pratiques qui la violent. La volonté du gouvernement doit être de reformuler la loi pour que sa lettre en respecte l'esprit.")
)

(mlhtdoc 'france020301
 (ML title "France 2002-03-01: EU Commission Directive Proposal Unacceptable" (de "Frankreich 2002-03-01: Richtlinienvorschlag der Europäischen Kommission inakzeptabel") (es "Francia 2002.03.01: La propuesta de Directiva de la Comisión Europea es inaceptable."))
 (ML descr "Le Ministre de l'Industrie Christian Pierret critique que la directive proposée par la Commission Européenne ne propose pas une limite claire a la brevetabilité et n'est pas basée sur une argumentation économique fondée.   Celle-ci devrait prendre en compte la réalité du secteur logiciel entier autant qu'un analyse des effets économiques de la pratique de l'OEB et sa compatibilité avec les buts formulés au sein du plan e.Europe." (en "The french government has crticised the CEC/BSA Software Patentability Directive Proposal at the European Internal Market Council's meeting of 2002-02-28, in which commissioner Bolkestein explained this proposal to the patent policy representatives of the European Union's member states.  France expressed dismay about the proposal's apparent failure to outline a clear limit on patentability, to critically assess the economic effects of the EPO's recent practise, to take serious research into account and to provide a valid rationale in view of european public policy targets such as those laid down in the e.Europe plan.  Industry minister Christian Pierret immediately published this position in a press release.") (de "Der französische Industrieminister Christian Pierret hat im europäischen Binnenmarktrat kritisiert, dass der von der Europäischen Kommission Vorschlag einer Richtlinie für die Patentierbarkeit von Rechenregeln in keiner Weise die versprochene klare Begrenzung der Patentierbarkeit bringt und den wirtschaftspolitischen Zielen der Europäischen Union zuwiderläuft.  Dies gab er in einer Presseerklärung bekannt.") (es "El ministro francés Christian Pierret escribe a la Comisión Europea expresando su desacuerdo con el aparente fallo de la propuesta de directiva, que no impone límites claros sobre la patentabilidad y no sirve a los intereses de los creadores europeos de software ni tiene en cuenta políticas europeas relacionadas, como la iniciativa e.Europe."))
 nil

 (sects
 (text (ML DrT "The Text" (de "Der Text") (fr "Le Texte")) 

 (ML MTE "MINISTRE DELEGUE A  L'INDUSTRIE, AUX PETITES ET MOYENNES ENTREPRISES, AU  COMMERCE, A L'ARTISANAT ET A LA CONSOMMATION" (en "Minister for Industry, Small and Medium Enterprises, Commerce, Crafts and Consumption") (de "Minister für die Industrie, die kleineren und mittleren Unternehmen, den Handel, das Handwerk und den Verbrauch") (es "Ministro de Industria, Pequeña y Mediana Empresa, Comercio, Artesanía y Consumo"))
 (ML CWn "Communiqué de presse" (en "Press Release") (de "Presseerklärung") (es "Nota de prensa"))  

 (ah "http://www.industrie.gouv.fr/accueil.htm")
 
 (ML Pea "Paris le 1er mars 2002" (en "Paris, 1st of March 2002") (de "Paris den ersten März 2002") (es "París, 1 de marzo de 2002"))
 
 (ML CeW "Christian PIERRET,  Ministre délégué à l'Industrie, aux PME, au Commerce, à l'Artisanat et à la Consommation, a fait part à la Commission Européenne de la position du gouvernement français concernant le projet de directive sur la brevetabilité des logiciels présenté aujourd'hui au Conseil marché intérieur. Constatant que le projet de directive n'apporte aucune des précisions attendues sur les limites et les exigences de la brevetabilité, le gouvernement s'inquiète du champ qui pourrait être ouvert à la brevetabilité de l'ensemble des logiciels voire des méthodes intellectuelles et commerciales. Or, il est apparu clairement en France comme en Europe qu'une telle extension est largement rejetée." (en "Christian PIERRET, the minister of Industrie, SMEs, Commerce, Crafts and Consumption has notified the European Commission of the french government's position concerning the draft for a directive on the patentability of software which was presented today to the council of the internal market.  Finding that the directive draft does not bring any of the expected clarifications concerning the limits of obtainable patentability, the government is worried about the scope of patentability which could be made open to all software and consequently to pure thought and business methods.  However it has appeared clearly in France as well as in Europe that such an extension is largely rejected.") (de "Der Minister für Industrie, KMU, den Handel, Handwerk und Verbrauch, Christian Pierret, hat die Europäische Kommission über die Position Frankreichs zum Entwurf einer Richtlinie über die Patentierbarkeit von Software unterrichtet, die heute dem Binnenmarktrat vorgelegt wurde.  Die Regierung stellt fest, dass der Richtlinienvorschlag keine der erwarteten Klärungen bezüglich der Grenzen der Patentierbarkeit bringt, und ist beunruhigt, dass das Gebiet des Patentierbaren nunmehr für Logikalien (Software) aller Art bis hin zu Verfahren für geistige und geschäftliche Tätigkeit geöffnet werden könnte.  Es hat sich jedoch in Frankreich ebenso wie in Europa in der Diskussion deutlich gezeigt, dass eine solche Ausweitung weitgehend auf Ablehnung stößt.") (es "Christian Pierre, el ministro de Industria, PYMEs, Comercio, Artesanía y Consumo ha notificado a la Comisión Europea la posición del gobierno francés sobre el borrador de directiva sobre la patentabilidad del software que fue presentado hoy al Consejo de Mercado Interno. Al encontrar que el borrador de directiva no aporta ninguna de las aclaraciones que se esperaban sobre los límites de lo que se considera patentable, el gobierno está preocupado por el ámbito lo patentable, que podría quedar abierto para todo el software, y por lo tanto, para el pensamiento puro y los modelos de negocio. Por el contrario, ha quedado claro que en Francia y en Europa esta extensión de la patentabilidad es rechazada por una gran mayoría.")) 
 (ML LtW "La France a rappelé qu'elle juge indispensable de disposer d'un bilan de la protection juridique des logiciels telle qu'elle résulte de la pratique de l'Organisation Européenne des Brevets (OEB) et des Etats membres. Le gouvernement français souhaite écarter tout projet qui aurait des conséquences négatives pour l'innovation, pour l'interopérabilité et les logiciels libres, et pour l'ensemble des acteurs (éditeurs, intégrateurs, utilisateurs), notamment les PME. Il considère que la proposition de directive ne répond pas de façon adéquate aux enjeux économiques, scientifiques et culturels du secteur du logiciel ainsi qu'à la nécessité de promouvoir l'innovation qui figure parmi les priorités du plan d'action « e.Europe ».  La Commission a mené des études et une consultation sur ce thème au dernier trimestre 2000. Le projet de directive présenté aux Etats membres n'indique pourtant pas clairement les risques que présenterait une validation juridique de la pratique de l'OEB dans les Etats membres de l'Union, au regard des avantages que l'on pourrait en attendre. Divers études et rapports conduits dans plusieurs Etats Membres sont apparus par ailleurs réservés quant à une telle évolution.  A l'initiative notamment de la France, la Conférence Diplomatique pour la révision de la Convention sur le Brevet Européen (CBE), qui s'est tenue à Munich en novembre 2000, avait décidé de ne pas modifier les dispositions de la Convention en la matière, souhaitant qu'une position européenne claire puisse être prise sur la base d'une analyse précise de ses conséquences sur les plans économique, technique et juridique." (en "France has made it clear that she finds it essential to account for the merits and demerits of the protection of software resulting from the current practise of the European Patent Organisation (EPO) and its member states.  The french government wants to avoid any draft that could have negative consequences on innovation, interoperability and free software as well as on the actors as a whole (publishers, integrators, users), especially the SMEs.  It believes that the approach of the directive fails to respond adequately to the economic, scientific and cultural issues of the software sector as well as to the need to promote innovation which is one of the priorities of the %(q:e.Europe) action plan.  The Commission has conducted studies and a consultation in the last third of the year 2000.  The directive draft presented to the member states however does not clearly assess the risks which a legitimation of the practise of the EPO in the member states would bring, in comparison to the advantages.  Various studies and reports conducted in several member states have appeared rather unenthusiastic about such an evolution.  At the initiative of France, among others, the Diplomatic Conference for the Revision of the European Patent Convention (EPC) which was held in Munich in november 2000 had decided not to change the rulings of the European Patent Convention on this subject, wishing that a clear European position could be found on the basis of a precise analysis of the economic, technical and legal consequences.") (de "Frankreich hat daran erinnert, dass seiner Meinung nach eine Bilanz des Rechtsschutzes zu ziehen ist, der sich aus der Praxis der Europäischen Patentorganisation (EPO) und der Mitgliedsstaaten ergibt.  Die französische Regierung möchte jegliches Vorhaben vermeiden, welches negative Auswirkungen auf die Innovation, die Interoperabilität, die freie Software und die Gesamtheit der Akteuere (Schaffende, Integrierer und Anwender von Software) haben könnte.  Wir glauben, dass der Richtlinienvorschlag weder in angemessener Weise auf die wirtschaftlichen, wissenschaftlichen und kulturellen Herausforderungen des Software-Sektors antwortet noch den Erfordernissen der Förderung der Innovation Rechnung trägt, wie sie im Aktionsplan %(q:e.Europe) als Priorität angeführt sind.  Die Kommission hat zu diesem Thema Studien und eine Konsultation im letzten Drittel des Jahres 2000 durchgeführt.  Dennoch zeigt der Richtlinienentwurf, wie er den Mitgliedsstaaten vorgelegt wurde, nicht klar die Risiken auf, die eine Durchsetzung der Praxis der EPO in den Mitgliedsstaaten mit sich bringen würde, und deren Verhältnis zu dem zu erwartenden Nutzen.  Im übrigen haben sich verschiedene in den Mitgliedsstaaten erschienene Studien eher reserviert über diese Aussichten geäußert.  Nicht zuletzt auf Betreiben Frankreichs hatte die Diplomatische Konferenz für die Revision des Europäischen Patentübereinkommens (EPÜ), die im November 2000 in München stattfand, beschlossen, die Vorgaben des Übereinkommens in dieser Sache nicht zu ändern, in dem Wunsch, dass eine klare europäische Position auf Grundlage einer präzisen Analyse der wirtschaftlichen, technischen und juristischen Folgen gefunden werden könnte. ") (es "Francia ha dejado claro que es esencial tener en cuenta los méritos y deméritos de la protección del software como resultado de las prácticas actuales de la Oficina Europea de Patentes (OEP) y de sus estados miembro. El gobierno francés quiere evitar cualquier borrador que pueda tener consecuencias negativas sobre la innovación, la interoperabilidad y el software libre, y sobre el sector en su conjunto (editores, integradores y usuarios de software), y más especialmente sobre las PYMEs. Cree que la aproximación seguida por la directiva no responde adecuadamente a los aspectos económicos, científicos y culturales del sector del software, ni a la necesidad de promover la innovación, que es una de las prioridades de el plan de acción de %(q:e.Europe). La Comisión realizó estudios y una consulta durante el último tercio del año 2000. Sin embargo, el borrador de directiva que se presentó a los estados miembro no aborda claramente los riesgos que conllevaría la legitimación de las prácticas de la EPO en los estados miembros, en comparación con las ventajas. Varios estudios e informes realizados en varios estados miembro han sido muy poco entusiastas con respecto a esta evolución. Por iniciativa de Francia, entre otros, la Conferencia Diplomática para la Revisión de la Convención Europea sobre Patentes (CEP), que tuvo lugar en Munich en noviembre de 2000, decidió no cambiar las reglas de la Convención Europea sobre Patentes en este aspecto, deseando que se encuentre una posición europea clara basada en un análisis preciso de las consecuencias económicas, técnicas y legales.")) 
 
 (dl (l (ML Cap "Contact presse" (en "Press Contact") (de "Pressekontakt") (es "Contacto de prensa")) (spaced " - " (ML Cea "Cabinet de Christian PIERRET" (en "Cabinet of Christian PIERRET") (de "Kabinett von Christian PIERRET") (es "Gabinete de Christian Pierret")) (ML LeW "Laurence GAUNE") (colons (ML t1l "tél" (en "phone") (de "tel") (es "teléfono")) "01 53 18 44 85"))))
)

(etc (tok etclinks)

 (linklist
  (ah 'france020313) 
  (ah "http://www.telecom.gouv.fr/dp/brevetlogiciel.pdf")
  (ah "http://www.minefi.gouv.fr/minefi/actualites/index.htm")
  'eubsa-swpat0202
  )
) ) )

(mlhtdoc 'tauss020312
 (ML title "Tauss 2002-03-12: Germany must say No to CEC/BSA Proposal and EPO Practise" (de "Tauss 2002-03-12: Deutschland muss EUK/BSA-Vorschlag und EPA-Praxis zurückweisen") (fr "Tauss 2002-03-12: L'Allemange doit refuser le Propos CE/BSA et la Pratique OEB"))
 (ML descr "Jörg Tauss, president of the New Media Commission of the German Federal Parliament and in charge of Media and Education in the Social Democratic Party (SPD)'s Parliamentary Fraction, has written a letter to his party colleague and minister of justice Prof. Herta Däubler-Gmelin.  Tauss asks Ms Däubler-Gmelin to opppose this proposal as resolutely as she opposed the planned modification of Art 52 EPC in november 2000.  This proposal, Tauss says, merely enshrines a highly questionable practise of the European Patent Office (EPO).  There is indeed a need for legislating at the EU level on the limits of patentability, Tauss says, but the EPO practise is the problem, not the solution.  Software patents seem harmful for innovation, incompatible with Europe's strategic interests, dangerous for open source software, less than helpful for proprietary software companies including such bing players as SAP, serving mainly monopoly interests of a few american monopolists behind BSA.  The minister should not be fooled by false assertions about the TRIPS treaty.  The TRIPS treaty is a reason to embrace the strict definition of %(e:technical invention) (teaching about physical causalities), according to which software does %(e:not) constitute or belong to a %(q:field of technology).  Tauss founds his explanations on an expert hearing which he together with colleagues from all parties in the parliament conducted in June 2001.  It seems that he represents a consensus position shared at least by most of the members of the parliamentary subcommission on the New Media.  This is confirmed by all publicly known statements given by other MPs so far.  The German Government should take a position similar to that of France and of the German Ministry of Economics (BMWi), Tauss says.  The latter has not been published yet, but it is known that there is widespread skepticism about software patents in the BMWi." (de "Der Vorsitzende des Bundestagsunterausschusses Neue Medien bittet seine Parteikollegin Bundesjustitzministerin Däubler-Gmelin, dem Brüsseler Vorschlag, Programmlogik patentierbar zu machen, eine ähnlich klare Absage zu erteilen wie Frau Däubler-Gmelin es im November 2000 gegenüber den Plänen der Patentlobby tat, die %(q:Programme für Datenverarbeitungsanlagen) von der Liste der Nicht-Erfindungen im Europäischen Patentübereinkommen (EPÜ) zu streichen.  Tauss fordert eine Unterstützung der Position Frankreichs und zählt auch das BMWi zum Kreis derer, die dieser Position zuneigen.  Grundsätzlich hält Tauss eine Klärung der Grenzen der Patentierbarkeit auf EU-Ebene für sinnvoll.  Der vorliegende Entwurf ziele aber darauf ab, im Sinne des EPA %(q:lästige Debatten zu beenden) und so die vom EPA verursachten Probleme weiter zu verschlimmern, statt sie zu lösen."))
 nil

(sects
(text (ML TWt "Der Brief" "The Letter")

(ml "JÖRG  TAUSS")
(ml "MITGLIED DES DEUTSCHEN BUNDESTAGES")
(ml "VORSITZENDER UNTERAUSSCHUSS NEUE MEDIEN")  

(ml "Jörg Tauss, MdB * Unter den Linden 50 * 11011  Berlin")
 
(ml "%(nl|An die|Bundesministerin für Justiz|Prof. Dr. Herta Däubler-Gmelin|Bundesministerium für Justiz| - Postaustausch -)") 

(ml "Berlin, den 12. März 2002")
       
(ml "Softwarepatente ­ Richtlinienentwurf der EU-Kommission")
       
(ml "Sehr geehrte Frau Ministerin,")

(filters ((be ah 'swpbtag016)) (ML dc1 "die EU-Kommission hat am 20. Februar 2002 den lange erwarteten Richtlinienentwurf zur Patentierbarkeit von Software beschlossen. Wie Sie wissen, ist diese Frage in den vergangenen Monaten und Jahren sehr kontrovers diskutiert worden. So hat auch der Unterausschuss Neue Medien gemeinsam mit dem Rechtsausschuss des Deutschen Bundestages am 21. Juni 2001 ein %(be:öffentliches Expertengespräch) durchgeführt, um sich hinsichtlich der Chancen und Risiken einer Erweiterung der Patentierbarkeit von Software zu informieren. Die kritischen Ergebnisse habe ich kursorisch zusammenfassen lassen und dem Vorsitzenden des Rechtsausschusses zur Verfügung gestellt (Anlage). Ebenso hat sich die Enquete-Kommission %(q:Globalisierung der Weltwirtschaft ­ Herausforderungen und Antworten) mit dem Problem der zunehmenden Monopolisierung des Wissens befasst und ausdrücklich vor den negativen Auswirklungen ­ etwa einer zu engen Auslegung der TRIPS-Bestimmungen ­ gewarnt. Hier wurden als negatives Beispiel neben der Gesundheits-, Landwirtschafts- und der Ernährungspolitik insbesondere eben auch der Softwarebereich angeführt."))

(filters ((ar ah 'epue52)) (ML Ier "In dieser Angelegenheit hatten Sie sich auf der Konferenz zum europäischen Patentübereinkommen im November 2000 zurecht gegen eine voreilige Änderung des %(ar:Art. 52 EPÜ) ausgesprochen. Mit dem von der EU-Kommission nach langem internen Streit zwischen den Generaldirektionen Binnenmarkt, Wettbewerb und Informationsgesellschaft beschlossenen Richtlinienvorschlag (KOM (2002) 92end.) gewinnt die Debatte nun wieder an Dynamik. Ohne den Vorschlag an dieser Stelle im Detail bewerten zu wollen, so lässt er doch zahlreiche Fragen offen. Zumindest irritierend ist es aber, dass der beschlossene Text in den entscheidenden Punkten wortgleich mit einem bereits länger kursierenden Entwurf ist, als dessen Autor ein Jurist der Business Software Alliance (BSA) gilt. Die BSA wiederum ist ein Interessenverband der großen Softwarehersteller ­ allen  voran Microsoft ­, der sich bisher weniger mit Patentrecht, als vielmehr mit der internationalen Durchsetzung eigener wirtschaftlicher Interessen beschäftigt hat. Wieso sich die EU-Kommission den Vorschlag eines Interessenverbandes zu eigen macht, sei dahingestellt. Politisch stehen für mich aber weiterhin folgende Fragen im Vordergrund:"))

(ul
 (ML Evi "Eine Übernahme des amerikanischen Softwarepatentsystems ist auch aufgrund der bisherigen negativen Erfahrungen in den USA abzulehnen. Ein europäischer Weg in der Softwarepatentpolitik erscheint nicht nur möglich, sondern auch angebracht und wird offensichtlich auch in Brüssel verfolgt.") 
 (ML Dum "Die bisherige, bereits rechtlich strittige Patentierungspolitik des EPA, ist zu evaluieren und unrechtmäßig erteilte Patente sind zu widerrufen. In diesem Zusammenhang von der %(q:Herstellung der Rechtssicherheit auf Grundlage des  Status Quo) zu sprechen, wie es der Richtlinienvorschlag tut, erscheint zumindest klärungsbedürftig. Um es klar zu sagen: Das EPA hat bereits eine Unmenge fraglicher Patente erteilt, die nicht ohne Prüfung mit einem Federstrich im Nachhinein legalisiert werden dürfen. Auf Basis einer Fehlentwicklung Rechtssicherheit herstellen zu wollen, nur um lästige Debatten zu beenden, erscheint mir nicht als ein angemessenes Vorgehen.")
 (ML Etz "Eine freie Patentierbarkeit von Software entzieht alternativen Entwicklungskonzepten die Grundlage, insbesondere Open Source-Software ­ wie die wirtschaftlich erfolgreiche Serversoftware Apache oder das Betriebssystem Linux ­ wäre in der jetzigen Form nicht mehr möglich (offenbar ist der Umweg über Brüssel ein guter Weg, um sich als weltweiter Monopolist seiner ärgsten Widersacher zu entledigen). Wir können nicht einerseits den Einsatz von Open Source-Software fordern und fördern, die auch hinsichtlich der zunehmend wichtigen Sicherheits- wie Kostenaspekte gerade für den öffentlichen Bereich attraktiv sind, andererseits diese insbesondere europäische Entwicklung durch freie Patentierbarkeit unmöglich machen und amerikanischen Unternehmen das Feld überlassen.")
) 

 (filters ((sp ah 'swpatpikta)) (ML DaW "Die Patentierbarkeit von Software ist eine Kernfrage der künftigen Entwicklung der Informations- und Wissensgesellschaft, da die Bedeutung sowohl elektronischer Information und Kommunikation, als auch der IT-Infrastruktur weiter zunehmen wird. Lassen wir es weiter zu, dass entscheidende Schnittstellen dieser Infrastruktur zunehmend monopolisiert und der allgemeinen gesellschaftlichen Verfügbarkeit entzogen werden, müssen wir uns nicht wundern, wenn in weiten Teilen naturgemäß rendite-orientierte Entscheidungen amerikanischer Unternehmen die Möglichkeits- und Entwicklungsbedingungen auch der europäischen Wissens- und Informationsgesellschaft wie -industrie bestimmen. Erster Hinweise auf die Auswirkungen können bereits beobachtet werden, hier möchte ich nur drei Beispiele kurz anführen: so ist ­ natürlich ­ in den USA eine Klage anhängig, in der es um die Verwendung von Hyperlinks geht, dem beinahe wichtigsten Navigationsprinzip im Internet. Dieses verletze Patente, so die Klägerin, die sie an diesem Verfahren halte ­ Lizenzgebühren für jeden Mausklick im Netz an ein einziges Unternehmen? In einem zweiten Verfahren werden patentrechtliche Ansprüche an dem Prinzip erhoben, Kopien digitaler Güter aus dem Internet herunterzuladen. Bei jedem dieser Downloads, etwa eines Musikstücks oder Programms, wären Gebühren an ein einzelnes Unternehmen fällig, nur weil es als erstes ein Verfahren patentieren ließ, welches viele Personen parallel ersonnen haben und das bisher milliardenfach frei eingesetzt wurde. Und drittens schließlich ist erst in diesen Tagen ein Rechtstreit in den USA nach 3 Jahren außergerichtlich beigelegt worden, der 1999 zwischen zwei Unternehmen um das sogenannte One-Click-Patent entbrannte. Hier wurde ein Verfahren patentiert, mit dem Kunden eines Online-Shops ein aktuell angezeigtes Produkt mit nur einem einzigen Mausklick bestellen konnten. Im Umkehrschluss verlangte die Patentinhaberin nunmehr, dass bei der Konkurrenz mindestens zwei Klicks notwendig sein müssen, um den Patentanspruch nicht zu verletzen ­ wieder ist der innovative Fortschritt mehr als fragwürdig und der Kampf um geringste Wettbewerbsvorteile mehr als offensichtlich. Die Analogien zur Biopatentdebatte sind offenkundig, auch hier ist nämlich die Frage zu stellen, welchen außerordentlichen gesellschaftlichen Beitrag die Innovationen der Unternehmen geleistet haben, die ein derart folgenreiches und langfristiges Verwertungsmonopol rechtfertigen könnten. Die EU-Kommission drückt sich um eine Antwort auf diese im Grunde politische Frage: was soll eigentlich patentierbar sein und was geht aus welchen Gründen zu weit? Bis heute gibt es keine Antwort darauf, ob etwa die drei beschriebenen amerikanischen Patente nun auch in Europa möglich sein sollen, oder eben nicht. Das EPA, darauf möchte ich mit Nachdruck ein weiteres mal hinweisen, hat mittlerweile %(sp:mehrere Tausend Patente) erteilt, deren Erfindungshöhe sich kaum positiv von den zitierten Beispielen abhebt und daher mehr als fraglich ist.")) 

 (filters ((a3 ah '(lref prop eubsa-swpat0202))) (ML Dho "Die EU-Kommission geht der Kernfrage in der Debatte um die Patentierbarkeit von Software gegenwärtig aus dem Weg. Sie lautet nämlich, ob und inwiefern Software nach den allgemeinen patentrechtlichen Grundsätzen tatsächlich eine Erfindung darstellen kann und auch tatsächlich dem Bereich dem Technik, auf den das Patentrecht begrenzt ist, zuzuordnen ist. Erfindungen im Sinne einer signifikanten Erweiterung gesellschaftlichen Könnens resp. ihrer technischen Problemlösungskapazitäten sind nicht bereits durch die handwerklich auch noch so gelungene Anwendung bestehenden Wissens und bestehender Verfahren gegeben, sie dürfen vielmehr selbst für fachkundige Personen nicht naheliegend sein. Technik im Sinne einer Lehre zum planmäßigen Handeln unter Einsatz beherrschbarer Naturkräfte zur Erreichung eines kausal übersehbaren Erfolges (so der BGH) bedarf offenbar des physikalischen Bezuges. Dieser ist aber in der Dualität von Software/Hardware in der IT-Welt nicht ohne weiteres gegeben, war es doch gerade die Neumann'sche Universalmaschine (die wir heute Computer nennen), die beide Bereiche voneinander unabhängig machte und eigenständige Entwicklungslinien ermöglichte. Jedes Programm (als algorithmisierte Logik) läuft auf jedem Rechner (als physikalisches Substrat der logischen Manipulationen), gleich welche Programmiersprache verwendet wird oder welcher Prozessor die Befehle abarbeitet ­ es ist eben eine logikoffene universale Rechenmaschine. Software ist daher eher einer handwerklichen Tätigkeit vergleichbar und folgt als textbasiertes Medium eher logischen Regeln als naturgesetzlichen Verfahren. Die Richtlinie verlangt von den Mitgliedstaaten in %(a3:Artikel 3) nun aber, so-genannte computerimplementierte Erfindungen als Teil der Technologie auszufassen und spricht sich damit grundsätzlich für ihre Patentierbarkeit aus, um die Rechtssicherheit zu erhöhen - die Beweggründe zu dieser weitreichenden Regelung sind nicht ersichtlich.")) 

(filters ((sa ah 'swpatsap) (tr ah 'swpattrips)) (ML Atl "Abgesehen davon, dass die rechtliche Frage der Patentierbarkeit von Logiken strittig ist und auch die innovationspolitische Notwendigkeit zumindest für den Bereich der Softwareentwicklung verneint werden kann, sollte auch ein weiteres Ziel nicht aus den Augen verloren werden: wir wollen den IT-Bereich wirklich internationalisieren und eine ­ vor allem amerikanisch dominierte ­ Monokultur in der Softwarelandschaft aufbrechen. Hier hat Europa eine einmalige Chance, gerade über Open Source-Projekte entscheidende Elemente der künftigen IT-Infrastruktur mitzubestimmen. Diese Chance darf nun nicht durch eine einseitige, an den Interessen (amerikanischer) Großkonzerne und deren patentjuristischer Abteilungen orientierte Richtlinie gefährdet werden. Oder, um sinngemäß die Worte von Prof. Plattner, dem Vorsitzenden des erfolgreichen deutschen Softwarehauses %(sa:SAP), anzuführen: man brauche keine Softwarepatente, um auf den Märkten für Software erfolgreich zu sein. Sehr wohl braucht man diese Patente aber, wenn man sich mit den aggressiven amerikanischen Unternehmen vor amerikanischen Gerichten auseinandersetzen will oder gar muss. Auch dort wolle eigentlich keiner Softwarepatente, doch wird die bestehende rechtliche Möglichkeit zum Rent-Seeking oder Marktabschottung eben inflationär genutzt. Dieser Zwang besteht für Europa ­ anders als es oft kolportiert wird ­ keineswegs, weder verlangt das %(tr:TRIPS) noch der gegenwärtig neu verhandelte Patenvertrag der WIPO explizit eine Patentierbarkeit von Software. Niemand beabsichtigt die international anerkannten patentrechtlichen Grundsätze generell in Frage zu stellen. Ich bin nur dezidiert der Auffassung, dass Software diese Anforderungen nicht zu erfüllen vermag und Softwarepatente darüber hinaus nachweislich volks- wie betriebswirtschaftlich negative Effekte produzieren würde."))  

(ML Dni "Die Richtlinie ist daher keineswegs überflüssig, sie ist sogar sachlich notwendig und kann ein entscheidendes Signal in die richtige Richtung geben. Dies setzt allerdings voraus, dass es gelingt, erhebliche Änderungen am gegenwärtigen Entwurf durchzusetzen. Hier unterstütze ich in jeder Hinsicht die Kritik Frankreichs oder auch aus dem Bundeswirtschaftsministerium zum Richtlinienvorschlag und möchte Sie bitten, sich ebenfalls für eine solche Verbesserung und auch Klarstellung einzusetzen. Indem ich auf Ihre weitere Unterstützung in dieser Auseinandersetzung sowie in den bevorstehenden Diskussionen baue, verbleibe ich")

(ML mue "mit freundlichen Grüßen")

)

(etc (tok etclinks)
(linklist
'swpbtag016
'swpattrips
'eubsa-swpat0202
'swpatpikta
)
)
)
)
 

 

Jörg Tauss 






















                                                                                        




 (linklist 'swpbtag026)
)

;eubsa-swpat0202-dir
)



