<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Arlene McCarthy 2003/09/01: %(q:The Myths - The Truth)

#descr: In response to the wave of protests against the proposed software patent directive COM(2002)92 2002/0047 in late August 2003, the European Parliament's rapporteur for this directive, Arlene McCarthy MEP, has published a %(q:Factsheet) which attempts to explain that she has been a victim of a %(q:misinformation campaign) and is in reality championning the protesters' cause.  We republish the paper with comments here.

#tin: Misinformation about Misinformers?

#reW: %(q:Computer-Implemented Invention) not Software?

#hRn: %(q:Technical Solution): Relying on an Abolished Requirement

#eqi: Amazon One Click Test Case: %(q:Parliament's Objective) achieved?

#anW: Ban on Publication not a Problem?

#WtF: Software Patents Not Allowed But Very Beneficial?

#nWl: National Courts: Slavish Followers?

#yei: Only Genuine Inventions?

#tam: The text's title accuses an unkown organisation of a %(q:misinformation campaign):

#Wsn: FACTSHEET - EU rules for patents for computer-implemented inventions

#flt: The Misinformation Campaign: Claims by the Free Software Alliance

#eno: The %(FSA) is an imaginary organisation, created by Arlene McCarthy for the purpose of press briefings.  The FSA is a mirror of the %(BSA): it fights aggressively and intrusively for the right of its members to free-ride on other people's intellectual property.  The FSA supports a caricature of the positions of the %(q:FFII) (Foundation for a Free Information Infrastructure) and the %(el:Eurolinux Alliance).  Unlike FFII, Eurolinux and BSA, the FSA consists only of %(cc:computer rights campaigners), and enjoys no support among commercial software producers.

#Wkr: The Myths - The Truth

#svc: In the following, McCarthy attempts to refute various %(q:myths).  We do not know for sure who are the authors of the %(q:myths), because McCarthy does not cite sources.  However it is clear that most of the %(q:myths) originate from the %(xm:FFII invitation letter to the 2003/08/27 events), which was sent to the members of the European Parliament by email and fax.

#Wat: This is a proposal for a software patent directive

#aoe: The proposal is for a patent to cover computer-implemented inventions.

#hle: In law, software, as such, is not patentable. Just as an invention in the physical world can enjoy patent rights so too can an invention relying on a computer application.

#lia: At the European Commission, this directive proposal is colloquially termed %(q:the software patent directive proposal) or %(q:softpat proposal).  David Ellard, one of the responsible persons at the Industrial Property Unit, referred to it as the %(q:software patentability directive) in the written program as well as in his oral speech at the OECD conference in Paris on Aug 28/29.

#titjust: The term %(q:computer-implemented invention) is not used by computer professionals.  It is in fact not in wide use at all.  It was introduced by the European Patent Office (EPO) in May 2000 in %(a6:Appendix 6) of the Trilateral Conference, where it served to legitimate business method patents, so as to bring EPO practise in line with the USA and Japan.  Much of the European Commission's directive proposal is based on wordings from this %(q:Appendix 6).

#puW: According to the original EPO definition of 2000 which is used by the European Commission, the term %(q:computer-implemented inventions) refers to nothing but software in the context of patent claims.  Accordingly, an anti-blocking system might not qualify as a computer-implemented invention, because the %(q:prima facie novel features) do not lie in the data processing system but in the way friction is applied to automobile tyres by means of automobile brakes.  The ABS would therefore not be a %(q:computer-implemented invention) but a %(q:brake-implemented invention).

#Wae: Arlene McCarthy herself uses the term %(q:computer-implemented invention) as a synonym for %(q:software innovation) in the %(q:Factsheet) (see below), and correctly so.

#oec: Until 1986, software innovations were not considered to be patentable inventions by the European Patent Office.  Some national courts are continuing to reject them as unpatentable even today.  Examples of caselaw which applies Art 52 EPC correctly can be found at

#tWe: Both of these software patents would have to be granted in the future under the directive which Arlene McCarthy is espousing.

#ttW: This is not new. The European Patent office and national patent offices have already granted patents for computer-implemented inventions. The proposal applies strictly to inventions which must satisfy the conditions of any invention; must be new, involve an inventive step and must make a technical contribution providing a technical solution to a problem.

#nrW: Nowhere in McCarthy's proposal is there any mention of a requirement that there be a %(q:technical solution to a problem).  On the contrary, Recital 12 as amended by Arlene McCarthy ensures that the existence of a %(q:technical problem) is enough, thereby carefully negating the EPO's earlier approach, which required a %(q:technical solution) as well:

#bec: Accordingly, even though a computer-implemented invention belongs by virtue of its very nature to a field of technology, it is important to make it clear that where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#iee: When assessing whether an inventive step is involved, it is usual to apply the problem and solution approach in order to establish that there is a technical problem to be solved. If no technical problem is present, then the invention cannot be considered to make a technical contribution to the state of the art.

#liW: As the Federal Patent Court explains nicely in its Error Search decision of 2002, cited above:

#bpatg17: If the solution of a technical problem could be a sufficient reason to attribute technical character to a computer implementation, then every implementation of a non-technical method would have to be patentable; this however would run against the conclusion of the Federal Court of Justice that the legal exclusion of computer programs from patentability does not allow us to adopt an approach which would make any teaching that is framed in computer-oriented instructions patentable.

#pnW: We have pointed this out to McCarthy as soon as her draft report came out:

#Whe: McCarthy however insisted on her amendment.  She seems to know that a requirement of %(q:technical solution) would be better than one of %(q:technical problem), and that her audience wants to hear that this requirement exists in the proposed directive.  Yet it does not, thanks to McCarthy's own efforts.

#Wao: The proposal would impose US-style unlimited patentability of algorithms and business methods such as Amazon's 'one-click' shopping.

#Wre: In fact the Parliament's objective is to stop the drift by the EPO and national patent offices to patent business methods. The Parliament's proposal is stronger than current law and practice of the EPO and is explicitly excluding the patentability of business methods and algorithms with the introduction of a new article and recitals, specifically Article 4a and recitals 13a and 13c.

#aan: The introduced recitals do not achieve %(q:the Parliament's objective), as has been shown in detail in the documentation to which our letter refers:

#cnl: Amazon's 'one-click' shopping would not be patentable under the terms of the parliament's proposals.

#WWy: Amazon One Click Shopping is without any doubt patentable under the terms of the %(jp:proposal of Arlene McCarthy's Legal Affairs Committee), but not under some other parliamentary proposals, such as that of Michel Rocard's Cultural Affairs Committee.  The %(et:FFII documentation) explains in detail why this is so.

#rcW: Article 4a

#umi: Exclusions from patentability:

#hWW: A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus.  Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.

#Wep: Amazon's One Click Shopping method solves a %(q:technical problem), namely that of performing a transaction with fewer mouse clicks.  Thereby it %(q:makes a technical contribution) and %(q:produces a technical effect beyond the normal physical interactions between a program and the computer).

#etW: Recital 13a

#Wis: (13a) However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present.  Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.

#toe: The %(et:FFII documentation) comments on this specifically:

#sif: The unexperienced reader may see this as a limitation of patentability.  The experienced reader understands: a computer-implemented business method constitutes a patentable invention, provided that it is framed as a solution to a %(q:technical problem).

#hec: In the case of Amazon One Click, the contribution to the state of the art could consist in reducing the number of required mouse-clicks.  This is without any doubt a %(q:technical contribution) according to the idiom of the proposed directive.

#etW2: Recital 13c

#cal: Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention.  Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem.  However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.

#smW: To the casual reader, this suggests that algorithms are not patentable.  To the attentive reader, it says clearly that algorithms can be patented simply by framing them as solutions to a problem of improving computing efficiency, thus monopolising the algorithm's use on the universal computer in all contexts where it could ever be of practical relevance.

#aan2: McCarthy should have explained where the documentation, on which our statements are based, is wrong, before calling these statements %(q:myths) and accusing us of %(q:misinformation).

#suW: Programmes and ISPs will be regularly sued for patent infringement.

#seW: The Parliament's proposal reinforces the right of computer programmers and software developers to engage in reverse engineering practices or to achieve interoperability as currently permitted under exceptions to the Software Copyright Directive.

#qef2: The %(q:truth) is unrelated to the %(q:myth).

#dcN: JURI has, against recommendation of the European Commission and the other committees (CULT and ITRE), introduced program claims into the directive proposal.  This means that according to the JURI proposal anyone who publishes a program on the Net, including web authors and internet service providers, can be sued for patent infringment.

#Wni: Again this has been documented in detail:

#uej: The issue of reverse engineering is unrelated to this question.  It does not help if programmers are allowed to reverse-engineer a patent-encumbered program and then forbidden to publish their own program, which may be based on such reverse-engineering.

#efd: Software patents kill efficient software development.

#eWo: Patents for computer-implemented inventions do not kill companies. Some 30,000 patents have already been handed out in this area by the European Patent Office, while at the same time Opensource software companies are flourishing with one company recently posting a 50% increase in world-wide shipment of its products. The Parliament's proposal welcomes the development and growth of open source software to ensure competition in the market place and prevent the dominance of any one player.

#tln: The Parliament's rapporteur is asking the Commission to monitor the impact of this law on both SMEs and Opensource software and prevent any abuse of the patent system as regards computer-implemented inventions. On the contrary, good patent law for computer-implemented inventions will protect software development companies and give them a return on their investment through license fees, enabling them to grow their company and provide alternatives to the dominance of global, multinational companies in the field of mputer-implemented inventions.

#qef: The %(q:truth) is unrelated to the %(q:myth).

#tos: Note however that McCarthy herself uses the term %(q:computer-implemented invention) in the sense of %(q:software).

#tai: The patents which have been granted by the EPO against the letter and spirit of the existing law currently have little value in Europe, because they are not (uniformly) enforcable.  Which is why McCarthy is asking for a directive.

#npn: Many companies have complained about severe harm to their business in the US, based on US patents many of which have identical EP cousins which are waiting to be unleashed by the directive.

#dWb: The proposal would legalise thousands of mathematical rules and business methods patents that have been granted by the European Patent Office against the letter and spirit of the law, making it impossible for national courts to revoke these patents.

#Eto: This is both confusing and wrong. Patents handed out by the EPO, for computer-implemented inventions have been granted on the basis of an interpretation of the European Patent Convention (EPC).  They therefore already enjoy legal status and where appeals against them have been launched, far from seeking to revoke these patents, national courts have in the majority of cases slavishly followed the decision of the EPO.

#lOr: Our letter cited examples of national courts and national judges which have not followed the EPO.  These include the highest German, French and Swedish courts and even some %(ep:recent EPO decisions).  In most countries, courts have not decided on these matters at all.  If Arlene McCarthy has statistics which demonstrate slavish behaviour of national courts, we would like to receive a copy.

#aof: Even the EPO is showing signs of wanting to move away from its current practise.  EPO president Dr. Ingo Kober has told journalists in November 2002 that software patentability is a bad idea, and vice president Prof. Manuel Desantes said at the %(oe:OECD meeting on 2003-08-29 in Paris): %(q:If, as the evidence which we heard during this conference suggests, software patents are not contributing to innovation and economic growth in Europe, we are willing to stop granting such patents, regardless of any discussions about technical effects, which usually lead nowhere.)  The presiding judge of the German Federal Court of Justice, Dr. Klaus-Juergen Melullis, recently moroever %(km:criticised) the proposed directive, pointing out that it codifies a practise which is undesirable and incompatible with the existing law.  By codifying an undesirable practise, the directive would arrest this practise and prevent its correction.  As McCarthy herself has said, the whole point of this exercise is to provide %(q:legal certainty) to patent owners, to assure them of their %(q:legal positions), and thereby to greatly enhance the commercial value of these %(q:legal positions), which are currently often treated as invalid by the negotiating partners of the corporate patent lawyers, who are pushing for this directive.

#tAa: The Parliament's amendments, in proposing a more restrictive and clearer interpretation of the law on the patentability of computer-implemented inventions is therefore %(q:not legalising patents), but seeking to ensure that only genuine inventions enjoy patents in the future. An EU law also opens up the avenue of appeals to the European Court of Justice to challenge bad patent decisions in a transparent and accountable way. It therefore enables the creation of European case law enacting the Parliament's demands to ensure the exclusion of the patenting of the business methods.

#sin: As we have seen, there is nothing whatsoever in McCarthy's directive proposal which could be of any help in distinguishing %(q:genuine inventions) from less genuine ones.

#eaW: One should not confuse the JURI amendments with %(q:the Parliament's amendments).  The Committees for Culture and Industry have indeed proposed helpful amendments.  But McCarthy's Legal Affairs Committee (JURI) has, as shown above, rejected all amendment proposals that could have limited patentability.  The new amendments for which JURI voted upon McCarthy's recommendation can only serve to strengthen the legal position of the owners of broad and trivial patents on computer-implemented algorithms and business methods.

#aap: The European Court of Justice (ECJ) and other EU organs are not necessarily more %(q:transparent and accountable) than the EPO.  The directive proposal would also contribute very little, if anything, to putting them in charge.  The ECJ is already in charge of internal market disharmonies, including those where EPC-based patent law is concerned, and the directive would also put the ECJ in charge of patent law only as far as internal market disharmonies are concerned.

#Wai: The Parliament's job is to set the rules which the EPO should follow, not vice versa.  The Parliament's job is not to codify what may arguably be a %(q:status quo) of some administrative or judicial body, but to set rules which %(ep:promote innnovation, competition and econonomic growth in the interest of Europe's industry and citizens).

#Jie: See the EPO's new mission statement.  Judging from the EPO's statements at this august's OECD conference, the EPO, while not being a democratically elected body, seems to be giving more serious consideration to the public interest than the European Parliament's rapporteur.

#uns: Caselaw %(q:harmonisation) accross national borders will usually be achieved by any new EU law, no matter whether it promotes innovation or not.  Only a law which stifles innovation and productivity, such as the EU software patent directive as amended by Arlene McCarthy, needs %(q:caselaw harmonisation) and %(q:status quo) arguments for its justification.

#aWl: Jacob Hallén

#r5j: Jacob Hallén, swedish software entrepreneur and president of %(PBF), an organisation representing 50 software SMEs who spoke at the parliamentary hearing of May 2003, comments on McCarthy's %(q:FactSheet):

#sIe: If Ms McCarthy is really interested in the outcome she claims to want in the %(q:Factsheet), then I can't see why she opposes the %(ap:amendments proposed by the FFII). The wording of the FFII amendments would only strengthen the law and makes it less prone to interpretation. It doesn't deviate from the goals expressed by Ms McCarthy in any major way.

#eto: This leads to one of the following conclusions:

#yWo: Ms McCarthy considers it a loss of prestige to have her proposal amended.

#Weg: Ms McCarthy doesn't understand that the proposed amendments strengthen her goals.

#nWf: Ms McCarthy understands that the proposed amendments strengthen her stated goals. However her stated goals are different from her true goals.

#ehi: I'm afraid that none of these alternatives reflect well on the character of Ms McCarthy, but certain alternatives are worse than others.

#ter: FT McCarthy 2003/09/03: Let's clarify what we're protecting

#tnW: The Financial Times publishes a letter to the editor, in which McCarthy says (1) that she isn't advocating software patents (2) that small and medium size software companies need patents and must be helped by means of a %(q:patent defence insurance) to assert these patents against big bullies.

#rnm: PDF original, converted from MSWord

#mxF: Letter of %(mh:Malcolm Harbour) and %(jw:Joachim Wuermeling) to the Financial Times.

#gcr: Also accuses %(q:open source lobbyists) of %(q:misinformation) about their good intentions.  While FFII is not an %(q:open source lobby) organisation, we have indeed reported about the actual deeds of Harbour and Wuermeling rather than about their claimed intentions.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amccarthy030901 ;
# txtlang: en ;
# multlin: t ;
# End: ;

