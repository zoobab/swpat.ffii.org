LETTERS TO THE EDITOR: European businesses are losing out to clued-up competitors 
By Simon Mounteney
Financial Times; Sep 03, 2003

 
>From Mr Simon Mounteney.

Sir, Frits Bolkestein, European commissioner for the internal market, rightly asserts that we need clear European Union rules on computer-implemented inventions ("Plotting a clear path for technical patents", August 28) but fails to stress perhaps the most important benefit of greater legal clarity for European companies.

There are already thousands of European patents for computer-implemented inventions, many filed by US and Japanese companies. However, varying legal treatments across the EU member states has created confusion. As a result, too few European businesses realise that it is possible to protect their inventions at all, let alone in their home market.

This is the real reason companies in the EU are losing out to competitors in the US and Japan, who are well aware of the commercial value of patents and immediately file to protect their innovations at home and abroad.

The Commission's proposed directive on the patentability of computer-implemented inventions would not significantly alter the availability of "software patents" in the EU. However, if the directive were to succeed in promoting greater understanding and clarity on the current legal position, the EU should be in a better position to exploit its intellectual property.

Simon Mounteney, Partner, Marks & Clerk, London WC2A 3LS
 
