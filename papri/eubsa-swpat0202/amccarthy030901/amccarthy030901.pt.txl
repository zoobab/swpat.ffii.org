<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Arlene McCarthy 2003/09/01: %(q:Os Mitos - A Verdade)

#descr: Em resposta à onda de protestos contra a proposta de directiva sobre patentes de software COM(2002)92 2002/0047 de finais de Agosto de 2003, a relatora do Parlamento Europeu para esta directiva, a Euro-deputada Arlene McCarthy, publicou uma %(q:Lista de Factos) que tenta explicar como tem sido uma vítima de uma %(q:campanha de desinformação) e que na realidade defende a causa dos protestantes. Aqui republicamos o documento de forma comentada.

#tin: Desinformação sobre Desinformadores?

#reW: %(q:Invenção Implementada num Computador) não Software?

#hRn: %(q:Solução Técnica): Confiando num Requisito Abolido

#eqi: Caso de Teste do One Click da Amazon: atingido %(q:o Objectivo do Parlamento)?

#anW: Banir a Publicação não é um Problema?

#WtF: Patentes de Software Não Permesidas mas Muito Benéficas?

#nWl: Tribunais Nacionais: Seguidismo Fiel?

#yei: Apenas Invenções Genuínas?

#tam: O título do texto acusa uma organização desconhecida de fazer uma %(q:campanha de desinformação):

#Wsn: FACTSHEET - EU rules for patents for computer-implemented inventions

#flt: The Misinformation Campaign: Claims by the Free Software Alliance

#eno: Assumimos que Arlene McCarthy se refere ou à FFII (Fundação para uma Infra-estrutura de Informação Livre) ou à Aliança Eurolinux. Ambas as organizações são suportadas por centenas de companhias de software que produzem software não-livre.

#Wkr: Os Mitos - A Verdade

#svc: No que se segue, McCarthy tenta refutar vários %(q:mitos). Não temos a certeza de quem serão os autores dos %(q:mitos), porque McCarthy não cita fontes. Contudo, é evidente que a maioria dos %(q:mitos) tem a sua origem no %(xm:convite da FFII para os eventos de 2003/08/27), que foi enviado aos membros do Parlamento Europeu por email e fax.

#Wat: Isto é uma proposta para uma directiva de patentes de software

#aoe: A proposta é para uma patente abranger invenções implementadas num computadores.

#hle: Na lei, o software como tal, não é patenteável. Tal como uma invenção no mundo físico pode aproveitar os direitos de patentes, também o pode fazer uma invenção que dependa de um programa de computador.

#lia: Na Comissão Europeia, esta proposta de directiva é coloquialmente referida como %(q:a proposta de patentes de software) ou %(q:proposta softpat). David Ellard, uma das pessoas responsáveis pela Unidade de Propriedade Industrial, referiu-se à proposta como a %(q:directiva de patenteabilidade do software) tanto no programa como na sua oração na conferência OECD em Paris, a 28 e 29 de Agosto.

#titjust: O termo %(q:invenção implementada num computador) não é utilizado pelos profissionais da informática. De facto, nem sequer é vastamente utilizado . Foi introduzido pelo Gabinete Europeu de Patentes (EPO) em Maio de 2000 no %(a6:Apêndice 6) da Conferência Trilateral, onde serviu para legitimar patentes de planos de negócio, trazendo assim a prática do EPO à linha do modelo dos EUA e Japão. Muito da proposta de directiva da Comissão Europeia é baseado em termos e expressões deste %(q:Apêndice 6).

#puW: De acordo com a definição original do EPO a 2000, que é utilizada pela Comissão Europeia, a expressão %(q:invenções implementadas num computador) não se refere a mais nada para além de software no contexto das reivindicações de patentes. Dessa forma, o sistema anti-bloqueio (ABS) pode não se qualificar como uma invenção implementada num computador, porque as %(q:capacidades prima facie novas) não residem no sistema de processamento de dados mas no modo em como a fricção é aplicada nos pneus através dos travões dos automóveis. Assim o ABS não seria uma %(q: invenção implementada num computador) mas uma %(q:invenção implementada em travões).

#Wae: A própria Arlene McCarthy utiliza a expressão %(q:invenção implementada num computador) como sinónimo para %(q:inovação em software) na %(q:Lista de Factos) (ver abaixo), e com razão.

#oec: Até 1986 as inovações no software não eram consideradas invenções patenteáveis pelo Gabinete Europeu de Patentes. Hoje em dia, alguns tribunais nacionais continuam a rejeitá-las como não patenteáveis. Exemplos de jurisprudência que aplicam o Artigo 52º da Convenção Europeia da Patentes (EPC) podem ser revistos em

#tWe: Estas duas patentes teriam de ser concedidas no futuro coberto pela directiva que Arlene McCarthy orienta.

#ttW: Isto não é novidade. O Gabinete Europeu de Patentes e os gabinetes de patentes nacionais tem estado a conceder patentes para invenções implementadas num computador. A proposta aplica-se estritamente a invenções que têm de satisfazer as condições de qualquer invenção; têm de ser uma novidade, envolver um passo inventivo e têm de dar uma contribuição técnica providenciando uma solução técnica para um problema.

#nrW: Em lado nenhuma na proposta de McCarthy existe um requisito de existir %(q:uma solução técnica para um problema). Pelo contrário, o Considerando 12 tal como emendado por Arlene McCarthy garante que a existência de um %(q:problema técnica) é suficiente, cuidadosamente negando a aproximação anterior do EPO, que exigia uma %(q:solução técnica) também:

#bec: Deste modo, embora os inventos que implicam programas de computador pertençam, pela sua própria natureza, a um determinado domínio da tecnologia, é importante deixar claro que, se um invento não der um contributo técnico para o progresso tecnológico, como aconteceria, por exemplo, se o seu contributo específico não tivesse carácter técnico, o invento não apresentará uma actividade inventiva, pelo que não será patenteável.

#iee: Quando se pretende saber se se trata de uma actividade inventiva ou não, é habitual recorrer à aproximação problema e solução de forma a estabelecer que há um problema técnico a ser resolvido. Se não estiver presente nenhum problema técnico, então a invenção não pode ser considerada como estando a contribuir para o progresso tecnológico.

#liW: Como bem explica o Tribunal Federal de Patentes na sua decisão de 2002 sobre Pesquisa de Erros:

#bpatg17: Se a solução de um problema técnico pudesse ser uma razão suficiente para atribuir um carácter técnico a uma implementação num computador, então toda a implementação de um método não técnico teria de ser patenteável; contudo isto iria contra a conclusão do Tribunal Federal de Justiça de que a exclusão legal da patenteabilidade dos programas de computador não nos permite adoptar uma aproximação que tornaria patenteável qualquer ensinamento que seja enquadrado em instruções orientadas ao computador.

#pnW: Apontamos isto a McCarthy assim que o rascunho do seu relatório saiu:

#Whe: McCarthy, contudo, insiste nas suas emendas. Parece saber que um requisito de %(q:solução técnica) seria melhor do que um de %(q:problema técnico), e que a sua plateia quer ouvir que tal requisito existe na proposta de directiva. Mas não existe, graças aos seus próprios esforços.

#Wao: A proposta irá impor patenteabilidade ilimitada de algoritmos e planos de

#Wre: Na realidade o objectivo do Parlamento é parar o desvio do EPO e dos gabinetes de patentes nacionais de patentearem planos de negócio. A proposta do Parlamento é mais forte do que a lei actual e a prática do EPO e exclui explicitamente a patenteabilidade de planos de negócio e algoritmos com a introdução de um novo artigo e considerandos, nomeadamente o Artigo 4 bis e os Considerandos 13 bis e 13 quater.

#aan: Os considerandos introduzidos não atingem %(q:o objectivo do Parlamento), como é demonstrado detalhadamente na documentação à qual a nossa carta se refere:

#cnl: O 'one-click' shopping da Amazon não seria patenteável segundo os termos das propostas do parlamento.

#WWy: O One Click Shopping da Amazon é sem dúvida patenteável segundo os termos da %(jp:proposta do Comité dos Assuntos Jurídicos de Arlene McCarthy), mas não segundo outras propostas parlamentares, tal como a do Comité Cultural de Michel Rocard. A %(et:documentação da FFII) explica detalhadamente porque é que isto acontece.

#rcW: Artigo 4 bis

#umi: Exclusões de patenteabilidade:

#hWW: Um invento que implique programas de computador não será encarado como sendo um contributo técnico apenas e só por envolver a utilização de um computador, de uma rede, ou de qualquer outro aparelho susceptível de programação. Da mesma forma, os inventos que impliquem programas de computador destinados à execução de procedimentos de natureza comercial, matemática ou outra, e que não produzam quaisquer efeitos técnicos, para além das normais interacções físicas entre o programa e a máquina, a rede ou qualquer outro aparelho programável em que possa correr, não será patenteável.

#Wep: O One Click Shopping da Amazon resolve um %(q:problema técnico), nomeadamente o de fazer uma transacção com menos cliques no rato. Assim %(q:torna-se uma contribuição técnica) e %(q:produz um efeito técnico para além das normais interacções físicas entre um programa e o computador).

#etW: Considerando 13 bis

#Wis: (13a) However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present.  Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.

#toe: A %(et:documentação da FFII) comenta sobre isto especificamente:

#sif: O leitor inexperiente pode ver isto como uma limitação da patenteabilidade. O leitor experiente entende: um plano de negócio implementado num computador constitui uma invenção patenteável, desde que seja enquadrado como uma solução para um %(q:problema técnico).

#hec: No caso do One Click da Amazon, a contribuição para o progresso tecnológico consistiria na redução do número necessário de cliques no rato. Isto é sem dúvida uma %(q:contribuição técnica) conforme a idiomática da proposta de directiva.

#etW2: Considerando 13 quater

#cal: Para além disso, um algoritmo, em si mesmo, não se reveste de índole técnica, pelo que não pode constituir um invento de carácter técnico.  Não obstante, um método que envolva o recurso a um algoritmo poderá ser patenteável, na condição de esse método ser utilizado para solucionar um problema técnico. Uma patente concedida a um método desse tipo não poderá, todavia, monopolizar o próprio algoritmo ou o respectivo uso em contextos que não estejam previstos na patente.

#smW: Isto sugere, ao leitor casual, que os algoritmos não são patenteáveis.  Para o leitor atento, diz claramente que os algoritmos podem ser patenteados simplesmente enquadrando-os como soluções para um problema de melhoria de eficiência do computador, monopolizando assim a utilização do algoritmo no computador universal em todos os contextos onde poderia ter relevância prática.

#aan2: McCarthy deveria ter explicado onde é que a documentação, na qual baseamos as nossas afirmações, está errada, antes de as acusar de %(q:mitos) e acusando-nos de %(q:desinformação).

#suW: Programadores e ISPs serão regularmente processados por infracção de patentes.

#seW: A proposta do Parlamento reforça o direito dos programadores de computadores e investigadores de software a praticar engenharia reversa ou a atingir a interoperabilidade tal como é permitido actualmente como excepções da Directiva sobre o Direito de Autor de Software.

#qef2: A %(q:verdade) não tem qualquer relação com o %(q:mito).

#dcN: A JURI introduziu, contra a recomendação da Comissão Europeia e os outros comités (CULT e ITRE), reivindicações de programas na proposta de directiva. Isto significa que conforme a proposta da JURI, qualquer um que publique um programa na Rede, incluindo autores de páginas Internet e ISPs, pode ser processado por infracção de patentes.

#Wni: Mais uma vez isto foi documentado detalhadamente:

#uej: O problema da engenharia reversa não tem qualquer relação com esta questão. Não ajuda se os programadores tiverem permissão para a fazer sobre um programa ofuscado por patentes e depois os proíbe de publicar o seu próprio programa, que pode ser baseado em tal engenharia reversa.

#efd: As patentes de software matam o desenvolvimento eficiente de software.

#eWo: As patentes para invenções implementadas num computador não matam companhias. Cerca de 30000 patentes já foram concedidas nesta área pelo Gabinete Europeu de Patentes, enquanto que ao mesmo tempo companhias de software Opensource florescem tendo uma delas anunciado recentemente um aumento de 50% na distribuição mundial dos seus produtos. A proposta o parlamento dá as boas vindas ao desenvolvimento e crescimento do software open source para garantir a competição no mercado e impedir o domínio por um só elemento.

#tln: A relatora do Parlamento está a pedir à Comissão que monitorize o impacto desta lei tanto em PMEs como no software Opensource e que impeça qualquer abuso do sistema de patentes no que diz respeito a invenções implementadas num computador. Pelo contrário, uma boa lei sobre invenções implementadas num computador irá proteger as companhias de desenvolvimento de software e dar-lhes um retorno ao seu seu investimento através das receitas de licenças, permitindo-lhes crescer e providenciar alternativas ao domínio de companhias multinacionais, globais, no campo das invenções implementadas num computador.

#qef: A %(q:verdade) não tem qualquer relação com o %(q:mito).

#tos: Note-se, contudo, que a própria McCarthy utiliza a expressão %(q:invenção implementada num computador) no sentido de %(q:software).

#tai: As patentes que têm sido concedidas pelo EPO contra a letra e o espírito da lei vigente têm pouco valor na Europa, porque não são (uniformemente) aplicáveis. É por causa disto que McCarthy está a pedir uma directiva.

#npn: Muitas companhias têm reclamado sérios danos nos seus negócios nos EUA, baseados em patentes dos EUA muitas das quais com primos idênticos nas Patentes Europeias que apenas esperam ser libertados pela directiva.

#dWb: A proposta irá legalizar milhares de patentes de regras matemáticas e planos de negócio que têm sido concedidas pelo Gabinete Europeu de Patentes contra a letra e o espírito da lei, tornando inviável a sua revogação pelos tribunais nacionais.

#Eto: Isto é tão confuso quanto errado. As patentes entregues pelo EPO, para invenções implementadas num computador, têm sido concedidas com base numa interpretação da Convenção Europeia de Patentes (EPC). Assim elas já dispões de um estatuto de legalidade e quando desafiadas por apelos em tribunal, longe de procurar revogar essas patentes, os tribunais nacionais têm na maioria dos casos seguido fielmente a decisão do EPO.

#lOr: A nossa carta citava exemplos de tribunais nacionais e juízes nacionais que não seguiram o EPO, Estes incluem os mais elevados tribunais alemães, franceses e suecos e mesmo algumas $(ep:decisões recentes do EPO). Na maioria dos países, os tribunais não decidiram sequer sobre estas matérias. Se Arlene McCarthy tem estatísticas que demonstram um seguidismo fiel da parte dos tribunais nacionais, gostaríamos de receber uma cópia.

#aof: Até mesmo o EPO está a dar sinais de querer afastar-se da sua prática actual. O presidente do EPO, Dr. Ingo Kober, disse a jornalistas em Novembro de 2002 que a patenteabilidade do software é uma ideia má, e o vice-presidente, Prof. Manuel Desantes, disse no %(oe:encontro OECD em 2003-08-29 em Paris): %(q:Se, como as provas que ouvimos nesta conferência sugerem, as patentes de software não estão a contribuir para a inovação e o crescimento económico na Europa, estamos dispostos a parar de conceder tais patentes, apesar de qualquer discussão a respeito de efeitos técnicos, que normalmente não levam a lado nenhum.) O juiz-presidente do Tribunal Federal de Justiça alemão, Dr. Klaus-Juergen Melullis, recentemente acrescentou %(km:críticas) à proposta de directiva, indicando que codifica uma prática que é indesejável e incompatível com a lei vigente. Codificando uma prática indesejável, a directiva força-nos a essa prática e impede a sua correcção. Como McCarthy disse, o sentido deste exercício é precisamente providenciar uma %(q:certeza legal) aos detentores de patentes, para lhes garantir as suas %(q:posições legais) e assim ampliar enormemente o valor comercial destas %(q:posições legais), que são actualmente tratadas frequentemente como inválidas pelos parceiros de negócio dos advogados de patentes empresariais, que estão a promover a toda a força esta directiva.

#tAa: As emendas do Parlamento, ao propor uma interpretação mais restritiva e clara da lei sobre a patenteabilidade de invenções implementadas num computador está, portanto, %(q:não a legalizar patentes), mas a procurar garantir que apenas invenções genuínas possam gozar de patentes no futuro. Uma lei na UE também abre uma via para apelos ao Tribunal Europeu de Justiça para desafiar más decisões de patentes de uma forma transparente e contabilizável. Assim capacita a criação de jurisprudência Europeia que actuando as exigências do Parlamento para garantir a exclusão da patenteabilidade de planos de negócio.

#sin: Como vimos não há nada, seja o que for, na proposta de directiva de McCarthy que poderia ser de alguma ajuda para distinguir %(q:invenções genuínas) das menos genuínas.

#eaW: Não se deve confundir as emendas da JURI com %(q:as emendas do Parlamento). Os Comités para a Cultura e Indústria propuseram, de facto, emendas úteis. Mas o Comité dos Assuntos Jurídicos de McCarthy (JURI) rejeitou, como já vimos, todas as propostas de emendas que poderiam limitar a patenteabilidade. As novas emendas que a JURI votou segundo as recomendações de McCarthy apenas servem para reforçar a posição legal dos donos de patentes triviais e abrangentes sobre algoritmos implementados num computador e planos de negócio.

#aap: O Tribunal Europeu de Justiça (ECJ) e outros órgãos da UE não são necessariamente mais %(q:transparentes e contabilizáveis) do que o EPO. A proposta de directiva também contribuiria muito pouco, se alguma coisa, para os colocar como responsáveis pelo processo legislativo. O ECJ já é responsável por desarmonias no mercado interno, incluindo aquelas que dizem respeito a leis de patentes baseadas na EPC, e a directiva iria também por o ECJ como responsável pela lei de patentes apenas no que diz respeito a desarmonias no mercado interno.

#Wai: A função do Parlamento é definir o conjunto de regras que o EPO deveria seguir, e não vice-versa. A função do Parlamento não é codificar o que pode discutivelmente ser um %(q:status quo) de alguma entidade administrativa ou judicial, mas definir regras que %(ep:promovam inovação, competição e crescimento económico no interessa dos cidadãos Europeus).

#Jie: Ver a nova declaração de missão do EPO. A julgar pelas declarações do EPO na conferência OECD no passado mês de Agosto, o EPO, embora não seja uma entidade democraticamente eleita, parece estar a levar a sua declaração de missão mais seriamente do que a relatora do Parlamento Europeu.

#uns: A %(q:harmonização) da jurisprudência através das fronteiras nacionais irá ocorrer como de costume através de qualquer nova lei da UE, não importa se promove a inovação ou não. Apenas uma lei que estagna a inovação e a produtividade, tal como a proposta de directiva de patentes de software da UE emendada por Arlene McCarthy, precisa de argumentos como a %(q:harmonização da jurisprudência) e do %(q:status quo) como justificação.

#aWl: Jacob Hallén

#r5j: Jacob Hallén, empresário de software sueco e presidente do %(PBF), uma organização representando 50 PMEs de software, que falou numa audição parlamentar em Maio de 2003, comenta a %(q:Lista de Factos) de McCarthy:

#sIe: Se a Sra. McCarthy está realmente interessada no objectivo que alega querer na sua %(q:Lista de Factos), então eu não consigo ver porque é que se opõe às %(ap:emendas propostas pela FFII). A sua formulação apenas reforça a lei e torna-a menos susceptível a interpretações. Não se desvia de forma significativa dos objectivos exprimidos pela Sra. McCarthy.

#eto: Isto leva a uma das seguintes conclusões:

#yWo: A Sra. McCarthy considera uma perda de prestígio ter a sua proposta emendada.

#Weg: A Sra. McCarthy não entende que as emendas propostas reforçam os seus objectivos.

#nWf: A Sra. McCarthy entende que as emendas propostas reforçam os seus objectivos declarados. Contudo, os seus objectivos declarados são diferentes dos seus verdadeiros objectivos.

#ehi: Receio que nenhuma destas alternativas reflicta bem sobre o carácter da Sra. McCarthy, mas certas alternativas são menos más que outras.

#ter: FT McCarthy 2003/09/03: Vamos clarificar o que vamos proteger

#tnW: O Financial Times publica uma carta ao editor, na qual McCarthy diz (1) que não está a promover patentes de software (2) que as pequenas e médias empresas de software precisam de patentes e que devem ser ajudadas através de %(q:seguros de defesa de patentes) para as proteger contra grandes rufiões.

#rnm: PDF original, convertido do MSWord

#mxF: Carta de %(mh:Malcolm Harbour) e %(jw:Joachim Wuermeling) ao Financial Times.

#gcr: Também acusa %(q:lobbyistas open source) de %(q:desinformação) a respeito das suas boas intenções. Enquanto que a FFII não é uma organização de %(q:lobby open source) temos, de facto, reportado acerca dos actos de Harbour e Wuermeling e não das suas alegadas intenções.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: rumise ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amccarthy030901 ;
# txtlang: pt ;
# multlin: t ;
# End: ;

