<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Arlene McCarthy 2003/09/01: %(q:Les Mythes - La Réalité)

#descr: En réponse à la vague de protestations contre la directive sur les brevets logiciels COM(2002)92 2002/0047 proposée fin août 2003, le rapporteur du Parlement européen pour cette directive, la Membre du Parlement (MPE) Arlene McCarthy a publié un %(q:ensemble de faits) qui tente d'expliquer qu'elle est la victime d'une %(q:campagne de désinformation) et qu'en réalité elle combat en faveur de la cause des protestataires. Nous publions ici cet article, accompagné des commentaires adéquats.

#tin: Désinformation sur les désinformateurs?

#reW: %(q:Invention mise en oeuvre sur ordinateur) non logicielle?

#hRn: %(q:Solution technique): on se fie à une condition supprimée

#eqi: L'esample d'%(q:Amazon One Click): L'%(q:objectif du Parlement) achevé?

#anW: L'interdiction de publication n'est pas un problème?

#WtF: Brevets logiciels non permis mais très bénéfiques?

#nWl: Les tribunaux nationaux: des suiveurs passifs?

#yei: Seulement de réelles inventions?

#tam: Le titre du text accuse une organisation inconnue de mener une %(q:campagne de désinformation):

#Wsn: FACTSHEET - EU rules for patents for computer-implemented inventions

#flt: La campagne de désinformation: les réclamations de l'Alliance du logiciel libre

#eno: Nous supposons qu'Arlene McCarthy fait référence soit à la FFII (Foundation for a Free Information Infrastructure - Fondation pour une Infrastructure Informatique Libre) ou à l'Alliance Eurolinux. Ces organisations sont toutes deux soutenues par des centaines de sociétés informatiques qui produisent des logiciels propriétaires.

#Wkr: Les Mythes - La réalité

#svc: Ci-dessous, McCarthy tente de réfuter divers %(q:mythes). Nous ne savons pas précisément qui sont les auteurs de ces %(q:mythes), parce que McCarthy ne cite pas de sources. Cependant, il est clair que la plupart des %(q:mythes) ont pour origine l'%(xm:la lettre d'invitation de la FFII aux événements du 27/8/2003), qui fut envoyée aux membres du Parlement européen par courriel et fax.

#Wat: Ceci est une proposition de directive pour le brevet logiciel

#aoe: La proposition concerne les brevets couvrant les inventions mises en oeuvre sur ordinateur.

#hle: Selon la loi, le logiciel, en tant que tel, n'est pas brevetable. De même qu'une invention dans le monde physique peut jouir des droits d'un brevet, c'est également la cas d'une invention dépendant d'une application informatique.

#lia: A la Commission européenne, la proposition de directive est familièrement denommée %(q:proposition de directive pour le brevet logiciel) ou %(q:proposition softpat). David Ellard, l'un des responsables de l'Unité Propriété Industrielle, y a fait référence en tant que %(q:directive sur la brevetabilité des logiciels) dans le programme écrit de même que dans son discours à la conférence de l'OCDE à Paris, les 28 et 29 août.

#titjust: Le terme %(q:invention mise en oeuvre sur ordinateur) n'est pas utilisé par les professionnels de l'informatique. En fait, ce terme n'est quasiment jamais utilisé. Il a été introduit par l'Office européen des brevets (OEB) en mai 2000 dans l'%(a6:Appendice 6) de la Conférence trilatérale, où il a servi à légitimer des brevets sur des méthods organisationnelles, afin d'aligner l'OEB sur les pratiques exercées aux Etats-Unis et au Japon. Une grande partie de la proposition de directive de la Commission européenne est basée sur les énoncés de cet %(q:Appendice 6).

#puW: Selon la définition originelle de l'OEB de 2000, qui est utilisée par la Commission européenne, le terme %(q:invention mise en oeuvre sur ordinateur) ne définit rien d'autre qu'un logiciel dans le contexte des demandes de brevets. En conséquence, un système anti-blocage ne pourrait être considéré comme une invention mise en oeuvre sur ordinateur, parce que la %(q:nouveauté) ne réside pas dans le traitement des données mais bien dans la façon dont la friction est appliquée sur les pneus de l'automobile, au moyen des freins de l'automobile. L'ABS ne pourrait dès lors être une %(q:invention mise en oeuvre sur ordinateur) mais une %(q:invention mise en oeuvre sur des freins).

#Wae: Arlene McCarthy elle-même utilise le terme %(q:invention mise en oeuvre sur ordinateur) comme synonyme d'%(q:innovation logicielle) dans l'%(q:ensemble de faits) (voir plus bas), et ce tout-à-fait correctement.

#oec: Jusqu'en 1986, L'Office européen des brevets ne considéraient pas les innovations logicielles comme étant sujet à demande de brevet. Certains tribunaux nationaux continuent à les rejeter jusqu'à maintenant. Des examples de jurisprudence qui applique correctement l'Art 2 EPC peuvent être consultés à

#tWe: Ces deux brevets logiciels devraient à l'avenir être accordés, selon la directive promue par Arlene McCarthy.

#ttW: Ce n'est pas nouveau. L'Office européen des brevets et les offices nationaux des brevets ont déjà accordé des brevets pour des inventions mises en oeuvre sur ordinateur. La proposition s'applique strictement à des inventions qui doivent satisfaire les conditions de n'importe qu'elle invention; elle doit être nouvelle, faire preuve d'une marque d'inventivité et proposer une contribution technique menant à une solution technique d'un problème.

#nrW: Nulle part dans la proposition de McCarthy n'est mentionnée une condition telle que la %(q:solution technique à un problème).  Au contraire, le considérant 12 tel qu'amendé par Arlene McCarthy garantit que l'existence d'un %(q:problème technique) est suffisante, anéantissant par là l'approche précédente de l'OEB, qui exigeait également une %(q:solution technique):

#bec: Donc, même si une invention mise en oeuvre sur ordinateur fait partie, par nature, du domaine de la technologie, il est important de dire clairement que, lorsqu'une invention napporte pas de contribution technique à l'état de l'art, comme ce serait le cas par exemple si sa contribution spécifique ne possède pas de caractère technique, l'invention ne fera pas preuve d'inventivité et conséquemment ne sera pas brevetable.

#iee: Lorsqu'on détermine s'il existe une marque d'inventivité, on applique habituellement l'approche %(q:problème et solution) afin d'établir qu'un problème technique doit être résolu. S'il n'y a pas de problème technique à résoudre, alors on ne peut affirmer que l'invention apporte une contribution technique à l'état de l'art.

#liW: Comme la Cour fédérale des brevet l'explique clairement dans sa décision de 2002 sur la %(q:Recherche d'Erreur), citée ci-dessus:

#bpatg17: Si la solution d'un problème technique pouvait être une raison suffisante pour attribuer un caractère technique à une mise en oeuvre logicielle, alors toute mise en oeuvre technique d'une méthode non technique devrait être brevetable; cependant, ceci contredirait la conclusion de la Cour fédérale de justice selon laquelle l'exclusion légale des programmes d'ordinateur de la brevetabilité ne nous permet pas d'adopter une approche qui rendrait brevetable toute connaissance exprimée sous forme d'instructions programmatiques.

#pnW: Nous avons fait remarquer ceci à McCarthy dès que son rapport préliminaire fut publié:

#Whe: McCarthy a cependant tenu à maintenir son amendment. Elle semble savoir qu'une condition telle que la %(q:solution technique) serait plus appropriée que la mention d'un %(q:problème technique), et que le public veut qu'on telle condition apparaisse dans la proposition de directive. Néanmoins, ce n'est pas le cas, grâce aux efforts de McCarthy elle-même.

#Wao: La proposition imposerait une bervetabilité illimitée, %(q:à l'américaine), des algorithmes et des méthodes organisationnelles telles que l'%(q:Amazon One-Click Shopping).

#Wre: En fait, l'objectif du Parlement est de stopper la dérive de l'OEB et des offices nationaux de brevets dans la fourniture de brevets sur les méthodes organisationnelles. La proposition du Parlement est plus sévère que la loi en vigueur et la pratique actuelle de l'OEB et exclut explicitement la brevetabilité des méthodes organisationnelles et des algorithmes par l'introduction de nouveaux articles et considérants, spécifiquement l'article 4a et les considérants 13a et 13c.

#aan: Les considérants rajoutés n'accomplissents pas les %(q:objectifs du Parlement), comme l'a montré de manière détaillée la documentation à laquelle se réfère notre lettre:

#cnl: %(q:Amazon One-Click Shopping) ne serait pas brevetable en raison des propositions du Parlement.

#WWy: %(q:Amazon One-Click Shopping) est sans aucun doute brevetable selon les termes de la %(jp:proposition du Comité des affaires juridiques d'Arlene McCarthy), mais pas selon diverses autres propositions parlementaires, comme celles du Comité des affaires culturelles de Michel Rocard. La %(et:documentation FFII) l'explique en détails.

#rcW: Article 4a

#umi: Exclusion de la brevetabilité:

#hWW: Une invention mise en oeuvre sur ordinateur ne sera pas considérée comme apportant une contribution technique simplement parce qu'elle comprend l'utilisation d'un ordinateur, un réseau ou un autre appareil programmable. En conséquence, des inventions comportant des programmes d'ordinateur qui mettent en oeuvre des méthodes organisationnelles, mathématiques ou autres et ne produisant aucun effet technique autre que les interactions physiques normales entre un programme et l'ordinateur, le réseau ou tout autre appareil programmable dans lequel il est exécuté, ne seront pas brevetables.

#Wep: La méthode %(q:One Click Shopping) d'Amazon résout un %(q:problème technique), en l'occurence celui d'exécuter une transaction en moins de %(q:clicks) de souris. De ce fait, elle %(q:apporte une contribution technique) et %(q:produit un effet technique au-delà des interactions physiques normales entre un programme et l'ordinateur).

#etW: Considérant 13a

#Wis: (13a) Cependant, la seule mise en oeuvre d'une méthode, par ailleurs non brevetable, sur un appareil tel qu'un ordinateur n'est pas en soi suffisant pour affirmer qu'une contribution technique existe. En conséquence, une méthode organisationnelle, ou autre, mise en oeuvre sur ordinateur, dont la seule contribution à l'état de l'art est non technique, ne peut être une invention brevetable.

#toe: La %(et:documentation FFII) commente ceci en particulier:

#sif: Le lecteur non initié peut considérer ceci comme une limitation de la brevetabilité. Le lecteur expérimenté comprend ceci: une méthode organisationnelle mise en oeuvre sur ordinateur peut être une invention brevetable pourvu qu'elle soit présentée comme une solution à un %(q:problème technique).

#hec: Dans le cas de %(q:Amazon One Click), la contribution à l'état de l'art peut consister en la réduction du nombre de %(q:clicks) requis. C'est à n'en pas douter une %(q:contribution technique) selon le langage de la proposition de directive.

#etW2: Considérant 13c

#cal: De plus, un algorithme est par essence non technique et donc ne peut constituer une invention technique. Néanmoins, une méthode comprenant l'utilisation d'un algorithme pourrait être brevetable pourvu qu'elle soit utilisée pour résoudre un problème technique. Cependant, tout brevet accordé pour une telle méthode ne pourra monopoliser l'algorithme lui-même ou son utilisation dans des contextes non prévus dans le brevet.

#smW: Pour le lecteur non averti, ceci suggère que les algorithmes ne sont pas brevetables. Pour le lecteur attentif, ceci dit clairement que les algorithmes peuvent être brevetés simplement en les présentant comme des solutions à un problème d'amélioration de l'efficacité du calcul, monopolisant de ce fait l'utilisation de l'algorithme sur l'ordinateur universel dans tous les contextes où il pourrait être d'une quelconque utilité.

#aan2: McCarthy aurait dû expliquer en quoi la documentation sur laquelle sont basées nos affirmations est erronée, avant de qualifier ces affirmations de %(q:mythes) et de nous accuser de %(q:désinformation).

#suW: Programmeurs and FAI seront régulièrement poursuivis en justice pour infraction au brevet.

#seW: La proposition du Parlement renforce le droit des programmeurs et développeurs de logiciels à pratiquer le %(q:reverse engineering) ou à mener à bien l'interopérabilité comme actuellement permis selon les exceptions à la Directive sur les droits d'auteurs de logiciels.

#qef2: La %(q:réalité) n'a pas de rapport avec le %(q:mythe).

#dcN: En dépit de la recommandation de la Commission européenne et d'autres comités (CULT et ITRE), JURI a introduit les revendications de programme dans la proposition de directive. Cela signifie que selon la proposition de JURI, quiconque publie un programme sur Internet, y compris des créateurs de sites et des fournisseurs d'accès, peut être poursuivi pour infraction au brevet.

#Wni: A nouveau, il existe une documentation détaillée:

#uej: La question du %(q:reverse engineering) n'a aucun rapport avec ce problème. Il ne sert à rien que les programmeurs soient autoriser à analyser un programme breveté pour se voir ensuite interdire la publication de leur propre programme.

#efd: Les brevets logiciels tuent le développement de logiciels.

#eWo: Les brevets pour des inventions mises en oeuvre sur ordinateur ne tuent pas les entreprises. Quelque 30000 brevets ont déjà été accordés dans ce domaine par l'Office européen des brevets, tandis que les sociétés de logiciels %(q:Open Source) sont en plein essor, une société ayant récemment annoncé une augmentation de 50% de la diffusion mondiale de ses produits. La proposition du Parlement accueille favorablement le développement et la croissance des sociétésde logiciels à code source ouvert pour garantir la compétition sur les marchés et prévenir la domination de l'un des acteurs.

#tln: Le rapporteur du Parlement demande à la Commission de mesurer l'impact de cette loi sur les PME et sur le logiciel à code source ouvert et de prévenir tout abus du système de brevet concernant les inventions mises en oeuvre sur ordinateur. Au contraire, une bonne loi sur les brevets des inventions mises en oeuvre sur ordinateur protégera les sociétés de développements de logiciels et leur fournira un retour sur investissement par l'intermédiaire de droits de licence, leur permettant de faire grandir leur société, et créera une alternative à la domination d'entreprises globales multinationales dans le domaine des inventions mises en oeuvre sur ordinateur.

#qef: La %(q:réalité) n'a aucun rapport avec le %(q:mythe).

#tos: Notez cependant que McCarthy elle-même utilise le terme %(q:invention mise en oeuvre sur ordinateur) dans le sens de %(q:logiciel).

#tai: Les brevets qui ont été accordés par l'OEB en dépit de la lettre et de l'esprit de la loi ont actuellement peu de valeur en Europe, parce qu'ils ne sont pas uniformément exécutoires. C'est bien pourquoi McCarthy demande une directive.

#npn: De nombreuses sociétés se sont plaintes d'une sévère entrave à leurs affaires aux Etats-Unis, à cause des brevets américains, dont une grande part a des cousins européens qui attendent d'être lâchés par la promulgation de la directive.

#dWb: La proposition légaliserait des milliers de brevets concernant des méthodes organisationnelles et des algorithmes mathématiques qui ont été accordés par l'Office européen des brevets en contradiction avec les lois en vigueur, rendant impossible la révocation de ces brevets par les tribunaux nationaux.

#Eto: Ceci est à la confus et faux. Les brevets octroyés pas l'OEB pour des inventions mises en oeuvre sur ordinateur ont été accordés sue base d'une interprétation de la Convention européenne sur les brevets (CEB). Ils jouissent donc d'ores et dèjà d'un statut légal et lorsque des appels ont été enregistrés contre eux, les tribunaux nationaux n'ont pas tenté de les annuler mais ont, dans la majorité des cas, suivi la décision de l'OEB.

#lOr: Notre lettre citait des exemples où des tribunaux et des juges nationaux n'ont pas suivi l'OEB. Parmi ceux-ci les plus hautes cours allemande, française et suédoise et même quelques %(ep:récentes décisions de l'OEB). Dans la plupart des pays, les tribunaux n'ont pas statués sur ces matières. Si Arlene McCarthy possède des statistiques qui démontrent un comportement suiviste des tribunaux nationaux, nous aimerions en avoir copie.

#aof: Même l'OEB semble vouloir renoncer à ses pratiques actuelles. Le président de l'OEB, le docteur Ingo Kober, a déclaré à des journalistes en novembre 2002 que la brevetabilité des logiciels est une mauvaise idée, et le vice-président, le professeur Manuel Desantes a dit, lors de la %(oe:réunion de l'OCDE le 29/8/2003 à Paris): %(q:Si, comme les preuves que nous avons entendues durant cette conférence le suggèrent, les brevets logiciels ne contribuent pas à l'innovation et à la croissance économique en Europe, nous sommes désireux d'arrêter d'octroyer ces brevets, quels que soient les résultats des discussions à propos d'effets techniques, qui généralement ne mènent nulle part.) Le juge présidant la Cours fédérale de justice allemande, le docteur Klaus-Juergen Melullis, a en plus récemment %(km:critiqué) la proposition de directive, soulignant qu'elle codifie une pratique indésirable et incompatible avec la loi existante. En codifiant une pratique indésirable, la directive la validerait et empêcherait sa correction. Comme McCarthy l'a dit elle-même, le but est de fournir une %(q:certitude légale) aux possesseurs de brevets, pour leur assurer une %(q:position légale), et par là grandement améliorer la valeur commerciale de cette %(q:position légale), qui est pour le moment souvent considérée comme invalide par les partenaires négociateurs des avocats en brevets des grands groupes, qui sont derrière cette directive.

#tAa: Les amendements du Parlement, en proposant une interprétation plus claire et plus restrictive de la loi sur la brevetabilité des inventions mises en oeuvre sur ordinateur %(q:ne légalisent donc pas les brevets), mais tentent de garantir que seules de réelles inventions jouissent de brevets dans le futur. Une loi de l'UE ouvre également la voie vers des appels à la Cours européenne de justices pour mettre en cause de mauvaises décisions sur les brevets, de manière transparente et responsable. Ils permettent donc la création d'une jurisprudence européenne mettant en oeuvre les exigences du Parlement sur l'exclusion de la brevetabilité des méthodes organisationnelles.

#sin: Comme nous l'avons vu, il n'y a absolument rien dans la proposition de McCarthy qui puisse permettre de distinguer les %(q:réelles inventions) d'autres moins réelles.

#eaW: On ne devrait pas confondre les amendements du JURI avec les %(q:amendements du Parlement). Les comités pour la Culture et l'Industrie ont en effet proposé des amendements utiles. Mais le Comité des affaires juridiques (JURI) de McCarthy a, comme montré ci-dessus, rejeté toutes les propositions d'amendements qui auraient pu limiter la brevetabilité. Les nouveaux amendements pour lesquels le JURI a voté sur la recommendation de McCarthy ne peuvent que favoriser le renforcement de la position légale des détenteurs de brevets triviaux et à large portée sur des algorithmes et des méthodes organisationnelles mis en oeuvre sur ordinateur.

#aap: La Cours européenne de justice (CEJ) et d'autre organes de l'UE ne sont pas nécessairement plus %(q:transparents et reseponsable) que l'OEB. La proposition de directive ne contribuerait que  peu à leur faire prendre en charge le processus législatif. La CEJ a déjà la charge de la non-harmonisation du marché intérieur, y compris en ce qui concerne la loi des brevets basée sur la CEB, et la directive rendrait la CEJ responsable de l'application de la loi sur les brevets seulement en ce qui concerne les problèmes d'harmonisation sur le marché intérieur.

#Wai: Le travail du Parlement est de définir les règles que l'OEB devrait appliquer, et non le contraire. Le devoir du Parlement n'est pas de codifier ce qui pourrait être discutablement qualifié de %(q:status quo) d'un groupe administratif ou juridique, mais de fixer des règles qui %(ep:promeuvent l'innovation, la compétition et la croissance économique, dans l'intérêt des citoyens de l'Europe).

#Jie: Voir la déclaration de la nouvelle mission de l'OEB. A en juger par les déclarations de l'OEB à la conférence de l'OCDE du mois d'août, l'OEB, bien que n'étant pas un corps démocratiquement élu, semble avoir pris sa mission plus à coeur que le rapporteur du Parlement européen.

#uns: L'%(q:harmonisation) de la jurisprudence au-delà des frontières nationales sera accomplie par toute nouvelle loi de l'UE, qu'elle promeuve l'innovation ou non. Seule une loi qui réprime l'innovation et la productivité, comme la directive de l'UE sur le brevet logiciel amendée par Arlene McCarthy, a besoin de justificatifs tels que %(q:l'harmonisation de la jurisprudence) et le %(q:status quo).

#aWl: Jacob Hallén

#r5j: Jacob Hallén, entrepreneur informatique suédois et président de %(PBF), une organisation représentant 50 PME de developpements logiciels qui a fait un discours lors de la séance parlementaire de mai 2003, fait le commentaire suivant à propos de l'%(q:ensemble de faits) de McCarthy:

#sIe: Si Mme McCarthy s'intéresse réellement au résultat qu'elle affirme vouloir dans son %(q:ensemble de faits), je ne vois pas pourquoi elle s'oppose aux %(ap:amendements proposés par la FFII). Les énoncés de la FFII ne feraient que renforcer la loi et en réduiraient les possibilités d'interprétation. Ils ne s'éloignent aucunement des buts exprimés par Mme McCarthy.

#eto: Ceci mène à l'une des trois conclusions ci-après:

#yWo: Mme McCarthy considère comme une perte de prestige que ses propositions soient amendées.

#Weg: Mme McCarthy ne comprend pas que les amendements proposés renforcent ses buts.

#nWf: Mme McCarthy comprend que les amendements proposés renforcent ses buts déclarés. Cependant ses buts déclarés diffèrent de ses buts véritables.

#ehi: Je craint qu'aucune de ces suppositions ne présente Mme McCarthy sous un jour très favorable, mais certaines sont pires que d'autres.

#ter: FT McCarthy 2003/09/03: Soyons clairs sur ce que nous protégeons:

#tnW: Le Financial Times publie une lettre à l'éditeur dans laquelle McCarthy dit (1) qu'elle ne plaide pas la cause des brevets logiciels (2) que les petites et moyennes entreprises de logiciels ont besoin de brevets et doivent être aidées au moyen d'une %(q:assurance de défense de brevet) pour défendre ces brevets face aux gros poissons.

#rnm: PDF original, converti à partir de MSWord

#mxF: Lettre de %(mh:Malcolm Harbour) et %(jw:Joachim Wuermeling) au Financial Times.

#gcr: Accuse aussi des %(q:lobbyists opensource) ou la %(q:désinformation) à propos de leurs bonnes intentions. Bien que la FFII ne soit pas une organisation de %(q:lobbyistes opensource), nous avons en effet rapporté les actes de Harbour et Wuermeling plutôt que leurs declarations d'intentions.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amccarthy030901 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

