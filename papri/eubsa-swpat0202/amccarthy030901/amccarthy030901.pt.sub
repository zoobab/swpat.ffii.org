\begin{subdocument}{amccarthy030901}{Arlene McCarthy 2003/09/01: ``Os Mitos - A Verdade''}{http://swpat.ffii.org/papri/eubsa-swpat0202/amccarthy030901/amccarthy030901.pt.html}{Workgroup\\swpatag@ffii.org\\vers\~{a}o inglesa 2003/09/04 por Rui Miguel Seabra\footnote{http://www.ansol.org/}}{Em resposta \`{a} onda de protestos contra a proposta de directiva sobre patentes de software COM(2002)92 2002/0047 de finais de Agosto de 2003, a relatora do Parlamento Europeu para esta directiva, a Euro-deputada Arlene McCarthy, publicou uma ``Lista de Factos'' que tenta explicar como tem sido uma v\'{\i}tima de uma ``campanha de desinforma\c{c}\~{a}o'' e que na realidade defende a causa dos protestantes. Aqui republicamos o documento de forma comentada.}
\begin{sect}{text}{O Texto}
\begin{sect}{title}{Desinforma\c{c}\~{a}o sobre Desinformadores?}
O t\'{\i}tulo do texto acusa uma organiza\c{c}\~{a}o desconhecida de fazer uma ``campanha de desinforma\c{c}\~{a}o'':

\begin{quote}
{\it \begin{center}
FACTSHEET - EU rules for patents for computer-implemented inventions

The Misinformation Campaign: Claims by the Free Software Alliance
\end{center}}
\end{quote}

Assumimos que Arlene McCarthy se refere ou \`{a} FFII (Funda\c{c}\~{a}o para uma Infra-estrutura de Informa\c{c}\~{a}o Livre) ou \`{a} Alian\c{c}a Eurolinux. Ambas as organiza\c{c}\~{o}es s\~{a}o suportadas por centenas de companhias de software que produzem software n\~{a}o-livre.

\begin{quote}
{\it Os Mitos - A Verdade}
\end{quote}

No que se segue, McCarthy tenta refutar v\'{a}rios ``mitos''. N\~{a}o temos a certeza de quem ser\~{a}o os autores dos ``mitos'', porque McCarthy n\~{a}o cita fontes. Contudo, \'{e} evidente que a maioria dos ``mitos'' tem a sua origem no convite da FFII para os eventos de 2003/08/27\footnote{http://swpat.ffii.org/xatra/meps038/swxmeps038.en.html}, que foi enviado aos membros do Parlamento Europeu por email e fax.
\end{sect}

\begin{sect}{swpat}{``Inven\c{c}\~{a}o Implementada num Computador'' n\~{a}o Software?}
\begin{quote}
{\it Mito: Isto \'{e} uma proposta para uma directiva de patentes de software

Verdade: A proposta \'{e} para uma patente abranger inven\c{c}\~{o}es implementadas num computadores.: Na lei, o software como tal, n\~{a}o \'{e} patente\'{a}vel. Tal como uma inven\c{c}\~{a}o no mundo f\'{\i}sico pode aproveitar os direitos de patentes, tamb\'{e}m o pode fazer uma inven\c{c}\~{a}o que dependa de um programa de computador.}
\end{quote}

Na Comiss\~{a}o Europeia, esta proposta de directiva \'{e} coloquialmente referida como ``a proposta de patentes de software'' ou ``proposta softpat''. David Ellard, uma das pessoas respons\'{a}veis pela Unidade de Propriedade Industrial, referiu-se \`{a} proposta como a ``directiva de patenteabilidade do software'' tanto no programa como na sua ora\c{c}\~{a}o na confer\^{e}ncia OECD em Paris, a 28 e 29 de Agosto.

O termo ``inven\c{c}\~{a}o implementada num computador'' n\~{a}o \'{e} utilizado pelos profissionais da inform\'{a}tica. De facto, nem sequer \'{e} vastamente utilizado . Foi introduzido pelo Gabinete Europeu de Patentes (EPO) em Maio de 2000 no Ap\^{e}ndice 6\footnote{http://swpat.ffii.org/papri/epo-tws-app6/epo-tws-app6.en.html} da Confer\^{e}ncia Trilateral, onde serviu para legitimar patentes de planos de neg\'{o}cio, trazendo assim a pr\'{a}tica do EPO \`{a} linha do modelo dos EUA e Jap\~{a}o. Muito da proposta de directiva da Comiss\~{a}o Europeia \'{e} baseado em termos e express\~{o}es deste ``Ap\^{e}ndice 6''.

De acordo com a defini\c{c}\~{a}o original do EPO a 2000, que \'{e} utilizada pela Comiss\~{a}o Europeia, a express\~{a}o ``inven\c{c}\~{o}es implementadas num computador'' n\~{a}o se refere a mais nada para al\'{e}m de software no contexto das reivindica\c{c}\~{o}es de patentes. Dessa forma, o sistema anti-bloqueio (ABS) pode n\~{a}o se qualificar como uma inven\c{c}\~{a}o implementada num computador, porque as ``capacidades prima facie novas'' n\~{a}o residem no sistema de processamento de dados mas no modo em como a fric\c{c}\~{a}o \'{e} aplicada nos pneus atrav\'{e}s dos trav\~{o}es dos autom\'{o}veis. Assim o ABS n\~{a}o seria uma ``inven\c{c}\~{a}o implementada num computador'' mas uma ``inven\c{c}\~{a}o implementada em trav\~{o}es''.

A pr\'{o}pria Arlene McCarthy utiliza a express\~{a}o ``inven\c{c}\~{a}o implementada num computador'' como sin\'{o}nimo para ``inova\c{c}\~{a}o em software'' na ``Lista de Factos'' (ver abaixo), e com raz\~{a}o.

At\'{e} 1986 as inova\c{c}\~{o}es no software n\~{a}o eram consideradas inven\c{c}\~{o}es patente\'{a}veis pelo Gabinete Europeu de Patentes. Hoje em dia, alguns tribunais nacionais continuam a rejeit\'{a}-las como n\~{a}o patente\'{a}veis. Exemplos de jurisprud\^{e}ncia que aplicam o Artigo 52\texmath{^o} da Conven\c{c}\~{a}o Europeia da Patentes (EPC) podem ser revistos em

\begin{itemize}
\item
EPO decision T 22/85 against IBM archival system\footnote{http://swpat.ffii.org/papri/epo-t850022/epo-t850022.en.html}

\item
BPatG 2002-03-26: Suche fehlerhafter Zeichenketten\footnote{http://swpat.ffii.org/papri/bpatg17-suche02/bpatg17-suche02.de.html}
\end{itemize}

Estas duas patentes teriam de ser concedidas no futuro coberto pela directiva que Arlene McCarthy orienta.
\end{sect}

\begin{sect}{tsolv}{``Solu\c{c}\~{a}o T\'{e}cnica'': Confiando num Requisito Abolido}
\begin{quote}
{\it Isto n\~{a}o \'{e} novidade. O Gabinete Europeu de Patentes e os gabinetes de patentes nacionais tem estado a conceder patentes para inven\c{c}\~{o}es implementadas num computador. A proposta aplica-se estritamente a inven\c{c}\~{o}es que t\^{e}m de satisfazer as condi\c{c}\~{o}es de qualquer inven\c{c}\~{a}o; t\^{e}m de ser uma novidade, envolver um passo inventivo e t\^{e}m de dar uma contribui\c{c}\~{a}o t\'{e}cnica providenciando uma solu\c{c}\~{a}o t\'{e}cnica para um problema.}
\end{quote}

Em lado nenhuma na proposta de McCarthy existe um requisito de existir ``uma solu\c{c}\~{a}o t\'{e}cnica para um problema''. Pelo contr\'{a}rio, o Considerando 12 tal como emendado por Arlene McCarthy garante que a exist\^{e}ncia de um ``problema t\'{e}cnica'' \'{e} suficiente, cuidadosamente negando a aproxima\c{c}\~{a}o anterior do EPO, que exigia uma ``solu\c{c}\~{a}o t\'{e}cnica'' tamb\'{e}m:

\begin{quote}
Recital 12

Deste modo, embora os inventos que implicam programas de computador perten\c{c}am, pela sua pr\'{o}pria natureza, a um determinado dom\'{\i}nio da tecnologia, \'{e} importante deixar claro que, se um invento n\~{a}o der um contributo t\'{e}cnico para o progresso tecnol\'{o}gico, como aconteceria, por exemplo, se o seu contributo espec\'{\i}fico n\~{a}o tivesse car\'{a}cter t\'{e}cnico, o invento n\~{a}o apresentar\'{a} uma actividade inventiva, pelo que n\~{a}o ser\'{a} patente\'{a}vel.

Quando se pretende saber se se trata de uma actividade inventiva ou n\~{a}o, \'{e} habitual recorrer \`{a} aproxima\c{c}\~{a}o problema e solu\c{c}\~{a}o de forma a estabelecer que h\'{a} um problema t\'{e}cnico a ser resolvido. Se n\~{a}o estiver presente nenhum problema t\'{e}cnico, ent\~{a}o a inven\c{c}\~{a}o n\~{a}o pode ser considerada como estando a contribuir para o progresso tecnol\'{o}gico.
\end{quote}

Como bem explica o Tribunal Federal de Patentes na sua decis\~{a}o de 2002 sobre Pesquisa de Erros:

\begin{quote}
Se a solu\c{c}\~{a}o de um problema t\'{e}cnico pudesse ser uma raz\~{a}o suficiente para atribuir um car\'{a}cter t\'{e}cnico a uma implementa\c{c}\~{a}o num computador, ent\~{a}o toda a implementa\c{c}\~{a}o de um m\'{e}todo n\~{a}o t\'{e}cnico teria de ser patente\'{a}vel; contudo isto iria contra a conclus\~{a}o do Tribunal Federal de Justi\c{c}a de que a exclus\~{a}o legal da patenteabilidade dos programas de computador n\~{a}o nos permite adoptar uma aproxima\c{c}\~{a}o que tornaria patente\'{a}vel qualquer ensinamento que seja enquadrado em instru\c{c}\~{o}es orientadas ao computador.
\end{quote}

Apontamos isto a McCarthy assim que o rascunho do seu relat\'{o}rio saiu:

ver McCarthy 2003-02-19: Amended Software Patent Directive Proposal\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/amccarthy0302/amccarthy0302.en.html}

McCarthy, contudo, insiste nas suas emendas. Parece saber que um requisito de ``solu\c{c}\~{a}o t\'{e}cnica'' seria melhor do que um de ``problema t\'{e}cnico'', e que a sua plateia quer ouvir que tal requisito existe na proposta de directiva. Mas n\~{a}o existe, gra\c{c}as aos seus pr\'{o}prios esfor\c{c}os.
\end{sect}

\begin{sect}{amazon}{Caso de Teste do One Click da Amazon: atingido ``o Objectivo do Parlamento''?}
\begin{quote}
{\it Mito: A proposta ir\'{a} impor patenteabilidade ilimitada de algoritmos e planos de

Verdade: Na realidade o objectivo do Parlamento \'{e} parar o desvio do EPO e dos gabinetes de patentes nacionais de patentearem planos de neg\'{o}cio. A proposta do Parlamento \'{e} mais forte do que a lei actual e a pr\'{a}tica do EPO e exclui explicitamente a patenteabilidade de planos de neg\'{o}cio e algoritmos com a introdu\c{c}\~{a}o de um novo artigo e considerandos, nomeadamente o Artigo 4 bis e os Considerandos 13 bis e 13 quater.}
\end{quote}

Os considerandos introduzidos n\~{a}o atingem ``o objectivo do Parlamento'', como \'{e} demonstrado detalhadamente na documenta\c{c}\~{a}o \`{a} qual a nossa carta se refere:

US Gov't Promoting Patent Extremism in the European Parliament\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/eubsa-tech.en.html}

\begin{quote}
{\it O 'one-click' shopping da Amazon n\~{a}o seria patente\'{a}vel segundo os termos das propostas do parlamento.}
\end{quote}

O One Click Shopping da Amazon \'{e} sem d\'{u}vida patente\'{a}vel segundo os termos da proposta do Comit\'{e} dos Assuntos Jur\'{\i}dicos de Arlene McCarthy\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/juri0304/juri0304.en.html}, mas n\~{a}o segundo outras propostas parlamentares, tal como a do Comit\'{e} Cultural de Michel Rocard. A documenta\c{c}\~{a}o da FFII\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/eubsa-tech.en.html} explica detalhadamente porque \'{e} que isto acontece.

\begin{quote}
{\it Artigo 4 bis

Exclus\~{o}es de patenteabilidade:

Um invento que implique programas de computador n\~{a}o ser\'{a} encarado como sendo um contributo t\'{e}cnico apenas e s\'{o} por envolver a utiliza\c{c}\~{a}o de um computador, de uma rede, ou de qualquer outro aparelho suscept\'{\i}vel de programa\c{c}\~{a}o. Da mesma forma, os inventos que impliquem programas de computador destinados \`{a} execu\c{c}\~{a}o de procedimentos de natureza comercial, matem\'{a}tica ou outra, e que n\~{a}o produzam quaisquer efeitos t\'{e}cnicos, para al\'{e}m das normais interac\c{c}\~{o}es f\'{\i}sicas entre o programa e a m\'{a}quina, a rede ou qualquer outro aparelho program\'{a}vel em que possa correr, n\~{a}o ser\'{a} patente\'{a}vel.}
\end{quote}

O One Click Shopping da Amazon resolve um ``problema t\'{e}cnico'', nomeadamente o de fazer uma transac\c{c}\~{a}o com menos cliques no rato. Assim ``torna-se uma contribui\c{c}\~{a}o t\'{e}cnica'' e ``produz um efeito t\'{e}cnico para al\'{e}m das normais interac\c{c}\~{o}es f\'{\i}sicas entre um programa e o computador''.

\begin{quote}
{\it Considerando 13 bis

(13a) However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present.  Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.}
\end{quote}

A documenta\c{c}\~{a}o da FFII\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/eubsa-tech.en.html} comenta sobre isto especificamente:

\begin{quote}
O leitor inexperiente pode ver isto como uma limita\c{c}\~{a}o da patenteabilidade. O leitor experiente entende: um plano de neg\'{o}cio implementado num computador constitui uma inven\c{c}\~{a}o patente\'{a}vel, desde que seja enquadrado como uma solu\c{c}\~{a}o para um ``problema t\'{e}cnico''.
\end{quote}

No caso do One Click da Amazon, a contribui\c{c}\~{a}o para o progresso tecnol\'{o}gico consistiria na redu\c{c}\~{a}o do n\'{u}mero necess\'{a}rio de cliques no rato. Isto \'{e} sem d\'{u}vida uma ``contribui\c{c}\~{a}o t\'{e}cnica'' conforme a idiom\'{a}tica da proposta de directiva.

\begin{quote}
{\it Considerando 13 quater

Para al\'{e}m disso, um algoritmo, em si mesmo, n\~{a}o se reveste de \'{\i}ndole t\'{e}cnica, pelo que n\~{a}o pode constituir um invento de car\'{a}cter t\'{e}cnico.  N\~{a}o obstante, um m\'{e}todo que envolva o recurso a um algoritmo poder\'{a} ser patente\'{a}vel, na condi\c{c}\~{a}o de esse m\'{e}todo ser utilizado para solucionar um problema t\'{e}cnico. Uma patente concedida a um m\'{e}todo desse tipo n\~{a}o poder\'{a}, todavia, monopolizar o pr\'{o}prio algoritmo ou o respectivo uso em contextos que n\~{a}o estejam previstos na patente.}
\end{quote}

A documenta\c{c}\~{a}o da FFII\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/tech/eubsa-tech.en.html} comenta sobre isto especificamente:

\begin{quote}
Isto sugere, ao leitor casual, que os algoritmos n\~{a}o s\~{a}o patente\'{a}veis.  Para o leitor atento, diz claramente que os algoritmos podem ser patenteados simplesmente enquadrando-os como solu\c{c}\~{o}es para um problema de melhoria de efici\^{e}ncia do computador, monopolizando assim a utiliza\c{c}\~{a}o do algoritmo no computador universal em todos os contextos onde poderia ter relev\^{a}ncia pr\'{a}tica.
\end{quote}

McCarthy deveria ter explicado onde \'{e} que a documenta\c{c}\~{a}o, na qual baseamos as nossas afirma\c{c}\~{o}es, est\'{a} errada, antes de as acusar de ``mitos'' e acusando-nos de ``desinforma\c{c}\~{a}o''.
\end{sect}

\begin{sect}{prog}{Banir a Publica\c{c}\~{a}o n\~{a}o \'{e} um Problema?}
\begin{quote}
{\it Mito: Programadores e ISPs ser\~{a}o regularmente processados por infrac\c{c}\~{a}o de patentes.

Verdade: A proposta do Parlamento refor\c{c}a o direito dos programadores de computadores e investigadores de software a praticar engenharia reversa ou a atingir a interoperabilidade tal como \'{e} permitido actualmente como excep\c{c}\~{o}es da Directiva sobre o Direito de Autor de Software.}
\end{quote}

A ``verdade'' n\~{a}o tem qualquer rela\c{c}\~{a}o com o ``mito''.

A JURI introduziu, contra a recomenda\c{c}\~{a}o da Comiss\~{a}o Europeia e os outros comit\'{e}s (CULT e ITRE), reivindica\c{c}\~{o}es de programas na proposta de directiva. Isto significa que conforme a proposta da JURI, qualquer um que publique um programa na Rede, incluindo autores de p\'{a}ginas Internet e ISPs, pode ser processado por infrac\c{c}\~{a}o de patentes.

Mais uma vez isto foi documentado detalhadamente:

Program Claims: Bans on Publication of Useful Patent Descriptions\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/prog/eubsa-prog.en.html}

O problema da engenharia reversa n\~{a}o tem qualquer rela\c{c}\~{a}o com esta quest\~{a}o. N\~{a}o ajuda se os programadores tiverem permiss\~{a}o para a fazer sobre um programa ofuscado por patentes e depois os pro\'{\i}be de publicar o seu pr\'{o}prio programa, que pode ser baseado em tal engenharia reversa.
\end{sect}

\begin{sect}{devel}{Patentes de Software N\~{a}o Permesidas mas Muito Ben\'{e}ficas?}
\begin{quote}
{\it Mito: As patentes de software matam o desenvolvimento eficiente de software.

Verdade: As patentes para inven\c{c}\~{o}es implementadas num computador n\~{a}o matam companhias. Cerca de 30000 patentes j\'{a} foram concedidas nesta \'{a}rea pelo Gabinete Europeu de Patentes, enquanto que ao mesmo tempo companhias de software Opensource florescem tendo uma delas anunciado recentemente um aumento de 50\percent{} na distribui\c{c}\~{a}o mundial dos seus produtos. A proposta o parlamento d\'{a} as boas vindas ao desenvolvimento e crescimento do software open source para garantir a competi\c{c}\~{a}o no mercado e impedir o dom\'{\i}nio por um s\'{o} elemento.: A relatora do Parlamento est\'{a} a pedir \`{a} Comiss\~{a}o que monitorize o impacto desta lei tanto em PMEs como no software Opensource e que impe\c{c}a qualquer abuso do sistema de patentes no que diz respeito a inven\c{c}\~{o}es implementadas num computador. Pelo contr\'{a}rio, uma boa lei sobre inven\c{c}\~{o}es implementadas num computador ir\'{a} proteger as companhias de desenvolvimento de software e dar-lhes um retorno ao seu seu investimento atrav\'{e}s das receitas de licen\c{c}as, permitindo-lhes crescer e providenciar alternativas ao dom\'{\i}nio de companhias multinacionais, globais, no campo das inven\c{c}\~{o}es implementadas num computador.}
\end{quote}

A ``verdade'' n\~{a}o tem qualquer rela\c{c}\~{a}o com o ``mito''.

Note-se, contudo, que a pr\'{o}pria McCarthy utiliza a express\~{a}o ``inven\c{c}\~{a}o implementada num computador'' no sentido de ``software''.

As patentes que t\^{e}m sido concedidas pelo EPO contra a letra e o esp\'{\i}rito da lei vigente t\^{e}m pouco valor na Europa, porque n\~{a}o s\~{a}o (uniformemente) aplic\'{a}veis. \'{E} por causa disto que McCarthy est\'{a} a pedir uma directiva.

Muitas companhias t\^{e}m reclamado s\'{e}rios danos nos seus neg\'{o}cios nos EUA, baseados em patentes dos EUA muitas das quais com primos id\^{e}nticos nas Patentes Europeias que apenas esperam ser libertados pela directiva.

ver \begin{itemize}
\item
European Software Patent Horror Gallery\footnote{http://swpat.ffii.org/pikta/swpatpikta.de.html}

\item
ftc02-pt\footnote{http://swpat.ffii.org/papri/ftc02/ftc02.en.html}

\item
Economists Slam McCarthy Software Patent Directive Proposal\footnote{http://swpat.ffii.org/lisri/03/ekon0820/swnekon030820.en.html}

\item
Quotations on Software Patents\footnote{http://swpat.ffii.org/vreji/cusku/swpatcusku.en.html}
\end{itemize}
\end{sect}

\begin{sect}{natkort}{Tribunais Nacionais: Seguidismo Fiel?}
\begin{quote}
{\it Mito: A proposta ir\'{a} legalizar milhares de patentes de regras matem\'{a}ticas e planos de neg\'{o}cio que t\^{e}m sido concedidas pelo Gabinete Europeu de Patentes contra a letra e o esp\'{\i}rito da lei, tornando invi\'{a}vel a sua revoga\c{c}\~{a}o pelos tribunais nacionais.

Verdade: Isto \'{e} t\~{a}o confuso quanto errado. As patentes entregues pelo EPO, para inven\c{c}\~{o}es implementadas num computador, t\^{e}m sido concedidas com base numa interpreta\c{c}\~{a}o da Conven\c{c}\~{a}o Europeia de Patentes (EPC). Assim elas j\'{a} disp\~{o}es de um estatuto de legalidade e quando desafiadas por apelos em tribunal, longe de procurar revogar essas patentes, os tribunais nacionais t\^{e}m na maioria dos casos seguido fielmente a decis\~{a}o do EPO.}
\end{quote}

A nossa carta citava exemplos de tribunais nacionais e ju\'{\i}zes nacionais que n\~{a}o seguiram o EPO, Estes incluem os mais elevados tribunais alem\~{a}es, franceses e suecos e mesmo algumas \$(ep:decis\~{o}es recentes do EPO). Na maioria dos pa\'{\i}ses, os tribunais n\~{a}o decidiram sequer sobre estas mat\'{e}rias. Se Arlene McCarthy tem estat\'{\i}sticas que demonstram um seguidismo fiel da parte dos tribunais nacionais, gostar\'{\i}amos de receber uma c\'{o}pia.

At\'{e} mesmo o EPO est\'{a} a dar sinais de querer afastar-se da sua pr\'{a}tica actual. O presidente do EPO, Dr. Ingo Kober, disse a jornalistas em Novembro de 2002 que a patenteabilidade do software \'{e} uma ideia m\'{a}, e o vice-presidente, Prof. Manuel Desantes, disse no encontro OECD em 2003-08-29 em Paris\footnote{http://swpat.ffii.org/lisri/03/oecd0901/swnoecd030901.en.html}: ``Se, como as provas que ouvimos nesta confer\^{e}ncia sugerem, as patentes de software n\~{a}o est\~{a}o a contribuir para a inova\c{c}\~{a}o e o crescimento econ\'{o}mico na Europa, estamos dispostos a parar de conceder tais patentes, apesar de qualquer discuss\~{a}o a respeito de efeitos t\'{e}cnicos, que normalmente n\~{a}o levam a lado nenhum.'' O juiz-presidente do Tribunal Federal de Justi\c{c}a alem\~{a}o, Dr. Klaus-Juergen Melullis, recentemente acrescentou cr\'{\i}ticas\footnote{http://swpat.ffii.org/papri/melullis02/melullis02.de.html} \`{a} proposta de directiva, indicando que codifica uma pr\'{a}tica que \'{e} indesej\'{a}vel e incompat\'{\i}vel com a lei vigente. Codificando uma pr\'{a}tica indesej\'{a}vel, a directiva for\c{c}a-nos a essa pr\'{a}tica e impede a sua correc\c{c}\~{a}o. Como McCarthy disse, o sentido deste exerc\'{\i}cio \'{e} precisamente providenciar uma ``certeza legal'' aos detentores de patentes, para lhes garantir as suas ``posi\c{c}\~{o}es legais'' e assim ampliar enormemente o valor comercial destas ``posi\c{c}\~{o}es legais'', que s\~{a}o actualmente tratadas frequentemente como inv\'{a}lidas pelos parceiros de neg\'{o}cio dos advogados de patentes empresariais, que est\~{a}o a promover a toda a for\c{c}a esta directiva.
\end{sect}

\begin{sect}{genuin}{Apenas Inven\c{c}\~{o}es Genu\'{\i}nas?}
\begin{quote}
{\it As emendas do Parlamento, ao propor uma interpreta\c{c}\~{a}o mais restritiva e clara da lei sobre a patenteabilidade de inven\c{c}\~{o}es implementadas num computador est\'{a}, portanto, ``n\~{a}o a legalizar patentes'', mas a procurar garantir que apenas inven\c{c}\~{o}es genu\'{\i}nas possam gozar de patentes no futuro. Uma lei na UE tamb\'{e}m abre uma via para apelos ao Tribunal Europeu de Justi\c{c}a para desafiar m\'{a}s decis\~{o}es de patentes de uma forma transparente e contabiliz\'{a}vel. Assim capacita a cria\c{c}\~{a}o de jurisprud\^{e}ncia Europeia que actuando as exig\^{e}ncias do Parlamento para garantir a exclus\~{a}o da patenteabilidade de planos de neg\'{o}cio.}
\end{quote}

Como vimos n\~{a}o h\'{a} nada, seja o que for, na proposta de directiva de McCarthy que poderia ser de alguma ajuda para distinguir ``inven\c{c}\~{o}es genu\'{\i}nas'' das menos genu\'{\i}nas.

N\~{a}o se deve confundir as emendas da JURI com ``as emendas do Parlamento''. Os Comit\'{e}s para a Cultura e Ind\'{u}stria propuseram, de facto, emendas \'{u}teis. Mas o Comit\'{e} dos Assuntos Jur\'{\i}dicos de McCarthy (JURI) rejeitou, como j\'{a} vimos, todas as propostas de emendas que poderiam limitar a patenteabilidade. As novas emendas que a JURI votou segundo as recomenda\c{c}\~{o}es de McCarthy apenas servem para refor\c{c}ar a posi\c{c}\~{a}o legal dos donos de patentes triviais e abrangentes sobre algoritmos implementados num computador e planos de neg\'{o}cio.

ver JURI votes for Fake Limits on Patentability\footnote{http://swpat.ffii.org/lisri/03/juri0617/swnjuri030617.en.html}

O Tribunal Europeu de Justi\c{c}a (ECJ) e outros \'{o}rg\~{a}os da UE n\~{a}o s\~{a}o necessariamente mais ``transparentes e contabiliz\'{a}veis'' do que o EPO. A proposta de directiva tamb\'{e}m contribuiria muito pouco, se alguma coisa, para os colocar como respons\'{a}veis pelo processo legislativo. O ECJ j\'{a} \'{e} respons\'{a}vel por desarmonias no mercado interno, incluindo aquelas que dizem respeito a leis de patentes baseadas na EPC, e a directiva iria tamb\'{e}m por o ECJ como respons\'{a}vel pela lei de patentes apenas no que diz respeito a desarmonias no mercado interno.

A fun\c{c}\~{a}o do Parlamento \'{e} definir o conjunto de regras que o EPO deveria seguir, e n\~{a}o vice-versa. A fun\c{c}\~{a}o do Parlamento n\~{a}o \'{e} codificar o que pode discutivelmente ser um ``status quo'' de alguma entidade administrativa ou judicial, mas definir regras que \emph{promovam inova\c{c}\~{a}o, competi\c{c}\~{a}o e crescimento econ\'{o}mico no interessa dos cidad\~{a}os Europeus}\footnote{Ver a nova declara\c{c}\~{a}o de miss\~{a}o do EPO. A julgar pelas declara\c{c}\~{o}es do EPO na confer\^{e}ncia OECD no passado m\^{e}s de Agosto, o EPO, embora n\~{a}o seja uma entidade democraticamente eleita, parece estar a levar a sua declara\c{c}\~{a}o de miss\~{a}o mais seriamente do que a relatora do Parlamento Europeu.}.

A ``harmoniza\c{c}\~{a}o'' da jurisprud\^{e}ncia atrav\'{e}s das fronteiras nacionais ir\'{a} ocorrer como de costume atrav\'{e}s de qualquer nova lei da UE, n\~{a}o importa se promove a inova\c{c}\~{a}o ou n\~{a}o. Apenas uma lei que estagna a inova\c{c}\~{a}o e a produtividade, tal como a proposta de directiva de patentes de software da UE emendada por Arlene McCarthy, precisa de argumentos como a ``harmoniza\c{c}\~{a}o da jurisprud\^{e}ncia'' e do ``status quo'' como justifica\c{c}\~{a}o.
\end{sect}
\end{sect}

\begin{sect}{cusku}{Cita\c{c}\~{o}es}
\begin{div}{mlhtml-div}
hallen0309Jacob Hall\'{e}nJacob Hall\'{e}n, empres\'{a}rio de software sueco e presidente do Python Business Forum\footnote{http://www.python-in-business.org/}, uma organiza\c{c}\~{a}o representando 50 PMEs de software, que falou numa audi\c{c}\~{a}o parlamentar em Maio de 2003, comenta a ``Lista de Factos'' de McCarthy:Se a Sra. McCarthy est\'{a} realmente interessada no objectivo que alega querer na sua ``Lista de Factos'', ent\~{a}o eu n\~{a}o consigo ver porque \'{e} que se op\~{o}e \`{a}s emendas propostas pela FFII\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/prop/eubsa-prop.en.html}. A sua formula\c{c}\~{a}o apenas refor\c{c}a a lei e torna-a menos suscept\'{\i}vel a interpreta\c{c}\~{o}es. N\~{a}o se desvia de forma significativa dos objectivos exprimidos pela Sra. McCarthy.Isto leva a uma das seguintes conclus\~{o}es:
\begin{enumerate}
\item
A Sra. McCarthy considera uma perda de prest\'{\i}gio ter a sua proposta emendada.

\item
A Sra. McCarthy n\~{a}o entende que as emendas propostas refor\c{c}am os seus objectivos.

\item
A Sra. McCarthy entende que as emendas propostas refor\c{c}am os seus objectivos declarados. Contudo, os seus objectivos declarados s\~{a}o diferentes dos seus verdadeiros objectivos.
\end{enumerate}Receio que nenhuma destas alternativas reflicta bem sobre o car\'{a}cter da Sra. McCarthy, mas certas alternativas s\~{a}o menos m\'{a}s que outras.
\end{div}
\end{sect}

\begin{sect}{links}{Links Anotados}
\begin{itemize}
\item
{\bf {\bf PDF original, convertido do MSWord\footnote{amccarthy030901.pdf}}}
\filbreak

\item
{\bf {\bf Arlene McCarthy PR ``hits out'' against ``dishonest ... bullying ... lobbyists''\footnote{http://swpat.ffii.org/lisri/03/amcc0902/swnamcc030902.en.html}}}

\begin{quote}
In response to last week's wave of protests against her software patent directive proposal, Arlene McCarthy MEP, rapporteur of the European Parliament for the EU Software Patent Directive Proposal, has circulated a press release which ``hits out against claims made by opponents to the new EU law''.  McCarthy, speaking in the name of UK Labour and partially of the parliament as a whole, calls on fellow MEPs to ``back EU plans for patents for inventions'' later this month, warning them not to be misled by a ``dishonest and unconstructive campaign'', ``orchestrated'' by a group of ``lobbyists'', who are ``bullying'' parliamentary staff and ``putting at risk jobs in the growing software industry''.
\end{quote}
\filbreak

\item
{\bf {\bf FT McCarthy 2003/09/03: Let's clarify what we're protecting\footnote{http://search.ft.com/search/article.html?id=030903001056}}}

\begin{quote}
O Financial Times publica uma carta ao editor, na qual McCarthy diz (1) que n\~{a}o est\'{a} a promover patentes de software (2) que as pequenas e m\'{e}dias empresas de software precisam de patentes e que devem ser ajudadas atrav\'{e}s de ``seguros de defesa de patentes'' para as proteger contra grandes rufi\~{o}es.
\end{quote}
\filbreak

\item
{\bf {\bf Carta de Malcolm Harbour\footnote{http://swpat.ffii.org/gasnu/mharbour/swpatmharbour.en.html} e Joachim Wuermeling\footnote{http://swpat.ffii.org/gasnu/jwuermeling/swpatjwuermeling.en.html} ao Financial Times.\footnote{ft-harwuerm030901.txt}}}

\begin{quote}
Tamb\'{e}m acusa ``lobbyistas open source'' de ``desinforma\c{c}\~{a}o'' a respeito das suas boas inten\c{c}\~{o}es. Enquanto que a FFII n\~{a}o \'{e} uma organiza\c{c}\~{a}o de ``lobby open source'' temos, de facto, reportado acerca dos actos de Harbour e Wuermeling e n\~{a}o das suas alegadas inten\c{c}\~{o}es.
\end{quote}
\filbreak

\item
{\bf {\bf UK Gov't Promoting Patent Extremism in the European Parliament\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/ukrep0309/ukrep0309.en.html}}}

\begin{quote}
The UK Government's Foreign Office is circulating a ``briefing to UK MEPs'', in which it instructs british members of the European Parliament to back Arlene McCarthy's position and vote (1) against any attempt to define what is technical or otherwise limit what is patentable (2) against Article 6a which allows converters to be written when standards are patented (3) for JURI Art 5 which forbids publication of descriptions of patented inventions on the Net.  The intervention of the government comes at a moment where McCarthy has shown nervous reactions in view of dwindling support in her party group.  The government statement can be attributed to the UK Patent Office and its policy working group, consisting mainly of patent lawyers from large corporations.  This group has been determining the software patent policy of the UK and largely also of the EU during recent years.
\end{quote}
\filbreak

\item
{\bf {\bf McCarthy 2003/05/03: Software Patent Directive Proposal FAQ\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/amccarthy0305/amccarthy0305.en.html}}}

\begin{quote}
Arlene McCarthy, member of the European Parliament and rapporteur of the Legal Affairs Commission (JURI) on the Software Patentability Directive Proposal explains her point of view in a FAQ manner.  She asked us to distribute this document to the participants of a conference which we organised in Brussels.  In a nutshell, she says that \begin{enumerate}
\item
Software patents, as granted by the European Patent Office (EPO) at present, are needed for various reasons, e.g. protecting Europe against competition from Asia, allowing European SMEs to compete in the US, etc

\item
The EPO is granting software patents but not patents on non-technical algorithms and business methods.

\item
The current proposal is designed to ensure that the EPO's practise is followed throughout Europe in a uniform manner and that a drift toward patenting of ``non-technical algorithms and business methods'' is halted.
\end{enumerate}  We debug McCarthy's questions and answers one by one.  It seems that many of McCarthy's proclaimed objectives could be achieved only by voting in favor of a series of CULT, ITRE and JURI amendment proposals which unfortunately have not enjoyed the support of the rapporteur.
\end{quote}
\filbreak

\item
{\bf {\bf McCarthy 2003-02-19: Amended Software Patent Directive Proposal\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/amccarthy0302/amccarthy0302.en.html}}}

\begin{quote}
Arlene McCarthy, British Labor MEP appointed by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI) to report on the European Commission's Software Patentability Directive Proposal (CEC/BSA Proposal), suggests that the European Parliament should enact the CEC/BSA version with additional safeguards to align Europe on the US practise and make sure that there can be no limit on patentability.  McCarthy reiterates the CEC/BSA software patent advocacy and misrepresents the wide-spread criticism without citing any of it.  Even economic and legal expertises ordered by the European Parliament and other critical opinions of EU institutions are not taken into account.  McCarthy's economic argumentation consists of tautologies and unfounded assertions, such as that companies like Ericsson and Alcatel need software patents to finance their R\&D, that SMEs need european software patents in order to compete in the USA, that patents are needed to keep developping countries at bay.  McCarthy uses the term ``computer-implemented inventions'' as a synonym for ``software innovations''.  These ``by their very nature belong to a field of technology''.  McCarthy insists that ``irreconcilable conflicts'' with the EPO must be avoided.  McCarthy says she wants to ``set clear limits as to what is patentable'' -- and that she wants to avoid the ``sterile discussions'' about ``technical effects'' and ``exclusions from patentability''.  Yet her proposal stays confined to such discussions.  McCarthy demands that all useful ideas, including algorithms and business methods, must be patentable as ``computer-implemented inventions''.  McCarthy proposes to recognise the EPO as Europe's supreme patent legislator and to make decisions of a few influential people at the EPO irreversible and binding for all of Europe.
\end{quote}
\filbreak

\item
{\bf {\bf Arlene McCarthy MEP and Software Patents\footnote{http://swpat.ffii.org/gasnu/amccarthy/swpatamccarthy.en.html}}}

\begin{quote}
British Member of the European Parliament, Labor/PSE, appointed by the Europarl Committtee for Legal Affairs and Internal Market (JURI) in 2002/03 to report on the software patentability directive.  In June 2002 Arlene McCarthy published a short report which aggressively promoted the agenda of the European Patent Office (EPO).   The paper charged the patent critics of having provided only invalid arguments, but failed to quote or refute any of these arguments.  Meanwhile various people from the EPO and patent lobby were in contact with McCarthy and boasted that their viewpoint would prevail and the discussion would soon be over.  A hearing arranged by McCarthy and the europarl webspace dedicated to the hearing both offered minimal room for critical views.  Arlene McCarthy's draft report of 2003/02/19, her explanatory note of 2003/05/03 and her refusal to accept any amendments which limit patentability or patent enforcability in any way show complete dedication to the interests of patent owners.  While staying away from all informed discussions and conferences on software patent questions, McCarthy has actively reaching out to the media in order to present herself as a victim of a ``dishonest and destructive misinformation campaign'' who is sincerely trying to limit patentability.  McCarthy has been serving the recording industry and various projects of the Commission's Directorate for the Internal Market (Bolkestein) with equal fervor. McCarthy's political business model appears to consist in (1) aggressively and unconditionally serving of the Commission and big business (2) obtaining favorabl treatment by Commission and Big Business for her electoral region / constituency.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: latex ;
% End: ;

