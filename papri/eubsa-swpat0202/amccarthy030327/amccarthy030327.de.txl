<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#irW: Co-initiator of the Appeal

#nWu: As of 2003/07/23, this page contained the photo of Arlene McCarthy with two friends from the music industry and an explanation %(q:Arlene with Jay Berman and Sir George Martin, campaigning for Music Industry rights in the European Parliament)

#crm: Diskussionen in Foren

#3aW: phm 2003/02/27: MdEP fordern %(q:harte Strafen) für %(q:jegliche Verletzung Geistigen Eigentums)

#uie: Yours sincerely

#ats: The written declaration is available to sign this afternoon outside of the Hemicycle. The declaration needs at least 313 signatures.

#rtg: We would really welcome your personal signature on this declaration to give a strong signal to Council and Commission of Parliament's concerns at the alarming levels of piracy and counterfeiting.

#sai: to raise consumer awareness that piracy and counterfeiting are not victimless crimes.

#rmn: to promote better cross-border cooperation between law enforcement authorities in the Member States in addition to strengthening the role of Europol in combating counterfeiting and piracy;

#hri: ensure that current and forthcoming legislation provides strong, harmonised civil sanctions for any intellectual property infringement and tough criminal penalties for commercial scale counterfeiting;

#cum: We are calling on the Council and the Commission to;

#tnt: Currently there are two important pieces of legislation going through the European Parliament, the enforcement directive and consultation on customs action against counterfeit and pirated goods (repealing Regulation 3295/94/EC).

#pje: Piracy and counterfeiting have now reached alarming levels in the EU and epidemic proportions in accession countries. Recent Commission statistics showed an increase of 900 per cent in pirated goods. Not only is counterfeiting and piracy leading to an average loss of 17 000 jobs per annum in the EU and millions in lost tax revenue for governments, but counterfeit goods can pose serious health and safety risks to consumers. It is organised crime networks that are behind the pirate trade, using profits from piracy and counterfeiting to finance drug trafficking and terrorism.

#kar: We are writing to ask for your support in signing our written declaration on the fight against piracy and counterfeiting in the enlarged EU.

#descr: MdEP Arlene McCarthy bringt in einem Aufruf an Kollegen Urheberrechtsverletzungen mit Terrorismus in Verbindung.

#title: MdEP wollen %(q:harte Strafen) für %(q:jegliche Verletzungen geistigen Eigentums)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amccarthy030327 ;
# txtlang: de ;
# multlin: t ;
# End: ;

