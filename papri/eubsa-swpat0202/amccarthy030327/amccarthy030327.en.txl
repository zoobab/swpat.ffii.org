<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: McCarthy et al 2003/02/27 demanding %(q:strong sanctions)

#descr: On 2003/03/27, MEP Arlene McCarthy published an appeal which calls for signatures to a letter that links copyright infringement to terrorism and calls for strong sanctions against %(q:any infringement of intellectual property).

#kar: We are writing to ask for your support in signing our written declaration on the fight against piracy and counterfeiting in the enlarged EU.

#pje: Piracy and counterfeiting have now reached alarming levels in the EU and epidemic proportions in accession countries. Recent Commission statistics showed an increase of 900 per cent in pirated goods. Not only is counterfeiting and piracy leading to an average loss of 17 000 jobs per annum in the EU and millions in lost tax revenue for governments, but counterfeit goods can pose serious health and safety risks to consumers. It is organised crime networks that are behind the pirate trade, using profits from piracy and counterfeiting to finance drug trafficking and terrorism.

#tnt: Currently there are two important pieces of legislation going through the European Parliament, the enforcement directive and consultation on customs action against counterfeit and pirated goods (repealing Regulation 3295/94/EC).

#cum: We are calling on the Council and the Commission to;

#hri: ensure that current and forthcoming legislation provides strong, harmonised civil sanctions for any intellectual property infringement and tough criminal penalties for commercial scale counterfeiting;

#rmn: to promote better cross-border cooperation between law enforcement authorities in the Member States in addition to strengthening the role of Europol in combating counterfeiting and piracy;

#sai: to raise consumer awareness that piracy and counterfeiting are not victimless crimes.

#rtg: We would really welcome your personal signature on this declaration to give a strong signal to Council and Commission of Parliament's concerns at the alarming levels of piracy and counterfeiting.

#ats: The written declaration is available to sign this afternoon outside of the Hemicycle. The declaration needs at least 313 signatures.

#uie: Yours sincerely

#3aW: phm 2003/02/27: MEPs demanding %(q:strong sanctions)

#crm: Mailing List dicsussions

#nWu: As of 2003/07/23, this page contained the photo of Arlene McCarthy with two friends from the music industry and an explanation %(q:Arlene with Jay Berman and Sir George Martin, campaigning for Music Industry rights in the European Parliament)

#irW: Co-initiator of the Appeal

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: amccarthy030327 ;
# txtlang: en ;
# multlin: t ;
# End: ;

