# -*- mode: makefile -*-

dkpto0209.en.lstex:
	lstex dkpto0209.en | sort -u > dkpto0209.en.lstex
dkpto0209.en.mk:	dkpto0209.en.lstex
	vcat /ul/prg/RC/texmake > dkpto0209.en.mk


dkpto0209.en.dvi:	dkpto0209.en.mk
	rm -f dkpto0209.en.lta
	if latex dkpto0209.en;then test -f dkpto0209.en.lta && latex dkpto0209.en;while tail -n 20 dkpto0209.en.log | grep -w references && latex dkpto0209.en;do eval;done;fi
	if test -r dkpto0209.en.idx;then makeindex dkpto0209.en && latex dkpto0209.en;fi

dkpto0209.en.pdf:	dkpto0209.en.ps
	if grep -w '\(CJK\|epsfig\)' dkpto0209.en.tex;then ps2pdf dkpto0209.en.ps;else rm -f dkpto0209.en.lta;if pdflatex dkpto0209.en;then test -f dkpto0209.en.lta && pdflatex dkpto0209.en;while tail -n 20 dkpto0209.en.log | grep -w references && pdflatex dkpto0209.en;do eval;done;fi;fi
	if test -r dkpto0209.en.idx;then makeindex dkpto0209.en && pdflatex dkpto0209.en;fi
dkpto0209.en.tty:	dkpto0209.en.dvi
	dvi2tty -q dkpto0209.en > dkpto0209.en.tty
dkpto0209.en.ps:	dkpto0209.en.dvi	
	dvips  dkpto0209.en
dkpto0209.en.001:	dkpto0209.en.dvi
	rm -f dkpto0209.en.[0-9][0-9][0-9]
	dvips -i -S 1  dkpto0209.en
dkpto0209.en.pbm:	dkpto0209.en.ps
	pstopbm dkpto0209.en.ps
dkpto0209.en.gif:	dkpto0209.en.ps
	pstogif dkpto0209.en.ps
dkpto0209.en.eps:	dkpto0209.en.dvi
	dvips -E -f dkpto0209.en > dkpto0209.en.eps
dkpto0209.en-01.g3n:	dkpto0209.en.ps
	ps2fax n dkpto0209.en.ps
dkpto0209.en-01.g3f:	dkpto0209.en.ps
	ps2fax f dkpto0209.en.ps
dkpto0209.en.ps.gz:	dkpto0209.en.ps
	gzip < dkpto0209.en.ps > dkpto0209.en.ps.gz
dkpto0209.en.ps.gz.uue:	dkpto0209.en.ps.gz
	uuencode dkpto0209.en.ps.gz dkpto0209.en.ps.gz > dkpto0209.en.ps.gz.uue
dkpto0209.en.faxsnd:	dkpto0209.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo dkpto0209.en-??.g3n`;source faxsnd main
dkpto0209.en_tex.ps:	
	cat dkpto0209.en.tex | splitlong | coco | m2ps > dkpto0209.en_tex.ps
dkpto0209.en_tex.ps.gz:	dkpto0209.en_tex.ps
	gzip < dkpto0209.en_tex.ps > dkpto0209.en_tex.ps.gz
dkpto0209.en-01.pgm:
	cat dkpto0209.en.ps | gs -q -sDEVICE=pgm -sOutputFile=dkpto0209.en-%02d.pgm -
dkpto0209.en/dkpto0209.en.html:	dkpto0209.en.dvi
	rm -fR dkpto0209.en;latex2html dkpto0209.en.tex
xview:	dkpto0209.en.dvi
	xdvi -s 8 dkpto0209.en &
tview:	dkpto0209.en.tty
	browse dkpto0209.en.tty 
gview:	dkpto0209.en.ps
	ghostview  dkpto0209.en.ps &
print:	dkpto0209.en.ps
	lpr -s -h dkpto0209.en.ps 
sprint:	dkpto0209.en.001
	for F in dkpto0209.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	dkpto0209.en.faxsnd
	faxsndjob view dkpto0209.en &
fsend:	dkpto0209.en.faxsnd
	faxsndjob jobs dkpto0209.en
viewgif:	dkpto0209.en.gif
	xv dkpto0209.en.gif &
viewpbm:	dkpto0209.en.pbm
	xv dkpto0209.en-??.pbm &
vieweps:	dkpto0209.en.eps
	ghostview dkpto0209.en.eps &	
clean:	dkpto0209.en.ps
	rm -f  dkpto0209.en-*.tex dkpto0209.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} dkpto0209.en-??.* dkpto0209.en_tex.* dkpto0209.en*~
dkpto0209.en.tgz:	clean
	set +f;LSFILES=`cat dkpto0209.en.ls???`;FILES=`ls dkpto0209.en.* $$LSFILES | sort -u`;tar czvf dkpto0209.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
