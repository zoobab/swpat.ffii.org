<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#Hae: Header Text

#TrA: Tabular Comparison: CEC/BSA vs CEU/DKPTO

#Rlt: Recitals 13 to 13c

#Aaw: A defined procedure or sequence of actions when performed in the
context of an apparatus such as a computer may make a technical
contribution to the state of the art and thereby constitute a
patentable invention. However, an algorithm which is defined without
reference to a physical environment is inherently non-technical and
cannot therefore constitute a patentable invention.

#Axn: A defined procedure or sequence of actions when performed in the
context of an apparatus such as a computer may make a technical
contribution to the state of the art and thereby constitute a
patentable invention.

#HWr: However, the mere implementation of an otherwise unpatentable method
on an apparatus such as a computer is not in itself sufficient to
warrant a finding that a technical contribution is present. Thus, a
computer-implemented business or other method in which the only
contribution to the state of the art is non-technical cannot
constitute a patentable invention.

#Mle: Moreover, if the contribution to the state of the art relates solely
to unpatentable matter, then there can be no patentable invention
irrespective of how that matter is presented in the claims.

#Fea: Furthermore, an algorithm which is defined without reference to a
physical environment is inherently non-technical and cannot therefore
constitute a patentable invention.

#Dit: Definitions

#FWW: For the purposes of this Directive the following definitions shall
apply:

#cWe: %(q:computer-implemented invention) means any invention the
performance of which involves the use of a computer, computer network
or other programmable apparatus and having one or more prima facie
novel features which are realised wholly or partly by means of a
computer program or computer programs;

#cWi: %(q:technical contribution) means a contribution to the state of the
art in a technical field which is not obvious to a person skilled in
the art.

#FWW2: For the purposes of this Directive the following definitions shall
apply:

#cWe2: %(q:computer-implemented invention) means any invention the
performance of which involves the use of a computer, computer network
or other programmable apparatus, the invention having one or more
features which are realised wholly or partly by means of a computer
program or computer programs;

#Tsv: This still means that pure computer programs, i.e. ideas consisting
exclusively of calculation rules and elements of the universal
computer, must be considered to be patentable inventions.  The crucial
question, whether a calculation rule defined in terms of the universal
computer is an invention, is answered only indirectly.  Yes,
calculation rules are, according to the DKPTO proposal just as
according to other EPO-dominated proposals, patentable inventions.  By
lumping logical and physical innovation together into the
propagandistic term %(q:computer-implemented invention) and
surrounding it with unintelligible pseudo-limitations, the DKPTO, like
other friends of the EPO, is trying to blur borders and assure that
patent offices can do whatever they like.

#cWt: %(q:technical contribution) means a contribution to the state of the
art in a field of technology which is [new and] not obvious to a
person skilled in the art. The technical contribution shall be
assessed by consideration of the difference between the state of the
art and the scope of the patent claim considered as a whole, which
must comprise technical features, irrespective of whether or not these
are accompanied by non-technical features.

#Wro: What must comprise technical features?  The %(q:difference) or the
%(q:claim as a whole)?  If the former is meant, untechnical ideas are
not patentable.  If the latter is meant, anything is patentable. 
Unfortunately the latter meaning seems to be intended -- and
reinforced by the appositive %(q:as a whole).

#CeW: Computer-implemented inventions as a field of technology

#Mue: Member States shall ensure that a computer-implemented invention is
considered to belong to a field of technology.

#Dlt: Deleted

#sop: substance incorporated in paragraph 4(1)

#Cse: Conditions for patentability

#ItW2: In order to involve an inventive step, a computer-implemented
invention must make a technical contribution. A %(q:technical
contribution) is a contribution to the state of the art in a technical
field which is not obvious to a person skilled in the art.

#Tnc: The technical contribution must be assessed by consideration of the
difference between the scope of the patent claim considered as a
whole, which may comprise both technical and non-technical features,
and the state of the art.

#MvI: Member States shall ensure that a computer-implemented invention is
considered to belong to a field of technology. However, in order to be
patentable it must be new, involve an inventive step and be
susceptible of industrial application. It is a condition for involving
an inventive step that a computer-implemented invention  makes a
technical contribution.

#Tea: The DKPTO is merely moving text around without making it any better: 
truth of statements such as %(q:X belongs to a field of Y) is
independent of legislative decisions.  The question of non-obviousness
(inventive step) again is independent of the question of technical
character.  An invention can be technical, non-obviousness cannot, no
matter how hard member states try.  Notwithstanding the DKPTO's
attempts at changing fundamental truths by means of legislation: if
computing (informatics/programming) is a %(q:field of technology)
under Art 27 %(tr:TRIPs), then it will become difficult to refuse any
software patent on other grounds than obviousness or lack of novelty. 
And this is precisely what the DKPTO's legalese junktalk is likely to
boil down to in practise: technicity is inherent in non-obviousness
and therefore no longer available as an independent means of
determining whether an invention is present.

#Aac: A computer-implemented invention shall not be regarded as making a
technical contribution merely because it involves the use of a
computer, network or other programmable apparatus. Accordingly,
computer programs which implement business, mathematical or other
methods having no technical character, and which do not produce any
technical effects beyond the normal physical interactions between the
program and the computer, network, or other apparatus in which it is
run, shall not be patentable.

#Trh: This could be seen as requiring that some peripheral hardware that
goes beyond the universal computer, such as e.g. equipment for an
industrial production process, must be present.  Unfortunately this is
not made clear, and any patent attorney who is unable to construct a
%(q:technical effect) according to the DKPTO proposal's terms will not
be worth his money.

#FWc: Form of Claims

#Msc: Member States shall ensure that a computer-implemented invention may
be claimed as a product, that is as a programmed computer, a
programmed computer network or other programmed apparatus, or as a
process carried out by such a computer, computer network or apparatus
through the execution of software.

#Msc2: Member States shall ensure that a computer-implemented invention may
be claimed as a product, that is as a programmed computer, a
programmed computer network or other programmed apparatus, or as a
process carried out by such a computer, computer network or apparatus
through the execution of software.

#Apa: A claim to a computer program, either on its own or on a carrier,
shall not be valid unless that program would, when loaded in a
computer, programmed computer network or other programmable apparatus,
implement a valid patent claim relating to the same application in
accordance with paragraph IV. [ The Italian Delegation has suggested
adding the following sentence: Nevertheless, patent protection for a
computer-implemented invention does not extend to the expression of a
computer program based on that invention, in source code or object
code or in any other form. ]

#IlW: If the computer program really did implement a technical invention, it
would be sufficient to claim the technical process which involves some
novel peripheral hardware or novel physical causality.  By allowing
direct program claims, the DKPTO makes it clear that it did not mean
paragraph IV as a real restriction on what can be patented.  The
Italian addition is likely to be interpreted as a tautology without
practical effect, but it shows that even within the Brussels round of
patent administrators some people are concerned about the CEU/DKPTO's
assault on freedom of expression.

#RWi: Relationship with Directive 91/250 CEC

#Aod2: Acts permitted under Directive 91/250/EEC on the legal protection of
computer programs by copyright, in particular provisions thereof
relating to decompilation and interoperability, or the provisions
concerning semiconductor topographies or trade marks, shall not be
affected through the protection granted by patents for inventions
within the scope of this Directive.

#Txp: The rights conferred by patents granted for inventions within the
scope of this Directive shall not extend to acts permitted under
Directive 91/250/EEC on the legal protection of computer programs by
copyright, in particular under the provisions thereof in respect of
decompilation and interoperability.

#TiW: This makes the article clearer and thereby somewhat stronger.  Still
it stops short of creating a meaningful interoperability exemption as
proposed by the %(p6:FFII counter-proposal's version of this article).

#Mir: Monitoring

#Tnt: The Commission shall monitor the impact of computer-implemented
inventions on innovation and competition, both within Europe and
internationally, and on European businesses, including electronic
commerce.

#Tta: The Commission shall monitor the impact of the protection by patents
of computer-implemented inventions on innovation and competition, both
within Europe and internationally, and on European businesses,
including electronic commerce.

#PWW: PDF version of the CEU/DKPTO's document

#Tss: The original was distributed as a native MSWord file.

#Trg: This page contains detailed accounts about the positions of the German
ministery of justice (BMJ) during the CEU IntProp Working Party
meetings.  The BMJ representative asked exactly for what the DKPTO
proposal implements, thereby acting against the explicit will of those
politicians in the red-green coalition government who are responsible
for IT policy.  Due to the strengthening of the green party and the
demise of a notoriously intransigent minister of justice, there is
currently a certain chance that things may change for the better.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: dkpto0209 ;
# txtlang: en ;
# multlin: t ;
# End: ;

