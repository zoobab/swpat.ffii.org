To: joerg.tauss@bundestag.de, goetz.stumpfeld@gruene-fraktion.de, ulrich.kelber@bundestag.de, grietje.bettin@bundestag.de
Cc: patinfo@ffii.org
Subject: Auswege aus der Welp-Position
--text follows this line--
S.g. Damen und Herren,

Von einigen Ministerialbeamten hörten wir, dass die Dänische
Ratspräsidentschaft es geschafft habe, die Bundesregierung in Sachen
Logikpatente/Textpatente weitgehend auf untragbare Positionen
festzulegen, die unter

	     http://swpat.ffii.org/papiere/eubsa-swpat0202/dkpto0209/

analysiert werden und sich auf folgendes reduzieren lassen:

	   - Wahrscheinlichkeit, dass Geschäftsmethoden patentiert
             werden, wird von 99,9% auf 99,8% reduziert.
           - Die von der Eur. Kommission abgelehnten Textpatente 
	     (Anspruch auf "Programm, dadurch gekennzeichnet dass
             ...") werden legalisiert, Patente greifen direkt in die
	     Publikationsfreiheit gemäß Art 10 EMRK / Art 5 GG ein.

Die Ministerialbeamten sind mit ihrer Festlegung nicht besonders
glücklich und suchen nun Gründe für einen Rückzieher.

In Frage käme m.E. vor allem der Vorbehalt, dass der Bundestag sich
noch mit der Sache befassen muss.
            
Folgende Positionen stoßen im Europa-Parlament auf viel Zustimmung und
werden vielleicht im Januar durchgehen.  Entscheidende Politiker haben
uns ähnliches zugesagt, aber darauf alleine wollen wir uns nicht
verlassen.

Der Bundestag könnte sehr einfache Grundsätze beschließen:

 - JA zur Patentierung programmierter Werkzeugmaschinen als ganzer, 
   NEIN zur Patentierung der Programmlogik.

 - JA zum Vorschlag der Eur. Kommission hinsichtlich Anspruchsformen,
   NEIN zu Ansprüchen auf "Programmprodukt", "Programm",
   "Textstruktur" etc, wie sie der Rat zusätzlich fordert.
   Begründung: 

   (1) direkter Eingriff in Publikationsfreiheit nicht
   durch Patente sondern nur durch Urheberrecht zu rechtfertigen 

   (2) Verletzung patentrechtlicher Systematik: ein Text ist kein
   technisches Erzeugnis oder Verfahren sondern allenfalls eine
   Beschreibung eines Verfahrens, vgl Patentschrift.  

   (3) Textansprüche werden typischerweise dort begehrt, wo die Neuheit
   des Verfahrens im Bereich der Programmlogik liegt, die aber gerade
   nicht patentierbar sein soll.

   (4) s. BPatG-Beschluss "Speichermedium" von 2000.
       http://swpat.ffii.org/papiere/bpatg17-suche00/

 - JA zur Grundidee des Eur. Rates, die Forderung nach einem
   Technischen Beitrag zu präzisieren.  NEIN zu den immer noch unklaren
   Definitionen, die der Rat anbietet (vgl auch ZVEI-Schreiben an das BMJ).  
   
   Der Schaffung eines unbestimmten Rechtsbegriffes zur Übertragung
   von Regelsetzungskompetenzen an die Judikative kann nicht
   zugestimmt werden (Prinzip der Gewaltenteilung, Verbot von
   belastenden [Grundrechts-]Eingriffen aufgrund unbestimmter
   Rechtsbegriffe, Rechtsstaatsprinzip, ...)

 - Nicht nur die Artikel des RiLi-Vorschlages sondern auch die
   zielsetzende Präambel und das Vorwort müssen geändert werden.  Ziel
   der Richtlinie darf nicht die Festschreibung eines wie auch immer
   gearteten rechtlichen Status Quo sondern nur die Schaffung einer
   klaren und wirklich innovationsfördernden Regelung für die Zukunft
   sein.  In die Überlegungen sind auch Freihaltungsbedürfnisse und
   Grundrechte der Informationsgesellschaft einzubeziehen. 

   Weitere Mängel der Präambel s 

	   http://swpat.ffii.org/papiere/eubsa-swpat0202/

   und Stellungnahme der Französischen Regierung von 2002-03-01.

Sicherlich müsste man bald einmal mit Frau Zypries sprechen.  In
diesem Falle wäre ich dankbar, wenn Sie ihr ein gedrucketes Exemplar
unserer Dokumentation, wie es sich jetzt unter

    http://swpat.ffii.org/treffen/2002/europarl11/
    http://swpat.ffii.org/treffen/2002/europarl11/cfa-de.pdf

findet, übergeben könnten.

Wir würden gerne einige Tage in Berlin verbringen, um die Sache
voranzubringen und mit ihrer Unterstützung in Bundestagskreisen
systematisch vorzusprechen.  Beim Europarl hatten wir hiermit Erfolg
und arbeiten auch weiter daran.  Leider schaffen wir es nicht, am
4. Dezember in Berlin einen "Parlamentarischen Abend" zu veranstalten.
Aber vielleicht lässt sich der 15. Januar anvisieren.

Ich bitte dabei um Ihren Rat und Ihre Unterstützung.

Vielen Dank

-- 
Hartmut Pilch, FFII e.V. und Eurolinux-Allianz            +49-89-12789608
Innovation vs Patentinflation                       http://swpat.ffii.org/ 
125000 Stimmen 400 Firmen gegen Logikpatente    http://www.noepatents.org/

 
