\begin{subdocument}{eubsa-cpedu}{Call for Action}{http://swpat.ffii.org/papers/eubsa-swpat0202/demands/index.en.html}{Workgroup\\swpatag@ffii.org}{The European Commission's proposal for the patentability of software innovations requires a clear response from the European Parliament, the member state governments and other political players. Here is what we think should be done.}
\begin{sect}{prob}{We are concerned that}
\begin{enumerate}
\item
The European Patent Office\footnote{http://swpat.ffii.org/players/epo/index.en.html} (EPO) has, in contradiction to the letter and spirit of the written law, granted tens of thousands of patents on programming and business ideas, below termed ``software patents''.

\item
The European Comission (CEC) is pressing ahead to legalise these patents and make them enforcable in all of Europe.  In doing so, it is disregarding the manifest will and well-argued reasoning of the vast majority of software professionals, software companies, computer scientists and economists.

\item
The CEC is basing its proposal\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/index.en.html} on a draft which was apparently written by Business Software Alliance\footnote{http://swpat.ffii.org/players/bsa/index.en.html} (BSA), a US based organisation dominated by a few big vendors, such as Microsoft.

\item
Software patents interfere with software copyright and tend to lead to the expropriation of software creators rather than to a protection of their property.  Of numerous existing economic studies\footnote{http://swpat.ffii.org/archive/mirror/impact/index.en.html}, none concludes that software patents lead to more productivity, innovation, knowledge diffusion or are in any other way macro-economically beneficial.  Software patentability, as proposed by CEC/BSA, moreover leads to various inconsistencies\footnote{http://swpat.ffii.org/analysis/invention/index.en.html} within the patent system and invalidates central assumptions on which it is built.  As a result, anything becomes patentable and there can no longer be any legal security.

\item
The institutions of the European patent system are not in any meaningful way subjected to democratic control.  The division between legislative and judicial powers is insufficient, and in particular the EPO seems to be a breeding ground for abusive and illegal practices\footnote{http://swpat.ffii.org/players/epo/index.en.html}.
\end{enumerate}
\end{sect}

\begin{sect}{solv}{For these reasons we recommend the following:}
\begin{enumerate}
\item
We urge the European Parliament and the Council to reject the directive proposal COM(2002)92 2002/0047.

\item
We urge the European Parliament to find a way to oblige the EPO to reinstate, as far as patentability is concerned, its examination guidelines of 1978\footnote{http://swpat.ffii.org/papers/epo-gl78/index.en.html} or an equivalent, so as to restore the correct interpretation of the EPC.

\item
We suggest that an independent european court should be charged with reexamining upon demand by any citizen any patents which prima facie may have been granted on the basis of an incorrect interpretation of the patentability provisions of the EPC, and that the EPO should in such cases be obliged to reimburse to the former patent owners all fees which it charged from them.

\item
We urge the legislators at the European and national levels to endorse the current EPC text and consider reinforcing it by modifying it according to the proposal http://swpat.ffii.org/analysis/epc52/index.en.html, as far as this is deemed necessary in order to avoid misinterpretation by lawcourts.

\item
We propose that the European Parliament and the Council consider clarifying the limits of patentability with regard to software and logical creations by issuing a european directive along the lines of the counter-proposals http://swpat.ffii.org/papers/eubsa-swpat0202/prop/index.en.html and http://swpat.ffii.org/papers/eubsa-swpat0202/prop/mini/index.en.html.

\item
We demand that any legislation (including CEC directive proposals as well as rules created by means of caselaw) concerning patentability be tested against a test suite\footnote{http://swpat.ffii.org/analysis/testsuite/index.en.html} of sample patent applications to see whether it would beyond any doubt lead to desired results and would not leave room for any more misinterpretations.

\item
We propose that the European Parliament set up a permanent Patentability Monitoring Comittee with the aim of ensuring that patents are granted only at conditions where they serve the public interest.  This Committee should be composed of MEPs and independent experts in various fields such as mathematics, informatics, natural sciences, engineering, economics, epistemology, ethics and law.  The number of patent practitioners, patent officials or other persons whose income and career is dependent on the patent community should be kept within very narrow limits (e.g. 10-20\percent{}).  The Comittee should benchmark any patent legislation as well as its interpretation by patent offices and patent courts.  Moreover it should conduct hearings, initiate case studies in the effects of the patent system and stimulate related research in the most open and inclusive possible way.  The Committee should report to the European Parliament to what extent patent reality conforms to patent theory and to the public policy goals of the European Community and its members.  The Committee would address the concerns raised by the European Parliament's Comittee on Legal Affairs and Internal Market for Quality Control in the EPO, as expressed in the discussion on the Community Patent regulation COM(2000)0412.

\item
We propose that the European Parliament set up a Comittee of Inquiry to investigate various allegations of irregular behaviour by the proponents of the software and gene patentability directives at the EPO and CEC, such as their close cooperation with a limited circle of lobbyists, their incoherent reasoning and their apparent contempt of democratic and legal principles, and to propose reform measures suitable for preventing this kind of phenomenon from occurring in the future.

\item
We expect that, as long as the problems at the EPO aren't solved, any new regulation such as the Community Patent\footnote{http://www.eurolinux.org/news/cpat01B/index.en.html} will be implemented through institutions other than the EPO.
\end{enumerate}
\end{sect}

\begin{sect}{sign}{Signatories}
\begin{center}
Harlem D\'{e}sir\footnote{http://www.harlemdesir.com} (MEP, PSF, France)\\
Miquel Mayol I Raynal (MEP, Esquerra Republicana de Catalunya\footnote{http://www.esquerra.org/}, Greens / European Free Alliance, Spain)\\
Luis Berenguer Fuster (MEP, PSE, Spain)\\
Raina Mercedes Echerer (MEP, Greens / European Free Alliance, Austria)\\
Rik Hindriks (MP, Netherlands, PvdA\footnote{http://www.pvda.nl/})\\
Jan v. Walsem (MP, Netherlands, Democraten 66\footnote{http://www.d66.nl/})\\
Grietje Bettin\footnote{http://www.sh.gruene.de:8080/MdB/Bettin/ein\_text?datum2=2002/05/13\percent{}2020\percent{}3A00\percent{}3A19\percent{}20GMT\percent{}2B2} (Member of the German Federal Parliament, Media Policy Speaker of the Green Party)\\
F\'{e}lix Lavilla Mart\'{\i}nez (Spanish Senator, Spokesperson for the socialist group in the Senate's Committee on the Information and Knowledge Society)\\
Arseni Gibert (Senator of the Girona region for Entesa Catalana de Progrés\footnote{http://www.senado.es/solotexto/legis7/grupos/GRECP.html})\\
Carles Bonet (Senator of the Girona region for ECP, ECP\footnote{http://www.senado.es/solotexto/legis7/grupos/GRECP.html})\\
Softcatal\'{a}\footnote{http://www.softcatala.org/} (Catalan Software Association, contact: Jordi Mas)\\
Marc Rius (information society delegate of http://www.ic-v.org/)\\
Marcel Batalla i Oliv\'{e} (President of the Information Policy Committee of the Catalonian Republican Party\footnote{http://www.esquerra.org/})\\
Monica Lochner-Fischer\footnote{http://www.lochner-fischer.de/} (member of Bavarian Parliament, SPD)\\
European Co-ordination Council of the Internet Society\footnote{http://www.isoc-ecc.org/} (ISOC-ECC), representing 22 European ISOC chapters (contact: Patrick Vande Walle)\\
VOV@SPD\footnote{http://www.vov.de/} (Internet Association of the German Social Democratic Party) (contact: Axel Schudak, Arne Brand and Boris Piwinger)\\
temPS r\'{e}els\footnote{http://www.temps-reels.net} (Internet Branch of the French Socialist Party) (contact: Thierry Noisette)\\
Danish Association of IT Professionals\footnote{http://www.prosa.dk/} (PROSA.dk) (contact: Peter Ussing)\\
Spanish Association of IT Professionals\footnote{http://www.ati.es/} (ATI.es) (contact: Josep Mol\'{a}s: presidente de la junta directiva general)\\
French Union of Professionals of Study, Consultation, Engineering, Informatics and Services\footnote{http://www.specis.org/} (SPECIS, contact: GIRAUD Bernard, BGiraud@fontenay.sema.slb.com)\\
Comisiones Obreras\footnote{http://www.ccoo.es/} (trade union with 900,000 members, Spain, contact: Alberto Gonz\'{a}lez)\\
M.U.N.C.I.\footnote{http://forums.munci.org/viewtopic.php?t=461} (Mouvement pour une Union Nationale des Consultants en Informatique) (contact: contact at munci org)\\
COMFIA\footnote{http://comfia.net/} (Spain) (Paco Gonz\'{a}lez)\\
Asociaci\'{o}n de Internautas\footnote{http://www.internautas.org} (Spain) (Victor Domingo)\\
Institute for Research in Artificial Intelligence\footnote{http://www.iiia.csic.es/} (IIIA) of the Superior Council of Scientific Research (CSIC) of Spain (contact: Francesc Esteva, director)\\
Faculty of Sciences of Ghent University (contact: Prof. dr. Luc Moens, dean)\\
ESR Pollmeier GmbH\footnote{http://www.esr-pollmeier.de/swpat} (contact: Stefan Pollmeier (Managing Director))\\
Opera Software\footnote{http://www.opera.com/} (contact: H\aa{}kon Wium Lie (CTO))\\
Jetter AG\footnote{http://www.jetter.de/} (contact: Martin Jetter (CEO))\\
Magix AGcontact: Oliver Lorenz (Head of Legal Department)\footnote{http://www.magix.com/}\\
Phaidros AG (contact: Matthias Schlegel (CEO))\\
Myriad Software\footnote{http://www.myriad-online.com/} (contact: Guillion Didier, manager)\\
David Axmark \& Michael Widenius (MySQL AB\footnote{http://www.mysql.com/}, founders)\\
Ole Husgaard (Sparre Software\footnote{http://www.sparre.dk/}, Managing Director)\\
Thorsten Lemke (Lemke Software GmbH\footnote{http://www.lemkesoft.com/}, founder and owner)\\
Stefan Englert (Gesellschaft f\"{u}r Informatik und Produktionstechnik mbH, Managing Director)\\
Rodolphe Quiedeville (Lolix SA\footnote{http://www.lolix.com/}, Managing Director)\\
Alan Cox (CTO of Redhat\footnote{http://www.redhat.com/}, kernel developper, Linux UK\footnote{http://www.linux.org.uk/})\\
Easter-Eggs SA\footnote{http://www.easter-eggs.com/} (contact: Emanuel Raviart (Managing Director))\\
Oberon.net GmbH\footnote{http://www.oberon.net} and LF.net GmbH\footnote{http://www.lf.net} (contact: Kurt Jaeger: Managing Director)\\
Tibosoft GmbH\footnote{http://www.tibosoft.de} (contact: Walter L\"{u}ckemann (Managing Director))\\
No Nonsense Software Inc\footnote{http://www.no-nonsense-software.com/} (contact: Bjarte Hetland (CEO) and Jarle Aasland (Product Manager))\\
Jean-Louis Lespes\footnote{http://www.innovence.fr/index1.php?url=138} (Innovence\footnote{http://www.innovence.fr/}, associated director, lecturer on industrial economics at ``Sorbonne'' University in Paris)\\
Skyrix GmbH\footnote{http://www.skyrix.com/} (contact: Jens Enders (Managing Director))\\
Fr\'{e}d\'{e}ric Broyd\'{e} (Chairman of Excem\footnote{http://www.eurexcem.com/}, Senior member of the IEEE)\\
Dr. Peter Gerwinski (G-N-U GmbH\footnote{http://www.g-n-u.de/}, Managing Director)\\
Intevation GmbH\footnote{http://www.intevation.de/} (contact: Bernhard Reiter and Jan-Oliver Wagner)\\
Dr. Jean-Paul Smets-Solanes (Nexedi\footnote{http://www.nexedi.com/}, Managing Director)\\
Markus DeWendt (Managing Director, Open Logic Systems\footnote{http://www.Open-LS.de/})\\
Stephan K\"{o}rner (Managing Director, Pilum Technology GmbH)\\
Stephan Nobis (Managing Director, ESN GmbH\footnote{http://www.netz-administration.de})\\
Ulf Dunkel (invers Software Vertrieb\footnote{http://www.calamus.net/} and Dunkel Software Distribution\footnote{http://www.dsd.net/}, Managing Director)\\
Dr. Jean-Philippe Rennard (economist, informatician, Managing Director, Eponyme SA\footnote{http://www.eponyme.com/})\\
Prof. Dr. Koen De Bosschere (professor of informatics, Ghent University, Belgium)\\
Prof. Dr. Wilfried Philips (professor of informatics, Ghent University, Belgium)\\
Prof. Dr. Dirk Stroobandt (Ghent University)\\
Prof. Dr. Daniel De Zutter (Ghent University)\\
Prof. Dr. Jan Vandewege (Ghent University)\\
Prof. Dr. Ghislain Hoffman (Ghent University)\\
Prof. Dr. Bart Dhoedt (Ghent University)\\
Prof. Dr. Piet Demeester (Ghent University)\\
Prof. Dr. Herman Tromp (Ghent University)\\
Prof. Dr. Kris Coolsaet (Ghent University)\\
Prof. Dr. Herman Bruyninckx (professor of informatics, K.U. Leuven, Belgium)\\
Prof. Dr. Veerle Fack (Ghent University)\\
Prof. Dr. Armand De Clercq (Ghent University)\\
Prof. Dr. Kim Mens (Universit\'{e} Catholique de Louvain)\\
Prof. Dr. Clemens H. Cap (professor of informatics, Rostock University, Germany)\\
Dr. Peter Bienstman (researcher, Ghent University)\\
Dr. Michiel Ronsse (researcher, Ghent University)\\
Dr. Karl-Friedrich Lenz\footnote{http://www.k.lenz.name/} (Professor (ky\^{o}ju) f\"{u}r Deutsches und Europ\"{a}isches Recht, Univ. Aoyama Gakuin, Japan)\\
Jozef Halbersztadt (Patent examiner at the Polish Patent Office, Warszawa)\\
Prof. Dr. iur. Christian Gizewski (Berlin Technical University, Action against Language Regulation and Informational Obstructionism\footnote{http://www.tu-berlin.de/fb1/AGiW/Cricetus/SOzuC1/Aktion.htm})\\
Prof. Dr. Herbert Hrachovec (Philosophy of Science and Theory of Ditgital Media at Institute of Philosophy of Vienna University)\\
Prof. Dr. Charles Durand (professor at Belfort-Montb\'{e}liard Technical University)\\
Dr. Frank Dittmann (Gesellschaft f\"{u}r Kybernetik e.V.\footnote{http://www.gesellschaft-fuer-kybernetik.org/})\\
Dr. Gregor Nickel (Institute for Mathematics, T\"{u}bingen University)\\
Koenraad Vandenborre (consultant, inno.com\footnote{http://www.inno.com})\\
Steven Noels (consultant, Outerthought.org\footnote{http://www.Outerthought.org})\\
Marc Portier (consultant, Outerthought.org\footnote{http://www.Outerthought.org})\\
Jan Van den Bergh (consultant, inno.com\footnote{http://www.inno.com})\\
Geert Premereur (consultant, inno.com\footnote{http://www.inno.com})\\
Europe Shareware\footnote{http://www.europe-shareware.org/} (contact: Pascal Ricard and Sylvain Perchaud)\\
German Unix User Group\footnote{http://www.guug.de/} (GUUG.de, contact: Christian Lademann, vice president)\\
Danish Unix User Group\footnote{http://www.dkuug.org/} (DKUUG.dk, contact: Ulf Nielsen)\\
French Speaking Association of Users of Linux and Free Software\footnote{http://www.aful.org} (AFUL.org, contact: St\'{e}fane Fermigier and Bernard Lang)\\
Association Pour la Recherche en Informatique Libre\footnote{http://www.april.org} (APRIL.org, contact: Fr\'{e}d\'{e}ric Couchet and Odile B\'{e}nassy)\\
Association of Spanish Users of GNU/Linux\footnote{http://www.hispalinux.es/} (HispaliNUX, contact: Juan Tom\'{a}s Garcia, president)\\
(contact: RA Dipl.-Phys. J\"{u}rgen Siepmann and Daniel Riek)\\
Association of Danish Commercial Linux Stakeholders\footnote{http://www.klid.dk} (KLID.dk, contact: Keld Simonsen)\\
Catalan Linux Users\footnote{http://www.caliu.org/Caliu/Patents/} (CALIU)\\
Free Software Foundation Europe\footnote{http://www.fsfeurope.org/} (contact: Georg Greve, president)\\
Foundation for a Free Information Infrastructure\footnote{http://www.ffii.org/index.en.html} (FFII.org)\\
Europa-Klub e.V.\footnote{http://www.europaklub.de} (contact: Prof. on. Siegfried Piotrowski)\\
Association Bordelaise des Utilisateurs de Logiciels Libres (ABUL.org, contact: Fran\c{c}ois Pellegrini)\\
SSLUG.dk\footnote{http://www.sslug.dk} (contact: Anne \O{}stergaard and Erik Josefsson)\\
Italian Free Software Association\footnote{http://www.software-libero.it/} (ASSOLI, contact: Simone Piccardi, Paolo Didone and Alessandro Rubini)\\
Vereniging Open Source Nederland\footnote{http://www.vosn.nl/patenten} (contact: Dr. Luuk van Dijk)\\
Jesus Gonz\'{a}lez Baharona (ProInnova\footnote{http://proinnova.hispalinux.es/})\\
Asociaci\'{o}n de Software Libre de Le\'{o}n\footnote{http://www.sleon.org/} (contact: Sergio Gonz\'{a}lez)\\
Softwarepatenter.dk\footnote{http:www.softwarepatenter.dk} (Carsten Svaneborg)\\
Erik Lange (MM Manager\footnote{http://www.mmanager.com/})\\
Janus Sandsgaard (Netcetera\footnote{http://www.netcetera.dk})\\
Ole Tange and Jesper M.Jensen (Linux-Kurser\footnote{http://www.linux-kurser.dk/})\\
more signatures\footnote{http://www.ffii.org/assoc/home/eubsa-sign.html} (58882 persons have so far signed this appeal through our new registry form.  They have thereby given us a very detailed mandate for speaking to members of the European Parliament. There is also a very simple and general Eurolinux Petition which you may want to sign first.)
\end{center}
\end{sect}

\begin{sect}{plinov}{Update of 2003/09/24}
the European Parliament has meanwhile already given an adequate response.  It is now up to the EU Council of Ministers\footnote{http://swpat.ffii.org/players/consilium/index.en.html} to support this resopnse and then to the Parliament, in a second reading, to remove remaining inconsistencies from the recitals.

see also EU Parliament Votes for Real Limits on Patentability\footnote{http://swpat.ffii.org/news/03/plen0924/index.en.html}
\end{sect}

\begin{sect}{tasks}{Questions, Things To Do, How you can Help}
\begin{itemize}
\item
{\bf {\bf How you can help us fight patent inflation\footnote{http://swpat.ffii.org/group/todo/index.en.html}}}
\filbreak

\item
{\bf {\bf Get People to Sign}}

\begin{quote}
Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions here\footnote{http://www.eurolinux.org/index.en.html}.

Contact cpedu-help@ffii.org if you want to submit signators or have further questions.
\end{quote}
\filbreak

\item
{\bf {\bf Distribute Posters}}

\begin{quote}
\begin{itemize}
\item
Of course you can breathe freely\footnote{http://www.vrijschrift.org/brussel/poster\_eu\_patents\_of\_course\_you\_can\_breathe\_freely.png}

\item
In the Heaven of Gates\footnote{http://www.vrijschrift.org/brussel/poster\_eu\_patents\_in\_the\_heaven\_of\_gates.png}
\end{itemize}
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\appendix

\subinputs{\input{/ul/sig/srv/res/www/ffii/swpat/patents/samples/swpikmupli.en.sub}
\input{/ul/sig/srv/res/www/ffii/swpat/patents/effects/swpikxrani.en.sub}
\input{/ul/sig/srv/res/www/ffii/swpat/archive/quotes/swpatcusku.en.sub}
\input{/ul/sig/srv/res/www/ffii/swpat/analysis/testsuite/swpatmanri.en.sub}
\input{/ul/sig/srv/res/www/ffii/swpat/papers/eubsa-swpat0202/prop/eubsa-prop.en.sub}
\input{/ul/sig/srv/res/www/ffii/swpat/papers/eubsa-swpat0202/amccarthy0302/amccarthy0302.en.sub}}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: latex ;
% End: ;

