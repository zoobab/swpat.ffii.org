<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Handlingsförslag och krav

#descr: Europeiska kommissionens förslag om patenterbarhet för datorrelaterade uppfinningar kräver ett tydligt svar från Europaparlamentet, medlemsländernas regeringar och andra politiska aktörer. Här förklarar vi vad vi anser måste göras.

#Wce: Vi är allvarligt oroade över

#Fea: Därför rekommenderar vi följande åtgärder:

#Sno: Undertecknare

#dfm: Update of 2003/09/24

#tef: %(ep:Europeiska patentverket) (EPO) har i strid med lagens ord och mening beviljat patent på tiotusentals programmeringsideer och affärsmetoder, nedan benämnda %(q:mjukvarupatent).

#toi: Europeiska kommissionen (CEC) vill snarast legalisera dessa patent för att de ska vinna laga kraft i hela Europa. Kommissionen nonchalerar därmed den tydliga önskan och välgrundade kritik som uttryckts av en överväldigande majoritet professionella programmerare, programvaruföretag, dataloger och ekonomer.

#tle: CEC grundar sitt %(eb:förslag) på ett utkast som uppenbarligen skrivits av %(BSA), en amerikansk organisation som domineras av ett fåtal stora programvaruleverantörer som t ex Microsoft.

#SnW: Mjukvarupatent kommer i konflikt med Upphovsrätten och riskerar att leda till expropriering snarare än skydd av mjukvaruutvecklares egendom. Av de många %(es:ekonomiska studier) som gjorts visar ingen att mjukvarupatent leder till ökad produktivitet, innovation, kunskapsspridning eller på något annat sätt har makroekonomiskt fördelaktiga effekter. Att införa patenterbarhet för programvara enligt CEC/BSAs förslag leder istället till flera olika %(sk:motsägelser) inom patentsystemet och ogiltigförklarar dess grundläggande principer. Följden blir att allt blir patenterbart och att rättsäkerheten upphävs.

#Tra: Det europeiska patentsystemets institutioner är inte på något meningsfullt sätt underkastade demokratisk kontroll. Uppdelningen mellan lagstiftande och dömande makt är bristfällig, och särskilt verkar %(ep:EPO utgöra en grogrund för obehöring och olaglig verksamhet).

#Uis: Vi uppmanar Europaparlamentet och Rådet att förkasta direktivförslaget %(REF)

#RWs: Vi uppmanar Europaparlamentet att hitta ett sätt att tvinga EPO att återinsätta, när det gäller patenterbarhet, sin %(ep:granskningshandbok från 1978) eller motsvarande, får att återställa en korrekt tolkning av EPC.

#Rue: Vi föreslår att en oberoende europeisk domstol på begäran ska vara skyldig att reexaminera patent som sannolikt har blivit beviljade på grund av en felaktig tolkning av EPC, och att EPO i sådana fall ska tvingas återbetala alla avgifter den drabbade patentinnehavaren betalat för dem.

#Ett: Vi kräver att lagstiftare på Europeisk och nationell nivå stadfäster den nuvarande ordalydelsen av EPC och att de överväger att utöka texten i enlighet med förslaget %(URL) om det bedöms nödvändigt för att undvika feltolkning i domstol.

#Ane: Vi föreslår att Europaparlamentet och Rådet överväger att klargöra gränserna för patenterbarhet av datorprogram och logiska verk genom att utfärda ett europeiskt direktiv i enlighet med förslaget %(URL).

#Dsl: Vi kräver att varje lagstiftning om patenterbarhet (såväl direktivförslaget från CEC som regler skapade genom prejudikat) prövas mot en %(ts:testsvit) av patentansökningar för att säkerställa att lagen leder till önskat utfall och att den inte längre lämnar något utrymme för feltolkningar.

#Prt: Vi föreslår att Europaparlamentet inrättar ett permanent utskott för övervakning av gränsen för patenterbarhet med syfte att garantera att patent enbart utfärdas på villkor att de är av samhällsintresse. Detta utskott bör bestå av europeiska parlamentsledamöter och oberoende experter från olika områden som t ex matematik, informatik, naturvetenskap, teknik, ekonomi, vetenskapsteori, etik och juridik. Andelen utövande patentjurister, anställda vid patentverken eller andra personer vars huvudsakliga inkomst och karriär ligger inom patentsystemet bör hållas på en låg nivå (10-20%). Utskottet bör undersöka både patentlagstiftning och den tolkning som görs vid patentverk och patentdomstolar. Det bör också anordna hearings, initiera fallstudier över patensystemets effekter och stimulera forskning på ett öppnet och omfattande sätt. Utskottet bör rapportera till Europaparlamentet i vilken utsträckning patentpraktik överensstämmer med patentteori och hur EU och EUs medlemsländers politiska målsättningar uppfylls. Utskottet skulle ta itu med den kvalitetskontroll inom EPO som Europaparlamentets utskott för rättsliga frågor och den inre marknaden diskuterat i samband med förslaget till rådets förordning om gemenskapspatent (KOM(2000) 412).

#AWo: Vi föreslår att Europaparlamentet iordningställer en utredande kommitte för att undersöka anklagelser om reglementsvidrigt uppförande av dem inom EPO och CEC som förespråkat mjukvaru- och genpatentdirektiven, som t ex deras nära samarbete med en liten krets lobbyister, deras osammanhängande argumentation och deras uppenbara förakt för lag och demokratiska principer, och för att föreslå ändamålsenliga reformer och åtgärder för att förhindra att liknande företeelser uppkommer i framtiden.

#EdW: Vi förväntar oss att så länge problemen inom EPO förblir olösta kommer varje förordning såsom t ex gemenskapspatentet att implementeras genom andra institutioner än EPO.

#Gen: Greens

#FrA: European Free Alliance

#MSe: Media Policy Speaker of the Green Party

#Sco: Spanish Senator, Spokesperson for the socialist group in the Senate's Committee on the Information and Knowledge Society

#Sna: Senador per Girona de l'%(ECP)

#aIo: Catalan Software Association

#mto: responsable de Societat de la Informació d'%(ICV)

#esquerra: President de la Sectorial de la Societat de la Informació d'%(es:Esquerra Republicana de Catalunya)

#eal: member of Bavarian Parliament

#isocecc: European Co-ordination Council of the Internet Society

#rut: representing 22 European ISOC chapters

#eei: Internet Branch of the French Socialist Party

#DiW: Forbundet af IT-professionelle

#ATI: Asociación de Técnicos en Informática

#SPECIS: Syndicat National Professionnel des Etudes, du Conseil, de l'Ingénierie, de l'Informatique et des Services

#etW: trade union with 900,000 members

#IIIA: %(ii:Institut d'Investigació en Intel·ligència Artificial) del %(cs:Consejo Superior de Investigaciones Científicas)

#tsi: Faculty of Sciences of Ghent University

#dea: dean

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#mng: manager

#fud: founders

#fed: founder and owner

#CWW: CTO of %1

#kle: kernel developper

#Pcn: Product Manager

#dta: directeur associé

#Dee: lecturer on industrial economics at %(q:Sorbonne) University in Paris

#anc: Chairman of %(EE)

#iee: Senior member of the IEEE

#cos: économiste

#nmc: expert informatique

#prh: Patent examiner at the Polish Patent Office

#AWm: Aktion gegen Öffentliche Sprachregelungen und Kommunikationserschwernisse

#PWo: Philosophy of Science and Theory of Ditgital Media at Institute of Philosophy of Vienna University

#mathinst: Institut für Mathematik

#DKUUG: Dansk UNIX-system Bruger Gruppe

#FiL

#Aln

#hispalinux: Asociación de Usuarios Españoles de GNU/Linux

#KLID: Komercielle Linux-interssenter I Danmark

#caliu: Catalan Linux Users

#IeA: Assoziazione Software Libero

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal through our %(ei:new registry form).  They have thereby given us a very detailed mandate for speaking to members of the European Parliament. There is also a very simple and general %(ep:Eurolinux Petition) which you may want to sign first.

#eun: the European Parliament has meanwhile already given an adequate response.  It is now up to the %(sc:EU Council of Ministers) to support this resopnse and then to the Parliament, in a second reading, to remove remaining inconsistencies from the recitals.

#tlS: Get People to Sign

#Pnr: Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions %(el:here).

#Mth: Contact %(MT) if you want to submit signators or have further questions.

#stt: Distribute Posters

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: erikj ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-cpedu ;
# txtlang: sv ;
# multlin: t ;
# End: ;

