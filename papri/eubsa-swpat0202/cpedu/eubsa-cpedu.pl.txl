<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Wezwanie do działania

#descr: Opracowany przez Komisję Europejską projekt dyrektywy dotyczącej objęcia prawem patentowym wynalazków z dziedziny oprogramowania komputerowego wymaga jednoznacznej reakcji ze strony Parlamentu Europejskiego, władz krajów członkowskich UE i innych ciał politycznych. Niniejszym przedstawiamy, jakie naszym zdaniem należy podjąć działania.

#Wce: Jesteśmy zaniepokojeni tym, że:

#Fea: Z tych powodów zalecamy, co następuje:

#Sno: Sygnatariusze

#dfm: Update of 2003/09/24

#tef: %(ep:Europejski Urząd Patentowy) (EUP), wbrew literze i duchowi obowiązującego prawa udzielił dziesiątków tysięcy patentów na rozwiązania programistyczne i metody prowadzenia działalności gospodarczej, które są niżej nazywane %(q:patentami na oprogramowanie komputerowe).

#toi: Komisja Europejska wywiera nacisk, by zalegalizować patenty na oprogramowanie komputerowe i egzekwować ich przestrzeganie w całej Europie. W swym dążeniu Komisja Europejska ignoruje wyraźnie wyrażaną wolę i dobrze uzasadnione racje zdecydowanej większości twórców oprogramowania, przedsiębiorstw informatycznych, informatyków akademickich i ekonomistów.

#tle: Komisja Europejska bazuje swą %(eb:propozycję) na założeniach, które są najprawdopodobniej autorstwa %(Business Software Alliance) (BSA) - amerykańskiej instytucji zdominowanej przez nielicznych, wielkich wytwórców oprogramowania, z firmą Microsoft na czele.

#SnW: Patenty na oprogramowanie komputerowe kłócą się z prawami autorskimi twórców oprogramowania i zamiast bronić własności intelektualnej autorów, zwykle dają odwrotny skutek, stając się narzędziem wywłaszczania twórców oprogramowania z ich praw. Spośród wielu przeprowadzonych %(es:badań ekonomicznych) żadne nie potwierdziło tezy, że patenty na oprogramowanie komputerowe skutkują wzrostem produktywności, innowacyjności, rozpowszechniania wiedzy, lub przynoszą inne korzyści w skali makroekonomicznej. Co więcej, zaakceptowanie patentów na oprogramowanie komputerowe, w formie zaproponowanej przez Komisję Europejską i BSA, prowadzi do rozmaitych %(sk:niekonsekwencji) wewnątrz systemu patentowego, a nawet jest sprzeczne z jego głównymi założeniami. Oznacza to, że opatentować można wszystko, a prawo nie zapewnia już żadnej ochrony przed nadużyciami

#Tra: Instytucje europejskiego systemu patentowego nie podlegają żadnej istotnej kontroli demokratycznej. Podział władzy między prawodawczą i sądowniczą jest niedostateczny. Wydaje się, że szczególnie w ramach %(ep:Europejskiego Urzędu Patentowego rozwinęło się podłoże dla będących tego skutkiem wątpliwych i bezprawnych praktyk).

#Uis: Wzywamy Parlament Europejski i Radę (Europejską) do odrzucenia propozycji dyrektywy %(REF).

#RWs: Wzywamy Parlament Europejski, by zobligował Europejski Urząd Patentowy do przywrócenia, w zakresie dotyczącym definicji wynalazku, %(ep:wytycznych do rozpatrywania zgłoszeń z 1978 roku), lub innych równoważnych im regulacji, które pozwoliłyby przywrócić właściwą interpretację Europejskiej Konwencji Patentowej (EKP).

#Rue: Proponujemy stworzenie europejskiej procedury sądowej, w ramach której na żądanie każdego obywatela ponownie rozpatrywano by zasadność utrzymania praw patentowych, w przypadkach gdy nie ulega wątpliwości (prima facie), że w trakcie udzielania patentu w niewłaściwy sposób stosowane były reguły EKP. W takich przypadkach EUP byłby zobowiązany zwracać właścicielowi unieważnionego patentu wszystkie pobrane opłaty.

#Ett: Wzywamy prawodawców szczebla europejskiego i państw członkowskich, by usankcjonowali i potwierdzili obecne brzmienie EKP, a także by rozważyli możliwość jej doprecyzowania przez uwzględnienie poprawek zgodnie z propozycją %(URL), w zakresie, jaki okaże się konieczny dla uniknięcia nieprawidłowych interpretacji w sądach.

#Ane: Proponujemy, by Parlament Europejski i Rada (Europejska) rozważyły kwestię jasnego określenia granic stosowalności prawa patentowego w odniesieniu do oprogramowania i innych tworów natury logicznej poprzez ustanowienie dyrektywy europejskiej, uwzględniającej zasady propozycji %(URL).

#Dsl: Żądamy, by wszelkie akty prawodawcze (w tym opracowywane w Komisji Europejskiej propozycje dyrektyw oraz wytyczne do orzekania wynikające z precedensów) dotyczące patentowalności podlegały wpierw testowaniu na %(ts:dobranych losowo zbiorach zgłoszeń), w celu stwierdzenia ponad wszelką wątpliwość, czy prowadzą one do pożądanych skutków i czy nie stwarzają niebezpieczeństwa niewłaściwych interpretacji.

#Prt: Proponujemy, by Parlament Europejski powołał stały Komitet Nadzoru Patentowego, którego zadaniem byłoby gwarantowanie zasady, że patenty są udzielane jedynie wtedy, gdy jest to w interesie publicznym. Komitet ten powinien się składać z deputowanych Parlamentu Europejskiego i niezależnych ekspertów z różnych dziedzin, w tym matematyki, informatyki, nauk przyrodniczych, techniki, ekonomii, epistemologii, etyki i prawa. Udział w pracach komitetu rzeczników patentowych, urzędników patentowych i innych osób, których dochody i kariery zależą od środowisk zawodowo związanych z patentowaniem powinien być utrzymany w wąskich granicach (np. 10-20%). Komitet powinien weryfikować, na podstawie odpowiednich standardów, całość prawodawstwa patentowego, a także jego interpretację przez urzędy patentowe i sądy. Powinien także prowadzić przesłuchania, inicjować prace studialne w zakresie skutków działania prawa patentowego, a także stymulować związane z tym analizy w możliwie szeroki i dostępny dla zainteresowanych sposób. Komitet winien przedkładać na forum Parlamentu Europejskiego raporty oceniające, w jakim stopniu praktyka patentowa odpowiada teorii oraz celom politycznym Unii Europejskiej i państw członkowskich. Ponadto Komitet powinien zainteresować się uwagami co do jakości udzielanych przez EUP patentów, jakie zostały zgłoszone przez Komitet ds. Prawa i Rynku Wewnętrznego Parlamentu Europejskiego podczas dyskusji na temat rozporządzenia o Patencie Wspólnotowym %(REF).

#AWo: Proponujemy, by Parlament Europejski powołał Komisję Śledczą dla zbadania podejrzeń o naruszające normy postępowanie inicjatorów opracowywanych w EUP i Komisji Europejskiej dyrektyw, dotyczących objęcia patentami rozwiązań z dziedziny oprogramowania komputerowego i wyników badań genetycznych. Do tych nieprawidłowości należą między innymi bliska współpraca z wąskim gronem lobbystów, nadużywania nie mającej związku z problematyką argumentacji oraz wyraźne lekceważenie zasad demokracji i reguł państwa prawa. Komisja powinna zaproponować środki właściwe dla zapobieżenia takim sytuacjom w przyszłości.

#EdW: Oczekujemy, że dopóki problemy EUP nie znajdą właściwego rozwiązania, wszelkie nowe uregulowania, takie jak np. %(cp:Patent Wspólnotowy), powinny być wdrażane przez inne niż EUP instytucje.

#Gen: Greens

#FrA: European Free Alliance

#MSe: Media Policy Speaker of the Green Party

#Sco: Spanish Senator, Spokesperson for the socialist group in the Senate's Committee on the Information and Knowledge Society

#Sna: Senador per Girona de l'%(ECP)

#aIo: Catalan Software Association

#mto: responsable de Societat de la Informació d'%(ICV)

#esquerra: President de la Sectorial de la Societat de la Informació d'%(es:Esquerra Republicana de Catalunya)

#eal: member of Bavarian Parliament

#isocecc: European Co-ordination Council of the Internet Society

#rut: representing 22 European ISOC chapters

#eei: Internet Branch of the French Socialist Party

#DiW: Duńskie Stowarzyszenie Profesjonalistów IT

#ATI: Asociación de Técnicos en Informática

#SPECIS: Syndicat National Professionnel des Etudes, du Conseil, de l'Ingénierie, de l'Informatique et des Services

#etW: trade union with 900,000 members

#IIIA: %(ii:Institut d'Investigació en Intel·ligència Artificial) del %(cs:Consejo Superior de Investigaciones Científicas)

#tsi: Faculty of Sciences of Ghent University

#dea: dean

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#mng: manager

#fud: founders

#fed: founder and owner

#CWW: %1 CTO

#kle: programista systemowy z zespołu tworzącego jądro

#Pcn: Product Manager

#dta: directeur associé

#Dee: lecturer on industrial economics at %(q:Sorbonne) University in Paris

#anc: Chairman of %(EE)

#iee: Senior member of the IEEE

#cos: économiste

#nmc: expert informatique

#prh: Patent examiner at the Polish Patent Office

#AWm: Aktion gegen Öffentliche Sprachregelungen und Kommunikationserschwernisse

#PWo: Philosophy of Science and Theory of Ditgital Media at Institute of Philosophy of Vienna University

#mathinst: Institut für Mathematik

#DKUUG: Dansk UNIX-system Bruger Gruppe

#FiL

#Aln

#hispalinux: Asociación de Usuarios Españoles de GNU/Linux

#KLID: Komercielle Linux-interssenter I Danmark

#caliu: Catalan Linux Users

#IeA: Assoziazione Software Libero

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal through our %(ei:new registry form).  They have thereby given us a very detailed mandate for speaking to members of the European Parliament. There is also a very simple and general %(ep:Eurolinux Petition) which you may want to sign first.

#eun: the European Parliament has meanwhile already given an adequate response.  It is now up to the %(sc:EU Council of Ministers) to support this resopnse and then to the Parliament, in a second reading, to remove remaining inconsistencies from the recitals.

#tlS: Get People to Sign

#Pnr: Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions %(el:here).

#Mth: Contact %(MT) if you want to submit signators or have further questions.

#stt: Distribute Posters

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: telenetforum ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-cpedu ;
# txtlang: pl ;
# multlin: t ;
# End: ;

