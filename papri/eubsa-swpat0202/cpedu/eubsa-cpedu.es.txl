<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Peticiones de acción

#descr: Lista de acciones para ser realizadas por el Parlamento Europeo y otras instituciones

#Wce: Nos preocupa que

#Fea: Por estas razones recomendamos lo siguiente:

#Sno: Signatories

#dfm: Update of 2003/09/24

#tef: La %(ep:Oficina Europea de Patentes) (OEP) ha concedido, en contradicción con la letra y el espíritu de la ley, decenas de miles de patentes sobre programación e ideas de negocio, a las que nos referiremos de ahora en adelante como  %(q:patentes de software).

#toi: La Comisión Europea (CE) está presionando para legalizar estas patentes y obligar a su cumplimiento en toda Europa. Al hacerlo, está despreciando las manifestaciones y los razonamientos de la amplia mayoría de profesionales del software, de las empresas del software, de los investigadores en software y de los economistas.

#tle: La CE basa su propuesta en un borrador escrito por la %(BSA), una organización basada en EE.UU. y dominada por unos pocos grandes fabricantes, como Microsoft.

#SnW: Las patentes de software interfieren con la aplicación de los derechos de autor (copyright) al software, y tienen a la expropiación de los creadores de programas en lugar de a proteger su propiedad. No existe ningún estudio económico que concluya que las patentes de software mejoran la productividad, la innovación, la difusión del conocimiento, ni produzcan de ninguna otra forma beneficios macroeconómicos. La patentabilidad del software, tal y como está propuesta por la CE y la BSA, conlleva sin embargo varias inconsistencias dentro del sistema de patentes, e invalida algunas de las suposiciones básicas sobre las que está construido. Como consecuencia, todo pasa a ser patentable, y se acaba cualquier seguridad jurídica al respecto.

#Tra: Las instituciones del sistema europeo de patentes no están sujetas de ninguna forma significativa al control democrático. La división entre los poderes legislativo y judicial es insuficiente, y en particular %(ep:la OEP parece estar abonando el terreno para las prácticas abusivas e ilegales).

#Uis: Urgimos al Parlamento Europeo y al Consejo Europeo que rechacen la propuesta de directiva %(REF).

#RWs: Urgimos al Parlamento Europeo a encontrar una forma de obligar a la OEP a reinstalar, en lo que refiere a patentabilidad, sus %(ep:normas de examen de 1978) o un equivalente, de forma que se restablezca la interpretación correcta de la Convención Europea de Patentes (CEP).

#Rue: Sugerimos que un tribunal europeo independiente debería encargarse de reexaminar, ante la petición de cualquier ciudadano, cualquier patente que a primera vista pueda haber sido concedida sobre la base de una interpretación incorrecta de la CEP, y que la OEP debería, en esos casos, ser obligada a reembolsar a los dueños de la patente todos los gastos que les hubiera acarreado la solicitud de esa patente.

#Ett: Urgimos a los legisladores, a nivel europeo y a nivel nacional, a ratificar el texto actual de la CEP y a considerar su reforzamiento modificándolo de acuerdo con la propuesta %(URL), en caso de que esto sea necesario para evitar interpretaciones equivocadas en los tribunales.

#Ane: Proponemos que el Parlamento Europeo y el Consejo Europeo consideren la clarificación de los límites a la patentabilidad con respecto al software y a las creaciones lógicas, promulgando una directiva europea que siga la propuesta %(URL).

#Dsl: Demandamos que cualquier legislación (incluyendo tanto las propuestas de directiva de la CE como las reglas que se crean mediante jurisprudencia) que tenga relación con la patentabilidad sea probada contra un %(ts:juego de pruebas) consistente en muestras de solicitudes de patente, para comprobar más allá de toda duda que produce los resultados deseados, y que no se deja espacio para más interpretaciones equivocadas.

#Prt: Proponemos que el Parlamento Europeo establezca un Comité Permanente de Monitorización de la Patentabilidad, con el objetivo de asegurar que las patentes se conceden sólo en condiciones que sirvan al interés público. Este Comité debería estar compuesto por parlamentarios y expertos de varios campos, como las matemáticas, la informática, las ciencias naturales, la ingeniería, la economía, la epistemología, la ética y el derecho. El número de juristas que litiguen habitualmente en temas de patentes, funcionarios de oficinas de patentes y otras personas cuyos ingresos y cuya carrera dependa de la comunidad relacionada con las patentes debería mantenerse dentro de límites estrechos (por ejemplo, 10-20%). El Comité debería contrastar cualquier legislación sobre patentes, y su interpretación por las oficinas y los tribunales de patentes. Además, debería organizar audiencias, iniciar estudios de casos sobre los efectos del sistema de patentes, y estimular la investigación relacionada de la forma más abierta e inclusiva que sea posible. El Comité debería informar al Parlamento Europeo hasta qué punto la realidad de las patentes coincide con la teoría sobre las patentes y con los objetivos de la política pública de la Comunidad Europea y sus miembros. El Comité se ocuparía también de los asuntos que proponga el Comité de Asuntos Legales y Mercado Interno del Parlamento Europeo sobre el control de calidad en la OEP, como se explica en la discusión sobre la regulación de la Patente Comunitaria %(REF).

#AWo: Proponemos que el Parlamento Europeo establezca un Comité de Investigación para investigar varias acusaciones de comportamiento irregular por los ponentes de las directivas sobre patentabilidad del software y de los genes, en la OEP y la CE, como su cooperación cercana con un círculo cerrado de influenciadores profesionales (lobbyists), su razonamiento incoherente y su aparente ignorancia de los principios legales y democráticos, y proponer medidas de reforma adecuadas para evitar que este fenómeno ocurra en el futuro.

#EdW: Esperamos que, mientras se resuelven los problemas en la OEP, cualquier nueva regulación, como la %(cp:Patente Comunitaria), se implemente mediante instituciones distintas de la OEP.

#Gen: Greens

#FrA: European Free Alliance

#MSe: Media Policy Speaker of the Green Party

#Sco: Portavoz del grupo socialista en la Comision de la Sociedad de la Información y el Conocimiento del Senado español

#Sna: Senador per Girona de l'%(ECP)

#aIo: Catalan Software Association

#mto: responsable de Societat de la Informació d'%(ICV)

#esquerra: President de la Sectorial de la Societat de la Informació d'%(es:Esquerra Republicana de Catalunya)

#eal: member of Bavarian Parliament

#isocecc: European Co-ordination Council of the Internet Society

#rut: representing 22 European ISOC chapters

#eei: Internet Branch of the French Socialist Party

#DiW: Forbundet af IT-professionelle

#ATI: Asociación de Técnicos en Informática

#SPECIS: Syndicat National Professionnel des Etudes, du Conseil, de l'Ingénierie, de l'Informatique et des Services

#etW: trade union with 900,000 members

#IIIA: %(ii:Institut d'Investigació en Intel·ligència Artificial) del %(cs:Consejo Superior de Investigaciones Científicas)

#tsi: Faculty of Sciences of Ghent University

#dea: dean

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#mng: manager

#fud: founders

#fed: founder and owner

#CWW: CTO of %1

#kle: kernel developper

#Pcn: Product Manager

#dta: directeur associé

#Dee: lecturer on industrial economics at %(q:Sorbonne) University in Paris

#anc: Chairman of %(EE)

#iee: Senior member of the IEEE

#cos: économiste

#nmc: expert informatique

#prh: ejaminador de patentes en la Oficina Polacca de Patentes

#AWm: Aktion gegen Öffentliche Sprachregelungen und Kommunikationserschwernisse

#PWo: Philosophy of Science and Theory of Ditgital Media at Institute of Philosophy of Vienna University

#mathinst: Institut für Mathematik

#DKUUG: Dansk UNIX-system Bruger Gruppe

#FiL

#Aln

#hispalinux: Asociación de Usuarios Españoles de GNU/Linux

#KLID: Komercielle Linux-interssenter I Danmark

#caliu: Catalan Linux Users

#IeA: Assoziazione Software Libero

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal through our %(ei:new registry form).  They have thereby given us a very detailed mandate for speaking to members of the European Parliament. There is also a very simple and general %(ep:Eurolinux Petition) which you may want to sign first.

#eun: the European Parliament has meanwhile already given an adequate response.  It is now up to the %(sc:EU Council of Ministers) to support this resopnse and then to the Parliament, in a second reading, to remove remaining inconsistencies from the recitals.

#tlS: Get People to Sign

#Pnr: Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions %(el:here).

#Mth: Contact %(MT) if you want to submit signators or have further questions.

#stt: Distribute Posters

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: baharona ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-cpedu ;
# txtlang: es ;
# multlin: t ;
# End: ;

