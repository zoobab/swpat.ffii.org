<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Peticions d'acció

#descr: Llista d'accions que ha de fer el Parlament Europeu i d'altres institucions

#Wce: Ens preocupa que

#Fea: Per aquestes raons recomanem el següent:

#Sno: Signataris

#dfm: Update of 2003/09/24

#tef: La %(ep:Oficina Europea de Patents) (OEP) ha concedit, en contradicció amb la lletra i l'esperit de la llei, decenes de milers de patents sobre programari i idees de negoci, a les quals ens referirem d'ara endavant com a  %(q:patents de programari).

#toi: La Comissió Europea (CE) pressiona per a legalitzar aquestes patents i obligar el seu cumpliment a tota Europa. En fer-ho, menysprea les manifestacions i els raonaments de l'àmplia majoria de professionals del programari, de les empreses de programari, dels investigadors de programari i dels economistes.

#tle: La CE basa la seva proposta en un esborrany escrit per la %(BSA), una organització basada als EUA i dominada per uns pocs grans fabricants, com ara Microsoft.

#SnW: Les patents de programari interfereixen en l'aplicació dels drets d'autor (copyright) del programari i tendeixen a la expropiació dels creadors de programes en comptes de protegir la seva propietat. No hi ha cap estudi econòmic que arribi a la conclusió que les patents de programari millorin la productivitat, la innovació, la difussió del coneixement, ni produeixin de cap altra manera beneficis macroeconòmics. La patentabilitat del programari, tal i com està proposada per la CE i la BSA, comporta en canvi diverses inconsistències dins del sistema de patents, i invalida algunes de les suposicions bàsiques sobre les què està construit. Com a conseqüència, tot passa a ser patentable i s'acaba qualsevol seguretat jurídica al respecte.

#Tra: Les institucions del sistema europeu de patents no estan subjectes de cap manera significativa al control democràtic. La divisió entre els poders legislatiu i judicial és insuficient i, en particular, %(ep:la OEP sembla abonar el terreny per a les pràctiques abusives i il·legals).

#Uis: Urgim al Parlament Europeu i al Consell Europeu que rebutgin la proposta de directiva %(REF).

#RWs: Urgim al Parlament Europeu a trobar una forma d'obligar la OEP a reinstal·lar, quant a patentabilitat, les seves %(ep:normes d'examen de 1978) o un equivalent, de manera que es reestableixi la interpretació correcta de la Convenció Europea de Patents (CEP).

#Rue: Suggerim que un tribunal europeu independent hauria d'encarregar-se de reexaminar, davant la petició de qualsevol ciutadà, qualsevol patent que a primera vista pugui haver estat concedida sobre la base d'una interpretació incorrecta de la CEP, i que la OEP hauria, en aquests casos, de ser obligada a retornar als amos de la patent totes les despeses de sol·licitud de la patent.

#Ett: Urgim els legisladors, a nivell europeu i a nivell nacional, a ratificar el texte actual de la CEP i a considerar el seu reforçament tot modificant-lo d'acord amb la proposta %(URL), en cas que això sigui necesari per a evitar interpretacions equivocades en els tribunals.

#Ane: Proposem que el Parlament Europeu i el Consell Europeu considerin la clarificació dels límits a la patentabilitat respecte del programari i les creacions lògiques, promulgant una directiva europea que sigui la proposta %(URL).

#Dsl: Demanem que cualsevol legislació (incloses tant les propostes de directiva de la CE com les regles que es creïn mitjançant jurisprudència) que tingui relació amb la patentabilitat sigui provada amb un (ts:joc de proves) consistent en mostres de sol·licituts de patent, per comprovar més enllà de tot dubte que produeix els resultats desitjats i que no es deixa espai per a interpretacions errònies.

#Prt: Proposem que el Parlament Europeu estableixi un Comitè Permanent de Monitorització de la Patentabilitat, amb l'objectiu d'assegurar que les patents es concedeixen només en condicions que serveixin a l'interés públic. Aquest Comitè hauria d'estar composat per parlamentaris i experts de diversos camps, com les matemàtiques, la informàtica, les ciències naturals, l'enginyeria, l'economia, l'epistemologia, l'ètica i el dret. El número de juristes que treballen habitualment en tems de patents, funcionaris d'oficines de patents i d'altres persones els ingresos i la carrera professional dels quals depenguin de la comunitat relacionada amb les patents hauria de mantenir-se dins de límits estrets (per exemple, 10-20%). El Comitè hauria de contrastar qualsevol legislació sobre patents i la seva interpretació per les oficines i els tribunals de patents. A més, hauria d'organitzar audiències, iniciar estudis de casos sobre els efectes del sistema de patents i estimular la investigació relacionada de la forma més oberta i inclusiva que sigui possible. El Comitè hauria d'informar el Parlament Europeu de fins a quin punt la realitat de les patents coincideix amb la teoria sobre les patents i amb els objectius de la política pública de la Comunitat Europea i els seus membres. El Comitè s'ocuparia també dels assumptes que propossi el Comitè d'Asumptes Legals i Mercat Intern del Parlament Europeu sobre el control de qualitat a la OEP, com s'explica a la discussió sobre la regulació de la Patent Comunitària %(REF).

#AWo: Proposem que el Parlament Europeu estableixi un Comitè d'Investigació per a investigar diverses acusacions de comportament irregular pels ponents de les directives sobre patentabilitat de programari i dels gens, a la OEP i la CE, com la seva cooperació propera amb un càlcul tancat d'influenciadors professionals (lobbyists), el seu raonament incoherent i la seva aparent ignorància dels principis legals i democràtics, així com proposar mesures de reforma adequades per evitar que aquest fenòmen torni a passar en el futur.

#EdW: Esperem que, mentre es resolen els problemes a la OEP, qualsevol nova regulació, com la %(cp:Patent Comunitària), s'implementi mitjantçant institucions diferents de la OEP.

#Gen: Greens

#FrA: European Free Alliance

#MSe: Media Policy Speaker of the Green Party

#Sco: Spanish Senator, Spokesperson for the socialist group in the Senate's Committee on the Information and Knowledge Society

#Sna: Senador per Girona de l'%(ECP)

#aIo: Catalan Software Association

#mto: responsable de Societat de la Informació d'%(ICV)

#esquerra: President de la Sectorial de la Societat de la Informació d'%(es:Esquerra Republicana de Catalunya)

#eal: member of Bavarian Parliament

#isocecc: European Co-ordination Council of the Internet Society

#rut: representing 22 European ISOC chapters

#eei: Internet Branch of the French Socialist Party

#DiW: Forbundet af IT-professionelle

#ATI: Asociación de Técnicos en Informática

#SPECIS: Syndicat National Professionnel des Etudes, du Conseil, de l'Ingénierie, de l'Informatique et des Services

#etW: trade union with 900,000 members

#IIIA: %(ii:Institut d'Investigació en Intel·ligència Artificial) del %(cs:Consejo Superior de Investigaciones Científicas)

#tsi: Faculty of Sciences of Ghent University

#dea: dean

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#mng: manager

#fud: founders

#fed: founder and owner

#CWW: CTO of %1

#kle: kernel developper

#Pcn: Product Manager

#dta: directeur associé

#Dee: lecturer on industrial economics at %(q:Sorbonne) University in Paris

#anc: Chairman of %(EE)

#iee: Senior member of the IEEE

#cos: économiste

#nmc: expert informatique

#prh: Patent examiner at the Polish Patent Office

#AWm: Aktion gegen Öffentliche Sprachregelungen und Kommunikationserschwernisse

#PWo: Philosophy of Science and Theory of Ditgital Media at Institute of Philosophy of Vienna University

#mathinst: Institut für Mathematik

#DKUUG: Dansk UNIX-system Bruger Gruppe

#FiL

#Aln

#hispalinux: Asociación de Usuarios Españoles de GNU/Linux

#KLID: Komercielle Linux-interssenter I Danmark

#caliu: Catalan Linux Users

#IeA: Assoziazione Software Libero

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal through our %(ei:new registry form).  They have thereby given us a very detailed mandate for speaking to members of the European Parliament. There is also a very simple and general %(ep:Eurolinux Petition) which you may want to sign first.

#eun: the European Parliament has meanwhile already given an adequate response.  It is now up to the %(sc:EU Council of Ministers) to support this resopnse and then to the Parliament, in a second reading, to remove remaining inconsistencies from the recitals.

#tlS: Get People to Sign

#Pnr: Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions %(el:here).

#Mth: Contact %(MT) if you want to submit signators or have further questions.

#stt: Distribute Posters

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-cpedu ;
# txtlang: ca ;
# multlin: t ;
# End: ;

