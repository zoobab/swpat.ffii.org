\select@language {catalan}
\contentsline {chapter}{\numberline {1}Ens preocupa que}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Per aquestes raons recomanem el seg\@umlaut {u}ent:}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Signataris}{7}{chapter.3}
\contentsline {chapter}{\numberline {4}Update of 2003/09/24}{11}{chapter.4}
\contentsline {chapter}{\numberline {5}Preguntes, Coses a fer, Com podeu ajudar}{12}{chapter.5}
\contentsline {chapter}{\numberline {A}Patents Europees de Programari: Exemples Escollits}{13}{appendix.A}
\contentsline {section}{\numberline {A.1}Exhibited Specimens}{13}{section.A.1}
\contentsline {section}{\numberline {A.2}Candidates}{22}{section.A.2}
\contentsline {section}{\numberline {A.3}Enlla\c {c}os anotats}{22}{section.A.3}
\contentsline {chapter}{\numberline {B}Patents de Programari en Acci\'{o}}{23}{appendix.B}
\contentsline {section}{\numberline {B.1}Some Well Documented Cases}{23}{section.B.1}
\contentsline {section}{\numberline {B.2}Enlla\c {c}os anotats}{32}{section.B.2}
\contentsline {chapter}{\numberline {C}Banc de proves de s\`{e}ries d'assaigs de legislaci\'{o} de patentabilitat}{34}{appendix.C}
\contentsline {section}{\numberline {C.1}Algunes mostres de patents}{34}{section.C.1}
\contentsline {section}{\numberline {C.2}Sample Claims}{36}{section.C.2}
\contentsline {section}{\numberline {C.3}Preguntes per respondre per cada}{37}{section.C.3}
\contentsline {subsection}{\numberline {C.3.1}Clarity}{37}{subsection.C.3.1}
\contentsline {subsection}{\numberline {C.3.2}Adequacy}{37}{subsection.C.3.2}
\contentsline {section}{\numberline {C.4}Comparison Table}{37}{section.C.4}
\contentsline {section}{\numberline {C.5}Legislacions candidates a ser assajades}{38}{section.C.5}
\contentsline {section}{\numberline {C.6}Enlla\c {c}os anotats}{40}{section.C.6}
