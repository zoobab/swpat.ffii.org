# -*- mode: makefile -*-

peticiones.es.lstex:
	lstex peticiones.es | sort -u > peticiones.es.lstex
peticiones.es.mk:	peticiones.es.lstex
	vcat /ul/prg/RC/texmake > peticiones.es.mk


peticiones.es.dvi:	peticiones.es.mk
	rm -f peticiones.es.lta
	if latex peticiones.es;then test -f peticiones.es.lta && latex peticiones.es;while tail -n 20 peticiones.es.log | grep -w references && latex peticiones.es;do eval;done;fi
	if test -r peticiones.es.idx;then makeindex peticiones.es && latex peticiones.es;fi

peticiones.es.pdf:	peticiones.es.ps
	if grep -w '\(CJK\|epsfig\)' peticiones.es.tex;then ps2pdf peticiones.es.ps;else rm -f peticiones.es.lta;if pdflatex peticiones.es;then test -f peticiones.es.lta && pdflatex peticiones.es;while tail -n 20 peticiones.es.log | grep -w references && pdflatex peticiones.es;do eval;done;fi;fi
	if test -r peticiones.es.idx;then makeindex peticiones.es && pdflatex peticiones.es;fi
peticiones.es.tty:	peticiones.es.dvi
	dvi2tty -q peticiones.es > peticiones.es.tty
peticiones.es.ps:	peticiones.es.dvi	
	dvips  peticiones.es
peticiones.es.001:	peticiones.es.dvi
	rm -f peticiones.es.[0-9][0-9][0-9]
	dvips -i -S 1  peticiones.es
peticiones.es.pbm:	peticiones.es.ps
	pstopbm peticiones.es.ps
peticiones.es.gif:	peticiones.es.ps
	pstogif peticiones.es.ps
peticiones.es.eps:	peticiones.es.dvi
	dvips -E -f peticiones.es > peticiones.es.eps
peticiones.es-01.g3n:	peticiones.es.ps
	ps2fax n peticiones.es.ps
peticiones.es-01.g3f:	peticiones.es.ps
	ps2fax f peticiones.es.ps
peticiones.es.ps.gz:	peticiones.es.ps
	gzip < peticiones.es.ps > peticiones.es.ps.gz
peticiones.es.ps.gz.uue:	peticiones.es.ps.gz
	uuencode peticiones.es.ps.gz peticiones.es.ps.gz > peticiones.es.ps.gz.uue
peticiones.es.faxsnd:	peticiones.es-01.g3n
	set -a;FAXRES=n;FILELIST=`echo peticiones.es-??.g3n`;source faxsnd main
peticiones.es_tex.ps:	
	cat peticiones.es.tex | splitlong | coco | m2ps > peticiones.es_tex.ps
peticiones.es_tex.ps.gz:	peticiones.es_tex.ps
	gzip < peticiones.es_tex.ps > peticiones.es_tex.ps.gz
peticiones.es-01.pgm:
	cat peticiones.es.ps | gs -q -sDEVICE=pgm -sOutputFile=peticiones.es-%02d.pgm -
peticiones.es/peticiones.es.html:	peticiones.es.dvi
	rm -fR peticiones.es;latex2html peticiones.es.tex
xview:	peticiones.es.dvi
	xdvi -s 8 peticiones.es &
tview:	peticiones.es.tty
	browse peticiones.es.tty 
gview:	peticiones.es.ps
	ghostview  peticiones.es.ps &
print:	peticiones.es.ps
	lpr -s -h peticiones.es.ps 
sprint:	peticiones.es.001
	for F in peticiones.es.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	peticiones.es.faxsnd
	faxsndjob view peticiones.es &
fsend:	peticiones.es.faxsnd
	faxsndjob jobs peticiones.es
viewgif:	peticiones.es.gif
	xv peticiones.es.gif &
viewpbm:	peticiones.es.pbm
	xv peticiones.es-??.pbm &
vieweps:	peticiones.es.eps
	ghostview peticiones.es.eps &	
clean:	peticiones.es.ps
	rm -f  peticiones.es-*.tex peticiones.es.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} peticiones.es-??.* peticiones.es_tex.* peticiones.es*~
peticiones.es.tgz:	clean
	set +f;LSFILES=`cat peticiones.es.ls???`;FILES=`ls peticiones.es.* $$LSFILES | sort -u`;tar czvf peticiones.es.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
