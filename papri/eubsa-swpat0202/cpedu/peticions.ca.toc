\select@language {catalan}
\contentsline {chapter}{\numberline {1}Ens preocupa que}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Per aquestes raons recomanem el seg\@umlaut {u}ent:}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Signataris}{7}{chapter.3}
\contentsline {chapter}{\numberline {A}Patents Europees de Programari: Exemples Escollits}{10}{appendix.A}
\contentsline {section}{\numberline {A.1}Exhibited Specimens}{10}{section.A.1}
\contentsline {section}{\numberline {A.2}Candidates}{16}{section.A.2}
\contentsline {chapter}{\numberline {B}Patents de Programari en Acci\'{o}}{17}{appendix.B}
\contentsline {section}{\numberline {B.1}Some Well Documented Cases}{17}{section.B.1}
\contentsline {section}{\numberline {B.2}Enlla\c {c}os anotats}{24}{section.B.2}
\contentsline {chapter}{\numberline {C}Banc de proves de s\`{e}ries d'assaigs de legislaci\'{o} de patentabilitat}{25}{appendix.C}
\contentsline {section}{\numberline {C.1}Algunes mostres de patents}{25}{section.C.1}
\contentsline {section}{\numberline {C.2}Preguntes per respondre per cada}{25}{section.C.2}
\contentsline {section}{\numberline {C.3}Comparison Table}{26}{section.C.3}
\contentsline {section}{\numberline {C.4}Legislacions candidates a ser assajades}{26}{section.C.4}
