<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Call for Action

#descr: The European Commission's proposal for the patentability of software innovations requires a clear response from the European Parliament, the member state governments and other political players. Here is what we think should be done.

#Wce: We are concerned that

#Fea: For these reasons we recommend the following:

#Sno: Signatories

#dfm: Update of 2003/09/24

#tef: The %(ep:European Patent Office) (EPO) has, in contradiction to the letter and spirit of the written law, granted tens of thousands of patents on programming and business ideas, below termed %(q:software patents).

#toi: The European Comission (CEC) is pressing ahead to legalise these patents and make them enforcable in all of Europe.  In doing so, it is disregarding the manifest will and well-argued reasoning of the vast majority of software professionals, software companies, computer scientists and economists.

#tle: The CEC is basing its %(eb:proposal) on a draft which was apparently written by %(BSA), a US based organisation dominated by a few big vendors, such as Microsoft.

#SnW: Software patents interfere with software copyright and tend to lead to the expropriation of software creators rather than to a protection of their property.  Of numerous existing %(es:economic studies), none concludes that software patents lead to more productivity, innovation, knowledge diffusion or are in any other way macro-economically beneficial.  Software patentability, as proposed by CEC/BSA, moreover leads to various %(sk:inconsistencies) within the patent system and invalidates central assumptions on which it is built.  As a result, anything becomes patentable and there can no longer be any legal security.

#Tra: The institutions of the European patent system are not in any meaningful way subjected to democratic control.  The division between legislative and judicial powers is insufficient, and in particular %(ep:the EPO seems to be a breeding ground for abusive and illegal practices).

#Uis: We urge the European Parliament and the Council to reject the directive proposal %(REF).

#RWs: We urge the European Parliament to find a way to oblige the EPO to reinstate, as far as patentability is concerned, its %(ep:examination guidelines of 1978) or an equivalent, so as to restore the correct interpretation of the EPC.

#Rue: We suggest that an independent european court should be charged with reexamining upon demand by any citizen any patents which prima facie may have been granted on the basis of an incorrect interpretation of the patentability provisions of the EPC, and that the EPO should in such cases be obliged to reimburse to the former patent owners all fees which it charged from them.

#Ett: We urge the legislators at the European and national levels to endorse the current EPC text and consider reinforcing it by modifying it according to the proposal %(URL), as far as this is deemed necessary in order to avoid misinterpretation by lawcourts.

#Ane: We propose that the European Parliament and the Council consider clarifying the limits of patentability with regard to software and logical creations by issuing a european directive along the lines of the counter-proposals %(URL).

#Dsl: We demand that any legislation (including CEC directive proposals as well as rules created by means of caselaw) concerning patentability be tested against a %(ts:test suite) of sample patent applications to see whether it would beyond any doubt lead to desired results and would not leave room for any more misinterpretations.

#Prt: We propose that the European Parliament set up a permanent Patentability Monitoring Comittee with the aim of ensuring that patents are granted only at conditions where they serve the public interest.  This Committee should be composed of MEPs and independent experts in various fields such as mathematics, informatics, natural sciences, engineering, economics, epistemology, ethics and law.  The number of patent practitioners, patent officials or other persons whose income and career is dependent on the patent community should be kept within very narrow limits (e.g. 10-20%).  The Comittee should benchmark any patent legislation as well as its interpretation by patent offices and patent courts.  Moreover it should conduct hearings, initiate case studies in the effects of the patent system and stimulate related research in the most open and inclusive possible way.  The Committee should report to the European Parliament to what extent patent reality conforms to patent theory and to the public policy goals of the European Community and its members.  The Committee would address the concerns raised by the European Parliament's Comittee on Legal Affairs and Internal Market for Quality Control in the EPO, as expressed in the discussion on the Community Patent regulation %(REF).

#AWo: We propose that the European Parliament set up a Comittee of Inquiry to investigate various allegations of irregular behaviour by the proponents of the software and gene patentability directives at the EPO and CEC, such as their close cooperation with a limited circle of lobbyists, their incoherent reasoning and their apparent contempt of democratic and legal principles, and to propose reform measures suitable for preventing this kind of phenomenon from occurring in the future.

#EdW: We expect that, as long as the problems at the EPO aren't solved, any new regulation such as the %(cp:Community Patent) will be implemented through institutions other than the EPO.

#Gen: Greens

#FrA: European Free Alliance

#MSe: Media Policy Speaker of the Green Party

#Sco: Spanish Senator, Spokesperson for the socialist group in the Senate's Committee on the Information and Knowledge Society

#Sna: Senator of the Girona region for %(ECP)

#aIo: Catalan Software Association

#mto: information society delegate of %(ICV)

#esquerra: President of the Information Policy Committee of the %(es:Catalonian Republican Party)

#eal: member of Bavarian Parliament

#isocecc: European Co-ordination Council of the Internet Society

#rut: representing 22 European ISOC chapters

#eei: Internet Branch of the French Socialist Party

#DiW: Danish Association of IT Professionals

#ATI: Spanish Association of IT Professionals

#SPECIS: French Union of Professionals of Study, Consultation, Engineering, Informatics and Services

#etW: trade union with 900,000 members

#IIIA: %(ii:Institute for Research in Artificial Intelligence) of the %(cs:Superior Council of Scientific Research) of Spain

#tsi: Faculty of Sciences of Ghent University

#dea: dean

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#mng: manager

#fud: founders

#fed: founder and owner

#CWW: CTO of %1

#kle: kernel developper

#Pcn: Product Manager

#dta: associated director

#Dee: lecturer on industrial economics at %(q:Sorbonne) University in Paris

#anc: Chairman of %(EE)

#iee: Senior member of the IEEE

#cos: economist

#nmc: informatician

#prh: Patent examiner at the Polish Patent Office

#AWm: Action against Language Regulation and Informational Obstructionism

#PWo: Philosophy of Science and Theory of Ditgital Media at Institute of Philosophy of Vienna University

#mathinst: Institute for Mathematics

#DKUUG: Danish Unix User Group

#FiL

#Aln

#hispalinux: Association of Spanish Users of GNU/Linux

#KLID: Association of Danish Commercial Linux Stakeholders

#caliu: Catalan Linux Users

#IeA: Italian Free Software Association

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal through our %(ei:new registry form).  They have thereby given us a very detailed mandate for speaking to members of the European Parliament. There is also a very simple and general %(ep:Eurolinux Petition) which you may want to sign first.

#eun: the European Parliament has meanwhile already given an adequate response.  It is now up to the %(sc:EU Council of Ministers) to support this resopnse and then to the Parliament, in a second reading, to remove remaining inconsistencies from the recitals.

#tlS: Get People to Sign

#Pnr: Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions %(el:here).

#Mth: Contact %(MT) if you want to submit signators or have further questions.

#stt: Distribute Posters

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-cpedu ;
# txtlang: en ;
# multlin: t ;
# End: ;

