# -*- mode: makefile -*-

eubsa-cpedu.en.lstex:
	lstex eubsa-cpedu.en | sort -u > eubsa-cpedu.en.lstex
eubsa-cpedu.en.mk:	eubsa-cpedu.en.lstex
	vcat /ul/prg/RC/texmake > eubsa-cpedu.en.mk


eubsa-cpedu.en.dvi:	eubsa-cpedu.en.mk
	rm -f eubsa-cpedu.en.lta
	if latex eubsa-cpedu.en;then test -f eubsa-cpedu.en.lta && latex eubsa-cpedu.en;while tail -n 20 eubsa-cpedu.en.log | grep -w references && latex eubsa-cpedu.en;do eval;done;fi
	if test -r eubsa-cpedu.en.idx;then makeindex eubsa-cpedu.en && latex eubsa-cpedu.en;fi

eubsa-cpedu.en.pdf:	eubsa-cpedu.en.ps
	if grep -w '\(CJK\|epsfig\)' eubsa-cpedu.en.tex;then ps2pdf eubsa-cpedu.en.ps;else rm -f eubsa-cpedu.en.lta;if pdflatex eubsa-cpedu.en;then test -f eubsa-cpedu.en.lta && pdflatex eubsa-cpedu.en;while tail -n 20 eubsa-cpedu.en.log | grep -w references && pdflatex eubsa-cpedu.en;do eval;done;fi;fi
	if test -r eubsa-cpedu.en.idx;then makeindex eubsa-cpedu.en && pdflatex eubsa-cpedu.en;fi
eubsa-cpedu.en.tty:	eubsa-cpedu.en.dvi
	dvi2tty -q eubsa-cpedu.en > eubsa-cpedu.en.tty
eubsa-cpedu.en.ps:	eubsa-cpedu.en.dvi	
	dvips  eubsa-cpedu.en
eubsa-cpedu.en.001:	eubsa-cpedu.en.dvi
	rm -f eubsa-cpedu.en.[0-9][0-9][0-9]
	dvips -i -S 1  eubsa-cpedu.en
eubsa-cpedu.en.pbm:	eubsa-cpedu.en.ps
	pstopbm eubsa-cpedu.en.ps
eubsa-cpedu.en.gif:	eubsa-cpedu.en.ps
	pstogif eubsa-cpedu.en.ps
eubsa-cpedu.en.eps:	eubsa-cpedu.en.dvi
	dvips -E -f eubsa-cpedu.en > eubsa-cpedu.en.eps
eubsa-cpedu.en-01.g3n:	eubsa-cpedu.en.ps
	ps2fax n eubsa-cpedu.en.ps
eubsa-cpedu.en-01.g3f:	eubsa-cpedu.en.ps
	ps2fax f eubsa-cpedu.en.ps
eubsa-cpedu.en.ps.gz:	eubsa-cpedu.en.ps
	gzip < eubsa-cpedu.en.ps > eubsa-cpedu.en.ps.gz
eubsa-cpedu.en.ps.gz.uue:	eubsa-cpedu.en.ps.gz
	uuencode eubsa-cpedu.en.ps.gz eubsa-cpedu.en.ps.gz > eubsa-cpedu.en.ps.gz.uue
eubsa-cpedu.en.faxsnd:	eubsa-cpedu.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo eubsa-cpedu.en-??.g3n`;source faxsnd main
eubsa-cpedu.en_tex.ps:	
	cat eubsa-cpedu.en.tex | splitlong | coco | m2ps > eubsa-cpedu.en_tex.ps
eubsa-cpedu.en_tex.ps.gz:	eubsa-cpedu.en_tex.ps
	gzip < eubsa-cpedu.en_tex.ps > eubsa-cpedu.en_tex.ps.gz
eubsa-cpedu.en-01.pgm:
	cat eubsa-cpedu.en.ps | gs -q -sDEVICE=pgm -sOutputFile=eubsa-cpedu.en-%02d.pgm -
eubsa-cpedu.en/eubsa-cpedu.en.html:	eubsa-cpedu.en.dvi
	rm -fR eubsa-cpedu.en;latex2html eubsa-cpedu.en.tex
xview:	eubsa-cpedu.en.dvi
	xdvi -s 8 eubsa-cpedu.en &
tview:	eubsa-cpedu.en.tty
	browse eubsa-cpedu.en.tty 
gview:	eubsa-cpedu.en.ps
	ghostview  eubsa-cpedu.en.ps &
print:	eubsa-cpedu.en.ps
	lpr -s -h eubsa-cpedu.en.ps 
sprint:	eubsa-cpedu.en.001
	for F in eubsa-cpedu.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	eubsa-cpedu.en.faxsnd
	faxsndjob view eubsa-cpedu.en &
fsend:	eubsa-cpedu.en.faxsnd
	faxsndjob jobs eubsa-cpedu.en
viewgif:	eubsa-cpedu.en.gif
	xv eubsa-cpedu.en.gif &
viewpbm:	eubsa-cpedu.en.pbm
	xv eubsa-cpedu.en-??.pbm &
vieweps:	eubsa-cpedu.en.eps
	ghostview eubsa-cpedu.en.eps &	
clean:	eubsa-cpedu.en.ps
	rm -f  eubsa-cpedu.en-*.tex eubsa-cpedu.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} eubsa-cpedu.en-??.* eubsa-cpedu.en_tex.* eubsa-cpedu.en*~
eubsa-cpedu.en.tgz:	clean
	set +f;LSFILES=`cat eubsa-cpedu.en.ls???`;FILES=`ls eubsa-cpedu.en.* $$LSFILES | sort -u`;tar czvf eubsa-cpedu.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
