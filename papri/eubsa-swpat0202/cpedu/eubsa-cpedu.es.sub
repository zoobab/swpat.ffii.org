\begin{subdocument}{eubsa-cpedu}{Peticiones de acci\'{o}n}{http://swpat.ffii.org/papri/eubsa-swpat0202/peticiones/index.es.html}{Workgroup\texmath{\backslash}\texmath{\backslash}swpatag@ffii.org\texmath{\backslash}\texmath{\backslash}versio de\_DE@euro 2003/12/16 de Jesus Gonz\'{a}lez Baharona\texmath{\backslash}footnote{http://proinnova.hispalinux.es/}}{Lista de acciones para ser realizadas por el Parlamento Europeo y otras instituciones}
\begin{sect}{prob}{Nos preocupa que}
\begin{enumerate}
\item
La Oficina Europea de Patentes\footnote{http://swpat.ffii.org/gasnu/epo/index.en.html} (OEP) ha concedido, en contradicci\'{o}n con la letra y el esp\'{\i}ritu de la ley, decenas de miles de patentes sobre programaci\'{o}n e ideas de negocio, a las que nos referiremos de ahora en adelante como  ``patentes de software''.

\item
La Comisi\'{o}n Europea (CE) est\'{a} presionando para legalizar estas patentes y obligar a su cumplimiento en toda Europa. Al hacerlo, est\'{a} despreciando las manifestaciones y los razonamientos de la amplia mayor\'{\i}a de profesionales del software, de las empresas del software, de los investigadores en software y de los economistas.

\item
La CE basa su propuesta en un borrador escrito por la Business Software Alliance\footnote{http://swpat.ffii.org/gasnu/bsa/index.en.html} (BSA), una organizaci\'{o}n basada en EE.UU. y dominada por unos pocos grandes fabricantes, como Microsoft.

\item
Las patentes de software interfieren con la aplicaci\'{o}n de los derechos de autor (copyright) al software, y tienen a la expropiaci\'{o}n de los creadores de programas en lugar de a proteger su propiedad. No existe ning\'{u}n estudio econ\'{o}mico que concluya que las patentes de software mejoran la productividad, la innovaci\'{o}n, la difusi\'{o}n del conocimiento, ni produzcan de ninguna otra forma beneficios macroecon\'{o}micos. La patentabilidad del software, tal y como est\'{a} propuesta por la CE y la BSA, conlleva sin embargo varias inconsistencias dentro del sistema de patentes, e invalida algunas de las suposiciones b\'{a}sicas sobre las que est\'{a} construido. Como consecuencia, todo pasa a ser patentable, y se acaba cualquier seguridad jur\'{\i}dica al respecto.

\item
Las instituciones del sistema europeo de patentes no est\'{a}n sujetas de ninguna forma significativa al control democr\'{a}tico. La divisi\'{o}n entre los poderes legislativo y judicial es insuficiente, y en particular la OEP parece estar abonando el terreno para las pr\'{a}cticas abusivas e ilegales\footnote{http://swpat.ffii.org/gasnu/epo/index.en.html}.
\end{enumerate}
\end{sect}

\begin{sect}{solv}{Por estas razones recomendamos lo siguiente:}
\begin{enumerate}
\item
Urgimos al Parlamento Europeo y al Consejo Europeo que rechacen la propuesta de directiva COM(2002)92 2002/0047.

\item
Urgimos al Parlamento Europeo a encontrar una forma de obligar a la OEP a reinstalar, en lo que refiere a patentabilidad, sus normas de examen de 1978\footnote{http://swpat.ffii.org/papri/epo-gl78/index.en.html} o un equivalente, de forma que se restablezca la interpretaci\'{o}n correcta de la Convenci\'{o}n Europea de Patentes (CEP).

\item
Sugerimos que un tribunal europeo independiente deber\'{\i}a encargarse de reexaminar, ante la petici\'{o}n de cualquier ciudadano, cualquier patente que a primera vista pueda haber sido concedida sobre la base de una interpretaci\'{o}n incorrecta de la CEP, y que la OEP deber\'{\i}a, en esos casos, ser obligada a reembolsar a los due\~{n}os de la patente todos los gastos que les hubiera acarreado la solicitud de esa patente.

\item
Urgimos a los legisladores, a nivel europeo y a nivel nacional, a ratificar el texto actual de la CEP y a considerar su reforzamiento modific\'{a}ndolo de acuerdo con la propuesta http://swpat.ffii.org/stidi/epc52/index.en.html, en caso de que esto sea necesario para evitar interpretaciones equivocadas en los tribunales.

\item
Proponemos que el Parlamento Europeo y el Consejo Europeo consideren la clarificaci\'{o}n de los l\'{\i}mites a la patentabilidad con respecto al software y a las creaciones l\'{o}gicas, promulgando una directiva europea que siga la propuesta http://swpat.ffii.org/papri/eubsa-swpat0202/prop/index.en.html  \&  http://swpat.ffii.org/papri/eubsa-swpat0202/prop/mini/index.en.html.

\item
Demandamos que cualquier legislaci\'{o}n (incluyendo tanto las propuestas de directiva de la CE como las reglas que se crean mediante jurisprudencia) que tenga relaci\'{o}n con la patentabilidad sea probada contra un juego de pruebas\footnote{http://swpat.ffii.org/stidi/manri/index.en.html} consistente en muestras de solicitudes de patente, para comprobar m\'{a}s all\'{a} de toda duda que produce los resultados deseados, y que no se deja espacio para m\'{a}s interpretaciones equivocadas.

\item
Proponemos que el Parlamento Europeo establezca un Comit\'{e} Permanente de Monitorizaci\'{o}n de la Patentabilidad, con el objetivo de asegurar que las patentes se conceden s\'{o}lo en condiciones que sirvan al inter\'{e}s p\'{u}blico. Este Comit\'{e} deber\'{\i}a estar compuesto por parlamentarios y expertos de varios campos, como las matem\'{a}ticas, la inform\'{a}tica, las ciencias naturales, la ingenier\'{\i}a, la econom\'{\i}a, la epistemolog\'{\i}a, la \'{e}tica y el derecho. El n\'{u}mero de juristas que litiguen habitualmente en temas de patentes, funcionarios de oficinas de patentes y otras personas cuyos ingresos y cuya carrera dependa de la comunidad relacionada con las patentes deber\'{\i}a mantenerse dentro de l\'{\i}mites estrechos (por ejemplo, 10-20\percent{}). El Comit\'{e} deber\'{\i}a contrastar cualquier legislaci\'{o}n sobre patentes, y su interpretaci\'{o}n por las oficinas y los tribunales de patentes. Adem\'{a}s, deber\'{\i}a organizar audiencias, iniciar estudios de casos sobre los efectos del sistema de patentes, y estimular la investigaci\'{o}n relacionada de la forma m\'{a}s abierta e inclusiva que sea posible. El Comit\'{e} deber\'{\i}a informar al Parlamento Europeo hasta qu\'{e} punto la realidad de las patentes coincide con la teor\'{\i}a sobre las patentes y con los objetivos de la pol\'{\i}tica p\'{u}blica de la Comunidad Europea y sus miembros. El Comit\'{e} se ocupar\'{\i}a tambi\'{e}n de los asuntos que proponga el Comit\'{e} de Asuntos Legales y Mercado Interno del Parlamento Europeo sobre el control de calidad en la OEP, como se explica en la discusi\'{o}n sobre la regulaci\'{o}n de la Patente Comunitaria COM(2000)0412.

\item
Proponemos que el Parlamento Europeo establezca un Comit\'{e} de Investigaci\'{o}n para investigar varias acusaciones de comportamiento irregular por los ponentes de las directivas sobre patentabilidad del software y de los genes, en la OEP y la CE, como su cooperaci\'{o}n cercana con un c\'{\i}rculo cerrado de influenciadores profesionales (lobbyists), su razonamiento incoherente y su aparente ignorancia de los principios legales y democr\'{a}ticos, y proponer medidas de reforma adecuadas para evitar que este fen\'{o}meno ocurra en el futuro.

\item
Esperamos que, mientras se resuelven los problemas en la OEP, cualquier nueva regulaci\'{o}n, como la Patente Comunitaria\footnote{http://www.eurolinux.org/news/cpat01B/index.es.html}, se implemente mediante instituciones distintas de la OEP.
\end{enumerate}
\end{sect}

\begin{sect}{sign}{Signatories}
\begin{center}
Harlem D\'{e}sir\footnote{http://www.harlemdesir.com} (parlamentario europeo, PSF, France)

Miquel Mayol I Raynal (parlamentario europeo, Esquerra Republicana de Catalunya\footnote{http://www.esquerra.org/}, Greens / European Free Alliance, Espa\~{n}a)

Luis Berenguer Fuster (parlamentario europeo, PSE, Espa\~{n}a)

Raina Mercedes Echerer (parlamentario europeo, Greens / European Free Alliance, Austria)

Rik Hindriks (MP, Holland, PvdA\footnote{http://www.pvda.nl/})

Jan v. Walsem (MP, Holland, Democraten 66\footnote{http://www.d66.nl/})

Grietje Bettin\footnote{http://www.sh.gruene.de:8080/MdB/Bettin/ein\_text?datum2=2002/05/13\percent{}2020\percent{}3A00\percent{}3A19\percent{}20GMT\percent{}2B2} (MdB, Media Policy Speaker of the Green Party)

F\'{e}lix Lavilla Mart\'{\i}nez (Portavoz del grupo socialista en la Comision de la Sociedad de la Informaci\'{o}n y el Conocimiento del Senado espa\~{n}ol)

Arseni Gibert (Senador per Girona de l'Entesa Catalana de Progrés\footnote{http://www.senado.es/solotexto/legis7/grupos/GRECP.html})

Carles Bonet (Senador per Girona de l'ECP, ECP\footnote{http://www.senado.es/solotexto/legis7/grupos/GRECP.html})

Softcatal\'{a}\footnote{http://www.softcatala.org/} (Catalan Software Association, contact: Jordi Mas)

Marc Rius (responsable de Societat de la Informaci\'{o} d'http://www.ic-v.org/)

Marcel Batalla i Oliv\'{e} (President de la Sectorial de la Societat de la Informaci\'{o} d'Esquerra Republicana de Catalunya\footnote{http://www.esquerra.org/})

Monica Lochner-Fischer\footnote{http://www.lochner-fischer.de/} (member of Bavarian Parliament, SPD)

European Co-ordination Council of the Internet Society\footnote{http://www.isoc-ecc.org/} (ISOC-ECC), representing 22 European ISOC chapters (contact: Patrick Vande Walle)

VOV@SPD\footnote{http://www.vov.de/} (Internet Association of the German Social Democratic Party) (contact: Axel Schudak, Arne Brand  \&  Boris Piwinger)

temPS r\'{e}els\footnote{http://www.temps-reels.net} (Internet Branch of the French Socialist Party) (contact: Thierry Noisette)

Forbundet af IT-professionelle\footnote{http://www.prosa.dk/} (PROSA.dk) (contact: Peter Ussing)

Asociaci\'{o}n de T\'{e}cnicos en Inform\'{a}tica\footnote{http://www.ati.es/} (ATI.es) (contact: Josep Mol\'{a}s: presidente de la junta directiva general)

Syndicat National Professionnel des Etudes, du Conseil, de l'Ing\'{e}nierie, de l'Informatique et des Services\footnote{http://www.specis.org/} (SPECIS, contact: GIRAUD Bernard, BGiraud@fontenay.sema.slb.com)

Comisiones Obreras\footnote{http://www.ccoo.es/} (trade union with 900,000 members, Espa\~{n}a, contact: Alberto Gonz\'{a}lez)

M.U.N.C.I.\footnote{http://forums.munci.org/viewtopic.php?t=461} (Mouvement pour une Union Nationale des Consultants en Informatique) (contact: contact at munci org)

COMFIA\footnote{http://comfia.net/} (Espa\~{n}a) (Paco Gonz\'{a}lez)

Asociaci\'{o}n de Internautas\footnote{http://www.internautas.org} (Espa\~{n}a) (Victor Domingo)

Institut d'Investigaci\'{o} en Intel$\cdot$lig\`{e}ncia Artificial\footnote{http://www.iiia.csic.es/} (IIIA) del Consejo Superior de Investigaciones Cient\'{\i}ficas (CSIC) (contact: Francesc Esteva, director)

Faculty of Sciences of Ghent University (contact: Prof. dr. Luc Moens, dean)

ESR Pollmeier GmbH\footnote{http://www.esr-pollmeier.de/swpat} (contact: Stefan Pollmeier (Managing Director))

Opera Software\footnote{http://www.opera.com/} (contact: H\aa{}kon Wium Lie (CTO))

Jetter AG\footnote{http://www.jetter.de/} (contact: Martin Jetter (CEO))

Magix AGcontact: Oliver Lorenz (Head of Legal Department)\footnote{http://www.magix.com/}

Phaidros AG (contact: Matthias Schlegel (CEO))

Myriad Software\footnote{http://www.myriad-online.com/} (contact: Guillion Didier, manager)

David Axmark \& Michael Widenius (MySQL AB\footnote{http://www.mysql.com/}, founders)

Ole Husgaard (Sparre Software\footnote{http://www.sparre.dk/}, Managing Director)

Thorsten Lemke (Lemke Software GmbH\footnote{http://www.lemkesoft.com/}, founder and owner)

Stefan Englert (Gesellschaft f\"{u}r Informatik und Produktionstechnik mbH, Managing Director)

Rodolphe Quiedeville (Lolix SA\footnote{http://www.lolix.com/}, Managing Director)

Alan Cox (CTO of Redhat\footnote{http://www.redhat.com/}, kernel developper, Linux UK\footnote{http://www.linux.org.uk/})

Easter-Eggs SA\footnote{http://www.easter-eggs.com/} (contact: Emanuel Raviart (Managing Director))

Oberon.net GmbH\footnote{http://www.oberon.net}  \&  LF.net GmbH\footnote{http://www.lf.net} (contact: Kurt Jaeger: Managing Director)

Tibosoft GmbH\footnote{http://www.tibosoft.de} (contact: Walter L\"{u}ckemann (Managing Director))

No Nonsense Software Inc\footnote{http://www.no-nonsense-software.com/} (contact: Bjarte Hetland (CEO)  \&  Jarle Aasland (Product Manager))

Jean-Louis Lespes\footnote{http://www.innovence.fr/index1.php?url=138} (Innovence\footnote{http://www.innovence.fr/}, directeur associ\'{e}, lecturer on industrial economics at ``Sorbonne'' University in Paris)

Skyrix GmbH\footnote{http://www.skyrix.com/} (contact: Jens Enders (Managing Director))

Fr\'{e}d\'{e}ric Broyd\'{e} (Chairman of Excem\footnote{http://www.eurexcem.com/}, Senior member of the IEEE)

Dr. Peter Gerwinski (G-N-U GmbH\footnote{http://www.g-n-u.de/}, Managing Director)

Intevation GmbH\footnote{http://www.intevation.de/} (contact: Bernhard Reiter  \&  Jan-Oliver Wagner)

Dr. Jean-Paul Smets-Solanes (Nexedi\footnote{http://www.nexedi.com/}, Managing Director)

Markus DeWendt (Managing Director, Open Logic Systems\footnote{http://www.Open-LS.de/})

Stephan K\"{o}rner (Managing Director, Pilum Technology GmbH)

Stephan Nobis (Managing Director, ESN GmbH\footnote{http://www.netz-administration.de})

Ulf Dunkel (invers Software Vertrieb\footnote{http://www.calamus.net/}  \&  Dunkel Software Distribution\footnote{http://www.dsd.net/}, Managing Director)

Dr. Jean-Philippe Rennard (\'{e}conomiste, expert informatique, Managing Director, Eponyme SA\footnote{http://www.eponyme.com/})

Prof. Dr. Koen De Bosschere (profesor de inform\'{a}tica, Universidad de Ghent, Belgium)

Prof. Dr. Wilfried Philips (profesor de inform\'{a}tica, Universidad de Ghent, Belgium)

Prof. Dr. Dirk Stroobandt (Universidad de Ghent)

Prof. Dr. Daniel De Zutter (Universidad de Ghent)

Prof. Dr. Jan Vandewege (Universidad de Ghent)

Prof. Dr. Ghislain Hoffman (Universidad de Ghent)

Prof. Dr. Bart Dhoedt (Universidad de Ghent)

Prof. Dr. Piet Demeester (Universidad de Ghent)

Prof. Dr. Herman Tromp (Universidad de Ghent)

Prof. Dr. Kris Coolsaet (Universidad de Ghent)

Prof. Dr. Herman Bruyninckx (profesor de inform\'{a}tica, K.U. Leuven, Belgium)

Prof. Dr. Veerle Fack (Universidad de Ghent)

Prof. Dr. Armand De Clercq (Universidad de Ghent)

Prof. Dr. Kim Mens (Universit\'{e} Catholique de Louvain)

Prof. Dr. Clemens H. Cap (profesor de inform\'{a}tica, Universidad de Rostock, Alema\~{n}a)

Dr. Peter Bienstman (researcher, Universidad de Ghent)

Dr. Michiel Ronsse (researcher, Universidad de Ghent)

Dr. Karl-Friedrich Lenz\footnote{http://www.k.lenz.name/} (Professor (ky\^{o}ju) f\"{u}r Deutsches und Europ\"{a}isches Recht, Univ. Aoyama Gakuin, Japan)

Jozef Halbersztadt (ejaminador de patentes en la Oficina Polacca de Patentes, Warszawa)

Prof. Dr. iur. Christian Gizewski (Universidad de Tecnica de Berlin, Aktion gegen \"{O}ffentliche Sprachregelungen und Kommunikationserschwernisse\footnote{http://www.tu-berlin.de/fb1/AGiW/Cricetus/SOzuC1/Aktion.htm})

Prof. Dr. Herbert Hrachovec (Philosophy of Science and Theory of Ditgital Media at Institute of Philosophy of Vienna University)

Prof. Dr. Charles Durand (profesor de Universidad de Tecnica de Belfort-Montb\'{e}liard)

Dr. Frank Dittmann (Gesellschaft f\"{u}r Kybernetik e.V.\footnote{http://www.gesellschaft-fuer-kybernetik.org/})

Dr. Gregor Nickel (Institut f\"{u}r Mathematik, Universidad de T\"{u}bingen)

Koenraad Vandenborre (consultant, inno.com\footnote{http://www.inno.com})

Steven Noels (consultant, Outerthought.org\footnote{http://www.Outerthought.org})

Marc Portier (consultant, Outerthought.org\footnote{http://www.Outerthought.org})

Jan Van den Bergh (consultant, inno.com\footnote{http://www.inno.com})

Geert Premereur (consultant, inno.com\footnote{http://www.inno.com})

Europe Shareware\footnote{http://www.europe-shareware.org/} (contact: Pascal Ricard  \&  Sylvain Perchaud)

German Unix User Group\footnote{http://www.guug.de/} (GUUG.de, contact: Christian Lademann, vice president)

Dansk UNIX-system Bruger Gruppe\footnote{http://www.dkuug.org/} (DKUUG.dk, contact: Ulf Nielsen)

Asociaci\'{o}n de Usuarios de Linux y Software Libre de Habla Francesa\footnote{http://www.aful.org} (AFUL.org, contact: St\'{e}fane Fermigier  \&  Bernard Lang)

Association para la Investigaci\'{o}n en Inform\'{a}tica Libre\footnote{http://www.april.org} (APRIL.org, contact: Fr\'{e}d\'{e}ric Couchet  \&  Odile B\'{e}nassy)

Asociaci\'{o}n de Usuarios Espa\~{n}oles de GNU/Linux\footnote{http://www.hispalinux.es/} (HispaliNUX, contact: Juan Tom\'{a}s Garcia, president)

(contact: RA Dipl.-Phys. J\"{u}rgen Siepmann  \&  Daniel Riek)

Komercielle Linux-interssenter I Danmark\footnote{http://www.klid.dk} (KLID.dk, contact: Keld Simonsen)

Catalan Linux Users\footnote{http://www.caliu.org/Caliu/Patents/} (CALIU)

Free Software Foundation Europe\footnote{http://www.fsfeurope.org/} (contact: Georg Greve, president)

信息共和促進協會\footnote{http://www.ffii.org/index.de.html} (FFII.org)

Europa-Klub e.V.\footnote{http://www.europaklub.de} (contact: Prof. on. Siegfried Piotrowski)

Association Bordelaise des Utilisateurs de Logiciels Libres (ABUL.org, contact: Fran\c{c}ois Pellegrini)

SSLUG.dk\footnote{http://www.sslug.dk} (contact: Anne \O{}stergaard  \&  Erik Josefsson)

Assoziazione Software Libero\footnote{http://www.software-libero.it/} (ASSOLI, contact: Simone Piccardi, Paolo Didone  \&  Alessandro Rubini)

Vereniging Open Source Nederland\footnote{http://www.vosn.nl/patenten} (contact: Dr. Luuk van Dijk)

Jesus González Baharona (ProInnova\footnote{http://proinnova.hispalinux.es/})

Asociaci\'{o}n de Software Libre de Le\'{o}n\footnote{http://www.sleon.org/} (contact: Sergio Gonz\'{a}lez)

Softwarepatenter.dk\footnote{http:www.softwarepatenter.dk} (Carsten Svaneborg)

Erik Lange (MM Manager\footnote{http://www.mmanager.com/})

Janus Sandsgaard (Netcetera\footnote{http://www.netcetera.dk})

Ole Tange  \&  Jesper M.Jensen (Linux-Kurser\footnote{http://www.linux-kurser.dk/})

more signatures\footnote{http://www.ffii.org/verein/cmima/index.en.html/eubsa-list.html} (14266 persons have so far signed this appeal through our new registry form.  They have thereby given us a very detailed mandate for speaking to members of the European Parliament. There is also a very simple and general Eurolinux Petition which you may want to sign first.)
\end{center}
\end{sect}

\begin{sect}{plinov}{Update of 2003/09/24}
the European Parliament has meanwhile already given an adequate response.  It is now up to the EU Council of Ministers\footnote{http://swpat.ffii.org/gasnu/consilium/index.en.html} to support this resopnse and then to the Parliament, in a second reading, to remove remaining inconsistencies from the recitals.

see also Il Parlamento Europeo Vota per i limiti Reali nella Brevettabilit\`{a}.\footnote{http://swpat.ffii.org/lisri/03/plen0924/index.en.html}
\end{sect}

\begin{sect}{tasks}{Questions, Things To Do, How you can Help}
\begin{itemize}
\item
{\bf {\bf Comment vous pouvez nous aider a mettre fin au cauchemare des brevets logiciels\footnote{http://swpat.ffii.org/girzu/gunka/index.en.html}}}
\filbreak

\item
{\bf {\bf Get People to Sign}}

\begin{quote}
Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions here\footnote{http://www.eurolinux.org/index.es.html}.

Contact cpedu-help@ffii.org if you want to submit signators or have further questions.
\end{quote}
\filbreak

\item
{\bf {\bf Distribute Posters}}

\begin{quote}
\begin{itemize}
\item
Of course you can breathe freely\footnote{http://www.vrijschrift.org/brussel/poster\_eu\_patents\_of\_course\_you\_can\_breathe\_freely.png}

\item
In the Heaven of Gates\footnote{http://www.vrijschrift.org/brussel/poster\_eu\_patents\_in\_the\_heaven\_of\_gates.png}
\end{itemize}
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\appendix

\subinputs{}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: latex ;
% End: ;

