\contentsline {chapter}{\numberline {1}We are concerned that}{5}{chapter.1}
\contentsline {chapter}{\numberline {2}For these reasons we recommend the following:}{6}{chapter.2}
\contentsline {chapter}{\numberline {3}Signatories}{8}{chapter.3}
\contentsline {chapter}{\numberline {A}European Software Patents: Assorted Examples}{11}{appendix.A}
\contentsline {section}{\numberline {A.1}Exhibited Specimens}{11}{section.A.1}
\contentsline {section}{\numberline {A.2}Candidates}{17}{section.A.2}
\contentsline {chapter}{\numberline {B}Software Patents in Action}{18}{appendix.B}
\contentsline {section}{\numberline {B.1}Some Well Documented Cases}{18}{section.B.1}
\contentsline {section}{\numberline {B.2}Annotated Links}{25}{section.B.2}
\contentsline {chapter}{\numberline {C}Quotations on the question of the patentability of rules of organisation and calculation}{26}{appendix.C}
\contentsline {section}{\numberline {C.1}Software \& Technology Practitioners}{26}{section.C.1}
\contentsline {section}{\numberline {C.2}Law Scholars, Judges, Patent Practitioners}{35}{section.C.2}
\contentsline {section}{\numberline {C.3}Economists}{44}{section.C.3}
\contentsline {section}{\numberline {C.4}Mathematicians, Philosophers, Generalists}{50}{section.C.4}
\contentsline {section}{\numberline {C.5}Politicians}{51}{section.C.5}
\contentsline {section}{\numberline {C.6}Patent strategists}{60}{section.C.6}
\contentsline {section}{\numberline {C.7}Annotated Links}{61}{section.C.7}
\contentsline {section}{\numberline {C.8}Questions, Things To Do, How you can Help}{61}{section.C.8}
\contentsline {chapter}{\numberline {D}Patentability Legislation Benchmarking Test Suite}{62}{appendix.D}
\contentsline {section}{\numberline {D.1}Some sample patents}{62}{section.D.1}
\contentsline {section}{\numberline {D.2}Questions to be answered for each}{62}{section.D.2}
\contentsline {section}{\numberline {D.3}Comparison Table}{63}{section.D.3}
\contentsline {section}{\numberline {D.4}Legislation candidates to be tested}{63}{section.D.4}
\contentsline {chapter}{\numberline {E}The BSA/CEC Proposal(s) and EPC-based Counter-Proposal}{64}{appendix.E}
\contentsline {section}{\numberline {E.1}Preamble}{64}{section.E.1}
\contentsline {section}{\numberline {E.2}The Articles}{71}{section.E.2}
\contentsline {section}{\numberline {E.3}Rationale}{78}{section.E.3}
\contentsline {section}{\numberline {E.4}Annotated Links}{79}{section.E.4}
\contentsline {chapter}{\numberline {F}European Patent Office: High Above Legality}{80}{appendix.F}
\contentsline {section}{\numberline {F.1}The special legal construction of the EPO}{81}{section.F.1}
\contentsline {section}{\numberline {F.2}Dissolving the invention concept of Art 52 EPC}{81}{section.F.2}
\contentsline {section}{\numberline {F.3}Disrespecting the Exceptions of Art 53 EPC}{81}{section.F.3}
\contentsline {section}{\numberline {F.4}Eroding the novelty concept of Art 54 EPC}{81}{section.F.4}
\contentsline {section}{\numberline {F.5}Formal view of Art 56 EPC: Lowest Non-Obviousness Standards of the World}{82}{section.F.5}
\contentsline {section}{\numberline {F.6}Continuously deteriorating examination quality}{82}{section.F.6}
\contentsline {section}{\numberline {F.7}BEST: assured worst examination quality, illegally introduced, legalised in 2000}{83}{section.F.7}
\contentsline {section}{\numberline {F.8}Novelty search problems increasingly complex, EPO lagging far behind}{84}{section.F.8}
\contentsline {section}{\numberline {F.9}Outlaw zone, poor working climate}{84}{section.F.9}
\contentsline {section}{\numberline {F.10}National controllers of the EPO aspiring for careers in EPO}{85}{section.F.10}
\contentsline {section}{\numberline {F.11}EPO Boards of Appeal lack Independence}{85}{section.F.11}
\contentsline {section}{\numberline {F.12}Withholding Patent Information from the Public}{86}{section.F.12}
\contentsline {section}{\numberline {F.13}Questions, Things To Do, How you can Help}{86}{section.F.13}
