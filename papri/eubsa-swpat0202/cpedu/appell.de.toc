\contentsline {chapter}{\numberline {1}Folgendes bereitet uns Sorge}{5}{chapter.1}
\contentsline {chapter}{\numberline {2}Daher empfehlen wir folgende Ma{\ss }nahmen}{6}{chapter.2}
\contentsline {chapter}{\numberline {3}Unterzeichner}{8}{chapter.3}
\contentsline {chapter}{\numberline {A}Europ\"{a}ische Softwarepatente: Einige Musterexemplare}{11}{appendix.A}
\contentsline {section}{\numberline {A.1}Ausstellungsst\"{u}cke}{11}{section.A.1}
\contentsline {section}{\numberline {A.2}Kandidaten}{19}{section.A.2}
\contentsline {chapter}{\numberline {B}Softwarepatente in Aktion}{20}{appendix.B}
\contentsline {section}{\numberline {B.1}Einige gut dokumentierte F\"{a}lle}{20}{section.B.1}
\contentsline {section}{\numberline {B.2}Kommentierte Verweise}{27}{section.B.2}
\contentsline {chapter}{\numberline {C}Zitate zur Frage der Patentierbarkeit von computer-implementierten Organisations- und Rechenregeln}{28}{appendix.C}
\contentsline {section}{\numberline {C.1}Softwerker und Techniker}{28}{section.C.1}
\contentsline {section}{\numberline {C.2}Rechtsgelehrte, Richter}{37}{section.C.2}
\contentsline {section}{\numberline {C.3}Wirtschaftswissenschaftler}{47}{section.C.3}
\contentsline {section}{\numberline {C.4}Mathematiker, Geisteswissenschaftler, Generalisten}{53}{section.C.4}
\contentsline {section}{\numberline {C.5}Politiker}{53}{section.C.5}
\contentsline {section}{\numberline {C.6}Patentstrategen}{63}{section.C.6}
\contentsline {section}{\numberline {C.7}Kommentierte Verweise}{64}{section.C.7}
\contentsline {section}{\numberline {C.8}Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}{64}{section.C.8}
\contentsline {chapter}{\numberline {D}Testsuite f\"{u}r die Gesetzgebung \"{u}ber die Grenzen der Patentierbarkeit}{65}{appendix.D}
\contentsline {section}{\numberline {D.1}Einige Beispielpatente}{65}{section.D.1}
\contentsline {section}{\numberline {D.2}Fragen, die f\"{u}r jedes Patent zu beantworten sind}{65}{section.D.2}
\contentsline {section}{\numberline {D.3}Vergleichstabelle}{66}{section.D.3}
\contentsline {section}{\numberline {D.4}Zu pr\"{u}fende Kandidaten: Gesetzesvorschl\"{a}ge von EUK/BSA, FFII/Eurolinux u.a.}{66}{section.D.4}
\contentsline {chapter}{\numberline {E}Entw\"{u}rfe von BSA/EUK und EP\"{U}-basierter Gegenentwurf}{67}{appendix.E}
\contentsline {section}{\numberline {E.1}Pr\"{a}ambel}{67}{section.E.1}
\contentsline {section}{\numberline {E.2}Die Artikel}{75}{section.E.2}
\contentsline {section}{\numberline {E.3}Zugrundeliegende Erw\"{a}gungen}{82}{section.E.3}
\contentsline {section}{\numberline {E.4}Kommentierte Verweise}{83}{section.E.4}
\contentsline {chapter}{\numberline {F}Europ\"{a}isches Patentamt: Hoch \"{u}ber dem Gesetz}{84}{appendix.F}
\contentsline {section}{\numberline {F.1}Das EPA: eine besondere Rechtskonstruktion}{85}{section.F.1}
\contentsline {section}{\numberline {F.2}Aufl\"{o}sung des Erfindungsbegriff von Art 52 EP\"{U}}{85}{section.F.2}
\contentsline {section}{\numberline {F.3}Missachtung der Patentierbarkeitsausnahmen von Art 53 EP\"{U}}{85}{section.F.3}
\contentsline {section}{\numberline {F.4}Aufweichung des Neuheitsbegriffs von Art 54 EP\"{U}}{85}{section.F.4}
\contentsline {section}{\numberline {F.5}Formelle Sicht von Art 52 EP\"{U}: Niedrigste Erfindungsh\"{o}he der Welt}{86}{section.F.5}
\contentsline {section}{\numberline {F.6}St\"{a}ndig sinkende Pr\"{u}fungsqualit\"{a}t}{86}{section.F.6}
\contentsline {section}{\numberline {F.7}BEST: schlechtestm\"{o}gliche Pr\"{u}fungsqualit\"{a}t, gesetzeswidrig eingef\"{u}hrt, 2000 legalisiert}{87}{section.F.7}
\contentsline {section}{\numberline {F.8}Probleme der Neuheitsrecherche zunehmend komplex, EPA weit abgeschlagen}{88}{section.F.8}
\contentsline {section}{\numberline {F.9}Rechtsfreier Raum, schlechtes Arbeitsklima}{88}{section.F.9}
\contentsline {section}{\numberline {F.10}Karrieren zwischen \"{U}berwachenden Organen und EPA}{89}{section.F.10}
\contentsline {section}{\numberline {F.11}Unabh\"{a}ngigkeit der Beschwerdekammern nicht gew\"{a}hrleistet}{89}{section.F.11}
\contentsline {section}{\numberline {F.12}Vorenthaltung von Patentinformationen.}{90}{section.F.12}
\contentsline {section}{\numberline {F.13}Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}{90}{section.F.13}
