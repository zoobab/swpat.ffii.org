<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Richiesta d'azione

#descr: La proposta della Commissione Europea sulla brevettabilità delle innovazioni nel software richiede che il Parlamento Europeo, i governi degli stati membri ed altre figure politiche diano una chiara risposta. Qui è riportato ciò che si ritiene debba esser fatto.

#Wce: Siamo preoccupati dal fatto che

#Fea: Per queste ragioni, raccomandiamo ciò che segue:

#Sno: Firmatari

#dfm: Update of 2003/09/24

#tef: l'%(ep:Ufficio Europeo per i Brevetti) (UEB), in contraddizione con il testo e lo spirito della legge, abbia garantito decine di migliaia di brevetti sulle idee riguardanti la programmazione e gli affari, che chiameremo %(q:brevetti sul software).

#toi: La Comissione Europea (CEC) sta esercitando pressioni affinché questi brevetti siano legalizzati e resi applicabili in tutta Europa. Nel fare ciò, la CEC sta ignorando il chiaro e ben argomentato appello della grande maggioranza di professionisti del software, compagnie, scenziati ed economi.

#tle: La CEC basa la sua %(eb:proposta) su una bozza di documento scritta apparentemente dalla %(BSA), una organizzazione statunitense guidata da poche grandi compagnie, come Microsoft, che ha un considevole interesse su tale argomento, dato che attualmente il 60% dei brevetti per il software accordati dall'UEB sono detenuti da compagnie statunitensi.

#SnW: I brevetti sul software interferiscono con il diritto d'autore su questo e per i creatori di software tendono a portare all'espropriazione piuttosto che alla protezione della loro proprietà. Dei numerosi %(es:studi economici) esistenti, nessuno conclude che i brevetti sul software portino ad una maggiore produttività, innovazione, diffusione del sapere o siano, in qualche altro modo, macro-economicamente vantaggiosi. La brevettabilità del software proposta da CEC/BSA, inoltre, porta a diverse %(sk:inconsistenze) all'interno del sistema dei brevetti e annulla le centrali assunzioni su cui si basa. Come risultato, ogni cosa diventa brevettabile e non ci può più essere alcuna sicurezza legale.

#Tra: Le istituzioni del sistema europeo dei brevetti non sono in alcun modo soggette sigificativamente ad un controllo democratico. La divisione tra potere legislativo e giudiziario non è sufficiente ed in particolare %(ep:l'UEB sembra essere terreno fertile per gli abusi e la pratica dell'illegalità).

#Uis: Sollecitiamo il Parlamento ed il Consiglio europeo a rifiutare proposta de direttiva %(REF).

#RWs: Sollecitiamo il Parlamento Europeo a trovare un modo per obbligare l'UEB a rifondarsi, per come è intesa la brevettabilità, sulle sue %(ep:linee guida d'indagine del 1978) o un equivalente, in modo da reinstaurare la corretta interpretazione della CBE.

#Rue: Suggeriamo che un tribunale europeo indipendente sia obbligato a riesaminare su richiesta di un qualunque cittadino un qualsiasi brevetto che a prima vista possa sembrare accordato sulla base di una scorretta interpretazione delle direttive sulla brevettabilità dell'EPC, e che l'UEB, in tali casi, sia obbligata a rimborsare ai precedenti detentori del brevetto tutte le tasse da loro pagate.

#Ett: Sollecitiamo i legislatori, sia a livello europeo che nazionale, affinché approvino il corrente testo dell'EPC e considerino la sua riapplicazione in accordo alla proposta %(URL), ciò fino a quando sarà ritenuto necessario, in modo da evitare interpretazioni scorrette da parte dei tribunali.

#Ane: Proponiamo che il Parlamento ed il Consiglio Europeo considerino di rendere palesi i limiti della brevettabilità nel caso del software e delle creazioni dell'ingegno emanando una direttiva europea secondo le linee delle contra-proposte %(URL).

#Dsl: Chiediamo che ogni proposta di legge (incluse le proposte della direttiva CEC e le regole create dai precedenti giuridici) riguardante la brevettabilità sia verificata attraverso un %(ts:sistema di prove) costituito da esempi di applicazione del brevetto, in modo da vedere al di là di ogni dubbio se ciò porterà effettivamente i risultati desiderati e non lascerà spazio ad alcuna interpretazione sbagliata.

#Prt: Proponiamo che il Parlamento Europeo crei un Comitato Permanente sulla Brevettabilità, con lo scopo di assicurare che i brevetti siano accordati solo nelle condizioni in cui questi siano vadano nella direzione del pubblico interesse. Questo comitato dovrebbe essere composto da persone del MEP ed indipendenti, esperti in vari campi dell'ingegno quali matematica, informatica, scienze naturali, ingegneria, economia, epistemiologia, etica e giurisprudenza. Il numero dei detentori di brevetti, funzionari dell'ambiente o altre persone le cui entrate e carriere dipendano dalla comunità dei brevetti, deve essere mantenuto esiguo (ad esempio il 10-20%). Il comitato dovrà controllare ogni legge sui brevetti così come le interpretazione che gli uffici brevetti e i tribunali ne faranno. Inoltre dovrà istituire incontri, proporre studi specifici sugli effetti del sistema dei brevetti  e stimolare una ricerca correlata nel modo più aperto e inclusivo possibile. Il comitato dovrebbe segnalare al Parlamento Europeo in che misura la realtà  dei brevetti é conforme alla teoria ed agli obiettivi di politica pubblica della Comunitá  Europea e dei relativi membri. Il lavoro di questo comitato dovrà rivolgersi verso le preoccupazioni sollevate dal Comitato del Parlamento Europeo per gli Affari Legali ed il Mercato Interno per il Controllo di Qualità nell'UEB, come espresso nella discussione sulla regoloamentazione comunitaria sui brevetti %(REF).

#AWo: Proponiamo che il Parlamento Europeo crei un Comitato d'inchiesta per investigare sulle varie accuse di comportamenti irregolari tenuti da coloro che propongono le direttive sulla brevettabilità  del software e delle opere d'ingegno all'UEB ed al CEC, come  la loro stretta collaborazione con una limitata cerchia di potenze, il loro ragionare incoerente ed il loro apparente disprezzo dei princìpi democratici e legali, e di proporre misure per una riforma in modo da prevenire il ritorno di questi fenomeni nel futuro

#EdW: Riteniamo che, almeno fino a quando i problemi nell'UEB non saranno risolti, ogni nuova regolamentazione, come %(cp:Brevetto comunitario), sia implementata attraverso istituzioni differenti dall'UEB.

#Gen: Verdi

#FrA: Allianza Libera Europea

#MSe: responsabile per informatica del Partito Verde

#Sco: Spanish Senator, Spokesperson for the socialist group in the Senate's Committee on the Information and Knowledge Society

#Sna: senatore per la regione Girona de %(ECP)

#aIo: Catalan Software Association

#mto: responsable de Societat de la Informació d'%(ICV)

#esquerra: President de la Sectorial de la Societat de la Informació d'%(es:Esquerra Republicana de Catalunya)

#eal: Membro del Parlamento Bavarese

#isocecc: European Co-ordination Council of the Internet Society

#rut: representing 22 European ISOC chapters

#eei: Internet Branch of the French Socialist Party

#DiW: Forbundet af IT-professionelle

#ATI: Asociación de Técnicos en Informática

#SPECIS: Syndicat National Professionnel des Etudes, du Conseil, de l'Ingénierie, de l'Informatique et des Services

#etW: trade union with 900,000 members

#IIIA: %(ii:Institut d'Investigació en Intel·ligència Artificial) del %(cs:Consejo Superior de Investigaciones Científicas)

#tsi: Faculty of Sciences of Ghent University

#dea: dean

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#mng: manager

#fud: founders

#fed: founder and owner

#CWW: CTO of %1

#kle: kernel developper

#Pcn: Product Manager

#dta: directeur associé

#Dee: lecturer on industrial economics at %(q:Sorbonne) University in Paris

#anc: Chairman of %(EE)

#iee: Senior member of the IEEE

#cos: économiste

#nmc: expert informatique

#prh: esaminatore de brevetti nell'Ufficio Polacco de Brevetti

#AWm: Azzione Contro Regulazione Linguistica é Ostruzzionsimo Informatico

#PWo: Philosophy of Science and Theory of Ditgital Media at Institute of Philosophy of Vienna University

#mathinst: Institut für Mathematik

#DKUUG: Dansk UNIX-system Bruger Gruppe

#FiL

#Aln

#hispalinux: Asociación de Usuarios Españoles de GNU/Linux

#KLID: Komercielle Linux-interssenter I Danmark

#caliu: Catalan Linux Users

#IeA: Assoziazione Software Libero

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal through our %(ei:new registry form).  They have thereby given us a very detailed mandate for speaking to members of the European Parliament. There is also a very simple and general %(ep:Eurolinux Petition) which you may want to sign first.

#eun: the European Parliament has meanwhile already given an adequate response.  It is now up to the %(sc:EU Council of Ministers) to support this resopnse and then to the Parliament, in a second reading, to remove remaining inconsistencies from the recitals.

#tlS: Get People to Sign

#Pnr: Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions %(el:here).

#Mth: Contact %(MT) if you want to submit signators or have further questions.

#stt: Distribute Posters

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: assoli ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-cpedu ;
# txtlang: it ;
# multlin: t ;
# End: ;

