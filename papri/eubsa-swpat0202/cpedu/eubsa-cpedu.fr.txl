<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Propositions de mesures

#descr: La proposition de directive de la Commission Européenne sur la Brevetabilité des Innovations Informatiques nécessite une réponse du Parlement Européen, des gouvernements des États membres et autres acteurs politiques. Voici nos propositions.

#Wce: Nous sommes préoccupés par les faits suivants

#Fea: Pour remédier à cela, nous proposons les mesures suivantes

#Sno: Signataires

#dfm: Mise à jour du 2003/09/24

#tef: L'%(ep:Office Européen des Brevets) (OEB) a, en contradiction tant avec la lettre qu'avec l'esprit de la loi, accordé plusieurs dizaines de milliers de brevets portant sur des idées relatives aux logiciels et aux méthodes commerciales, que nous appellerons ci-dessous %(q:brevets logiciels).

#toi: La Commission Européenne (CEC) oeuvre à la légalisation de ces brevets, afin de les rendre valides dans toute l'Europe. En agissant de la sorte, elle méprise le souhait manifeste et le raisonnement argumenté de la grande majorité des professionnels de l'informatique, ainsi que des économistes.

#tle: La CCE a basé sa %(eb:proposition) sur un document qui a apparemment été écrit par la %(BSA), une organisation étasunienne dominée par de grandes entreprises telles que Microsoft.

#SnW: Les brevets logiciels interfèrent avec le droit d'auteur et le copygight, et tendent à l'expropriation des créateurs de logiciels plutôt qu'à la protection de leur propriété. Aucune des nombreuses %(es:études économiques) n'a pu montrer que les brevets logiciels induisent plus de productivité, d'innovation, de diffusion du savoir ou sont de quelque autre manière bénéfiques sur le plan macro-économique. La brevetabilité logicielle, telle que proposée par la CEC/BSA, crée qui plus est de nombreuses %(sk:incohérences) au sein du système de brevets et invalide certains des fondements sur lesquels il est construit. Il en résulte que tout peut être brevetable et qu'il ne peut plus exister de sécurité juridique.

#Tra: Les institutions participant au système de brevet européen ne sont pas soumises à un contrôle démocratique suffisant. La séparation entre pouvoirs législatif et judiciaire est insuffisante ; en particulier, %(ep:l'OEB semble être à l'origine de nombreuses pratiques abusives et illégales).

#Uis: Nous pressons le Parlement et le Conseil Européen de rejeter la proposition de directive %(REF).

#RWs: Nous incitons le Parlement Européen à trouver un moyen de contraindre l'OEB de revenir, en ce qui concerne la brevetabilité, à ses %(ep:règles d'examen de 1978) ou à leur équivalent, de façon à rétablir une interprétation correcte de la Convention Européenne des Brevets (CEB).

#Rue: Nous suggérons qu'une commission européenne indépendante soit chargée de rééxaminer, à la demande de tout citoyen, tout brevet qui aurait été délivré sur la base d'une interprétation incorrecte de la CEB, et que l'OEB soit dans ce cas contraint à rembourser les anciens titulaires des brevets invalides de tous les frais qu'ils avaient dû acquiter.

#Ett: Nous incitons les législateurs, tant au niveau national qu'européen, à soutenir le texte actuel de la CEB et à le renforcer en le modifiant dans le sens de notre proposition %(URL), autant qu'il est nécessaire afin d'éviter des erreurs d'interprétations de la part des tribunaux.

#Ane: Nous proposons que le Parlement et le Conseil Européens clarifient les limites de la brevetabilité en ce qui concerne les créations logicielles et intellectuelles en promulgant une directive européenne allant dans le sens des contre-propositions %(URL).

#Dsl: Nous demandons que toute réglementation (incluant les propositions de directive de la CEC aussi bien que les règles créées par la jurisprudence) concernant la brevetabilité soit testée au moyen d'un %(ts:jeu de test) de demandes de brevets types afin de vérifier qu'elle produit sans doute aucun les effets désirés et ne laisse pas de place aux erreurs d'interprétation.

#Prt: Nous proposons que le Parlement Européen mette en place un Comité permanent de Surveillance de la Brevetabilité dans le but de s'assurer que les brevets ne soient délivrés que dans des conditions où ils servent l'intérêt général. Ce comité serait composé de Parlementaires Européens et d'experts indépendants appartenant à des domaines divers comme les mathématiques, l'informatique, les sciences naturelles, l'ingéniérie, l'économie, l'épistémologie, l'éthique et le droit. Le nombre de conseils en brevets, de représentants des offices de brevets, ou de personnes dont les revenus ou la carrière dépendent du système des brevets devrait être maintenu dans des limites très strictes (par exemple 10-20%). Le Comité évaluerait toute réglementation relative aux brevets ainsi que son interprétation par les offices de brevets et les tribunaux. De plus, il mènerait des auditions, initierait des études de cas sur les effets du système des brevets, et stimulerait la recherche de la façon la plus ouverte et la plus large possible. Le Comité rapporterait au Parlement Européen la manière dont la réalité des brevets se conforme à la théorie qui les sous-tend et aux objectifs d'intérêt général de la Communauté Européenne et de ses membres. Le Comité prendrait en charge les inquiétudes levées par le Comité du Parlement Européen sur les Questions Légales et le Marché Intérieur pour le Contrôle de la Qualité à l'OEB, telles qu'exprimées au cours de la discussion sur la réglementation de Brevet Communautaire %(REF)

#AWo: Nous proposons que le Parlement Européen crée une Commission d'Enquête afin de vérifier le bien fondé de nombreuses allégations de comportement irrégulier de la part des initiateurs des directives sur la brevetabilité des logiciels et des gènes à l'OEB et à la CEC, ainsi que de leur étroite collaboration avec un cercle restreint de lobbyistes, de leur raisonnement incohérent et de leur apparent mépris des principes légaux et démocratique, et afin de proposer des mesures de réforme destinées à éviter que ce phénomène ne se reproduise à l'avenir.

#EdW: Nous souhaitons que, tant que les problèmes mis en évidence à l'OEB ne sont pas résolus, toute nouvelle réglementation,  comme celle traitant du %(cp:Brevet Communautaire), soit mise en oeuvre par des institutions autres que l'OEB.

#Gen: Verts

#FrA: Alliance Libre Européenne

#MSe: Porte-Parole Média et Informatique du Parti Vert

#Sco: Sénateur Espagnol, Porte Parole du Groupe Socialiste dans la Commission sur la Société de Information et Savoir

#Sna: sénateur pour la région Girona de l'%(ECP)

#aIo: Catalan Software Association

#mto: délégué de Société d'Information de %(ICV)

#esquerra: Président du Comité Société d'Information du %(es:Parti Republicain de la Catalogne)

#eal: Membre du Parlament Bavarois

#isocecc: Commission de Coordination Européenne de ISOC

#rut: en representant 21 sections européennes de ISOC

#eei: Section Internet du Parti Socialiste Français

#DiW: Association Danoise des Professionels de l'Informatique

#ATI: Association Espagnole des Informaticiens

#SPECIS: Syndicat National Professionnel des Etudes, du Conseil, de l'Ingénierie, de l'Informatique et des Services

#etW: Syndicat à 900000 membres

#IIIA: %(ii:Institut pour la Recherche en Intelligence Artificielle) du %(cs:Conseil Supérieur pour la Recherche Scientifique) de l'Espagne

#tsi: Faculté des Sciences de l'Université de Ghent

#dea: dean

#dat: Département Juridique

#Mic: Matthias Schlegel

#mng: manager

#fud: fondateurs

#fed: fondateur et propriétaire

#CWW: CTO of %1

#kle: kernel developper

#Pcn: Gérant de Produits

#dta: directeur associé

#Dee: professeur d'économie industrielle à l'Université %(q:La Sorbonne) de Paris

#anc: Chairman of %(EE)

#iee: Senior member of the IEEE

#cos: économiste

#nmc: expert informatique

#prh: examinateur de brevets dans l'Office Polonais de Brevets

#AWm: Action Contre Réglement Linguistique et Obstructionism Informatique

#PWo: Philosophie de Science et Théorie des Média Digitales auprès de l'Institut de Philosophie de l'Université de Vienne

#mathinst: Institut de Mathématique

#DKUUG: Association Danoise des Utilisateurs de systèmes Unix

#FiL

#Aln

#hispalinux: Association des Utilisateurs Españols de GNU/Linux

#KLID: Association des Intéressé Commerciaux en Linux

#caliu: Association des Utilisateurs de GNU/Linux en Langue Catalonienne

#IeA: Association Italienne pour le Logiciel Libre

#rgr: autres signataires

#gcr: %(N) personnes ont jusqu'au présent signé cet appel à travers d'un %(ei:formulaire de régistration) en langue allemande), ainsi nous donnant un mandat très claire et détaillé pour nôtre travail auprès du Parlament Européen.  Vous voudriez peut-être aussi signer la %(ep:Pétition Eurolinux) qui est écrite en termes plus simples et généraux.

#eun: Le Parlament a déja donné une reponse adéquate.  Maintenant le %(sc:Conséil de Ministre de l'UE) doit soutenir cette reponse, et la Parlament doit la perfectionner dans la seconde lecture.

#tlS: Obtenir des Signataires

#Pnr: Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions %(el:here).

#Mth: Contact %(MT) if you want to submit signators or have further questions.

#stt: Distribute Posters

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: pelegrin ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-cpedu ;
# txtlang: fr ;
# multlin: t ;
# End: ;

