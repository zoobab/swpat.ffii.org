<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Handlingsforslag og krav

#descr: Forslaget fra den Europæiske Kommission om patenterbarhed for computerrelaterede opfindelser kræver et tydeligt svar fra Europaparlamentet, medlemslandenes regeringer og andre politiske aktører. Her forklarer vi, hvad vi mener der skal gøres.

#Wce: Vi er alvorligt bekymrede over

#Fea: Derfor anbefaler vi at følgende vedtages:

#Sno: Undertegnere

#dfm: Update of 2003/09/24

#tef: %(ep:Den Europæiske Patentmyndighed) (EPO) har modsat lovens bogstav og mening godkendt patenter for titusindevis af programmeringsideer og erhvervsmetoder, herefter nævnt %(q:softwarepatenter).

#toi: Den Europæiske Kommission (CEC) agter snarest at legalisere disse patenter, således at de bliver lovlige i hele Europa. Kommissionen negligere herved det udtalte ønske og den velfunderede kritik, som er blevet udtrykt af en overvældende majoritet af professionelle programmører, softwarevirksomheder, dataloger og økonomer.

#tle: CEC grunder deres %(eb:forslag) på et udkast, der tilsyneladende er skrevet af %(BSA), en amerikansk organisation domineret af nogle få, store softwareleverandører, som f.eks. Microsoft, der har en stor økonomisk interesse på området.

#SnW: Softwarepatenter vil være i konflikt med Ophavretslovgivningen og risikerer at føre til ekspropriering af, snarere end beskyttelse af, softwareudviklernes ejendom. Af de mange %(es:økonomiske studier) der er blevet foretaget, dokumentere ingen at softwarepatenter fører til forøget produktivitet, innovation, vidensspredning eller på nogen anden måde giver makroøkonomiskt fordelagtige effekter. At tillade patenterbarhed for software i følge CEC/BSAs forslag vil snarere medføre flere forskellige %(sk:modsigelser) indenfor patentsystemet samt ugyldiggøre dets grundlæggende principper. Resultatet vil blive at alt bliver patenterbart og at retssikkerheden ophæves.

#Tra: Det europæiske patentsystems institutioner er ikke på nogen meningsfuld måde underkastet demokratisk kontrol. Der er en manglende opdeling mellem lovgivende og dømmende magt, og i særdeleshed ser %(ep:EPO ud til at udgøre en grobund for misbrugende og ulovlig virksomhed).

#Uis: Vi opfordrer Europaparlamentet og Rådet at, afvise direktivforslaget %(REF)

#RWs: Vi opfordrer Europaparlamentet at, finde en måde at tvinge EPO at genindføre, hvad gælder patenterbarhed, sin %(ep:granskningshåndbog fra 1978) eller tilsvarende, for at genvinde en korrekt fortolkning af EPC.

#Rue: Vi foreslår at, en uafhængig europæisk domstol på opfordring af enhver medborger, skal kunne reeksaminere patenter der ser ud til at være beviljet som følge af en fejlagtig fortolkning af EPC, og at EPO i disse tilfælde skal blive tvunget at, tilbagebetale alle gebyrer som den ramte indehaver betalt for patenterne.

#Ett: Vi kræver at, lovgivere på Europæisk og nationalt niveau stadfæster den nuværende formulering af EPC og at de tager i betragtning at, udvige teksten i følge forslaget %(URL), hvis det er nødvendigt for at undgå fejlfortolkning i domstolene.

#Ane: Vi foreslår at, Europaparlamentet og Rådet tager i betragtning at, klargøre grænser for patenterbarhed af computerprogrammer og logiske værker, igennem formuleringen af et europæiskt direktiv i følge forslaget %(URL).

#Dsl: Vi kræver at, enhver lovgivning om patenterbarhed (herunder direktivforslaget fra CEC og tidligere regler skabt gennem præjudikat) prøves imod en %(ts:testrække) af patentansøgninger for at sikre, at loven fører til det ønskede udfald og at den ikke længrer skaber mulighed for fejlfortolkninger.

#Prt: Vi foreslår at, Europaparlamentet installerer et permanent udvalg for overvågning af grænsen for patenterbarhed, med det formål at garantere at, patenter alene bliver udstedt på vilkår at de er i samfundets interesse. Dette udvalg bør dannes af europæiske parlamentsmedlemmer og uafhængige eksperter fra forskellige områder, som f eks matematik, informatik, naturvidenskab, teknik, økonomi, videnskabsteori, etik og jura. Andelen af udøvende patentjurister ansatte ved patentministerier, eller andre personer, for hvilke den hovedsagelige indkomst og karriere ligger indenfor patentsystemet, bør holdes på et lavt niveau (10-20%). Udvalget bør undersøge både patentlovgivningen og den fortolkning som bliver foretaget ved patentkontorer og patentdomstoler. Der bør også ordnes hearings, initieres case studies over patentsystemets effekter og stimulere åben og omfattende forskning. Udvalget bør rapportere til Europaparlamentet i hvad udbredning patentpraktik overensstemmer med patentteori og hvordan de politiske mål indenfor EU og medlemslandene i EU bliver opfyldt. Udvalget ville tage fat på den kvalitetskontrol indenfor EPO, som det europæiske parlaments udvalg for retsspørgsmål og det indre marked diskuteret i sammenhæng med forslaget til rådets ordning om fællespatent (KOM(2000) 412).

#AWo: Vi foreslår at, Europaparlamentet sætter en kommitte i stand for at, undersøge anklager om reglementsfjendlig adfærd af dem indenfor EPO og CEC der er fortaler for software- og genpatentdirektiverne, f.eks. deres tætte samarbejde med en lille kreds lobbyister, deres usammenhængende argumentation og deres åbenbare foragt for lov og demokratiske principper og for at, foreslå vedtægter for at hindre lignende opførsel i fremtiden.

#EdW: Vi venter at, så længe som problemerne indenfor EPO ikke bliver løst, vil enhver ordning som f.eks. fællesskabspatentet at implementeres gennem andre institutioner end EPO.

#Gen: Greens

#FrA: European Free Alliance

#MSe: Media Policy Speaker of the Green Party

#Sco: Spanish Senator, Spokesperson for the socialist group in the Senate's Committee on the Information and Knowledge Society

#Sna: Senador per Girona de l'%(ECP)

#aIo: Catalan Software Association

#mto: responsable de Societat de la Informació d'%(ICV)

#esquerra: President de la Sectorial de la Societat de la Informació d'%(es:Esquerra Republicana de Catalunya)

#eal: member of Bavarian Parliament

#isocecc: European Co-ordination Council of the Internet Society

#rut: representing 22 European ISOC chapters

#eei: Internet Branch of the French Socialist Party

#DiW: Forbundet af IT-professionelle

#ATI: Asociación de Técnicos en Informática

#SPECIS: Syndicat National Professionnel des Etudes, du Conseil, de l'Ingénierie, de l'Informatique et des Services

#etW: trade union with 900,000 members

#IIIA: %(ii:Institut d'Investigació en Intel·ligència Artificial) del %(cs:Consejo Superior de Investigaciones Científicas)

#tsi: Faculty of Sciences of Ghent University

#dea: dean

#dat: Head of Legal Department

#Mic: Matthias Schlegel

#mng: manager

#fud: founders

#fed: founder and owner

#CWW: CTO of %1

#kle: kernel developper

#Pcn: Product Manager

#dta: directeur associé

#Dee: lecturer on industrial economics at %(q:Sorbonne) University in Paris

#anc: Chairman of %(EE)

#iee: Senior member of the IEEE

#cos: économiste

#nmc: expert informatique

#prh: Patent examiner at the Polish Patent Office

#AWm: Aktion gegen Öffentliche Sprachregelungen und Kommunikationserschwernisse

#PWo: Philosophy of Science and Theory of Ditgital Media at Institute of Philosophy of Vienna University

#mathinst: Institut für Mathematik

#DKUUG: Dansk UNIX-system Bruger Gruppe

#FiL

#Aln

#hispalinux: Asociación de Usuarios Españoles de GNU/Linux

#KLID: Komercielle Linux-interssenter I Danmark

#caliu: Catalan Linux Users

#IeA: Assoziazione Software Libero

#rgr: more signatures

#gcr: %(N) persons have so far signed this appeal through our %(ei:new registry form).  They have thereby given us a very detailed mandate for speaking to members of the European Parliament. There is also a very simple and general %(ep:Eurolinux Petition) which you may want to sign first.

#eun: the European Parliament has meanwhile already given an adequate response.  It is now up to the %(sc:EU Council of Ministers) to support this resopnse and then to the Parliament, in a second reading, to remove remaining inconsistencies from the recitals.

#tlS: Get People to Sign

#Pnr: Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions %(el:here).

#Mth: Contact %(MT) if you want to submit signators or have further questions.

#stt: Distribute Posters

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: erikj ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-cpedu ;
# txtlang: da ;
# multlin: t ;
# End: ;

