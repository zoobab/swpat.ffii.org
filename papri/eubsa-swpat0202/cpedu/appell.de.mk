# -*- mode: makefile -*-

appell.de.lstex:
	lstex appell.de | sort -u > appell.de.lstex
appell.de.mk:	appell.de.lstex
	vcat /ul/prg/RC/texmake > appell.de.mk


appell.de.dvi:	appell.de.mk
	rm -f appell.de.lta
	if latex appell.de;then test -f appell.de.lta && latex appell.de;while tail -n 20 appell.de.log | grep -w references && latex appell.de;do eval;done;fi
	if test -r appell.de.idx;then makeindex appell.de && latex appell.de;fi

appell.de.pdf:	appell.de.ps
	if grep -w '\(CJK\|epsfig\)' appell.de.tex;then ps2pdf appell.de.ps;else rm -f appell.de.lta;if pdflatex appell.de;then test -f appell.de.lta && pdflatex appell.de;while tail -n 20 appell.de.log | grep -w references && pdflatex appell.de;do eval;done;fi;fi
	if test -r appell.de.idx;then makeindex appell.de && pdflatex appell.de;fi
appell.de.tty:	appell.de.dvi
	dvi2tty -q appell.de > appell.de.tty
appell.de.ps:	appell.de.dvi	
	dvips  appell.de
appell.de.001:	appell.de.dvi
	rm -f appell.de.[0-9][0-9][0-9]
	dvips -i -S 1  appell.de
appell.de.pbm:	appell.de.ps
	pstopbm appell.de.ps
appell.de.gif:	appell.de.ps
	pstogif appell.de.ps
appell.de.eps:	appell.de.dvi
	dvips -E -f appell.de > appell.de.eps
appell.de-01.g3n:	appell.de.ps
	ps2fax n appell.de.ps
appell.de-01.g3f:	appell.de.ps
	ps2fax f appell.de.ps
appell.de.ps.gz:	appell.de.ps
	gzip < appell.de.ps > appell.de.ps.gz
appell.de.ps.gz.uue:	appell.de.ps.gz
	uuencode appell.de.ps.gz appell.de.ps.gz > appell.de.ps.gz.uue
appell.de.faxsnd:	appell.de-01.g3n
	set -a;FAXRES=n;FILELIST=`echo appell.de-??.g3n`;source faxsnd main
appell.de_tex.ps:	
	cat appell.de.tex | splitlong | coco | m2ps > appell.de_tex.ps
appell.de_tex.ps.gz:	appell.de_tex.ps
	gzip < appell.de_tex.ps > appell.de_tex.ps.gz
appell.de-01.pgm:
	cat appell.de.ps | gs -q -sDEVICE=pgm -sOutputFile=appell.de-%02d.pgm -
appell.de/appell.de.html:	appell.de.dvi
	rm -fR appell.de;latex2html appell.de.tex
xview:	appell.de.dvi
	xdvi -s 8 appell.de &
tview:	appell.de.tty
	browse appell.de.tty 
gview:	appell.de.ps
	ghostview  appell.de.ps &
print:	appell.de.ps
	lpr -s -h appell.de.ps 
sprint:	appell.de.001
	for F in appell.de.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	appell.de.faxsnd
	faxsndjob view appell.de &
fsend:	appell.de.faxsnd
	faxsndjob jobs appell.de
viewgif:	appell.de.gif
	xv appell.de.gif &
viewpbm:	appell.de.pbm
	xv appell.de-??.pbm &
vieweps:	appell.de.eps
	ghostview appell.de.eps &	
clean:	appell.de.ps
	rm -f  appell.de-*.tex appell.de.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} appell.de-??.* appell.de_tex.* appell.de*~
appell.de.tgz:	clean
	set +f;LSFILES=`cat appell.de.ls???`;FILES=`ls appell.de.* $$LSFILES | sort -u`;tar czvf appell.de.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
