\select@language {french}
\select@language {french}
\contentsline {chapter}{\numberline {1}Nous sommes pr\'{e}occup\'{e}s par les faits suivants}{4}{chapter.1}
\contentsline {chapter}{\numberline {2}Pour rem\'{e}dier \`{a} cela, nous proposons les mesures suivantes}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}Signataires}{7}{chapter.3}
\contentsline {chapter}{\numberline {A}Brevets Logiciels Europ\'{e}ens\@DP Quelques \'{E}chantillons}{11}{appendix.A}
\contentsline {section}{\numberline {A.1}Exemplaire Exhibit\'{e}s}{11}{section.A.1}
\contentsline {section}{\numberline {A.2}Candidats}{17}{section.A.2}
\contentsline {chapter}{\numberline {B}Brevets Logiciels en Action}{18}{appendix.B}
\contentsline {section}{\numberline {B.1}Quelques Cas bien document\'{e}s}{18}{section.B.1}
\contentsline {section}{\numberline {B.2}Liens Annot\'{e}s}{24}{section.B.2}
\contentsline {chapter}{\numberline {C}Citations sur la Question de la Brevetabilit\'{e} des R\`{e}gles d'Organisation et de Calcul}{25}{appendix.C}
\contentsline {section}{\numberline {C.1}Informaticiens et Techniciens}{25}{section.C.1}
\contentsline {section}{\numberline {C.2}Experts en Droit, Juges}{34}{section.C.2}
\contentsline {section}{\numberline {C.3}\'{E}conomistes}{43}{section.C.3}
\contentsline {section}{\numberline {C.4}Math\'{e}maticiens, Savants de la Pens\'{e}e, Intellectuels}{49}{section.C.4}
\contentsline {section}{\numberline {C.5}Politiciens}{50}{section.C.5}
\contentsline {section}{\numberline {C.6}Strat\`{e}ges en Brevet}{59}{section.C.6}
\contentsline {section}{\numberline {C.7}Liens Annot\'{e}s}{60}{section.C.7}
\contentsline {section}{\numberline {C.8}Questions, Choses a faires, Comment vous pouvez aider}{61}{section.C.8}
\contentsline {chapter}{\numberline {D}Ensemble de tests pour la l\'{e}gislation sur les limites de la brevetabilit\'{e}}{62}{appendix.D}
\contentsline {section}{\numberline {D.1}Quelques exemples de brevets}{62}{section.D.1}
\contentsline {section}{\numberline {D.2}Questions auxquelles chaque brevet doit r\'{e}pondre}{62}{section.D.2}
\contentsline {section}{\numberline {D.3}Tableau comparatif}{63}{section.D.3}
\contentsline {section}{\numberline {D.4}Candidats \`{a} essayer\@DP Propositions de loi de CEC/BSA, FFII/Eurolinux et d'autres}{63}{section.D.4}
\contentsline {chapter}{\numberline {E}Proposition(s) BSA/CCE et Contre-Proposition Bas\'{e}e sur la CBE}{64}{appendix.E}
\contentsline {section}{\numberline {E.1}Pr\'{e}ambule}{64}{section.E.1}
\contentsline {section}{\numberline {E.2}Les Articles}{71}{section.E.2}
\contentsline {section}{\numberline {E.3}Remarques}{78}{section.E.3}
\contentsline {section}{\numberline {E.4}Liens Annot\'{e}s}{79}{section.E.4}
