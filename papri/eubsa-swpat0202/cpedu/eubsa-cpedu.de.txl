<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Aufruf zum Handeln

#descr: Der Richtlinienvorschlag der Europäischen Kommission für die Patentierbarkeit von Software-Innovationen erfordert eine klare Antwort vom Europäischen Parlament, den nationalen Regierungen und anderen Akteuren.  Hier erklären wir, was unserer Meinung nach zu tun ist.

#Wce: Folgendes bereitet uns Sorge

#Fea: Daher empfehlen wir folgende Maßnahmen

#Sno: Unterzeichner

#dfm: Aktualisierung 2003/09/24

#tef: Das %(ep:Europäische Patentamt) (EPA) hat im Widerspruch zum Buchstaben und Geist des geltenden Gesetzes zehntausende von Patenten auf Programm- und Geschäftslogik erteilt, die wir im folgenden %(q:Logikpatente) oder %(q:Softwarepatente) nennen.

#toi: Die Europäische Kommission (EUK) drängt darauf, diese Patente zu legalisieren und in ganz Europa durchsetzbar zu machen.  Dabei missachtet sie den deutlichen Willen und die wohlbegründeten Argumente der großen Mehrheit von Software-Fachleuten, Software-Firmen, Informatikern und Wirtschaftswissenschaftlern.

#tle: Die EUK gründet ihren %(eb:Vorschlag) offenbar auf einen Entwurf der %(BSA), einer amerikanischen Organisation, die von wenigen großen Herstellern, insbesondere Microsoft, dominiert wird.

#SnW: Softwarepatente konkurrieren mit dem Software-Urheberrecht und führen eher zu einer Enteignung von Software-Autoren als zu einem Schutz für deren Investitionen.  Unter zahlreichen einschlägigen %(es:wirtschaftswissenschaftlichen Studien) gibt es keine, die behauptet, Softwarepatente würden positives zur Produktivität, Innovation oder Wissensverbreitung beitragen oder in sonstiger Weise der Volkswirtschaft zugute kommen.  Die Patentierbarkeit von Programmlogik, wie von der EUK/BSA vorgeschlagen, führt ferner zu allerlei %(sk:Ungereimtheiten) innerhalb des Patentsystems und untergräbt zentrale Annahmen, auf denen dieses System beruht.  Im Ergebnis wird dann prinzipiell alles patentierbar, und es kann keine Rechtssicherheit mehr geben.

#Tra: Die Institutionen des europäischen Patentsystems unterliegen keiner wirksamen demokratischen Kontrolle.  Die Trennung zwischen der gesetzgebenden und der rechtsprechenden Gewalt ist unzureichend.  Insbesondere das EPA hat sich offenbar %(ep:zu einer Brutstätte missbräuchlicher und gesetzeswidriger Praktiken entwickelt).

#Uis: Wir bitten das Europäische Parlament und den Europarat, den Richtlinienvorschlag %(REF) zurückzuweisen.

#RWs: Wir bitten das Europäische Parlament, geeignete Wege zu finden, um das EPA dazu zu verpflichten, hinsichtlich der Frage der Patentierbarkeit die %(ep:Prüfungsrichtlinien von 1978) oder etwas entsprechendes wieder einzusetzen, um die richtige Auslegung des EPÜ wiederherzustellen.

#Rue: Wir schlagen vor, dass ein unabhängiges europäisches Gericht beauftragt werden sollte, auf Anrufung durch jeden Bürger hin jegliche Patente, deren Erteilung prima facie auf einer falschen Auslegung der Patentierbarkeitsbestimmungen des EPÜ beruhte, erneut zu prüfen, und dass das EPA in solchen Fällen den ehemaligen Patentinhaber alle Gebühren zurückerstatten sollte, die es von ihm kassiert hat.

#Ett: Wir bitten die Gesetzgeber der europäischen und nationalen Ebene, den derzeitigen EPÜ-Text zu bekräftigen und eine weitere Präzisierung anhand des Vorschlages %(URL) zu erwägen, soweit dies erforderlich erscheint, um weiteren Fehlauslegungen durch Gerichte vorzubeugen.

#Ane: Wir schlagen dem Europäischen Parlament und dem Rat vor, eine Klärung der Grenzen der Patentierbarkeit im Hinblick auf Software und logische Schöpfungen durch eine europäische Richtlinie im Sinne der Gegenvorschläge %(URL) in Erwägung zu ziehen.

#Dsl: Wir fordern, dass jegliche zur Debatte stehende Gesetzesregel (einschließlich Richtlinienentwürfe der EUK und durch Richterrecht entstandene Regeln) anhand einer %(ts:Reihe von Beispielpatenten) daraufhin überprüft wird, ob sie zu den erwünschten Ergebnissen führt, ohne Raum für weitere Fehlauslegungen zu lassen.

#Prt: Wir schlagen dem Europäischen Parlament vor, es möge einen ständigen Ausschuss zur Überwachung der Grenzen der Patentierbarkeit bilden, um sicher zu stellen, dass Patente nur zu Bedingungen erteilt werden, unter denen sie dem Gemeinwohl dienen.  Dieser Ausschuss sollte aus MdEPs und unabhängigen Fachleuten aus verschiedenen Gebieten wie z.B. Mathematik, Informatik, Naturwissenschaft, Technik, Wirtschaftswissenschaft, Wissenschaftstheorie, Ethik und Recht, bestehen.  Der Anteil der Patentanwälte, Patentfunktionäre oder sonstiger Personen, deren Einkommen und Karriere vom Patentwesen abhängt, sollte in sehr engen Grenzen (z.B. 10-20%) gehalten werden.  Der Ausschuss sollte alle Regelungsvorschläge für die Patentierbarkeit an Fallbeispielen verifizieren und auf ihre volkswirtschaftliche Wirkung hin und ihre Übereinstimmung mit den politischen Zielen der Europäischen Gemeinschaft hin untersuchen.  Der Ausshuss würde somit auf die Sorgen eingehen, die vom Rechtsausschuss des Europaparlaments hinsichtlich der Patentqualität beim EPA im Zusammenhang mit der Diskussion des Gemeinschaftspatents in der Verordnung %(REF) geäußert wurden.

#AWo: Wir schlagen dem Europäischen Parlament vor, es möge einen Untersuchungsausschuss einrichten, um verschiedene Vorwürfe von Fehlverhalten seitens der Initiatoren der Software- und Genpatentierungsrichtlinien in EPA und EUK zu untersuchen, wie z.B. ihre enge Zusammenarbeit mit einm begrenzten Kreis von Lobbyisten, ihre sprunghafte Argumentation und ihre offenbare Verachtung demokratischer und rechtsstaatlicher Prinzipien, und Maßnahmen vorzuschlagen, durch die Missstände dieser Art für die Zukunft verhindert werden können.

#EdW: Wir erwarten dass solange die Probleme des EPA nicht gelöst sind jegliche neue Regelung wie z.B. das %(cp:Gemeinschaftspatent) nur ohne Einbeziehung des EPA verwirklicht wird.

#Gen: Grüne

#FrA: Europäisches Freies Bündnis

#MSe: Mediensprecherin der Fraktion Bündnis 90 / Die Grünen

#Sco: Spanischer Senator, Sprecher der sozialistischen Fraktion im Ausschuss für Informations- und Wissensgesellschaft

#Sna: Senator der %(ECP) für die Region Girona

#aIo: Catalan Software Association

#mto: Referent Informationsgesellschaft %(ICV)

#esquerra: Präsident des Ausschusses Informationsgesellschaft der %(es:Katalonischen Republikanischen Partei)

#eal: Mitglied des Bayerischen Landtags

#isocecc: Europäische Koordinationsausschuss der ISOC

#rut: stellvertretend für 22 europäische ISOC-Sektionen

#eei: Internet-Sektion der Sozialistischen Partei Frankreichs

#DiW: Dänischer Verband der IT-Fachleute

#ATI: Spanischer Verband der Informatiker

#SPECIS: Französischer Verband der Fachleute in Wissenschaft, Beratung, Technik, Informatik und Dienstleistungen

#etW: Gewerkschaft mit 900000 Mitgliedern

#IIIA: %(ii:Institut für Forschung in Künstlicher Intelligenz) des Spanischen %(cs:Obersten Forschungsrates)

#tsi: Naturwissenschaftliche Fakultät der Universität Ghent

#dea: Dekan

#dat: Leiter der Rechtsabteilung

#Mic: Matthias Schlegel

#mng: Geschäftsführer

#fud: Gründer

#fed: Gründer und Besitzer

#CWW: CTO of %1

#kle: kernel developper

#Pcn: Produktmanager

#dta: directeur associé

#Dee: Dozent für Industrielle Ökonomie der Sorbonne-Universität in Paris

#anc: Chairman of %(EE)

#iee: Senior member of the IEEE

#cos: Wirtschaftswissenschaftler

#nmc: Informatiker

#prh: Patentprüfer am Polnischen Patentamt

#AWm: Aktion gegen Öffentliche Sprachregelungen und Kommunikationserschwernisse

#PWo: Wissenschaftsphilosophie und Theorie digitaler Medien am Institut für Philosophie der Universität Wien

#mathinst: Institut für Mathematik

#DKUUG: Dänische Vereinigung der Unix-Anwender

#FiL

#Aln

#hispalinux: Verband der Spanischen Anwender von GNU/Linux

#KLID: Verband der Kommerziellen Linux-Interessenten in Dänemark

#caliu: Verband der Katalonischsprachigen Anwender von GNU/Linux

#IeA: Italienischer Verband für Freie Software

#rgr: weitere Unterzeichner

#gcr: %(N) Personen haben diesen Appell über unser %(ei:neues Eintragsformular) unterzeichnet und uns damit einen sehr detaillierten Auftrag für unsere Arbeit am Europäischen Parlament erteilt.  Es gibt darüber hinaus die sehr einfache und allgemein gehaltene %(ep:Eurolinux-Petition), die Sie vielleicht auch unterzeichnen möchten.

#eun: Das Parlament hat inzwischen bereits eine angemessene Antwort gegeben.  Es liegt nun am %(sc:Ministerrat der EU), diese Antwort zu unterstützen, und am Parlament, sie in einer zweiten Lesung zu perfektionieren.

#tlS: Unterzeichner gewinnen

#Pnr: Contact your MEP, ask him/her to support the CfA and if possible to sign it, see instructions %(el:here).

#Mth: Contact %(MT) if you want to submit signators or have further questions.

#stt: Distribute Posters

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-cpedu ;
# txtlang: de ;
# multlin: t ;
# End: ;

