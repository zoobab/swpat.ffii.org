<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: DIHK 2002/04/20, 10/04: Gegen Softwarepatente und EuK-Vorschlag

#descr: RA Doris Möller hat für den Deutschen Industrie- und Handelskammertag (DIHK) eine Stellungnahme zum EU-Richtlinienvorschlag über die Patentierbarkeit computer-implementierter Organisations- und Rechenregeln entworfen, die Bedenken äußert und eine klarere Begrenzung der Patentierbarkeit fordert.  Damit hebt sich der DIHK positiv von Verbänden wie BDI, Bitkom und Internationale Handelskammer (ICC) ab, die trotz überwiegend gegenteiliger Meinungen ihrer Mitglieder ihren Patentjuristen erlaubten, sich im Namen %(q:der Industrie) für möglichst weitgehende Patentierbarkeit stark zu machen.

#Dkl: DIHK 2002-10-04 zum EUK-Vorschlag: Softwarepatente klar abgrenzen!

#PdW: Pressemeldung der Deutschen Industrie- und Handelskammer, die am 4. Oktober 2002 über OTS an die Redaktionen der Tageszeitungen versandt wurde.  In diesem kurzen Text erklärt RA Doris Möller, dass Softwarepatente nicht wünschenswert sind, und wirft der Europäische Kommission vor, in ihrerm Richtlinienentwurf nicht für einen klaren Ausschluss von Softwarepatenten gesorgt zu haben.  Die Presseerklärung kommt einen Tag nach einem Treffen der Patent-Arbeitsgruppe des Europäischen Rates, auf welchem die dänische Präsidentschaft vorschlug mit Unterstützung der deutschen Delegation vorschlug, noch einen Schritt weiter zu gehen als die Europäische Kommission und unmittelbare Programmansprüche zu legalisieren.  Die Presseerklärung beruht auf einem Arbeitspapier von RA Möller, welches seit April 2002 unter den Mitgliedern der DIHK zirkulierte.

#Bj4: Berlin, 20.04.2002

#cer: %(nl|Stellungnahme|des Deutschen Industrie- und Handelskammertages|zur Patentierbarkeit|computerimplementierter Erfindungen)

#Zor: Ziel des von der EU-Kommission vorgelegten Richtlinienvorschlages ist, die Divergenzen zwischen Patenteinträgen, die über das Europäische Patentamt oder über Anmeldungen nach dem nationalen Recht bei computerimplementierten Erfindungen entstanden sind, zu überwinden.  Dabei müssen die Grenzen der Patentierbarkeit für computerimplementierte Erfindungen festgelegt werden, um die bestehenden Rechtsunsicherheiten in diesem Bereich zu überwinden.

#Dse: Der DIHK spricht sich generell gegen eine isolierte Patentierbarkeit von Software aus. Die Patentierung von Software an sich scheitert oft daran, dass das Gros der neu bzw. aktuell erstellten Software- nach den uns übermittelten Stellungnahmen - auf einer Kombination von Grundbausteinen basiert, die zum anerkannten und bereits publizierten Wissensstand der Informatik gehören. Sie werden bei jeder Informatik- oder Technikausbildung gelehrt. Mathematische Formeln und Zusammenhänge sollten weiterhin grundsätzlich nicht patentierbar sein, weil sie eine nachträgliche Beschreibung natürlicher oder technischer Phänomene darstellen. Hier liegt keine Erfindung, sondern eine Erläuterung/Beschreibung eines natürlichen gegebenen Ablaufs vor.  Allenfalls handelt es sich um ein Entdecken von etwas, was bisher schon existent war. Anders kann allenfalls bei neuartigen Suchalgorithmen argumentiert werden, die nicht einfach aus vorhandenen Bausteinen abgeleitet werden können. Sollten diese Algorithmen zusammen mit der entsprechenden Hardware angemeldet werden und sie für die Lösung eines speziellen technischen Problems entwickelt worden sein, dann ist ein erfindungsspezifischer Hintergrund vorhanden, der für die im Patentbereich notwendige Erfindung spricht. Nur in solchen Fallgestaltungen dürften derartigen Patentierungen nicht zu einer Benachteiligung von KMUs (klein- und mittelständischen Unternehmen) führen.

#Nnj: Nach Art. 3 werden nur computerimplementierte Erfindungen geschützt. In Art. 2 werden diese aber begrifflich dadurch definiert, dass zu der Ausführung der Erfindung lediglich eine programmierbare Einrichtung erforderlich ist, die mindestens ein neuartiges Merkmal aufweist. Danach wären Computerprogramme in jeder Form, also auch als Source Code oder Algorithmus, patentierbar. Dies ist nicht gewollt! Es widerspricht zudem dem System des gewerblichen Rechtsschutzes in Europa.

#Dic: Der DIHK wendet sich auch dagegen, das bisherige System im gewerblichen Rechtsschutz im Hinblick auf die amerikanischen Verhältnisse zu verwässern. Eine solche Vorgehensweise wäre kurzsichtig und mittelfristig nicht von Vorteil für die Unternehmen und deren Wettbewerbsfähigkeit.

#UWW: Unter diesen Vorgaben sollte im Rahmen der Begriffsbestimmung in Art. 2 der Richtlinie auch von der Wortwahl her sicher gestellt werden, dass tatsächlich Computerprogramme als solche nicht patentierbar sind. Die Rechtsprechung des BGH, die die Technizität einer computerbezogenen Lehre an das Erfordernis einer patentrechtswürdigen Eigenheit bzw. eines weiteren technischen Effekts knüpft, muss erhalten bleiben. Der %(q:technische Beitrag) muss außerhalb des Computerprogramms als solchem liegen, also nicht schon deshalb bejaht werden, weil bei allen computerbezogenen Programmen physikalische Veränderungen bei der Hardware bei Ausführung der Programmbefehle entstehen. Dies ist u. E. noch durch stärkere Präzisierung in Art. 2 und Art. 4 Ziff. 2 der Richtlinie deutlich zu machen.

#Dgu: Die Richtlinie kann ihr vorgegebenes Ziel nur erreichen, wenn es gelingt, klare Unterscheidungskriterien für die Abgrenzung zwischen Programmen, die aufgrund ihrer technischen Folgen auf dem jeweiligen Fachgebiet einen technischen Fortschritt bewirken und den übrigen Computerprogrammen, die unverändert allein dem Urheberschutz unterliegen, festzulegen. Dies erscheint nach dem bereits Gesagten zweifelhaft.

#Aei: Als problematisch wird auch angesehen, dass in Art. 4 der Richtlinie Hinweise auf eine gewisse Erfindungshöhe völlig fehlen. Damit besteht entgegen den Äußerungen der Kommission dennoch das Risiko, dass die Patentämter - nach amerikanischem Vorbild - von Anmeldungen für zahllose Trivialpatente überschüttet werden. Es ist zu befürchten, dass auf diese Weise große Softwarehersteller versuchen werden, jegliche Entwicklungen kleinerer Unternehmen durch %(q:Vorratsanmeldungen) bereits im Keim zu ersticken. Es wäre nicht im Sinne der Weiterentwicklung unserer Wirtschaft, wenn für nahezu jede noch so triviale Änderung einer technischen Software Patentschutz erreicht werden könnte, während die Anforderungen an den Urheberschutz für sonstige Software erheblich höher lägen.

#Pdt: Problematisch könnte bei Inkrafttreten der Richtlinie und deren Umsetzung in die nationalen Rechte auch die Aufarbeitung der Altfälle sein, bei denen die niedergelegten Voraussetzungen bei Erteilung des Patentschutzes noch nicht beachtet wurden. Möglicherweise müssen dafür Übergangsregelungen geschaffen werden.

#Aee: Ausdrücklich begrüßen wir den Vorschlag in Art. 8, der vorsieht, dass nach 3 Jahren über die Auswirkungen der Richtlinie dem Europäischen Parlament und dem Rat Bericht erstatten werden muss. Allerdings muss dazu bedacht werden, dass in einem solchen Bericht nicht zum Ausdruck kommen kann, wie viele Unternehmen wegen fehlender Marktchancen aufgrund der Behinderungen durch das neue Patentrecht erst gar nicht gegründet oder in ihrer Entwicklung gehemmt wurden.

#Wnw: Wir bitten daher das Bundesjustizministerium, bei den Beratungen zu dem Richtlinienvorschlag - bei allem Verständnis für die Notwendigkeit der Harmonisierung - zu beachten, dass das System des gewerblichen Rechtsschutzes auch bei Schaffung der Möglichkeit, computerimplementierte Erfindungen zu patentieren, gewahrt bleibt und die Voraussetzungen für die Patentierbarkeit konkreter abgefasst und insbesondere das Merkmal der Erfindungshöhe nicht vernachlässigt wird.

#IWn: Im Übrigen möchten wir auf den %(rs:kritischen Beitrag von Roman Sedlmaier) in %(q:Mitteilungen der deutschen Patentanwälte) zu diesem Thema ausdrücklich hinweisen.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: dihk0204 ;
# txtlang: de ;
# multlin: t ;
# End: ;

