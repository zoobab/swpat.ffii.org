<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Francia 2002.03.01: La propuesta de Directiva de la Comisión Europea es inaceptable.

#descr: El ministro francés Christian Pierret escribe a la Comisión Europea expresando su desacuerdo con el aparente fallo de la propuesta de directiva, que no impone límites claros sobre la patentabilidad y no sirve a los intereses de los creadores europeos de software ni tiene en cuenta políticas europeas relacionadas, como la iniciativa e.Europe.

#DrT: The Text

#MTE: Ministro de Industria, Pequeña y Mediana Empresa, Comercio, Artesanía y Consumo

#CWn: Nota de prensa

#Pea: París, 1 de marzo de 2002

#CeW: Christian Pierre, el ministro de Industria, PYMEs, Comercio, Artesanía y Consumo ha notificado a la Comisión Europea la posición del gobierno francés sobre el borrador de directiva sobre la patentabilidad del software que fue presentado hoy al Consejo de Mercado Interno. Al encontrar que el borrador de directiva no aporta ninguna de las aclaraciones que se esperaban sobre los límites de lo que se considera patentable, el gobierno está preocupado por el ámbito lo patentable, que podría quedar abierto para todo el software, y por lo tanto, para el pensamiento puro y los modelos de negocio. Por el contrario, ha quedado claro que en Francia y en Europa esta extensión de la patentabilidad es rechazada por una gran mayoría.

#LtW: Francia ha dejado claro que es esencial tener en cuenta los méritos y deméritos de la protección del software como resultado de las prácticas actuales de la Oficina Europea de Patentes (OEP) y de sus estados miembro. El gobierno francés quiere evitar cualquier borrador que pueda tener consecuencias negativas sobre la innovación, la interoperabilidad y el software libre, y sobre el sector en su conjunto (editores, integradores y usuarios de software), y más especialmente sobre las PYMEs. Cree que la aproximación seguida por la directiva no responde adecuadamente a los aspectos económicos, científicos y culturales del sector del software, ni a la necesidad de promover la innovación, que es una de las prioridades de el plan de acción de %(q:e.Europe). La Comisión realizó estudios y una consulta durante el último tercio del año 2000. Sin embargo, el borrador de directiva que se presentó a los estados miembro no aborda claramente los riesgos que conllevaría la legitimación de las prácticas de la EPO en los estados miembros, en comparación con las ventajas. Varios estudios e informes realizados en varios estados miembro han sido muy poco entusiastas con respecto a esta evolución. Por iniciativa de Francia, entre otros, la Conferencia Diplomática para la Revisión de la Convención Europea sobre Patentes (CEP), que tuvo lugar en Munich en noviembre de 2000, decidió no cambiar las reglas de la Convención Europea sobre Patentes en este aspecto, deseando que se encuentre una posición europea clara basada en un análisis preciso de las consecuencias económicas, técnicas y legales.

#Cap: Contacto de prensa

#Cea: Gabinete de Christian Pierret

#LeW: Laurence GAUNE

#t1l: teléfono

#TWs: L'extension du domaine des brevets aux logiciels, absurdité économique qui handicaperait nos entreprises et freinerait le développement de ce secteur, doit être refusée.

#LWn: Libération 2002-03-12: Tous les Candidats contre le Brevet Logiciel

#Cen: Collection de citations de Jospin, Chirac, Mamère, Chevenement, les libéraux et les communistes, qui touses disent avec des raisonnements différents que les brevets logiciels sont une mauvaise idée

#ryr: news report by a leading German IT policy magazine, leads readers in the forum to exclaim %(q:Vive La France!)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: france020301 ;
# txtlang: es ;
# multlin: t ;
# End: ;

