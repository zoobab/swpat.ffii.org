<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Frankreich 2002-03-01: Richtlinienvorschlag der Europäischen Kommission inakzeptabel

#descr: Der französische Industrieminister Christian Pierret hat im europäischen Binnenmarktrat kritisiert, dass der von der Europäischen Kommission Vorschlag einer Richtlinie für die Patentierbarkeit von Rechenregeln in keiner Weise die versprochene klare Begrenzung der Patentierbarkeit bringt und den wirtschaftspolitischen Zielen der Europäischen Union zuwiderläuft.  Dies gab er in einer Presseerklärung bekannt.

#DrT: Der Text

#MTE: Minister für die Industrie, die kleineren und mittleren Unternehmen, den Handel, das Handwerk und den Verbrauch

#CWn: Presseerklärung

#Pea: Paris den ersten März 2002

#CeW: Der Minister für Industrie, KMU, den Handel, Handwerk und Verbrauch, Christian Pierret, hat die Europäische Kommission über die Position Frankreichs zum Entwurf einer Richtlinie über die Patentierbarkeit von Software unterrichtet, die heute dem Binnenmarktrat vorgelegt wurde.  Die Regierung stellt fest, dass der Richtlinienvorschlag keine der erwarteten Klärungen bezüglich der Grenzen der Patentierbarkeit bringt, und ist beunruhigt, dass das Gebiet des Patentierbaren nunmehr für Logikalien (Software) aller Art bis hin zu Verfahren für geistige und geschäftliche Tätigkeit geöffnet werden könnte.  Es hat sich jedoch in Frankreich ebenso wie in Europa in der Diskussion deutlich gezeigt, dass eine solche Ausweitung weitgehend auf Ablehnung stößt.

#LtW: Frankreich hat daran erinnert, dass seiner Meinung nach eine Bilanz des Rechtsschutzes zu ziehen ist, der sich aus der Praxis der Europäischen Patentorganisation (EPO) und der Mitgliedsstaaten ergibt.  Die französische Regierung möchte jegliches Vorhaben vermeiden, welches negative Auswirkungen auf die Innovation, die Interoperabilität, die freie Software und die Gesamtheit der Akteuere (Schaffende, Integrierer und Anwender von Software) haben könnte.  Wir glauben, dass der Richtlinienvorschlag weder in angemessener Weise auf die wirtschaftlichen, wissenschaftlichen und kulturellen Herausforderungen des Software-Sektors antwortet noch den Erfordernissen der Förderung der Innovation Rechnung trägt, wie sie im Aktionsplan %(q:e.Europe) als Priorität angeführt sind.  Die Kommission hat zu diesem Thema Studien und eine Konsultation im letzten Drittel des Jahres 2000 durchgeführt.  Dennoch zeigt der Richtlinienentwurf, wie er den Mitgliedsstaaten vorgelegt wurde, nicht klar die Risiken auf, die eine Durchsetzung der Praxis der EPO in den Mitgliedsstaaten mit sich bringen würde, und deren Verhältnis zu dem zu erwartenden Nutzen.  Im übrigen haben sich verschiedene in den Mitgliedsstaaten erschienene Studien eher reserviert über diese Aussichten geäußert.  Nicht zuletzt auf Betreiben Frankreichs hatte die Diplomatische Konferenz für die Revision des Europäischen Patentübereinkommens (EPÜ), die im November 2000 in München stattfand, beschlossen, die Vorgaben des Übereinkommens in dieser Sache nicht zu ändern, in dem Wunsch, dass eine klare europäische Position auf Grundlage einer präzisen Analyse der wirtschaftlichen, technischen und juristischen Folgen gefunden werden könnte.

#Cap: Pressekontakt

#Cea: Kabinett von Christian PIERRET

#LeW: Laurence GAUNE

#t1l: tel

#TWs: Die Ausweitung des Bereichs des Patentierbaren auf Software, ein wirtschaftlicher Unsinn der unsere Unternehmen behindern und diesen Sektor bremsen wuerde, muss verweigert werden.

#LWn: Libération 2002-03-12: Alle Parteien und Präsidentschaftskandidaten gegen Swpat

#Cen: Die französische Tageszeitung zitiert Jospin, Chirac und die Kandidaten der Liberalen, Kommunisten, Grünen u.a., die alle mit verschiedenen Begründungen erläutern, warum Softwarepatente schlecht sind und entsprechende Brüsseler Pläne verhindert werden müssen.

#ryr: news report by a leading German IT policy magazine, leads readers in the forum to exclaim %(q:Vive La France!)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: france020301 ;
# txtlang: de ;
# multlin: t ;
# End: ;

