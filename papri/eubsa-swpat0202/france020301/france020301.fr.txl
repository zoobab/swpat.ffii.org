<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: France 2002-03-01: EU Commission Directive Proposal Unacceptable

#descr: Le Ministre de l'Industrie Christian Pierret critique que la directive proposée par la Commission Européenne ne propose pas une limite claire a la brevetabilité et n'est pas basée sur une argumentation économique fondée.   Celle-ci devrait prendre en compte la réalité du secteur logiciel entier autant qu'un analyse des effets économiques de la pratique de l'OEB et sa compatibilité avec les buts formulés au sein du plan e.Europe.

#DrT: Le Texte

#MTE: MINISTRE DELEGUE A  L'INDUSTRIE, AUX PETITES ET MOYENNES ENTREPRISES, AU  COMMERCE, A L'ARTISANAT ET A LA CONSOMMATION

#CWn: Communiqué de presse

#Pea: Paris le 1er mars 2002

#CeW: Christian PIERRET,  Ministre délégué à l'Industrie, aux PME, au Commerce, à l'Artisanat et à la Consommation, a fait part à la Commission Européenne de la position du gouvernement français concernant le projet de directive sur la brevetabilité des logiciels présenté aujourd'hui au Conseil marché intérieur. Constatant que le projet de directive n'apporte aucune des précisions attendues sur les limites et les exigences de la brevetabilité, le gouvernement s'inquiète du champ qui pourrait être ouvert à la brevetabilité de l'ensemble des logiciels voire des méthodes intellectuelles et commerciales. Or, il est apparu clairement en France comme en Europe qu'une telle extension est largement rejetée.

#LtW: La France a rappelé qu'elle juge indispensable de disposer d'un bilan de la protection juridique des logiciels telle qu'elle résulte de la pratique de l'Organisation Européenne des Brevets (OEB) et des Etats membres. Le gouvernement français souhaite écarter tout projet qui aurait des conséquences négatives pour l'innovation, pour l'interopérabilité et les logiciels libres, et pour l'ensemble des acteurs (éditeurs, intégrateurs, utilisateurs), notamment les PME. Il considère que la proposition de directive ne répond pas de façon adéquate aux enjeux économiques, scientifiques et culturels du secteur du logiciel ainsi qu'à la nécessité de promouvoir l'innovation qui figure parmi les priorités du plan d'action « e.Europe ».  La Commission a mené des études et une consultation sur ce thème au dernier trimestre 2000. Le projet de directive présenté aux Etats membres n'indique pourtant pas clairement les risques que présenterait une validation juridique de la pratique de l'OEB dans les Etats membres de l'Union, au regard des avantages que l'on pourrait en attendre. Divers études et rapports conduits dans plusieurs Etats Membres sont apparus par ailleurs réservés quant à une telle évolution.  A l'initiative notamment de la France, la Conférence Diplomatique pour la révision de la Convention sur le Brevet Européen (CBE), qui s'est tenue à Munich en novembre 2000, avait décidé de ne pas modifier les dispositions de la Convention en la matière, souhaitant qu'une position européenne claire puisse être prise sur la base d'une analyse précise de ses conséquences sur les plans économique, technique et juridique.

#Cap: Contact presse

#Cea: Cabinet de Christian PIERRET

#LeW: Laurence GAUNE

#t1l: tél

#TWs: L'extension du domaine des brevets aux logiciels, absurdité économique qui handicaperait nos entreprises et freinerait le développement de ce secteur, doit être refusée.

#LWn: Libération 2002-03-12: Tous les Candidats contre le Brevet Logiciel

#Cen: Collection de citations de Jospin, Chirac, Mamère, Chevenement, les libéraux et les communistes, qui touses disent avec des raisonnements différents que les brevets logiciels sont une mauvaise idée

#ryr: news report by a leading German IT policy magazine, leads readers in the forum to exclaim %(q:Vive La France!)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: france020301 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

