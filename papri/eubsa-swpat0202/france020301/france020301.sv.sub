\begin{subdocument}{france020301}{France 2002-03-01: EU Commission Directive Proposal Unacceptable}{http://swpat.ffii.org/papri/eubsa-swpat0202/france020301/index.sv.html}{Workgroup\\swpatag@ffii.org}{Le Ministre de l'Industrie Christian Pierret critique que la directive propos\'{e}e par la Commission Europ\'{e}enne ne propose pas une limite claire a la brevetabilit\'{e} et n'est pas bas\'{e}e sur une argumentation \'{e}conomique fond\'{e}e.   Celle-ci devrait prendre en compte la r\'{e}alit\'{e} du secteur logiciel entier autant qu'un analyse des effets \'{e}conomiques de la pratique de l'OEB et sa compatibilit\'{e} avec les buts formul\'{e}s au sein du plan e.Europe.}
\begin{sect}{text}{The Text}
MINISTRE DELEGUE A  L'INDUSTRIE, AUX PETITES ET MOYENNES ENTREPRISES, AU  COMMERCE, A L'ARTISANAT ET A LA CONSOMMATION

Communiqu\'{e} de presse

http://www.industrie.gouv.fr/accueil.htm

Paris le 1er mars 2002

Christian PIERRET,  Ministre d\'{e}l\'{e}gu\'{e} \`{a} l'Industrie, aux PME, au Commerce, \`{a} l'Artisanat et \`{a} la Consommation, a fait part \`{a} la Commission Europ\'{e}enne de la position du gouvernement fran\c{c}ais concernant le projet de directive sur la brevetabilit\'{e} des logiciels pr\'{e}sent\'{e} aujourd'hui au Conseil march\'{e} int\'{e}rieur. Constatant que le projet de directive n'apporte aucune des pr\'{e}cisions attendues sur les limites et les exigences de la brevetabilit\'{e}, le gouvernement s'inqui\`{e}te du champ qui pourrait \^{e}tre ouvert \`{a} la brevetabilit\'{e} de l'ensemble des logiciels voire des m\'{e}thodes intellectuelles et commerciales. Or, il est apparu clairement en France comme en Europe qu'une telle extension est largement rejet\'{e}e.

La France a rappel\'{e} qu'elle juge indispensable de disposer d'un bilan de la protection juridique des logiciels telle qu'elle r\'{e}sulte de la pratique de l'Organisation Europ\'{e}enne des Brevets (OEB) et des Etats membres. Le gouvernement fran\c{c}ais souhaite \'{e}carter tout projet qui aurait des cons\'{e}quences n\'{e}gatives pour l'innovation, pour l'interop\'{e}rabilit\'{e} et les logiciels libres, et pour l'ensemble des acteurs (\'{e}diteurs, int\'{e}grateurs, utilisateurs), notamment les PME. Il consid\`{e}re que la proposition de directive ne r\'{e}pond pas de fa\c{c}on ad\'{e}quate aux enjeux \'{e}conomiques, scientifiques et culturels du secteur du logiciel ainsi qu'\`{a} la n\'{e}cessit\'{e} de promouvoir l'innovation qui figure parmi les priorit\'{e}s du plan d'action `` e.Europe ''.  La Commission a men\'{e} des \'{e}tudes et une consultation sur ce th\`{e}me au dernier trimestre 2000. Le projet de directive pr\'{e}sent\'{e} aux Etats membres n'indique pourtant pas clairement les risques que pr\'{e}senterait une validation juridique de la pratique de l'OEB dans les Etats membres de l'Union, au regard des avantages que l'on pourrait en attendre. Divers \'{e}tudes et rapports conduits dans plusieurs Etats Membres sont apparus par ailleurs r\'{e}serv\'{e}s quant \`{a} une telle \'{e}volution.  A l'initiative notamment de la France, la Conf\'{e}rence Diplomatique pour la r\'{e}vision de la Convention sur le Brevet Europ\'{e}en (CBE), qui s'est tenue \`{a} Munich en novembre 2000, avait d\'{e}cid\'{e} de ne pas modifier les dispositions de la Convention en la mati\`{e}re, souhaitant qu'une position europ\'{e}enne claire puisse \^{e}tre prise sur la base d'une analyse pr\'{e}cise de ses cons\'{e}quences sur les plans \'{e}conomique, technique et juridique.

\begin{description}
\item[Contact presse:]\  -  Cabinet de Christian PIERRET Laurence GAUNE t\'{e}l: 01 53 18 44 85
\end{description}
\end{sect}\begin{sect}{links}{Annotated Links}
\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf All French Presidential Candidates against Software Patentability\footnote{}}}

\begin{quote}
The top candidates of all french parties have expressed themselves clearly and even fervently against software patents.  Each candidate seems happy to find that his particular political thinking can serve as a good basis for explaining why  software patents are bad.
\end{quote}
\filbreak

\item
{\bf {\bf Parti Socialiste contre Brevets Logiciels et Pratique OEB\footnote{http://www.parti-socialiste.fr/tic/ps-tic\_2002.php}}}
\filbreak

\item
{\bf {\bf Jean-Pierre Chev\`{e}nement 2002\footnote{http://www.chevenement2002.net/dit/index.php?idcle=85}}}

\begin{quote}
L'extension du domaine des brevets aux logiciels, absurdit\'{e} \'{e}conomique qui handicaperait nos entreprises et freinerait le d\'{e}veloppement de ce secteur, doit \^{e}tre refus\'{e}e.
\end{quote}
\filbreak

\item
{\bf {\bf Telepolis: Softwarepatente? Non merci!\footnote{http://lists.ffii.org/archive/mails/swpat/2002/Mar/0040.html}}}

\begin{quote}
news report by a leading German IT policy magazine, leads readers in the forum to exclaim ``Vive La France!''
\end{quote}
\filbreak

\item
{\bf {\bf Documents du Gouvernement Fran\c{c}ais\footnote{http://www.telecom.gouv.fr/dp/brevetlogiciel.pdf}}}
\filbreak

\item
{\bf {\bf Rapport du Conseil des Mines\footnote{http://www.minefi.gouv.fr/minefi/actualites/index.htm}}}
\filbreak

\item
{\bf {\bf Table Ronde Brevets Logiciels 1999\footnote{http://www.finances.gouv.fr/societe\_information/tables/CR-22oct99-breveta.htm}}}
\filbreak

\item
{\bf {\bf Lib\'{e}ration 2002-03-12: Tous les Candidats contre le Brevet Logiciel\footnote{http://www.liberation.fr/quotidien/semaine/020312-040031013ECON.html}}}

\begin{quote}
Collection de citations de Jospin, Chirac, Mam\`{e}re, Chevenement, les lib\'{e}raux et les communistes, qui touses disent avec des raisonnements diff\'{e}rents que les brevets logiciels sont une mauvaise id\'{e}e
\end{quote}
\filbreak

\item
{\bf {\bf CEC \& BSA 2002-02-20: proposal to make all useful ideas patentable\footnote{http://swpat.ffii.org/papri/eubsa-swpat0202/index.en.html}}}

\begin{quote}
The European Commission (CEC) proposes to legalise the granting of patents on computer programs as such in Europe and ensure that there is no longer any legal foundation for refusing american-style software and business method patents in Europe.   ``But wait a minute, the CEC doesn't say that in its press release!'' you may think.  Quite right!  To find out what they are really saying, you need to read the proposal itself.  But be careful, it is written in an esoteric Newspeak from the European Patent Office (EPO), in which normal words often mean quite the opposite of what you would expect.  Also you may get stuck in a long and confusing advocacy preface, which mixes EPO slang with belief statements about the importance of patents and proprietary software, implicitely suggesting some kind of connection between the two.  This text disregards the opinions of virtually all respected software developpers and economists, citing as its only source of information about the software reality two unpublished studies from BSA \& friends (alliance for copyright enforcement dominated by Microsoft and other large US companies) about the importance of proprietary software.  These studies do not even deal with patents!  The advocacy text and the proposal itself were apparently drafted on behalf of the CEC by an employee of BSA.  Below we cite the complete proposal, adding proofs for BSA's role as well as an analysis of the content, based on a tabular comparison of the BSA and CEC versions with a debugged version based on the European Patent Convention (EPC) and related doctrines as found in the EPO examination guidelines of 1978 and the caselaw of the time.  This EPC version help you to appreciate the clarity and wisdom of the patentability rules in the currently valid law, which the CEC's patent lawyer friends have worked hard to deform during the last few years.
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: mlatex ;
% End: ;

