<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: France 2002-03-01: EU Commission Directive Proposal Unacceptable

#descr: The french government has crticised the CEC/BSA Software Patentability Directive Proposal at the European Internal Market Council's meeting of 2002-02-28, in which commissioner Bolkestein explained this proposal to the patent policy representatives of the European Union's member states.  France expressed dismay about the proposal's apparent failure to outline a clear limit on patentability, to critically assess the economic effects of the EPO's recent practise, to take serious research into account and to provide a valid rationale in view of european public policy targets such as those laid down in the e.Europe plan.  Industry minister Christian Pierret immediately published this position in a press release.

#DrT: The Text

#MTE: Minister for Industry, Small and Medium Enterprises, Commerce, Crafts and Consumption

#CWn: Press Release

#Pea: Paris, 1st of March 2002

#CeW: Christian PIERRET, the minister of Industrie, SMEs, Commerce, Crafts and Consumption has notified the European Commission of the french government's position concerning the draft for a directive on the patentability of software which was presented today to the council of the internal market.  Finding that the directive draft does not bring any of the expected clarifications concerning the limits of obtainable patentability, the government is worried about the scope of patentability which could be made open to all software and consequently to pure thought and business methods.  However it has appeared clearly in France as well as in Europe that such an extension is largely rejected.

#LtW: France has made it clear that she finds it essential to account for the merits and demerits of the protection of software resulting from the current practise of the European Patent Organisation (EPO) and its member states.  The french government wants to avoid any draft that could have negative consequences on innovation, interoperability and free software as well as on the actors as a whole (publishers, integrators, users), especially the SMEs.  It believes that the approach of the directive fails to respond adequately to the economic, scientific and cultural issues of the software sector as well as to the need to promote innovation which is one of the priorities of the %(q:e.Europe) action plan.  The Commission has conducted studies and a consultation in the last third of the year 2000.  The directive draft presented to the member states however does not clearly assess the risks which a legitimation of the practise of the EPO in the member states would bring, in comparison to the advantages.  Various studies and reports conducted in several member states have appeared rather unenthusiastic about such an evolution.  At the initiative of France, among others, the Diplomatic Conference for the Revision of the European Patent Convention (EPC) which was held in Munich in november 2000 had decided not to change the rulings of the European Patent Convention on this subject, wishing that a clear European position could be found on the basis of a precise analysis of the economic, technical and legal consequences.

#Cap: Press Contact

#Cea: Cabinet of Christian PIERRET

#LeW: Laurence GAUNE

#t1l: phone

#TWs: The extension of the scope of patentability to software, an economic absurdity which would handicap our enterprises and stifle development in this sector, must be denied.

#LWn: Libération 2002-03-12: Alle French Parties and Candidates against Software Patents

#Cen: Collection of quotes from Jospin, Chirac, greens, liberals, communists etc who all explain with different rationales that software patents are bad and EU proposals in this direction must be opposed.

#ryr: news report by a leading German IT policy magazine, leads readers in the forum to exclaim %(q:Vive La France!)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: france020301 ;
# txtlang: en ;
# multlin: t ;
# End: ;

