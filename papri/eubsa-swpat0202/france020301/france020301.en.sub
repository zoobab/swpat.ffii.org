\begin{subdocument}{france020301}{France 2002-03-01: EU Commission Directive Proposal Unacceptable}{http://swpat.ffii.org/papers/eubsa-swpat0202/france020301/index.en.html}{Workgroup\\swpatag@ffii.org}{The french government has crticised the CEC/BSA Software Patentability Directive Proposal at the European Internal Market Council's meeting of 2002-02-28, in which commissioner Bolkestein explained this proposal to the patent policy representatives of the European Union's member states.  France expressed dismay about the proposal's apparent failure to outline a clear limit on patentability, to critically assess the economic effects of the EPO's recent practise, to take serious research into account and to provide a valid rationale in view of european public policy targets such as those laid down in the e.Europe plan.  Industry minister Christian Pierret immediately published this position in a press release.}
\begin{sect}{text}{The Text}
Minister for Industry, Small and Medium Enterprises, Commerce, Crafts and Consumption

Press Release

http://www.industrie.gouv.fr/accueil.htm

Paris, 1st of March 2002

Christian PIERRET, the minister of Industrie, SMEs, Commerce, Crafts and Consumption has notified the European Commission of the french government's position concerning the draft for a directive on the patentability of software which was presented today to the council of the internal market.  Finding that the directive draft does not bring any of the expected clarifications concerning the limits of obtainable patentability, the government is worried about the scope of patentability which could be made open to all software and consequently to pure thought and business methods.  However it has appeared clearly in France as well as in Europe that such an extension is largely rejected.

France has made it clear that she finds it essential to account for the merits and demerits of the protection of software resulting from the current practise of the European Patent Organisation (EPO) and its member states.  The french government wants to avoid any draft that could have negative consequences on innovation, interoperability and free software as well as on the actors as a whole (publishers, integrators, users), especially the SMEs.  It believes that the approach of the directive fails to respond adequately to the economic, scientific and cultural issues of the software sector as well as to the need to promote innovation which is one of the priorities of the ``e.Europe'' action plan.  The Commission has conducted studies and a consultation in the last third of the year 2000.  The directive draft presented to the member states however does not clearly assess the risks which a legitimation of the practise of the EPO in the member states would bring, in comparison to the advantages.  Various studies and reports conducted in several member states have appeared rather unenthusiastic about such an evolution.  At the initiative of France, among others, the Diplomatic Conference for the Revision of the European Patent Convention (EPC) which was held in Munich in november 2000 had decided not to change the rulings of the European Patent Convention on this subject, wishing that a clear European position could be found on the basis of a precise analysis of the economic, technical and legal consequences.

\begin{description}
\item[Press Contact:]\  -  Cabinet of Christian PIERRET Laurence GAUNE phone: 01 53 18 44 85
\end{description}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf All French Presidential Candidates against Software Patentability\footnote{http://lists.ffii.org/archive/mails/news/2002/Mar/0005.html}}}

\begin{quote}
The top candidates of all french parties have expressed themselves clearly and even fervently against software patents.  Each candidate seems happy to find that his particular political thinking can serve as a good basis for explaining why  software patents are bad.
\end{quote}
\filbreak

\item
{\bf {\bf Parti Socialiste contre Brevets Logiciels et Pratique OEB\footnote{http://www.parti-socialiste.fr/tic/ps-tic\_2002.php}}}
\filbreak

\item
{\bf {\bf Jean-Pierre Chev\`{e}nement 2002\footnote{http://www.chevenement2002.net/dit/index.php?idcle=85}}}

\begin{quote}
The extension of the scope of patentability to software, an economic absurdity which would handicap our enterprises and stifle development in this sector, must be denied.
\end{quote}
\filbreak

\item
{\bf {\bf Telepolis: Softwarepatente? Non merci!\footnote{http://lists.ffii.org/archive/mails/swpat/2002/Mar/0040.html}}}

\begin{quote}
news report by a leading German IT policy magazine, leads readers in the forum to exclaim ``Vive La France!''
\end{quote}
\filbreak

\item
{\bf {\bf Documents du Gouvernement Fran\c{c}ais\footnote{http://www.telecom.gouv.fr/dp/brevetlogiciel.pdf}}}
\filbreak

\item
{\bf {\bf Rapport du Conseil des Mines\footnote{http://www.minefi.gouv.fr/minefi/actualites/index.htm}}}
\filbreak

\item
{\bf {\bf Table Ronde Brevets Logiciels 1999\footnote{http://www.finances.gouv.fr/societe\_information/tables/CR-22oct99-breveta.htm}}}
\filbreak

\item
{\bf {\bf Lib\'{e}ration 2002-03-12: Alle French Parties and Candidates against Software Patents\footnote{http://www.liberation.fr/quotidien/semaine/020312-040031013ECON.html}}}

\begin{quote}
Collection of quotes from Jospin, Chirac, greens, liberals, communists etc who all explain with different rationales that software patents are bad and EU proposals in this direction must be opposed.
\end{quote}
\filbreak

\item
{\bf {\bf CEC \& BSA 2002-02-20: proposal to make all useful ideas patentable\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/index.en.html}}}

\begin{quote}
The European Commission (CEC) proposes to legalise the granting of patents on computer programs as such in Europe and ensure that there is no longer any legal foundation for refusing american-style software and business method patents in Europe.   ``But wait a minute, the CEC doesn't say that in its press release!'' you may think.  Quite right!  To find out what they are really saying, you need to read the proposal itself.  But be careful, it is written in an esoteric Newspeak from the European Patent Office (EPO), in which normal words often mean quite the opposite of what you would expect.  Also you may get stuck in a long and confusing advocacy preface, which mixes EPO slang with belief statements about the importance of patents and proprietary software, implicitely suggesting some kind of connection between the two.  This text disregards the opinions of virtually all respected software developpers and economists, citing as its only source of information about the software reality two unpublished studies from BSA \& friends (alliance for copyright enforcement dominated by Microsoft and other large US companies) about the importance of proprietary software.  These studies do not even deal with patents!  The advocacy text and the proposal itself were apparently drafted on behalf of the CEC by an employee of BSA.  Below we cite the complete proposal, adding proofs for BSA's role as well as an analysis of the content, based on a tabular comparison of the BSA and CEC versions with a debugged version based on the European Patent Convention (EPC) and related doctrines as found in the EPO examination guidelines of 1978 and the caselaw of the time.  This EPC version help you to appreciate the clarity and wisdom of the patentability rules in the currently valid law, which the CEC's patent lawyer friends have worked hard to deform during the last few years.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
% mode: latex ;
% End: ;

