<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: CEC & BSA 2002-02-20: proposta per a fer totes les bones idees
patentables

#descr: La Comissió Europea (CE) proposa de legalitzar la concessió de patents
de programes d'ordinador com a tals a Europa i assegurar que ja no hi
hagi cap fonament legal per a refusar l'estil americà  de patents de
programari i mètodes de negoci a Europa.   %(q:Però espereu un moment,
la CE no diu això al comunicat de premsa!) podeu pensar.  És cert! 
Per a trobar el que diu realment, heu de llegir la mateixa proposta. 
Però amb compte, està escrita en un nou llenguatge esotèric de la
Oficina Europea de Patents (OEP), on les paraules normals sovint volen
dir el contrari del què s'esperaria.  També t'has de ficar en un
prefaci llarg i confús, que barreja argot de l'OEP amb creences sobre
la importància de les patents i el programari propietari, suggerint
implícitament alguna connexió entre ambdós.  Aquest text oblida les
opinions de gairebé tots els respectats desenvolupadors i economistes,
citant com a única font d'informació sobre la realitat del programari
dos estudis no publicats de la BSA i col·legues (aliança pels drets
d'autor dominada per Microsoft i d'altres grans empreses americanes)
sobre la importància del programari propietari.  Aquests estudis ni
tan sols parlen de patents!  El text de defensa i la proposta mateixa
sembla que les va redactar, en nom de la CE, un empleat de la BSA.  A
sota, citem la proposta completa, afegint-hi proves del rol de la BSA
com també una anàlisi del contingut, basat en una comparació tabular
de les versions de la BSA i de la CE amb una versió corregida basada
en la Convenció Europea de Patents (CEP) i doctrines relacionades tal
com es troben a les línies d'actuació d'examinació de l'OEP del 1978 i
la jurisprudència d'aquell temps.  Aquesta versió de CEP ens ajuda a
apreciar la claredat i el seny a les lleis de patentabilitat
actualment vàlides, les quals els amics dels advocats de patents de la
CE han treballat dur aquests últims anys per deformar-les.

#TCp: Edició de text tabular a les versions de la BSA i la CE amb
contra-proposta amb l'esperit de l'EPC

#HCd: Aquí presentem una edició crítica dels texts, amb anotacions i una
comparació tabular entre l'esborrany inicial i final de la CE/BSA i
una versió afegida de la FFII, que mostra què va malament amb la
versió de la CE/BSA i com s'hauria de reescriure de manera coherent i
adequada. Ressaltem algunes diferències en negreta.

#Pad: Proposal to make all useful ideas patentable, based on a draft from
BSA, released by the European Commission on 2002-02-20

#Uma: Upon Release of its Software Patentability Directive Proposal (based
on a draft by BSA), the European Commission lies to the press and to
the world about the contents of this Directive Proposal, trying to
create the impression that this directive proposal excludes patents on
business methods and software as such.

#Aey: An text by the European Commission's (CEC) software patentability law
drafters, designed to (dis)inform journalists and the general public
about what is at stake and to soothe widespread fears that the CEC
might be legalising unwanted software and business method patents. 
The text tries to achieve this by using EPO/UKPO Patent Newspeak, in
which normal phrases may have two radically different meanings, one
for consupmtion by politicians, journalists and citizens, i.e. the
readers of this FAQ, and another as understood by patent
professionals.

#Jro: JURI working documents

#WBt: Working documents of the Legal Affairs Commission of the European
Parliament about the CEC/BSA software patentability proposal of
2002-02-20, published 2002-06-19, inititially consisting of a short
report by MEP Arlene McCarthy and a study ordered by the European
Commission.

#Pi1: Lenz 2002-03-01: Sinking the Software Patent Proposal

#lpa: Karl-Friedrich Lenz, professor of European Law, lists some legal and
constitutional arguments to explain why the CEC/BSA proposal is a
legal and political scandal, starting from the fact that the European
Commission is using %(q:harmonisation) and %(q:clarification) merely
as pretexts to declare itself competent for promoting an unspeakable
political agenda which does not fall in the Commission's competentce.

#DxW: Datamonitor 2000-09: Packaged Software Industry in Europe

#ArW2: A rarely cited and largely unknown study by a company called
Datamonitor about the importance of proprietary software as a creator
of jobs in Europe.  The study claims that proprietary software will
create 1/2 million new jobs in the next few years.  Praises Ireland as
a tiger state which is profiting from this job miracle thanks to its
low tax rates.  The original of this study seems to be inaccessible on
the Net.  Various summaries and references have been published on
Microsoft's website in the context of Microsoft's lobbying work.  In
2002 this study found its way into the advocacy preface of the
European Commission's software patentability proposal.  This was
apparently due to the influence of BSA in the drafting work.  The
study does not deal with the subject of software patents.  It
correctly states that the biggest asset of software companies is their
manpower, i.e. their ability to manage complex copyrighted works and
quickly turn out nifty software rather than in their patents or their
ability to invent a mousetrap.

#MWw: Microsoft: The growth of the packaged software industry in Norway

#AWi2: An example of Microsoft's use of the Datamonitor Study for political
persuasion efforts in Norway.

#Aoh: A controversial debate between two people about the directive proposal
and the FFII criticism.  The %(q:anonymous coward) is Erik Josefsson
from SSLUG, his opponent is a patent lawyer from Philips who maintains
a %(im:website which argues in favor of the EPO's recent
interpretation of the EPC) (and thus basically also of the directive
proposal).

#pnc: press release of Linux-Verband, an association of Linux companies and
users in Germany

#Mmn: Mingorance denies our allegations that he wrote the proposal, arguing
that he would have written the %(q:form of claims) section
differently.  We think that the decision to not follow the EPO on the
claim form question was indeed taken by the CEC.  We do not doubt that
the CEC played an important role in shaping the proposal.  But so did
Mingorance.

#AWg: An journalist reports that he just phonecalled the European Commission
to find out whether the BSA version that Eurolinux proposed was really
the version that the CEC was going to publish that day.  He was told
by the Commission's press speaker that the version which Eurolinux
published was not theirs but one %(q:from the industry).  Several
French journalists who called on the same morning received the same
answer.  One even received the answer %(q:no, that draft is not from
us but from BSA).

#AvA: A fairly comprehensive account of the BSA/CEC proposal scandal

#CcB: CEC Press Release inconsistent with CEC/BSA directive content

#Eui: Eurolinux Warning to journalists issued on 2002-02-20 shortly after
publication of CEC/BSA directive proposal

#Rsr: Robin Webb (UK PTO & Gov't) 2002-02-20: proposes to remove all limits
to patentability and to rewrite Art 52 EPC so as to reflect EPO
practise

#Tla: The UK PTO conducted its own consultation, which showed an
overwhelming wish of software professionals to be free of software
patents.  But the UK PTO, speaking in the name of the UK government,
reinterprets this as a legitimation to remove all limits on
patentability by modifying Art 52 EPC at the Diplomatic Conference in
June 2002.  The %(pr:proposal) has one argument going for it:  while
rendering Art 52 meaningless, it at least brings clarity.  It
describes in fairly straightforward (and yet still sufficiently
deceptive) terms what the EPO is doing and what the patent movement
wants.  This paper was presented by the UKTPO on the same day as the
EU-BSA Directive Proposal.  The UKPTO had held a parallel consultation
exercise in Great Britain.  Its exercise ignored public opinion using
the same double-talk about %(q:technical contribution) and by
selective choice of examples made sure that this talk is misunderstood
by the public.  Yet the UKPTO has shown the Commission that even
massive fraud and %(e:theft of intellectual property with with a
further legal effect), jointly committed by the EPO, the CEC and the
UKPTO against the european software industry and the european public,
can be carried out in a graceful and mannered fashion.

#Ftr: French news report saying that the CEC proposal fails to bring any
clarity to the debate

#Jti: Joint statement of the European shareware associations condemning the
directive proposal as a hostile act against the creators of software
in Europe

#Gyu: German MEP of GUE/NGL strongly warning against Copylock (EuroDMCA) and
Swpat Directives

#Prc: Pointers to more press reports, especially from France

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-swpat0202 ;
# txtlang: ca ;
# multlin: t ;
# End: ;

