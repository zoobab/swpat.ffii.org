\begin{subdocument}{eubsa-opin}{Opinions on CEC/BSA Software Patent Directive Proposal}{http://swpat.ffii.org/papers/eubsa-swpat0202/eubsa-opin.en.html}{Workgroup\\swpatag@ffii.org}{Comments from politicians, associations, institutions etc}
\begin{itemize}
\item
{\bf {\bf McCarthy 2003/06/12: Letter to the Guardian}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy030612/index.en.html}}

\begin{quote}
Arlene McCarthy, Member of the European Parliament for UK labour and rapporteur of the JURI committeee for the software patent directive proposal, has for the first time directly answered arguments from critics in a letter to the british newspaper The Guardian.  Yet the basic questions, e.g. what should be patentable and how McCarthy's proposals achieve this, remain unanswered.  McCarthy reiterates demagogic statements of whose untruth she is well-informed and even resorts to lies in the strictest sense of the term, such as saying that she introduced a provision to allow decompilation.  McCarthy moreover attacks the GNU General Public License in an apparent effort to shift focus to unrelated subjects and incite flamewars with the free software community.  We analyse McCarthy's fallacies and the political context of her letter.
\end{quote}
\filbreak

\item
{\bf {\bf McCarthy 2003/05/03: Software Patent Directive Proposal FAQ}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy0305/index.en.html}}

\begin{quote}
Arlene McCarthy, member of the European Parliament and rapporteur of the Legal Affairs Commission (JURI) on the Software Patentability Directive Proposal explains her point of view in a FAQ manner.  She asked us to distribute this document to the participants of a conference which we organised in Brussels.  In a nutshell, she says that \begin{enumerate}
\item
Software patents, as granted by the European Patent Office (EPO) at present, are needed for various reasons, e.g. protecting Europe against competition from Asia, allowing European SMEs to compete in the US, etc

\item
The EPO is granting software patents but not patents on non-technical algorithms and business methods.

\item
The current proposal is designed to ensure that the EPO's practise is followed throughout Europe in a uniform manner and that a drift toward patenting of ``non-technical algorithms and business methods'' is halted.
\end{enumerate}  We debug McCarthy's questions and answers one by one.  It seems that many of McCarthy's proclaimed objectives could be achieved only by voting in favor of a series of CULT, ITRE and JURI amendment proposals which unfortunately have not enjoyed the support of the rapporteur.
\end{quote}
\filbreak

\item
{\bf {\bf IV-net.at 2003/04/24: Letter to Austrian EuroMPs}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/ivat0304/index.de.html}}

\begin{quote}
Stephan Maras, speaker for the Industrial Property Commission of the Austrian Industry Association IV-net.at, demands in the name of machine engineering and electronics companies that not only programmed industrial processes but also pure data processing programs must be directly patentable and claimable as texts.  MEP Malcolm Harbour (UK, PPE/Conservatives) will table an amendment which allows text claims.  Moreover Maras asserts that copyright is suitable only for poems, whereas in the case of computer programs it is easy to circumvent -- mere translation from C to Perl is enough.  Maras sent this statement to Austrian MEPs together with a ``Joint Statement of Industry'' from major european associations which argues in the same sense.
\end{quote}
\filbreak

\item
{\bf {\bf Unice/Eicta/ICC etc 2003/05/22: ``Joint Statement of the Industry'' for Software Patents}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/ipat0304/index.en.html}}

\begin{quote}
In a ``Joint Statement of the Industry'', directed to the Members of the European Parliament (MEPs), the presidents of various industry associations, including Graham Taylor from the ``Open Forum Europe'' as a representative of the Linux/Opensource world, asks the legislators to ensure \begin{enumerate}
\item
that computer programs are treated as patentable inventions and

\item
programs are directly claimable, so that programmers can be sued for publishing a program.
\end{enumerate}  Moreover, as means of ``protecting opensource software'', the signatories ask the European Parliament to ensure that \begin{enumerate}
\item
whenever an interface is patented, interoperable software may not be published or used without a license

\item
business methods are patentable ``only'' to the extent that a computer or some other device is involved

\item
the European Commission shall publish more papers (obituaries?) about the effects of patents on SMEs.
\end{enumerate} This statement was sent to many MEPs, together with accompanying letters from the patent arms of national industry associations.   Traditionally industry associations have left patent politics completely to corporate patent lawyers, who typically form each association's ``industrial property committee''.  This committee's papers are usually signed by the president of the association without further consultation of other committees.  Gentle doublespeak with an ungentle insider meaning, as explained above, is also part of the game.  What is unusual about the current letter is that an apparent ``opensource community leader'' was enlisted for the maneuver.
\end{quote}
\filbreak

\item
{\bf {\bf JURI 2003/04-6 Amendments: Real and Fake Limits on Patentability}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/juri0304/index.en.html}}

\begin{quote}
Members of the European Parliament's Commission on Legal Affairs and the Internal Market (JURI) submitted amendments to the European Commission's software patent directive proposal. While some MEPs are asking to bring the directive in line with Art 52 EPC so as to clearly restate that programs for computers are not patentable inventions, another group of MEPs is endorsing the EPO's recent practice of unlimited patentability, shrouded in more or less euphemistic wordings. Among the latter, some propose to make programs directly claimable, so as to ensure that software patents are not only granted but achieve maximal blocking effects.  This latter group obtained a 2/3 majority, with some exceptions.  We document in tabular form what was at stake, what various parties recommended, and what JURI finally voted for on 2003/06/17.
\end{quote}
\filbreak

\item
{\bf {\bf McCarthy 2003-02-19: Amended Software Patent Directive Proposal}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy0302/index.en.html}}

\begin{quote}
Arlene McCarthy, British Labor MEP appointed by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI) to report on the European Commission's Software Patentability Directive Proposal (CEC/BSA Proposal), suggests that the European Parliament should enact the CEC/BSA version with additional safeguards to align Europe on the US practise and make sure that there can be no limit on patentability.  McCarthy reiterates the CEC/BSA software patent advocacy and misrepresents the wide-spread criticism without citing any of it.  Even economic and legal expertises ordered by the European Parliament and other critical opinions of EU institutions are not taken into account.  McCarthy's economic argumentation consists of tautologies and unfounded assertions, such as that companies like Ericsson and Alcatel need software patents to finance their R\&D, that SMEs need european software patents in order to compete in the USA, that patents are needed to keep developping countries at bay.  McCarthy uses the term ``computer-implemented inventions'' as a synonym for ``software innovations''.  These ``by their very nature belong to a field of technology''.  McCarthy insists that ``irreconcilable conflicts'' with the EPO must be avoided.  McCarthy says she wants to ``set clear limits as to what is patentable'' -- and that she wants to avoid the ``sterile discussions'' about ``technical effects'' and ``exclusions from patentability''.  Yet her proposal stays confined to such discussions.  McCarthy demands that all useful ideas, including algorithms and business methods, must be patentable as ``computer-implemented inventions''.  McCarthy proposes to recognise the EPO as Europe's supreme patent legislator and to make decisions of a few influential people at the EPO irreversible and binding for all of Europe.
\end{quote}
\filbreak

\item
{\bf {\bf Plooij/ITRE Counter-Proposal: Publication and Interoperation do not Infringe}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/itre0212/index.en.html}}

\begin{quote}
The European Parliament's industry committee (ITRE) is working on a counter-proposal for the CEC/BSA software directive proposal, based on a draft by its rapporteur Elly Plooij Van Gorsel, a dutch liberal Member of the European Parliament (MEP).  Plooij's first draft states that patents are not needed for protecting the interests of software creators and proposes to assure that programs can at least be freely distributed without any fear of patent liabilities.  Moreover, any use of a patented method for the purpose of interoperability (compliance with de facto standards) is not considered to be an infringement.  The proposal is strong on defininig these limits but stops short of correcting CEC/BSA misconceptions about ``computer-implemented invention'', ``technical contribution'' etc, thus leaving it undecided whether Amazon One-Click and the like are patentable inventions or not.  Current law says they are not, but the European Patent Office (EPO) says they are, and the European Commission wants to bring the law in line with the EPO's position.  The ITRE counter-proposal solves only part of the problem, but fellow MEPs in ITRE have tabled amendments.  Some of these are designed to complete the solution, others to dismantle it.
\end{quote}
\filbreak

\item
{\bf {\bf Rocard/CULT 2002-12-09: Data Processing is Not a Field of Technology}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/cult0212/index.en.html}}

\begin{quote}
The chairman of the European Parliament's Cultural Affairs Commission (CULT), Michel Rocard, proposes amendments to the Software Patentability Directive which are intended to exclude patents on program logic and limit patentability to the field of engineering with forces of nature.  Rocard aptly speaks the TRIPs idiom of the European Commission.  Rocard's amendments constitute the first counter-proposal of an official EU institution where the content actually corresponds to the packaging.  It is less ambitious than the FFII counter-proposal and leaves more loopholes.  If enacted, it would preserve the systematics of the current law (Art 52 EPC) and thereby oblige the patent offices to achieve those very aims which hard-pressed patent politicians in Brussels, London, Berlin and elsewhere always say are theirs.
\end{quote}
\filbreak

\item
{\bf {\bf ESC 2002-09: Europe should reconfirm Non-Patentability of Software!}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/esc0209/index.en.html}}

\begin{quote}
The Economic and Social Council of the European Union, a consultative organ of experts from various fields, criticises the European Patent Office's software caselaw and the European Commission's proposal for a software patentability directive and asks the European Parliament to reject the proposal and instead ask for a reconfirmation of the non-patentability of software.  This study met strong resistance from a group of supporters of the European Patent Office, but was in the end passed with a 2/3 majority.
\end{quote}
\filbreak

\item
{\bf {\bf CEU/DKPTO 2002/09/23..: Software Patentability Directive Amendment Proposal}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/dkpto0209/index.en.html}}

\begin{quote}
The Council of the European Union (CEU) proposes to rewrite some articles of the CEC/BSA proposal of 2002/02/20 in order to take into account various criticisms made by national delegations to the Council's Intellectual Property Working Party, a workgroup consisting of delegates from national patent administrations.  This counter-proposal was worked out by the delegates from Denmark, i.e. from the Danish Patent Office (DKPTO), which are presiding over the workgroup during the second half of 2002.  The paper is the subject of decision at the Working Party's session on 2002/10/03 in Brussels.  We present the paper in tabular comparison with the original CEC/BSA proposal of 2002/02/20.  It becomes evident that the DKPTO proposal, while strengthening the rhetorical emphasis on the ``technical contribution'', creates additional ambiguities and in effect further widens the scope of patentability.
\end{quote}
\filbreak

\item
{\bf {\bf Monopoly Commission 2002-07-08 report warns against software patents and CEC directive proposal}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/mopoko0207/index.de.html}}

\begin{quote}
The German Monopoly Commission, a consultative state organ loosely associated with the Federal Cartel Office and the Ministry of Economics, has published its 14th main report (Hauptbericht) about the state of concentration in the German Economy.  This report warns that software patents stifle innovation and competition, points out that the practise of the European Patent Office is ``incompatible with Art 52 EPC'' and asks the German government to resist the European Commission's current attempts at legalising software patents.
\end{quote}
\filbreak

\item
{\bf {\bf EPO 2002-06-21: report to SACEPO on CEC/BSA proposal}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/epo020621/index.en.html}}

\begin{quote}
The European Patent Office (EPO) presents a preliminary position on the proposal for a European Directive on the patentability of software innovations and asks its Standing Advisory Committee (SACEPO), a council consisting mainly of corporate patent lawyers, for expression of opinions. The EPO paper refers to the Eurolinux campaign as a ``fundamentalist'' position promoted by a ``strong lobby''.  It points out that the CEC/BSA proposal is largely but not completely based on EPO positions and names some points where further clarification would be helpful.  It also explains the state of deliberations in the Council Working Group and the European Parliament and discloses that the EPO has a seat on the ``council working party on intellectual property'' as a delegate of the European Commission.  The Eurolinux Alliance wonders why it cannot have a seat on this ``working party'' and on SACEPO.
\end{quote}
\filbreak

\item
{\bf {\bf DG IV Bakels 2002-06-19: The Patentability of Computer Programs}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/dgiv0206/index.en.html}}

\begin{quote}
A study on software patentability which was commissioned by the European Parliament's Research Directorate as a reference for deliberations about the European Commission's proposal to make all useful ideas patentable.  Unlike most other studies commissioned by EU institutions, this one does not seek to hide the problems with software patents and takes a refreshingly undogmatic look.  Bakels finds that the proposed directive fails on all its proposed goals (clarification, harmonisation etc) and contains numerous inconsistencies.  Concerning the impact of software patents, Bakels finds that ``it is crucial to change the long-standing tradition of patents being granted for relatively simple inventions''.  Yet Bakels offers no hints as to how this long-standing problem can be solved and does not properly assess how some other possible filters such as concreteness and physical substance (technical character) or ``invention character'' (laid out in Art 52 EPC) relate to it.
\end{quote}
\filbreak

\item
{\bf {\bf MEP Arlene McCarthy 2002-06-19: Report on the CEC/BSA Directive Proposal}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy0206/index.en.html}}

\begin{quote}
In a democratic Europe, one would expect the legislative power, such as the European Parliament, to critically monitor the activities of the executive and judicial powers, such as the European Commission (CEC) and the European Patent Office (EPO), and to correct their shortcomings.  The british Labor MEP Arlene McCarthy, as a rapporteur to the European Parliament's Committee for Legal Affairs and the Internal Markt on the proposed software patentability directive COM(02) 92, however merely reaffirms the beliefs of the patent lawyers who have been dominating the dossier at the CEC and the EPO.  McCarthy's report disregards the opinions of virtually all respected programmers and economists and fails to correctly identify the controversial issues.  Yet at the end McCarthy asks a few questions which provide us with a certain hope that eventually a more open debate can be brought about.  Please read our answers below.
\end{quote}
\filbreak

\item
{\bf {\bf Bundestag 2002-05-14: CDU/CSU Enquete about Software Patents}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/cducsu020514/index.de.html}}

\begin{quote}
Nachdem die Bundesregierung lange Zeit geschwiegen und auch die \"{o}ffentliche Anfrage ihres Medienexperten MdB J\"{o}rg Tauss nicht beantwortet hat, fragt die unionschristliche Opposition unter Federf\"{u}hrung ihres Medienexperten MdB Dr. Martin Mayer die Bundesregierung, ob der Br\"{u}sseler Richtlinienvorschlag ihrer Meinung nach der F\"{o}rderung von Innovation und Forschung dienlich und hinreichend klar gefasst ist.  Wie schon anl\"{a}sslich seiner Anfrage vom Herbst 2000 gelingt es Mayer, im wesentlichen klare Gedanken zu fassen.  Allerdings weist der Text auch Unzul\"{a}nglichkeiten auf, die wir in dieser Rezension aufzeigen.
\end{quote}
\filbreak

\item
{\bf {\bf DE Green Party: CEC patentability proposal harmful}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/gruene020513/index.en.html}}

\begin{quote}
While the german government has not found a clear position on software patents and has been named as an ally by patent lobby circles in Brussels, the delegates for economic and media policy of the parliamentary group of the Green Party, junior ally of the Social Democrats in the governing coalition, has published a declaration which unambiguously rejects the Brussels draft.  It says inter alias \begin{quote}
{\it The directive draft of the European Commission is heading for the wrong direction. Software patents impede innovation, free software developpers, Open Source and SMEs. ... The Commission has, without giving due consideration to the economic studies which were presented to it, published a premature law proposal.}
\end{quote}  In principle, the Green Party welcomes the opportunity to clarify the legal situation:  \begin{quote}
{\it We have in thepast witnessed a silent erosion of the legal provisions against patenting software.  Therefore we must now again clarify that software is not patentable.  Continued legal insecurity is damaging to enterprises.}

{\it Software development does not fall into the category of inventing but rather of skillful development of large works, just as the term suggests.  It makes no sense to extend a legal instrument designed for technical inventions into the area of software, which is what the EU Commission's proposal actually does.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf CEC \& BSA trying to impose unlimited patentability on Sweden}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/sslug0205/index.en.html}}

\begin{quote}
In a statement submitted to the Swedish Ministry of Justice on behalf of SSLUG, a group of 6100 programmers and users of free software in the area around Copenhagen and Malm\"{o}, Erik Josefsson shows how an influential group at the European Commission and the European Patent Office has eroded the standards of patentability and is trying to impose a regime of patentability on all achievements of the human mind that can help to solve some practical problem.  This influential group has also, by overstretching the competence of the EPO's Technical Boards of Appeal, illegally overruled the Swedish courts and damaged the Swedish constitutional order.  Even in their most recent decisions in the mid-nineties, the Swedish courts did not agree with the EPO's illegal practice, but now the European Commission is set to force this practice on Sweden by means of ``european harmonisation''.  It was the duty of the EPO to abide by a role of ``cold harmonisation'' in the first place: act as a conservative follower and summarizer of national caselaw rather than as an innovative trendsetter pursuing its own agenda.  Josefsson cites ample examples of patents granted by the EPO and rejected by Swedish courts.
\end{quote}
\filbreak

\item
{\bf {\bf BDI 2002-04-15 zum EUK/BSA-Entwurf: die Industrie will keine Patentierungsm\"{o}glichkeiten einb\"{u}{\ss}en}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/bdi020415/index.de.html}}

\begin{quote}
Hiermit nimmt der Bundesverband der Deutschen Industrie (BDI) zu dem knapp 2 Monate zuvor von der Europ\"{a}ischen Kommission vorgelegten Entwurf einer Richtlinie \"{u}ber die Patentierbarkeit computer-implementierter Organisations- und Rechenregeln Stellung.  Der BDI sollte satzungsgem\"{a}{\ss} die Meinung seiner Mitgliedsverb\"{a}nde b\"{u}ndeln.  In der Stellungnahme ist von deren durchaus uneinheitlicher Meinungsbildung jedoch \"{u}berhaupt nicht die Rede.  Es werden auch nicht Interessen von Unternehmen artikuliert.  Stattdessen finden sich nur Maximalforderungen der Patentbranche wieder, die mit abstrakten Begriffen \"{u}ber abstrakte Begriffe in einem esoterischen Jargon des Europ\"{a}ischen Patentamtes sprechen und dabei h\"{a}ufig noch \"{u}ber die eigenen Abstraktionen stolpern.  So werden z.B. Formulierungen der Kommission, die auf eine m\"{o}glichst weite Patentierbarkeit zielen, als Einschr\"{a}nkungen der Patentierbarkeit verstanden und bek\"{a}mpft.  Das einzige Interesse der Industrie besteht laut BDI-Papier darin, keinerlei M\"{o}glichkeiten der Patenterlangung und -durchsetzung zu verlieren.  Die Sicht der von Patentdickichten belasteten und bedrohten Unternehmen kommt ebenso wenig zur Sprache wie die (unter Patentjuristen allgemein unbekannte) volkswirtschaftliche Kritik am Patentwesen und seiner Ausdehnung.
\end{quote}
\filbreak

\item
{\bf {\bf German Chamber of Commerce 2002/04/20, 10/04: against Software Patents and CEC Proposal}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/dihk0204/index.de.html}}

\begin{quote}
Press release of the German Chamber of Industry and Commerce, which was sent via OTS to the press organs on 2002-10-04.  In this short text and a preceding longer paper, attorney Doris M\"{o}ller explains that software patents are undesirable and criticises the European Commission's Directive Proposal for failing to exclude software from patentability.
\end{quote}
\filbreak

\item
{\bf {\bf Tauss 2002-03-12: Germany must say No to CEC/BSA Proposal and EPO Practise}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/tauss020312/index.en.html}}

\begin{quote}
J\"{o}rg Tauss, president of the New Media Commission of the German Federal Parliament and in charge of Media and Education in the Social Democratic Party (SPD)'s Parliamentary Fraction, has written a letter to his party colleague and minister of justice Prof. Herta D\"{a}ubler-Gmelin.  Tauss asks Ms D\"{a}ubler-Gmelin to opppose this proposal as resolutely as she opposed the planned modification of Art 52 EPC in november 2000.  This proposal, Tauss says, merely enshrines a highly questionable practise of the European Patent Office (EPO).  There is indeed a need for legislating at the EU level on the limits of patentability, Tauss says, but the EPO practise is the problem, not the solution.  Software patents seem harmful for innovation, incompatible with Europe's strategic interests, dangerous for open source software, less than helpful for proprietary software companies including such bing players as SAP, serving mainly the interests of a few american monopolists behind BSA.  The minister should not be fooled by false assertions about the TRIPS treaty.  The TRIPS treaty is a reason to embrace the strict definition of \emph{technical invention} (teaching about physical causalities), according to which software does \emph{not} constitute or belong to a ``field of technology''.  Tauss founds his explanations on an expert hearing which he together with colleagues from all parties in the parliament conducted in June 2001.  It seems that he represents a consensus position shared at least by most of the members of the parliamentary subcommission on the New Media.  This is confirmed by all publicly known statements given by other MPs so far.  The German Government should take a position similar to that of France and of the German Ministry of Economics (BMWi), Tauss says.  The latter has not been published yet, but it is known that there is widespread skepticism about software patents in the BMWi.
\end{quote}
\filbreak

\item
{\bf {\bf EUK/BSA Richtlinienvorschlag Logikpatente: Anmerkung von Roman Sedlmaier 2002/03}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/sedl0203/index.de.html}}

\begin{quote}
Ein M\"{u}nchener Rechtsanwalt zeigt Widerspr\"{u}che im Richtlinienvorschlag der Europ\"{a}ischen Kommission zur Patentierbarkeit computer-implementierter Organisations- und Rechenregeln auf.  Der Begriff der ``Erfindung'' in doppeldeutiger Weise gebraucht, die Pr\"{u}fung des Vorliegens einer Erfindung werde systemwidrig mit der Frage der Erfindungsh\"{o}he vermengt.  Indem die EU-Kommission zugleich eine ``ganzheitliche Betrachtung'' und deren Gegenteil, n\"{a}mlich eine ``Fokussierung auf einen technischen Beitrag'', vorschreibe, verlange sie von einem Richter unm\"{o}gliches.  Durch diese und andere Ungereimtheiten schaffe der Vorschlag vor allem neue Unklarheiten und verfehle seine vorgegebenen Ziele.  Sofern in Computerprogrammen Erfindungen gesehen werden, sei das Verbot von Programmansp\"{u}chen gem\"{a}{\ss} EU-Vorschlag angesichts Art 27 TRIPs wirkungslos.  Wenn, wie von der EUK behauptet, der Begriff der Technischen Erfindung st\"{a}ndig im Hinblick auf Weiterentwicklungen der Technik im Fluss bleiben m\"{u}sse, so k\"{o}nne es letztlich gar keine sinnvolle RiLi zu diesem Thema geben.   Eine einheitliche Rechtsprechung lasse sich unter diesen Umst\"{a}nden nur durch Einrichtung eines h\"{o}chsten europ\"{a}ischen Patentgerichtes erreichen.  Ein Gro{\ss}teil der Patentjuristen, die sich mit dem EU-Vorschlag besch\"{a}ftigt haben (z.B. BGH-Richter), seufzen hinter vorgehaltener Hand \"{u}ber die Widerspr\"{u}chlichkeit und mangelnde handwerkliche Qualit\"{a}t des EUK/BSA-Entwurfs.   Dem jungen Anwalt Sedlmaier kommt die Rolle zu, das uns\"{a}gliche zu sagen, und die Patentanwaltskammer bietet ihm hierf\"{u}r im M\"{a}rz 2002 sogar eine Trib\"{u}ne, die ihm Beachtung bis in die zust\"{a}ndigen patentjuristischen Abteilungen der Bundesregierung hinein sichert.  Kollegen in Regierung und Verb\"{a}nden f\"{u}hlen sich dadurch ermutigt, aus der Reihe zu tanzen oder eine Abwartehaltung einzunehmen.
\end{quote}
\filbreak

\item
{\bf {\bf France 2002-03-01: EU Commission Directive Proposal Unacceptable}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/france020301/index.en.html}}

\begin{quote}
The french government has crticised the CEC/BSA Software Patentability Directive Proposal at the European Internal Market Council's meeting of 2002-02-28, in which commissioner Bolkestein explained this proposal to the patent policy representatives of the European Union's member states.  France expressed dismay about the proposal's apparent failure to outline a clear limit on patentability, to critically assess the economic effects of the EPO's recent practise, to take serious research into account and to provide a valid rationale in view of european public policy targets such as those laid down in the e.Europe plan.  Industry minister Christian Pierret immediately published this position in a press release.
\end{quote}
\filbreak

\item
{\bf {\bf No\"{e}l Mam\`{e}re 2002-20-28: Let's just delete the ``As Such'' clause!}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/mamere020228/index.en.html}}

\begin{quote}
The Presidency Candidate of the French Green Party joins the Socialist Party and his fellow contender Jean-Pierre Chev\`{e}nement in general rejection of software patents and specifically criticises the software patentability directive proposal of the European Commission of 2002-02-20, demands a return to a strict adherence to the EPC and a removal of the much-abused ``as such'' clause from the EPC and its French equivalent, asks the French government to act firmly based on the principles agreed upon recently by the Socialist Party, says that asserting the law against an illegal patent court practise is of highest political priority because constitutional freedoms are touched and the prosperity of the software industry in Europe is under threat.
\end{quote}
\filbreak

\item
{\bf {\bf Dutch Labor Party 2002-02-20: Criticism of EU Software Patentability Proposal}\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/pvda020220/index.en.html}}

\begin{quote}
Dutch Labor Party criticises that the European Commission's draft in no way fulfills the Dutch Parliament's demands for strict patentability standards and on the contrary is cause of concern and necessitates a lot of sharp questions, which the government should ask Bolkestein.  The PVdA position is based on the recommendations of a joint work group of the largest software industry association FENIT.nl and the Opensource Association VOSN.nl
\end{quote}
\filbreak
\end{itemize}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: mlatex ;
% End: ;

