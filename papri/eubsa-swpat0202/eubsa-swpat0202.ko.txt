<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: The European Commission (CEC) proposes to legalise the granting of patents on computer programs as such in Europe and ensure that there is no longer any legal foundation for refusing american-style software and business method patents in Europe.   %(q:But wait a minute, the CEC doesn't say that in its press release!) you may think.  Quite right!  To find out what they are really saying, you need to read the proposal itself.  But be careful, it is written in an esoteric Newspeak from the European Patent Office (EPO), in which normal words often mean quite the opposite of what you would expect.  Also you may get stuck in a long and confusing advocacy preface, which mixes EPO slang with belief statements about the importance of patents and proprietary software, implicitely suggesting some kind of connection between the two.  This text disregards the opinions of virtually all respected software developpers and economists, citing as its only source of information about the software reality two unpublished studies from BSA & friends (alliance for copyright enforcement dominated by Microsoft and other large US companies) about the importance of proprietary software.  These studies do not even deal with patents!  The advocacy text and the proposal itself were apparently drafted on behalf of the CEC by an employee of BSA.  Below we cite the complete proposal, adding proofs for BSA's role as well as an analysis of the content, based on a tabular comparison of the BSA and CEC versions with a debugged version based on the European Patent Convention (EPC) and related doctrines as found in the EPO examination guidelines of 1978 and the caselaw of the time.  This EPC version help you to appreciate the clarity and wisdom of the patentability rules in the currently valid law, which the CEC's patent lawyer friends have worked hard to deform during the last few years.
title: CEC & BSA 2002-02-20: proposal to make all useful ideas patentable
TCp: Tabular text edition in BSA, CEC versions with counter-proposal in the spirit of the EPC
HCd: Here we are presenting a critical edition of the text, with annotation and a tabular comparison between the CEC/BSA initial and final draft and an added FFII version, which shows what is wrong with the CEC/BSA version and how it could be rewritten in a coherent and adequate way.  We highlighted some differences by bold typeface.
Dii: Draft an de-desinforming FAQ, basically on the same questions as the %(ef:CEC FAQ).  Write other documents which might be published here.  Sumbit them as plain text files.
MOs: Make a list of various kinds of patents from the EPO patent register and challenge the legislators to say whether the directive would allow them and why.
hii: help working on this document
Acm: Ask IT associations to support this approach, which is the only responsible one, no matter how you think about software patents.
Hce: Help us mobilise the press and politicians.  Instructions on how to procede can be obtained after contacting us.
Iic: If you are a journalist, uphold the ethics of your profession
Bos: Be critical,  distinguish between other people's opinion and your own, don't decorate yourself with the work of others, point to sources.  Don't worship power or money (nor Robin Hood), don't cultivate clichees.  Don't transscribe anybody's press releases, be as critical to Bolkestein & Co as to us.  Don't easily believe any patent law expert who claims to speak in the name of an industry, an association or a company.
Dso: Demand a written position statement from your government.
Taa2: The patent lawyers of the European Commission will convene meetings of governmental patent representatives.  Your government must give its patent officials clear written instructions.   Since they tend to obey only the patent establishment and disregard written instructions from their government, the government must also publish these instructions.  Your government/parliament should have a workgroup on patentability most of whose members do not belong to the patent family.
Pcu: Propose to your national legislators that the much abused %(q:as such) clause be deleted from your national patent law's equivalent of Art 52 EPC.
Tto2: The %(q:as such) clause is merely explanatory.  There is nothing wrong with it, but it is redundant and has become the basis of a %(mo:whole system of illegal caselaw).  This would send a signal to your national judges, and it would be simple to do.  See our %(do:documentation about Art 52 EPC).
Ael: A discussion about the limits of patentability can and should of course be conducted at the European Community level.  It must be conducted by economists and software people.  Patent lawyers should stay outside for a while.  Bolkestein, %(ik:Kober) and Liikanen owe the public some explanations.  An investigation for misconduct, illoyalty and possibly corruption should be envisaged.  Particularly the activities of the %(uk:british patent family) in Brussels (%(LST)) need to be scrutinised. The degree of illoyalty exposed in this analysis is difficult to explain without the assumption of corruption.  If you are an investigative journalist, please spend some time on these questions.
Aii: Are you an organisational talent?  Here is a great challenge.
LyW: Look, we are the people, we are the software creators, we are even what the CEC/BSA call the %(q:economic majority).  But we are by far not as well organised as the patent family.  Help us turn our 100000 supporters and 300 (actually really > 600, but work not done) corporate sponsors into a power that will no longer be ignored.  It may be a turning point on the way to a democratic Europe.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: eubsa-swpat0202 ;
# txtlang: ko ;
# End: ;

