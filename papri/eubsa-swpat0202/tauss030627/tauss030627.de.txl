<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Tauss 2003/06/27: Brief an sozialdemokratische Europarlamentarier

#descr: MdB Jörg Tauss macht die Kollegen in der SPD-Fraktion darauf aufmerksam, dass der mit Unterstützung von Rothley u.a. verabschiedete JURI-Beschluss große Gefahren birgt und erheblicher Nachbesserungen im Plenum bedarf, die Zeit erfordern.

#dre: %(nl|An die|Abgeordneten der SPD im|Europäischen Parlament

#WrM: - per eMail

#lWi: Berlin, den 27. Juni 2003

#nmr: Richtlinie zu computerimplementierten Erfindungen

#isa: Hier: die Beratungen des Europäischen Parlamentes

#enl: Liebe Kolleginnen und Kollegen,

#crI: mit Erstaunen entnehme ich der %(fp:Fachpresse), dass es insbesondere SPD-EU-Parlamentarier sein sollen, die an einer schnellen Verabschiedung der Richtlinie zu computerimplementierten  Erfindungen  interessiert  sind.  Zumal  die  inhaltlichen  Kontroversen nachvollziehbar, begründet und in keiner Weise gelöst sind, halte ich die Eile in dieser Sache weder für sachdienlich noch für angebracht. Insofern ist es zu begrüßen, dass aktuellen Agenturmeldungen zufolge der Entwurf nun  offenbar  doch  wie ursprünglich  vorgesehen  erst im  Herbst  im Plenum beraten  werden  soll.  Ich  will  Euch  in  diesem  Zusammenhang  einige Informationen  aus  Sicht  der  Forschungs-,  Medien-  und  Kommunikationspolitik  der  SPD-Bundestagsfraktion zukommen zu lassen. Ich hoffe, dass diese den ein oder anderen noch von den Vorzügen des ein oder anderen Änderungsantrages, der im Rechtsausschuss des EP nicht angenommen wurde, zu überzeugen vermag.

#jel: Die Patentierbarkeit von Software ist eine Kernfrage der künftigen Entwicklung der Informations-  und  Wissensgesellschaft,  da  die  Bedeutung  sowohl  elektronischer  Information  und Kommunikation, als auch der IT-Infrastruktur  weiter  zunehmen  wird.  Die Patent-Diskussion wird im allgemeinen radikaler geführt, als es sachlich bedingt angemessen wäre. Weder geht es um die Neuauflage der ewigen Debatte, ob Patente volkswirtschaftlich mehr schaden oder nutzen, noch um die Durchsetzung lediglich partikularer, radikal libertärer Positionen aus den Reihen der zahlreichen Netz- und IT-Communities. Auch stellt niemand etwa die patentrechtlichen  Grundsätze  ernsthaft  in  Frage,  wie  sie  in  den  zahlreichen  internationalen  Abkommen normiert sind. Und klar ist ebenfalls, dass die deutlich zunehmende Bedeutung informationstechnischer  Komponenten  sowohl  für  das  Innovationsgeschehen  bzw.  ­dynamik insgesamt als auch für die Wettbewerbsfähigkeit von Unternehmen neue Anforderungen an den gewerblichen Rechtsschutz stellt. Es gibt neue, informationstechnisch geprägte schutzwürdige Bereiche gewerblichen Handelns. Doch geht bisher Kommission und EP der eigentlichen Aufgabe aus dem Weg, diese hinreichend eindeutig zu bestimmen. Daher geht es aus meiner Sicht in der Frage der Softwarepatente vielmehr um vier im Grunde politische ­ und erst nachrangig um rechtliche ­ Kernfragen:

#WWd: Wie verhindern wir mit Hilfe der Richtlinie, dass sich eine zu erwartende Ausweitung bzw. nachträgliche Legitimierung der inflationären EPA-Praxis nachteilig auf die  Forschungs- und  Innovationsdynamik  im  IT-Bereich  auswirkt?  Hier  sollten  insbesondere  die  KMUs und  die  wissenschaftliche  Forschung  nicht  unnötig  in  Rechtsunsicherheit  gestürzt  werden.  Übrigens  ist  es  eine  alte  Forderung,  das  EPA  einer  effektiveren  demokratischen Kontrolle zu unterwerfen, wobei hier natürlich die Einflussmöglichkeiten der EU begrenzt sind. Dies kann selbstverständlich lediglich ein Prüfauftrag an das EPÜ sein.

#mef: Wie bestimmen wir in der Richtlinie die Grenze zwischen computerimplementierten Erfindungen, die wir zurecht schützen wollen, und zwischen sogenannten kaum  innovativen oder reinen Softwareprogrammen, die wir analog des EPÜ auch weiterhin nicht schützen wollen? Diese Grenze spielt sicherlich in der praktischen Patentprüfung die größte Rolle und fragt u.a. nach der erforderlichen Mindesterfindungshöhe. Hierzu schweigt der Richtlinien-Entwurf beharrlich.

#krW: Besonders aus standortpolitischer Sicht ist  es  geradezu  ein  Skandal,  in  welcher Weise sich letztlich Interessen der zahlreichen, teilweise marktbeherrschenden amerikanischen Anbieter  und  der  Hand  voll  europäischer  Konzerne  haben  durchsetzen  können  (auch wenn in diesen zumeist im Außenverhalten die Patentabteilungen über die durchweg kritischen Entwicklungsabteilungen dominieren). Ich bitte Euch, einfach einen Blick auf die Herkunft der Antragsteller der Patentanmeldungen beispielsweise am EPA zu werfen: es dominieren  eindeutig  US-Großkonzerne  resp.  deren  europäische  Tochterunternehmen. Die europäische ­ und insbesondere auch die deutsche ­ Softwareindustrie ist hingegen mit  wenigen  Ausnahmen  klein-  bis  mittelständisch  geprägt.  Nicht  nur  die  Patentanmeldungs- wie Durchsetzungskosten bedeuten für die KMU ein wachsendes Geschäftsrisiko, auch die zunehmende Rechtsunsicherheit aufgrund der prinzipiellen Anspruchsintransparenz (zumindest bis zum kostentreibenden Urteil vor den Patentgerichten) lähmt notwendigerweise sowohl die Innovations- wie Investitionsbereitschaft. Europäische  Interessen werden offenbar mit der Richtlinie kaum offensiv verfolgt.

#cto: Und schließlich sehen wir nach wie vor trotz der marginalen Änderungen des Rechtsausschusses  in  der    Richtlinie  eine  Verschlechterung  der  Rahmenbedingungen  für  Open-Source-Software-Projekte.  Die Politik  kann aber  nicht einerseits den Einsatz  von  Open-Source-Software fordern und fördern, die auch hinsichtlich der zunehmend wichtigen Sicherheits-  wie  Kostenaspekte  gerade für den  öffentlichen  Bereich  attraktiv  ist, andererseits  diese  Entwicklung  durch  intendierte  Rechtsunsicherheit  unmöglich  machen  und zugleich amerikanischen  Unternehmen das  Feld  überlassen.  Es  sollte  nochmals  daran erinnert werden, dass Europa bisher sozusagen Weltmarktführer für die Entwicklung und für den Einsatz von Open-Source-Software ist.

#oiu: Zu  allen  vier  Punkten  haben  wir  im  Deutschen  Bundestag  bereits  ausgiebig  beraten.  Ich verweise auf die beigefügten Unterlagen, insbesondere auf das Protokoll eines Expertengesprächs des Deutschen Bundestages, auf den Endbericht der Enquete-Kommission zur Globalisierung der Weltwirtschaft, der dem Thema Softwarepatente ein eigenes Kapitel widmet, und auf die kritische Einlassung der Monopolkommission zu den volkswirtschaftlichen Folgen einer erweiterten Patentierbarkeit von Software bzw. einer weiteren Erhöhung der Rechtsunsicherheit. Die vorliegende Richtlinie vermag weder in ihrer Ursprungsfassung noch in Ihrer nun  vom  Rechtsausschuss  des  EP  beschlossenen  Fassung  der  dringend  gebotenen Rechtssicherheit in diesem Bereich zu dienen. Zudem ist aufgrund der interpretationsoffenen und teilweise kurios unterbestimmten Vorschriften mitnichten eine klärende Harmonisierung der Patentpraktiken europäischer Patentämter zu erwarten ­ im Grunde ist absehbar, dass sie vielmehr die Uneinheitlichkeit potenzieren wird und somit allein der legitimatorische Effekt für die derzeitige EPA-Praxis als Wirkung übrig bleibt.

#ate: Das Hauptziel einer Richtlinie sollte die Steigerung und Sicherung von Rechtssicherheit sein. Dies setzt klare Normen und eindeutige, transparente und demokratisch kontrollierte Verfahren  voraus.  Eine  Richtlinie  sollte  beispielsweise  inhaltlich  hinreichend  bestimmt  sein,  um Streitfälle entscheiden zu können. In Anlage habe ich u.A. bestehende, vom EPA erteilte und kontroverse  Patentansprüche  zusammengestellt.  Die  Bestimmungen  der  Richtlinie  sollten zumindest ansatzweise Auskunft darüber geben können, ob der entsprechende Gegenstand nun unter den Patentschutz fällt oder nicht ­ erst dies würde  zu einer  Vereinheitlichung der europäischen Patenterteilungs- und Patentspruchpraxis führen. Die Kommission und das EP sind dieser Aufgabe bisher nicht gerecht geworden. Dabei zeigen zahlreiche Änderungsanträge im Rechtsausschuss bereits in die richtige Richtung. Ich hoffe, dass in der Plenumssitzung am Montag zumindest einige der Anträge Gehör finden werden.

#dtW: Eine EU-Richtlinie ist daher keineswegs eine Fehlentwicklung, sie ist sogar sachlich notwendig und kann ein entscheidendes Signal in die richtige Richtung geben. Daher kann der Ansatz einer europaweit einheitlichen Richtlinie nur begrüßt werden. Dies setzt allerdings voraus,  dass  es  gelingt,  erhebliche  Klarstellungen  am  gegenwärtigen  Entwurf  durchzusetzen.  Das Augenmerk sollte daher nicht auf dem %(q:Ob), sondern vielmehr auf der Frage liegen, wie wir Trivialpatente nachhaltig verhindern können und wie ein verlässlicher Rahmen auch für alternative oder neue Konzepte der Softwareentwicklung geschaffen werden kann, denen ein großes  Potenzial für die  künftige  IT-Entwicklung  zukommt.  Hierzu  sind  eindeutige  Normen und Verfahren und nicht letztlich patentjuristische Begriffsakrobatik gefragt.

#eaE: Für Rückfragen stehe ich jederzeit gern zur Verfügung. Ich bitte Euch aber eindringlich, die zahlreichen nicht angenommenen Änderungsanträge zu sichten und bei Eurer Entscheidung zu berücksichtigen.

#WlG: Mit freundlichen Grüßen

#Gwa

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: tauss030627 ;
# txtlang: de ;
# multlin: t ;
# End: ;

