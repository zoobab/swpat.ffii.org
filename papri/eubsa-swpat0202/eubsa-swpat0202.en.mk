# -*- mode: makefile -*-

eubsa-swpat0202.en.lstex:
	lstex eubsa-swpat0202.en | sort -u > eubsa-swpat0202.en.lstex
eubsa-swpat0202.en.mk:	eubsa-swpat0202.en.lstex
	vcat /ul/prg/RC/texmake > eubsa-swpat0202.en.mk


eubsa-swpat0202.en.dvi:	eubsa-swpat0202.en.mk
	rm -f eubsa-swpat0202.en.lta
	if latex eubsa-swpat0202.en;then test -f eubsa-swpat0202.en.lta && latex eubsa-swpat0202.en;while tail -n 20 eubsa-swpat0202.en.log | grep -w references && latex eubsa-swpat0202.en;do eval;done;fi
	if test -r eubsa-swpat0202.en.idx;then makeindex eubsa-swpat0202.en && latex eubsa-swpat0202.en;fi

eubsa-swpat0202.en.pdf:	eubsa-swpat0202.en.ps
	if grep -w '\(CJK\|epsfig\)' eubsa-swpat0202.en.tex;then ps2pdf eubsa-swpat0202.en.ps;else rm -f eubsa-swpat0202.en.lta;if pdflatex eubsa-swpat0202.en;then test -f eubsa-swpat0202.en.lta && pdflatex eubsa-swpat0202.en;while tail -n 20 eubsa-swpat0202.en.log | grep -w references && pdflatex eubsa-swpat0202.en;do eval;done;fi;fi
	if test -r eubsa-swpat0202.en.idx;then makeindex eubsa-swpat0202.en && pdflatex eubsa-swpat0202.en;fi
eubsa-swpat0202.en.tty:	eubsa-swpat0202.en.dvi
	dvi2tty -q eubsa-swpat0202.en > eubsa-swpat0202.en.tty
eubsa-swpat0202.en.ps:	eubsa-swpat0202.en.dvi	
	dvips  eubsa-swpat0202.en
eubsa-swpat0202.en.001:	eubsa-swpat0202.en.dvi
	rm -f eubsa-swpat0202.en.[0-9][0-9][0-9]
	dvips -i -S 1  eubsa-swpat0202.en
eubsa-swpat0202.en.pbm:	eubsa-swpat0202.en.ps
	pstopbm eubsa-swpat0202.en.ps
eubsa-swpat0202.en.gif:	eubsa-swpat0202.en.ps
	pstogif eubsa-swpat0202.en.ps
eubsa-swpat0202.en.eps:	eubsa-swpat0202.en.dvi
	dvips -E -f eubsa-swpat0202.en > eubsa-swpat0202.en.eps
eubsa-swpat0202.en-01.g3n:	eubsa-swpat0202.en.ps
	ps2fax n eubsa-swpat0202.en.ps
eubsa-swpat0202.en-01.g3f:	eubsa-swpat0202.en.ps
	ps2fax f eubsa-swpat0202.en.ps
eubsa-swpat0202.en.ps.gz:	eubsa-swpat0202.en.ps
	gzip < eubsa-swpat0202.en.ps > eubsa-swpat0202.en.ps.gz
eubsa-swpat0202.en.ps.gz.uue:	eubsa-swpat0202.en.ps.gz
	uuencode eubsa-swpat0202.en.ps.gz eubsa-swpat0202.en.ps.gz > eubsa-swpat0202.en.ps.gz.uue
eubsa-swpat0202.en.faxsnd:	eubsa-swpat0202.en-01.g3n
	set -a;FAXRES=n;FILELIST=`echo eubsa-swpat0202.en-??.g3n`;source faxsnd main
eubsa-swpat0202.en_tex.ps:	
	cat eubsa-swpat0202.en.tex | splitlong | coco | m2ps > eubsa-swpat0202.en_tex.ps
eubsa-swpat0202.en_tex.ps.gz:	eubsa-swpat0202.en_tex.ps
	gzip < eubsa-swpat0202.en_tex.ps > eubsa-swpat0202.en_tex.ps.gz
eubsa-swpat0202.en-01.pgm:
	cat eubsa-swpat0202.en.ps | gs -q -sDEVICE=pgm -sOutputFile=eubsa-swpat0202.en-%02d.pgm -
eubsa-swpat0202.en/eubsa-swpat0202.en.html:	eubsa-swpat0202.en.dvi
	rm -fR eubsa-swpat0202.en;latex2html eubsa-swpat0202.en.tex
xview:	eubsa-swpat0202.en.dvi
	xdvi -s 8 eubsa-swpat0202.en &
tview:	eubsa-swpat0202.en.tty
	browse eubsa-swpat0202.en.tty 
gview:	eubsa-swpat0202.en.ps
	ghostview  eubsa-swpat0202.en.ps &
print:	eubsa-swpat0202.en.ps
	lpr -s -h eubsa-swpat0202.en.ps 
sprint:	eubsa-swpat0202.en.001
	for F in eubsa-swpat0202.en.[0-9][0-9][0-9];do lpr -s -h $$F;done
fview:	eubsa-swpat0202.en.faxsnd
	faxsndjob view eubsa-swpat0202.en &
fsend:	eubsa-swpat0202.en.faxsnd
	faxsndjob jobs eubsa-swpat0202.en
viewgif:	eubsa-swpat0202.en.gif
	xv eubsa-swpat0202.en.gif &
viewpbm:	eubsa-swpat0202.en.pbm
	xv eubsa-swpat0202.en-??.pbm &
vieweps:	eubsa-swpat0202.en.eps
	ghostview eubsa-swpat0202.en.eps &	
clean:	eubsa-swpat0202.en.ps
	rm -f  eubsa-swpat0202.en-*.tex eubsa-swpat0202.en.{dvi,log,aux,toc,lof,el,err,tar,tgz,faxsnd,*.{gz,uue}} eubsa-swpat0202.en-??.* eubsa-swpat0202.en_tex.* eubsa-swpat0202.en*~
tgz:	clean
	set +f;LSFILES=`cat eubsa-swpat0202.en.ls???`;FILES=`ls eubsa-swpat0202.en.* $$LSFILES | sort -u`;tar czvf eubsa-swpat0202.en.tgz $$FILES;chmod 440 $$LSFILES;rm -f $$FILES
