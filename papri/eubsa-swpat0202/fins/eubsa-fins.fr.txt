<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

descr: BSA seulement
title: Financial Statement
EoR: Editors Remark
Tee: This statement is only found in the BSA version.  It has been mysteriously truncated from the CEC-adopted Directive Proposal.
TOR: TITLE OF OPERATION
Pdi: Proposal for a European Parliament and Council Directive on the patentability of computer-implemented inventions.
BEI: BUDGET HEADINGS INVOLVED
Non: None.
LAB: LEGAL BASIS
A5C: Article 95 of the CEC Treaty.
DOW: DESCRIPTION OF THE OPERATION
Gaj: General objective
Hea: Harmonisation and clarification of Member States' patent laws and practices concerning the patentability of computer-implemented inventions.
PWn: Period covered and arrangements for renewal
Uei: Unspecified.
CAE: CLASSIFICATION OF EXPENDITURE
TFN: TYPE OF EXPENDITURE
FWo: FINANCIAL IMPACT (on Part B)
Non2: None
FEW: FRAUD PREVENTION MEASURES
EON: ELEMENTS OF COST-EFFECTIVENESS ANALYSIS
Siy: Specific and quantifiable objectives; target population
BWe: By clarifying the legal framework concerning the patentability of computer-implemented inventions, the initiative should make it possible for businesses and in particular for SMEs all over Europe to make increased use of the possibility to obtain patents for such inventions.  Furthermore, by harmonising the patentability conditions, the proposed Directive should facilitate the cross-border exchange of patented software.
Eos: European businesses should also benefit from the increased certainty which will be brought about by clarifying that computer-implemented business methods with no technical character (%(q:pure) business methods) cannot be patented. This will engender an environment in which innovative business methods can flourish without fear of damaging legal action.
Gfo: Grounds for the operation
Tra: The interested circles consulted have strongly asked for a harmonisation of law and practices on the subject which should also remove the ambiguity and legal uncertainty surrounding it.
MWW: Monitoring and evaluation of the operation
ArW: Article 5 of the proposed Directive provides for the Commission to report to the Parliament and the Council no later than three years after expiration of the time limit for implementation of the proposed Directive. The Commission will report through service papers made by the staff assigned to the administration of the operation. Any proposals for adjusting the proposed system could be put forward at that time.
ARI: ADMINISTRATIVE EXPENDITURE (PART A OF SECTION III OF THE GENERAL BUDGET)
Nee: No effect.
EWe: Effect on the number of posts
Nee2: No effect.
Oia: Overall financial impact of additional human resources
Nee3: No effect.
Iir2: Increase in other administrative expenditure arising from the operation
ntt: negigible effect on administrative expenditure.
IAE: IMPACT ASSESSMENT FORM
TWr2: Title of proposal
Pnp: Proposal for a Directive on the patentability of computer-implemented inventions
DWc: Document Reference Number

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: eubsa-fins ;
# txtlang: fr ;
# End: ;

