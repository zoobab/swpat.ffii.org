<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EUK/BSA Richtlinienvorschlag Logikpatente: Anmerkung von Roman Sedlmaier 2002/03

#descr: Ein Münchener Rechtsanwalt zeigt Widersprüche im Richtlinienvorschlag der Europäischen Kommission zur Patentierbarkeit computer-implementierter Organisations- und Rechenregeln auf.  Der Begriff der %(q:Erfindung) in doppeldeutiger Weise gebraucht, die Prüfung des Vorliegens einer Erfindung werde systemwidrig mit der Frage der Erfindungshöhe vermengt.  Indem die EU-Kommission zugleich eine %(q:ganzheitliche Betrachtung) und deren Gegenteil, nämlich eine %(q:Fokussierung auf einen technischen Beitrag), vorschreibe, verlange sie von einem Richter unmögliches.  Durch diese und andere Ungereimtheiten schaffe der Vorschlag vor allem neue Unklarheiten und verfehle seine vorgegebenen Ziele.  Sofern in Computerprogrammen Erfindungen gesehen werden, sei das Verbot von Programmanspüchen gemäß EU-Vorschlag angesichts Art 27 TRIPs wirkungslos.  Wenn, wie von der EUK behauptet, der Begriff der Technischen Erfindung ständig im Hinblick auf Weiterentwicklungen der Technik im Fluss bleiben müsse, so könne es letztlich gar keine sinnvolle RiLi zu diesem Thema geben.   Eine einheitliche Rechtsprechung lasse sich unter diesen Umständen nur durch Einrichtung eines höchsten europäischen Patentgerichtes erreichen.  Ein Großteil der Patentjuristen, die sich mit dem EU-Vorschlag beschäftigt haben (z.B. BGH-Richter), seufzen hinter vorgehaltener Hand über die Widersprüchlichkeit und mangelnde handwerkliche Qualität des EUK/BSA-Entwurfs.   Dem jungen Anwalt Sedlmaier kommt die Rolle zu, das unsägliche zu sagen, und die Patentanwaltskammer bietet ihm hierfür im März 2002 sogar eine Tribüne, die ihm Beachtung bis in die zuständigen patentjuristischen Abteilungen der Bundesregierung hinein sichert.  Kollegen in Regierung und Verbänden fühlen sich dadurch ermutigt, aus der Reihe zu tanzen oder eine Abwartehaltung einzunehmen.

#Ans: Anmerkungen des Autors

#Dae: Der Richtlinienvorschlag für die Patentierbarkeit computerimplementierter Erfindungen - eine Anmerkung

#RWm: %(fn:*:Roman Sedlmaier)

#Ant: Am 20.02.2002 hat die EU-Kommission den lang erwarteten %(fn:1:Richtlinienvorschlag über die Patentierbarkeit computerimplementierter Erfindungen) veröffentlicht.

#Dc2: Die vorgeschlagene Richtlinie gliedert sich in elf Artikel, von denen die Artikel 2 bis 5 eine besondere Relevanz aufweisen.

#Tmf: Technischer Beitrag zum Stand der Technik auf dem Gebiet der Technik

#ItW: In Artikel 2 werden die Begriffe %(q:computerimplementierte Erfindung) und %(q:technischer Beitrag) definiert. Hierbei soll der Begriff %(q:computerimplementierte Erfindung) jede Erfindung erfassen, zu deren Ausführung ein Computer, ein Computernetz oder eine sonstige programmierbare Vorrichtung eingesetzt wird und die auf den ersten Blick mindestens ein neuartiges Merkmal aufweist, das ganz oder teilweise mit einem oder mehreren Computerprogrammen realisiert wird.

#Ibw: Im Rahmen dieser Begriffsbestimmung bleibt bereits fraglich, was die Kommission unter dem Begriff %(q:Erfindung) versteht. Meint %(q:Erfindung) die %(q:technische Lehre) iSv § 1 Abs. 1-3 PatG bzw. Art. 52 Abs. 1-3 EPÜ, also unter Einbeziehung des Patentierungsverbots für %(q:Computerprogramme als solche)? Oder bedarf es, damit eine Erfindung vorliegt, nur das Vorhandensein einer gewissen %(q:Grundtechnizität), die %(fn:2:bei allen computerbezogenen Lehren aufgrund der auftretenden physikalischen Veränderungen bei der Hardware bei Ausführung von Programmbefehlen besteht)?

#Icd: Im ersten Fall würde sich gegenüber der bisherigen Rechtsprechung des BGH zu computerbezogenen Verfahren nichts ändern, da der BGH in der Vergangenheit die Technizität einer computerbezogenen Lehre %(fn:3:immer an das Erfordernis einer patentrechtswürdigen Eigenheit bzw. eines weiteren technischen Effekts geknüpft hat).

#Wml: Wahrscheinlicher ist allerdings der zweite Fall, da die Richtlinie die Erfindungseigenschaft nach § 1 PatG nur an die Ausführung mittels einer programmierbaren Vorrichtung knüpft und damit ausschließlich auf die Grundtechnizität, die jedem Computerprogramm zu eigen ist, abstellt. Offen bleibt dann allerdings, was nach dem Richtlinienvorschlag unter %(q:Computerprogramme als solche) iSv § 1 Abs. 2, 3 PatG bzw. Art. 52 Abs. 2, 3 EPÜ zu verstehen ist.

#JWh: Jedenfalls werden die vom %(fn:4:BGH) und vom %(fn:5:EPA) gefundenen, mehr oder minder kongruenten Definitionen für %(q:Computerprogramme als solche) nicht mehr zutreffend sein. Unter Beachtung der Begründung zum Richtlinienvorschlag ist wohl davon auszugehen, daß der Richtlinienvorschlag %(q:Computerprogramme als solche) mit %(q:abstrakten Algorithmen), die keinen physikalischen Bezug erfordern, gleichsetzt, da diese ihrem Wesen nach nicht technisch sind, und somit auch nicht als patentierbare Erfindung angesehen werden %(fn:6:können0. Unter Beachtung dieser Erklärung wird auch der Begriff des %(q:Computerprogramms) in Art. 2 (a) aE verständlich, der mit dem Begriff %(q:Algorithmus) gleichgesetzt wird.

#Dda: Damit nimmt die Begriffsbestimmung der %(q:computerimplementierten Erfindung) eine Auslegung des Begriffs der Erfindung (§ 1 Abs. 1-3 PatG bzw. Art. 52 Abs. 1-3 EPÜ) vor, nach der nunmehr eine Grundtechnizität genügen %(fn:7:soll). Maßgeblich danach ist, daß der angemeldeten Lehre einerseits eine (nicht unbedingt technische) Neuerung zugrunde liegt, und daß andererseits zur Realisierung dieser Lehre unter Einschluß der Neuerung eine programmierbare Vorrichtung verwendet wird. Diese Vorgabe des Anmelders soll in objektiver Hinsicht nur auf Schlüssigkeit zu überprüfen sein. Sofern eine derartige Schlüssigkeit bejaht wird, stellt die Erfindung per Definition auch eine technische Lehre im Sinne von § 1 PatG bzw. Art. 52 EPÜ dar. Damit entscheidet das Kriterium der fotografischen Neuheit über die Patentfähigkeit einer technischen Vorrichtung oder eines technischen Verfahrens und %(fn:8:nicht die Technizität einer Lehre).

#Awe: Artikel 4 Abs. 1, in dem die Voraussetzungen der Patentierbarkeit festgelegt werden, fordert zusätzlich, daß diese %(q:computerimplementierte Erfindung) gewerblich anwendbar und neu ist und auf einer erfinderischen Tätigkeit beruht.

#Ier: Im Gegensatz zu den heutigen Regelungen in § 1 Abs. 1-3 PatG bzw. Art. 52 Abs. 1-3 EPÜ zeigt sich, daß es künftig für die Feststellung einer technischen Lehre nicht mehr darauf ankommt, ob die computerbezogene Lehre einen Beitrag %(q:in technischer Hinsicht) %(fn:9:leistet). Mit einer derartigen Auslegung stößt der Richtlinienvorschlag die bisherige Systematik des Patentrechts um, derzufolge eine inhaltliche Prüfung einer patentrechtlichen Lehre im Rahmen der Prüfung der Erfindungseigenschaft stattfindet. Die vom EPA in den Entscheidungen %(q:Computerprogrammprodukt) und vom BGH in der Entscheidung %(fn:11:Suche fehlerhafter Zeichenketten) gefundene Definition des %(q:weiteren technischen Effekts) bzw. der %(q:technischen Eigenheit) soll als inhaltliche Anforderung systemwidrig auf die Stufe der erfinderischen Tätigkeit verlagert werden.

#DeW: Die Kommission führt hierzu als Begründung die Entscheidung %(q:Steuerung eines Pensionsystems) ins Feld, aus der sie herausgelesen habe, daß alle Programme, die auf einem Computer ablaufen, per Definition als technisch anzusehen sind (da es sich bei dem Computer um eine Maschine handelt). Sie würden somit die erste Hürde auf dem Weg zur Patentierbarkeit %(fn:13:überwinden).

#DFu: Dabei unterscheidet die Kommission offensichtlich nicht zwischen Vorrichtungs- und Verfahrensanspruch. Sollte die Auffassung der Kommission richtig sein, so stellt sich die Frage, warum die Beschwerdekammer des EPA in der Entscheidung %(q:Steuerung eines Pensionssystems) dem Verfahrensanspruch gerade nicht die Stellung einer technischen Lehre zugebilligt hat. Hieran scheiterte bereits der Verfahrensanspruch, obschon im Anspruch eindeutig klargestellt war, daß das Verfahren sog. %(q:computing means) %(fn:14:benutzt).

#MiE: Man muß jedoch wohl aus dem Richtlinienvorschlag den Schluß ziehen, daß die Kommission folgende Auslegung von § 1 PatG bzw. Art. 52 EPÜ in Bezug auf computerbezogene Erfindungen festschreiben will, auch wenn die Begründungen hierfür ins Leere laufen:

#Cte: Computerbezogene Vorrichtungs- und Verfahrensansprüche betreffen stets eine technische Lehre, weil in beiden Fällen die angemeldete Lehre von einer technischen Vorrichtung notwendigerweise Gebrauch macht.

#Oed: Ob die Neuerung auf dem Gebiet der Technik liegt oder nicht, ist unerheblich.

#Dne: Damit folgt die Kommission der Rechtsprechung des BGH in der Entscheidung %(fn:15:Sprachanalyseeinrichtung) sowie der Entscheidung des EPA %(fn:16:Steuerung eines Pensionsystems), weitet aber gleichzeitig die dort für Vorrichtungsansprüche getroffene Regelung auch auf Verfahrensansprüche aus.

#Iia: In Art. 2 (b) definiert der Richtlinienvorschlag den Begriff des %(q:technischen Beitrags). Danach ist ein %(q:technischer Beitrag) ein Beitrag zum Stand der Technik auf einem Gebiet der Technik, der für eine fachkundige Person nicht nahe liegend ist.

#ArP: Art. 4 Abs. 2 knüpft an den in Art. 2 (b) definierten Begriff des %(q:technischen Beitrags) an und ordnet ihn dem Patentierungserfordernis der erfinderischen Tätigkeit zu.

#Ite: In Art. 4 Abs. 3 schreibt der Richtlinienvorschlag vor, daß bei der Ermittlung des technischen Beitrags zu beurteilen ist, inwieweit sich der Gegenstand des Patentanspruchs in seiner Gesamtheit, der sowohl technische als auch nichttechnische Merkmale umfassen kann, vom Stand der Technik abhebt.

#Aig: Abgesehen davon, daß die Richtlinie mit Art. 4 Abs. 2 und 3 ein gewachsenes System in bezug auf eine spezifische Gruppe von Erfindungen verändern will, indem sie die inhaltliche Prüfung einer computerbezogenen Lehre auf die Stufe der Erfindungshöhe verlagert, zeichnet sie auch einen anderen Weg vor, als den die Rechtsprechung des EPA sowie die deutsche und britische Rechtsprechung eingeschlagen haben, und verlangt schlichtweg Unmögliches.

#Ehs: Eine Geschäftsmethode stellt beispielsweise nach dem Richtlinienvorschlag schon deshalb eine technische Lehre dar, weil sie eine Neuerung enthält, die zwar für sich gesehen nicht technisch ist, aber zu ihrer Ausführung nach dem Patentanspruch eines Rechners bedarf. Auf der Ebene der Erfindungshöhe soll nunmehr festgestellt werden, ob diese Geschäftsmethode einen Beitrag zum Stand der Technik auf dem Gebiet der Technik liefert, der für eine fachkundige Person nicht naheliegend ist. Nach Art. 4 Abs. 3 muß dabei die Ermittlung des technischen Beitrags in einer Gesamtbetrachtung erfolgen, d.h. ohne daß %(q:Gewichtungen) vorgenommen werden, welcher Aspekt den wichtigeren Beitrag zum Erfolg der Erfindung %(fn:17:leistet).

#Wui: Wie soll so etwas gelingen? Entweder nimmt man eine Gesamtbetrachtung ohne Einschränkung vor, dann folgt daraus eine Erweiterung des Patentschutzes für computerimplementierte Erfindungen, weil dann alles zu berücksichtigen ist, worin sich die angemeldete Lehre vom Stand der Technik %(fn:18:unterscheidet). Damit würde auch die Geschäftsmethode, die keinen technischen, sondern z.B. nur einen finanzmathematischen Fortschritt beinhaltet, als erfinderisch gelten. Insofern widersprechen sich die Begriffe %(q:Beitrag) und %(q:Gesamtbetrachtung).

#OvW: Oder man rechnet dem jeweiligen Fachmann in Anlehnung an die Entscheidung %(fn:19:Steuerung eines Pensionssystems) die nichttechnischen Neuerungen einer Lehre zu. Damit erweitert man fiktiv den bekannten Stand der Technik, so daß nur noch technische Neuerungen in der angemeldeten Lehre bei der Beurteilung der Erfindungshöhe zum Tragen kommen. Dieser Weg bleibt letztlich als einziger übrig, sofern die Kommission die derzeitige Patentierungspraxis bei computerbezogenen Erfindungen nicht ausweiten und damit der US-amerikanischen Praxis angleichen %(fn:20:möchte).

#UWt: Um bei einem Gegenstand, der möglicherweise technische wie auch nichttechnische Neuerungen gegenüber dem Stand der Technik enthält, die technischen Merkmale auf ihre Erfindungsqualität hin untersuchen zu können, muß eine Fokussierung stattfinden. Die derzeitige Rechtsprechung in Bezug auf computerbezogene Verfahrensansprüche nimmt eine derartige Fokussierung bei der Klärung der %(q:Technizität) der Lehre (§ 1 PatG bzw. Art. 52 EPÜ) %(fn:21:vor). Bei computerbezogenen Vorrichtungsansprüchen hingegen hat die Beschwerdekammer des EPA eine Fokussierung mittels Wissenszurechnung der untechnischen Neuerungen für den jeweiligen Fachmann %(fn:22:vorgenommen). Der BGH zieht hingegen in der Entscheidung %(q:Sprachanalyseeinrichtung) auf der Prüfungsebene der erfinderischen Tätigkeit eine unterschiedliche Gewichtung in %(fn:23:Betracht).

#DWe: Die gesamte europäische Rechtsprechung geht gerade von der Notwendigkeit einer Gewichtung aus, um feststellen zu können, ob ein technischer oder nichttechnischer Beitrag geleistet wird. Umstritten war hierbei lediglich, an welcher Stelle eine solche Kernbetrachtung stattzufinden %(fn:24:hat).

#Uaf: Unter Beachtung der Vorgaben des Richtlinienentwurfs wird eine derartige Fokussierungsnotwendigkeit zwangsläufig auf das Wissen des Fachmanns übertragen; dies hat zur Folge, daß der Fachmann alle nichttechnischen Neuerungen aufgrund einer Wissensfiktion kennt, obschon sich diese Neuerungen nicht im Stand der Technik %(fn:25:wiederfinden).

#DWe2: Der Richtlinienvorschlag sagt hierzu in den Erläuterungen zu Art. 426:  Folglich wird einer computerimplementierten Erfindung, die zwar den früheren Stand der Technik bereichert, aber keinen technischen Charakter hat, die erfinderische Tätigkeit abgesprochen, selbst wenn die (nicht-technische) Bereicherung des früheren Standes der Technik über das Naheliegen hinausgeht. Bei der Beurteilung der erfinderischen Tätigkeit muß die Frage, welcher Stand der Technik und welcher Wissensstand des Fachmanns zugrunde zu legen ist, anhand der Kriterien beantwortet werden, die bei der Beurteilung der erfinderischen Tätigkeit im Allgemeinen angewandt werden.

#Zcu: Zunächst einmal deuten die Vorgaben in Art. 4 Richtlinienentwurf darauf hin, daß dem Fachmann die ggf. erfinderische, nichttechnische Neuerung zugerechnet wird, um auf diese Weise eine gesamtbetrachtungsgemäße Fokussierung zur Feststellung eines ausreichenden technischen Beitrags für die gesamte angemeldete Lehre zu ermöglichen. Dies lehnt sich offensichtlich an die Rechtsprechung der Beschwerdekammer des EPA in der Entscheidung %(fn:27:Steuerung eines Pensionssystems) an. In dem dort gegebenen Fall war diese Zurechnung unter gleichzeitiger Durchführung einer Gesamtbetrachtung möglich, da die Lehre letztlich keinen technischen Beitrag offenbarte. Problematisch erscheint jedoch ein Fall wie der des BGH in der Entscheidung %(q:Suche fehlerhafter Zeichenketten), in dem nichttechnische Merkmale (Statistiken etc.) eine technische, computerspezifische Wirkung entfaltet haben28. In einem solchen Fall sind technische und nichttechnische Merkmale so miteinander verknüpft, daß im Falle einer Wissenszurechnung der nichttechnischen Neuerung ggf. mittelbare technische Wirkungen der gesamten Lehre verloren gehen29. Somit müßte spätestens an dieser Stelle eine - wie auch immer geartete - Kernbetrachtung dahingehend stattfinden, worin der Erfindungsschritt in technischer Hinsicht liegt, und ob das nichttechnische Merkmal einen technischen Beitrag in Bezug auf diesen Erfindungsschritt liefert. Nur dann, wenn das nichttechnische Merkmal keinen technischen Beitrag für den Erfindungsschritt liefert, kann dieses Merkmal dem Fachmann zugerechnet werden.

#Saj: Sinn und Zweck der Richtlinie ist eine einheitliche Rechtsauslegung sowie mehr Rechtssicherheit. Weder das eine noch das andere ist auf diesem Wege gesichert. Die indirekte Forderung, daß dem Fachmann außertechnisches, ggf. erfinderisches Wissen zugerechnet wird, findet dabei bereits seinen Widerspruch in der bisherigen Rechtsprechung des BGH. Danach ist es unzulässig, in den zur Beurteilung der Neuheit und der Erfindungshöhe heranzuziehenden Stand der Technik Erkenntnisse hineinzuinterpretieren, die erst die Lehre des Patents %(fn:30:gebracht hat).

#RWW: Richtig wäre es daher, die bisherige Rechtsprechung zu computerbezogenen Verfahrensansprüchen heranzuziehen und diese unter Beachtung einer Gesamtbetrachtung festzuschreiben bzw. zu modifizieren. Dies bedeutet, daß man auf der Ebene der %(q:Technizitätsprüfung) feststellt, welche technischen Effekte bzw. technische Eigenheiten das Programm besitzt (beispielsweise: Steuerungseffekt oder Automatisierung). Diese Prüfung hat dabei abstrakt zu erfolgen. Derjenige technische Effekt, der nach Maßgabe der Anmeldung den Erfindungsschritt ausmachen soll, wird dann auf der Ebene der Erfindungshöhe untersucht, ob er in Bezug auf den gesamten Erfindungsgegenstand gegenüber dem Stand der Technik %(fn:31:erfinderisch ist). Nur so läßt sich beispielsweise die Entscheidung des EPA %(q:Steuerung eines Pensionssystems) hinsichtlich des Verfahrensanspruchs %(fn:32:erklären). Auf diese Weise wird sichergestellt, daß man einer computerbezogenen Lehre die notwendige Technizität nur zuweist, wenn der relevante Beitrag technisch ist.

#FWn: Form des Patentanspruchs

#Iev: In Art. 5 der Entwurfsrichtlinie sollen computerimplementierte Erfindungen entweder als programmierter Computer oder eine vergleichbare programmierte Vorrichtung beansprucht werden (d.h. als Erzeugnis), oder aber als Verfahren, wenn dieses von einem Computer, einem Computernetz oder einer sonstigen Vorrichtung durch Ausführung von Software verwirklicht wird.

#DAn: Durch die Aufzählung der verschiedenen Erzeugnisansprüche schließt der Richtlinienvorschlag Ansprüche auf Computerprogramme allein oder als Aufzeichnung auf einem Datenträger aus. Hierauf weist die Kommission in ihrer Erläuterung zu Artikel 533 auch ausdrücklich hin. Als Begründung führt sie hierfür an, daß andernfalls dies so verstanden werden könnte, als würden Patente auf Computerprogramme %(q:als solche) erteilt.

#Dtr: Die Kommission stellt daneben in der %(fn:34:Richtlinienbegründung) fest, daß eine bisherige nationale Rechtsprechung auf dem Gebiet der computerimplementierten Erfindungen überwiegend in nur zwei Mitgliedstaaten entwickelt worden sei: in Deutschland und im Vereinigten Königreich. Dabei würden Deutschland, das Vereinigte Königreich sowie das EPA Ansprüche auf Programmprodukte %(fn:35:für zulässig erachten). Andere Mitgliedsstaaten seien dem jedoch noch nicht gefolgt.

#Bes: Bereits diese Argumentationskette zeigt, wie gewollt die Begründung der Notwendigkeit einer Gemeinschaftsmaßnahme zur Harmonisierung der nationalen Rechtsvorschriften ist. Es ist dabei auch nicht ersichtlich, daß es hinsichtlich der Ansprüche auf Programmprodukte einer Harmonisierung bedarf. Die ganz herrschende Meinung in Europa geht von der Zulässigkeit von Ansprüchen auf Programmprodukte aus und hat dies mehr oder minder patentrechtlich begründet. Andere Mitgliedsstaaten haben hierzu jedenfalls keine davon abweichende Haltung publiziert.

#OeA: Obschon die ganz herrschende Ansicht Ansprüche auf Computerprogrammprodukte für zulässig hält, spricht sich die Kommission dagegen aus; dies aber nicht mit einer Harmonisierungsbegründung, sondern mit dem Argument, daß die Beanspruchung von Computerprogrammen allein oder als Aufzeichnung auf einem Datenträger so verstanden werden könnte, als würden Patente auf Computerprogramme %(q:als solche) %(fn:36:erteilt).

#Daz: Dabei würde die Einschränkung der grundsätzlich möglichen Anspruchsformen unter Ausschluß von Ansprüchen auf Computerprogramme, die auf Speichermedien %(fn:37:abgelegt sind), gegen das TRIPS-Übereinkommen verstoßen. Denn nach Art. 27 Abs. 1 Satz 2 TRIPS können Patentrechte auch ausgeübt werden, ohne daß hinsichtlich des Gebiets der Technik diskriminiert werden darf.

#NWn: Nach § 35 Abs. 1 Satz 3 Nr. 2 PatG (bzw. Art. 84 EPÜ) muß die Anmeldung einen oder mehrere Patentansprüche enthalten, in denen angegeben ist, was als patentfähig unter Schutz gestellt werden soll. Hierbei gilt allgemein, daß die Ansprüche als Ganzes zu betrachten sind sowie technische und nicht-technische Merkmale enthalten %(fn:38:können). Nebengeordnete Ansprüche sind möglich, müssen aber den Grundsatz der Einheitlichkeit wahren.

#DeW2: Die Einheitlichkeit ist hierbei beispielsweise bei Teilgeräten mit angepaßten Schnittstellen gewahrt: Sender und Empfänger; Stecker und Steckdose; Steuermodul und Gesamtgerät. Übertragen auf den Bereich der Datenverarbeitungsanlage bedeutet dies, daß der Datenträger als Steuerelement mit einer technischen Schnittstelle zum Computersystem dergestalt fungiert, daß das Speichermedium elektronisch auslesbare Steuersignale erzeugt, die wiederum so mit dem Computersystem zusammenwirken, daß die Schritte des erfindungsgemäßen Verfahrens ausgeführt werden.

#HeW: Hinzu kommt hierbei die ständige Rechtsprechung des %(fn:39:Bundesgerichtshofs), nach der es der Anmelder in der Hand hat, durch entsprechende Fassung der Patentansprüche dafür zu sorgen, daß gegebenenfalls schon in die Vorbereitungsphase patentverletzender Handlungen hineinverlegter Patentschutz erteilt wird.

#Dut: Die Einheitlichkeit in Bezug auf ein Speichermedium entfällt somit auch nicht deshalb, weil sich hierbei der Patentanspruch auf ein sog. Zwischenprodukt bezieht, da es dem Anmelder nach allgemeiner Rechtsauffassung freistehen muß, den Patentschutz dort zu beanspruchen, wo der technische Fortschritt begründet wird oder dort, wo er in Erscheinung %(fn:40:tritt).

#Wid: Würde Patentschutz für Ansprüche auf digitalen Speichermedien gesetzlich versagt, obschon in dem dort gespeicherten Programm der technische Fortschritt begründet wird, so wäre damit auch eine Verletzung des Diskriminierungsverbots in Art. 27 Abs. 1 Satz 2 TRIPS verbunden. Denn dadurch würden Zwischenprodukte im Hinblick auf %(q:computerimplementierte Erfindungen) patentrechtlich schlechter gestellt.

#Dat: Daneben steht auch Art. 5 unmittelbar im Gegensatz zu Art. 4 Abs. 1 iVm Art. 2 (a).  Ein beanspruchtes Speichermedium muß über die auf ihm aufgebrachten auslesbaren Daten nach dem Wortlaut des Patentanspruchs in der Regel derart mit einem programmierbaren Computersystem zusammenwirken können, daß insbesondere ein zumeist gleichzeitig beanspruchtes Verfahren ausgeführt wird. Die Anweisung, die sich nach dem Anspruch auf ein Programmprodukt ergibt, dient danach der Realisierung eines bestimmten %(fn:41:Computerprogramms) und unterfällt damit Art. 2 (a), da zur Ausführung dieser Lehre ein Computer eingesetzt %(fn:42:wird). Nach Art. 4 Abs. 1 sind aber die Mitgliedsstaaten verpflichtet sicherzustellen, daß alle %(q:computerimplementierte Erfindungen) (d.h. auch Programme auf einem Speichermedium) patentierbar sind.

#Dke: Damit müßte Art. 5 (Form des Patentanspruchs) eine Ausschlußwirkung für Ansprüche auf Programmprodukte haben und insofern wiederum Art. 2 (a) indirekt beschränken. Eine entsprechende materiell-rechtliche Ausschlußregelung hierzu fehlt jedoch, so daß der materiellrechtlichen Verpflichtung in Art. 4 Abs. 1 auch unter dem Blickwinkel des Diskriminierungsverbots in Art. 27 Abs. 1 Satz 2 TRIPS sowie des Mandats der Kommission nur für eine Harmonisierung43 (s.o.) Vorrang zukommt.

#Zsl: Zusammenfassende Stellungnahme

#Dsa: Die EU-Kommission hat einen Richtlinienentwurf vorgestellt, der noch der weiteren Erörterung bedarf. Im Falle der ungeänderten Richtlinienumsetzung würde damit intensiv in die bestehende, über Jahrzehnte gewachsene Patentsystematik eingegriffen. Damit würde ein Sonderpatentrecht für computerbezogene Erfindungen geschaffen, das letztendlich nicht zu einer Rechtsharmonisierung führt, sondern vielmehr einen gegenteiligen Effekt bewirkt. Eine Rechtsharmonisierung in ganz Europa für computerimplementierte Erfindungen wäre nur dann möglich, wenn entweder definiert würde, wann ein technischer Beitrag gegeben ist, oder wenn ein übergeordnetes EU-weites Patentgericht errichtet würde. Nachdem der Begriff der Technik nicht abschließend faßbar ist, ohne zukünftige Technologien auszuschließen, sollte die Kommission sich eher für eine baldige Errichtung eines EU-einheitlichen übergeordneten Patentgerichtes zur Harmonisierung unterschiedlicher nationaler Rechtsauffassungen einsetzen.

#RwM: Rechtsanwalt in München.

#Axa: Abrufbar im Internet unter: http://europa.eu.int/comm/internal_market/en/indprop/com02-92de.pdf.

#VoA: Vgl. EPA ­ Computerprogrammprodukt, ABl EPA 1999, 609 unter 6.2.

#vf3: vgl. insofern BGH ­ Suche fehlerhafter Zeichenketten, Mitt. 2001, 553; sowie Sedlmaier, Mitt. 2002,....

#Baj: BGH ­ Suche fehlerhafter Zeichenketten, aaO, vgl. Fn. 3.

#ErW: EPA ­ Computerprogrammprodukt, aaO, vgl. Fn. 2.

#Vuu: Vgl. insofern die Vorschlagsbegründung der Richtlinie, Fn. 1, Seite 8 unter %(q:Funktion von Algorithmen).

#ahW: aA die bisherige Rechtsprechung, vgl. BGH ­ Suche fehlerhafter Zeichenketten, aaO, vgl. Fn. 3; EPA ­ Computerprogrammprodukt, aaO, vgl. Fn. 2.

#Vrn: Vgl. hierzu BGH ­ Sprachanalyseeinrichtung, Mitt. 2000, 359.

#SWA: So aber die bisherige Rechtsprechung, vgl. Fn. 7, sowie EPA ­ Steuerung eines Pensionssystems, CRi 2001, 18 = ABl. EPA 2001, 441, in Bezug auf den Verfahrensanspruch.

#ErW2: EPA ­ Computerprogrammprodukt, aaO, vgl. Fn. 2.

#Bhe: BGH ­ Suche fehlerhafter Zeichenketten, aaO, vgl. Fn. 3.

#Ens: EPA ­ Steuerung eines Pensionssystems, aaO, vgl. Fn. 9.

#Vue: Vorschlagsbegründung der Richtlinie, Fn. 1, Seite 7.

#Vdt: Vgl. weitergehend: Sedlmaier, Mitt. 2000, .....

#Bya: BGH ­ Sprachanalyseeinrichtung, aaO, vgl. Fn. 8.

#Ens2: EPA ­ Steuerung eines Pensionssystems, aaO, vgl. Fn. 9.

#Vrr: Vgl. insofern auch Erläuterung zu Art. 4 des RiLi-Vorschlags, Fn. 1, Seite 16.

#Sgd: So z.B. Kraßer, GRUR 2001, 959, 964 unter IV. aE, für die derzeitige Rechtslage hinsichtlich der Beurteilung der Erfindungshöhe mit dem Hinweis auf Art. 54 II EPÜ, der eine Gesamtbetrachtung fordere.

#Ens3: EPA ­ Steuerung eines Pensionssystems, aaO, vgl. Fn. 9.

#Vht: Vgl. Begründung des RiLi-Vorschlags unter %(q:Gewählter Ansatz), 3. Absatz, Fn. 1, Seite 12.

#VlW: Vgl. BGH ­ Logikverifikationsverfahren, Mitt. 2000, 293; BGH ­ Suche fehlerhafter Zeichenketten, aaO, vgl. Fn. 3; EPA ­ Computerprogrammprodukt, aaO, vgl. Fn. 2; EPA ­ Steuerung eines Pensionssystems, aaO, vgl. Fn. 9.

#Ens4: EPA ­ Steuerung eines Pensionssystems, aaO, vgl. Fn. 9.

#Blj: BGH ­ Sprachanalyseeinrichtung, aaO, vgl. Fn. 8; vgl. hierzu auch Anders, GRUR 2001, 555, 588 unter V.; Sedlmaier, Mitt. 2002, .......

#Vs5: Vgl. z.B. Anders GRUR 2001, 555, 560 unter X.

#sus: so jedenfalls EPA ­ Steuerung eines Pensionssystems, aaO, vgl. Fn. 9.

#EWa: Erläuterung zu Art. 4 des RiLi-Vorschlags, Fn. 1, Seite 15.

#Ens5: EPA ­ Steuerung eines Pensionssystems, aaO, vgl. Fn. 9.

#vA5: vgl. insofern auch Anders, GRUR 2001, 555, 559 unter VII.

#vWl: vgl. Sedlmaier Mitt. 2002, .... unter 3 b).

#B11: BGH, GRUR 1989, 899 ­ Sauerteig.

#sdp: so EPA ­ Computerprogrammprodukt, aaO, vgl. Fn. 2; sowie in Befolgung der in der Entscheidung %(q:Computerprogrammprodukt) aufgestellten Grundsätze EPA ­ Steuerung eines Pensionssystems, aaO, vgl. Fn. 9, hinsichtlich des Verfahrensanspruchs; wohl auch BGH ­ Suche fehlerhafter Zeichenketten, aaO, vgl. Fn. 3; vgl. weiterhin Sedlmaier, Mitt. 2002, .....

#VSt: Vgl. insofern Sedlmaier, Mitt. 2002, ......

#EWa2: Erläuterung zu Art. 5 des RiLi-Vorschlags, Fn. 1, Seite 16.

#BrW: Begründung zum RiLi-Vorschlags, Fn. 1, Seite 10, vorletzter Absatz.

#BiF: Begründung zum RiLi-Vorschlags, Fn. 1, Seite 11 f.

#EWa3: Erläuterung zu Art. 5 des RiLi-Vorschlags, Fn. 1, Seite 16.

#VuV: Vgl. insoweit auch die Erläuterung zu Art. 5 des RiLi-Vorschlags, Fn. 1, Seite 16.

#Vag: Vgl. BGH ­ Sprachanalyseeinrichtung, aaO, vgl. Fn. 8.

#V2n: Vgl. BGH ­ Heliumeinspeisung, GRUR 1992, 305, 308; BGH. ­ Verschlußvorrichtung für Gießkannen, GRUR 1989, 103, 104.

#Bmh: BGH ­ Disiloxan, GRUR 1969, 265, 267; nunmehr implizit bestätigt in BGH ­ Suche fehlerhafter Zeichenketten, aaO, vgl. Fn. 3.

#set: so BGH ­ Suche fehlerhafter Zeichenketten, aaO, vgl. Fn. 3.

#VRW: Vgl. Art. 2 (a) RiLi-Vorschlags, Fn. 1, Seite 21.

#VA9: Vgl. Art. 95 EGV.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: sedl0203 ;
# txtlang: de ;
# multlin: t ;
# End: ;

