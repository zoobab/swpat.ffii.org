<meta http-equiv=Content-Type content=text/plain; charset=utf-8>

descr: Dutch Labor Party criticises that the European Commission's draft in no way fulfills the Dutch Parliament's demands for strict patentability standards and on the contrary is cause of concern and necessitates a lot of sharp questions, which the government should ask Bolkestein.  The PVdA position is based on the recommendations of a joint work group of the largest software industry association FENIT.nl and the Opensource Association VOSN.nl
title: Partij Van De Arbeid 2002-02-20: Ontevreden over Concept EU Richtlijn Software Octrooien
PET: PERSBERICHT PVDA
PVC: PARTIJ VAN DE ARBEID ONTEVREDEN OVER CONCEPT EU RICHTLIJN SOFTWARE OCTROOIEN
Vtk: Vandaag is met een persbericht de concept EU richtlijn over software octrooien verschenen. Deze richtlijn zal een dwingend karakter krijgen binnen de EU en nationale wetgeving zal daaraan moeten worden aangepast. Ondanks grote bezwaren van meerdere Europese landen biedt het concept veel ruimere mogelijkheden o software te patenteren dan tot nu toe. De PvdA heeft zich meermalen uitgesproken tegen de mogelijkheid om triviale software octrooien erkend te krijgen. De PvdA is voorstander van patenten ter bescherming van reele investeringen in innovatie. Maar dan moet het wel om technische vernieuwing en aantoonbaar werkende produkten gaan. Anders ontstaat er ruimte om slechts papieren ideeen te octrooiieren en vervolgens kleine bedrijven te dwingen daarvoor te betalen. De PvdA wenst de open toegang tot kennis en innvatie voor alle ondernemers te beschermen. Op initiatief van de PvdA hebben de Vereniging Open Source Nederland en de FENIT, brancheorganisatie van softwarebedrijven gezamenlijk criteria ontwikkeld waar software aan moet voldoen voor er een patent verstrekt kan worden.
ZnW: Zowel kleine zelfstandige softwareontwikkelaars als grotere softwarebedrijven erkennen het belang van harde scherpe criteria voor softwarepatenten om te zorgen dat echte inovatie beloond wordt en machtsvorming wordt tegengegaan.
Szk: Staatssecretaris Ybema heeft de Kamer meermalen toegezegd dat hij in Europees verband strenge regelgeving en heldere criteria zou bevorderen. Daarvan is niets terug te vinden in de concept richtlijn. De vraag is hoe dat komt en in een serie schriftelijke vragen nodigt Rik Hindriks namens de PvdA-fractie uit om aan te geven hoe hij alsnog een wijziging denkt te gaan realiseren.
Vef: Voor meer informatie
VaW: Vragen aan de staatssecretaris van Economische Zaken van het lid Hindriks (PvdA)
HWa: Heeft u kennis genomen van de press release en faq over de draft directive inzake software patenten die vandaag door de EU is gepubliceerd?
HWd: Heeft U kennis genomen van het commentaar van software organisaties op deze draft directives? Hoe beoordeelt dit commentaar?
Bin: Bent U van mening dat de voorstellen van de EU in overeenstemming zijn met de inzet van de Nederlandse Regering? Zo nee, welke acties bent U voornemens te ondernemen om de voorstellen alsnog in overeenstemming te brengen met deze inzet?
Bil: Bent U van mening dat de voorstellen van de EU in voldoende mate rekening houden met gewenste criteria inzake het erkennen van softwarepatenten? Zo ja, op grond waarvan? Zo nee, hoe denkt U de criteria, zoals die zijn ontwikkeld door VOSN en FENIT alsnog in voldoende mate in de richtlijn verwerkt te krijgen?
Oed: Op welke wijze heeft U een actieve rol gespeeld bij de totstandkoming van deze draft directive?
Otm: Op welke wijze zult U alsnog de door U toegezegde initiatieven om scherpe criteria ingevoerd te krijgen nemen in het Europees overleg over deze draft directive?
Iok: Is het naar uw oordeel mogelijk om op grond van de nu bekende tekts van de draft directive te komen tot het ongeldig verklaren van triviale software octrooien? Zo ja, op grond waarvan meent U dat en welke softwareoctrooien zal dat betreffen? Zo nee, bent U voornemens daartoe nadere acties te ondernemen?
IWW2: Is het juist dat deze draft EU directive is geschreven door Francisco Mingorance? Indien de directe verantwoordlijkheid elders ligt is het dan juist dat Francisco Mingorance een belangrijke rol heeft vervuld bij het schrijven van deze draft directive? Indien dat het geval is, hoe beoordeelt U dan zijn relatie met de Business Software Alliance en de daarin vertegenwoordigde grote Amerikaanse softwareondernemingen?
Bld: Bent U bereid om ten aanzien van de wijze van totstandkoming, alsmede de invloed daarop van externe adviseurs, van deze draft directive nadere informatie te vragen aan de Europese Commissie?
BWE: Bent U bereid om nader overleg te voeren met de Vereniging Open Source Nederland en de FENIT over deze draft directive van de EU? Kan de Kamer nadere berichten over dat overleg tegemoet zien?
Znr: Zie algemeen overleg 21670 nr 14 over een Algemeen Overleg gehouden 14 Maart 2001
UnW: Uit 21670 nr 14 %(q:De staatssecretaris gaat niet akkoord met wijzigingen van het verdrag zonder verbeterde regels. Hij gaat de strijd in Europa aan tegen triviale octrooien. Hij zal initiatieven nemen waar het gaat om scherpe criteria voor het vernauwen van de mazen, in overleg met onder andere het BIE, de FENIT en de VOSN. Hij zal ook kijken naar verkorting van de termijn. Er komt voorlichting aan het MKB en voor het reces of in ieder geval vlak daarna zal er een evaluatie komen.) En %(q:Volgens de staatssecretaris is deze samenvatting correct.)

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: pvda020220 ;
# txtlang: sv ;
# End: ;

