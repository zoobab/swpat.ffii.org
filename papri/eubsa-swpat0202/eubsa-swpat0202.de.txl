<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: EUK & BSA 2002-02-20: Vorschlag, alle nützlichen Ideen patentierbar zu
machen

#descr: Die Europäische Kommission (EUK) schlägt vor, die Patentierung von
Patenten auf Datenverarbeitungsprogrammen als solchen zu legalisieren
und sicher zu stellen, dass breite und triviale Patente auf Programm-
und Geschäftslogik, wie sie derzeit vor allem in den USA von sich
reden machen, künftig auch hier in Europa Bestand haben und von keinem
Gericht mehr zurückgewiesen werden können.  %(q:Aber hallo, die EUK
sagt in ihrer Presseerklärung etwas ganz anderes!), möchten Sie
vielleicht einwenden.  Ganz richtig!  Um herauszufinden, was die EUK
wirklich sagt, müssen Sie nämlich nicht die PE sondern den Vorschlag
selbst lesen.  Aber Vorsicht, der ist in einem Neusprech vom
Europäischen Patentamt (EPA) verfasst, in dem gewöhnliche Wörter oft
das Gegenteil dessen bedeuten, was Sie erwarten würden. Zur Verwirrung
trägt noch ein langer werbender Vorspann bei, in dem die Wichtigkeit
von Patenten und proprietärer Software beschworen wird, wobei dem
software-unerfahrenen Zielpublikum ein Zusammenhang zwischen beiden
suggeriert wird.  Dieser Text ignoriert die Meinungen von allen
geachteten Programmierern und Wirtschaftswissenschaftlern und stützt
seine spärlichen Aussagen über die Ökonomie der Software-Entwicklung
nur auf zwei unveröffentlichte Studien aus dem Umfeld von BSA (von
Microsoft und anderen amerikanischen Großunternehmen dominierter
Verband zur Durchsetzung des Urheberrechts) über die Wichtigkeit
proprietärer Software.  Diese Studien haben überhaupt nicht
Softwarepatente zum Thema!  Der Werbe-Vorspann und der Vorschlag
selber wurden offensichtlich für die EUK von einem Angestellten von
BSA redigiert.  Unten zitieren wir den vollständigen Vorschlag
zusammen mit Belegen für die Rolle von BSA, einer Analyse des Inhalts
und einer tabellarischen Gegenüberstellung der BSA- und EUK-Version
sowie einer EPÜ-Version, d.h. eines Gegenvorschlages im Geiste des
Europäischen Patentübereinkommen von 1973 und der aufgeklärten
Patentliteratur.  Die EPÜ-Version sollte Ihnen helfen, die Klarheit
und Weisheit der heute gültigen gesetzlichen Regelung verstehen, an
deren Aushebelung die Patentanwälte der Europäischen Kommission
gemeinsam mit EPA, BSA u.a. in den letzten Jahren hart gearbeitet
haben.

#TCp: Tabellarische Textausgabe in BSA- und EUK-Fassung in Gegenüberstellung
mit Gegenvorschlag im Geiste des EPÜ

#HCd: Here we are presenting a critical edition of the text, with annotation
and a tabular comparison between the CEC/BSA initial and final draft
and an added FFII version, which shows what is wrong with the CEC/BSA
version and how it could be rewritten in a coherent and adequate way. 
We highlighted some differences by bold typeface.

#Pad: Vorschlag, alle nützlichen Ideen patentierbar zu machen, auf einem
Entwurf von BSA beruhend, von der Europäischen Kommission am
2002-02-20 freigegeben

#Uma: Upon Release of its Software Patentability Directive Proposal (based
on a draft by BSA), the European Commission lies to the press and to
the world about the contents of this Directive Proposal, trying to
create the impression that this directive proposal excludes patents on
business methods and software as such.

#Aey: An text by the European Commission's (CEC) software patentability law
drafters, designed to (dis)inform journalists and the general public
about what is at stake and to soothe widespread fears that the CEC
might be legalising unwanted software and business method patents. 
The text tries to achieve this by using EPO/UKPO Patent Newspeak, in
which normal phrases may have two radically different meanings, one
for consupmtion by politicians, journalists and citizens, i.e. the
readers of this FAQ, and another as understood by patent
professionals.

#Jro: JURI working documents

#WBt: Working documents of the Legal Affairs Commission of the European
Parliament about the CEC/BSA software patentability proposal of
2002-02-20, published 2002-06-19, inititially consisting of a short
report by MEP Arlene McCarthy and a study ordered by the European
Commission.

#Pi1: Lenz 2002-03-01: Sinking the Software Patent Proposal

#lpa: Karl-Friedrich Lenz, professor of European Law, lists some legal and
constitutional arguments to explain why the CEC/BSA proposal is a
legal and political scandal, starting from the fact that the European
Commission is using %(q:harmonisation) and %(q:clarification) merely
as pretexts to declare itself competent for promoting an unspeakable
political agenda which does not fall in the Commission's competentce.

#DxW: Datamonitor 2000-09: Packaged Software Industry in Europe

#ArW2: A rarely cited and largely unknown study by a company called
Datamonitor about the importance of proprietary software as a creator
of jobs in Europe.  The study claims that proprietary software will
create 1/2 million new jobs in the next few years.  Praises Ireland as
a tiger state which is profiting from this job miracle thanks to its
low tax rates.  The original of this study seems to be inaccessible on
the Net.  Various summaries and references have been published on
Microsoft's website in the context of Microsoft's lobbying work.  In
2002 this study found its way into the advocacy preface of the
European Commission's software patentability proposal.  This was
apparently due to the influence of BSA in the drafting work.  The
study does not deal with the subject of software patents.  It
correctly states that the biggest asset of software companies is their
manpower, i.e. their ability to manage complex copyrighted works and
quickly turn out nifty software rather than in their patents or their
ability to invent a mousetrap.

#MWw: Microsoft: The growth of the packaged software industry in Norway

#AWi2: An example of Microsoft's use of the Datamonitor Study for political
persuasion efforts in Norway.

#Aoh: A controversial debate between two people about the directive proposal
and the FFII criticism.  The %(q:anonymous coward) is Erik Josefsson
from SSLUG, his opponent is a patent lawyer from Philips who maintains
a %(im:website which argues in favor of the EPO's recent
interpretation of the EPC) (and thus basically also of the directive
proposal).

#pnc: press release of Linux-Verband, an association of Linux companies and
users in Germany

#Mmn: Mingorance denies our allegations that he wrote the proposal, arguing
that he would have written the %(q:form of claims) section
differently.  We think that the decision to not follow the EPO on the
claim form question was indeed taken by the CEC.  We do not doubt that
the CEC played an important role in shaping the proposal.  But so did
Mingorance.

#AWg: An journalist reports that he just phonecalled the European Commission
to find out whether the BSA version that Eurolinux proposed was really
the version that the CEC was going to publish that day.  He was told
by the Commission's press speaker that the version which Eurolinux
published was not theirs but one %(q:from the industry).  Several
French journalists who called on the same morning received the same
answer.  One even received the answer %(q:no, that draft is not from
us but from BSA).

#AvA: A fairly comprehensive account of the BSA/CEC proposal scandal

#CcB: EUK-Presseerklärung steht im Widerspruch zum Inhalt des
RiLi-Vorschlages

#Eui: Eurolinux Warning to journalists issued on 2002-02-20 shortly after
publication of CEC/BSA directive proposal

#Rsr: Robin Webb (UK PTO & Gov't) 2002-02-20: proposes to remove all limits
to patentability and to rewrite Art 52 EPC so as to reflect EPO
practise

#Tla: The UK PTO conducted its own consultation, which showed an
overwhelming wish of software professionals to be free of software
patents.  But the UK PTO, speaking in the name of the UK government,
reinterprets this as a legitimation to remove all limits on
patentability by modifying Art 52 EPC at the Diplomatic Conference in
June 2002.  The %(pr:proposal) has one argument going for it:  while
rendering Art 52 meaningless, it at least brings clarity.  It
describes in fairly straightforward (and yet still sufficiently
deceptive) terms what the EPO is doing and what the patent movement
wants.  This paper was presented by the UKTPO on the same day as the
EU-BSA Directive Proposal.  The UKPTO had held a parallel consultation
exercise in Great Britain.  Its exercise ignored public opinion using
the same double-talk about %(q:technical contribution) and by
selective choice of examples made sure that this talk is misunderstood
by the public.  Yet the UKPTO has shown the Commission that even
massive fraud and %(e:theft of intellectual property with with a
further legal effect), jointly committed by the EPO, the CEC and the
UKPTO against the european software industry and the european public,
can be carried out in a graceful and mannered fashion.

#Ftr: Französischer Pressebericht, der resümiert, dieser RiLi-Entwurf bringe
keinerlei Klarheit

#Jti: gemeinsame Erklärung der europäischen Shareware-Verbände, die den
RiLi-Vorschlag als einen feindlichen Akte gegen die Schöpfer von
Software in Europa verurteilen

#Gyu: deutsche Europarlamentarierin von GUE/NGL warnt vor EU-Richtlinien zu
Kopierschutz und Softwarepatenten

#Prc: Zahlreiche Verweise, u.a. auf neue Presseberichte insbesondere aus
Frankreich

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
# mailto: mlhtimport@ffii.org ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: eubsa-swpat0202 ;
# txtlang: de ;
# multlin: t ;
# End: ;

