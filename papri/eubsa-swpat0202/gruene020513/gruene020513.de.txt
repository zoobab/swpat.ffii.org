<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

Gwa: Grüne gegen EU-Entwurf zur Patentierbarkeit von Software
Mnn: Während die deutsche Bundesregierung sich zur Softwarepatent-Frage bedeckt hält und in Brüsseler Patentlobby-Kreisen bereits als Verbündeter gehandelt wird, finden die Medien- und wirtschaftspolitische Sprecher der Bundestagsfraktion %(q:Bündnis 90 / Die Grünen) in einer Presseerklärung klare Worte.  Darin heißt es u.a. %(bc|Der Richtlinienvorschlag der Europäischen Kommission geht in die falsche Richtung. Softwarepatente behindern Innovationen, freie Softwareentwickler, Open Source und kleine und mittleren Unternehmen. ... Die Kommission hat die vielfältigen Studien, die zu ihrem Konsultationsentwurf vorgelegt worden sind, unzureichend ausgewertet und nun verfrüht einen Entwurf vorgelegt.)  Grundsätzlich stehen die Grünen dem Vorhaben einer EU-Richtlinie aufgeschlossen gegenüber, denn %(bc|Wir haben in der Vergangenheit einen schleichenden Prozess der immer stärkeren Patentierbarkeit von Software erlebt. Hier muss klargestellt werden, dass Software nicht patentierbar ist. Denn eine unsichere Rechtslage schadet den Unternehmen.|Bei der Softwareentwicklung handelt es sich weniger um Erfindungen, als vielmehr - wie der Name schon sagt - um eher handwerkliche Entwicklungsarbeit. Es macht keinen Sinn, die Rechte technischer Erfindungen auf diesen Bereich zu übertragen. Das aber tut der Richtlinienentwurf der Kommission.)
Gna: Grietje Bettin: Grüne gegen Patentierbarkeit von Software
Dfr: Die Erklärung oben auf der Leitseite der für Netz- und Medienpolitik zuständigen Abgeordneten

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: gruene020513 ;
# txtlang: de ;
# End: ;

