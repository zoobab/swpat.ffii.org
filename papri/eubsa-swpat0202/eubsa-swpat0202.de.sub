\begin{subdocument}{eubsa-swpat0202}{EUK & BSA 2002-02-20: Vorschlag, alle nützlichen Ideen patentierbar zu machen}{http://swpat.ffii.org/papri/eubsa-swpat0202/index.de.html}{Arbeitsgruppe\\\url{swpatag@ffii.org}\\deutsche Version 2003/12/20 von Hartmut PILCH\footnote{\url{http://www.ffii.org/\~phm}}}{Die Europ\"{a}ische Kommission (EUK) schl\"{a}gt vor, die Patentierung von Patenten auf Datenverarbeitungsprogrammen als solchen zu legalisieren und sicher zu stellen, dass breite und triviale Patente auf Programm- und Gesch\"{a}ftslogik, wie sie derzeit vor allem in den USA von sich reden machen, k\"{u}nftig auch hier in Europa Bestand haben und von keinem Gericht mehr zur\"{u}ckgewiesen werden k\"{o}nnen.  ``Aber hallo, die EUK sagt in ihrer Presseerkl\"{a}rung etwas ganz anderes!'', m\"{o}chten Sie vielleicht einwenden.  Ganz richtig!  Um herauszufinden, was die EUK wirklich sagt, m\"{u}ssen Sie n\"{a}mlich nicht die PE sondern den Vorschlag selbst lesen.  Aber Vorsicht, der ist in einem Neusprech vom Europ\"{a}ischen Patentamt (EPA) verfasst, in dem gew\"{o}hnliche W\"{o}rter oft das Gegenteil dessen bedeuten, was Sie erwarten w\"{u}rden. Zur Verwirrung tr\"{a}gt noch ein langer werbender Vorspann bei, in dem die Wichtigkeit von Patenten und propriet\"{a}rer Software beschworen wird, wobei dem software-unerfahrenen Zielpublikum ein Zusammenhang zwischen beiden suggeriert wird.  Dieser Text ignoriert die Meinungen von allen geachteten Programmierern und Wirtschaftswissenschaftlern und st\"{u}tzt seine sp\"{a}rlichen Aussagen \"{u}ber die \"{O}konomie der Software-Entwicklung nur auf zwei unver\"{o}ffentlichte Studien aus dem Umfeld von BSA (von Microsoft und anderen amerikanischen Gro{\ss}unternehmen dominierter Verband zur Durchsetzung des Urheberrechts) \"{u}ber die Wichtigkeit propriet\"{a}rer Software.  Diese Studien haben \"{u}berhaupt nicht Softwarepatente zum Thema!  Der Werbe-Vorspann und der Vorschlag selber wurden offensichtlich f\"{u}r die EUK von einem Angestellten von BSA redigiert.  Unten zitieren wir den vollst\"{a}ndigen Vorschlag zusammen mit Belegen f\"{u}r die Rolle von BSA, einer Analyse des Inhalts und einer tabellarischen Gegen\"{u}berstellung der BSA- und EUK-Version sowie einer EP\"{U}-Version, d.h. eines Gegenvorschlages im Geiste des Europ\"{a}ischen Patent\"{u}bereinkommen von 1973 und der aufgekl\"{a}rten Patentliteratur.  Die EP\"{U}-Version sollte Ihnen helfen, die Klarheit und Weisheit der heute g\"{u}ltigen gesetzlichen Regelung verstehen, an deren Aushebelung die Patentanw\"{a}lte der Europ\"{a}ischen Kommission gemeinsam mit EPA, BSA u.a. in den letzten Jahren hart gearbeitet haben.}
\begin{sect}{intro}{Introduction}
\begin{itemize}
\item
{\bf {\bf Introduction\footnote{\url{}}}}

\begin{quote}
Warum all dieses Aufhebens um den Vorschlag der Europ\"{a}ischen Kommission, Datenverarbeitungsprogramme patentierbar zu machen?  Was hat BSA damit zu tun?
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{esse}{Essence of the Proposal}
\begin{itemize}
\item
{\bf {\bf Essence of the Proposal\footnote{\url{}}}}

\begin{quote}
Die Europ\"{a}ische Kommission pr\"{a}sentiert ihren Vorschlag ``\"{u}ber die Patentierbarkeit computer-implementierter Erfindungen'' als gem\"{a}{\ss}igten Versuch, ``das Recht zu harmonisieren'' und ``klarzustellen'', dass ``amerikanische Verh\"{a}ltnisse'' von Europa fern gehalten werden sollen.  Offenbar wei{\ss} die Europ\"{a}ische Kommission sehr gut, was f\"{u}r einen Vorschlag die \"{O}ffentlichkeit gerne sehen w\"{u}rde.  Bei genauerem Lesen des Vorschlags stellt sich aber schnell heraus, dass die Europ\"{a}ische Kommission das Gegenteil von dem h\"{a}lt, was sie verspricht.  Hier finden Sie eine kurze Zusammenfassung dessen, was der Vorschlag im aktuellen europ\"{a}ischen Kontext tats\"{a}chlich tut.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{text}{Tabellarische Textausgabe in BSA- und EUK-Fassung in Gegen\"{u}berstellung mit Gegenvorschlag im Geiste des EP\"{U}}
Here we are presenting a critical edition of the text, with annotation and a tabular comparison between the CEC/BSA initial and final draft and an added FFII version, which shows what is wrong with the CEC/BSA version and how it could be rewritten in a coherent and adequate way.  We highlighted some differences by bold typeface.

\begin{sect}{pref}{BSA/CEC Advocative Preface}
\begin{itemize}
\item
{\bf {\bf BSA/CEC Advocative Preface\footnote{\url{}}}}

\begin{quote}
The European Commission's software patentability proposal of 2002-02-20 is based on a draft from the Business Software Alliance (BSA).  Especially the advocative preface of this proposal contains arguments and materials that were supplied by the BSA.  The argumentation tries to create a vague impression that ``business software'' or ``packaged software'' is related to patents, jobs, and wealth, and that purely copyright-based software is related to an ``open-source movement'' that wants everything in the world to be available for free.  The argumentation is full of popular errors and lies, which are easily discovered by comparing it to our debugged version, listed on the right side of the table.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{prop}{EU Software Patent Directive Amendment Proposals}
\begin{itemize}
\item
{\bf {\bf EU Software Patent Directive Amendment Proposals\footnote{\url{}}}}

\begin{quote}
Am 2002-02-20 schlug die Europ\"{a}ische Kommission vor, Comptuterprogramme als patentierbare Erfindungen zu betrachten und es sehr schwer zu machen, eine Patentanmeldung auf einen Algorithmus oder eine logische Funktionalit\"{a}t, einschlie{\ss}lich Gesch\"{a}ftsmodell, abzulehnen, sofern diese in den typischen Begriffen der Informatik (e.g. Rechner, Ein- und Ausgabe, Speicher, Datenbank etc) beansprucht wird.  Wir haben einen Gegenvorschlag ausgearbeitet, der die Freiheit des rechnergest\"{u}tzten Denkens, Rechnens, Organisierens und Formulierens und das urheberrechtliche Eigentum der Software-Autoren wahrt und zugleich die Patentierbarkeit von technischen Erfindungen (Probleml\"{o}sung durch Einsatz beherrschbarer Naturkr\"{a}fte) bekr\"{a}ftigt, wie sie im Europ\"{a}ischen Patent\"{u}bereinkommen (EP\"{U}), dem TRIPs-Abkommen und in musterg\"{u}ltigen Kommentaren und Lehrb\"{u}chern des Patentrechts zum Ausdruck kommt.  Dieser Gegenvorschlag erf\"{a}hrt die Unterst\"{u}tzung zahlreicher ma{\ss}geblicher Akteure in den Bereich Software, Wirtschaftswissenschaften, Politik und Recht.
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{fins}{Financial Statement}
\begin{itemize}
\item
{\bf {\bf Financial Statement\footnote{\url{}}}}

\begin{quote}
nur BSA
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{post}{The Proposal}
\begin{itemize}
\item
{\bf {\bf The Proposal\footnote{\url{}}}}

\begin{quote}
Werbendes Nachwort, nur in der BSA-Fassung
\end{quote}
\filbreak
\end{itemize}
\end{sect}

\begin{sect}{fnot}{Footnotes}
\begin{itemize}
\item
{\bf {\bf Footnotes\footnote{\url{}}}}

\begin{quote}
Footnotes in the BSA version and the European Commission's version of the EU software patentability directive proposal of 2002-02-20
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{sect}

\begin{sect}{tasks}{Fragen, Aufgaben, Wie Sie helfen k\"{o}nnen}
siehe FFII/Eurolinux 2003/04 Letter to Software Creators and Users\footnote{\url{http://localhost/swpat/xatra/parl034/index.de.html}}
\end{sect}

\begin{sect}{links}{Annotated Links}
\begin{itemize}
\item
{\bf {\bf JURI working documents\footnote{\url{http://www.europarl.eu.int/meetdocs/committees/juri/20020619/juri20020619.htm}}}}

\begin{quote}
Working documents of the Legal Affairs Commission of the European Parliament about the CEC/BSA software patentability proposal of 2002-02-20, published 2002-06-19, inititially consisting of a short report by MEP Arlene McCarthy and a study ordered by the European Commission.
\end{quote}
\filbreak

\item
{\bf {\bf Europarl Hearing of 2002-05-15\footnote{\url{http://localhost/swpat/penmi/2002/swpparl025/index.de.html}}}}

\begin{quote}
Eine Gruppe von Abgeordneten des Europ\"{a}ischen Parlaments lud Anthony Howard (der in der Dienststelle f\"{u}r Gewerblichen Rechtschutz der Europ\"{a}ischen Kommission f\"{u}r die Softwarepatentierbarkeitsrichtlinie zust\"{a}ndig ist) und Hartmut Pilch von FFII/Eurolinux zu einer \"{o}ffentlichen Anh\"{o}rung in Stra{\ss}burg ein.  Hier finden Sie die Einzelheiten dieser Anh\"{o}rung, u.a. ein paar Pr\"{a}sentationsschaubilder.
\end{quote}
\filbreak

\item
{\bf {\bf Patentability Legislation Benchmarking Test Suite\footnote{\url{http://localhost/swpat/stidi/manri/index.de.html}}}}

\begin{quote}
Um eine Patentierbarkeitsrichtlinie auf Tauglichkeit zu pr\"{u}fen, sollten wir sie an Beispiel-Innovationen ausprobieren.  F\"{u}r jedes Beispiel gibt es einen Stand der Technik, eine technische Lehre und eine Reihe von Anspr\"{u}chen.  In der Annahme, dass die Beispiele zutreffend beschrieben wurden, probieren wir dann unsere neue Gesetzesregel daran aus.  Unser Augenmerkt liegt auf (1) Klarheit (2) Angemessenheit:  f\"{u}hrt die vorgeschlagene Regelung zu einem vorhersagbaren Urteil?  Welche der Anspr\"{u}che w\"{u}rden erteilt?  Entspricht dieses Ergebnis unseren W\"{u}nschen?  Wir probieren verschiedene Gesetzesvorschl\"{a}ge an der gleichen Beispielserie (Testsuite) aus und vergleichen, welches am besten abschneidet.  F\"{u}r Programmierer ist es Ehrensache, dass man ``die Fehler beseitigt, bevor man das Programm freigibt'' (first fix the bugs, then release the code).  Testsuiten sind ein bekanntes Mittel zur Erreichung dieses Ziels.  Gem\"{a}{\ss} Art. 27 TRIPS geh\"{o}rt die Gesetzgebung zu einem ``Gebiet der Technik'' namens ``Sozialtechnik'' (social engineering), nicht wahr?  Technizit\"{a}t hin oder her, es ist Zeit an die Gesetzgebung mit derjenigen methodischen Strenge heran zu gehen, die \"{u}berall dort angesagt ist, wo schlechte Konstruktionsentscheidungen das Leben der Menschen stark beeintr\"{a}chtigen k\"{o}nnen.
\end{quote}
\filbreak

\item
{\bf {\bf Slashdot: Directive Proposal is Unclear\footnote{\url{http://yro.slashdot.org/comments.pl?sid=29501&threshold=-1&commentsort=0&tid=155&mode=thread&pid=3174509}}}}

\begin{quote}
A controversial debate between two people about the directive proposal and the FFII criticism.  The ``anonymous coward'' is Erik Josefsson from SSLUG, his opponent is a patent lawyer from Philips who maintains a website which argues in favor of the EPO's recent interpretation of the EPC\footnote{\url{http://www.iusmentis.com/patents/software/}} (and thus basically also of the directive proposal).
\end{quote}
\filbreak

\item
{\bf {\bf (All French Presidential Candidates against Swpat)\footnote{\url{(mail news 2002 3 5)}}}}

\begin{quote}
Die Spitzenkandidaten aller franz\"{o}sischen Parteien haben sich gegen deutlich gegen die Patentierbarkeit von Software-Ideen ausgesprochen und dabei z.T. recht unterschiedliche politische Vorstellungen zur Begr\"{u}ndung dieser Position angef\"{u}hrt.
\end{quote}
\filbreak

\item
{\bf {\bf UK Patent Orifice: Software Patentability Rally Brussels 2002-06-19\footnote{\url{http://localhost/swpat/penmi/2002/ukpo06/index.de.html}}}}

\begin{quote}
The UK Patent Office, wearing the hat of the British Government, has entrusted one of its subcontractors to organise a high profile rally in support of the proposed CEC/BSA software patentability directive.  Speeches will be held by hard core patent movement activists from the European Commission, the UK Government, the European Patent Office, the IBM patent department and some well-known patent law firms.  At the end of an intensive 6 hour long software patentablity propaganda firework a podium discussion will be held in which an ``open source representative'' will be allowed to sit at the table.  The organisers' initial choice was Alan Cox.  Alan has occasionally expressed deep concern about the software patent problem but, as a core developper of the Linux kernel and other key projects, he never really had the time to study the legal intricacies.  This fact alongside with his long hair and beard seemed to make him particularly qualified for the role of the social romantic dissident, generously tolerated by a group of patent lawyers posing as the businessmen of the real software industry.  Alan immediately understood the game and declined the invitation, handing the case over to the Eurolinux Alliance.  We have meanwhile been put on the conference program, but our abstract was doctored, our criticism suppressed, our message distorted, so that now we nevertheless fit into the design of this rally.
\end{quote}
\filbreak

\item
{\bf {\bf Lenz 2002-03-01: Sinking the Software Patent Proposal\footnote{\url{http://k.lenz.name/LB/archives/000264.html}}}}

\begin{quote}
Karl-Friedrich Lenz, professor of European Law, lists some legal and constitutional arguments to explain why the CEC/BSA proposal is a legal and political scandal, starting from the fact that the European Commission is using ``harmonisation'' and ``clarification'' merely as pretexts to declare itself competent for promoting an unspeakable political agenda which does not fall in the Commission's competentce.

siehe auch Bundesregierung verklagt Brüssel wegen Kompetenzerschleichung unter dem Vorwand der \percent{}(q:Binnenmarkt-Harmonisierung)\footnote{\url{http://medi-report.de/nachrichten/2000/06/20000615-06.htm}}
\end{quote}
\filbreak

\item
{\bf {\bf LiVe 2002-02-28: Scandalous push for unlimited patentability\footnote{\url{http://www.linux-verband.de/nachrichten/allgemein/20020228102108}}}}

\begin{quote}
press release of Linux-Verband, an association of Linux companies and users in Germany
\end{quote}
\filbreak

\item
{\bf {\bf BSA Europe - Press Release - BSA Responds to EU Software Patent Directive Adoption\footnote{\url{http://www.bsa.org/europe-eng/press/newsreleases/2002-02-21.936.phtml}}}}

\begin{quote}
Mingorance denies our allegations that he wrote the proposal, arguing that he would have written the ``form of claims'' section differently.  We think that the decision to not follow the EPO on the claim form question was indeed taken by the CEC.  We do not doubt that the CEC played an important role in shaping the proposal.  But so did Mingorance.
\end{quote}
\filbreak

\item
{\bf {\bf \url{(mail fitug 2002 2 489)}}}

\begin{quote}
An journalist reports that he just phonecalled the European Commission to find out whether the BSA version that Eurolinux proposed was really the version that the CEC was going to publish that day.  He was told by the Commission's press speaker that the version which Eurolinux published was not theirs but one ``from the industry''.  Several French journalists who called on the same morning received the same answer.  One even received the answer ``no, that draft is not from us but from BSA''.
\end{quote}
\filbreak

\item
{\bf {\bf ZDNet News: Row erupts over European patent plan\footnote{\url{http://zdnet.com.com/2100-1104-842159.html}}}}

\begin{quote}
A fairly comprehensive account of the BSA/CEC proposal scandal
\end{quote}
\filbreak

\item
{\bf {\bf FAQ\footnote{\url{}}}}
\filbreak

\item
{\bf {\bf Eurolinux 2002-02-20: Collusion Discovered between BSA and Euorpean Commission\footnote{\url{http://petition.eurolinux.org/pr/pr17.html}}}}

\begin{quote}
Eurolinux Press Release
\end{quote}
\filbreak

\item
{\bf {\bf EUK-Presseerkl\"{a}rung steht im Widerspruch zum Inhalt des RiLi-Vorschlages\footnote{\url{http://lists.ffii.org/archive/mails/news/2002/Feb/0007.html}}}}

\begin{quote}
Eurolinux Warning to journalists issued on 2002-02-20 shortly after publication of CEC/BSA directive proposal
\end{quote}
\filbreak

\item
{\bf {\bf Robin Webb (UK PTO \& Gov't) 2002-02-20: proposes to remove all limits to patentability and to rewrite Art 52 EPC so as to reflect EPO practise\footnote{\url{http://www.patent.gov.uk/about/ippd/issues/index.htm\#Software Patents}}}}

\begin{quote}
The UK PTO conducted its own consultation, which showed an overwhelming wish of software professionals to be free of software patents.  But the UK PTO, speaking in the name of the UK government, reinterprets this as a legitimation to remove all limits on patentability by modifying Art 52 EPC at the Diplomatic Conference in June 2002.  The proposal\footnote{\url{http://www.patent.gov.uk/about/ippd/issues/software.htm}} has one argument going for it:  while rendering Art 52 meaningless, it at least brings clarity.  It describes in fairly straightforward (and yet still sufficiently deceptive) terms what the EPO is doing and what the patent movement wants.  This paper was presented by the UKTPO on the same day as the EU-BSA Directive Proposal.  The UKPTO had held a parallel consultation exercise in Great Britain.  Its exercise ignored public opinion using the same double-talk about ``technical contribution'' and by selective choice of examples made sure that this talk is misunderstood by the public.  Yet the UKPTO has shown the Commission that even massive fraud and \emph{theft of intellectual property with with a further legal effect}, jointly committed by the EPO, the CEC and the UKPTO against the european software industry and the european public, can be carried out in a graceful and mannered fashion.
\end{quote}
\filbreak

\item
{\bf {\bf L'innovation logicielle remise en cause par la Commission européenne\footnote{\url{http://solutions.journaldunet.com/0202/020222_eurolinux.shtml}}}}

\begin{quote}
Franz\"{o}sischer Pressebericht, der res\"{u}miert, dieser RiLi-Entwurf bringe keinerlei Klarheit
\end{quote}
\filbreak

\item
{\bf {\bf Une directive contre le logiciel européen\footnote{\url{http://www.europeshareware.free.fr/pages/pr_swpat3.html}}}}

\begin{quote}
gemeinsame Erkl\"{a}rung der europ\"{a}ischen Shareware-Verb\"{a}nde, die den RiLi-Vorschlag als einen feindlichen Akte gegen die Sch\"{o}pfer von Software in Europa verurteilen
\end{quote}
\filbreak

\item
{\bf {\bf Ilka Schr\"{o}der: Patent\"{u}berwachung\footnote{\url{https://www.ilka.org/material/denkpause/17/patentueberwachung.html}}}}

\begin{quote}
deutsche Europarlamentarierin von GUE/NGL warnt vor EU-Richtlinien zu Kopierschutz und Softwarepatenten
\end{quote}
\filbreak

\item
{\bf {\bf European Commission will propose to replace clear limits on patentability with empty words\footnote{\url{http://localhost/eurolinux/news/warn01C/index.de.html}}}}

\begin{quote}
Die Eurolinux-Allianz von Softwarefirmen und Verb\"{a}nden wurde aus verl\"{a}sslichen Quellen unterrichtet, dass die Europ\"{a}ische Kommission (EuK) in wenigen Tagen einen Vorschlag f\"{u}r eine EG-Richtlinie \"{u}ber die Grenzen der Patentierbarkeit im Hinblick auf Computerprogramme ver\"{o}ffentlichen wird.  Die meisten Programmierer wollen keine Patente sondern nur das Urheberrecht angewendet sehen.  Volkswirtschaftlichen Forschungsergebnisse deuten darauf hin, dass Softwarepatente die Wirtschaft eher bremsen als ankurbeln.  Die EuK wird diesen Einsichten in ihrer Presseerkl\"{a}rung verbalen Tribut zollen.  In ihrem Richtlinienentwurf wird sie jedoch vorschlagen, Softwarepatente amerikanischer Pr\"{a}gung in Europa zu legalisieren und alle wirksamen Grenzen der Patentierbarkeit zu beseitigen.  Wer \"{u}ber Grundkenntnisse in Euro-Patentkauderwelsch verf\"{u}gt, wird die Diskrepanz leicht bemerken.  Es ist nicht so schwierig, wie es aussieht.  Nehmen Sie sich 20 Minuten Zeit!  Wir erschlie{\ss}en Ihnen den Zugang zu einer Debatte, die zumindest in den n\"{a}chsten 1-2 Jahren, w\"{a}hrend der Richtlinienvorschlag beim europ\"{a}ische Parlament und beim europ\"{a}ischen Rat anliegt, noch hohe Wellen schlagen d\"{u}rfte.
\end{quote}
\filbreak

\item
{\bf {\bf Bernard Lang: The CEC/BSA Directive Proposal and the reactions\footnote{\url{http://pauillac.inria.fr/\~lang/arte/brevet/directive.html}}}}

\begin{quote}
Zahlreiche Verweise, u.a. auf neue Presseberichte insbesondere aus Frankreich
\end{quote}
\filbreak

\item
{\bf {\bf Datamonitor 2000-09: Packaged Software Industry in Europe\footnote{\url{http://www.microsoft.com/europe/newsletter/docs/issue9_datamonitor.doc}}}}

\begin{quote}
A rarely cited and largely unknown study by a company called Datamonitor about the importance of proprietary software as a creator of jobs in Europe.  The study claims that proprietary software will create 1/2 million new jobs in the next few years.  Praises Ireland as a tiger state which is profiting from this job miracle thanks to its low tax rates.  The original of this study seems to be inaccessible on the Net.  Various summaries and references have been published on Microsoft's website in the context of Microsoft's lobbying work.  In 2002 this study found its way into the advocacy preface of the European Commission's software patentability proposal.  This was apparently due to the influence of BSA in the drafting work.  The study does not deal with the subject of software patents.  It correctly states that the biggest asset of software companies is their manpower, i.e. their ability to manage complex copyrighted works and quickly turn out nifty software rather than in their patents or their ability to invent a mousetrap.
\end{quote}
\filbreak

\item
{\bf {\bf Microsoft: The growth of the packaged software industry in Norway\footnote{\url{http://www.microsoft.com/europe/newsletter/docs/issue9_norway.doc}}}}

\begin{quote}
An example of Microsoft's use of the Datamonitor Study for political persuasion efforts in Norway.
\end{quote}
\filbreak

\item
{\bf {\bf Quotations on Software Patents\footnote{\url{http://localhost/swpat/vreji/cusku.de.html}}}}

\begin{quote}
Einschl\"{a}gige Zitate aus Rechtstexten, wirtschaftwissenschaftlichen Analysen, politischen Beschl\"{u}ssen und Aussagen von Programmierern, Informatikern, Wissenschaftlern, Politikern und anderen Pers\"{o}nlichkeiten des \"{o}ffentlichen Lebens.
\end{quote}
\filbreak

\item
{\bf {\bf Research on the MacroEconomic Effects of Patents\footnote{\url{http://localhost/swpat/vreji/minra/sisku.de.html}}}}

\begin{quote}
Seit Fritz Machlups Bericht an den US-Kongress von 1958 hat sich eine Reihe von Studien \"{u}ber die Wirkungen des Patentwesens auf verschiedene Bereiche der Volkswirtschaft angesammelt.  Einige besch\"{a}ftigen sich mit besonderen Typen von Innovation (sequentiell, in komplexen Systemen) oder mit besonderen Branchen (Halbleiter, Software, Genetik).  Alle Studien weisen darauf hin, dass Patente zumindest an dem Ende der typologischen Skala, zu dem Software geh\"{o}rt, die Innovation eher hemmen als f\"{o}rdern.  Einige regierungsgef\"{o}rderte Studien aus dem Umfeld der Patentbewegung (Institute f\"{u}r Geistiges Eigentum etc) kombinieren negative Befunde \"{u}ber die volkswirtschaftlichen Wirkungen von Softwarepatenten mit der Forderung nach Legalisierung von Softwarepatenten.
\end{quote}
\filbreak

\item
{\bf {\bf Software Patentability with Compensatory Regulation: a Cost Evaluation\footnote{\url{http://localhost/swpat/stidi/pleji/index.de.html}}}}

\begin{quote}
Europa bereitet wesentliche \"{A}nderungen seines Patentsystem vor.  Das Europ\"{a}ische Patentamt (EPA) hat vorgeschlagen, Beschr\"{a}nkungen der Patentierbarkeit wie z.B. den Ausschluss von Computerprogrammen in Art 52 des Europ\"{a}ischen Patent\"{u}bereinkommens (EP\"{U}) aufzuheben.  Eine Stellungnahme der Franz\"{o}sischen Technikakademie (Acad\'{e}mie des Technologies) unterst\"{u}tzt diesen Plan aber schl\"{a}gt zus\"{a}tzlich Regelung zur Begrenzung m\"{o}glicher sch\"{a}dlicher Folgen von Softwarepatenten vor.  In diesem Artikel versuchen wir, die Kosten solcher Ausgleichsregelungen zu ermitteln.  Es ergeben sich j\"{a}hrliche Ausgaben von 1-5 Milliarten EUR f\"{u}r die Europ\"{a}ische Gemeinschaft.  Verschiedene Ans\"{a}tze und billigere Alternativen werden in diesem Artikel untersucht.
\end{quote}
\filbreak

\item
{\bf {\bf Patentjurisprudenz auf Schlitterkurs -- der Preis für die Demontage des Technikbegriffs\footnote{\url{http://localhost/swpat/stidi/korcu/index.de.html}}}}

\begin{quote}
Bisher geh\"{o}ren Computerprogramme ebenso wie andere \emph{Organisations- und Rechenregeln} in Europa nicht zu den \emph{patentf\"{a}higen Erfindungen}, was nicht ausschlie{\ss}t, dass ein patentierbares Herstellungsverfahren durch Software gesteuert werden kann. Das Europ\"{a}ische Patentamt und einige nationale Gerichte haben diese zun\"{a}chst klare Regel jedoch immer weiter aufgeweicht.  Dadurch droht das ganze Patentwesen in einem Morast der Beliebigkeit, Rechtsunsicherheit und Funktionsuntauglichkeit zu versinken.  Dieser Artikel gibt eine Einf\"{u}hrung in die Thematik und einen \"{U}berblick \"{u}ber die rechtswissenschaftliche Fachliteratur.
\end{quote}
\filbreak
\end{itemize}
\end{sect}
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /usr/share/emacs/site-lisp/phm/sys/mlht.el ;
% mode: latex ;
% End: ;

