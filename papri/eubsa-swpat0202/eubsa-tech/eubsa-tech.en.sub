\begin{subdocument}{eubsa-tech}{Why Amazon One Click Shopping is Patentable under the Proposed EU Directive}{http://swpat.ffii.org/papers/eubsa-swpat0202/eubsa-tech/index.en.html}{Workgroup\\swpatag@ffii.org}{According to European Commissions (CEC)'s Directive Proposal COM(2002)92 for the ``Patentability of Computer-Implemented Inventions'' and the revised version approved by the European Parliament's Committee for Legal Affairs and the Internal Market (JURI), algorithms and business methods such as Amazon One Click Shopping are without doubt patentable subject matter. This is because \begin{enumerate}
\item
Any ``computer-implemented'' algorithm or business method is considered in principle an patentable subject matter

\item
The additional requirement of ``technical contribution in the inventive step'' is not a real obstacle.

\item
CEC and JURI have built in additional layers of safeguard to ensure that, even if some of the most objectionable provisions are revised by the Parliament, unlimited patentability still remains ensured.
\end{enumerate}}
According to EPO/CEC/JURI doctrine, the ``inventive step'' between the ``closest prior art'' and the ``invention'' must involve what is called a ``technical contribution'' or ``solution of a technical problem'': an improvement of computing efficiency, e.g. a reduction in the number of mouse-clicks.  This ``technical contribution in the inventive step'' exists in different wordings in the US as well.  US patent attorneys therefore generally write business method patents in a way that allows them to pass the EPO's requirements, and thousands have already passed.  An attorney who is unable to patent algorithms and business methods under the EPO/CEC/JURI Directive would not be worth his lawyer's fee.

As if this was still not enough, CEC/JURI take care to ensure that the ``technical contribution'' can consist of ``non-technical features'' only, and that the term ``technical'' is not defined.  Contrary amendment proposals from other Parliamentary committees were rejected by JURI.

The word ``technical'' is still in use at the EPO.  In some decisions\footnote{http://swpat.ffii.org/papers/epo-t990049/index.en.html} it retains its old meaning of ``concrete and physical''.  In others it seems to mean only ``concrete'' or even ``related to specialist knowledge''.  One EPO judge proposed\footnote{http://swpat.ffii.org/papers/jwip-schar98/index.en.html} to redefine ``technical'' as ``practical''.

The EPO deliberately maintains this ambiguous and undefined status of the term ``technical''.  One EPO official explains\footnote{http://elj.warwick.ac.uk/jilt/03-1/aharonian.htm} why:

\begin{quote}
{\it I'm glad you asked me about that. It's a wonderful word, fuzzy and yet sounds meaningful. We love it. Until 2001, it had no basis whatsoever in the EPC, just a passing mention in a couple of rules. .... I agree that we've never defined 'technical'. It's deliberate and allows us to fine-tune as a consensus develops on what should be patentable. Yes, I do know the correct way to do that is by amending the law, but have you any idea how hard it is to get consensus on amending the EPC?}
\end{quote}

Proponents of the software patentability directive at the European Commission also insist that the word ``technical'' \begin{enumerate}
\item
must be the only criterion for distinguishing patentable from unpatentable

\item
must be without definition, because ``inventions are by definition imprevisible'' and ``technology is not static''.
\end{enumerate}  MEP Arlene McCarthy also draws upon the ambiguity of the word ``technical'' when she promotes the directive proposal\footnote{http://swpat.ffii.org/papers/eubsa-swpat0202/amccarthy0305/index.en.html\#nontech} as follows:

\begin{center}
\begin{tabular}{|C{89}|}
\hline
\begin{quote}
{\it Programmes which underpin non-technical inventions will also not be patentable, in particular those concerned with non-technical business methods (such as ``reverse auction'' arrangements) along with the methods themselves. Although these are widely patented in the United States, in Europe it has been shown that there to be no case for such extension of patents in Europe.}
\end{quote}\\\hline
\end{tabular}
\end{center}

McCarthy excludes only ``non-technical business methods'' from patentability, and the European Patent Office (EPO) finds all ``computer-implemented business methods'' to be ``technical''.  Let us quote what the EPO and leading patent practitioners have to say about this:

\ifmlhtlinks
\begin{itemize}
\item
{\bf {\bf EPO 2000/05/19: Examination of ``business method'' applications\footnote{http://swpat.ffii.org/papers/epo-tws-app6/index.en.html}}}

\begin{quote}
\begin{center}
\begin{tabular}{|C{89}|}
\hline
\begin{quote}
{\it It should be emphasised that ... the computer implementation of a ... business method, can involve ``technical considerations'', and therefore be considered the solution of a technical problem, if implementation features are claimed.}
\end{quote}\\\hline
\end{tabular}
\end{center}
\end{quote}
\filbreak

\item
{\bf {\bf W\"{u}sthoff 2001: Strategic Advice to Banks on Business Method Patents in Europe\footnote{http://swpat.ffii.org/papers/wuesthoff-bm01/index.en.html}}}

\begin{quote}
\begin{center}
\begin{tabular}{|C{89}|}
\hline
\begin{quote}
{\it Of course, the liberal practice in the U.S. requires to file almost any business method for patent. These business methods are always implemented by a computer (I have not seen any other example).

[\dots]

When studying a business method computer software program it turns out that in most cases that program includes at least one aspect (feature) which might qualify under the European standards as ``technical''.}
\end{quote}\\\hline
\end{tabular}
\end{center}
\end{quote}
\filbreak

\item
{\bf {\bf K. Beresford, World Patent Information 23 (2001) 253-263: European patents for software, E-commerce and business model inventions.}}

\begin{quote}
Abstract: It is commonly held that software, e-commerce and business models related inventions are inhenrently unpatentable in Europe. In reviewing the situation, this article emphasises the large number of inventions in this field which have in fact been patented through the EPO or through national patent offices in Europe. [...] He shows that, if proper attention is paid to both the substance and the form of claims and description, to direct the reader to the technical problem and its solution, effective protection is in fact very often available.
\end{quote}
\filbreak

\item
{\bf {\bf Iusmentis: EPO Business Method Patents\footnote{http://www.iusmentis.com/patents/businessmethods/epoexamples/}}}

\begin{quote}
A patent attorney informs about the EPO's practise of granting business method patents, citing numerous examples.
\end{quote}
\filbreak

\item
{\bf {\bf EPO T0931/95: Pension Benefit System\footnote{http://legal.european-patent-office.org/dg3/biblio/t950931eu1.htm}}}

\begin{quote}
EPO decides in 2000 that the ``computer-implemented business method'' in question is statutory subject matter (i.e. an ``invention''), but does not ``make a technical contribution in its inventive step''.  Amidst their efforts at revising Art 52 EPC in summer 2000, EPO representatives immediately touted this as the new model caselaw of the EPO.  It later became, together with Appendix 6 of the Trilateral Conference documentation, the basis of the CEC software patent directive draft.  It is however to be doubted whether this decision provides a clear guidance to anyone except perhaps for claim-drafting patent attorneys.  The judges admit that their basis for rejecting the patent is undefined:  \begin{quote}
{\it It may very well be that, as put forward by the appellant, the meaning of the term ``technical'' or ``technical character'' is not particularly clear. However, this also applies to the term ``invention''. In the Board's view the fact that the exact meaning of a term may be disputed does in itself not necessarily constitute a good reason for not using the term as a criterion, certainly not in the absence of a better term; case law may clarify this issue.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Erwin J. Basinski: Business Method Patents in Europe - A Saussurean Explanation\footnote{http://www.mofo.com/news/general.cfm?MCatID=\&concentrationID=\&ID=141\&Type=5}}}

\begin{quote}
A US patent attorney explains how claims to business methods must be crafted if they are to pass at the EPO in the light of the ``Pension Benefit System'' decision.  Basinski concludes that \begin{quote}
{\it If you have any intent to file a Business Method type application in Europe/Japan/etc. and even for purposes of the US you will have to approach the invention a little differently.  The EPO rules should generally be followed because if you do it their way it should work in the US and Japan.}
\end{quote}  Basinski explains that, by following the EPO's rules, the applicant will have to add some redundant specifications to his patent description which do not make the patent less broad but rather more cryptic: \begin{quote}
{\it In the background section, one should describe the ``technical problem'' addressed by the invention, in terms of how the system/method makes the computer function ``more efficiently'' ``faster'' ``in a less costly way'' ``more of something'' in doing what the invention is doing (such that you have a ``further technical effect'' rather than just the interaction of computer and its instructions). The description of the technical solution then becomes a cryptic statement of what the invention does and should specify the novelty and inventive step.}
\end{quote}
\end{quote}
\filbreak

\item
{\bf {\bf Kober 2001: Die Rolle des Europ\"{a}ischen Patentamts\footnote{http://swpat.ffii.org/papers/grur-kober0106/index.de.html}}}

\begin{quote}
Dr. h.c. Ingo Kober, president of the European Patent Office (EPO), explains how much the european patent system has already achieved, how important it has become for the knowledge economy and that, as far as software and business methods are concerned, the EPO enjoys the full support of the national governments.  Yes, they did not fulfill the EPO's wish to remove the computer program and business method exclusions in Art 52 EPC at the Diplomatic Conference of November 2000, but that does not change the fact that the EPO, as its president explains, the EPO grants patents on \emph{computer programs and computer-implemented business methods}, nor does it imply any criticism of this EPO practise.
\end{quote}
\filbreak

\item
{\bf {\bf BPatG Error Search 2002/03/26: system for improved computing efficiency = program as such\footnote{http://swpat.ffii.org/papers/bpatg17-suche02/index.de.html}}}

\begin{quote}
In 2002 the German Federal Patent Court (Error Search decision, 26 March 2002, BPatG 17 W (pat) 69/98) rejects an EPO patent on a non-technical innovation and points out that the EPO's doctrine of ``technical contribution'' makes all business methods patentable and is therefore in violation of the European Patent Convention:

\begin{center}
\begin{tabular}{|C{89}|}
\hline
\begin{quote}
{\it The applicant sees as a decisive indication of technicity of the method that it is based on a technical problem.  Because the proposed method does not need a dictionary, the memory space for this can be saved.  [...] As far as the technical problem is concerned, this can only be considered as an indication but not as a proof of technicity of the process.  If computer implementations of non-technical processes were attributed a technical character merely because they display different specific characteristics, such as needing less computing time or less storage space, the consequence of this would be that any computer implementation would have to be deemed to be of technical character.  This is because any distinct process will have distinct implementation characteristics, that allow it to either save computing time or save storage space.  These properties are, at least in the present case, not based on a technical achievement but result from the chosen non-technical method.  If the fact that such a problem is solved could be a sufficient reason for attributing a technical character to a computer implementation, then every implementation of a non-technical method would have to be patentable; this however would run against the conclusion of the Federal Court of Justice that the legal exclusion of computer programs from patentability does not allow us to adopt an approach which would make any teaching that is framed in computer-oriented instructions patentable.}
\end{quote}\\\hline
\end{tabular}
\end{center}
\end{quote}
\filbreak
\end{itemize}
\else
\dots
\fi

From this it becomes clear that the ``technical contribution'' means \emph{trivial improvement of computing efficiency}, as found in any ``computer-implemented business method''.  This ``contribution'' is not ``made'' but \emph{constructed} by the patent attorney.  A patent attorney who can not construct a ``technical problem'' from a business method would hardly be worth his fees.  And any solution to a ``technical problem'' will necessarily entail some kind of improvement in computing efficiency.

Once software becomes patentable, the word ``technical'' can no longer mean ``concrete and physical'' but at best ``concrete''.  However, since all non-trivial advances in software are of abstract nature\footnote{http://swpat.ffii.org/papers/ist-tamai98/index.en.html}, ``concrete'' in this context implies ``trivial''.  Which of course does not mean that a business method must be trivial in order to qualify for patentability at the EPO.  E.g. Karmarkar's solution to the problem of the travelling salesman was not trivial.  However its ingenuity lies in the field of mathematics, and in order to qualify for an EPO patent, ingenuity in mathematics is said to be irrelevant, whereas novelty (compared to known mathematical methods) and the presence of a technical contribution (trivial improvement in efficiency of solving a constructed computing problem) are necessary conditions.
\end{subdocument}

% Local Variables: ;
% coding: utf-8 ;
% srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
% mode: mlatex ;
% End: ;

