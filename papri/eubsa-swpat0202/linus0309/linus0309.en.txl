<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Linus Torvals & Alan Cox 2003/09/22: Letter to the European Parliament

#descr: explains dangers of software patents and chances of the European Parliament to save Europe from these dangers this week, recommends that MEPs should support the voting recommendations of FFII.

#hft: Open Letter to the Honourable Pat Cox, the President of the European Parliament, members of the European Parliament:

#eMC: Dear Mr. Cox,

#cer: We have been following with growing concern that Europe has been extending patentability to computer programs. Now European Parliament is about to vote on a directive that could put a stop to this development, or make it worse, depending on how it is amended by the Parliament.

#jdd: US experience shows that, unlike traditional patents, software patents do not encourage innovation and R&D, quite the contrary. In particular they hurt small and medium-sized enterprises and generally newcomers in the market. They will just weaken the market and increase spending on patents and litigation, at the expense of technological innovation and research.

#stW: Especially dangerous are attempts to abuse the patent system by preventing interoperability as a means of avoiding competition with technological ability. Standards should never be patentable! Likewise, patents should never be used as means for preventing publication of information - the whole idea of patents is to provide time-limited monopoly in exchange for publication of the invention.

#dhr: Software patents are also the utmost threat to the development of Linux and other free software products, as we are forced to see every day while we work with the Linux development. We want to be able to provide the world with free high class, high quality, highly innovative software products that really empower the users and offer the best and only real chance to narrow the digital divide. Please do not make this harder to us that it already is! In conclusion, we would recommend You to vote for such amendments that

#WWa: clarify limits of patentability so that computer programs, algorithms and business methods really cannot be patented as such;

#eoo: make sure that patents cannot be abused to avoid technical competition by preventing interoperability of competing products; and

#tWo: ensure that patents cannot be used to prevent publication of information.

#wne: To that end we would suggest following FFII's voting recommendations on this directive (see %(URL)).

#iel: Sincerely,

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: linus0309 ;
# txtlang: en ;
# multlin: t ;
# End: ;

