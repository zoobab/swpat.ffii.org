<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Linus Torvals & Alan Cox 2003/09/22: Lettre au Parlement Européen

#descr: explique les danger des brevets logiciels et les possibilités pour le Parlement  Européen de sauver l'Europe de ces dangers cette semaine, recommande que les membres du parlement européen suivent les recommandations de vote de la FFII.

#hft: Lettre ouverte à l'honorable Pat Cox, président du parlement Européen, et aux membres du parlement Européen :

#eMC: Cher M. Cox,

#cer: Nous avons suivi avec un intérêt croissant le fait que l'Europe allait étendre la brevetabilité aux programmes d'ordinateur. Aujourd'hui le Parlement européen va voter une directive qui pourrait mettre un arrêt à ce développement, ou le rendre plus difficile, selon la façon dont il est amendé par le Parlement.

#jdd: L'expérience des USA montre que, à la différence des brevets traditionnels, les brevets logiciels n'encouragent pas l'innovation et la R&D, c'est tout à fait le contraire. En particulier ils ont fait du mal aux petites et moyennes entreprises et généralement aux nouveaux venus sur le marché. Ils affaibliront juste le marché et augmenteront les dépenses à propos des brevets et des litiges, aux dépens de l'innovation et de la recherche technologique.

#stW: Particulièrement dangereuses sont les tentatives de détourner le système de brevet pour empêcher l'interopérabilité, afin d'éviter la compétition sur les capacités technologiques. Les normes ne devraient jamais être brevetables !  De même, les brevets ne devraient jamais être employés en tant que moyens pour empêcher la publication d'information - l'idée même des brevets est de fournir un monopole limité en temps en échange de la publication de l'invention.

#dhr: Les brevets logiciels sont également la plus grande menace face au développement de Linux et des autres logiciels libres, ce que nous sommes forcés de voir chaque jour tandis que nous travaillons au développement de Linux. Nous voulons pouvoir fournir au monde des logiciels libres high class [comment traduire ça ?], de haute qualité, fortement innovateurs qui donnent vraiment du pouvoir aux utilisateurs et offrent la meilleure et la seule vraie chance de réduire le fossé numérique. S'il vous plaît, ne nous rendez pas la tâche plus dure qu'elle ne l'est déjà ! En conclusion, nous vous recommanderions pour voter pour de tels amendements qui :

#WWa: clarifient les limites du brevetabilité de telle sorte que les programmes d'ordinateur, les algorithmes et les méthodes des affaires ne puissent vraiment pas être brevetés en tant que tels ;

#eoo: s'assurent que les brevets ne puissent pas être détournés dans le but d'éviter la concurrence technique en empêchant l'interopérabilité des produits concurrents ; et

#tWo: s'assurent que les brevets ne puissent pas être employés pour empêcher la publication d'information.

#wne: À cet effet nous suggérerions de suivre les recommandations de vote du FFII concernant cette directive (voir %(URL)).

#iel: Sincèrement,

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/eubsa.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: linus0309 ;
# txtlang: fr ;
# multlin: t ;
# End: ;

