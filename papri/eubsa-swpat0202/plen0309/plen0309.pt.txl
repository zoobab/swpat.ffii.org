<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Proposta para Directiva do Concelho e Parlamento Europeu sobre os limites da patenteabilidade no que diz respeito ao processamento automático de dados e os seus campos de aplicação

#descr: O Parlamento Europeu vai decidir sobre a Directiva de Patentes de Software a 24 de Setembro. A directiva tal commo proposta pela Comissão Europeia destroi a estrutura básica da lei actual (Art. 52º da Convenção Europeia de Patentes) e substitui-a pelo Standard Trilateral defindo pelos gabinetes de patentes dos EUA, Europa e Japão em 2000, de acordo com o qual todas as soluções para problemas %(q:implementadas num computador) são invenções patenteáveis. Alguns membros do Parlamento propuseram emendas cujo objectivo é manter o conceito mais restricto de invenção da Convenção Europeia de Patentes, enquanto que outros promovem a patenteabilidade ilimitada de acordo com o Standard Trilateral, embora vestindo as roupas da restrição retórica. Tentamos fazer uma análise comparativa de todas as emendas propostas, para ajudar quem pode decidir a reconhecer se estão a votar por limites falso ou reais sobre a patenteabilidade.

#Lgn: Legenda e Exemplo Comentado

#ril: Artigos e Moções

#eia: Rejeição/Aceitação, Título e Considerandos

#enm: Numero da emenda (para efeitos de referência neste documento, as emendas realmente tabeladas provavelmente terão uma numeração diferente)

#jac: pro, contra, indeciso

#tuW: 410 votaram a favor, 178 votaram contra

#TitTit: Título

#TitCec: Proposta para Directiva do Concelho e Parlamento Europeu sobre a patenteabilidade de invenções implementadas num computador

#trW: texto da %(op:proposta original da Comissão Europeia)

#TitSyn: O objectivo da directiva não pode ser declarar como ideias patenteáveis todos os géneros de %(q:invenções implementadas num computador). O objectivo é clarificar os limites da patenteabilidade no que diz respeito ao processamento automático de dados e os seus diversos campos (técnicos e não técnicos) de aplicação, e isto deve ser escrito no título de forma clara e sem ambiguidade.

#ung: Justificação da amostra/comentários

#enm2: Números das emendas

#uia: Subscritores

#amd: recomendação de voto

#ogs: resultado da votação

#Am29: Proposta para Directiva do Concelho e Parlamento Europeu sobre os limites da patenteabilidade no que diz respeito ao processamento automático de dados e os seus campos de aplicação

#eet: texto da emenda

#tIa: Patent Insurance Extensions

#Art1: This Directive lays down rules for the patentability of computer-implemented inventions.

#nWw: No sentido do Art. 52º.2 da Convenção Europeia de Patentes (CEP), as ideias implementadas em hardware genérico de computador não consideradas invenções. A expressão %(q:invenção implementada num computador) não é conhecida aos peritos na arte, e como as únicas coisas que podem ser implementadas num computador são programas de computador (uma máquina de lavar roupa ou um telefone celular não podem ser implementados num computador), sugere que os programas de computador podem ser invenções. Foi introduzida pelo Gabinete Europeu de Patentes em 2000 no infâme %(e6:Apêndice 6, no sítio www, do documento Trilateral) de forma a alinhar a lei de patentes Europeia com as leis dos EUA e Japão, permitindo patentes para %(q:planos de negócio implementados num computador), em antecipação do desaparecer do Art. 52º.2 da CEP.

#Am116: This directive lays down the rules concerning the limits of patentability and patent enforceability with respect to computer programs.

#Art2a: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more prima facie novel features which are realised wholly or partly by means of a computer program or computer programs;

#apt: O Artigo 52º da PEC afirma claramente que um programa de computador (um objecto de reivindicação constituído por equipamento de processamento de dados e regras para a sua operação) não pode ser uma invenção patenteável.

#fet: As emendas 36, 117 e 42 clarificam que uma inovação apenas é patenteável se estiver conforme o Art. 52 da PEC, independentemente de parte da sua implementação se tratar ou não de um programa de computador. A Emenda 14 permitiria que um programa de computador puro possa ser reivindicado como %(q:uma invenção cuja actuação envolve o uso de um computador e tendo todas as suas capacidades realizadas através de um programa de computador), contradizendo assim o Art. 52º da CEP.

#Am36: %(q:computer-implemented invention) means any invention in the sense of the European Patent Convention the performance of which involves the use of a computer, computer network or other programmable apparatus and having in its implementations one or more non-technical features which are realised wholly or partly by a computer program or computer programs, besides the technical features that any invention must contribute;

#Am14: %(q:computer-implemented invention) means any invention the performance of which involves the use of a computer, computer network or other programmable apparatus and having one or more features which are realised wholly or partly by means of a computer program or computer programs;

#Art2b: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art.

#Art2bSyn1: Uma directiva que torna a patenteabilidade dependente da expressão %(q:contributo técnico) tem que definir essa expressão de forma clara.

#Art2bSyn96: A Emenda 96 parece implicar que apenas o problema, mas não a solução, necessita ser técnico. A palavra %(q:significante) não tem nenhum sentido legal e por isso leva à confusão e não à clarificação. Evidentemente é necessário definir claramente o que significa %(q:técnico) para isto ter algum efeito útil. Exigir que um problema técnico seja resolvido é tão mau ou pior quanto a proposta da CEC. Não nos devemos focar na aplicação (o problema resolvido) mas na solução (o resultado de investigação empírica que possa merecer uma patente). Pode-se dizer de qualquer software que resolve um problema técnico, mas se a solução apenas inova no software, não pode ser considerada uma solução técnica (ou pelo menos não uma nova solução técnica).

#Art2bSyn69: Amendment 69 goes in the right direction by stating that %(q:processing, handling, and presentation of information do not belong to a technical field).  It risks mixing the %(q:technical contribution) (= invention) requirement with non-obviousness requirements, similar to 96.  However even though the wording is redundant, there is nothing wrong with saying that the contribution (invention) must be %(q:non-obvious).  This can even help to correct EPO misperceptions, according which %(q:the non-obviousness must contain a technical contribution).  Given that this amendment also defines what %(q:technical fields) (Art 27 TRIPs) are, it is extremely useful.

#Art2bSyn107: Por fim, a Emenda 107 acerta em cheio: afirma claramente que um %(q:contributo técnico) é um sinónimo para invenção e repete os requisitos de patententeabilidade do Art. 52º da CEP. Esta emenda não limita a patenteabilidade de forma alguma, apenas confirma o Art. 52º da CEP e remove qualquer confusão que possa derivar da mistura dos testes de patenteabilidade, que o texto original da CEC e outras emendas confunde.

#Am69: %(q:technical contribution) means a contribution to the state of the art in a technical field which is not obvious to a person skilled in the art. The use of natural forces to control physical effects beyond the digital representation of information belongs to a technical field. The processing, handling, and presentation of information do not belong to a technical field, even where technical devices are employed for such purposes.

#Am107: %(q:technical contribution), also called %(q:invention), means a contribution to the state of the art in technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application.

#Am96: %(q:technical contribution) means a contribution, involving an inventive step to a technical field which solves an existing technical problem or extends the state of the art in a significant way to a person skilled in the art.

#Art2baSyn: Since the rapporteur's attempt to %(q:make it clear what is patentable and what not) hinges entirely on the word %(q:technical), this term must be defined clearly and restrictively. The only definition which achieves this is the one along the lines of amendments 55=97=108, which is found in various national case laws and in some patent laws (e.g. Nordic Patent Treaty and patent laws of Poland, Japan, Taiwan et al). This definition is based on valid concepts of science and epistemology and has been proven to have a clear meaning in practise. It assures that broad, expensive and insecurity-fraught broad exclusion rights such as patents are used only in areas where there is an economic rationale for them, and that abstract-logical innovation is the domain of copyright and copyright-like sui generis rights only.  The word %(q:numerical) in Amendment 55 is apparently the result of mistranslation from French, should read %(q:digital). The rough definition given here corresponds to the worldwide common understanding of the term, implicit in current-day case law and in the JURI report other statements about what should be patentable (e.g. %(q:not software as such, but inventions related to mobile phones, engine control devices, household appliances, ...) ).  This understanding should be made explicit for the purpose of clarification.

#Am97: %(q:technical field) means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. %(q:Technical) means %(q:belonging to a technical field). The use of forces of nature to control physical effects beyond the digital representation of information belongs to a technical domain. The production, handling, processing, distribution and presentation of information do not belong to a technical field, even when technical devices are employed for such purposes.

#Am37: %(q:Technology) means %(q:applied natural science).  %(q:Technical) means %(q:concrete and physical).

#Am39Syn: Isto é a doutrina standard das patentes na maioria das juridisções.  As %(q:quatro forças da natureza) são um conceito reconhecido da epistemologia (teoria da ciência). Enquanto que a matemática é abstracta e não relacionada com as forças da natureza, alguns planos de negócio podem bem depender da química das células cerebrais do cliente, o que contudo não é controlável, i.e. não determinístico, sujeito ao livre arbítrio. Assim a expressão %(q:forças da natureza controláveis) exclui claramente o que precisa de ser excluído mas ainda assim providencia flexibilidade suficiente para inclusão de outros futuros campos de ciência natural aplicada para além das actualmente reconhecidas %(q:4 forças da natureza). De notar que esta emenda não torna não patenteáveis as invenções em aparelhos como telefones celulares ou máquinas de lavar roupa, apenas assegura que simplesmente acrescentar um programa de computador a um aparelho (técnico) conhecido não torna o programa de computador patentável. Inversamente, adicionar um programa de computador a uma invenção já patentável, não torna esta invenção não patenteável (pois a invenção continuará a resolver um problema utilizando forças controláveis da natureza, independentemente da presença de um programa de computador).

#Am39: %(q:invention) in the sense of patent law means %(q:solution of a problem by use of controllable forces of nature) ;

#Am38Syn: Não queremos que inovações na %(q:indústria da música) ou na %(q:indústria de serviços legais) atinjam os requisitos de %(q:aplicação industrial) do TRIPS. A palavra %(q:indústria) é frequentemente utilizada hoje em dia com significados extendidos que não são apropriados ao contexto das leis de patentes.

#Am38: %(q:industry) in the sense of patent law means %(q:automated production of material goods);

#Art3: Member States shall ensure that a computer-implemented invention is considered to belong to a field of technology.

#Art3Syn: O texto da Comissão Europeia diz que todas as ideias %(q:implementadas num computador) são invenções patenteáveis segundo o Art. 27º do TRIPs. A rejeição é uma coisa boa, mas uma clarificação de como o Art. 27º do TRIPs se aplica teria sido ainda melhor.

#Art3aSyn1: Um dos objectivos principais do projecto da directiva tem sido a clarificação da interpretação do Art. 52º da CEP à luz do Art. 27º do TRIPs, que diz que %(q:as invenções em todos os campos tecnológicos) têm de ser patenteáveis. Propomos traduzir a linguagem do Art. 52º.2 da CEP para a linguagem do Art. 27º do TRIPs, como o faz a Emenda CULT-16.

#Art3aSyn2: Se o processamento de dados é %(q:técnico), então qualquer coisa o é.

#Art3aSyn3: Data processing is a common basis of all fields of technology and non-technology. With the advent of the (universal) computer in the 1950s, automated data processing (ADP) became pervasive in society and industry.

#Art3aSyn4: Tal como escreve Gert Kolle, um reconhecido teórico  por detrás de decisões dos anos 1970s de excluir o software da patenteabilidade, em 1977 (ver Gert Kolle 1977: Technik, Datenverarbeitung und Patentrecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs):

#Art3aSynGK77: O Processamento Automático de Dados (ADP) tornou-se hoje uma indispensável ferramenta auxiliar em todos os domínios da sociedade humana e assim permanecerá no futuro. É ubíquo. ... O seu significado instrumental, as suas funções auxiliares e subordinadas distinguem o ADP dos ... campos tecnológicos individuais e associa-o a áreas tais como a administração de empresas, cujos métodos e resultados do trabalho ... são necessários a todas as empresas, e para os quais prevalece a necessidade de assegurar a sua livre disponibilidade, tal como indicado.

#Art3aSynCult: A CULT foi sensata ao votar por uma clara exclusão do processamento de dados do alcance do significado de %(q:tecnologia).

#Art3aSynStud: Diversos estudos, alguns deles conduzidos por instituições da UE,  tal como as opiniões do Comité Economico-Social Europeu e o Comité Europeu das Regiões, explicam detalhadamente porque é que a economia da Europa irá sofrer danos se as inovações no processamento de dados não forem excluidas da patenteabilidade.

#Am45: Member states shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not considered to be inventions in the sense of patent law.

#Art4N1: Member States shall ensure that a computer-implemented invention is patentable on the condition that it is susceptible of industrial application, is new, and involves an inventive step.

#Art4N2: Member States shall ensure that it is a condition of involving an inventive step that a computer-implemented invention must make a technical contribution.

#Art4N3: The technical contribution shall be assessed by consideration of the difference between the scope of the patent claim considered as a whole, elements of which may comprise both technical and non-technical features, and the state of the art.

#Art4Syn: Este artigo mistura o teste da não-obviosidade com o teste da invenção, enfranquecendo assim ambos e entrando em conflicto com o Art. 52º da CEP.

#cpW: JURI has split this article into parts 1, 2 and 3.  This is dangerous because it prevents certain amendments, which may apply to the whole article, from being put to vote.

#iaa: Split into parts 1 2 and 3.

#Art4N1Syn: Segundo o Art. 52º da CEP as %(q:invenções implementadas num computador) (i.e. ideias enquadradas em termos de equipamento genérico de processamento de dados = programas de computador) não são invenções. Ver também a análise ao artigo 1º. A Emenda 56=98=109 corrige este erro e reafirma o Art. 52º da CEP. Nenhuma destas emendas limita a patenteabilidade, mas pelos menos esta emenda define as coisas de uma forma formalmente correcta.

#Am16N1: In order to be patentable, a computer-implemented invention must be susceptible of industrial application and new and involve an inventive step.

#Am98: Member States shall ensure that patents are granted only for technical inventions which are new, non-obvious and susceptible of industrial application.

#Art41aSyn: Doctrine of the German Federal Patent Court's Error Search Decision of 2002, negates an EPO doctrine which makes most computerised business methods patentable.

#Am47: Member States shall ensure that computer implemented solutions to technical problems are not considered to be patentable inventions when they only improve efficiency in the use of resources within the data processing system.

#Art41bSyn: Uma boa reafirmação do Art. 52º da CEP. Como o objectivo desta directiva é harmonizar e clarificar, é de essencial importância reforçar a interpretação correcta da CEP, dado que é a base das leis de patentes na Europa.

#Am48: Member States shall ensure that it is a condition of constituting an invention in the sense of patent law that an innovation, regardless of whether it involves the use of a computer or not, must be of technical character.

#Art4N2Syn: O texto da Comissão desvia-se do Art. 52º da CEP. Como explicado na análise do Considerando 11º, é preciso definir primeiro se há uma invenção/contributo técnico (ou utilizando a definição negativa do Art. 52 da CEP: um programa de computador não é uma invenção, ou utilizando a definição positiva que emergiu da jurisprudência, baseada em forças controláveis da natureza). Depois, deve-se verificar se esta invenção passa os outros 3 testes do Art. 52º, dos quais o passo inventivo (= não-obviosidade) é um deles. O texto da Comissão reivindica que o teste do passo inventivo é um requisito para algo que pode ser uma invenção. Como um programa de computador pode não ser óbvio, poderia ser uma invenção de acordo com esta definição (ao contrário do que o Art. 52º da CEP diz).

#Art4N2SynP2: Para além disto, o texto da Comissão implica que as ideias enquadradas em termos de um computador de uso genérico (programas de computador) são invenções patenteáveis. Ser apagado, como propõe a emenda 82, poderia ser uma ajuda, porque o único efeito deste parágrafo é confundir os teste de patenteabilidade dessa forma impossibilitando a rejeição de candidaturas de patentes não-estutárias sem examinação substancial da parte dos gabinetes de patentes nacionais. A Emenda 16 comete o mesmo erro do texto da Comissão, apenas limpa um pouco melhor a linguagem. A Emenda 40 foi retratada, e agora é tabelada como uma inserção (83).

#Am16N2: In order to involve an inventive step, a computer-implemented invention must make a technical contribution.

#Art4N3Syn1: Quando se patenteia uma invenção, reivindica-se uma aplicação em particular dessa invenção. Esta aplicação é o que é descrito nas reivindicações. Como tal, as reivindicações como um todo contêm tanto a própria invenção de certa forma, tal como um número de capacidades não patenteáveis (tais como, por exemplo, um programa de computador) que são exigidas para a aplicação da invenção. Isto significa que mesmo que se exiga que a invenção seja técnica, as reivindicações podem conter capacidades não técnicas (ao qual não nos opomos, pois é assim que as patentes sempre funcionaram).

#Art4N3Syn2: Contudo, a proposta da Comissão Europeia nulifica o seu próprio conceito de %(q:contributo técnico) ao permitir que a contribuição possa consistir em capacidades não técnicas. A versão da JURI apenas afirma que as reivindicações devem conter capacidades técnicas, mas isto não é nenhum limite real. Um exemplo clarifica isto. Suponhamos que queremos patentear um programa de computador, então dizemos que isso é a %(q:contributo técnico). Nas reivindicações descrevemos a aplicação deste programa, então dizemos que patenteamos a execução deste programa num computador. Agora as reivindicações, como um todo, contêm características técnicas (o computador), e a diferença entre as reivindicações e o todo (computador, conhecido, + programa, novo) e o estado da arte (o computador conhecido) é o programa de computador. Assim um programa de computador pode ser um contributo técnico, de acordo com esta condição, o que é completamente contraditório (pois um programa de computador não pode ser técnico). É evidente que é necessária uma correcção tal como a que está na emenda 57=99=110, se o conceito de %(q:contributo técnico) for mesmo para ser utilizado.

#lWo: O período de graça da novidade proposto pela emenda 100 é uma questão independente. Como demonstrado por uma %(ng:consulta recente  conduzida pelo Gabinete de Patentes do Reino Unido), é muito controverso mesmo entre aqueles que supostamente beneficiariam dele. Não é claro se um período de novidade iria sequer, com osugerido pelo relatório da ITRE, beneficiar as PMEs. No caso do desenvolvimento do software livre, poderia ainda causar insegurança adicional acerca de se as ideias publicadas estão livres de patentes.

#Am16N3: The technical contribution shall be assessed by considering the state of the art and the scope of the patent claim considered as a whole, which must comprise technical features, irrespective whether or not such features are accompanied by non-technical features.

#Am99: The technical contribution shall be assessed by consideration of the difference between all of the the technical features of the patent claim and the state of the art.

#Am100P1: The significant extent of the technical contribution shall be assessed by consideration of the difference between the technical elements included in the scope of the patent claim considered as a whole and the state of the art. Elements disclosed by the applicant for a patent over a period of six months before the date of the application shall not be considered to be part of the state of the art when assessing that particular claim.

#Art43aSyn: Como mencionado na análise do artigo 1º, a expressão %(q:invenção implementada num computador) é contraditória. Como mencionado na análise do Considerando 11º, %(q:invenção) e %(q:contributo técnico) são sinónimos: o que foi inventado foi a sua contribuição (técnica) para o progresso. No calão utilizado pela Directiva, contudo, %(q:contributo técnico) apenas significa %(q:solução para um problema técnico na proximidade da mais próxima documentação de arte anterior a respeito da invenção reinvidicada). Neste contexto, é incerto que efeito terá qualquer teste, não importa quão restricto, poderá ter por cer incerto acerca do que se aplica.

#Art43aSynTest: Para além disto, dizendo que este teste %(q:deve ser aplicado), a provisão não exige claramente que o teste %(e:tem de ter sucesso), nem define o que entende por %(q:contributo técnico).

#Art43aSynInd: Por fim, não existe definição legal de %(q:aplicação industrial no sentido estrito da expressão, tanto em termos de método como de resultado).

#Art43aSynVal: Apesar desta falta de visão, recomendamos que seja suportada a Emenda 70. Pelo menos codifica aspectos centrais do conceito de %(q:invenção técnica). Assim, em combinação com outras emendas, poderia eventualmente contribuir para definir um limite claro à patenteabilidade.

#Am70: In determining whether a given computer-implemented invention makes a technical contribution, the following test shall be used: whether it constitutes a new teaching on cause-effect relations in the use of controllable forces of natures and has an industrial application in the strict sense of the expression, in terms of both method and result.

#Art4aSyn: A Emenda 17 assume que a %(q:a interacção física normal entre um programa e o computador) significa algo. Não significa. Um significado poderia ser atribuído, por exemplo, especificando %(q:aumento da eficiência do computador), %(q:poupanças na utilização da memória), etc %(q:que não constituem um contributo técnico), ou %(q:que estão no âmbito da normal interacção física entre um programa e o computador), emprestando da %(es:decisão recente %(tp|Pesquisa de Erro|Suche Fehlerhafter Zeichenketten)). Sem uma tal especificação, a Emenda 17 apenas adiciona obfuscação em vez de clarificação.

#Am17: %(al|Exclusions from patentability|A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus.  Accordingly, inventions involving computer programs which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.)

#Art4bSyn: Algune comentários à emenda acima.

#Am87: %(al|Exclusions from patentability|A computer-implemented invention shall not be regarded as making a technical contribution merely because it involves the use of a computer, network or other programmable apparatus. Accordingly, inventions merely involving computer programs (data processing) which implement business, mathematical or other methods and do not produce any technical effects beyond the normal physical interactions between a program and the computer, network or other programmable apparatus in which it is run shall not be patentable.)

#Art4cSyn1: A directiva tal como proposta pela CEC e pela JURI é baseada no Standard Trilateral dos Gabinetes de Patentes dos EUA, do Japão e da Europa de 2000, que consideram todos os programas de computador como invenções patenteáveis. Isto é exprimido na expressão %(q:invenção implementada num computador), que foi introduzido pelo Gabinete Europeu de Patentes de forma a suportar este standard. Já não há um teste para se saber se estamos perante uma invenção, mas apenas um teste à %(q:contributo técnico no passo inventivo), i.e. o passo entre a %(q:arte anterior mais próxima) e a %(q:invenção) têm de ser enquadrados nos termos de um %(q:problema técnico), por exemplo um problema de melhorar a velocidade ou a eficiência  no uso da memória do computador. Utilizando esta doutrina do EPO, qualquer algoritmo ou plano de negócio se torna efectivamente patenteável, como tem sido apontado pelo Tribunal Federal de Patentes alemão numa %(es:decisão recente).

#Art4cSyn2: As Emendas 47 e 60 claramente dizem ao EPO que deve seguir o Tribunal Federal de Patentes alemão de recusar-se a considerar melhorias na eficiência do processamento de dados um %(q:contributo técnico).

#Am60: Member States shall ensure that computer-implemented solutions to technical problems are not considered to be patentable inventions merely because they improve efficiency in the use of resources within the data processing system.

#Art4dSyn1: Esta emenda poderia por si mesma substituir muitas das provisões da directiva. As Recomendações do EPO de 1978 oferecem-nos uma interpretação clara do Art 52. Isto foi alterado, em resposta a pedidos da indústria, através de novas versões das Recomendações, com o custo de se ter tornado as regras contraditórias e se ter introduzido uma divisão. Os objectivos desta directiva deveriam a resolução dessa divisão e a concretização da aplicação do tratado TRIPs de 1994. O primeiro objectivo pode ser atingido por esta única emenda. Note-se que esta emenda não afirma que as Recomendações do EPO de 1978 devem ser tornadas lei, afirma meramante que elas providenciam uma interpretação correcta do Art 52 EPC.

#Am46: Member States shall ensure that patents on computerised innovations are upheld and enforced only if they were granted according to the rules of Article 52 of the European Patent Convention of 1973, as explained in the European Patent Office's Examination Guidelines of 1978.

#Art5: Member States shall ensure that a computer-implemented invention may be claimed as a product, that is as a programmed computer, a programmed computer network or other programmed apparatus, or as a process carried out by such a computer, computer network or apparatus through the execution of software.

#Art5Syn18: A emenda 18 propõe a introdução de reivindicações de programas, isto é reivindicações do tipo %(q:um programa que quando lido para a memória de um computador executa um qualquer processo.) Isto tornaria todas as publicações programas de computador em possíveis infractores directos de patentes, colocando por isso em risco programadores, investigadores e fornecedores de serviços de Internet. Sem as reivindicações de programas os fornecedores de serviços de Internet apenas seriam responsabilizados indirectamente, por exemplo através das garantias que oferecem aos seus clientes. Como tal, a questão das reivindicações dos programas parece ser de natureza mais simbólica do que material: o lobby das patentes quer que fique dito de forma inequívoca que o software como tal é patenteável. A CEC não chega a propor esta última consequência.

#Art5Syn58: No entanto, o texto da CEC ainda sugere que o computador de uso genérico e regras de cálculo conhecidos (= programas de computadores) constituem produtos e processos patenteáveis. A emenda 101/58 tenta resolver este problema. Pode ser uma boa ideia substituir %(q:processos técnicos de produção operados por computador) por %(q:os processos inventivos executados nesse tipo de equipamento) se isso for ainda possível na forma de uma %(q:emenda de compromisso). A emenda 102 define %(q:invenção implementada em computador) como sendo apenas um produto ou um processo técnico de produção. Desde que %(q:técnico) seja definido, isto pode ser útil. No entanto, um computador que executa um programa pode também ser considerado como um %(q:dispositivo programado), e patentear o uso de um programa de computador quando executado num computador tem o mesmo efeito que patentear o programa em si mesmo: não existe outra forma de utilizar um programa de computador senão executando-o num computador.

#Am18: A claim to a computer program, on its own, on a carrier or as a signal, shall be allowable only if such program would, when loaded or run on a computer, computer network or other programmable apparatus, implement a product or carry out a process patentable under Articles 4 and 4a.

#Am101: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is a set of equipment comprising both programmable apparatus and devices which use forces of nature in an inventive way, or as a technical production process operated by such a computer, computer network or apparatus through the execution of software.

#Am102: Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is as a programmed device, or as a technical production process.

#Art5aSyn: O software livre dá contribuições importantes para a inovação e para a difusão de conhecimento sem benificiar do sistema de patentes. Isto deveria ser reflectido na política de inovação e na legislação, não implicando que o software proprietário deva ser patenteável. Infelizmente esta emenda utiliza o termo %(q:invenção implementada em computador), que foi introduzido pelo EPO em 2000 numa tentativa de justificar a patenteabilidade de operações de processamento de dados de uso genérico (software). Isto deveria ser corrigido numa segunda leitura.

#Am62: %(al|Limitation of the effects of patents granted to computer-implemented inventions|%(linol|Member States shall ensure that the rights conferred by the patent shall not extend to the acts done to run, copy, distribute, study, change or improve a computer program which is distributed under a licence that provides for:|the freedom to run the program;|the freedom to study how the program works, and adapt it to the user's needs;|the freedom to redistribute copies under the same licence conditions;|the freedom to improve the program, and release improvements to the public under the same licence conditions;|free access to the source code of the program.))

#Art51a: This amendment apparently intends to exclude claims to ways of operating data processing hardware (= computer programs), but proposes inappropriate regulatory means.  Patent claims always comprise more than the invention (= technical contribution).  If there is a technical invention (e.g. a new chemical process), then there is no reason why the patent claim should not comprise this invention together with non-invention elements, such as a computer program that controls the reaction.  A claim does not describe an invention but the scope of an exclusion right which is legitimated by an invention.

#Am72: Die Mitgliedstaaten stellen sicher, dass auf computerimplementierte Erfindungen erteilte Patentansprüche nur den technischen Beitrag umfassen, der den Patentanspruch begründet. Ein Patentanspruch auf ein Computerprogramm, sei es auf das Programm allein oder auf ein auf einem Datenträger vorliegendes Programm, ist unzulässig.

#Am103Syn: Sem esta emenda, os possuidores de patentes de software poderiam ainda enviar cartas do tipo %(q:cesse e desista) a programadores ou distribuidores de software, acusando-os de infracção contributória. Este artigo garante que o direito de publicação, como garantido pelo Art 10 ECHR tem precedencia sobre as patentes. Por outro lado, esta emenda não evita que sejam deferidas patentes de software nem previne o seu uso contra os utilizadores finais do software. Vendedores de software proprietário e distribuidores comerciais de Linux poderiam ainda ficar sob pressão dos seus clientes, que normalmente exigiriam uma obrigação contratual que os isolasse dos problemas de patentes.

#Am103: Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.

#Am104N1Syn: O alcance da patente é definida normalmente pelas pretensões e se algo incide fora do alcance destas pretensões, isso não constitui uma infracção. Por este motivo a emenda, em primeira análise, parece tautológica. A intenção desta emenda poderá ser permitir a simulação de processos patenteáveis em sistemas normais de processamento de dados. Sendo assim, isso deveria ser afirmado claramente, como é feito em algumas das outras emendas.

#Am104N1: Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.

#Art5dSyn: Estas emendas, ao contrário do que alguns possam pensar, não servem para promover ou proteger o software livre e de código aberto, antes pretendem assegurar que a sua divulgação, inerente no sistema de patentes, é tomada em consideração e que o software, tal como outro objecto de informação, se encontra do lado de divulgação da patente ao invés do lado da exclusão e do monopólio. Isto torna um pouco mais difícil que outros se vejam impedidos de realizar aspectos não concretizados pelo programador mas que são obviamente possíveis uma vez que o modelo computacional está perfeitamente definido. Quando se publica código fonte, quem o publica oferece pelo menos conhecimento real sobre como resolver um problema, ao contrário do que se diz %(q:meios de processamento ligado a meios de entrada e saída de forma a calcular-se uma função tal que o resultado da função referida quando enviado através dos referidos meios de saída soluciona o problema que o utilizador pretendia resolver.)

#Am104N2: Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program is published as part of the patent description without any restricting licensing terms.

#Art6: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trademarks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#Art6Syn: A proposta da Comissão Europeia não protege a interoperabilidade mas, ao invés, assegura que software interoperável não pode ser publicado ou usado quando uma dada interface é patenteada. O único comportamento que a Comissão Europeia pretende permitir neste caso é a descompilação, que de qualquer modo não infingiria qualquer patente (uma vez que uma patente envolve o uso do que é descrito nas pretensões e não o estudo do objecto referido). Para além disso, as emendas 66 e 67 especificam as leis nas quais os privilégios de interoperabilidade podem ser baseados, tornando assim mais claro que a provisão não pode ter qualquer efeito.

#Am19: The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.

#Art6aSyn: As emendas 20 e 50 são de facto as únicas que formulam um efectivo privilégio de interoperabilidade. No entanto, foram %(rm:demonstrados) receios de que a expressão %(q:sistemas de computadores) possa ser interpretada estritamente no sentido de %(q:arquitecturas de hardware). Esta interpretação deverá se possível ser substituída por %(q:sistemas de processamento de dados). A emenda 50 faz isso mas dada a abundância de emendas que tentam destruir esta garantia de interoperabilidade esperamos que esta emenda possa ser retirada em favor da emenda 20, uma vez que esta já foi aceite em CULT, ITRE e JURI.

#Am76Syn: A emenda 76 retira toda e qualquer clareza no que diz respeito a quando é ou não permitido o uso de técnicas patenteadas para interoperação. Vai contra o objectivo principal da directiva, que é concretizar certas regras abstractas, incluindo as do Art 30 TRIP, e com isso atingir clareza e segurança legal.

#Am105Syn: É aprazível que a emenda 105 concorde connosco na afirmação de que uma %(q:invenção implementada em computador) não pode significar mais que software em execução num computador. No entanto, as excepções que menciona são estranhas. Em primeiro lugar, se a invenção depende da máquina, não há qualquer exigência de interoperabilidade. Se não há exigência de interoperabilidade, a emenda 20 não se pode aplicar. Reciprocamente, se existe um tal exigência, então a invenção patenteada não funcionará como uma máquina independente ou invenção técnica. A segunda afirmação é deveras perigosa, uma vez que espera que os tibunais decidam, falhando assim na obtenção do objectivo da directiva, a clareza. Como demonstram os casos anti-trust contra a Microsoft tanto nos EUA como na Europa, é extremamente difícil e moroso determinar se algum comportamento desrespeita a lei da competição actual. Isto retiraria o sentido da afirmação acerca da interoperabilidade e colocaria PME's numa desvantagem ainda maior, uma vez que as grandes companhias dispoem de incomparavelmente mais meios para usar em casos resolvidos em tribunais.

#Am50: Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion between the conventions used in two different data processing systems so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement..

#Am20: Member States shall ensure that wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or network so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement.

#Am76: Member States shall ensure that, wherever the use of a patented technique is needed for a significant purpose such as ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement, provided that it does not unreasonably conflict with a normal exploitation of the patent and does not unreasonably prejudice the legitimate interests of the patent owner, taking account of the legitimate interests of third parties.

#Am105: %(al|Use of patented technologies|Member States shall ensure that, wherever the use of a patented technique is needed for the sole purpose of ensuring conversion of the conventions used in two different computer systems or networks so as to allow communication and exchange of data content between them, such use is not considered to be a patent infringement, provided that:|%(ol|the invention embodying the computer-implemented software is not working as an independent machine or technical invention, and|the patent is not in conflict with European competition law.))

#Art6b: This amendment seems to create a new property right outside of the patent system.  It establishes a category called %(q:computer-implemented inventions), which belong to no %(q:field of technology).  This is an interesting approach, although 7 years is probably still too long and some further questions need to be clarified in order for this to be workable.

#Am106: %(al|Term of patent|The term of a patent granted for computer-implemented inventions pursuant to this Directive shall be 7 years as from the date of filing.)

#Artt7: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, including electronic commerce.

#Art7Syn: Este artigo não tem qualquer efeito regulador. Para além disso, na sua forma actual, concede à Unidade de Propriedade Industrial da Comissão Europeia (Mercado Interno) ainda mais fundos e oportunidades de produção de %(q:estudos) propagandísticos que não %(le:apreciam qualquer espécie de respeito na comunidade académica). Apesar do período de envio e apreciação de emendas a este artigo estar quase terminado, a matéria proposta nas emendas 91ff por Marianne Thyssen é sem qualquer dúvida mais merecedor de um estudo atento do que o que foi até agora proposto pelos CEC e JURI.

#Am21: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises, and electronic commerce.

#Am91: The Commission shall monitor the impact of patent protection for computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses. Special attention will be paid to the position of small and medium-sized enterprises and of electronic commerce and to the impact of dominant positions on the functioning of the market.

#Am71Syn: A emenda 71 apela à criação de um sistema segurador para a aquisição de patentes e litigação, apesar de não haver exemplo prático de um tal sistema. A sua possibilidade teórica tal como foi sugerida à DG MARKT num relatório de consultadoria é baseado em suposições erradas, como foi explicado na %(le:carta aberta ao Parlamento Europeu), redigida por um número de reconhecidos economistas.

#Am90Syn: A emenda 90 é parecida mas menos explícita.

#Art7PI: Both amendments 71 and 90 call for an insurance for promotion not of defence of software companies against patent aggression, but, on the contrary, of aggressive use of patents by specialised patent litigation SMEs such as Eolas and Allvoice against software companies.  The archetype for the %(q:Patent Defence) concept is the %(q:Patent Defence Union) created by the CEO of %(AV).  Allvoice is the %(q:ten-person company located in an employment blackspot in south-west England) which Arlene McCarthy praises in her JURI Draft Report.  Allvoice has hardly produced any software itself, certainly not speech recognition software, but it used two trivial and broad patents on user interfaces in order to extort money from real speech recognition software companies.  By promoting the Patent Defence scheme as set out in this amendment, MEPs should note that they would be promoting litigation instead of innovation.

#weW: Several EU studies on SMEs and software patents have found that there are systematic reasons why SMEs are not using the patent system.  These are unlikely to be overcome by any proselytising system, no matter how much public money is poured into it.

#Am90: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents.

#Am71P1: The Commission shall monitor the impact of computer-implemented inventions on innovation and competition, both within Europe and internationally, and on European businesses, especially small and medium-sized enterprises and the open source community, and electronic commerce.

#Am71P2: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents, in particular through the creation of a defence fund and the introduction of special rules on legal costs.

#Am71P3: It shall report on its findings to the European Parliament and the Council and present appropriate proposals for legislation without delay.

#rbr: The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on

#lrl: These provisions have no regulatory effect, and are mostly of minor importance.  The PPE-DE amendments (by M. Thyssen) do bring some improvement.  The interoperability privilege formulated by Art 6a is a new regulatory approach, which needs observation and reporting, also in respect to how Art 30ff TRIPs is to be concretised in European law.  These discussions have been missing so far.  Amendment 93 is better worded for this purpose than 89.  Amendment 94 somewhat clarifies the purpose of the reporting exercise, thus making the article clearer.

#cia: the impact on the conversion of the conventions used in two different computer systems to allow communication and exchange of data

#Am92: whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and

#nvy: whether the option outlined in the Directive concerning the use of a patented invention for the sole purpose of ensuring interoperability between two systems is adequate;

#neh: In this report the Commission shall justify why it believes an amendment of the Directive in question necessary or not and, if required, will list the points which it intends to propose an amendment to.

#Rej: Rejection/Acceptance

#app: approves the proposed directive

#dWt: It is possible to correct the directive using the tabled amendments.  If the right amendments are adopted, the directive could be beneficial for the EU. So adopting a thoroughly corrected directive would be better than rejection. On the other hand, amending a directive this thoroughly is a complex task, especially in plenary, and the European Parliament may not succeed in fixing the abundant assumptions and misconceptions introduced by the Commission. The directive went through a procedure that hasn't taken into account most interested parties, academic studies or evidences. Rejecting the text and asking the Commission to start over with due process would also be justified. In fact, the number of mistakes needing correction is so large, that relying on the results of plenary vote for a good directive is very risky. Given the risk to the progress, freedom and the EU-companies' ability to compete if the amending partially fails, rejection is more desirable.  This is legislation for the next 20 years.  Although it has already taken some time, we are still in an early process of understanding and not in a hurry.

#ute: reject the proposed directive

#Rec1Cec: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. effective and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Rec1Syn: A Comissão afirma grosseiramente, contra todas as evidências económicas, incluindo a %(bh:muito detalhada investigação experimental de Bessen e Hunt de 2003), que as patentes incentivam o investimento no desenvolvimento de software. Além disso, o termo %(q:invenção implementada em computador) é confuso. É melhor não fazer declarações a fazê-las erradamente.

#Am1: The realisation of the internal market implies the elimination of restrictions to free circulation and of distortions in competition, while creating an environment which is favourable to innovation and investment. In this context the protection of inventions by means of patents is an essential element for the success of the internal market. Effective, transparent and harmonised protection of computer-implemented inventions throughout the Member States is essential in order to maintain and encourage investment in this field.

#Rec5Cec: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer- implemented inventions and provide an incentive for investment and innovation.

#Rec5Syn1: Tanto a Comissão Europeia (CEC) como a Emenda 2 (JURI) afirmam, dogmaticamente, que as patentes de software estimulam a inovação. Seria mais apropriado anunciar um determinado objectivo da política, através do qual se poderia avaliar o regime de patenteabilidade proposto.

#Rec5Syn2: A Emenda 2 parece apelar ao facto de que a redacção de uma determinada lei da União Europeia, independentemente do conteúdo, por si já conduz a certeza legal, apenas porque o tribunal Luxemburguês pode interpretar esta lei. Esta afirmação não é apenas questionável, não faz sequer sentido na medida em que %(ol|a Patente Comunitária está a caminho|existem maneiras mais fáceis e sistemáticas de questionar o tribunal Luxemburguês do que fazer passar uma directiva para interpretar um aspecto da Convenção Europeira de Patentes)

#Am2: Therefore, the legal rules as interpreted by Member States' courts should be harmonised and the law governing the patentability of computer-implemented inventions should be made transparent. The resulting legal certainty should enable enterprises to derive the maximum advantage from patents for computer-implemented inventions and provide an incentive for investment and innovation.

#css: Legal certainty will also be secured by the fact that, in case of doubt as to the interpretation of this Directive, national courts may and national courts of last instance must seek a ruling from the Court of Justice.

#Rec5aSyn: Concordamos que as regras introduzidas no Artigo 52 da Convenção Europeia de Patentes foram moldadas para além do razoável pelo Gabinete Europeu de Patentes (EPO) e que o seu significado original, tal como foi descrito em 1978 nas directrizes de avaliação de patentes do Gabinete, deveria ser reafirmado.

#Am88: The rules pursuant to Article 52 of the European Patent Convention concerning the limits to patentability should be confirmed and clarified. The consequent legal certainty should help to foster a climate conducive to investment and innovation in the field of software.

#Rec6Cec: The Community and its Member States are bound by the Agreement on trade-related aspects of intellectual property rights (TRIPS), approved by Council Decision 94/800/EC of 22 December 1994 concerning the conclusion on behalf of the European Community, as regards matters within its competence, of the agreements reached in the Uruguay Round multilateral negotiations (1986-1994). Article 27(1) of TRIPS provides that patents shall be available for any inventions, whether products or processes, in all fields of technology, provided that they are new, involve an inventive step and are capable of industrial application. Moreover, according to TRIPS, patent rights should be available and patent rights enjoyable without discrimination as to the field of technology. These principles should accordingly apply to computer-implemented inventions.

#Rec6Syn: Achamos necessário clarificar que há limites no que se pode colocar dentro do termo %(q:ramos de tecnologia) segundo o Artigo 27 TRIPS e que este artigo não está concebido para atribuir patenteabilidade ilimitada, antes evitar conflitos no mercado livre, que podem ser causados por excepções indevidas bem como por extensões indevidas à patenteabilidade. Esta interpretação de TRIPS é confirmada indirectamente pela recente pressão exercida pelo governo dos EUA contra o Artigo 27 TRIPs na base de que ele pode excluir (ou pode ser concebido para excluir) patentes de métodos de software e negócio, que o governo americano pretende atribuir segundo a nova proposta do Substantive Patent Law Treaty, ver %(bh:artigo de Brian Kahin em First Monday). A Emenda 31 remove o artigo de facto incorrecto.

#Am31: Deletion is good, but clarification would have been better.

#Rec7Cec: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception, however, applies and is justified only to the extent that a patent application or patent relates to such subject-matter or activities as such, because the said subject-matter and activities as such do not belong to a field of technology.

#Rec7Syn: O Artigo 52 da Convenção Europeia de Patentes afirma que programas para computadores etc não são %(e:invenções) no contexto da lei de patentes, isto é, que o sistema constituído por hardware genérico para computação e a combionação de regras de cálculo e execução nesse hardware não pode constituir uma patente. Não afirma que tais sistemas possam ser patenteados caso sejam declarados %(q:de outra forma) ou %(q:técnicos). A Emenda 5 resolve este problema reafirmando o Artigo 52 da Convenção Europeia de Patentes (como foi sugerido na Emenda 88 ao Relato 5 (a) (novo) acima). Note ainda que a exclusão de programas para computadores não é uma excepção, é antes uma parte da regulamentação que define o que é de facto uma %(q:invenção).

#Am32: Under the Convention on the Grant of European Patents signed in Munich on 5 October 1973 and the patent laws of the Member States, programs for computers together with discoveries, scientific theories, mathematical methods, aesthetic creations, schemes, rules and methods for performing mental acts, playing games or doing business, and presentations of information are expressly not regarded as inventions and are therefore excluded from patentability. This exception applies because the said subject-matter and activities do not belong to a field of technology.

#Am3: The aim of this Directive is not to amend the European Patent Convention, but to prevent different interpretations of its provisions.

#Rec7bSyn: Nova resolução apelando a maior responsabilização por parte do Gabinete Europeu de Patentes. O efeito seria mais forte se estivesse no formato de artigo, mas ainda assim representa um bom princípio. É importante que todas as fontes de conflito sejam referidas de modo a facilitar a sua resolução.

#Am95: Parliament has repeatedly asked the European Patent Office to review its operating rules and for the Office to be publicly accountable in the exercise of its functions. In this connection it would be particularly desirable to reconsider the practice in which the Office sees fit to obtain payment for the patents that it grants, as this practice harms the public nature of the institution.  In its resolution1 on the decision by the European Patent Office with regard to patent No EP 695 351 granted on 8 December 1999, Parliament requested a review of the OfficeÕs operating rules to ensure that it was publicly accountable in the exercise of its functions.

#cdn: Software patents would be bad for all software developers who aren't backed by a big company with a lot of money and lawyers, and this obviously includes a number of Free Software developers.

#Am61: Free software is providing a highly valuable and socially useful way of building common and shared innovation and knowledge diffusion.

#Rec11: Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they should make a technical contribution to the state of the art.

#nry: The four patentibility tests laid out in Art 52 EPC state that there must be a (technical) invention and that additionally, this invention must be susceptible of industrial application, new and involve an inventive step. The %(q:inventive step) conditions is defined in Art 56 EPC as requiring the invention not to be an obvious combination of known techniques to a person skilled in the art. Additionally, %(q:invention) and %(q:technical contribution) are synonyms: you can only invent new things, and this invention is your (technical) contribution to the state of the art.  Therefore, the CEC original and amendments 4 and 84 are in direct contradiction with the EPC. Additionally, such a logic assures that patentability is simply a question of claim wording, see %(se:Four Separate Tests on One Unified Object).  In order to avoid confusion and conflicts with EPC Art 52 and 56, the recital needs to be rewritten or deleted.

#Am51: While computer programs are abstract and do not belong to any particular field, they are used to describe and control processes in all fields of applied natural and social science.

#Am4: In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, computer-implemented inventions should make a technical contribution to the state of the art.

#Am84: In order to be patentable, inventions in general and computer-implemented inventions in particular must be susceptible of industrial application, new and involve an inventive step. In order to involve an inventive step, they must in addition make a new technical contribution to the state of the art, in order to distinguish them from pure software.

#Rec11Syn: O texto da comissão declara que os programas de computador constituem invenções técnicas. Remove o requisito independente de invenção (= %(q:contributo técnico)) e funde-o com o requisito de não-obviosidade (= %q(passo inventivo), ver os comentários ao Considerando 11). Isto leva a inconsistências teóricas e a consequências práticas indesejáveis.

#Rec11bSyn: A emenda 73 seria uma reescrição perfeita do Art 52 EPC se tivesse dito %(q:invenções) em vez de %(q:invenções implementadas em computador). Como está presentemente, parece uma regra especial para invenções que involvam computadores. É no entanto útil porque reforça a noção de que a mesma invenção deve passar quatro testes. Ver no Art 1 os motivos porque o termo %(q:invenção implementada em computador) é enganador.

#Am73: Computer-implemented inventions are only patentable if they may be considered to belong to a field of technology and, in addition, are new, involve an inventive step and are susceptible of industrial application..

#Rec12: Accordingly, where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable.

#Rec12Syn: O texto da Comissão Europeia funde o teste de %(q:invenção técnica) com o de %(q:passo inventivo), enfraquecendo por isso ambos os teste e abrindo um espaço infinito a interpretações. Isto, contradiz o Art 52 EPC, é teóricamente inconsistente, e leva a consequências práticas indesejáveis, tais como tornar irrealizáveis os exame de patentes em alguns institutos nacionais de patentes. Novamente, ver a explicação no Considerando 11 para mais informações. A emenda 5 da JURI torna as coisas ainda piores: em primeiro lugar, literalmente re-introduz o Art 3 (que ela própria eliminou), em segundo lugar afirma que o requisito de %(q:passo inventivo) é algo completamente diferente do que o que é dito no Art 56 EPC, em terceiro lugar começa a falar em %(q:problema técnico) que deveria ser resolvido (enquanto que a patenteabilidade não tem nada a ver com o tipo de problema que é resolvido mas sim com a solução empregue para o resolver) e finalmente também repete os erros do texto CEC original.

#Am5: Accordingly, even though a computer-implemented invention belongs by virtue of its very nature to a field of technology, it is important to make it clear that where an invention does not make a technical contribution to the state of the art, as would be the case, for example, where its specific contribution lacks a technical character, the invention will lack an inventive step and thus will not be patentable. When assessing whether an inventive step is involved, it is usual to apply the problem and solution approach in order to establish that there is a technical problem to be solved. If no technical problem is present, then the invention cannot be considered to make a technical contribution to the state of the art.

#Am114: Accordingly, an innovation that does not make a technical contribution to the state of the art is not an invention in the sense of patent law.

#Rec13: A defined procedure or sequence of actions when performed in the context of an apparatus such as a computer may make a technical contribution to the state of the art and thereby constitute a patentable invention. However, an algorithm which is defined without reference to a physical environment is inherently non-technical and cannot therefore constitute a patentable invention.

#Rec13Syn: Colocado no contexto da examinação prática de patentes, esta afirmação não é o que parece ser. Alarga o âmbito de patenteabilidade, mesmo comparada com a prática corrente do EPO, ao permitir a patenteabilidade de soluções não técnicas para problemas técnicos. Mais ainda, torna todos os algoritmos patenteáveis em todas as situações, porque um %(q:problema técnico) pode sempre ser definido em termos do computador de uso genérico, ao mesmo tempo que providencia uma forma de reivindicar algoritmos na sua forma mais abstracta.

#Rec13aSyn: A ambiguidade da síntaxe desta emenda deixa pouco claro se %(q:métodos de negócio) em geral são considerados %(q:métodos em que a única contribuição para o estado da arte é não técnica) ou se certos métodos de negócio podem conter %(q:contributos técnicos). Mais importante ainda é o facto de que esta emenda não vai na realidade excluir nada, porque os advogados de patentes serão rápidos a afirmar que alguns pormenores relacionados com computadores são característicos da %(q:invenção) que, como é dito noutra parte da proposta de directiva, tem que ser %(q:considerada como um todo). Para isto ser de alguma utilidade, a ambiguidade da síntaxe em torno de %(q:outro método) teria que ser removida e a categoria de %(q:métodos não técnicos) teria que ser explicada por definições e/ou exemplos.

#Am6: However, the mere implementation of an otherwise unpatentable method on an apparatus such as a computer is not in itself sufficient to warrant a finding that a technical contribution is present. Accordingly, a computer-implemented business method or other method in which the only contribution to the state of the art is non-technical cannot constitute a patentable invention.

#Rec13bSyn: Isto enuncia uma posição do senso comum que tem muitas vezes sido ignorada por forma a extender a patenteabilidade. Só por si não consegue alcançar muito e a afirmação na legislação de que a lei %(q:não pode ser deturpada) é um desejo vão. No entanto, um desejo é melhor do que nada.

#Am7: If the contribution to the state of the art relates solely to unpatentable matter, there can be no patentable invention irrespective of how the matter is presented in the claims. For example, the requirement for technical contribution cannot be circumvented merely by specifying technical means in the patent claims.

#Am8: Furthermore, an algorithm is inherently non-technical and therefore cannot constitute a technical invention. Nonetheless, a method involving the use of an algorithm might be patentable provided that the method is used to solve a technical problem. However, any patent granted for such a method would not monopolise the algorithm itself or its use in contexts not foreseen in the patent.

#Rec13dSyn1: A emenda apenas reafirma a definição de reivindicações: as reivindicações definem o direito de exclusão concedido pela patente. Permite reivindicações de software ao meramente se referir a equipamento computacional de uso genérico ou a processos de computador, independentemente de onde se encontra a inovação. É semelhante a dizer: podes reivindicar seja o que for desde que utilizes vocabulário como armazenamento de dados, processador ou aparelho algures nas reivindicações, mas tem o cuidado de tornares as reivindicações tão abrangentes quanto queiras porque não terás o monopólio daquilo que não reivindicares.

#Rec13dSyn2: A emenda pode ter um benificio indirecto: parece contradizer a emenda 18 ao artigo 5 ao dizer que só aparelhos programados e processos podem ser reivindicados.

#Am9: The scope of the exclusive rights conferred by any patent are defined by the claims.  Computer-implemented inventions must be claimed with reference to either a product such as a programmed apparatus, or to a process carried out in such an apparatus. Accordingly, where individual elements of software are used in contexts which do not involve the realisation of any validly claimed product or process, such use will not constitute patent infringement.

#Rec14: The legal protection of computer-implemented inventions should not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law should remain the essential basis for the legal protection of computer-implemented inventions as adapted or added to in certain specific respects as set out in this Directive.

#Rec14Syn1: O Considerando CEC não é demasiado mau (embora utilize o termo %(q:invenção implementada em computador), ver o artigo 1). Simplesmente diz que a lei de patentes não deve ser substituida. A emenda 10 é menos clara. Por um lado equaciona a %(q:posição da legislação actual) (?) com %(q:práticas do EPO). Por outro lado diz que a patenteabilidade dos métodos de negócio deve ser evitada. No entanto o próprio EPO tem vindo a deferir muitas patentes para métodos de negócio e tem facultado razões legais para apoiar as patntes a métodos de negócio. O único efeito possível da emenda, se é que tem algum, será atribuir responsabilidade legislativa ao EPO, cuja prática iria parecer definir a patenteabilidade, ao invés se cingir à legislação votada pelo Parlamento Europeu e outros organismos com representatividade democrática.

#Am86Syn: A emenda 84 não pede a codificação da lei de acordo com a prática corrente do EPO, mas por outro lado fala de %(q:métodos não patenteáveis, tais como procedimentos triviais e métodos de negócio). Não só os métodos de negócio triviais devem permanecer não patenteáveis, mas sim todos os métodos de negócio (senão haveria um desvio em relação ao Art 52 EPC, que deve ser evitado como foi sugerido na emenda 88).

#Am10: The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position having regard to the practices of the European Patent Office with a view to securing legal certainty, transparency, and clarity in the law and avoiding any drift towards the patentability of unpatentable methods, such as business methods.

#Am86: The legal protection of computer-implemented inventions does not necessitate the creation of a separate body of law in place of the rules of national patent law. The rules of national patent law remain the essential basis for the legal protection of computer-implemented inventions. This Directive simply clarifies the present legal position with a view to securing legal certainty, transparency, and clarity of the law and avoiding any drift towards the patentability of unpatentable methods such as trivial procedures and business methods.

#Rec16: The competitive position of European industry in relation to its major trading partners would be improved if the current differences in the legal protection of computer-implemented inventions were eliminated and the legal situation was transparent.

#Rec16Syn: A unificação da prática legislativa em si mesma não é uma garantia de melhoria da situação da indústria Europeia. Esta directiva não deveria utilizar o pretexto da %(q:harmonização) para alterar as regras do Art 52 EPC, que estão já de facto em vigor em todos os países. O facto de as patentes de software serem ou não boas para as empresas Europeias que desenvolvem software é independente do facto de a indústria de manufactura tradicional se estar a deslocar para as economias de baixo custo fora da UE. A alteração 11 e o texto CEC assume que extender a protecção de patentes à indústria de desenvolvimento de software irá melhorar a competitividade das empresas da UE, embora 75% das 30.000 patentes de software já concedidas (que CEC/JURI denominam patentes de %(q:invenções implementadas em computador)) estão nas mãos de empresas Americanas ou Japonesas. Adicionalmente, se a Europa não tem patentes de software, as empresas Europeias podem perfeitamente obtê-las nos EUA ou em outras regiões e levá-las a efeito nesses locais, enquanto as empresas estrangeiras afectadas não podem fazer o mesmo aí. Nesse sentido, não haver patentes de software na Europa é uma vantagem competitiva.

#Am11: The competitive position of European industry in relation to its major trading partners will be improved if the current differences in the legal protection of computer-implemented inventions are eliminated and the legal situation is transparent. With the present trend for traditional manufacturing industry to shift their operations to low-cost economies outside the European Union, the importance of intellectual property protection and in particular patent protection is self-evident.

#Am35: The competitive position of European industry in relation to its major trading partners could be improved if the current schism in judicial practice concerning the limits of patentability with regard to computer programs was eliminated.

#Rec17: Diese Richtlinie berührt nicht die Wettbewerbsvorschriften, insbesondere Artikel 81 und 82 EG-Vertrag.

#Rec17Syn: Software patents pose their own competition problems.  It would have been appropriate to solve these within the directive and to regulate how Art 30-31 TRIPs apply, e.g. by a recital which supports Art 6a.  It is not enough to rely on already-existing competition law.  Thus, it would have been more appropriate to delete this recital or to replace it by something that addresses the problems which the legislator is facing.  If this amendment is supported, it might be construed as a sign of approval by the EP for the European Commission's failure to address competition problems within this directive.

#Am12: This Directive should be without prejudice to the application of the competition rules, in particular Articles 81 and 82 of the Treaty.

#Rec18: Acts permitted under Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular provisions thereof relating to decompilation and interoperability, or the provisions concerning semiconductor topographies or trade marks, shall not be affected through the protection granted by patents for inventions within the scope of this Directive.

#Rec18Syn: A proposta CEC apenas finge proteger a inter-operacionalidade mas na realidade assegura que os direitos de patentes não podem ser limitados de qualquer forma por preocupações de inter-operacionalidade. Ver a explicação do Art 6 para isto. A emenda 13 só restringe ainda mais os meios disponíveis para limitar o direito de patentes aos especificados num conjunto específico de legislação.

#Am13: The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability. In particular, acts which, under Articles 5 and 6 of Directive 91/250/EEC, do not require authorisation of the rightholder with respect to the rightholder's copyrights in or pertaining to a computer program, and which, but for Articles 5 or 6 of Directive 91/250/EEC, would require such authorisation, shall not require authorisation of the rightholder with respect to the rightholder's patent rights in or pertaining to the computer program.

#Rec18bSyn: Este Considerando sugere que programas de computador podem constituir uma invenção (uma vez que não existem outras invenções que tenham código fonte ou possam ser des-compiladas), o que contradiz o Art 52 EPC que explicitamente exclui a patenteabilidade de programas de computador. O software nunca pode ser parte de uma invenção. Pode ser mencionado nas reivindicações da patente mas nesse caso só para clarificar a aplicação da verdadeira invenção e para limitar a abrangência do monopólio que a patente atribui a quem a possui (isto é, se alguém conseguir fazer uso da invenção sem utilizar um programa de computador, não irá infringir uma patente que só descreve o uso dessa invenção em combinação com um programa de computador).

#Am74: The application of this directive must not deviate from the original foundations of patent law, which means that the applicant of the patent must provide a description of all elements of the invention, including the source code, and that research into it, and as such decompilation, must be made possible. This way of working is indispensable to make compulsory licensing possible, for example if the obligation to supply the market is not fulfilled.

#Rec18tSyn: Este Considerando também sugere que os programas de computador podem ser invenções.

#Am75: De qualquer forma, a lei de todos os estados membros deve assegurar que as patentes contenham novidades e um passo inventivo, para previnir que invencoes que são já do domínio público sejam re-apropriadas, simplesmente porque pertencem a um programa de computador.

#pmd: Europarl 2003/09: Amendments

#eus: Here the amendments for the plenary decision on the software patent directive are published in various languages.

#ata: Many of the participants, including those who are supposed to benefit from the novelty grace period, express doubts about this double-edged concept.

#enW: Another version of the publishable amendments, sometimes slightly ahead of this page.

#soW: Results of the Vote

#Wan: Public Documents Regarding the Plenary Vote

#aaW: Amendments tabled by various groups and analyses thereof, as far as they can be published.

#eus2: Create a multilingual amendments database

#lrl2: One relational database table is enough, consisting of three fields: amendment number, language symbol and text.

#WnW: More tables can be added later.

#WWi: Help make this page multilingual

#uhn: It is very much valued by many MEPs if they can read this analysis in their language.  This page can also be useful as a reference for the Council and for the second reading.

#diW: This page is created using a multilinguality managment system, which makes it convenient for you to contribute, if you observe a few rules.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plen0309 ;
# txtlang: pt ;
# multlin: t ;
# End: ;

