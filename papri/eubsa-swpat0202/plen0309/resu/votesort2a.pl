$mep=0;
$groups=0;
$targets=0;
print "<table border=1>";

while (<>) {


      if (! (/:/)) {
      		$group = $_;
      		if ($mep > 0) {&print_group};
      		print "<tr><td colwidth=21> $group </td></tr>\n";
      		next;
      }
      
      $mep++;
      chomp;
      s/^[\s\d\.]* : //;
      $line{$mep} = $_;
      @f = split(':');
      $score{$mep}=0;
	$wscore{$mep}=0;
	$total{$mep}=0;
      if ($targets < 1) {
	      for ($vote=1;$vote<=20;$vote++) {
		   $ballot = $f[2*$vote];
		   $bval =0;
		   if ($ballot eq '+') {$bval=0.5};
		   if ($ballot eq '-') {$bval = -0.5};

		   #print "  $ballot  :  $bval \n";
		   $friend_vote{$vote}=$bval;
	      }
	      $score{$mep}=0;
	      $targets=1;
      } elsif ($targets < 2) {
	      for ($vote=1;$vote<=20;$vote++) {
		   $ballot = $f[2*$vote];
		   $bval =0;
		   if ($ballot eq '+') {$bval=0.5};
		   if ($ballot eq '-') {$bval = -0.5};

		   #print "  $ballot  :  $bval \n";
		   $weight{$vote} = abs($bval - $friend_vote{$vote});
	           $score{$mep} += abs($bval - $friend_vote{$vote});
	           $wscore{$mep} += 2 * $weight{$vote} * abs($bval - $friend_vote{$vote});
	           $total{$mep} += abs($bval);
	      }
	      $targets=2;
      } else {
	      for ($vote=1;$vote<=20;$vote++) {
		   $ballot = $f[2*$vote];
		   $bval =0;
		   if ($ballot eq '+') {$bval=0.5};
		   if ($ballot eq '-') {$bval = -0.5};

	           $score{$mep} += abs($bval - $friend_vote{$vote});
		   $wscore{$mep} += 2 * $weight{$vote} * abs($bval - $friend_vote{$vote});
	           $total{$mep} += abs($bval);
		   #print "  $ballot  :  $bval  :  $score{$mep}\n";
	     }
      }
}      			
if ($mep > 0) {&print_group};
      
      	   
sub by_score {
	($wscore{$b} <=> $wscore{$a})  
	||  ($score{$b} <=> $score{$a})  
	||  ($total{$b} <=> $total{$a})  
	||  ($line{$a} cmp $line{$b});
}

sub text_print_group {
	foreach $i (sort by_score (1..$mep)) {
		printf('%4.1f',$wscore{$i});
		print " : $line{$i}\n";
	}
	$mep = 0;
}
      	   

sub print_group {
	foreach $i (sort by_score (1..$mep)) {
      		print "<tr><td>";
		printf('%4.1f',$wscore{$i});
      		print "</td>";
      		@f = split(/:/,$line{$i});
      		$name = shift(@f);
  		print "<td>" .$name. "</td>";
	      	for ($vote=1;$vote<=20;$vote++) {
		   	$ballot = $f[2*$vote-1];
		   	$bval =0;
		   	if ($ballot eq '+') {$bval=0.5};
		   	if ($ballot eq '-') {$bval = -0.5};

		    	$diff = abs($bval - $friend_vote{$vote});
		        if (($targets < 2) || $weight{$vote}) {
		        	if ($diff == 1.0)  {$col = '#ff0000'};
		        	if ($diff == 0.5)  {$col = '#ffff00'};
		        	if ($diff == 0.0)  {$col = '#00ff00'};
		        } else {
		        	if ($diff == 1.0)  {$col = '#bb4444'};
		        	if ($diff == 0.5)  {$col = '#bbbb44'};
		        	if ($diff == 0.0)  {$col = '#44bb44'};
		        }
		        if ($ballot eq " ") {
    				print "<td bgcolor=$col>$ballot</td>";
    			} else {
    				print "<td bgcolor=$col>$vote:$ballot</td>";
    			}
	      	}
      		print "</tr>\n";
      	}
      	$mep=0;
}
     
      

