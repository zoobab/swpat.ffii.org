<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
<head>
<meta name="author" content="Workgroup">
<link href="/favicon.ico" rel="shortcut icon">
<meta name="review" content="2003/09/29">
<meta name="generator" content="a2e Multilingual Hypertext System">
<meta http-equiv="Content-Language" content="es">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="title" content="Europarl 2003-09-24: Amended Software Patent Directive">
<meta name="description" content="Consolidated version of the amended directive &quot;on the patentability of computer-implemented inventions&quot; for which the European Parliament voted on 2003-09-24.">
<title>Europarl 2003-09-24: Amended Software Patent Directive</title>
<style type="text/css">
<!--
H1,H2,H3,H4,H5,H6 {font-family:verdana,helvetica,arial,sans-serif}
A {font-family:verdana,helvetica,arial,sans-serif}
A:link {text-decoration:none;font-weight:bold}
A:visited{text-decoration:none;font-weight:bold}
A:active {text-decoration:none;font-weight:bold}-->
</style>
</head>
<body bgcolor="#F4FEF8" link="#0000AA" vlink="#0000AA" alink="#0000AA" text="#004010"><a href="index.en.html" charset=utf-8><img src="/img/flags/en.png" alt="[EN English]" border="1" height=25></a>
<a href="index.fr.html" charset=utf-8><img src="/img/flags/fr.png" alt="[FR Francais]" border="1" height=25></a>
<a href="index.de.html" charset=utf-8><img src="/img/flags/de.png" alt="[DE Deutsch]" border="1" height=25></a>
<a href="index.it.html" charset=utf-8><img src="/img/flags/it.png" alt="[IT Italiano]" border="1" height=25></a>
<a href="plenresu0309.es.txt"><img src="/img/icons.other/txt.png" alt="[translatable text]" border="1" height=25></a>
<a href="/girzu/gunka/index.de.html"><img src="/img/icons.other/questionmark.png" alt="[howto help]" border="1" height=25></a>
<a href="plenresu0309.es.pdf"><img src="/img/icons.other/pdficon.png" alt="[printable version]" border="1" height=25></a>
<a href="http://kwiki.ffii.org/Plenresu0309Es"><img src="/img/icons.other/groupedit.png" alt="[Readers' Comments]" border="0"></a>
<table width="100%">
<tr><th align=center bgcolor="#A0F3B9"><a href="../index.es.html">Europarl 2003/09</a></th><td align=center bgcolor="#A0F3B9"><a href="../kond/index.en.html">Checklist</a></td><td align=center bgcolor="#A0F3B9"><a href="../vote/index.en.html">Votes</a></td><td align=center bgcolor="#A0F3B9"><a href="../deba/index.en.html">Debate 03/09/23</a></td><td align=center bgcolor="#004010"><b><font color="#A0F3B9">result</font></b></td></tr>
</table>
<div align="center"><h1><a name="top">Europarl 2003-09-24: Amended Software Patent Directive</a></h1></div>
<p>
<cite>Consolidated version of the amended directive &quot;on the patentability of computer-implemented inventions&quot; for which the European Parliament voted on 2003-09-24.</cite>
<p>
<ol type="1"><li><a href="#text">The Text</a></li>
<li><a href="#links">Annotated Links</a></li></ol>

<a name="text"><h2>1. The Text</h2></a>
<p>
Directive on the patentability of computer-implemented inventions
<p>
<ol type="1"><li><a href="#art1">Article 1: Purpose</a></li>
<li><a href="#art2">Article 2: Definitions</a></li>
<li><a href="#art3a">Article 3a: Fields of Technology</a></li>
<li><a href="#art4">Article 4: Rules of Patentability</a></li>
<li><a href="#art4a">Article 4a: Causas de exclusi&oacute;n de la patentabilidad</a></li>
<li><a href="#art5">Article 5: Form of Claims; and further provisions</a></li>
<li><a href="#art6">Article 6: Interoperability</a></li></ol>

<a name="art1"><h3>1.1. Article 1: Purpose</h3></a>
<p>
This Directive lays down rules for the patentability of computer-implemented inventions.
<p>
<a name="art2"><h3>1.2. Article 2: Definitions</h3></a>
<p>
2a. &quot;invenci&oacute;n implementada en ordenador&quot;, toda invenci&oacute;n en el sentido del Convenio sobre la Patente Europea para cuya ejecuci&oacute;n se requiera la utilizaci&oacute;n de un ordenador, una red inform&aacute;tica u otro aparato programable y que tenga en sus aplicaciones una o m&aacute;s caracter&iacute;sticas no t&eacute;cnicas que se realicen total o parcialmente mediante un programa o programas de ordenador, sin perjuicio de las caracter&iacute;sticas t&eacute;cnicas que debe presentar toda invenci&oacute;n;
<p>
2b. &quot;technical contribution&quot;, also called &quot;invention&quot;, means a contribution to the state of the art in technical field. The technical character of the contribution is one of the four requirements for patentability. Additionally, to deserve a patent, the technical contribution has to be new, non-obvious, and susceptible of industrial application.
<p>
2c. &quot;technical field&quot; means an industrial application domain requiring the use of controllable forces of nature to achieve predictable results. &quot;Technical&quot; means &quot;belonging to a technical field&quot;. The use of forces of nature to control physical effects beyond the digital representation of information belongs to a technical domain. The production, handling, processing, distribution and presentation of information do not belong to a technical field, even when technical devices are employed for such purposes.
<p>
2d. &quot;industria&quot; en el sentido del Derecho de patentes, &quot;producci&oacute;n autom&aacute;tica de bienes materiales&quot;;
<p>
<a name="art3a"><h3>1.3. Article 3a: Fields of Technology</h3></a>
<p>
3a. Member states shall ensure that data processing is not considered to be a field of technology in the sense of patent law, and that innovations in the field of data processing are not considered to be inventions in the sense of patent law.
<p>
<a name="art4"><h3>1.4. Article 4: Rules of Patentability</h3></a>
<p>
4.1. Para ser patentable, una invenci&oacute;n implementada en ordenador deber&aacute; ser susceptible de aplicaci&oacute;n industrial, nueva y suponer una actividad inventiva. Para entra&ntilde;ar una actividad inventiva, la invenci&oacute;n implementada en ordenador deber&aacute; aportar una contribuci&oacute;n t&eacute;cnica.
<p>
4.2. Los Estados miembros garantizar&aacute;n que constituya una condici&oacute;n necesaria de la actividad inventiva el hecho de que una invenci&oacute;n implementada en ordenador aporte una contribuci&oacute;n t&eacute;cnica.
<p>
4.3. The significant extent of the technical contribution shall be assessed by consideration of the difference between the technical elements included in the scope of the patent claim considered as a whole and the state of the art.
<p>
4.3a. In determining whether a given computer-implemented invention makes a technical contribution, the following test shall be used: whether it constitutes a new teaching on cause-effect relations in the use of controllable forces of natures and has an industrial application in the strict sense of the expression, in terms of both method and result.
<p>
<a name="art4a"><h3>1.5. Article 4a: Causas de exclusi&oacute;n de la patentabilidad</h3></a>
<p>
4a.1. No se considerar&aacute; que una invenci&oacute;n implementada en ordenador aporta una contribuci&oacute;n t&eacute;cnica meramente porque implique el uso de un ordenador, red u otro aparato programable. En consecuencia, no ser&aacute;n patentables las invenciones que utilizan programas inform&aacute;ticos que aplican m&eacute;todos comerciales, matem&aacute;ticos o de otro tipo y no producen efectos t&eacute;cnicos, aparte de la normal interacci&oacute;n f&iacute;sica entre un programa y el ordenador, red o aparato programable de otro tipo en que se ejecute.
<p>
4a.2. Los Estados miembros garantizar&aacute;n que las soluciones a problemas t&eacute;cnicos implementadas en ordenador no se consideren como invenciones patentables simplemente por el hecho de que mejoran la eficiencia en la utilizaci&oacute;n de los recursos dentro del sistema de tratamiento de datos.
<p>
<a name="art5"><h3>1.6. Article 5: Form of Claims; and further provisions</h3></a>
<p>
5. Member States shall ensure that a computer-implemented invention may be claimed only as a product, that is a set of equipment comprising both programmable apparatus and devices which use forces of nature in an inventive way, or as a technical production process operated by such a computer, computer network or apparatus through the execution of software.
<p>
5a. Member States shall ensure that the production, handling, processing, distribution and publication of information, in whatever form, can never constitute direct or indirect infringement of a patent, even when a technical apparatus is used for that purpose.
<p>
5b. Member States shall ensure that patent claims granted in respect of computer-implemented inventions include only the technical contribution which justifies the patent claim. 
<p>
5c. Member States shall ensure that the use of a computer program for purposes that do not belong to the scope of the patent cannot constitute a direct or indirect patent infringement.
<p>
5d. Member States shall ensure that whenever a patent claim names features that imply the use of a computer program, a well-functioning and well documented reference implementation of such a program is published as part of the patent description without any restricting licensing terms.
<p>
<a name="art6"><h3>1.7. Article 6: Interoperability</h3></a>
<p>
6. The rights conferred by patents granted for inventions within the scope of this Directive shall not affect acts permitted under Articles 5 and 6 of Directive 91/250/EEC on the legal protection of computer programs by copyright, in particular under the provisions thereof in respect of decompilation and interoperability.
<p>
6a. Los Estados miembros garantizar&aacute;n que, en cualquier caso en que sea necesaria la utilizaci&oacute;n de una t&eacute;cnica patentada con un prop&oacute;sito significativo, como es asegurar la conversi&oacute;n de las convenciones utilizadas en dos sistemas de ordenadores o redes diferentes a fin de hacer posible la comunicaci&oacute;n y el intercambio rec&iacute;proco del contenido de datos, esta utilizaci&oacute;n no se considere una violaci&oacute;n de la patente.
<p>
<a name="links"><h2>2. Annotated Links</h2></a>
<p>
<table border=1 cellpadding=20>
<tr><th align=left><dl><dt><b><img src="/img/icons.other/gotodoc.png" alt="-&gt;"><a href="http://www3.europarl.eu.int/omk/omnsapir.so/pv2?PRG=DOCPV&APP=PV2&LANGUE=EN&SDOCTA=2&TXTLST=1&POS=1&Type_Doc=RESOL&TPV=PROV&DATE=240903&PrgPrev=PRG@TITRE|APP@PV2|TYPEF@TITRE|YEAR@03|Find@%2a%69%6e%76%65%6e%74%69%6f%6e%73|FILE@BIBLIO03|PLAGE@1&TYPEF=TITRE&NUMB=1&DATEF=030924">The amendments approved, from the EP.</a></b></dt>
<dd>consolidated version by EP, changes were still being applied by the secretariat during the days after the vote.</dd></dl></th></tr>
</table>
<p>
<br>
<br>
<div align="center"><font size=2>[ <a href="../index.es.html">Europarl 2003/09 Software Patent Directive Amendments: Real vs Fake Limits</a> | <a href="../kond/index.en.html">Conditions for Approval of the Software Patent Directive Proposal</a> | <a href="../vote/index.en.html">Software Patent Directive Decision 2003/08/23: Voting Recommendations for MEPs</a> | <a href="../deba/index.en.html">Plenary Debate 03/09/23</a> | Europarl 2003-09-24: Amended Software Patent Directive ]</font></div>
<form action="http://www.google.com/search" method="get">
<input name=q size=25 type=text value="">
<input name=btnG size=25 type=submit value="Google Search ffii.org">
<input name=q size=25 type=hidden value="site:ffii.org">
</form>
<hr>
<address>
http://swpat.ffii.org/papri/eubsa-swpat0202/plen0309/resu/index.es.html<br>
<a href="http://www.gnu.org/licenses/fdl.html">&copy;</a>
2003/09/29
<a href="../../../../girzu/index.de.html">Workgroup</a><br>
versio es 2002/04/24 de <a href="http://www.proinnova.hispalinux.es/">proinnova</a>
</address></body>
</html>

<!-- Local Variables: -->
<!-- coding: utf-8 -->
<!-- srcfile: /ul/prg/src/mlht/app/swpat/plenresu0309.el -->
<!-- mode: html -->
<!-- End: -->

