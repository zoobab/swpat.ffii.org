\contentsline {chapter}{\numberline {1}The Text}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Article 1: Purpose}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Article 2: Definitions}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Article 3a: Fields of Technology}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Article 4: Rules of Patentability}{4}{section.1.4}
\contentsline {section}{\numberline {1.5}Article 4a: Exclusions from patentability}{5}{section.1.5}
\contentsline {section}{\numberline {1.6}Article 5: Form of Claims; and further provisions}{5}{section.1.6}
\contentsline {section}{\numberline {1.7}Article 6: Interoperability}{5}{section.1.7}
\contentsline {chapter}{\numberline {2}Annotated Links}{6}{chapter.2}
