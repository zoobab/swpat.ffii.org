<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#descr: Version consolidée des principales provisions (Art 1-6) de la Directive %(q:sur la brevetabilité des inventions mises en oevre par ordinateur) pour lesquelles le parlament européen a voté le 24 septembre de 2003.
#title: PE 2003-09-24: Directive Brevets Logiciels Amendées
#WWt: Directive concernant la brevetabilité des inventions mises en oeuvre par ordinateurs
#purp: Objectif
#defi: Définitions
#efn: Champs de Technique
#eai: Rèles de Brevetabilité
#uoa: Exclusions de la brevetabilité
#ono: Formulation de la revendication et autres dispositions
#tel: Interopérabilité
#Art1: Cette directive établit les règles pour la brevetabilité des inventions mises en oeuvre par ordinateurs.
#Am36: %(q:invention mise en oeuvre par ordinateur) désigne toute invention au sens de la Convention sur le brevet européen dont l'exécution implique l'utilisation d'un ordinateur, d'un réseau informatique ou d'un autre appareil programmable et présentant dans sa mise en oeuvre une ou plusieurs caractéristiques non techniques qui sont réalisées totalement ou en partie par un ou plusieurs programmes d'ordinateurs, en plus des caractéristiques techniques que toute invention doit apporter;
#Am107: %(q:contribution technique), aussi appelée %(q:invention), désigne une contribution à l'état de la technique dans un domaine technique. Le caractère technique d'une contribution est une des quatre conditions pour la brevetabilité. Additionnellement, pour mériter un brevet, la contribution technique doit être nouvelle, non évidente, et susceptible d'application industrielle.
#Am97: %(q:domaine technique) désigne un domaine industriel d'application nécessitant l'utilisation de forces contrôlables de la nature pour obtenir des résultats prévisibles. %(q:Technique) signifie %(q:appartenant à un domaine technique). L'utilisation de forces de la nature pour contrôler des effets physiques au delà de la représentation numérique de l'information appartient à un domaine technique. La production, la manipulation, le traitement, la distribution et la présentation de l'information n'appartiennent pas à un domaine technique, même si des dispositifs techniques sont utilisés dans ce but.
#Am38: %(q:industrie) dans le sens de la loi sur les brevets signifie %(q:production automatisée de biens matériels);
#Am45: Les Etats membres veillent à ce que le traitement des données ne soit pas considéré comme un domaine technique au sens du droit des brevets et à ce que les innovations en matière de traitement des données ne constituent pas des inventions au sens du droit des brevets.
#Am16N1: Pour être brevetable, une invention mise en oeuvre par ordinateur doit être susceptible d'application industrielle, être nouvelle et impliquer une activité inventive. Pour impliquer une activité inventive, une invention mise en oeuvre par ordinateur doit apporter une contribution technique.
#Am16N2: Les Etats membres veillent à ce que le fait qu'une invention mise en oeuvre par ordinateur apporte une contribution technique constitue une condition nécessaire à l'existence d'une activité inventive.
#Am100P1: Le caractère notable de la contribution technique est évalué en prenant en considération la différence entre les éléments techniques inclus dans l'objet de la revendication de brevet considéré dans son ensemble et l'état de la technique.
#Am70: Pour déterminer si une invention mise en oeuvre par ordinateur apporte une contribution technique, il y a lieu d'établir si elle apporte une connaissance nouvelle sur les relations de causalité en ce qui concerne l'utilisation des forces contrôlables de la nature et si elle a une application industrielle au sens strict de l'expression, tant sous l'angle de la méthode que sous celui du résultat.
#Am17: Une invention mise en oeuvre par ordinateur n'est pas considérée comme apportant une contribution technique uniquement parce qu'elle implique l'utilisation d'un ordinateur, d'un réseau ou d'un autre appareil programmable. En conséquence, ne sont pas brevetables les inventions impliquant des programmes d'ordinateurs, qui mettent en oeuvre des méthodes commerciales, des méthodes mathématiques ou d'autres méthodes, si ces inventions ne produisent pas d'effets techniques en dehors des interactions physiques normales entre un programme et l'ordinateur, le réseau ou un autre appareil programmable sur lequel il est exécuté.
#Am60: Les Etats membres veillent à ce que les solutions, mises en oeuvre par ordinateur, à des problèmes techniques ne soient pas considérées comme des inventions brevetables au seul motif qu'elles améliorent l'efficacité de l'utilisation des ressources dans le système de traitement des données.
#Am101: Les Etats membres veillent à ce qu'une invention mise en oeuvre par ordinateur ne puisse être revendiquée qu'en tant que produit, c'est-à-dire un ensemble d'équipements comprenant à la fois des appareils et des dispositifs programmables qui utilisent les forces de la nature d'une façon inventive, ou en tant que procédé de production technique exploité par un tel ordinateur, réseau d'ordinateur ou autre appareil à travers l'exécution d'un programme.
#Am103: Les Etats membres veillent à ce que la production, la manipulation, le traitement, la distribution et la publication de l'information, sous quelque forme que ce soit, ne puisse jamais constituer une contrefaçon de brevet, directe ou indirecte, même lorsqu'un dispositif technique est utilisé dans ce but. 
#Wce: Member States shall ensure that patent claims granted in respect of computer-implemented inventions include only the technical contribution which justifies the patent claim. 
#Am104N1: Les Etats membres veillent à ce que l'utilisation d'un programme d'ordinateur à des fins qui ne relèvent pas de l'objet du brevet ne puisse constituer une contrefaçon de brevet, directe ou indirecte.
#Am104N2: Les Etats membres veillent à ce que, lorsqu'une revendication de brevet mentionne des caractéristiques impliquant l'utilisation d'un programme d'ordinateur, la description publiée comprenne une mise en ½uvre de référence, opérationnelle et bien documentée, de ce programme, sans conditions de licence restrictives.
#Am19: Les droits conférés par les brevets d'invention délivrés dans le cadre de la présente directive ne portent pas atteinte aux actes permis en vertu des articles 5 et 6 de la directive 91/250/CEE concernant la protection juridique des programmes d'ordinateurs par un droit d'auteur, notamment en vertu des dispositions particulières relatives à la décompilation et à l'interopérabilité.
#Am76P1: Les Etats membres veillent à ce que, lorsque le recours à une technique brevetée est nécessaire à une fin significative, par exemple pour assurer la conversion des conventions utilisées dans deux systèmes ou réseaux informatiques différents, de façon à permettre entre eux la communication et l'échange de données, ce recours ne soit pas considéré comme une contrefaçon de brevet.
#mpW: The amendments approved, from the EP.
#sdn: consolidated version by EP, changes were still being applied by the secretariat during the days after the vote.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/app/swpat/plenresu0309.el ;
# mailto: mlhtimport@a2e.de ;
# passwd: XXXX ;
# feature: swpatdir ;
# dok: plenresu0309 ;
# txtlang: fr ;
# End: ;

