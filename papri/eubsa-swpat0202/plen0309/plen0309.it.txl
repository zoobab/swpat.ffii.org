<meta http-equiv="Content-Type" content="text/plain; charset=utf-8">

#title: Proposta di direttiva del Parlamento europeo e del Consiglio relativa ai limiti della brevettabilità in relazione all'elaborazione automatizzata dei dati e ai suoi campi di applicazione

#descr: È previsto che il Parlamento Europeo sia chiamato a decidere sulla Direttiva sulla Brevettabilità del Software il 23 Settembre. La direttiva, come proposta dalla Commissione Europea, demolisce la struttura base della legge attualmente in vigore (Art. 52 della Convenzione Europea sui Brevetti) e la sostituisce con uno Standard Trilatrale sviluppato nel 2000 dagli uffici brevetti USA, Europeo e Giapponese, in accordo con il quale tutte le soluzioni a problemi %(q:implementate al computer) sono invenzioni brevettabili. Alcuni membri del Parlamento hanno proposto emendamenti che hanno lo scopo di confermare il più restrittivo concetto di invenzione presente nella Convenzione Europea sui Brevetti, mentre altri spingono per una brevettabilità illimitata in accordo con lo Standard Trilaterale, sebbene retoricamente in panni restrittivi. Noi tentiamo un'analisi comparativa di tutti gli emendamenti proposti, così da facilitare coloro i quali dovranno decidere nel riconoscere se stiano votando per una limitazione vera oppure falsa della brevettabilità.

#Lgn: Leggenda e Esempio Commentato

#ril: Articoli e Mozioni

#eia: Rifiuti/Approvazioni, Titoli e Considerazioni

#enm: Numero dell'emendamento (per scopi di riferimento in questo documento, i veri numeri degli emendamenti saranno probabilmente diversi)

#jac: pro, contro, indecisi

#tuW: 410 hanno votato in favore, 178 hanno votato contro

#TitTit: Titolo

#TitCec: Proposta per una Direttiva del Parlamento Europeo e il Consiglio sulla patentabilità delle invenzioni implementate al computer.

#trW: Testo della %(op:proposta originale della Commissione Europea)

#TitSyn: Lo scopo della direttiva non può essere quello di dichiarare tutti i tipi di idee %(q:realizzate con un computer) come invenzioni brevettabili. Piuttosto, l'intenzione è quella di chiarire i limiti della brevettabilità rispetto all'elaborazione automatica dei dati, nonchè rispetto ai suoi varii campi di applicazione (tecnici e non tecnici). Ciò deve essere espresso nel titolo in modo chiaro e non ambiguo.

#ung: Sample justification/comments

#enm2: Numeri degli Emendamenti

#uia: Firmatari

#amd: raccomandazioni sul voto

#ogs: risultati delle votazioni

#Am29: Proposta di direttiva del Parlamento europeo e del Consiglio relativa ai limiti della brevettabilità in relazione all'elaborazione automatizzata dei dati e ai suoi campi di applicazione

#eet: testo dell'emendamento

#tIa: Patent Insurance Extensions

#Art1: La presente direttiva stabilisce norme relative alla brevettabilità delle invenzioni attuate per mezzo di elaboratori elettronici.

#nWw: Le idee implementate in hardware generico di computer non sono invenzioni nel senso espresso dall'Art. 52(2) EPC. Il termine %(q:invenzione implementata con il computer) non è noto colui che è competente in materia, e siccome l'unica cosa che possa essere implementata in un computer sono i programmi per computer (una lavatrice o un telefono cellulare non possono essere implementati in un computer), esso suggerisce che i programmi di computer possano essere invenzioni. Esso è stato introdotto dall'EPO nel 2000 nella famigerata %(e6:Appendice 6 del documento Trilaterale Website) con il fine di portare la legge sui brevetti Europea in linea con gli USA ed il Giappone e per permettere brevetti per %(q:metodi di business implementati con il computer), in anticipazione dell'eliminazione dell'Art. 52(2) EPC.

#Am116: La presente direttiva stabilisce norme relative ai limiti della brevettabilità e dell'applicabilità dei brevetti per quanto riguarda i programmi per elaboratori elettronici.

#Art2a: %(q:invenzione attuata per mezzo di elaboratori elettronici), un'invenzione la cui esecuzione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta a prima vista una o più caratteristiche di novità che sono realizzate in tutto o in parte per mezzo di uno o più programmi per elaboratore;

#apt: L'Art. 52 della EPC afferma chiaramente che un programma per computer (un oggetto che consiste di un equipaggiamento per l'elaborazione dei dati e le regole per le sue operazioni) non può costituire un invenzione brevettabile.

#fet: Gli emendamenti 36, 117 e 42 chiarificano che un'innovazione è brevettabile solamente se è conforme all'Art. 52 dell'EPC, a prescindere che un programma di computer faccia parte o no della sua implementazione.  L'emendamento 14 permetterebbe che un puro programmma per computer sia dichiarato come %(q:un'invenzione la cui esecuzione coinvolge l'uso di un computer e avente tutte le sue caratteristiche realizzate per tramite di un programma per computer), contraddicendo in questo modo l'Art. 52 dell'EPC.

#Am36: %(q:invenzione attuata per mezzo di elaboratori elettronici), un'invenzione ai sensi della Convenzione per il brevetto europeo la cui esecuzione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta nelle sue applicazioni una o più caratteristiche non tecniche che sono realizzate in tutto o in parte per mezzo di uno o più programmi per elaboratore, oltre al contributo tecnico che ogni invenzione deve arrecare;

#Am14: %(q:invenzione attuata per mezzo di elaboratori elettronici), un'invenzione la cui esecuzione implica l'uso di un elaboratore, di una rete di elaboratori o di un altro apparecchio programmabile e che presenta una o più caratteristiche che sono realizzate in tutto o in parte per mezzo di uno o più programmi per elaboratore;

#Art2b: %(q:contributo tecnico), un contributo allo stato dell'arte in un settore tecnico, giudicato non ovvio da una persona competente nella materia.

#Art2bSyn1: Una direttiva che fa girare la brevettabilità intorno alla parola %(q:contributo tecnico) deve spiegare questo termine chiaramente.

#Art2bSyn96: L'emendamento 96 sembra implicare che solo il problema ha bisogno di essere tecnico, ma non la sua soluzione. La parola %(q:significativo) non ha significato legalmente, e quindi porta alla confusione piuttosto che alla chiarezza. Ovviamente, il termine %(q:tecnico) ha bisogno di essere definito chiaramente se deve avere un effetto utile. Richiedere un problema tecnico da risolvere è pessimo da parte di un CEC o peggio. Non dovremmo concentrarci sull'applicazione (il problema da risolvere) ma sulla soluzione (il risultato di una ricerca empirica che potrebbe meritare un brevetto). Si potrebbe dire che ogni software risolve un problema, ma se la soluzione si limita ad innovare il software, non può essere definita una soluzione tecnica (o almeno non una nuova soluzione tecnica).

#Art2bSyn69: Amendment 69 goes in the right direction by stating that %(q:processing, handling, and presentation of information do not belong to a technical field).  It risks mixing the %(q:technical contribution) (= invention) requirement with non-obviousness requirements, similar to 96.  However even though the wording is redundant, there is nothing wrong with saying that the contribution (invention) must be %(q:non-obvious).  This can even help to correct EPO misperceptions, according which %(q:the non-obviousness must contain a technical contribution).  Given that this amendment also defines what %(q:technical fields) (Art 27 TRIPs) are, it is extremely useful.

#Art2bSyn107: Infine, l'emendamento 107 è corretto: afferma chiaramente che %(q:contributo tecnico) è sinonimo di invenzione e ripete i requisiti di brevettabilità dell'Art 52 EPC. Qesto emendamento non limita la brevettabilità in alcun modo, ma conferma semplicemente l'Art 52 EPC e rimuove ogni confusione che potrebbe insorgere mischiando i test di brevettabilità, come fanno il testo CEC originale e gli altri emendamenti.

#Am69: %(q:contributo tecnico), un contributo allo stato dell'arte in un settore tecnico, giudicato non ovvio da una persona competente nella materia. L'impiego delle forze della natura per controllare gli effetti fisici al di là della rappresentazione digitale delle informazioni rientra in un settore tecnico. Il trattamento, la manipolazione e le presentazioni di informazioni non rientrano in un settore tecnico, anche se sono utilizzati apparecchi tecnici per effettuarli.

#Am107: %(q:contributo tecnico), altrimenti chiamato %(q:invenzione), un contributo allo stato dell'arte in un settore tecnico. La natura tecnica del contributo costituisce uno dei quattro requisiti della brevettabilità. In aggiunta, per poter ricevere un brevetto, il contributo tecnico deve presentare un carattere di novità, essere non ovvio ed atto ad un'applicazione industriale.

#Am96: %(q:contributo tecnico), un contributo, che implichi un'attività inventiva, ad un settore tecnico, che risolva un problema tecnico esistente o estenda lo stato dell'arte in misura significativa per una persona competente nella materia.

#Art2baSyn: Since the rapporteur's attempt to %(q:make it clear what is patentable and what not) hinges entirely on the word %(q:technical), this term must be defined clearly and restrictively. The only definition which achieves this is the one along the lines of amendments 55,97,108, which is found in various national case laws and in some patent laws (e.g. Nordic Patent Treaty and patent laws of Poland, Japan, Taiwan et al). This definition is based on valid concepts of science and empistemology and has been proven to have a clear meaning in practise. It assures that broad, expensive and insecurity-fraught broad exclusion rights such as patents are used only in areas where there is an economic rationale for them, and that abstract-logical innovation is the domain of copyright and copyright-like sui generis rights only.  The rough definition given here corresponds to the worldwide common understanding of the term, implicit in current-day case law and in the JURI report other statements about what should be patentable (e.g. %(q:not software as such, but inventions related to mobile phones, engine control devices, household appliances, ...) ).  This understanding should be made explicit for the purpose of clarification.

#Am97: %(q:settore tecnico) un settore industriale d'applicazione che presuppone l'utilizzo di forze naturali controllabili per ottenere risultati prevedibili. %(q:Tecnico) significa %(q:appartenente a un settore della tecnologia). L'impiego delle forze della natura per controllare gli effetti fisici al di là della rappresentazione digitale delle informazioni rientra in un settore tecnico. La produzione, la manipolazione, il trattamento, la distribuzione e la presentazione dell'informazione non appartengono a un settore della tecnologia, anche se a tali fini sono utilizzati dispositivi tecnici.

#Am37: %(q:tecnologia), ai sensi del diritto dei brevetti, scienza naturale applicata; %(q:tecnico), ai sensi del diritto dei brevetti, concreto e fisico.

#Am39Syn: Questa è un luogo comune standard in molti ordinamenti sui brevetti. Le %(q:quattro forze della natura) sono un concetto acquisito dell'epistemologia (teoria della scienza). Mentre la matematica è astratta e non è collegata alle forze della natura, alcuni metodi commerciali possono ben dipendere dalla chimica delle cellule cerebrali del cliente, cosa peraltro non controllabile, (ossia non deterministica) e soggetta al libero arbitrio. Quindi, il termine %(q:forze controllabili della natura) esclude chiaramente ciò che va escluso e tuttavia fornisce abbastanza flessibilità per includere potenziali futuri campi di applicazione delle scienze naturali, oltre a quelli oggi conosciuti come le %(q:quattro forze della natura). Si noti che questo emendamento non impedisce di rendere brevettabili le invenzioni in dispositivi come i telefoni cellulari o le lavatrici, semplicemente fa sì che la semplice aggiunta di un nuovo programma per computer a un dispositivo (tecnico) già noto non renda brevettabile il programma per computer stesso. Al contrario, aggiungere un programma per computer a un'invenzione altrimenti brevettabile non rende questa invenzione non brevettabile (visto che l'invenzione risolve ancora un problema usando forze controllabili della natura, a prescindere dalla presenza del programma per computer).

#Am39: %(q:invenzione), ai sensi del diritto dei brevetti, soluzione di un problema che prevede l'impiego di forze naturali controllabili.

#Am38Syn: Non vogliamo che le innovazioni nella %(q:industria discografica) o nella %(q:industria dei servizi legali) rispettino il requisito TRIPs della %(q:applicabilità industriale). La parola %(q:industria) è oggi spesso usata con significati allargati che non sono appropriati nel contesto del diritto brevettuale.

#Am38: %(q:industria), ai sensi del diritto dei brevetti, produzione automatizzata di beni materiali;

#Art3: Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia considerata appartenente ad un settore della tecnologia.

#Art3Syn: Il testo della Commissione Europea afferma che tutte le idee %(q:realizzate con un computer) implementate con il computer sono invenzioni brevettabili secondo l'Art 27 TRIPs. Bocciarlo è bene, ma sarebbe stato meglio chiarire in che modo si applica l'Art 27 TRIPs.

#Art3aSyn1: Uno dei principali obiettivi del progetto di direttiva è stato quello di chiarire l'interpretazione dell'Art 52 EPC alla luce dell'Art 27 TRIPs, che afferma che le %(q:inventions in all fields of technology) invenzioni in tutti i campi della tecnologia devono essere brevettabili. Proponiamo di tradurre l'Art 52(2) EPC nel linguaggio dell'Art 27 TRIPs, secondo le linee dell'Emendamento CULT-16.

#Art3aSyn2: Se l'elaborazione dei dati è %(q:technical), allora ogni cosa è %(q:technical).

#Art3aSyn3: L'elaborazione dei dati è un fattore comune in tutti i campi tecnologici e non tecnologici. Con l'avvento (universale) del computer negli anni 50, l'elaborazione dei dati automatizzata (EDP) è permeata nella società e nell'industria

#Art3aSyn4: Come ha scritto nel 1977 Gert Kolle, uno dei teorici principali dietro la decisione, negli anni 70, di escludere il software dalla brevettabilità (vedi Gert Kolle 1977: Technik, Datenverarbeitung und Paten trecht -- Bermerkungen zur Dispositionsprogramm - Entscheidung des Bundesgerichtshofs):

#Art3aSynGK77: L'elaborazione automatica dei dati (EDP) è diventata oggi un indispensabile strumento di ausilio in tutti i campi della società umana e rimarrà così in futuro. E' onnipresente. ... Il suo significato strumentale, la sua funzione ausiliaria e dipendente, contraddistingue l'EDP dagli altri campi della tecnologia e la paragona ad aree come l'amministrazione di impresa, i cui metodi e risultati di lavoro ... sono necessari ad ogni impresa e per la quale, pertanto, si indica la necessità %(q:prima facie) di assicurarne la libera disponibilità.

#Art3aSynCult: CULT è stato ben raccomandato di votare per una chiara esclusione dell'elaborazione dati dall'obiettivo di %(q:technology).

#Art3aSynStud: Numerosi lavori, alcuni dei quali condotti da istituzioni EU, come anche le opinioni del Comitato Sociale ed Economico Europeo delle Regioni, spiegano in dettaglio perchè l'economia dell'Europa riceverebbe un danno, se le innovazioni riguardanti l'elaborazione dei dati non venissero chiaramente escluse dalla brevettabilità.

#Am45: Gli Stati membri assicurano che l'elaborazione dei dati non venga considerata un settore della tecnologia ai sensi del diritto dei brevetti e che le innovazioni nel settore dell'elaborazione di dati non vengano considerate invenzioni ai sensi del diritto dei brevetti.

#Art4N1: Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici sia brevettabile, a condizione che sia atta ad un'applicazione industriale, presenti un carattere di novità e implichi un'attività inventiva.

#Art4N2: Gli Stati membri assicurano che, affinchè sia considerata implicante un'attività inventiva, un'invenzione attuata per mezzo di elaboratori elettronici arrechi un contributo tecnico.

#Art4N3: Il contributo tecnico è valutato considerando la differenza tra l'oggetto della rivendicazione di brevetto nel suo insieme, i cui elementi possono comprendere caratteristiche tecniche e non tecniche, e lo stato dell'arte.

#Art4Syn: Questo articolo confonde il test di non-ovvietà con il test di invenzione, indebolendoli entrambi e creando un conflitto con l'Art 52 EPC.

#cpW: Il JURI ha diviso questo articolo nelle parti 1, 2 e 3. Questo è pericoloso perchè previene alcuni emendamenti, che potrebbero applicarsi all'intero articolo, dall'essere inseriti nell voto.

#iaa: Diviso nelle parti 1, 2 e 3

#Art4N1Syn: Le %(q:invenzioni realizzate col computer) (cioè idee inquadrate in termini di equipaggiamento per elaborazione dati di utilizzo generale = programmi per computer) sono non-invenzioni secondo l'Art 52 EPC. vedi anche l'analisi relativa all'articolo 1. L'emendamento 56=98=109 sistema il conflitto e ri-espone l'Art 52 EPC. Nessuno degli emendamenti limita la brevettabilità, ma questo emendamento almeno espone le cose in maniera formalmente corretta.

#Am16N1: Onde poter essere brevettabile, un'invenzione attuata per mezzo di elaboratori elettronici deve essere suscettibile di applicazione industriale, presentare un carattere di novità e implicare un'attività inventiva. Per implicare un'attività inventiva, un'invenzione attuata per mezzo di elaboratori elettronici deve apportare un contributo tecnico.

#Am98: Gli Stati membri assicurano che i brevetti siano rilasciati solo per invenzioni tecniche che presentano un carattere di novità, di non-ovvietà e siano atti ad un'applicazione industriale.

#Art41aSyn: Doctrine of the German Federal Patent Court's Error Search Decision of 2002, negates an EPO doctrine which makes most computerised business methods patentable.

#Am47: Member States shall ensure that computer implemented solutions to technical problems are not considered to be patentable inventions when they only improve efficiency in the use of resources within the data processing system.

#Art41bSyn: Una riesposizione propria dell'Art 52 EPC. Dal momento che l'obiettivo di questa direttiva è di armonizzare e chiarire, è di importanza preminente sforzarsi di dare una interpretazione corretta della EPC, dal momento che è la base della normativa sui brevetti in Europa.

#Am48: Gli Stati membri assicurano che una condizione della costituzione di una invenzione ai sensi del diritto dei brevetti sia che una innovazione, a prescindere dal fatto che comporti o meno l'utilizzazione di un elaboratore elettronico, debba essere di carattere tecnico.

#Art4N2Syn: Il testo della Commissione devia dell'Art 52 EPC. Come spiegato dall'analisi del Recital 11, si deve prima determinare se c'è un'invenzione o contributo tecnico (sia usando la definizione in negativo dell'Art 52 EPC: un programma di computer non è un'invenzione, oppure usando la definizione al positivo che è emersa dai precedenti di legge, basato su forze della natura controllabili). In seguito, si deve controllare se questa invenzione supera gli altri 3 test dell'Art 52, dei quali il progresso creativo (= non-ovvietà) è uno. Il testo della Commissione richiede che il test sul passo creativo sia un requisito perchè qualcosa sia una invenzione. Dal momento che un programma di computer può essere non-ovvio, potrebbe essere un invenzione secondo questa definizione (al contrario di quanto dice l'Art 52 EPC).

#Art4N2SynP2: Inoltre, il testo della Commissione implica che idee inquadrate in termini di equipaggiamento per elaborazione dati di utilizzo generale (programmi per computer) sono invenzioni brevettabili. La cancellazione, come proposta dall'emendamento 82, potrebbe essere utile, perchè l'unico effetto di questo paragrafo è di confondere i test di brevettabilità e quindi negare a un ufficio brevetti nazionale la possibilità di rigettare le domande di brevetto %(q:non-statutory) senza un'esame sostanziale. L'emendamento 16 commette lo stesso errore del testo della commissione, chiarisce solo un pò il testo. L'emendamento 40 sarà ritirato; è stato reinserito come aggiunta (83).

#Am16N2: Gli Stati membri assicurano che costituisca una condizione necessaria dell'attività inventiva il fatto che un'invenzione attuata per mezzo di elaboratori elettronici apporti un contributo tecnico.

#Art4N3Syn1: Quando si brevetta un'invenzione, si brevetta una sua particolare applicazione. Tale applicazione è ciò che si descrive nella richiesta di brevetto. Come tale la richiesta (nel suo insieme) contiene in un certo senso sia la l'invenzione stessa che un certo numero di caratteristiche non brevettabili (p.es. un programma per elaboratore) necessarie per l'applicazione dell'invenzione. Questo significa che anche se si richiede che l'invenzione sia tecnica, la richiesta può contenere elementi non-tecnici (che non sono affatto oggetto di contestazione, i brevetti hanno sempre funzionato così).

#Art4N3Syn2: Comunque, la proposta della Commissione Europea annulla il suo stesso concetto di %(q:contributo tecnico), consentendo che il contributo consista di elementi non-tecnici. La versione del JURI dichiara solo che la richiesta debba contenere elementi tecnici, ma questo non è un limite reale. Un esempio può chiarire questo punto. Si immagini di voler brevettare un programma per elaboratore, quindi diciamo che quello sia il %(q:contributo tecnico). Nella richiesta, si descrive l'applicazione di questo programma, ovvero diciamo che brevettiamo l'esecuzione di questo programma su un elaboratore. Ora la richiesta di brevetto nel suo insieme contiene elementi tecnici (l'elaboratore), e la differenza fra la richiesta nel suo insieme (elaboratore noto + programma nuovo) e lo stato dell'arte (elaboratore nuovo) è il programma dell'elaboratore. Quindi un programma per elaboratore può essere un contributo tecnico, secondo queste condizioni, il che è completamente contraddittorio (perchè un programma per elaboratore non può essere tecnico). E' chiaro che è necessaria una correzione come quella dell'emendamento 57=99=110, se si vuole usare in qualche modo il concetto di %(q:contributo tecnico).

#lWo: Il periodo di sospensione proposto dall'emendamento 100 è una questione totalmente diversa. Come mostrato da una %(ng:recente consultazione condotta dall'ufficio brevetti inglese), è molto controverso anche fra quanti sono supposti di beneficiarne. Infatti non è chiaro se un periodo di sospensione produrrà benefici, come suggerito dal rapporto ITRE, alle PMI. Nel caso dello sviluppo open source, esso potrebbe anche provocare ulteriore insicurezza riguardo al fatto se idee rese pubblche siano libere da brevetti.

#Am16N3: Il contributo tecnico è valutato considerando lo stato dellarte e loggetto della rivendicazione di brevetto nel suo insieme, che deve comprendere le caratteristiche tecniche, indipendentemente dal fatto che tali caratteristiche siano o non siano accompagnate da caratteristiche non tecniche.

#Am99: Il contributo tecnico è valutato considerando la differenza tra tutte le caratteristiche tecniche della rivendicazione di brevetto e lo stato dell'arte.

#Am100P1: La portata significativa del contributo tecnico è valutata considerando la differenza tra gli elementi tecnici inclusi nell'oggetto della rivendicazione di brevetto nel suo insieme e lo stato dell'arte. Gli elementi divulgati dal richiedente il brevetto nei sei mesi che precedono la data di deposito della domanda non sono considerati parte dello stato dell'arte al momento della valutazione della rivendicazione di cui trattasi.

#Art43aSyn: Come menzionato nell'analisi dell'articolo 1, %(q:invenzione realizzata con un computer) è un termine contraddittorio. Come menzionato nell'analisi del Recital 11, %(q:invenzione) e %(q:contributo tecnico) sono sinonimi: quello che si inventa è il proprio contributo (tecnico) allo stato dell'arte. Nel gergo usato dalla Direttiva, però, %(q:contributo tecnico) significa solo %(q:soluzione ad un problema tecnico come distanza che intercorre tra il più vicino documento di arte precedente e l'invenzione reclamata). In questo contesto non è chiaro quale sia l'effetto di qualunque verifica, comunque restrittiva, perchè non è chiaro a cosa si applichi.

#Art43aSynTest: Inoltre, dicendo che tale test %(q:deve essere applicato), la disposizione non richiede chiaramente che tale test debba effettivamente %(e:essere passato), e nemmeno che definisca cosa si intenda per %(q:contributo tecnico).

#Art43aSynInd: Infine, non esiste una definizione legale di %(q:applicazione industriale nello stretto senso del'espressione, in termini sia di metodi che di risultati).

#Art43aSynVal: A dispetto di queste limitazioni, si raccomanda di supportare l'emendamento 70. Se non altro esso codifica l'aspetto centrale del concetto di %(q:invenzione tecnica). Quindi, in combinazione con altri emendamenti, potrebbe alla fine contribuire a tracciare un chiaro limite di brevettabilità.

#Am70: Per determinare se una data invenzione attuata per mezzo di elaboratore elettronico arreca un contributo tecnico, si applica il seguente criterio: si valuta se essa costituisce un nuovo insegnamento sulle relazioni di causa-effetto nell'impiego delle forze controllabili della natura e se ha un'applicazione industriale nel senso stretto dell'espressione, in termini sia di metodo che di risultato.

#Art4aSyn: L'emendamento 17 assume che %(q:normale interazione fisica tra un programma e il computer) significhi qualcosa. Non è così. Ispirandosi alla recente decisione %(es:%(tp|Error Search|Suche Fehlerhafter Zeichenketten) decision) del German Federal Patent Court, si potrebbe ad esempio adottare l'interpretazione secondo cui %(q:l'incremento dell'efficienza del computer), %(q:risparmio nell'utilizzo della memoria), etc %(q:non costituiscono un contributo tecnico), o %(q:sono nella portata tra un programma e il computer). Senza una tale precisazione, l'Emendamento 17 aggiunge solo confusione invece di chiarezza.

#Am17: %(al|Cause di esclusione dalla brevettabilità|Un'invenzione attuata per mezzo di elaboratori elettronici non è considerata arrecante un contributo tecnico semplicemente perchè implica luso di un elaboratore, di una rete o di un altro apparecchio programmabile. Pertanto, non sono brevettabili le invenzioni implicanti programmi per elaboratori che applicano metodi per attività commerciali, metodi matematici o di altro tipo e non producono alcun effetto tecnico oltre a quello delle normali interazioni fisiche tra un programma e lelaboratore, la rete o un altro apparecchio programmabile in cui viene eseguito.)

#Art4bSyn: Stessi commenti che per l'emendamento di cui sopra.

#Am87: %(al|Esclusioni dalla brevettabilità:|Un'invenzione attuata per mezzo di elaboratori elettronici non è considerata arrecante un contributo tecnico semplicemente perchè implica luso di un elaboratore, di una rete o di un altro apparecchio programmabile. Pertanto, non sono brevettabili le invenzioni implicanti unicamente programmi di elaboratori (elaborazione di dati) che applicano metodi per attività commerciali, metodi matematici o di altro tipo e non producono alcun effetto tecnico oltre a quello delle normali interazioni fisiche tra un programma e lelaboratore, la rete o un altro apparecchio programmabile in cui viene eseguito.)

#Art4cSyn1: La direttiva come proposta dal CEC e JURI è basata sullo Standard Trilaterale del 2000 degli uffici brevetti statunitensi, giapponesi ed europei, che considera tutti i programmi per elaboratore come invenzioni brevettabili. Questo è espresso nel termine %(q:invenzione realizzata con un computer), che fu introdotto dall'EPO per supportare tale standard. Non esiste più un test che verifichi che l'invenzione sia presente, ma solo un test di %(q:contributo tecnico al progresso tecnologico), ovvero il progresso fra lo %(q:stato dell'arte più vicino) e l'%(q:invenzione) deve essere inquadrato in termini di un %(q:problema tecnico), cioè un problema di miglioramento della velocità o di efficienza nell'uso della memoria di un elaboratore. Usando questa normativa EPO, ogni algoritmo o tecnica di business diventa in effetti brevettabile, come è stato sottolineato dalla German Federal Patent Court in una %(es:recente decisione).

#Art4cSyn2: Gli emendamenti 47 e 60 indicano chiaramente all'EPO di seguire la Corte Federale Tedesca per i Brevetti nel rifiutare i miglioramenti nell'efficienza del processamento dei dati come %(q:contributo tecnico).

#Am60: Gli Stati membri garantiscono che le soluzioni attuate mediante elaboratore elettronico rispetto a problemi tecnici non siano considerate invenzioni brevettabili solo perchè migliorano l'efficacia nell'impiego delle risorse del sistema di trattamento dei dati.

#Art4dSyn1: Questo emendamento da solo potrebbe rimpiazzare la maggior parte degli argomenti della direttiva. Le linee guida dell'EPO del 1978 forniscono una chiara interpretazione dell'Art 52. Questo fu cambiato %(q:in risposta alle richieste dell'industria) da una versione successiva delle linee guida, al costo di rendere le regole contraddittorie e introducendo uno scisma. Gli obiettivi di questa direttiva dovrebbero essere di risolvere lo scisma e concretizzare l'applicazione del trattato TRIPs del 1994. Il primo obiettivo può essere raggiunto da questo singolo emendamento. Notare che l'emendamento non dice che le linee guida dell'EPO del 1978 debbano diventare legge. Meramente dice che forniscono una giusta interpretazione dell'Art 52 EPC.

#Am46: Gli Stati membri assicurano che i brevetti sulle innovazioni nelsettore dell'informatica vengano legittimati e applicati soltanto se sono stati concessi a norma dell'articolo 52 della Convenzione europea sui brevetti del 1973 secondo l'interpretazione di cui negli Orientamenti per gli esami dell'Ufficio europeo dei brevetti del 1978.

#Art5: Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici possa essere rivendicata come prodotto, ossia come elaboratore programmato, rete di elaboratori programmati o altro apparecchio programmato, o come processo realizzato da tale elaboratore, rete di elaboratori o apparecchio mediante l'esecuzione di un software.

#Art5Syn18: L'emendamento 18 propone di introdurre le rivendicazioni nei programmi, ovvero rivendicazioni della tipo %(bc:un programma, caratterizzato dal fatto che una volta caricato nella memoria di un computer [ qualche processo ] venga eseguito.) Questo renderebbe ogni pubblicazione di software una potenziale diretta violazione di brevetto e quindi metterebbe a rischio programmatori, ricercatori e distributori di servizi Internet. Senza le rivendicazioni sui programmi, i distributori potrebbero essere ritenuti responsabili solo indirettamente, ad esempio attraverso la garanzia che danno ai loro clienti. In questo modo, la richiesta di rivendicazione sui programmi sembra essere più simbolica che di natura materiale: la lobby dei brevetti vuole documentare in modo inequivocabile la brevettabilità del software. CEC ha preso le distanze da quest'ultima conseguenza.

#Art5Syn58: Finora, l'impostazione di CEC suggerisce che i dispositivi generici di calcolo e le regole per il calcolo su di questi eseguite (= i programmi per computer) sono prodotti e processi brevettabili. L'emendamento 101/58 prova a risolvere questo problema. Potrebbe essere una buona idea sostituire %(q:processi tecnici di produzione guidati da un tale computer) con %(q:the inventive processes running on such a set of equipment) se è ancora possibile nella forma di un %(q:emendamento di compromesso). L'emendamento 102 definisce %(q:invenzione realizzata con un computer) come un prodotto o un processo produttivo tecnologico. Se %(q:tecnico) fosse definito, potrebbe essere utile. Ciò nonostante, un computer che esegue un programma potrebbe anche esseere interpretato come un %(q:dispositivo programmato), e brevettare l'uso di un programma quando eseguito su un computer ha lo stesso effetto di brevettare il programma in se stesso: non c'è altro modo di usare un programma per computer se non l'esecuzione su un computer.

#Am18: Una rivendicazione di un programma per elaboratori elettronici relativa al programma in sè per sè, collocato su un vettore oppure come segnale, è ammissibile solo se tale programma, una volta installato o in funzione su un elaboratore elettronico, una rete di elaboratori o altro apparecchio programmabile, dia origine ad un prodotto o realizzi un processo brevettabile ai sensi degli articoli 4 e 4-bis.

#Am101: Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici possa essere rivendicata solo come prodotto, ossia come un insieme di dispositivi comprensivo di apparecchi programmabili che utilizzano le forze della natura in modo creativo, o come processo tecnico di produzione realizzato da tale elaboratore, rete di elaboratori o apparecchio mediante l'esecuzione di un software.

#Am102: Gli Stati membri assicurano che un'invenzione attuata per mezzo di elaboratori elettronici possa essere rivendicata solo come prodotto, ossia come dispositivo programmato o come processo tecnico di produzione.

#Art5aSyn: Il software libero rende importanti contributi all'innovazione e alla diffusione del sapere senza beneficiare del sistema dei brevetti. Questo dovrebbe essere tenuto in conto nella politica dell'innovazione e nella legge, senza implicare che il software proprietario dovrebbe essere brevettabile. Sfortunatamente questo emendamento usa il termine %(q:invenzioni realizzate con un computer), che fu introdotto nel 2000 dall'EPO nel tentativo di implicare la brevettabilità delle generiche operazioni di elaborazione dei dati (software). Questo dovrebbe essere corretto in seconda lettura.

#Am62: %(al|Limitazione degli effetti dei brevetti concessi alle invenzioni attuate per mezzo di elaboratore elettronico|%(linol|Gli Stati membri garantiscono che i diritti collegati al brevetto non vengano estesi alle attività effettuate per eseguire, copiare, distribuire, studiare, modificare o migliorare un programma informatico, distribuito sotto una licenza che prevede:|la libertà di eseguire il programma;|la libertà di studiare come il programma funziona e di adattarlo alle necessità dell'utente;|la libertà di ridistribuire copie alle stesse condizioni previste dalla licenza;|la libertà di migliorare il programma e di divulgare i miglioramenti al pubblico, alle stesse condizioni previste dalla licenza;|il libero accesso al codice fonte del programma.))

#Art51a: Questo emendamento apparentemente intende escludere le richieste su modi di elaborare i dati in hardware (= programmi per computer), ma propone metodi regolatori inappropriati. Le richieste dei brevetti comprendono sempre più di un'invenzione (= contributo tecnologico). Se c'è un'invenzione tecnologica (es. un nuovo processo chimico), allora non c'è ragione per cui la richiesta di brevetto non debba comprendere questa invenzione insieme a degli elementi non-inventivi, come un programma per computer che controlla la reazione. Una richiesta non descrive un'invenzione, ma lo ambito di un diritto di esclusione che è leggittimato da un'invenzione.

#Am72: Gli Stati membri garantiscono che le rivendicazioni di brevetto accolte per le invenzioni attuate per mezzo di elaboratori elettronici comprendano solo il contributo tecnico che motiva la rivendicazione di brevetto. Una rivendicazione di brevetto per un programma per elaboratore, relativa al solo programma o ad un programma esistente su un supporto dati, non è accettabile.

#Am103Syn: Senza questo emendamento, i possessori di brevetti software potrebbero ancora inviare lettere di diffida ai programmatori o ai distributori di software accusandoli di violazione di contibuti. Questo articolo assicura che il diritto di pubblicazione come garantito dall'articolo 10 ECHR abbia precedenza sui brevetti. D'altra parte, questo emendamento non previene la garanzia dei brevetti software o la loro imposizione contro gli utenti del software. I produttori di software proprietario e i distributori commerciali di Linux potrebbero essere ancora messi sotto pressione dai loro clienti, che tipicamente imporrebbero una obbligazione contrattuale della loro autorizzazione.

#Am103: Gli Stati membri assicurano che la produzione, la manipolazione, il trattamento, la distribuzione e la pubblicazione di informazioni, in qualsiasi forma, non possano mai costituire una violazione di brevetto, diretta o indiretta, anche se a tale fine sono stati utilizzati dispositivi tecnici.

#Am104N1Syn: Lo scopo del brevetto è normalmente definito dai diritti, e se qualcosa esce dallo scopo del diritto, non costituisce una violazione. Così l'emendamento, a prima vista, sembra tautologico. L'intenzione di questo emendamento può essere quella di permettere la simulazione di processi brevettabili su un procedimento standard di elaborazione dei dati. Se così fosse, dovrebbe essere detto chiaramente, come viene fatto in alcuni degli altri emendamenti.

#Am104N1: Gli Stati membri assicurano che l'uso di un programma per elaboratore per scopi che non riguardano l'oggetto del brevetto non possa costituire una violazione di brevetto diretta o indiretta.

#Art5dSyn: Questi emendamenti, al contrario di quello che qualcuno può pensare, non servono a promuovere software free/opensource, ma piuttosto di assicurare che l'obbligo di rivelazione che è inerente nel sistema dei brevetti venga preso seriamente e che il software è, come ogni oggetto di informazione, dalla parte della rivelazione del brevetto piuttosto che in quella della sua esclusione/monopolizzazione. Questo rende un pò più difficile bloccare altri individui dal fare cose che nemmeno lo sviluppatore ha fatto, ma che sono ovviamente possibili dato che il modello di computazione è perfettamente definito e si sa sempre quello che si può fare con un computer. Quando si pubblica codice sorgente funzionante almeno si offre un pò di conoscenza reale di come si risolve il problema, al contrario del paragrafo %(q:i mezzi di calcolo accoppiati a mezzi di ingresso/uscita in modo che calcolino una funzione tale che il risultato di detta funzione attraverso i detti messi di uscita risolve il problema che l'utente voleva risolvere).

#Am104N2: Gli Stati membri assicurano che, qualora una rivendicazione di brevetto menzioni caratteristiche che implicano l'uso di un programma per elaboratore, un'applicazione di riferimento, ben funzionante e ben documentata, di tale programma sia pubblicata come parte della descrizione senza condizioni di licenza restrittive.

#Art6: La protezione conferita dai brevetti per le invenzioni che rientrano nel campo d'applicazione della presente direttiva lascia impregiudicate le facoltà riconosciute dalla direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare le disposizioni relative alla decompilazione e all'interoperabilità o le disposizioni relative alle topografie dei semiconduttori o ai marchi commerciali.

#Art6Syn: La proposta della Commissione Europea non protegge l'interoperabilità ma, al contrario, assicura che il software interoperabile non possa essere pubblicato o usato quando un'interfaccia è brevettata. Il solo comportamento che la Commissione Europea vuole permettere in questo caso è la decompilazione, che non infrangerebbe comunque i brevetti (poichè i brevetti implicano l'uso di quello che è descritto nei diritti, non lo studio dell'oggetto in esame). Inoltre gli emendamenti 66 e 67 specificano le leggi sulle quali i privilegi di interoperabilità possono essere basati, rendendo così ancora più certo che la misura non possa avere alcun effetto.

#Am19: La protezione conferita dai brevetti per le invenzioni che rientrano nel campo d'applicazione della presente direttiva lascia impregiudicate le facoltà riconosciute dalla direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare le disposizioni relative alla decompilazione e all'interoperabilità o le disposizioni relative alle topografie dei semiconduttori o ai marchi commerciali.

#Art6aSyn: Gli emendamenti 20 e 50 sono gli unici che formulano un vero privilegio di interoperabilità.  Il termine %(q:diversi sistemi e reti) dovrebbe essere corretto a %(q:sistemi di elaborazione dei dati) se possibile. L'emendamento 50 fa proprio questo.  20 è stato già accettato in CULT, ITRE e JURI.

#Am76Syn: L'emendamento 76 elimina i chiarimenti riguardo a quando l'uso di una tecnica brevettata per fini di interoperabilità è permessa e quando no. Va contro l'obiettivo principale della direttiva, che è quello di rendere concrete alcune meta-regole troppo astratte, comprese quelle dell'art 30 TRIP, così da ottenere chiarezza e certezza del diritto.

#Am105Syn: Siamo contenti che l'emendamento 105 concordi con noi nel sostenere che un'%(q:invenzione realizzata con un computer) non può significare altro che software che gira su un elaboratore. Però le eccezioni che cita sono molto strane. Per prima cosa: se l'invenzione è una macchina indipendente, per definizione non c'è alcun bisogno di interoperabilità. Se non c'è alcun bisogno di interoperabilità l'emendamento 20 non si applica. Viceversa, se c'è bisogno di interoperabilità allora l'invenzione brevettata non potrà funzionare come una macchina indipendente o un'innovazione tecnologica. La seconda clausola è piuttosto pericolosa perchè lascia tutto alla discrezionalità dei giudici, fallendo pertanto lo scopo chiarificatore della direttiva. Come dimostra il caso anti-trust contro Microsoft negli USA e in Europa, è molto difficile e oneroso determinare se un dato comportamento viola le leggi sulla concorrenza. Ciò renderebbe la clausola dell'interoperabilità poco efficace e metterebbe le piccole e medie imprese ulteriormente in posizione svantaggiata, dato che le grandi imprese hanno molti più fondi a disposizione da spendere in contenziosi.

#Am50: Gli Stati membri assicurano che, ogniqualvolta l'utilizzazione di una tecnica brevettata sia necessaria per il solo scopo di assicurare la conversione tra le convenzioni utilizzate in due sistemi diversi di elaborazione di dati in modo da consentire reciprocamente la comunicazione e lo scambio di contenuto di dati, tale utilizzazione non sia considerata una violazione del brevetto.

#Am20: Gli Stati membri assicurano che, nel caso in cui l'uso di una tecnica brevettata sia necessario al solo fine di garantire la conversione delle convenzioni utilizzate in due diversi sistemi o reti di elaboratori elettronici, così da consentire la comunicazione e lo scambio dei dati fra di essi, detto uso non sia considerato una violazione di brevetto.

#Am76: Gli Stati membri assicurano che, in ogni caso in cui l'uso di una tecnica brevettata sia necessario per un fine importante quale ad esempio garantire la conversione delle convenzioni utilizzate in due diversi sistemi o reti di elaboratori elettronici, così da consentire la comunicazione e lo scambio dei dati fra di essi, detto uso non sia considerato una violazione di brevetto purchè non sia indebitamente in contrasto con un normale sfruttamento del brevetto e non pregiudichi in modo ingiustificato i legittimi interessi del titolare del brevetto, tenuto conto dei legittimi interessi dei terzi.

#Am105: %(al|Uso delle tecnologie brevettate|Gli Stati membri assicurano che, nel caso in cui l'uso di una tecnica brevettata sia necessario al solo fine di garantire la conversione delle convenzioni utilizzate in due diversi sistemi o reti di elaboratori elettronici, così da consentire la comunicazione e lo scambio dei dati fra di essi, detto uso non sia considerato una violazione di brevetto, a condizione che:|%(ol|l'invenzione che costituisce il software utilizzato nel sistema informatico non funzioni come macchina indipendente o invenzione tecnica e|il brevetto non contrasti con la normativa europea in materia di concorrenza.))

#Art6b: Questo emendamento sembra creare un nuovo diritto di proprietà al di fuori del sistema dei brevetti. Esso stabilisce una categoria chiamata %(q:invenzioni implementate con il computer), che non appartiene a nessun %(q:campo della tecnologia). Questo è un approccio interessante, sebbene 7 anni siano probabilmente ancora troppi e alcune ulteriori questioni debbano venir chiarite affinchè questo sia realizzabile.

#Am106: %(al|Durata del brevetto|La durata del brevetto rilasciato per invenzioni attuate per mezzo di elaboratori elettronici, a norma della presente direttiva, è di 7 anni dalla data di presentazione della domanda.)

#Artt7: La Commissione osserva gli effetti delle invenzioni attuate per mezzo di elaboratori elettronici sull'innovazione e sulla concorrenza, in Europa e sul piano internazionale, e sulle imprese europee, compreso il commercio elettronico.

#Art7Syn: Ci si dovrebbe assicurare che le ricerche non siano fatte dai legali brevettuali del DG MARKT, bensì da esperti del mondo del software e dell'economia. L'emendamento 71 parla della creazione di un sistema di assicurazioni per l'acquisizione e i contenziosi, anche se non c'è alcun esempio pratico di un tale sistema. La possibilità teorica suggerita dal rapporto dei consulenti del DG MARKT è basata su un'assunzione sbagliata come si spiega in %(le:open letter to the European Parliament) firmata da un certo numero di economisti di spicco. L'emendamento 91 è un pò meglio, ma, dato che si fa esplicito riferimento alle piccole e medie imprese, si dovrebbero citare anche il software free/opensource e la standardizzazione. Inoltre non si garantisce che lo studio sia compiuto da un gruppo indipendente di esperti di economia del software.

#Am21: La Commissione osserva gli effetti della protezione conferita dal brevetto per quanto riguarda le invenzioni attuate per mezzo di elaboratori elettronici sull'innovazione e sulla concorrenza, in Europa e sul piano internazionale, e sulle imprese europee, in particolare le PMI e il commercio elettronico.

#Am91: La Commissione osserva gli effetti della protezione conferita dal brevetto per quanto riguarda le invenzioni attuate per mezzo di elaboratori elettronici sull'innovazione e sulla concorrenza, in Europa e sul piano internazionale, e sulle imprese europee. Particolare attenzione viene attribuita alla posizione delle PMI e a quelle del commercio elettronico, nonchè agli effetti delle posizioni dominanti sul funzionamento del mercato.

#Am71Syn: Amendment 71 calls for creation of an insurance system for patent acquisition and litigation, although there is no practical example of such a working system. The theoretical possibility as suggested by a consultants report to DG MARKT is also based on wrong assumptions, as explained in %(le:open letter to the European Parliament) written by a number of distinguished economists.

#Am90Syn: Amendment 90 is like 71, but less explicit.

#Art7PI: Both amendments 71 and 90 call for an insurance for promotion not of defence of software companies against patent aggression, but, on the contrary, of aggressive use of patents by specialised patent litigation SMEs such as Eolas and Allvoice against software companies.  The archetype for the %(q:Patent Defence) concept is the %(q:Patent Defence Union) created by the CEO of %(AV).  Allvoice is the %(q:ten-person company located in an employment blackspot in south-west England) which Arlene McCarthy praises in her JURI Draft Report.  Allvoice has hardly produced any software itself, certainly not speech recognition software, but it used two trivial and broad patents on user interfaces in order to extort money from real speech recognition software companies.  By promoting the Patent Defence scheme as set out in this amendment, MEPs should note that they would be promoting litigation instead of innovation.

#weW: Several EU studies on SMEs and software patents have found that there are systematic reasons why SMEs are not using the patent system.  These are unlikely to be overcome by any proselytising system, no matter how much public money is poured into it.

#Am90: The Commission shall examine the question of how to make patent protection more readily accessible to small and medium-sized enterprises and ways of assisting them with the costs of obtaining and enforcing patents.

#Am71P1: La Commissione osserva gli effetti delle invenzioni attuate per mezzo di elaboratori elettronici sull'innovazione e sulla concorrenza, in Europa e sul piano internazionale, e sulle imprese europee, in particolare le piccole e medie imprese e i produttori di software libero, nonchè il commercio elettronico.

#Am71P2: La Commissione valuta come rendere la tutela brevettuale più prontamente accessibile alle piccole e medie imprese e come assistere queste ultime per quanto riguarda i costi legati all'ottenimento e all'applicazione dei brevetti, segnatamente attraverso la creazione di un fondo di difesa e l'introduzione di regole speciali concernenti le spese legali.

#Am71P3: La Commissione riferisce al Parlamento europeo e al Consiglio in merito ai risultati che ha raggiunto e presenta senza ritardi proposte legislative appropriate.

#rbr: The Commission shall report to the European Parliament and the Council by [DATE (three years from the date specified in Article 9(1))] at the latest on

#lrl: These provisions have no regulatory effect, and are mostly of minor importance.  The PPE-DE amendments (by M. Thyssen) do bring some improvement.  The interoperability privilege formulated by Art 6a is a new regulatory approach, which needs observation and reporting, also in respect to how Art 30ff TRIPs is to be concretised in European law.  These discussions have been missing so far.  Amendment 93 is better worded for this purpose than 89.  Amendment 94 somewhat clarifies the purpose of the reporting exercise, thus making the article clearer.

#cia: the impact on the conversion of the conventions used in two different computer systems to allow communication and exchange of data

#Am92: whether the rules governing the term of the patent and the determination of the patentability requirements, and more specifically novelty, inventive step and the proper scope of claims, are adequate; and

#nvy: whether the option outlined in the Directive concerning the use of a patented invention for the sole purpose of ensuring interoperability between two systems is adequate;

#neh: In this report the Commission shall justify why it believes an amendment of the Directive in question necessary or not and, if required, will list the points which it intends to propose an amendment to.

#Rej: Rifiuti/Approvazioni

#app: approva la direttiva proposta

#dWt: E' possibile correggere la direttiva usando gli emendamenti elencati. Se i giusti emendamenti vengono adottati, la direttiva potrà portare benefici alla UE. Pertanto l'adozione di una direttiva attentamente corretta sarebbe meglio del suo rigetto. D'altro canto, emendare una direttiva così attentamente è un compito complesso, specialmente in seduta plenaria, ed il Parlamento Europeo potrebbe non riuscire a correggere i numerosi assunti e fraintendimenti introdotti dalla Commissione. La direttiva ha percorso un iter che non ha tenuto conto delle parti maggiormente interessate, studi accademici o testimonianze. Rigettare il testo e chiedere alla Commissione di ricominciare da capo con il dovuto procedimento sarebbe altrettanto giutificabile. Infatti, il numero di errori che necessitano di venir corretti è così ampio, che confidare in una buona direttiva ottenuta con voto plenario è molto azzardato. Visti i rischi al progresso, la libertà e all'abilità per le compagnie dell'EU di competere se gli emendamenti dovessero fallire parzialmente, il rigetto è maggiormente desiderabile. Questa è la legislazione per i prossimi 20 anni. Sebbene ciò abbia già richiesto del tempo, siamo ancora all'inizio del processo di comprensione e non vi è fretta.

#ute: rifiuta la direttiva proposta

#Rec1Cec: La realizzazione del mercato interno implica l'eliminazione delle restrizioni alla libera circolazione e delle distorsioni della concorrenza nonchè la creazione di condizioni favorevoli all'innovazione e agli investimenti. In questo contesto la protezione delle invenzioni mediante i brevetti è un elemento essenziale per il successo del mercato interno. Una protezione efficace ed armonizzata delle invenzioni attuate per mezzo di elaboratori elettronici in tutti gli Stati membri è indispensabile per mantenere e stimolare gli investimenti in questo campo.

#Rec1Syn: La Commissione postula, in contrasto con ogni %(ss:evidenza economica, compreso %(bh:very detailed empirical research by Bessen & Hunt from 2003), che i brevetti incoraggino gli investimenti nello sviluppo del software. Inoltre il termine %(q:invenzione realizzata con un computer) è fonte di confusione. E' meglio non avere un obiettivo preciso, piuttosto che averne uno sbagliato.

#Am1: La realizzazione del mercato interno implica l'eliminazione delle restrizioni alla libera circolazione e delle distorsioni della concorrenza nonchè la creazione di condizioni favorevoli all'innovazione e agli investimenti. In questo contesto la protezione delle invenzioni mediante i brevetti è un elemento essenziale per il successo del mercato interno. Una protezione efficace, trasparente ed armonizzata delle invenzioni attuate per mezzo di elaboratori elettronici in tutti gli Stati membri è indispensabile per mantenere e stimolare gli investimenti in questo campo.

#Rec5Cec: È pertanto necessario armonizzare le disposizioni di legge e la loro interpretazione da parte dei tribunali degli Stati membri e rendere trasparenti le norme che disciplinano la brevettabilità delle invenzioni attuate per mezzo di elaboratori elettronici. La certezza giuridica che ne risulterà dovrebbe permettere alle imprese di ricavare il massimo vantaggio dai brevetti di invenzioni attuate per mezzo di elaboratori elettronici e stimolare gli investimenti e l'innovazione.

#Rec5Syn1: Sia la Commissione Europea (CEC) che l'emendamento 2 (JURI) affermano dogmaticamente che i brevetti del software stimolano l'innovazione. Sarebbe più opportuno dichiarare un obiettivo politico, rispetto al quale il proposto regime di brevettabilità possa essere valutato.

#Rec5Syn2: L'emendamento 2 sembra affermare che il semplice fatto di scrivere una legge europea, a prescindere da ciò che contiene, %(q:sfocia nella certezza legale), solo perchè il Tribunale del Lussemburgo può interpretare tale legge. Questa proposizione non solo è dubbia, ma anche senza costrutto, visto che il brevetto comunitario è sotto di esso in ogni caso. Ci sono modi più semplici e sistematici di conferire autorità alla corte di Lussemburgo che aggirare l'interpretazione di una direttiva per un aspetto della European Patent Convention.

#Am2: È pertanto necessario armonizzare le disposizioni di legge che disciplinano la brevettabilità delle invenzioni attuate per mezzo di elaboratori elettronici, onde garantire che la certezza giuridica che ne risulterà e il livello dei requisiti richiesti per la brevettabilità permettano alle imprese innovative di ricavare il massimo vantaggio dal loro processo inventivo e stimolare gli investimenti e l'innovazione. La certezza giuridica viene altresì garantita dal fatto che, in caso di dubbio sullinterpretazione della presente direttiva, i tribunali nazionali possono e i tribunali nazionali di ultima istanza devono chiedere una sentenza alla Corte di giustizia delle Comunità europee.

#css: La certezza giuridica viene altresì garantita dal fatto che, in caso di dubbio sull'interpretazione della presente direttiva, i tribunali nazionali possono e i tribunali nazionali di ultima istanza devono chiedere una sentenza alla Corte di giustizia delle Comunità europee.

#Rec5aSyn: Siamo d'accordo sul fatto che le regole esposte nell'articolo 52 EPC sono state forzate ben oltre l'interpretazione dell'EPO e che il loro significato originario, come descritto nelle linee guida all'esame dei brevetti EPO 1978, deve essere riconfermato.

#Am88: Occorre confermare e precisare le norme riguardanti i limiti della brevettabilità, ai sensi dell'articolo 52 dellaccordo europeo sui brevetti. La certezza giuridica che ne deriva dovrebbe contribuire ad un clima più aperto agli investimenti e alle innovazioni nel settore dei software.

#Rec6Cec: La Comunità e i suoi Stati membri sono parti dell'accordo sugli aspetti dei diritti di proprietà intellettuali attinenti al commercio, approvato con la decisione del Consiglio 94/800/CE, del 22 dicembre 1994, relativa alla conclusione a nome della Comunità europea, per le materie di sua competenza, degli accordi dei negoziati multilaterali dell'Uruguay Round (1986-1994). L'articolo 27, paragrafo 1 di detto accordo dispone che possono costituire oggetto di brevetto tutte le invenzioni, di prodotti o di processi, in tutti i campi della tecnologia, che presentino carattere di novità, implichino un'attività inventiva e siano atte ad un'applicazione industriale. Inoltre, in base all'accordo, i brevetti possono essere ottenuti e i relativi diritti possono essere esercitati senza discriminazioni quanto al settore della tecnologia. Questi principi valgono di conseguenza per le invenzioni attuate per mezzo di elaboratori elettronici.

#Rec6Syn: Vogliamo sia chiaro che ci sono dei limiti a quello che può essere compreso nel termine %(q:campi della tecnologia) secondo l'art. 27 TRIP e che questo articolo non è pensato per permettere una brevettabilità illimitata, bensì per limitare gli attriti nel libero mercato, che potrebbero essere provocati da indebite eccezioni, così come da indebite estensioni della brevettabilità. Questa interpretazione del TRIP è confermata indirettamente dalle attività di lobbying del governo americano contro l'art. 27 TRIP, sulla base del fatto che tale articolo potrebbe escludere (o potrebbe essere usato per escludere) brevetti di software e pratiche commerciali, che invece il Governo USA vuole regolare con il nuovo progetto di legge Substantive Patent Law Treaty, si veda %(bh:Brian Kahin article in First Monday). L'emendamento 31 cancella l'articolo sbagliato.

#Am31: soppresso

#Rec7Cec: Secondo la convenzione sul rilascio dei brevetti europei, firmata a Monaco di Baviera il 5 ottobre 1973, e secondo le legislazioni degli Stati membri in materia di brevetti, i programmi per elaboratore, nonchè le scoperte, le teorie scientifiche, i metodi matematici, le creazioni estetiche, i piani, i principi e i metodi per attività intellettuali, giochi o attività commerciali e le presentazioni di informazioni sono espressamente non considerati invenzioni e sono quindi esclusi dalla brevettabilità. Questa eccezione, tuttavia, si applica ed è giustificata soltanto nella misura in cui una domanda di brevetto o un brevetto si riferisce a tali materie o attività in quanto tali, perchè tali materie o attività in quanto tali non appartengono ad un settore della tecnologia.

#Rec7Syn: Il significato di questo provvedimento sembra poco chiaro. L'obiettivo di una direttiva è quello di stabilire delle regole, non di correggere altri testi. Anche se gli stati membri EPC decidessero che vale la pena emendare l'art. 52 EPC (piuttosto che superarlo con altri mezzi legali), potrebbero emendare l'EPC e dire di farlo in funzione della direttiva.

#Am32: Secondo la convenzione sul rilascio dei brevetti europei, firmata a Monaco di Baviera il 5 ottobre 1973, e secondo le legislazioni degli Stati membri in materia di brevetti, i programmi per elaboratore, nonchè le scoperte, le teorie scientifiche, i metodi matematici, le creazioni estetiche, i piani, i principi e i metodi per attività intellettuali, giochi o attività commerciali e le presentazioni di informazioni sono espressamente non considerati invenzioni e sono quindi esclusi dalla brevettabilità. Questa eccezione si applica perchè tali materie o attività non appartengono ad un settore della tecnologia.

#Am3: Con la presente direttiva non si intende modificare la succitata Convenzione, bensì evitare che possano esistere interpretazioni divergenti del relativo testo.

#Rec7bSyn: Una nuova risoluzione è necessaria per una maggiore responsabilizzazione dell'EPO. L'effetto sarebbe maggiore se fosse un articolo, ma è comunque un buon principio da conservare. E' importante dichiarare tutte le fonti di conflitto per facilitare le eventuali soluzioni.

#Am95: Il Parlamento europeo ha chiesto a più riprese che l'Ufficio europeo dei brevetti rivedesse le sue norme di funzionamento e che fosse soggetto a controllo pubblico nell'esercizio delle sue funzioni. In proposito, sarebbe particolarmente opportuno rimettere in discussione la prassi in base alla quale l'Ufficio percepisce introiti per i brevetti che rilascia, in quanto essa nuoce al carattere pubblico di tale organismo. Nella sua risoluzione sulla decisione dell'Ufficio europeo dei brevetti concernente il brevetto n. EP 695 351 rilasciato l'8 dicembre 1999 il Parlamento europeo ha chiesto la revisione delle norme di funzionamento dell'Ufficio in questione, onde garantire che esso %q(sia soggetto a un obbligo di pubblicità nell'esercizio delle sue funzioni).

#cdn: La brevettabilità del software sarebbe un problema per tutti gli sviluppatori di software che non siano sostenuti dalle risorse finanziarie e legali di una grossa azienda, e questo ovviamente include un notevole numero di sviluppatori di software gratuito.

#Am61: Il software gratuito fornisce un modo estremamente prezioso e socialmente utile per costruire un'innovazione comune e condivisa, nonchè una divulgazione della conoscenza.

#Rec11: Although computer-implemented inventions are considered to belong to a field of technology, in order to involve an inventive step, in common with inventions in general, they should make a technical contribution to the state of the art.

#nry: The four patentibility tests laid out in Art 52 EPC state that there must be a (technical) invention and that additionally, this invention must be susceptible of industrial application, new and involve an inventive step. The %(q:inventive step) conditions is defined in Art 56 EPC as requiring the invention not to be an obvious combination of known techniques to a person skilled in the art. Additionally, %(q:invention) and %(q:technical contribution) are synonyms: you can only invent new things, and this invention is your (technical) contribution to the state of the art.  Therefore, the CEC original and amendments 4 and 84 are in direct contradiction with the EPC. Additionally, such a logic assures that patentability is simply a question of claim wording, see %(se:Four Separate Tests on One Unified Object).  In order to avoid confusion and conflicts with EPC Art 52 and 56, the recital needs to be rewritten or deleted.

#Am51: Sebbene i programmi per elaboratori elettronici siano astratti e non appartengano a nessunsettore particolare, vengono utilizzati per illustrare e controllare i processi in tutti i settori delle scienze naturali applicate e delle scienze sociali.

#Am4: Per poter essere brevettabili, le invenzioni in generale e le invenzioni attuate per mezzo di elaboratori elettronici in particolare devono essere suscettibili di applicazione industriale, presentare un carattere di novità e implicare unattività inventiva. Le invenzioni attuate per mezzo di elaboratori elettronici devono costituire un contributo tecnico allo stato dell'arte per poter essere considerate implicanti un'attività inventiva.

#Am84: Per poter essere brevettabili, le invenzioni in generale e le invenzioni attuate per mezzo di elaboratori elettronici in particolare devono essere suscettibili di applicazione industriale, presentare un carattere di novità e implicare unattività inventiva. Le invenzioni attuate per mezzo di elaboratori elettronici devono costituire un contributo tecnico nuovo allo stato dell'arte per poter essere distinte dai meri software e considerate implicanti un'attività inventiva.

#Rec11Syn: Il testo della Commissione dichiara che i programmi per computer sono invenzioni tecniche. Rimuove il requisito indipendente di invenzione (= il %(q:contributo tecnico)) e lo unisce nel requisito della non-ovvietà (= il %q(progresso tecnologico), leggere commenti della proposta 11). Questo porta ad una teorica inconsistenza e a conseguenze pratiche non desiderabili.

#Rec11bSyn: L'emendamento 73 sarebbe una perfetta riformulazione dell'articolo 52 EPC se avesse detto %(q:invenzioni) invece di %(q:invenzioni realizzate con un computer). Così come è ora, sembra come se ci fosse una particolare regola per le invenzioni riguardanti i computer. E' anche utile che ribadisca la nozione che la stessa invenzione debba passare i quattro test. Leggere l'articolo 1 per il motivo per cui il termine %(q:invenzione realizzata con un computer) è fuorviante.

#Am73: Le invenzioni attuate per mezzo di elaboratori elettronici sono brevettabili solo se rientrano nel settore della tecnica e inoltre presentano carattere di novità, implicano un'attività inventiva e sono suscettibili di applicazioni industriali.

#Rec12: Di conseguenza, se un'invenzione non costituisce un contributo tecnico allo stato dell'arte, come nel caso in cui, ad esempio, il suo contributo specifico non presenta un carattere tecnico, non pu? essere considerata implicante un'attivit? inventiva e quindi non ? brevettabile.

#Rec12Syn: Il testo della Commissione Europea fonde la prova di %(q:invenzione tecnica) dentro la prova del %(q:progresso tecnologico) indebolendo pertanto entrambe le prove ed aprendo uno spazio infinito di interpretazione. Questo contraddice l'Art 52 EPC, che è incoerente da un punto di vista teorico, e conduce a conseguenze pratiche indesiderabili, come rendere non fattibile l'esame da parte di alcuni uffici nazionali dei brevetti. Di nuovo, si veda la spiegazione nel Recital 11 per maggiori informazioni. L'emendamento 5 di JURI rende le cose ancora peggiori: in primo luogo, letteralmente reintroduce l'articolo 3 (che aveva abrogato), in secondo luogo afferma che il requisito del %(q:progresso inventivo) è qualcosa di completamente diverso da quello che afferma l'Art 56 EPC, in terzo luogo comincia a parlare di un %(q:problema tecnico) che dovrebbe essere risolto (mentre la brevettabilità non ha niente a che vedere con il tipo di problema risolto, ma piuttosto con la soluzione impiegata), e infine ripete anche gli errori dell'originale CEC.

#Am5: Di conseguenza, anche se una invenzione attuata per mezzo di elaboratori elettronici appartiene per sua stessa natura al settore della tecnologia è importante stabilire chiaramente che, se un'invenzione non costituisce un contributo tecnico allo stato dell'arte, come nel caso in cui, ad esempio, il suo contributo specifico non presenta un carattere tecnico, non può essere considerata implicante un'attività inventiva e quindi non è brevettabile. Qualora si valuti se sia presente unattività inventiva, si è soliti applicare lapproccio problema-soluzione onde stabilire se vi sia un problema tecnico da risolvere. Se non viene riscontrato alcun problema tecnico, linvenzione non può essere considerata contributo tecnico allo stato dellarte.

#Am114: Di conseguenza, un'innovazione che non costituisca un contributo tecnico allo stato dell'arte, non è considerata un'invenzione ai sensi delle norme in materia di brevetti.

#Rec13: Una procedura definita o una sequenza di azioni svolte nel contesto di un apparato come un computer possono fornire un contributo tecnico allo stato dell'arte e quindi costituire un invenzione brevettabile. Comunque, un algoritmo che è definito senza alcun riferimento ad un contesto fisico non è tecnico e non può quindi costituire un'invenzione brevettabile.

#Rec13Syn: Inserita nel contesto delle procedure di esame di un brevetto, questa affermazione non è ciò che sembra. Allarga la brevettabilità anche rispetto alle pratiche EPO, permettendo così la brevettabilità di soluzioni non-tecniche a problemi tecnici. Inoltre, rende tutti gli algoritmi brevettabili in ogni situazione, poichè un %(q:problema tecnico) può sempre essere definito da un dispositivo computazionale generico, il che allo stesso tempo fornisce un modo di rivendicare diritti sugli algoritmi nella loro forma più astratta.

#Rec13aSyn: Le ambiguità sintattiche in questo emendamento lasciano in dubbio se in generale le %(q:pratiche commerciali) sono da considerarsi %(q:metodi dove il solo contributo allo stato dell'arte non è tecnico) o se alcune pratiche commerciali possono contenere %(q:contributi tecnici). Ancora più importante è il fatto che questo emendamento in realtà non esclude alcunchè, perchè gli avvocati esperti di brevetti immediatamente affermeranno che le funzionalità legate all'uso dei computer sono caratteristiche per l' %(q:invenzione), che, come si dice altrove nella direttiva, deve essere %(q:considerata interamente). Perchè l'emendamento sia di una qualche utilità, l'ambiguità sintattica circa gli %(q:altri metodi) va rimossa e la categoria dei %(q:metodi non tecnici) va spiegata con definizioni ed esempi.

#Am6: Tuttavia, la semplice attuazione di un metodo altrimenti non brevettabile su di un apparecchio come un elaboratore non è di per sè sufficiente per giustificare la conclusione che un contributo tecnico sia presente. Di conseguenza, un metodo per attività commerciali o di altro tipo che venga attuato per mezzo di elaboratori elettronici, in cui lunico contributo allo stato dellarte non sia tecnico, non può costituire uninvenzione brevettabile.

#Rec13bSyn: Questo fa un'affermazione di buon senso che è stata spesso trascuratain favore dell'estensione della brevettabilità. Questo emendamento da solo non fa granchè e l'affermazione che la legge %(q:non può essere aggirata) è una pia illusione. D'altronde un'illusione è meglio di niente.

#Am7: Se il contributo allo stato dellarte è relativo unicamente a elementi non brevettabili, non vi può essere uninvenzione brevettabile, indipendentemente dal modo in cui tali elementi vengano presentati nelle rivendicazioni. Per esempio, il requisito di contributo tecnico non può essere eluso semplicemente specificando mezzi tecnici nelle rivendicazioni di brevetto.

#Am8: Inoltre, un algoritmo è intrinsecamente non tecnico e, pertanto, non può costituire uninvenzione tecnica. Tuttavia, un metodo che comporti lutilizzazione di un algoritmo può essere brevettabile purchè venga usato per risolvere un problema tecnico. Ciononostante, un brevetto concesso per tale metodo non deve permettere che si monopolizzi lo stesso algoritmo o la sua utilizzazione in contesti non previsti nel brevetto.

#Rec13dSyn1: L'emendamento riafferma semplicemente la definizione di rivendicazione brevettuale: la rivendicazione brevettuale definisce il diritto di esclusione garantito dal brevetto. Esso permette di esercitare tale diritto sul software, semplicemente facendo  riferimento a generici apparati di calcolo o processi di calcolo, indipendentemente dalla presenza di innovazione. E' come dire: si può brevettare fintantochè si utilizzano termini come memorizzazione, processore o apparato nella rivendicazione, ma si faccia attenzione a non estendere troppo le proprie rivendicazioni, perchè non si può estendere il monopolio su ciò di cui il brevetto non tratta.

#Rec13dSyn2: L'emendamento può avere un beneficio indiretto: sembra contraddire l'emendamento 18 all'articolo 5 dicendo che solo gli apparati programmati e i processi possono essere rivendicati nei brevetti.

#Am9: La portata dei diritti esclusivi conferiti da un brevetto è definita nelle relative rivendicazioni. Le invenzioni attuate per mezzo di elaboratori elettronici vanno rivendicate con riferimento a un prodotto, quale un apparecchio programmato, o a un processo svolto da tale apparecchio. Di conseguenza, lutilizzo di singoli elementi di un programma per elaboratore in contesti che non comportano la realizzazione di un prodotto o un processo regolarmente rivendicato non deve costituire una violazione di brevetto.

#Rec14: La tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici non deve richiedere una legislazione specifica che sostituisca le norme nazionali in materia di brevetti. Le norme nazionali in materia di brevetti restano la base essenziale della tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici, con le modifiche o le integrazioni relative a specifici aspetti richieste dalla presente direttiva.

#Rec14Syn1: Il considerando CEC non è male (anche se usa il termine %(q:invenzioni realizzate con un computer) invenzioni implementate tramite computer, si veda l'Articolo 1). Afferma semplicemente che la legge sui brevetti non dovrebbe essere sostituita.  L'emendamento 10 è meno chiaro. Da una parte identifica %(q:la situazione legale) attuale con %(q:le pratiche dell'Ufficio Europeo dei brevetti). Dall'altra parte afferma che la brevettabilità delle pratiche commerciali dev'essere evitata. Comunque l'EPO stesso ha concesso molti brevetti per pratiche commerciali e ha fornito delle %(a6:motivazioni legali per sostenere i brevetti sulle pratiche commerciali). L'unico possibile effetto dell'emendamento, se c'è, sarebbe di cedere la responsabilità legislativa all'EPO, finendo per far definire la brevettabilità dalle pratiche dell'EPO, invece che dalle leggi votate dal Parlamento Europeo e da altri corpi rappresentativi democratici.

#Am86Syn: L'emendamento 84 non chiede una codifica delle pratiche attuali dell'EPO, ma d'altra parte parla di %q(pratiche non brevettabili, come procedure elementari e pratiche commerciali). Non solo le pratiche commerciali elementari devono restare non brevettabili, ma tutte le pratiche commerciali devono restare tali (altrimenti stiamo deviando dall'Art 52 EPC, cosa che andrebbe evitata, come suggerito dall'emendamento 88).

#Am10: La tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici non richiede una legislazione specifica che sostituisca le norme nazionali in materia di brevetti. Le norme nazionali in materia di brevetti restano la base essenziale della tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici. La presente direttiva semplicemente chiarisce lattuale posizione giuridica tenendo presenti le pratiche dellUfficio europeo dei brevetti in vista di garantire la certezza giuridica, la trasparenza e la chiarezza della legislazione ed evitare qualsiasi transizione verso la brevettabilità di metodi non brevettabili, come i metodi per attività commerciali.

#Am86: La tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici non richiede una legislazione specifica che sostituisca le norme nazionali in materia di brevetti. Le norme nazionali in materia di brevetti restano la base essenziale della tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici. La presente direttiva si limita a chiarire lattuale posizione giuridica per garantire la certezza giuridica, la trasparenza e la chiarezza della legislazione e contrastare la tendenza a sancire la brevettabilità di metodi non brevettabili, come quelli ovvi e quelli per attività commerciali.

#Rec16: La posizione concorrenziale dell'industria europea in rapporto ai suoi principali partner commerciali sarebbe rafforzata dall'eliminazione delle differenze attuali nella tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici e dalla trasparenza della situazione giuridica.

#Rec16Syn: L'unificazione della norme giuridiche di per sè stessa non costituisce una garanzia di un miglioramento della situazione delle industrie europee. Questa direttiva non dovrebbe usare il pretesto dell'%(q:armonizzazione) per cambiare le regole dell'Art. 52 EPC, che sono già in vigore in tutti gli stati. Che i brevetti software siano o no un bene per le compagnie europee che sviluppano software è indipendente dal fatto che l'industria tradizionale manifatturiera si sta muovendo verso economie a basso costo al di fuori dell'EU.

#Am11: La posizione concorrenziale dell'industria europea in rapporto ai suoi principali partner commerciali sarà rafforzata dall'eliminazione delle differenze attuali nella tutela giuridica delle invenzioni attuate per mezzo di elaboratori elettronici e dalla trasparenza della situazione giuridica. Data lattuale tendenza dellindustria manifatturiera tradizionale di dislocare le proprie attività verso economie a basso costo al di fuori dellUnione europea, limportanza della tutela della proprietà intellettuale e, in particolare, della tutela del brevetto è di per sè evidente.

#Am35: La posizione concorrenziale dell'industria europea in rapporto ai suoi principali partner commerciali potrebbe essere rafforzata dall'eliminazione dell'attuale separazione nella prassi giurisprudenziale circa i limiti della brevettabilità per quanto riguarda i programmi informatici.

#Rec17: La presente direttiva lascia impregiudicata l'applicazione delle norme in materia di concorrenza, in particolare gli articoli 81 e 82 del trattato.

#Rec17Syn: I brevetti software pongono i loro peculiari problemi di concorrenza. Sarebbe stato appropriato risolvere questi problemi all'interno della direttiva e per regolamentare come l'Art. 30-31 TRIPs vada applicato, es. da un resoconto che supporta l'Art. 6a. Non è sufficiente affidarsi alle leggi sulla concorrenza già esistenti. Perciò, sarebbe stato più appropriato eliminare questo resoconto o sostituirlo con qualcosa che risolvesse i problemi che il legislatore sta incontrando. Se questo emendamento è appoggiato, esso potrebbe essere interpretato come un segno di approvazione da parte dell'EP per il fallimento della Commissione Europea nel risolvere i problemi di concorrenza all'interno di questa direttiva.

#Am12: La presente direttiva dovrebbe lasciare impregiudicata l'applicazione delle norme in materia di concorrenza, in particolare gli articoli 81 e 82 del trattato.

#Rec18: La protezione conferita dai brevetti per le invenzioni che rientrano nel campo d'applicazione della presente direttiva lascia impregiudicate le facolt? riconosciute dalla direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare le disposizioni relative alla decompilazione e all'interoperabilit? o le disposizioni relative alle topografie dei semiconduttori o ai marchi commerciali.

#Rec18Syn: La proposta CEC dichiara di proteggere solo l'interoperabilità, ma in realtà assicura che i diritti di brevetto non possano essere limitati in alcun modo da considerazioni di interoperabilità. Si veda la spiegazione dell'Art. 6 per questo. L'emendamento 13 non fa altro che restringere ulteriormente le condizioni che possono essere usate per evitare i diritti di brevetto a quelle scritte in uno specifico insieme di leggi.

#Am13: I diritti conferiti dai brevetti rilasciati per le invenzioni che rientrano nel campo d'applicazione della presente direttiva devono lasciare impregiudicate le facoltà riconosciute ai sensi degli articoli 5 e 6 della direttiva 91/250/CEE relativa alla tutela giuridica dei programmi per elaboratore mediante il diritto d'autore, in particolare nel quadro delle disposizioni per quanto riguarda la decompilazione e l'interoperabilità. In particolare, gli atti che, a norma degli articoli 5 e 6 di detta direttiva, non sono soggetti allautorizzazione del titolare del diritto per quel che concerne i diritti dautore del titolare attinenti ad un programma per elaboratore o in esso contenuti e che, fatto salvo per i suddetti articoli, necessitano di tale autorizzazione, non devono essere soggetti allautorizzazione del titolare del diritto per quel che concerne i diritti del brevettatore attinenti al programma per elaboratore o in esso contenuti.

#Rec18bSyn: Questo suggerisce che i programmi per computer possono essere una invenzione (visto che non ci sono altre innovazioni che abbiano del codice sorgente o che possano essere decompilate), il che contraddice l'articolo 52 EPC, in quanto esso esplicitamente esclude i programmi per computer dalla brevettabilità. Il software non può mai essere considerato parte di un'invenzione. Esso può essere menzionato nella descrizione del brevetto, ma in quel caso questo serve solo per chiarire l'applicazione dell'invenzione vera e propria e a limitare la portata del monopolio che il brevetto garantisce a chi lo detiene (es. se qualcuno ha intenzione di usare l'invenzione senza usare un programma per computer, egli non infrangerà un brevetto che descrive l'uso di suddetta invenzione in combinazione con un programma per computer).

#Am74: L'applicazione della presente direttiva non può rappresentare una rottura rispetto ai principi tradizionali del diritto dei brevetti; pertanto chi richiede il brevetto deveinserire la descrizione di tutti gli elementi della sua invenzione, compreso il codice di base e deve permettere una indagine sul brevetto e anche la sua scomposizione. Questo meccanismo risultanecessario per permettere le licenze obbligatorie, ad esempio quando non venga rispettato l'obbligo di rifornire il mercato.

#Rec18tSyn: Anche questo suggerisce  l'idea che i programmi per computer possono essere invenzioni.

#Am75: In ogni caso, la legislazione degli Stati membri deve assicurare che i brevetti contengano novità e un apporto di inventiva per impedire che ci si appropri di invenzioni già di dominio pubblico inserendole semplicemente in un programma per elaboratori elettronici.

#pmd: Europarlamento 2003/09: Emendamenti

#eus: Qui gli emendamenti per la decisione plenaria sulla direttiva dei brevetti del software sono pubblicati in diversi linguaggi.

#ata: Molti dei partecipanti, includendo quelli che sono supposti beneficiare dal periodo di sospensione, esprimono dubbi riguardo questo concetto dal duplice aspetto.

#enW: Un'altra versione degli emendamenti pubblicabili, qualche volta leggermente in anticipo a questa pagina.

#soW: Results of the Vote

#Wan: Documenti pubblici riguardanti il Voto Plenario

#aaW: Emendamenti stesi da vari gruppi e relative analisi, nella misura in cui possano essere pubblicate.

#eus2: Create a multilingual amendments database

#lrl2: One relational database table is enough, consisting of three fields: amendment number, language symbol and text.

#WnW: More tables can be added later.

#WWi: Gli aiuti rendono questa pagina multilingua

#uhn: E' molto apprezzato da molti MEP poter leggere queste analisi nella loro lingua.

#diW: Questa pagina è stata creata con un sistema di gestione multilingua, che rende conveniente per lei contribuire, se osserva qualche regola.

# Local Variables: ;
# coding: utf-8 ;
# srcfile: /ul/prg/src/mlht/sys/mlhtmake.el ;
# mailto: mlhtimport@a2e.de ;
# login: phm ;
# passwd: YYYYY ;
# feature: swpatdir ;
# dok: plen0309 ;
# txtlang: it ;
# multlin: t ;
# End: ;

